/**
 * @file gntdebug.c GNT Debug API
 * @ingroup finch
 */
/* finch
 *
 * Finch is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <internal.h>
#include <gnt.h>
#include <gntbox.h>
#include <gntbutton.h>
#include <gntcheckbox.h>
#include <gntentry.h>
#include <gntfilesel.h>
#include <gntlabel.h>
#include <gntline.h>
#include <gnttextview.h>
#include "gntdebug.h"
#include "finch.h"
#include "notify.h"
#include "util.h"
#include <stdio.h>
#include <string.h>
#define PREF_ROOT "/finch/debug"

static gboolean handle_fprintf_stderr_cb(GIOChannel *source,GIOCondition cond,gpointer null)
{
  gssize size;
  char message[1024UL];
  size = read(g_io_channel_unix_get_fd(source),message,(sizeof(message) - 1));
  if (size <= 0) {
/* Something bad probably happened elsewhere ... let's ignore */
  }
  else {
    message[size] = 0;
    g_log("stderr",G_LOG_LEVEL_WARNING,"%s",message);
  }
  return 1;
}

static void handle_fprintf_stderr(gboolean stop)
{
  GIOChannel *stderrch;
  static int readhandle = -1;
  int pipes[2UL];
  if (stop != 0) {
    if (readhandle >= 0) {
      g_source_remove(readhandle);
      readhandle = -1;
    }
    return ;
  }
  if (pipe(pipes) != 0) {
    readhandle = -1;
    return ;
  };
  dup2(pipes[1],2);
  stderrch = g_io_channel_unix_new(pipes[0]);
  g_io_channel_set_close_on_unref(stderrch,1);
  readhandle = (g_io_add_watch_full(stderrch,(-100),(G_IO_IN | G_IO_ERR | G_IO_PRI),handle_fprintf_stderr_cb,0,0));
  g_io_channel_unref(stderrch);
}
static struct __unnamed_class___F0_L94_C8_unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__tview__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__search__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__paused {
GntWidget *window;
GntWidget *tview;
GntWidget *search;
gboolean paused;}debug;

static gboolean match_string(const char *category,const char *args)
{
  const char *str = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)debug.search),gnt_entry_get_gtype()))));
  if (!(str != 0) || !(( *str) != 0)) 
    return 1;
  if (g_strrstr(category,str) != ((gchar *)((void *)0))) 
    return 1;
  if (g_strrstr(args,str) != ((gchar *)((void *)0))) 
    return 1;
  return 0;
}

static void finch_debug_print(PurpleDebugLevel level,const char *category,const char *args)
{
  if (((debug.window != 0) && !(debug.paused != 0)) && (match_string(category,args) != 0)) {
    int pos = gnt_text_view_get_lines_below(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)debug.tview),gnt_text_view_get_gtype()))));
    GntTextFormatFlags flag = GNT_TEXT_FLAG_NORMAL;
    const char *mdate;
    time_t mtime = time(0);
    mdate = purple_utf8_strftime("%H:%M:%S ",(localtime((&mtime))));
    gnt_text_view_append_text_with_flags(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)debug.tview),gnt_text_view_get_gtype()))),mdate,flag);
    gnt_text_view_append_text_with_flags(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)debug.tview),gnt_text_view_get_gtype()))),category,GNT_TEXT_FLAG_BOLD);
    gnt_text_view_append_text_with_flags(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)debug.tview),gnt_text_view_get_gtype()))),": ",GNT_TEXT_FLAG_BOLD);
    switch(level){
      case PURPLE_DEBUG_WARNING:
{
        flag |= GNT_TEXT_FLAG_UNDERLINE;
      }
      case PURPLE_DEBUG_ERROR:
{
      }
      case PURPLE_DEBUG_FATAL:
{
        flag |= GNT_TEXT_FLAG_BOLD;
        break; 
      }
      default:
{
        break; 
      }
    }
    gnt_text_view_append_text_with_flags(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)debug.tview),gnt_text_view_get_gtype()))),args,flag);
    if (pos <= 1) 
      gnt_text_view_scroll(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)debug.tview),gnt_text_view_get_gtype()))),0);
  }
}

static gboolean finch_debug_is_enabled(PurpleDebugLevel level,const char *category)
{
  return (debug.window != 0) && !(debug.paused != 0);
}
static PurpleDebugUiOps uiops = {(finch_debug_print), (finch_debug_is_enabled), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* padding */
};

PurpleDebugUiOps *finch_debug_get_ui_ops()
{
  return &uiops;
}

static void reset_debug_win(GntWidget *w,gpointer null)
{
  debug.window = (debug.tview = (debug.search = ((GntWidget *)((void *)0))));
}

static void clear_debug_win(GntWidget *w,GntTextView *tv)
{
  gnt_text_view_clear(tv);
}

static void print_stderr(const char *string)
{
  g_printerr("%s",string);
}

static void toggle_pause(GntWidget *w,gpointer n)
{
  debug.paused = !(debug.paused != 0);
}
/* Xerox */

static void purple_glib_log_handler(const gchar *domain,GLogLevelFlags flags,const gchar *msg,gpointer user_data)
{
  PurpleDebugLevel level;
  char *new_msg = (char *)((void *)0);
  char *new_domain = (char *)((void *)0);
  if ((flags & G_LOG_LEVEL_ERROR) == G_LOG_LEVEL_ERROR) 
    level = PURPLE_DEBUG_ERROR;
  else if ((flags & G_LOG_LEVEL_CRITICAL) == G_LOG_LEVEL_CRITICAL) 
    level = PURPLE_DEBUG_FATAL;
  else if ((flags & G_LOG_LEVEL_WARNING) == G_LOG_LEVEL_WARNING) 
    level = PURPLE_DEBUG_WARNING;
  else if ((flags & G_LOG_LEVEL_MESSAGE) == G_LOG_LEVEL_MESSAGE) 
    level = PURPLE_DEBUG_INFO;
  else if ((flags & G_LOG_LEVEL_INFO) == G_LOG_LEVEL_INFO) 
    level = PURPLE_DEBUG_INFO;
  else if ((flags & G_LOG_LEVEL_DEBUG) == G_LOG_LEVEL_DEBUG) 
    level = PURPLE_DEBUG_MISC;
  else {
    purple_debug_warning("gntdebug","Unknown glib logging level in %d\n",flags);
/* This will never happen. */
    level = PURPLE_DEBUG_MISC;
  }
  if (msg != ((const gchar *)((void *)0))) 
    new_msg = purple_utf8_try_convert(msg);
  if (domain != ((const gchar *)((void *)0))) 
    new_domain = purple_utf8_try_convert(domain);
  if (new_msg != ((char *)((void *)0))) {
    purple_debug(level,((new_domain != ((char *)((void *)0)))?new_domain : "g_log"),"%s\n",new_msg);
    g_free(new_msg);
  }
  g_free(new_domain);
}

static void size_changed_cb(GntWidget *widget,int oldw,int oldh)
{
  int w;
  int h;
  gnt_widget_get_size(widget,&w,&h);
  purple_prefs_set_int("/finch/debug/size/width",w);
  purple_prefs_set_int("/finch/debug/size/height",h);
}

static gboolean for_real(gpointer entry)
{
  purple_prefs_set_string("/finch/debug/filter",gnt_entry_get_text(entry));
  return 0;
}

static void update_filter_string(GntEntry *entry,gpointer null)
{
  int id = (g_timeout_add(1000,for_real,entry));
  g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"update-filter",((gpointer )((glong )id)),((GDestroyNotify )g_source_remove));
}

static void file_save(GntFileSel *fs,const char *path,const char *file,GntTextView *tv)
{
  FILE *fp;
  if ((fp = fopen(path,"w+")) == ((FILE *)((void *)0))) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Unable to open file."))),0,0,0);
    return ;
  }
  fprintf(fp,"Finch Debug Log : %s\n",purple_date_format_full(0));
  fprintf(fp,"%s",( *(tv -> string)).str);
  fclose(fp);
  gnt_widget_destroy(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)fs),gnt_widget_get_gtype()))));
}

static void file_cancel(GntWidget *w,GntFileSel *fs)
{
  gnt_widget_destroy(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)fs),gnt_widget_get_gtype()))));
}

static void save_debug_win(GntWidget *w,GntTextView *tv)
{
  GntWidget *window = gnt_file_sel_new();
  GntFileSel *sel = (GntFileSel *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_file_sel_get_gtype()));
  gnt_file_sel_set_current_location(sel,purple_home_dir());
  gnt_file_sel_set_suggested_filename(sel,"debug.txt");
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)sel),((GType )(20 << 2))))),"file_selected",((GCallback )file_save),tv,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> cancel)),((GType )(20 << 2))))),"activate",((GCallback )file_cancel),sel,0,((GConnectFlags )0));
  gnt_widget_show(window);
}

void finch_debug_window_show()
{
  GntWidget *wid;
  GntWidget *box;
  GntWidget *label;
  debug.paused = 0;
  if (debug.window != 0) {
    gnt_window_present(debug.window);
    return ;
  }
  debug.window = gnt_box_new(0,1);
  gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)debug.window),gnt_box_get_gtype()))),1);
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)debug.window),gnt_box_get_gtype()))),((const char *)(dgettext("pidgin","Debug Window"))));
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)debug.window),gnt_box_get_gtype()))),0);
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)debug.window),gnt_box_get_gtype()))),GNT_ALIGN_MID);
  debug.tview = gnt_text_view_new();
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)debug.window),gnt_box_get_gtype()))),debug.tview);
  gnt_widget_set_size(debug.tview,purple_prefs_get_int("/finch/debug/size/width"),purple_prefs_get_int("/finch/debug/size/height"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)debug.tview),((GType )(20 << 2))))),"size_changed",((GCallback )size_changed_cb),0,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)debug.window),gnt_box_get_gtype()))),gnt_line_new(0));
  box = gnt_box_new(0,0);
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),GNT_ALIGN_MID);
  gnt_box_set_fill(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),0);
/* XXX: Setting the GROW_Y for the following widgets don't make sense. But right now
	 * it's necessary to make the width of the debug window resizable ... like I said,
	 * it doesn't make sense. The bug is likely in the packing in gntbox.c.
	 */
  wid = gnt_button_new(((const char *)(dgettext("pidgin","Clear"))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)wid),((GType )(20 << 2))))),"activate",((GCallback )clear_debug_win),debug.tview,0,((GConnectFlags )0));
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)wid),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_GROW_Y;
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),wid);
  wid = gnt_button_new(((const char *)(dgettext("pidgin","Save"))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)wid),((GType )(20 << 2))))),"activate",((GCallback )save_debug_win),debug.tview,0,((GConnectFlags )0));
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)wid),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_GROW_Y;
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),wid);
  debug.search = gnt_entry_new(purple_prefs_get_string("/finch/debug/filter"));
  label = gnt_label_new(((const char *)(dgettext("pidgin","Filter:"))));
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)label),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_GROW_X);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),label);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),debug.search);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)debug.search),((GType )(20 << 2))))),"text_changed",((GCallback )update_filter_string),0,0,((GConnectFlags )0));
  wid = gnt_check_box_new(((const char *)(dgettext("pidgin","Pause"))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)wid),((GType )(20 << 2))))),"toggled",((GCallback )toggle_pause),0,0,((GConnectFlags )0));
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)wid),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_GROW_Y;
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),wid);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)debug.window),gnt_box_get_gtype()))),box);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_GROW_Y;
  gnt_widget_set_name(debug.window,"debug-window");
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)debug.window),((GType )(20 << 2))))),"destroy",((GCallback )reset_debug_win),0,0,((GConnectFlags )0));
  gnt_text_view_attach_scroll_widget(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)debug.tview),gnt_text_view_get_gtype()))),debug.window);
  gnt_text_view_attach_pager_widget(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)debug.tview),gnt_text_view_get_gtype()))),debug.window);
  gnt_widget_show(debug.window);
}

static gboolean start_with_debugwin(gpointer null)
{
  finch_debug_window_show();
  return 0;
}

void finch_debug_init()
{
/* Xerox */
#define REGISTER_G_LOG_HANDLER(name) \
	g_log_set_handler((name), G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL \
					  | G_LOG_FLAG_RECURSION, \
					  purple_glib_log_handler, NULL)
/* Register the glib log handlers. */
  g_log_set_handler(0,(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION),purple_glib_log_handler,0);
  g_log_set_handler("GLib",(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION),purple_glib_log_handler,0);
  g_log_set_handler("GModule",(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION),purple_glib_log_handler,0);
  g_log_set_handler("GLib-GObject",(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION),purple_glib_log_handler,0);
  g_log_set_handler("GThread",(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION),purple_glib_log_handler,0);
  g_log_set_handler("Gnt",(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION),purple_glib_log_handler,0);
#ifdef USE_GSTREAMER
  g_log_set_handler("GStreamer",(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION),purple_glib_log_handler,0);
#endif
  g_log_set_handler("stderr",(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION),purple_glib_log_handler,0);
/* Redirect the debug messages to stderr */
  g_set_print_handler(print_stderr);
  if (!(purple_debug_is_enabled() != 0)) 
    handle_fprintf_stderr(0);
  purple_prefs_add_none("/finch/debug");
  purple_prefs_add_string("/finch/debug/filter","");
  purple_prefs_add_none("/finch/debug/size");
  purple_prefs_add_int("/finch/debug/size/width",60);
  purple_prefs_add_int("/finch/debug/size/height",15);
  if (purple_debug_is_enabled() != 0) 
    g_timeout_add(0,start_with_debugwin,0);
}

void finch_debug_uninit()
{
  handle_fprintf_stderr(1);
}
