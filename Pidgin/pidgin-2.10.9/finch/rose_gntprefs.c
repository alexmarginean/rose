/**
 * @file gntprefs.c GNT Preferences API
 * @ingroup finch
 */
/* finch
 *
 * Finch is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "finch.h"
#include <internal.h>
#include <prefs.h>
#include <savedstatuses.h>
#include "gntprefs.h"
#include "gntrequest.h"
#include "gnt.h"
#include "gntwidget.h"
#include <string.h>
static struct __unnamed_class___F0_L40_C8_unknown_scope_and_name_variable_declaration__variable_type___Pb__GList_GList__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__freestrings__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L10R_variable_name_unknown_scope_and_name__scope__showing__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window {
/* strings to be freed when the pref-window is closed */
GList *freestrings;
gboolean showing;
GntWidget *window;}pref_request;

void finch_prefs_init()
{
  purple_prefs_add_none("/finch");
  purple_prefs_add_none("/finch/plugins");
  purple_prefs_add_path_list("/finch/plugins/loaded",0);
  purple_prefs_add_path_list("/finch/plugins/seen",0);
  purple_prefs_add_none("/finch/conversations");
  purple_prefs_add_bool("/finch/conversations/timestamps",1);
  purple_prefs_add_bool("/finch/conversations/notify_typing",0);
  purple_prefs_add_none("/finch/filelocations");
  purple_prefs_add_path("/finch/filelocations/last_save_folder","");
  purple_prefs_add_path("/finch/filelocations/last_save_folder","");
}

void finch_prefs_update_old()
{
  const char *str = (const char *)((void *)0);
  purple_prefs_rename("/gaim/gnt","/finch");
  purple_prefs_rename("/purple/gnt","/finch");
  if (((str = purple_prefs_get_string("/purple/away/idle_reporting")) != 0) && (strcmp(str,"gaim") == 0)) 
    purple_prefs_set_string("/purple/away/idle_reporting","purple");
}
typedef struct __unnamed_class___F0_L75_C9_unknown_scope_and_name_variable_declaration__variable_type_L273R_variable_name_unknown_scope_and_name__scope__type__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__pref__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__label__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb___Fb___Pb__GList_GList__typedef_declaration__Pe___Gb__Fe___Pe___variable_name_unknown_scope_and_name__scope__lv {
PurplePrefType type;
const char *pref;
const char *label;
/* If the value is to be selected from a number of choices */
GList *(*lv)();}Prefs;

static GList *get_log_options()
{
  return purple_log_logger_get_options();
}

static GList *get_idle_options()
{
  GList *list = (GList *)((void *)0);
  list = g_list_append(list,((char *)((const char *)(dgettext("pidgin","Based on keyboard use")))));
  list = g_list_append(list,"system");
  list = g_list_append(list,((char *)((const char *)(dgettext("pidgin","From last sent message")))));
  list = g_list_append(list,"purple");
  list = g_list_append(list,((char *)((const char *)(dgettext("pidgin","Never")))));
  list = g_list_append(list,"never");
  return list;
}

static GList *get_status_titles()
{
  GList *list = (GList *)((void *)0);
  GList *iter;
  for (iter = purple_savedstatuses_get_all(); iter != 0; iter = (iter -> next)) {{
      char *str;
      if (purple_savedstatus_is_transient((iter -> data)) != 0) 
        continue; 
      str = g_strdup_printf("%ld",purple_savedstatus_get_creation_time((iter -> data)));
      list = g_list_append(list,((char *)(purple_savedstatus_get_title((iter -> data)))));
      list = g_list_append(list,str);
      pref_request.freestrings = g_list_prepend(pref_request.freestrings,str);
    }
  }
  return list;
}

static PurpleRequestField *get_pref_field(Prefs *prefs)
{
  PurpleRequestField *field = (PurpleRequestField *)((void *)0);
  if ((prefs -> lv) == ((GList *(*)())((void *)0))) {
    switch(prefs -> type){
      case PURPLE_PREF_BOOLEAN:
{
        field = purple_request_field_bool_new((prefs -> pref),((const char *)(dgettext("pidgin",(prefs -> label)))),purple_prefs_get_bool((prefs -> pref)));
        break; 
      }
      case PURPLE_PREF_INT:
{
        field = purple_request_field_int_new((prefs -> pref),((const char *)(dgettext("pidgin",(prefs -> label)))),purple_prefs_get_int((prefs -> pref)));
        break; 
      }
      case PURPLE_PREF_STRING:
{
        field = purple_request_field_string_new((prefs -> pref),((const char *)(dgettext("pidgin",(prefs -> label)))),purple_prefs_get_string((prefs -> pref)),0);
        break; 
      }
      default:
{
        break; 
      }
    }
  }
  else {
    GList *list = ( *(prefs -> lv))();
    GList *iter;
    if (list != 0) 
      field = purple_request_field_list_new((prefs -> pref),((const char *)(dgettext("pidgin",(prefs -> label)))));
    for (iter = list; iter != 0; iter = (iter -> next)) {
      gboolean select = 0;
      const char *data = (iter -> data);
      int idata;
      iter = (iter -> next);
      switch(prefs -> type){
        case PURPLE_PREF_BOOLEAN:
{
          sscanf((iter -> data),"%d",&idata);
          if (purple_prefs_get_bool((prefs -> pref)) == idata) 
            select = 1;
          break; 
        }
        case PURPLE_PREF_INT:
{
          sscanf((iter -> data),"%d",&idata);
          if (purple_prefs_get_int((prefs -> pref)) == idata) 
            select = 1;
          break; 
        }
        case PURPLE_PREF_STRING:
{
          if (strcmp(purple_prefs_get_string((prefs -> pref)),(iter -> data)) == 0) 
            select = 1;
          break; 
        }
        default:
{
          break; 
        }
      }
      purple_request_field_list_add_icon(field,data,0,(iter -> data));
      if (select != 0) 
        purple_request_field_list_add_selected(field,data);
    }
    g_list_free(list);
  }
  return field;
}
static Prefs blist[] = {{(PURPLE_PREF_BOOLEAN), ("/finch/blist/idletime"), ("Show Idle Time"), ((GList *(*)())((void *)0))}, {(PURPLE_PREF_BOOLEAN), ("/finch/blist/showoffline"), ("Show Offline Buddies"), ((GList *(*)())((void *)0))}, {(PURPLE_PREF_NONE), ((const char *)((void *)0)), ((const char *)((void *)0)), ((GList *(*)())((void *)0))}};
static Prefs convs[] = {{(PURPLE_PREF_BOOLEAN), ("/finch/conversations/timestamps"), ("Show Timestamps"), ((GList *(*)())((void *)0))}, {(PURPLE_PREF_BOOLEAN), ("/finch/conversations/notify_typing"), ("Notify buddies when you are typing"), ((GList *(*)())((void *)0))}, {(PURPLE_PREF_NONE), ((const char *)((void *)0)), ((const char *)((void *)0)), ((GList *(*)())((void *)0))}};
static Prefs logging[] = {{(PURPLE_PREF_STRING), ("/purple/logging/format"), ("Log format"), (get_log_options)}, {(PURPLE_PREF_BOOLEAN), ("/purple/logging/log_ims"), ("Log IMs"), ((GList *(*)())((void *)0))}, {(PURPLE_PREF_BOOLEAN), ("/purple/logging/log_chats"), ("Log chats"), ((GList *(*)())((void *)0))}, {(PURPLE_PREF_BOOLEAN), ("/purple/logging/log_system"), ("Log status change events"), ((GList *(*)())((void *)0))}, {(PURPLE_PREF_NONE), ((const char *)((void *)0)), ((const char *)((void *)0)), ((GList *(*)())((void *)0))}};
static Prefs idle[] = {{(PURPLE_PREF_STRING), ("/purple/away/idle_reporting"), ("Report Idle time"), (get_idle_options)}, {(PURPLE_PREF_BOOLEAN), ("/purple/away/away_when_idle"), ("Change status when idle"), ((GList *(*)())((void *)0))}, {(PURPLE_PREF_INT), ("/purple/away/mins_before_away"), ("Minutes before changing status"), ((GList *(*)())((void *)0))}, {(PURPLE_PREF_INT), ("/purple/savedstatus/idleaway"), ("Change status to"), (get_status_titles)}, {(PURPLE_PREF_NONE), ((const char *)((void *)0)), ((const char *)((void *)0)), ((GList *(*)())((void *)0))}};

static void free_strings()
{
  g_list_foreach(pref_request.freestrings,((GFunc )g_free),0);
  g_list_free(pref_request.freestrings);
  pref_request.freestrings = ((GList *)((void *)0));
  pref_request.showing = 0;
}

static void save_cb(void *data,PurpleRequestFields *allfields)
{
  finch_request_save_in_prefs(data,allfields);
  free_strings();
}

static void add_pref_group(PurpleRequestFields *fields,const char *title,Prefs *prefs)
{
  PurpleRequestField *field;
  PurpleRequestFieldGroup *group;
  int i;
  group = purple_request_field_group_new(title);
  purple_request_fields_add_group(fields,group);
  for (i = 0; prefs[i].pref != 0; i++) {
    field = get_pref_field((prefs + i));
    if (field != 0) 
      purple_request_field_group_add_field(group,field);
  }
}

void finch_prefs_show_all()
{
  PurpleRequestFields *fields;
  if (pref_request.showing != 0) {
    gnt_window_present(pref_request.window);
    return ;
  }
  fields = purple_request_fields_new();
  add_pref_group(fields,((const char *)(dgettext("pidgin","Buddy List"))),blist);
  add_pref_group(fields,((const char *)(dgettext("pidgin","Conversations"))),convs);
  add_pref_group(fields,((const char *)(dgettext("pidgin","Logging"))),logging);
  add_pref_group(fields,((const char *)(dgettext("pidgin","Idle"))),idle);
  pref_request.showing = 1;
  pref_request.window = (purple_request_fields(0,((const char *)(dgettext("pidgin","Preferences"))),0,0,fields,((const char *)(dgettext("pidgin","Save"))),((GCallback )save_cb),((const char *)(dgettext("pidgin","Cancel"))),free_strings,0,0,0,0));
}
