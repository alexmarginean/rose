/**
 * @file gntplugin.c GNT Plugins API
 * @ingroup finch
 */
/* finch
 *
 * Finch is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <internal.h>
#include <gnt.h>
#include <gntbox.h>
#include <gntbutton.h>
#include <gntlabel.h>
#include <gntline.h>
#include <gnttree.h>
#include <gntutils.h>
#include "finch.h"
#include "debug.h"
#include "notify.h"
#include "request.h"
#include "gntplugin.h"
#include "gntrequest.h"
static struct __unnamed_class___F0_L45_C8_unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__tree__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__aboot__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__conf {
GntWidget *tree;
GntWidget *window;
GntWidget *aboot;
GntWidget *conf;}plugins;
static GHashTable *confwins;
static GntWidget *process_pref_frame(PurplePluginPrefFrame *frame);

static void free_stringlist(GList *list)
{
  g_list_foreach(list,((GFunc )g_free),0);
  g_list_free(list);
}

static void decide_conf_button(PurplePlugin *plugin)
{
  if ((purple_plugin_is_loaded(plugin) != 0) && ((((((plugin -> info) != ((PurplePluginInfo *)((void *)0))) && (( *(plugin -> info)).ui_info != ((void *)((void *)0)))) && !(strcmp(( *(plugin -> info)).ui_requirement,"gnt-purple") != 0)) && (((FinchPluginFrame )( *(plugin -> info)).ui_info) != ((GntWidget *(*)())((void *)0)))) || ((( *(plugin -> info)).prefs_info != 0) && (( *( *(plugin -> info)).prefs_info).get_plugin_pref_frame != 0)))) 
    gnt_widget_set_visible(plugins.conf,1);
  else 
    gnt_widget_set_visible(plugins.conf,0);
  gnt_box_readjust(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)plugins.window),gnt_box_get_gtype()))));
  gnt_widget_draw(plugins.window);
}

static void plugin_toggled_cb(GntWidget *tree,PurplePlugin *plugin,gpointer null)
{
  if (gnt_tree_get_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),plugin) != 0) {
    if (!(purple_plugin_load(plugin) != 0)) {
      purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","ERROR"))),((const char *)(dgettext("pidgin","loading plugin failed"))),0,0,0);
      gnt_tree_set_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),plugin,0);
    }
  }
  else {
    GntWidget *win;
    if (!(purple_plugin_unload(plugin) != 0)) {
      purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","ERROR"))),((const char *)(dgettext("pidgin","unloading plugin failed"))),0,0,0);
      purple_plugin_disable(plugin);
      gnt_tree_set_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),plugin,1);
    }
    if ((confwins != 0) && ((win = (g_hash_table_lookup(confwins,plugin))) != ((GntWidget *)((void *)0)))) {
      gnt_widget_destroy(win);
    }
  }
  decide_conf_button(plugin);
  finch_plugins_save_loaded();
}
/* Xerox */

void finch_plugins_save_loaded()
{
  purple_plugins_save_loaded("/finch/plugins/loaded");
}

static void selection_changed(GntWidget *widget,gpointer old,gpointer current,gpointer null)
{
  PurplePlugin *plugin = current;
  char *text;
  GList *list = (GList *)((void *)0);
  GList *iter = (GList *)((void *)0);
  if (!(plugin != 0)) 
    return ;
/* If the selected plugin was unseen before, mark it as seen. But save the list
	 * only when the plugin list is closed. So if the user enables a plugin, and it
	 * crashes, it won't get marked as seen so the user can fix the bug and still
	 * quickly find the plugin in the list.
	 * I probably mean 'plugin developers' by 'users' here. */
  list = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"seen-list"));
  if (list != 0) 
    iter = g_list_find_custom(list,(plugin -> path),((GCompareFunc )strcmp));
  if (!(iter != 0)) {
    list = g_list_prepend(list,(g_strdup((plugin -> path))));
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"seen-list",list);
  }
/* XXX: Use formatting and stuff */
  gnt_text_view_clear(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)plugins.aboot),gnt_text_view_get_gtype()))));
  text = g_strdup_printf(((const char *)(dgettext("pidgin","Name: %s\nVersion: %s\nDescription: %s\nAuthor: %s\nWebsite: %s\nFilename: %s\n"))),((((const char *)(dgettext("pidgin",( *(plugin -> info)).name))) != 0)?((const char *)(dgettext("pidgin",( *(plugin -> info)).name))) : ""),((((const char *)(dgettext("pidgin",( *(plugin -> info)).version))) != 0)?((const char *)(dgettext("pidgin",( *(plugin -> info)).version))) : ""),((((const char *)(dgettext("pidgin",( *(plugin -> info)).description))) != 0)?((const char *)(dgettext("pidgin",( *(plugin -> info)).description))) : ""),((((const char *)(dgettext("pidgin",( *(plugin -> info)).author))) != 0)?((const char *)(dgettext("pidgin",( *(plugin -> info)).author))) : ""),((((const char *)(dgettext("pidgin",( *(plugin -> info)).homepage))) != 0)?((const char *)(dgettext("pidgin",( *(plugin -> info)).homepage))) : ""),(((plugin -> path) != 0)?(plugin -> path) : ""));
  gnt_text_view_append_text_with_flags(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)plugins.aboot),gnt_text_view_get_gtype()))),text,GNT_TEXT_FLAG_NORMAL);
  gnt_text_view_scroll(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)plugins.aboot),gnt_text_view_get_gtype()))),0);
  g_free(text);
  decide_conf_button(plugin);
}

static void reset_plugin_window(GntWidget *window,gpointer null)
{
  GList *list = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)plugins.tree),((GType )(20 << 2))))),"seen-list"));
  purple_prefs_set_path_list("/finch/plugins/seen",list);
  g_list_foreach(list,((GFunc )g_free),0);
  g_list_free(list);
  plugins.window = ((GntWidget *)((void *)0));
  plugins.tree = ((GntWidget *)((void *)0));
  plugins.aboot = ((GntWidget *)((void *)0));
}

static int plugin_compare(PurplePlugin *p1,PurplePlugin *p2)
{
  char *s1 = g_utf8_strup(( *(p1 -> info)).name,(-1));
  char *s2 = g_utf8_strup(( *(p2 -> info)).name,(-1));
  int ret = g_utf8_collate(s1,s2);
  g_free(s1);
  g_free(s2);
  return ret;
}

static void confwin_init()
{
  confwins = g_hash_table_new(g_direct_hash,g_direct_equal);
}

static void remove_confwin(GntWidget *window,gpointer plugin)
{
  g_hash_table_remove(confwins,plugin);
}

static void configure_plugin_cb(GntWidget *button,gpointer null)
{
  PurplePlugin *plugin;
  FinchPluginFrame callback;
  do {
    if (plugins.tree != ((GntWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugins.tree != NULL");
      return ;
    };
  }while (0);
  plugin = (gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)plugins.tree),gnt_tree_get_gtype())))));
  if (!(purple_plugin_is_loaded(plugin) != 0)) {
    purple_notify_message(plugin,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Error"))),((const char *)(dgettext("pidgin","Plugin need to be loaded before you can configure it."))),0,0,0);
    return ;
  }
  if ((confwins != 0) && (g_hash_table_lookup(confwins,plugin) != 0)) 
    return ;
  if (((((plugin -> info) != ((PurplePluginInfo *)((void *)0))) && (( *(plugin -> info)).ui_info != ((void *)((void *)0)))) && !(strcmp(( *(plugin -> info)).ui_requirement,"gnt-purple") != 0)) && ((callback = ((FinchPluginFrame )( *(plugin -> info)).ui_info)) != ((GntWidget *(*)())((void *)0)))) {
    GntWidget *window = gnt_box_new(0,1);
    GntWidget *box;
    GntWidget *button;
    gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),1);
    gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),( *(plugin -> info)).name);
    gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),GNT_ALIGN_MID);
    box = ( *callback)();
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),box);
    box = gnt_box_new(0,0);
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),box);
    button = gnt_button_new(((const char *)(dgettext("pidgin","Close"))));
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )gnt_widget_destroy),window,0,G_CONNECT_SWAPPED);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"destroy",((GCallback )remove_confwin),plugin,0,((GConnectFlags )0));
    gnt_widget_show(window);
    if (confwins == ((GHashTable *)((void *)0))) 
      confwin_init();
    g_hash_table_insert(confwins,plugin,window);
  }
  else if ((( *(plugin -> info)).prefs_info != 0) && (( *( *(plugin -> info)).prefs_info).get_plugin_pref_frame != 0)) {
    GntWidget *win = process_pref_frame(( *( *( *(plugin -> info)).prefs_info).get_plugin_pref_frame)(plugin));
    if (confwins == ((GHashTable *)((void *)0))) 
      confwin_init();
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"destroy",((GCallback )remove_confwin),plugin,0,((GConnectFlags )0));
    g_hash_table_insert(confwins,plugin,win);
    return ;
  }
  else {
    purple_notify_message(plugin,PURPLE_NOTIFY_MSG_INFO,((const char *)(dgettext("pidgin","Error"))),((const char *)(dgettext("pidgin","No configuration options for this plugin."))),0,0,0);
    return ;
  }
}

static void install_selected_file_cb(gpointer handle,const char *filename)
{
/* Try to init the selected file.
	 * If it succeeds, try to make a copy of the file in $USERDIR/plugins/.
	 * If the copy succeeds, unload and destroy the plugin in the original
	 *  location and init+load the new one.
	 * Select the plugin in the plugin list.
	 */
  char *path;
  PurplePlugin *plugin;
  do {
    if (plugins.window != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugins.window");
      return ;
    };
  }while (0);
  plugin = purple_plugin_probe(filename);
  if (!(plugin != 0)) {
    purple_notify_message(handle,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Error loading plugin"))),((const char *)(dgettext("pidgin","The selected file is not a valid plugin."))),((const char *)(dgettext("pidgin","Please open the debug window and try again to see the exact error message."))),0,0);
    return ;
  }
  if (g_list_find(gnt_tree_get_rows(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)plugins.tree),gnt_tree_get_gtype())))),plugin) != 0) {
    purple_plugin_load(plugin);
    gnt_tree_set_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)plugins.tree),gnt_tree_get_gtype()))),plugin,purple_plugin_is_loaded(plugin));
    gnt_tree_set_selected(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)plugins.tree),gnt_tree_get_gtype()))),plugin);
    return ;
  }
  path = g_build_filename(purple_user_dir(),"plugins",((void *)((void *)0)));
  if (purple_build_dir(path,256 | 128 | 64) == 0) {
    char *content = (char *)((void *)0);
    gsize length = 0;
    if (g_file_get_contents(filename,&content,&length,0) != 0) {
      char *file = g_path_get_basename(filename);
      g_free(path);
      path = g_build_filename(purple_user_dir(),"plugins",file,((void *)((void *)0)));
      if (purple_util_write_data_to_file_absolute(path,content,length) != 0) {
        purple_plugin_destroy(plugin);
        plugin = purple_plugin_probe(path);
        if (!(plugin != 0)) {
          purple_debug_warning("gntplugin","This is really strange. %s can be loaded, but %s can\'t!\n",filename,path);
          g_unlink(path);
          plugin = purple_plugin_probe(filename);
        }
      }
      else {
      }
    }
    g_free(content);
  }
  g_free(path);
  purple_plugin_load(plugin);
  if (( *(plugin -> info)).type == PURPLE_PLUGIN_LOADER) {
    GList *cur;
    for (cur = ( *((PurplePluginLoaderInfo *)( *(plugin -> info)).extra_info)).exts; cur != ((GList *)((void *)0)); cur = (cur -> next)) 
      purple_plugins_probe((cur -> data));
    return ;
  }
  if (((( *(plugin -> info)).type != PURPLE_PLUGIN_STANDARD) || ((( *(plugin -> info)).flags & 1) != 0UL)) || ((plugin -> error) != 0)) 
    return ;
  gnt_tree_add_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)plugins.tree),gnt_tree_get_gtype()))),plugin,gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)plugins.tree),gnt_tree_get_gtype()))),( *(plugin -> info)).name),0,0);
  gnt_tree_set_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)plugins.tree),gnt_tree_get_gtype()))),plugin,purple_plugin_is_loaded(plugin));
  gnt_tree_set_row_flags(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)plugins.tree),gnt_tree_get_gtype()))),plugin,GNT_TEXT_FLAG_BOLD);
  gnt_tree_set_selected(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)plugins.tree),gnt_tree_get_gtype()))),plugin);
}

static void install_plugin_cb(GntWidget *w,gpointer null)
{
  static int handle;
  purple_request_close_with_handle((&handle));
  purple_request_file((&handle),((const char *)(dgettext("pidgin","Select plugin to install"))),0,0,((GCallback )install_selected_file_cb),0,0,0,0,(&handle));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)w),((GType )(20 << 2))))),"destroy",((GCallback )purple_request_close_with_handle),(&handle),0,G_CONNECT_SWAPPED);
}

void finch_plugins_show_all()
{
  GntWidget *window;
  GntWidget *tree;
  GntWidget *box;
  GntWidget *aboot;
  GntWidget *button;
  GList *iter;
  GList *seen;
  if (plugins.window != 0) {
    gnt_window_present(plugins.window);
    return ;
  }
  purple_plugins_probe("so");
  plugins.window = (window = gnt_box_new(0,1));
  gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),1);
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),((const char *)(dgettext("pidgin","Plugins"))));
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),0);
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),GNT_ALIGN_MID);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_label_new(((const char *)(dgettext("pidgin","You can (un)load plugins from the following list.")))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_line_new(0));
  box = gnt_box_new(0,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),box);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_line_new(0));
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),0);
  plugins.tree = (tree = gnt_tree_new());
  gnt_tree_set_compare_func(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),((GCompareFunc )plugin_compare));
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_NO_BORDER;
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),tree);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),gnt_line_new(1));
  plugins.aboot = (aboot = gnt_text_view_new());
  gnt_text_view_set_flag(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)aboot),gnt_text_view_get_gtype()))),GNT_TEXT_VIEW_TOP_ALIGN);
  gnt_widget_set_size(aboot,40,20);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),aboot);
  seen = purple_prefs_get_path_list("/finch/plugins/seen");
  for (iter = purple_plugins_get_all(); iter != 0; iter = (iter -> next)) {{
      PurplePlugin *plug = (iter -> data);
      if (( *(plug -> info)).type == PURPLE_PLUGIN_LOADER) {
        GList *cur;
        for (cur = ( *((PurplePluginLoaderInfo *)( *(plug -> info)).extra_info)).exts; cur != ((GList *)((void *)0)); cur = (cur -> next)) 
          purple_plugins_probe((cur -> data));
        continue; 
      }
      if (((( *(plug -> info)).type != PURPLE_PLUGIN_STANDARD) || ((( *(plug -> info)).flags & 1) != 0UL)) || ((plug -> error) != 0)) 
        continue; 
      gnt_tree_add_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),plug,gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),( *(plug -> info)).name),0,0);
      gnt_tree_set_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),plug,purple_plugin_is_loaded(plug));
      if (!(g_list_find_custom(seen,(plug -> path),((GCompareFunc )strcmp)) != 0)) 
        gnt_tree_set_row_flags(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),plug,GNT_TEXT_FLAG_BOLD);
    }
  }
  gnt_tree_set_col_width(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),0,30);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"toggled",((GCallback )plugin_toggled_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"selection_changed",((GCallback )selection_changed),0,0,((GConnectFlags )0));
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"seen-list",seen);
  box = gnt_box_new(0,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),box);
  button = gnt_button_new(((const char *)(dgettext("pidgin","Install Plugin..."))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  gnt_util_set_trigger_widget(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype()))),((cur_term -> type.Strings[77] != 0)?cur_term -> type.Strings[77] : ""),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )install_plugin_cb),0,0,((GConnectFlags )0));
  button = gnt_button_new(((const char *)(dgettext("pidgin","Close"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )gnt_widget_destroy),window,0,G_CONNECT_SWAPPED);
  plugins.conf = (button = gnt_button_new(((const char *)(dgettext("pidgin","Configure Plugin")))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )configure_plugin_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"destroy",((GCallback )reset_plugin_window),0,0,((GConnectFlags )0));
  gnt_widget_show(window);
  decide_conf_button((gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))))));
}

static GntWidget *process_pref_frame(PurplePluginPrefFrame *frame)
{
  PurpleRequestField *field;
  PurpleRequestFields *fields;
  PurpleRequestFieldGroup *group = (PurpleRequestFieldGroup *)((void *)0);
  GList *prefs;
  GList *stringlist = (GList *)((void *)0);
  GntWidget *ret = (GntWidget *)((void *)0);
  fields = purple_request_fields_new();
  for (prefs = purple_plugin_pref_frame_get_prefs(frame); prefs != 0; prefs = (prefs -> next)) {{
      PurplePluginPref *pref = (prefs -> data);
      PurplePrefType type;
      const char *name = purple_plugin_pref_get_name(pref);
      const char *label = purple_plugin_pref_get_label(pref);
      if (name == ((const char *)((void *)0))) {
        if (label == ((const char *)((void *)0))) 
          continue; 
        if ((purple_plugin_pref_get_type(pref)) == PURPLE_PLUGIN_PREF_INFO) {
          field = purple_request_field_label_new("*",purple_plugin_pref_get_label(pref));
          purple_request_field_group_add_field(group,field);
        }
        else {
          group = purple_request_field_group_new(label);
          purple_request_fields_add_group(fields,group);
        }
        continue; 
      }
      field = ((PurpleRequestField *)((void *)0));
      type = purple_prefs_get_type(name);
      if ((purple_plugin_pref_get_type(pref)) == PURPLE_PLUGIN_PREF_CHOICE) {
        GList *list = purple_plugin_pref_get_choices(pref);
        gpointer current_value = (gpointer )((void *)0);
        switch(type){
          case PURPLE_PREF_BOOLEAN:
{
            current_value = (g_strdup_printf("%d",purple_prefs_get_bool(name)));
            break; 
          }
          case PURPLE_PREF_INT:
{
            current_value = (g_strdup_printf("%d",purple_prefs_get_int(name)));
            break; 
          }
          case PURPLE_PREF_STRING:
{
            current_value = (g_strdup(purple_prefs_get_string(name)));
            break; 
          }
          default:
{
            continue; 
          }
        }
        field = purple_request_field_list_new(name,label);
        purple_request_field_list_set_multi_select(field,0);
        while((list != 0) && ((list -> next) != 0)){
          const char *label = (list -> data);
          char *value = (char *)((void *)0);
          switch(type){
            case PURPLE_PREF_BOOLEAN:
{
              value = g_strdup_printf("%d",((gint )((glong )( *(list -> next)).data)));
              break; 
            }
            case PURPLE_PREF_INT:
{
              value = g_strdup_printf("%d",((gint )((glong )( *(list -> next)).data)));
              break; 
            }
            case PURPLE_PREF_STRING:
{
              value = g_strdup(( *(list -> next)).data);
              break; 
            }
            default:
{
              break; 
            }
          }
          stringlist = g_list_prepend(stringlist,value);
          purple_request_field_list_add_icon(field,label,0,value);
          if (strcmp(value,current_value) == 0) 
            purple_request_field_list_add_selected(field,label);
          list = ( *(list -> next)).next;
        }
        g_free(current_value);
      }
      else {
        switch(type){
          case PURPLE_PREF_BOOLEAN:
{
            field = purple_request_field_bool_new(name,label,purple_prefs_get_bool(name));
            break; 
          }
          case PURPLE_PREF_INT:
{
            field = purple_request_field_int_new(name,label,purple_prefs_get_int(name));
            break; 
          }
          case PURPLE_PREF_STRING:
{
            field = purple_request_field_string_new(name,label,purple_prefs_get_string(name),((purple_plugin_pref_get_format_type(pref)) & PURPLE_STRING_FORMAT_TYPE_MULTILINE));
            break; 
          }
          default:
{
            break; 
          }
        }
      }
      if (field != 0) {
        if (group == ((PurpleRequestFieldGroup *)((void *)0))) {
          group = purple_request_field_group_new(((const char *)(dgettext("pidgin","Preferences"))));
          purple_request_fields_add_group(fields,group);
        }
        purple_request_field_group_add_field(group,field);
      }
    }
  }
  ret = (purple_request_fields(0,((const char *)(dgettext("pidgin","Preferences"))),0,0,fields,((const char *)(dgettext("pidgin","Save"))),((GCallback )finch_request_save_in_prefs),((const char *)(dgettext("pidgin","Cancel"))),0,0,0,0,0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ret),((GType )(20 << 2))))),"destroy",((GCallback )free_stringlist),stringlist,0,G_CONNECT_SWAPPED);
  return ret;
}
