/**
 * @file gntrequest.c GNT Request API
 * @ingroup finch
 */
/* finch
 *
 * Finch is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <internal.h>
#include <gnt.h>
#include <gntbox.h>
#include <gntbutton.h>
#include <gntcheckbox.h>
#include <gntcombobox.h>
#include <gntentry.h>
#include <gntfilesel.h>
#include <gntlabel.h>
#include <gntline.h>
#include <gnttree.h>
#include "finch.h"
#include "gntrequest.h"
#include "debug.h"
#include "util.h"
/* XXX: Until gobjectification ... */
#undef FINCH_GET_DATA
#undef FINCH_SET_DATA
#define FINCH_GET_DATA(obj)  purple_request_field_get_ui_data(obj)
#define FINCH_SET_DATA(obj, data)  purple_request_field_set_ui_data(obj, data)
typedef struct __unnamed_class___F0_L50_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__v__Pe___variable_name_unknown_scope_and_name__scope__user_data__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__dialog__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L187R__Pe___variable_name_unknown_scope_and_name__scope__cbs__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__save {
void *user_data;
GntWidget *dialog;
GCallback *cbs;
gboolean save;}FinchFileRequest;

static GntWidget *setup_request_window(const char *title,const char *primary,const char *secondary,PurpleRequestType type)
{
  GntWidget *window;
  window = gnt_box_new(0,1);
  gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),1);
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),title);
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),GNT_ALIGN_MID);
  if (primary != 0) 
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_label_new_with_format(primary,GNT_TEXT_FLAG_BOLD));
  if (secondary != 0) 
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_label_new(secondary));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"destroy",((GCallback )purple_request_close),((gpointer )((glong )type)),0,G_CONNECT_SWAPPED);
  return window;
}
/**
 * If the window is closed by the wm (ie, without triggering any of
 * the buttons, then do some default callback.
 */

static void setup_default_callback(GntWidget *window,gpointer default_cb,gpointer data)
{
  if (default_cb == ((void *)((void *)0))) 
    return ;
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"default-callback",default_cb);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"destroy",((GCallback )default_cb),data,0,G_CONNECT_SWAPPED);
}

static void action_performed(GntWidget *button,gpointer data)
{
  g_signal_handlers_disconnect_matched(data,G_SIGNAL_MATCH_FUNC,0,0,0,g_object_get_data(data,"default-callback"),0);
}
/**
 * window: this is the window
 * userdata: the userdata to pass to the primary callbacks
 * cb: the callback
 * data: data for the callback
 * (text, primary-callback) pairs, ended by a NULL
 *
 * The cancellation callback should be the last callback sent.
 */

static GntWidget *setup_button_box(GntWidget *win,gpointer userdata,gpointer cb,gpointer data,... )
{
  GntWidget *box;
  GntWidget *button = (GntWidget *)((void *)0);
  va_list list;
  const char *text;
  gpointer callback;
  box = gnt_box_new(0,0);
  va_start(list,data);
  while((text = (va_arg(list,const char *))) != 0){
    callback = (va_arg(list,gpointer ));
    button = gnt_button_new(text);
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate-callback",callback);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate-userdata",userdata);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )action_performed),win,0,((GConnectFlags )0));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )cb),data,0,((GConnectFlags )0));
  }
  if (button != 0) 
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"cancellation-function",((gpointer )((gpointer )((glong )1))));
  va_end(list);
  return box;
}

static void notify_input_cb(GntWidget *button,GntWidget *entry)
{
  PurpleRequestInputCb callback = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate-callback"));
  gpointer data = g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate-userdata");
  const char *text = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_entry_get_gtype()))));
  if (callback != 0) 
    ( *callback)(data,text);
  while((button -> parent) != 0)
    button = (button -> parent);
  purple_request_close(PURPLE_REQUEST_INPUT,button);
}

static void *finch_request_input(const char *title,const char *primary,const char *secondary,const char *default_value,gboolean multiline,gboolean masked,gchar *hint,const char *ok_text,GCallback ok_cb,const char *cancel_text,GCallback cancel_cb,PurpleAccount *account,const char *who,PurpleConversation *conv,void *user_data)
{
  GntWidget *window;
  GntWidget *box;
  GntWidget *entry;
  window = setup_request_window(title,primary,secondary,PURPLE_REQUEST_INPUT);
  entry = gnt_entry_new(default_value);
  if (masked != 0) 
    gnt_entry_set_masked(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_entry_get_gtype()))),1);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),entry);
  box = setup_button_box(window,user_data,notify_input_cb,entry,ok_text,ok_cb,cancel_text,cancel_cb,((void *)((void *)0)));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),box);
  setup_default_callback(window,cancel_cb,user_data);
  gnt_widget_show(window);
  return window;
}

static void finch_close_request(PurpleRequestType type,gpointer ui_handle)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)ui_handle),gnt_widget_get_gtype()));
  if (type == PURPLE_REQUEST_FIELDS) {
    PurpleRequestFields *fields = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"fields"));
    purple_request_fields_destroy(fields);
  }
  while((widget -> parent) != 0)
    widget = (widget -> parent);
  gnt_widget_destroy(widget);
}

static void request_choice_cb(GntWidget *button,GntComboBox *combo)
{
  PurpleRequestChoiceCb callback = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate-callback"));
  gpointer data = g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate-userdata");
  int choice = (((gint )((glong )(gnt_combo_box_get_selected_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype()))))))) - 1);
  if (callback != 0) 
    ( *callback)(data,choice);
  while((button -> parent) != 0)
    button = (button -> parent);
  purple_request_close(PURPLE_REQUEST_INPUT,button);
}

static void *finch_request_choice(const char *title,const char *primary,const char *secondary,int default_value,const char *ok_text,GCallback ok_cb,const char *cancel_text,GCallback cancel_cb,PurpleAccount *account,const char *who,PurpleConversation *conv,void *user_data,va_list choices)
{
  GntWidget *window;
  GntWidget *combo;
  GntWidget *box;
  const char *text;
  int val;
  window = setup_request_window(title,primary,secondary,PURPLE_REQUEST_CHOICE);
  combo = gnt_combo_box_new();
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),combo);
  while((text = (va_arg(choices,const char *))) != 0){
    val = (va_arg(choices,int ));
    gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype()))),((gpointer )((glong )(val + 1))),text);
  }
  gnt_combo_box_set_selected(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype()))),((gpointer )((glong )(default_value + 1))));
  box = setup_button_box(window,user_data,request_choice_cb,combo,ok_text,ok_cb,cancel_text,cancel_cb,((void *)((void *)0)));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),box);
  setup_default_callback(window,cancel_cb,user_data);
  gnt_widget_show(window);
  return window;
}

static void request_action_cb(GntWidget *button,GntWidget *window)
{
  PurpleRequestActionCb callback = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate-callback"));
  gpointer data = g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate-userdata");
  int id = (gint )((glong )(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate-id")));
  if (callback != 0) 
    ( *callback)(data,id);
  purple_request_close(PURPLE_REQUEST_ACTION,window);
}

static void *finch_request_action(const char *title,const char *primary,const char *secondary,int default_value,PurpleAccount *account,const char *who,PurpleConversation *conv,void *user_data,size_t actioncount,va_list actions)
{
  GntWidget *window;
  GntWidget *box;
  GntWidget *button;
  GntWidget *focus = (GntWidget *)((void *)0);
  int i;
  window = setup_request_window(title,primary,secondary,PURPLE_REQUEST_ACTION);
  box = gnt_box_new(0,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),box);
  for (i = 0; i < actioncount; i++) {
    const char *text = va_arg(actions,const char *);
    PurpleRequestActionCb callback = va_arg(actions,PurpleRequestActionCb );
    button = gnt_button_new(text);
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate-callback",callback);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate-userdata",user_data);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate-id",((gpointer )((glong )i)));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )request_action_cb),window,0,((GConnectFlags )0));
    if (i == default_value) 
      focus = button;
  }
  gnt_widget_show(window);
  if (focus != 0) 
    gnt_box_give_focus_to_child(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),focus);
  return window;
}

static void request_fields_cb(GntWidget *button,PurpleRequestFields *fields)
{
  PurpleRequestFieldsCb callback = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate-callback"));
  gpointer data = g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate-userdata");
  GList *list;
/* Update the data of the fields. Pidgin does this differently. Instead of
	 * updating the fields at the end like here, it updates the appropriate field
	 * instantly whenever a change is made. That allows it to make sure the
	 * 'required' fields are entered before the user can hit OK. It's not the case
	 * here, althought it can be done. */
  for (list = purple_request_fields_get_groups(fields); list != 0; list = (list -> next)) {
    PurpleRequestFieldGroup *group = (list -> data);
    GList *fields = purple_request_field_group_get_fields(group);
    for (; fields != 0; fields = (fields -> next)) {{
        PurpleRequestField *field = (fields -> data);
        PurpleRequestFieldType type = purple_request_field_get_type(field);
        if (!(purple_request_field_is_visible(field) != 0)) 
          continue; 
        if (type == PURPLE_REQUEST_FIELD_BOOLEAN) {
          GntWidget *check = (purple_request_field_get_ui_data(field));
          gboolean value = gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)check),gnt_check_box_get_gtype()))));
          purple_request_field_bool_set_value(field,value);
        }
        else if (type == PURPLE_REQUEST_FIELD_STRING) {
          GntWidget *entry = (purple_request_field_get_ui_data(field));
          const char *text = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_entry_get_gtype()))));
          purple_request_field_string_set_value(field,(((text != 0) && (( *text) != 0))?text : ((const char *)((void *)0))));
        }
        else if (type == PURPLE_REQUEST_FIELD_INTEGER) {
          GntWidget *entry = (purple_request_field_get_ui_data(field));
          const char *text = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_entry_get_gtype()))));
          int value = ((text != 0) && (( *text) != 0))?atoi(text) : 0;
          purple_request_field_int_set_value(field,value);
        }
        else if (type == PURPLE_REQUEST_FIELD_CHOICE) {
          GntWidget *combo = (purple_request_field_get_ui_data(field));
          int id;
          id = ((gint )((glong )(gnt_combo_box_get_selected_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype())))))));
          purple_request_field_choice_set_value(field,id);
        }
        else if (type == PURPLE_REQUEST_FIELD_LIST) {
          GList *list = (GList *)((void *)0);
          GList *iter;
          if (purple_request_field_list_get_multi_select(field) != 0) {
            GntWidget *tree = (purple_request_field_get_ui_data(field));
            iter = purple_request_field_list_get_items(field);
            for (; iter != 0; iter = (iter -> next)) {
              const char *text = (iter -> data);
              gpointer key = purple_request_field_list_get_data(field,text);
              if (gnt_tree_get_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),key) != 0) 
                list = g_list_prepend(list,((gpointer )text));
            }
          }
          else {
            GntWidget *combo = (purple_request_field_get_ui_data(field));
            gpointer data = gnt_combo_box_get_selected_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype()))));
            iter = purple_request_field_list_get_items(field);
{
              for (; iter != 0; iter = (iter -> next)) {
                const char *text = (iter -> data);
                gpointer key = purple_request_field_list_get_data(field,text);
                if (key == data) {
                  list = g_list_prepend(list,((gpointer )text));
                  break; 
                }
              }
            }
          }
          purple_request_field_list_set_selected(field,list);
          g_list_free(list);
        }
        else if (type == PURPLE_REQUEST_FIELD_ACCOUNT) {
          GntWidget *combo = (purple_request_field_get_ui_data(field));
          PurpleAccount *acc = (gnt_combo_box_get_selected_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype())))));
          purple_request_field_account_set_value(field,acc);
        }
      }
    }
  }
  purple_notify_close_with_handle(button);
  if (!(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"cancellation-function") != 0) && !(purple_request_fields_all_required_filled(fields) != 0)) {
    purple_notify_message(button,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Error"))),((const char *)(dgettext("pidgin","You must fill all the required fields."))),((const char *)(dgettext("pidgin","The required fields are underlined."))),0,0);
    return ;
  }
  if (callback != 0) 
    ( *callback)(data,fields);
  while((button -> parent) != 0)
    button = (button -> parent);
  purple_request_close(PURPLE_REQUEST_FIELDS,button);
}

static void update_selected_account(GntEntry *username,const char *start,const char *end,GntComboBox *accountlist)
{
  GList *accounts = gnt_tree_get_rows(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(accountlist -> dropdown)),gnt_tree_get_gtype()))));
  const char *name = gnt_entry_get_text(username);
{
    while(accounts != 0){
      if (purple_find_buddy((accounts -> data),name) != 0) {
        gnt_combo_box_set_selected(accountlist,(accounts -> data));
        gnt_widget_draw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)accountlist),gnt_widget_get_gtype()))));
        break; 
      }
      accounts = (accounts -> next);
    }
  }
}

static GntWidget *create_boolean_field(PurpleRequestField *field)
{
  const char *label = purple_request_field_get_label(field);
  GntWidget *check = gnt_check_box_new(label);
  gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)check),gnt_check_box_get_gtype()))),purple_request_field_bool_get_default_value(field));
  return check;
}

static GntWidget *create_string_field(PurpleRequestField *field,GntWidget **username)
{
  const char *hint = purple_request_field_get_type_hint(field);
  GntWidget *entry = gnt_entry_new(purple_request_field_string_get_default_value(field));
  gnt_entry_set_masked(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_entry_get_gtype()))),purple_request_field_string_is_masked(field));
  if ((hint != 0) && (purple_str_has_prefix(hint,"screenname") != 0)) {
    PurpleBlistNode *node = purple_blist_get_root();
    gboolean offline = purple_str_has_suffix(hint,"all");
    for (; node != 0; node = purple_blist_node_next(node,offline)) {
      if (!((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)) 
        continue; 
      gnt_entry_add_suggest(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_entry_get_gtype()))),purple_buddy_get_name(((PurpleBuddy *)node)));
    }
    gnt_entry_set_always_suggest(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_entry_get_gtype()))),1);
    if (username != 0) 
       *username = entry;
  }
  else if ((hint != 0) && !(strcmp(hint,"group") != 0)) {
    PurpleBlistNode *node;
    for (node = purple_blist_get_root(); node != 0; node = purple_blist_node_get_sibling_next(node)) {
      if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) 
        gnt_entry_add_suggest(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_entry_get_gtype()))),purple_group_get_name(((PurpleGroup *)node)));
    }
  }
  return entry;
}

static GntWidget *create_integer_field(PurpleRequestField *field)
{
  char str[256UL];
  int val = purple_request_field_int_get_default_value(field);
  GntWidget *entry;
  snprintf(str,(sizeof(str)),"%d",val);
  entry = gnt_entry_new(str);
  gnt_entry_set_flag(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_entry_get_gtype()))),GNT_ENTRY_FLAG_INT);
  return entry;
}

static GntWidget *create_choice_field(PurpleRequestField *field)
{
  int id;
  GList *list;
  GntWidget *combo = gnt_combo_box_new();
  list = purple_request_field_choice_get_labels(field);
  for (id = 1; list != 0; ((list = (list -> next)) , id++)) {
    gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype()))),((gpointer )((glong )id)),(list -> data));
  }
  gnt_combo_box_set_selected(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype()))),((gpointer )((glong )(purple_request_field_choice_get_default_value(field)))));
  return combo;
}

static GntWidget *create_list_field(PurpleRequestField *field)
{
  GntWidget *ret = (GntWidget *)((void *)0);
  GList *list;
  gboolean multi = purple_request_field_list_get_multi_select(field);
  if (multi != 0) {
    GntWidget *tree = gnt_tree_new();
    list = purple_request_field_list_get_items(field);
    for (; list != 0; list = (list -> next)) {
      const char *text = (list -> data);
      gpointer key = purple_request_field_list_get_data(field,text);
      gnt_tree_add_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),key,gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),text),0,0);
      if (purple_request_field_list_is_selected(field,text) != 0) 
        gnt_tree_set_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),key,1);
    }
    ret = tree;
  }
  else {
    GntWidget *combo = gnt_combo_box_new();
    list = purple_request_field_list_get_items(field);
    for (; list != 0; list = (list -> next)) {
      const char *text = (list -> data);
      gpointer key = purple_request_field_list_get_data(field,text);
      gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype()))),key,text);
      if (purple_request_field_list_is_selected(field,text) != 0) 
        gnt_combo_box_set_selected(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype()))),key);
    }
    ret = combo;
  }
  return ret;
}

static GntWidget *create_account_field(PurpleRequestField *field)
{
  gboolean all;
  PurpleAccount *def;
  GList *list;
  GntWidget *combo = gnt_combo_box_new();
  all = purple_request_field_account_get_show_all(field);
  def = purple_request_field_account_get_value(field);
  if (!(def != 0)) 
    def = purple_request_field_account_get_default_value(field);
  if (all != 0) 
    list = purple_accounts_get_all();
  else 
    list = purple_connections_get_all();
  for (; list != 0; list = (list -> next)) {
    PurpleAccount *account;
    char *text;
    if (all != 0) 
      account = (list -> data);
    else 
      account = purple_connection_get_account((list -> data));
    text = g_strdup_printf("%s (%s)",purple_account_get_username(account),purple_account_get_protocol_name(account));
    gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype()))),account,text);
    g_free(text);
    if (account == def) 
      gnt_combo_box_set_selected(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype()))),account);
  }
/* ew */
  gnt_widget_set_size(combo,20,3);
  return combo;
}

static void *finch_request_fields(const char *title,const char *primary,const char *secondary,PurpleRequestFields *allfields,const char *ok,GCallback ok_cb,const char *cancel,GCallback cancel_cb,PurpleAccount *account,const char *who,PurpleConversation *conv,void *userdata)
{
  GntWidget *window;
  GntWidget *box;
  GList *grlist;
  GntWidget *username = (GntWidget *)((void *)0);
  GntWidget *accountlist = (GntWidget *)((void *)0);
  window = setup_request_window(title,primary,secondary,PURPLE_REQUEST_FIELDS);
/* This is how it's going to work: the request-groups are going to be
	 * stacked vertically one after the other. A GntLine will be separating
	 * the groups. */
  box = gnt_box_new(0,1);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),0);
  gnt_box_set_fill(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),1);
  for (grlist = purple_request_fields_get_groups(allfields); grlist != 0; grlist = (grlist -> next)) {
    PurpleRequestFieldGroup *group = (grlist -> data);
    GList *fields = purple_request_field_group_get_fields(group);
    GntWidget *hbox;
    const char *title = purple_request_field_group_get_title(group);
    if (title != 0) 
      gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),gnt_label_new_with_format(title,GNT_TEXT_FLAG_BOLD));
    for (; fields != 0; fields = (fields -> next)) {{
        PurpleRequestField *field = (fields -> data);
        PurpleRequestFieldType type = purple_request_field_get_type(field);
        const char *label = purple_request_field_get_label(field);
        if (!(purple_request_field_is_visible(field) != 0)) 
          continue; 
/* hrm */
        hbox = gnt_box_new(1,0);
        gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),hbox);
        if ((type != PURPLE_REQUEST_FIELD_BOOLEAN) && (label != 0)) {
          GntWidget *l;
          if (purple_request_field_is_required(field) != 0) 
            l = gnt_label_new_with_format(label,GNT_TEXT_FLAG_UNDERLINE);
          else 
            l = gnt_label_new(label);
          gnt_widget_set_size(l,0,1);
          gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),l);
        }
        if (type == PURPLE_REQUEST_FIELD_BOOLEAN) {
          purple_request_field_set_ui_data(field,(create_boolean_field(field)));
        }
        else if (type == PURPLE_REQUEST_FIELD_STRING) {
          purple_request_field_set_ui_data(field,(create_string_field(field,&username)));
        }
        else if (type == PURPLE_REQUEST_FIELD_INTEGER) {
          purple_request_field_set_ui_data(field,(create_integer_field(field)));
        }
        else if (type == PURPLE_REQUEST_FIELD_CHOICE) {
          purple_request_field_set_ui_data(field,(create_choice_field(field)));
        }
        else if (type == PURPLE_REQUEST_FIELD_LIST) {
          purple_request_field_set_ui_data(field,(create_list_field(field)));
        }
        else if (type == PURPLE_REQUEST_FIELD_ACCOUNT) {
          accountlist = create_account_field(field);
          purple_request_field_set_ui_data(field,accountlist);
        }
        else {
          purple_request_field_set_ui_data(field,(gnt_label_new_with_format(((const char *)(dgettext("pidgin","Not implemented yet."))),GNT_TEXT_FLAG_BOLD)));
        }
        gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),GNT_ALIGN_MID);
        gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(purple_request_field_get_ui_data(field))),gnt_widget_get_gtype()))));
      }
    }
    if ((grlist -> next) != 0) 
      gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),gnt_line_new(0));
  }
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),box);
  box = setup_button_box(window,userdata,request_fields_cb,allfields,ok,ok_cb,cancel,cancel_cb,((void *)((void *)0)));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),box);
  setup_default_callback(window,cancel_cb,userdata);
  gnt_widget_show(window);
  if ((username != 0) && (accountlist != 0)) {
    g_signal_connect_data(username,"completion",((GCallback )update_selected_account),accountlist,0,((GConnectFlags )0));
  }
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"fields",allfields);
  return window;
}

static void file_cancel_cb(gpointer fq,GntWidget *wid)
{
  FinchFileRequest *data = fq;
  if ((data -> cbs)[1] != ((void (*)())((void *)0))) 
    ( *((PurpleRequestFileCb )(data -> cbs)[1]))((data -> user_data),0);
  purple_request_close(PURPLE_REQUEST_FILE,(data -> dialog));
}

static void file_ok_cb(gpointer fq,GntWidget *widget)
{
  FinchFileRequest *data = fq;
  char *file = gnt_file_sel_get_selected_file(((GntFileSel *)(g_type_check_instance_cast(((GTypeInstance *)(data -> dialog)),gnt_file_sel_get_gtype()))));
  char *dir = g_path_get_dirname(file);
  if ((data -> cbs)[0] != ((void (*)())((void *)0))) 
    ( *((PurpleRequestFileCb )(data -> cbs)[0]))((data -> user_data),file);
  g_free(file);
  purple_prefs_set_path((((data -> save) != 0)?"/finch/filelocations/last_save_folder" : "/finch/filelocations/last_open_folder"),dir);
  g_free(dir);
  purple_request_close(PURPLE_REQUEST_FILE,(data -> dialog));
}

static void file_request_destroy(FinchFileRequest *data)
{
  g_free((data -> cbs));
  g_free(data);
}

static FinchFileRequest *finch_file_request_window(const char *title,const char *path,GCallback ok_cb,GCallback cancel_cb,void *user_data)
{
  GntWidget *window = gnt_file_sel_new();
  GntFileSel *sel = (GntFileSel *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_file_sel_get_gtype()));
  FinchFileRequest *data = (FinchFileRequest *)(g_malloc0_n(1,(sizeof(FinchFileRequest ))));
  data -> user_data = user_data;
  data -> cbs = ((GCallback *)(g_malloc0_n(2,(sizeof(GCallback )))));
  (data -> cbs)[0] = ok_cb;
  (data -> cbs)[1] = cancel_cb;
  data -> dialog = window;
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),title);
  gnt_file_sel_set_current_location(sel,(((path != 0) && (( *path) != 0))?path : purple_home_dir()));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> cancel)),((GType )(20 << 2))))),"activate",((GCallback )action_performed),window,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> select)),((GType )(20 << 2))))),"activate",((GCallback )action_performed),window,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> cancel)),((GType )(20 << 2))))),"activate",((GCallback )file_cancel_cb),data,0,G_CONNECT_SWAPPED);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> select)),((GType )(20 << 2))))),"activate",((GCallback )file_ok_cb),data,0,G_CONNECT_SWAPPED);
  setup_default_callback(window,file_cancel_cb,data);
  g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"filerequestdata",data,((GDestroyNotify )file_request_destroy));
  return data;
}

static void *finch_request_file(const char *title,const char *filename,gboolean savedialog,GCallback ok_cb,GCallback cancel_cb,PurpleAccount *account,const char *who,PurpleConversation *conv,void *user_data)
{
  FinchFileRequest *data;
  const char *path;
  path = purple_prefs_get_path((((savedialog != 0)?"/finch/filelocations/last_save_folder" : "/finch/filelocations/last_open_folder")));
  data = finch_file_request_window(((title != 0)?title : (((savedialog != 0)?((const char *)(dgettext("pidgin","Save File..."))) : ((const char *)(dgettext("pidgin","Open File...")))))),path,ok_cb,cancel_cb,user_data);
  data -> save = savedialog;
  if (savedialog != 0) 
    gnt_file_sel_set_suggested_filename(((GntFileSel *)(g_type_check_instance_cast(((GTypeInstance *)(data -> dialog)),gnt_file_sel_get_gtype()))),filename);
  gnt_widget_show((data -> dialog));
  return (data -> dialog);
}

static void *finch_request_folder(const char *title,const char *dirname,GCallback ok_cb,GCallback cancel_cb,PurpleAccount *account,const char *who,PurpleConversation *conv,void *user_data)
{
  FinchFileRequest *data;
  data = finch_file_request_window(((title != 0)?title : ((const char *)(dgettext("pidgin","Choose Location...")))),dirname,ok_cb,cancel_cb,user_data);
  data -> save = 1;
  gnt_file_sel_set_dirs_only(((GntFileSel *)(g_type_check_instance_cast(((GTypeInstance *)(data -> dialog)),gnt_file_sel_get_gtype()))),1);
  gnt_widget_show((data -> dialog));
  return (data -> dialog);
}
static PurpleRequestUiOps uiops = {(finch_request_input), (finch_request_choice), (finch_request_action), (finch_request_fields), (finch_request_file), (finch_close_request), (finch_request_folder), ((void *(*)(const char *, const char *, const char *, int , PurpleAccount *, const char *, PurpleConversation *, gconstpointer , gsize , void *, size_t , va_list ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))};

PurpleRequestUiOps *finch_request_get_ui_ops()
{
  return &uiops;
}

void finch_request_init()
{
}

void finch_request_uninit()
{
}

void finch_request_save_in_prefs(gpointer null,PurpleRequestFields *allfields)
{
  GList *list;
  for (list = purple_request_fields_get_groups(allfields); list != 0; list = (list -> next)) {
    PurpleRequestFieldGroup *group = (list -> data);
    GList *fields = purple_request_field_group_get_fields(group);
    for (; fields != 0; fields = (fields -> next)) {
      PurpleRequestField *field = (fields -> data);
      PurpleRequestFieldType type = purple_request_field_get_type(field);
      PurplePrefType pt;
      gpointer val = (gpointer )((void *)0);
      const char *id = purple_request_field_get_id(field);
      switch(type){
        case PURPLE_REQUEST_FIELD_LIST:
{
          val = ( *purple_request_field_list_get_selected(field)).data;
          val = purple_request_field_list_get_data(field,val);
          break; 
        }
        case PURPLE_REQUEST_FIELD_BOOLEAN:
{
          val = ((gpointer )((glong )(purple_request_field_bool_get_value(field))));
          break; 
        }
        case PURPLE_REQUEST_FIELD_INTEGER:
{
          val = ((gpointer )((glong )(purple_request_field_int_get_value(field))));
          break; 
        }
        case PURPLE_REQUEST_FIELD_STRING:
{
          val = ((gpointer )(purple_request_field_string_get_value(field)));
          break; 
        }
        default:
{
          break; 
        }
      }
      pt = purple_prefs_get_type(id);
{
        switch(pt){
          case PURPLE_PREF_INT:
{
{
              long tmp = ((gint )((glong )val));
/* Lists always return string */
              if (type == PURPLE_REQUEST_FIELD_LIST) 
                sscanf(val,"%ld",&tmp);
              purple_prefs_set_int(id,((gint )tmp));
              break; 
            }
          }
          case PURPLE_PREF_BOOLEAN:
{
            purple_prefs_set_bool(id,((gint )((glong )val)));
            break; 
          }
          case PURPLE_PREF_STRING:
{
            purple_prefs_set_string(id,val);
            break; 
          }
          default:
{
            break; 
          }
        }
      }
    }
  }
}

GntWidget *finch_request_field_get_widget(PurpleRequestField *field)
{
  GntWidget *ret = (GntWidget *)((void *)0);
  switch(purple_request_field_get_type(field)){
    case PURPLE_REQUEST_FIELD_BOOLEAN:
{
      ret = create_boolean_field(field);
      break; 
    }
    case PURPLE_REQUEST_FIELD_STRING:
{
      ret = create_string_field(field,0);
      break; 
    }
    case PURPLE_REQUEST_FIELD_INTEGER:
{
      ret = create_integer_field(field);
      break; 
    }
    case PURPLE_REQUEST_FIELD_CHOICE:
{
      ret = create_choice_field(field);
      break; 
    }
    case PURPLE_REQUEST_FIELD_LIST:
{
      ret = create_list_field(field);
      break; 
    }
    case PURPLE_REQUEST_FIELD_ACCOUNT:
{
      ret = create_account_field(field);
      break; 
    }
    default:
{
      purple_debug_error("GntRequest","Unimplemented request-field %d\n",purple_request_field_get_type(field));
      break; 
    }
  }
  return ret;
}
