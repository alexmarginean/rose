/**
 * @file gntlog.c GNT Log viewer
 * @ingroup finch
 */
/* finch
 *
 * Finch is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <internal.h>
#include "finch.h"
#include <gnt.h>
#include <gntbox.h>
#include <gntbutton.h>
#include <gntentry.h>
#include <gntlabel.h>
#include <gnttextview.h>
#include <gnttree.h>
#include <gntwindow.h>
#include "account.h"
#include "debug.h"
#include "log.h"
#include "notify.h"
#include "request.h"
#include "util.h"
#include "gntlog.h"
static GHashTable *log_viewers = (GHashTable *)((void *)0);
static void populate_log_tree(FinchLogViewer *lv);
static FinchLogViewer *syslog_viewer = (FinchLogViewer *)((void *)0);

struct log_viewer_hash_t 
{
  PurpleLogType type;
  char *username;
  PurpleAccount *account;
  PurpleContact *contact;
}
;

static guint log_viewer_hash(gconstpointer data)
{
  const struct log_viewer_hash_t *viewer = data;
  if ((viewer -> contact) != ((PurpleContact *)((void *)0))) 
    return g_direct_hash((viewer -> contact));
  if ((viewer -> account) != 0) {
    return g_str_hash((viewer -> username)) + g_str_hash((purple_account_get_username((viewer -> account))));
  }
  return g_direct_hash(viewer);
}

static gboolean log_viewer_equal(gconstpointer y,gconstpointer z)
{
  const struct log_viewer_hash_t *a;
  const struct log_viewer_hash_t *b;
  int ret;
  char *normal;
  a = y;
  b = z;
  if ((a -> contact) != ((PurpleContact *)((void *)0))) {
    if ((b -> contact) != ((PurpleContact *)((void *)0))) 
      return (a -> contact) == (b -> contact);
    else 
      return 0;
  }
  else {
    if ((b -> contact) != ((PurpleContact *)((void *)0))) 
      return 0;
  }
  if (((a -> username) != 0) && ((b -> username) != 0)) {
    normal = g_strdup(purple_normalize((a -> account),(a -> username)));
    ret = (((a -> account) == (b -> account)) && !(strcmp(normal,purple_normalize((b -> account),(b -> username))) != 0));
    g_free(normal);
  }
  else {
    ret = (a == b);
  }
  return ret;
}

static const char *log_get_date(PurpleLog *log)
{
  if ((log -> tm) != 0) 
    return purple_date_format_full((log -> tm));
  else 
    return purple_date_format_full((localtime((&log -> time))));
}

static void search_cb(GntWidget *button,FinchLogViewer *lv)
{
  const char *search_term = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> entry)),gnt_entry_get_gtype()))));
  GList *logs;
  if (!(( *search_term) != 0)) {
/* reset the tree */
    gnt_tree_remove_all(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> tree)),gnt_tree_get_gtype()))));
    g_free((lv -> search));
    lv -> search = ((char *)((void *)0));
    populate_log_tree(lv);
    return ;
  }
  if (((lv -> search) != ((char *)((void *)0))) && !(strcmp((lv -> search),search_term) != 0)) {
    return ;
  }
  g_free((lv -> search));
  lv -> search = g_strdup(search_term);
  gnt_tree_remove_all(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> tree)),gnt_tree_get_gtype()))));
  gnt_text_view_clear(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> text)),gnt_text_view_get_gtype()))));
  for (logs = (lv -> logs); logs != ((GList *)((void *)0)); logs = (logs -> next)) {
    char *read = purple_log_read(((PurpleLog *)(logs -> data)),0);
    if (((read != 0) && (( *read) != 0)) && (purple_strcasestr(read,search_term) != 0)) {
      PurpleLog *log = (logs -> data);
      gnt_tree_add_row_last(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> tree)),gnt_tree_get_gtype()))),log,gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> tree)),gnt_tree_get_gtype()))),log_get_date(log)),0);
    }
    g_free(read);
  }
}

static void destroy_cb(GntWidget *w,struct log_viewer_hash_t *ht)
{
  FinchLogViewer *lv = syslog_viewer;
  if (ht != ((struct log_viewer_hash_t *)((void *)0))) {
    lv = (g_hash_table_lookup(log_viewers,ht));
    g_hash_table_remove(log_viewers,ht);
    g_free((ht -> username));
    g_free(ht);
  }
  else 
    syslog_viewer = ((FinchLogViewer *)((void *)0));
  purple_request_close_with_handle(lv);
  g_list_foreach((lv -> logs),((GFunc )purple_log_free),0);
  g_list_free((lv -> logs));
  g_free((lv -> search));
  g_free(lv);
  gnt_widget_destroy(w);
}

static void log_select_cb(GntWidget *w,gpointer old,gpointer new,FinchLogViewer *viewer)
{
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)w),gnt_tree_get_gtype()));
  PurpleLog *log = (PurpleLog *)((void *)0);
  PurpleLogReadFlags flags;
  char *read = (char *)((void *)0);
  char *strip;
  char *newline;
  if (!((viewer -> search) != 0) && !(gnt_tree_get_parent_key(tree,new) != 0)) 
    return ;
  log = ((PurpleLog *)new);
  if (log == ((PurpleLog *)((void *)0))) 
    return ;
  if ((log -> type) != PURPLE_LOG_SYSTEM) {
    char *title;
    if ((log -> type) == PURPLE_LOG_CHAT) 
      title = g_strdup_printf(((const char *)(dgettext("pidgin","Conversation in %s on %s"))),(log -> name),log_get_date(log));
    else 
      title = g_strdup_printf(((const char *)(dgettext("pidgin","Conversation with %s on %s"))),(log -> name),log_get_date(log));
    gnt_label_set_text(((GntLabel *)(g_type_check_instance_cast(((GTypeInstance *)(viewer -> label)),gnt_label_get_gtype()))),title);
    g_free(title);
  }
  read = purple_log_read(log,&flags);
  if (flags != PURPLE_LOG_READ_NO_NEWLINE) {
    newline = purple_strdup_withhtml(read);
    strip = purple_markup_strip_html(newline);
    g_free(newline);
  }
  else {
    strip = purple_markup_strip_html(read);
  }
  viewer -> flags = flags;
  purple_signal_emit(finch_log_get_handle(),"log-displaying",viewer,log);
  gnt_text_view_clear(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(viewer -> text)),gnt_text_view_get_gtype()))));
  gnt_text_view_append_text_with_flags(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(viewer -> text)),gnt_text_view_get_gtype()))),strip,GNT_TEXT_FLAG_NORMAL);
  g_free(read);
  g_free(strip);
}
/* I want to make this smarter, but haven't come up with a cool algorithm to do so, yet.
 * I want the tree to be divided into groups like "Today," "Yesterday," "Last week,"
 * "August," "2002," etc. based on how many conversation took place in each subdivision.
 *
 * For now, I'll just make it a flat list.
 */

static void populate_log_tree(FinchLogViewer *lv)
/* Logs are made from trees in real life.
        This is a tree made from logs */
{
  const char *pmonth;
  char *month = (char *)((void *)0);
  char prev_top_month[30UL] = "";
  GList *logs = (lv -> logs);
  while(logs != ((GList *)((void *)0))){
    PurpleLog *log = (logs -> data);
    pmonth = purple_utf8_strftime(((const char *)(dgettext("pidgin","%B %Y"))),((((log -> tm) != 0)?(log -> tm) : localtime((&log -> time)))));
    if (strcmp(pmonth,prev_top_month) != 0) {
      month = g_strdup(pmonth);
/* top level */
      gnt_tree_add_row_last(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> tree)),gnt_tree_get_gtype()))),month,gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> tree)),gnt_tree_get_gtype()))),month),0);
      gnt_tree_set_expanded(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> tree)),gnt_tree_get_gtype()))),month,0);
      g_strlcpy(prev_top_month,month,(sizeof(prev_top_month)));
    }
/* sub */
    gnt_tree_add_row_last(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> tree)),gnt_tree_get_gtype()))),log,gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> tree)),gnt_tree_get_gtype()))),log_get_date(log)),month);
    logs = (logs -> next);
  }
}

static FinchLogViewer *display_log_viewer(struct log_viewer_hash_t *ht,GList *logs,const char *title,int log_size)
{
  FinchLogViewer *lv;
  char *text;
  GntWidget *vbox;
  GntWidget *hbox;
  GntWidget *size_label;
  if (logs == ((GList *)((void *)0))) {
/* No logs were found. */
    const char *log_preferences = (const char *)((void *)0);
    if (ht == ((struct log_viewer_hash_t *)((void *)0))) {
      if (!(purple_prefs_get_bool("/purple/logging/log_system") != 0)) 
        log_preferences = ((const char *)(dgettext("pidgin","System events will only be logged if the \"Log all status changes to system log\" preference is enabled.")));
    }
    else {
      if ((ht -> type) == PURPLE_LOG_IM) {
        if (!(purple_prefs_get_bool("/purple/logging/log_ims") != 0)) 
          log_preferences = ((const char *)(dgettext("pidgin","Instant messages will only be logged if the \"Log all instant messages\" preference is enabled.")));
      }
      else if ((ht -> type) == PURPLE_LOG_CHAT) {
        if (!(purple_prefs_get_bool("/purple/logging/log_chats") != 0)) 
          log_preferences = ((const char *)(dgettext("pidgin","Chats will only be logged if the \"Log all chats\" preference is enabled.")));
      }
      g_free((ht -> username));
      g_free(ht);
    }
    purple_notify_message(0,PURPLE_NOTIFY_MSG_INFO,title,((const char *)(dgettext("pidgin","No logs were found"))),log_preferences,0,0);
    return 0;
  }
  lv = ((FinchLogViewer *)(g_malloc0_n(1,(sizeof(FinchLogViewer )))));
  lv -> logs = logs;
  if (ht != ((struct log_viewer_hash_t *)((void *)0))) 
    g_hash_table_insert(log_viewers,ht,lv);
/* Window ***********/
  lv -> window = gnt_window_box_new(0,1);
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> window)),gnt_box_get_gtype()))),title);
  gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> window)),gnt_box_get_gtype()))),1);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> window)),gnt_box_get_gtype()))),0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> window)),((GType )(20 << 2))))),"destroy",((GCallback )destroy_cb),ht,0,((GConnectFlags )0));
  vbox = gnt_box_new(0,1);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> window)),gnt_box_get_gtype()))),vbox);
/* Label ************/
  text = g_strdup_printf("%s",title);
  lv -> label = gnt_label_new_with_format(text,GNT_TEXT_FLAG_BOLD);
  g_free(text);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),(lv -> label));
  hbox = gnt_box_new(0,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),hbox);
/* List *************/
  lv -> tree = gnt_tree_new();
  gnt_widget_set_size((lv -> tree),30,0);
  populate_log_tree(lv);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> tree)),((GType )(20 << 2))))),"selection-changed",((GCallback )log_select_cb),lv,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),(lv -> tree));
/* Viewer ************/
  lv -> text = gnt_text_view_new();
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),(lv -> text));
  gnt_text_view_set_flag(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> text)),gnt_text_view_get_gtype()))),GNT_TEXT_VIEW_TOP_ALIGN);
  hbox = gnt_box_new(0,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),hbox);
/* Log size ************/
  if (log_size != 0) {
    char *sz_txt = purple_str_size_to_units(log_size);
    text = g_strdup_printf("%s %s",((const char *)(dgettext("pidgin","Total log size:"))),sz_txt);
    size_label = gnt_label_new(text);
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),size_label);
    g_free(sz_txt);
    g_free(text);
  }
/* Search box **********/
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),gnt_label_new(((const char *)(dgettext("pidgin","Scroll/Search: ")))));
  lv -> entry = gnt_entry_new("");
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),(lv -> entry));
  g_signal_connect_data(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> entry)),gnt_entry_get_gtype()))),"activate",((GCallback )search_cb),lv,0,((GConnectFlags )0));
  gnt_text_view_attach_scroll_widget(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> text)),gnt_text_view_get_gtype()))),(lv -> entry));
  gnt_text_view_attach_pager_widget(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> text)),gnt_text_view_get_gtype()))),(lv -> entry));
  gnt_widget_show((lv -> window));
  return lv;
}

static void our_logging_blows(PurpleLogSet *set,PurpleLogSet *setagain,GList **list)
{
/* The iteration happens on the first list. So we use the shorter list in front */
  if ((set -> type) != PURPLE_LOG_IM) 
    return ;
   *list = g_list_concat(purple_log_get_logs(PURPLE_LOG_IM,(set -> name),(set -> account)), *list);
}

void finch_log_show(PurpleLogType type,const char *username,PurpleAccount *account)
{
  struct log_viewer_hash_t *ht;
  FinchLogViewer *lv = (FinchLogViewer *)((void *)0);
  const char *name = username;
  char *title;
  GList *logs = (GList *)((void *)0);
  int size = 0;
  if (type != PURPLE_LOG_IM) {
    do {
      if (account != ((PurpleAccount *)((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
        return ;
      };
    }while (0);
    do {
      if (username != ((const char *)((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"username != NULL");
        return ;
      };
    }while (0);
  }
  ht = ((struct log_viewer_hash_t *)(g_malloc0_n(1,(sizeof(struct log_viewer_hash_t )))));
  ht -> type = type;
  ht -> username = g_strdup(username);
  ht -> account = account;
  if (log_viewers == ((GHashTable *)((void *)0))) {
    log_viewers = g_hash_table_new(log_viewer_hash,log_viewer_equal);
  }
  else if ((lv = (g_hash_table_lookup(log_viewers,ht))) != 0) {
    gnt_window_present((lv -> window));
    g_free((ht -> username));
    g_free(ht);
    return ;
  }
  if (type == PURPLE_LOG_CHAT) {
    PurpleChat *chat;
    chat = purple_blist_find_chat(account,username);
    if (chat != ((PurpleChat *)((void *)0))) 
      name = purple_chat_get_name(chat);
    title = g_strdup_printf(((const char *)(dgettext("pidgin","Conversations in %s"))),name);
  }
  else {
    PurpleBuddy *buddy;
    if (username != 0) {
      buddy = purple_find_buddy(account,username);
      if (buddy != ((PurpleBuddy *)((void *)0))) 
        name = purple_buddy_get_contact_alias(buddy);
      title = g_strdup_printf(((const char *)(dgettext("pidgin","Conversations with %s"))),name);
    }
    else {
      title = g_strdup(((const char *)(dgettext("pidgin","All Conversations"))));
    }
  }
  if (username != 0) {
    logs = purple_log_get_logs(type,username,account);
    size = purple_log_get_total_size(type,username,account);
  }
  else {
/* This will happen only for IMs */
    GHashTable *table = purple_log_get_log_sets();
    g_hash_table_foreach(table,((GHFunc )our_logging_blows),(&logs));
    g_hash_table_destroy(table);
    logs = g_list_sort(logs,purple_log_compare);
    size = 0;
  }
  display_log_viewer(ht,logs,title,size);
  g_free(title);
}

void finch_log_show_contact(PurpleContact *contact)
{
  struct log_viewer_hash_t *ht;
  PurpleBlistNode *child;
  FinchLogViewer *lv = (FinchLogViewer *)((void *)0);
  GList *logs = (GList *)((void *)0);
  const char *name = (const char *)((void *)0);
  char *title;
  int total_log_size = 0;
  do {
    if (contact != ((PurpleContact *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"contact != NULL");
      return ;
    };
  }while (0);
  ht = ((struct log_viewer_hash_t *)(g_malloc0_n(1,(sizeof(struct log_viewer_hash_t )))));
  ht -> type = PURPLE_LOG_IM;
  ht -> contact = contact;
  if (log_viewers == ((GHashTable *)((void *)0))) {
    log_viewers = g_hash_table_new(log_viewer_hash,log_viewer_equal);
  }
  else if ((lv = (g_hash_table_lookup(log_viewers,ht))) != 0) {
    gnt_window_present((lv -> window));
    g_free(ht);
    return ;
  }
  for (child = purple_blist_node_get_first_child(((PurpleBlistNode *)contact)); child != 0; child = purple_blist_node_get_sibling_next(child)) {{
      const char *name;
      PurpleAccount *account;
      if (!((purple_blist_node_get_type(child)) == PURPLE_BLIST_BUDDY_NODE)) 
        continue; 
      name = purple_buddy_get_name(((PurpleBuddy *)child));
      account = purple_buddy_get_account(((PurpleBuddy *)child));
      logs = g_list_concat(purple_log_get_logs(PURPLE_LOG_IM,name,account),logs);
      total_log_size += purple_log_get_total_size(PURPLE_LOG_IM,name,account);
    }
  }
  logs = g_list_sort(logs,purple_log_compare);
  name = purple_contact_get_alias(contact);
  if (!(name != 0)) 
    name = purple_buddy_get_contact_alias(purple_contact_get_priority_buddy(contact));
/* This will happen if the contact doesn't have an alias,
	 * and none of the contact's buddies are online.
	 * There is probably a better way to deal with this. */
  if (name == ((const char *)((void *)0))) {
    child = purple_blist_node_get_first_child(((PurpleBlistNode *)contact));
    if ((child != ((PurpleBlistNode *)((void *)0))) && ((purple_blist_node_get_type(child)) == PURPLE_BLIST_BUDDY_NODE)) 
      name = purple_buddy_get_contact_alias(((PurpleBuddy *)child));
    if (name == ((const char *)((void *)0))) 
      name = "";
  }
  title = g_strdup_printf(((const char *)(dgettext("pidgin","Conversations with %s"))),name);
  display_log_viewer(ht,logs,title,total_log_size);
  g_free(title);
}

void finch_syslog_show()
{
  GList *accounts = (GList *)((void *)0);
  GList *logs = (GList *)((void *)0);
  if (syslog_viewer != ((FinchLogViewer *)((void *)0))) {
    gnt_window_present((syslog_viewer -> window));
    return ;
  }
  for (accounts = purple_accounts_get_all(); accounts != ((GList *)((void *)0)); accounts = (accounts -> next)) {{
      PurpleAccount *account = (PurpleAccount *)(accounts -> data);
      if (purple_find_prpl(purple_account_get_protocol_id(account)) == ((PurplePlugin *)((void *)0))) 
        continue; 
      logs = g_list_concat(purple_log_get_system_logs(account),logs);
    }
  }
  logs = g_list_sort(logs,purple_log_compare);
  syslog_viewer = display_log_viewer(0,logs,((const char *)(dgettext("pidgin","System Log"))),0);
}
/****************************************************************************
 * GNT LOG SUBSYSTEM *******************************************************
 ****************************************************************************/

void *finch_log_get_handle()
{
  static int handle;
  return (&handle);
}

void finch_log_init()
{
  void *handle = finch_log_get_handle();
  purple_signal_register(handle,"log-displaying",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_BOXED,"FinchLogViewer *"),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_LOG));
}

void finch_log_uninit()
{
  purple_signals_unregister_by_instance(finch_log_get_handle());
}
