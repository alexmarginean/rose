/**
 * @file gntmedia.c GNT Media API
 * @ingroup finch
 */
/* finch
 *
 * Finch is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <internal.h>
#include "finch.h"
#include "gntconv.h"
#include "gntmedia.h"
#include "gnt.h"
#include "gntbutton.h"
#include "gntbox.h"
#include "gntlabel.h"
#include "cmds.h"
#include "conversation.h"
#include "debug.h"
#include "mediamanager.h"
/* An incredibly large part of the following is from gtkmedia.c */
#ifdef USE_VV
#include "media-gst.h"
#undef hangup
#define FINCH_TYPE_MEDIA            (finch_media_get_type())
#define FINCH_MEDIA(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), FINCH_TYPE_MEDIA, FinchMedia))
#define FINCH_MEDIA_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass), FINCH_TYPE_MEDIA, FinchMediaClass))
#define FINCH_IS_MEDIA(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), FINCH_TYPE_MEDIA))
#define FINCH_IS_MEDIA_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), FINCH_TYPE_MEDIA))
#define FINCH_MEDIA_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), FINCH_TYPE_MEDIA, FinchMediaClass))
typedef struct _FinchMedia FinchMedia;
typedef struct _FinchMediaClass FinchMediaClass;
typedef struct _FinchMediaPrivate FinchMediaPrivate;
typedef enum _FinchMediaState FinchMediaState;

struct _FinchMediaClass 
{
  GntBoxClass parent_class;
}
;

struct _FinchMedia 
{
  GntBox parent;
  FinchMediaPrivate *priv;
}
;

struct _FinchMediaPrivate 
{
  PurpleMedia *media;
  GntWidget *accept;
  GntWidget *reject;
  GntWidget *hangup;
  GntWidget *calling;
  PurpleConversation *conv;
}
;
#define FINCH_MEDIA_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE((obj), FINCH_TYPE_MEDIA, FinchMediaPrivate))
static void finch_media_class_init(FinchMediaClass *klass);
static void finch_media_init(FinchMedia *media);
static void finch_media_finalize(GObject *object);
static void finch_media_get_property(GObject *object,guint prop_id,GValue *value,GParamSpec *pspec);
static void finch_media_set_property(GObject *object,guint prop_id,const GValue *value,GParamSpec *pspec);
static GntBoxClass *parent_class = (GntBoxClass *)((void *)0);
enum __unnamed_enum___F0_L93_C1_MESSAGE__COMMA__LAST_SIGNAL {MESSAGE,LAST_SIGNAL};
static guint finch_media_signals[1UL] = {(0)};
enum __unnamed_enum___F0_L99_C1_PROP_0__COMMA__PROP_MEDIA {PROP_0,PROP_MEDIA};

static GType finch_media_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(FinchMediaClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )finch_media_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(FinchMedia ))), (0), ((GInstanceInitFunc )finch_media_init), ((const GTypeValueTable *)((void *)0))};
    type = g_type_register_static(gnt_box_get_gtype(),"FinchMedia",&info,0);
  }
  return type;
}

static void finch_media_class_init(FinchMediaClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *)klass;
  parent_class = (g_type_class_peek_parent(klass));
  gobject_class -> finalize = finch_media_finalize;
  gobject_class -> set_property = finch_media_set_property;
  gobject_class -> get_property = finch_media_get_property;
  g_object_class_install_property(gobject_class,PROP_MEDIA,g_param_spec_object("media","PurpleMedia","The PurpleMedia associated with this media.",purple_media_get_type(),(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  finch_media_signals[MESSAGE] = g_signal_new("message",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,0,0,0,g_cclosure_marshal_VOID__STRING,((GType )(1 << 2)),1,((GType )(16 << 2)));
  g_type_class_add_private(klass,(sizeof(FinchMediaPrivate )));
}

static void finch_media_init(FinchMedia *media)
{
  media -> priv = ((FinchMediaPrivate *)(g_type_instance_get_private(((GTypeInstance *)media),finch_media_get_type())));
  ( *(media -> priv)).calling = gnt_label_new(((const char *)(dgettext("pidgin","Calling..."))));
  ( *(media -> priv)).hangup = gnt_button_new(((const char *)(dgettext("pidgin","Hangup"))));
  ( *(media -> priv)).accept = gnt_button_new(((const char *)(dgettext("pidgin","Accept"))));
  ( *(media -> priv)).reject = gnt_button_new(((const char *)(dgettext("pidgin","Reject"))));
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)media),gnt_box_get_gtype()))),GNT_ALIGN_MID);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)media),gnt_box_get_gtype()))),( *(media -> priv)).accept);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)media),gnt_box_get_gtype()))),( *(media -> priv)).reject);
}

static void finch_media_finalize(GObject *media)
{
  FinchMedia *gntmedia = (FinchMedia *)(g_type_check_instance_cast(((GTypeInstance *)media),finch_media_get_type()));
  purple_debug_info("gntmedia","finch_media_finalize\n");
  if (( *(gntmedia -> priv)).media != 0) 
    g_object_unref(( *(gntmedia -> priv)).media);
}

static void finch_media_emit_message(FinchMedia *gntmedia,const char *msg)
{
  g_signal_emit(gntmedia,finch_media_signals[MESSAGE],0,msg);
}

static void finch_media_connected_cb(PurpleMedia *media,FinchMedia *gntmedia)
{
  GntWidget *parent;
  finch_media_emit_message(gntmedia,((const char *)(dgettext("pidgin","Call in progress."))));
  gnt_box_remove(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)gntmedia),gnt_box_get_gtype()))),( *(gntmedia -> priv)).accept);
  gnt_box_remove(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)gntmedia),gnt_box_get_gtype()))),( *(gntmedia -> priv)).reject);
  gnt_box_remove(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)gntmedia),gnt_box_get_gtype()))),( *(gntmedia -> priv)).hangup);
  gnt_box_remove(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)gntmedia),gnt_box_get_gtype()))),( *(gntmedia -> priv)).calling);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)gntmedia),gnt_box_get_gtype()))),( *(gntmedia -> priv)).hangup);
  gnt_widget_destroy(( *(gntmedia -> priv)).accept);
  gnt_widget_destroy(( *(gntmedia -> priv)).reject);
  gnt_widget_destroy(( *(gntmedia -> priv)).calling);
  ( *(gntmedia -> priv)).accept = ((GntWidget *)((void *)0));
  ( *(gntmedia -> priv)).reject = ((GntWidget *)((void *)0));
  ( *(gntmedia -> priv)).calling = ((GntWidget *)((void *)0));
  parent = ((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)gntmedia),gnt_widget_get_gtype())));
  while((parent -> parent) != 0)
    parent = (parent -> parent);
  gnt_box_readjust(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)parent),gnt_box_get_gtype()))));
  gnt_widget_draw(parent);
}

static void finch_media_wait_cb(PurpleMedia *media,FinchMedia *gntmedia)
{
  GntWidget *parent;
  gnt_box_remove(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)gntmedia),gnt_box_get_gtype()))),( *(gntmedia -> priv)).accept);
  gnt_box_remove(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)gntmedia),gnt_box_get_gtype()))),( *(gntmedia -> priv)).reject);
  gnt_box_remove(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)gntmedia),gnt_box_get_gtype()))),( *(gntmedia -> priv)).hangup);
  gnt_box_remove(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)gntmedia),gnt_box_get_gtype()))),( *(gntmedia -> priv)).calling);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)gntmedia),gnt_box_get_gtype()))),( *(gntmedia -> priv)).calling);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)gntmedia),gnt_box_get_gtype()))),( *(gntmedia -> priv)).hangup);
  parent = ((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)gntmedia),gnt_widget_get_gtype())));
  while((parent -> parent) != 0)
    parent = (parent -> parent);
  gnt_box_readjust(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)parent),gnt_box_get_gtype()))));
  gnt_widget_draw(parent);
}

static void finch_media_state_changed_cb(PurpleMedia *media,PurpleMediaState state,gchar *sid,gchar *name,FinchMedia *gntmedia)
{
  purple_debug_info("gntmedia","state: %d sid: %s name: %s\n",state,sid,name);
  if ((sid == ((gchar *)((void *)0))) && (name == ((gchar *)((void *)0)))) {
    if (state == PURPLE_MEDIA_STATE_END) {
      finch_media_emit_message(gntmedia,((const char *)(dgettext("pidgin","The call has been terminated."))));
      finch_conversation_set_info_widget(( *(gntmedia -> priv)).conv,0);
      gnt_widget_destroy(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)gntmedia),gnt_widget_get_gtype()))));
/*
			 * XXX: This shouldn't have to be here
			 * to free the FinchMedia widget.
			 */
      g_object_unref(gntmedia);
    }
  }
  else if (state == PURPLE_MEDIA_STATE_CONNECTED) {
    finch_media_connected_cb(media,gntmedia);
  }
  else if ((((state == PURPLE_MEDIA_STATE_NEW) && (sid != ((gchar *)((void *)0)))) && (name != ((gchar *)((void *)0)))) && (purple_media_is_initiator(media,sid,name) == 0)) {
    PurpleAccount *account;
    PurpleBuddy *buddy;
    const gchar *alias;
    PurpleMediaSessionType type = purple_media_get_session_type(media,sid);
    gchar *message = (gchar *)((void *)0);
    account = purple_media_get_account(( *(gntmedia -> priv)).media);
    buddy = purple_find_buddy(account,name);
    alias = ((buddy != 0)?purple_buddy_get_contact_alias(buddy) : name);
    if ((type & PURPLE_MEDIA_AUDIO) != 0U) {
      message = g_strdup_printf(((const char *)(dgettext("pidgin","%s wishes to start an audio session with you."))),alias);
    }
    else {
      message = g_strdup_printf(((const char *)(dgettext("pidgin","%s is trying to start an unsupported media session type with you."))),alias);
    }
    finch_media_emit_message(gntmedia,message);
    g_free(message);
  }
}

static void finch_media_stream_info_cb(PurpleMedia *media,PurpleMediaInfoType type,gchar *sid,gchar *name,gboolean local,FinchMedia *gntmedia)
{
  if (type == PURPLE_MEDIA_INFO_REJECT) {
    finch_media_emit_message(gntmedia,((const char *)(dgettext("pidgin","You have rejected the call."))));
  }
}

static void finch_media_accept_cb(PurpleMedia *media,GntWidget *widget)
{
  purple_media_stream_info(media,PURPLE_MEDIA_INFO_ACCEPT,0,0,1);
}

static void finch_media_hangup_cb(PurpleMedia *media,GntWidget *widget)
{
  purple_media_stream_info(media,PURPLE_MEDIA_INFO_HANGUP,0,0,1);
}

static void finch_media_reject_cb(PurpleMedia *media,GntWidget *widget)
{
  purple_media_stream_info(media,PURPLE_MEDIA_INFO_REJECT,0,0,1);
}

static void finch_media_set_property(GObject *object,guint prop_id,const GValue *value,GParamSpec *pspec)
{
  FinchMedia *media;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = finch_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = 1;
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"FINCH_IS_MEDIA(object)");
      return ;
    };
  }while (0);
  media = ((FinchMedia *)(g_type_check_instance_cast(((GTypeInstance *)object),finch_media_get_type())));
{
    switch(prop_id){
      case PROP_MEDIA:
{
{
          if (( *(media -> priv)).media != 0) 
            g_object_unref(( *(media -> priv)).media);
          ( *(media -> priv)).media = (g_value_get_object(value));
          g_object_ref(( *(media -> priv)).media);
          g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *(media -> priv)).accept),((GType )(20 << 2))))),"activate",((GCallback )finch_media_accept_cb),( *(media -> priv)).media,0,G_CONNECT_SWAPPED);
          g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *(media -> priv)).reject),((GType )(20 << 2))))),"activate",((GCallback )finch_media_reject_cb),( *(media -> priv)).media,0,G_CONNECT_SWAPPED);
          g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *(media -> priv)).hangup),((GType )(20 << 2))))),"activate",((GCallback )finch_media_hangup_cb),( *(media -> priv)).media,0,G_CONNECT_SWAPPED);
          if (purple_media_is_initiator(( *(media -> priv)).media,0,0) == 1) {
            finch_media_wait_cb(( *(media -> priv)).media,media);
          }
          g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *(media -> priv)).media),((GType )(20 << 2))))),"state-changed",((GCallback )finch_media_state_changed_cb),media,0,((GConnectFlags )0));
          g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *(media -> priv)).media),((GType )(20 << 2))))),"stream-info",((GCallback )finch_media_stream_info_cb),media,0,((GConnectFlags )0));
          break; 
        }
      }
      default:
{
        do {
          GObject *_glib__object = (GObject *)object;
          GParamSpec *_glib__pspec = (GParamSpec *)pspec;
          guint _glib__property_id = prop_id;
          g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","gntmedia.c:345","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
        }while (0);
        break; 
      }
    }
  }
}

static void finch_media_get_property(GObject *object,guint prop_id,GValue *value,GParamSpec *pspec)
{
  FinchMedia *media;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = finch_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = 1;
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"FINCH_IS_MEDIA(object)");
      return ;
    };
  }while (0);
  media = ((FinchMedia *)(g_type_check_instance_cast(((GTypeInstance *)object),finch_media_get_type())));
  switch(prop_id){
    case PROP_MEDIA:
{
      g_value_set_object(value,( *(media -> priv)).media);
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = prop_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","gntmedia.c:363","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static GntWidget *finch_media_new(PurpleMedia *media)
{
  return (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(g_object_new(finch_media_get_type(),"media",media,"vertical",0,"homogeneous",0,((void *)((void *)0))))),gnt_widget_get_gtype()));
}

static void gntmedia_message_cb(FinchMedia *gntmedia,const char *msg,PurpleConversation *conv)
{
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
    purple_conv_im_write(purple_conversation_get_im_data(conv),0,msg,PURPLE_MESSAGE_SYSTEM,time(0));
  }
}

static gboolean finch_new_media(PurpleMediaManager *manager,PurpleMedia *media,PurpleAccount *account,gchar *name,gpointer null)
{
  GntWidget *gntmedia;
  PurpleConversation *conv;
  conv = purple_conversation_new(PURPLE_CONV_TYPE_IM,account,name);
  gntmedia = finch_media_new(media);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)gntmedia),((GType )(20 << 2))))),"message",((GCallback )gntmedia_message_cb),conv,0,((GConnectFlags )0));
  ( *( *((FinchMedia *)(g_type_check_instance_cast(((GTypeInstance *)gntmedia),finch_media_get_type())))).priv).conv = conv;
  finch_conversation_set_info_widget(conv,gntmedia);
  return 1;
}

static PurpleCmdRet call_cmd_cb(PurpleConversation *conv,const char *cmd,char **args,char **eror,gpointer data)
{
  PurpleAccount *account = purple_conversation_get_account(conv);
  if (!(purple_prpl_initiate_media(account,purple_conversation_get_name(conv),PURPLE_MEDIA_AUDIO) != 0)) 
    return PURPLE_CMD_STATUS_FAILED;
  return PURPLE_CMD_STATUS_OK;
}

static GstElement *create_default_audio_src(PurpleMedia *media,const gchar *session_id,const gchar *participant)
{
  GstElement *src;
  src = gst_element_factory_make("gconfaudiosrc",0);
  if (src == ((GstElement *)((void *)0))) 
    src = gst_element_factory_make("autoaudiosrc",0);
  if (src == ((GstElement *)((void *)0))) 
    src = gst_element_factory_make("alsasrc",0);
  if (src == ((GstElement *)((void *)0))) 
    src = gst_element_factory_make("osssrc",0);
  if (src == ((GstElement *)((void *)0))) 
    src = gst_element_factory_make("dshowaudiosrc",0);
  if (src == ((GstElement *)((void *)0))) {
    purple_debug_error("gntmedia","Unable to find a suitable element for the default audio source.\n");
    return 0;
  }
  gst_object_set_name(((GstObject *)src),"finchdefaultaudiosrc");
  return src;
}

static GstElement *create_default_audio_sink(PurpleMedia *media,const gchar *session_id,const gchar *participant)
{
  GstElement *sink;
  sink = gst_element_factory_make("gconfaudiosink",0);
  if (sink == ((GstElement *)((void *)0))) 
    sink = gst_element_factory_make("autoaudiosink",0);
  if (sink == ((GstElement *)((void *)0))) {
    purple_debug_error("gntmedia","Unable to find a suitable element for the default audio sink.\n");
    return 0;
  }
  return sink;
}
#endif  /* USE_VV */

void finch_media_manager_init()
{
#ifdef USE_VV
  PurpleMediaManager *manager = purple_media_manager_get();
  PurpleMediaElementInfo *default_audio_src = (g_object_new(purple_media_element_info_get_type(),"id","finchdefaultaudiosrc","name","Finch Default Audio Source","type",PURPLE_MEDIA_ELEMENT_AUDIO | PURPLE_MEDIA_ELEMENT_SRC | PURPLE_MEDIA_ELEMENT_ONE_SRC | PURPLE_MEDIA_ELEMENT_UNIQUE,"create-cb",create_default_audio_src,((void *)((void *)0))));
  PurpleMediaElementInfo *default_audio_sink = (g_object_new(purple_media_element_info_get_type(),"id","finchdefaultaudiosink","name","Finch Default Audio Sink","type",PURPLE_MEDIA_ELEMENT_AUDIO | PURPLE_MEDIA_ELEMENT_SINK | PURPLE_MEDIA_ELEMENT_ONE_SINK,"create-cb",create_default_audio_sink,((void *)((void *)0))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)manager),((GType )(20 << 2))))),"init-media",((GCallback )finch_new_media),0,0,((GConnectFlags )0));
  purple_cmd_register("call","",PURPLE_CMD_P_DEFAULT,PURPLE_CMD_FLAG_IM,0,call_cmd_cb,((const char *)(dgettext("pidgin","call: Make an audio call."))),0);
  purple_media_manager_set_ui_caps(manager,(PURPLE_MEDIA_CAPS_AUDIO | PURPLE_MEDIA_CAPS_AUDIO_SINGLE_DIRECTION));
  purple_debug_info("gntmedia","Registering media element types\n");
  purple_media_manager_set_active_element(manager,default_audio_src);
  purple_media_manager_set_active_element(manager,default_audio_sink);
#endif
}

void finch_media_manager_uninit()
{
#ifdef USE_VV
  PurpleMediaManager *manager = purple_media_manager_get();
  g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)manager),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )finch_new_media),0);
#endif
}
