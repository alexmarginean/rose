/**
 * @file gntstatus.c GNT Status API
 * @ingroup finch
 */
/* finch
 *
 * Finch is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <internal.h>
#include <gnt.h>
#include <gntbox.h>
#include <gntbutton.h>
#include <gntcombobox.h>
#include <gntentry.h>
#include <gntlabel.h>
#include <gntline.h>
#include <gnttree.h>
#include <gntutils.h>
#include "finch.h"
#include <notify.h>
#include <request.h>
#include "gntstatus.h"
static struct __unnamed_class___F0_L45_C8_unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__tree {
GntWidget *window;
GntWidget *tree;}statuses;
typedef struct __unnamed_class___F0_L51_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L410R__Pe___variable_name_unknown_scope_and_name__scope__saved__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__title__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__type__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__message__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__tree__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L84R__Pe___variable_name_unknown_scope_and_name__scope__hash {
PurpleSavedStatus *saved;
GntWidget *window;
GntWidget *title;
GntWidget *type;
GntWidget *message;
GntWidget *tree;
/* list of windows for substatuses */
GHashTable *hash;}EditStatus;
typedef struct __unnamed_class___F0_L62_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L247R__Pe___variable_name_unknown_scope_and_name__scope__account__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__CL275R__Pe___variable_name_unknown_scope_and_name__scope__type__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__message {
PurpleAccount *account;
const PurpleStatusType *type;
char *message;}RowInfo;
typedef struct __unnamed_class___F0_L69_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__type__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__message__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__EditStatusL436R__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__parent__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__RowInfoL437R__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__key {
GntWidget *window;
GntWidget *type;
GntWidget *message;
EditStatus *parent;
RowInfo *key;}EditSubStatus;
/* List of opened edit-status dialogs */
static GList *edits;

static void reset_status_window(GntWidget *widget,gpointer null)
{
  statuses.window = ((GntWidget *)((void *)0));
  statuses.tree = ((GntWidget *)((void *)0));
}

static void populate_statuses(GntTree *tree)
{
  GList *list;
  for (list = purple_savedstatuses_get_all(); list != 0; list = (list -> next)) {{
      PurpleSavedStatus *saved = (list -> data);
      const char *title;
      const char *type;
      const char *message;
      if (purple_savedstatus_is_transient(saved) != 0) 
        continue; 
      title = purple_savedstatus_get_title(saved);
      type = purple_primitive_get_name_from_type(purple_savedstatus_get_type(saved));
/* XXX: Strip possible markups */
      message = purple_savedstatus_get_message(saved);
      gnt_tree_add_row_last(tree,saved,gnt_tree_create_row(tree,title,type,message),0);
    }
  }
}

static void really_delete_status(PurpleSavedStatus *saved)
{
  GList *iter;
{
    for (iter = edits; iter != 0; iter = (iter -> next)) {
      EditStatus *edit = (iter -> data);
      if ((edit -> saved) == saved) {
        gnt_widget_destroy((edit -> window));
        break; 
      }
    }
  }
  if (statuses.tree != 0) 
    gnt_tree_remove(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)statuses.tree),gnt_tree_get_gtype()))),saved);
  purple_savedstatus_delete(purple_savedstatus_get_title(saved));
}

static void ask_before_delete(GntWidget *button,gpointer null)
{
  char *ask;
  PurpleSavedStatus *saved;
  do {
    if (statuses.tree != ((GntWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"statuses.tree != NULL");
      return ;
    };
  }while (0);
  saved = (gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)statuses.tree),gnt_tree_get_gtype())))));
  ask = g_strdup_printf(((const char *)(dgettext("pidgin","Are you sure you want to delete \"%s\""))),purple_savedstatus_get_title(saved));
  purple_request_action(saved,((const char *)(dgettext("pidgin","Delete Status"))),ask,0,0,0,0,0,saved,2,((const char *)(dgettext("pidgin","Delete"))),really_delete_status,((const char *)(dgettext("pidgin","Cancel"))),((void *)((void *)0)));
  g_free(ask);
}

static void use_savedstatus_cb(GntWidget *widget,gpointer null)
{
  do {
    if (statuses.tree != ((GntWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"statuses.tree != NULL");
      return ;
    };
  }while (0);
  purple_savedstatus_activate((gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)statuses.tree),gnt_tree_get_gtype()))))));
}

static void edit_savedstatus_cb(GntWidget *widget,gpointer null)
{
  do {
    if (statuses.tree != ((GntWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"statuses.tree != NULL");
      return ;
    };
  }while (0);
  finch_savedstatus_edit((gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)statuses.tree),gnt_tree_get_gtype()))))));
}

void finch_savedstatus_show_all()
{
  GntWidget *window;
  GntWidget *tree;
  GntWidget *box;
  GntWidget *button;
  int widths[] = {(25), (12), (35)};
  if (statuses.window != 0) {
    gnt_window_present(statuses.window);
    return ;
  }
  statuses.window = (window = gnt_box_new(0,1));
  gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),1);
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),((const char *)(dgettext("pidgin","Saved Statuses"))));
  gnt_box_set_fill(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),0);
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),GNT_ALIGN_MID);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),0);
/* XXX: Add some sorting function to sort alphabetically, perhaps */
  statuses.tree = (tree = gnt_tree_new_with_columns(3));
  gnt_tree_set_column_titles(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),((const char *)(dgettext("pidgin","Title"))),((const char *)(dgettext("pidgin","Type"))),((const char *)(dgettext("pidgin","Message"))));
  gnt_tree_set_show_title(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),1);
  gnt_tree_set_column_width_ratio(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),widths);
  gnt_widget_set_size(tree,72,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),tree);
  populate_statuses(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))));
  box = gnt_box_new(0,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),box);
  button = gnt_button_new(((const char *)(dgettext("pidgin","Use"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )use_savedstatus_cb),0,0,((GConnectFlags )0));
  button = gnt_button_new(((const char *)(dgettext("pidgin","Add"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  gnt_util_set_trigger_widget(tree,((cur_term -> type.Strings[77] != 0)?cur_term -> type.Strings[77] : ""),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )finch_savedstatus_edit),0,0,G_CONNECT_SWAPPED);
  button = gnt_button_new(((const char *)(dgettext("pidgin","Edit"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )edit_savedstatus_cb),0,0,((GConnectFlags )0));
  button = gnt_button_new(((const char *)(dgettext("pidgin","Delete"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  gnt_util_set_trigger_widget(tree,((cur_term -> type.Strings[59] != 0)?cur_term -> type.Strings[59] : ""),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )ask_before_delete),0,0,((GConnectFlags )0));
  button = gnt_button_new(((const char *)(dgettext("pidgin","Close"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )gnt_widget_destroy),window,0,G_CONNECT_SWAPPED);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"destroy",((GCallback )reset_status_window),0,0,((GConnectFlags )0));
  gnt_widget_show(window);
}

static void destroy_substatus_win(PurpleAccount *account,EditSubStatus *sub,gpointer null)
{
/* the "destroy" callback will remove entry from the hashtable */
  gnt_widget_destroy((sub -> window));
}

static void free_key(gpointer key,gpointer n)
{
  RowInfo *row = key;
  g_free((row -> message));
  g_free(key);
}

static void update_edit_list(GntWidget *widget,EditStatus *edit)
{
  edits = g_list_remove(edits,edit);
  purple_notify_close_with_handle(edit);
  g_hash_table_foreach((edit -> hash),((GHFunc )destroy_substatus_win),0);
  g_list_foreach(gnt_tree_get_rows(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(edit -> tree)),gnt_tree_get_gtype())))),free_key,0);
  g_free(edit);
}

static void set_substatuses(EditStatus *edit)
{
  GList *iter;
  for (iter = gnt_tree_get_rows(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(edit -> tree)),gnt_tree_get_gtype())))); iter != 0; iter = (iter -> next)) {
    RowInfo *key = (iter -> data);
    if (gnt_tree_get_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(edit -> tree)),gnt_tree_get_gtype()))),key) != 0) {
      purple_savedstatus_set_substatus((edit -> saved),(key -> account),(key -> type),(key -> message));
    }
  }
}

static void use_trans_status_cb(GntWidget *button,EditStatus *edit)
{
  const char *message;
  PurpleStatusPrimitive prim;
  PurpleSavedStatus *saved;
  message = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(edit -> message)),gnt_entry_get_gtype()))));
  prim = ((gint )((glong )(gnt_combo_box_get_selected_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(edit -> type)),gnt_combo_box_get_gtype())))))));
  saved = purple_savedstatus_find_transient_by_type_and_message(prim,message);
  if (saved == ((PurpleSavedStatus *)((void *)0))) {
    saved = purple_savedstatus_new(0,prim);
    edit -> saved = saved;
    set_substatuses(edit);
  }
  purple_savedstatus_set_message(saved,message);
  purple_savedstatus_activate(saved);
  gnt_widget_destroy((edit -> window));
}

static void save_savedstatus_cb(GntWidget *button,EditStatus *edit)
{
  const char *title;
  const char *message;
  PurpleStatusPrimitive prim;
  PurpleSavedStatus *find;
  title = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(edit -> title)),gnt_entry_get_gtype()))));
  message = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(edit -> message)),gnt_entry_get_gtype()))));
  if (!(message != 0) || !(( *message) != 0)) 
    message = ((const char *)((void *)0));
  prim = ((gint )((glong )(gnt_combo_box_get_selected_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(edit -> type)),gnt_combo_box_get_gtype())))))));
  if (!(title != 0) || !(( *title) != 0)) {
    purple_notify_message(edit,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Error"))),((const char *)(dgettext("pidgin","Invalid title"))),((const char *)(dgettext("pidgin","Please enter a non-empty title for the status."))),0,0);
    gnt_box_give_focus_to_child(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(edit -> window)),gnt_box_get_gtype()))),(edit -> title));
    return ;
  }
  find = purple_savedstatus_find(title);
  if ((find != 0) && (find != (edit -> saved))) {
    purple_notify_message(edit,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Error"))),((const char *)(dgettext("pidgin","Duplicate title"))),((const char *)(dgettext("pidgin","Please enter a different title for the status."))),0,0);
    gnt_box_give_focus_to_child(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(edit -> window)),gnt_box_get_gtype()))),(edit -> title));
    return ;
  }
  if ((edit -> saved) == ((PurpleSavedStatus *)((void *)0))) {
    edit -> saved = purple_savedstatus_new(title,prim);
    purple_savedstatus_set_message((edit -> saved),message);
    set_substatuses(edit);
    if (statuses.tree != 0) 
      gnt_tree_add_row_last(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)statuses.tree),gnt_tree_get_gtype()))),(edit -> saved),gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)statuses.tree),gnt_tree_get_gtype()))),title,purple_primitive_get_name_from_type(prim),message),0);
  }
  else {
    purple_savedstatus_set_title((edit -> saved),title);
    purple_savedstatus_set_type((edit -> saved),prim);
    purple_savedstatus_set_message((edit -> saved),message);
    if (statuses.tree != 0) {
      gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)statuses.tree),gnt_tree_get_gtype()))),(edit -> saved),0,title);
      gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)statuses.tree),gnt_tree_get_gtype()))),(edit -> saved),1,purple_primitive_get_name_from_type(prim));
      gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)statuses.tree),gnt_tree_get_gtype()))),(edit -> saved),2,message);
    }
  }
  if (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"use") != 0) 
    purple_savedstatus_activate((edit -> saved));
  gnt_widget_destroy((edit -> window));
}

static void add_substatus(EditStatus *edit,PurpleAccount *account)
{
  char *name;
  const char *type = (const char *)((void *)0);
  const char *message = (const char *)((void *)0);
  PurpleSavedStatusSub *sub = (PurpleSavedStatusSub *)((void *)0);
  RowInfo *key;
  if (!(edit != 0) || !((edit -> tree) != 0)) 
    return ;
  if ((edit -> saved) != 0) 
    sub = purple_savedstatus_get_substatus((edit -> saved),account);
  key = ((RowInfo *)(g_malloc0_n(1,(sizeof(RowInfo )))));
  key -> account = account;
  if (sub != 0) {
    key -> type = purple_savedstatus_substatus_get_type(sub);
    type = purple_status_type_get_name((key -> type));
    message = purple_savedstatus_substatus_get_message(sub);
    key -> message = g_strdup(message);
  }
  name = g_strdup_printf("%s (%s)",purple_account_get_username(account),purple_account_get_protocol_name(account));
  gnt_tree_add_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(edit -> tree)),gnt_tree_get_gtype()))),key,gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(edit -> tree)),gnt_tree_get_gtype()))),name,((type != 0)?type : ""),((message != 0)?message : "")),0,0);
  if (sub != 0) 
    gnt_tree_set_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(edit -> tree)),gnt_tree_get_gtype()))),key,1);
  g_free(name);
}

static void substatus_window_destroy_cb(GntWidget *window,EditSubStatus *sub)
{
  g_hash_table_remove(( *(sub -> parent)).hash,( *(sub -> key)).account);
  g_free(sub);
}

static void save_substatus_cb(GntWidget *widget,EditSubStatus *sub)
{
  PurpleSavedStatus *saved = ( *(sub -> parent)).saved;
  RowInfo *row = (sub -> key);
  const char *message;
  PurpleStatusType *type;
  type = (gnt_combo_box_get_selected_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(sub -> type)),gnt_combo_box_get_gtype())))));
  message = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(sub -> message)),gnt_entry_get_gtype()))));
  row -> type = type;
  row -> message = g_strdup(message);
/* Save the substatus if the savedstatus actually exists. */
  if (saved != 0) 
    purple_savedstatus_set_substatus(saved,(row -> account),type,message);
  gnt_tree_set_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)( *(sub -> parent)).tree),gnt_tree_get_gtype()))),row,1);
  gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)( *(sub -> parent)).tree),gnt_tree_get_gtype()))),row,1,purple_status_type_get_name(type));
  gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)( *(sub -> parent)).tree),gnt_tree_get_gtype()))),row,2,message);
  gnt_widget_destroy((sub -> window));
}

static gboolean popup_substatus(GntTree *tree,const char *key,EditStatus *edit)
{
  if ((key[0] == 32) && (key[1] == 0)) {
    EditSubStatus *sub;
    GntWidget *window;
    GntWidget *combo;
    GntWidget *entry;
    GntWidget *box;
    GntWidget *button;
    GntWidget *l;
    PurpleSavedStatusSub *substatus = (PurpleSavedStatusSub *)((void *)0);
    GList *iter;
    char *name;
    RowInfo *selected = (gnt_tree_get_selection_data(tree));
    PurpleAccount *account = (selected -> account);
    if (gnt_tree_get_choice(tree,selected) != 0) {
/* There was a savedstatus for this account. Now remove it. */
      g_free((selected -> message));
      selected -> type = ((const PurpleStatusType *)((void *)0));
      selected -> message = ((char *)((void *)0));
/* XXX: should we really be saving it right now? */
      purple_savedstatus_unset_substatus((edit -> saved),account);
      gnt_tree_change_text(tree,account,1,0);
      gnt_tree_change_text(tree,account,2,0);
      return 0;
    }
    if (g_hash_table_lookup((edit -> hash),account) != 0) 
      return 1;
    if ((edit -> saved) != 0) 
      substatus = purple_savedstatus_get_substatus((edit -> saved),account);
    sub = ((EditSubStatus *)(g_malloc0_n(1,(sizeof(EditSubStatus )))));
    sub -> parent = edit;
    sub -> key = selected;
    sub -> window = (window = gnt_box_new(0,1));
    gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),1);
/* XXX: a better title */
    gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),((const char *)(dgettext("pidgin","Substatus"))));
    gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),GNT_ALIGN_MID);
    box = gnt_box_new(0,0);
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),gnt_label_new(((const char *)(dgettext("pidgin","Account:")))));
    name = g_strdup_printf("%s (%s)",purple_account_get_username(account),purple_account_get_protocol_name(account));
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),gnt_label_new(name));
    g_free(name);
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),box);
    box = gnt_box_new(0,0);
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),(l = gnt_label_new(((const char *)(dgettext("pidgin","Status:"))))));
/* I don't like having to do this */
    gnt_widget_set_size(l,0,1);
    sub -> type = (combo = gnt_combo_box_new());
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),combo);
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),box);
    for (iter = purple_account_get_status_types(account); iter != 0; iter = (iter -> next)) {{
        PurpleStatusType *type = (iter -> data);
        if (!(purple_status_type_is_user_settable(type) != 0)) 
          continue; 
        gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype()))),type,purple_status_type_get_name(type));
      }
    }
    box = gnt_box_new(0,0);
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),gnt_label_new(((const char *)(dgettext("pidgin","Message:")))));
    sub -> message = (entry = gnt_entry_new(((substatus != 0)?purple_savedstatus_substatus_get_message(substatus) : ((const char *)((void *)0)))));
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),entry);
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),box);
    box = gnt_box_new(0,0);
    button = gnt_button_new(((const char *)(dgettext("pidgin","Cancel"))));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )gnt_widget_destroy),window,0,G_CONNECT_SWAPPED);
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
    button = gnt_button_new(((const char *)(dgettext("pidgin","Save"))));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )save_substatus_cb),sub,0,((GConnectFlags )0));
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),box);
    gnt_widget_show(window);
    g_hash_table_insert((edit -> hash),account,sub);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"destroy",((GCallback )substatus_window_destroy_cb),sub,0,((GConnectFlags )0));
    return 1;
  }
  return 0;
}

void finch_savedstatus_edit(PurpleSavedStatus *saved)
{
  EditStatus *edit;
  GntWidget *window;
  GntWidget *box;
  GntWidget *button;
  GntWidget *entry;
  GntWidget *combo;
  GntWidget *label;
  GntWidget *tree;
  PurpleStatusPrimitive prims[] = {(PURPLE_STATUS_AVAILABLE), (PURPLE_STATUS_AWAY), (PURPLE_STATUS_INVISIBLE), (PURPLE_STATUS_OFFLINE), (PURPLE_STATUS_UNSET)};
  PurpleStatusPrimitive current;
  GList *iter;
  int i;
  if (saved != 0) {
    GList *iter;
    for (iter = edits; iter != 0; iter = (iter -> next)) {
      edit = (iter -> data);
      if ((edit -> saved) == saved) 
        return ;
    }
  }
  edit = ((EditStatus *)(g_malloc0_n(1,(sizeof(EditStatus )))));
  edit -> saved = saved;
  edit -> window = (window = gnt_box_new(0,1));
  gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),1);
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),((const char *)(dgettext("pidgin","Edit Status"))));
  gnt_box_set_fill(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),1);
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),GNT_ALIGN_MID);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),0);
  edits = g_list_append(edits,edit);
/* Title */
  box = gnt_box_new(0,0);
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),GNT_ALIGN_LEFT);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),box);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),gnt_label_new(((const char *)(dgettext("pidgin","Title")))));
  edit -> title = (entry = gnt_entry_new(((saved != 0)?purple_savedstatus_get_title(saved) : ((const char *)((void *)0)))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),entry);
/* Type */
  box = gnt_box_new(0,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),box);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),(label = gnt_label_new(((const char *)(dgettext("pidgin","Status"))))));
  gnt_widget_set_size(label,0,1);
  edit -> type = (combo = gnt_combo_box_new());
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),combo);
  current = ((saved != 0)?purple_savedstatus_get_type(saved) : PURPLE_STATUS_UNSET);
  for (i = 0; prims[i] != PURPLE_STATUS_UNSET; i++) {
    gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype()))),((gpointer )((glong )prims[i])),purple_primitive_get_name_from_type(prims[i]));
    if (prims[i] == current) 
      gnt_combo_box_set_selected(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype()))),((gpointer )((glong )current)));
  }
/* Message */
  box = gnt_box_new(0,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),box);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),gnt_label_new(((const char *)(dgettext("pidgin","Message")))));
  edit -> message = (entry = gnt_entry_new(((saved != 0)?purple_savedstatus_get_message(saved) : ((const char *)((void *)0)))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),entry);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_line_new(0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_label_new(((const char *)(dgettext("pidgin","Use different status for following accounts")))));
  edit -> hash = g_hash_table_new(g_direct_hash,g_direct_equal);
  edit -> tree = (tree = gnt_tree_new_with_columns(3));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),tree);
  gnt_tree_set_show_title(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),1);
  gnt_tree_set_column_titles(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),((const char *)(dgettext("pidgin","Account"))),((const char *)(dgettext("pidgin","Status"))),((const char *)(dgettext("pidgin","Message"))));
  gnt_tree_set_col_width(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),0,30);
  gnt_tree_set_col_width(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),1,10);
  gnt_tree_set_col_width(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),2,30);
  for (iter = purple_accounts_get_all(); iter != 0; iter = (iter -> next)) {
    add_substatus(edit,(iter -> data));
  }
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"key_pressed",((GCallback )popup_substatus),edit,0,((GConnectFlags )0));
/* The buttons */
  box = gnt_box_new(0,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),box);
/* Use */
  button = gnt_button_new(((const char *)(dgettext("pidgin","Use"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )use_trans_status_cb),edit,0,((GConnectFlags )0));
/* Save */
  button = gnt_button_new(((const char *)(dgettext("pidgin","Save"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"use",0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )save_savedstatus_cb),edit,0,((GConnectFlags )0));
/* Save & Use */
  button = gnt_button_new(((const char *)(dgettext("pidgin","Save & Use"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"use",((gpointer )((gpointer )((glong )1))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )save_savedstatus_cb),edit,0,((GConnectFlags )0));
/* Cancel */
  button = gnt_button_new(((const char *)(dgettext("pidgin","Cancel"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )gnt_widget_destroy),window,0,G_CONNECT_SWAPPED);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"destroy",((GCallback )update_edit_list),edit,0,((GConnectFlags )0));
  gnt_widget_show(window);
}
