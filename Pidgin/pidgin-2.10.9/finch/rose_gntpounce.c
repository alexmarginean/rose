/**
 * @file gntpounce.c GNT Buddy Pounce API
 * @ingroup finch
 */
/* finch
 *
 * Finch is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include <internal.h>
#include <gnt.h>
#include <gntbox.h>
#include <gntbutton.h>
#include <gntcheckbox.h>
#include <gntcombobox.h>
#include <gntentry.h>
#include <gntlabel.h>
#include <gntline.h>
#include <gnttree.h>
#include <gntutils.h>
#include "finch.h"
#include "account.h"
#include "conversation.h"
#include "debug.h"
#include "notify.h"
#include "prpl.h"
#include "request.h"
#include "server.h"
#include "util.h"
#include "gntpounce.h"
typedef struct __unnamed_class___F0_L54_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L413R__Pe___variable_name_unknown_scope_and_name__scope__pounce__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L247R__Pe___variable_name_unknown_scope_and_name__scope__account__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__account_menu__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__buddy_entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__on_away__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__signon__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__signoff__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__away__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__away_return__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__idle__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__idle_return__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__typing__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__typed__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__stop_typing__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__message_recv__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__open_win__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__popup__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__popup_entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__send_msg__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__send_msg_entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__exec_cmd__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__exec_cmd_entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__play_sound__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__save_pounce__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__save_button {
/* Pounce data */
PurplePounce *pounce;
PurpleAccount *account;
/* The window */
GntWidget *window;
/* Pounce on Whom */
GntWidget *account_menu;
GntWidget *buddy_entry;
/* Pounce options */
GntWidget *on_away;
/* Pounce When Buddy... */
GntWidget *signon;
GntWidget *signoff;
GntWidget *away;
GntWidget *away_return;
GntWidget *idle;
GntWidget *idle_return;
GntWidget *typing;
GntWidget *typed;
GntWidget *stop_typing;
GntWidget *message_recv;
/* Action */
GntWidget *open_win;
GntWidget *popup;
GntWidget *popup_entry;
GntWidget *send_msg;
GntWidget *send_msg_entry;
GntWidget *exec_cmd;
GntWidget *exec_cmd_entry;
GntWidget *play_sound;
GntWidget *save_pounce;
/* Buttons */
GntWidget *save_button;}PurpleGntPounceDialog;
typedef struct __unnamed_class___F0_L99_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__tree__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__modify_button__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__delete_button {
GntWidget *window;
GntWidget *tree;
GntWidget *modify_button;
GntWidget *delete_button;}PouncesManager;
static PouncesManager *pounces_manager = (PouncesManager *)((void *)0);
/**************************************************************************
 * Callbacks
 **************************************************************************/

static gint delete_win_cb(GntWidget *w,PurpleGntPounceDialog *dialog)
{
  gnt_widget_destroy((dialog -> window));
  g_free(dialog);
  return 1;
}

static void cancel_cb(GntWidget *w,PurpleGntPounceDialog *dialog)
{
  gnt_widget_destroy((dialog -> window));
}

static void add_pounce_to_treeview(GntTree *tree,PurplePounce *pounce)
{
  PurpleAccount *account;
  const char *pouncer;
  const char *pouncee;
  account = purple_pounce_get_pouncer(pounce);
  pouncer = purple_account_get_username(account);
  pouncee = purple_pounce_get_pouncee(pounce);
  gnt_tree_add_row_last(tree,pounce,gnt_tree_create_row(tree,pouncer,pouncee),0);
}

static void populate_pounces_list(PouncesManager *dialog)
{
  GList *pounces;
  gnt_tree_remove_all(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> tree)),gnt_tree_get_gtype()))));
  for (pounces = purple_pounces_get_all_for_ui("gnt-purple"); pounces != ((GList *)((void *)0)); pounces = g_list_delete_link(pounces,pounces)) {
    add_pounce_to_treeview(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> tree)),gnt_tree_get_gtype()))),(pounces -> data));
  }
}

static void update_pounces()
{
/* Rebuild the pounces list if the pounces manager is open */
  if (pounces_manager != ((PouncesManager *)((void *)0))) {
    populate_pounces_list(pounces_manager);
  }
}

static void signed_on_off_cb(PurpleConnection *gc,gpointer user_data)
{
  update_pounces();
}

static void setup_buddy_list_suggestion(GntEntry *entry,gboolean offline)
{
  PurpleBlistNode *node = purple_blist_get_root();
  for (; node != 0; node = purple_blist_node_next(node,offline)) {
    if (!((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)) 
      continue; 
    gnt_entry_add_suggest(entry,purple_buddy_get_name(((PurpleBuddy *)node)));
  }
}

static void save_pounce_cb(GntWidget *w,PurpleGntPounceDialog *dialog)
{
  const char *name;
  const char *message;
  const char *command;
  const char *reason;
  PurplePounceEvent events = PURPLE_POUNCE_NONE;
  PurplePounceOption options = PURPLE_POUNCE_OPTION_NONE;
  name = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> buddy_entry)),gnt_entry_get_gtype()))));
  if (( *name) == 0) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Please enter a buddy to pounce."))),0,0,0);
    return ;
  }
/* Options */
  if (gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> on_away)),gnt_check_box_get_gtype())))) != 0) 
    options |= PURPLE_POUNCE_OPTION_AWAY;
/* Events */
  if (gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> signon)),gnt_check_box_get_gtype())))) != 0) 
    events |= PURPLE_POUNCE_SIGNON;
  if (gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> signoff)),gnt_check_box_get_gtype())))) != 0) 
    events |= PURPLE_POUNCE_SIGNOFF;
  if (gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> away)),gnt_check_box_get_gtype())))) != 0) 
    events |= PURPLE_POUNCE_AWAY;
  if (gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> away_return)),gnt_check_box_get_gtype())))) != 0) 
    events |= PURPLE_POUNCE_AWAY_RETURN;
  if (gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> idle)),gnt_check_box_get_gtype())))) != 0) 
    events |= PURPLE_POUNCE_IDLE;
  if (gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> idle_return)),gnt_check_box_get_gtype())))) != 0) 
    events |= PURPLE_POUNCE_IDLE_RETURN;
  if (gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> typing)),gnt_check_box_get_gtype())))) != 0) 
    events |= PURPLE_POUNCE_TYPING;
  if (gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> typed)),gnt_check_box_get_gtype())))) != 0) 
    events |= PURPLE_POUNCE_TYPED;
  if (gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> stop_typing)),gnt_check_box_get_gtype())))) != 0) 
    events |= PURPLE_POUNCE_TYPING_STOPPED;
  if (gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> message_recv)),gnt_check_box_get_gtype())))) != 0) 
    events |= PURPLE_POUNCE_MESSAGE_RECEIVED;
/* Data fields */
  message = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> send_msg_entry)),gnt_entry_get_gtype()))));
  command = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> exec_cmd_entry)),gnt_entry_get_gtype()))));
  reason = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> popup_entry)),gnt_entry_get_gtype()))));
  if (( *reason) == 0) 
    reason = ((const char *)((void *)0));
  if (( *message) == 0) 
    message = ((const char *)((void *)0));
  if (( *command) == 0) 
    command = ((const char *)((void *)0));
  if ((dialog -> pounce) == ((PurplePounce *)((void *)0))) {
    dialog -> pounce = purple_pounce_new("gnt-purple",(dialog -> account),name,events,options);
  }
  else {
    purple_pounce_set_events((dialog -> pounce),events);
    purple_pounce_set_options((dialog -> pounce),options);
    purple_pounce_set_pouncer((dialog -> pounce),(dialog -> account));
    purple_pounce_set_pouncee((dialog -> pounce),name);
  }
/* Actions */
  purple_pounce_action_set_enabled((dialog -> pounce),"open-window",gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> open_win)),gnt_check_box_get_gtype())))));
  purple_pounce_action_set_enabled((dialog -> pounce),"popup-notify",gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> popup)),gnt_check_box_get_gtype())))));
  purple_pounce_action_set_enabled((dialog -> pounce),"send-message",gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> send_msg)),gnt_check_box_get_gtype())))));
  purple_pounce_action_set_enabled((dialog -> pounce),"execute-command",gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> exec_cmd)),gnt_check_box_get_gtype())))));
  purple_pounce_action_set_enabled((dialog -> pounce),"play-beep",gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> play_sound)),gnt_check_box_get_gtype())))));
  purple_pounce_action_set_attribute((dialog -> pounce),"send-message","message",message);
  purple_pounce_action_set_attribute((dialog -> pounce),"execute-command","command",command);
  purple_pounce_action_set_attribute((dialog -> pounce),"popup-notify","reason",reason);
/* Set the defaults for next time. */
  purple_prefs_set_bool("/finch/pounces/default_actions/open-window",gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> open_win)),gnt_check_box_get_gtype())))));
  purple_prefs_set_bool("/finch/pounces/default_actions/popup-notify",gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> popup)),gnt_check_box_get_gtype())))));
  purple_prefs_set_bool("/finch/pounces/default_actions/send-message",gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> send_msg)),gnt_check_box_get_gtype())))));
  purple_prefs_set_bool("/finch/pounces/default_actions/execute-command",gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> exec_cmd)),gnt_check_box_get_gtype())))));
  purple_prefs_set_bool("/finch/pounces/default_actions/play-beep",gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> play_sound)),gnt_check_box_get_gtype())))));
  purple_pounce_set_save((dialog -> pounce),gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> save_pounce)),gnt_check_box_get_gtype())))));
  purple_pounce_set_pouncer((dialog -> pounce),((PurpleAccount *)(gnt_combo_box_get_selected_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> account_menu)),gnt_combo_box_get_gtype())))))));
  update_pounces();
  gnt_widget_destroy((dialog -> window));
}

void finch_pounce_editor_show(PurpleAccount *account,const char *name,PurplePounce *cur_pounce)
{
  PurpleGntPounceDialog *dialog;
  GntWidget *window;
  GntWidget *bbox;
  GntWidget *hbox;
  GntWidget *vbox;
  GntWidget *button;
  GntWidget *combo;
  GList *list;
  do {
    if (((cur_pounce != ((PurplePounce *)((void *)0))) || (account != ((PurpleAccount *)((void *)0)))) || (purple_accounts_get_all() != ((GList *)((void *)0)))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"(cur_pounce != NULL) || (account != NULL) || (purple_accounts_get_all() != NULL)");
      return ;
    };
  }while (0);
  dialog = ((PurpleGntPounceDialog *)(g_malloc0_n(1,(sizeof(PurpleGntPounceDialog )))));
  if (cur_pounce != ((PurplePounce *)((void *)0))) {
    dialog -> pounce = cur_pounce;
    dialog -> account = purple_pounce_get_pouncer(cur_pounce);
  }
  else if (account != ((PurpleAccount *)((void *)0))) {
    dialog -> pounce = ((PurplePounce *)((void *)0));
    dialog -> account = account;
  }
  else {
    GList *connections = purple_connections_get_all();
    PurpleConnection *gc;
    if (connections != ((GList *)((void *)0))) {
      gc = ((PurpleConnection *)(connections -> data));
      dialog -> account = purple_connection_get_account(gc);
    }
    else 
      dialog -> account = ( *purple_accounts_get_all()).data;
    dialog -> pounce = ((PurplePounce *)((void *)0));
  }
/* Create the window. */
  dialog -> window = (window = gnt_box_new(0,1));
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),0);
  gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),1);
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),GNT_ALIGN_LEFT);
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),((cur_pounce == ((PurplePounce *)((void *)0)))?((const char *)(dgettext("pidgin","New Buddy Pounce"))) : ((const char *)(dgettext("pidgin","Edit Buddy Pounce")))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"destroy",((GCallback )delete_win_cb),dialog,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_label_new_with_format(((const char *)(dgettext("pidgin","Pounce Who"))),GNT_TEXT_FLAG_BOLD));
/* Account: */
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_label_new(((const char *)(dgettext("pidgin","Account:")))));
  dialog -> account_menu = (combo = gnt_combo_box_new());
  list = purple_accounts_get_all();
  for (; list != 0; list = (list -> next)) {
    PurpleAccount *account;
    char *text;
    account = (list -> data);
    text = g_strdup_printf("%s (%s)",purple_account_get_username(account),purple_account_get_protocol_name(account));
    gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype()))),account,text);
    g_free(text);
  }
  if ((dialog -> account) != 0) 
    gnt_combo_box_set_selected(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype()))),(dialog -> account));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),combo);
/* Buddy: */
  hbox = gnt_box_new(0,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),gnt_label_new(((const char *)(dgettext("pidgin","Buddy name:")))));
  dialog -> buddy_entry = gnt_entry_new(0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),(dialog -> buddy_entry));
  setup_buddy_list_suggestion(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> buddy_entry)),gnt_entry_get_gtype()))),1);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),hbox);
  if (cur_pounce != ((PurplePounce *)((void *)0))) {
    gnt_entry_set_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> buddy_entry)),gnt_entry_get_gtype()))),purple_pounce_get_pouncee(cur_pounce));
  }
  else if (name != ((const char *)((void *)0))) {
    gnt_entry_set_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> buddy_entry)),gnt_entry_get_gtype()))),name);
  }
/* Create the event frame */
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_line_new(0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_label_new_with_format(((const char *)(dgettext("pidgin","Pounce When Buddy..."))),GNT_TEXT_FLAG_BOLD));
  dialog -> signon = gnt_check_box_new(((const char *)(dgettext("pidgin","Signs on"))));
  dialog -> signoff = gnt_check_box_new(((const char *)(dgettext("pidgin","Signs off"))));
  dialog -> away = gnt_check_box_new(((const char *)(dgettext("pidgin","Goes away"))));
  dialog -> away_return = gnt_check_box_new(((const char *)(dgettext("pidgin","Returns from away"))));
  dialog -> idle = gnt_check_box_new(((const char *)(dgettext("pidgin","Becomes idle"))));
  dialog -> idle_return = gnt_check_box_new(((const char *)(dgettext("pidgin","Is no longer idle"))));
  dialog -> typing = gnt_check_box_new(((const char *)(dgettext("pidgin","Starts typing"))));
  dialog -> typed = gnt_check_box_new(((const char *)(dgettext("pidgin","Pauses while typing"))));
  dialog -> stop_typing = gnt_check_box_new(((const char *)(dgettext("pidgin","Stops typing"))));
  dialog -> message_recv = gnt_check_box_new(((const char *)(dgettext("pidgin","Sends a message"))));
  hbox = gnt_box_new(1,0);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),2);
  vbox = gnt_box_new(0,1);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),vbox);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),(dialog -> signon));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),(dialog -> away));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),(dialog -> idle));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),(dialog -> typing));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),(dialog -> stop_typing));
  vbox = gnt_box_new(0,1);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),vbox);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),(dialog -> signoff));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),(dialog -> away_return));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),(dialog -> idle_return));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),(dialog -> typed));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),(dialog -> message_recv));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),hbox);
/* Create the "Action" frame. */
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_line_new(0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_label_new_with_format(((const char *)(dgettext("pidgin","Action"))),GNT_TEXT_FLAG_BOLD));
  dialog -> open_win = gnt_check_box_new(((const char *)(dgettext("pidgin","Open an IM window"))));
  dialog -> popup = gnt_check_box_new(((const char *)(dgettext("pidgin","Pop up a notification"))));
  dialog -> send_msg = gnt_check_box_new(((const char *)(dgettext("pidgin","Send a message"))));
  dialog -> exec_cmd = gnt_check_box_new(((const char *)(dgettext("pidgin","Execute a command"))));
  dialog -> play_sound = gnt_check_box_new(((const char *)(dgettext("pidgin","Play a sound"))));
  dialog -> send_msg_entry = gnt_entry_new(0);
  dialog -> exec_cmd_entry = gnt_entry_new(0);
  dialog -> popup_entry = gnt_entry_new(0);
  dialog -> exec_cmd_entry = gnt_entry_new(0);
  hbox = gnt_box_new(0,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),(dialog -> open_win));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),hbox);
  hbox = gnt_box_new(0,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),(dialog -> popup));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),(dialog -> popup_entry));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),hbox);
  hbox = gnt_box_new(0,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),(dialog -> send_msg));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),(dialog -> send_msg_entry));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),hbox);
  hbox = gnt_box_new(0,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),(dialog -> exec_cmd));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),(dialog -> exec_cmd_entry));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),hbox);
  hbox = gnt_box_new(0,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),(dialog -> play_sound));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),hbox);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_line_new(0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_label_new_with_format(((const char *)(dgettext("pidgin","Options"))),GNT_TEXT_FLAG_BOLD));
  dialog -> on_away = gnt_check_box_new(((const char *)(dgettext("pidgin","Pounce only when my status is not Available"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),(dialog -> on_away));
  dialog -> save_pounce = gnt_check_box_new(((const char *)(dgettext("pidgin","Recurring"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),(dialog -> save_pounce));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_line_new(0));
/* Now the button box! */
  bbox = gnt_box_new(0,0);
/* Cancel button */
  button = gnt_button_new(((const char *)(dgettext("pidgin","Cancel"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gnt_box_get_gtype()))),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )cancel_cb),dialog,0,((GConnectFlags )0));
/* Save button */
  dialog -> save_button = (button = gnt_button_new(((const char *)(dgettext("pidgin","Save")))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gnt_box_get_gtype()))),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )save_pounce_cb),dialog,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),bbox);
/* Set the values of stuff. */
  if (cur_pounce != ((PurplePounce *)((void *)0))) {
    PurplePounceEvent events = purple_pounce_get_events(cur_pounce);
    PurplePounceOption options = purple_pounce_get_options(cur_pounce);
    const char *value;
/* Options */
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> on_away)),gnt_check_box_get_gtype()))),(options & PURPLE_POUNCE_OPTION_AWAY));
/* Events */
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> signon)),gnt_check_box_get_gtype()))),(events & PURPLE_POUNCE_SIGNON));
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> signoff)),gnt_check_box_get_gtype()))),(events & PURPLE_POUNCE_SIGNOFF));
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> away)),gnt_check_box_get_gtype()))),(events & PURPLE_POUNCE_AWAY));
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> away_return)),gnt_check_box_get_gtype()))),(events & PURPLE_POUNCE_AWAY_RETURN));
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> idle)),gnt_check_box_get_gtype()))),(events & PURPLE_POUNCE_IDLE));
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> idle_return)),gnt_check_box_get_gtype()))),(events & PURPLE_POUNCE_IDLE_RETURN));
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> typing)),gnt_check_box_get_gtype()))),(events & PURPLE_POUNCE_TYPING));
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> typed)),gnt_check_box_get_gtype()))),(events & PURPLE_POUNCE_TYPED));
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> stop_typing)),gnt_check_box_get_gtype()))),(events & PURPLE_POUNCE_TYPING_STOPPED));
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> message_recv)),gnt_check_box_get_gtype()))),(events & PURPLE_POUNCE_MESSAGE_RECEIVED));
/* Actions */
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> open_win)),gnt_check_box_get_gtype()))),purple_pounce_action_is_enabled(cur_pounce,"open-window"));
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> popup)),gnt_check_box_get_gtype()))),purple_pounce_action_is_enabled(cur_pounce,"popup-notify"));
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> send_msg)),gnt_check_box_get_gtype()))),purple_pounce_action_is_enabled(cur_pounce,"send-message"));
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> exec_cmd)),gnt_check_box_get_gtype()))),purple_pounce_action_is_enabled(cur_pounce,"execute-command"));
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> play_sound)),gnt_check_box_get_gtype()))),purple_pounce_action_is_enabled(cur_pounce,"play-beep"));
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> save_pounce)),gnt_check_box_get_gtype()))),purple_pounce_get_save(cur_pounce));
    if ((value = purple_pounce_action_get_attribute(cur_pounce,"send-message","message")) != ((const char *)((void *)0))) {
      gnt_entry_set_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> send_msg_entry)),gnt_entry_get_gtype()))),value);
    }
    if ((value = purple_pounce_action_get_attribute(cur_pounce,"popup-notify","reason")) != ((const char *)((void *)0))) {
      gnt_entry_set_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> popup_entry)),gnt_entry_get_gtype()))),value);
    }
    if ((value = purple_pounce_action_get_attribute(cur_pounce,"execute-command","command")) != ((const char *)((void *)0))) {
      gnt_entry_set_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> exec_cmd_entry)),gnt_entry_get_gtype()))),value);
    }
  }
  else {
    PurpleBuddy *buddy = (PurpleBuddy *)((void *)0);
    if (name != ((const char *)((void *)0))) 
      buddy = purple_find_buddy(account,name);
/* Set some defaults */
    if (buddy == ((PurpleBuddy *)((void *)0))) {
      gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> signon)),gnt_check_box_get_gtype()))),1);
    }
    else {
      if (!(((buddy != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0))) {
        gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> signon)),gnt_check_box_get_gtype()))),1);
      }
      else {
        gboolean default_set = 0;
        PurplePresence *presence = purple_buddy_get_presence(buddy);
        if (purple_presence_is_idle(presence) != 0) {
          gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> idle_return)),gnt_check_box_get_gtype()))),1);
          default_set = 1;
        }
        if (!(purple_presence_is_available(presence) != 0)) {
          gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> away_return)),gnt_check_box_get_gtype()))),1);
          default_set = 1;
        }
        if (!(default_set != 0)) {
          gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> signon)),gnt_check_box_get_gtype()))),1);
        }
      }
    }
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> open_win)),gnt_check_box_get_gtype()))),purple_prefs_get_bool("/finch/pounces/default_actions/open-window"));
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> popup)),gnt_check_box_get_gtype()))),purple_prefs_get_bool("/finch/pounces/default_actions/popup-notify"));
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> send_msg)),gnt_check_box_get_gtype()))),purple_prefs_get_bool("/finch/pounces/default_actions/send-message"));
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> exec_cmd)),gnt_check_box_get_gtype()))),purple_prefs_get_bool("/finch/pounces/default_actions/execute-command"));
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> play_sound)),gnt_check_box_get_gtype()))),purple_prefs_get_bool("/finch/pounces/default_actions/play-beep"));
  }
  gnt_widget_show(window);
}

static gboolean pounces_manager_destroy_cb(GntWidget *widget,gpointer user_data)
{
  PouncesManager *dialog = user_data;
  dialog -> window = ((GntWidget *)((void *)0));
  finch_pounces_manager_hide();
  return 0;
}

static void pounces_manager_add_cb(GntButton *button,gpointer user_data)
{
  if (purple_accounts_get_all() == ((GList *)((void *)0))) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Cannot create pounce"))),((const char *)(dgettext("pidgin","You do not have any accounts."))),((const char *)(dgettext("pidgin","You must create an account first before you can create a pounce."))),0,0);
    return ;
  }
  finch_pounce_editor_show(0,0,0);
}

static void pounces_manager_modify_cb(GntButton *button,gpointer user_data)
{
  PouncesManager *dialog = user_data;
  PurplePounce *pounce = (gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> tree)),gnt_tree_get_gtype())))));
  if (pounce != 0) 
    finch_pounce_editor_show(0,0,pounce);
}

static void pounces_manager_delete_confirm_cb(PurplePounce *pounce)
{
  gnt_tree_remove(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(pounces_manager -> tree)),gnt_tree_get_gtype()))),pounce);
  purple_request_close_with_handle(pounce);
  purple_pounce_destroy(pounce);
}

static void pounces_manager_delete_cb(GntButton *button,gpointer user_data)
{
  PouncesManager *dialog = user_data;
  PurplePounce *pounce;
  PurpleAccount *account;
  const char *pouncer;
  const char *pouncee;
  char *buf;
  pounce = ((PurplePounce *)(gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> tree)),gnt_tree_get_gtype()))))));
  if (pounce == ((PurplePounce *)((void *)0))) 
    return ;
  account = purple_pounce_get_pouncer(pounce);
  pouncer = purple_account_get_username(account);
  pouncee = purple_pounce_get_pouncee(pounce);
  buf = g_strdup_printf(((const char *)(dgettext("pidgin","Are you sure you want to delete the pounce on %s for %s\?"))),pouncee,pouncer);
  purple_request_action(pounce,0,buf,0,0,account,pouncee,0,pounce,2,((const char *)(dgettext("pidgin","Delete"))),pounces_manager_delete_confirm_cb,((const char *)(dgettext("pidgin","Cancel"))),((void *)((void *)0)));
  g_free(buf);
}

static void pounces_manager_close_cb(GntButton *button,gpointer user_data)
{
  finch_pounces_manager_hide();
}

void finch_pounces_manager_show()
{
  PouncesManager *dialog;
  GntWidget *bbox;
  GntWidget *button;
  GntWidget *tree;
  GntWidget *win;
  if (pounces_manager != ((PouncesManager *)((void *)0))) {
    gnt_window_present((pounces_manager -> window));
    return ;
  }
  pounces_manager = (dialog = ((PouncesManager *)(g_malloc0_n(1,(sizeof(PouncesManager ))))));
  dialog -> window = (win = gnt_box_new(0,1));
  gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),1);
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),((const char *)(dgettext("pidgin","Buddy Pounces"))));
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"destroy",((GCallback )pounces_manager_destroy_cb),dialog,0,((GConnectFlags )0));
/* List of saved buddy pounces */
  dialog -> tree = (tree = ((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(gnt_tree_new_with_columns(2))),gnt_widget_get_gtype()))));
  gnt_tree_set_column_titles(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),"Account","Pouncee",((void *)((void *)0)));
  gnt_tree_set_show_title(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),1);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),tree);
/* Button box. */
  bbox = gnt_box_new(0,0);
/* Add button */
  button = gnt_button_new(((const char *)(dgettext("pidgin","Add"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gnt_box_get_gtype()))),button);
  gnt_util_set_trigger_widget(tree,((cur_term -> type.Strings[77] != 0)?cur_term -> type.Strings[77] : ""),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )pounces_manager_add_cb),dialog,0,((GConnectFlags )0));
/* Modify button */
  button = gnt_button_new(((const char *)(dgettext("pidgin","Modify"))));
  dialog -> modify_button = button;
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gnt_box_get_gtype()))),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )pounces_manager_modify_cb),dialog,0,((GConnectFlags )0));
/* Delete button */
  button = gnt_button_new(((const char *)(dgettext("pidgin","Delete"))));
  dialog -> delete_button = button;
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gnt_box_get_gtype()))),button);
  gnt_util_set_trigger_widget(tree,((cur_term -> type.Strings[59] != 0)?cur_term -> type.Strings[59] : ""),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )pounces_manager_delete_cb),dialog,0,((GConnectFlags )0));
/* Close button */
  button = gnt_button_new(((const char *)(dgettext("pidgin","Close"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gnt_box_get_gtype()))),button);
  gnt_widget_show(button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )pounces_manager_close_cb),dialog,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),bbox);
  gnt_widget_show(win);
  populate_pounces_list(pounces_manager);
}

void finch_pounces_manager_hide()
{
  if (pounces_manager == ((PouncesManager *)((void *)0))) 
    return ;
  if ((pounces_manager -> window) != ((GntWidget *)((void *)0))) 
    gnt_widget_destroy((pounces_manager -> window));
  purple_signals_disconnect_by_handle(pounces_manager);
  g_free(pounces_manager);
  pounces_manager = ((PouncesManager *)((void *)0));
}

static void pounce_cb(PurplePounce *pounce,PurplePounceEvent events,void *data)
{
  PurpleConversation *conv;
  PurpleAccount *account;
  PurpleBuddy *buddy;
  const char *pouncee;
  const char *alias;
  pouncee = purple_pounce_get_pouncee(pounce);
  account = purple_pounce_get_pouncer(pounce);
  buddy = purple_find_buddy(account,pouncee);
  if (buddy != ((PurpleBuddy *)((void *)0))) {
    alias = purple_buddy_get_alias(buddy);
    if (alias == ((const char *)((void *)0))) 
      alias = pouncee;
  }
  else 
    alias = pouncee;
  if (purple_pounce_action_is_enabled(pounce,"open-window") != 0) {
    if (!(purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,pouncee,account) != 0)) 
      purple_conversation_new(PURPLE_CONV_TYPE_IM,account,pouncee);
  }
  if (purple_pounce_action_is_enabled(pounce,"popup-notify") != 0) {
    char *tmp = (char *)((void *)0);
    const char *name_shown;
    const char *reason;
    struct __unnamed_class___F0_L813_C3_L444R_variable_declaration__variable_type_L441R_variable_name_L444R__scope__event__DELIMITER__L444R_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_L444R__scope__format {
    PurplePounceEvent event;
    const char *format;}messages[] = {{(PURPLE_POUNCE_TYPING), ((const char *)(dgettext("pidgin","%s has started typing to you (%s)")))}, {(PURPLE_POUNCE_TYPED), ((const char *)(dgettext("pidgin","%s has paused while typing to you (%s)")))}, {(PURPLE_POUNCE_SIGNON), ((const char *)(dgettext("pidgin","%s has signed on (%s)")))}, {(PURPLE_POUNCE_IDLE_RETURN), ((const char *)(dgettext("pidgin","%s has returned from being idle (%s)")))}, {(PURPLE_POUNCE_AWAY_RETURN), ((const char *)(dgettext("pidgin","%s has returned from being away (%s)")))}, {(PURPLE_POUNCE_TYPING_STOPPED), ((const char *)(dgettext("pidgin","%s has stopped typing to you (%s)")))}, {(PURPLE_POUNCE_SIGNOFF), ((const char *)(dgettext("pidgin","%s has signed off (%s)")))}, {(PURPLE_POUNCE_IDLE), ((const char *)(dgettext("pidgin","%s has become idle (%s)")))}, {(PURPLE_POUNCE_AWAY), ((const char *)(dgettext("pidgin","%s has gone away. (%s)")))}, {(PURPLE_POUNCE_MESSAGE_RECEIVED), ((const char *)(dgettext("pidgin","%s has sent you a message. (%s)")))}, {(0), ((const char *)((void *)0))}};
    int i;
    reason = purple_pounce_action_get_attribute(pounce,"popup-notify","reason");
{
/*
		 * Here we place the protocol name in the pounce dialog to lessen
		 * confusion about what protocol a pounce is for.
		 */
      for (i = 0; messages[i].format != ((const char *)((void *)0)); i++) {
        if ((messages[i].event & events) != 0U) {
          tmp = g_strdup_printf(messages[i].format,alias,purple_account_get_protocol_name(account));
          break; 
        }
      }
    }
    if (tmp == ((char *)((void *)0))) 
      tmp = g_strdup(((const char *)(dgettext("pidgin","Unknown pounce event. Please report this!"))));
/*
		 * Ok here is where I change the second argument, title, from
		 * NULL to the account alias if we have it or the account
		 * name if that's all we have
		 */
    if ((name_shown = purple_account_get_alias(account)) == ((const char *)((void *)0))) 
      name_shown = purple_account_get_username(account);
    if (reason == ((const char *)((void *)0))) {
      purple_notify_message(0,PURPLE_NOTIFY_MSG_INFO,name_shown,tmp,purple_date_format_full(0),0,0);
    }
    else {
      char *tmp2 = g_strdup_printf("%s\n\n%s",reason,purple_date_format_full(0));
      purple_notify_message(0,PURPLE_NOTIFY_MSG_INFO,name_shown,tmp,tmp2,0,0);
      g_free(tmp2);
    }
    g_free(tmp);
  }
  if (purple_pounce_action_is_enabled(pounce,"send-message") != 0) {
    const char *message;
    message = purple_pounce_action_get_attribute(pounce,"send-message","message");
    if (message != ((const char *)((void *)0))) {
      conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,pouncee,account);
      if (conv == ((PurpleConversation *)((void *)0))) 
        conv = purple_conversation_new(PURPLE_CONV_TYPE_IM,account,pouncee);
      purple_conversation_write(conv,0,message,PURPLE_MESSAGE_SEND,time(0));
      serv_send_im(purple_account_get_connection(account),((char *)pouncee),((char *)message),0);
    }
  }
  if (purple_pounce_action_is_enabled(pounce,"execute-command") != 0) {
    const char *command;
    command = purple_pounce_action_get_attribute(pounce,"execute-command","command");
    if (command != ((const char *)((void *)0))) {
      char *localecmd = g_locale_from_utf8(command,(-1),0,0,0);
      if (localecmd != ((char *)((void *)0))) {
        int pid = fork();
        if (pid == 0) {
          char *args[4UL];
          args[0] = "sh";
          args[1] = "-c";
          args[2] = ((char *)localecmd);
          args[3] = ((char *)((void *)0));
          execvp(args[0],args);
          _exit(0);
        }
        g_free(localecmd);
      }
    }
  }
  if (purple_pounce_action_is_enabled(pounce,"play-beep") != 0) {
    beep();
  }
}

static void free_pounce(PurplePounce *pounce)
{
  update_pounces();
}

static void new_pounce(PurplePounce *pounce)
{
  purple_pounce_action_register(pounce,"open-window");
  purple_pounce_action_register(pounce,"popup-notify");
  purple_pounce_action_register(pounce,"send-message");
  purple_pounce_action_register(pounce,"execute-command");
  purple_pounce_action_register(pounce,"play-beep");
  update_pounces();
}

void *finch_pounces_get_handle()
{
  static int handle;
  return (&handle);
}

void finch_pounces_init()
{
  purple_pounces_register_handler("gnt-purple",pounce_cb,new_pounce,free_pounce);
  purple_prefs_add_none("/finch/pounces");
  purple_prefs_add_none("/finch/pounces/default_actions");
  purple_prefs_add_bool("/finch/pounces/default_actions/open-window",0);
  purple_prefs_add_bool("/finch/pounces/default_actions/popup-notify",1);
  purple_prefs_add_bool("/finch/pounces/default_actions/send-message",0);
  purple_prefs_add_bool("/finch/pounces/default_actions/execute-command",0);
  purple_prefs_add_bool("/finch/pounces/default_actions/play-beep",0);
  purple_prefs_add_none("/finch/pounces/dialog");
  purple_signal_connect(purple_connections_get_handle(),"signed-on",finch_pounces_get_handle(),((PurpleCallback )signed_on_off_cb),0);
  purple_signal_connect(purple_connections_get_handle(),"signed-off",finch_pounces_get_handle(),((PurpleCallback )signed_on_off_cb),0);
}
/* XXX: There's no such thing in pidgin. Perhaps there should be? */

void finch_pounces_uninit()
{
  purple_pounces_unregister_handler("gnt-purple");
  purple_signals_disconnect_by_handle(finch_pounces_get_handle());
}
