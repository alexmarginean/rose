/**
 * @file gntft.c GNT File Transfer UI
 * @ingroup finch
 */
/* finch
 *
 * Finch is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <internal.h>
#include "finch.h"
#include <gnt.h>
#include <gntbox.h>
#include <gntbutton.h>
#include <gntcheckbox.h>
#include <gntlabel.h>
#include <gnttree.h>
#include "debug.h"
#include "notify.h"
#include "ft.h"
#include "prpl.h"
#include "util.h"
#include "gntft.h"
#include "prefs.h"
#define FINCHXFER(xfer) \
	(PurpleGntXferUiData *)FINCH_GET_DATA(xfer)
typedef struct __unnamed_class___F0_L48_C9_unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__keep_open__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__auto_clear__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_ginti__typedef_declaration_variable_name_unknown_scope_and_name__scope__num_transfers__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__tree__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__remove_button__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__stop_button__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__close_button {
gboolean keep_open;
gboolean auto_clear;
gint num_transfers;
GntWidget *window;
GntWidget *tree;
GntWidget *remove_button;
GntWidget *stop_button;
GntWidget *close_button;}PurpleGntXferDialog;
static PurpleGntXferDialog *xfer_dialog = (PurpleGntXferDialog *)((void *)0);
typedef struct __unnamed_class___F0_L64_C9_unknown_scope_and_name_variable_declaration__variable_type_L5R_variable_name_unknown_scope_and_name__scope__last_updated_time__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__in_list__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__name__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__notified {
time_t last_updated_time;
gboolean in_list;
char *name;
/* Has the completion of the transfer been notified? */
gboolean notified;}PurpleGntXferUiData;
enum __unnamed_enum___F0_L74_C1_COLUMN_PROGRESS__COMMA__COLUMN_FILENAME__COMMA__COLUMN_SIZE__COMMA__COLUMN_SPEED__COMMA__COLUMN_REMAINING__COMMA__COLUMN_STATUS__COMMA__NUM_COLUMNS {COLUMN_PROGRESS,COLUMN_FILENAME,COLUMN_SIZE,COLUMN_SPEED,COLUMN_REMAINING,COLUMN_STATUS,NUM_COLUMNS};
/**************************************************************************
 * Utility Functions
 **************************************************************************/

static void update_title_progress()
{
  GList *list;
  int num_active_xfers = 0;
  guint64 total_bytes_xferred = 0;
  guint64 total_file_size = 0;
  if ((xfer_dialog == ((PurpleGntXferDialog *)((void *)0))) || ((xfer_dialog -> window) == ((GntWidget *)((void *)0)))) 
    return ;
/* Find all active transfers */
  for (list = gnt_tree_get_rows(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(xfer_dialog -> tree)),gnt_tree_get_gtype())))); list != 0; list = (list -> next)) {
    PurpleXfer *xfer = (PurpleXfer *)(list -> data);
    if ((purple_xfer_get_status(xfer)) == PURPLE_XFER_STATUS_STARTED) {
      num_active_xfers++;
      total_bytes_xferred += purple_xfer_get_bytes_sent(xfer);
      total_file_size += purple_xfer_get_size(xfer);
    }
  }
/* Update the title */
  if (num_active_xfers > 0) {
    gchar *title;
    int total_pct = 0;
    if (total_file_size > 0) {
      total_pct = ((100 * total_bytes_xferred) / total_file_size);
    }
    title = g_strdup_printf((ngettext("File Transfers - %d%% of %d file","File Transfers - %d%% of %d files",num_active_xfers)),total_pct,num_active_xfers);
    gnt_screen_rename_widget((xfer_dialog -> window),title);
    g_free(title);
  }
  else {
    gnt_screen_rename_widget((xfer_dialog -> window),((const char *)(dgettext("pidgin","File Transfers"))));
  }
}
/**************************************************************************
 * Callbacks
 **************************************************************************/

static void toggle_keep_open_cb(GntWidget *w)
{
  xfer_dialog -> keep_open = !((xfer_dialog -> keep_open) != 0);
  purple_prefs_set_bool("/finch/filetransfer/keep_open",(xfer_dialog -> keep_open));
}

static void toggle_clear_finished_cb(GntWidget *w)
{
  xfer_dialog -> auto_clear = !((xfer_dialog -> auto_clear) != 0);
  purple_prefs_set_bool("/finch/filetransfer/clear_finished",(xfer_dialog -> auto_clear));
  if ((xfer_dialog -> auto_clear) != 0) {
    GList *iter = purple_xfers_get_all();
    while(iter != 0){
      PurpleXfer *xfer = (iter -> data);
      iter = (iter -> next);
      if ((purple_xfer_is_completed(xfer) != 0) || (purple_xfer_is_canceled(xfer) != 0)) 
        finch_xfer_dialog_remove_xfer(xfer);
    }
  }
}

static void remove_button_cb(GntButton *button)
{
  PurpleXfer *selected_xfer = (gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(xfer_dialog -> tree)),gnt_tree_get_gtype())))));
  if ((selected_xfer != 0) && ((purple_xfer_is_completed(selected_xfer) != 0) || (purple_xfer_is_canceled(selected_xfer) != 0))) {
    finch_xfer_dialog_remove_xfer(selected_xfer);
  }
}

static void stop_button_cb(GntButton *button)
{
  PurpleXfer *selected_xfer = (gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(xfer_dialog -> tree)),gnt_tree_get_gtype())))));
  PurpleXferStatusType status;
  if (!(selected_xfer != 0)) 
    return ;
  status = purple_xfer_get_status(selected_xfer);
  if (((status != PURPLE_XFER_STATUS_CANCEL_LOCAL) && (status != PURPLE_XFER_STATUS_CANCEL_REMOTE)) && (status != PURPLE_XFER_STATUS_DONE)) 
    purple_xfer_cancel_local(selected_xfer);
}
/**************************************************************************
 * Dialog Building Functions
 **************************************************************************/

void finch_xfer_dialog_new()
{
  GList *iter;
  GntWidget *window;
  GntWidget *bbox;
  GntWidget *button;
  GntWidget *checkbox;
  GntWidget *tree;
  int widths[] = {(8), (12), (8), (8), (8), (8), (-1)};
  if (!(xfer_dialog != 0)) 
    xfer_dialog = ((PurpleGntXferDialog *)(g_malloc0_n(1,(sizeof(PurpleGntXferDialog )))));
  xfer_dialog -> keep_open = purple_prefs_get_bool("/finch/filetransfer/keep_open");
  xfer_dialog -> auto_clear = purple_prefs_get_bool("/finch/filetransfer/clear_finished");
/* Create the window. */
  xfer_dialog -> window = (window = gnt_box_new(0,1));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"destroy",((GCallback )finch_xfer_dialog_destroy),0,0,((GConnectFlags )0));
  gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),1);
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),((const char *)(dgettext("pidgin","File Transfers"))));
  gnt_box_set_fill(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),1);
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),GNT_ALIGN_MID);
  xfer_dialog -> tree = (tree = gnt_tree_new_with_columns(NUM_COLUMNS));
  gnt_tree_set_column_titles(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),((const char *)(dgettext("pidgin","Progress"))),((const char *)(dgettext("pidgin","Filename"))),((const char *)(dgettext("pidgin","Size"))),((const char *)(dgettext("pidgin","Speed"))),((const char *)(dgettext("pidgin","Remaining"))),((const char *)(dgettext("pidgin","Status"))));
  gnt_tree_set_column_width_ratio(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),widths);
  gnt_tree_set_column_resizable(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),COLUMN_PROGRESS,0);
  gnt_tree_set_column_resizable(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),COLUMN_SIZE,0);
  gnt_tree_set_column_resizable(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),COLUMN_SPEED,0);
  gnt_tree_set_column_resizable(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),COLUMN_REMAINING,0);
  gnt_widget_set_size(tree,70,-1);
  gnt_tree_set_show_title(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),1);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),tree);
  checkbox = gnt_check_box_new(((const char *)(dgettext("pidgin","Close this window when all transfers finish"))));
  gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)checkbox),gnt_check_box_get_gtype()))),!((xfer_dialog -> keep_open) != 0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)checkbox),((GType )(20 << 2))))),"toggled",((GCallback )toggle_keep_open_cb),0,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),checkbox);
  checkbox = gnt_check_box_new(((const char *)(dgettext("pidgin","Clear finished transfers"))));
  gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)checkbox),gnt_check_box_get_gtype()))),(xfer_dialog -> auto_clear));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)checkbox),((GType )(20 << 2))))),"toggled",((GCallback )toggle_clear_finished_cb),0,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),checkbox);
  bbox = gnt_box_new(0,0);
  xfer_dialog -> remove_button = (button = gnt_button_new(((const char *)(dgettext("pidgin","Remove")))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )remove_button_cb),0,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gnt_box_get_gtype()))),button);
  xfer_dialog -> stop_button = (button = gnt_button_new(((const char *)(dgettext("pidgin","Stop")))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )stop_button_cb),0,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gnt_box_get_gtype()))),button);
  xfer_dialog -> close_button = (button = gnt_button_new(((const char *)(dgettext("pidgin","Close")))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )finch_xfer_dialog_destroy),0,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gnt_box_get_gtype()))),button);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),bbox);
  for (iter = purple_xfers_get_all(); iter != 0; iter = (iter -> next)) {
    PurpleXfer *xfer = (PurpleXfer *)(iter -> data);
    PurpleGntXferUiData *data = (PurpleGntXferUiData *)(xfer -> ui_data);
    if ((data -> in_list) != 0) {
      finch_xfer_dialog_add_xfer(xfer);
      finch_xfer_dialog_update_xfer(xfer);
      gnt_tree_set_selected(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),xfer);
    }
  }
  gnt_widget_show((xfer_dialog -> window));
}

void finch_xfer_dialog_destroy()
{
  gnt_widget_destroy((xfer_dialog -> window));
  g_free(xfer_dialog);
  xfer_dialog = ((PurpleGntXferDialog *)((void *)0));
}

void finch_xfer_dialog_show()
{
  if (xfer_dialog == ((PurpleGntXferDialog *)((void *)0))) 
    finch_xfer_dialog_new();
  else 
    gnt_window_present((xfer_dialog -> window));
}

void finch_xfer_dialog_add_xfer(PurpleXfer *xfer)
{
  PurpleGntXferUiData *data;
  PurpleXferType type;
  char *size_str;
  char *remaining_str;
  char *lfilename;
  char *utf8;
  do {
    if (xfer_dialog != ((PurpleGntXferDialog *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer_dialog != NULL");
      return ;
    };
  }while (0);
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  purple_xfer_ref(xfer);
  data = ((PurpleGntXferUiData *)(xfer -> ui_data));
  data -> in_list = 1;
  finch_xfer_dialog_show();
  data -> last_updated_time = 0;
  type = purple_xfer_get_type(xfer);
  size_str = purple_str_size_to_units(purple_xfer_get_size(xfer));
  remaining_str = purple_str_size_to_units(purple_xfer_get_bytes_remaining(xfer));
  lfilename = g_path_get_basename(purple_xfer_get_local_filename(xfer));
  utf8 = g_filename_to_utf8(lfilename,(-1),0,0,0);
  g_free(lfilename);
  lfilename = utf8;
  gnt_tree_add_row_last(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(xfer_dialog -> tree)),gnt_tree_get_gtype()))),xfer,gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(xfer_dialog -> tree)),gnt_tree_get_gtype()))),"0.0",((type == PURPLE_XFER_RECEIVE)?purple_xfer_get_filename(xfer) : lfilename),size_str,"0.0","",((const char *)(dgettext("pidgin","Waiting for transfer to begin")))),0);
  g_free(lfilename);
  g_free(size_str);
  g_free(remaining_str);
  xfer_dialog -> num_transfers++;
  update_title_progress();
}

void finch_xfer_dialog_remove_xfer(PurpleXfer *xfer)
{
  PurpleGntXferUiData *data;
  do {
    if (xfer_dialog != ((PurpleGntXferDialog *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer_dialog != NULL");
      return ;
    };
  }while (0);
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  data = ((PurpleGntXferUiData *)(xfer -> ui_data));
  if (data == ((PurpleGntXferUiData *)((void *)0))) 
    return ;
  if (!((data -> in_list) != 0)) 
    return ;
  data -> in_list = 0;
  gnt_tree_remove(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(xfer_dialog -> tree)),gnt_tree_get_gtype()))),xfer);
  xfer_dialog -> num_transfers--;
  if (((xfer_dialog -> num_transfers) == 0) && !((xfer_dialog -> keep_open) != 0)) 
    finch_xfer_dialog_destroy();
  else 
    update_title_progress();
  purple_xfer_unref(xfer);
}

void finch_xfer_dialog_cancel_xfer(PurpleXfer *xfer)
{
  PurpleGntXferUiData *data;
  const gchar *status;
  do {
    if (xfer_dialog != ((PurpleGntXferDialog *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer_dialog != NULL");
      return ;
    };
  }while (0);
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  data = ((PurpleGntXferUiData *)(xfer -> ui_data));
  if (data == ((PurpleGntXferUiData *)((void *)0))) 
    return ;
  if (!((data -> in_list) != 0)) 
    return ;
  if (((purple_xfer_get_status(xfer)) == PURPLE_XFER_STATUS_CANCEL_LOCAL) && ((xfer_dialog -> auto_clear) != 0)) {
    finch_xfer_dialog_remove_xfer(xfer);
    return ;
  }
  update_title_progress();
  if (purple_xfer_is_canceled(xfer) != 0) 
    status = ((const char *)(dgettext("pidgin","Cancelled")));
  else 
    status = ((const char *)(dgettext("pidgin","Failed")));
  gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(xfer_dialog -> tree)),gnt_tree_get_gtype()))),xfer,COLUMN_STATUS,status);
}

void finch_xfer_dialog_update_xfer(PurpleXfer *xfer)
{
  PurpleGntXferUiData *data;
  char *size_str;
  char *remaining_str;
  time_t current_time;
  char prog_str[5UL];
  double kb_sent;
  double kbps = 0.0;
  time_t elapsed;
  time_t now;
  char *kbsec;
  gboolean send;
  if ((now = purple_xfer_get_end_time(xfer)) == 0) 
    now = time(0);
  kb_sent = ((purple_xfer_get_bytes_sent(xfer)) / 1024.0);
  elapsed = ((purple_xfer_get_start_time(xfer) > 0)?(now - purple_xfer_get_start_time(xfer)) : 0);
  kbps = ((elapsed > 0)?(kb_sent / elapsed) : 0);
  do {
    if (xfer_dialog != ((PurpleGntXferDialog *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer_dialog != NULL");
      return ;
    };
  }while (0);
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  if ((data = ((PurpleGntXferUiData *)(xfer -> ui_data))) == ((PurpleGntXferUiData *)((void *)0))) 
    return ;
  if (((data -> in_list) == 0) || ((data -> notified) != 0)) 
    return ;
  current_time = time(0);
  if (((current_time - (data -> last_updated_time)) == 0) && !(purple_xfer_is_completed(xfer) != 0)) {
/* Don't update the window more than once per second */
    return ;
  }
  data -> last_updated_time = current_time;
  send = ((purple_xfer_get_type(xfer)) == PURPLE_XFER_SEND);
  size_str = purple_str_size_to_units(purple_xfer_get_size(xfer));
  remaining_str = purple_str_size_to_units(purple_xfer_get_bytes_remaining(xfer));
  kbsec = g_strdup_printf(((const char *)(dgettext("pidgin","%.2f KiB/s"))),kbps);
  gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(xfer_dialog -> tree)),gnt_tree_get_gtype()))),xfer,COLUMN_PROGRESS,(g_ascii_dtostr(prog_str,(sizeof(prog_str)),(purple_xfer_get_progress(xfer) * 100.))));
  gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(xfer_dialog -> tree)),gnt_tree_get_gtype()))),xfer,COLUMN_SIZE,size_str);
  gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(xfer_dialog -> tree)),gnt_tree_get_gtype()))),xfer,COLUMN_REMAINING,remaining_str);
  gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(xfer_dialog -> tree)),gnt_tree_get_gtype()))),xfer,COLUMN_SPEED,kbsec);
  g_free(size_str);
  g_free(remaining_str);
  g_free(kbsec);
  if (purple_xfer_is_completed(xfer) != 0) {
    gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(xfer_dialog -> tree)),gnt_tree_get_gtype()))),xfer,COLUMN_STATUS,((send != 0)?((const char *)(dgettext("pidgin","Sent"))) : ((const char *)(dgettext("pidgin","Received")))));
    gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(xfer_dialog -> tree)),gnt_tree_get_gtype()))),xfer,COLUMN_REMAINING,((const char *)(dgettext("pidgin","Finished"))));
    if (!(send != 0)) {
      char *msg = g_strdup_printf(((const char *)(dgettext("pidgin","The file was saved as %s."))),purple_xfer_get_local_filename(xfer));
      purple_xfer_conversation_write(xfer,msg,0);
      g_free(msg);
    }
    data -> notified = 1;
  }
  else {
    gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(xfer_dialog -> tree)),gnt_tree_get_gtype()))),xfer,COLUMN_STATUS,((send != 0)?((const char *)(dgettext("pidgin","Sending"))) : ((const char *)(dgettext("pidgin","Receiving")))));
  }
  update_title_progress();
  if ((purple_xfer_is_completed(xfer) != 0) && ((xfer_dialog -> auto_clear) != 0)) 
    finch_xfer_dialog_remove_xfer(xfer);
}
/**************************************************************************
 * File Transfer UI Ops
 **************************************************************************/

static void finch_xfer_new_xfer(PurpleXfer *xfer)
{
  PurpleGntXferUiData *data;
/* This is where we're setting xfer->ui_data for the first time. */
  data = ((PurpleGntXferUiData *)(g_malloc0_n(1,(sizeof(PurpleGntXferUiData )))));
  xfer -> ui_data = data;
}

static void finch_xfer_destroy(PurpleXfer *xfer)
{
  PurpleGntXferUiData *data;
  data = ((PurpleGntXferUiData *)(xfer -> ui_data));
  if (data != 0) {
    g_free((data -> name));
    g_free(data);
    xfer -> ui_data = ((void *)((void *)0));
  }
}

static void finch_xfer_add_xfer(PurpleXfer *xfer)
{
  if (!(xfer_dialog != 0)) 
    finch_xfer_dialog_new();
  finch_xfer_dialog_add_xfer(xfer);
  gnt_tree_set_selected(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(xfer_dialog -> tree)),gnt_tree_get_gtype()))),xfer);
}

static void finch_xfer_update_progress(PurpleXfer *xfer,double percent)
{
  if (xfer_dialog != 0) 
    finch_xfer_dialog_update_xfer(xfer);
}

static void finch_xfer_cancel_local(PurpleXfer *xfer)
{
  if (xfer_dialog != 0) 
    finch_xfer_dialog_cancel_xfer(xfer);
}

static void finch_xfer_cancel_remote(PurpleXfer *xfer)
{
  if (xfer_dialog != 0) 
    finch_xfer_dialog_cancel_xfer(xfer);
}
static PurpleXferUiOps ops = {(finch_xfer_new_xfer), (finch_xfer_destroy), (finch_xfer_add_xfer), (finch_xfer_update_progress), (finch_xfer_cancel_local), (finch_xfer_cancel_remote), ((gssize (*)(PurpleXfer *, const guchar *, gssize ))((void *)0)), ((gssize (*)(PurpleXfer *, guchar **, gssize ))((void *)0)), ((void (*)(PurpleXfer *, const guchar *, gsize ))((void *)0)), ((void (*)(PurpleXfer *, const gchar *))((void *)0))
/* padding */
};
/**************************************************************************
 * GNT File Transfer API
 **************************************************************************/

void finch_xfers_init()
{
  purple_prefs_add_none("/finch/filetransfer");
  purple_prefs_add_bool("/finch/filetransfer/clear_finished",1);
  purple_prefs_add_bool("/finch/filetransfer/keep_open",0);
}

void finch_xfers_uninit()
{
  if (xfer_dialog != ((PurpleGntXferDialog *)((void *)0))) 
    finch_xfer_dialog_destroy();
}

PurpleXferUiOps *finch_xfers_get_ui_ops()
{
  return &ops;
}
