/**
 * @file gntroomlist.c GNT Room List API
 * @ingroup finch
 */
/* finch
 *
 * Finch is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "finch.h"
#include <internal.h>
#include "gntrequest.h"
#include "gntroomlist.h"
#include "gntbox.h"
#include "gntbutton.h"
#include "gntcombobox.h"
#include "gnttextview.h"
#include "gnttree.h"
#include "gntwindow.h"
#include "debug.h"
#define PREF_ROOT "/finch/roomlist"
/* Yes, just one roomlist at a time. Let's not get greedy. Aight? */
struct _FinchRoomlist {
GntWidget *window;
GntWidget *accounts;
GntWidget *tree;
GntWidget *details;
GntWidget *getlist;
GntWidget *add;
GntWidget *join;
GntWidget *stop;
GntWidget *close;
PurpleAccount *account;
PurpleRoomlist *roomlist;}froomlist;
typedef struct _FinchRoomlist FinchRoomlist;

static void unset_roomlist(gpointer null)
{
  froomlist.window = ((GntWidget *)((void *)0));
  if (froomlist.roomlist != 0) {
    purple_roomlist_unref(froomlist.roomlist);
    froomlist.roomlist = ((PurpleRoomlist *)((void *)0));
  }
  froomlist.account = ((PurpleAccount *)((void *)0));
  froomlist.tree = ((GntWidget *)((void *)0));
}

static void update_roomlist(PurpleRoomlist *list)
{
  if (froomlist.roomlist == list) 
    return ;
  if (froomlist.roomlist != 0) 
    purple_roomlist_unref(froomlist.roomlist);
  if ((froomlist.roomlist = list) != ((PurpleRoomlist *)((void *)0))) 
    purple_roomlist_ref(list);
}

static void fl_stop(GntWidget *button,gpointer null)
{
  if ((froomlist.roomlist != 0) && (purple_roomlist_get_in_progress(froomlist.roomlist) != 0)) 
    purple_roomlist_cancel_get_list(froomlist.roomlist);
}

static void fl_get_list(GntWidget *button,gpointer null)
{
  PurpleAccount *account = (gnt_combo_box_get_selected_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)froomlist.accounts),gnt_combo_box_get_gtype())))));
  PurpleConnection *gc = purple_account_get_connection(account);
  if (!(gc != 0)) 
    return ;
  update_roomlist(0);
  froomlist.roomlist = purple_roomlist_get_list(gc);
  gnt_box_give_focus_to_child(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)froomlist.window),gnt_box_get_gtype()))),froomlist.tree);
}

static void fl_add_chat(GntWidget *button,gpointer null)
{
  char *name;
  PurpleRoomlistRoom *room = (gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)froomlist.tree),gnt_tree_get_gtype())))));
  PurpleConnection *gc = purple_account_get_connection(froomlist.account);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  if ((gc == ((PurpleConnection *)((void *)0))) || (room == ((PurpleRoomlistRoom *)((void *)0)))) 
    return ;
  prpl_info = ((PurplePluginProtocolInfo *)( *( *purple_connection_get_prpl(gc)).info).extra_info);
  if ((prpl_info != ((PurplePluginProtocolInfo *)((void *)0))) && ((prpl_info -> roomlist_room_serialize) != 0)) 
    name = ( *(prpl_info -> roomlist_room_serialize))(room);
  else 
    name = g_strdup(purple_roomlist_room_get_name(room));
  purple_blist_request_add_chat(froomlist.account,0,0,name);
  g_free(name);
}

static void fl_close(GntWidget *button,gpointer null)
{
  gnt_widget_destroy(froomlist.window);
}

static void roomlist_activated(GntWidget *widget)
{
  PurpleRoomlistRoom *room = (gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype())))));
  if (!(room != 0)) 
    return ;
  switch(purple_roomlist_room_get_type(room)){
    case PURPLE_ROOMLIST_ROOMTYPE_ROOM:
{
      purple_roomlist_room_join(froomlist.roomlist,room);
      break; 
    }
    case PURPLE_ROOMLIST_ROOMTYPE_CATEGORY:
{
      if (!((room -> expanded_once) != 0)) {
        purple_roomlist_expand_category(froomlist.roomlist,room);
        room -> expanded_once = 1;
      }
      break; 
    }
  }
  gnt_tree_set_expanded(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()))),room,1);
}

static void roomlist_selection_changed(GntWidget *widget,gpointer old,gpointer current,gpointer null)
{
  GList *iter;
  GList *field;
  PurpleRoomlistRoom *room = current;
  GntTextView *tv = (GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)froomlist.details),gnt_text_view_get_gtype()));
  gboolean first = 1;
  gnt_text_view_clear(tv);
  if (!(room != 0)) 
    return ;
  for (((iter = purple_roomlist_room_get_fields(room)) , (field = purple_roomlist_get_fields(froomlist.roomlist))); (iter != 0) && (field != 0); ((iter = (iter -> next)) , (field = (field -> next)))) {{
      PurpleRoomlistField *f = (field -> data);
      char *label = (char *)((void *)0);
      if (purple_roomlist_field_get_hidden(f) != 0) {
        continue; 
      }
      if (!(first != 0)) 
        gnt_text_view_append_text_with_flags(tv,"\n",GNT_TEXT_FLAG_NORMAL);
      gnt_text_view_append_text_with_flags(tv,purple_roomlist_field_get_label(f),GNT_TEXT_FLAG_BOLD);
      gnt_text_view_append_text_with_flags(tv,": ",GNT_TEXT_FLAG_BOLD);
      switch(purple_roomlist_field_get_type(f)){
        case PURPLE_ROOMLIST_FIELD_BOOL:
{
          label = g_strdup(((((iter -> data) != 0)?"True" : "False")));
          break; 
        }
        case PURPLE_ROOMLIST_FIELD_INT:
{
          label = g_strdup_printf("%d",((gint )((glong )(iter -> data))));
          break; 
        }
        case PURPLE_ROOMLIST_FIELD_STRING:
{
          label = g_strdup((iter -> data));
          break; 
        }
      }
      gnt_text_view_append_text_with_flags(tv,label,GNT_TEXT_FLAG_NORMAL);
      g_free(label);
      first = 0;
    }
  }
  if ((purple_roomlist_room_get_type(room)) == PURPLE_ROOMLIST_ROOMTYPE_CATEGORY) {
    if (!(first != 0)) 
      gnt_text_view_append_text_with_flags(tv,"\n",GNT_TEXT_FLAG_NORMAL);
    gnt_text_view_append_text_with_flags(tv,((const char *)(dgettext("pidgin","Hit \'Enter\' to find more rooms of this category."))),GNT_TEXT_FLAG_NORMAL);
  }
}

static void roomlist_account_changed(GntWidget *widget,gpointer old,gpointer current,gpointer null)
{
  if (froomlist.account == current) {
    return ;
  }
  froomlist.account = current;
  if (froomlist.roomlist != 0) {
    if (purple_roomlist_get_in_progress(froomlist.roomlist) != 0) 
      purple_roomlist_cancel_get_list(froomlist.roomlist);
    update_roomlist(0);
  }
  gnt_tree_remove_all(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)froomlist.tree),gnt_tree_get_gtype()))));
  gnt_widget_draw(froomlist.tree);
}

static void reset_account_list(PurpleAccount *account)
{
  GList *list;
  GntComboBox *accounts = (GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)froomlist.accounts),gnt_combo_box_get_gtype()));
  gnt_combo_box_remove_all(accounts);
  for (list = purple_connections_get_all(); list != 0; list = (list -> next)) {
    PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
    PurpleConnection *gc = (list -> data);
    prpl_info = ((PurplePluginProtocolInfo *)( *( *purple_connection_get_prpl(gc)).info).extra_info);
    if (((purple_connection_get_state(gc)) == PURPLE_CONNECTED) && ((prpl_info -> roomlist_get_list) != ((PurpleRoomlist *(*)(PurpleConnection *))((void *)0)))) {
      PurpleAccount *account = purple_connection_get_account(gc);
      char *text = g_strdup_printf("%s (%s)",purple_account_get_username(account),purple_account_get_protocol_name(account));
      gnt_combo_box_add_data(accounts,account,text);
      g_free(text);
    }
  }
}

static void size_changed_cb(GntWidget *widget,int oldw,int oldh)
{
  int w;
  int h;
  gnt_widget_get_size(widget,&w,&h);
  purple_prefs_set_int("/finch/roomlist/size/width",w);
  purple_prefs_set_int("/finch/roomlist/size/height",h);
}

static void setup_roomlist(PurpleAccount *account)
{
  GntWidget *window;
  GntWidget *tree;
  GntWidget *hbox;
  GntWidget *accounts;
  int iter;
  struct __unnamed_class___F0_L269_C2_L435R__L436R__scope____SgSS2___variable_declaration__variable_type___Pb__Cc__Pe___variable_name_L435R__L436R__scope____SgSS2____scope__label__DELIMITER__L435R__L436R__scope____SgSS2___variable_declaration__variable_type_L187R_variable_name_L435R__L436R__scope____SgSS2____scope__callback__DELIMITER__L435R__L436R__scope____SgSS2___variable_declaration__variable_type___Pb____Pb__GntWidget_GntWidget__typedef_declaration__Pe____Pe___variable_name_L435R__L436R__scope____SgSS2____scope__widget {
  const char *label;
  GCallback callback;
  GntWidget **widget;}buttons[] = {{((const char *)(dgettext("pidgin","Stop"))), ((GCallback )fl_stop), (&froomlist.stop)}, {((const char *)(dgettext("pidgin","Get"))), ((GCallback )fl_get_list), (&froomlist.getlist)}, {((const char *)(dgettext("pidgin","Add"))), ((GCallback )fl_add_chat), (&froomlist.add)}, {((const char *)(dgettext("pidgin","Close"))), ((GCallback )fl_close), (&froomlist.close)}, {((const char *)((void *)0)), ((GCallback )((void *)0)), ((GntWidget **)((void *)0))}};
  if (froomlist.window != 0) 
    return ;
  froomlist.window = (window = gnt_window_new());
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"vertical",1,((void *)((void *)0)));
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),0);
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),((const char *)(dgettext("pidgin","Room List"))));
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),GNT_ALIGN_MID);
  froomlist.accounts = (accounts = gnt_combo_box_new());
  reset_account_list(account);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),accounts);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)accounts),((GType )(20 << 2))))),"selection-changed",((GCallback )roomlist_account_changed),0,0,((GConnectFlags )0));
  froomlist.account = (gnt_combo_box_get_selected_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)accounts),gnt_combo_box_get_gtype())))));
  froomlist.tree = (tree = gnt_tree_new_with_columns(2));
  gnt_tree_set_show_title(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),1);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"activate",((GCallback )roomlist_activated),0,0,((GConnectFlags )0));
  gnt_tree_set_column_titles(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),((const char *)(dgettext("pidgin","Name"))),"");
  gnt_tree_set_show_separator(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),0);
  gnt_tree_set_col_width(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),1,1);
  gnt_tree_set_column_resizable(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),1,0);
  gnt_tree_set_search_column(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),tree);
  froomlist.details = gnt_text_view_new();
  gnt_text_view_set_flag(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)froomlist.details),gnt_text_view_get_gtype()))),GNT_TEXT_VIEW_TOP_ALIGN);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),froomlist.details);
  gnt_widget_set_size(froomlist.details,-1,8);
  hbox = gnt_box_new(0,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),hbox);
  for (iter = 0; buttons[iter].label != 0; iter++) {
    GntWidget *button = gnt_button_new(buttons[iter].label);
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),button);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",buttons[iter].callback,0,0,((GConnectFlags )0));
     *buttons[iter].widget = button;
    gnt_text_view_attach_scroll_widget(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)froomlist.details),gnt_text_view_get_gtype()))),button);
  }
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"selection-changed",((GCallback )roomlist_selection_changed),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"destroy",((GCallback )unset_roomlist),0,0,((GConnectFlags )0));
}

static void fl_show_with_account(PurpleAccount *account)
{
  setup_roomlist(account);
  g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)froomlist.window),((GType )(20 << 2))))),G_SIGNAL_MATCH_FUNC,0,0,0,((GCallback )size_changed_cb),0);
  gnt_widget_show(froomlist.window);
  gnt_screen_resize_widget(froomlist.window,purple_prefs_get_int("/finch/roomlist/size/width"),purple_prefs_get_int("/finch/roomlist/size/height"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)froomlist.window),((GType )(20 << 2))))),"size_changed",((GCallback )size_changed_cb),0,0,((GConnectFlags )0));
  gnt_window_present(froomlist.window);
}

static void fl_create(PurpleRoomlist *list)
{
  list -> ui_data = (&froomlist);
  setup_roomlist(0);
  update_roomlist(list);
}

static void fl_set_fields(PurpleRoomlist *list,GList *fields)
{
}

static void fl_add_room(PurpleRoomlist *roomlist,PurpleRoomlistRoom *room)
{
  gboolean category;
  if (froomlist.roomlist != roomlist) 
    return ;
  category = ((purple_roomlist_room_get_type(room)) == PURPLE_ROOMLIST_ROOMTYPE_CATEGORY);
  gnt_tree_remove(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)froomlist.tree),gnt_tree_get_gtype()))),room);
  gnt_tree_add_row_after(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)froomlist.tree),gnt_tree_get_gtype()))),room,gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)froomlist.tree),gnt_tree_get_gtype()))),purple_roomlist_room_get_name(room),((category != 0)?"<" : "")),(purple_roomlist_room_get_parent(room)),0);
  gnt_tree_set_expanded(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)froomlist.tree),gnt_tree_get_gtype()))),room,!(category != 0));
}

static void fl_destroy(PurpleRoomlist *list)
{
  if (!(froomlist.window != 0)) 
    return ;
  if (froomlist.roomlist == list) {
    froomlist.roomlist = ((PurpleRoomlist *)((void *)0));
    gnt_tree_remove_all(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)froomlist.tree),gnt_tree_get_gtype()))));
    gnt_widget_draw(froomlist.tree);
  }
}
static PurpleRoomlistUiOps ui_ops = {(fl_show_with_account), (fl_create), (fl_set_fields), (fl_add_room), ((void (*)(PurpleRoomlist *, gboolean ))((void *)0)), (fl_destroy), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* void (*show_with_account)(PurpleAccount *account); **< Force the ui to pop up a dialog and get the list */
/* void (*create)(PurpleRoomlist *list); **< A new list was created. */
/* void (*set_fields)(PurpleRoomlist *list, GList *fields); **< Sets the columns. */
/* void (*add_room)(PurpleRoomlist *list, PurpleRoomlistRoom *room); **< Add a room to the list. */
/* void (*in_progress)(PurpleRoomlist *list, gboolean flag); **< Are we fetching stuff still? */
/* void (*destroy)(PurpleRoomlist *list); **< We're destroying list. */
/* void (*_purple_reserved1)(void); */
/* void (*_purple_reserved2)(void); */
/* void (*_purple_reserved3)(void); */
/* void (*_purple_reserved4)(void); */
};

PurpleRoomlistUiOps *finch_roomlist_get_ui_ops()
{
  return &ui_ops;
}

void finch_roomlist_show_all()
{
  purple_roomlist_show_with_account(0);
}

void finch_roomlist_init()
{
  purple_prefs_add_none("/finch/roomlist");
  purple_prefs_add_none("/finch/roomlist/size");
  purple_prefs_add_int("/finch/roomlist/size/width",60);
  purple_prefs_add_int("/finch/roomlist/size/height",15);
}

void finch_roomlist_uninit()
{
}
