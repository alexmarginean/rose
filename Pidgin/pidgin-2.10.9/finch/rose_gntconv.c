/**
 * @file gntconv.c GNT Conversation API
 * @ingroup finch
 */
/* finch
 *
 * Finch is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <internal.h>
#include "finch.h"
#include <cmds.h>
#include <core.h>
#include <idle.h>
#include <prefs.h>
#include <util.h>
#include "gntaccount.h"
#include "gntblist.h"
#include "gntconv.h"
#include "gntdebug.h"
#include "gntlog.h"
#include "gntplugin.h"
#include "gntpounce.h"
#include "gntprefs.h"
#include "gntrequest.h"
#include "gntsound.h"
#include "gntstatus.h"
#include "gnt.h"
#include "gntbox.h"
#include "gntentry.h"
#include "gntlabel.h"
#include "gntmenu.h"
#include "gntmenuitem.h"
#include "gntmenuitemcheck.h"
#include "gntstyle.h"
#include "gnttextview.h"
#include "gnttree.h"
#include "gntutils.h"
#include "gntwindow.h"
#define PREF_ROOT	"/finch/conversations"
#define PREF_CHAT   PREF_ROOT "/chats"
#define PREF_USERLIST PREF_CHAT "/userlist"
#include "config.h"
static void finch_write_common(PurpleConversation *conv,const char *who,const char *message,PurpleMessageFlags flags,time_t mtime);
static void generate_send_to_menu(FinchConv *ggc);
static int color_message_receive;
static int color_message_send;
static int color_message_highlight;
static int color_message_action;
static int color_timestamp;

static PurpleBuddy *find_buddy_for_conversation(PurpleConversation *conv)
{
  return purple_find_buddy(purple_conversation_get_account(conv),purple_conversation_get_name(conv));
}

static PurpleChat *find_chat_for_conversation(PurpleConversation *conv)
{
  return purple_blist_find_chat(purple_conversation_get_account(conv),purple_conversation_get_name(conv));
}

static PurpleBlistNode *get_conversation_blist_node(PurpleConversation *conv)
{
  PurpleBlistNode *node = (PurpleBlistNode *)((void *)0);
  switch(purple_conversation_get_type(conv)){
    case PURPLE_CONV_TYPE_IM:
{
      node = ((PurpleBlistNode *)(find_buddy_for_conversation(conv)));
      node = ((node != 0)?purple_blist_node_get_parent(node) : ((struct _PurpleBlistNode *)((void *)0)));
      break; 
    }
    case PURPLE_CONV_TYPE_CHAT:
{
      node = ((PurpleBlistNode *)(find_chat_for_conversation(conv)));
      break; 
    }
    default:
{
      break; 
    }
  }
  return node;
}

static void send_typing_notification(GntWidget *w,FinchConv *ggconv)
{
  const char *text = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(ggconv -> entry)),gnt_entry_get_gtype()))));
  gboolean empty = ((!(text != 0) || !(( *text) != 0)) || (( *text) == '/'));
  if (purple_prefs_get_bool("/finch/conversations/notify_typing") != 0) {
    PurpleConversation *conv = (ggconv -> active_conv);
    PurpleConvIm *im = purple_conversation_get_im_data(conv);
    if (!(empty != 0)) {
      gboolean send = (purple_conv_im_get_send_typed_timeout(im) == 0);
      purple_conv_im_stop_send_typed_timeout(im);
      purple_conv_im_start_send_typed_timeout(im);
      if ((send != 0) || ((purple_conv_im_get_type_again(im) != 0) && (time(0) > purple_conv_im_get_type_again(im)))) {
        unsigned int timeout;
        timeout = serv_send_typing(purple_conversation_get_gc(conv),purple_conversation_get_name(conv),PURPLE_TYPING);
        purple_conv_im_set_type_again(im,timeout);
      }
    }
    else {
      purple_conv_im_stop_send_typed_timeout(im);
      serv_send_typing(purple_conversation_get_gc(conv),purple_conversation_get_name(conv),PURPLE_NOT_TYPING);
    }
  }
}

static void entry_key_pressed(GntWidget *w,FinchConv *ggconv)
{
  const char *text = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(ggconv -> entry)),gnt_entry_get_gtype()))));
  if ((( *text) == '/') && (text[1] != '/')) {
    PurpleConversation *conv = (ggconv -> active_conv);
    PurpleCmdStatus status;
    const char *cmdline = (text + 1);
    char *error = (char *)((void *)0);
    char *escape;
    escape = g_markup_escape_text(cmdline,(-1));
    status = purple_cmd_do_command(conv,cmdline,escape,&error);
    g_free(escape);
    switch(status){
      case PURPLE_CMD_STATUS_OK:
{
        break; 
      }
      case PURPLE_CMD_STATUS_NOT_FOUND:
{
        purple_conversation_write(conv,"",((const char *)(dgettext("pidgin","No such command."))),PURPLE_MESSAGE_NO_LOG,time(0));
        break; 
      }
      case PURPLE_CMD_STATUS_WRONG_ARGS:
{
        purple_conversation_write(conv,"",((const char *)(dgettext("pidgin","Syntax Error:  You typed the wrong number of arguments to that command."))),PURPLE_MESSAGE_NO_LOG,time(0));
        break; 
      }
      case PURPLE_CMD_STATUS_FAILED:
{
        purple_conversation_write(conv,"",((error != 0)?error : ((const char *)(dgettext("pidgin","Your command failed for an unknown reason.")))),PURPLE_MESSAGE_NO_LOG,time(0));
        break; 
      }
      case PURPLE_CMD_STATUS_WRONG_TYPE:
{
        if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) 
          purple_conversation_write(conv,"",((const char *)(dgettext("pidgin","That command only works in chats, not IMs."))),PURPLE_MESSAGE_NO_LOG,time(0));
        else 
          purple_conversation_write(conv,"",((const char *)(dgettext("pidgin","That command only works in IMs, not chats."))),PURPLE_MESSAGE_NO_LOG,time(0));
        break; 
      }
      case PURPLE_CMD_STATUS_WRONG_PRPL:
{
        purple_conversation_write(conv,"",((const char *)(dgettext("pidgin","That command doesn\'t work on this protocol."))),PURPLE_MESSAGE_NO_LOG,time(0));
        break; 
      }
    }
    g_free(error);
  }
  else if (!(purple_account_is_connected((purple_conversation_get_account((ggconv -> active_conv)))) != 0)) {
    purple_conversation_write((ggconv -> active_conv),"",((const char *)(dgettext("pidgin","Message was not sent, because you are not signed on."))),(PURPLE_MESSAGE_ERROR | PURPLE_MESSAGE_NO_LOG),time(0));
  }
  else {
    char *escape = purple_markup_escape_text(((( *text) == '/')?(text + 1) : text),(-1));
    switch(purple_conversation_get_type((ggconv -> active_conv))){
      case PURPLE_CONV_TYPE_IM:
{
        purple_conv_im_send_with_flags(purple_conversation_get_im_data((ggconv -> active_conv)),escape,PURPLE_MESSAGE_SEND);
        break; 
      }
      case PURPLE_CONV_TYPE_CHAT:
{
        purple_conv_chat_send(purple_conversation_get_chat_data((ggconv -> active_conv)),escape);
        break; 
      }
      default:
{
        g_free(escape);
        do {
          g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","gntconv.c",206,((const char *)__func__));
          return ;
        }while (0);
      }
    }
    g_free(escape);
    purple_idle_touch();
  }
  gnt_entry_add_to_history(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(ggconv -> entry)),gnt_entry_get_gtype()))),text);
  gnt_entry_clear(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(ggconv -> entry)),gnt_entry_get_gtype()))));
}

static void closing_window(GntWidget *window,FinchConv *ggconv)
{
  GList *list = (ggconv -> list);
  ggconv -> window = ((GntWidget *)((void *)0));
  while(list != 0){
    PurpleConversation *conv = (list -> data);
    list = (list -> next);
    purple_conversation_destroy(conv);
  }
}

static void size_changed_cb(GntWidget *widget,int width,int height)
{
  int w;
  int h;
  gnt_widget_get_size(widget,&w,&h);
  purple_prefs_set_int("/finch/conversations/size/width",w);
  purple_prefs_set_int("/finch/conversations/size/height",h);
}

static void save_position_cb(GntWidget *w,int x,int y)
{
  purple_prefs_set_int("/finch/conversations/position/x",x);
  purple_prefs_set_int("/finch/conversations/position/y",y);
}

static PurpleConversation *find_conv_with_contact(PurpleAccount *account,const char *name)
{
  PurpleBlistNode *node;
  PurpleBuddy *buddy = purple_find_buddy(account,name);
  PurpleConversation *ret = (PurpleConversation *)((void *)0);
  if (!(buddy != 0)) 
    return 0;
{
    for (node = purple_blist_node_get_first_child(purple_blist_node_get_parent(((PurpleBlistNode *)buddy))); node != 0; node = purple_blist_node_get_sibling_next(node)) {
      if (node == ((PurpleBlistNode *)buddy)) 
        continue; 
      if ((ret = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,purple_buddy_get_name(((PurpleBuddy *)node)),(purple_buddy_get_account(((PurpleBuddy *)node))))) != ((PurpleConversation *)((void *)0))) 
        break; 
    }
  }
  return ret;
}

static char *get_conversation_title(PurpleConversation *conv,PurpleAccount *account)
{
  return g_strdup_printf(((const char *)(dgettext("pidgin","%s (%s -- %s)"))),purple_conversation_get_title(conv),purple_account_get_username(account),purple_account_get_protocol_name(account));
}

static void update_buddy_typing(PurpleAccount *account,const char *who,gpointer null)
{
  PurpleConversation *conv;
  FinchConv *ggc;
  PurpleConvIm *im = (PurpleConvIm *)((void *)0);
  char *title;
  char *str;
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,who,account);
  if (!(conv != 0)) 
    return ;
  im = purple_conversation_get_im_data(conv);
  ggc = (conv -> ui_data);
  if ((purple_conv_im_get_typing_state(im)) == PURPLE_TYPING) {
    int scroll;
    str = get_conversation_title(conv,account);
    title = g_strdup_printf(((const char *)(dgettext("pidgin","%s [%s]"))),str,((gnt_ascii_only() != 0)?"T" : "\342\243\277"));
    g_free(str);
    scroll = gnt_text_view_get_lines_below(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> tv)),gnt_text_view_get_gtype()))));
    str = g_strdup_printf(((const char *)(dgettext("pidgin","\n%s is typing..."))),purple_conversation_get_title(conv));
/* Updating is a little buggy. So just remove and add a new one */
    gnt_text_view_tag_change(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> tv)),gnt_text_view_get_gtype()))),"typing",0,1);
    gnt_text_view_append_text_with_tag(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> tv)),gnt_text_view_get_gtype()))),str,GNT_TEXT_FLAG_DIM,"typing");
    g_free(str);
    if (scroll <= 1) 
      gnt_text_view_scroll(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> tv)),gnt_text_view_get_gtype()))),0);
  }
  else {
    title = get_conversation_title(conv,account);
    gnt_text_view_tag_change(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> tv)),gnt_text_view_get_gtype()))),"typing"," ",1);
  }
  gnt_screen_rename_widget((ggc -> window),title);
  g_free(title);
}

static void chat_left_cb(PurpleConversation *conv,gpointer null)
{
  finch_write_common(conv,0,((const char *)(dgettext("pidgin","You have left this chat."))),PURPLE_MESSAGE_SYSTEM,time(0));
}

static void buddy_signed_on_off(PurpleBuddy *buddy,gpointer null)
{
  PurpleConversation *conv = find_conv_with_contact(purple_buddy_get_account(buddy),purple_buddy_get_name(buddy));
  if (conv == ((PurpleConversation *)((void *)0))) 
    return ;
  generate_send_to_menu((conv -> ui_data));
}

static void account_signed_on_off(PurpleConnection *gc,gpointer null)
{
  GList *list = purple_get_ims();
  while(list != 0){
    PurpleConversation *conv = (list -> data);
    PurpleConversation *cc = find_conv_with_contact(purple_conversation_get_account(conv),purple_conversation_get_name(conv));
    if (cc != 0) 
      generate_send_to_menu((cc -> ui_data));
    list = (list -> next);
  }
  if ((purple_connection_get_state(gc)) == PURPLE_CONNECTED) {
/* We just signed on. Let's see if there's any chat that we have open,
		 * and hadn't left before the disconnect. */
    list = purple_get_chats();
    while(list != 0){{
        PurpleConversation *conv = (list -> data);
        PurpleChat *chat;
        GHashTable *comps = (GHashTable *)((void *)0);
        list = (list -> next);
        if ((purple_conversation_get_account(conv) != purple_connection_get_account(gc)) || !(purple_conversation_get_data(conv,"want-to-rejoin") != 0)) 
          continue; 
        chat = find_chat_for_conversation(conv);
        if (chat == ((PurpleChat *)((void *)0))) {
          PurplePluginProtocolInfo *info = (PurplePluginProtocolInfo *)( *( *purple_connection_get_prpl(gc)).info).extra_info;
          if ((info -> chat_info_defaults) != ((GHashTable *(*)(PurpleConnection *, const char *))((void *)0))) 
            comps = ( *(info -> chat_info_defaults))(gc,purple_conversation_get_name(conv));
        }
        else {
          comps = purple_chat_get_components(chat);
        }
        serv_join_chat(gc,comps);
        if ((chat == ((PurpleChat *)((void *)0))) && (comps != ((GHashTable *)((void *)0)))) 
          g_hash_table_destroy(comps);
      }
    }
  }
}

static void account_signing_off(PurpleConnection *gc)
{
  GList *list = purple_get_chats();
  PurpleAccount *account = purple_connection_get_account(gc);
/* We are about to sign off. See which chats we are currently in, and mark
	 * them for rejoin on reconnect. */
  while(list != 0){
    PurpleConversation *conv = (list -> data);
    if (!(purple_conv_chat_has_left(purple_conversation_get_chat_data(conv)) != 0) && (purple_conversation_get_account(conv) == account)) {
      purple_conversation_set_data(conv,"want-to-rejoin",((gpointer )((gpointer )((glong )1))));
      purple_conversation_write(conv,0,((const char *)(dgettext("pidgin","The account has disconnected and you are no longer in this chat. You will be automatically rejoined in the chat when the account reconnects."))),PURPLE_MESSAGE_SYSTEM,time(0));
    }
    list = (list -> next);
  }
}

static gpointer finch_conv_get_handle()
{
  static int handle;
  return (&handle);
}

static void cleared_message_history_cb(PurpleConversation *conv,gpointer data)
{
  FinchConv *ggc = (conv -> ui_data);
  if (ggc != 0) 
    gnt_text_view_clear(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> tv)),gnt_text_view_get_gtype()))));
}

static void clear_scrollback_cb(GntMenuItem *item,gpointer ggconv)
{
  FinchConv *ggc = ggconv;
  purple_conversation_clear_message_history((ggc -> active_conv));
}

static void send_file_cb(GntMenuItem *item,gpointer ggconv)
{
  FinchConv *ggc = ggconv;
  serv_send_file(purple_conversation_get_gc((ggc -> active_conv)),purple_conversation_get_name((ggc -> active_conv)),0);
}

static void add_pounce_cb(GntMenuItem *item,gpointer ggconv)
{
  FinchConv *ggc = ggconv;
  finch_pounce_editor_show(purple_conversation_get_account((ggc -> active_conv)),purple_conversation_get_name((ggc -> active_conv)),0);
}

static void get_info_cb(GntMenuItem *item,gpointer ggconv)
{
  FinchConv *ggc = ggconv;
  finch_retrieve_user_info(purple_conversation_get_gc((ggc -> active_conv)),purple_conversation_get_name((ggc -> active_conv)));
}

static void toggle_timestamps_cb(GntMenuItem *item,gpointer ggconv)
{
  purple_prefs_set_bool("/finch/conversations/timestamps",!(purple_prefs_get_bool("/finch/conversations/timestamps") != 0));
}

static void toggle_logging_cb(GntMenuItem *item,gpointer ggconv)
{
  FinchConv *fc = ggconv;
  PurpleConversation *conv = (fc -> active_conv);
  gboolean logging = gnt_menuitem_check_get_checked(((GntMenuItemCheck *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_check_get_gtype()))));
  GList *iter;
  if (logging == purple_conversation_is_logging(conv)) 
    return ;
/* Xerox */
  if (logging != 0) {
/* Enable logging first so the message below can be logged. */
    purple_conversation_set_logging(conv,1);
    purple_conversation_write(conv,0,((const char *)(dgettext("pidgin","Logging started. Future messages in this conversation will be logged."))),PURPLE_MESSAGE_SYSTEM,time(0));
  }
  else {
    purple_conversation_write(conv,0,((const char *)(dgettext("pidgin","Logging stopped. Future messages in this conversation will not be logged."))),PURPLE_MESSAGE_SYSTEM,time(0));
/* Disable the logging second, so that the above message can be logged. */
    purple_conversation_set_logging(conv,0);
  }
/* Each conversation with the same person will have the same logging setting */
  for (iter = (fc -> list); iter != 0; iter = (iter -> next)) {
    if ((iter -> data) == conv) 
      continue; 
    purple_conversation_set_logging((iter -> data),logging);
  }
}

static void toggle_sound_cb(GntMenuItem *item,gpointer ggconv)
{
  FinchConv *fc = ggconv;
  PurpleBlistNode *node = get_conversation_blist_node((fc -> active_conv));
  fc -> flags ^= FINCH_CONV_NO_SOUND;
  if (node != 0) 
    purple_blist_node_set_bool(node,"gnt-mute-sound",!(!(((fc -> flags) & FINCH_CONV_NO_SOUND) != 0U)));
}

static void send_to_cb(GntMenuItem *m,gpointer n)
{
  PurpleAccount *account = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)m),((GType )(20 << 2))))),"purple_account"));
  gchar *buddy = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)m),((GType )(20 << 2))))),"purple_buddy_name"));
  PurpleConversation *conv = purple_conversation_new(PURPLE_CONV_TYPE_IM,account,buddy);
  finch_conversation_set_active(conv);
}

static void view_log_cb(GntMenuItem *n,gpointer ggc)
{
  FinchConv *fc;
  PurpleConversation *conv;
  PurpleLogType type;
  const char *name;
  PurpleAccount *account;
  GSList *buddies;
  GSList *cur;
  fc = ggc;
  conv = (fc -> active_conv);
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) 
    type = PURPLE_LOG_IM;
  else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) 
    type = PURPLE_LOG_CHAT;
  else 
    return ;
  name = purple_conversation_get_name(conv);
  account = purple_conversation_get_account(conv);
  buddies = purple_find_buddies(account,name);
  for (cur = buddies; cur != ((GSList *)((void *)0)); cur = (cur -> next)) {
    PurpleBlistNode *node = (cur -> data);
    if ((node != ((PurpleBlistNode *)((void *)0))) && ((purple_blist_node_get_sibling_prev(node) != 0) || (purple_blist_node_get_sibling_next(node) != 0))) {
      finch_log_show_contact(((PurpleContact *)(purple_blist_node_get_parent(node))));
      g_slist_free(buddies);
      return ;
    }
  }
  g_slist_free(buddies);
  finch_log_show(type,name,account);
}

static void generate_send_to_menu(FinchConv *ggc)
{
  GntWidget *sub;
  GntWidget *menu = (ggc -> menu);
  GntMenuItem *item;
  GSList *buds;
  GList *list = (GList *)((void *)0);
  buds = purple_find_buddies(purple_conversation_get_account((ggc -> active_conv)),purple_conversation_get_name((ggc -> active_conv)));
  if (!(buds != 0)) 
    return ;
  if ((item = ( *ggc -> u.im).sendto) == ((GntMenuItem *)((void *)0))) {
    item = gnt_menuitem_new(((const char *)(dgettext("pidgin","Send To"))));
    gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_menu_get_gtype()))),item);
    ( *ggc -> u.im).sendto = item;
  }
  sub = gnt_menu_new(GNT_MENU_POPUP);
  gnt_menuitem_set_submenu(item,((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))));
  for (; buds != 0; buds = g_slist_delete_link(buds,buds)) {
    PurpleBlistNode *node = (PurpleBlistNode *)(purple_buddy_get_contact(((PurpleBuddy *)(buds -> data))));
    for (node = purple_blist_node_get_first_child(node); node != ((PurpleBlistNode *)((void *)0)); node = purple_blist_node_get_sibling_next(node)) {
      PurpleBuddy *buddy = (PurpleBuddy *)node;
      PurpleAccount *account = purple_buddy_get_account(buddy);
      if (purple_account_is_connected(account) != 0) {
/* Use the PurplePresence to get unique buddies. */
        PurplePresence *presence = purple_buddy_get_presence(buddy);
        if (!(g_list_find(list,presence) != 0)) 
          list = g_list_prepend(list,presence);
      }
    }
  }
  for (list = g_list_reverse(list); list != ((GList *)((void *)0)); list = g_list_delete_link(list,list)) {
    PurplePresence *pre = (list -> data);
    PurpleBuddy *buddy = purple_presence_get_buddy(pre);
    PurpleAccount *account = purple_buddy_get_account(buddy);
    gchar *name = g_strdup(purple_buddy_get_name(buddy));
    gchar *text = g_strdup_printf("%s (%s)",purple_buddy_get_name(buddy),purple_account_get_username(account));
    item = gnt_menuitem_new(text);
    g_free(text);
    gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
    gnt_menuitem_set_callback(item,send_to_cb,0);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"purple_account",account);
    g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"purple_buddy_name",name,g_free);
  }
}

static void invite_cb(GntMenuItem *item,gpointer ggconv)
{
  FinchConv *fc = ggconv;
  PurpleConversation *conv = (fc -> active_conv);
  purple_conv_chat_invite_user(purple_conversation_get_chat_data(conv),0,0,1);
}

static void gg_create_menu(FinchConv *ggc)
{
  GntWidget *menu;
  GntWidget *sub;
  GntMenuItem *item;
  ggc -> menu = (menu = gnt_menu_new(GNT_MENU_TOPLEVEL));
  gnt_window_set_menu(((GntWindow *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> window)),gnt_window_get_gtype()))),((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_menu_get_gtype()))));
  item = gnt_menuitem_new(((const char *)(dgettext("pidgin","Conversation"))));
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_menu_get_gtype()))),item);
  sub = gnt_menu_new(GNT_MENU_POPUP);
  gnt_menuitem_set_submenu(item,((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))));
  item = gnt_menuitem_new(((const char *)(dgettext("pidgin","Clear Scrollback"))));
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
  gnt_menuitem_set_callback(item,clear_scrollback_cb,ggc);
  item = gnt_menuitem_check_new(((const char *)(dgettext("pidgin","Show Timestamps"))));
  gnt_menuitem_check_set_checked(((GntMenuItemCheck *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_check_get_gtype()))),purple_prefs_get_bool("/finch/conversations/timestamps"));
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
  gnt_menuitem_set_callback(item,toggle_timestamps_cb,ggc);
  if ((purple_conversation_get_type((ggc -> active_conv))) == PURPLE_CONV_TYPE_IM) {
    PurpleAccount *account = purple_conversation_get_account((ggc -> active_conv));
    PurpleConnection *gc = purple_account_get_connection(account);
    PurplePluginProtocolInfo *pinfo = (gc != 0)?((PurplePluginProtocolInfo *)( *( *purple_connection_get_prpl(gc)).info).extra_info) : ((struct _PurplePluginProtocolInfo *)((void *)0));
    if ((pinfo != 0) && ((pinfo -> get_info) != 0)) {
      item = gnt_menuitem_new(((const char *)(dgettext("pidgin","Get Info"))));
      gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
      gnt_menuitem_set_callback(item,get_info_cb,ggc);
    }
    item = gnt_menuitem_new(((const char *)(dgettext("pidgin","Add Buddy Pounce..."))));
    gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
    gnt_menuitem_set_callback(item,add_pounce_cb,ggc);
    if (((pinfo != 0) && ((pinfo -> send_file) != 0)) && (!((pinfo -> can_receive_file) != 0) || (( *(pinfo -> can_receive_file))(gc,purple_conversation_get_name((ggc -> active_conv))) != 0))) {
      item = gnt_menuitem_new(((const char *)(dgettext("pidgin","Send File"))));
      gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
      gnt_menuitem_set_callback(item,send_file_cb,ggc);
    }
    generate_send_to_menu(ggc);
  }
  else if ((purple_conversation_get_type((ggc -> active_conv))) == PURPLE_CONV_TYPE_CHAT) {
    item = gnt_menuitem_new(((const char *)(dgettext("pidgin","Invite..."))));
    gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
    gnt_menuitem_set_callback(item,invite_cb,ggc);
  }
  item = gnt_menuitem_new(((const char *)(dgettext("pidgin","View Log..."))));
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
  gnt_menuitem_set_callback(item,view_log_cb,ggc);
  item = gnt_menuitem_check_new(((const char *)(dgettext("pidgin","Enable Logging"))));
  gnt_menuitem_check_set_checked(((GntMenuItemCheck *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_check_get_gtype()))),purple_conversation_is_logging((ggc -> active_conv)));
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
  gnt_menuitem_set_callback(item,toggle_logging_cb,ggc);
  item = gnt_menuitem_check_new(((const char *)(dgettext("pidgin","Enable Sounds"))));
  gnt_menuitem_check_set_checked(((GntMenuItemCheck *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_check_get_gtype()))),!(((ggc -> flags) & FINCH_CONV_NO_SOUND) != 0U));
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
  gnt_menuitem_set_callback(item,toggle_sound_cb,ggc);
}

static void create_conv_from_userlist(GntWidget *widget,FinchConv *fc)
{
  PurpleAccount *account = purple_conversation_get_account((fc -> active_conv));
  PurpleConnection *gc = purple_account_get_connection(account);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  char *name;
  char *realname;
  if (!(gc != 0)) {
    purple_conversation_write((fc -> active_conv),0,((const char *)(dgettext("pidgin","You are not connected."))),PURPLE_MESSAGE_SYSTEM,time(0));
    return ;
  }
  name = (gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype())))));
  prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info);
  if ((prpl_info != 0) && (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).get_cb_real_name))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).get_cb_real_name))) < (prpl_info -> struct_size))) && ((prpl_info -> get_cb_real_name) != ((char *(*)(PurpleConnection *, int , const char *))((void *)0))))) 
    realname = ( *(prpl_info -> get_cb_real_name))(gc,purple_conv_chat_get_id((purple_conversation_get_chat_data((fc -> active_conv)))),name);
  else 
    realname = ((char *)((void *)0));
  purple_conversation_new(PURPLE_CONV_TYPE_IM,account,((realname != 0)?realname : name));
  g_free(realname);
}

static void gained_focus_cb(GntWindow *window,FinchConv *fc)
{
  GList *iter;
  for (iter = (fc -> list); iter != 0; iter = (iter -> next)) {
    purple_conversation_set_data((iter -> data),"unseen-count",0);
    purple_conversation_update((iter -> data),PURPLE_CONV_UPDATE_UNSEEN);
  }
}

static void completion_cb(GntEntry *entry,const char *start,const char *end)
{
  if ((start == (entry -> start)) && (( *start) != '/')) 
    gnt_widget_key_pressed(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_widget_get_gtype()))),": ");
}

static void gg_setup_commands(FinchConv *fconv,gboolean remove_first)
{
  GList *commands;
  char command[256UL] = "/";
  if (remove_first != 0) {
    commands = purple_cmd_list(0);
    for (; commands != 0; commands = g_list_delete_link(commands,commands)) {
      g_strlcpy((command + 1),(commands -> data),(sizeof(command) - 1));
      gnt_entry_remove_suggest(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(fconv -> entry)),gnt_entry_get_gtype()))),command);
    }
  }
  commands = purple_cmd_list((fconv -> active_conv));
  for (; commands != 0; commands = g_list_delete_link(commands,commands)) {
    g_strlcpy((command + 1),(commands -> data),(sizeof(command) - 1));
    gnt_entry_add_suggest(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(fconv -> entry)),gnt_entry_get_gtype()))),command);
  }
}

static void cmd_added_cb(const char *cmd,PurpleCmdPriority prior,PurpleCmdFlag flags,FinchConv *fconv)
{
  gg_setup_commands(fconv,1);
}

static void cmd_removed_cb(const char *cmd,FinchConv *fconv)
{
  char command[256UL] = "/";
  g_strlcpy((command + 1),cmd,(sizeof(command) - 1));
  gnt_entry_remove_suggest(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(fconv -> entry)),gnt_entry_get_gtype()))),command);
  gg_setup_commands(fconv,1);
}

static void finch_create_conversation(PurpleConversation *conv)
{
  FinchConv *ggc = (conv -> ui_data);
  char *title;
  PurpleConversationType type;
  PurpleConversation *cc;
  PurpleAccount *account;
  PurpleBlistNode *convnode = (PurpleBlistNode *)((void *)0);
  if (ggc != 0) {
    gnt_window_present((ggc -> window));
    return ;
  }
  account = purple_conversation_get_account(conv);
  cc = find_conv_with_contact(account,purple_conversation_get_name(conv));
  if ((cc != 0) && ((cc -> ui_data) != 0)) 
    ggc = (cc -> ui_data);
  else 
    ggc = ((FinchConv *)(g_malloc0_n(1,(sizeof(FinchConv )))));
/* Each conversation with the same person will have the same logging setting */
  if ((ggc -> list) != 0) {
    purple_conversation_set_logging(conv,purple_conversation_is_logging(( *(ggc -> list)).data));
  }
  ggc -> list = g_list_prepend((ggc -> list),conv);
  ggc -> active_conv = conv;
  conv -> ui_data = ggc;
  if (((cc != 0) && ((cc -> ui_data) != 0)) && (cc != conv)) {
    finch_conversation_set_active(conv);
    return ;
  }
  type = purple_conversation_get_type(conv);
  title = get_conversation_title(conv,account);
  ggc -> window = gnt_window_box_new(0,1);
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> window)),gnt_box_get_gtype()))),title);
  gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> window)),gnt_box_get_gtype()))),1);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> window)),gnt_box_get_gtype()))),0);
  switch(purple_conversation_get_type(conv)){
    case PURPLE_CONV_TYPE_UNKNOWN:
{
      gnt_widget_set_name((ggc -> window),"conversation-window-unknown");
      break; 
    }
    case PURPLE_CONV_TYPE_IM:
{
      gnt_widget_set_name((ggc -> window),"conversation-window-im");
      break; 
    }
    case PURPLE_CONV_TYPE_CHAT:
{
      gnt_widget_set_name((ggc -> window),"conversation-window-chat");
      break; 
    }
    case PURPLE_CONV_TYPE_MISC:
{
      gnt_widget_set_name((ggc -> window),"conversation-window-misc");
      break; 
    }
    case PURPLE_CONV_TYPE_ANY:
{
      gnt_widget_set_name((ggc -> window),"conversation-window-any");
      break; 
    }
  }
  ggc -> tv = gnt_text_view_new();
  gnt_widget_set_name((ggc -> tv),"conversation-window-textview");
  gnt_widget_set_size((ggc -> tv),purple_prefs_get_int("/finch/conversations/size/width"),purple_prefs_get_int("/finch/conversations/size/height"));
  if (type == PURPLE_CONV_TYPE_CHAT) {
    GntWidget *hbox;
    GntWidget *tree;
    FinchConvChat *fc = (ggc -> u.chat = ((FinchConvChat *)(g_malloc0_n(1,(sizeof(FinchConvChat ))))));
    hbox = gnt_box_new(0,0);
    gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),0);
    tree = (fc -> userlist = gnt_tree_new_with_columns(2));
/* The flag column */
    gnt_tree_set_col_width(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),0,1);
    gnt_tree_set_compare_func(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),((GCompareFunc )g_utf8_collate));
    gnt_tree_set_hash_fns(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),g_str_hash,g_str_equal,g_free);
    gnt_tree_set_search_column(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),1);
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_NO_BORDER;
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),(ggc -> tv));
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),tree);
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> window)),gnt_box_get_gtype()))),hbox);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"activate",((GCallback )create_conv_from_userlist),ggc,0,((GConnectFlags )0));
    gnt_widget_set_visible(tree,purple_prefs_get_bool("/finch/conversations/chats/userlist"));
  }
  else {
    ggc -> u.im = ((FinchConvIm *)(g_malloc0_n(1,(sizeof(FinchConvIm )))));
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> window)),gnt_box_get_gtype()))),(ggc -> tv));
  }
  ggc -> info = gnt_box_new(0,1);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> window)),gnt_box_get_gtype()))),(ggc -> info));
  ggc -> entry = gnt_entry_new(0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> window)),gnt_box_get_gtype()))),(ggc -> entry));
  gnt_widget_set_name((ggc -> entry),"conversation-window-entry");
  gnt_entry_set_history_length(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> entry)),gnt_entry_get_gtype()))),-1);
  gnt_entry_set_word_suggest(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> entry)),gnt_entry_get_gtype()))),1);
  gnt_entry_set_always_suggest(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> entry)),gnt_entry_get_gtype()))),0);
  gnt_text_view_attach_scroll_widget(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> tv)),gnt_text_view_get_gtype()))),(ggc -> entry));
  gnt_text_view_attach_pager_widget(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> tv)),gnt_text_view_get_gtype()))),(ggc -> entry));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> entry)),((GType )(20 << 2))))),"activate",((GCallback )entry_key_pressed),ggc,0,G_CONNECT_AFTER);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> entry)),((GType )(20 << 2))))),"completion",((GCallback )completion_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> window)),((GType )(20 << 2))))),"destroy",((GCallback )closing_window),ggc,0,((GConnectFlags )0));
  gnt_widget_set_position((ggc -> window),purple_prefs_get_int("/finch/conversations/position/x"),purple_prefs_get_int("/finch/conversations/position/y"));
  gnt_widget_show((ggc -> window));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> tv)),((GType )(20 << 2))))),"size_changed",((GCallback )size_changed_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> window)),((GType )(20 << 2))))),"position_set",((GCallback )save_position_cb),0,0,((GConnectFlags )0));
  if (type == PURPLE_CONV_TYPE_IM) {
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> entry)),((GType )(20 << 2))))),"text_changed",((GCallback )send_typing_notification),ggc,0,((GConnectFlags )0));
  }
  convnode = get_conversation_blist_node(conv);
  if (((convnode != 0) && (purple_blist_node_get_bool(convnode,"gnt-mute-sound") != 0)) || !(finch_sound_is_enabled() != 0)) 
    ggc -> flags |= FINCH_CONV_NO_SOUND;
  gg_create_menu(ggc);
  gg_setup_commands(ggc,0);
  purple_signal_connect(purple_cmds_get_handle(),"cmd-added",ggc,((GCallback )cmd_added_cb),ggc);
  purple_signal_connect(purple_cmds_get_handle(),"cmd-removed",ggc,((GCallback )cmd_removed_cb),ggc);
  g_free(title);
  gnt_box_give_focus_to_child(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> window)),gnt_box_get_gtype()))),(ggc -> entry));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> window)),((GType )(20 << 2))))),"gained-focus",((GCallback )gained_focus_cb),ggc,0,((GConnectFlags )0));
}

static void finch_destroy_conversation(PurpleConversation *conv)
{
/* do stuff here */
  FinchConv *ggc = (conv -> ui_data);
  ggc -> list = g_list_remove((ggc -> list),conv);
  if (((ggc -> list) != 0) && (conv == (ggc -> active_conv))) {
    ggc -> active_conv = ( *(ggc -> list)).data;
    gg_setup_commands(ggc,1);
  }
  if ((ggc -> list) == ((GList *)((void *)0))) {
    g_free(ggc -> u.chat);
    purple_signals_disconnect_by_handle(ggc);
    if ((ggc -> window) != 0) 
      gnt_widget_destroy((ggc -> window));
    g_free(ggc);
  }
}

static void finch_write_common(PurpleConversation *conv,const char *who,const char *message,PurpleMessageFlags flags,time_t mtime)
{
  FinchConv *ggconv = (conv -> ui_data);
  char *strip;
  char *newline;
  GntTextFormatFlags fl = 0;
  int pos;
  do {
    if (ggconv != ((FinchConv *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ggconv != NULL");
      return ;
    };
  }while (0);
  if (((flags & PURPLE_MESSAGE_SYSTEM) != 0U) && !((flags & PURPLE_MESSAGE_NOTIFY) != 0U)) {
    flags &= (~(PURPLE_MESSAGE_SEND | PURPLE_MESSAGE_RECV));
  }
  if ((ggconv -> active_conv) != conv) {
    if ((flags & (PURPLE_MESSAGE_SEND | PURPLE_MESSAGE_RECV)) != 0U) 
      finch_conversation_set_active(conv);
    else 
      return ;
  }
  pos = gnt_text_view_get_lines_below(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(ggconv -> tv)),gnt_text_view_get_gtype()))));
  gnt_text_view_tag_change(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(ggconv -> tv)),gnt_text_view_get_gtype()))),"typing",0,1);
  gnt_text_view_append_text_with_flags(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(ggconv -> tv)),gnt_text_view_get_gtype()))),"\n",GNT_TEXT_FLAG_NORMAL);
/* Unnecessary to print the timestamp for delayed message */
  if (purple_prefs_get_bool("/finch/conversations/timestamps") != 0) {
    if (!(mtime != 0L)) 
      time(&mtime);
    gnt_text_view_append_text_with_flags(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(ggconv -> tv)),gnt_text_view_get_gtype()))),purple_utf8_strftime("(%H:%M:%S)",(localtime((&mtime)))),(gnt_color_pair(color_timestamp)));
  }
  gnt_text_view_append_text_with_flags(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(ggconv -> tv)),gnt_text_view_get_gtype())))," ",GNT_TEXT_FLAG_NORMAL);
  if ((flags & PURPLE_MESSAGE_AUTO_RESP) != 0U) 
    gnt_text_view_append_text_with_flags(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(ggconv -> tv)),gnt_text_view_get_gtype()))),((const char *)(dgettext("pidgin","<AUTO-REPLY> "))),GNT_TEXT_FLAG_BOLD);
  if ((((who != 0) && (( *who) != 0)) && ((flags & (PURPLE_MESSAGE_SEND | PURPLE_MESSAGE_RECV)) != 0U)) && !((flags & PURPLE_MESSAGE_NOTIFY) != 0U)) {
    char *name = (char *)((void *)0);
    GntTextFormatFlags msgflags = GNT_TEXT_FLAG_NORMAL;
    gboolean me = 0;
    if (purple_message_meify(((char *)message),(-1)) != 0) {
      name = g_strdup_printf("*** %s",who);
      if (!((flags & PURPLE_MESSAGE_SEND) != 0U) && ((flags & PURPLE_MESSAGE_NICK) != 0U)) 
        msgflags = (gnt_color_pair(color_message_highlight));
      else 
        msgflags = (gnt_color_pair(color_message_action));
      me = 1;
    }
    else {
      name = g_strdup_printf("%s",who);
      if ((flags & PURPLE_MESSAGE_SEND) != 0U) 
        msgflags = (gnt_color_pair(color_message_send));
      else if ((flags & PURPLE_MESSAGE_NICK) != 0U) 
        msgflags = (gnt_color_pair(color_message_highlight));
      else 
        msgflags = (gnt_color_pair(color_message_receive));
    }
    gnt_text_view_append_text_with_flags(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(ggconv -> tv)),gnt_text_view_get_gtype()))),name,msgflags);
    gnt_text_view_append_text_with_flags(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(ggconv -> tv)),gnt_text_view_get_gtype()))),((me != 0)?" " : ": "),GNT_TEXT_FLAG_NORMAL);
    g_free(name);
  }
  else 
    fl = GNT_TEXT_FLAG_DIM;
  if ((flags & PURPLE_MESSAGE_ERROR) != 0U) 
    fl |= GNT_TEXT_FLAG_BOLD;
/* XXX: Remove this workaround when textview can parse messages. */
  newline = purple_strdup_withhtml(message);
  strip = purple_markup_strip_html(newline);
  gnt_text_view_append_text_with_flags(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(ggconv -> tv)),gnt_text_view_get_gtype()))),strip,fl);
  g_free(newline);
  g_free(strip);
  if (((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) && ((purple_conv_im_get_typing_state((purple_conversation_get_im_data(conv)))) == PURPLE_TYPING)) {
    strip = g_strdup_printf(((const char *)(dgettext("pidgin","\n%s is typing..."))),purple_conversation_get_title(conv));
    gnt_text_view_append_text_with_tag(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(ggconv -> tv)),gnt_text_view_get_gtype()))),strip,GNT_TEXT_FLAG_DIM,"typing");
    g_free(strip);
  }
  if (pos <= 1) 
    gnt_text_view_scroll(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(ggconv -> tv)),gnt_text_view_get_gtype()))),0);
  if ((flags & (PURPLE_MESSAGE_RECV | PURPLE_MESSAGE_NICK | PURPLE_MESSAGE_ERROR)) != 0U) 
    gnt_widget_set_urgent((ggconv -> tv));
  if (((flags & PURPLE_MESSAGE_RECV) != 0U) && !(gnt_widget_has_focus((ggconv -> window)) != 0)) {
    int count = (gint )((glong )(purple_conversation_get_data(conv,"unseen-count")));
    purple_conversation_set_data(conv,"unseen-count",((gpointer )((glong )(count + 1))));
    purple_conversation_update(conv,PURPLE_CONV_UPDATE_UNSEEN);
  }
}

static void finch_write_chat(PurpleConversation *conv,const char *who,const char *message,PurpleMessageFlags flags,time_t mtime)
{
  purple_conversation_write(conv,who,message,flags,mtime);
}

static void finch_write_im(PurpleConversation *conv,const char *who,const char *message,PurpleMessageFlags flags,time_t mtime)
{
  PurpleAccount *account = purple_conversation_get_account(conv);
  if ((flags & PURPLE_MESSAGE_SEND) != 0U) {
    who = purple_connection_get_display_name((purple_account_get_connection(account)));
    if (!(who != 0)) 
      who = purple_account_get_alias(account);
    if (!(who != 0)) 
      who = purple_account_get_username(account);
  }
  else if ((flags & PURPLE_MESSAGE_RECV) != 0U) {
    PurpleBuddy *buddy;
    who = purple_conversation_get_name(conv);
    buddy = purple_find_buddy(account,who);
    if (buddy != 0) 
      who = purple_buddy_get_contact_alias(buddy);
  }
  purple_conversation_write(conv,who,message,flags,mtime);
}

static void finch_write_conv(PurpleConversation *conv,const char *who,const char *alias,const char *message,PurpleMessageFlags flags,time_t mtime)
{
  const char *name;
  if ((alias != 0) && (( *alias) != 0)) 
    name = alias;
  else if ((who != 0) && (( *who) != 0)) 
    name = who;
  else 
    name = ((const char *)((void *)0));
  finch_write_common(conv,name,message,flags,mtime);
}

static const char *chat_flag_text(PurpleConvChatBuddyFlags flags)
{
  if ((flags & PURPLE_CBFLAGS_FOUNDER) != 0U) 
    return "~";
  if ((flags & PURPLE_CBFLAGS_OP) != 0U) 
    return "@";
  if ((flags & PURPLE_CBFLAGS_HALFOP) != 0U) 
    return "%";
  if ((flags & PURPLE_CBFLAGS_VOICE) != 0U) 
    return "+";
  return " ";
}

static void finch_chat_add_users(PurpleConversation *conv,GList *users,gboolean new_arrivals)
{
  FinchConv *ggc = (conv -> ui_data);
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> entry)),gnt_entry_get_gtype()));
  if (!(new_arrivals != 0)) {
/* Print the list of users in the room */
    GString *string = g_string_new(0);
    GList *iter;
    int count = (g_list_length(users));
    g_string_printf(string,(ngettext("List of %d user:\n","List of %d users:\n",count)),count);
    for (iter = users; iter != 0; iter = (iter -> next)) {
      PurpleConvChatBuddy *cbuddy = (iter -> data);
      char *str;
      if ((str = (cbuddy -> alias)) == ((char *)((void *)0))) 
        str = (cbuddy -> name);
      g_string_append_printf(string,"[ %s ]",str);
    }
    purple_conversation_write(conv,0,(string -> str),PURPLE_MESSAGE_SYSTEM,time(0));
    g_string_free(string,1);
  }
  for (; users != 0; users = (users -> next)) {
    PurpleConvChatBuddy *cbuddy = (users -> data);
    GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)( *ggc -> u.chat).userlist),gnt_tree_get_gtype()));
    gnt_entry_add_suggest(entry,(cbuddy -> name));
    gnt_entry_add_suggest(entry,(cbuddy -> alias));
    gnt_tree_add_row_after(tree,(g_strdup((cbuddy -> name))),gnt_tree_create_row(tree,chat_flag_text((cbuddy -> flags)),(cbuddy -> alias)),0,0);
  }
}

static void finch_chat_rename_user(PurpleConversation *conv,const char *old,const char *new_n,const char *new_a)
{
/* Update the name for string completion */
  FinchConv *ggc = (conv -> ui_data);
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> entry)),gnt_entry_get_gtype()));
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)( *ggc -> u.chat).userlist),gnt_tree_get_gtype()));
  PurpleConvChatBuddy *cb = purple_conv_chat_cb_find(purple_conversation_get_chat_data(conv),new_n);
  gnt_entry_remove_suggest(entry,old);
  gnt_tree_remove(tree,((gpointer )old));
  gnt_entry_add_suggest(entry,new_n);
  gnt_entry_add_suggest(entry,new_a);
  gnt_tree_add_row_after(tree,(g_strdup(new_n)),gnt_tree_create_row(tree,chat_flag_text((cb -> flags)),new_a),0,0);
}

static void finch_chat_remove_users(PurpleConversation *conv,GList *list)
{
/* Remove the name from string completion */
  FinchConv *ggc = (conv -> ui_data);
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(ggc -> entry)),gnt_entry_get_gtype()));
  for (; list != 0; list = (list -> next)) {
    GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)( *ggc -> u.chat).userlist),gnt_tree_get_gtype()));
    gnt_entry_remove_suggest(entry,(list -> data));
    gnt_tree_remove(tree,(list -> data));
  }
}

static void finch_chat_update_user(PurpleConversation *conv,const char *user)
{
  PurpleConvChatBuddy *cb = purple_conv_chat_cb_find(purple_conversation_get_chat_data(conv),user);
  FinchConv *ggc = (conv -> ui_data);
  gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)( *ggc -> u.chat).userlist),gnt_tree_get_gtype()))),((gpointer )user),0,chat_flag_text((cb -> flags)));
}

static void finch_conv_present(PurpleConversation *conv)
{
  FinchConv *fc = (FinchConv *)(conv -> ui_data);
  if ((fc != 0) && ((fc -> window) != 0)) 
    gnt_window_present((fc -> window));
}

static gboolean finch_conv_has_focus(PurpleConversation *conv)
{
  FinchConv *fc = (FinchConv *)(conv -> ui_data);
  if ((fc != 0) && ((fc -> window) != 0)) 
    return gnt_widget_has_focus((fc -> window));
  return 0;
}
static PurpleConversationUiOps conv_ui_ops = {(finch_create_conversation), (finch_destroy_conversation), (finch_write_chat), (finch_write_im), (finch_write_conv), (finch_chat_add_users), (finch_chat_rename_user), (finch_chat_remove_users), (finch_chat_update_user), (finch_conv_present), (finch_conv_has_focus), ((gboolean (*)(PurpleConversation *, const char *, gboolean ))((void *)0)), ((void (*)(PurpleConversation *, const char *, const guchar *, gsize ))((void *)0)), ((void (*)(PurpleConversation *, const char *))((void *)0)), ((void (*)(PurpleConversation *, const char *))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* present */
/* has_focus */
/* custom_smiley_add */
/* custom_smiley_write */
/* custom_smiley_close */
/* send_confirm */
};

PurpleConversationUiOps *finch_conv_get_ui_ops()
{
  return &conv_ui_ops;
}
/* Xerox */

static PurpleCmdRet say_command_cb(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) 
    purple_conv_im_send(purple_conversation_get_im_data(conv),args[0]);
  else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) 
    purple_conv_chat_send(purple_conversation_get_chat_data(conv),args[0]);
  return PURPLE_CMD_RET_OK;
}
/* Xerox */

static PurpleCmdRet me_command_cb(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  char *tmp;
  tmp = g_strdup_printf("/me %s",args[0]);
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) 
    purple_conv_im_send(purple_conversation_get_im_data(conv),tmp);
  else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) 
    purple_conv_chat_send(purple_conversation_get_chat_data(conv),tmp);
  g_free(tmp);
  return PURPLE_CMD_RET_OK;
}
/* Xerox */

static PurpleCmdRet debug_command_cb(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  char *tmp;
  char *markup;
  if (!(g_ascii_strcasecmp(args[0],"version") != 0)) {
    tmp = g_strdup_printf("Using Finch v%s with libpurple v%s.","2.10.9",purple_core_get_version());
  }
  else if (!(g_ascii_strcasecmp(args[0],"plugins") != 0)) {
/* Show all the loaded plugins, including the protocol plugins and plugin loaders.
		 * This is intentional, since third party prpls are often sources of bugs, and some
		 * plugin loaders (e.g. mono) can also be buggy.
		 */
    GString *str = g_string_new("Loaded Plugins: ");
    const GList *plugins = (purple_plugins_get_loaded());
    if (plugins != 0) {
      for (; plugins != 0; plugins = (plugins -> next)) {
        str = g_string_append(str,purple_plugin_get_name((plugins -> data)));
        if ((plugins -> next) != 0) 
          str = g_string_append(str,", ");
      }
    }
    else {
      str = g_string_append(str,"(none)");
    }
    tmp = g_string_free(str,0);
  }
  else {
    purple_conversation_write(conv,0,((const char *)(dgettext("pidgin","Supported debug options are: plugins version"))),(PURPLE_MESSAGE_NO_LOG | PURPLE_MESSAGE_ERROR),time(0));
    return PURPLE_CMD_RET_OK;
  }
  markup = g_markup_escape_text(tmp,(-1));
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) 
    purple_conv_im_send(purple_conversation_get_im_data(conv),markup);
  else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) 
    purple_conv_chat_send(purple_conversation_get_chat_data(conv),markup);
  g_free(tmp);
  g_free(markup);
  return PURPLE_CMD_RET_OK;
}
/* Xerox */

static PurpleCmdRet clear_command_cb(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  purple_conversation_clear_message_history(conv);
  return PURPLE_CMD_RET_OK;
}
/* Xerox */

static PurpleCmdRet help_command_cb(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  GList *l;
  GList *text;
  GString *s;
  if (args[0] != ((char *)((void *)0))) {
    s = g_string_new("");
    text = purple_cmd_help(conv,args[0]);
    if (text != 0) {
      for (l = text; l != 0; l = (l -> next)) 
        if ((l -> next) != 0) 
          g_string_append_printf(s,"%s\n",((char *)(l -> data)));
        else 
          g_string_append_printf(s,"%s",((char *)(l -> data)));
    }
    else {
      g_string_append(s,((const char *)(dgettext("pidgin","No such command (in this context)."))));
    }
  }
  else {
    s = g_string_new(((const char *)(dgettext("pidgin","Use \"/help &lt;command&gt;\" for help on a specific command.\nThe following commands are available in this context:\n"))));
    text = purple_cmd_list(conv);
    for (l = text; l != 0; l = (l -> next)) 
      if ((l -> next) != 0) 
        g_string_append_printf(s,"%s, ",((char *)(l -> data)));
      else 
        g_string_append_printf(s,"%s.",((char *)(l -> data)));
    g_list_free(text);
  }
  purple_conversation_write(conv,0,(s -> str),PURPLE_MESSAGE_NO_LOG,time(0));
  g_string_free(s,1);
  return PURPLE_CMD_RET_OK;
}

static PurpleCmdRet cmd_show_window(PurpleConversation *conv,const char *cmd,char **args,char **error,gpointer data)
{
  void (*callback)() = data;
  ( *callback)();
  return PURPLE_CMD_RET_OK;
}

static PurpleCmdRet cmd_message_color(PurpleConversation *conv,const char *cmd,char **args,char **error,gpointer data)
{
  int *msgclass = (int *)((void *)0);
  int fg;
  int bg;
  if (strcmp(args[0],"receive") == 0) 
    msgclass = &color_message_receive;
  else if (strcmp(args[0],"send") == 0) 
    msgclass = &color_message_send;
  else if (strcmp(args[0],"highlight") == 0) 
    msgclass = &color_message_highlight;
  else if (strcmp(args[0],"action") == 0) 
    msgclass = &color_message_action;
  else if (strcmp(args[0],"timestamp") == 0) 
    msgclass = &color_timestamp;
  else {
    if (error != 0) 
       *error = g_strdup_printf(((const char *)(dgettext("pidgin","%s is not a valid message class. See \'/help msgcolor\' for valid message classes."))),args[0]);
    return PURPLE_CMD_RET_FAILED;
  }
  fg = gnt_colors_get_color(args[1]);
  if (fg == -22) {
    if (error != 0) 
       *error = g_strdup_printf(((const char *)(dgettext("pidgin","%s is not a valid color. See \'/help msgcolor\' for valid colors."))),args[1]);
    return PURPLE_CMD_RET_FAILED;
  }
  bg = gnt_colors_get_color(args[2]);
  if (bg == -22) {
    if (error != 0) 
       *error = g_strdup_printf(((const char *)(dgettext("pidgin","%s is not a valid color. See \'/help msgcolor\' for valid colors."))),args[2]);
    return PURPLE_CMD_RET_FAILED;
  }
  init_pair(( *msgclass),fg,bg);
  return PURPLE_CMD_RET_OK;
}

static PurpleCmdRet users_command_cb(PurpleConversation *conv,const char *cmd,char **args,char **error,gpointer data)
{
  FinchConv *fc = (conv -> ui_data);
  FinchConvChat *ch;
  if (!(fc != 0)) 
    return PURPLE_CMD_RET_FAILED;
  ch = fc -> u.chat;
  gnt_widget_set_visible((ch -> userlist),(( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(ch -> userlist)),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_INVISIBLE));
  gnt_box_readjust(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(fc -> window)),gnt_box_get_gtype()))));
  gnt_box_give_focus_to_child(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(fc -> window)),gnt_box_get_gtype()))),(fc -> entry));
  purple_prefs_set_bool("/finch/conversations/chats/userlist",!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(ch -> userlist)),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_INVISIBLE) != 0U));
  return PURPLE_CMD_RET_OK;
}

void finch_conversation_init()
{
  color_message_send = gnt_style_get_color(0,"color-message-sent");
  if (!(color_message_send != 0)) 
    color_message_send = gnt_color_add_pair(6,-1);
  color_message_receive = gnt_style_get_color(0,"color-message-received");
  if (!(color_message_receive != 0)) 
    color_message_receive = gnt_color_add_pair(1,-1);
  color_message_highlight = gnt_style_get_color(0,"color-message-highlight");
  if (!(color_message_highlight != 0)) 
    color_message_highlight = gnt_color_add_pair(2,-1);
  color_timestamp = gnt_style_get_color(0,"color-timestamp");
  if (!(color_timestamp != 0)) 
    color_timestamp = gnt_color_add_pair(4,-1);
  color_message_action = gnt_style_get_color(0,"color-message-action");
  if (!(color_message_action != 0)) 
    color_message_action = gnt_color_add_pair(3,-1);
  purple_prefs_add_none("/finch/conversations");
  purple_prefs_add_none("/finch/conversations/size");
  purple_prefs_add_int("/finch/conversations/size/width",70);
  purple_prefs_add_int("/finch/conversations/size/height",20);
  purple_prefs_add_none("/finch/conversations/position");
  purple_prefs_add_int("/finch/conversations/position/x",0);
  purple_prefs_add_int("/finch/conversations/position/y",0);
  purple_prefs_add_none("/finch/conversations/chats");
  purple_prefs_add_bool("/finch/conversations/chats/userlist",0);
/* Xerox the commands */
  purple_cmd_register("say","S",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM),0,say_command_cb,((const char *)(dgettext("pidgin","say &lt;message&gt;:  Send a message normally as if you weren\'t using a command."))),0);
  purple_cmd_register("me","S",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM),0,me_command_cb,((const char *)(dgettext("pidgin","me &lt;action&gt;:  Send an IRC style action to a buddy or chat."))),0);
  purple_cmd_register("debug","w",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM),0,debug_command_cb,((const char *)(dgettext("pidgin","debug &lt;option&gt;:  Send various debug information to the current conversation."))),0);
  purple_cmd_register("clear","",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM),0,clear_command_cb,((const char *)(dgettext("pidgin","clear: Clears the conversation scrollback."))),0);
  purple_cmd_register("help","w",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM | PURPLE_CMD_FLAG_ALLOW_WRONG_ARGS),0,help_command_cb,((const char *)(dgettext("pidgin","help &lt;command&gt;:  Help on a specific command."))),0);
  purple_cmd_register("users","",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_ALLOW_WRONG_ARGS),0,users_command_cb,((const char *)(dgettext("pidgin","users:  Show the list of users in the chat."))),0);
/* Now some commands to bring up some other windows */
  purple_cmd_register("plugins","",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM),0,cmd_show_window,((const char *)(dgettext("pidgin","plugins: Show the plugins window."))),finch_plugins_show_all);
  purple_cmd_register("buddylist","",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM),0,cmd_show_window,((const char *)(dgettext("pidgin","buddylist: Show the buddylist."))),finch_blist_show);
  purple_cmd_register("accounts","",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM),0,cmd_show_window,((const char *)(dgettext("pidgin","accounts: Show the accounts window."))),finch_accounts_show_all);
  purple_cmd_register("debugwin","",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM),0,cmd_show_window,((const char *)(dgettext("pidgin","debugwin: Show the debug window."))),finch_debug_window_show);
  purple_cmd_register("prefs","",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM),0,cmd_show_window,((const char *)(dgettext("pidgin","prefs: Show the preference window."))),finch_prefs_show_all);
  purple_cmd_register("status","",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM),0,cmd_show_window,((const char *)(dgettext("pidgin","statuses: Show the savedstatuses window."))),finch_savedstatus_show_all);
/* Allow customizing the message colors using a command during run-time */
  purple_cmd_register("msgcolor","www",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM),0,cmd_message_color,((const char *)(dgettext("pidgin","msgcolor &lt;class&gt; &lt;foreground&gt; &lt;background&gt;: Set the color for different classes of messages in the conversation window.<br>    &lt;class&gt;: receive, send, highlight, action, timestamp<br>    &lt;foreground/background&gt;: black, red, green, blue, white, gray, darkgray, magenta, cyan, default<br><br>EXAMPLE:<br>    msgcolor send cyan default"))),0);
  purple_cmd_register("msgcolour","www",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM),0,cmd_message_color,((const char *)(dgettext("pidgin","msgcolor &lt;class&gt; &lt;foreground&gt; &lt;background&gt;: Set the color for different classes of messages in the conversation window.<br>    &lt;class&gt;: receive, send, highlight, action, timestamp<br>    &lt;foreground/background&gt;: black, red, green, blue, white, gray, darkgray, magenta, cyan, default<br><br>EXAMPLE:<br>    msgcolor send cyan default"))),0);
  purple_signal_connect(purple_conversations_get_handle(),"buddy-typing",finch_conv_get_handle(),((PurpleCallback )update_buddy_typing),0);
  purple_signal_connect(purple_conversations_get_handle(),"buddy-typing-stopped",finch_conv_get_handle(),((PurpleCallback )update_buddy_typing),0);
  purple_signal_connect(purple_conversations_get_handle(),"chat-left",finch_conv_get_handle(),((PurpleCallback )chat_left_cb),0);
  purple_signal_connect(purple_conversations_get_handle(),"cleared-message-history",finch_conv_get_handle(),((PurpleCallback )cleared_message_history_cb),0);
  purple_signal_connect(purple_blist_get_handle(),"buddy-signed-on",finch_conv_get_handle(),((PurpleCallback )buddy_signed_on_off),0);
  purple_signal_connect(purple_blist_get_handle(),"buddy-signed-off",finch_conv_get_handle(),((PurpleCallback )buddy_signed_on_off),0);
  purple_signal_connect(purple_connections_get_handle(),"signed-on",finch_conv_get_handle(),((PurpleCallback )account_signed_on_off),0);
  purple_signal_connect(purple_connections_get_handle(),"signed-off",finch_conv_get_handle(),((PurpleCallback )account_signed_on_off),0);
  purple_signal_connect(purple_connections_get_handle(),"signing-off",finch_conv_get_handle(),((PurpleCallback )account_signing_off),0);
}

void finch_conversation_uninit()
{
  purple_signals_disconnect_by_handle(finch_conv_get_handle());
}

void finch_conversation_set_active(PurpleConversation *conv)
{
  FinchConv *ggconv = (conv -> ui_data);
  PurpleAccount *account;
  char *title;
  do {
    if (ggconv != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ggconv");
      return ;
    };
  }while (0);
  do {
    if (g_list_find((ggconv -> list),conv) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"g_list_find(ggconv->list, conv)");
      return ;
    };
  }while (0);
  if ((ggconv -> active_conv) == conv) 
    return ;
  ggconv -> active_conv = conv;
  gg_setup_commands(ggconv,1);
  account = purple_conversation_get_account(conv);
  title = get_conversation_title(conv,account);
  gnt_screen_rename_widget((ggconv -> window),title);
  g_free(title);
}

void finch_conversation_set_info_widget(PurpleConversation *conv,GntWidget *widget)
{
  FinchConv *fc = (conv -> ui_data);
  int height;
  int width;
  gnt_box_remove_all(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(fc -> info)),gnt_box_get_gtype()))));
  if (widget != 0) {
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(fc -> info)),gnt_box_get_gtype()))),widget);
    gnt_box_readjust(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(fc -> info)),gnt_box_get_gtype()))));
  }
  gnt_widget_get_size((fc -> window),&width,&height);
  gnt_box_readjust(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(fc -> window)),gnt_box_get_gtype()))));
  gnt_screen_resize_widget((fc -> window),width,height);
  gnt_box_give_focus_to_child(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(fc -> window)),gnt_box_get_gtype()))),(fc -> entry));
}
