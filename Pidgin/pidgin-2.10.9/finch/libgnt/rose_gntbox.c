/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "gntinternal.h"
#include "gntbox.h"
#include "gntstyle.h"
#include "gntutils.h"
#include <string.h>
#define PROP_LAST_RESIZE_S "last-resize"
#define PROP_SIZE_QUEUED_S "size-queued"
enum __unnamed_enum___F0_L33_C1_PROP_0__COMMA__PROP_VERTICAL__COMMA__PROP_HOMO {PROP_0,PROP_VERTICAL,
/* ... */
PROP_HOMO};
enum __unnamed_enum___F0_L40_C1_SIGS {SIGS=1};
static GntWidgetClass *parent_class = (GntWidgetClass *)((void *)0);
static GntWidget *find_focusable_widget(GntBox *box);

static void add_to_focus(gpointer value,gpointer data)
{
  GntBox *box = (GntBox *)(g_type_check_instance_cast(((GTypeInstance *)data),gnt_box_get_gtype()));
  GntWidget *w = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)value),gnt_widget_get_gtype()));
  if ((({
    GTypeInstance *__inst = (GTypeInstance *)w;
    GType __t = gnt_box_get_gtype();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = 1;
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
    g_list_foreach(( *((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)w),gnt_box_get_gtype())))).list,add_to_focus,box);
  else if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)w),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_CAN_TAKE_FOCUS) != 0U) 
    box -> focus = g_list_append((box -> focus),w);
}

static void get_title_thingies(GntBox *box,char *title,int *p,int *r)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype()));
  int len;
  char *end = (char *)(gnt_util_onscreen_width_to_pointer(title,(widget -> priv.width - 4),&len));
  if (p != 0) 
     *p = ((widget -> priv.width - len) / 2);
  if (r != 0) 
     *r = ((widget -> priv.width + len) / 2);
   *end = 0;
}

static void gnt_box_draw(GntWidget *widget)
{
  GntBox *box = (GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype()));
  if (((box -> focus) == ((GList *)((void *)0))) && ((widget -> parent) == ((GntWidget *)((void *)0)))) 
    g_list_foreach((box -> list),add_to_focus,box);
  g_list_foreach((box -> list),((GFunc )gnt_widget_draw),0);
  if (((box -> title) != 0) && !((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_NO_BORDER) != 0U)) {
    int pos;
    int right;
    char *title = g_strdup((box -> title));
    get_title_thingies(box,title,&pos,&right);
    if (gnt_widget_has_focus(widget) != 0) 
      wbkgdset((widget -> window),(0 | gnt_color_pair(GNT_COLOR_TITLE)));
    else 
      wbkgdset((widget -> window),(0 | gnt_color_pair(GNT_COLOR_TITLE_D)));
    (wmove((widget -> window),0,(pos - 1)) == -1)?-1 : waddch((widget -> window),(acs_map[(unsigned char )'u'] | (gnt_color_pair(GNT_COLOR_NORMAL))));
    (wmove((widget -> window),0,pos) == -1)?-1 : waddnstr((widget -> window),C_(title),-1);
    (wmove((widget -> window),0,right) == -1)?-1 : waddch((widget -> window),(acs_map[(unsigned char )'t'] | (gnt_color_pair(GNT_COLOR_NORMAL))));
    g_free(title);
  }
  gnt_box_sync_children(box);
}

static void reposition_children(GntWidget *widget)
{
  GList *iter;
  GntBox *box = (GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype()));
  int w;
  int h;
  int curx;
  int cury;
  int max;
  gboolean has_border = 0;
  w = (h = 0);
  max = 0;
  curx = widget -> priv.x;
  cury = widget -> priv.y;
  if (!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_NO_BORDER) != 0U)) {
    has_border = 1;
    curx += 1;
    cury += 1;
  }
  for (iter = (box -> list); iter != 0; iter = (iter -> next)) {
    if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(iter -> data)),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_INVISIBLE) != 0U) 
      continue; 
    gnt_widget_set_position(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(iter -> data)),gnt_widget_get_gtype()))),curx,cury);
    gnt_widget_get_size(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(iter -> data)),gnt_widget_get_gtype()))),&w,&h);
    if ((box -> vertical) != 0) {
      if (h != 0) {
        cury += (h + (box -> pad));
        if (max < w) 
          max = w;
      }
    }
    else {
      if (w != 0) {
        curx += (w + (box -> pad));
        if (max < h) 
          max = h;
      }
    }
  }
  if (has_border != 0) {
    curx += 1;
    cury += 1;
    max += 2;
  }
  if ((box -> list) != 0) {
    if ((box -> vertical) != 0) 
      cury -= (box -> pad);
    else 
      curx -= (box -> pad);
  }
  if ((box -> vertical) != 0) {
    widget -> priv.width = max;
    widget -> priv.height = (cury - widget -> priv.y);
  }
  else {
    widget -> priv.width = (curx - widget -> priv.x);
    widget -> priv.height = max;
  }
}

static void gnt_box_set_position(GntWidget *widget,int x,int y)
{
  GList *iter;
  int changex;
  int changey;
  changex = (widget -> priv.x - x);
  changey = (widget -> priv.y - y);
  for (iter = ( *((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype())))).list; iter != 0; iter = (iter -> next)) {
    GntWidget *w = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(iter -> data)),gnt_widget_get_gtype()));
    gnt_widget_set_position(w,(w -> priv.x - changex),(w -> priv.y - changey));
  }
}

static void gnt_box_size_request(GntWidget *widget)
{
  GntBox *box = (GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype()));
  GList *iter;
  int maxw = 0;
  int maxh = 0;
  g_list_foreach((box -> list),((GFunc )gnt_widget_size_request),0);
  for (iter = (box -> list); iter != 0; iter = (iter -> next)) {
    int w;
    int h;
    gnt_widget_get_size(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(iter -> data)),gnt_widget_get_gtype()))),&w,&h);
    if (maxh < h) 
      maxh = h;
    if (maxw < w) 
      maxw = w;
  }
  for (iter = (box -> list); iter != 0; iter = (iter -> next)) {
    int w;
    int h;
    GntWidget *wid = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(iter -> data)),gnt_widget_get_gtype()));
    gnt_widget_get_size(wid,&w,&h);
    if ((box -> homogeneous) != 0) {
      if ((box -> vertical) != 0) 
        h = maxh;
      else 
        w = maxw;
    }
    if ((box -> fill) != 0) {
      if ((box -> vertical) != 0) 
        w = maxw;
      else 
        h = maxh;
    }
    gnt_widget_confirm_size(wid,w,h);
    gnt_widget_set_size(wid,w,h);
  }
  reposition_children(widget);
}

static void gnt_box_map(GntWidget *widget)
{
  if ((widget -> priv.width == 0) || (widget -> priv.height == 0)) {
    gnt_widget_size_request(widget);
    find_focusable_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype()))));
  };
}
/* Ensures that the current widget can take focus */

static GntWidget *find_focusable_widget(GntBox *box)
{
/* XXX: Make sure the widget is visible? */
  if (((box -> focus) == ((GList *)((void *)0))) && (( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype())))).parent == ((GntWidget *)((void *)0)))) 
    g_list_foreach((box -> list),add_to_focus,box);
  if (((box -> active) == ((GntWidget *)((void *)0))) && ((box -> focus) != 0)) 
    box -> active = ( *(box -> focus)).data;
  return box -> active;
}

static void find_next_focus(GntBox *box)
{
  gpointer last = (box -> active);
{
    do {
      GList *iter = g_list_find((box -> focus),(box -> active));
      if ((iter != 0) && ((iter -> next) != 0)) 
        box -> active = ( *(iter -> next)).data;
      else if ((box -> focus) != 0) 
        box -> active = ( *(box -> focus)).data;
      if (!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(box -> active)),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_INVISIBLE) != 0U) && ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(box -> active)),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_CAN_TAKE_FOCUS) != 0U)) 
        break; 
    }while ((box -> active) != last);
  }
}

static void find_prev_focus(GntBox *box)
{
  gpointer last = (box -> active);
  if (!((box -> focus) != 0)) 
    return ;
{
    do {
      GList *iter = g_list_find((box -> focus),(box -> active));
      if (!(iter != 0)) 
        box -> active = ( *(box -> focus)).data;
      else if (!((iter -> prev) != 0)) 
        box -> active = ( *g_list_last((box -> focus))).data;
      else 
        box -> active = ( *(iter -> prev)).data;
      if (!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(box -> active)),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_INVISIBLE) != 0U)) 
        break; 
    }while ((box -> active) != last);
  }
}

static gboolean gnt_box_key_pressed(GntWidget *widget,const char *text)
{
  GntBox *box = (GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype()));
  gboolean ret;
  if (!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_DISABLE_ACTIONS) != 0U)) 
    return 0;
  if (((box -> active) == ((GntWidget *)((void *)0))) && !(find_focusable_widget(box) != 0)) 
    return 0;
  if (gnt_widget_key_pressed((box -> active),text) != 0) 
    return 1;
/* This dance is necessary to make sure that the child widgets get a chance
	   to trigger their bindings first */
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_DISABLE_ACTIONS);
  ret = gnt_widget_key_pressed(widget,text);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_DISABLE_ACTIONS;
  return ret;
}

static gboolean box_focus_change(GntBox *box,gboolean next)
{
  GntWidget *now;
  now = (box -> active);
  if (next != 0) {
    find_next_focus(box);
  }
  else {
    find_prev_focus(box);
  }
  if ((now != 0) && (now != (box -> active))) {
    gnt_widget_set_focus(now,0);
    gnt_widget_set_focus((box -> active),1);
    return 1;
  }
  return 0;
}

static gboolean action_focus_next(GntBindable *bindable,GList *null)
{
  return box_focus_change(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_box_get_gtype()))),1);
}

static gboolean action_focus_prev(GntBindable *bindable,GList *null)
{
  return box_focus_change(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_box_get_gtype()))),0);
}

static void gnt_box_lost_focus(GntWidget *widget)
{
  GntWidget *w = ( *((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype())))).active;
  if (w != 0) 
    gnt_widget_set_focus(w,0);
  gnt_widget_draw(widget);
}

static void gnt_box_gained_focus(GntWidget *widget)
{
  GntWidget *w = ( *((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype())))).active;
  if (w != 0) 
    gnt_widget_set_focus(w,1);
  gnt_widget_draw(widget);
}

static void gnt_box_destroy(GntWidget *w)
{
  GntBox *box = (GntBox *)(g_type_check_instance_cast(((GTypeInstance *)w),gnt_box_get_gtype()));
  gnt_box_remove_all(box);
  gnt_screen_release(w);
}

static void gnt_box_expose(GntWidget *widget,int x,int y,int width,int height)
{
  WINDOW *win = newwin(height,width,(widget -> priv.y + y),(widget -> priv.x + x));
  copywin((widget -> window),win,y,x,0,0,(height - 1),(width - 1),0);
  wrefresh(win);
  delwin(win);
}

static gboolean gnt_box_confirm_size(GntWidget *widget,int width,int height)
{
  GList *iter;
  GntBox *box = (GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype()));
  int wchange;
  int hchange;
  GntWidget *child;
  GntWidget *last;
  if (!((box -> list) != 0)) 
    return 1;
  wchange = (widget -> priv.width - width);
  hchange = (widget -> priv.height - height);
  if ((wchange == 0) && (hchange == 0)) 
/* Quit playing games with my size */
    return 1;
  child = ((GntWidget *)((void *)0));
  last = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)box),((GType )(20 << 2))))),"last-resize"));
{
/* First, make sure all the widgets will fit into the box after resizing. */
    for (iter = (box -> list); iter != 0; iter = (iter -> next)) {
      GntWidget *wid = (iter -> data);
      int w;
      int h;
      gnt_widget_get_size(wid,&w,&h);
      if ((((((wid != last) && !(child != 0)) && (w > 0)) && (h > 0)) && !((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)wid),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_INVISIBLE) != 0U)) && (gnt_widget_confirm_size(wid,(w - wchange),(h - hchange)) != 0)) {
        child = wid;
        break; 
      }
    }
  }
  if (!(child != 0) && ((child = last) != 0)) {
    int w;
    int h;
    gnt_widget_get_size(child,&w,&h);
    if (!(gnt_widget_confirm_size(child,(w - wchange),(h - hchange)) != 0)) 
      child = ((GntWidget *)((void *)0));
  }
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)box),((GType )(20 << 2))))),"size-queued",child);
  if (child != 0) {
    for (iter = (box -> list); iter != 0; iter = (iter -> next)) {{
        GntWidget *wid = (iter -> data);
        int w;
        int h;
        if (wid == child) 
          continue; 
        gnt_widget_get_size(wid,&w,&h);
        if ((box -> vertical) != 0) {
/* For a vertical box, if we are changing the width, make sure the widgets
				 * in the box will fit after resizing the width. */
          if (((wchange > 0) && (w >= child -> priv.width)) && !(gnt_widget_confirm_size(wid,(w - wchange),h) != 0)) 
            return 0;
        }
        else {
/* If we are changing the height, make sure the widgets in the box fit after
				 * the resize. */
          if (((hchange > 0) && (h >= child -> priv.height)) && !(gnt_widget_confirm_size(wid,w,(h - hchange)) != 0)) 
            return 0;
        }
      }
    }
  }
  return child != ((GntWidget *)((void *)0));
}

static void gnt_box_size_changed(GntWidget *widget,int oldw,int oldh)
{
  int wchange;
  int hchange;
  GList *i;
  GntBox *box = (GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype()));
  GntWidget *wid;
  int tw;
  int th;
  wchange = (widget -> priv.width - oldw);
  hchange = (widget -> priv.height - oldh);
  wid = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)box),((GType )(20 << 2))))),"size-queued"));
  if (wid != 0) {
    gnt_widget_get_size(wid,&tw,&th);
    gnt_widget_set_size(wid,(tw + wchange),(th + hchange));
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)box),((GType )(20 << 2))))),"size-queued",0);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)box),((GType )(20 << 2))))),"last-resize",wid);
  }
  if ((box -> vertical) != 0) 
    hchange = 0;
  else 
    wchange = 0;
  for (i = (box -> list); i != 0; i = (i -> next)) {
    if (wid != (i -> data)) {
      gnt_widget_get_size(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(i -> data)),gnt_widget_get_gtype()))),&tw,&th);
      gnt_widget_set_size((i -> data),(tw + wchange),(th + hchange));
    }
  }
  reposition_children(widget);
}

static gboolean gnt_box_clicked(GntWidget *widget,GntMouseEvent event,int cx,int cy)
{
  GList *iter;
  for (iter = ( *((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype())))).list; iter != 0; iter = (iter -> next)) {
    int x;
    int y;
    int w;
    int h;
    GntWidget *wid = (iter -> data);
    gnt_widget_get_position(wid,&x,&y);
    gnt_widget_get_size(wid,&w,&h);
    if ((((cx >= x) && (cx < (x + w))) && (cy >= y)) && (cy < (y + h))) {
      if ((event <= GNT_MIDDLE_MOUSE_DOWN) && ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)wid),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_CAN_TAKE_FOCUS) != 0U)) {
        while((widget -> parent) != 0)
          widget = (widget -> parent);
        gnt_box_give_focus_to_child(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype()))),wid);
      }
      return gnt_widget_clicked(wid,event,cx,cy);
    }
  }
  return 0;
}

static void gnt_box_set_property(GObject *obj,guint prop_id,const GValue *value,GParamSpec *spec)
{
  GntBox *box = (GntBox *)(g_type_check_instance_cast(((GTypeInstance *)obj),gnt_box_get_gtype()));
  switch(prop_id){
    case PROP_VERTICAL:
{
      box -> vertical = g_value_get_boolean(value);
      break; 
    }
    case PROP_HOMO:
{
      box -> homogeneous = g_value_get_boolean(value);
      break; 
    }
    default:
{
      do {
        g_log("Gnt",G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","gntbox.c",547,((const char *)__func__));
        return ;
      }while (0);
      break; 
    }
  }
}

static void gnt_box_get_property(GObject *obj,guint prop_id,GValue *value,GParamSpec *spec)
{
  GntBox *box = (GntBox *)(g_type_check_instance_cast(((GTypeInstance *)obj),gnt_box_get_gtype()));
  switch(prop_id){
    case PROP_VERTICAL:
{
      g_value_set_boolean(value,(box -> vertical));
      break; 
    }
    case PROP_HOMO:
{
      g_value_set_boolean(value,(box -> homogeneous));
      break; 
    }
    default:
{
      break; 
    }
  }
}

static void gnt_box_class_init(GntBoxClass *klass)
{
  GntBindableClass *bindable = (GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()));
  GObjectClass *gclass = (GObjectClass *)(g_type_check_class_cast(((GTypeClass *)klass),((GType )(20 << 2))));
  parent_class = ((GntWidgetClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_widget_get_gtype())));
  parent_class -> destroy = gnt_box_destroy;
  parent_class -> draw = gnt_box_draw;
  parent_class -> expose = gnt_box_expose;
  parent_class -> map = gnt_box_map;
  parent_class -> size_request = gnt_box_size_request;
  parent_class -> set_position = gnt_box_set_position;
  parent_class -> key_pressed = gnt_box_key_pressed;
  parent_class -> clicked = gnt_box_clicked;
  parent_class -> lost_focus = gnt_box_lost_focus;
  parent_class -> gained_focus = gnt_box_gained_focus;
  parent_class -> confirm_size = gnt_box_confirm_size;
  parent_class -> size_changed = gnt_box_size_changed;
  gclass -> set_property = gnt_box_set_property;
  gclass -> get_property = gnt_box_get_property;
  g_object_class_install_property(gclass,PROP_VERTICAL,g_param_spec_boolean("vertical","Vertical","Whether the child widgets in the box should be stacked vertically.",1,(G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB)));
  g_object_class_install_property(gclass,PROP_HOMO,g_param_spec_boolean("homogeneous","Homogeneous","Whether the child widgets in the box should have the same size.",1,(G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB)));
  gnt_bindable_class_register_action(bindable,"focus-next",action_focus_next,"\t",((void *)((void *)0)));
  gnt_bindable_register_binding(bindable,"focus-next",((cur_term -> type.Strings[83] != 0)?cur_term -> type.Strings[83] : ""),((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"focus-prev",action_focus_prev,((cur_term -> type.Strings[0] != 0)?cur_term -> type.Strings[0] : (((cur_term -> type.Strings[148] != 0)?cur_term -> type.Strings[148] : ""))),((void *)((void *)0)));
  gnt_bindable_register_binding(bindable,"focus-prev",((cur_term -> type.Strings[79] != 0)?cur_term -> type.Strings[79] : ""),((void *)((void *)0)));
  gnt_style_read_actions(( *((GTypeClass *)klass)).g_type,bindable);
}

static void gnt_box_init(GTypeInstance *instance,gpointer class)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_widget_get_gtype()));
  GntBox *box = (GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype()));
/* Initially make both the height and width resizable.
	 * Update the flags as necessary when widgets are added to it. */
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= (GNT_WIDGET_GROW_X | GNT_WIDGET_GROW_Y);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= (GNT_WIDGET_CAN_TAKE_FOCUS | GNT_WIDGET_DISABLE_ACTIONS);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= (GNT_WIDGET_NO_BORDER | GNT_WIDGET_NO_SHADOW);
  box -> pad = 1;
  box -> fill = 1;;
}
/******************************************************************************
 * GntBox API
 *****************************************************************************/

GType gnt_box_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(GntBoxClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gnt_box_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GntBox ))), (0), (gnt_box_init), ((const GTypeValueTable *)((void *)0))
/* base_init		*/
/* base_finalize	*/
/* class_finalize	*/
/* class_data		*/
/* n_preallocs		*/
/* instance_init	*/
/* value_table		*/
};
    type = g_type_register_static(gnt_widget_get_gtype(),"GntBox",&info,0);
  }
  return type;
}

GntWidget *gnt_box_new(gboolean homo,gboolean vert)
{
  GntWidget *widget = (g_object_new(gnt_box_get_gtype(),0));
  GntBox *box = (GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype()));
  box -> homogeneous = homo;
  box -> vertical = vert;
  box -> alignment = (((vert != 0)?GNT_ALIGN_LEFT : GNT_ALIGN_MID));
  return widget;
}

void gnt_box_add_widget(GntBox *b,GntWidget *widget)
{
  b -> list = g_list_append((b -> list),widget);
  widget -> parent = ((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)b),gnt_widget_get_gtype())));
}

void gnt_box_set_title(GntBox *b,const char *title)
{
  char *prev = (b -> title);
  GntWidget *w = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)b),gnt_widget_get_gtype()));
  b -> title = g_strdup(title);
  if (((w -> window) != 0) && !((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)w),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_NO_BORDER) != 0U)) {
/* Erase the old title */
    int pos;
    int right;
    get_title_thingies(b,prev,&pos,&right);
    (wmove((w -> window),0,(pos - 1)) == -1)?-1 : whline((w -> window),(acs_map[(unsigned char )'q'] | (gnt_color_pair(GNT_COLOR_NORMAL))),((right - pos) + 2));
  }
  g_free(prev);
}

void gnt_box_set_pad(GntBox *box,int pad)
{
  box -> pad = pad;
/* XXX: Perhaps redraw if already showing? */
}

void gnt_box_set_toplevel(GntBox *box,gboolean set)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype()));
  if (set != 0) {
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags &= (~(GNT_WIDGET_NO_BORDER | GNT_WIDGET_NO_SHADOW));
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_CAN_TAKE_FOCUS;
  }
  else {
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= (GNT_WIDGET_NO_BORDER | GNT_WIDGET_NO_SHADOW);
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_CAN_TAKE_FOCUS);
  }
}

void gnt_box_sync_children(GntBox *box)
{
  GList *iter;
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype()));
  int pos = 1;
  if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_NO_BORDER) != 0U) 
    pos = 0;
  if (!((box -> active) != 0)) 
    find_focusable_widget(box);
  for (iter = (box -> list); iter != 0; iter = (iter -> next)) {{
      GntWidget *w = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(iter -> data)),gnt_widget_get_gtype()));
      int height;
      int width;
      int x;
      int y;
      if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)w),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_INVISIBLE) != 0U) 
        continue; 
      if ((({
        GTypeInstance *__inst = (GTypeInstance *)w;
        GType __t = gnt_box_get_gtype();
        gboolean __r;
        if (!(__inst != 0)) 
          __r = 0;
        else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
          __r = 1;
        else 
          __r = g_type_check_instance_is_a(__inst,__t);
        __r;
      })) != 0) 
        gnt_box_sync_children(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)w),gnt_box_get_gtype()))));
      gnt_widget_get_size(w,&width,&height);
      x = (w -> priv.x - widget -> priv.x);
      y = (w -> priv.y - widget -> priv.y);
      if ((box -> vertical) != 0) {
        x = pos;
        if ((box -> alignment) == GNT_ALIGN_RIGHT) 
          x += (widget -> priv.width - width);
        else if ((box -> alignment) == GNT_ALIGN_MID) 
          x += ((widget -> priv.width - width) / 2);
        if ((x + width) > (widget -> priv.width - pos)) 
          x -= ((x + width) - (widget -> priv.width - pos));
      }
      else {
        y = pos;
        if ((box -> alignment) == GNT_ALIGN_BOTTOM) 
          y += (widget -> priv.height - height);
        else if ((box -> alignment) == GNT_ALIGN_MID) 
          y += ((widget -> priv.height - height) / 2);
        if ((y + height) >= (widget -> priv.height - pos)) 
          y = ((widget -> priv.height - height) - pos);
      }
      copywin((w -> window),(widget -> window),0,0,y,x,((y + height) - 1),((x + width) - 1),0);
      gnt_widget_set_position(w,(x + widget -> priv.x),(y + widget -> priv.y));
      if (w == (box -> active)) {
        wmove((widget -> window),(y + ((((w -> window) != 0)?( *(w -> window))._cury : -1))),(x + ((((w -> window) != 0)?( *(w -> window))._curx : -1))));
      }
    }
  }
}

void gnt_box_set_alignment(GntBox *box,GntAlignment alignment)
{
  box -> alignment = alignment;
}

void gnt_box_remove(GntBox *box,GntWidget *widget)
{
  box -> list = g_list_remove((box -> list),widget);
  if ((((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_CAN_TAKE_FOCUS) != 0U) && (( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype())))).parent == ((GntWidget *)((void *)0)))) && ((box -> focus) != 0)) {
    if (widget == (box -> active)) {
      find_next_focus(box);
/* There's only one widget */
      if ((box -> active) == widget) 
        box -> active = ((GntWidget *)((void *)0));
    }
    box -> focus = g_list_remove((box -> focus),widget);
  }
  if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U) 
    gnt_widget_draw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype()))));
}

void gnt_box_remove_all(GntBox *box)
{
  g_list_foreach((box -> list),((GFunc )gnt_widget_destroy),0);
  g_list_free((box -> list));
  g_list_free((box -> focus));
  box -> list = ((GList *)((void *)0));
  box -> focus = ((GList *)((void *)0));
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype())))).priv.width = 0;
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype())))).priv.height = 0;
}

void gnt_box_readjust(GntBox *box)
{
  GList *iter;
  GntWidget *wid;
  int width;
  int height;
  if (( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype())))).parent != ((GntWidget *)((void *)0))) 
    return ;
  for (iter = (box -> list); iter != 0; iter = (iter -> next)) {
    GntWidget *w = (iter -> data);
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)w;
      GType __t = gnt_box_get_gtype();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = 1;
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
      gnt_box_readjust(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)w),gnt_box_get_gtype()))));
    else {
      ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)w),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_MAPPED);
      w -> priv.width = 0;
      w -> priv.height = 0;
    }
  }
  wid = ((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype())));
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)wid),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_MAPPED);
  wid -> priv.width = 0;
  wid -> priv.height = 0;
  if ((wid -> parent) == ((GntWidget *)((void *)0))) {
    g_list_free((box -> focus));
    box -> focus = ((GList *)((void *)0));
    box -> active = ((GntWidget *)((void *)0));
    gnt_widget_size_request(wid);
    gnt_widget_get_size(wid,&width,&height);
    gnt_screen_resize_widget(wid,width,height);
    find_focusable_widget(box);
  }
}

void gnt_box_set_fill(GntBox *box,gboolean fill)
{
  box -> fill = fill;
}

void gnt_box_move_focus(GntBox *box,int dir)
{
  GntWidget *now;
  if ((box -> active) == ((GntWidget *)((void *)0))) {
    find_focusable_widget(box);
    return ;
  }
  now = (box -> active);
  if (dir == 1) 
    find_next_focus(box);
  else if (dir == -1) 
    find_prev_focus(box);
  if ((now != 0) && (now != (box -> active))) {
    gnt_widget_set_focus(now,0);
    gnt_widget_set_focus((box -> active),1);
  }
  if (( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype())))).window != 0) 
    gnt_widget_draw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype()))));
}

void gnt_box_give_focus_to_child(GntBox *box,GntWidget *widget)
{
  GList *find;
  gpointer now;
  while(( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype())))).parent != 0)
    box = ((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype())))).parent),gnt_box_get_gtype())));
  find = g_list_find((box -> focus),widget);
  now = (box -> active);
  if (find != 0) 
    box -> active = widget;
  if ((now != 0) && (now != (box -> active))) {
    gnt_widget_set_focus(now,0);
    gnt_widget_set_focus((box -> active),1);
  }
  if (( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype())))).window != 0) 
    gnt_widget_draw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype()))));
}
