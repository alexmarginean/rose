/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "config.h"
#ifdef USE_PYTHON
#include <Python.h>
#endif
/* Python.h may define _GNU_SOURCE and _XOPEN_SOURCE_EXTENDED, so protect
 * these checks with #ifndef/!defined() */
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#if !defined _XOPEN_SOURCE_EXTENDED && (defined(__APPLE__) || defined(__unix__)) && !defined(__FreeBSD__)
#define _XOPEN_SOURCE_EXTENDED
#endif
#include <glib.h>
#if GLIB_CHECK_VERSION(2,6,0)
#	include <glib/gstdio.h>
#else
#	include <sys/types.h>
#	include <sys/stat.h>
#	include <fcntl.h>
#	define g_fopen open
#endif
#include <ctype.h>
#include <gmodule.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "gntinternal.h"
#undef GNT_LOG_DOMAIN
#define GNT_LOG_DOMAIN "WM"
#include "gntwm.h"
#include "gntstyle.h"
#include "gntmarshal.h"
#include "gnt.h"
#include "gntbox.h"
#include "gntbutton.h"
#include "gntentry.h"
#include "gntfilesel.h"
#include "gntlabel.h"
#include "gntmenu.h"
#include "gnttextview.h"
#include "gnttree.h"
#include "gntutils.h"
#include "gntwindow.h"
#define IDLE_CHECK_INTERVAL 5 /* 5 seconds */
enum __unnamed_enum___F0_L75_C1_SIG_NEW_WIN__COMMA__SIG_DECORATE_WIN__COMMA__SIG_CLOSE_WIN__COMMA__SIG_CONFIRM_RESIZE__COMMA__SIG_RESIZED__COMMA__SIG_CONFIRM_MOVE__COMMA__SIG_MOVED__COMMA__SIG_UPDATE_WIN__COMMA__SIG_GIVE_FOCUS__COMMA__SIG_KEY_PRESS__COMMA__SIG_MOUSE_CLICK__COMMA__SIG_TERMINAL_REFRESH__COMMA__SIGS {SIG_NEW_WIN,SIG_DECORATE_WIN,SIG_CLOSE_WIN,SIG_CONFIRM_RESIZE,SIG_RESIZED,SIG_CONFIRM_MOVE,SIG_MOVED,SIG_UPDATE_WIN,SIG_GIVE_FOCUS,SIG_KEY_PRESS,SIG_MOUSE_CLICK,SIG_TERMINAL_REFRESH,SIGS};
static guint signals[12UL] = {(0)};
static void gnt_wm_new_window_real(GntWM *wm,GntWidget *widget);
static void gnt_wm_win_resized(GntWM *wm,GntNode *node);
static void gnt_wm_win_moved(GntWM *wm,GntNode *node);
static void gnt_wm_give_focus(GntWM *wm,GntWidget *widget);
static void update_window_in_list(GntWM *wm,GntWidget *wid);
static void shift_window(GntWM *wm,GntWidget *widget,int dir);
static gboolean workspace_next(GntBindable *wm,GList *n);
static gboolean workspace_prev(GntBindable *wm,GList *n);
#ifndef NO_WIDECHAR
static int widestringwidth(wchar_t *wide);
#endif
static void ensure_normal_mode(GntWM *wm);
static gboolean write_already(gpointer data);
static int write_timeout;
static time_t last_active_time;
static gboolean idle_update;
/* list of WS with unseen activitiy */
static GList *act = (GList *)((void *)0);
static gboolean ignore_keys = 0;
#ifdef USE_PYTHON
static gboolean started_python = 0;
#endif

static GList *g_list_bring_to_front(GList *list,gpointer data)
{
  list = g_list_remove(list,data);
  list = g_list_prepend(list,data);
  return list;
}

static void free_node(gpointer data)
{
  GntNode *node = data;
  hide_panel((node -> panel));
  del_panel((node -> panel));
  g_free(node);
}

void gnt_wm_copy_win(GntWidget *widget,GntNode *node)
{
  WINDOW *src;
  WINDOW *dst;
  if (!(node != 0)) 
    return ;
  src = (widget -> window);
  dst = (node -> window);
  copywin(src,dst,(node -> scroll),0,0,0,((((dst != 0)?((dst -> _maxy) + 1) : -1)) - 1),((((dst != 0)?((dst -> _maxx) + 1) : -1)) - 1),0);
/* Update the hardware cursor */
  if (((({
    GTypeInstance *__inst = (GTypeInstance *)widget;
    GType __t = gnt_window_get_gtype();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = 1;
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) || (((
{
    GTypeInstance *__inst = (GTypeInstance *)widget;
    GType __t = gnt_box_get_gtype();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = 1;
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) 
{
    GntWidget *active = ( *((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype())))).active;
    if (active != 0) {
      int curx = (active -> priv.x + ((((active -> window) != 0)?( *(active -> window))._curx : -1)));
      int cury = (active -> priv.y + ((((active -> window) != 0)?( *(active -> window))._cury : -1)));
      if (wmove((node -> window),(cury - widget -> priv.y),(curx - widget -> priv.x)) != 0) 
        wmove((node -> window),0,0);
    }
  }
}
/**
 * The following is a workaround for a bug in most versions of ncursesw.
 * Read about it in: http://article.gmane.org/gmane.comp.lib.ncurses.bugs/2751
 *
 * In short, if a panel hides one cell of a multi-cell character, then the rest
 * of the characters in that line get screwed. The workaround here is to erase
 * any such character preemptively.
 *
 * Caveat: If a wide character is erased, and the panel above it is moved enough
 * to expose the entire character, it is not always redrawn.
 */

static void work_around_for_ncurses_bug()
{
#ifndef NO_WIDECHAR
  PANEL *panel = (PANEL *)((void *)0);
  while((panel = panel_below(panel)) != ((PANEL *)((void *)0))){
    int sx;
    int ex;
    int sy;
    int ey;
    int w;
    int y;
    cchar_t ch;
    PANEL *below = panel;
    sx = ( *(panel -> win))._begx;
    ex = (( *(panel -> win))._maxx + sx);
    sy = ( *(panel -> win))._begy;
    ey = (( *(panel -> win))._maxy + sy);
    while((below = panel_below(below)) != ((PANEL *)((void *)0))){
      if ((sy > (( *(below -> win))._begy + ( *(below -> win))._maxy)) || (ey < ( *(below -> win))._begy)) 
        continue; 
      if ((sx > (( *(below -> win))._begx + ( *(below -> win))._maxx)) || (ex < ( *(below -> win))._begx)) 
        continue; 
      for (y = ((sy > ( *(below -> win))._begy)?sy : ( *(below -> win))._begy); y <= (((ey < (( *(below -> win))._begy + ( *(below -> win))._maxy))?ey : (( *(below -> win))._begy + ( *(below -> win))._maxy))); y++) {
        if ((((wmove((below -> win),(y - ( *(below -> win))._begy),((sx - 1) - ( *(below -> win))._begx)) == -1)?-1 : win_wch((below -> win),&ch))) != 0) 
          goto right;
        w = widestringwidth(ch.chars);
        if ((w > 1) && ((ch.attr & 1) != 0UL)) {
          ch.chars[0] = 32;
          ch.attr &= ~((((chtype )1UL) << 0 + 8) - 1UL);
          (wmove((below -> win),(y - ( *(below -> win))._begy),((sx - 1) - ( *(below -> win))._begx)) == -1)?-1 : wadd_wch((below -> win),(&ch));
          wtouchln((below -> win),(y - ( *(below -> win))._begy),1,1);
        }
        right:
        if ((((wmove((below -> win),(y - ( *(below -> win))._begy),((ex + 1) - ( *(below -> win))._begx)) == -1)?-1 : win_wch((below -> win),&ch))) != 0) 
          continue; 
        w = widestringwidth(ch.chars);
        if ((w > 1) && !((ch.attr & 1) != 0UL)) {
          ch.chars[0] = 32;
          ch.attr &= ~((((chtype )1UL) << 0 + 8) - 1UL);
          (wmove((below -> win),(y - ( *(below -> win))._begy),((ex + 1) - ( *(below -> win))._begx)) == -1)?-1 : wadd_wch((below -> win),(&ch));
          wtouchln((below -> win),(y - ( *(below -> win))._begy),1,1);
        }
      }
    }
  }
#endif
}

static void update_act_msg()
{
  GntWidget *label;
  GList *iter;
  static GntWidget *message = (GntWidget *)((void *)0);
  GString *text = g_string_new("act: ");
  if (message != 0) 
    gnt_widget_destroy(message);
  if (!(act != 0)) 
    return ;
  for (iter = act; iter != 0; iter = (iter -> next)) {
    GntWS *ws = (iter -> data);
    g_string_append_printf(text,"%s, ",gnt_ws_get_name(ws));
  }
  g_string_erase(text,((text -> len) - 2),2);
  message = gnt_box_new(0,1);
  label = gnt_label_new_with_format((text -> str),(GNT_TEXT_FLAG_BOLD | GNT_TEXT_FLAG_HIGHLIGHT));
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)message),gnt_box_get_gtype())))),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_CAN_TAKE_FOCUS);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)message),gnt_box_get_gtype())))),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_TRANSIENT;
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)message),gnt_box_get_gtype()))),label);
  gnt_widget_set_name(message,"wm-message");
  gnt_widget_set_position(message,0,0);
  gnt_widget_draw(message);
  g_string_free(text,1);
}

static gboolean update_screen(GntWM *wm)
{
  if ((wm -> mode) == GNT_KP_MODE_WAIT_ON_CHILD) 
    return 1;
  if ((wm -> menu) != 0) {
    GntMenu *top = (wm -> menu);
    while(top != 0){
      GntNode *node = (g_hash_table_lookup((wm -> nodes),top));
      if (node != 0) 
        top_panel((node -> panel));
      top = (top -> submenu);
    }
  }
  work_around_for_ncurses_bug();
  update_panels();
  doupdate();
  return 1;
}

static gboolean sanitize_position(GntWidget *widget,int *x,int *y,gboolean m)
{
  int X_MAX = (stdscr != 0)?((stdscr -> _maxx) + 1) : -1;
  int Y_MAX = ((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) - 1);
  int w;
  int h;
  int nx;
  int ny;
  gboolean changed = 0;
  GntWindowFlags flags = ((((
{
    GTypeInstance *__inst = (GTypeInstance *)widget;
    GType __t = gnt_window_get_gtype();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = 1;
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)?(gnt_window_get_maximize(((GntWindow *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_window_get_gtype()))))) : 0);
  gnt_widget_get_size(widget,&w,&h);
  if (x != 0) {
    if (((m != 0) && ((flags & GNT_WINDOW_MAXIMIZE_X) != 0U)) && ( *x != 0)) {
       *x = 0;
      changed = 1;
    }
    else if (( *x + w) > X_MAX) {
      nx = ((0 > (X_MAX - w))?0 : (X_MAX - w));
      if (nx !=  *x) {
         *x = nx;
        changed = 1;
      }
    }
  }
  if (y != 0) {
    if (((m != 0) && ((flags & GNT_WINDOW_MAXIMIZE_Y) != 0U)) && ( *y != 0)) {
       *y = 0;
      changed = 1;
    }
    else if (( *y + h) > Y_MAX) {
      ny = ((0 > (Y_MAX - h))?0 : (Y_MAX - h));
      if (ny !=  *y) {
         *y = ny;
        changed = 1;
      }
    }
  }
  return changed;
}

static void refresh_node(GntWidget *widget,GntNode *node,gpointer m)
{
  int x;
  int y;
  int w;
  int h;
  int nw;
  int nh;
  int X_MAX = (stdscr != 0)?((stdscr -> _maxx) + 1) : -1;
  int Y_MAX = ((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) - 1);
  GntWindowFlags flags = 0;
  if ((m != 0) && ((({
    GTypeInstance *__inst = (GTypeInstance *)widget;
    GType __t = gnt_window_get_gtype();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = 1;
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) 
{
    flags = gnt_window_get_maximize(((GntWindow *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_window_get_gtype()))));
  }
  gnt_widget_get_position(widget,&x,&y);
  gnt_widget_get_size(widget,&w,&h);
  if (sanitize_position(widget,&x,&y,!(!(m != 0))) != 0) 
    gnt_screen_move_widget(widget,x,y);
  if ((flags & GNT_WINDOW_MAXIMIZE_X) != 0U) 
    nw = X_MAX;
  else 
    nw = ((w < X_MAX)?w : X_MAX);
  if ((flags & GNT_WINDOW_MAXIMIZE_Y) != 0U) 
    nh = Y_MAX;
  else 
    nh = ((h < Y_MAX)?h : Y_MAX);
  if ((nw != w) || (nh != h)) 
    gnt_screen_resize_widget(widget,nw,nh);
}

static void read_window_positions(GntWM *wm)
{
#if GLIB_CHECK_VERSION(2,6,0)
  GKeyFile *gfile = g_key_file_new();
  char *filename = g_build_filename(g_get_home_dir(),".gntpositions",((void *)((void *)0)));
  GError *error = (GError *)((void *)0);
  char **keys;
  gsize nk;
  if (!(g_key_file_load_from_file(gfile,filename,G_KEY_FILE_NONE,&error) != 0)) {
    g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: %s","WM","void read_window_positions(struct _GntWM *)",(error -> message));
    g_error_free(error);
    g_free(filename);
    return ;
  }
  keys = g_key_file_get_keys(gfile,"positions",&nk,&error);
  if (error != 0) {
    g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: %s","WM","void read_window_positions(struct _GntWM *)",(error -> message));
    g_error_free(error);
    error = ((GError *)((void *)0));
  }
  else {
    while(nk-- != 0UL){
      char *title = keys[nk];
      gsize l;
      char **coords = g_key_file_get_string_list(gfile,"positions",title,&l,0);
      if (l == 2) {
        int x = atoi(coords[0]);
        int y = atoi(coords[1]);
        GntPosition *p = (GntPosition *)(g_malloc0_n(1,(sizeof(GntPosition ))));
        p -> x = x;
        p -> y = y;
        g_hash_table_replace((wm -> positions),(g_strdup((title + 1))),p);
      }
      else {
        g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: Invalid number of arguments (%lu) for positioning a window.","WM","void read_window_positions(struct _GntWM *)",l);
      }
      g_strfreev(coords);
    }
    g_strfreev(keys);
  }
  g_free(filename);
  g_key_file_free(gfile);
#endif
}

static gboolean check_idle(gpointer n)
{
  if (idle_update != 0) {
    time(&last_active_time);
    idle_update = 0;
  }
  return 1;
}

static void gnt_wm_init(GTypeInstance *instance,gpointer class)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_wm_get_gtype()));
  wm -> workspaces = ((GList *)((void *)0));
  wm -> name_places = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
  wm -> title_places = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
  gnt_style_read_workspaces(wm);
  if ((wm -> workspaces) == ((GList *)((void *)0))) {
    wm -> cws = gnt_ws_new("default");
    gnt_wm_add_workspace(wm,(wm -> cws));
  }
  else {
    wm -> cws = ( *(wm -> workspaces)).data;
  }
  wm -> event_stack = 0;
  wm -> tagged = ((GList *)((void *)0));
  wm -> windows = ((struct __unnamed_class___F219_L89_C2__GntWM_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name__GntWM__scope__window__DELIMITER___GntWM_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name__GntWM__scope__tree *)((void *)0));
  wm -> actions = ((struct __unnamed_class___F219_L89_C2__GntWM_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name__GntWM__scope__window__DELIMITER___GntWM_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name__GntWM__scope__tree *)((void *)0));
  wm -> nodes = g_hash_table_new_full(g_direct_hash,g_direct_equal,0,free_node);
  wm -> positions = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
  if (gnt_style_get_bool(GNT_STYLE_REMPOS,1) != 0) 
    read_window_positions(wm);
  g_timeout_add_seconds(5,check_idle,0);
  time(&last_active_time);
  gnt_wm_switch_workspace(wm,0);
}

static void switch_window(GntWM *wm,int direction,gboolean urgent)
{
  GntWidget *w = (GntWidget *)((void *)0);
  GntWidget *wid = (GntWidget *)((void *)0);
  int pos;
  int orgpos;
  if ((wm -> _list.window != 0) || ((wm -> menu) != 0)) 
    return ;
  if (!(( *(wm -> cws)).ordered != 0) || !(( *( *(wm -> cws)).ordered).next != 0)) 
    return ;
  if ((wm -> mode) != GNT_KP_MODE_NORMAL) {
    ensure_normal_mode(wm);
  }
  w = ( *( *(wm -> cws)).ordered).data;
  orgpos = (pos = g_list_index(( *(wm -> cws)).list,w));
  do {
    pos += direction;
    if (pos < 0) {
      wid = ( *g_list_last(( *(wm -> cws)).list)).data;
      pos = (g_list_length(( *(wm -> cws)).list) - 1);
    }
    else if (pos >= g_list_length(( *(wm -> cws)).list)) {
      wid = ( *( *(wm -> cws)).list).data;
      pos = 0;
    }
    else 
      wid = (g_list_nth_data(( *(wm -> cws)).list,pos));
  }while (((urgent != 0) && !((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)wid),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_URGENT) != 0U)) && (pos != orgpos));
  gnt_wm_raise_window(wm,wid);
}

static gboolean window_next(GntBindable *bindable,GList *null)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  switch_window(wm,1,0);
  return 1;
}

static gboolean window_prev(GntBindable *bindable,GList *null)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  switch_window(wm,-1,0);
  return 1;
}

static gboolean switch_window_n(GntBindable *bind,GList *list)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_wm_get_gtype()));
  GList *l;
  int n;
  if (!(( *(wm -> cws)).ordered != 0)) 
    return 1;
  if (list != 0) 
    n = ((gint )((glong )(list -> data)));
  else 
    n = 0;
  if ((l = g_list_nth(( *(wm -> cws)).list,n)) != ((GList *)((void *)0))) {
    gnt_wm_raise_window(wm,(l -> data));
  }
  return 1;
}

static gboolean window_scroll_up(GntBindable *bindable,GList *null)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  GntWidget *window;
  GntNode *node;
  if (!(( *(wm -> cws)).ordered != 0)) 
    return 1;
  window = ( *( *(wm -> cws)).ordered).data;
  node = (g_hash_table_lookup((wm -> nodes),window));
  if (!(node != 0)) 
    return 1;
  if ((node -> scroll) != 0) {
    node -> scroll--;
    gnt_wm_copy_win(window,node);
    update_screen(wm);
  }
  return 1;
}

static gboolean window_scroll_down(GntBindable *bindable,GList *null)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  GntWidget *window;
  GntNode *node;
  int w;
  int h;
  if (!(( *(wm -> cws)).ordered != 0)) 
    return 1;
  window = ( *( *(wm -> cws)).ordered).data;
  node = (g_hash_table_lookup((wm -> nodes),window));
  if (!(node != 0)) 
    return 1;
  gnt_widget_get_size(window,&w,&h);
  if ((h - (node -> scroll)) > ((((node -> window) != 0)?(( *(node -> window))._maxy + 1) : -1))) {
    node -> scroll++;
    gnt_wm_copy_win(window,node);
    update_screen(wm);
  }
  return 1;
}

static gboolean window_close(GntBindable *bindable,GList *null)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  if (wm -> _list.window != 0) 
    return 1;
  if (( *(wm -> cws)).ordered != 0) {
    gnt_widget_destroy(( *( *(wm -> cws)).ordered).data);
    ensure_normal_mode(wm);
  }
  return 1;
}

static void destroy__list(GntWidget *widget,GntWM *wm)
{
  wm -> _list.window = ((GntWidget *)((void *)0));
  wm -> _list.tree = ((GntWidget *)((void *)0));
  wm -> windows = ((struct __unnamed_class___F219_L89_C2__GntWM_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name__GntWM__scope__window__DELIMITER___GntWM_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name__GntWM__scope__tree *)((void *)0));
  wm -> actions = ((struct __unnamed_class___F219_L89_C2__GntWM_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name__GntWM__scope__window__DELIMITER___GntWM_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name__GntWM__scope__tree *)((void *)0));
  update_screen(wm);
}

static void setup__list(GntWM *wm)
{
  GntWidget *tree;
  GntWidget *win;
  ensure_normal_mode(wm);
  win = (wm -> _list.window = gnt_box_new(0,0));
  gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),1);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),0);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_TRANSIENT;
  tree = (wm -> _list.tree = gnt_tree_new());
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),tree);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"destroy",((GCallback )destroy__list),wm,0,((GConnectFlags )0));
}

static void window_list_activate(GntTree *tree,GntWM *wm)
{
  GntBindable *sel = (gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype())))));
  gnt_widget_destroy(wm -> _list.window);
  if (!(sel != 0)) 
    return ;
  if ((({
    GTypeInstance *__inst = (GTypeInstance *)sel;
    GType __t = gnt_ws_get_gtype();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = 1;
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
{
    gnt_wm_switch_workspace(wm,g_list_index((wm -> workspaces),sel));
  }
  else {
    gnt_wm_raise_window(wm,((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)sel),gnt_widget_get_gtype()))));
  }
}

static void populate_window_list(GntWM *wm,gboolean workspace)
{
  GList *iter;
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)( *(wm -> windows)).tree),gnt_tree_get_gtype()));
  if (!(workspace != 0)) {
    for (iter = ( *(wm -> cws)).list; iter != 0; iter = (iter -> next)) {
      GntBox *box = (GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(iter -> data)),gnt_box_get_gtype()));
      gnt_tree_add_row_last(tree,box,gnt_tree_create_row(tree,(box -> title)),0);
      update_window_in_list(wm,((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype()))));
    }
  }
  else {
    GList *ws = (wm -> workspaces);
    for (; ws != 0; ws = (ws -> next)) {
      gnt_tree_add_row_last(tree,(ws -> data),gnt_tree_create_row(tree,gnt_ws_get_name(((GntWS *)(g_type_check_instance_cast(((GTypeInstance *)(ws -> data)),gnt_ws_get_gtype()))))),0);
      for (iter = ( *((GntWS *)(g_type_check_instance_cast(((GTypeInstance *)(ws -> data)),gnt_ws_get_gtype())))).list; iter != 0; iter = (iter -> next)) {
        GntBox *box = (GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(iter -> data)),gnt_box_get_gtype()));
        gnt_tree_add_row_last(tree,box,gnt_tree_create_row(tree,(box -> title)),(ws -> data));
        update_window_in_list(wm,((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype()))));
      }
    }
  }
}

static gboolean window_list_key_pressed(GntWidget *widget,const char *text,GntWM *wm)
{
  if ((text[1] == 0) && (( *(wm -> cws)).ordered != 0)) {
    GntBindable *sel = (gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype())))));
    switch(text[0]){
      case ',':
{
      }
      case '-':
{
        if ((({
          GTypeInstance *__inst = (GTypeInstance *)sel;
          GType __t = gnt_ws_get_gtype();
          gboolean __r;
          if (!(__inst != 0)) 
            __r = 0;
          else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
            __r = 1;
          else 
            __r = g_type_check_instance_is_a(__inst,__t);
          __r;
        })) != 0) 
{
/* reorder the workspace. */
        }
        else 
          shift_window(wm,((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)sel),gnt_widget_get_gtype()))),-1);
        break; 
      }
      case '.':
{
      }
      case '=':
{
        if ((({
          GTypeInstance *__inst = (GTypeInstance *)sel;
          GType __t = gnt_ws_get_gtype();
          gboolean __r;
          if (!(__inst != 0)) 
            __r = 0;
          else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
            __r = 1;
          else 
            __r = g_type_check_instance_is_a(__inst,__t);
          __r;
        })) != 0) 
{
/* reorder the workspace. */
        }
        else 
          shift_window(wm,((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)sel),gnt_widget_get_gtype()))),1);
        break; 
      }
      default:
{
        return 0;
      }
    }
    gnt_tree_remove_all(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()))));
    populate_window_list(wm,((gint )((glong )(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"workspace")))));
    gnt_tree_set_selected(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()))),sel);
    return 1;
  }
  return 0;
}

static void list_of_windows(GntWM *wm,gboolean workspace)
{
  GntWidget *tree;
  GntWidget *win;
  setup__list(wm);
  wm -> windows = &wm -> _list;
  win = ( *(wm -> windows)).window;
  tree = ( *(wm -> windows)).tree;
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),((workspace != 0)?"Workspace List" : "Window List"));
  populate_window_list(wm,workspace);
  if (( *(wm -> cws)).ordered != 0) 
    gnt_tree_set_selected(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),( *( *(wm -> cws)).ordered).data);
  else if (workspace != 0) 
    gnt_tree_set_selected(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),(wm -> cws));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"activate",((GCallback )window_list_activate),wm,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"key_pressed",((GCallback )window_list_key_pressed),wm,0,((GConnectFlags )0));
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"workspace",((gpointer )((glong )workspace)));
  gnt_tree_set_col_width(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),0,((((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)) / 3));
  gnt_widget_set_size(tree,0,((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) / 2));
  gnt_widget_set_position(win,((((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)) / 3),((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) / 4));
  gnt_widget_show(win);
}

static gboolean window_list(GntBindable *bindable,GList *null)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  if ((wm -> _list.window != 0) || ((wm -> menu) != 0)) 
    return 1;
  if (!(( *(wm -> cws)).ordered != 0)) 
    return 1;
  list_of_windows(wm,0);
  return 1;
}

static void dump_file_save(GntFileSel *fs,const char *path,const char *f,gpointer n)
{
  FILE *file;
  int x;
  int y;
  chtype old = 0;
  chtype now = 0;
  struct __unnamed_class___F0_L714_C2_L402R__L403R__scope____SgSS2___variable_declaration__variable_type_c_variable_name_L402R__L403R__scope____SgSS2____scope__ascii__DELIMITER__L402R__L403R__scope____SgSS2___variable_declaration__variable_type___Pb__c__Pe___variable_name_L402R__L403R__scope____SgSS2____scope__unicode {
  char ascii;
  char *unicode;}unis[] = {{('q'), ("&#x2500;")}, {('t'), ("&#x251c;")}, {('u'), ("&#x2524;")}, {('x'), ("&#x2502;")}, {('-'), ("&#x2191;")}, {('.'), ("&#x2193;")}, {('l'), ("&#x250c;")}, {('k'), ("&#x2510;")}, {('m'), ("&#x2514;")}, {('j'), ("&#x2518;")}, {('a'), ("&#x2592;")}, {('n'), ("&#x253c;")}, {('w'), ("&#x252c;")}, {('v'), ("&#x2534;")}, {(0), ((char *)((void *)0))}};
  gnt_widget_destroy(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)fs),gnt_widget_get_gtype()))));
  if ((file = fopen(path,"w+")) == ((FILE *)((void *)0))) {
    return ;
  }
  fprintf(file,"<head>\n  <meta http-equiv=\'Content-Type\' content=\'text/html; charset=utf-8\' />\n</head>\n<body>\n");
  fprintf(file,"<pre>");
  for (y = 0; y < (((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)); y++) {
    for (x = 0; x < (((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)); x++) {
      char ch[2UL] = {(0), (0)};
      char *print;
#ifdef NO_WIDECHAR
#else
      cchar_t wch;
      char unicode[12UL];
      (wmove(curscr,y,x) == -1)?-1 : win_wch(curscr,&wch);
      now = wch.attr;
      ch[0] = ((char )(wch.chars[0] & 0xff));
#endif
#define CHECK(attr, start, end) \
			do \
			{  \
				if (now & attr)  \
				{  \
					if (!(old & attr))  \
						fprintf(file, "%s", start);  \
				}  \
				else if (old & attr)  \
				{  \
					fprintf(file, "%s", end);  \
				}  \
			} while (0)
      do {
        if ((now & (((chtype )1UL) << 13 + 8)) != 0UL) {
          if (!((old & (((chtype )1UL) << 13 + 8)) != 0UL)) 
            fprintf(file,"%s","<b>");
        }
        else if ((old & (((chtype )1UL) << 13 + 8)) != 0UL) {
          fprintf(file,"%s","</b>");
        }
      }while (0);
      do {
        if ((now & (((chtype )1UL) << 9 + 8)) != 0UL) {
          if (!((old & (((chtype )1UL) << 9 + 8)) != 0UL)) 
            fprintf(file,"%s","<u>");
        }
        else if ((old & (((chtype )1UL) << 9 + 8)) != 0UL) {
          fprintf(file,"%s","</u>");
        }
      }while (0);
      do {
        if ((now & (((chtype )1UL) << 11 + 8)) != 0UL) {
          if (!((old & (((chtype )1UL) << 11 + 8)) != 0UL)) 
            fprintf(file,"%s","<blink>");
        }
        else if ((old & (((chtype )1UL) << 11 + 8)) != 0UL) {
          fprintf(file,"%s","</blink>");
        }
      }while (0);
      if (((now & (((chtype )((1UL << 8) - 1UL)) << 0 + 8)) != (old & (((chtype )((1UL << 8) - 1UL)) << 0 + 8))) || ((now & (((chtype )1UL) << 10 + 8)) != (old & (((chtype )1UL) << 10 + 8)))) {
        int ret;
        short fgp;
        short bgp;
        short r;
        short g;
        short b;
        struct __unnamed_class___F0_L781_C5_L404R_variable_declaration__variable_type_i_variable_name_L404R__scope__r__DELIMITER__L404R_variable_declaration__variable_type_i_variable_name_L404R__scope__g__DELIMITER__L404R_variable_declaration__variable_type_i_variable_name_L404R__scope__b {
        int r;
        int g;
        int b;}fg;
        struct __unnamed_class___F0_L781_C5_L404R_variable_declaration__variable_type_i_variable_name_L404R__scope__r__DELIMITER__L404R_variable_declaration__variable_type_i_variable_name_L404R__scope__g__DELIMITER__L404R_variable_declaration__variable_type_i_variable_name_L404R__scope__b bg;
        ret = pair_content(((int )(((now & (((chtype )((1UL << 8) - 1UL)) << 0 + 8)) & (((chtype )((1UL << 8) - 1UL)) << 0 + 8)) >> 8)),&fgp,&bgp);
        if (fgp == -1) 
          fgp = 0;
        if (bgp == -1) 
          bgp = 7;
        if ((now & (((chtype )1UL) << 10 + 8)) != 0UL) {
          short tmp = fgp;
          fgp = bgp;
          bgp = tmp;
        }
        ret = color_content(fgp,&r,&g,&b);
        fg.r = r;
        fg.b = b;
        fg.g = g;
        ret = color_content(bgp,&r,&g,&b);
        bg.r = r;
        bg.b = b;
        bg.g = g;
#define ADJUST(x) (x = x * 255 / 1000)
        fg.r = ((fg.r * 0xff) / 1000);
        fg.g = ((fg.g * 0xff) / 1000);
        fg.b = ((fg.b * 0xff) / 1000);
        bg.r = ((bg.r * 0xff) / 1000);
        bg.b = ((bg.b * 0xff) / 1000);
        bg.g = ((bg.g * 0xff) / 1000);
        if (x != 0) 
          fprintf(file,"</span>");
        fprintf(file,"<span style=\"background:#%02x%02x%02x;color:#%02x%02x%02x\">",bg.r,bg.g,bg.b,fg.r,fg.g,fg.b);
      }
      print = ch;
#ifndef NO_WIDECHAR
      if (wch.chars[0] > 0xff) {
        snprintf(unicode,(sizeof(unicode)),"&#x%x;",((unsigned int )wch.chars[0]));
        print = unicode;
      }
#endif
      if ((now & (((chtype )1UL) << 14 + 8)) != 0UL) {
        int u;
{
          for (u = 0; unis[u].ascii != 0; u++) {
            if (ch[0] == unis[u].ascii) {
              print = unis[u].unicode;
              break; 
            }
          }
        }
        if (!(unis[u].ascii != 0)) 
          print = " ";
      }
      if (ch[0] == '&') 
        fprintf(file,"&amp;");
      else if (ch[0] == 60) 
        fprintf(file,"&lt;");
      else if (ch[0] == '>') 
        fprintf(file,"&gt;");
      else 
        fprintf(file,"%s",print);
      old = now;
    }
    fprintf(file,"</span>\n");
    old = 0;
  }
  fprintf(file,"</pre>\n</body>");
  fclose(file);
}

static void dump_file_cancel(GntWidget *w,GntFileSel *fs)
{
  gnt_widget_destroy(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)fs),gnt_widget_get_gtype()))));
}

static gboolean dump_screen(GntBindable *b,GList *null)
{
  GntWidget *window = gnt_file_sel_new();
  GntFileSel *sel = (GntFileSel *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_file_sel_get_gtype()));
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"vertical",1,((void *)((void *)0)));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_label_new("Please enter the filename to save the screenshot."));
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),"Save Screenshot...");
  gnt_file_sel_set_suggested_filename(sel,"dump.html");
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)sel),((GType )(20 << 2))))),"file_selected",((GCallback )dump_file_save),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> cancel)),((GType )(20 << 2))))),"activate",((GCallback )dump_file_cancel),sel,0,((GConnectFlags )0));
  gnt_widget_show(window);
  return 1;
}

static void shift_window(GntWM *wm,GntWidget *widget,int dir)
{
  GList *all = ( *(wm -> cws)).list;
  GList *list = g_list_find(all,widget);
  int length;
  int pos;
  if (!(list != 0)) 
    return ;
  length = (g_list_length(all));
  pos = g_list_position(all,list);
  pos += dir;
  if (dir > 0) 
    pos++;
  if (pos < 0) 
    pos = length;
  else if (pos > length) 
    pos = 0;
  all = g_list_insert(all,widget,pos);
  all = g_list_delete_link(all,list);
  ( *(wm -> cws)).list = all;
  gnt_ws_draw_taskbar((wm -> cws),0);
  if (( *(wm -> cws)).ordered != 0) {
    GntWidget *w = ( *( *(wm -> cws)).ordered).data;
    GntNode *node = (g_hash_table_lookup((wm -> nodes),w));
    top_panel((node -> panel));
    update_panels();
    doupdate();
  }
}

static gboolean shift_left(GntBindable *bindable,GList *null)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  if (wm -> _list.window != 0) 
    return 1;
  if (!(( *(wm -> cws)).ordered != 0)) 
    return 0;
  shift_window(wm,( *( *(wm -> cws)).ordered).data,-1);
  return 1;
}

static gboolean shift_right(GntBindable *bindable,GList *null)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  if (wm -> _list.window != 0) 
    return 1;
  if (!(( *(wm -> cws)).ordered != 0)) 
    return 0;
  shift_window(wm,( *( *(wm -> cws)).ordered).data,1);
  return 1;
}

static void action_list_activate(GntTree *tree,GntWM *wm)
{
  GntAction *action = (gnt_tree_get_selection_data(tree));
  ( *(action -> callback))();
  gnt_widget_destroy(wm -> _list.window);
}

static int compare_action(gconstpointer p1,gconstpointer p2)
{
  const GntAction *a1 = p1;
  const GntAction *a2 = p2;
  return g_utf8_collate((a1 -> label),(a2 -> label));
}

static gboolean list_actions(GntBindable *bindable,GList *null)
{
  GntWidget *tree;
  GntWidget *win;
  GList *iter;
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  int n;
  if ((wm -> _list.window != 0) || ((wm -> menu) != 0)) 
    return 1;
  if ((wm -> acts) == ((GList *)((void *)0))) 
    return 1;
  setup__list(wm);
  wm -> actions = &wm -> _list;
  win = ( *(wm -> actions)).window;
  tree = ( *(wm -> actions)).tree;
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),"Actions");
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_NO_BORDER;
/* XXX: Do we really want this? */
  gnt_tree_set_compare_func(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),compare_action);
  for (iter = (wm -> acts); iter != 0; iter = (iter -> next)) {
    GntAction *action = (iter -> data);
    gnt_tree_add_row_last(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),action,gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),(action -> label)),0);
  }
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"activate",((GCallback )action_list_activate),wm,0,((GConnectFlags )0));
  n = (g_list_length((wm -> acts)));
  gnt_widget_set_size(tree,0,n);
  gnt_widget_set_position(win,0,(((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) - 3) - n));
  gnt_widget_show(win);
  return 1;
}
#ifndef NO_WIDECHAR

static int widestringwidth(wchar_t *wide)
{
  int len;
  int ret;
  char *string;
  len = (wcstombs(0,wide,0) + 1);
  string = ((char *)(g_malloc0_n(len,(sizeof(char )))));
  wcstombs(string,wide,len);
  ret = ((string != 0)?gnt_util_onscreen_width(string,0) : 1);
  g_free(string);
  return ret;
}
#endif
/* Returns the onscreen width of the character at the position */

static int reverse_char(WINDOW *d,int y,int x,gboolean set)
{
#define DECIDE(ch) (set ? ((ch) | A_REVERSE) : ((ch) & ~A_REVERSE))
#ifdef NO_WIDECHAR
#else
  cchar_t ch;
  int wc = 1;
  if ((((wmove(d,y,x) == -1)?-1 : win_wch(d,&ch))) == 0) {
    wc = widestringwidth(ch.chars);
    ch.attr = ((set != 0)?(ch.attr | (((chtype )1UL) << 10 + 8)) : (ch.attr & (~(((chtype )1UL) << 10 + 8))));
/* XXX: This is a workaround for a bug */
    ch.attr &= (((chtype )(~(1UL - 1UL))) << 0 + 8);
    (wmove(d,y,x) == -1)?-1 : wadd_wch(d,(&ch));
  }
  return wc;
#endif
}

static void window_reverse(GntWidget *win,gboolean set,GntWM *wm)
{
  int i;
  int w;
  int h;
  WINDOW *d;
  if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_NO_BORDER) != 0U) 
    return ;
  d = (win -> window);
  gnt_widget_get_size(win,&w,&h);
  if (gnt_widget_has_shadow(win) != 0) {
    --w;
    --h;
  }
/* the top and bottom */
  for (i = 0; i < w; i += reverse_char(d,0,i,set)) ;
  for (i = 0; i < w; i += reverse_char(d,(h - 1),i,set)) ;
/* the left and right */
  for (i = 0; i < h; i += reverse_char(d,i,0,set)) ;
  for (i = 0; i < h; i += reverse_char(d,i,(w - 1),set)) ;
  gnt_wm_copy_win(win,(g_hash_table_lookup((wm -> nodes),win)));
  update_screen(wm);
}

static void ensure_normal_mode(GntWM *wm)
{
  if ((wm -> mode) != GNT_KP_MODE_NORMAL) {
    if (( *(wm -> cws)).ordered != 0) 
      window_reverse(( *( *(wm -> cws)).ordered).data,0,wm);
    wm -> mode = GNT_KP_MODE_NORMAL;
  }
}

static gboolean start_move(GntBindable *bindable,GList *null)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  if ((wm -> _list.window != 0) || ((wm -> menu) != 0)) 
    return 1;
  if (!(( *(wm -> cws)).ordered != 0)) 
    return 1;
  wm -> mode = GNT_KP_MODE_MOVE;
  window_reverse(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)( *( *(wm -> cws)).ordered).data),gnt_widget_get_gtype()))),1,wm);
  return 1;
}

static gboolean start_resize(GntBindable *bindable,GList *null)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  if ((wm -> _list.window != 0) || ((wm -> menu) != 0)) 
    return 1;
  if (!(( *(wm -> cws)).ordered != 0)) 
    return 1;
  wm -> mode = GNT_KP_MODE_RESIZE;
  window_reverse(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)( *( *(wm -> cws)).ordered).data),gnt_widget_get_gtype()))),1,wm);
  return 1;
}

static gboolean wm_quit(GntBindable *bindable,GList *list)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  if (write_timeout != 0) 
    write_already(wm);
  g_main_loop_quit((wm -> loop));
  return 1;
}

static gboolean return_true(GntWM *wm,GntWidget *w,int *a,int *b)
{
  return 1;
}

static gboolean refresh_screen(GntBindable *bindable,GList *null)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  GList *iter;
  endwin();
  wrefresh(stdscr);
  g_hash_table_foreach((wm -> nodes),((GHFunc )refresh_node),((gpointer )((gpointer )((glong )1))));
  g_signal_emit(wm,signals[SIG_TERMINAL_REFRESH],0);
  for (iter = g_list_last(( *(wm -> cws)).ordered); iter != 0; iter = (iter -> prev)) {
    GntWidget *w = (iter -> data);
    GntNode *node = (g_hash_table_lookup((wm -> nodes),w));
    top_panel((node -> panel));
  }
  gnt_ws_draw_taskbar((wm -> cws),1);
  update_screen(wm);
/* endwin resets the cursor to normal */
  curs_set(0);
  return 1;
}

static gboolean toggle_clipboard(GntBindable *bindable,GList *n)
{
  static GntWidget *clip;
  gchar *text;
  int maxx;
  int maxy;
  if (clip != 0) {
    gnt_widget_destroy(clip);
    clip = ((GntWidget *)((void *)0));
    return 1;
  }
  ((maxy = ((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) , (maxx = ((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)));
  text = gnt_get_clipboard_string();
  clip = gnt_window_box_new(0,0);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)clip),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_TRANSIENT;
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)clip),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_NO_BORDER;
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)clip),gnt_box_get_gtype()))),0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)clip),gnt_box_get_gtype()))),gnt_label_new(" "));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)clip),gnt_box_get_gtype()))),gnt_label_new(text));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)clip),gnt_box_get_gtype()))),gnt_label_new(" "));
  gnt_widget_set_position(clip,0,0);
  gnt_widget_draw(clip);
  g_free(text);
  return 1;
}

static void remove_tag(gpointer wid,gpointer wim)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)wim),gnt_wm_get_gtype()));
  GntWidget *w = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)wid),gnt_widget_get_gtype()));
  wm -> tagged = g_list_remove((wm -> tagged),w);
  (wmove((w -> window),0,1) == -1)?-1 : whline((w -> window),(acs_map[(unsigned char )'q'] | (gnt_color_pair(GNT_COLOR_NORMAL))),3);
  gnt_widget_draw(w);
}

static gboolean tag_widget(GntBindable *b,GList *params)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)b),gnt_wm_get_gtype()));
  GntWidget *widget;
  if (!(( *(wm -> cws)).ordered != 0)) 
    return 0;
  widget = ( *( *(wm -> cws)).ordered).data;
  if (g_list_find((wm -> tagged),widget) != 0) {
    remove_tag(widget,wm);
    return 1;
  }
  wm -> tagged = g_list_prepend((wm -> tagged),widget);
  wbkgdset((widget -> window),(32 | gnt_color_pair(GNT_COLOR_HIGHLIGHT)));
  mvwprintw((widget -> window),0,1,"[T]");
  gnt_widget_draw(widget);
  return 1;
}

static void widget_move_ws(gpointer wid,gpointer w)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)w),gnt_wm_get_gtype()));
  gnt_wm_widget_move_workspace(wm,(wm -> cws),((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)wid),gnt_widget_get_gtype()))));
}

static gboolean place_tagged(GntBindable *b,GList *params)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)b),gnt_wm_get_gtype()));
  g_list_foreach((wm -> tagged),widget_move_ws,wm);
  g_list_foreach((wm -> tagged),remove_tag,wm);
  g_list_free((wm -> tagged));
  wm -> tagged = ((GList *)((void *)0));
  return 1;
}

static gboolean workspace_list(GntBindable *b,GList *params)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)b),gnt_wm_get_gtype()));
  if ((wm -> _list.window != 0) || ((wm -> menu) != 0)) 
    return 1;
  list_of_windows(wm,1);
  return 1;
}

static gboolean workspace_new(GntBindable *bindable,GList *null)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  GntWS *ws = gnt_ws_new(0);
  gnt_wm_add_workspace(wm,ws);
  gnt_wm_switch_workspace(wm,g_list_index((wm -> workspaces),ws));
  return 1;
}

static gboolean ignore_keys_start(GntBindable *bindable,GList *n)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  if ((!((wm -> menu) != 0) && !(wm -> _list.window != 0)) && ((wm -> mode) == GNT_KP_MODE_NORMAL)) {
    ignore_keys = 1;
    return 1;
  }
  return 0;
}

static gboolean ignore_keys_end(GntBindable *bindable,GList *n)
{
  if (ignore_keys != 0) {
    ignore_keys = 0;
    return 1;
  }
  return 0;
}

static gboolean window_next_urgent(GntBindable *bindable,GList *n)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  switch_window(wm,1,1);
  return 1;
}

static gboolean window_prev_urgent(GntBindable *bindable,GList *n)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  switch_window(wm,-1,1);
  return 1;
}
#ifdef USE_PYTHON

static void python_script_selected(GntFileSel *fs,const char *path,const char *f,gpointer n)
{
  char *dir = g_path_get_dirname(path);
  FILE *file = fopen(path,"r");
  PyObject *pp = PySys_GetObject("path");
  PyObject *dirobj = PyString_FromString(dir);
  PyList_Insert(pp,0,dirobj);
  do {
    if (--( *((PyObject *)dirobj)).ob_refcnt != 0) ;
    else 
      ( *( *( *((PyObject *)((PyObject *)dirobj))).ob_type).tp_dealloc)(((PyObject *)((PyObject *)dirobj)));
  }while (0);
  PyRun_SimpleFileExFlags(file,path,0,0);
  fclose(file);
  if (PyErr_Occurred() != 0) {
    PyErr_Print();
  }
  g_free(dir);
  gnt_widget_destroy(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)fs),gnt_widget_get_gtype()))));
}

static gboolean run_python(GntBindable *bindable,GList *n)
{
  GntWidget *window = gnt_file_sel_new();
  GntFileSel *sel = (GntFileSel *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_file_sel_get_gtype()));
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"vertical",1,((void *)((void *)0)));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_label_new("Please select the python script you want to run."));
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),"Select Python Script...");
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)sel),((GType )(20 << 2))))),"file_selected",((GCallback )python_script_selected),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> cancel)),((GType )(20 << 2))))),"activate",((GCallback )gnt_widget_destroy),sel,0,G_CONNECT_SWAPPED);
  gnt_widget_show(window);
  return 1;
}
#endif  /* USE_PYTHON */

static gboolean help_for_bindable(GntWM *wm,GntBindable *bindable)
{
  gboolean ret = 1;
  GntBindableClass *klass = (GntBindableClass *)( *((GTypeInstance *)bindable)).g_class;
  if ((klass -> help_window) != 0) {
    gnt_wm_raise_window(wm,((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(klass -> help_window)),gnt_widget_get_gtype()))));
  }
  else {
    ret = gnt_bindable_build_help_window(bindable);
  }
  return ret;
}

static gboolean help_for_wm(GntBindable *bindable,GList *null)
{
  return help_for_bindable(((GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()))),bindable);
}

static gboolean help_for_window(GntBindable *bindable,GList *null)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  GntWidget *widget;
  if (!(( *(wm -> cws)).ordered != 0)) 
    return 0;
  widget = ( *( *(wm -> cws)).ordered).data;
  return help_for_bindable(wm,((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_bindable_get_gtype()))));
}

static gboolean help_for_widget(GntBindable *bindable,GList *null)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  GntWidget *widget;
  if (!(( *(wm -> cws)).ordered != 0)) 
    return 1;
  widget = ( *( *(wm -> cws)).ordered).data;
  if (!((({
    GTypeInstance *__inst = (GTypeInstance *)widget;
    GType __t = gnt_box_get_gtype();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = 1;
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) 
    return 1;
  return help_for_bindable(wm,((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)( *((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype())))).active),gnt_bindable_get_gtype()))));
}

static void accumulate_windows(gpointer window,gpointer node,gpointer p)
{
  GList *list =  *((GList **)p);
  list = g_list_prepend(list,window);
   *((GList **)p) = list;
}

static void gnt_wm_destroy(GObject *obj)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)obj),gnt_wm_get_gtype()));
  GList *list = (GList *)((void *)0);
  g_hash_table_foreach((wm -> nodes),accumulate_windows,(&list));
  g_list_foreach(list,((GFunc )gnt_widget_destroy),0);
  g_list_free(list);
  g_hash_table_destroy((wm -> nodes));
  wm -> nodes = ((GHashTable *)((void *)0));
  while((wm -> workspaces) != 0){
    g_object_unref(( *(wm -> workspaces)).data);
    wm -> workspaces = g_list_delete_link((wm -> workspaces),(wm -> workspaces));
  }
#ifdef USE_PYTHON
  if (started_python != 0) {
    Py_Finalize();
    started_python = 0;
  }
#endif
}

static void gnt_wm_class_init(GntWMClass *klass)
{
  int i;
  GObjectClass *gclass = (GObjectClass *)(g_type_check_class_cast(((GTypeClass *)klass),((GType )(20 << 2))));
  char key[32UL];
  gclass -> dispose = gnt_wm_destroy;
  klass -> new_window = gnt_wm_new_window_real;
  klass -> decorate_window = ((void (*)(GntWM *, GntWidget *))((void *)0));
  klass -> close_window = ((gboolean (*)(GntWM *, GntWidget *))((void *)0));
  klass -> window_resize_confirm = return_true;
  klass -> window_resized = gnt_wm_win_resized;
  klass -> window_move_confirm = return_true;
  klass -> window_moved = gnt_wm_win_moved;
  klass -> window_update = ((void (*)(GntWM *, GntNode *))((void *)0));
  klass -> key_pressed = ((gboolean (*)(GntWM *, const char *))((void *)0));
  klass -> mouse_clicked = ((gboolean (*)(GntWM *, GntMouseEvent , int , int , GntWidget *))((void *)0));
  klass -> give_focus = gnt_wm_give_focus;
  signals[SIG_NEW_WIN] = g_signal_new("new_win",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWMClass *)((GntWMClass *)0))).new_window))),0,0,g_cclosure_marshal_VOID__POINTER,((GType )(1 << 2)),1,((GType )(17 << 2)));
  signals[SIG_DECORATE_WIN] = g_signal_new("decorate_win",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWMClass *)((GntWMClass *)0))).decorate_window))),0,0,g_cclosure_marshal_VOID__POINTER,((GType )(1 << 2)),1,((GType )(17 << 2)));
  signals[SIG_CLOSE_WIN] = g_signal_new("close_win",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWMClass *)((GntWMClass *)0))).close_window))),0,0,g_cclosure_marshal_VOID__POINTER,((GType )(1 << 2)),1,((GType )(17 << 2)));
  signals[SIG_CONFIRM_RESIZE] = g_signal_new("confirm_resize",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWMClass *)((GntWMClass *)0))).window_resize_confirm))),gnt_boolean_handled_accumulator,0,gnt_closure_marshal_BOOLEAN__POINTER_POINTER_POINTER,((GType )(5 << 2)),3,((GType )(17 << 2)),((GType )(17 << 2)),((GType )(17 << 2)));
  signals[SIG_CONFIRM_MOVE] = g_signal_new("confirm_move",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWMClass *)((GntWMClass *)0))).window_move_confirm))),gnt_boolean_handled_accumulator,0,gnt_closure_marshal_BOOLEAN__POINTER_POINTER_POINTER,((GType )(5 << 2)),3,((GType )(17 << 2)),((GType )(17 << 2)),((GType )(17 << 2)));
  signals[SIG_RESIZED] = g_signal_new("window_resized",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWMClass *)((GntWMClass *)0))).window_resized))),0,0,g_cclosure_marshal_VOID__POINTER,((GType )(1 << 2)),1,((GType )(17 << 2)));
  signals[SIG_MOVED] = g_signal_new("window_moved",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWMClass *)((GntWMClass *)0))).window_moved))),0,0,g_cclosure_marshal_VOID__POINTER,((GType )(1 << 2)),1,((GType )(17 << 2)));
  signals[SIG_UPDATE_WIN] = g_signal_new("window_update",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWMClass *)((GntWMClass *)0))).window_update))),0,0,g_cclosure_marshal_VOID__POINTER,((GType )(1 << 2)),1,((GType )(17 << 2)));
  signals[SIG_GIVE_FOCUS] = g_signal_new("give_focus",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWMClass *)((GntWMClass *)0))).give_focus))),0,0,g_cclosure_marshal_VOID__POINTER,((GType )(1 << 2)),1,((GType )(17 << 2)));
  signals[SIG_MOUSE_CLICK] = g_signal_new("mouse_clicked",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWMClass *)((GntWMClass *)0))).mouse_clicked))),gnt_boolean_handled_accumulator,0,gnt_closure_marshal_BOOLEAN__INT_INT_INT_POINTER,((GType )(5 << 2)),4,((GType )(6 << 2)),((GType )(6 << 2)),((GType )(6 << 2)),((GType )(17 << 2)));
  signals[SIG_TERMINAL_REFRESH] = g_signal_new("terminal-refresh",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWMClass *)((GntWMClass *)0))).terminal_refresh))),0,0,g_cclosure_marshal_VOID__VOID,((GType )(1 << 2)),0);
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"window-next",window_next,"\033n",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"window-prev",window_prev,"\033p",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"window-close",window_close,"\033c",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"window-list",window_list,"\033w",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"dump-screen",dump_screen,"\033D",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"shift-left",shift_left,"\033,",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"shift-right",shift_right,"\033.",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"action-list",list_actions,"\033a",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"start-move",start_move,"\033m",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"start-resize",start_resize,"\033r",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"wm-quit",wm_quit,"\033q",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"refresh-screen",refresh_screen,"\033l",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"switch-window-n",switch_window_n,0,((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"window-scroll-down",window_scroll_down,"\033\n",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"window-scroll-up",window_scroll_up,"\033\v",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"help-for-widget",help_for_widget,"\033/",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"workspace-new",workspace_new,((cur_term -> type.Strings[75] != 0)?cur_term -> type.Strings[75] : ""),((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"workspace-next",workspace_next,"\033>",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"workspace-prev",workspace_prev,"\033<",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"window-tag",tag_widget,"\033t",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"place-tagged",place_tagged,"\033T",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"workspace-list",workspace_list,"\033s",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"toggle-clipboard",toggle_clipboard,"\033C",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"help-for-wm",help_for_wm,"\033\\",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"help-for-window",help_for_window,"\033|",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"ignore-keys-start",ignore_keys_start,0,((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"ignore-keys-end",ignore_keys_end,"\033\a",((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"window-next-urgent",window_next_urgent,"\033\t",((void *)((void *)0)));
  snprintf(key,(sizeof(key)),"\033%s",((cur_term -> type.Strings[0] != 0)?cur_term -> type.Strings[0] : (((cur_term -> type.Strings[148] != 0)?cur_term -> type.Strings[148] : ""))));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"window-prev-urgent",window_prev_urgent,((key[1] != 0)?key : ((char *)((void *)0))),((void *)((void *)0)));
#ifdef USE_PYTHON
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"run-python",run_python,((cur_term -> type.Strings[69] != 0)?cur_term -> type.Strings[69] : ""),((void *)((void *)0)));
  if (!(Py_IsInitialized() != 0)) {
    Py_SetProgramName("gnt");
    Py_Initialize();
    started_python = 1;
  }
#endif
  gnt_style_read_actions(( *((GTypeClass *)klass)).g_type,((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))));
/* Make sure Alt+x are detected properly. */
  for (i = 48; i <= '9'; i++) {
    char str[] = "\033X";
    str[1] = i;
    gnt_keys_add_combination(str);
  };
}
/******************************************************************************
 * GntWM API
 *****************************************************************************/

GType gnt_wm_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(GntWMClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gnt_wm_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GntWM ))), (0), (gnt_wm_init), ((const GTypeValueTable *)((void *)0))
/* base_init		*/
/* base_finalize	*/
/* class_data		*/
/* n_preallocs		*/
/* instance_init	*/
/* value_table		*/
};
    type = g_type_register_static(gnt_bindable_get_gtype(),"GntWM",&info,0);
  }
  return type;
}

void gnt_wm_add_workspace(GntWM *wm,GntWS *ws)
{
  wm -> workspaces = g_list_append((wm -> workspaces),ws);
}

gboolean gnt_wm_switch_workspace(GntWM *wm,gint n)
{
  GntWS *s = (g_list_nth_data((wm -> workspaces),n));
  if (!(s != 0)) 
    return 0;
  if (wm -> _list.window != 0) {
    gnt_widget_destroy(wm -> _list.window);
  }
  ensure_normal_mode(wm);
  gnt_ws_hide((wm -> cws),(wm -> nodes));
  wm -> cws = s;
  gnt_ws_show((wm -> cws),(wm -> nodes));
  gnt_ws_draw_taskbar((wm -> cws),1);
  update_screen(wm);
  if (( *(wm -> cws)).ordered != 0) {
    gnt_wm_raise_window(wm,( *( *(wm -> cws)).ordered).data);
  }
  if ((act != 0) && (g_list_find(act,(wm -> cws)) != 0)) {
    act = g_list_remove(act,(wm -> cws));
    update_act_msg();
  }
  return 1;
}

gboolean gnt_wm_switch_workspace_prev(GntWM *wm)
{
  int n = g_list_index((wm -> workspaces),(wm -> cws));
  return gnt_wm_switch_workspace(wm,--n);
}

gboolean gnt_wm_switch_workspace_next(GntWM *wm)
{
  int n = g_list_index((wm -> workspaces),(wm -> cws));
  return gnt_wm_switch_workspace(wm,++n);
}

static gboolean workspace_next(GntBindable *wm,GList *n)
{
  return gnt_wm_switch_workspace_next(((GntWM *)(g_type_check_instance_cast(((GTypeInstance *)wm),gnt_wm_get_gtype()))));
}

static gboolean workspace_prev(GntBindable *wm,GList *n)
{
  return gnt_wm_switch_workspace_prev(((GntWM *)(g_type_check_instance_cast(((GTypeInstance *)wm),gnt_wm_get_gtype()))));
}

void gnt_wm_widget_move_workspace(GntWM *wm,GntWS *neww,GntWidget *widget)
{
  GntWS *oldw = gnt_wm_widget_find_workspace(wm,widget);
  GntNode *node;
  if (!(oldw != 0) || (oldw == neww)) 
    return ;
  node = (g_hash_table_lookup((wm -> nodes),widget));
  if ((node != 0) && ((node -> ws) == neww)) 
    return ;
  if (node != 0) 
    node -> ws = neww;
  gnt_ws_remove_widget(oldw,widget);
  gnt_ws_add_widget(neww,widget);
  if (neww == (wm -> cws)) {
    gnt_ws_widget_show(widget,(wm -> nodes));
  }
  else {
    gnt_ws_widget_hide(widget,(wm -> nodes));
  }
}

static gint widget_in_workspace(gconstpointer workspace,gconstpointer wid)
{
  GntWS *s = (GntWS *)workspace;
  if (((s -> list) != 0) && (g_list_find((s -> list),wid) != 0)) 
    return 0;
  return 1;
}

GntWS *gnt_wm_widget_find_workspace(GntWM *wm,GntWidget *widget)
{
  GList *l = g_list_find_custom((wm -> workspaces),widget,widget_in_workspace);
  if (l != 0) 
    return (l -> data);
  return 0;
}

static void free_workspaces(gpointer data,gpointer n)
{
  GntWS *s = data;
  g_free((s -> name));
}

void gnt_wm_set_workspaces(GntWM *wm,GList *workspaces)
{
  g_list_foreach((wm -> workspaces),free_workspaces,0);
  wm -> workspaces = workspaces;
  gnt_wm_switch_workspace(wm,0);
}

static void update_window_in_list(GntWM *wm,GntWidget *wid)
{
  GntTextFormatFlags flag = 0;
  if ((wm -> windows) == ((struct __unnamed_class___F219_L89_C2__GntWM_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name__GntWM__scope__window__DELIMITER___GntWM_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name__GntWM__scope__tree *)((void *)0))) 
    return ;
  if ((( *(wm -> cws)).ordered != 0) && (wid == ( *( *(wm -> cws)).ordered).data)) 
    flag |= GNT_TEXT_FLAG_DIM;
  else if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)wid),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_URGENT) != 0U) 
    flag |= GNT_TEXT_FLAG_BOLD;
  gnt_tree_set_row_flags(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)( *(wm -> windows)).tree),gnt_tree_get_gtype()))),wid,flag);
}

static gboolean match_title(gpointer title,gpointer n,gpointer wid_title)
{
/* XXX: do any regex magic here. */
  if (g_strrstr(((gchar *)wid_title),((gchar *)title)) != 0) 
    return 1;
  return 0;
}
#if !GLIB_CHECK_VERSION(2,4,0)
#endif

static GntWS *new_widget_find_workspace(GntWM *wm,GntWidget *widget)
{
  GntWS *ret = (GntWS *)((void *)0);
  const gchar *name;
  const gchar *title;
  title = ( *((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype())))).title;
  if (title != 0) 
    ret = (g_hash_table_find((wm -> title_places),match_title,((gpointer )title)));
  if (ret != 0) 
    return ret;
  name = gnt_widget_get_name(widget);
  if (name != 0) 
    ret = (g_hash_table_find((wm -> name_places),match_title,((gpointer )name)));
  return (ret != 0)?ret : (wm -> cws);
}

static void gnt_wm_new_window_real(GntWM *wm,GntWidget *widget)
{
  GntNode *node;
  gboolean transient = 0;
  if ((widget -> window) == ((WINDOW *)((void *)0))) 
    return ;
  node = ((GntNode *)(g_malloc0_n(1,(sizeof(GntNode )))));
  node -> me = widget;
  node -> scroll = 0;
  g_hash_table_replace((wm -> nodes),widget,node);
  refresh_node(widget,node,((gpointer )((gpointer )((glong )1))));
  transient = !(!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(node -> me)),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_TRANSIENT) != 0U));
#if 1
{
    int x;
    int y;
    int w;
    int h;
    int maxx;
    int maxy;
    gboolean shadow = 1;
    if (!(gnt_widget_has_shadow(widget) != 0)) 
      shadow = 0;
    x = widget -> priv.x;
    y = widget -> priv.y;
    w = (widget -> priv.width + shadow);
    h = (widget -> priv.height + shadow);
    maxx = ((stdscr != 0)?((stdscr -> _maxx) + 1) : -1);
/* room for the taskbar */
    maxy = ((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) - 1);
    x = ((0 > x)?0 : x);
    y = ((0 > y)?0 : y);
    if ((x + w) >= maxx) 
      x = ((0 > (maxx - w))?0 : (maxx - w));
    if ((y + h) >= maxy) 
      y = ((0 > (maxy - h))?0 : (maxy - h));
    w = ((w < maxx)?w : maxx);
    h = ((h < maxy)?h : maxy);
    node -> window = newwin(h,w,y,x);
    gnt_wm_copy_win(widget,node);
  }
#endif
  node -> panel = new_panel((node -> window));
  set_panel_userptr((node -> panel),node);
  if (!(transient != 0)) {
    GntWS *ws = (wm -> cws);
    if ((node -> me) != wm -> _list.window) {
      if ((({
        GTypeInstance *__inst = (GTypeInstance *)widget;
        GType __t = gnt_box_get_gtype();
        gboolean __r;
        if (!(__inst != 0)) 
          __r = 0;
        else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
          __r = 1;
        else 
          __r = g_type_check_instance_is_a(__inst,__t);
        __r;
      })) != 0) 
{
        ws = new_widget_find_workspace(wm,widget);
      }
      node -> ws = ws;
      ws -> list = g_list_append((ws -> list),widget);
      ws -> ordered = g_list_append((ws -> ordered),widget);
    }
    if ((((wm -> event_stack) != 0) || ((node -> me) == wm -> _list.window)) || ((node -> me) == ( *(ws -> ordered)).data)) {
      gnt_wm_raise_window(wm,(node -> me));
    }
    else {
/* New windows should not grab focus */
      bottom_panel((node -> panel));
      gnt_widget_set_focus((node -> me),0);
      gnt_widget_set_urgent((node -> me));
      if ((wm -> cws) != ws) 
        gnt_ws_widget_hide(widget,(wm -> nodes));
    }
  }
}

void gnt_wm_new_window(GntWM *wm,GntWidget *widget)
{
  while((widget -> parent) != 0)
    widget = (widget -> parent);
  if (((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_INVISIBLE) != 0U) || (g_hash_table_lookup((wm -> nodes),widget) != 0)) {
    update_screen(wm);
    return ;
  }
  if ((({
    GTypeInstance *__inst = (GTypeInstance *)widget;
    GType __t = gnt_box_get_gtype();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = 1;
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
{
    const char *title = ( *((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype())))).title;
    GntPosition *p = (GntPosition *)((void *)0);
    if ((title != 0) && ((p = (g_hash_table_lookup((wm -> positions),title))) != ((GntPosition *)((void *)0)))) {
      sanitize_position(widget,&p -> x,&p -> y,1);
      gnt_widget_set_position(widget,(p -> x),(p -> y));
      mvwin((widget -> window),(p -> y),(p -> x));
    }
  }
  g_signal_emit(wm,signals[SIG_NEW_WIN],0,widget);
  g_signal_emit(wm,signals[SIG_DECORATE_WIN],0,widget);
  if (((wm -> windows) != 0) && !((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_TRANSIENT) != 0U)) {
    if (((((({
      GTypeInstance *__inst = (GTypeInstance *)widget;
      GType __t = gnt_box_get_gtype();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = 1;
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) && (( *((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype())))).title != 0)) && (wm -> _list.window != widget)) && ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_CAN_TAKE_FOCUS) != 0U)) 
{
      gnt_tree_add_row_last(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)( *(wm -> windows)).tree),gnt_tree_get_gtype()))),widget,gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)( *(wm -> windows)).tree),gnt_tree_get_gtype()))),( *((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype())))).title),((g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *(wm -> windows)).tree),((GType )(20 << 2))))),"workspace") != 0)?(wm -> cws) : ((struct _GntWS *)((void *)0))));
      update_window_in_list(wm,widget);
    }
  }
  gnt_ws_draw_taskbar((wm -> cws),0);
  update_screen(wm);
}

void gnt_wm_window_decorate(GntWM *wm,GntWidget *widget)
{
  g_signal_emit(wm,signals[SIG_DECORATE_WIN],0,widget);
}

void gnt_wm_window_close(GntWM *wm,GntWidget *widget)
{
  GntWS *s;
  int pos;
  gboolean transient = !(!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_TRANSIENT) != 0U));
  s = gnt_wm_widget_find_workspace(wm,widget);
  if (g_hash_table_lookup((wm -> nodes),widget) == ((void *)((void *)0))) 
    return ;
  g_signal_emit(wm,signals[SIG_CLOSE_WIN],0,widget);
  g_hash_table_remove((wm -> nodes),widget);
  if ((wm -> windows) != 0) {
    gnt_tree_remove(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)( *(wm -> windows)).tree),gnt_tree_get_gtype()))),widget);
  }
  if (s != 0) {
    pos = g_list_index((s -> list),widget);
    if (pos != -1) {
      s -> list = g_list_remove((s -> list),widget);
      s -> ordered = g_list_remove((s -> ordered),widget);
      if (((s -> ordered) != 0) && ((wm -> cws) == s)) 
        gnt_wm_raise_window(wm,( *(s -> ordered)).data);
    }
  }
  else if (((transient != 0) && ((wm -> cws) != 0)) && (( *(wm -> cws)).ordered != 0)) {
    gnt_wm_update_window(wm,( *( *(wm -> cws)).ordered).data);
  }
  gnt_ws_draw_taskbar((wm -> cws),0);
  update_screen(wm);
}

time_t gnt_wm_get_idle_time()
{
  return time(0) - last_active_time;
}

gboolean gnt_wm_process_input(GntWM *wm,const char *keys)
{
  gboolean ret = 0;
  keys = gnt_bindable_remap_keys(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)wm),gnt_bindable_get_gtype()))),keys);
  idle_update = 1;
  if (ignore_keys != 0) {
    if ((keys != 0) && !(strcmp(keys,"\033\a") != 0)) {
      if (gnt_bindable_perform_action_key(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)wm),gnt_bindable_get_gtype()))),keys) != 0) {
        return 1;
      }
    }
    return (( *(wm -> cws)).ordered != 0)?gnt_widget_key_pressed(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)( *( *(wm -> cws)).ordered).data),gnt_widget_get_gtype()))),keys) : 0;
  }
  if (gnt_bindable_perform_action_key(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)wm),gnt_bindable_get_gtype()))),keys) != 0) {
    return 1;
  }
/* Do some manual checking */
  if ((( *(wm -> cws)).ordered != 0) && ((wm -> mode) != GNT_KP_MODE_NORMAL)) {
    int xmin = 0;
    int ymin = 0;
    int xmax = (stdscr != 0)?((stdscr -> _maxx) + 1) : -1;
    int ymax = ((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) - 1);
    int x;
    int y;
    int w;
    int h;
    GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)( *( *(wm -> cws)).ordered).data),gnt_widget_get_gtype()));
    int ox;
    int oy;
    int ow;
    int oh;
    gnt_widget_get_position(widget,&x,&y);
    gnt_widget_get_size(widget,&w,&h);
    ox = x;
    oy = y;
    ow = w;
    oh = h;
    if ((wm -> mode) == GNT_KP_MODE_MOVE) {
      if (strcmp(keys,(((cur_term -> type.Strings[79] != 0)?cur_term -> type.Strings[79] : ""))) == 0) {
        if (x > xmin) 
          x--;
      }
      else if (strcmp(keys,(((cur_term -> type.Strings[83] != 0)?cur_term -> type.Strings[83] : ""))) == 0) {
        if ((x + w) < xmax) 
          x++;
      }
      else if (strcmp(keys,(((cur_term -> type.Strings[87] != 0)?cur_term -> type.Strings[87] : ""))) == 0) {
        if (y > ymin) 
          y--;
      }
      else if (strcmp(keys,(((cur_term -> type.Strings[61] != 0)?cur_term -> type.Strings[61] : ""))) == 0) {
        if ((y + h) < ymax) 
          y++;
      }
      if ((ox != x) || (oy != y)) {
        gnt_screen_move_widget(widget,x,y);
        window_reverse(widget,1,wm);
        return 1;
      }
    }
    else if ((wm -> mode) == GNT_KP_MODE_RESIZE) {
      if (strcmp(keys,(((cur_term -> type.Strings[79] != 0)?cur_term -> type.Strings[79] : ""))) == 0) {
        w--;
      }
      else if (strcmp(keys,(((cur_term -> type.Strings[83] != 0)?cur_term -> type.Strings[83] : ""))) == 0) {
        if ((x + w) < xmax) 
          w++;
      }
      else if (strcmp(keys,(((cur_term -> type.Strings[87] != 0)?cur_term -> type.Strings[87] : ""))) == 0) {
        h--;
      }
      else if (strcmp(keys,(((cur_term -> type.Strings[61] != 0)?cur_term -> type.Strings[61] : ""))) == 0) {
        if ((y + h) < ymax) 
          h++;
      }
      if ((oh != h) || (ow != w)) {
        gnt_screen_resize_widget(widget,w,h);
        window_reverse(widget,1,wm);
        return 1;
      }
    }
    if ((strcmp(keys,"\r") == 0) || (strcmp(keys,"\033") == 0)) {
      window_reverse(widget,0,wm);
      wm -> mode = GNT_KP_MODE_NORMAL;
    }
    return 1;
  }
/* Escape to close the window-list or action-list window */
  if (strcmp(keys,"\033") == 0) {
    if (wm -> _list.window != 0) {
      gnt_widget_destroy(wm -> _list.window);
      return 1;
    }
  }
  else if (((keys[0] == 27) && ((( *__ctype_b_loc())[(int )keys[1]] & ((unsigned short )_ISdigit)) != 0)) && (keys[2] == 0)) {
/* Alt+x for quick switch */
    int n = (keys[1] - 48);
    GList *list = (GList *)((void *)0);
    if (n == 0) 
      n = 10;
    list = g_list_append(list,((gpointer )((glong )(n - 1))));
    switch_window_n(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)wm),gnt_bindable_get_gtype()))),list);
    g_list_free(list);
    return 1;
  }
  if ((wm -> menu) != 0) 
    ret = gnt_widget_key_pressed(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(wm -> menu)),gnt_widget_get_gtype()))),keys);
  else if (wm -> _list.window != 0) 
    ret = gnt_widget_key_pressed(wm -> _list.window,keys);
  else if (( *(wm -> cws)).ordered != 0) {
    GntWidget *win = ( *( *(wm -> cws)).ordered).data;
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)win;
      GType __t = gnt_window_get_gtype();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = 1;
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
      GntMenu *menu = ( *((GntWindow *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_window_get_gtype())))).menu;
      if (menu != 0) {
        const char *id = gnt_window_get_accel_item(((GntWindow *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_window_get_gtype()))),keys);
        if (id != 0) {
          GntMenuItem *item = gnt_menu_get_item(menu,id);
          if (item != 0) 
            ret = gnt_menuitem_activate(item);
        }
      }
    }
    if (!(ret != 0)) 
      ret = gnt_widget_key_pressed(win,keys);
  }
  return ret;
}

static void gnt_wm_win_resized(GntWM *wm,GntNode *node)
{
/*refresh_node(node->me, node, NULL);*/
}

static void gnt_wm_win_moved(GntWM *wm,GntNode *node)
{
  refresh_node((node -> me),node,0);
}

void gnt_wm_resize_window(GntWM *wm,GntWidget *widget,int width,int height)
{
  gboolean ret = 1;
  GntNode *node;
  int maxx;
  int maxy;
  while((widget -> parent) != 0)
    widget = (widget -> parent);
  node = (g_hash_table_lookup((wm -> nodes),widget));
  if (!(node != 0)) 
    return ;
  g_signal_emit(wm,signals[SIG_CONFIRM_RESIZE],0,widget,&width,&height,&ret);
  if (!(ret != 0)) 
/* resize is not permitted */
    return ;
  hide_panel((node -> panel));
  gnt_widget_set_size(widget,width,height);
  gnt_widget_draw(widget);
  maxx = ((stdscr != 0)?((stdscr -> _maxx) + 1) : -1);
  maxy = ((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) - 1);
  height = ((height < maxy)?height : maxy);
  width = ((width < maxx)?width : maxx);
  wresize((node -> window),height,width);
  replace_panel((node -> panel),(node -> window));
  g_signal_emit(wm,signals[SIG_RESIZED],0,node);
  show_panel((node -> panel));
  update_screen(wm);
}

static void write_gdi(gpointer key,gpointer value,gpointer data)
{
  GntPosition *p = value;
  fprintf(data,".%s = %d;%d\n",((char *)key),(p -> x),(p -> y));
}

static gboolean write_already(gpointer data)
{
  GntWM *wm = data;
  FILE *file;
  char *filename;
  filename = g_build_filename(g_get_home_dir(),".gntpositions",((void *)((void *)0)));
  file = fopen(filename,"wb");
  if (file == ((FILE *)((void *)0))) {
    g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: error opening file (%s) to save positions","WM","int write_already(void *)",filename);
  }
  else {
    fprintf(file,"[positions]\n");
    g_hash_table_foreach((wm -> positions),write_gdi,file);
    fclose(file);
  }
  g_free(filename);
  g_source_remove(write_timeout);
  write_timeout = 0;
  return 0;
}

static void write_positions_to_file(GntWM *wm)
{
  if (write_timeout != 0) {
    g_source_remove(write_timeout);
  }
  write_timeout = (g_timeout_add_seconds(10,write_already,wm));
}

void gnt_wm_move_window(GntWM *wm,GntWidget *widget,int x,int y)
{
  gboolean ret = 1;
  GntNode *node;
  while((widget -> parent) != 0)
    widget = (widget -> parent);
  node = (g_hash_table_lookup((wm -> nodes),widget));
  if (!(node != 0)) 
    return ;
  g_signal_emit(wm,signals[SIG_CONFIRM_MOVE],0,widget,&x,&y,&ret);
  if (!(ret != 0)) 
/* resize is not permitted */
    return ;
  gnt_widget_set_position(widget,x,y);
  move_panel((node -> panel),y,x);
  g_signal_emit(wm,signals[SIG_MOVED],0,node);
  if (((gnt_style_get_bool(GNT_STYLE_REMPOS,1) != 0) && ((({
    GTypeInstance *__inst = (GTypeInstance *)widget;
    GType __t = gnt_box_get_gtype();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = 1;
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) && !((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_TRANSIENT) != 0U)) 
{
    const char *title = ( *((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype())))).title;
    if (title != 0) {
      GntPosition *p = (GntPosition *)(g_malloc0_n(1,(sizeof(GntPosition ))));
      GntWidget *wid = (node -> me);
      p -> x = wid -> priv.x;
      p -> y = wid -> priv.y;
      g_hash_table_replace((wm -> positions),(g_strdup(title)),p);
      write_positions_to_file(wm);
    }
  }
  update_screen(wm);
}

static void gnt_wm_give_focus(GntWM *wm,GntWidget *widget)
{
  GntNode *node = (g_hash_table_lookup((wm -> nodes),widget));
  if (!(node != 0)) 
    return ;
  if (((widget != wm -> _list.window) && !((({
    GTypeInstance *__inst = (GTypeInstance *)widget;
    GType __t = gnt_menu_get_gtype();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = 1;
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) && (( *( *(wm -> cws)).ordered).data != widget)) 
{
    GntWidget *w = ( *( *(wm -> cws)).ordered).data;
    ( *(wm -> cws)).ordered = g_list_bring_to_front(( *(wm -> cws)).ordered,widget);
    gnt_widget_set_focus(w,0);
  }
  gnt_widget_set_focus(widget,1);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_URGENT);
  gnt_widget_draw(widget);
  top_panel((node -> panel));
  if (wm -> _list.window != 0) {
    GntNode *nd = (g_hash_table_lookup((wm -> nodes),wm -> _list.window));
    top_panel((nd -> panel));
  }
  gnt_ws_draw_taskbar((wm -> cws),0);
  update_screen(wm);
}

void gnt_wm_update_window(GntWM *wm,GntWidget *widget)
{
  GntNode *node = (GntNode *)((void *)0);
  GntWS *ws;
  while((widget -> parent) != 0)
    widget = (widget -> parent);
  if (!((({
    GTypeInstance *__inst = (GTypeInstance *)widget;
    GType __t = gnt_menu_get_gtype();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = 1;
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) 
{
    if (!((({
      GTypeInstance *__inst = (GTypeInstance *)widget;
      GType __t = gnt_box_get_gtype();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = 1;
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0)) 
      return ;
    gnt_box_sync_children(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype()))));
  }
  ws = gnt_wm_widget_find_workspace(wm,widget);
  node = (g_hash_table_lookup((wm -> nodes),widget));
  if (node == ((GntNode *)((void *)0))) {
    gnt_wm_new_window(wm,widget);
  }
  else 
    g_signal_emit(wm,signals[SIG_UPDATE_WIN],0,node);
  if ((ws == (wm -> cws)) || ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_TRANSIENT) != 0U)) {
    gnt_wm_copy_win(widget,node);
    gnt_ws_draw_taskbar((wm -> cws),0);
    update_screen(wm);
  }
  else if (((ws != 0) && (ws != (wm -> cws))) && ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_URGENT) != 0U)) {
    if (!(act != 0) || ((act != 0) && !(g_list_find(act,ws) != 0))) 
      act = g_list_prepend(act,ws);
    update_act_msg();
  }
}

gboolean gnt_wm_process_click(GntWM *wm,GntMouseEvent event,int x,int y,GntWidget *widget)
{
  gboolean ret = 1;
  idle_update = 1;
  g_signal_emit(wm,signals[SIG_MOUSE_CLICK],0,event,x,y,widget,&ret);
  return ret;
}

void gnt_wm_raise_window(GntWM *wm,GntWidget *widget)
{
  GntWS *ws = gnt_wm_widget_find_workspace(wm,widget);
  if ((wm -> cws) != ws) 
    gnt_wm_switch_workspace(wm,g_list_index((wm -> workspaces),ws));
  if (widget != ( *( *(wm -> cws)).ordered).data) {
    GntWidget *wid = ( *( *(wm -> cws)).ordered).data;
    ( *(wm -> cws)).ordered = g_list_bring_to_front(( *(wm -> cws)).ordered,widget);
    gnt_widget_set_focus(wid,0);
    gnt_widget_draw(wid);
  }
  gnt_widget_set_focus(widget,1);
  gnt_widget_draw(widget);
  g_signal_emit(wm,signals[SIG_GIVE_FOCUS],0,widget);
}

void gnt_wm_set_event_stack(GntWM *wm,gboolean set)
{
  wm -> event_stack = set;
}
