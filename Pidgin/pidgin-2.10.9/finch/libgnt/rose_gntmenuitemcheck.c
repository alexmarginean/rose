/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "gntinternal.h"
#include "gntmenuitemcheck.h"
static GntMenuItemClass *parent_class = (GntMenuItemClass *)((void *)0);

static void gnt_menuitem_check_class_init(GntMenuItemCheckClass *klass)
{
  parent_class = ((GntMenuItemClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_menuitem_get_gtype())));;
}

static void gnt_menuitem_check_init(GTypeInstance *instance,gpointer class)
{;
}
/******************************************************************************
 * GntMenuItemCheck API
 *****************************************************************************/

GType gnt_menuitem_check_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(GntMenuItemCheckClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gnt_menuitem_check_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GntMenuItemCheck ))), (0), (gnt_menuitem_check_init), ((const GTypeValueTable *)((void *)0))
/* base_init		*/
/* base_finalize	*/
/* class_finalize	*/
/* class_data		*/
/* n_preallocs		*/
/* instance_init	*/
/* value_table		*/
};
    type = g_type_register_static(gnt_menuitem_get_gtype(),"GntMenuItemCheck",&info,0);
  }
  return type;
}

GntMenuItem *gnt_menuitem_check_new(const char *text)
{
  GntMenuItem *item = (g_object_new(gnt_menuitem_check_get_gtype(),0));
  GntMenuItem *menuitem = (GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()));
  menuitem -> text = g_strdup(text);
  return item;
}

gboolean gnt_menuitem_check_get_checked(GntMenuItemCheck *item)
{
  return item -> checked;
}

void gnt_menuitem_check_set_checked(GntMenuItemCheck *item,gboolean set)
{
  item -> checked = set;
}
