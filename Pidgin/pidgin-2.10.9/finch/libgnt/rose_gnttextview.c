/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "gntinternal.h"
#undef GNT_LOG_DOMAIN
#define GNT_LOG_DOMAIN "TextView"
#include "gntstyle.h"
#include "gnttextview.h"
#include "gntutils.h"
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
enum __unnamed_enum___F0_L35_C1_SIGS {SIGS=1};
typedef struct __unnamed_class___F0_L40_C9_unknown_scope_and_name_variable_declaration__variable_type_L291R_variable_name_unknown_scope_and_name__scope__tvflag__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_chtypeUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__flags__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__start__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__end {
GntTextFormatFlags tvflag;
chtype flags;
int start;
/* This is the next byte of the last character of this segment */
int end;}GntTextSegment;
typedef struct __unnamed_class___F0_L48_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GList_GList__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__segments__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__length__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L10R_variable_name_unknown_scope_and_name__scope__soft {
/* A list of GntTextSegments */
GList *segments;
/* The current length of the line so far (ie. onscreen width) */
int length;
/* TRUE if it's an overflow from prev. line */
gboolean soft;}GntTextLine;
typedef struct __unnamed_class___F0_L55_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__name__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__start__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__end {
char *name;
int start;
int end;}GntTextTag;
static GntWidgetClass *parent_class = (GntWidgetClass *)((void *)0);
static gchar *select_start;
static gchar *select_end;
static gboolean double_click;
static void reset_text_view(GntTextView *view);

static gboolean text_view_contains(GntTextView *view,const char *str)
{
  return (str >= ( *(view -> string)).str) && (str < (( *(view -> string)).str + ( *(view -> string)).len));
}

static void gnt_text_view_draw(GntWidget *widget)
{
  GntTextView *view = (GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_text_view_get_gtype()));
  int n;
  int i = 0;
  GList *lines;
  int rows;
  int scrcol;
/* Used for top-aligned text */
  int comp = 0;
  gboolean has_scroll = !(((view -> flags) & GNT_TEXT_VIEW_NO_SCROLL) != 0U);
  wbkgd((widget -> window),(gnt_color_pair(GNT_COLOR_NORMAL)));
  werase((widget -> window));
  n = (g_list_length((view -> list)));
  if ((((view -> flags) & GNT_TEXT_VIEW_TOP_ALIGN) != 0U) && (n < widget -> priv.height)) {
    GList *now = (view -> list);
    comp = (widget -> priv.height - n);
    view -> list = g_list_nth_prev((view -> list),comp);
    if (!((view -> list) != 0)) {
      view -> list = g_list_first(now);
      comp = (widget -> priv.height - g_list_length((view -> list)));
    }
    else {
      comp = 0;
    }
  }
  for (((i = 0) , (lines = (view -> list))); (i < widget -> priv.height) && (lines != 0); (i++ , (lines = (lines -> next)))) {
    GList *iter;
    GntTextLine *line = (lines -> data);
    wmove((widget -> window),(((widget -> priv.height - 1) - i) - comp),0);
    for (iter = (line -> segments); iter != 0; iter = (iter -> next)) {
      GntTextSegment *seg = (iter -> data);
      char *end = (( *(view -> string)).str + (seg -> end));
      char back =  *end;
      chtype fl = (seg -> flags);
       *end = 0;
      if (((select_start != 0) && (select_start < (( *(view -> string)).str + (seg -> start)))) && (select_end > (( *(view -> string)).str + (seg -> end)))) {
        fl |= (((chtype )1UL) << 10 + 8);
        ((widget -> window) != 0)?(((( *(widget -> window))._attrs = ((attr_t )fl)) , 0)) : -1;
        wprintw((widget -> window),"%s",C_((( *(view -> string)).str + (seg -> start))));
      }
      else if (((select_start != 0) && (select_end != 0)) && (((select_start >= (( *(view -> string)).str + (seg -> start))) && (select_start <= (( *(view -> string)).str + (seg -> end)))) || ((select_end <= (( *(view -> string)).str + (seg -> end))) && (select_start <= (( *(view -> string)).str + (seg -> start)))))) {
        char *cur = (( *(view -> string)).str + (seg -> start));
        while(( *cur) != 0){
          gchar *last = (cur + g_utf8_skip[ *((const guchar *)cur)]);
          gchar *str;
          if ((cur >= select_start) && (cur <= select_end)) 
            fl |= (((chtype )1UL) << 10 + 8);
          else 
            fl = (seg -> flags);
          str = g_strndup(cur,(last - cur));
          ((widget -> window) != 0)?(((( *(widget -> window))._attrs = ((attr_t )fl)) , 0)) : -1;
          waddnstr((widget -> window),C_(str),-1);
          g_free(str);
          cur = (cur + g_utf8_skip[ *((const guchar *)cur)]);
        }
      }
      else {
        ((widget -> window) != 0)?(((( *(widget -> window))._attrs = ((attr_t )fl)) , 0)) : -1;
        wprintw((widget -> window),"%s",C_((( *(view -> string)).str + (seg -> start))));
      }
       *end = back;
    }
    wattr_off((widget -> window),((attr_t )((((chtype )1UL) << 9 + 8) | (((chtype )1UL) << 11 + 8) | (((chtype )1UL) << 10 + 8))),0);
    whline((widget -> window),32,((widget -> priv.width - (line -> length)) - has_scroll));
  }
  scrcol = (widget -> priv.width - 1);
  rows = (widget -> priv.height - 2);
  if ((has_scroll != 0) && (rows > 0)) {
    int total = (g_list_length(g_list_first((view -> list))));
    int showing;
    int position;
    int up;
    int down;
    showing = (((rows * rows) / total) + 1);
    showing = ((rows < showing)?rows : showing);
    total -= rows;
    up = (g_list_length(lines));
    down = (total - up);
    position = (((rows - showing) * up) / (((1 > (up + down))?1 : (up + down))));
    position = (((lines != ((GList *)((void *)0))) > position)?(lines != ((GList *)((void *)0))) : position);
    if ((showing + position) > rows) 
      position = (rows - showing);
    if ((((showing + position) == rows) && ((view -> list) != 0)) && (( *(view -> list)).prev != 0)) 
      position = ((1 > ((rows - 1) - showing))?1 : ((rows - 1) - showing));
    else if ((((showing + position) < rows) && ((view -> list) != 0)) && !(( *(view -> list)).prev != 0)) 
      position = (rows - showing);
    (wmove((widget -> window),(position + 1),scrcol) == -1)?-1 : wvline((widget -> window),(acs_map[(unsigned char )'a'] | (gnt_color_pair(GNT_COLOR_HIGHLIGHT_D))),showing);
  }
  if (has_scroll != 0) {
    (wmove((widget -> window),0,scrcol) == -1)?-1 : waddch((widget -> window),((((lines != 0)?acs_map[(unsigned char )'-'] : 32)) | (gnt_color_pair(GNT_COLOR_HIGHLIGHT_D))));
    (wmove((widget -> window),(widget -> priv.height - 1),scrcol) == -1)?-1 : waddch((widget -> window),((((((view -> list) != 0) && (( *(view -> list)).prev != 0))?acs_map[(unsigned char )'.'] : 32)) | (gnt_color_pair(GNT_COLOR_HIGHLIGHT_D))));
  }
  wmove((widget -> window),0,0);
}

static void gnt_text_view_size_request(GntWidget *widget)
{
  if (!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U)) {
    gnt_widget_set_size(widget,64,20);
  }
}

static void gnt_text_view_map(GntWidget *widget)
{
  if ((widget -> priv.width == 0) || (widget -> priv.height == 0)) 
    gnt_widget_size_request(widget);;
}

static gboolean gnt_text_view_key_pressed(GntWidget *widget,const char *text)
{
  return 0;
}

static void free_text_segment(gpointer data,gpointer null)
{
  GntTextSegment *seg = data;
  g_free(seg);
}

static void free_text_line(gpointer data,gpointer null)
{
  GntTextLine *line = data;
  g_list_foreach((line -> segments),free_text_segment,0);
  g_list_free((line -> segments));
  g_free(line);
}

static void free_tag(gpointer data,gpointer null)
{
  GntTextTag *tag = data;
  g_free((tag -> name));
  g_free(tag);
}

static void gnt_text_view_destroy(GntWidget *widget)
{
  GntTextView *view = (GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_text_view_get_gtype()));
  view -> list = g_list_first((view -> list));
  g_list_foreach((view -> list),free_text_line,0);
  g_list_free((view -> list));
  g_list_foreach((view -> tags),free_tag,0);
  g_list_free((view -> tags));
  g_string_free((view -> string),1);
}

static char *gnt_text_view_get_p(GntTextView *view,int x,int y)
{
  int n;
  int i = 0;
  GntWidget *wid = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)view),gnt_widget_get_gtype()));
  GntTextLine *line;
  GList *lines;
  GList *segs;
  GntTextSegment *seg;
  gchar *pos;
  n = (g_list_length((view -> list)));
  y = (wid -> priv.height - y);
  if (n < y) {
    x = 0;
    y = (n - 1);
  }
  lines = g_list_nth((view -> list),(y - 1));
  if (!(lines != 0)) 
    return 0;
  do {
    line = (lines -> data);
    lines = (lines -> next);
  }while (((line != 0) && !((line -> segments) != 0)) && (lines != 0));
/* no valid line */
  if (!(line != 0) || !((line -> segments) != 0)) 
    return 0;
  segs = (line -> segments);
  seg = ((GntTextSegment *)(segs -> data));
  pos = (( *(view -> string)).str + (seg -> start));
  x = ((x < (line -> length))?x : (line -> length));
  while(++i <= x){
    gunichar *u;
    pos = (pos + g_utf8_skip[ *((const guchar *)pos)]);
    u = g_utf8_to_ucs4(pos,(-1),0,0,0);
    if ((u != 0) && (g_unichar_iswide( *u) != 0)) 
      i++;
    g_free(u);
  }
  return pos;
}

static GString *select_word_text(GntTextView *view,gchar *c)
{
  gchar *start = c;
  gchar *end = c;
  gchar *t;
  gchar *endsize;
{
    while((t = g_utf8_prev_char(start)) != 0){
      if (!((g_ascii_table[(guchar )( *t)] & G_ASCII_SPACE) != 0)) {
        if (start == ( *(view -> string)).str) 
          break; 
        start = t;
      }
      else 
        break; 
    }
  }
{
    while((t = (end + g_utf8_skip[ *((const guchar *)end)])) != 0){
      if (!((g_ascii_table[(guchar )( *t)] & G_ASCII_SPACE) != 0)) 
        end = t;
      else 
        break; 
    }
  }
  select_start = start;
  select_end = end;
/* End at the correct byte */
  endsize = (select_end + g_utf8_skip[ *((const guchar *)select_end)]);
  return g_string_new_len(start,(endsize - start));
}

static gboolean too_slow(gpointer n)
{
  double_click = 0;
  return 0;
}

static gboolean gnt_text_view_clicked(GntWidget *widget,GntMouseEvent event,int x,int y)
{
  if (event == GNT_MOUSE_SCROLL_UP) {
    gnt_text_view_scroll(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_text_view_get_gtype()))),-1);
  }
  else if (event == GNT_MOUSE_SCROLL_DOWN) {
    gnt_text_view_scroll(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_text_view_get_gtype()))),1);
  }
  else if (event == GNT_LEFT_MOUSE_DOWN) {
    select_start = gnt_text_view_get_p(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_text_view_get_gtype()))),(x - widget -> priv.x),(y - widget -> priv.y));
    g_timeout_add(500,too_slow,0);
  }
  else if (event == GNT_MOUSE_UP) {
    GntTextView *view = (GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_text_view_get_gtype()));
    if (text_view_contains(view,select_start) != 0) {
      GString *clip;
      select_end = gnt_text_view_get_p(view,(x - widget -> priv.x),(y - widget -> priv.y));
      if (select_end < select_start) {
        gchar *t = select_start;
        select_start = select_end;
        select_end = t;
      }
      if (select_start == select_end) {
        if (double_click != 0) {
          clip = select_word_text(view,select_start);
          double_click = 0;
        }
        else {
          double_click = 1;
          select_start = 0;
          select_end = 0;
          gnt_widget_draw(widget);
          return 1;
        }
      }
      else {
/* End at the correct byte */
        gchar *endsize = (select_end + g_utf8_skip[ *((const guchar *)select_end)]);
        clip = g_string_new_len(select_start,(endsize - select_start));
      }
      gnt_widget_draw(widget);
      gnt_set_clipboard_string((clip -> str));
      g_string_free(clip,1);
    }
  }
  else 
    return 0;
  return 1;
}

static void gnt_text_view_reflow(GntTextView *view)
{
/* This is pretty ugly, and inefficient. Someone do something about it. */
  GntTextLine *line;
  GList *back;
  GList *iter;
  GList *list;
  GString *string;
/* no. of 'real' lines */
  int pos = 0;
  list = (view -> list);
  while((list -> prev) != 0){
    line = (list -> data);
    if (!((line -> soft) != 0)) 
      pos++;
    list = (list -> prev);
  }
  back = g_list_last((view -> list));
  view -> list = ((GList *)((void *)0));
  string = (view -> string);
  view -> string = ((GString *)((void *)0));
  reset_text_view(view);
  view -> string = g_string_set_size((view -> string),(string -> len));
  ( *(view -> string)).len = 0;
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)view),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_DRAWING;
  for (; back != 0; back = (back -> prev)) {
    line = (back -> data);
    if (((back -> next) != 0) && !((line -> soft) != 0)) {
      gnt_text_view_append_text_with_flags(view,"\n",GNT_TEXT_FLAG_NORMAL);
    }
    for (iter = (line -> segments); iter != 0; iter = (iter -> next)) {
      GntTextSegment *seg = (iter -> data);
      char *start = ((string -> str) + (seg -> start));
      char *end = ((string -> str) + (seg -> end));
      char back =  *end;
       *end = 0;
      gnt_text_view_append_text_with_flags(view,start,(seg -> tvflag));
       *end = back;
    }
    free_text_line(line,0);
  }
  g_list_free(list);
  list = (view -> list = g_list_first((view -> list)));
/* Go back to the line that was in view before resizing started */
  while(pos-- != 0){
    while(( *((GntTextLine *)(list -> data))).soft != 0)
      list = (list -> next);
    list = (list -> next);
  }
  view -> list = list;
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)view),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_DRAWING);
  if (( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)view),gnt_widget_get_gtype())))).window != 0) 
    gnt_widget_draw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)view),gnt_widget_get_gtype()))));
  g_string_free(string,1);
}

static void gnt_text_view_size_changed(GntWidget *widget,int w,int h)
{
  if ((w != widget -> priv.width) && ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U)) {
    gnt_text_view_reflow(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_text_view_get_gtype()))));
  }
}

static void gnt_text_view_class_init(GntTextViewClass *klass)
{
  parent_class = ((GntWidgetClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_widget_get_gtype())));
  parent_class -> destroy = gnt_text_view_destroy;
  parent_class -> draw = gnt_text_view_draw;
  parent_class -> map = gnt_text_view_map;
  parent_class -> size_request = gnt_text_view_size_request;
  parent_class -> key_pressed = gnt_text_view_key_pressed;
  parent_class -> clicked = gnt_text_view_clicked;
  parent_class -> size_changed = gnt_text_view_size_changed;;
}

static void gnt_text_view_init(GTypeInstance *instance,gpointer class)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_widget_get_gtype()));
  GntTextView *view = (GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_text_view_get_gtype()));
  GntTextLine *line = (GntTextLine *)(g_malloc0_n(1,(sizeof(GntTextLine ))));
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= (GNT_WIDGET_NO_BORDER | GNT_WIDGET_NO_SHADOW | GNT_WIDGET_GROW_Y | GNT_WIDGET_GROW_X);
  widget -> priv.minw = 5;
  widget -> priv.minh = 2;
  view -> string = g_string_new(0);
  view -> list = g_list_append((view -> list),line);;
}
/******************************************************************************
 * GntTextView API
 *****************************************************************************/

GType gnt_text_view_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(GntTextViewClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gnt_text_view_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GntTextView ))), (0), (gnt_text_view_init), ((const GTypeValueTable *)((void *)0))
/* base_init		*/
/* base_finalize	*/
/* class_finalize	*/
/* class_data		*/
/* n_preallocs		*/
/* instance_init	*/
/* value_table		*/
};
    type = g_type_register_static(gnt_widget_get_gtype(),"GntTextView",&info,0);
  }
  return type;
}

GntWidget *gnt_text_view_new()
{
  GntWidget *widget = (g_object_new(gnt_text_view_get_gtype(),0));
  return widget;
}

void gnt_text_view_append_text_with_flags(GntTextView *view,const char *text,GntTextFormatFlags flags)
{
  gnt_text_view_append_text_with_tag(view,text,flags,0);
}

void gnt_text_view_append_text_with_tag(GntTextView *view,const char *text,GntTextFormatFlags flags,const char *tagname)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)view),gnt_widget_get_gtype()));
  int fl = 0;
  const char *start;
  const char *end;
  GList *list = (view -> list);
  GntTextLine *line;
  int len;
  gboolean has_scroll = !(((view -> flags) & GNT_TEXT_VIEW_NO_SCROLL) != 0U);
  gboolean wrap_word = !(((view -> flags) & GNT_TEXT_VIEW_WRAP_CHAR) != 0U);
  if ((text == ((const char *)((void *)0))) || (( *text) == 0)) 
    return ;
  fl = (gnt_text_format_flag_to_chtype(flags));
  len = ( *(view -> string)).len;
  view -> string = g_string_append((view -> string),text);
  if (tagname != 0) {
    GntTextTag *tag = (GntTextTag *)(g_malloc0_n(1,(sizeof(GntTextTag ))));
    tag -> name = g_strdup(tagname);
    tag -> start = len;
    tag -> end = ( *(view -> string)).len;
    view -> tags = g_list_append((view -> tags),tag);
  }
  view -> list = g_list_first((view -> list));
  start = (end = (( *(view -> string)).str + len));
  while(( *start) != 0){{
      GntTextLine *oldl;
      GntTextSegment *seg = (GntTextSegment *)((void *)0);
      if ((( *end) == 10) || (( *end) == 13)) {
        if (!(strncmp(end,"\r\n",2) != 0)) 
          end++;
        end++;
        start = end;
        gnt_text_view_next_line(view);
        view -> list = g_list_first((view -> list));
        continue; 
      }
      line = ( *(view -> list)).data;
      if ((line -> length) == (widget -> priv.width - has_scroll)) {
/* The last added line was exactly the same width as the widget */
        line = ((GntTextLine *)(g_malloc0_n(1,(sizeof(GntTextLine )))));
        line -> soft = 1;
        view -> list = g_list_prepend((view -> list),line);
      }
      if (((end = (strchr(start,13))) != ((const char *)((void *)0))) || ((end = (strchr(start,10))) != ((const char *)((void *)0)))) {
        len = gnt_util_onscreen_width(start,(end - has_scroll));
        if ((widget -> priv.width > 0) && (len >= ((widget -> priv.width - (line -> length)) - has_scroll))) {
          end = ((const char *)((void *)0));
        }
      }
      if (end == ((const char *)((void *)0))) 
        end = gnt_util_onscreen_width_to_pointer(start,((widget -> priv.width - (line -> length)) - has_scroll),&len);
/* Try to append to the previous segment if possible */
      if ((line -> segments) != 0) {
        seg = ( *g_list_last((line -> segments))).data;
        if ((seg -> flags) != fl) 
          seg = ((GntTextSegment *)((void *)0));
      }
      if (seg == ((GntTextSegment *)((void *)0))) {
        seg = ((GntTextSegment *)(g_malloc0_n(1,(sizeof(GntTextSegment )))));
        seg -> start = (start - ( *(view -> string)).str);
        seg -> tvflag = flags;
        seg -> flags = fl;
        line -> segments = g_list_append((line -> segments),seg);
      }
      oldl = line;
      if ((((wrap_word != 0) && (( *end) != 0)) && (( *end) != 10)) && (( *end) != 13)) {
        const char *tmp = end;
        while((((end != 0) && (( *end) != 10)) && (( *end) != 13)) && !((g_ascii_table[(guchar )( *end)] & G_ASCII_SPACE) != 0)){
          end = (g_utf8_find_prev_char((( *(view -> string)).str + (seg -> start)),end));
        }
        if (!(end != 0) || !((g_ascii_table[(guchar )( *end)] & G_ASCII_SPACE) != 0)) 
          end = tmp;
        else 
/* Remove the space */
          end++;
        line = ((GntTextLine *)(g_malloc0_n(1,(sizeof(GntTextLine )))));
        line -> soft = 1;
        view -> list = g_list_prepend((view -> list),line);
      }
      seg -> end = (end - ( *(view -> string)).str);
      oldl -> length += len;
      start = end;
    }
  }
  view -> list = list;
  gnt_widget_draw(widget);
}

void gnt_text_view_scroll(GntTextView *view,int scroll)
{
  if (scroll == 0) {
    view -> list = g_list_first((view -> list));
  }
  else if (scroll > 0) {
    GList *list = g_list_nth_prev((view -> list),scroll);
    if (list == ((GList *)((void *)0))) 
      list = g_list_first((view -> list));
    view -> list = list;
  }
  else if (scroll < 0) {
    GList *list = g_list_nth((view -> list),(-scroll));
    if (list == ((GList *)((void *)0))) 
      list = g_list_last((view -> list));
    view -> list = list;
  }
  gnt_widget_draw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)view),gnt_widget_get_gtype()))));
}

void gnt_text_view_next_line(GntTextView *view)
{
  GntTextLine *line = (GntTextLine *)(g_malloc0_n(1,(sizeof(GntTextLine ))));
  GList *list = (view -> list);
  view -> list = g_list_prepend(g_list_first((view -> list)),line);
  view -> list = list;
  gnt_widget_draw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)view),gnt_widget_get_gtype()))));
}

chtype gnt_text_format_flag_to_chtype(GntTextFormatFlags flags)
{
  chtype fl = 0;
  if ((flags & GNT_TEXT_FLAG_BOLD) != 0U) 
    fl |= (((chtype )1UL) << 13 + 8);
  if ((flags & GNT_TEXT_FLAG_UNDERLINE) != 0U) 
    fl |= (((chtype )1UL) << 9 + 8);
  if ((flags & GNT_TEXT_FLAG_BLINK) != 0U) 
    fl |= (((chtype )1UL) << 11 + 8);
  if ((flags & GNT_TEXT_FLAG_DIM) != 0U) 
    fl |= ((((chtype )1UL) << 12 + 8) | (gnt_color_pair(GNT_COLOR_DISABLED)));
  else if ((flags & GNT_TEXT_FLAG_HIGHLIGHT) != 0U) 
    fl |= ((((chtype )1UL) << 12 + 8) | (gnt_color_pair(GNT_COLOR_HIGHLIGHT)));
  else if ((flags & (((chtype )((1UL << 8) - 1UL)) << 0 + 8)) == 0) 
    fl |= (gnt_color_pair(GNT_COLOR_NORMAL));
  else 
    fl |= (flags & (((chtype )((1UL << 8) - 1UL)) << 0 + 8));
  return fl;
}

static void reset_text_view(GntTextView *view)
{
  GntTextLine *line;
  g_list_foreach((view -> list),free_text_line,0);
  g_list_free((view -> list));
  view -> list = ((GList *)((void *)0));
  line = ((GntTextLine *)(g_malloc0_n(1,(sizeof(GntTextLine )))));
  view -> list = g_list_append((view -> list),line);
  if ((view -> string) != 0) 
    g_string_free((view -> string),1);
  view -> string = g_string_new(0);
}

void gnt_text_view_clear(GntTextView *view)
{
  reset_text_view(view);
  g_list_foreach((view -> tags),free_tag,0);
  view -> tags = ((GList *)((void *)0));
  if (( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)view),gnt_widget_get_gtype())))).window != 0) 
    gnt_widget_draw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)view),gnt_widget_get_gtype()))));
}

int gnt_text_view_get_lines_below(GntTextView *view)
{
  int below = 0;
  GList *list = (view -> list);
  while((list = (list -> prev)) != 0)
    ++below;
  return below;
}

int gnt_text_view_get_lines_above(GntTextView *view)
{
  int above = 0;
  GList *list;
  list = g_list_nth((view -> list),( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)view),gnt_widget_get_gtype())))).priv.height);
  if (!(list != 0)) 
    return 0;
  while((list = (list -> next)) != 0)
    ++above;
  return above;
}
/**
 * XXX: There are quite possibly more than a few bugs here.
 */

int gnt_text_view_tag_change(GntTextView *view,const char *name,const char *text,gboolean all)
{
  GList *alllines = g_list_first((view -> list));
  GList *list;
  GList *next;
  GList *iter;
  GList *inext;
  const int text_length = ((text != 0)?strlen(text) : 0);
  int count = 0;
{
    for (list = (view -> tags); list != 0; list = next) {
      GntTextTag *tag = (list -> data);
      next = (list -> next);
      if (strcmp((tag -> name),name) == 0) {
        int change;
        char *before;
        char *after;
        count++;
        before = g_strndup(( *(view -> string)).str,(tag -> start));
        after = g_strdup((( *(view -> string)).str + (tag -> end)));
        change = (((tag -> end) - (tag -> start)) - text_length);
        g_string_printf((view -> string),"%s%s%s",before,((text != 0)?text : ""),after);
        g_free(before);
        g_free(after);
/* Update the offsets of the next tags */
        for (iter = next; iter != 0; iter = (iter -> next)) {
          GntTextTag *t = (iter -> data);
          t -> start -= change;
          t -> end -= change;
        }
/* Update the offsets of the segments */
        for (iter = alllines; iter != 0; iter = inext) {
          GList *segs;
          GList *snext;
          GntTextLine *line = (iter -> data);
          inext = (iter -> next);
          for (segs = (line -> segments); segs != 0; segs = snext) {
            GntTextSegment *seg = (segs -> data);
            snext = (segs -> next);
            if ((seg -> start) >= (tag -> end)) {
/* The segment is somewhere after the tag */
              seg -> start -= change;
              seg -> end -= change;
            }
            else if ((seg -> end) <= (tag -> start)) {
/* This segment is somewhere in front of the tag */
            }
            else if ((seg -> start) >= (tag -> start)) {
/* This segment starts in the middle of the tag */
              if (text == ((const char *)((void *)0))) {
                free_text_segment(seg,0);
                line -> segments = g_list_delete_link((line -> segments),segs);
                if ((line -> segments) == ((GList *)((void *)0))) {
                  free_text_line(line,0);
                  line = ((GntTextLine *)((void *)0));
                  if ((view -> list) == iter) {
                    if (inext != 0) 
                      view -> list = inext;
                    else 
                      view -> list = (iter -> prev);
                  }
                  alllines = g_list_delete_link(alllines,iter);
                }
              }
              else {
/* XXX: (null) */
                seg -> start = (tag -> start);
                seg -> end = ((tag -> end) - change);
              }
              if (line != 0) 
                line -> length -= change;
/* XXX: Make things work if the tagged text spans over several lines. */
            }
            else {
/* XXX: handle the rest of the conditions */
              g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: WTF! This needs to be handled properly!!%s","TextView","int gnt_text_view_tag_change(struct _GntTextView *, const char *, const char *, int)","");
            }
          }
        }
        if (text == ((const char *)((void *)0))) {
/* Remove the tag */
          view -> tags = g_list_delete_link((view -> tags),list);
          free_tag(tag,0);
        }
        else {
          tag -> end -= change;
        }
        if (!(all != 0)) 
          break; 
      }
    }
  }
  gnt_widget_draw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)view),gnt_widget_get_gtype()))));
  return count;
}

static gboolean scroll_tv(GntWidget *wid,const char *key,GntTextView *tv)
{
  if (strcmp(key,(((cur_term -> type.Strings[82] != 0)?cur_term -> type.Strings[82] : ""))) == 0) {
    gnt_text_view_scroll(tv,-(( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tv),gnt_widget_get_gtype())))).priv.height - 2));
  }
  else if (strcmp(key,(((cur_term -> type.Strings[81] != 0)?cur_term -> type.Strings[81] : ""))) == 0) {
    gnt_text_view_scroll(tv,(( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tv),gnt_widget_get_gtype())))).priv.height - 2));
  }
  else if (strcmp(key,(((cur_term -> type.Strings[61] != 0)?cur_term -> type.Strings[61] : ""))) == 0) {
    gnt_text_view_scroll(tv,1);
  }
  else if (strcmp(key,(((cur_term -> type.Strings[87] != 0)?cur_term -> type.Strings[87] : ""))) == 0) {
    gnt_text_view_scroll(tv,-1);
  }
  else {
    return 0;
  }
  return 1;
}

void gnt_text_view_attach_scroll_widget(GntTextView *view,GntWidget *widget)
{
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"key_pressed",((GCallback )scroll_tv),view,0,((GConnectFlags )0));
}

void gnt_text_view_set_flag(GntTextView *view,GntTextViewFlag flag)
{
  view -> flags |= flag;
}
/* Pager and editor setups */
struct __unnamed_class___F0_L843_C1_unknown_scope_and_name_variable_declaration__variable_type___Pb__L255R__Pe___variable_name_unknown_scope_and_name__scope__tv__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__file {
GntTextView *tv;
char *file;}pageditor;

static void cleanup_pageditor()
{
  unlink(pageditor.file);
  g_free(pageditor.file);
  pageditor.file = ((char *)((void *)0));
  pageditor.tv = ((GntTextView *)((void *)0));
}

static void editor_end_cb(int status,gpointer data)
{
  if (status == 0) {
    char *text = (char *)((void *)0);
    if (g_file_get_contents(pageditor.file,&text,0,0) != 0) {
      reset_text_view(pageditor.tv);
      gnt_text_view_append_text_with_flags(pageditor.tv,text,GNT_TEXT_FLAG_NORMAL);
      gnt_text_view_scroll(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)pageditor.tv),gnt_text_view_get_gtype()))),0);
      g_free(text);
    }
  }
  cleanup_pageditor();
}

static void pager_end_cb(int status,gpointer data)
{
  cleanup_pageditor();
}

static gboolean check_for_ext_cb(GntWidget *widget,const char *key,GntTextView *view)
{
  static const char *pager = (const char *)((void *)0);
  static const char *editor = (const char *)((void *)0);
  char *argv[] = {((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0))};
  static char path[1024UL];
  static int len = -1;
  FILE *file;
  gboolean ret;
  gboolean pg;
  if (pager == ((const char *)((void *)0))) {
    pager = gnt_key_translate((gnt_style_get_from_name("pager","key")));
    if (pager == ((const char *)((void *)0))) 
      pager = "\033v";
    editor = gnt_key_translate((gnt_style_get_from_name("editor","key")));
    if (editor == ((const char *)((void *)0))) 
      editor = "\033e";
    len = g_snprintf(path,(sizeof(path)),"%s/gnt",g_get_tmp_dir());
  }
  else {
    g_snprintf((path + len),(sizeof(path) - len),"XXXXXX");
  }
  if (strcmp(key,pager) == 0) {
    if (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"pager-for") != view) 
      return 0;
    pg = 1;
  }
  else if (strcmp(key,editor) == 0) {
    if (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"editor-for") != view) 
      return 0;
    pg = 0;
  }
  else {
    return 0;
  }
  file = fdopen(g_mkstemp(path),"wb");
  if (!(file != 0)) 
    return 0;
  fprintf(file,"%s",( *(view -> string)).str);
  fclose(file);
  pageditor.tv = view;
  pageditor.file = g_strdup(path);
  argv[0] = gnt_style_get_from_name((((pg != 0)?"pager" : "editor")),"path");
  argv[0] = ((argv[0] != 0)?argv[0] : getenv((((pg != 0)?"PAGER" : "EDITOR"))));
  argv[0] = ((argv[0] != 0)?argv[0] : (((pg != 0)?"less" : "vim")));
  argv[1] = path;
  ret = gnt_giveup_console(0,argv,0,0,0,0,((pg != 0)?pager_end_cb : editor_end_cb),0);
  return ret;
}

void gnt_text_view_attach_pager_widget(GntTextView *view,GntWidget *pager)
{
  g_signal_connect_data(pager,"key_pressed",((GCallback )check_for_ext_cb),view,0,((GConnectFlags )0));
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pager),((GType )(20 << 2))))),"pager-for",view);
}

void gnt_text_view_attach_editor_widget(GntTextView *view,GntWidget *wid)
{
  g_signal_connect_data(wid,"key_pressed",((GCallback )check_for_ext_cb),view,0,((GConnectFlags )0));
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)wid),((GType )(20 << 2))))),"editor-for",view);
}
