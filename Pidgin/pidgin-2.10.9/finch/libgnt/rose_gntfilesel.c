/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "gntinternal.h"
#undef GNT_LOG_DOMAIN
#define GNT_LOG_DOMAIN "FileSel"
#include "gntbutton.h"
#include "gntentry.h"
#include "gntfilesel.h"
#include "gntlabel.h"
#include "gntmarshal.h"
#include "gntstyle.h"
#include "gnttree.h"
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#if 0
#include <glob.h>
#endif
enum __unnamed_enum___F0_L44_C1_SIG_FILE_SELECTED__COMMA__SIGS {SIG_FILE_SELECTED,SIGS};
static GntWindowClass *parent_class = (GntWindowClass *)((void *)0);
static guint signals[1UL] = {(0)};
static void (*orig_map)(GntWidget *);
static void (*orig_size_request)(GntWidget *);
static void select_activated_cb(GntWidget *button,GntFileSel *sel);

static void gnt_file_sel_destroy(GntWidget *widget)
{
  GntFileSel *sel = (GntFileSel *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_file_sel_get_gtype()));
  g_free((sel -> current));
  g_free((sel -> suggest));
  if ((sel -> tags) != 0) {
    g_list_foreach((sel -> tags),((GFunc )g_free),0);
    g_list_free((sel -> tags));
  }
}
#if !GLIB_CHECK_VERSION(2,8,0)
/* ripped from glib/gfileutils.c */
/* Ignore empty elements */
/* If the leading and trailing separator strings are in the
				 * same element and overlap, the result is exactly that element
				 */
#endif

static char *process_path(const char *path)
{
  char **splits = (char **)((void *)0);
  int i;
  int j;
  char *str;
  char *ret;
  splits = g_strsplit(path,"/",(-1));
  for (((i = 0) , (j = 0)); splits[i] != 0; i++) {
    if (strcmp(splits[i],".") == 0) {
      g_free(splits[i]);
      splits[i] = ((char *)((void *)0));
    }
    else if (strcmp(splits[i],"..") == 0) {
      if (j != 0) 
        j--;
      g_free(splits[i]);
      splits[i] = ((char *)((void *)0));
    }
    else {
      if (i != j) {
        g_free(splits[j]);
        splits[j] = splits[i];
        splits[i] = ((char *)((void *)0));
      }
      j++;
    }
  }
  g_free(splits[j]);
  splits[j] = ((char *)((void *)0));
  str = g_build_pathv("/",splits);
  ret = g_strdup_printf("/%s",str);
  g_free(str);
  g_strfreev(splits);
  return ret;
}

static void update_location(GntFileSel *sel)
{
  char *old;
  const char *tmp;
  tmp = (((sel -> suggest) != 0)?(sel -> suggest) : ((const char *)(gnt_tree_get_selection_data((((sel -> dirsonly) != 0)?((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> dirs)),gnt_tree_get_gtype()))) : ((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> files)),gnt_tree_get_gtype()))))))));
  old = g_strdup_printf("%s%s%s",(((sel -> current) != 0)?(sel -> current) : ""),((((((sel -> current) != 0)?(sel -> current) : ""))[1] != 0)?"/" : ""),((tmp != 0)?tmp : ""));
  gnt_entry_set_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> location)),gnt_entry_get_gtype()))),old);
  g_free(old);
}

static gboolean is_tagged(GntFileSel *sel,const char *f)
{
  char *ret = g_strdup_printf("%s%s%s",(sel -> current),(((sel -> current)[1] != 0)?"/" : ""),f);
  gboolean find = (g_list_find_custom((sel -> tags),ret,((GCompareFunc )g_utf8_collate)) != ((GList *)((void *)0)));
  g_free(ret);
  return find;
}

GntFile *gnt_file_new_dir(const char *name)
{
  GntFile *file = (GntFile *)(g_malloc0_n(1,(sizeof(GntFile ))));
  file -> basename = g_strdup(name);
  file -> type = GNT_FILE_DIR;
  return file;
}

GntFile *gnt_file_new(const char *name,unsigned long size)
{
  GntFile *file = (GntFile *)(g_malloc0_n(1,(sizeof(GntFile ))));
  file -> basename = g_strdup(name);
  file -> type = GNT_FILE_REGULAR;
  file -> size = size;
  return file;
}

static gboolean local_read_fn(const char *path,GList **files,GError **error)
{
  GDir *dir;
  GntFile *file;
  const char *str;
  dir = g_dir_open(path,0,error);
  if ((dir == ((GDir *)((void *)0))) || ((error != 0) && ( *error != 0))) {
    return 0;
  }
   *files = ((GList *)((void *)0));
  if ((( *path) != 0) && (strcmp(path,"/") != 0)) {
    file = gnt_file_new_dir("..");
     *files = g_list_prepend( *files,file);
  }
  while((str = g_dir_read_name(dir)) != ((const char *)((void *)0))){
    char *fp = g_build_filename(path,str,((void *)((void *)0)));
    struct stat st;
    if (stat(fp,&st) != 0) {
      g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: Error stating location %s","FileSel","int local_read_fn(const char *, struct _GList **, struct _GError **)",fp);
    }
    else {
      if ((st.st_mode & 0170000) == 0040000) {
        file = gnt_file_new_dir(str);
      }
      else {
        file = gnt_file_new(str,((long )st.st_size));
      }
       *files = g_list_prepend( *files,file);
    }
    g_free(fp);
  }
  g_dir_close(dir);
   *files = g_list_reverse( *files);
  return 1;
}

static void gnt_file_free(GntFile *file)
{
  g_free((file -> fullpath));
  g_free((file -> basename));
  g_free(file);
}

static gboolean location_changed(GntFileSel *sel,GError **err)
{
  GList *files;
  GList *iter;
  gboolean success;
  if (!((sel -> dirs) != 0)) 
    return 1;
  gnt_tree_remove_all(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> dirs)),gnt_tree_get_gtype()))));
  if ((sel -> files) != 0) 
    gnt_tree_remove_all(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> files)),gnt_tree_get_gtype()))));
  gnt_entry_set_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> location)),gnt_entry_get_gtype()))),0);
  if ((sel -> current) == ((char *)((void *)0))) {
    if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)sel),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U) 
      gnt_widget_draw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)sel),gnt_widget_get_gtype()))));
    return 1;
  }
/* XXX:\
	 * XXX: This is blocking.
	 * XXX:/
	 */
  files = ((GList *)((void *)0));
  if ((sel -> read_fn) != 0) 
    success = ( *(sel -> read_fn))((sel -> current),&files,err);
  else 
    success = local_read_fn((sel -> current),&files,err);
  if (!(success != 0) || ( *err != 0)) {
    g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: error opening location %s (%s)","FileSel","int location_changed(struct _GntFileSel *, struct _GError **)",(sel -> current),(( *err != 0)?( *( *err)).message : "reason unknown"));
    return 0;
  }
  for (iter = files; iter != 0; iter = (iter -> next)) {
    GntFile *file = (iter -> data);
    char *str = (file -> basename);
    if ((file -> type) == GNT_FILE_DIR) {
      gnt_tree_add_row_after(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> dirs)),gnt_tree_get_gtype()))),(g_strdup(str)),gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> dirs)),gnt_tree_get_gtype()))),str),0,0);
      if ((((sel -> multiselect) != 0) && ((sel -> dirsonly) != 0)) && (is_tagged(sel,str) != 0)) 
        gnt_tree_set_row_flags(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> dirs)),gnt_tree_get_gtype()))),((gpointer )str),GNT_TEXT_FLAG_BOLD);
    }
    else if (!((sel -> dirsonly) != 0)) {
      char size[128UL];
      snprintf(size,(sizeof(size)),"%ld",(file -> size));
      gnt_tree_add_row_after(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> files)),gnt_tree_get_gtype()))),(g_strdup(str)),gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> files)),gnt_tree_get_gtype()))),str,size,""),0,0);
      if (((sel -> multiselect) != 0) && (is_tagged(sel,str) != 0)) 
        gnt_tree_set_row_flags(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> files)),gnt_tree_get_gtype()))),((gpointer )str),GNT_TEXT_FLAG_BOLD);
    }
  }
  g_list_foreach(files,((GFunc )gnt_file_free),0);
  g_list_free(files);
  if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)sel),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U) 
    gnt_widget_draw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)sel),gnt_widget_get_gtype()))));
  return 1;
}

static gboolean dir_key_pressed(GntTree *tree,const char *key,GntFileSel *sel)
{
  if ((strcmp(key,"\r") == 0) || (strcmp(key,"\n") == 0)) {
    char *str = g_strdup((gnt_tree_get_selection_data(tree)));
    char *path;
    char *dir;
    if (!(str != 0)) 
      return 1;
    path = g_build_filename((sel -> current),str,((void *)((void *)0)));
    dir = g_path_get_basename((sel -> current));
    if (!(gnt_file_sel_set_current_location(sel,path) != 0)) {
      gnt_tree_set_selected(tree,str);
    }
    else if (strcmp(str,"..") == 0) {
      gnt_tree_set_selected(tree,dir);
    }
    gnt_bindable_perform_action_named(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_bindable_get_gtype()))),"end-search",((void *)((void *)0)));
    g_free(dir);
    g_free(str);
    g_free(path);
    return 1;
  }
  return 0;
}

static gboolean location_key_pressed(GntTree *tree,const char *key,GntFileSel *sel)
{
  char *path;
  char *str;
#if 0
#endif
  if ((strcmp(key,"\r") != 0) && (strcmp(key,"\n") != 0)) 
    return 0;
  str = ((char *)(gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> location)),gnt_entry_get_gtype()))))));
  if (( *str) == '/') 
    path = g_strdup(str);
  else 
    path = g_strdup_printf("%s/%s",(sel -> current),str);
  str = process_path(path);
  g_free(path);
  path = str;
  if (gnt_file_sel_set_current_location(sel,path) != 0) 
    goto success;
  path = g_path_get_dirname(str);
  g_free(str);
  if (!(gnt_file_sel_set_current_location(sel,path) != 0)) {
    g_free(path);
    return 0;
  }
#if 0
/* XXX: there needs to be a way to allow other methods for globbing,
	 * like the read_fn stuff. */
/* XXX: do something with the return value */
/* XXX: check the return value */
#endif
  success:
  g_free(path);
  return 1;
}

static void file_sel_changed(GntWidget *widget,gpointer old,gpointer current,GntFileSel *sel)
{
  if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_HAS_FOCUS) != 0U) {
    g_free((sel -> suggest));
    sel -> suggest = ((char *)((void *)0));
    update_location(sel);
  }
}

static void gnt_file_sel_map(GntWidget *widget)
{
  GntFileSel *sel = (GntFileSel *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_file_sel_get_gtype()));
  GntWidget *hbox;
  GntWidget *vbox;
  if ((sel -> current) == ((char *)((void *)0))) 
    gnt_file_sel_set_current_location(sel,g_get_home_dir());
  vbox = gnt_box_new(0,1);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),0);
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),GNT_ALIGN_MID);
/* The dir. and files list */
  hbox = gnt_box_new(0,0);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),(sel -> dirs));
  if (!((sel -> dirsonly) != 0)) {
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),(sel -> files));
  }
  else {
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> dirs)),((GType )(20 << 2))))),"selection_changed",((GCallback )file_sel_changed),sel,0,((GConnectFlags )0));
  }
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),hbox);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),(sel -> location));
/* The buttons */
  hbox = gnt_box_new(0,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),(sel -> cancel));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),(sel -> select));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),hbox);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)sel),gnt_box_get_gtype()))),vbox);
  ( *orig_map)(widget);
  update_location(sel);
}

static gboolean toggle_tag_selection(GntBindable *bind,GList *null)
{
  GntFileSel *sel = (GntFileSel *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_file_sel_get_gtype()));
  char *str;
  GList *find;
  char *file;
  GntWidget *tree;
  if (!((sel -> multiselect) != 0)) 
    return 0;
  tree = (((sel -> dirsonly) != 0)?(sel -> dirs) : (sel -> files));
  if (!(gnt_widget_has_focus(tree) != 0) || (gnt_tree_is_searching(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype())))) != 0)) 
    return 0;
  file = (gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype())))));
  str = gnt_file_sel_get_selected_file(sel);
  if ((find = g_list_find_custom((sel -> tags),str,((GCompareFunc )g_utf8_collate))) != ((GList *)((void *)0))) {
    g_free((find -> data));
    sel -> tags = g_list_delete_link((sel -> tags),find);
    gnt_tree_set_row_flags(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),file,GNT_TEXT_FLAG_NORMAL);
    g_free(str);
  }
  else {
    sel -> tags = g_list_prepend((sel -> tags),str);
    gnt_tree_set_row_flags(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),file,GNT_TEXT_FLAG_BOLD);
  }
  gnt_bindable_perform_action_named(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_bindable_get_gtype()))),"move-down",((void *)((void *)0)));
  return 1;
}

static gboolean clear_tags(GntBindable *bind,GList *null)
{
  GntFileSel *sel = (GntFileSel *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_file_sel_get_gtype()));
  GntWidget *tree;
  GList *iter;
  if (!((sel -> multiselect) != 0)) 
    return 0;
  tree = (((sel -> dirsonly) != 0)?(sel -> dirs) : (sel -> files));
  if (!(gnt_widget_has_focus(tree) != 0) || (gnt_tree_is_searching(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype())))) != 0)) 
    return 0;
  g_list_foreach((sel -> tags),((GFunc )g_free),0);
  g_list_free((sel -> tags));
  sel -> tags = ((GList *)((void *)0));
  for (iter = ( *((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype())))).list; iter != 0; iter = (iter -> next)) 
    gnt_tree_set_row_flags(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),(iter -> data),GNT_TEXT_FLAG_NORMAL);
  return 1;
}

static gboolean up_directory(GntBindable *bind,GList *null)
{
  char *path;
  char *dir;
  GntFileSel *sel = (GntFileSel *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_file_sel_get_gtype()));
  if (!(gnt_widget_has_focus((sel -> dirs)) != 0) && !(gnt_widget_has_focus((sel -> files)) != 0)) 
    return 0;
  if ((gnt_tree_is_searching(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> dirs)),gnt_tree_get_gtype())))) != 0) || (gnt_tree_is_searching(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> files)),gnt_tree_get_gtype())))) != 0)) 
    return 0;
  path = g_build_filename((sel -> current),"..",((void *)((void *)0)));
  dir = g_path_get_basename((sel -> current));
  if (gnt_file_sel_set_current_location(sel,path) != 0) 
    gnt_tree_set_selected(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> dirs)),gnt_tree_get_gtype()))),dir);
  g_free(dir);
  g_free(path);
  return 1;
}

static void gnt_file_sel_size_request(GntWidget *widget)
{
  GntFileSel *sel;
  if (widget -> priv.height > 0) 
    return ;
  sel = ((GntFileSel *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_file_sel_get_gtype())));
  ( *(sel -> dirs)).priv.height = 16;
  ( *(sel -> files)).priv.height = 16;
  ( *orig_size_request)(widget);
}

static void gnt_file_sel_class_init(GntFileSelClass *klass)
{
  GntBindableClass *bindable = (GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()));
  GntWidgetClass *kl = (GntWidgetClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_widget_get_gtype()));
  parent_class = ((GntWindowClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_window_get_gtype())));
  kl -> destroy = gnt_file_sel_destroy;
  orig_map = (kl -> map);
  kl -> map = gnt_file_sel_map;
  orig_size_request = (kl -> size_request);
  kl -> size_request = gnt_file_sel_size_request;
  signals[SIG_FILE_SELECTED] = g_signal_new("file_selected",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntFileSelClass *)((GntFileSelClass *)0))).file_selected))),0,0,gnt_closure_marshal_VOID__STRING_STRING,((GType )(1 << 2)),2,((GType )(16 << 2)),((GType )(16 << 2)));
  gnt_bindable_class_register_action(bindable,"toggle-tag",toggle_tag_selection,"t",((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"clear-tags",clear_tags,"c",((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"up-directory",up_directory,((cur_term -> type.Strings[55] != 0)?cur_term -> type.Strings[55] : ""),((void *)((void *)0)));
  gnt_style_read_actions(( *((GTypeClass *)klass)).g_type,((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))));;
}

static void gnt_file_sel_init(GTypeInstance *instance,gpointer class)
{
  GntFileSel *sel = (GntFileSel *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_file_sel_get_gtype()));
  sel -> dirs = gnt_tree_new();
  gnt_tree_set_compare_func(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> dirs)),gnt_tree_get_gtype()))),((GCompareFunc )g_utf8_collate));
  gnt_tree_set_hash_fns(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> dirs)),gnt_tree_get_gtype()))),g_str_hash,g_str_equal,g_free);
  gnt_tree_set_column_titles(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> dirs)),gnt_tree_get_gtype()))),"Directories");
  gnt_tree_set_show_title(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> dirs)),gnt_tree_get_gtype()))),1);
  gnt_tree_set_col_width(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> dirs)),gnt_tree_get_gtype()))),0,20);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> dirs)),((GType )(20 << 2))))),"key_pressed",((GCallback )dir_key_pressed),sel,0,((GConnectFlags )0));
/* Name, Size */
  sel -> files = gnt_tree_new_with_columns(2);
  gnt_tree_set_compare_func(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> files)),gnt_tree_get_gtype()))),((GCompareFunc )g_utf8_collate));
  gnt_tree_set_hash_fns(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> files)),gnt_tree_get_gtype()))),g_str_hash,g_str_equal,g_free);
  gnt_tree_set_column_titles(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> files)),gnt_tree_get_gtype()))),"Filename","Size");
  gnt_tree_set_show_title(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> files)),gnt_tree_get_gtype()))),1);
  gnt_tree_set_col_width(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> files)),gnt_tree_get_gtype()))),0,25);
  gnt_tree_set_col_width(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> files)),gnt_tree_get_gtype()))),1,10);
  gnt_tree_set_column_is_right_aligned(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> files)),gnt_tree_get_gtype()))),1,1);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> files)),((GType )(20 << 2))))),"selection_changed",((GCallback )file_sel_changed),sel,0,((GConnectFlags )0));
/* The location entry */
  sel -> location = gnt_entry_new(0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> location)),((GType )(20 << 2))))),"key_pressed",((GCallback )location_key_pressed),sel,0,((GConnectFlags )0));
  sel -> cancel = gnt_button_new("Cancel");
  sel -> select = gnt_button_new("Select");
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> files)),((GType )(20 << 2))))),"activate",((GCallback )gnt_widget_activate),(sel -> select),0,G_CONNECT_SWAPPED);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> select)),((GType )(20 << 2))))),"activate",((GCallback )select_activated_cb),sel,0,((GConnectFlags )0));
}
/******************************************************************************
 * GntFileSel API
 *****************************************************************************/

GType gnt_file_sel_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(GntFileSelClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gnt_file_sel_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GntFileSel ))), (0), (gnt_file_sel_init), ((const GTypeValueTable *)((void *)0))
/* base_init		*/
/* base_finalize	*/
/* class_finalize	*/
/* class_data		*/
/* n_preallocs		*/
/* instance_init	*/
};
    type = g_type_register_static(gnt_window_get_gtype(),"GntFileSel",&info,0);
  }
  return type;
}

static void select_activated_cb(GntWidget *button,GntFileSel *sel)
{
  char *path = gnt_file_sel_get_selected_file(sel);
  char *file = g_path_get_basename(path);
  g_signal_emit(sel,signals[SIG_FILE_SELECTED],0,path,file);
  g_free(file);
  g_free(path);
}

GntWidget *gnt_file_sel_new()
{
  GntWidget *widget = (g_object_new(gnt_file_sel_get_gtype(),0));
  return widget;
}

gboolean gnt_file_sel_set_current_location(GntFileSel *sel,const char *path)
{
  char *old;
  GError *error = (GError *)((void *)0);
  gboolean ret = 1;
  old = (sel -> current);
  sel -> current = process_path(path);
  if (!(location_changed(sel,&error) != 0)) {
    g_error_free(error);
    error = ((GError *)((void *)0));
    g_free((sel -> current));
    sel -> current = old;
    location_changed(sel,&error);
    ret = 0;
  }
  else 
    g_free(old);
  update_location(sel);
  return ret;
}

void gnt_file_sel_set_dirs_only(GntFileSel *sel,gboolean dirs)
{
  sel -> dirsonly = dirs;
}

gboolean gnt_file_sel_get_dirs_only(GntFileSel *sel)
{
  return sel -> dirsonly;
}

void gnt_file_sel_set_suggested_filename(GntFileSel *sel,const char *suggest)
{
  g_free((sel -> suggest));
  sel -> suggest = g_strdup(suggest);
}

char *gnt_file_sel_get_selected_file(GntFileSel *sel)
{
  char *ret;
  if ((sel -> dirsonly) != 0) {
    ret = g_path_get_dirname(gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> location)),gnt_entry_get_gtype())))));
  }
  else {
    ret = g_strdup(gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> location)),gnt_entry_get_gtype())))));
  }
  return ret;
}

void gnt_file_sel_set_must_exist(GntFileSel *sel,gboolean must)
{
/*XXX: What do I do with this? */
  sel -> must_exist = must;
}

gboolean gnt_file_sel_get_must_exist(GntFileSel *sel)
{
  return sel -> must_exist;
}

void gnt_file_sel_set_multi_select(GntFileSel *sel,gboolean set)
{
  sel -> multiselect = set;
}

GList *gnt_file_sel_get_selected_multi_files(GntFileSel *sel)
{
  GList *list = (GList *)((void *)0);
  GList *iter;
  char *str = gnt_file_sel_get_selected_file(sel);
  for (iter = (sel -> tags); iter != 0; iter = (iter -> next)) {
    list = g_list_prepend(list,(g_strdup((iter -> data))));
    if (g_utf8_collate(str,(iter -> data)) != 0) {
      g_free(str);
      str = ((char *)((void *)0));
    }
  }
  if (str != 0) 
    list = g_list_prepend(list,str);
  list = g_list_reverse(list);
  return list;
}

void gnt_file_sel_set_read_fn(GntFileSel *sel,gboolean (*read_fn)(const char *, GList **, GError **))
{
  sel -> read_fn = read_fn;
}
