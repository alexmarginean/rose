/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "gntinternal.h"
#undef GNT_LOG_DOMAIN
#define GNT_LOG_DOMAIN "Keys"
#include "gntkeys.h"
#include <glib.h>
#include <stdlib.h>
#include <string.h>
char *gnt_key_cup;
char *gnt_key_cdown;
char *gnt_key_cleft;
char *gnt_key_cright;
static const char *term;
static GHashTable *specials;

void gnt_init_keys()
{
  const char *controls[] = {(""), ("c-"), ("ctrl-"), ("ctr-"), ("ctl-"), ((const char *)((void *)0))};
  const char *alts[] = {(""), ("alt-"), ("a-"), ("m-"), ("meta-"), ((const char *)((void *)0))};
  int c;
  int a;
  int ch;
  char key[32UL];
  if (term == ((const char *)((void *)0))) {
    term = (getenv("TERM"));
    if (!(term != 0)) 
/* Just in case */
      term = "";
  }
  if (((strstr(term,"xterm")) == term) || (strcmp(term,"rxvt") == 0)) {
    gnt_key_cup = "\033[1;5A";
    gnt_key_cdown = "\033[1;5B";
    gnt_key_cright = "\033[1;5C";
    gnt_key_cleft = "\033[1;5D";
  }
  else if (((strstr(term,"screen")) == term) || (strcmp(term,"rxvt-unicode") == 0)) {
    gnt_key_cup = "\033Oa";
    gnt_key_cdown = "\033Ob";
    gnt_key_cright = "\033Oc";
    gnt_key_cleft = "\033Od";
  }
  specials = g_hash_table_new(g_str_hash,g_str_equal);
#define INSERT_KEY(k, code) do { \
		g_hash_table_insert(specials, g_strdup(k), g_strdup(code)); \
		gnt_keys_add_combination(code); \
	} while (0)
  do {
    g_hash_table_insert(specials,(g_strdup("home")),(g_strdup(((cur_term -> type.Strings[76] != 0)?cur_term -> type.Strings[76] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[76] != 0)?cur_term -> type.Strings[76] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("end")),(g_strdup(((cur_term -> type.Strings[164] != 0)?cur_term -> type.Strings[164] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[164] != 0)?cur_term -> type.Strings[164] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("pageup")),(g_strdup(((cur_term -> type.Strings[82] != 0)?cur_term -> type.Strings[82] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[82] != 0)?cur_term -> type.Strings[82] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("pagedown")),(g_strdup(((cur_term -> type.Strings[81] != 0)?cur_term -> type.Strings[81] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[81] != 0)?cur_term -> type.Strings[81] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("insert")),(g_strdup(((cur_term -> type.Strings[77] != 0)?cur_term -> type.Strings[77] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[77] != 0)?cur_term -> type.Strings[77] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("delete")),(g_strdup(((cur_term -> type.Strings[59] != 0)?cur_term -> type.Strings[59] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[59] != 0)?cur_term -> type.Strings[59] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("back_tab")),(g_strdup(((cur_term -> type.Strings[0] != 0)?cur_term -> type.Strings[0] : (((cur_term -> type.Strings[148] != 0)?cur_term -> type.Strings[148] : ""))))));
    gnt_keys_add_combination(((cur_term -> type.Strings[0] != 0)?cur_term -> type.Strings[0] : (((cur_term -> type.Strings[148] != 0)?cur_term -> type.Strings[148] : ""))));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("left")),(g_strdup(((cur_term -> type.Strings[79] != 0)?cur_term -> type.Strings[79] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[79] != 0)?cur_term -> type.Strings[79] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("right")),(g_strdup(((cur_term -> type.Strings[83] != 0)?cur_term -> type.Strings[83] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[83] != 0)?cur_term -> type.Strings[83] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("up")),(g_strdup(((cur_term -> type.Strings[87] != 0)?cur_term -> type.Strings[87] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[87] != 0)?cur_term -> type.Strings[87] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("down")),(g_strdup(((cur_term -> type.Strings[61] != 0)?cur_term -> type.Strings[61] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[61] != 0)?cur_term -> type.Strings[61] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("tab")),(g_strdup("\t")));
    gnt_keys_add_combination("\t");
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("escape")),(g_strdup("\033")));
    gnt_keys_add_combination("\033");
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("space")),(g_strdup(" ")));
    gnt_keys_add_combination(" ");
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("return")),(g_strdup(cur_term -> type.Strings[2])));
    gnt_keys_add_combination(cur_term -> type.Strings[2]);
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("menu")),(g_strdup(((cur_term -> type.Strings[221] != 0)?cur_term -> type.Strings[221] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[221] != 0)?cur_term -> type.Strings[221] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("f1")),(g_strdup(((cur_term -> type.Strings[66] != 0)?cur_term -> type.Strings[66] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[66] != 0)?cur_term -> type.Strings[66] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("f2")),(g_strdup(((cur_term -> type.Strings[68] != 0)?cur_term -> type.Strings[68] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[68] != 0)?cur_term -> type.Strings[68] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("f3")),(g_strdup(((cur_term -> type.Strings[69] != 0)?cur_term -> type.Strings[69] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[69] != 0)?cur_term -> type.Strings[69] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("f4")),(g_strdup(((cur_term -> type.Strings[70] != 0)?cur_term -> type.Strings[70] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[70] != 0)?cur_term -> type.Strings[70] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("f5")),(g_strdup(((cur_term -> type.Strings[71] != 0)?cur_term -> type.Strings[71] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[71] != 0)?cur_term -> type.Strings[71] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("f6")),(g_strdup(((cur_term -> type.Strings[72] != 0)?cur_term -> type.Strings[72] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[72] != 0)?cur_term -> type.Strings[72] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("f7")),(g_strdup(((cur_term -> type.Strings[73] != 0)?cur_term -> type.Strings[73] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[73] != 0)?cur_term -> type.Strings[73] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("f8")),(g_strdup(((cur_term -> type.Strings[74] != 0)?cur_term -> type.Strings[74] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[74] != 0)?cur_term -> type.Strings[74] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("f9")),(g_strdup(((cur_term -> type.Strings[75] != 0)?cur_term -> type.Strings[75] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[75] != 0)?cur_term -> type.Strings[75] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("f10")),(g_strdup(((cur_term -> type.Strings[67] != 0)?cur_term -> type.Strings[67] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[67] != 0)?cur_term -> type.Strings[67] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("f11")),(g_strdup(((cur_term -> type.Strings[216] != 0)?cur_term -> type.Strings[216] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[216] != 0)?cur_term -> type.Strings[216] : ""));
  }while (0);
  do {
    g_hash_table_insert(specials,(g_strdup("f12")),(g_strdup(((cur_term -> type.Strings[217] != 0)?cur_term -> type.Strings[217] : ""))));
    gnt_keys_add_combination(((cur_term -> type.Strings[217] != 0)?cur_term -> type.Strings[217] : ""));
  }while (0);
#define REM_LENGTH  (sizeof(key) - (cur - key))
#define INSERT_COMB(k, code) do { \
		snprintf(key, sizeof(key), "%s%s%s", controls[c], alts[a], k);  \
		INSERT_KEY(key, code);  \
	} while (0)
#define INSERT_COMB_CODE(k, c1, c2) do { \
		char __[32]; \
		snprintf(__, sizeof(__), "%s%s", c1, c2); \
		INSERT_COMB(k, __); \
	} while (0)
/* Lower-case alphabets */
  for (((a = 0) , (c = 0)); controls[c] != 0; (c++ , (a = 0))) {
    if (c != 0) {
      do {
        snprintf(key,(sizeof(key)),"%s%s%s",controls[c],alts[a],"up");
        do {
          g_hash_table_insert(specials,(g_strdup(key)),(g_strdup(gnt_key_cup)));
          gnt_keys_add_combination(gnt_key_cup);
        }while (0);
      }while (0);
      do {
        snprintf(key,(sizeof(key)),"%s%s%s",controls[c],alts[a],"down");
        do {
          g_hash_table_insert(specials,(g_strdup(key)),(g_strdup(gnt_key_cdown)));
          gnt_keys_add_combination(gnt_key_cdown);
        }while (0);
      }while (0);
      do {
        snprintf(key,(sizeof(key)),"%s%s%s",controls[c],alts[a],"left");
        do {
          g_hash_table_insert(specials,(g_strdup(key)),(g_strdup(gnt_key_cleft)));
          gnt_keys_add_combination(gnt_key_cleft);
        }while (0);
      }while (0);
      do {
        snprintf(key,(sizeof(key)),"%s%s%s",controls[c],alts[a],"right");
        do {
          g_hash_table_insert(specials,(g_strdup(key)),(g_strdup(gnt_key_cright)));
          gnt_keys_add_combination(gnt_key_cright);
        }while (0);
      }while (0);
    }
    for (a = 0; alts[a] != 0; a++) {
      for (ch = 0; ch < 26; ch++) {
        char str[2UL] = {(('a' + ch)), (0)};
        char code[4UL] = "\000\000\000";
        int ind = 0;
        if (a != 0) 
          code[ind++] = 27;
        code[ind] = ((((c != 0)?1 : 'a')) + ch);
        do {
          snprintf(key,(sizeof(key)),"%s%s%s",controls[c],alts[a],str);
          do {
            g_hash_table_insert(specials,(g_strdup(key)),(g_strdup(code)));
            gnt_keys_add_combination(code);
          }while (0);
        }while (0);
      }
      if ((c == 0) && (a != 0)) {
        do {
          snprintf(key,(sizeof(key)),"%s%s%s",controls[c],alts[a],"tab");
          do {
            g_hash_table_insert(specials,(g_strdup(key)),(g_strdup("\033\t")));
            gnt_keys_add_combination("\033\t");
          }while (0);
        }while (0);
        do {
          char __[32UL];
          snprintf(__,(sizeof(__)),"%s%s","\033",((cur_term -> type.Strings[87] != 0)?cur_term -> type.Strings[87] : ""));
          do {
            snprintf(key,(sizeof(key)),"%s%s%s",controls[c],alts[a],"up");
            do {
              g_hash_table_insert(specials,(g_strdup(key)),(g_strdup(__)));
              gnt_keys_add_combination(__);
            }while (0);
          }while (0);
        }while (0);
        do {
          char __[32UL];
          snprintf(__,(sizeof(__)),"%s%s","\033",((cur_term -> type.Strings[61] != 0)?cur_term -> type.Strings[61] : ""));
          do {
            snprintf(key,(sizeof(key)),"%s%s%s",controls[c],alts[a],"down");
            do {
              g_hash_table_insert(specials,(g_strdup(key)),(g_strdup(__)));
              gnt_keys_add_combination(__);
            }while (0);
          }while (0);
        }while (0);
        do {
          char __[32UL];
          snprintf(__,(sizeof(__)),"%s%s","\033",((cur_term -> type.Strings[79] != 0)?cur_term -> type.Strings[79] : ""));
          do {
            snprintf(key,(sizeof(key)),"%s%s%s",controls[c],alts[a],"left");
            do {
              g_hash_table_insert(specials,(g_strdup(key)),(g_strdup(__)));
              gnt_keys_add_combination(__);
            }while (0);
          }while (0);
        }while (0);
        do {
          char __[32UL];
          snprintf(__,(sizeof(__)),"%s%s","\033",((cur_term -> type.Strings[83] != 0)?cur_term -> type.Strings[83] : ""));
          do {
            snprintf(key,(sizeof(key)),"%s%s%s",controls[c],alts[a],"right");
            do {
              g_hash_table_insert(specials,(g_strdup(key)),(g_strdup(__)));
              gnt_keys_add_combination(__);
            }while (0);
          }while (0);
        }while (0);
      }
    }
  }
  c = 0;
  for (a = 0; alts[a] != 0; a++) {
/* Upper-case alphabets */
    for (ch = 0; ch < 26; ch++) {
      char str[2UL] = {((65 + ch)), (0)};
      char code[] = {(27), ((65 + ch)), (0)};
      do {
        snprintf(key,(sizeof(key)),"%s%s%s",controls[c],alts[a],str);
        do {
          g_hash_table_insert(specials,(g_strdup(key)),(g_strdup(code)));
          gnt_keys_add_combination(code);
        }while (0);
      }while (0);
    }
/* Digits */
    for (ch = 0; ch < 10; ch++) {
      char str[2UL] = {((48 + ch)), (0)};
      char code[] = {(27), ((48 + ch)), (0)};
      do {
        snprintf(key,(sizeof(key)),"%s%s%s",controls[c],alts[a],str);
        do {
          g_hash_table_insert(specials,(g_strdup(key)),(g_strdup(code)));
          gnt_keys_add_combination(code);
        }while (0);
      }while (0);
    }
  }
}

void gnt_keys_refine(char *text)
{
  while((( *text) == 27) && (text[1] == 27))
    text++;
  if (((( *text) == 27) && (text[1] == '[')) && ((text[2] >= 65) && (text[2] <= 68))) {
/* Apparently this is necessary for urxvt and screen and xterm */
    if (((((strstr(term,"screen")) == term) || (strcmp(term,"rxvt-unicode") == 0)) || ((strstr(term,"xterm")) == term)) || ((strstr(term,"vt100")) == term)) 
       *(text + 1) = 79;
  }
  else if (g_utf8_get_char(text) == 195) {
    if ((text[2] == 0) && ((strstr(term,"xterm")) == term)) {
       *text = 27;
/* Say wha? */
       *(text + 1) -= 64;
    }
  }
}

const char *gnt_key_translate(const char *name)
{
  return ((name != 0)?g_hash_table_lookup(specials,name) : ((void *)((void *)0)));
}
typedef struct __unnamed_class___F0_L182_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__name__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__key {
const char *name;
const char *key;}gntkey;

static void get_key_name(gpointer key,gpointer value,gpointer data)
{
  gntkey *k = data;
  if ((k -> name) != 0) 
    return ;
  if (g_utf8_collate(value,(k -> key)) == 0) 
    k -> name = key;
}

const char *gnt_key_lookup(const char *key)
{
  gntkey k = {((const char *)((void *)0)), (key)};
  g_hash_table_foreach(specials,get_key_name,(&k));
  return k.name;
}
/**
 * The key-bindings will be saved in a tree. When a keystroke happens, GNT will
 * find the sequence that matches a binding and return the length.
 * A sequence should not be a prefix of another sequence. If it is, then only
 * the shortest one will be processed. If we want to change that, we will need
 * to allow getting the k-th prefix that matches the input, and pay attention
 * to the return value of gnt_wm_process_input in gntmain.c.
 */
#define SIZE 256
#define IS_END         1 << 0

struct _node 
{
  struct _node *next[256UL];
  int ref;
  int flags;
}
;
static struct _node root = {(1), (0)};

static void add_path(struct _node *node,const char *path)
{
  struct _node *n = (struct _node *)((void *)0);
  if (!(path != 0) || !(( *path) != 0)) {
    node -> flags |= 1 << 0;
    return ;
  }
  while((( *path) != 0) && ((node -> next)[(unsigned char )( *path)] != 0)){
    node = (node -> next)[(unsigned char )( *path)];
    node -> ref++;
    path++;
  }
  if (!(( *path) != 0)) 
    return ;
  n = ((struct _node *)(g_malloc0_n(1,(sizeof(struct _node )))));
  n -> ref = 1;
  (node -> next)[(unsigned char )( *(path++))] = n;
  add_path(n,path);
}

void gnt_keys_add_combination(const char *path)
{
  add_path(&root,path);
}

static void del_path(struct _node *node,const char *path)
{
  struct _node *next = (struct _node *)((void *)0);
  if (!(( *path) != 0)) 
    return ;
  next = (node -> next)[(unsigned char )( *path)];
  if (!(next != 0)) 
    return ;
  del_path(next,(path + 1));
  next -> ref--;
  if ((next -> ref) == 0) {
    (node -> next)[(unsigned char )( *path)] = ((struct _node *)((void *)0));
    g_free(next);
  }
}

void gnt_keys_del_combination(const char *path)
{
  del_path(&root,path);
}

int gnt_keys_find_combination(const char *path)
{
  int depth = 0;
  struct _node *n = &root;
  root.flags &= ~1 << 0;
  while(((( *path) != 0) && ((n -> next)[(unsigned char )( *path)] != 0)) && !(((n -> flags) & 1 << 0) != 0)){
    if ((!((g_ascii_table[(guchar )( *path)] & G_ASCII_SPACE) != 0) && !((g_ascii_table[(guchar )( *path)] & G_ASCII_CNTRL) != 0)) && !((g_ascii_table[(guchar )( *path)] & G_ASCII_GRAPH) != 0)) 
      return 0;
    n = (n -> next)[(unsigned char )( *(path++))];
    depth++;
  }
  if (!(((n -> flags) & 1 << 0) != 0)) 
    depth = 0;
  return depth;
}

static void print_path(struct _node *node,int depth)
{
  int i;
  for (i = 0; i < 256; i++) {
    if ((node -> next)[i] != 0) {
      g_printerr("%*c (%d:%d)\n",(depth * 4),i,( *(node -> next)[i]).ref,( *(node -> next)[i]).flags);
      print_path((node -> next)[i],(depth + 1));
    }
  }
}
/* this is purely for debugging purposes. */
void gnt_keys_print_combinations();

void gnt_keys_print_combinations()
{
  g_printerr("--------\n");
  print_path(&root,1);
  g_printerr("--------\n");
}
