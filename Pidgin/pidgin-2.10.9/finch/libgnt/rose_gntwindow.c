/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "gntinternal.h"
#include "gntstyle.h"
#include "gntwindow.h"
#include <string.h>

struct _GntWindowPriv 
{
/* key => menuitem-id */
  GHashTable *accels;
  GntWindowFlags flags;
}
;
enum __unnamed_enum___F0_L35_C1_SIG_WORKSPACE_HIDE__COMMA__SIG_WORKSPACE_SHOW__COMMA__SIGS {SIG_WORKSPACE_HIDE,SIG_WORKSPACE_SHOW,SIGS};
static guint signals[2UL] = {(0)};
static GntBoxClass *parent_class = (GntBoxClass *)((void *)0);
static void (*org_destroy)(GntWidget *);

static gboolean show_menu(GntBindable *bind,GList *null)
{
  GntWindow *win = (GntWindow *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_window_get_gtype()));
  if ((win -> menu) != 0) {
    GntMenu *menu = (win -> menu);
    gnt_screen_menu_show(menu);
    if ((menu -> type) == GNT_MENU_TOPLEVEL) {
      GntMenuItem *item;
      item = (g_list_nth_data((menu -> list),(menu -> selected)));
      if ((item != 0) && (gnt_menuitem_get_submenu(item) != 0)) {
        gnt_widget_activate(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_widget_get_gtype()))));
      }
    }
    return 1;
  }
  return 0;
}

static void gnt_window_destroy(GntWidget *widget)
{
  GntWindow *window = (GntWindow *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_window_get_gtype()));
  if ((window -> menu) != 0) 
    gnt_widget_destroy(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(window -> menu)),gnt_widget_get_gtype()))));
  if ((window -> priv) != 0) {
    if (( *(window -> priv)).accels != 0) 
      g_hash_table_destroy(( *(window -> priv)).accels);
    g_free((window -> priv));
  }
  ( *org_destroy)(widget);
}

static void gnt_window_class_init(GntWindowClass *klass)
{
  GntBindableClass *bindable = (GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()));
  GntWidgetClass *wid_class = (GntWidgetClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_widget_get_gtype()));
  parent_class = ((GntBoxClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_box_get_gtype())));
  org_destroy = (wid_class -> destroy);
  wid_class -> destroy = gnt_window_destroy;
  signals[SIG_WORKSPACE_HIDE] = g_signal_new("workspace-hidden",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,0,0,0,g_cclosure_marshal_VOID__VOID,((GType )(1 << 2)),0);
  signals[SIG_WORKSPACE_SHOW] = g_signal_new("workspace-shown",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,0,0,0,g_cclosure_marshal_VOID__VOID,((GType )(1 << 2)),0);
  gnt_bindable_class_register_action(bindable,"show-menu",show_menu,"\017",((void *)((void *)0)));
  gnt_bindable_register_binding(bindable,"show-menu",((cur_term -> type.Strings[67] != 0)?cur_term -> type.Strings[67] : ""),((void *)((void *)0)));
  gnt_style_read_actions(( *((GTypeClass *)klass)).g_type,bindable);;
}

static void gnt_window_init(GTypeInstance *instance,gpointer class)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_widget_get_gtype()));
  GntWindow *win = (GntWindow *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_window_get_gtype()));
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags &= (~(GNT_WIDGET_NO_BORDER | GNT_WIDGET_NO_SHADOW));
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_CAN_TAKE_FOCUS;
  win -> priv = ((GntWindowPriv *)(g_malloc0_n(1,(sizeof(GntWindowPriv )))));
  ( *(win -> priv)).accels = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);;
}
/******************************************************************************
 * GntWindow API
 *****************************************************************************/

GType gnt_window_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(GntWindowClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gnt_window_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GntWindow ))), (0), (gnt_window_init), ((const GTypeValueTable *)((void *)0))
/* base_init		*/
/* base_finalize	*/
/* class_finalize	*/
/* class_data		*/
/* n_preallocs		*/
/* instance_init	*/
/* value_table		*/
};
    type = g_type_register_static(gnt_box_get_gtype(),"GntWindow",&info,0);
  }
  return type;
}

GntWidget *gnt_window_new()
{
  GntWidget *widget = (g_object_new(gnt_window_get_gtype(),0));
  return widget;
}

GntWidget *gnt_window_box_new(gboolean homo,gboolean vert)
{
  GntWidget *wid = gnt_window_new();
  GntBox *box = (GntBox *)(g_type_check_instance_cast(((GTypeInstance *)wid),gnt_box_get_gtype()));
  box -> homogeneous = homo;
  box -> vertical = vert;
  box -> alignment = (((vert != 0)?GNT_ALIGN_LEFT : GNT_ALIGN_MID));
  return wid;
}

void gnt_window_workspace_hiding(GntWindow *window)
{
  if ((window -> menu) != 0) 
    gnt_widget_hide(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(window -> menu)),gnt_widget_get_gtype()))));
  g_signal_emit(window,signals[SIG_WORKSPACE_HIDE],0);
}

void gnt_window_workspace_showing(GntWindow *window)
{
  g_signal_emit(window,signals[SIG_WORKSPACE_SHOW],0);
}

void gnt_window_set_menu(GntWindow *window,GntMenu *menu)
{
/* If a menu already existed, then destroy that first. */
  const char *name = gnt_widget_get_name(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_widget_get_gtype()))));
  if ((window -> menu) != 0) 
    gnt_widget_destroy(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(window -> menu)),gnt_widget_get_gtype()))));
  window -> menu = menu;
  if ((name != 0) && ((window -> priv) != 0)) {
    if (!(gnt_style_read_menu_accels(name,( *(window -> priv)).accels) != 0)) {
      g_hash_table_destroy(( *(window -> priv)).accels);
      ( *(window -> priv)).accels = ((GHashTable *)((void *)0));
    }
  }
}

const char *gnt_window_get_accel_item(GntWindow *window,const char *key)
{
  if (( *(window -> priv)).accels != 0) 
    return (g_hash_table_lookup(( *(window -> priv)).accels,key));
  return 0;
}

void gnt_window_set_maximize(GntWindow *window,GntWindowFlags maximize)
{
  if ((maximize & GNT_WINDOW_MAXIMIZE_X) != 0U) 
    ( *(window -> priv)).flags |= GNT_WINDOW_MAXIMIZE_X;
  else 
    ( *(window -> priv)).flags &= (~GNT_WINDOW_MAXIMIZE_X);
  if ((maximize & GNT_WINDOW_MAXIMIZE_Y) != 0U) 
    ( *(window -> priv)).flags |= GNT_WINDOW_MAXIMIZE_Y;
  else 
    ( *(window -> priv)).flags &= (~GNT_WINDOW_MAXIMIZE_Y);
}

GntWindowFlags gnt_window_get_maximize(GntWindow *window)
{
  return (( *(window -> priv)).flags & (GNT_WINDOW_MAXIMIZE_X | GNT_WINDOW_MAXIMIZE_Y));
}
