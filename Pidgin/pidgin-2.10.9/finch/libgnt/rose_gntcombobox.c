/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "gntinternal.h"
#include "gntbox.h"
#include "gntcombobox.h"
#include "gnttree.h"
#include "gntmarshal.h"
#include "gntstyle.h"
#include "gntutils.h"
#include <string.h>
enum __unnamed_enum___F0_L33_C1_SIG_SELECTION_CHANGED__COMMA__SIGS {SIG_SELECTION_CHANGED,SIGS};
static GntWidgetClass *parent_class = (GntWidgetClass *)((void *)0);
static guint signals[1UL] = {(0)};
static void (*widget_lost_focus)(GntWidget *);

static void set_selection(GntComboBox *box,gpointer key)
{
  if ((box -> selected) != key) {
/* XXX: make sure the key actually does exist */
    gpointer old = (box -> selected);
    box -> selected = key;
    if (( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype())))).window != 0) 
      gnt_widget_draw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype()))));
    if ((box -> dropdown) != 0) 
      gnt_tree_set_selected(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(box -> dropdown)),gnt_tree_get_gtype()))),key);
    g_signal_emit(box,signals[SIG_SELECTION_CHANGED],0,old,key);
  }
}

static void hide_popup(GntComboBox *box,gboolean set)
{
  gnt_widget_set_size((box -> dropdown),(( *(box -> dropdown)).priv.width - 1),( *(box -> dropdown)).priv.height);
  if (set != 0) 
    set_selection(box,gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(box -> dropdown)),gnt_tree_get_gtype())))));
  else 
    gnt_tree_set_selected(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(box -> dropdown)),gnt_tree_get_gtype()))),(box -> selected));
  gnt_widget_hide(( *(box -> dropdown)).parent);
}

static void gnt_combo_box_draw(GntWidget *widget)
{
  GntComboBox *box = (GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_combo_box_get_gtype()));
  char *text = (char *)((void *)0);
  char *s;
  GntColorType type;
  int len;
  if (((box -> dropdown) != 0) && ((box -> selected) != 0)) 
    text = gnt_tree_get_selection_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(box -> dropdown)),gnt_tree_get_gtype()))));
  if (text == ((char *)((void *)0))) 
    text = g_strdup("");
  if (gnt_widget_has_focus(widget) != 0) 
    type = GNT_COLOR_HIGHLIGHT;
  else 
    type = GNT_COLOR_NORMAL;
  wbkgdset((widget -> window),(0 | gnt_color_pair(type)));
  s = ((char *)(gnt_util_onscreen_width_to_pointer(text,(widget -> priv.width - 4),&len)));
   *s = 0;
  (wmove((widget -> window),1,1) == -1)?-1 : waddnstr((widget -> window),C_(text),-1);
  whline((widget -> window),(32 | gnt_color_pair(type)),((widget -> priv.width - 4) - len));
  (wmove((widget -> window),1,(widget -> priv.width - 3)) == -1)?-1 : waddch((widget -> window),(acs_map[(unsigned char )'x'] | (gnt_color_pair(GNT_COLOR_NORMAL))));
  (wmove((widget -> window),1,(widget -> priv.width - 2)) == -1)?-1 : waddch((widget -> window),(acs_map[(unsigned char )'.'] | (gnt_color_pair(GNT_COLOR_NORMAL))));
  wmove((widget -> window),1,1);
  g_free(text);;
}

static void gnt_combo_box_size_request(GntWidget *widget)
{
  if (!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U)) {
    GntWidget *dd = ( *((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_combo_box_get_gtype())))).dropdown;
    gnt_widget_size_request(dd);
/* For now, a combobox will have border */
    widget -> priv.height = 3;
    widget -> priv.width = ((10 > (dd -> priv.width + 2))?10 : (dd -> priv.width + 2));
  }
}

static void gnt_combo_box_map(GntWidget *widget)
{
  if ((widget -> priv.width == 0) || (widget -> priv.height == 0)) 
    gnt_widget_size_request(widget);;
}

static void popup_dropdown(GntComboBox *box)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype()));
  GntWidget *parent = ( *(box -> dropdown)).parent;
  int height = (g_list_length(( *((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(box -> dropdown)),gnt_tree_get_gtype())))).list));
  int y = ((widget -> priv.y + widget -> priv.height) - 1);
  gnt_widget_set_size((box -> dropdown),widget -> priv.width,(height + 2));
  if (((y + height) + 2) >= (((stdscr != 0)?((stdscr -> _maxy) + 1) : -1))) 
    y = ((widget -> priv.y - height) - 1);
  gnt_widget_set_position(parent,widget -> priv.x,y);
  if ((parent -> window) != 0) {
    mvwin((parent -> window),y,widget -> priv.x);
    wresize((parent -> window),(height + 2),widget -> priv.width);
  }
  parent -> priv.width = widget -> priv.width;
  parent -> priv.height = (height + 2);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)parent),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_INVISIBLE);
  gnt_widget_draw(parent);
}

static gboolean gnt_combo_box_key_pressed(GntWidget *widget,const char *text)
{
  GntComboBox *box = (GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_combo_box_get_gtype()));
  gboolean showing = !(!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)( *(box -> dropdown)).parent),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U));
  if (showing != 0) {
    if (text[1] == 0) {
      switch(text[0]){
        case '\t':
{
        }
        case '\n':
{
        }
        case '\r':
{
          hide_popup(box,1);
          return 1;
        }
        case 27:
{
          hide_popup(box,0);
          return 1;
        }
      }
    }
  }
  if (gnt_widget_key_pressed((box -> dropdown),text) != 0) {
    if (!(showing != 0)) 
      popup_dropdown(box);
    return 1;
  }
{
#define SEARCH_IN_RANGE(start, end) do { \
		GntTreeRow *row; \
		for (row = start; row != end; \
				row = gnt_tree_row_get_next(tree, row)) { \
			gpointer key = gnt_tree_row_get_key(tree, row); \
			GList *list = gnt_tree_get_row_text_list(tree, key); \
			gboolean found = FALSE; \
			found = (list->data && g_ascii_strncasecmp(text, list->data, len) == 0); \
			g_list_foreach(list, (GFunc)g_free, NULL); \
			g_list_free(list); \
			if (found) { \
				if (!showing) \
					popup_dropdown(box); \
				gnt_tree_set_selected(tree, key); \
				return TRUE; \
			} \
		} \
} while (0)
    int len = (strlen(text));
    GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(box -> dropdown)),gnt_tree_get_gtype()));
    GntTreeRow *current = (tree -> current);
    do {
      GntTreeRow *row;
      for (row = gnt_tree_row_get_next(tree,current); row != ((GntTreeRow *)((void *)0)); row = gnt_tree_row_get_next(tree,row)) {
        gpointer key = gnt_tree_row_get_key(tree,row);
        GList *list = gnt_tree_get_row_text_list(tree,key);
        gboolean found = 0;
        found = (((list -> data) != 0) && (g_ascii_strncasecmp(text,(list -> data),len) == 0));
        g_list_foreach(list,((GFunc )g_free),0);
        g_list_free(list);
        if (found != 0) {
          if (!(showing != 0)) 
            popup_dropdown(box);
          gnt_tree_set_selected(tree,key);
          return 1;
        }
      }
    }while (0);
    do {
      GntTreeRow *row;
      for (row = (tree -> top); row != current; row = gnt_tree_row_get_next(tree,row)) {
        gpointer key = gnt_tree_row_get_key(tree,row);
        GList *list = gnt_tree_get_row_text_list(tree,key);
        gboolean found = 0;
        found = (((list -> data) != 0) && (g_ascii_strncasecmp(text,(list -> data),len) == 0));
        g_list_foreach(list,((GFunc )g_free),0);
        g_list_free(list);
        if (found != 0) {
          if (!(showing != 0)) 
            popup_dropdown(box);
          gnt_tree_set_selected(tree,key);
          return 1;
        }
      }
    }while (0);
#undef SEARCH_IN_RANGE
  }
  return 0;
}

static void gnt_combo_box_destroy(GntWidget *widget)
{
  gnt_widget_destroy(( *( *((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_combo_box_get_gtype())))).dropdown).parent);
}

static void gnt_combo_box_lost_focus(GntWidget *widget)
{
  GntComboBox *combo = (GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_combo_box_get_gtype()));
  if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)( *(combo -> dropdown)).parent),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U) 
    hide_popup(combo,0);
  ( *widget_lost_focus)(widget);
}

static gboolean gnt_combo_box_clicked(GntWidget *widget,GntMouseEvent event,int x,int y)
{
  GntComboBox *box = (GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_combo_box_get_gtype()));
  gboolean dshowing = (( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)( *(box -> dropdown)).parent),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED);
  if (event == GNT_MOUSE_SCROLL_UP) {
    if (dshowing != 0) 
      gnt_widget_key_pressed((box -> dropdown),((cur_term -> type.Strings[87] != 0)?cur_term -> type.Strings[87] : ""));
  }
  else if (event == GNT_MOUSE_SCROLL_DOWN) {
    if (dshowing != 0) 
      gnt_widget_key_pressed((box -> dropdown),((cur_term -> type.Strings[61] != 0)?cur_term -> type.Strings[61] : ""));
  }
  else if (event == GNT_LEFT_MOUSE_DOWN) {
    if (dshowing != 0) {
      hide_popup(box,1);
    }
    else {
      popup_dropdown(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_combo_box_get_gtype()))));
    }
  }
  else 
    return 0;
  return 1;
}

static void gnt_combo_box_size_changed(GntWidget *widget,int oldw,int oldh)
{
  GntComboBox *box = (GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_combo_box_get_gtype()));
  gnt_widget_set_size((box -> dropdown),(widget -> priv.width - 1),( *(box -> dropdown)).priv.height);
}

static gboolean dropdown_menu(GntBindable *b,GList *null)
{
  if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)( *( *((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)b),gnt_combo_box_get_gtype())))).dropdown).parent),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U) 
    return 0;
  popup_dropdown(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)b),gnt_combo_box_get_gtype()))));
  return 1;
}

static void gnt_combo_box_class_init(GntComboBoxClass *klass)
{
  GntBindableClass *bindable = (GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()));
  parent_class = ((GntWidgetClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_widget_get_gtype())));
  parent_class -> destroy = gnt_combo_box_destroy;
  parent_class -> draw = gnt_combo_box_draw;
  parent_class -> map = gnt_combo_box_map;
  parent_class -> size_request = gnt_combo_box_size_request;
  parent_class -> key_pressed = gnt_combo_box_key_pressed;
  parent_class -> clicked = gnt_combo_box_clicked;
  parent_class -> size_changed = gnt_combo_box_size_changed;
  widget_lost_focus = (parent_class -> lost_focus);
  parent_class -> lost_focus = gnt_combo_box_lost_focus;
  signals[SIG_SELECTION_CHANGED] = g_signal_new("selection-changed",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,0,0,0,gnt_closure_marshal_VOID__POINTER_POINTER,((GType )(1 << 2)),2,((GType )(17 << 2)),((GType )(17 << 2)));
  gnt_bindable_class_register_action(bindable,"dropdown",dropdown_menu,((cur_term -> type.Strings[61] != 0)?cur_term -> type.Strings[61] : ""),((void *)((void *)0)));
  gnt_bindable_register_binding(bindable,"dropdown",((cur_term -> type.Strings[87] != 0)?cur_term -> type.Strings[87] : ""),((void *)((void *)0)));
  gnt_style_read_actions(( *((GTypeClass *)klass)).g_type,bindable);;
}

static void gnt_combo_box_init(GTypeInstance *instance,gpointer class)
{
  GntWidget *box;
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_widget_get_gtype()));
  GntComboBox *combo = (GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_combo_box_get_gtype()));
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags |= (GNT_WIDGET_GROW_X | GNT_WIDGET_CAN_TAKE_FOCUS | GNT_WIDGET_NO_SHADOW);
  combo -> dropdown = gnt_tree_new();
  box = gnt_box_new(0,0);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype())))).priv.flags |= (GNT_WIDGET_NO_SHADOW | GNT_WIDGET_NO_BORDER | GNT_WIDGET_TRANSIENT);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),(combo -> dropdown));
  widget -> priv.minw = 4;
  widget -> priv.minh = 3;;
}
/******************************************************************************
 * GntComboBox API
 *****************************************************************************/

GType gnt_combo_box_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(GntComboBoxClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gnt_combo_box_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GntComboBox ))), (0), (gnt_combo_box_init), ((const GTypeValueTable *)((void *)0))
/* base_init		*/
/* base_finalize	*/
/* class_finalize	*/
/* class_data		*/
/* n_preallocs		*/
/* instance_init	*/
/* value_table		*/
};
    type = g_type_register_static(gnt_widget_get_gtype(),"GntComboBox",&info,0);
  }
  return type;
}

GntWidget *gnt_combo_box_new()
{
  GntWidget *widget = (g_object_new(gnt_combo_box_get_gtype(),0));
  return widget;
}

void gnt_combo_box_add_data(GntComboBox *box,gpointer key,const char *text)
{
  gnt_tree_add_row_last(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(box -> dropdown)),gnt_tree_get_gtype()))),key,gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(box -> dropdown)),gnt_tree_get_gtype()))),text),0);
  if ((box -> selected) == ((void *)((void *)0))) 
    set_selection(box,key);
}

gpointer gnt_combo_box_get_selected_data(GntComboBox *box)
{
  return box -> selected;
}

void gnt_combo_box_set_selected(GntComboBox *box,gpointer key)
{
  set_selection(box,key);
}

void gnt_combo_box_remove(GntComboBox *box,gpointer key)
{
  gnt_tree_remove(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(box -> dropdown)),gnt_tree_get_gtype()))),key);
  if ((box -> selected) == key) 
    set_selection(box,0);
}

void gnt_combo_box_remove_all(GntComboBox *box)
{
  gnt_tree_remove_all(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(box -> dropdown)),gnt_tree_get_gtype()))));
  set_selection(box,0);
}
