/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "gntinternal.h"
#include "gntmenu.h"
#include "gntmenuitemcheck.h"
#include <ctype.h>
#include <string.h>
enum __unnamed_enum___F0_L30_C1_SIGS {SIGS=1};
enum __unnamed_enum___F0_L35_C1_ITEM_TEXT__COMMA__ITEM_TRIGGER__COMMA__ITEM_SUBMENU__COMMA__NUM_COLUMNS {ITEM_TEXT,ITEM_TRIGGER,ITEM_SUBMENU,NUM_COLUMNS};
static GntTreeClass *parent_class = (GntTreeClass *)((void *)0);
static void (*org_draw)(GntWidget *);
static void (*org_destroy)(GntWidget *);
static void (*org_map)(GntWidget *);
static void (*org_size_request)(GntWidget *);
static gboolean (*org_key_pressed)(GntWidget *, const char *);
static gboolean (*org_clicked)(GntWidget *, GntMouseEvent , int , int );
static void menuitem_activate(GntMenu *menu,GntMenuItem *item);

static void menu_hide_all(GntMenu *menu)
{
  while((menu -> parentmenu) != 0)
    menu = (menu -> parentmenu);
  gnt_widget_hide(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_widget_get_gtype()))));
}

static void show_submenu(GntMenu *menu)
{
  GntMenuItem *item;
  if ((menu -> type) != GNT_MENU_TOPLEVEL) 
    return ;
  item = (g_list_nth_data((menu -> list),(menu -> selected)));
  if (!(item != 0) || !((item -> submenu) != 0)) 
    return ;
  menuitem_activate(menu,item);
}

static void gnt_menu_draw(GntWidget *widget)
{
  GntMenu *menu = (GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_menu_get_gtype()));
  GList *iter;
  chtype type;
  int i;
  if ((menu -> type) == GNT_MENU_TOPLEVEL) {
    wbkgdset((widget -> window),(0 | gnt_color_pair(GNT_COLOR_HIGHLIGHT)));
    werase((widget -> window));
    for (((i = 0) , (iter = (menu -> list))); iter != 0; ((iter = (iter -> next)) , i++)) {
      GntMenuItem *item = (GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)(iter -> data)),gnt_menuitem_get_gtype()));
      type = (32 | gnt_color_pair(GNT_COLOR_HIGHLIGHT));
      if (i == (menu -> selected)) 
        type |= (((chtype )1UL) << 10 + 8);
      item -> priv.x = (((((widget -> window) != 0)?( *(widget -> window))._curx : -1)) + widget -> priv.x);
      item -> priv.y = ((((((widget -> window) != 0)?( *(widget -> window))._cury : -1)) + widget -> priv.y) + 1);
      wbkgdset((widget -> window),type);
      wprintw((widget -> window)," %s   ",C_((item -> text)));
    }
  }
  else {
    ( *org_draw)(widget);
  };
}

static void gnt_menu_size_request(GntWidget *widget)
{
  GntMenu *menu = (GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_menu_get_gtype()));
  if ((menu -> type) == GNT_MENU_TOPLEVEL) {
    widget -> priv.height = 1;
    widget -> priv.width = ((stdscr != 0)?((stdscr -> _maxx) + 1) : -1);
  }
  else {
    ( *org_size_request)(widget);
    widget -> priv.height = (g_list_length((menu -> list)) + 2);
  }
}

static void menu_tree_add(GntMenu *menu,GntMenuItem *item,GntMenuItem *parent)
{
  char trigger[4UL] = "\000 )";
  if (((trigger[1] = gnt_menuitem_get_trigger(item)) != 0) && (trigger[1] != 32)) 
    trigger[0] = 40;
  if ((({
    GTypeInstance *__inst = (GTypeInstance *)item;
    GType __t = gnt_menuitem_check_get_gtype();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = 1;
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
{
    gnt_tree_add_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_tree_get_gtype()))),item,gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_tree_get_gtype()))),(item -> text),trigger," "),parent,0);
    gnt_tree_set_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_tree_get_gtype()))),item,gnt_menuitem_check_get_checked(((GntMenuItemCheck *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_check_get_gtype())))));
  }
  else 
    gnt_tree_add_row_last(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_tree_get_gtype()))),item,gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_tree_get_gtype()))),(item -> text),trigger,(((item -> submenu) != 0)?">" : " ")),parent);
  if (0 && ((item -> submenu) != 0)) {
    GntMenu *sub = (GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)(item -> submenu)),gnt_menu_get_gtype()));
    GList *iter;
    for (iter = (sub -> list); iter != 0; iter = (iter -> next)) {
      GntMenuItem *it = (GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)(iter -> data)),gnt_menuitem_get_gtype()));
      menu_tree_add(menu,it,item);
    }
  }
}
#define GET_VAL(ch)  ((ch >= '0' && ch <= '9') ? (ch - '0') : (ch >= 'a' && ch <= 'z') ? (10 + ch - 'a') : 36)

static void assign_triggers(GntMenu *menu)
{
  GList *iter;
  gboolean bools[37UL];
  memset(bools,0,(sizeof(bools)));
  bools[36] = 1;
  for (iter = (menu -> list); iter != 0; iter = (iter -> next)) {{
      GntMenuItem *item = (iter -> data);
      char trigger = (tolower((gnt_menuitem_get_trigger(item))));
      if ((trigger == 0) || (trigger == 32)) 
        continue; 
      bools[(((trigger >= 48) && (trigger <= '9'))?(trigger - 48) : ((((trigger >= 'a') && (trigger <= 'z'))?((10 + trigger) - 'a') : 36)))] = 1;
    }
  }
  for (iter = (menu -> list); iter != 0; iter = (iter -> next)) {{
      GntMenuItem *item = (iter -> data);
      char trigger = gnt_menuitem_get_trigger(item);
      const char *text = (item -> text);
      if (trigger != 0) 
        continue; 
{
        while(( *text) != 0){{
            char ch = (tolower(( *(text++))));
            char t[2UL] = {(ch), (0)};
            if (((ch == 32) || (bools[(((ch >= 48) && (ch <= '9'))?(ch - 48) : ((((ch >= 'a') && (ch <= 'z'))?((10 + ch) - 'a') : 36)))] != 0)) || (gnt_bindable_check_key(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_bindable_get_gtype()))),t) != 0)) 
              continue; 
            trigger = ch;
            break; 
          }
        }
      }
      if (trigger == 0) 
        trigger = (item -> text)[0];
      gnt_menuitem_set_trigger(item,trigger);
      bools[(((trigger >= 48) && (trigger <= '9'))?(trigger - 48) : ((((trigger >= 'a') && (trigger <= 'z'))?((10 + trigger) - 'a') : 36)))] = 1;
    }
  }
}

static void gnt_menu_map(GntWidget *widget)
{
  GntMenu *menu = (GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_menu_get_gtype()));
  if ((menu -> type) == GNT_MENU_TOPLEVEL) {
    gnt_widget_size_request(widget);
  }
  else {
/* Populate the tree */
    GList *iter;
    gnt_tree_remove_all(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()))));
/* Try to assign some trigger for the items */
    assign_triggers(menu);
    for (iter = (menu -> list); iter != 0; iter = (iter -> next)) {
      GntMenuItem *item = (GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)(iter -> data)),gnt_menuitem_get_gtype()));
      menu_tree_add(menu,item,0);
    }
    ( *org_map)(widget);
    gnt_tree_adjust_columns(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()))));
  };
}

static void menuitem_activate(GntMenu *menu,GntMenuItem *item)
{
  if (!(item != 0)) 
    return ;
  if (gnt_menuitem_activate(item) != 0) {
    menu_hide_all(menu);
  }
  else {
    if ((item -> submenu) != 0) {
      GntMenu *sub = (GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)(item -> submenu)),gnt_menu_get_gtype()));
      menu -> submenu = sub;
/* Submenus are *never* toplevel */
      sub -> type = GNT_MENU_POPUP;
      sub -> parentmenu = menu;
      if ((menu -> type) != GNT_MENU_TOPLEVEL) {
        GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_widget_get_gtype()));
        item -> priv.x = ((widget -> priv.x + widget -> priv.width) - 1);
        item -> priv.y = (widget -> priv.y + gnt_tree_get_selection_visible_line(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_tree_get_gtype())))));
      }
      gnt_widget_set_position(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_widget_get_gtype()))),item -> priv.x,item -> priv.y);
      ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_INVISIBLE);
      gnt_widget_draw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_widget_get_gtype()))));
    }
    else {
      menu_hide_all(menu);
    }
  }
}

static GList *find_item_with_trigger(GList *start,GList *end,char trigger)
{
  GList *iter;
  for (iter = start; iter != (((end != 0)?end : ((struct _GList *)((void *)0)))); iter = (iter -> next)) {
    if ((gnt_menuitem_get_trigger((iter -> data))) == trigger) 
      return iter;
  }
  return 0;
}

static gboolean check_for_trigger(GntMenu *menu,char trigger)
{
/* check for a trigger key */
  GList *iter;
  GList *find;
  GList *nth = g_list_find((menu -> list),(gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_tree_get_gtype()))))));
  if (nth == ((GList *)((void *)0))) 
    return 0;
  find = find_item_with_trigger((nth -> next),0,trigger);
  if (!(find != 0)) 
    find = find_item_with_trigger((menu -> list),(nth -> next),trigger);
  if (!(find != 0)) 
    return 0;
  if (find != nth) {
    gnt_tree_set_selected(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_tree_get_gtype()))),(find -> data));
    iter = find_item_with_trigger((find -> next),0,trigger);
    if ((iter != ((GList *)((void *)0))) && (iter != find)) 
      return 1;
    iter = find_item_with_trigger((menu -> list),nth,trigger);
    if ((iter != ((GList *)((void *)0))) && (iter != find)) 
      return 1;
  }
  gnt_widget_activate(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_widget_get_gtype()))));
  return 1;
}

static gboolean gnt_menu_key_pressed(GntWidget *widget,const char *text)
{
  GntMenu *menu = (GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_menu_get_gtype()));
  int current = (menu -> selected);
  if ((menu -> submenu) != 0) {
    GntMenu *sub = menu;
    do 
      sub = (sub -> submenu);while ((sub -> submenu) != 0);
    if (gnt_widget_key_pressed(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_widget_get_gtype()))),text) != 0) 
      return 1;
    if ((menu -> type) != GNT_MENU_TOPLEVEL) 
      return 0;
  }
  if (((text[0] == 27) && (text[1] == 0)) || (((menu -> type) != GNT_MENU_TOPLEVEL) && (strcmp(text,(((cur_term -> type.Strings[79] != 0)?cur_term -> type.Strings[79] : ""))) == 0))) {
/* Escape closes menu */
    GntMenu *par = (menu -> parentmenu);
    if (par != ((GntMenu *)((void *)0))) {
      par -> submenu = ((GntMenu *)((void *)0));
      gnt_widget_hide(widget);
    }
    else 
      gnt_widget_hide(widget);
    if ((par != 0) && ((par -> type) == GNT_MENU_TOPLEVEL)) 
      gnt_menu_key_pressed(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)par),gnt_widget_get_gtype()))),text);
    return 1;
  }
  if ((menu -> type) == GNT_MENU_TOPLEVEL) {
    if (strcmp(text,(((cur_term -> type.Strings[79] != 0)?cur_term -> type.Strings[79] : ""))) == 0) {
      menu -> selected--;
      if ((menu -> selected) < 0) 
        menu -> selected = (g_list_length((menu -> list)) - 1);
    }
    else if (strcmp(text,(((cur_term -> type.Strings[83] != 0)?cur_term -> type.Strings[83] : ""))) == 0) {
      menu -> selected++;
      if ((menu -> selected) >= g_list_length((menu -> list))) 
        menu -> selected = 0;
    }
    else if ((strcmp(text,cur_term -> type.Strings[2]) == 0) || (strcmp(text,(((cur_term -> type.Strings[61] != 0)?cur_term -> type.Strings[61] : ""))) == 0)) {
      gnt_widget_activate(widget);
    }
    if (current != (menu -> selected)) {
      GntMenu *sub = (menu -> submenu);
      if (sub != 0) 
        gnt_widget_hide(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_widget_get_gtype()))));
      show_submenu(menu);
      gnt_widget_draw(widget);
      return 1;
    }
  }
  else {
    if (text[1] == 0) {
      if (check_for_trigger(menu,text[0]) != 0) 
        return 1;
    }
    else if (strcmp(text,(((cur_term -> type.Strings[83] != 0)?cur_term -> type.Strings[83] : ""))) == 0) {
      GntMenuItem *item = (gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_tree_get_gtype())))));
      if ((item != 0) && ((item -> submenu) != 0)) {
        menuitem_activate(menu,item);
        return 1;
      }
    }
    if (gnt_bindable_perform_action_key(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_bindable_get_gtype()))),text) != 0) 
      return 1;
    return ( *org_key_pressed)(widget,text);
  }
  return gnt_bindable_perform_action_key(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_bindable_get_gtype()))),text);
}

static void gnt_menu_destroy(GntWidget *widget)
{
  GntMenu *menu = (GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_menu_get_gtype()));
  g_list_foreach((menu -> list),((GFunc )g_object_unref),0);
  g_list_free((menu -> list));
  ( *org_destroy)(widget);
}

static void gnt_menu_toggled(GntTree *tree,gpointer key)
{
  GntMenuItem *item = (GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)key),gnt_menuitem_get_gtype()));
  GntMenu *menu = (GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_menu_get_gtype()));
  gboolean check = gnt_menuitem_check_get_checked(((GntMenuItemCheck *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_check_get_gtype()))));
  gnt_menuitem_check_set_checked(((GntMenuItemCheck *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_check_get_gtype()))),!(check != 0));
  gnt_menuitem_activate(item);
  while(menu != 0){
    gnt_widget_hide(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_widget_get_gtype()))));
    menu = (menu -> parentmenu);
  }
}

static void gnt_menu_activate(GntWidget *widget)
{
  GntMenu *menu = (GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_menu_get_gtype()));
  GntMenuItem *item;
  if ((menu -> type) == GNT_MENU_TOPLEVEL) {
    item = (g_list_nth_data((menu -> list),(menu -> selected)));
  }
  else {
    item = (gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_tree_get_gtype())))));
  }
  if (item != 0) {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)item;
      GType __t = gnt_menuitem_check_get_gtype();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = 1;
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
      gnt_menu_toggled(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()))),item);
    else 
      menuitem_activate(menu,item);
  }
}

static void gnt_menu_hide(GntWidget *widget)
{
  GntMenu *sub;
  GntMenu *menu = (GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_menu_get_gtype()));
  while((sub = (menu -> submenu)) != 0)
    gnt_widget_hide(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_widget_get_gtype()))));
  if ((menu -> parentmenu) != 0) 
    ( *(menu -> parentmenu)).submenu = ((GntMenu *)((void *)0));
}

static gboolean gnt_menu_clicked(GntWidget *widget,GntMouseEvent event,int x,int y)
{
  if (( *((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_menu_get_gtype())))).type != GNT_MENU_POPUP) 
    return 0;
  if ((org_clicked != 0) && (( *org_clicked)(widget,event,x,y) != 0)) 
    return 1;
  gnt_widget_activate(widget);
  return 1;
}

static void gnt_menu_class_init(GntMenuClass *klass)
{
  GntWidgetClass *wid_class = (GntWidgetClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_widget_get_gtype()));
  parent_class = ((GntTreeClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_tree_get_gtype())));
  org_destroy = (wid_class -> destroy);
  org_map = (wid_class -> map);
  org_draw = (wid_class -> draw);
  org_key_pressed = (wid_class -> key_pressed);
  org_size_request = (wid_class -> size_request);
  org_clicked = (wid_class -> clicked);
  wid_class -> destroy = gnt_menu_destroy;
  wid_class -> draw = gnt_menu_draw;
  wid_class -> map = gnt_menu_map;
  wid_class -> size_request = gnt_menu_size_request;
  wid_class -> key_pressed = gnt_menu_key_pressed;
  wid_class -> activate = gnt_menu_activate;
  wid_class -> hide = gnt_menu_hide;
  wid_class -> clicked = gnt_menu_clicked;
  parent_class -> toggled = gnt_menu_toggled;;
}

static void gnt_menu_init(GTypeInstance *instance,gpointer class)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_widget_get_gtype()));
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= (GNT_WIDGET_NO_SHADOW | GNT_WIDGET_NO_BORDER | GNT_WIDGET_CAN_TAKE_FOCUS | GNT_WIDGET_TRANSIENT | GNT_WIDGET_DISABLE_ACTIONS);;
}
/******************************************************************************
 * GntMenu API
 *****************************************************************************/

GType gnt_menu_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(GntMenuClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gnt_menu_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GntMenu ))), (0), (gnt_menu_init), ((const GTypeValueTable *)((void *)0))
/* base_init		*/
/* base_finalize	*/
/* class_finalize	*/
/* class_data		*/
/* n_preallocs		*/
/* instance_init	*/
/* value_table		*/
};
    type = g_type_register_static(gnt_tree_get_gtype(),"GntMenu",&info,0);
  }
  return type;
}

GntWidget *gnt_menu_new(GntMenuType type)
{
  GntWidget *widget = (g_object_new(gnt_menu_get_gtype(),0));
  GntMenu *menu = (GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_menu_get_gtype()));
  menu -> list = ((GList *)((void *)0));
  menu -> selected = 0;
  menu -> type = type;
  if (type == GNT_MENU_TOPLEVEL) {
    widget -> priv.x = 0;
    widget -> priv.y = 0;
  }
  else {
    ( *((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype())))).show_separator = 0;
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"columns",NUM_COLUMNS,((void *)((void *)0)));
    gnt_tree_set_col_width(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()))),ITEM_TRIGGER,3);
    gnt_tree_set_column_resizable(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()))),ITEM_TRIGGER,0);
    gnt_tree_set_col_width(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()))),ITEM_SUBMENU,1);
    gnt_tree_set_column_resizable(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()))),ITEM_SUBMENU,0);
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_NO_BORDER);
  }
  return widget;
}

void gnt_menu_add_item(GntMenu *menu,GntMenuItem *item)
{
  menu -> list = g_list_append((menu -> list),item);
}

GntMenuItem *gnt_menu_get_item(GntMenu *menu,const char *id)
{
  GntMenuItem *item = (GntMenuItem *)((void *)0);
  GList *iter = (menu -> list);
  if (!(id != 0) || !(( *id) != 0)) 
    return 0;
{
    for (; iter != 0; iter = (iter -> next)) {
      GntMenu *sub;
      item = (iter -> data);
      sub = gnt_menuitem_get_submenu(item);
      if (sub != 0) {
        item = gnt_menu_get_item(sub,id);
        if (item != 0) 
          break; 
      }
      else {
        const char *itid = gnt_menuitem_get_id(item);
        if ((itid != 0) && (strcmp(itid,id) == 0)) 
          break; 
/* XXX: Perhaps look at the menu-label as well? */
      }
      item = ((GntMenuItem *)((void *)0));
    }
  }
  return item;
}
