#include "gntmarshal.h"
#include	<glib-object.h>
#ifdef G_ENABLE_DEBUG
#define g_marshal_value_peek_boolean(v)  g_value_get_boolean (v)
#define g_marshal_value_peek_char(v)     g_value_get_schar (v)
#define g_marshal_value_peek_uchar(v)    g_value_get_uchar (v)
#define g_marshal_value_peek_int(v)      g_value_get_int (v)
#define g_marshal_value_peek_uint(v)     g_value_get_uint (v)
#define g_marshal_value_peek_long(v)     g_value_get_long (v)
#define g_marshal_value_peek_ulong(v)    g_value_get_ulong (v)
#define g_marshal_value_peek_int64(v)    g_value_get_int64 (v)
#define g_marshal_value_peek_uint64(v)   g_value_get_uint64 (v)
#define g_marshal_value_peek_enum(v)     g_value_get_enum (v)
#define g_marshal_value_peek_flags(v)    g_value_get_flags (v)
#define g_marshal_value_peek_float(v)    g_value_get_float (v)
#define g_marshal_value_peek_double(v)   g_value_get_double (v)
#define g_marshal_value_peek_string(v)   (char*) g_value_get_string (v)
#define g_marshal_value_peek_param(v)    g_value_get_param (v)
#define g_marshal_value_peek_boxed(v)    g_value_get_boxed (v)
#define g_marshal_value_peek_pointer(v)  g_value_get_pointer (v)
#define g_marshal_value_peek_object(v)   g_value_get_object (v)
#define g_marshal_value_peek_variant(v)  g_value_get_variant (v)
#else /* !G_ENABLE_DEBUG */
/* WARNING: This code accesses GValues directly, which is UNSUPPORTED API.
 *          Do not access GValues directly in your code. Instead, use the
 *          g_value_get_*() functions
 */
#define g_marshal_value_peek_boolean(v)  (v)->data[0].v_int
#define g_marshal_value_peek_char(v)     (v)->data[0].v_int
#define g_marshal_value_peek_uchar(v)    (v)->data[0].v_uint
#define g_marshal_value_peek_int(v)      (v)->data[0].v_int
#define g_marshal_value_peek_uint(v)     (v)->data[0].v_uint
#define g_marshal_value_peek_long(v)     (v)->data[0].v_long
#define g_marshal_value_peek_ulong(v)    (v)->data[0].v_ulong
#define g_marshal_value_peek_int64(v)    (v)->data[0].v_int64
#define g_marshal_value_peek_uint64(v)   (v)->data[0].v_uint64
#define g_marshal_value_peek_enum(v)     (v)->data[0].v_long
#define g_marshal_value_peek_flags(v)    (v)->data[0].v_ulong
#define g_marshal_value_peek_float(v)    (v)->data[0].v_float
#define g_marshal_value_peek_double(v)   (v)->data[0].v_double
#define g_marshal_value_peek_string(v)   (v)->data[0].v_pointer
#define g_marshal_value_peek_param(v)    (v)->data[0].v_pointer
#define g_marshal_value_peek_boxed(v)    (v)->data[0].v_pointer
#define g_marshal_value_peek_pointer(v)  (v)->data[0].v_pointer
#define g_marshal_value_peek_object(v)   (v)->data[0].v_pointer
#define g_marshal_value_peek_variant(v)  (v)->data[0].v_pointer
#endif /* !G_ENABLE_DEBUG */
/* BOOLEAN:VOID (./genmarshal:1) */

void gnt_closure_marshal_BOOLEAN__VOID(GClosure *closure,GValue *return_value,guint n_param_values,const GValue *param_values,gpointer invocation_hint,gpointer marshal_data)
{
  typedef gboolean (*GMarshalFunc_BOOLEAN__VOID)(gpointer , gpointer );
  register GMarshalFunc_BOOLEAN__VOID callback;
  register GCClosure *cc = (GCClosure *)closure;
  register gpointer data1;
  register gpointer data2;
  gboolean v_return;
  do {
    if (return_value != ((GValue *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"return_value != NULL");
      return ;
    };
  }while (0);
  do {
    if (n_param_values == 1) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"n_param_values == 1");
      return ;
    };
  }while (0);
  if (( *((GClosure *)closure)).derivative_flag != 0) {
    data1 = (closure -> data);
    data2 = g_value_peek_pointer((param_values + 0));
  }
  else {
    data1 = g_value_peek_pointer((param_values + 0));
    data2 = (closure -> data);
  }
  callback = ((GMarshalFunc_BOOLEAN__VOID )(((marshal_data != 0)?marshal_data : (cc -> callback))));
  v_return = ( *callback)(data1,data2);
  g_value_set_boolean(return_value,v_return);
}
/* BOOLEAN:STRING (./genmarshal:2) */

void gnt_closure_marshal_BOOLEAN__STRING(GClosure *closure,GValue *return_value,guint n_param_values,const GValue *param_values,gpointer invocation_hint,gpointer marshal_data)
{
  typedef gboolean (*GMarshalFunc_BOOLEAN__STRING)(gpointer , gpointer , gpointer );
  register GMarshalFunc_BOOLEAN__STRING callback;
  register GCClosure *cc = (GCClosure *)closure;
  register gpointer data1;
  register gpointer data2;
  gboolean v_return;
  do {
    if (return_value != ((GValue *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"return_value != NULL");
      return ;
    };
  }while (0);
  do {
    if (n_param_values == 2) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"n_param_values == 2");
      return ;
    };
  }while (0);
  if (( *((GClosure *)closure)).derivative_flag != 0) {
    data1 = (closure -> data);
    data2 = g_value_peek_pointer((param_values + 0));
  }
  else {
    data1 = g_value_peek_pointer((param_values + 0));
    data2 = (closure -> data);
  }
  callback = ((GMarshalFunc_BOOLEAN__STRING )(((marshal_data != 0)?marshal_data : (cc -> callback))));
  v_return = ( *callback)(data1,( *(param_values + 1)).data[0].v_pointer,data2);
  g_value_set_boolean(return_value,v_return);
}
/* VOID:INT,INT,INT,INT (./genmarshal:3) */

void gnt_closure_marshal_VOID__INT_INT_INT_INT(GClosure *closure,GValue *return_value,guint n_param_values,const GValue *param_values,gpointer invocation_hint,gpointer marshal_data)
{
  typedef void (*GMarshalFunc_VOID__INT_INT_INT_INT)(gpointer , gint , gint , gint , gint , gpointer );
  register GMarshalFunc_VOID__INT_INT_INT_INT callback;
  register GCClosure *cc = (GCClosure *)closure;
  register gpointer data1;
  register gpointer data2;
  do {
    if (n_param_values == 5) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"n_param_values == 5");
      return ;
    };
  }while (0);
  if (( *((GClosure *)closure)).derivative_flag != 0) {
    data1 = (closure -> data);
    data2 = g_value_peek_pointer((param_values + 0));
  }
  else {
    data1 = g_value_peek_pointer((param_values + 0));
    data2 = (closure -> data);
  }
  callback = ((GMarshalFunc_VOID__INT_INT_INT_INT )(((marshal_data != 0)?marshal_data : (cc -> callback))));
  ( *callback)(data1,( *(param_values + 1)).data[0].v_int,( *(param_values + 2)).data[0].v_int,( *(param_values + 3)).data[0].v_int,( *(param_values + 4)).data[0].v_int,data2);
}
/* VOID:INT,INT (./genmarshal:4) */

void gnt_closure_marshal_VOID__INT_INT(GClosure *closure,GValue *return_value,guint n_param_values,const GValue *param_values,gpointer invocation_hint,gpointer marshal_data)
{
  typedef void (*GMarshalFunc_VOID__INT_INT)(gpointer , gint , gint , gpointer );
  register GMarshalFunc_VOID__INT_INT callback;
  register GCClosure *cc = (GCClosure *)closure;
  register gpointer data1;
  register gpointer data2;
  do {
    if (n_param_values == 3) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"n_param_values == 3");
      return ;
    };
  }while (0);
  if (( *((GClosure *)closure)).derivative_flag != 0) {
    data1 = (closure -> data);
    data2 = g_value_peek_pointer((param_values + 0));
  }
  else {
    data1 = g_value_peek_pointer((param_values + 0));
    data2 = (closure -> data);
  }
  callback = ((GMarshalFunc_VOID__INT_INT )(((marshal_data != 0)?marshal_data : (cc -> callback))));
  ( *callback)(data1,( *(param_values + 1)).data[0].v_int,( *(param_values + 2)).data[0].v_int,data2);
}
/* VOID:POINTER,POINTER (./genmarshal:5) */

void gnt_closure_marshal_VOID__POINTER_POINTER(GClosure *closure,GValue *return_value,guint n_param_values,const GValue *param_values,gpointer invocation_hint,gpointer marshal_data)
{
  typedef void (*GMarshalFunc_VOID__POINTER_POINTER)(gpointer , gpointer , gpointer , gpointer );
  register GMarshalFunc_VOID__POINTER_POINTER callback;
  register GCClosure *cc = (GCClosure *)closure;
  register gpointer data1;
  register gpointer data2;
  do {
    if (n_param_values == 3) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"n_param_values == 3");
      return ;
    };
  }while (0);
  if (( *((GClosure *)closure)).derivative_flag != 0) {
    data1 = (closure -> data);
    data2 = g_value_peek_pointer((param_values + 0));
  }
  else {
    data1 = g_value_peek_pointer((param_values + 0));
    data2 = (closure -> data);
  }
  callback = ((GMarshalFunc_VOID__POINTER_POINTER )(((marshal_data != 0)?marshal_data : (cc -> callback))));
  ( *callback)(data1,( *(param_values + 1)).data[0].v_pointer,( *(param_values + 2)).data[0].v_pointer,data2);
}
/* BOOLEAN:INT,INT (./genmarshal:6) */

void gnt_closure_marshal_BOOLEAN__INT_INT(GClosure *closure,GValue *return_value,guint n_param_values,const GValue *param_values,gpointer invocation_hint,gpointer marshal_data)
{
  typedef gboolean (*GMarshalFunc_BOOLEAN__INT_INT)(gpointer , gint , gint , gpointer );
  register GMarshalFunc_BOOLEAN__INT_INT callback;
  register GCClosure *cc = (GCClosure *)closure;
  register gpointer data1;
  register gpointer data2;
  gboolean v_return;
  do {
    if (return_value != ((GValue *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"return_value != NULL");
      return ;
    };
  }while (0);
  do {
    if (n_param_values == 3) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"n_param_values == 3");
      return ;
    };
  }while (0);
  if (( *((GClosure *)closure)).derivative_flag != 0) {
    data1 = (closure -> data);
    data2 = g_value_peek_pointer((param_values + 0));
  }
  else {
    data1 = g_value_peek_pointer((param_values + 0));
    data2 = (closure -> data);
  }
  callback = ((GMarshalFunc_BOOLEAN__INT_INT )(((marshal_data != 0)?marshal_data : (cc -> callback))));
  v_return = ( *callback)(data1,( *(param_values + 1)).data[0].v_int,( *(param_values + 2)).data[0].v_int,data2);
  g_value_set_boolean(return_value,v_return);
}
/* BOOLEAN:INT,INT,INT (./genmarshal:7) */

void gnt_closure_marshal_BOOLEAN__INT_INT_INT(GClosure *closure,GValue *return_value,guint n_param_values,const GValue *param_values,gpointer invocation_hint,gpointer marshal_data)
{
  typedef gboolean (*GMarshalFunc_BOOLEAN__INT_INT_INT)(gpointer , gint , gint , gint , gpointer );
  register GMarshalFunc_BOOLEAN__INT_INT_INT callback;
  register GCClosure *cc = (GCClosure *)closure;
  register gpointer data1;
  register gpointer data2;
  gboolean v_return;
  do {
    if (return_value != ((GValue *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"return_value != NULL");
      return ;
    };
  }while (0);
  do {
    if (n_param_values == 4) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"n_param_values == 4");
      return ;
    };
  }while (0);
  if (( *((GClosure *)closure)).derivative_flag != 0) {
    data1 = (closure -> data);
    data2 = g_value_peek_pointer((param_values + 0));
  }
  else {
    data1 = g_value_peek_pointer((param_values + 0));
    data2 = (closure -> data);
  }
  callback = ((GMarshalFunc_BOOLEAN__INT_INT_INT )(((marshal_data != 0)?marshal_data : (cc -> callback))));
  v_return = ( *callback)(data1,( *(param_values + 1)).data[0].v_int,( *(param_values + 2)).data[0].v_int,( *(param_values + 3)).data[0].v_int,data2);
  g_value_set_boolean(return_value,v_return);
}
/* BOOLEAN:POINTER,POINTER,POINTER (./genmarshal:8) */

void gnt_closure_marshal_BOOLEAN__POINTER_POINTER_POINTER(GClosure *closure,GValue *return_value,guint n_param_values,const GValue *param_values,gpointer invocation_hint,gpointer marshal_data)
{
  typedef gboolean (*GMarshalFunc_BOOLEAN__POINTER_POINTER_POINTER)(gpointer , gpointer , gpointer , gpointer , gpointer );
  register GMarshalFunc_BOOLEAN__POINTER_POINTER_POINTER callback;
  register GCClosure *cc = (GCClosure *)closure;
  register gpointer data1;
  register gpointer data2;
  gboolean v_return;
  do {
    if (return_value != ((GValue *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"return_value != NULL");
      return ;
    };
  }while (0);
  do {
    if (n_param_values == 4) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"n_param_values == 4");
      return ;
    };
  }while (0);
  if (( *((GClosure *)closure)).derivative_flag != 0) {
    data1 = (closure -> data);
    data2 = g_value_peek_pointer((param_values + 0));
  }
  else {
    data1 = g_value_peek_pointer((param_values + 0));
    data2 = (closure -> data);
  }
  callback = ((GMarshalFunc_BOOLEAN__POINTER_POINTER_POINTER )(((marshal_data != 0)?marshal_data : (cc -> callback))));
  v_return = ( *callback)(data1,( *(param_values + 1)).data[0].v_pointer,( *(param_values + 2)).data[0].v_pointer,( *(param_values + 3)).data[0].v_pointer,data2);
  g_value_set_boolean(return_value,v_return);
}
/* BOOLEAN:INT,INT,INT,POINTER (./genmarshal:9) */

void gnt_closure_marshal_BOOLEAN__INT_INT_INT_POINTER(GClosure *closure,GValue *return_value,guint n_param_values,const GValue *param_values,gpointer invocation_hint,gpointer marshal_data)
{
  typedef gboolean (*GMarshalFunc_BOOLEAN__INT_INT_INT_POINTER)(gpointer , gint , gint , gint , gpointer , gpointer );
  register GMarshalFunc_BOOLEAN__INT_INT_INT_POINTER callback;
  register GCClosure *cc = (GCClosure *)closure;
  register gpointer data1;
  register gpointer data2;
  gboolean v_return;
  do {
    if (return_value != ((GValue *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"return_value != NULL");
      return ;
    };
  }while (0);
  do {
    if (n_param_values == 5) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"n_param_values == 5");
      return ;
    };
  }while (0);
  if (( *((GClosure *)closure)).derivative_flag != 0) {
    data1 = (closure -> data);
    data2 = g_value_peek_pointer((param_values + 0));
  }
  else {
    data1 = g_value_peek_pointer((param_values + 0));
    data2 = (closure -> data);
  }
  callback = ((GMarshalFunc_BOOLEAN__INT_INT_INT_POINTER )(((marshal_data != 0)?marshal_data : (cc -> callback))));
  v_return = ( *callback)(data1,( *(param_values + 1)).data[0].v_int,( *(param_values + 2)).data[0].v_int,( *(param_values + 3)).data[0].v_int,( *(param_values + 4)).data[0].v_pointer,data2);
  g_value_set_boolean(return_value,v_return);
}
/* VOID:STRING,STRING (./genmarshal:10) */

void gnt_closure_marshal_VOID__STRING_STRING(GClosure *closure,GValue *return_value,guint n_param_values,const GValue *param_values,gpointer invocation_hint,gpointer marshal_data)
{
  typedef void (*GMarshalFunc_VOID__STRING_STRING)(gpointer , gpointer , gpointer , gpointer );
  register GMarshalFunc_VOID__STRING_STRING callback;
  register GCClosure *cc = (GCClosure *)closure;
  register gpointer data1;
  register gpointer data2;
  do {
    if (n_param_values == 3) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"n_param_values == 3");
      return ;
    };
  }while (0);
  if (( *((GClosure *)closure)).derivative_flag != 0) {
    data1 = (closure -> data);
    data2 = g_value_peek_pointer((param_values + 0));
  }
  else {
    data1 = g_value_peek_pointer((param_values + 0));
    data2 = (closure -> data);
  }
  callback = ((GMarshalFunc_VOID__STRING_STRING )(((marshal_data != 0)?marshal_data : (cc -> callback))));
  ( *callback)(data1,( *(param_values + 1)).data[0].v_pointer,( *(param_values + 2)).data[0].v_pointer,data2);
}
/* VOID:POINTER,BOOLEAN (./genmarshal:11) */

void gnt_closure_marshal_VOID__POINTER_BOOLEAN(GClosure *closure,GValue *return_value,guint n_param_values,const GValue *param_values,gpointer invocation_hint,gpointer marshal_data)
{
  typedef void (*GMarshalFunc_VOID__POINTER_BOOLEAN)(gpointer , gpointer , gboolean , gpointer );
  register GMarshalFunc_VOID__POINTER_BOOLEAN callback;
  register GCClosure *cc = (GCClosure *)closure;
  register gpointer data1;
  register gpointer data2;
  do {
    if (n_param_values == 3) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"n_param_values == 3");
      return ;
    };
  }while (0);
  if (( *((GClosure *)closure)).derivative_flag != 0) {
    data1 = (closure -> data);
    data2 = g_value_peek_pointer((param_values + 0));
  }
  else {
    data1 = g_value_peek_pointer((param_values + 0));
    data2 = (closure -> data);
  }
  callback = ((GMarshalFunc_VOID__POINTER_BOOLEAN )(((marshal_data != 0)?marshal_data : (cc -> callback))));
  ( *callback)(data1,( *(param_values + 1)).data[0].v_pointer,( *(param_values + 2)).data[0].v_int,data2);
}
