/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "gntinternal.h"
#include "gntcheckbox.h"
enum __unnamed_enum___F0_L26_C1_SIG_TOGGLED__COMMA__SIGS {SIG_TOGGLED=1,SIGS};
static GntButtonClass *parent_class = (GntButtonClass *)((void *)0);
static guint signals[2UL] = {(0)};

static void gnt_check_box_draw(GntWidget *widget)
{
  GntCheckBox *cb = (GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_check_box_get_gtype()));
  GntColorType type;
  gboolean focus = gnt_widget_has_focus(widget);
  if (focus != 0) 
    type = GNT_COLOR_HIGHLIGHT;
  else 
    type = GNT_COLOR_NORMAL;
  wbkgdset((widget -> window),(0 | gnt_color_pair(type)));
  (wmove((widget -> window),0,0) == -1)?-1 : waddch((widget -> window),'[');
  (wmove((widget -> window),0,1) == -1)?-1 : waddch((widget -> window),(((((cb -> checked) != 0)?'X' : 32)) | (((focus != 0)?(((chtype )1UL) << 9 + 8) : 1UL - 1UL))));
  (wmove((widget -> window),0,2) == -1)?-1 : waddch((widget -> window),']');
  wbkgdset((widget -> window),(0 | gnt_color_pair(GNT_COLOR_NORMAL)));
  (wmove((widget -> window),0,4) == -1)?-1 : waddnstr((widget -> window),C_(( *( *((GntButton *)(g_type_check_instance_cast(((GTypeInstance *)cb),gnt_button_get_gtype())))).priv).text),-1);
  wmove((widget -> window),0,1);;
}

static void toggle_selection(GntWidget *widget)
{
  ( *((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_check_box_get_gtype())))).checked = !(( *((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_check_box_get_gtype())))).checked != 0);
  g_signal_emit(widget,signals[SIG_TOGGLED],0);
  gnt_widget_draw(widget);
}

static gboolean gnt_check_box_key_pressed(GntWidget *widget,const char *text)
{
  if ((text[0] == 32) && (text[1] == 0)) {
    toggle_selection(widget);
    return 1;
  }
  return 0;
}

static gboolean gnt_check_box_clicked(GntWidget *widget,GntMouseEvent event,int x,int y)
{
  if (event == GNT_LEFT_MOUSE_DOWN) {
    toggle_selection(widget);
    return 1;
  }
  return 0;
}

static void gnt_check_box_class_init(GntCheckBoxClass *klass)
{
  GntWidgetClass *wclass = (GntWidgetClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_widget_get_gtype()));
  parent_class = ((GntButtonClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_button_get_gtype())));
/*parent_class->destroy = gnt_check_box_destroy;*/
  wclass -> draw = gnt_check_box_draw;
/*parent_class->map = gnt_check_box_map;*/
/*parent_class->size_request = gnt_check_box_size_request;*/
  wclass -> key_pressed = gnt_check_box_key_pressed;
  wclass -> clicked = gnt_check_box_clicked;
  signals[SIG_TOGGLED] = g_signal_new("toggled",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntCheckBoxClass *)((GntCheckBoxClass *)0))).toggled))),0,0,g_cclosure_marshal_VOID__VOID,((GType )(1 << 2)),0);;
}

static void gnt_check_box_init(GTypeInstance *instance,gpointer class)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_widget_get_gtype()));
  widget -> priv.minh = 1;
  widget -> priv.minw = 4;
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= (GNT_WIDGET_NO_BORDER | GNT_WIDGET_NO_SHADOW);;
}
/******************************************************************************
 * GntCheckBox API
 *****************************************************************************/

GType gnt_check_box_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(GntCheckBoxClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gnt_check_box_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GntCheckBox ))), (0), (gnt_check_box_init), ((const GTypeValueTable *)((void *)0))
/* base_init		*/
/* base_finalize	*/
/* class_finalize	*/
/* class_data		*/
/* n_preallocs		*/
/* instance_init	*/
/* value_table		*/
};
    type = g_type_register_static(gnt_button_get_gtype(),"GntCheckBox",&info,0);
  }
  return type;
}

GntWidget *gnt_check_box_new(const char *text)
{
  GntWidget *widget = (g_object_new(gnt_check_box_get_gtype(),0));
  ( *( *((GntButton *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_button_get_gtype())))).priv).text = g_strdup(text);
  gnt_widget_set_take_focus(widget,1);
  return widget;
}

void gnt_check_box_set_checked(GntCheckBox *box,gboolean set)
{
  if (set != (box -> checked)) {
    box -> checked = set;
    g_signal_emit(box,signals[SIG_TOGGLED],0);
  }
}

gboolean gnt_check_box_get_checked(GntCheckBox *box)
{
  return box -> checked;
}
