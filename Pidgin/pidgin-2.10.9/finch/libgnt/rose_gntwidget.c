/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
/* Stuff brutally ripped from Gflib */
#include "gntinternal.h"
#include "gntwidget.h"
#include "gntstyle.h"
#include "gntmarshal.h"
#include "gntutils.h"
#include "gnt.h"
enum __unnamed_enum___F0_L32_C1_SIG_DESTROY__COMMA__SIG_DRAW__COMMA__SIG_HIDE__COMMA__SIG_GIVE_FOCUS__COMMA__SIG_LOST_FOCUS__COMMA__SIG_KEY_PRESSED__COMMA__SIG_MAP__COMMA__SIG_ACTIVATE__COMMA__SIG_EXPOSE__COMMA__SIG_SIZE_REQUEST__COMMA__SIG_CONFIRM_SIZE__COMMA__SIG_SIZE_CHANGED__COMMA__SIG_POSITION__COMMA__SIG_CLICKED__COMMA__SIG_CONTEXT_MENU__COMMA__SIGS {SIG_DESTROY,SIG_DRAW,SIG_HIDE,SIG_GIVE_FOCUS,SIG_LOST_FOCUS,SIG_KEY_PRESSED,SIG_MAP,SIG_ACTIVATE,SIG_EXPOSE,SIG_SIZE_REQUEST,SIG_CONFIRM_SIZE,SIG_SIZE_CHANGED,SIG_POSITION,SIG_CLICKED,SIG_CONTEXT_MENU,SIGS};
static GObjectClass *parent_class = (GObjectClass *)((void *)0);
static guint signals[15UL] = {(0)};
static void init_widget(GntWidget *widget);

static void gnt_widget_init(GTypeInstance *instance,gpointer class)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_widget_get_gtype()));
  widget -> priv.name = ((char *)((void *)0));;
}

static void gnt_widget_map(GntWidget *widget)
{
/* Get some default size for the widget */
;
  g_signal_emit(widget,signals[SIG_MAP],0);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_MAPPED;
}

static void gnt_widget_dispose(GObject *obj)
{
  GntWidget *self = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)obj),gnt_widget_get_gtype()));
  g_signal_emit(self,signals[SIG_DESTROY],0);
  ( *(parent_class -> dispose))(obj);;
}

static void gnt_widget_focus_change(GntWidget *widget)
{
  if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U) 
    gnt_widget_draw(widget);
}

static gboolean gnt_widget_dummy_confirm_size(GntWidget *widget,int width,int height)
{
  gboolean shadow;
  if ((width < widget -> priv.minw) || (height < widget -> priv.minh)) 
    return 0;
  shadow = gnt_widget_has_shadow(widget);
  if (((widget -> priv.width + shadow) != width) && !((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_GROW_X) != 0U)) 
    return 0;
  if (((widget -> priv.height + shadow) != height) && !((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_GROW_Y) != 0U)) 
    return 0;
  return 1;
}

static gboolean context_menu(GntBindable *bind,GList *null)
{
  gboolean ret = 0;
  g_signal_emit(bind,signals[SIG_CONTEXT_MENU],0,&ret);
  return ret;
}

static void gnt_widget_class_init(GntWidgetClass *klass)
{
  GObjectClass *obj_class = (GObjectClass *)(g_type_check_class_cast(((GTypeClass *)klass),((GType )(20 << 2))));
  parent_class = (g_type_class_peek_parent(klass));
  obj_class -> dispose = gnt_widget_dispose;
  klass -> destroy = gnt_widget_destroy;
  klass -> show = gnt_widget_show;
  klass -> draw = gnt_widget_draw;
  klass -> expose = gnt_widget_expose;
  klass -> map = gnt_widget_map;
  klass -> lost_focus = gnt_widget_focus_change;
  klass -> gained_focus = gnt_widget_focus_change;
  klass -> confirm_size = gnt_widget_dummy_confirm_size;
  klass -> key_pressed = ((gboolean (*)(GntWidget *, const char *))((void *)0));
  klass -> activate = ((void (*)(GntWidget *))((void *)0));
  klass -> clicked = ((gboolean (*)(GntWidget *, GntMouseEvent , int , int ))((void *)0));
  signals[SIG_DESTROY] = g_signal_new("destroy",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWidgetClass *)((GntWidgetClass *)0))).destroy))),0,0,g_cclosure_marshal_VOID__VOID,((GType )(1 << 2)),0);
  signals[SIG_GIVE_FOCUS] = g_signal_new("gained-focus",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWidgetClass *)((GntWidgetClass *)0))).gained_focus))),0,0,g_cclosure_marshal_VOID__VOID,((GType )(1 << 2)),0);
  signals[SIG_LOST_FOCUS] = g_signal_new("lost-focus",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWidgetClass *)((GntWidgetClass *)0))).lost_focus))),0,0,g_cclosure_marshal_VOID__VOID,((GType )(1 << 2)),0);
  signals[SIG_ACTIVATE] = g_signal_new("activate",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWidgetClass *)((GntWidgetClass *)0))).activate))),0,0,g_cclosure_marshal_VOID__VOID,((GType )(1 << 2)),0);
  signals[SIG_MAP] = g_signal_new("map",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWidgetClass *)((GntWidgetClass *)0))).map))),0,0,g_cclosure_marshal_VOID__VOID,((GType )(1 << 2)),0);
  signals[SIG_DRAW] = g_signal_new("draw",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWidgetClass *)((GntWidgetClass *)0))).draw))),0,0,g_cclosure_marshal_VOID__VOID,((GType )(1 << 2)),0);
  signals[SIG_HIDE] = g_signal_new("hide",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWidgetClass *)((GntWidgetClass *)0))).hide))),0,0,g_cclosure_marshal_VOID__VOID,((GType )(1 << 2)),0);
  signals[SIG_EXPOSE] = g_signal_new("expose",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWidgetClass *)((GntWidgetClass *)0))).expose))),0,0,gnt_closure_marshal_VOID__INT_INT_INT_INT,((GType )(1 << 2)),4,((GType )(6 << 2)),((GType )(6 << 2)),((GType )(6 << 2)),((GType )(6 << 2)));
  signals[SIG_POSITION] = g_signal_new("position-set",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWidgetClass *)((GntWidgetClass *)0))).set_position))),0,0,gnt_closure_marshal_VOID__INT_INT,((GType )(1 << 2)),2,((GType )(6 << 2)),((GType )(6 << 2)));
  signals[SIG_SIZE_REQUEST] = g_signal_new("size_request",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWidgetClass *)((GntWidgetClass *)0))).size_request))),0,0,g_cclosure_marshal_VOID__VOID,((GType )(1 << 2)),0);
  signals[SIG_SIZE_CHANGED] = g_signal_new("size_changed",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWidgetClass *)((GntWidgetClass *)0))).size_changed))),0,0,gnt_closure_marshal_VOID__INT_INT,((GType )(1 << 2)),2,((GType )(6 << 2)),((GType )(6 << 2)));
  signals[SIG_CONFIRM_SIZE] = g_signal_new("confirm_size",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWidgetClass *)((GntWidgetClass *)0))).confirm_size))),0,0,gnt_closure_marshal_BOOLEAN__INT_INT,((GType )(5 << 2)),2,((GType )(6 << 2)),((GType )(6 << 2)));
  signals[SIG_KEY_PRESSED] = g_signal_new("key_pressed",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWidgetClass *)((GntWidgetClass *)0))).key_pressed))),gnt_boolean_handled_accumulator,0,gnt_closure_marshal_BOOLEAN__STRING,((GType )(5 << 2)),1,((GType )(16 << 2)));
  signals[SIG_CLICKED] = g_signal_new("clicked",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntWidgetClass *)((GntWidgetClass *)0))).clicked))),gnt_boolean_handled_accumulator,0,gnt_closure_marshal_BOOLEAN__INT_INT_INT,((GType )(5 << 2)),3,((GType )(6 << 2)),((GType )(6 << 2)),((GType )(6 << 2)));
  signals[SIG_CONTEXT_MENU] = g_signal_new("context-menu",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,0,gnt_boolean_handled_accumulator,0,gnt_closure_marshal_BOOLEAN__VOID,((GType )(5 << 2)),0);
/* This is relevant for all widgets */
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"context-menu",context_menu,((cur_term -> type.Strings[221] != 0)?cur_term -> type.Strings[221] : ""),((void *)((void *)0)));
  gnt_bindable_register_binding(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"context-menu",((cur_term -> type.Strings[216] != 0)?cur_term -> type.Strings[216] : ""),((void *)((void *)0)));
  gnt_bindable_register_binding(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"context-menu","\030",((void *)((void *)0)));
  gnt_style_read_actions(( *((GTypeClass *)klass)).g_type,((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))));;
}
/******************************************************************************
 * GntWidget API
 *****************************************************************************/

GType gnt_widget_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(GntWidgetClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gnt_widget_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GntWidget ))), (0), (gnt_widget_init), ((const GTypeValueTable *)((void *)0))
/* base_init		*/
/* base_finalize	*/
/* class_data		*/
/* n_preallocs		*/
/* instance_init	*/
/* value_table		*/
};
    type = g_type_register_static(gnt_bindable_get_gtype(),"GntWidget",&info,G_TYPE_FLAG_ABSTRACT);
  }
  return type;
}

void gnt_widget_set_take_focus(GntWidget *widget,gboolean can)
{
  if (can != 0) 
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_CAN_TAKE_FOCUS;
  else 
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_CAN_TAKE_FOCUS);
}
/**
 * gnt_widget_destroy:
 * @obj: The #GntWidget instance.
 *
 * Emits the "destroy" signal notifying all reference holders that they
 * should release @obj.
 */

void gnt_widget_destroy(GntWidget *obj)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)obj;
      GType __t = gnt_widget_get_gtype();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = 1;
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning("Gnt",((const char *)__func__),"GNT_IS_WIDGET(obj)");
      return ;
    };
  }while (0);
  if (!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)obj),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_DESTROYING) != 0U)) {
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)obj),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_DESTROYING;
    gnt_widget_hide(obj);
    delwin((obj -> window));
    g_object_run_dispose(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)obj),((GType )(20 << 2))))));
  };
}

void gnt_widget_show(GntWidget *widget)
{
  gnt_widget_draw(widget);
  gnt_screen_occupy(widget);
}

void gnt_widget_draw(GntWidget *widget)
{
/* Draw the widget */
  if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_DRAWING) != 0U) 
    return ;
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_DRAWING;
  if (!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U)) {
    gnt_widget_map(widget);
  }
  if ((widget -> window) == ((WINDOW *)((void *)0))) {
#if 0
/* room for the taskbar */
#else
/* XXX: */
    widget -> window = newpad((widget -> priv.height + 20),(widget -> priv.width + 20));
#endif
    init_widget(widget);
  }
  g_signal_emit(widget,signals[SIG_DRAW],0);
  gnt_widget_queue_update(widget);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_DRAWING);
}

gboolean gnt_widget_key_pressed(GntWidget *widget,const char *keys)
{
  gboolean ret;
  if (!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_CAN_TAKE_FOCUS) != 0U)) 
    return 0;
  if (!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_DISABLE_ACTIONS) != 0U) && (gnt_bindable_perform_action_key(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_bindable_get_gtype()))),keys) != 0)) 
    return 1;
  keys = gnt_bindable_remap_keys(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_bindable_get_gtype()))),keys);
  g_signal_emit(widget,signals[SIG_KEY_PRESSED],0,keys,&ret);
  return ret;
}

gboolean gnt_widget_clicked(GntWidget *widget,GntMouseEvent event,int x,int y)
{
  gboolean ret;
  g_signal_emit(widget,signals[SIG_CLICKED],0,event,x,y,&ret);
  if (!(ret != 0) && (event == GNT_RIGHT_MOUSE_DOWN)) 
    ret = gnt_bindable_perform_action_named(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_bindable_get_gtype()))),"context-menu",((void *)((void *)0)));
  return ret;
}

void gnt_widget_expose(GntWidget *widget,int x,int y,int width,int height)
{
  g_signal_emit(widget,signals[SIG_EXPOSE],0,x,y,width,height);
}

void gnt_widget_hide(GntWidget *widget)
{
  g_signal_emit(widget,signals[SIG_HIDE],0);
  wbkgdset((widget -> window),(0 | gnt_color_pair(GNT_COLOR_NORMAL)));
#if 0
/* XXX: I have no clue why, but this seemed to be necessary. */
#endif
  gnt_screen_release(widget);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_INVISIBLE;
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_MAPPED);
}

void gnt_widget_set_position(GntWidget *wid,int x,int y)
{
  g_signal_emit(wid,signals[SIG_POSITION],0,x,y);
/* XXX: Need to install properties for these and g_object_notify */
  wid -> priv.x = x;
  wid -> priv.y = y;
}

void gnt_widget_get_position(GntWidget *wid,int *x,int *y)
{
  if (x != 0) 
     *x = wid -> priv.x;
  if (y != 0) 
     *y = wid -> priv.y;
}

void gnt_widget_size_request(GntWidget *widget)
{
  g_signal_emit(widget,signals[SIG_SIZE_REQUEST],0);
}

void gnt_widget_get_size(GntWidget *wid,int *width,int *height)
{
  gboolean shadow = 1;
  if (!(gnt_widget_has_shadow(wid) != 0)) 
    shadow = 0;
  if (width != 0) 
     *width = (wid -> priv.width + shadow);
  if (height != 0) 
     *height = (wid -> priv.height + shadow);
}

static void init_widget(GntWidget *widget)
{
  gboolean shadow = 1;
  if (!(gnt_widget_has_shadow(widget) != 0)) 
    shadow = 0;
  wbkgd((widget -> window),(gnt_color_pair(GNT_COLOR_NORMAL)));
  werase((widget -> window));
  if (!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_NO_BORDER) != 0U)) {
/* - This is ugly. */
/* - What's your point? */
    (wmove((widget -> window),0,0) == -1)?-1 : wvline((widget -> window),(acs_map[(unsigned char )'x'] | (gnt_color_pair(GNT_COLOR_NORMAL))),widget -> priv.height);
    (wmove((widget -> window),0,(widget -> priv.width - 1)) == -1)?-1 : wvline((widget -> window),(acs_map[(unsigned char )'x'] | (gnt_color_pair(GNT_COLOR_NORMAL))),widget -> priv.height);
    (wmove((widget -> window),(widget -> priv.height - 1),0) == -1)?-1 : whline((widget -> window),(acs_map[(unsigned char )'q'] | (gnt_color_pair(GNT_COLOR_NORMAL))),widget -> priv.width);
    (wmove((widget -> window),0,0) == -1)?-1 : whline((widget -> window),(acs_map[(unsigned char )'q'] | (gnt_color_pair(GNT_COLOR_NORMAL))),widget -> priv.width);
    (wmove((widget -> window),0,0) == -1)?-1 : waddch((widget -> window),(acs_map[(unsigned char )'l'] | (gnt_color_pair(GNT_COLOR_NORMAL))));
    (wmove((widget -> window),0,(widget -> priv.width - 1)) == -1)?-1 : waddch((widget -> window),(acs_map[(unsigned char )'k'] | (gnt_color_pair(GNT_COLOR_NORMAL))));
    (wmove((widget -> window),(widget -> priv.height - 1),0) == -1)?-1 : waddch((widget -> window),(acs_map[(unsigned char )'m'] | (gnt_color_pair(GNT_COLOR_NORMAL))));
    (wmove((widget -> window),(widget -> priv.height - 1),(widget -> priv.width - 1)) == -1)?-1 : waddch((widget -> window),(acs_map[(unsigned char )'j'] | (gnt_color_pair(GNT_COLOR_NORMAL))));
  }
  if (shadow != 0) {
    wbkgdset((widget -> window),(0 | gnt_color_pair(GNT_COLOR_SHADOW)));
    (wmove((widget -> window),1,widget -> priv.width) == -1)?-1 : wvline((widget -> window),32,widget -> priv.height);
    (wmove((widget -> window),widget -> priv.height,1) == -1)?-1 : whline((widget -> window),32,widget -> priv.width);
  }
}

gboolean gnt_widget_set_size(GntWidget *widget,int width,int height)
{
  gboolean ret = 1;
  if (gnt_widget_has_shadow(widget) != 0) {
    width--;
    height--;
  }
  if (width <= 0) 
    width = widget -> priv.width;
  if (height <= 0) 
    height = widget -> priv.height;
  if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U) {
    ret = gnt_widget_confirm_size(widget,width,height);
  }
  if (ret != 0) {
    gboolean shadow = 1;
    int oldw;
    int oldh;
    if (!(gnt_widget_has_shadow(widget) != 0)) 
      shadow = 0;
    oldw = widget -> priv.width;
    oldh = widget -> priv.height;
    widget -> priv.width = width;
    widget -> priv.height = height;
    if (((width + shadow) >= ((((widget -> window) != 0)?(( *(widget -> window))._maxx + 1) : -1))) || ((height + shadow) >= ((((widget -> window) != 0)?(( *(widget -> window))._maxy + 1) : -1)))) {
      delwin((widget -> window));
      widget -> window = newpad((height + 20),(width + 20));
    }
    g_signal_emit(widget,signals[SIG_SIZE_CHANGED],0,oldw,oldh);
    if ((widget -> window) != 0) {
      init_widget(widget);
    }
    if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U) 
      init_widget(widget);
    else 
      ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_MAPPED;
  }
  return ret;
}

gboolean gnt_widget_set_focus(GntWidget *widget,gboolean set)
{
  if (!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_CAN_TAKE_FOCUS) != 0U)) 
    return 0;
  if ((set != 0) && !((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_HAS_FOCUS) != 0U)) {
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_HAS_FOCUS;
    g_signal_emit(widget,signals[SIG_GIVE_FOCUS],0);
  }
  else if (!(set != 0) && ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_HAS_FOCUS) != 0U)) {
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_HAS_FOCUS);
    g_signal_emit(widget,signals[SIG_LOST_FOCUS],0);
  }
  else 
    return 0;
  return 1;
}

void gnt_widget_set_name(GntWidget *widget,const char *name)
{
  g_free(widget -> priv.name);
  widget -> priv.name = g_strdup(name);
}

const char *gnt_widget_get_name(GntWidget *widget)
{
  return widget -> priv.name;
}

void gnt_widget_activate(GntWidget *widget)
{
  g_signal_emit(widget,signals[SIG_ACTIVATE],0);
}

static gboolean update_queue_callback(gpointer data)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)data),gnt_widget_get_gtype()));
  if (!(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"gnt:queue_update") != 0)) 
    return 0;
  if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U) 
    gnt_screen_update(widget);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"gnt:queue_update",0);
  return 0;
}

void gnt_widget_queue_update(GntWidget *widget)
{
  if ((widget -> window) == ((WINDOW *)((void *)0))) 
    return ;
  while((widget -> parent) != 0)
    widget = (widget -> parent);
  if (!(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"gnt:queue_update") != 0)) {
    int id = (g_timeout_add(0,update_queue_callback,widget));
    g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"gnt:queue_update",((gpointer )((glong )id)),((GDestroyNotify )g_source_remove));
  }
}

gboolean gnt_widget_confirm_size(GntWidget *widget,int width,int height)
{
  gboolean ret = 0;
  g_signal_emit(widget,signals[SIG_CONFIRM_SIZE],0,width,height,&ret);
  return ret;
}

void gnt_widget_set_visible(GntWidget *widget,gboolean set)
{
  if (set != 0) 
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_INVISIBLE);
  else 
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_INVISIBLE;
}

gboolean gnt_widget_has_shadow(GntWidget *widget)
{
  return !((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_NO_SHADOW) != 0U) && (gnt_style_get_bool(GNT_STYLE_SHADOW,0) != 0);
}
