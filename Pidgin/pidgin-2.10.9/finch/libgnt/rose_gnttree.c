/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "gntinternal.h"
#include "gntmarshal.h"
#include "gntstyle.h"
#include "gnttree.h"
#include "gntutils.h"
#include <string.h>
#include <ctype.h>
#define SEARCH_TIMEOUT_S 4   /* 4 secs */
#define SEARCHING(tree)  (tree->priv->search && tree->priv->search->len > 0)
#define COLUMN_INVISIBLE(tree, index)  (tree->columns[index].flags & GNT_TREE_COLUMN_INVISIBLE)
#define BINARY_DATA(tree, index)       (tree->columns[index].flags & GNT_TREE_COLUMN_BINARY_DATA)
#define RIGHT_ALIGNED(tree, index)       (tree->columns[index].flags & GNT_TREE_COLUMN_RIGHT_ALIGNED)
enum __unnamed_enum___F0_L39_C1_PROP_0__COMMA__PROP_COLUMNS__COMMA__PROP_EXPANDER {PROP_0,PROP_COLUMNS,PROP_EXPANDER};
enum __unnamed_enum___F0_L46_C1_SIG_SELECTION_CHANGED__COMMA__SIG_SCROLLED__COMMA__SIG_TOGGLED__COMMA__SIG_COLLAPSED__COMMA__SIGS {SIG_SELECTION_CHANGED,SIG_SCROLLED,SIG_TOGGLED,SIG_COLLAPSED,SIGS};

struct _GntTreePriv 
{
  GString *search;
  int search_timeout;
  int search_column;
  gboolean (*search_func)(GntTree *, gpointer , const char *, const char *);
  GCompareFunc compare;
  int lastvisible;
  int expander_level;
}
;
#define	TAB_SIZE 3
/* XXX: Make this one into a GObject?
 * 		 ... Probably not */

struct _GntTreeRow 
{
  void *key;
/* XXX: unused */
  void *data;
  gboolean collapsed;
/* Is this a choice-box?
	                               If choice is true, then child will be NULL */
  gboolean choice;
  gboolean isselected;
  GntTextFormatFlags flags;
  int color;
  GntTreeRow *parent;
  GntTreeRow *child;
  GntTreeRow *next;
  GntTreeRow *prev;
  GList *columns;
  GntTree *tree;
}
;

struct _GntTreeCol 
{
  char *text;
  gboolean isbinary;
/* How many columns does it span? */
  int span;
}
;
static void tree_selection_changed(GntTree *,GntTreeRow *,GntTreeRow *);
static void _gnt_tree_init_internals(GntTree *tree,int col);
static GntWidgetClass *parent_class = (GntWidgetClass *)((void *)0);
static guint signals[4UL] = {(0)};

static void readjust_columns(GntTree *tree)
{
  int i;
  int col;
  int total;
  int width;
#define WIDTH(i) (tree->columns[i].width_ratio ? tree->columns[i].width_ratio : tree->columns[i].width)
  gnt_widget_get_size(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype()))),&width,0);
  if (!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_NO_BORDER) != 0U)) 
    width -= 2;
/* Exclude the scrollbar from the calculation */
  width -= 1;
  for (((i = 0) , (total = 0)); i < (tree -> ncol); i++) {
    if (((tree -> columns)[i].flags & GNT_TREE_COLUMN_INVISIBLE) != 0U) 
      continue; 
    if (((tree -> columns)[i].flags & GNT_TREE_COLUMN_FIXED_SIZE) != 0U) 
      width -= (((((tree -> columns)[i].width_ratio != 0)?(tree -> columns)[i].width_ratio : (tree -> columns)[i].width)) + (( *(tree -> priv)).lastvisible != i));
    else 
      total += (((((tree -> columns)[i].width_ratio != 0)?(tree -> columns)[i].width_ratio : (tree -> columns)[i].width)) + (( *(tree -> priv)).lastvisible != i));
  }
  if (total == 0) 
    return ;
  for (i = 0; i < (tree -> ncol); i++) {
    if (((tree -> columns)[i].flags & GNT_TREE_COLUMN_INVISIBLE) != 0U) 
      continue; 
    if (((tree -> columns)[i].flags & GNT_TREE_COLUMN_FIXED_SIZE) != 0U) 
      col = (((tree -> columns)[i].width_ratio != 0)?(tree -> columns)[i].width_ratio : (tree -> columns)[i].width);
    else 
      col = ((((((tree -> columns)[i].width_ratio != 0)?(tree -> columns)[i].width_ratio : (tree -> columns)[i].width)) * width) / total);
    gnt_tree_set_col_width(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),i,col);
  }
}
/* Move the item at position old to position new */

static GList *g_list_reposition_child(GList *list,int old,int new)
{
  gpointer item = g_list_nth_data(list,old);
  list = g_list_remove(list,item);
  if (old < new) 
/* because the positions would have shifted after removing the item */
    new--;
  list = g_list_insert(list,item,new);
  return list;
}

static GntTreeRow *_get_next(GntTreeRow *row,gboolean godeep)
{
  if (row == ((GntTreeRow *)((void *)0))) 
    return 0;
  if ((godeep != 0) && ((row -> child) != 0)) 
    return row -> child;
  if ((row -> next) != 0) 
    return row -> next;
  return _get_next((row -> parent),0);
}

static gboolean row_matches_search(GntTreeRow *row)
{
  GntTree *t = (row -> tree);
  if ((( *(t -> priv)).search != 0) && (( *( *(t -> priv)).search).len > 0)) {
    GntTreeCol *col = (((col = (g_list_nth_data((row -> columns),( *(t -> priv)).search_column))) != 0)?col : ( *(row -> columns)).data);
    char *one;
    char *two;
    char *z;
    if (( *(t -> priv)).search_func != 0) 
      return ( *( *(t -> priv)).search_func)(t,(row -> key),( *( *(t -> priv)).search).str,(col -> text));
    one = g_utf8_casefold((col -> text),(-1));
    two = g_utf8_casefold(( *( *(t -> priv)).search).str,(-1));
    z = strstr(one,two);
    g_free(one);
    g_free(two);
    if (z == ((char *)((void *)0))) 
      return 0;
  }
  return 1;
}

static GntTreeRow *get_next(GntTreeRow *row)
{
  if (row == ((GntTreeRow *)((void *)0))) 
    return 0;
{
    while((row = _get_next(row,!((row -> collapsed) != 0))) != ((GntTreeRow *)((void *)0))){
      if (row_matches_search(row) != 0) 
        break; 
    }
  }
  return row;
}
/* Returns the n-th next row. If it doesn't exist, returns NULL */

static GntTreeRow *get_next_n(GntTreeRow *row,int n)
{
  while((row != 0) && (n-- != 0))
    row = get_next(row);
  return row;
}
/* Returns the n-th next row. If it doesn't exist, then the last non-NULL node */

static GntTreeRow *get_next_n_opt(GntTreeRow *row,int n,int *pos)
{
  GntTreeRow *next = row;
  int r = 0;
  if (row == ((GntTreeRow *)((void *)0))) 
    return 0;
  while((row != 0) && (n-- != 0)){
    row = get_next(row);
    if (row != 0) {
      next = row;
      r++;
    }
  }
  if (pos != 0) 
     *pos = r;
  return next;
}

static GntTreeRow *get_last_child(GntTreeRow *row)
{
  if (row == ((GntTreeRow *)((void *)0))) 
    return 0;
  if (!((row -> collapsed) != 0) && ((row -> child) != 0)) 
    row = (row -> child);
  else 
    return row;
  while((row -> next) != 0)
    row = (row -> next);
  return get_last_child(row);
}

static GntTreeRow *get_prev(GntTreeRow *row)
{
  if (row == ((GntTreeRow *)((void *)0))) 
    return 0;
{
    while(row != 0){
      if ((row -> prev) != 0) 
        row = get_last_child((row -> prev));
      else 
        row = (row -> parent);
      if (!(row != 0) || (row_matches_search(row) != 0)) 
        break; 
    }
  }
  return row;
}

static GntTreeRow *get_prev_n(GntTreeRow *row,int n)
{
  while((row != 0) && (n-- != 0))
    row = get_prev(row);
  return row;
}
/* Distance of row from the root */
/* XXX: This is uber-inefficient */

static int get_root_distance(GntTreeRow *row)
{
  if (row == ((GntTreeRow *)((void *)0))) 
    return -1;
  return get_root_distance(get_prev(row)) + 1;
}
/* Returns the distance between a and b.
 * If a is 'above' b, then the distance is positive */

static int get_distance(GntTreeRow *a,GntTreeRow *b)
{
/* First get the distance from a to the root.
	 * Then the distance from b to the root.
	 * Subtract.
	 * It's not that good, but it works. */
  int ha = get_root_distance(a);
  int hb = get_root_distance(b);
  return hb - ha;
}

static int find_depth(GntTreeRow *row)
{
  int dep = -1;
  while(row != 0){
    dep++;
    row = (row -> parent);
  }
  return dep;
}

static char *update_row_text(GntTree *tree,GntTreeRow *row)
{
  GString *string = g_string_new(0);
  GList *iter;
  int i;
  gboolean notfirst = 0;
  for (((i = 0) , (iter = (row -> columns))); (i < (tree -> ncol)) && (iter != 0); (i++ , (iter = (iter -> next)))) {{
      GntTreeCol *col = (iter -> data);
      const char *text;
      int len;
      int fl = 0;
      gboolean cut = 0;
      int width;
      const char *display;
      if (((tree -> columns)[i].flags & GNT_TREE_COLUMN_INVISIBLE) != 0U) 
        continue; 
      if (((tree -> columns)[i].flags & GNT_TREE_COLUMN_BINARY_DATA) != 0U) 
        display = "";
      else 
        display = (col -> text);
      len = gnt_util_onscreen_width(display,0);
      width = (tree -> columns)[i].width;
      if (i == 0) {
        if ((row -> choice) != 0) {
          g_string_append_printf(string,"[%c] ",(((row -> isselected) != 0)?'X' : 32));
          fl = 4;
        }
        else if ((find_depth(row) < ( *(tree -> priv)).expander_level) && ((row -> child) != 0)) {
          if ((row -> collapsed) != 0) {
            string = g_string_append(string,"+ ");
          }
          else {
            string = g_string_append(string,"- ");
          }
          fl = 2;
        }
        else {
          fl = (3 * find_depth(row));
          g_string_append_printf(string,"%*s",fl,"");
        }
        len += fl;
      }
      else if ((notfirst != 0) && ((tree -> show_separator) != 0)) 
        g_string_append_c_inline(string,'|');
      else 
        g_string_append_c_inline(string,32);
      notfirst = 1;
      if (len > width) {
        len = ((1 > (width - 1))?1 : (width - 1));
        cut = 1;
      }
      if ((((tree -> columns)[i].flags & GNT_TREE_COLUMN_RIGHT_ALIGNED) != 0U) && (len < (tree -> columns)[i].width)) {
        g_string_append_printf(string,"%*s",((width - len) - cut),"");
      }
      text = gnt_util_onscreen_width_to_pointer(display,(len - fl),0);
      string = g_string_append_len(string,display,(text - display));
/* ellipsis */
      if ((cut != 0) && (width > 1)) {
        if (gnt_ascii_only() != 0) 
          g_string_append_c_inline(string,'~');
        else 
          string = g_string_append(string,"\342\200\246");
        len++;
      }
      if ((!(((tree -> columns)[i].flags & GNT_TREE_COLUMN_RIGHT_ALIGNED) != 0U) && (len < (tree -> columns)[i].width)) && ((iter -> next) != 0)) 
        g_string_append_printf(string,"%*s",(width - len),"");
    }
  }
  return g_string_free(string,0);
}
#define NEXT_X x += tree->columns[i].width + (i > 0 ? 1 : 0)

static void tree_mark_columns(GntTree *tree,int pos,int y,chtype type)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype()));
  int i;
  int x = pos;
  gboolean notfirst = 0;
  for (i = 0; i < ((tree -> ncol) - 1); i++) {
    if (!(((tree -> columns)[i].flags & GNT_TREE_COLUMN_INVISIBLE) != 0U)) {
      notfirst = 1;
      x += ((tree -> columns)[i].width + (((i > 0)?1 : 0)));
    }
    if (!(((tree -> columns)[i + 1].flags & GNT_TREE_COLUMN_INVISIBLE) != 0U) && (notfirst != 0)) 
      (wmove((widget -> window),y,x) == -1)?-1 : waddch((widget -> window),type);
  }
}

static void redraw_tree(GntTree *tree)
{
  int start;
  int i;
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype()));
  GntTreeRow *row;
  int pos;
  int up;
  int down = 0;
  int rows;
  int scrcol;
  int current = 0;
  if (!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U)) 
    return ;
  if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_NO_BORDER) != 0U) 
    pos = 0;
  else 
    pos = 1;
  if ((tree -> top) == ((GntTreeRow *)((void *)0))) 
    tree -> top = (tree -> root);
  if (((tree -> current) == ((GntTreeRow *)((void *)0))) && ((tree -> root) != ((GntTreeRow *)((void *)0)))) {
    tree -> current = (tree -> root);
    tree_selection_changed(tree,0,(tree -> current));
  }
  wbkgd((widget -> window),(gnt_color_pair(GNT_COLOR_NORMAL)));
  start = 0;
  if ((tree -> show_title) != 0) {
    int i;
    int x = pos;
    (wmove((widget -> window),(pos + 1),pos) == -1)?-1 : whline((widget -> window),(acs_map[(unsigned char )'q'] | (gnt_color_pair(GNT_COLOR_NORMAL))),((widget -> priv.width - pos) - 1));
    (wmove((widget -> window),pos,pos) == -1)?-1 : whline((widget -> window),(32 | gnt_color_pair(GNT_COLOR_NORMAL)),((widget -> priv.width - pos) - 1));
    for (i = 0; i < (tree -> ncol); i++) {
      if (((tree -> columns)[i].flags & GNT_TREE_COLUMN_INVISIBLE) != 0U) {
        continue; 
      }
      (wmove((widget -> window),pos,(x + (x != pos))) == -1)?-1 : waddnstr((widget -> window),(tree -> columns)[i].title,(tree -> columns)[i].width);
      x += ((tree -> columns)[i].width + (((i > 0)?1 : 0)));
    }
    if (pos != 0) {
      tree_mark_columns(tree,pos,0,(((((tree -> show_separator) != 0)?acs_map[(unsigned char )'w'] : acs_map[(unsigned char )'q'])) | (gnt_color_pair(GNT_COLOR_NORMAL))));
      tree_mark_columns(tree,pos,(widget -> priv.height - pos),(((((tree -> show_separator) != 0)?acs_map[(unsigned char )'v'] : acs_map[(unsigned char )'q'])) | (gnt_color_pair(GNT_COLOR_NORMAL))));
    }
    tree_mark_columns(tree,pos,(pos + 1),(((((tree -> show_separator) != 0)?acs_map[(unsigned char )'n'] : acs_map[(unsigned char )'q'])) | (gnt_color_pair(GNT_COLOR_NORMAL))));
    tree_mark_columns(tree,pos,pos,(((((tree -> show_separator) != 0)?acs_map[(unsigned char )'x'] : 32)) | (gnt_color_pair(GNT_COLOR_NORMAL))));
    start = 2;
  }
  rows = (((widget -> priv.height - (pos * 2)) - start) - 1);
  tree -> bottom = get_next_n_opt((tree -> top),rows,&down);
  if (down < rows) {
    tree -> top = get_prev_n((tree -> bottom),rows);
    if ((tree -> top) == ((GntTreeRow *)((void *)0))) 
      tree -> top = (tree -> root);
  }
  up = get_distance((tree -> top),(tree -> current));
  if (up < 0) 
    tree -> top = (tree -> current);
  else if (up >= (widget -> priv.height - pos)) 
    tree -> top = get_prev_n((tree -> current),rows);
  if (((tree -> top) != 0) && !(row_matches_search((tree -> top)) != 0)) 
    tree -> top = get_next((tree -> top));
  row = (tree -> top);
/* exclude the borders and the scrollbar */
  scrcol = ((widget -> priv.width - 1) - (2 * pos));
  if (((tree -> current) != 0) && !(row_matches_search((tree -> current)) != 0)) {
    GntTreeRow *old = (tree -> current);
    tree -> current = (tree -> top);
    tree_selection_changed(tree,old,(tree -> current));
  }
  for (i = (start + pos); (row != 0) && (i < (widget -> priv.height - pos)); (i++ , (row = get_next(row)))) {{
      char *str;
      int wr;
      GntTextFormatFlags flags = (row -> flags);
      int attr = 0;
      if (!(row_matches_search(row) != 0)) 
        continue; 
      str = update_row_text(tree,row);
      if ((wr = gnt_util_onscreen_width(str,0)) > scrcol) {
        char *s = (char *)(gnt_util_onscreen_width_to_pointer(str,scrcol,&wr));
         *s = 0;
      }
      if ((flags & GNT_TEXT_FLAG_BOLD) != 0U) 
        attr |= (((chtype )1UL) << 13 + 8);
      if ((flags & GNT_TEXT_FLAG_UNDERLINE) != 0U) 
        attr |= (((chtype )1UL) << 9 + 8);
      if ((flags & GNT_TEXT_FLAG_BLINK) != 0U) 
        attr |= (((chtype )1UL) << 11 + 8);
      if (row == (tree -> current)) {
        current = i;
        attr |= (((chtype )1UL) << 13 + 8);
        if (gnt_widget_has_focus(widget) != 0) 
          attr |= gnt_color_pair(GNT_COLOR_HIGHLIGHT);
        else 
          attr |= gnt_color_pair(GNT_COLOR_HIGHLIGHT_D);
      }
      else {
        if ((flags & GNT_TEXT_FLAG_DIM) != 0U) 
          if ((row -> color) != 0) 
            attr |= ((((chtype )1UL) << 12 + 8) | (gnt_color_pair((row -> color))));
          else 
            attr |= ((((chtype )1UL) << 12 + 8) | (gnt_color_pair(GNT_COLOR_DISABLED)));
        else if ((flags & GNT_TEXT_FLAG_HIGHLIGHT) != 0U) 
          attr |= ((((chtype )1UL) << 12 + 8) | (gnt_color_pair(GNT_COLOR_HIGHLIGHT)));
        else if ((row -> color) != 0) 
          attr |= gnt_color_pair((row -> color));
        else 
          attr |= gnt_color_pair(GNT_COLOR_NORMAL);
      }
      wbkgdset((widget -> window),(0 | attr));
      (wmove((widget -> window),i,pos) == -1)?-1 : waddnstr((widget -> window),C_(str),-1);
      whline((widget -> window),32,(scrcol - wr));
      tree -> bottom = row;
      g_free(str);
      tree_mark_columns(tree,pos,i,(((((tree -> show_separator) != 0)?acs_map[(unsigned char )'x'] : 32)) | attr));
    }
  }
  wbkgdset((widget -> window),(0 | gnt_color_pair(GNT_COLOR_NORMAL)));
  while(i < (widget -> priv.height - pos)){
    (wmove((widget -> window),i,pos) == -1)?-1 : whline((widget -> window),32,((widget -> priv.width - (pos * 2)) - 1));
    tree_mark_columns(tree,pos,i,(((tree -> show_separator) != 0)?acs_map[(unsigned char )'x'] : 32));
    i++;
  }
/* position of the scrollbar */
  scrcol = ((widget -> priv.width - pos) - 1);
  rows--;
  if (rows > 0) {
    int total = 0;
    int showing;
    int position;
    get_next_n_opt((tree -> root),(g_list_length((tree -> list))),&total);
    showing = (((rows * rows) / (((total > 1)?total : 1))) + 1);
    showing = ((rows < showing)?rows : showing);
    total -= rows;
    up = get_distance((tree -> root),(tree -> top));
    down = (total - up);
    position = (((rows - showing) * up) / (((1 > (up + down))?1 : (up + down))));
    position = ((((tree -> top) != (tree -> root)) > position)?((tree -> top) != (tree -> root)) : position);
    if ((showing + position) > rows) 
      position = (rows - showing);
    if (((showing + position) == rows) && (row != 0)) 
      position = ((0 > ((rows - 1) - showing))?0 : ((rows - 1) - showing));
    else if (((showing + position) < rows) && !(row != 0)) 
      position = (rows - showing);
    position += ((pos + start) + 1);
    (wmove((widget -> window),((pos + start) + 1),scrcol) == -1)?-1 : wvline((widget -> window),(32 | gnt_color_pair(GNT_COLOR_NORMAL)),rows);
    (wmove((widget -> window),position,scrcol) == -1)?-1 : wvline((widget -> window),(acs_map[(unsigned char )'a'] | (gnt_color_pair(GNT_COLOR_HIGHLIGHT_D))),showing);
  }
  (wmove((widget -> window),(start + pos),scrcol) == -1)?-1 : waddch((widget -> window),(((((tree -> top) != (tree -> root))?acs_map[(unsigned char )'-'] : 32)) | (gnt_color_pair(GNT_COLOR_HIGHLIGHT_D))));
  (wmove((widget -> window),((widget -> priv.height - pos) - 1),scrcol) == -1)?-1 : waddch((widget -> window),((((row != 0)?acs_map[(unsigned char )'.'] : 32)) | (gnt_color_pair(GNT_COLOR_HIGHLIGHT_D))));
/* If there's a search-text, show it in the bottom of the tree */
  if ((( *(tree -> priv)).search != 0) && (( *( *(tree -> priv)).search).len > 0)) {
    const char *str = gnt_util_onscreen_width_to_pointer(( *( *(tree -> priv)).search).str,(scrcol - 1),0);
    wbkgdset((widget -> window),(0 | gnt_color_pair(GNT_COLOR_HIGHLIGHT_D)));
    (wmove((widget -> window),((widget -> priv.height - pos) - 1),pos) == -1)?-1 : waddnstr((widget -> window),( *( *(tree -> priv)).search).str,(str - ( *( *(tree -> priv)).search).str));
  }
  wmove((widget -> window),current,pos);
  gnt_widget_queue_update(widget);
}

static void gnt_tree_draw(GntWidget *widget)
{
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()));
  redraw_tree(tree);;
}

static void gnt_tree_size_request(GntWidget *widget)
{
  if (widget -> priv.height == 0) 
/* XXX: Why?! */
    widget -> priv.height = 10;
  if (widget -> priv.width == 0) {
    GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()));
    int i;
    int width = 0;
    width = (1 + (2 * !((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_NO_BORDER) != 0U)));
    for (i = 0; i < (tree -> ncol); i++) 
      if (!(((tree -> columns)[i].flags & GNT_TREE_COLUMN_INVISIBLE) != 0U)) {
        width = (width + (tree -> columns)[i].width);
        if (( *(tree -> priv)).lastvisible != i) 
          width++;
      }
    widget -> priv.width = width;
  }
}

static void gnt_tree_map(GntWidget *widget)
{
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()));
  if ((widget -> priv.width == 0) || (widget -> priv.height == 0)) {
    gnt_widget_size_request(widget);
  }
  tree -> top = (tree -> root);
  tree -> current = (tree -> root);;
}

static void tree_selection_changed(GntTree *tree,GntTreeRow *old,GntTreeRow *current)
{
  g_signal_emit(tree,signals[SIG_SELECTION_CHANGED],0,((old != 0)?(old -> key) : ((void *)((void *)0))),((current != 0)?(current -> key) : ((void *)((void *)0))));
}

static gboolean action_down(GntBindable *bind,GList *null)
{
  int dist;
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_tree_get_gtype()));
  GntTreeRow *old = (tree -> current);
  GntTreeRow *row = get_next((tree -> current));
  if (row == ((GntTreeRow *)((void *)0))) 
    return 0;
  tree -> current = row;
  if ((dist = get_distance((tree -> current),(tree -> bottom))) < 0) 
    gnt_tree_scroll(tree,-dist);
  else 
    redraw_tree(tree);
  if (old != (tree -> current)) 
    tree_selection_changed(tree,old,(tree -> current));
  return 1;
}

static gboolean action_move_parent(GntBindable *bind,GList *null)
{
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_tree_get_gtype()));
  GntTreeRow *row = (tree -> current);
  int dist;
  if ((!(row != 0) || !((row -> parent) != 0)) || ((( *(tree -> priv)).search != 0) && (( *( *(tree -> priv)).search).len > 0))) 
    return 0;
  tree -> current = (row -> parent);
  if ((dist = get_distance((tree -> current),(tree -> top))) > 0) 
    gnt_tree_scroll(tree,-dist);
  else 
    redraw_tree(tree);
  tree_selection_changed(tree,row,(tree -> current));
  return 1;
}

static gboolean action_up(GntBindable *bind,GList *list)
{
  int dist;
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_tree_get_gtype()));
  GntTreeRow *old = (tree -> current);
  GntTreeRow *row = get_prev((tree -> current));
  if (!(row != 0)) 
    return 0;
  tree -> current = row;
  if ((dist = get_distance((tree -> current),(tree -> top))) > 0) 
    gnt_tree_scroll(tree,-dist);
  else 
    redraw_tree(tree);
  if (old != (tree -> current)) 
    tree_selection_changed(tree,old,(tree -> current));
  return 1;
}

static gboolean action_page_down(GntBindable *bind,GList *null)
{
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_tree_get_gtype()));
  GntTreeRow *old = (tree -> current);
  GntTreeRow *row = get_next((tree -> bottom));
  if (row != 0) {
    int dist = get_distance((tree -> top),(tree -> current));
    tree -> top = (tree -> bottom);
    tree -> current = get_next_n_opt((tree -> top),dist,0);
    redraw_tree(tree);
  }
  else if ((tree -> current) != (tree -> bottom)) {
    tree -> current = (tree -> bottom);
    redraw_tree(tree);
  }
  if (old != (tree -> current)) 
    tree_selection_changed(tree,old,(tree -> current));
  return 1;
}

static gboolean action_page_up(GntBindable *bind,GList *null)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_widget_get_gtype()));
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_tree_get_gtype()));
  GntTreeRow *row;
  GntTreeRow *old = (tree -> current);
  if ((tree -> top) != (tree -> root)) {
    int dist = get_distance((tree -> top),(tree -> current));
    row = get_prev_n((tree -> top),(((widget -> priv.height - 1) - ((tree -> show_title) * 2)) - (2 * ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_NO_BORDER) == 0))));
    if (row == ((GntTreeRow *)((void *)0))) 
      row = (tree -> root);
    tree -> top = row;
    tree -> current = get_next_n_opt((tree -> top),dist,0);
    redraw_tree(tree);
  }
  else if ((tree -> current) != (tree -> top)) {
    tree -> current = (tree -> top);
    redraw_tree(tree);
  }
  if (old != (tree -> current)) 
    tree_selection_changed(tree,old,(tree -> current));
  return 1;
}

static void end_search(GntTree *tree)
{
  if (( *(tree -> priv)).search != 0) {
    g_source_remove(( *(tree -> priv)).search_timeout);
    g_string_free(( *(tree -> priv)).search,1);
    ( *(tree -> priv)).search = ((GString *)((void *)0));
    ( *(tree -> priv)).search_timeout = 0;
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_DISABLE_ACTIONS);
  }
}

static gboolean search_timeout(gpointer data)
{
  GntTree *tree = data;
  end_search(tree);
  redraw_tree(tree);
  return 0;
}

static gboolean gnt_tree_key_pressed(GntWidget *widget,const char *text)
{
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()));
  GntTreeRow *old = (tree -> current);
  if ((text[0] == 13) || (text[0] == 10)) {
    end_search(tree);
    gnt_widget_activate(widget);
  }
  else if (( *(tree -> priv)).search != 0) {
    gboolean changed = 1;
    if (g_unichar_isprint(( *text)) != 0) {
      ( *(tree -> priv)).search = g_string_append_c_inline(( *(tree -> priv)).search, *text);
    }
    else if (g_utf8_collate(text,(((cur_term -> type.Strings[55] != 0)?cur_term -> type.Strings[55] : ""))) == 0) {
      if (( *( *(tree -> priv)).search).len != 0UL) 
        ( *( *(tree -> priv)).search).str[--( *( *(tree -> priv)).search).len] = 0;
    }
    else 
      changed = 0;
    if (changed != 0) {
      redraw_tree(tree);
    }
    else {
      gnt_bindable_perform_action_key(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_bindable_get_gtype()))),text);
    }
    g_source_remove(( *(tree -> priv)).search_timeout);
    ( *(tree -> priv)).search_timeout = (g_timeout_add_seconds(4,search_timeout,tree));
    return 1;
  }
  else if ((text[0] == 32) && (text[1] == 0)) {
/* Space pressed */
    GntTreeRow *row = (tree -> current);
    if ((row != 0) && ((row -> child) != 0)) {
      row -> collapsed = !((row -> collapsed) != 0);
      redraw_tree(tree);
      g_signal_emit(tree,signals[SIG_COLLAPSED],0,(row -> key),(row -> collapsed));
    }
    else if ((row != 0) && ((row -> choice) != 0)) {
      row -> isselected = !((row -> isselected) != 0);
      g_signal_emit(tree,signals[SIG_TOGGLED],0,(row -> key));
      redraw_tree(tree);
    }
  }
  else {
    return 0;
  }
  if (old != (tree -> current)) {
    tree_selection_changed(tree,old,(tree -> current));
  }
  return 1;
}

static void gnt_tree_free_columns(GntTree *tree)
{
  int i;
  for (i = 0; i < (tree -> ncol); i++) {
    g_free((tree -> columns)[i].title);
  }
  g_free((tree -> columns));
}

static void gnt_tree_destroy(GntWidget *widget)
{
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()));
  end_search(tree);
  if ((tree -> hash) != 0) 
    g_hash_table_destroy((tree -> hash));
  g_list_free((tree -> list));
  gnt_tree_free_columns(tree);
  g_free((tree -> priv));
}

static gboolean gnt_tree_clicked(GntWidget *widget,GntMouseEvent event,int x,int y)
{
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()));
  GntTreeRow *old = (tree -> current);
  if (event == GNT_MOUSE_SCROLL_UP) {
    action_up(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_bindable_get_gtype()))),0);
  }
  else if (event == GNT_MOUSE_SCROLL_DOWN) {
    action_down(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_bindable_get_gtype()))),0);
  }
  else if (event == GNT_LEFT_MOUSE_DOWN) {
    GntTreeRow *row;
    GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()));
    int pos = 1;
    if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_NO_BORDER) != 0U) 
      pos = 0;
    if ((tree -> show_title) != 0) 
      pos += 2;
    pos = ((y - widget -> priv.y) - pos);
    row = get_next_n((tree -> top),pos);
    if ((row != 0) && ((tree -> current) != row)) {
      GntTreeRow *old = (tree -> current);
      tree -> current = row;
      redraw_tree(tree);
      tree_selection_changed(tree,old,(tree -> current));
    }
    else if ((row != 0) && (row == (tree -> current))) {
      if ((row -> choice) != 0) {
        row -> isselected = !((row -> isselected) != 0);
        g_signal_emit(tree,signals[SIG_TOGGLED],0,(row -> key));
        redraw_tree(tree);
      }
      else {
        gnt_widget_activate(widget);
      }
    }
  }
  else {
    return 0;
  }
  if (old != (tree -> current)) {
    tree_selection_changed(tree,old,(tree -> current));
  }
  return 1;
}

static void gnt_tree_size_changed(GntWidget *widget,int w,int h)
{
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()));
  if (widget -> priv.width <= 0) 
    return ;
  readjust_columns(tree);
}

static gboolean start_search(GntBindable *bindable,GList *list)
{
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_tree_get_gtype()));
  if (( *(tree -> priv)).search != 0) 
    return 0;
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_DISABLE_ACTIONS;
  ( *(tree -> priv)).search = g_string_new(0);
  ( *(tree -> priv)).search_timeout = (g_timeout_add_seconds(4,search_timeout,tree));
  return 1;
}

static gboolean end_search_action(GntBindable *bindable,GList *list)
{
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_tree_get_gtype()));
  if (( *(tree -> priv)).search == ((GString *)((void *)0))) 
    return 0;
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_DISABLE_ACTIONS);
  end_search(tree);
  redraw_tree(tree);
  return 1;
}

static gboolean move_first_action(GntBindable *bind,GList *null)
{
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_tree_get_gtype()));
  GntTreeRow *row = (tree -> root);
  GntTreeRow *old = (tree -> current);
  if ((row != 0) && !(row_matches_search(row) != 0)) 
    row = get_next(row);
  if (row != 0) {
    tree -> current = row;
    redraw_tree(tree);
    if (old != (tree -> current)) 
      tree_selection_changed(tree,old,(tree -> current));
  }
  return 1;
}

static gboolean move_last_action(GntBindable *bind,GList *null)
{
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_tree_get_gtype()));
  GntTreeRow *old = (tree -> current);
  GntTreeRow *row = (tree -> bottom);
  GntTreeRow *next;
  while((next = get_next(row)) != 0)
    row = next;
  if (row != 0) {
    tree -> current = row;
    redraw_tree(tree);
    if (old != (tree -> current)) 
      tree_selection_changed(tree,old,(tree -> current));
  }
  return 1;
}

static void gnt_tree_set_property(GObject *obj,guint prop_id,const GValue *value,GParamSpec *spec)
{
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)obj),gnt_tree_get_gtype()));
{
    switch(prop_id){
      case PROP_COLUMNS:
{
        _gnt_tree_init_internals(tree,g_value_get_int(value));
        break; 
      }
      case PROP_EXPANDER:
{
        if (( *(tree -> priv)).expander_level == g_value_get_int(value)) 
          break; 
        ( *(tree -> priv)).expander_level = g_value_get_int(value);
        g_object_notify(obj,"expander-level");
      }
      default:
{
        break; 
      }
    }
  }
}

static void gnt_tree_get_property(GObject *obj,guint prop_id,GValue *value,GParamSpec *spec)
{
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)obj),gnt_tree_get_gtype()));
  switch(prop_id){
    case PROP_COLUMNS:
{
      g_value_set_int(value,(tree -> ncol));
      break; 
    }
    case PROP_EXPANDER:
{
      g_value_set_int(value,( *(tree -> priv)).expander_level);
      break; 
    }
    default:
{
      break; 
    }
  }
}

static void gnt_tree_class_init(GntTreeClass *klass)
{
  GntBindableClass *bindable = (GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()));
  GObjectClass *gclass = (GObjectClass *)(g_type_check_class_cast(((GTypeClass *)klass),((GType )(20 << 2))));
  parent_class = ((GntWidgetClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_widget_get_gtype())));
  parent_class -> destroy = gnt_tree_destroy;
  parent_class -> draw = gnt_tree_draw;
  parent_class -> map = gnt_tree_map;
  parent_class -> size_request = gnt_tree_size_request;
  parent_class -> key_pressed = gnt_tree_key_pressed;
  parent_class -> clicked = gnt_tree_clicked;
  parent_class -> size_changed = gnt_tree_size_changed;
  gclass -> set_property = gnt_tree_set_property;
  gclass -> get_property = gnt_tree_get_property;
  g_object_class_install_property(gclass,PROP_COLUMNS,g_param_spec_int("columns","Columns","Number of columns in the tree.",1,2147483647,1,(G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB)));
  g_object_class_install_property(gclass,PROP_EXPANDER,g_param_spec_int("expander-level","Expander level","Number of levels to show expander in the tree.",0,2147483647,1,(G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB)));
  signals[SIG_SELECTION_CHANGED] = g_signal_new("selection-changed",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntTreeClass *)((GntTreeClass *)0))).selection_changed))),0,0,gnt_closure_marshal_VOID__POINTER_POINTER,((GType )(1 << 2)),2,((GType )(17 << 2)),((GType )(17 << 2)));
  signals[SIG_SCROLLED] = g_signal_new("scrolled",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,0,0,0,g_cclosure_marshal_VOID__INT,((GType )(1 << 2)),1,((GType )(6 << 2)));
  signals[SIG_TOGGLED] = g_signal_new("toggled",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntTreeClass *)((GntTreeClass *)0))).toggled))),0,0,g_cclosure_marshal_VOID__POINTER,((GType )(1 << 2)),1,((GType )(17 << 2)));
  signals[SIG_COLLAPSED] = g_signal_new("collapse-toggled",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,0,0,0,gnt_closure_marshal_VOID__POINTER_BOOLEAN,((GType )(1 << 2)),2,((GType )(17 << 2)),((GType )(5 << 2)));
  gnt_bindable_class_register_action(bindable,"move-up",action_up,((cur_term -> type.Strings[87] != 0)?cur_term -> type.Strings[87] : ""),((void *)((void *)0)));
  gnt_bindable_register_binding(bindable,"move-up","\020",((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"move-down",action_down,((cur_term -> type.Strings[61] != 0)?cur_term -> type.Strings[61] : ""),((void *)((void *)0)));
  gnt_bindable_register_binding(bindable,"move-down","\016",((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"move-parent",action_move_parent,((cur_term -> type.Strings[55] != 0)?cur_term -> type.Strings[55] : ""),((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"page-up",action_page_up,((cur_term -> type.Strings[82] != 0)?cur_term -> type.Strings[82] : ""),((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"page-down",action_page_down,((cur_term -> type.Strings[81] != 0)?cur_term -> type.Strings[81] : ""),((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"start-search",start_search,"/",((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"end-search",end_search_action,"\033",((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"move-first",move_first_action,((cur_term -> type.Strings[76] != 0)?cur_term -> type.Strings[76] : ""),((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"move-last",move_last_action,((cur_term -> type.Strings[164] != 0)?cur_term -> type.Strings[164] : ""),((void *)((void *)0)));
  gnt_style_read_actions(( *((GTypeClass *)klass)).g_type,bindable);;
}

static void gnt_tree_init(GTypeInstance *instance,gpointer class)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_widget_get_gtype()));
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()));
  tree -> show_separator = 1;
  tree -> priv = ((GntTreePriv *)(g_malloc0_n(1,(sizeof(GntTreePriv )))));
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= (GNT_WIDGET_GROW_X | GNT_WIDGET_GROW_Y | GNT_WIDGET_CAN_TAKE_FOCUS | GNT_WIDGET_NO_SHADOW);
  gnt_widget_set_take_focus(widget,1);
  widget -> priv.minw = 4;
  widget -> priv.minh = 1;;
}
/******************************************************************************
 * GntTree API
 *****************************************************************************/

GType gnt_tree_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(GntTreeClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gnt_tree_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GntTree ))), (0), (gnt_tree_init), ((const GTypeValueTable *)((void *)0))
/* base_init		*/
/* base_finalize	*/
/* class_finalize	*/
/* class_data		*/
/* n_preallocs		*/
/* instance_init	*/
/* value_table		*/
};
    type = g_type_register_static(gnt_widget_get_gtype(),"GntTree",&info,0);
  }
  return type;
}

static void free_tree_col(gpointer data)
{
  GntTreeCol *col = data;
  if (!((col -> isbinary) != 0)) 
    g_free((col -> text));
  g_free(col);
}

static void free_tree_row(gpointer data)
{
  GntTreeRow *row = data;
  if (!(row != 0)) 
    return ;
  g_list_foreach((row -> columns),((GFunc )free_tree_col),0);
  g_list_free((row -> columns));
  g_free(row);
}

GntWidget *gnt_tree_new()
{
  return gnt_tree_new_with_columns(1);
}

void gnt_tree_set_visible_rows(GntTree *tree,int rows)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype()));
  widget -> priv.height = rows;
  if (!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_NO_BORDER) != 0U)) 
    widget -> priv.height += 2;
}

int gnt_tree_get_visible_rows(GntTree *tree)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype()));
  int ret = widget -> priv.height;
  if (!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_NO_BORDER) != 0U)) 
    ret -= 2;
  return ret;
}

GList *gnt_tree_get_rows(GntTree *tree)
{
  return tree -> list;
}

void gnt_tree_scroll(GntTree *tree,int count)
{
  GntTreeRow *row;
  if (count < 0) {
    if (get_root_distance((tree -> top)) == 0) 
      return ;
    row = get_prev_n((tree -> top),-count);
    if (row == ((GntTreeRow *)((void *)0))) 
      row = (tree -> root);
    tree -> top = row;
  }
  else {
    get_next_n_opt((tree -> bottom),count,&count);
    tree -> top = get_next_n((tree -> top),count);
  }
  redraw_tree(tree);
  g_signal_emit(tree,signals[SIG_SCROLLED],0,count);
}

static gpointer find_position(GntTree *tree,gpointer key,gpointer parent)
{
  GntTreeRow *row;
  if (( *(tree -> priv)).compare == ((gint (*)(gconstpointer , gconstpointer ))((void *)0))) 
    return 0;
  if (parent == ((void *)((void *)0))) 
    row = (tree -> root);
  else 
    row = (g_hash_table_lookup((tree -> hash),parent));
  if (!(row != 0)) 
    return 0;
  if (parent != 0) 
    row = (row -> child);
  while(row != 0){
    if (( *( *(tree -> priv)).compare)(key,(row -> key)) < 0) 
      return ((row -> prev) != 0)?( *(row -> prev)).key : ((void *)((void *)0));
    if ((row -> next) != 0) 
      row = (row -> next);
    else 
      return row -> key;
  }
  return 0;
}

void gnt_tree_sort_row(GntTree *tree,gpointer key)
{
  GntTreeRow *row;
  GntTreeRow *q;
  GntTreeRow *s;
  int current;
  int newp;
  if (!(( *(tree -> priv)).compare != 0)) 
    return ;
  row = (g_hash_table_lookup((tree -> hash),key));
  do {
    if (row != ((GntTreeRow *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning("Gnt",((const char *)__func__),"row != NULL");
      return ;
    };
  }while (0);
  current = g_list_index((tree -> list),key);
  if ((row -> parent) != 0) 
    s = ( *(row -> parent)).child;
  else 
    s = (tree -> root);
  q = ((GntTreeRow *)((void *)0));
{
    while(s != 0){
      if (( *( *(tree -> priv)).compare)((row -> key),(s -> key)) < 0) 
        break; 
      q = s;
      s = (s -> next);
    }
  }
/* Move row between q and s */
  if ((row == q) || (row == s)) 
    return ;
  if (q == ((GntTreeRow *)((void *)0))) {
/* row becomes the first child of its parent */
/* row->prev cannot be NULL at this point */
    ( *(row -> prev)).next = (row -> next);
    if ((row -> next) != 0) 
      ( *(row -> next)).prev = (row -> prev);
    if ((row -> parent) != 0) 
      ( *(row -> parent)).child = row;
    else 
      tree -> root = row;
    row -> next = s;
/* s cannot be NULL */
    s -> prev = row;
    row -> prev = ((GntTreeRow *)((void *)0));
    newp = (g_list_index((tree -> list),s) - 1);
  }
  else {
    if ((row -> prev) != 0) {
      ( *(row -> prev)).next = (row -> next);
    }
    else {
/* row was the first child of its parent */
      if ((row -> parent) != 0) 
        ( *(row -> parent)).child = (row -> next);
      else 
        tree -> top = (row -> next);
    }
    if ((row -> next) != 0) 
      ( *(row -> next)).prev = (row -> prev);
    q -> next = row;
    row -> prev = q;
    if (s != 0) 
      s -> prev = row;
    row -> next = s;
    newp = (g_list_index((tree -> list),q) + 1);
  }
  tree -> list = g_list_reposition_child((tree -> list),current,newp);
  redraw_tree(tree);
}

GntTreeRow *gnt_tree_add_row_after(GntTree *tree,void *key,GntTreeRow *row,void *parent,void *bigbro)
{
  GntTreeRow *pr = (GntTreeRow *)((void *)0);
  if (g_hash_table_lookup((tree -> hash),key) != 0) {
    gnt_tree_remove(tree,key);
  }
  row -> tree = tree;
  row -> key = key;
  row -> data = ((void *)((void *)0));
  g_hash_table_replace((tree -> hash),key,row);
  if ((bigbro == ((void *)((void *)0))) && (( *(tree -> priv)).compare != 0)) {
    bigbro = find_position(tree,key,parent);
  }
  if ((tree -> root) == ((GntTreeRow *)((void *)0))) {
    tree -> root = row;
    tree -> list = g_list_prepend((tree -> list),key);
  }
  else {
    int position = 0;
    if (bigbro != 0) {
      pr = (g_hash_table_lookup((tree -> hash),bigbro));
      if (pr != 0) {
        if ((pr -> next) != 0) 
          ( *(pr -> next)).prev = row;
        row -> next = (pr -> next);
        row -> prev = pr;
        pr -> next = row;
        row -> parent = (pr -> parent);
        position = g_list_index((tree -> list),bigbro);
      }
    }
    if ((pr == ((GntTreeRow *)((void *)0))) && (parent != 0)) {
      pr = (g_hash_table_lookup((tree -> hash),parent));
      if (pr != 0) {
        if ((pr -> child) != 0) 
          ( *(pr -> child)).prev = row;
        row -> next = (pr -> child);
        pr -> child = row;
        row -> parent = pr;
        position = g_list_index((tree -> list),parent);
      }
    }
    if (pr == ((GntTreeRow *)((void *)0))) {
      GntTreeRow *r = (tree -> root);
      row -> next = r;
      if (r != 0) 
        r -> prev = row;
      if ((tree -> current) == (tree -> root)) 
        tree -> current = row;
      tree -> root = row;
      tree -> list = g_list_prepend((tree -> list),key);
    }
    else {
      tree -> list = g_list_insert((tree -> list),key,(position + 1));
    }
  }
  redraw_tree(tree);
  return row;
}

GntTreeRow *gnt_tree_add_row_last(GntTree *tree,void *key,GntTreeRow *row,void *parent)
{
  GntTreeRow *pr = (GntTreeRow *)((void *)0);
  GntTreeRow *br = (GntTreeRow *)((void *)0);
  if (parent != 0) 
    pr = (g_hash_table_lookup((tree -> hash),parent));
  if (pr != 0) 
    br = (pr -> child);
  else 
    br = (tree -> root);
  if (br != 0) {
    while((br -> next) != 0)
      br = (br -> next);
  }
  return gnt_tree_add_row_after(tree,key,row,parent,((br != 0)?(br -> key) : ((void *)((void *)0))));
}

gpointer gnt_tree_get_selection_data(GntTree *tree)
{
  if ((tree -> current) != 0) 
/* XXX: perhaps we should just get rid of 'data' */
    return ( *(tree -> current)).key;
  return 0;
}

char *gnt_tree_get_selection_text(GntTree *tree)
{
  if ((tree -> current) != 0) 
    return update_row_text(tree,(tree -> current));
  return 0;
}

GList *gnt_tree_get_row_text_list(GntTree *tree,gpointer key)
{
  GList *list = (GList *)((void *)0);
  GList *iter;
  GntTreeRow *row = ((key != 0)?g_hash_table_lookup((tree -> hash),key) : (tree -> current));
  int i;
  if (!(row != 0)) 
    return 0;
  for (((i = 0) , (iter = (row -> columns))); (i < (tree -> ncol)) && (iter != 0); (i++ , (iter = (iter -> next)))) {
    GntTreeCol *col = (iter -> data);
    list = g_list_append(list,(((((tree -> columns)[i].flags & GNT_TREE_COLUMN_BINARY_DATA) != 0U)?(col -> text) : g_strdup((col -> text)))));
  }
  return list;
}

GList *gnt_tree_get_selection_text_list(GntTree *tree)
{
  return gnt_tree_get_row_text_list(tree,0);
}

void gnt_tree_remove(GntTree *tree,gpointer key)
{
  GntTreeRow *row = (g_hash_table_lookup((tree -> hash),key));
/* Only redraw after all child nodes are removed */
  static int depth = 0;
  if (row != 0) {
    gboolean redraw = 0;
    if ((row -> child) != 0) {
      depth++;
      while((row -> child) != 0){
        gnt_tree_remove(tree,( *(row -> child)).key);
      }
      depth--;
    }
    if ((get_distance((tree -> top),row) >= 0) && (get_distance(row,(tree -> bottom)) >= 0)) 
      redraw = 1;
/* Update root/top/current/bottom if necessary */
    if ((tree -> root) == row) 
      tree -> root = get_next(row);
    if ((tree -> top) == row) {
      if ((tree -> top) != (tree -> root)) 
        tree -> top = get_prev(row);
      else 
        tree -> top = get_next(row);
    }
    if ((tree -> current) == row) {
      if ((tree -> current) != (tree -> root)) 
        tree -> current = get_prev(row);
      else 
        tree -> current = get_next(row);
      tree_selection_changed(tree,row,(tree -> current));
    }
    if ((tree -> bottom) == row) {
      tree -> bottom = get_prev(row);
    }
/* Fix the links */
    if ((row -> next) != 0) 
      ( *(row -> next)).prev = (row -> prev);
    if (((row -> parent) != 0) && (( *(row -> parent)).child == row)) 
      ( *(row -> parent)).child = (row -> next);
    if ((row -> prev) != 0) 
      ( *(row -> prev)).next = (row -> next);
    g_hash_table_remove((tree -> hash),key);
    tree -> list = g_list_remove((tree -> list),key);
    if ((redraw != 0) && (depth == 0)) {
      redraw_tree(tree);
    }
  }
}

static gboolean return_true(gpointer key,gpointer data,gpointer null)
{
  return 1;
}

void gnt_tree_remove_all(GntTree *tree)
{
  tree -> root = ((GntTreeRow *)((void *)0));
  g_hash_table_foreach_remove((tree -> hash),((GHRFunc )return_true),tree);
  g_list_free((tree -> list));
  tree -> list = ((GList *)((void *)0));
  tree -> current = (tree -> top = (tree -> bottom = ((GntTreeRow *)((void *)0))));
}

int gnt_tree_get_selection_visible_line(GntTree *tree)
{
  return get_distance((tree -> top),(tree -> current)) + !(!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_NO_BORDER) != 0U));
}

void gnt_tree_change_text(GntTree *tree,gpointer key,int colno,const char *text)
{
  GntTreeRow *row;
  GntTreeCol *col;
  do {
    if (colno < (tree -> ncol)) {
    }
    else {
      g_return_if_fail_warning("Gnt",((const char *)__func__),"colno < tree->ncol");
      return ;
    };
  }while (0);
  row = (g_hash_table_lookup((tree -> hash),key));
  if (row != 0) {
    col = (g_list_nth_data((row -> columns),colno));
    if (((tree -> columns)[colno].flags & GNT_TREE_COLUMN_BINARY_DATA) != 0U) {
      col -> text = ((gpointer )text);
    }
    else {
      g_free((col -> text));
      col -> text = g_strdup(((text != 0)?text : ""));
    }
    if ((((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U) && (get_distance((tree -> top),row) >= 0)) && (get_distance(row,(tree -> bottom)) >= 0)) 
      redraw_tree(tree);
  }
}

GntTreeRow *gnt_tree_add_choice(GntTree *tree,void *key,GntTreeRow *row,void *parent,void *bigbro)
{
  GntTreeRow *r;
  r = (g_hash_table_lookup((tree -> hash),key));
  do {
    if (!(r != 0) || !((r -> choice) != 0)) {
    }
    else {
      g_return_if_fail_warning("Gnt",((const char *)__func__),"!r || !r->choice");
      return 0;
    };
  }while (0);
  if (bigbro == ((void *)((void *)0))) {
    if (( *(tree -> priv)).compare != 0) 
      bigbro = find_position(tree,key,parent);
    else {
      r = (g_hash_table_lookup((tree -> hash),parent));
      if (!(r != 0)) 
        r = (tree -> root);
      else 
        r = (r -> child);
      if (r != 0) {
        while((r -> next) != 0)
          r = (r -> next);
        bigbro = (r -> key);
      }
    }
  }
  row = gnt_tree_add_row_after(tree,key,row,parent,bigbro);
  row -> choice = 1;
  return row;
}

void gnt_tree_set_choice(GntTree *tree,void *key,gboolean set)
{
  GntTreeRow *row = (g_hash_table_lookup((tree -> hash),key));
  if (!(row != 0)) 
    return ;
  do {
    if ((row -> choice) != 0) {
    }
    else {
      g_return_if_fail_warning("Gnt",((const char *)__func__),"row->choice");
      return ;
    };
  }while (0);
  row -> isselected = set;
  redraw_tree(tree);
}

gboolean gnt_tree_get_choice(GntTree *tree,void *key)
{
  GntTreeRow *row = (g_hash_table_lookup((tree -> hash),key));
  if (!(row != 0)) 
    return 0;
  do {
    if ((row -> choice) != 0) {
    }
    else {
      g_return_if_fail_warning("Gnt",((const char *)__func__),"row->choice");
      return 0;
    };
  }while (0);
  return row -> isselected;
}

void gnt_tree_set_row_flags(GntTree *tree,void *key,GntTextFormatFlags flags)
{
  GntTreeRow *row = (g_hash_table_lookup((tree -> hash),key));
  if (!(row != 0) || ((row -> flags) == flags)) 
    return ;
  row -> flags = flags;
/* XXX: It shouldn't be necessary to redraw the whole darned tree */
  redraw_tree(tree);
}

void gnt_tree_set_row_color(GntTree *tree,void *key,int color)
{
  GntTreeRow *row = (g_hash_table_lookup((tree -> hash),key));
  if (!(row != 0) || ((row -> color) == color)) 
    return ;
  row -> color = color;
  redraw_tree(tree);
}

void gnt_tree_set_selected(GntTree *tree,void *key)
{
  int dist;
  GntTreeRow *row = (g_hash_table_lookup((tree -> hash),key));
  if (!(row != 0) || (row == (tree -> current))) 
    return ;
  if ((tree -> top) == ((GntTreeRow *)((void *)0))) 
    tree -> top = row;
  if ((tree -> bottom) == ((GntTreeRow *)((void *)0))) 
    tree -> bottom = row;
  tree -> current = row;
  if ((dist = get_distance((tree -> current),(tree -> bottom))) < 0) 
    gnt_tree_scroll(tree,-dist);
  else if ((dist = get_distance((tree -> current),(tree -> top))) > 0) 
    gnt_tree_scroll(tree,-dist);
  else 
    redraw_tree(tree);
  tree_selection_changed(tree,row,(tree -> current));
}

static void _gnt_tree_init_internals(GntTree *tree,int col)
{
  gnt_tree_free_columns(tree);
  tree -> ncol = col;
  tree -> hash = g_hash_table_new_full(g_direct_hash,g_direct_equal,0,free_tree_row);
  tree -> columns = ((struct _GntTreeColInfo *)(g_malloc0_n(col,(sizeof(struct _GntTreeColInfo )))));
  ( *(tree -> priv)).lastvisible = (col - 1);
  while(col-- != 0){
    (tree -> columns)[col].width = 15;
  }
  tree -> list = ((GList *)((void *)0));
  tree -> show_title = 0;
  g_object_notify(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"columns");
}

GntWidget *gnt_tree_new_with_columns(int col)
{
  GntWidget *widget = (g_object_new(gnt_tree_get_gtype(),"columns",col,"expander-level",1,((void *)((void *)0))));
  return widget;
}

GntTreeRow *gnt_tree_create_row_from_list(GntTree *tree,GList *list)
{
  GList *iter;
  int i;
  GntTreeRow *row = (GntTreeRow *)(g_malloc0_n(1,(sizeof(GntTreeRow ))));
  for (((i = 0) , (iter = list)); (i < (tree -> ncol)) && (iter != 0); ((iter = (iter -> next)) , i++)) {
    GntTreeCol *col = (GntTreeCol *)(g_malloc0_n(1,(sizeof(GntTreeCol ))));
    col -> span = 1;
    if (((tree -> columns)[i].flags & GNT_TREE_COLUMN_BINARY_DATA) != 0U) {
      col -> text = (iter -> data);
      col -> isbinary = 1;
    }
    else {
      col -> text = g_strdup(((((iter -> data) != 0)?(iter -> data) : "")));
      col -> isbinary = 0;
    }
    row -> columns = g_list_append((row -> columns),col);
  }
  return row;
}

GntTreeRow *gnt_tree_create_row(GntTree *tree,... )
{
  int i;
  va_list args;
  GList *list = (GList *)((void *)0);
  GntTreeRow *row;
  va_start(args,tree);
  for (i = 0; i < (tree -> ncol); i++) {
    list = g_list_append(list,(va_arg(args,char *)));
  }
  va_end(args);
  row = gnt_tree_create_row_from_list(tree,list);
  g_list_free(list);
  return row;
}

void gnt_tree_set_col_width(GntTree *tree,int col,int width)
{
  do {
    if (col < (tree -> ncol)) {
    }
    else {
      g_return_if_fail_warning("Gnt",((const char *)__func__),"col < tree->ncol");
      return ;
    };
  }while (0);
  (tree -> columns)[col].width = width;
  if ((tree -> columns)[col].width_ratio == 0) 
    (tree -> columns)[col].width_ratio = width;
}

void gnt_tree_set_column_title(GntTree *tree,int index,const char *title)
{
  g_free((tree -> columns)[index].title);
  (tree -> columns)[index].title = g_strdup(title);
}

void gnt_tree_set_column_titles(GntTree *tree,... )
{
  int i;
  va_list args;
  va_start(args,tree);
  for (i = 0; i < (tree -> ncol); i++) {
    const char *title = va_arg(args,const char *);
    (tree -> columns)[i].title = g_strdup(title);
  }
  va_end(args);
}

void gnt_tree_set_show_title(GntTree *tree,gboolean set)
{
  tree -> show_title = set;
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype())))).priv.minh = ((set != 0)?6 : 4);
}

void gnt_tree_set_compare_func(GntTree *tree,GCompareFunc func)
{
  ( *(tree -> priv)).compare = func;
}

void gnt_tree_set_expanded(GntTree *tree,void *key,gboolean expanded)
{
  GntTreeRow *row = (g_hash_table_lookup((tree -> hash),key));
  if (row != 0) {
    row -> collapsed = !(expanded != 0);
    if (( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype())))).window != 0) 
      gnt_widget_draw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype()))));
    g_signal_emit(tree,signals[SIG_COLLAPSED],0,key,(row -> collapsed));
  }
}

void gnt_tree_set_show_separator(GntTree *tree,gboolean set)
{
  tree -> show_separator = set;
}

void gnt_tree_adjust_columns(GntTree *tree)
{
  GntTreeRow *row = (tree -> root);
  int *widths;
  int i;
  int twidth;
  widths = ((int *)(g_malloc0_n((tree -> ncol),(sizeof(int )))));
  while(row != 0){
    GList *iter;
    for (((i = 0) , (iter = (row -> columns))); iter != 0; ((iter = (iter -> next)) , i++)) {
      GntTreeCol *col = (iter -> data);
      int w = gnt_util_onscreen_width((col -> text),0);
      if ((i == 0) && ((row -> choice) != 0)) 
        w += 4;
      if (i == 0) {
        w += (find_depth(row) * 3);
      }
      if (widths[i] < w) 
        widths[i] = w;
    }
    row = get_next(row);
  }
  twidth = (1 + (2 * !((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_NO_BORDER) != 0U)));
  for (i = 0; i < (tree -> ncol); i++) {
    if (((tree -> columns)[i].flags & GNT_TREE_COLUMN_FIXED_SIZE) != 0U) 
      widths[i] = (tree -> columns)[i].width;
    gnt_tree_set_col_width(tree,i,widths[i]);
    if (!(((tree -> columns)[i].flags & GNT_TREE_COLUMN_INVISIBLE) != 0U)) {
      twidth = (twidth + widths[i]);
      if (( *(tree -> priv)).lastvisible != i) 
        twidth += 1;
    }
  }
  g_free(widths);
  gnt_widget_set_size(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype()))),twidth,-1);
}

void gnt_tree_set_hash_fns(GntTree *tree,gpointer hash,gpointer eq,gpointer kd)
{
  g_hash_table_foreach_remove((tree -> hash),return_true,0);
  g_hash_table_destroy((tree -> hash));
  tree -> hash = g_hash_table_new_full(hash,eq,kd,free_tree_row);
}

static void set_column_flag(GntTree *tree,int col,GntTreeColumnFlag flag,gboolean set)
{
  if (set != 0) 
    (tree -> columns)[col].flags |= flag;
  else 
    (tree -> columns)[col].flags &= (~flag);
}

void gnt_tree_set_column_visible(GntTree *tree,int col,gboolean vis)
{
  do {
    if (col < (tree -> ncol)) {
    }
    else {
      g_return_if_fail_warning("Gnt",((const char *)__func__),"col < tree->ncol");
      return ;
    };
  }while (0);
  set_column_flag(tree,col,GNT_TREE_COLUMN_INVISIBLE,!(vis != 0));
  if (vis != 0) {
/* the column is visible */
    if (( *(tree -> priv)).lastvisible < col) 
      ( *(tree -> priv)).lastvisible = col;
  }
  else {
    if (( *(tree -> priv)).lastvisible == col) {
      while(( *(tree -> priv)).lastvisible != 0){
        ( *(tree -> priv)).lastvisible--;
        if (!(((tree -> columns)[( *(tree -> priv)).lastvisible].flags & GNT_TREE_COLUMN_INVISIBLE) != 0U)) 
          break; 
      }
    }
  }
  if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U) 
    readjust_columns(tree);
}

void gnt_tree_set_column_resizable(GntTree *tree,int col,gboolean res)
{
  do {
    if (col < (tree -> ncol)) {
    }
    else {
      g_return_if_fail_warning("Gnt",((const char *)__func__),"col < tree->ncol");
      return ;
    };
  }while (0);
  set_column_flag(tree,col,GNT_TREE_COLUMN_FIXED_SIZE,!(res != 0));
}

void gnt_tree_set_column_is_binary(GntTree *tree,int col,gboolean bin)
{
  do {
    if (col < (tree -> ncol)) {
    }
    else {
      g_return_if_fail_warning("Gnt",((const char *)__func__),"col < tree->ncol");
      return ;
    };
  }while (0);
  set_column_flag(tree,col,GNT_TREE_COLUMN_BINARY_DATA,bin);
}

void gnt_tree_set_column_is_right_aligned(GntTree *tree,int col,gboolean right)
{
  do {
    if (col < (tree -> ncol)) {
    }
    else {
      g_return_if_fail_warning("Gnt",((const char *)__func__),"col < tree->ncol");
      return ;
    };
  }while (0);
  set_column_flag(tree,col,GNT_TREE_COLUMN_RIGHT_ALIGNED,right);
}

void gnt_tree_set_column_width_ratio(GntTree *tree,int cols[])
{
  int i;
  for (i = 0; (i < (tree -> ncol)) && (cols[i] != 0); i++) {
    (tree -> columns)[i].width_ratio = cols[i];
  }
}

void gnt_tree_set_search_column(GntTree *tree,int col)
{
  do {
    if (col < (tree -> ncol)) {
    }
    else {
      g_return_if_fail_warning("Gnt",((const char *)__func__),"col < tree->ncol");
      return ;
    };
  }while (0);
  do {
    if (!(((tree -> columns)[col].flags & GNT_TREE_COLUMN_BINARY_DATA) != 0U)) {
    }
    else {
      g_return_if_fail_warning("Gnt",((const char *)__func__),"!BINARY_DATA(tree, col)");
      return ;
    };
  }while (0);
  ( *(tree -> priv)).search_column = col;
}

gboolean gnt_tree_is_searching(GntTree *tree)
{
  return ( *(tree -> priv)).search != ((GString *)((void *)0));
}

void gnt_tree_set_search_function(GntTree *tree,gboolean (*func)(GntTree *, gpointer , const char *, const char *))
{
  ( *(tree -> priv)).search_func = func;
}

gpointer gnt_tree_get_parent_key(GntTree *tree,gpointer key)
{
  GntTreeRow *row = (g_hash_table_lookup((tree -> hash),key));
  return ((row != 0) && ((row -> parent) != 0))?( *(row -> parent)).key : ((void *)((void *)0));
}

gpointer gnt_tree_row_get_key(GntTree *tree,GntTreeRow *row)
{
  do {
    if ((row != 0) && ((row -> tree) == tree)) {
    }
    else {
      g_return_if_fail_warning("Gnt",((const char *)__func__),"row && row->tree == tree");
      return 0;
    };
  }while (0);
  return row -> key;
}

GntTreeRow *gnt_tree_row_get_next(GntTree *tree,GntTreeRow *row)
{
  do {
    if ((row != 0) && ((row -> tree) == tree)) {
    }
    else {
      g_return_if_fail_warning("Gnt",((const char *)__func__),"row && row->tree == tree");
      return 0;
    };
  }while (0);
  return row -> next;
}

GntTreeRow *gnt_tree_row_get_prev(GntTree *tree,GntTreeRow *row)
{
  do {
    if ((row != 0) && ((row -> tree) == tree)) {
    }
    else {
      g_return_if_fail_warning("Gnt",((const char *)__func__),"row && row->tree == tree");
      return 0;
    };
  }while (0);
  return row -> prev;
}

GntTreeRow *gnt_tree_row_get_child(GntTree *tree,GntTreeRow *row)
{
  do {
    if ((row != 0) && ((row -> tree) == tree)) {
    }
    else {
      g_return_if_fail_warning("Gnt",((const char *)__func__),"row && row->tree == tree");
      return 0;
    };
  }while (0);
  return row -> child;
}

GntTreeRow *gnt_tree_row_get_parent(GntTree *tree,GntTreeRow *row)
{
  do {
    if ((row != 0) && ((row -> tree) == tree)) {
    }
    else {
      g_return_if_fail_warning("Gnt",((const char *)__func__),"row && row->tree == tree");
      return 0;
    };
  }while (0);
  return row -> parent;
}
