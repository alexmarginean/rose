/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <string.h>
#include "gntinternal.h"
#undef GNT_LOG_DOMAIN
#define GNT_LOG_DOMAIN "Bindable"
#include "gntbindable.h"
#include "gntstyle.h"
#include "gnt.h"
#include "gntutils.h"
#include "gnttextview.h"
#include "gnttree.h"
#include "gntbox.h"
#include "gntbutton.h"
#include "gntwindow.h"
#include "gntlabel.h"
static GObjectClass *parent_class = (GObjectClass *)((void *)0);
static struct __unnamed_class___F0_L42_C8_unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__okeys__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__keys__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L243R__Pe___variable_name_unknown_scope_and_name__scope__klass__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__name__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GList_GList__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__params {
/* Old keystrokes */
char *okeys;
/* New Keystrokes being bound to the action */
char *keys;
/* Class of the object that's getting keys rebound */
GntBindableClass *klass;
/* The name of the action */
char *name;
/* The list of paramaters */
GList *params;}rebind_info;

static void gnt_bindable_free_rebind_info()
{
  g_free(rebind_info.name);
  g_free(rebind_info.keys);
  g_free(rebind_info.okeys);
}

static void gnt_bindable_rebinding_cancel(GntWidget *button,gpointer data)
{
  gnt_bindable_free_rebind_info();
  gnt_widget_destroy(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)data),gnt_widget_get_gtype()))));
}

static void gnt_bindable_rebinding_rebind(GntWidget *button,gpointer data)
{
  if (rebind_info.keys != 0) {
    gnt_bindable_register_binding(rebind_info.klass,0,rebind_info.okeys,rebind_info.params);
    gnt_bindable_register_binding(rebind_info.klass,rebind_info.name,rebind_info.keys,rebind_info.params);
  }
  gnt_bindable_free_rebind_info();
  gnt_widget_destroy(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)data),gnt_widget_get_gtype()))));
}

static gboolean gnt_bindable_rebinding_grab_key(GntBindable *bindable,const char *text,gpointer data)
{
  GntTextView *textview = (GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)data),gnt_text_view_get_gtype()));
  char *new_text;
  const char *tmp;
  if ((text != 0) && (( *text) != 0)) {
/* Rebinding tab or enter for something is probably not that great an idea */
    if (!(strcmp(text,"\t") != 0) || !(strcmp(text,cur_term -> type.Strings[2]) != 0)) {
      return 0;
    }
    tmp = gnt_key_lookup(text);
    new_text = g_strdup_printf("KEY: \"%s\"",tmp);
    gnt_text_view_clear(textview);
    gnt_text_view_append_text_with_flags(textview,new_text,GNT_TEXT_FLAG_NORMAL);
    g_free(new_text);
    g_free(rebind_info.keys);
    rebind_info.keys = g_strdup(text);
    return 1;
  }
  return 0;
}

static void gnt_bindable_rebinding_activate(GntBindable *data,gpointer bindable)
{
  const char *widget_name = g_type_name(( *((GTypeClass *)( *((GTypeInstance *)bindable)).g_class)).g_type);
  char *keys;
  GntWidget *key_textview;
  GntWidget *label;
  GntWidget *bind_button;
  GntWidget *cancel_button;
  GntWidget *button_box;
  GList *current_row_data;
  char *tmp;
  GntWidget *win = gnt_window_new();
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)data),gnt_tree_get_gtype()));
  GntWidget *vbox = gnt_box_new(0,1);
  rebind_info.klass = ((GntBindableClass *)( *((GTypeInstance *)bindable)).g_class);
  current_row_data = gnt_tree_get_selection_text_list(tree);
  rebind_info.name = g_strdup((g_list_nth_data(current_row_data,1)));
  keys = (gnt_tree_get_selection_data(tree));
  rebind_info.okeys = g_strdup(gnt_key_translate(keys));
  rebind_info.params = ((GList *)((void *)0));
  g_list_foreach(current_row_data,((GFunc )g_free),0);
  g_list_free(current_row_data);
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),GNT_ALIGN_MID);
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),"Key Capture");
  tmp = g_strdup_printf("Type the new bindings for %s in a %s.",rebind_info.name,widget_name);
  label = gnt_label_new(tmp);
  g_free(tmp);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),label);
  tmp = g_strdup_printf("KEY: \"%s\"",keys);
  key_textview = gnt_text_view_new();
  gnt_widget_set_size(key_textview,key_textview -> priv.x,2);
  gnt_text_view_append_text_with_flags(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)key_textview),gnt_text_view_get_gtype()))),tmp,GNT_TEXT_FLAG_NORMAL);
  g_free(tmp);
  gnt_widget_set_name(key_textview,"keystroke");
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),key_textview);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"key_pressed",((GCallback )gnt_bindable_rebinding_grab_key),key_textview,0,((GConnectFlags )0));
  button_box = gnt_box_new(0,0);
  bind_button = gnt_button_new("BIND");
  gnt_widget_set_name(bind_button,"bind");
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)button_box),gnt_box_get_gtype()))),bind_button);
  cancel_button = gnt_button_new("Cancel");
  gnt_widget_set_name(cancel_button,"cancel");
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)button_box),gnt_box_get_gtype()))),cancel_button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)bind_button),((GType )(20 << 2))))),"activate",((GCallback )gnt_bindable_rebinding_rebind),win,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)cancel_button),((GType )(20 << 2))))),"activate",((GCallback )gnt_bindable_rebinding_cancel),win,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),button_box);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),vbox);
  gnt_widget_show(win);
}
typedef struct __unnamed_class___F0_L175_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L54R__Pe___variable_name_unknown_scope_and_name__scope__hash__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntTree_GntTree__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__tree {
GHashTable *hash;
GntTree *tree;}BindingView;

static void add_binding(gpointer key,gpointer value,gpointer data)
{
  BindingView *bv = data;
  GntBindableActionParam *act = value;
  const char *name = (g_hash_table_lookup((bv -> hash),(act -> action)));
  if ((name != 0) && (( *name) != 0)) {
    const char *k = gnt_key_lookup(key);
    if (!(k != 0)) 
      k = key;
    gnt_tree_add_row_after((bv -> tree),((gpointer )k),gnt_tree_create_row((bv -> tree),k,name),0,0);
  }
}

static void add_action(gpointer key,gpointer value,gpointer data)
{
  BindingView *bv = data;
  g_hash_table_insert((bv -> hash),value,key);
}

static void gnt_bindable_class_init(GntBindableClass *klass)
{
  parent_class = (g_type_class_peek_parent(klass));
  klass -> actions = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,((GDestroyNotify )gnt_bindable_action_free));
  klass -> bindings = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,((GDestroyNotify )gnt_bindable_action_param_free));
  gnt_style_read_actions(( *((GTypeClass *)klass)).g_type,((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))));;
}

static gpointer bindable_clone(GntBindableAction *action)
{
  GntBindableAction *ret = (GntBindableAction *)(g_malloc0_n(1,(sizeof(GntBindableAction ))));
  ret -> name = g_strdup((action -> name));
  ret -> u = (action -> u);
  return ret;
}

static gpointer binding_clone(GntBindableActionParam *param)
{
  GntBindableActionParam *p = (GntBindableActionParam *)(g_malloc0_n(1,(sizeof(GntBindableActionParam ))));
  p -> list = g_list_copy((param -> list));
  p -> action = (param -> action);
  return p;
}

static void duplicate_hashes(GntBindableClass *klass)
{
/* Duplicate the bindings from parent class */
  if ((klass -> actions) != 0) {
    klass -> actions = g_hash_table_duplicate((klass -> actions),g_str_hash,g_str_equal,g_free,((GDestroyNotify )gnt_bindable_action_free),((GDupFunc )g_strdup),((GDupFunc )bindable_clone));
    klass -> bindings = g_hash_table_duplicate((klass -> bindings),g_str_hash,g_str_equal,g_free,((GDestroyNotify )gnt_bindable_action_param_free),((GDupFunc )g_strdup),((GDupFunc )binding_clone));
  }
  else {
    klass -> actions = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,((GDestroyNotify )gnt_bindable_action_free));
    klass -> bindings = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,((GDestroyNotify )gnt_bindable_action_param_free));
  };
}
/******************************************************************************
 * GntBindable API
 *****************************************************************************/

GType gnt_bindable_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(GntBindableClass ))), ((GBaseInitFunc )duplicate_hashes), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gnt_bindable_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GntBindable ))), (0), ((GInstanceInitFunc )((void *)0)), ((const GTypeValueTable *)((void *)0))
/* base_init		*/
/* base_finalize	*/
/* class_data		*/
/* n_preallocs		*/
/* instance_init	*/
/* value_table		*/
};
    type = g_type_register_static(((GType )(20 << 2)),"GntBindable",&info,G_TYPE_FLAG_ABSTRACT);
  }
  return type;
}
/**
 * Key Remaps
 */

const char *gnt_bindable_remap_keys(GntBindable *bindable,const char *text)
{
  const char *remap = (const char *)((void *)0);
  GType type = ( *((GTypeClass *)( *((GTypeInstance *)bindable)).g_class)).g_type;
  GntBindableClass *klass = (GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)((GntBindableClass *)( *((GTypeInstance *)bindable)).g_class)),gnt_bindable_get_gtype()));
  if ((klass -> remaps) == ((GHashTable *)((void *)0))) {
    klass -> remaps = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
    gnt_styles_get_keyremaps(type,(klass -> remaps));
  }
  remap = (g_hash_table_lookup((klass -> remaps),text));
  return (remap != 0)?remap : text;
}
/**
 * Actions and Bindings
 */

gboolean gnt_bindable_perform_action_named(GntBindable *bindable,const char *name,... )
{
  GntBindableClass *klass = (GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)((GntBindableClass *)( *((GTypeInstance *)bindable)).g_class)),gnt_bindable_get_gtype()));
  GList *list = (GList *)((void *)0);
  va_list args;
  GntBindableAction *action;
  void *p;
  va_start(args,name);
  while((p = (va_arg(args,void *))) != ((void *)((void *)0)))
    list = g_list_append(list,p);
  va_end(args);
  action = (g_hash_table_lookup((klass -> actions),name));
  if ((action != 0) && (action -> u.action != 0)) {
    return ( *action -> u.action)(bindable,list);
  }
  return 0;
}

gboolean gnt_bindable_perform_action_key(GntBindable *bindable,const char *keys)
{
  GntBindableClass *klass = (GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)((GntBindableClass *)( *((GTypeInstance *)bindable)).g_class)),gnt_bindable_get_gtype()));
  GntBindableActionParam *param = (g_hash_table_lookup((klass -> bindings),keys));
  if ((param != 0) && ((param -> action) != 0)) {
    if ((param -> list) != 0) 
      return ( *( *(param -> action)).u.action)(bindable,(param -> list));
    else 
      return ( *( *(param -> action)).u.action_noparam)(bindable);
  }
  return 0;
}

gboolean gnt_bindable_check_key(GntBindable *bindable,const char *keys)
{
  GntBindableClass *klass = (GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)((GntBindableClass *)( *((GTypeInstance *)bindable)).g_class)),gnt_bindable_get_gtype()));
  GntBindableActionParam *param = (g_hash_table_lookup((klass -> bindings),keys));
  return (param != 0) && ((param -> action) != 0);
}

static void register_binding(GntBindableClass *klass,const char *name,const char *trigger,GList *list)
{
  GntBindableActionParam *param;
  GntBindableAction *action;
  if ((name == ((const char *)((void *)0))) || (( *name) == 0)) {
    g_hash_table_remove((klass -> bindings),((char *)trigger));
    gnt_keys_del_combination(trigger);
    return ;
  }
  action = (g_hash_table_lookup((klass -> actions),name));
  if (!(action != 0)) {
    g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: Invalid action name %s for %s","Bindable","void register_binding(struct _GntBindableClass *, const char *, const char *, struct _GList *)",name,g_type_name(( *((GTypeClass *)klass)).g_type));
    if (list != 0) 
      g_list_free(list);
    return ;
  }
  param = ((GntBindableActionParam *)(g_malloc0_n(1,(sizeof(GntBindableActionParam )))));
  param -> action = action;
  param -> list = list;
  g_hash_table_replace((klass -> bindings),(g_strdup(trigger)),param);
  gnt_keys_add_combination(trigger);
}

void gnt_bindable_register_binding(GntBindableClass *klass,const char *name,const char *trigger,... )
{
  GList *list = (GList *)((void *)0);
  va_list args;
  void *data;
  va_start(args,trigger);
  while((data = (va_arg(args,void *))) != 0){
    list = g_list_append(list,data);
  }
  va_end(args);
  register_binding(klass,name,trigger,list);
}

void gnt_bindable_class_register_action(GntBindableClass *klass,const char *name,GntBindableActionCallback callback,const char *trigger,... )
{
  void *data;
  va_list args;
  GntBindableAction *action = (GntBindableAction *)(g_malloc0_n(1,(sizeof(GntBindableAction ))));
  GList *list;
  action -> name = g_strdup(name);
  action -> u.action = callback;
  g_hash_table_replace((klass -> actions),(g_strdup(name)),action);
  if ((trigger != 0) && (( *trigger) != 0)) {
    list = ((GList *)((void *)0));
    va_start(args,trigger);
    while((data = (va_arg(args,void *))) != 0){
      list = g_list_append(list,data);
    }
    va_end(args);
    register_binding(klass,name,trigger,list);
  }
}

void gnt_bindable_action_free(GntBindableAction *action)
{
  g_free((action -> name));
  g_free(action);
}

void gnt_bindable_action_param_free(GntBindableActionParam *param)
{
/* XXX: There may be a leak here for string parameters */
  g_list_free((param -> list));
  g_free(param);
}

GntBindable *gnt_bindable_bindings_view(GntBindable *bind)
{
  GntBindable *tree = (GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)(gnt_tree_new_with_columns(2))),gnt_bindable_get_gtype()));
  GntBindableClass *klass = (GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)((GntBindableClass *)( *((GTypeInstance *)bind)).g_class)),gnt_bindable_get_gtype()));
  GHashTable *hash = g_hash_table_new(g_direct_hash,g_direct_equal);
  BindingView bv = {(hash), ((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype())))};
  gnt_tree_set_compare_func(bv.tree,((GCompareFunc )g_utf8_collate));
  g_hash_table_foreach((klass -> actions),add_action,(&bv));
  g_hash_table_foreach((klass -> bindings),add_binding,(&bv));
  if (( *((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype())))).list == ((GList *)((void *)0))) {
    gnt_widget_destroy(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype()))));
    tree = ((GntBindable *)((void *)0));
  }
  else 
    gnt_tree_adjust_columns(bv.tree);
  g_hash_table_destroy(hash);
  return tree;
}

static void reset_binding_window(GntBindableClass *window,gpointer k)
{
  GntBindableClass *klass = (GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)k),gnt_bindable_get_gtype()));
  klass -> help_window = ((GntBindable *)((void *)0));
}

gboolean gnt_bindable_build_help_window(GntBindable *bindable)
{
  GntWidget *tree;
  GntBindableClass *klass = (GntBindableClass *)( *((GTypeInstance *)bindable)).g_class;
  char *title;
  tree = ((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(gnt_bindable_bindings_view(bindable))),gnt_widget_get_gtype())));
  klass -> help_window = ((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)(gnt_window_new())),gnt_bindable_get_gtype())));
  title = g_strdup_printf("Bindings for %s",g_type_name(( *((GTypeClass *)( *((GTypeInstance *)bindable)).g_class)).g_type));
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(klass -> help_window)),gnt_box_get_gtype()))),title);
  if (tree != 0) {
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"activate",((GCallback )gnt_bindable_rebinding_activate),bindable,0,((GConnectFlags )0));
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(klass -> help_window)),gnt_box_get_gtype()))),tree);
  }
  else 
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(klass -> help_window)),gnt_box_get_gtype()))),gnt_label_new("This widget has no customizable bindings."));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(klass -> help_window)),((GType )(20 << 2))))),"destroy",((GCallback )reset_binding_window),klass,0,((GConnectFlags )0));
  gnt_widget_show(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(klass -> help_window)),gnt_widget_get_gtype()))));
  g_free(title);
  return 1;
}
