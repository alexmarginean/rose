/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "gntinternal.h"
#include "gntline.h"
enum __unnamed_enum___F0_L26_C1_PROP_0__COMMA__PROP_VERTICAL {PROP_0,PROP_VERTICAL};
enum __unnamed_enum___F0_L32_C1_SIGS {SIGS=1};
static GntWidgetClass *parent_class = (GntWidgetClass *)((void *)0);

static void gnt_line_draw(GntWidget *widget)
{
  GntLine *line = (GntLine *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_line_get_gtype()));
  if ((line -> vertical) != 0) 
    (wmove((widget -> window),1,0) == -1)?-1 : wvline((widget -> window),(acs_map[(unsigned char )'x'] | (gnt_color_pair(GNT_COLOR_NORMAL))),(widget -> priv.height - 2));
  else 
    (wmove((widget -> window),0,1) == -1)?-1 : whline((widget -> window),(acs_map[(unsigned char )'q'] | (gnt_color_pair(GNT_COLOR_NORMAL))),(widget -> priv.width - 2));
}

static void gnt_line_size_request(GntWidget *widget)
{
  if (( *((GntLine *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_line_get_gtype())))).vertical != 0) {
    widget -> priv.width = 1;
    widget -> priv.height = 5;
  }
  else {
    widget -> priv.width = 5;
    widget -> priv.height = 1;
  }
}

static void gnt_line_map(GntWidget *widget)
{
  if ((widget -> priv.width == 0) || (widget -> priv.height == 0)) 
    gnt_widget_size_request(widget);;
}

static void gnt_line_set_property(GObject *obj,guint prop_id,const GValue *value,GParamSpec *spec)
{
  GntLine *line = (GntLine *)(g_type_check_instance_cast(((GTypeInstance *)obj),gnt_line_get_gtype()));
  switch(prop_id){
    case PROP_VERTICAL:
{
      line -> vertical = g_value_get_boolean(value);
      if ((line -> vertical) != 0) {
        ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)line),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_GROW_Y;
      }
      else {
        ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)line),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_GROW_X;
      }
      break; 
    }
    default:
{
      break; 
    }
  }
}

static void gnt_line_get_property(GObject *obj,guint prop_id,GValue *value,GParamSpec *spec)
{
  GntLine *line = (GntLine *)(g_type_check_instance_cast(((GTypeInstance *)obj),gnt_line_get_gtype()));
  switch(prop_id){
    case PROP_VERTICAL:
{
      g_value_set_boolean(value,(line -> vertical));
      break; 
    }
    default:
{
      break; 
    }
  }
}

static void gnt_line_class_init(GntLineClass *klass)
{
  GObjectClass *gclass = (GObjectClass *)(g_type_check_class_cast(((GTypeClass *)klass),((GType )(20 << 2))));
  parent_class = ((GntWidgetClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_widget_get_gtype())));
  parent_class -> draw = gnt_line_draw;
  parent_class -> map = gnt_line_map;
  parent_class -> size_request = gnt_line_size_request;
  gclass -> set_property = gnt_line_set_property;
  gclass -> get_property = gnt_line_get_property;
  g_object_class_install_property(gclass,PROP_VERTICAL,g_param_spec_boolean("vertical","Vertical","Whether it\'s a vertical line or a horizontal one.",1,(G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB)));
}

static void gnt_line_init(GTypeInstance *instance,gpointer class)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_widget_get_gtype()));
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= (GNT_WIDGET_NO_SHADOW | GNT_WIDGET_NO_BORDER);
  widget -> priv.minw = 1;
  widget -> priv.minh = 1;;
}
/******************************************************************************
 * GntLine API
 *****************************************************************************/

GType gnt_line_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(GntLineClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gnt_line_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GntLine ))), (0), (gnt_line_init), ((const GTypeValueTable *)((void *)0))
/* base_init		*/
/* base_finalize	*/
/* class_finalize	*/
/* class_data		*/
/* n_preallocs		*/
/* instance_init	*/
/* value_table		*/
};
    type = g_type_register_static(gnt_widget_get_gtype(),"GntLine",&info,0);
  }
  return type;
}

GntWidget *gnt_line_new(gboolean vertical)
{
  GntWidget *widget = (g_object_new(gnt_line_get_gtype(),"vertical",vertical,((void *)((void *)0))));
  return widget;
}
