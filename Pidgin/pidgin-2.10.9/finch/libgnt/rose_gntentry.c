/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <ctype.h>
#include <string.h>
#include "gntinternal.h"
#include "gntbox.h"
#include "gntentry.h"
#include "gntmarshal.h"
#include "gntstyle.h"
#include "gnttree.h"
#include "gntutils.h"
enum __unnamed_enum___F0_L34_C1_SIG_TEXT_CHANGED__COMMA__SIG_COMPLETION__COMMA__SIGS {SIG_TEXT_CHANGED,SIG_COMPLETION,SIGS};
typedef enum __unnamed_enum___F0_L41_C9_ENTRY_JAIL__COMMA__ENTRY_DEL_BWD_WORD__COMMA__ENTRY_DEL_BWD_CHAR__COMMA__ENTRY_DEL_FWD_WORD__COMMA__ENTRY_DEL_FWD_CHAR__COMMA__ENTRY_DEL_EOL__COMMA__ENTRY_DEL_BOL {
/* Suspend the kill ring. */
ENTRY_JAIL=-1,ENTRY_DEL_BWD_WORD=1,ENTRY_DEL_BWD_CHAR,ENTRY_DEL_FWD_WORD,ENTRY_DEL_FWD_CHAR,ENTRY_DEL_EOL,ENTRY_DEL_BOL}GntEntryAction;

struct _GntEntryKillRing 
{
  GString *buffer;
  GntEntryAction last;
}
;

struct _GntEntrySearch 
{
  char *needle;
}
;
static guint signals[2UL] = {(0)};
static GntWidgetClass *parent_class = (GntWidgetClass *)((void *)0);
static gboolean gnt_entry_key_pressed(GntWidget *widget,const char *text);
static void gnt_entry_set_text_internal(GntEntry *entry,const char *text);

static gboolean update_kill_ring(GntEntry *entry,GntEntryAction action,const char *text,int len)
{
  if (action < 0) {
    ( *(entry -> killring)).last = action;
    return 0;
  }
  if (len == 0) 
    len = (strlen(text));
  else if (len < 0) {
    text += len;
    len = -len;
  }
  if (action != ( *(entry -> killring)).last) {
    struct __unnamed_class___F0_L86_C3_L294R_variable_declaration__variable_type_GntEntryActionL291R__typedef_declaration_variable_name_L294R__scope__one__DELIMITER__L294R_variable_declaration__variable_type_GntEntryActionL291R__typedef_declaration_variable_name_L294R__scope__two {
    GntEntryAction one;
    GntEntryAction two;}merges[] = {{(ENTRY_DEL_BWD_WORD), (ENTRY_DEL_FWD_WORD)}, {(ENTRY_DEL_BWD_CHAR), (ENTRY_DEL_FWD_CHAR)}, {(ENTRY_DEL_BOL), (ENTRY_DEL_EOL)}, {(ENTRY_JAIL), (ENTRY_JAIL)}};
    int i;
{
      for (i = 0; merges[i].one != ENTRY_JAIL; i++) {
        if ((merges[i].one == ( *(entry -> killring)).last) && (merges[i].two == action)) {
          g_string_append_len(( *(entry -> killring)).buffer,text,len);
          break; 
        }
        else if ((merges[i].one == action) && (merges[i].two == ( *(entry -> killring)).last)) {
          g_string_prepend_len(( *(entry -> killring)).buffer,text,len);
          break; 
        }
      }
    }
    if (merges[i].one == ENTRY_JAIL) {
      g_string_assign(( *(entry -> killring)).buffer,text);
      g_string_truncate(( *(entry -> killring)).buffer,len);
    }
    ( *(entry -> killring)).last = action;
  }
  else {
    if ((action == ENTRY_DEL_BWD_CHAR) || (action == ENTRY_DEL_BWD_WORD)) 
      g_string_prepend_len(( *(entry -> killring)).buffer,text,len);
    else 
      g_string_append_len(( *(entry -> killring)).buffer,text,len);
  }
  return 1;
}

static void destroy_suggest(GntEntry *entry)
{
  if ((entry -> ddown) != 0) {
    gnt_widget_destroy(( *(entry -> ddown)).parent);
    entry -> ddown = ((GntWidget *)((void *)0));
  }
}

static char *get_beginning_of_word(GntEntry *entry)
{
  char *s = (entry -> cursor);
{
    while(s > (entry -> start)){
      char *t = g_utf8_find_prev_char((entry -> start),s);
      if ((( *__ctype_b_loc())[(int )( *t)] & ((unsigned short )_ISspace)) != 0) 
        break; 
      s = t;
    }
  }
  return s;
}

static gboolean complete_suggest(GntEntry *entry,const char *text)
{
  int offstart = 0;
  int offend = 0;
  if ((entry -> word) != 0) {
    char *s = get_beginning_of_word(entry);
    const char *iter = text;
    offstart = (g_utf8_pointer_to_offset((entry -> start),s));
    while((( *iter) != 0) && (toupper(( *s)) == toupper(( *iter)))){
       *(s++) =  *(iter++);
    }
    if (( *iter) != 0) {
      gnt_entry_key_pressed(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_widget_get_gtype()))),iter);
    }
    offend = (g_utf8_pointer_to_offset((entry -> start),(entry -> cursor)));
  }
  else {
    offstart = 0;
    gnt_entry_set_text_internal(entry,text);
    offend = (g_utf8_strlen(text,(-1)));
  }
  g_signal_emit(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),signals[SIG_COMPLETION],0,((entry -> start) + offstart),((entry -> start) + offend));
  update_kill_ring(entry,ENTRY_JAIL,0,0);
  return 1;
}

static int max_common_prefix(const char *s,const char *t)
{
  const char *f = s;
  while(((( *f) != 0) && (( *t) != 0)) && (( *f) == ( *(t++))))
    f++;
  return (f - s);
}

static gboolean show_suggest_dropdown(GntEntry *entry)
{
  char *suggest = (char *)((void *)0);
  int len;
  int offset = 0;
  int x;
  int y;
  int count = 0;
  GList *iter;
  const char *text = (const char *)((void *)0);
  const char *sgst = (const char *)((void *)0);
  int max = -1;
  if ((entry -> word) != 0) {
    char *s = get_beginning_of_word(entry);
    suggest = g_strndup(s,((entry -> cursor) - s));
    if ((entry -> scroll) < s) 
      offset = gnt_util_onscreen_width((entry -> scroll),s);
  }
  else 
    suggest = g_strdup((entry -> start));
/* Don't need to use the utf8-function here */
  len = (strlen(suggest));
  if ((entry -> ddown) == ((GntWidget *)((void *)0))) {
    GntWidget *box = gnt_box_new(0,1);
    entry -> ddown = gnt_tree_new();
    gnt_tree_set_compare_func(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(entry -> ddown)),gnt_tree_get_gtype()))),((GCompareFunc )g_utf8_collate));
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),(entry -> ddown));
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_TRANSIENT;
    gnt_widget_get_position(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_widget_get_gtype()))),&x,&y);
    x += offset;
    y++;
    if ((y + 10) >= (((stdscr != 0)?((stdscr -> _maxy) + 1) : -1))) 
      y -= 11;
    gnt_widget_set_position(box,x,y);
  }
  else 
    gnt_tree_remove_all(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(entry -> ddown)),gnt_tree_get_gtype()))));
  for (((count = 0) , (iter = (entry -> suggests))); iter != 0; iter = (iter -> next)) {
    text = (iter -> data);
    if ((g_ascii_strncasecmp(suggest,text,len) == 0) && (strlen(text) >= len)) {
      gnt_tree_add_row_after(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(entry -> ddown)),gnt_tree_get_gtype()))),((gpointer )text),gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(entry -> ddown)),gnt_tree_get_gtype()))),text),0,0);
      count++;
      if (max == -1) 
        max = (strlen(text) - len);
      else if (max != 0) 
        max = ((max < max_common_prefix((sgst + len),(text + len)))?max : max_common_prefix((sgst + len),(text + len)));
      sgst = text;
    }
  }
  g_free(suggest);
  if (count == 0) {
    destroy_suggest(entry);
    return 0;
  }
  else if (count == 1) {
    char *store = g_strndup((entry -> start),((entry -> end) - (entry -> start)));
    gboolean ret;
    destroy_suggest(entry);
    complete_suggest(entry,sgst);
    ret = (strncmp(store,(entry -> start),((entry -> end) - (entry -> start))) != 0);
    g_free(store);
    return ret;
  }
  else {
    if (max > 0) {
      GntWidget *ddown = (entry -> ddown);
      char *match = g_strndup((sgst + len),max);
      entry -> ddown = ((GntWidget *)((void *)0));
      gnt_entry_key_pressed(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_widget_get_gtype()))),match);
      g_free(match);
      if ((entry -> ddown) != 0) 
        gnt_widget_destroy(ddown);
      else 
        entry -> ddown = ddown;
    }
    gnt_widget_draw(( *(entry -> ddown)).parent);
  }
  return 1;
}

static void gnt_entry_draw(GntWidget *widget)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_entry_get_gtype()));
  int stop;
  gboolean focus;
  int curpos;
  if ((focus = gnt_widget_has_focus(widget)) != 0) 
    wbkgdset((widget -> window),(0 | gnt_color_pair(GNT_COLOR_TEXT_NORMAL)));
  else 
    wbkgdset((widget -> window),(0 | gnt_color_pair(GNT_COLOR_HIGHLIGHT_D)));
  if ((entry -> masked) != 0) {
    (wmove((widget -> window),0,0) == -1)?-1 : whline((widget -> window),((gnt_ascii_only() != 0)?'*' : acs_map[(unsigned char )'~']),(g_utf8_pointer_to_offset((entry -> scroll),(entry -> end))));
  }
  else 
    mvwprintw((widget -> window),0,0,"%s",C_((entry -> scroll)));
  stop = gnt_util_onscreen_width((entry -> scroll),(entry -> end));
  if (stop < widget -> priv.width) 
    (wmove((widget -> window),0,stop) == -1)?-1 : whline((widget -> window),'_',(widget -> priv.width - stop));
  curpos = gnt_util_onscreen_width((entry -> scroll),(entry -> cursor));
  if (focus != 0) 
    (wmove((widget -> window),0,curpos) == -1)?-1 : wchgat((widget -> window),1,(((chtype )1UL) << 10 + 8),GNT_COLOR_TEXT_NORMAL,0);
  wmove((widget -> window),0,curpos);;
}

static void gnt_entry_size_request(GntWidget *widget)
{
  if (!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U)) {
    widget -> priv.height = 1;
    widget -> priv.width = 20;
  }
}

static void gnt_entry_map(GntWidget *widget)
{
  if ((widget -> priv.width == 0) || (widget -> priv.height == 0)) 
    gnt_widget_size_request(widget);;
}

static void entry_redraw(GntWidget *widget)
{
  gnt_entry_draw(widget);
  gnt_widget_queue_update(widget);
}

static void entry_text_changed(GntEntry *entry)
{
  g_signal_emit(entry,signals[SIG_TEXT_CHANGED],0);
}

static gboolean move_back(GntBindable *bind,GList *null)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  if ((entry -> cursor) <= (entry -> start)) 
    return 0;
  entry -> cursor = g_utf8_find_prev_char((entry -> start),(entry -> cursor));
  if ((entry -> cursor) < (entry -> scroll)) 
    entry -> scroll = (entry -> cursor);
  update_kill_ring(entry,ENTRY_JAIL,0,0);
  entry_redraw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_widget_get_gtype()))));
  return 1;
}

static gboolean move_forward(GntBindable *bind,GList *list)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  if ((entry -> cursor) >= (entry -> end)) 
    return 0;
  entry -> cursor = g_utf8_find_next_char((entry -> cursor),0);
  while(gnt_util_onscreen_width((entry -> scroll),(entry -> cursor)) >= ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_widget_get_gtype())))).priv.width)
    entry -> scroll = g_utf8_find_next_char((entry -> scroll),0);
  update_kill_ring(entry,ENTRY_JAIL,0,0);
  entry_redraw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_widget_get_gtype()))));
  return 1;
}

static gboolean backspace(GntBindable *bind,GList *null)
{
  int len;
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  if ((entry -> cursor) <= (entry -> start)) 
    return 1;
  len = ((entry -> cursor) - g_utf8_find_prev_char((entry -> start),(entry -> cursor)));
  update_kill_ring(entry,ENTRY_JAIL,(entry -> cursor),-len);
  entry -> cursor -= len;
  memmove((entry -> cursor),((entry -> cursor) + len),((entry -> end) - (entry -> cursor)));
  entry -> end -= len;
  if ((entry -> scroll) > (entry -> start)) 
    entry -> scroll = g_utf8_find_prev_char((entry -> start),(entry -> scroll));
  entry_redraw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_widget_get_gtype()))));
  if ((entry -> ddown) != 0) 
    show_suggest_dropdown(entry);
  entry_text_changed(entry);
  return 1;
}

static gboolean delkey(GntBindable *bind,GList *null)
{
  int len;
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  if ((entry -> cursor) >= (entry -> end)) 
    return 0;
  len = (g_utf8_find_next_char((entry -> cursor),0) - (entry -> cursor));
  update_kill_ring(entry,ENTRY_JAIL,(entry -> cursor),len);
  memmove((entry -> cursor),((entry -> cursor) + len),((((entry -> end) - (entry -> cursor)) - len) + 1));
  entry -> end -= len;
  entry_redraw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_widget_get_gtype()))));
  if ((entry -> ddown) != 0) 
    show_suggest_dropdown(entry);
  entry_text_changed(entry);
  return 1;
}

static gboolean move_start(GntBindable *bind,GList *null)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  entry -> scroll = (entry -> cursor = (entry -> start));
  entry_redraw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_widget_get_gtype()))));
  update_kill_ring(entry,ENTRY_JAIL,0,0);
  return 1;
}

static gboolean move_end(GntBindable *bind,GList *null)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  entry -> cursor = (entry -> end);
/* This should be better than this */
  while(gnt_util_onscreen_width((entry -> scroll),(entry -> cursor)) >= ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_widget_get_gtype())))).priv.width)
    entry -> scroll = g_utf8_find_next_char((entry -> scroll),0);
  entry_redraw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_widget_get_gtype()))));
  update_kill_ring(entry,ENTRY_JAIL,0,0);
  return 1;
}

static gboolean history_next(GntBindable *bind,GList *null)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  if (((entry -> histlength) != 0) && (( *(entry -> history)).prev != 0)) {
    entry -> history = ( *(entry -> history)).prev;
    gnt_entry_set_text_internal(entry,( *(entry -> history)).data);
    destroy_suggest(entry);
    entry_text_changed(entry);
    update_kill_ring(entry,ENTRY_JAIL,0,0);
    return 1;
  }
  return 0;
}

static gboolean history_prev(GntBindable *bind,GList *null)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  if (((entry -> histlength) != 0) && (( *(entry -> history)).next != 0)) {
    if (( *(entry -> history)).prev == ((GList *)((void *)0))) {
/* Save the current contents */
      char *text = g_strdup(gnt_entry_get_text(entry));
      g_free(( *(entry -> history)).data);
      ( *(entry -> history)).data = text;
    }
    entry -> history = ( *(entry -> history)).next;
    gnt_entry_set_text_internal(entry,( *(entry -> history)).data);
    destroy_suggest(entry);
    entry_text_changed(entry);
    update_kill_ring(entry,ENTRY_JAIL,0,0);
    return 1;
  }
  return 0;
}

static gboolean history_search(GntBindable *bind,GList *null)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  GList *iter;
  const char *current;
  if ((( *(entry -> history)).prev != 0) && (( *(entry -> search)).needle != 0)) 
    current = ( *(entry -> search)).needle;
  else 
    current = gnt_entry_get_text(entry);
  if ((!((entry -> histlength) != 0) || !(( *(entry -> history)).next != 0)) || !(( *current) != 0)) 
    return 0;
{
    for (iter = ( *(entry -> history)).next; iter != 0; iter = (iter -> next)) {
      const char *str = (iter -> data);
/* A more utf8-friendly version of strstr would have been better, but
		 * for now, this will have to do. */
      if (strstr(str,current) != ((char *)((void *)0))) 
        break; 
    }
  }
  if (!(iter != 0)) 
    return 1;
  if (( *(entry -> history)).prev == ((GList *)((void *)0))) {
/* We are doing it for the first time. Save the current contents */
    char *text = g_strdup(gnt_entry_get_text(entry));
    g_free(( *(entry -> search)).needle);
    ( *(entry -> search)).needle = g_strdup(current);
    g_free(( *(entry -> history)).data);
    ( *(entry -> history)).data = text;
  }
  entry -> history = iter;
  gnt_entry_set_text_internal(entry,( *(entry -> history)).data);
  destroy_suggest(entry);
  entry_text_changed(entry);
  update_kill_ring(entry,ENTRY_JAIL,0,0);
  return 1;
}

static gboolean clipboard_paste(GntBindable *bind,GList *n)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  gchar *i;
  gchar *text;
  gchar *a;
  gchar *all;
  text = (i = gnt_get_clipboard_string());
  while(( *i) != 0){
    i = (i + g_utf8_skip[ *((const guchar *)i)]);
    if ((( *i) == 13) || (( *i) == 10)) 
       *i = 32;
  }
  a = g_strndup((entry -> start),((entry -> cursor) - (entry -> start)));
  all = g_strconcat(a,text,(entry -> cursor),((void *)((void *)0)));
  gnt_entry_set_text_internal(entry,all);
  update_kill_ring(entry,ENTRY_JAIL,0,0);
  g_free(a);
  g_free(text);
  g_free(all);
  return 1;
}

static gboolean suggest_show(GntBindable *bind,GList *null)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  if ((entry -> ddown) != 0) {
    gnt_bindable_perform_action_named(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)(entry -> ddown)),gnt_bindable_get_gtype()))),"move-down",((void *)((void *)0)));
    return 1;
  }
  return show_suggest_dropdown(entry);
}

static gboolean suggest_next(GntBindable *bind,GList *null)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  if ((entry -> ddown) != 0) {
    gnt_bindable_perform_action_named(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)(entry -> ddown)),gnt_bindable_get_gtype()))),"move-down",((void *)((void *)0)));
    return 1;
  }
  return 0;
}

static gboolean suggest_prev(GntBindable *bind,GList *null)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  if ((entry -> ddown) != 0) {
    gnt_bindable_perform_action_named(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)(entry -> ddown)),gnt_bindable_get_gtype()))),"move-up",((void *)((void *)0)));
    return 1;
  }
  return 0;
}

static gboolean suggest_next_page(GntBindable *bind,GList *null)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  if ((entry -> ddown) != 0) {
    gnt_bindable_perform_action_named(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)(entry -> ddown)),gnt_bindable_get_gtype()))),"page-down",((void *)((void *)0)));
    return 1;
  }
  return 0;
}

static gboolean suggest_prev_page(GntBindable *bind,GList *null)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  if ((entry -> ddown) != 0) {
    gnt_bindable_perform_action_named(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)(entry -> ddown)),gnt_bindable_get_gtype()))),"page-up",((void *)((void *)0)));
    return 1;
  }
  return 0;
}

static gboolean del_to_home(GntBindable *bind,GList *null)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  if ((entry -> cursor) <= (entry -> start)) 
    return 1;
  update_kill_ring(entry,ENTRY_DEL_BOL,(entry -> start),((entry -> cursor) - (entry -> start)));
  memmove((entry -> start),(entry -> cursor),((entry -> end) - (entry -> cursor)));
  entry -> end -= ((entry -> cursor) - (entry -> start));
  entry -> cursor = (entry -> scroll = (entry -> start));
  memset((entry -> end),0,((entry -> buffer) - ((entry -> end) - (entry -> start))));
  entry_redraw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_widget_get_gtype()))));
  entry_text_changed(entry);
  return 1;
}

static gboolean del_to_end(GntBindable *bind,GList *null)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  if ((entry -> end) <= (entry -> cursor)) 
    return 1;
  update_kill_ring(entry,ENTRY_DEL_EOL,(entry -> cursor),((entry -> end) - (entry -> cursor)));
  entry -> end = (entry -> cursor);
  memset((entry -> end),0,((entry -> buffer) - ((entry -> end) - (entry -> start))));
  entry_redraw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_widget_get_gtype()))));
  entry_text_changed(entry);
  return 1;
}
#define SAME(a,b)    ((g_unichar_isalnum(a) && g_unichar_isalnum(b)) || \
				(g_unichar_isspace(a) && g_unichar_isspace(b)) || \
				(g_unichar_iswide(a) && g_unichar_iswide(b)) || \
				(g_unichar_ispunct(a) && g_unichar_ispunct(b)))

static const char *begin_word(const char *text,const char *begin)
{
  gunichar ch = 0;
  while((text > begin) && (!(( *text) != 0) || (g_unichar_isspace(g_utf8_get_char(text)) != 0)))
    text = (g_utf8_find_prev_char(begin,text));
  ch = g_utf8_get_char(text);
{
    while((text = (g_utf8_find_prev_char(begin,text))) >= begin){
      gunichar cur = g_utf8_get_char(text);
      if (!(((((g_unichar_isalnum(ch) != 0) && (g_unichar_isalnum(cur) != 0)) || ((g_unichar_isspace(ch) != 0) && (g_unichar_isspace(cur) != 0))) || ((g_unichar_iswide(ch) != 0) && (g_unichar_iswide(cur) != 0))) || ((g_unichar_ispunct(ch) != 0) && (g_unichar_ispunct(cur) != 0)))) 
        break; 
    }
  }
  return (text != 0)?(g_utf8_find_next_char(text,0)) : begin;
}

static const char *next_begin_word(const char *text,const char *end)
{
  gunichar ch = 0;
  while(((text != 0) && (text < end)) && (g_unichar_isspace(g_utf8_get_char(text)) != 0))
    text = (g_utf8_find_next_char(text,end));
  if (text != 0) {
    ch = g_utf8_get_char(text);
{
      while(((text = (g_utf8_find_next_char(text,end))) != ((const char *)((void *)0))) && (text <= end)){
        gunichar cur = g_utf8_get_char(text);
        if (!(((((g_unichar_isalnum(ch) != 0) && (g_unichar_isalnum(cur) != 0)) || ((g_unichar_isspace(ch) != 0) && (g_unichar_isspace(cur) != 0))) || ((g_unichar_iswide(ch) != 0) && (g_unichar_iswide(cur) != 0))) || ((g_unichar_ispunct(ch) != 0) && (g_unichar_ispunct(cur) != 0)))) 
          break; 
      }
    }
  }
  return (text != 0)?text : end;
}
#undef SAME

static gboolean move_back_word(GntBindable *bind,GList *null)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  const char *iter = (g_utf8_find_prev_char((entry -> start),(entry -> cursor)));
  if (iter < (entry -> start)) 
    return 1;
  iter = begin_word(iter,(entry -> start));
  entry -> cursor = ((char *)iter);
  if ((entry -> cursor) < (entry -> scroll)) 
    entry -> scroll = (entry -> cursor);
  update_kill_ring(entry,ENTRY_JAIL,0,0);
  entry_redraw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_widget_get_gtype()))));
  return 1;
}

static gboolean del_prev_word(GntBindable *bind,GList *null)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_widget_get_gtype()));
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  char *iter = g_utf8_find_prev_char((entry -> start),(entry -> cursor));
  int count;
  if (iter < (entry -> start)) 
    return 1;
  iter = ((char *)(begin_word(iter,(entry -> start))));
  count = ((entry -> cursor) - iter);
  update_kill_ring(entry,ENTRY_DEL_BWD_WORD,iter,count);
  memmove(iter,(entry -> cursor),((entry -> end) - (entry -> cursor)));
  entry -> end -= count;
  entry -> cursor = iter;
  if ((entry -> cursor) <= (entry -> scroll)) {
    entry -> scroll = (((entry -> cursor) - widget -> priv.width) + 2);
    if ((entry -> scroll) < (entry -> start)) 
      entry -> scroll = (entry -> start);
  }
  memset((entry -> end),0,((entry -> buffer) - ((entry -> end) - (entry -> start))));
  entry_redraw(widget);
  entry_text_changed(entry);
  return 1;
}

static gboolean move_forward_word(GntBindable *bind,GList *list)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_widget_get_gtype()));
  entry -> cursor = ((char *)(next_begin_word((entry -> cursor),(entry -> end))));
  while(gnt_util_onscreen_width((entry -> scroll),(entry -> cursor)) >= widget -> priv.width){
    entry -> scroll = g_utf8_find_next_char((entry -> scroll),0);
  }
  update_kill_ring(entry,ENTRY_JAIL,0,0);
  entry_redraw(widget);
  return 1;
}

static gboolean delete_forward_word(GntBindable *bind,GList *list)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_widget_get_gtype()));
  char *iter = (char *)(next_begin_word((entry -> cursor),(entry -> end)));
  int len = (((entry -> end) - iter) + 1);
  if (len <= 0) 
    return 1;
  update_kill_ring(entry,ENTRY_DEL_FWD_WORD,(entry -> cursor),(iter - (entry -> cursor)));
  memmove((entry -> cursor),iter,len);
  len = (iter - (entry -> cursor));
  entry -> end -= len;
  memset((entry -> end),0,len);
  entry_redraw(widget);
  entry_text_changed(entry);
  return 1;
}

static gboolean transpose_chars(GntBindable *bind,GList *null)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  char *current;
  char *prev;
/* that's right */
  char hold[8UL];
  if ((entry -> cursor) <= (entry -> start)) 
    return 0;
  if (!(( *(entry -> cursor)) != 0)) 
    entry -> cursor = g_utf8_find_prev_char((entry -> start),(entry -> cursor));
  current = (entry -> cursor);
  prev = g_utf8_find_prev_char((entry -> start),(entry -> cursor));
  move_forward(bind,null);
/* Let's do this dance! */
  memcpy(hold,prev,(current - prev));
  memmove(prev,current,((entry -> cursor) - current));
  memcpy((prev + ((entry -> cursor) - current)),hold,(current - prev));
  update_kill_ring(entry,ENTRY_JAIL,0,0);
  entry_redraw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_widget_get_gtype()))));
  entry_text_changed(entry);
  return 1;
}

static gboolean entry_yank(GntBindable *bind,GList *null)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_entry_get_gtype()));
  gnt_entry_key_pressed(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_widget_get_gtype()))),( *( *(entry -> killring)).buffer).str);
  return 1;
}

static gboolean gnt_entry_key_pressed(GntWidget *widget,const char *text)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_entry_get_gtype()));
  if (text[0] == 27) {
    if (text[1] == 0) {
      destroy_suggest(entry);
      return 1;
    }
    return 0;
  }
  if ((((text[0] == 13) || (text[0] == 32)) || (text[0] == 10)) && ((entry -> ddown) != 0)) {
    char *text = g_strdup((gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(entry -> ddown)),gnt_tree_get_gtype()))))));
    destroy_suggest(entry);
    complete_suggest(entry,text);
    g_free(text);
    update_kill_ring(entry,ENTRY_JAIL,0,0);
    entry_text_changed(entry);
    return 1;
  }
  if (!((( *__ctype_b_loc())[(int )text[0]] & ((unsigned short )_IScntrl)) != 0)) {
    const char *str;
    const char *next;
    for (str = text; ( *str) != 0; str = next) {{
        int len;
        next = (g_utf8_find_next_char(str,0));
        len = (next - str);
/* Valid input? */
/* XXX: Is it necessary to use _unichar_ variants here? */
        if (((( *__ctype_b_loc())[(int )( *str)] & ((unsigned short )_ISpunct)) != 0) && (((entry -> flag) & GNT_ENTRY_FLAG_NO_PUNCT) != 0U)) 
          continue; 
        if (((( *__ctype_b_loc())[(int )( *str)] & ((unsigned short )_ISspace)) != 0) && (((entry -> flag) & GNT_ENTRY_FLAG_NO_SPACE) != 0U)) 
          continue; 
        if (((( *__ctype_b_loc())[(int )( *str)] & ((unsigned short )_ISalpha)) != 0) && !(((entry -> flag) & GNT_ENTRY_FLAG_ALPHA) != 0U)) 
          continue; 
        if (((( *__ctype_b_loc())[(int )( *str)] & ((unsigned short )_ISdigit)) != 0) && !(((entry -> flag) & GNT_ENTRY_FLAG_INT) != 0U)) 
          continue; 
/* Reached the max? */
        if (((entry -> max) != 0) && (g_utf8_pointer_to_offset((entry -> start),(entry -> end)) >= (entry -> max))) 
          continue; 
        if ((((entry -> end) + len) - (entry -> start)) >= (entry -> buffer)) {
/* This will cause the buffer to grow */
          char *tmp = g_strdup((entry -> start));
          gnt_entry_set_text_internal(entry,tmp);
          g_free(tmp);
        }
        memmove(((entry -> cursor) + len),(entry -> cursor),(((entry -> end) - (entry -> cursor)) + 1));
        entry -> end += len;
        while(str < next){
          if ((( *str) == 13) || (( *str) == 10)) 
             *(entry -> cursor) = 32;
          else 
             *(entry -> cursor) =  *str;
          entry -> cursor++;
          str++;
        }
        while(gnt_util_onscreen_width((entry -> scroll),(entry -> cursor)) >= widget -> priv.width)
          entry -> scroll = g_utf8_find_next_char((entry -> scroll),0);
        if ((entry -> ddown) != 0) 
          show_suggest_dropdown(entry);
      }
    }
    update_kill_ring(entry,ENTRY_JAIL,0,0);
    entry_redraw(widget);
    entry_text_changed(entry);
    return 1;
  }
  if ((text[0] == 13) || (text[0] == 10)) {
    gnt_widget_activate(widget);
    return 1;
  }
  return 0;
}

static void jail_killring(GntEntryKillRing *kr)
{
  g_string_free((kr -> buffer),1);
  g_free(kr);
}

static void gnt_entry_destroy(GntWidget *widget)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_entry_get_gtype()));
  g_free((entry -> start));
  if ((entry -> history) != 0) {
    entry -> history = g_list_first((entry -> history));
    g_list_foreach((entry -> history),((GFunc )g_free),0);
    g_list_free((entry -> history));
  }
  if ((entry -> suggests) != 0) {
    g_list_foreach((entry -> suggests),((GFunc )g_free),0);
    g_list_free((entry -> suggests));
  }
  if ((entry -> ddown) != 0) {
    gnt_widget_destroy(( *(entry -> ddown)).parent);
  }
  g_free(( *(entry -> search)).needle);
  g_free((entry -> search));
  jail_killring((entry -> killring));
}

static void gnt_entry_lost_focus(GntWidget *widget)
{
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_entry_get_gtype()));
  destroy_suggest(entry);
  entry_redraw(widget);
}

static gboolean gnt_entry_clicked(GntWidget *widget,GntMouseEvent event,int x,int y)
{
  if (event == GNT_MIDDLE_MOUSE_DOWN) {
    clipboard_paste(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_bindable_get_gtype()))),0);
    return 1;
  }
  return 0;
}

static void gnt_entry_class_init(GntEntryClass *klass)
{
  GntBindableClass *bindable = (GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()));
  char s[3UL] = {(27), erasechar(), (0)};
  parent_class = ((GntWidgetClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_widget_get_gtype())));
  parent_class -> clicked = gnt_entry_clicked;
  parent_class -> destroy = gnt_entry_destroy;
  parent_class -> draw = gnt_entry_draw;
  parent_class -> map = gnt_entry_map;
  parent_class -> size_request = gnt_entry_size_request;
  parent_class -> key_pressed = gnt_entry_key_pressed;
  parent_class -> lost_focus = gnt_entry_lost_focus;
  signals[SIG_TEXT_CHANGED] = g_signal_new("text_changed",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntEntryClass *)((GntEntryClass *)0))).text_changed))),0,0,g_cclosure_marshal_VOID__VOID,((GType )(1 << 2)),0);
  signals[SIG_COMPLETION] = g_signal_new("completion",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,0,0,0,gnt_closure_marshal_VOID__POINTER_POINTER,((GType )(1 << 2)),2,((GType )(17 << 2)),((GType )(17 << 2)));
  gnt_bindable_class_register_action(bindable,"cursor-home",move_start,"\001",((void *)((void *)0)));
  gnt_bindable_register_binding(bindable,"cursor-home",((cur_term -> type.Strings[76] != 0)?cur_term -> type.Strings[76] : ""),((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"cursor-end",move_end,"\005",((void *)((void *)0)));
  gnt_bindable_register_binding(bindable,"cursor-end",((cur_term -> type.Strings[164] != 0)?cur_term -> type.Strings[164] : ""),((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"delete-prev",backspace,((cur_term -> type.Strings[55] != 0)?cur_term -> type.Strings[55] : ""),((void *)((void *)0)));
  gnt_bindable_register_binding(bindable,"delete-prev",(s + 1),((void *)((void *)0)));
  gnt_bindable_register_binding(bindable,"delete-prev","\b",((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"delete-next",delkey,((cur_term -> type.Strings[59] != 0)?cur_term -> type.Strings[59] : ""),((void *)((void *)0)));
  gnt_bindable_register_binding(bindable,"delete-next","\004",((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"delete-start",del_to_home,"\025",((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"delete-end",del_to_end,"\v",((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"delete-prev-word",del_prev_word,"\027",((void *)((void *)0)));
  gnt_bindable_register_binding(bindable,"delete-prev-word",s,((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"cursor-prev-word",move_back_word,"\033b",((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"cursor-prev",move_back,((cur_term -> type.Strings[79] != 0)?cur_term -> type.Strings[79] : ""),((void *)((void *)0)));
  gnt_bindable_register_binding(bindable,"cursor-prev","\002",((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"cursor-next",move_forward,((cur_term -> type.Strings[83] != 0)?cur_term -> type.Strings[83] : ""),((void *)((void *)0)));
  gnt_bindable_register_binding(bindable,"cursor-next","\006",((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"cursor-next-word",move_forward_word,"\033f",((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"delete-next-word",delete_forward_word,"\033d",((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"transpose-chars",transpose_chars,"\024",((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"yank",entry_yank,"\031",((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"suggest-show",suggest_show,"\t",((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"suggest-next",suggest_next,((cur_term -> type.Strings[61] != 0)?cur_term -> type.Strings[61] : ""),((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"suggest-prev",suggest_prev,((cur_term -> type.Strings[87] != 0)?cur_term -> type.Strings[87] : ""),((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"suggest-next-page",suggest_next_page,((cur_term -> type.Strings[81] != 0)?cur_term -> type.Strings[81] : ""),((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"suggest-prev-page",suggest_prev_page,((cur_term -> type.Strings[82] != 0)?cur_term -> type.Strings[82] : ""),((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"history-next",history_next,((gnt_key_cdown != 0)?gnt_key_cdown : ""),((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"history-prev",history_prev,((gnt_key_cup != 0)?gnt_key_cup : ""),((void *)((void *)0)));
  gnt_bindable_register_binding(bindable,"history-prev","\020",((void *)((void *)0)));
  gnt_bindable_register_binding(bindable,"history-next","\016",((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"history-search",history_search,"\022",((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"clipboard-paste",clipboard_paste,"\026",((void *)((void *)0)));
  gnt_style_read_actions(( *((GTypeClass *)klass)).g_type,((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))));;
}

static GntEntryKillRing *new_killring()
{
  GntEntryKillRing *kr = (GntEntryKillRing *)(g_malloc0_n(1,(sizeof(GntEntryKillRing ))));
  kr -> buffer = g_string_new(0);
  return kr;
}

static void gnt_entry_init(GTypeInstance *instance,gpointer class)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_widget_get_gtype()));
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_entry_get_gtype()));
  entry -> flag = (GNT_ENTRY_FLAG_ALPHA | GNT_ENTRY_FLAG_INT);
  entry -> max = 0;
  entry -> histlength = 0;
  entry -> history = ((GList *)((void *)0));
  entry -> word = 1;
  entry -> always = 0;
  entry -> suggests = ((GList *)((void *)0));
  entry -> killring = new_killring();
  entry -> search = ((GntEntrySearch *)(g_malloc0_n(1,(sizeof(GntEntrySearch )))));
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags |= (GNT_WIDGET_NO_BORDER | GNT_WIDGET_NO_SHADOW | GNT_WIDGET_CAN_TAKE_FOCUS);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_GROW_X;
  widget -> priv.minw = 3;
  widget -> priv.minh = 1;;
}
/******************************************************************************
 * GntEntry API
 *****************************************************************************/

GType gnt_entry_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(GntEntryClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gnt_entry_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GntEntry ))), (0), (gnt_entry_init), ((const GTypeValueTable *)((void *)0))
/* base_init		*/
/* base_finalize	*/
/* class_finalize	*/
/* class_data		*/
/* n_preallocs		*/
/* instance_init	*/
/* value_table		*/
};
    type = g_type_register_static(gnt_widget_get_gtype(),"GntEntry",&info,0);
  }
  return type;
}

GntWidget *gnt_entry_new(const char *text)
{
  GntWidget *widget = (g_object_new(gnt_entry_get_gtype(),0));
  GntEntry *entry = (GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_entry_get_gtype()));
  gnt_entry_set_text_internal(entry,text);
  return widget;
}

static void gnt_entry_set_text_internal(GntEntry *entry,const char *text)
{
  int len;
  int scroll;
  int cursor;
  g_free((entry -> start));
  if ((text != 0) && (text[0] != 0)) {
    len = (strlen(text));
  }
  else {
    len = 0;
  }
  entry -> buffer = (len + 128);
  scroll = ((entry -> scroll) - (entry -> start));
  cursor = ((entry -> end) - (entry -> cursor));
  entry -> start = ((char *)(g_malloc0_n((entry -> buffer),(sizeof(char )))));
  if (text != 0) 
    snprintf((entry -> start),(len + 1),"%s",text);
  entry -> end = ((entry -> start) + len);
  if ((entry -> scroll = ((entry -> start) + scroll)) > (entry -> end)) 
    entry -> scroll = (entry -> end);
  if ((entry -> cursor = ((entry -> end) - cursor)) > (entry -> end)) 
    entry -> cursor = (entry -> end);
  if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U) 
    entry_redraw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_widget_get_gtype()))));
}

void gnt_entry_set_text(GntEntry *entry,const char *text)
{
  gboolean changed = 1;
  if ((text == ((const char *)((void *)0))) && ((entry -> start) == ((char *)((void *)0)))) 
    changed = 0;
  if (((text != 0) && ((entry -> start) != 0)) && (g_utf8_collate(text,(entry -> start)) == 0)) 
    changed = 0;
  gnt_entry_set_text_internal(entry,text);
  if (changed != 0) 
    entry_text_changed(entry);
}

void gnt_entry_set_max(GntEntry *entry,int max)
{
  entry -> max = max;
}

void gnt_entry_set_flag(GntEntry *entry,GntEntryFlag flag)
{
  entry -> flag = flag;
/* XXX: Check the existing string to make sure the flags are respected? */
}

const char *gnt_entry_get_text(GntEntry *entry)
{
  return (entry -> start);
}

void gnt_entry_clear(GntEntry *entry)
{
  gnt_entry_set_text_internal(entry,0);
  entry -> scroll = (entry -> cursor = (entry -> end = (entry -> start)));
  entry_redraw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_widget_get_gtype()))));
  destroy_suggest(entry);
  entry_text_changed(entry);
}

void gnt_entry_set_masked(GntEntry *entry,gboolean set)
{
  entry -> masked = set;
}

void gnt_entry_add_to_history(GntEntry *entry,const char *text)
{
/* Need to set_history_length first */
  do {
    if ((entry -> history) != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning("Gnt",((const char *)__func__),"entry->history != NULL");
      return ;
    };
  }while (0);
  if (g_list_length((entry -> history)) >= (entry -> histlength)) 
    return ;
  entry -> history = g_list_first((entry -> history));
  g_free(( *(entry -> history)).data);
  ( *(entry -> history)).data = (g_strdup(text));
  entry -> history = g_list_prepend((entry -> history),0);
}

void gnt_entry_set_history_length(GntEntry *entry,int num)
{
  if (num == 0) {
    entry -> histlength = num;
    if ((entry -> history) != 0) {
      entry -> history = g_list_first((entry -> history));
      g_list_foreach((entry -> history),((GFunc )g_free),0);
      g_list_free((entry -> history));
      entry -> history = ((GList *)((void *)0));
    }
    return ;
  }
  if ((entry -> histlength) == 0) {
    entry -> histlength = num;
    entry -> history = g_list_append(0,0);
    return ;
  }
  if ((num > 0) && (num < (entry -> histlength))) {
    GList *first;
    GList *iter;
    int index = 0;
    for (((first = (entry -> history)) , (index = 0)); (first -> prev) != 0; ((first = (first -> prev)) , index++)) ;
    while((iter = g_list_nth(first,num)) != ((GList *)((void *)0))){
      g_free((iter -> data));
      first = g_list_delete_link(first,iter);
    }
    entry -> histlength = num;
    if (index >= num) 
      entry -> history = g_list_last(first);
    return ;
  }
  entry -> histlength = num;
}

void gnt_entry_set_word_suggest(GntEntry *entry,gboolean word)
{
  entry -> word = word;
}

void gnt_entry_set_always_suggest(GntEntry *entry,gboolean always)
{
  entry -> always = always;
}

void gnt_entry_add_suggest(GntEntry *entry,const char *text)
{
  GList *find;
  if (!(text != 0) || !(( *text) != 0)) 
    return ;
  find = g_list_find_custom((entry -> suggests),text,((GCompareFunc )g_utf8_collate));
  if (find != 0) 
    return ;
  entry -> suggests = g_list_append((entry -> suggests),(g_strdup(text)));
}

void gnt_entry_remove_suggest(GntEntry *entry,const char *text)
{
  GList *find = g_list_find_custom((entry -> suggests),text,((GCompareFunc )g_utf8_collate));
  if (find != 0) {
    g_free((find -> data));
    entry -> suggests = g_list_delete_link((entry -> suggests),find);
  }
}
