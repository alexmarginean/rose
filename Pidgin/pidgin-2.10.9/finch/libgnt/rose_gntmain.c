/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#define _GNU_SOURCE
#if (defined(__APPLE__) || defined(__unix__)) && !defined(__FreeBSD__) && !defined(__OpenBSD__)
#define _XOPEN_SOURCE_EXTENDED
#endif
#include "config.h"
#include <gmodule.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "gntinternal.h"
#undef GNT_LOG_DOMAIN
#define GNT_LOG_DOMAIN "Main"
#include "gnt.h"
#include "gntbox.h"
#include "gntbutton.h"
#include "gntcolors.h"
#include "gntclipboard.h"
#include "gntkeys.h"
#include "gntlabel.h"
#include "gntmenu.h"
#include "gntstyle.h"
#include "gnttree.h"
#include "gntutils.h"
#include "gntwindow.h"
#include "gntwm.h"
#include <panel.h>
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
/**
 * Notes: Interesting functions to look at:
 * 	scr_dump, scr_init, scr_restore: for workspaces
 *
 * 	Need to wattrset for colors to use with PDCurses.
 */
static GIOChannel *channel = (GIOChannel *)((void *)0);
static guint channel_read_callback = 0;
static guint channel_error_callback = 0;
static gboolean ascii_only;
static gboolean mouse_enabled;
static void setup_io();
static gboolean refresh_screen();
static GntWM *wm;
static GntClipboard *clipboard;
int gnt_need_conversation_to_locale;
#define HOLDING_ESCAPE  (escape_stuff.timer != 0)
static struct __unnamed_class___F0_L89_C8_unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__timer {
int timer;}escape_stuff;

static gboolean escape_timeout(gpointer data)
{
  gnt_wm_process_input(wm,"\033");
  escape_stuff.timer = 0;
  return 0;
}
/**
 * Mouse support:
 *  - bring a window on top if you click on its taskbar
 *  - click on the top-bar of the active window and drag+drop to move a window
 *  - click on a window to bring it to focus
 *   - allow scrolling in tree/textview on wheel-scroll event
 *   - click to activate button or select a row in tree
 *  wishlist:
 *   - have a little [X] on the windows, and clicking it will close that window.
 */

static gboolean detect_mouse_action(const char *buffer)
{
  int x;
  int y;
  static enum __unnamed_enum___F0_L115_C9_MOUSE_NONE__COMMA__MOUSE_LEFT__COMMA__MOUSE_RIGHT__COMMA__MOUSE_MIDDLE {MOUSE_NONE,MOUSE_LEFT,MOUSE_RIGHT,MOUSE_MIDDLE}button = MOUSE_NONE;
  static GntWidget *remember = (GntWidget *)((void *)0);
  static int offset = 0;
  GntMouseEvent event;
  GntWidget *widget = (GntWidget *)((void *)0);
  PANEL *p = (PANEL *)((void *)0);
  if (!(( *(wm -> cws)).ordered != 0) || (buffer[0] != 27)) 
    return 0;
  buffer++;
  if (strlen(buffer) < 5) 
    return 0;
  x = buffer[3];
  y = buffer[4];
  if (x < 0) 
    x += 256;
  if (y < 0) 
    y += 256;
  x -= 33;
  y -= 33;
{
    while((p = panel_below(p)) != ((PANEL *)((void *)0))){{
        const GntNode *node = (panel_userptr(p));
        GntWidget *wid;
        if (!(node != 0)) 
          continue; 
        wid = (node -> me);
        if ((x >= wid -> priv.x) && (x < (wid -> priv.x + wid -> priv.width))) {
          if ((y >= wid -> priv.y) && (y < (wid -> priv.y + wid -> priv.height))) {
            widget = wid;
            break; 
          }
        }
      }
    }
  }
  if (strncmp(buffer,"[M ",3) == 0) {
/* left button down */
/* Bring the window you clicked on to front */
/* If you click on the topbar, then you can drag to move the window */
    event = GNT_LEFT_MOUSE_DOWN;
  }
  else if (strncmp(buffer,"[M\"",3) == 0) {
/* right button down */
    event = GNT_RIGHT_MOUSE_DOWN;
  }
  else if (strncmp(buffer,"[M!",3) == 0) {
/* middle button down */
    event = GNT_MIDDLE_MOUSE_DOWN;
  }
  else if (strncmp(buffer,"[M`",3) == 0) {
/* wheel up*/
    event = GNT_MOUSE_SCROLL_UP;
  }
  else if (strncmp(buffer,"[Ma",3) == 0) {
/* wheel down */
    event = GNT_MOUSE_SCROLL_DOWN;
  }
  else if (strncmp(buffer,"[M#",3) == 0) {
/* button up */
    event = GNT_MOUSE_UP;
  }
  else 
    return 0;
  if ((widget != 0) && (gnt_wm_process_click(wm,event,x,y,widget) != 0)) 
    return 1;
  if ((((event == GNT_LEFT_MOUSE_DOWN) && (widget != 0)) && (widget != wm -> _list.window)) && !((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_TRANSIENT) != 0U)) {
    if (widget != ( *( *(wm -> cws)).ordered).data) {
      gnt_wm_raise_window(wm,widget);
    }
    if (y == widget -> priv.y) {
      offset = (x - widget -> priv.x);
      remember = widget;
      button = MOUSE_LEFT;
    }
  }
  else if (event == GNT_MOUSE_UP) {
    if ((button == MOUSE_NONE) && (y == ((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) - 1))) {
/* Clicked on the taskbar */
      int n = (g_list_length(( *(wm -> cws)).list));
      if (n != 0) {
        int width = ((((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)) / n);
        gnt_bindable_perform_action_named(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)wm),gnt_bindable_get_gtype()))),"switch-window-n",(x / width),((void *)((void *)0)));
      }
    }
    else if ((button == MOUSE_LEFT) && (remember != 0)) {
      x -= offset;
      if (x < 0) 
        x = 0;
      if (y < 0) 
        y = 0;
      gnt_screen_move_widget(remember,x,y);
    }
    button = MOUSE_NONE;
    remember = ((GntWidget *)((void *)0));
    offset = 0;
  }
  if (widget != 0) 
    gnt_widget_clicked(widget,event,x,y);
  return 1;
}

static gboolean io_invoke_error(GIOChannel *source,GIOCondition cond,gpointer data)
{
  int id = (gint )((glong )data);
  g_source_remove(id);
  g_io_channel_unref(source);
  channel = ((GIOChannel *)((void *)0));
  setup_io();
  return 1;
}

static gboolean io_invoke(GIOChannel *source,GIOCondition cond,gpointer null)
{
  char keys[256UL];
  gssize rd;
  char *k;
  char *cvrt = (char *)((void *)0);
  if ((wm -> mode) == GNT_KP_MODE_WAIT_ON_CHILD) 
    return 0;
  rd = read(0,(keys + (escape_stuff.timer != 0)),(sizeof(keys) - 1 - (escape_stuff.timer != 0)));
  if (rd < 0) {
/* This should return ERR, but let's see what it really returns */
    int ch = wgetch(stdscr);
    endwin();
    printf("ERROR: %s\n",strerror( *__errno_location()));
    printf("File descriptor is: %d\n\nGIOChannel is: %p\ngetch() = %d\n",0,source,ch);
    raise(6);
  }
  else if (rd == 0) {
    endwin();
    printf("EOF\n");
    raise(6);
  }
  rd += (escape_stuff.timer != 0);
  if (escape_stuff.timer != 0) {
    keys[0] = 27;
    g_source_remove(escape_stuff.timer);
    escape_stuff.timer = 0;
  }
  keys[rd] = 0;
  gnt_wm_set_event_stack(wm,1);
  cvrt = g_locale_to_utf8(keys,rd,((gsize *)(&rd)),0,0);
  k = ((cvrt != 0)?cvrt : keys);
  if ((mouse_enabled != 0) && (detect_mouse_action(k) != 0)) 
    goto end;
{
#if 0
/* I am not sure what's happening here. If this actually does something,
	 * then this needs to go in gnt_keys_refine. */
/* Alt not sending ESC* */
#endif
    while(rd != 0L){
      char back;
      int p;
      if ((k[0] == 27) && (rd == 1)) {
        escape_stuff.timer = (g_timeout_add(250,escape_timeout,0));
        break; 
      }
      gnt_keys_refine(k);
      p = ((1 > gnt_keys_find_combination(k))?1 : gnt_keys_find_combination(k));
      back = k[p];
      k[p] = 0;
/* XXX: */
      gnt_wm_process_input(wm,k);
      k[p] = back;
      rd -= p;
      k += p;
    }
  }
  end:
  if (wm != 0) 
    gnt_wm_set_event_stack(wm,0);
  g_free(cvrt);
  return 1;
}

static void setup_io()
{
  int result;
  channel = g_io_channel_unix_new(0);
  g_io_channel_set_close_on_unref(channel,1);
#if 0
#endif
  channel_read_callback = (result = (g_io_add_watch_full(channel,(-100),(G_IO_IN | G_IO_HUP | G_IO_ERR | G_IO_PRI),io_invoke,0,0)));
  channel_error_callback = g_io_add_watch_full(channel,(-100),G_IO_NVAL,io_invoke_error,((gpointer )((glong )result)),0);
/* Apparently this caused crashes for some people.
	                                 But irssi does this, so I am going to assume the
	                                 crashes were caused by some other stuff. */
  g_io_channel_unref(channel);
  g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: setting up IO (%d)","Main","void setup_io()",channel_read_callback);
}

static gboolean refresh_screen()
{
  gnt_bindable_perform_action_named(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)wm),gnt_bindable_get_gtype()))),"refresh-screen",((void *)((void *)0)));
  return 0;
}
/* Xerox */

static void clean_pid()
{
  int status;
  pid_t pid;
  do {
    pid = waitpid((-1),&status,1);
  }while ((pid != 0) && (pid != ((pid_t )(-1))));
  if ((pid == ((pid_t )(-1))) && ( *__errno_location() != 10)) {
    char errmsg[8192UL];
    g_snprintf(errmsg,8192,"Warning: waitpid() returned %d",pid);
    perror(errmsg);
  }
}

static void exit_confirmed(gpointer null)
{
  gnt_bindable_perform_action_named(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)wm),gnt_bindable_get_gtype()))),"wm-quit",((void *)((void *)0)));
}

static void exit_win_close(GntWidget *w,GntWidget **win)
{
   *win = ((GntWidget *)((void *)0));
}

static void ask_before_exit()
{
  static GntWidget *win = (GntWidget *)((void *)0);
  GntWidget *bbox;
  GntWidget *button;
  if ((wm -> menu) != 0) {
    do {
      gnt_widget_hide(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(wm -> menu)),gnt_widget_get_gtype()))));
      if ((wm -> menu) != 0) 
        wm -> menu = ( *(wm -> menu)).parentmenu;
    }while ((wm -> menu) != 0);
  }
  if (win != 0) 
    goto raise;
  win = gnt_window_box_new(0,1);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),gnt_label_new("Are you sure you want to quit\?"));
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),"Quit\?");
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),GNT_ALIGN_MID);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"destroy",((GCallback )exit_win_close),(&win),0,((GConnectFlags )0));
  bbox = gnt_box_new(0,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),bbox);
  button = gnt_button_new("Quit");
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )exit_confirmed),0,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gnt_box_get_gtype()))),button);
  button = gnt_button_new("Cancel");
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )gnt_widget_destroy),win,0,G_CONNECT_SWAPPED);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gnt_box_get_gtype()))),button);
  gnt_widget_show(win);
  raise:
  gnt_wm_raise_window(wm,win);
}
#ifdef SIGWINCH
static void (*org_winch_handler)(int );
#endif

static void sighandler(int sig)
{
  switch(sig){
#ifdef SIGWINCH
    case 28:
{
      werase(stdscr);
      g_idle_add(((GSourceFunc )refresh_screen),0);
      if (org_winch_handler != 0) 
        ( *org_winch_handler)(sig);
      signal(28,sighandler);
      break; 
    }
#endif
    case 17:
{
      clean_pid();
      signal(17,sighandler);
      break; 
    }
    case 2:
{
      ask_before_exit();
      signal(2,sighandler);
      break; 
    }
  }
}

static void init_wm()
{
  const char *name = gnt_style_get(GNT_STYLE_WM);
  gpointer handle;
  if ((name != 0) && (( *name) != 0)) {
    handle = (g_module_open(name,G_MODULE_BIND_LAZY));
    if (handle != 0) {
      gboolean (*init)(GntWM **);
      if (g_module_symbol(handle,"gntwm_init",((gpointer )(&init))) != 0) {
        ( *init)(&wm);
      }
    }
  }
  if (wm == ((GntWM *)((void *)0))) 
    wm = (g_object_new(gnt_wm_get_gtype(),0));
}

void gnt_init()
{
  char *filename;
  const char *locale;
  if (channel != 0) 
    return ;
  locale = (setlocale(6,""));
  setup_io();
#ifdef NO_WIDECHAR
#else
  if ((locale != 0) && ((strstr(locale,"UTF") != 0) || (strstr(locale,"utf") != 0))) {
    ascii_only = 0;
  }
  else {
    ascii_only = 1;
    gnt_need_conversation_to_locale = 1;
  }
#endif
  initscr();
  typeahead(-1);
  noecho();
  curs_set(0);
  gnt_init_keys();
  gnt_init_styles();
  filename = g_build_filename(g_get_home_dir(),".gntrc",((void *)((void *)0)));
  gnt_style_read_configure_file(filename);
  g_free(filename);
  gnt_init_colors();
  wbkgdset(stdscr,(0 | gnt_color_pair(GNT_COLOR_NORMAL)));
  wrefresh(stdscr);
#ifdef ALL_MOUSE_EVENTS
  if ((mouse_enabled = gnt_style_get_bool(GNT_STYLE_MOUSE,0)) != 0) 
    mousemask(((0010L << (5 - 1) * 6) - 1 | 0010L << (5 - 1) * 6),0);
#endif
  wbkgdset(stdscr,(0 | gnt_color_pair(GNT_COLOR_NORMAL)));
  werase(stdscr);
  wrefresh(stdscr);
#ifdef SIGWINCH
  org_winch_handler = signal(28,sighandler);
#endif
  signal(17,sighandler);
  signal(2,sighandler);
  signal(13,((__sighandler_t )((__sighandler_t )1)));
  g_type_init();
  init_wm();
  clipboard = (g_object_new(gnt_clipboard_get_gtype(),0));
}

void gnt_main()
{
  wm -> loop = g_main_loop_new(0,0);
  g_main_loop_run((wm -> loop));
}
/*********************************
 * Stuff for 'window management' *
 *********************************/

void gnt_window_present(GntWidget *window)
{
  if ((wm -> event_stack) != 0) 
    gnt_wm_raise_window(wm,window);
  else 
    gnt_widget_set_urgent(window);
}

void gnt_screen_occupy(GntWidget *widget)
{
  gnt_wm_new_window(wm,widget);
}

void gnt_screen_release(GntWidget *widget)
{
  if (wm != 0) 
    gnt_wm_window_close(wm,widget);
}

void gnt_screen_update(GntWidget *widget)
{
  gnt_wm_update_window(wm,widget);
}

gboolean gnt_widget_has_focus(GntWidget *widget)
{
  GntWidget *w;
  if (!(widget != 0)) 
    return 0;
  if ((({
    GTypeInstance *__inst = (GTypeInstance *)widget;
    GType __t = gnt_menu_get_gtype();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = 1;
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
    return 1;
  w = widget;
  while((widget -> parent) != 0)
    widget = (widget -> parent);
  if (widget == wm -> _list.window) 
    return 1;
  if ((( *(wm -> cws)).ordered != 0) && (( *( *(wm -> cws)).ordered).data == widget)) {
    if (((({
      GTypeInstance *__inst = (GTypeInstance *)widget;
      GType __t = gnt_box_get_gtype();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = 1;
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) && ((( *((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype())))).active == w) || (widget == w))) 
      return 1;
  }
  return 0;
}

void gnt_widget_set_urgent(GntWidget *widget)
{
  while((widget -> parent) != 0)
    widget = (widget -> parent);
  if ((( *(wm -> cws)).ordered != 0) && (( *( *(wm -> cws)).ordered).data == widget)) 
    return ;
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_URGENT;
  gnt_wm_update_window(wm,widget);
}

void gnt_quit()
{
/* Prevent io_invoke() from being called after wm is destroyed */
  g_source_remove(channel_error_callback);
  g_source_remove(channel_read_callback);
  channel_error_callback = 0;
  channel_read_callback = 0;
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)wm),((GType )(20 << 2))))));
  wm = ((GntWM *)((void *)0));
  update_panels();
  doupdate();
  gnt_uninit_colors();
  gnt_uninit_styles();
  endwin();
}

gboolean gnt_ascii_only()
{
  return ascii_only;
}

void gnt_screen_resize_widget(GntWidget *widget,int width,int height)
{
  gnt_wm_resize_window(wm,widget,width,height);
}

void gnt_screen_move_widget(GntWidget *widget,int x,int y)
{
  gnt_wm_move_window(wm,widget,x,y);
}

void gnt_screen_rename_widget(GntWidget *widget,const char *text)
{
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype()))),text);
  gnt_widget_draw(widget);
  gnt_wm_update_window(wm,widget);
}

void gnt_register_action(const char *label,void (*callback)())
{
  GntAction *action = (GntAction *)(g_malloc0_n(1,(sizeof(GntAction ))));
  action -> label = (g_strdup(label));
  action -> callback = callback;
  wm -> acts = g_list_append((wm -> acts),action);
}

static void reset_menu(GntWidget *widget,gpointer null)
{
  wm -> menu = ((GntMenu *)((void *)0));
}

gboolean gnt_screen_menu_show(gpointer newmenu)
{
  if ((wm -> menu) != 0) {
/* For now, if a menu is being displayed, then another menu
		 * can NOT take over. */
    return 0;
  }
  wm -> menu = newmenu;
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(wm -> menu)),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_INVISIBLE);
  gnt_widget_draw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(wm -> menu)),gnt_widget_get_gtype()))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(wm -> menu)),((GType )(20 << 2))))),"hide",((GCallback )reset_menu),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(wm -> menu)),((GType )(20 << 2))))),"destroy",((GCallback )reset_menu),0,0,((GConnectFlags )0));
  return 1;
}

void gnt_set_clipboard_string(const gchar *string)
{
  gnt_clipboard_set_string(clipboard,string);
}

GntClipboard *gnt_get_clipboard()
{
  return clipboard;
}

gchar *gnt_get_clipboard_string()
{
  return gnt_clipboard_get_string(clipboard);
}
#if GLIB_CHECK_VERSION(2,4,0)
typedef struct __unnamed_class___F0_L679_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb___Fb_v_Gb_i__sep__L11R_Fe___Pe___variable_name_unknown_scope_and_name__scope__callback__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L11R_variable_name_unknown_scope_and_name__scope__data {
void (*callback)(int , gpointer );
gpointer data;}ChildProcess;

static void reap_child(GPid pid,gint status,gpointer data)
{
  ChildProcess *cp = data;
  if ((cp -> callback) != 0) {
    ( *(cp -> callback))(status,(cp -> data));
  }
  g_free(cp);
  clean_pid();
  wm -> mode = GNT_KP_MODE_NORMAL;
  endwin();
  setup_io();
  wrefresh(stdscr);
  refresh_screen();
}
#endif

gboolean gnt_giveup_console(const char *wd,char **argv,char **envp,gint *stin,gint *stout,gint *sterr,void (*callback)(int , gpointer ),gpointer data)
{
#if GLIB_CHECK_VERSION(2,4,0)
  GPid pid = 0;
  ChildProcess *cp = (ChildProcess *)((void *)0);
  if (!(g_spawn_async_with_pipes(wd,argv,envp,(G_SPAWN_SEARCH_PATH | G_SPAWN_DO_NOT_REAP_CHILD),((GSpawnChildSetupFunc )endwin),0,&pid,stin,stout,sterr,0) != 0)) 
    return 0;
  cp = ((ChildProcess *)(g_malloc0_n(1,(sizeof(ChildProcess )))));
  cp -> callback = callback;
  cp -> data = data;
  g_source_remove(channel_read_callback);
  wm -> mode = GNT_KP_MODE_WAIT_ON_CHILD;
  g_child_watch_add(pid,reap_child,cp);
  return 1;
#else
#endif
}

gboolean gnt_is_refugee()
{
#if GLIB_CHECK_VERSION(2,4,0)
  return (wm != 0) && ((wm -> mode) == GNT_KP_MODE_WAIT_ON_CHILD);
#else
#endif
}

const char *C_(const char *x)
{
  static char *c = (char *)((void *)0);
  if (gnt_need_conversation_to_locale != 0) {
    GError *error = (GError *)((void *)0);
    g_free(c);
    c = g_locale_from_utf8(x,(-1),0,0,&error);
    if ((c == ((char *)((void *)0))) || (error != 0)) {
      char *store = c;
      c = ((char *)((void *)0));
      g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: Error: %s\n","Main","const char *C_(const char *)",((error != 0)?(error -> message) : "(unknown)"));
      g_error_free(error);
      error = ((GError *)((void *)0));
      g_free(c);
      c = store;
    }
    return (c != 0)?c : x;
  }
  else 
    return x;
}
