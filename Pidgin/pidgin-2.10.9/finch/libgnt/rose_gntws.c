/*
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <gmodule.h>
#include "gntinternal.h"
#include "gntbox.h"
#include "gntwidget.h"
#include "gntwindow.h"
#include "gntwm.h"
#include "gntws.h"

static void widget_hide(gpointer data,gpointer nodes)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)data),gnt_widget_get_gtype()));
  GntNode *node = (g_hash_table_lookup(nodes,widget));
  if ((({
    GTypeInstance *__inst = (GTypeInstance *)widget;
    GType __t = gnt_window_get_gtype();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = 1;
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
    gnt_window_workspace_hiding(((GntWindow *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_window_get_gtype()))));
  if (node != 0) 
    hide_panel((node -> panel));
}

static void widget_show(gpointer data,gpointer nodes)
{
  GntNode *node = (g_hash_table_lookup(nodes,data));
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)data),gnt_widget_get_gtype())))),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_INVISIBLE);
  if (node != 0) {
    show_panel((node -> panel));
    gnt_wm_copy_win(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)data),gnt_widget_get_gtype()))),node);
  }
}

void gnt_ws_draw_taskbar(GntWS *ws,gboolean reposition)
{
  static WINDOW *taskbar = (WINDOW *)((void *)0);
  GList *iter;
  int n;
  int width = 0;
  int i;
  if (gnt_is_refugee() != 0) 
    return ;
  if (taskbar == ((WINDOW *)((void *)0))) {
    taskbar = newwin(1,((stdscr != 0)?((stdscr -> _maxx) + 1) : -1),((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) - 1),0);
  }
  else if (reposition != 0) {
    int Y_MAX = ((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) - 1);
    mvwin(taskbar,Y_MAX,0);
  }
  wbkgdset(taskbar,(0 | gnt_color_pair(GNT_COLOR_NORMAL)));
  werase(taskbar);
  n = (g_list_length((ws -> list)));
  if (n != 0) 
    width = ((((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)) / n);
  for (((i = 0) , (iter = (ws -> list))); iter != 0; ((iter = (iter -> next)) , i++)) {
    GntWidget *w = (iter -> data);
    int color;
    const char *title;
    if (w == ( *(ws -> ordered)).data) {
/* This is the current window in focus */
      color = GNT_COLOR_TITLE;
    }
    else if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)w),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_URGENT) != 0U) {
/* This is a window with the URGENT hint set */
      color = GNT_COLOR_URGENT;
    }
    else {
      color = GNT_COLOR_NORMAL;
    }
    wbkgdset(taskbar,(0 | gnt_color_pair(color)));
    if ((iter -> next) != 0) 
      (wmove(taskbar,0,(width * i)) == -1)?-1 : whline(taskbar,(32 | gnt_color_pair(color)),width);
    else 
      (wmove(taskbar,0,(width * i)) == -1)?-1 : whline(taskbar,(32 | gnt_color_pair(color)),((((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)) - (width * i)));
    title = ( *((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)w),gnt_box_get_gtype())))).title;
    mvwprintw(taskbar,0,(width * i),"%s",((title != 0)?C_(title) : "<gnt>"));
    if (i != 0) 
      (wmove(taskbar,0,((width * i) - 1)) == -1)?-1 : waddch(taskbar,((acs_map[(unsigned char )'x'] | (((chtype )1UL) << 8 + 8)) | (gnt_color_pair(GNT_COLOR_NORMAL))));
  }
  wrefresh(taskbar);
}

static void gnt_ws_init(GTypeInstance *instance,gpointer class)
{
  GntWS *ws = (GntWS *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_ws_get_gtype()));
  ws -> list = ((GList *)((void *)0));
  ws -> ordered = ((GList *)((void *)0));
  ws -> name = ((char *)((void *)0));
}

void gnt_ws_add_widget(GntWS *ws,GntWidget *wid)
{
  GntWidget *oldfocus;
  oldfocus = ((((ws -> ordered) != 0)?( *(ws -> ordered)).data : ((void *)((void *)0))));
  ws -> list = g_list_append((ws -> list),wid);
  ws -> ordered = g_list_prepend((ws -> ordered),wid);
  if (oldfocus != 0) 
    gnt_widget_set_focus(oldfocus,0);
}

void gnt_ws_remove_widget(GntWS *ws,GntWidget *wid)
{
  ws -> list = g_list_remove((ws -> list),wid);
  ws -> ordered = g_list_remove((ws -> ordered),wid);
}

void gnt_ws_set_name(GntWS *ws,const gchar *name)
{
  g_free((ws -> name));
  ws -> name = g_strdup(name);
}

void gnt_ws_hide(GntWS *ws,GHashTable *nodes)
{
  g_list_foreach((ws -> ordered),widget_hide,nodes);
}

void gnt_ws_widget_hide(GntWidget *widget,GHashTable *nodes)
{
  widget_hide(widget,nodes);
}

void gnt_ws_widget_show(GntWidget *widget,GHashTable *nodes)
{
  widget_show(widget,nodes);
}

void gnt_ws_show(GntWS *ws,GHashTable *nodes)
{
  GList *l;
  for (l = g_list_last((ws -> ordered)); l != 0; l = ((l != 0)?( *((GList *)l)).prev : ((struct _GList *)((void *)0)))) 
    widget_show((l -> data),nodes);
}

GType gnt_ws_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(GntWSClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )((void *)0)), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GntWS ))), (0), (gnt_ws_init), ((const GTypeValueTable *)((void *)0))
/* base_init		*/
/* base_finalize	*/
/*(GClassInitFunc)gnt_ws_class_init,*/
/* class_data		*/
/* n_preallocs		*/
/* instance_init	*/
/* value_table		*/
};
    type = g_type_register_static(gnt_bindable_get_gtype(),"GntWS",&info,0);
  }
  return type;
}

GntWS *gnt_ws_new(const char *name)
{
  GntWS *ws = (GntWS *)(g_type_check_instance_cast(((GTypeInstance *)(g_object_new(gnt_ws_get_gtype(),0))),gnt_ws_get_gtype()));
  ws -> name = g_strdup(((name != 0)?name : "(noname)"));
  return ws;
}

const char *gnt_ws_get_name(GntWS *ws)
{
  return (ws -> name);
}
