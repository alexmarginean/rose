/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "config.h"
#include "gntinternal.h"
#undef GNT_LOG_DOMAIN
#define GNT_LOG_DOMAIN "Utils"
#include "gntbutton.h"
#include "gntcheckbox.h"
#include "gntcombobox.h"
#include "gntentry.h"
#include "gntlabel.h"
#include "gntline.h"
#include "gnttextview.h"
#include "gnttree.h"
#include "gntutils.h"
#include "gntwindow.h"
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#ifndef NO_LIBXML
#include <libxml/parser.h>
#include <libxml/tree.h>
#endif

void gnt_util_get_text_bound(const char *text,int *width,int *height)
{
  const char *s = text;
  const char *last;
  int count = 1;
  int max = 0;
  int len;
/* XXX: ew ... everyone look away */
  last = s;
  if (s != 0) {
    while(( *s) != 0){
      if ((( *s) == 10) || (( *s) == 13)) {
        count++;
        len = gnt_util_onscreen_width(last,s);
        if (max < len) 
          max = len;
        last = (s + 1);
      }
      s = ((char *)(s + g_utf8_skip[ *((const guchar *)s)]));
    }
    len = gnt_util_onscreen_width(last,s);
    if (max < len) 
      max = len;
  }
  if (height != 0) 
     *height = count;
  if (width != 0) 
     *width = (max + (count > 1));
}

int gnt_util_onscreen_width(const char *start,const char *end)
{
  int width = 0;
  if (end == ((const char *)((void *)0))) 
    end = (start + strlen(start));
  while(start < end){
    width += ((g_unichar_iswide(g_utf8_get_char(start)) != 0)?2 : 1);
    start = ((char *)(start + g_utf8_skip[ *((const guchar *)start)]));
  }
  return width;
}

const char *gnt_util_onscreen_width_to_pointer(const char *string,int len,int *w)
{
  int size;
  int width = 0;
  const char *str = string;
  if (len <= 0) {
    len = gnt_util_onscreen_width(string,0);
  }
{
    while((width < len) && (( *str) != 0)){
      size = ((g_unichar_iswide(g_utf8_get_char(str)) != 0)?2 : 1);
      if ((width + size) > len) 
        break; 
      str = ((char *)(str + g_utf8_skip[ *((const guchar *)str)]));
      width += size;
    }
  }
  if (w != 0) 
     *w = width;
  return str;
}

char *gnt_util_onscreen_fit_string(const char *string,int maxw)
{
  const char *start;
  const char *end;
  GString *str;
  if (maxw <= 0) 
    maxw = ((((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)) - 4);
  start = string;
  str = g_string_new(0);
  while(( *start) != 0){
    if (((end = (strchr(start,10))) != ((const char *)((void *)0))) || ((end = (strchr(start,13))) != ((const char *)((void *)0)))) {
      if (gnt_util_onscreen_width(start,end) > maxw) 
        end = ((const char *)((void *)0));
    }
    if (end == ((const char *)((void *)0))) 
      end = gnt_util_onscreen_width_to_pointer(start,maxw,0);
    str = g_string_append_len(str,start,(end - start));
    if (( *end) != 0) {
      str = g_string_append_c_inline(str,10);
      if ((( *end) == 10) || (( *end) == 13)) 
        end++;
    }
    start = end;
  }
  return g_string_free(str,0);
}

struct duplicate_fns 
{
  GDupFunc key_dup;
  GDupFunc value_dup;
  GHashTable *table;
}
;

static void duplicate_values(gpointer key,gpointer value,gpointer data)
{
  struct duplicate_fns *fns = data;
  g_hash_table_insert((fns -> table),(((fns -> key_dup) != 0)?( *(fns -> key_dup))(key) : key),(((fns -> value_dup) != 0)?( *(fns -> value_dup))(value) : value));
}

GHashTable *g_hash_table_duplicate(GHashTable *src,GHashFunc hash,GEqualFunc equal,GDestroyNotify key_d,GDestroyNotify value_d,GDupFunc key_dup,GDupFunc value_dup)
{
  GHashTable *dest = g_hash_table_new_full(hash,equal,key_d,value_d);
  struct duplicate_fns fns = {(key_dup), (value_dup), (dest)};
  g_hash_table_foreach(src,duplicate_values,(&fns));
  return dest;
}

gboolean gnt_boolean_handled_accumulator(GSignalInvocationHint *ihint,GValue *return_accu,const GValue *handler_return,gpointer dummy)
{
  gboolean continue_emission;
  gboolean signal_handled;
  signal_handled = g_value_get_boolean(handler_return);
  g_value_set_boolean(return_accu,signal_handled);
  continue_emission = !(signal_handled != 0);
  return continue_emission;
}
typedef struct __unnamed_class___F0_L189_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L54R__Pe___variable_name_unknown_scope_and_name__scope__hash__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntTree_GntTree__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__tree {
GHashTable *hash;
GntTree *tree;}BindingView;

static void add_binding(gpointer key,gpointer value,gpointer data)
{
  BindingView *bv = data;
  GntBindableActionParam *act = value;
  const char *name = (g_hash_table_lookup((bv -> hash),(act -> action)));
  if ((name != 0) && (( *name) != 0)) {
    const char *k = gnt_key_lookup(key);
    if (!(k != 0)) 
      k = key;
    gnt_tree_add_row_after((bv -> tree),((gpointer )k),gnt_tree_create_row((bv -> tree),k,name),0,0);
  }
}

static void add_action(gpointer key,gpointer value,gpointer data)
{
  BindingView *bv = data;
  g_hash_table_insert((bv -> hash),value,key);
}

GntWidget *gnt_widget_bindings_view(GntWidget *widget)
{
  GntBindable *bind = (GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_bindable_get_gtype()));
  GntWidget *tree = gnt_tree_new_with_columns(2);
  GntBindableClass *klass = (GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)((GntBindableClass *)( *((GTypeInstance *)bind)).g_class)),gnt_bindable_get_gtype()));
  GHashTable *hash = g_hash_table_new(g_direct_hash,g_direct_equal);
  BindingView bv = {(hash), ((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype())))};
  gnt_tree_set_compare_func(bv.tree,((GCompareFunc )g_utf8_collate));
  g_hash_table_foreach((klass -> actions),add_action,(&bv));
  g_hash_table_foreach((klass -> bindings),add_binding,(&bv));
  if (( *((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype())))).list == ((GList *)((void *)0))) {
    gnt_widget_destroy(tree);
    tree = ((GntWidget *)((void *)0));
  }
  else 
    gnt_tree_adjust_columns(bv.tree);
  g_hash_table_destroy(hash);
  return tree;
}
#ifndef NO_LIBXML

static GntWidget *gnt_widget_from_xmlnode(xmlNode *node,GntWidget **data[],int max)
{
  GntWidget *widget = (GntWidget *)((void *)0);
  char *name;
  char *id;
  char *prop;
  char *content;
  int val;
  if (((node == ((xmlNode *)((void *)0))) || ((node -> name) == ((const xmlChar *)((void *)0)))) || ((node -> type) != XML_ELEMENT_NODE)) 
    return 0;
  name = ((char *)(node -> name));
  content = ((char *)(xmlNodeGetContent(node)));
  if ((strcmp((name + 1),"window") == 0) || (strcmp((name + 1),"box") == 0)) {
    xmlNode *ch;
    char *title;
    gboolean vert = (( *name) == 'v');
    if (name[1] == 'w') 
      widget = gnt_window_box_new(0,vert);
    else 
      widget = gnt_box_new(0,vert);
    title = ((char *)(xmlGetProp(node,((xmlChar *)"title"))));
    if (title != 0) {
      gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype()))),title);
      ( *xmlFree)(title);
    }
    prop = ((char *)(xmlGetProp(node,((xmlChar *)"fill"))));
    if (prop != 0) {
      if (sscanf(prop,"%d",&val) == 1) 
        gnt_box_set_fill(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype()))),!(!(val != 0)));
      ( *xmlFree)(prop);
    }
    prop = ((char *)(xmlGetProp(node,((xmlChar *)"align"))));
    if (prop != 0) {
      if (sscanf(prop,"%d",&val) == 1) 
        gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype()))),val);
      ( *xmlFree)(prop);
    }
    prop = ((char *)(xmlGetProp(node,((xmlChar *)"pad"))));
    if (prop != 0) {
      if (sscanf(prop,"%d",&val) == 1) 
        gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype()))),val);
      ( *xmlFree)(prop);
    }
    for (ch = (node -> children); ch != 0; ch = (ch -> next)) 
      gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype()))),gnt_widget_from_xmlnode(ch,data,max));
  }
  else if (strcmp(name,"button") == 0) {
    widget = gnt_button_new(content);
  }
  else if (strcmp(name,"label") == 0) {
    widget = gnt_label_new(content);
  }
  else if (strcmp(name,"entry") == 0) {
    widget = gnt_entry_new(content);
  }
  else if (strcmp(name,"combobox") == 0) {
    widget = gnt_combo_box_new();
  }
  else if (strcmp(name,"checkbox") == 0) {
    widget = gnt_check_box_new(content);
  }
  else if (strcmp(name,"tree") == 0) {
    widget = gnt_tree_new();
  }
  else if (strcmp(name,"textview") == 0) {
    widget = gnt_text_view_new();
  }
  else if (strcmp((name + 1),"line") == 0) {
    widget = gnt_line_new((( *name) == 'v'));
  }
  ( *xmlFree)(content);
  if (widget == ((GntWidget *)((void *)0))) {
    g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: Invalid widget name %s","Utils","struct _GntWidget *gnt_widget_from_xmlnode(struct _xmlNode *, struct _GntWidget ***, int)",name);
    return 0;
  }
  id = ((char *)(xmlGetProp(node,((xmlChar *)"id"))));
  if (id != 0) {
    int i;
    if (((sscanf(id,"%d",&i) == 1) && (i >= 0)) && (i < max)) {
       *data[i] = widget;
      ( *xmlFree)(id);
    }
  }
  prop = ((char *)(xmlGetProp(node,((xmlChar *)"border"))));
  if (prop != 0) {
    int val;
    if (sscanf(prop,"%d",&val) == 1) {
      if (val != 0) 
        ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_NO_BORDER);
      else 
        ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_NO_BORDER;
    }
    ( *xmlFree)(prop);
  }
  prop = ((char *)(xmlGetProp(node,((xmlChar *)"shadow"))));
  if (prop != 0) {
    int val;
    if (sscanf(prop,"%d",&val) == 1) {
      if (val != 0) 
        ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_NO_BORDER);
      else 
        ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_NO_BORDER;
    }
    ( *xmlFree)(prop);
  }
  return widget;
}
#endif

void gnt_util_parse_widgets(const char *string,int num,... )
{
#ifndef NO_LIBXML
  xmlParserCtxtPtr ctxt;
  xmlDocPtr doc;
  xmlNodePtr node;
  va_list list;
  GntWidget ***data;
  int id;
  ctxt = xmlNewParserCtxt();
  doc = xmlCtxtReadDoc(ctxt,((xmlChar *)string),0,0,XML_PARSE_NOBLANKS);
  data = ((GntWidget ***)(g_malloc0_n(num,(sizeof(GntWidget **)))));
  va_start(list,num);
  for (id = 0; id < num; id++) 
    data[id] = (va_arg(list,gpointer ));
  node = xmlDocGetRootElement(doc);
  gnt_widget_from_xmlnode(node,data,num);
  xmlFreeDoc(doc);
  xmlFreeParserCtxt(ctxt);
  va_end(list);
  g_free(data);
#endif
}
#ifndef NO_LIBXML

static void util_parse_html_to_tv(xmlNode *node,GntTextView *tv,GntTextFormatFlags flag)
{
  const char *name;
  char *content;
  xmlNode *ch;
  char *url = (char *)((void *)0);
  gboolean insert_nl_s = 0;
  gboolean insert_nl_e = 0;
  if (((node == ((xmlNode *)((void *)0))) || ((node -> name) == ((const xmlChar *)((void *)0)))) || ((node -> type) != XML_ELEMENT_NODE)) 
    return ;
  name = ((char *)(node -> name));
  if ((((g_ascii_strcasecmp(name,"b") == 0) || (g_ascii_strcasecmp(name,"strong") == 0)) || (g_ascii_strcasecmp(name,"i") == 0)) || (g_ascii_strcasecmp(name,"blockquote") == 0)) {
    flag |= GNT_TEXT_FLAG_BOLD;
  }
  else if (g_ascii_strcasecmp(name,"u") == 0) {
    flag |= GNT_TEXT_FLAG_UNDERLINE;
  }
  else if (g_ascii_strcasecmp(name,"br") == 0) {
    insert_nl_e = 1;
  }
  else if (g_ascii_strcasecmp(name,"a") == 0) {
    flag |= GNT_TEXT_FLAG_UNDERLINE;
    url = ((char *)(xmlGetProp(node,((xmlChar *)"href"))));
  }
  else if ((((((g_ascii_strcasecmp(name,"h1") == 0) || (g_ascii_strcasecmp(name,"h2") == 0)) || (g_ascii_strcasecmp(name,"h3") == 0)) || (g_ascii_strcasecmp(name,"h4") == 0)) || (g_ascii_strcasecmp(name,"h5") == 0)) || (g_ascii_strcasecmp(name,"h6") == 0)) {
    insert_nl_s = 1;
    insert_nl_e = 1;
  }
  else if (g_ascii_strcasecmp(name,"title") == 0) {
    insert_nl_s = 1;
    insert_nl_e = 1;
    flag |= (GNT_TEXT_FLAG_BOLD | GNT_TEXT_FLAG_UNDERLINE);
  }
  else {
/* XXX: Process other possible tags */
  }
  if (insert_nl_s != 0) 
    gnt_text_view_append_text_with_flags(tv,"\n",flag);
  for (ch = (node -> children); ch != 0; ch = (ch -> next)) {
    if ((ch -> type) == XML_ELEMENT_NODE) {
      util_parse_html_to_tv(ch,tv,flag);
    }
    else if ((ch -> type) == XML_TEXT_NODE) {
      content = ((char *)(xmlNodeGetContent(ch)));
      gnt_text_view_append_text_with_flags(tv,content,flag);
      ( *xmlFree)(content);
    }
  }
  if (url != 0) {
    char *href = g_strdup_printf(" (%s)",url);
    gnt_text_view_append_text_with_flags(tv,href,flag);
    g_free(href);
    ( *xmlFree)(url);
  }
  if (insert_nl_e != 0) 
    gnt_text_view_append_text_with_flags(tv,"\n",flag);
}
#endif

gboolean gnt_util_parse_xhtml_to_textview(const char *string,GntTextView *tv)
{
#ifdef NO_LIBXML
#else
  xmlParserCtxtPtr ctxt;
  xmlDocPtr doc;
  xmlNodePtr node;
  GntTextFormatFlags flag = GNT_TEXT_FLAG_NORMAL;
  gboolean ret = 0;
  ctxt = xmlNewParserCtxt();
  doc = xmlCtxtReadDoc(ctxt,((xmlChar *)string),0,0,XML_PARSE_NOBLANKS | XML_PARSE_RECOVER);
  if (doc != 0) {
    node = xmlDocGetRootElement(doc);
    util_parse_html_to_tv(node,tv,flag);
    xmlFreeDoc(doc);
    ret = 1;
  }
  xmlFreeParserCtxt(ctxt);
  return ret;
#endif
}
/* Setup trigger widget */
typedef struct __unnamed_class___F0_L473_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__text__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__button {
char *text;
GntWidget *button;}TriggerButton;

static void free_trigger_button(TriggerButton *b)
{
  g_free((b -> text));
  g_free(b);
}

static gboolean key_pressed(GntWidget *widget,const char *text,TriggerButton *trig)
{
  if (((text != 0) && ((trig -> text) != 0)) && (strcmp(text,(trig -> text)) == 0)) {
    gnt_widget_activate((trig -> button));
    return 1;
  }
  return 0;
}

void gnt_util_set_trigger_widget(GntWidget *wid,const char *text,GntWidget *button)
{
  TriggerButton *tb = (TriggerButton *)(g_malloc0_n(1,(sizeof(TriggerButton ))));
  tb -> text = g_strdup(text);
  tb -> button = button;
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)wid),((GType )(20 << 2))))),"key_pressed",((GCallback )key_pressed),tb,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"destroy",((GCallback )free_trigger_button),tb,0,G_CONNECT_SWAPPED);
}
