/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 **/
#include "gntinternal.h"
#include "gntprogressbar.h"
#include "gntutils.h"
#include <string.h>
typedef struct _GntProgressBarPrivate {
gdouble fraction;
gboolean show_value;
GntProgressBarOrientation orientation;}GntProgressBarPrivate;

struct _GntProgressBar 
{
  GntWidget parent;
#if !GLIB_CHECK_VERSION(2,4,0)
#endif
}
;
#if GLIB_CHECK_VERSION(2,4,0)
#define GNT_PROGRESS_BAR_GET_PRIVATE(o)   (G_TYPE_INSTANCE_GET_PRIVATE ((o), GNT_TYPE_PROGRESS_BAR, GntProgressBarPrivate))
#else
#define GNT_PROGRESS_BAR_GET_PRIVATE(o)   &(GNT_PROGRESS_BAR(o)->priv)
#endif
static GntWidgetClass *parent_class = (GntWidgetClass *)((void *)0);

static void gnt_progress_bar_draw(GntWidget *widget)
{
  GntProgressBarPrivate *priv = (GntProgressBarPrivate *)(g_type_instance_get_private(((GTypeInstance *)((GntProgressBar *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_progress_bar_get_type())))),gnt_progress_bar_get_type()));
  gchar progress[8UL];
  gint start;
  gint end;
  gint i;
  gint pos;
  int color;
  g_snprintf(progress,(sizeof(progress)),"%.1f%%",((priv -> fraction) * 100));
  color = gnt_color_pair(GNT_COLOR_NORMAL);
  switch(priv -> orientation){
    case GNT_PROGRESS_LEFT_TO_RIGHT:
{
    }
    case GNT_PROGRESS_RIGHT_TO_LEFT:
{
      start = ((((priv -> orientation) == GNT_PROGRESS_LEFT_TO_RIGHT)?0 : ((1.0 - (priv -> fraction)) * widget -> priv.width)));
      end = ((((priv -> orientation) == GNT_PROGRESS_LEFT_TO_RIGHT)?(widget -> priv.width * (priv -> fraction)) : widget -> priv.width));
/* background */
      for (i = 0; i < widget -> priv.height; i++) 
        (wmove((widget -> window),i,0) == -1)?-1 : whline((widget -> window),(32 | color),widget -> priv.width);
/* foreground */
      for (i = 0; i < widget -> priv.height; i++) 
        (wmove((widget -> window),i,start) == -1)?-1 : whline((widget -> window),((acs_map[(unsigned char )'a'] | color) | (((chtype )1UL) << 10 + 8)),end);
/* text */
      if ((priv -> show_value) != 0) {
        pos = ((widget -> priv.width / 2) - (strlen(progress) / 2));
        for (i = 0; i < progress[i]; (i++ , pos++)) {
          ((widget -> window) != 0)?(((( *(widget -> window))._attrs = (color | ((((pos < start) || (pos > end))?1UL - 1UL : (((chtype )1UL) << 10 + 8))))) , 0)) : -1;
          mvwprintw((widget -> window),(widget -> priv.height / 2),pos,"%c",progress[i]);
        }
        ((widget -> window) != 0)?(((( *(widget -> window))._attrs = ((attr_t )color)) , 0)) : -1;
      }
      break; 
    }
    case GNT_PROGRESS_BOTTOM_TO_TOP:
{
    }
    case GNT_PROGRESS_TOP_TO_BOTTOM:
{
      start = ((((priv -> orientation) == GNT_PROGRESS_TOP_TO_BOTTOM)?0 : ((1.0 - (priv -> fraction)) * widget -> priv.height)));
      end = ((((priv -> orientation) == GNT_PROGRESS_TOP_TO_BOTTOM)?(widget -> priv.height * (priv -> fraction)) : widget -> priv.height));
/* background */
      for (i = 0; i < widget -> priv.width; i++) 
        (wmove((widget -> window),0,i) == -1)?-1 : wvline((widget -> window),(32 | color),widget -> priv.height);
/* foreground */
      for (i = 0; i < widget -> priv.width; i++) 
        (wmove((widget -> window),start,i) == -1)?-1 : wvline((widget -> window),((acs_map[(unsigned char )'a'] | color) | (((chtype )1UL) << 10 + 8)),end);
/* text */
      if ((priv -> show_value) != 0) {
        pos = ((widget -> priv.height / 2) - (strlen(progress) / 2));
        for (i = 0; i < progress[i]; (i++ , pos++)) {
          ((widget -> window) != 0)?(((( *(widget -> window))._attrs = (color | ((((pos < start) || (pos > end))?1UL - 1UL : (((chtype )1UL) << 10 + 8))))) , 0)) : -1;
          mvwprintw((widget -> window),pos,(widget -> priv.width / 2),"%c\n",progress[i]);
        }
        ((widget -> window) != 0)?(((( *(widget -> window))._attrs = ((attr_t )color)) , 0)) : -1;
      }
      break; 
    }
    default:
{
      do {
        g_assertion_message_expr("Gnt","gntprogressbar.c",114,((const char *)__func__),0);
      }while (0);
    }
  }
}

static void gnt_progress_bar_size_request(GntWidget *widget)
{
  gnt_widget_set_size(widget,widget -> priv.minw,widget -> priv.minh);
}

static void gnt_progress_bar_class_init(gpointer klass,gpointer class_data)
{
  GObjectClass *g_class = (GObjectClass *)(g_type_check_class_cast(((GTypeClass *)klass),((GType )(20 << 2))));
  parent_class = ((GntWidgetClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_widget_get_gtype())));
#if GLIB_CHECK_VERSION(2,4,0)
  g_type_class_add_private(g_class,(sizeof(GntProgressBarPrivate )));
#endif
  parent_class -> draw = gnt_progress_bar_draw;
  parent_class -> size_request = gnt_progress_bar_size_request;
}

static void gnt_progress_bar_init(GTypeInstance *instance,gpointer g_class)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_widget_get_gtype()));
  GntProgressBarPrivate *priv = (GntProgressBarPrivate *)(g_type_instance_get_private(((GTypeInstance *)((GntProgressBar *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_progress_bar_get_type())))),gnt_progress_bar_get_type()));
  gnt_widget_set_take_focus(widget,0);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= (GNT_WIDGET_NO_BORDER | GNT_WIDGET_NO_SHADOW | GNT_WIDGET_GROW_X);
  widget -> priv.minw = 8;
  widget -> priv.minh = 1;
  priv -> show_value = 1;
}

GType gnt_progress_bar_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(GntProgressBarClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), (gnt_progress_bar_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GntProgressBar ))), (0), (gnt_progress_bar_init), ((const GTypeValueTable *)((void *)0))
/* base_init */
/* base_finalize */
/* class_init */
/* class_finalize */
/* class_data */
/* n_preallocs */
/* instance_init */
/* value_table */
};
    type = g_type_register_static(gnt_widget_get_gtype(),"GntProgressBar",&info,0);
  }
  return type;
}

GntWidget *gnt_progress_bar_new()
{
  GntWidget *widget = (g_object_new(gnt_progress_bar_get_type(),0));
  return widget;
}

void gnt_progress_bar_set_fraction(GntProgressBar *pbar,gdouble fraction)
{
  GntProgressBarPrivate *priv = (GntProgressBarPrivate *)(g_type_instance_get_private(((GTypeInstance *)pbar),gnt_progress_bar_get_type()));
  if (fraction > 1.0) 
    priv -> fraction = 1.0;
  else if (fraction < 0.0) 
    priv -> fraction = 0.0;
  else 
    priv -> fraction = fraction;
  if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)pbar),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U) 
    gnt_widget_draw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)pbar),gnt_widget_get_gtype()))));
}

void gnt_progress_bar_set_orientation(GntProgressBar *pbar,GntProgressBarOrientation orientation)
{
  GntProgressBarPrivate *priv = (GntProgressBarPrivate *)(g_type_instance_get_private(((GTypeInstance *)pbar),gnt_progress_bar_get_type()));
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)pbar),gnt_widget_get_gtype()));
  priv -> orientation = orientation;
  if ((orientation == GNT_PROGRESS_LEFT_TO_RIGHT) || (orientation == GNT_PROGRESS_RIGHT_TO_LEFT)) {
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)pbar),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_GROW_X;
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)pbar),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_GROW_Y);
    widget -> priv.minw = 8;
    widget -> priv.minh = 1;
  }
  else {
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)pbar),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_GROW_X);
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)pbar),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_GROW_Y;
    widget -> priv.minw = 1;
    widget -> priv.minh = 8;
  }
  if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)pbar),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U) 
    gnt_widget_draw(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)pbar),gnt_widget_get_gtype()))));
}

void gnt_progress_bar_set_show_progress(GntProgressBar *pbar,gboolean show)
{
  GntProgressBarPrivate *priv = (GntProgressBarPrivate *)(g_type_instance_get_private(((GTypeInstance *)pbar),gnt_progress_bar_get_type()));
  priv -> show_value = show;
}

gdouble gnt_progress_bar_get_fraction(GntProgressBar *pbar)
{
  GntProgressBarPrivate *priv = (GntProgressBarPrivate *)(g_type_instance_get_private(((GTypeInstance *)pbar),gnt_progress_bar_get_type()));
  return priv -> fraction;
}

GntProgressBarOrientation gnt_progress_bar_get_orientation(GntProgressBar *pbar)
{
  GntProgressBarPrivate *priv = (GntProgressBarPrivate *)(g_type_instance_get_private(((GTypeInstance *)pbar),gnt_progress_bar_get_type()));
  return priv -> orientation;
}

gboolean gnt_progress_bar_get_show_progress(GntProgressBar *pbar)
{
  GntProgressBarPrivate *priv = (GntProgressBarPrivate *)(g_type_instance_get_private(((GTypeInstance *)pbar),gnt_progress_bar_get_type()));
  return priv -> show_value;
}
