/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "gntclipboard.h"
enum __unnamed_enum___F0_L25_C1_SIG_CLIPBOARD__COMMA__SIGS {SIG_CLIPBOARD,SIGS};
static guint signals[1UL] = {(0)};

static void gnt_clipboard_class_init(GntClipboardClass *klass)
{
  signals[SIG_CLIPBOARD] = g_signal_new("clipboard_changed",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,0,0,0,g_cclosure_marshal_VOID__POINTER,((GType )(1 << 2)),1,((GType )(17 << 2)));
}
/******************************************************************************
 * GntClipboard API
 *****************************************************************************/

void gnt_clipboard_set_string(GntClipboard *clipboard,const gchar *string)
{
  g_free((clipboard -> string));
  clipboard -> string = g_strdup(string);
  g_signal_emit(clipboard,signals[SIG_CLIPBOARD],0,(clipboard -> string));
}

gchar *gnt_clipboard_get_string(GntClipboard *clipboard)
{
  return g_strdup((clipboard -> string));
}

static void gnt_clipboard_init(GTypeInstance *instance,gpointer class)
{
  GntClipboard *clipboard = (GntClipboard *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_clipboard_get_gtype()));
  clipboard -> string = g_strdup("");
}

GType gnt_clipboard_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(GntClipboardClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gnt_clipboard_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GntClipboard ))), (0), (gnt_clipboard_init), ((const GTypeValueTable *)((void *)0))
/* base_init		*/
/* base_finalize	*/
/* class_data		*/
/* n_preallocs		*/
/* instance_init	*/
/* value_table		*/
};
    type = g_type_register_static(((GType )(20 << 2)),"GntClipboard",&info,0);
  }
  return type;
}
