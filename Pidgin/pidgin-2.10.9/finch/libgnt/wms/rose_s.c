#include "internal.h"
#include <string.h>
#include <sys/types.h>
#include "gnt.h"
#include "gntbox.h"
#include "gntmenu.h"
#include "gntstyle.h"
#include "gntwm.h"
#include "gntwindow.h"
#include "gntlabel.h"
#include "blist.h"
#define TYPE_S				(s_get_gtype())
#ifdef _S
#undef _S
#endif
typedef struct _S {
GntWM inherit;}S;
typedef struct _SClass {
GntWMClass inherit;}SClass;
GType s_get_gtype();
void gntwm_init(GntWM **wm);
static void (*org_new_window)(GntWM *, GntWidget *);

static void envelope_buddylist(GntWidget *win)
{
  int w;
  int h;
  gnt_widget_get_size(win,&w,&h);
  wresize((win -> window),h,(w + 1));
  (wmove((win -> window),0,w) == -1)?-1 : wvline((win -> window),(acs_map[(unsigned char )'x'] | (((chtype )GNT_COLOR_NORMAL) << 0 + 8)),h);
  wtouchln((win -> window),0,(((win -> window) != 0)?(( *(win -> window))._maxy + 1) : -1),1);
}

static void envelope_normal_window(GntWidget *win)
{
  int w;
  int h;
  if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_widget_get_gtype())))).priv.flags & (GNT_WIDGET_NO_BORDER | GNT_WIDGET_TRANSIENT)) != 0U) 
    return ;
  gnt_widget_get_size(win,&w,&h);
  wbkgdset((win -> window),(32 | (((chtype )GNT_COLOR_NORMAL) << 0 + 8)));
  mvwprintw((win -> window),0,(w - 4),"[X]");
}

static void s_decorate_window(GntWM *wm,GntWidget *win)
{
  const char *name;
  name = gnt_widget_get_name(win);
  if ((name != 0) && (strcmp(name,"buddylist") == 0)) {
    envelope_buddylist(win);
  }
  else {
    envelope_normal_window(win);
  }
}

static void s_window_update(GntWM *wm,GntNode *node)
{
  s_decorate_window(wm,(node -> me));
}

static void s_new_window(GntWM *wm,GntWidget *win)
{
  int x;
  int y;
  int w;
  int h;
  int maxx;
  int maxy;
  const char *name;
  gboolean blist = 0;
  if (!((({
    GTypeInstance *__inst = (GTypeInstance *)win;
    GType __t = gnt_menu_get_gtype();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = 1;
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) 
{
    ((maxy = ((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) , (maxx = ((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)));
    gnt_widget_get_position(win,&x,&y);
    gnt_widget_get_size(win,&w,&h);
    name = gnt_widget_get_name(win);
    if ((name != 0) && (strcmp(name,"buddylist") == 0)) {
/* The buddylist doesn't have no border nor nothing! */
      x = 0;
      y = 0;
      h = (maxy - 1);
      blist = 1;
      gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),0);
      ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_CAN_TAKE_FOCUS;
      gnt_widget_set_position(win,x,y);
      mvwin((win -> window),y,x);
/* XXX: Why is the +2 needed here? -- sadrul */
      gnt_widget_set_size(win,-1,(h + 2));
    }
    else if (!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_TRANSIENT) != 0U)) {
      const char *title = ( *((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype())))).title;
      if ((title == ((const char *)((void *)0))) || !(g_hash_table_lookup((wm -> positions),title) != 0)) {
/* In the middle of the screen */
        x = ((maxx - w) / 2);
        y = ((maxy - h) / 2);
        gnt_widget_set_position(win,x,y);
        mvwin((win -> window),y,x);
      }
    }
  }
  ( *org_new_window)(wm,win);
  if (blist != 0) 
    gnt_wm_raise_window(wm,win);
}

static GntWidget *find_widget(GntWM *wm,const char *wname)
{
  GList *iter = ( *(wm -> cws)).list;
  for (; iter != 0; iter = (iter -> next)) {
    GntWidget *widget = (iter -> data);
    const char *name = gnt_widget_get_name(widget);
    if ((name != 0) && (strcmp(name,wname) == 0)) {
      return widget;
    }
  }
  return 0;
}

static gboolean s_mouse_clicked(GntWM *wm,GntMouseEvent event,int cx,int cy,GntWidget *widget)
{
  int x;
  int y;
  int w;
  int h;
  if (!(widget != 0)) 
    return 0;
/* This might be a place to bring up a context menu */
  if ((event != GNT_LEFT_MOUSE_DOWN) || ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_NO_BORDER) != 0U)) 
    return 0;
  gnt_widget_get_position(widget,&x,&y);
  gnt_widget_get_size(widget,&w,&h);
  if ((cy == y) && (cx == ((x + w) - 3))) {
    gnt_widget_destroy(widget);
    return 1;
  }
  return 0;
}

static gboolean toggle_buddylist(GntBindable *bindable,GList *null)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  GntWidget *blist = find_widget(wm,"buddylist");
  if (blist != 0) 
    gnt_widget_destroy(blist);
  else 
    purple_blist_show();
  return 1;
}

static void s_class_init(SClass *klass)
{
  GntWMClass *pclass = (GntWMClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_wm_get_gtype()));
  org_new_window = (pclass -> new_window);
  pclass -> new_window = s_new_window;
  pclass -> decorate_window = s_decorate_window;
  pclass -> window_update = s_window_update;
  pclass -> mouse_clicked = s_mouse_clicked;
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"toggle-buddylist",toggle_buddylist,"\033b",((void *)((void *)0)));
  gnt_style_read_actions(( *((GTypeClass *)klass)).g_type,((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))));;
}

void gntwm_init(GntWM **wm)
{
   *wm = (g_object_new(s_get_gtype(),0));
}

GType s_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(SClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )s_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(S ))), (0), ((GInstanceInitFunc )((void *)0)), ((const GTypeValueTable *)((void *)0))
/* base_init		*/
/* base_finalize	*/
/* class_data		*/
/* n_preallocs		*/
/* instance_init	*/
};
    type = g_type_register_static(gnt_wm_get_gtype(),"GntS",&info,0);
  }
  return type;
}
