/*
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
/**
 * 1. Buddylist is aligned on the left.
 * 2. The rest of the screen is split into MxN grid for conversation windows.
 * 	- M = split-h in ~/.gntrc:[irssi]
 * 	- N = split-v in ~/.gntrc:[irssi]
 *	- Press alt-shift-k/j/l/h to move the selected window to the frame
 *	  above/below/left/right of the current frame.
 * 3. All the other windows are always centered.
 */
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "gntinternal.h"
#include "gnt.h"
#include "gntbox.h"
#include "gntmenu.h"
#include "gntstyle.h"
#include "gntwm.h"
#include "gntwindow.h"
#include "gntlabel.h"
#define TYPE_IRSSI				(irssi_get_gtype())
typedef struct _Irssi {
GntWM inherit;
int vert;
int horiz;
/* This is changed whenever the buddylist is opened/closed or resized. */
int buddylistwidth;}Irssi;
typedef struct _IrssiClass {
GntWMClass inherit;}IrssiClass;
GType irssi_get_gtype();
void gntwm_init(GntWM **wm);
static void (*org_new_window)(GntWM *, GntWidget *);

static void get_xywh_for_frame(Irssi *irssi,int hor,int vert,int *x,int *y,int *w,int *h)
{
  int width;
  int height;
  int rx;
  int ry;
  width = (((((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)) - (irssi -> buddylistwidth)) / (irssi -> horiz));
  height = (((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) - 1) / (irssi -> vert));
  if (width != 0) {
    rx = (irssi -> buddylistwidth);
  }
  else {
    rx = 0;
    width = ((((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)) / (irssi -> horiz));
  }
  if (hor != 0) 
    rx += (hor * width);
  if (rx != 0) 
    rx++;
  ry = 0;
  if (vert != 0) 
    ry += ((vert * height) + 1);
  if (x != 0) 
     *x = rx;
  if (y != 0) 
     *y = ry;
  if (w != 0) {
     *w = ((hor == ((irssi -> horiz) - 1))?((((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)) - rx) : (width - 1));
  }
  if (h != 0) {
     *h = ((vert == ((irssi -> vert) - 1))?(((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) - 1) - ry) : (height - !(!(vert != 0))));
  }
}

static void draw_line_separators(Irssi *irssi)
{
  int x;
  int y;
  int width;
  int height;
  wclear(stdscr);
/* Draw the separator for the buddylist */
  if ((irssi -> buddylistwidth) != 0) 
    (wmove(stdscr,0,(irssi -> buddylistwidth)) == -1)?-1 : wvline(stdscr,(acs_map[(unsigned char )'x'] | (((chtype )GNT_COLOR_NORMAL) << 0 + 8)),((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) - 1));
/* Now the separators for the conversation windows */
  width = (((((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)) - (irssi -> buddylistwidth)) / (irssi -> horiz));
  height = (((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) - 1) / (irssi -> vert));
  for (x = 1; x < (irssi -> horiz); x++) {
    (wmove(stdscr,0,((irssi -> buddylistwidth) + (x * width))) == -1)?-1 : wvline(stdscr,(acs_map[(unsigned char )'x'] | (((chtype )GNT_COLOR_NORMAL) << 0 + 8)),((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) - 1));
  }
  for (y = 1; y < (irssi -> vert); y++) {
    (wmove(stdscr,(y * height),((irssi -> buddylistwidth) + 1)) == -1)?-1 : whline(stdscr,(acs_map[(unsigned char )'q'] | (((chtype )GNT_COLOR_NORMAL) << 0 + 8)),((((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)) - (irssi -> buddylistwidth)));
    for (x = 1; x < (irssi -> horiz); x++) {
      (wmove(stdscr,(y * height),((x * width) + (irssi -> buddylistwidth))) == -1)?-1 : waddch(stdscr,(acs_map[(unsigned char )'n'] | (((chtype )GNT_COLOR_NORMAL) << 0 + 8)));
    }
    if ((irssi -> buddylistwidth) != 0) 
      (wmove(stdscr,(y * height),(irssi -> buddylistwidth)) == -1)?-1 : waddch(stdscr,(acs_map[(unsigned char )'t'] | (((chtype )GNT_COLOR_NORMAL) << 0 + 8)));
  }
}

static gboolean is_budddylist(GntWidget *win)
{
  const char *name = gnt_widget_get_name(win);
  if ((name != 0) && (strcmp(name,"buddylist") == 0)) 
    return 1;
  return 0;
}

static void remove_border_set_position_size(GntWM *wm,GntWidget *win,int x,int y,int w,int h)
{
  gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),0);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_CAN_TAKE_FOCUS;
  gnt_widget_set_position(win,x,y);
  mvwin((win -> window),y,x);
  gnt_widget_set_size(win,((w < 0)?-1 : (w + 2)),(h + 2));
}

static void irssi_new_window(GntWM *wm,GntWidget *win)
{
  const char *name;
  int x;
  int y;
  int w;
  int h;
  name = gnt_widget_get_name(win);
  if (!(name != 0) || !(strstr(name,"conversation-window") != 0)) {
    if (!((({
      GTypeInstance *__inst = (GTypeInstance *)win;
      GType __t = gnt_menu_get_gtype();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = 1;
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) && !((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_TRANSIENT) != 0U)) 
{
      if (!(name != 0) || (strcmp(name,"buddylist") != 0)) {
        gnt_widget_get_size(win,&w,&h);
        x = (((((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)) - w) / 2);
        y = (((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) - h) / 2);
        gnt_widget_set_position(win,x,y);
        mvwin((win -> window),y,x);
      }
      else {
        gnt_window_set_maximize(((GntWindow *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_window_get_gtype()))),GNT_WINDOW_MAXIMIZE_Y);
        remove_border_set_position_size(wm,win,0,0,-1,((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) - 1));
        gnt_widget_get_size(win,&( *((Irssi *)wm)).buddylistwidth,0);
        draw_line_separators(((Irssi *)wm));
      }
    }
    ( *org_new_window)(wm,win);
    return ;
  }
/* The window we have here is a conversation window. */
/* XXX: There should be some way to remember which frame a conversation window
	 * was in the last time. Perhaps save them in some ~/.gntpositionirssi or some
	 * such. */
  get_xywh_for_frame(((Irssi *)wm),0,0,&x,&y,&w,&h);
  remove_border_set_position_size(wm,win,x,y,w,h);
  ( *org_new_window)(wm,win);
}

static void irssi_window_resized(GntWM *wm,GntNode *node)
{
  if (!(is_budddylist((node -> me)) != 0)) 
    return ;
  gnt_widget_get_size((node -> me),&( *((Irssi *)wm)).buddylistwidth,0);
  draw_line_separators(((Irssi *)wm));
}

static gboolean irssi_close_window(GntWM *wm,GntWidget *win)
{
  if (is_budddylist(win) != 0) 
    ( *((Irssi *)wm)).buddylistwidth = 0;
  return 0;
}

static gboolean update_conv_window_title(GntNode *node)
{
  char title[256UL];
  int x;
  int y;
  snprintf(title,(sizeof(title)),"%d: %s",(((gint )((glong )(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(node -> me)),((GType )(20 << 2))))),"irssi-index")))) + 1),( *((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(node -> me)),gnt_box_get_gtype())))).title);
  ((y = (((node -> window) != 0)?( *(node -> window))._cury : -1)) , (x = (((node -> window) != 0)?( *(node -> window))._curx : -1)));
  wbkgdset((node -> window),(0 | (((chtype )(((gnt_widget_has_focus((node -> me)) != 0)?GNT_COLOR_TITLE : GNT_COLOR_TITLE_D))) << 0 + 8)));
  (wmove((node -> window),0,0) == -1)?-1 : waddnstr((node -> window),title,-1);
  wmove((node -> window),y,x);
  if (!(gnt_is_refugee() != 0)) {
    update_panels();
    doupdate();
  }
  return 0;
}

static void irssi_update_window(GntWM *wm,GntNode *node)
{
  GntWidget *win = (node -> me);
  const char *name = gnt_widget_get_name(win);
  if ((!(name != 0) || !((({
    GTypeInstance *__inst = (GTypeInstance *)win;
    GType __t = gnt_box_get_gtype();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = 1;
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) || !(strstr(name,"conversation-window") != 0)) 
    return ;
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"irssi-index",((gpointer )((glong )(g_list_index(( *(wm -> cws)).list,win)))));
  g_timeout_add(0,((GSourceFunc )update_conv_window_title),node);
}

static void find_window_position(Irssi *irssi,GntWidget *win,int *h,int *v)
{
  int x;
  int y;
  int width;
  int height;
  gnt_widget_get_position(win,&x,&y);
  width = (((((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)) - (irssi -> buddylistwidth)) / (irssi -> horiz));
  height = (((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) - 1) / (irssi -> vert));
  if (h != 0) 
     *h = ((width != 0)?((x - (irssi -> buddylistwidth)) / width) : (x / ((((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)) / (irssi -> horiz))));
  if (v != 0) 
     *v = (y / height);
}

static gboolean move_direction(GntBindable *bindable,GList *list)
{
  GntWM *wm = (GntWM *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_wm_get_gtype()));
  Irssi *irssi = (Irssi *)wm;
  int vert;
  int hor;
  int x;
  int y;
  int w;
  int h;
  GntWidget *win;
  if ((( *(wm -> cws)).ordered == ((GList *)((void *)0))) || (is_budddylist((win = ((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)( *( *(wm -> cws)).ordered).data),gnt_widget_get_gtype()))))) != 0)) 
    return 0;
  find_window_position(irssi,win,&hor,&vert);
  switch((gint )((glong )(list -> data))){
    case 'k':
{
      vert = ((0 > (vert - 1))?0 : (vert - 1));
      break; 
    }
    case 'j':
{
      vert = (((vert + 1) < ((irssi -> vert) - 1))?(vert + 1) : ((irssi -> vert) - 1));
      break; 
    }
    case 'l':
{
      hor = (((hor + 1) < ((irssi -> horiz) - 1))?(hor + 1) : ((irssi -> horiz) - 1));
      break; 
    }
    case 'h':
{
      hor = ((0 > (hor - 1))?0 : (hor - 1));
      break; 
    }
  }
  get_xywh_for_frame(irssi,hor,vert,&x,&y,&w,&h);
  gnt_wm_move_window(wm,win,x,y);
  gnt_wm_resize_window(wm,win,w,h);
  return 1;
}

static void refresh_window(GntWidget *widget,GntNode *node,Irssi *irssi)
{
  int vert;
  int hor;
  int x;
  int y;
  int w;
  int h;
  const char *name;
  if (!((({
    GTypeInstance *__inst = (GTypeInstance *)widget;
    GType __t = gnt_window_get_gtype();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = 1;
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) 
    return ;
  if (is_budddylist(widget) != 0) {
    return ;
  }
  name = gnt_widget_get_name(widget);
  if ((name != 0) && (strstr(name,"conversation-window") != 0)) {
    int cx;
    int cy;
    int cw;
    int ch;
    gnt_widget_get_position(widget,&cx,&cy);
    gnt_widget_get_size(widget,&cw,&ch);
    find_window_position(irssi,widget,&hor,&vert);
    get_xywh_for_frame(irssi,hor,vert,&x,&y,&w,&h);
    if ((x != cx) || (y != cy)) 
      gnt_wm_move_window(((GntWM *)(g_type_check_instance_cast(((GTypeInstance *)irssi),gnt_wm_get_gtype()))),widget,x,y);
    if ((w != cw) || (h != ch)) 
      gnt_wm_resize_window(((GntWM *)(g_type_check_instance_cast(((GTypeInstance *)irssi),gnt_wm_get_gtype()))),widget,w,h);
  }
}

static void irssi_terminal_refresh(GntWM *wm)
{
  draw_line_separators(((Irssi *)wm));
  g_hash_table_foreach((wm -> nodes),((GHFunc )refresh_window),wm);
}

static void irssi_class_init(IrssiClass *klass)
{
  GntWMClass *pclass = (GntWMClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_wm_get_gtype()));
  org_new_window = (pclass -> new_window);
  pclass -> new_window = irssi_new_window;
  pclass -> window_resized = irssi_window_resized;
  pclass -> close_window = irssi_close_window;
  pclass -> window_update = irssi_update_window;
  pclass -> terminal_refresh = irssi_terminal_refresh;
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"move-up",move_direction,"\033K",((gpointer )((gpointer )((glong )'k'))),((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"move-down",move_direction,"\033J",((gpointer )((gpointer )((glong )'j'))),((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"move-right",move_direction,"\033L",((gpointer )((gpointer )((glong )'l'))),((void *)((void *)0)));
  gnt_bindable_class_register_action(((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))),"move-left",move_direction,"\033H",((gpointer )((gpointer )((glong )'h'))),((void *)((void *)0)));
  gnt_style_read_actions(( *((GTypeClass *)klass)).g_type,((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))));;
}

void gntwm_init(GntWM **wm)
{
  char *style = (char *)((void *)0);
  Irssi *irssi;
  irssi = (g_object_new(irssi_get_gtype(),0));
   *wm = ((GntWM *)(g_type_check_instance_cast(((GTypeInstance *)irssi),gnt_wm_get_gtype())));
  style = gnt_style_get_from_name("irssi","split-v");
  irssi -> vert = ((style != 0)?atoi(style) : 1);
  g_free(style);
  style = gnt_style_get_from_name("irssi","split-h");
  irssi -> horiz = ((style != 0)?atoi(style) : 1);
  g_free(style);
  irssi -> vert = (((irssi -> vert) > 1)?(irssi -> vert) : 1);
  irssi -> horiz = (((irssi -> horiz) > 1)?(irssi -> horiz) : 1);
  irssi -> buddylistwidth = 0;
}

GType irssi_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(IrssiClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )irssi_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(Irssi ))), (0), ((GInstanceInitFunc )((void *)0)), ((const GTypeValueTable *)((void *)0))
/* base_init		*/
/* base_finalize	*/
/* class_data		*/
/* n_preallocs		*/
/* instance_init	*/
};
    type = g_type_register_static(gnt_wm_get_gtype(),"GntIrssi",&info,0);
  }
  return type;
}
