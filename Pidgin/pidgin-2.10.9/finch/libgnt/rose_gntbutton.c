/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <stdlib.h>
#include <string.h>
#include "gntinternal.h"
#include "gntbutton.h"
#include "gntstyle.h"
#include "gntutils.h"
enum __unnamed_enum___F0_L31_C1_SIGS {SIGS=1};
static GntWidgetClass *parent_class = (GntWidgetClass *)((void *)0);
static gboolean small_button = 0;

static void gnt_button_draw(GntWidget *widget)
{
  GntButton *button = (GntButton *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_button_get_gtype()));
  GntColorType type;
  gboolean focus;
  if ((focus = gnt_widget_has_focus(widget)) != 0) 
    type = GNT_COLOR_HIGHLIGHT;
  else 
    type = GNT_COLOR_NORMAL;
  wbkgdset((widget -> window),(0 | gnt_color_pair(type)));
  (wmove((widget -> window),((small_button != 0)?0 : 1),2) == -1)?-1 : waddnstr((widget -> window),C_(( *(button -> priv)).text),-1);
  if (small_button != 0) {
    type = GNT_COLOR_HIGHLIGHT;
    (wmove((widget -> window),0,0) == -1)?-1 : wchgat((widget -> window),widget -> priv.width,((focus != 0)?(((chtype )1UL) << 13 + 8) : (((chtype )1UL) << 10 + 8)),type,0);
  };
}

static void gnt_button_size_request(GntWidget *widget)
{
  GntButton *button = (GntButton *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_button_get_gtype()));
  gnt_util_get_text_bound(( *(button -> priv)).text,&widget -> priv.width,&widget -> priv.height);
  widget -> priv.width += 4;
  if (!((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_NO_BORDER) != 0U)) 
    widget -> priv.height += 2;
}

static void gnt_button_map(GntWidget *widget)
{
  if ((widget -> priv.width == 0) || (widget -> priv.height == 0)) 
    gnt_widget_size_request(widget);;
}

static gboolean gnt_button_clicked(GntWidget *widget,GntMouseEvent event,int x,int y)
{
  if (event == GNT_LEFT_MOUSE_DOWN) {
    gnt_widget_activate(widget);
    return 1;
  }
  return 0;
}

static void gnt_button_destroy(GntWidget *widget)
{
  GntButton *button = (GntButton *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_button_get_gtype()));
  g_free(( *(button -> priv)).text);
  g_free((button -> priv));
}

static gboolean button_activate(GntBindable *bind,GList *null)
{
  gnt_widget_activate(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)bind),gnt_widget_get_gtype()))));
  return 1;
}

static void gnt_button_class_init(GntWidgetClass *klass)
{
  char *style;
  GntBindableClass *bindable = (GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()));
  parent_class = ((GntWidgetClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_widget_get_gtype())));
  parent_class -> draw = gnt_button_draw;
  parent_class -> map = gnt_button_map;
  parent_class -> size_request = gnt_button_size_request;
  parent_class -> clicked = gnt_button_clicked;
  parent_class -> destroy = gnt_button_destroy;
  style = gnt_style_get_from_name(0,"small-button");
  small_button = gnt_style_parse_bool(style);
  g_free(style);
  gnt_bindable_class_register_action(bindable,"activate",button_activate,cur_term -> type.Strings[2],((void *)((void *)0)));
  gnt_style_read_actions(( *((GTypeClass *)klass)).g_type,((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))));
}

static void gnt_button_init(GTypeInstance *instance,gpointer class)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_widget_get_gtype()));
  GntButton *button = (GntButton *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_button_get_gtype()));
  button -> priv = ((GntButtonPriv *)(g_malloc0_n(1,(sizeof(GntButtonPriv )))));
  widget -> priv.minw = 4;
  widget -> priv.minh = ((small_button != 0)?1 : 3);
  if (small_button != 0) 
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= (GNT_WIDGET_NO_BORDER | GNT_WIDGET_NO_SHADOW);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags &= (~(GNT_WIDGET_GROW_X | GNT_WIDGET_GROW_Y));;
}
/******************************************************************************
 * GntButton API
 *****************************************************************************/

GType gnt_button_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(GntButtonClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gnt_button_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GntButton ))), (0), (gnt_button_init), ((const GTypeValueTable *)((void *)0))
/* base_init		*/
/* base_finalize	*/
/* class_finalize	*/
/* class_data		*/
/* n_preallocs		*/
/* instance_init	*/
/* value_table		*/
};
    type = g_type_register_static(gnt_widget_get_gtype(),"GntButton",&info,0);
  }
  return type;
}

GntWidget *gnt_button_new(const char *text)
{
  GntWidget *widget = (g_object_new(gnt_button_get_gtype(),0));
  GntButton *button = (GntButton *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_button_get_gtype()));
  ( *(button -> priv)).text = gnt_util_onscreen_fit_string(text,-1);
  gnt_widget_set_take_focus(widget,1);
  return widget;
}
