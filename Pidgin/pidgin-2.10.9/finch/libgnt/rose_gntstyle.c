/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "gntinternal.h"
#undef GNT_LOG_DOMAIN
#define GNT_LOG_DOMAIN "Style"
#include "gntstyle.h"
#include "gntcolors.h"
#include "gntws.h"
#include <glib.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#define MAX_WORKSPACES 99
#if GLIB_CHECK_VERSION(2,6,0)
static GKeyFile *gkfile;
#endif
static char *str_styles[5UL];
static int int_styles[5UL];
static int bool_styles[5UL];

const char *gnt_style_get(GntStyle style)
{
  return str_styles[style];
}

char *gnt_style_get_from_name(const char *group,const char *key)
{
#if GLIB_CHECK_VERSION(2,6,0)
  const char *prg = g_get_prgname();
  if ((((group == ((const char *)((void *)0))) || (( *group) == 0)) && (prg != 0)) && (g_key_file_has_group(gkfile,prg) != 0)) 
    group = prg;
  if (!(group != 0)) 
    group = "general";
  return g_key_file_get_value(gkfile,group,key,0);
#else
#endif
}

int gnt_style_get_color(char *group,char *key)
{
#if GLIB_CHECK_VERSION(2,6,0)
  int fg = 0;
  int bg = 0;
  gsize n;
  char **vals;
  int ret = 0;
  vals = gnt_style_get_string_list(group,key,&n);
  if ((vals != 0) && (n == 2)) {
    fg = gnt_colors_get_color(vals[0]);
    bg = gnt_colors_get_color(vals[1]);
    ret = gnt_color_add_pair(fg,bg);
  }
  g_strfreev(vals);
  return ret;
#else
#endif
}

char **gnt_style_get_string_list(const char *group,const char *key,gsize *length)
{
#if GLIB_CHECK_VERSION(2,6,0)
  const char *prg = g_get_prgname();
  if ((((group == ((const char *)((void *)0))) || (( *group) == 0)) && (prg != 0)) && (g_key_file_has_group(gkfile,prg) != 0)) 
    group = prg;
  if (!(group != 0)) 
    group = "general";
  return g_key_file_get_string_list(gkfile,group,key,length,0);
#else
#endif
}

gboolean gnt_style_get_bool(GntStyle style,gboolean def)
{
  const char *str;
  if (bool_styles[style] != -1) 
    return bool_styles[style];
  str = gnt_style_get(style);
  bool_styles[style] = ((str != 0)?gnt_style_parse_bool(str) : def);
  return bool_styles[style];
}

gboolean gnt_style_parse_bool(const char *str)
{
  gboolean def = 0;
  int i;
  if (str != 0) {
    if (g_ascii_strcasecmp(str,"false") == 0) 
      def = 0;
    else if (g_ascii_strcasecmp(str,"true") == 0) 
      def = 1;
    else if (sscanf(str,"%d",&i) == 1) {
      if (i != 0) 
        def = 1;
      else 
        def = 0;
    }
  }
  return def;
}
#if GLIB_CHECK_VERSION(2,6,0)

static void refine(char *text)
{
  char *s = text;
  char *t = text;
  while(( *s) != 0){
    if ((( *s) == '^') && (s[1] == '[')) {
/* escape */
       *t = 27;
      s++;
    }
    else if (( *s) == '\\') {
      if (s[1] == 0) 
         *t = 32;
      else {
        s++;
        if ((( *s) == 'r') || (( *s) == 'n')) 
           *t = 13;
        else if (( *s) == 't') 
           *t = 9;
        else 
           *t =  *s;
      }
    }
    else 
       *t =  *s;
    t++;
    s++;
  }
   *t = 0;
}

static char *parse_key(const char *key)
{
  return (char *)(gnt_key_translate(key));
}
#endif

void gnt_style_read_workspaces(GntWM *wm)
{
#if GLIB_CHECK_VERSION(2,6,0)
  int i;
  gchar *name;
  gsize c;
  for (i = 1; i < 99; ++i) {
    int j;
    GntWS *ws;
    gchar **titles;
    char group[32UL];
    g_snprintf(group,(sizeof(group)),"Workspace-%d",i);
    name = g_key_file_get_value(gkfile,group,"name",0);
    if (!(name != 0)) 
      return ;
    ws = gnt_ws_new(name);
    gnt_wm_add_workspace(wm,ws);
    g_free(name);
    titles = g_key_file_get_string_list(gkfile,group,"window-names",&c,0);
    if (titles != 0) {
      for (j = 0; j < c; ++j) 
        g_hash_table_replace((wm -> name_places),(g_strdup(titles[j])),ws);
      g_strfreev(titles);
    }
    titles = g_key_file_get_string_list(gkfile,group,"window-titles",&c,0);
    if (titles != 0) {
      for (j = 0; j < c; ++j) 
        g_hash_table_replace((wm -> title_places),(g_strdup(titles[j])),ws);
      g_strfreev(titles);
    }
  }
#endif
}

void gnt_style_read_actions(GType type,GntBindableClass *klass)
{
#if GLIB_CHECK_VERSION(2,6,0)
  char *name;
  GError *error = (GError *)((void *)0);
  name = g_strdup_printf("%s::binding",g_type_name(type));
  if (g_key_file_has_group(gkfile,name) != 0) {
    gsize len = 0;
    char **keys;
    keys = g_key_file_get_keys(gkfile,name,&len,&error);
    if (error != 0) {
      g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: %s","Style","void gnt_style_read_actions(unsigned long, struct _GntBindableClass *)",(error -> message));
      g_error_free(error);
      g_free(name);
      return ;
    }
    while(len-- != 0UL){
      char *key;
      char *action;
      key = g_strdup(keys[len]);
      action = g_key_file_get_string(gkfile,name,keys[len],&error);
      if (error != 0) {
        g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: %s","Style","void gnt_style_read_actions(unsigned long, struct _GntBindableClass *)",(error -> message));
        g_error_free(error);
        error = ((GError *)((void *)0));
      }
      else {
        const char *keycode = (parse_key(key));
        if (keycode == ((const char *)((void *)0))) {
          g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: Invalid key-binding %s","Style","void gnt_style_read_actions(unsigned long, struct _GntBindableClass *)",key);
        }
        else {
          gnt_bindable_register_binding(klass,action,keycode,((void *)((void *)0)));
        }
      }
      g_free(key);
      g_free(action);
    }
    g_strfreev(keys);
  }
  g_free(name);
#endif
}

gboolean gnt_style_read_menu_accels(const char *name,GHashTable *table)
{
#if GLIB_CHECK_VERSION(2,6,0)
  char *kname;
  GError *error = (GError *)((void *)0);
  gboolean ret = 0;
  kname = g_strdup_printf("%s::menu",name);
  if (g_key_file_has_group(gkfile,kname) != 0) {
    gsize len = 0;
    char **keys;
    keys = g_key_file_get_keys(gkfile,kname,&len,&error);
    if (error != 0) {
      g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: %s","Style","int gnt_style_read_menu_accels(const char *, struct _GHashTable *)",(error -> message));
      g_error_free(error);
      g_free(kname);
      return ret;
    }
    while(len-- != 0UL){
      char *key;
      char *menuid;
      key = g_strdup(keys[len]);
      menuid = g_key_file_get_string(gkfile,kname,keys[len],&error);
      if (error != 0) {
        g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: %s","Style","int gnt_style_read_menu_accels(const char *, struct _GHashTable *)",(error -> message));
        g_error_free(error);
        error = ((GError *)((void *)0));
      }
      else {
        const char *keycode = (parse_key(key));
        if (keycode == ((const char *)((void *)0))) {
          g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: Invalid key-binding %s","Style","int gnt_style_read_menu_accels(const char *, struct _GHashTable *)",key);
        }
        else {
          ret = 1;
          g_hash_table_replace(table,(g_strdup(keycode)),menuid);
          menuid = ((char *)((void *)0));
        }
      }
      g_free(key);
      g_free(menuid);
    }
    g_strfreev(keys);
  }
  g_free(kname);
  return ret;
#endif
  return 0;
}

void gnt_styles_get_keyremaps(GType type,GHashTable *hash)
{
#if GLIB_CHECK_VERSION(2,6,0)
  char *name;
  GError *error = (GError *)((void *)0);
  name = g_strdup_printf("%s::remap",g_type_name(type));
  if (g_key_file_has_group(gkfile,name) != 0) {
    gsize len = 0;
    char **keys;
    keys = g_key_file_get_keys(gkfile,name,&len,&error);
    if (error != 0) {
      g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: %s","Style","void gnt_styles_get_keyremaps(unsigned long, struct _GHashTable *)",(error -> message));
      g_error_free(error);
      g_free(name);
      return ;
    }
    while(len-- != 0UL){
      char *key;
      char *replace;
      key = g_strdup(keys[len]);
      replace = g_key_file_get_string(gkfile,name,keys[len],&error);
      if (error != 0) {
        g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: %s","Style","void gnt_styles_get_keyremaps(unsigned long, struct _GHashTable *)",(error -> message));
        g_error_free(error);
        error = ((GError *)((void *)0));
        g_free(key);
      }
      else {
        refine(key);
        refine(replace);
        g_hash_table_insert(hash,key,replace);
      }
    }
    g_strfreev(keys);
  }
  g_free(name);
#endif
}
#if GLIB_CHECK_VERSION(2,6,0)

static void read_general_style(GKeyFile *kfile)
{
  GError *error = (GError *)((void *)0);
  gsize nkeys;
  const char *prgname = g_get_prgname();
  char **keys = (char **)((void *)0);
  int i;
  struct __unnamed_class___F0_L388_C2_L286R__L287R__scope____SgSS2___variable_declaration__variable_type___Pb__Cc__Pe___variable_name_L286R__L287R__scope____SgSS2____scope__style__DELIMITER__L286R__L287R__scope____SgSS2___variable_declaration__variable_type_GntStyleL288R__typedef_declaration_variable_name_L286R__L287R__scope____SgSS2____scope__en {
  const char *style;
  GntStyle en;}styles[] = {{("shadow"), (GNT_STYLE_SHADOW)}, {("customcolor"), (GNT_STYLE_COLOR)}, {("mouse"), (GNT_STYLE_MOUSE)}, {("wm"), (GNT_STYLE_WM)}, {("remember_position"), (GNT_STYLE_REMPOS)}, {((const char *)((void *)0)), (0)}};
  if ((prgname != 0) && (( *prgname) != 0)) 
    keys = g_key_file_get_keys(kfile,prgname,&nkeys,0);
  if (keys == ((char **)((void *)0))) {
    prgname = "general";
    keys = g_key_file_get_keys(kfile,prgname,&nkeys,&error);
  }
  if (error != 0) {
    g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: %s","Style","void read_general_style(struct _GKeyFile *)",(error -> message));
    g_error_free(error);
  }
  else {
    for (i = 0; styles[i].style != 0; i++) {
      str_styles[styles[i].en] = g_key_file_get_string(kfile,prgname,styles[i].style,0);
    }
  }
  g_strfreev(keys);
}
#endif

void gnt_style_read_configure_file(const char *filename)
{
#if GLIB_CHECK_VERSION(2,6,0)
  GError *error = (GError *)((void *)0);
  gkfile = g_key_file_new();
  if (!(g_key_file_load_from_file(gkfile,filename,(G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS),&error) != 0)) {
    g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: %s","Style","void gnt_style_read_configure_file(const char *)",(error -> message));
    g_error_free(error);
    return ;
  }
  gnt_colors_parse(gkfile);
  read_general_style(gkfile);
#endif
}

void gnt_init_styles()
{
  int i;
  for (i = 0; i < GNT_STYLES; i++) {
    str_styles[i] = ((char *)((void *)0));
    int_styles[i] = -1;
    bool_styles[i] = -1;
  }
}

void gnt_uninit_styles()
{
  int i;
  for (i = 0; i < GNT_STYLES; i++) {
    g_free(str_styles[i]);
    str_styles[i] = ((char *)((void *)0));
  }
#if GLIB_CHECK_VERSION(2,6,0)
  g_key_file_free(gkfile);
  gkfile = ((GKeyFile *)((void *)0));
#endif
}
