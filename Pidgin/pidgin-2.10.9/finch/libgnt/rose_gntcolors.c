/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "config.h"
#include <ncurses.h>
#include "gntinternal.h"
#undef GNT_LOG_DOMAIN
#define GNT_LOG_DOMAIN "Colors"
#include "gntcolors.h"
#include "gntstyle.h"
#include <glib.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
static gboolean hascolors;
static int custom_type = GNT_COLORS;
static struct __unnamed_class___F0_L42_C8_unknown_scope_and_name_variable_declaration__variable_type_s_variable_name_unknown_scope_and_name__scope__r__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_s_variable_name_unknown_scope_and_name__scope__g__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_s_variable_name_unknown_scope_and_name__scope__b {
short r;
short g;
short b;}colors[7UL];

static void backup_colors()
{
  short i;
  for (i = 0; i < GNT_TOTAL_COLORS; i++) {
    color_content(i,&colors[i].r,&colors[i].g,&colors[i].b);
  }
}

static gboolean can_use_custom_color()
{
  return (gnt_style_get_bool(GNT_STYLE_COLOR,0) != 0) && ((can_change_color()) != 0);
}

static void restore_colors()
{
  short i;
  for (i = 0; i < GNT_TOTAL_COLORS; i++) {
    init_color(i,colors[i].r,colors[i].g,colors[i].b);
  }
}

void gnt_init_colors()
{
  static gboolean init = 0;
  int defaults;
  if (init != 0) 
    return ;
  init = 1;
  start_color();
  if (!((hascolors = (has_colors())) != 0)) 
    return ;
  defaults = use_default_colors();
  if (can_use_custom_color() != 0) {
    backup_colors();
/* Do some init_color()s */
    init_color(GNT_COLOR_BLACK,0,0,0);
    init_color(GNT_COLOR_RED,1000,0,0);
    init_color(GNT_COLOR_GREEN,0,1000,0);
    init_color(GNT_COLOR_BLUE,250,250,700);
    init_color(GNT_COLOR_WHITE,1000,1000,1000);
    init_color(GNT_COLOR_GRAY,699,699,699);
    init_color(GNT_COLOR_DARK_GRAY,256,256,256);
/* Now some init_pair()s */
    init_pair(GNT_COLOR_NORMAL,GNT_COLOR_BLACK,GNT_COLOR_WHITE);
    init_pair(GNT_COLOR_HIGHLIGHT,GNT_COLOR_WHITE,GNT_COLOR_BLUE);
    init_pair(GNT_COLOR_SHADOW,GNT_COLOR_BLACK,GNT_COLOR_DARK_GRAY);
    init_pair(GNT_COLOR_TITLE,GNT_COLOR_WHITE,GNT_COLOR_BLUE);
    init_pair(GNT_COLOR_TITLE_D,GNT_COLOR_WHITE,GNT_COLOR_GRAY);
    init_pair(GNT_COLOR_TEXT_NORMAL,GNT_COLOR_WHITE,GNT_COLOR_BLUE);
    init_pair(GNT_COLOR_HIGHLIGHT_D,GNT_COLOR_BLACK,GNT_COLOR_GRAY);
    init_pair(GNT_COLOR_DISABLED,GNT_COLOR_GRAY,GNT_COLOR_WHITE);
    init_pair(GNT_COLOR_URGENT,GNT_COLOR_WHITE,GNT_COLOR_RED);
  }
  else {
    int bg;
    if (defaults == 0) {
      init_pair(GNT_COLOR_NORMAL,(-1),(-1));
      bg = -1;
    }
    else {
      init_pair(GNT_COLOR_NORMAL,0,7);
      bg = 7;
    }
    init_pair(GNT_COLOR_DISABLED,3,bg);
    init_pair(GNT_COLOR_URGENT,2,bg);
    init_pair(GNT_COLOR_HIGHLIGHT,7,4);
    init_pair(GNT_COLOR_SHADOW,0,0);
    init_pair(GNT_COLOR_TITLE,7,4);
    init_pair(GNT_COLOR_TITLE_D,7,0);
    init_pair(GNT_COLOR_TEXT_NORMAL,7,4);
    init_pair(GNT_COLOR_HIGHLIGHT_D,6,0);
  }
}

void gnt_uninit_colors()
{
  if (can_use_custom_color() != 0) 
    restore_colors();
}
#if GLIB_CHECK_VERSION(2,6,0)

int gnt_colors_get_color(char *key)
{
  int color;
  gboolean custom = can_use_custom_color();
  key = g_strchomp(g_strchug(key));
  if (strcmp(key,"black") == 0) 
    color = ((custom != 0)?GNT_COLOR_BLACK : 0);
  else if (strcmp(key,"red") == 0) 
    color = ((custom != 0)?GNT_COLOR_RED : 1);
  else if (strcmp(key,"green") == 0) 
    color = ((custom != 0)?GNT_COLOR_GREEN : 2);
  else if (strcmp(key,"blue") == 0) 
    color = ((custom != 0)?GNT_COLOR_BLUE : 4);
  else if (strcmp(key,"white") == 0) 
    color = ((custom != 0)?GNT_COLOR_WHITE : 7);
  else if ((strcmp(key,"gray") == 0) || (strcmp(key,"grey") == 0)) 
/* eh? */
    color = ((custom != 0)?GNT_COLOR_GRAY : 3);
  else if ((strcmp(key,"darkgray") == 0) || (strcmp(key,"darkgrey") == 0)) 
    color = ((custom != 0)?GNT_COLOR_DARK_GRAY : 0);
  else if (strcmp(key,"magenta") == 0) 
    color = 5;
  else if (strcmp(key,"cyan") == 0) 
    color = 6;
  else if (strcmp(key,"default") == 0) 
    color = -1;
  else {
    g_log("Gnt",G_LOG_LEVEL_WARNING,"Invalid color name: %s\n",key);
    color = -22;
  }
  return color;
}

void gnt_colors_parse(GKeyFile *kfile)
{
  GError *error = (GError *)((void *)0);
  gsize nkeys;
  char **keys = g_key_file_get_keys(kfile,"colors",&nkeys,&error);
  if (error != 0) {
    g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: %s","Colors","void gnt_colors_parse(struct _GKeyFile *)",(error -> message));
    g_error_free(error);
    error = ((GError *)((void *)0));
  }
  else if (nkeys != 0UL) {
    gnt_init_colors();
    while(nkeys-- != 0UL){{
        gsize len;
        gchar *key = keys[nkeys];
        char **list = g_key_file_get_string_list(kfile,"colors",key,&len,0);
        if (len == 3) {
          int r = atoi(list[0]);
          int g = atoi(list[1]);
          int b = atoi(list[2]);
          int color = -1;
          key = g_ascii_strdown(key,(-1));
          color = gnt_colors_get_color(key);
          g_free(key);
          if (color == -22) {
            g_strfreev(list);
            continue; 
          }
          init_color(color,r,g,b);
        }
        g_strfreev(list);
      }
    }
    g_strfreev(keys);
  }
  gnt_color_pairs_parse(kfile);
}

void gnt_color_pairs_parse(GKeyFile *kfile)
{
  GError *error = (GError *)((void *)0);
  gsize nkeys;
  char **keys = g_key_file_get_keys(kfile,"colorpairs",&nkeys,&error);
  if (error != 0) {
    g_log("Gnt",G_LOG_LEVEL_WARNING,"(%s) %s: %s","Colors","void gnt_color_pairs_parse(struct _GKeyFile *)",(error -> message));
    g_error_free(error);
    return ;
  }
  else if (nkeys != 0UL) 
    gnt_init_colors();
  while(nkeys-- != 0UL){{
      gsize len;
      gchar *key = keys[nkeys];
      char **list = g_key_file_get_string_list(kfile,"colorpairs",key,&len,0);
      if (len == 2) {
        GntColorType type = 0;
        gchar *fgc = g_ascii_strdown(list[0],(-1));
        gchar *bgc = g_ascii_strdown(list[1],(-1));
        int fg = gnt_colors_get_color(fgc);
        int bg = gnt_colors_get_color(bgc);
        g_free(fgc);
        g_free(bgc);
        if ((fg == -22) || (bg == -22)) {
          g_strfreev(list);
          continue; 
        }
        key = g_ascii_strdown(key,(-1));
        if (strcmp(key,"normal") == 0) 
          type = GNT_COLOR_NORMAL;
        else if (strcmp(key,"highlight") == 0) 
          type = GNT_COLOR_HIGHLIGHT;
        else if (strcmp(key,"highlightd") == 0) 
          type = GNT_COLOR_HIGHLIGHT_D;
        else if (strcmp(key,"shadow") == 0) 
          type = GNT_COLOR_SHADOW;
        else if (strcmp(key,"title") == 0) 
          type = GNT_COLOR_TITLE;
        else if (strcmp(key,"titled") == 0) 
          type = GNT_COLOR_TITLE_D;
        else if (strcmp(key,"text") == 0) 
          type = GNT_COLOR_TEXT_NORMAL;
        else if (strcmp(key,"disabled") == 0) 
          type = GNT_COLOR_DISABLED;
        else if (strcmp(key,"urgent") == 0) 
          type = GNT_COLOR_URGENT;
        else {
          g_strfreev(list);
          g_free(key);
          continue; 
        }
        g_free(key);
        init_pair(type,fg,bg);
      }
      g_strfreev(list);
    }
  }
  g_strfreev(keys);
}
#endif  /* GKeyFile */

int gnt_color_pair(int pair)
{
  return ((hascolors != 0)?(((chtype )pair) << 0 + 8) : ((((((pair == GNT_COLOR_NORMAL) || (pair == GNT_COLOR_HIGHLIGHT_D)) || (pair == GNT_COLOR_TITLE_D)) || (pair == GNT_COLOR_DISABLED))?0 : (((chtype )1UL) << 8 + 8))));
}

int gnt_color_add_pair(int fg,int bg)
{
  init_pair(custom_type,fg,bg);
  return custom_type++;
}
