/**
 * GNT - The GLib Ncurses Toolkit
 *
 * GNT is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "gntinternal.h"
#include "gntcolors.h"
#include "gntkeys.h"
#include "gntslider.h"
#include "gntstyle.h"
enum __unnamed_enum___F0_L29_C1_SIG_VALUE_CHANGED__COMMA__SIGS {SIG_VALUE_CHANGED,SIGS};
static guint signals[1UL] = {(0)};
static GntWidgetClass *parent_class = (GntWidgetClass *)((void *)0);
/* returns TRUE if the value was changed */

static gboolean sanitize_value(GntSlider *slider)
{
  if ((slider -> current) < (slider -> min)) 
    slider -> current = (slider -> min);
  else if ((slider -> current) > (slider -> max)) 
    slider -> current = (slider -> max);
  else 
    return 0;
  return 1;
}

static void redraw_slider(GntSlider *slider)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)slider),gnt_widget_get_gtype()));
  if ((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_MAPPED) != 0U) 
    gnt_widget_draw(widget);
}

static void slider_value_changed(GntSlider *slider)
{
  g_signal_emit(slider,signals[SIG_VALUE_CHANGED],0,(slider -> current));
}

static void gnt_slider_draw(GntWidget *widget)
{
  GntSlider *slider = (GntSlider *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_slider_get_gtype()));
  int attr = 0;
  int position;
  int size = 0;
  if ((slider -> vertical) != 0) 
    size = widget -> priv.height;
  else 
    size = widget -> priv.width;
  if (gnt_widget_has_focus(widget) != 0) 
    attr |= GNT_COLOR_HIGHLIGHT;
  else 
    attr |= GNT_COLOR_HIGHLIGHT_D;
  if ((slider -> max) != (slider -> min)) 
    position = (((size - 1) * ((slider -> current) - (slider -> min))) / ((slider -> max) - (slider -> min)));
  else 
    position = 0;
  if ((slider -> vertical) != 0) {
    (wmove((widget -> window),(size - position),0) == -1)?-1 : wvline((widget -> window),((acs_map[(unsigned char )'x'] | (gnt_color_pair(GNT_COLOR_NORMAL))) | (((chtype )1UL) << 13 + 8)),position);
    (wmove((widget -> window),0,0) == -1)?-1 : wvline((widget -> window),(acs_map[(unsigned char )'x'] | (gnt_color_pair(GNT_COLOR_NORMAL))),(size - position));
  }
  else {
    (wmove((widget -> window),0,0) == -1)?-1 : whline((widget -> window),((acs_map[(unsigned char )'q'] | (gnt_color_pair(GNT_COLOR_NORMAL))) | (((chtype )1UL) << 13 + 8)),position);
    (wmove((widget -> window),0,position) == -1)?-1 : whline((widget -> window),(acs_map[(unsigned char )'q'] | (gnt_color_pair(GNT_COLOR_NORMAL))),(size - position));
  }
  (wmove((widget -> window),(((slider -> vertical) != 0)?((size - position) - 1) : 0),(((slider -> vertical) != 0)?0 : position)) == -1)?-1 : waddch((widget -> window),(acs_map[(unsigned char )'a'] | (gnt_color_pair(attr))));
}

static void gnt_slider_size_request(GntWidget *widget)
{
  if (( *((GntSlider *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_slider_get_gtype())))).vertical != 0) {
    widget -> priv.width = 1;
    widget -> priv.height = 5;
  }
  else {
    widget -> priv.width = 5;
    widget -> priv.height = 1;
  }
}

static void gnt_slider_map(GntWidget *widget)
{
  if ((widget -> priv.width == 0) || (widget -> priv.height == 0)) 
    gnt_widget_size_request(widget);;
}

static gboolean step_back(GntBindable *bindable,GList *null)
{
  GntSlider *slider = (GntSlider *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_slider_get_gtype()));
  gnt_slider_advance_step(slider,-1);
  return 1;
}

static gboolean small_step_back(GntBindable *bindable,GList *null)
{
  GntSlider *slider = (GntSlider *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_slider_get_gtype()));
  gnt_slider_set_value(slider,((slider -> current) - (slider -> smallstep)));
  return 1;
}

static gboolean large_step_back(GntBindable *bindable,GList *null)
{
  GntSlider *slider = (GntSlider *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_slider_get_gtype()));
  gnt_slider_set_value(slider,((slider -> current) - (slider -> largestep)));
  return 1;
}

static gboolean step_forward(GntBindable *bindable,GList *list)
{
  GntSlider *slider = (GntSlider *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_slider_get_gtype()));
  gnt_slider_advance_step(slider,1);
  return 1;
}

static gboolean small_step_forward(GntBindable *bindable,GList *null)
{
  GntSlider *slider = (GntSlider *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_slider_get_gtype()));
  gnt_slider_set_value(slider,((slider -> current) + (slider -> smallstep)));
  return 1;
}

static gboolean large_step_forward(GntBindable *bindable,GList *null)
{
  GntSlider *slider = (GntSlider *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_slider_get_gtype()));
  gnt_slider_set_value(slider,((slider -> current) + (slider -> largestep)));
  return 1;
}

static gboolean move_min_value(GntBindable *bindable,GList *null)
{
  GntSlider *slider = (GntSlider *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_slider_get_gtype()));
  gnt_slider_set_value(slider,(slider -> min));
  return 1;
}

static gboolean move_max_value(GntBindable *bindable,GList *null)
{
  GntSlider *slider = (GntSlider *)(g_type_check_instance_cast(((GTypeInstance *)bindable),gnt_slider_get_gtype()));
  gnt_slider_set_value(slider,(slider -> max));
  return 1;
}

static void gnt_slider_class_init(GntSliderClass *klass)
{
  GntBindableClass *bindable = (GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()));
  parent_class = ((GntWidgetClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_widget_get_gtype())));
  parent_class -> draw = gnt_slider_draw;
  parent_class -> map = gnt_slider_map;
  parent_class -> size_request = gnt_slider_size_request;
  klass -> changed = ((void (*)(GntSlider *, int ))((void *)0));
  signals[SIG_VALUE_CHANGED] = g_signal_new("changed",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GntSliderClass *)((GntSliderClass *)0))).changed))),0,0,g_cclosure_marshal_VOID__INT,((GType )(1 << 2)),1,((GType )(6 << 2)));
  gnt_bindable_class_register_action(bindable,"step-backward",step_back,((cur_term -> type.Strings[79] != 0)?cur_term -> type.Strings[79] : ""),((void *)((void *)0)));
  gnt_bindable_register_binding(bindable,"step-backward",((cur_term -> type.Strings[61] != 0)?cur_term -> type.Strings[61] : ""),((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"step-forward",step_forward,((cur_term -> type.Strings[83] != 0)?cur_term -> type.Strings[83] : ""),((void *)((void *)0)));
  gnt_bindable_register_binding(bindable,"step-forward",((cur_term -> type.Strings[87] != 0)?cur_term -> type.Strings[87] : ""),((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"small-step-backward",small_step_back,((gnt_key_cleft != 0)?gnt_key_cleft : ""),((void *)((void *)0)));
  gnt_bindable_register_binding(bindable,"small-step-backward",((gnt_key_cdown != 0)?gnt_key_cdown : ""),((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"small-step-forward",small_step_forward,((gnt_key_cright != 0)?gnt_key_cright : ""),((void *)((void *)0)));
  gnt_bindable_register_binding(bindable,"small-step-forward",((gnt_key_cup != 0)?gnt_key_cup : ""),((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"large-step-backward",large_step_back,((cur_term -> type.Strings[81] != 0)?cur_term -> type.Strings[81] : ""),((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"large-step-forward",large_step_forward,((cur_term -> type.Strings[82] != 0)?cur_term -> type.Strings[82] : ""),((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"min-value",move_min_value,((cur_term -> type.Strings[76] != 0)?cur_term -> type.Strings[76] : ""),((void *)((void *)0)));
  gnt_bindable_class_register_action(bindable,"max-value",move_max_value,((cur_term -> type.Strings[164] != 0)?cur_term -> type.Strings[164] : ""),((void *)((void *)0)));
  gnt_style_read_actions(( *((GTypeClass *)klass)).g_type,((GntBindableClass *)(g_type_check_class_cast(((GTypeClass *)klass),gnt_bindable_get_gtype()))));
}

static void gnt_slider_init(GTypeInstance *instance,gpointer class)
{
  GntWidget *widget = (GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)instance),gnt_widget_get_gtype()));
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= (GNT_WIDGET_NO_SHADOW | GNT_WIDGET_NO_BORDER | GNT_WIDGET_CAN_TAKE_FOCUS);
  widget -> priv.minw = 1;
  widget -> priv.minh = 1;;
}
/******************************************************************************
 * GntSlider API
 *****************************************************************************/

GType gnt_slider_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(GntSliderClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gnt_slider_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GntSlider ))), (0), (gnt_slider_init), ((const GTypeValueTable *)((void *)0))
/* base_init        */
/* base_finalize    */
/* class_finalize   */
/* class_data       */
/* n_preallocs      */
/* instance_init    */
/* value_table      */
};
    type = g_type_register_static(gnt_widget_get_gtype(),"GntSlider",&info,0);
  }
  return type;
}

GntWidget *gnt_slider_new(gboolean vertical,int max,int min)
{
  GntWidget *widget = (g_object_new(gnt_slider_get_gtype(),0));
  GntSlider *slider = (GntSlider *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_slider_get_gtype()));
  slider -> vertical = vertical;
  if (vertical != 0) {
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_GROW_Y;
  }
  else {
    ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_GROW_X;
  }
  gnt_slider_set_range(slider,max,min);
  slider -> step = 1;
  return widget;
}

void gnt_slider_set_value(GntSlider *slider,int value)
{
  int old;
  if ((slider -> current) == value) 
    return ;
  old = (slider -> current);
  slider -> current = value;
  sanitize_value(slider);
  if (old == (slider -> current)) 
    return ;
  redraw_slider(slider);
  slider_value_changed(slider);
}

int gnt_slider_get_value(GntSlider *slider)
{
  return slider -> current;
}

int gnt_slider_advance_step(GntSlider *slider,int steps)
{
  gnt_slider_set_value(slider,((slider -> current) + (steps * (slider -> step))));
  return slider -> current;
}

void gnt_slider_set_step(GntSlider *slider,int step)
{
  slider -> step = step;
}

void gnt_slider_set_small_step(GntSlider *slider,int step)
{
  slider -> smallstep = step;
}

void gnt_slider_set_large_step(GntSlider *slider,int step)
{
  slider -> largestep = step;
}

void gnt_slider_set_range(GntSlider *slider,int max,int min)
{
  slider -> max = ((max > min)?max : min);
  slider -> min = ((max < min)?max : min);
  sanitize_value(slider);
}

static void update_label(GntSlider *slider,int current_value,GntLabel *label)
{
  char value[256UL];
  g_snprintf(value,(sizeof(value)),"%d/%d",current_value,(slider -> max));
  gnt_label_set_text(label,value);
}

void gnt_slider_reflect_label(GntSlider *slider,GntLabel *label)
{
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)slider),((GType )(20 << 2))))),"changed",((GCallback )update_label),label,0,((GConnectFlags )0));
}
