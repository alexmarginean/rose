/**
 * @file gntsound.c GNT Sound API
 * @ingroup finch
 */
/* finch
 *
 * Finch is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "finch.h"
#include <internal.h>
#ifdef _WIN32
#include <windows.h>
#include <mmsystem.h>
#endif
#ifdef USE_GSTREAMER
#include <gst/gst.h>
#endif /* USE_GSTREAMER */
#include "debug.h"
#include "notify.h"
#include "prefs.h"
#include "sound.h"
#include "util.h"
#include "gntconv.h"
#include "gntbox.h"
#include "gntwindow.h"
#include "gntcombobox.h"
#include "gntlabel.h"
#include "gntconv.h"
#include "gntsound.h"
#include "gntwidget.h"
#include "gntentry.h"
#include "gntcheckbox.h"
#include "gntline.h"
#include "gntslider.h"
#include "gnttree.h"
#include "gntfilesel.h"
typedef struct __unnamed_class___F0_L60_C9_unknown_scope_and_name_variable_declaration__variable_type_L676R_variable_name_unknown_scope_and_name__scope__id__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__label__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__pref__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__def__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__file {
PurpleSoundEventID id;
char *label;
char *pref;
char *def;
char *file;}FinchSoundEvent;
typedef struct __unnamed_class___F0_L68_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__method__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__command__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__conv_focus__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__while_status__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__volume__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__events__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__selector__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__profiles__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__new_profile__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__original_profile {
GntWidget *method;
GntWidget *command;
GntWidget *conv_focus;
GntWidget *while_status;
GntWidget *volume;
GntWidget *events;
GntWidget *window;
GntWidget *selector;
GntWidget *profiles;
GntWidget *new_profile;
gchar *original_profile;}SoundPrefDialog;
#define DEFAULT_PROFILE "default"
static SoundPrefDialog *pref_dialog;
#define PLAY_SOUND_TIMEOUT 15000
static guint mute_login_sounds_timeout = 0;
static gboolean mute_login_sounds = 0;
#ifdef USE_GSTREAMER
static gboolean gst_init_failed;
#endif /* USE_GSTREAMER */
static FinchSoundEvent sounds[12UL] = {{(PURPLE_SOUND_BUDDY_ARRIVE), ("Buddy logs in"), ("login"), ("login.wav"), ((char *)((void *)0))}, {(PURPLE_SOUND_BUDDY_LEAVE), ("Buddy logs out"), ("logout"), ("logout.wav"), ((char *)((void *)0))}, {(PURPLE_SOUND_RECEIVE), ("Message received"), ("im_recv"), ("receive.wav"), ((char *)((void *)0))}, {(PURPLE_SOUND_FIRST_RECEIVE), ("Message received begins conversation"), ("first_im_recv"), ("receive.wav"), ((char *)((void *)0))}, {(PURPLE_SOUND_SEND), ("Message sent"), ("send_im"), ("send.wav"), ((char *)((void *)0))}, {(PURPLE_SOUND_CHAT_JOIN), ("Person enters chat"), ("join_chat"), ("login.wav"), ((char *)((void *)0))}, {(PURPLE_SOUND_CHAT_LEAVE), ("Person leaves chat"), ("left_chat"), ("logout.wav"), ((char *)((void *)0))}, {(PURPLE_SOUND_CHAT_YOU_SAY), ("You talk in chat"), ("send_chat_msg"), ("send.wav"), ((char *)((void *)0))}, {(PURPLE_SOUND_CHAT_SAY), ("Others talk in chat"), ("chat_msg_recv"), ("receive.wav"), ((char *)((void *)0))}, {(PURPLE_SOUND_POUNCE_DEFAULT), ((char *)((void *)0)), ("pounce_default"), ("alert.wav"), ((char *)((void *)0))}, {(PURPLE_SOUND_CHAT_NICK), ("Someone says your username in chat"), ("nick_said"), ("alert.wav"), ((char *)((void *)0))}, {(PURPLE_SOUND_GOT_ATTENTION), ("Attention received"), ("got_attention"), ("alert.wav"), ((char *)((void *)0))}};

const char *finch_sound_get_active_profile()
{
  return purple_prefs_get_string("/finch/sound/actprofile");
}
/* This method creates a pref name based on the current active profile.
 * So if "Home" is the current active profile the pref name
 * [FINCH_PREFS_ROOT "/sound/profiles/Home/$NAME"] is created.
 */

static gchar *make_pref(const char *name)
{
  static char pref_string[512UL];
  g_snprintf(pref_string,(sizeof(pref_string)),"/finch/sound/profiles/%s%s",finch_sound_get_active_profile(),name);
  return pref_string;
}

static gboolean unmute_login_sounds_cb(gpointer data)
{
  mute_login_sounds = 0;
  mute_login_sounds_timeout = 0;
  return 0;
}

static gboolean chat_nick_matches_name(PurpleConversation *conv,const char *aname)
{
  PurpleConvChat *chat = (PurpleConvChat *)((void *)0);
  char *nick = (char *)((void *)0);
  char *name = (char *)((void *)0);
  gboolean ret = 0;
  PurpleAccount *account;
  chat = purple_conversation_get_chat_data(conv);
  if (chat == ((PurpleConvChat *)((void *)0))) 
    return ret;
  account = purple_conversation_get_account(conv);
  nick = g_strdup(purple_normalize(account,(chat -> nick)));
  name = g_strdup(purple_normalize(account,aname));
  if (g_utf8_collate(nick,name) == 0) 
    ret = 1;
  g_free(nick);
  g_free(name);
  return ret;
}
/*
 * play a sound event for a conversation, honoring make_sound flag
 * of conversation and checking for focus if conv_focus pref is set
 */

static void play_conv_event(PurpleConversation *conv,PurpleSoundEventID event)
{
/* If we should not play the sound for some reason, then exit early */
  if (conv != ((PurpleConversation *)((void *)0))) {
    FinchConv *gntconv;
    gboolean has_focus;
    gntconv = ((FinchConv *)(conv -> ui_data));
    has_focus = purple_conversation_has_focus(conv);
    if ((((gntconv -> flags) & FINCH_CONV_NO_SOUND) != 0U) || ((has_focus != 0) && !(purple_prefs_get_bool((make_pref("/conv_focus"))) != 0))) {
      return ;
    }
  }
  purple_sound_play_event(event,((conv != 0)?purple_conversation_get_account(conv) : ((struct _PurpleAccount *)((void *)0))));
}

static void buddy_state_cb(PurpleBuddy *buddy,PurpleSoundEventID event)
{
  purple_sound_play_event(event,(purple_buddy_get_account(buddy)));
}

static void im_msg_received_cb(PurpleAccount *account,char *sender,char *message,PurpleConversation *conv,PurpleMessageFlags flags,PurpleSoundEventID event)
{
  if ((flags & PURPLE_MESSAGE_DELAYED) != 0U) 
    return ;
  if (conv == ((PurpleConversation *)((void *)0))) {
    purple_sound_play_event(PURPLE_SOUND_FIRST_RECEIVE,account);
  }
  else {
    play_conv_event(conv,event);
  }
}

static void im_msg_sent_cb(PurpleAccount *account,const char *receiver,const char *message,PurpleSoundEventID event)
{
  PurpleConversation *conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,receiver,account);
  play_conv_event(conv,event);
}

static void chat_buddy_join_cb(PurpleConversation *conv,const char *name,PurpleConvChatBuddyFlags flags,gboolean new_arrival,PurpleSoundEventID event)
{
  if ((new_arrival != 0) && !(chat_nick_matches_name(conv,name) != 0)) 
    play_conv_event(conv,event);
}

static void chat_buddy_left_cb(PurpleConversation *conv,const char *name,const char *reason,PurpleSoundEventID event)
{
  if (!(chat_nick_matches_name(conv,name) != 0)) 
    play_conv_event(conv,event);
}

static void chat_msg_sent_cb(PurpleAccount *account,const char *message,int id,PurpleSoundEventID event)
{
  PurpleConnection *conn = purple_account_get_connection(account);
  PurpleConversation *conv = (PurpleConversation *)((void *)0);
  if (conn != ((PurpleConnection *)((void *)0))) 
    conv = purple_find_chat(conn,id);
  play_conv_event(conv,event);
}

static void chat_msg_received_cb(PurpleAccount *account,char *sender,char *message,PurpleConversation *conv,PurpleMessageFlags flags,PurpleSoundEventID event)
{
  PurpleConvChat *chat;
  if ((flags & PURPLE_MESSAGE_DELAYED) != 0U) 
    return ;
  chat = purple_conversation_get_chat_data(conv);
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return ;
    };
  }while (0);
  if (purple_conv_chat_is_user_ignored(chat,sender) != 0) 
    return ;
  if (chat_nick_matches_name(conv,sender) != 0) 
    return ;
  if (((flags & PURPLE_MESSAGE_NICK) != 0U) || (purple_utf8_has_word(message,(chat -> nick)) != 0)) 
    play_conv_event(conv,PURPLE_SOUND_CHAT_NICK);
  else 
    play_conv_event(conv,event);
}

static void got_attention_cb(PurpleAccount *account,const char *who,PurpleConversation *conv,guint type,PurpleSoundEventID event)
{
  play_conv_event(conv,event);
}
/*
 * We mute sounds for the 10 seconds after you log in so that
 * you don't get flooded with sounds when the blist shows all
 * your buddies logging in.
 */

static void account_signon_cb(PurpleConnection *gc,gpointer data)
{
  if (mute_login_sounds_timeout != 0) 
    g_source_remove(mute_login_sounds_timeout);
  mute_login_sounds = 1;
  mute_login_sounds_timeout = purple_timeout_add_seconds(10,unmute_login_sounds_cb,0);
}

static void *finch_sound_get_handle()
{
  static int handle;
  return (&handle);
}
/* This gets called when the active profile changes */

static void initialize_profile(const char *name,PurplePrefType type,gconstpointer val,gpointer null)
{
  FinchSoundEvent *event;
  if (purple_prefs_exists((make_pref(""))) != 0) 
    return ;
  purple_prefs_add_none((make_pref("")));
  purple_prefs_add_none((make_pref("/enabled")));
  purple_prefs_add_none((make_pref("/file")));
  for (event = sounds; (event - sounds) < PURPLE_NUM_SOUNDS; event++) {
    char pref[512UL];
    g_snprintf(pref,(sizeof(pref)),"/enabled/%s",(event -> pref));
    purple_prefs_add_bool((make_pref(pref)),0);
    g_snprintf(pref,(sizeof(pref)),"/file/%s",(event -> pref));
    purple_prefs_add_path((make_pref(pref)),"");
  }
  purple_prefs_add_bool((make_pref("/conv_focus")),0);
  purple_prefs_add_bool((make_pref("/mute")),0);
  purple_prefs_add_path((make_pref("/command")),"");
  purple_prefs_add_string((make_pref("/method")),"automatic");
  purple_prefs_add_int((make_pref("/volume")),50);
}

static void update_profiles()
{
  GList *list = finch_sound_get_profiles();
  for (; list != 0; list = g_list_delete_link(list,list)) {
    char pname[512UL];
/* got_attention was added in libpurple 2.7.0 */
    g_snprintf(pname,(sizeof(pname)),"/finch/sound/profiles/%s%s",((char *)(list -> data)),"/enabled/got_attention");
    purple_prefs_add_bool(pname,0);
    g_snprintf(pname,(sizeof(pname)),"/finch/sound/profiles/%s%s",((char *)(list -> data)),"/file/got_attention");
    purple_prefs_add_path(pname,"");
    g_free((list -> data));
  }
}

static void finch_sound_init()
{
  void *gnt_sound_handle = finch_sound_get_handle();
  void *blist_handle = purple_blist_get_handle();
  void *conv_handle = purple_conversations_get_handle();
#ifdef USE_GSTREAMER
  GError *error = (GError *)((void *)0);
#endif
  purple_signal_connect(purple_connections_get_handle(),"signed-on",gnt_sound_handle,((PurpleCallback )account_signon_cb),0);
  purple_prefs_add_none("/finch/sound");
  purple_prefs_add_string("/finch/sound/actprofile","default");
  purple_prefs_add_none("/finch/sound/profiles");
  purple_prefs_connect_callback(gnt_sound_handle,"/finch/sound/actprofile",initialize_profile,0);
  purple_prefs_trigger_callback("/finch/sound/actprofile");
#ifdef USE_GSTREAMER
  purple_debug_info("sound","Initializing sound output drivers.\n");
#if (GST_VERSION_MAJOR > 0 || \
	(GST_VERSION_MAJOR == 0 && GST_VERSION_MINOR > 10) || \
	 (GST_VERSION_MAJOR == 0 && GST_VERSION_MINOR == 10 && GST_VERSION_MICRO >= 10))
  gst_registry_fork_set_enabled(0);
#endif
  if ((gst_init_failed = !(gst_init_check(0,0,&error) != 0)) != 0) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","GStreamer Failure"))),((const char *)(dgettext("pidgin","GStreamer failed to initialize."))),((error != 0)?(error -> message) : ""),0,0);
    if (error != 0) {
      g_error_free(error);
      error = ((GError *)((void *)0));
    }
  }
#endif /* USE_GSTREAMER */
  purple_signal_connect(blist_handle,"buddy-signed-on",gnt_sound_handle,((PurpleCallback )buddy_state_cb),0);
  purple_signal_connect(blist_handle,"buddy-signed-off",gnt_sound_handle,((PurpleCallback )buddy_state_cb),((void *)((gpointer )((glong )PURPLE_SOUND_BUDDY_LEAVE))));
  purple_signal_connect(conv_handle,"received-im-msg",gnt_sound_handle,((PurpleCallback )im_msg_received_cb),((void *)((gpointer )((glong )PURPLE_SOUND_RECEIVE))));
  purple_signal_connect(conv_handle,"sent-im-msg",gnt_sound_handle,((PurpleCallback )im_msg_sent_cb),((void *)((gpointer )((glong )PURPLE_SOUND_SEND))));
  purple_signal_connect(conv_handle,"chat-buddy-joined",gnt_sound_handle,((PurpleCallback )chat_buddy_join_cb),((void *)((gpointer )((glong )PURPLE_SOUND_CHAT_JOIN))));
  purple_signal_connect(conv_handle,"chat-buddy-left",gnt_sound_handle,((PurpleCallback )chat_buddy_left_cb),((void *)((gpointer )((glong )PURPLE_SOUND_CHAT_LEAVE))));
  purple_signal_connect(conv_handle,"sent-chat-msg",gnt_sound_handle,((PurpleCallback )chat_msg_sent_cb),((void *)((gpointer )((glong )PURPLE_SOUND_CHAT_YOU_SAY))));
  purple_signal_connect(conv_handle,"received-chat-msg",gnt_sound_handle,((PurpleCallback )chat_msg_received_cb),((void *)((gpointer )((glong )PURPLE_SOUND_CHAT_SAY))));
  purple_signal_connect(conv_handle,"got-attention",gnt_sound_handle,((PurpleCallback )got_attention_cb),((void *)((gpointer )((glong )PURPLE_SOUND_GOT_ATTENTION))));
  update_profiles();
}

static void finch_sound_uninit()
{
#ifdef USE_GSTREAMER
  if (!(gst_init_failed != 0)) 
    gst_deinit();
#endif
  purple_signals_disconnect_by_handle(finch_sound_get_handle());
}
#ifdef USE_GSTREAMER

static gboolean bus_call(GstBus *bus,GstMessage *msg,gpointer data)
{
  GstElement *play = data;
  GError *err = (GError *)((void *)0);
  switch(( *((GstMessage *)msg)).type){
    case GST_MESSAGE_ERROR:
{
      gst_message_parse_error(msg,&err,0);
      purple_debug_error("gstreamer","%s\n",(err -> message));
      g_error_free(err);
    }
/* fall-through and clean up */
    case GST_MESSAGE_EOS:
{
      gst_element_set_state(play,GST_STATE_NULL);
      gst_object_unref(((GstObject *)(g_type_check_instance_cast(((GTypeInstance *)play),gst_object_get_type()))));
      break; 
    }
    case GST_MESSAGE_WARNING:
{
      gst_message_parse_warning(msg,&err,0);
      purple_debug_warning("gstreamer","%s\n",(err -> message));
      g_error_free(err);
      break; 
    }
    default:
{
      break; 
    }
  }
  return 1;
}
#endif

static void finch_sound_play_file(const char *filename)
{
  const char *method;
#ifdef USE_GSTREAMER
  float volume;
  char *uri;
  GstElement *sink = (GstElement *)((void *)0);
  GstElement *play = (GstElement *)((void *)0);
  GstBus *bus = (GstBus *)((void *)0);
#endif
  if (purple_prefs_get_bool((make_pref("/mute"))) != 0) 
    return ;
  method = purple_prefs_get_string((make_pref("/method")));
  if (!(strcmp(method,"nosound") != 0)) {
    return ;
  }
  else if (!(strcmp(method,"beep") != 0)) {
    beep();
    return ;
  }
  if (!(g_file_test(filename,G_FILE_TEST_EXISTS) != 0)) {
    purple_debug_error("gntsound","sound file (%s) does not exist.\n",filename);
    return ;
  }
#ifndef _WIN32
  if (!(strcmp(method,"custom") != 0)) {
    const char *sound_cmd;
    char *command;
    char *esc_filename;
    GError *error = (GError *)((void *)0);
    sound_cmd = purple_prefs_get_path((make_pref("/command")));
    if (!(sound_cmd != 0) || (( *sound_cmd) == 0)) {
      purple_debug_error("gntsound","\'Command\' sound method has been chosen, but no command has been set.");
      return ;
    }
    esc_filename = g_shell_quote(filename);
    if (strstr(sound_cmd,"%s") != 0) 
      command = purple_strreplace(sound_cmd,"%s",esc_filename);
    else 
      command = g_strdup_printf("%s %s",sound_cmd,esc_filename);
    if (!(g_spawn_command_line_async(command,&error) != 0)) {
      purple_debug_error("gntsound","sound command could not be launched: %s\n",(error -> message));
      g_error_free(error);
    }
    g_free(esc_filename);
    g_free(command);
    return ;
  }
#ifdef USE_GSTREAMER
/* Perhaps do beep instead? */
  if (gst_init_failed != 0) 
    return ;
  volume = (((float )(((purple_prefs_get_int((make_pref("/volume"))) > 100)?100 : (((purple_prefs_get_int((make_pref("/volume"))) < 0)?0 : purple_prefs_get_int((make_pref("/volume")))))))) / 50);
  if (!(strcmp(method,"automatic") != 0)) {
    if (purple_running_gnome() != 0) {
      sink = gst_element_factory_make("gconfaudiosink","sink");
    }
    if (!(sink != 0)) 
      sink = gst_element_factory_make("autoaudiosink","sink");
    if (!(sink != 0)) {
      purple_debug_error("sound","Unable to create GStreamer audiosink.\n");
      return ;
    }
  }
  else if (!(strcmp(method,"esd") != 0)) {
    sink = gst_element_factory_make("esdsink","sink");
    if (!(sink != 0)) {
      purple_debug_error("sound","Unable to create GStreamer audiosink.\n");
      return ;
    }
  }
  else if (!(strcmp(method,"alsa") != 0)) {
    sink = gst_element_factory_make("alsasink","sink");
    if (!(sink != 0)) {
      purple_debug_error("sound","Unable to create GStreamer audiosink.\n");
      return ;
    }
  }
  else {
    purple_debug_error("sound","Unknown sound method \'%s\'\n",method);
    return ;
  }
  play = gst_element_factory_make("playbin","play");
  if (play == ((GstElement *)((void *)0))) {
    return ;
  }
  uri = g_strdup_printf("file://%s",filename);
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)play),((GType )(20 << 2))))),"uri",uri,"volume",volume,"audio-sink",sink,((void *)((void *)0)));
  bus = gst_pipeline_get_bus(((GstPipeline *)(g_type_check_instance_cast(((GTypeInstance *)play),gst_pipeline_get_type()))));
  gst_bus_add_watch(bus,bus_call,play);
  gst_element_set_state(play,GST_STATE_PLAYING);
  gst_object_unref(bus);
  g_free(uri);
#else /* USE_GSTREAMER */
#endif /* USE_GSTREAMER */
#else /* _WIN32 */
#endif /* _WIN32 */
}

static void finch_sound_play_event(PurpleSoundEventID event)
{
  char *enable_pref;
  char *file_pref;
  if ((event == PURPLE_SOUND_BUDDY_ARRIVE) && (mute_login_sounds != 0)) 
    return ;
  if ((event >= PURPLE_NUM_SOUNDS) || (event >= sizeof(sounds) / sizeof(sounds[0]))) {
    purple_debug_error("sound","got request for unknown sound: %d\n",event);
    return ;
  }
  enable_pref = g_strdup_printf("/finch/sound/profiles/%s/enabled/%s",finch_sound_get_active_profile(),sounds[event].pref);
  file_pref = g_strdup_printf("/finch/sound/profiles/%s/file/%s",finch_sound_get_active_profile(),sounds[event].pref);
/* check NULL for sounds that don't have an option, ie buddy pounce */
  if (purple_prefs_get_bool(enable_pref) != 0) {
    char *filename = g_strdup(purple_prefs_get_path(file_pref));
    if (!(filename != 0) || !(strlen(filename) != 0UL)) {
      g_free(filename);
/* XXX Consider creating a constant for "sounds/purple" to be shared with Pidgin */
      filename = g_build_filename("/usr/local/share","sounds","purple",sounds[event].def,((void *)((void *)0)));
    }
    purple_sound_play_file(filename,0);
    g_free(filename);
  }
  g_free(enable_pref);
  g_free(file_pref);
}

GList *finch_sound_get_profiles()
{
  GList *list = (GList *)((void *)0);
  GList *iter;
  iter = purple_prefs_get_children_names("/finch/sound/profiles");
  while(iter != 0){
    list = g_list_append(list,(g_strdup((strrchr((iter -> data),'/') + 1))));
    g_free((iter -> data));
    iter = g_list_delete_link(iter,iter);
  }
  return list;
}
/* This will also create it if it doesn't exist */

void finch_sound_set_active_profile(const char *name)
{
  purple_prefs_set_string("/finch/sound/actprofile",name);
}

static gboolean finch_sound_profile_exists(const char *name)
{
  gchar *tmp;
  gboolean ret = purple_prefs_exists((tmp = g_strdup_printf("/finch/sound/profiles/%s",name)));
  g_free(tmp);
  return ret;
}

static void save_cb(GntWidget *button,gpointer win)
{
  GList *itr;
  purple_prefs_set_string((make_pref("/method")),(gnt_combo_box_get_selected_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> method)),gnt_combo_box_get_gtype()))))));
  purple_prefs_set_path((make_pref("/command")),gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> command)),gnt_entry_get_gtype())))));
  purple_prefs_set_bool((make_pref("/conv_focus")),gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> conv_focus)),gnt_check_box_get_gtype())))));
  purple_prefs_set_int("/purple/sound/while_status",((gint )((glong )(gnt_combo_box_get_selected_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> while_status)),gnt_combo_box_get_gtype()))))))));
  purple_prefs_set_int((make_pref("/volume")),gnt_slider_get_value(((GntSlider *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> volume)),gnt_slider_get_gtype())))));
  for (itr = gnt_tree_get_rows(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> events)),gnt_tree_get_gtype())))); itr != 0; itr = (itr -> next)) {
    FinchSoundEvent *event = (sounds + ((gint )((glong )(itr -> data))));
    char *filepref = g_strdup_printf("/finch/sound/profiles/%s/file/%s",finch_sound_get_active_profile(),(event -> pref));
    char *boolpref = g_strdup_printf("/finch/sound/profiles/%s/enabled/%s",finch_sound_get_active_profile(),(event -> pref));
    purple_prefs_set_bool(boolpref,gnt_tree_get_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> events)),gnt_tree_get_gtype()))),(itr -> data)));
    purple_prefs_set_path(filepref,(((event -> file) != 0)?(event -> file) : ""));
    g_free(filepref);
    g_free(boolpref);
  }
  gnt_widget_destroy(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_widget_get_gtype()))));
}

static void file_cb(GntFileSel *w,const char *path,const char *file,gpointer data)
{
  FinchSoundEvent *event = data;
  g_free((event -> file));
  event -> file = g_strdup(path);
  gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> events)),gnt_tree_get_gtype()))),((gpointer )((glong )(event -> id))),1,file);
  gnt_tree_set_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> events)),gnt_tree_get_gtype()))),((gpointer )((glong )(event -> id))),1);
  gnt_widget_destroy(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)w),gnt_widget_get_gtype()))));
}

static void test_cb(GntWidget *button,gpointer null)
{
  PurpleSoundEventID id = ((gint )((glong )(gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> events)),gnt_tree_get_gtype())))))));
  FinchSoundEvent *event = (sounds + id);
  char *enabled;
  char *file;
  char *tmpfile;
  char *volpref;
  gboolean temp_value;
  int volume;
  enabled = g_strdup_printf("/finch/sound/profiles/%s/enabled/%s",finch_sound_get_active_profile(),(event -> pref));
  file = g_strdup_printf("/finch/sound/profiles/%s/file/%s",finch_sound_get_active_profile(),(event -> pref));
  volpref = g_strdup((make_pref("/volume")));
  temp_value = purple_prefs_get_bool(enabled);
  tmpfile = g_strdup(purple_prefs_get_path(file));
  volume = purple_prefs_get_int(volpref);
  purple_prefs_set_path(file,(event -> file));
  if (!(temp_value != 0)) 
    purple_prefs_set_bool(enabled,1);
  purple_prefs_set_int(volpref,gnt_slider_get_value(((GntSlider *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> volume)),gnt_slider_get_gtype())))));
  purple_sound_play_event(id,0);
  if (!(temp_value != 0)) 
    purple_prefs_set_bool(enabled,0);
  purple_prefs_set_path(file,tmpfile);
  purple_prefs_set_int(volpref,volume);
  g_free(enabled);
  g_free(file);
  g_free(tmpfile);
  g_free(volpref);
}

static void reset_cb(GntWidget *button,gpointer null)
{
/* Don't dereference this pointer ! */
  gpointer key = gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> events)),gnt_tree_get_gtype()))));
  FinchSoundEvent *event = (sounds + ((gint )((glong )key)));
  g_free((event -> file));
  event -> file = ((char *)((void *)0));
  gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> events)),gnt_tree_get_gtype()))),key,1,((const char *)(dgettext("pidgin","(default)"))));
}

static void choose_cb(GntWidget *button,gpointer null)
{
  GntWidget *w = gnt_file_sel_new();
  GntFileSel *sel = (GntFileSel *)(g_type_check_instance_cast(((GTypeInstance *)w),gnt_file_sel_get_gtype()));
  PurpleSoundEventID id = ((gint )((glong )(gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> events)),gnt_tree_get_gtype())))))));
  FinchSoundEvent *event = (sounds + id);
  char *path = (char *)((void *)0);
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)w),gnt_box_get_gtype()))),((const char *)(dgettext("pidgin","Select Sound File ..."))));
  gnt_file_sel_set_current_location(sel,(((event != 0) && ((event -> file) != 0))?(path = g_path_get_dirname((event -> file))) : purple_home_dir()));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(sel -> cancel)),((GType )(20 << 2))))),"activate",((GCallback )gnt_widget_destroy),sel,0,G_CONNECT_SWAPPED);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)sel),((GType )(20 << 2))))),"file_selected",((GCallback )file_cb),event,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)sel),((GType )(20 << 2))))),"destroy",((GCallback )g_nullify_pointer),(&pref_dialog -> selector),0,G_CONNECT_SWAPPED);
/* If there's an already open file-selector, close that one. */
  if ((pref_dialog -> selector) != 0) 
    gnt_widget_destroy((pref_dialog -> selector));
  pref_dialog -> selector = w;
  gnt_widget_show(w);
  g_free(path);
}

static void release_pref_dialog(GntBindable *data,gpointer null)
{
  GList *itr;
  for (itr = gnt_tree_get_rows(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> events)),gnt_tree_get_gtype())))); itr != 0; itr = (itr -> next)) {
    PurpleSoundEventID id = ((gint )((glong )(itr -> data)));
    FinchSoundEvent *e = (sounds + id);
    g_free((e -> file));
    e -> file = ((char *)((void *)0));
  }
  if ((pref_dialog -> selector) != 0) 
    gnt_widget_destroy((pref_dialog -> selector));
  g_free((pref_dialog -> original_profile));
  g_free(pref_dialog);
  pref_dialog = ((SoundPrefDialog *)((void *)0));
}

static void load_pref_window(const char *profile)
{
  gint i;
  finch_sound_set_active_profile(profile);
  gnt_combo_box_set_selected(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> method)),gnt_combo_box_get_gtype()))),((gchar *)(purple_prefs_get_string((make_pref("/method"))))));
  gnt_entry_set_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> command)),gnt_entry_get_gtype()))),purple_prefs_get_path((make_pref("/command"))));
  gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> conv_focus)),gnt_check_box_get_gtype()))),purple_prefs_get_bool((make_pref("/conv_focus"))));
  gnt_combo_box_set_selected(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> while_status)),gnt_combo_box_get_gtype()))),((gpointer )((glong )(purple_prefs_get_int("/purple/sound/while_status")))));
  gnt_slider_set_value(((GntSlider *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> volume)),gnt_slider_get_gtype()))),((purple_prefs_get_int((make_pref("/volume"))) > 100)?100 : (((purple_prefs_get_int((make_pref("/volume"))) < 0)?0 : purple_prefs_get_int((make_pref("/volume")))))));
  for (i = 0; i < PURPLE_NUM_SOUNDS; i++) {{
      FinchSoundEvent *event = (sounds + i);
      gchar *boolpref;
      gchar *filepref;
      gchar *basename = (gchar *)((void *)0);
      const char *profile = finch_sound_get_active_profile();
      filepref = g_strdup_printf("/finch/sound/profiles/%s/file/%s",profile,(event -> pref));
      g_free((event -> file));
      event -> file = g_strdup(purple_prefs_get_path(filepref));
      g_free(filepref);
      if ((event -> label) == ((char *)((void *)0))) {
        continue; 
      }
      boolpref = g_strdup_printf("/finch/sound/profiles/%s/enabled/%s",profile,(event -> pref));
      gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> events)),gnt_tree_get_gtype()))),((gpointer )((glong )i)),0,(event -> label));
      gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> events)),gnt_tree_get_gtype()))),((gpointer )((glong )i)),1,(((event -> file)[0] != 0)?(basename = g_path_get_basename((event -> file))) : ((const char *)(dgettext("pidgin","(default)")))));
      g_free(basename);
      gnt_tree_set_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> events)),gnt_tree_get_gtype()))),((gpointer )((glong )i)),purple_prefs_get_bool(boolpref));
      g_free(boolpref);
    }
  }
  gnt_tree_set_selected(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> profiles)),gnt_tree_get_gtype()))),((gchar *)(finch_sound_get_active_profile())));
  gnt_widget_draw((pref_dialog -> window));
}

static void reload_pref_window(const char *profile)
{
  if (!(strcmp(profile,finch_sound_get_active_profile()) != 0)) 
    return ;
  load_pref_window(profile);
}

static void prof_del_cb(GntWidget *button,gpointer null)
{
  const char *profile = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> new_profile)),gnt_entry_get_gtype()))));
  gchar *pref;
  if (!(strcmp(profile,"default") != 0)) 
    return ;
  pref = g_strdup_printf("/finch/sound/profiles/%s",profile);
  purple_prefs_remove(pref);
  g_free(pref);
  if (!(strcmp((pref_dialog -> original_profile),profile) != 0)) {
    g_free((pref_dialog -> original_profile));
    pref_dialog -> original_profile = g_strdup("default");
  }
  if (!(strcmp(profile,finch_sound_get_active_profile()) != 0)) 
    reload_pref_window("default");
  gnt_tree_remove(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> profiles)),gnt_tree_get_gtype()))),((gchar *)profile));
}

static void prof_add_cb(GntButton *button,GntEntry *entry)
{
  const char *profile = gnt_entry_get_text(entry);
  GntTreeRow *row;
  if (!(finch_sound_profile_exists(profile) != 0)) {
    gpointer key = (g_strdup(profile));
    row = gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> profiles)),gnt_tree_get_gtype()))),profile);
    gnt_tree_add_row_after(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> profiles)),gnt_tree_get_gtype()))),key,row,0,0);
    gnt_entry_set_text(entry,"");
    gnt_tree_set_selected(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(pref_dialog -> profiles)),gnt_tree_get_gtype()))),key);
    finch_sound_set_active_profile(key);
  }
  else 
    reload_pref_window(profile);
}

static void prof_load_cb(GntTree *tree,gpointer oldkey,gpointer newkey,gpointer null)
{
  reload_pref_window(newkey);
}

static void cancel_cb(GntButton *button,gpointer win)
{
  finch_sound_set_active_profile((pref_dialog -> original_profile));
  gnt_widget_destroy(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_widget_get_gtype()))));
}

void finch_sounds_show_all()
{
  GntWidget *box;
  GntWidget *tmpbox;
  GntWidget *splitbox;
  GntWidget *cmbox;
  GntWidget *slider;
  GntWidget *entry;
  GntWidget *chkbox;
  GntWidget *button;
  GntWidget *label;
  GntWidget *tree;
  GntWidget *win;
  gint i;
  GList *itr;
  GList *list;
  if (pref_dialog != 0) {
    gnt_window_present((pref_dialog -> window));
    return ;
  }
  pref_dialog = ((SoundPrefDialog *)(g_malloc0_n(1,(sizeof(SoundPrefDialog )))));
  pref_dialog -> original_profile = g_strdup(finch_sound_get_active_profile());
  pref_dialog -> window = (win = gnt_window_box_new(0,1));
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),0);
  gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),1);
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),((const char *)(dgettext("pidgin","Sound Preferences"))));
  gnt_box_set_fill(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),1);
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),GNT_ALIGN_MID);
/* Profiles */
  splitbox = gnt_box_new(0,0);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)splitbox),gnt_box_get_gtype()))),0);
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)splitbox),gnt_box_get_gtype()))),GNT_ALIGN_TOP);
  box = gnt_box_new(0,1);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),gnt_label_new_with_format(((const char *)(dgettext("pidgin","Profiles"))),GNT_TEXT_FLAG_BOLD));
  pref_dialog -> profiles = (tree = gnt_tree_new());
  gnt_tree_set_hash_fns(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),g_str_hash,g_str_equal,g_free);
  gnt_tree_set_compare_func(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),((GCompareFunc )g_ascii_strcasecmp));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"selection-changed",((GCallback )prof_load_cb),0,0,((GConnectFlags )0));
  itr = (list = finch_sound_get_profiles());
  for (; itr != 0; itr = (itr -> next)) {
/* Do not free itr->data. It's the stored as a key for the tree, and will
		 * be freed when the tree is destroyed. */
    gnt_tree_add_row_after(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),(itr -> data),gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),(itr -> data)),0,0);
  }
  g_list_free(list);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),tree);
  pref_dialog -> new_profile = (entry = gnt_entry_new(""));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),entry);
  tmpbox = gnt_box_new(0,0);
  button = gnt_button_new("Add");
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )prof_add_cb),entry,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)tmpbox),gnt_box_get_gtype()))),button);
  button = gnt_button_new("Delete");
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )prof_del_cb),0,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)tmpbox),gnt_box_get_gtype()))),button);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),tmpbox);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)splitbox),gnt_box_get_gtype()))),box);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)splitbox),gnt_box_get_gtype()))),gnt_line_new(1));
/* Sound method */
  box = gnt_box_new(0,1);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),0);
  pref_dialog -> method = (cmbox = gnt_combo_box_new());
  gnt_tree_set_hash_fns(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)( *((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)cmbox),gnt_combo_box_get_gtype())))).dropdown),gnt_tree_get_gtype()))),g_str_hash,g_str_equal,0);
  gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)cmbox),gnt_combo_box_get_gtype()))),"automatic",((const char *)(dgettext("pidgin","Automatic"))));
  gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)cmbox),gnt_combo_box_get_gtype()))),"alsa","ALSA");
  gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)cmbox),gnt_combo_box_get_gtype()))),"esd","ESD");
  gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)cmbox),gnt_combo_box_get_gtype()))),"beep",((const char *)(dgettext("pidgin","Console Beep"))));
  gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)cmbox),gnt_combo_box_get_gtype()))),"custom",((const char *)(dgettext("pidgin","Command"))));
  gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)cmbox),gnt_combo_box_get_gtype()))),"nosound",((const char *)(dgettext("pidgin","No Sound"))));
  label = gnt_label_new_with_format(((const char *)(dgettext("pidgin","Sound Method"))),GNT_TEXT_FLAG_BOLD);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),label);
  tmpbox = gnt_box_new(1,0);
  gnt_box_set_fill(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)tmpbox),gnt_box_get_gtype()))),0);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)tmpbox),gnt_box_get_gtype()))),0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)tmpbox),gnt_box_get_gtype()))),gnt_label_new(((const char *)(dgettext("pidgin","Method: ")))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)tmpbox),gnt_box_get_gtype()))),cmbox);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),tmpbox);
  tmpbox = gnt_box_new(1,0);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)tmpbox),gnt_box_get_gtype()))),0);
  gnt_box_set_fill(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)tmpbox),gnt_box_get_gtype()))),0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)tmpbox),gnt_box_get_gtype()))),gnt_label_new(((const char *)(dgettext("pidgin","Sound Command\n(%s for filename)")))));
  pref_dialog -> command = (entry = gnt_entry_new(""));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)tmpbox),gnt_box_get_gtype()))),entry);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),tmpbox);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),gnt_line_new(0));
/* Sound options */
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),gnt_label_new_with_format(((const char *)(dgettext("pidgin","Sound Options"))),GNT_TEXT_FLAG_BOLD));
  pref_dialog -> conv_focus = (chkbox = gnt_check_box_new(((const char *)(dgettext("pidgin","Sounds when conversation has focus")))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),chkbox);
  tmpbox = gnt_box_new(1,0);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)tmpbox),gnt_box_get_gtype()))),0);
  gnt_box_set_fill(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)tmpbox),gnt_box_get_gtype()))),0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)tmpbox),gnt_box_get_gtype()))),gnt_label_new("Enable Sounds:"));
  pref_dialog -> while_status = (cmbox = gnt_combo_box_new());
  gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)cmbox),gnt_combo_box_get_gtype()))),((gpointer )((gpointer )((glong )3))),((const char *)(dgettext("pidgin","Always"))));
  gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)cmbox),gnt_combo_box_get_gtype()))),((gpointer )((gpointer )((glong )1))),((const char *)(dgettext("pidgin","Only when available"))));
  gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)cmbox),gnt_combo_box_get_gtype()))),((gpointer )((gpointer )((glong )2))),((const char *)(dgettext("pidgin","Only when not available"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)tmpbox),gnt_box_get_gtype()))),cmbox);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),tmpbox);
  tmpbox = gnt_box_new(1,0);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)tmpbox),gnt_box_get_gtype()))),0);
  gnt_box_set_fill(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)tmpbox),gnt_box_get_gtype()))),0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)tmpbox),gnt_box_get_gtype()))),gnt_label_new(((const char *)(dgettext("pidgin","Volume(0-100):")))));
  pref_dialog -> volume = (slider = gnt_slider_new(0,100,0));
  gnt_slider_set_step(((GntSlider *)(g_type_check_instance_cast(((GTypeInstance *)slider),gnt_slider_get_gtype()))),5);
  gnt_slider_set_small_step(((GntSlider *)(g_type_check_instance_cast(((GTypeInstance *)slider),gnt_slider_get_gtype()))),1);
  gnt_slider_set_large_step(((GntSlider *)(g_type_check_instance_cast(((GTypeInstance *)slider),gnt_slider_get_gtype()))),20);
  label = gnt_label_new("");
  gnt_slider_reflect_label(((GntSlider *)(g_type_check_instance_cast(((GTypeInstance *)slider),gnt_slider_get_gtype()))),((GntLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gnt_label_get_gtype()))));
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)tmpbox),gnt_box_get_gtype()))),1);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)tmpbox),gnt_box_get_gtype()))),slider);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)tmpbox),gnt_box_get_gtype()))),label);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),tmpbox);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)splitbox),gnt_box_get_gtype()))),box);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),splitbox);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),gnt_line_new(0));
/* Sound events */
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),gnt_label_new_with_format(((const char *)(dgettext("pidgin","Sound Events"))),GNT_TEXT_FLAG_BOLD));
  pref_dialog -> events = (tree = gnt_tree_new_with_columns(2));
  gnt_tree_set_column_titles(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),((const char *)(dgettext("pidgin","Event"))),((const char *)(dgettext("pidgin","File"))));
  gnt_tree_set_show_title(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),1);
  for (i = 0; i < PURPLE_NUM_SOUNDS; i++) {{
      FinchSoundEvent *event = (sounds + i);
      if ((event -> label) == ((char *)((void *)0))) {
        continue; 
      }
      gnt_tree_add_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),((gpointer )((glong )i)),gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),(event -> label),(event -> def)),0,0);
    }
  }
  gnt_tree_adjust_columns(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),tree);
  box = gnt_box_new(0,0);
  button = gnt_button_new(((const char *)(dgettext("pidgin","Test"))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )test_cb),0,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  button = gnt_button_new(((const char *)(dgettext("pidgin","Reset"))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )reset_cb),0,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  button = gnt_button_new(((const char *)(dgettext("pidgin","Choose..."))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )choose_cb),0,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),box);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),gnt_line_new(0));
/* Add new stuff before this */
  box = gnt_box_new(0,0);
  gnt_box_set_fill(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),1);
  button = gnt_button_new(((const char *)(dgettext("pidgin","Save"))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )save_cb),win,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  button = gnt_button_new(((const char *)(dgettext("pidgin","Cancel"))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )cancel_cb),win,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),box);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"destroy",((GCallback )release_pref_dialog),0,0,((GConnectFlags )0));
  load_pref_window(finch_sound_get_active_profile());
  gnt_widget_show(win);
}

gboolean finch_sound_is_enabled()
{
  const char *pref = (make_pref("/method"));
  const char *method = purple_prefs_get_string(pref);
  if (!(method != 0)) 
    return 0;
  if (strcmp(method,"nosound") == 0) 
    return 0;
  if (purple_prefs_get_int((make_pref("/volume"))) <= 0) 
    return 0;
  return 1;
}
static PurpleSoundUiOps sound_ui_ops = {(finch_sound_init), (finch_sound_uninit), (finch_sound_play_file), (finch_sound_play_event), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))};

PurpleSoundUiOps *finch_sound_get_ui_ops()
{
  return &sound_ui_ops;
}
