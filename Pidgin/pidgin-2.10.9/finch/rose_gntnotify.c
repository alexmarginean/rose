/**
 * @file gntnotify.c GNT Notify API
 * @ingroup finch
 */
/* finch
 *
 * Finch is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <internal.h>
#include <gnt.h>
#include <gntbox.h>
#include <gntbutton.h>
#include <gntlabel.h>
#include <gnttree.h>
#include <gntutils.h>
#include <gntwindow.h>
#include "finch.h"
#include <util.h>
#include "gntnotify.h"
#include "debug.h"
static struct __unnamed_class___F0_L43_C8_unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__tree {
GntWidget *window;
GntWidget *tree;}emaildialog;

static void notify_msg_window_destroy_cb(GntWidget *window,PurpleNotifyMsgType type)
{
  purple_notify_close(type,window);
}

static void *finch_notify_message(PurpleNotifyMsgType type,const char *title,const char *primary,const char *secondary)
{
  GntWidget *window;
  GntWidget *button;
  GntTextFormatFlags pf = 0;
  GntTextFormatFlags sf = 0;
  switch(type){
    case PURPLE_NOTIFY_MSG_ERROR:
{
      sf |= GNT_TEXT_FLAG_BOLD;
    }
    case PURPLE_NOTIFY_MSG_WARNING:
{
      pf |= GNT_TEXT_FLAG_UNDERLINE;
    }
    case PURPLE_NOTIFY_MSG_INFO:
{
      pf |= GNT_TEXT_FLAG_BOLD;
      break; 
    }
  }
  window = gnt_window_box_new(0,1);
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),title);
  gnt_box_set_fill(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),0);
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),GNT_ALIGN_MID);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),0);
  if (primary != 0) 
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_label_new_with_format(primary,pf));
  button = gnt_button_new(((const char *)(dgettext("pidgin","OK"))));
  if (secondary != 0) {
    GntWidget *msg;
/* XXX: This is broken.  type is PurpleNotifyMsgType, not
		 * PurpleNotifyType.  Also, the if() followed by the
		 * inner switch doesn't make much sense.
		 */
    if (type == PURPLE_NOTIFY_FORMATTED) {
      int width = -1;
      int height = -1;
      char *plain = (char *)secondary;
      msg = gnt_text_view_new();
      gnt_text_view_set_flag(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)msg),gnt_text_view_get_gtype()))),(GNT_TEXT_VIEW_TOP_ALIGN | GNT_TEXT_VIEW_NO_SCROLL));
{
        switch(type){
          case PURPLE_NOTIFY_FORMATTED:
{
            plain = purple_markup_strip_html(secondary);
            if (gnt_util_parse_xhtml_to_textview(secondary,((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)msg),gnt_text_view_get_gtype())))) != 0) 
              break; 
          }
/* Fallthrough */
          default:
{
            gnt_text_view_append_text_with_flags(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)msg),gnt_text_view_get_gtype()))),plain,sf);
          }
        }
      }
      gnt_text_view_attach_scroll_widget(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)msg),gnt_text_view_get_gtype()))),button);
      gnt_util_get_text_bound(plain,&width,&height);
      gnt_widget_set_size(msg,(width + 3),(height + 1));
      if (plain != secondary) 
        g_free(plain);
    }
    else {
      msg = gnt_label_new_with_format(secondary,sf);
    }
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),msg);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"info-widget",msg);
  }
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )gnt_widget_destroy),window,0,G_CONNECT_SWAPPED);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"destroy",((GCallback )notify_msg_window_destroy_cb),((gpointer )((glong )type)),0,((GConnectFlags )0));
  gnt_widget_show(window);
  return window;
}
/* handle is, in all/most occasions, a GntWidget * */

static void finch_close_notify(PurpleNotifyType type,void *handle)
{
  GntWidget *widget = handle;
  if (!(widget != 0)) 
    return ;
  while((widget -> parent) != 0)
    widget = (widget -> parent);
  if (type == PURPLE_NOTIFY_SEARCHRESULTS) 
    purple_notify_searchresults_free((g_object_get_data(handle,"notify-results")));
#if 1
/* This did not seem to be necessary */
  g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )notify_msg_window_destroy_cb),((gpointer )((glong )type)));
#endif
  gnt_widget_destroy(widget);
}

static void *finch_notify_formatted(const char *title,const char *primary,const char *secondary,const char *text)
{
  char *xhtml = (char *)((void *)0);
  char *t = g_strdup_printf("<span>%s%s%s</span>",((secondary != 0)?secondary : ""),((secondary != 0)?"\n" : ""),((text != 0)?text : ""));
  void *ret;
  purple_markup_html_to_xhtml(t,&xhtml,0);
  ret = finch_notify_message(PURPLE_NOTIFY_FORMATTED,title,primary,xhtml);
  g_free(t);
  g_free(xhtml);
  return ret;
}

static void reset_email_dialog()
{
  emaildialog.window = ((GntWidget *)((void *)0));
  emaildialog.tree = ((GntWidget *)((void *)0));
}

static void setup_email_dialog()
{
  GntWidget *box;
  GntWidget *tree;
  GntWidget *button;
  if (emaildialog.window != 0) 
    return ;
  emaildialog.window = (box = gnt_box_new(0,1));
  gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),1);
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),((const char *)(dgettext("pidgin","Emails"))));
  gnt_box_set_fill(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),0);
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),GNT_ALIGN_MID);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),gnt_label_new_with_format(((const char *)(dgettext("pidgin","You have mail!"))),GNT_TEXT_FLAG_BOLD));
  emaildialog.tree = (tree = gnt_tree_new_with_columns(3));
  gnt_tree_set_column_titles(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),((const char *)(dgettext("pidgin","Account"))),((const char *)(dgettext("pidgin","Sender"))),((const char *)(dgettext("pidgin","Subject"))));
  gnt_tree_set_show_title(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),1);
  gnt_tree_set_col_width(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),0,15);
  gnt_tree_set_col_width(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),1,25);
  gnt_tree_set_col_width(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),2,25);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),tree);
  button = gnt_button_new(((const char *)(dgettext("pidgin","Close"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )gnt_widget_destroy),box,0,G_CONNECT_SWAPPED);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)box),((GType )(20 << 2))))),"destroy",((GCallback )reset_email_dialog),0,0,((GConnectFlags )0));
}

static void *finch_notify_emails(PurpleConnection *gc,size_t count,gboolean detailed,const char **subjects,const char **froms,const char **tos,const char **urls)
{
  PurpleAccount *account = purple_connection_get_account(gc);
  GString *message = g_string_new(0);
  void *ret;
  static int key = 0;
  if (count == 0) 
    return 0;
  if (!(detailed != 0)) {
    g_string_append_printf(message,(ngettext("%s (%s) has %d new message.","%s (%s) has %d new messages.",((int )count))),((tos != 0)? *tos : purple_account_get_username(account)),purple_account_get_protocol_name(account),((int )count));
  }
  else {
    char *to;
    gboolean newwin = (emaildialog.window == ((GntWidget *)((void *)0)));
    if (newwin != 0) 
      setup_email_dialog();
    to = g_strdup_printf("%s (%s)",((tos != 0)? *tos : purple_account_get_username(account)),purple_account_get_protocol_name(account));
    gnt_tree_add_row_after(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)emaildialog.tree),gnt_tree_get_gtype()))),((gpointer )((glong )(++key))),gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)emaildialog.tree),gnt_tree_get_gtype()))),to,((froms != 0)? *froms : "[Unknown sender]"), *subjects),0,0);
    g_free(to);
    if (newwin != 0) 
      gnt_widget_show(emaildialog.window);
    else 
      gnt_window_present(emaildialog.window);
    return 0;
  }
  ret = finch_notify_message(PURPLE_NOTIFY_EMAIL,((const char *)(dgettext("pidgin","New Mail"))),((const char *)(dgettext("pidgin","You have mail!"))),(message -> str));
  g_string_free(message,1);
  return ret;
}

static void *finch_notify_email(PurpleConnection *gc,const char *subject,const char *from,const char *to,const char *url)
{
  return finch_notify_emails(gc,1,(subject != ((const char *)((void *)0))),((subject != 0)?&subject : ((const char **)((void *)0))),((from != 0)?&from : ((const char **)((void *)0))),((to != 0)?&to : ((const char **)((void *)0))),((url != 0)?&url : ((const char **)((void *)0))));
}
/** User information. **/
static GHashTable *userinfo;

static char *userinfo_hash(PurpleAccount *account,const char *who)
{
  char key[256UL];
  g_snprintf(key,(sizeof(key)),"%s - %s",purple_account_get_username(account),purple_normalize(account,who));
  return g_utf8_strup(key,(-1));
}

static void remove_userinfo(GntWidget *widget,gpointer key)
{
  g_hash_table_remove(userinfo,key);
}

static char *purple_notify_user_info_get_xhtml(PurpleNotifyUserInfo *user_info)
{
  GList *l;
  GString *text;
  text = g_string_new("<span>");
  for (l = purple_notify_user_info_get_entries(user_info); l != ((GList *)((void *)0)); l = (l -> next)) {
    PurpleNotifyUserInfoEntry *user_info_entry = (l -> data);
    PurpleNotifyUserInfoEntryType type = purple_notify_user_info_entry_get_type(user_info_entry);
    const char *label = purple_notify_user_info_entry_get_label(user_info_entry);
    const char *value = purple_notify_user_info_entry_get_value(user_info_entry);
/* Handle the label/value pair itself */
    if (type == PURPLE_NOTIFY_USER_INFO_ENTRY_SECTION_HEADER) 
      g_string_append(text,"<u>");
    if (label != 0) 
      g_string_append_printf(text,"<b>%s</b>",label);
    g_string_append(text,"<span>");
    if ((label != 0) && (value != 0)) 
      g_string_append(text,": ");
    if (value != 0) {
      char *strip = purple_markup_strip_html(value);
      g_string_append(text,strip);
      g_free(strip);
    }
    g_string_append(text,"</span>");
    if (type == PURPLE_NOTIFY_USER_INFO_ENTRY_SECTION_HEADER) 
      g_string_append(text,"</u>");
    else if (type == PURPLE_NOTIFY_USER_INFO_ENTRY_SECTION_BREAK) 
      g_string_append(text,"<HR/>");
    g_string_append(text,"<BR/>");
  }
  g_string_append(text,"</span>");
  return g_string_free(text,0);
}

static void *finch_notify_userinfo(PurpleConnection *gc,const char *who,PurpleNotifyUserInfo *user_info)
{
  char *primary;
  char *info;
  void *ui_handle;
  char *key = userinfo_hash(purple_connection_get_account(gc),who);
  info = purple_notify_user_info_get_xhtml(user_info);
  ui_handle = g_hash_table_lookup(userinfo,key);
  if (ui_handle != ((void *)((void *)0))) {
    GntTextView *msg = (GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ui_handle),((GType )(20 << 2))))),"info-widget"))),gnt_text_view_get_gtype()));
    char *strip = purple_markup_strip_html(info);
    int tvw;
    int tvh;
    int width;
    int height;
    int ntvw;
    int ntvh;
    while(( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)ui_handle),gnt_widget_get_gtype())))).parent != 0)
      ui_handle = ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)ui_handle),gnt_widget_get_gtype())))).parent;
    gnt_widget_get_size(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)ui_handle),gnt_widget_get_gtype()))),&width,&height);
    gnt_widget_get_size(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)msg),gnt_widget_get_gtype()))),&tvw,&tvh);
    gnt_text_view_clear(msg);
    if (!(gnt_util_parse_xhtml_to_textview(info,msg) != 0)) 
      gnt_text_view_append_text_with_flags(msg,strip,GNT_TEXT_FLAG_NORMAL);
    gnt_text_view_scroll(msg,0);
    gnt_util_get_text_bound(strip,&ntvw,&ntvh);
    ntvw += 3;
    ntvh++;
    gnt_screen_resize_widget(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)ui_handle),gnt_widget_get_gtype()))),(width + (((0 > (ntvw - tvw))?0 : (ntvw - tvw)))),(height + (((0 > (ntvh - tvh))?0 : (ntvh - tvh)))));
    g_free(strip);
    g_free(key);
  }
  else {
    primary = g_strdup_printf(((const char *)(dgettext("pidgin","Info for %s"))),who);
    ui_handle = finch_notify_formatted(((const char *)(dgettext("pidgin","Buddy Information"))),primary,0,info);
    g_hash_table_insert(userinfo,key,ui_handle);
    g_free(primary);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ui_handle),((GType )(20 << 2))))),"destroy",((GCallback )remove_userinfo),key,0,((GConnectFlags )0));
  }
  g_free(info);
  return ui_handle;
}

static void notify_button_activated(GntWidget *widget,PurpleNotifySearchButton *b)
{
  GList *list = (GList *)((void *)0);
  PurpleAccount *account = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"notify-account"));
  gpointer data = g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"notify-data");
  list = gnt_tree_get_selection_text_list(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"notify-tree"))),gnt_tree_get_gtype()))));
  ( *(b -> callback))(purple_account_get_connection(account),list,data);
  g_list_foreach(list,((GFunc )g_free),0);
  g_list_free(list);
}

static void finch_notify_sr_new_rows(PurpleConnection *gc,PurpleNotifySearchResults *results,void *data)
{
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)data),gnt_tree_get_gtype()));
  GList *o;
/* XXX: Do I need to empty the tree here? */
  for (o = (results -> rows); o != 0; o = (o -> next)) {
    gnt_tree_add_row_after(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),(o -> data),gnt_tree_create_row_from_list(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),(o -> data)),0,0);
  }
}

static void *finch_notify_searchresults(PurpleConnection *gc,const char *title,const char *primary,const char *secondary,PurpleNotifySearchResults *results,gpointer data)
{
  GntWidget *window;
  GntWidget *tree;
  GntWidget *box;
  GntWidget *button;
  GList *iter;
  int columns;
  int i;
  window = gnt_box_new(0,1);
  gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),1);
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),title);
  gnt_box_set_fill(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),1);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),0);
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),GNT_ALIGN_MID);
  if (primary != 0) 
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_label_new_with_format(primary,GNT_TEXT_FLAG_BOLD));
  if (secondary != 0) 
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_label_new_with_format(secondary,GNT_TEXT_FLAG_NORMAL));
  columns = (g_list_length((results -> columns)));
  tree = gnt_tree_new_with_columns(columns);
  gnt_tree_set_show_title(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),1);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),tree);
  i = 0;
  for (iter = (results -> columns); iter != 0; iter = (iter -> next)) {
    PurpleNotifySearchColumn *column = (iter -> data);
    gnt_tree_set_column_title(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),i,(column -> title));
    i++;
  }
  box = gnt_box_new(1,0);
  for (iter = (results -> buttons); iter != 0; iter = (iter -> next)) {
    PurpleNotifySearchButton *b = (iter -> data);
    const char *text;
    switch(b -> type){
      case PURPLE_NOTIFY_BUTTON_LABELED:
{
        text = (b -> label);
        break; 
      }
      case PURPLE_NOTIFY_BUTTON_CONTINUE:
{
        text = ((const char *)(dgettext("pidgin","Continue")));
        break; 
      }
      case PURPLE_NOTIFY_BUTTON_ADD:
{
        text = ((const char *)(dgettext("pidgin","Add")));
        break; 
      }
      case PURPLE_NOTIFY_BUTTON_INFO:
{
        text = ((const char *)(dgettext("pidgin","Info")));
        break; 
      }
      case PURPLE_NOTIFY_BUTTON_IM:
{
        text = ((const char *)(dgettext("pidgin","IM")));
        break; 
      }
      case PURPLE_NOTIFY_BUTTON_JOIN:
{
        text = ((const char *)(dgettext("pidgin","Join")));
        break; 
      }
      case PURPLE_NOTIFY_BUTTON_INVITE:
{
        text = ((const char *)(dgettext("pidgin","Invite")));
        break; 
      }
      default:
{
        text = ((const char *)(dgettext("pidgin","(none)")));
      }
    }
    button = gnt_button_new(text);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"notify-account",(purple_connection_get_account(gc)));
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"notify-data",data);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"notify-tree",tree);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )notify_button_activated),b,0,((GConnectFlags )0));
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  }
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),box);
  finch_notify_sr_new_rows(gc,results,tree);
  gnt_widget_show(window);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"notify-results",results);
  return tree;
}

static void *finch_notify_uri(const char *url)
{
  return finch_notify_message(PURPLE_NOTIFY_URI,((const char *)(dgettext("pidgin","URI"))),url,0);
}
static PurpleNotifyUiOps ops = {(finch_notify_message), (finch_notify_email), (finch_notify_emails), (finch_notify_formatted), (finch_notify_searchresults), (finch_notify_sr_new_rows), (finch_notify_userinfo), (finch_notify_uri), (finch_close_notify), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* The rest of the notify-uiops return a GntWidget.
                                     These widgets should be destroyed from here. */
};

PurpleNotifyUiOps *finch_notify_get_ui_ops()
{
  return &ops;
}

void finch_notify_init()
{
  userinfo = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,0);
}

void finch_notify_uninit()
{
  g_hash_table_destroy(userinfo);
}
