/**
 * @file grouping.c  Provides different grouping options.
 *
 * Copyright (C) 2008 Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,  USA
 */
#define PURPLE_PLUGIN
#include "internal.h"
#include "purple.h"
#include "gntblist.h"
#include "gntplugin.h"
#include "gnttree.h"
static FinchBlistManager *default_manager;
/**
 * Online/Offline
 */
static PurpleBlistNode online = {(PURPLE_BLIST_OTHER_NODE)};
static PurpleBlistNode offline = {(PURPLE_BLIST_OTHER_NODE)};

static gboolean on_offline_init()
{
  GntTree *tree = finch_blist_get_tree();
  gnt_tree_add_row_after(tree,(&online),gnt_tree_create_row(tree,((const char *)(dgettext("pidgin","Online")))),0,0);
  gnt_tree_add_row_after(tree,(&offline),gnt_tree_create_row(tree,((const char *)(dgettext("pidgin","Offline")))),0,(&online));
  return 1;
}

static gboolean on_offline_can_add_node(PurpleBlistNode *node)
{
  switch(purple_blist_node_get_type(node)){
    case PURPLE_BLIST_CONTACT_NODE:
{
{
        PurpleContact *contact = (PurpleContact *)node;
        if ((contact -> currentsize) > 0) 
          return 1;
        return 0;
      }
      break; 
    }
    case PURPLE_BLIST_BUDDY_NODE:
{
{
        PurpleBuddy *buddy = (PurpleBuddy *)node;
        if (((buddy != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0)) 
          return 1;
        if ((purple_prefs_get_bool("/finch/blist/showoffline") != 0) && (purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) 
          return 1;
        return 0;
      }
      break; 
    }
    case PURPLE_BLIST_CHAT_NODE:
{
{
        PurpleChat *chat = (PurpleChat *)node;
        return purple_account_is_connected((purple_chat_get_account(chat)));
      }
      break; 
    }
    default:
{
      return 0;
    }
  }
}

static gpointer on_offline_find_parent(PurpleBlistNode *node)
{
  gpointer ret = (gpointer )((void *)0);
  switch(purple_blist_node_get_type(node)){
    case PURPLE_BLIST_CONTACT_NODE:
{
      node = ((PurpleBlistNode *)(purple_contact_get_priority_buddy(((PurpleContact *)node))));
      ret = (((((((PurpleBuddy *)node) != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(((PurpleBuddy *)node)))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(((PurpleBuddy *)node)))) != 0))?(&online) : (&offline)));
      break; 
    }
    case PURPLE_BLIST_BUDDY_NODE:
{
      ret = (purple_blist_node_get_parent(node));
      finch_blist_manager_add_node(ret);
      break; 
    }
    case PURPLE_BLIST_CHAT_NODE:
{
      ret = (&online);
      break; 
    }
    default:
{
      break; 
    }
  }
  return ret;
}

static gboolean on_offline_create_tooltip(gpointer selected_row,GString **body,char **tool_title)
{
  PurpleBlistNode *node = selected_row;
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_OTHER_NODE) {
/* There should be some easy way of getting the total online count,
		 * or total number of chats. Doing a loop here will probably be pretty
		 * expensive. */
    if (body != 0) 
       *body = g_string_new(((node == &online)?((const char *)(dgettext("pidgin","Online Buddies"))) : ((const char *)(dgettext("pidgin","Offline Buddies")))));
    return 1;
  }
  else {
    return (default_manager != 0)?( *(default_manager -> create_tooltip))(selected_row,body,tool_title) : 0;
  }
}
static FinchBlistManager on_offline = {("on-offline"), ("Online/Offline"), (on_offline_init), ((gboolean (*)())((void *)0)), (on_offline_can_add_node), (on_offline_find_parent), (on_offline_create_tooltip), {((gpointer )((void *)0)), ((gpointer )((void *)0)), ((gpointer )((void *)0)), ((gpointer )((void *)0))}};
/**
 * Meebo-like Grouping.
 */
static PurpleBlistNode meebo = {(PURPLE_BLIST_OTHER_NODE)};

static gboolean meebo_init()
{
  GntTree *tree = finch_blist_get_tree();
  if (!(g_list_find(gnt_tree_get_rows(tree),(&meebo)) != 0)) {
    gnt_tree_add_row_last(tree,(&meebo),gnt_tree_create_row(tree,((const char *)(dgettext("pidgin","Offline")))),0);
  }
  return 1;
}

static gpointer meebo_find_parent(PurpleBlistNode *node)
{
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    PurpleBuddy *buddy = purple_contact_get_priority_buddy(((PurpleContact *)node));
    if ((buddy != 0) && !(((buddy != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0))) {
      return (&meebo);
    }
  }
  return ( *(default_manager -> find_parent))(node);
}
static FinchBlistManager meebo_group = {("meebo"), ("Meebo"), (meebo_init), ((gboolean (*)())((void *)0)), ((gboolean (*)(PurpleBlistNode *))((void *)0)), (meebo_find_parent), ((gboolean (*)(gpointer , GString **, char **))((void *)0)), {((gpointer )((void *)0)), ((gpointer )((void *)0)), ((gpointer )((void *)0)), ((gpointer )((void *)0))}};
/**
 * No Grouping.
 */

static gboolean no_group_init()
{
  GntTree *tree = finch_blist_get_tree();
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"expander-level",0,((void *)((void *)0)));
  return 1;
}

static gboolean no_group_uninit()
{
  GntTree *tree = finch_blist_get_tree();
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"expander-level",1,((void *)((void *)0)));
  return 1;
}

static gboolean no_group_can_add_node(PurpleBlistNode *node)
{
/* These happen to be the same */
  return on_offline_can_add_node(node);
}

static gpointer no_group_find_parent(PurpleBlistNode *node)
{
  gpointer ret = (gpointer )((void *)0);
  switch(purple_blist_node_get_type(node)){
    case PURPLE_BLIST_BUDDY_NODE:
{
      ret = (purple_blist_node_get_parent(node));
      finch_blist_manager_add_node(ret);
      break; 
    }
    default:
{
      break; 
    }
  }
  return ret;
}
static FinchBlistManager no_group = {("no-group"), ("No Grouping"), (no_group_init), (no_group_uninit), (no_group_can_add_node), (no_group_find_parent), ((gboolean (*)(gpointer , GString **, char **))((void *)0)), {((gpointer )((void *)0)), ((gpointer )((void *)0)), ((gpointer )((void *)0)), ((gpointer )((void *)0))}};
/**
 * Nested Grouping
 */
static GHashTable *groups;

static gboolean nested_group_init()
{
  groups = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
  return 1;
}

static gboolean nested_group_uninit()
{
  g_hash_table_destroy(groups);
  groups = ((GHashTable *)((void *)0));
  return 1;
}

static gpointer nested_group_find_parent(PurpleBlistNode *node)
{
  char *name;
  PurpleGroup *group;
  char *sep;
  PurpleBlistNode *ret;
  PurpleBlistNode *parent;
  GntTree *tree;
  if (!((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE)) 
    return ( *(default_manager -> find_parent))(node);
  group = ((PurpleGroup *)node);
  name = g_strdup(purple_group_get_name(group));
  if (!((sep = strchr(name,'/')) != 0)) {
    g_free(name);
    return ( *(default_manager -> find_parent))(node);
  }
  tree = finch_blist_get_tree();
  parent = ((PurpleBlistNode *)((void *)0));
  while(sep != 0){
     *sep = 0;
    if ((sep[1] != 0) && ((ret = ((PurpleBlistNode *)(purple_find_group(name)))) != 0)) {
      finch_blist_manager_add_node(ret);
      parent = ret;
    }
    else if (!((ret = (g_hash_table_lookup(groups,name))) != 0)) {
      ret = ((PurpleBlistNode *)(g_malloc0_n(1,(sizeof(PurpleBlistNode )))));
      g_hash_table_insert(groups,(g_strdup(name)),ret);
      ret -> type = PURPLE_BLIST_OTHER_NODE;
      gnt_tree_add_row_last(tree,ret,gnt_tree_create_row(tree,name),parent);
      parent = ret;
    }
     *sep = '/';
    sep = strchr((sep + 1),'/');
  }
  g_free(name);
  return ret;
}

static gboolean nested_group_create_tooltip(gpointer selected_row,GString **body,char **title)
{
  PurpleBlistNode *node = selected_row;
  if (!(node != 0) || ((purple_blist_node_get_type(node)) != PURPLE_BLIST_OTHER_NODE)) 
    return ( *(default_manager -> create_tooltip))(selected_row,body,title);
  if (body != 0) 
/* Perhaps list the child groups/subgroups? */
     *body = g_string_new(((const char *)(dgettext("pidgin","Nested Subgroup"))));
  return 1;
}

static gboolean nested_group_can_add_node(PurpleBlistNode *node)
{
  PurpleBlistNode *group;
  int len;
  if (!((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE)) 
    return ( *(default_manager -> can_add_node))(node);
  if (( *(default_manager -> can_add_node))(node) != 0) 
    return 1;
  len = (strlen(purple_group_get_name(((PurpleGroup *)node))));
  group = purple_blist_get_root();
  for (; group != 0; group = purple_blist_node_get_sibling_next(group)) {
    if (group == node) 
      continue; 
    if ((strncmp(purple_group_get_name(((PurpleGroup *)node)),purple_group_get_name(((PurpleGroup *)group)),len) == 0) && (( *(default_manager -> can_add_node))(group) != 0)) 
      return 1;
  }
  return 0;
}
static FinchBlistManager nested_group = {("nested"), ("Nested Grouping (experimental)"), (nested_group_init), (nested_group_uninit), (nested_group_find_parent), (nested_group_create_tooltip), (nested_group_can_add_node)};

static gboolean plugin_load(PurplePlugin *plugin)
{
  default_manager = finch_blist_manager_find("default");
  finch_blist_install_manager((&on_offline));
  finch_blist_install_manager((&meebo_group));
  finch_blist_install_manager((&no_group));
  finch_blist_install_manager((&nested_group));
  return 1;
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  finch_blist_uninstall_manager((&on_offline));
  finch_blist_uninstall_manager((&meebo_group));
  finch_blist_uninstall_manager((&no_group));
  finch_blist_uninstall_manager((&nested_group));
  return 1;
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gnt-purple"), (0), ((GList *)((void *)0)), (0), ("grouping"), ("Grouping"), ("2.10.9"), ("Provides alternate buddylist grouping options."), ("Provides alternate buddylist grouping options."), ("Sadrul H Chowdhury <sadrul@users.sourceforge.net>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))};

static void init_plugin(PurplePlugin *plugin)
{
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
