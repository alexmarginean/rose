/**
 * @file lastlog.c Lastlog plugin for purple-text.
 *
 * Copyright (C) 2006 Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#define PLUGIN_STATIC_NAME	GntLastlog
#include "internal.h"
#include <plugin.h>
#include <version.h>
#include <cmds.h>
#include <gnt.h>
#include <gnttextview.h>
#include <gntwindow.h>
#include <gntconv.h>
#include <gntplugin.h>
static PurpleCmdId cmd;

static gboolean window_kpress_cb(GntWidget *wid,const char *key,GntTextView *view)
{
  if (key[0] == 27) {
    if (strcmp(key,(((cur_term -> type.Strings[61] != 0)?cur_term -> type.Strings[61] : ""))) == 0) 
      gnt_text_view_scroll(view,1);
    else if (strcmp(key,(((cur_term -> type.Strings[87] != 0)?cur_term -> type.Strings[87] : ""))) == 0) 
      gnt_text_view_scroll(view,-1);
    else if (strcmp(key,(((cur_term -> type.Strings[81] != 0)?cur_term -> type.Strings[81] : ""))) == 0) 
      gnt_text_view_scroll(view,(wid -> priv.height - 2));
    else if (strcmp(key,(((cur_term -> type.Strings[82] != 0)?cur_term -> type.Strings[82] : ""))) == 0) 
      gnt_text_view_scroll(view,-(wid -> priv.height - 2));
    else 
      return 0;
    return 1;
  }
  return 0;
}

static PurpleCmdRet lastlog_cb(PurpleConversation *conv,const char *cmd,char **args,char **error,gpointer null)
{
  FinchConv *ggconv = (conv -> ui_data);
  char **strings = g_strsplit(( *( *((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(ggconv -> tv)),gnt_text_view_get_gtype())))).string).str,"\n",0);
  GntWidget *win;
  GntWidget *tv;
  int i;
  int j;
  win = gnt_window_new();
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),((const char *)(dgettext("pidgin","Lastlog"))));
  tv = gnt_text_view_new();
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),tv);
  gnt_widget_show(win);
  for (i = 0; strings[i] != 0; i++) {
    if (strstr(strings[i],args[0]) != ((char *)((void *)0))) {
      char **finds = g_strsplit(strings[i],args[0],0);
      for (j = 0; finds[j] != 0; j++) {
        if (j != 0) 
          gnt_text_view_append_text_with_flags(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gnt_text_view_get_gtype()))),args[0],GNT_TEXT_FLAG_BOLD);
        gnt_text_view_append_text_with_flags(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gnt_text_view_get_gtype()))),finds[j],GNT_TEXT_FLAG_NORMAL);
      }
      g_strfreev(finds);
      gnt_text_view_append_text_with_flags(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gnt_text_view_get_gtype()))),"\n",GNT_TEXT_FLAG_NORMAL);
    }
  }
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"key_pressed",((GCallback )window_kpress_cb),tv,0,((GConnectFlags )0));
  g_strfreev(strings);
  return PURPLE_CMD_STATUS_OK;
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  cmd = purple_cmd_register("lastlog","s",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM),0,lastlog_cb,((const char *)(dgettext("pidgin","lastlog: Searches for a substring in the backlog."))),0);
/* Translator Note: The "backlog" is the conversation buffer/history. */
  return 1;
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  purple_cmd_unregister(cmd);
  return 1;
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gnt-purple"), (0), ((GList *)((void *)0)), (0), ("gntlastlog"), ("GntLastlog"), ("2.10.9"), ("Lastlog plugin."), ("Lastlog plugin."), ("Sadrul H Chowdhury <sadrul@users.sourceforge.net>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
