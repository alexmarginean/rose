/**
 * @file gntgf.c Minimal toaster plugin in Gnt.
 *
 * Copyright (C) 2006 Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#define PLUGIN_STATIC_NAME	GntGf
#define PREFS_PREFIX          "/plugins/gnt/gntgf"
#define PREFS_EVENT           PREFS_PREFIX "/events"
#define PREFS_EVENT_SIGNONF   PREFS_EVENT "/signonf"
#define PREFS_EVENT_IM_MSG    PREFS_EVENT "/immsg"
#define PREFS_EVENT_CHAT_MSG  PREFS_EVENT "/chatmsg"
#define PREFS_EVENT_CHAT_NICK PREFS_EVENT "/chatnick"
#define PREFS_BEEP            PREFS_PREFIX "/beep"
#define MAX_COLS	3
#ifdef HAVE_X11
#define PREFS_URGENT          PREFS_PREFIX "/urgent"
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#endif
#include <glib.h>
#include <plugin.h>
#include <version.h>
#include <blist.h>
#include <conversation.h>
#include <debug.h>
#include <eventloop.h>
#include <util.h>
#include <gnt.h>
#include <gntbox.h>
#include <gntbutton.h>
#include <gntcheckbox.h>
#include <gntlabel.h>
#include <gnttree.h>
#include "gntplugin.h"
#include "gntconv.h"
typedef struct __unnamed_class___F0_L63_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__timer__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__column {
GntWidget *window;
int timer;
int column;}GntToast;
static GList *toasters;
static int gpsy[3UL];
static int gpsw[3UL];

static void destroy_toaster(GntToast *toast)
{
  toasters = g_list_remove(toasters,toast);
  gnt_widget_destroy((toast -> window));
  purple_timeout_remove((toast -> timer));
  g_free(toast);
}

static gboolean remove_toaster(GntToast *toast)
{
  GList *iter;
  int h;
  int col;
  int nwin[3UL];
  gnt_widget_get_size((toast -> window),0,&h);
  gpsy[toast -> column] -= h;
  col = (toast -> column);
  memset((&nwin),0,(sizeof(nwin)));
  destroy_toaster(toast);
  for (iter = toasters; iter != 0; iter = (iter -> next)) {{
      int x;
      int y;
      toast = (iter -> data);
      nwin[toast -> column]++;
      if ((toast -> column) != col) 
        continue; 
      gnt_widget_get_position((toast -> window),&x,&y);
      y += h;
      gnt_screen_move_widget((toast -> window),x,y);
    }
  }
  if (nwin[col] == 0) 
    gpsw[col] = 0;
  return 0;
}
#ifdef HAVE_X11

static int error_handler(Display *dpy,XErrorEvent *error)
{
  char buffer[1024UL];
  XGetErrorText(dpy,(error -> error_code),buffer,(sizeof(buffer)));
  purple_debug_error("gntgf","Could not set urgent to the window: %s.\n",buffer);
  return 0;
}

static void urgent()
{
/* This is from deryni/tuomov's urgent_test.c */
  Display *dpy;
  Window id;
  const char *ids;
  XWMHints *hints;
  ids = (getenv("WINDOWID"));
  if (ids == ((const char *)((void *)0))) 
    return ;
  id = (atoi(ids));
  dpy = XOpenDisplay(0);
  if (dpy == ((Display *)((void *)0))) 
    return ;
  XSetErrorHandler(error_handler);
  hints = XGetWMHints(dpy,id);
  if (hints != 0) {
    hints -> flags |= 1L << 8;
    XSetWMHints(dpy,id,hints);
    XFree(hints);
  }
  XSetErrorHandler(0);
  XFlush(dpy);
  XCloseDisplay(dpy);
}
#endif

static void notify(PurpleConversation *conv,const char *fmt,... )
{
  GntWidget *window;
  GntToast *toast;
  char *str;
  int h;
  int w;
  int i;
  va_list args;
  if (purple_prefs_get_bool("/plugins/gnt/gntgf/beep") != 0) 
    beep();
  if (conv != ((PurpleConversation *)((void *)0))) {
    FinchConv *fc = (conv -> ui_data);
    if (gnt_widget_has_focus((fc -> window)) != 0) 
      return ;
  }
#ifdef HAVE_X11
  if (purple_prefs_get_bool("/plugins/gnt/gntgf/urgent") != 0) 
    urgent();
#endif
  window = gnt_box_new(0,1);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_TRANSIENT;
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_NO_BORDER);
  va_start(args,fmt);
  str = g_strdup_vprintf(fmt,args);
  va_end(args);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_label_new_with_format(str,GNT_TEXT_FLAG_HIGHLIGHT));
  g_free(str);
  gnt_widget_size_request(window);
  gnt_widget_get_size(window,&w,&h);
  for (i = 0; (i < 3) && ((gpsy[i] + h) >= (((stdscr != 0)?((stdscr -> _maxy) + 1) : -1))); ++i) ;
  if (i >= 3) {
    purple_debug_warning("GntGf","Dude, that\'s way too many popups\n");
    gnt_widget_destroy(window);
    return ;
  }
  toast = ((GntToast *)(g_malloc0_n(1,(sizeof(GntToast )))));
  toast -> window = window;
  toast -> column = i;
  gpsy[i] += h;
  if (w > gpsw[i]) {
    if (i == 0) 
      gpsw[i] = w;
    else 
      gpsw[i] = ((gpsw[i - 1] + w) + 1);
  }
  if ((i == 0) || ((w + gpsw[i - 1]) >= (((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)))) {
/* if it's going to be too far left, overlap. */
    gnt_widget_set_position(window,(((((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)) - w) - 1),(((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) - gpsy[i]) - 1));
  }
  else {
    gnt_widget_set_position(window,((((((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)) - gpsw[i - 1]) - w) - 1),(((((stdscr != 0)?((stdscr -> _maxy) + 1) : -1)) - gpsy[i]) - 1));
  }
  gnt_widget_draw(window);
  toast -> timer = (purple_timeout_add_seconds(4,((GSourceFunc )remove_toaster),toast));
  toasters = g_list_prepend(toasters,toast);
}

static void buddy_signed_on(PurpleBuddy *buddy,gpointer null)
{
  if (purple_prefs_get_bool("/plugins/gnt/gntgf/events/signonf") != 0) 
    notify(0,((const char *)(dgettext("pidgin","%s just signed on"))),purple_buddy_get_alias(buddy));
}

static void buddy_signed_off(PurpleBuddy *buddy,gpointer null)
{
  if (purple_prefs_get_bool("/plugins/gnt/gntgf/events/signonf") != 0) 
    notify(0,((const char *)(dgettext("pidgin","%s just signed off"))),purple_buddy_get_alias(buddy));
}

static void received_im_msg(PurpleAccount *account,const char *sender,const char *msg,PurpleConversation *conv,PurpleMessageFlags flags,gpointer null)
{
  if (purple_prefs_get_bool("/plugins/gnt/gntgf/events/immsg") != 0) 
    notify(conv,((const char *)(dgettext("pidgin","%s sent you a message"))),sender);
}

static void received_chat_msg(PurpleAccount *account,const char *sender,const char *msg,PurpleConversation *conv,PurpleMessageFlags flags,gpointer null)
{
  const char *nick;
  if ((flags & PURPLE_MESSAGE_WHISPER) != 0U) 
    return ;
  nick = ( *purple_conversation_get_chat_data(conv)).nick;
  if (g_utf8_collate(sender,nick) == 0) 
    return ;
  if ((purple_prefs_get_bool("/plugins/gnt/gntgf/events/chatnick") != 0) && (purple_utf8_has_word(msg,nick) != 0)) 
    notify(conv,((const char *)(dgettext("pidgin","%s said your nick in %s"))),sender,purple_conversation_get_name(conv));
  else if (purple_prefs_get_bool("/plugins/gnt/gntgf/events/chatmsg") != 0) 
    notify(conv,((const char *)(dgettext("pidgin","%s sent a message in %s"))),sender,purple_conversation_get_name(conv));
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  purple_signal_connect(purple_blist_get_handle(),"buddy-signed-on",plugin,((PurpleCallback )buddy_signed_on),0);
  purple_signal_connect(purple_blist_get_handle(),"buddy-signed-off",plugin,((PurpleCallback )buddy_signed_off),0);
  purple_signal_connect(purple_conversations_get_handle(),"received-im-msg",plugin,((PurpleCallback )received_im_msg),0);
  purple_signal_connect(purple_conversations_get_handle(),"received-chat-msg",plugin,((PurpleCallback )received_chat_msg),0);
  memset((&gpsy),0,(sizeof(gpsy)));
  memset((&gpsw),0,(sizeof(gpsw)));
  return 1;
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  while(toasters != 0){
    GntToast *toast = (toasters -> data);
    destroy_toaster(toast);
  }
  return 1;
}
static struct __unnamed_class___F0_L300_C8_unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__pref__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__display {
char *pref;
char *display;}prefs[] = {{("/plugins/gnt/gntgf/events/signonf"), ("Buddy signs on/off")}, {("/plugins/gnt/gntgf/events/immsg"), ("You receive an IM")}, {("/plugins/gnt/gntgf/events/chatmsg"), ("Someone speaks in a chat")}, {("/plugins/gnt/gntgf/events/chatnick"), ("Someone says your name in a chat")}, {((char *)((void *)0)), ((char *)((void *)0))}};

static void pref_toggled(GntTree *tree,char *key,gpointer null)
{
  purple_prefs_set_bool(key,gnt_tree_get_choice(tree,key));
}

static void toggle_option(GntCheckBox *check,gpointer str)
{
  purple_prefs_set_bool(str,gnt_check_box_get_checked(check));
}

static GntWidget *config_frame()
{
  GntWidget *window;
  GntWidget *tree;
  GntWidget *check;
  int i;
  window = gnt_box_new(0,1);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),0);
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),GNT_ALIGN_MID);
  gnt_box_set_fill(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),1);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_label_new(((const char *)(dgettext("pidgin","Notify with a toaster when")))));
  tree = gnt_tree_new();
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),tree);
  for (i = 0; prefs[i].pref != 0; i++) {
    gnt_tree_add_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),prefs[i].pref,gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),prefs[i].display),0,0);
    gnt_tree_set_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),prefs[i].pref,purple_prefs_get_bool(prefs[i].pref));
  }
  gnt_tree_set_col_width(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),0,40);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"toggled",((GCallback )pref_toggled),0,0,((GConnectFlags )0));
  check = gnt_check_box_new(((const char *)(dgettext("pidgin","Beep too!"))));
  gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)check),gnt_check_box_get_gtype()))),purple_prefs_get_bool("/plugins/gnt/gntgf/beep"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)check),((GType )(20 << 2))))),"toggled",((GCallback )toggle_option),"/plugins/gnt/gntgf/beep",0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),check);
#ifdef HAVE_X11
  check = gnt_check_box_new(((const char *)(dgettext("pidgin","Set URGENT for the terminal window."))));
  gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)check),gnt_check_box_get_gtype()))),purple_prefs_get_bool("/plugins/gnt/gntgf/urgent"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)check),((GType )(20 << 2))))),"toggled",((GCallback )toggle_option),"/plugins/gnt/gntgf/urgent",0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),check);
#endif
  return window;
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gnt-purple"), (0), ((GList *)((void *)0)), (0), ("gntgf"), ("GntGf"), ("2.10.9"), ("Toaster plugin"), ("Toaster plugin"), ("Sadrul H Chowdhury <sadrul@users.sourceforge.net>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), (config_frame), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
  purple_prefs_add_none("/plugins");
  purple_prefs_add_none("/plugins/gnt");
  purple_prefs_add_none("/plugins/gnt/gntgf");
  purple_prefs_add_none("/plugins/gnt/gntgf/events");
  purple_prefs_add_bool("/plugins/gnt/gntgf/events/signonf",1);
  purple_prefs_add_bool("/plugins/gnt/gntgf/events/immsg",1);
  purple_prefs_add_bool("/plugins/gnt/gntgf/events/chatmsg",1);
  purple_prefs_add_bool("/plugins/gnt/gntgf/events/chatnick",1);
  purple_prefs_add_bool("/plugins/gnt/gntgf/beep",1);
#ifdef HAVE_X11
  purple_prefs_add_bool("/plugins/gnt/gntgf/urgent",0);
#endif
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
