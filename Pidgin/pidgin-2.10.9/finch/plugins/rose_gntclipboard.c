/**
 * @file gntclipboard.c
 *
 * Copyright (C) 2007 Richard Nelson <wabz@whatsbeef.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include <glib.h>
#define PLUGIN_STATIC_NAME	GntClipboard
#ifdef HAVE_X11
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#endif
#include <sys/types.h>
#include <signal.h>
#include <glib.h>
#include <plugin.h>
#include <version.h>
#include <debug.h>
#include <notify.h>
#include <gntwm.h>
#include <gntplugin.h>
#ifdef HAVE_X11
static pid_t child = 0;
static gulong sig_handle;

static void set_clip(gchar *string)
{
  Window w;
  XEvent e;
  XEvent respond;
  XSelectionRequestEvent *req;
  const char *ids;
  Display *dpy = XOpenDisplay(0);
  if (!(dpy != 0)) 
    return ;
  ids = (getenv("WINDOWID"));
  if (ids == ((const char *)((void *)0))) 
    return ;
  w = (atoi(ids));
  XSetSelectionOwner(dpy,((Atom )1),w,0L);
  XFlush(dpy);
  XSelectInput(dpy,w,1L << 17);
  while(1){
/* this blocks. */
    XNextEvent(dpy,&e);
    req = &e.xselectionrequest;
    if (e.type == 30) {
      XChangeProperty(dpy,(req -> requestor),(req -> property),((Atom )31),8,0,((unsigned char *)string),(strlen(string)));
      respond.xselection.property = (req -> property);
      respond.xselection.type = 31;
      respond.xselection.display = (req -> display);
      respond.xselection.requestor = (req -> requestor);
      respond.xselection.selection = (req -> selection);
      respond.xselection.target = (req -> target);
      respond.xselection.time = (req -> time);
      XSendEvent(dpy,(req -> requestor),0,0,&respond);
      XFlush(dpy);
    }
    else if (e.type == 29) {
      return ;
    }
  }
}

static void clipboard_changed(GntWM *wm,gchar *string)
{
  if (child != 0) {
    kill(child,15);
  }
  if ((child = (fork() == 0)) != 0) {
    set_clip(string);
    _exit(0);
  }
}
#endif

static gboolean plugin_load(PurplePlugin *plugin)
{
#ifdef HAVE_X11
  if (!(XOpenDisplay(0) != 0)) {
    purple_debug_warning("gntclipboard","Couldn\'t find X display\n");
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Error"))),((const char *)(dgettext("pidgin","Error loading the plugin."))),((const char *)(dgettext("pidgin","Couldn\'t find X display"))),0,0);
    return 0;
  }
  if (!(getenv("WINDOWID") != 0)) {
    purple_debug_warning("gntclipboard","Couldn\'t find window\n");
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Error"))),((const char *)(dgettext("pidgin","Error loading the plugin."))),((const char *)(dgettext("pidgin","Couldn\'t find window"))),0,0);
    return 0;
  }
  sig_handle = g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gnt_get_clipboard())),((GType )(20 << 2))))),"clipboard_changed",((GCallback )clipboard_changed),0,0,((GConnectFlags )0));
  return 1;
#else
#endif
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
#ifdef HAVE_X11
  if (child != 0) {
    kill(child,15);
    child = 0;
  }
  g_signal_handler_disconnect(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gnt_get_clipboard())),((GType )(20 << 2))))),sig_handle);
#endif
  return 1;
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gnt-purple"), (0), ((GList *)((void *)0)), (0), ("gntclipboard"), ("GntClipboard"), ("2.10.9"), ("Clipboard plugin"), ("When the gnt clipboard contents change, the contents are made available to X, if possible."), ("Richard Nelson <wabz@whatsbeef.net>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
