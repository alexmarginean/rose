/**
 * @file gnttinyurl.c
 *
 * Copyright (C) 2009 Richard Nelson <wabz@whatsbeef.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include <glib.h>
#define PLUGIN_STATIC_NAME	TinyURL
#define PREFS_BASE          "/plugins/gnt/tinyurl"
#define PREF_LENGTH  PREFS_BASE "/length"
#define PREF_URL  PREFS_BASE "/url"
#include <conversation.h>
#include <signals.h>
#include <glib.h>
#include <plugin.h>
#include <version.h>
#include <debug.h>
#include <notify.h>
#include <gntconv.h>
#include <gntplugin.h>
#include <gntlabel.h>
#include <gnttextview.h>
#include <gntwindow.h>
static int tag_num = 0;
typedef struct __unnamed_class___F0_L51_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L298R__Pe___variable_name_unknown_scope_and_name__scope__conv__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__tag__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__num {
PurpleConversation *conv;
gchar *tag;
int num;}CbInfo;
static void process_urls(PurpleConversation *conv,GList *urls);
/* 3 functions from util.c */

static gboolean badchar(char c)
{
  switch(c){
    case '\0':
{
    }
    case '\n':
{
    }
    case '\r':
{
    }
    case ' ':
{
    }
    case '"':
{
    }
    case '\'':
{
    }
    case ',':
{
    }
    case '<':
{
    }
    case '>':
{
      return 1;
    }
    default:
{
      return 0;
    }
  }
}

static gboolean badentity(const char *c)
{
  if ((!(g_ascii_strncasecmp(c,"&lt;",4) != 0) || !(g_ascii_strncasecmp(c,"&gt;",4) != 0)) || !(g_ascii_strncasecmp(c,"&quot;",6) != 0)) {
    return 1;
  }
  return 0;
}

static GList *extract_urls(const char *text)
{
  const char *t;
  const char *c;
  const char *q = (const char *)((void *)0);
  char *url_buf;
  GList *ret = (GList *)((void *)0);
  gboolean inside_html = 0;
  int inside_paren = 0;
  c = text;
{
    while(( *c) != 0){
      if ((( *c) == 40) && !(inside_html != 0)) {
        inside_paren++;
        c++;
      }
      if (inside_html != 0) {
        if (( *c) == '>') {
          inside_html = 0;
        }
        else if (!(q != 0) && ((( *c) == '\"') || (( *c) == '\''))) {
          q = c;
        }
        else if (q != 0) {
          if (( *c) == ( *q)) 
            q = ((const char *)((void *)0));
        }
      }
      else if (( *c) == 60) {
        inside_html = 1;
        if (!(g_ascii_strncasecmp(c,"<A",2) != 0)) {{
            while(1){
              if (( *c) == '>') {
                inside_html = 0;
                break; 
              }
              c++;
              if (!(( *c) != 0)) 
                break; 
            }
          }
        }
      }
      else if ((( *c) == 'h') && (!(g_ascii_strncasecmp(c,"http://",7) != 0) || !(g_ascii_strncasecmp(c,"https://",8) != 0))) {
        t = c;
{
          while(1){
            if ((badchar( *t) != 0) || (badentity(t) != 0)) {
              if ((!(g_ascii_strncasecmp(c,"http://",7) != 0) && ((t - c) == 7)) || (!(g_ascii_strncasecmp(c,"https://",8) != 0) && ((t - c) == 8))) {
                break; 
              }
              if ((( *t) == ',') && (t[1] != 32)) {
                t++;
                continue; 
              }
              if (( *(t - 1)) == '.') 
                t--;
              if ((( *(t - 1)) == ')') && (inside_paren > 0)) {
                t--;
              }
              url_buf = g_strndup(c,(t - c));
              if (!(g_list_find_custom(ret,url_buf,((GCompareFunc )strcmp)) != 0)) {
                purple_debug_info("TinyURL","Added URL %s\n",url_buf);
                ret = g_list_append(ret,url_buf);
              }
              else {
                g_free(url_buf);
              }
              c = t;
              break; 
            }
            t++;
          }
        }
      }
      else if (!(g_ascii_strncasecmp(c,"www.",4) != 0) && (((c == text) || (badchar(c[-1]) != 0)) || (badentity((c - 1)) != 0))) {
        if (c[4] != '.') {
          t = c;
{
            while(1){
              if ((badchar( *t) != 0) || (badentity(t) != 0)) {
                if ((t - c) == 4) {
                  break; 
                }
                if ((( *t) == ',') && (t[1] != 32)) {
                  t++;
                  continue; 
                }
                if (( *(t - 1)) == '.') 
                  t--;
                if ((( *(t - 1)) == ')') && (inside_paren > 0)) {
                  t--;
                }
                url_buf = g_strndup(c,(t - c));
                if (!(g_list_find_custom(ret,url_buf,((GCompareFunc )strcmp)) != 0)) {
                  purple_debug_info("TinyURL","Added URL %s\n",url_buf);
                  ret = g_list_append(ret,url_buf);
                }
                else {
                  g_free(url_buf);
                }
                c = t;
                break; 
              }
              t++;
            }
          }
        }
      }
      if ((( *c) == ')') && !(inside_html != 0)) {
        inside_paren--;
        c++;
      }
      if (( *c) == 0) 
        break; 
      c++;
    }
  }
  return ret;
}

static void url_fetched(PurpleUtilFetchUrlData *url_data,gpointer cb_data,const gchar *url_text,gsize len,const gchar *error_message)
{
  CbInfo *data = (CbInfo *)cb_data;
  PurpleConversation *conv = (data -> conv);
  GList *convs = purple_get_conversations();
/* ensure the conversation still exists */
  for (; convs != 0; convs = (convs -> next)) {
    if (((PurpleConversation *)(convs -> data)) == conv) {
      FinchConv *fconv = (FinchConv *)(conv -> ui_data);
      gchar *str = g_strdup_printf("[%d] %s",(data -> num),url_text);
      GntTextView *tv = (GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(fconv -> tv)),gnt_text_view_get_gtype()));
      gnt_text_view_tag_change(tv,(data -> tag),str,0);
      g_free(str);
      g_free((data -> tag));
      g_free(data);
      return ;
    }
  }
  g_free((data -> tag));
  g_free(data);
  purple_debug_info("TinyURL","Conversation no longer exists... :(\n");
}

static void free_urls(gpointer data,gpointer null)
{
  g_free(data);
}

static gboolean writing_msg(PurpleAccount *account,char *sender,char **message,PurpleConversation *conv,PurpleMessageFlags flags)
{
  GString *t;
  GList *iter;
  GList *urls;
  GList *next;
  int c = 0;
  if ((flags & (PURPLE_MESSAGE_SEND | PURPLE_MESSAGE_INVISIBLE)) != 0U) 
    return 0;
  urls = (purple_conversation_get_data(conv,"TinyURLs"));
/* message was cancelled somewhere? Reset. */
  if (urls != ((GList *)((void *)0))) 
    g_list_foreach(urls,free_urls,0);
  g_list_free(urls);
  urls = extract_urls(( *message));
  if (!(urls != 0)) 
    return 0;
  t = g_string_new(( *message));
  g_free(( *message));
  for (iter = urls; iter != 0; iter = next) {
    next = (iter -> next);
    if (g_utf8_strlen(((char *)(iter -> data)),(-1)) >= (purple_prefs_get_int("/plugins/gnt/tinyurl/length"))) {
      int pos;
      int x = 0;
      gchar *j;
      gchar *s;
      gchar *str;
      gchar *orig;
      glong len = g_utf8_strlen((iter -> data),(-1));
      s = g_strdup((t -> str));
      orig = s;
      str = g_strdup_printf("[%d]",++c);
{
/* replace all occurrences */
        while((j = strstr(s,(iter -> data))) != 0){
          pos = ((j - orig) + (x++ * 3));
          s = (j + len);
          t = g_string_insert(t,(pos + len),str);
          if (( *s) == 0) 
            break; 
        }
      }
      g_free(orig);
      g_free(str);
      continue; 
    }
    else {
      g_free((iter -> data));
      urls = g_list_delete_link(urls,iter);
    }
  }
   *message = (t -> str);
  g_string_free(t,0);
  if (conv == ((PurpleConversation *)((void *)0))) 
    conv = purple_conversation_new(PURPLE_CONV_TYPE_IM,account,sender);
  purple_conversation_set_data(conv,"TinyURLs",urls);
  return 0;
}

static void wrote_msg(PurpleAccount *account,char *sender,char *message,PurpleConversation *conv,PurpleMessageFlags flags)
{
  GList *urls;
  urls = (purple_conversation_get_data(conv,"TinyURLs"));
  if (((flags & PURPLE_MESSAGE_SEND) != 0U) || (urls == ((GList *)((void *)0)))) 
    return ;
  process_urls(conv,urls);
  purple_conversation_set_data(conv,"TinyURLs",0);
}
/* Frees 'urls' */

static void process_urls(PurpleConversation *conv,GList *urls)
{
  GList *iter;
  int c;
  FinchConv *fconv = (FinchConv *)(conv -> ui_data);
  GntTextView *tv = (GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)(fconv -> tv)),gnt_text_view_get_gtype()));
  for (((iter = urls) , (c = 0)); iter != 0; iter = (iter -> next)) {
    int i;
    CbInfo *cbdata;
    gchar *url;
    gchar *str;
    gchar *tmp;
    cbdata = ((CbInfo *)(g_malloc_n(1,(sizeof(CbInfo )))));
    cbdata -> num = ++c;
    cbdata -> tag = g_strdup_printf("%s%d","tiny_",tag_num++);
    cbdata -> conv = conv;
    tmp = purple_unescape_html(((char *)(iter -> data)));
    if ((g_ascii_strncasecmp(tmp,"http://",7) != 0) && (g_ascii_strncasecmp(tmp,"https://",8) != 0)) {
      url = g_strdup_printf("%shttp%%3A%%2F%%2F%s",purple_prefs_get_string("/plugins/gnt/tinyurl/url"),purple_url_encode(tmp));
    }
    else {
      url = g_strdup_printf("%s%s",purple_prefs_get_string("/plugins/gnt/tinyurl/url"),purple_url_encode(tmp));
    }
    g_free(tmp);
    purple_util_fetch_url_request(url,1,"finch",0,0,0,url_fetched,cbdata);;
    i = gnt_text_view_get_lines_below(tv);
    str = g_strdup_printf(((const char *)(dgettext("pidgin","\nFetching TinyURL..."))));
    gnt_text_view_append_text_with_tag(tv,str,GNT_TEXT_FLAG_DIM,(cbdata -> tag));
    g_free(str);
    if (i == 0) 
      gnt_text_view_scroll(tv,0);
    g_free((iter -> data));
    g_free(url);
  }
  g_list_free(urls);
}

static void free_conv_urls(PurpleConversation *conv)
{
  GList *urls = (purple_conversation_get_data(conv,"TinyURLs"));
  if (urls != 0) 
    g_list_foreach(urls,free_urls,0);
  g_list_free(urls);
}

static void tinyurl_notify_fetch_cb(PurpleUtilFetchUrlData *urldata,gpointer cbdata,const gchar *urltext,gsize len,const gchar *error)
{
  GntWidget *win = cbdata;
  GntWidget *label = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"info-widget"));
  char *message;
  message = g_strdup_printf(((const char *)(dgettext("pidgin","TinyURL for above: %s"))),urltext);
  gnt_label_set_text(((GntLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gnt_label_get_gtype()))),message);
  g_free(message);
  g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),G_SIGNAL_MATCH_FUNC,0,0,0,((GCallback )purple_util_fetch_url_cancel),0);
}

static void *tinyurl_notify_uri(const char *uri)
{
  char *fullurl = (char *)((void *)0);
  GntWidget *win;
  PurpleUtilFetchUrlData *urlcb;
/* XXX: The following expects that finch_notify_message gets called. This
	 * may not always happen, e.g. when another plugin sets its own
	 * notify_message. So tread carefully. */
  win = (purple_notify_message(0,PURPLE_NOTIFY_URI,((const char *)(dgettext("pidgin","URI"))),uri,((const char *)(dgettext("pidgin","Please wait while TinyURL fetches a shorter URL ..."))),0,0));
  if (!((({
    GTypeInstance *__inst = (GTypeInstance *)win;
    GType __t = gnt_window_get_gtype();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = 1;
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) || !(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"info-widget") != 0)) 
    return win;
  if ((g_ascii_strncasecmp(uri,"http://",7) != 0) && (g_ascii_strncasecmp(uri,"https://",8) != 0)) {
    fullurl = g_strdup_printf("%shttp%%3A%%2F%%2F%s",purple_prefs_get_string("/plugins/gnt/tinyurl/url"),purple_url_encode(uri));
  }
  else {
    fullurl = g_strdup_printf("%s%s",purple_prefs_get_string("/plugins/gnt/tinyurl/url"),purple_url_encode(uri));
  }
/* Store the return value of _fetch_url and destroy that when win is
	   destroyed, so that the callback for _fetch_url does not try to molest a
	   non-existent window */
  urlcb = purple_util_fetch_url_request(fullurl,1,"finch",0,0,0,tinyurl_notify_fetch_cb,win);;
  g_free(fullurl);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"destroy",((GCallback )purple_util_fetch_url_cancel),urlcb,0,G_CONNECT_SWAPPED);
  return win;
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  PurpleNotifyUiOps *ops = purple_notify_get_ui_ops();
  plugin -> extra = (ops -> notify_uri);
  ops -> notify_uri = tinyurl_notify_uri;
  purple_signal_connect(purple_conversations_get_handle(),"wrote-im-msg",plugin,((PurpleCallback )wrote_msg),0);
  purple_signal_connect(purple_conversations_get_handle(),"wrote-chat-msg",plugin,((PurpleCallback )wrote_msg),0);
  purple_signal_connect(purple_conversations_get_handle(),"writing-im-msg",plugin,((PurpleCallback )writing_msg),0);
  purple_signal_connect(purple_conversations_get_handle(),"writing-chat-msg",plugin,((PurpleCallback )writing_msg),0);
  purple_signal_connect(purple_conversations_get_handle(),"deleting-conversation",plugin,((PurpleCallback )free_conv_urls),0);
  return 1;
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  PurpleNotifyUiOps *ops = purple_notify_get_ui_ops();
  if ((ops -> notify_uri) == tinyurl_notify_uri) 
    ops -> notify_uri = (plugin -> extra);
  return 1;
}

static PurplePluginPrefFrame *get_plugin_pref_frame(PurplePlugin *plugin)
{
  PurplePluginPrefFrame *frame;
  PurplePluginPref *pref;
  frame = purple_plugin_pref_frame_new();
  pref = purple_plugin_pref_new_with_name("/plugins/gnt/tinyurl/length");
  purple_plugin_pref_set_label(pref,((const char *)(dgettext("pidgin","Only create TinyURL for URLs of this length or greater"))));
  purple_plugin_pref_frame_add(frame,pref);
  pref = purple_plugin_pref_new_with_name("/plugins/gnt/tinyurl/url");
  purple_plugin_pref_set_label(pref,((const char *)(dgettext("pidgin","TinyURL (or other) address prefix"))));
  purple_plugin_pref_frame_add(frame,pref);
  return frame;
}
static PurplePluginUiInfo prefs_info = {(get_plugin_pref_frame), (0), ((PurplePluginPrefFrame *)((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* page_num (Reserved) */
/* frame (Reserved) */
/* padding */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gnt-purple"), (0), ((GList *)((void *)0)), (0), ("TinyURL"), ("TinyURL"), ("2.10.9"), ("TinyURL plugin"), ("When receiving a message with URL(s), use TinyURL for easier copying"), ("Richard Nelson <wabz@whatsbeef.net>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), (&prefs_info), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< prefs_info */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
  purple_prefs_add_none("/plugins/gnt/tinyurl");
  purple_prefs_add_int("/plugins/gnt/tinyurl/length",30);
  purple_prefs_add_string("/plugins/gnt/tinyurl/url","http://tinyurl.com/api-create.php\?url=");
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
