/**
 * @file gnthistory.c Show log from previous conversation
 *
 * Copyright (C) 2006 Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
/* Ripped from gtk/plugins/history.c */
#include "internal.h"
#include "conversation.h"
#include "debug.h"
#include "log.h"
#include "request.h"
#include "prefs.h"
#include "signals.h"
#include "util.h"
#include "version.h"
#include "gntconv.h"
#include "gntplugin.h"
#include "gntrequest.h"
#define HISTORY_PLUGIN_ID "gnt-history"
#define HISTORY_SIZE (4 * 1024)

static void historize(PurpleConversation *c)
{
  PurpleAccount *account = purple_conversation_get_account(c);
  const char *name = purple_conversation_get_name(c);
  PurpleConversationType convtype;
  GList *logs = (GList *)((void *)0);
  const char *alias = name;
  PurpleLogReadFlags flags;
  char *history;
  char *header;
  PurpleMessageFlags mflag;
  convtype = purple_conversation_get_type(c);
  if (convtype == PURPLE_CONV_TYPE_IM) {
    GSList *buddies;
    GSList *cur;
    FinchConv *fc = (FinchConv *)(c -> ui_data);
/* We were already in the middle of a conversation. */
    if (((fc -> list) != 0) && (( *(fc -> list)).next != 0)) 
      return ;
/* If we're not logging, don't show anything.
		 * Otherwise, we might show a very old log. */
    if (!(purple_prefs_get_bool("/purple/logging/log_ims") != 0)) 
      return ;
/* Find buddies for this conversation. */
    buddies = purple_find_buddies(account,name);
/* If we found at least one buddy, save the first buddy's alias. */
    if (buddies != ((GSList *)((void *)0))) 
      alias = purple_buddy_get_contact_alias(((PurpleBuddy *)(buddies -> data)));
{
      for (cur = buddies; cur != ((GSList *)((void *)0)); cur = (cur -> next)) {
        PurpleBlistNode *node = (cur -> data);
        if ((node != ((PurpleBlistNode *)((void *)0))) && ((purple_blist_node_get_sibling_prev(node) != ((PurpleBlistNode *)((void *)0))) || (purple_blist_node_get_sibling_next(node) != ((PurpleBlistNode *)((void *)0))))) {
          PurpleBlistNode *node2;
          alias = purple_buddy_get_contact_alias(((PurpleBuddy *)node));
/* We've found a buddy that matches this conversation.  It's part of a
				 * PurpleContact with more than one PurpleBuddy.  Loop through the PurpleBuddies
				 * in the contact and get all the logs. */
          for (node2 = purple_blist_node_get_first_child(purple_blist_node_get_parent(node)); node2 != ((PurpleBlistNode *)((void *)0)); node2 = purple_blist_node_get_sibling_next(node2)) {
            logs = g_list_concat(purple_log_get_logs(PURPLE_LOG_IM,purple_buddy_get_name(((PurpleBuddy *)node2)),purple_buddy_get_account(((PurpleBuddy *)node2))),logs);
          }
          break; 
        }
      }
    }
    g_slist_free(buddies);
    if (logs == ((GList *)((void *)0))) 
      logs = purple_log_get_logs(PURPLE_LOG_IM,name,account);
    else 
      logs = g_list_sort(logs,purple_log_compare);
  }
  else if (convtype == PURPLE_CONV_TYPE_CHAT) {
/* If we're not logging, don't show anything.
		 * Otherwise, we might show a very old log. */
    if (!(purple_prefs_get_bool("/purple/logging/log_chats") != 0)) 
      return ;
    logs = purple_log_get_logs(PURPLE_LOG_CHAT,name,account);
  }
  if (logs == ((GList *)((void *)0))) 
    return ;
  mflag = (PURPLE_MESSAGE_NO_LOG | PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_DELAYED);
  history = purple_log_read(((PurpleLog *)(logs -> data)),&flags);
  header = g_strdup_printf(((const char *)(dgettext("pidgin","<b>Conversation with %s on %s:</b><br>"))),alias,purple_date_format_full((localtime((&( *((PurpleLog *)(logs -> data))).time)))));
  purple_conversation_write(c,"",header,mflag,time(0));
  g_free(header);
  if ((flags & PURPLE_LOG_READ_NO_NEWLINE) != 0U) 
    purple_str_strip_char(history,10);
  purple_conversation_write(c,"",history,mflag,time(0));
  g_free(history);
  purple_conversation_write(c,"","<hr>",mflag,time(0));
  g_list_foreach(logs,((GFunc )purple_log_free),0);
  g_list_free(logs);
}

static void history_prefs_check(PurplePlugin *plugin)
{
  if (!(purple_prefs_get_bool("/purple/logging/log_ims") != 0) && !(purple_prefs_get_bool("/purple/logging/log_chats") != 0)) {
    PurpleRequestFields *fields = purple_request_fields_new();
    PurpleRequestFieldGroup *group;
    PurpleRequestField *field;
    struct __unnamed_class___F0_L143_C3_L432R_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_L432R__scope__pref__DELIMITER__L432R_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_L432R__scope__label {
    const char *pref;
    const char *label;}prefs[] = {{("/purple/logging/log_ims"), ("Log IMs")}, {("/purple/logging/log_chats"), ("Log chats")}, {((const char *)((void *)0)), ((const char *)((void *)0))}};
    int iter;
    GList *list = purple_log_logger_get_options();
    const char *system = purple_prefs_get_string("/purple/logging/format");
    group = purple_request_field_group_new(((const char *)(dgettext("pidgin","Logging"))));
    field = purple_request_field_list_new("/purple/logging/format",((const char *)(dgettext("pidgin","Log format"))));
    while(list != 0){
      const char *label = (const char *)(dgettext("pidgin",(list -> data)));
      list = g_list_delete_link(list,list);
      purple_request_field_list_add_icon(field,label,0,(list -> data));
      if ((system != 0) && (strcmp(system,(list -> data)) == 0)) 
        purple_request_field_list_add_selected(field,label);
      list = g_list_delete_link(list,list);
    }
    purple_request_field_group_add_field(group,field);
    for (iter = 0; prefs[iter].pref != 0; iter++) {
      field = purple_request_field_bool_new(prefs[iter].pref,((const char *)(dgettext("pidgin",prefs[iter].label))),purple_prefs_get_bool(prefs[iter].pref));
      purple_request_field_group_add_field(group,field);
    }
    purple_request_fields_add_group(fields,group);
    purple_request_fields(plugin,0,((const char *)(dgettext("pidgin","History Plugin Requires Logging"))),((const char *)(dgettext("pidgin","Logging can be enabled from Tools -> Preferences -> Logging.\n\nEnabling logs for instant messages and/or chats will activate history for the same conversation type(s)."))),fields,((const char *)(dgettext("pidgin","OK"))),((GCallback )finch_request_save_in_prefs),((const char *)(dgettext("pidgin","Cancel"))),0,0,0,0,plugin);
  }
}

static void history_prefs_cb(const char *name,PurplePrefType type,gconstpointer val,gpointer data)
{
  history_prefs_check(((PurplePlugin *)data));
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  purple_signal_connect(purple_conversations_get_handle(),"conversation-created",plugin,((PurpleCallback )historize),0);
  purple_prefs_connect_callback(plugin,"/purple/logging/log_ims",history_prefs_cb,plugin);
  purple_prefs_connect_callback(plugin,"/purple/logging/log_chats",history_prefs_cb,plugin);
  history_prefs_check(plugin);
  return 1;
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("gnt-history"), ("GntHistory"), ("2.10.9"), ("Shows recently logged conversations in new conversations."), ("When a new conversation is opened this plugin will insert the last conversation into the current conversation."), ("Sean Egan <seanegan@gmail.com>\nSadrul H Chowdhury <sadrul@users.sourceforge.net>"), ("http://pidgin.im/"), (plugin_load), ((gboolean (*)(PurplePlugin *))((void *)0)), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
