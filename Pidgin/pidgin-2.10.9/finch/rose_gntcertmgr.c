/**
 * @file gntcertmgr.c GNT Certificate Manager API
 * @ingroup finch
 */
/* finch
 *
 * Finch is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include <internal.h>
#include "finch.h"
#include "certificate.h"
#include "debug.h"
#include "notify.h"
#include "request.h"
#include "gntcertmgr.h"
#include "gntbutton.h"
#include "gntlabel.h"
#include "gnttree.h"
#include "gntutils.h"
#include "gntwindow.h"
struct __unnamed_class___F0_L44_C1_unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__tree__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L351R__Pe___variable_name_unknown_scope_and_name__scope__pool {
GntWidget *window;
GntWidget *tree;
PurpleCertificatePool *pool;}certmgr;
/* Pretty much Xerox of gtkcertmgr */
/* Add certificate */

static void tls_peers_mgmt_import_ok2_cb(gpointer data,const char *result)
{
  PurpleCertificate *crt = (PurpleCertificate *)data;
  const char *id = result;
/* TODO: Perhaps prompt if you're overwriting a cert? */
  purple_certificate_pool_store(purple_certificate_find_pool("x509","tls_peers"),id,crt);
  purple_certificate_destroy(crt);
}

static void tls_peers_mgmt_import_cancel2_cb(gpointer data,const char *result)
{
  PurpleCertificate *crt = (PurpleCertificate *)data;
  purple_certificate_destroy(crt);
}

static void tls_peers_mgmt_import_ok_cb(gpointer data,const char *filename)
{
  PurpleCertificateScheme *x509;
  PurpleCertificate *crt;
  x509 = purple_certificate_pool_get_scheme(purple_certificate_find_pool("x509","tls_peers"));
  crt = purple_certificate_import(x509,filename);
  if (crt != ((PurpleCertificate *)((void *)0))) {
    gchar *default_hostname;
    default_hostname = purple_certificate_get_subject_name(crt);
    purple_request_input(0,((const char *)(dgettext("pidgin","Certificate Import"))),((const char *)(dgettext("pidgin","Specify a hostname"))),((const char *)(dgettext("pidgin","Type the host name this certificate is for."))),default_hostname,0,0,0,((const char *)(dgettext("pidgin","OK"))),((GCallback )tls_peers_mgmt_import_ok2_cb),((const char *)(dgettext("pidgin","Cancel"))),((GCallback )tls_peers_mgmt_import_cancel2_cb),0,0,0,crt);
    g_free(default_hostname);
  }
  else {
    gchar *secondary;
    secondary = g_strdup_printf(((const char *)(dgettext("pidgin","File %s could not be imported.\nMake sure that the file is readable and in PEM format.\n"))),filename);
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Certificate Import Error"))),((const char *)(dgettext("pidgin","X.509 certificate import failed"))),secondary,0,0);
    g_free(secondary);
  }
}

static void add_cert_cb(GntWidget *button,gpointer null)
{
  purple_request_file(0,((const char *)(dgettext("pidgin","Select a PEM certificate"))),"certificate.pem",0,((GCallback )tls_peers_mgmt_import_ok_cb),0,0,0,0,0);
}
/* Save certs in some file */

static void tls_peers_mgmt_export_ok_cb(gpointer data,const char *filename)
{
  PurpleCertificate *crt = (PurpleCertificate *)data;
  if (!(purple_certificate_export(filename,crt) != 0)) {
    gchar *secondary;
    secondary = g_strdup_printf(((const char *)(dgettext("pidgin","Export to file %s failed.\nCheck that you have write permission to the target path\n"))),filename);
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Certificate Export Error"))),((const char *)(dgettext("pidgin","X.509 certificate export failed"))),secondary,0,0);
    g_free(secondary);
  }
  purple_certificate_destroy(crt);
}

static void save_cert_cb(GntWidget *button,gpointer null)
{
  PurpleCertificate *crt;
  const char *key;
  if (!(certmgr.window != 0)) 
    return ;
  key = (gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)certmgr.tree),gnt_tree_get_gtype())))));
  if (!(key != 0)) 
    return ;
  crt = purple_certificate_pool_retrieve(certmgr.pool,key);
  if (!(crt != 0)) {
    purple_debug_error("gntcertmgr/tls_peers_mgmt","Id %s was not in the peers cache\?!\n",key);
    return ;
  }
  purple_request_file(((void *)key),((const char *)(dgettext("pidgin","PEM X.509 Certificate Export"))),"certificate.pem",1,((GCallback )tls_peers_mgmt_export_ok_cb),((GCallback )purple_certificate_destroy),0,0,0,crt);
}
/* Show information about a cert */

static void info_cert_cb(GntWidget *button,gpointer null)
{
  const char *key;
  PurpleCertificate *crt;
  gchar *subject;
  GByteArray *fpr_sha1;
  gchar *fpr_sha1_asc;
  gchar *primary;
  gchar *secondary;
  if (!(certmgr.window != 0)) 
    return ;
  key = (gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)certmgr.tree),gnt_tree_get_gtype())))));
  if (!(key != 0)) 
    return ;
  crt = purple_certificate_pool_retrieve(certmgr.pool,key);
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return ;
    };
  }while (0);
  primary = g_strdup_printf(((const char *)(dgettext("pidgin","Certificate for %s"))),key);
  fpr_sha1 = purple_certificate_get_fingerprint_sha1(crt);
  fpr_sha1_asc = purple_base16_encode_chunked((fpr_sha1 -> data),(fpr_sha1 -> len));
  subject = purple_certificate_get_subject_name(crt);
  secondary = g_strdup_printf(((const char *)(dgettext("pidgin","Common name: %s\n\nSHA1 fingerprint:\n%s"))),subject,fpr_sha1_asc);
  purple_notify_message(0,PURPLE_NOTIFY_MSG_INFO,((const char *)(dgettext("pidgin","SSL Host Certificate"))),primary,secondary,0,0);
  g_free(primary);
  g_free(secondary);
  g_byte_array_free(fpr_sha1,1);
  g_free(fpr_sha1_asc);
  g_free(subject);
  purple_certificate_destroy(crt);
}
/* Delete a cert */

static void tls_peers_mgmt_delete_confirm_cb(gchar *id,gint dontcare)
{
  if (!(purple_certificate_pool_delete(certmgr.pool,id) != 0)) {
    purple_debug_warning("gntcertmgr/tls_peers_mgmt","Deletion failed on id %s\n",id);
  };
  g_free(id);
}

static void delete_cert_cb(GntWidget *button,gpointer null)
{
  gchar *primary;
  const char *key;
  if (!(certmgr.window != 0)) 
    return ;
  key = (gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)certmgr.tree),gnt_tree_get_gtype())))));
  if (!(key != 0)) 
    return ;
  primary = g_strdup_printf(((const char *)(dgettext("pidgin","Really delete certificate for %s\?"))),key);
  purple_request_close_with_handle(((void *)key));
  purple_request_action(((void *)key),((const char *)(dgettext("pidgin","Confirm certificate delete"))),primary,0,0,0,0,0,(g_strdup(key)),2,((const char *)(dgettext("pidgin","_Yes"))),tls_peers_mgmt_delete_confirm_cb,((const char *)(dgettext("pidgin","_No"))),g_free);
  g_free(primary);
}
/* populate the list */

static void populate_cert_list()
{
  GList *idlist;
  GList *l;
  if (!(certmgr.window != 0)) 
    return ;
  gnt_tree_remove_all(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)certmgr.tree),gnt_tree_get_gtype()))));
  idlist = purple_certificate_pool_get_idlist(purple_certificate_find_pool("x509","tls_peers"));
  for (l = idlist; l != 0; l = (l -> next)) {
    gnt_tree_add_row_last(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)certmgr.tree),gnt_tree_get_gtype()))),(g_strdup((l -> data))),gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)certmgr.tree),gnt_tree_get_gtype()))),(l -> data)),0);
  }
  purple_certificate_pool_destroy_idlist(idlist);
}

static void cert_list_added(PurpleCertificatePool *pool,const char *id,gpointer null)
{
  do {
    if (certmgr.window != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"certmgr.window");
      return ;
    };
  }while (0);
  gnt_tree_add_row_last(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)certmgr.tree),gnt_tree_get_gtype()))),(g_strdup(id)),gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)certmgr.tree),gnt_tree_get_gtype()))),id),0);
}

static void cert_list_removed(PurpleCertificatePool *pool,const char *id,gpointer null)
{
  do {
    if (certmgr.window != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"certmgr.window");
      return ;
    };
  }while (0);
  purple_request_close_with_handle(((void *)id));
  gnt_tree_remove(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)certmgr.tree),gnt_tree_get_gtype()))),((void *)id));
}

void finch_certmgr_show()
{
  GntWidget *win;
  GntWidget *tree;
  GntWidget *box;
  GntWidget *button;
  PurpleCertificatePool *pool;
  if (certmgr.window != 0) {
    gnt_window_present(certmgr.window);
    return ;
  }
  certmgr.window = (win = gnt_window_box_new(0,1));
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),((const char *)(dgettext("pidgin","Certificate Manager"))));
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),0);
  certmgr.tree = (tree = gnt_tree_new());
  gnt_tree_set_hash_fns(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),g_str_hash,g_str_equal,g_free);
  gnt_tree_set_column_title(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),0,((const char *)(dgettext("pidgin","Hostname"))));
  gnt_tree_set_show_title(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_tree_get_gtype()))),1);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),tree);
  box = gnt_box_new(0,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)win),gnt_box_get_gtype()))),box);
  button = gnt_button_new(((const char *)(dgettext("pidgin","Add"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )add_cert_cb),0,0,((GConnectFlags )0));
  gnt_util_set_trigger_widget(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype()))),((cur_term -> type.Strings[77] != 0)?cur_term -> type.Strings[77] : ""),button);
  button = gnt_button_new(((const char *)(dgettext("pidgin","Save"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )save_cert_cb),0,0,((GConnectFlags )0));
  button = gnt_button_new(((const char *)(dgettext("pidgin","Info"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )info_cert_cb),0,0,((GConnectFlags )0));
  button = gnt_button_new(((const char *)(dgettext("pidgin","Delete"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )delete_cert_cb),0,0,((GConnectFlags )0));
  gnt_util_set_trigger_widget(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype()))),((cur_term -> type.Strings[59] != 0)?cur_term -> type.Strings[59] : ""),button);
  button = gnt_button_new(((const char *)(dgettext("pidgin","Close"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )gnt_widget_destroy),win,0,G_CONNECT_SWAPPED);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"destroy",((GCallback )g_nullify_pointer),(&certmgr.window),0,G_CONNECT_SWAPPED);
  populate_cert_list();
  pool = (certmgr.pool = purple_certificate_find_pool("x509","tls_peers"));
  purple_signal_connect(pool,"certificate-stored",win,((PurpleCallback )cert_list_added),0);
  purple_signal_connect(pool,"certificate-deleted",win,((PurpleCallback )cert_list_removed),0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"destroy",((GCallback )purple_signals_disconnect_by_handle),0,0,((GConnectFlags )0));
  gnt_widget_show(certmgr.window);
}
