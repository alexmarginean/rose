/**
 * @file gntconn.c GNT Connection API
 * @ingroup finch
 */
/* finch
 *
 * Finch is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <internal.h>
#include "finch.h"
#include "account.h"
#include "core.h"
#include "connection.h"
#include "debug.h"
#include "request.h"
#include "gntaccount.h"
#include "gntconn.h"
#define INITIAL_RECON_DELAY_MIN  8000
#define INITIAL_RECON_DELAY_MAX 60000
#define MAX_RECON_DELAY 600000
typedef struct __unnamed_class___F0_L43_C9_unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__delay__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_guintUi__typedef_declaration_variable_name_unknown_scope_and_name__scope__timeout {
int delay;
guint timeout;}FinchAutoRecon;
/**
 * Contains accounts that are auto-reconnecting.
 * The key is a pointer to the PurpleAccount and the
 * value is a pointer to a FinchAutoRecon.
 */
static GHashTable *hash = (GHashTable *)((void *)0);

static void free_auto_recon(gpointer data)
{
  FinchAutoRecon *info = data;
  if ((info -> timeout) != 0) 
    g_source_remove((info -> timeout));
  g_free(info);
}

static gboolean do_signon(gpointer data)
{
  PurpleAccount *account = data;
  FinchAutoRecon *info;
  PurpleStatus *status;
  purple_debug_info("autorecon","do_signon called\n");
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  info = (g_hash_table_lookup(hash,account));
  if (info != 0) 
    info -> timeout = 0;
  status = purple_account_get_active_status(account);
  if (purple_status_is_online(status) != 0) {
    purple_debug_info("autorecon","calling purple_account_connect\n");
    purple_account_connect(account);
    purple_debug_info("autorecon","done calling purple_account_connect\n");
  }
  return 0;
}

static void ce_modify_account_cb(PurpleAccount *account)
{
  finch_account_dialog_show(account);
}

static void ce_enable_account_cb(PurpleAccount *account)
{
  purple_account_set_enabled(account,"gnt-purple",(!0));
}

static void finch_connection_report_disconnect(PurpleConnection *gc,PurpleConnectionError reason,const char *text)
{
  FinchAutoRecon *info;
  PurpleAccount *account = purple_connection_get_account(gc);
  if (!(purple_connection_error_is_fatal(reason) != 0)) {
    info = (g_hash_table_lookup(hash,account));
    if (info == ((FinchAutoRecon *)((void *)0))) {
      info = ((FinchAutoRecon *)(g_malloc0_n(1,(sizeof(FinchAutoRecon )))));
      g_hash_table_insert(hash,account,info);
      info -> delay = g_random_int_range(8000,60000);
    }
    else {
      info -> delay = (((2 * (info -> delay)) < 600000)?(2 * (info -> delay)) : 600000);
      if ((info -> timeout) != 0) 
        g_source_remove((info -> timeout));
    }
    info -> timeout = g_timeout_add((info -> delay),do_signon,account);
  }
  else {
    char *act;
    char *primary;
    char *secondary;
    act = g_strdup_printf(((const char *)(dgettext("pidgin","%s (%s)"))),purple_account_get_username(account),purple_account_get_protocol_name(account));
    primary = g_strdup_printf(((const char *)(dgettext("pidgin","%s disconnected."))),act);
    secondary = g_strdup_printf(((const char *)(dgettext("pidgin","%s\n\nFinch will not attempt to reconnect the account until you correct the error and re-enable the account."))),text);
    purple_request_action(account,0,primary,secondary,2,account,0,0,account,3,((const char *)(dgettext("pidgin","OK"))),((void *)((void *)0)),((const char *)(dgettext("pidgin","Modify Account"))),((PurpleCallback )ce_modify_account_cb),((const char *)(dgettext("pidgin","Re-enable Account"))),((PurpleCallback )ce_enable_account_cb));
    g_free(act);
    g_free(primary);
    g_free(secondary);
    purple_account_set_enabled(account,"gnt-purple",0);
  }
}

static void account_removed_cb(PurpleAccount *account,gpointer user_data)
{
  g_hash_table_remove(hash,account);
}

static void *finch_connection_get_handle()
{
  static int handle;
  return (&handle);
}
static PurpleConnectionUiOps ops = {((void (*)(PurpleConnection *, const char *, size_t , size_t ))((void *)0)), ((void (*)(PurpleConnection *))((void *)0)), ((void (*)(PurpleConnection *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), (finch_connection_report_disconnect), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* connect_progress */
/* connected */
/* disconnected */
/* notice */
/* network_connected */
/* network_disconnected */
};

PurpleConnectionUiOps *finch_connections_get_ui_ops()
{
  return &ops;
}

void finch_connections_init()
{
  hash = g_hash_table_new_full(g_direct_hash,g_direct_equal,0,free_auto_recon);
  purple_signal_connect(purple_accounts_get_handle(),"account-removed",finch_connection_get_handle(),((PurpleCallback )account_removed_cb),0);
}

void finch_connections_uninit()
{
  purple_signals_disconnect_by_handle(finch_connection_get_handle());
  g_hash_table_destroy(hash);
}
