/**
 * @file gntaccount.c GNT Account API
 * @ingroup finch
 */
/* finch
 *
 * Finch is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <internal.h>
#include <gnt.h>
#include <gntbox.h>
#include <gntbutton.h>
#include <gntcheckbox.h>
#include <gntcombobox.h>
#include <gntentry.h>
#include <gntlabel.h>
#include <gntline.h>
#include <gnttree.h>
#include <gntutils.h>
#include <gntwindow.h>
#include "finch.h"
#include <account.h>
#include <accountopt.h>
#include <connection.h>
#include <notify.h>
#include <plugin.h>
#include <request.h>
#include <savedstatuses.h>
#include "gntaccount.h"
#include "gntblist.h"
#include <string.h>
typedef struct __unnamed_class___F0_L55_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__tree {
GntWidget *window;
GntWidget *tree;}FinchAccountList;
static FinchAccountList accounts;
typedef struct __unnamed_class___F0_L63_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L247R__Pe___variable_name_unknown_scope_and_name__scope__account__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__protocol__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__username__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__password__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__alias__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__splits__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GList_GList__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__split_entries__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GList_GList__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__prpl_entries__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__prpls__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__newmail__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__remember__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__regserver {
/* NULL for a new account */
PurpleAccount *account;
GntWidget *window;
GntWidget *protocol;
GntWidget *username;
GntWidget *password;
GntWidget *alias;
GntWidget *splits;
GList *split_entries;
GList *prpl_entries;
GntWidget *prpls;
GntWidget *newmail;
GntWidget *remember;
GntWidget *regserver;}AccountEditDialog;
/* This is necessary to close an edit-dialog when an account is deleted */
static GList *accountdialogs;

static void account_add(PurpleAccount *account)
{
  gnt_tree_add_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)accounts.tree),gnt_tree_get_gtype()))),account,gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)accounts.tree),gnt_tree_get_gtype()))),purple_account_get_username(account),purple_account_get_protocol_name(account)),0,0);
  gnt_tree_set_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)accounts.tree),gnt_tree_get_gtype()))),account,purple_account_get_enabled(account,"gnt-purple"));
}

static void edit_dialog_destroy(AccountEditDialog *dialog)
{
  accountdialogs = g_list_remove(accountdialogs,dialog);
  g_list_free((dialog -> prpl_entries));
  g_list_free((dialog -> split_entries));
  g_free(dialog);
}

static void save_account_cb(AccountEditDialog *dialog)
{
  PurpleAccount *account;
  PurplePlugin *plugin;
  PurplePluginProtocolInfo *prplinfo;
  const char *value;
  GString *username;
/* XXX: Do some error checking first. */
  plugin = (gnt_combo_box_get_selected_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> protocol)),gnt_combo_box_get_gtype())))));
  prplinfo = ((PurplePluginProtocolInfo *)( *(plugin -> info)).extra_info);
/* Username && user-splits */
  value = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> username)),gnt_entry_get_gtype()))));
  if ((value == ((const char *)((void *)0))) || (( *value) == 0)) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Error"))),(((dialog -> account) != 0)?((const char *)(dgettext("pidgin","Account was not modified"))) : ((const char *)(dgettext("pidgin","Account was not added")))),((const char *)(dgettext("pidgin","Username of an account must be non-empty."))),0,0);
    return ;
  }
  username = g_string_new(value);
  if (prplinfo != ((PurplePluginProtocolInfo *)((void *)0))) {
    GList *iter;
    GList *entries;
    for (((iter = (prplinfo -> user_splits)) , (entries = (dialog -> split_entries))); (iter != 0) && (entries != 0); ((iter = (iter -> next)) , (entries = (entries -> next)))) {
      PurpleAccountUserSplit *split = (iter -> data);
      GntWidget *entry = (entries -> data);
      value = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_entry_get_gtype()))));
      if ((value == ((const char *)((void *)0))) || (( *value) == 0)) 
        value = purple_account_user_split_get_default_value(split);
      g_string_append_printf(username,"%c%s",(purple_account_user_split_get_separator(split)),value);
    }
  }
  if ((dialog -> account) == ((PurpleAccount *)((void *)0))) {
    account = purple_account_new((username -> str),purple_plugin_get_id(plugin));
    purple_accounts_add(account);
  }
  else {
    account = (dialog -> account);
/* Protocol */
    if (purple_account_is_disconnected(account) != 0) {
      purple_account_set_protocol_id(account,purple_plugin_get_id(plugin));
      purple_account_set_username(account,(username -> str));
    }
    else {
      const char *old = purple_account_get_protocol_id(account);
      char *oldprpl;
      if (strcmp(old,purple_plugin_get_id(plugin)) != 0) {
        purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Error"))),((const char *)(dgettext("pidgin","Account was not modified"))),((const char *)(dgettext("pidgin","The account\'s protocol cannot be changed while it is connected to the server."))),0,0);
        return ;
      }
      oldprpl = g_strdup(purple_normalize(account,purple_account_get_username(account)));
      if (g_utf8_collate(oldprpl,purple_normalize(account,(username -> str))) != 0) {
        purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Error"))),((const char *)(dgettext("pidgin","Account was not modified"))),((const char *)(dgettext("pidgin","The account\'s username cannot be changed while it is connected to the server."))),0,0);
        g_free(oldprpl);
        return ;
      }
      g_free(oldprpl);
      purple_account_set_username(account,(username -> str));
    }
  }
  g_string_free(username,1);
/* Alias */
  value = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> alias)),gnt_entry_get_gtype()))));
  purple_account_set_alias(account,value);
/* Remember password and password */
  purple_account_set_remember_password(account,gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> remember)),gnt_check_box_get_gtype())))));
  value = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> password)),gnt_entry_get_gtype()))));
  if ((value != 0) && (( *value) != 0)) 
    purple_account_set_password(account,value);
  else 
    purple_account_set_password(account,0);
/* Mail notification */
  purple_account_set_check_mail(account,gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> newmail)),gnt_check_box_get_gtype())))));
/* Protocol options */
  if (prplinfo != 0) {
    GList *iter;
    GList *entries;
    for (((iter = (prplinfo -> protocol_options)) , (entries = (dialog -> prpl_entries))); (iter != 0) && (entries != 0); ((iter = (iter -> next)) , (entries = (entries -> next)))) {
      PurpleAccountOption *option = (iter -> data);
      GntWidget *entry = (entries -> data);
      PurplePrefType type = purple_account_option_get_type(option);
      const char *setting = purple_account_option_get_setting(option);
      if (type == PURPLE_PREF_STRING) {
        const char *value = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_entry_get_gtype()))));
        purple_account_set_string(account,setting,value);
      }
      else if (type == PURPLE_PREF_INT) {
        const char *str = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_entry_get_gtype()))));
        int value = 0;
        if (str != 0) 
          value = atoi(str);
        purple_account_set_int(account,setting,value);
      }
      else if (type == PURPLE_PREF_BOOLEAN) {
        gboolean value = gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_check_box_get_gtype()))));
        purple_account_set_bool(account,setting,value);
      }
      else if (type == PURPLE_PREF_STRING_LIST) {
        gchar *value = (gnt_combo_box_get_selected_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_combo_box_get_gtype())))));
        purple_account_set_string(account,setting,value);
      }
      else {
        do {
          g_assertion_message_expr(0,"gntaccount.c",244,((const char *)__func__),0);
        }while (0);
      }
    }
  }
/* XXX: Proxy options */
  if ((accounts.window != 0) && (accounts.tree != 0)) {
    gnt_tree_set_selected(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)accounts.tree),gnt_tree_get_gtype()))),account);
    gnt_box_give_focus_to_child(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)accounts.window),gnt_box_get_gtype()))),accounts.tree);
  }
  if (((prplinfo != 0) && ((prplinfo -> register_user) != 0)) && (gnt_check_box_get_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> regserver)),gnt_check_box_get_gtype())))) != 0)) {
    purple_account_register(account);
  }
  else if ((dialog -> account) == ((PurpleAccount *)((void *)0))) {
/* This is a new account. Set it to the current status. */
/* Xerox from gtkaccount.c :D */
    const PurpleSavedStatus *saved_status;
    saved_status = (purple_savedstatus_get_current());
    if (saved_status != ((const PurpleSavedStatus *)((void *)0))) {
      purple_savedstatus_activate_for_account(saved_status,account);
      purple_account_set_enabled(account,"gnt-purple",1);
    }
  }
/* In case of a new account, the 'Accounts' window is updated from the account-added
	 * callback. In case of changes in an existing account, we need to explicitly do it
	 * here.
	 */
  if (((dialog -> account) != ((PurpleAccount *)((void *)0))) && (accounts.window != 0)) {
    gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)accounts.tree),gnt_tree_get_gtype()))),(dialog -> account),0,purple_account_get_username((dialog -> account)));
    gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)accounts.tree),gnt_tree_get_gtype()))),(dialog -> account),1,purple_account_get_protocol_name((dialog -> account)));
  }
  gnt_widget_destroy((dialog -> window));
}

static void update_user_splits(AccountEditDialog *dialog)
{
  GntWidget *hbox;
  PurplePlugin *plugin;
  PurplePluginProtocolInfo *prplinfo;
  GList *iter;
  GList *entries;
  char *username = (char *)((void *)0);
  if ((dialog -> splits) != 0) {
    gnt_box_remove_all(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> splits)),gnt_box_get_gtype()))));
    g_list_free((dialog -> split_entries));
  }
  else {
    dialog -> splits = gnt_box_new(0,1);
    gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> splits)),gnt_box_get_gtype()))),0);
    gnt_box_set_fill(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> splits)),gnt_box_get_gtype()))),1);
  }
  dialog -> split_entries = ((GList *)((void *)0));
  plugin = (gnt_combo_box_get_selected_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> protocol)),gnt_combo_box_get_gtype())))));
  if (!(plugin != 0)) 
    return ;
  prplinfo = ((PurplePluginProtocolInfo *)( *(plugin -> info)).extra_info);
  username = (((dialog -> account) != 0)?g_strdup(purple_account_get_username((dialog -> account))) : ((char *)((void *)0)));
  for (iter = (prplinfo -> user_splits); iter != 0; iter = (iter -> next)) {
    PurpleAccountUserSplit *split = (iter -> data);
    GntWidget *entry;
    char *buf;
    hbox = gnt_box_new(1,0);
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> splits)),gnt_box_get_gtype()))),hbox);
    buf = g_strdup_printf("%s:",purple_account_user_split_get_text(split));
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),gnt_label_new(buf));
    entry = gnt_entry_new(0);
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),entry);
    dialog -> split_entries = g_list_append((dialog -> split_entries),entry);
    g_free(buf);
  }
  for (((iter = g_list_last((prplinfo -> user_splits))) , (entries = g_list_last((dialog -> split_entries)))); (iter != 0) && (entries != 0); ((iter = (iter -> prev)) , (entries = (entries -> prev)))) {
    GntWidget *entry = (entries -> data);
    PurpleAccountUserSplit *split = (iter -> data);
    const char *value = (const char *)((void *)0);
    char *s;
    if ((dialog -> account) != 0) {
      if (purple_account_user_split_get_reverse(split) != 0) 
        s = strrchr(username,(purple_account_user_split_get_separator(split)));
      else 
        s = strchr(username,(purple_account_user_split_get_separator(split)));
      if (s != ((char *)((void *)0))) {
         *s = 0;
        s++;
        value = s;
      }
    }
    if (value == ((const char *)((void *)0))) 
      value = purple_account_user_split_get_default_value(split);
    if (value != ((const char *)((void *)0))) 
      gnt_entry_set_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_entry_get_gtype()))),value);
  }
  if (username != ((char *)((void *)0))) 
    gnt_entry_set_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> username)),gnt_entry_get_gtype()))),username);
  g_free(username);
}

static void add_protocol_options(AccountEditDialog *dialog)
{
  PurplePlugin *plugin;
  PurplePluginProtocolInfo *prplinfo;
  GList *iter;
  GntWidget *vbox;
  GntWidget *box;
  PurpleAccount *account;
  if ((dialog -> prpls) != 0) 
    gnt_box_remove_all(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> prpls)),gnt_box_get_gtype()))));
  else {
    dialog -> prpls = (vbox = gnt_box_new(0,1));
    gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),0);
    gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),GNT_ALIGN_LEFT);
    gnt_box_set_fill(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),1);
  }
  if ((dialog -> prpl_entries) != 0) {
    g_list_free((dialog -> prpl_entries));
    dialog -> prpl_entries = ((GList *)((void *)0));
  }
  vbox = (dialog -> prpls);
  plugin = (gnt_combo_box_get_selected_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> protocol)),gnt_combo_box_get_gtype())))));
  if (!(plugin != 0)) 
    return ;
  prplinfo = ((PurplePluginProtocolInfo *)( *(plugin -> info)).extra_info);
  account = (dialog -> account);
  for (iter = (prplinfo -> protocol_options); iter != 0; iter = (iter -> next)) {
    PurpleAccountOption *option = (iter -> data);
    PurplePrefType type = purple_account_option_get_type(option);
    box = gnt_box_new(1,0);
    gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),0);
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gnt_box_get_gtype()))),box);
    if (type == PURPLE_PREF_BOOLEAN) {
      GntWidget *widget = gnt_check_box_new(purple_account_option_get_text(option));
      gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),widget);
      dialog -> prpl_entries = g_list_append((dialog -> prpl_entries),widget);
      if (account != 0) 
        gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_check_box_get_gtype()))),purple_account_get_bool(account,purple_account_option_get_setting(option),purple_account_option_get_default_bool(option)));
      else 
        gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_check_box_get_gtype()))),purple_account_option_get_default_bool(option));
    }
    else {
      gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),gnt_label_new(purple_account_option_get_text(option)));
      if (type == PURPLE_PREF_STRING_LIST) {
        GntWidget *combo = gnt_combo_box_new();
        GList *opt_iter = purple_account_option_get_list(option);
        const char *dv = purple_account_option_get_default_list_value(option);
        const char *active = dv;
        if (account != 0) 
          active = purple_account_get_string(account,purple_account_option_get_setting(option),dv);
        gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),combo);
        dialog -> prpl_entries = g_list_append((dialog -> prpl_entries),combo);
        for (; opt_iter != 0; opt_iter = (opt_iter -> next)) {
          PurpleKeyValuePair *kvp = (opt_iter -> data);
          gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype()))),(kvp -> value),(kvp -> key));
          if (g_str_equal((kvp -> value),active) != 0) 
            gnt_combo_box_set_selected(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype()))),(kvp -> value));
        }
      }
      else {
        GntWidget *entry = gnt_entry_new(0);
        gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),entry);
        dialog -> prpl_entries = g_list_append((dialog -> prpl_entries),entry);
        if (type == PURPLE_PREF_STRING) {
          const char *dv = purple_account_option_get_default_string(option);
          if (account != 0) 
            gnt_entry_set_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_entry_get_gtype()))),purple_account_get_string(account,purple_account_option_get_setting(option),dv));
          else 
            gnt_entry_set_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_entry_get_gtype()))),dv);
        }
        else if (type == PURPLE_PREF_INT) {
          char str[32UL];
          int value = purple_account_option_get_default_int(option);
          if (account != 0) 
            value = purple_account_get_int(account,purple_account_option_get_setting(option),value);
          snprintf(str,(sizeof(str)),"%d",value);
          gnt_entry_set_flag(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_entry_get_gtype()))),GNT_ENTRY_FLAG_INT);
          gnt_entry_set_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_entry_get_gtype()))),str);
        }
        else {
          do {
            g_assertion_message_expr(0,"gntaccount.c",485,((const char *)__func__),0);
          }while (0);
        }
      }
    }
  }
/* Show the registration checkbox only in a new account dialog,
	 * and when the selected prpl has the support for it. */
  gnt_widget_set_visible((dialog -> regserver),((account == ((PurpleAccount *)((void *)0))) && ((prplinfo -> register_user) != ((void (*)(PurpleAccount *))((void *)0)))));
}

static void update_user_options(AccountEditDialog *dialog)
{
  PurplePlugin *plugin;
  PurplePluginProtocolInfo *prplinfo;
  plugin = (gnt_combo_box_get_selected_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> protocol)),gnt_combo_box_get_gtype())))));
  if (!(plugin != 0)) 
    return ;
  prplinfo = ((PurplePluginProtocolInfo *)( *(plugin -> info)).extra_info);
  if ((dialog -> newmail) == ((GntWidget *)((void *)0))) 
    dialog -> newmail = gnt_check_box_new(((const char *)(dgettext("pidgin","New mail notifications"))));
  if ((dialog -> account) != 0) 
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> newmail)),gnt_check_box_get_gtype()))),purple_account_get_check_mail((dialog -> account)));
  if (!(prplinfo != 0) || !(((prplinfo -> options) & OPT_PROTO_MAIL_CHECK) != 0U)) 
    gnt_widget_set_visible((dialog -> newmail),0);
  else 
    gnt_widget_set_visible((dialog -> newmail),1);
  if ((dialog -> remember) == ((GntWidget *)((void *)0))) 
    dialog -> remember = gnt_check_box_new(((const char *)(dgettext("pidgin","Remember password"))));
  if ((dialog -> account) != 0) 
    gnt_check_box_set_checked(((GntCheckBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> remember)),gnt_check_box_get_gtype()))),purple_account_get_remember_password((dialog -> account)));
}

static void prpl_changed_cb(GntWidget *combo,PurplePlugin *old,PurplePlugin *new,AccountEditDialog *dialog)
{
  update_user_splits(dialog);
  add_protocol_options(dialog);
/* This may not be necessary here */
  update_user_options(dialog);
  gnt_box_readjust(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> window)),gnt_box_get_gtype()))));
  gnt_widget_draw((dialog -> window));
}

static void edit_account(PurpleAccount *account)
{
  GntWidget *window;
  GntWidget *hbox;
  GntWidget *combo;
  GntWidget *button;
  GntWidget *entry;
  GList *list;
  GList *iter;
  AccountEditDialog *dialog;
  PurplePlugin *plugin;
  if (account != 0) {
    GList *iter;
    for (iter = accountdialogs; iter != 0; iter = (iter -> next)) {
      AccountEditDialog *dlg = (iter -> data);
      if ((dlg -> account) == account) 
        return ;
    }
  }
  list = purple_plugins_get_protocols();
  if (list == ((GList *)((void *)0))) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Error"))),((const char *)(dgettext("pidgin","There are no protocol plugins installed."))),((const char *)(dgettext("pidgin","(You probably forgot to \'make install\'.)"))),0,0);
    return ;
  }
  dialog = ((AccountEditDialog *)(g_malloc0_n(1,(sizeof(AccountEditDialog )))));
  accountdialogs = g_list_prepend(accountdialogs,dialog);
  dialog -> window = (window = gnt_box_new(0,1));
  dialog -> account = account;
  gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),1);
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),((account != 0)?((const char *)(dgettext("pidgin","Modify Account"))) : ((const char *)(dgettext("pidgin","New Account")))));
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),GNT_ALIGN_MID);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),0);
  gnt_widget_set_name(window,"edit-account");
  gnt_box_set_fill(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),1);
  hbox = gnt_box_new(1,0);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),hbox);
  dialog -> protocol = (combo = gnt_combo_box_new());
  for (iter = list; iter != 0; iter = (iter -> next)) {
    gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype()))),(iter -> data),( *( *((PurplePlugin *)(iter -> data))).info).name);
  }
  plugin = purple_plugins_find_with_id(purple_account_get_protocol_id(account));
  if ((account != 0) && (plugin != 0)) 
    gnt_combo_box_set_selected(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype()))),plugin);
  else 
    gnt_combo_box_set_selected(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gnt_combo_box_get_gtype()))),(list -> data));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)combo),((GType )(20 << 2))))),"selection-changed",((GCallback )prpl_changed_cb),dialog,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),gnt_label_new(((const char *)(dgettext("pidgin","Protocol:")))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),combo);
  hbox = gnt_box_new(1,0);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),hbox);
  dialog -> username = (entry = gnt_entry_new(0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),gnt_label_new(((const char *)(dgettext("pidgin","Username:")))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),entry);
/* User splits */
  update_user_splits(dialog);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),(dialog -> splits));
  hbox = gnt_box_new(1,0);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),hbox);
  dialog -> password = (entry = gnt_entry_new(0));
  gnt_entry_set_masked(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_entry_get_gtype()))),1);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),gnt_label_new(((const char *)(dgettext("pidgin","Password:")))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),entry);
  if (account != 0) 
    gnt_entry_set_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_entry_get_gtype()))),purple_account_get_password(account));
  hbox = gnt_box_new(1,0);
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),hbox);
  dialog -> alias = (entry = gnt_entry_new(0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),gnt_label_new(((const char *)(dgettext("pidgin","Alias:")))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),entry);
  if (account != 0) 
    gnt_entry_set_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gnt_entry_get_gtype()))),purple_account_get_alias(account));
/* User options */
  update_user_options(dialog);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),(dialog -> remember));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),(dialog -> newmail));
/* Register checkbox */
  dialog -> regserver = gnt_check_box_new(((const char *)(dgettext("pidgin","Create this account on the server"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),(dialog -> regserver));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),gnt_line_new(0));
/* The advanced box */
  add_protocol_options(dialog);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),(dialog -> prpls));
/* TODO: Add proxy options */
/* The button box */
  hbox = gnt_box_new(0,0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))),hbox);
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),GNT_ALIGN_MID);
  button = gnt_button_new(((const char *)(dgettext("pidgin","Cancel"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )gnt_widget_destroy),window,0,G_CONNECT_SWAPPED);
  button = gnt_button_new(((const char *)(dgettext("pidgin","Save"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gnt_box_get_gtype()))),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )save_account_cb),dialog,0,G_CONNECT_SWAPPED);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"destroy",((GCallback )edit_dialog_destroy),dialog,0,G_CONNECT_SWAPPED);
  gnt_widget_show(window);
  gnt_box_readjust(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)window),gnt_box_get_gtype()))));
  gnt_widget_draw(window);
}

static void add_account_cb(GntWidget *widget,gpointer null)
{
  edit_account(0);
}

static void modify_account_cb(GntWidget *widget,GntTree *tree)
{
  PurpleAccount *account = (gnt_tree_get_selection_data(tree));
  if (!(account != 0)) 
    return ;
  edit_account(account);
}

static void really_delete_account(PurpleAccount *account)
{
  GList *iter;
{
    for (iter = accountdialogs; iter != 0; iter = (iter -> next)) {
      AccountEditDialog *dlg = (iter -> data);
      if ((dlg -> account) == account) {
        gnt_widget_destroy((dlg -> window));
        break; 
      }
    }
  }
/* Close any other opened delete window */
  purple_request_close_with_handle(account);
  purple_accounts_delete(account);
}

static void delete_account_cb(GntWidget *widget,GntTree *tree)
{
  PurpleAccount *account;
  char *prompt;
  account = (gnt_tree_get_selection_data(tree));
  if (!(account != 0)) 
    return ;
  prompt = g_strdup_printf(((const char *)(dgettext("pidgin","Are you sure you want to delete %s\?"))),purple_account_get_username(account));
  purple_request_action(account,((const char *)(dgettext("pidgin","Delete Account"))),prompt,0,-1,account,0,0,account,2,((const char *)(dgettext("pidgin","Delete"))),really_delete_account,((const char *)(dgettext("pidgin","Cancel"))),((void *)((void *)0)));
  g_free(prompt);
}

static void account_toggled(GntWidget *widget,void *key,gpointer null)
{
  PurpleAccount *account = key;
  gboolean enabled = gnt_tree_get_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()))),key);
  if (enabled != 0) 
    purple_savedstatus_activate_for_account((purple_savedstatus_get_current()),account);
  purple_account_set_enabled(account,"gnt-purple",enabled);
}

static gboolean account_list_key_pressed_cb(GntWidget *widget,const char *text,gpointer null)
{
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype()));
  PurpleAccount *account = (gnt_tree_get_selection_data(tree));
  int move;
  int pos;
  int count;
  GList *accounts;
  if (!(account != 0)) 
    return 0;
  switch(text[0]){
    case '-':
{
      move = -1;
      break; 
    }
    case '=':
{
/* XXX: This seems to be a bug in libpurple */
      move = 2;
      break; 
    }
    default:
{
      return 0;
    }
  }
  accounts = purple_accounts_get_all();
  count = (g_list_length(accounts));
  pos = g_list_index(accounts,account);
  pos = ((((move + pos) + count) + 1) % (count + 1));
  purple_accounts_reorder(account,pos);
/* I don't like this, but recreating the entire list seems to be
	 * the easiest way of doing it */
  gnt_tree_remove_all(tree);
  accounts = purple_accounts_get_all();
  for (; accounts != 0; accounts = (accounts -> next)) 
    account_add((accounts -> data));
  gnt_tree_set_selected(tree,account);
  return 1;
}

static void reset_accounts_win(GntWidget *widget,gpointer null)
{
  accounts.window = ((GntWidget *)((void *)0));
  accounts.tree = ((GntWidget *)((void *)0));
}

void finch_accounts_show_all()
{
  GList *iter;
  GntWidget *box;
  GntWidget *button;
  if (accounts.window != 0) {
    gnt_window_present(accounts.window);
    return ;
  }
  accounts.window = gnt_box_new(0,1);
  gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)accounts.window),gnt_box_get_gtype()))),1);
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)accounts.window),gnt_box_get_gtype()))),((const char *)(dgettext("pidgin","Accounts"))));
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)accounts.window),gnt_box_get_gtype()))),0);
  gnt_box_set_alignment(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)accounts.window),gnt_box_get_gtype()))),GNT_ALIGN_MID);
  gnt_widget_set_name(accounts.window,"accounts");
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)accounts.window),gnt_box_get_gtype()))),gnt_label_new(((const char *)(dgettext("pidgin","You can enable/disable accounts from the following list.")))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)accounts.window),gnt_box_get_gtype()))),gnt_line_new(0));
  accounts.tree = gnt_tree_new_with_columns(2);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)accounts.tree),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_NO_BORDER;
  for (iter = purple_accounts_get_all(); iter != 0; iter = (iter -> next)) {
    PurpleAccount *account = (iter -> data);
    account_add(account);
  }
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)accounts.tree),((GType )(20 << 2))))),"toggled",((GCallback )account_toggled),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)accounts.tree),((GType )(20 << 2))))),"key_pressed",((GCallback )account_list_key_pressed_cb),0,0,((GConnectFlags )0));
  gnt_tree_set_col_width(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)accounts.tree),gnt_tree_get_gtype()))),0,40);
  gnt_tree_set_col_width(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)accounts.tree),gnt_tree_get_gtype()))),1,10);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)accounts.window),gnt_box_get_gtype()))),accounts.tree);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)accounts.window),gnt_box_get_gtype()))),gnt_line_new(0));
  box = gnt_box_new(0,0);
  button = gnt_button_new(((const char *)(dgettext("pidgin","Add"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  gnt_util_set_trigger_widget(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)accounts.tree),gnt_widget_get_gtype()))),((cur_term -> type.Strings[77] != 0)?cur_term -> type.Strings[77] : ""),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )add_account_cb),0,0,((GConnectFlags )0));
  button = gnt_button_new(((const char *)(dgettext("pidgin","Modify"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )modify_account_cb),accounts.tree,0,((GConnectFlags )0));
  button = gnt_button_new(((const char *)(dgettext("pidgin","Delete"))));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),button);
  gnt_util_set_trigger_widget(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)accounts.tree),gnt_widget_get_gtype()))),((cur_term -> type.Strings[59] != 0)?cur_term -> type.Strings[59] : ""),button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"activate",((GCallback )delete_account_cb),accounts.tree,0,((GConnectFlags )0));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)accounts.window),gnt_box_get_gtype()))),box);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)accounts.window),((GType )(20 << 2))))),"destroy",((GCallback )reset_accounts_win),0,0,((GConnectFlags )0));
  gnt_widget_show(accounts.window);
}

void finch_account_dialog_show(PurpleAccount *account)
{
  edit_account(account);
}

static gpointer finch_accounts_get_handle()
{
  static int handle;
  return (&handle);
}

static void account_added_callback(PurpleAccount *account)
{
  if (accounts.window == ((GntWidget *)((void *)0))) 
    return ;
  account_add(account);
  gnt_widget_draw(accounts.tree);
}

static void account_removed_callback(PurpleAccount *account)
{
  if (accounts.window == ((GntWidget *)((void *)0))) 
    return ;
  gnt_tree_remove(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)accounts.tree),gnt_tree_get_gtype()))),account);
}

static void account_abled_cb(PurpleAccount *account,gpointer user_data)
{
  if (accounts.window == ((GntWidget *)((void *)0))) 
    return ;
  gnt_tree_set_choice(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)accounts.tree),gnt_tree_get_gtype()))),account,((gint )((glong )user_data)));
}

void finch_accounts_init()
{
  GList *iter;
  purple_signal_connect(purple_accounts_get_handle(),"account-added",finch_accounts_get_handle(),((PurpleCallback )account_added_callback),0);
  purple_signal_connect(purple_accounts_get_handle(),"account-removed",finch_accounts_get_handle(),((PurpleCallback )account_removed_callback),0);
  purple_signal_connect(purple_accounts_get_handle(),"account-disabled",finch_accounts_get_handle(),((PurpleCallback )account_abled_cb),0);
  purple_signal_connect(purple_accounts_get_handle(),"account-enabled",finch_accounts_get_handle(),((PurpleCallback )account_abled_cb),((void *)((gpointer )((glong )1))));
  iter = purple_accounts_get_all();
  if (iter != 0) {{
      for (; iter != 0; iter = (iter -> next)) {
        if (purple_account_get_enabled((iter -> data),"gnt-purple") != 0) 
          break; 
      }
    }
    if (!(iter != 0)) 
      finch_accounts_show_all();
  }
  else {
    edit_account(0);
    finch_accounts_show_all();
  }
}

void finch_accounts_uninit()
{
  if (accounts.window != 0) 
    gnt_widget_destroy(accounts.window);
}
/* The following uiops stuff are copied from gtkaccount.c */
typedef struct __unnamed_class___F0_L922_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L247R__Pe___variable_name_unknown_scope_and_name__scope__account__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__username__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__alias {
PurpleAccount *account;
char *username;
char *alias;}AddUserData;

static char *make_info(PurpleAccount *account,PurpleConnection *gc,const char *remote_user,const char *id,const char *alias,const char *msg)
{
  if ((msg != ((const char *)((void *)0))) && (( *msg) == 0)) 
    msg = ((const char *)((void *)0));
  return g_strdup_printf(((const char *)(dgettext("pidgin","%s%s%s%s has made %s his or her buddy%s%s"))),remote_user,((alias != ((const char *)((void *)0)))?" (" : ""),((alias != ((const char *)((void *)0)))?alias : ""),((alias != ((const char *)((void *)0)))?")" : ""),((id != ((const char *)((void *)0)))?id : (((purple_connection_get_display_name(gc) != ((const char *)((void *)0)))?purple_connection_get_display_name(gc) : purple_account_get_username(account)))),((msg != ((const char *)((void *)0)))?": " : "."),((msg != ((const char *)((void *)0)))?msg : ""));
}

static void notify_added(PurpleAccount *account,const char *remote_user,const char *id,const char *alias,const char *msg)
{
  char *buffer;
  PurpleConnection *gc;
  gc = purple_account_get_connection(account);
  buffer = make_info(account,gc,remote_user,id,alias,msg);
  purple_notify_message(0,PURPLE_NOTIFY_MSG_INFO,0,buffer,0,0,0);
  g_free(buffer);
}

static void free_add_user_data(AddUserData *data)
{
  g_free((data -> username));
  if ((data -> alias) != ((char *)((void *)0))) 
    g_free((data -> alias));
  g_free(data);
}

static void add_user_cb(AddUserData *data)
{
  PurpleConnection *gc = purple_account_get_connection((data -> account));
  if (g_list_find(purple_connections_get_all(),gc) != 0) {
    purple_blist_request_add_buddy((data -> account),(data -> username),0,(data -> alias));
  }
  free_add_user_data(data);
}

static void request_add(PurpleAccount *account,const char *remote_user,const char *id,const char *alias,const char *msg)
{
  char *buffer;
  PurpleConnection *gc;
  AddUserData *data;
  gc = purple_account_get_connection(account);
  data = ((AddUserData *)(g_malloc0_n(1,(sizeof(AddUserData )))));
  data -> account = account;
  data -> username = g_strdup(remote_user);
  data -> alias = ((alias != ((const char *)((void *)0)))?g_strdup(alias) : ((char *)((void *)0)));
  buffer = make_info(account,gc,remote_user,id,alias,msg);
  purple_request_action(0,0,((const char *)(dgettext("pidgin","Add buddy to your list\?"))),buffer,-1,account,remote_user,0,data,2,((const char *)(dgettext("pidgin","Add"))),((GCallback )add_user_cb),((const char *)(dgettext("pidgin","Cancel"))),((GCallback )free_add_user_data));
  g_free(buffer);
}
/* Copied from gtkaccount.c */
typedef struct __unnamed_class___F0_L1019_C9_unknown_scope_and_name_variable_declaration__variable_type_L249R_variable_name_unknown_scope_and_name__scope__auth_cb__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L249R_variable_name_unknown_scope_and_name__scope__deny_cb__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__v__Pe___variable_name_unknown_scope_and_name__scope__data__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__username__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__alias__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L247R__Pe___variable_name_unknown_scope_and_name__scope__account {
PurpleAccountRequestAuthorizationCb auth_cb;
PurpleAccountRequestAuthorizationCb deny_cb;
void *data;
char *username;
char *alias;
PurpleAccount *account;}auth_and_add;

static void free_auth_and_add(auth_and_add *aa)
{
  g_free((aa -> username));
  g_free((aa -> alias));
  g_free(aa);
}

static void authorize_and_add_cb(auth_and_add *aa)
{
  ( *(aa -> auth_cb))((aa -> data));
  purple_blist_request_add_buddy((aa -> account),(aa -> username),0,(aa -> alias));
}

static void deny_no_add_cb(auth_and_add *aa)
{
  ( *(aa -> deny_cb))((aa -> data));
}

static void *finch_request_authorize(PurpleAccount *account,const char *remote_user,const char *id,const char *alias,const char *message,gboolean on_list,PurpleAccountRequestAuthorizationCb auth_cb,PurpleAccountRequestAuthorizationCb deny_cb,void *user_data)
{
  char *buffer;
  PurpleConnection *gc;
  void *uihandle;
  gc = purple_account_get_connection(account);
  if ((message != ((const char *)((void *)0))) && (( *message) == 0)) 
    message = ((const char *)((void *)0));
  buffer = g_strdup_printf(((const char *)(dgettext("pidgin","%s%s%s%s wants to add %s to his or her buddy list%s%s"))),remote_user,((alias != ((const char *)((void *)0)))?" (" : ""),((alias != ((const char *)((void *)0)))?alias : ""),((alias != ((const char *)((void *)0)))?")" : ""),((id != ((const char *)((void *)0)))?id : (((purple_connection_get_display_name(gc) != ((const char *)((void *)0)))?purple_connection_get_display_name(gc) : purple_account_get_username(account)))),((message != ((const char *)((void *)0)))?": " : "."),((message != ((const char *)((void *)0)))?message : ""));
  if (!(on_list != 0)) {
    GntWidget *widget;
    GList *iter;
    auth_and_add *aa = (auth_and_add *)(g_malloc_n(1,(sizeof(auth_and_add ))));
    aa -> auth_cb = auth_cb;
    aa -> deny_cb = deny_cb;
    aa -> data = user_data;
    aa -> username = g_strdup(remote_user);
    aa -> alias = g_strdup(alias);
    aa -> account = account;
    uihandle = (gnt_window_box_new(0,1));
    gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)uihandle),gnt_box_get_gtype()))),((const char *)(dgettext("pidgin","Authorize buddy\?"))));
    gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)uihandle),gnt_box_get_gtype()))),0);
    widget = (purple_request_action(0,((const char *)(dgettext("pidgin","Authorize buddy\?"))),buffer,0,-1,account,remote_user,0,aa,2,((const char *)(dgettext("pidgin","Authorize"))),authorize_and_add_cb,((const char *)(dgettext("pidgin","Deny"))),deny_no_add_cb));
    gnt_screen_release(widget);
    gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype()))),0);
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)uihandle),gnt_box_get_gtype()))),widget);
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)uihandle),gnt_box_get_gtype()))),gnt_line_new(0));
    widget = (finch_retrieve_user_info(purple_account_get_connection(account),remote_user));
{
      for (iter = ( *((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype())))).list; iter != 0; iter = (iter -> next)) {
        if ((({
          GTypeInstance *__inst = (GTypeInstance *)(iter -> data);
          GType __t = gnt_button_get_gtype();
          gboolean __r;
          if (!(__inst != 0)) 
            __r = 0;
          else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
            __r = 1;
          else 
            __r = g_type_check_instance_is_a(__inst,__t);
          __r;
        })) != 0) 
{
          gnt_widget_destroy((iter -> data));
          gnt_box_remove(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype()))),(iter -> data));
          break; 
        }
      }
    }
    gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_box_get_gtype()))),0);
    gnt_screen_release(widget);
    gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)uihandle),gnt_box_get_gtype()))),widget);
    gnt_widget_show(uihandle);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)uihandle),((GType )(20 << 2))))),"destroy",((GCallback )free_auth_and_add),aa,0,G_CONNECT_SWAPPED);
  }
  else {
    uihandle = purple_request_action(0,((const char *)(dgettext("pidgin","Authorize buddy\?"))),buffer,0,-1,account,remote_user,0,user_data,2,((const char *)(dgettext("pidgin","Authorize"))),auth_cb,((const char *)(dgettext("pidgin","Deny"))),deny_cb);
  }
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)uihandle),((GType )(20 << 2))))),"destroy",((GCallback )purple_account_request_close),0,0,((GConnectFlags )0));
  g_free(buffer);
  return uihandle;
}

static void finch_request_close(void *uihandle)
{
  purple_request_close(PURPLE_REQUEST_ACTION,uihandle);
}
static PurpleAccountUiOps ui_ops = {(notify_added), ((void (*)(PurpleAccount *, PurpleStatus *))((void *)0)), (request_add), (finch_request_authorize), (finch_request_close), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))};

PurpleAccountUiOps *finch_accounts_get_ui_ops()
{
  return &ui_ops;
}
