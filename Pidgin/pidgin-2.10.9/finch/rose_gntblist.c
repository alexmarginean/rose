/**
 * @file gntblist.c GNT BuddyList API
 * @ingroup finch
 */
/* finch
 *
 * Finch is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <internal.h>
#include "finch.h"
#include <account.h>
#include <blist.h>
#include <log.h>
#include <notify.h>
#include <privacy.h>
#include <request.h>
#include <savedstatuses.h>
#include <server.h>
#include <signal.h>
#include <status.h>
#include <util.h>
#include "debug.h"
#include "gntbox.h"
#include "gntcolors.h"
#include "gntcombobox.h"
#include "gntentry.h"
#include "gntft.h"
#include "gntlabel.h"
#include "gntline.h"
#include "gntlog.h"
#include "gntmenu.h"
#include "gntmenuitem.h"
#include "gntmenuitemcheck.h"
#include "gntpounce.h"
#include "gntstyle.h"
#include "gnttree.h"
#include "gntutils.h"
#include "gntwindow.h"
#include "gntblist.h"
#include "gntconv.h"
#include "gntstatus.h"
#include <string.h>
#define PREF_ROOT "/finch/blist"
#define TYPING_TIMEOUT_S 4
#define SHOW_EMPTY_GROUP_TIMEOUT  60
typedef struct __unnamed_class___F0_L69_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__tree__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__tooltip__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L284R__Pe___variable_name_unknown_scope_and_name__scope__tnode__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GList_GList__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__tagged__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__context__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L284R__Pe___variable_name_unknown_scope_and_name__scope__cnode__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__status__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__statustext__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__typing__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GntWidget_GntWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__menu__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L412R__Pe___variable_name_unknown_scope_and_name__scope__accounts__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L412R__Pe___variable_name_unknown_scope_and_name__scope__plugins__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L412R__Pe___variable_name_unknown_scope_and_name__scope__grouping__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GList_GList__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__new_group__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_guintUi__typedef_declaration_variable_name_unknown_scope_and_name__scope__new_group_timeout__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L456R__Pe___variable_name_unknown_scope_and_name__scope__manager {
GntWidget *window;
GntWidget *tree;
GntWidget *tooltip;
/* Who is the tooltip being displayed for? */
PurpleBlistNode *tnode;
/* A list of tagged blistnodes */
GList *tagged;
GntWidget *context;
PurpleBlistNode *cnode;
/* XXX: I am KISSing */
/* Dropdown with the statuses  */
GntWidget *status;
/* Status message */
GntWidget *statustext;
int typing;
GntWidget *menu;
/* These are the menuitems that get regenerated */
GntMenuItem *accounts;
GntMenuItem *plugins;
GntMenuItem *grouping;
/* When a new group is manually added, it is empty, but we still want to show it
	 * for a while (SHOW_EMPTY_GROUP_TIMEOUT seconds) even if 'show empty groups' is
	 * not selected.
	 */
GList *new_group;
guint new_group_timeout;
FinchBlistManager *manager;}FinchBlist;
typedef struct __unnamed_class___F0_L102_C9_unknown_scope_and_name_variable_declaration__variable_type_L55R_variable_name_unknown_scope_and_name__scope__row__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_guintUi__typedef_declaration_variable_name_unknown_scope_and_name__scope__signed_timer {
/* the row in the GntTree */
gpointer row;
/* used when 'recently' signed on/off */
guint signed_timer;}FinchBlistNode;
typedef enum __unnamed_enum___F0_L108_C9_STATUS_PRIMITIVE__COMMA__STATUS_SAVED_POPULAR__COMMA__STATUS_SAVED_ALL__COMMA__STATUS_SAVED_NEW {STATUS_PRIMITIVE,STATUS_SAVED_POPULAR,STATUS_SAVED_ALL,STATUS_SAVED_NEW}StatusType;
typedef struct __unnamed_class___F0_L116_C9_unknown_scope_and_name_variable_declaration__variable_type_StatusTypeL457R__typedef_declaration_variable_name_unknown_scope_and_name__scope__type__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__variable_name_unknown_scope_and_name__scope__u {
StatusType type;
union __unnamed_class___F0_L119_C2_L458R_variable_declaration__variable_type_L460R_variable_name_L458R__scope__prim__DELIMITER__L458R_variable_declaration__variable_type___Pb__L371R__Pe___variable_name_L458R__scope__saved {
PurpleStatusPrimitive prim;
PurpleSavedStatus *saved;}u;}StatusBoxItem;
static FinchBlist *ggblist;
static void add_buddy(PurpleBuddy *buddy,FinchBlist *ggblist);
static void add_contact(PurpleContact *contact,FinchBlist *ggblist);
static void add_group(PurpleGroup *group,FinchBlist *ggblist);
static void add_chat(PurpleChat *chat,FinchBlist *ggblist);
static void add_node(PurpleBlistNode *node,FinchBlist *ggblist);
static void node_update(PurpleBuddyList *list,PurpleBlistNode *node);
#if 0
#endif
static void draw_tooltip(FinchBlist *ggblist);
static void tooltip_for_buddy(PurpleBuddy *buddy,GString *str,gboolean full);
static gboolean remove_typing_cb(gpointer null);
static void remove_peripherals(FinchBlist *ggblist);
static const char *get_display_name(PurpleBlistNode *node);
static void savedstatus_changed(PurpleSavedStatus *now,PurpleSavedStatus *old);
static void blist_show(PurpleBuddyList *list);
static void update_node_display(PurpleBlistNode *buddy,FinchBlist *ggblist);
static void update_buddy_display(PurpleBuddy *buddy,FinchBlist *ggblist);
static gboolean account_autojoin_cb(PurpleConnection *pc,gpointer null);
static void finch_request_add_buddy(PurpleAccount *account,const char *username,const char *grp,const char *alias);
static void menu_group_set_cb(GntMenuItem *item,gpointer null);
/* Sort functions */
static int blist_node_compare_position(PurpleBlistNode *n1,PurpleBlistNode *n2);
static int blist_node_compare_text(PurpleBlistNode *n1,PurpleBlistNode *n2);
static int blist_node_compare_status(PurpleBlistNode *n1,PurpleBlistNode *n2);
static int blist_node_compare_log(PurpleBlistNode *n1,PurpleBlistNode *n2);
static int color_available;
static int color_away;
static int color_offline;
static int color_idle;
/**
 * Buddy List Manager functions.
 */

static gboolean default_can_add_node(PurpleBlistNode *node)
{
  gboolean offline = purple_prefs_get_bool("/finch/blist/showoffline");
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    PurpleBuddy *buddy = (PurpleBuddy *)node;
    FinchBlistNode *fnode = (node -> ui_data);
    if (!(purple_buddy_get_contact(buddy) != 0)) 
/* When a new buddy is added and show-offline is set */
      return 0;
    if (((buddy != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0)) 
/* The buddy is online */
      return 1;
    if (!(purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) 
/* The account is disconnected. Do not show */
      return 0;
    if (offline != 0) 
/* We want to see offline buddies too */
      return 1;
    if ((fnode != 0) && ((fnode -> signed_timer) != 0U)) 
/* Show if the buddy just signed off */
      return 1;
    if (purple_blist_node_get_bool(node,"show_offline") != 0) 
      return 1;
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    PurpleBlistNode *nd;
    for (nd = purple_blist_node_get_first_child(node); nd != 0; nd = purple_blist_node_get_sibling_next(nd)) {
      if (default_can_add_node(nd) != 0) 
        return 1;
    }
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
    PurpleChat *chat = (PurpleChat *)node;
    if (purple_account_is_connected((purple_chat_get_account(chat))) != 0) 
/* Show whenever the account is online */
      return 1;
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
    PurpleBlistNode *nd;
    gboolean empty = purple_prefs_get_bool("/finch/blist/emptygroups");
    if (empty != 0) 
/* If we want to see empty groups, we can show any group */
      return 1;
    for (nd = purple_blist_node_get_first_child(node); nd != 0; nd = purple_blist_node_get_sibling_next(nd)) {
      if (default_can_add_node(nd) != 0) 
        return 1;
    }
    if (((ggblist != 0) && ((ggblist -> new_group) != 0)) && (g_list_find((ggblist -> new_group),node) != 0)) 
      return 1;
  }
  return 0;
}

static gpointer default_find_parent(PurpleBlistNode *node)
{
  gpointer ret = (gpointer )((void *)0);
  switch(purple_blist_node_get_type(node)){
    case PURPLE_BLIST_CONTACT_NODE:
{
    }
    case PURPLE_BLIST_BUDDY_NODE:
{
    }
    case PURPLE_BLIST_CHAT_NODE:
{
      ret = (purple_blist_node_get_parent(node));
      break; 
    }
    default:
{
      break; 
    }
  }
  if (ret != 0) 
    add_node(ret,ggblist);
  return ret;
}

static gboolean default_create_tooltip(gpointer selected_row,GString **body,char **tool_title)
{
  GString *str;
  PurpleBlistNode *node = selected_row;
  int lastseen = 0;
  char *title;
  if (!(node != 0) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_OTHER_NODE)) 
    return 0;
  str = g_string_new("");
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    PurpleBuddy *pr = purple_contact_get_priority_buddy(((PurpleContact *)node));
    gboolean offline = !(((pr != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(pr))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(pr))) != 0));
    gboolean showoffline = purple_prefs_get_bool("/finch/blist/showoffline");
    const char *name = purple_buddy_get_name(pr);
    title = g_strdup(name);
    tooltip_for_buddy(pr,str,1);
    for (node = purple_blist_node_get_first_child(node); node != 0; node = purple_blist_node_get_sibling_next(node)) {{
        PurpleBuddy *buddy = (PurpleBuddy *)node;
        if (offline != 0) {
          int value = purple_blist_node_get_int(node,"last_seen");
          if (value > lastseen) 
            lastseen = value;
        }
        if (node == ((PurpleBlistNode *)pr)) 
          continue; 
        if (!(purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) 
          continue; 
        if (!(showoffline != 0) && !(((buddy != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0))) 
          continue; 
        str = g_string_append(str,"\n----------\n");
        tooltip_for_buddy(buddy,str,0);
      }
    }
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    PurpleBuddy *buddy = (PurpleBuddy *)node;
    tooltip_for_buddy(buddy,str,1);
    title = g_strdup(purple_buddy_get_name(buddy));
    if (!(((((PurpleBuddy *)node) != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(((PurpleBuddy *)node)))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(((PurpleBuddy *)node)))) != 0))) 
      lastseen = purple_blist_node_get_int(node,"last_seen");
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
    PurpleGroup *group = (PurpleGroup *)node;
    g_string_append_printf(str,((const char *)(dgettext("pidgin","Online: %d\nTotal: %d"))),purple_blist_get_group_online_count(group),purple_blist_get_group_size(group,0));
    title = g_strdup(purple_group_get_name(group));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
    PurpleChat *chat = (PurpleChat *)node;
    PurpleAccount *account = purple_chat_get_account(chat);
    g_string_append_printf(str,((const char *)(dgettext("pidgin","Account: %s (%s)"))),purple_account_get_username(account),purple_account_get_protocol_name(account));
    title = g_strdup(purple_chat_get_name(chat));
  }
  else {
    g_string_free(str,1);
    return 0;
  }
  if (lastseen > 0) {
    char *tmp = purple_str_seconds_to_string((time(0) - lastseen));
    g_string_append_printf(str,((const char *)(dgettext("pidgin","\nLast Seen: %s ago"))),tmp);
    g_free(tmp);
  }
  if (tool_title != 0) 
     *tool_title = title;
  else 
    g_free(title);
  if (body != 0) 
     *body = str;
  else 
    g_string_free(str,1);
  return 1;
}
static FinchBlistManager default_manager = {("default"), ("Default"), ((gboolean (*)())((void *)0)), ((gboolean (*)())((void *)0)), (default_can_add_node), (default_find_parent), (default_create_tooltip), {((gpointer )((void *)0)), ((gpointer )((void *)0)), ((gpointer )((void *)0)), ((gpointer )((void *)0))}};
static GList *managers;

static FinchBlistNode *create_finch_blist_node(PurpleBlistNode *node,gpointer row)
{
  FinchBlistNode *fnode = (node -> ui_data);
  if (!(fnode != 0)) {
    fnode = ((FinchBlistNode *)(g_malloc0_n(1,(sizeof(FinchBlistNode )))));
    fnode -> signed_timer = 0;
    node -> ui_data = fnode;
  }
  fnode -> row = row;
  return fnode;
}

static void reset_blist_node_ui_data(PurpleBlistNode *node)
{
  FinchBlistNode *fnode = (node -> ui_data);
  if (fnode == ((FinchBlistNode *)((void *)0))) 
    return ;
  if ((fnode -> signed_timer) != 0U) 
    purple_timeout_remove((fnode -> signed_timer));
  g_free(fnode);
  node -> ui_data = ((void *)((void *)0));
}

static int get_display_color(PurpleBlistNode *node)
{
  PurpleBuddy *buddy;
  int color = 0;
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) 
    node = ((PurpleBlistNode *)(purple_contact_get_priority_buddy(((PurpleContact *)node))));
  if (!((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)) 
    return 0;
  buddy = ((PurpleBuddy *)node);
  if (purple_presence_is_idle((purple_buddy_get_presence(buddy))) != 0) {
    color = color_idle;
  }
  else if (purple_presence_is_available((purple_buddy_get_presence(buddy))) != 0) {
    color = color_available;
  }
  else if ((purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0) && !(purple_presence_is_available((purple_buddy_get_presence(buddy))) != 0)) {
    color = color_away;
  }
  else if (!(purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0)) {
    color = color_offline;
  }
  return color;
}

static GntTextFormatFlags get_blist_node_flag(PurpleBlistNode *node)
{
  GntTextFormatFlags flag = 0;
  FinchBlistNode *fnode = (node -> ui_data);
  if (((ggblist -> tagged) != 0) && (g_list_find((ggblist -> tagged),node) != 0)) 
    flag |= GNT_TEXT_FLAG_BOLD;
  if ((fnode != 0) && ((fnode -> signed_timer) != 0U)) 
    flag |= GNT_TEXT_FLAG_BLINK;
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    node = ((PurpleBlistNode *)(purple_contact_get_priority_buddy(((PurpleContact *)node))));
    fnode = (node -> ui_data);
    if ((fnode != 0) && ((fnode -> signed_timer) != 0U)) 
      flag |= GNT_TEXT_FLAG_BLINK;
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
/* If the node is collapsed, then check to see if any of the priority buddies of
		 * any of the contacts within this group recently signed on/off, and set the blink
		 * flag appropriately. */
/* XXX: Refs #5444 */
/* XXX: there's no way I can ask if the node is expanded or not? *sigh*
		 * API addition would be necessary */
#if 0
#endif
  }
  return flag;
}

static void blist_update_row_flags(PurpleBlistNode *node)
{
  gnt_tree_set_row_flags(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),node,get_blist_node_flag(node));
  gnt_tree_set_row_color(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),node,get_display_color(node));
}
#if 0
#endif

static void new_node(PurpleBlistNode *node)
{
}

static void add_node(PurpleBlistNode *node,FinchBlist *ggblist)
{
  if ((node -> ui_data) != 0) 
    return ;
  if (!(( *( *(ggblist -> manager)).can_add_node)(node) != 0)) 
    return ;
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) 
    add_buddy(((PurpleBuddy *)node),ggblist);
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) 
    add_contact(((PurpleContact *)node),ggblist);
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) 
    add_group(((PurpleGroup *)node),ggblist);
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) 
    add_chat(((PurpleChat *)node),ggblist);
  draw_tooltip(ggblist);
}

void finch_blist_manager_add_node(PurpleBlistNode *node)
{
  add_node(node,ggblist);
}

static void remove_tooltip(FinchBlist *ggblist)
{
  gnt_widget_destroy((ggblist -> tooltip));
  ggblist -> tooltip = ((GntWidget *)((void *)0));
  ggblist -> tnode = ((PurpleBlistNode *)((void *)0));
}

static void node_remove(PurpleBuddyList *list,PurpleBlistNode *node)
{
  FinchBlist *ggblist = (list -> ui_data);
  PurpleBlistNode *parent;
  if ((ggblist == ((FinchBlist *)((void *)0))) || ((node -> ui_data) == ((void *)((void *)0)))) 
    return ;
  if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) && ((ggblist -> new_group) != 0)) {
    ggblist -> new_group = g_list_remove((ggblist -> new_group),node);
  }
  gnt_tree_remove(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),node);
  reset_blist_node_ui_data(node);
  if ((ggblist -> tagged) != 0) 
    ggblist -> tagged = g_list_remove((ggblist -> tagged),node);
  parent = purple_blist_node_get_parent(node);
  for (node = purple_blist_node_get_first_child(node); node != 0; node = purple_blist_node_get_sibling_next(node)) 
    node_remove(list,node);
  if (parent != 0) {
    if (!(( *( *(ggblist -> manager)).can_add_node)(parent) != 0)) 
      node_remove(list,parent);
    else 
      node_update(list,parent);
  }
  draw_tooltip(ggblist);
}

static void node_update(PurpleBuddyList *list,PurpleBlistNode *node)
{
/* It really looks like this should never happen ... but it does.
           This will at least emit a warning to the log when it
           happens, so maybe someone will figure it out. */
  do {
    if (node != ((PurpleBlistNode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return ;
    };
  }while (0);
  if ((list -> ui_data) == ((void *)((void *)0))) 
/* XXX: this is probably the place to auto-join chats */
    return ;
  if ((ggblist -> window) == ((GntWidget *)((void *)0))) 
    return ;
  if ((node -> ui_data) != ((void *)((void *)0))) {
    gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),node,0,get_display_name(node));
    gnt_tree_sort_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),node);
    blist_update_row_flags(node);
    if (gnt_tree_get_parent_key(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),node) != ( *( *(ggblist -> manager)).find_parent)(node)) 
      node_remove(list,node);
  }
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    PurpleBuddy *buddy = (PurpleBuddy *)node;
    add_node(((PurpleBlistNode *)buddy),(list -> ui_data));
    node_update(list,purple_blist_node_get_parent(node));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
    add_node(node,(list -> ui_data));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    if ((node -> ui_data) == ((void *)((void *)0))) {
/* The core seems to expect the UI to add the buddies. */
      for (node = purple_blist_node_get_first_child(node); node != 0; node = purple_blist_node_get_sibling_next(node)) 
        add_node(node,(list -> ui_data));
    }
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
    if (!(( *( *(ggblist -> manager)).can_add_node)(node) != 0)) 
      node_remove(list,node);
    else 
      add_node(node,(list -> ui_data));
  }
  if ((ggblist -> tnode) == node) {
    draw_tooltip(ggblist);
  }
}

static void new_list(PurpleBuddyList *list)
{
  if (ggblist != 0) 
    return ;
  ggblist = ((FinchBlist *)(g_malloc0_n(1,(sizeof(FinchBlist )))));
  list -> ui_data = ggblist;
  ggblist -> manager = finch_blist_manager_find(purple_prefs_get_string("/finch/blist/grouping"));
  if (!((ggblist -> manager) != 0)) 
    ggblist -> manager = &default_manager;
}

static void destroy_list(PurpleBuddyList *list)
{
  if (ggblist == ((FinchBlist *)((void *)0))) 
    return ;
  gnt_widget_destroy((ggblist -> window));
  g_free(ggblist);
  ggblist = ((FinchBlist *)((void *)0));
}

static gboolean remove_new_empty_group(gpointer data)
{
  PurpleBuddyList *list;
  if (!(ggblist != 0)) 
    return 0;
  list = purple_get_blist();
  do {
    if (list != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"list");
      return 0;
    };
  }while (0);
  ggblist -> new_group_timeout = 0;
  while((ggblist -> new_group) != 0){
    PurpleBlistNode *group = ( *(ggblist -> new_group)).data;
    ggblist -> new_group = g_list_delete_link((ggblist -> new_group),(ggblist -> new_group));
    node_update(list,group);
  }
  return 0;
}

static void add_buddy_cb(void *data,PurpleRequestFields *allfields)
{
  const char *username = purple_request_fields_get_string(allfields,"screenname");
  const char *alias = purple_request_fields_get_string(allfields,"alias");
  const char *group = purple_request_fields_get_string(allfields,"group");
  const char *invite = purple_request_fields_get_string(allfields,"invite");
  PurpleAccount *account = purple_request_fields_get_account(allfields,"account");
  const char *error = (const char *)((void *)0);
  PurpleGroup *grp;
  PurpleBuddy *buddy;
  if (!(username != 0)) 
    error = ((const char *)(dgettext("pidgin","You must provide a username for the buddy.")));
  else if (!(group != 0)) 
    error = ((const char *)(dgettext("pidgin","You must provide a group.")));
  else if (!(account != 0)) 
    error = ((const char *)(dgettext("pidgin","You must select an account.")));
  else if (!(purple_account_is_connected(account) != 0)) 
    error = ((const char *)(dgettext("pidgin","The selected account is not online.")));
  if (error != 0) {
    finch_request_add_buddy(account,username,group,alias);
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Error"))),((const char *)(dgettext("pidgin","Error adding buddy"))),error,0,0);
    return ;
  }
  grp = purple_find_group(group);
  if (!(grp != 0)) {
    grp = purple_group_new(group);
    purple_blist_add_group(grp,0);
  }
/* XXX: Ask to merge if there's already a buddy with the same alias in the same group (#4553) */
  if ((buddy = purple_find_buddy_in_group(account,username,grp)) == ((PurpleBuddy *)((void *)0))) {
    buddy = purple_buddy_new(account,username,alias);
    purple_blist_add_buddy(buddy,0,grp,0);
  }
  purple_account_add_buddy_with_invite(account,buddy,invite);
}

static void finch_request_add_buddy(PurpleAccount *account,const char *username,const char *grp,const char *alias)
{
  PurpleRequestFields *fields = purple_request_fields_new();
  PurpleRequestFieldGroup *group = purple_request_field_group_new(0);
  PurpleRequestField *field;
  purple_request_fields_add_group(fields,group);
  field = purple_request_field_string_new("screenname",((const char *)(dgettext("pidgin","Username"))),username,0);
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_string_new("alias",((const char *)(dgettext("pidgin","Alias (optional)"))),alias,0);
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_string_new("invite",((const char *)(dgettext("pidgin","Invite message (optional)"))),0,0);
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_string_new("group",((const char *)(dgettext("pidgin","Add in group"))),grp,0);
  purple_request_field_group_add_field(group,field);
  purple_request_field_set_type_hint(field,"group");
  field = purple_request_field_account_new("account",((const char *)(dgettext("pidgin","Account"))),0);
  purple_request_field_account_set_show_all(field,0);
  if (account != 0) 
    purple_request_field_account_set_value(field,account);
  purple_request_field_group_add_field(group,field);
  purple_request_fields(0,((const char *)(dgettext("pidgin","Add Buddy"))),0,((const char *)(dgettext("pidgin","Please enter buddy information."))),fields,((const char *)(dgettext("pidgin","Add"))),((GCallback )add_buddy_cb),((const char *)(dgettext("pidgin","Cancel"))),0,account,0,0,0);
}

static void join_chat(PurpleChat *chat)
{
  PurpleAccount *account = purple_chat_get_account(chat);
  const char *name;
  PurpleConversation *conv;
  const char *alias;
/* This hack here is to work around the fact that there's no good way of
	 * getting the actual name of a chat. I don't understand why we return
	 * the alias for a chat when all we want is the name. */
  alias = (chat -> alias);
  chat -> alias = ((char *)((void *)0));
  name = purple_chat_get_name(chat);
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,name,account);
  chat -> alias = ((char *)alias);
  if (!(conv != 0) || (purple_conv_chat_has_left(purple_conversation_get_chat_data(conv)) != 0)) {
    serv_join_chat(purple_account_get_connection(account),purple_chat_get_components(chat));
  }
  else if (conv != 0) {
    purple_conversation_present(conv);
  }
}

static void add_chat_cb(void *data,PurpleRequestFields *allfields)
{
  PurpleAccount *account;
  const char *alias;
  const char *name;
  const char *group;
  PurpleChat *chat;
  PurpleGroup *grp;
  GHashTable *hash = (GHashTable *)((void *)0);
  PurpleConnection *gc;
  gboolean autojoin;
  PurplePluginProtocolInfo *info;
  account = purple_request_fields_get_account(allfields,"account");
  name = purple_request_fields_get_string(allfields,"name");
  alias = purple_request_fields_get_string(allfields,"alias");
  group = purple_request_fields_get_string(allfields,"group");
  autojoin = purple_request_fields_get_bool(allfields,"autojoin");
  if ((!(purple_account_is_connected(account) != 0) || !(name != 0)) || !(( *name) != 0)) 
    return ;
  if (!(group != 0) || !(( *group) != 0)) 
    group = ((const char *)(dgettext("pidgin","Chats")));
  gc = purple_account_get_connection(account);
  info = ((PurplePluginProtocolInfo *)( *( *purple_connection_get_prpl(gc)).info).extra_info);
  if ((info -> chat_info_defaults) != ((GHashTable *(*)(PurpleConnection *, const char *))((void *)0))) 
    hash = ( *(info -> chat_info_defaults))(gc,name);
  chat = purple_chat_new(account,name,hash);
  if (chat != ((PurpleChat *)((void *)0))) {
    if ((grp = purple_find_group(group)) == ((PurpleGroup *)((void *)0))) {
      grp = purple_group_new(group);
      purple_blist_add_group(grp,0);
    }
    purple_blist_add_chat(chat,grp,0);
    purple_blist_alias_chat(chat,alias);
    purple_blist_node_set_bool(((PurpleBlistNode *)chat),"gnt-autojoin",autojoin);
    if (autojoin != 0) {
      join_chat(chat);
    }
  }
}

static void finch_request_add_chat(PurpleAccount *account,PurpleGroup *grp,const char *alias,const char *name)
{
  PurpleRequestFields *fields = purple_request_fields_new();
  PurpleRequestFieldGroup *group = purple_request_field_group_new(0);
  PurpleRequestField *field;
  purple_request_fields_add_group(fields,group);
  field = purple_request_field_account_new("account",((const char *)(dgettext("pidgin","Account"))),0);
  purple_request_field_account_set_show_all(field,0);
  if (account != 0) 
    purple_request_field_account_set_value(field,account);
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_string_new("name",((const char *)(dgettext("pidgin","Name"))),name,0);
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_string_new("alias",((const char *)(dgettext("pidgin","Alias"))),alias,0);
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_string_new("group",((const char *)(dgettext("pidgin","Group"))),((grp != 0)?purple_group_get_name(grp) : ((const char *)((void *)0))),0);
  purple_request_field_group_add_field(group,field);
  purple_request_field_set_type_hint(field,"group");
  field = purple_request_field_bool_new("autojoin",((const char *)(dgettext("pidgin","Auto-join"))),0);
  purple_request_field_group_add_field(group,field);
  purple_request_fields(0,((const char *)(dgettext("pidgin","Add Chat"))),0,((const char *)(dgettext("pidgin","You can edit more information from the context menu later."))),fields,((const char *)(dgettext("pidgin","Add"))),((GCallback )add_chat_cb),((const char *)(dgettext("pidgin","Cancel"))),0,0,0,0,0);
}

static void add_group_cb(gpointer null,const char *group)
{
  PurpleGroup *grp;
  if (!(group != 0) || !(( *group) != 0)) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Error"))),((const char *)(dgettext("pidgin","Error adding group"))),((const char *)(dgettext("pidgin","You must give a name for the group to add."))),0,0);
    return ;
  }
  grp = purple_find_group(group);
  if (!(grp != 0)) {
    grp = purple_group_new(group);
    purple_blist_add_group(grp,0);
  }
  if (!(ggblist != 0)) 
    return ;
/* Treat the group as a new group even if it had existed before. This should
	 * make things easier to add buddies to empty groups (new or old) without having
	 * to turn on 'show empty groups' setting */
  ggblist -> new_group = g_list_prepend((ggblist -> new_group),grp);
  if ((ggblist -> new_group_timeout) != 0U) 
    purple_timeout_remove((ggblist -> new_group_timeout));
  ggblist -> new_group_timeout = purple_timeout_add_seconds(60,remove_new_empty_group,0);
/* Select the group */
  if ((ggblist -> tree) != 0) {
    FinchBlistNode *fnode = ( *((PurpleBlistNode *)grp)).ui_data;
    if (!(fnode != 0)) 
      add_node(((PurpleBlistNode *)grp),ggblist);
    gnt_tree_set_selected(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),grp);
  }
}

static void finch_request_add_group()
{
  purple_request_input(0,((const char *)(dgettext("pidgin","Add Group"))),0,((const char *)(dgettext("pidgin","Enter the name of the group"))),0,0,0,0,((const char *)(dgettext("pidgin","Add"))),((GCallback )add_group_cb),((const char *)(dgettext("pidgin","Cancel"))),0,0,0,0,0);
}
static PurpleBlistUiOps blist_ui_ops = {(new_list), (new_node), (blist_show), (node_update), (node_remove), (destroy_list), ((void (*)(PurpleBuddyList *, gboolean ))((void *)0)), (finch_request_add_buddy), (finch_request_add_chat), (finch_request_add_group), ((void (*)(PurpleBlistNode *))((void *)0)), ((void (*)(PurpleBlistNode *))((void *)0)), ((void (*)(PurpleAccount *))((void *)0)), ((void (*)())((void *)0))};

static gpointer finch_blist_get_handle()
{
  static int handle;
  return (&handle);
}

static void add_group(PurpleGroup *group,FinchBlist *ggblist)
{
  gpointer parent;
  PurpleBlistNode *node = (PurpleBlistNode *)group;
  if ((node -> ui_data) != 0) 
    return ;
  parent = ( *( *(ggblist -> manager)).find_parent)(((PurpleBlistNode *)group));
  create_finch_blist_node(node,(gnt_tree_add_row_after(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),group,gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),get_display_name(node)),parent,0)));
  gnt_tree_set_expanded(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),node,!(purple_blist_node_get_bool(node,"collapsed") != 0));
}

static const char *get_display_name(PurpleBlistNode *node)
{
  static char text[2096UL];
  char status[8UL] = " ";
  const char *name = (const char *)((void *)0);
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) 
/* XXX: this can return NULL?! */
    node = ((PurpleBlistNode *)(purple_contact_get_priority_buddy(((PurpleContact *)node))));
  if (node == ((PurpleBlistNode *)((void *)0))) 
    return 0;
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    PurpleBuddy *buddy = (PurpleBuddy *)node;
    PurpleStatusPrimitive prim;
    PurplePresence *presence;
    PurpleStatus *now;
    gboolean ascii = gnt_ascii_only();
    presence = purple_buddy_get_presence(buddy);
    if (purple_presence_is_status_primitive_active(presence,PURPLE_STATUS_MOBILE) != 0) 
      strncpy(status,((ascii != 0)?":" : "\342\230\216"),(sizeof(status) - 1));
    else {
      now = purple_presence_get_active_status(presence);
      prim = purple_status_type_get_primitive((purple_status_get_type(now)));
      switch(prim){
        case PURPLE_STATUS_OFFLINE:
{
          strncpy(status,((ascii != 0)?"x" : "\342\212\227"),(sizeof(status) - 1));
          break; 
        }
        case PURPLE_STATUS_AVAILABLE:
{
          strncpy(status,((ascii != 0)?"o" : "\342\227\257"),(sizeof(status) - 1));
          break; 
        }
        default:
{
          strncpy(status,((ascii != 0)?"." : "\342\212\226"),(sizeof(status) - 1));
          break; 
        }
      }
    }
    name = purple_buddy_get_alias(buddy);
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
    PurpleChat *chat = (PurpleChat *)node;
    name = purple_chat_get_name(chat);
    strncpy(status,"~",(sizeof(status) - 1));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) 
    return purple_group_get_name(((PurpleGroup *)node));
  g_snprintf(text,(sizeof(text) - 1),"%s %s",status,name);
  return text;
}

static void add_chat(PurpleChat *chat,FinchBlist *ggblist)
{
  gpointer parent;
  PurpleBlistNode *node = (PurpleBlistNode *)chat;
  if ((node -> ui_data) != 0) 
    return ;
  if (!(purple_account_is_connected((purple_chat_get_account(chat))) != 0)) 
    return ;
  parent = ( *( *(ggblist -> manager)).find_parent)(((PurpleBlistNode *)chat));
  create_finch_blist_node(node,(gnt_tree_add_row_after(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),chat,gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),get_display_name(node)),parent,0)));
}

static void add_contact(PurpleContact *contact,FinchBlist *ggblist)
{
  gpointer parent;
  PurpleBlistNode *node = (PurpleBlistNode *)contact;
  const char *name;
  if ((node -> ui_data) != 0) 
    return ;
  name = get_display_name(node);
  if (name == ((const char *)((void *)0))) 
    return ;
  parent = ( *( *(ggblist -> manager)).find_parent)(((PurpleBlistNode *)contact));
  create_finch_blist_node(node,(gnt_tree_add_row_after(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),contact,gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),name),parent,0)));
  gnt_tree_set_expanded(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),contact,0);
}

static void add_buddy(PurpleBuddy *buddy,FinchBlist *ggblist)
{
  gpointer parent;
  PurpleBlistNode *node = (PurpleBlistNode *)buddy;
  PurpleContact *contact;
  if ((node -> ui_data) != 0) 
    return ;
  contact = purple_buddy_get_contact(buddy);
  parent = ( *( *(ggblist -> manager)).find_parent)(((PurpleBlistNode *)buddy));
  create_finch_blist_node(node,(gnt_tree_add_row_after(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),buddy,gnt_tree_create_row(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),get_display_name(node)),parent,0)));
  blist_update_row_flags(((PurpleBlistNode *)buddy));
  if (buddy == purple_contact_get_priority_buddy(contact)) 
    blist_update_row_flags(((PurpleBlistNode *)contact));
}
#if 0
#endif

PurpleBlistUiOps *finch_blist_get_ui_ops()
{
  return &blist_ui_ops;
}

static void selection_activate(GntWidget *widget,FinchBlist *ggblist)
{
  GntTree *tree = (GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()));
  PurpleBlistNode *node = (gnt_tree_get_selection_data(tree));
  if (!(node != 0)) 
    return ;
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) 
    node = ((PurpleBlistNode *)(purple_contact_get_priority_buddy(((PurpleContact *)node))));
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    PurpleBuddy *buddy = (PurpleBuddy *)node;
    PurpleConversation *conv;
    conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,purple_buddy_get_name(buddy),(purple_buddy_get_account(buddy)));
    if (!(conv != 0)) {
      conv = purple_conversation_new(PURPLE_CONV_TYPE_IM,purple_buddy_get_account(buddy),purple_buddy_get_name(buddy));
    }
    else {
      FinchConv *ggconv = (conv -> ui_data);
      gnt_window_present((ggconv -> window));
    }
    finch_conversation_set_active(conv);
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
    join_chat(((PurpleChat *)node));
  }
}

static void context_menu_callback(GntMenuItem *item,gpointer data)
{
  PurpleMenuAction *action = data;
  PurpleBlistNode *node = (ggblist -> cnode);
  if (action != 0) {
    void (*callback)(PurpleBlistNode *, gpointer );
    callback = ((void (*)(PurpleBlistNode *, gpointer ))(action -> callback));
    if (callback != 0) 
      ( *callback)(node,(action -> data));
    else 
      return ;
  }
}

static void gnt_append_menu_action(GntMenu *menu,PurpleMenuAction *action,gpointer parent)
{
  GList *list;
  GntMenuItem *item;
  if (action == ((PurpleMenuAction *)((void *)0))) 
    return ;
  item = gnt_menuitem_new((action -> label));
  if ((action -> callback) != 0) 
    gnt_menuitem_set_callback(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),context_menu_callback,action);
  gnt_menu_add_item(menu,((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))));
  if ((action -> children) != 0) {
    GntWidget *sub = gnt_menu_new(GNT_MENU_POPUP);
    gnt_menuitem_set_submenu(item,((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))));
    for (list = (action -> children); list != 0; list = (list -> next)) 
      gnt_append_menu_action(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),(list -> data),action);
  }
}

static void append_proto_menu(GntMenu *menu,PurpleConnection *gc,PurpleBlistNode *node)
{
  GList *list;
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)( *( *purple_connection_get_prpl(gc)).info).extra_info;
  if (!(prpl_info != 0) || !((prpl_info -> blist_node_menu) != 0)) 
    return ;
  for (list = ( *(prpl_info -> blist_node_menu))(node); list != 0; list = g_list_delete_link(list,list)) {{
      PurpleMenuAction *act = (PurpleMenuAction *)(list -> data);
      if (!(act != 0)) 
        continue; 
      act -> data = node;
      gnt_append_menu_action(menu,act,0);
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menu),((GType )(20 << 2))))),"destroy",((GCallback )purple_menu_action_free),act,0,G_CONNECT_SWAPPED);
    }
  }
}

static void add_custom_action(GntMenu *menu,const char *label,PurpleCallback callback,gpointer data)
{
  PurpleMenuAction *action = purple_menu_action_new(label,callback,data,0);
  gnt_append_menu_action(menu,action,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menu),((GType )(20 << 2))))),"destroy",((GCallback )purple_menu_action_free),action,0,G_CONNECT_SWAPPED);
}

static void chat_components_edit_ok(PurpleChat *chat,PurpleRequestFields *allfields)
{
  GList *groups;
  GList *fields;
  for (groups = purple_request_fields_get_groups(allfields); groups != 0; groups = (groups -> next)) {
    fields = purple_request_field_group_get_fields((groups -> data));
    for (; fields != 0; fields = (fields -> next)) {
      PurpleRequestField *field = (fields -> data);
      const char *id;
      char *val;
      id = purple_request_field_get_id(field);
      if ((purple_request_field_get_type(field)) == PURPLE_REQUEST_FIELD_INTEGER) 
        val = g_strdup_printf("%d",purple_request_field_int_get_value(field));
      else 
        val = g_strdup(purple_request_field_string_get_value(field));
      if (!(val != 0)) {
        g_hash_table_remove(purple_chat_get_components(chat),id);
      }
      else {
/* val should not be free'd */
        g_hash_table_replace(purple_chat_get_components(chat),(g_strdup(id)),val);
      }
    }
  }
}

static void chat_components_edit(PurpleBlistNode *selected,PurpleChat *chat)
{
  PurpleRequestFields *fields = purple_request_fields_new();
  PurpleRequestFieldGroup *group = purple_request_field_group_new(0);
  PurpleRequestField *field;
  GList *parts;
  GList *iter;
  struct proto_chat_entry *pce;
  PurpleConnection *gc;
  purple_request_fields_add_group(fields,group);
  gc = purple_account_get_connection((purple_chat_get_account(chat)));
  parts = ( *( *((PurplePluginProtocolInfo *)( *( *purple_connection_get_prpl(gc)).info).extra_info)).chat_info)(gc);
  for (iter = parts; iter != 0; iter = (iter -> next)) {
    pce = (iter -> data);
    if ((pce -> is_int) != 0) {
      int val;
      const char *str = (g_hash_table_lookup(purple_chat_get_components(chat),(pce -> identifier)));
      if (!(str != 0) || (sscanf(str,"%d",&val) != 1)) 
        val = (pce -> min);
      field = purple_request_field_int_new((pce -> identifier),(pce -> label),val);
    }
    else {
      field = purple_request_field_string_new((pce -> identifier),(pce -> label),(g_hash_table_lookup(purple_chat_get_components(chat),(pce -> identifier))),0);
      if ((pce -> secret) != 0) 
        purple_request_field_string_set_masked(field,1);
    }
    if ((pce -> required) != 0) 
      purple_request_field_set_required(field,1);
    purple_request_field_group_add_field(group,field);
    g_free(pce);
  }
  g_list_free(parts);
  purple_request_fields(0,((const char *)(dgettext("pidgin","Edit Chat"))),0,((const char *)(dgettext("pidgin","Please Update the necessary fields."))),fields,((const char *)(dgettext("pidgin","Edit"))),((GCallback )chat_components_edit_ok),((const char *)(dgettext("pidgin","Cancel"))),0,0,0,0,chat);
}

static void autojoin_toggled(GntMenuItem *item,gpointer data)
{
  PurpleMenuAction *action = data;
  purple_blist_node_set_bool((action -> data),"gnt-autojoin",gnt_menuitem_check_get_checked(((GntMenuItemCheck *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_check_get_gtype())))));
}

static void create_chat_menu(GntMenu *menu,PurpleChat *chat)
{
  PurpleMenuAction *action = purple_menu_action_new(((const char *)(dgettext("pidgin","Auto-join"))),0,chat,0);
  GntMenuItem *check = gnt_menuitem_check_new((action -> label));
  gnt_menuitem_check_set_checked(((GntMenuItemCheck *)(g_type_check_instance_cast(((GTypeInstance *)check),gnt_menuitem_check_get_gtype()))),purple_blist_node_get_bool(((PurpleBlistNode *)chat),"gnt-autojoin"));
  gnt_menu_add_item(menu,check);
  gnt_menuitem_set_callback(check,autojoin_toggled,action);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menu),((GType )(20 << 2))))),"destroy",((GCallback )purple_menu_action_free),action,0,G_CONNECT_SWAPPED);
  add_custom_action(menu,((const char *)(dgettext("pidgin","Edit Settings"))),((PurpleCallback )chat_components_edit),chat);
}

static void finch_add_buddy(PurpleBlistNode *selected,PurpleGroup *grp)
{
  purple_blist_request_add_buddy(0,0,((grp != 0)?purple_group_get_name(grp) : ((const char *)((void *)0))),0);
}

static void finch_add_group(PurpleBlistNode *selected,PurpleGroup *grp)
{
  purple_blist_request_add_group();
}

static void finch_add_chat(PurpleBlistNode *selected,PurpleGroup *grp)
{
  purple_blist_request_add_chat(0,grp,0,0);
}

static void create_group_menu(GntMenu *menu,PurpleGroup *group)
{
  add_custom_action(menu,((const char *)(dgettext("pidgin","Add Buddy"))),((PurpleCallback )finch_add_buddy),group);
  add_custom_action(menu,((const char *)(dgettext("pidgin","Add Chat"))),((PurpleCallback )finch_add_chat),group);
  add_custom_action(menu,((const char *)(dgettext("pidgin","Add Group"))),((PurpleCallback )finch_add_group),group);
}

gpointer finch_retrieve_user_info(PurpleConnection *conn,const char *name)
{
  PurpleNotifyUserInfo *info = purple_notify_user_info_new();
  gpointer uihandle;
  purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Information"))),((const char *)(dgettext("pidgin","Retrieving..."))));
  uihandle = purple_notify_userinfo(conn,name,info,0,0);
  purple_notify_user_info_destroy(info);
  serv_get_info(conn,name);
  return uihandle;
}

static void finch_blist_get_buddy_info_cb(PurpleBlistNode *selected,PurpleBuddy *buddy)
{
  finch_retrieve_user_info(purple_account_get_connection((purple_buddy_get_account(buddy))),purple_buddy_get_name(buddy));
}

static void finch_blist_menu_send_file_cb(PurpleBlistNode *selected,PurpleBuddy *buddy)
{
  serv_send_file(purple_account_get_connection((purple_buddy_get_account(buddy))),purple_buddy_get_name(buddy),0);
}

static void finch_blist_pounce_node_cb(PurpleBlistNode *selected,PurpleBlistNode *node)
{
  PurpleBuddy *b;
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) 
    b = purple_contact_get_priority_buddy(((PurpleContact *)node));
  else 
    b = ((PurpleBuddy *)node);
  finch_pounce_editor_show(purple_buddy_get_account(b),purple_buddy_get_name(b),0);
}

static void toggle_block_buddy(GntMenuItem *item,gpointer buddy)
{
  gboolean block = gnt_menuitem_check_get_checked(((GntMenuItemCheck *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_check_get_gtype()))));
  PurpleAccount *account = purple_buddy_get_account(buddy);
  const char *name = purple_buddy_get_name(buddy);
  (block != 0)?purple_privacy_deny(account,name,0,0) : purple_privacy_allow(account,name,0,0);
}

static void toggle_show_offline(GntMenuItem *item,gpointer buddy)
{
  purple_blist_node_set_bool(buddy,"show_offline",!(purple_blist_node_get_bool(buddy,"show_offline") != 0));
  if (!(( *( *(ggblist -> manager)).can_add_node)(buddy) != 0)) 
    node_remove(purple_get_blist(),buddy);
  else 
    node_update(purple_get_blist(),buddy);
}

static void create_buddy_menu(GntMenu *menu,PurpleBuddy *buddy)
{
  PurpleAccount *account;
  gboolean permitted;
  GntMenuItem *item;
  PurplePluginProtocolInfo *prpl_info;
  PurpleConnection *gc = purple_account_get_connection((purple_buddy_get_account(buddy)));
  prpl_info = ((PurplePluginProtocolInfo *)( *( *purple_connection_get_prpl(gc)).info).extra_info);
  if ((prpl_info != 0) && ((prpl_info -> get_info) != 0)) {
    add_custom_action(menu,((const char *)(dgettext("pidgin","Get Info"))),((PurpleCallback )finch_blist_get_buddy_info_cb),buddy);
  }
  add_custom_action(menu,((const char *)(dgettext("pidgin","Add Buddy Pounce"))),((PurpleCallback )finch_blist_pounce_node_cb),buddy);
  if ((prpl_info != 0) && ((prpl_info -> send_file) != 0)) {
    if (!((prpl_info -> can_receive_file) != 0) || (( *(prpl_info -> can_receive_file))(gc,purple_buddy_get_name(buddy)) != 0)) 
      add_custom_action(menu,((const char *)(dgettext("pidgin","Send File"))),((PurpleCallback )finch_blist_menu_send_file_cb),buddy);
  }
  account = purple_buddy_get_account(buddy);
  permitted = purple_privacy_check(account,purple_buddy_get_name(buddy));
  item = gnt_menuitem_check_new(((const char *)(dgettext("pidgin","Blocked"))));
  gnt_menuitem_check_set_checked(((GntMenuItemCheck *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_check_get_gtype()))),!(permitted != 0));
  gnt_menuitem_set_callback(item,toggle_block_buddy,buddy);
  gnt_menu_add_item(menu,item);
  item = gnt_menuitem_check_new(((const char *)(dgettext("pidgin","Show when offline"))));
  gnt_menuitem_check_set_checked(((GntMenuItemCheck *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_check_get_gtype()))),purple_blist_node_get_bool(((PurpleBlistNode *)buddy),"show_offline"));
  gnt_menuitem_set_callback(item,toggle_show_offline,buddy);
  gnt_menu_add_item(menu,item);
/* Protocol actions */
  append_proto_menu(menu,purple_account_get_connection((purple_buddy_get_account(buddy))),((PurpleBlistNode *)buddy));
}

static void append_extended_menu(GntMenu *menu,PurpleBlistNode *node)
{
  GList *iter;
  for (iter = purple_blist_node_get_extended_menu(node); iter != 0; iter = g_list_delete_link(iter,iter)) {
    gnt_append_menu_action(menu,(iter -> data),0);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menu),((GType )(20 << 2))))),"destroy",((GCallback )purple_menu_action_free),(iter -> data),0,G_CONNECT_SWAPPED);
  }
}
/* Xerox'd from gtkdialogs.c:purple_gtkdialogs_remove_contact_cb */

static void remove_contact(PurpleContact *contact)
{
  PurpleBlistNode *bnode;
  PurpleBlistNode *cnode;
  PurpleGroup *group;
  cnode = ((PurpleBlistNode *)contact);
  group = ((PurpleGroup *)(purple_blist_node_get_parent(cnode)));
  for (bnode = purple_blist_node_get_first_child(cnode); bnode != 0; bnode = purple_blist_node_get_sibling_next(bnode)) {
    PurpleBuddy *buddy = (PurpleBuddy *)bnode;
    PurpleAccount *account = purple_buddy_get_account(buddy);
    if (purple_account_is_connected(account) != 0) 
      purple_account_remove_buddy(account,buddy,group);
  }
  purple_blist_remove_contact(contact);
}

static void rename_blist_node(PurpleBlistNode *node,const char *newname)
{
  const char *name = newname;
  if ((name != 0) && !(( *name) != 0)) 
    name = ((const char *)((void *)0));
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    PurpleContact *contact = (PurpleContact *)node;
    PurpleBuddy *buddy = purple_contact_get_priority_buddy(contact);
    purple_blist_alias_contact(contact,name);
    purple_blist_alias_buddy(buddy,name);
    serv_alias_buddy(buddy);
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    purple_blist_alias_buddy(((PurpleBuddy *)node),name);
    serv_alias_buddy(((PurpleBuddy *)node));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) 
    purple_blist_alias_chat(((PurpleChat *)node),name);
  else if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) && (name != ((const char *)((void *)0)))) 
    purple_blist_rename_group(((PurpleGroup *)node),name);
  else 
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","gntblist.c",1423,((const char *)__func__));
      return ;
    }while (0);
}

static void finch_blist_rename_node_cb(PurpleBlistNode *selected,PurpleBlistNode *node)
{
  const char *name = (const char *)((void *)0);
  char *prompt;
  const char *text;
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) 
    name = purple_contact_get_alias(((PurpleContact *)node));
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) 
    name = purple_buddy_get_contact_alias(((PurpleBuddy *)node));
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) 
    name = purple_chat_get_name(((PurpleChat *)node));
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) 
    name = purple_group_get_name(((PurpleGroup *)node));
  else 
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","gntblist.c",1442,((const char *)__func__));
      return ;
    }while (0);
  prompt = g_strdup_printf(((const char *)(dgettext("pidgin","Please enter the new name for %s"))),name);
  text = (((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE)?((const char *)(dgettext("pidgin","Rename"))) : ((const char *)(dgettext("pidgin","Set Alias"))));
  purple_request_input(node,text,prompt,((const char *)(dgettext("pidgin","Enter empty string to reset the name."))),name,0,0,0,text,((GCallback )rename_blist_node),((const char *)(dgettext("pidgin","Cancel"))),0,0,0,0,node);
  g_free(prompt);
}

static void showlog_cb(PurpleBlistNode *sel,PurpleBlistNode *node)
{
  PurpleLogType type;
  PurpleAccount *account;
  char *name = (char *)((void *)0);
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    PurpleBuddy *b = (PurpleBuddy *)node;
    type = PURPLE_LOG_IM;
    name = g_strdup(purple_buddy_get_name(b));
    account = purple_buddy_get_account(b);
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
    PurpleChat *c = (PurpleChat *)node;
    PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
    type = PURPLE_LOG_CHAT;
    account = purple_chat_get_account(c);
    prpl_info = ((PurplePluginProtocolInfo *)( *( *purple_find_prpl(purple_account_get_protocol_id(account))).info).extra_info);
    if ((prpl_info != 0) && ((prpl_info -> get_chat_name) != 0)) {
      name = ( *(prpl_info -> get_chat_name))(purple_chat_get_components(c));
    }
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    finch_log_show_contact(((PurpleContact *)node));
    return ;
  }
  else {
/* This callback should not have been registered for a node
		 * that doesn't match the type of one of the blocks above. */
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","gntblist.c",1483,((const char *)__func__));
      return ;
    }while (0);
  }
  if ((name != 0) && (account != 0)) {
    finch_log_show(type,name,account);
    g_free(name);
  }
}
/* Xeroxed from gtkdialogs.c:purple_gtkdialogs_remove_group_cb*/

static void remove_group(PurpleGroup *group)
{
  PurpleBlistNode *cnode;
  PurpleBlistNode *bnode;
  cnode = purple_blist_node_get_first_child(((PurpleBlistNode *)group));
  while(cnode != 0){
    if ((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CONTACT_NODE) {
      bnode = purple_blist_node_get_first_child(cnode);
      cnode = purple_blist_node_get_sibling_next(cnode);
      while(bnode != 0){
        PurpleBuddy *buddy;
        if ((purple_blist_node_get_type(bnode)) == PURPLE_BLIST_BUDDY_NODE) {
          PurpleAccount *account;
          buddy = ((PurpleBuddy *)bnode);
          bnode = purple_blist_node_get_sibling_next(bnode);
          account = purple_buddy_get_account(buddy);
          if (purple_account_is_connected(account) != 0) {
            purple_account_remove_buddy(account,buddy,group);
            purple_blist_remove_buddy(buddy);
          }
        }
        else {
          bnode = purple_blist_node_get_sibling_next(bnode);
        }
      }
    }
    else if ((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CHAT_NODE) {
      PurpleChat *chat = (PurpleChat *)cnode;
      cnode = purple_blist_node_get_sibling_next(cnode);
      if (purple_account_is_connected((purple_chat_get_account(chat))) != 0) 
        purple_blist_remove_chat(chat);
    }
    else {
      cnode = purple_blist_node_get_sibling_next(cnode);
    }
  }
  purple_blist_remove_group(group);
}

static void finch_blist_remove_node(PurpleBlistNode *node)
{
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    remove_contact(((PurpleContact *)node));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    PurpleBuddy *buddy = (PurpleBuddy *)node;
    PurpleGroup *group = purple_buddy_get_group(buddy);
    purple_account_remove_buddy(purple_buddy_get_account(buddy),buddy,group);
    purple_blist_remove_buddy(buddy);
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
    purple_blist_remove_chat(((PurpleChat *)node));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
    remove_group(((PurpleGroup *)node));
  }
}

static void finch_blist_remove_node_cb(PurpleBlistNode *selected,PurpleBlistNode *node)
{
  PurpleAccount *account = (PurpleAccount *)((void *)0);
  char *primary;
  const char *name;
  const char *sec = (const char *)((void *)0);
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    PurpleContact *c = (PurpleContact *)node;
    name = purple_contact_get_alias(c);
    if ((c -> totalsize) > 1) 
      sec = ((const char *)(dgettext("pidgin","Removing this contact will also remove all the buddies in the contact")));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    name = purple_buddy_get_name(((PurpleBuddy *)node));
    account = purple_buddy_get_account(((PurpleBuddy *)node));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
    name = purple_chat_get_name(((PurpleChat *)node));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
    name = purple_group_get_name(((PurpleGroup *)node));
    sec = ((const char *)(dgettext("pidgin","Removing this group will also remove all the buddies in the group")));
  }
  else 
    return ;
  primary = g_strdup_printf(((const char *)(dgettext("pidgin","Are you sure you want to remove %s\?"))),name);
/* XXX: anything to do with the returned ui-handle? */
  purple_request_action(node,((const char *)(dgettext("pidgin","Confirm Remove"))),primary,sec,1,account,name,0,node,2,((const char *)(dgettext("pidgin","Remove"))),finch_blist_remove_node,((const char *)(dgettext("pidgin","Cancel"))),((void *)((void *)0)));
  g_free(primary);
}

static void finch_blist_toggle_tag_buddy(PurpleBlistNode *node)
{
  GList *iter;
  if (node == ((PurpleBlistNode *)((void *)0))) 
    return ;
  if (((ggblist -> tagged) != 0) && ((iter = g_list_find((ggblist -> tagged),node)) != ((GList *)((void *)0)))) {
    ggblist -> tagged = g_list_delete_link((ggblist -> tagged),iter);
  }
  else {
    ggblist -> tagged = g_list_prepend((ggblist -> tagged),node);
  }
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) 
    update_buddy_display(purple_contact_get_priority_buddy(((PurpleContact *)node)),ggblist);
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) 
    update_buddy_display(((PurpleBuddy *)node),ggblist);
  else 
    update_node_display(node,ggblist);
}

static void finch_blist_place_tagged(PurpleBlistNode *target)
{
  PurpleGroup *tg = (PurpleGroup *)((void *)0);
  PurpleContact *tc = (PurpleContact *)((void *)0);
  if ((target == ((PurpleBlistNode *)((void *)0))) || ((purple_blist_node_get_type(target)) == PURPLE_BLIST_OTHER_NODE)) 
    return ;
  if ((purple_blist_node_get_type(target)) == PURPLE_BLIST_GROUP_NODE) 
    tg = ((PurpleGroup *)target);
  else if ((purple_blist_node_get_type(target)) == PURPLE_BLIST_BUDDY_NODE) {
    tc = ((PurpleContact *)(purple_blist_node_get_parent(target)));
    tg = ((PurpleGroup *)(purple_blist_node_get_parent(((PurpleBlistNode *)tc))));
  }
  else {
    if ((purple_blist_node_get_type(target)) == PURPLE_BLIST_CONTACT_NODE) 
      tc = ((PurpleContact *)target);
    tg = ((PurpleGroup *)(purple_blist_node_get_parent(target)));
  }
  if ((ggblist -> tagged) != 0) {
    GList *list = (ggblist -> tagged);
    ggblist -> tagged = ((GList *)((void *)0));
    while(list != 0){
      PurpleBlistNode *node = (list -> data);
      list = g_list_delete_link(list,list);
      if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
        update_node_display(node,ggblist);
/* Add the group after the current group */
        purple_blist_add_group(((PurpleGroup *)node),((PurpleBlistNode *)tg));
      }
      else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
        update_buddy_display(purple_contact_get_priority_buddy(((PurpleContact *)node)),ggblist);
        if (((PurpleBlistNode *)tg) == target) {
/* The target is a group, just add the contact to the group. */
          purple_blist_add_contact(((PurpleContact *)node),tg,0);
        }
        else if (tc != 0) {
/* The target is either a buddy, or a contact. Merge with that contact. */
          purple_blist_merge_contact(((PurpleContact *)node),((PurpleBlistNode *)tc));
        }
        else {
/* The target is a chat. Add the contact to the group after this chat. */
          purple_blist_add_contact(((PurpleContact *)node),0,target);
        }
      }
      else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
        update_buddy_display(((PurpleBuddy *)node),ggblist);
        if (((PurpleBlistNode *)tg) == target) {
/* The target is a group. Add this buddy in a new contact under this group. */
          purple_blist_add_buddy(((PurpleBuddy *)node),0,tg,0);
        }
        else if ((purple_blist_node_get_type(target)) == PURPLE_BLIST_CONTACT_NODE) {
/* Add to the contact. */
          purple_blist_add_buddy(((PurpleBuddy *)node),tc,0,0);
        }
        else if ((purple_blist_node_get_type(target)) == PURPLE_BLIST_BUDDY_NODE) {
/* Add to the contact after the selected buddy. */
          purple_blist_add_buddy(((PurpleBuddy *)node),0,0,target);
        }
        else if ((purple_blist_node_get_type(target)) == PURPLE_BLIST_CHAT_NODE) {
/* Add to the selected chat's group. */
          purple_blist_add_buddy(((PurpleBuddy *)node),0,tg,0);
        }
      }
      else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
        update_node_display(node,ggblist);
        if (((PurpleBlistNode *)tg) == target) 
          purple_blist_add_chat(((PurpleChat *)node),tg,0);
        else 
          purple_blist_add_chat(((PurpleChat *)node),0,target);
      }
    }
  }
}

static void context_menu_destroyed(GntWidget *widget,FinchBlist *ggblist)
{
  ggblist -> context = ((GntWidget *)((void *)0));
}

static void draw_context_menu(FinchBlist *ggblist)
{
  PurpleBlistNode *node = (PurpleBlistNode *)((void *)0);
  GntWidget *context = (GntWidget *)((void *)0);
  GntTree *tree = (GntTree *)((void *)0);
  int x;
  int y;
  int top;
  int width;
  char *title = (char *)((void *)0);
  if ((ggblist -> context) != 0) 
    return ;
  tree = ((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype())));
  node = (gnt_tree_get_selection_data(tree));
  if ((node != 0) && ((purple_blist_node_get_type(node)) == PURPLE_BLIST_OTHER_NODE)) 
    return ;
  if ((ggblist -> tooltip) != 0) 
    remove_tooltip(ggblist);
  ggblist -> cnode = node;
  ggblist -> context = (context = gnt_menu_new(GNT_MENU_POPUP));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)context),((GType )(20 << 2))))),"destroy",((GCallback )context_menu_destroyed),ggblist,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)context),((GType )(20 << 2))))),"hide",((GCallback )gnt_widget_destroy),0,0,((GConnectFlags )0));
  if (!(node != 0)) {
    create_group_menu(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)context),gnt_menu_get_gtype()))),0);
    title = g_strdup(((const char *)(dgettext("pidgin","Buddy List"))));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    ggblist -> cnode = ((PurpleBlistNode *)(purple_contact_get_priority_buddy(((PurpleContact *)node))));
    create_buddy_menu(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)context),gnt_menu_get_gtype()))),((PurpleBuddy *)(ggblist -> cnode)));
    title = g_strdup(purple_contact_get_alias(((PurpleContact *)node)));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    PurpleBuddy *buddy = (PurpleBuddy *)node;
    create_buddy_menu(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)context),gnt_menu_get_gtype()))),buddy);
    title = g_strdup(purple_buddy_get_name(buddy));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
    PurpleChat *chat = (PurpleChat *)node;
    create_chat_menu(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)context),gnt_menu_get_gtype()))),chat);
    title = g_strdup(purple_chat_get_name(chat));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
    PurpleGroup *group = (PurpleGroup *)node;
    create_group_menu(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)context),gnt_menu_get_gtype()))),group);
    title = g_strdup(purple_group_get_name(group));
  }
  append_extended_menu(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)context),gnt_menu_get_gtype()))),node);
/* These are common for everything */
  if (node != 0) {
    add_custom_action(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)context),gnt_menu_get_gtype()))),(((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE)?((const char *)(dgettext("pidgin","Rename"))) : ((const char *)(dgettext("pidgin","Alias")))),((PurpleCallback )finch_blist_rename_node_cb),node);
    add_custom_action(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)context),gnt_menu_get_gtype()))),((const char *)(dgettext("pidgin","Remove"))),((PurpleCallback )finch_blist_remove_node_cb),node);
    if (((ggblist -> tagged) != 0) && (((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE))) {
      add_custom_action(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)context),gnt_menu_get_gtype()))),((const char *)(dgettext("pidgin","Place tagged"))),((PurpleCallback )finch_blist_place_tagged),node);
    }
    if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE)) {
      add_custom_action(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)context),gnt_menu_get_gtype()))),((const char *)(dgettext("pidgin","Toggle Tag"))),((PurpleCallback )finch_blist_toggle_tag_buddy),node);
    }
    if (!((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE)) {
      add_custom_action(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)context),gnt_menu_get_gtype()))),((const char *)(dgettext("pidgin","View Log"))),((PurpleCallback )showlog_cb),node);
    }
  }
/* Set the position for the popup */
  gnt_widget_get_position(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype()))),&x,&y);
  gnt_widget_get_size(((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)tree),gnt_widget_get_gtype()))),&width,0);
  top = gnt_tree_get_selection_visible_line(tree);
  x += width;
  y += (top - 1);
  gnt_widget_set_position(context,x,y);
  gnt_screen_menu_show(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)context),gnt_menu_get_gtype()))));
  g_free(title);
}

static void tooltip_for_buddy(PurpleBuddy *buddy,GString *str,gboolean full)
{
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  PurpleAccount *account;
  PurpleNotifyUserInfo *user_info;
  PurplePresence *presence;
  const char *alias = purple_buddy_get_alias(buddy);
  char *tmp;
  char *strip;
  user_info = purple_notify_user_info_new();
  account = purple_buddy_get_account(buddy);
  presence = purple_buddy_get_presence(buddy);
  if (!(full != 0) || (g_utf8_collate(purple_buddy_get_name(buddy),alias) != 0)) {
    char *esc = g_markup_escape_text(alias,(-1));
    purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Nickname"))),esc);
    g_free(esc);
  }
  tmp = g_strdup_printf("%s (%s)",purple_account_get_username(account),purple_account_get_protocol_name(account));
  purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Account"))),tmp);
  g_free(tmp);
  prpl = purple_find_prpl(purple_account_get_protocol_id(account));
  prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if ((prpl_info != 0) && ((prpl_info -> tooltip_text) != 0)) {
    ( *(prpl_info -> tooltip_text))(buddy,user_info,full);
  }
  if (purple_prefs_get_bool("/finch/blist/idletime") != 0) {
    PurplePresence *pre = purple_buddy_get_presence(buddy);
    if (purple_presence_is_idle(pre) != 0) {
      time_t idle = purple_presence_get_idle_time(pre);
      if (idle > 0) {
        char *st = purple_str_seconds_to_string((time(0) - idle));
        purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Idle"))),st);
        g_free(st);
      }
    }
  }
  tmp = purple_notify_user_info_get_text_with_newline(user_info,"<BR>");
  purple_notify_user_info_destroy(user_info);
  strip = purple_markup_strip_html(tmp);
  g_string_append(str,strip);
  if (purple_presence_is_status_primitive_active(presence,PURPLE_STATUS_MOBILE) != 0) {
    g_string_append(str,"\n");
    g_string_append(str,((const char *)(dgettext("pidgin","On Mobile"))));
  }
  g_free(strip);
  g_free(tmp);
}

static GString *make_sure_text_fits(GString *string)
{
  int maxw = ((((stdscr != 0)?((stdscr -> _maxx) + 1) : -1)) - 3);
  char *str = gnt_util_onscreen_fit_string((string -> str),maxw);
  string = g_string_assign(string,str);
  g_free(str);
  return string;
}

static gboolean draw_tooltip_real(FinchBlist *ggblist)
{
  PurpleBlistNode *node;
  int x;
  int y;
  int top;
  int width;
  int w;
  int h;
  GString *str = (GString *)((void *)0);
  GntTree *tree;
  GntWidget *widget;
  GntWidget *box;
  GntWidget *tv;
  char *title = (char *)((void *)0);
  widget = (ggblist -> tree);
  tree = ((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)widget),gnt_tree_get_gtype())));
  if (!(gnt_widget_has_focus((ggblist -> tree)) != 0) || (((ggblist -> context) != 0) && !((( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> context)),gnt_widget_get_gtype())))).priv.flags & GNT_WIDGET_INVISIBLE) != 0U))) 
    return 0;
  if ((ggblist -> tooltip) != 0) {
/* XXX: Once we can properly redraw on expose events, this can be removed at the end
		 * to avoid the blinking*/
    remove_tooltip(ggblist);
  }
  node = (gnt_tree_get_selection_data(tree));
  if (!(node != 0)) 
    return 0;
  if (!(( *( *(ggblist -> manager)).create_tooltip)(node,&str,&title) != 0)) 
    return 0;
  gnt_widget_get_position(widget,&x,&y);
  gnt_widget_get_size(widget,&width,0);
  top = gnt_tree_get_selection_visible_line(tree);
  x += width;
  y += (top - 1);
  box = gnt_box_new(0,0);
  gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),1);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_NO_SHADOW;
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),title);
  str = make_sure_text_fits(str);
  gnt_util_get_text_bound((str -> str),&w,&h);
  h = ((1 > h)?1 : h);
  tv = gnt_text_view_new();
  gnt_widget_set_size(tv,(w + 1),h);
  gnt_text_view_set_flag(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gnt_text_view_get_gtype()))),GNT_TEXT_VIEW_NO_SCROLL);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_box_get_gtype()))),tv);
  if ((x + w) >= (((stdscr != 0)?((stdscr -> _maxx) + 1) : -1))) 
    x -= ((w + width) + 2);
  gnt_widget_set_position(box,x,y);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype())))).priv.flags &= (~GNT_WIDGET_CAN_TAKE_FOCUS);
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)box),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_TRANSIENT;
  gnt_widget_draw(box);
  gnt_text_view_append_text_with_flags(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gnt_text_view_get_gtype()))),(str -> str),GNT_TEXT_FLAG_NORMAL);
  gnt_text_view_scroll(((GntTextView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gnt_text_view_get_gtype()))),0);
  g_free(title);
  g_string_free(str,1);
  ggblist -> tooltip = box;
  ggblist -> tnode = node;
  gnt_widget_set_name((ggblist -> tooltip),"tooltip");
  return 0;
}

static void draw_tooltip(FinchBlist *ggblist)
{
/* When an account has signed off, it removes one buddy at a time.
	 * Drawing the tooltip after removing each buddy is expensive. On
	 * top of that, if the selected buddy belongs to the disconnected
	 * account, then retreiving the tooltip for that causes crash. So
	 * let's make sure we wait for all the buddies to be removed first.*/
  int id = (g_timeout_add(0,((GSourceFunc )draw_tooltip_real),ggblist));
  g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> window)),((GType )(20 << 2))))),"draw_tooltip_calback",((gpointer )((glong )id)),((GDestroyNotify )g_source_remove));
}

static void selection_changed(GntWidget *widget,gpointer old,gpointer current,FinchBlist *ggblist)
{
  remove_peripherals(ggblist);
  draw_tooltip(ggblist);
}

static gboolean context_menu(GntWidget *widget,FinchBlist *ggblist)
{
  draw_context_menu(ggblist);
  return 1;
}

static gboolean key_pressed(GntWidget *widget,const char *text,FinchBlist *ggblist)
{
  if ((text[0] == 27) && (text[1] == 0)) {
/* Escape was pressed */
    if (gnt_tree_is_searching(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype())))) != 0) 
      gnt_bindable_perform_action_named(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_bindable_get_gtype()))),"end-search",((void *)((void *)0)));
    remove_peripherals(ggblist);
  }
  else if (strcmp(text,(((cur_term -> type.Strings[77] != 0)?cur_term -> type.Strings[77] : ""))) == 0) {
    PurpleBlistNode *node = (gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype())))));
    purple_blist_request_add_buddy(0,0,(((node != 0) && ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE))?purple_group_get_name(((PurpleGroup *)node)) : ((const char *)((void *)0))),0);
  }
  else if (!(gnt_tree_is_searching(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype())))) != 0)) {
    if (strcmp(text,"t") == 0) {
      finch_blist_toggle_tag_buddy((gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))))));
      gnt_bindable_perform_action_named(((GntBindable *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_bindable_get_gtype()))),"move-down",((void *)((void *)0)));
    }
    else if (strcmp(text,"a") == 0) {
      finch_blist_place_tagged((gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))))));
    }
    else 
      return 0;
  }
  else 
    return 0;
  return 1;
}

static void update_node_display(PurpleBlistNode *node,FinchBlist *ggblist)
{
  GntTextFormatFlags flag = get_blist_node_flag(node);
  gnt_tree_set_row_flags(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),node,flag);
}

static void update_buddy_display(PurpleBuddy *buddy,FinchBlist *ggblist)
{
  PurpleContact *contact;
  contact = purple_buddy_get_contact(buddy);
  gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),buddy,0,get_display_name(((PurpleBlistNode *)buddy)));
  gnt_tree_change_text(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),contact,0,get_display_name(((PurpleBlistNode *)contact)));
  blist_update_row_flags(((PurpleBlistNode *)buddy));
  if (buddy == purple_contact_get_priority_buddy(contact)) 
    blist_update_row_flags(((PurpleBlistNode *)contact));
  if ((ggblist -> tnode) == ((PurpleBlistNode *)buddy)) 
    draw_tooltip(ggblist);
}

static void buddy_status_changed(PurpleBuddy *buddy,PurpleStatus *old,PurpleStatus *now,FinchBlist *ggblist)
{
  update_buddy_display(buddy,ggblist);
}

static void buddy_idle_changed(PurpleBuddy *buddy,int old,int new,FinchBlist *ggblist)
{
  update_buddy_display(buddy,ggblist);
}

static void remove_peripherals(FinchBlist *ggblist)
{
  if ((ggblist -> tooltip) != 0) 
    remove_tooltip(ggblist);
  else if ((ggblist -> context) != 0) 
    gnt_widget_destroy((ggblist -> context));
}

static void size_changed_cb(GntWidget *w,int wi,int h)
{
  int width;
  int height;
  gnt_widget_get_size(w,&width,&height);
  purple_prefs_set_int("/finch/blist/size/width",width);
  purple_prefs_set_int("/finch/blist/size/height",height);
}

static void save_position_cb(GntWidget *w,int x,int y)
{
  purple_prefs_set_int("/finch/blist/position/x",x);
  purple_prefs_set_int("/finch/blist/position/y",y);
}

static void reset_blist_window(GntWidget *window,gpointer null)
{
  PurpleBlistNode *node;
  purple_signals_disconnect_by_handle(finch_blist_get_handle());
  ( *purple_get_blist()).ui_data = ((void *)((void *)0));
  node = purple_blist_get_root();
  while(node != 0){
    reset_blist_node_ui_data(node);
    node = purple_blist_node_next(node,1);
  }
  if ((ggblist -> typing) != 0) 
    purple_timeout_remove((ggblist -> typing));
  remove_peripherals(ggblist);
  if ((ggblist -> tagged) != 0) 
    g_list_free((ggblist -> tagged));
  if ((ggblist -> new_group_timeout) != 0U) 
    purple_timeout_remove((ggblist -> new_group_timeout));
  if ((ggblist -> new_group) != 0) 
    g_list_free((ggblist -> new_group));
  g_free(ggblist);
  ggblist = ((FinchBlist *)((void *)0));
}

static void populate_buddylist()
{
  PurpleBlistNode *node;
  PurpleBuddyList *list;
  if (( *(ggblist -> manager)).init != 0) 
    ( *( *(ggblist -> manager)).init)();
  if (strcmp(purple_prefs_get_string("/finch/blist/sort_type"),"text") == 0) {
    gnt_tree_set_compare_func(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),((GCompareFunc )blist_node_compare_text));
  }
  else if (strcmp(purple_prefs_get_string("/finch/blist/sort_type"),"status") == 0) {
    gnt_tree_set_compare_func(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),((GCompareFunc )blist_node_compare_status));
  }
  else if (strcmp(purple_prefs_get_string("/finch/blist/sort_type"),"log") == 0) {
    gnt_tree_set_compare_func(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),((GCompareFunc )blist_node_compare_log));
  }
  list = purple_get_blist();
  node = purple_blist_get_root();
  while(node != 0){
    node_update(list,node);
    node = purple_blist_node_next(node,0);
  }
}

static void destroy_status_list(GList *list)
{
  g_list_foreach(list,((GFunc )g_free),0);
  g_list_free(list);
}

static void populate_status_dropdown()
{
  int i;
  GList *iter;
  GList *items = (GList *)((void *)0);
  StatusBoxItem *item = (StatusBoxItem *)((void *)0);
/* First the primitives */
  PurpleStatusPrimitive prims[] = {(PURPLE_STATUS_AVAILABLE), (PURPLE_STATUS_AWAY), (PURPLE_STATUS_INVISIBLE), (PURPLE_STATUS_OFFLINE), (PURPLE_STATUS_UNSET)};
  gnt_combo_box_remove_all(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> status)),gnt_combo_box_get_gtype()))));
  for (i = 0; prims[i] != PURPLE_STATUS_UNSET; i++) {
    item = ((StatusBoxItem *)(g_malloc0_n(1,(sizeof(StatusBoxItem )))));
    item -> type = STATUS_PRIMITIVE;
    item -> u.prim = prims[i];
    items = g_list_prepend(items,item);
    gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> status)),gnt_combo_box_get_gtype()))),item,purple_primitive_get_name_from_type(prims[i]));
  }
/* Now the popular statuses */
  for (iter = purple_savedstatuses_get_popular(6); iter != 0; iter = g_list_delete_link(iter,iter)) {
    item = ((StatusBoxItem *)(g_malloc0_n(1,(sizeof(StatusBoxItem )))));
    item -> type = STATUS_SAVED_POPULAR;
    item -> u.saved = (iter -> data);
    items = g_list_prepend(items,item);
    gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> status)),gnt_combo_box_get_gtype()))),item,purple_savedstatus_get_title((iter -> data)));
  }
/* New savedstatus */
  item = ((StatusBoxItem *)(g_malloc0_n(1,(sizeof(StatusBoxItem )))));
  item -> type = STATUS_SAVED_NEW;
  items = g_list_prepend(items,item);
  gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> status)),gnt_combo_box_get_gtype()))),item,((const char *)(dgettext("pidgin","New..."))));
/* More savedstatuses */
  item = ((StatusBoxItem *)(g_malloc0_n(1,(sizeof(StatusBoxItem )))));
  item -> type = STATUS_SAVED_ALL;
  items = g_list_prepend(items,item);
  gnt_combo_box_add_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> status)),gnt_combo_box_get_gtype()))),item,((const char *)(dgettext("pidgin","Saved..."))));
/* The keys for the combobox are created here, and never used
	 * anywhere else. So make sure the keys are freed when the widget
	 * is destroyed. */
  g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> status)),((GType )(20 << 2))))),"list of statuses",items,((GDestroyNotify )destroy_status_list));
}

static void redraw_blist(const char *name,PurplePrefType type,gconstpointer val,gpointer data)
{
  PurpleBlistNode *node;
  PurpleBlistNode *sel;
  FinchBlistManager *manager;
  if (ggblist == ((FinchBlist *)((void *)0))) 
    return ;
  manager = finch_blist_manager_find(purple_prefs_get_string("/finch/blist/grouping"));
  if (manager == ((FinchBlistManager *)((void *)0))) 
    manager = &default_manager;
  if ((ggblist -> manager) != manager) {
    if (( *(ggblist -> manager)).uninit != 0) 
      ( *( *(ggblist -> manager)).uninit)();
    ggblist -> manager = manager;
    if ((manager -> can_add_node) == ((gboolean (*)(PurpleBlistNode *))((void *)0))) 
      manager -> can_add_node = default_can_add_node;
    if ((manager -> find_parent) == ((gpointer (*)(PurpleBlistNode *))((void *)0))) 
      manager -> find_parent = default_find_parent;
    if ((manager -> create_tooltip) == ((gboolean (*)(gpointer , GString **, char **))((void *)0))) 
      manager -> create_tooltip = default_create_tooltip;
  }
  if ((ggblist -> window) == ((GntWidget *)((void *)0))) 
    return ;
  sel = (gnt_tree_get_selection_data(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype())))));
  gnt_tree_remove_all(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))));
  node = purple_blist_get_root();
  for (; node != 0; node = purple_blist_node_next(node,1)) 
    reset_blist_node_ui_data(node);
  populate_buddylist();
  gnt_tree_set_selected(((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))),sel);
  draw_tooltip(ggblist);
}

void finch_blist_init()
{
  color_available = gnt_style_get_color(0,"color-available");
  if (!(color_available != 0)) 
    color_available = gnt_color_add_pair(2,-1);
  color_away = gnt_style_get_color(0,"color-away");
  if (!(color_away != 0)) 
    color_away = gnt_color_add_pair(4,-1);
  color_idle = gnt_style_get_color(0,"color-idle");
  if (!(color_idle != 0)) 
    color_idle = gnt_color_add_pair(6,-1);
  color_offline = gnt_style_get_color(0,"color-offline");
  if (!(color_offline != 0)) 
    color_offline = gnt_color_add_pair(1,-1);
  purple_prefs_add_none("/finch/blist");
  purple_prefs_add_none("/finch/blist/size");
  purple_prefs_add_int("/finch/blist/size/width",20);
  purple_prefs_add_int("/finch/blist/size/height",17);
  purple_prefs_add_none("/finch/blist/position");
  purple_prefs_add_int("/finch/blist/position/x",0);
  purple_prefs_add_int("/finch/blist/position/y",0);
  purple_prefs_add_bool("/finch/blist/idletime",1);
  purple_prefs_add_bool("/finch/blist/showoffline",0);
  purple_prefs_add_bool("/finch/blist/emptygroups",0);
  purple_prefs_add_string("/finch/blist/sort_type","text");
  purple_prefs_add_string("/finch/blist/grouping","default");
  purple_prefs_connect_callback(finch_blist_get_handle(),"/finch/blist/emptygroups",redraw_blist,0);
  purple_prefs_connect_callback(finch_blist_get_handle(),"/finch/blist/showoffline",redraw_blist,0);
  purple_prefs_connect_callback(finch_blist_get_handle(),"/finch/blist/sort_type",redraw_blist,0);
  purple_prefs_connect_callback(finch_blist_get_handle(),"/finch/blist/grouping",redraw_blist,0);
  purple_signal_connect_priority(purple_connections_get_handle(),"autojoin",purple_blist_get_handle(),((GCallback )account_autojoin_cb),0,9999);
  finch_blist_install_manager((&default_manager));
}

static gboolean remove_typing_cb(gpointer null)
{
  PurpleSavedStatus *current;
  const char *message;
  const char *newmessage;
  char *escnewmessage;
  PurpleStatusPrimitive prim;
  PurpleStatusPrimitive newprim;
  StatusBoxItem *item;
  current = purple_savedstatus_get_current();
  message = purple_savedstatus_get_message(current);
  prim = purple_savedstatus_get_type(current);
  newmessage = gnt_entry_get_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> statustext)),gnt_entry_get_gtype()))));
  item = (gnt_combo_box_get_selected_data(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> status)),gnt_combo_box_get_gtype())))));
  escnewmessage = ((newmessage != 0)?g_markup_escape_text(newmessage,(-1)) : ((char *)((void *)0)));
  switch(item -> type){
    case STATUS_PRIMITIVE:
{
      newprim = item -> u.prim;
      break; 
    }
    case STATUS_SAVED_POPULAR:
{
      newprim = purple_savedstatus_get_type(item -> u.saved);
      break; 
    }
    default:
{
/* 'New' or 'Saved' is selected, but this should never happen. */
      goto end;
    }
  }
  if ((newprim != prim) || ((((message != 0) && !(escnewmessage != 0)) || (!(message != 0) && (escnewmessage != 0))) || (((message != 0) && (escnewmessage != 0)) && (g_utf8_collate(message,escnewmessage) != 0)))) {
    PurpleSavedStatus *status = purple_savedstatus_find_transient_by_type_and_message(newprim,escnewmessage);
/* Holy Crap! That's a LAWNG function name */
    if (status == ((PurpleSavedStatus *)((void *)0))) {
      status = purple_savedstatus_new(0,newprim);
      purple_savedstatus_set_message(status,escnewmessage);
    }
    purple_savedstatus_activate(status);
  }
  gnt_box_give_focus_to_child(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> window)),gnt_box_get_gtype()))),(ggblist -> tree));
  end:
  g_free(escnewmessage);
  if ((ggblist -> typing) != 0) 
    purple_timeout_remove((ggblist -> typing));
  ggblist -> typing = 0;
  return 0;
}

static void status_selection_changed(GntComboBox *box,StatusBoxItem *old,StatusBoxItem *now,gpointer null)
{
  gnt_entry_set_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> statustext)),gnt_entry_get_gtype()))),0);
  if ((now -> type) == STATUS_SAVED_POPULAR) {
/* Set the status immediately */
    purple_savedstatus_activate(now -> u.saved);
  }
  else if ((now -> type) == STATUS_PRIMITIVE) {
/* Move the focus to the entry box */
/* XXX: Make sure the selected status can have a message */
    gnt_box_move_focus(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> window)),gnt_box_get_gtype()))),1);
    ggblist -> typing = (purple_timeout_add_seconds(4,((GSourceFunc )remove_typing_cb),0));
  }
  else if ((now -> type) == STATUS_SAVED_ALL) {
/* Restore the selection to reflect current status. */
    savedstatus_changed(purple_savedstatus_get_current(),0);
    gnt_box_give_focus_to_child(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> window)),gnt_box_get_gtype()))),(ggblist -> tree));
    finch_savedstatus_show_all();
  }
  else if ((now -> type) == STATUS_SAVED_NEW) {
    savedstatus_changed(purple_savedstatus_get_current(),0);
    gnt_box_give_focus_to_child(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> window)),gnt_box_get_gtype()))),(ggblist -> tree));
    finch_savedstatus_edit(0);
  }
  else 
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","gntblist.c",2314,((const char *)__func__));
      return ;
    }while (0);
}

static gboolean status_text_changed(GntEntry *entry,const char *text,gpointer null)
{
  if (((text[0] == 27) || ((text[0] == 9) && (text[1] == 0))) && ((ggblist -> typing) == 0)) 
    return 0;
  if ((ggblist -> typing) != 0) 
    purple_timeout_remove((ggblist -> typing));
  ggblist -> typing = 0;
  if ((text[0] == 13) && (text[1] == 0)) {
/* Set the status only after you press 'Enter' */
    remove_typing_cb(0);
    return 1;
  }
  ggblist -> typing = (purple_timeout_add_seconds(4,((GSourceFunc )remove_typing_cb),0));
  return 0;
}

static void savedstatus_changed(PurpleSavedStatus *now,PurpleSavedStatus *old)
{
  GList *list;
  PurpleStatusPrimitive prim;
  const char *message;
  gboolean found = 0;
  gboolean saved = 1;
  if (!(ggblist != 0)) 
    return ;
/* Block the signals we don't want to emit */
  g_signal_handlers_block_matched((ggblist -> status),G_SIGNAL_MATCH_FUNC,0,0,0,status_selection_changed,0);
  g_signal_handlers_block_matched((ggblist -> statustext),G_SIGNAL_MATCH_FUNC,0,0,0,status_text_changed,0);
  prim = purple_savedstatus_get_type(now);
  message = purple_savedstatus_get_message(now);
/* Rebuild the status dropdown */
  populate_status_dropdown();
{
    while(!(found != 0)){
      list = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> status)),((GType )(20 << 2))))),"list of statuses"));
{
        for (; list != 0; list = (list -> next)) {
          StatusBoxItem *item = (list -> data);
          if ((((saved != 0) && ((item -> type) != STATUS_PRIMITIVE)) && (item -> u.saved == now)) || ((!(saved != 0) && ((item -> type) == STATUS_PRIMITIVE)) && (item -> u.prim == prim))) {
            char *mess = purple_unescape_html(message);
            gnt_combo_box_set_selected(((GntComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> status)),gnt_combo_box_get_gtype()))),item);
            gnt_entry_set_text(((GntEntry *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> statustext)),gnt_entry_get_gtype()))),mess);
            gnt_widget_draw((ggblist -> status));
            g_free(mess);
            found = 1;
            break; 
          }
        }
      }
      if (!(saved != 0)) 
        break; 
      saved = 0;
    }
  }
  g_signal_handlers_unblock_matched((ggblist -> status),G_SIGNAL_MATCH_FUNC,0,0,0,status_selection_changed,0);
  g_signal_handlers_unblock_matched((ggblist -> statustext),G_SIGNAL_MATCH_FUNC,0,0,0,status_text_changed,0);
}

static int blist_node_compare_position(PurpleBlistNode *n1,PurpleBlistNode *n2)
{
  while((n1 = purple_blist_node_get_sibling_prev(n1)) != ((PurpleBlistNode *)((void *)0)))
    if (n1 == n2) 
      return 1;
  return -1;
}

static int blist_node_compare_text(PurpleBlistNode *n1,PurpleBlistNode *n2)
{
  const char *s1;
  const char *s2;
  char *us1;
  char *us2;
  int ret;
  if ((purple_blist_node_get_type(n1)) != (purple_blist_node_get_type(n2))) 
    return blist_node_compare_position(n1,n2);
  switch(purple_blist_node_get_type(n1)){
    case PURPLE_BLIST_CHAT_NODE:
{
      s1 = purple_chat_get_name(((PurpleChat *)n1));
      s2 = purple_chat_get_name(((PurpleChat *)n2));
      break; 
    }
    case PURPLE_BLIST_BUDDY_NODE:
{
      return purple_presence_compare((purple_buddy_get_presence(((PurpleBuddy *)n1))),(purple_buddy_get_presence(((PurpleBuddy *)n2))));
      break; 
    }
    case PURPLE_BLIST_CONTACT_NODE:
{
      s1 = purple_contact_get_alias(((PurpleContact *)n1));
      s2 = purple_contact_get_alias(((PurpleContact *)n2));
      break; 
    }
    default:
{
      return blist_node_compare_position(n1,n2);
    }
  }
  us1 = g_utf8_strup(s1,(-1));
  us2 = g_utf8_strup(s2,(-1));
  ret = g_utf8_collate(us1,us2);
  g_free(us1);
  g_free(us2);
  return ret;
}

static int blist_node_compare_status(PurpleBlistNode *n1,PurpleBlistNode *n2)
{
  int ret;
  if ((purple_blist_node_get_type(n1)) != (purple_blist_node_get_type(n2))) 
    return blist_node_compare_position(n1,n2);
  switch(purple_blist_node_get_type(n1)){
    case PURPLE_BLIST_CONTACT_NODE:
{
      n1 = ((PurpleBlistNode *)(purple_contact_get_priority_buddy(((PurpleContact *)n1))));
      n2 = ((PurpleBlistNode *)(purple_contact_get_priority_buddy(((PurpleContact *)n2))));
    }
/* now compare the presence of the priority buddies */
    case PURPLE_BLIST_BUDDY_NODE:
{
      ret = purple_presence_compare((purple_buddy_get_presence(((PurpleBuddy *)n1))),(purple_buddy_get_presence(((PurpleBuddy *)n2))));
      if (ret != 0) 
        return ret;
      break; 
    }
    default:
{
      return blist_node_compare_position(n1,n2);
      break; 
    }
  }
/* Sort alphabetically if presence is not comparable */
  ret = blist_node_compare_text(n1,n2);
  return ret;
}

static int get_contact_log_size(PurpleBlistNode *c)
{
  int log = 0;
  PurpleBlistNode *node;
  for (node = purple_blist_node_get_first_child(c); node != 0; node = purple_blist_node_get_sibling_next(node)) {
    PurpleBuddy *b = (PurpleBuddy *)node;
    log += purple_log_get_total_size(PURPLE_LOG_IM,purple_buddy_get_name(b),purple_buddy_get_account(b));
  }
  return log;
}

static int blist_node_compare_log(PurpleBlistNode *n1,PurpleBlistNode *n2)
{
  int ret;
  PurpleBuddy *b1;
  PurpleBuddy *b2;
  if ((purple_blist_node_get_type(n1)) != (purple_blist_node_get_type(n2))) 
    return blist_node_compare_position(n1,n2);
  switch(purple_blist_node_get_type(n1)){
    case PURPLE_BLIST_BUDDY_NODE:
{
      b1 = ((PurpleBuddy *)n1);
      b2 = ((PurpleBuddy *)n2);
      ret = (purple_log_get_total_size(PURPLE_LOG_IM,purple_buddy_get_name(b2),purple_buddy_get_account(b2)) - purple_log_get_total_size(PURPLE_LOG_IM,purple_buddy_get_name(b1),purple_buddy_get_account(b1)));
      if (ret != 0) 
        return ret;
      break; 
    }
    case PURPLE_BLIST_CONTACT_NODE:
{
      ret = (get_contact_log_size(n2) - get_contact_log_size(n1));
      if (ret != 0) 
        return ret;
      break; 
    }
    default:
{
      return blist_node_compare_position(n1,n2);
    }
  }
  ret = blist_node_compare_text(n1,n2);
  return ret;
}

static void plugin_action(GntMenuItem *item,gpointer data)
{
  PurplePluginAction *action = data;
  if ((action != 0) && ((action -> callback) != 0)) 
    ( *(action -> callback))(action);
}

static void build_plugin_actions(GntMenuItem *item,PurplePlugin *plugin,gpointer context)
{
  GntWidget *sub = gnt_menu_new(GNT_MENU_POPUP);
  GList *actions;
  GntMenuItem *menuitem;
  gnt_menuitem_set_submenu(item,((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))));
  for (actions = ((((plugin -> info) != ((PurplePluginInfo *)((void *)0))) && (( *(plugin -> info)).actions != ((GList *(*)(PurplePlugin *, gpointer ))((void *)0))))?( *( *(plugin -> info)).actions)(plugin,context) : ((struct _GList *)((void *)0))); actions != 0; actions = g_list_delete_link(actions,actions)) {
    if ((actions -> data) != 0) {
      PurplePluginAction *action = (actions -> data);
      action -> plugin = plugin;
      action -> context = context;
      menuitem = gnt_menuitem_new((action -> label));
      gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),menuitem);
      gnt_menuitem_set_callback(menuitem,plugin_action,action);
      g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"plugin_action",action,((GDestroyNotify )purple_plugin_action_free));
    }
  }
}

static gboolean buddy_recent_signed_on_off(gpointer data)
{
  PurpleBlistNode *node = data;
  FinchBlistNode *fnode = (node -> ui_data);
  purple_timeout_remove((fnode -> signed_timer));
  fnode -> signed_timer = 0;
  if (!(( *( *(ggblist -> manager)).can_add_node)(node) != 0)) {
    node_remove(purple_get_blist(),node);
  }
  else {
    update_node_display(node,ggblist);
    if ((purple_blist_node_get_parent(node) != 0) && ((purple_blist_node_get_type(purple_blist_node_get_parent(node))) == PURPLE_BLIST_CONTACT_NODE)) 
      update_node_display(purple_blist_node_get_parent(node),ggblist);
  }
  return 0;
}

static gboolean buddy_signed_on_off_cb(gpointer data)
{
  PurpleBlistNode *node = data;
  FinchBlistNode *fnode = (node -> ui_data);
  if (!(ggblist != 0) || !(fnode != 0)) 
    return 0;
  if ((fnode -> signed_timer) != 0U) 
    purple_timeout_remove((fnode -> signed_timer));
  fnode -> signed_timer = purple_timeout_add_seconds(6,((GSourceFunc )buddy_recent_signed_on_off),data);
  update_node_display(node,ggblist);
  if ((purple_blist_node_get_parent(node) != 0) && ((purple_blist_node_get_type(purple_blist_node_get_parent(node))) == PURPLE_BLIST_CONTACT_NODE)) 
    update_node_display(purple_blist_node_get_parent(node),ggblist);
  return 0;
}

static void buddy_signed_on_off(PurpleBuddy *buddy,gpointer null)
{
  g_idle_add(buddy_signed_on_off_cb,buddy);
}

static void reconstruct_plugins_menu()
{
  GntWidget *sub;
  GntMenuItem *plg;
  GList *iter;
  if (!(ggblist != 0)) 
    return ;
  if ((ggblist -> plugins) == ((GntMenuItem *)((void *)0))) 
    ggblist -> plugins = gnt_menuitem_new(((const char *)(dgettext("pidgin","Plugins"))));
  plg = (ggblist -> plugins);
  sub = gnt_menu_new(GNT_MENU_POPUP);
  gnt_menuitem_set_submenu(plg,((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))));
  for (iter = purple_plugins_get_loaded(); iter != 0; iter = (iter -> next)) {{
      PurplePlugin *plugin = (iter -> data);
      GntMenuItem *item;
      if (( *(plugin -> info)).type == PURPLE_PLUGIN_PROTOCOL) 
        continue; 
      if (!(((plugin -> info) != ((PurplePluginInfo *)((void *)0))) && (( *(plugin -> info)).actions != ((GList *(*)(PurplePlugin *, gpointer ))((void *)0))))) 
        continue; 
      item = gnt_menuitem_new(((const char *)(dgettext("pidgin",( *(plugin -> info)).name))));
      gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
      build_plugin_actions(item,plugin,0);
    }
  }
}

static void reconstruct_accounts_menu()
{
  GntWidget *sub;
  GntMenuItem *acc;
  GntMenuItem *item;
  GList *iter;
  if (!(ggblist != 0)) 
    return ;
  if ((ggblist -> accounts) == ((GntMenuItem *)((void *)0))) 
    ggblist -> accounts = gnt_menuitem_new(((const char *)(dgettext("pidgin","Accounts"))));
  acc = (ggblist -> accounts);
  sub = gnt_menu_new(GNT_MENU_POPUP);
  gnt_menuitem_set_submenu(acc,((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))));
  for (iter = purple_accounts_get_all_active(); iter != 0; iter = g_list_delete_link(iter,iter)) {{
      PurpleAccount *account = (iter -> data);
      PurpleConnection *gc = purple_account_get_connection(account);
      PurplePlugin *prpl;
      if (!(gc != 0) || !((purple_connection_get_state(gc)) == PURPLE_CONNECTED)) 
        continue; 
      prpl = purple_connection_get_prpl(gc);
      if (((prpl -> info) != ((PurplePluginInfo *)((void *)0))) && (( *(prpl -> info)).actions != ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)))) {
        item = gnt_menuitem_new(purple_account_get_username(account));
        gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
        build_plugin_actions(item,prpl,gc);
      }
    }
  }
}

static void reconstruct_grouping_menu()
{
  GList *iter;
  GntWidget *subsub;
  if (!(ggblist != 0) || !((ggblist -> grouping) != 0)) 
    return ;
  subsub = gnt_menu_new(GNT_MENU_POPUP);
  gnt_menuitem_set_submenu((ggblist -> grouping),((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)subsub),gnt_menu_get_gtype()))));
  for (iter = managers; iter != 0; iter = (iter -> next)) {
    char menuid[128UL];
    FinchBlistManager *manager = (iter -> data);
    GntMenuItem *item = gnt_menuitem_new(((const char *)(dgettext("pidgin",(manager -> name)))));
    g_snprintf(menuid,(sizeof(menuid)),"grouping-%s",(manager -> id));
    gnt_menuitem_set_id(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),menuid);
    gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)subsub),gnt_menu_get_gtype()))),item);
    g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"grouping-id",(g_strdup((manager -> id))),g_free);
    gnt_menuitem_set_callback(item,menu_group_set_cb,0);
  }
}

static gboolean auto_join_chats(gpointer data)
{
  PurpleBlistNode *node;
  PurpleConnection *pc = data;
  PurpleAccount *account = purple_connection_get_account(pc);
  for (node = purple_blist_get_root(); node != 0; node = purple_blist_node_next(node,0)) {
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
      PurpleChat *chat = (PurpleChat *)node;
      if ((purple_chat_get_account(chat) == account) && (purple_blist_node_get_bool(node,"gnt-autojoin") != 0)) 
        serv_join_chat(purple_account_get_connection(account),purple_chat_get_components(chat));
    }
  }
  return 0;
}

static gboolean account_autojoin_cb(PurpleConnection *gc,gpointer null)
{
  g_idle_add(auto_join_chats,gc);
  return 1;
}

static void toggle_pref_cb(GntMenuItem *item,gpointer n)
{
  purple_prefs_set_bool(n,!(purple_prefs_get_bool(n) != 0));
}

static void sort_blist_change_cb(GntMenuItem *item,gpointer n)
{
  purple_prefs_set_string("/finch/blist/sort_type",n);
}

static void block_select_cb(gpointer data,PurpleRequestFields *fields)
{
  PurpleAccount *account = purple_request_fields_get_account(fields,"account");
  const char *name = purple_request_fields_get_string(fields,"screenname");
  if (((account != 0) && (name != 0)) && (( *name) != 0)) {
    if (purple_request_fields_get_choice(fields,"block") == 1) {
      purple_privacy_deny(account,name,0,0);
    }
    else {
      purple_privacy_allow(account,name,0,0);
    }
  }
}

static void block_select(GntMenuItem *item,gpointer n)
{
  PurpleRequestFields *fields;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  fields = purple_request_fields_new();
  group = purple_request_field_group_new(0);
  purple_request_fields_add_group(fields,group);
  field = purple_request_field_string_new("screenname",((const char *)(dgettext("pidgin","Name"))),0,0);
  purple_request_field_set_type_hint(field,"screenname");
  purple_request_field_set_required(field,1);
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_account_new("account",((const char *)(dgettext("pidgin","Account"))),0);
  purple_request_field_set_type_hint(field,"account");
  purple_request_field_set_visible(field,((purple_connections_get_all() != ((GList *)((void *)0))) && (( *purple_connections_get_all()).next != ((GList *)((void *)0)))));
  purple_request_field_set_required(field,1);
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_choice_new("block",((const char *)(dgettext("pidgin","Block/Unblock"))),1);
  purple_request_field_choice_add(field,((const char *)(dgettext("pidgin","Block"))));
  purple_request_field_choice_add(field,((const char *)(dgettext("pidgin","Unblock"))));
  purple_request_field_group_add_field(group,field);
  purple_request_fields((purple_get_blist()),((const char *)(dgettext("pidgin","Block/Unblock"))),0,((const char *)(dgettext("pidgin","Please enter the username or alias of the person you would like to Block/Unblock."))),fields,((const char *)(dgettext("pidgin","OK"))),((GCallback )block_select_cb),((const char *)(dgettext("pidgin","Cancel"))),0,0,0,0,0);
}
/* send_im_select* -- Xerox */

static void send_im_select_cb(gpointer data,PurpleRequestFields *fields)
{
  PurpleAccount *account;
  const char *username;
  PurpleConversation *conv;
  account = purple_request_fields_get_account(fields,"account");
  username = purple_request_fields_get_string(fields,"screenname");
  conv = purple_conversation_new(PURPLE_CONV_TYPE_IM,account,username);
  purple_conversation_present(conv);
}

static void send_im_select(GntMenuItem *item,gpointer n)
{
  PurpleRequestFields *fields;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  fields = purple_request_fields_new();
  group = purple_request_field_group_new(0);
  purple_request_fields_add_group(fields,group);
  field = purple_request_field_string_new("screenname",((const char *)(dgettext("pidgin","Name"))),0,0);
  purple_request_field_set_type_hint(field,"screenname");
  purple_request_field_set_required(field,1);
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_account_new("account",((const char *)(dgettext("pidgin","Account"))),0);
  purple_request_field_set_type_hint(field,"account");
  purple_request_field_set_visible(field,((purple_connections_get_all() != ((GList *)((void *)0))) && (( *purple_connections_get_all()).next != ((GList *)((void *)0)))));
  purple_request_field_set_required(field,1);
  purple_request_field_group_add_field(group,field);
  purple_request_fields((purple_get_blist()),((const char *)(dgettext("pidgin","New Instant Message"))),0,((const char *)(dgettext("pidgin","Please enter the username or alias of the person you would like to IM."))),fields,((const char *)(dgettext("pidgin","OK"))),((GCallback )send_im_select_cb),((const char *)(dgettext("pidgin","Cancel"))),0,0,0,0,0);
}

static void join_chat_select_cb(gpointer data,PurpleRequestFields *fields)
{
  PurpleAccount *account;
  const char *name;
  PurpleConnection *gc;
  PurpleChat *chat;
  GHashTable *hash = (GHashTable *)((void *)0);
  PurpleConversation *conv;
  account = purple_request_fields_get_account(fields,"account");
  name = purple_request_fields_get_string(fields,"chat");
  if (!(purple_account_is_connected(account) != 0)) 
    return ;
  gc = purple_account_get_connection(account);
/* Create a new conversation now. This will give focus to the new window.
	 * But it's necessary to pretend that we left the chat, because otherwise
	 * a new conversation window will pop up when we finally join the chat. */
  if (!((conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,name,account)) != 0)) {
    conv = purple_conversation_new(PURPLE_CONV_TYPE_CHAT,account,name);
    purple_conv_chat_left(purple_conversation_get_chat_data(conv));
  }
  else {
    purple_conversation_present(conv);
  }
  chat = purple_blist_find_chat(account,name);
  if (chat == ((PurpleChat *)((void *)0))) {
    PurplePluginProtocolInfo *info = (PurplePluginProtocolInfo *)( *( *purple_connection_get_prpl(gc)).info).extra_info;
    if ((info -> chat_info_defaults) != ((GHashTable *(*)(PurpleConnection *, const char *))((void *)0))) 
      hash = ( *(info -> chat_info_defaults))(gc,name);
  }
  else {
    hash = purple_chat_get_components(chat);
  }
  serv_join_chat(gc,hash);
  if ((chat == ((PurpleChat *)((void *)0))) && (hash != ((GHashTable *)((void *)0)))) 
    g_hash_table_destroy(hash);
}

static void join_chat_select(GntMenuItem *item,gpointer n)
{
  PurpleRequestFields *fields;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  fields = purple_request_fields_new();
  group = purple_request_field_group_new(0);
  purple_request_fields_add_group(fields,group);
  field = purple_request_field_string_new("chat",((const char *)(dgettext("pidgin","Channel"))),0,0);
  purple_request_field_set_required(field,1);
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_account_new("account",((const char *)(dgettext("pidgin","Account"))),0);
  purple_request_field_set_type_hint(field,"account");
  purple_request_field_set_visible(field,((purple_connections_get_all() != ((GList *)((void *)0))) && (( *purple_connections_get_all()).next != ((GList *)((void *)0)))));
  purple_request_field_set_required(field,1);
  purple_request_field_group_add_field(group,field);
  purple_request_fields((purple_get_blist()),((const char *)(dgettext("pidgin","Join a Chat"))),0,((const char *)(dgettext("pidgin","Please enter the name of the chat you want to join."))),fields,((const char *)(dgettext("pidgin","Join"))),((GCallback )join_chat_select_cb),((const char *)(dgettext("pidgin","Cancel"))),0,0,0,0,0);
}

static void view_log_select_cb(gpointer data,PurpleRequestFields *fields)
{
  PurpleAccount *account;
  const char *name;
  PurpleBuddy *buddy;
  PurpleContact *contact;
  account = purple_request_fields_get_account(fields,"account");
  name = purple_request_fields_get_string(fields,"screenname");
  buddy = purple_find_buddy(account,name);
  if (buddy != 0) {
    contact = purple_buddy_get_contact(buddy);
  }
  else {
    contact = ((PurpleContact *)((void *)0));
  }
  if (contact != 0) {
    finch_log_show_contact(contact);
  }
  else {
    finch_log_show(PURPLE_LOG_IM,name,account);
  }
}

static void view_log_cb(GntMenuItem *item,gpointer n)
{
  PurpleRequestFields *fields;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  fields = purple_request_fields_new();
  group = purple_request_field_group_new(0);
  purple_request_fields_add_group(fields,group);
  field = purple_request_field_string_new("screenname",((const char *)(dgettext("pidgin","Name"))),0,0);
  purple_request_field_set_type_hint(field,"screenname-all");
  purple_request_field_set_required(field,1);
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_account_new("account",((const char *)(dgettext("pidgin","Account"))),0);
  purple_request_field_set_type_hint(field,"account");
  purple_request_field_set_visible(field,((purple_accounts_get_all() != ((GList *)((void *)0))) && (( *purple_accounts_get_all()).next != ((GList *)((void *)0)))));
  purple_request_field_set_required(field,1);
  purple_request_field_group_add_field(group,field);
  purple_request_field_account_set_show_all(field,1);
  purple_request_fields((purple_get_blist()),((const char *)(dgettext("pidgin","View Log"))),0,((const char *)(dgettext("pidgin","Please enter the username or alias of the person whose log you would like to view."))),fields,((const char *)(dgettext("pidgin","OK"))),((GCallback )view_log_select_cb),((const char *)(dgettext("pidgin","Cancel"))),0,0,0,0,0);
}

static void view_all_logs_cb(GntMenuItem *item,gpointer n)
{
  finch_log_show(PURPLE_LOG_IM,0,0);
}

static void menu_add_buddy_cb(GntMenuItem *item,gpointer null)
{
  purple_blist_request_add_buddy(0,0,0,0);
}

static void menu_add_chat_cb(GntMenuItem *item,gpointer null)
{
  purple_blist_request_add_chat(0,0,0,0);
}

static void menu_add_group_cb(GntMenuItem *item,gpointer null)
{
  purple_blist_request_add_group();
}

static void menu_group_set_cb(GntMenuItem *item,gpointer null)
{
  const char *id = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"grouping-id"));
  purple_prefs_set_string("/finch/blist/grouping",id);
}

static void create_menu()
{
  GntWidget *menu;
  GntWidget *sub;
  GntWidget *subsub;
  GntMenuItem *item;
  GntWindow *window;
  if (!(ggblist != 0)) 
    return ;
  window = ((GntWindow *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> window)),gnt_window_get_gtype())));
  ggblist -> menu = (menu = gnt_menu_new(GNT_MENU_TOPLEVEL));
  gnt_window_set_menu(window,((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_menu_get_gtype()))));
  item = gnt_menuitem_new(((const char *)(dgettext("pidgin","Options"))));
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_menu_get_gtype()))),item);
  sub = gnt_menu_new(GNT_MENU_POPUP);
  gnt_menuitem_set_submenu(item,((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))));
  item = gnt_menuitem_new(((const char *)(dgettext("pidgin","Send IM..."))));
  gnt_menuitem_set_id(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),"send-im");
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
  gnt_menuitem_set_callback(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),send_im_select,0);
  item = gnt_menuitem_new(((const char *)(dgettext("pidgin","Block/Unblock..."))));
  gnt_menuitem_set_id(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),"block-unblock");
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
  gnt_menuitem_set_callback(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),block_select,0);
  item = gnt_menuitem_new(((const char *)(dgettext("pidgin","Join Chat..."))));
  gnt_menuitem_set_id(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),"join-chat");
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
  gnt_menuitem_set_callback(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),join_chat_select,0);
  item = gnt_menuitem_new(((const char *)(dgettext("pidgin","View Log..."))));
  gnt_menuitem_set_id(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),"view-log");
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
  gnt_menuitem_set_callback(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),view_log_cb,0);
  item = gnt_menuitem_new(((const char *)(dgettext("pidgin","View All Logs"))));
  gnt_menuitem_set_id(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),"view-all-logs");
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
  gnt_menuitem_set_callback(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),view_all_logs_cb,0);
  item = gnt_menuitem_new(((const char *)(dgettext("pidgin","Show"))));
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
  subsub = gnt_menu_new(GNT_MENU_POPUP);
  gnt_menuitem_set_submenu(item,((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)subsub),gnt_menu_get_gtype()))));
  item = gnt_menuitem_check_new(((const char *)(dgettext("pidgin","Empty groups"))));
  gnt_menuitem_set_id(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),"show-empty-groups");
  gnt_menuitem_check_set_checked(((GntMenuItemCheck *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_check_get_gtype()))),purple_prefs_get_bool("/finch/blist/emptygroups"));
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)subsub),gnt_menu_get_gtype()))),item);
  gnt_menuitem_set_callback(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),toggle_pref_cb,"/finch/blist/emptygroups");
  item = gnt_menuitem_check_new(((const char *)(dgettext("pidgin","Offline buddies"))));
  gnt_menuitem_set_id(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),"show-offline-buddies");
  gnt_menuitem_check_set_checked(((GntMenuItemCheck *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_check_get_gtype()))),purple_prefs_get_bool("/finch/blist/showoffline"));
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)subsub),gnt_menu_get_gtype()))),item);
  gnt_menuitem_set_callback(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),toggle_pref_cb,"/finch/blist/showoffline");
  item = gnt_menuitem_new(((const char *)(dgettext("pidgin","Sort"))));
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
  subsub = gnt_menu_new(GNT_MENU_POPUP);
  gnt_menuitem_set_submenu(item,((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)subsub),gnt_menu_get_gtype()))));
  item = gnt_menuitem_new(((const char *)(dgettext("pidgin","By Status"))));
  gnt_menuitem_set_id(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),"sort-status");
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)subsub),gnt_menu_get_gtype()))),item);
  gnt_menuitem_set_callback(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),sort_blist_change_cb,"status");
  item = gnt_menuitem_new(((const char *)(dgettext("pidgin","Alphabetically"))));
  gnt_menuitem_set_id(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),"sort-alpha");
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)subsub),gnt_menu_get_gtype()))),item);
  gnt_menuitem_set_callback(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),sort_blist_change_cb,"text");
  item = gnt_menuitem_new(((const char *)(dgettext("pidgin","By Log Size"))));
  gnt_menuitem_set_id(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),"sort-log");
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)subsub),gnt_menu_get_gtype()))),item);
  gnt_menuitem_set_callback(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),sort_blist_change_cb,"log");
  item = gnt_menuitem_new(((const char *)(dgettext("pidgin","Add"))));
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
  subsub = gnt_menu_new(GNT_MENU_POPUP);
  gnt_menuitem_set_submenu(item,((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)subsub),gnt_menu_get_gtype()))));
  item = gnt_menuitem_new(((const char *)(dgettext("pidgin","Buddy"))));
  gnt_menuitem_set_id(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),"add-buddy");
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)subsub),gnt_menu_get_gtype()))),item);
  gnt_menuitem_set_callback(item,menu_add_buddy_cb,0);
  item = gnt_menuitem_new(((const char *)(dgettext("pidgin","Chat"))));
  gnt_menuitem_set_id(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),"add-chat");
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)subsub),gnt_menu_get_gtype()))),item);
  gnt_menuitem_set_callback(item,menu_add_chat_cb,0);
  item = gnt_menuitem_new(((const char *)(dgettext("pidgin","Group"))));
  gnt_menuitem_set_id(((GntMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gnt_menuitem_get_gtype()))),"add-group");
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)subsub),gnt_menu_get_gtype()))),item);
  gnt_menuitem_set_callback(item,menu_add_group_cb,0);
  ggblist -> grouping = (item = gnt_menuitem_new(((const char *)(dgettext("pidgin","Grouping")))));
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)sub),gnt_menu_get_gtype()))),item);
  reconstruct_grouping_menu();
  reconstruct_accounts_menu();
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_menu_get_gtype()))),(ggblist -> accounts));
  reconstruct_plugins_menu();
  gnt_menu_add_item(((GntMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gnt_menu_get_gtype()))),(ggblist -> plugins));
}

void finch_blist_show()
{
  blist_show(purple_get_blist());
}

static void group_collapsed(GntWidget *widget,PurpleBlistNode *node,gboolean collapsed,gpointer null)
{
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) 
    purple_blist_node_set_bool(node,"collapsed",collapsed);
}

static void blist_show(PurpleBuddyList *list)
{
  if (ggblist == ((FinchBlist *)((void *)0))) 
    new_list(list);
  else if ((ggblist -> window) != 0) {
    gnt_window_present((ggblist -> window));
    return ;
  }
  ggblist -> window = gnt_window_box_new(0,1);
  gnt_widget_set_name((ggblist -> window),"buddylist");
  gnt_box_set_toplevel(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> window)),gnt_box_get_gtype()))),1);
  gnt_box_set_title(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> window)),gnt_box_get_gtype()))),((const char *)(dgettext("pidgin","Buddy List"))));
  gnt_box_set_pad(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> window)),gnt_box_get_gtype()))),0);
  ggblist -> tree = gnt_tree_new();
  ( *((GntWidget *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_widget_get_gtype())))).priv.flags |= GNT_WIDGET_NO_BORDER;
  gnt_widget_set_size((ggblist -> tree),purple_prefs_get_int("/finch/blist/size/width"),purple_prefs_get_int("/finch/blist/size/height"));
  gnt_widget_set_position((ggblist -> window),purple_prefs_get_int("/finch/blist/position/x"),purple_prefs_get_int("/finch/blist/position/y"));
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> window)),gnt_box_get_gtype()))),(ggblist -> tree));
  ggblist -> status = gnt_combo_box_new();
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> window)),gnt_box_get_gtype()))),(ggblist -> status));
  ggblist -> statustext = gnt_entry_new(0);
  gnt_box_add_widget(((GntBox *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> window)),gnt_box_get_gtype()))),(ggblist -> statustext));
  gnt_widget_show((ggblist -> window));
  purple_signal_connect(purple_connections_get_handle(),"signed-on",finch_blist_get_handle(),((PurpleCallback )reconstruct_accounts_menu),0);
  purple_signal_connect(purple_connections_get_handle(),"signed-off",finch_blist_get_handle(),((PurpleCallback )reconstruct_accounts_menu),0);
  purple_signal_connect(purple_accounts_get_handle(),"account-actions-changed",finch_blist_get_handle(),((PurpleCallback )reconstruct_accounts_menu),0);
  purple_signal_connect(purple_blist_get_handle(),"buddy-status-changed",finch_blist_get_handle(),((PurpleCallback )buddy_status_changed),ggblist);
  purple_signal_connect(purple_blist_get_handle(),"buddy-idle-changed",finch_blist_get_handle(),((PurpleCallback )buddy_idle_changed),ggblist);
  purple_signal_connect(purple_plugins_get_handle(),"plugin-load",finch_blist_get_handle(),((PurpleCallback )reconstruct_plugins_menu),0);
  purple_signal_connect(purple_plugins_get_handle(),"plugin-unload",finch_blist_get_handle(),((PurpleCallback )reconstruct_plugins_menu),0);
  purple_signal_connect(purple_blist_get_handle(),"buddy-signed-on",finch_blist_get_handle(),((PurpleCallback )buddy_signed_on_off),ggblist);
  purple_signal_connect(purple_blist_get_handle(),"buddy-signed-off",finch_blist_get_handle(),((PurpleCallback )buddy_signed_on_off),ggblist);
#if 0
/* These I plan to use to indicate unread-messages etc. */
#endif
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),((GType )(20 << 2))))),"selection_changed",((GCallback )selection_changed),ggblist,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),((GType )(20 << 2))))),"key_pressed",((GCallback )key_pressed),ggblist,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),((GType )(20 << 2))))),"context-menu",((GCallback )context_menu),ggblist,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),((GType )(20 << 2))))),"collapse-toggled",((GCallback )group_collapsed),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),((GType )(20 << 2))))),"activate",((GCallback )selection_activate),ggblist,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),((GType )(20 << 2))))),"gained-focus",((GCallback )draw_tooltip),ggblist,0,(G_CONNECT_AFTER | G_CONNECT_SWAPPED));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),((GType )(20 << 2))))),"lost-focus",((GCallback )remove_peripherals),ggblist,0,(G_CONNECT_AFTER | G_CONNECT_SWAPPED));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> window)),((GType )(20 << 2))))),"workspace-hidden",((GCallback )remove_peripherals),ggblist,0,(G_CONNECT_AFTER | G_CONNECT_SWAPPED));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),((GType )(20 << 2))))),"size_changed",((GCallback )size_changed_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> window)),((GType )(20 << 2))))),"position_set",((GCallback )save_position_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> window)),((GType )(20 << 2))))),"destroy",((GCallback )reset_blist_window),0,0,((GConnectFlags )0));
/* Status signals */
  purple_signal_connect(purple_savedstatuses_get_handle(),"savedstatus-changed",finch_blist_get_handle(),((PurpleCallback )savedstatus_changed),0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> status)),((GType )(20 << 2))))),"selection_changed",((GCallback )status_selection_changed),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> statustext)),((GType )(20 << 2))))),"key_pressed",((GCallback )status_text_changed),0,0,((GConnectFlags )0));
  create_menu();
  populate_buddylist();
  savedstatus_changed(purple_savedstatus_get_current(),0);
}

void finch_blist_uninit()
{
}

gboolean finch_blist_get_position(int *x,int *y)
{
  if (!(ggblist != 0) || !((ggblist -> window) != 0)) 
    return 0;
  gnt_widget_get_position((ggblist -> window),x,y);
  return 1;
}

void finch_blist_set_position(int x,int y)
{
  gnt_widget_set_position((ggblist -> window),x,y);
}

gboolean finch_blist_get_size(int *width,int *height)
{
  if (!(ggblist != 0) || !((ggblist -> window) != 0)) 
    return 0;
  gnt_widget_get_size((ggblist -> window),width,height);
  return 1;
}

void finch_blist_set_size(int width,int height)
{
  gnt_widget_set_size((ggblist -> window),width,height);
}

void finch_blist_install_manager(const FinchBlistManager *manager)
{
  if (!(g_list_find(managers,manager) != 0)) {
    managers = g_list_append(managers,((gpointer )manager));
    reconstruct_grouping_menu();
    if (strcmp((manager -> id),purple_prefs_get_string("/finch/blist/grouping")) == 0) 
      purple_prefs_trigger_callback("/finch/blist/grouping");
  }
}

void finch_blist_uninstall_manager(const FinchBlistManager *manager)
{
  if (g_list_find(managers,manager) != 0) {
    managers = g_list_remove(managers,manager);
    reconstruct_grouping_menu();
    if (strcmp((manager -> id),purple_prefs_get_string("/finch/blist/grouping")) == 0) 
      purple_prefs_trigger_callback("/finch/blist/grouping");
  }
}

FinchBlistManager *finch_blist_manager_find(const char *id)
{
  GList *iter = managers;
  if (!(id != 0)) 
    return 0;
  for (; iter != 0; iter = (iter -> next)) {
    FinchBlistManager *m = (iter -> data);
    if (strcmp(id,(m -> id)) == 0) 
      return m;
  }
  return 0;
}

GntTree *finch_blist_get_tree()
{
  return (ggblist != 0)?((GntTree *)(g_type_check_instance_cast(((GTypeInstance *)(ggblist -> tree)),gnt_tree_get_gtype()))) : ((struct _GntTree *)((void *)0));
}
