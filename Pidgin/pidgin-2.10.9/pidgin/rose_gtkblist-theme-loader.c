/*
 * GTKBlistThemeLoader for Pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include <stdlib.h>
#include <string.h>
#include "xmlnode.h"
#include "debug.h"
#include "util.h"
#include "gtkblist-theme-loader.h"
#include "gtkblist-theme.h"
/******************************************************************************
 * Globals
 *****************************************************************************/
#define DEFAULT_TEXT_COLOR "black"
/*****************************************************************************
 * Buddy List Theme Builder
 *****************************************************************************/

static PidginThemeFont *pidgin_theme_font_parse(xmlnode *node)
{
  const char *font;
  const char *colordesc;
  GdkColor color;
  font = xmlnode_get_attrib(node,"font");
  if (((colordesc = xmlnode_get_attrib(node,"color")) == ((const char *)((void *)0))) || !(gdk_color_parse(colordesc,&color) != 0)) 
    gdk_color_parse("black",&color);
  return pidgin_theme_font_new(font,&color);
}

static GdkColor *parse_color(xmlnode *node,const char *tag)
{
  const char *temp = xmlnode_get_attrib(node,tag);
  GdkColor color;
  if ((temp != 0) && (gdk_color_parse(temp,&color) != 0)) {
    gdk_colormap_alloc_color(gdk_colormap_get_system(),&color,0,(!0));
    return gdk_color_copy((&color));
  }
  else {
    return 0;
  }
}

static PurpleTheme *pidgin_blist_loader_build(const gchar *dir)
{
  xmlnode *root_node = (xmlnode *)((void *)0);
  xmlnode *sub_node;
  xmlnode *sub_sub_node;
  gchar *filename_full;
  gchar *data = (gchar *)((void *)0);
  const gchar *temp;
  const gchar *name;
  gboolean success = (!0);
  GdkColor *bgcolor;
  GdkColor *expanded_bgcolor;
  GdkColor *collapsed_bgcolor;
  GdkColor *contact_color;
  PidginThemeFont *expanded;
  PidginThemeFont *collapsed;
  PidginThemeFont *contact;
  PidginThemeFont *online;
  PidginThemeFont *away;
  PidginThemeFont *offline;
  PidginThemeFont *idle;
  PidginThemeFont *message;
  PidginThemeFont *message_nick_said;
  PidginThemeFont *status;
  PidginBlistLayout layout;
  PidginBlistTheme *theme;
  int i;
  struct __unnamed_class___F0_L87_C2_L1548R__L1549R__scope____SgSS2___variable_declaration__variable_type___Pb__Cc__Pe___variable_name_L1548R__L1549R__scope____SgSS2____scope__tag__DELIMITER__L1548R__L1549R__scope____SgSS2___variable_declaration__variable_type___Pb____Pb__L1523R__Pe____Pe___variable_name_L1548R__L1549R__scope____SgSS2____scope__font {
  const char *tag;
  PidginThemeFont **font;}lookups[] = {{("contact_text"), (&contact)}, {("online_text"), (&online)}, {("away_text"), (&away)}, {("offline_text"), (&offline)}, {("idle_text"), (&idle)}, {("message_text"), (&message)}, {("message_nick_said_text"), (&message_nick_said)}, {("status_text"), (&status)}, {((const char *)((void *)0)), ((PidginThemeFont **)((void *)0))}};
  bgcolor = (expanded_bgcolor = (collapsed_bgcolor = (contact_color = ((GdkColor *)((void *)0)))));
  expanded = ((PidginThemeFont *)((void *)0));
  collapsed = ((PidginThemeFont *)((void *)0));
  contact = ((PidginThemeFont *)((void *)0));
  online = ((PidginThemeFont *)((void *)0));
  away = ((PidginThemeFont *)((void *)0));
  offline = ((PidginThemeFont *)((void *)0));
  idle = ((PidginThemeFont *)((void *)0));
  message = ((PidginThemeFont *)((void *)0));
  message_nick_said = ((PidginThemeFont *)((void *)0));
  status = ((PidginThemeFont *)((void *)0));
/* Find the theme file */
  do {
    if (dir != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"dir != NULL");
      return 0;
    };
  }while (0);
  filename_full = g_build_filename(dir,"theme.xml",((void *)((void *)0)));
  if (g_file_test(filename_full,G_FILE_TEST_IS_REGULAR) != 0) 
    root_node = xmlnode_from_file(dir,"theme.xml","buddy list themes","blist-loader");
  g_free(filename_full);
  if (root_node == ((xmlnode *)((void *)0))) 
    return 0;
  sub_node = xmlnode_get_child(root_node,"description");
  data = xmlnode_get_data(sub_node);
  name = xmlnode_get_attrib(root_node,"name");
/* <blist> */
  success = ((name != 0) && (purple_strequal(xmlnode_get_attrib(root_node,"type"),"pidgin buddy list") != 0));
  if (!(success != 0)) 
    purple_debug_warning("gtkblist-theme-loader","Missing attribute or problem with the root element\n");
  if (success != 0) {
    if ((success = ((sub_node = xmlnode_get_child(root_node,"blist")) != ((xmlnode *)((void *)0)))) != 0) 
      bgcolor = parse_color(sub_node,"color");
    else 
      purple_debug_warning("gtkblist-theme-loader","Missing or problem with tags: <blist>.\n");
  }
/* <groups> */
  if (success != 0) {
    if ((success = (((sub_node = xmlnode_get_child(root_node,"groups")) != ((xmlnode *)((void *)0))) && ((sub_sub_node = xmlnode_get_child(sub_node,"expanded")) != ((xmlnode *)((void *)0))))) != 0) {
      expanded = pidgin_theme_font_parse(sub_sub_node);
      expanded_bgcolor = parse_color(sub_sub_node,"background");
    }
    else 
      purple_debug_warning("gtkblist-theme-loader","Missing or problem with tags: <groups> <expanded>.\n");
  }
  if (success != 0) {
    if ((success = ((sub_node != ((xmlnode *)((void *)0))) && ((sub_sub_node = xmlnode_get_child(sub_node,"collapsed")) != ((xmlnode *)((void *)0))))) != 0) {
      collapsed = pidgin_theme_font_parse(sub_sub_node);
      collapsed_bgcolor = parse_color(sub_sub_node,"background");
    }
    else 
      purple_debug_warning("gtkblist-theme-loader","Missing or problem with tags: <groups> <collapsed>.\n");
  }
/* <buddys> */
  if (success != 0) {
    if ((success = (((sub_node = xmlnode_get_child(root_node,"buddys")) != ((xmlnode *)((void *)0))) && ((sub_sub_node = xmlnode_get_child(sub_node,"placement")) != ((xmlnode *)((void *)0))))) != 0) {
      layout.status_icon = (((temp = xmlnode_get_attrib(sub_sub_node,"status_icon")) != ((const gchar *)((void *)0)))?atoi(temp) : 0);
      layout.text = (((temp = xmlnode_get_attrib(sub_sub_node,"name")) != ((const gchar *)((void *)0)))?atoi(temp) : 1);
      layout.emblem = (((temp = xmlnode_get_attrib(sub_sub_node,"emblem")) != ((const gchar *)((void *)0)))?atoi(temp) : 2);
      layout.protocol_icon = (((temp = xmlnode_get_attrib(sub_sub_node,"protocol_icon")) != ((const gchar *)((void *)0)))?atoi(temp) : 3);
      layout.buddy_icon = (((temp = xmlnode_get_attrib(sub_sub_node,"buddy_icon")) != ((const gchar *)((void *)0)))?atoi(temp) : 4);
      layout.show_status = (((temp = xmlnode_get_attrib(sub_sub_node,"status_icon")) != ((const gchar *)((void *)0)))?(atoi(temp) != 0) : 1);
    }
    else 
      purple_debug_warning("gtkblist-theme-loader","Missing or problem with tags: <buddys> <placement>.\n");
  }
  if (success != 0) {
    if ((success = ((sub_node != ((xmlnode *)((void *)0))) && ((sub_sub_node = xmlnode_get_child(sub_node,"background")) != ((xmlnode *)((void *)0))))) != 0) 
      contact_color = parse_color(sub_sub_node,"color");
    else 
      purple_debug_warning("gtkblist-theme-loader","Missing or problem with tags: <buddys> <background>.\n");
  }
  for (i = 0; (success != 0) && (lookups[i].tag != 0); i++) {
    if ((success = ((sub_node != ((xmlnode *)((void *)0))) && ((sub_sub_node = xmlnode_get_child(sub_node,lookups[i].tag)) != ((xmlnode *)((void *)0))))) != 0) {
       *lookups[i].font = pidgin_theme_font_parse(sub_sub_node);
    }
    else {
       *lookups[i].font = ((PidginThemeFont *)((void *)0));
    }
  }
/* name is required for theme manager */
  success = ((success != 0) && (xmlnode_get_attrib(root_node,"name") != ((const char *)((void *)0))));
/* the new theme */
  theme = (g_object_new(pidgin_blist_theme_get_type(),"type","blist","name",name,"author",xmlnode_get_attrib(root_node,"author"),"image",xmlnode_get_attrib(root_node,"image"),"directory",dir,"description",data,"background-color",bgcolor,"layout",&layout,"expanded-color",expanded_bgcolor,"expanded-text",expanded,"collapsed-color",collapsed_bgcolor,"collapsed-text",collapsed,"contact-color",contact_color,"contact",contact,"online",online,"away",away,"offline",offline,"idle",idle,"message",message,"message_nick_said",message_nick_said,"status",status,((void *)((void *)0))));
  for (i = 0; lookups[i].tag != 0; i++) {
    if ( *lookups[i].font != 0) {
      pidgin_theme_font_free( *lookups[i].font);
    }
  }
  pidgin_theme_font_free(expanded);
  pidgin_theme_font_free(collapsed);
  xmlnode_free(root_node);
  g_free(data);
/* malformed xml file - also frees all partial data*/
  if (!(success != 0)) {
    g_object_unref(theme);
    theme = ((PidginBlistTheme *)((void *)0));
  }
  if (bgcolor != 0) 
    gdk_color_free(bgcolor);
  if (expanded_bgcolor != 0) 
    gdk_color_free(expanded_bgcolor);
  if (collapsed_bgcolor != 0) 
    gdk_color_free(collapsed_bgcolor);
  if (contact_color != 0) 
    gdk_color_free(contact_color);
  return (PurpleTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),purple_theme_get_type()));
}
/******************************************************************************
 * GObject Stuff
 *****************************************************************************/

static void pidgin_blist_theme_loader_class_init(PidginBlistThemeLoaderClass *klass)
{
  PurpleThemeLoaderClass *loader_klass = (PurpleThemeLoaderClass *)(g_type_check_class_cast(((GTypeClass *)klass),purple_theme_loader_get_type()));
  loader_klass -> purple_theme_loader_build = pidgin_blist_loader_build;
}

GType pidgin_blist_theme_loader_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(PidginBlistThemeLoaderClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )pidgin_blist_theme_loader_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(PidginBlistThemeLoader ))), (0), ((GInstanceInitFunc )((void *)0)), ((const GTypeValueTable *)((void *)0))
/* base_init */
/* base_finalize */
/* class_init */
/* class_finalize */
/* class_data */
/* n_preallocs */
/* instance_init */
/* value table */
};
    type = g_type_register_static(purple_theme_loader_get_type(),"PidginBlistThemeLoader",&info,0);
  }
  return type;
}
