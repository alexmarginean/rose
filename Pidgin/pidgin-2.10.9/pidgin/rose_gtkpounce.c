/**
 * @file gtkpounce.c GTK+ Buddy Pounce API
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "pidgin.h"
#include "account.h"
#include "conversation.h"
#include "debug.h"
#include "prpl.h"
#include "request.h"
#include "server.h"
#include "sound.h"
#include "util.h"
#include "gtkblist.h"
#include "gtkdialogs.h"
#include "gtkimhtml.h"
#include "gtkpounce.h"
#include "gtknotify.h"
#include "pidginstock.h"
#include "gtkutils.h"
/**
 * These are used for the GtkTreeView when you're scrolling through
 * all your saved pounces.
 */
enum __unnamed_enum___F0_L51_C1_POUNCES_MANAGER_COLUMN_POUNCE__COMMA__POUNCES_MANAGER_COLUMN_ICON__COMMA__POUNCES_MANAGER_COLUMN_TARGET__COMMA__POUNCES_MANAGER_COLUMN_ACCOUNT__COMMA__POUNCES_MANAGER_COLUMN_RECURRING__COMMA__POUNCES_MANAGER_NUM_COLUMNS {
/* Hidden column containing the PurplePounce */
POUNCES_MANAGER_COLUMN_POUNCE,POUNCES_MANAGER_COLUMN_ICON,POUNCES_MANAGER_COLUMN_TARGET,POUNCES_MANAGER_COLUMN_ACCOUNT,POUNCES_MANAGER_COLUMN_RECURRING,POUNCES_MANAGER_NUM_COLUMNS};
typedef struct __unnamed_class___F0_L62_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L1601R__Pe___variable_name_unknown_scope_and_name__scope__pounce__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L247R__Pe___variable_name_unknown_scope_and_name__scope__account__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__account_menu__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__buddy_entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__on_away__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__signon__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__signoff__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__away__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__away_return__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__idle__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__idle_return__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__typing__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__typed__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__stop_typing__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__message_recv__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__open_win__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__popup__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__popup_entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__send_msg__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__send_msg_entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__exec_cmd__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__exec_cmd_entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__exec_cmd_browse__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__play_sound__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__play_sound_entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__play_sound_browse__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__play_sound_test__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__play_sound_reset__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__save_pounce__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__save_button {
/* Pounce data */
PurplePounce *pounce;
PurpleAccount *account;
/* The window */
GtkWidget *window;
/* Pounce on Whom */
GtkWidget *account_menu;
GtkWidget *buddy_entry;
/* Pounce options */
GtkWidget *on_away;
/* Pounce When Buddy... */
GtkWidget *signon;
GtkWidget *signoff;
GtkWidget *away;
GtkWidget *away_return;
GtkWidget *idle;
GtkWidget *idle_return;
GtkWidget *typing;
GtkWidget *typed;
GtkWidget *stop_typing;
GtkWidget *message_recv;
/* Action */
GtkWidget *open_win;
GtkWidget *popup;
GtkWidget *popup_entry;
GtkWidget *send_msg;
GtkWidget *send_msg_entry;
GtkWidget *exec_cmd;
GtkWidget *exec_cmd_entry;
GtkWidget *exec_cmd_browse;
GtkWidget *play_sound;
GtkWidget *play_sound_entry;
GtkWidget *play_sound_browse;
GtkWidget *play_sound_test;
GtkWidget *play_sound_reset;
GtkWidget *save_pounce;
/* Buttons */
GtkWidget *save_button;}PidginPounceDialog;
typedef struct __unnamed_class___F0_L112_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1172R__Pe___variable_name_unknown_scope_and_name__scope__model__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__treeview__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__modify_button__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__delete_button {
GtkWidget *window;
GtkListStore *model;
GtkWidget *treeview;
GtkWidget *modify_button;
GtkWidget *delete_button;}PouncesManager;
static PouncesManager *pounces_manager = (PouncesManager *)((void *)0);
/**************************************************************************
 * Callbacks
 **************************************************************************/

static gint delete_win_cb(GtkWidget *w,GdkEventAny *e,PidginPounceDialog *dialog)
{
  gtk_widget_destroy((dialog -> window));
  g_free(dialog);
  return (!0);
}

static void cancel_cb(GtkWidget *w,PidginPounceDialog *dialog)
{
  delete_win_cb(0,0,dialog);
}

static void pounce_update_entry_fields(void *user_data,const char *filename)
{
  GtkWidget *entry = (GtkWidget *)user_data;
  gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),filename);
}

static void filesel(GtkWidget *widget,gpointer data)
{
  GtkWidget *entry;
  const gchar *name;
  entry = ((GtkWidget *)data);
  name = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))));
  purple_request_file(entry,((const char *)(dgettext("pidgin","Select a file"))),name,0,((GCallback )pounce_update_entry_fields),0,0,0,0,entry);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"destroy",((GCallback )purple_request_close_with_handle),entry,0,G_CONNECT_SWAPPED);
}

static void pounce_test_sound(GtkWidget *w,GtkWidget *entry)
{
  const char *filename;
  gboolean temp_mute;
  temp_mute = purple_prefs_get_bool("/pidgin/sound/mute");
  if (temp_mute != 0) 
    purple_prefs_set_bool("/pidgin/sound/mute",0);
  filename = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))));
  if (((filename != ((const char *)((void *)0))) && (( *filename) != 0)) && (strcmp(filename,((const char *)(dgettext("pidgin","(default)")))) != 0)) 
    purple_sound_play_file(filename,0);
  else 
    purple_sound_play_event(PURPLE_SOUND_POUNCE_DEFAULT,0);
  if (temp_mute != 0) 
    purple_prefs_set_bool("/pidgin/sound/mute",(!0));
}

static void pounce_reset_sound(GtkWidget *w,GtkWidget *entry)
{
  gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),((const char *)(dgettext("pidgin","(default)"))));
}

static void add_pounce_to_treeview(GtkListStore *model,PurplePounce *pounce)
{
  GtkTreeIter iter;
  PurpleAccount *account;
  gboolean recurring;
  const char *pouncer;
  const char *pouncee;
  GdkPixbuf *pixbuf;
  account = purple_pounce_get_pouncer(pounce);
  pixbuf = pidgin_create_prpl_icon(account,PIDGIN_PRPL_ICON_MEDIUM);
  pouncer = purple_account_get_username(account);
  pouncee = purple_pounce_get_pouncee(pounce);
  recurring = purple_pounce_get_save(pounce);
  gtk_list_store_append(model,&iter);
  gtk_list_store_set(model,&iter,POUNCES_MANAGER_COLUMN_POUNCE,pounce,POUNCES_MANAGER_COLUMN_ICON,pixbuf,POUNCES_MANAGER_COLUMN_TARGET,pouncee,POUNCES_MANAGER_COLUMN_ACCOUNT,pouncer,POUNCES_MANAGER_COLUMN_RECURRING,recurring,-1);
  if (pixbuf != ((GdkPixbuf *)((void *)0))) 
    g_object_unref(pixbuf);
}

static void populate_pounces_list(PouncesManager *dialog)
{
  GList *pounces;
  gtk_list_store_clear((dialog -> model));
  for (pounces = purple_pounces_get_all_for_ui("gtk-gaim"); pounces != ((GList *)((void *)0)); pounces = g_list_delete_link(pounces,pounces)) {
    add_pounce_to_treeview((dialog -> model),(pounces -> data));
  }
}

static void update_pounces()
{
/* Rebuild the pounces list if the pounces manager is open */
  if (pounces_manager != ((PouncesManager *)((void *)0))) {
    populate_pounces_list(pounces_manager);
  }
}

static void signed_on_off_cb(PurpleConnection *gc,gpointer user_data)
{
  update_pounces();
}

static void save_pounce_cb(GtkWidget *w,PidginPounceDialog *dialog)
{
  const char *name;
  const char *command;
  const char *sound;
  const char *reason;
  char *message;
  PurplePounceEvent events = PURPLE_POUNCE_NONE;
  PurplePounceOption options = PURPLE_POUNCE_OPTION_NONE;
  name = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> buddy_entry)),gtk_entry_get_type()))));
  if (( *name) == 0) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Please enter a buddy to pounce."))),0,0,0);
    return ;
  }
/* Options */
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> on_away)),gtk_toggle_button_get_type())))) != 0) 
    options |= PURPLE_POUNCE_OPTION_AWAY;
/* Events */
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> signon)),gtk_toggle_button_get_type())))) != 0) 
    events |= PURPLE_POUNCE_SIGNON;
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> signoff)),gtk_toggle_button_get_type())))) != 0) 
    events |= PURPLE_POUNCE_SIGNOFF;
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> away)),gtk_toggle_button_get_type())))) != 0) 
    events |= PURPLE_POUNCE_AWAY;
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> away_return)),gtk_toggle_button_get_type())))) != 0) 
    events |= PURPLE_POUNCE_AWAY_RETURN;
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> idle)),gtk_toggle_button_get_type())))) != 0) 
    events |= PURPLE_POUNCE_IDLE;
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> idle_return)),gtk_toggle_button_get_type())))) != 0) 
    events |= PURPLE_POUNCE_IDLE_RETURN;
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> typing)),gtk_toggle_button_get_type())))) != 0) 
    events |= PURPLE_POUNCE_TYPING;
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> typed)),gtk_toggle_button_get_type())))) != 0) 
    events |= PURPLE_POUNCE_TYPED;
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> stop_typing)),gtk_toggle_button_get_type())))) != 0) 
    events |= PURPLE_POUNCE_TYPING_STOPPED;
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> message_recv)),gtk_toggle_button_get_type())))) != 0) 
    events |= PURPLE_POUNCE_MESSAGE_RECEIVED;
/* Data fields */
  message = gtk_imhtml_get_markup(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> send_msg_entry)),gtk_imhtml_get_type()))));
  command = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> exec_cmd_entry)),gtk_entry_get_type()))));
  sound = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> play_sound_entry)),gtk_entry_get_type()))));
  reason = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> popup_entry)),gtk_entry_get_type()))));
  if (( *reason) == 0) 
    reason = ((const char *)((void *)0));
  if (( *message) == 0) {
    g_free(message);
    message = ((char *)((void *)0));
  }
  if (( *command) == 0) 
    command = ((const char *)((void *)0));
  if ((( *sound) == 0) || !(strcmp(sound,((const char *)(dgettext("pidgin","(default)")))) != 0)) 
    sound = ((const char *)((void *)0));
/* If the pounce has already been triggered, let's pretend it is a new one */
  if (((dialog -> pounce) != ((PurplePounce *)((void *)0))) && (g_list_find(purple_pounces_get_all(),(dialog -> pounce)) == ((GList *)((void *)0)))) {
    purple_debug_info("gtkpounce","Saving pounce that no longer exists; creating new pounce.\n");
    dialog -> pounce = ((PurplePounce *)((void *)0));
  }
  if ((dialog -> pounce) == ((PurplePounce *)((void *)0))) {
    dialog -> pounce = purple_pounce_new("gtk-gaim",(dialog -> account),name,events,options);
  }
  else {
    purple_pounce_set_events((dialog -> pounce),events);
    purple_pounce_set_options((dialog -> pounce),options);
    purple_pounce_set_pouncer((dialog -> pounce),(dialog -> account));
    purple_pounce_set_pouncee((dialog -> pounce),name);
  }
/* Actions */
  purple_pounce_action_set_enabled((dialog -> pounce),"open-window",gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> open_win)),gtk_toggle_button_get_type())))));
  purple_pounce_action_set_enabled((dialog -> pounce),"popup-notify",gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> popup)),gtk_toggle_button_get_type())))));
  purple_pounce_action_set_enabled((dialog -> pounce),"send-message",gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> send_msg)),gtk_toggle_button_get_type())))));
  purple_pounce_action_set_enabled((dialog -> pounce),"execute-command",gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> exec_cmd)),gtk_toggle_button_get_type())))));
  purple_pounce_action_set_enabled((dialog -> pounce),"play-sound",gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> play_sound)),gtk_toggle_button_get_type())))));
  purple_pounce_action_set_attribute((dialog -> pounce),"send-message","message",message);
  purple_pounce_action_set_attribute((dialog -> pounce),"execute-command","command",command);
  purple_pounce_action_set_attribute((dialog -> pounce),"play-sound","filename",sound);
  purple_pounce_action_set_attribute((dialog -> pounce),"popup-notify","reason",reason);
/* Set the defaults for next time. */
  purple_prefs_set_bool("/pidgin/pounces/default_actions/open-window",gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> open_win)),gtk_toggle_button_get_type())))));
  purple_prefs_set_bool("/pidgin/pounces/default_actions/popup-notify",gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> popup)),gtk_toggle_button_get_type())))));
  purple_prefs_set_bool("/pidgin/pounces/default_actions/send-message",gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> send_msg)),gtk_toggle_button_get_type())))));
  purple_prefs_set_bool("/pidgin/pounces/default_actions/execute-command",gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> exec_cmd)),gtk_toggle_button_get_type())))));
  purple_prefs_set_bool("/pidgin/pounces/default_actions/play-sound",gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> play_sound)),gtk_toggle_button_get_type())))));
  purple_pounce_set_save((dialog -> pounce),gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> save_pounce)),gtk_toggle_button_get_type())))));
  update_pounces();
  g_free(message);
  delete_win_cb(0,0,dialog);
}

static void pounce_choose_cb(GtkWidget *item,PurpleAccount *account,PidginPounceDialog *dialog)
{
  dialog -> account = account;
}

static void buddy_changed_cb(GtkEntry *entry,PidginPounceDialog *dialog)
{
  if ((dialog -> save_button) == ((GtkWidget *)((void *)0))) 
    return ;
  gtk_widget_set_sensitive((dialog -> save_button),(( *gtk_entry_get_text(entry)) != 0));
}

static void message_recv_toggle(GtkButton *message_recv,GtkWidget *send_msg)
{
  gboolean active = gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)message_recv),gtk_toggle_button_get_type()))));
  gtk_widget_set_sensitive(send_msg,!(active != 0));
  if (active != 0) 
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)send_msg),gtk_toggle_button_get_type()))),0);
}

static void pounce_dnd_recv(GtkWidget *widget,GdkDragContext *dc,gint x,gint y,GtkSelectionData *sd,guint info,guint t,gpointer data)
{
  PidginPounceDialog *dialog;
  if ((sd -> target) == gdk_atom_intern("PURPLE_BLIST_NODE",0)) {
    PurpleBlistNode *node = (PurpleBlistNode *)((void *)0);
    PurpleBuddy *buddy;
    memcpy((&node),(sd -> data),(sizeof(node)));
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) 
      buddy = purple_contact_get_priority_buddy(((PurpleContact *)node));
    else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) 
      buddy = ((PurpleBuddy *)node);
    else 
      return ;
    dialog = ((PidginPounceDialog *)data);
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> buddy_entry)),gtk_entry_get_type()))),(buddy -> name));
    dialog -> account = (buddy -> account);
    pidgin_account_option_menu_set_selected((dialog -> account_menu),(buddy -> account));
    gtk_drag_finish(dc,(!0),((dc -> action) == GDK_ACTION_MOVE),t);
  }
  else if ((sd -> target) == gdk_atom_intern("application/x-im-contact",0)) {
    char *protocol = (char *)((void *)0);
    char *username = (char *)((void *)0);
    PurpleAccount *account;
    if (pidgin_parse_x_im_contact(((const char *)(sd -> data)),0,&account,&protocol,&username,0) != 0) {
      if (account == ((PurpleAccount *)((void *)0))) {
        purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","You are not currently signed on with an account that can add that buddy."))),0,0,0);
      }
      else {
        dialog = ((PidginPounceDialog *)data);
        gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> buddy_entry)),gtk_entry_get_type()))),username);
        dialog -> account = account;
        pidgin_account_option_menu_set_selected((dialog -> account_menu),account);
      }
    }
    g_free(username);
    g_free(protocol);
    gtk_drag_finish(dc,(!0),((dc -> action) == GDK_ACTION_MOVE),t);
  }
}
static const GtkTargetEntry dnd_targets[] = {{("PURPLE_BLIST_NODE"), (GTK_TARGET_SAME_APP), (0)}, {("application/x-im-contact"), (0), (1)}};

static void reset_send_msg_entry(PidginPounceDialog *dialog,GtkWidget *dontcare)
{
  PurpleAccount *account = pidgin_account_option_menu_get_selected((dialog -> account_menu));
  gtk_imhtml_setup_entry(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> send_msg_entry)),gtk_imhtml_get_type()))),(((account != 0) && ((account -> gc) != 0))?( *(account -> gc)).flags : PURPLE_CONNECTION_HTML));
}

void pidgin_pounce_editor_show(PurpleAccount *account,const char *name,PurplePounce *cur_pounce)
{
  PidginPounceDialog *dialog;
  GtkWidget *window;
  GtkWidget *label;
  GtkWidget *vbox1;
  GtkWidget *vbox2;
  GtkWidget *hbox;
  GtkWidget *button;
  GtkWidget *frame;
  GtkWidget *table;
  GtkSizeGroup *sg;
  GPtrArray *sound_widgets;
  GPtrArray *exec_widgets;
  GtkWidget *send_msg_imhtml;
  do {
    if (((cur_pounce != ((PurplePounce *)((void *)0))) || (account != ((PurpleAccount *)((void *)0)))) || (purple_accounts_get_all() != ((GList *)((void *)0)))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"(cur_pounce != NULL) || (account != NULL) || (purple_accounts_get_all() != NULL)");
      return ;
    };
  }while (0);
  dialog = ((PidginPounceDialog *)(g_malloc0_n(1,(sizeof(PidginPounceDialog )))));
  if (cur_pounce != ((PurplePounce *)((void *)0))) {
    dialog -> pounce = cur_pounce;
    dialog -> account = purple_pounce_get_pouncer(cur_pounce);
  }
  else if (account != ((PurpleAccount *)((void *)0))) {
    dialog -> pounce = ((PurplePounce *)((void *)0));
    dialog -> account = account;
  }
  else {
    GList *connections = purple_connections_get_all();
    PurpleConnection *gc;
    if (connections != ((GList *)((void *)0))) {
      gc = ((PurpleConnection *)(connections -> data));
      dialog -> account = purple_connection_get_account(gc);
    }
    else 
      dialog -> account = ( *purple_accounts_get_all()).data;
    dialog -> pounce = ((PurplePounce *)((void *)0));
  }
  sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
/* Create the window. */
  dialog -> window = (window = gtk_dialog_new());
  gtk_window_set_title(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_window_get_type()))),((cur_pounce == ((PurplePounce *)((void *)0)))?((const char *)(dgettext("pidgin","Add Buddy Pounce"))) : ((const char *)(dgettext("pidgin","Modify Buddy Pounce")))));
  gtk_window_set_role(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_window_get_type()))),"buddy_pounce");
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> window)),gtk_container_get_type()))),12);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"delete_event",((GCallback )delete_win_cb),dialog,0,((GConnectFlags )0));
/* Create the parent vbox for everything. */
  vbox1 = ( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type())))).vbox;
/* Create the vbox that will contain all the prefs stuff. */
  vbox2 = gtk_vbox_new(0,6);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox1),gtk_box_get_type()))),vbox2,(!0),(!0),0);
/* Create the "Pounce on Whom" frame. */
  frame = pidgin_make_frame(vbox2,((const char *)(dgettext("pidgin","Pounce on Whom"))));
/* Account: */
  hbox = gtk_hbox_new(0,6);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)frame),gtk_box_get_type()))),hbox,0,0,0);
  gtk_widget_show(hbox);
  label = gtk_label_new_with_mnemonic(((const char *)(dgettext("pidgin","_Account:"))));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  gtk_widget_show(label);
  gtk_size_group_add_widget(sg,label);
  dialog -> account_menu = pidgin_account_option_menu_new((dialog -> account),(!0),((GCallback )pounce_choose_cb),0,dialog);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),(dialog -> account_menu),0,0,0);
  gtk_widget_show((dialog -> account_menu));
  pidgin_set_accessible_label((dialog -> account_menu),label);
/* Buddy: */
  hbox = gtk_hbox_new(0,6);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)frame),gtk_box_get_type()))),hbox,0,0,0);
  gtk_widget_show(hbox);
  label = gtk_label_new_with_mnemonic(((const char *)(dgettext("pidgin","_Buddy name:"))));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  gtk_widget_show(label);
  gtk_size_group_add_widget(sg,label);
  dialog -> buddy_entry = gtk_entry_new();
  pidgin_setup_screenname_autocomplete_with_filter((dialog -> buddy_entry),(dialog -> account_menu),pidgin_screenname_autocomplete_default_filter,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),(dialog -> buddy_entry),(!0),(!0),0);
  gtk_widget_show((dialog -> buddy_entry));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> buddy_entry)),((GType )(20 << 2))))),"changed",((GCallback )buddy_changed_cb),dialog,0,((GConnectFlags )0));
  pidgin_set_accessible_label((dialog -> buddy_entry),label);
  if (cur_pounce != ((PurplePounce *)((void *)0))) {
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> buddy_entry)),gtk_entry_get_type()))),purple_pounce_get_pouncee(cur_pounce));
  }
  else if (name != ((const char *)((void *)0))) {
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> buddy_entry)),gtk_entry_get_type()))),name);
  }
/* Create the "Pounce When Buddy..." frame. */
  frame = pidgin_make_frame(vbox2,((const char *)(dgettext("pidgin","Pounce When Buddy..."))));
  table = gtk_table_new(5,2,0);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)frame),gtk_container_get_type()))),table);
  gtk_table_set_col_spacings(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),12);
  gtk_widget_show(table);
  dialog -> signon = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Si_gns on"))));
  dialog -> signoff = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Signs o_ff"))));
  dialog -> away = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Goes a_way"))));
  dialog -> away_return = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Ret_urns from away"))));
  dialog -> idle = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Becomes _idle"))));
  dialog -> idle_return = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Is no longer i_dle"))));
  dialog -> typing = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Starts _typing"))));
  dialog -> typed = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","P_auses while typing"))));
  dialog -> stop_typing = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Stops t_yping"))));
  dialog -> message_recv = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Sends a _message"))));
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> message_recv),0,1,0,1,GTK_FILL,0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> signon),0,1,1,2,GTK_FILL,0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> signoff),0,1,2,3,GTK_FILL,0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> away),1,2,0,1,GTK_FILL,0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> away_return),1,2,1,2,GTK_FILL,0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> idle),1,2,2,3,GTK_FILL,0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> idle_return),2,3,0,1,GTK_FILL,0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> typing),2,3,1,2,GTK_FILL,0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> typed),2,3,2,3,GTK_FILL,0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> stop_typing),3,4,0,1,GTK_FILL,0,0,0);
  gtk_widget_show((dialog -> signon));
  gtk_widget_show((dialog -> signoff));
  gtk_widget_show((dialog -> away));
  gtk_widget_show((dialog -> away_return));
  gtk_widget_show((dialog -> idle));
  gtk_widget_show((dialog -> idle_return));
  gtk_widget_show((dialog -> typing));
  gtk_widget_show((dialog -> typed));
  gtk_widget_show((dialog -> stop_typing));
  gtk_widget_show((dialog -> message_recv));
/* Create the "Action" frame. */
  frame = pidgin_make_frame(vbox2,((const char *)(dgettext("pidgin","Action"))));
  table = gtk_table_new(3,6,0);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)frame),gtk_container_get_type()))),table);
  gtk_table_set_col_spacings(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),12);
  gtk_widget_show(table);
  dialog -> open_win = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Ope_n an IM window"))));
  dialog -> popup = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","_Pop up a notification"))));
  dialog -> send_msg = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Send a _message"))));
  dialog -> exec_cmd = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","E_xecute a command"))));
  dialog -> play_sound = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","P_lay a sound"))));
  send_msg_imhtml = pidgin_create_imhtml((!0),&dialog -> send_msg_entry,0,0);
  reset_send_msg_entry(dialog,0);
  dialog -> exec_cmd_entry = gtk_entry_new();
  dialog -> popup_entry = gtk_entry_new();
  dialog -> exec_cmd_browse = gtk_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Brows_e..."))));
  dialog -> play_sound_entry = gtk_entry_new();
  gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> play_sound_entry)),gtk_entry_get_type()))),((const char *)(dgettext("pidgin","(default)"))));
  gtk_editable_set_editable(((GtkEditable *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> play_sound_entry)),gtk_editable_get_type()))),0);
  dialog -> play_sound_browse = gtk_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Br_owse..."))));
  dialog -> play_sound_test = gtk_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Pre_view"))));
  dialog -> play_sound_reset = gtk_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Reset"))));
  gtk_widget_set_sensitive(send_msg_imhtml,0);
  gtk_widget_set_sensitive((dialog -> exec_cmd_entry),0);
  gtk_widget_set_sensitive((dialog -> popup_entry),0);
  gtk_widget_set_sensitive((dialog -> exec_cmd_browse),0);
  gtk_widget_set_sensitive((dialog -> play_sound_entry),0);
  gtk_widget_set_sensitive((dialog -> play_sound_browse),0);
  gtk_widget_set_sensitive((dialog -> play_sound_test),0);
  gtk_widget_set_sensitive((dialog -> play_sound_reset),0);
  g_object_unref(sg);
  sg = gtk_size_group_new(GTK_SIZE_GROUP_VERTICAL);
  gtk_size_group_add_widget(sg,(dialog -> open_win));
  gtk_size_group_add_widget(sg,(dialog -> popup));
  gtk_size_group_add_widget(sg,(dialog -> popup_entry));
  gtk_size_group_add_widget(sg,(dialog -> exec_cmd));
  gtk_size_group_add_widget(sg,(dialog -> exec_cmd_entry));
  gtk_size_group_add_widget(sg,(dialog -> exec_cmd_browse));
  gtk_size_group_add_widget(sg,(dialog -> play_sound));
  gtk_size_group_add_widget(sg,(dialog -> play_sound_entry));
  gtk_size_group_add_widget(sg,(dialog -> play_sound_browse));
  gtk_size_group_add_widget(sg,(dialog -> play_sound_test));
  gtk_size_group_add_widget(sg,(dialog -> play_sound_reset));
  g_object_unref(sg);
  sg = ((GtkSizeGroup *)((void *)0));
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> open_win),0,1,0,1,GTK_FILL,0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> popup),0,1,1,2,GTK_FILL,0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> popup_entry),1,5,1,2,GTK_FILL,0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> send_msg),0,5,2,3,GTK_FILL,0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),send_msg_imhtml,0,5,3,4,GTK_FILL,0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> exec_cmd),0,1,4,5,GTK_FILL,0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> exec_cmd_entry),1,2,4,5,GTK_FILL,0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> exec_cmd_browse),2,3,4,5,(GTK_FILL | GTK_EXPAND),0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> play_sound),0,1,5,6,GTK_FILL,0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> play_sound_entry),1,2,5,6,GTK_FILL,0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> play_sound_browse),2,3,5,6,(GTK_FILL | GTK_EXPAND),0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> play_sound_test),3,4,5,6,(GTK_FILL | GTK_EXPAND),0,0,0);
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> play_sound_reset),4,5,5,6,(GTK_FILL | GTK_EXPAND),0,0,0);
  gtk_table_set_row_spacings(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(6 / 2));
  gtk_widget_show((dialog -> open_win));
  gtk_widget_show((dialog -> popup));
  gtk_widget_show((dialog -> popup_entry));
  gtk_widget_show((dialog -> send_msg));
  gtk_widget_show(send_msg_imhtml);
  gtk_widget_show((dialog -> exec_cmd));
  gtk_widget_show((dialog -> exec_cmd_entry));
  gtk_widget_show((dialog -> exec_cmd_browse));
  gtk_widget_show((dialog -> play_sound));
  gtk_widget_show((dialog -> play_sound_entry));
  gtk_widget_show((dialog -> play_sound_browse));
  gtk_widget_show((dialog -> play_sound_test));
  gtk_widget_show((dialog -> play_sound_reset));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> message_recv)),((GType )(20 << 2))))),"clicked",((GCallback )message_recv_toggle),(dialog -> send_msg),0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> send_msg)),((GType )(20 << 2))))),"clicked",((GCallback )pidgin_toggle_sensitive),send_msg_imhtml,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> popup)),((GType )(20 << 2))))),"clicked",((GCallback )pidgin_toggle_sensitive),(dialog -> popup_entry),0,((GConnectFlags )0));
  exec_widgets = g_ptr_array_new();
  g_ptr_array_add(exec_widgets,(dialog -> exec_cmd_entry));
  g_ptr_array_add(exec_widgets,(dialog -> exec_cmd_browse));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> exec_cmd)),((GType )(20 << 2))))),"clicked",((GCallback )pidgin_toggle_sensitive_array),exec_widgets,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> exec_cmd_browse)),((GType )(20 << 2))))),"clicked",((GCallback )filesel),(dialog -> exec_cmd_entry),0,((GConnectFlags )0));
  g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> window)),((GType )(20 << 2))))),"exec-widgets",exec_widgets,((GDestroyNotify )g_ptr_array_free));
  sound_widgets = g_ptr_array_new();
  g_ptr_array_add(sound_widgets,(dialog -> play_sound_entry));
  g_ptr_array_add(sound_widgets,(dialog -> play_sound_browse));
  g_ptr_array_add(sound_widgets,(dialog -> play_sound_test));
  g_ptr_array_add(sound_widgets,(dialog -> play_sound_reset));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> play_sound)),((GType )(20 << 2))))),"clicked",((GCallback )pidgin_toggle_sensitive_array),sound_widgets,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> play_sound_browse)),((GType )(20 << 2))))),"clicked",((GCallback )filesel),(dialog -> play_sound_entry),0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> play_sound_test)),((GType )(20 << 2))))),"clicked",((GCallback )pounce_test_sound),(dialog -> play_sound_entry),0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> play_sound_reset)),((GType )(20 << 2))))),"clicked",((GCallback )pounce_reset_sound),(dialog -> play_sound_entry),0,((GConnectFlags )0));
  g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> window)),((GType )(20 << 2))))),"sound-widgets",sound_widgets,((GDestroyNotify )g_ptr_array_free));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> send_msg_entry)),((GType )(20 << 2))))),"format_function_clear",((GCallback )reset_send_msg_entry),dialog,0,G_CONNECT_SWAPPED);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> account_menu)),((GType )(20 << 2))))),"changed",((GCallback )reset_send_msg_entry),dialog,0,G_CONNECT_SWAPPED);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> send_msg_entry)),((GType )(20 << 2))))),"message_send",((GCallback )save_pounce_cb),dialog,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> popup_entry)),((GType )(20 << 2))))),"activate",((GCallback )save_pounce_cb),dialog,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> exec_cmd_entry)),((GType )(20 << 2))))),"activate",((GCallback )save_pounce_cb),dialog,0,((GConnectFlags )0));
/* Create the "Options" frame. */
  frame = pidgin_make_frame(vbox2,((const char *)(dgettext("pidgin","Options"))));
  table = gtk_table_new(2,1,0);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)frame),gtk_container_get_type()))),table);
  gtk_table_set_col_spacings(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),12);
  gtk_widget_show(table);
  dialog -> on_away = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","P_ounce only when my status is not Available"))));
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> on_away),0,1,0,1,GTK_FILL,0,0,0);
  dialog -> save_pounce = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","_Recurring"))));
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> save_pounce),0,1,1,2,GTK_FILL,0,0,0);
  gtk_widget_show((dialog -> on_away));
  gtk_widget_show((dialog -> save_pounce));
/* Cancel button */
  button = gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),"gtk-cancel",GTK_RESPONSE_CANCEL);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )cancel_cb),dialog,0,((GConnectFlags )0));
/* Save/Add button */
  dialog -> save_button = (button = gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),(((cur_pounce == ((PurplePounce *)((void *)0)))?"gtk-add" : "gtk-save")),GTK_RESPONSE_OK));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )save_pounce_cb),dialog,0,((GConnectFlags )0));
  if (( *gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> buddy_entry)),gtk_entry_get_type()))))) == 0) 
    gtk_widget_set_sensitive(button,0);
/* Setup drag-and-drop */
  gtk_drag_dest_set(window,(GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_DROP),dnd_targets,(sizeof(dnd_targets) / sizeof(GtkTargetEntry )),GDK_ACTION_COPY);
  gtk_drag_dest_set((dialog -> buddy_entry),(GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_DROP),dnd_targets,(sizeof(dnd_targets) / sizeof(GtkTargetEntry )),GDK_ACTION_COPY);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"drag_data_received",((GCallback )pounce_dnd_recv),dialog,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> buddy_entry)),((GType )(20 << 2))))),"drag_data_received",((GCallback )pounce_dnd_recv),dialog,0,((GConnectFlags )0));
/* Set the values of stuff. */
  if (cur_pounce != ((PurplePounce *)((void *)0))) {
    PurplePounceEvent events = purple_pounce_get_events(cur_pounce);
    PurplePounceOption options = purple_pounce_get_options(cur_pounce);
    const char *value;
/* Options */
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> on_away)),gtk_toggle_button_get_type()))),(options & PURPLE_POUNCE_OPTION_AWAY));
/* Events */
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> signon)),gtk_toggle_button_get_type()))),(events & PURPLE_POUNCE_SIGNON));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> signoff)),gtk_toggle_button_get_type()))),(events & PURPLE_POUNCE_SIGNOFF));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> away)),gtk_toggle_button_get_type()))),(events & PURPLE_POUNCE_AWAY));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> away_return)),gtk_toggle_button_get_type()))),(events & PURPLE_POUNCE_AWAY_RETURN));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> idle)),gtk_toggle_button_get_type()))),(events & PURPLE_POUNCE_IDLE));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> idle_return)),gtk_toggle_button_get_type()))),(events & PURPLE_POUNCE_IDLE_RETURN));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> typing)),gtk_toggle_button_get_type()))),(events & PURPLE_POUNCE_TYPING));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> typed)),gtk_toggle_button_get_type()))),(events & PURPLE_POUNCE_TYPED));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> stop_typing)),gtk_toggle_button_get_type()))),(events & PURPLE_POUNCE_TYPING_STOPPED));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> message_recv)),gtk_toggle_button_get_type()))),(events & PURPLE_POUNCE_MESSAGE_RECEIVED));
/* Actions */
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> open_win)),gtk_toggle_button_get_type()))),purple_pounce_action_is_enabled(cur_pounce,"open-window"));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> popup)),gtk_toggle_button_get_type()))),purple_pounce_action_is_enabled(cur_pounce,"popup-notify"));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> send_msg)),gtk_toggle_button_get_type()))),purple_pounce_action_is_enabled(cur_pounce,"send-message"));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> exec_cmd)),gtk_toggle_button_get_type()))),purple_pounce_action_is_enabled(cur_pounce,"execute-command"));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> play_sound)),gtk_toggle_button_get_type()))),purple_pounce_action_is_enabled(cur_pounce,"play-sound"));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> save_pounce)),gtk_toggle_button_get_type()))),purple_pounce_get_save(cur_pounce));
    if ((value = purple_pounce_action_get_attribute(cur_pounce,"send-message","message")) != ((const char *)((void *)0))) {
      gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> send_msg_entry)),gtk_imhtml_get_type()))),value,0,0);
    }
    if ((value = purple_pounce_action_get_attribute(cur_pounce,"popup-notify","reason")) != ((const char *)((void *)0))) {
      gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> popup_entry)),gtk_entry_get_type()))),value);
    }
    if ((value = purple_pounce_action_get_attribute(cur_pounce,"execute-command","command")) != ((const char *)((void *)0))) {
      gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> exec_cmd_entry)),gtk_entry_get_type()))),value);
    }
    if ((value = purple_pounce_action_get_attribute(cur_pounce,"play-sound","filename")) != ((const char *)((void *)0))) {
      gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> play_sound_entry)),gtk_entry_get_type()))),(((value != 0) && (( *value) != 0))?value : ((const char *)(dgettext("pidgin","(default)")))));
    }
  }
  else {
    PurpleBuddy *buddy = (PurpleBuddy *)((void *)0);
    if (name != ((const char *)((void *)0))) 
      buddy = purple_find_buddy(account,name);
/* Set some defaults */
    if (buddy == ((PurpleBuddy *)((void *)0))) {
      gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> signon)),gtk_toggle_button_get_type()))),(!0));
    }
    else {
      if (!(((buddy != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0))) {
        gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> signon)),gtk_toggle_button_get_type()))),(!0));
      }
      else {
        gboolean default_set = 0;
        PurplePresence *presence = purple_buddy_get_presence(buddy);
        if (purple_presence_is_idle(presence) != 0) {
          gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> idle_return)),gtk_toggle_button_get_type()))),(!0));
          default_set = (!0);
        }
        if (!(purple_presence_is_available(presence) != 0)) {
          gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> away_return)),gtk_toggle_button_get_type()))),(!0));
          default_set = (!0);
        }
        if (!(default_set != 0)) {
          gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> signon)),gtk_toggle_button_get_type()))),(!0));
        }
      }
    }
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> open_win)),gtk_toggle_button_get_type()))),purple_prefs_get_bool("/pidgin/pounces/default_actions/open-window"));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> popup)),gtk_toggle_button_get_type()))),purple_prefs_get_bool("/pidgin/pounces/default_actions/popup-notify"));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> send_msg)),gtk_toggle_button_get_type()))),purple_prefs_get_bool("/pidgin/pounces/default_actions/send-message"));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> exec_cmd)),gtk_toggle_button_get_type()))),purple_prefs_get_bool("/pidgin/pounces/default_actions/execute-command"));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> play_sound)),gtk_toggle_button_get_type()))),purple_prefs_get_bool("/pidgin/pounces/default_actions/play-sound"));
  }
  gtk_widget_show(vbox2);
  gtk_widget_show(window);
}

static gboolean pounces_manager_configure_cb(GtkWidget *widget,GdkEventConfigure *event,PouncesManager *dialog)
{
  if ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_object_get_type())))).flags & GTK_VISIBLE) != 0) {
    purple_prefs_set_int("/pidgin/pounces/dialog/width",(event -> width));
    purple_prefs_set_int("/pidgin/pounces/dialog/height",(event -> height));
  }
  return 0;
}

static gboolean pounces_manager_find_pounce(GtkTreeIter *iter,PurplePounce *pounce)
{
  GtkTreeModel *model = (GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(pounces_manager -> model)),gtk_tree_model_get_type()));
  PurplePounce *p;
  if (!(gtk_tree_model_get_iter_first(model,iter) != 0)) 
    return 0;
  gtk_tree_model_get(model,iter,POUNCES_MANAGER_COLUMN_POUNCE,&p,-1);
  if (pounce == p) 
    return (!0);
  while(gtk_tree_model_iter_next(model,iter) != 0){
    gtk_tree_model_get(model,iter,POUNCES_MANAGER_COLUMN_POUNCE,&p,-1);
    if (pounce == p) 
      return (!0);
  }
  return 0;
}

static gboolean pounces_manager_destroy_cb(GtkWidget *widget,GdkEvent *event,gpointer user_data)
{
  PouncesManager *dialog = user_data;
  dialog -> window = ((GtkWidget *)((void *)0));
  pidgin_pounces_manager_hide();
  return 0;
}

static void pounces_manager_connection_cb(PurpleConnection *gc,GtkWidget *add_button)
{
  gtk_widget_set_sensitive(add_button,(purple_connections_get_all() != ((GList *)((void *)0))));
}

static void pounces_manager_add_cb(GtkButton *button,gpointer user_data)
{
  pidgin_pounce_editor_show(0,0,0);
}

static void pounces_manager_modify_foreach(GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter,gpointer user_data)
{
  PurplePounce *pounce;
  gtk_tree_model_get(model,iter,POUNCES_MANAGER_COLUMN_POUNCE,&pounce,-1);
  pidgin_pounce_editor_show(0,0,pounce);
}

static void pounces_manager_modify_cb(GtkButton *button,gpointer user_data)
{
  PouncesManager *dialog = user_data;
  GtkTreeSelection *selection;
  selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treeview)),gtk_tree_view_get_type()))));
  gtk_tree_selection_selected_foreach(selection,pounces_manager_modify_foreach,user_data);
}

static void pounces_manager_delete_confirm_cb(PurplePounce *pounce)
{
  GtkTreeIter iter;
  if ((pounces_manager != 0) && (pounces_manager_find_pounce(&iter,pounce) != 0)) 
    gtk_list_store_remove((pounces_manager -> model),&iter);
  purple_request_close_with_handle(pounce);
  purple_pounce_destroy(pounce);
}

static void pounces_manager_delete_foreach(GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter,gpointer user_data)
{
  PurplePounce *pounce;
  PurpleAccount *account;
  const char *pouncer;
  const char *pouncee;
  char *buf;
  gtk_tree_model_get(model,iter,POUNCES_MANAGER_COLUMN_POUNCE,&pounce,-1);
  account = purple_pounce_get_pouncer(pounce);
  pouncer = purple_account_get_username(account);
  pouncee = purple_pounce_get_pouncee(pounce);
  buf = g_strdup_printf(((const char *)(dgettext("pidgin","Are you sure you want to delete the pounce on %s for %s\?"))),pouncee,pouncer);
  purple_request_action(pounce,0,buf,0,0,account,pouncee,0,pounce,2,((const char *)(dgettext("pidgin","Delete"))),pounces_manager_delete_confirm_cb,((const char *)(dgettext("pidgin","Cancel"))),((void *)((void *)0)));
  g_free(buf);
}

static void pounces_manager_delete_cb(GtkButton *button,gpointer user_data)
{
  PouncesManager *dialog = user_data;
  GtkTreeSelection *selection;
  selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treeview)),gtk_tree_view_get_type()))));
  gtk_tree_selection_selected_foreach(selection,pounces_manager_delete_foreach,user_data);
}

static void pounces_manager_close_cb(GtkButton *button,gpointer user_data)
{
  pidgin_pounces_manager_hide();
}

static void pounce_selected_cb(GtkTreeSelection *sel,gpointer user_data)
{
  PouncesManager *dialog = user_data;
  int num_selected = 0;
  num_selected = gtk_tree_selection_count_selected_rows(sel);
  gtk_widget_set_sensitive((dialog -> modify_button),(num_selected > 0));
  gtk_widget_set_sensitive((dialog -> delete_button),(num_selected > 0));
}

static gboolean pounce_double_click_cb(GtkTreeView *treeview,GdkEventButton *event,gpointer user_data)
{
  PouncesManager *dialog = user_data;
  GtkTreePath *path;
  GtkTreeIter iter;
  PurplePounce *pounce;
/* Figure out which node was clicked */
  if (!(gtk_tree_view_get_path_at_pos(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treeview)),gtk_tree_view_get_type()))),(event -> x),(event -> y),&path,0,0,0) != 0)) 
    return 0;
  gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter,path);
  gtk_tree_path_free(path);
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter,POUNCES_MANAGER_COLUMN_POUNCE,&pounce,-1);
  if (((pounce != ((PurplePounce *)((void *)0))) && ((event -> button) == 1)) && ((event -> type) == GDK_2BUTTON_PRESS)) {
    pidgin_pounce_editor_show(0,0,pounce);
    return (!0);
  }
  return 0;
}

static void pounces_manager_recurring_cb(GtkCellRendererToggle *renderer,gchar *path_str,gpointer user_data)
{
  PouncesManager *dialog = user_data;
  PurplePounce *pounce;
  gboolean recurring;
  GtkTreeModel *model = (GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()));
  GtkTreeIter iter;
  gtk_tree_model_get_iter_from_string(model,&iter,path_str);
  gtk_tree_model_get(model,&iter,POUNCES_MANAGER_COLUMN_POUNCE,&pounce,POUNCES_MANAGER_COLUMN_RECURRING,&recurring,-1);
  purple_pounce_set_save(pounce,!(recurring != 0));
  update_pounces();
}

static gboolean search_func(GtkTreeModel *model,gint column,const gchar *key,GtkTreeIter *iter,gpointer search_data)
{
  gboolean result;
  char *haystack;
  gtk_tree_model_get(model,iter,column,&haystack,-1);
  result = (purple_strcasestr(haystack,key) == ((const char *)((void *)0)));
  g_free(haystack);
  return result;
}

static GtkWidget *create_pounces_list(PouncesManager *dialog)
{
  GtkWidget *treeview;
  GtkTreeSelection *sel;
  GtkTreeViewColumn *column;
  GtkCellRenderer *renderer;
/* Create the list model */
  dialog -> model = gtk_list_store_new(POUNCES_MANAGER_NUM_COLUMNS,((GType )(17 << 2)),gdk_pixbuf_get_type(),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(5 << 2)));
/* Create the treeview */
  treeview = gtk_tree_view_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),((GType )(20 << 2))))));
  dialog -> treeview = treeview;
  gtk_tree_view_set_rules_hint(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),(!0));
  sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))));
  gtk_tree_selection_set_mode(sel,GTK_SELECTION_MULTIPLE);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)sel),((GType )(20 << 2))))),"changed",((GCallback )pounce_selected_cb),dialog,0,((GConnectFlags )0));
/* Handle double-clicking */
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)treeview),((GType )(20 << 2))))),"button_press_event",((GCallback )pounce_double_click_cb),dialog,0,((GConnectFlags )0));
  gtk_widget_show(treeview);
/* Pouncee Column */
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_title(column,((const char *)(dgettext("pidgin","Pounce Target"))));
  gtk_tree_view_column_set_resizable(column,(!0));
  gtk_tree_view_column_set_min_width(column,200);
  gtk_tree_view_column_set_sort_column_id(column,POUNCES_MANAGER_COLUMN_TARGET);
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),column);
/* Icon */
  renderer = gtk_cell_renderer_pixbuf_new();
  gtk_tree_view_column_pack_start(column,renderer,0);
  gtk_tree_view_column_add_attribute(column,renderer,"pixbuf",POUNCES_MANAGER_COLUMN_ICON);
/* Pouncee */
  renderer = gtk_cell_renderer_text_new();
  gtk_tree_view_column_pack_start(column,renderer,(!0));
  gtk_tree_view_column_add_attribute(column,renderer,"text",POUNCES_MANAGER_COLUMN_TARGET);
/* Account Column */
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_title(column,((const char *)(dgettext("pidgin","Account"))));
  gtk_tree_view_column_set_resizable(column,(!0));
  gtk_tree_view_column_set_min_width(column,200);
  gtk_tree_view_column_set_sort_column_id(column,POUNCES_MANAGER_COLUMN_ACCOUNT);
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),column);
  renderer = gtk_cell_renderer_text_new();
  gtk_tree_view_column_pack_start(column,renderer,(!0));
  gtk_tree_view_column_add_attribute(column,renderer,"text",POUNCES_MANAGER_COLUMN_ACCOUNT);
/* Recurring Column */
  renderer = gtk_cell_renderer_toggle_new();
  column = gtk_tree_view_column_new_with_attributes(((const char *)(dgettext("pidgin","Recurring"))),renderer,"active",POUNCES_MANAGER_COLUMN_RECURRING,((void *)((void *)0)));
  gtk_tree_view_column_set_sort_column_id(column,POUNCES_MANAGER_COLUMN_RECURRING);
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),column);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)renderer),((GType )(20 << 2))))),"toggled",((GCallback )pounces_manager_recurring_cb),dialog,0,((GConnectFlags )0));
/* Enable CTRL+F searching */
  gtk_tree_view_set_search_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),POUNCES_MANAGER_COLUMN_TARGET);
  gtk_tree_view_set_search_equal_func(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),search_func,0,0);
/* Sort the pouncee column by default */
  gtk_tree_sortable_set_sort_column_id(((GtkTreeSortable *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_sortable_get_type()))),POUNCES_MANAGER_COLUMN_TARGET,GTK_SORT_ASCENDING);
/* Populate list */
  populate_pounces_list(dialog);
  return pidgin_make_scrollable(treeview,GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC,GTK_SHADOW_IN,-1,-1);
}

void pidgin_pounces_manager_show()
{
  PouncesManager *dialog;
  GtkWidget *button;
  GtkWidget *list;
  GtkWidget *vbox;
  GtkWidget *win;
  int width;
  int height;
  if (pounces_manager != ((PouncesManager *)((void *)0))) {
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(pounces_manager -> window)),gtk_window_get_type()))));
    return ;
  }
  pounces_manager = (dialog = ((PouncesManager *)(g_malloc0_n(1,(sizeof(PouncesManager ))))));
  width = purple_prefs_get_int("/pidgin/pounces/dialog/width");
  height = purple_prefs_get_int("/pidgin/pounces/dialog/height");
  dialog -> window = (win = pidgin_create_dialog(((const char *)(dgettext("pidgin","Buddy Pounces"))),12,"pounces",(!0)));
  gtk_window_set_default_size(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_window_get_type()))),width,height);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"delete_event",((GCallback )pounces_manager_destroy_cb),dialog,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"configure_event",((GCallback )pounces_manager_configure_cb),dialog,0,((GConnectFlags )0));
/* Setup the vbox */
  vbox = pidgin_dialog_get_vbox_with_properties(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),0,12);
/* List of saved buddy pounces */
  list = create_pounces_list(dialog);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),list,(!0),(!0),0);
/* Add button */
  button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),"pidgin-add",((GCallback )pounces_manager_add_cb),dialog);
  gtk_widget_set_sensitive(button,(purple_accounts_get_all() != ((GList *)((void *)0))));
  purple_signal_connect(purple_connections_get_handle(),"signed-on",pounces_manager,((PurpleCallback )pounces_manager_connection_cb),button);
  purple_signal_connect(purple_connections_get_handle(),"signed-off",pounces_manager,((PurpleCallback )pounces_manager_connection_cb),button);
/* Modify button */
  button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),"pidgin-modify",((GCallback )pounces_manager_modify_cb),dialog);
  gtk_widget_set_sensitive(button,0);
  dialog -> modify_button = button;
/* Delete button */
  button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),"gtk-delete",((GCallback )pounces_manager_delete_cb),dialog);
  gtk_widget_set_sensitive(button,0);
  dialog -> delete_button = button;
/* Close button */
  pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),"gtk-close",((GCallback )pounces_manager_close_cb),dialog);
  gtk_widget_show(win);
}

void pidgin_pounces_manager_hide()
{
  if (pounces_manager == ((PouncesManager *)((void *)0))) 
    return ;
  if ((pounces_manager -> window) != ((GtkWidget *)((void *)0))) 
    gtk_widget_destroy((pounces_manager -> window));
  purple_signals_disconnect_by_handle(pounces_manager);
  g_free(pounces_manager);
  pounces_manager = ((PouncesManager *)((void *)0));
}

static void pounce_cb(PurplePounce *pounce,PurplePounceEvent events,void *data)
{
  PurpleConversation *conv;
  PurpleAccount *account;
  PurpleBuddy *buddy;
  const char *pouncee;
  const char *alias;
  pouncee = purple_pounce_get_pouncee(pounce);
  account = purple_pounce_get_pouncer(pounce);
  buddy = purple_find_buddy(account,pouncee);
  if (buddy != ((PurpleBuddy *)((void *)0))) {
    alias = purple_buddy_get_alias(buddy);
    if (alias == ((const char *)((void *)0))) 
      alias = pouncee;
  }
  else 
    alias = pouncee;
  if (purple_pounce_action_is_enabled(pounce,"open-window") != 0) {
    if (!(purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,pouncee,account) != 0)) 
      purple_conversation_new(PURPLE_CONV_TYPE_IM,account,pouncee);
  }
  if (purple_pounce_action_is_enabled(pounce,"popup-notify") != 0) {
    char *tmp;
    const char *reason;
    reason = purple_pounce_action_get_attribute(pounce,"popup-notify","reason");
/*
		 * Here we place the protocol name in the pounce dialog to lessen
		 * confusion about what protocol a pounce is for.
		 */
    tmp = g_strdup((((events & PURPLE_POUNCE_TYPING) != 0U)?((const char *)(dgettext("pidgin","Started typing"))) : ((((events & PURPLE_POUNCE_TYPED) != 0U)?((const char *)(dgettext("pidgin","Paused while typing"))) : ((((events & PURPLE_POUNCE_SIGNON) != 0U)?((const char *)(dgettext("pidgin","Signed on"))) : ((((events & PURPLE_POUNCE_IDLE_RETURN) != 0U)?((const char *)(dgettext("pidgin","Returned from being idle"))) : ((((events & PURPLE_POUNCE_AWAY_RETURN) != 0U)?((const char *)(dgettext("pidgin","Returned from being away"))) : ((((events & PURPLE_POUNCE_TYPING_STOPPED) != 0U)?((const char *)(dgettext("pidgin","Stopped typing"))) : ((((events & PURPLE_POUNCE_SIGNOFF) != 0U)?((const char *)(dgettext("pidgin","Signed off"))) : ((((events & PURPLE_POUNCE_IDLE) != 0U)?((const char *)(dgettext("pidgin","Became idle"))) : ((((events & PURPLE_POUNCE_AWAY) != 0U)?((const char *)(dgettext("pidgin","Went away"))) : ((((events & PURPLE_POUNCE_MESSAGE_RECEIVED) != 0U)?((const char *)(dgettext("pidgin","Sent a message"))) : ((const char *)(dgettext("pidgin","Unknown.... Please report this!")))))))))))))))))))))));
    pidgin_notify_pounce_add(account,pounce,alias,tmp,reason,purple_date_format_full(0));
    g_free(tmp);
  }
  if (purple_pounce_action_is_enabled(pounce,"send-message") != 0) {
    const char *message;
    message = purple_pounce_action_get_attribute(pounce,"send-message","message");
    if (message != ((const char *)((void *)0))) {
      conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,pouncee,account);
      if (conv == ((PurpleConversation *)((void *)0))) 
        conv = purple_conversation_new(PURPLE_CONV_TYPE_IM,account,pouncee);
      purple_conversation_write(conv,0,message,PURPLE_MESSAGE_SEND,time(0));
      serv_send_im((account -> gc),((char *)pouncee),((char *)message),0);
    }
  }
  if (purple_pounce_action_is_enabled(pounce,"execute-command") != 0) {
    const char *command;
    command = purple_pounce_action_get_attribute(pounce,"execute-command","command");
    if (command != ((const char *)((void *)0))) {
#ifndef _WIN32
      char *localecmd = g_locale_from_utf8(command,(-1),0,0,0);
      if (localecmd != ((char *)((void *)0))) {
        int pid = fork();
        if (pid == 0) {
          char *args[4UL];
          args[0] = "sh";
          args[1] = "-c";
          args[2] = ((char *)localecmd);
          args[3] = ((char *)((void *)0));
          execvp(args[0],args);
          _exit(0);
        }
        g_free(localecmd);
      }
#else /* !_WIN32 */
#endif /* !_WIN32 */
    }
  }
  if (purple_pounce_action_is_enabled(pounce,"play-sound") != 0) {
    const char *sound;
    sound = purple_pounce_action_get_attribute(pounce,"play-sound","filename");
    if (sound != ((const char *)((void *)0))) 
      purple_sound_play_file(sound,account);
    else 
      purple_sound_play_event(PURPLE_SOUND_POUNCE_DEFAULT,account);
  }
}

static void free_pounce(PurplePounce *pounce)
{
  update_pounces();
}

static void new_pounce(PurplePounce *pounce)
{
  purple_pounce_action_register(pounce,"open-window");
  purple_pounce_action_register(pounce,"popup-notify");
  purple_pounce_action_register(pounce,"send-message");
  purple_pounce_action_register(pounce,"execute-command");
  purple_pounce_action_register(pounce,"play-sound");
  update_pounces();
}

void *pidgin_pounces_get_handle()
{
  static int handle;
  return (&handle);
}

void pidgin_pounces_init()
{
  purple_pounces_register_handler("gtk-gaim",pounce_cb,new_pounce,free_pounce);
  purple_prefs_add_none("/pidgin/pounces");
  purple_prefs_add_none("/pidgin/pounces/default_actions");
  purple_prefs_add_bool("/pidgin/pounces/default_actions/open-window",0);
  purple_prefs_add_bool("/pidgin/pounces/default_actions/popup-notify",(!0));
  purple_prefs_add_bool("/pidgin/pounces/default_actions/send-message",0);
  purple_prefs_add_bool("/pidgin/pounces/default_actions/execute-command",0);
  purple_prefs_add_bool("/pidgin/pounces/default_actions/play-sound",0);
  purple_prefs_add_none("/pidgin/pounces/dialog");
  purple_prefs_add_int("/pidgin/pounces/dialog/width",520);
  purple_prefs_add_int("/pidgin/pounces/dialog/height",321);
  purple_signal_connect(purple_connections_get_handle(),"signed-on",pidgin_pounces_get_handle(),((PurpleCallback )signed_on_off_cb),0);
  purple_signal_connect(purple_connections_get_handle(),"signed-off",pidgin_pounces_get_handle(),((PurpleCallback )signed_on_off_cb),0);
}
