/*
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "debug.h"
#include "gtkmenutray.h"
#include <gtk/gtk.h>
/******************************************************************************
 * Enums
 *****************************************************************************/
enum __unnamed_enum___F0_L29_C1_PROP_ZERO__COMMA__PROP_BOX {PROP_ZERO,PROP_BOX};
/******************************************************************************
 * Globals
 *****************************************************************************/
static GObjectClass *parent_class = (GObjectClass *)((void *)0);
/******************************************************************************
 * Internal Stuff
 *****************************************************************************/
/******************************************************************************
 * Item Stuff
 *****************************************************************************/

static void pidgin_menu_tray_select(GtkItem *item)
{
/* this may look like nothing, but it's really overriding the
	 * GtkMenuItem's select function so that it doesn't get highlighted like
	 * a normal menu item would.
	 */
}

static void pidgin_menu_tray_deselect(GtkItem *item)
{
/* Probably not necessary, but I'd rather be safe than sorry.  We're
	 * overridding the select, so it makes sense to override deselect as well.
	 */
}
/******************************************************************************
 * Widget Stuff
 *****************************************************************************/
/******************************************************************************
 * Object Stuff
 *****************************************************************************/

static void pidgin_menu_tray_get_property(GObject *obj,guint param_id,GValue *value,GParamSpec *pspec)
{
  PidginMenuTray *menu_tray = (PidginMenuTray *)(g_type_check_instance_cast(((GTypeInstance *)obj),pidgin_menu_tray_get_gtype()));
  switch(param_id){
    case PROP_BOX:
{
      g_value_set_object(value,(pidgin_menu_tray_get_box(menu_tray)));
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)obj;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = param_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","gtkmenutray.c:79","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void pidgin_menu_tray_map(GtkWidget *widget)
{
  ( *( *((GtkWidgetClass *)(g_type_check_class_cast(((GTypeClass *)parent_class),gtk_widget_get_type())))).map)(widget);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_container_get_type()))),( *((PidginMenuTray *)(g_type_check_instance_cast(((GTypeInstance *)widget),pidgin_menu_tray_get_gtype())))).tray);
}

static void pidgin_menu_tray_finalize(GObject *obj)
{
  PidginMenuTray *tray = (PidginMenuTray *)(g_type_check_instance_cast(((GTypeInstance *)obj),pidgin_menu_tray_get_gtype()));
#if 0
/* This _might_ be leaking, but I have a sneaking suspicion that the widget is
	 * getting destroyed in GtkContainer's finalize function.  But if were are
	 * leaking here, be sure to figure out why this causes a crash.
	 *	-- Gary
	 */
#endif
  if ((tray -> tooltips) != 0) {
    gtk_object_sink(((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)(tray -> tooltips)),gtk_object_get_type()))));
  }
  ( *( *((GObjectClass *)(g_type_check_class_cast(((GTypeClass *)parent_class),((GType )(20 << 2)))))).finalize)(obj);
}

static void pidgin_menu_tray_class_init(PidginMenuTrayClass *klass)
{
  GObjectClass *object_class = (GObjectClass *)(g_type_check_class_cast(((GTypeClass *)klass),((GType )(20 << 2))));
  GtkItemClass *item_class = (GtkItemClass *)(g_type_check_class_cast(((GTypeClass *)klass),gtk_item_get_type()));
  GtkWidgetClass *widget_class = (GtkWidgetClass *)(g_type_check_class_cast(((GTypeClass *)klass),gtk_widget_get_type()));
  GParamSpec *pspec;
  parent_class = (g_type_class_peek_parent(klass));
  object_class -> finalize = pidgin_menu_tray_finalize;
  object_class -> get_property = pidgin_menu_tray_get_property;
  item_class -> select = pidgin_menu_tray_select;
  item_class -> deselect = pidgin_menu_tray_deselect;
  widget_class -> map = pidgin_menu_tray_map;
  pspec = g_param_spec_object("box","The box","The box",gtk_box_get_type(),G_PARAM_READABLE);
  g_object_class_install_property(object_class,PROP_BOX,pspec);
}

static void pidgin_menu_tray_init(PidginMenuTray *menu_tray)
{
  GtkWidget *widget = (GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)menu_tray),gtk_widget_get_type()));
  GtkSettings *settings;
  gint height = (-1);
  gtk_menu_item_set_right_justified(((GtkMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menu_tray),gtk_menu_item_get_type()))),(!0));
  if (!((({
    GTypeInstance *__inst = (GTypeInstance *)(menu_tray -> tray);
    GType __t = gtk_widget_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) 
    menu_tray -> tray = gtk_hbox_new(0,0);
  settings = gtk_settings_get_for_screen(gtk_widget_get_screen(widget));
  if (gtk_icon_size_lookup_for_settings(settings,GTK_ICON_SIZE_MENU,0,&height) != 0) {
    gtk_widget_set_size_request(widget,(-1),height);
  }
  gtk_widget_show((menu_tray -> tray));
}
/******************************************************************************
 * API
 *****************************************************************************/

GType pidgin_menu_tray_get_gtype()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(PidginMenuTrayClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )pidgin_menu_tray_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(PidginMenuTray ))), (0), ((GInstanceInitFunc )pidgin_menu_tray_init), ((const GTypeValueTable *)((void *)0))};
    type = g_type_register_static(gtk_menu_item_get_type(),"PidginMenuTray",&info,0);
  }
  return type;
}

GtkWidget *pidgin_menu_tray_new()
{
  return (g_object_new(pidgin_menu_tray_get_gtype(),0));
}

GtkWidget *pidgin_menu_tray_get_box(PidginMenuTray *menu_tray)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)menu_tray;
      GType __t = pidgin_menu_tray_get_gtype();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_MENU_TRAY(menu_tray)");
      return 0;
    };
  }while (0);
  return menu_tray -> tray;
}

static void pidgin_menu_tray_add(PidginMenuTray *menu_tray,GtkWidget *widget,const char *tooltip,gboolean prepend)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)menu_tray;
      GType __t = pidgin_menu_tray_get_gtype();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_MENU_TRAY(menu_tray)");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)widget;
      GType __t = gtk_widget_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_WIDGET(widget)");
      return ;
    };
  }while (0);
  if ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_object_get_type())))).flags & GTK_NO_WINDOW) != 0) {
    GtkWidget *event;
    event = gtk_event_box_new();
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)event),gtk_container_get_type()))),widget);
    gtk_widget_show(event);
    widget = event;
  }
  pidgin_menu_tray_set_tooltip(menu_tray,widget,tooltip);
  if (prepend != 0) 
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(menu_tray -> tray)),gtk_box_get_type()))),widget,0,0,0);
  else 
    gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(menu_tray -> tray)),gtk_box_get_type()))),widget,0,0,0);
}

void pidgin_menu_tray_append(PidginMenuTray *menu_tray,GtkWidget *widget,const char *tooltip)
{
  pidgin_menu_tray_add(menu_tray,widget,tooltip,0);
}

void pidgin_menu_tray_prepend(PidginMenuTray *menu_tray,GtkWidget *widget,const char *tooltip)
{
  pidgin_menu_tray_add(menu_tray,widget,tooltip,(!0));
}

void pidgin_menu_tray_set_tooltip(PidginMenuTray *menu_tray,GtkWidget *widget,const char *tooltip)
{
  if (!((menu_tray -> tooltips) != 0)) 
    menu_tray -> tooltips = gtk_tooltips_new();
/* Should we check whether widget is a child of menu_tray? */
/*
	 * If the widget does not have it's own window, then it
	 * must have automatically been added to an event box
	 * when it was added to the menu tray.  If this is the
	 * case, we want to set the tooltip on the widget's parent,
	 * not on the widget itself.
	 */
  if ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_object_get_type())))).flags & GTK_NO_WINDOW) != 0) 
    widget = (widget -> parent);
  gtk_tooltips_set_tip((menu_tray -> tooltips),widget,tooltip,0);
}
