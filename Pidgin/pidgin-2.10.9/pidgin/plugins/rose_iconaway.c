/* purple
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "pidgin.h"
#include "conversation.h"
#include "signals.h"
#include "version.h"
#include "gtkconv.h"
#include "gtkplugin.h"
#define ICONAWAY_PLUGIN_ID "gtk-iconaway"

static void iconify_windows(PurpleAccount *account,PurpleStatus *old,PurpleStatus *newstatus)
{
  PurplePresence *presence;
  PidginWindow *win;
  GList *windows;
  presence = purple_status_get_presence(newstatus);
  if (purple_presence_is_available(presence) != 0) 
    return ;
  purple_blist_set_visible(0);
  for (windows = pidgin_conv_windows_get_list(); windows != ((GList *)((void *)0)); windows = (windows -> next)) {
    win = ((PidginWindow *)(windows -> data));
    gtk_window_iconify(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),gtk_window_get_type()))));
  }
}
/*
 *  EXPORTED FUNCTIONS
 */

static gboolean plugin_load(PurplePlugin *plugin)
{
  purple_signal_connect(purple_accounts_get_handle(),"account-status-changed",plugin,((PurpleCallback )iconify_windows),0);
  return (!0);
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gtk-gaim"), (0), ((GList *)((void *)0)), (0), ("gtk-iconaway"), ("Iconify on Away"), ("2.10.9"), ("Iconifies the buddy list and your conversations when you go away."), ("Iconifies the buddy list and your conversations when you go away."), ("Eric Warmenhoven <eric@warmenhoven.org>"), ("http://pidgin.im/"), (plugin_load), ((gboolean (*)(PurplePlugin *))((void *)0)), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
