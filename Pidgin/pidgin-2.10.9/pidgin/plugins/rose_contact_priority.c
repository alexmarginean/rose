/*
 * Contact priority settings plugin.
 *
 * Copyright (C) 2003 Etan Reisner, <deryni9@users.sourceforge.net>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301, USA.
 */
#include "internal.h"
#include "pidgin.h"
#include "gtkplugin.h"
#include "gtkutils.h"
#include "prefs.h"
#include "version.h"
#define CONTACT_PRIORITY_PLUGIN_ID "gtk-contact-priority"

static void select_account(GtkWidget *widget,PurpleAccount *account,gpointer data)
{
  gtk_spin_button_set_value(((GtkSpinButton *)(g_type_check_instance_cast(((GTypeInstance *)data),gtk_spin_button_get_type()))),((gdouble )(purple_account_get_int(account,"score",0))));
}

static void account_update(GtkWidget *widget,GtkOptionMenu *optmenu)
{
  PurpleAccount *account = (PurpleAccount *)((void *)0);
  account = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_menu_get_active(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_option_menu_get_menu(optmenu))),gtk_menu_get_type())))))),((GType )(20 << 2))))),"account"));
  purple_account_set_int(account,"score",gtk_spin_button_get_value_as_int(((GtkSpinButton *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_spin_button_get_type())))));
}

static void pref_update(GtkWidget *widget,char *pref)
{
  if ((purple_prefs_get_type(pref)) == PURPLE_PREF_INT) 
    purple_prefs_set_int(pref,gtk_spin_button_get_value_as_int(((GtkSpinButton *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_spin_button_get_type())))));
  if ((purple_prefs_get_type(pref)) == PURPLE_PREF_BOOLEAN) 
    purple_prefs_set_bool(pref,gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_toggle_button_get_type())))));
}
static const struct PurpleContactPriorityStatuses {
const char *id;
const char *description;}statuses[] = {{("idle"), ("Buddy is idle")}, {("away"), ("Buddy is away")}, {("extended_away"), ("Buddy is \"extended\" away")}, 
#if 0
/* Not used yet. */
#endif
{("offline"), ("Buddy is offline")}, {((const char *)((void *)0)), ((const char *)((void *)0))}};

static GtkWidget *get_config_frame(PurplePlugin *plugin)
{
  GtkWidget *ret = (GtkWidget *)((void *)0);
  GtkWidget *hbox = (GtkWidget *)((void *)0);
  GtkWidget *frame = (GtkWidget *)((void *)0);
  GtkWidget *vbox = (GtkWidget *)((void *)0);
  GtkWidget *label = (GtkWidget *)((void *)0);
  GtkWidget *spin = (GtkWidget *)((void *)0);
  GtkWidget *check = (GtkWidget *)((void *)0);
  GtkWidget *optmenu = (GtkWidget *)((void *)0);
  GtkObject *adj = (GtkObject *)((void *)0);
  GtkSizeGroup *sg = (GtkSizeGroup *)((void *)0);
  PurpleAccount *account = (PurpleAccount *)((void *)0);
  int i;
  gboolean last_match = purple_prefs_get_bool("/purple/contact/last_match");
  sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  ret = gtk_vbox_new(0,18);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_container_get_type()))),12);
  frame = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Point values to use when..."))));
  vbox = gtk_vbox_new(0,5);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)frame),gtk_container_get_type()))),vbox);
/* Status Spinboxes */
  for (i = 0; (statuses[i].id != ((const char *)((void *)0))) && (statuses[i].description != ((const char *)((void *)0))); i++) {
    char *pref = g_strconcat("/purple/status/scores/",statuses[i].id,((void *)((void *)0)));
    hbox = gtk_hbox_new(0,5);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
    label = gtk_label_new_with_mnemonic(((const char *)(dgettext("pidgin",statuses[i].description))));
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
    gtk_size_group_add_widget(sg,label);
    gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0);
    adj = gtk_adjustment_new((purple_prefs_get_int(pref)),(-500),500,1,1,1);
    spin = gtk_spin_button_new(((GtkAdjustment *)adj),1,0);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)spin),((GType )(20 << 2))))),"value-changed",((GCallback )pref_update),pref,0,((GConnectFlags )0));
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),spin,0,0,0);
    g_free(pref);
  }
/* Explanation */
  label = gtk_label_new(0);
  gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),((const char *)(dgettext("pidgin","The buddy with the <i>largest score</i> is the buddy who will have priority in the contact.\n"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),label,0,0,0);
/* Last match */
  hbox = gtk_hbox_new(0,5);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  check = gtk_check_button_new_with_label(((const char *)(dgettext("pidgin","Use last buddy when scores are equal"))));
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)check),gtk_toggle_button_get_type()))),last_match);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)check),((GType )(20 << 2))))),"toggled",((GCallback )pref_update),"/purple/contact/last_match",0,((GConnectFlags )0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),check,0,0,0);
  frame = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Point values to use for account..."))));
  vbox = gtk_vbox_new(0,5);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)frame),gtk_container_get_type()))),vbox);
/* Account */
  hbox = gtk_hbox_new(0,5);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
/* make this here so I can use it in the option menu callback, we'll
	 * actually set it up later */
  adj = gtk_adjustment_new(0,(-500),500,1,1,1);
  spin = gtk_spin_button_new(((GtkAdjustment *)adj),1,0);
  optmenu = pidgin_account_option_menu_new(0,(!0),((GCallback )select_account),0,spin);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),optmenu,0,0,0);
/* this is where we set up the spin button we made above */
  account = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_menu_get_active(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_option_menu_get_menu(((GtkOptionMenu *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),gtk_option_menu_get_type())))))),gtk_menu_get_type())))))),((GType )(20 << 2))))),"account"));
  gtk_spin_button_set_value(((GtkSpinButton *)(g_type_check_instance_cast(((GTypeInstance *)spin),gtk_spin_button_get_type()))),((gdouble )(purple_account_get_int(account,"score",0))));
  gtk_spin_button_set_adjustment(((GtkSpinButton *)(g_type_check_instance_cast(((GTypeInstance *)spin),gtk_spin_button_get_type()))),((GtkAdjustment *)(g_type_check_instance_cast(((GTypeInstance *)adj),gtk_adjustment_get_type()))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)spin),((GType )(20 << 2))))),"value-changed",((GCallback )account_update),optmenu,0,((GConnectFlags )0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),spin,0,0,0);
  gtk_widget_show_all(ret);
  g_object_unref(sg);
  return ret;
}
static PidginPluginUiInfo ui_info = {(get_config_frame), (0), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* page_num (Reserved) */
/* Padding */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gtk-gaim"), (0), ((GList *)((void *)0)), (0), ("gtk-contact-priority"), ("Contact Priority"), ("2.10.9"), ("Allows for controlling the values associated with different buddy states."), ("Allows for changing the point values of idle/away/offline states for buddies in contact priority computations."), ("Etan Reisner <deryni@eden.rutgers.edu>"), ("http://pidgin.im/"), ((gboolean (*)(PurplePlugin *))((void *)0)), ((gboolean (*)(PurplePlugin *))((void *)0)), ((void (*)(PurplePlugin *))((void *)0)), ((&ui_info)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**< summary        */
/**< description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/**< prefs_info     */
/**< actions        */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
