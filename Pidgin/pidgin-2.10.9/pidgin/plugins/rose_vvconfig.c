/*
 * Configures microphones and webcams for voice and video
 * Copyright (C) 2009 Mike Ruprecht <cmaiku@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301, USA.
 */
#include "internal.h"
#include "debug.h"
#include "mediamanager.h"
#include "media-gst.h"
#include "version.h"
#include "gtkplugin.h"
#include "gtkutils.h"
#include "gtkprefs.h"
#include <gst/interfaces/propertyprobe.h>
/* container window for showing a stand-alone configurator */
static GtkWidget *window = (GtkWidget *)((void *)0);
static PurpleMediaElementInfo *old_video_src = (PurpleMediaElementInfo *)((void *)0);
static PurpleMediaElementInfo *old_video_sink = (PurpleMediaElementInfo *)((void *)0);
static PurpleMediaElementInfo *old_audio_src = (PurpleMediaElementInfo *)((void *)0);
static PurpleMediaElementInfo *old_audio_sink = (PurpleMediaElementInfo *)((void *)0);
static const gchar *AUDIO_SRC_PLUGINS[] = {("alsasrc"), ("ALSA"), ("osssrc"), ("OSS"), ("pulsesrc"), ("PulseAudio"), ("sndiosrc"), ("sndio"), ("audiotestsrc"), ("Test Sound"), ((const gchar *)((void *)0))
/* "esdmon",	"ESD", ? */
/* "audiotestsrc wave=silence", "Silence", */
};
static const gchar *AUDIO_SINK_PLUGINS[] = {("alsasink"), ("ALSA"), ("artsdsink"), ("aRts"), ("esdsink"), ("ESD"), ("osssink"), ("OSS"), ("pulsesink"), ("PulseAudio"), ("sndiosink"), ("sndio"), ((const gchar *)((void *)0))};
static const gchar *VIDEO_SRC_PLUGINS[] = {("videotestsrc"), ("Test Input"), ("dshowvideosrc"), ("DirectDraw"), ("ksvideosrc"), ("KS Video"), ("qcamsrc"), ("Quickcam"), ("v4lsrc"), ("Video4Linux"), ("v4l2src"), ("Video4Linux2"), ("v4lmjpegsrc"), ("Video4Linux MJPEG"), ((const gchar *)((void *)0))};
static const gchar *VIDEO_SINK_PLUGINS[] = {("directdrawsink"), ("DirectDraw"), ("glimagesink"), ("OpenGL"), ("ximagesink"), ("X Window System"), ("xvimagesink"), ("X Window System (Xv)"), ((const gchar *)((void *)0))
/* "aasink",	"AALib", Didn't work for me */
};

static GList *get_element_devices(const gchar *element_name)
{
  GList *ret = (GList *)((void *)0);
  GstElement *element;
  GObjectClass *klass;
  GstPropertyProbe *probe;
  const GParamSpec *pspec;
  ret = g_list_prepend(ret,((gpointer )((const char *)(dgettext("pidgin","Default")))));
  ret = g_list_prepend(ret,"");
  if (!(strcmp(element_name,"<custom>") != 0) || (( *element_name) == 0)) {
    return g_list_reverse(ret);
  }
  element = gst_element_factory_make(element_name,"test");
  if (!(element != 0)) {
    purple_debug_info("vvconfig","\'%s\' - unable to find element\n",element_name);
    return g_list_reverse(ret);
  }
  klass = ((GObjectClass *)( *((GTypeInstance *)element)).g_class);
  if (!(klass != 0)) {
    purple_debug_info("vvconfig","\'%s\' - unable to find G_Object Class\n",element_name);
    return g_list_reverse(ret);
  }
  if (((!(g_object_class_find_property(klass,"device") != 0) || !((({
    GTypeInstance *__inst = (GTypeInstance *)element;
    GType __t = gst_property_probe_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) || !((probe = ((GstPropertyProbe *)(g_type_check_instance_cast(((GTypeInstance *)element),gst_property_probe_get_type())))) != 0)) || !((pspec = gst_property_probe_get_property(probe,"device")) != 0)) 
{
    purple_debug_info("vvconfig","\'%s\' - no device\n",element_name);
  }
  else {
    gint n;
    GValueArray *array;
/* Set autoprobe[-fps] to FALSE to avoid delays when probing. */
    if (g_object_class_find_property(klass,"autoprobe") != 0) {
      g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)element),((GType )(20 << 2))))),"autoprobe",0,((void *)((void *)0)));
      if (g_object_class_find_property(klass,"autoprobe-fps") != 0) {
        g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)element),((GType )(20 << 2))))),"autoprobe-fps",0,((void *)((void *)0)));
      }
    }
    array = gst_property_probe_probe_and_get_values(probe,pspec);
    if (array == ((GValueArray *)((void *)0))) {
      purple_debug_info("vvconfig","\'%s\' has no devices\n",element_name);
      return g_list_reverse(ret);
    }
    for (n = 0; n < (array -> n_values); ++n) {{
        GValue *device;
        const gchar *name;
        const gchar *device_name;
        device = g_value_array_get_nth(array,n);
        g_object_set_property(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)element),((GType )(20 << 2))))),"device",device);
        if ((gst_element_set_state(element,GST_STATE_READY)) != GST_STATE_CHANGE_SUCCESS) {
          purple_debug_warning("vvconfig","Error changing state of %s\n",element_name);
          continue; 
        }
        g_object_get(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)element),((GType )(20 << 2))))),"device-name",&name,((void *)((void *)0)));
        device_name = g_value_get_string(device);
        if (name == ((const gchar *)((void *)0))) 
          name = ((const char *)(dgettext("pidgin","Unknown")));
        purple_debug_info("vvconfig","Found device %s : %s for %s\n",device_name,name,element_name);
        ret = g_list_prepend(ret,((gpointer )name));
        ret = g_list_prepend(ret,((gpointer )device_name));
        gst_element_set_state(element,GST_STATE_NULL);
      }
    }
  }
  gst_object_unref(element);
  return g_list_reverse(ret);
}

static GList *get_element_plugins(const gchar **plugins)
{
  GList *ret = (GList *)((void *)0);
  ret = g_list_prepend(ret,"Default");
  ret = g_list_prepend(ret,"");
  for (; (plugins[0] != 0) && (plugins[1] != 0); plugins += 2) {
    if (gst_default_registry_check_feature_version(plugins[0],0,0,0) != 0) {
      ret = g_list_prepend(ret,((gpointer )plugins[1]));
      ret = g_list_prepend(ret,((gpointer )plugins[0]));
    }
  }
  ret = g_list_reverse(ret);
  return ret;
}

static void device_changed_cb(const gchar *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  GtkSizeGroup *sg = data;
  GtkWidget *parent;
  GtkWidget *widget;
  GSList *widgets;
  GList *devices;
  GValue gvalue;
  gint position;
  gchar *label;
  gchar *pref;
  widgets = gtk_size_group_get_widgets(((GtkSizeGroup *)(g_type_check_instance_cast(((GTypeInstance *)sg),gtk_size_group_get_type()))));
{
    for (; widgets != 0; widgets = ((widgets != 0)?( *((GSList *)widgets)).next : ((struct _GSList *)((void *)0)))) {
      const gchar *widget_name = gtk_widget_get_name(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(widgets -> data)),gtk_widget_get_type()))));
      if (!(strcmp(widget_name,name) != 0)) {
        gchar *temp_str;
        gchar delimiters[3UL] = {(0), (0), (0)};
        const gchar *text;
        gint keyval;
        gint pos;
        widget = (widgets -> data);
/* Get label with _ from the GtkLabel */
        text = gtk_label_get_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_label_get_type()))));
        keyval = (gtk_label_get_mnemonic_keyval(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_label_get_type())))));
        delimiters[0] = g_ascii_tolower(keyval);
        delimiters[1] = g_ascii_toupper(keyval);
        pos = (strcspn(text,delimiters));
        if (pos != -1) {
          temp_str = g_strndup(text,pos);
          label = g_strconcat(temp_str,"_",(text + pos),((void *)((void *)0)));
          g_free(temp_str);
        }
        else {
          label = g_strdup(text);
        }
        break; 
      }
    }
  }
  if (widgets == ((GSList *)((void *)0))) 
    return ;
  parent = gtk_widget_get_parent(widget);
  widget = parent;
  parent = gtk_widget_get_parent(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_widget_get_type()))));
  gvalue.g_type = 0;
  g_value_init(&gvalue,((GType )(6 << 2)));
  gtk_container_child_get_property(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)parent),gtk_container_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_widget_get_type()))),"position",&gvalue);
  position = g_value_get_int((&gvalue));
  g_value_unset(&gvalue);
  gtk_widget_destroy(widget);
  pref = g_strdup(name);
  strcpy(((pref + strlen(pref)) - strlen("plugin")),"device");
  devices = get_element_devices(value);
  if (g_list_find_custom(devices,(purple_prefs_get_string(pref)),((GCompareFunc )strcmp)) == ((GList *)((void *)0))) 
    purple_prefs_set_string(pref,( *(((devices != 0)?( *((GList *)devices)).next : ((struct _GList *)((void *)0))))).data);
  widget = pidgin_prefs_dropdown_from_list(parent,label,PURPLE_PREF_STRING,pref,devices);
  g_list_free(devices);
  g_signal_connect_data(widget,"destroy",((GCallback )g_free),pref,0,G_CONNECT_SWAPPED);
  g_free(label);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_misc_get_type()))),0,0.5);
  gtk_widget_set_name(widget,name);
  gtk_size_group_add_widget(sg,widget);
  gtk_box_reorder_child(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)parent),gtk_box_get_type()))),gtk_widget_get_parent(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_widget_get_type())))),position);
}

static void get_plugin_frame(GtkWidget *parent,GtkSizeGroup *sg,const gchar *name,const gchar *plugin_label,const gchar **plugin_strs,const gchar *plugin_pref,const gchar *device_label,const gchar *device_pref)
{
  GtkWidget *vbox;
  GtkWidget *widget;
  GList *plugins;
  GList *devices;
  vbox = pidgin_make_frame(parent,name);
/* Setup plugin preference */
  plugins = get_element_plugins(plugin_strs);
  widget = pidgin_prefs_dropdown_from_list(vbox,plugin_label,PURPLE_PREF_STRING,plugin_pref,plugins);
  g_list_free(plugins);
  gtk_size_group_add_widget(sg,widget);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_misc_get_type()))),0,0.5);
/* Setup device preference */
  devices = get_element_devices(purple_prefs_get_string(plugin_pref));
  if (g_list_find_custom(devices,(purple_prefs_get_string(device_pref)),((GCompareFunc )strcmp)) == ((GList *)((void *)0))) 
    purple_prefs_set_string(device_pref,( *(((devices != 0)?( *((GList *)devices)).next : ((struct _GList *)((void *)0))))).data);
  widget = pidgin_prefs_dropdown_from_list(vbox,device_label,PURPLE_PREF_STRING,device_pref,devices);
  g_list_free(devices);
  gtk_widget_set_name(widget,plugin_pref);
  gtk_size_group_add_widget(sg,widget);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_misc_get_type()))),0,0.5);
  purple_prefs_connect_callback(vbox,plugin_pref,device_changed_cb,sg);
  g_signal_connect_data(vbox,"destroy",((GCallback )purple_prefs_disconnect_by_handle),vbox,0,G_CONNECT_SWAPPED);
}

static GtkWidget *get_plugin_config_frame(PurplePlugin *plugin)
{
  GtkWidget *notebook;
  GtkWidget *vbox_audio;
  GtkWidget *vbox_video;
  GtkSizeGroup *sg;
  notebook = gtk_notebook_new();
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)notebook),gtk_container_get_type()))),12);
  gtk_widget_show(notebook);
  vbox_audio = gtk_vbox_new(0,18);
  vbox_video = gtk_vbox_new(0,18);
  gtk_notebook_append_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)notebook),gtk_notebook_get_type()))),vbox_audio,gtk_label_new(((const char *)(dgettext("pidgin","Audio")))));
  gtk_notebook_append_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)notebook),gtk_notebook_get_type()))),vbox_video,gtk_label_new(((const char *)(dgettext("pidgin","Video")))));
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox_audio),gtk_container_get_type()))),12);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox_video),gtk_container_get_type()))),12);
  gtk_widget_show(vbox_audio);
  gtk_widget_show(vbox_video);
  sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  get_plugin_frame(vbox_audio,sg,((const char *)(dgettext("pidgin","Output"))),((const char *)(dgettext("pidgin","_Plugin"))),AUDIO_SINK_PLUGINS,"/plugins/core/vvconfig/audio/sink/plugin",((const char *)(dgettext("pidgin","_Device"))),"/plugins/core/vvconfig/audio/sink/device");
  get_plugin_frame(vbox_audio,sg,((const char *)(dgettext("pidgin","Input"))),((const char *)(dgettext("pidgin","P_lugin"))),AUDIO_SRC_PLUGINS,"/plugins/core/vvconfig/audio/src/plugin",((const char *)(dgettext("pidgin","D_evice"))),"/plugins/core/vvconfig/audio/src/device");
  get_plugin_frame(vbox_video,sg,((const char *)(dgettext("pidgin","Output"))),((const char *)(dgettext("pidgin","_Plugin"))),VIDEO_SINK_PLUGINS,"/plugins/gtk/vvconfig/video/sink/plugin",((const char *)(dgettext("pidgin","_Device"))),"/plugins/gtk/vvconfig/video/sink/device");
  get_plugin_frame(vbox_video,sg,((const char *)(dgettext("pidgin","Input"))),((const char *)(dgettext("pidgin","P_lugin"))),VIDEO_SRC_PLUGINS,"/plugins/core/vvconfig/video/src/plugin",((const char *)(dgettext("pidgin","D_evice"))),"/plugins/core/vvconfig/video/src/device");
  return notebook;
}

static GstElement *create_video_src(PurpleMedia *media,const gchar *session_id,const gchar *participant)
{
  const gchar *plugin = purple_prefs_get_string("/plugins/core/vvconfig/video/src/plugin");
  const gchar *device = purple_prefs_get_string("/plugins/core/vvconfig/video/src/device");
  GstElement *ret;
  if (plugin[0] == 0) 
    return purple_media_element_info_call_create(old_video_src,media,session_id,participant);
  ret = gst_element_factory_make(plugin,"vvconfig-videosrc");
  if (device[0] != 0) 
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ret),((GType )(20 << 2))))),"device",device,((void *)((void *)0)));
  if (!(strcmp(plugin,"videotestsrc") != 0)) 
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ret),((GType )(20 << 2))))),"is-live",1,((void *)((void *)0)));
  return ret;
}

static GstElement *create_video_sink(PurpleMedia *media,const gchar *session_id,const gchar *participant)
{
  const gchar *plugin = purple_prefs_get_string("/plugins/gtk/vvconfig/video/sink/plugin");
  const gchar *device = purple_prefs_get_string("/plugins/gtk/vvconfig/video/sink/device");
  GstElement *ret;
  if (plugin[0] == 0) 
    return purple_media_element_info_call_create(old_video_sink,media,session_id,participant);
  ret = gst_element_factory_make(plugin,0);
  if (device[0] != 0) 
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ret),((GType )(20 << 2))))),"device",device,((void *)((void *)0)));
  return ret;
}

static GstElement *create_audio_src(PurpleMedia *media,const gchar *session_id,const gchar *participant)
{
  const gchar *plugin = purple_prefs_get_string("/plugins/core/vvconfig/audio/src/plugin");
  const gchar *device = purple_prefs_get_string("/plugins/core/vvconfig/audio/src/device");
  GstElement *ret;
  if (plugin[0] == 0) 
    return purple_media_element_info_call_create(old_audio_src,media,session_id,participant);
  ret = gst_element_factory_make(plugin,0);
  if (device[0] != 0) 
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ret),((GType )(20 << 2))))),"device",device,((void *)((void *)0)));
  return ret;
}

static GstElement *create_audio_sink(PurpleMedia *media,const gchar *session_id,const gchar *participant)
{
  const gchar *plugin = purple_prefs_get_string("/plugins/core/vvconfig/audio/sink/plugin");
  const gchar *device = purple_prefs_get_string("/plugins/core/vvconfig/audio/sink/device");
  GstElement *ret;
  if (plugin[0] == 0) 
    return purple_media_element_info_call_create(old_audio_sink,media,session_id,participant);
  ret = gst_element_factory_make(plugin,0);
  if (device[0] != 0) 
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ret),((GType )(20 << 2))))),"device",device,((void *)((void *)0)));
  return ret;
}

static void set_element_info_cond(PurpleMediaElementInfo *old_info,PurpleMediaElementInfo *new_info,const gchar *id)
{
  gchar *element_id = purple_media_element_info_get_id(old_info);
  if (!(strcmp(element_id,id) != 0)) 
    purple_media_manager_set_active_element(purple_media_manager_get(),new_info);
  g_free(element_id);
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  PurpleMediaManager *manager;
  PurpleMediaElementInfo *video_src;
  PurpleMediaElementInfo *video_sink;
  PurpleMediaElementInfo *audio_src;
  PurpleMediaElementInfo *audio_sink;
/* Disable the plugin if the UI doesn't support VV */
  if ((purple_media_manager_get_ui_caps(purple_media_manager_get())) == PURPLE_MEDIA_CAPS_NONE) 
    return 0;
  purple_prefs_add_none("/plugins/core/vvconfig");
  purple_prefs_add_none("/plugins/core/vvconfig/audio");
  purple_prefs_add_none("/plugins/core/vvconfig/audio/src");
  purple_prefs_add_string("/plugins/core/vvconfig/audio/src/plugin","");
  purple_prefs_add_string("/plugins/core/vvconfig/audio/src/device","");
  purple_prefs_add_none("/plugins/core/vvconfig/audio/sink");
  purple_prefs_add_string("/plugins/core/vvconfig/audio/sink/plugin","");
  purple_prefs_add_string("/plugins/core/vvconfig/audio/sink/device","");
  purple_prefs_add_none("/plugins/core/vvconfig/video");
  purple_prefs_add_none("/plugins/core/vvconfig/video/src");
  purple_prefs_add_string("/plugins/core/vvconfig/video/src/plugin","");
  purple_prefs_add_string("/plugins/core/vvconfig/video/src/device","");
  purple_prefs_add_none("/plugins/gtk/vvconfig");
  purple_prefs_add_none("/plugins/gtk/vvconfig/video");
  purple_prefs_add_none("/plugins/gtk/vvconfig/video/sink");
  purple_prefs_add_string("/plugins/gtk/vvconfig/video/sink/plugin","");
  purple_prefs_add_string("/plugins/gtk/vvconfig/video/sink/device","");
  video_src = (g_object_new(purple_media_element_info_get_type(),"id","vvconfig-videosrc","name","VV Conf Plugin Video Source","type",PURPLE_MEDIA_ELEMENT_VIDEO | PURPLE_MEDIA_ELEMENT_SRC | PURPLE_MEDIA_ELEMENT_ONE_SRC | PURPLE_MEDIA_ELEMENT_UNIQUE,"create-cb",create_video_src,((void *)((void *)0))));
  video_sink = (g_object_new(purple_media_element_info_get_type(),"id","vvconfig-videosink","name","VV Conf Plugin Video Sink","type",PURPLE_MEDIA_ELEMENT_VIDEO | PURPLE_MEDIA_ELEMENT_SINK | PURPLE_MEDIA_ELEMENT_ONE_SINK,"create-cb",create_video_sink,((void *)((void *)0))));
  audio_src = (g_object_new(purple_media_element_info_get_type(),"id","vvconfig-audiosrc","name","VV Conf Plugin Audio Source","type",PURPLE_MEDIA_ELEMENT_AUDIO | PURPLE_MEDIA_ELEMENT_SRC | PURPLE_MEDIA_ELEMENT_ONE_SRC | PURPLE_MEDIA_ELEMENT_UNIQUE,"create-cb",create_audio_src,((void *)((void *)0))));
  audio_sink = (g_object_new(purple_media_element_info_get_type(),"id","vvconfig-audiosink","name","VV Conf Plugin Audio Sink","type",PURPLE_MEDIA_ELEMENT_AUDIO | PURPLE_MEDIA_ELEMENT_SINK | PURPLE_MEDIA_ELEMENT_ONE_SINK,"create-cb",create_audio_sink,((void *)((void *)0))));
  purple_debug_info("gtkmedia","Registering media element types\n");
  manager = purple_media_manager_get();
  old_video_src = purple_media_manager_get_active_element(manager,(PURPLE_MEDIA_ELEMENT_VIDEO | PURPLE_MEDIA_ELEMENT_SRC));
  old_video_sink = purple_media_manager_get_active_element(manager,(PURPLE_MEDIA_ELEMENT_VIDEO | PURPLE_MEDIA_ELEMENT_SINK));
  old_audio_src = purple_media_manager_get_active_element(manager,(PURPLE_MEDIA_ELEMENT_AUDIO | PURPLE_MEDIA_ELEMENT_SRC));
  old_audio_sink = purple_media_manager_get_active_element(manager,(PURPLE_MEDIA_ELEMENT_AUDIO | PURPLE_MEDIA_ELEMENT_SINK));
  set_element_info_cond(old_video_src,video_src,"pidgindefaultvideosrc");
  set_element_info_cond(old_video_sink,video_sink,"pidgindefaultvideosink");
  set_element_info_cond(old_audio_src,audio_src,"pidgindefaultaudiosrc");
  set_element_info_cond(old_audio_sink,audio_sink,"pidgindefaultaudiosink");
  return (!0);
}

static void config_destroy(GtkObject *w,gpointer nul)
{
  purple_debug_info("vvconfig","closing vv configuration window\n");
  window = ((GtkWidget *)((void *)0));
}

static void config_close(GtkObject *w,gpointer nul)
{
  gtk_widget_destroy(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_widget_get_type()))));
}
typedef GtkWidget *(*FrameCreateCb)(PurplePlugin *);

static void show_config(PurplePluginAction *action)
{
  if (!(window != 0)) {
    FrameCreateCb create_frame = (action -> user_data);
    GtkWidget *vbox = gtk_vbox_new(0,12);
    GtkWidget *hbox = gtk_hbox_new(0,12);
    GtkWidget *config_frame = ( *create_frame)(0);
    GtkWidget *close = gtk_button_new_from_stock("gtk-close");
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),config_frame);
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),hbox);
    window = pidgin_create_window((action -> label),12,0,0);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"destroy",((GCallback )config_destroy),0,0,((GConnectFlags )0));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)close),((GType )(20 << 2))))),"clicked",((GCallback )config_close),0,0,((GConnectFlags )0));
    gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),close,0,0,12);
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_container_get_type()))),vbox);
    gtk_widget_show(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)close),gtk_widget_get_type()))));
    gtk_widget_show(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_widget_get_type()))));
    gtk_widget_show(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_widget_get_type()))));
  }
  gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_window_get_type()))));
}

static GstElement *create_pipeline()
{
  GstElement *pipeline = gst_pipeline_new("voicetest");
  GstElement *src = create_audio_src(0,0,0);
  GstElement *sink = create_audio_sink(0,0,0);
  GstElement *volume = gst_element_factory_make("volume","volume");
  GstElement *level = gst_element_factory_make("level","level");
  GstElement *valve = gst_element_factory_make("valve","valve");
  gst_bin_add_many(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)pipeline),gst_bin_get_type()))),src,volume,level,valve,sink,((void *)((void *)0)));
  gst_element_link_many(src,volume,level,valve,sink,((void *)((void *)0)));
  gst_element_set_state(((GstElement *)(g_type_check_instance_cast(((GTypeInstance *)pipeline),gst_element_get_type()))),GST_STATE_PLAYING);
  return pipeline;
}

static void on_volume_change_cb(GtkRange *range,GstBin *pipeline)
{
  GstElement *volume;
  do {
    if (pipeline != ((GstBin *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pipeline != NULL");
      return ;
    };
  }while (0);
  volume = gst_bin_get_by_name(pipeline,"volume");
  g_object_set(volume,"volume",(gtk_range_get_value(range) / 10.0),((void *)((void *)0)));
}

static gdouble gst_msg_db_to_percent(GstMessage *msg,gchar *value_name)
{
  const GValue *list;
  const GValue *value;
  gdouble value_db;
  gdouble percent;
  list = gst_structure_get_value(gst_message_get_structure(msg),value_name);
  value = gst_value_list_get_value(list,0);
  value_db = g_value_get_double(value);
  percent = pow(10,(value_db / 20));
  return (percent > 1.0)?1.0 : percent;
}
typedef struct __unnamed_class___F0_L594_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L1659R__Pe___variable_name_unknown_scope_and_name__scope__level__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkRange_GtkRange__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__threshold {
GtkProgressBar *level;
GtkRange *threshold;}BusCbCtx;

static gboolean gst_bus_cb(GstBus *bus,GstMessage *msg,BusCbCtx *ctx)
{
  if ((( *((GstMessage *)msg)).type == GST_MESSAGE_ELEMENT) && (gst_structure_has_name((msg -> structure),"level") != 0)) {
    GstElement *src = (GstElement *)(g_type_check_instance_cast(((GTypeInstance *)( *((GstMessage *)msg)).src),gst_element_get_type()));
    gchar *name = gst_object_get_name(((GstObject *)src));
    if (!(strcmp(name,"level") != 0)) {
      gdouble percent;
      gdouble threshold;
      GstElement *valve;
      percent = gst_msg_db_to_percent(msg,"rms");
      gtk_progress_bar_set_fraction((ctx -> level),(percent * 5));
      percent = gst_msg_db_to_percent(msg,"decay");
      threshold = (gtk_range_get_value((ctx -> threshold)) / 100.0);
      valve = gst_bin_get_by_name(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)((GstElement *)( *((GstObject *)src)).parent)),gst_bin_get_type()))),"valve");
      g_object_set(valve,"drop",(percent < threshold),((void *)((void *)0)));
      g_object_set((ctx -> level),"text",((percent < threshold)?((const char *)(dgettext("pidgin","DROP"))) : " "),((void *)((void *)0)));
    }
    g_free(name);
  }
  return (!0);
}

static void voice_test_frame_destroy_cb(GtkObject *w,GstElement *pipeline)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)pipeline;
      GType __t = gst_element_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GST_IS_ELEMENT(pipeline)");
      return ;
    };
  }while (0);
  gst_element_set_state(pipeline,GST_STATE_NULL);
  gst_object_unref(pipeline);
}

static void volume_scale_destroy_cb(GtkRange *volume,gpointer nul)
{
  purple_prefs_set_int("/purple/media/audio/volume/input",(gtk_range_get_value(volume)));
}

static gchar *threshold_value_format_cb(GtkScale *scale,gdouble value)
{
  return g_strdup_printf("%.*f%%",gtk_scale_get_digits(scale),value);
}

static void threshold_scale_destroy_cb(GtkRange *threshold,gpointer nul)
{
  purple_prefs_set_int("/purple/media/audio/silence_threshold",(gtk_range_get_value(threshold)));
}

static GtkWidget *get_voice_test_frame(PurplePlugin *plugin)
{
  GtkWidget *vbox = gtk_vbox_new(0,12);
  GtkWidget *level = gtk_progress_bar_new();
  GtkWidget *volume = gtk_hscale_new_with_range(0,100,1);
  GtkWidget *threshold = gtk_hscale_new_with_range(0,100,1);
  GtkWidget *label;
  GtkTable *table = (GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_table_new(2,2,0))),gtk_table_get_type()));
  GstElement *pipeline;
  GstBus *bus;
  BusCbCtx *ctx;
  g_object_set(vbox,"width-request",500,((void *)((void *)0)));
  gtk_table_set_row_spacings(table,6);
  gtk_table_set_col_spacings(table,6);
  label = gtk_label_new(((const char *)(dgettext("pidgin","Volume:"))));
  g_object_set(label,"xalign",0.0,((void *)((void *)0)));
  gtk_table_attach(table,label,0,1,0,1,GTK_FILL,0,0,0);
  gtk_table_attach_defaults(table,volume,1,2,0,1);
  label = gtk_label_new(((const char *)(dgettext("pidgin","Silence threshold:"))));
  g_object_set(label,"xalign",0.0,"yalign",1.0,((void *)((void *)0)));
  gtk_table_attach(table,label,0,1,1,2,GTK_FILL,GTK_FILL,0,0);
  gtk_table_attach_defaults(table,threshold,1,2,1,2);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),level);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_widget_get_type()))));
  gtk_widget_show_all(vbox);
  pipeline = create_pipeline();
  bus = gst_pipeline_get_bus(((GstPipeline *)(g_type_check_instance_cast(((GTypeInstance *)pipeline),gst_pipeline_get_type()))));
  gst_bus_add_signal_watch(bus);
  ctx = ((BusCbCtx *)(g_malloc_n(1,(sizeof(BusCbCtx )))));
  ctx -> level = ((GtkProgressBar *)(g_type_check_instance_cast(((GTypeInstance *)level),gtk_progress_bar_get_type())));
  ctx -> threshold = ((GtkRange *)(g_type_check_instance_cast(((GTypeInstance *)threshold),gtk_range_get_type())));
  g_signal_connect_data(bus,"message",((GCallback )gst_bus_cb),ctx,((GClosureNotify )g_free),0);
  gst_object_unref(bus);
  g_signal_connect_data(volume,"value-changed",((GCallback )on_volume_change_cb),pipeline,0,((GConnectFlags )0));
  gtk_range_set_value(((GtkRange *)(g_type_check_instance_cast(((GTypeInstance *)volume),gtk_range_get_type()))),(purple_prefs_get_int("/purple/media/audio/volume/input")));
  gtk_widget_set(volume,"draw-value",0,((void *)((void *)0)));
  gtk_range_set_value(((GtkRange *)(g_type_check_instance_cast(((GTypeInstance *)threshold),gtk_range_get_type()))),(purple_prefs_get_int("/purple/media/audio/silence_threshold")));
  g_signal_connect_data(vbox,"destroy",((GCallback )voice_test_frame_destroy_cb),pipeline,0,((GConnectFlags )0));
  g_signal_connect_data(volume,"destroy",((GCallback )volume_scale_destroy_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(threshold,"format-value",((GCallback )threshold_value_format_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(threshold,"destroy",((GCallback )threshold_scale_destroy_cb),0,0,((GConnectFlags )0));
  return vbox;
}

static GList *actions(PurplePlugin *plugin,gpointer context)
{
  GList *l = (GList *)((void *)0);
  PurplePluginAction *act = (PurplePluginAction *)((void *)0);
  act = purple_plugin_action_new(((const char *)(dgettext("pidgin","Input and Output Settings"))),show_config);
  act -> user_data = get_plugin_config_frame;
  l = g_list_append(l,act);
  act = purple_plugin_action_new(((const char *)(dgettext("pidgin","Microphone Test"))),show_config);
  act -> user_data = get_voice_test_frame;
  l = g_list_append(l,act);
  return l;
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  PurpleMediaManager *manager = purple_media_manager_get();
  purple_media_manager_set_active_element(manager,old_video_src);
  purple_media_manager_set_active_element(manager,old_video_sink);
  purple_media_manager_set_active_element(manager,old_audio_src);
  purple_media_manager_set_active_element(manager,old_audio_sink);
  return (!0);
}
static PidginPluginUiInfo ui_info = {(get_plugin_config_frame), (0), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* page_num (Reserved) */
/* Padding */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gtk-gaim"), (0), ((GList *)((void *)0)), (0), ("gtk-maiku-vvconfig"), ("Voice/Video Settings"), ("2.10.9"), ("Configure your microphone and webcam."), ("Configure microphone and webcam settings for voice/video calls."), ("Mike Ruprecht <cmaiku@gmail.com>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((&ui_info)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), (actions), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< magic          */
/**< major version  */
/**< minor version  */
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**< summary        */
/**< description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/**< prefs_info     */
/**< actions        */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
