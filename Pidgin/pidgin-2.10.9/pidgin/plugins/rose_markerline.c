/*
 * Markerline - Draw a line to indicate new messages in a conversation.
 * Copyright (C) 2006
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02111-1301, USA.
 */
#include "internal.h"
#define PLUGIN_ID			"gtk-plugin_pack-markerline"
#define PLUGIN_NAME			N_("Markerline")
#define PLUGIN_STATIC_NAME	Markerline
#define PLUGIN_SUMMARY		N_("Draw a line to indicate new messages in a conversation.")
#define PLUGIN_DESCRIPTION	N_("Draw a line to indicate new messages in a conversation.")
#define PLUGIN_AUTHOR		"Sadrul H Chowdhury <sadrul@users.sourceforge.net>"
/* System headers */
#include <gdk/gdk.h>
#include <glib.h>
#include <gtk/gtk.h>
/* Purple headers */
#include <gtkconv.h>
#include <gtkimhtml.h>
#include <gtkplugin.h>
#include <version.h>
#define PREF_PREFIX     "/plugins/gtk/" PLUGIN_ID
#define PREF_IMS        PREF_PREFIX "/ims"
#define PREF_CHATS      PREF_PREFIX "/chats"

static int imhtml_expose_cb(GtkWidget *widget,GdkEventExpose *event,PidginConversation *gtkconv)
{
  int y;
  int last_y;
  int offset;
  GdkRectangle visible_rect;
  GtkTextIter iter;
  GdkRectangle buf;
  int pad;
  PurpleConversation *conv = (gtkconv -> active_conv);
  PurpleConversationType type = purple_conversation_get_type(conv);
  if (((type == PURPLE_CONV_TYPE_CHAT) && !(purple_prefs_get_bool("/plugins/gtk/gtk-plugin_pack-markerline/chats") != 0)) || ((type == PURPLE_CONV_TYPE_IM) && !(purple_prefs_get_bool("/plugins/gtk/gtk-plugin_pack-markerline/ims") != 0))) 
    return 0;
  gtk_text_view_get_visible_rect(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type()))),&visible_rect);
  offset = ((gint )((glong )(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"markerline"))));
  if (offset != 0) {
    gtk_text_buffer_get_iter_at_offset(gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type())))),&iter,offset);
    gtk_text_view_get_iter_location(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type()))),(&iter),&buf);
    last_y = (buf.y + buf.height);
    pad = ((gtk_text_view_get_pixels_below_lines(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type())))) + gtk_text_view_get_pixels_above_lines(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type()))))) / 2);
    last_y += pad;
  }
  else 
    last_y = 0;
  gtk_text_view_buffer_to_window_coords(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type()))),GTK_TEXT_WINDOW_TEXT,0,last_y,0,&y);
  if (y >= event -> area.y) {
    GdkColor red = {(0), (0xffff), (0), (0)};
    cairo_t *cr = gdk_cairo_create(((GdkDrawable *)(g_type_check_instance_cast(((GTypeInstance *)(event -> window)),gdk_drawable_get_type()))));
    gdk_cairo_set_source_color(cr,(&red));
    cairo_move_to(cr,0.0,(y + 0.5));
    cairo_rel_line_to(cr,visible_rect.width,0.0);
    cairo_set_line_width(cr,1.0);
    cairo_stroke(cr);
    cairo_destroy(cr);
  }
  return 0;
}

static void update_marker_for_gtkconv(PidginConversation *gtkconv)
{
  GtkTextIter iter;
  GtkTextBuffer *buffer;
  do {
    if (gtkconv != ((PidginConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gtkconv != NULL");
      return ;
    };
  }while (0);
  buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_text_view_get_type()))));
  if (!(gtk_text_buffer_get_char_count(buffer) != 0)) 
    return ;
  gtk_text_buffer_get_end_iter(buffer,&iter);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),((GType )(20 << 2))))),"markerline",((gpointer )((glong )(gtk_text_iter_get_offset((&iter))))));
  gtk_widget_queue_draw((gtkconv -> imhtml));
}

static gboolean focus_removed(GtkWidget *widget,GdkEventVisibility *event,PidginWindow *win)
{
  PurpleConversation *conv;
  PidginConversation *gtkconv;
  conv = pidgin_conv_window_get_active_conversation(win);
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return 0;
    };
  }while (0);
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  update_marker_for_gtkconv(gtkconv);
  return 0;
}
#if 0
#endif

static void page_switched(GtkWidget *widget,GtkWidget *page,gint num,PidginWindow *win)
{
  focus_removed(0,0,win);
}

static void detach_from_gtkconv(PidginConversation *gtkconv,gpointer null)
{
  g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,imhtml_expose_cb,gtkconv);
}

static void detach_from_pidgin_window(PidginWindow *win,gpointer null)
{
  g_list_foreach(pidgin_conv_window_get_gtkconvs(win),((GFunc )detach_from_gtkconv),0);
  g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,page_switched,win);
  g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,focus_removed,win);
  gtk_widget_queue_draw((win -> window));
}

static void attach_to_gtkconv(PidginConversation *gtkconv,gpointer null)
{
  detach_from_gtkconv(gtkconv,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),((GType )(20 << 2))))),"expose_event",((GCallback )imhtml_expose_cb),gtkconv,0,((GConnectFlags )0));
}

static void attach_to_pidgin_window(PidginWindow *win,gpointer null)
{
  g_list_foreach(pidgin_conv_window_get_gtkconvs(win),((GFunc )attach_to_gtkconv),0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),"focus_out_event",((GCallback )focus_removed),win,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),((GType )(20 << 2))))),"switch_page",((GCallback )page_switched),win,0,((GConnectFlags )0));
  gtk_widget_queue_draw((win -> window));
}

static void detach_from_all_windows()
{
  g_list_foreach(pidgin_conv_windows_get_list(),((GFunc )detach_from_pidgin_window),0);
}

static void attach_to_all_windows()
{
  g_list_foreach(pidgin_conv_windows_get_list(),((GFunc )attach_to_pidgin_window),0);
}

static void conv_created(PidginConversation *gtkconv,gpointer null)
{
  PidginWindow *win;
  win = pidgin_conv_get_window(gtkconv);
  if (!(win != 0)) 
    return ;
  detach_from_pidgin_window(win,0);
  attach_to_pidgin_window(win,0);
}

static void jump_to_markerline(PurpleConversation *conv,gpointer null)
{
  PidginConversation *gtkconv = (PidginConversation *)(conv -> ui_data);
  int offset;
  GtkTextIter iter;
  if (!(gtkconv != 0)) 
    return ;
  offset = ((gint )((glong )(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),((GType )(20 << 2))))),"markerline"))));
  gtk_text_buffer_get_iter_at_offset(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type())))).text_buffer,&iter,offset);
  gtk_text_view_scroll_to_iter(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_text_view_get_type()))),&iter,0,(!0),0,0);
}

static void conv_menu_cb(PurpleConversation *conv,GList **list)
{
  PurpleConversationType type = purple_conversation_get_type(conv);
  gboolean enabled = (((type == PURPLE_CONV_TYPE_IM) && (purple_prefs_get_bool("/plugins/gtk/gtk-plugin_pack-markerline/ims") != 0)) || ((type == PURPLE_CONV_TYPE_CHAT) && (purple_prefs_get_bool("/plugins/gtk/gtk-plugin_pack-markerline/chats") != 0)));
  PurpleMenuAction *action = purple_menu_action_new(((const char *)(dgettext("pidgin","Jump to markerline"))),((enabled != 0)?((PurpleCallback )jump_to_markerline) : ((void (*)())((void *)0))),0,0);
   *list = g_list_append( *list,action);
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  attach_to_all_windows();
  purple_signal_connect(pidgin_conversations_get_handle(),"conversation-displayed",plugin,((PurpleCallback )conv_created),0);
  purple_signal_connect(purple_conversations_get_handle(),"conversation-extended-menu",plugin,((PurpleCallback )conv_menu_cb),0);
  return (!0);
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  detach_from_all_windows();
  return (!0);
}

static PurplePluginPrefFrame *get_plugin_pref_frame(PurplePlugin *plugin)
{
  PurplePluginPrefFrame *frame;
  PurplePluginPref *pref;
  frame = purple_plugin_pref_frame_new();
  pref = purple_plugin_pref_new_with_label(((const char *)(dgettext("pidgin","Draw Markerline in "))));
  purple_plugin_pref_frame_add(frame,pref);
  pref = purple_plugin_pref_new_with_name_and_label("/plugins/gtk/gtk-plugin_pack-markerline/ims",((const char *)(dgettext("pidgin","_IM windows"))));
  purple_plugin_pref_frame_add(frame,pref);
  pref = purple_plugin_pref_new_with_name_and_label("/plugins/gtk/gtk-plugin_pack-markerline/chats",((const char *)(dgettext("pidgin","C_hat windows"))));
  purple_plugin_pref_frame_add(frame,pref);
  return frame;
}
static PurplePluginUiInfo prefs_info = {(get_plugin_pref_frame), (0), ((PurplePluginPrefFrame *)((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* padding */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gtk-gaim"), (0), ((GList *)((void *)0)), (0), ("gtk-plugin_pack-markerline"), ("Markerline"), ("2.10.9"), ("Draw a line to indicate new messages in a conversation."), ("Draw a line to indicate new messages in a conversation."), ("Sadrul H Chowdhury <sadrul@users.sourceforge.net>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), (&prefs_info), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* Magic				*/
/* Purple Major Version	*/
/* Purple Minor Version	*/
/* plugin type			*/
/* ui requirement		*/
/* flags				*/
/* dependencies			*/
/* priority				*/
/* plugin id			*/
/* name					*/
/* version				*/
/* summary				*/
/* description			*/
/* author				*/
/* website				*/
/* load					*/
/* unload				*/
/* destroy				*/
/* ui_info				*/
/* extra_info			*/
/* prefs_info			*/
/* actions				*/
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
  purple_prefs_add_none("/plugins/gtk/gtk-plugin_pack-markerline");
  purple_prefs_add_bool("/plugins/gtk/gtk-plugin_pack-markerline/ims",0);
  purple_prefs_add_bool("/plugins/gtk/gtk-plugin_pack-markerline/chats",(!0));
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
