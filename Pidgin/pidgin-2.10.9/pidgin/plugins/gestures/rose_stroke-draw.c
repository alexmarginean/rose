/*
  GNOME stroke implementation
  Copyright (c) 2000, 2001 Dan Nicolaescu
  See the file COPYING for distribution information.
*/
#include "config.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <glib.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include "gstroke.h"
#include "gstroke-internal.h"
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#if !GTK_CHECK_VERSION(2,14,0)
#define gtk_widget_get_window(x) x->window
#endif
static void gstroke_invisible_window_init(GtkWidget *widget);
/*FIXME: Maybe these should be put in a structure, and not static...*/
static Display *gstroke_disp = (Display *)((void *)0);
static Window gstroke_window;
static GC gstroke_gc;
static int mouse_button = 2;
static gboolean draw_strokes = 0;
#define GSTROKE_TIMEOUT_DURATION 10
#define GSTROKE_SIGNALS "gstroke_signals"

struct gstroke_func_and_data 
{
  void (*func)(GtkWidget *, void *);
  gpointer data;
}
;
/*FIXME: maybe it's better to just make 2 static variables, not a
  structure */

struct mouse_position 
{
  struct s_point last_point;
  gboolean invalid;
}
;
static struct mouse_position last_mouse_position;
static guint timer_id;
static void gstroke_execute(GtkWidget *widget,const gchar *name);

static void record_stroke_segment(GtkWidget *widget)
{
  gint x;
  gint y;
  struct gstroke_metrics *metrics;
  do {
    if (widget != ((GtkWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"widget != NULL");
      return ;
    };
  }while (0);
  gtk_widget_get_pointer(widget,&x,&y);
  if (last_mouse_position.invalid != 0) 
    last_mouse_position.invalid = 0;
  else if (gstroke_draw_strokes() != 0) {
#if 1
    XDrawLine(gstroke_disp,gstroke_window,gstroke_gc,last_mouse_position.last_point.x,last_mouse_position.last_point.y,x,y);
/* XFlush (gstroke_disp); */
#else
/* FIXME: this does not work. It will only work if we create a
         corresponding GDK window for stroke_window and draw on
         that... */
#endif
  }
  if ((last_mouse_position.last_point.x != x) || (last_mouse_position.last_point.y != y)) {
    last_mouse_position.last_point.x = x;
    last_mouse_position.last_point.y = y;
    metrics = ((struct gstroke_metrics *)(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"gstroke_metrics")));
    _gstroke_record(x,y,metrics);
  }
}

static gint gstroke_timeout(gpointer data)
{
  GtkWidget *widget;
  do {
    if (data != ((void *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"data != NULL");
      return 0;
    };
  }while (0);
  widget = ((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)data),gtk_widget_get_type())));
  record_stroke_segment(widget);
  return (!0);
}

static void gstroke_cancel(GdkEvent *event)
{
  last_mouse_position.invalid = (!0);
  if (timer_id > 0) 
    g_source_remove(timer_id);
  timer_id = 0;
  if (event != ((GdkEvent *)((void *)0))) 
    gdk_pointer_ungrab(event -> button.time);
  if ((gstroke_draw_strokes() != 0) && (gstroke_disp != ((Display *)((void *)0)))) {
/* get rid of the invisible stroke window */
    XUnmapWindow(gstroke_disp,gstroke_window);
    XFlush(gstroke_disp);
  }
}

static gint process_event(GtkWidget *widget,GdkEvent *event,gpointer data)
{
  static GtkWidget *original_widget = (GtkWidget *)((void *)0);
  static GdkCursor *cursor = (GdkCursor *)((void *)0);
{
    switch(event -> type){
      case GDK_BUTTON_PRESS:
{
        if (event -> button.button != gstroke_get_mouse_button()) {
/* Similar to the bug below catch when any other button is
			 * clicked after the middle button is clicked (but possibly
			 * not released)
			 */
          gstroke_cancel(event);
          original_widget = ((GtkWidget *)((void *)0));
          break; 
        }
/* remeber the widget where
                                   the stroke started */
        original_widget = widget;
        gstroke_invisible_window_init(widget);
        record_stroke_segment(widget);
        if (cursor == ((GdkCursor *)((void *)0))) 
          cursor = gdk_cursor_new(GDK_PENCIL);
        gdk_pointer_grab(gtk_widget_get_window(widget),0,GDK_BUTTON_RELEASE_MASK,0,cursor,event -> button.time);
        timer_id = g_timeout_add(10,gstroke_timeout,widget);
        return (!0);
      }
      case GDK_BUTTON_RELEASE:
{
        if ((event -> button.button != gstroke_get_mouse_button()) || (original_widget == ((GtkWidget *)((void *)0)))) {
/* Nice bug when you hold down one button and press another. */
/* We'll just cancel the gesture instead. */
          gstroke_cancel(event);
          original_widget = ((GtkWidget *)((void *)0));
          break; 
        }
        last_mouse_position.invalid = (!0);
        original_widget = ((GtkWidget *)((void *)0));
        g_source_remove(timer_id);
        gdk_pointer_ungrab(event -> button.time);
        timer_id = 0;
{
          char result[32UL];
          struct gstroke_metrics *metrics;
          metrics = ((struct gstroke_metrics *)(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"gstroke_metrics")));
          if (gstroke_draw_strokes() != 0) {
/* get rid of the invisible stroke window */
            XUnmapWindow(gstroke_disp,gstroke_window);
            XFlush(gstroke_disp);
          }
          _gstroke_canonical(result,metrics);
          gstroke_execute(widget,result);
        }
        return 0;
      }
      default:
{
        break; 
      }
    }
  }
  return 0;
}

void gstroke_set_draw_strokes(gboolean draw)
{
  draw_strokes = draw;
}

gboolean gstroke_draw_strokes()
{
  return draw_strokes;
}

void gstroke_set_mouse_button(gint button)
{
  mouse_button = button;
}

guint gstroke_get_mouse_button()
{
  return mouse_button;
}

void gstroke_enable(GtkWidget *widget)
{
  struct gstroke_metrics *metrics = (struct gstroke_metrics *)(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"gstroke_metrics"));
  if (metrics == ((struct gstroke_metrics *)((void *)0))) {
    metrics = ((struct gstroke_metrics *)(g_malloc((sizeof(struct gstroke_metrics )))));
    metrics -> pointList = ((GSList *)((void *)0));
    metrics -> min_x = 10000;
    metrics -> min_y = 10000;
    metrics -> max_x = 0;
    metrics -> max_y = 0;
    metrics -> point_count = 0;
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"gstroke_metrics",metrics);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"event",((GCallback )process_event),0,0,((GConnectFlags )0));
  }
  else 
    _gstroke_init(metrics);
  last_mouse_position.invalid = (!0);
}

void gstroke_disable(GtkWidget *widget)
{
  g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )process_event),0);
}

guint gstroke_signal_connect(GtkWidget *widget,const gchar *name,void (*func)(GtkWidget *, void *),gpointer data)
{
  struct gstroke_func_and_data *func_and_data;
  GHashTable *hash_table = (GHashTable *)(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"gstroke_signals"));
  if (!(hash_table != 0)) {
    hash_table = g_hash_table_new(g_str_hash,g_str_equal);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"gstroke_signals",((gpointer )hash_table));
  }
  func_and_data = ((struct gstroke_func_and_data *)(g_malloc_n(1,(sizeof(struct gstroke_func_and_data )))));
  func_and_data -> func = func;
  func_and_data -> data = data;
  g_hash_table_insert(hash_table,((gpointer )name),((gpointer )func_and_data));
  return (!0);
}

static void gstroke_execute(GtkWidget *widget,const gchar *name)
{
  GHashTable *hash_table = (GHashTable *)(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"gstroke_signals"));
#if 0
#endif
  if (hash_table != 0) {
    struct gstroke_func_and_data *fd = (struct gstroke_func_and_data *)(g_hash_table_lookup(hash_table,name));
    if (fd != 0) 
      ( *(fd -> func))(widget,(fd -> data));
  }
}

void gstroke_cleanup(GtkWidget *widget)
{
  struct gstroke_metrics *metrics;
  GHashTable *hash_table = (GHashTable *)(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"gstroke_signals"));
  if (hash_table != 0) 
/*  FIXME: does this delete the elements too?  */
    g_hash_table_destroy(hash_table);
  g_object_steal_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"gstroke_signals");
  metrics = ((struct gstroke_metrics *)(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"gstroke_metrics")));
  if (metrics != 0) 
    g_free(metrics);
  g_object_steal_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"gstroke_metrics");
}
/* This function should be written using GTK+ primitives*/

static void gstroke_invisible_window_init(GtkWidget *widget)
{
  XSetWindowAttributes w_attr;
  XWindowAttributes orig_w_attr;
  unsigned long mask;
  unsigned long col_border;
  unsigned long col_background;
  unsigned int border_width;
  XSizeHints hints;
  Display *disp = gdk_x11_drawable_get_xdisplay(gdk_x11_window_get_drawable_impl(gtk_widget_get_window(widget)));
  Window wind = gdk_x11_drawable_get_xid(gtk_widget_get_window(widget));
  int screen = ( *((_XPrivDisplay )disp)).default_screen;
  if (!(gstroke_draw_strokes() != 0)) 
    return ;
  gstroke_disp = disp;
/* X server should save what's underneath */
  XGetWindowAttributes(gstroke_disp,wind,&orig_w_attr);
  hints.x = orig_w_attr.x;
  hints.y = orig_w_attr.y;
  hints.width = orig_w_attr.width;
  hints.height = orig_w_attr.height;
  mask = (1L << 10);
  w_attr.save_under = 1;
/* inhibit all the decorations */
  mask |= (1L << 9);
  w_attr.override_redirect = 1;
/* Don't set a background, transparent window */
  mask |= (1L << 0);
  w_attr.background_pixmap = 0L;
/* Default input window look */
  col_background = ( *((_XPrivDisplay )gstroke_disp)).screens[screen].white_pixel;
/* no border for the window */
#if 0
#endif
  border_width = 0;
  col_border = ( *((_XPrivDisplay )gstroke_disp)).screens[screen].black_pixel;
  gstroke_window = XCreateSimpleWindow(gstroke_disp,wind,0,0,(hints.width - (2 * border_width)),(hints.height - (2 * border_width)),border_width,col_border,col_background);
  gstroke_gc = XCreateGC(gstroke_disp,gstroke_window,0,0);
  XSetFunction(gstroke_disp,gstroke_gc,10);
  XChangeWindowAttributes(gstroke_disp,gstroke_window,mask,&w_attr);
  XSetLineAttributes(gstroke_disp,gstroke_gc,2,0,1,0);
  XMapRaised(gstroke_disp,gstroke_window);
#if 0
/*FIXME: is this call really needed? If yes, does it need the real
    argc and argv? */
/* Receive the close window client message */
/* FIXME: is this really needed? If yes, something should be done
       with wmdelete...*/
#endif
}
