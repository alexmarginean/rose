/*
 * Mouse gestures plugin for Purple
 *
 * Copyright (C) 2003 Christian Hammond.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02111-1301, USA.
 */
#include "internal.h"
#include "pidgin.h"
#include "debug.h"
#include "prefs.h"
#include "signals.h"
#include "version.h"
#include "gtkconv.h"
#include "gtkplugin.h"
#include "gtkutils.h"
#include "gstroke.h"
#define GESTURES_PLUGIN_ID "gtk-x11-gestures"

static void stroke_close(GtkWidget *widget,void *data)
{
  PurpleConversation *conv;
  PidginConversation *gtkconv;
  conv = ((PurpleConversation *)data);
/* Double-check */
  if (!(purple_conversation_get_ui_ops(conv) == pidgin_conversations_get_conv_ui_ops())) 
    return ;
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  gstroke_cleanup((gtkconv -> imhtml));
  purple_conversation_destroy(conv);
}

static void switch_page(PidginWindow *win,GtkDirectionType dir)
{
  int count;
  int current;
  count = gtk_notebook_get_n_pages(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))));
  current = gtk_notebook_get_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))));
  if (dir == GTK_DIR_LEFT) {
    gtk_notebook_set_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),(current - 1));
  }
  else if (dir == GTK_DIR_RIGHT) {
    if (current == (count - 1)) 
      gtk_notebook_set_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),0);
    else 
      gtk_notebook_set_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),(current + 1));
  }
}

static void stroke_prev_tab(GtkWidget *widget,void *data)
{
  PurpleConversation *conv;
  PidginConversation *gtkconv;
  PidginWindow *win;
  conv = ((PurpleConversation *)data);
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  win = (gtkconv -> win);
  switch_page(win,GTK_DIR_LEFT);
}

static void stroke_next_tab(GtkWidget *widget,void *data)
{
  PurpleConversation *conv;
  PidginWindow *win;
  conv = ((PurpleConversation *)data);
  win = ( *((PidginConversation *)(conv -> ui_data))).win;
  switch_page(win,GTK_DIR_RIGHT);
}

static void stroke_new_win(GtkWidget *widget,void *data)
{
  PidginWindow *new_win;
  PidginWindow *old_win;
  PurpleConversation *conv;
  conv = ((PurpleConversation *)data);
  old_win = ( *((PidginConversation *)(conv -> ui_data))).win;
  if (pidgin_conv_window_get_gtkconv_count(old_win) <= 1) 
    return ;
  new_win = pidgin_conv_window_new();
  pidgin_conv_window_remove_gtkconv(old_win,((PidginConversation *)(conv -> ui_data)));
  pidgin_conv_window_add_gtkconv(new_win,((PidginConversation *)(conv -> ui_data)));
  pidgin_conv_window_show(new_win);
}

static void attach_signals(PurpleConversation *conv)
{
  PidginConversation *gtkconv;
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  gstroke_enable((gtkconv -> imhtml));
  gstroke_signal_connect((gtkconv -> imhtml),"14789",stroke_close,conv);
  gstroke_signal_connect((gtkconv -> imhtml),"1456",stroke_close,conv);
  gstroke_signal_connect((gtkconv -> imhtml),"1489",stroke_close,conv);
  gstroke_signal_connect((gtkconv -> imhtml),"74123",stroke_next_tab,conv);
  gstroke_signal_connect((gtkconv -> imhtml),"7456",stroke_next_tab,conv);
  gstroke_signal_connect((gtkconv -> imhtml),"96321",stroke_prev_tab,conv);
  gstroke_signal_connect((gtkconv -> imhtml),"9654",stroke_prev_tab,conv);
  gstroke_signal_connect((gtkconv -> imhtml),"25852",stroke_new_win,conv);
}

static void new_conv_cb(PurpleConversation *conv)
{
  if (purple_conversation_get_ui_ops(conv) == pidgin_conversations_get_conv_ui_ops()) 
    attach_signals(conv);
}
#if 0
#if GTK_CHECK_VERSION(2,4,0)
#else
#endif
#endif

static void toggle_draw_cb(GtkToggleButton *toggle,gpointer data)
{
  purple_prefs_set_bool("/plugins/gtk/X11/gestures/visual",gtk_toggle_button_get_active(toggle));
}

static void visual_pref_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  gstroke_set_draw_strokes(((gboolean )((glong )value)));
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  PurpleConversation *conv;
  GList *l;
  for (l = purple_get_conversations(); l != ((GList *)((void *)0)); l = (l -> next)) {
    conv = ((PurpleConversation *)(l -> data));
    if (!(purple_conversation_get_ui_ops(conv) == pidgin_conversations_get_conv_ui_ops())) 
      continue; 
    attach_signals(conv);
  }
  purple_signal_connect(purple_conversations_get_handle(),"conversation-created",plugin,((PurpleCallback )new_conv_cb),0);
  return (!0);
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  PurpleConversation *conv;
  PidginConversation *gtkconv;
  GList *l;
  for (l = purple_get_conversations(); l != ((GList *)((void *)0)); l = (l -> next)) {
    conv = ((PurpleConversation *)(l -> data));
    if (!(purple_conversation_get_ui_ops(conv) == pidgin_conversations_get_conv_ui_ops())) 
      continue; 
    gtkconv = ((PidginConversation *)(conv -> ui_data));
    gstroke_cleanup((gtkconv -> imhtml));
    gstroke_disable((gtkconv -> imhtml));
  }
  return (!0);
}

static GtkWidget *get_config_frame(PurplePlugin *plugin)
{
  GtkWidget *ret;
  GtkWidget *vbox;
  GtkWidget *toggle;
#if 0
#if GTK_CHECK_VERSION(2,4,0)
#endif
#endif
/* Outside container */
  ret = gtk_vbox_new(0,18);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_container_get_type()))),12);
/* Configuration frame */
  vbox = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Mouse Gestures Configuration"))));
#if 0
#if GTK_CHECK_VERSION(2,4,0)
/* Mouse button drop-down menu */
#else
/* Mouse button drop-down menu */
#endif
#endif
/* "Visual gesture display" checkbox */
  toggle = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","_Visual gesture display"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),toggle,0,0,0);
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)toggle),gtk_toggle_button_get_type()))),purple_prefs_get_bool("/plugins/gtk/X11/gestures/visual"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toggle),((GType )(20 << 2))))),"toggled",((GCallback )toggle_draw_cb),0,0,((GConnectFlags )0));
  gtk_widget_show_all(ret);
  return ret;
}
static PidginPluginUiInfo ui_info = {(get_config_frame), (0), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* page_num (Reserved) */
/* padding */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gtk-gaim"), (0), ((GList *)((void *)0)), (0), ("gtk-x11-gestures"), ("Mouse Gestures"), ("2.10.9"), ("Provides support for mouse gestures"), ("Allows support for mouse gestures in conversation windows. Drag the middle mouse button to perform certain actions:\n \342\200\242 Drag down and then to the right to close a conversation.\n \342\200\242 Drag up and then to the left to switch to the previous conversation.\n \342\200\242 Drag up and then to the right to switch to the next conversation."), ("Christian Hammond <chipx86@gnupdate.org>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((&ui_info)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
  purple_prefs_add_none("/plugins/gtk");
  purple_prefs_add_none("/plugins/gtk/X11");
  purple_prefs_add_none("/plugins/gtk/X11/gestures");
  purple_prefs_add_bool("/plugins/gtk/X11/gestures/visual",0);
  purple_prefs_connect_callback(plugin,"/plugins/gtk/X11/gestures/visual",visual_pref_cb,0);
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
