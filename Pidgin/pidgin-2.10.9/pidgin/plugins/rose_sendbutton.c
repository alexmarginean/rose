/*
 * SendButton - Add a Send button to the conversation window entry area.
 * Copyright (C) 2008 Etan Reisner <deryni@pidgin.im>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301, USA.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "internal.h"
#include "version.h"
#include "pidgin.h"
#include "gtkconv.h"
#include "gtkplugin.h"

static void send_button_cb(GtkButton *button,PidginConversation *gtkconv)
{
  g_signal_emit_by_name((gtkconv -> entry),"message_send");
}

static void input_buffer_changed(GtkTextBuffer *text_buffer,GtkWidget *send_button)
{
  if (gtk_text_buffer_get_char_count(text_buffer) != 0) 
    gtk_widget_set_sensitive(send_button,(!0));
  else 
    gtk_widget_set_sensitive(send_button,0);
}

static void create_send_button_pidgin(PidginConversation *gtkconv)
{
  GtkWidget *send_button;
  GtkTextBuffer *buf;
  guint signal_id;
  send_button = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> lower_hbox)),((GType )(20 << 2))))),"send_button"));
  if (send_button != ((GtkWidget *)((void *)0))) 
    return ;
  send_button = gtk_button_new_with_mnemonic(((const char *)(dgettext("pidgin","_Send"))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)send_button),((GType )(20 << 2))))),"clicked",((GCallback )send_button_cb),gtkconv,0,((GConnectFlags )0));
  gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> lower_hbox)),gtk_box_get_type()))),send_button,0,0,0);
  gtk_widget_show(send_button);
  buf = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_text_view_get_type()))));
  if (buf != 0) {
    signal_id = (g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buf),((GType )(20 << 2))))),"changed",((GCallback )input_buffer_changed),send_button,0,((GConnectFlags )0)));
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)send_button),((GType )(20 << 2))))),"buffer-signal",((gpointer )((glong )signal_id)));
    input_buffer_changed(buf,send_button);
  }
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> lower_hbox)),((GType )(20 << 2))))),"send_button",send_button);
}

static void remove_send_button_pidgin(PidginConversation *gtkconv)
{
  GtkWidget *send_button = (GtkWidget *)((void *)0);
  send_button = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> lower_hbox)),((GType )(20 << 2))))),"send_button"));
  if (send_button != ((GtkWidget *)((void *)0))) {
    GtkTextBuffer *buf;
    guint signal_id;
    buf = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_text_view_get_type()))));
    signal_id = ((gint )((glong )(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)send_button),((GType )(20 << 2))))),"buffer-signal"))));
    if ((buf != 0) && (signal_id != 0U)) 
      g_signal_handler_disconnect(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buf),((GType )(20 << 2))))),signal_id);
    gtk_widget_destroy(send_button);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> lower_hbox)),((GType )(20 << 2))))),"send_button",0);
  }
}

static void conversation_displayed_cb(PidginConversation *gtkconv)
{
  GtkWidget *send_button = (GtkWidget *)((void *)0);
  send_button = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> lower_hbox)),((GType )(20 << 2))))),"send_button"));
  if (send_button == ((GtkWidget *)((void *)0))) {
    create_send_button_pidgin(gtkconv);
  }
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  GList *convs = purple_get_conversations();
  void *gtk_conv_handle = pidgin_conversations_get_handle();
  purple_signal_connect(gtk_conv_handle,"conversation-displayed",plugin,((PurpleCallback )conversation_displayed_cb),0);
/*
	purple_signal_connect(gtk_conv_handle, "conversation-hiding", plugin,
	                      PURPLE_CALLBACK(conversation_hiding_cb), NULL);
	 */
  while(convs != 0){
    PurpleConversation *conv = (PurpleConversation *)(convs -> data);
/* Setup Send button */
    if (purple_conversation_get_ui_ops(conv) == pidgin_conversations_get_conv_ui_ops()) {
      create_send_button_pidgin(((PidginConversation *)(conv -> ui_data)));
    }
    convs = (convs -> next);
  }
  return (!0);
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  GList *convs = purple_get_conversations();
  while(convs != 0){
    PurpleConversation *conv = (PurpleConversation *)(convs -> data);
/* Remove Send button */
    if (purple_conversation_get_ui_ops(conv) == pidgin_conversations_get_conv_ui_ops()) {
      remove_send_button_pidgin(((PidginConversation *)(conv -> ui_data)));
    }
    convs = (convs -> next);
  }
  return (!0);
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gtk-gaim"), (0), ((GList *)((void *)0)), (0), ("gtksendbutton"), ("Send Button"), ("2.10.9"), ("Conversation Window Send Button."), ("Adds a Send button to the entry area of the conversation window. Intended for use when no physical keyboard is present."), ("Etan Reisner <deryni@pidgin.im>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< major version */
/**< minor version */
/**< type */
/**< ui_requirement */
/**< flags */
/**< dependencies */
/**< priority */
/**< id */
/**< name */
/**< version */
/**< summary */
/**< description */
/**< author */
/**< homepage */
/**< load */
/**< unload */
/**< destroy */
/**< ui_info */
/**< extra_info */
/**< prefs_info */
/**< actions */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
