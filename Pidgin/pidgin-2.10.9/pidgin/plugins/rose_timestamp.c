/*
 * Purple - iChat-style timestamps
 *
 * Copyright (C) 2002-2003, Sean Egan
 * Copyright (C) 2003, Chris J. Friesen <Darth_Sebulba04@yahoo.com>
 * Copyright (C) 2007, Andrew Gaul <andrew@gaul.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "conversation.h"
#include "debug.h"
#include "prefs.h"
#include "signals.h"
#include "version.h"
#include "gtkimhtml.h"
#include "gtkplugin.h"
#include "gtkprefs.h"
#include "gtkutils.h"
#define TIMESTAMP_PLUGIN_ID "gtk-timestamp"
/* minutes externally, seconds internally, and milliseconds in preferences */
static int interval = 5 * 60;

static void timestamp_display(PurpleConversation *conv,time_t then,time_t now)
{
  PidginConversation *gtk_conv = (PidginConversation *)(conv -> ui_data);
  GtkWidget *imhtml = (gtk_conv -> imhtml);
  GtkTextBuffer *buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))));
  GtkTextIter iter;
  const char *mdate;
  int y;
  int height;
  GdkRectangle rect;
  gboolean scrolled = 0;
  GtkTextTag *tag;
/* display timestamp */
  mdate = purple_utf8_strftime((((then == 0)?"%H:%M" : "\n%H:%M")),(localtime((&now))));
  gtk_text_buffer_get_end_iter(buffer,&iter);
/* is the view already scrolled? */
  gtk_text_view_get_visible_rect(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),&rect);
  gtk_text_view_get_line_yrange(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),(&iter),&y,&height);
  if (((y + height) - (rect.y + rect.height)) > height) 
    scrolled = (!0);
  if ((tag = gtk_text_tag_table_lookup(gtk_text_buffer_get_tag_table(buffer),"TIMESTAMP")) == ((GtkTextTag *)((void *)0))) 
    tag = gtk_text_buffer_create_tag(buffer,"TIMESTAMP","foreground","#888888","justification",GTK_JUSTIFY_CENTER,"weight",PANGO_WEIGHT_BOLD,((void *)((void *)0)));
  gtk_text_buffer_insert_with_tags(buffer,&iter,mdate,(strlen(mdate)),tag,((void *)((void *)0)));
/* scroll view if necessary */
  gtk_text_view_get_visible_rect(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),&rect);
  gtk_text_view_get_line_yrange(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),(&iter),&y,&height);
  if ((!(scrolled != 0) && (((y + height) - (rect.y + rect.height)) > height)) && (gtk_text_buffer_get_char_count(buffer) != 0)) {
    gboolean smooth = purple_prefs_get_bool("/pidgin/conversations/use_smooth_scrolling");
    gtk_imhtml_scroll_to_end(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),smooth);
  }
}

static gboolean timestamp_displaying_conv_msg(PurpleAccount *account,const char *who,char **buffer,PurpleConversation *conv,PurpleMessageFlags flags,void *data)
{
  time_t now = ((time(0) / interval) * interval);
  time_t then;
  if (!(g_list_find(purple_get_conversations(),conv) != 0)) 
    return 0;
  then = ((gint )((glong )(purple_conversation_get_data(conv,"timestamp-last"))));
  if ((now - then) >= interval) {
    timestamp_display(conv,then,now);
    purple_conversation_set_data(conv,"timestamp-last",((gpointer )((glong )now)));
  }
  return 0;
}

static void timestamp_new_convo(PurpleConversation *conv)
{
  if (!(g_list_find(purple_get_conversations(),conv) != 0)) 
    return ;
  purple_conversation_set_data(conv,"timestamp-last",0);
}

static void set_timestamp(GtkWidget *spinner,void *null)
{
  int tm;
  tm = gtk_spin_button_get_value_as_int(((GtkSpinButton *)(g_type_check_instance_cast(((GTypeInstance *)spinner),gtk_spin_button_get_type()))));
  purple_debug(PURPLE_DEBUG_MISC,"timestamp","setting interval to %d minutes\n",tm);
  interval = (tm * 60);
  purple_prefs_set_int("/plugins/gtk/timestamp/interval",(interval * 1000));
}

static GtkWidget *get_config_frame(PurplePlugin *plugin)
{
  GtkWidget *ret;
  GtkWidget *frame;
  GtkWidget *label;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkObject *adj;
  GtkWidget *spinner;
  ret = gtk_vbox_new(0,18);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_container_get_type()))),12);
  frame = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Display Timestamps Every"))));
  vbox = gtk_vbox_new(0,5);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)frame),gtk_container_get_type()))),vbox);
  hbox = gtk_hbox_new(0,5);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,5);
/* XXX limit to divisors of 60? */
  adj = gtk_adjustment_new((interval / 60),1,60,1,0,0);
  spinner = gtk_spin_button_new(((GtkAdjustment *)(g_type_check_instance_cast(((GTypeInstance *)adj),gtk_adjustment_get_type()))),0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),spinner,(!0),(!0),0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)spinner),((GType )(20 << 2))))),"value-changed",((GCallback )set_timestamp),0,0,((GConnectFlags )0));
  label = gtk_label_new(((const char *)(dgettext("pidgin","minutes"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,5);
  gtk_widget_show_all(ret);
  return ret;
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  void *conv_handle = purple_conversations_get_handle();
  void *gtkconv_handle = pidgin_conversations_get_handle();
/* lower priority to display initial timestamp after logged messages */
  purple_signal_connect_priority(conv_handle,"conversation-created",plugin,((PurpleCallback )timestamp_new_convo),0,0 + 1);
  purple_signal_connect(gtkconv_handle,"displaying-chat-msg",plugin,((PurpleCallback )timestamp_displaying_conv_msg),0);
  purple_signal_connect(gtkconv_handle,"displaying-im-msg",plugin,((PurpleCallback )timestamp_displaying_conv_msg),0);
  interval = (purple_prefs_get_int("/plugins/gtk/timestamp/interval") / 1000);
  return (!0);
}
static PidginPluginUiInfo ui_info = {(get_config_frame), (0), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* page_num (Reserved) */
/* padding */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gtk-gaim"), (0), ((GList *)((void *)0)), (0), ("gtk-timestamp"), ("Timestamp"), ("2.10.9"), ("Display iChat-style timestamps"), ("Display iChat-style timestamps every N minutes."), ("Sean Egan <seanegan@gmail.com>"), ("http://pidgin.im/"), (plugin_load), ((gboolean (*)(PurplePlugin *))((void *)0)), ((void (*)(PurplePlugin *))((void *)0)), ((&ui_info)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
  purple_prefs_add_none("/plugins/gtk/timestamp");
  purple_prefs_add_int("/plugins/gtk/timestamp/interval",(interval * 1000));
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
