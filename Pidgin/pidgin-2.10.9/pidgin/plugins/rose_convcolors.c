/*
 * Conversation Colors
 * Copyright (C) 2006
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02111-1301, USA.
 */
#include "internal.h"
#define PLUGIN_ID			"gtk-plugin_pack-convcolors"
#define PLUGIN_NAME			N_("Conversation Colors")
#define PLUGIN_STATIC_NAME	ConversationColors
#define PLUGIN_SUMMARY		N_("Customize colors in the conversation window")
#define PLUGIN_DESCRIPTION	N_("Customize colors in the conversation window")
#define PLUGIN_AUTHOR		"Sadrul H Chowdhury <sadrul@users.sourceforge.net>"
/* System headers */
#include <gdk/gdk.h>
#include <glib.h>
#include <gtk/gtk.h>
/* Purple headers */
#include <gtkplugin.h>
#include <version.h>
#include <conversation.h>
#include <gtkconv.h>
#include <gtkprefs.h>
#include <gtkutils.h>
#define	PREF_PREFIX	"/plugins/gtk/" PLUGIN_ID
#define PREF_IGNORE	PREF_PREFIX "/ignore_incoming"
#define PREF_CHATS	PREF_PREFIX "/chats"
#define PREF_IMS	PREF_PREFIX "/ims"
#define	PREF_SEND	PREF_PREFIX "/send"
#define	PREF_SEND_C	PREF_SEND "/color"
#define	PREF_SEND_F	PREF_SEND "/format"
#define	PREF_SEND_E	PREF_SEND "/enabled"
#define PREF_RECV	PREF_PREFIX "/recv"
#define	PREF_RECV_C	PREF_RECV "/color"
#define	PREF_RECV_F	PREF_RECV "/format"
#define	PREF_RECV_E	PREF_RECV "/enabled"
#define PREF_SYSTEM	PREF_PREFIX "/system"
#define	PREF_SYSTEM_C	PREF_SYSTEM "/color"
#define	PREF_SYSTEM_F	PREF_SYSTEM "/format"
#define	PREF_SYSTEM_E	PREF_SYSTEM "/enabled"
#define PREF_ERROR	PREF_PREFIX "/error"
#define	PREF_ERROR_C	PREF_ERROR "/color"
#define	PREF_ERROR_F	PREF_ERROR "/format"
#define	PREF_ERROR_E	PREF_ERROR "/enabled"
#define PREF_NICK	PREF_PREFIX "/nick"
#define	PREF_NICK_C	PREF_NICK "/color"
#define	PREF_NICK_F	PREF_NICK "/format"
#define	PREF_NICK_E	PREF_NICK "/enabled"
enum __unnamed_enum___F0_L73_C1_FONT_BOLD__COMMA__FONT_ITALIC__COMMA__FONT_UNDERLINE {FONT_BOLD=1,FONT_ITALIC,FONT_UNDERLINE=4};
static struct __unnamed_class___F0_L80_C8_unknown_scope_and_name_variable_declaration__variable_type_L1611R_variable_name_unknown_scope_and_name__scope__flag__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__prefix__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__text {
PurpleMessageFlags flag;
char *prefix;
const char *text;}formats[] = {{(PURPLE_MESSAGE_ERROR), ("/plugins/gtk/gtk-plugin_pack-convcolors/error"), ("Error Messages")}, {(PURPLE_MESSAGE_NICK), ("/plugins/gtk/gtk-plugin_pack-convcolors/nick"), ("Highlighted Messages")}, {(PURPLE_MESSAGE_SYSTEM), ("/plugins/gtk/gtk-plugin_pack-convcolors/system"), ("System Messages")}, {(PURPLE_MESSAGE_SEND), ("/plugins/gtk/gtk-plugin_pack-convcolors/send"), ("Sent Messages")}, {(PURPLE_MESSAGE_RECV), ("/plugins/gtk/gtk-plugin_pack-convcolors/recv"), ("Received Messages")}, {(0), ((char *)((void *)0)), ((const char *)((void *)0))}};

static gboolean displaying_msg(PurpleAccount *account,const char *who,char **displaying,PurpleConversation *conv,PurpleMessageFlags flags)
{
  int i;
  char tmp[128UL];
  char *t;
  gboolean bold;
  gboolean italic;
  gboolean underline;
  int f;
  const char *color;
  gboolean rtl = 0;
{
    for (i = 0; formats[i].prefix != 0; i++) 
      if ((flags & formats[i].flag) != 0U) 
        break; 
  }
  if (!(formats[i].prefix != 0)) 
    return 0;
  g_snprintf(tmp,(sizeof(tmp)),"%s/enabled",formats[i].prefix);
  if ((!(purple_prefs_get_bool(tmp) != 0) || (((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) && !(purple_prefs_get_bool("/plugins/gtk/gtk-plugin_pack-convcolors/ims") != 0))) || (((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) && !(purple_prefs_get_bool("/plugins/gtk/gtk-plugin_pack-convcolors/chats") != 0))) 
    return 0;
  g_snprintf(tmp,(sizeof(tmp)),"%s/color",formats[i].prefix);
  color = purple_prefs_get_string(tmp);
  g_snprintf(tmp,(sizeof(tmp)),"%s/format",formats[i].prefix);
  f = purple_prefs_get_int(tmp);
  bold = (f & FONT_BOLD);
  italic = (f & FONT_ITALIC);
  underline = (f & FONT_UNDERLINE);
  rtl = purple_markup_is_rtl(( *displaying));
  if (purple_prefs_get_bool("/plugins/gtk/gtk-plugin_pack-convcolors/ignore_incoming") != 0) {
/* This seems to be necessary, especially for received messages. */
    t =  *displaying;
     *displaying = purple_strreplace(t,"\n","<br>");
    g_free(t);
    t =  *displaying;
     *displaying = purple_markup_strip_html(t);
    g_free(t);
    t =  *displaying;
     *displaying = g_markup_escape_text(t,(-1));
    g_free(t);
/* Restore the links */
    t =  *displaying;
     *displaying = purple_markup_linkify(t);
    g_free(t);
  }
  if ((color != 0) && (( *color) != 0)) {
    t =  *displaying;
     *displaying = g_strdup_printf("<FONT COLOR=\"%s\">%s</FONT>",color,t);
    g_free(t);
  }
  t =  *displaying;
   *displaying = g_strdup_printf("%s%s%s%s%s%s%s%s%s",((bold != 0)?"<B>" : "</B>"),((italic != 0)?"<I>" : "</I>"),((underline != 0)?"<U>" : "</U>"),((rtl != 0)?"<SPAN style=\"direction:rtl;text-align:right;\">" : ""),t,((rtl != 0)?"</SPAN>" : ""),((bold != 0)?"</B>" : "<B>"),((italic != 0)?"</I>" : "<I>"),((underline != 0)?"</U>" : "<U>"));
  g_free(t);
  return 0;
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  purple_signal_connect(pidgin_conversations_get_handle(),"displaying-im-msg",plugin,((PurpleCallback )displaying_msg),0);
  purple_signal_connect(pidgin_conversations_get_handle(),"displaying-chat-msg",plugin,((PurpleCallback )displaying_msg),0);
  return (!0);
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  return (!0);
}
/* Ripped from PurpleRC */

static void color_response(GtkDialog *color_dialog,gint response,const char *data)
{
  if (response == GTK_RESPONSE_OK) {
#if GTK_CHECK_VERSION(2,14,0)
    GtkWidget *colorsel = gtk_color_selection_dialog_get_color_selection(((GtkColorSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)color_dialog),gtk_color_selection_dialog_get_type()))));
#else
#endif
    GdkColor color;
    char colorstr[8UL];
    char tmp[128UL];
    gtk_color_selection_get_current_color(((GtkColorSelection *)(g_type_check_instance_cast(((GTypeInstance *)colorsel),gtk_color_selection_get_type()))),&color);
    g_snprintf(colorstr,(sizeof(colorstr)),"#%02X%02X%02X",(color.red / 256),(color.green / 256),(color.blue / 256));
    g_snprintf(tmp,(sizeof(tmp)),"%s/color",data);
    purple_prefs_set_string(tmp,colorstr);
  }
  gtk_widget_destroy(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)color_dialog),gtk_widget_get_type()))));
}

static void set_color(GtkWidget *widget,const char *data)
{
  GtkWidget *color_dialog = (GtkWidget *)((void *)0);
  GdkColor color;
  char title[128UL];
  char tmp[128UL];
  g_snprintf(title,(sizeof(title)),((const char *)(dgettext("pidgin","Select Color for %s"))),((const char *)(dgettext("pidgin",data))));
  color_dialog = gtk_color_selection_dialog_new(title);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)color_dialog),((GType )(20 << 2))))),"response",((GCallback )color_response),((gpointer )data),0,((GConnectFlags )0));
  g_snprintf(tmp,(sizeof(tmp)),"%s/color",data);
  if (gdk_color_parse(purple_prefs_get_string(tmp),&color) != 0) {
#if GTK_CHECK_VERSION(2,14,0)
    gtk_color_selection_set_current_color(((GtkColorSelection *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_color_selection_dialog_get_color_selection(((GtkColorSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)color_dialog),gtk_color_selection_dialog_get_type())))))),gtk_color_selection_get_type()))),(&color));
#else
#endif
  }
  gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)color_dialog),gtk_window_get_type()))));
}

static void toggle_enabled(GtkWidget *widget,gpointer data)
{
  const char *prefix = ((char *)data);
  gboolean e;
  char tmp[128UL];
  g_snprintf(tmp,(sizeof(tmp)),"%s/enabled",prefix);
  e = purple_prefs_get_bool(tmp);
  purple_prefs_set_bool(tmp,!(e != 0));
}

static void toggle_something(const char *prefix,int format)
{
  int f;
  char tmp[128UL];
  g_snprintf(tmp,(sizeof(tmp)),"%s/format",prefix);
  f = purple_prefs_get_int(tmp);
  f ^= format;
  purple_prefs_set_int(tmp,f);
}

static void toggle_bold(GtkWidget *widget,gpointer data)
{
  toggle_something(data,FONT_BOLD);
}

static void toggle_italic(GtkWidget *widget,gpointer data)
{
  toggle_something(data,FONT_ITALIC);
}

static void toggle_underline(GtkWidget *widget,gpointer data)
{
  toggle_something(data,FONT_UNDERLINE);
}

static void enable_toggled(const char *name,PurplePrefType type,gconstpointer val,gpointer data)
{
  GtkWidget *widget = (GtkWidget *)data;
  gtk_widget_set_sensitive(widget,((gint )((glong )val)));
}

static void disconnect_prefs_callbacks(GtkObject *object,gpointer data)
{
  PurplePlugin *plugin = (PurplePlugin *)data;
  purple_prefs_disconnect_by_handle(plugin);
}

static GtkWidget *get_config_frame(PurplePlugin *plugin)
{
  GtkWidget *ret;
  GtkWidget *frame;
  int i;
  ret = gtk_vbox_new(0,18);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_container_get_type()))),12);
  for (i = 0; formats[i].prefix != 0; i++) {
    char tmp[128UL];
    char tmp2[128UL];
    int f;
    gboolean e;
    GtkWidget *vbox;
    GtkWidget *hbox;
    GtkWidget *button;
    g_snprintf(tmp2,(sizeof(tmp2)),"%s/enabled",formats[i].prefix);
    e = purple_prefs_get_bool(tmp2);
    g_snprintf(tmp,(sizeof(tmp)),"%s/format",formats[i].prefix);
    f = purple_prefs_get_int(tmp);
    frame = pidgin_make_frame(ret,((const char *)(dgettext("pidgin",formats[i].text))));
    vbox = gtk_vbox_new(0,6);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)frame),gtk_box_get_type()))),vbox,0,0,0);
    hbox = gtk_hbox_new(0,6);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
    button = gtk_check_button_new_with_label(((const char *)(dgettext("pidgin","Enabled"))));
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),button,0,0,0);
    if (e != 0) 
      gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_toggle_button_get_type()))),(!0));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )toggle_enabled),formats[i].prefix,0,((GConnectFlags )0));
    button = pidgin_pixbuf_button_from_stock(" Color","gtk-select-color",PIDGIN_BUTTON_HORIZONTAL);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),button,0,0,0);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )set_color),formats[i].prefix,0,((GConnectFlags )0));
    gtk_widget_set_sensitive(button,e);
    purple_prefs_connect_callback(plugin,tmp2,enable_toggled,button);
    button = gtk_check_button_new_with_label(((const char *)(dgettext("pidgin","Bold"))));
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),button,0,0,0);
    if ((f & FONT_BOLD) != 0) 
      gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_toggle_button_get_type()))),(!0));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )toggle_bold),formats[i].prefix,0,((GConnectFlags )0));
    gtk_widget_set_sensitive(button,e);
    purple_prefs_connect_callback(plugin,tmp2,enable_toggled,button);
    button = gtk_check_button_new_with_label(((const char *)(dgettext("pidgin","Italic"))));
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),button,0,0,0);
    if ((f & FONT_ITALIC) != 0) 
      gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_toggle_button_get_type()))),(!0));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )toggle_italic),formats[i].prefix,0,((GConnectFlags )0));
    gtk_widget_set_sensitive(button,e);
    purple_prefs_connect_callback(plugin,tmp2,enable_toggled,button);
    button = gtk_check_button_new_with_label(((const char *)(dgettext("pidgin","Underline"))));
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),button,0,0,0);
    if ((f & FONT_UNDERLINE) != 0) 
      gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_toggle_button_get_type()))),(!0));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )toggle_underline),formats[i].prefix,0,((GConnectFlags )0));
    gtk_widget_set_sensitive(button,e);
    purple_prefs_connect_callback(plugin,tmp2,enable_toggled,button);
  }
  g_signal_connect_data(((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_object_get_type()))),"destroy",((GCallback )disconnect_prefs_callbacks),plugin,0,((GConnectFlags )0));
  frame = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","General"))));
  pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","Ignore incoming format"))),"/plugins/gtk/gtk-plugin_pack-convcolors/ignore_incoming",frame);
  pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","Apply in Chats"))),"/plugins/gtk/gtk-plugin_pack-convcolors/chats",frame);
  pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","Apply in IMs"))),"/plugins/gtk/gtk-plugin_pack-convcolors/ims",frame);
  gtk_widget_show_all(ret);
  return ret;
}
static PidginPluginUiInfo ui_info = {(get_config_frame), (0), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* padding */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gtk-gaim"), (0), ((GList *)((void *)0)), (0), ("gtk-plugin_pack-convcolors"), ("Conversation Colors"), ("2.10.9"), ("Customize colors in the conversation window"), ("Customize colors in the conversation window"), ("Sadrul H Chowdhury <sadrul@users.sourceforge.net>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((&ui_info)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* Magic              */
/* Purple Major Version */
/* Purple Minor Version */
/* plugin type        */
/* ui requirement     */
/* flags              */
/* dependencies       */
/* priority           */
/* plugin id          */
/* name               */
/* version            */
/* summary            */
/* description        */
/* author             */
/* website            */
/* load               */
/* unload             */
/* destroy            */
/* ui_info            */
/* extra_info         */
/* prefs_info         */
/* actions            */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
  purple_prefs_add_none("/plugins/gtk/gtk-plugin_pack-convcolors");
  purple_prefs_add_bool("/plugins/gtk/gtk-plugin_pack-convcolors/ignore_incoming",(!0));
  purple_prefs_add_bool("/plugins/gtk/gtk-plugin_pack-convcolors/chats",(!0));
  purple_prefs_add_bool("/plugins/gtk/gtk-plugin_pack-convcolors/ims",(!0));
  purple_prefs_add_none("/plugins/gtk/gtk-plugin_pack-convcolors/send");
  purple_prefs_add_none("/plugins/gtk/gtk-plugin_pack-convcolors/recv");
  purple_prefs_add_none("/plugins/gtk/gtk-plugin_pack-convcolors/system");
  purple_prefs_add_none("/plugins/gtk/gtk-plugin_pack-convcolors/error");
  purple_prefs_add_none("/plugins/gtk/gtk-plugin_pack-convcolors/nick");
  purple_prefs_add_string("/plugins/gtk/gtk-plugin_pack-convcolors/send/color","#909090");
  purple_prefs_add_string("/plugins/gtk/gtk-plugin_pack-convcolors/recv/color","#000000");
  purple_prefs_add_string("/plugins/gtk/gtk-plugin_pack-convcolors/system/color","#50a050");
  purple_prefs_add_string("/plugins/gtk/gtk-plugin_pack-convcolors/error/color","#ff0000");
  purple_prefs_add_string("/plugins/gtk/gtk-plugin_pack-convcolors/nick/color","#0000dd");
  purple_prefs_add_int("/plugins/gtk/gtk-plugin_pack-convcolors/send/format",0);
  purple_prefs_add_int("/plugins/gtk/gtk-plugin_pack-convcolors/recv/format",0);
  purple_prefs_add_int("/plugins/gtk/gtk-plugin_pack-convcolors/system/format",FONT_ITALIC);
  purple_prefs_add_int("/plugins/gtk/gtk-plugin_pack-convcolors/error/format",FONT_BOLD | FONT_UNDERLINE);
  purple_prefs_add_int("/plugins/gtk/gtk-plugin_pack-convcolors/nick/format",FONT_BOLD);
  purple_prefs_add_bool("/plugins/gtk/gtk-plugin_pack-convcolors/send/enabled",(!0));
  purple_prefs_add_bool("/plugins/gtk/gtk-plugin_pack-convcolors/recv/enabled",(!0));
  purple_prefs_add_bool("/plugins/gtk/gtk-plugin_pack-convcolors/system/enabled",(!0));
  purple_prefs_add_bool("/plugins/gtk/gtk-plugin_pack-convcolors/error/enabled",(!0));
  purple_prefs_add_bool("/plugins/gtk/gtk-plugin_pack-convcolors/nick/enabled",(!0));
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
