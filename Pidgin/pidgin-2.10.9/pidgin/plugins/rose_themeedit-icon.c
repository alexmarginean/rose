/* Pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "pidgin.h"
#include "debug.h"
#include "version.h"
#include "theme-manager.h"
#include "gtkblist.h"
#include "gtkblist-theme.h"
#include "gtkutils.h"
#include "gtkplugin.h"
#include "pidginstock.h"
#include "themeedit-icon.h"
typedef enum __unnamed_enum___F0_L36_C9_FLAG_SIZE_MICROSOPIC__COMMA__FLAG_SIZE_EXTRA_SMALL__COMMA__FLAG_SIZE_SMALL__COMMA__FLAG_SIZE_MEDIUM__COMMA__FLAG_SIZE_LARGE__COMMA__FLAG_SIZE_HUGE__COMMA__FLAG_SIZE_NONE {FLAG_SIZE_MICROSOPIC,FLAG_SIZE_EXTRA_SMALL,FLAG_SIZE_SMALL,FLAG_SIZE_MEDIUM,FLAG_SIZE_LARGE,FLAG_SIZE_HUGE,FLAG_SIZE_NONE}SectionFlags;
#define SECTION_FLAGS_ALL (0x3f)
static const char *stocksizes[] = {("pidgin-icon-size-tango-microscopic"), ("pidgin-icon-size-tango-extra-small"), ("pidgin-icon-size-tango-small"), ("pidgin-icon-size-tango-medium"), ("pidgin-icon-size-tango-large"), ("pidgin-icon-size-tango-huge"), (((void *)0))};
static const struct options {
const char *stockid;
const char *text;}statuses[] = {{("pidgin-status-available"), ("Available")}, {("pidgin-status-away"), ("Away")}, {("pidgin-status-xa"), ("Extended Away")}, {("pidgin-status-busy"), ("Busy")}, {("pidgin-status-offline"), ("Offline")}, {("pidgin-status-login"), ("Just logged in")}, {("pidgin-status-logout"), ("Just logged out")}, {("pidgin-status-person"), ("Icon for Contact/\nIcon for Unknown person")}, {("pidgin-status-chat"), ("Icon for Chat")}, {((const char *)((void *)0)), ((const char *)((void *)0))}};
static const struct options chatemblems[] = {{("pidgin-status-ignored"), ("Ignored")}, {("pidgin-status-founder"), ("Founder")}, 
/* A user in a chat room who has special privileges. */
{("pidgin-status-operator"), ("Operator")}, 
/* A half operator is someone who has a subset of the privileges
	   that an operator has. */
{("pidgin-status-halfop"), ("Half Operator")}, {("pidgin-status-voice"), ("Voice")}, {((const char *)((void *)0)), ((const char *)((void *)0))}};
static const struct options dialogicons[] = {{("pidgin-dialog-auth"), ("Authorization dialog")}, {("pidgin-dialog-error"), ("Error dialog")}, {("pidgin-dialog-info"), ("Information dialog")}, {("pidgin-dialog-mail"), ("Mail dialog")}, {("pidgin-dialog-question"), ("Question dialog")}, {("pidgin-dialog-warning"), ("Warning dialog")}, {((const char *)((void *)0)), ((const char *)((void *)0))}, {("pidgin-dialog-cool"), ("What kind of dialog is this\?")}};
static const struct __unnamed_class___F0_L94_C14_unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__heading__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Coptions__Pe___variable_name_unknown_scope_and_name__scope__options__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_SectionFlagsL1630R__typedef_declaration_variable_name_unknown_scope_and_name__scope__flags {
const char *heading;
const struct options *options;
SectionFlags flags;}sections[] = {{("Status Icons"), (statuses), ((0x3f ^ 1 << FLAG_SIZE_HUGE))}, {("Chatroom Emblems"), (chatemblems), (FLAG_SIZE_SMALL)}, {("Dialog Icons"), (dialogicons), ((1 << FLAG_SIZE_EXTRA_SMALL | 1 << FLAG_SIZE_HUGE))}, {((const char *)((void *)0)), ((const struct options *)((void *)0)), (0)}};

static PidginStatusIconTheme *create_icon_theme(GtkWidget *window)
{
  int s;
  int i;
  int j;
  const char *dirname = g_get_tmp_dir();
  PidginStatusIconTheme *theme;
  const char *author;
#ifndef _WIN32
  author = (getlogin());
#else
#endif
  theme = (g_object_new(pidgin_status_icon_theme_get_type(),"type","status-icon","author",author,"directory",dirname,((void *)((void *)0))));
  for (s = 0; sections[s].heading != 0; s++) {
    GtkWidget *vbox = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),sections[s].heading));
    for (i = 0; sections[s].options[i].stockid != 0; i++) {{
        GtkWidget *image = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)vbox),((GType )(20 << 2))))),sections[s].options[i].stockid));
        GdkPixbuf *pixbuf = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)image),((GType )(20 << 2))))),"pixbuf"));
        if (!(pixbuf != 0)) 
          continue; 
        pidgin_icon_theme_set_icon(((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),sections[s].options[i].stockid,sections[s].options[i].stockid);
        for (j = 0; stocksizes[j] != 0; j++) {{
            int width;
            int height;
            GtkIconSize iconsize;
            char size[8UL];
            char *name;
            GdkPixbuf *scale;
            GError *error = (GError *)((void *)0);
            if (!((sections[s].flags & (1 << j)) != 0U)) 
              continue; 
            iconsize = gtk_icon_size_from_name(stocksizes[j]);
            gtk_icon_size_lookup(iconsize,&width,&height);
            g_snprintf(size,(sizeof(size)),"%d",width);
            if (i == 0) {
              name = g_build_filename(dirname,size,((void *)((void *)0)));
              purple_build_dir(name,256 | 128 | 64);
              g_free(name);
            }
            name = g_build_filename(dirname,size,sections[s].options[i].stockid,((void *)((void *)0)));
            scale = gdk_pixbuf_scale_simple(pixbuf,width,height,GDK_INTERP_BILINEAR);
            gdk_pixbuf_save(scale,name,"png",&error,"compression","9",((void *)((void *)0)));
            g_free(name);
            g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)scale),((GType )(20 << 2))))));
            if (error != 0) 
              g_error_free(error);
          }
        }
      }
    }
  }
  return theme;
}

static void use_icon_theme(GtkWidget *w,GtkWidget *window)
{
/* I don't quite understand the icon-theme stuff. For example, I don't
	 * know why PidginIconTheme needs to be abstract, or how PidginStatusIconTheme
	 * would be different from other PidginIconTheme's (e.g. PidginStockIconTheme)
	 * etc., but anyway, this works for now.
	 *
	 * Here's an interesting note: A PidginStatusIconTheme can be used for both
	 * stock and status icons. Like I said, I don't quite know how they could be
	 * different. So I am going to just keep it as it is, for now anyway, until I
	 * have the time to dig through this, or someone explains this stuff to me
	 * clearly.
	 *		-- Sad
	 */
  PidginStatusIconTheme *theme = create_icon_theme(window);
  pidgin_stock_load_status_icon_theme(((PidginStatusIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_status_icon_theme_get_type()))));
  pidgin_stock_load_stock_icon_theme(((PidginStockIconTheme *)theme));
  pidgin_blist_refresh(purple_get_blist());
  g_object_unref(theme);
}
#ifdef NOT_SADRUL
/* TODO: SAVE! */
#endif

static void close_icon_theme(GtkWidget *w,GtkWidget *window)
{
  gtk_widget_destroy(window);
}

static void stock_icon_selected(const char *filename,gpointer image)
{
  GError *error = (GError *)((void *)0);
  GdkPixbuf *scale;
  int i;
  GdkPixbuf *pixbuf;
  if (!(filename != 0)) 
    return ;
  pixbuf = gdk_pixbuf_new_from_file(filename,&error);
  if ((error != 0) || !(pixbuf != 0)) {
    purple_debug_error("theme-editor-icon","Unable to load icon file \'%s\' (%s)\n",filename,((error != 0)?(error -> message) : "Reason unknown"));
    if (error != 0) 
      g_error_free(error);
    return ;
  }
  scale = gdk_pixbuf_scale_simple(pixbuf,16,16,GDK_INTERP_BILINEAR);
  gtk_image_set_from_pixbuf(((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)image),gtk_image_get_type()))),scale);
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)scale),((GType )(20 << 2))))));
/* Update the size previews */
  for (i = 0; stocksizes[i] != 0; i++) {{
      int width;
      int height;
      GtkIconSize iconsize;
      GtkWidget *prev = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)image),((GType )(20 << 2))))),stocksizes[i]));
      if (!(prev != 0)) 
        continue; 
      iconsize = gtk_icon_size_from_name(stocksizes[i]);
      gtk_icon_size_lookup(iconsize,&width,&height);
      scale = gdk_pixbuf_scale_simple(pixbuf,width,height,GDK_INTERP_BILINEAR);
      gtk_image_set_from_pixbuf(((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)prev),gtk_image_get_type()))),scale);
      g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)scale),((GType )(20 << 2))))));
    }
  }
/* Save the original pixbuf so we can use it for resizing later */
  g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)image),((GType )(20 << 2))))),"pixbuf",pixbuf,((GDestroyNotify )g_object_unref));
}

static gboolean change_stock_image(GtkWidget *widget,GdkEventButton *event,GtkWidget *image)
{
  GtkWidget *win = pidgin_buddy_icon_chooser_new(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_widget_get_toplevel(widget))),gtk_window_get_type()))),stock_icon_selected,image);
  gtk_window_set_title(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_window_get_type()))),(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)image),((GType )(20 << 2))))),"localized-name")));
  gtk_widget_show_all(win);
  return (!0);
}

void pidgin_icon_theme_edit(PurplePluginAction *unused)
{
  GtkWidget *dialog;
  GtkWidget *box;
  GtkWidget *vbox;
  GtkWidget *notebook;
  GtkSizeGroup *sizegroup;
  int s;
  int i;
  int j;
  dialog = pidgin_create_dialog(((const char *)(dgettext("pidgin","Pidgin Icon Theme Editor"))),0,"theme-editor-icon",0);
  box = pidgin_dialog_get_vbox_with_properties(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),0,6);
  notebook = gtk_notebook_new();
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gtk_box_get_type()))),notebook,(!0),(!0),6);
  sizegroup = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  for (s = 0; sections[s].heading != 0; s++) {
    const char *heading = sections[s].heading;
    box = gtk_vbox_new(0,0);
    gtk_notebook_append_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)notebook),gtk_notebook_get_type()))),box,gtk_label_new(heading));
    vbox = pidgin_make_frame(box,heading);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dialog),((GType )(20 << 2))))),heading,vbox);
    for (i = 0; sections[s].options[i].stockid != 0; i++) {
      const char *id = sections[s].options[i].stockid;
      const char *text = (const char *)(dgettext("pidgin",sections[s].options[i].text));
      GtkWidget *hbox = gtk_hbox_new(0,18);
      GtkWidget *label = gtk_label_new(text);
      GtkWidget *image = gtk_image_new_from_stock(id,gtk_icon_size_from_name("pidgin-icon-size-tango-extra-small"));
      GtkWidget *ebox = gtk_event_box_new();
      gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ebox),gtk_container_get_type()))),image);
      gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ebox),((GType )(20 << 2))))),"button-press-event",((GCallback )change_stock_image),image,0,((GConnectFlags )0));
      g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)image),((GType )(20 << 2))))),"property-name",((gpointer )id));
      g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)image),((GType )(20 << 2))))),"localized-name",((gpointer )text));
      gtk_size_group_add_widget(sizegroup,label);
      gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
      gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),ebox,0,0,0);
      for (j = 0; stocksizes[j] != 0; j++) {{
          GtkWidget *sh;
          if (!((sections[s].flags & (1 << j)) != 0U)) 
            continue; 
          sh = gtk_image_new_from_stock(id,gtk_icon_size_from_name(stocksizes[j]));
          gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),sh,0,0,0);
          g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)image),((GType )(20 << 2))))),stocksizes[j],sh);
        }
      }
      gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
      g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)vbox),((GType )(20 << 2))))),id,image);
    }
  }
#ifdef NOT_SADRUL
#endif
  pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),"gtk-apply",((GCallback )use_icon_theme),dialog);
  pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),"gtk-close",((GCallback )close_icon_theme),dialog);
  gtk_widget_show_all(dialog);
  g_object_unref(sizegroup);
}
