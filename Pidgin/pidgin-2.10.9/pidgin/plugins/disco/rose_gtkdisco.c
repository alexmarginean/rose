/**
 * @file gtkdisco.c GTK+ Service Discovery UI
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "debug.h"
#include "gtkutils.h"
#include "pidgin.h"
#include "request.h"
#include "pidgintooltip.h"
#include "gtkdisco.h"
#include "xmppdisco.h"
GList *dialogs = (GList *)((void *)0);
enum __unnamed_enum___F0_L39_C1_PIXBUF_COLUMN__COMMA__NAME_COLUMN__COMMA__DESCRIPTION_COLUMN__COMMA__SERVICE_COLUMN__COMMA__NUM_OF_COLUMNS {PIXBUF_COLUMN,NAME_COLUMN,DESCRIPTION_COLUMN,SERVICE_COLUMN,NUM_OF_COLUMNS};

static void pidgin_disco_list_destroy(PidginDiscoList *list)
{
  g_hash_table_destroy((list -> services));
  if (((list -> dialog) != 0) && (( *(list -> dialog)).discolist == list)) 
    ( *(list -> dialog)).discolist = ((PidginDiscoList *)((void *)0));
  if ((list -> tree) != 0) {
    gtk_widget_destroy((list -> tree));
    list -> tree = ((GtkWidget *)((void *)0));
  }
  g_free(((gchar *)(list -> server)));
  g_free(list);
}

PidginDiscoList *pidgin_disco_list_ref(PidginDiscoList *list)
{
  do {
    if (list != ((PidginDiscoList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"list != NULL");
      return 0;
    };
  }while (0);
  ++list -> ref;
  purple_debug_misc("xmppdisco","reffing list, ref count now %d\n",(list -> ref));
  return list;
}

void pidgin_disco_list_unref(PidginDiscoList *list)
{
  do {
    if (list != ((PidginDiscoList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"list != NULL");
      return ;
    };
  }while (0);
  --list -> ref;
  purple_debug_misc("xmppdisco","unreffing list, ref count now %d\n",(list -> ref));
  if ((list -> ref) == 0) 
    pidgin_disco_list_destroy(list);
}

void pidgin_disco_list_set_in_progress(PidginDiscoList *list,gboolean in_progress)
{
  PidginDiscoDialog *dialog = (list -> dialog);
  if (!(dialog != 0)) 
    return ;
  list -> in_progress = in_progress;
  if (in_progress != 0) {
    gtk_widget_set_sensitive((dialog -> account_widget),0);
    gtk_widget_set_sensitive((dialog -> stop_button),(!0));
    gtk_widget_set_sensitive((dialog -> browse_button),0);
  }
  else {
    gtk_progress_bar_set_fraction(((GtkProgressBar *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> progress)),gtk_progress_bar_get_type()))),0.0);
    gtk_widget_set_sensitive((dialog -> account_widget),(!0));
    gtk_widget_set_sensitive((dialog -> stop_button),0);
    gtk_widget_set_sensitive((dialog -> browse_button),(!0));
/*
		gtk_widget_set_sensitive(dialog->register_button, FALSE);
		gtk_widget_set_sensitive(dialog->add_button, FALSE);
*/
  }
}

static GdkPixbuf *pidgin_disco_load_icon(XmppDiscoService *service,const char *size)
{
  GdkPixbuf *pixbuf = (GdkPixbuf *)((void *)0);
  char *filename = (char *)((void *)0);
  do {
    if (service != ((XmppDiscoService *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"service != NULL");
      return 0;
    };
  }while (0);
  do {
    if (size != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"size != NULL");
      return 0;
    };
  }while (0);
  if (((service -> type) == XMPP_DISCO_SERVICE_TYPE_GATEWAY) && ((service -> gateway_type) != 0)) {
    char *tmp = g_strconcat((service -> gateway_type),".png",((void *)((void *)0)));
    filename = g_build_filename("/usr/local/share","pixmaps","pidgin","protocols",size,tmp,((void *)((void *)0)));
    g_free(tmp);
#if 0
#endif
  }
  else if ((service -> type) == XMPP_DISCO_SERVICE_TYPE_CHAT) 
    filename = g_build_filename("/usr/local/share","pixmaps","pidgin","status",size,"chat.png",((void *)((void *)0)));
  if (filename != 0) {
    pixbuf = gdk_pixbuf_new_from_file(filename,0);
    g_free(filename);
  }
  return pixbuf;
}
static void pidgin_disco_create_tree(PidginDiscoList *pdl);

static void dialog_select_account_cb(GObject *w,PurpleAccount *account,PidginDiscoDialog *dialog)
{
  gboolean change = (account != (dialog -> account));
  dialog -> account = account;
  gtk_widget_set_sensitive((dialog -> browse_button),(account != ((PurpleAccount *)((void *)0))));
  if ((change != 0) && ((dialog -> discolist) != 0)) {
    if (( *(dialog -> discolist)).tree != 0) {
      gtk_widget_destroy(( *(dialog -> discolist)).tree);
      ( *(dialog -> discolist)).tree = ((GtkWidget *)((void *)0));
    }
    pidgin_disco_list_unref((dialog -> discolist));
    dialog -> discolist = ((PidginDiscoList *)((void *)0));
  }
}

static void register_button_cb(GtkWidget *unused,PidginDiscoDialog *dialog)
{
  xmpp_disco_service_register((dialog -> selected));
}

static void discolist_cancel_cb(PidginDiscoList *pdl,const char *server)
{
  ( *(pdl -> dialog)).prompt_handle = ((gpointer *)((void *)0));
  pidgin_disco_list_set_in_progress(pdl,0);
  pidgin_disco_list_unref(pdl);
}

static void discolist_ok_cb(PidginDiscoList *pdl,const char *server)
{
  ( *(pdl -> dialog)).prompt_handle = ((gpointer *)((void *)0));
  gtk_widget_set_sensitive(( *(pdl -> dialog)).browse_button,(!0));
  if (!(server != 0) || !(( *server) != 0)) {
    purple_notify_message(my_plugin,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Invalid Server"))),((const char *)(dgettext("pidgin","Invalid Server"))),0,0,0);
    pidgin_disco_list_set_in_progress(pdl,0);
    pidgin_disco_list_unref(pdl);
    return ;
  }
  pdl -> server = (g_strdup(server));
  pidgin_disco_list_set_in_progress(pdl,(!0));
  xmpp_disco_start(pdl);
}

static void browse_button_cb(GtkWidget *button,PidginDiscoDialog *dialog)
{
  PurpleConnection *pc;
  PidginDiscoList *pdl;
  const char *username;
  const char *at;
  const char *slash;
  char *server = (char *)((void *)0);
  pc = purple_account_get_connection((dialog -> account));
  if (!(pc != 0)) 
    return ;
  gtk_widget_set_sensitive((dialog -> browse_button),0);
  gtk_widget_set_sensitive((dialog -> add_button),0);
  gtk_widget_set_sensitive((dialog -> register_button),0);
  if ((dialog -> discolist) != ((PidginDiscoList *)((void *)0))) {
    if (( *(dialog -> discolist)).tree != 0) {
      gtk_widget_destroy(( *(dialog -> discolist)).tree);
      ( *(dialog -> discolist)).tree = ((GtkWidget *)((void *)0));
    }
    pidgin_disco_list_unref((dialog -> discolist));
  }
  pdl = (dialog -> discolist = ((PidginDiscoList *)(g_malloc0_n(1,(sizeof(PidginDiscoList ))))));
  pdl -> services = g_hash_table_new_full(0,0,0,((GDestroyNotify )gtk_tree_row_reference_free));
  pdl -> pc = pc;
/* We keep a copy... */
  pidgin_disco_list_ref(pdl);
  pdl -> dialog = dialog;
  pidgin_disco_create_tree(pdl);
  if ((dialog -> account_widget) != 0) 
    gtk_widget_set_sensitive((dialog -> account_widget),0);
  username = purple_account_get_username((dialog -> account));
  at = (strchr(username,64));
  slash = (strchr(username,'/'));
  if ((at != 0) && !(slash != 0)) {
    server = g_strdup_printf("%s",(at + 1));
  }
  else if (((at != 0) && (slash != 0)) && ((at + 1) < slash)) {
    server = g_strdup_printf("%.*s",((int )(slash - (at + 1))),(at + 1));
  }
  if (server == ((char *)((void *)0))) 
/* This shouldn't ever happen since the account is connected */
    server = g_strdup("jabber.org");
/* Note to translators: The string "Enter an XMPP Server" is asking the
	   user to type the name of an XMPP server which will then be queried */
  dialog -> prompt_handle = (purple_request_input(my_plugin,((const char *)(dgettext("pidgin","Server name request"))),((const char *)(dgettext("pidgin","Enter an XMPP Server"))),((const char *)(dgettext("pidgin","Select an XMPP server to query"))),server,0,0,0,((const char *)(dgettext("pidgin","Find Services"))),((PurpleCallback )discolist_ok_cb),((const char *)(dgettext("pidgin","Cancel"))),((PurpleCallback )discolist_cancel_cb),purple_connection_get_account(pc),0,0,pdl));
  g_free(server);
}

static void add_to_blist_cb(GtkWidget *unused,PidginDiscoDialog *dialog)
{
  XmppDiscoService *service = (dialog -> selected);
  PurpleAccount *account;
  const char *jid;
  do {
    if (service != ((XmppDiscoService *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"service != NULL");
      return ;
    };
  }while (0);
  account = purple_connection_get_account(( *(service -> list)).pc);
  jid = (service -> jid);
  if ((service -> type) == XMPP_DISCO_SERVICE_TYPE_CHAT) 
    purple_blist_request_add_chat(account,0,0,jid);
  else 
    purple_blist_request_add_buddy(account,jid,0,0);
}

static gboolean service_click_cb(GtkTreeView *tree,GdkEventButton *event,gpointer user_data)
{
  PidginDiscoList *pdl;
  XmppDiscoService *service;
  GtkWidget *menu;
  GtkTreePath *path;
  GtkTreeIter iter;
  GValue val;
  if (((event -> button) != 3) || ((event -> type) != GDK_BUTTON_PRESS)) 
    return 0;
  pdl = user_data;
/* Figure out what was clicked */
  if (!(gtk_tree_view_get_path_at_pos(tree,(event -> x),(event -> y),&path,0,0,0) != 0)) 
    return 0;
  gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> model)),gtk_tree_model_get_type()))),&iter,path);
  gtk_tree_path_free(path);
  val.g_type = 0;
  gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> model)),gtk_tree_model_get_type()))),&iter,SERVICE_COLUMN,&val);
  service = (g_value_get_pointer((&val)));
  if (!(service != 0)) 
    return 0;
  menu = gtk_menu_new();
  if (((service -> flags) & XMPP_DISCO_ADD) != 0U) 
    pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Add to Buddy List"))),"gtk-add",((GCallback )add_to_blist_cb),(pdl -> dialog),0,0,0);
  if (((service -> flags) & XMPP_DISCO_REGISTER) != 0U) {
    GtkWidget *item = pidgin_new_item(menu,((const char *)(dgettext("pidgin","Register"))));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"activate",((GCallback )register_button_cb),(pdl -> dialog),0,((GConnectFlags )0));
  }
  gtk_widget_show_all(menu);
  gtk_menu_popup(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_get_type()))),0,0,0,0,(event -> button),(event -> time));
  return 0;
}

static void selection_changed_cb(GtkTreeSelection *selection,PidginDiscoList *pdl)
{
  GtkTreeIter iter;
  GValue val;
  PidginDiscoDialog *dialog = (pdl -> dialog);
  if (gtk_tree_selection_get_selected(selection,0,&iter) != 0) {
    val.g_type = 0;
    gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> model)),gtk_tree_model_get_type()))),&iter,SERVICE_COLUMN,&val);
    dialog -> selected = (g_value_get_pointer((&val)));
    if (!((dialog -> selected) != 0)) {
      gtk_widget_set_sensitive((dialog -> add_button),0);
      gtk_widget_set_sensitive((dialog -> register_button),0);
      return ;
    }
    gtk_widget_set_sensitive((dialog -> add_button),(( *(dialog -> selected)).flags & XMPP_DISCO_ADD));
    gtk_widget_set_sensitive((dialog -> register_button),(( *(dialog -> selected)).flags & XMPP_DISCO_REGISTER));
  }
  else {
    gtk_widget_set_sensitive((dialog -> add_button),0);
    gtk_widget_set_sensitive((dialog -> register_button),0);
  }
}

static void row_expanded_cb(GtkTreeView *tree,GtkTreeIter *arg1,GtkTreePath *rg2,gpointer user_data)
{
  PidginDiscoList *pdl;
  XmppDiscoService *service;
  GValue val;
  pdl = user_data;
  val.g_type = 0;
  gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> model)),gtk_tree_model_get_type()))),arg1,SERVICE_COLUMN,&val);
  service = (g_value_get_pointer((&val)));
  xmpp_disco_service_expand(service);
}

static void row_activated_cb(GtkTreeView *tree_view,GtkTreePath *path,GtkTreeViewColumn *column,gpointer user_data)
{
  PidginDiscoList *pdl = user_data;
  GtkTreeIter iter;
  XmppDiscoService *service;
  GValue val;
  if (!(gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> model)),gtk_tree_model_get_type()))),&iter,path) != 0)) 
    return ;
  val.g_type = 0;
  gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> model)),gtk_tree_model_get_type()))),&iter,SERVICE_COLUMN,&val);
  service = (g_value_get_pointer((&val)));
  if (((service -> flags) & XMPP_DISCO_BROWSE) != 0U) 
    if (gtk_tree_view_row_expanded(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> tree)),gtk_tree_view_get_type()))),path) != 0) 
      gtk_tree_view_collapse_row(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> tree)),gtk_tree_view_get_type()))),path);
    else 
      gtk_tree_view_expand_row(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> tree)),gtk_tree_view_get_type()))),path,0);
  else if (((service -> flags) & XMPP_DISCO_REGISTER) != 0U) 
    register_button_cb(0,(pdl -> dialog));
  else if (((service -> flags) & XMPP_DISCO_ADD) != 0U) 
    add_to_blist_cb(0,(pdl -> dialog));
}

static void destroy_win_cb(GtkWidget *window,gpointer d)
{
  PidginDiscoDialog *dialog = d;
  PidginDiscoList *list = (dialog -> discolist);
  if ((dialog -> prompt_handle) != 0) 
    purple_request_close(PURPLE_REQUEST_INPUT,(dialog -> prompt_handle));
  if (list != 0) {
    list -> dialog = ((PidginDiscoDialog *)((void *)0));
    if ((list -> in_progress) != 0) 
      list -> in_progress = 0;
    pidgin_disco_list_unref(list);
  }
  dialogs = g_list_remove(dialogs,d);
  g_free(dialog);
}

static void stop_button_cb(GtkButton *button,PidginDiscoDialog *dialog)
{
  pidgin_disco_list_set_in_progress((dialog -> discolist),0);
}

static void close_button_cb(GtkButton *button,PidginDiscoDialog *dialog)
{
  GtkWidget *window = (dialog -> window);
  gtk_widget_destroy(window);
}

static gboolean account_filter_func(PurpleAccount *account)
{
  return purple_strequal(purple_account_get_protocol_id(account),"prpl-jabber");
}

static gboolean disco_paint_tooltip(GtkWidget *tipwindow,gpointer data)
{
  PangoLayout *layout = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tipwindow),((GType )(20 << 2))))),"tooltip-plugin"));
#if GTK_CHECK_VERSION(2,14,0)
  gtk_paint_layout(gtk_widget_get_style(tipwindow),gtk_widget_get_window(tipwindow),GTK_STATE_NORMAL,0,0,tipwindow,"tooltip",6,6,layout);
#else
#endif
  return (!0);
}

static gboolean disco_create_tooltip(GtkWidget *tipwindow,GtkTreePath *path,gpointer data,int *w,int *h)
{
  PidginDiscoList *pdl = data;
  GtkTreeIter iter;
  PangoLayout *layout;
  int width;
  int height;
  XmppDiscoService *service;
  GValue val;
  const char *type = (const char *)((void *)0);
  char *markup;
  char *jid;
  char *name;
  char *desc = (char *)((void *)0);
  if (!(gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> model)),gtk_tree_model_get_type()))),&iter,path) != 0)) 
    return 0;
  val.g_type = 0;
  gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> model)),gtk_tree_model_get_type()))),&iter,SERVICE_COLUMN,&val);
  service = (g_value_get_pointer((&val)));
  if (!(service != 0)) 
    return 0;
  switch(service -> type){
    case XMPP_DISCO_SERVICE_TYPE_UNSET:
{
      type = ((const char *)(dgettext("pidgin","Unknown")));
      break; 
    }
    case XMPP_DISCO_SERVICE_TYPE_GATEWAY:
{
      type = ((const char *)(dgettext("pidgin","Gateway")));
      break; 
    }
    case XMPP_DISCO_SERVICE_TYPE_DIRECTORY:
{
      type = ((const char *)(dgettext("pidgin","Directory")));
      break; 
    }
    case XMPP_DISCO_SERVICE_TYPE_CHAT:
{
      type = ((const char *)(dgettext("pidgin","Chat")));
      break; 
    }
    case XMPP_DISCO_SERVICE_TYPE_PUBSUB_COLLECTION:
{
      type = ((const char *)(dgettext("pidgin","PubSub Collection")));
      break; 
    }
    case XMPP_DISCO_SERVICE_TYPE_PUBSUB_LEAF:
{
      type = ((const char *)(dgettext("pidgin","PubSub Leaf")));
      break; 
    }
    case XMPP_DISCO_SERVICE_TYPE_OTHER:
{
      type = ((const char *)(dgettext("pidgin","Other")));
      break; 
    }
  }
  markup = g_strdup_printf("<span size=\'x-large\' weight=\'bold\'>%s</span>\n<b>%s:</b> %s%s%s",(name = g_markup_escape_text((service -> name),(-1))),type,(jid = g_markup_escape_text((service -> jid),(-1))),(((service -> description) != 0)?((const char *)(dgettext("pidgin","\n<b>Description:</b> "))) : ""),(((service -> description) != 0)?(desc = g_markup_escape_text((service -> description),(-1))) : ""));
  layout = gtk_widget_create_pango_layout(tipwindow,0);
  pango_layout_set_markup(layout,markup,-1);
  pango_layout_set_wrap(layout,PANGO_WRAP_WORD);
  pango_layout_set_width(layout,500000);
  pango_layout_get_size(layout,&width,&height);
  g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tipwindow),((GType )(20 << 2))))),"tooltip-plugin",layout,g_object_unref);
  if (w != 0) 
     *w = (((((int )width) + 512) >> 10) + 12);
  if (h != 0) 
     *h = (((((int )height) + 512) >> 10) + 12);
  g_free(markup);
  g_free(jid);
  g_free(name);
  g_free(desc);
  return (!0);
}

static void pidgin_disco_create_tree(PidginDiscoList *pdl)
{
  GtkCellRenderer *text_renderer;
  GtkCellRenderer *pixbuf_renderer;
  GtkTreeViewColumn *column;
  GtkTreeSelection *selection;
  pdl -> model = gtk_tree_store_new(NUM_OF_COLUMNS,gdk_pixbuf_get_type(),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(17 << 2)));
/* PIXBUF_COLUMN */
/* NAME_COLUMN */
/* DESCRIPTION_COLUMN */
/* SERVICE_COLUMN */
  pdl -> tree = gtk_tree_view_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> model)),gtk_tree_model_get_type()))));
  gtk_tree_view_set_rules_hint(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> tree)),gtk_tree_view_get_type()))),(!0));
  selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> tree)),gtk_tree_view_get_type()))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)selection),((GType )(20 << 2))))),"changed",((GCallback )selection_changed_cb),pdl,0,((GConnectFlags )0));
  g_object_unref((pdl -> model));
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *(pdl -> dialog)).sw),gtk_container_get_type()))),(pdl -> tree));
  gtk_widget_show((pdl -> tree));
  text_renderer = gtk_cell_renderer_text_new();
  pixbuf_renderer = gtk_cell_renderer_pixbuf_new();
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_title(column,((const char *)(dgettext("pidgin","Name"))));
  gtk_tree_view_column_pack_start(column,pixbuf_renderer,0);
  gtk_tree_view_column_set_attributes(column,pixbuf_renderer,"pixbuf",PIXBUF_COLUMN,((void *)((void *)0)));
  gtk_tree_view_column_pack_start(column,text_renderer,(!0));
  gtk_tree_view_column_set_attributes(column,text_renderer,"text",NAME_COLUMN,((void *)((void *)0)));
  gtk_tree_view_column_set_sizing(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),GTK_TREE_VIEW_COLUMN_GROW_ONLY);
  gtk_tree_view_column_set_resizable(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),(!0));
  gtk_tree_view_column_set_sort_column_id(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),NAME_COLUMN);
  gtk_tree_view_column_set_reorderable(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),(!0));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> tree)),gtk_tree_view_get_type()))),column);
  column = gtk_tree_view_column_new_with_attributes(((const char *)(dgettext("pidgin","Description"))),text_renderer,"text",DESCRIPTION_COLUMN,((void *)((void *)0)));
  gtk_tree_view_column_set_sizing(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),GTK_TREE_VIEW_COLUMN_GROW_ONLY);
  gtk_tree_view_column_set_resizable(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),(!0));
  gtk_tree_view_column_set_sort_column_id(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),DESCRIPTION_COLUMN);
  gtk_tree_view_column_set_reorderable(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),(!0));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> tree)),gtk_tree_view_get_type()))),column);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> tree)),((GType )(20 << 2))))),"button-press-event",((GCallback )service_click_cb),pdl,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> tree)),((GType )(20 << 2))))),"row-expanded",((GCallback )row_expanded_cb),pdl,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> tree)),((GType )(20 << 2))))),"row-activated",((GCallback )row_activated_cb),pdl,0,((GConnectFlags )0));
  pidgin_tooltip_setup_for_treeview((pdl -> tree),pdl,disco_create_tooltip,disco_paint_tooltip);
}

void pidgin_disco_signed_off_cb(PurpleConnection *pc)
{
  GList *node;
  for (node = dialogs; node != 0; node = (node -> next)) {
    PidginDiscoDialog *dialog = (node -> data);
    PidginDiscoList *list = (dialog -> discolist);
    if ((list != 0) && ((list -> pc) == pc)) {
      if ((list -> in_progress) != 0) 
        pidgin_disco_list_set_in_progress(list,0);
      if ((list -> tree) != 0) {
        gtk_widget_destroy((list -> tree));
        list -> tree = ((GtkWidget *)((void *)0));
      }
      pidgin_disco_list_unref(list);
      dialog -> discolist = ((PidginDiscoList *)((void *)0));
      gtk_widget_set_sensitive((dialog -> browse_button),(pidgin_account_option_menu_get_selected((dialog -> account_widget)) != ((PurpleAccount *)((void *)0))));
      gtk_widget_set_sensitive((dialog -> register_button),0);
      gtk_widget_set_sensitive((dialog -> add_button),0);
    }
  }
}

void pidgin_disco_dialogs_destroy_all()
{
  while(dialogs != 0){
    PidginDiscoDialog *dialog = (dialogs -> data);
    gtk_widget_destroy((dialog -> window));
/* destroy_win_cb removes the dialog from the list */
  }
}

PidginDiscoDialog *pidgin_disco_dialog_new()
{
  PidginDiscoDialog *dialog;
  GtkWidget *window;
  GtkWidget *vbox;
  GtkWidget *vbox2;
  GtkWidget *bbox;
  dialog = ((PidginDiscoDialog *)(g_malloc0_n(1,(sizeof(PidginDiscoDialog )))));
  dialogs = g_list_prepend(dialogs,dialog);
/* Create the window. */
  dialog -> window = (window = pidgin_create_dialog(((const char *)(dgettext("pidgin","Service Discovery"))),12,"service discovery",(!0)));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"destroy",((GCallback )destroy_win_cb),dialog,0,((GConnectFlags )0));
/* Create the parent vbox for everything. */
  vbox = pidgin_dialog_get_vbox_with_properties(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),0,12);
  vbox2 = gtk_vbox_new(0,12);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),vbox2);
  gtk_widget_show(vbox2);
/* accounts dropdown list */
  dialog -> account_widget = pidgin_account_option_menu_new(0,0,((GCallback )dialog_select_account_cb),account_filter_func,dialog);
  dialog -> account = pidgin_account_option_menu_get_selected((dialog -> account_widget));
  pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox2),gtk_box_get_type()))),((const char *)(dgettext("pidgin","_Account:"))),0,(dialog -> account_widget),(!0),0);
/* scrolled window */
  dialog -> sw = pidgin_make_scrollable(0,GTK_POLICY_ALWAYS,GTK_POLICY_ALWAYS,GTK_SHADOW_IN,-1,250);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox2),gtk_box_get_type()))),(dialog -> sw),(!0),(!0),0);
/* progress bar */
  dialog -> progress = gtk_progress_bar_new();
  gtk_progress_bar_set_pulse_step(((GtkProgressBar *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> progress)),gtk_progress_bar_get_type()))),0.1);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox2),gtk_box_get_type()))),(dialog -> progress),0,0,0);
  gtk_widget_show((dialog -> progress));
/* button box */
  bbox = pidgin_dialog_get_action_area(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))));
  gtk_box_set_spacing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),6);
  gtk_button_box_set_layout(((GtkButtonBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_button_box_get_type()))),GTK_BUTTONBOX_END);
/* stop button */
  dialog -> stop_button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),"gtk-stop",((GCallback )stop_button_cb),dialog);
  gtk_widget_set_sensitive((dialog -> stop_button),0);
/* browse button */
  dialog -> browse_button = pidgin_pixbuf_button_from_stock(((const char *)(dgettext("pidgin","_Browse"))),"gtk-refresh",PIDGIN_BUTTON_HORIZONTAL);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),(dialog -> browse_button),0,0,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> browse_button)),((GType )(20 << 2))))),"clicked",((GCallback )browse_button_cb),dialog,0,((GConnectFlags )0));
  gtk_widget_set_sensitive((dialog -> browse_button),((dialog -> account) != ((PurpleAccount *)((void *)0))));
  gtk_widget_show((dialog -> browse_button));
/* register button */
  dialog -> register_button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> window)),gtk_dialog_get_type()))),((const char *)(dgettext("pidgin","Register"))),((GCallback )register_button_cb),dialog);
  gtk_widget_set_sensitive((dialog -> register_button),0);
/* add button */
  dialog -> add_button = pidgin_pixbuf_button_from_stock(((const char *)(dgettext("pidgin","_Add"))),"gtk-add",PIDGIN_BUTTON_HORIZONTAL);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),(dialog -> add_button),0,0,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> add_button)),((GType )(20 << 2))))),"clicked",((GCallback )add_to_blist_cb),dialog,0,((GConnectFlags )0));
  gtk_widget_set_sensitive((dialog -> add_button),0);
  gtk_widget_show((dialog -> add_button));
/* close button */
  dialog -> close_button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),"gtk-close",((GCallback )close_button_cb),dialog);
/* show the dialog window and return the dialog */
  gtk_widget_show((dialog -> window));
  return dialog;
}

void pidgin_disco_add_service(PidginDiscoList *pdl,XmppDiscoService *service,XmppDiscoService *parent)
{
  PidginDiscoDialog *dialog;
  GtkTreeIter iter;
  GtkTreeIter parent_iter;
  GtkTreeIter child;
  GdkPixbuf *pixbuf = (GdkPixbuf *)((void *)0);
  gboolean append = (!0);
  dialog = (pdl -> dialog);
  do {
    if (dialog != ((PidginDiscoDialog *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"dialog != NULL");
      return ;
    };
  }while (0);
  if (service != ((XmppDiscoService *)((void *)0))) 
    purple_debug_info("xmppdisco","Adding service \"%s\"\n",(service -> name));
  else 
    purple_debug_info("xmppdisco","Service \"%s\" has no childrens\n",(parent -> name));
  gtk_progress_bar_pulse(((GtkProgressBar *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> progress)),gtk_progress_bar_get_type()))));
  if (parent != 0) {
    GtkTreeRowReference *rr;
    GtkTreePath *path;
    rr = (g_hash_table_lookup((pdl -> services),parent));
    path = gtk_tree_row_reference_get_path(rr);
    if (path != 0) {
      gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> model)),gtk_tree_model_get_type()))),&parent_iter,path);
      gtk_tree_path_free(path);
      if (gtk_tree_model_iter_children(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> model)),gtk_tree_model_get_type()))),&child,&parent_iter) != 0) {
        PidginDiscoList *tmp;
        gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> model)),gtk_tree_model_get_type()))),&child,SERVICE_COLUMN,&tmp,-1);
        if (!(tmp != 0)) 
          append = 0;
      }
    }
  }
  if (service == ((XmppDiscoService *)((void *)0))) {
    if ((parent != ((XmppDiscoService *)((void *)0))) && !(append != 0)) 
      gtk_tree_store_remove((pdl -> model),&child);
    return ;
  }
  if (append != 0) 
    gtk_tree_store_append((pdl -> model),&iter,((parent != 0)?&parent_iter : ((struct _GtkTreeIter *)((void *)0))));
  else 
    iter = child;
  if (((service -> flags) & XMPP_DISCO_BROWSE) != 0U) {
    GtkTreeRowReference *rr;
    GtkTreePath *path;
    gtk_tree_store_append((pdl -> model),&child,&iter);
    path = gtk_tree_model_get_path(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> model)),gtk_tree_model_get_type()))),&iter);
    rr = gtk_tree_row_reference_new(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(pdl -> model)),gtk_tree_model_get_type()))),path);
    g_hash_table_insert((pdl -> services),service,rr);
    gtk_tree_path_free(path);
  }
  pixbuf = pidgin_disco_load_icon(service,"16");
  gtk_tree_store_set((pdl -> model),&iter,PIXBUF_COLUMN,pixbuf,NAME_COLUMN,(service -> name),DESCRIPTION_COLUMN,(service -> description),SERVICE_COLUMN,service,-1);
  if (pixbuf != 0) 
    g_object_unref(pixbuf);
}
