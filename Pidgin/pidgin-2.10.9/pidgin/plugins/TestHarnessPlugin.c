#define PURPLE_PLUGINS

#include <glib.h>

#include "notify.h"
#include "plugin.h"
#include "version.h"

#include <check.h>

//in load we will run our checks!

START_TEST(test_1) {

    //ck_abort();
}

END_TEST



Suite *
money_suite(void) {

    Suite *s = suite_create("GenTransSuite");
    // Core test case 
    TCase *tc_core = tcase_create("Core");
    //tcase_set_timeout(tc_core, 15);
    tcase_add_test(tc_core, test_1);
    //tcase_add_test(tc_core, test_2);
    suite_add_tcase(s, tc_core);
    return s;

}

static gboolean
plugin_load(PurplePlugin *plugin) {




    purple_notify_message(plugin, PURPLE_NOTIFY_MSG_INFO, "TEST HARNESS PLUGIN",
            "This is the Test Harness Plugin plugin :)", NULL, NULL, NULL);

    int number_failed;
    Suite *s = money_suite();
    SRunner *sr = srunner_create(s);
    srunner_run_all(sr, CK_SILENT);

    //printf("\n\n");

    //srunner_print(sr, CK_NORMAL);

   // number_failed = srunner_ntests_failed(sr);
    //srunner_free(sr);

    //exit (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;

    if (1 == 1) {
        exit(0);
    }




    return TRUE;
}

static PurplePluginInfo info = {
    PURPLE_PLUGIN_MAGIC,
    PURPLE_MAJOR_VERSION,
    PURPLE_MINOR_VERSION,
    PURPLE_PLUGIN_STANDARD,
    NULL,
    0,
    NULL,
    PURPLE_PRIORITY_DEFAULT,

    "core-hello_world",
    "Test Harness",
    "1.1",

    "Test Harness Plugin",
    "Test Harness Plugin",
    "My Name <email@helloworld.tld>",
    "http://helloworld.tld",

    plugin_load,
    NULL,
    NULL,

    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};

static void
init_plugin(PurplePlugin *plugin) {
}

PURPLE_INIT_PLUGIN(hello_world, init_plugin, info)
