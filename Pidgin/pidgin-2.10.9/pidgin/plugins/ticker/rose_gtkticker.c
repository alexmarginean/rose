/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02111-1301, USA.
 */
/*
 * GtkTicker Copyright 2000 Syd Logan
 */
/* FIXME: GTK+ deprecated GTK_WIDGET_MAPPED/REALIZED, but don't provide
          accessor functions yet. */
#undef GSEAL_ENABLE
#include "gtkticker.h"
#include <gtk/gtk.h>
/* These don't seem to be in a release yet. See BZ #69872 */
#define gtk_widget_is_mapped(x) GTK_WIDGET_MAPPED(x)
#define gtk_widget_is_realized(x) GTK_WIDGET_REALIZED(x)
#define gtk_widget_set_realized(x,y) do {\
	if (y) \
		GTK_WIDGET_SET_FLAGS(x, GTK_REALIZED); \
	else \
		GTK_WIDGET_UNSET_FLAGS(x, GTK_REALIZED); \
} while(0)
#define gtk_widget_set_mapped(x,y) do {\
	if (y) \
		GTK_WIDGET_SET_FLAGS(x, GTK_MAPPED); \
	else \
		GTK_WIDGET_UNSET_FLAGS(x, GTK_MAPPED); \
} while(0)
#if !GTK_CHECK_VERSION(2,18,0)
#define gtk_widget_get_visible(x) GTK_WIDGET_VISIBLE(x)
#if !GTK_CHECK_VERSION(2,14,0)
#define gtk_widget_get_window(x) x->window
#endif
#endif
static void gtk_ticker_compute_offsets(GtkTicker *ticker);
static void gtk_ticker_class_init(GtkTickerClass *klass);
static void gtk_ticker_init(GtkTicker *ticker);
static void gtk_ticker_map(GtkWidget *widget);
static void gtk_ticker_realize(GtkWidget *widget);
static void gtk_ticker_size_request(GtkWidget *widget,GtkRequisition *requisition);
static void gtk_ticker_size_allocate(GtkWidget *widget,GtkAllocation *allocation);
static void gtk_ticker_add_real(GtkContainer *container,GtkWidget *widget);
static void gtk_ticker_remove_real(GtkContainer *container,GtkWidget *widget);
static void gtk_ticker_forall(GtkContainer *container,gboolean include_internals,GtkCallback callback,gpointer callback_data);
static GType gtk_ticker_child_type(GtkContainer *container);
static GtkContainerClass *parent_class = (GtkContainerClass *)((void *)0);

GType gtk_ticker_get_type()
{
  static GType ticker_type = 0;
  ticker_type = g_type_from_name("GtkTicker");
  if (!(ticker_type != 0UL)) {
    static const GTypeInfo ticker_info = {((sizeof(GtkTickerClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gtk_ticker_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GtkTicker ))), (0), ((GInstanceInitFunc )gtk_ticker_init), ((const GTypeValueTable *)((void *)0))};
    ticker_type = g_type_register_static(gtk_container_get_type(),"GtkTicker",&ticker_info,0);
  }
  else 
/* kludge to re-initialise the class if it's already registered */
if (parent_class == ((GtkContainerClass *)((void *)0))) {
    gtk_ticker_class_init(((GtkTickerClass *)(g_type_class_peek(ticker_type))));
  }
  return ticker_type;
}

static void gtk_ticker_finalize(GObject *object)
{
  gtk_ticker_stop_scroll(((GtkTicker *)(g_type_check_instance_cast(((GTypeInstance *)object),gtk_ticker_get_type()))));
  ( *( *((GObjectClass *)(g_type_check_class_cast(((GTypeClass *)parent_class),((GType )(20 << 2)))))).finalize)(object);
}

static void gtk_ticker_class_init(GtkTickerClass *class)
{
  GObjectClass *gobject_class;
  GtkWidgetClass *widget_class;
  GtkContainerClass *container_class;
  gobject_class = ((GObjectClass *)class);
  widget_class = ((GtkWidgetClass *)class);
  container_class = ((GtkContainerClass *)class);
  parent_class = (g_type_class_ref(gtk_container_get_type()));
  gobject_class -> finalize = gtk_ticker_finalize;
  widget_class -> map = gtk_ticker_map;
  widget_class -> realize = gtk_ticker_realize;
  widget_class -> size_request = gtk_ticker_size_request;
  widget_class -> size_allocate = gtk_ticker_size_allocate;
  container_class -> add = gtk_ticker_add_real;
  container_class -> remove = gtk_ticker_remove_real;
  container_class -> forall = gtk_ticker_forall;
  container_class -> child_type = gtk_ticker_child_type;
}

static GType gtk_ticker_child_type(GtkContainer *container)
{
  return gtk_widget_get_type();
}

static void gtk_ticker_init(GtkTicker *ticker)
{
#if GTK_CHECK_VERSION(2,18,0)
  gtk_widget_set_has_window(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_widget_get_type()))),(!0));
#else
#endif
  ticker -> interval = ((guint )200);
  ticker -> scootch = ((guint )2);
  ticker -> children = ((GList *)((void *)0));
  ticker -> timer = 0;
  ticker -> dirty = (!0);
}

GtkWidget *gtk_ticker_new()
{
  return (GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(g_object_new(gtk_ticker_get_type(),0))),gtk_widget_get_type()));
}

static void gtk_ticker_put(GtkTicker *ticker,GtkWidget *widget)
{
  GtkTickerChild *child_info;
  do {
    if (ticker != ((GtkTicker *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ticker != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)ticker;
      GType __t = gtk_ticker_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_TICKER (ticker)");
      return ;
    };
  }while (0);
  do {
    if (widget != ((GtkWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"widget != NULL");
      return ;
    };
  }while (0);
  child_info = ((GtkTickerChild *)(g_malloc_n(1,(sizeof(GtkTickerChild )))));
  child_info -> widget = widget;
  child_info -> x = 0;
  gtk_widget_set_parent(widget,((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_widget_get_type()))));
  ticker -> children = g_list_append((ticker -> children),child_info);
  if ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_object_get_type())))).flags & GTK_REALIZED) != 0) 
    gtk_widget_realize(widget);
  if ((gtk_widget_get_visible(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_widget_get_type())))) != 0) && (gtk_widget_get_visible(widget) != 0)) {
    if ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_widget_get_type())))),gtk_object_get_type())))).flags & GTK_MAPPED) != 0) 
      gtk_widget_map(widget);
    gtk_widget_queue_resize(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_widget_get_type()))));
  }
}

void gtk_ticker_set_interval(GtkTicker *ticker,gint interval)
{
  do {
    if (ticker != ((GtkTicker *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ticker != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)ticker;
      GType __t = gtk_ticker_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_TICKER (ticker)");
      return ;
    };
  }while (0);
  if (interval < 0) 
    interval = 200;
  ticker -> interval = interval;
}

guint gtk_ticker_get_interval(GtkTicker *ticker)
{
  do {
    if (ticker != ((GtkTicker *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ticker != NULL");
      return (-1);
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)ticker;
      GType __t = gtk_ticker_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_TICKER (ticker)");
      return (-1);
    };
  }while (0);
  return ticker -> interval;
}

void gtk_ticker_set_scootch(GtkTicker *ticker,gint scootch)
{
  do {
    if (ticker != ((GtkTicker *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ticker != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)ticker;
      GType __t = gtk_ticker_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_TICKER (ticker)");
      return ;
    };
  }while (0);
  if (scootch <= 0) 
    scootch = 2;
  ticker -> scootch = scootch;
  ticker -> dirty = (!0);
}

guint gtk_ticker_get_scootch(GtkTicker *ticker)
{
  do {
    if (ticker != ((GtkTicker *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ticker != NULL");
      return (-1);
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)ticker;
      GType __t = gtk_ticker_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_TICKER (ticker)");
      return (-1);
    };
  }while (0);
  return ticker -> scootch;
}

void gtk_ticker_set_spacing(GtkTicker *ticker,gint spacing)
{
  do {
    if (ticker != ((GtkTicker *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ticker != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)ticker;
      GType __t = gtk_ticker_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_TICKER (ticker)");
      return ;
    };
  }while (0);
  if (spacing < 0) 
    spacing = 0;
  ticker -> spacing = spacing;
  ticker -> dirty = (!0);
}

static int ticker_timeout(gpointer data)
{
  GtkTicker *ticker = (GtkTicker *)data;
  if (gtk_widget_get_visible(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_widget_get_type())))) != 0) 
    gtk_widget_queue_resize(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_widget_get_type()))));
  return !0;
}

void gtk_ticker_start_scroll(GtkTicker *ticker)
{
  do {
    if (ticker != ((GtkTicker *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ticker != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)ticker;
      GType __t = gtk_ticker_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_TICKER (ticker)");
      return ;
    };
  }while (0);
  if ((ticker -> timer) != 0) 
    return ;
  ticker -> timer = (g_timeout_add((ticker -> interval),ticker_timeout,ticker));
}

void gtk_ticker_stop_scroll(GtkTicker *ticker)
{
  do {
    if (ticker != ((GtkTicker *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ticker != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)ticker;
      GType __t = gtk_ticker_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_TICKER (ticker)");
      return ;
    };
  }while (0);
  if ((ticker -> timer) == 0) 
    return ;
  g_source_remove((ticker -> timer));
  ticker -> timer = 0;
}

guint gtk_ticker_get_spacing(GtkTicker *ticker)
{
  do {
    if (ticker != ((GtkTicker *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ticker != NULL");
      return (-1);
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)ticker;
      GType __t = gtk_ticker_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_TICKER (ticker)");
      return (-1);
    };
  }while (0);
  return (ticker -> spacing);
}

static void gtk_ticker_map(GtkWidget *widget)
{
  GtkTicker *ticker;
  GtkTickerChild *child;
  GList *children;
  do {
    if (widget != ((GtkWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"widget != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)widget;
      GType __t = gtk_ticker_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_TICKER (widget)");
      return ;
    };
  }while (0);
  do {
    if (1) 
      do {
        ( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_object_get_type())))).flags |= GTK_MAPPED;
      }while (0);
    else 
      do {
        ( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_object_get_type())))).flags &= (~GTK_MAPPED);
      }while (0);
  }while (0);
  ticker = ((GtkTicker *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_ticker_get_type())));
  children = (ticker -> children);
  while(children != 0){
    child = (children -> data);
    children = (children -> next);
    if ((gtk_widget_get_visible((child -> widget)) != 0) && !((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)(child -> widget)),gtk_object_get_type())))).flags & GTK_MAPPED) != 0)) 
      gtk_widget_map((child -> widget));
  }
  gdk_window_show(gtk_widget_get_window(widget));
}

static void gtk_ticker_realize(GtkWidget *widget)
{
  GdkWindowAttr attributes;
  gint attributes_mask;
  GdkWindow *window;
  GtkStyle *style;
#if GTK_CHECK_VERSION(2,18,0)
  GtkAllocation allocation;
#endif
  do {
    if (widget != ((GtkWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"widget != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)widget;
      GType __t = gtk_ticker_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_TICKER (widget)");
      return ;
    };
  }while (0);
  do {
    if (1) 
      do {
        ( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_object_get_type())))).flags |= GTK_REALIZED;
      }while (0);
    else 
      do {
        ( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_object_get_type())))).flags &= (~GTK_REALIZED);
      }while (0);
  }while (0);
  attributes.window_type = GDK_WINDOW_CHILD;
#if GTK_CHECK_VERSION(2,18,0)
  gtk_widget_get_allocation(widget,&allocation);
  attributes.x = allocation.x;
  attributes.y = allocation.y;
  attributes.width = allocation.width;
  attributes.height = allocation.height;
#else
#endif
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.visual = gtk_widget_get_visual(widget);
  attributes.colormap = gtk_widget_get_colormap(widget);
  attributes.event_mask = gtk_widget_get_events(widget);
  attributes.event_mask |= GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK;
  attributes_mask = (GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP);
  window = gdk_window_new(gtk_widget_get_parent_window(widget),&attributes,attributes_mask);
#if GTK_CHECK_VERSION(2,18,0)
  gtk_widget_set_window(widget,window);
#else
#endif
  gdk_window_set_user_data(window,widget);
#if GTK_CHECK_VERSION(2,14,0)
  style = gtk_widget_get_style(widget);
  style = gtk_style_attach(style,window);
  gtk_widget_set_style(widget,style);
#else
#endif
  gtk_style_set_background(style,window,GTK_STATE_NORMAL);
}

static void gtk_ticker_size_request(GtkWidget *widget,GtkRequisition *requisition)
{
  GtkTicker *ticker;
  GtkTickerChild *child;
  GList *children;
  GtkRequisition child_requisition;
  guint border_width;
  do {
    if (widget != ((GtkWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"widget != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)widget;
      GType __t = gtk_ticker_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_TICKER (widget)");
      return ;
    };
  }while (0);
  do {
    if (requisition != ((GtkRequisition *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"requisition != NULL");
      return ;
    };
  }while (0);
  ticker = ((GtkTicker *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_ticker_get_type())));
  requisition -> width = 0;
  requisition -> height = 0;
  children = (ticker -> children);
  while(children != 0){
    child = (children -> data);
    children = (children -> next);
    if (gtk_widget_get_visible((child -> widget)) != 0) {
      gtk_widget_size_request((child -> widget),&child_requisition);
      requisition -> height = (((requisition -> height) > child_requisition.height)?(requisition -> height) : child_requisition.height);
      requisition -> width += (child_requisition.width + (ticker -> spacing));
    }
  }
  if ((requisition -> width) > (ticker -> spacing)) 
    requisition -> width -= (ticker -> spacing);
  border_width = gtk_container_get_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_container_get_type()))));
  requisition -> height += (border_width * 2);
  requisition -> width += (border_width * 2);
}

static void gtk_ticker_compute_offsets(GtkTicker *ticker)
{
  GtkTickerChild *child;
  GtkRequisition child_requisition;
  GList *children;
  guint16 border_width;
  do {
    if (ticker != ((GtkTicker *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ticker != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)ticker;
      GType __t = gtk_ticker_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_TICKER(ticker)");
      return ;
    };
  }while (0);
  border_width = (gtk_container_get_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_container_get_type())))));
#if GTK_CHECK_VERSION(2,18,0)
{
    GtkAllocation allocation;
    gtk_widget_get_allocation(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_widget_get_type()))),&allocation);
    ticker -> width = allocation.width;
  }
#else
#endif
  ticker -> total = 0;
  children = (ticker -> children);
  while(children != 0){
    child = (children -> data);
    child -> x = 0;
    if (gtk_widget_get_visible((child -> widget)) != 0) {
      gtk_widget_get_child_requisition((child -> widget),&child_requisition);
      child -> offset = (ticker -> total);
      ticker -> total += ((child_requisition.width + border_width) + (ticker -> spacing));
    }
    children = (children -> next);
  }
  ticker -> dirty = 0;
}

static void gtk_ticker_size_allocate(GtkWidget *widget,GtkAllocation *allocation)
{
  GtkTicker *ticker;
  GtkTickerChild *child;
  GtkAllocation child_allocation;
  GtkRequisition child_requisition;
  GList *children;
  guint16 border_width;
  do {
    if (widget != ((GtkWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"widget != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)widget;
      GType __t = gtk_ticker_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_TICKER(widget)");
      return ;
    };
  }while (0);
  do {
    if (allocation != ((GtkAllocation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"allocation != NULL");
      return ;
    };
  }while (0);
  ticker = ((GtkTicker *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_ticker_get_type())));
#if GTK_CHECK_VERSION(2,18,0)
{
    GtkAllocation a;
    gtk_widget_get_allocation(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_widget_get_type()))),&a);
    if (a.width != (ticker -> width)) 
      ticker -> dirty = (!0);
  }
#else
#endif
  if ((ticker -> dirty) == !0) {
    gtk_ticker_compute_offsets(ticker);
  }
#if GTK_CHECK_VERSION(2,18,0)
  gtk_widget_set_allocation(widget,allocation);
#else
#endif
  if ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_object_get_type())))).flags & GTK_REALIZED) != 0) 
    gdk_window_move_resize(gtk_widget_get_window(widget),(allocation -> x),(allocation -> y),(allocation -> width),(allocation -> height));
  border_width = (gtk_container_get_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_container_get_type())))));
  children = (ticker -> children);
  while(children != 0){
    child = (children -> data);
    child -> x -= (ticker -> scootch);
    if (gtk_widget_get_visible((child -> widget)) != 0) {
      gtk_widget_get_child_requisition((child -> widget),&child_requisition);
      child_allocation.width = child_requisition.width;
      child_allocation.x = (((child -> offset) + border_width) + (child -> x));
      if ((child_allocation.x + child_allocation.width) < (allocation -> x)) {
        if ((ticker -> total) >= (allocation -> width)) {
          child -> x += (((allocation -> x) + (allocation -> width)) + ((ticker -> total) - ((allocation -> x) + (allocation -> width))));
        }
        else {
          child -> x += ((allocation -> x) + (allocation -> width));
        }
      }
      child_allocation.y = border_width;
      child_allocation.height = child_requisition.height;
      gtk_widget_size_allocate((child -> widget),&child_allocation);
    }
    children = (children -> next);
  }
}

void gtk_ticker_add(GtkTicker *ticker,GtkWidget *widget)
{
  gtk_ticker_add_real(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_container_get_type()))),widget);
  ticker -> dirty = (!0);
}

void gtk_ticker_remove(GtkTicker *ticker,GtkWidget *widget)
{
  gtk_ticker_remove_real(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_container_get_type()))),widget);
  ticker -> dirty = (!0);
}

static void gtk_ticker_add_real(GtkContainer *container,GtkWidget *widget)
{
  do {
    if (container != ((GtkContainer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"container != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)container;
      GType __t = gtk_ticker_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_TICKER (container)");
      return ;
    };
  }while (0);
  do {
    if (widget != ((GtkWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"widget != NULL");
      return ;
    };
  }while (0);
  gtk_ticker_put(((GtkTicker *)(g_type_check_instance_cast(((GTypeInstance *)container),gtk_ticker_get_type()))),widget);
}

static void gtk_ticker_remove_real(GtkContainer *container,GtkWidget *widget)
{
  GtkTicker *ticker;
  GtkTickerChild *child;
  GList *children;
  do {
    if (container != ((GtkContainer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"container != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)container;
      GType __t = gtk_ticker_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_TICKER (container)");
      return ;
    };
  }while (0);
  do {
    if (widget != ((GtkWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"widget != NULL");
      return ;
    };
  }while (0);
  ticker = ((GtkTicker *)(g_type_check_instance_cast(((GTypeInstance *)container),gtk_ticker_get_type())));
  children = (ticker -> children);
{
    while(children != 0){
      child = (children -> data);
      if ((child -> widget) == widget) {
        gboolean was_visible = gtk_widget_get_visible(widget);
        gtk_widget_unparent(widget);
        ticker -> children = g_list_remove_link((ticker -> children),children);
        g_list_free(children);
        g_free(child);
        if ((was_visible != 0) && (gtk_widget_get_visible(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)container),gtk_widget_get_type())))) != 0)) 
          gtk_widget_queue_resize(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)container),gtk_widget_get_type()))));
        break; 
      }
      children = (children -> next);
    }
  }
}

static void gtk_ticker_forall(GtkContainer *container,gboolean include_internals,GtkCallback callback,gpointer callback_data)
{
  GtkTicker *ticker;
  GtkTickerChild *child;
  GList *children;
  do {
    if (container != ((GtkContainer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"container != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)container;
      GType __t = gtk_ticker_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_TICKER (container)");
      return ;
    };
  }while (0);
  do {
    if (callback != ((void (*)(GtkWidget *, gpointer ))((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"callback != NULL");
      return ;
    };
  }while (0);
  ticker = ((GtkTicker *)(g_type_check_instance_cast(((GTypeInstance *)container),gtk_ticker_get_type())));
  children = (ticker -> children);
  while(children != 0){
    child = (children -> data);
    children = (children -> next);
    ( *callback)((child -> widget),callback_data);
  }
}
