/*
 * Purple Ticker Plugin
 * The line below doesn't apply at all, does it?  It should be Syd, Sean, and
 * maybe Nathan, I believe.
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02111-1301, USA.
 */
/*
 * ticker.c -- Syd Logan, Summer 2000
 * pluginized- Sean Egan, Summer 2002
 */
#include "internal.h"
#include "pidgin.h"
#include "blist.h"
#include "conversation.h"
#include "debug.h"
#include "prpl.h"
#include "signals.h"
#include "version.h"
#include "gtkblist.h"
#include "gtkplugin.h"
#include "gtkutils.h"
#include "pidginstock.h"
#include "gtkticker.h"
#define TICKER_PLUGIN_ID "gtk-ticker"
static GtkWidget *tickerwindow = (GtkWidget *)((void *)0);
static GtkWidget *ticker;
typedef struct __unnamed_class___F0_L49_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L287R__Pe___variable_name_unknown_scope_and_name__scope__contact__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__ebox__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__label__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__icon__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_guintUi__typedef_declaration_variable_name_unknown_scope_and_name__scope__timeout {
PurpleContact *contact;
GtkWidget *ebox;
GtkWidget *label;
GtkWidget *icon;
guint timeout;}TickerData;
static GList *tickerbuds = (GList *)((void *)0);
static void buddy_ticker_update_contact(PurpleContact *contact);

static gboolean buddy_ticker_destroy_window(GtkWidget *window,GdkEventAny *event,gpointer data)
{
  if (window != 0) 
    gtk_widget_hide(window);
/* don't actually destroy the window */
  return (!0);
}

static void buddy_ticker_create_window()
{
  if (tickerwindow != 0) {
    gtk_widget_show(tickerwindow);
    return ;
  }
  tickerwindow = pidgin_create_window(((const char *)(dgettext("pidgin","Buddy Ticker"))),0,"ticker",(!0));
  gtk_window_set_default_size(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)tickerwindow),gtk_window_get_type()))),500,(-1));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tickerwindow),((GType )(20 << 2))))),"delete_event",((GCallback )buddy_ticker_destroy_window),0,0,((GConnectFlags )0));
  ticker = gtk_ticker_new();
  gtk_ticker_set_spacing(((GtkTicker *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_ticker_get_type()))),20);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)tickerwindow),gtk_container_get_type()))),ticker);
  gtk_ticker_set_interval(((GtkTicker *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_ticker_get_type()))),500);
  gtk_ticker_set_scootch(((GtkTicker *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_ticker_get_type()))),10);
  gtk_ticker_start_scroll(((GtkTicker *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_ticker_get_type()))));
  gtk_widget_set_size_request(ticker,1,(-1));
  gtk_widget_show_all(tickerwindow);
}

static gboolean buddy_click_cb(GtkWidget *widget,GdkEventButton *event,gpointer user_data)
{
  PurpleContact *contact = user_data;
  PurpleBuddy *b = purple_contact_get_priority_buddy(contact);
  PurpleConversation *conv = purple_conversation_new(PURPLE_CONV_TYPE_IM,purple_buddy_get_account(b),purple_buddy_get_name(b));
  purple_conversation_present(conv);
  return (!0);
}

static TickerData *buddy_ticker_find_contact(PurpleContact *c)
{
  GList *tb;
  for (tb = tickerbuds; tb != 0; tb = (tb -> next)) {
    TickerData *td = (tb -> data);
    if ((td -> contact) == c) 
      return td;
  }
  return 0;
}

static void buddy_ticker_set_pixmap(PurpleContact *c)
{
  TickerData *td = buddy_ticker_find_contact(c);
  PurpleBuddy *buddy;
  PurplePresence *presence;
  const char *stock;
  if (!(td != 0)) 
    return ;
  buddy = purple_contact_get_priority_buddy(c);
  presence = purple_buddy_get_presence(buddy);
  stock = pidgin_stock_id_from_presence(presence);
  if (!((td -> icon) != 0)) {
    td -> icon = gtk_image_new();
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(td -> icon)),((GType )(20 << 2))))),"stock",stock,"icon-size",gtk_icon_size_from_name("pidgin-icon-size-tango-microscopic"),((void *)((void *)0)));
  }
  else {
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(td -> icon)),((GType )(20 << 2))))),"stock",stock,((void *)((void *)0)));
  }
}

static gboolean buddy_ticker_set_pixmap_cb(gpointer data)
{
  TickerData *td = data;
  if (g_list_find(tickerbuds,td) != ((GList *)((void *)0))) {
    buddy_ticker_update_contact((td -> contact));
    td -> timeout = 0;
  }
  return 0;
}

static void buddy_ticker_add_buddy(PurpleBuddy *b)
{
  GtkWidget *hbox;
  TickerData *td;
  PurpleContact *contact;
  contact = purple_buddy_get_contact(b);
  buddy_ticker_create_window();
  if (!(ticker != 0)) 
    return ;
  if (buddy_ticker_find_contact(contact) != 0) {
    buddy_ticker_update_contact(contact);
    return ;
  }
  td = ((TickerData *)(g_malloc0_n(1,(sizeof(TickerData )))));
  td -> contact = contact;
  tickerbuds = g_list_append(tickerbuds,td);
  td -> ebox = gtk_event_box_new();
  gtk_ticker_add(((GtkTicker *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_ticker_get_type()))),(td -> ebox));
  hbox = gtk_hbox_new(0,0);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(td -> ebox)),gtk_container_get_type()))),hbox);
  buddy_ticker_set_pixmap(contact);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),(td -> icon),0,0,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(td -> ebox)),((GType )(20 << 2))))),"button-press-event",((GCallback )buddy_click_cb),contact,0,((GConnectFlags )0));
  td -> label = gtk_label_new(purple_contact_get_alias(contact));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),(td -> label),0,0,2);
  gtk_widget_show_all((td -> ebox));
  gtk_widget_show(tickerwindow);
/*
	 * Update the icon in a few seconds (after the open door icon has
	 * changed).  This is somewhat ugly.
	 */
  td -> timeout = g_timeout_add(11000,buddy_ticker_set_pixmap_cb,td);
}

static void buddy_ticker_remove(TickerData *td)
{
  gtk_ticker_remove(((GtkTicker *)(g_type_check_instance_cast(((GTypeInstance *)ticker),gtk_ticker_get_type()))),(td -> ebox));
  tickerbuds = g_list_remove(tickerbuds,td);
  if ((td -> timeout) != 0) 
    g_source_remove((td -> timeout));
  g_free(td);
}

static void buddy_ticker_update_contact(PurpleContact *contact)
{
  TickerData *td = buddy_ticker_find_contact(contact);
  if (!(td != 0)) 
    return ;
/* pop up the ticker window again */
  buddy_ticker_create_window();
  if (purple_contact_get_priority_buddy(contact) == ((PurpleBuddy *)((void *)0))) 
    buddy_ticker_remove(td);
  else {
    buddy_ticker_set_pixmap(contact);
    gtk_label_set_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(td -> label)),gtk_label_get_type()))),purple_contact_get_alias(contact));
  }
}

static void buddy_ticker_remove_buddy(PurpleBuddy *b)
{
  PurpleContact *c = purple_buddy_get_contact(b);
  TickerData *td = buddy_ticker_find_contact(c);
  if (!(td != 0)) 
    return ;
  purple_contact_invalidate_priority_buddy(c);
/* pop up the ticker window again */
  buddy_ticker_create_window();
  buddy_ticker_update_contact(c);
}

static void buddy_ticker_show()
{
  PurpleBlistNode *gnode;
  PurpleBlistNode *cnode;
  PurpleBlistNode *bnode;
  PurpleBuddy *b;
  for (gnode = purple_blist_get_root(); gnode != 0; gnode = purple_blist_node_get_sibling_next(gnode)) {
    if (!((purple_blist_node_get_type(gnode)) == PURPLE_BLIST_GROUP_NODE)) 
      continue; 
    for (cnode = purple_blist_node_get_first_child(gnode); cnode != 0; cnode = purple_blist_node_get_sibling_next(cnode)) {
      if (!((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CONTACT_NODE)) 
        continue; 
      for (bnode = purple_blist_node_get_first_child(cnode); bnode != 0; bnode = purple_blist_node_get_sibling_next(bnode)) {
        if (!((purple_blist_node_get_type(bnode)) == PURPLE_BLIST_BUDDY_NODE)) 
          continue; 
        b = ((PurpleBuddy *)bnode);
        if (((b != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(b))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(b))) != 0)) 
          buddy_ticker_add_buddy(b);
      }
    }
  }
}

static void buddy_signon_cb(PurpleBuddy *b)
{
  PurpleContact *c = purple_buddy_get_contact(b);
  purple_contact_invalidate_priority_buddy(c);
  if (buddy_ticker_find_contact(c) != 0) 
    buddy_ticker_update_contact(c);
  else 
    buddy_ticker_add_buddy(b);
}

static void buddy_signoff_cb(PurpleBuddy *b)
{
  buddy_ticker_remove_buddy(b);
  if (!(tickerbuds != 0)) 
    gtk_widget_hide(tickerwindow);
}

static void status_changed_cb(PurpleBuddy *b,PurpleStatus *os,PurpleStatus *s)
{
  PurpleContact *c = purple_buddy_get_contact(b);
  if (buddy_ticker_find_contact(c) != 0) 
    buddy_ticker_set_pixmap(c);
  else 
    buddy_ticker_add_buddy(b);
}

static void signoff_cb(PurpleConnection *gc)
{
  TickerData *td;
  if (!(purple_connections_get_all() != 0)) {
    while(tickerbuds != 0){
      td = (tickerbuds -> data);
      tickerbuds = g_list_delete_link(tickerbuds,tickerbuds);
      if ((td -> timeout) != 0) 
        g_source_remove((td -> timeout));
      g_free(td);
    }
    gtk_widget_destroy(tickerwindow);
    tickerwindow = ((GtkWidget *)((void *)0));
    ticker = ((GtkWidget *)((void *)0));
  }
  else {
    GList *t = tickerbuds;
    while(t != 0){
      td = (t -> data);
      t = (t -> next);
      buddy_ticker_update_contact((td -> contact));
    }
  }
}
/*
 *  EXPORTED FUNCTIONS
 */

static gboolean plugin_load(PurplePlugin *plugin)
{
  void *blist_handle = purple_blist_get_handle();
  purple_signal_connect(purple_connections_get_handle(),"signed-off",plugin,((PurpleCallback )signoff_cb),0);
  purple_signal_connect(blist_handle,"buddy-signed-on",plugin,((PurpleCallback )buddy_signon_cb),0);
  purple_signal_connect(blist_handle,"buddy-signed-off",plugin,((PurpleCallback )buddy_signoff_cb),0);
  purple_signal_connect(blist_handle,"buddy-status-changed",plugin,((PurpleCallback )status_changed_cb),0);
  if (purple_connections_get_all() != 0) 
    buddy_ticker_show();
  return (!0);
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  TickerData *td;
  while(tickerbuds != 0){
    td = (tickerbuds -> data);
    tickerbuds = g_list_delete_link(tickerbuds,tickerbuds);
    if ((td -> timeout) != 0) 
      g_source_remove((td -> timeout));
    g_free(td);
  }
  if (tickerwindow != ((GtkWidget *)((void *)0))) {
    gtk_widget_destroy(tickerwindow);
    tickerwindow = ((GtkWidget *)((void *)0));
  }
  return (!0);
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gtk-gaim"), (0), ((GList *)((void *)0)), (0), ("gtk-ticker"), ("Buddy Ticker"), ("2.10.9"), ("A horizontal scrolling version of the buddy list."), ("A horizontal scrolling version of the buddy list."), ("Syd Logan"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
