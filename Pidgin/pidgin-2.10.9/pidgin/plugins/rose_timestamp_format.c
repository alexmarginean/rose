#include "internal.h"
#include "debug.h"
#include "log.h"
#include "plugin.h"
#include "util.h"
#include "version.h"
#include "gtkconv.h"
#include "gtkplugin.h"
#include "gtkimhtml.h"
#include <time.h>

static const char *format_12hour_hour(const struct tm *tm)
{
  static char hr[3UL];
  int hour = ((tm -> tm_hour) % 12);
  if (hour == 0) 
    hour = 12;
  g_snprintf(hr,(sizeof(hr)),"%d",hour);
  return hr;
}

static PurplePluginPrefFrame *get_plugin_pref_frame(PurplePlugin *plugin)
{
  PurplePluginPrefFrame *frame;
  PurplePluginPref *ppref;
  char *tmp;
  frame = purple_plugin_pref_frame_new();
  ppref = purple_plugin_pref_new_with_label(((const char *)(dgettext("pidgin","Timestamp Format Options"))));
  purple_plugin_pref_frame_add(frame,ppref);
  tmp = g_strdup(((const char *)(dgettext("pidgin","_Force timestamp format:"))));
  ppref = purple_plugin_pref_new_with_name_and_label("/plugins/gtk/timestamp_format/force",tmp);
  purple_plugin_pref_set_type(ppref,PURPLE_PLUGIN_PREF_CHOICE);
  purple_plugin_pref_add_choice(ppref,((const char *)(dgettext("pidgin","Use system default"))),"default");
  purple_plugin_pref_add_choice(ppref,((const char *)(dgettext("pidgin","12 hour time format"))),"force12");
  purple_plugin_pref_add_choice(ppref,((const char *)(dgettext("pidgin","24 hour time format"))),"force24");
  purple_plugin_pref_frame_add(frame,ppref);
  g_free(tmp);
  ppref = purple_plugin_pref_new_with_label(((const char *)(dgettext("pidgin","Show dates in..."))));
  purple_plugin_pref_frame_add(frame,ppref);
  ppref = purple_plugin_pref_new_with_name_and_label("/plugins/gtk/timestamp_format/use_dates/conversation",((const char *)(dgettext("pidgin","Co_nversations:"))));
  purple_plugin_pref_set_type(ppref,PURPLE_PLUGIN_PREF_CHOICE);
  purple_plugin_pref_add_choice(ppref,((const char *)(dgettext("pidgin","For delayed messages"))),"automatic");
  purple_plugin_pref_add_choice(ppref,((const char *)(dgettext("pidgin","For delayed messages and in chats"))),"chats");
  purple_plugin_pref_add_choice(ppref,((const char *)(dgettext("pidgin","Always"))),"always");
  purple_plugin_pref_frame_add(frame,ppref);
  ppref = purple_plugin_pref_new_with_name_and_label("/plugins/gtk/timestamp_format/use_dates/log",((const char *)(dgettext("pidgin","_Message Logs:"))));
  purple_plugin_pref_set_type(ppref,PURPLE_PLUGIN_PREF_CHOICE);
  purple_plugin_pref_add_choice(ppref,((const char *)(dgettext("pidgin","For delayed messages"))),"automatic");
  purple_plugin_pref_add_choice(ppref,((const char *)(dgettext("pidgin","For delayed messages and in chats"))),"chats");
  purple_plugin_pref_add_choice(ppref,((const char *)(dgettext("pidgin","Always"))),"always");
  purple_plugin_pref_frame_add(frame,ppref);
  return frame;
}

static char *timestamp_cb_common(PurpleConversation *conv,time_t t,gboolean show_date,const char *force,const char *dates,gboolean parens)
{
  struct tm *tm;
  do {
    if (dates != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"dates != NULL");
      return 0;
    };
  }while (0);
  tm = localtime((&t));
  if (((show_date != 0) || !(strcmp(dates,"always") != 0)) || (((conv != ((PurpleConversation *)((void *)0))) && ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT)) && !(strcmp(dates,"chats") != 0))) {
    if (g_str_equal(force,"force24") != 0) 
      return g_strdup_printf("%s%s%s",((parens != 0)?"(" : ""),purple_utf8_strftime("%Y-%m-%d %H:%M:%S",tm),((parens != 0)?")" : ""));
    else if (g_str_equal(force,"force12") != 0) {
      char *date = g_strdup_printf("%s",purple_utf8_strftime("%Y-%m-%d ",tm));
      char *remtime = g_strdup_printf("%s",purple_utf8_strftime(":%M:%S %p",tm));
      const char *hour = format_12hour_hour(tm);
      char *output;
      output = g_strdup_printf("%s%s%s%s%s",((parens != 0)?"(" : ""),date,hour,remtime,((parens != 0)?")" : ""));
      g_free(date);
      g_free(remtime);
      return output;
    }
    else 
      return g_strdup_printf("%s%s%s",((parens != 0)?"(" : ""),purple_date_format_long(tm),((parens != 0)?")" : ""));
  }
  if (g_str_equal(force,"force24") != 0) 
    return g_strdup_printf("%s%s%s",((parens != 0)?"(" : ""),purple_utf8_strftime("%H:%M:%S",tm),((parens != 0)?")" : ""));
  else if (g_str_equal(force,"force12") != 0) {
    const char *hour = format_12hour_hour(tm);
    char *remtime = g_strdup_printf("%s",purple_utf8_strftime(":%M:%S %p",tm));
    char *output = g_strdup_printf("%s%s%s%s",((parens != 0)?"(" : ""),hour,remtime,((parens != 0)?")" : ""));
    g_free(remtime);
    return output;
  }
  return 0;
}

static char *conversation_timestamp_cb(PurpleConversation *conv,time_t t,gboolean show_date,gpointer data)
{
  const char *force = purple_prefs_get_string("/plugins/gtk/timestamp_format/force");
  const char *dates = purple_prefs_get_string("/plugins/gtk/timestamp_format/use_dates/conversation");
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return 0;
    };
  }while (0);
  return timestamp_cb_common(conv,t,show_date,force,dates,(!0));
}

static char *log_timestamp_cb(PurpleLog *log,time_t t,gboolean show_date,gpointer data)
{
  const char *force = purple_prefs_get_string("/plugins/gtk/timestamp_format/force");
  const char *dates = purple_prefs_get_string("/plugins/gtk/timestamp_format/use_dates/log");
  do {
    if (log != ((PurpleLog *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"log != NULL");
      return 0;
    };
  }while (0);
  return timestamp_cb_common((log -> conv),t,show_date,force,dates,0);
}

static void menu_cb(GtkWidget *item,gpointer data)
{
  PurplePlugin *plugin = data;
  GtkWidget *frame = pidgin_plugin_get_config_frame(plugin);
  GtkWidget *dialog;
  if (!(frame != 0)) 
    return ;
  dialog = gtk_dialog_new_with_buttons("",0,(GTK_DIALOG_NO_SEPARATOR | GTK_DIALOG_DESTROY_WITH_PARENT),"gtk-close",GTK_RESPONSE_CLOSE,((void *)((void *)0)));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dialog),((GType )(20 << 2))))),"response",((GCallback )gtk_widget_destroy),dialog,0,G_CONNECT_AFTER);
#if GTK_CHECK_VERSION(2,14,0)
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_dialog_get_content_area(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type())))))),gtk_container_get_type()))),frame);
#else
#endif
  gtk_window_set_role(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_window_get_type()))),"plugin_config");
  gtk_window_set_title(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_window_get_type()))),((const char *)(dgettext("pidgin",purple_plugin_get_name(plugin)))));
  gtk_widget_show_all(dialog);
}

static gboolean textview_emission_hook(GSignalInvocationHint *hint,guint n_params,const GValue *pvalues,gpointer data)
{
  GtkTextView *view = (GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(g_value_get_object(pvalues))),gtk_text_view_get_type()));
  GtkWidget *menu;
  GtkWidget *item;
  GtkTextBuffer *buffer;
  GtkTextIter cursor;
  int cx;
  int cy;
  int bx;
  int by;
  if (!((({
    GTypeInstance *__inst = (GTypeInstance *)view;
    GType __t = gtk_imhtml_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) 
    return (!0);
#if GTK_CHECK_VERSION(2,14,0)
  if (!(gdk_window_get_pointer(gtk_widget_get_window(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)view),gtk_widget_get_type())))),&cx,&cy,0) != 0)) 
    return (!0);
#else
#endif
  buffer = gtk_text_view_get_buffer(view);
  gtk_text_view_window_to_buffer_coords(view,GTK_TEXT_WINDOW_TEXT,cx,cy,&bx,&by);
  gtk_text_view_get_iter_at_location(view,&cursor,bx,by);
  if (!(gtk_text_iter_has_tag((&cursor),gtk_text_tag_table_lookup(gtk_text_buffer_get_tag_table(buffer),"comment")) != 0)) 
    return (!0);
  menu = (g_value_get_object((pvalues + 1)));
  item = gtk_menu_item_new_with_label(((const char *)(dgettext("pidgin","Timestamp Format Options"))));
  gtk_widget_show_all(item);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"activate",((GCallback )menu_cb),data,0,((GConnectFlags )0));
  gtk_menu_shell_insert(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item,0);
  item = gtk_separator_menu_item_new();
  gtk_widget_show(item);
  gtk_menu_shell_insert(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item,1);
  return (!0);
}
static guint signal_id;
static gulong hook_id;

static gboolean plugin_load(PurplePlugin *plugin)
{
  gpointer klass = (gpointer )((void *)0);
  purple_signal_connect(pidgin_conversations_get_handle(),"conversation-timestamp",plugin,((PurpleCallback )conversation_timestamp_cb),0);
  purple_signal_connect(purple_log_get_handle(),"log-timestamp",plugin,((PurpleCallback )log_timestamp_cb),0);
  klass = g_type_class_ref(gtk_text_view_get_type());
/* In 3.0.0, use purple_g_signal_connect_flags */
  g_signal_parse_name("populate_popup",gtk_text_view_get_type(),&signal_id,0,0);
  hook_id = g_signal_add_emission_hook(signal_id,0,textview_emission_hook,plugin,0);
  g_type_class_unref(klass);
  return (!0);
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  g_signal_remove_emission_hook(signal_id,hook_id);
  return (!0);
}
static PurplePluginUiInfo prefs_info = {(get_plugin_pref_frame), (0), ((PurplePluginPrefFrame *)((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* page num (Reserved) */
/* frame (Reserved) */
/* padding */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gtk-gaim"), (0), ((GList *)((void *)0)), (0), ("core-timestamp_format"), ("Message Timestamp Formats"), ("2.10.9"), ("Customizes the message timestamp formats."), ("This plugin allows the user to customize conversation and logging message timestamp formats."), ("Richard Laager <rlaager@pidgin.im>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), (&prefs_info), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/**< prefs_info     */
/**< actions        */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
  purple_prefs_add_none("/plugins/gtk");
  purple_prefs_add_none("/plugins/gtk/timestamp_format");
  if (!(purple_prefs_exists("/plugins/gtk/timestamp_format/force") != 0) && (purple_prefs_exists("/plugins/gtk/timestamp_format/force_24hr") != 0)) {
    if (purple_prefs_get_bool("/plugins/gtk/timestamp_format/force_24hr") != 0) 
      purple_prefs_add_string("/plugins/gtk/timestamp_format/force","force24");
    else 
      purple_prefs_add_string("/plugins/gtk/timestamp_format/force","default");
  }
  else 
    purple_prefs_add_string("/plugins/gtk/timestamp_format/force","default");
  purple_prefs_add_none("/plugins/gtk/timestamp_format/use_dates");
  purple_prefs_add_string("/plugins/gtk/timestamp_format/use_dates/conversation","automatic");
  purple_prefs_add_string("/plugins/gtk/timestamp_format/use_dates/log","automatic");
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
