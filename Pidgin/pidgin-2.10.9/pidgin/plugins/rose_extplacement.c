/*
 * Extra conversation placement options for Purple
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301, USA.
 */
#include "internal.h"
#include "pidgin.h"
#include "conversation.h"
#include "version.h"
#include "gtkplugin.h"
#include "gtkconv.h"
#include "gtkconvwin.h"

static void conv_placement_by_number(PidginConversation *conv)
{
  PidginWindow *win = (PidginWindow *)((void *)0);
  GList *wins = (GList *)((void *)0);
  if (purple_prefs_get_bool("/plugins/gtk/extplacement/placement_number_separate") != 0) 
    win = pidgin_conv_window_last_with_type(purple_conversation_get_type((conv -> active_conv)));
  else if ((wins = pidgin_conv_windows_get_list()) != ((GList *)((void *)0))) 
    win = ( *g_list_last(wins)).data;
  if (win == ((PidginWindow *)((void *)0))) {
    win = pidgin_conv_window_new();
    pidgin_conv_window_add_gtkconv(win,conv);
    pidgin_conv_window_show(win);
  }
  else {
    int max_count = purple_prefs_get_int("/plugins/gtk/extplacement/placement_number");
    int count = (pidgin_conv_window_get_gtkconv_count(win));
    if (count < max_count) 
      pidgin_conv_window_add_gtkconv(win,conv);
    else {
      GList *l = (GList *)((void *)0);
      for (l = pidgin_conv_windows_get_list(); l != ((GList *)((void *)0)); l = (l -> next)) {
        win = (l -> data);
        if ((purple_prefs_get_bool("/plugins/gtk/extplacement/placement_number_separate") != 0) && ((purple_conversation_get_type((pidgin_conv_window_get_active_conversation(win)))) != (purple_conversation_get_type((conv -> active_conv))))) 
          continue; 
        count = (pidgin_conv_window_get_gtkconv_count(win));
        if (count < max_count) {
          pidgin_conv_window_add_gtkconv(win,conv);
          return ;
        }
      }
      win = pidgin_conv_window_new();
      pidgin_conv_window_add_gtkconv(win,conv);
      pidgin_conv_window_show(win);
    }
  }
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  pidgin_conv_placement_add_fnc("number",((const char *)(dgettext("pidgin","By conversation count"))),conv_placement_by_number);
  purple_prefs_trigger_callback("/pidgin/conversations/placement");
  return (!0);
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  pidgin_conv_placement_remove_fnc("number");
  purple_prefs_trigger_callback("/pidgin/conversations/placement");
  return (!0);
}

static PurplePluginPrefFrame *get_plugin_pref_frame(PurplePlugin *plugin)
{
  PurplePluginPrefFrame *frame;
  PurplePluginPref *ppref;
  frame = purple_plugin_pref_frame_new();
  ppref = purple_plugin_pref_new_with_label(((const char *)(dgettext("pidgin","Conversation Placement"))));
  purple_plugin_pref_frame_add(frame,ppref);
/* Translators: "New conversations" should match the text in the preferences dialog and "By conversation count" should be the same text used above */
  ppref = purple_plugin_pref_new_with_label(((const char *)(dgettext("pidgin","Note: The preference for \"New conversations\" must be set to \"By conversation count\"."))));
  purple_plugin_pref_set_type(ppref,PURPLE_PLUGIN_PREF_INFO);
  purple_plugin_pref_frame_add(frame,ppref);
  ppref = purple_plugin_pref_new_with_name_and_label("/plugins/gtk/extplacement/placement_number",((const char *)(dgettext("pidgin","Number of conversations per window"))));
  purple_plugin_pref_set_bounds(ppref,1,50);
  purple_plugin_pref_frame_add(frame,ppref);
  ppref = purple_plugin_pref_new_with_name_and_label("/plugins/gtk/extplacement/placement_number_separate",((const char *)(dgettext("pidgin","Separate IM and Chat windows when placing by number"))));
  purple_plugin_pref_frame_add(frame,ppref);
  return frame;
}
static PurplePluginUiInfo prefs_info = {(get_plugin_pref_frame), (0), ((PurplePluginPrefFrame *)((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* page_num (Reserved) */
/* frame (Reserved) */
/* padding */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gtk-gaim"), (0), ((GList *)((void *)0)), (0), ("gtk-extplacement"), ("ExtPlacement"), ("2.10.9"), ("Extra conversation placement options."), ("Restrict the number of conversations per windows, optionally separating IMs and Chats"), ("Stu Tomlinson <stu@nosnilmot.com>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), (&prefs_info), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type			*/
/**< ui_requirement	*/
/**< flags			*/
/**< dependencies	*/
/**< priority		*/
/**< id				*/
/**< name			*/
/**< version		*/
/**< summary		*/
/**  description	*/
/**< author			*/
/**< homepage		*/
/**< load			*/
/**< unload			*/
/**< destroy		*/
/**< ui_info		*/
/**< extra_info		*/
/**< prefs_info		*/
/**< actions		*/
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
  purple_prefs_add_none("/plugins/gtk/extplacement");
  purple_prefs_add_int("/plugins/gtk/extplacement/placement_number",4);
  purple_prefs_add_bool("/plugins/gtk/extplacement/placement_number_separate",0);
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
