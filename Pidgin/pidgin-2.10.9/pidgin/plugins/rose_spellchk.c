/*
 * Purple - Replace certain misspelled words with their correct form.
 *
 * Signification changes were made by Benjamin Kahn ("xkahn") and
 * Richard Laager ("rlaager") in April 2005--you may want to contact
 * them if you have questions.
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
/*
 * A lot of this code (especially the config code) was taken directly
 * or nearly directly from xchat, version 1.4.2 by Peter Zelezny and others.
 */
#include "internal.h"
#include "pidgin.h"
#include "debug.h"
#include "notify.h"
#include "signals.h"
#include "util.h"
#include "version.h"
#include "gtkplugin.h"
#include "gtkprefs.h"
#include "gtkutils.h"
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#define SPELLCHECK_PLUGIN_ID "gtk-spellcheck"
#define SPELLCHK_OBJECT_KEY "spellchk"
enum __unnamed_enum___F0_L54_C1_BAD_COLUMN__COMMA__GOOD_COLUMN__COMMA__WORD_ONLY_COLUMN__COMMA__CASE_SENSITIVE_COLUMN__COMMA__N_COLUMNS {BAD_COLUMN,GOOD_COLUMN,WORD_ONLY_COLUMN,CASE_SENSITIVE_COLUMN,N_COLUMNS};

struct _spellchk 
{
  GtkTextView *view;
  GtkTextMark *mark_insert_start;
  GtkTextMark *mark_insert_end;
  gchar *word;
  gboolean inserting;
  gboolean ignore_correction;
  gboolean ignore_correction_on_send;
  gint pos;
}
;
typedef struct _spellchk spellchk;
static GtkListStore *model;

static gboolean is_word_uppercase(const gchar *word)
{
  for (; word[0] != 0; word = (g_utf8_find_next_char(word,0))) {
    gunichar c = g_utf8_get_char(word);
    if (!(((g_unichar_isupper(c) != 0) || (g_unichar_ispunct(c) != 0)) || (g_unichar_isspace(c) != 0))) 
      return 0;
  }
  return (!0);
}

static gboolean is_word_lowercase(const gchar *word)
{
  for (; word[0] != 0; word = (g_utf8_find_next_char(word,0))) {
    gunichar c = g_utf8_get_char(word);
    if (!(((g_unichar_islower(c) != 0) || (g_unichar_ispunct(c) != 0)) || (g_unichar_isspace(c) != 0))) 
      return 0;
  }
  return (!0);
}

static gboolean is_word_proper(const gchar *word)
{
  if (word[0] == 0) 
    return 0;
  if (!(g_unichar_isupper(g_utf8_get_char_validated(word,(-1))) != 0)) 
    return 0;
  return is_word_lowercase((g_utf8_offset_to_pointer(word,1)));
}

static gchar *make_word_proper(const gchar *word)
{
  char buf[7UL];
  gchar *lower = g_utf8_strdown(word,(-1));
  gint bytes;
  gchar *ret;
  bytes = g_unichar_to_utf8(g_unichar_toupper(g_utf8_get_char(word)),buf);
  buf[((bytes < sizeof(buf) - 1)?bytes : sizeof(buf) - 1)] = 0;
  ret = g_strconcat(buf,g_utf8_offset_to_pointer(lower,1),((void *)((void *)0)));
  g_free(lower);
  return ret;
}

static gboolean substitute_simple_buffer(GtkTextBuffer *buffer)
{
  GtkTextIter start;
  GtkTextIter end;
  GtkTreeIter treeiter;
  gchar *text = (gchar *)((void *)0);
  gtk_text_buffer_get_iter_at_offset(buffer,&start,0);
  gtk_text_buffer_get_iter_at_offset(buffer,&end,0);
  gtk_text_iter_forward_to_end(&end);
  text = gtk_text_buffer_get_text(buffer,(&start),(&end),0);
  if ((gtk_tree_model_get_iter_first(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&treeiter) != 0) && (text != 0)) {
    do {{
        GValue val1;
        const gchar *bad;
        gchar *cursor;
        glong char_pos;
        val1.g_type = 0;
        gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&treeiter,WORD_ONLY_COLUMN,&val1);
        if (g_value_get_boolean((&val1)) != 0) {
          g_value_unset(&val1);
          continue; 
        }
        g_value_unset(&val1);
        gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&treeiter,BAD_COLUMN,&val1);
        bad = g_value_get_string((&val1));
/* using g_utf8_* to get /character/ offsets instead of byte offsets for buffer */
        if ((cursor = g_strrstr(text,bad)) != 0) {
          GValue val2;
          const gchar *good;
          val2.g_type = 0;
          gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&treeiter,GOOD_COLUMN,&val2);
          good = g_value_get_string((&val2));
          char_pos = g_utf8_pointer_to_offset(text,cursor);
          gtk_text_buffer_get_iter_at_offset(buffer,&start,char_pos);
          gtk_text_buffer_get_iter_at_offset(buffer,&end,(char_pos + g_utf8_strlen(bad,(-1))));
          gtk_text_buffer_delete(buffer,&start,&end);
          gtk_text_buffer_get_iter_at_offset(buffer,&start,char_pos);
          gtk_text_buffer_insert(buffer,&start,good,(-1));
          g_value_unset(&val2);
          g_free(text);
          g_value_unset(&val1);
          return (!0);
        }
        g_value_unset(&val1);
      }
    }while (gtk_tree_model_iter_next(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&treeiter) != 0);
  }
  g_free(text);
  return 0;
}

static gchar *substitute_word(gchar *word)
{
  GtkTreeIter iter;
  gchar *outword;
  gchar *lowerword;
  gchar *foldedword;
  if (word == ((gchar *)((void *)0))) 
    return 0;
  lowerword = g_utf8_strdown(word,(-1));
  foldedword = g_utf8_casefold(word,(-1));
  if (gtk_tree_model_get_iter_first(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter) != 0) {
    do {{
        GValue val1;
        gboolean case_sensitive;
        const char *bad;
        gchar *tmpbad = (gchar *)((void *)0);
        val1.g_type = 0;
        gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,WORD_ONLY_COLUMN,&val1);
        if (!(g_value_get_boolean((&val1)) != 0)) {
          g_value_unset(&val1);
          continue; 
        }
        g_value_unset(&val1);
        gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,CASE_SENSITIVE_COLUMN,&val1);
        case_sensitive = g_value_get_boolean((&val1));
        g_value_unset(&val1);
        gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,BAD_COLUMN,&val1);
        bad = g_value_get_string((&val1));
        if (((case_sensitive != 0) && !(strcmp(bad,word) != 0)) || (!(case_sensitive != 0) && (!(strcmp(bad,lowerword) != 0) || (!(is_word_lowercase(bad) != 0) && !(strcmp((tmpbad = g_utf8_casefold(bad,(-1))),foldedword) != 0))))) {
          GValue val2;
          const char *good;
          g_free(tmpbad);
          val2.g_type = 0;
          gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,GOOD_COLUMN,&val2);
          good = g_value_get_string((&val2));
          if ((!(case_sensitive != 0) && (is_word_lowercase(bad) != 0)) && (is_word_lowercase(good) != 0)) {
            if (is_word_uppercase(word) != 0) 
              outword = g_utf8_strup(good,(-1));
            else if (is_word_proper(word) != 0) 
              outword = make_word_proper(good);
            else 
              outword = g_strdup(good);
          }
          else 
            outword = g_strdup(good);
          g_value_unset(&val1);
          g_value_unset(&val2);
          g_free(lowerword);
          g_free(foldedword);
          return outword;
        }
        g_value_unset(&val1);
        g_free(tmpbad);
      }
    }while (gtk_tree_model_iter_next(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter) != 0);
  }
  g_free(lowerword);
  g_free(foldedword);
  return 0;
}

static void spellchk_free(spellchk *spell)
{
  GtkTextBuffer *buffer;
  do {
    if (spell != ((spellchk *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"spell != NULL");
      return ;
    };
  }while (0);
  buffer = gtk_text_view_get_buffer((spell -> view));
  g_signal_handlers_disconnect_matched(buffer,G_SIGNAL_MATCH_DATA,0,0,0,0,spell);
  g_free((spell -> word));
  g_free(spell);
}
/* Pango doesn't know about the "'" character.  Let's fix that. */

static gboolean spellchk_inside_word(GtkTextIter *iter)
{
  gunichar ucs4_char = gtk_text_iter_get_char(iter);
  gchar *utf8_str;
  gchar c = 0;
  utf8_str = g_ucs4_to_utf8((&ucs4_char),1,0,0,0);
  if (utf8_str != ((gchar *)((void *)0))) {
    c = utf8_str[0];
    g_free(utf8_str);
  }
/* Hack because otherwise typing things like U.S. gets difficult
	 * if you have 'u' -> 'you' set as a correction...
	 *
	 * Part 1 of 2: This marks . as being an inside-word character. */
  if (c == '.') 
    return (!0);
/* Avoid problems with \r, for example (SF #1289031). */
  if (c == '\\') 
    return (!0);
  if (gtk_text_iter_inside_word(iter) == !0) 
    return (!0);
  if (c == '\'') {
    gboolean result = gtk_text_iter_backward_char(iter);
    gboolean output = gtk_text_iter_inside_word(iter);
    if (result != 0) {
/*
			 * Hack so that "u'll" will correct correctly.
			 */
      ucs4_char = gtk_text_iter_get_char(iter);
      utf8_str = g_ucs4_to_utf8((&ucs4_char),1,0,0,0);
      if (utf8_str != ((gchar *)((void *)0))) {
        c = utf8_str[0];
        g_free(utf8_str);
        if ((c == 'u') || (c == 'U')) {
          gtk_text_iter_forward_char(iter);
          return 0;
        }
      }
      gtk_text_iter_forward_char(iter);
    }
    return output;
  }
  else if (c == '&') 
    return (!0);
  return 0;
}

static gboolean spellchk_backward_word_start(GtkTextIter *iter)
{
  int output;
  int result;
  output = gtk_text_iter_backward_word_start(iter);
/* It didn't work...  */
  if (!(output != 0)) 
    return 0;
  while(spellchk_inside_word(iter) != 0){
    result = gtk_text_iter_backward_char(iter);
/* We can't go backwards anymore?  We're at the beginning of the word. */
    if (!(result != 0)) 
      return (!0);
    if (!(spellchk_inside_word(iter) != 0)) {
      gtk_text_iter_forward_char(iter);
      return (!0);
    }
    output = gtk_text_iter_backward_word_start(iter);
    if (!(output != 0)) 
      return 0;
  }
  return (!0);
}

static gboolean check_range(spellchk *spell,GtkTextBuffer *buffer,GtkTextIter start,GtkTextIter end,gboolean sending)
{
  gboolean replaced;
  gboolean result;
  gchar *tmp;
  int period_count = 0;
  gchar *word;
  GtkTextMark *mark;
  GtkTextIter pos;
  if ((replaced = substitute_simple_buffer(buffer)) != 0) {
    mark = gtk_text_buffer_get_insert(buffer);
    gtk_text_buffer_get_iter_at_mark(buffer,&pos,mark);
    spell -> pos = gtk_text_iter_get_offset((&pos));
    gtk_text_buffer_get_iter_at_mark(buffer,&start,mark);
    gtk_text_buffer_get_iter_at_mark(buffer,&end,mark);
  }
  if (!(sending != 0)) {
/* We need to go backwords to find out if we are inside a word or not. */
    gtk_text_iter_backward_char(&end);
    if (spellchk_inside_word(&end) != 0) {
      gtk_text_iter_forward_char(&end);
/* We only pay attention to whole words. */
      return replaced;
    }
  }
/* We could be in the middle of a whitespace block.  Check for that. */
  result = gtk_text_iter_backward_char(&end);
  if (!(spellchk_inside_word(&end) != 0)) {
    if (result != 0) 
      gtk_text_iter_forward_char(&end);
    return replaced;
  }
  if (result != 0) 
    gtk_text_iter_forward_char(&end);
/* Move backwards to the beginning of the word. */
  spellchk_backward_word_start(&start);
  g_free((spell -> word));
  spell -> word = gtk_text_iter_get_text((&start),(&end));
/* Hack because otherwise typing things like U.S. gets difficult
	 * if you have 'u' -> 'you' set as a correction...
	 *
	 * Part 2 of 2: This chops periods off the end of the word so
	 * the right substitution entry is found. */
  tmp = g_strdup((spell -> word));
  if ((tmp != ((gchar *)((void *)0))) && (( *tmp) != 0)) {
    gchar *c;
{
      for (c = ((tmp + strlen(tmp)) - 1); c != tmp; c--) {
        if (( *c) == '.') {
           *c = 0;
          period_count++;
        }
        else 
          break; 
      }
    }
  }
  if ((word = substitute_word(tmp)) != 0) {
    GtkTextMark *mark;
    GtkTextIter pos;
    gchar *tmp2;
    int i;
    for (i = 1; i <= period_count; i++) {
      tmp2 = g_strconcat(word,".",((void *)((void *)0)));
      g_free(word);
      word = tmp2;
    }
    gtk_text_buffer_delete(buffer,&start,&end);
    gtk_text_buffer_insert(buffer,&start,word,(-1));
    mark = gtk_text_buffer_get_insert(buffer);
    gtk_text_buffer_get_iter_at_mark(buffer,&pos,mark);
    spell -> pos = gtk_text_iter_get_offset((&pos));
    g_free(word);
    g_free(tmp);
    return (!0);
  }
  g_free(tmp);
  g_free((spell -> word));
  spell -> word = ((gchar *)((void *)0));
  return replaced;
}
/* insertion works like this:
 *  - before the text is inserted, we mark the position in the buffer.
 *  - after the text is inserted, we see where our mark is and use that and
 *    the current position to check the entire range of inserted text.
 *
 * this may be overkill for the common case (inserting one character). */

static void insert_text_before(GtkTextBuffer *buffer,GtkTextIter *iter,gchar *text,gint len,spellchk *spell)
{
  if ((spell -> inserting) == !0) 
    return ;
  spell -> inserting = (!0);
  g_free((spell -> word));
  spell -> word = ((gchar *)((void *)0));
  gtk_text_buffer_move_mark(buffer,(spell -> mark_insert_start),iter);
}

static void insert_text_after(GtkTextBuffer *buffer,GtkTextIter *iter,gchar *text,gint len,spellchk *spell)
{
  GtkTextIter start;
  GtkTextIter end;
  GtkTextMark *mark;
  spell -> ignore_correction_on_send = 0;
  if ((spell -> ignore_correction) != 0) {
    spell -> ignore_correction = 0;
    return ;
  }
/* we need to check a range of text. */
  gtk_text_buffer_get_iter_at_mark(buffer,&start,(spell -> mark_insert_start));
  if (len == 1) 
    check_range(spell,buffer,start, *iter,0);
/* if check_range modified the buffer, iter has been invalidated */
  mark = gtk_text_buffer_get_insert(buffer);
  gtk_text_buffer_get_iter_at_mark(buffer,&end,mark);
  gtk_text_buffer_move_mark(buffer,(spell -> mark_insert_end),(&end));
  spell -> inserting = 0;
}

static void delete_range_after(GtkTextBuffer *buffer,GtkTextIter *start,GtkTextIter *end,spellchk *spell)
{
  GtkTextIter start2;
  GtkTextIter end2;
  GtkTextMark *mark;
  GtkTextIter pos;
  gint place;
  spell -> ignore_correction_on_send = 0;
  if (!((spell -> word) != 0)) 
    return ;
  if ((spell -> inserting) == !0) 
    return ;
  spell -> inserting = (!0);
  mark = gtk_text_buffer_get_insert(buffer);
  gtk_text_buffer_get_iter_at_mark(buffer,&pos,mark);
  place = gtk_text_iter_get_offset((&pos));
  if ((place + 1) != (spell -> pos)) {
    g_free((spell -> word));
    spell -> word = ((gchar *)((void *)0));
    return ;
  }
  gtk_text_buffer_get_iter_at_mark(buffer,&start2,(spell -> mark_insert_start));
  gtk_text_buffer_get_iter_at_mark(buffer,&end2,(spell -> mark_insert_end));
  gtk_text_buffer_delete(buffer,&start2,&end2);
  gtk_text_buffer_insert(buffer,&start2,(spell -> word),(-1));
  spell -> ignore_correction = (!0);
  spell -> ignore_correction_on_send = (!0);
  spell -> inserting = 0;
  g_free((spell -> word));
  spell -> word = ((gchar *)((void *)0));
}

static void message_send_cb(GtkWidget *widget,spellchk *spell)
{
  GtkTextBuffer *buffer;
  GtkTextIter start;
  GtkTextIter end;
  GtkTextMark *mark;
  gboolean replaced;
  if ((spell -> ignore_correction_on_send) != 0) {
    spell -> ignore_correction_on_send = 0;
    return ;
  }
#if 0
#endif
  buffer = gtk_text_view_get_buffer((spell -> view));
  gtk_text_buffer_get_end_iter(buffer,&start);
  gtk_text_buffer_get_end_iter(buffer,&end);
  spell -> inserting = (!0);
  replaced = check_range(spell,buffer,start,end,(!0));
  spell -> inserting = 0;
/* if check_range modified the buffer, iter has been invalidated */
  mark = gtk_text_buffer_get_insert(buffer);
  gtk_text_buffer_get_iter_at_mark(buffer,&end,mark);
  gtk_text_buffer_move_mark(buffer,(spell -> mark_insert_end),(&end));
  if (replaced != 0) {
    g_signal_stop_emission_by_name(widget,"message_send");
    spell -> ignore_correction_on_send = (!0);
  }
}

static void spellchk_new_attach(PurpleConversation *conv)
{
  spellchk *spell;
  GtkTextBuffer *buffer;
  GtkTextIter start;
  GtkTextIter end;
  PidginConversation *gtkconv;
  GtkTextView *view;
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  view = ((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_text_view_get_type())));
  spell = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)view),((GType )(20 << 2))))),"spellchk"));
  if (spell != ((spellchk *)((void *)0))) 
    return ;
/* attach to the widget */
  spell = ((spellchk *)(g_malloc0_n(1,(sizeof(spellchk )))));
  spell -> view = view;
  g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)view),((GType )(20 << 2))))),"spellchk",spell,((GDestroyNotify )spellchk_free));
  buffer = gtk_text_view_get_buffer(view);
/* we create the mark here, but we don't use it until text is
	 * inserted, so we don't really care where iter points.  */
  gtk_text_buffer_get_bounds(buffer,&start,&end);
  spell -> mark_insert_start = gtk_text_buffer_create_mark(buffer,"spellchk-insert-start",(&start),(!0));
  spell -> mark_insert_end = gtk_text_buffer_create_mark(buffer,"spellchk-insert-end",(&start),(!0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buffer),((GType )(20 << 2))))),"delete-range",((GCallback )delete_range_after),spell,0,G_CONNECT_AFTER);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buffer),((GType )(20 << 2))))),"insert-text",((GCallback )insert_text_before),spell,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buffer),((GType )(20 << 2))))),"insert-text",((GCallback )insert_text_after),spell,0,G_CONNECT_AFTER);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),((GType )(20 << 2))))),"message_send",((GCallback )message_send_cb),spell,0,((GConnectFlags )0));
}

static int buf_get_line(char *ibuf,char **buf,int *position,gsize len)
{
  int pos =  *position;
  int spos = pos;
  if (pos == len) 
    return 0;
  while(!((ibuf[pos] == 10) || ((ibuf[pos] == 13) && (ibuf[pos + 1] != 10)))){
    pos++;
    if (pos == len) 
      return 0;
  }
  if (((pos != 0) && (ibuf[pos] == 10)) && (ibuf[pos - 1] == 13)) 
    ibuf[pos - 1] = 0;
  ibuf[pos] = 0;
   *buf = (ibuf + spos);
  pos++;
   *position = pos;
  return 1;
}

static void load_conf()
{
/* Corrections to change "...", "(c)", "(r)", and "(tm)" to their
	 * Unicode character equivalents were not added here even though
	 * they existed in the source list(s). I think these corrections
	 * would be more trouble than they're worth.
	 */
  const char *const defaultconf = "BAD abbout\nGOOD about\nBAD abotu\nGOOD about\nBAD abouta\nGOOD about a\nBAD aboutit\nGOOD about it\nBAD aboutthe\nGOOD about the\nBAD abscence\nGOOD absence\nBAD accesories\nGOOD accessories\nBAD accidant\nGOOD accident\nBAD accomodate\nGOOD accommodate\nBAD accordingto\nGOOD according to\nBAD accross\nGOOD across\nBAD acheive\nGOOD achieve\nBAD acheived\nGOOD achieved\nBAD acheiving\nGOOD achieving\nBAD acn\nGOOD can\nBAD acommodate\nGOOD accommodate\nBAD acomodate\nGOOD accommodate\nBAD actualyl\nGOOD actually\nBAD additinal\nGOOD additional\nBAD addtional\nGOOD additional\nBAD adequit\nGOOD adequate\nBAD adequite\nGOOD adequate\nBAD adn\nGOOD and\nBAD advanage\nGOOD advantage\nBAD affraid\nGOOD afraid\nBAD afterthe\nGOOD after the\nCOMPLETE 0\nBAD againstt he \nGOOD against the \nBAD aganist\nGOOD against\nBAD aggresive\nGOOD aggressive\nBAD agian\nGOOD again\nBAD agreemeent\nGOOD agreement\nBAD agreemeents\nGOOD agreements\nBAD agreemnet\nGOOD agreement\nBAD agreemnets\nGOOD agreements\nBAD agressive\nGOOD aggressive\nBAD agressiveness\nGOOD aggressiveness\nBAD ahd\nGOOD had\nBAD ahold\nGOOD a hold\nBAD ahppen\nGOOD happen\nBAD ahve\nGOOD have\nBAD allready\nGOOD already\nBAD allwasy\nGOOD always\nBAD allwyas\nGOOD always\nBAD almots\nGOOD almost\nBAD almsot\nGOOD almost\nBAD alomst\nGOOD almost\nBAD alot\nGOOD a lot\nBAD alraedy\nGOOD already\nBAD alreayd\nGOOD already\nBAD alreday\nGOOD already\nBAD alwasy\nGOOD always\nBAD alwats\nGOOD always\nBAD alway\nGOOD always\nBAD alwyas\nGOOD always\nBAD amde\nGOOD made\nBAD Ameria\nGOOD America\nBAD amke\nGOOD make\nBAD amkes\nGOOD makes\nBAD anbd\nGOOD and\nBAD andone\nGOOD and one\nBAD andteh\nGOOD and the\nBAD andthe\nGOOD and the\nCOMPLETE 0\nBAD andt he \nGOOD and the \nBAD anothe\nGOOD another\nBAD anual\nGOOD annual\nBAD any1\nGOOD anyone\nBAD apparant\nGOOD apparent\nBAD apparrent\nGOOD apparent\nBAD appearence\nGOOD appearance\nBAD appeares\nGOOD appears\nBAD applicaiton\nGOOD application\nBAD applicaitons\nGOOD applications\nBAD applyed\nGOOD applied\nBAD appointiment\nGOOD appointment\nBAD approrpiate\nGOOD appropriate\nBAD approrpriate\nGOOD appropriate\nBAD aquisition\nGOOD acquisition\nBAD aquisitions\nGOOD acquisitions\nBAD arent\nGOOD aren\'t\nCOMPLETE 0\nBAD aren;t \nGOOD aren\'t \nBAD arguement\nGOOD argument\nBAD arguements\nGOOD arguments\nCOMPLETE 0\nBAD arn\'t \nGOOD aren\'t \nBAD arond\nGOOD around\nBAD artical\nGOOD article\nBAD articel\nGOOD article\nBAD asdvertising\nGOOD advertising\nCOMPLETE 0\nBAD askt he \nGOOD ask the \nBAD assistent\nGOOD assistant\nBAD asthe\nGOOD as the\nBAD atention\nGOOD attention\nBAD atmospher\nGOOD atmosphere\nBAD attentioin\nGOOD attention\nBAD atthe\nGOOD at the\nBAD audeince\nGOOD audience\nBAD audiance\nGOOD audience\nBAD authentification\nGOOD authentication\nBAD availalbe\nGOOD available\nBAD awya\nGOOD away\nBAD aywa\nGOOD away\nBAD b4\nGOOD before\nBAD bakc\nGOOD back\nBAD balence\nGOOD balance\nBAD ballance\nGOOD balance\nBAD baout\nGOOD about\nBAD bcak\nGOOD back\nBAD bcuz\nGOOD because\nBAD beacuse\nGOOD because\nBAD becasue\nGOOD because\nBAD becaus\nGOOD because\nBAD becausea\nGOOD because a\nBAD becauseof\nGOOD because of\nBAD becausethe\nGOOD because the\nBAD becauseyou\nGOOD because you\nCOMPLETE 0\nBAD beckon call\nGOOD beck and call\nBAD becomeing\nGOOD becoming\nBAD becomming\nGOOD becoming\nBAD becuase\nGOOD because\nBAD becuse\nGOOD because\nBAD befoer\nGOOD before\nBAD beggining\nGOOD beginning\nBAD begining\nGOOD beginning\nBAD beginining\nGOOD beginning\nBAD beleiev\nGOOD believe\nBAD beleieve\nGOOD believe\nBAD beleif\nGOOD belief\nBAD beleive\nGOOD believe\nBAD beleived\nGOOD believed\nBAD beleives\nGOOD believes\nBAD belive\nGOOD believe\nBAD belived\nGOOD believed\nBAD belives\nGOOD believes\nBAD benifit\nGOOD benefit\nBAD benifits\nGOOD benefits\nBAD betwen\nGOOD between\nBAD beutiful\nGOOD beautiful\nBAD blase\nGOOD blas\303\251\nBAD boxs\nGOOD boxes\nBAD brodcast\nGOOD broadcast\nBAD butthe\nGOOD but the\nBAD bve\nGOOD be\nCOMPLETE 0\nBAD byt he \nGOOD by the \nBAD cafe\nGOOD caf\303\251\nBAD caharcter\nGOOD character\nBAD calcullated\nGOOD calculated\nBAD calulated\nGOOD calculated\nBAD candidtae\nGOOD candidate\nBAD candidtaes\nGOOD candidates\nCOMPLETE 0\nBAD case and point\nGOOD case in point\nBAD cant\nGOOD can\'t\nCOMPLETE 0\nBAD can;t \nGOOD can\'t \nCOMPLETE 0\nBAD can\'t of been\nGOOD can\'t have been\nBAD catagory\nGOOD category\nBAD categiory\nGOOD category\nBAD certian\nGOOD certain\nBAD challange\nGOOD challenge\nBAD challanges\nGOOD challenges\nBAD chaneg\nGOOD change\nBAD chanegs\nGOOD changes\nBAD changable\nGOOD changeable\nBAD changeing\nGOOD changing\nBAD changng\nGOOD changing\nBAD charachter\nGOOD character\nBAD charachters\nGOOD characters\nBAD charactor\nGOOD character\nBAD charecter\nGOOD character\nBAD charector\nGOOD character\nBAD cheif\nGOOD chief\nBAD chekc\nGOOD check\nBAD chnage\nGOOD change\nBAD cieling\nGOOD ceiling\nBAD circut\nGOOD circuit\nBAD claer\nGOOD clear\nBAD claered\nGOOD cleared\nBAD claerly\nGOOD clearly\nBAD cliant\nGOOD client\nBAD cliche\nGOOD clich\303\251\nBAD cna\nGOOD can\nBAD colection\nGOOD collection\nBAD comanies\nGOOD companies\nBAD comany\nGOOD company\nBAD comapnies\nGOOD companies\nBAD comapny\nGOOD company\nBAD combintation\nGOOD combination\nBAD comited\nGOOD committed\nBAD comittee\nGOOD committee\nBAD commadn\nGOOD command\nBAD comming\nGOOD coming\nBAD commitee\nGOOD committee\nBAD committe\nGOOD committee\nBAD committment\nGOOD commitment\nBAD committments\nGOOD commitments\nBAD committy\nGOOD committee\nBAD comntain\nGOOD contain\nBAD comntains\nGOOD contains\nBAD compair\nGOOD compare\nCOMPLETE 0\nBAD company;s \nGOOD company\'s \nBAD competetive\nGOOD competitive\nBAD compleated\nGOOD completed\nBAD compleatly\nGOOD completely\nBAD compleatness\nGOOD completeness\nBAD completly\nGOOD completely\nBAD completness\nGOOD completeness\nBAD composate\nGOOD composite\nBAD comtain\nGOOD contain\nBAD comtains\nGOOD contains\nBAD comunicate\nGOOD communicate\nBAD comunity\nGOOD community\nBAD condolances\nGOOD condolences\nBAD conected\nGOOD connected\nBAD conferance\nGOOD conference\nBAD confirmmation\nGOOD confirmation\nBAD congradulations\nGOOD congratulations\nBAD considerit\nGOOD considerate\nBAD considerite\nGOOD considerate\nBAD consonent\nGOOD consonant\nBAD conspiricy\nGOOD conspiracy\nBAD consultent\nGOOD consultant\nBAD convertable\nGOOD convertible\nBAD cooparate\nGOOD cooperate\nBAD cooporate\nGOOD cooperate\nBAD corproation\nGOOD corporation\nBAD corproations\nGOOD corporations\nBAD corruptable\nGOOD corruptible\nBAD cotten\nGOOD cotton\nBAD coudl\nGOOD could\nCOMPLETE 0\nBAD coudln\'t \nGOOD couldn\'t \nCOMPLETE 0\nBAD coudn\'t \nGOOD couldn\'t \nBAD couldnt\nGOOD couldn\'t\nCOMPLETE 0\nBAD couldn;t \nGOOD couldn\'t \nCOMPLETE 0\nBAD could of been\nGOOD could have been\nCOMPLETE 0\nBAD could of had\nGOOD could have had\nBAD couldthe\nGOOD could the\nBAD couldve\nGOOD could\'ve\nBAD cpoy\nGOOD copy\nBAD creme\nGOOD cr\303\250me\nBAD ctaegory\nGOOD category\nBAD cu\nGOOD see you\nBAD cusotmer\nGOOD customer\nBAD cusotmers\nGOOD customers\nBAD cutsomer\nGOOD customer\nBAD cutsomers\nGOOD customer\nBAD cuz\nGOOD because\nBAD cxan\nGOOD can\nBAD danceing\nGOOD dancing\nBAD dcument\nGOOD document\nBAD deatils\nGOOD details\nBAD decison\nGOOD decision\nBAD decisons\nGOOD decisions\nBAD decor\nGOOD d\303\251cor\nBAD defendent\nGOOD defendant\nBAD definately\nGOOD definitely\nCOMPLETE 0\nBAD deja vu\nGOOD d\303\251j\303\240 vu\nBAD deptartment\nGOOD department\nBAD desicion\nGOOD decision\nBAD desicions\nGOOD decisions\nBAD desision\nGOOD decision\nBAD desisions\nGOOD decisions\nBAD detente\nGOOD d\303\251tente\nBAD develeoprs\nGOOD developers\nBAD devellop\nGOOD develop\nBAD develloped\nGOOD developed\nBAD develloper\nGOOD developer\nBAD devellopers\nGOOD developers\nBAD develloping\nGOOD developing\nBAD devellopment\nGOOD development\nBAD devellopments\nGOOD developments\nBAD devellops\nGOOD develop\nBAD develope\nGOOD develop\nBAD developement\nGOOD development\nBAD developements\nGOOD developments\nBAD developor\nGOOD developer\nBAD developors\nGOOD developers\nBAD develpment\nGOOD development\nBAD diaplay\nGOOD display\nBAD didint\nGOOD didn\'t\nBAD didnot\nGOOD did not\nBAD didnt\nGOOD didn\'t\nCOMPLETE 0\nBAD didn;t \nGOOD didn\'t \nBAD difefrent\nGOOD different\nBAD diferences\nGOOD differences\nBAD differance\nGOOD difference\nBAD differances\nGOOD differences\nBAD differant\nGOOD different\nBAD differemt\nGOOD different\nBAD differnt\nGOOD different\nBAD diffrent\nGOOD different\nBAD directer\nGOOD director\nBAD directers\nGOOD directors\nBAD directiosn\nGOOD direction\nBAD disatisfied\nGOOD dissatisfied\nBAD discoverd\nGOOD discovered\nBAD disign\nGOOD design\nBAD dispaly\nGOOD display\nBAD dissonent\nGOOD dissonant\nBAD distribusion\nGOOD distribution\nBAD divsion\nGOOD division\nBAD docuement\nGOOD documents\nBAD docuemnt\nGOOD document\nBAD documetn\nGOOD document\nBAD documnet\nGOOD document\nBAD documnets\nGOOD documents\nCOMPLETE 0\nBAD doens\'t \nGOOD doesn\'t \nBAD doese\nGOOD does\nCOMPLETE 0\nBAD doe snot \nGOOD does not \nBAD doesnt\nGOOD doesn\'t\nCOMPLETE 0\nBAD doesn;t \nGOOD doesn\'t \nBAD doign\nGOOD doing\nBAD doimg\nGOOD doing\nBAD doind\nGOOD doing\nBAD dollers\nGOOD dollars\nBAD donig\nGOOD doing\nBAD donno\nGOOD don\'t know\nBAD dont\nGOOD don\'t\nCOMPLETE 0\nBAD do\'nt \nGOOD don\'t \nCOMPLETE 0\nBAD don;t \nGOOD don\'t \nCOMPLETE 0\nBAD don\'t no \nGOOD don\'t know \nCOMPLETE 0\nBAD dosn\'t \nGOOD doesn\'t \nBAD driveing\nGOOD driving\nBAD drnik\nGOOD drink\nBAD dunno\nGOOD don\'t know\nBAD eclair\nGOOD \303\251clair\nBAD efel\nGOOD feel\nBAD effecient\nGOOD efficient\nBAD efort\nGOOD effort\nBAD eforts\nGOOD efforts\nBAD ehr\nGOOD her\nBAD eligable\nGOOD eligible\nBAD embarass\nGOOD embarrass\nBAD emigre\nGOOD \303\251migr\303\251\nBAD enought\nGOOD enough\nBAD entree\nGOOD entr\303\251e\nBAD enuf\nGOOD enough\nBAD equippment\nGOOD equipment\nBAD equivalant\nGOOD equivalent\nBAD esle\nGOOD else\nBAD especally\nGOOD especially\nBAD especialyl\nGOOD especially\nBAD espesially\nGOOD especially\nBAD essense\nGOOD essence\nBAD excellance\nGOOD excellence\nBAD excellant\nGOOD excellent\nBAD excercise\nGOOD exercise\nBAD exchagne\nGOOD exchange\nBAD exchagnes\nGOOD exchanges\nBAD excitment\nGOOD excitement\nBAD exhcange\nGOOD exchange\nBAD exhcanges\nGOOD exchanges\nBAD experiance\nGOOD experience\nBAD experienc\nGOOD experience\nBAD exprience\nGOOD experience\nBAD exprienced\nGOOD experienced\nBAD eyt\nGOOD yet\nBAD facade\nGOOD fa\303\247ade\nBAD faeture\nGOOD feature\nBAD faetures\nGOOD feature\nBAD familair\nGOOD familiar\nBAD familar\nGOOD familiar\nBAD familliar\nGOOD familiar\nBAD fammiliar\nGOOD familiar\nBAD feild\nGOOD field\nBAD feilds\nGOOD fields\nBAD fianlly\nGOOD finally\nBAD fidn\nGOOD find\nBAD finalyl\nGOOD finally\nBAD firends\nGOOD friends\nBAD firts\nGOOD first\nBAD follwo\nGOOD follow\nBAD follwoing\nGOOD following\nBAD fora\nGOOD for a\nCOMPLETE 0\nBAD for all intensive purposes\nfor all intents and purposes\nBAD foriegn\nGOOD foreign\nBAD forthe\nGOOD for the\nBAD forwrd\nGOOD forward\nBAD forwrds\nGOOD forwards\nBAD foudn\nGOOD found\nBAD foward\nGOOD forward\nBAD fowards\nGOOD forwards\nBAD freind\nGOOD friend\nBAD freindly\nGOOD friendly\nBAD freinds\nGOOD friends\nBAD friday\nGOOD Friday\nBAD frmo\nGOOD from\nBAD fromthe\nGOOD from the\nCOMPLETE 0\nBAD fromt he \nGOOD from the \nBAD furneral\nGOOD funeral\nBAD fwe\nGOOD few\nBAD garantee\nGOOD guarantee\nBAD gaurd\nGOOD guard\nBAD gemeral\nGOOD general\nBAD gerat\nGOOD great\nBAD geting\nGOOD getting\nBAD gettin\nGOOD getting\nBAD gievn\nGOOD given\nBAD giveing\nGOOD giving\nBAD gloabl\nGOOD global\nBAD goign\nGOOD going\nBAD gonig\nGOOD going\nBAD govenment\nGOOD government\nBAD goverment\nGOOD government\nBAD gruop\nGOOD group\nBAD gruops\nGOOD groups\nBAD grwo\nGOOD grow\nBAD guidlines\nGOOD guidelines\nBAD hadbeen\nGOOD had been\nBAD hadnt\nGOOD hadn\'t\nCOMPLETE 0\nBAD hadn;t \nGOOD hadn\'t \nBAD haev\nGOOD have\nBAD hapen\nGOOD happen\nBAD hapened\nGOOD happened\nBAD hapening\nGOOD happening\nBAD hapens\nGOOD happens\nBAD happend\nGOOD happened\nBAD hasbeen\nGOOD has been\nBAD hasnt\nGOOD hasn\'t\nCOMPLETE 0\nBAD hasn;t \nGOOD hasn\'t \nBAD havebeen\nGOOD have been\nBAD haveing\nGOOD having\nBAD havent\nGOOD haven\'t\nCOMPLETE 0\nBAD haven;t \nGOOD haven\'t \nBAD hda\nGOOD had\nBAD hearign\nGOOD hearing\nCOMPLETE 0\nBAD he;d \nGOOD he\'d \nBAD heirarchy\nGOOD hierarchy\nBAD hel\nGOOD he\'ll\nCOMPLETE 0\nBAD he;ll \nGOOD he\'ll \nBAD helpfull\nGOOD helpful\nBAD herat\nGOOD heart\nBAD heres\nGOOD here\'s\nCOMPLETE 0\nBAD here;s \nGOOD here\'s \nBAD hes\nGOOD he\'s\nCOMPLETE 0\nBAD he;s \nGOOD he\'s \nBAD hesaid\nGOOD he said\nBAD hewas\nGOOD he was\nBAD hge\nGOOD he\nBAD hismelf\nGOOD himself\nBAD hlep\nGOOD help\nBAD hott\nGOOD hot\nBAD hows\nGOOD how\'s\nBAD hsa\nGOOD has\nBAD hse\nGOOD she\nBAD hsi\nGOOD his\nBAD hte\nGOOD the\nBAD htere\nGOOD there\nBAD htese\nGOOD these\nBAD htey\nGOOD they\nBAD hting\nGOOD thing\nBAD htink\nGOOD think\nBAD htis\nGOOD this\nCOMPLETE 0\nBAD htp:\nGOOD http:\nCOMPLETE 0\nBAD http:\\\\nGOOD http://\nBAD httpL\nGOOD http:\nBAD hvae\nGOOD have\nBAD hvaing\nGOOD having\nBAD hwich\nGOOD which\nBAD i\nGOOD I\nCOMPLETE 0\nBAD i c \nGOOD I see \nCOMPLETE 0\nBAD i;d \nGOOD I\'d \nCOMPLETE 0\nBAD i\'d \nGOOD I\'d \nCOMPLETE 0\nBAD I;d \nGOOD I\'d \nBAD idae\nGOOD idea\nBAD idaes\nGOOD ideas\nBAD identofy\nGOOD identify\nBAD ihs\nGOOD his\nBAD iits the\nGOOD it\'s the\nCOMPLETE 0\nBAD i\'ll \nGOOD I\'ll \nCOMPLETE 0\nBAD I;ll \nGOOD I\'ll \nCOMPLETE 0\nBAD i;m \nGOOD I\'m \nCOMPLETE 0\nBAD i\'m \nGOOD I\'m \nCOMPLETE 0\nBAD I\"m \nGOOD I\'m \nBAD imediate\nGOOD immediate\nBAD imediatly\nGOOD immediately\nBAD immediatly\nGOOD immediately\nBAD importent\nGOOD important\nBAD importnat\nGOOD important\nBAD impossable\nGOOD impossible\nBAD improvemnt\nGOOD improvement\nBAD improvment\nGOOD improvement\nBAD includ\nGOOD include\nBAD indecate\nGOOD indicate\nBAD indenpendence\nGOOD independence\nBAD indenpendent\nGOOD independent\nBAD indepedent\nGOOD independent\nBAD independance\nGOOD independence\nBAD independant\nGOOD independent\nBAD influance\nGOOD influence\nBAD infomation\nGOOD information\nBAD informatoin\nGOOD information\nBAD inital\nGOOD initial\nBAD instaleld\nGOOD installed\nBAD insted\nGOOD instead\nBAD insurence\nGOOD insurance\nBAD inteh\nGOOD in the\nBAD interum\nGOOD interim\nBAD inthe\nGOOD in the\nCOMPLETE 0\nBAD int he \nGOOD in the \nBAD inturn\nGOOD intern\nBAD inwhich\nGOOD in which\nCOMPLETE 0\nBAD i snot \nGOOD is not \nBAD isnt\nGOOD isn\'t\nCOMPLETE 0\nBAD isn;t \nGOOD isn\'t \nBAD isthe\nGOOD is the\nBAD itd\nGOOD it\'d\nCOMPLETE 0\nBAD it;d \nGOOD it\'d \nBAD itis\nGOOD it is\nBAD ititial\nGOOD initial\nBAD itll\nGOOD it\'ll\nCOMPLETE 0\nBAD it;ll \nGOOD it\'ll \nBAD itnerest\nGOOD interest\nBAD itnerested\nGOOD interested\nBAD itneresting\nGOOD interesting\nBAD itnerests\nGOOD interests\nCOMPLETE 0\nBAD it;s \nGOOD it\'s \nBAD itsa\nGOOD it\'s a\nCOMPLETE 0\nBAD  its a \nGOOD  it\'s a \nCOMPLETE 0\nBAD  it snot \nGOOD  it\'s not \nCOMPLETE 0\nBAD  it\' snot \nGOOD  it\'s not \nCOMPLETE 0\nBAD  its the \nGOOD  it\'s the \nBAD itwas\nGOOD it was\nBAD ive\nGOOD I\'ve\nCOMPLETE 0\nBAD i;ve \nGOOD I\'ve \nCOMPLETE 0\nBAD i\'ve \nGOOD I\'ve \nBAD iwll\nGOOD will\nBAD iwth\nGOOD with\nBAD jsut\nGOOD just\nBAD jugment\nGOOD judgment\nBAD kno\nGOOD know\nBAD knowldge\nGOOD knowledge\nBAD knowlege\nGOOD knowledge\nBAD knwo\nGOOD know\nBAD knwon\nGOOD known\nBAD knwos\nGOOD knows\nBAD konw\nGOOD know\nBAD konwn\nGOOD known\nBAD konws\nGOOD knows\nBAD labratory\nGOOD laboratory\nBAD labtop\nGOOD laptop\nBAD lastyear\nGOOD last year\nBAD laterz\nGOOD later\nBAD learnign\nGOOD learning\nBAD lenght\nGOOD length\nCOMPLETE 0\nBAD let;s \nGOOD let\'s \nCOMPLETE 0\nBAD let\'s him \nGOOD lets him \nCOMPLETE 0\nBAD let\'s it \nGOOD lets it \nBAD levle\nGOOD level\nBAD libary\nGOOD library\nBAD librarry\nGOOD library\nBAD librery\nGOOD library\nBAD liek\nGOOD like\nBAD liekd\nGOOD liked\nBAD lieutenent\nGOOD lieutenant\nBAD liev\nGOOD live\nBAD likly\nGOOD likely\nBAD lisense\nGOOD license\nBAD littel\nGOOD little\nBAD litttle\nGOOD little\nBAD liuke\nGOOD like\nBAD liveing\nGOOD living\nBAD loev\nGOOD love\nBAD lonly\nGOOD lonely\nBAD lookign\nGOOD looking\nBAD m\nGOOD am\nBAD maintainence\nGOOD maintenance\nBAD maintenence\nGOOD maintenance\nBAD makeing\nGOOD making\nBAD managment\nGOOD management\nBAD mantain\nGOOD maintain\nBAD marraige\nGOOD marriage\nCOMPLETE 0\nBAD may of been\nGOOD may have been\nCOMPLETE 0\nBAD may of had\nGOOD may have had\nBAD memeber\nGOOD member\nBAD merchent\nGOOD merchant\nBAD mesage\nGOOD message\nBAD mesages\nGOOD messages\nCOMPLETE 0\nBAD might of been\nGOOD might have been\nCOMPLETE 0\nBAD might of had\nGOOD might have had\nBAD mispell\nGOOD misspell\nBAD mispelling\nGOOD misspelling\nBAD mispellings\nGOOD misspellings\nBAD mkae\nGOOD make\nBAD mkaes\nGOOD makes\nBAD mkaing\nGOOD making\nBAD moeny\nGOOD money\nBAD monday\nGOOD Monday\nBAD morgage\nGOOD mortgage\nBAD mroe\nGOOD more\nCOMPLETE 0\nBAD must of been\nGOOD must have been\nCOMPLETE 0\nBAD must of had\nGOOD must have had\nCOMPLETE 0\nBAD mute point\nGOOD moot point\nBAD mysefl\nGOOD myself\nBAD myu\nGOOD my\nBAD naive\nGOOD na\303\257ve\nBAD ne1\nGOOD anyone\nBAD neway\nGOOD anyway\nBAD neways\nGOOD anyways\nBAD necassarily\nGOOD necessarily\nBAD necassary\nGOOD necessary\nBAD neccessarily\nGOOD necessarily\nBAD neccessary\nGOOD necessary\nBAD necesarily\nGOOD necessarily\nBAD necesary\nGOOD necessary\nBAD negotiaing\nGOOD negotiating\nBAD nkow\nGOOD know\nBAD nothign\nGOOD nothing\nBAD nto\nGOOD not\nBAD nver\nGOOD never\nBAD nwe\nGOOD new\nBAD nwo\nGOOD now\nBAD obediant\nGOOD obedient\nBAD ocasion\nGOOD occasion\nBAD occassion\nGOOD occasion\nBAD occurance\nGOOD occurrence\nBAD occured\nGOOD occurred\nBAD occurence\nGOOD occurrence\nBAD occurrance\nGOOD occurrence\nBAD oclock\nGOOD o\'clock\nBAD oculd\nGOOD could\nBAD ocur\nGOOD occur\nBAD oeprator\nGOOD operator\nBAD ofits\nGOOD of its\nBAD ofthe\nGOOD of the\nBAD oft he\nGOOD of the\nBAD oging\nGOOD going\nBAD ohter\nGOOD other\nBAD omre\nGOOD more\nBAD oneof\nGOOD one of\nBAD onepoint\nGOOD one point\nBAD onthe\nGOOD on the\nCOMPLETE 0\nBAD ont he \nGOOD on the \nBAD onyl\nGOOD only\nBAD oppasite\nGOOD opposite\nBAD opperation\nGOOD operation\nBAD oppertunity\nGOOD opportunity\nBAD opposate\nGOOD opposite\nBAD opposible\nGOOD opposable\nBAD opposit\nGOOD opposite\nBAD oppotunities\nGOOD opportunities\nBAD oppotunity\nGOOD opportunity\nBAD orginization\nGOOD organization\nBAD orginized\nGOOD organized\nBAD otehr\nGOOD other\nBAD otu\nGOOD out\nBAD outof\nGOOD out of\nBAD overthe\nGOOD over the\nBAD owrk\nGOOD work\nBAD owuld\nGOOD would\nBAD oxident\nGOOD oxidant\nBAD papaer\nGOOD paper\nBAD passe\nGOOD pass\303\251\nBAD parliment\nGOOD parliament\nBAD partof\nGOOD part of\nBAD paymetn\nGOOD payment\nBAD paymetns\nGOOD payments\nBAD pciture\nGOOD picture\nBAD peice\nGOOD piece\nBAD peices\nGOOD pieces\nBAD peolpe\nGOOD people\nBAD peopel\nGOOD people\nBAD percentof\nGOOD percent of\nBAD percentto\nGOOD percent to\nBAD performence\nGOOD performance\nBAD perhasp\nGOOD perhaps\nBAD perhpas\nGOOD perhaps\nBAD permanant\nGOOD permanent\nBAD perminent\nGOOD permanent\nBAD personalyl\nGOOD personally\nBAD pleasent\nGOOD pleasant\nBAD pls\nGOOD please\nBAD plz\nGOOD please\nBAD poeple\nGOOD people\nBAD porblem\nGOOD problem\nBAD porblems\nGOOD problems\nBAD porvide\nGOOD provide\nBAD possable\nGOOD possible\nBAD postition\nGOOD position\nBAD potatoe\nGOOD potato\nBAD potatos\nGOOD potatoes\nBAD potentialy\nGOOD potentially\nBAD ppl\nGOOD people\nBAD pregnent\nGOOD pregnant\nBAD presance\nGOOD presence\nBAD primative\nGOOD primitive\nBAD probally\nGOOD probably\nBAD probelm\nGOOD problem\nBAD probelms\nGOOD problems\nBAD probly\nGOOD probably\nBAD prolly\nGOOD probably\nBAD proly\nGOOD probably\nBAD prominant\nGOOD prominent\nBAD proposterous\nGOOD preposterous\nBAD protege\nGOOD prot\303\251g\303\251\nBAD protoge\nGOOD prot\303\251g\303\251\nBAD psoition\nGOOD position\nBAD ptogress\nGOOD progress\nBAD pursuade\nGOOD persuade\nBAD puting\nGOOD putting\nBAD pwoer\nGOOD power\nBAD quater\nGOOD quarter\nBAD quaters\nGOOD quarters\nBAD quesion\nGOOD question\nBAD quesions\nGOOD questions\nBAD questioms\nGOOD questions\nBAD questiosn\nGOOD questions\nBAD questoin\nGOOD question\nBAD quetion\nGOOD question\nBAD quetions\nGOOD questions\nBAD r\nGOOD are\nBAD raeson\nGOOD reason\nBAD realyl\nGOOD really\nBAD reccomend\nGOOD recommend\nBAD reccommend\nGOOD recommend\nBAD receieve\nGOOD receive\nBAD recieve\nGOOD receive\nBAD recieved\nGOOD received\nBAD recieving\nGOOD receiving\nBAD recomend\nGOOD recommend\nBAD recomendation\nGOOD recommendation\nBAD recomendations\nGOOD recommendations\nBAD recomended\nGOOD recommended\nBAD reconize\nGOOD recognize\nBAD recrod\nGOOD record\nBAD rediculous\nGOOD ridiculous\nBAD rediculus\nGOOD ridiculous\nBAD reguard\nGOOD regard\nBAD religous\nGOOD religious\nBAD reluctent\nGOOD reluctant\nBAD remeber\nGOOD remember\nBAD reommend\nGOOD recommend\nBAD representativs\nGOOD representatives\nBAD representives\nGOOD representatives\nBAD represetned\nGOOD represented\nBAD represnt\nGOOD represent\nBAD reserach\nGOOD research\nBAD resollution\nGOOD resolution\nBAD resorces\nGOOD resources\nBAD respomd\nGOOD respond\nBAD respomse\nGOOD response\nBAD responce\nGOOD response\nBAD responsability\nGOOD responsibility\nBAD responsable\nGOOD responsible\nBAD responsibile\nGOOD responsible\nBAD responsiblity\nGOOD responsibility\nBAD restaraunt\nGOOD restaurant\nBAD restuarant\nGOOD restaurant\nBAD reult\nGOOD result\nBAD reveiw\nGOOD review\nBAD reveiwing\nGOOD reviewing\nBAD rumers\nGOOD rumors\nBAD rwite\nGOOD write\nBAD rythm\nGOOD rhythm\nBAD saidhe\nGOOD said he\nBAD saidit\nGOOD said it\nBAD saidthat\nGOOD said that\nBAD saidthe\nGOOD said the\nCOMPLETE 0\nBAD saidt he \nGOOD said the \nBAD sandwhich\nGOOD sandwich\nBAD sandwitch\nGOOD sandwich\nBAD saturday\nGOOD Saturday\nBAD scedule\nGOOD schedule\nBAD sceduled\nGOOD scheduled\nBAD seance\nGOOD s\303\251ance\nBAD secratary\nGOOD secretary\nBAD sectino\nGOOD section\nBAD seh\nGOOD she\nBAD selectoin\nGOOD selection\nBAD sence\nGOOD sense\nBAD sentance\nGOOD sentence\nBAD separeate\nGOOD separate\nBAD seperate\nGOOD separate\nBAD sercumstances\nGOOD circumstances\nBAD shcool\nGOOD school\nCOMPLETE 0\nBAD she;d \nGOOD she\'d \nCOMPLETE 0\nBAD she;ll \nGOOD she\'ll \nBAD shes\nGOOD she\'s\nCOMPLETE 0\nBAD she;s \nGOOD she\'s \nBAD shesaid\nGOOD she said\nBAD shineing\nGOOD shining\nBAD shiped\nGOOD shipped\nBAD shoudl\nGOOD should\nCOMPLETE 0\nBAD shoudln\'t \nGOOD shouldn\'t \nBAD shouldent\nGOOD shouldn\'t\nBAD shouldnt\nGOOD shouldn\'t\nCOMPLETE 0\nBAD shouldn;t \nGOOD shouldn\'t \nCOMPLETE 0\nBAD should of been\nGOOD should have been\nCOMPLETE 0\nBAD should of had\nGOOD should have had\nBAD shouldve\nGOOD should\'ve\nBAD showinf\nGOOD showing\nBAD signifacnt\nGOOD significant\nBAD simalar\nGOOD similar\nBAD similiar\nGOOD similar\nBAD simpyl\nGOOD simply\nBAD sincerly\nGOOD sincerely\nBAD sitll\nGOOD still\nBAD smae\nGOOD same\nBAD smoe\nGOOD some\nBAD soem\nGOOD some\nBAD sohw\nGOOD show\nBAD soical\nGOOD social\nBAD some1\nGOOD someone\nBAD somethign\nGOOD something\nBAD someting\nGOOD something\nBAD somewaht\nGOOD somewhat\nBAD somthing\nGOOD something\nBAD somtimes\nGOOD sometimes\nCOMPLETE 0\nBAD sot hat \nGOOD so that \nBAD soudn\nGOOD sound\nBAD soudns\nGOOD sounds\nBAD speach\nGOOD speech\nBAD specificaly\nGOOD specifically\nBAD specificalyl\nGOOD specifically\nBAD spelt\nGOOD spelled\nBAD sry\nGOOD sorry\nCOMPLETE 0\nBAD state of the ark\nGOOD state of the art\nBAD statment\nGOOD statement\nBAD statments\nGOOD statements\nBAD stnad\nGOOD stand\nBAD stopry\nGOOD story\nBAD stoyr\nGOOD story\nBAD stpo\nGOOD stop\nBAD strentgh\nGOOD strength\nBAD stroy\nGOOD story\nBAD struggel\nGOOD struggle\nBAD strugle\nGOOD struggle\nBAD studnet\nGOOD student\nBAD successfull\nGOOD successful\nBAD successfuly\nGOOD successfully\nBAD successfulyl\nGOOD successfully\nBAD sucess\nGOOD success\nBAD sucessfull\nGOOD successful\nBAD sufficiant\nGOOD sufficient\nBAD sum1\nGOOD someone\nBAD sunday\nGOOD Sunday\nBAD suposed\nGOOD supposed\nBAD supposably\nGOOD supposedly\nBAD suppossed\nGOOD supposed\nBAD suprise\nGOOD surprise\nBAD suprised\nGOOD surprised\nBAD sux\nGOOD sucks\nBAD swiming\nGOOD swimming\nBAD tahn\nGOOD than\nBAD taht\nGOOD that\nCOMPLETE 0\nBAD take it for granite\nGOOD take it for granted\nCOMPLETE 0\nBAD taken for granite\nGOOD taken for granted\nBAD talekd\nGOOD talked\nBAD talkign\nGOOD talking\nBAD tath\nGOOD that\nBAD tecnical\nGOOD technical\nBAD teh\nGOOD the\nBAD tehy\nGOOD they\nCOMPLETE 0\nBAD tellt he \nGOOD tell the \nBAD termoil\nGOOD turmoil\nBAD tets\nGOOD test\nBAD tghe\nGOOD the\nBAD tghis\nGOOD this\nBAD thansk\nGOOD thanks\nBAD thanx\nGOOD thanks\nBAD thats\nGOOD that\'s\nBAD thatthe\nGOOD that the\nCOMPLETE 0\nBAD thatt he \nGOOD that the \nBAD thecompany\nGOOD the company\nBAD thefirst\nGOOD the first\nBAD thegovernment\nGOOD the government\nCOMPLETE 0\nBAD their are \nGOOD there are \nCOMPLETE 0\nBAD their aren\'t \nGOOD there aren\'t \nCOMPLETE 0\nBAD their is \nGOOD there is \nBAD themself\nGOOD themselves\nBAD themselfs\nGOOD themselves\nBAD thenew\nGOOD the new\nBAD theres\nGOOD there\'s\nCOMPLETE 0\nBAD there\'s is \nGOOD theirs is \nCOMPLETE 0\nBAD there\'s isn\'t \nGOOD theirs isn\'t \nBAD theri\nGOOD their\nBAD thesame\nGOOD the same\nBAD thetwo\nGOOD the two\nBAD theyd\nGOOD they\'d\nCOMPLETE 0\nBAD they;d \nGOOD they\'d \nCOMPLETE 0\nBAD they;l \nGOOD they\'ll \nBAD theyll\nGOOD they\'ll\nCOMPLETE 0\nBAD they;ll \nGOOD they\'ll \nCOMPLETE 0\nBAD they;r \nGOOD they\'re \nCOMPLETE 0\nBAD theyre \nGOOD they\'re \nCOMPLETE 0\nBAD they;re \nGOOD they\'re \nCOMPLETE 0\nBAD they\'re are \nGOOD there are \nCOMPLETE 0\nBAD they\'re is \nGOOD there is \nCOMPLETE 0\nBAD they;v \nGOOD they\'ve \nBAD theyve\nGOOD they\'ve\nCOMPLETE 0\nBAD they;ve \nGOOD they\'ve \nBAD thgat\nGOOD that\nBAD thge\nGOOD the\nBAD thier\nGOOD their \nBAD thigsn\nGOOD things\nBAD thisyear\nGOOD this year\nBAD thme\nGOOD them\nBAD thna\nGOOD than\nBAD thne\nGOOD then\nBAD thnig\nGOOD thing\nBAD thnigs\nGOOD things\nBAD tho\nGOOD though\nBAD threatend\nGOOD threatened\nBAD thsi\nGOOD this\nBAD thsoe\nGOOD those\nBAD thta\nGOOD that\nBAD thursday\nGOOD Thursday\nBAD thx\nGOOD thanks\nBAD tihs\nGOOD this\nBAD timne\nGOOD time\nBAD tiogether\nGOOD together\nBAD tje\nGOOD the\nBAD tjhe\nGOOD the\nBAD tkae\nGOOD take\nBAD tkaes\nGOOD takes\nBAD tkaing\nGOOD taking\nBAD tlaking\nGOOD talking\nBAD tnx\nGOOD thanks\nBAD todya\nGOOD today\nBAD togehter\nGOOD together\nCOMPLETE 0\nBAD toldt he \nGOOD told the \nBAD tomorow\nGOOD tomorrow\nBAD tongiht\nGOOD tonight\nBAD tonihgt\nGOOD tonight\nBAD tonite\nGOOD tonight\nBAD totaly\nGOOD totally\nBAD totalyl\nGOOD totally\nBAD tothe\nGOOD to the\nCOMPLETE 0\nBAD tot he \nGOOD to the \nBAD touche\nGOOD touch\303\251\nBAD towrad\nGOOD toward\nBAD traditionalyl\nGOOD traditionally\nBAD transfered\nGOOD transferred\nBAD truely\nGOOD truly\nBAD truley\nGOOD truly\nBAD tryed\nGOOD tried\nBAD tthe\nGOOD the\nBAD tuesday\nGOOD Tuesday\nBAD tyhat\nGOOD that\nBAD tyhe\nGOOD the\nBAD u\nGOOD you\nBAD udnerstand\nGOOD understand\nBAD understnad\nGOOD understand\nCOMPLETE 0\nBAD undert he \nGOOD under the \nBAD unforseen\nGOOD unforeseen\nBAD UnitedStates\nGOOD United States\nBAD unliek\nGOOD unlike\nBAD unpleasently\nGOOD unpleasantly\nBAD untill\nGOOD until\nBAD untilll\nGOOD until\nBAD ur\nGOOD you are\nBAD useing\nGOOD using\nBAD usualyl\nGOOD usually\nBAD veyr\nGOOD very\nBAD virtualyl\nGOOD virtually\nBAD visavis\nGOOD vis-a-vis\nCOMPLETE 0\nBAD vis-a-vis\nGOOD vis-\303\240-vis\nBAD vrey\nGOOD very\nBAD vulnerible\nGOOD vulnerable\nBAD waht\nGOOD what\nBAD warrent\nGOOD warrant\nCOMPLETE 0\nBAD wa snot \nGOOD was not \nCOMPLETE 0\nBAD wasnt \nGOOD wasn\'t \nCOMPLETE 0\nBAD wasn;t \nGOOD wasn\'t \nBAD wat\nGOOD what\nBAD watn\nGOOD want\nCOMPLETE 0\nBAD we;d \nGOOD we\'d \nBAD wednesday\nGOOD Wednesday\nBAD wehn\nGOOD when\nCOMPLETE 0\nBAD we\'l \nGOOD we\'ll \nCOMPLETE 0\nBAD we;ll \nGOOD we\'ll \nCOMPLETE 0\nBAD we;re \nGOOD we\'re \nBAD werent\nGOOD weren\'t\nCOMPLETE 0\nBAD weren;t \nGOOD weren\'t \nCOMPLETE 0\nBAD wern\'t \nGOOD weren\'t \nBAD werre\nGOOD were\nBAD weve\nGOOD we\'ve\nCOMPLETE 0\nBAD we;ve \nGOOD we\'ve \nBAD whats\nGOOD what\'s\nCOMPLETE 0\nBAD what;s \nGOOD what\'s \nBAD whcih\nGOOD which\nCOMPLETE 0\nBAD whent he \nGOOD when the \nBAD wheres\nGOOD where\'s\nCOMPLETE 0\nBAD where;s \nGOOD where\'s \nBAD wherre\nGOOD where\nBAD whic\nGOOD which\nCOMPLETE 0\nBAD whicht he \nGOOD which the \nBAD whihc\nGOOD which\nBAD wholl\nGOOD who\'ll\nBAD whos\nGOOD who\'s\nCOMPLETE 0\nBAD who;s \nGOOD who\'s \nBAD whove\nGOOD who\'ve\nCOMPLETE 0\nBAD who;ve \nGOOD who\'ve \nBAD whta\nGOOD what\nBAD whys\nGOOD why\'s\nBAD wief\nGOOD wife\nBAD wierd\nGOOD weird\nBAD wihch\nGOOD which\nBAD wiht\nGOOD with\nBAD willbe\nGOOD will be\nCOMPLETE 0\nBAD will of been\nGOOD will have been\nCOMPLETE 0\nBAD will of had\nGOOD will have had\nBAD windoes\nGOOD windows\nBAD witha\nGOOD with a\nBAD withdrawl\nGOOD withdrawal\nBAD withe\nGOOD with\nCOMPLETE 0\nBAD withthe \nGOOD with the \nBAD witht he\nGOOD with the\nBAD wiull\nGOOD will\nBAD wnat\nGOOD want\nBAD wnated\nGOOD wanted\nBAD wnats\nGOOD wants\nBAD woh\nGOOD who\nBAD wohle\nGOOD whole\nBAD wokr\nGOOD work\nBAD wont\nGOOD won\'t\nCOMPLETE 0\nBAD wo\'nt \nGOOD won\'t \nCOMPLETE 0\nBAD won;t \nGOOD won\'t \nBAD woudl\nGOOD would\nCOMPLETE 0\nBAD woudln\'t \nGOOD wouldn\'t \nBAD wouldbe\nGOOD would be\nBAD wouldnt\nGOOD wouldn\'t\nCOMPLETE 0\nBAD wouldn;t \nGOOD wouldn\'t \nCOMPLETE 0\nBAD would of been\nGOOD would have been\nCOMPLETE 0\nBAD would of had\nGOOD would have had\nBAD wouldve\nGOOD would\'ve\nBAD wriet\nGOOD write\nBAD writting\nGOOD writing\nBAD wrod\nGOOD word\nBAD wroet\nGOOD wrote\nBAD wroking\nGOOD working\nBAD wtih\nGOOD with\nBAD wuould\nGOOD would\nBAD wud\nGOOD would\nBAD wut\nGOOD what\nBAD wya\nGOOD way\nBAD y\nGOOD why\nBAD yeh\nGOOD yeah\nBAD yera\nGOOD year\nBAD yeras\nGOOD years\nBAD yersa\nGOOD years\nBAD yoiu\nGOOD you\nBAD youare\nGOOD you are\nBAD youd\nGOOD you\'d\nCOMPLETE 0\nBAD you;d \nGOOD you\'d \nBAD youll\nGOOD you\'ll\nCOMPLETE 0\nBAD your a \nGOOD you\'re a \nCOMPLETE 0\nBAD your an \nGOOD you\'re an \nBAD youre\nGOOD you\'re\nCOMPLETE 0\nBAD you;re \nGOOD you\'re \nCOMPLETE 0\nBAD you\'re own \nGOOD your own \nCOMPLETE 0\nBAD your her \nGOOD you\'re her \nCOMPLETE 0\nBAD your here \nGOOD you\'re here \nCOMPLETE 0\nBAD your his \nGOOD you\'re his \nCOMPLETE 0\nBAD your my \nGOOD you\'re my \nCOMPLETE 0\nBAD your the \nGOOD you\'re the \nCOMPLETE 0\nBAD your their \nGOOD you\'re their \nCOMPLETE 0\nBAD your your \nGOOD you\'re your \nBAD youve\nGOOD you\'ve\nCOMPLETE 0\nBAD you;ve \nGOOD you\'ve \nBAD ytou\nGOOD you\nBAD yuo\nGOOD you\nBAD yuor\nGOOD your\n";
  gchar *buf;
  gchar *ibuf;
  GHashTable *hashes;
  char bad[82UL] = "";
  char good[256UL] = "";
  int pnt = 0;
  gsize size;
  gboolean complete = (!0);
  gboolean case_sensitive = 0;
  buf = g_build_filename(purple_user_dir(),"dict",((void *)((void *)0)));
  g_file_get_contents(buf,&ibuf,&size,0);
  g_free(buf);
  if (!(ibuf != 0)) {
    ibuf = g_strdup(defaultconf);
    size = strlen(defaultconf);
  }
  model = gtk_list_store_new(N_COLUMNS,((GType )(16 << 2)),((GType )(16 << 2)),((GType )(5 << 2)),((GType )(5 << 2)));
  hashes = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,0);
  while(buf_get_line(ibuf,&buf,&pnt,size) != 0){
    if (( *buf) != '#') {
      if (!(g_ascii_strncasecmp(buf,"BAD ",4) != 0)) {
        strncpy(bad,(buf + 4),81);
      }
      else if (!(g_ascii_strncasecmp(buf,"CASE ",5) != 0)) {
        case_sensitive = ((buf[5] == 48)?0 : !0);
      }
      else if (!(g_ascii_strncasecmp(buf,"COMPLETE ",9) != 0)) {
        complete = ((buf[9] == 48)?0 : !0);
      }
      else if (!(g_ascii_strncasecmp(buf,"GOOD ",5) != 0)) {
        strncpy(good,(buf + 5),255);
        if (((( *bad) != 0) && (( *good) != 0)) && (g_hash_table_lookup(hashes,bad) == ((void *)((void *)0)))) {
          GtkTreeIter iter;
/* We don't actually need to store the good string, since this
					 * hash is just being used to eliminate duplicate bad strings.
					 * The value has to be non-NULL so the lookup above will work.
					 */
          g_hash_table_insert(hashes,(g_strdup(bad)),((gpointer )((gpointer )((glong )1))));
          if (!(complete != 0)) 
            case_sensitive = (!0);
          gtk_list_store_append(model,&iter);
          gtk_list_store_set(model,&iter,BAD_COLUMN,bad,GOOD_COLUMN,good,WORD_ONLY_COLUMN,complete,CASE_SENSITIVE_COLUMN,case_sensitive,-1);
        }
        bad[0] = 0;
        complete = (!0);
        case_sensitive = 0;
      }
    }
  }
  g_free(ibuf);
  g_hash_table_destroy(hashes);
  gtk_tree_sortable_set_sort_column_id(((GtkTreeSortable *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_sortable_get_type()))),0,GTK_SORT_ASCENDING);
}
static GtkWidget *tree;
static GtkWidget *bad_entry;
static GtkWidget *good_entry;
static GtkWidget *complete_toggle;
static GtkWidget *case_toggle;
static void save_list();

static void on_edited(GtkCellRendererText *cellrenderertext,gchar *path,gchar *arg2,gpointer data)
{
  GtkTreeIter iter;
  GValue val;
  if (arg2[0] == 0) {
    gdk_beep();
    return ;
  }
  do {
    if (gtk_tree_model_get_iter_from_string(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,path) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(model), &iter, path)");
      return ;
    };
  }while (0);
  val.g_type = 0;
  gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,((gint )((glong )data)),&val);
  if (strcmp(arg2,g_value_get_string((&val))) != 0) {
    gtk_list_store_set(model,&iter,((gint )((glong )data)),arg2,-1);
    save_list();
  }
  g_value_unset(&val);
}

static void word_only_toggled(GtkCellRendererToggle *cellrenderertoggle,gchar *path,gpointer data)
{
  GtkTreeIter iter;
  gboolean enabled;
  do {
    if (gtk_tree_model_get_iter_from_string(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,path) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(model), &iter, path)");
      return ;
    };
  }while (0);
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,WORD_ONLY_COLUMN,&enabled,-1);
  gtk_list_store_set(((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_list_store_get_type()))),&iter,WORD_ONLY_COLUMN,!(enabled != 0),-1);
/* I want to be sure that the above change has happened to the GtkTreeView first. */
  gtk_list_store_set(((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_list_store_get_type()))),&iter,CASE_SENSITIVE_COLUMN,enabled,-1);
  save_list();
}

static void case_sensitive_toggled(GtkCellRendererToggle *cellrenderertoggle,gchar *path,gpointer data)
{
  GtkTreeIter iter;
  gboolean enabled;
  do {
    if (gtk_tree_model_get_iter_from_string(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,path) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(model), &iter, path)");
      return ;
    };
  }while (0);
/* Prevent the case sensitive column from changing on non-whole word replacements.
	 * Ideally, the column would be set insensitive in the word_only_toggled callback. */
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,WORD_ONLY_COLUMN,&enabled,-1);
  if (!(enabled != 0)) 
    return ;
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,CASE_SENSITIVE_COLUMN,&enabled,-1);
  gtk_list_store_set(((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_list_store_get_type()))),&iter,CASE_SENSITIVE_COLUMN,!(enabled != 0),-1);
  save_list();
}

static void list_add_new()
{
  GtkTreeIter iter;
  const char *word = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)bad_entry),gtk_entry_get_type()))));
  gboolean case_sensitive = gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)case_toggle),gtk_toggle_button_get_type()))));
  if (gtk_tree_model_get_iter_first(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter) != 0) {
    char *tmpword = g_utf8_casefold(word,(-1));
    do {
      GValue bad_val;
      gboolean match;
      bad_val.g_type = 0;
      gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,BAD_COLUMN,&bad_val);
      if (case_sensitive != 0) {
        GValue case_sensitive_val;
        case_sensitive_val.g_type = 0;
        gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,CASE_SENSITIVE_COLUMN,&case_sensitive_val);
/* If they're both case-sensitive, then compare directly.
				 * Otherwise, they overlap. */
        if (g_value_get_boolean((&case_sensitive_val)) != 0) {
          match = !(strcmp(g_value_get_string((&bad_val)),word) != 0);
        }
        else {
          char *bad = g_utf8_casefold(g_value_get_string((&bad_val)),(-1));
          match = !(strcmp(bad,tmpword) != 0);
          g_free(bad);
        }
        g_value_unset(&case_sensitive_val);
      }
      else {
        char *bad = g_utf8_casefold(g_value_get_string((&bad_val)),(-1));
        match = !(strcmp(bad,tmpword) != 0);
        g_free(bad);
      }
      if (match != 0) {
        g_value_unset(&bad_val);
        g_free(tmpword);
        purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Duplicate Correction"))),((const char *)(dgettext("pidgin","The specified word already exists in the correction list."))),gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)bad_entry),gtk_entry_get_type())))),0,0);
        return ;
      }
      g_value_unset(&bad_val);
    }while (gtk_tree_model_iter_next(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter) != 0);
    g_free(tmpword);
  }
  gtk_list_store_append(model,&iter);
  gtk_list_store_set(model,&iter,BAD_COLUMN,word,GOOD_COLUMN,gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)good_entry),gtk_entry_get_type())))),WORD_ONLY_COLUMN,gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)complete_toggle),gtk_toggle_button_get_type())))),CASE_SENSITIVE_COLUMN,case_sensitive,-1);
  gtk_editable_delete_text(((GtkEditable *)(g_type_check_instance_cast(((GTypeInstance *)bad_entry),gtk_editable_get_type()))),0,(-1));
  gtk_editable_delete_text(((GtkEditable *)(g_type_check_instance_cast(((GTypeInstance *)good_entry),gtk_editable_get_type()))),0,(-1));
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)complete_toggle),gtk_toggle_button_get_type()))),(!0));
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)case_toggle),gtk_toggle_button_get_type()))),0);
  gtk_widget_grab_focus(bad_entry);
  save_list();
}

static void add_selected_row_to_list(GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter,gpointer data)
{
  GtkTreeRowReference *row_reference;
  GSList **list = (GSList **)data;
  row_reference = gtk_tree_row_reference_new(model,path);
   *list = g_slist_prepend( *list,row_reference);
}

static void remove_row(void *data1,gpointer data2)
{
  GtkTreeRowReference *row_reference;
  GtkTreePath *path;
  GtkTreeIter iter;
  row_reference = ((GtkTreeRowReference *)data1);
  path = gtk_tree_row_reference_get_path(row_reference);
  if (gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,path) != 0) 
    gtk_list_store_remove(model,&iter);
  gtk_tree_path_free(path);
  gtk_tree_row_reference_free(row_reference);
}

static void list_delete()
{
  GtkTreeSelection *sel;
  GSList *list = (GSList *)((void *)0);
  sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type()))));
  gtk_tree_selection_selected_foreach(sel,add_selected_row_to_list,(&list));
  g_slist_foreach(list,remove_row,0);
  g_slist_free(list);
  save_list();
}

static void save_list()
{
  GString *data;
  GtkTreeIter iter;
  data = g_string_new("");
  if (gtk_tree_model_get_iter_first(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter) != 0) {
    do {
      GValue val0;
      GValue val1;
      GValue val2;
      GValue val3;
      val0.g_type = 0;
      val1.g_type = 0;
      val2.g_type = 0;
      val3.g_type = 0;
      gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,BAD_COLUMN,&val0);
      gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,GOOD_COLUMN,&val1);
      gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,WORD_ONLY_COLUMN,&val2);
      gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,CASE_SENSITIVE_COLUMN,&val3);
      g_string_append_printf(data,"COMPLETE %d\nCASE %d\nBAD %s\nGOOD %s\n\n",g_value_get_boolean((&val2)),g_value_get_boolean((&val3)),g_value_get_string((&val0)),g_value_get_string((&val1)));
      g_value_unset(&val0);
      g_value_unset(&val1);
      g_value_unset(&val2);
      g_value_unset(&val3);
    }while (gtk_tree_model_iter_next(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter) != 0);
  }
  purple_util_write_data_to_file("dict",(data -> str),(-1));
  g_string_free(data,(!0));
}

static void on_selection_changed(GtkTreeSelection *sel,gpointer data)
{
  gint num_selected;
  num_selected = gtk_tree_selection_count_selected_rows(sel);
  gtk_widget_set_sensitive(((GtkWidget *)data),(num_selected > 0));
}

static gboolean non_empty(const char *s)
{
  while((( *s) != 0) && ((g_ascii_table[(guchar )( *s)] & G_ASCII_SPACE) != 0))
    s++;
  return ( *s);
}

static void on_entry_changed(GtkEditable *editable,gpointer data)
{
  gtk_widget_set_sensitive(((GtkWidget *)data),((non_empty(gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)bad_entry),gtk_entry_get_type()))))) != 0) && (non_empty(gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)good_entry),gtk_entry_get_type()))))) != 0)));
}
/*
 *  EXPORTED FUNCTIONS
 */

static gboolean plugin_load(PurplePlugin *plugin)
{
  void *conv_handle = purple_conversations_get_handle();
  GList *convs;
  load_conf();
/* Attach to existing conversations */
  for (convs = purple_get_conversations(); convs != ((GList *)((void *)0)); convs = (convs -> next)) {
    spellchk_new_attach(((PurpleConversation *)(convs -> data)));
  }
  purple_signal_connect(conv_handle,"conversation-created",plugin,((PurpleCallback )spellchk_new_attach),0);
  return (!0);
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  GList *convs;
/* Detach from existing conversations */
  for (convs = purple_get_conversations(); convs != ((GList *)((void *)0)); convs = (convs -> next)) {
    PidginConversation *gtkconv = (PidginConversation *)( *((PurpleConversation *)(convs -> data))).ui_data;
    spellchk *spell = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),((GType )(20 << 2))))),"spellchk"));
    g_signal_handlers_disconnect_matched((gtkconv -> entry),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,message_send_cb,spell);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),((GType )(20 << 2))))),"spellchk",0);
  }
  return (!0);
}

static void whole_words_button_toggled(GtkToggleButton *complete_toggle,GtkToggleButton *case_toggle)
{
  gboolean enabled = gtk_toggle_button_get_active(complete_toggle);
  gtk_toggle_button_set_active(case_toggle,!(enabled != 0));
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)case_toggle),gtk_widget_get_type()))),enabled);
}

static GtkWidget *get_config_frame(PurplePlugin *plugin)
{
  GtkWidget *ret;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *button;
  GtkSizeGroup *sg;
  GtkSizeGroup *sg2;
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;
  GtkWidget *vbox2;
  GtkWidget *vbox3;
  ret = gtk_vbox_new(0,18);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_container_get_type()))),12);
  vbox = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Text Replacements"))));
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),4);
  gtk_widget_show(vbox);
  tree = gtk_tree_view_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))));
  gtk_tree_view_set_rules_hint(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type()))),(!0));
  gtk_widget_set_size_request(tree,(-1),200);
  renderer = gtk_cell_renderer_text_new();
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)renderer),((GType )(20 << 2))))),"editable",!0,((void *)((void *)0)));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)renderer),((GType )(20 << 2))))),"edited",((GCallback )on_edited),0,0,((GConnectFlags )0));
  column = gtk_tree_view_column_new_with_attributes(((const char *)(dgettext("pidgin","You type"))),renderer,"text",BAD_COLUMN,((void *)((void *)0)));
  gtk_tree_view_column_set_sizing(column,GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_fixed_width(column,150);
  gtk_tree_view_column_set_resizable(column,(!0));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type()))),column);
  renderer = gtk_cell_renderer_text_new();
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)renderer),((GType )(20 << 2))))),"editable",!0,((void *)((void *)0)));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)renderer),((GType )(20 << 2))))),"edited",((GCallback )on_edited),((gpointer )((gpointer )((glong )1))),0,((GConnectFlags )0));
  column = gtk_tree_view_column_new_with_attributes(((const char *)(dgettext("pidgin","You send"))),renderer,"text",GOOD_COLUMN,((void *)((void *)0)));
  gtk_tree_view_column_set_sizing(column,GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_fixed_width(column,150);
  gtk_tree_view_column_set_resizable(column,(!0));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type()))),column);
  renderer = gtk_cell_renderer_toggle_new();
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)renderer),((GType )(20 << 2))))),"activatable",!0,((void *)((void *)0)));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)renderer),((GType )(20 << 2))))),"toggled",((GCallback )word_only_toggled),0,0,((GConnectFlags )0));
  column = gtk_tree_view_column_new_with_attributes(((const char *)(dgettext("pidgin","Whole words only"))),renderer,"active",WORD_ONLY_COLUMN,((void *)((void *)0)));
  gtk_tree_view_column_set_resizable(column,(!0));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type()))),column);
  renderer = gtk_cell_renderer_toggle_new();
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)renderer),((GType )(20 << 2))))),"activatable",!0,((void *)((void *)0)));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)renderer),((GType )(20 << 2))))),"toggled",((GCallback )case_sensitive_toggled),0,0,((GConnectFlags )0));
  column = gtk_tree_view_column_new_with_attributes(((const char *)(dgettext("pidgin","Case sensitive"))),renderer,"active",CASE_SENSITIVE_COLUMN,((void *)((void *)0)));
  gtk_tree_view_column_set_resizable(column,(!0));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type()))),column);
  gtk_tree_selection_set_mode(gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type())))),GTK_SELECTION_MULTIPLE);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),pidgin_make_scrollable(tree,GTK_POLICY_NEVER,GTK_POLICY_ALWAYS,GTK_SHADOW_IN,-1,-1),(!0),(!0),0);
  gtk_widget_show(tree);
  hbox = gtk_hbutton_box_new();
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  gtk_widget_show(hbox);
  button = gtk_button_new_from_stock("gtk-delete");
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )list_delete),0,0,((GConnectFlags )0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),button,0,0,0);
  gtk_widget_set_sensitive(button,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type())))))),((GType )(20 << 2))))),"changed",((GCallback )on_selection_changed),button,0,((GConnectFlags )0));
  gtk_widget_show(button);
  vbox = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Add a new text replacement"))));
  hbox = gtk_hbox_new(0,6);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,(!0),(!0),0);
  gtk_widget_show(hbox);
  vbox2 = gtk_vbox_new(0,6);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),vbox2,(!0),(!0),0);
  gtk_widget_show(vbox2);
  sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  sg2 = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  bad_entry = gtk_entry_new();
/* Set a minimum size. Since they're in a size group, the other entry will match up. */
  gtk_widget_set_size_request(bad_entry,350,(-1));
  gtk_size_group_add_widget(sg2,bad_entry);
  pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox2),gtk_box_get_type()))),((const char *)(dgettext("pidgin","You _type:"))),sg,bad_entry,0,0);
  good_entry = gtk_entry_new();
  gtk_size_group_add_widget(sg2,good_entry);
  pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox2),gtk_box_get_type()))),((const char *)(dgettext("pidgin","You _send:"))),sg,good_entry,0,0);
/* Created here so it can be passed to whole_words_button_toggled. */
  case_toggle = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","_Exact case match (uncheck for automatic case handling)"))));
  complete_toggle = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Only replace _whole words"))));
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)complete_toggle),gtk_toggle_button_get_type()))),(!0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)complete_toggle),((GType )(20 << 2))))),"clicked",((GCallback )whole_words_button_toggled),case_toggle,0,((GConnectFlags )0));
  gtk_widget_show(complete_toggle);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox2),gtk_box_get_type()))),complete_toggle,0,0,0);
/* The button is created above so it can be passed to whole_words_button_toggled. */
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)case_toggle),gtk_toggle_button_get_type()))),0);
  gtk_widget_show(case_toggle);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox2),gtk_box_get_type()))),case_toggle,0,0,0);
  button = gtk_button_new_from_stock("gtk-add");
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )list_add_new),0,0,((GConnectFlags )0));
  vbox3 = gtk_vbox_new(0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),vbox3,(!0),0,0);
  gtk_widget_show(vbox3);
  gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox3),gtk_box_get_type()))),button,0,0,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)bad_entry),((GType )(20 << 2))))),"changed",((GCallback )on_entry_changed),button,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)good_entry),((GType )(20 << 2))))),"changed",((GCallback )on_entry_changed),button,0,((GConnectFlags )0));
  gtk_widget_set_sensitive(button,0);
  gtk_widget_show(button);
#if 0
#endif
  gtk_widget_show_all(ret);
  g_object_unref(sg);
  g_object_unref(sg2);
  return ret;
}
static PidginPluginUiInfo ui_info = {(get_config_frame), (0), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* page_num (Reserved) */
/* padding */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gtk-gaim"), (0), ((GList *)((void *)0)), (0), ("gtk-spellcheck"), ("Text replacement"), ("2.10.9"), ("Replaces text in outgoing messages according to user-defined rules."), ("Replaces text in outgoing messages according to user-defined rules."), ("Eric Warmenhoven <eric@warmenhoven.org>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((&ui_info)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
#if 0
#endif
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
