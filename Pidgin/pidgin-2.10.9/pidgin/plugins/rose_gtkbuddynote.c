/*
 * GtkBuddyNote - Store notes on particular buddies
 * Copyright (C) 2007 Etan Reisner <deryni@pidgin.im>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301, USA.
 */
#include "internal.h"
#include <gtkblist.h>
#include <gtkplugin.h>
#include <debug.h>
#include <version.h>

static void append_to_tooltip(PurpleBlistNode *node,GString *text,gboolean full)
{
  if (full != 0) {
    const gchar *note = purple_blist_node_get_string(node,"notes");
    if ((note != ((const gchar *)((void *)0))) && (( *note) != 0)) {
      char *tmp;
      char *esc;
      purple_markup_html_to_xhtml(note,0,&tmp);
      esc = g_markup_escape_text(tmp,(-1));
      g_free(tmp);
      g_string_append_printf(text,((const char *)(dgettext("pidgin","\n<b>Buddy Note</b>: %s"))),esc);
      g_free(esc);
    }
  }
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  purple_signal_connect(pidgin_blist_get_handle(),"drawing-tooltip",plugin,((PurpleCallback )append_to_tooltip),0);
  return (!0);
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  PurplePlugin *buddynote = (PurplePlugin *)((void *)0);
  buddynote = purple_plugins_find_with_id("core-plugin_pack-buddynote");
  purple_plugin_unload(buddynote);
  return (!0);
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gtk-gaim"), (0), ((GList *)((void *)0)), (0), ("gtkbuddynote"), ("Buddy Notes"), ("2.10.9"), ("Store notes on particular buddies."), ("Adds the option to store notes for buddies on your buddy list."), ("Etan Reisner <deryni@pidgin.im>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< major version */
/**< minor version */
/**< type */
/**< ui_requirement */
/**< flags */
/**< dependencies */
/**< priority */
/**< id */
/**< name */
/**< version */
/**< summary */
/**< description */
/**< author */
/**< homepage */
/**< load */
/**< unload */
/**< destroy */
/**< ui_info */
/**< extra_info */
/**< prefs_info */
/**< actions */
/* padding */
};

static gboolean check_for_buddynote(gpointer data)
{
  PurplePlugin *buddynote = (PurplePlugin *)((void *)0);
  PurplePlugin *plugin = (PurplePlugin *)data;
  buddynote = purple_plugins_find_with_id("core-plugin_pack-buddynote");
  if (buddynote == ((PurplePlugin *)((void *)0))) {
    buddynote = purple_plugins_find_with_basename("buddynote");
  }
  if (buddynote != ((PurplePlugin *)((void *)0))) {
    PurplePluginInfo *bninfo = (buddynote -> info);
    bninfo -> flags = 1;
/* If non-gtk buddy note plugin is loaded, but we are not, then load
		 * ourselves, otherwise people upgrading from pre-gtkbuddynote days
		 * will not have 'Buddy Notes' showing as loaded in the plugins list.
		 * We also trigger a save on the list of plugins because it's not been
		 * loaded through the UI. */
    if ((purple_plugin_is_loaded(buddynote) != 0) && !(purple_plugin_is_loaded(plugin) != 0)) {
      purple_plugin_load(plugin);
      pidgin_plugins_save();
    }
  }
  else {
    info.flags = 1;
  }
  return 0;
}

static void init_plugin(PurplePlugin *plugin)
{
/* Use g_idle_add so that the rest of the plugins can get loaded
	 * before we do our check. */
  g_idle_add(check_for_buddynote,plugin);
  info.dependencies = g_list_append(info.dependencies,"core-plugin_pack-buddynote");
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
