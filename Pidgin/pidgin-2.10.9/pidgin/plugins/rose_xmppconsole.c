/*
 * Purple - XMPP debugging tool
 *
 * Copyright (C) 2002-2003, Sean Egan
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "gtkplugin.h"
#include "version.h"
#include "prpl.h"
#include "xmlnode.h"
#include "gtkimhtml.h"
#include "gtkutils.h"
typedef struct __unnamed_class___F0_L31_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L256R__Pe___variable_name_unknown_scope_and_name__scope__gc__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__hbox__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__dropdown__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__imhtml__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__sw__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__count__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GList_GList__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__accounts {
PurpleConnection *gc;
GtkWidget *window;
GtkWidget *hbox;
GtkWidget *dropdown;
GtkWidget *imhtml;
GtkWidget *entry;
GtkWidget *sw;
int count;
GList *accounts;}XmppConsole;
XmppConsole *console = (XmppConsole *)((void *)0);
static void *xmpp_console_handle = (void *)((void *)0);
#define BRACKET_COLOR "#940f8c"
#define TAG_COLOR "#8b1dab"
#define ATTR_NAME_COLOR "#a02961"
#define ATTR_VALUE_COLOR "#324aa4"
#define XMLNS_COLOR "#2cb12f"

static char *xmlnode_to_pretty_str(xmlnode *node,int *len,int depth)
{
  GString *text = g_string_new("");
  xmlnode *c;
  char *node_name;
  char *esc;
  char *esc2;
  char *tab = (char *)((void *)0);
  gboolean need_end = 0;
  gboolean pretty = (!0);
  do {
    if (node != ((xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return 0;
    };
  }while (0);
  if ((pretty != 0) && (depth != 0)) {
    tab = g_strnfill(depth,9);
    text = g_string_append(text,tab);
  }
  node_name = g_markup_escape_text((node -> name),(-1));
  g_string_append_printf(text,"<font color=\'#940f8c\'>&lt;</font><font color=\'#8b1dab\'><b>%s</b></font>",node_name);
  if ((node -> xmlns) != 0) {
    if (((!((node -> parent) != 0) || !(( *(node -> parent)).xmlns != 0)) || (strcmp((node -> xmlns),( *(node -> parent)).xmlns) != 0)) && (strcmp((node -> xmlns),"jabber:client") != 0)) {
      char *xmlns = g_markup_escape_text((node -> xmlns),(-1));
      g_string_append_printf(text," <font color=\'#a02961\'><b>xmlns</b></font>=\'<font color=\'#2cb12f\'><b>%s</b></font>\'",xmlns);
      g_free(xmlns);
    }
  }
  for (c = (node -> child); c != 0; c = (c -> next)) {
    if ((c -> type) == XMLNODE_TYPE_ATTRIB) {
      esc = g_markup_escape_text((c -> name),(-1));
      esc2 = g_markup_escape_text((c -> data),(-1));
      g_string_append_printf(text," <font color=\'#a02961\'><b>%s</b></font>=\'<font color=\'#324aa4\'>%s</font>\'",esc,esc2);
      g_free(esc);
      g_free(esc2);
    }
    else if (((c -> type) == XMLNODE_TYPE_TAG) || ((c -> type) == XMLNODE_TYPE_DATA)) {
      if ((c -> type) == XMLNODE_TYPE_DATA) 
        pretty = 0;
      need_end = (!0);
    }
  }
  if (need_end != 0) {
    g_string_append_printf(text,"<font color=\'#940f8c\'>&gt;</font>%s",((pretty != 0)?"<br>" : ""));
    for (c = (node -> child); c != 0; c = (c -> next)) {
      if ((c -> type) == XMLNODE_TYPE_TAG) {
        int esc_len;
        esc = xmlnode_to_pretty_str(c,&esc_len,(depth + 1));
        text = g_string_append_len(text,esc,esc_len);
        g_free(esc);
      }
      else if (((c -> type) == XMLNODE_TYPE_DATA) && ((c -> data_sz) > 0)) {
        esc = g_markup_escape_text((c -> data),(c -> data_sz));
        text = g_string_append(text,esc);
        g_free(esc);
      }
    }
    if ((tab != 0) && (pretty != 0)) 
      text = g_string_append(text,tab);
    g_string_append_printf(text,"<font color=\'#940f8c\'>&lt;</font>/<font color=\'#8b1dab\'><b>%s</b></font><font color=\'#940f8c\'>&gt;</font><br>",node_name);
  }
  else {
    g_string_append_printf(text,"/<font color=\'#940f8c\'>&gt;</font><br>");
  }
  g_free(node_name);
  g_free(tab);
  if (len != 0) 
     *len = (text -> len);
  return g_string_free(text,0);
}

static void xmlnode_received_cb(PurpleConnection *gc,xmlnode **packet,gpointer null)
{
  char *str;
  char *formatted;
  if (!(console != 0) || ((console -> gc) != gc)) 
    return ;
  str = xmlnode_to_pretty_str( *packet,0,0);
  formatted = g_strdup_printf("<body bgcolor=\'#ffcece\'><pre>%s</pre></body>",str);
  gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(console -> imhtml)),gtk_imhtml_get_type()))),formatted,0,0);
  g_free(formatted);
  g_free(str);
}

static void xmlnode_sent_cb(PurpleConnection *gc,char **packet,gpointer null)
{
  char *str;
  char *formatted;
  xmlnode *node;
  if (!(console != 0) || ((console -> gc) != gc)) 
    return ;
  node = xmlnode_from_str(( *packet),(-1));
  if (!(node != 0)) 
    return ;
  str = xmlnode_to_pretty_str(node,0,0);
  formatted = g_strdup_printf("<body bgcolor=\'#dcecc4\'><pre>%s</pre></body>",str);
  gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(console -> imhtml)),gtk_imhtml_get_type()))),formatted,0,0);
  g_free(formatted);
  g_free(str);
  xmlnode_free(node);
}

static void message_send_cb(GtkWidget *widget,gpointer p)
{
  GtkTextIter start;
  GtkTextIter end;
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurpleConnection *gc;
  GtkTextBuffer *buffer;
  char *text;
  gc = (console -> gc);
  if (gc != 0) 
    prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info);
  buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(console -> entry)),gtk_text_view_get_type()))));
  gtk_text_buffer_get_start_iter(buffer,&start);
  gtk_text_buffer_get_end_iter(buffer,&end);
  text = gtk_imhtml_get_text(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(console -> entry)),gtk_imhtml_get_type()))),&start,&end);
  if ((prpl_info != 0) && ((prpl_info -> send_raw) != ((int (*)(PurpleConnection *, const char *, int ))((void *)0)))) 
    ( *(prpl_info -> send_raw))(gc,text,(strlen(text)));
  g_free(text);
  gtk_imhtml_delete(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(console -> entry)),gtk_imhtml_get_type()))),0,0);
}

static void entry_changed_cb(GtkTextBuffer *buffer,void *data)
{
  char *xmlstr;
  char *str;
  GtkTextIter iter;
  int wrapped_lines;
  int lines;
  GdkRectangle oneline;
  int height;
  int pad_top;
  int pad_inside;
  int pad_bottom;
  GtkTextIter start;
  GtkTextIter end;
  xmlnode *node;
  wrapped_lines = 1;
  gtk_text_buffer_get_start_iter(buffer,&iter);
  gtk_text_view_get_iter_location(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(console -> entry)),gtk_text_view_get_type()))),(&iter),&oneline);
  while(gtk_text_view_forward_display_line(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(console -> entry)),gtk_text_view_get_type()))),&iter) != 0)
    wrapped_lines++;
  lines = gtk_text_buffer_get_line_count(buffer);
/* Show a maximum of 64 lines */
  lines = ((lines < 6)?lines : 6);
  wrapped_lines = ((wrapped_lines < 6)?wrapped_lines : 6);
  pad_top = gtk_text_view_get_pixels_above_lines(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(console -> entry)),gtk_text_view_get_type()))));
  pad_bottom = gtk_text_view_get_pixels_below_lines(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(console -> entry)),gtk_text_view_get_type()))));
  pad_inside = gtk_text_view_get_pixels_inside_wrap(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(console -> entry)),gtk_text_view_get_type()))));
  height = (((oneline.height + pad_top) + pad_bottom) * lines);
  height += ((oneline.height + pad_inside) * (wrapped_lines - lines));
  gtk_widget_set_size_request((console -> sw),(-1),(height + 6));
  gtk_text_buffer_get_start_iter(buffer,&start);
  gtk_text_buffer_get_end_iter(buffer,&end);
  str = gtk_text_buffer_get_text(buffer,(&start),(&end),0);
  if (!(str != 0)) 
    return ;
  xmlstr = g_strdup_printf("<xml>%s</xml>",str);
  node = xmlnode_from_str(xmlstr,(-1));
  if (node != 0) {
    gtk_imhtml_clear_formatting(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(console -> entry)),gtk_imhtml_get_type()))));
  }
  else {
    gtk_imhtml_toggle_background(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(console -> entry)),gtk_imhtml_get_type()))),"#ffcece");
  }
  g_free(str);
  g_free(xmlstr);
  if (node != 0) 
    xmlnode_free(node);
}

static void iq_clicked_cb(GtkWidget *w,gpointer nul)
{
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *to_entry;
  GtkWidget *label;
  GtkWidget *type_combo;
  GtkSizeGroup *sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  GtkTextIter iter;
  GtkTextBuffer *buffer;
  const char *to;
  int result;
  char *stanza;
  GtkWidget *dialog = gtk_dialog_new_with_buttons("<iq/>",((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(console -> window)),gtk_window_get_type()))),(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT),"gtk-cancel",GTK_RESPONSE_REJECT,"gtk-ok",GTK_RESPONSE_ACCEPT,((void *)((void *)0)));
  gtk_dialog_set_has_separator(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),0);
  gtk_dialog_set_default_response(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),GTK_RESPONSE_ACCEPT);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_container_get_type()))),12);
#if GTK_CHECK_VERSION(2,14,0)
  vbox = gtk_dialog_get_content_area(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))));
#else
#endif
  hbox = gtk_hbox_new(0,3);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  label = gtk_label_new("To:");
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_size_group_add_widget(sg,label);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  to_entry = gtk_entry_new();
  gtk_entry_set_activates_default(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)to_entry),gtk_entry_get_type()))),(!0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),to_entry,0,0,0);
  hbox = gtk_hbox_new(0,3);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  label = gtk_label_new("Type:");
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_size_group_add_widget(sg,label);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  type_combo = gtk_combo_box_new_text();
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type()))),"get");
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type()))),"set");
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type()))),"result");
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type()))),"error");
  gtk_combo_box_set_active(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type()))),0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),type_combo,0,0,0);
  gtk_widget_show_all(vbox);
  result = gtk_dialog_run(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))));
  if (result != GTK_RESPONSE_ACCEPT) {
    gtk_widget_destroy(dialog);
    return ;
  }
  to = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)to_entry),gtk_entry_get_type()))));
  stanza = g_strdup_printf("<iq %s%s%s id=\'console%x\' type=\'%s\'></iq>",(((to != 0) && (( *to) != 0))?"to=\'" : ""),(((to != 0) && (( *to) != 0))?to : ""),(((to != 0) && (( *to) != 0))?"\'" : ""),g_random_int(),gtk_combo_box_get_active_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type())))));
  buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(console -> entry)),gtk_text_view_get_type()))));
  gtk_text_buffer_set_text(buffer,stanza,(-1));
  gtk_text_buffer_get_iter_at_offset(buffer,&iter,(strstr(stanza,"</iq>") - stanza));
  gtk_text_buffer_place_cursor(buffer,(&iter));
  g_free(stanza);
  gtk_widget_destroy(dialog);
  g_object_unref(sg);
}

static void presence_clicked_cb(GtkWidget *w,gpointer nul)
{
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *to_entry;
  GtkWidget *status_entry;
  GtkWidget *priority_entry;
  GtkWidget *label;
  GtkWidget *show_combo;
  GtkWidget *type_combo;
  GtkSizeGroup *sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  GtkTextIter iter;
  GtkTextBuffer *buffer;
  const char *to;
  const char *type;
  const char *status;
  const char *show;
  const char *priority;
  int result;
  char *stanza;
  GtkWidget *dialog = gtk_dialog_new_with_buttons("<presence/>",((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(console -> window)),gtk_window_get_type()))),(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT),"gtk-cancel",GTK_RESPONSE_REJECT,"gtk-ok",GTK_RESPONSE_ACCEPT,((void *)((void *)0)));
  gtk_dialog_set_has_separator(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),0);
  gtk_dialog_set_default_response(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),GTK_RESPONSE_ACCEPT);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_container_get_type()))),12);
#if GTK_CHECK_VERSION(2,14,0)
  vbox = gtk_dialog_get_content_area(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))));
#else
#endif
  hbox = gtk_hbox_new(0,3);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  label = gtk_label_new("To:");
  gtk_size_group_add_widget(sg,label);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  to_entry = gtk_entry_new();
  gtk_entry_set_activates_default(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)to_entry),gtk_entry_get_type()))),(!0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),to_entry,0,0,0);
  hbox = gtk_hbox_new(0,3);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  label = gtk_label_new("Type:");
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_size_group_add_widget(sg,label);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  type_combo = gtk_combo_box_new_text();
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type()))),"default");
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type()))),"unavailable");
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type()))),"subscribe");
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type()))),"unsubscribe");
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type()))),"subscribed");
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type()))),"unsubscribed");
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type()))),"probe");
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type()))),"error");
  gtk_combo_box_set_active(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type()))),0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),type_combo,0,0,0);
  hbox = gtk_hbox_new(0,3);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  label = gtk_label_new("Show:");
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_size_group_add_widget(sg,label);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  show_combo = gtk_combo_box_new_text();
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)show_combo),gtk_combo_box_get_type()))),"default");
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)show_combo),gtk_combo_box_get_type()))),"away");
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)show_combo),gtk_combo_box_get_type()))),"dnd");
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)show_combo),gtk_combo_box_get_type()))),"xa");
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)show_combo),gtk_combo_box_get_type()))),"chat");
  gtk_combo_box_set_active(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)show_combo),gtk_combo_box_get_type()))),0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),show_combo,0,0,0);
  hbox = gtk_hbox_new(0,3);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  label = gtk_label_new("Status:");
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_size_group_add_widget(sg,label);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  status_entry = gtk_entry_new();
  gtk_entry_set_activates_default(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)status_entry),gtk_entry_get_type()))),(!0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),status_entry,0,0,0);
  hbox = gtk_hbox_new(0,3);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  label = gtk_label_new("Priority:");
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_size_group_add_widget(sg,label);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  priority_entry = gtk_spin_button_new_with_range((-128),127,1);
  gtk_spin_button_set_value(((GtkSpinButton *)(g_type_check_instance_cast(((GTypeInstance *)priority_entry),gtk_spin_button_get_type()))),0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),priority_entry,0,0,0);
  gtk_widget_show_all(vbox);
  result = gtk_dialog_run(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))));
  if (result != GTK_RESPONSE_ACCEPT) {
    gtk_widget_destroy(dialog);
    return ;
  }
  to = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)to_entry),gtk_entry_get_type()))));
  type = (gtk_combo_box_get_active_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type())))));
  if (!(strcmp(type,"default") != 0)) 
    type = "";
  show = (gtk_combo_box_get_active_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)show_combo),gtk_combo_box_get_type())))));
  if (!(strcmp(show,"default") != 0)) 
    show = "";
  status = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)status_entry),gtk_entry_get_type()))));
  priority = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)priority_entry),gtk_entry_get_type()))));
  if (!(strcmp(priority,"0") != 0)) 
    priority = "";
  stanza = g_strdup_printf("<presence %s%s%s id=\'console%x\' %s%s%s>%s%s%s%s%s%s%s%s%s</presence>",((( *to) != 0)?"to=\'" : ""),((( *to) != 0)?to : ""),((( *to) != 0)?"\'" : ""),g_random_int(),((( *type) != 0)?"type=\'" : ""),((( *type) != 0)?type : ""),((( *type) != 0)?"\'" : ""),((( *show) != 0)?"<show>" : ""),((( *show) != 0)?show : ""),((( *show) != 0)?"</show>" : ""),((( *status) != 0)?"<status>" : ""),((( *status) != 0)?status : ""),((( *status) != 0)?"</status>" : ""),((( *priority) != 0)?"<priority>" : ""),((( *priority) != 0)?priority : ""),((( *priority) != 0)?"</priority>" : ""));
  buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(console -> entry)),gtk_text_view_get_type()))));
  gtk_text_buffer_set_text(buffer,stanza,(-1));
  gtk_text_buffer_get_iter_at_offset(buffer,&iter,(strstr(stanza,"</presence>") - stanza));
  gtk_text_buffer_place_cursor(buffer,(&iter));
  g_free(stanza);
  gtk_widget_destroy(dialog);
  g_object_unref(sg);
}

static void message_clicked_cb(GtkWidget *w,gpointer nul)
{
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *to_entry;
  GtkWidget *body_entry;
  GtkWidget *thread_entry;
  GtkWidget *subject_entry;
  GtkWidget *label;
  GtkWidget *type_combo;
  GtkSizeGroup *sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  GtkTextIter iter;
  GtkTextBuffer *buffer;
  const char *to;
  const char *body;
  const char *thread;
  const char *subject;
  char *stanza;
  int result;
  GtkWidget *dialog = gtk_dialog_new_with_buttons("<message/>",((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(console -> window)),gtk_window_get_type()))),(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT),"gtk-cancel",GTK_RESPONSE_REJECT,"gtk-ok",GTK_RESPONSE_ACCEPT,((void *)((void *)0)));
  gtk_dialog_set_has_separator(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),0);
  gtk_dialog_set_default_response(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),GTK_RESPONSE_ACCEPT);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_container_get_type()))),12);
#if GTK_CHECK_VERSION(2,14,0)
  vbox = gtk_dialog_get_content_area(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))));
#else
#endif
  hbox = gtk_hbox_new(0,3);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  label = gtk_label_new("To:");
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_size_group_add_widget(sg,label);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  to_entry = gtk_entry_new();
  gtk_entry_set_activates_default(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)to_entry),gtk_entry_get_type()))),(!0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),to_entry,0,0,0);
  hbox = gtk_hbox_new(0,3);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  label = gtk_label_new("Type:");
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_size_group_add_widget(sg,label);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  type_combo = gtk_combo_box_new_text();
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type()))),"chat");
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type()))),"headline");
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type()))),"groupchat");
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type()))),"normal");
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type()))),"error");
  gtk_combo_box_set_active(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type()))),0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),type_combo,0,0,0);
  hbox = gtk_hbox_new(0,3);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  label = gtk_label_new("Body:");
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_size_group_add_widget(sg,label);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  body_entry = gtk_entry_new();
  gtk_entry_set_activates_default(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)body_entry),gtk_entry_get_type()))),(!0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),body_entry,0,0,0);
  hbox = gtk_hbox_new(0,3);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  label = gtk_label_new("Subject:");
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_size_group_add_widget(sg,label);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  subject_entry = gtk_entry_new();
  gtk_entry_set_activates_default(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)subject_entry),gtk_entry_get_type()))),(!0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),subject_entry,0,0,0);
  hbox = gtk_hbox_new(0,3);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  label = gtk_label_new("Thread:");
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_size_group_add_widget(sg,label);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  thread_entry = gtk_entry_new();
  gtk_entry_set_activates_default(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)thread_entry),gtk_entry_get_type()))),(!0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),thread_entry,0,0,0);
  gtk_widget_show_all(vbox);
  result = gtk_dialog_run(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))));
  if (result != GTK_RESPONSE_ACCEPT) {
    gtk_widget_destroy(dialog);
    return ;
  }
  to = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)to_entry),gtk_entry_get_type()))));
  body = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)body_entry),gtk_entry_get_type()))));
  thread = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)thread_entry),gtk_entry_get_type()))));
  subject = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)subject_entry),gtk_entry_get_type()))));
  stanza = g_strdup_printf("<message %s%s%s id=\'console%x\' type=\'%s\'>%s%s%s%s%s%s%s%s%s</message>",((( *to) != 0)?"to=\'" : ""),((( *to) != 0)?to : ""),((( *to) != 0)?"\'" : ""),g_random_int(),gtk_combo_box_get_active_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)type_combo),gtk_combo_box_get_type())))),((( *body) != 0)?"<body>" : ""),((( *body) != 0)?body : ""),((( *body) != 0)?"</body>" : ""),((( *subject) != 0)?"<subject>" : ""),((( *subject) != 0)?subject : ""),((( *subject) != 0)?"</subject>" : ""),((( *thread) != 0)?"<thread>" : ""),((( *thread) != 0)?thread : ""),((( *thread) != 0)?"</thread>" : ""));
  buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(console -> entry)),gtk_text_view_get_type()))));
  gtk_text_buffer_set_text(buffer,stanza,(-1));
  gtk_text_buffer_get_iter_at_offset(buffer,&iter,(strstr(stanza,"</message>") - stanza));
  gtk_text_buffer_place_cursor(buffer,(&iter));
  g_free(stanza);
  gtk_widget_destroy(dialog);
  g_object_unref(sg);
}

static void signing_on_cb(PurpleConnection *gc)
{
  if (!(console != 0)) 
    return ;
  gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(console -> dropdown)),gtk_combo_box_get_type()))),purple_account_get_username((gc -> account)));
  console -> accounts = g_list_append((console -> accounts),gc);
  console -> count++;
  if ((console -> count) == 1) 
    console -> gc = gc;
  else 
    gtk_widget_show_all((console -> hbox));
}

static void signed_off_cb(PurpleConnection *gc)
{
  int i = 0;
  GList *l;
  if (!(console != 0)) 
    return ;
  l = (console -> accounts);
{
    while(l != 0){
      PurpleConnection *g = (l -> data);
      if (gc == g) 
        break; 
      i++;
      l = (l -> next);
    }
  }
  if (l == ((GList *)((void *)0))) 
    return ;
  gtk_combo_box_remove_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(console -> dropdown)),gtk_combo_box_get_type()))),i);
  console -> accounts = g_list_remove((console -> accounts),gc);
  console -> count--;
  if (gc == (console -> gc)) {
    console -> gc = ((PurpleConnection *)((void *)0));
    gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(console -> imhtml)),gtk_imhtml_get_type()))),((const char *)(dgettext("pidgin","<font color=\'#777777\'>Logged out.</font>"))),0,0);
  }
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  PurplePlugin *jabber;
  jabber = purple_find_prpl("prpl-jabber");
  if (!(jabber != 0)) 
    return 0;
  xmpp_console_handle = plugin;
  purple_signal_connect(jabber,"jabber-receiving-xmlnode",xmpp_console_handle,((PurpleCallback )xmlnode_received_cb),0);
  purple_signal_connect(jabber,"jabber-sending-text",xmpp_console_handle,((PurpleCallback )xmlnode_sent_cb),0);
  purple_signal_connect(purple_connections_get_handle(),"signing-on",plugin,((PurpleCallback )signing_on_cb),0);
  purple_signal_connect(purple_connections_get_handle(),"signed-off",plugin,((PurpleCallback )signed_off_cb),0);
  return (!0);
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  if (console != 0) 
    gtk_widget_destroy((console -> window));
  return (!0);
}

static void console_destroy(GtkObject *window,gpointer nul)
{
  g_list_free((console -> accounts));
  g_free(console);
  console = ((XmppConsole *)((void *)0));
}

static void dropdown_changed_cb(GtkComboBox *widget,gpointer nul)
{
  PurpleAccount *account;
  if (!(console != 0)) 
    return ;
  account = purple_accounts_find((gtk_combo_box_get_active_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(console -> dropdown)),gtk_combo_box_get_type()))))),"prpl-jabber");
  if (!(account != 0) || !((account -> gc) != 0)) 
    return ;
  console -> gc = (account -> gc);
  gtk_imhtml_delete(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(console -> imhtml)),gtk_imhtml_get_type()))),0,0);
}

static void create_console(PurplePluginAction *action)
{
  GtkWidget *vbox = gtk_vbox_new(0,6);
  GtkWidget *label;
  GtkTextBuffer *buffer;
  GtkWidget *toolbar;
  GList *connections;
  GtkToolItem *button;
  if (console != 0) {
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(console -> window)),gtk_window_get_type()))));
    return ;
  }
  console = ((XmppConsole *)(g_malloc0_n(1,(sizeof(XmppConsole )))));
  console -> window = pidgin_create_window(((const char *)(dgettext("pidgin","XMPP Console"))),12,0,(!0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(console -> window)),((GType )(20 << 2))))),"destroy",((GCallback )console_destroy),0,0,((GConnectFlags )0));
  gtk_window_set_default_size(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(console -> window)),gtk_window_get_type()))),580,400);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(console -> window)),gtk_container_get_type()))),vbox);
  console -> hbox = gtk_hbox_new(0,3);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),(console -> hbox),0,0,0);
  label = gtk_label_new(((const char *)(dgettext("pidgin","Account: "))));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0.0,0.5);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(console -> hbox)),gtk_box_get_type()))),label,0,0,0);
  console -> dropdown = gtk_combo_box_new_text();
  for (connections = purple_connections_get_all(); connections != 0; connections = (connections -> next)) {
    PurpleConnection *gc = (connections -> data);
    if (!(strcmp(purple_account_get_protocol_id((purple_connection_get_account(gc))),"prpl-jabber") != 0)) {
      console -> count++;
      console -> accounts = g_list_append((console -> accounts),gc);
      gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(console -> dropdown)),gtk_combo_box_get_type()))),purple_account_get_username((purple_connection_get_account(gc))));
      if (!((console -> gc) != 0)) 
        console -> gc = gc;
    }
  }
  gtk_combo_box_set_active(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(console -> dropdown)),gtk_combo_box_get_type()))),0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(console -> hbox)),gtk_box_get_type()))),(console -> dropdown),(!0),(!0),0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(console -> dropdown)),((GType )(20 << 2))))),"changed",((GCallback )dropdown_changed_cb),0,0,((GConnectFlags )0));
  console -> imhtml = gtk_imhtml_new(0,0);
  if ((console -> count) == 0) 
    gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(console -> imhtml)),gtk_imhtml_get_type()))),((const char *)(dgettext("pidgin","<font color=\'#777777\'>Not connected to XMPP</font>"))),0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),pidgin_make_scrollable((console -> imhtml),GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC,GTK_SHADOW_ETCHED_IN,-1,-1),(!0),(!0),0);
  toolbar = gtk_toolbar_new();
  button = gtk_tool_button_new(0,"<iq/>");
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )iq_clicked_cb),0,0,((GConnectFlags )0));
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_container_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_widget_get_type()))));
  button = gtk_tool_button_new(0,"<presence/>");
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )presence_clicked_cb),0,0,((GConnectFlags )0));
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_container_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_widget_get_type()))));
  button = gtk_tool_button_new(0,"<message/>");
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )message_clicked_cb),0,0,((GConnectFlags )0));
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_container_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_widget_get_type()))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),toolbar,0,0,0);
  console -> entry = gtk_imhtml_new(0,0);
  gtk_imhtml_set_whole_buffer_formatting_only(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(console -> entry)),gtk_imhtml_get_type()))),(!0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(console -> entry)),((GType )(20 << 2))))),"message_send",((GCallback )message_send_cb),console,0,((GConnectFlags )0));
  console -> sw = pidgin_make_scrollable((console -> entry),GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC,GTK_SHADOW_ETCHED_IN,-1,-1);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),(console -> sw),0,0,0);
  gtk_imhtml_set_editable(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(console -> entry)),gtk_imhtml_get_type()))),(!0));
  buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(console -> entry)),gtk_text_view_get_type()))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buffer),((GType )(20 << 2))))),"changed",((GCallback )entry_changed_cb),0,0,((GConnectFlags )0));
  entry_changed_cb(buffer,0);
  gtk_widget_show_all((console -> window));
  if ((console -> count) < 2) 
    gtk_widget_hide((console -> hbox));
}

static GList *actions(PurplePlugin *plugin,gpointer context)
{
  GList *l = (GList *)((void *)0);
  PurplePluginAction *act = (PurplePluginAction *)((void *)0);
  act = purple_plugin_action_new(((const char *)(dgettext("pidgin","XMPP Console"))),create_console);
  l = g_list_append(l,act);
  return l;
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gtk-gaim"), (0), ((GList *)((void *)0)), (0), ("gtk-xmpp"), ("XMPP Console"), ("2.10.9"), ("Send and receive raw XMPP stanzas."), ("This plugin is useful for debugging XMPP servers or clients."), ("Sean Egan <seanegan@gmail.com>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), (actions), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
