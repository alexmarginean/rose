/**
 * @file pidginrc.c Pidgin GTK+ resource control plugin.
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "gtkplugin.h"
#include "gtkprefs.h"
#include "gtkutils.h"
#include "util.h"
#include "version.h"
static guint pref_callback;
static const gchar *color_prefs[] = {("/plugins/gtk/purplerc/color/GtkIMHtml::hyperlink-color"), ("/plugins/gtk/purplerc/color/GtkIMHtml::hyperlink-visited-color"), ("/plugins/gtk/purplerc/color/GtkIMHtml::send-name-color"), ("/plugins/gtk/purplerc/color/GtkIMHtml::receive-name-color"), ("/plugins/gtk/purplerc/color/GtkIMHtml::highlight-name-color"), ("/plugins/gtk/purplerc/color/GtkIMHtml::action-name-color"), ("/plugins/gtk/purplerc/color/GtkIMHtml::typing-notification-color")};
static const gchar *color_prefs_set[] = {("/plugins/gtk/purplerc/set/color/GtkIMHtml::hyperlink-color"), ("/plugins/gtk/purplerc/set/color/GtkIMHtml::hyperlink-visited-color"), ("/plugins/gtk/purplerc/set/color/GtkIMHtml::send-name-color"), ("/plugins/gtk/purplerc/set/color/GtkIMHtml::receive-name-color"), ("/plugins/gtk/purplerc/set/color/GtkIMHtml::highlight-name-color"), ("/plugins/gtk/purplerc/set/color/GtkIMHtml::action-name-color"), ("/plugins/gtk/purplerc/set/color/GtkIMHtml::typing-notification-color")};
static const gchar *color_names[] = {("Hyperlink Color"), ("Visited Hyperlink Color"), ("Sent Message Name Color"), ("Received Message Name Color"), ("Highlighted Message Name Color"), ("Action Message Name Color"), ("Typing Notification Color")};
static GtkWidget *color_widgets[sizeof(color_prefs) / sizeof(color_prefs[0])];
static const gchar *widget_size_prefs[] = {("/plugins/gtk/purplerc/size/GtkTreeView::horizontal_separator")};
static const gchar *widget_size_prefs_set[] = {("/plugins/gtk/purplerc/set/size/GtkTreeView::horizontal_separator")};
static const gchar *widget_size_names[] = {("GtkTreeView Horizontal Separation")};
static GtkWidget *widget_size_widgets[sizeof(widget_size_prefs) / sizeof(widget_size_prefs[0])];
static const gchar *font_prefs[] = {("/plugins/gtk/purplerc/font/*pidgin_conv_entry"), ("/plugins/gtk/purplerc/font/*pidgin_conv_imhtml"), ("/plugins/gtk/purplerc/font/*pidgin_request_imhtml"), ("/plugins/gtk/purplerc/font/*pidgin_notify_imhtml")};
static const gchar *font_prefs_set[] = {("/plugins/gtk/purplerc/set/font/*pidgin_conv_entry"), ("/plugins/gtk/purplerc/set/font/*pidgin_conv_imhtml"), ("/plugins/gtk/purplerc/set/font/*pidgin_request_imhtml"), ("/plugins/gtk/purplerc/set/font/*pidgin_notify_imhtml")};
static const gchar *font_names[] = {("Conversation Entry"), ("Conversation History"), ("Request Dialog"), ("Notify Dialog")};
static GtkWidget *font_widgets[sizeof(font_prefs) / sizeof(font_prefs[0])];
/*
static const gchar *widget_bool_prefs[] = {
};
static const gchar *widget_bool_prefs_set[] = {
};
static const gchar *widget_bool_names[] = {
};
static GtkWidget *widget_bool_widgets[G_N_ELEMENTS(widget_bool_prefs)];
*/

static GString *make_gtkrc_string()
{
  gint i;
  gchar *prefbase = (gchar *)((void *)0);
  GString *style_string = g_string_new("");
  if (purple_prefs_get_bool("/plugins/gtk/purplerc/set/gtk-font-name") != 0) {
    const gchar *pref = purple_prefs_get_string("/plugins/gtk/purplerc/gtk-font-name");
    if ((pref != ((const gchar *)((void *)0))) && (strcmp(pref,"") != 0)) {
      g_string_append_printf(style_string,"gtk-font-name = \"%s\"\n",pref);
    }
  }
  if (purple_prefs_get_bool("/plugins/gtk/purplerc/set/gtk-key-theme-name") != 0) {
    const gchar *pref = purple_prefs_get_string("/plugins/gtk/purplerc/gtk-key-theme-name");
    if ((pref != ((const gchar *)((void *)0))) && (strcmp(pref,"") != 0)) {
      g_string_append_printf(style_string,"gtk-key-theme-name = \"%s\"\n",pref);
    }
  }
  g_string_append(style_string,"style \"purplerc_style\"\n{");
  if (purple_prefs_get_bool("/plugins/gtk/purplerc/set/disable-typing-notification") != 0) {
    g_string_append(style_string,"\tGtkIMHtml::typing-notification-enable = 0\n");
  }
  for (i = 0; i < sizeof(color_prefs) / sizeof(color_prefs[0]); i++) {
    if (purple_prefs_get_bool(color_prefs_set[i]) != 0) {
      const gchar *pref;
      pref = purple_prefs_get_string(color_prefs[i]);
      if ((pref != ((const gchar *)((void *)0))) && (strcmp(pref,"") != 0)) {
        prefbase = g_path_get_basename(color_prefs[i]);
        g_string_append_printf(style_string,"\n\t%s = \"%s\"",prefbase,pref);
        g_free(prefbase);
      }
    }
  }
  for (i = 0; i < sizeof(widget_size_prefs) / sizeof(widget_size_prefs[0]); i++) {
    if (purple_prefs_get_bool(widget_size_prefs_set[i]) != 0) {
      prefbase = g_path_get_basename(widget_size_prefs[i]);
      g_string_append_printf(style_string,"\n\t%s = %d",prefbase,purple_prefs_get_int(widget_size_prefs[i]));
      g_free(prefbase);
    }
  }
/*
	for (i = 0; i < G_N_ELEMENTS(widget_bool_prefs); i++) {
		if (purple_prefs_get_bool(widget_bool_prefs_set[i])) {
			prefbase = g_path_get_basename(widget_bool_prefs[i]);
			g_string_append_printf(style_string,
			                       "\t%s = %d\n", prefbase,
			                       purple_prefs_get_bool(widget_bool_prefs[i]));
			g_free(prefbase);
		}
	}
	*/
  g_string_append(style_string,"\n}\nwidget_class \"*\" style \"purplerc_style\"\n");
  for (i = 0; i < sizeof(font_prefs) / sizeof(font_prefs[0]); i++) {
    if (purple_prefs_get_bool(font_prefs_set[i]) != 0) {
      const gchar *pref;
      pref = purple_prefs_get_string(font_prefs[i]);
      if ((pref != ((const gchar *)((void *)0))) && (strcmp(pref,"") != 0)) {
        prefbase = g_path_get_basename(font_prefs[i]);
        g_string_append_printf(style_string,"style \"%s_style\"\n{\n\tfont_name = \"%s\"\n}\nwidget \"%s\" style \"%s_style\"\n",prefbase,pref,prefbase,prefbase);
        g_free(prefbase);
      }
    }
  }
  return style_string;
}

static void purplerc_make_changes()
{
  GString *str = make_gtkrc_string();
  GtkSettings *setting = (GtkSettings *)((void *)0);
  gtk_rc_parse_string((str -> str));
  g_string_free(str,(!0));
  setting = gtk_settings_get_default();
  gtk_rc_reset_styles(setting);
}

static void purplerc_write(GtkWidget *widget,gpointer data)
{
  GString *str = make_gtkrc_string();
  str = g_string_prepend(str,"# This file automatically written by the Pidgin GTK+ Theme Control plugin.\n# Any changes to this file will be overwritten by the plugin when told to\n# write the settings again.\n# The FAQ (http://developer.pidgin.im/wiki/FAQ) contains some further examples\n# of possible pidgin gtkrc settings.\n");
  purple_util_write_data_to_file("gtkrc-2.0",(str -> str),(-1));
  g_string_free(str,(!0));
}

static void purplerc_reread(GtkWidget *widget,gpointer data)
{
  gtk_rc_reparse_all();
/* I don't know if this is necessary but if not it shouldn't hurt. */
  purplerc_make_changes();
}

static void purplerc_pref_changed_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  purplerc_make_changes();
}

static void purplerc_color_response(GtkDialog *color_dialog,gint response,gpointer data)
{
  gint subscript = (gint )((glong )data);
  if (response == GTK_RESPONSE_OK) {
    GdkColor color;
    gchar colorstr[8UL];
#if GTK_CHECK_VERSION(2,14,0)
    GtkWidget *colorsel = gtk_color_selection_dialog_get_color_selection(((GtkColorSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)color_dialog),gtk_color_selection_dialog_get_type()))));
#else
#endif
    gtk_color_selection_get_current_color(((GtkColorSelection *)(g_type_check_instance_cast(((GTypeInstance *)colorsel),gtk_color_selection_get_type()))),&color);
    g_snprintf(colorstr,(sizeof(colorstr)),"#%02X%02X%02X",(color.red / 256),(color.green / 256),(color.blue / 256));
    purple_prefs_set_string(color_prefs[subscript],colorstr);
  }
  gtk_widget_destroy(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)color_dialog),gtk_widget_get_type()))));
}

static void purplerc_set_color(GtkWidget *widget,gpointer data)
{
  GdkColor color;
  gchar title[128UL];
  const gchar *pref = (const gchar *)((void *)0);
  GtkWidget *color_dialog = (GtkWidget *)((void *)0);
  gint subscript = (gint )((glong )data);
  g_snprintf(title,(sizeof(title)),((const char *)(dgettext("pidgin","Select Color for %s"))),((const char *)(dgettext("pidgin",color_names[(gint )((glong )data)]))));
  color_dialog = gtk_color_selection_dialog_new(((const char *)(dgettext("pidgin","Select Color"))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)color_dialog),((GType )(20 << 2))))),"response",((GCallback )purplerc_color_response),data,0,((GConnectFlags )0));
  pref = purple_prefs_get_string(color_prefs[subscript]);
  if ((pref != ((const gchar *)((void *)0))) && (strcmp(pref,"") != 0)) {
    if (gdk_color_parse(pref,&color) != 0) {
#if GTK_CHECK_VERSION(2,14,0)
      gtk_color_selection_set_current_color(((GtkColorSelection *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_color_selection_dialog_get_color_selection(((GtkColorSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)color_dialog),gtk_color_selection_dialog_get_type())))))),gtk_color_selection_get_type()))),(&color));
#else
#endif
    }
  }
  gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)color_dialog),gtk_window_get_type()))));
}

static void purplerc_font_response(GtkDialog *font_dialog,gint response,gpointer data)
{
  const gchar *prefpath;
  gint subscript = (gint )((glong )data);
  if (response == GTK_RESPONSE_OK) {
    gchar *fontname = (gchar *)((void *)0);
    if (subscript == -1) {
      prefpath = "/plugins/gtk/purplerc/gtk-font-name";
    }
    else {
      prefpath = font_prefs[subscript];
    }
    fontname = gtk_font_selection_dialog_get_font_name(((GtkFontSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)font_dialog),gtk_font_selection_dialog_get_type()))));
    purple_prefs_set_string(prefpath,fontname);
    g_free(fontname);
  }
  gtk_widget_destroy(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)font_dialog),gtk_widget_get_type()))));
}

static void purplerc_set_font(GtkWidget *widget,gpointer data)
{
  gchar title[128UL];
  GtkWidget *font_dialog = (GtkWidget *)((void *)0);
  gint subscript = (gint )((glong )data);
  const gchar *pref = (const gchar *)((void *)0);
  const gchar *prefpath = (const gchar *)((void *)0);
  if (subscript == -1) {
    g_snprintf(title,(sizeof(title)),((const char *)(dgettext("pidgin","Select Interface Font"))));
    prefpath = "/plugins/gtk/purplerc/gtk-font-name";
  }
  else {
    g_snprintf(title,(sizeof(title)),((const char *)(dgettext("pidgin","Select Font for %s"))),((const char *)(dgettext("pidgin",font_names[subscript]))));
    prefpath = font_prefs[subscript];
  }
  font_dialog = gtk_font_selection_dialog_new(title);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)font_dialog),((GType )(20 << 2))))),"response",((GCallback )purplerc_font_response),data,0,((GConnectFlags )0));
  pref = purple_prefs_get_string(prefpath);
  if ((pref != ((const gchar *)((void *)0))) && (strcmp(pref,"") != 0)) {
    gtk_font_selection_dialog_set_font_name(((GtkFontSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)font_dialog),gtk_font_selection_dialog_get_type()))),pref);
  }
  gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)font_dialog),gtk_window_get_type()))));
}

static gboolean purplerc_plugin_load(PurplePlugin *plugin)
{
  purplerc_make_changes();
  pref_callback = purple_prefs_connect_callback(plugin,"/plugins/gtk/purplerc",purplerc_pref_changed_cb,0);
  return (!0);
}

static gboolean purplerc_plugin_unload(PurplePlugin *plugin)
{
  purple_prefs_disconnect_callback(pref_callback);
  return (!0);
}

static GtkWidget *purplerc_make_interface_vbox()
{
  GtkWidget *vbox = (GtkWidget *)((void *)0);
  GtkWidget *hbox = (GtkWidget *)((void *)0);
  GtkWidget *check = (GtkWidget *)((void *)0);
  GtkSizeGroup *labelsg = (GtkSizeGroup *)((void *)0);
  gint i;
  vbox = gtk_vbox_new(0,18);
  labelsg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),12);
  for (i = 0; i < sizeof(color_prefs) / sizeof(color_prefs[0]); i++) {
    hbox = gtk_hbox_new(0,18);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
    check = pidgin_prefs_checkbox(((const char *)(dgettext("pidgin",color_names[i]))),color_prefs_set[i],hbox);
    gtk_size_group_add_widget(labelsg,check);
    color_widgets[i] = pidgin_pixbuf_button_from_stock("","gtk-select-color",PIDGIN_BUTTON_HORIZONTAL);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),color_widgets[i],0,0,0);
    gtk_widget_set_sensitive(color_widgets[i],purple_prefs_get_bool(color_prefs_set[i]));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)check),((GType )(20 << 2))))),"toggled",((GCallback )pidgin_toggle_sensitive),color_widgets[i],0,((GConnectFlags )0));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)color_widgets[i]),((GType )(20 << 2))))),"clicked",((GCallback )purplerc_set_color),((gpointer )((glong )i)),0,((GConnectFlags )0));
  }
  g_object_unref(labelsg);
  return vbox;
}

static GtkWidget *purplerc_make_fonts_vbox()
{
  GtkWidget *vbox = (GtkWidget *)((void *)0);
  GtkWidget *hbox = (GtkWidget *)((void *)0);
  GtkWidget *check = (GtkWidget *)((void *)0);
  GtkWidget *widget = (GtkWidget *)((void *)0);
  GtkSizeGroup *labelsg = (GtkSizeGroup *)((void *)0);
  int i;
  vbox = gtk_vbox_new(0,18);
  labelsg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),12);
  hbox = gtk_hbox_new(0,18);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  check = pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","GTK+ Interface Font"))),"/plugins/gtk/purplerc/set/gtk-font-name",hbox);
  gtk_size_group_add_widget(labelsg,check);
  widget = pidgin_pixbuf_button_from_stock("","gtk-select-font",PIDGIN_BUTTON_HORIZONTAL);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),widget,0,0,0);
  gtk_widget_set_sensitive(widget,purple_prefs_get_bool("/plugins/gtk/purplerc/set/gtk-font-name"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)check),((GType )(20 << 2))))),"toggled",((GCallback )pidgin_toggle_sensitive),widget,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"clicked",((GCallback )purplerc_set_font),((gpointer )((gpointer )((glong )(-1)))),0,((GConnectFlags )0));
  for (i = 0; i < sizeof(font_prefs) / sizeof(font_prefs[0]); i++) {
    hbox = gtk_hbox_new(0,18);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
    check = pidgin_prefs_checkbox(((const char *)(dgettext("pidgin",font_names[i]))),font_prefs_set[i],hbox);
    gtk_size_group_add_widget(labelsg,check);
    font_widgets[i] = pidgin_pixbuf_button_from_stock("","gtk-select-font",PIDGIN_BUTTON_HORIZONTAL);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),font_widgets[i],0,0,0);
    gtk_widget_set_sensitive(font_widgets[i],purple_prefs_get_bool(font_prefs_set[i]));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)check),((GType )(20 << 2))))),"toggled",((GCallback )pidgin_toggle_sensitive),font_widgets[i],0,((GConnectFlags )0));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)font_widgets[i]),((GType )(20 << 2))))),"clicked",((GCallback )purplerc_set_font),((gpointer )((glong )i)),0,((GConnectFlags )0));
  }
  g_object_unref(labelsg);
  return vbox;
}

static GtkWidget *purplerc_make_misc_vbox()
{
/* Note: Intentionally not using the size group argument to the
	 * pidgin_prefs_labeled_* functions they only add the text label to
	 * the size group not the whole thing, which isn't what I want. */
  GtkWidget *vbox = (GtkWidget *)((void *)0);
  GtkWidget *hbox = (GtkWidget *)((void *)0);
  GtkWidget *check = (GtkWidget *)((void *)0);
  GtkWidget *widget = (GtkWidget *)((void *)0);
  GtkSizeGroup *labelsg = (GtkSizeGroup *)((void *)0);
  int i;
  vbox = gtk_vbox_new(0,18);
  labelsg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),12);
  hbox = gtk_hbox_new(0,18);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  check = pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","GTK+ Text Shortcut Theme"))),"/plugins/gtk/purplerc/set/gtk-key-theme-name",hbox);
  gtk_size_group_add_widget(labelsg,check);
  widget = pidgin_prefs_labeled_entry(hbox,"","/plugins/gtk/purplerc/gtk-key-theme-name",0);
  gtk_widget_set_sensitive(widget,purple_prefs_get_bool("/plugins/gtk/purplerc/set/gtk-key-theme-name"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)check),((GType )(20 << 2))))),"toggled",((GCallback )pidgin_toggle_sensitive),widget,0,((GConnectFlags )0));
  for (i = 0; i < sizeof(widget_size_prefs) / sizeof(widget_size_prefs[0]); i++) {
    hbox = gtk_hbox_new(0,18);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
    check = pidgin_prefs_checkbox(((const char *)(dgettext("pidgin",widget_size_names[i]))),widget_size_prefs_set[i],hbox);
    gtk_size_group_add_widget(labelsg,check);
    widget_size_widgets[i] = pidgin_prefs_labeled_spin_button(hbox,"",widget_size_prefs[i],0,50,0);
    gtk_widget_set_sensitive(widget_size_widgets[i],purple_prefs_get_bool(widget_size_prefs_set[i]));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)check),((GType )(20 << 2))))),"toggled",((GCallback )pidgin_toggle_sensitive),widget_size_widgets[i],0,((GConnectFlags )0));
  }
  hbox = gtk_hbox_new(0,18);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  check = pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","Disable Typing Notification Text"))),"/plugins/gtk/purplerc/set/disable-typing-notification",hbox);
/* Widget boolean stuff */
/*
	for (i = 0; i < G_N_ELEMENTS(widget_bool_prefs); i++) {
		hbox = gtk_hbox_new(FALSE, PIDGIN_HIG_CAT_SPACE);
		gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
		check = pidgin_prefs_checkbox(_(widget_bool_names[i]),
		                              widget_bool_prefs_set[i], hbox);
		gtk_size_group_add_widget(labelsg, check);
		widget_bool_widgets[i] = pidgin_prefs_checkbox("", widget_bool_prefs[i], hbox);
		gtk_widget_set_sensitive(widget_bool_widgets[i],
		                         purple_prefs_get_bool(widget_bool_prefs_set[i]));
		g_signal_connect(G_OBJECT(check), "toggled",
		                 G_CALLBACK(pidgin_toggle_sensitive),
		                 widget_bool_widgets[i]);
	}
	*/
  g_object_unref(labelsg);
  return vbox;
}

static GtkWidget *purplerc_get_config_frame(PurplePlugin *plugin)
{
  gchar *tmp;
  GtkWidget *check = (GtkWidget *)((void *)0);
  GtkWidget *label = (GtkWidget *)((void *)0);
  GtkWidget *ret = (GtkWidget *)((void *)0);
  GtkWidget *hbox = (GtkWidget *)((void *)0);
  GtkWidget *frame = (GtkWidget *)((void *)0);
  GtkWidget *note = (GtkWidget *)((void *)0);
#ifndef _WIN32
  const gchar *homepath = "$HOME";
#else
#endif
  ret = gtk_vbox_new(0,18);
  note = gtk_notebook_new();
  label = gtk_label_new(0);
  hbox = gtk_hbox_new(0,18);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_container_get_type()))),12);
  tmp = g_strdup_printf("<span weight=\"bold\">%s</span>",((const char *)(dgettext("pidgin","GTK+ Theme Control Settings"))));
  gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),tmp);
  g_free(tmp);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_box_get_type()))),hbox,0,0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_box_get_type()))),note,0,0,0);
  label = gtk_label_new(((const char *)(dgettext("pidgin","Colors"))));
  gtk_notebook_insert_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)note),gtk_notebook_get_type()))),purplerc_make_interface_vbox(),label,(-1));
  label = gtk_label_new(((const char *)(dgettext("pidgin","Fonts"))));
  gtk_notebook_insert_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)note),gtk_notebook_get_type()))),purplerc_make_fonts_vbox(),label,(-1));
  label = gtk_label_new(((const char *)(dgettext("pidgin","Miscellaneous"))));
  gtk_notebook_insert_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)note),gtk_notebook_get_type()))),purplerc_make_misc_vbox(),label,(-1));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_box_get_type()))),gtk_hseparator_new(),(!0),(!0),0);
  frame = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Gtkrc File Tools"))));
  hbox = gtk_hbox_new(0,18);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)frame),gtk_box_get_type()))),hbox,0,0,0);
  tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Write settings to %s%sgtkrc-2.0"))),homepath,"/.purple/");
  check = gtk_button_new_with_label(tmp);
  g_free(tmp);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),check,(!0),(!0),0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)check),((GType )(20 << 2))))),"clicked",((GCallback )purplerc_write),0,0,((GConnectFlags )0));
  check = gtk_button_new_with_label(((const char *)(dgettext("pidgin","Re-read gtkrc files"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),check,(!0),(!0),0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)check),((GType )(20 << 2))))),"clicked",((GCallback )purplerc_reread),0,0,((GConnectFlags )0));
  gtk_widget_show_all(ret);
  return ret;
}
static PidginPluginUiInfo purplerc_ui_info = {(purplerc_get_config_frame), (0), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* page_num (Reserved) */
/* padding */
};
static PurplePluginInfo purplerc_info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gtk-gaim"), (0), ((GList *)((void *)0)), (0), ("purplerc"), ("Pidgin GTK+ Theme Control"), ("2.10.9"), ("Provides access to commonly used gtkrc settings."), ("Provides access to commonly used gtkrc settings."), ("Etan Reisner <deryni@pidgin.im>"), ("http://pidgin.im/"), (purplerc_plugin_load), (purplerc_plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((&purplerc_ui_info)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* padding */
};

static void purplerc_init(PurplePlugin *plugin)
{
  gint i;
  purple_prefs_add_none("/plugins");
  purple_prefs_add_none("/plugins/gtk");
  purple_prefs_add_none("/plugins/gtk/purplerc");
  purple_prefs_add_none("/plugins/gtk/purplerc/set");
  purple_prefs_add_string("/plugins/gtk/purplerc/gtk-font-name","");
  purple_prefs_add_bool("/plugins/gtk/purplerc/set/gtk-font-name",0);
  purple_prefs_add_string("/plugins/gtk/purplerc/gtk-key-theme-name","");
  purple_prefs_add_bool("/plugins/gtk/purplerc/set/gtk-key-theme-name",0);
  purple_prefs_add_none("/plugins/gtk/purplerc/color");
  purple_prefs_add_none("/plugins/gtk/purplerc/set/color");
  for (i = 0; i < sizeof(color_prefs) / sizeof(color_prefs[0]); i++) {
    purple_prefs_add_string(color_prefs[i],"");
    purple_prefs_add_bool(color_prefs_set[i],0);
  }
  purple_prefs_add_none("/plugins/gtk/purplerc/size");
  purple_prefs_add_none("/plugins/gtk/purplerc/set/size");
  for (i = 0; i < sizeof(widget_size_prefs) / sizeof(widget_size_prefs[0]); i++) {
    purple_prefs_add_int(widget_size_prefs[i],0);
    purple_prefs_add_bool(widget_size_prefs_set[i],0);
  }
  purple_prefs_add_none("/plugins/gtk/purplerc/font");
  purple_prefs_add_none("/plugins/gtk/purplerc/set/font");
  for (i = 0; i < sizeof(font_prefs) / sizeof(font_prefs[0]); i++) {
    purple_prefs_add_string(font_prefs[i],"");
    purple_prefs_add_bool(font_prefs_set[i],0);
  }
/*
	purple_prefs_add_none("/plugins/gtk/purplerc/bool");
	purple_prefs_add_none("/plugins/gtk/purplerc/set/bool");
	for (i = 0; i < G_N_ELEMENTS(widget_bool_prefs); i++) {
		purple_prefs_add_bool(widget_bool_prefs[i], TRUE);
		purple_prefs_add_bool(widget_bool_prefs_set[i], FALSE);
	}
	*/
  purple_prefs_add_bool("/plugins/gtk/purplerc/disable-typing-notification",0);
  purple_prefs_add_bool("/plugins/gtk/purplerc/set/disable-typing-notification",0);
/* remove old cursor color prefs */
  purple_prefs_remove("/plugins/gtk/purplerc/color/GtkWidget::cursor-color");
  purple_prefs_remove("/plugins/gtk/purplerc/color/GtkWidget::secondary-cursor-color");
  purple_prefs_remove("/plugins/gtk/purplerc/set/color/GtkWidget::cursor-color");
  purple_prefs_remove("/plugins/gtk/purplerc/set/color/GtkWidget::secondary-cursor-color");
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &purplerc_info;
  purplerc_init(plugin);
  return purple_plugin_register(plugin);
}
