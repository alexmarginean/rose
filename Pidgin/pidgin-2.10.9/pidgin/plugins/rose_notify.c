/*
 * Purple buddy notification plugin.
 *
 * Copyright (C) 2000-2001, Eric Warmenhoven (original code)
 * Copyright (C) 2002, Etan Reisner <deryni@eden.rutgers.edu> (rewritten code)
 * Copyright (C) 2003, Christian Hammond (update for changed API)
 * Copyright (C) 2003, Brian Tarricone <bjt23@cornell.edu> (mostly rewritten)
 * Copyright (C) 2003, Mark Doliner (minor cleanup)
 * Copyright (C) 2003, Etan Reisner (largely rewritten again)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
/* TODO
 * 22:22:17 <seanegan> deryni: speaking of notify.c... you know what else
 * might be a neat feature?
 * 22:22:30 <seanegan> Changing the window icon.
 * 22:23:25 <deryni> seanegan: To what?
 * 22:23:42 <seanegan> deryni: I dunno. Flash it between the regular icon and
 * blank or something.
 * 22:23:53 <deryni> Also I think purple might re-set that sort of frequently,
 * but I'd have to look.
 * 22:25:16 <seanegan> deryni: I keep my conversations in one workspace and am
 * frequently in an another, and the icon flashing in the pager would be a
 * neat visual clue.
 */
/*
 * From Etan, 2002:
 *  -Added config dialog
 *  -Added control over notification method
 *  -Added control over when to release notification
 *
 *  -Added option to get notification for chats also
 *  -Cleaned up code
 *  -Added option to notify on click as it's own option
 *   rather then as what happens when on focus isn't clicked
 *  -Added apply button to change the denotification methods for
 *   open conversation windows
 *  -Fixed apply to conversations, count now keeps count across applies
 *  -Fixed(?) memory leak, and in the process fixed some stupidities
 *  -Hit enter when done editing the title string entry box to save it
 *
 * Thanks to Carles Pina i Estany <carles@pinux.info>
 *   for count of new messages option
 *
 * From Brian, 20 July 2003:
 *  -Use new xml prefs
 *  -Better handling of notification states tracking
 *  -Better pref change handling
 *  -Fixed a possible memleak and possible crash (rare)
 *  -Use gtk_window_get_title() rather than gtkwin->title
 *  -Other random fixes and cleanups
 *
 * Etan again, 12 August 2003:
 *  -Better use of the new xml prefs
 *  -Removed all bitmask stuff
 *  -Even better pref change handling
 *  -Removed unnecessary functions
 *  -Reworking of notification/unnotification stuff
 *  -Header file include cleanup
 *  -General code cleanup
 *
 * Etan yet again, 04 April 2004:
 *  -Re-added Urgent option
 *  -Re-added unnotify on focus option (still needs work, as it will only
 *  react to focus-in events when the entry or history widgets are focused)
 *
 * Sean, 08 January, 2005:
 *  -Added Raise option, formally in Purple proper
 */
#include "internal.h"
#include "pidgin.h"
#include "gtkprefs.h"
#include "conversation.h"
#include "prefs.h"
#include "signals.h"
#include "version.h"
#include "debug.h"
#include "gtkplugin.h"
#include "gtkutils.h"
#define NOTIFY_PLUGIN_ID "gtk-x11-notify"
static PurplePlugin *my_plugin = (PurplePlugin *)((void *)0);
#ifdef HAVE_X11
static GdkAtom _Cardinal = (GdkAtom )((GdkAtom )((gpointer )((gulong )0)));
static GdkAtom _PurpleUnseenCount = (GdkAtom )((GdkAtom )((gpointer )((gulong )0)));
#endif
/* notification set/unset */
static int notify(PurpleConversation *conv,gboolean increment);
static void notify_win(PidginWindow *purplewin,PurpleConversation *conv);
static void unnotify(PurpleConversation *conv,gboolean reset);
static int unnotify_cb(GtkWidget *widget,gpointer data,PurpleConversation *conv);
/* gtk widget callbacks for prefs panel */
static void type_toggle_cb(GtkWidget *widget,gpointer data);
static void method_toggle_cb(GtkWidget *widget,gpointer data);
static void notify_toggle_cb(GtkWidget *widget,gpointer data);
static gboolean options_entry_cb(GtkWidget *widget,GdkEventFocus *event,gpointer data);
static void apply_method();
static void apply_notify();
/* string function */
static void handle_string(PidginWindow *purplewin);
/* count_title function */
static void handle_count_title(PidginWindow *purplewin);
/* count_xprop function */
static void handle_count_xprop(PidginWindow *purplewin);
/* urgent function */
static void handle_urgent(PidginWindow *purplewin,gboolean set);
/* raise function */
static void handle_raise(PidginWindow *purplewin);
/* present function */
static void handle_present(PurpleConversation *conv);
/****************************************/
/* Begin doing stuff below this line... */
/****************************************/

static guint count_messages(PidginWindow *purplewin)
{
  guint count = 0;
  GList *convs = (GList *)((void *)0);
  GList *l;
  for (convs = (purplewin -> gtkconvs); convs != ((GList *)((void *)0)); convs = (convs -> next)) {
    PidginConversation *conv = (convs -> data);
    for (l = (conv -> convs); l != ((GList *)((void *)0)); l = (l -> next)) {
      count += ((gint )((glong )(purple_conversation_get_data((l -> data),"notify-message-count"))));
    }
  }
  return count;
}

static int notify(PurpleConversation *conv,gboolean increment)
{
  gint count;
  gboolean has_focus;
  PidginWindow *purplewin = (PidginWindow *)((void *)0);
  if ((conv == ((PurpleConversation *)((void *)0))) || (((PidginConversation *)(conv -> ui_data)) == ((PidginConversation *)((void *)0)))) 
    return 0;
/* We want to remove the notifications, but not reset the counter */
  unnotify(conv,0);
  purplewin = ( *((PidginConversation *)(conv -> ui_data))).win;
/* If we aren't doing notifications for this type of conversation, return */
  if ((((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) && !(purple_prefs_get_bool("/plugins/gtk/X11/notify/type_im") != 0)) || (((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) && !(purple_prefs_get_bool("/plugins/gtk/X11/notify/type_chat") != 0))) 
    return 0;
  g_object_get(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(purplewin -> window)),((GType )(20 << 2))))),"has-toplevel-focus",&has_focus,((void *)((void *)0)));
  if ((purple_prefs_get_bool("/plugins/gtk/X11/notify/type_focused") != 0) || !(has_focus != 0)) {
    if (increment != 0) {
      count = ((gint )((glong )(purple_conversation_get_data(conv,"notify-message-count"))));
      count++;
      purple_conversation_set_data(conv,"notify-message-count",((gpointer )((glong )count)));
    }
    notify_win(purplewin,conv);
  }
  return 0;
}

static void notify_win(PidginWindow *purplewin,PurpleConversation *conv)
{
  if (count_messages(purplewin) <= 0) 
    return ;
  if (purple_prefs_get_bool("/plugins/gtk/X11/notify/method_count") != 0) 
    handle_count_title(purplewin);
  if (purple_prefs_get_bool("/plugins/gtk/X11/notify/method_count_xprop") != 0) 
    handle_count_xprop(purplewin);
  if (purple_prefs_get_bool("/plugins/gtk/X11/notify/method_string") != 0) 
    handle_string(purplewin);
  if (purple_prefs_get_bool("/plugins/gtk/X11/notify/method_urgent") != 0) 
    handle_urgent(purplewin,(!0));
  if (purple_prefs_get_bool("/plugins/gtk/X11/notify/method_raise") != 0) 
    handle_raise(purplewin);
  if (purple_prefs_get_bool("/plugins/gtk/X11/notify/method_present") != 0) 
    handle_present(conv);
}

static void unnotify(PurpleConversation *conv,gboolean reset)
{
  PurpleConversation *active_conv = (PurpleConversation *)((void *)0);
  PidginWindow *purplewin = (PidginWindow *)((void *)0);
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  if (((PidginConversation *)(conv -> ui_data)) == ((PidginConversation *)((void *)0))) 
    return ;
  purplewin = ( *((PidginConversation *)(conv -> ui_data))).win;
  active_conv = pidgin_conv_window_get_active_conversation(purplewin);
/* reset the conversation window title */
  purple_conversation_autoset_title(active_conv);
  if (reset != 0) {
/* Only need to actually remove the urgent hinting here, since
		 * removing it just to have it readded in re-notify is an
		 * unnecessary couple extra RTs to the server */
    handle_urgent(purplewin,0);
    purple_conversation_set_data(conv,"notify-message-count",0);
/* Same logic as for the urgent hint, xprops are also a RT.
		 * This needs to go here so that it gets the updated message
		 * count. */
    handle_count_xprop(purplewin);
  }
}

static int unnotify_cb(GtkWidget *widget,gpointer data,PurpleConversation *conv)
{
  if (((gint )((glong )(purple_conversation_get_data(conv,"notify-message-count")))) != 0) 
    unnotify(conv,(!0));
  return 0;
}

static gboolean message_displayed_cb(PurpleAccount *account,const char *who,char *message,PurpleConversation *conv,PurpleMessageFlags flags)
{
  if ((((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) && (purple_prefs_get_bool("/plugins/gtk/X11/notify/type_chat_nick") != 0)) && !((flags & PURPLE_MESSAGE_NICK) != 0U)) 
    return 0;
  if (((flags & PURPLE_MESSAGE_RECV) != 0U) && !((flags & PURPLE_MESSAGE_DELAYED) != 0U)) 
    notify(conv,(!0));
  return 0;
}

static void im_sent_im(PurpleAccount *account,const char *receiver,const char *message)
{
  PurpleConversation *conv = (PurpleConversation *)((void *)0);
  if (purple_prefs_get_bool("/plugins/gtk/X11/notify/notify_send") != 0) {
    conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,receiver,account);
    unnotify(conv,(!0));
  }
}

static void chat_sent_im(PurpleAccount *account,const char *message,int id)
{
  PurpleConversation *conv = (PurpleConversation *)((void *)0);
  if (purple_prefs_get_bool("/plugins/gtk/X11/notify/notify_send") != 0) {
    conv = purple_find_chat((purple_account_get_connection(account)),id);
    unnotify(conv,(!0));
  }
}

static int attach_signals(PurpleConversation *conv)
{
  PidginConversation *gtkconv = (PidginConversation *)((void *)0);
  GSList *imhtml_ids = (GSList *)((void *)0);
  GSList *entry_ids = (GSList *)((void *)0);
  guint id;
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  if (!(gtkconv != 0)) {
    purple_debug_misc("notify","Failed to find gtkconv\n");
    return 0;
  }
  if (purple_prefs_get_bool("/plugins/gtk/X11/notify/notify_focus") != 0) {
/* TODO should really find a way to make this work no matter
		 * where the focus is inside the conv window, without having
		 * to bind to focus-in-event on the g(d|t)kwindow */
/* try setting the signal on the focus-in-event for
		 * gtkwin->notebook->container? */
    id = (g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),((GType )(20 << 2))))),"focus-in-event",((GCallback )unnotify_cb),conv,0,((GConnectFlags )0)));
    entry_ids = g_slist_append(entry_ids,((gpointer )((gulong )id)));
    id = (g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),((GType )(20 << 2))))),"focus-in-event",((GCallback )unnotify_cb),conv,0,((GConnectFlags )0)));
    imhtml_ids = g_slist_append(imhtml_ids,((gpointer )((gulong )id)));
  }
  if (purple_prefs_get_bool("/plugins/gtk/X11/notify/notify_click") != 0) {
/* TODO similarly should really find a way to allow for
		 * clicking in other places of the window */
    id = (g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),((GType )(20 << 2))))),"button-press-event",((GCallback )unnotify_cb),conv,0,((GConnectFlags )0)));
    entry_ids = g_slist_append(entry_ids,((gpointer )((gulong )id)));
    id = (g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),((GType )(20 << 2))))),"button-press-event",((GCallback )unnotify_cb),conv,0,((GConnectFlags )0)));
    imhtml_ids = g_slist_append(imhtml_ids,((gpointer )((gulong )id)));
  }
  if (purple_prefs_get_bool("/plugins/gtk/X11/notify/notify_type") != 0) {
    id = (g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),((GType )(20 << 2))))),"key-press-event",((GCallback )unnotify_cb),conv,0,((GConnectFlags )0)));
    entry_ids = g_slist_append(entry_ids,((gpointer )((gulong )id)));
  }
  purple_conversation_set_data(conv,"notify-imhtml-signals",imhtml_ids);
  purple_conversation_set_data(conv,"notify-entry-signals",entry_ids);
  return 0;
}

static void detach_signals(PurpleConversation *conv)
{
  PidginConversation *gtkconv = (PidginConversation *)((void *)0);
  GSList *ids = (GSList *)((void *)0);
  GSList *l;
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  if (!(gtkconv != 0)) 
    return ;
  ids = (purple_conversation_get_data(conv,"notify-imhtml-signals"));
  for (l = ids; l != ((GSList *)((void *)0)); l = (l -> next)) 
    g_signal_handler_disconnect((gtkconv -> imhtml),((gint )((glong )(l -> data))));
  g_slist_free(ids);
  ids = (purple_conversation_get_data(conv,"notify-entry-signals"));
  for (l = ids; l != ((GSList *)((void *)0)); l = (l -> next)) 
    g_signal_handler_disconnect((gtkconv -> entry),((gint )((glong )(l -> data))));
  g_slist_free(ids);
  purple_conversation_set_data(conv,"notify-message-count",0);
  purple_conversation_set_data(conv,"notify-imhtml-signals",0);
  purple_conversation_set_data(conv,"notify-entry-signals",0);
}

static void conv_created(PurpleConversation *conv)
{
  purple_conversation_set_data(conv,"notify-message-count",0);
/* always attach the signals, notify() will take care of conversation
	 * type checking */
  attach_signals(conv);
}

static void conv_switched(PurpleConversation *conv)
{
#if 0
#endif
/*
	 * If the conversation was switched, then make sure we re-notify
	 * because Purple will have overwritten our custom window title.
	 */
  notify(conv,0);
#if 0
/* if we don't have notification on the window then we don't want to
		 * re-notify it */
#endif
}

static void deleting_conv(PurpleConversation *conv)
{
  PidginWindow *purplewin = (PidginWindow *)((void *)0);
  PidginConversation *gtkconv = (PidginConversation *)(conv -> ui_data);
  if (gtkconv == ((PidginConversation *)((void *)0))) 
    return ;
  detach_signals(conv);
  purplewin = (gtkconv -> win);
  handle_urgent(purplewin,0);
  purple_conversation_set_data(conv,"notify-message-count",0);
#if 0
/* i think this line crashes */
#endif
}
#if 0
/*
			purple_conversation_autoset_title(active_conv);
			handle_urgent(new_purplewin, FALSE);
				*/
/*
			PurpleConversation *old_active_conv = NULL;
			old_active_conv = purple_conv_window_get_active_conversation(new_purplewin);
			purple_conversation_autoset_title(old_active_conv);
			handle_urgent(old_purplewin, FALSE);
			if (count_messages(old_purplewin))
				notify_win(old_purplewin);
			purple_conversation_autoset_title(active_conv);
			handle_urgent(new_purplewin, FALSE);
			if (count_messages(new_purplewin))
				notify_win(new_purplewin);
				*/
/*
		purple_conversation_autoset_title(active_conv);
		handle_urgent(old_purplewin, FALSE);
		if (count_messages(old_purplewin))
			notify_win(old_purplewin);
			*/
#endif

static void handle_string(PidginWindow *purplewin)
{
  GtkWindow *window = (GtkWindow *)((void *)0);
  gchar newtitle[256UL];
  do {
    if (purplewin != ((PidginWindow *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purplewin != NULL");
      return ;
    };
  }while (0);
  window = ((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(purplewin -> window)),gtk_window_get_type())));
  do {
    if (window != ((GtkWindow *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"window != NULL");
      return ;
    };
  }while (0);
  g_snprintf(newtitle,(sizeof(newtitle)),"%s%s",purple_prefs_get_string("/plugins/gtk/X11/notify/title_string"),gtk_window_get_title(window));
  gtk_window_set_title(window,newtitle);
}

static void handle_count_title(PidginWindow *purplewin)
{
  GtkWindow *window;
  char newtitle[256UL];
  do {
    if (purplewin != ((PidginWindow *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purplewin != NULL");
      return ;
    };
  }while (0);
  window = ((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(purplewin -> window)),gtk_window_get_type())));
  do {
    if (window != ((GtkWindow *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"window != NULL");
      return ;
    };
  }while (0);
  g_snprintf(newtitle,(sizeof(newtitle)),"[%d] %s",count_messages(purplewin),gtk_window_get_title(window));
  gtk_window_set_title(window,newtitle);
}

static void handle_count_xprop(PidginWindow *purplewin)
{
#ifdef HAVE_X11
  guint count;
  GtkWidget *window;
  GdkWindow *gdkwin;
  window = (purplewin -> window);
  do {
    if (window != ((GtkWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"window != NULL");
      return ;
    };
  }while (0);
  if (_PurpleUnseenCount == ((GdkAtom )((GdkAtom )((gpointer )((gulong )0))))) {
    _PurpleUnseenCount = gdk_atom_intern("_PIDGIN_UNSEEN_COUNT",0);
  }
  if (_Cardinal == ((GdkAtom )((GdkAtom )((gpointer )((gulong )0))))) {
    _Cardinal = gdk_atom_intern("CARDINAL",0);
  }
  count = count_messages(purplewin);
#if GTK_CHECK_VERSION(2,14,0)
  gdkwin = gtk_widget_get_window(window);
#else
#endif
  gdk_property_change(gdkwin,_PurpleUnseenCount,_Cardinal,32,GDK_PROP_MODE_REPLACE,((guchar *)(&count)),1);
#endif
}

static void handle_urgent(PidginWindow *purplewin,gboolean set)
{
  do {
    if (purplewin != ((PidginWindow *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purplewin != NULL");
      return ;
    };
  }while (0);
  do {
    if ((purplewin -> window) != ((GtkWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purplewin->window != NULL");
      return ;
    };
  }while (0);
  pidgin_set_urgent(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(purplewin -> window)),gtk_window_get_type()))),set);
}

static void handle_raise(PidginWindow *purplewin)
{
  pidgin_conv_window_raise(purplewin);
}

static void handle_present(PurpleConversation *conv)
{
  if (pidgin_conv_is_hidden(((PidginConversation *)(conv -> ui_data))) != 0) 
    return ;
  purple_conversation_present(conv);
}

static void type_toggle_cb(GtkWidget *widget,gpointer data)
{
  gboolean on = gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_toggle_button_get_type()))));
  gchar pref[256UL];
  g_snprintf(pref,(sizeof(pref)),"/plugins/gtk/X11/notify/%s",((char *)data));
  purple_prefs_set_bool(pref,on);
}

static void method_toggle_cb(GtkWidget *widget,gpointer data)
{
  gboolean on = gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_toggle_button_get_type()))));
  gchar pref[256UL];
  g_snprintf(pref,(sizeof(pref)),"/plugins/gtk/X11/notify/%s",((char *)data));
  purple_prefs_set_bool(pref,on);
  if (!(strcmp(data,"method_string") != 0)) {
    GtkWidget *entry = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"title-entry"));
    gtk_widget_set_sensitive(entry,on);
    purple_prefs_set_string("/plugins/gtk/X11/notify/title_string",gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type())))));
  }
  apply_method();
}

static void notify_toggle_cb(GtkWidget *widget,gpointer data)
{
  gboolean on = gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_toggle_button_get_type()))));
  gchar pref[256UL];
  g_snprintf(pref,(sizeof(pref)),"/plugins/gtk/X11/notify/%s",((char *)data));
  purple_prefs_set_bool(pref,on);
  apply_notify();
}

static gboolean options_entry_cb(GtkWidget *widget,GdkEventFocus *evt,gpointer data)
{
  if (data == ((void *)((void *)0))) 
    return 0;
  if (!(strcmp(data,"method_string") != 0)) {
    purple_prefs_set_string("/plugins/gtk/X11/notify/title_string",gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_entry_get_type())))));
  }
  apply_method();
  return 0;
}

static void apply_method()
{
  GList *convs;
  for (convs = purple_get_conversations(); convs != ((GList *)((void *)0)); convs = (convs -> next)) {
    PurpleConversation *conv = (PurpleConversation *)(convs -> data);
/* remove notifications */
    unnotify(conv,0);
    if (((gint )((glong )(purple_conversation_get_data(conv,"notify-message-count")))) != 0) 
/* reattach appropriate notifications */
      notify(conv,0);
  }
}

static void apply_notify()
{
  GList *convs = purple_get_conversations();
  while(convs != 0){
    PurpleConversation *conv = (PurpleConversation *)(convs -> data);
/* detach signals */
    detach_signals(conv);
/* reattach appropriate signals */
    attach_signals(conv);
    convs = (convs -> next);
  }
}

static GtkWidget *get_config_frame(PurplePlugin *plugin)
{
  GtkWidget *ret = (GtkWidget *)((void *)0);
  GtkWidget *frame = (GtkWidget *)((void *)0);
  GtkWidget *vbox = (GtkWidget *)((void *)0);
  GtkWidget *hbox = (GtkWidget *)((void *)0);
  GtkWidget *toggle = (GtkWidget *)((void *)0);
  GtkWidget *entry = (GtkWidget *)((void *)0);
  GtkWidget *ref;
  ret = gtk_vbox_new(0,18);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_container_get_type()))),12);
/*---------- "Notify For" ----------*/
  frame = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Notify For"))));
  vbox = gtk_vbox_new(0,5);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)frame),gtk_container_get_type()))),vbox);
  toggle = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","_IM windows"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),toggle,0,0,0);
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)toggle),gtk_toggle_button_get_type()))),purple_prefs_get_bool("/plugins/gtk/X11/notify/type_im"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toggle),((GType )(20 << 2))))),"toggled",((GCallback )type_toggle_cb),"type_im",0,((GConnectFlags )0));
  toggle = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","C_hat windows"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),toggle,0,0,0);
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)toggle),gtk_toggle_button_get_type()))),purple_prefs_get_bool("/plugins/gtk/X11/notify/type_chat"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toggle),((GType )(20 << 2))))),"toggled",((GCallback )type_toggle_cb),"type_chat",0,((GConnectFlags )0));
  ref = toggle;
  toggle = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","\t_Only when someone says your username"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),toggle,0,0,0);
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)toggle),gtk_toggle_button_get_type()))),purple_prefs_get_bool("/plugins/gtk/X11/notify/type_chat_nick"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toggle),((GType )(20 << 2))))),"toggled",((GCallback )type_toggle_cb),"type_chat_nick",0,((GConnectFlags )0));
  gtk_widget_set_sensitive(toggle,gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)ref),gtk_toggle_button_get_type())))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ref),((GType )(20 << 2))))),"toggled",((GCallback )pidgin_toggle_sensitive),toggle,0,((GConnectFlags )0));
  toggle = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","_Focused windows"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),toggle,0,0,0);
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)toggle),gtk_toggle_button_get_type()))),purple_prefs_get_bool("/plugins/gtk/X11/notify/type_focused"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toggle),((GType )(20 << 2))))),"toggled",((GCallback )type_toggle_cb),"type_focused",0,((GConnectFlags )0));
/*---------- "Notification Methods" ----------*/
  frame = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Notification Methods"))));
  vbox = gtk_vbox_new(0,5);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)frame),gtk_container_get_type()))),vbox);
/* String method button */
  hbox = gtk_hbox_new(0,18);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  toggle = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Prepend _string into window title:"))));
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)toggle),gtk_toggle_button_get_type()))),purple_prefs_get_bool("/plugins/gtk/X11/notify/method_string"));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),toggle,0,0,0);
  entry = gtk_entry_new();
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),entry,0,0,0);
  gtk_entry_set_max_length(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),10);
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_widget_get_type()))),purple_prefs_get_bool("/plugins/gtk/X11/notify/method_string"));
  gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),purple_prefs_get_string("/plugins/gtk/X11/notify/title_string"));
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toggle),((GType )(20 << 2))))),"title-entry",entry);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toggle),((GType )(20 << 2))))),"toggled",((GCallback )method_toggle_cb),"method_string",0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"focus-out-event",((GCallback )options_entry_cb),"method_string",0,((GConnectFlags )0));
/* Count method button */
  toggle = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Insert c_ount of new messages into window title"))));
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)toggle),gtk_toggle_button_get_type()))),purple_prefs_get_bool("/plugins/gtk/X11/notify/method_count"));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),toggle,0,0,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toggle),((GType )(20 << 2))))),"toggled",((GCallback )method_toggle_cb),"method_count",0,((GConnectFlags )0));
#ifdef HAVE_X11
/* Count xprop method button */
  toggle = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Insert count of new message into _X property"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),toggle,0,0,0);
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)toggle),gtk_toggle_button_get_type()))),purple_prefs_get_bool("/plugins/gtk/X11/notify/method_count_xprop"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toggle),((GType )(20 << 2))))),"toggled",((GCallback )method_toggle_cb),"method_count_xprop",0,((GConnectFlags )0));
/* Urgent method button */
  toggle = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Set window manager \"_URGENT\" hint"))));
#else
#endif
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),toggle,0,0,0);
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)toggle),gtk_toggle_button_get_type()))),purple_prefs_get_bool("/plugins/gtk/X11/notify/method_urgent"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toggle),((GType )(20 << 2))))),"toggled",((GCallback )method_toggle_cb),"method_urgent",0,((GConnectFlags )0));
/* Raise window method button */
  toggle = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","R_aise conversation window"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),toggle,0,0,0);
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)toggle),gtk_toggle_button_get_type()))),purple_prefs_get_bool("/plugins/gtk/X11/notify/method_raise"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toggle),((GType )(20 << 2))))),"toggled",((GCallback )method_toggle_cb),"method_raise",0,((GConnectFlags )0));
/* Present conversation method button */
  toggle = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","_Present conversation window"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),toggle,0,0,0);
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)toggle),gtk_toggle_button_get_type()))),purple_prefs_get_bool("/plugins/gtk/X11/notify/method_present"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toggle),((GType )(20 << 2))))),"toggled",((GCallback )method_toggle_cb),"method_present",0,((GConnectFlags )0));
/*---------- "Notification Removals" ----------*/
  frame = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Notification Removal"))));
  vbox = gtk_vbox_new(0,5);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)frame),gtk_container_get_type()))),vbox);
/* Remove on focus button */
  toggle = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Remove when conversation window _gains focus"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),toggle,0,0,0);
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)toggle),gtk_toggle_button_get_type()))),purple_prefs_get_bool("/plugins/gtk/X11/notify/notify_focus"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toggle),((GType )(20 << 2))))),"toggled",((GCallback )notify_toggle_cb),"notify_focus",0,((GConnectFlags )0));
/* Remove on click button */
  toggle = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Remove when conversation window _receives click"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),toggle,0,0,0);
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)toggle),gtk_toggle_button_get_type()))),purple_prefs_get_bool("/plugins/gtk/X11/notify/notify_click"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toggle),((GType )(20 << 2))))),"toggled",((GCallback )notify_toggle_cb),"notify_click",0,((GConnectFlags )0));
/* Remove on type button */
  toggle = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Remove when _typing in conversation window"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),toggle,0,0,0);
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)toggle),gtk_toggle_button_get_type()))),purple_prefs_get_bool("/plugins/gtk/X11/notify/notify_type"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toggle),((GType )(20 << 2))))),"toggled",((GCallback )notify_toggle_cb),"notify_type",0,((GConnectFlags )0));
/* Remove on message send button */
  toggle = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Remove when a _message gets sent"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),toggle,0,0,0);
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)toggle),gtk_toggle_button_get_type()))),purple_prefs_get_bool("/plugins/gtk/X11/notify/notify_send"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toggle),((GType )(20 << 2))))),"toggled",((GCallback )notify_toggle_cb),"notify_send",0,((GConnectFlags )0));
#if 0
/* Remove on conversation switch button */
#endif
  gtk_widget_show_all(ret);
  return ret;
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  GList *convs = purple_get_conversations();
  void *conv_handle = purple_conversations_get_handle();
  void *gtk_conv_handle = pidgin_conversations_get_handle();
  my_plugin = plugin;
  purple_signal_connect(gtk_conv_handle,"displayed-im-msg",plugin,((PurpleCallback )message_displayed_cb),0);
  purple_signal_connect(gtk_conv_handle,"displayed-chat-msg",plugin,((PurpleCallback )message_displayed_cb),0);
  purple_signal_connect(gtk_conv_handle,"conversation-switched",plugin,((PurpleCallback )conv_switched),0);
  purple_signal_connect(conv_handle,"sent-im-msg",plugin,((PurpleCallback )im_sent_im),0);
  purple_signal_connect(conv_handle,"sent-chat-msg",plugin,((PurpleCallback )chat_sent_im),0);
  purple_signal_connect(conv_handle,"conversation-created",plugin,((PurpleCallback )conv_created),0);
  purple_signal_connect(conv_handle,"deleting-conversation",plugin,((PurpleCallback )deleting_conv),0);
#if 0
#endif
  while(convs != 0){
    PurpleConversation *conv = (PurpleConversation *)(convs -> data);
/* attach signals */
    attach_signals(conv);
    convs = (convs -> next);
  }
  return (!0);
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  GList *convs = purple_get_conversations();
  while(convs != 0){
    PurpleConversation *conv = (PurpleConversation *)(convs -> data);
/* kill signals */
    detach_signals(conv);
    convs = (convs -> next);
  }
  return (!0);
}
static PidginPluginUiInfo ui_info = {(get_config_frame), (0), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* page_num (Reserved) */
/* padding */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gtk-gaim"), (0), ((GList *)((void *)0)), (0), ("gtk-x11-notify"), ("Message Notification"), ("2.10.9"), ("Provides a variety of ways of notifying you of unread messages."), ("Provides a variety of ways of notifying you of unread messages."), ("Etan Reisner <deryni@eden.rutgers.edu>,\nBrian Tarricone <bjt23@cornell.edu>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((&ui_info)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
  purple_prefs_add_none("/plugins/gtk");
  purple_prefs_add_none("/plugins/gtk/X11");
  purple_prefs_add_none("/plugins/gtk/X11/notify");
  purple_prefs_add_bool("/plugins/gtk/X11/notify/type_im",(!0));
  purple_prefs_add_bool("/plugins/gtk/X11/notify/type_chat",0);
  purple_prefs_add_bool("/plugins/gtk/X11/notify/type_chat_nick",0);
  purple_prefs_add_bool("/plugins/gtk/X11/notify/type_focused",0);
  purple_prefs_add_bool("/plugins/gtk/X11/notify/method_string",0);
  purple_prefs_add_string("/plugins/gtk/X11/notify/title_string","(*)");
  purple_prefs_add_bool("/plugins/gtk/X11/notify/method_urgent",0);
  purple_prefs_add_bool("/plugins/gtk/X11/notify/method_count",0);
  purple_prefs_add_bool("/plugins/gtk/X11/notify/method_count_xprop",0);
  purple_prefs_add_bool("/plugins/gtk/X11/notify/method_raise",0);
  purple_prefs_add_bool("/plugins/gtk/X11/notify/method_present",0);
  purple_prefs_add_bool("/plugins/gtk/X11/notify/notify_focus",(!0));
  purple_prefs_add_bool("/plugins/gtk/X11/notify/notify_click",0);
  purple_prefs_add_bool("/plugins/gtk/X11/notify/notify_type",(!0));
  purple_prefs_add_bool("/plugins/gtk/X11/notify/notify_send",(!0));
  purple_prefs_add_bool("/plugins/gtk/X11/notify/notify_switch",(!0));
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
