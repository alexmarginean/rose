/* Pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "pidgin.h"
#include "version.h"
#include "theme-manager.h"
#include "gtkblist.h"
#include "gtkblist-theme.h"
#include "gtkutils.h"
#include "gtkplugin.h"
#define PLUGIN_ID "gtk-theme-editor"
#include "themeedit-icon.h"

static gboolean prop_type_is_color(PidginBlistTheme *theme,const char *prop)
{
  PidginBlistThemeClass *klass = (PidginBlistThemeClass *)( *((GTypeInstance *)theme)).g_class;
  GParamSpec *spec = g_object_class_find_property(((GObjectClass *)(g_type_check_class_cast(((GTypeClass *)klass),((GType )(20 << 2))))),prop);
  return (
{
    GTypeInstance *__inst = (GTypeInstance *)spec;
    GType __t = g_param_spec_types[16];
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  });
}
#ifdef NOT_SADRUL
/* TODO: SAVE! */
#endif

static void close_blist_theme(GtkWidget *w,GtkWidget *window)
{
  gtk_widget_destroy(window);
}

static void theme_color_selected(GtkDialog *dialog,gint response,const char *prop)
{
  if (response == GTK_RESPONSE_OK) {
    GtkWidget *colorsel;
    GdkColor color;
    PidginBlistTheme *theme;
#if GTK_CHECK_VERSION(2,14,0)
    colorsel = gtk_color_selection_dialog_get_color_selection(((GtkColorSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_color_selection_dialog_get_type()))));
#else
#endif
    gtk_color_selection_get_current_color(((GtkColorSelection *)(g_type_check_instance_cast(((GTypeInstance *)colorsel),gtk_color_selection_get_type()))),&color);
    theme = pidgin_blist_get_theme();
    if (prop_type_is_color(theme,prop) != 0) {
      g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2))))),prop,&color,((void *)((void *)0)));
    }
    else {
      PidginThemeFont *font = (PidginThemeFont *)((void *)0);
      g_object_get(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2))))),prop,&font,((void *)((void *)0)));
      if (!(font != 0)) {
        font = pidgin_theme_font_new(0,&color);
        g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2))))),prop,font,((void *)((void *)0)));
        pidgin_theme_font_free(font);
      }
      else {
        pidgin_theme_font_set_color(font,(&color));
      }
    }
    pidgin_blist_set_theme(theme);
  }
  gtk_widget_destroy(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_widget_get_type()))));
}

static void theme_font_face_selected(GtkWidget *dialog,gint response,gpointer font)
{
  if ((response == GTK_RESPONSE_OK) || (response == GTK_RESPONSE_APPLY)) {
    const char *fontname = (gtk_font_selection_dialog_get_font_name(((GtkFontSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_font_selection_dialog_get_type())))));
    pidgin_theme_font_set_font_face(font,fontname);
    pidgin_blist_refresh(purple_get_blist());
  }
  gtk_widget_destroy(dialog);
}

static void theme_font_select_face(GtkWidget *widget,gpointer prop)
{
  GtkWidget *dialog;
  PidginBlistTheme *theme;
  PidginThemeFont *font = (PidginThemeFont *)((void *)0);
  const char *face;
  theme = pidgin_blist_get_theme();
  g_object_get(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2))))),prop,&font,((void *)((void *)0)));
  if (!(font != 0)) {
    font = pidgin_theme_font_new(0,0);
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2))))),prop,font,((void *)((void *)0)));
    pidgin_theme_font_free(font);
    g_object_get(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2))))),prop,&font,((void *)((void *)0)));
  }
  face = pidgin_theme_font_get_font_face(font);
  dialog = gtk_font_selection_dialog_new(((const char *)(dgettext("pidgin","Select Font"))));
  if ((face != 0) && (( *face) != 0)) 
    gtk_font_selection_dialog_set_font_name(((GtkFontSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_font_selection_dialog_get_type()))),face);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dialog),((GType )(20 << 2))))),"response",((GCallback )theme_font_face_selected),font,0,((GConnectFlags )0));
  gtk_widget_show_all(dialog);
}

static void theme_color_select(GtkWidget *widget,gpointer prop)
{
  GtkWidget *dialog;
  PidginBlistTheme *theme;
  const GdkColor *color = (const GdkColor *)((void *)0);
  theme = pidgin_blist_get_theme();
  if (prop_type_is_color(theme,prop) != 0) {
    g_object_get(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2))))),prop,&color,((void *)((void *)0)));
  }
  else {
    PidginThemeFont *pair = (PidginThemeFont *)((void *)0);
    g_object_get(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2))))),prop,&pair,((void *)((void *)0)));
    if (pair != 0) 
      color = pidgin_theme_font_get_color(pair);
  }
  dialog = gtk_color_selection_dialog_new(((const char *)(dgettext("pidgin","Select Color"))));
#if GTK_CHECK_VERSION(2,14,0)
  if (color != 0) 
    gtk_color_selection_set_current_color(((GtkColorSelection *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_color_selection_dialog_get_color_selection(((GtkColorSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_color_selection_dialog_get_type())))))),gtk_color_selection_get_type()))),color);
#else
#endif
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dialog),((GType )(20 << 2))))),"response",((GCallback )theme_color_selected),prop,0,((GConnectFlags )0));
  gtk_widget_show_all(dialog);
}

static GtkWidget *pidgin_theme_create_color_selector(const char *text,const char *blurb,const char *prop,GtkSizeGroup *sizegroup)
{
  GtkWidget *color;
  GtkWidget *hbox;
  GtkWidget *label;
  hbox = gtk_hbox_new(0,18);
  label = gtk_label_new(((const char *)(dgettext("pidgin",text))));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_size_group_add_widget(sizegroup,label);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
#if GTK_CHECK_VERSION(2, 12, 0)
  gtk_widget_set_tooltip_text(label,blurb);
#endif
  color = pidgin_pixbuf_button_from_stock("","gtk-select-color",PIDGIN_BUTTON_HORIZONTAL);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)color),((GType )(20 << 2))))),"clicked",((GCallback )theme_color_select),((gpointer )prop),0,((GConnectFlags )0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),color,0,0,0);
  return hbox;
}

static GtkWidget *pidgin_theme_create_font_selector(const char *text,const char *blurb,const char *prop,GtkSizeGroup *sizegroup)
{
  GtkWidget *color;
  GtkWidget *font;
  GtkWidget *hbox;
  GtkWidget *label;
  hbox = gtk_hbox_new(0,18);
  label = gtk_label_new(((const char *)(dgettext("pidgin",text))));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_size_group_add_widget(sizegroup,label);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
#if GTK_CHECK_VERSION(2, 12, 0)
  gtk_widget_set_tooltip_text(label,blurb);
#endif
  font = pidgin_pixbuf_button_from_stock("","gtk-select-font",PIDGIN_BUTTON_HORIZONTAL);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)font),((GType )(20 << 2))))),"clicked",((GCallback )theme_font_select_face),((gpointer )prop),0,((GConnectFlags )0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),font,0,0,0);
  color = pidgin_pixbuf_button_from_stock("","gtk-select-color",PIDGIN_BUTTON_HORIZONTAL);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)color),((GType )(20 << 2))))),"clicked",((GCallback )theme_color_select),((gpointer )prop),0,((GConnectFlags )0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),color,0,0,0);
  return hbox;
}

static void pidgin_blist_theme_edit(PurplePluginAction *unused)
{
  GtkWidget *dialog;
  GtkWidget *box;
  GtkSizeGroup *group;
  PidginBlistTheme *theme;
  GObjectClass *klass;
  int i;
  int j;
  static struct __unnamed_class___F0_L238_C9_L1624R__L1625R__scope____SgSS2___variable_declaration__variable_type___Pb__Cc__Pe___variable_name_L1624R__L1625R__scope____SgSS2____scope__header__DELIMITER__L1624R__L1625R__scope____SgSS2___variable_declaration__variable_type__Ab___Pb__Cc__Pe___index_12_Ae__variable_name_L1624R__L1625R__scope____SgSS2____scope__props {
  const char *header;
  const char *props[12UL];}sections[] = {{("Contact"), {("contact-color"), ("contact"), ("online"), ("away"), ("offline"), ("idle"), ("message"), ("message_nick_said"), ("status"), ((const char *)((void *)0))}}, {("Group"), {("expanded-color"), ("expanded-text"), ("collapsed-color"), ("collapsed-text"), ((const char *)((void *)0))}}, {((const char *)((void *)0)), {}}};
  dialog = pidgin_create_dialog(((const char *)(dgettext("pidgin","Pidgin Buddylist Theme Editor"))),0,"theme-editor-blist",0);
  box = pidgin_dialog_get_vbox_with_properties(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),0,6);
  theme = pidgin_blist_get_theme();
  if (!(theme != 0)) {
    const char *author;
#ifndef _WIN32
    author = (getlogin());
#else
#endif
    theme = (g_object_new(pidgin_blist_theme_get_type(),"type","blist","author",author,((void *)((void *)0))));
    pidgin_blist_set_theme(theme);
  }
  klass = ((GObjectClass *)(g_type_check_class_cast(((GTypeClass *)((PidginBlistThemeClass *)( *((GTypeInstance *)theme)).g_class)),((GType )(20 << 2)))));
  group = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  for (i = 0; sections[i].header != 0; i++) {
    GtkWidget *vbox;
    GtkWidget *hbox;
    GParamSpec *spec;
    vbox = pidgin_make_frame(box,((const char *)(dgettext("pidgin",sections[i].header))));
    for (j = 0; sections[i].props[j] != 0; j++) {
      const char *label;
      const char *blurb;
      spec = g_object_class_find_property(klass,sections[i].props[j]);
      label = g_param_spec_get_nick(spec);
      blurb = g_param_spec_get_blurb(spec);
      if ((({
        GTypeInstance *__inst = (GTypeInstance *)spec;
        GType __t = g_param_spec_types[16];
        gboolean __r;
        if (!(__inst != 0)) 
          __r = 0;
        else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
          __r = (!0);
        else 
          __r = g_type_check_instance_is_a(__inst,__t);
        __r;
      })) != 0) 
{
        hbox = pidgin_theme_create_color_selector(label,blurb,sections[i].props[j],group);
        gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
      }
      else {
        hbox = pidgin_theme_create_font_selector(label,blurb,sections[i].props[j],group);
        gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
      }
    }
  }
  gtk_dialog_set_has_separator(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),(!0));
#ifdef NOT_SADRUL
#endif
  pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),"gtk-close",((GCallback )close_blist_theme),dialog);
  gtk_widget_show_all(dialog);
  g_object_unref(group);
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  return (!0);
}

static GList *actions(PurplePlugin *plugin,gpointer context)
{
  GList *l = (GList *)((void *)0);
  PurplePluginAction *act = (PurplePluginAction *)((void *)0);
  act = purple_plugin_action_new(((const char *)(dgettext("pidgin","Edit Buddylist Theme"))),pidgin_blist_theme_edit);
  l = g_list_append(l,act);
  act = purple_plugin_action_new(((const char *)(dgettext("pidgin","Edit Icon Theme"))),pidgin_icon_theme_edit);
  l = g_list_append(l,act);
  return l;
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gtk-gaim"), (0), ((GList *)((void *)0)), (0), ("gtk-theme-editor"), ("Pidgin Theme Editor"), ("2.10.9"), ("Pidgin Theme Editor."), ("Pidgin Theme Editor"), ("Sadrul Habib Chowdhury <imadil@gmail.com>"), ("http://pidgin.im/"), (plugin_load), ((gboolean (*)(PurplePlugin *))((void *)0)), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), (actions), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
