/*
 * Signals test plugin.
 *
 * Copyright (C) 2003 Christian Hammond.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02111-1301, USA.
 */
#define GTK_SIGNAL_TEST_PLUGIN_ID "gtk-signals-test"
#include "internal.h"
#include <gtk/gtk.h>
#include "debug.h"
#include "version.h"
#include "gtkaccount.h"
#include "gtkblist.h"
#include "gtkconv.h"
#include "gtkplugin.h"
/**************************************************************************
 * Account subsystem signal callbacks
 **************************************************************************/

static void account_modified_cb(PurpleAccount *account,void *data)
{
  purple_debug_info("gtk-signal-test","account modified cb\n");
}
/**************************************************************************
 * Buddy List subsystem signal callbacks
 **************************************************************************/

static void blist_created_cb(PurpleBuddyList *blist,void *data)
{
  purple_debug_info("gtk-signal-test","buddy list created\n");
}

static void blist_drawing_tooltip_cb(PurpleBlistNode *node,GString *str,gboolean full,void *data)
{
  purple_debug_info("gtk-signal-test","drawing tooltip cb\n");
}
/**************************************************************************
 * Conversation subsystem signal callbacks
 **************************************************************************/

static void conversation_dragging_cb(PidginWindow *source,PidginWindow *destination)
{
  purple_debug_info("gtk-signal-test","conversation dragging cb\n");
}

static gboolean displaying_im_msg_cb(PurpleAccount *account,const char *who,char **buffer,PurpleConversation *conv,PurpleMessageFlags flags,void *data)
{
  purple_debug_misc("gtk-signals test","displaying-im-msg (%s, %s)\n",purple_conversation_get_name(conv), *buffer);
  return 0;
}

static void displayed_im_msg_cb(PurpleAccount *account,const char *who,const char *buffer,PurpleConversation *conv,PurpleMessageFlags flags,void *data)
{
  purple_debug_misc("gtk-signals test","displayed-im-msg (%s, %s)\n",purple_conversation_get_name(conv),buffer);
}

static gboolean displaying_chat_msg_cb(PurpleAccount *account,const char *who,char **buffer,PurpleConversation *conv,PurpleMessageFlags flags,void *data)
{
  purple_debug_misc("gtk-signals test","displaying-chat-msg (%s, %s)\n",purple_conversation_get_name(conv), *buffer);
  return 0;
}

static void displayed_chat_msg_cb(PurpleAccount *account,const char *who,const char *buffer,PurpleConversation *conv,PurpleMessageFlags flags,void *data)
{
  purple_debug_misc("gtk-signals test","displayed-chat-msg (%s, %s)\n",purple_conversation_get_name(conv),buffer);
}

static void conversation_switched_cb(PurpleConversation *conv,void *data)
{
  purple_debug_misc("gtk-signals test","conversation-switched (%s)\n",purple_conversation_get_name(conv));
}
/**************************************************************************
 * Plugin stuff
 **************************************************************************/

static gboolean plugin_load(PurplePlugin *plugin)
{
  void *accounts_handle = pidgin_account_get_handle();
  void *blist_handle = pidgin_blist_get_handle();
  void *conv_handle = pidgin_conversations_get_handle();
/* Accounts subsystem signals */
  purple_signal_connect(accounts_handle,"account-modified",plugin,((PurpleCallback )account_modified_cb),0);
/* Buddy List subsystem signals */
  purple_signal_connect(blist_handle,"gtkblist-created",plugin,((PurpleCallback )blist_created_cb),0);
  purple_signal_connect(blist_handle,"drawing-tooltip",plugin,((PurpleCallback )blist_drawing_tooltip_cb),0);
/* Conversations subsystem signals */
  purple_signal_connect(conv_handle,"conversation-dragging",plugin,((PurpleCallback )conversation_dragging_cb),0);
  purple_signal_connect(conv_handle,"displaying-im-msg",plugin,((PurpleCallback )displaying_im_msg_cb),0);
  purple_signal_connect(conv_handle,"displayed-im-msg",plugin,((PurpleCallback )displayed_im_msg_cb),0);
  purple_signal_connect(conv_handle,"displaying-chat-msg",plugin,((PurpleCallback )displaying_chat_msg_cb),0);
  purple_signal_connect(conv_handle,"displayed-chat-msg",plugin,((PurpleCallback )displayed_chat_msg_cb),0);
  purple_signal_connect(conv_handle,"conversation-switched",plugin,((PurpleCallback )conversation_switched_cb),0);
  return (!0);
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  return (!0);
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gtk-gaim"), (0), ((GList *)((void *)0)), (0), ("gtk-signals-test"), ("GTK Signals Test"), ("2.10.9"), ("Test to see that all ui signals are working properly."), ("Test to see that all ui signals are working properly."), ("Gary Kramlich <amc_grim@users.sf.net>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
