/* Puts last 4k of log in new conversations a la Everybuddy (and then
 * stolen by Trillian "Pro") */
#include "internal.h"
#include "pidgin.h"
#include "conversation.h"
#include "debug.h"
#include "log.h"
#include "notify.h"
#include "prefs.h"
#include "signals.h"
#include "util.h"
#include "version.h"
#include "gtkconv.h"
#include "gtkimhtml.h"
#include "gtkplugin.h"
#define HISTORY_PLUGIN_ID "gtk-history"
#define HISTORY_SIZE (4 * 1024)

static gboolean _scroll_imhtml_to_end(gpointer data)
{
  GtkIMHtml *imhtml = data;
  gtk_imhtml_scroll_to_end(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),0);
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))));
  return 0;
}

static void historize(PurpleConversation *c)
{
  PurpleAccount *account = purple_conversation_get_account(c);
  const char *name = purple_conversation_get_name(c);
  PurpleConversationType convtype;
  GList *logs = (GList *)((void *)0);
  const char *alias = name;
  guint flags;
  char *history;
  PidginConversation *gtkconv;
  GtkIMHtmlOptions options = GTK_IMHTML_NO_COLOURS;
  char *header;
  char *protocol;
  char *escaped_alias;
  const char *header_date;
  convtype = purple_conversation_get_type(c);
  gtkconv = ((PidginConversation *)(c -> ui_data));
  do {
    if (gtkconv != ((PidginConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gtkconv != NULL");
      return ;
    };
  }while (0);
/* An IM which is the first active conversation. */
  do {
    if ((gtkconv -> convs) != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gtkconv->convs != NULL");
      return ;
    };
  }while (0);
  if ((convtype == PURPLE_CONV_TYPE_IM) && !(( *(gtkconv -> convs)).next != 0)) {
    GSList *buddies;
    GSList *cur;
/* If we're not logging, don't show anything.
		 * Otherwise, we might show a very old log. */
    if (!(purple_prefs_get_bool("/purple/logging/log_ims") != 0)) 
      return ;
/* Find buddies for this conversation. */
    buddies = purple_find_buddies(account,name);
/* If we found at least one buddy, save the first buddy's alias. */
    if (buddies != ((GSList *)((void *)0))) 
      alias = purple_buddy_get_contact_alias(((PurpleBuddy *)(buddies -> data)));
{
      for (cur = buddies; cur != ((GSList *)((void *)0)); cur = (cur -> next)) {
        PurpleBlistNode *node = (cur -> data);
        PurpleBlistNode *prev = purple_blist_node_get_sibling_prev(node);
        PurpleBlistNode *next = purple_blist_node_get_sibling_next(node);
        if ((node != ((PurpleBlistNode *)((void *)0))) && ((prev != ((PurpleBlistNode *)((void *)0))) || (next != ((PurpleBlistNode *)((void *)0))))) {
          PurpleBlistNode *node2;
          PurpleBlistNode *parent = purple_blist_node_get_parent(node);
          PurpleBlistNode *child = purple_blist_node_get_first_child(parent);
          alias = purple_buddy_get_contact_alias(((PurpleBuddy *)node));
/* We've found a buddy that matches this conversation.  It's part of a
				 * PurpleContact with more than one PurpleBuddy.  Loop through the PurpleBuddies
				 * in the contact and get all the logs. */
          for (node2 = child; node2 != ((PurpleBlistNode *)((void *)0)); node2 = purple_blist_node_get_sibling_next(node2)) {
            logs = g_list_concat(purple_log_get_logs(PURPLE_LOG_IM,purple_buddy_get_name(((PurpleBuddy *)node2)),purple_buddy_get_account(((PurpleBuddy *)node2))),logs);
          }
          break; 
        }
      }
    }
    g_slist_free(buddies);
    if (logs == ((GList *)((void *)0))) 
      logs = purple_log_get_logs(PURPLE_LOG_IM,name,account);
    else 
      logs = g_list_sort(logs,purple_log_compare);
  }
  else if (convtype == PURPLE_CONV_TYPE_CHAT) {
/* If we're not logging, don't show anything.
		 * Otherwise, we might show a very old log. */
    if (!(purple_prefs_get_bool("/purple/logging/log_chats") != 0)) 
      return ;
    logs = purple_log_get_logs(PURPLE_LOG_CHAT,name,account);
  }
  if (logs == ((GList *)((void *)0))) 
    return ;
  history = purple_log_read(((PurpleLog *)(logs -> data)),(&flags));
  gtkconv = ((PidginConversation *)(c -> ui_data));
  if ((flags & PURPLE_LOG_READ_NO_NEWLINE) != 0U) 
    options |= GTK_IMHTML_NO_NEWLINE;
  protocol = g_strdup(gtk_imhtml_get_protocol_name(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type())))));
  gtk_imhtml_set_protocol_name(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),purple_account_get_protocol_name(( *((PurpleLog *)(logs -> data))).account));
  if (gtk_text_buffer_get_char_count(gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_text_view_get_type()))))) != 0) 
    gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),"<BR>",options,0);
  escaped_alias = g_markup_escape_text(alias,(-1));
  if (( *((PurpleLog *)(logs -> data))).tm != 0) 
    header_date = purple_date_format_full(( *((PurpleLog *)(logs -> data))).tm);
  else 
    header_date = purple_date_format_full((localtime((&( *((PurpleLog *)(logs -> data))).time))));
  header = g_strdup_printf(((const char *)(dgettext("pidgin","<b>Conversation with %s on %s:</b><br>"))),escaped_alias,header_date);
  gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),header,options,0);
  g_free(header);
  g_free(escaped_alias);
  g_strchomp(history);
  gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),history,options,0);
  g_free(history);
  gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),"<hr>",options,0);
  gtk_imhtml_set_protocol_name(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),protocol);
  g_free(protocol);
  g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),((GType )(20 << 2))))));
  g_idle_add(_scroll_imhtml_to_end,(gtkconv -> imhtml));
  g_list_foreach(logs,((GFunc )purple_log_free),0);
  g_list_free(logs);
}

static void history_prefs_check(PurplePlugin *plugin)
{
  if (!(purple_prefs_get_bool("/purple/logging/log_ims") != 0) && !(purple_prefs_get_bool("/purple/logging/log_chats") != 0)) {
    purple_notify_message(plugin,PURPLE_NOTIFY_MSG_WARNING,0,((const char *)(dgettext("pidgin","History Plugin Requires Logging"))),((const char *)(dgettext("pidgin","Logging can be enabled from Tools -> Preferences -> Logging.\n\nEnabling logs for instant messages and/or chats will activate history for the same conversation type(s)."))),0,0);
  }
}

static void history_prefs_cb(const char *name,PurplePrefType type,gconstpointer val,gpointer data)
{
  history_prefs_check(((PurplePlugin *)data));
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  purple_signal_connect(purple_conversations_get_handle(),"conversation-created",plugin,((PurpleCallback )historize),0);
/* XXX: Do we want to listen to pidgin's "conversation-displayed" signal? */
  purple_prefs_connect_callback(plugin,"/purple/logging/log_ims",history_prefs_cb,plugin);
  purple_prefs_connect_callback(plugin,"/purple/logging/log_chats",history_prefs_cb,plugin);
  history_prefs_check(plugin);
  return (!0);
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ("gtk-gaim"), (0), ((GList *)((void *)0)), (0), ("gtk-history"), ("History"), ("2.10.9"), ("Shows recently logged conversations in new conversations."), ("When a new conversation is opened this plugin will insert the last conversation into the current conversation."), ("Sean Egan <seanegan@gmail.com>"), ("http://pidgin.im/"), (plugin_load), ((gboolean (*)(PurplePlugin *))((void *)0)), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
