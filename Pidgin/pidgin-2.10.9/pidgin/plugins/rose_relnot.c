/*
 * Release Notification Plugin
 *
 * Copyright (C) 2003, Nathan Walp <faceprint@faceprint.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02111-1301, USA.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#ifndef PURPLE_PLUGINS
#define PURPLE_PLUGINS
#endif
#include "internal.h"
#include <string.h>
#include "connection.h"
#include "core.h"
#include "debug.h"
#include "gtkblist.h"
#include "gtkutils.h"
#include "notify.h"
#include "pidginstock.h"
#include "prefs.h"
#include "util.h"
#include "version.h"
#include "pidgin.h"
/* 1 day */
#define MIN_CHECK_INTERVAL 60 * 60 * 24

static void release_hide()
{
/* No-op.  We may use this method in the future to avoid showing
	 * the popup twice */
}

static void release_show()
{
  purple_notify_uri(0,"http://pidgin.im/");
}

static void version_fetch_cb(PurpleUtilFetchUrlData *url_data,gpointer user_data,const gchar *response,size_t len,const gchar *error_message)
{
  gchar *cur_ver;
  const char *tmp;
  const char *changelog;
  char response_code[4UL];
  GtkWidget *release_dialog;
  GString *message;
  int i = 0;
  if (((error_message != 0) || !(response != 0)) || !(len != 0UL)) 
    return ;
  memset(response_code,0,(sizeof(response_code)));
/* Parse the status code - the response should be in the form of "HTTP/?.? 200 ..." */
  if ((tmp = (strstr(response," "))) != ((const char *)((void *)0))) {
    tmp++;
/* Read the 3 digit status code */
    if ((len - (tmp - response)) > 3) {
      memcpy(response_code,tmp,3);
    }
  }
  if (strcmp(response_code,"200") != 0) {
    purple_debug_error("relnot","Didn\'t recieve a HTTP status code of 200.\n");
    return ;
  }
/* Go to the start of the data */
  if ((changelog = (strstr(response,"\r\n\r\n"))) == ((const char *)((void *)0))) {
    purple_debug_error("relnot","Unable to find start of HTTP response data.\n");
    return ;
  }
  changelog += 4;
  while((changelog[i] != 0) && (changelog[i] != 10))
    i++;
/* this basically means the version thing wasn't in the format we were
	 * looking for so sourceforge is probably having web server issues, and
	 * we should try again later */
  if (i == 0) 
    return ;
  cur_ver = g_strndup(changelog,i);
  message = g_string_new("");
  g_string_append_printf(message,((const char *)(dgettext("pidgin","You can upgrade to %s %s today."))),((const char *)(dgettext("pidgin","Pidgin"))),cur_ver);
  release_dialog = pidgin_make_mini_dialog(0,"pidgin-dialog-info",((const char *)(dgettext("pidgin","New Version Available"))),(message -> str),0,((const char *)(dgettext("pidgin","Later"))),((PurpleCallback )release_hide),((const char *)(dgettext("pidgin","Download Now"))),((PurpleCallback )release_show),((void *)((void *)0)));
  pidgin_blist_add_alert(release_dialog);
  g_string_free(message,(!0));
  g_free(cur_ver);
}

static void do_check()
{
  int last_check = purple_prefs_get_int("/plugins/gtk/relnot/last_check");
  if (!(last_check != 0) || ((time(0) - last_check) > (60 * 60 * 24))) {
    gchar *url;
    gchar *request;
    const char *host = "pidgin.im";
    url = g_strdup_printf("https://%s/version.php\?version=%s&build=%s",host,purple_core_get_version(),"purple");
#ifdef _WIN32
#else
#endif
    request = g_strdup_printf("GET %s HTTP/1.0\r\nConnection: close\r\nAccept: */*\r\nHost: %s\r\n\r\n",url,host);
    purple_util_fetch_url_request_len(url,(!0),0,0,request,(!0),(-1),version_fetch_cb,0);
    g_free(request);
    g_free(url);
    purple_prefs_set_int("/plugins/gtk/relnot/last_check",(time(0)));
  }
}

static void signed_on_cb(PurpleConnection *gc,void *data)
{
  do_check();
}
/**************************************************************************
 * Plugin stuff
 **************************************************************************/

static gboolean plugin_load(PurplePlugin *plugin)
{
  purple_signal_connect(purple_connections_get_handle(),"signed-on",plugin,((PurpleCallback )signed_on_cb),0);
/* we don't check if we're offline */
  if (purple_connections_get_all() != 0) 
    do_check();
  return (!0);
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("gtk-relnot"), ("Release Notification"), ("2.10.9"), ("Checks periodically for new releases."), ("Checks periodically for new releases and notifies the user with the ChangeLog."), ("Nathan Walp <faceprint@faceprint.com>"), ("http://pidgin.im/"), (plugin_load), ((gboolean (*)(PurplePlugin *))((void *)0)), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
  purple_prefs_add_none("/plugins/gtk/relnot");
  purple_prefs_add_int("/plugins/gtk/relnot/last_check",0);
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
