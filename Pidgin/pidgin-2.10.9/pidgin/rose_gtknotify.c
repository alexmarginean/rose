/**
 * @file gtknotify.c GTK+ Notification API
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "pidgin.h"
#include <gdk/gdkkeysyms.h>
#include "account.h"
#include "connection.h"
#include "debug.h"
#include "prefs.h"
#include "pidginstock.h"
#include "util.h"
#include "gtkblist.h"
#include "gtkimhtml.h"
#include "gtknotify.h"
#include "gtkpounce.h"
#include "gtkutils.h"
typedef struct __unnamed_class___F0_L44_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__count {
GtkWidget *window;
int count;}PidginUserInfo;
typedef struct __unnamed_class___F0_L50_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L247R__Pe___variable_name_unknown_scope_and_name__scope__account__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__url__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__label__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__count__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__purple_has_handle {
PurpleAccount *account;
char *url;
GtkWidget *label;
int count;
gboolean purple_has_handle;}PidginNotifyMailData;
typedef struct __unnamed_class___F0_L59_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L247R__Pe___variable_name_unknown_scope_and_name__scope__account__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1597R__Pe___variable_name_unknown_scope_and_name__scope__pounce__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__pouncee {
PurpleAccount *account;
PurplePounce *pounce;
char *pouncee;}PidginNotifyPounceData;
typedef struct __unnamed_class___F0_L67_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L247R__Pe___variable_name_unknown_scope_and_name__scope__account__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1172R__Pe___variable_name_unknown_scope_and_name__scope__model__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__treeview__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L55R_variable_name_unknown_scope_and_name__scope__user_data__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1632R__Pe___variable_name_unknown_scope_and_name__scope__results {
PurpleAccount *account;
GtkListStore *model;
GtkWidget *treeview;
GtkWidget *window;
gpointer user_data;
PurpleNotifySearchResults *results;}PidginNotifySearchResultsData;
typedef struct __unnamed_class___F0_L78_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L1651R__Pe___variable_name_unknown_scope_and_name__scope__button__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1653R__Pe___variable_name_unknown_scope_and_name__scope__data {
PurpleNotifySearchButton *button;
PidginNotifySearchResultsData *data;}PidginNotifySearchResultsButtonData;
enum __unnamed_enum___F0_L85_C1_PIDGIN_MAIL_ICON__COMMA__PIDGIN_MAIL_TEXT__COMMA__PIDGIN_MAIL_DATA__COMMA__COLUMNS_PIDGIN_MAIL {PIDGIN_MAIL_ICON,PIDGIN_MAIL_TEXT,PIDGIN_MAIL_DATA,COLUMNS_PIDGIN_MAIL};
enum __unnamed_enum___F0_L93_C1_PIDGIN_POUNCE_ICON__COMMA__PIDGIN_POUNCE_ALIAS__COMMA__PIDGIN_POUNCE_EVENT__COMMA__PIDGIN_POUNCE_TEXT__COMMA__PIDGIN_POUNCE_DATE__COMMA__PIDGIN_POUNCE_DATA__COMMA__COLUMNS_PIDGIN_POUNCE {PIDGIN_POUNCE_ICON,PIDGIN_POUNCE_ALIAS,PIDGIN_POUNCE_EVENT,PIDGIN_POUNCE_TEXT,PIDGIN_POUNCE_DATE,PIDGIN_POUNCE_DATA,COLUMNS_PIDGIN_POUNCE};
typedef struct _PidginNotifyDialog {
/*
	 * This must be first so PidginNotifyDialog can masquerade as the
	 * dialog widget.
	 */
GtkWidget *dialog;
GtkWidget *treeview;
GtkTreeStore *treemodel;
GtkLabel *label;
GtkWidget *open_button;
GtkWidget *dismiss_button;
GtkWidget *edit_button;
int total_count;
gboolean in_use;}PidginNotifyDialog;
typedef enum  {PIDGIN_NOTIFY_MAIL,PIDGIN_NOTIFY_POUNCE,PIDGIN_NOTIFY_TYPES}PidginNotifyType;
static PidginNotifyDialog *mail_dialog = (PidginNotifyDialog *)((void *)0);
static PidginNotifyDialog *pounce_dialog = (PidginNotifyDialog *)((void *)0);
static PidginNotifyDialog *pidgin_create_notification_dialog(PidginNotifyType type);
static void *pidgin_notify_emails(PurpleConnection *gc,size_t count,gboolean detailed,const char **subjects,const char **froms,const char **tos,const char **urls);
static void pidgin_close_notify(PurpleNotifyType type,void *ui_handle);

static void message_response_cb(GtkDialog *dialog,gint id,GtkWidget *widget)
{
  purple_notify_close(PURPLE_NOTIFY_MESSAGE,widget);
}

static void pounce_response_close(PidginNotifyDialog *dialog)
{
  GtkTreeIter iter;
  PidginNotifyPounceData *pounce_data;
  while(gtk_tree_model_get_iter_first(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(pounce_dialog -> treemodel)),gtk_tree_model_get_type()))),&iter) != 0){
    gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(pounce_dialog -> treemodel)),gtk_tree_model_get_type()))),&iter,PIDGIN_POUNCE_DATA,&pounce_data,-1);
    gtk_tree_store_remove((dialog -> treemodel),&iter);
    g_free((pounce_data -> pouncee));
    g_free(pounce_data);
  }
  gtk_widget_destroy((pounce_dialog -> dialog));
  g_free(pounce_dialog);
  pounce_dialog = ((PidginNotifyDialog *)((void *)0));
}

static void delete_foreach(GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter,gpointer data)
{
  PidginNotifyPounceData *pounce_data;
  gtk_tree_model_get(model,iter,PIDGIN_POUNCE_DATA,&pounce_data,-1);
  if (pounce_data != ((PidginNotifyPounceData *)((void *)0))) {
    g_free((pounce_data -> pouncee));
    g_free(pounce_data);
  }
}

static void open_im_foreach(GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter,gpointer data)
{
  PidginNotifyPounceData *pounce_data;
  gtk_tree_model_get(model,iter,PIDGIN_POUNCE_DATA,&pounce_data,-1);
  if (pounce_data != ((PidginNotifyPounceData *)((void *)0))) {
    PurpleConversation *conv;
    conv = purple_conversation_new(PURPLE_CONV_TYPE_IM,(pounce_data -> account),(pounce_data -> pouncee));
    purple_conversation_present(conv);
  }
}

static void append_to_list(GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter,gpointer data)
{
  GList **list = data;
   *list = g_list_prepend( *list,(gtk_tree_path_copy(path)));
}

static void pounce_response_dismiss()
{
  GtkTreeModel *model = (GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(pounce_dialog -> treemodel)),gtk_tree_model_get_type()));
  GtkTreeSelection *selection;
  GtkTreeIter iter;
  GtkTreeIter new_selection;
  GList *list = (GList *)((void *)0);
  gboolean found_selection = 0;
  selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(pounce_dialog -> treeview)),gtk_tree_view_get_type()))));
  gtk_tree_selection_selected_foreach(selection,delete_foreach,pounce_dialog);
  gtk_tree_selection_selected_foreach(selection,append_to_list,(&list));
  do {
    if (list != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"list != NULL");
      return ;
    };
  }while (0);
  if ((list -> next) == ((GList *)((void *)0))) {
    gtk_tree_model_get_iter(model,&new_selection,(list -> data));
    if (gtk_tree_model_iter_next(model,&new_selection) != 0) 
      found_selection = (!0);
    else {
/* This is the last thing in the list */
      GtkTreePath *path;
/* Because gtk_tree_model_iter_prev doesn't exist... */
      gtk_tree_model_get_iter(model,&new_selection,(list -> data));
      path = gtk_tree_model_get_path(model,&new_selection);
      if (gtk_tree_path_prev(path) != 0) {
        gtk_tree_model_get_iter(model,&new_selection,path);
        found_selection = (!0);
      }
      gtk_tree_path_free(path);
    }
  }
  while(list != 0){
    if (gtk_tree_model_get_iter(model,&iter,(list -> data)) != 0) {
      gtk_tree_store_remove(((GtkTreeStore *)(g_type_check_instance_cast(((GTypeInstance *)(pounce_dialog -> treemodel)),gtk_tree_store_get_type()))),&iter);
    }
    gtk_tree_path_free((list -> data));
    list = g_list_delete_link(list,list);
  }
  if (gtk_tree_model_get_iter_first(model,&iter) != 0) {
    if (found_selection != 0) 
      gtk_tree_selection_select_iter(selection,&new_selection);
    else 
      gtk_tree_selection_select_iter(selection,&iter);
  }
  else 
    pounce_response_close(pounce_dialog);
}

static void pounce_response_open_ims()
{
  GtkTreeSelection *selection;
  selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(pounce_dialog -> treeview)),gtk_tree_view_get_type()))));
  gtk_tree_selection_selected_foreach(selection,open_im_foreach,pounce_dialog);
  pounce_response_dismiss();
}

static void pounce_response_edit_cb(GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter,gpointer data)
{
  PidginNotifyPounceData *pounce_data;
  PidginNotifyDialog *dialog = (PidginNotifyDialog *)data;
  PurplePounce *pounce;
  GList *list;
  list = purple_pounces_get_all();
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treemodel)),gtk_tree_model_get_type()))),iter,PIDGIN_POUNCE_DATA,&pounce_data,-1);
  for (; list != ((GList *)((void *)0)); list = (list -> next)) {
    pounce = (list -> data);
    if (pounce == (pounce_data -> pounce)) {
      pidgin_pounce_editor_show((pounce_data -> account),0,(pounce_data -> pounce));
      return ;
    }
  }
  purple_debug_warning("gtknotify","Pounce was destroyed.\n");
}

static void pounce_response_cb(GtkDialog *dlg,gint id,PidginNotifyDialog *dialog)
{
  GtkTreeSelection *selection = (GtkTreeSelection *)((void *)0);
  switch(id){
    case -7:
{
    }
    case -4:
{
      pounce_response_close(dialog);
      break; 
    }
    case -8:
{
      pounce_response_open_ims();
      break; 
    }
    case -9:
{
      pounce_response_dismiss();
      break; 
    }
    case -10:
{
      selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treeview)),gtk_tree_view_get_type()))));
      gtk_tree_selection_selected_foreach(selection,pounce_response_edit_cb,dialog);
      break; 
    }
  }
}

static void pounce_row_selected_cb(GtkTreeView *tv,GtkTreePath *path,GtkTreeViewColumn *col,gpointer data)
{
  GtkTreeSelection *selection;
  int count;
  selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(pounce_dialog -> treeview)),gtk_tree_view_get_type()))));
  count = gtk_tree_selection_count_selected_rows(selection);
  if (count == 0) {
    gtk_widget_set_sensitive((pounce_dialog -> open_button),0);
    gtk_widget_set_sensitive((pounce_dialog -> edit_button),0);
    gtk_widget_set_sensitive((pounce_dialog -> dismiss_button),0);
  }
  else if (count == 1) {
    GList *pounces;
    GList *list;
    PidginNotifyPounceData *pounce_data;
    GtkTreeIter iter;
    list = gtk_tree_selection_get_selected_rows(selection,0);
    gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(pounce_dialog -> treemodel)),gtk_tree_model_get_type()))),&iter,(list -> data));
    gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(pounce_dialog -> treemodel)),gtk_tree_model_get_type()))),&iter,PIDGIN_POUNCE_DATA,&pounce_data,-1);
    g_list_foreach(list,((GFunc )gtk_tree_path_free),0);
    g_list_free(list);
    pounces = purple_pounces_get_all();
{
      for (; pounces != ((GList *)((void *)0)); pounces = (pounces -> next)) {
        PurplePounce *pounce = (pounces -> data);
        if (pounce == (pounce_data -> pounce)) {
          gtk_widget_set_sensitive((pounce_dialog -> edit_button),(!0));
          break; 
        }
      }
    }
    gtk_widget_set_sensitive((pounce_dialog -> open_button),(!0));
    gtk_widget_set_sensitive((pounce_dialog -> dismiss_button),(!0));
  }
  else {
    gtk_widget_set_sensitive((pounce_dialog -> open_button),(!0));
    gtk_widget_set_sensitive((pounce_dialog -> edit_button),0);
    gtk_widget_set_sensitive((pounce_dialog -> dismiss_button),(!0));
  }
}

static void reset_mail_dialog(GtkDialog *unused)
{
  if ((mail_dialog -> in_use) != 0) 
    return ;
  gtk_widget_destroy((mail_dialog -> dialog));
  g_free(mail_dialog);
  mail_dialog = ((PidginNotifyDialog *)((void *)0));
}

static void email_response_cb(GtkDialog *unused,gint id,PidginNotifyDialog *unused2)
{
  PidginNotifyMailData *data = (PidginNotifyMailData *)((void *)0);
  GtkTreeModel *model = (GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(mail_dialog -> treemodel)),gtk_tree_model_get_type()));
  GtkTreeIter iter;
  if (id == GTK_RESPONSE_YES) {
/* A single row activated. Remove that row. */
    GtkTreeSelection *selection;
    selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(mail_dialog -> treeview)),gtk_tree_view_get_type()))));
    if (gtk_tree_selection_get_selected(selection,0,&iter) != 0) {
      gtk_tree_model_get(model,&iter,PIDGIN_MAIL_DATA,&data,-1);
      purple_notify_uri(0,(data -> url));
      gtk_tree_store_remove((mail_dialog -> treemodel),&iter);
      if ((data -> purple_has_handle) != 0) 
        purple_notify_close(PURPLE_NOTIFY_EMAILS,data);
      else 
        pidgin_close_notify(PURPLE_NOTIFY_EMAILS,data);
      if (gtk_tree_model_get_iter_first(model,&iter) != 0) 
        return ;
    }
    else 
      return ;
  }
  else {
/* Remove all the rows */
    while(gtk_tree_model_get_iter_first(model,&iter) != 0){
      gtk_tree_model_get(model,&iter,PIDGIN_MAIL_DATA,&data,-1);
      if (id == GTK_RESPONSE_ACCEPT) 
        purple_notify_uri(0,(data -> url));
      gtk_tree_store_remove((mail_dialog -> treemodel),&iter);
      if ((data -> purple_has_handle) != 0) 
        purple_notify_close(PURPLE_NOTIFY_EMAILS,data);
      else 
        pidgin_close_notify(PURPLE_NOTIFY_EMAILS,data);
    }
  }
  reset_mail_dialog(0);
}

static void email_row_activated_cb(GtkTreeView *tv,GtkTreePath *path,GtkTreeViewColumn *col,gpointer data)
{
  email_response_cb(0,GTK_RESPONSE_YES,0);
}

static gboolean formatted_close_cb(GtkWidget *win,GdkEvent *event,void *user_data)
{
  purple_notify_close(PURPLE_NOTIFY_FORMATTED,win);
  return 0;
}

static gboolean searchresults_close_cb(PidginNotifySearchResultsData *data,GdkEvent *event,gpointer user_data)
{
  purple_notify_close(PURPLE_NOTIFY_SEARCHRESULTS,data);
  return 0;
}

static void searchresults_callback_wrapper_cb(GtkWidget *widget,PidginNotifySearchResultsButtonData *bd)
{
  PidginNotifySearchResultsData *data = (bd -> data);
  GtkTreeSelection *selection;
  GtkTreeModel *model;
  GtkTreeIter iter;
  PurpleNotifySearchButton *button;
  GList *row = (GList *)((void *)0);
  gchar *str;
  int i;
  do {
    if (data != ((PidginNotifySearchResultsData *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"data != NULL");
      return ;
    };
  }while (0);
  selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(data -> treeview)),gtk_tree_view_get_type()))));
  if (gtk_tree_selection_get_selected(selection,&model,&iter) != 0) {
    for (i = 1; i < gtk_tree_model_get_n_columns(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type())))); i++) {
      gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,i,&str,-1);
      row = g_list_append(row,str);
    }
  }
  button = (bd -> button);
  ( *(button -> callback))(purple_account_get_connection((data -> account)),row,(data -> user_data));
  g_list_foreach(row,((GFunc )g_free),0);
  g_list_free(row);
}

static void *pidgin_notify_message(PurpleNotifyMsgType type,const char *title,const char *primary,const char *secondary)
{
  GtkWidget *dialog;
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *img = (GtkWidget *)((void *)0);
  char label_text[2048UL];
  const char *icon_name = (const char *)((void *)0);
  char *primary_esc;
  char *secondary_esc;
  switch(type){
    case PURPLE_NOTIFY_MSG_ERROR:
{
      icon_name = "pidgin-dialog-error";
      break; 
    }
    case PURPLE_NOTIFY_MSG_WARNING:
{
      icon_name = "pidgin-dialog-warning";
      break; 
    }
    case PURPLE_NOTIFY_MSG_INFO:
{
      icon_name = "pidgin-dialog-info";
      break; 
    }
    default:
{
      icon_name = ((const char *)((void *)0));
      break; 
    }
  }
  if (icon_name != ((const char *)((void *)0))) {
    img = gtk_image_new_from_stock(icon_name,gtk_icon_size_from_name("pidgin-icon-size-tango-huge"));
    gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)img),gtk_misc_get_type()))),0,0);
  }
  dialog = gtk_dialog_new_with_buttons(((title != 0)?title : ""),0,0,"gtk-close",GTK_RESPONSE_CLOSE,((void *)((void *)0)));
  gtk_window_set_role(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_window_get_type()))),"notify_dialog");
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dialog),((GType )(20 << 2))))),"response",((GCallback )message_response_cb),dialog,0,((GConnectFlags )0));
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_container_get_type()))),12);
  gtk_window_set_resizable(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_window_get_type()))),0);
  gtk_dialog_set_has_separator(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),0);
  gtk_box_set_spacing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type())))).vbox),gtk_box_get_type()))),12);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type())))).vbox),gtk_container_get_type()))),6);
  hbox = gtk_hbox_new(0,12);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type())))).vbox),gtk_container_get_type()))),hbox);
  if (img != ((GtkWidget *)((void *)0))) 
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),img,0,0,0);
  primary_esc = g_markup_escape_text(primary,(-1));
  secondary_esc = ((secondary != ((const char *)((void *)0)))?g_markup_escape_text(secondary,(-1)) : ((char *)((void *)0)));
  g_snprintf(label_text,(sizeof(label_text)),"<span weight=\"bold\" size=\"larger\">%s</span>%s%s",primary_esc,((secondary != 0)?"\n\n" : ""),((secondary != 0)?secondary_esc : ""));
  g_free(primary_esc);
  g_free(secondary_esc);
  label = gtk_label_new(0);
  gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),label_text);
  gtk_label_set_line_wrap(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(!0));
  gtk_label_set_selectable(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(!0));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  pidgin_auto_parent_window(dialog);
  gtk_widget_show_all(dialog);
  return dialog;
}

static void selection_changed_cb(GtkTreeSelection *sel,PidginNotifyDialog *dialog)
{
  GtkTreeIter iter;
  GtkTreeModel *model;
  PidginNotifyMailData *data;
  gboolean active = (!0);
  if (gtk_tree_selection_get_selected(sel,&model,&iter) == 0) 
    active = 0;
  else {
    gtk_tree_model_get(model,&iter,PIDGIN_MAIL_DATA,&data,-1);
    if ((data -> url) == ((char *)((void *)0))) 
      active = 0;
  }
  gtk_widget_set_sensitive((dialog -> open_button),active);
}

static void *pidgin_notify_email(PurpleConnection *gc,const char *subject,const char *from,const char *to,const char *url)
{
  return pidgin_notify_emails(gc,1,(subject != ((const char *)((void *)0))),((subject == ((const char *)((void *)0)))?((const char **)((void *)0)) : &subject),((from == ((const char *)((void *)0)))?((const char **)((void *)0)) : &from),((to == ((const char *)((void *)0)))?((const char **)((void *)0)) : &to),((url == ((const char *)((void *)0)))?((const char **)((void *)0)) : &url));
}

static int mail_window_focus_cb(GtkWidget *widget,GdkEventFocus *focus,gpointer null)
{
  pidgin_set_urgent(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_window_get_type()))),0);
  return 0;
}
/* count == 0 means this is a detailed mail notification.
 * count > 0 mean non-detailed.
 */

static void *pidgin_notify_add_mail(GtkTreeStore *treemodel,PurpleAccount *account,char *notification,const char *url,int count,gboolean clear,gboolean *new_data)
{
  PidginNotifyMailData *data = (PidginNotifyMailData *)((void *)0);
  GtkTreeIter iter;
  GdkPixbuf *icon;
  gboolean new_n = (!0);
  if ((count > 0) || (clear != 0)) {
/* Allow only one non-detailed email notification for each account */
    if (gtk_tree_model_get_iter_first(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)treemodel),gtk_tree_model_get_type()))),&iter) != 0) {
      gboolean advanced;
{
        do {
          advanced = 0;
          gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)treemodel),gtk_tree_model_get_type()))),&iter,PIDGIN_MAIL_DATA,&data,-1);
          if ((data != 0) && ((data -> account) == account)) {
            if (clear != 0) {
              advanced = gtk_tree_store_remove(treemodel,&iter);
              mail_dialog -> total_count -= (data -> count);
              if ((data -> purple_has_handle) != 0) 
                purple_notify_close(PURPLE_NOTIFY_EMAILS,data);
              else 
                pidgin_close_notify(PURPLE_NOTIFY_EMAILS,data);
/* We're completely done if we've processed all entries */
              if (!(advanced != 0)) 
                return 0;
            }
            else if ((data -> count) > 0) {
              new_n = 0;
              g_free((data -> url));
              data -> url = ((char *)((void *)0));
              mail_dialog -> total_count -= (data -> count);
              break; 
            }
          }
        }while ((advanced != 0) || (gtk_tree_model_iter_next(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)treemodel),gtk_tree_model_get_type()))),&iter) != 0));
      }
    }
  }
  if (clear != 0) 
    return 0;
  icon = pidgin_create_prpl_icon(account,PIDGIN_PRPL_ICON_MEDIUM);
  if (new_n != 0) {
    data = ((PidginNotifyMailData *)(g_malloc0_n(1,(sizeof(PidginNotifyMailData )))));
    data -> purple_has_handle = (!0);
    gtk_tree_store_append(treemodel,&iter,0);
  }
  if (url != ((const char *)((void *)0))) 
    data -> url = g_strdup(url);
  gtk_tree_store_set(treemodel,&iter,PIDGIN_MAIL_ICON,icon,PIDGIN_MAIL_TEXT,notification,PIDGIN_MAIL_DATA,data,-1);
  data -> account = account;
/* count == 0 indicates we're adding a single detailed e-mail */
  data -> count = ((count > 0)?count : 1);
  if (icon != 0) 
    g_object_unref(icon);
  if (new_data != 0) 
     *new_data = new_n;
  return data;
}

static void *pidgin_notify_emails(PurpleConnection *gc,size_t count,gboolean detailed,const char **subjects,const char **froms,const char **tos,const char **urls)
{
  char *notification;
  PurpleAccount *account;
  PidginNotifyMailData *data = (PidginNotifyMailData *)((void *)0);
  PidginNotifyMailData *data2;
  gboolean new_data = 0;
/* Don't bother updating if there aren't new emails and we don't have any displayed currently */
  if ((count == 0) && (mail_dialog == ((PidginNotifyDialog *)((void *)0)))) 
    return 0;
  account = purple_connection_get_account(gc);
  if (mail_dialog == ((PidginNotifyDialog *)((void *)0))) 
    mail_dialog = pidgin_create_notification_dialog(PIDGIN_NOTIFY_MAIL);
  mail_dialog -> total_count += count;
  if (detailed != 0) {
    for (; count != 0UL; --count) {
      char *to_text = (char *)((void *)0);
      char *from_text = (char *)((void *)0);
      char *subject_text = (char *)((void *)0);
      char *tmp;
      gboolean first = (!0);
      if (tos != ((const char **)((void *)0))) {
        tmp = g_markup_escape_text( *tos,(-1));
        to_text = g_strdup_printf("<b>%s</b>: %s\n",((const char *)(dgettext("pidgin","Account"))),tmp);
        g_free(tmp);
        first = 0;
        tos++;
      }
      if (froms != ((const char **)((void *)0))) {
        tmp = g_markup_escape_text( *froms,(-1));
        from_text = g_strdup_printf("%s<b>%s</b>: %s\n",((first != 0)?"<br>" : ""),((const char *)(dgettext("pidgin","Sender"))),tmp);
        g_free(tmp);
        first = 0;
        froms++;
      }
      if (subjects != ((const char **)((void *)0))) {
        tmp = g_markup_escape_text( *subjects,(-1));
        subject_text = g_strdup_printf("%s<b>%s</b>: %s",((first != 0)?"<br>" : ""),((const char *)(dgettext("pidgin","Subject"))),tmp);
        g_free(tmp);
        first = 0;
        subjects++;
      }
#define SAFE(x) ((x) ? (x) : "")
      notification = g_strdup_printf("%s%s%s",((to_text != 0)?to_text : ""),((from_text != 0)?from_text : ""),((subject_text != 0)?subject_text : ""));
#undef SAFE
      g_free(to_text);
      g_free(from_text);
      g_free(subject_text);
/* If we don't keep track of this, will leak "data" for each of the notifications except the last */
      data2 = (pidgin_notify_add_mail((mail_dialog -> treemodel),account,notification,((urls != 0)? *urls : ((const char *)((void *)0))),0,0,&new_data));
      if ((data2 != 0) && (new_data != 0)) {
        if (data != 0) 
          data -> purple_has_handle = 0;
        data = data2;
      }
      g_free(notification);
      if (urls != ((const char **)((void *)0))) 
        urls++;
    }
  }
  else {
    if (count > 0) {
      notification = g_strdup_printf((ngettext("%s has %d new message.","%s has %d new messages.",((int )count))), *tos,((int )count));
      data2 = (pidgin_notify_add_mail((mail_dialog -> treemodel),account,notification,((urls != 0)? *urls : ((const char *)((void *)0))),count,0,&new_data));
      if ((data2 != 0) && (new_data != 0)) {
        if (data != 0) 
          data -> purple_has_handle = 0;
        data = data2;
      }
      g_free(notification);
    }
    else {
/* Clear out all mails for the account */
      pidgin_notify_add_mail((mail_dialog -> treemodel),account,0,0,0,(!0),0);
      if ((mail_dialog -> total_count) == 0) {
/*
				 * There is no API to clear the headline specifically
				 * This will trigger reset_mail_dialog()
				 */
        pidgin_blist_set_headline(0,0,0,0,0);
        return 0;
      }
    }
  }
  if (!((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)(mail_dialog -> dialog)),gtk_object_get_type())))).flags & GTK_VISIBLE) != 0)) {
    GdkPixbuf *pixbuf = gtk_widget_render_icon((mail_dialog -> dialog),"pidgin-dialog-mail",gtk_icon_size_from_name("pidgin-icon-size-tango-extra-small"),0);
    char *label_text = g_strdup_printf((ngettext("<b>%d new email.</b>","<b>%d new emails.</b>",(mail_dialog -> total_count))),(mail_dialog -> total_count));
/* So that _set_headline doesn't accidentally
										   remove the notifications when replacing an
										   old notification. */
    mail_dialog -> in_use = (!0);
    pidgin_blist_set_headline(label_text,pixbuf,((GCallback )gtk_widget_show_all),(mail_dialog -> dialog),((GDestroyNotify )reset_mail_dialog));
    mail_dialog -> in_use = 0;
    g_free(label_text);
    if (pixbuf != 0) 
      g_object_unref(pixbuf);
  }
  else if (!((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)(mail_dialog -> dialog)),gtk_object_get_type())))).flags & GTK_HAS_FOCUS) != 0)) 
    pidgin_set_urgent(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(mail_dialog -> dialog)),gtk_window_get_type()))),(!0));
  return data;
}

static gboolean formatted_input_cb(GtkWidget *win,GdkEventKey *event,gpointer data)
{
  if ((event -> keyval) == 0xff1b) {
    purple_notify_close(PURPLE_NOTIFY_FORMATTED,win);
    return (!0);
  }
  return 0;
}

static GtkIMHtmlOptions notify_imhtml_options()
{
  GtkIMHtmlOptions options = 0;
  if (!(purple_prefs_get_bool("/pidgin/conversations/show_incoming_formatting") != 0)) 
    options |= (GTK_IMHTML_NO_COLOURS | GTK_IMHTML_NO_FONTS | GTK_IMHTML_NO_SIZES);
  options |= GTK_IMHTML_NO_COMMENTS;
  options |= GTK_IMHTML_NO_TITLE;
  options |= GTK_IMHTML_NO_NEWLINE;
  options |= GTK_IMHTML_NO_SCROLL;
  return options;
}

static void *pidgin_notify_formatted(const char *title,const char *primary,const char *secondary,const char *text)
{
  GtkWidget *window;
  GtkWidget *vbox;
  GtkWidget *label;
  GtkWidget *button;
  GtkWidget *imhtml;
  GtkWidget *frame;
  char label_text[2048UL];
  char *linked_text;
  char *primary_esc;
  char *secondary_esc;
  window = gtk_dialog_new();
  gtk_window_set_title(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_window_get_type()))),title);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_container_get_type()))),12);
  gtk_window_set_resizable(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_window_get_type()))),(!0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"delete_event",((GCallback )formatted_close_cb),0,0,((GConnectFlags )0));
/* Setup the main vbox */
  vbox = ( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type())))).vbox;
/* Setup the descriptive label */
  primary_esc = g_markup_escape_text(primary,(-1));
  secondary_esc = ((secondary != ((const char *)((void *)0)))?g_markup_escape_text(secondary,(-1)) : ((char *)((void *)0)));
  g_snprintf(label_text,(sizeof(label_text)),"<span weight=\"bold\" size=\"larger\">%s</span>%s%s",primary_esc,((secondary != 0)?"\n" : ""),((secondary != 0)?secondary_esc : ""));
  g_free(primary_esc);
  g_free(secondary_esc);
  label = gtk_label_new(0);
  gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),label_text);
  gtk_label_set_line_wrap(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(!0));
  gtk_label_set_selectable(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(!0));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),label,0,0,0);
  gtk_widget_show(label);
/* Add the imhtml */
  frame = pidgin_create_imhtml(0,&imhtml,0,0);
  gtk_widget_set_name(imhtml,"pidgin_notify_imhtml");
  gtk_imhtml_set_format_functions(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),((gtk_imhtml_get_format_functions(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))))) | GTK_IMHTML_IMAGE));
  gtk_widget_set_size_request(imhtml,300,250);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),frame,(!0),(!0),0);
  gtk_widget_show(frame);
/* Add the Close button. */
  button = gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),"gtk-close",GTK_RESPONSE_CLOSE);
  gtk_widget_grab_focus(button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )formatted_close_cb),window,0,G_CONNECT_SWAPPED);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"key_press_event",((GCallback )formatted_input_cb),0,0,((GConnectFlags )0));
/* Make sure URLs are clickable */
  linked_text = purple_markup_linkify(text);
  gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),linked_text,notify_imhtml_options(),0);
  g_free(linked_text);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"info-widget",imhtml);
/* Show the window */
  pidgin_auto_parent_window(window);
  gtk_widget_show(window);
  return window;
}

static void pidgin_notify_searchresults_new_rows(PurpleConnection *gc,PurpleNotifySearchResults *results,void *data_)
{
  PidginNotifySearchResultsData *data = data_;
  GtkListStore *model = (data -> model);
  GtkTreeIter iter;
  GdkPixbuf *pixbuf;
  GList *row;
  GList *column;
  guint n;
  gtk_list_store_clear((data -> model));
  pixbuf = pidgin_create_prpl_icon(purple_connection_get_account(gc),PIDGIN_PRPL_ICON_SMALL);
  for (row = (results -> rows); row != ((GList *)((void *)0)); row = (row -> next)) {
    gtk_list_store_append(model,&iter);
    gtk_list_store_set(model,&iter,0,pixbuf,-1);
    n = 1;
    for (column = (row -> data); column != ((GList *)((void *)0)); column = (column -> next)) {
      GValue v;
      v.g_type = 0;
      g_value_init(&v,((GType )(16 << 2)));
      g_value_set_string(&v,(column -> data));
      gtk_list_store_set_value(model,&iter,n,&v);
      n++;
    }
  }
  if (pixbuf != ((GdkPixbuf *)((void *)0))) 
    g_object_unref(pixbuf);
}

static void *pidgin_notify_searchresults(PurpleConnection *gc,const char *title,const char *primary,const char *secondary,PurpleNotifySearchResults *results,gpointer user_data)
{
  GtkWidget *window;
  GtkWidget *treeview;
  GtkWidget *close_button;
  GType *col_types;
  GtkListStore *model;
  GtkCellRenderer *renderer;
  guint col_num;
  GList *columniter;
  guint i;
  GList *l;
  GtkWidget *vbox;
  GtkWidget *label;
  PidginNotifySearchResultsData *data;
  char *label_text;
  char *primary_esc;
  char *secondary_esc;
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return 0;
    };
  }while (0);
  do {
    if (results != ((PurpleNotifySearchResults *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"results != NULL");
      return 0;
    };
  }while (0);
  data = (g_malloc((sizeof(PidginNotifySearchResultsData ))));
  data -> user_data = user_data;
  data -> results = results;
/* Create the window */
  window = gtk_dialog_new();
  gtk_window_set_title(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_window_get_type()))),((title != 0)?title : ((const char *)(dgettext("pidgin","Search Results")))));
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_container_get_type()))),12);
  gtk_window_set_resizable(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_window_get_type()))),(!0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"delete_event",((GCallback )searchresults_close_cb),data,0,G_CONNECT_SWAPPED);
/* Setup the main vbox */
  vbox = ( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type())))).vbox;
/* Setup the descriptive label */
  primary_esc = ((primary != ((const char *)((void *)0)))?g_markup_escape_text(primary,(-1)) : ((char *)((void *)0)));
  secondary_esc = ((secondary != ((const char *)((void *)0)))?g_markup_escape_text(secondary,(-1)) : ((char *)((void *)0)));
  label_text = g_strdup_printf("<span weight=\"bold\" size=\"larger\">%s</span>%s%s",((primary != 0)?primary_esc : ""),(((primary != 0) && (secondary != 0))?"\n" : ""),((secondary != 0)?secondary_esc : ""));
  g_free(primary_esc);
  g_free(secondary_esc);
  label = gtk_label_new(0);
  gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),label_text);
  gtk_label_set_line_wrap(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(!0));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),label,0,0,0);
  gtk_widget_show(label);
  g_free(label_text);
/* +1 is for the automagically created Status column. */
  col_num = (g_list_length((results -> columns)) + 1);
/* Setup the list model */
  col_types = ((GType *)(g_malloc0_n(col_num,(sizeof(GType )))));
/* There always is this first column. */
  col_types[0] = gdk_pixbuf_get_type();
  for (i = 1; i < col_num; i++) {
    col_types[i] = ((GType )(16 << 2));
  }
  model = gtk_list_store_newv(col_num,col_types);
  g_free(col_types);
/* Setup the treeview */
  treeview = gtk_tree_view_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)model),((GType )(20 << 2))))));
  gtk_tree_view_set_rules_hint(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),(!0));
  gtk_widget_set_size_request(treeview,500,400);
  gtk_tree_selection_set_mode(gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type())))),GTK_SELECTION_SINGLE);
  gtk_tree_view_set_headers_visible(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),(!0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),pidgin_make_scrollable(treeview,GTK_POLICY_AUTOMATIC,GTK_POLICY_ALWAYS,GTK_SHADOW_IN,-1,-1),(!0),(!0),0);
  gtk_widget_show(treeview);
  renderer = gtk_cell_renderer_pixbuf_new();
  gtk_tree_view_insert_column_with_attributes(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),(-1),"",renderer,"pixbuf",0,((void *)((void *)0)));
  i = 1;
  for (columniter = (results -> columns); columniter != ((GList *)((void *)0)); columniter = (columniter -> next)) {
    PurpleNotifySearchColumn *column = (columniter -> data);
    renderer = gtk_cell_renderer_text_new();
    gtk_tree_view_insert_column_with_attributes(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),(-1),(column -> title),renderer,"text",i,((void *)((void *)0)));
    i++;
  }
  for (l = (results -> buttons); l != 0; l = (l -> next)) {
    PurpleNotifySearchButton *b = (l -> data);
    GtkWidget *button = (GtkWidget *)((void *)0);
    switch(b -> type){
      case PURPLE_NOTIFY_BUTTON_LABELED:
{
        if ((b -> label) != 0) {
          button = gtk_button_new_with_label((b -> label));
        }
        else {
          purple_debug_warning("gtknotify","Missing button label\n");
        }
        break; 
      }
      case PURPLE_NOTIFY_BUTTON_CONTINUE:
{
        button = gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),"gtk-go-forward",GTK_RESPONSE_NONE);
        break; 
      }
      case PURPLE_NOTIFY_BUTTON_ADD:
{
        button = gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),"gtk-add",GTK_RESPONSE_NONE);
        break; 
      }
      case PURPLE_NOTIFY_BUTTON_INFO:
{
        button = gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),"pidgin-info",GTK_RESPONSE_NONE);
        break; 
      }
      case PURPLE_NOTIFY_BUTTON_IM:
{
        button = gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),"pidgin-message-new",GTK_RESPONSE_NONE);
        break; 
      }
      case PURPLE_NOTIFY_BUTTON_JOIN:
{
        button = gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),"pidgin-chat",GTK_RESPONSE_NONE);
        break; 
      }
      case PURPLE_NOTIFY_BUTTON_INVITE:
{
        button = gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),"pidgin-invite",GTK_RESPONSE_NONE);
        break; 
      }
      default:
{
        purple_debug_warning("gtknotify","Incorrect button type: %d\n",(b -> type));
      }
    }
    if (button != ((GtkWidget *)((void *)0))) {
      PidginNotifySearchResultsButtonData *bd;
      bd = ((PidginNotifySearchResultsButtonData *)(g_malloc0_n(1,(sizeof(PidginNotifySearchResultsButtonData )))));
      bd -> button = b;
      bd -> data = data;
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )searchresults_callback_wrapper_cb),bd,0,((GConnectFlags )0));
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"destroy",((GCallback )g_free),bd,0,G_CONNECT_SWAPPED);
    }
  }
/* Add the Close button */
  close_button = gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),"gtk-close",GTK_RESPONSE_CLOSE);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)close_button),((GType )(20 << 2))))),"clicked",((GCallback )searchresults_close_cb),data,0,G_CONNECT_SWAPPED);
  data -> account = (gc -> account);
  data -> model = model;
  data -> treeview = treeview;
  data -> window = window;
/* Insert rows. */
  pidgin_notify_searchresults_new_rows(gc,results,data);
/* Show the window */
  pidgin_auto_parent_window(window);
  gtk_widget_show(window);
  return data;
}
/** Xerox'ed from Finch! How the tables have turned!! ;) **/
/** User information. **/
static GHashTable *userinfo;

static char *userinfo_hash(PurpleAccount *account,const char *who)
{
  char key[256UL];
  snprintf(key,(sizeof(key)),"%s - %s",purple_account_get_username(account),purple_normalize(account,who));
  return g_utf8_strup(key,(-1));
}

static void remove_userinfo(GtkWidget *widget,gpointer key)
{
  PidginUserInfo *pinfo = (g_hash_table_lookup(userinfo,key));
  while(pinfo -> count-- != 0)
    purple_notify_close(PURPLE_NOTIFY_USERINFO,widget);
  g_hash_table_remove(userinfo,key);
}

static void *pidgin_notify_userinfo(PurpleConnection *gc,const char *who,PurpleNotifyUserInfo *user_info)
{
  char *info;
  void *ui_handle;
  char *key = userinfo_hash(purple_connection_get_account(gc),who);
  PidginUserInfo *pinfo = (PidginUserInfo *)((void *)0);
  if (!(userinfo != 0)) {
    userinfo = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
  }
  info = purple_notify_user_info_get_text_with_newline(user_info,"<br />");
  pinfo = (g_hash_table_lookup(userinfo,key));
  if (pinfo != ((PidginUserInfo *)((void *)0))) {
    GtkIMHtml *imhtml = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(pinfo -> window)),((GType )(20 << 2))))),"info-widget"));
    char *linked_text = purple_markup_linkify(info);
    gtk_imhtml_delete(imhtml,0,0);
    gtk_imhtml_append_text_with_images(imhtml,linked_text,notify_imhtml_options(),0);
    g_free(linked_text);
    g_free(key);
    ui_handle = (pinfo -> window);
    pinfo -> count++;
  }
  else {
    char *primary = g_strdup_printf(((const char *)(dgettext("pidgin","Info for %s"))),who);
    ui_handle = pidgin_notify_formatted(((const char *)(dgettext("pidgin","Buddy Information"))),primary,0,info);
    g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ui_handle),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )formatted_close_cb),0);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ui_handle),((GType )(20 << 2))))),"destroy",((GCallback )remove_userinfo),key,0,((GConnectFlags )0));
    g_free(primary);
    pinfo = ((PidginUserInfo *)(g_malloc0_n(1,(sizeof(PidginUserInfo )))));
    pinfo -> window = ui_handle;
    pinfo -> count = 1;
    g_hash_table_insert(userinfo,key,pinfo);
  }
  g_free(info);
  return ui_handle;
}

static void pidgin_close_notify(PurpleNotifyType type,void *ui_handle)
{
  if ((type == PURPLE_NOTIFY_EMAIL) || (type == PURPLE_NOTIFY_EMAILS)) {
    PidginNotifyMailData *data = (PidginNotifyMailData *)ui_handle;
    if (data != 0) {
      g_free((data -> url));
      g_free(data);
    }
  }
  else if (type == PURPLE_NOTIFY_SEARCHRESULTS) {
    PidginNotifySearchResultsData *data = (PidginNotifySearchResultsData *)ui_handle;
    gtk_widget_destroy((data -> window));
    purple_notify_searchresults_free((data -> results));
    g_free(data);
  }
  else if (ui_handle != ((void *)((void *)0))) 
    gtk_widget_destroy(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)ui_handle),gtk_widget_get_type()))));
}
#ifndef _WIN32

static gboolean uri_command(GSList *arg_list,gboolean sync)
{
  gchar *tmp;
  GError *error = (GError *)((void *)0);
  GSList *it;
  gchar **argv;
  gint argc;
  gint i;
  gchar *program;
  do {
    if (arg_list != ((GSList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"arg_list != NULL");
      return 0;
    };
  }while (0);
  program = (arg_list -> data);
  purple_debug_misc("gtknotify","Executing %s (%s)\n",program,((sync != 0)?"sync" : "async"));
  if (!(purple_program_is_valid(program) != 0)) {
    purple_debug_error("gtknotify","Command \"%s\" is invalid\n",program);
    tmp = g_strdup_printf(((const char *)(dgettext("pidgin","The browser command \"%s\" is invalid."))),((program != 0)?program : "(null)"));
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Unable to open URL"))),tmp,0,0);
    g_free(tmp);
    return 0;
  }
  argc = (g_slist_length(arg_list));
  argv = ((gchar **)(g_malloc_n((argc + 1),(sizeof(gchar *)))));
  i = 0;
  for (it = arg_list; it != 0; it = ((it != 0)?( *((GSList *)it)).next : ((struct _GSList *)((void *)0)))) {
    if (purple_debug_is_verbose() != 0) {
      purple_debug_misc("gtknotify","argv[%d] = \"%s\"\n",i,((gchar *)(it -> data)));
    }
    argv[i++] = (it -> data);
  }
  argv[i] = ((gchar *)((void *)0));
  if (sync != 0) {
    gint exit_status = 0;
    if ((g_spawn_sync(0,argv,0,(G_SPAWN_SEARCH_PATH | G_SPAWN_STDOUT_TO_DEV_NULL | G_SPAWN_STDERR_TO_DEV_NULL),0,0,0,0,&exit_status,&error) != 0) && (exit_status == 0)) {
      g_free(argv);
      return (!0);
    }
    purple_debug_error("gtknotify","Error launching \"%s\": %s (status: %d)\n",program,((error != 0)?(error -> message) : "(null)"),exit_status);
    tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Error launching \"%s\": %s"))),program,((error != 0)?(error -> message) : "(null)"));
    g_error_free(error);
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Unable to open URL"))),tmp,0,0);
    g_free(tmp);
    g_free(argv);
    return 0;
  }
  if (g_spawn_async(0,argv,0,(G_SPAWN_SEARCH_PATH | G_SPAWN_STDOUT_TO_DEV_NULL | G_SPAWN_STDERR_TO_DEV_NULL),0,0,0,&error) != 0) {
    g_free(argv);
    return (!0);
  }
  purple_debug_warning("gtknotify","Error launching \"%s\": %s\n",program,((error != 0)?(error -> message) : "(null)"));
  g_error_free(error);
  g_free(argv);
  return 0;
}
#endif /* _WIN32 */

static void *pidgin_notify_uri(const char *uri)
{
#ifndef _WIN32
  const char *web_browser;
  int place;
  gchar *uri_escaped;
  gchar *uri_custom = (gchar *)((void *)0);
  GSList *argv = (GSList *)((void *)0);
  GSList *argv_remote = (GSList *)((void *)0);
  gchar **usercmd_argv = (gchar **)((void *)0);
/* Replace some special characters like $ with their percent-encoded
	   value. This shouldn't be necessary because we shell-escape the entire
	   arg before exec'ing the browser, however, we had a report that a URL
	   containing $(xterm) was causing xterm to start on his system. This is
	   obviously a bug on his system, but it's pretty easy for us to protect
	   against it. */
  uri_escaped = g_uri_escape_string(uri,":;/%#,+\?=&@",0);
  web_browser = purple_prefs_get_string("/pidgin/browsers/browser");
  place = purple_prefs_get_int("/pidgin/browsers/place");
/* if they are running gnome, use the gnome web browser */
  if (purple_running_gnome() == !0) {
    char *tmp = g_find_program_in_path("xdg-open");
    if (tmp == ((char *)((void *)0))) 
      argv = g_slist_append(argv,"gnome-open");
    else 
      argv = g_slist_append(argv,"xdg-open");
    g_free(tmp);
    argv = g_slist_append(argv,uri_escaped);
  }
  else if (purple_running_osx() == !0) {
    argv = g_slist_append(argv,"open");
    argv = g_slist_append(argv,uri_escaped);
  }
  else if (!(strcmp(web_browser,"epiphany") != 0) || !(strcmp(web_browser,"galeon") != 0)) {
    argv = g_slist_append(argv,((gpointer )web_browser));
    if (place == PIDGIN_BROWSER_NEW_WINDOW) 
      argv = g_slist_append(argv,"-w");
    else if (place == PIDGIN_BROWSER_NEW_TAB) 
      argv = g_slist_append(argv,"-n");
    argv = g_slist_append(argv,uri_escaped);
  }
  else if (!(strcmp(web_browser,"xdg-open") != 0)) {
    argv = g_slist_append(argv,"xdg-open");
    argv = g_slist_append(argv,uri_escaped);
  }
  else if (!(strcmp(web_browser,"gnome-open") != 0)) {
    argv = g_slist_append(argv,"gnome-open");
    argv = g_slist_append(argv,uri_escaped);
  }
  else if (!(strcmp(web_browser,"kfmclient") != 0)) {
    argv = g_slist_append(argv,"kfmclient");
    argv = g_slist_append(argv,"openURL");
    argv = g_slist_append(argv,uri_escaped);
/*
		 * Does Konqueror have options to open in new tab
		 * and/or current window?
		 */
  }
  else if (((!(strcmp(web_browser,"mozilla") != 0) || !(strcmp(web_browser,"mozilla-firebird") != 0)) || !(strcmp(web_browser,"firefox") != 0)) || !(strcmp(web_browser,"seamonkey") != 0)) {
    argv = g_slist_append(argv,((gpointer )web_browser));
    argv = g_slist_append(argv,uri_escaped);
    do {
      if (uri_custom == ((gchar *)((void *)0))) ;
      else 
        g_assertion_message_expr(0,"gtknotify.c",1344,((const char *)__func__),"uri_custom == NULL");
    }while (0);
    if (place == PIDGIN_BROWSER_NEW_WINDOW) {
      uri_custom = g_strdup_printf("openURL(%s,new-window)",uri_escaped);
    }
    else if (place == PIDGIN_BROWSER_NEW_TAB) {
      uri_custom = g_strdup_printf("openURL(%s,new-tab)",uri_escaped);
    }
    else if (place == PIDGIN_BROWSER_CURRENT) {
      uri_custom = g_strdup_printf("openURL(%s)",uri_escaped);
    }
    if (uri_custom != ((gchar *)((void *)0))) {
      argv_remote = g_slist_append(argv_remote,((gpointer )web_browser));
/* Firefox 0.9 and higher require a "-a firefox" option
			 * when using -remote commands. This breaks older
			 * versions of mozilla. So we include this other handly
			 * little string when calling firefox. If the API for
			 * remote calls changes any more in firefox then firefox
			 * should probably be split apart from mozilla-firebird
			 * and mozilla... but this is good for now.
			 */
      if (!(strcmp(web_browser,"firefox") != 0)) {
        argv_remote = g_slist_append(argv_remote,"-a");
        argv_remote = g_slist_append(argv_remote,"firefox");
      }
      argv_remote = g_slist_append(argv_remote,"-remote");
      argv_remote = g_slist_append(argv_remote,uri_custom);
    }
  }
  else if (!(strcmp(web_browser,"netscape") != 0)) {
    argv = g_slist_append(argv,"netscape");
    argv = g_slist_append(argv,uri_escaped);
    if (place == PIDGIN_BROWSER_NEW_WINDOW) {
      uri_custom = g_strdup_printf("openURL(%s,new-window)",uri_escaped);
    }
    else if (place == PIDGIN_BROWSER_CURRENT) {
      uri_custom = g_strdup_printf("openURL(%s)",uri_escaped);
    }
    if (uri_custom != 0) {
      argv_remote = g_slist_append(argv_remote,"netscape");
      argv_remote = g_slist_append(argv_remote,"-remote");
      argv_remote = g_slist_append(argv_remote,uri_custom);
    }
  }
  else if (!(strcmp(web_browser,"opera") != 0)) {
    argv = g_slist_append(argv,"opera");
    if (place == PIDGIN_BROWSER_NEW_WINDOW) 
      argv = g_slist_append(argv,"-newwindow");
    else if (place == PIDGIN_BROWSER_NEW_TAB) 
      argv = g_slist_append(argv,"-newpage");
    else if (place == PIDGIN_BROWSER_CURRENT) 
      argv = g_slist_append(argv,"-activetab");
/* `opera -remote "openURL(%s)"` command causes Pidgin's hang,
		 * if there is no Opera instance running.
		 */
    argv = g_slist_append(argv,uri_escaped);
  }
  else if (!(strcmp(web_browser,"google-chrome") != 0)) {
/* Google Chrome doesn't have command-line arguments that
		 * control the opening of links from external calls. This is
		 * controlled solely from a preference within Google Chrome.
		 */
    argv = g_slist_append(argv,"google-chrome");
    argv = g_slist_append(argv,uri_escaped);
  }
  else if (!(strcmp(web_browser,"chrome") != 0)) {
/* Chromium doesn't have command-line arguments that control
		 * the opening of links from external calls. This is controlled
		 * solely from a preference within Chromium.
		 */
    argv = g_slist_append(argv,"chrome");
    argv = g_slist_append(argv,uri_escaped);
  }
  else if (!(strcmp(web_browser,"chromium-browser") != 0)) {
/* Chromium doesn't have command-line arguments that control the
		 * opening of links from external calls. This is controlled
		 * solely from a preference within Chromium.
		 */
    argv = g_slist_append(argv,"chromium-browser");
    argv = g_slist_append(argv,uri_escaped);
  }
  else if (!(strcmp(web_browser,"custom") != 0)) {
    GError *error = (GError *)((void *)0);
    const char *usercmd_command;
    gint usercmd_argc;
    gint i;
    gboolean uri_added = 0;
    usercmd_command = purple_prefs_get_string("/pidgin/browsers/manual_command");
    if ((usercmd_command == ((const char *)((void *)0))) || (( *usercmd_command) == 0)) {
      purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Unable to open URL"))),((const char *)(dgettext("pidgin","The \'Manual\' browser command has been chosen, but no command has been set."))),0,0);
      g_free(uri_escaped);
      return 0;
    }
    if (!(g_shell_parse_argv(usercmd_command,&usercmd_argc,&usercmd_argv,&error) != 0)) {
      purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Unable to open URL: the \'Manual\' browser command seems invalid."))),((error != 0)?(error -> message) : ((char *)((void *)0))),0,0);
      g_error_free(error);
      g_free(uri_escaped);
      return 0;
    }
    for (i = 0; i < usercmd_argc; i++) {{
        gchar *cmd_part = usercmd_argv[i];
        if ((uri_added != 0) || (strstr(cmd_part,"%s") == ((char *)((void *)0)))) {
          argv = g_slist_append(argv,cmd_part);
          continue; 
        }
        uri_custom = purple_strreplace(cmd_part,"%s",uri_escaped);
        argv = g_slist_append(argv,uri_custom);
        uri_added = (!0);
      }
    }
/* There is no "%s" in the browser command. Assume the user
		 * wanted the URL tacked on to the end of the command.
		 */
    if (!(uri_added != 0)) 
      argv = g_slist_append(argv,uri_escaped);
  }
  if (argv_remote != ((GSList *)((void *)0))) {
/* try the remote command first */
    if (!(uri_command(argv_remote,(!0)) != 0)) 
      uri_command(argv,0);
  }
  else 
    uri_command(argv,0);
  g_strfreev(usercmd_argv);
  g_free(uri_escaped);
  g_free(uri_custom);
  g_slist_free(argv);
  g_slist_free(argv_remote);
#else /* !_WIN32 */
#endif /* !_WIN32 */
  return 0;
}

void pidgin_notify_pounce_add(PurpleAccount *account,PurplePounce *pounce,const char *alias,const char *event,const char *message,const char *date)
{
  GdkPixbuf *icon;
  GtkTreeIter iter;
  PidginNotifyPounceData *pounce_data;
  gboolean first = (pounce_dialog == ((PidginNotifyDialog *)((void *)0)));
  if (pounce_dialog == ((PidginNotifyDialog *)((void *)0))) 
    pounce_dialog = pidgin_create_notification_dialog(PIDGIN_NOTIFY_POUNCE);
  icon = pidgin_create_prpl_icon(account,PIDGIN_PRPL_ICON_SMALL);
  pounce_data = ((PidginNotifyPounceData *)(g_malloc_n(1,(sizeof(PidginNotifyPounceData )))));
  pounce_data -> account = account;
  pounce_data -> pounce = pounce;
  pounce_data -> pouncee = g_strdup(purple_pounce_get_pouncee(pounce));
  gtk_tree_store_append((pounce_dialog -> treemodel),&iter,0);
  gtk_tree_store_set((pounce_dialog -> treemodel),&iter,PIDGIN_POUNCE_ICON,icon,PIDGIN_POUNCE_ALIAS,alias,PIDGIN_POUNCE_EVENT,event,PIDGIN_POUNCE_TEXT,((message != ((const char *)((void *)0)))?message : ((const char *)(dgettext("pidgin","No message")))),PIDGIN_POUNCE_DATE,date,PIDGIN_POUNCE_DATA,pounce_data,-1);
  if (first != 0) {
    GtkTreeSelection *selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(pounce_dialog -> treeview)),gtk_tree_view_get_type()))));
    gtk_tree_selection_select_iter(selection,&iter);
  }
  if (icon != 0) 
    g_object_unref(icon);
  gtk_widget_show_all((pounce_dialog -> dialog));
}

static PidginNotifyDialog *pidgin_create_notification_dialog(PidginNotifyType type)
{
  GtkTreeStore *model = (GtkTreeStore *)((void *)0);
  GtkWidget *dialog = (GtkWidget *)((void *)0);
  GtkWidget *label = (GtkWidget *)((void *)0);
  GtkCellRenderer *rend;
  GtkTreeViewColumn *column;
  GtkWidget *button = (GtkWidget *)((void *)0);
  GtkWidget *vbox = (GtkWidget *)((void *)0);
  GtkTreeSelection *sel;
  PidginNotifyDialog *spec_dialog = (PidginNotifyDialog *)((void *)0);
  do {
    if (type < PIDGIN_NOTIFY_TYPES) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type < PIDGIN_NOTIFY_TYPES");
      return 0;
    };
  }while (0);
  if (type == PIDGIN_NOTIFY_MAIL) {
    do {
      if (mail_dialog == ((PidginNotifyDialog *)((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"mail_dialog == NULL");
        return mail_dialog;
      };
    }while (0);
    model = gtk_tree_store_new(COLUMNS_PIDGIN_MAIL,gdk_pixbuf_get_type(),((GType )(16 << 2)),((GType )(17 << 2)));
  }
  else if (type == PIDGIN_NOTIFY_POUNCE) {
    do {
      if (pounce_dialog == ((PidginNotifyDialog *)((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"pounce_dialog == NULL");
        return pounce_dialog;
      };
    }while (0);
    model = gtk_tree_store_new(COLUMNS_PIDGIN_POUNCE,gdk_pixbuf_get_type(),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(17 << 2)));
  }
  dialog = gtk_dialog_new();
/* Setup the dialog */
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_container_get_type()))),6);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type())))).vbox),gtk_container_get_type()))),6);
  gtk_dialog_set_has_separator(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),0);
  gtk_box_set_spacing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type())))).vbox),gtk_box_get_type()))),12);
/* Vertical box */
  vbox = ( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type())))).vbox;
/* Golden ratio it up! */
  gtk_widget_set_size_request(dialog,550,400);
  spec_dialog = ((PidginNotifyDialog *)(g_malloc0_n(1,(sizeof(PidginNotifyDialog )))));
  spec_dialog -> dialog = dialog;
  spec_dialog -> treemodel = model;
  spec_dialog -> treeview = gtk_tree_view_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)model),((GType )(20 << 2))))));
  gtk_tree_view_set_rules_hint(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(spec_dialog -> treeview)),gtk_tree_view_get_type()))),(!0));
  if (type == PIDGIN_NOTIFY_MAIL) {
    gtk_window_set_title(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_window_get_type()))),((const char *)(dgettext("pidgin","New Mail"))));
    gtk_window_set_role(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_window_get_type()))),"new_mail_detailed");
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dialog),((GType )(20 << 2))))),"focus-in-event",((GCallback )mail_window_focus_cb),0,0,((GConnectFlags )0));
    gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),((const char *)(dgettext("pidgin","Open All Messages"))),GTK_RESPONSE_ACCEPT);
    button = gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),"pidgin-stock-open-mail",GTK_RESPONSE_YES);
    spec_dialog -> open_button = button;
    gtk_tree_view_set_headers_visible(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(spec_dialog -> treeview)),gtk_tree_view_get_type()))),0);
    gtk_tree_view_set_search_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(spec_dialog -> treeview)),gtk_tree_view_get_type()))),PIDGIN_MAIL_TEXT);
    gtk_tree_view_set_search_equal_func(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(spec_dialog -> treeview)),gtk_tree_view_get_type()))),pidgin_tree_view_search_equal_func,0,0);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dialog),((GType )(20 << 2))))),"response",((GCallback )email_response_cb),spec_dialog,0,((GConnectFlags )0));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(spec_dialog -> treeview)),gtk_tree_view_get_type())))))),((GType )(20 << 2))))),"changed",((GCallback )selection_changed_cb),spec_dialog,0,((GConnectFlags )0));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(spec_dialog -> treeview)),((GType )(20 << 2))))),"row-activated",((GCallback )email_row_activated_cb),0,0,((GConnectFlags )0));
    column = gtk_tree_view_column_new();
    gtk_tree_view_column_set_resizable(column,(!0));
    rend = gtk_cell_renderer_pixbuf_new();
    gtk_tree_view_column_pack_start(column,rend,0);
    gtk_tree_view_column_set_attributes(column,rend,"pixbuf",PIDGIN_MAIL_ICON,((void *)((void *)0)));
    rend = gtk_cell_renderer_text_new();
    gtk_tree_view_column_pack_start(column,rend,(!0));
    gtk_tree_view_column_set_attributes(column,rend,"markup",PIDGIN_MAIL_TEXT,((void *)((void *)0)));
    gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(spec_dialog -> treeview)),gtk_tree_view_get_type()))),column);
    label = gtk_label_new(0);
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),((const char *)(dgettext("pidgin","<span weight=\"bold\" size=\"larger\">You have mail!</span>"))));
  }
  else if (type == PIDGIN_NOTIFY_POUNCE) {
    gtk_window_set_title(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_window_get_type()))),((const char *)(dgettext("pidgin","New Pounces"))));
    button = gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),((const char *)(dgettext("pidgin","IM"))),GTK_RESPONSE_YES);
    gtk_widget_set_sensitive(button,0);
    spec_dialog -> open_button = button;
    button = gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),"pidgin-modify",GTK_RESPONSE_APPLY);
    gtk_widget_set_sensitive(button,0);
    spec_dialog -> edit_button = button;
/* Translators: Make sure you translate "Dismiss" differently than
		   "close"!  This string is used in the "You have pounced" dialog
		   that appears when one of your Buddy Pounces is triggered.  In
		   this context "Dismiss" means "I acknowledge that I've seen that
		   this pounce was triggered--remove it from this list."  Translating
		   it as "Remove" is acceptable if you can't think of a more precise
		   word. */
    button = gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),((const char *)(dgettext("pidgin","Dismiss"))),GTK_RESPONSE_NO);
    gtk_widget_set_sensitive(button,0);
    spec_dialog -> dismiss_button = button;
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dialog),((GType )(20 << 2))))),"response",((GCallback )pounce_response_cb),spec_dialog,0,((GConnectFlags )0));
    column = gtk_tree_view_column_new();
    gtk_tree_view_column_set_title(column,((const char *)(dgettext("pidgin","Buddy"))));
    gtk_tree_view_column_set_resizable(column,(!0));
    rend = gtk_cell_renderer_pixbuf_new();
    gtk_tree_view_column_pack_start(column,rend,0);
    gtk_tree_view_column_set_attributes(column,rend,"pixbuf",PIDGIN_POUNCE_ICON,((void *)((void *)0)));
    rend = gtk_cell_renderer_text_new();
    gtk_tree_view_column_pack_start(column,rend,0);
    gtk_tree_view_column_add_attribute(column,rend,"text",PIDGIN_POUNCE_ALIAS);
    gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(spec_dialog -> treeview)),gtk_tree_view_get_type()))),column);
    column = gtk_tree_view_column_new();
    gtk_tree_view_column_set_title(column,((const char *)(dgettext("pidgin","Event"))));
    gtk_tree_view_column_set_resizable(column,(!0));
    rend = gtk_cell_renderer_text_new();
    gtk_tree_view_column_pack_start(column,rend,0);
    gtk_tree_view_column_add_attribute(column,rend,"text",PIDGIN_POUNCE_EVENT);
    gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(spec_dialog -> treeview)),gtk_tree_view_get_type()))),column);
    column = gtk_tree_view_column_new();
    gtk_tree_view_column_set_title(column,((const char *)(dgettext("pidgin","Message"))));
    gtk_tree_view_column_set_resizable(column,(!0));
    rend = gtk_cell_renderer_text_new();
    gtk_tree_view_column_pack_start(column,rend,0);
    gtk_tree_view_column_add_attribute(column,rend,"text",PIDGIN_POUNCE_TEXT);
    gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(spec_dialog -> treeview)),gtk_tree_view_get_type()))),column);
    column = gtk_tree_view_column_new();
    gtk_tree_view_column_set_title(column,((const char *)(dgettext("pidgin","Date"))));
    gtk_tree_view_column_set_resizable(column,(!0));
    rend = gtk_cell_renderer_text_new();
    gtk_tree_view_column_pack_start(column,rend,0);
    gtk_tree_view_column_add_attribute(column,rend,"text",PIDGIN_POUNCE_DATE);
    gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(spec_dialog -> treeview)),gtk_tree_view_get_type()))),column);
    label = gtk_label_new(0);
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),((const char *)(dgettext("pidgin","<span weight=\"bold\" size=\"larger\">You have pounced!</span>"))));
    sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(spec_dialog -> treeview)),gtk_tree_view_get_type()))));
    gtk_tree_selection_set_mode(sel,GTK_SELECTION_MULTIPLE);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)sel),((GType )(20 << 2))))),"changed",((GCallback )pounce_row_selected_cb),0,0,((GConnectFlags )0));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(spec_dialog -> treeview)),((GType )(20 << 2))))),"row-activated",((GCallback )pounce_response_open_ims),0,0,((GConnectFlags )0));
  }
  button = gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),"gtk-close",GTK_RESPONSE_CLOSE);
  gtk_label_set_line_wrap(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(!0));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),label,0,0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),pidgin_make_scrollable((spec_dialog -> treeview),GTK_POLICY_AUTOMATIC,GTK_POLICY_ALWAYS,GTK_SHADOW_IN,-1,-1),(!0),(!0),2);
  return spec_dialog;
}

static void signed_off_cb(PurpleConnection *gc,gpointer unused)
{
/* Clear any pending emails for this account */
  pidgin_notify_emails(gc,0,0,0,0,0,0);
  if ((mail_dialog != ((PidginNotifyDialog *)((void *)0))) && ((mail_dialog -> total_count) == 0)) 
    reset_mail_dialog(0);
}

static void *pidgin_notify_get_handle()
{
  static int handle;
  return (&handle);
}

void pidgin_notify_init()
{
  void *handle = pidgin_notify_get_handle();
  purple_signal_connect(purple_connections_get_handle(),"signed-off",handle,((PurpleCallback )signed_off_cb),0);
}

void pidgin_notify_uninit()
{
  purple_signals_disconnect_by_handle(pidgin_notify_get_handle());
}
static PurpleNotifyUiOps ops = {(pidgin_notify_message), (pidgin_notify_email), (pidgin_notify_emails), (pidgin_notify_formatted), (pidgin_notify_searchresults), (pidgin_notify_searchresults_new_rows), (pidgin_notify_userinfo), (pidgin_notify_uri), (pidgin_close_notify), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))};

PurpleNotifyUiOps *pidgin_notify_get_ui_ops()
{
  return &ops;
}
