/*
 * Themes for Pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "pidgin.h"
#include "conversation.h"
#include "debug.h"
#include "prpl.h"
#include "util.h"
#include "gtkconv.h"
#include "gtkdialogs.h"
#include "gtkimhtml.h"
#include "gtksmiley.h"
#include "gtkthemes.h"
GSList *smiley_themes = (GSList *)((void *)0);
struct smiley_theme *current_smiley_theme;
static void pidgin_themes_destroy_smiley_theme_smileys(struct smiley_theme *theme);

gboolean pidgin_themes_smileys_disabled()
{
  if (!(current_smiley_theme != 0)) 
    return 1;
  return strcmp((current_smiley_theme -> name),"none") == 0;
}

static void pidgin_themes_destroy_smiley_theme(struct smiley_theme *theme)
{
  pidgin_themes_destroy_smiley_theme_smileys(theme);
  g_free((theme -> name));
  g_free((theme -> desc));
  g_free((theme -> author));
  g_free((theme -> icon));
  g_free((theme -> path));
  g_free(theme);
}

static void pidgin_themes_remove_theme_dir(const char *theme_dir_name)
{
  GString *str = (GString *)((void *)0);
  const char *file_name = (const char *)((void *)0);
  GDir *theme_dir = (GDir *)((void *)0);
  if ((theme_dir = g_dir_open(theme_dir_name,0,0)) != ((GDir *)((void *)0))) {
    if ((str = g_string_new(theme_dir_name)) != ((GString *)((void *)0))) {
      while((file_name = g_dir_read_name(theme_dir)) != ((const char *)((void *)0))){
        g_string_printf(str,"%s%s%s",theme_dir_name,"/",file_name);
        g_unlink((str -> str));
      }
      g_string_free(str,(!0));
    }
    g_dir_close(theme_dir);
    g_rmdir(theme_dir_name);
  }
}

void pidgin_themes_remove_smiley_theme(const char *file)
{
  char *theme_dir = (char *)((void *)0);
  char *last_slash = (char *)((void *)0);
  do {
    if (((const char *)((void *)0)) != file) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"NULL != file");
      return ;
    };
  }while (0);
  if (!(g_file_test(file,G_FILE_TEST_EXISTS) != 0)) 
    return ;
  if ((theme_dir = g_strdup(file)) == ((char *)((void *)0))) 
    return ;
  if ((last_slash = g_strrstr(theme_dir,"/")) != ((char *)((void *)0))) {
    GSList *iter = (GSList *)((void *)0);
    struct smiley_theme *theme = (struct smiley_theme *)((void *)0);
    struct smiley_theme *new_theme = (struct smiley_theme *)((void *)0);
     *last_slash = 0;
/* Delete files on disk */
    pidgin_themes_remove_theme_dir(theme_dir);
{
/* Find theme in themes list and remove it */
      for (iter = smiley_themes; iter != 0; iter = (iter -> next)) {
        theme = ((struct smiley_theme *)(iter -> data));
        if (!(strcmp((theme -> path),file) != 0)) 
          break; 
      }
    }
    if (iter != 0) {
      if (theme == current_smiley_theme) {
        new_theme = ((struct smiley_theme *)(((((GSList *)((void *)0)) == (iter -> next))?(((smiley_themes == iter)?((void *)((void *)0)) : (smiley_themes -> data))) : ( *(iter -> next)).data)));
        if (new_theme != 0) 
          purple_prefs_set_string("/pidgin/smileys/theme",(new_theme -> name));
        else 
          current_smiley_theme = ((struct smiley_theme *)((void *)0));
      }
      smiley_themes = g_slist_delete_link(smiley_themes,iter);
/* Destroy theme structure */
      pidgin_themes_destroy_smiley_theme(theme);
    }
  }
  g_free(theme_dir);
}

static void _pidgin_themes_smiley_themeize(GtkWidget *imhtml,gboolean custom)
{
  struct smiley_list *list;
  if (!(current_smiley_theme != 0)) 
    return ;
  gtk_imhtml_remove_smileys(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))));
  list = (current_smiley_theme -> list);
  while(list != 0){
    char *sml = !(strcmp((list -> sml),"default") != 0)?((char *)((void *)0)) : (list -> sml);
    GSList *icons = (list -> smileys);
    while(icons != 0){
      gtk_imhtml_associate_smiley(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),sml,(icons -> data));
      icons = (icons -> next);
    }
    if (custom == !0) {
      icons = pidgin_smileys_get_all();
      while(icons != 0){
        gtk_imhtml_associate_smiley(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),sml,(icons -> data));
        icons = (icons -> next);
      }
    }
    list = (list -> next);
  }
}

void pidgin_themes_smiley_themeize(GtkWidget *imhtml)
{
  _pidgin_themes_smiley_themeize(imhtml,0);
}

void pidgin_themes_smiley_themeize_custom(GtkWidget *imhtml)
{
  _pidgin_themes_smiley_themeize(imhtml,(!0));
}

static void pidgin_themes_destroy_smiley_theme_smileys(struct smiley_theme *theme)
{
  GHashTable *already_freed;
  struct smiley_list *wer;
  already_freed = g_hash_table_new(g_direct_hash,g_direct_equal);
  for (wer = (theme -> list); wer != ((struct smiley_list *)((void *)0)); wer = (theme -> list)) {
    while((wer -> smileys) != 0){
      GtkIMHtmlSmiley *uio = ( *(wer -> smileys)).data;
      if ((uio -> imhtml) != 0) {
        g_signal_handlers_disconnect_matched((uio -> imhtml),G_SIGNAL_MATCH_DATA,0,0,0,0,uio);
      }
      if ((uio -> icon) != 0) 
        g_object_unref((uio -> icon));
      if (g_hash_table_lookup(already_freed,(uio -> file)) == ((void *)((void *)0))) {
        g_free((uio -> file));
        g_hash_table_insert(already_freed,(uio -> file),((gpointer )((gpointer )((glong )1))));
      }
      g_free((uio -> smile));
      g_free(uio);
      wer -> smileys = g_slist_delete_link((wer -> smileys),(wer -> smileys));
    }
    theme -> list = (wer -> next);
    g_free((wer -> sml));
    g_free(wer);
  }
  theme -> list = ((struct smiley_list *)((void *)0));
  g_hash_table_destroy(already_freed);
}

static void pidgin_smiley_themes_remove_non_existing()
{
  static struct smiley_theme *theme = (struct smiley_theme *)((void *)0);
  GSList *iter = (GSList *)((void *)0);
  if (!(smiley_themes != 0)) 
    return ;
  for (iter = smiley_themes; iter != 0; iter = (iter -> next)) {
    theme = ((struct smiley_theme *)(iter -> data));
    if (!(g_file_test((theme -> path),G_FILE_TEST_EXISTS) != 0)) {
      if (theme == current_smiley_theme) 
        current_smiley_theme = ((struct smiley_theme *)(((((GSList *)((void *)0)) == (iter -> next))?((void *)((void *)0)) : ( *(iter -> next)).data)));
      pidgin_themes_destroy_smiley_theme(theme);
      iter -> data = ((gpointer )((void *)0));
    }
  }
/* Remove all elements whose data is NULL */
  smiley_themes = g_slist_remove_all(smiley_themes,0);
  if (!(current_smiley_theme != 0) && (smiley_themes != 0)) {
    struct smiley_theme *smile = ( *g_slist_last(smiley_themes)).data;
    pidgin_themes_load_smiley_theme((smile -> path),(!0));
  }
}

void pidgin_themes_load_smiley_theme(const char *file,gboolean load)
{
  FILE *f = fopen(file,"rb");
  char buf[256UL];
  char *i;
  gsize line_nbr = 0;
  struct smiley_theme *theme = (struct smiley_theme *)((void *)0);
  struct smiley_list *list = (struct smiley_list *)((void *)0);
  GSList *lst = smiley_themes;
  char *dirname;
  if (!(f != 0)) 
    return ;
{
    while(lst != 0){
      struct smiley_theme *thm = (lst -> data);
      if (!(strcmp((thm -> path),file) != 0)) {
        theme = thm;
        break; 
      }
      lst = (lst -> next);
    }
  }
  if ((theme != ((struct smiley_theme *)((void *)0))) && (theme == current_smiley_theme)) {
/* Don't reload the theme if it is already loaded */
    fclose(f);
    return ;
  }
  if (theme == ((struct smiley_theme *)((void *)0))) {
    theme = ((struct smiley_theme *)(g_malloc0_n(1,(sizeof(struct smiley_theme )))));
    theme -> path = g_strdup(file);
    smiley_themes = g_slist_prepend(smiley_themes,theme);
  }
  dirname = g_path_get_dirname(file);
{
    while(!(feof(f) != 0)){
      if (!(fgets(buf,(sizeof(buf)),f) != 0)) {
        break; 
      }
      line_nbr++;
      if ((buf[0] == '#') || (buf[0] == 0)) 
        continue; 
      else {
        int len = (strlen(buf));
        while((len != 0) && ((buf[len - 1] == 13) || (buf[len - 1] == 10)))
          buf[--len] = 0;
        if (len == 0) 
          continue; 
      }
      if (!(g_utf8_validate(buf,(-1),0) != 0)) {
        purple_debug_error("gtkthemes","%s:%d is invalid UTF-8\n",file,line_nbr);
        continue; 
      }
      i = buf;
      while((( *__ctype_b_loc())[(int )( *i)] & ((unsigned short )_ISspace)) != 0)
        i++;
      if (((( *i) == '[') && (strchr(i,']') != 0)) && (load != 0)) {
        struct smiley_list *child = (struct smiley_list *)(g_malloc0_n(1,(sizeof(struct smiley_list ))));
        child -> sml = g_strndup((i + 1),((strchr(i,']') - i) - 1));
        if ((theme -> list) != 0) 
          list -> next = child;
        else 
          theme -> list = child;
/* Reverse the Smiley list since it was built in reverse order for efficiency reasons */
        if (list != ((struct smiley_list *)((void *)0))) 
          list -> smileys = g_slist_reverse((list -> smileys));
        list = child;
      }
      else if (!(g_ascii_strncasecmp(i,"Name=",strlen("Name=")) != 0)) {
        g_free((theme -> name));
        theme -> name = g_strdup((i + strlen("Name=")));
      }
      else if (!(g_ascii_strncasecmp(i,"Description=",strlen("Description=")) != 0)) {
        g_free((theme -> desc));
        theme -> desc = g_strdup((i + strlen("Description=")));
      }
      else if (!(g_ascii_strncasecmp(i,"Icon=",strlen("Icon=")) != 0)) {
        g_free((theme -> icon));
        theme -> icon = g_build_filename(dirname,(i + strlen("Icon=")),((void *)((void *)0)));
      }
      else if (!(g_ascii_strncasecmp(i,"Author=",strlen("Author=")) != 0)) {
        g_free((theme -> author));
        theme -> author = g_strdup((i + strlen("Author=")));
      }
      else if ((load != 0) && (list != 0)) {
        gboolean hidden = 0;
        char *sfile = (char *)((void *)0);
        if ((( *i) == '!') && (i[1] == 32)) {
          hidden = (!0);
          i = (i + 2);
        }
        while(( *i) != 0){
          char l[64UL];
          int li = 0;
          char *next;
{
            while(((( *i) != 0) && !((( *__ctype_b_loc())[(int )( *i)] & ((unsigned short )_ISspace)) != 0)) && (li < sizeof(l) - 1)){
              if ((( *i) == '\\') && (i[1] != 0)) 
                i++;
              next = (i + g_utf8_skip[ *((const guchar *)i)]);
              if ((next - i) > ((sizeof(l) - li) - 1)) {
                break; 
              }
              while(i != next)
                l[li++] =  *(i++);
            }
          }
          l[li] = 0;
          if (!(sfile != 0)) {
            sfile = g_build_filename(dirname,l,((void *)((void *)0)));
          }
          else {
            GtkIMHtmlSmiley *smiley = gtk_imhtml_smiley_create(sfile,l,hidden,0);
            list -> smileys = g_slist_prepend((list -> smileys),smiley);
          }
          while((( *__ctype_b_loc())[(int )( *i)] & ((unsigned short )_ISspace)) != 0)
            i++;
        }
        g_free(sfile);
      }
    }
  }
/* Reverse the Smiley list since it was built in reverse order for efficiency reasons */
  if (list != ((struct smiley_list *)((void *)0))) 
    list -> smileys = g_slist_reverse((list -> smileys));
  g_free(dirname);
  fclose(f);
  if ((!((theme -> name) != 0) || !((theme -> desc) != 0)) || !((theme -> author) != 0)) {
    purple_debug_error("gtkthemes","Invalid file format, not loading smiley theme from \'%s\'\n",file);
    smiley_themes = g_slist_remove(smiley_themes,theme);
    pidgin_themes_destroy_smiley_theme(theme);
    return ;
  }
  if (load != 0) {
    GList *cnv;
    if (current_smiley_theme != 0) 
      pidgin_themes_destroy_smiley_theme_smileys(current_smiley_theme);
    current_smiley_theme = theme;
    for (cnv = purple_get_conversations(); cnv != ((GList *)((void *)0)); cnv = (cnv -> next)) {
      PurpleConversation *conv = (cnv -> data);
      if (purple_conversation_get_ui_ops(conv) == pidgin_conversations_get_conv_ui_ops()) {
/* We want to see our custom smileys on our entry if we write the shortcut */
        pidgin_themes_smiley_themeize(( *((PidginConversation *)(conv -> ui_data))).imhtml);
        pidgin_themes_smiley_themeize_custom(( *((PidginConversation *)(conv -> ui_data))).entry);
      }
    }
  }
}

void pidgin_themes_smiley_theme_probe()
{
  GDir *dir;
  const gchar *file;
  gchar *path;
  gchar *test_path;
  int l;
  char *probedirs[3UL];
  pidgin_smiley_themes_remove_non_existing();
  probedirs[0] = g_build_filename("/usr/local/share","pixmaps","pidgin","emotes",((void *)((void *)0)));
  probedirs[1] = g_build_filename(purple_user_dir(),"smileys",((void *)((void *)0)));
  probedirs[2] = 0;
  for (l = 0; probedirs[l] != 0; l++) {
    dir = g_dir_open(probedirs[l],0,0);
    if (dir != 0) {
      while((file = g_dir_read_name(dir)) != 0){
        test_path = g_build_filename(probedirs[l],file,((void *)((void *)0)));
        if (g_file_test(test_path,G_FILE_TEST_IS_DIR) != 0) {
          path = g_build_filename(probedirs[l],file,"theme",((void *)((void *)0)));
/* Here we check to see that the theme has proper syntax.
					 * We set the second argument to FALSE so that it doesn't load
					 * the theme yet.
					 */
          pidgin_themes_load_smiley_theme(path,0);
          g_free(path);
        }
        g_free(test_path);
      }
      g_dir_close(dir);
    }
    else if (l == 1) {
      mkdir(probedirs[l],(256 | 128 | 64));
    }
    g_free(probedirs[l]);
  }
  if (!(current_smiley_theme != 0) && (smiley_themes != 0)) {
    struct smiley_theme *smile = (smiley_themes -> data);
    pidgin_themes_load_smiley_theme((smile -> path),(!0));
  }
}

GSList *pidgin_themes_get_proto_smileys(const char *id)
{
  PurplePlugin *proto;
  struct smiley_list *list;
  struct smiley_list *def;
  if ((current_smiley_theme == ((struct smiley_theme *)((void *)0))) || ((current_smiley_theme -> list) == ((struct smiley_list *)((void *)0)))) 
    return 0;
  def = (list = (current_smiley_theme -> list));
  if (id == ((const char *)((void *)0))) 
    return def -> smileys;
  proto = purple_find_prpl(id);
{
    while(list != 0){
      if (!(strcmp((list -> sml),"default") != 0)) 
        def = list;
      else if ((proto != 0) && !(strcmp(( *(proto -> info)).name,(list -> sml)) != 0)) 
        break; 
      list = (list -> next);
    }
  }
  return (list != 0)?(list -> smileys) : (def -> smileys);
}

void pidgin_themes_init()
{
  GSList *l;
  const char *current_theme = purple_prefs_get_string("/pidgin/smileys/theme");
  pidgin_themes_smiley_theme_probe();
{
    for (l = smiley_themes; l != 0; l = (l -> next)) {
      struct smiley_theme *smile = (l -> data);
      if (((smile -> name) != 0) && (strcmp(current_theme,(smile -> name)) == 0)) {
        pidgin_themes_load_smiley_theme((smile -> path),(!0));
        break; 
      }
    }
  }
/* If we still don't have a smiley theme, choose the first one */
  if (!(current_smiley_theme != 0) && (smiley_themes != 0)) {
    struct smiley_theme *smile = (smiley_themes -> data);
    pidgin_themes_load_smiley_theme((smile -> path),(!0));
  }
}
