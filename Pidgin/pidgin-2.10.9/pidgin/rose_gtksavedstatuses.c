/**
 * @file gtksavedstatus.c GTK+ Saved Status Editor UI
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "account.h"
#include "notify.h"
#include "request.h"
#include "savedstatuses.h"
#include "status.h"
#include "util.h"
#include "gtkblist.h"
#include "pidgin.h"
#include "gtkimhtml.h"
#include "gtkimhtmltoolbar.h"
#include "gtksavedstatuses.h"
#include "pidginstock.h"
#include "gtkutils.h"
/*
 * TODO: Should attach to the account-deleted and account-added signals
 *       and update the GtkListStores in any StatusEditor windows that
 *       may be open.
 */
/**
 * These are used for the GtkTreeView when you're scrolling through
 * all your saved statuses.
 */
enum __unnamed_enum___F0_L54_C1_STATUS_WINDOW_COLUMN_TITLE__COMMA__STATUS_WINDOW_COLUMN_TYPE__COMMA__STATUS_WINDOW_COLUMN_MESSAGE__COMMA__STATUS_WINDOW_COLUMN_WINDOW__COMMA__STATUS_WINDOW_COLUMN_ICON__COMMA__STATUS_WINDOW_NUM_COLUMNS {STATUS_WINDOW_COLUMN_TITLE,STATUS_WINDOW_COLUMN_TYPE,STATUS_WINDOW_COLUMN_MESSAGE,
/** A hidden column containing a pointer to the editor for this saved status. */
STATUS_WINDOW_COLUMN_WINDOW,STATUS_WINDOW_COLUMN_ICON,STATUS_WINDOW_NUM_COLUMNS};
/**
 * These are used for the GtkTreeView containing the list of accounts
 * at the bottom of the window when you're editing a particular
 * saved status.
 */
enum __unnamed_enum___F0_L70_C1_STATUS_EDITOR_COLUMN_ACCOUNT__COMMA__STATUS_EDITOR_COLUMN_WINDOW__COMMA__STATUS_EDITOR_COLUMN_ENABLE_SUBSTATUS__COMMA__STATUS_EDITOR_COLUMN_ICON__COMMA__STATUS_EDITOR_COLUMN_USERNAME__COMMA__STATUS_EDITOR_COLUMN_STATUS_ID__COMMA__STATUS_EDITOR_COLUMN_STATUS_NAME__COMMA__STATUS_EDITOR_COLUMN_STATUS_MESSAGE__COMMA__STATUS_EDITOR_COLUMN_STATUS_ICON__COMMA__STATUS_EDITOR_NUM_COLUMNS {
/** A hidden column containing a pointer to the PurpleAccount. */
STATUS_EDITOR_COLUMN_ACCOUNT,
/** A hidden column containing a pointer to the editor for this substatus. */
STATUS_EDITOR_COLUMN_WINDOW,STATUS_EDITOR_COLUMN_ENABLE_SUBSTATUS,STATUS_EDITOR_COLUMN_ICON,STATUS_EDITOR_COLUMN_USERNAME,
/** A hidden column containing the ID of this PurpleStatusType. */
STATUS_EDITOR_COLUMN_STATUS_ID,STATUS_EDITOR_COLUMN_STATUS_NAME,STATUS_EDITOR_COLUMN_STATUS_MESSAGE,STATUS_EDITOR_COLUMN_STATUS_ICON,STATUS_EDITOR_NUM_COLUMNS};
/**
 * These are used in the GtkComboBox to select the specific PurpleStatusType
 * when setting a (sub)status for a particular saved status.
 */
enum __unnamed_enum___F0_L91_C1_STATUS_COLUMN_ICON__COMMA__STATUS_COLUMN_STATUS_ID__COMMA__STATUS_COLUMN_STATUS_NAME__COMMA__STATUS_NUM_COLUMNS {STATUS_COLUMN_ICON,
/** A hidden column containing the ID of this PurpleStatusType. */
STATUS_COLUMN_STATUS_ID,STATUS_COLUMN_STATUS_NAME,STATUS_NUM_COLUMNS};
typedef struct __unnamed_class___F0_L100_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1184R__Pe___variable_name_unknown_scope_and_name__scope__model__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__treeview__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__use_button__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__modify_button__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__delete_button {
GtkWidget *window;
GtkListStore *model;
GtkWidget *treeview;
GtkWidget *use_button;
GtkWidget *modify_button;
GtkWidget *delete_button;}StatusWindow;
typedef struct __unnamed_class___F0_L110_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1184R__Pe___variable_name_unknown_scope_and_name__scope__model__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__treeview__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkButton_GtkButton__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__saveanduse_button__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkButton_GtkButton__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__save_button__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__original_title__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkEntry_GtkEntry__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__title__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1209R__Pe___variable_name_unknown_scope_and_name__scope__type__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkIMHtml_GtkIMHtml__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__message {
GtkWidget *window;
GtkListStore *model;
GtkWidget *treeview;
GtkButton *saveanduse_button;
GtkButton *save_button;
gchar *original_title;
GtkEntry *title;
GtkComboBox *type;
GtkIMHtml *message;}StatusEditor;
typedef struct __unnamed_class___F0_L124_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__StatusEditorL1657R__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__status_editor__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L247R__Pe___variable_name_unknown_scope_and_name__scope__account__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1184R__Pe___variable_name_unknown_scope_and_name__scope__model__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1209R__Pe___variable_name_unknown_scope_and_name__scope__box__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkIMHtml_GtkIMHtml__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__message__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1601R__Pe___variable_name_unknown_scope_and_name__scope__toolbar {
StatusEditor *status_editor;
PurpleAccount *account;
GtkWidget *window;
GtkListStore *model;
GtkComboBox *box;
GtkIMHtml *message;
GtkIMHtmlToolbar *toolbar;}SubStatusEditor;
static StatusWindow *status_window = (StatusWindow *)((void *)0);
/**************************************************************************
* Status window
**************************************************************************/

static gboolean status_window_find_savedstatus(GtkTreeIter *iter,const char *title)
{
  GtkTreeModel *model;
  char *cur;
  if ((status_window == ((StatusWindow *)((void *)0))) || (title == ((const char *)((void *)0)))) 
    return 0;
  model = ((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_window -> model)),gtk_tree_model_get_type())));
  if (!(gtk_tree_model_get_iter_first(model,iter) != 0)) 
    return 0;
  do {
    gtk_tree_model_get(model,iter,STATUS_WINDOW_COLUMN_TITLE,&cur,-1);
    if (!(strcmp(title,cur) != 0)) {
      g_free(cur);
      return (!0);
    }
    g_free(cur);
  }while (gtk_tree_model_iter_next(model,iter) != 0);
  return 0;
}

static gboolean status_window_destroy_cb(GtkWidget *widget,GdkEvent *event,gpointer user_data)
{
  StatusWindow *dialog = user_data;
  dialog -> window = ((GtkWidget *)((void *)0));
  pidgin_status_window_hide();
  return 0;
}

static void status_window_use_cb(GtkButton *button,StatusWindow *dialog)
{
  GtkTreeSelection *selection;
  GtkTreeIter iter;
  GList *list = (GList *)((void *)0);
  int num_selected = 0;
  selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treeview)),gtk_tree_view_get_type()))));
  num_selected = gtk_tree_selection_count_selected_rows(selection);
  if (num_selected != 1) 
/*
		 * This shouldn't happen because the "Use" button should have
		 * been grayed out.  Oh well.
		 */
    return ;
  list = gtk_tree_selection_get_selected_rows(selection,0);
  if (gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter,(list -> data)) != 0) {
    gchar *title;
    PurpleSavedStatus *saved_status;
    gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter,STATUS_WINDOW_COLUMN_TITLE,&title,-1);
    saved_status = purple_savedstatus_find(title);
    g_free(title);
    purple_savedstatus_activate(saved_status);
  }
  g_list_foreach(list,((GFunc )gtk_tree_path_free),0);
  g_list_free(list);
}

static void status_window_add_cb(GtkButton *button,gpointer user_data)
{
  pidgin_status_editor_show(0,0);
}

static void status_window_modify_foreach(GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter,gpointer user_data)
{
  gchar *title;
  PurpleSavedStatus *saved_status;
  gtk_tree_model_get(model,iter,STATUS_WINDOW_COLUMN_TITLE,&title,-1);
  saved_status = purple_savedstatus_find(title);
  g_free(title);
  pidgin_status_editor_show((!0),saved_status);
}

static void status_window_modify_cb(GtkButton *button,gpointer user_data)
{
  StatusWindow *dialog = user_data;
  GtkTreeSelection *selection;
  selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treeview)),gtk_tree_view_get_type()))));
  gtk_tree_selection_selected_foreach(selection,status_window_modify_foreach,user_data);
}

static void status_window_delete_cancel_cb(gpointer data)
{
  GList *sel_titles = data;
  g_list_foreach(sel_titles,((GFunc )g_free),0);
  g_list_free(sel_titles);
}

static void status_window_delete_confirm_cb(gpointer data)
{
  GtkTreeIter iter;
  GList *sel_titles = data;
  GList *l;
  char *title;
  for (l = sel_titles; l != ((GList *)((void *)0)); l = (l -> next)) {
    title = (l -> data);
    if (purple_savedstatus_find(title) != purple_savedstatus_get_current()) {
      if (status_window_find_savedstatus(&iter,title) != 0) 
        gtk_list_store_remove((status_window -> model),&iter);
      purple_savedstatus_delete(title);
    }
    g_free(title);
  }
  g_list_free(sel_titles);
}

static void status_window_delete_cb(GtkButton *button,gpointer user_data)
{
  StatusWindow *dialog = user_data;
  GtkTreeIter iter;
  GtkTreeSelection *selection;
  GList *sel_paths;
  GList *l;
  GList *sel_titles = (GList *)((void *)0);
  GtkTreeModel *model = (GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()));
  char *title;
  gpointer handle;
  selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treeview)),gtk_tree_view_get_type()))));
  sel_paths = gtk_tree_selection_get_selected_rows(selection,0);
/* This is ugly because we're not allowed to modify the model from within
	 * gtk_tree_selection_selected_foreach() and the GtkTreePaths can become invalid
	 * when something is removed from the model.  The selection can also change while
	 * the request dialog is displayed, so we need to capture the selected rows at this time. */
  for (l = sel_paths; l != ((GList *)((void *)0)); l = (l -> next)) {
    if (gtk_tree_model_get_iter(model,&iter,(l -> data)) != 0) {
      gtk_tree_model_get(model,&iter,STATUS_WINDOW_COLUMN_TITLE,&title,-1);
      sel_titles = g_list_prepend(sel_titles,title);
    }
    gtk_tree_path_free((l -> data));
  }
  g_list_free(sel_paths);
  do {
    if (sel_titles != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"sel_titles != NULL");
      return ;
    };
  }while (0);
  if (!((sel_titles -> next) != 0)) {
    title = g_strdup_printf(((const char *)(dgettext("pidgin","Are you sure you want to delete %s\?"))),((const gchar *)(sel_titles -> data)));
    handle = (purple_savedstatus_find((sel_titles -> data)));
  }
  else {
    title = g_strdup(((const char *)(dgettext("pidgin","Are you sure you want to delete the selected saved statuses\?"))));
    handle = dialog;
  }
  purple_request_action(handle,0,title,0,0,0,0,0,sel_titles,2,((const char *)(dgettext("pidgin","Delete"))),status_window_delete_confirm_cb,((const char *)(dgettext("pidgin","Cancel"))),status_window_delete_cancel_cb);
  g_free(title);
}

static void status_window_close_cb(GtkButton *button,gpointer user_data)
{
  pidgin_status_window_hide();
}

static void status_selected_cb(GtkTreeSelection *sel,gpointer user_data)
{
  StatusWindow *dialog = user_data;
  GList *sel_paths;
  GList *tmp;
  gboolean can_use = (!0);
  gboolean can_delete = (!0);
  int num_selected;
  GtkTreeModel *model = (GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()));
  sel_paths = gtk_tree_selection_get_selected_rows(sel,0);
  for (((tmp = sel_paths) , (num_selected = 0)); tmp != 0; ((tmp = (tmp -> next)) , num_selected++)) {
    GtkTreeIter iter;
    char *title;
    if (gtk_tree_model_get_iter(model,&iter,(tmp -> data)) != 0) {
      gtk_tree_model_get(model,&iter,STATUS_WINDOW_COLUMN_TITLE,&title,-1);
      if (purple_savedstatus_find(title) == purple_savedstatus_get_current()) {
        can_use = (can_delete = 0);
      }
      g_free(title);
    }
    gtk_tree_path_free((tmp -> data));
  }
  gtk_widget_set_sensitive((dialog -> use_button),((num_selected == 1) && (can_use != 0)));
  gtk_widget_set_sensitive((dialog -> modify_button),(num_selected > 0));
  gtk_widget_set_sensitive((dialog -> delete_button),((num_selected > 0) && (can_delete != 0)));
  g_list_free(sel_paths);
}

static const gchar *get_stock_icon_from_primitive(PurpleStatusPrimitive type)
{
  return pidgin_stock_id_from_status_primitive(type);
}

static void add_status_to_saved_status_list(GtkListStore *model,PurpleSavedStatus *saved_status)
{
  GtkTreeIter iter;
  const char *title;
  const char *type;
  const gchar *icon;
  char *message;
  if (purple_savedstatus_is_transient(saved_status) != 0) 
    return ;
  title = purple_savedstatus_get_title(saved_status);
  type = purple_primitive_get_name_from_type(purple_savedstatus_get_type(saved_status));
  message = purple_markup_strip_html(purple_savedstatus_get_message(saved_status));
  icon = get_stock_icon_from_primitive(purple_savedstatus_get_type(saved_status));
  gtk_list_store_append(model,&iter);
  gtk_list_store_set(model,&iter,STATUS_WINDOW_COLUMN_ICON,icon,STATUS_WINDOW_COLUMN_TITLE,title,STATUS_WINDOW_COLUMN_TYPE,type,STATUS_WINDOW_COLUMN_MESSAGE,message,-1);
  g_free(message);
}

static void populate_saved_status_list(StatusWindow *dialog)
{
  GList *saved_statuses;
  gtk_list_store_clear((dialog -> model));
  for (saved_statuses = purple_savedstatuses_get_all(); saved_statuses != ((GList *)((void *)0)); saved_statuses = ((saved_statuses != 0)?( *((GList *)saved_statuses)).next : ((struct _GList *)((void *)0)))) {
    add_status_to_saved_status_list((dialog -> model),(saved_statuses -> data));
  }
}

static gboolean search_func(GtkTreeModel *model,gint column,const gchar *key,GtkTreeIter *iter,gpointer search_data)
{
  gboolean result;
  char *haystack;
  gtk_tree_model_get(model,iter,column,&haystack,-1);
  result = (purple_strcasestr(haystack,key) == ((const char *)((void *)0)));
  g_free(haystack);
  return result;
}

static void savedstatus_activated_cb(GtkTreeView *view,GtkTreePath *path,GtkTreeViewColumn *column,StatusWindow *dialog)
{
  status_window_use_cb(0,dialog);
  status_window_close_cb(0,dialog);
}

static void saved_status_updated_cb(PurpleSavedStatus *status,StatusWindow *sw)
{
  populate_saved_status_list(sw);
}

static GtkWidget *create_saved_status_list(StatusWindow *dialog)
{
  GtkWidget *treeview;
  GtkTreeSelection *sel;
  GtkTreeViewColumn *column;
  GtkCellRenderer *renderer;
/* Create the list model */
  dialog -> model = gtk_list_store_new(STATUS_WINDOW_NUM_COLUMNS,((GType )(16 << 2)),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(17 << 2)),((GType )(16 << 2)));
/* Create the treeview */
  treeview = gtk_tree_view_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))));
  dialog -> treeview = treeview;
  gtk_tree_view_set_rules_hint(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),(!0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)treeview),((GType )(20 << 2))))),"row-activated",((GCallback )savedstatus_activated_cb),dialog,0,((GConnectFlags )0));
  sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))));
  gtk_tree_selection_set_mode(sel,GTK_SELECTION_MULTIPLE);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)sel),((GType )(20 << 2))))),"changed",((GCallback )status_selected_cb),dialog,0,((GConnectFlags )0));
/* Add columns */
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_title(column,((const char *)(dgettext("pidgin","Title"))));
  gtk_tree_view_column_set_resizable(column,(!0));
  gtk_tree_view_column_set_min_width(column,100);
  gtk_tree_view_column_set_sort_column_id(column,STATUS_WINDOW_COLUMN_TITLE);
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),column);
  renderer = gtk_cell_renderer_text_new();
  gtk_tree_view_column_pack_start(column,renderer,(!0));
  gtk_tree_view_column_add_attribute(column,renderer,"text",STATUS_WINDOW_COLUMN_TITLE);
  g_object_set(renderer,"ellipsize",PANGO_ELLIPSIZE_END,((void *)((void *)0)));
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_title(column,((const char *)(dgettext("pidgin","Type"))));
  gtk_tree_view_column_set_resizable(column,(!0));
  gtk_tree_view_column_set_sort_column_id(column,STATUS_WINDOW_COLUMN_TYPE);
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),column);
  renderer = gtk_cell_renderer_pixbuf_new();
  gtk_tree_view_column_pack_start(column,renderer,(!0));
  gtk_tree_view_column_add_attribute(column,renderer,"stock-id",STATUS_WINDOW_COLUMN_ICON);
  renderer = gtk_cell_renderer_text_new();
  gtk_tree_view_column_pack_start(column,renderer,(!0));
  gtk_tree_view_column_add_attribute(column,renderer,"text",STATUS_WINDOW_COLUMN_TYPE);
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_title(column,((const char *)(dgettext("pidgin","Message"))));
  gtk_tree_view_column_set_resizable(column,(!0));
  gtk_tree_view_column_set_sort_column_id(column,STATUS_WINDOW_COLUMN_MESSAGE);
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),column);
  renderer = gtk_cell_renderer_text_new();
  gtk_tree_view_column_pack_start(column,renderer,(!0));
  gtk_tree_view_column_add_attribute(column,renderer,"text",STATUS_WINDOW_COLUMN_MESSAGE);
  g_object_set(renderer,"ellipsize",PANGO_ELLIPSIZE_END,((void *)((void *)0)));
/* Enable CTRL+F searching */
  gtk_tree_view_set_search_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),STATUS_WINDOW_COLUMN_TITLE);
  gtk_tree_view_set_search_equal_func(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),search_func,0,0);
/* Sort the title column by default */
  gtk_tree_sortable_set_sort_column_id(((GtkTreeSortable *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_sortable_get_type()))),STATUS_WINDOW_COLUMN_TITLE,GTK_SORT_ASCENDING);
/* Populate list */
  populate_saved_status_list(dialog);
  gtk_widget_show_all(treeview);
  return pidgin_make_scrollable(treeview,GTK_POLICY_AUTOMATIC,GTK_POLICY_ALWAYS,GTK_SHADOW_IN,-1,-1);
}

static gboolean configure_cb(GtkWidget *widget,GdkEventConfigure *event,StatusWindow *dialog)
{
  if ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_object_get_type())))).flags & GTK_VISIBLE) != 0) {
    purple_prefs_set_int("/pidgin/status/dialog/width",(event -> width));
    purple_prefs_set_int("/pidgin/status/dialog/height",(event -> height));
  }
  return 0;
}

static void current_status_changed(PurpleSavedStatus *old,PurpleSavedStatus *new_status,StatusWindow *dialog)
{
  status_selected_cb(gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treeview)),gtk_tree_view_get_type())))),dialog);
}

void pidgin_status_window_show()
{
  StatusWindow *dialog;
  GtkWidget *bbox;
  GtkWidget *button;
  GtkWidget *list;
  GtkWidget *vbox;
  GtkWidget *win;
  int width;
  int height;
  if (status_window != ((StatusWindow *)((void *)0))) {
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(status_window -> window)),gtk_window_get_type()))));
    return ;
  }
  status_window = (dialog = ((StatusWindow *)(g_malloc0_n(1,(sizeof(StatusWindow ))))));
  width = purple_prefs_get_int("/pidgin/status/dialog/width");
  height = purple_prefs_get_int("/pidgin/status/dialog/height");
  dialog -> window = (win = pidgin_create_dialog(((const char *)(dgettext("pidgin","Saved Statuses"))),12,"statuses",(!0)));
  gtk_window_set_default_size(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_window_get_type()))),width,height);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"delete_event",((GCallback )status_window_destroy_cb),dialog,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"configure_event",((GCallback )configure_cb),dialog,0,((GConnectFlags )0));
/* Setup the vbox */
  vbox = pidgin_dialog_get_vbox_with_properties(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),0,12);
/* List of saved status states */
  list = create_saved_status_list(dialog);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),list,(!0),(!0),0);
/* Button box. */
  bbox = pidgin_dialog_get_action_area(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))));
/* Use button */
  button = pidgin_pixbuf_button_from_stock(((const char *)(dgettext("pidgin","_Use"))),"gtk-execute",PIDGIN_BUTTON_HORIZONTAL);
  dialog -> use_button = button;
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),button,0,0,0);
  gtk_widget_set_sensitive(button,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )status_window_use_cb),dialog,0,((GConnectFlags )0));
/* Add button */
  pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),"pidgin-add",((GCallback )status_window_add_cb),dialog);
/* Modify button */
  button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),"pidgin-modify",((GCallback )status_window_modify_cb),dialog);
  dialog -> modify_button = button;
  gtk_widget_set_sensitive(button,0);
/* Delete button */
  button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),"gtk-delete",((GCallback )status_window_delete_cb),dialog);
  dialog -> delete_button = button;
  gtk_widget_set_sensitive(button,0);
/* Close button */
  pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),"gtk-close",((GCallback )status_window_close_cb),dialog);
  purple_signal_connect(purple_savedstatuses_get_handle(),"savedstatus-changed",status_window,((PurpleCallback )current_status_changed),dialog);
  purple_signal_connect(purple_savedstatuses_get_handle(),"savedstatus-added",status_window,((PurpleCallback )saved_status_updated_cb),dialog);
  purple_signal_connect(purple_savedstatuses_get_handle(),"savedstatus-deleted",status_window,((PurpleCallback )saved_status_updated_cb),dialog);
  purple_signal_connect(purple_savedstatuses_get_handle(),"savedstatus-modified",status_window,((PurpleCallback )saved_status_updated_cb),dialog);
  gtk_widget_show_all(win);
}

void pidgin_status_window_hide()
{
  if (status_window == ((StatusWindow *)((void *)0))) 
    return ;
  if ((status_window -> window) != ((GtkWidget *)((void *)0))) 
    gtk_widget_destroy((status_window -> window));
  purple_request_close_with_handle(status_window);
  purple_notify_close_with_handle(status_window);
  purple_signals_disconnect_by_handle(status_window);
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_window -> model)),((GType )(20 << 2))))));
  g_free(status_window);
  status_window = ((StatusWindow *)((void *)0));
}
/**************************************************************************
* Status editor
**************************************************************************/
static void substatus_editor_cancel_cb(GtkButton *button,gpointer user_data);

static void status_editor_remove_dialog(StatusEditor *dialog)
{
  GtkTreeModel *model;
  GtkTreeIter iter;
/* Remove the reference to this dialog from our parent's list store */
  if (status_window_find_savedstatus(&iter,(dialog -> original_title)) != 0) {
    gtk_list_store_set((status_window -> model),&iter,STATUS_WINDOW_COLUMN_WINDOW,((void *)((void *)0)),-1);
  }
/* Close any substatus editors that may be open */
  model = ((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type())));
  if (gtk_tree_model_get_iter_first(model,&iter) != 0) {
    do {
      SubStatusEditor *substatus_dialog;
      gtk_tree_model_get(model,&iter,STATUS_EDITOR_COLUMN_WINDOW,&substatus_dialog,-1);
      if (substatus_dialog != ((SubStatusEditor *)((void *)0))) {
        gtk_list_store_set((dialog -> model),&iter,STATUS_EDITOR_COLUMN_WINDOW,((void *)((void *)0)),-1);
        substatus_editor_cancel_cb(0,substatus_dialog);
      }
    }while (gtk_tree_model_iter_next(model,&iter) != 0);
  }
}

static void status_editor_destroy_cb(GtkWidget *widget,gpointer user_data)
{
  StatusEditor *dialog = user_data;
  status_editor_remove_dialog(dialog);
  g_free((dialog -> original_title));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),((GType )(20 << 2))))));
  g_free(dialog);
}

static void status_editor_cancel_cb(GtkButton *button,gpointer user_data)
{
  StatusEditor *dialog = user_data;
  gtk_widget_destroy((dialog -> window));
}

static void status_editor_ok_cb(GtkButton *button,gpointer user_data)
{
  StatusEditor *dialog = user_data;
  const char *title;
  PurpleStatusPrimitive type;
  char *message;
  char *unformatted;
  PurpleSavedStatus *saved_status = (PurpleSavedStatus *)((void *)0);
  GtkTreeModel *model;
  GtkTreeIter iter;
  title = gtk_entry_get_text((dialog -> title));
/*
	 * If we're saving this status, and the title is already taken
	 * then show an error dialog and don't do anything.
	 */
  if ((((button == (dialog -> saveanduse_button)) || (button == (dialog -> save_button))) && (purple_savedstatus_find(title) != ((PurpleSavedStatus *)((void *)0)))) && (((dialog -> original_title) == ((gchar *)((void *)0))) || (strcmp(title,(dialog -> original_title)) != 0))) {
    purple_notify_message(status_window,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Title already in use.  You must choose a unique title."))),0,0,0);
    return ;
  }
  type = (gtk_combo_box_get_active((dialog -> type)) + (PURPLE_STATUS_UNSET + 1));
  message = gtk_imhtml_get_markup((dialog -> message));
  unformatted = purple_markup_strip_html(message);
/*
	 * If we're editing an old status, then lookup the old status.
	 * Note: It is possible that it has been deleted or renamed
	 *       or something, and no longer exists.
	 */
  if ((dialog -> original_title) != ((gchar *)((void *)0))) {
    GtkTreeIter iter;
    saved_status = purple_savedstatus_find((dialog -> original_title));
    if (status_window_find_savedstatus(&iter,(dialog -> original_title)) != 0) 
      gtk_list_store_remove((status_window -> model),&iter);
  }
  if (saved_status == ((PurpleSavedStatus *)((void *)0))) {
/* This is a new status */
    if ((button == (dialog -> saveanduse_button)) || (button == (dialog -> save_button))) 
      saved_status = purple_savedstatus_new(title,type);
    else 
      saved_status = purple_savedstatus_new(0,type);
  }
  else {
/* Modify the old status */
    if (strcmp(title,(dialog -> original_title)) != 0) 
      purple_savedstatus_set_title(saved_status,title);
    purple_savedstatus_set_type(saved_status,type);
  }
  if (( *unformatted) == 0) 
    purple_savedstatus_set_message(saved_status,0);
  else 
    purple_savedstatus_set_message(saved_status,message);
/* Set any substatuses */
  model = ((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type())));
  if (gtk_tree_model_get_iter_first(model,&iter) != 0) {
    do {
      PurpleAccount *account;
      gboolean enabled;
      char *id;
      char *message;
      PurpleStatusType *type;
      gtk_tree_model_get(model,&iter,STATUS_EDITOR_COLUMN_ACCOUNT,&account,STATUS_EDITOR_COLUMN_ENABLE_SUBSTATUS,&enabled,STATUS_EDITOR_COLUMN_STATUS_ID,&id,STATUS_EDITOR_COLUMN_STATUS_MESSAGE,&message,-1);
      if (enabled != 0) {
        type = purple_account_get_status_type(account,id);
        purple_savedstatus_set_substatus(saved_status,account,type,message);
      }
      else {
        purple_savedstatus_unset_substatus(saved_status,account);
      }
      g_free(id);
      g_free(message);
    }while (gtk_tree_model_iter_next(model,&iter) != 0);
  }
  g_free(message);
  g_free(unformatted);
/* If they clicked on "Save & Use" or "Use," then activate the status */
  if (button != (dialog -> save_button)) 
    purple_savedstatus_activate(saved_status);
  gtk_widget_destroy((dialog -> window));
}

static void editor_title_changed_cb(GtkWidget *widget,gpointer user_data)
{
  StatusEditor *dialog = user_data;
  const gchar *text;
  text = gtk_entry_get_text((dialog -> title));
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> saveanduse_button)),gtk_widget_get_type()))),(( *text) != 0));
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> save_button)),gtk_widget_get_type()))),(( *text) != 0));
}

static GtkWidget *create_status_type_menu(PurpleStatusPrimitive type)
{
  int i;
  GtkWidget *dropdown;
  GtkListStore *store;
  GtkTreeIter iter;
  GtkCellRenderer *renderer;
  store = gtk_list_store_new(STATUS_NUM_COLUMNS,((GType )(16 << 2)),((GType )(16 << 2)),((GType )(16 << 2)));
  for (i = PURPLE_STATUS_UNSET + 1; i < PURPLE_STATUS_NUM_PRIMITIVES; i++) {
/* Someone should fix this for 3.0.0.  The independent boolean
		 * should probably be set on the status type, not the status.
		 * I guess that would prevent third party plugins from creating
		 * independent statuses?
		 */
    if (((i == PURPLE_STATUS_MOBILE) || (i == PURPLE_STATUS_MOOD)) || (i == PURPLE_STATUS_TUNE)) 
/*
			 * Special-case these.  They're intended to be independent
			 * status types, so don't show them in the list.
			 */
      continue; 
    gtk_list_store_append(store,&iter);
    gtk_list_store_set(store,&iter,STATUS_COLUMN_ICON,get_stock_icon_from_primitive(i),STATUS_COLUMN_STATUS_ID,purple_primitive_get_id_from_type(i),STATUS_COLUMN_STATUS_NAME,purple_primitive_get_name_from_type(i),-1);
  }
  dropdown = gtk_combo_box_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)store),gtk_tree_model_get_type()))));
  renderer = gtk_cell_renderer_pixbuf_new();
  gtk_cell_layout_pack_start(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)dropdown),gtk_cell_layout_get_type()))),renderer,0);
  gtk_cell_layout_set_attributes(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)dropdown),gtk_cell_layout_get_type()))),renderer,"stock-id",STATUS_COLUMN_ICON,((void *)((void *)0)));
  renderer = gtk_cell_renderer_text_new();
  gtk_cell_layout_pack_start(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)dropdown),gtk_cell_layout_get_type()))),renderer,(!0));
  gtk_cell_layout_set_attributes(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)dropdown),gtk_cell_layout_get_type()))),renderer,"text",STATUS_COLUMN_STATUS_NAME,((void *)((void *)0)));
  gtk_combo_box_set_active(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)dropdown),gtk_combo_box_get_type()))),(type - (PURPLE_STATUS_UNSET + 1)));
  return dropdown;
}
static void edit_substatus(StatusEditor *status_editor,PurpleAccount *account);

static void edit_substatus_cb(GtkTreeView *tv,GtkTreePath *path,GtkTreeViewColumn *col,gpointer user_data)
{
  StatusEditor *dialog = user_data;
  GtkTreeIter iter;
  PurpleAccount *account;
  gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter,path);
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter,STATUS_EDITOR_COLUMN_ACCOUNT,&account,-1);
  edit_substatus(dialog,account);
}

static void status_editor_substatus_cb(GtkCellRendererToggle *renderer,gchar *path_str,gpointer data)
{
  StatusEditor *dialog = (StatusEditor *)data;
  GtkTreeIter iter;
  gboolean enabled;
  PurpleAccount *account;
  gtk_tree_model_get_iter_from_string(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter,path_str);
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter,STATUS_EDITOR_COLUMN_ACCOUNT,&account,STATUS_EDITOR_COLUMN_ENABLE_SUBSTATUS,&enabled,-1);
  enabled = !(enabled != 0);
  if (enabled != 0) {
    edit_substatus(dialog,account);
  }
  else {
/* Remove the substatus */
    gtk_list_store_set((dialog -> model),&iter,STATUS_EDITOR_COLUMN_ENABLE_SUBSTATUS,enabled,STATUS_EDITOR_COLUMN_STATUS_ID,((void *)((void *)0)),STATUS_EDITOR_COLUMN_STATUS_NAME,((void *)((void *)0)),STATUS_EDITOR_COLUMN_STATUS_MESSAGE,((void *)((void *)0)),STATUS_EDITOR_COLUMN_STATUS_ICON,((void *)((void *)0)),-1);
  }
}

static void status_editor_add_columns(StatusEditor *dialog)
{
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;
/* Enable Different status column */
  renderer = gtk_cell_renderer_toggle_new();
  gtk_tree_view_insert_column_with_attributes(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treeview)),gtk_tree_view_get_type()))),(-1),((const char *)(dgettext("pidgin","Different"))),renderer,"active",STATUS_EDITOR_COLUMN_ENABLE_SUBSTATUS,((void *)((void *)0)));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)renderer),((GType )(20 << 2))))),"toggled",((GCallback )status_editor_substatus_cb),dialog,0,((GConnectFlags )0));
/* Username column */
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column,(!0));
  gtk_tree_view_column_set_title(column,((const char *)(dgettext("pidgin","Username"))));
  gtk_tree_view_insert_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treeview)),gtk_tree_view_get_type()))),column,(-1));
  gtk_tree_view_column_set_resizable(column,(!0));
/* Icon */
  renderer = gtk_cell_renderer_pixbuf_new();
  gtk_tree_view_column_pack_start(column,renderer,0);
  gtk_tree_view_column_add_attribute(column,renderer,"pixbuf",STATUS_EDITOR_COLUMN_ICON);
/* Username */
  renderer = gtk_cell_renderer_text_new();
  gtk_tree_view_column_pack_start(column,renderer,(!0));
  gtk_tree_view_column_add_attribute(column,renderer,"text",STATUS_EDITOR_COLUMN_USERNAME);
/* Status column */
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column,(!0));
  gtk_tree_view_column_set_title(column,((const char *)(dgettext("pidgin","Status"))));
  gtk_tree_view_insert_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treeview)),gtk_tree_view_get_type()))),column,(-1));
  gtk_tree_view_column_set_resizable(column,(!0));
  renderer = gtk_cell_renderer_pixbuf_new();
  gtk_tree_view_column_pack_start(column,renderer,0);
  gtk_tree_view_column_add_attribute(column,renderer,"stock-id",STATUS_EDITOR_COLUMN_STATUS_ICON);
  renderer = gtk_cell_renderer_text_new();
  gtk_tree_view_column_pack_start(column,renderer,(!0));
  gtk_tree_view_column_add_attribute(column,renderer,"text",STATUS_EDITOR_COLUMN_STATUS_NAME);
/* Message column */
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_resizable(column,(!0));
  gtk_tree_view_column_set_title(column,((const char *)(dgettext("pidgin","Message"))));
  gtk_tree_view_insert_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treeview)),gtk_tree_view_get_type()))),column,(-1));
  gtk_tree_view_column_set_resizable(column,(!0));
  renderer = gtk_cell_renderer_text_new();
  gtk_tree_view_column_pack_start(column,renderer,(!0));
  gtk_tree_view_column_add_attribute(column,renderer,"text",STATUS_EDITOR_COLUMN_STATUS_MESSAGE);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treeview)),((GType )(20 << 2))))),"row-activated",((GCallback )edit_substatus_cb),dialog,0,((GConnectFlags )0));
}

static void status_editor_set_account(GtkListStore *store,PurpleAccount *account,GtkTreeIter *iter,PurpleSavedStatusSub *substatus)
{
  GdkPixbuf *pixbuf;
  const char *id = (const char *)((void *)0);
  const char *name = (const char *)((void *)0);
  const char *message = (const char *)((void *)0);
  PurpleStatusPrimitive prim = PURPLE_STATUS_UNSET;
  pixbuf = pidgin_create_prpl_icon(account,PIDGIN_PRPL_ICON_MEDIUM);
  if ((pixbuf != ((GdkPixbuf *)((void *)0))) && !(purple_account_is_connected(account) != 0)) {
    gdk_pixbuf_saturate_and_pixelate(pixbuf,pixbuf,0.0,0);
  }
  if (substatus != ((PurpleSavedStatusSub *)((void *)0))) {
    const PurpleStatusType *type;
    type = purple_savedstatus_substatus_get_type(substatus);
    id = purple_status_type_get_id(type);
    name = purple_status_type_get_name(type);
    prim = purple_status_type_get_primitive(type);
    if (purple_status_type_get_attr(type,"message") != 0) 
      message = purple_savedstatus_substatus_get_message(substatus);
  }
  gtk_list_store_set(store,iter,STATUS_EDITOR_COLUMN_ACCOUNT,account,STATUS_EDITOR_COLUMN_ENABLE_SUBSTATUS,(substatus != ((PurpleSavedStatusSub *)((void *)0))),STATUS_EDITOR_COLUMN_ICON,pixbuf,STATUS_EDITOR_COLUMN_USERNAME,purple_account_get_username(account),STATUS_EDITOR_COLUMN_STATUS_ID,id,STATUS_EDITOR_COLUMN_STATUS_NAME,name,STATUS_EDITOR_COLUMN_STATUS_MESSAGE,message,STATUS_EDITOR_COLUMN_STATUS_ICON,get_stock_icon_from_primitive(prim),-1);
  if (pixbuf != ((GdkPixbuf *)((void *)0))) 
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
}

static void status_editor_add_account(StatusEditor *dialog,PurpleAccount *account,PurpleSavedStatusSub *substatus)
{
  GtkTreeIter iter;
  gtk_list_store_append((dialog -> model),&iter);
  status_editor_set_account((dialog -> model),account,&iter,substatus);
}

static void status_editor_populate_list(StatusEditor *dialog,PurpleSavedStatus *saved_status)
{
  GList *iter;
  PurpleSavedStatusSub *substatus;
  gtk_list_store_clear((dialog -> model));
  for (iter = purple_accounts_get_all(); iter != ((GList *)((void *)0)); iter = (iter -> next)) {
    PurpleAccount *account = (PurpleAccount *)(iter -> data);
    if (saved_status != ((PurpleSavedStatus *)((void *)0))) 
      substatus = purple_savedstatus_get_substatus(saved_status,account);
    else 
      substatus = ((PurpleSavedStatusSub *)((void *)0));
    status_editor_add_account(dialog,account,substatus);
  }
}

void pidgin_status_editor_show(gboolean edit,PurpleSavedStatus *saved_status)
{
  GtkTreeIter iter;
  StatusEditor *dialog;
  GtkSizeGroup *sg;
  GtkWidget *bbox;
  GtkWidget *button;
  GtkWidget *dbox;
  GtkWidget *expander;
  GtkWidget *dropdown;
  GtkWidget *entry;
  GtkWidget *frame;
  GtkWidget *hbox;
  GtkWidget *text;
  GtkWidget *toolbar;
  GtkWidget *vbox;
  GtkWidget *win;
  GList *focus_chain = (GList *)((void *)0);
  if (edit != 0) {
    do {
      if (saved_status != ((PurpleSavedStatus *)((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"saved_status != NULL");
        return ;
      };
    }while (0);
    do {
      if (!(purple_savedstatus_is_transient(saved_status) != 0)) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"!purple_savedstatus_is_transient(saved_status)");
        return ;
      };
    }while (0);
  }
/* Find a possible window for this saved status and present it */
  if ((edit != 0) && (status_window_find_savedstatus(&iter,purple_savedstatus_get_title(saved_status)) != 0)) {
    gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_window -> model)),gtk_tree_model_get_type()))),&iter,STATUS_WINDOW_COLUMN_WINDOW,&dialog,-1);
    if (dialog != ((StatusEditor *)((void *)0))) {
      gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> window)),gtk_window_get_type()))));
      return ;
    }
  }
  dialog = ((StatusEditor *)(g_malloc0_n(1,(sizeof(StatusEditor )))));
  if ((edit != 0) && (status_window_find_savedstatus(&iter,purple_savedstatus_get_title(saved_status)) != 0)) {
    gtk_list_store_set((status_window -> model),&iter,STATUS_WINDOW_COLUMN_WINDOW,dialog,-1);
  }
  if (edit != 0) 
    dialog -> original_title = g_strdup(purple_savedstatus_get_title(saved_status));
  dialog -> window = (win = pidgin_create_dialog(((const char *)(dgettext("pidgin","Status"))),12,"status",(!0)));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"destroy",((GCallback )status_editor_destroy_cb),dialog,0,((GConnectFlags )0));
/* Setup the vbox */
  vbox = pidgin_dialog_get_vbox_with_properties(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),0,12);
  sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
/* Title */
  entry = gtk_entry_new();
  dialog -> title = ((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type())));
  if (((saved_status != ((PurpleSavedStatus *)((void *)0))) && !(purple_savedstatus_is_transient(saved_status) != 0)) && (purple_savedstatus_get_title(saved_status) != ((const char *)((void *)0)))) 
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),purple_savedstatus_get_title(saved_status));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"changed",((GCallback )editor_title_changed_cb),dialog,0,((GConnectFlags )0));
  pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),((const char *)(dgettext("pidgin","_Title:"))),sg,entry,(!0),0);
/* Status type */
  if (saved_status != ((PurpleSavedStatus *)((void *)0))) 
    dropdown = create_status_type_menu(purple_savedstatus_get_type(saved_status));
  else 
    dropdown = create_status_type_menu(PURPLE_STATUS_AWAY);
  dialog -> type = ((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)dropdown),gtk_combo_box_get_type())));
  pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),((const char *)(dgettext("pidgin","_Status:"))),sg,dropdown,(!0),0);
/* Status message */
  frame = pidgin_create_imhtml((!0),&text,&toolbar,0);
  dialog -> message = ((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)text),gtk_imhtml_get_type())));
  hbox = pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),((const char *)(dgettext("pidgin","_Message:"))),sg,frame,(!0),0);
  gtk_container_child_set(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),hbox,"expand",!0,"fill",!0,((void *)((void *)0)));
  focus_chain = g_list_prepend(focus_chain,(dialog -> message));
  gtk_container_set_focus_chain(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_container_get_type()))),focus_chain);
  g_list_free(focus_chain);
  gtk_imhtml_set_return_inserts_newline((dialog -> message));
  if ((saved_status != ((PurpleSavedStatus *)((void *)0))) && (purple_savedstatus_get_message(saved_status) != ((const char *)((void *)0)))) 
    gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)text),gtk_imhtml_get_type()))),purple_savedstatus_get_message(saved_status),0,0);
/* Different status message expander */
  expander = gtk_expander_new_with_mnemonic(((const char *)(dgettext("pidgin","Use a _different status for some accounts"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),expander,0,0,0);
/* Setup the box that the expander will cover */
  dbox = gtk_vbox_new(0,18);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)expander),gtk_container_get_type()))),dbox);
/* Create the list model */
  dialog -> model = gtk_list_store_new(STATUS_EDITOR_NUM_COLUMNS,((GType )(17 << 2)),((GType )(17 << 2)),((GType )(5 << 2)),gdk_pixbuf_get_type(),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(16 << 2)));
/* Create the treeview */
  dialog -> treeview = gtk_tree_view_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))));
  gtk_tree_view_set_rules_hint(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treeview)),gtk_tree_view_get_type()))),(!0));
  gtk_widget_set_size_request((dialog -> treeview),(-1),150);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)dbox),gtk_box_get_type()))),pidgin_make_scrollable((dialog -> treeview),GTK_POLICY_AUTOMATIC,GTK_POLICY_ALWAYS,GTK_SHADOW_IN,-1,-1),(!0),(!0),0);
/* Add columns */
  status_editor_add_columns(dialog);
/* Populate list */
  status_editor_populate_list(dialog,saved_status);
/* Expand the treeview if we have substatuses */
  gtk_expander_set_expanded(((GtkExpander *)(g_type_check_instance_cast(((GTypeInstance *)expander),gtk_expander_get_type()))),((saved_status != ((PurpleSavedStatus *)((void *)0))) && (purple_savedstatus_has_substatuses(saved_status) != 0)));
/* Button box */
  bbox = pidgin_dialog_get_action_area(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))));
  gtk_box_set_spacing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),6);
  gtk_button_box_set_layout(((GtkButtonBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_button_box_get_type()))),GTK_BUTTONBOX_END);
/* Cancel button */
  pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),"gtk-cancel",((GCallback )status_editor_cancel_cb),dialog);
/* Use button */
  button = pidgin_pixbuf_button_from_stock(((const char *)(dgettext("pidgin","_Use"))),"gtk-execute",PIDGIN_BUTTON_HORIZONTAL);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),button,0,0,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )status_editor_ok_cb),dialog,0,((GConnectFlags )0));
/* Save & Use button */
  button = pidgin_pixbuf_button_from_stock(((const char *)(dgettext("pidgin","Sa_ve & Use"))),"gtk-ok",PIDGIN_BUTTON_HORIZONTAL);
  dialog -> saveanduse_button = ((GtkButton *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_button_get_type())));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),button,0,0,0);
  if ((dialog -> original_title) == ((gchar *)((void *)0))) 
    gtk_widget_set_sensitive(button,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )status_editor_ok_cb),dialog,0,((GConnectFlags )0));
/* Save button */
  button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),"gtk-save",((GCallback )status_editor_ok_cb),dialog);
  if ((dialog -> original_title) == ((gchar *)((void *)0))) 
    gtk_widget_set_sensitive(button,0);
  dialog -> save_button = ((GtkButton *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_button_get_type())));
  gtk_widget_show_all(win);
  g_object_unref(sg);
}
/**************************************************************************
* SubStatus editor
**************************************************************************/

static void substatus_selection_changed_cb(GtkComboBox *box,gpointer user_data)
{
  SubStatusEditor *select = user_data;
  GtkTreeIter iter;
  char *id;
  PurpleStatusType *type;
  if (!(gtk_combo_box_get_active_iter(box,&iter) != 0)) 
    return ;
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(select -> model)),gtk_tree_model_get_type()))),&iter,STATUS_COLUMN_STATUS_ID,&id,-1);
  type = purple_account_get_status_type((select -> account),id);
  g_free(id);
  if (purple_status_type_get_attr(type,"message") == ((PurpleStatusAttr *)((void *)0))) {
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(select -> message)),gtk_widget_get_type()))),0);
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(select -> toolbar)),gtk_widget_get_type()))),0);
  }
  else {
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(select -> message)),gtk_widget_get_type()))),(!0));
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(select -> toolbar)),gtk_widget_get_type()))),(!0));
  }
}

static gboolean status_editor_find_account_in_treemodel(GtkTreeIter *iter,StatusEditor *status_editor,PurpleAccount *account)
{
  GtkTreeModel *model;
  PurpleAccount *cur;
  do {
    if (status_editor != ((StatusEditor *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"status_editor != NULL");
      return 0;
    };
  }while (0);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  model = ((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_editor -> model)),gtk_tree_model_get_type())));
  if (!(gtk_tree_model_get_iter_first(model,iter) != 0)) 
    return 0;
  do {
    gtk_tree_model_get(model,iter,STATUS_EDITOR_COLUMN_ACCOUNT,&cur,-1);
    if (cur == account) 
      return (!0);
  }while (gtk_tree_model_iter_next(model,iter) != 0);
  return 0;
}

static void substatus_editor_remove_dialog(SubStatusEditor *dialog)
{
  GtkTreeIter iter;
  if (status_editor_find_account_in_treemodel(&iter,(dialog -> status_editor),(dialog -> account)) != 0) {
/* Remove the reference to this dialog from our parent's list store */
    gtk_list_store_set(( *(dialog -> status_editor)).model,&iter,STATUS_EDITOR_COLUMN_WINDOW,((void *)((void *)0)),-1);
  }
}

static void substatus_editor_destroy_cb(GtkWidget *widget,gpointer user_data)
{
  SubStatusEditor *dialog = user_data;
  substatus_editor_remove_dialog(dialog);
  g_free(dialog);
}

static void substatus_editor_cancel_cb(GtkButton *button,gpointer user_data)
{
  SubStatusEditor *dialog = user_data;
  gtk_widget_destroy((dialog -> window));
}

static void substatus_editor_ok_cb(GtkButton *button,gpointer user_data)
{
  SubStatusEditor *dialog = user_data;
  StatusEditor *status_editor;
  GtkTreeIter iter;
  PurpleStatusType *type;
  char *id = (char *)((void *)0);
  char *message = (char *)((void *)0);
  const char *name = (const char *)((void *)0);
  const char *stock = (const char *)((void *)0);
  if (!(gtk_combo_box_get_active_iter((dialog -> box),&iter) != 0)) {
    gtk_widget_destroy((dialog -> window));
    return ;
  }
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter,STATUS_COLUMN_STATUS_ID,&id,-1);
  type = purple_account_get_status_type((dialog -> account),id);
  if (purple_status_type_get_attr(type,"message") != ((PurpleStatusAttr *)((void *)0))) 
    message = gtk_imhtml_get_markup(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> message)),gtk_imhtml_get_type()))));
  name = purple_status_type_get_name(type);
  stock = get_stock_icon_from_primitive(purple_status_type_get_primitive(type));
  status_editor = (dialog -> status_editor);
  if (status_editor_find_account_in_treemodel(&iter,status_editor,(dialog -> account)) != 0) {
    gtk_list_store_set((status_editor -> model),&iter,STATUS_EDITOR_COLUMN_ENABLE_SUBSTATUS,!0,STATUS_EDITOR_COLUMN_STATUS_ID,id,STATUS_EDITOR_COLUMN_STATUS_NAME,name,STATUS_EDITOR_COLUMN_STATUS_MESSAGE,message,STATUS_EDITOR_COLUMN_WINDOW,((void *)((void *)0)),STATUS_EDITOR_COLUMN_STATUS_ICON,stock,-1);
  }
  gtk_widget_destroy((dialog -> window));
  g_free(id);
  g_free(message);
}

static void edit_substatus(StatusEditor *status_editor,PurpleAccount *account)
{
  char *tmp;
  SubStatusEditor *dialog;
  GtkSizeGroup *sg;
  GtkWidget *combo;
  GtkWidget *hbox;
  GtkWidget *frame;
  GtkWidget *label;
  GtkWidget *text;
  GtkWidget *toolbar;
  GtkWidget *vbox;
  GtkWidget *win;
  GtkTreeIter iter;
  GtkCellRenderer *rend;
  char *status_id = (char *)((void *)0);
  char *message = (char *)((void *)0);
  gboolean parent_dialog_has_substatus = 0;
  GList *list;
  gboolean select = 0;
  do {
    if (status_editor != ((StatusEditor *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"status_editor != NULL");
      return ;
    };
  }while (0);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  status_editor_find_account_in_treemodel(&iter,status_editor,account);
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_editor -> model)),gtk_tree_model_get_type()))),&iter,STATUS_EDITOR_COLUMN_WINDOW,&dialog,-1);
  if (dialog != ((SubStatusEditor *)((void *)0))) {
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> window)),gtk_window_get_type()))));
    return ;
  }
  dialog = ((SubStatusEditor *)(g_malloc0_n(1,(sizeof(SubStatusEditor )))));
  gtk_list_store_set((status_editor -> model),&iter,STATUS_EDITOR_COLUMN_WINDOW,dialog,-1);
  dialog -> status_editor = status_editor;
  dialog -> account = account;
  tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Status for %s"))),purple_account_get_username(account));
  dialog -> window = (win = pidgin_create_dialog(tmp,12,"substatus",(!0)));
  g_free(tmp);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"destroy",((GCallback )substatus_editor_destroy_cb),dialog,0,((GConnectFlags )0));
/* Setup the vbox */
  vbox = pidgin_dialog_get_vbox_with_properties(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),0,12);
  sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
/* Status type */
  hbox = gtk_hbox_new(0,6);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  label = gtk_label_new_with_mnemonic(((const char *)(dgettext("pidgin","_Status:"))));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  gtk_size_group_add_widget(sg,label);
  dialog -> model = gtk_list_store_new(STATUS_NUM_COLUMNS,((GType )(16 << 2)),((GType )(16 << 2)),((GType )(16 << 2)));
  combo = gtk_combo_box_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))));
  dialog -> box = ((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gtk_combo_box_get_type())));
  rend = ((GtkCellRenderer *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_cell_renderer_pixbuf_new())),gtk_cell_renderer_get_type())));
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)rend),((GType )(20 << 2))))),"stock-size",gtk_icon_size_from_name("pidgin-icon-size-tango-extra-small"),((void *)((void *)0)));
  gtk_cell_layout_pack_start(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)combo),gtk_cell_layout_get_type()))),rend,0);
  gtk_cell_layout_set_attributes(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)combo),gtk_cell_layout_get_type()))),rend,"stock-id",STATUS_COLUMN_ICON,((void *)((void *)0)));
  rend = ((GtkCellRenderer *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_cell_renderer_text_new())),gtk_cell_renderer_get_type())));
  gtk_cell_layout_pack_start(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)combo),gtk_cell_layout_get_type()))),rend,(!0));
  gtk_cell_layout_set_attributes(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)combo),gtk_cell_layout_get_type()))),rend,"text",STATUS_COLUMN_STATUS_NAME,((void *)((void *)0)));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)combo),((GType )(20 << 2))))),"changed",((GCallback )substatus_selection_changed_cb),dialog,0,((GConnectFlags )0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),combo,0,0,0);
/* Status mesage */
  hbox = gtk_hbox_new(0,6);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,(!0),(!0),0);
  label = gtk_label_new_with_mnemonic(((const char *)(dgettext("pidgin","_Message:"))));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  gtk_size_group_add_widget(sg,label);
  frame = pidgin_create_imhtml((!0),&text,&toolbar,0);
  dialog -> message = ((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)text),gtk_imhtml_get_type())));
  dialog -> toolbar = ((GtkIMHtmlToolbar *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_imhtmltoolbar_get_type())));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),frame,(!0),(!0),0);
/* Cancel button */
  pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),"gtk-cancel",((GCallback )substatus_editor_cancel_cb),dialog);
/* OK button */
  pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),"gtk-ok",((GCallback )substatus_editor_ok_cb),dialog);
/* Seed the input widgets with the current values */
/* Only look at the saved status if we can't find it in the parent status dialog's substatuses model */
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_editor -> model)),gtk_tree_model_get_type()))),&iter,STATUS_EDITOR_COLUMN_ENABLE_SUBSTATUS,&parent_dialog_has_substatus,-1);
  if (parent_dialog_has_substatus != 0) {
    gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_editor -> model)),gtk_tree_model_get_type()))),&iter,STATUS_EDITOR_COLUMN_STATUS_ID,&status_id,STATUS_EDITOR_COLUMN_STATUS_MESSAGE,&message,-1);
  }
  else if ((status_editor -> original_title) != ((gchar *)((void *)0))) {
    PurpleSavedStatus *saved_status = (PurpleSavedStatus *)((void *)0);
    PurpleSavedStatusSub *substatus = (PurpleSavedStatusSub *)((void *)0);
    if ((saved_status = purple_savedstatus_find((status_editor -> original_title))) != ((PurpleSavedStatus *)((void *)0))) {
      if ((substatus = purple_savedstatus_get_substatus(saved_status,account)) != ((PurpleSavedStatusSub *)((void *)0))) {
        message = ((char *)(purple_savedstatus_substatus_get_message(substatus)));
        status_id = ((char *)(purple_status_type_get_id(purple_savedstatus_substatus_get_type(substatus))));
      }
    }
  }
/* TODO: Else get the generic status type from our parent */
  if (message != 0) 
    gtk_imhtml_append_text_with_images((dialog -> message),message,0,0);
  for (list = purple_account_get_status_types(account); list != 0; list = (list -> next)) {{
      PurpleStatusType *status_type;
      const char *id;
      const char *name;
      PurpleStatusPrimitive prim;
      status_type = (list -> data);
/*
		 * Only allow users to select statuses that are flagged as
		 * "user settable" and that aren't independent.
		 */
      if (!(purple_status_type_is_user_settable(status_type) != 0) || (purple_status_type_is_independent(status_type) != 0)) 
        continue; 
      id = purple_status_type_get_id(status_type);
      prim = purple_status_type_get_primitive(status_type);
      name = purple_status_type_get_name(status_type);
      gtk_list_store_append((dialog -> model),&iter);
      gtk_list_store_set((dialog -> model),&iter,STATUS_COLUMN_ICON,pidgin_stock_id_from_status_primitive(prim),STATUS_COLUMN_STATUS_ID,id,STATUS_COLUMN_STATUS_NAME,name,-1);
      if ((status_id != ((char *)((void *)0))) && !(strcmp(status_id,id) != 0)) {
        gtk_combo_box_set_active_iter(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gtk_combo_box_get_type()))),&iter);
        select = (!0);
      }
    }
  }
  if (!(select != 0)) 
    gtk_combo_box_set_active(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gtk_combo_box_get_type()))),0);
  if (parent_dialog_has_substatus != 0) {
/* These two were gotten from the parent tree model, so they need to be freed */
    g_free(status_id);
    g_free(message);
  }
  gtk_widget_show_all(win);
  g_object_unref(sg);
}
/**************************************************************************
 * Utilities                                                              *
 **************************************************************************/
enum __unnamed_enum___F0_L1559_C1_SS_MENU_ENTRY_TYPE_PRIMITIVE__COMMA__SS_MENU_ENTRY_TYPE_SAVEDSTATUS {SS_MENU_ENTRY_TYPE_PRIMITIVE,SS_MENU_ENTRY_TYPE_SAVEDSTATUS};
enum __unnamed_enum___F0_L1564_C1_SS_MENU_TYPE_COLUMN__COMMA__SS_MENU_ICON_COLUMN__COMMA__SS_MENU_TEXT_COLUMN__COMMA__SS_MENU_DATA_COLUMN__COMMA__SS_MENU_EMBLEM_COLUMN__COMMA__SS_MENU_EMBLEM_VISIBLE_COLUMN__COMMA__SS_MENU_NUM_COLUMNS {
/** _SSMenuEntryType */
SS_MENU_TYPE_COLUMN,
/**
	 * This is a GdkPixbuf (the other columns are strings).
	 * This column is visible.
	 */
SS_MENU_ICON_COLUMN,
/** The text displayed on the status box.  This column is visible. */
SS_MENU_TEXT_COLUMN,
/**
	 * This value depends on SS_MENU_TYPE_COLUMN.  For _SAVEDSTATUS types,
	 * this is the creation time.  For _PRIMITIVE types,
	 * this is the PurpleStatusPrimitive.
	 */
SS_MENU_DATA_COLUMN,
/**
	 * This is the emblem to use for this status
	 */
SS_MENU_EMBLEM_COLUMN,
/**
	 * And whether or not that emblem is visible
	 */
SS_MENU_EMBLEM_VISIBLE_COLUMN,SS_MENU_NUM_COLUMNS};

static void status_menu_cb(GtkComboBox *widget,void (*callback)(PurpleSavedStatus *))
{
  GtkTreeIter iter;
  int type;
  gpointer data;
  PurpleSavedStatus *status = (PurpleSavedStatus *)((void *)0);
  if (!(gtk_combo_box_get_active_iter(widget,&iter) != 0)) 
    return ;
  gtk_tree_model_get(gtk_combo_box_get_model(widget),&iter,SS_MENU_TYPE_COLUMN,&type,SS_MENU_DATA_COLUMN,&data,-1);
  if (type == SS_MENU_ENTRY_TYPE_PRIMITIVE) {
    PurpleStatusPrimitive primitive = ((gint )((glong )data));
    status = purple_savedstatus_find_transient_by_type_and_message(primitive,0);
    if (status == ((PurpleSavedStatus *)((void *)0))) 
      status = purple_savedstatus_new(0,primitive);
  }
  else if (type == SS_MENU_ENTRY_TYPE_SAVEDSTATUS) 
    status = purple_savedstatus_find_by_creation_time(((gint )((glong )data)));
  ( *callback)(status);
}

static gint saved_status_sort_alphabetically_func(gconstpointer a,gconstpointer b)
{
  const PurpleSavedStatus *saved_status_a = a;
  const PurpleSavedStatus *saved_status_b = b;
  return g_utf8_collate(purple_savedstatus_get_title(saved_status_a),purple_savedstatus_get_title(saved_status_b));
}

static gboolean pidgin_status_menu_add_primitive(GtkListStore *model,GtkWidget *w,PurpleStatusPrimitive primitive,PurpleSavedStatus *current_status)
{
  GtkTreeIter iter;
  gboolean currently_selected = 0;
  gtk_list_store_append(model,&iter);
  gtk_list_store_set(model,&iter,SS_MENU_TYPE_COLUMN,SS_MENU_ENTRY_TYPE_PRIMITIVE,SS_MENU_ICON_COLUMN,pidgin_stock_id_from_status_primitive(primitive),SS_MENU_TEXT_COLUMN,purple_primitive_get_name_from_type(primitive),SS_MENU_DATA_COLUMN,((gpointer )((glong )primitive)),SS_MENU_EMBLEM_VISIBLE_COLUMN,0,-1);
  if (((purple_savedstatus_is_transient(current_status) != 0) && !(purple_savedstatus_has_substatuses(current_status) != 0)) && ((purple_savedstatus_get_type(current_status)) == primitive)) 
    currently_selected = (!0);
  return currently_selected;
}

static void pidgin_status_menu_update_iter(GtkWidget *combobox,GtkListStore *store,GtkTreeIter *iter,PurpleSavedStatus *status)
{
  PurpleStatusPrimitive primitive;
  if (store == ((GtkListStore *)((void *)0))) 
    store = ((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_combo_box_get_model(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combobox),gtk_combo_box_get_type())))))),gtk_list_store_get_type())));
  primitive = purple_savedstatus_get_type(status);
  gtk_list_store_set(store,iter,SS_MENU_TYPE_COLUMN,SS_MENU_ENTRY_TYPE_SAVEDSTATUS,SS_MENU_ICON_COLUMN,pidgin_stock_id_from_status_primitive(primitive),SS_MENU_TEXT_COLUMN,purple_savedstatus_get_title(status),SS_MENU_DATA_COLUMN,((gpointer )(purple_savedstatus_get_creation_time(status))),SS_MENU_EMBLEM_COLUMN,"gtk-save",SS_MENU_EMBLEM_VISIBLE_COLUMN,!0,-1);
}

static gboolean pidgin_status_menu_find_iter(GtkListStore *store,GtkTreeIter *iter,PurpleSavedStatus *find)
{
  int type;
  gpointer data;
  time_t creation_time = purple_savedstatus_get_creation_time(find);
  GtkTreeModel *model = (GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)store),gtk_tree_model_get_type()));
  if (!(gtk_tree_model_get_iter_first(model,iter) != 0)) 
    return 0;
  do {
    gtk_tree_model_get(model,iter,SS_MENU_TYPE_COLUMN,&type,SS_MENU_DATA_COLUMN,&data,-1);
    if (type == SS_MENU_ENTRY_TYPE_PRIMITIVE) 
      continue; 
    if (((gint )((glong )data)) == creation_time) 
      return (!0);
  }while (gtk_tree_model_iter_next(model,iter) != 0);
  return 0;
}

static void savedstatus_added_cb(PurpleSavedStatus *status,GtkWidget *combobox)
{
  GtkListStore *store;
  GtkTreeIter iter;
  if (purple_savedstatus_is_transient(status) != 0) 
    return ;
  store = ((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_combo_box_get_model(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combobox),gtk_combo_box_get_type())))))),gtk_list_store_get_type())));
  gtk_list_store_append(store,&iter);
  pidgin_status_menu_update_iter(combobox,store,&iter,status);
}

static void savedstatus_deleted_cb(PurpleSavedStatus *status,GtkWidget *combobox)
{
  GtkListStore *store;
  GtkTreeIter iter;
  if (purple_savedstatus_is_transient(status) != 0) 
    return ;
  store = ((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_combo_box_get_model(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combobox),gtk_combo_box_get_type())))))),gtk_list_store_get_type())));
  if (pidgin_status_menu_find_iter(store,&iter,status) != 0) 
    gtk_list_store_remove(store,&iter);
}

static void savedstatus_modified_cb(PurpleSavedStatus *status,GtkWidget *combobox)
{
  GtkListStore *store;
  GtkTreeIter iter;
  if (purple_savedstatus_is_transient(status) != 0) 
    return ;
  store = ((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_combo_box_get_model(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combobox),gtk_combo_box_get_type())))))),gtk_list_store_get_type())));
  if (pidgin_status_menu_find_iter(store,&iter,status) != 0) 
    pidgin_status_menu_update_iter(combobox,store,&iter,status);
}

GtkWidget *pidgin_status_menu(PurpleSavedStatus *current_status,GCallback callback)
{
  GtkWidget *combobox;
  GtkListStore *model;
  GList *sorted;
  GList *cur;
  int i = 0;
  int index = -1;
  GtkTreeIter iter;
  GtkCellRenderer *text_rend;
  GtkCellRenderer *icon_rend;
  GtkCellRenderer *emblem_rend;
  model = gtk_list_store_new(SS_MENU_NUM_COLUMNS,((GType )(6 << 2)),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(17 << 2)),((GType )(16 << 2)),((GType )(5 << 2)));
  combobox = gtk_combo_box_new();
  if (pidgin_status_menu_add_primitive(model,combobox,PURPLE_STATUS_AVAILABLE,current_status) != 0) 
    index = i;
  i++;
  if (pidgin_status_menu_add_primitive(model,combobox,PURPLE_STATUS_AWAY,current_status) != 0) 
    index = i;
  i++;
  if (pidgin_status_menu_add_primitive(model,combobox,PURPLE_STATUS_INVISIBLE,current_status) != 0) 
    index = i;
  i++;
  if (pidgin_status_menu_add_primitive(model,combobox,PURPLE_STATUS_OFFLINE,current_status) != 0) 
    index = i;
  i++;
  sorted = g_list_copy(purple_savedstatuses_get_all());
  sorted = g_list_sort(sorted,saved_status_sort_alphabetically_func);
  for (cur = sorted; cur != 0; cur = (cur -> next)) {
    PurpleSavedStatus *status = (PurpleSavedStatus *)(cur -> data);
    if (!(purple_savedstatus_is_transient(status) != 0)) {
      gtk_list_store_append(model,&iter);
      pidgin_status_menu_update_iter(combobox,model,&iter,status);
      if (status == current_status) 
        index = i;
      i++;
    }
  }
  g_list_free(sorted);
  gtk_combo_box_set_model(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combobox),gtk_combo_box_get_type()))),((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))));
  text_rend = gtk_cell_renderer_text_new();
  icon_rend = gtk_cell_renderer_pixbuf_new();
  emblem_rend = gtk_cell_renderer_pixbuf_new();
  gtk_cell_layout_pack_start(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)combobox),gtk_cell_layout_get_type()))),icon_rend,0);
  gtk_cell_layout_pack_start(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)combobox),gtk_cell_layout_get_type()))),text_rend,(!0));
  gtk_cell_layout_pack_start(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)combobox),gtk_cell_layout_get_type()))),emblem_rend,0);
  gtk_cell_layout_set_attributes(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)combobox),gtk_cell_layout_get_type()))),icon_rend,"stock-id",SS_MENU_ICON_COLUMN,((void *)((void *)0)));
  gtk_cell_layout_set_attributes(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)combobox),gtk_cell_layout_get_type()))),text_rend,"markup",SS_MENU_TEXT_COLUMN,((void *)((void *)0)));
  gtk_cell_layout_set_attributes(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)combobox),gtk_cell_layout_get_type()))),emblem_rend,"stock-id",SS_MENU_EMBLEM_COLUMN,"visible",SS_MENU_EMBLEM_VISIBLE_COLUMN,((void *)((void *)0)));
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)icon_rend),((GType )(20 << 2))))),"stock-size",gtk_icon_size_from_name("pidgin-icon-size-tango-extra-small"),((void *)((void *)0)));
  g_object_set(text_rend,"ellipsize",PANGO_ELLIPSIZE_END,((void *)((void *)0)));
  gtk_combo_box_set_active(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combobox),gtk_combo_box_get_type()))),index);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)combobox),((GType )(20 << 2))))),"changed",((GCallback )status_menu_cb),callback,0,((GConnectFlags )0));
/* Make sure the list is updated dynamically when a substatus is changed/deleted
	 * or a new one is added. */
  purple_signal_connect(purple_savedstatuses_get_handle(),"savedstatus-added",combobox,((GCallback )savedstatus_added_cb),combobox);
  purple_signal_connect(purple_savedstatuses_get_handle(),"savedstatus-deleted",combobox,((GCallback )savedstatus_deleted_cb),combobox);
  purple_signal_connect(purple_savedstatuses_get_handle(),"savedstatus-modified",combobox,((GCallback )savedstatus_modified_cb),combobox);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)combobox),((GType )(20 << 2))))),"destroy",((GCallback )purple_signals_disconnect_by_handle),0,0,((GConnectFlags )0));
  return combobox;
}
/**************************************************************************
* GTK+ saved status glue
**************************************************************************/

void *pidgin_status_get_handle()
{
  static int handle;
  return (&handle);
}

void pidgin_status_init()
{
  purple_prefs_add_none("/pidgin/status");
  purple_prefs_add_none("/pidgin/status/dialog");
  purple_prefs_add_int("/pidgin/status/dialog/width",550);
  purple_prefs_add_int("/pidgin/status/dialog/height",250);
}

void pidgin_status_uninit()
{
  pidgin_status_window_hide();
}
