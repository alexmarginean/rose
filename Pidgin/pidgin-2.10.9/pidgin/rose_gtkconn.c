/*
 * @file gtkconn.c GTK+ Connection API
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "pidgin.h"
#include "account.h"
#include "debug.h"
#include "notify.h"
#include "prefs.h"
#include "gtkblist.h"
#include "gtkconn.h"
#include "gtkdialogs.h"
#include "gtkstatusbox.h"
#include "pidginstock.h"
#include "gtkutils.h"
#include "util.h"
#define INITIAL_RECON_DELAY_MIN  8000
#define INITIAL_RECON_DELAY_MAX 60000
#define MAX_RECON_DELAY 600000
#define MAX_RACCOON_DELAY "shorter in urban areas"
typedef struct __unnamed_class___F0_L47_C9_unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__delay__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_guintUi__typedef_declaration_variable_name_unknown_scope_and_name__scope__timeout {
int delay;
guint timeout;}PidginAutoRecon;
/**
 * Contains accounts that are auto-reconnecting.
 * The key is a pointer to the PurpleAccount and the
 * value is a pointer to a PidginAutoRecon.
 */
static GHashTable *auto_reconns = (GHashTable *)((void *)0);

static void pidgin_connection_connect_progress(PurpleConnection *gc,const char *text,size_t step,size_t step_count)
{
  PidginBuddyList *gtkblist = pidgin_blist_get_default_gtk_blist();
  if (!(gtkblist != 0)) 
    return ;
  pidgin_status_box_set_connecting(((PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> statusbox)),pidgin_status_box_get_type()))),(purple_connections_get_connecting() != ((GList *)((void *)0))));
  pidgin_status_box_pulse_connecting(((PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> statusbox)),pidgin_status_box_get_type()))));
}

static void pidgin_connection_connected(PurpleConnection *gc)
{
  PurpleAccount *account;
  PidginBuddyList *gtkblist;
  account = purple_connection_get_account(gc);
  gtkblist = pidgin_blist_get_default_gtk_blist();
  if (gtkblist != ((PidginBuddyList *)((void *)0))) 
    pidgin_status_box_set_connecting(((PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> statusbox)),pidgin_status_box_get_type()))),(purple_connections_get_connecting() != ((GList *)((void *)0))));
  g_hash_table_remove(auto_reconns,account);
}

static void pidgin_connection_disconnected(PurpleConnection *gc)
{
  PidginBuddyList *gtkblist = pidgin_blist_get_default_gtk_blist();
  if (!(gtkblist != 0)) 
    return ;
  pidgin_status_box_set_connecting(((PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> statusbox)),pidgin_status_box_get_type()))),(purple_connections_get_connecting() != ((GList *)((void *)0))));
  if (purple_connections_get_all() != ((GList *)((void *)0))) 
    return ;
  pidgin_dialogs_destroy_all();
}

static void free_auto_recon(gpointer data)
{
  PidginAutoRecon *info = data;
  if ((info -> timeout) != 0) 
    g_source_remove((info -> timeout));
  g_free(info);
}

static gboolean do_signon(gpointer data)
{
  PurpleAccount *account = data;
  PidginAutoRecon *info;
  PurpleStatus *status;
  purple_debug_info("autorecon","do_signon called\n");
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  info = (g_hash_table_lookup(auto_reconns,account));
  if (info != 0) 
    info -> timeout = 0;
  status = purple_account_get_active_status(account);
  if (purple_status_is_online(status) != 0) {
    purple_debug_info("autorecon","calling purple_account_connect\n");
    purple_account_connect(account);
    purple_debug_info("autorecon","done calling purple_account_connect\n");
  }
  return 0;
}

static void pidgin_connection_report_disconnect_reason(PurpleConnection *gc,PurpleConnectionError reason,const char *text)
{
  PurpleAccount *account = (PurpleAccount *)((void *)0);
  PidginAutoRecon *info;
  account = purple_connection_get_account(gc);
  info = (g_hash_table_lookup(auto_reconns,account));
  if (!(purple_connection_error_is_fatal(reason) != 0)) {
    if (info == ((PidginAutoRecon *)((void *)0))) {
      info = ((PidginAutoRecon *)(g_malloc0_n(1,(sizeof(PidginAutoRecon )))));
      g_hash_table_insert(auto_reconns,account,info);
      info -> delay = g_random_int_range(8000,60000);
    }
    else {
      info -> delay = (((2 * (info -> delay)) < 600000)?(2 * (info -> delay)) : 600000);
      if ((info -> timeout) != 0) 
        g_source_remove((info -> timeout));
    }
    info -> timeout = g_timeout_add((info -> delay),do_signon,account);
  }
  else {
    if (info != ((PidginAutoRecon *)((void *)0))) 
      g_hash_table_remove(auto_reconns,account);
    purple_account_set_enabled(account,"gtk-gaim",0);
  }
}

static void pidgin_connection_network_connected()
{
  GList *list;
  GList *l;
  PidginBuddyList *gtkblist = pidgin_blist_get_default_gtk_blist();
  if (gtkblist != 0) 
    pidgin_status_box_set_network_available(((PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> statusbox)),pidgin_status_box_get_type()))),(!0));
  l = (list = purple_accounts_get_all_active());
  while(l != 0){
    PurpleAccount *account = (PurpleAccount *)(l -> data);
    g_hash_table_remove(auto_reconns,account);
    if (purple_account_is_disconnected(account) != 0) 
      do_signon(account);
    l = (l -> next);
  }
  g_list_free(list);
}

static void pidgin_connection_network_disconnected()
{
  GList *list;
  GList *l;
  PidginBuddyList *gtkblist = pidgin_blist_get_default_gtk_blist();
  if (gtkblist != 0) 
    pidgin_status_box_set_network_available(((PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> statusbox)),pidgin_status_box_get_type()))),0);
  l = (list = purple_accounts_get_all_active());
  while(l != 0){
    PurpleAccount *a = (PurpleAccount *)(l -> data);
    if (!(purple_account_is_disconnected(a) != 0)) {
      char *password = g_strdup(purple_account_get_password(a));
      purple_account_disconnect(a);
      purple_account_set_password(a,password);
      g_free(password);
    }
    l = (l -> next);
  }
  g_list_free(list);
}

static void pidgin_connection_notice(PurpleConnection *gc,const char *text)
{
}
static PurpleConnectionUiOps conn_ui_ops = {(pidgin_connection_connect_progress), (pidgin_connection_connected), (pidgin_connection_disconnected), (pidgin_connection_notice), ((void (*)(PurpleConnection *, const char *))((void *)0)), (pidgin_connection_network_connected), (pidgin_connection_network_disconnected), (pidgin_connection_report_disconnect_reason), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* report_disconnect */
};

PurpleConnectionUiOps *pidgin_connections_get_ui_ops()
{
  return &conn_ui_ops;
}

static void account_removed_cb(PurpleAccount *account,gpointer user_data)
{
  g_hash_table_remove(auto_reconns,account);
}
/**************************************************************************
* GTK+ connection glue
**************************************************************************/

void *pidgin_connection_get_handle()
{
  static int handle;
  return (&handle);
}

void pidgin_connection_init()
{
  auto_reconns = g_hash_table_new_full(g_direct_hash,g_direct_equal,0,free_auto_recon);
  purple_signal_connect(purple_accounts_get_handle(),"account-removed",pidgin_connection_get_handle(),((PurpleCallback )account_removed_cb),0);
}

void pidgin_connection_uninit()
{
  purple_signals_disconnect_by_handle(pidgin_connection_get_handle());
  g_hash_table_destroy(auto_reconns);
}
