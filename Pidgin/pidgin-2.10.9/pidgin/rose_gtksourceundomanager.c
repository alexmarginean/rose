/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * gtksourceundomanager.c
 * This file is part of GtkSourceView
 *
 * Copyright (C) 1998, 1999 Alex Roberts, Evan Lawrence
 * Copyright (C) 2000, 2001 Chema Celorio, Paolo Maggi
 * Copyright (C) 2002-2005  Paolo Maggi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02111-1301, USA.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <glib.h>
#include <stdlib.h>
#include <string.h>
#include "gtksourceundomanager.h"
#include "gtksourceview-marshal.h"
#define DEFAULT_MAX_UNDO_LEVELS		25
typedef struct _GtkSourceUndoAction GtkSourceUndoAction;
typedef struct _GtkSourceUndoInsertAction GtkSourceUndoInsertAction;
typedef struct _GtkSourceUndoDeleteAction GtkSourceUndoDeleteAction;
typedef struct _GtkSourceUndoInsertAnchorAction GtkSourceUndoInsertAnchorAction;
typedef enum __unnamed_enum___F0_L46_C9_GTK_SOURCE_UNDO_ACTION_INSERT__COMMA__GTK_SOURCE_UNDO_ACTION_DELETE__COMMA__GTK_SOURCE_UNDO_ACTION_INSERT_ANCHOR {GTK_SOURCE_UNDO_ACTION_INSERT,GTK_SOURCE_UNDO_ACTION_DELETE,GTK_SOURCE_UNDO_ACTION_INSERT_ANCHOR}GtkSourceUndoActionType;
/*
 * We use offsets instead of GtkTextIters because the last ones
 * require to much memory in this context without giving us any advantage.
 */

struct _GtkSourceUndoInsertAction 
{
  gint pos;
  gchar *text;
  gint length;
  gint chars;
}
;

struct _GtkSourceUndoDeleteAction 
{
  gint start;
  gint end;
  gchar *text;
  gboolean forward;
}
;

struct _GtkSourceUndoInsertAnchorAction 
{
  gint pos;
  GtkTextChildAnchor *anchor;
}
;

struct _GtkSourceUndoAction 
{
  GtkSourceUndoActionType action_type;
  union __unnamed_class___F0_L83_C2__GtkSourceUndoAction_variable_declaration__variable_type_L1400R_variable_name__GtkSourceUndoAction__scope__insert__DELIMITER___GtkSourceUndoAction_variable_declaration__variable_type_L1401R_variable_name__GtkSourceUndoAction__scope__delete__DELIMITER___GtkSourceUndoAction_variable_declaration__variable_type_L1402R_variable_name__GtkSourceUndoAction__scope__insert_anchor {
  GtkSourceUndoInsertAction insert;
  GtkSourceUndoDeleteAction delete;
  GtkSourceUndoInsertAnchorAction insert_anchor;}action;
  gint order_in_group;
/* It is TRUE whether the action can be merged with the following action. */
  guint mergeable:1UL;
/* It is TRUE whether the action is marked as "modified".
	 * An action is marked as "modified" if it changed the
	 * state of the buffer from "not modified" to "modified". Only the first
	 * action of a group can be marked as modified.
	 * There can be a single action marked as "modified" in the actions list.
	 */
  guint modified:1UL;
}
;

struct _GtkSourceUndoManagerPrivate 
{
  GtkTextBuffer *document;
  GList *actions;
  gint next_redo;
  gint actions_in_current_group;
  gint running_not_undoable_actions;
  gint num_of_groups;
  gint max_undo_levels;
  guint can_undo:1UL;
  guint can_redo:1UL;
/* It is TRUE whether, while undoing an action of the current group (with order_in_group > 1),
	 * the state of the buffer changed from "not modified" to "modified".
	 */
  guint modified_undoing_group:1UL;
/* Pointer to the action (in the action list) marked as "modified".
	 * It is NULL when no action is marked as "modified". */
  GtkSourceUndoAction *modified_action;
}
;
enum __unnamed_enum___F0_L131_C1_CAN_UNDO__COMMA__CAN_REDO__COMMA__LAST_SIGNAL {CAN_UNDO,CAN_REDO,LAST_SIGNAL};
static void gtk_source_undo_manager_class_init(GtkSourceUndoManagerClass *klass);
static void gtk_source_undo_manager_init(GtkSourceUndoManager *um);
static void gtk_source_undo_manager_finalize(GObject *object);
static void gtk_source_undo_manager_insert_text_handler(GtkTextBuffer *buffer,GtkTextIter *pos,const gchar *text,gint length,GtkSourceUndoManager *um);
static void gtk_source_undo_manager_insert_anchor_handler(GtkTextBuffer *buffer,GtkTextIter *pos,GtkTextChildAnchor *anchor,GtkSourceUndoManager *um);
static void gtk_source_undo_manager_delete_range_handler(GtkTextBuffer *buffer,GtkTextIter *start,GtkTextIter *end,GtkSourceUndoManager *um);
static void gtk_source_undo_manager_begin_user_action_handler(GtkTextBuffer *buffer,GtkSourceUndoManager *um);
static void gtk_source_undo_manager_modified_changed_handler(GtkTextBuffer *buffer,GtkSourceUndoManager *um);
static void gtk_source_undo_manager_free_action_list(GtkSourceUndoManager *um);
static void gtk_source_undo_manager_add_action(GtkSourceUndoManager *um,const GtkSourceUndoAction *undo_action);
static void gtk_source_undo_manager_free_first_n_actions(GtkSourceUndoManager *um,gint n);
static void gtk_source_undo_manager_check_list_size(GtkSourceUndoManager *um);
static gboolean gtk_source_undo_manager_merge_action(GtkSourceUndoManager *um,const GtkSourceUndoAction *undo_action);
static GObjectClass *parent_class = (GObjectClass *)((void *)0);
static guint undo_manager_signals[2UL] = {(0)};

GType gtk_source_undo_manager_get_type()
{
  static GType undo_manager_type = 0;
  if (undo_manager_type == 0) {
    static const GTypeInfo our_info = {((sizeof(GtkSourceUndoManagerClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gtk_source_undo_manager_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GtkSourceUndoManager ))), (0), ((GInstanceInitFunc )gtk_source_undo_manager_init), ((const GTypeValueTable *)((void *)0))
/* base_init */
/* base_finalize */
/* class_finalize */
/* class_data */
/* n_preallocs */
/* value_table */
};
    undo_manager_type = g_type_register_static(((GType )(20 << 2)),"GtkSourceUndoManager",&our_info,0);
  }
  return undo_manager_type;
}

static void gtk_source_undo_manager_class_init(GtkSourceUndoManagerClass *klass)
{
  GObjectClass *object_class = (GObjectClass *)(g_type_check_class_cast(((GTypeClass *)klass),((GType )(20 << 2))));
  parent_class = (g_type_class_peek_parent(klass));
  object_class -> finalize = gtk_source_undo_manager_finalize;
  klass -> can_undo = ((void (*)(GtkSourceUndoManager *, gboolean ))((void *)0));
  klass -> can_redo = ((void (*)(GtkSourceUndoManager *, gboolean ))((void *)0));
  undo_manager_signals[CAN_UNDO] = g_signal_new("can_undo",( *((GTypeClass *)object_class)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GtkSourceUndoManagerClass *)((GtkSourceUndoManagerClass *)0))).can_undo))),0,0,g_cclosure_marshal_VOID__BOOLEAN,((GType )(1 << 2)),1,((GType )(5 << 2)));
  undo_manager_signals[CAN_REDO] = g_signal_new("can_redo",( *((GTypeClass *)object_class)).g_type,G_SIGNAL_RUN_LAST,((glong )((size_t )(&( *((GtkSourceUndoManagerClass *)((GtkSourceUndoManagerClass *)0))).can_redo))),0,0,g_cclosure_marshal_VOID__BOOLEAN,((GType )(1 << 2)),1,((GType )(5 << 2)));
}

static void gtk_source_undo_manager_init(GtkSourceUndoManager *um)
{
  um -> priv = ((GtkSourceUndoManagerPrivate *)(g_malloc0_n(1,(sizeof(GtkSourceUndoManagerPrivate )))));
  ( *(um -> priv)).actions = ((GList *)((void *)0));
  ( *(um -> priv)).next_redo = 0;
  ( *(um -> priv)).can_undo = 0;
  ( *(um -> priv)).can_redo = 0;
  ( *(um -> priv)).running_not_undoable_actions = 0;
  ( *(um -> priv)).num_of_groups = 0;
  ( *(um -> priv)).max_undo_levels = 25;
  ( *(um -> priv)).modified_action = ((GtkSourceUndoAction *)((void *)0));
  ( *(um -> priv)).modified_undoing_group = 0;
}

static void gtk_source_undo_manager_finalize(GObject *object)
{
  GtkSourceUndoManager *um;
  do {
    if (object != ((GObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"object != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = gtk_source_undo_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_SOURCE_IS_UNDO_MANAGER (object)");
      return ;
    };
  }while (0);
  um = ((GtkSourceUndoManager *)(g_type_check_instance_cast(((GTypeInstance *)object),gtk_source_undo_manager_get_type())));
  do {
    if ((um -> priv) != ((GtkSourceUndoManagerPrivate *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"um->priv != NULL");
      return ;
    };
  }while (0);
  if (( *(um -> priv)).actions != ((GList *)((void *)0))) {
    gtk_source_undo_manager_free_action_list(um);
  }
  g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *(um -> priv)).document),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )gtk_source_undo_manager_delete_range_handler),um);
  g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *(um -> priv)).document),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )gtk_source_undo_manager_insert_text_handler),um);
  g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *(um -> priv)).document),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )gtk_source_undo_manager_insert_anchor_handler),um);
  g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *(um -> priv)).document),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )gtk_source_undo_manager_begin_user_action_handler),um);
  g_free((um -> priv));
  ( *( *((GObjectClass *)(g_type_check_class_cast(((GTypeClass *)parent_class),((GType )(20 << 2)))))).finalize)(object);
}

GtkSourceUndoManager *gtk_source_undo_manager_new(GtkTextBuffer *buffer)
{
  GtkSourceUndoManager *um;
  um = ((GtkSourceUndoManager *)(g_type_check_instance_cast(((GTypeInstance *)(g_object_new(gtk_source_undo_manager_get_type(),0))),gtk_source_undo_manager_get_type())));
  do {
    if ((um -> priv) != ((GtkSourceUndoManagerPrivate *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"um->priv != NULL");
      return 0;
    };
  }while (0);
  ( *(um -> priv)).document = buffer;
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buffer),((GType )(20 << 2))))),"insert_text",((GCallback )gtk_source_undo_manager_insert_text_handler),um,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buffer),((GType )(20 << 2))))),"insert_child_anchor",((GCallback )gtk_source_undo_manager_insert_anchor_handler),um,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buffer),((GType )(20 << 2))))),"delete_range",((GCallback )gtk_source_undo_manager_delete_range_handler),um,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buffer),((GType )(20 << 2))))),"begin_user_action",((GCallback )gtk_source_undo_manager_begin_user_action_handler),um,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buffer),((GType )(20 << 2))))),"modified_changed",((GCallback )gtk_source_undo_manager_modified_changed_handler),um,0,((GConnectFlags )0));
  return um;
}

void gtk_source_undo_manager_begin_not_undoable_action(GtkSourceUndoManager *um)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)um;
      GType __t = gtk_source_undo_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_SOURCE_IS_UNDO_MANAGER (um)");
      return ;
    };
  }while (0);
  do {
    if ((um -> priv) != ((GtkSourceUndoManagerPrivate *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"um->priv != NULL");
      return ;
    };
  }while (0);
  ++( *(um -> priv)).running_not_undoable_actions;
}

static void gtk_source_undo_manager_end_not_undoable_action_internal(GtkSourceUndoManager *um)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)um;
      GType __t = gtk_source_undo_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_SOURCE_IS_UNDO_MANAGER (um)");
      return ;
    };
  }while (0);
  do {
    if ((um -> priv) != ((GtkSourceUndoManagerPrivate *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"um->priv != NULL");
      return ;
    };
  }while (0);
  do {
    if (( *(um -> priv)).running_not_undoable_actions > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"um->priv->running_not_undoable_actions > 0");
      return ;
    };
  }while (0);
  --( *(um -> priv)).running_not_undoable_actions;
}

void gtk_source_undo_manager_end_not_undoable_action(GtkSourceUndoManager *um)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)um;
      GType __t = gtk_source_undo_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_SOURCE_IS_UNDO_MANAGER (um)");
      return ;
    };
  }while (0);
  do {
    if ((um -> priv) != ((GtkSourceUndoManagerPrivate *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"um->priv != NULL");
      return ;
    };
  }while (0);
  gtk_source_undo_manager_end_not_undoable_action_internal(um);
  if (( *(um -> priv)).running_not_undoable_actions == 0) {
    gtk_source_undo_manager_free_action_list(um);
    ( *(um -> priv)).next_redo = (-1);
    if (( *(um -> priv)).can_undo != 0) {
      ( *(um -> priv)).can_undo = 0;
      g_signal_emit(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)um),((GType )(20 << 2))))),undo_manager_signals[CAN_UNDO],0,0);
    }
    if (( *(um -> priv)).can_redo != 0) {
      ( *(um -> priv)).can_redo = 0;
      g_signal_emit(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)um),((GType )(20 << 2))))),undo_manager_signals[CAN_REDO],0,0);
    }
  }
}

gboolean gtk_source_undo_manager_can_undo(const GtkSourceUndoManager *um)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)um;
      GType __t = gtk_source_undo_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_SOURCE_IS_UNDO_MANAGER (um)");
      return 0;
    };
  }while (0);
  do {
    if ((um -> priv) != ((GtkSourceUndoManagerPrivate *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"um->priv != NULL");
      return 0;
    };
  }while (0);
  return ( *(um -> priv)).can_undo;
}

gboolean gtk_source_undo_manager_can_redo(const GtkSourceUndoManager *um)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)um;
      GType __t = gtk_source_undo_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_SOURCE_IS_UNDO_MANAGER (um)");
      return 0;
    };
  }while (0);
  do {
    if ((um -> priv) != ((GtkSourceUndoManagerPrivate *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"um->priv != NULL");
      return 0;
    };
  }while (0);
  return ( *(um -> priv)).can_redo;
}

static void set_cursor(GtkTextBuffer *buffer,gint cursor)
{
  GtkTextIter iter;
/* Place the cursor at the requested position */
  gtk_text_buffer_get_iter_at_offset(buffer,&iter,cursor);
  gtk_text_buffer_place_cursor(buffer,(&iter));
}

static void insert_text(GtkTextBuffer *buffer,gint pos,const gchar *text,gint len)
{
  GtkTextIter iter;
  gtk_text_buffer_get_iter_at_offset(buffer,&iter,pos);
  gtk_text_buffer_insert(buffer,&iter,text,len);
}

static void insert_anchor(GtkTextBuffer *buffer,gint pos,GtkTextChildAnchor *anchor)
{
  GtkTextIter iter;
  gtk_text_buffer_get_iter_at_offset(buffer,&iter,pos);
  gtk_text_buffer_insert_child_anchor(buffer,&iter,anchor);
}

static void delete_text(GtkTextBuffer *buffer,gint start,gint end)
{
  GtkTextIter start_iter;
  GtkTextIter end_iter;
  gtk_text_buffer_get_iter_at_offset(buffer,&start_iter,start);
  if (end < 0) 
    gtk_text_buffer_get_end_iter(buffer,&end_iter);
  else 
    gtk_text_buffer_get_iter_at_offset(buffer,&end_iter,end);
  gtk_text_buffer_delete(buffer,&start_iter,&end_iter);
}

static gchar *get_chars(GtkTextBuffer *buffer,gint start,gint end)
{
  GtkTextIter start_iter;
  GtkTextIter end_iter;
  gtk_text_buffer_get_iter_at_offset(buffer,&start_iter,start);
  if (end < 0) 
    gtk_text_buffer_get_end_iter(buffer,&end_iter);
  else 
    gtk_text_buffer_get_iter_at_offset(buffer,&end_iter,end);
  return gtk_text_buffer_get_slice(buffer,(&start_iter),(&end_iter),(!0));
}

void gtk_source_undo_manager_undo(GtkSourceUndoManager *um)
{
  GtkSourceUndoAction *undo_action;
  gboolean modified = 0;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)um;
      GType __t = gtk_source_undo_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_SOURCE_IS_UNDO_MANAGER (um)");
      return ;
    };
  }while (0);
  do {
    if ((um -> priv) != ((GtkSourceUndoManagerPrivate *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"um->priv != NULL");
      return ;
    };
  }while (0);
  do {
    if (( *(um -> priv)).can_undo != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"um->priv->can_undo");
      return ;
    };
  }while (0);
  ( *(um -> priv)).modified_undoing_group = 0;
  gtk_source_undo_manager_begin_not_undoable_action(um);
  do {
    undo_action = (g_list_nth_data(( *(um -> priv)).actions,(( *(um -> priv)).next_redo + 1)));
    do {
      if (undo_action != ((GtkSourceUndoAction *)((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"undo_action != NULL");
        return ;
      };
    }while (0);
/* undo_action->modified can be TRUE only if undo_action->order_in_group <= 1 */
    do {
      if (((undo_action -> order_in_group) <= 1) || (((undo_action -> order_in_group) > 1) && !((undo_action -> modified) != 0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"(undo_action->order_in_group <= 1) || ((undo_action->order_in_group > 1) && !undo_action->modified)");
        return ;
      };
    }while (0);
    if ((undo_action -> order_in_group) <= 1) {
/* Set modified to TRUE only if the buffer did not change its state from
			 * "not modified" to "modified" undoing an action (with order_in_group > 1)
			 * in current group. */
      modified = (((undo_action -> modified) != 0) && !(( *(um -> priv)).modified_undoing_group != 0));
    }
    switch(undo_action -> action_type){
      case GTK_SOURCE_UNDO_ACTION_DELETE:
{
        insert_text(( *(um -> priv)).document,undo_action -> action.delete.start,undo_action -> action.delete.text,(strlen(undo_action -> action.delete.text)));
        if (undo_action -> action.delete.forward != 0) 
          set_cursor(( *(um -> priv)).document,undo_action -> action.delete.start);
        else 
          set_cursor(( *(um -> priv)).document,undo_action -> action.delete.end);
        break; 
      }
      case GTK_SOURCE_UNDO_ACTION_INSERT:
{
        delete_text(( *(um -> priv)).document,undo_action -> action.insert.pos,(undo_action -> action.insert.pos + undo_action -> action.insert.chars));
        set_cursor(( *(um -> priv)).document,undo_action -> action.insert.pos);
        break; 
      }
      case GTK_SOURCE_UNDO_ACTION_INSERT_ANCHOR:
{
        delete_text(( *(um -> priv)).document,undo_action -> action.insert_anchor.pos,(undo_action -> action.insert_anchor.pos + 1));
/* XXX: This may be a bug in GTK+ */
        ( *undo_action -> action.insert_anchor.anchor).segment = ((gpointer )((void *)0));
        break; 
      }
      default:
{
/* Unknown action type. */
        do {
          g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","gtksourceundomanager.c",534,((const char *)__func__));
          return ;
        }while (0);
      }
    }
    ++( *(um -> priv)).next_redo;
  }while ((undo_action -> order_in_group) > 1);
  if (modified != 0) {
    --( *(um -> priv)).next_redo;
    gtk_text_buffer_set_modified(( *(um -> priv)).document,0);
    ++( *(um -> priv)).next_redo;
  }
  gtk_source_undo_manager_end_not_undoable_action_internal(um);
  ( *(um -> priv)).modified_undoing_group = 0;
  if (!(( *(um -> priv)).can_redo != 0)) {
    ( *(um -> priv)).can_redo = (!0);
    g_signal_emit(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)um),((GType )(20 << 2))))),undo_manager_signals[CAN_REDO],0,!0);
  }
  if (( *(um -> priv)).next_redo >= ((gint )(g_list_length(( *(um -> priv)).actions) - 1))) {
    ( *(um -> priv)).can_undo = 0;
    g_signal_emit(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)um),((GType )(20 << 2))))),undo_manager_signals[CAN_UNDO],0,0);
  }
}

void gtk_source_undo_manager_redo(GtkSourceUndoManager *um)
{
  GtkSourceUndoAction *undo_action;
  gboolean modified = 0;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)um;
      GType __t = gtk_source_undo_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_SOURCE_IS_UNDO_MANAGER (um)");
      return ;
    };
  }while (0);
  do {
    if ((um -> priv) != ((GtkSourceUndoManagerPrivate *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"um->priv != NULL");
      return ;
    };
  }while (0);
  do {
    if (( *(um -> priv)).can_redo != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"um->priv->can_redo");
      return ;
    };
  }while (0);
  undo_action = (g_list_nth_data(( *(um -> priv)).actions,( *(um -> priv)).next_redo));
  do {
    if (undo_action != ((GtkSourceUndoAction *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"undo_action != NULL");
      return ;
    };
  }while (0);
  gtk_source_undo_manager_begin_not_undoable_action(um);
  do {
    if ((undo_action -> modified) != 0) {
      do {
        if ((undo_action -> order_in_group) <= 1) {
        }
        else {
          g_return_if_fail_warning(0,((const char *)__func__),"undo_action->order_in_group <= 1");
          return ;
        };
      }while (0);
      modified = (!0);
    }
    --( *(um -> priv)).next_redo;
    switch(undo_action -> action_type){
      case GTK_SOURCE_UNDO_ACTION_DELETE:
{
        delete_text(( *(um -> priv)).document,undo_action -> action.delete.start,undo_action -> action.delete.end);
        set_cursor(( *(um -> priv)).document,undo_action -> action.delete.start);
        break; 
      }
      case GTK_SOURCE_UNDO_ACTION_INSERT:
{
        set_cursor(( *(um -> priv)).document,undo_action -> action.insert.pos);
        insert_text(( *(um -> priv)).document,undo_action -> action.insert.pos,undo_action -> action.insert.text,undo_action -> action.insert.length);
        break; 
      }
      case GTK_SOURCE_UNDO_ACTION_INSERT_ANCHOR:
{
        set_cursor(( *(um -> priv)).document,undo_action -> action.insert_anchor.pos);
        insert_anchor(( *(um -> priv)).document,undo_action -> action.insert_anchor.pos,undo_action -> action.insert_anchor.anchor);
        break; 
      }
      default:
{
/* Unknown action type */
        ++( *(um -> priv)).next_redo;
        do {
          g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","gtksourceundomanager.c",637,((const char *)__func__));
          return ;
        }while (0);
      }
    }
    if (( *(um -> priv)).next_redo < 0) 
      undo_action = ((GtkSourceUndoAction *)((void *)0));
    else 
      undo_action = (g_list_nth_data(( *(um -> priv)).actions,( *(um -> priv)).next_redo));
  }while ((undo_action != ((GtkSourceUndoAction *)((void *)0))) && ((undo_action -> order_in_group) > 1));
  if (modified != 0) {
    ++( *(um -> priv)).next_redo;
    gtk_text_buffer_set_modified(( *(um -> priv)).document,0);
    --( *(um -> priv)).next_redo;
  }
  gtk_source_undo_manager_end_not_undoable_action_internal(um);
  if (( *(um -> priv)).next_redo < 0) {
    ( *(um -> priv)).can_redo = 0;
    g_signal_emit(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)um),((GType )(20 << 2))))),undo_manager_signals[CAN_REDO],0,0);
  }
  if (!(( *(um -> priv)).can_undo != 0)) {
    ( *(um -> priv)).can_undo = (!0);
    g_signal_emit(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)um),((GType )(20 << 2))))),undo_manager_signals[CAN_UNDO],0,!0);
  }
}

static void gtk_source_undo_action_free(GtkSourceUndoAction *action)
{
  if (action == ((GtkSourceUndoAction *)((void *)0))) 
    return ;
  if ((action -> action_type) == GTK_SOURCE_UNDO_ACTION_INSERT) 
    g_free(action -> action.insert.text);
  else if ((action -> action_type) == GTK_SOURCE_UNDO_ACTION_DELETE) 
    g_free(action -> action.delete.text);
  else if ((action -> action_type) == GTK_SOURCE_UNDO_ACTION_INSERT_ANCHOR) 
    g_object_unref(action -> action.insert_anchor.anchor);
  else {
    g_free(action);
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","gtksourceundomanager.c",683,((const char *)__func__));
      return ;
    }while (0);
  }
  g_free(action);
}

static void gtk_source_undo_manager_free_action_list(GtkSourceUndoManager *um)
{
  GList *l;
  l = ( *(um -> priv)).actions;
  while(l != ((GList *)((void *)0))){
    GtkSourceUndoAction *action = (l -> data);
    if ((action -> order_in_group) == 1) 
      --( *(um -> priv)).num_of_groups;
    if ((action -> modified) != 0) 
      ( *(um -> priv)).modified_action = ((GtkSourceUndoAction *)((void *)0));
    gtk_source_undo_action_free(action);
    l = ((l != 0)?( *((GList *)l)).next : ((struct _GList *)((void *)0)));
  }
  g_list_free(( *(um -> priv)).actions);
  ( *(um -> priv)).actions = ((GList *)((void *)0));
}

static void gtk_source_undo_manager_insert_text_handler(GtkTextBuffer *buffer,GtkTextIter *pos,const gchar *text,gint length,GtkSourceUndoManager *um)
{
  GtkSourceUndoAction undo_action;
  if (( *(um -> priv)).running_not_undoable_actions > 0) 
    return ;
  undo_action.action_type = GTK_SOURCE_UNDO_ACTION_INSERT;
  undo_action.action.insert.pos = gtk_text_iter_get_offset(pos);
  undo_action.action.insert.text = ((gchar *)text);
  undo_action.action.insert.length = length;
  undo_action.action.insert.chars = (g_utf8_strlen(text,length));
  if ((undo_action.action.insert.chars > 1) || (g_utf8_get_char(text) == 10)) 
    undo_action.mergeable = 0;
  else 
    undo_action.mergeable = (!0);
  undo_action.modified = 0;
  gtk_source_undo_manager_add_action(um,(&undo_action));
}

static void gtk_source_undo_manager_insert_anchor_handler(GtkTextBuffer *buffer,GtkTextIter *pos,GtkTextChildAnchor *anchor,GtkSourceUndoManager *um)
{
  GtkSourceUndoAction undo_action;
  if (( *(um -> priv)).running_not_undoable_actions > 0) 
    return ;
  undo_action.action_type = GTK_SOURCE_UNDO_ACTION_INSERT_ANCHOR;
  undo_action.action.insert_anchor.pos = gtk_text_iter_get_offset(pos);
  undo_action.action.insert_anchor.anchor = (g_object_ref(anchor));
  undo_action.mergeable = 0;
  undo_action.modified = 0;
  gtk_source_undo_manager_add_action(um,(&undo_action));
}

static void gtk_source_undo_manager_delete_range_handler(GtkTextBuffer *buffer,GtkTextIter *start,GtkTextIter *end,GtkSourceUndoManager *um)
{
  GtkSourceUndoAction undo_action;
  GtkTextIter insert_iter;
  if (( *(um -> priv)).running_not_undoable_actions > 0) 
    return ;
  undo_action.action_type = GTK_SOURCE_UNDO_ACTION_DELETE;
  gtk_text_iter_order(start,end);
  undo_action.action.delete.start = gtk_text_iter_get_offset(start);
  undo_action.action.delete.end = gtk_text_iter_get_offset(end);
  undo_action.action.delete.text = get_chars(buffer,undo_action.action.delete.start,undo_action.action.delete.end);
/* figure out if the user used the Delete or the Backspace key */
  gtk_text_buffer_get_iter_at_mark(buffer,&insert_iter,gtk_text_buffer_get_insert(buffer));
  if (gtk_text_iter_get_offset((&insert_iter)) <= undo_action.action.delete.start) 
    undo_action.action.delete.forward = (!0);
  else 
    undo_action.action.delete.forward = 0;
  if (((undo_action.action.delete.end - undo_action.action.delete.start) > 1) || (g_utf8_get_char(undo_action.action.delete.text) == 10)) 
    undo_action.mergeable = 0;
  else 
    undo_action.mergeable = (!0);
  undo_action.modified = 0;
  gtk_source_undo_manager_add_action(um,(&undo_action));
  g_free(undo_action.action.delete.text);
}

static void gtk_source_undo_manager_begin_user_action_handler(GtkTextBuffer *buffer,GtkSourceUndoManager *um)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)um;
      GType __t = gtk_source_undo_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_SOURCE_IS_UNDO_MANAGER (um)");
      return ;
    };
  }while (0);
  do {
    if ((um -> priv) != ((GtkSourceUndoManagerPrivate *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"um->priv != NULL");
      return ;
    };
  }while (0);
  if (( *(um -> priv)).running_not_undoable_actions > 0) 
    return ;
  ( *(um -> priv)).actions_in_current_group = 0;
}

static void gtk_source_undo_manager_add_action(GtkSourceUndoManager *um,const GtkSourceUndoAction *undo_action)
{
  GtkSourceUndoAction *action;
  if (( *(um -> priv)).next_redo >= 0) {
    gtk_source_undo_manager_free_first_n_actions(um,(( *(um -> priv)).next_redo + 1));
  }
  ( *(um -> priv)).next_redo = (-1);
  if (!(gtk_source_undo_manager_merge_action(um,undo_action) != 0)) {
    action = ((GtkSourceUndoAction *)(g_malloc_n(1,(sizeof(GtkSourceUndoAction )))));
     *action =  *undo_action;
    if ((action -> action_type) == GTK_SOURCE_UNDO_ACTION_INSERT) 
      action -> action.insert.text = g_strndup(undo_action -> action.insert.text,undo_action -> action.insert.length);
    else if ((action -> action_type) == GTK_SOURCE_UNDO_ACTION_DELETE) 
      action -> action.delete.text = g_strdup(undo_action -> action.delete.text);
    else if ((action -> action_type) == GTK_SOURCE_UNDO_ACTION_INSERT_ANCHOR) {
/* Nothing needs to be done */
    }
    else {
      g_free(action);
      do {
        g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","gtksourceundomanager.c",853,((const char *)__func__));
        return ;
      }while (0);
    }
    ++( *(um -> priv)).actions_in_current_group;
    action -> order_in_group = ( *(um -> priv)).actions_in_current_group;
    if ((action -> order_in_group) == 1) 
      ++( *(um -> priv)).num_of_groups;
    ( *(um -> priv)).actions = g_list_prepend(( *(um -> priv)).actions,action);
  }
  gtk_source_undo_manager_check_list_size(um);
  if (!(( *(um -> priv)).can_undo != 0)) {
    ( *(um -> priv)).can_undo = (!0);
    g_signal_emit(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)um),((GType )(20 << 2))))),undo_manager_signals[CAN_UNDO],0,!0);
  }
  if (( *(um -> priv)).can_redo != 0) {
    ( *(um -> priv)).can_redo = 0;
    g_signal_emit(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)um),((GType )(20 << 2))))),undo_manager_signals[CAN_REDO],0,0);
  }
}

static void gtk_source_undo_manager_free_first_n_actions(GtkSourceUndoManager *um,gint n)
{
  gint i;
  if (( *(um -> priv)).actions == ((GList *)((void *)0))) 
    return ;
  for (i = 0; i < n; i++) {
    GtkSourceUndoAction *action = ( *g_list_first(( *(um -> priv)).actions)).data;
    if ((action -> order_in_group) == 1) 
      --( *(um -> priv)).num_of_groups;
    if ((action -> modified) != 0) 
      ( *(um -> priv)).modified_action = ((GtkSourceUndoAction *)((void *)0));
    gtk_source_undo_action_free(action);
    ( *(um -> priv)).actions = g_list_delete_link(( *(um -> priv)).actions,( *(um -> priv)).actions);
    if (( *(um -> priv)).actions == ((GList *)((void *)0))) 
      return ;
  }
}

static void gtk_source_undo_manager_check_list_size(GtkSourceUndoManager *um)
{
  gint undo_levels;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)um;
      GType __t = gtk_source_undo_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_SOURCE_IS_UNDO_MANAGER (um)");
      return ;
    };
  }while (0);
  do {
    if ((um -> priv) != ((GtkSourceUndoManagerPrivate *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"um->priv != NULL");
      return ;
    };
  }while (0);
  undo_levels = gtk_source_undo_manager_get_max_undo_levels(um);
  if (undo_levels < 1) 
    return ;
  if (( *(um -> priv)).num_of_groups > undo_levels) {
    GtkSourceUndoAction *undo_action;
    GList *last;
    last = g_list_last(( *(um -> priv)).actions);
    undo_action = ((GtkSourceUndoAction *)(last -> data));
    do {
      GList *tmp;
      if ((undo_action -> order_in_group) == 1) 
        --( *(um -> priv)).num_of_groups;
      if ((undo_action -> modified) != 0) 
        ( *(um -> priv)).modified_action = ((GtkSourceUndoAction *)((void *)0));
      gtk_source_undo_action_free(undo_action);
      tmp = ((last != 0)?( *((GList *)last)).prev : ((struct _GList *)((void *)0)));
      ( *(um -> priv)).actions = g_list_delete_link(( *(um -> priv)).actions,last);
      last = tmp;
      do {
        if (last != ((GList *)((void *)0))) {
        }
        else {
          g_return_if_fail_warning(0,((const char *)__func__),"last != NULL");
          return ;
        };
      }while (0);
      undo_action = ((GtkSourceUndoAction *)(last -> data));
    }while (((undo_action -> order_in_group) > 1) || (( *(um -> priv)).num_of_groups > undo_levels));
  }
}
/**
 * gtk_source_undo_manager_merge_action:
 * @um: a #GtkSourceUndoManager.
 * @undo_action: a #GtkSourceUndoAction.
 *
 * This function tries to merge the undo action at the top of
 * the stack with a new undo action. So when we undo for example
 * typing, we can undo the whole word and not each letter by itself.
 *
 * Return Value: %TRUE is merge was successful, %FALSE otherwise.
 **/

static gboolean gtk_source_undo_manager_merge_action(GtkSourceUndoManager *um,const GtkSourceUndoAction *undo_action)
{
  GtkSourceUndoAction *last_action;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)um;
      GType __t = gtk_source_undo_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_SOURCE_IS_UNDO_MANAGER (um)");
      return 0;
    };
  }while (0);
  do {
    if ((um -> priv) != ((GtkSourceUndoManagerPrivate *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"um->priv != NULL");
      return 0;
    };
  }while (0);
  if (( *(um -> priv)).actions == ((GList *)((void *)0))) 
    return 0;
  last_action = ((GtkSourceUndoAction *)(g_list_nth_data(( *(um -> priv)).actions,0)));
  if (!((last_action -> mergeable) != 0)) 
    return 0;
  if (!((undo_action -> mergeable) != 0) || ((undo_action -> action_type) != (last_action -> action_type))) {
    last_action -> mergeable = 0;
    return 0;
  }
  if ((undo_action -> action_type) == GTK_SOURCE_UNDO_ACTION_DELETE) {
    if ((last_action -> action.delete.forward != undo_action -> action.delete.forward) || ((last_action -> action.delete.start != undo_action -> action.delete.start) && (last_action -> action.delete.start != undo_action -> action.delete.end))) {
      last_action -> mergeable = 0;
      return 0;
    }
    if (last_action -> action.delete.start == undo_action -> action.delete.start) {
      gchar *str;
#define L  (last_action->action.delete.end - last_action->action.delete.start - 1)
#define g_utf8_get_char_at(p,i) g_utf8_get_char(g_utf8_offset_to_pointer((p),(i)))
/* Deleted with the delete key */
      if (((g_utf8_get_char(undo_action -> action.delete.text) != 32) && (g_utf8_get_char(undo_action -> action.delete.text) != 9)) && ((g_utf8_get_char((g_utf8_offset_to_pointer(last_action -> action.delete.text,((last_action -> action.delete.end - last_action -> action.delete.start) - 1)))) == 32) || (g_utf8_get_char((g_utf8_offset_to_pointer(last_action -> action.delete.text,((last_action -> action.delete.end - last_action -> action.delete.start) - 1)))) == 9))) {
        last_action -> mergeable = 0;
        return 0;
      }
      str = g_strdup_printf("%s%s",last_action -> action.delete.text,undo_action -> action.delete.text);
      g_free(last_action -> action.delete.text);
      last_action -> action.delete.end += (undo_action -> action.delete.end - undo_action -> action.delete.start);
      last_action -> action.delete.text = str;
    }
    else {
      gchar *str;
/* Deleted with the backspace key */
      if (((g_utf8_get_char(undo_action -> action.delete.text) != 32) && (g_utf8_get_char(undo_action -> action.delete.text) != 9)) && ((g_utf8_get_char(last_action -> action.delete.text) == 32) || (g_utf8_get_char(last_action -> action.delete.text) == 9))) {
        last_action -> mergeable = 0;
        return 0;
      }
      str = g_strdup_printf("%s%s",undo_action -> action.delete.text,last_action -> action.delete.text);
      g_free(last_action -> action.delete.text);
      last_action -> action.delete.start = undo_action -> action.delete.start;
      last_action -> action.delete.text = str;
    }
  }
  else if ((undo_action -> action_type) == GTK_SOURCE_UNDO_ACTION_INSERT) {
    gchar *str;
#define I (last_action->action.insert.chars - 1)
    if ((undo_action -> action.insert.pos != (last_action -> action.insert.pos + last_action -> action.insert.chars)) || (((g_utf8_get_char(undo_action -> action.insert.text) != 32) && (g_utf8_get_char(undo_action -> action.insert.text) != 9)) && ((g_utf8_get_char((g_utf8_offset_to_pointer(last_action -> action.insert.text,(last_action -> action.insert.chars - 1)))) == 32) || (g_utf8_get_char((g_utf8_offset_to_pointer(last_action -> action.insert.text,(last_action -> action.insert.chars - 1)))) == 9)))) {
      last_action -> mergeable = 0;
      return 0;
    }
    str = g_strdup_printf("%s%s",last_action -> action.insert.text,undo_action -> action.insert.text);
    g_free(last_action -> action.insert.text);
    last_action -> action.insert.length += undo_action -> action.insert.length;
    last_action -> action.insert.text = str;
    last_action -> action.insert.chars += undo_action -> action.insert.chars;
  }
  else if ((undo_action -> action_type) == GTK_SOURCE_UNDO_ACTION_INSERT_ANCHOR) {
/* Nothing needs to be done */
  }
  else 
/* Unknown action inside undo merge encountered */
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","gtksourceundomanager.c",1079,((const char *)__func__));
      return (!0);
    }while (0);
  return (!0);
}

gint gtk_source_undo_manager_get_max_undo_levels(GtkSourceUndoManager *um)
{
  do {
    if (um != ((GtkSourceUndoManager *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"um != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)um;
      GType __t = gtk_source_undo_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_SOURCE_IS_UNDO_MANAGER (um)");
      return 0;
    };
  }while (0);
  return ( *(um -> priv)).max_undo_levels;
}

void gtk_source_undo_manager_set_max_undo_levels(GtkSourceUndoManager *um,gint max_undo_levels)
{
  gint old_levels;
  do {
    if (um != ((GtkSourceUndoManager *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"um != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)um;
      GType __t = gtk_source_undo_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_SOURCE_IS_UNDO_MANAGER (um)");
      return ;
    };
  }while (0);
  old_levels = ( *(um -> priv)).max_undo_levels;
  ( *(um -> priv)).max_undo_levels = max_undo_levels;
  if (max_undo_levels < 1) 
    return ;
  if (old_levels > max_undo_levels) {
/* strip redo actions first */
    while((( *(um -> priv)).next_redo >= 0) && (( *(um -> priv)).num_of_groups > max_undo_levels)){
      gtk_source_undo_manager_free_first_n_actions(um,1);
      ( *(um -> priv)).next_redo--;
    }
/* now remove undo actions if necessary */
    gtk_source_undo_manager_check_list_size(um);
/* emit "can_undo" and/or "can_redo" if appropiate */
    if ((( *(um -> priv)).next_redo < 0) && (( *(um -> priv)).can_redo != 0)) {
      ( *(um -> priv)).can_redo = 0;
      g_signal_emit(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)um),((GType )(20 << 2))))),undo_manager_signals[CAN_REDO],0,0);
    }
    if ((( *(um -> priv)).can_undo != 0) && (( *(um -> priv)).next_redo >= ((gint )(g_list_length(( *(um -> priv)).actions) - 1)))) {
      ( *(um -> priv)).can_undo = 0;
      g_signal_emit(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)um),((GType )(20 << 2))))),undo_manager_signals[CAN_UNDO],0,0);
    }
  }
}

static void gtk_source_undo_manager_modified_changed_handler(GtkTextBuffer *buffer,GtkSourceUndoManager *um)
{
  GtkSourceUndoAction *action;
  GList *list;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)um;
      GType __t = gtk_source_undo_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_SOURCE_IS_UNDO_MANAGER (um)");
      return ;
    };
  }while (0);
  do {
    if ((um -> priv) != ((GtkSourceUndoManagerPrivate *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"um->priv != NULL");
      return ;
    };
  }while (0);
  if (( *(um -> priv)).actions == ((GList *)((void *)0))) 
    return ;
  list = g_list_nth(( *(um -> priv)).actions,(( *(um -> priv)).next_redo + 1));
  if (list != ((GList *)((void *)0))) 
    action = ((GtkSourceUndoAction *)(list -> data));
  else 
    action = ((GtkSourceUndoAction *)((void *)0));
  if (gtk_text_buffer_get_modified(buffer) == 0) {
    if (action != ((GtkSourceUndoAction *)((void *)0))) 
      action -> mergeable = 0;
    if (( *(um -> priv)).modified_action != ((GtkSourceUndoAction *)((void *)0))) {
      ( *( *(um -> priv)).modified_action).modified = 0;
      ( *(um -> priv)).modified_action = ((GtkSourceUndoAction *)((void *)0));
    }
    return ;
  }
  if (action == ((GtkSourceUndoAction *)((void *)0))) {
    do {
      if (( *(um -> priv)).running_not_undoable_actions > 0) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"um->priv->running_not_undoable_actions > 0");
        return ;
      };
    }while (0);
    return ;
  }
/* gtk_text_buffer_get_modified (buffer) == TRUE */
  do {
    if (( *(um -> priv)).modified_action == ((GtkSourceUndoAction *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"um->priv->modified_action == NULL");
      return ;
    };
  }while (0);
  if ((action -> order_in_group) > 1) 
    ( *(um -> priv)).modified_undoing_group = (!0);
  while((action -> order_in_group) > 1){
    list = ((list != 0)?( *((GList *)list)).next : ((struct _GList *)((void *)0)));
    do {
      if (list != ((GList *)((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"list != NULL");
        return ;
      };
    }while (0);
    action = ((GtkSourceUndoAction *)(list -> data));
    do {
      if (action != ((GtkSourceUndoAction *)((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"action != NULL");
        return ;
      };
    }while (0);
  }
  action -> modified = (!0);
  ( *(um -> priv)).modified_action = action;
}
