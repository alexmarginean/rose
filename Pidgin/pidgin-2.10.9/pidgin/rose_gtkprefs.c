/**
 * @file gtkprefs.c GTK+ Preferences
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "pidgin.h"
#include "debug.h"
#include "nat-pmp.h"
#include "notify.h"
#include "prefs.h"
#include "proxy.h"
#include "prpl.h"
#include "request.h"
#include "savedstatuses.h"
#include "sound.h"
#include "sound-theme.h"
#include "stun.h"
#include "theme-manager.h"
#include "upnp.h"
#include "util.h"
#include "network.h"
#include "gtkblist.h"
#include "gtkconv.h"
#include "gtkdebug.h"
#include "gtkdialogs.h"
#include "gtkimhtml.h"
#include "gtkimhtmltoolbar.h"
#include "gtkprefs.h"
#include "gtksavedstatuses.h"
#include "gtksound.h"
#include "gtkstatus-icon-theme.h"
#include "gtkthemes.h"
#include "gtkutils.h"
#include "pidginstock.h"
#define PROXYHOST 0
#define PROXYPORT 1
#define PROXYUSER 2
#define PROXYPASS 3
#define PREFS_OPTIMAL_ICON_SIZE 32

struct theme_info 
{
  gchar *type;
  gchar *extension;
  gchar *original_name;
}
;
/* Main dialog */
static GtkWidget *prefs = (GtkWidget *)((void *)0);
/* Notebook */
static GtkWidget *prefsnotebook = (GtkWidget *)((void *)0);
static int notebook_page = 0;
/* Conversations page */
static GtkWidget *sample_imhtml = (GtkWidget *)((void *)0);
/* Themes page */
static GtkWidget *prefs_sound_themes_combo_box;
static GtkWidget *prefs_blist_themes_combo_box;
static GtkWidget *prefs_status_themes_combo_box;
static GtkWidget *prefs_smiley_themes_combo_box;
/* Sound theme specific */
static GtkWidget *sound_entry = (GtkWidget *)((void *)0);
static int sound_row_sel = 0;
static gboolean prefs_sound_themes_loading;
/* These exist outside the lifetime of the prefs dialog */
static GtkListStore *prefs_sound_themes;
static GtkListStore *prefs_blist_themes;
static GtkListStore *prefs_status_icon_themes;
static GtkListStore *prefs_smiley_themes;
/*
 * PROTOTYPES
 */
static void delete_prefs(GtkWidget *,void *);

static void update_spin_value(GtkWidget *w,GtkWidget *spin)
{
  const char *key = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)spin),((GType )(20 << 2))))),"val"));
  int value;
  value = gtk_spin_button_get_value_as_int(((GtkSpinButton *)(g_type_check_instance_cast(((GTypeInstance *)spin),gtk_spin_button_get_type()))));
  purple_prefs_set_int(key,value);
}

GtkWidget *pidgin_prefs_labeled_spin_button(GtkWidget *box,const gchar *title,const char *key,int min,int max,GtkSizeGroup *sg)
{
  GtkWidget *spin;
  GtkObject *adjust;
  int val;
  val = purple_prefs_get_int(key);
  adjust = gtk_adjustment_new(val,min,max,1,1,0);
  spin = gtk_spin_button_new(((GtkAdjustment *)(g_type_check_instance_cast(((GTypeInstance *)adjust),gtk_adjustment_get_type()))),1,0);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)spin),((GType )(20 << 2))))),"val",((char *)key));
  if (max < 10000) 
    gtk_widget_set_size_request(spin,50,(-1));
  else 
    gtk_widget_set_size_request(spin,60,(-1));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)adjust),((GType )(20 << 2))))),"value-changed",((GCallback )update_spin_value),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)spin),gtk_widget_get_type()))),0,((GConnectFlags )0));
  gtk_widget_show(spin);
  return pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gtk_box_get_type()))),title,sg,spin,0,0);
}

static void entry_set(GtkEntry *entry,gpointer data)
{
  const char *key = (const char *)data;
  purple_prefs_set_string(key,gtk_entry_get_text(entry));
}

GtkWidget *pidgin_prefs_labeled_entry(GtkWidget *page,const gchar *title,const char *key,GtkSizeGroup *sg)
{
  GtkWidget *entry;
  const gchar *value;
  value = purple_prefs_get_string(key);
  entry = gtk_entry_new();
  gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),value);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"changed",((GCallback )entry_set),((char *)key),0,((GConnectFlags )0));
  gtk_widget_show(entry);
  return pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)page),gtk_box_get_type()))),title,sg,entry,(!0),0);
}

GtkWidget *pidgin_prefs_labeled_password(GtkWidget *page,const gchar *title,const char *key,GtkSizeGroup *sg)
{
  GtkWidget *entry;
  const gchar *value;
  value = purple_prefs_get_string(key);
  entry = gtk_entry_new();
  gtk_entry_set_visibility(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),0);
  gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),value);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"changed",((GCallback )entry_set),((char *)key),0,((GConnectFlags )0));
  gtk_widget_show(entry);
  return pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)page),gtk_box_get_type()))),title,sg,entry,(!0),0);
}

static void dropdown_set(GObject *w,const char *key)
{
  const char *str_value;
  int int_value;
  PurplePrefType type;
  type = ((gint )((glong )(g_object_get_data(w,"type"))));
  if (type == PURPLE_PREF_INT) {
    int_value = ((gint )((glong )(g_object_get_data(w,"value"))));
    purple_prefs_set_int(key,int_value);
  }
  else if (type == PURPLE_PREF_STRING) {
    str_value = ((const char *)(g_object_get_data(w,"value")));
    purple_prefs_set_string(key,str_value);
  }
  else if (type == PURPLE_PREF_BOOLEAN) {
    purple_prefs_set_bool(key,((gint )((glong )(g_object_get_data(w,"value")))));
  }
}

GtkWidget *pidgin_prefs_dropdown_from_list(GtkWidget *box,const gchar *title,PurplePrefType type,const char *key,GList *menuitems)
{
  GtkWidget *dropdown;
  GtkWidget *opt;
  GtkWidget *menu;
  GtkWidget *label = (GtkWidget *)((void *)0);
  gchar *text;
  const char *stored_str = (const char *)((void *)0);
  int stored_int = 0;
  int int_value = 0;
  const char *str_value = (const char *)((void *)0);
  int o = 0;
  do {
    if (menuitems != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"menuitems != NULL");
      return 0;
    };
  }while (0);
  dropdown = gtk_option_menu_new();
  menu = gtk_menu_new();
  if (type == PURPLE_PREF_INT) 
    stored_int = purple_prefs_get_int(key);
  else if (type == PURPLE_PREF_STRING) 
    stored_str = purple_prefs_get_string(key);
  while((menuitems != ((GList *)((void *)0))) && ((text = ((char *)(menuitems -> data))) != ((gchar *)((void *)0)))){
    menuitems = ((menuitems != 0)?( *((GList *)menuitems)).next : ((struct _GList *)((void *)0)));
    do {
      if (menuitems != ((GList *)((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"menuitems != NULL");
        return 0;
      };
    }while (0);
    opt = gtk_menu_item_new_with_label(text);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)opt),((GType )(20 << 2))))),"type",((gpointer )((glong )type)));
    if (type == PURPLE_PREF_INT) {
      int_value = ((gint )((glong )(menuitems -> data)));
      g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)opt),((GType )(20 << 2))))),"value",((gpointer )((glong )int_value)));
    }
    else if (type == PURPLE_PREF_STRING) {
      str_value = ((const char *)(menuitems -> data));
      g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)opt),((GType )(20 << 2))))),"value",((char *)str_value));
    }
    else if (type == PURPLE_PREF_BOOLEAN) {
      g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)opt),((GType )(20 << 2))))),"value",(menuitems -> data));
    }
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)opt),((GType )(20 << 2))))),"activate",((GCallback )dropdown_set),((char *)key),0,((GConnectFlags )0));
    gtk_widget_show(opt);
    gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),opt);
    if ((((type == PURPLE_PREF_INT) && (stored_int == int_value)) || (((type == PURPLE_PREF_STRING) && (stored_str != ((const char *)((void *)0)))) && !(strcmp(stored_str,str_value) != 0))) || ((type == PURPLE_PREF_BOOLEAN) && (purple_prefs_get_bool(key) == ((gint )((glong )(menuitems -> data)))))) {
      gtk_menu_set_active(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_get_type()))),o);
    }
    menuitems = ((menuitems != 0)?( *((GList *)menuitems)).next : ((struct _GList *)((void *)0)));
    o++;
  }
  gtk_option_menu_set_menu(((GtkOptionMenu *)(g_type_check_instance_cast(((GTypeInstance *)dropdown),gtk_option_menu_get_type()))),menu);
  pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gtk_box_get_type()))),title,0,dropdown,0,&label);
  return label;
}

GtkWidget *pidgin_prefs_dropdown(GtkWidget *box,const gchar *title,PurplePrefType type,const char *key,... )
{
  va_list ap;
  GList *menuitems = (GList *)((void *)0);
  GtkWidget *dropdown = (GtkWidget *)((void *)0);
  char *name;
  int int_value;
  const char *str_value;
  do {
    if (((type == PURPLE_PREF_BOOLEAN) || (type == PURPLE_PREF_INT)) || (type == PURPLE_PREF_STRING)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type == PURPLE_PREF_BOOLEAN || type == PURPLE_PREF_INT || type == PURPLE_PREF_STRING");
      return 0;
    };
  }while (0);
  va_start(ap,key);
  while((name = (va_arg(ap,char *))) != ((char *)((void *)0))){
    menuitems = g_list_prepend(menuitems,name);
    if ((type == PURPLE_PREF_INT) || (type == PURPLE_PREF_BOOLEAN)) {
      int_value = (va_arg(ap,int ));
      menuitems = g_list_prepend(menuitems,((gpointer )((glong )int_value)));
    }
    else {
      str_value = (va_arg(ap,const char *));
      menuitems = g_list_prepend(menuitems,((char *)str_value));
    }
  }
  va_end(ap);
  do {
    if (menuitems != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"menuitems != NULL");
      return 0;
    };
  }while (0);
  menuitems = g_list_reverse(menuitems);
  dropdown = pidgin_prefs_dropdown_from_list(box,title,type,key,menuitems);
  g_list_free(menuitems);
  return dropdown;
}

static void delete_prefs(GtkWidget *asdf,void *gdsa)
{
/* Close any "select sound" request dialogs */
  purple_request_close_with_handle(prefs);
/* Unregister callbacks. */
  purple_prefs_disconnect_by_handle(prefs);
/* NULL-ify globals */
  sound_entry = ((GtkWidget *)((void *)0));
  sound_row_sel = 0;
  prefs_sound_themes_loading = 0;
  prefs_sound_themes_combo_box = ((GtkWidget *)((void *)0));
  prefs_blist_themes_combo_box = ((GtkWidget *)((void *)0));
  prefs_status_themes_combo_box = ((GtkWidget *)((void *)0));
  prefs_smiley_themes_combo_box = ((GtkWidget *)((void *)0));
  sample_imhtml = ((GtkWidget *)((void *)0));
  notebook_page = 0;
  prefsnotebook = ((GtkWidget *)((void *)0));
  prefs = ((GtkWidget *)((void *)0));
}

static gchar *get_theme_markup(const char *name,gboolean custom,const char *author,const char *description)
{
  return g_strdup_printf("<b>%s</b>%s%s%s%s\n<span foreground=\'dim grey\'>%s</span>",name,((custom != 0)?" " : ""),((custom != 0)?((const char *)(dgettext("pidgin","(Custom)"))) : ""),((author != ((const char *)((void *)0)))?" - " : ""),((author != ((const char *)((void *)0)))?author : ""),((description != ((const char *)((void *)0)))?description : ""));
}

static void smileys_refresh_theme_list()
{
  GdkPixbuf *pixbuf;
  GSList *themes;
  GtkTreeIter iter;
  pidgin_themes_smiley_theme_probe();
  if (!((themes = smiley_themes) != 0)) 
    return ;
  while(themes != 0){
    struct smiley_theme *theme = (themes -> data);
    char *description = get_theme_markup(((const char *)(dgettext("pidgin",(theme -> name)))),0,((const char *)(dgettext("pidgin",(theme -> author)))),((const char *)(dgettext("pidgin",(theme -> desc)))));
    gtk_list_store_append(prefs_smiley_themes,&iter);
/*
		 * LEAK - Gentoo memprof thinks pixbuf is leaking here... but it
		 * looks like it should be ok to me.  Anyone know what's up?  --Mark
		 */
    pixbuf = (((theme -> icon) != 0)?pidgin_pixbuf_new_from_file((theme -> icon)) : ((struct _GdkPixbuf *)((void *)0)));
    gtk_list_store_set(prefs_smiley_themes,&iter,0,pixbuf,1,description,2,(theme -> name),-1);
    if (pixbuf != ((GdkPixbuf *)((void *)0))) 
      g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
    g_free(description);
    themes = (themes -> next);
  }
}
/* Rebuild the markup for the sound theme selection for "(Custom)" themes */

static void pref_sound_generate_markup()
{
  gboolean print_custom;
  gboolean customized;
  const gchar *author;
  const gchar *description;
  const gchar *current_theme;
  gchar *name;
  gchar *markup;
  PurpleSoundTheme *theme;
  GtkTreeIter iter;
  customized = pidgin_sound_is_customized();
  current_theme = purple_prefs_get_string("/pidgin/sound/theme");
  if (gtk_tree_model_get_iter_first(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)prefs_sound_themes),gtk_tree_model_get_type()))),&iter) != 0) {
    do {
      gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)prefs_sound_themes),gtk_tree_model_get_type()))),&iter,2,&name,-1);
      print_custom = (((customized != 0) && (name != 0)) && (g_str_equal(current_theme,name) != 0));
      if (!(name != 0) || (( *name) == 0)) {
        g_free(name);
        name = g_strdup(((const char *)(dgettext("pidgin","Default"))));
        author = ((const char *)(dgettext("pidgin","Penguin Pimps")));
        description = ((const char *)(dgettext("pidgin","The default Pidgin sound theme")));
      }
      else {
        theme = ((PurpleSoundTheme *)(g_type_check_instance_cast(((GTypeInstance *)(purple_theme_manager_find_theme(name,"sound"))),purple_sound_theme_get_type())));
        author = purple_theme_get_author(((PurpleTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),purple_theme_get_type()))));
        description = purple_theme_get_description(((PurpleTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),purple_theme_get_type()))));
      }
      markup = get_theme_markup(name,print_custom,author,description);
      gtk_list_store_set(prefs_sound_themes,&iter,1,markup,-1);
      g_free(name);
      g_free(markup);
    }while (gtk_tree_model_iter_next(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)prefs_sound_themes),gtk_tree_model_get_type()))),&iter) != 0);
  }
}
/* adds the themes to the theme list from the manager so they can be displayed in prefs */

static void prefs_themes_sort(PurpleTheme *theme)
{
  GdkPixbuf *pixbuf = (GdkPixbuf *)((void *)0);
  GtkTreeIter iter;
  gchar *image_full = (gchar *)((void *)0);
  gchar *markup;
  const gchar *name;
  const gchar *author;
  const gchar *description;
  if ((({
    GTypeInstance *__inst = (GTypeInstance *)theme;
    GType __t = purple_sound_theme_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
{
    image_full = purple_theme_get_image_full(theme);
    if (image_full != ((gchar *)((void *)0))) {
      pixbuf = pidgin_pixbuf_new_from_file_at_scale(image_full,32,32,(!0));
      g_free(image_full);
    }
    else 
      pixbuf = ((GdkPixbuf *)((void *)0));
    gtk_list_store_append(prefs_sound_themes,&iter);
    gtk_list_store_set(prefs_sound_themes,&iter,0,pixbuf,2,purple_theme_get_name(theme),-1);
    if (pixbuf != ((GdkPixbuf *)((void *)0))) 
      g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
  }
  else if (((({
    GTypeInstance *__inst = (GTypeInstance *)theme;
    GType __t = pidgin_blist_theme_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) || (((
{
    GTypeInstance *__inst = (GTypeInstance *)theme;
    GType __t = pidgin_status_icon_theme_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) 
{
    GtkListStore *store;
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
      store = prefs_blist_themes;
    else 
      store = prefs_status_icon_themes;
    image_full = purple_theme_get_image_full(theme);
    if (image_full != ((gchar *)((void *)0))) {
      pixbuf = pidgin_pixbuf_new_from_file_at_scale(image_full,32,32,(!0));
      g_free(image_full);
    }
    else 
      pixbuf = ((GdkPixbuf *)((void *)0));
    name = purple_theme_get_name(theme);
    author = purple_theme_get_author(theme);
    description = purple_theme_get_description(theme);
    markup = get_theme_markup(name,0,author,description);
    gtk_list_store_append(store,&iter);
    gtk_list_store_set(store,&iter,0,pixbuf,1,markup,2,name,-1);
    g_free(markup);
    if (pixbuf != ((GdkPixbuf *)((void *)0))) 
      g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
  }
}

static void prefs_set_active_theme_combo(GtkWidget *combo_box,GtkListStore *store,const gchar *current_theme)
{
  GtkTreeIter iter;
  gchar *theme = (gchar *)((void *)0);
  gboolean unset = (!0);
  if (((current_theme != 0) && (( *current_theme) != 0)) && (gtk_tree_model_get_iter_first(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)store),gtk_tree_model_get_type()))),&iter) != 0)) {
    do {
      gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)store),gtk_tree_model_get_type()))),&iter,2,&theme,-1);
      if (g_str_equal(current_theme,theme) != 0) {
        gtk_combo_box_set_active_iter(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo_box),gtk_combo_box_get_type()))),&iter);
        unset = 0;
      }
      g_free(theme);
    }while (gtk_tree_model_iter_next(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)store),gtk_tree_model_get_type()))),&iter) != 0);
  }
  if (unset != 0) 
    gtk_combo_box_set_active(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo_box),gtk_combo_box_get_type()))),0);
}

static void prefs_themes_refresh()
{
  GdkPixbuf *pixbuf = (GdkPixbuf *)((void *)0);
  gchar *tmp;
  GtkTreeIter iter;
  prefs_sound_themes_loading = (!0);
/* refresh the list of themes in the manager */
  purple_theme_manager_refresh();
  tmp = g_build_filename("/usr/local/share","icons","hicolor","32x32","apps","pidgin.png",((void *)((void *)0)));
  pixbuf = pidgin_pixbuf_new_from_file_at_scale(tmp,32,32,(!0));
  g_free(tmp);
/* sound themes */
  gtk_list_store_clear(prefs_sound_themes);
  gtk_list_store_append(prefs_sound_themes,&iter);
  gtk_list_store_set(prefs_sound_themes,&iter,0,pixbuf,2,"",-1);
/* blist themes */
  gtk_list_store_clear(prefs_blist_themes);
  gtk_list_store_append(prefs_blist_themes,&iter);
  tmp = get_theme_markup(((const char *)(dgettext("pidgin","Default"))),0,((const char *)(dgettext("pidgin","Penguin Pimps"))),((const char *)(dgettext("pidgin","The default Pidgin buddy list theme"))));
  gtk_list_store_set(prefs_blist_themes,&iter,0,pixbuf,1,tmp,2,"",-1);
  g_free(tmp);
/* status icon themes */
  gtk_list_store_clear(prefs_status_icon_themes);
  gtk_list_store_append(prefs_status_icon_themes,&iter);
  tmp = get_theme_markup(((const char *)(dgettext("pidgin","Default"))),0,((const char *)(dgettext("pidgin","Penguin Pimps"))),((const char *)(dgettext("pidgin","The default Pidgin status icon theme"))));
  gtk_list_store_set(prefs_status_icon_themes,&iter,0,pixbuf,1,tmp,2,"",-1);
  g_free(tmp);
  if (pixbuf != 0) 
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
/* smiley themes */
  gtk_list_store_clear(prefs_smiley_themes);
  purple_theme_manager_for_each_theme(prefs_themes_sort);
  pref_sound_generate_markup();
  smileys_refresh_theme_list();
/* set active */
  prefs_set_active_theme_combo(prefs_sound_themes_combo_box,prefs_sound_themes,purple_prefs_get_string("/pidgin/sound/theme"));
  prefs_set_active_theme_combo(prefs_blist_themes_combo_box,prefs_blist_themes,purple_prefs_get_string("/pidgin/blist/theme"));
  prefs_set_active_theme_combo(prefs_status_themes_combo_box,prefs_status_icon_themes,purple_prefs_get_string("/pidgin/status/icon-theme"));
  prefs_set_active_theme_combo(prefs_smiley_themes_combo_box,prefs_smiley_themes,purple_prefs_get_string("/pidgin/smileys/theme"));
  prefs_sound_themes_loading = 0;
}
/* init all the theme variables so that the themes can be sorted later and used by pref pages */

static void prefs_themes_init()
{
  prefs_sound_themes = gtk_list_store_new(3,gdk_pixbuf_get_type(),((GType )(16 << 2)),((GType )(16 << 2)));
  prefs_blist_themes = gtk_list_store_new(3,gdk_pixbuf_get_type(),((GType )(16 << 2)),((GType )(16 << 2)));
  prefs_status_icon_themes = gtk_list_store_new(3,gdk_pixbuf_get_type(),((GType )(16 << 2)),((GType )(16 << 2)));
  prefs_smiley_themes = gtk_list_store_new(3,gdk_pixbuf_get_type(),((GType )(16 << 2)),((GType )(16 << 2)));
}

static PurpleTheme *prefs_theme_find_theme(const gchar *path,const gchar *type)
{
  PurpleTheme *theme = purple_theme_manager_load_theme(path,type);
  GDir *dir = g_dir_open(path,0,0);
  const gchar *next;
  while(!((({
    GTypeInstance *__inst = (GTypeInstance *)theme;
    GType __t = purple_theme_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) && ((next = g_dir_read_name(dir)) != 0))
{
    gchar *next_path = g_build_filename(path,next,((void *)((void *)0)));
    if (g_file_test(next_path,G_FILE_TEST_IS_DIR) != 0) 
      theme = prefs_theme_find_theme(next_path,type);
    g_free(next_path);
  }
  g_dir_close(dir);
  return theme;
}
/* Eww. Seriously ewww. But thanks, grim! This is taken from guifications2 */

static gboolean purple_theme_file_copy(const gchar *source,const gchar *destination)
{
  FILE *src;
  FILE *dest;
  gint chr = (-1);
  if (!((src = fopen(source,"rb")) != 0)) 
    return 0;
  if (!((dest = fopen(destination,"wb")) != 0)) {
    fclose(src);
    return 0;
  }
  while((chr = fgetc(src)) != -1){
    fputc(chr,dest);
  }
  fclose(dest);
  fclose(src);
  return (!0);
}

static void free_theme_info(struct theme_info *info)
{
  if (info != ((struct theme_info *)((void *)0))) {
    g_free((info -> type));
    g_free((info -> extension));
    g_free((info -> original_name));
    g_free(info);
  }
}
/* installs a theme, info is freed by function */

static void theme_install_theme(char *path,struct theme_info *info)
{
#ifndef _WIN32
  gchar *command;
#endif
  gchar *destdir;
  const char *tail;
  gboolean is_smiley_theme;
  gboolean is_archive;
  PurpleTheme *theme = (PurpleTheme *)((void *)0);
  if (info == ((struct theme_info *)((void *)0))) 
    return ;
/* check the extension */
  tail = ((((info -> extension) != 0)?(info -> extension) : strrchr(path,'.')));
  if (!(tail != 0)) {
    free_theme_info(info);
    return ;
  }
  is_archive = (!(g_ascii_strcasecmp(tail,".gz") != 0) || !(g_ascii_strcasecmp(tail,".tgz") != 0));
/* Just to be safe */
  g_strchomp(path);
  if ((is_smiley_theme = g_str_equal((info -> type),"smiley")) != 0) 
    destdir = g_build_filename(purple_user_dir(),"smileys",((void *)((void *)0)));
  else 
    destdir = g_build_filename(purple_user_dir(),"themes","temp",((void *)((void *)0)));
/* We'll check this just to make sure. This also lets us do something different on
	 * other platforms, if need be */
  if (is_archive != 0) {
#ifndef _WIN32
    gchar *path_escaped = g_shell_quote(path);
    gchar *destdir_escaped = g_shell_quote(destdir);
    if (!(g_file_test(destdir,G_FILE_TEST_IS_DIR) != 0)) 
      purple_build_dir(destdir,256 | 128 | 64);
    command = g_strdup_printf("tar > /dev/null xzf %s -C %s",path_escaped,destdir_escaped);
    g_free(path_escaped);
    g_free(destdir_escaped);
/* Fire! */
    if (system(command) != 0) {
      purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Theme failed to unpack."))),0,0,0);
      g_free(command);
      g_free(destdir);
      free_theme_info(info);
      return ;
    }
#else
#endif
  }
  if (is_smiley_theme != 0) {
/* just extract the folder to the smiley directory */
    prefs_themes_refresh();
  }
  else if (is_archive != 0) {
    theme = prefs_theme_find_theme(destdir,(info -> type));
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = purple_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
/* create the location for the theme */
      gchar *theme_dest = g_build_filename(purple_user_dir(),"themes",purple_theme_get_name(theme),"purple",(info -> type),((void *)((void *)0)));
      if (!(g_file_test(theme_dest,G_FILE_TEST_IS_DIR) != 0)) 
        purple_build_dir(theme_dest,256 | 128 | 64);
      g_free(theme_dest);
      theme_dest = g_build_filename(purple_user_dir(),"themes",purple_theme_get_name(theme),"purple",(info -> type),((void *)((void *)0)));
/* move the entire directory to new location */
      rename(purple_theme_get_dir(theme),theme_dest);
      g_free(theme_dest);
      remove(destdir);
      g_object_unref(theme);
      prefs_themes_refresh();
    }
    else {
/* something was wrong with the theme archive */
      g_unlink(destdir);
      purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Theme failed to load."))),0,0,0);
    }
/* just a single file so copy it to a new temp directory and attempt to load it*/
  }
  else {
    gchar *temp_path;
    gchar *temp_file;
    temp_path = g_build_filename(purple_user_dir(),"themes","temp","sub_folder",((void *)((void *)0)));
    if ((info -> original_name) != ((gchar *)((void *)0))) {
/* name was changed from the original (probably a dnd) change it back before loading */
      temp_file = g_build_filename(temp_path,(info -> original_name),((void *)((void *)0)));
    }
    else {
      gchar *source_name = g_path_get_basename(path);
      temp_file = g_build_filename(temp_path,source_name,((void *)((void *)0)));
      g_free(source_name);
    }
    if (!(g_file_test(temp_path,G_FILE_TEST_IS_DIR) != 0)) 
      purple_build_dir(temp_path,256 | 128 | 64);
    if (purple_theme_file_copy(path,temp_file) != 0) {
/* find the theme, could be in subfolder */
      theme = prefs_theme_find_theme(temp_path,(info -> type));
      if ((({
        GTypeInstance *__inst = (GTypeInstance *)theme;
        GType __t = purple_theme_get_type();
        gboolean __r;
        if (!(__inst != 0)) 
          __r = 0;
        else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
          __r = (!0);
        else 
          __r = g_type_check_instance_is_a(__inst,__t);
        __r;
      })) != 0) 
{
        gchar *theme_dest = g_build_filename(purple_user_dir(),"themes",purple_theme_get_name(theme),"purple",(info -> type),((void *)((void *)0)));
        if (!(g_file_test(theme_dest,G_FILE_TEST_IS_DIR) != 0)) 
          purple_build_dir(theme_dest,256 | 128 | 64);
        rename(purple_theme_get_dir(theme),theme_dest);
        g_free(theme_dest);
        g_object_unref(theme);
        prefs_themes_refresh();
      }
      else {
        remove(temp_path);
        purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Theme failed to load."))),0,0,0);
      }
    }
    else {
      purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Theme failed to copy."))),0,0,0);
    }
    g_free(temp_file);
    g_free(temp_path);
  }
  g_free(destdir);
  free_theme_info(info);
}

static void theme_got_url(PurpleUtilFetchUrlData *url_data,gpointer user_data,const gchar *themedata,size_t len,const gchar *error_message)
{
  FILE *f;
  gchar *path;
  size_t wc;
  if ((error_message != ((const gchar *)((void *)0))) || (len == 0)) {
    free_theme_info(user_data);
    return ;
  }
  f = purple_mkstemp(&path,(!0));
  wc = fwrite(themedata,len,1,f);
  if (wc != 1) {
    purple_debug_warning("theme_got_url","Unable to write theme data.\n");
    fclose(f);
    g_unlink(path);
    g_free(path);
    free_theme_info(user_data);
    return ;
  }
  fclose(f);
  theme_install_theme(path,user_data);
  g_unlink(path);
  g_free(path);
}

static void theme_dnd_recv(GtkWidget *widget,GdkDragContext *dc,guint x,guint y,GtkSelectionData *sd,guint info,guint t,gpointer user_data)
{
  gchar *name = g_strchomp(((gchar *)(sd -> data)));
  if (((sd -> length) >= 0) && ((sd -> format) == 8)) {
/* Well, it looks like the drag event was cool.
		 * Let's do something with it */
    gchar *temp;
    struct theme_info *info = (struct theme_info *)(g_malloc0_n(1,(sizeof(struct theme_info ))));
    info -> type = g_strdup(((gchar *)user_data));
    info -> extension = g_strdup((g_strrstr(name,".")));
    temp = g_strrstr(name,"/");
    info -> original_name = ((temp != 0)?g_strdup((++temp)) : ((char *)((void *)0)));
    if (!(g_ascii_strncasecmp(name,"file://",7) != 0)) {
      GError *converr = (GError *)((void *)0);
      gchar *tmp;
/* It looks like we're dealing with a local file. Let's
			 * just untar it in the right place */
      if (!((tmp = g_filename_from_uri(name,0,&converr)) != 0)) {
        purple_debug(PURPLE_DEBUG_ERROR,"theme dnd","%s\n",((converr != 0)?(converr -> message) : "g_filename_from_uri error"));
        free_theme_info(info);
        return ;
      }
      theme_install_theme(tmp,info);
      g_free(tmp);
    }
    else if (!(g_ascii_strncasecmp(name,"http://",7) != 0)) {
/* Oo, a web drag and drop. This is where things
			 * will start to get interesting */
      purple_util_fetch_url_request(name,(!0),0,0,0,0,theme_got_url,info);;
    }
    else if (!(g_ascii_strncasecmp(name,"https://",8) != 0)) {
/* purple_util_fetch_url() doesn't support HTTPS, but we want users
			 * to be able to drag and drop links from the SF trackers, so
			 * we'll try it as an HTTP URL. */
      char *tmp = g_strdup((name + 1));
      tmp[0] = 'h';
      tmp[1] = 't';
      tmp[2] = 't';
      tmp[3] = 'p';
      purple_util_fetch_url_request(tmp,(!0),0,0,0,0,theme_got_url,info);;
      g_free(tmp);
    }
    else 
      free_theme_info(info);
    gtk_drag_finish(dc,(!0),0,t);
  }
  gtk_drag_finish(dc,0,0,t);
}
/* builds a theme combo box from a list store with colums: icon preview, markup, theme name */

static GtkWidget *prefs_build_theme_combo_box(GtkListStore *store,const char *current_theme,const char *type)
{
  GtkCellRenderer *cell_rend;
  GtkWidget *combo_box;
  GtkTargetEntry te[3UL] = {{("text/plain"), (0), (0)}, {("text/uri-list"), (0), (1)}, {("STRING"), (0), (2)}};
  do {
    if ((store != ((GtkListStore *)((void *)0))) && (current_theme != ((const char *)((void *)0)))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"store != NULL && current_theme != NULL");
      return 0;
    };
  }while (0);
  combo_box = gtk_combo_box_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)store),gtk_tree_model_get_type()))));
  cell_rend = gtk_cell_renderer_pixbuf_new();
  gtk_cell_renderer_set_fixed_size(cell_rend,32,32);
  gtk_cell_layout_pack_start(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)combo_box),gtk_cell_layout_get_type()))),cell_rend,0);
  gtk_cell_layout_set_attributes(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)combo_box),gtk_cell_layout_get_type()))),cell_rend,"pixbuf",0,((void *)((void *)0)));
  cell_rend = gtk_cell_renderer_text_new();
  gtk_cell_layout_pack_start(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)combo_box),gtk_cell_layout_get_type()))),cell_rend,(!0));
  gtk_cell_layout_set_attributes(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)combo_box),gtk_cell_layout_get_type()))),cell_rend,"markup",1,((void *)((void *)0)));
  g_object_set(cell_rend,"ellipsize",PANGO_ELLIPSIZE_END,((void *)((void *)0)));
  gtk_drag_dest_set(combo_box,(GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_HIGHLIGHT | GTK_DEST_DEFAULT_DROP),te,(sizeof(te) / sizeof(GtkTargetEntry )),(GDK_ACTION_COPY | GDK_ACTION_MOVE));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)combo_box),((GType )(20 << 2))))),"drag_data_received",((GCallback )theme_dnd_recv),((gpointer )type),0,((GConnectFlags )0));
  return combo_box;
}
/* sets the current sound theme */

static void prefs_set_sound_theme_cb(GtkComboBox *combo_box,gpointer user_data)
{
  gint i;
  gchar *pref;
  gchar *new_theme;
  GtkTreeIter new_iter;
  if ((gtk_combo_box_get_active_iter(combo_box,&new_iter) != 0) && !(prefs_sound_themes_loading != 0)) {
    gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)prefs_sound_themes),gtk_tree_model_get_type()))),&new_iter,2,&new_theme,-1);
    purple_prefs_set_string("/pidgin/sound/theme",new_theme);
/* New theme removes all customization */
    for (i = 0; i < PURPLE_NUM_SOUNDS; i++) {
      pref = g_strdup_printf("/pidgin/sound/file/%s",pidgin_sound_get_event_option(i));
      purple_prefs_set_path(pref,"");
      g_free(pref);
    }
/* gets rid of the "(Custom)" from the last selection */
    pref_sound_generate_markup();
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)sound_entry),gtk_entry_get_type()))),((const char *)(dgettext("pidgin","(default)"))));
    g_free(new_theme);
  }
}
/* sets the current smiley theme */

static void prefs_set_smiley_theme_cb(GtkComboBox *combo_box,gpointer user_data)
{
  gchar *new_theme;
  GtkTreeIter new_iter;
  if (gtk_combo_box_get_active_iter(combo_box,&new_iter) != 0) {
    gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)prefs_smiley_themes),gtk_tree_model_get_type()))),&new_iter,2,&new_theme,-1);
    purple_prefs_set_string("/pidgin/smileys/theme",new_theme);
    pidgin_themes_smiley_themeize(sample_imhtml);
    g_free(new_theme);
  }
}
/* Does same as normal sort, except "none" is sorted first */

static gint pidgin_sort_smileys(GtkTreeModel *model,GtkTreeIter *a,GtkTreeIter *b,gpointer userdata)
{
  gint ret = 0;
  gchar *name1 = (gchar *)((void *)0);
  gchar *name2 = (gchar *)((void *)0);
  gtk_tree_model_get(model,a,2,&name1,-1);
  gtk_tree_model_get(model,b,2,&name2,-1);
  if ((name1 == ((gchar *)((void *)0))) || (name2 == ((gchar *)((void *)0)))) {
    if (!((name1 == ((gchar *)((void *)0))) && (name2 == ((gchar *)((void *)0))))) 
      ret = ((name1 == ((gchar *)((void *)0)))?-1 : 1);
  }
  else if (!(g_ascii_strcasecmp(name1,"none") != 0)) {
    if (!(g_utf8_collate(name1,name2) != 0)) 
      ret = 0;
    else 
/* Sort name1 first */
      ret = (-1);
  }
  else if (!(g_ascii_strcasecmp(name2,"none") != 0)) {
/* Sort name2 first */
    ret = 1;
  }
  else {
/* Neither string is "none", default to normal sort */
    ret = purple_utf8_strcasecmp(name1,name2);
  }
  g_free(name1);
  g_free(name2);
  return ret;
}
/* sets the current buddy list theme */

static void prefs_set_blist_theme_cb(GtkComboBox *combo_box,gpointer user_data)
{
  PidginBlistTheme *theme = (PidginBlistTheme *)((void *)0);
  GtkTreeIter iter;
  gchar *name = (gchar *)((void *)0);
  if (gtk_combo_box_get_active_iter(combo_box,&iter) != 0) {
    gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)prefs_blist_themes),gtk_tree_model_get_type()))),&iter,2,&name,-1);
    if (!(name != 0) || !(g_str_equal(name,"") != 0)) 
      theme = ((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)(purple_theme_manager_find_theme(name,"blist"))),pidgin_blist_theme_get_type())));
    g_free(name);
    pidgin_blist_set_theme(theme);
  }
}
/* sets the current icon theme */

static void prefs_set_status_icon_theme_cb(GtkComboBox *combo_box,gpointer user_data)
{
  PidginStatusIconTheme *theme = (PidginStatusIconTheme *)((void *)0);
  GtkTreeIter iter;
  gchar *name = (gchar *)((void *)0);
  if (gtk_combo_box_get_active_iter(combo_box,&iter) != 0) {
    gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)prefs_status_icon_themes),gtk_tree_model_get_type()))),&iter,2,&name,-1);
    if (!(name != 0) || !(g_str_equal(name,"") != 0)) 
      theme = ((PidginStatusIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)(purple_theme_manager_find_theme(name,"status-icon"))),pidgin_status_icon_theme_get_type())));
    g_free(name);
    pidgin_stock_load_status_icon_theme(theme);
    pidgin_blist_refresh(purple_get_blist());
  }
}

static GtkWidget *add_theme_prefs_combo(GtkWidget *vbox,GtkSizeGroup *combo_sg,GtkSizeGroup *label_sg,GtkListStore *theme_store,GCallback combo_box_cb,gpointer combo_box_cb_user_data,const char *label_str,const char *prefs_path,const char *theme_type)
{
  GtkWidget *label;
  GtkWidget *combo_box = (GtkWidget *)((void *)0);
  GtkWidget *themesel_hbox = gtk_hbox_new(0,6);
  label = gtk_label_new(label_str);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_size_group_add_widget(label_sg,label);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)themesel_hbox),gtk_box_get_type()))),label,0,0,0);
  combo_box = prefs_build_theme_combo_box(theme_store,purple_prefs_get_string(prefs_path),theme_type);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)combo_box),((GType )(20 << 2))))),"changed",((GCallback )combo_box_cb),combo_box_cb_user_data,0,((GConnectFlags )0));
  gtk_size_group_add_widget(combo_sg,combo_box);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)themesel_hbox),gtk_box_get_type()))),combo_box,(!0),(!0),0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),themesel_hbox,0,0,0);
  return combo_box;
}

static GtkWidget *theme_page()
{
  GtkWidget *label;
  GtkWidget *ret;
  GtkWidget *vbox;
  GtkSizeGroup *label_sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  GtkSizeGroup *combo_sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  ret = gtk_vbox_new(0,18);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_container_get_type()))),12);
  vbox = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Theme Selections"))));
/* Instructions */
  label = gtk_label_new(((const char *)(dgettext("pidgin","Select a theme that you would like to use from the lists below.\nNew themes can be installed by dragging and dropping them onto the theme list."))));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_label_set_justify(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),GTK_JUSTIFY_LEFT);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),label,(!0),0,0);
  gtk_widget_show(label);
/* Buddy List Themes */
  prefs_blist_themes_combo_box = add_theme_prefs_combo(vbox,combo_sg,label_sg,prefs_blist_themes,((GCallback )prefs_set_blist_theme_cb),0,((const char *)(dgettext("pidgin","Buddy List Theme:"))),"/pidgin/blist/theme","blist");
/* Status Icon Themes */
  prefs_status_themes_combo_box = add_theme_prefs_combo(vbox,combo_sg,label_sg,prefs_status_icon_themes,((GCallback )prefs_set_status_icon_theme_cb),0,((const char *)(dgettext("pidgin","Status Icon Theme:"))),"/pidgin/status/icon-theme","icon");
/* Sound Themes */
  prefs_sound_themes_combo_box = add_theme_prefs_combo(vbox,combo_sg,label_sg,prefs_sound_themes,((GCallback )prefs_set_sound_theme_cb),0,((const char *)(dgettext("pidgin","Sound Theme:"))),"/pidgin/sound/theme","sound");
/* Smiley Themes */
  prefs_smiley_themes_combo_box = add_theme_prefs_combo(vbox,combo_sg,label_sg,prefs_smiley_themes,((GCallback )prefs_set_smiley_theme_cb),0,((const char *)(dgettext("pidgin","Smiley Theme:"))),"/pidgin/smileys/theme","smiley");
/* Custom sort so "none" theme is at top of list */
  gtk_tree_sortable_set_sort_func(((GtkTreeSortable *)(g_type_check_instance_cast(((GTypeInstance *)prefs_smiley_themes),gtk_tree_sortable_get_type()))),2,pidgin_sort_smileys,0,0);
  gtk_tree_sortable_set_sort_column_id(((GtkTreeSortable *)(g_type_check_instance_cast(((GTypeInstance *)prefs_smiley_themes),gtk_tree_sortable_get_type()))),2,GTK_SORT_ASCENDING);
  gtk_widget_show_all(ret);
  return ret;
}

static void formatting_toggle_cb(GtkIMHtml *imhtml,GtkIMHtmlButtons buttons,void *toolbar)
{
  gboolean bold;
  gboolean italic;
  gboolean uline;
  gtk_imhtml_get_current_format(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),&bold,&italic,&uline);
  if ((buttons & GTK_IMHTML_BOLD) != 0) 
    purple_prefs_set_bool("/pidgin/conversations/send_bold",bold);
  if ((buttons & GTK_IMHTML_ITALIC) != 0) 
    purple_prefs_set_bool("/pidgin/conversations/send_italic",italic);
  if ((buttons & GTK_IMHTML_UNDERLINE) != 0) 
    purple_prefs_set_bool("/pidgin/conversations/send_underline",uline);
  if (((buttons & GTK_IMHTML_GROW) != 0) || ((buttons & GTK_IMHTML_SHRINK) != 0)) 
    purple_prefs_set_int("/pidgin/conversations/font_size",gtk_imhtml_get_current_fontsize(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))));
  if ((buttons & GTK_IMHTML_FACE) != 0) {
    char *face = gtk_imhtml_get_current_fontface(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))));
    if (!(face != 0)) 
      face = g_strdup("");
    purple_prefs_set_string("/pidgin/conversations/font_face",face);
    g_free(face);
  }
  if ((buttons & GTK_IMHTML_FORECOLOR) != 0) {
    char *color = gtk_imhtml_get_current_forecolor(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))));
    if (!(color != 0)) 
      color = g_strdup("");
    purple_prefs_set_string("/pidgin/conversations/fgcolor",color);
    g_free(color);
  }
  if ((buttons & GTK_IMHTML_BACKCOLOR) != 0) {
    char *color;
    GObject *object;
    color = gtk_imhtml_get_current_backcolor(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))));
    if (!(color != 0)) 
      color = g_strdup("");
/* Block the signal to prevent a loop. */
    object = (g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2)))))));
    g_signal_handlers_block_matched(object,G_SIGNAL_MATCH_DATA,0,0,0,0,toolbar);
/* Clear the backcolor. */
    gtk_imhtml_toggle_backcolor(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),"");
/* Unblock the signal. */
    g_signal_handlers_unblock_matched(object,G_SIGNAL_MATCH_DATA,0,0,0,0,toolbar);
    g_object_unref(object);
/* This will fire a toggle signal and get saved below. */
    gtk_imhtml_toggle_background(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),color);
    g_free(color);
  }
  if ((buttons & GTK_IMHTML_BACKGROUND) != 0) {
    char *color = gtk_imhtml_get_current_background(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))));
    if (!(color != 0)) 
      color = g_strdup("");
    purple_prefs_set_string("/pidgin/conversations/bgcolor",color);
    g_free(color);
  }
}

static void formatting_clear_cb(GtkIMHtml *imhtml,void *data)
{
  purple_prefs_set_bool("/pidgin/conversations/send_bold",0);
  purple_prefs_set_bool("/pidgin/conversations/send_italic",0);
  purple_prefs_set_bool("/pidgin/conversations/send_underline",0);
  purple_prefs_set_int("/pidgin/conversations/font_size",3);
  purple_prefs_set_string("/pidgin/conversations/font_face","");
  purple_prefs_set_string("/pidgin/conversations/fgcolor","");
  purple_prefs_set_string("/pidgin/conversations/bgcolor","");
}

static void conversation_usetabs_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  gboolean usetabs = (gint )((glong )value);
  if (usetabs != 0) 
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)data),gtk_widget_get_type()))),(!0));
  else 
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)data),gtk_widget_get_type()))),0);
}
#define CONVERSATION_CLOSE_ACCEL_PATH "<main>/Conversation/Close"
/* Filled in in keyboard_shortcuts(). */
static GtkAccelKey ctrl_w = {(0), (0), (0)};
static GtkAccelKey escape = {(0), (0), (0)};
static guint escape_closes_conversation_cb_id = 0;

static gboolean accel_is_escape(GtkAccelKey *k)
{
  return ((k -> accel_key) == escape.accel_key) && ((k -> accel_mods) == escape.accel_mods);
}
/* Update the tickybox in Preferences when the keybinding for Conversation ->
 * Close is changed via Gtk.
 */

static void conversation_close_accel_changed_cb(GtkAccelMap *object,gchar *accel_path,guint accel_key,GdkModifierType accel_mods,gpointer checkbox_)
{
  GtkToggleButton *checkbox = (GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)checkbox_),gtk_toggle_button_get_type()));
  GtkAccelKey new = {(accel_key), (accel_mods), (0)};
  g_signal_handler_block(checkbox,escape_closes_conversation_cb_id);
  gtk_toggle_button_set_active(checkbox,accel_is_escape(&new));
  g_signal_handler_unblock(checkbox,escape_closes_conversation_cb_id);
}

static void escape_closes_conversation_cb(GtkWidget *w,gpointer unused)
{
  gboolean active = gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)w),gtk_toggle_button_get_type()))));
  gboolean changed;
  GtkAccelKey *new_key = (active != 0)?(&escape) : (&ctrl_w);
  changed = gtk_accel_map_change_entry("<main>/Conversation/Close",(new_key -> accel_key),(new_key -> accel_mods),(!0));
/* If another path is already bound to the new accelerator,
	 * _change_entry tries to delete that binding (because it was passed
	 * replace=TRUE).  If that other path is locked, then _change_entry
	 * will fail.  We don't ever lock any accelerator paths, so this case
	 * should never arise.
	 */
  if (!(changed != 0)) 
    purple_debug_warning("gtkprefs","Escape accel failed to change\n");
}
/* Creates preferences for keyboard shortcuts that it's hard to change with the
 * standard Gtk accelerator-changing mechanism.
 */

static void keyboard_shortcuts(GtkWidget *page)
{
  GtkWidget *vbox = pidgin_make_frame(page,((const char *)(dgettext("pidgin","Keyboard Shortcuts"))));
  GtkWidget *checkbox;
  GtkAccelKey current = {(0), (0), (0)};
  GtkAccelMap *map = gtk_accel_map_get();
/* Maybe it would be better just to hardcode the values?
	 * -- resiak, 2007-04-30
	 */
  if (ctrl_w.accel_key == 0) {
    gtk_accelerator_parse("<Control>w",&ctrl_w.accel_key,&ctrl_w.accel_mods);
    do {
      if (ctrl_w.accel_key != 0) ;
      else 
        g_assertion_message_expr(0,"gtkprefs.c",1305,((const char *)__func__),"ctrl_w.accel_key != 0");
    }while (0);
    gtk_accelerator_parse("Escape",&escape.accel_key,&escape.accel_mods);
    do {
      if (escape.accel_key != 0) ;
      else 
        g_assertion_message_expr(0,"gtkprefs.c",1309,((const char *)__func__),"escape.accel_key != 0");
    }while (0);
  }
  checkbox = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Cl_ose conversations with the Escape key"))));
  gtk_accel_map_lookup_entry("<main>/Conversation/Close",&current);
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)checkbox),gtk_toggle_button_get_type()))),accel_is_escape(&current));
  escape_closes_conversation_cb_id = (g_signal_connect_data(checkbox,"clicked",((GCallback )escape_closes_conversation_cb),0,0,((GConnectFlags )0)));
  g_signal_connect_object(map,"changed::<main>/Conversation/Close",((GCallback )conversation_close_accel_changed_cb),checkbox,((GConnectFlags )0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),checkbox,0,0,0);
}

static GtkWidget *interface_page()
{
  GtkWidget *ret;
  GtkWidget *vbox;
  GtkWidget *vbox2;
  GtkWidget *label;
  GtkSizeGroup *sg;
  GList *names = (GList *)((void *)0);
  ret = gtk_vbox_new(0,18);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_container_get_type()))),12);
  sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
/* System Tray */
  vbox = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","System Tray Icon"))));
  label = pidgin_prefs_dropdown(vbox,((const char *)(dgettext("pidgin","_Show system tray icon:"))),PURPLE_PREF_STRING,"/pidgin/docklet/show",((const char *)(dgettext("pidgin","Always"))),"always",((const char *)(dgettext("pidgin","On unread messages"))),"pending",((const char *)(dgettext("pidgin","Never"))),"never",((void *)((void *)0)));
  gtk_size_group_add_widget(sg,label);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0.0,0.5);
  vbox = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Conversation Window"))));
  label = pidgin_prefs_dropdown(vbox,((const char *)(dgettext("pidgin","_Hide new IM conversations:"))),PURPLE_PREF_STRING,"/pidgin/conversations/im/hide_new",((const char *)(dgettext("pidgin","Never"))),"never",((const char *)(dgettext("pidgin","When away"))),"away",((const char *)(dgettext("pidgin","Always"))),"always",((void *)((void *)0)));
  gtk_size_group_add_widget(sg,label);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0.0,0.5);
#ifdef _WIN32
#endif
/* All the tab options! */
  vbox = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Tabs"))));
  pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","Show IMs and chats in _tabbed windows"))),"/pidgin/conversations/tabs",vbox);
/*
	 * Connect a signal to the above preference.  When conversations are not
	 * shown in a tabbed window then all tabbing options should be disabled.
	 */
  vbox2 = gtk_vbox_new(0,9);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),vbox2,0,0,0);
  purple_prefs_connect_callback(prefs,"/pidgin/conversations/tabs",conversation_usetabs_cb,vbox2);
  if (!(purple_prefs_get_bool("/pidgin/conversations/tabs") != 0)) 
    gtk_widget_set_sensitive(vbox2,0);
  pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","Show close b_utton on tabs"))),"/pidgin/conversations/close_on_tabs",vbox2);
  label = pidgin_prefs_dropdown(vbox2,((const char *)(dgettext("pidgin","_Placement:"))),PURPLE_PREF_INT,"/pidgin/conversations/tab_side",((const char *)(dgettext("pidgin","Top"))),GTK_POS_TOP,((const char *)(dgettext("pidgin","Bottom"))),GTK_POS_BOTTOM,((const char *)(dgettext("pidgin","Left"))),GTK_POS_LEFT,((const char *)(dgettext("pidgin","Right"))),GTK_POS_RIGHT,((const char *)(dgettext("pidgin","Left Vertical"))),GTK_POS_LEFT | 8,((const char *)(dgettext("pidgin","Right Vertical"))),GTK_POS_RIGHT | 8,((void *)((void *)0)));
  gtk_size_group_add_widget(sg,label);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0.0,0.5);
  names = pidgin_conv_placement_get_options();
  label = pidgin_prefs_dropdown_from_list(vbox2,((const char *)(dgettext("pidgin","N_ew conversations:"))),PURPLE_PREF_STRING,"/pidgin/conversations/placement",names);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0.0,0.5);
  gtk_size_group_add_widget(sg,label);
  g_list_free(names);
  keyboard_shortcuts(ret);
  gtk_widget_show_all(ret);
  g_object_unref(sg);
  return ret;
}
#ifdef _WIN32
#endif

static GtkWidget *conv_page()
{
  GtkWidget *ret;
  GtkWidget *vbox;
  GtkWidget *toolbar;
  GtkWidget *iconpref1;
  GtkWidget *iconpref2;
  GtkWidget *imhtml;
  GtkWidget *frame;
  GtkWidget *hbox;
  GtkWidget *checkbox;
  GtkWidget *spin_button;
  ret = gtk_vbox_new(0,18);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_container_get_type()))),12);
  vbox = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Conversations"))));
  pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","Show _formatting on incoming messages"))),"/pidgin/conversations/show_incoming_formatting",vbox);
  pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","Close IMs immediately when the tab is closed"))),"/pidgin/conversations/im/close_immediately",vbox);
  iconpref1 = pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","Show _detailed information"))),"/pidgin/conversations/im/show_buddy_icons",vbox);
  iconpref2 = pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","Enable buddy ic_on animation"))),"/pidgin/conversations/im/animate_buddy_icons",vbox);
  if (!(purple_prefs_get_bool("/pidgin/conversations/im/show_buddy_icons") != 0)) 
    gtk_widget_set_sensitive(iconpref2,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)iconpref1),((GType )(20 << 2))))),"clicked",((GCallback )pidgin_toggle_sensitive),iconpref2,0,((GConnectFlags )0));
  pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","_Notify buddies that you are typing to them"))),"/purple/conversations/im/send_typing",vbox);
#ifdef USE_GTKSPELL
  pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","Highlight _misspelled words"))),"/pidgin/conversations/spellcheck",vbox);
#endif
  pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","Use smooth-scrolling"))),"/pidgin/conversations/use_smooth_scrolling",vbox);
#ifdef _WIN32
#endif
  hbox = gtk_hbox_new(0,6);
  checkbox = pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","Resize incoming custom smileys"))),"/pidgin/conversations/resize_custom_smileys",hbox);
  spin_button = pidgin_prefs_labeled_spin_button(hbox,((const char *)(dgettext("pidgin","Maximum size:"))),"/pidgin/conversations/custom_smileys_size",16,512,0);
  if (!(purple_prefs_get_bool("/pidgin/conversations/resize_custom_smileys") != 0)) 
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)spin_button),gtk_widget_get_type()))),0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)checkbox),((GType )(20 << 2))))),"clicked",((GCallback )pidgin_toggle_sensitive),spin_button,0,((GConnectFlags )0));
  pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),0,0,hbox,(!0),0);
  pidgin_prefs_labeled_spin_button(vbox,((const char *)(dgettext("pidgin","Minimum input area height in lines:"))),"/pidgin/conversations/minimum_entry_lines",1,8,0);
#ifdef _WIN32
#endif
  vbox = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Default Formatting"))));
  frame = pidgin_create_imhtml((!0),&imhtml,&toolbar,0);
  gtk_widget_show(frame);
  gtk_widget_set_name(imhtml,"pidgin_prefs_font_imhtml");
  gtk_widget_set_size_request(frame,450,(-1));
  gtk_imhtml_set_whole_buffer_formatting_only(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),(!0));
  gtk_imhtml_set_format_functions(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),(GTK_IMHTML_BOLD | GTK_IMHTML_ITALIC | GTK_IMHTML_UNDERLINE | GTK_IMHTML_GROW | GTK_IMHTML_SHRINK | GTK_IMHTML_FACE | GTK_IMHTML_FORECOLOR | GTK_IMHTML_BACKCOLOR | GTK_IMHTML_BACKGROUND));
  gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),((const char *)(dgettext("pidgin","This is how your outgoing message text will appear when you use protocols that support formatting."))),0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),frame,(!0),(!0),0);
  gtk_imhtml_setup_entry(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),(PURPLE_CONNECTION_HTML | PURPLE_CONNECTION_FORMATTING_WBFO));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"format_function_toggle",((GCallback )formatting_toggle_cb),toolbar,0,G_CONNECT_AFTER);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"format_function_clear",((GCallback )formatting_clear_cb),0,0,G_CONNECT_AFTER);
  sample_imhtml = imhtml;
  gtk_widget_show(ret);
  return ret;
}

static void network_ip_changed(GtkEntry *entry,gpointer data)
{
  const gchar *text = gtk_entry_get_text(entry);
  GdkColor color;
  if ((text != 0) && (( *text) != 0)) {
    if (purple_ip_address_is_valid(text) != 0) {
      color.red = 0xAFFF;
      color.green = 0xFFFF;
      color.blue = 0xAFFF;
      purple_network_set_public_ip(text);
    }
    else {
      color.red = 0xFFFF;
      color.green = 0xAFFF;
      color.blue = 0xAFFF;
    }
    gtk_widget_modify_base(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_widget_get_type()))),GTK_STATE_NORMAL,(&color));
  }
  else {
    purple_network_set_public_ip("");
    gtk_widget_modify_base(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_widget_get_type()))),GTK_STATE_NORMAL,0);
  }
}

static gboolean network_stun_server_changed_cb(GtkWidget *widget,GdkEventFocus *event,gpointer data)
{
  GtkEntry *entry = (GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_entry_get_type()));
  purple_prefs_set_string("/purple/network/stun_server",gtk_entry_get_text(entry));
  purple_network_set_stun_server(gtk_entry_get_text(entry));
  return 0;
}

static gboolean network_turn_server_changed_cb(GtkWidget *widget,GdkEventFocus *event,gpointer data)
{
  GtkEntry *entry = (GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_entry_get_type()));
  purple_prefs_set_string("/purple/network/turn_server",gtk_entry_get_text(entry));
  purple_network_set_turn_server(gtk_entry_get_text(entry));
  return 0;
}

static void proxy_changed_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  GtkWidget *frame = data;
  const char *proxy = value;
  if ((strcmp(proxy,"none") != 0) && (strcmp(proxy,"envvar") != 0)) 
    gtk_widget_show_all(frame);
  else 
    gtk_widget_hide(frame);
}

static void proxy_print_option(GtkEntry *entry,int entrynum)
{
  if (entrynum == 0) 
    purple_prefs_set_string("/purple/proxy/host",gtk_entry_get_text(entry));
  else if (entrynum == 1) 
    purple_prefs_set_int("/purple/proxy/port",atoi(gtk_entry_get_text(entry)));
  else if (entrynum == 2) 
    purple_prefs_set_string("/purple/proxy/username",gtk_entry_get_text(entry));
  else if (entrynum == 3) 
    purple_prefs_set_string("/purple/proxy/password",gtk_entry_get_text(entry));
}

static void proxy_button_clicked_cb(GtkWidget *button,gchar *program)
{
  GError *err = (GError *)((void *)0);
  if (g_spawn_command_line_async(program,&err) != 0) 
    return ;
  purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Cannot start proxy configuration program."))),(err -> message),0,0);
  g_error_free(err);
}
#ifndef _WIN32

static void browser_button_clicked_cb(GtkWidget *button,gchar *path)
{
  GError *err = (GError *)((void *)0);
  if (g_spawn_command_line_async(path,&err) != 0) 
    return ;
  purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Cannot start browser configuration program."))),(err -> message),0,0);
  g_error_free(err);
}
#endif

static void auto_ip_button_clicked_cb(GtkWidget *button,gpointer null)
{
  const char *ip;
  PurpleStunNatDiscovery *stun;
  char *auto_ip_text;
/* purple_network_get_my_ip will return the IP that was set by the user with
	   purple_network_set_public_ip, so make a lookup for the auto-detected IP
	   ourselves. */
  if (purple_prefs_get_bool("/purple/network/auto_ip") != 0) {
/* Check if STUN discovery was already done */
    stun = purple_stun_discover(0);
    if ((stun != ((PurpleStunNatDiscovery *)((void *)0))) && ((stun -> status) == PURPLE_STUN_STATUS_DISCOVERED)) {
      ip = (stun -> publicip);
    }
    else {
/* Attempt to get the IP from a NAT device using UPnP */
      ip = purple_upnp_get_public_ip();
      if (ip == ((const char *)((void *)0))) {
/* Attempt to get the IP from a NAT device using NAT-PMP */
        ip = (purple_pmp_get_public_ip());
        if (ip == ((const char *)((void *)0))) {
/* Just fetch the IP of the local system */
          ip = purple_network_get_local_system_ip(-1);
        }
      }
    }
  }
  else 
    ip = ((const char *)(dgettext("pidgin","Disabled")));
  auto_ip_text = g_strdup_printf(((const char *)(dgettext("pidgin","Use _automatically detected IP address: %s"))),ip);
  gtk_button_set_label(((GtkButton *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_button_get_type()))),auto_ip_text);
  g_free(auto_ip_text);
}

static GtkWidget *network_page()
{
  GtkWidget *ret;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *entry;
  GtkWidget *label;
  GtkWidget *auto_ip_checkbox;
  GtkWidget *ports_checkbox;
  GtkWidget *spin_button;
  GtkSizeGroup *sg;
  ret = gtk_vbox_new(0,18);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_container_get_type()))),12);
  vbox = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","IP Address"))));
  sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  entry = gtk_entry_new();
  gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),purple_prefs_get_string("/purple/network/stun_server"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"focus-out-event",((GCallback )network_stun_server_changed_cb),0,0,((GConnectFlags )0));
  gtk_widget_show(entry);
  pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),((const char *)(dgettext("pidgin","ST_UN server:"))),sg,entry,(!0),0);
  hbox = gtk_hbox_new(0,6);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),hbox);
  label = gtk_label_new(0);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_container_get_type()))),label);
  gtk_size_group_add_widget(sg,label);
  label = gtk_label_new(0);
  gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),((const char *)(dgettext("pidgin","<span style=\"italic\">Example: stunserver.org</span>"))));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0.0,0.5);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_container_get_type()))),label);
  auto_ip_checkbox = pidgin_prefs_checkbox("Use _automatically detected IP address","/purple/network/auto_ip",vbox);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)auto_ip_checkbox),((GType )(20 << 2))))),"clicked",((GCallback )auto_ip_button_clicked_cb),0,0,((GConnectFlags )0));
/* Update label */
  auto_ip_button_clicked_cb(auto_ip_checkbox,0);
  entry = gtk_entry_new();
  gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),purple_network_get_public_ip());
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"changed",((GCallback )network_ip_changed),0,0,((GConnectFlags )0));
  hbox = pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),((const char *)(dgettext("pidgin","Public _IP:"))),sg,entry,(!0),0);
  if (purple_prefs_get_bool("/purple/network/auto_ip") != 0) {
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_widget_get_type()))),0);
  }
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)auto_ip_checkbox),((GType )(20 << 2))))),"clicked",((GCallback )pidgin_toggle_sensitive),hbox,0,((GConnectFlags )0));
  g_object_unref(sg);
  vbox = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Ports"))));
  sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","_Enable automatic router port forwarding"))),"/purple/network/map_ports",vbox);
  hbox = gtk_hbox_new(0,6);
  ports_checkbox = pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","_Manually specify range of ports to listen on:"))),"/purple/network/ports_range_use",hbox);
  spin_button = pidgin_prefs_labeled_spin_button(hbox,((const char *)(dgettext("pidgin","_Start:"))),"/purple/network/ports_range_start",0,0xFFFF,sg);
  if (!(purple_prefs_get_bool("/purple/network/ports_range_use") != 0)) 
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)spin_button),gtk_widget_get_type()))),0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ports_checkbox),((GType )(20 << 2))))),"clicked",((GCallback )pidgin_toggle_sensitive),spin_button,0,((GConnectFlags )0));
  spin_button = pidgin_prefs_labeled_spin_button(hbox,((const char *)(dgettext("pidgin","_End:"))),"/purple/network/ports_range_end",0,0xFFFF,sg);
  if (!(purple_prefs_get_bool("/purple/network/ports_range_use") != 0)) 
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)spin_button),gtk_widget_get_type()))),0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ports_checkbox),((GType )(20 << 2))))),"clicked",((GCallback )pidgin_toggle_sensitive),spin_button,0,((GConnectFlags )0));
  pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),0,0,hbox,(!0),0);
  g_object_unref(sg);
/* TURN server */
  vbox = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Relay Server (TURN)"))));
  sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  entry = gtk_entry_new();
  gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),purple_prefs_get_string("/purple/network/turn_server"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"focus-out-event",((GCallback )network_turn_server_changed_cb),0,0,((GConnectFlags )0));
  gtk_widget_show(entry);
  hbox = pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),((const char *)(dgettext("pidgin","_TURN server:"))),sg,entry,(!0),0);
  pidgin_prefs_labeled_spin_button(hbox,((const char *)(dgettext("pidgin","_UDP Port:"))),"/purple/network/turn_port",0,0xFFFF,0);
  pidgin_prefs_labeled_spin_button(hbox,((const char *)(dgettext("pidgin","T_CP Port:"))),"/purple/network/turn_port_tcp",0,0xFFFF,0);
  hbox = pidgin_prefs_labeled_entry(vbox,((const char *)(dgettext("pidgin","Use_rname:"))),"/purple/network/turn_username",sg);
  pidgin_prefs_labeled_password(hbox,((const char *)(dgettext("pidgin","Pass_word:"))),"/purple/network/turn_password",0);
  gtk_widget_show_all(ret);
  g_object_unref(sg);
  return ret;
}
#ifndef _WIN32

static gboolean manual_browser_set(GtkWidget *entry,GdkEventFocus *event,gpointer data)
{
  const char *program = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))));
  purple_prefs_set_string("/pidgin/browsers/manual_command",program);
/* carry on normally */
  return 0;
}

static GList *get_available_browsers()
{
  

  struct browser 
{
    char *name;
    char *command;
  }
;
/* Sorted reverse alphabetically */
  static const struct browser possible_browsers[] = {{("Seamonkey"), ("seamonkey")}, {("Opera"), ("opera")}, {("Netscape"), ("netscape")}, {("Mozilla"), ("mozilla")}, {("Konqueror"), ("kfmclient")}, {("Google Chrome"), ("google-chrome")}, 
/* Do not move the line below.  Code below expects gnome-open to be in
		 * this list immediately after xdg-open! */
{("Desktop Default"), ("xdg-open")}, {("GNOME Default"), ("gnome-open")}, {("Galeon"), ("galeon")}, {("Firefox"), ("firefox")}, {("Firebird"), ("mozilla-firebird")}, {("Epiphany"), ("epiphany")}, 
/* Translators: please do not translate "chromium-browser" here! */
{("Chromium (chromium-browser)"), ("chromium-browser")}, 
/* Translators: please do not translate "chrome" here! */
{("Chromium (chrome)"), ("chrome")}};
  static const int num_possible_browsers = (sizeof(possible_browsers) / sizeof(possible_browsers[0]));
  GList *browsers = (GList *)((void *)0);
  int i = 0;
  char *browser_setting = (char *)(purple_prefs_get_string("/pidgin/browsers/browser"));
  browsers = g_list_prepend(browsers,((gpointer )"custom"));
  browsers = g_list_prepend(browsers,((gpointer )((const char *)(dgettext("pidgin","Manual")))));
  for (i = 0; i < num_possible_browsers; i++) {
    if (purple_program_is_valid(possible_browsers[i].command) != 0) {
      browsers = g_list_prepend(browsers,possible_browsers[i].command);
      browsers = g_list_prepend(browsers,((gpointer )((const char *)(dgettext("pidgin",possible_browsers[i].name)))));
      if ((browser_setting != 0) && !(strcmp(possible_browsers[i].command,browser_setting) != 0)) 
        browser_setting = ((char *)((void *)0));
/* If xdg-open is valid, prefer it over gnome-open and skip forward */
      if (!(strcmp(possible_browsers[i].command,"xdg-open") != 0)) {
        if ((browser_setting != 0) && !(strcmp("gnome-open",browser_setting) != 0)) {
          purple_prefs_set_string("/pidgin/browsers/browser",possible_browsers[i].command);
          browser_setting = ((char *)((void *)0));
        }
        i++;
      }
    }
  }
  if (browser_setting != 0) 
    purple_prefs_set_string("/pidgin/browsers/browser","custom");
  return browsers;
}

static void browser_changed1_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  GtkWidget *hbox = data;
  const char *browser = value;
  gtk_widget_set_sensitive(hbox,strcmp(browser,"custom"));
}

static void browser_changed2_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  GtkWidget *hbox = data;
  const char *browser = value;
  gtk_widget_set_sensitive(hbox,!(strcmp(browser,"custom") != 0));
}

static GtkWidget *browser_page()
{
  GtkWidget *ret;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *entry;
  GtkWidget *browser_button;
  GtkSizeGroup *sg;
  GList *browsers = (GList *)((void *)0);
  ret = gtk_vbox_new(0,18);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_container_get_type()))),12);
  vbox = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Browser Selection"))));
  if (purple_running_gnome() != 0) {
    gchar *path;
    hbox = gtk_hbox_new(0,6);
    label = gtk_label_new(((const char *)(dgettext("pidgin","Browser preferences are configured in GNOME preferences"))));
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),hbox);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
    hbox = gtk_hbox_new(0,6);
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),hbox);
    path = g_find_program_in_path("gnome-control-center");
    if (path != ((gchar *)((void *)0))) {
      gchar *tmp = g_strdup_printf("%s info",path);
      g_free(path);
      path = tmp;
    }
    else {
      path = g_find_program_in_path("gnome-default-applications-properties");
    }
    if (path == ((gchar *)((void *)0))) {
      label = gtk_label_new(0);
      gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),((const char *)(dgettext("pidgin","<b>Browser configuration program was not found.</b>"))));
      gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
    }
    else {
      browser_button = gtk_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Configure _Browser"))));
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)browser_button),((GType )(20 << 2))))),"clicked",((GCallback )browser_button_clicked_cb),path,((GClosureNotify )g_free),0);
      gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),browser_button,0,0,0);
    }
    gtk_widget_show_all(ret);
  }
  else {
    sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
    browsers = get_available_browsers();
    if (browsers != ((GList *)((void *)0))) {
      label = pidgin_prefs_dropdown_from_list(vbox,((const char *)(dgettext("pidgin","_Browser:"))),PURPLE_PREF_STRING,"/pidgin/browsers/browser",browsers);
      g_list_free(browsers);
      gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
      gtk_size_group_add_widget(sg,label);
      hbox = gtk_hbox_new(0,0);
      label = pidgin_prefs_dropdown(hbox,((const char *)(dgettext("pidgin","_Open link in:"))),PURPLE_PREF_INT,"/pidgin/browsers/place",((const char *)(dgettext("pidgin","Browser default"))),PIDGIN_BROWSER_DEFAULT,((const char *)(dgettext("pidgin","Existing window"))),PIDGIN_BROWSER_CURRENT,((const char *)(dgettext("pidgin","New window"))),PIDGIN_BROWSER_NEW_WINDOW,((const char *)(dgettext("pidgin","New tab"))),PIDGIN_BROWSER_NEW_TAB,((void *)((void *)0)));
      gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
      gtk_size_group_add_widget(sg,label);
      gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
      if (!(strcmp(purple_prefs_get_string("/pidgin/browsers/browser"),"custom") != 0)) 
        gtk_widget_set_sensitive(hbox,0);
      purple_prefs_connect_callback(prefs,"/pidgin/browsers/browser",browser_changed1_cb,hbox);
    }
    entry = gtk_entry_new();
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),purple_prefs_get_string("/pidgin/browsers/manual_command"));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"focus-out-event",((GCallback )manual_browser_set),0,0,((GConnectFlags )0));
    hbox = pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),((const char *)(dgettext("pidgin","_Manual:\n(%s for URL)"))),sg,entry,(!0),0);
    if (strcmp(purple_prefs_get_string("/pidgin/browsers/browser"),"custom") != 0) 
      gtk_widget_set_sensitive(hbox,0);
    purple_prefs_connect_callback(prefs,"/pidgin/browsers/browser",browser_changed2_cb,hbox);
    gtk_widget_show_all(ret);
    g_object_unref(sg);
  }
  return ret;
}
#endif /*_WIN32*/

static GtkWidget *proxy_page()
{
  GtkWidget *ret = (GtkWidget *)((void *)0);
  GtkWidget *vbox = (GtkWidget *)((void *)0);
  GtkWidget *hbox = (GtkWidget *)((void *)0);
  GtkWidget *table = (GtkWidget *)((void *)0);
  GtkWidget *entry = (GtkWidget *)((void *)0);
  GtkWidget *label = (GtkWidget *)((void *)0);
  GtkWidget *proxy_button = (GtkWidget *)((void *)0);
  GtkWidget *prefs_proxy_frame = (GtkWidget *)((void *)0);
  PurpleProxyInfo *proxy_info;
  ret = gtk_vbox_new(0,18);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_container_get_type()))),12);
  vbox = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Proxy Server"))));
  prefs_proxy_frame = gtk_vbox_new(0,6);
  if (purple_running_gnome() != 0) {
    gchar *path = (gchar *)((void *)0);
    hbox = gtk_hbox_new(0,6);
    label = gtk_label_new(((const char *)(dgettext("pidgin","Proxy preferences are configured in GNOME preferences"))));
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),hbox);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
    hbox = gtk_hbox_new(0,6);
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),hbox);
    path = g_find_program_in_path("gnome-network-properties");
    if (path == ((gchar *)((void *)0))) 
      path = g_find_program_in_path("gnome-network-preferences");
    if (path == ((gchar *)((void *)0))) {
      path = g_find_program_in_path("gnome-control-center");
      if (path != ((gchar *)((void *)0))) {
        char *tmp = g_strdup_printf("%s network",path);
        g_free(path);
        path = tmp;
      }
    }
    if (path == ((gchar *)((void *)0))) {
      label = gtk_label_new(0);
      gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),((const char *)(dgettext("pidgin","<b>Proxy configuration program was not found.</b>"))));
      gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
    }
    else {
      proxy_button = gtk_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Configure _Proxy"))));
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)proxy_button),((GType )(20 << 2))))),"clicked",((GCallback )proxy_button_clicked_cb),path,0,((GConnectFlags )0));
      gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),proxy_button,0,0,0);
    }
/* NOTE: path leaks, but only when the prefs window is destroyed,
		         which is never */
    gtk_widget_show_all(ret);
  }
  else {
    GtkWidget *prefs_proxy_subframe = gtk_vbox_new(0,0);
/* This is a global option that affects SOCKS4 usage even with
		 * account-specific proxy settings */
    pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","Use remote _DNS with SOCKS4 proxies"))),"/purple/proxy/socks4_remotedns",prefs_proxy_frame);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),prefs_proxy_frame,0,0,0);
    pidgin_prefs_dropdown(prefs_proxy_frame,((const char *)(dgettext("pidgin","Proxy t_ype:"))),PURPLE_PREF_STRING,"/purple/proxy/type",((const char *)(dgettext("pidgin","No proxy"))),"none",((const char *)(dgettext("pidgin","SOCKS 4"))),"socks4",((const char *)(dgettext("pidgin","SOCKS 5"))),"socks5",((const char *)(dgettext("pidgin","Tor/Privacy (SOCKS5)"))),"tor",((const char *)(dgettext("pidgin","HTTP"))),"http",((const char *)(dgettext("pidgin","Use Environmental Settings"))),"envvar",((void *)((void *)0)));
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)prefs_proxy_frame),gtk_box_get_type()))),prefs_proxy_subframe,0,0,0);
    proxy_info = purple_global_proxy_get_info();
    gtk_widget_show_all(ret);
    purple_prefs_connect_callback(prefs,"/purple/proxy/type",proxy_changed_cb,prefs_proxy_subframe);
    table = gtk_table_new(4,2,0);
    gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_container_get_type()))),0);
    gtk_table_set_col_spacings(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),5);
    gtk_table_set_row_spacings(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),10);
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)prefs_proxy_subframe),gtk_container_get_type()))),table);
    label = gtk_label_new_with_mnemonic(((const char *)(dgettext("pidgin","_Host:"))));
    gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),1.0,0.5);
    gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),label,0,1,0,1,GTK_FILL,0,0,0);
    entry = gtk_entry_new();
    gtk_label_set_mnemonic_widget(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),entry);
    gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),entry,1,2,0,1,GTK_FILL,0,0,0);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"changed",((GCallback )proxy_print_option),0,0,((GConnectFlags )0));
    if ((proxy_info != ((PurpleProxyInfo *)((void *)0))) && (purple_proxy_info_get_host(proxy_info) != 0)) 
      gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),purple_proxy_info_get_host(proxy_info));
    hbox = gtk_hbox_new((!0),5);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
    pidgin_set_accessible_label(entry,label);
    label = gtk_label_new_with_mnemonic(((const char *)(dgettext("pidgin","P_ort:"))));
    gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),1.0,0.5);
    gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),label,2,3,0,1,GTK_FILL,0,0,0);
    entry = gtk_spin_button_new_with_range(0,0xFFFF,1);
    gtk_label_set_mnemonic_widget(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),entry);
    gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),entry,3,4,0,1,GTK_FILL,0,0,0);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"changed",((GCallback )proxy_print_option),((gpointer )((void *)1)),0,((GConnectFlags )0));
    if ((proxy_info != ((PurpleProxyInfo *)((void *)0))) && (purple_proxy_info_get_port(proxy_info) != 0)) {
      gtk_spin_button_set_value(((GtkSpinButton *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_spin_button_get_type()))),(purple_proxy_info_get_port(proxy_info)));
    }
    pidgin_set_accessible_label(entry,label);
    label = gtk_label_new_with_mnemonic(((const char *)(dgettext("pidgin","User_name:"))));
    gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),1.0,0.5);
    gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),label,0,1,1,2,GTK_FILL,0,0,0);
    entry = gtk_entry_new();
    gtk_label_set_mnemonic_widget(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),entry);
    gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),entry,1,2,1,2,GTK_FILL,0,0,0);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"changed",((GCallback )proxy_print_option),((gpointer )((void *)2)),0,((GConnectFlags )0));
    if ((proxy_info != ((PurpleProxyInfo *)((void *)0))) && (purple_proxy_info_get_username(proxy_info) != ((const char *)((void *)0)))) 
      gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),purple_proxy_info_get_username(proxy_info));
    hbox = gtk_hbox_new((!0),5);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
    pidgin_set_accessible_label(entry,label);
    label = gtk_label_new_with_mnemonic(((const char *)(dgettext("pidgin","Pa_ssword:"))));
    gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),1.0,0.5);
    gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),label,2,3,1,2,GTK_FILL,0,0,0);
    entry = gtk_entry_new();
    gtk_label_set_mnemonic_widget(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),entry);
    gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),entry,3,4,1,2,GTK_FILL,0,0,0);
    gtk_entry_set_visibility(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),0);
#if !GTK_CHECK_VERSION(2,16,0)
#endif /* Less than GTK+ 2.16 */
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"changed",((GCallback )proxy_print_option),((gpointer )((void *)3)),0,((GConnectFlags )0));
    if ((proxy_info != ((PurpleProxyInfo *)((void *)0))) && (purple_proxy_info_get_password(proxy_info) != ((const char *)((void *)0)))) 
      gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),purple_proxy_info_get_password(proxy_info));
    pidgin_set_accessible_label(entry,label);
    proxy_changed_cb("/purple/proxy/type",PURPLE_PREF_STRING,(purple_prefs_get_string("/purple/proxy/type")),prefs_proxy_subframe);
  }
  return ret;
}

static GtkWidget *logging_page()
{
  GtkWidget *ret;
  GtkWidget *vbox;
  GList *names;
  ret = gtk_vbox_new(0,18);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_container_get_type()))),12);
  vbox = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Logging"))));
  names = purple_log_logger_get_options();
  pidgin_prefs_dropdown_from_list(vbox,((const char *)(dgettext("pidgin","Log _format:"))),PURPLE_PREF_STRING,"/purple/logging/format",names);
  g_list_free(names);
  pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","Log all _instant messages"))),"/purple/logging/log_ims",vbox);
  pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","Log all c_hats"))),"/purple/logging/log_chats",vbox);
  pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","Log all _status changes to system log"))),"/purple/logging/log_system",vbox);
  gtk_widget_show_all(ret);
  return ret;
}
#ifndef _WIN32

static gint sound_cmd_yeah(GtkEntry *entry,gpointer d)
{
  purple_prefs_set_path("/pidgin/sound/command",gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type())))));
  return (!0);
}

static void sound_changed1_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  GtkWidget *hbox = data;
  const char *method = value;
  gtk_widget_set_sensitive(hbox,!(strcmp(method,"custom") != 0));
}

static void sound_changed2_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  GtkWidget *vbox = data;
  const char *method = value;
  gtk_widget_set_sensitive(vbox,strcmp(method,"none"));
}
#endif /* !_WIN32 */
#ifdef USE_GSTREAMER

static void sound_changed3_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  GtkWidget *hbox = data;
  const char *method = value;
  gtk_widget_set_sensitive(hbox,((!(strcmp(method,"automatic") != 0) || !(strcmp(method,"alsa") != 0)) || !(strcmp(method,"esd") != 0)));
}
#endif /* USE_GSTREAMER */

static void event_toggled(GtkCellRendererToggle *cell,gchar *pth,gpointer data)
{
  GtkTreeModel *model = (GtkTreeModel *)data;
  GtkTreeIter iter;
  GtkTreePath *path = gtk_tree_path_new_from_string(pth);
  char *pref;
  gtk_tree_model_get_iter(model,&iter,path);
  gtk_tree_model_get(model,&iter,2,&pref,-1);
  purple_prefs_set_bool(pref,!(gtk_cell_renderer_toggle_get_active(cell) != 0));
  g_free(pref);
  gtk_list_store_set(((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_list_store_get_type()))),&iter,0,!(gtk_cell_renderer_toggle_get_active(cell) != 0),-1);
  gtk_tree_path_free(path);
}

static void test_sound(GtkWidget *button,gpointer i_am_NULL)
{
  char *pref;
  gboolean temp_enabled;
  gboolean temp_mute;
  pref = g_strdup_printf("/pidgin/sound/enabled/%s",pidgin_sound_get_event_option(sound_row_sel));
  temp_enabled = purple_prefs_get_bool(pref);
  temp_mute = purple_prefs_get_bool("/pidgin/sound/mute");
  if (!(temp_enabled != 0)) 
    purple_prefs_set_bool(pref,(!0));
  if (temp_mute != 0) 
    purple_prefs_set_bool("/pidgin/sound/mute",0);
  purple_sound_play_event(sound_row_sel,0);
  if (!(temp_enabled != 0)) 
    purple_prefs_set_bool(pref,0);
  if (temp_mute != 0) 
    purple_prefs_set_bool("/pidgin/sound/mute",(!0));
  g_free(pref);
}
/*
 * Resets a sound file back to default.
 */

static void reset_sound(GtkWidget *button,gpointer i_am_also_NULL)
{
  gchar *pref;
  pref = g_strdup_printf("/pidgin/sound/file/%s",pidgin_sound_get_event_option(sound_row_sel));
  purple_prefs_set_path(pref,"");
  g_free(pref);
  gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)sound_entry),gtk_entry_get_type()))),((const char *)(dgettext("pidgin","(default)"))));
  pref_sound_generate_markup();
}

static void sound_chosen_cb(void *user_data,const char *filename)
{
  gchar *pref;
  int sound;
  sound = ((gint )((glong )user_data));
/* Set it -- and forget it */
  pref = g_strdup_printf("/pidgin/sound/file/%s",pidgin_sound_get_event_option(sound));
  purple_prefs_set_path(pref,filename);
  g_free(pref);
/*
	 * If the sound we just changed is still the currently selected
	 * sound, then update the box showing the file name.
	 */
  if (sound == sound_row_sel) 
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)sound_entry),gtk_entry_get_type()))),filename);
  pref_sound_generate_markup();
}

static void select_sound(GtkWidget *button,gpointer being_NULL_is_fun)
{
  gchar *pref;
  const char *filename;
  pref = g_strdup_printf("/pidgin/sound/file/%s",pidgin_sound_get_event_option(sound_row_sel));
  filename = purple_prefs_get_path(pref);
  g_free(pref);
  if (( *filename) == 0) 
    filename = ((const char *)((void *)0));
  purple_request_file(prefs,((const char *)(dgettext("pidgin","Sound Selection"))),filename,0,((GCallback )sound_chosen_cb),0,0,0,0,((gpointer )((glong )sound_row_sel)));
}
#ifdef USE_GSTREAMER

static gchar *prefs_sound_volume_format(GtkScale *scale,gdouble val)
{
  if (val < 15) {
    return g_strdup_printf(((const char *)(dgettext("pidgin","Quietest"))));
  }
  else if (val < 30) {
    return g_strdup_printf(((const char *)(dgettext("pidgin","Quieter"))));
  }
  else if (val < 45) {
    return g_strdup_printf(((const char *)(dgettext("pidgin","Quiet"))));
  }
  else if (val < 55) {
    return g_strdup_printf(((const char *)(dgettext("pidgin","Normal"))));
  }
  else if (val < 70) {
    return g_strdup_printf(((const char *)(dgettext("pidgin","Loud"))));
  }
  else if (val < 85) {
    return g_strdup_printf(((const char *)(dgettext("pidgin","Louder"))));
  }
  else {
    return g_strdup_printf(((const char *)(dgettext("pidgin","Loudest"))));
  }
}

static void prefs_sound_volume_changed(GtkRange *range)
{
  int val = (int )(gtk_range_get_value(((GtkRange *)(g_type_check_instance_cast(((GTypeInstance *)range),gtk_range_get_type())))));
  purple_prefs_set_int("/pidgin/sound/volume",val);
}
#endif

static void prefs_sound_sel(GtkTreeSelection *sel,GtkTreeModel *model)
{
  GtkTreeIter iter;
  GValue val;
  const char *file;
  char *pref;
  if (!(gtk_tree_selection_get_selected(sel,&model,&iter) != 0)) 
    return ;
  val.g_type = 0;
  gtk_tree_model_get_value(model,&iter,3,&val);
  sound_row_sel = (g_value_get_uint((&val)));
  pref = g_strdup_printf("/pidgin/sound/file/%s",pidgin_sound_get_event_option(sound_row_sel));
  file = purple_prefs_get_path(pref);
  g_free(pref);
  if (sound_entry != 0) 
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)sound_entry),gtk_entry_get_type()))),(((file != 0) && (( *file) != 0))?file : ((const char *)(dgettext("pidgin","(default)")))));
  g_value_unset(&val);
  pref_sound_generate_markup();
}

static void mute_changed_cb(const char *pref_name,PurplePrefType pref_type,gconstpointer val,gpointer data)
{
  GtkToggleButton *button = data;
  gboolean muted = (gint )((glong )val);
  do {
    if (!(strcmp(pref_name,"/pidgin/sound/mute") != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"!strcmp (pref_name, PIDGIN_PREFS_ROOT \"/sound/mute\")");
      return ;
    };
  }while (0);
/* Block the handler that re-sets the preference. */
  g_signal_handlers_block_matched(button,G_SIGNAL_MATCH_DATA,0,0,0,0,((gpointer )pref_name));
  gtk_toggle_button_set_active(button,muted);
  g_signal_handlers_unblock_matched(button,G_SIGNAL_MATCH_DATA,0,0,0,0,((gpointer )pref_name));
}

static GtkWidget *sound_page()
{
  GtkWidget *ret;
  GtkWidget *vbox;
  GtkWidget *vbox2;
  GtkWidget *sw;
  GtkWidget *button;
  GtkSizeGroup *sg;
  GtkTreeIter iter;
  GtkWidget *event_view;
  GtkListStore *event_store;
  GtkCellRenderer *rend;
  GtkTreeViewColumn *col;
  GtkTreeSelection *sel;
  GtkTreePath *path;
  GtkWidget *hbox;
  int j;
  const char *file;
  char *pref;
#ifndef _WIN32
  GtkWidget *dd;
  GtkWidget *entry;
  const char *cmd;
#endif
  ret = gtk_vbox_new(0,18);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_container_get_type()))),12);
  sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  vbox2 = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Sound Options"))));
  vbox = gtk_vbox_new(0,6);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox2),gtk_box_get_type()))),vbox,0,0,0);
#ifndef _WIN32
  dd = pidgin_prefs_dropdown(vbox2,((const char *)(dgettext("pidgin","_Method:"))),PURPLE_PREF_STRING,"/pidgin/sound/method",((const char *)(dgettext("pidgin","Console beep"))),"beep",((const char *)(dgettext("pidgin","Automatic"))),"automatic","ESD","esd","ALSA","alsa",((const char *)(dgettext("pidgin","Command"))),"custom",((const char *)(dgettext("pidgin","No sounds"))),"none",((void *)((void *)0)));
#ifdef USE_GSTREAMER
#endif
  gtk_size_group_add_widget(sg,dd);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)dd),gtk_misc_get_type()))),0,0.5);
  entry = gtk_entry_new();
  gtk_editable_set_editable(((GtkEditable *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_editable_get_type()))),(!0));
  cmd = purple_prefs_get_path("/pidgin/sound/command");
  if (cmd != 0) 
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),cmd);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"changed",((GCallback )sound_cmd_yeah),0,0,((GConnectFlags )0));
  hbox = pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),((const char *)(dgettext("pidgin","Sound c_ommand:\n(%s for filename)"))),sg,entry,(!0),0);
  purple_prefs_connect_callback(prefs,"/pidgin/sound/method",sound_changed1_cb,hbox);
  gtk_widget_set_sensitive(hbox,!(strcmp(purple_prefs_get_string("/pidgin/sound/method"),"custom") != 0));
#endif /* _WIN32 */
  button = pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","M_ute sounds"))),"/pidgin/sound/mute",vbox);
  purple_prefs_connect_callback(prefs,"/pidgin/sound/mute",mute_changed_cb,button);
  pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","Sounds when conversation has _focus"))),"/pidgin/sound/conv_focus",vbox);
  pidgin_prefs_dropdown(vbox,((const char *)(dgettext("pidgin","_Enable sounds:"))),PURPLE_PREF_INT,"/purple/sound/while_status",((const char *)(dgettext("pidgin","Only when available"))),1,((const char *)(dgettext("pidgin","Only when not available"))),2,((const char *)(dgettext("pidgin","Always"))),3,((void *)((void *)0)));
#ifdef USE_GSTREAMER
  sw = gtk_hscale_new_with_range(0.0,100.0,5.0);
  gtk_range_set_increments(((GtkRange *)(g_type_check_instance_cast(((GTypeInstance *)sw),gtk_range_get_type()))),5.0,25.0);
  gtk_range_set_value(((GtkRange *)(g_type_check_instance_cast(((GTypeInstance *)sw),gtk_range_get_type()))),(purple_prefs_get_int("/pidgin/sound/volume")));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)sw),((GType )(20 << 2))))),"format-value",((GCallback )prefs_sound_volume_format),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)sw),((GType )(20 << 2))))),"value-changed",((GCallback )prefs_sound_volume_changed),0,0,((GConnectFlags )0));
  hbox = pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),((const char *)(dgettext("pidgin","V_olume:"))),0,sw,(!0),0);
  purple_prefs_connect_callback(prefs,"/pidgin/sound/method",sound_changed3_cb,hbox);
  sound_changed3_cb("/pidgin/sound/method",PURPLE_PREF_STRING,(purple_prefs_get_string("/pidgin/sound/method")),hbox);
#endif
#ifndef _WIN32
  gtk_widget_set_sensitive(vbox,strcmp(purple_prefs_get_string("/pidgin/sound/method"),"none"));
  purple_prefs_connect_callback(prefs,"/pidgin/sound/method",sound_changed2_cb,vbox);
#endif
  vbox = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Sound Events"))));
/* The following is an ugly hack to make the frame expand so the
	 * sound events list is big enough to be usable */
  gtk_box_set_child_packing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(vbox -> parent)),gtk_box_get_type()))),vbox,(!0),(!0),0,GTK_PACK_START);
  gtk_box_set_child_packing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)( *(vbox -> parent)).parent),gtk_box_get_type()))),(vbox -> parent),(!0),(!0),0,GTK_PACK_START);
  gtk_box_set_child_packing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)( *( *(vbox -> parent)).parent).parent),gtk_box_get_type()))),( *(vbox -> parent)).parent,(!0),(!0),0,GTK_PACK_START);
/* SOUND SELECTION */
  event_store = gtk_list_store_new(4,((GType )(5 << 2)),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(7 << 2)));
  for (j = 0; j < PURPLE_NUM_SOUNDS; j++) {{
      char *pref = g_strdup_printf("/pidgin/sound/enabled/%s",pidgin_sound_get_event_option(j));
      const char *label = pidgin_sound_get_event_label(j);
      if (label == ((const char *)((void *)0))) {
        g_free(pref);
        continue; 
      }
      gtk_list_store_append(event_store,&iter);
      gtk_list_store_set(event_store,&iter,0,purple_prefs_get_bool(pref),1,((const char *)(dgettext("pidgin",label))),2,pref,3,j,-1);
      g_free(pref);
    }
  }
  event_view = gtk_tree_view_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)event_store),gtk_tree_model_get_type()))));
  rend = gtk_cell_renderer_toggle_new();
  sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)event_view),gtk_tree_view_get_type()))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)sel),((GType )(20 << 2))))),"changed",((GCallback )prefs_sound_sel),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)rend),((GType )(20 << 2))))),"toggled",((GCallback )event_toggled),event_store,0,((GConnectFlags )0));
  path = gtk_tree_path_new_first();
  gtk_tree_selection_select_path(sel,path);
  gtk_tree_path_free(path);
  col = gtk_tree_view_column_new_with_attributes(((const char *)(dgettext("pidgin","Play"))),rend,"active",0,((void *)((void *)0)));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)event_view),gtk_tree_view_get_type()))),col);
  rend = gtk_cell_renderer_text_new();
  col = gtk_tree_view_column_new_with_attributes(((const char *)(dgettext("pidgin","Event"))),rend,"text",1,((void *)((void *)0)));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)event_view),gtk_tree_view_get_type()))),col);
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)event_store),((GType )(20 << 2))))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),pidgin_make_scrollable(event_view,GTK_POLICY_NEVER,GTK_POLICY_AUTOMATIC,GTK_SHADOW_IN,-1,100),(!0),(!0),0);
  hbox = gtk_hbox_new(0,6);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  sound_entry = gtk_entry_new();
  pref = g_strdup_printf("/pidgin/sound/file/%s",pidgin_sound_get_event_option(0));
  file = purple_prefs_get_path(pref);
  g_free(pref);
  gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)sound_entry),gtk_entry_get_type()))),(((file != 0) && (( *file) != 0))?file : ((const char *)(dgettext("pidgin","(default)")))));
  gtk_editable_set_editable(((GtkEditable *)(g_type_check_instance_cast(((GTypeInstance *)sound_entry),gtk_editable_get_type()))),0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),sound_entry,0,0,6);
  button = gtk_button_new_with_mnemonic(((const char *)(dgettext("pidgin","_Browse..."))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )select_sound),0,0,((GConnectFlags )0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),button,0,0,1);
  button = gtk_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Pre_view"))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )test_sound),0,0,((GConnectFlags )0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),button,0,0,1);
  button = gtk_button_new_with_mnemonic(((const char *)(dgettext("pidgin","_Reset"))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )reset_sound),0,0,((GConnectFlags )0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),button,0,0,1);
  gtk_widget_show_all(ret);
  g_object_unref(sg);
  return ret;
}

static void set_idle_away(PurpleSavedStatus *status)
{
  purple_prefs_set_int("/purple/savedstatus/idleaway",(purple_savedstatus_get_creation_time(status)));
}

static void set_startupstatus(PurpleSavedStatus *status)
{
  purple_prefs_set_int("/purple/savedstatus/startup",(purple_savedstatus_get_creation_time(status)));
}

static GtkWidget *away_page()
{
  GtkWidget *ret;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *dd;
  GtkWidget *label;
  GtkWidget *button;
  GtkWidget *select;
  GtkWidget *menu;
  GtkSizeGroup *sg;
  ret = gtk_vbox_new(0,18);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_container_get_type()))),12);
  sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
/* Idle stuff */
  vbox = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Idle"))));
  dd = pidgin_prefs_dropdown(vbox,((const char *)(dgettext("pidgin","_Report idle time:"))),PURPLE_PREF_STRING,"/purple/away/idle_reporting",((const char *)(dgettext("pidgin","Never"))),"none",((const char *)(dgettext("pidgin","From last sent message"))),"purple",((const char *)(dgettext("pidgin","Based on keyboard or mouse use"))),"system",((void *)((void *)0)));
#if defined(USE_SCREENSAVER) || defined(HAVE_IOKIT)
#endif
  gtk_size_group_add_widget(sg,dd);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)dd),gtk_misc_get_type()))),0,0.5);
  select = pidgin_prefs_labeled_spin_button(vbox,((const char *)(dgettext("pidgin","_Minutes before becoming idle:"))),"/purple/away/mins_before_away",1,24 * 60,sg);
  hbox = gtk_hbox_new(0,6);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  button = pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","Change to this status when _idle:"))),"/purple/away/away_when_idle",hbox);
  gtk_size_group_add_widget(sg,button);
/* TODO: Show something useful if we don't have any saved statuses. */
  menu = pidgin_status_menu(purple_savedstatus_get_idleaway(),((GCallback )set_idle_away));
  gtk_size_group_add_widget(sg,menu);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),menu,0,0,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )pidgin_toggle_sensitive),menu,0,((GConnectFlags )0));
  if (!(purple_prefs_get_bool("/purple/away/away_when_idle") != 0)) 
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_widget_get_type()))),0);
/* Away stuff */
  vbox = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Away"))));
  dd = pidgin_prefs_dropdown(vbox,((const char *)(dgettext("pidgin","_Auto-reply:"))),PURPLE_PREF_STRING,"/purple/away/auto_reply",((const char *)(dgettext("pidgin","Never"))),"never",((const char *)(dgettext("pidgin","When away"))),"away",((const char *)(dgettext("pidgin","When both away and idle"))),"awayidle",((void *)((void *)0)));
  gtk_size_group_add_widget(sg,dd);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)dd),gtk_misc_get_type()))),0,0.5);
/* Signon status stuff */
  vbox = pidgin_make_frame(ret,((const char *)(dgettext("pidgin","Status at Startup"))));
  button = pidgin_prefs_checkbox(((const char *)(dgettext("pidgin","Use status from last _exit at startup"))),"/purple/savedstatus/startup_current_status",vbox);
  gtk_size_group_add_widget(sg,button);
/* TODO: Show something useful if we don't have any saved statuses. */
  menu = pidgin_status_menu(purple_savedstatus_get_startup(),((GCallback )set_startupstatus));
  gtk_size_group_add_widget(sg,menu);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )pidgin_toggle_sensitive),menu,0,((GConnectFlags )0));
  pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),((const char *)(dgettext("pidgin","Status to a_pply at startup:"))),sg,menu,(!0),&label);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )pidgin_toggle_sensitive),label,0,((GConnectFlags )0));
  if (purple_prefs_get_bool("/purple/savedstatus/startup_current_status") != 0) {
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_widget_get_type()))),0);
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_widget_get_type()))),0);
  }
  gtk_widget_show_all(ret);
  g_object_unref(sg);
  return ret;
}

static int prefs_notebook_add_page(const char *text,GtkWidget *page,int ind)
{
  return gtk_notebook_append_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)prefsnotebook),gtk_notebook_get_type()))),page,gtk_label_new(text));
}

static void prefs_notebook_init()
{
  prefs_notebook_add_page(((const char *)(dgettext("pidgin","Interface"))),interface_page(),notebook_page++);
#ifndef _WIN32
/* We use the registered default browser in windows */
/* if the user is running Mac OS X, hide the browsers tab */
  if (purple_running_osx() == 0) 
    prefs_notebook_add_page(((const char *)(dgettext("pidgin","Browser"))),browser_page(),notebook_page++);
#endif
  prefs_notebook_add_page(((const char *)(dgettext("pidgin","Conversations"))),conv_page(),notebook_page++);
  prefs_notebook_add_page(((const char *)(dgettext("pidgin","Logging"))),logging_page(),notebook_page++);
  prefs_notebook_add_page(((const char *)(dgettext("pidgin","Network"))),network_page(),notebook_page++);
  prefs_notebook_add_page(((const char *)(dgettext("pidgin","Proxy"))),proxy_page(),notebook_page++);
  prefs_notebook_add_page(((const char *)(dgettext("pidgin","Sounds"))),sound_page(),notebook_page++);
  prefs_notebook_add_page(((const char *)(dgettext("pidgin","Status / Idle"))),away_page(),notebook_page++);
  prefs_notebook_add_page(((const char *)(dgettext("pidgin","Themes"))),theme_page(),notebook_page++);
}

void pidgin_prefs_show()
{
  GtkWidget *vbox;
  GtkWidget *notebook;
  GtkWidget *button;
  if (prefs != 0) {
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)prefs),gtk_window_get_type()))));
    return ;
  }
/* copy the preferences to tmp values...
	 * I liked "take affect immediately" Oh well :-( */
/* (that should have been "effect," right?) */
/* Back to instant-apply! I win!  BU-HAHAHA! */
/* Create the window */
  prefs = pidgin_create_dialog(((const char *)(dgettext("pidgin","Preferences"))),12,"preferences",0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)prefs),((GType )(20 << 2))))),"destroy",((GCallback )delete_prefs),0,0,((GConnectFlags )0));
  vbox = pidgin_dialog_get_vbox_with_properties(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)prefs),gtk_dialog_get_type()))),0,12);
/* The notebook */
  prefsnotebook = (notebook = gtk_notebook_new());
  gtk_notebook_set_tab_pos(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)notebook),gtk_notebook_get_type()))),GTK_POS_LEFT);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),notebook,0,0,0);
  gtk_widget_show(prefsnotebook);
  button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)prefs),gtk_dialog_get_type()))),"gtk-close",0,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )gtk_widget_destroy),prefs,0,G_CONNECT_SWAPPED);
  prefs_notebook_init();
/* Refresh the list of themes before showing the preferences window */
  prefs_themes_refresh();
/* Show everything. */
  gtk_widget_show(prefs);
}

static void set_bool_pref(GtkWidget *w,const char *key)
{
  purple_prefs_set_bool(key,gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)w),gtk_toggle_button_get_type())))));
}

GtkWidget *pidgin_prefs_checkbox(const char *text,const char *key,GtkWidget *page)
{
  GtkWidget *button;
  button = gtk_check_button_new_with_mnemonic(text);
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_toggle_button_get_type()))),purple_prefs_get_bool(key));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)page),gtk_box_get_type()))),button,0,0,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )set_bool_pref),((char *)key),0,((GConnectFlags )0));
  gtk_widget_show(button);
  return button;
}

static void smiley_theme_pref_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  const char *themename = value;
  GSList *themes;
{
    for (themes = smiley_themes; themes != 0; themes = (themes -> next)) {
      struct smiley_theme *smile = (themes -> data);
      if (((smile -> name) != 0) && (strcmp(themename,(smile -> name)) == 0)) {
        pidgin_themes_load_smiley_theme((smile -> path),(!0));
        break; 
      }
    }
  }
}

void pidgin_prefs_init()
{
  purple_prefs_add_none("/pidgin");
  purple_prefs_add_none("/plugins/gtk");
#ifndef _WIN32
/* Browsers */
  purple_prefs_add_none("/pidgin/browsers");
  purple_prefs_add_int("/pidgin/browsers/place",PIDGIN_BROWSER_DEFAULT);
  purple_prefs_add_string("/pidgin/browsers/manual_command","");
  purple_prefs_add_string("/pidgin/browsers/browser","xdg-open");
#endif
/* Plugins */
  purple_prefs_add_none("/pidgin/plugins");
  purple_prefs_add_path_list("/pidgin/plugins/loaded",0);
/* File locations */
  purple_prefs_add_none("/pidgin/filelocations");
  purple_prefs_add_path("/pidgin/filelocations/last_save_folder","");
  purple_prefs_add_path("/pidgin/filelocations/last_open_folder","");
  purple_prefs_add_path("/pidgin/filelocations/last_icon_folder","");
/* Themes */
  prefs_themes_init();
/* Smiley Themes */
  purple_prefs_add_none("/pidgin/smileys");
  purple_prefs_add_string("/pidgin/smileys/theme","Default");
/* Smiley Callbacks */
  purple_prefs_connect_callback((&prefs),"/pidgin/smileys/theme",smiley_theme_pref_cb,0);
  pidgin_prefs_update_old();
}

void pidgin_prefs_update_old()
{
  const char *str = (const char *)((void *)0);
  purple_prefs_rename("/gaim/gtk","/pidgin");
/* Rename some old prefs */
  purple_prefs_rename("/pidgin/logging/log_ims","/purple/logging/log_ims");
  purple_prefs_rename("/pidgin/logging/log_chats","/purple/logging/log_chats");
  purple_prefs_rename("/purple/conversations/placement","/pidgin/conversations/placement");
  purple_prefs_rename("/pidgin/debug/timestamps","/purple/debug/timestamps");
  purple_prefs_rename("/pidgin/conversations/im/raise_on_events","/plugins/gtk/X11/notify/method_raise");
  purple_prefs_rename_boolean_toggle("/pidgin/conversations/ignore_colors","/pidgin/conversations/show_incoming_formatting");
/*
	 * this path pref changed to a string, so migrate.  I know this will break
	 * things for and confuse users that use multiple versions with the same
	 * config directory, but I'm not inclined to want to deal with that at the
	 * moment. -- rekkanoryo
	 */
  if ((str = purple_prefs_get_path("/pidgin/browsers/command")) != ((const char *)((void *)0))) {
    purple_prefs_set_string("/pidgin/browsers/manual_command",str);
    purple_prefs_remove("/pidgin/browsers/command");
  }
/* this string pref moved into the core, try to be friendly */
  purple_prefs_rename("/pidgin/idle/reporting_method","/purple/away/idle_reporting");
  if (((str = purple_prefs_get_string("/purple/away/idle_reporting")) != 0) && (strcmp(str,"gaim") == 0)) 
    purple_prefs_set_string("/purple/away/idle_reporting","purple");
/* Remove some no-longer-used prefs */
  purple_prefs_remove("/pidgin/blist/auto_expand_contacts");
  purple_prefs_remove("/pidgin/blist/button_style");
  purple_prefs_remove("/pidgin/blist/grey_idle_buddies");
  purple_prefs_remove("/pidgin/blist/raise_on_events");
  purple_prefs_remove("/pidgin/blist/show_group_count");
  purple_prefs_remove("/pidgin/blist/show_warning_level");
  purple_prefs_remove("/pidgin/conversations/button_type");
  purple_prefs_remove("/pidgin/conversations/ctrl_enter_sends");
  purple_prefs_remove("/pidgin/conversations/enter_sends");
  purple_prefs_remove("/pidgin/conversations/escape_closes");
  purple_prefs_remove("/pidgin/conversations/html_shortcuts");
  purple_prefs_remove("/pidgin/conversations/icons_on_tabs");
  purple_prefs_remove("/pidgin/conversations/send_formatting");
  purple_prefs_remove("/pidgin/conversations/show_smileys");
  purple_prefs_remove("/pidgin/conversations/show_urls_as_links");
  purple_prefs_remove("/pidgin/conversations/smiley_shortcuts");
  purple_prefs_remove("/pidgin/conversations/use_custom_bgcolor");
  purple_prefs_remove("/pidgin/conversations/use_custom_fgcolor");
  purple_prefs_remove("/pidgin/conversations/use_custom_font");
  purple_prefs_remove("/pidgin/conversations/use_custom_size");
  purple_prefs_remove("/pidgin/conversations/chat/old_tab_complete");
  purple_prefs_remove("/pidgin/conversations/chat/tab_completion");
  purple_prefs_remove("/pidgin/conversations/im/hide_on_send");
  purple_prefs_remove("/pidgin/conversations/chat/color_nicks");
  purple_prefs_remove("/pidgin/conversations/chat/raise_on_events");
  purple_prefs_remove("/pidgin/conversations/ignore_fonts");
  purple_prefs_remove("/pidgin/conversations/ignore_font_sizes");
  purple_prefs_remove("/pidgin/conversations/passthrough_unknown_commands");
  purple_prefs_remove("/pidgin/idle");
  purple_prefs_remove("/pidgin/logging/individual_logs");
  purple_prefs_remove("/pidgin/sound/signon");
  purple_prefs_remove("/pidgin/sound/silent_signon");
/* Convert old queuing prefs to hide_new 3-way pref. */
  if ((purple_prefs_exists("/plugins/gtk/docklet/queue_messages") != 0) && (purple_prefs_get_bool("/plugins/gtk/docklet/queue_messages") != 0)) {
    purple_prefs_set_string("/pidgin/conversations/im/hide_new","always");
  }
  else if ((purple_prefs_exists("/pidgin/away/queue_messages") != 0) && (purple_prefs_get_bool("/pidgin/away/queue_messages") != 0)) {
    purple_prefs_set_string("/pidgin/conversations/im/hide_new","away");
  }
  purple_prefs_remove("/pidgin/away/queue_messages");
  purple_prefs_remove("/pidgin/away");
  purple_prefs_remove("/plugins/gtk/docklet/queue_messages");
  purple_prefs_remove("/pidgin/conversations/chat/default_width");
  purple_prefs_remove("/pidgin/conversations/chat/default_height");
  purple_prefs_remove("/pidgin/conversations/im/default_width");
  purple_prefs_remove("/pidgin/conversations/im/default_height");
  purple_prefs_rename("/pidgin/conversations/x","/pidgin/conversations/im/x");
  purple_prefs_rename("/pidgin/conversations/y","/pidgin/conversations/im/y");
}
