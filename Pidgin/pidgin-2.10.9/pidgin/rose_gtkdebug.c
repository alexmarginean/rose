/**
 * @file gtkdebug.c GTK+ Debug API
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "pidgin.h"
#include "notify.h"
#include "prefs.h"
#include "request.h"
#include "util.h"
#include "gtkdebug.h"
#include "gtkdialogs.h"
#include "gtkimhtml.h"
#include "gtkutils.h"
#include "pidginstock.h"
#ifdef HAVE_REGEX_H
# include <regex.h>
#	define USE_REGEX 1
#else
#if GLIB_CHECK_VERSION(2,14,0)
#	define USE_REGEX 1
#endif
#endif /* HAVE_REGEX_H */
#include <gdk/gdkkeysyms.h>
typedef struct __unnamed_class___F0_L51_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__text__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1172R__Pe___variable_name_unknown_scope_and_name__scope__store__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__paused__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__filter__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__expression__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__invert__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__highlight__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_guintUi__typedef_declaration_variable_name_unknown_scope_and_name__scope__timer__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L1607R_variable_name_unknown_scope_and_name__scope__regex__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__filterlevel {
GtkWidget *window;
GtkWidget *text;
GtkListStore *store;
gboolean paused;
#ifdef USE_REGEX
GtkWidget *filter;
GtkWidget *expression;
gboolean invert;
gboolean highlight;
guint timer;
#	ifdef HAVE_REGEX_H
regex_t regex;
#	else
#	endif /* HAVE_REGEX_H */
#else
#endif /* USE_REGEX */
GtkWidget *filterlevel;}DebugWindow;
static const char debug_fg_colors[][8UL] = {("#000000"), ("#666666"), ("#000000"), ("#660000"), ("#FF0000"), ("#FF0000")
/**< All debug levels. */
/**< Misc.             */
/**< Information.      */
/**< Warnings.         */
/**< Errors.           */
/**< Fatal errors.     */
};
static DebugWindow *debug_win = (DebugWindow *)((void *)0);
static guint debug_enabled_timer = 0;
#ifdef USE_REGEX
static void regex_filter_all(DebugWindow *win);
static void regex_show_all(DebugWindow *win);
#endif /* USE_REGEX */

static gint debug_window_destroy(GtkWidget *w,GdkEvent *event,void *unused)
{
  purple_prefs_disconnect_by_handle(pidgin_debug_get_handle());
#ifdef USE_REGEX
  if ((debug_win -> timer) != 0) {
    const gchar *text;
    purple_timeout_remove((debug_win -> timer));
    text = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(debug_win -> expression)),gtk_entry_get_type()))));
    purple_prefs_set_string("/pidgin/debug/regex",text);
  }
#ifdef HAVE_REGEX_H
  regfree(&debug_win -> regex);
#else
#endif /* HAVE_REGEX_H */
#endif /* USE_REGEX */
/* If the "Save Log" dialog is open then close it */
  purple_request_close_with_handle(debug_win);
  g_free(debug_win);
  debug_win = ((DebugWindow *)((void *)0));
  purple_prefs_set_bool("/pidgin/debug/enabled",0);
  return 0;
}

static gboolean configure_cb(GtkWidget *w,GdkEventConfigure *event,DebugWindow *win)
{
  if ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)w),gtk_object_get_type())))).flags & GTK_VISIBLE) != 0) {
    purple_prefs_set_int("/pidgin/debug/width",(event -> width));
    purple_prefs_set_int("/pidgin/debug/height",(event -> height));
  }
  return 0;
}
#ifndef USE_REGEX
#endif /* USE_REGEX */

static void save_writefile_cb(void *user_data,const char *filename)
{
  DebugWindow *win = (DebugWindow *)user_data;
  FILE *fp;
  char *tmp;
  if ((fp = fopen(filename,"w+")) == ((FILE *)((void *)0))) {
    purple_notify_message(win,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Unable to open file."))),0,0,0);
    return ;
  }
  tmp = gtk_imhtml_get_text(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(win -> text)),gtk_imhtml_get_type()))),0,0);
  fprintf(fp,"Pidgin Debug Log : %s\n",purple_date_format_full(0));
  fprintf(fp,"%s",tmp);
  g_free(tmp);
  fclose(fp);
}

static void save_cb(GtkWidget *w,DebugWindow *win)
{
  purple_request_file(win,((const char *)(dgettext("pidgin","Save Debug Log"))),"purple-debug.log",(!0),((GCallback )save_writefile_cb),0,0,0,0,win);
}

static void clear_cb(GtkWidget *w,DebugWindow *win)
{
  gtk_imhtml_delete(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(win -> text)),gtk_imhtml_get_type()))),0,0);
#ifdef USE_REGEX
  gtk_list_store_clear((win -> store));
#endif /* USE_REGEX */
}

static void pause_cb(GtkWidget *w,DebugWindow *win)
{
  win -> paused = gtk_toggle_tool_button_get_active(((GtkToggleToolButton *)(g_type_check_instance_cast(((GTypeInstance *)w),gtk_toggle_tool_button_get_type()))));
#ifdef USE_REGEX
  if (!((win -> paused) != 0)) {
    if (gtk_toggle_tool_button_get_active(((GtkToggleToolButton *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filter)),gtk_toggle_tool_button_get_type())))) != 0) 
      regex_filter_all(win);
    else 
      regex_show_all(win);
  }
#endif /* USE_REGEX */
}
/******************************************************************************
 * regex stuff
 *****************************************************************************/
#ifdef USE_REGEX

static void regex_clear_color(GtkWidget *w)
{
  gtk_widget_modify_base(w,GTK_STATE_NORMAL,0);
}

static void regex_change_color(GtkWidget *w,guint16 r,guint16 g,guint16 b)
{
  GdkColor color;
  color.red = r;
  color.green = g;
  color.blue = b;
  gtk_widget_modify_base(w,GTK_STATE_NORMAL,(&color));
}

static void regex_highlight_clear(DebugWindow *win)
{
  GtkIMHtml *imhtml = (GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(win -> text)),gtk_imhtml_get_type()));
  GtkTextIter s;
  GtkTextIter e;
  gtk_text_buffer_get_start_iter((imhtml -> text_buffer),&s);
  gtk_text_buffer_get_end_iter((imhtml -> text_buffer),&e);
  gtk_text_buffer_remove_tag_by_name((imhtml -> text_buffer),"regex",(&s),(&e));
}

static void regex_match(DebugWindow *win,const gchar *text)
{
  GtkIMHtml *imhtml = (GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(win -> text)),gtk_imhtml_get_type()));
#ifdef HAVE_REGEX_H
/* adjust if necessary */
  regmatch_t matches[4UL];
  size_t n_matches = (sizeof(matches) / sizeof(matches[0]));
  gint inverted;
#else
#endif /* HAVE_REGEX_H */
  gchar *plaintext;
  if (!(text != 0)) 
    return ;
/* I don't like having to do this, but we need it for highlighting.  Plus
	 * it makes the ^ and $ operators work :)
	 */
  plaintext = purple_markup_strip_html(text);
/* we do a first pass to see if it matches at all.  If it does we append
	 * it, and work out the offsets to highlight.
	 */
#ifdef HAVE_REGEX_H
  inverted = (((win -> invert) != 0)?REG_NOMATCH : 0);
  if (regexec((&win -> regex),plaintext,n_matches,matches,0) == inverted) {
#else
#endif /* HAVE_REGEX_H */
    gchar *p = plaintext;
    GtkTextIter ins;
    gint i;
    gint offset = 0;
    gtk_text_buffer_get_iter_at_mark((imhtml -> text_buffer),&ins,gtk_text_buffer_get_insert((imhtml -> text_buffer)));
    i = gtk_text_iter_get_offset((&ins));
    gtk_imhtml_append_text_with_images(imhtml,text,0,0);
/* If we're not highlighting or the expression is inverted, we're
		 * done and move on.
		 */
    if (!((win -> highlight) != 0) || ((win -> invert) != 0)) {
      g_free(plaintext);
#ifndef HAVE_REGEX_H
#endif
      return ;
    }
/* we use a do-while to highlight the first match, and then continue
		 * if necessary...
		 */
#ifdef HAVE_REGEX_H
    do {
      size_t m;
{
        for (m = 0; m < n_matches; m++) {
          GtkTextIter ms;
          GtkTextIter me;
          if (matches[m].rm_eo == -1) 
            break; 
          i += offset;
          gtk_text_buffer_get_iter_at_offset((imhtml -> text_buffer),&ms,(i + matches[m].rm_so));
          gtk_text_buffer_get_iter_at_offset((imhtml -> text_buffer),&me,(i + matches[m].rm_eo));
          gtk_text_buffer_apply_tag_by_name((imhtml -> text_buffer),"regex",(&ms),(&me));
          offset = matches[m].rm_eo;
        }
      }
      p += offset;
    }while (regexec((&win -> regex),p,n_matches,matches,1) == inverted);
#else
#endif /* HAVE_REGEX_H */
  }
  g_free(plaintext);
}

static gboolean regex_filter_all_cb(GtkTreeModel *m,GtkTreePath *p,GtkTreeIter *iter,gpointer data)
{
  DebugWindow *win = (DebugWindow *)data;
  gchar *text;
  PurpleDebugLevel level;
  gtk_tree_model_get(m,iter,0,&text,1,&level,-1);
  if (level >= (purple_prefs_get_int("/pidgin/debug/filterlevel"))) 
    regex_match(win,text);
  g_free(text);
  return 0;
}

static void regex_filter_all(DebugWindow *win)
{
  gtk_imhtml_delete(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(win -> text)),gtk_imhtml_get_type()))),0,0);
  if ((win -> highlight) != 0) 
    regex_highlight_clear(win);
  gtk_tree_model_foreach(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(win -> store)),gtk_tree_model_get_type()))),regex_filter_all_cb,win);
}

static gboolean regex_show_all_cb(GtkTreeModel *m,GtkTreePath *p,GtkTreeIter *iter,gpointer data)
{
  DebugWindow *win = (DebugWindow *)data;
  gchar *text;
  PurpleDebugLevel level;
  gtk_tree_model_get(m,iter,0,&text,1,&level,-1);
  if (level >= (purple_prefs_get_int("/pidgin/debug/filterlevel"))) 
    gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(win -> text)),gtk_imhtml_get_type()))),text,0,0);
  g_free(text);
  return 0;
}

static void regex_show_all(DebugWindow *win)
{
  gtk_imhtml_delete(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(win -> text)),gtk_imhtml_get_type()))),0,0);
  if ((win -> highlight) != 0) 
    regex_highlight_clear(win);
  gtk_tree_model_foreach(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(win -> store)),gtk_tree_model_get_type()))),regex_show_all_cb,win);
}

static void regex_compile(DebugWindow *win)
{
  const gchar *text;
  text = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(win -> expression)),gtk_entry_get_type()))));
  if ((text == ((const gchar *)((void *)0))) || (( *text) == 0)) {
    regex_clear_color((win -> expression));
    gtk_widget_set_sensitive((win -> filter),0);
    return ;
  }
#ifdef HAVE_REGEX_H
  regfree(&win -> regex);
  if (regcomp(&win -> regex,text,1 | 1 << 1) != 0) {
#else
#endif
/* failed to compile */
    regex_change_color((win -> expression),0xFFFF,0xAFFF,0xAFFF);
    gtk_widget_set_sensitive((win -> filter),0);
  }
  else {
/* compiled successfully */
    regex_change_color((win -> expression),0xAFFF,0xFFFF,0xAFFF);
    gtk_widget_set_sensitive((win -> filter),(!0));
  }
/* we check if the filter is on in case it was only of the options that
	 * got changed, and not the expression.
	 */
  if (gtk_toggle_tool_button_get_active(((GtkToggleToolButton *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filter)),gtk_toggle_tool_button_get_type())))) != 0) 
    regex_filter_all(win);
}

static void regex_pref_filter_cb(const gchar *name,PurplePrefType type,gconstpointer val,gpointer data)
{
  DebugWindow *win = (DebugWindow *)data;
  gboolean active = (gint )((glong )val);
  gboolean current;
  if (!(win != 0) || !((win -> window) != 0)) 
    return ;
  current = gtk_toggle_tool_button_get_active(((GtkToggleToolButton *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filter)),gtk_toggle_tool_button_get_type()))));
  if (active != current) 
    gtk_toggle_tool_button_set_active(((GtkToggleToolButton *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filter)),gtk_toggle_tool_button_get_type()))),active);
}

static void regex_pref_expression_cb(const gchar *name,PurplePrefType type,gconstpointer val,gpointer data)
{
  DebugWindow *win = (DebugWindow *)data;
  const gchar *exp = (const gchar *)val;
  gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(win -> expression)),gtk_entry_get_type()))),exp);
}

static void regex_pref_invert_cb(const gchar *name,PurplePrefType type,gconstpointer val,gpointer data)
{
  DebugWindow *win = (DebugWindow *)data;
  gboolean active = (gint )((glong )val);
  win -> invert = active;
  if (gtk_toggle_tool_button_get_active(((GtkToggleToolButton *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filter)),gtk_toggle_tool_button_get_type())))) != 0) 
    regex_filter_all(win);
}

static void regex_pref_highlight_cb(const gchar *name,PurplePrefType type,gconstpointer val,gpointer data)
{
  DebugWindow *win = (DebugWindow *)data;
  gboolean active = (gint )((glong )val);
  win -> highlight = active;
  if (gtk_toggle_tool_button_get_active(((GtkToggleToolButton *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filter)),gtk_toggle_tool_button_get_type())))) != 0) 
    regex_filter_all(win);
}

static void regex_row_changed_cb(GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter,DebugWindow *win)
{
  gchar *text;
  PurpleDebugLevel level;
  if (!(win != 0) || !((win -> window) != 0)) 
    return ;
/* If the debug window is paused, we just return since it's in the store.
	 * We don't call regex_match because it doesn't make sense to check the
	 * string if it's paused.  When we unpause we clear the imhtml and
	 * reiterate over the store to handle matches that were outputted when
	 * we were paused.
	 */
  if ((win -> paused) != 0) 
    return ;
  gtk_tree_model_get(model,iter,0,&text,1,&level,-1);
  if (level >= (purple_prefs_get_int("/pidgin/debug/filterlevel"))) {
    if (gtk_toggle_tool_button_get_active(((GtkToggleToolButton *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filter)),gtk_toggle_tool_button_get_type())))) != 0) {
      regex_match(win,text);
    }
    else {
      gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(win -> text)),gtk_imhtml_get_type()))),text,0,0);
    }
  }
  g_free(text);
}

static gboolean regex_timer_cb(DebugWindow *win)
{
  const gchar *text;
  text = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(win -> expression)),gtk_entry_get_type()))));
  purple_prefs_set_string("/pidgin/debug/regex",text);
  win -> timer = 0;
  return 0;
}

static void regex_changed_cb(GtkWidget *w,DebugWindow *win)
{
  if (gtk_toggle_tool_button_get_active(((GtkToggleToolButton *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filter)),gtk_toggle_tool_button_get_type())))) != 0) {
    gtk_toggle_tool_button_set_active(((GtkToggleToolButton *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filter)),gtk_toggle_tool_button_get_type()))),0);
  }
  if ((win -> timer) == 0) 
    win -> timer = purple_timeout_add_seconds(5,((GSourceFunc )regex_timer_cb),win);
  regex_compile(win);
}

static void regex_key_release_cb(GtkWidget *w,GdkEventKey *e,DebugWindow *win)
{
  if ((((e -> keyval) == 0xff0d) && (((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filter)),gtk_object_get_type())))).flags & GTK_SENSITIVE) != 0) && ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filter)),gtk_object_get_type())))).flags & GTK_PARENT_SENSITIVE) != 0))) && !(gtk_toggle_tool_button_get_active(((GtkToggleToolButton *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filter)),gtk_toggle_tool_button_get_type())))) != 0)) {
    gtk_toggle_tool_button_set_active(((GtkToggleToolButton *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filter)),gtk_toggle_tool_button_get_type()))),(!0));
  }
}

static void regex_menu_cb(GtkWidget *item,const gchar *pref)
{
  gboolean active;
  active = gtk_check_menu_item_get_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_check_menu_item_get_type()))));
  purple_prefs_set_bool(pref,active);
}

static void regex_popup_cb(GtkEntry *entry,GtkWidget *menu,DebugWindow *win)
{
  pidgin_separator(menu);
  pidgin_new_check_item(menu,((const char *)(dgettext("pidgin","Invert"))),((GCallback )regex_menu_cb),"/pidgin/debug/invert",(win -> invert));
  pidgin_new_check_item(menu,((const char *)(dgettext("pidgin","Highlight matches"))),((GCallback )regex_menu_cb),"/pidgin/debug/highlight",(win -> highlight));
}

static void regex_filter_toggled_cb(GtkToggleToolButton *button,DebugWindow *win)
{
  gboolean active;
  active = gtk_toggle_tool_button_get_active(button);
  purple_prefs_set_bool("/pidgin/debug/filter",active);
  if (!((({
    GTypeInstance *__inst = (GTypeInstance *)(win -> text);
    GType __t = gtk_imhtml_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) 
    return ;
  if (active != 0) 
    regex_filter_all(win);
  else 
    regex_show_all(win);
}

static void filter_level_pref_changed(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  DebugWindow *win = data;
  if (((gint )((glong )value)) != gtk_combo_box_get_active(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filterlevel)),gtk_combo_box_get_type()))))) 
    gtk_combo_box_set_active(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filterlevel)),gtk_combo_box_get_type()))),((gint )((glong )value)));
  if (gtk_toggle_tool_button_get_active(((GtkToggleToolButton *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filter)),gtk_toggle_tool_button_get_type())))) != 0) 
    regex_filter_all(win);
  else 
    regex_show_all(win);
}
#endif /* USE_REGEX */

static void filter_level_changed_cb(GtkWidget *combo,gpointer null)
{
  purple_prefs_set_int("/pidgin/debug/filterlevel",gtk_combo_box_get_active(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gtk_combo_box_get_type())))));
}

static void toolbar_style_pref_changed_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  gtk_toolbar_set_style(((GtkToolbar *)(g_type_check_instance_cast(((GTypeInstance *)data),gtk_toolbar_get_type()))),((gint )((glong )value)));
}

static void toolbar_icon_pref_changed(GtkWidget *item,GtkWidget *toolbar)
{
  int style = (gint )((glong )(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"user_data")));
  purple_prefs_set_int("/pidgin/debug/style",style);
}

static gboolean toolbar_context(GtkWidget *toolbar,GdkEventButton *event,gpointer null)
{
  GtkWidget *menu;
  GtkWidget *item;
  const char *text[3UL];
  GtkToolbarStyle value[3UL];
  int i;
  if (!(((event -> button) == 3) && ((event -> type) == GDK_BUTTON_PRESS))) 
    return 0;
  text[0] = ((const char *)(dgettext("pidgin","_Icon Only")));
  value[0] = GTK_TOOLBAR_ICONS;
  text[1] = ((const char *)(dgettext("pidgin","_Text Only")));
  value[1] = GTK_TOOLBAR_TEXT;
  text[2] = ((const char *)(dgettext("pidgin","_Both Icon & Text")));
  value[2] = GTK_TOOLBAR_BOTH_HORIZ;
  menu = gtk_menu_new();
  for (i = 0; i < 3; i++) {
    item = gtk_check_menu_item_new_with_mnemonic(text[i]);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"user_data",((gpointer )((glong )value[i])));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"activate",((GCallback )toolbar_icon_pref_changed),toolbar,0,((GConnectFlags )0));
    if (value[i] == (purple_prefs_get_int("/pidgin/debug/style"))) 
      gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_check_menu_item_get_type()))),(!0));
    gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item);
  }
  gtk_widget_show_all(menu);
  gtk_menu_popup(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_get_type()))),0,0,0,0,0,gtk_get_current_event_time());
  return 0;
}

static DebugWindow *debug_window_new()
{
  DebugWindow *win;
  GtkWidget *vbox;
  GtkWidget *toolbar;
  GtkWidget *frame;
  gint width;
  gint height;
  void *handle;
  GtkToolItem *item;
#if !GTK_CHECK_VERSION(2,12,0)
#endif
  win = ((DebugWindow *)(g_malloc0_n(1,(sizeof(DebugWindow )))));
  width = purple_prefs_get_int("/pidgin/debug/width");
  height = purple_prefs_get_int("/pidgin/debug/height");
  win -> window = pidgin_create_window(((const char *)(dgettext("pidgin","Debug Window"))),0,"debug",(!0));
  purple_debug_info("gtkdebug","Setting dimensions to %d, %d\n",width,height);
  gtk_window_set_default_size(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),gtk_window_get_type()))),width,height);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),"delete_event",((GCallback )debug_window_destroy),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),"configure_event",((GCallback )configure_cb),win,0,((GConnectFlags )0));
  handle = pidgin_debug_get_handle();
#ifdef USE_REGEX
/* the list store for all the messages */
  win -> store = gtk_list_store_new(2,((GType )(16 << 2)),((GType )(6 << 2)));
/* row-changed gets called when we do gtk_list_store_set, and row-inserted
	 * gets called with gtk_list_store_append, which is a
	 * completely empty row. So we just ignore row-inserted, and deal with row
	 * changed. -Gary
	 */
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> store)),((GType )(20 << 2))))),"row-changed",((GCallback )regex_row_changed_cb),win,0,((GConnectFlags )0));
#endif /* USE_REGEX */
/* Setup the vbox */
  vbox = gtk_vbox_new(0,0);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),gtk_container_get_type()))),vbox);
  if (purple_prefs_get_bool("/pidgin/debug/toolbar") != 0) {
/* Setup our top button bar thingie. */
    toolbar = gtk_toolbar_new();
#if !GTK_CHECK_VERSION(2,12,0)
#endif
#if !GTK_CHECK_VERSION(2,14,0)
#endif
    gtk_toolbar_set_show_arrow(((GtkToolbar *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_toolbar_get_type()))),(!0));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),((GType )(20 << 2))))),"button-press-event",((GCallback )toolbar_context),win,0,((GConnectFlags )0));
    gtk_toolbar_set_style(((GtkToolbar *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_toolbar_get_type()))),(purple_prefs_get_int("/pidgin/debug/style")));
    purple_prefs_connect_callback(handle,"/pidgin/debug/style",toolbar_style_pref_changed_cb,toolbar);
    gtk_toolbar_set_icon_size(((GtkToolbar *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_toolbar_get_type()))),GTK_ICON_SIZE_SMALL_TOOLBAR);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),toolbar,0,0,0);
#ifndef USE_REGEX
/* Find button */
#if GTK_CHECK_VERSION(2,12,0)
#else
#endif
#endif /* USE_REGEX */
/* Save */
    item = gtk_tool_button_new_from_stock("gtk-save");
    gtk_tool_item_set_is_important(item,(!0));
#if GTK_CHECK_VERSION(2,12,0)
    gtk_tool_item_set_tooltip_text(item,((const char *)(dgettext("pidgin","Save"))));
#else
#endif
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"clicked",((GCallback )save_cb),win,0,((GConnectFlags )0));
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_container_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_widget_get_type()))));
/* Clear button */
    item = gtk_tool_button_new_from_stock("gtk-clear");
    gtk_tool_item_set_is_important(item,(!0));
#if GTK_CHECK_VERSION(2,12,0)
    gtk_tool_item_set_tooltip_text(item,((const char *)(dgettext("pidgin","Clear"))));
#else
#endif
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"clicked",((GCallback )clear_cb),win,0,((GConnectFlags )0));
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_container_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_widget_get_type()))));
    item = gtk_separator_tool_item_new();
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_container_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_widget_get_type()))));
/* Pause */
    item = gtk_toggle_tool_button_new_from_stock("pidgin-pause");
    gtk_tool_item_set_is_important(item,(!0));
#if GTK_CHECK_VERSION(2,12,0)
    gtk_tool_item_set_tooltip_text(item,((const char *)(dgettext("pidgin","Pause"))));
#else
#endif
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"clicked",((GCallback )pause_cb),win,0,((GConnectFlags )0));
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_container_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_widget_get_type()))));
#ifdef USE_REGEX
/* regex stuff */
    item = gtk_separator_tool_item_new();
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_container_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_widget_get_type()))));
/* regex toggle button */
    item = gtk_toggle_tool_button_new_from_stock("gtk-find");
    gtk_tool_item_set_is_important(item,(!0));
    win -> filter = ((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_widget_get_type())));
    gtk_tool_button_set_label(((GtkToolButton *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filter)),gtk_tool_button_get_type()))),((const char *)(dgettext("pidgin","Filter"))));
#if GTK_CHECK_VERSION(2,12,0)
    gtk_tool_item_set_tooltip_text(((GtkToolItem *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filter)),gtk_tool_item_get_type()))),((const char *)(dgettext("pidgin","Filter"))));
#else
#endif
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filter)),((GType )(20 << 2))))),"clicked",((GCallback )regex_filter_toggled_cb),win,0,((GConnectFlags )0));
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_container_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filter)),gtk_widget_get_type()))));
/* we purposely disable the toggle button here in case
		 * /purple/gtk/debug/expression has an empty string.  If it does not have
		 * an empty string, the change signal will get called and make the
		 * toggle button sensitive.
		 */
    gtk_widget_set_sensitive((win -> filter),0);
    gtk_toggle_tool_button_set_active(((GtkToggleToolButton *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filter)),gtk_toggle_tool_button_get_type()))),purple_prefs_get_bool("/pidgin/debug/filter"));
    purple_prefs_connect_callback(handle,"/pidgin/debug/filter",regex_pref_filter_cb,win);
/* regex entry */
    win -> expression = gtk_entry_new();
    item = gtk_tool_item_new();
#if GTK_CHECK_VERSION(2,12,0)
    gtk_widget_set_tooltip_text((win -> expression),((const char *)(dgettext("pidgin","Right click for more options."))));
#else
#endif
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_container_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(win -> expression)),gtk_widget_get_type()))));
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_container_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_widget_get_type()))));
/* this needs to be before the text is set from the pref if we want it
		 * to colorize a stored expression.
		 */
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> expression)),((GType )(20 << 2))))),"changed",((GCallback )regex_changed_cb),win,0,((GConnectFlags )0));
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(win -> expression)),gtk_entry_get_type()))),purple_prefs_get_string("/pidgin/debug/regex"));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> expression)),((GType )(20 << 2))))),"populate-popup",((GCallback )regex_popup_cb),win,0,((GConnectFlags )0));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> expression)),((GType )(20 << 2))))),"key-release-event",((GCallback )regex_key_release_cb),win,0,((GConnectFlags )0));
    purple_prefs_connect_callback(handle,"/pidgin/debug/regex",regex_pref_expression_cb,win);
/* connect the rest of our pref callbacks */
    win -> invert = purple_prefs_get_bool("/pidgin/debug/invert");
    purple_prefs_connect_callback(handle,"/pidgin/debug/invert",regex_pref_invert_cb,win);
    win -> highlight = purple_prefs_get_bool("/pidgin/debug/highlight");
    purple_prefs_connect_callback(handle,"/pidgin/debug/highlight",regex_pref_highlight_cb,win);
#endif /* USE_REGEX */
    item = gtk_separator_tool_item_new();
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_container_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_widget_get_type()))));
    item = gtk_tool_item_new();
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_container_get_type()))),gtk_label_new(((const char *)(dgettext("pidgin","Level ")))));
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_container_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_widget_get_type()))));
    win -> filterlevel = gtk_combo_box_new_text();
    item = gtk_tool_item_new();
#if GTK_CHECK_VERSION(2,12,0)
    gtk_widget_set_tooltip_text((win -> filterlevel),((const char *)(dgettext("pidgin","Select the debug filter level."))));
#else
#endif
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_container_get_type()))),(win -> filterlevel));
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_container_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_widget_get_type()))));
    gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filterlevel)),gtk_combo_box_get_type()))),((const char *)(dgettext("pidgin","All"))));
    gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filterlevel)),gtk_combo_box_get_type()))),((const char *)(dgettext("pidgin","Misc"))));
    gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filterlevel)),gtk_combo_box_get_type()))),((const char *)(dgettext("pidgin","Info"))));
    gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filterlevel)),gtk_combo_box_get_type()))),((const char *)(dgettext("pidgin","Warning"))));
    gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filterlevel)),gtk_combo_box_get_type()))),((const char *)(dgettext("pidgin","Error "))));
    gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filterlevel)),gtk_combo_box_get_type()))),((const char *)(dgettext("pidgin","Fatal Error"))));
    gtk_combo_box_set_active(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filterlevel)),gtk_combo_box_get_type()))),purple_prefs_get_int("/pidgin/debug/filterlevel"));
#ifdef USE_REGEX
    purple_prefs_connect_callback(handle,"/pidgin/debug/filterlevel",filter_level_pref_changed,win);
#endif
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> filterlevel)),((GType )(20 << 2))))),"changed",((GCallback )filter_level_changed_cb),0,0,((GConnectFlags )0));
  }
/* Add the gtkimhtml */
  frame = pidgin_create_imhtml(0,&win -> text,0,0);
  gtk_imhtml_set_format_functions(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(win -> text)),gtk_imhtml_get_type()))),(GTK_IMHTML_ALL ^ GTK_IMHTML_SMILEY ^ GTK_IMHTML_IMAGE));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),frame,(!0),(!0),0);
  gtk_widget_show(frame);
#ifdef USE_REGEX
/* add the tag for regex highlighting */
  gtk_text_buffer_create_tag(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(win -> text)),gtk_imhtml_get_type())))).text_buffer,"regex","background","#FFAFAF","weight","bold",((void *)((void *)0)));
#endif /* USE_REGEX */
  gtk_widget_show_all((win -> window));
  return win;
}

static gboolean debug_enabled_timeout_cb(gpointer data)
{
  debug_enabled_timer = 0;
  if (data != 0) 
    pidgin_debug_window_show();
  else 
    pidgin_debug_window_hide();
  return 0;
}

static void debug_enabled_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  debug_enabled_timer = g_timeout_add(0,debug_enabled_timeout_cb,((gpointer )((glong )((gint )((glong )value)))));
}

static void pidgin_glib_log_handler(const gchar *domain,GLogLevelFlags flags,const gchar *msg,gpointer user_data)
{
  PurpleDebugLevel level;
  char *new_msg = (char *)((void *)0);
  char *new_domain = (char *)((void *)0);
  if ((flags & G_LOG_LEVEL_ERROR) == G_LOG_LEVEL_ERROR) 
    level = PURPLE_DEBUG_ERROR;
  else if ((flags & G_LOG_LEVEL_CRITICAL) == G_LOG_LEVEL_CRITICAL) 
    level = PURPLE_DEBUG_FATAL;
  else if ((flags & G_LOG_LEVEL_WARNING) == G_LOG_LEVEL_WARNING) 
    level = PURPLE_DEBUG_WARNING;
  else if ((flags & G_LOG_LEVEL_MESSAGE) == G_LOG_LEVEL_MESSAGE) 
    level = PURPLE_DEBUG_INFO;
  else if ((flags & G_LOG_LEVEL_INFO) == G_LOG_LEVEL_INFO) 
    level = PURPLE_DEBUG_INFO;
  else if ((flags & G_LOG_LEVEL_DEBUG) == G_LOG_LEVEL_DEBUG) 
    level = PURPLE_DEBUG_MISC;
  else {
    purple_debug_warning("gtkdebug","Unknown glib logging level in %d\n",flags);
/* This will never happen. */
    level = PURPLE_DEBUG_MISC;
  }
  if (msg != ((const gchar *)((void *)0))) 
    new_msg = purple_utf8_try_convert(msg);
  if (domain != ((const gchar *)((void *)0))) 
    new_domain = purple_utf8_try_convert(domain);
  if (new_msg != ((char *)((void *)0))) {
    purple_debug(level,((new_domain != ((char *)((void *)0)))?new_domain : "g_log"),"%s\n",new_msg);
    g_free(new_msg);
  }
  g_free(new_domain);
}
#ifdef _WIN32
#endif

void pidgin_debug_init()
{
/* Debug window preferences. */
/*
	 * NOTE: This must be set before prefs are loaded, and the callbacks
	 *       set after they are loaded, since prefs sets the enabled
	 *       preference here and that loads the window, which calls the
	 *       configure event, which overrides the width and height! :P
	 */
  purple_prefs_add_none("/pidgin/debug");
/* Controls printing to the debug window */
  purple_prefs_add_bool("/pidgin/debug/enabled",0);
  purple_prefs_add_int("/pidgin/debug/filterlevel",PURPLE_DEBUG_ALL);
  purple_prefs_add_int("/pidgin/debug/style",GTK_TOOLBAR_BOTH_HORIZ);
  purple_prefs_add_bool("/pidgin/debug/toolbar",(!0));
  purple_prefs_add_int("/pidgin/debug/width",450);
  purple_prefs_add_int("/pidgin/debug/height",250);
#ifdef USE_REGEX
  purple_prefs_add_string("/pidgin/debug/regex","");
  purple_prefs_add_bool("/pidgin/debug/filter",0);
  purple_prefs_add_bool("/pidgin/debug/invert",0);
  purple_prefs_add_bool("/pidgin/debug/case_insensitive",0);
  purple_prefs_add_bool("/pidgin/debug/highlight",0);
#endif /* USE_REGEX */
  purple_prefs_connect_callback(0,"/pidgin/debug/enabled",debug_enabled_cb,0);
#define REGISTER_G_LOG_HANDLER(name) \
	g_log_set_handler((name), G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL \
					  | G_LOG_FLAG_RECURSION, \
					  pidgin_glib_log_handler, NULL)
/* Register the glib/gtk log handlers. */
  g_log_set_handler(0,(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION),pidgin_glib_log_handler,0);
  g_log_set_handler("Gdk",(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION),pidgin_glib_log_handler,0);
  g_log_set_handler("Gtk",(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION),pidgin_glib_log_handler,0);
  g_log_set_handler("GdkPixbuf",(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION),pidgin_glib_log_handler,0);
  g_log_set_handler("GLib",(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION),pidgin_glib_log_handler,0);
  g_log_set_handler("GModule",(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION),pidgin_glib_log_handler,0);
  g_log_set_handler("GLib-GObject",(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION),pidgin_glib_log_handler,0);
  g_log_set_handler("GThread",(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION),pidgin_glib_log_handler,0);
#ifdef USE_GSTREAMER
  g_log_set_handler("GStreamer",(G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION),pidgin_glib_log_handler,0);
#endif
#ifdef _WIN32
#endif
}

void pidgin_debug_uninit()
{
  purple_debug_set_ui_ops(0);
  if (debug_enabled_timer != 0) 
    g_source_remove(debug_enabled_timer);
}

void pidgin_debug_window_show()
{
  if (debug_win == ((DebugWindow *)((void *)0))) 
    debug_win = debug_window_new();
  gtk_widget_show((debug_win -> window));
  purple_prefs_set_bool("/pidgin/debug/enabled",(!0));
}

void pidgin_debug_window_hide()
{
  if (debug_win != ((DebugWindow *)((void *)0))) {
    gtk_widget_destroy((debug_win -> window));
    debug_window_destroy(0,0,0);
  }
}

static void pidgin_debug_print(PurpleDebugLevel level,const char *category,const char *arg_s)
{
#ifdef USE_REGEX
  GtkTreeIter iter;
#endif /* USE_REGEX */
  gchar *ts_s;
  gchar *esc_s;
  gchar *cat_s;
  gchar *tmp;
  gchar *s;
  const char *mdate;
  time_t mtime;
  if ((debug_win == ((DebugWindow *)((void *)0))) || !(purple_prefs_get_bool("/pidgin/debug/enabled") != 0)) {
    return ;
  }
  mtime = time(0);
  mdate = purple_utf8_strftime("%H:%M:%S",(localtime((&mtime))));
  ts_s = g_strdup_printf("(%s) ",mdate);
  if (category == ((const char *)((void *)0))) 
    cat_s = g_strdup("");
  else 
    cat_s = g_strdup_printf("<b>%s:</b> ",category);
  esc_s = g_markup_escape_text(arg_s,(-1));
  s = g_strdup_printf("<font color=\"%s\">%s%s%s</font>",debug_fg_colors[level],ts_s,cat_s,esc_s);
  g_free(ts_s);
  g_free(cat_s);
  g_free(esc_s);
  tmp = purple_utf8_try_convert(s);
  g_free(s);
  s = tmp;
  if (level == PURPLE_DEBUG_FATAL) {
    tmp = g_strdup_printf("<b>%s</b>",s);
    g_free(s);
    s = tmp;
  }
#ifdef USE_REGEX
/* add the text to the list store */
  gtk_list_store_append((debug_win -> store),&iter);
  gtk_list_store_set((debug_win -> store),&iter,0,s,1,level,-1);
#else /* USE_REGEX */
#endif /* !USE_REGEX */
  g_free(s);
}

static gboolean pidgin_debug_is_enabled(PurpleDebugLevel level,const char *category)
{
  return (debug_win != ((DebugWindow *)((void *)0))) && (purple_prefs_get_bool("/pidgin/debug/enabled") != 0);
}
static PurpleDebugUiOps ops = {(pidgin_debug_print), (pidgin_debug_is_enabled), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))};

PurpleDebugUiOps *pidgin_debug_get_ui_ops()
{
  return &ops;
}

void *pidgin_debug_get_handle()
{
  static int handle;
  return (&handle);
}
