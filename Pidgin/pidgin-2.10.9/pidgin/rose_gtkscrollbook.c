/*
 * @file gtkscrollbook.c GTK+ Scrolling notebook widget
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "gtkscrollbook.h"
static void pidgin_scroll_book_init(PidginScrollBook *scroll_book);
static void pidgin_scroll_book_class_init(PidginScrollBookClass *klass);
static void pidgin_scroll_book_forall(GtkContainer *c,gboolean include_internals,GtkCallback callback,gpointer user_data);

GType pidgin_scroll_book_get_type()
{
  static GType scroll_book_type = 0;
  if (!(scroll_book_type != 0UL)) {
    static const GTypeInfo scroll_book_info = {((sizeof(PidginScrollBookClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )pidgin_scroll_book_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(PidginScrollBook ))), (0), ((GInstanceInitFunc )pidgin_scroll_book_init), ((const GTypeValueTable *)((void *)0))
/* base_init */
/* base_finalize */
/* class_finalize */
/* class_data */
/* value_table */
};
    scroll_book_type = g_type_register_static(gtk_vbox_get_type(),"PidginScrollBook",&scroll_book_info,0);
  }
  return scroll_book_type;
}

static gboolean scroll_left_cb(PidginScrollBook *scroll_book,GdkEventButton *event)
{
  int index;
  if ((event -> type) != GDK_BUTTON_PRESS) 
    return 0;
  index = gtk_notebook_get_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(scroll_book -> notebook)),gtk_notebook_get_type()))));
  if (index > 0) 
    gtk_notebook_set_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(scroll_book -> notebook)),gtk_notebook_get_type()))),(index - 1));
  return (!0);
}

static gboolean scroll_right_cb(PidginScrollBook *scroll_book,GdkEventButton *event)
{
  int index;
  int count;
  if ((event -> type) != GDK_BUTTON_PRESS) 
    return 0;
  index = gtk_notebook_get_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(scroll_book -> notebook)),gtk_notebook_get_type()))));
  count = gtk_notebook_get_n_pages(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(scroll_book -> notebook)),gtk_notebook_get_type()))));
  if ((index + 1) < count) 
    gtk_notebook_set_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(scroll_book -> notebook)),gtk_notebook_get_type()))),(index + 1));
  return (!0);
}

static void refresh_scroll_box(PidginScrollBook *scroll_book,int index,int count)
{
  char *label;
  gtk_widget_show_all(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)scroll_book),gtk_widget_get_type()))));
  if (count < 1) 
    gtk_widget_hide_all((scroll_book -> hbox));
  else {
    gtk_widget_show_all((scroll_book -> hbox));
    if (count == 1) {
      gtk_widget_hide((scroll_book -> label));
      gtk_widget_hide((scroll_book -> left_arrow));
      gtk_widget_hide((scroll_book -> right_arrow));
    }
  }
  label = g_strdup_printf("<span size=\'smaller\' weight=\'bold\'>(%d/%d)</span>",(index + 1),count);
  gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(scroll_book -> label)),gtk_label_get_type()))),label);
  g_free(label);
  if (index == 0) 
    gtk_widget_set_sensitive((scroll_book -> left_arrow),0);
  else 
    gtk_widget_set_sensitive((scroll_book -> left_arrow),(!0));
  if ((index + 1) == count) 
    gtk_widget_set_sensitive((scroll_book -> right_arrow),0);
  else 
    gtk_widget_set_sensitive((scroll_book -> right_arrow),(!0));
}

static void page_count_change_cb(PidginScrollBook *scroll_book)
{
  int count;
  int index = gtk_notebook_get_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(scroll_book -> notebook)),gtk_notebook_get_type()))));
  count = gtk_notebook_get_n_pages(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(scroll_book -> notebook)),gtk_notebook_get_type()))));
  refresh_scroll_box(scroll_book,index,count);
}

static gboolean scroll_close_cb(PidginScrollBook *scroll_book,GdkEventButton *event)
{
  if ((event -> type) == GDK_BUTTON_PRESS) 
    gtk_widget_destroy(gtk_notebook_get_nth_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(scroll_book -> notebook)),gtk_notebook_get_type()))),gtk_notebook_get_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(scroll_book -> notebook)),gtk_notebook_get_type()))))));
  return 0;
}

static void switch_page_cb(GtkNotebook *notebook,GtkNotebookPage *page,guint page_num,PidginScrollBook *scroll_book)
{
  int count;
  count = gtk_notebook_get_n_pages(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(scroll_book -> notebook)),gtk_notebook_get_type()))));
  refresh_scroll_box(scroll_book,page_num,count);
}

static void pidgin_scroll_book_add(GtkContainer *container,GtkWidget *widget)
{
  PidginScrollBook *scroll_book;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)widget;
      GType __t = gtk_widget_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_WIDGET (widget)");
      return ;
    };
  }while (0);
  do {
    if ((widget -> parent) == ((GtkWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"widget->parent == NULL");
      return ;
    };
  }while (0);
  scroll_book = ((PidginScrollBook *)(g_type_check_instance_cast(((GTypeInstance *)container),pidgin_scroll_book_get_type())));
  scroll_book -> children = g_list_append((scroll_book -> children),widget);
  gtk_widget_show(widget);
  gtk_notebook_append_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(scroll_book -> notebook)),gtk_notebook_get_type()))),widget,0);
  page_count_change_cb(((PidginScrollBook *)(g_type_check_instance_cast(((GTypeInstance *)container),pidgin_scroll_book_get_type()))));
}

static void pidgin_scroll_book_remove(GtkContainer *container,GtkWidget *widget)
{
  int page;
  PidginScrollBook *scroll_book;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)widget;
      GType __t = gtk_widget_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_WIDGET(widget)");
      return ;
    };
  }while (0);
  scroll_book = ((PidginScrollBook *)(g_type_check_instance_cast(((GTypeInstance *)container),pidgin_scroll_book_get_type())));
  scroll_book -> children = g_list_remove((scroll_book -> children),widget);
/* gtk_widget_unparent(widget); */
  page = gtk_notebook_page_num(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)( *((PidginScrollBook *)(g_type_check_instance_cast(((GTypeInstance *)container),pidgin_scroll_book_get_type())))).notebook),gtk_notebook_get_type()))),widget);
  if (page >= 0) {
    gtk_notebook_remove_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)( *((PidginScrollBook *)(g_type_check_instance_cast(((GTypeInstance *)container),pidgin_scroll_book_get_type())))).notebook),gtk_notebook_get_type()))),page);
  }
}

static void pidgin_scroll_book_forall(GtkContainer *container,gboolean include_internals,GtkCallback callback,gpointer callback_data)
{
#if 0
#endif
  PidginScrollBook *scroll_book;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)container;
      GType __t = gtk_container_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_CONTAINER(container)");
      return ;
    };
  }while (0);
  scroll_book = ((PidginScrollBook *)(g_type_check_instance_cast(((GTypeInstance *)container),pidgin_scroll_book_get_type())));
  if (include_internals != 0) {
    ( *callback)((scroll_book -> hbox),callback_data);
    ( *callback)((scroll_book -> notebook),callback_data);
  }
#if 0
#endif
}

static void pidgin_scroll_book_class_init(PidginScrollBookClass *klass)
{
  GtkContainerClass *container_class = (GtkContainerClass *)klass;
  container_class -> add = pidgin_scroll_book_add;
  container_class -> remove = pidgin_scroll_book_remove;
  container_class -> forall = pidgin_scroll_book_forall;
}

static gboolean close_button_left_cb(GtkWidget *widget,GdkEventCrossing *event,GtkLabel *label)
{
  static GdkCursor *ptr = (GdkCursor *)((void *)0);
  if (ptr == ((GdkCursor *)((void *)0))) {
    ptr = gdk_cursor_new(GDK_LEFT_PTR);
  }
  gtk_label_set_markup(label,"\303\227");
  gdk_window_set_cursor((event -> window),ptr);
  return 0;
}

static gboolean close_button_entered_cb(GtkWidget *widget,GdkEventCrossing *event,GtkLabel *label)
{
  static GdkCursor *hand = (GdkCursor *)((void *)0);
  if (hand == ((GdkCursor *)((void *)0))) {
    hand = gdk_cursor_new(GDK_HAND2);
  }
  gtk_label_set_markup(label,"<u>\303\227</u>");
  gdk_window_set_cursor((event -> window),hand);
  return 0;
}

static void pidgin_scroll_book_init(PidginScrollBook *scroll_book)
{
  GtkWidget *eb;
  GtkWidget *close_button;
  scroll_book -> hbox = gtk_hbox_new(0,0);
/* Close */
  eb = gtk_event_box_new();
  gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(scroll_book -> hbox)),gtk_box_get_type()))),eb,0,0,0);
  gtk_event_box_set_visible_window(((GtkEventBox *)(g_type_check_instance_cast(((GTypeInstance *)eb),gtk_event_box_get_type()))),0);
  gtk_widget_set_events(eb,(GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK));
  close_button = gtk_label_new("\303\227");
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)eb),((GType )(20 << 2))))),"enter-notify-event",((GCallback )close_button_entered_cb),close_button,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)eb),((GType )(20 << 2))))),"leave-notify-event",((GCallback )close_button_left_cb),close_button,0,((GConnectFlags )0));
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)eb),gtk_container_get_type()))),close_button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)eb),((GType )(20 << 2))))),"button-press-event",((GCallback )scroll_close_cb),scroll_book,0,G_CONNECT_SWAPPED);
/* Right arrow */
  eb = gtk_event_box_new();
  gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(scroll_book -> hbox)),gtk_box_get_type()))),eb,0,0,0);
  scroll_book -> right_arrow = gtk_arrow_new(GTK_ARROW_RIGHT,GTK_SHADOW_NONE);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)eb),gtk_container_get_type()))),(scroll_book -> right_arrow));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)eb),((GType )(20 << 2))))),"button-press-event",((GCallback )scroll_right_cb),scroll_book,0,G_CONNECT_SWAPPED);
/* Count */
  scroll_book -> label = gtk_label_new(0);
  gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(scroll_book -> hbox)),gtk_box_get_type()))),(scroll_book -> label),0,0,0);
/* Left arrow */
  eb = gtk_event_box_new();
  gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(scroll_book -> hbox)),gtk_box_get_type()))),eb,0,0,0);
  scroll_book -> left_arrow = gtk_arrow_new(GTK_ARROW_LEFT,GTK_SHADOW_NONE);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)eb),gtk_container_get_type()))),(scroll_book -> left_arrow));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)eb),((GType )(20 << 2))))),"button-press-event",((GCallback )scroll_left_cb),scroll_book,0,G_CONNECT_SWAPPED);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)scroll_book),gtk_box_get_type()))),(scroll_book -> hbox),0,0,0);
  scroll_book -> notebook = gtk_notebook_new();
  gtk_notebook_set_show_tabs(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(scroll_book -> notebook)),gtk_notebook_get_type()))),0);
  gtk_notebook_set_show_border(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(scroll_book -> notebook)),gtk_notebook_get_type()))),0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)scroll_book),gtk_box_get_type()))),(scroll_book -> notebook),(!0),(!0),0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(scroll_book -> notebook)),((GType )(20 << 2))))),"remove",((GCallback )page_count_change_cb),scroll_book,0,G_CONNECT_SWAPPED);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(scroll_book -> notebook)),((GType )(20 << 2))))),"switch-page",((GCallback )switch_page_cb),scroll_book,0,((GConnectFlags )0));
  gtk_widget_show_all((scroll_book -> notebook));
}

GtkWidget *pidgin_scroll_book_new()
{
  return (g_object_new(pidgin_scroll_book_get_type(),0));
}
