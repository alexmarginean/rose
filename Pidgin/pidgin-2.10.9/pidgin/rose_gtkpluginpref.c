/**
 * @file gtkpluginpref.c GTK+ Plugin preferences
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include "debug.h"
#include "internal.h"
#include "pluginpref.h"
#include "prefs.h"
#include "gtkimhtml.h"
#include "gtkpluginpref.h"
#include "gtkprefs.h"
#include "gtkutils.h"

static gboolean entry_cb(GtkWidget *entry,gpointer data)
{
  char *pref = data;
  purple_prefs_set_string(pref,gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type())))));
  return 0;
}

static void imhtml_cb(GtkTextBuffer *buffer,gpointer data)
{
  char *pref;
  char *text;
  GtkIMHtml *imhtml = data;
  pref = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"pref-key"));
  do {
    if (pref != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pref");
      return ;
    };
  }while (0);
  text = gtk_imhtml_get_markup(imhtml);
  purple_prefs_set_string(pref,text);
  g_free(text);
}

static void imhtml_format_cb(GtkIMHtml *imhtml,GtkIMHtmlButtons buttons,gpointer data)
{
  imhtml_cb(gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type())))),data);
}

static void make_string_pref(GtkWidget *parent,PurplePluginPref *pref,GtkSizeGroup *sg)
{
  GtkWidget *box;
  GtkWidget *gtk_label;
  GtkWidget *entry;
  const gchar *pref_name;
  const gchar *pref_label;
  PurpleStringFormatType format;
  pref_name = purple_plugin_pref_get_name(pref);
  pref_label = purple_plugin_pref_get_label(pref);
  format = purple_plugin_pref_get_format_type(pref);
  switch(purple_plugin_pref_get_type(pref)){
    case PURPLE_PLUGIN_PREF_CHOICE:
{
      gtk_label = pidgin_prefs_dropdown_from_list(parent,pref_label,PURPLE_PREF_STRING,pref_name,purple_plugin_pref_get_choices(pref));
      gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)gtk_label),gtk_misc_get_type()))),0,0.5);
      if (sg != 0) 
        gtk_size_group_add_widget(sg,gtk_label);
      break; 
    }
    default:
{
      if (format == PURPLE_STRING_FORMAT_TYPE_NONE) {
        entry = gtk_entry_new();
        gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),purple_prefs_get_string(pref_name));
        gtk_entry_set_max_length(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),(purple_plugin_pref_get_max_length(pref)));
        if (purple_plugin_pref_get_masked(pref) != 0) {
          gtk_entry_set_visibility(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),0);
#if !GTK_CHECK_VERSION(2,16,0)
#endif /* Less than GTK+ 2.16 */
        }
        g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"changed",((GCallback )entry_cb),((gpointer )pref_name),0,((GConnectFlags )0));
        pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)parent),gtk_box_get_type()))),pref_label,sg,entry,(!0),0);
      }
      else {
        GtkWidget *hbox;
        GtkWidget *spacer;
        GtkWidget *imhtml;
        GtkWidget *toolbar;
        GtkWidget *frame;
        box = gtk_vbox_new(0,6);
        gtk_widget_show(box);
        gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)parent),gtk_box_get_type()))),box,0,0,0);
        gtk_label = gtk_label_new_with_mnemonic(pref_label);
        gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)gtk_label),gtk_misc_get_type()))),0,0.5);
        gtk_widget_show(gtk_label);
        gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gtk_box_get_type()))),gtk_label,0,0,0);
        if (sg != 0) 
          gtk_size_group_add_widget(sg,gtk_label);
        hbox = gtk_hbox_new(0,6);
        gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gtk_box_get_type()))),hbox,0,0,0);
        gtk_widget_show(hbox);
        spacer = gtk_label_new("    ");
        gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),spacer,0,0,0);
        gtk_widget_show(spacer);
        frame = pidgin_create_imhtml((!0),&imhtml,&toolbar,0);
        if (!((format & PURPLE_STRING_FORMAT_TYPE_HTML) != 0U)) 
          gtk_widget_destroy(toolbar);
        gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),purple_prefs_get_string(pref_name),(((format & PURPLE_STRING_FORMAT_TYPE_MULTILINE) != 0U)?0 : GTK_IMHTML_NO_NEWLINE),0);
        gtk_label_set_mnemonic_widget(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)gtk_label),gtk_label_get_type()))),imhtml);
        gtk_widget_show_all(frame);
        g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"pref-key",((gpointer )pref_name));
        g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type())))))),((GType )(20 << 2))))),"changed",((GCallback )imhtml_cb),imhtml,0,((GConnectFlags )0));
        g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"format_function_toggle",((GCallback )imhtml_format_cb),imhtml,0,((GConnectFlags )0));
        gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),frame,(!0),(!0),0);
      }
      break; 
    }
  }
}

static void make_int_pref(GtkWidget *parent,PurplePluginPref *pref,GtkSizeGroup *sg)
{
  GtkWidget *gtk_label;
  const gchar *pref_name;
  const gchar *pref_label;
  gint max;
  gint min;
  pref_name = purple_plugin_pref_get_name(pref);
  pref_label = purple_plugin_pref_get_label(pref);
  switch(purple_plugin_pref_get_type(pref)){
    case PURPLE_PLUGIN_PREF_CHOICE:
{
      gtk_label = pidgin_prefs_dropdown_from_list(parent,pref_label,PURPLE_PREF_INT,pref_name,purple_plugin_pref_get_choices(pref));
      gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)gtk_label),gtk_misc_get_type()))),0,0.5);
      if (sg != 0) 
        gtk_size_group_add_widget(sg,gtk_label);
      break; 
    }
    default:
{
      purple_plugin_pref_get_bounds(pref,&min,&max);
      pidgin_prefs_labeled_spin_button(parent,pref_label,pref_name,min,max,sg);
      break; 
    }
  }
}

static void make_info_pref(GtkWidget *parent,PurplePluginPref *pref)
{
  GtkWidget *gtk_label = gtk_label_new(purple_plugin_pref_get_label(pref));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)gtk_label),gtk_misc_get_type()))),0,0);
  gtk_label_set_line_wrap(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)gtk_label),gtk_label_get_type()))),(!0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)parent),gtk_box_get_type()))),gtk_label,0,0,0);
  gtk_widget_show(gtk_label);
}

GtkWidget *pidgin_plugin_pref_create_frame(PurplePluginPrefFrame *frame)
{
  GtkWidget *ret;
  GtkWidget *parent;
  GtkSizeGroup *sg;
  GList *pp;
  do {
    if (frame != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"frame");
      return 0;
    };
  }while (0);
  sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  parent = (ret = gtk_vbox_new(0,16));
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_container_get_type()))),12);
  gtk_widget_show(ret);
  for (pp = purple_plugin_pref_frame_get_prefs(frame); pp != ((GList *)((void *)0)); pp = (pp -> next)) {{
      PurplePluginPref *pref = (PurplePluginPref *)(pp -> data);
      const char *name = purple_plugin_pref_get_name(pref);
      const char *label = purple_plugin_pref_get_label(pref);
      if (name == ((const char *)((void *)0))) {
        if (label == ((const char *)((void *)0))) 
          continue; 
        if ((purple_plugin_pref_get_type(pref)) == PURPLE_PLUGIN_PREF_INFO) {
          make_info_pref(parent,pref);
        }
        else {
          parent = pidgin_make_frame(ret,label);
          gtk_widget_show(parent);
        }
        continue; 
      }
      switch(purple_prefs_get_type(name)){
        case PURPLE_PREF_BOOLEAN:
{
          pidgin_prefs_checkbox(label,name,parent);
          break; 
        }
        case PURPLE_PREF_INT:
{
          make_int_pref(parent,pref,sg);
          break; 
        }
        case PURPLE_PREF_STRING:
{
          make_string_pref(parent,pref,sg);
          break; 
        }
        default:
{
          break; 
        }
      }
    }
  }
  g_object_unref(sg);
  return ret;
}
