/*
 * @file gtkblist.c GTK+ BuddyList API
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "pidgin.h"
#include "account.h"
#include "connection.h"
#include "core.h"
#include "debug.h"
#include "notify.h"
#include "prpl.h"
#include "prefs.h"
#include "plugin.h"
#include "request.h"
#include "signals.h"
#include "pidginstock.h"
#include "theme-loader.h"
#include "theme-manager.h"
#include "util.h"
#include "gtkaccount.h"
#include "gtkblist.h"
#include "gtkcellrendererexpander.h"
#include "gtkcertmgr.h"
#include "gtkconv.h"
#include "gtkdebug.h"
#include "gtkdialogs.h"
#include "gtkft.h"
#include "gtklog.h"
#include "gtkmenutray.h"
#include "gtkpounce.h"
#include "gtkplugin.h"
#include "gtkprefs.h"
#include "gtkprivacy.h"
#include "gtkroomlist.h"
#include "gtkstatusbox.h"
#include "gtkscrollbook.h"
#include "gtksmiley.h"
#include "gtkblist-theme.h"
#include "gtkblist-theme-loader.h"
#include "gtkutils.h"
#include "pidgin/minidialog.h"
#include "pidgin/pidgintooltip.h"
#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
typedef struct __unnamed_class___F0_L73_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L247R__Pe___variable_name_unknown_scope_and_name__scope__account__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkBox_GtkBox__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__vbox__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__account_menu__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1295R__Pe___variable_name_unknown_scope_and_name__scope__sg {
PurpleAccount *account;
GtkWidget *window;
GtkBox *vbox;
GtkWidget *account_menu;
GtkSizeGroup *sg;}PidginBlistRequestData;
typedef struct __unnamed_class___F0_L82_C9_unknown_scope_and_name_variable_declaration__variable_type_L1693R_variable_name_unknown_scope_and_name__scope__rq_data__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__combo__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__entry_for_alias__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__entry_for_invite {
PidginBlistRequestData rq_data;
GtkWidget *combo;
GtkWidget *entry;
GtkWidget *entry_for_alias;
GtkWidget *entry_for_invite;}PidginAddBuddyData;
typedef struct __unnamed_class___F0_L92_C9_unknown_scope_and_name_variable_declaration__variable_type_L1693R_variable_name_unknown_scope_and_name__scope__rq_data__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__default_chat_name__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GList_GList__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__entries {
PidginBlistRequestData rq_data;
gchar *default_chat_name;
GList *entries;}PidginChatData;
typedef struct __unnamed_class___F0_L99_C9_unknown_scope_and_name_variable_declaration__variable_type_L1695R_variable_name_unknown_scope_and_name__scope__chat_data__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__alias_entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__group_combo__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__autojoin__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__persistent {
PidginChatData chat_data;
GtkWidget *alias_entry;
GtkWidget *group_combo;
GtkWidget *autojoin;
GtkWidget *persistent;}PidginAddChatData;
typedef struct __unnamed_class___F0_L109_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L1632R__Pe___variable_name_unknown_scope_and_name__scope__error_scrollbook__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1697R__Pe___variable_name_unknown_scope_and_name__scope__signed_on_elsewhere__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1588R__Pe___variable_name_unknown_scope_and_name__scope__current_theme {
/** Used to hold error minidialogs.  Gets packed
	 *  inside PidginBuddyList.error_buttons
	 */
PidginScrollBook *error_scrollbook;
/** Pointer to the mini-dialog about having signed on elsewhere, if one
	 *  is showing; @c NULL otherwise.
	 */
PidginMiniDialog *signed_on_elsewhere;
PidginBlistTheme *current_theme;}PidginBuddyListPrivate;
#define PIDGIN_BUDDY_LIST_GET_PRIVATE(list) \
	((PidginBuddyListPrivate *)((list)->priv))
static GtkWidget *accountmenu = (GtkWidget *)((void *)0);
static guint visibility_manager_count = 0;
static GdkVisibilityState gtk_blist_visibility = GDK_VISIBILITY_UNOBSCURED;
static gboolean gtk_blist_focused = 0;
static gboolean editing_blist = 0;
static GList *pidgin_blist_sort_methods = (GList *)((void *)0);
static struct pidgin_blist_sort_method *current_sort_method = (struct pidgin_blist_sort_method *)((void *)0);
static void sort_method_none(PurpleBlistNode *node,PurpleBuddyList *blist,GtkTreeIter groupiter,GtkTreeIter *cur,GtkTreeIter *iter);
static void sort_method_alphabetical(PurpleBlistNode *node,PurpleBuddyList *blist,GtkTreeIter groupiter,GtkTreeIter *cur,GtkTreeIter *iter);
static void sort_method_status(PurpleBlistNode *node,PurpleBuddyList *blist,GtkTreeIter groupiter,GtkTreeIter *cur,GtkTreeIter *iter);
static void sort_method_log_activity(PurpleBlistNode *node,PurpleBuddyList *blist,GtkTreeIter groupiter,GtkTreeIter *cur,GtkTreeIter *iter);
static PidginBuddyList *gtkblist = (PidginBuddyList *)((void *)0);
static GList *groups_tree();
static gboolean pidgin_blist_refresh_timer(PurpleBuddyList *list);
static void pidgin_blist_update_buddy(PurpleBuddyList *list,PurpleBlistNode *node,gboolean status_change);
static void pidgin_blist_selection_changed(GtkTreeSelection *selection,gpointer data);
static void pidgin_blist_update(PurpleBuddyList *list,PurpleBlistNode *node);
static void pidgin_blist_update_group(PurpleBuddyList *list,PurpleBlistNode *node);
static void pidgin_blist_update_contact(PurpleBuddyList *list,PurpleBlistNode *node);
static char *pidgin_get_tooltip_text(PurpleBlistNode *node,gboolean full);
static const char *item_factory_translate_func(const char *path,gpointer func_data);
static gboolean get_iter_from_node(PurpleBlistNode *node,GtkTreeIter *iter);
static gboolean buddy_is_displayable(PurpleBuddy *buddy);
static void redo_buddy_list(PurpleBuddyList *list,gboolean remove,gboolean rerender);
static void pidgin_blist_collapse_contact_cb(GtkWidget *w,PurpleBlistNode *node);
static char *pidgin_get_group_title(PurpleBlistNode *gnode,gboolean expanded);
static void pidgin_blist_expand_contact_cb(GtkWidget *w,PurpleBlistNode *node);
static void set_urgent();
typedef enum __unnamed_enum___F0_L160_C9_PIDGIN_BLIST_NODE_HAS_PENDING_MESSAGE__COMMA__PIDGIN_BLIST_CHAT_HAS_PENDING_MESSAGE_WITH_NICK {
/* Whether there's pending message in a conversation */
PIDGIN_BLIST_NODE_HAS_PENDING_MESSAGE=1,
/* Whether there's a pending message in a chat that mentions our nick */
PIDGIN_BLIST_CHAT_HAS_PENDING_MESSAGE_WITH_NICK}PidginBlistNodeFlags;
typedef struct _pidgin_blist_node {
GtkTreeRowReference *row;
gboolean contact_expanded;
gboolean recent_signonoff;
gint recent_signonoff_timer;
struct __unnamed_class___F0_L170_C2__pidgin_blist_node_variable_declaration__variable_type___Pb__L298R__Pe___variable_name__pidgin_blist_node__scope__conv__DELIMITER___pidgin_blist_node_variable_declaration__variable_type_L5R_variable_name__pidgin_blist_node__scope__last_message__DELIMITER___pidgin_blist_node_variable_declaration__variable_type_L1699R_variable_name__pidgin_blist_node__scope__flags {
PurpleConversation *conv;
/* timestamp for last displayed message */
time_t last_message;
PidginBlistNodeFlags flags;}conv;}PidginBlistNode;
/***************************************************
 *              Callbacks                          *
 ***************************************************/

static gboolean gtk_blist_visibility_cb(GtkWidget *w,GdkEventVisibility *event,gpointer data)
{
  GdkVisibilityState old_state = gtk_blist_visibility;
  gtk_blist_visibility = (event -> state);
  if ((gtk_blist_visibility == GDK_VISIBILITY_FULLY_OBSCURED) && (old_state != GDK_VISIBILITY_FULLY_OBSCURED)) {
/* no longer fully obscured */
    pidgin_blist_refresh_timer(purple_get_blist());
  }
/* continue to handle event normally */
  return 0;
}

static gboolean gtk_blist_window_state_cb(GtkWidget *w,GdkEventWindowState *event,gpointer data)
{
  if (((event -> changed_mask) & GDK_WINDOW_STATE_WITHDRAWN) != 0U) {
    if (((event -> new_window_state) & GDK_WINDOW_STATE_WITHDRAWN) != 0U) 
      purple_prefs_set_bool("/pidgin/blist/list_visible",0);
    else {
      purple_prefs_set_bool("/pidgin/blist/list_visible",(!0));
      pidgin_blist_refresh_timer(purple_get_blist());
    }
  }
  if (((event -> changed_mask) & GDK_WINDOW_STATE_MAXIMIZED) != 0U) {
    if (((event -> new_window_state) & GDK_WINDOW_STATE_MAXIMIZED) != 0U) 
      purple_prefs_set_bool("/pidgin/blist/list_maximized",(!0));
    else 
      purple_prefs_set_bool("/pidgin/blist/list_maximized",0);
  }
/* Refresh gtkblist if un-iconifying */
  if (((event -> changed_mask) & GDK_WINDOW_STATE_ICONIFIED) != 0U) {
    if (!(((event -> new_window_state) & GDK_WINDOW_STATE_ICONIFIED) != 0U)) 
      pidgin_blist_refresh_timer(purple_get_blist());
  }
  return 0;
}

static gboolean gtk_blist_delete_cb(GtkWidget *w,GdkEventAny *event,gpointer data)
{
  if (visibility_manager_count != 0U) 
    purple_blist_set_visible(0);
  else 
    purple_core_quit();
/* we handle everything, event should not propogate further */
  return (!0);
}

static gboolean gtk_blist_configure_cb(GtkWidget *w,GdkEventConfigure *event,gpointer data)
{
/* unfortunately GdkEventConfigure ignores the window gravity, but  *
	 * the only way we have of setting the position doesn't. we have to *
	 * call get_position because it does pay attention to the gravity.  *
	 * this is inefficient and I agree it sucks, but it's more likely   *
	 * to work correctly.                                    - Robot101 */
  gint x;
  gint y;
/* check for visibility because when we aren't visible, this will   *
	 * give us bogus (0,0) coordinates.                      - xOr      */
  if ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)w),gtk_object_get_type())))).flags & GTK_VISIBLE) != 0) 
    gtk_window_get_position(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)w),gtk_window_get_type()))),&x,&y);
  else 
/* carry on normally */
    return 0;
#ifdef _WIN32
/* Workaround for GTK+ bug # 169811 - "configure_event" is fired
	 * when the window is being maximized */
#endif
/* don't save if nothing changed */
  if ((((x == purple_prefs_get_int("/pidgin/blist/x")) && (y == purple_prefs_get_int("/pidgin/blist/y"))) && ((event -> width) == purple_prefs_get_int("/pidgin/blist/width"))) && ((event -> height) == purple_prefs_get_int("/pidgin/blist/height"))) {
/* carry on normally */
    return 0;
  }
/* don't save off-screen positioning */
  if (((((x + (event -> width)) < 0) || ((y + (event -> height)) < 0)) || (x > gdk_screen_width())) || (y > gdk_screen_height())) {
/* carry on normally */
    return 0;
  }
/* ignore changes when maximized */
  if (purple_prefs_get_bool("/pidgin/blist/list_maximized") != 0) 
    return 0;
/* store the position */
  purple_prefs_set_int("/pidgin/blist/x",x);
  purple_prefs_set_int("/pidgin/blist/y",y);
  purple_prefs_set_int("/pidgin/blist/width",(event -> width));
  purple_prefs_set_int("/pidgin/blist/height",(event -> height));
/* continue to handle event normally */
  return 0;
}

static void gtk_blist_menu_info_cb(GtkWidget *w,PurpleBuddy *b)
{
  PurpleAccount *account = purple_buddy_get_account(b);
  pidgin_retrieve_user_info(purple_account_get_connection(account),purple_buddy_get_name(b));
}

static void gtk_blist_menu_im_cb(GtkWidget *w,PurpleBuddy *b)
{
  pidgin_dialogs_im_with_user(purple_buddy_get_account(b),purple_buddy_get_name(b));
}
#ifdef USE_VV

static void gtk_blist_menu_audio_call_cb(GtkWidget *w,PurpleBuddy *b)
{
  purple_prpl_initiate_media(purple_buddy_get_account(b),purple_buddy_get_name(b),PURPLE_MEDIA_AUDIO);
}

static void gtk_blist_menu_video_call_cb(GtkWidget *w,PurpleBuddy *b)
{
/* if the buddy supports both audio and video, start a combined call,
	 otherwise start a pure video session */
  if (((purple_prpl_get_media_caps(purple_buddy_get_account(b),purple_buddy_get_name(b))) & PURPLE_MEDIA_CAPS_AUDIO_VIDEO) != 0U) {
    purple_prpl_initiate_media(purple_buddy_get_account(b),purple_buddy_get_name(b),(PURPLE_MEDIA_AUDIO | PURPLE_MEDIA_VIDEO));
  }
  else {
    purple_prpl_initiate_media(purple_buddy_get_account(b),purple_buddy_get_name(b),PURPLE_MEDIA_VIDEO);
  }
}
#endif

static void gtk_blist_menu_send_file_cb(GtkWidget *w,PurpleBuddy *b)
{
  PurpleAccount *account = purple_buddy_get_account(b);
  serv_send_file(purple_account_get_connection(account),purple_buddy_get_name(b),0);
}

static void gtk_blist_menu_move_to_cb(GtkWidget *w,PurpleBlistNode *node)
{
  PurpleGroup *group = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)w),((GType )(20 << 2))))),"groupnode"));
  purple_blist_add_contact(((PurpleContact *)node),group,0);
}

static void gtk_blist_menu_autojoin_cb(GtkWidget *w,PurpleChat *chat)
{
  purple_blist_node_set_bool(((PurpleBlistNode *)chat),"gtk-autojoin",gtk_check_menu_item_get_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)w),gtk_check_menu_item_get_type())))));
}

static void gtk_blist_menu_persistent_cb(GtkWidget *w,PurpleChat *chat)
{
  purple_blist_node_set_bool(((PurpleBlistNode *)chat),"gtk-persistent",gtk_check_menu_item_get_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)w),gtk_check_menu_item_get_type())))));
}

static PurpleConversation *find_conversation_with_buddy(PurpleBuddy *buddy)
{
  PidginBlistNode *ui = (purple_blist_node_get_ui_data(((PurpleBlistNode *)buddy)));
  if (ui != 0) 
    return ui -> conv.conv;
  return purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,purple_buddy_get_name(buddy),(purple_buddy_get_account(buddy)));
}

static void gtk_blist_join_chat(PurpleChat *chat)
{
  PurpleAccount *account;
  PurpleConversation *conv;
  PurplePluginProtocolInfo *prpl_info;
  GHashTable *components;
  const char *name;
  char *chat_name;
  account = purple_chat_get_account(chat);
  prpl_info = ((PurplePluginProtocolInfo *)( *( *purple_find_prpl(purple_account_get_protocol_id(account))).info).extra_info);
  components = purple_chat_get_components(chat);
  if ((prpl_info != 0) && ((prpl_info -> get_chat_name) != 0)) 
    chat_name = ( *(prpl_info -> get_chat_name))(components);
  else 
    chat_name = ((char *)((void *)0));
  if (chat_name != 0) 
    name = chat_name;
  else 
    name = purple_chat_get_name(chat);
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,name,account);
  if (conv != ((PurpleConversation *)((void *)0))) {
    pidgin_conv_attach_to_conversation(conv);
    purple_conversation_present(conv);
  }
  serv_join_chat(purple_account_get_connection(account),components);
  g_free(chat_name);
}

static void gtk_blist_menu_join_cb(GtkWidget *w,PurpleChat *chat)
{
  gtk_blist_join_chat(chat);
}

static void gtk_blist_renderer_editing_cancelled_cb(GtkCellRenderer *renderer,PurpleBuddyList *list)
{
  editing_blist = 0;
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)renderer),((GType )(20 << 2))))),"editable",0,((void *)((void *)0)));
  pidgin_blist_refresh(list);
}

static void gtk_blist_renderer_editing_started_cb(GtkCellRenderer *renderer,GtkCellEditable *editable,gchar *path_str,gpointer user_data)
{
  GtkTreeIter iter;
  GtkTreePath *path = (GtkTreePath *)((void *)0);
  PurpleBlistNode *node;
  const char *text = (const char *)((void *)0);
  path = gtk_tree_path_new_from_string(path_str);
  gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,path);
  gtk_tree_path_free(path);
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,NODE_COLUMN,&node,-1);
  switch(purple_blist_node_get_type(node)){
    case PURPLE_BLIST_CONTACT_NODE:
{
      text = purple_contact_get_alias(((PurpleContact *)node));
      break; 
    }
    case PURPLE_BLIST_BUDDY_NODE:
{
      text = purple_buddy_get_alias(((PurpleBuddy *)node));
      break; 
    }
    case PURPLE_BLIST_GROUP_NODE:
{
      text = purple_group_get_name(((PurpleGroup *)node));
      break; 
    }
    case PURPLE_BLIST_CHAT_NODE:
{
      text = purple_chat_get_name(((PurpleChat *)node));
      break; 
    }
    default:
{
      do {
        g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","gtkblist.c",444,((const char *)__func__));
        return ;
      }while (0);
    }
  }
  if ((({
    GTypeInstance *__inst = (GTypeInstance *)editable;
    GType __t = gtk_entry_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
{
    GtkEntry *entry = (GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)editable),gtk_entry_get_type()));
    gtk_entry_set_text(entry,text);
  }
  editing_blist = (!0);
}

static void gtk_blist_do_personize(GList *merges)
{
  PurpleBlistNode *contact = (PurpleBlistNode *)((void *)0);
  int max = 0;
  GList *tmp;
/* First, we find the contact to merge the rest of the buddies into.
 	 * This will be the contact with the most buddies in it; ties are broken
 	 * by which contact is higher in the list
 	 */
  for (tmp = merges; tmp != 0; tmp = (tmp -> next)) {{
      PurpleBlistNode *node = (tmp -> data);
      PurpleBlistNode *b;
      PurpleBlistNodeType type;
      int i = 0;
      type = purple_blist_node_get_type(node);
      if (type == PURPLE_BLIST_BUDDY_NODE) {
        node = purple_blist_node_get_parent(node);
        type = purple_blist_node_get_type(node);
      }
      if (type != PURPLE_BLIST_CONTACT_NODE) 
        continue; 
      for (b = purple_blist_node_get_first_child(node); b != 0; b = purple_blist_node_get_sibling_next(b)) {
        i++;
      }
      if (i > max) {
        contact = node;
        max = i;
      }
    }
  }
  if (contact == ((PurpleBlistNode *)((void *)0))) 
    return ;
/* Merge all those buddies into this contact */
  for (tmp = merges; tmp != 0; tmp = (tmp -> next)) {{
      PurpleBlistNode *node = (tmp -> data);
      if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) 
        node = purple_blist_node_get_parent(node);
      if (node == contact) 
        continue; 
      purple_blist_merge_contact(((PurpleContact *)node),contact);
    }
  }
/* And show the expanded contact, so the people know what's going on */
  pidgin_blist_expand_contact_cb(0,contact);
  g_list_free(merges);
}

static void gtk_blist_auto_personize(PurpleBlistNode *group,const char *alias)
{
  PurpleBlistNode *contact;
  PurpleBlistNode *buddy;
  GList *merges = (GList *)((void *)0);
  int i = 0;
  char *a = g_utf8_casefold(alias,(-1));
  for (contact = purple_blist_node_get_first_child(group); contact != ((PurpleBlistNode *)((void *)0)); contact = purple_blist_node_get_sibling_next(contact)) {{
      char *node_alias;
      if ((purple_blist_node_get_type(contact)) != PURPLE_BLIST_CONTACT_NODE) 
        continue; 
      node_alias = g_utf8_casefold(purple_contact_get_alias(((PurpleContact *)contact)),(-1));
      if ((node_alias != 0) && !(g_utf8_collate(node_alias,a) != 0)) {
        merges = g_list_append(merges,contact);
        i++;
        g_free(node_alias);
        continue; 
      }
      g_free(node_alias);
{
        for (buddy = purple_blist_node_get_first_child(contact); buddy != 0; buddy = purple_blist_node_get_sibling_next(buddy)) {
          if ((purple_blist_node_get_type(buddy)) != PURPLE_BLIST_BUDDY_NODE) 
            continue; 
          node_alias = g_utf8_casefold(purple_buddy_get_alias(((PurpleBuddy *)buddy)),(-1));
          if ((node_alias != 0) && !(g_utf8_collate(node_alias,a) != 0)) {
            merges = g_list_append(merges,buddy);
            i++;
            g_free(node_alias);
            break; 
          }
          g_free(node_alias);
        }
      }
    }
  }
  g_free(a);
  if (i > 1) {
    char *msg = g_strdup_printf((ngettext("You have %d contact named %s. Would you like to merge them\?","You currently have %d contacts named %s. Would you like to merge them\?",i)),i,alias);
    purple_request_action(0,0,msg,((const char *)(dgettext("pidgin","Merging these contacts will cause them to share a single entry on the buddy list and use a single conversation window. You can separate them again by choosing \'Expand\' from the contact\'s context menu"))),0,0,0,0,merges,2,((const char *)(dgettext("pidgin","_Yes"))),((PurpleCallback )gtk_blist_do_personize),((const char *)(dgettext("pidgin","_No"))),((PurpleCallback )g_list_free));
    g_free(msg);
  }
  else 
    g_list_free(merges);
}

static void gtk_blist_renderer_edited_cb(GtkCellRendererText *text_rend,char *arg1,char *arg2,PurpleBuddyList *list)
{
  GtkTreeIter iter;
  GtkTreePath *path;
  PurpleBlistNode *node;
  PurpleGroup *dest;
  editing_blist = 0;
  path = gtk_tree_path_new_from_string(arg1);
  gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,path);
  gtk_tree_path_free(path);
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,NODE_COLUMN,&node,-1);
  gtk_tree_view_set_enable_search(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type()))),(!0));
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> text_rend)),((GType )(20 << 2))))),"editable",0,((void *)((void *)0)));
  switch(purple_blist_node_get_type(node)){
    case PURPLE_BLIST_CONTACT_NODE:
{
{
        PurpleContact *contact = (PurpleContact *)node;
        struct _pidgin_blist_node *gtknode = (struct _pidgin_blist_node *)(purple_blist_node_get_ui_data(node));
/*
				 * XXX Using purple_contact_get_alias here breaks because we
				 * specifically want to check the contact alias only (i.e. not
				 * the priority buddy, which purple_contact_get_alias does).
				 * Adding yet another get_alias is evil, so figure this out
				 * later :-P
				 */
        if (((contact -> alias) != 0) || ((gtknode -> contact_expanded) != 0)) {
          purple_blist_alias_contact(contact,arg2);
          gtk_blist_auto_personize(purple_blist_node_get_parent(node),arg2);
        }
        else {
          PurpleBuddy *buddy = purple_contact_get_priority_buddy(contact);
          purple_blist_alias_buddy(buddy,arg2);
          serv_alias_buddy(buddy);
          gtk_blist_auto_personize(purple_blist_node_get_parent(node),arg2);
        }
      }
      break; 
    }
    case PURPLE_BLIST_BUDDY_NODE:
{
{
        PurpleGroup *group = purple_buddy_get_group(((PurpleBuddy *)node));
        purple_blist_alias_buddy(((PurpleBuddy *)node),arg2);
        serv_alias_buddy(((PurpleBuddy *)node));
        gtk_blist_auto_personize(((PurpleBlistNode *)group),arg2);
      }
      break; 
    }
    case PURPLE_BLIST_GROUP_NODE:
{
      dest = purple_find_group(arg2);
      if ((dest != ((PurpleGroup *)((void *)0))) && (purple_utf8_strcasecmp(arg2,purple_group_get_name(((PurpleGroup *)node))) != 0)) {
        pidgin_dialogs_merge_groups(((PurpleGroup *)node),arg2);
      }
      else {
        purple_blist_rename_group(((PurpleGroup *)node),arg2);
      }
      break; 
    }
    case PURPLE_BLIST_CHAT_NODE:
{
      purple_blist_alias_chat(((PurpleChat *)node),arg2);
      break; 
    }
    default:
{
      break; 
    }
  }
  pidgin_blist_refresh(list);
}

static void chat_components_edit_ok(PurpleChat *chat,PurpleRequestFields *allfields)
{
  GList *groups;
  GList *fields;
  for (groups = purple_request_fields_get_groups(allfields); groups != 0; groups = (groups -> next)) {
    fields = purple_request_field_group_get_fields((groups -> data));
    for (; fields != 0; fields = (fields -> next)) {
      PurpleRequestField *field = (fields -> data);
      const char *id;
      char *val;
      id = purple_request_field_get_id(field);
      if ((purple_request_field_get_type(field)) == PURPLE_REQUEST_FIELD_INTEGER) 
        val = g_strdup_printf("%d",purple_request_field_int_get_value(field));
      else 
        val = g_strdup(purple_request_field_string_get_value(field));
      if (!(val != 0)) {
        g_hash_table_remove(purple_chat_get_components(chat),id);
      }
      else {
/* val should not be free'd */
        g_hash_table_replace(purple_chat_get_components(chat),(g_strdup(id)),val);
      }
    }
  }
}

static void chat_components_edit(GtkWidget *w,PurpleBlistNode *node)
{
  PurpleRequestFields *fields = purple_request_fields_new();
  PurpleRequestFieldGroup *group = purple_request_field_group_new(0);
  PurpleRequestField *field;
  GList *parts;
  GList *iter;
  struct proto_chat_entry *pce;
  PurpleConnection *gc;
  PurpleChat *chat = (PurpleChat *)node;
  purple_request_fields_add_group(fields,group);
  gc = purple_account_get_connection((purple_chat_get_account(chat)));
  parts = ( *( *((PurplePluginProtocolInfo *)( *( *purple_connection_get_prpl(gc)).info).extra_info)).chat_info)(gc);
  for (iter = parts; iter != 0; iter = (iter -> next)) {
    pce = (iter -> data);
    if ((pce -> is_int) != 0) {
      int val;
      const char *str = (g_hash_table_lookup(purple_chat_get_components(chat),(pce -> identifier)));
      if (!(str != 0) || (sscanf(str,"%d",&val) != 1)) 
        val = (pce -> min);
      field = purple_request_field_int_new((pce -> identifier),(pce -> label),val);
    }
    else {
      field = purple_request_field_string_new((pce -> identifier),(pce -> label),(g_hash_table_lookup(purple_chat_get_components(chat),(pce -> identifier))),0);
      if ((pce -> secret) != 0) 
        purple_request_field_string_set_masked(field,(!0));
    }
    if ((pce -> required) != 0) 
      purple_request_field_set_required(field,(!0));
    purple_request_field_group_add_field(group,field);
    g_free(pce);
  }
  g_list_free(parts);
  purple_request_fields(0,((const char *)(dgettext("pidgin","Edit Chat"))),0,((const char *)(dgettext("pidgin","Please update the necessary fields."))),fields,((const char *)(dgettext("pidgin","Save"))),((GCallback )chat_components_edit_ok),((const char *)(dgettext("pidgin","Cancel"))),0,0,0,0,chat);
}

static void gtk_blist_menu_alias_cb(GtkWidget *w,PurpleBlistNode *node)
{
  GtkTreeIter iter;
  GtkTreePath *path;
  if (!(get_iter_from_node(node,&iter) != 0)) {
/* This is either a bug, or the buddy is in a collapsed contact */
    node = purple_blist_node_get_parent(node);
    if (!(get_iter_from_node(node,&iter) != 0)) 
/* Now it's definitely a bug */
      return ;
  }
  pidgin_blist_tooltip_destroy();
  path = gtk_tree_model_get_path(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter);
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> text_rend)),((GType )(20 << 2))))),"editable",!0,((void *)((void *)0)));
  gtk_tree_view_set_enable_search(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type()))),0);
  gtk_widget_grab_focus((gtkblist -> treeview));
  gtk_tree_view_set_cursor_on_cell(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type()))),path,(gtkblist -> text_column),(gtkblist -> text_rend),(!0));
  gtk_tree_path_free(path);
}

static void gtk_blist_menu_bp_cb(GtkWidget *w,PurpleBuddy *b)
{
  pidgin_pounce_editor_show(purple_buddy_get_account(b),purple_buddy_get_name(b),0);
}

static void gtk_blist_menu_showlog_cb(GtkWidget *w,PurpleBlistNode *node)
{
  PurpleLogType type;
  PurpleAccount *account;
  char *name = (char *)((void *)0);
  pidgin_set_cursor((gtkblist -> window),GDK_WATCH);
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    PurpleBuddy *b = (PurpleBuddy *)node;
    type = PURPLE_LOG_IM;
    name = g_strdup(purple_buddy_get_name(b));
    account = purple_buddy_get_account(b);
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
    PurpleChat *c = (PurpleChat *)node;
    PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
    type = PURPLE_LOG_CHAT;
    account = purple_chat_get_account(c);
    prpl_info = ((PurplePluginProtocolInfo *)( *( *purple_find_prpl(purple_account_get_protocol_id(account))).info).extra_info);
    if ((prpl_info != 0) && ((prpl_info -> get_chat_name) != 0)) {
      name = ( *(prpl_info -> get_chat_name))(purple_chat_get_components(c));
    }
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    pidgin_log_show_contact(((PurpleContact *)node));
    pidgin_clear_cursor((gtkblist -> window));
    return ;
  }
  else {
    pidgin_clear_cursor((gtkblist -> window));
/* This callback should not have been registered for a node
		 * that doesn't match the type of one of the blocks above. */
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","gtkblist.c",771,((const char *)__func__));
      return ;
    }while (0);
  }
  if ((name != 0) && (account != 0)) {
    pidgin_log_show(type,name,account);
    pidgin_clear_cursor((gtkblist -> window));
  }
  g_free(name);
}

static void gtk_blist_menu_showoffline_cb(GtkWidget *w,PurpleBlistNode *node)
{
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    purple_blist_node_set_bool(node,"show_offline",!(purple_blist_node_get_bool(node,"show_offline") != 0));
    pidgin_blist_update(purple_get_blist(),node);
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    PurpleBlistNode *bnode;
    gboolean setting = !(purple_blist_node_get_bool(node,"show_offline") != 0);
    purple_blist_node_set_bool(node,"show_offline",setting);
    for (bnode = purple_blist_node_get_first_child(node); bnode != ((PurpleBlistNode *)((void *)0)); bnode = purple_blist_node_get_sibling_next(bnode)) {
      purple_blist_node_set_bool(bnode,"show_offline",setting);
      pidgin_blist_update(purple_get_blist(),bnode);
    }
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
    PurpleBlistNode *cnode;
    PurpleBlistNode *bnode;
    gboolean setting = !(purple_blist_node_get_bool(node,"show_offline") != 0);
    purple_blist_node_set_bool(node,"show_offline",setting);
    for (cnode = purple_blist_node_get_first_child(node); cnode != ((PurpleBlistNode *)((void *)0)); cnode = purple_blist_node_get_sibling_next(cnode)) {
      purple_blist_node_set_bool(cnode,"show_offline",setting);
      for (bnode = purple_blist_node_get_first_child(cnode); bnode != ((PurpleBlistNode *)((void *)0)); bnode = purple_blist_node_get_sibling_next(bnode)) {
        purple_blist_node_set_bool(bnode,"show_offline",setting);
        pidgin_blist_update(purple_get_blist(),bnode);
      }
    }
  }
}

static void gtk_blist_show_systemlog_cb()
{
  pidgin_syslog_show();
}

static void gtk_blist_show_onlinehelp_cb()
{
  purple_notify_uri(0,"http://pidgin.im/documentation");
}

static void do_join_chat(PidginChatData *data)
{
  if (data != 0) {
    GHashTable *components = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
    GList *tmp;
    PurpleChat *chat;
    for (tmp = (data -> entries); tmp != ((GList *)((void *)0)); tmp = (tmp -> next)) {
      if (g_object_get_data((tmp -> data),"is_spin") != 0) {
        g_hash_table_replace(components,(g_strdup((g_object_get_data((tmp -> data),"identifier")))),(g_strdup_printf("%d",gtk_spin_button_get_value_as_int((tmp -> data)))));
      }
      else {
        g_hash_table_replace(components,(g_strdup((g_object_get_data((tmp -> data),"identifier")))),(g_strdup(gtk_entry_get_text((tmp -> data)))));
      }
    }
    chat = purple_chat_new(data -> rq_data.account,0,components);
    gtk_blist_join_chat(chat);
    purple_blist_remove_chat(chat);
  }
}

static void do_joinchat(GtkWidget *dialog,int id,PidginChatData *info)
{
  switch(id){
    case -5:
{
      do_join_chat(info);
      break; 
    }
    case 1:
{
      pidgin_roomlist_dialog_show_with_account(info -> rq_data.account);
      return ;
      break; 
    }
  }
  gtk_widget_destroy(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_widget_get_type()))));
  g_list_free((info -> entries));
  g_free(info);
}
/*
 * Check the values of all the text entry boxes.  If any required input
 * strings are empty then don't allow the user to click on "OK."
 */

static void set_sensitive_if_input_cb(GtkWidget *entry,gpointer user_data)
{
  PurplePluginProtocolInfo *prpl_info;
  PurpleConnection *gc;
  PidginChatData *data;
  GList *tmp;
  const char *text;
  gboolean required;
  gboolean sensitive = (!0);
  data = user_data;
  for (tmp = (data -> entries); tmp != ((GList *)((void *)0)); tmp = (tmp -> next)) {
    if (!(g_object_get_data((tmp -> data),"is_spin") != 0)) {
      required = ((gint )((glong )(g_object_get_data((tmp -> data),"required"))));
      text = gtk_entry_get_text((tmp -> data));
      if ((required != 0) && (( *text) == 0)) 
        sensitive = 0;
    }
  }
  gtk_dialog_set_response_sensitive(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)data -> rq_data.window),gtk_dialog_get_type()))),GTK_RESPONSE_OK,sensitive);
  gc = purple_account_get_connection(data -> rq_data.account);
  prpl_info = ((gc != ((PurpleConnection *)((void *)0)))?((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info) : ((struct _PurplePluginProtocolInfo *)((void *)0)));
  sensitive = ((prpl_info != ((PurplePluginProtocolInfo *)((void *)0))) && ((prpl_info -> roomlist_get_list) != ((PurpleRoomlist *(*)(PurpleConnection *))((void *)0))));
  gtk_dialog_set_response_sensitive(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)data -> rq_data.window),gtk_dialog_get_type()))),1,sensitive);
}

static void pidgin_blist_update_privacy_cb(PurpleBuddy *buddy)
{
  struct _pidgin_blist_node *ui_data = (purple_blist_node_get_ui_data(((PurpleBlistNode *)buddy)));
  if ((ui_data == ((struct _pidgin_blist_node *)((void *)0))) || ((ui_data -> row) == ((GtkTreeRowReference *)((void *)0)))) 
    return ;
  pidgin_blist_update_buddy(purple_get_blist(),((PurpleBlistNode *)buddy),(!0));
}

static gboolean chat_account_filter_func(PurpleAccount *account)
{
  PurpleConnection *gc = purple_account_get_connection(account);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  if (gc == ((PurpleConnection *)((void *)0))) 
    return 0;
  prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info);
  return (prpl_info -> chat_info) != ((GList *(*)(PurpleConnection *))((void *)0));
}

gboolean pidgin_blist_joinchat_is_showable()
{
  GList *c;
  PurpleConnection *gc;
  for (c = purple_connections_get_all(); c != ((GList *)((void *)0)); c = (c -> next)) {
    gc = (c -> data);
    if (chat_account_filter_func(purple_connection_get_account(gc)) != 0) 
      return (!0);
  }
  return 0;
}

static GtkWidget *make_blist_request_dialog(PidginBlistRequestData *data,PurpleAccount *account,const char *title,const char *window_role,const char *label_text,GCallback callback_func,PurpleFilterAccountFunc filter_func,GCallback response_cb)
{
  GtkWidget *label;
  GtkWidget *img;
  GtkWidget *hbox;
  GtkWidget *vbox;
  GtkWindow *blist_window;
  PidginBuddyList *gtkblist;
  data -> account = account;
  img = gtk_image_new_from_stock("pidgin-dialog-question",gtk_icon_size_from_name("pidgin-icon-size-tango-huge"));
  gtkblist = ((PidginBuddyList *)(purple_blist_get_ui_data()));
  blist_window = ((gtkblist != 0)?((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_window_get_type()))) : ((struct _GtkWindow *)((void *)0)));
  data -> window = gtk_dialog_new_with_buttons(title,blist_window,GTK_DIALOG_NO_SEPARATOR,0);
  gtk_window_set_transient_for(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(data -> window)),gtk_window_get_type()))),blist_window);
  gtk_dialog_set_default_response(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(data -> window)),gtk_dialog_get_type()))),GTK_RESPONSE_OK);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(data -> window)),gtk_container_get_type()))),6);
  gtk_window_set_resizable(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(data -> window)),gtk_window_get_type()))),0);
  gtk_box_set_spacing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(data -> window)),gtk_dialog_get_type())))).vbox),gtk_box_get_type()))),12);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(data -> window)),gtk_dialog_get_type())))).vbox),gtk_container_get_type()))),6);
  gtk_window_set_role(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(data -> window)),gtk_window_get_type()))),window_role);
  hbox = gtk_hbox_new(0,12);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(data -> window)),gtk_dialog_get_type())))).vbox),gtk_container_get_type()))),hbox);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),img,0,0,0);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)img),gtk_misc_get_type()))),0,0);
  vbox = gtk_vbox_new(0,5);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_container_get_type()))),vbox);
  label = gtk_label_new(label_text);
  gtk_widget_set_size_request(label,400,(-1));
  gtk_label_set_line_wrap(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(!0));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),label,0,0,0);
  data -> sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  data -> account_menu = pidgin_account_option_menu_new(account,0,callback_func,filter_func,data);
  pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),((const char *)(dgettext("pidgin","A_ccount"))),(data -> sg),(data -> account_menu),(!0),0);
  data -> vbox = ((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_vbox_new(0,5))),gtk_box_get_type())));
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(data -> vbox)),gtk_container_get_type()))),0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(data -> vbox)),gtk_widget_get_type()))),0,0,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(data -> window)),((GType )(20 << 2))))),"response",response_cb,data,0,((GConnectFlags )0));
  g_object_unref((data -> sg));
  return vbox;
}

static void rebuild_chat_entries(PidginChatData *data,const char *default_chat_name)
{
  PurpleConnection *gc;
  GList *list = (GList *)((void *)0);
  GList *tmp;
  GHashTable *defaults = (GHashTable *)((void *)0);
  struct proto_chat_entry *pce;
  gboolean focus = (!0);
  do {
    if (data -> rq_data.account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"data->rq_data.account != NULL");
      return ;
    };
  }while (0);
  gc = purple_account_get_connection(data -> rq_data.account);
  gtk_container_foreach(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)data -> rq_data.vbox),gtk_container_get_type()))),((GtkCallback )gtk_widget_destroy),0);
  g_list_free((data -> entries));
  data -> entries = ((GList *)((void *)0));
  if (( *((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info)).chat_info != ((GList *(*)(PurpleConnection *))((void *)0))) 
    list = ( *( *((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info)).chat_info)(gc);
  if (( *((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info)).chat_info_defaults != ((GHashTable *(*)(PurpleConnection *, const char *))((void *)0))) 
    defaults = ( *( *((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info)).chat_info_defaults)(gc,default_chat_name);
  for (tmp = list; tmp != 0; tmp = (tmp -> next)) {
    GtkWidget *input;
    pce = (tmp -> data);
    if ((pce -> is_int) != 0) {
      GtkObject *adjust;
      adjust = gtk_adjustment_new((pce -> min),(pce -> min),(pce -> max),1,10,10);
      input = gtk_spin_button_new(((GtkAdjustment *)(g_type_check_instance_cast(((GTypeInstance *)adjust),gtk_adjustment_get_type()))),1,0);
      gtk_widget_set_size_request(input,50,(-1));
      pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)data -> rq_data.vbox),gtk_box_get_type()))),(pce -> label),data -> rq_data.sg,input,0,0);
    }
    else {
      char *value;
      input = gtk_entry_new();
      gtk_entry_set_activates_default(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)input),gtk_entry_get_type()))),(!0));
      value = (g_hash_table_lookup(defaults,(pce -> identifier)));
      if (value != ((char *)((void *)0))) 
        gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)input),gtk_entry_get_type()))),value);
      if ((pce -> secret) != 0) {
        gtk_entry_set_visibility(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)input),gtk_entry_get_type()))),0);
#if !GTK_CHECK_VERSION(2,16,0)
#endif /* Less than GTK+ 2.16 */
      }
      pidgin_add_widget_to_vbox(data -> rq_data.vbox,(pce -> label),data -> rq_data.sg,input,(!0),0);
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)input),((GType )(20 << 2))))),"changed",((GCallback )set_sensitive_if_input_cb),data,0,((GConnectFlags )0));
    }
/* Do the following for any type of input widget */
    if (focus != 0) {
      gtk_widget_grab_focus(input);
      focus = 0;
    }
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)input),((GType )(20 << 2))))),"identifier",((gpointer )(pce -> identifier)));
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)input),((GType )(20 << 2))))),"is_spin",((gpointer )((glong )(pce -> is_int))));
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)input),((GType )(20 << 2))))),"required",((gpointer )((glong )(pce -> required))));
    data -> entries = g_list_append((data -> entries),input);
    g_free(pce);
  }
  g_list_free(list);
  g_hash_table_destroy(defaults);
/* Set whether the "OK" button should be clickable initially */
  set_sensitive_if_input_cb(0,data);
  gtk_widget_show_all(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)data -> rq_data.vbox),gtk_widget_get_type()))));
}

static void chat_select_account_cb(GObject *w,PurpleAccount *account,PidginChatData *data)
{
  if (strcmp(purple_account_get_protocol_id(data -> rq_data.account),purple_account_get_protocol_id(account)) == 0) {
    data -> rq_data.account = account;
  }
  else {
    data -> rq_data.account = account;
    rebuild_chat_entries(data,(data -> default_chat_name));
  }
}

void pidgin_blist_joinchat_show()
{
  PidginChatData *data = (PidginChatData *)((void *)0);
  data = ((PidginChatData *)(g_malloc0_n(1,(sizeof(PidginChatData )))));
  make_blist_request_dialog(((PidginBlistRequestData *)data),0,((const char *)(dgettext("pidgin","Join a Chat"))),"join_chat",((const char *)(dgettext("pidgin","Please enter the appropriate information about the chat you would like to join.\n"))),((GCallback )chat_select_account_cb),chat_account_filter_func,((GCallback )do_joinchat));
  gtk_dialog_add_buttons(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)data -> rq_data.window),gtk_dialog_get_type()))),((const char *)(dgettext("pidgin","Room _List"))),1,"gtk-cancel",GTK_RESPONSE_CANCEL,"pidgin-chat",GTK_RESPONSE_OK,((void *)((void *)0)));
  gtk_dialog_set_default_response(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)data -> rq_data.window),gtk_dialog_get_type()))),GTK_RESPONSE_OK);
  data -> default_chat_name = ((gchar *)((void *)0));
  data -> rq_data.account = pidgin_account_option_menu_get_selected(data -> rq_data.account_menu);
  rebuild_chat_entries(data,0);
  gtk_widget_show_all(data -> rq_data.window);
}

static void gtk_blist_row_expanded_cb(GtkTreeView *tv,GtkTreeIter *iter,GtkTreePath *path,gpointer user_data)
{
  PurpleBlistNode *node;
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),iter,NODE_COLUMN,&node,-1);
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
    char *title;
    title = pidgin_get_group_title(node,(!0));
    gtk_tree_store_set((gtkblist -> treemodel),iter,NAME_COLUMN,title,-1);
    g_free(title);
    purple_blist_node_set_bool(node,"collapsed",0);
    pidgin_blist_tooltip_destroy();
  }
}

static void gtk_blist_row_collapsed_cb(GtkTreeView *tv,GtkTreeIter *iter,GtkTreePath *path,gpointer user_data)
{
  PurpleBlistNode *node;
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),iter,NODE_COLUMN,&node,-1);
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
    char *title;
    struct _pidgin_blist_node *gtknode;
    PurpleBlistNode *cnode;
    title = pidgin_get_group_title(node,0);
    gtk_tree_store_set((gtkblist -> treemodel),iter,NAME_COLUMN,title,-1);
    g_free(title);
    purple_blist_node_set_bool(node,"collapsed",(!0));
    for (cnode = purple_blist_node_get_first_child(node); cnode != 0; cnode = purple_blist_node_get_sibling_next(cnode)) {
      if ((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CONTACT_NODE) {
        gtknode = (purple_blist_node_get_ui_data(cnode));
        if (!((gtknode -> contact_expanded) != 0)) 
          continue; 
        gtknode -> contact_expanded = 0;
        pidgin_blist_update_contact(0,cnode);
      }
    }
    pidgin_blist_tooltip_destroy();
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    pidgin_blist_collapse_contact_cb(0,node);
  }
}

static void gtk_blist_row_activated_cb(GtkTreeView *tv,GtkTreePath *path,GtkTreeViewColumn *col,gpointer data)
{
  PurpleBlistNode *node;
  GtkTreeIter iter;
  gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,path);
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,NODE_COLUMN,&node,-1);
  if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)) {
    PurpleBuddy *buddy;
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) 
      buddy = purple_contact_get_priority_buddy(((PurpleContact *)node));
    else 
      buddy = ((PurpleBuddy *)node);
    pidgin_dialogs_im_with_user(purple_buddy_get_account(buddy),purple_buddy_get_name(buddy));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
    gtk_blist_join_chat(((PurpleChat *)node));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
/*		if (gtk_tree_view_row_expanded(tv, path))
			gtk_tree_view_collapse_row(tv, path);
		else
			gtk_tree_view_expand_row(tv,path,FALSE);*/
  }
}

static void pidgin_blist_add_chat_cb()
{
  GtkTreeSelection *sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type()))));
  GtkTreeIter iter;
  PurpleBlistNode *node;
  if (gtk_tree_selection_get_selected(sel,0,&iter) != 0) {
    gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,NODE_COLUMN,&node,-1);
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) 
      purple_blist_request_add_chat(0,purple_buddy_get_group(((PurpleBuddy *)node)),0,0);
    if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE)) 
      purple_blist_request_add_chat(0,purple_contact_get_group(((PurpleContact *)node)),0,0);
    else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) 
      purple_blist_request_add_chat(0,((PurpleGroup *)node),0,0);
  }
  else {
    purple_blist_request_add_chat(0,0,0,0);
  }
}

static void pidgin_blist_add_buddy_cb()
{
  GtkTreeSelection *sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type()))));
  GtkTreeIter iter;
  PurpleBlistNode *node;
  if (gtk_tree_selection_get_selected(sel,0,&iter) != 0) {
    gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,NODE_COLUMN,&node,-1);
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
      PurpleGroup *group = purple_buddy_get_group(((PurpleBuddy *)node));
      purple_blist_request_add_buddy(0,0,purple_group_get_name(group),0);
    }
    else if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE)) {
      PurpleGroup *group = purple_contact_get_group(((PurpleContact *)node));
      purple_blist_request_add_buddy(0,0,purple_group_get_name(group),0);
    }
    else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
      purple_blist_request_add_buddy(0,0,purple_group_get_name(((PurpleGroup *)node)),0);
    }
  }
  else {
    purple_blist_request_add_buddy(0,0,0,0);
  }
}

static void pidgin_blist_remove_cb(GtkWidget *w,PurpleBlistNode *node)
{
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    pidgin_dialogs_remove_buddy(((PurpleBuddy *)node));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
    pidgin_dialogs_remove_chat(((PurpleChat *)node));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
    pidgin_dialogs_remove_group(((PurpleGroup *)node));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    pidgin_dialogs_remove_contact(((PurpleContact *)node));
  }
}

struct _expand 
{
  GtkTreeView *treeview;
  GtkTreePath *path;
  PurpleBlistNode *node;
}
;

static gboolean scroll_to_expanded_cell(gpointer data)
{
  struct _expand *ex = data;
  gtk_tree_view_scroll_to_cell((ex -> treeview),(ex -> path),0,0,0,0);
  pidgin_blist_update_contact(0,(ex -> node));
  gtk_tree_path_free((ex -> path));
  g_free(ex);
  return 0;
}

static void pidgin_blist_expand_contact_cb(GtkWidget *w,PurpleBlistNode *node)
{
  struct _pidgin_blist_node *gtknode;
  GtkTreeIter iter;
  GtkTreeIter parent;
  PurpleBlistNode *bnode;
  GtkTreePath *path;
  if (!((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE)) 
    return ;
  gtknode = (purple_blist_node_get_ui_data(node));
  gtknode -> contact_expanded = (!0);
  for (bnode = purple_blist_node_get_first_child(node); bnode != 0; bnode = purple_blist_node_get_sibling_next(bnode)) {
    pidgin_blist_update(0,bnode);
  }
/* This ensures that the bottom buddy is visible, i.e. not scrolled off the alignment */
  if (get_iter_from_node(node,&parent) != 0) {
    struct _expand *ex = (struct _expand *)(g_malloc0_n(1,(sizeof(struct _expand ))));
    gtk_tree_model_iter_nth_child(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,&parent,(gtk_tree_model_iter_n_children(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&parent) - 1));
    path = gtk_tree_model_get_path(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter);
/* Let the treeview draw so it knows where to scroll */
    ex -> treeview = ((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type())));
    ex -> path = path;
    ex -> node = purple_blist_node_get_first_child(node);
    g_idle_add(scroll_to_expanded_cell,ex);
  }
}

static void pidgin_blist_collapse_contact_cb(GtkWidget *w,PurpleBlistNode *node)
{
  PurpleBlistNode *bnode;
  struct _pidgin_blist_node *gtknode;
  if (!((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE)) 
    return ;
  gtknode = (purple_blist_node_get_ui_data(node));
  gtknode -> contact_expanded = 0;
  for (bnode = purple_blist_node_get_first_child(node); bnode != 0; bnode = purple_blist_node_get_sibling_next(bnode)) {
    pidgin_blist_update(0,bnode);
  }
}

static void toggle_privacy(GtkWidget *widget,PurpleBlistNode *node)
{
  PurpleBuddy *buddy;
  PurpleAccount *account;
  gboolean permitted;
  const char *name;
  if (!((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)) 
    return ;
  buddy = ((PurpleBuddy *)node);
  account = purple_buddy_get_account(buddy);
  name = purple_buddy_get_name(buddy);
  permitted = purple_privacy_check(account,name);
/* XXX: Perhaps ask whether to restore the previous lists where appropirate? */
  if (permitted != 0) 
    purple_privacy_deny(account,name,0,0);
  else 
    purple_privacy_allow(account,name,0,0);
  pidgin_blist_update(purple_get_blist(),node);
}

void pidgin_append_blist_node_privacy_menu(GtkWidget *menu,PurpleBlistNode *node)
{
  PurpleBuddy *buddy = (PurpleBuddy *)node;
  PurpleAccount *account;
  gboolean permitted;
  account = purple_buddy_get_account(buddy);
  permitted = purple_privacy_check(account,purple_buddy_get_name(buddy));
  pidgin_new_item_from_stock(menu,((permitted != 0)?((const char *)(dgettext("pidgin","_Block"))) : ((const char *)(dgettext("pidgin","Un_block")))),((permitted != 0)?"pidgin-block" : "pidgin-unblock"),((GCallback )toggle_privacy),node,0,0,0);
}

void pidgin_append_blist_node_proto_menu(GtkWidget *menu,PurpleConnection *gc,PurpleBlistNode *node)
{
  GList *l;
  GList *ll;
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info;
  if (!(prpl_info != 0) || !((prpl_info -> blist_node_menu) != 0)) 
    return ;
  for (l = (ll = ( *(prpl_info -> blist_node_menu))(node)); l != 0; l = (l -> next)) {
    PurpleMenuAction *act = (PurpleMenuAction *)(l -> data);
    pidgin_append_menu_action(menu,act,node);
  }
  g_list_free(ll);
}

void pidgin_append_blist_node_extended_menu(GtkWidget *menu,PurpleBlistNode *node)
{
  GList *l;
  GList *ll;
  for (l = (ll = purple_blist_node_get_extended_menu(node)); l != 0; l = (l -> next)) {
    PurpleMenuAction *act = (PurpleMenuAction *)(l -> data);
    pidgin_append_menu_action(menu,act,node);
  }
  g_list_free(ll);
}

static void pidgin_append_blist_node_move_to_menu(GtkWidget *menu,PurpleBlistNode *node)
{
  GtkWidget *submenu;
  GtkWidget *menuitem;
  PurpleBlistNode *group;
  menuitem = gtk_menu_item_new_with_label(((const char *)(dgettext("pidgin","Move to"))));
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem);
  gtk_widget_show(menuitem);
  submenu = gtk_menu_new();
  gtk_menu_item_set_submenu(((GtkMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_menu_item_get_type()))),submenu);
  for (group = purple_blist_get_root(); group != 0; group = purple_blist_node_get_sibling_next(group)) {
    if (!((purple_blist_node_get_type(group)) == PURPLE_BLIST_GROUP_NODE)) 
      continue; 
    if (group == purple_blist_node_get_parent(node)) 
      continue; 
    menuitem = pidgin_new_item_from_stock(submenu,purple_group_get_name(((PurpleGroup *)group)),0,((GCallback )gtk_blist_menu_move_to_cb),node,0,0,0);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"groupnode",group);
  }
  gtk_widget_show_all(submenu);
}

void pidgin_blist_make_buddy_menu(GtkWidget *menu,PurpleBuddy *buddy,gboolean sub)
{
  PurpleAccount *account = (PurpleAccount *)((void *)0);
  PurpleConnection *pc = (PurpleConnection *)((void *)0);
  PurplePluginProtocolInfo *prpl_info;
  PurpleContact *contact;
  PurpleBlistNode *node;
  gboolean contact_expanded = 0;
  do {
    if (menu != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"menu");
      return ;
    };
  }while (0);
  do {
    if (buddy != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy");
      return ;
    };
  }while (0);
  account = purple_buddy_get_account(buddy);
  pc = purple_account_get_connection(account);
  prpl_info = ((PurplePluginProtocolInfo *)( *( *purple_connection_get_prpl(pc)).info).extra_info);
  node = ((PurpleBlistNode *)buddy);
  contact = purple_buddy_get_contact(buddy);
  if (contact != 0) {
    PidginBlistNode *node = (purple_blist_node_get_ui_data(((PurpleBlistNode *)contact)));
    contact_expanded = (node -> contact_expanded);
  }
  if ((prpl_info != 0) && ((prpl_info -> get_info) != 0)) {
    pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Get _Info"))),"pidgin-info",((GCallback )gtk_blist_menu_info_cb),buddy,0,0,0);
  }
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","I_M"))),"pidgin-message-new",((GCallback )gtk_blist_menu_im_cb),buddy,0,0,0);
#ifdef USE_VV
  if ((prpl_info != 0) && ((prpl_info -> get_media_caps) != 0)) {
    PurpleAccount *account = purple_buddy_get_account(buddy);
    const gchar *who = purple_buddy_get_name(buddy);
    PurpleMediaCaps caps = purple_prpl_get_media_caps(account,who);
    if ((caps & PURPLE_MEDIA_CAPS_AUDIO) != 0U) {
      pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Audio Call"))),"pidgin-audio-call",((GCallback )gtk_blist_menu_audio_call_cb),buddy,0,0,0);
    }
    if ((caps & PURPLE_MEDIA_CAPS_AUDIO_VIDEO) != 0U) {
      pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Audio/_Video Call"))),"pidgin-video-call",((GCallback )gtk_blist_menu_video_call_cb),buddy,0,0,0);
    }
    else if ((caps & PURPLE_MEDIA_CAPS_VIDEO) != 0U) {
      pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Video Call"))),"pidgin-video-call",((GCallback )gtk_blist_menu_video_call_cb),buddy,0,0,0);
    }
  }
#endif
  if ((prpl_info != 0) && ((prpl_info -> send_file) != 0)) {
    if (!((prpl_info -> can_receive_file) != 0) || (( *(prpl_info -> can_receive_file))(( *(buddy -> account)).gc,(buddy -> name)) != 0)) {
      pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Send File..."))),"pidgin-send-file",((GCallback )gtk_blist_menu_send_file_cb),buddy,0,0,0);
    }
  }
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Add Buddy _Pounce..."))),0,((GCallback )gtk_blist_menu_bp_cb),buddy,0,0,0);
  if (((((node -> parent) != 0) && (( *( *(node -> parent)).child).next != 0)) && !(sub != 0)) && !(contact_expanded != 0)) {
    pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","View _Log"))),0,((GCallback )gtk_blist_menu_showlog_cb),contact,0,0,0);
  }
  else if (!(sub != 0)) {
    pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","View _Log"))),0,((GCallback )gtk_blist_menu_showlog_cb),buddy,0,0,0);
  }
  if (!(((purple_blist_node_get_flags(((PurpleBlistNode *)node))) & PURPLE_BLIST_NODE_FLAG_NO_SAVE) != 0U)) {
    gboolean show_offline = purple_blist_node_get_bool(node,"show_offline");
    pidgin_new_item_from_stock(menu,((show_offline != 0)?((const char *)(dgettext("pidgin","Hide When Offline"))) : ((const char *)(dgettext("pidgin","Show When Offline")))),0,((GCallback )gtk_blist_menu_showoffline_cb),node,0,0,0);
  }
  pidgin_append_blist_node_proto_menu(menu,( *(buddy -> account)).gc,node);
  pidgin_append_blist_node_extended_menu(menu,node);
  if (!(contact_expanded != 0) && (contact != ((PurpleContact *)((void *)0)))) 
    pidgin_append_blist_node_move_to_menu(menu,((PurpleBlistNode *)contact));
  if (((((node -> parent) != 0) && (( *( *(node -> parent)).child).next != 0)) && !(sub != 0)) && !(contact_expanded != 0)) {
    pidgin_separator(menu);
    pidgin_append_blist_node_privacy_menu(menu,node);
    pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Alias..."))),"pidgin-alias",((GCallback )gtk_blist_menu_alias_cb),contact,0,0,0);
    pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Remove"))),"gtk-remove",((GCallback )pidgin_blist_remove_cb),contact,0,0,0);
  }
  else if (!(sub != 0) || (contact_expanded != 0)) {
    pidgin_separator(menu);
    pidgin_append_blist_node_privacy_menu(menu,node);
    pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Alias..."))),"pidgin-alias",((GCallback )gtk_blist_menu_alias_cb),buddy,0,0,0);
    pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Remove"))),"gtk-remove",((GCallback )pidgin_blist_remove_cb),buddy,0,0,0);
  }
}

static gboolean gtk_blist_key_press_cb(GtkWidget *tv,GdkEventKey *event,gpointer data)
{
  PurpleBlistNode *node;
  GtkTreeIter iter;
  GtkTreeIter parent;
  GtkTreeSelection *sel;
  GtkTreePath *path;
  sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gtk_tree_view_get_type()))));
  if (!(gtk_tree_selection_get_selected(sel,0,&iter) != 0)) 
    return 0;
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,NODE_COLUMN,&node,-1);
  if ((((event -> state) & GDK_CONTROL_MASK) != 0U) && (((event -> keyval) == 'o') || ((event -> keyval) == 'O'))) {
    PurpleBuddy *buddy;
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
      buddy = purple_contact_get_priority_buddy(((PurpleContact *)node));
    }
    else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
      buddy = ((PurpleBuddy *)node);
    }
    else {
      return 0;
    }
    if (buddy != 0) 
      pidgin_retrieve_user_info(( *(buddy -> account)).gc,(buddy -> name));
  }
  else {
    switch(event -> keyval){
      case 0xffbf:
{
        gtk_blist_menu_alias_cb(tv,node);
        break; 
      }
      case 0xff51:
{
        path = gtk_tree_model_get_path(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter);
        if (gtk_tree_view_row_expanded(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gtk_tree_view_get_type()))),path) != 0) {
/* Collapse the Group */
          gtk_tree_view_collapse_row(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gtk_tree_view_get_type()))),path);
          gtk_tree_path_free(path);
          return (!0);
        }
        else {
/* Select the Parent */
          if (gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,path) != 0) {
            if (gtk_tree_model_iter_parent(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&parent,&iter) != 0) {
              gtk_tree_path_free(path);
              path = gtk_tree_model_get_path(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&parent);
              gtk_tree_view_set_cursor(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gtk_tree_view_get_type()))),path,0,0);
              gtk_tree_path_free(path);
              return (!0);
            }
          }
        }
        gtk_tree_path_free(path);
        break; 
      }
      case 0xff53:
{
        path = gtk_tree_model_get_path(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter);
        if (!(gtk_tree_view_row_expanded(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gtk_tree_view_get_type()))),path) != 0)) {
/* Expand the Group */
          if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
            pidgin_blist_expand_contact_cb(0,node);
            gtk_tree_path_free(path);
            return (!0);
          }
          else if (!((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)) {
            gtk_tree_view_expand_row(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gtk_tree_view_get_type()))),path,0);
            gtk_tree_path_free(path);
            return (!0);
          }
        }
        else {
/* Select the First Child */
          if (gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&parent,path) != 0) {
            if (gtk_tree_model_iter_nth_child(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,&parent,0) != 0) {
              gtk_tree_path_free(path);
              path = gtk_tree_model_get_path(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter);
              gtk_tree_view_set_cursor(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gtk_tree_view_get_type()))),path,0,0);
              gtk_tree_path_free(path);
              return (!0);
            }
          }
        }
        gtk_tree_path_free(path);
        break; 
      }
    }
  }
  return 0;
}

static void set_node_custom_icon_cb(const gchar *filename,gpointer data)
{
  if (filename != 0) {
    PurpleBlistNode *node = (PurpleBlistNode *)data;
    purple_buddy_icons_node_set_custom_icon_from_file(node,filename);
  }
}

static void set_node_custom_icon(GtkWidget *w,PurpleBlistNode *node)
{
/* This doesn't keep track of the returned dialog (so that successive
	 * calls could be made to re-display that dialog). Do we want that? */
  GtkWidget *win = pidgin_buddy_icon_chooser_new(0,set_node_custom_icon_cb,node);
  gtk_widget_show_all(win);
}

static void remove_node_custom_icon(GtkWidget *w,PurpleBlistNode *node)
{
  purple_buddy_icons_node_set_custom_icon(node,0,0);
}

static void add_buddy_icon_menu_items(GtkWidget *menu,PurpleBlistNode *node)
{
  GtkWidget *item;
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Set Custom Icon"))),0,((GCallback )set_node_custom_icon),node,0,0,0);
  item = pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Remove Custom Icon"))),0,((GCallback )remove_node_custom_icon),node,0,0,0);
  if (!(purple_buddy_icons_node_has_custom_icon(node) != 0)) 
    gtk_widget_set_sensitive(item,0);
}

static GtkWidget *create_group_menu(PurpleBlistNode *node,PurpleGroup *g)
{
  GtkWidget *menu;
  GtkWidget *item;
  menu = gtk_menu_new();
  item = pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Add _Buddy..."))),"gtk-add",((GCallback )pidgin_blist_add_buddy_cb),node,0,0,0);
  gtk_widget_set_sensitive(item,(purple_connections_get_all() != ((GList *)((void *)0))));
  item = pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Add C_hat..."))),"gtk-add",((GCallback )pidgin_blist_add_chat_cb),node,0,0,0);
  gtk_widget_set_sensitive(item,pidgin_blist_joinchat_is_showable());
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Delete Group"))),"gtk-remove",((GCallback )pidgin_blist_remove_cb),node,0,0,0);
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Rename"))),0,((GCallback )gtk_blist_menu_alias_cb),node,0,0,0);
  if (!(((purple_blist_node_get_flags(node)) & PURPLE_BLIST_NODE_FLAG_NO_SAVE) != 0U)) {
    gboolean show_offline = purple_blist_node_get_bool(node,"show_offline");
    pidgin_new_item_from_stock(menu,((show_offline != 0)?((const char *)(dgettext("pidgin","Hide When Offline"))) : ((const char *)(dgettext("pidgin","Show When Offline")))),0,((GCallback )gtk_blist_menu_showoffline_cb),node,0,0,0);
  }
  add_buddy_icon_menu_items(menu,node);
  pidgin_append_blist_node_extended_menu(menu,node);
  return menu;
}

static GtkWidget *create_chat_menu(PurpleBlistNode *node,PurpleChat *c)
{
  GtkWidget *menu;
  gboolean autojoin;
  gboolean persistent;
  menu = gtk_menu_new();
  autojoin = purple_blist_node_get_bool(node,"gtk-autojoin");
  persistent = purple_blist_node_get_bool(node,"gtk-persistent");
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Join"))),"pidgin-chat",((GCallback )gtk_blist_menu_join_cb),node,0,0,0);
  pidgin_new_check_item(menu,((const char *)(dgettext("pidgin","Auto-Join"))),((GCallback )gtk_blist_menu_autojoin_cb),node,autojoin);
  pidgin_new_check_item(menu,((const char *)(dgettext("pidgin","Persistent"))),((GCallback )gtk_blist_menu_persistent_cb),node,persistent);
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","View _Log"))),0,((GCallback )gtk_blist_menu_showlog_cb),node,0,0,0);
  pidgin_append_blist_node_proto_menu(menu,( *(c -> account)).gc,node);
  pidgin_append_blist_node_extended_menu(menu,node);
  pidgin_separator(menu);
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Edit Settings..."))),0,((GCallback )chat_components_edit),node,0,0,0);
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Alias..."))),"pidgin-alias",((GCallback )gtk_blist_menu_alias_cb),node,0,0,0);
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Remove"))),"gtk-remove",((GCallback )pidgin_blist_remove_cb),node,0,0,0);
  add_buddy_icon_menu_items(menu,node);
  return menu;
}

static GtkWidget *create_contact_menu(PurpleBlistNode *node)
{
  GtkWidget *menu;
  menu = gtk_menu_new();
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","View _Log"))),0,((GCallback )gtk_blist_menu_showlog_cb),node,0,0,0);
  pidgin_separator(menu);
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Alias..."))),"pidgin-alias",((GCallback )gtk_blist_menu_alias_cb),node,0,0,0);
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Remove"))),"gtk-remove",((GCallback )pidgin_blist_remove_cb),node,0,0,0);
  add_buddy_icon_menu_items(menu,node);
  pidgin_separator(menu);
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Collapse"))),"gtk-zoom-out",((GCallback )pidgin_blist_collapse_contact_cb),node,0,0,0);
  pidgin_append_blist_node_extended_menu(menu,node);
  return menu;
}

static GtkWidget *create_buddy_menu(PurpleBlistNode *node,PurpleBuddy *b)
{
  struct _pidgin_blist_node *gtknode = (struct _pidgin_blist_node *)(node -> ui_data);
  GtkWidget *menu;
  GtkWidget *menuitem;
  gboolean show_offline = purple_prefs_get_bool("/pidgin/blist/show_offline_buddies");
  menu = gtk_menu_new();
  pidgin_blist_make_buddy_menu(menu,b,0);
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    pidgin_separator(menu);
    add_buddy_icon_menu_items(menu,node);
    if ((gtknode -> contact_expanded) != 0) {
      pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Collapse"))),"gtk-zoom-out",((GCallback )pidgin_blist_collapse_contact_cb),node,0,0,0);
    }
    else {
      pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Expand"))),"gtk-zoom-in",((GCallback )pidgin_blist_expand_contact_cb),node,0,0,0);
    }
    if (( *(node -> child)).next != 0) {
      PurpleBlistNode *bnode;
      for (bnode = (node -> child); bnode != 0; bnode = (bnode -> next)) {{
          PurpleBuddy *buddy = (PurpleBuddy *)bnode;
          GdkPixbuf *buf;
          GtkWidget *submenu;
          GtkWidget *image;
          if (buddy == b) 
            continue; 
          if (!(( *(buddy -> account)).gc != 0)) 
            continue; 
          if (!(show_offline != 0) && !(((buddy != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0))) 
            continue; 
          menuitem = gtk_image_menu_item_new_with_label((buddy -> name));
          buf = pidgin_create_prpl_icon((buddy -> account),PIDGIN_PRPL_ICON_SMALL);
          image = gtk_image_new_from_pixbuf(buf);
          g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buf),((GType )(20 << 2))))));
          gtk_image_menu_item_set_image(((GtkImageMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_image_menu_item_get_type()))),image);
          gtk_widget_show(image);
          gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem);
          gtk_widget_show(menuitem);
          submenu = gtk_menu_new();
          gtk_menu_item_set_submenu(((GtkMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_menu_item_get_type()))),submenu);
          gtk_widget_show(submenu);
          pidgin_blist_make_buddy_menu(submenu,buddy,(!0));
        }
      }
    }
  }
  return menu;
}

static gboolean pidgin_blist_show_context_menu(PurpleBlistNode *node,GtkMenuPositionFunc func,GtkWidget *tv,guint button,guint32 time)
{
  struct _pidgin_blist_node *gtknode;
  GtkWidget *menu = (GtkWidget *)((void *)0);
  gboolean handled = 0;
  gtknode = ((struct _pidgin_blist_node *)(node -> ui_data));
/* Create a menu based on the thing we right-clicked on */
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
    PurpleGroup *g = (PurpleGroup *)node;
    menu = create_group_menu(node,g);
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
    PurpleChat *c = (PurpleChat *)node;
    menu = create_chat_menu(node,c);
  }
  else if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) && ((gtknode -> contact_expanded) != 0)) {
    menu = create_contact_menu(node);
  }
  else if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)) {
    PurpleBuddy *b;
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) 
      b = purple_contact_get_priority_buddy(((PurpleContact *)node));
    else 
      b = ((PurpleBuddy *)node);
    menu = create_buddy_menu(node,b);
  }
#ifdef _WIN32
/* Unhook the tooltip-timeout since we don't want a tooltip
	 * to appear and obscure the context menu we are about to show
	   This is a workaround for GTK+ bug 107320. */
#endif
/* Now display the menu */
  if (menu != ((GtkWidget *)((void *)0))) {
    gtk_widget_show_all(menu);
    gtk_menu_popup(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_get_type()))),0,0,func,tv,button,time);
    handled = (!0);
  }
  return handled;
}

static gboolean gtk_blist_button_press_cb(GtkWidget *tv,GdkEventButton *event,gpointer user_data)
{
  GtkTreePath *path;
  PurpleBlistNode *node;
  GtkTreeIter iter;
  GtkTreeSelection *sel;
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  struct _pidgin_blist_node *gtknode;
  gboolean handled = 0;
/* Here we figure out which node was clicked */
  if (!(gtk_tree_view_get_path_at_pos(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gtk_tree_view_get_type()))),(event -> x),(event -> y),&path,0,0,0) != 0)) 
    return 0;
  gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,path);
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,NODE_COLUMN,&node,-1);
  gtknode = ((struct _pidgin_blist_node *)(node -> ui_data));
/* Right click draws a context menu */
  if (((event -> button) == 3) && ((event -> type) == GDK_BUTTON_PRESS)) {
    handled = pidgin_blist_show_context_menu(node,0,tv,3,(event -> time));
/* CTRL+middle click expands or collapse a contact */
  }
  else if (((((event -> button) == 2) && ((event -> type) == GDK_BUTTON_PRESS)) && (((event -> state) & GDK_CONTROL_MASK) != 0U)) && ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE)) {
    if ((gtknode -> contact_expanded) != 0) 
      pidgin_blist_collapse_contact_cb(0,node);
    else 
      pidgin_blist_expand_contact_cb(0,node);
    handled = (!0);
/* Double middle click gets info */
  }
  else if ((((event -> button) == 2) && ((event -> type) == GDK_2BUTTON_PRESS)) && (((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE))) {
    PurpleBuddy *b;
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) 
      b = purple_contact_get_priority_buddy(((PurpleContact *)node));
    else 
      b = ((PurpleBuddy *)node);
    prpl = purple_find_prpl(purple_account_get_protocol_id((b -> account)));
    if (prpl != ((PurplePlugin *)((void *)0))) 
      prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
    if ((prpl != 0) && ((prpl_info -> get_info) != 0)) 
      pidgin_retrieve_user_info(( *(b -> account)).gc,(b -> name));
    handled = (!0);
  }
#if (1)
/*
	 * This code only exists because GTK+ doesn't work.  If we return
	 * FALSE here, as would be normal the event propoagates down and
	 * somehow gets interpreted as the start of a drag event.
	 *
	 * Um, isn't it _normal_ to return TRUE here?  Since the event
	 * was handled?  --Mark
	 */
  if (handled != 0) {
    sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gtk_tree_view_get_type()))));
    gtk_tree_selection_select_path(sel,path);
    gtk_tree_path_free(path);
    return (!0);
  }
#endif
  gtk_tree_path_free(path);
  return 0;
}

static gboolean pidgin_blist_popup_menu_cb(GtkWidget *tv,void *user_data)
{
  PurpleBlistNode *node;
  GtkTreeIter iter;
  GtkTreeSelection *sel;
  gboolean handled = 0;
  sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gtk_tree_view_get_type()))));
  if (!(gtk_tree_selection_get_selected(sel,0,&iter) != 0)) 
    return 0;
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,NODE_COLUMN,&node,-1);
/* Shift+F10 draws a context menu */
  handled = pidgin_blist_show_context_menu(node,pidgin_treeview_popup_menu_position_func,tv,0,0L);
  return handled;
}

static void pidgin_blist_buddy_details_cb(gpointer data,guint action,GtkWidget *item)
{
  pidgin_set_cursor((gtkblist -> window),GDK_WATCH);
  purple_prefs_set_bool("/pidgin/blist/show_buddy_icons",gtk_check_menu_item_get_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_check_menu_item_get_type())))));
  pidgin_clear_cursor((gtkblist -> window));
}

static void pidgin_blist_show_idle_time_cb(gpointer data,guint action,GtkWidget *item)
{
  pidgin_set_cursor((gtkblist -> window),GDK_WATCH);
  purple_prefs_set_bool("/pidgin/blist/show_idle_time",gtk_check_menu_item_get_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_check_menu_item_get_type())))));
  pidgin_clear_cursor((gtkblist -> window));
}

static void pidgin_blist_show_protocol_icons_cb(gpointer data,guint action,GtkWidget *item)
{
  purple_prefs_set_bool("/pidgin/blist/show_protocol_icons",gtk_check_menu_item_get_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_check_menu_item_get_type())))));
}

static void pidgin_blist_show_empty_groups_cb(gpointer data,guint action,GtkWidget *item)
{
  pidgin_set_cursor((gtkblist -> window),GDK_WATCH);
  purple_prefs_set_bool("/pidgin/blist/show_empty_groups",gtk_check_menu_item_get_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_check_menu_item_get_type())))));
  pidgin_clear_cursor((gtkblist -> window));
}

static void pidgin_blist_edit_mode_cb(gpointer callback_data,guint callback_action,GtkWidget *checkitem)
{
  pidgin_set_cursor((gtkblist -> window),GDK_WATCH);
  purple_prefs_set_bool("/pidgin/blist/show_offline_buddies",gtk_check_menu_item_get_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)checkitem),gtk_check_menu_item_get_type())))));
  pidgin_clear_cursor((gtkblist -> window));
}

static void pidgin_blist_mute_sounds_cb(gpointer data,guint action,GtkWidget *item)
{
  purple_prefs_set_bool("/pidgin/sound/mute",( *((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_check_menu_item_get_type())))).active);
}

static void pidgin_blist_mute_pref_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_item_factory_get_item((gtkblist -> ift),"/Tools/Mute Sounds"))),gtk_check_menu_item_get_type()))),((gboolean )((glong )value)));
}

static void pidgin_blist_sound_method_pref_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  gboolean sensitive = (!0);
  if (!(strcmp(value,"none") != 0)) 
    sensitive = 0;
  gtk_widget_set_sensitive(gtk_item_factory_get_widget((gtkblist -> ift),"/Tools/Mute Sounds"),sensitive);
}

static void add_buddies_from_vcard(const char *prpl_id,PurpleGroup *group,GList *list,const char *alias)
{
  GList *l;
  PurpleAccount *account = (PurpleAccount *)((void *)0);
  PurpleConnection *gc;
  if (list == ((GList *)((void *)0))) 
    return ;
{
    for (l = purple_connections_get_all(); l != ((GList *)((void *)0)); l = (l -> next)) {
      gc = ((PurpleConnection *)(l -> data));
      account = purple_connection_get_account(gc);
      if (!(strcmp(purple_account_get_protocol_id(account),prpl_id) != 0)) 
        break; 
      account = ((PurpleAccount *)((void *)0));
    }
  }
  if (account != ((PurpleAccount *)((void *)0))) {
    for (l = list; l != ((GList *)((void *)0)); l = (l -> next)) {
      purple_blist_request_add_buddy(account,(l -> data),((group != 0)?(group -> name) : ((char *)((void *)0))),alias);
    }
  }
  g_list_foreach(list,((GFunc )g_free),0);
  g_list_free(list);
}

static gboolean parse_vcard(const char *vcard,PurpleGroup *group)
{
  char *temp_vcard;
  char *s;
  char *c;
  char *alias = (char *)((void *)0);
  GList *aims = (GList *)((void *)0);
  GList *icqs = (GList *)((void *)0);
  GList *yahoos = (GList *)((void *)0);
  GList *msns = (GList *)((void *)0);
  GList *jabbers = (GList *)((void *)0);
  s = (temp_vcard = g_strdup(vcard));
  while((( *s) != 0) && (strncmp(s,"END:vCard",strlen("END:vCard")) != 0)){{
      char *field;
      char *value;
      field = s;
/* Grab the field */
      while((((( *s) != 13) && (( *s) != 10)) && (( *s) != 0)) && (( *s) != ':'))
        s++;
      if (( *s) == 13) 
        s++;
      if (( *s) == 10) {
        s++;
        continue; 
      }
      if (( *s) != 0) 
         *(s++) = 0;
      if ((c = strchr(field,';')) != ((char *)((void *)0))) 
         *c = 0;
/* Proceed to the end of the line */
      value = s;
      while(((( *s) != 13) && (( *s) != 10)) && (( *s) != 0))
        s++;
      if (( *s) == 13) 
         *(s++) = 0;
      if (( *s) == 10) 
         *(s++) = 0;
/* We only want to worry about a few fields here. */
      if (!(strcmp(field,"FN") != 0)) 
        alias = g_strdup(value);
      else if ((((!(strcmp(field,"X-AIM") != 0) || !(strcmp(field,"X-ICQ") != 0)) || !(strcmp(field,"X-YAHOO") != 0)) || !(strcmp(field,"X-MSN") != 0)) || !(strcmp(field,"X-JABBER") != 0)) {
        char **values = g_strsplit(value,":",0);
        char **im;
        for (im = values;  *im != ((char *)((void *)0)); im++) {
          if (!(strcmp(field,"X-AIM") != 0)) 
            aims = g_list_append(aims,(g_strdup(( *im))));
          else if (!(strcmp(field,"X-ICQ") != 0)) 
            icqs = g_list_append(icqs,(g_strdup(( *im))));
          else if (!(strcmp(field,"X-YAHOO") != 0)) 
            yahoos = g_list_append(yahoos,(g_strdup(( *im))));
          else if (!(strcmp(field,"X-MSN") != 0)) 
            msns = g_list_append(msns,(g_strdup(( *im))));
          else if (!(strcmp(field,"X-JABBER") != 0)) 
            jabbers = g_list_append(jabbers,(g_strdup(( *im))));
        }
        g_strfreev(values);
      }
    }
  }
  g_free(temp_vcard);
  if (((((aims == ((GList *)((void *)0))) && (icqs == ((GList *)((void *)0)))) && (yahoos == ((GList *)((void *)0)))) && (msns == ((GList *)((void *)0)))) && (jabbers == ((GList *)((void *)0)))) {
    g_free(alias);
    return 0;
  }
  add_buddies_from_vcard("prpl-aim",group,aims,alias);
  add_buddies_from_vcard("prpl-icq",group,icqs,alias);
  add_buddies_from_vcard("prpl-yahoo",group,yahoos,alias);
  add_buddies_from_vcard("prpl-msn",group,msns,alias);
  add_buddies_from_vcard("prpl-jabber",group,jabbers,alias);
  g_free(alias);
  return (!0);
}
#ifdef _WIN32
/* Unhook the tooltip-timeout since we don't want a tooltip
	 * to appear and obscure the dragging operation.
	 * This is a workaround for GTK+ bug 107320. */
#endif

static void pidgin_blist_drag_data_get_cb(GtkWidget *widget,GdkDragContext *dc,GtkSelectionData *data,guint info,guint time,gpointer null)
{
  if ((data -> target) == gdk_atom_intern("PURPLE_BLIST_NODE",0)) {
    GtkTreeRowReference *ref = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dc),((GType )(20 << 2))))),"gtk-tree-view-source-row"));
    GtkTreePath *sourcerow = gtk_tree_row_reference_get_path(ref);
    GtkTreeIter iter;
    PurpleBlistNode *node = (PurpleBlistNode *)((void *)0);
    if (!(sourcerow != 0)) 
      return ;
    gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,sourcerow);
    gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,NODE_COLUMN,&node,-1);
    gtk_selection_data_set(data,gdk_atom_intern("PURPLE_BLIST_NODE",0),8,((void *)(&node)),(sizeof(node)));
/* bits */
    gtk_tree_path_free(sourcerow);
  }
  else if ((data -> target) == gdk_atom_intern("application/x-im-contact",0)) {
    GtkTreeRowReference *ref;
    GtkTreePath *sourcerow;
    GtkTreeIter iter;
    PurpleBlistNode *node = (PurpleBlistNode *)((void *)0);
    PurpleBuddy *buddy;
    PurpleConnection *gc;
    GString *str;
    const char *protocol;
    ref = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dc),((GType )(20 << 2))))),"gtk-tree-view-source-row"));
    sourcerow = gtk_tree_row_reference_get_path(ref);
    if (!(sourcerow != 0)) 
      return ;
    gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,sourcerow);
    gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,NODE_COLUMN,&node,-1);
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
      buddy = purple_contact_get_priority_buddy(((PurpleContact *)node));
    }
    else if (!((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)) {
      gtk_tree_path_free(sourcerow);
      return ;
    }
    else {
      buddy = ((PurpleBuddy *)node);
    }
    gc = purple_account_get_connection((buddy -> account));
    if (gc == ((PurpleConnection *)((void *)0))) {
      gtk_tree_path_free(sourcerow);
      return ;
    }
    protocol = ( *( *((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info)).list_icon)((buddy -> account),buddy);
    str = g_string_new(0);
    g_string_printf(str,"MIME-Version: 1.0\r\nContent-Type: application/x-im-contact\r\nX-IM-Protocol: %s\r\nX-IM-Username: %s\r\n",protocol,(buddy -> name));
    if ((buddy -> alias) != ((char *)((void *)0))) {
      g_string_append_printf(str,"X-IM-Alias: %s\r\n",(buddy -> alias));
    }
    g_string_append(str,"\r\n");
    gtk_selection_data_set(data,gdk_atom_intern("application/x-im-contact",0),8,((const guchar *)(str -> str)),(strlen((str -> str)) + 1));
/* bits */
    g_string_free(str,(!0));
    gtk_tree_path_free(sourcerow);
  }
}

static void pidgin_blist_drag_data_rcv_cb(GtkWidget *widget,GdkDragContext *dc,guint x,guint y,GtkSelectionData *sd,guint info,guint t)
{
  if ((gtkblist -> drag_timeout) != 0U) {
    g_source_remove((gtkblist -> drag_timeout));
    gtkblist -> drag_timeout = 0;
  }
  if (((sd -> target) == gdk_atom_intern("PURPLE_BLIST_NODE",0)) && ((sd -> data) != 0)) {
    PurpleBlistNode *n = (PurpleBlistNode *)((void *)0);
    GtkTreePath *path = (GtkTreePath *)((void *)0);
    GtkTreeViewDropPosition position;
    memcpy((&n),(sd -> data),(sizeof(n)));
    if (gtk_tree_view_get_dest_row_at_pos(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_tree_view_get_type()))),x,y,&path,&position) != 0) {
/* if we're here, I think it means the drop is ok */
      GtkTreeIter iter;
      PurpleBlistNode *node;
      struct _pidgin_blist_node *gtknode;
      gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,path);
      gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,NODE_COLUMN,&node,-1);
      gtknode = (node -> ui_data);
      if ((purple_blist_node_get_type(n)) == PURPLE_BLIST_CONTACT_NODE) {
        PurpleContact *c = (PurpleContact *)n;
        if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) && ((gtknode -> contact_expanded) != 0)) {
          purple_blist_merge_contact(c,node);
        }
        else if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE)) {
          switch(position){
            case GTK_TREE_VIEW_DROP_AFTER:
{
            }
            case GTK_TREE_VIEW_DROP_INTO_OR_AFTER:
{
              purple_blist_add_contact(c,((PurpleGroup *)(node -> parent)),node);
              break; 
            }
            case GTK_TREE_VIEW_DROP_BEFORE:
{
            }
            case GTK_TREE_VIEW_DROP_INTO_OR_BEFORE:
{
              purple_blist_add_contact(c,((PurpleGroup *)(node -> parent)),(node -> prev));
              break; 
            }
          }
        }
        else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
          purple_blist_add_contact(c,((PurpleGroup *)node),0);
        }
        else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
          purple_blist_merge_contact(c,node);
        }
      }
      else if ((purple_blist_node_get_type(n)) == PURPLE_BLIST_BUDDY_NODE) {
        PurpleBuddy *b = (PurpleBuddy *)n;
        if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
          switch(position){
            case GTK_TREE_VIEW_DROP_AFTER:
{
            }
            case GTK_TREE_VIEW_DROP_INTO_OR_AFTER:
{
              purple_blist_add_buddy(b,((PurpleContact *)(node -> parent)),((PurpleGroup *)( *(node -> parent)).parent),node);
              break; 
            }
            case GTK_TREE_VIEW_DROP_BEFORE:
{
            }
            case GTK_TREE_VIEW_DROP_INTO_OR_BEFORE:
{
              purple_blist_add_buddy(b,((PurpleContact *)(node -> parent)),((PurpleGroup *)( *(node -> parent)).parent),(node -> prev));
              break; 
            }
          }
        }
        else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
          purple_blist_add_buddy(b,0,((PurpleGroup *)(node -> parent)),0);
        }
        else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
          purple_blist_add_buddy(b,0,((PurpleGroup *)node),0);
        }
        else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
          if ((gtknode -> contact_expanded) != 0) {
            switch(position){
              case GTK_TREE_VIEW_DROP_AFTER:
{
              }
              case GTK_TREE_VIEW_DROP_INTO_OR_BEFORE:
{
              }
              case GTK_TREE_VIEW_DROP_INTO_OR_AFTER:
{
                purple_blist_add_buddy(b,((PurpleContact *)node),((PurpleGroup *)(node -> parent)),0);
                break; 
              }
              case GTK_TREE_VIEW_DROP_BEFORE:
{
                purple_blist_add_buddy(b,0,((PurpleGroup *)(node -> parent)),(node -> prev));
                break; 
              }
            }
          }
          else {
            switch(position){
              case GTK_TREE_VIEW_DROP_AFTER:
{
              }
              case GTK_TREE_VIEW_DROP_INTO_OR_AFTER:
{
                purple_blist_add_buddy(b,0,((PurpleGroup *)(node -> parent)),0);
                break; 
              }
              case GTK_TREE_VIEW_DROP_BEFORE:
{
              }
              case GTK_TREE_VIEW_DROP_INTO_OR_BEFORE:
{
                purple_blist_add_buddy(b,0,((PurpleGroup *)(node -> parent)),(node -> prev));
                break; 
              }
            }
          }
        }
      }
      else if ((purple_blist_node_get_type(n)) == PURPLE_BLIST_CHAT_NODE) {
        PurpleChat *chat = (PurpleChat *)n;
        if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
          switch(position){
            case GTK_TREE_VIEW_DROP_BEFORE:
{
            }
            case GTK_TREE_VIEW_DROP_AFTER:
{
            }
            case GTK_TREE_VIEW_DROP_INTO_OR_BEFORE:
{
            }
            case GTK_TREE_VIEW_DROP_INTO_OR_AFTER:
{
              purple_blist_add_chat(chat,((PurpleGroup *)( *(node -> parent)).parent),(node -> parent));
              break; 
            }
          }
        }
        else if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE)) {
          switch(position){
            case GTK_TREE_VIEW_DROP_AFTER:
{
            }
            case GTK_TREE_VIEW_DROP_INTO_OR_AFTER:
{
              purple_blist_add_chat(chat,((PurpleGroup *)(node -> parent)),node);
              break; 
            }
            case GTK_TREE_VIEW_DROP_BEFORE:
{
            }
            case GTK_TREE_VIEW_DROP_INTO_OR_BEFORE:
{
              purple_blist_add_chat(chat,((PurpleGroup *)(node -> parent)),(node -> prev));
              break; 
            }
          }
        }
        else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
          purple_blist_add_chat(chat,((PurpleGroup *)node),0);
        }
      }
      else if ((purple_blist_node_get_type(n)) == PURPLE_BLIST_GROUP_NODE) {
        PurpleGroup *g = (PurpleGroup *)n;
        if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
          switch(position){
            case GTK_TREE_VIEW_DROP_AFTER:
{
            }
            case GTK_TREE_VIEW_DROP_INTO_OR_AFTER:
{
              purple_blist_add_group(g,node);
              break; 
            }
            case GTK_TREE_VIEW_DROP_BEFORE:
{
            }
            case GTK_TREE_VIEW_DROP_INTO_OR_BEFORE:
{
              purple_blist_add_group(g,(node -> prev));
              break; 
            }
          }
        }
        else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
          purple_blist_add_group(g,( *(node -> parent)).parent);
        }
        else if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE)) {
          purple_blist_add_group(g,(node -> parent));
        }
      }
      gtk_tree_path_free(path);
      gtk_drag_finish(dc,(!0),((dc -> action) == GDK_ACTION_MOVE),t);
    }
  }
  else if (((sd -> target) == gdk_atom_intern("application/x-im-contact",0)) && ((sd -> data) != 0)) {
    PurpleGroup *group = (PurpleGroup *)((void *)0);
    GtkTreePath *path = (GtkTreePath *)((void *)0);
    GtkTreeViewDropPosition position;
    PurpleAccount *account;
    char *protocol = (char *)((void *)0);
    char *username = (char *)((void *)0);
    char *alias = (char *)((void *)0);
    if (gtk_tree_view_get_dest_row_at_pos(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_tree_view_get_type()))),x,y,&path,&position) != 0) {
      GtkTreeIter iter;
      PurpleBlistNode *node;
      gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,path);
      gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,NODE_COLUMN,&node,-1);
      if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
        group = ((PurpleGroup *)( *(node -> parent)).parent);
      }
      else if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE)) {
        group = ((PurpleGroup *)(node -> parent));
      }
      else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
        group = ((PurpleGroup *)node);
      }
    }
    if (pidgin_parse_x_im_contact(((const char *)(sd -> data)),0,&account,&protocol,&username,&alias) != 0) {
      if (account == ((PurpleAccount *)((void *)0))) {
        purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","You are not currently signed on with an account that can add that buddy."))),0,0,0);
      }
      else {
        purple_blist_request_add_buddy(account,username,((group != 0)?(group -> name) : ((char *)((void *)0))),alias);
      }
    }
    g_free(username);
    g_free(protocol);
    g_free(alias);
    if (path != ((GtkTreePath *)((void *)0))) 
      gtk_tree_path_free(path);
    gtk_drag_finish(dc,(!0),((dc -> action) == GDK_ACTION_MOVE),t);
  }
  else if (((sd -> target) == gdk_atom_intern("text/x-vcard",0)) && ((sd -> data) != 0)) {
    gboolean result;
    PurpleGroup *group = (PurpleGroup *)((void *)0);
    GtkTreePath *path = (GtkTreePath *)((void *)0);
    GtkTreeViewDropPosition position;
    if (gtk_tree_view_get_dest_row_at_pos(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_tree_view_get_type()))),x,y,&path,&position) != 0) {
      GtkTreeIter iter;
      PurpleBlistNode *node;
      gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,path);
      gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,NODE_COLUMN,&node,-1);
      if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
        group = ((PurpleGroup *)( *(node -> parent)).parent);
      }
      else if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE)) {
        group = ((PurpleGroup *)(node -> parent));
      }
      else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
        group = ((PurpleGroup *)node);
      }
    }
    result = parse_vcard(((const gchar *)(sd -> data)),group);
    gtk_drag_finish(dc,result,((dc -> action) == GDK_ACTION_MOVE),t);
  }
  else if (((sd -> target) == gdk_atom_intern("text/uri-list",0)) && ((sd -> data) != 0)) {
    GtkTreePath *path = (GtkTreePath *)((void *)0);
    GtkTreeViewDropPosition position;
    if (gtk_tree_view_get_dest_row_at_pos(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_tree_view_get_type()))),x,y,&path,&position) != 0) {
      GtkTreeIter iter;
      PurpleBlistNode *node;
      gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,path);
      gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,NODE_COLUMN,&node,-1);
      if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE)) {
        PurpleBuddy *b = ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)?((PurpleBuddy *)node) : purple_contact_get_priority_buddy(((PurpleContact *)node));
        pidgin_dnd_file_manage(sd,(b -> account),(b -> name));
        gtk_drag_finish(dc,(!0),((dc -> action) == GDK_ACTION_MOVE),t);
      }
      else {
        gtk_drag_finish(dc,0,0,t);
      }
    }
  }
}
/* Altered from do_colorshift in gnome-panel */

static void do_alphashift(GdkPixbuf *pixbuf,int shift)
{
  gint i;
  gint j;
  gint width;
  gint height;
  gint padding;
  guchar *pixels;
  int val;
  if (!(gdk_pixbuf_get_has_alpha(pixbuf) != 0)) 
    return ;
  width = gdk_pixbuf_get_width(pixbuf);
  height = gdk_pixbuf_get_height(pixbuf);
  padding = (gdk_pixbuf_get_rowstride(pixbuf) - (width * 4));
  pixels = gdk_pixbuf_get_pixels(pixbuf);
  for (i = 0; i < height; i++) {
    for (j = 0; j < width; j++) {
      pixels++;
      pixels++;
      pixels++;
      val = (( *pixels) - shift);
       *(pixels++) = (((val > 255)?255 : (((val < 0)?0 : val))));
    }
    pixels += padding;
  }
}

static GdkPixbuf *pidgin_blist_get_buddy_icon(PurpleBlistNode *node,gboolean scaled,gboolean greyed)
{
  gsize len;
  PurpleBuddy *buddy = (PurpleBuddy *)((void *)0);
  PurpleGroup *group = (PurpleGroup *)((void *)0);
  const guchar *data = (const guchar *)((void *)0);
  GdkPixbuf *buf;
  GdkPixbuf *ret = (GdkPixbuf *)((void *)0);
  PurpleBuddyIcon *icon = (PurpleBuddyIcon *)((void *)0);
  PurpleAccount *account = (PurpleAccount *)((void *)0);
  PurpleContact *contact = (PurpleContact *)((void *)0);
  PurpleStoredImage *custom_img;
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  gint orig_width;
  gint orig_height;
  gint scale_width;
  gint scale_height;
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    buddy = purple_contact_get_priority_buddy(((PurpleContact *)node));
    contact = ((PurpleContact *)node);
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    buddy = ((PurpleBuddy *)node);
    contact = purple_buddy_get_contact(buddy);
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
    group = ((PurpleGroup *)node);
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
/* We don't need to do anything here. We just need to not fall
		 * into the else block and return. */
  }
  else {
    return 0;
  }
  if (buddy != 0) {
    account = purple_buddy_get_account(buddy);
  }
  if ((account != 0) && ((account -> gc) != 0)) {
    prpl_info = ((PurplePluginProtocolInfo *)( *( *( *(account -> gc)).prpl).info).extra_info);
  }
#if 0
#endif
/* If we have a contact then this is either a contact or a buddy and
	 * we want to fetch the custom icon for the contact. If we don't have
	 * a contact then this is a group or some other type of node and we
	 * want to use that directly. */
  if (contact != 0) {
    custom_img = purple_buddy_icons_node_find_custom_icon(((PurpleBlistNode *)contact));
  }
  else {
    custom_img = purple_buddy_icons_node_find_custom_icon(node);
  }
  if (custom_img != 0) {
    data = (purple_imgstore_get_data(custom_img));
    len = purple_imgstore_get_size(custom_img);
  }
  if (data == ((const guchar *)((void *)0))) {
    if (buddy != 0) {
/* Not sure I like this...*/
      if (!((icon = purple_buddy_icons_find((buddy -> account),(buddy -> name))) != 0)) 
        return 0;
      data = (purple_buddy_icon_get_data(icon,&len));
    }
    if (data == ((const guchar *)((void *)0))) 
      return 0;
  }
  buf = pidgin_pixbuf_from_data(data,len);
  purple_buddy_icon_unref(icon);
  if (!(buf != 0)) {
    purple_debug_warning("gtkblist","Couldn\'t load buddy icon on account %s (%s)  buddyname=%s  custom_img_data=%p\n",((account != 0)?purple_account_get_username(account) : "(no account)"),((account != 0)?purple_account_get_protocol_id(account) : "(no account)"),((buddy != 0)?purple_buddy_get_name(buddy) : "(no buddy)"),((custom_img != 0)?purple_imgstore_get_data(custom_img) : ((const void *)((void *)0))));
    purple_imgstore_unref(custom_img);
    return 0;
  }
  purple_imgstore_unref(custom_img);
  if (greyed != 0) {
    gboolean offline = 0;
    gboolean idle = 0;
    if (buddy != 0) {
      PurplePresence *presence = purple_buddy_get_presence(buddy);
      if (!(((buddy != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0))) 
        offline = (!0);
      if (purple_presence_is_idle(presence) != 0) 
        idle = (!0);
    }
    else if (group != 0) {
      if (purple_blist_get_group_online_count(group) == 0) 
        offline = (!0);
    }
    if (offline != 0) 
      gdk_pixbuf_saturate_and_pixelate(buf,buf,0.0,0);
    if (idle != 0) 
      gdk_pixbuf_saturate_and_pixelate(buf,buf,0.25,0);
  }
/* I'd use the pidgin_buddy_icon_get_scale_size() thing, but it won't
	 * tell me the original size, which I need for scaling purposes. */
  scale_width = (orig_width = gdk_pixbuf_get_width(buf));
  scale_height = (orig_height = gdk_pixbuf_get_height(buf));
  if ((prpl_info != 0) && ((prpl_info -> icon_spec.scale_rules & PURPLE_ICON_SCALE_DISPLAY) != 0U)) 
    purple_buddy_icon_get_scale_size(&prpl_info -> icon_spec,&scale_width,&scale_height);
  if (((scaled != 0) || (scale_height > 200)) || (scale_width > 200)) {
    GdkPixbuf *tmpbuf;
    float scale_size = ((scaled != 0)?32.0 : 200.0);
    if (scale_height > scale_width) {
      scale_width = ((scale_size * ((double )scale_width)) / ((double )scale_height));
      scale_height = scale_size;
    }
    else {
      scale_height = ((scale_size * ((double )scale_height)) / ((double )scale_width));
      scale_width = scale_size;
    }
/* Scale & round before making square, so rectangular (but
		 * non-square) images get rounded corners too. */
    tmpbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB,(!0),8,scale_width,scale_height);
    gdk_pixbuf_fill(tmpbuf,0);
    gdk_pixbuf_scale(buf,tmpbuf,0,0,scale_width,scale_height,0,0,(((double )scale_width) / ((double )orig_width)),(((double )scale_height) / ((double )orig_height)),GDK_INTERP_BILINEAR);
    if (pidgin_gdk_pixbuf_is_opaque(tmpbuf) != 0) 
      pidgin_gdk_pixbuf_make_round(tmpbuf);
    ret = gdk_pixbuf_new(GDK_COLORSPACE_RGB,(!0),8,scale_size,scale_size);
    gdk_pixbuf_fill(ret,0);
    gdk_pixbuf_copy_area(tmpbuf,0,0,scale_width,scale_height,ret,((scale_size - scale_width) / 2),((scale_size - scale_height) / 2));
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tmpbuf),((GType )(20 << 2))))));
  }
  else {
    ret = gdk_pixbuf_scale_simple(buf,scale_width,scale_height,GDK_INTERP_BILINEAR);
  }
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buf),((GType )(20 << 2))))));
  return ret;
}
/* # - Status Icon
 * P - Protocol Icon
 * A - Buddy Icon
 * [ - SMALL_SPACE
 * = - LARGE_SPACE
 *                   +--- STATUS_SIZE                +--- td->avatar_width
 *                   |         +-- td->name_width    |
 *                +----+   +-------+            +---------+
 *                |    |   |       |            |         |
 *                +-------------------------------------------+
 *                |       [          =        [               |--- TOOLTIP_BORDER
 *name_height --+-| ######[BuddyName = PP     [   AAAAAAAAAAA |--+
 *              | | ######[          = PP     [   AAAAAAAAAAA |  |
 * STATUS SIZE -| | ######[[[[[[[[[[[[[[[[[[[[[   AAAAAAAAAAA |  |
 *           +--+-| ######[Account: So-and-so [   AAAAAAAAAAA |  |-- td->avatar_height
 *           |    |       [Idle: 4h 15m       [   AAAAAAAAAAA |  |
 *  height --+    |       [Foo: Bar, Baz      [   AAAAAAAAAAA |  |
 *           |    |       [Status: Awesome    [   AAAAAAAAAAA |--+
 *           +----|       [Stop: Hammer Time  [               |
 *                |       [                   [               |--- TOOLTIP_BORDER
 *                +-------------------------------------------+
 *                 |       |                |                |
 *                 |       +----------------+                |
 *                 |               |                         |
 *                 |               +-- td->width             |
 *                 |                                         |
 *                 +---- TOOLTIP_BORDER                      +---- TOOLTIP_BORDER
 *
 *
 */
#define STATUS_SIZE 16
#define TOOLTIP_BORDER 12
#define SMALL_SPACE 6
#define LARGE_SPACE 12
#define PRPL_SIZE 16

struct tooltip_data 
{
  PangoLayout *layout;
  PangoLayout *name_layout;
  GdkPixbuf *prpl_icon;
  GdkPixbuf *status_icon;
  GdkPixbuf *avatar;
  gboolean avatar_is_prpl_icon;
  int avatar_width;
  int avatar_height;
  int name_height;
  int name_width;
  int width;
  int height;
  int padding;
}
;

static PangoLayout *create_pango_layout(const char *markup,int *width,int *height)
{
  PangoLayout *layout;
  int w;
  int h;
  layout = gtk_widget_create_pango_layout((gtkblist -> tipwindow),0);
  pango_layout_set_markup(layout,markup,-1);
  pango_layout_set_wrap(layout,PANGO_WRAP_WORD);
  pango_layout_set_width(layout,300000);
  pango_layout_get_size(layout,&w,&h);
  if (width != 0) 
     *width = ((((int )w) + 512) >> 10);
  if (height != 0) 
     *height = ((((int )h) + 512) >> 10);
  return layout;
}

static struct tooltip_data *create_tip_for_account(PurpleAccount *account)
{
  struct tooltip_data *td = (struct tooltip_data *)(g_malloc0_n(1,(sizeof(struct tooltip_data ))));
  td -> status_icon = pidgin_create_prpl_icon(account,PIDGIN_PRPL_ICON_SMALL);
/* Yes, status_icon, not prpl_icon */
  if (purple_account_is_disconnected(account) != 0) 
    gdk_pixbuf_saturate_and_pixelate((td -> status_icon),(td -> status_icon),0.0,0);
  td -> layout = create_pango_layout(purple_account_get_username(account),&td -> width,&td -> height);
  td -> padding = 6;
  return td;
}

static struct tooltip_data *create_tip_for_node(PurpleBlistNode *node,gboolean full)
{
  struct tooltip_data *td = (struct tooltip_data *)(g_malloc0_n(1,(sizeof(struct tooltip_data ))));
  PurpleAccount *account = (PurpleAccount *)((void *)0);
  char *tmp = (char *)((void *)0);
  char *node_name = (char *)((void *)0);
  char *tooltip_text = (char *)((void *)0);
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    account = ( *((PurpleBuddy *)node)).account;
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
    account = ( *((PurpleChat *)node)).account;
  }
  td -> padding = 12;
  td -> status_icon = pidgin_blist_get_status_icon(node,PIDGIN_STATUS_ICON_LARGE);
  td -> avatar = pidgin_blist_get_buddy_icon(node,!(full != 0),0);
  if (account != ((PurpleAccount *)((void *)0))) {
    td -> prpl_icon = pidgin_create_prpl_icon(account,PIDGIN_PRPL_ICON_SMALL);
  }
  tooltip_text = pidgin_get_tooltip_text(node,full);
  if ((tooltip_text != 0) && (( *tooltip_text) != 0)) {
    td -> layout = create_pango_layout(tooltip_text,&td -> width,&td -> height);
  }
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    tmp = g_markup_escape_text(purple_buddy_get_name(((PurpleBuddy *)node)),(-1));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
    tmp = g_markup_escape_text(purple_chat_get_name(((PurpleChat *)node)),(-1));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
    tmp = g_markup_escape_text(purple_group_get_name(((PurpleGroup *)node)),(-1));
  }
  else {
/* I don't believe this can happen currently, I think
		 * everything that calls this function checks for one of the
		 * above node types first. */
    tmp = g_strdup(((const char *)(dgettext("pidgin","Unknown node type"))));
  }
  node_name = g_strdup_printf("<span size=\'x-large\' weight=\'bold\'>%s</span>",((tmp != 0)?tmp : ""));
  g_free(tmp);
  td -> name_layout = create_pango_layout(node_name,&td -> name_width,&td -> name_height);
  td -> name_width += 6 + 16;
  td -> name_height = (((td -> name_height) > 16 + 6)?(td -> name_height) : 16 + 6);
#if 0  /* PRPL Icon as avatar */
#endif
  if ((td -> avatar) != 0) {
    td -> avatar_width = gdk_pixbuf_get_width((td -> avatar));
    td -> avatar_height = gdk_pixbuf_get_height((td -> avatar));
  }
  g_free(node_name);
  g_free(tooltip_text);
  return td;
}

static gboolean pidgin_blist_paint_tip(GtkWidget *widget,gpointer null)
{
  GtkStyle *style;
  int current_height;
  int max_width;
  int max_text_width;
  int max_avatar_width;
  GList *l;
  int prpl_col = 0;
  GtkTextDirection dir = gtk_widget_get_direction(widget);
  int status_size = 0;
  if ((gtkblist -> tooltipdata) == ((GList *)((void *)0))) 
    return 0;
  style = ( *(gtkblist -> tipwindow)).style;
  max_text_width = 0;
  max_avatar_width = 0;
  for (l = (gtkblist -> tooltipdata); l != 0; l = (l -> next)) {
    struct tooltip_data *td = (l -> data);
    max_text_width = ((max_text_width > ((((td -> width) > (td -> name_width))?(td -> width) : (td -> name_width))))?max_text_width : ((((td -> width) > (td -> name_width))?(td -> width) : (td -> name_width))));
    max_avatar_width = ((max_avatar_width > (td -> avatar_width))?max_avatar_width : (td -> avatar_width));
    if ((td -> status_icon) != 0) 
      status_size = 16;
  }
  max_width = ((((((12 + status_size) + 6) + max_text_width) + 6) + max_avatar_width) + 12);
  if (dir == GTK_TEXT_DIR_RTL) 
    prpl_col = ((12 + max_avatar_width) + 6);
  else 
    prpl_col = ((((12 + status_size) + 6) + max_text_width) - 16);
  current_height = 12;
  for (l = (gtkblist -> tooltipdata); l != 0; l = (l -> next)) {
    struct tooltip_data *td = (l -> data);
    if (((td -> avatar) != 0) && (pidgin_gdk_pixbuf_is_opaque((td -> avatar)) != 0)) {
      if (dir == GTK_TEXT_DIR_RTL) 
        gtk_paint_flat_box(style,( *(gtkblist -> tipwindow)).window,GTK_STATE_NORMAL,GTK_SHADOW_OUT,0,(gtkblist -> tipwindow),"tooltip",(12 - 1),(current_height - 1),((td -> avatar_width) + 2),((td -> avatar_height) + 2));
      else 
        gtk_paint_flat_box(style,( *(gtkblist -> tipwindow)).window,GTK_STATE_NORMAL,GTK_SHADOW_OUT,0,(gtkblist -> tipwindow),"tooltip",((max_width - ((td -> avatar_width) + 12)) - 1),(current_height - 1),((td -> avatar_width) + 2),((td -> avatar_height) + 2));
    }
    if ((td -> status_icon) != 0) {
      if (dir == GTK_TEXT_DIR_RTL) 
        gdk_draw_pixbuf(((GdkDrawable *)(g_type_check_instance_cast(((GTypeInstance *)( *(gtkblist -> tipwindow)).window),gdk_drawable_get_type()))),0,(td -> status_icon),0,0,((max_width - 12) - status_size),current_height,(-1),(-1),GDK_RGB_DITHER_NONE,0,0);
      else 
        gdk_draw_pixbuf(((GdkDrawable *)(g_type_check_instance_cast(((GTypeInstance *)( *(gtkblist -> tipwindow)).window),gdk_drawable_get_type()))),0,(td -> status_icon),0,0,12,current_height,(-1),(-1),GDK_RGB_DITHER_NONE,0,0);
    }
    if ((td -> avatar) != 0) {
      if (dir == GTK_TEXT_DIR_RTL) 
        gdk_draw_pixbuf(((GdkDrawable *)(g_type_check_instance_cast(((GTypeInstance *)( *(gtkblist -> tipwindow)).window),gdk_drawable_get_type()))),0,(td -> avatar),0,0,12,current_height,(-1),(-1),GDK_RGB_DITHER_NONE,0,0);
      else 
        gdk_draw_pixbuf(((GdkDrawable *)(g_type_check_instance_cast(((GTypeInstance *)( *(gtkblist -> tipwindow)).window),gdk_drawable_get_type()))),0,(td -> avatar),0,0,(max_width - ((td -> avatar_width) + 12)),current_height,(-1),(-1),GDK_RGB_DITHER_NONE,0,0);
    }
    if (!((td -> avatar_is_prpl_icon) != 0) && ((td -> prpl_icon) != 0)) 
      gdk_draw_pixbuf(((GdkDrawable *)(g_type_check_instance_cast(((GTypeInstance *)( *(gtkblist -> tipwindow)).window),gdk_drawable_get_type()))),0,(td -> prpl_icon),0,0,prpl_col,(current_height + (((td -> name_height) / 2) - 16 / 2)),(-1),(-1),GDK_RGB_DITHER_NONE,0,0);
    if ((td -> name_layout) != 0) {
      if (dir == GTK_TEXT_DIR_RTL) {
        gtk_paint_layout(style,( *(gtkblist -> tipwindow)).window,GTK_STATE_NORMAL,0,0,(gtkblist -> tipwindow),"tooltip",((max_width - ((12 + status_size) + 6)) - (((int )300000) + 512 >> 10)),current_height,(td -> name_layout));
      }
      else {
        gtk_paint_layout(style,( *(gtkblist -> tipwindow)).window,GTK_STATE_NORMAL,0,0,(gtkblist -> tipwindow),"tooltip",((12 + status_size) + 6),current_height,(td -> name_layout));
      }
    }
    if ((td -> layout) != 0) {
      if (dir != GTK_TEXT_DIR_RTL) {
        gtk_paint_layout(style,( *(gtkblist -> tipwindow)).window,GTK_STATE_NORMAL,0,0,(gtkblist -> tipwindow),"tooltip",((12 + status_size) + 6),(current_height + (td -> name_height)),(td -> layout));
      }
      else {
        gtk_paint_layout(style,( *(gtkblist -> tipwindow)).window,GTK_STATE_NORMAL,0,0,(gtkblist -> tipwindow),"tooltip",((max_width - ((12 + status_size) + 6)) - (((int )300000) + 512 >> 10)),(current_height + (td -> name_height)),(td -> layout));
      }
    }
    current_height += ((((((td -> name_height) + (td -> height)) > (td -> avatar_height))?((td -> name_height) + (td -> height)) : (td -> avatar_height))) + (td -> padding));
  }
  return 0;
}

static void pidgin_blist_destroy_tooltip_data()
{
  while((gtkblist -> tooltipdata) != 0){
    struct tooltip_data *td = ( *(gtkblist -> tooltipdata)).data;
    if ((td -> avatar) != 0) 
      g_object_unref((td -> avatar));
    if ((td -> status_icon) != 0) 
      g_object_unref((td -> status_icon));
    if ((td -> prpl_icon) != 0) 
      g_object_unref((td -> prpl_icon));
    if ((td -> layout) != 0) 
      g_object_unref((td -> layout));
    if ((td -> name_layout) != 0) 
      g_object_unref((td -> name_layout));
    g_free(td);
    gtkblist -> tooltipdata = g_list_delete_link((gtkblist -> tooltipdata),(gtkblist -> tooltipdata));
  }
}

void pidgin_blist_tooltip_destroy()
{
  pidgin_blist_destroy_tooltip_data();
  pidgin_tooltip_destroy();
}

static void pidgin_blist_align_tooltip(struct tooltip_data *td,GtkWidget *widget)
{
  GtkTextDirection dir = gtk_widget_get_direction(widget);
  if (dir == GTK_TEXT_DIR_RTL) {
    char *layout_name = purple_markup_strip_html(pango_layout_get_text((td -> name_layout)));
    PangoDirection dir = pango_find_base_dir(layout_name,(-1));
    if ((dir == PANGO_DIRECTION_RTL) || (dir == PANGO_DIRECTION_NEUTRAL)) 
      pango_layout_set_alignment((td -> name_layout),PANGO_ALIGN_RIGHT);
    g_free(layout_name);
    pango_layout_set_alignment((td -> layout),PANGO_ALIGN_RIGHT);
  }
}

static gboolean pidgin_blist_create_tooltip_for_node(GtkWidget *widget,gpointer data,int *w,int *h)
{
  PurpleBlistNode *node = data;
  int width;
  int height;
  GList *list;
  int max_text_width = 0;
  int max_avatar_width = 0;
  int status_size = 0;
  if ((gtkblist -> tooltipdata) != 0) {
    gtkblist -> tipwindow = ((GtkWidget *)((void *)0));
    pidgin_blist_destroy_tooltip_data();
  }
  gtkblist -> tipwindow = widget;
  if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)) {
    struct tooltip_data *td = create_tip_for_node(node,(!0));
    pidgin_blist_align_tooltip(td,(gtkblist -> tipwindow));
    gtkblist -> tooltipdata = g_list_append((gtkblist -> tooltipdata),td);
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
    PurpleGroup *group = (PurpleGroup *)node;
    GSList *accounts;
    struct tooltip_data *td = create_tip_for_node(node,(!0));
    pidgin_blist_align_tooltip(td,(gtkblist -> tipwindow));
    gtkblist -> tooltipdata = g_list_append((gtkblist -> tooltipdata),td);
/* Accounts with buddies in group */
    accounts = purple_group_get_accounts(group);
    for (; accounts != ((GSList *)((void *)0)); accounts = g_slist_delete_link(accounts,accounts)) {
      PurpleAccount *account = (accounts -> data);
      td = create_tip_for_account(account);
      gtkblist -> tooltipdata = g_list_append((gtkblist -> tooltipdata),td);
    }
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    PurpleBlistNode *child;
    PurpleBuddy *b = purple_contact_get_priority_buddy(((PurpleContact *)node));
    for (child = (node -> child); child != 0; child = (child -> next)) {
      if (((purple_blist_node_get_type(child)) == PURPLE_BLIST_BUDDY_NODE) && (buddy_is_displayable(((PurpleBuddy *)child)) != 0)) {
        struct tooltip_data *td = create_tip_for_node(child,(b == ((PurpleBuddy *)child)));
        pidgin_blist_align_tooltip(td,(gtkblist -> tipwindow));
        if (b == ((PurpleBuddy *)child)) {
          gtkblist -> tooltipdata = g_list_prepend((gtkblist -> tooltipdata),td);
        }
        else {
          gtkblist -> tooltipdata = g_list_append((gtkblist -> tooltipdata),td);
        }
      }
    }
  }
  else {
    return 0;
  }
  height = (width = 0);
  for (list = (gtkblist -> tooltipdata); list != 0; list = (list -> next)) {
    struct tooltip_data *td = (list -> data);
    max_text_width = ((max_text_width > ((((td -> width) > (td -> name_width))?(td -> width) : (td -> name_width))))?max_text_width : ((((td -> width) > (td -> name_width))?(td -> width) : (td -> name_width))));
    max_avatar_width = ((max_avatar_width > (td -> avatar_width))?max_avatar_width : (td -> avatar_width));
    height += (((((((16 > (td -> avatar_height))?16 : (td -> avatar_height))) > ((td -> height) + (td -> name_height)))?(((16 > (td -> avatar_height))?16 : (td -> avatar_height))) : ((td -> height) + (td -> name_height)))) + (td -> padding));
    if ((td -> status_icon) != 0) 
      status_size = ((status_size > 16)?status_size : 16);
  }
  height += 12;
  width = ((((((12 + status_size) + 6) + max_text_width) + 6) + max_avatar_width) + 12);
  if (w != 0) 
     *w = width;
  if (h != 0) 
     *h = height;
  return (!0);
}

static gboolean pidgin_blist_expand_timeout(GtkWidget *tv)
{
  GtkTreePath *path;
  GtkTreeIter iter;
  PurpleBlistNode *node;
  struct _pidgin_blist_node *gtknode;
  if (!(gtk_tree_view_get_path_at_pos(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gtk_tree_view_get_type()))),gtkblist -> tip_rect.x,(gtkblist -> tip_rect.y + (gtkblist -> tip_rect.height / 2)),&path,0,0,0) != 0)) 
    return 0;
  gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,path);
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,NODE_COLUMN,&node,-1);
  if (!((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE)) {
    gtk_tree_path_free(path);
    return 0;
  }
  gtknode = (node -> ui_data);
  if (!((gtknode -> contact_expanded) != 0)) {
    GtkTreeIter i;
    pidgin_blist_expand_contact_cb(0,node);
    gtk_tree_view_get_cell_area(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gtk_tree_view_get_type()))),path,0,&gtkblist -> contact_rect);
    gdk_drawable_get_size(((GdkDrawable *)(g_type_check_instance_cast(((GTypeInstance *)(tv -> window)),gdk_drawable_get_type()))),&gtkblist -> contact_rect.width,0);
    gtkblist -> mouseover_contact = node;
    gtk_tree_path_down(path);
    while(gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&i,path) != 0){
      GdkRectangle rect;
      gtk_tree_view_get_cell_area(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gtk_tree_view_get_type()))),path,0,&rect);
      gtkblist -> contact_rect.height += rect.height;
      gtk_tree_path_next(path);
    }
  }
  gtk_tree_path_free(path);
  return 0;
}

static gboolean buddy_is_displayable(PurpleBuddy *buddy)
{
  struct _pidgin_blist_node *gtknode;
  if (!(buddy != 0)) 
    return 0;
  gtknode = ( *((PurpleBlistNode *)buddy)).ui_data;
  return (purple_account_is_connected((buddy -> account)) != 0) && ((((purple_presence_is_online((buddy -> presence)) != 0) || ((gtknode != 0) && ((gtknode -> recent_signonoff) != 0))) || (purple_prefs_get_bool("/pidgin/blist/show_offline_buddies") != 0)) || (purple_blist_node_get_bool(((PurpleBlistNode *)buddy),"show_offline") != 0));
}

void pidgin_blist_draw_tooltip(PurpleBlistNode *node,GtkWidget *widget)
{
  pidgin_tooltip_show(widget,node,pidgin_blist_create_tooltip_for_node,pidgin_blist_paint_tip);
}

static gboolean pidgin_blist_drag_motion_cb(GtkWidget *tv,GdkDragContext *drag_context,gint x,gint y,guint time,gpointer user_data)
{
  GtkTreePath *path;
  int delay;
  GdkRectangle rect;
/*
	 * When dragging a buddy into a contact, this is the delay before
	 * the contact auto-expands.
	 */
  delay = 900;
  if ((gtkblist -> drag_timeout) != 0U) {
    if ((y > gtkblist -> tip_rect.y) && ((y - gtkblist -> tip_rect.height) < gtkblist -> tip_rect.y)) 
      return 0;
/* We've left the cell.  Remove the timeout and create a new one below */
    g_source_remove((gtkblist -> drag_timeout));
  }
  gtk_tree_view_get_path_at_pos(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gtk_tree_view_get_type()))),x,y,&path,0,0,0);
  gtk_tree_view_get_cell_area(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gtk_tree_view_get_type()))),path,0,&rect);
  if (path != 0) 
    gtk_tree_path_free(path);
/* Only autoexpand when in the middle of the cell to avoid annoying un-intended expands */
  if ((y < (rect.y + (rect.height / 3))) || (y > (rect.y + (2 * (rect.height / 3))))) 
    return 0;
  rect.height = (rect.height / 3);
  rect.y += rect.height;
  gtkblist -> tip_rect = rect;
  gtkblist -> drag_timeout = g_timeout_add(delay,((GSourceFunc )pidgin_blist_expand_timeout),tv);
  if ((gtkblist -> mouseover_contact) != 0) {
    if ((y < gtkblist -> contact_rect.y) || ((y - gtkblist -> contact_rect.height) > gtkblist -> contact_rect.y)) {
      pidgin_blist_collapse_contact_cb(0,(gtkblist -> mouseover_contact));
      gtkblist -> mouseover_contact = ((PurpleBlistNode *)((void *)0));
    }
  }
  return 0;
}

static gboolean pidgin_blist_create_tooltip(GtkWidget *widget,GtkTreePath *path,gpointer null,int *w,int *h)
{
  GtkTreeIter iter;
  PurpleBlistNode *node;
  gboolean editable = 0;
/* If we're editing a cell (e.g. alias editing), don't show the tooltip */
  g_object_get(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> text_rend)),((GType )(20 << 2))))),"editable",&editable,((void *)((void *)0)));
  if (editable != 0) 
    return 0;
  if ((gtkblist -> tooltipdata) != 0) {
    gtkblist -> tipwindow = ((GtkWidget *)((void *)0));
    pidgin_blist_destroy_tooltip_data();
  }
  gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,path);
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,NODE_COLUMN,&node,-1);
  return pidgin_blist_create_tooltip_for_node(widget,node,w,h);
}

static gboolean pidgin_blist_motion_cb(GtkWidget *tv,GdkEventMotion *event,gpointer null)
{
  if ((gtkblist -> mouseover_contact) != 0) {
    if (((event -> y) < gtkblist -> contact_rect.y) || (((event -> y) - gtkblist -> contact_rect.height) > gtkblist -> contact_rect.y)) {
      pidgin_blist_collapse_contact_cb(0,(gtkblist -> mouseover_contact));
      gtkblist -> mouseover_contact = ((PurpleBlistNode *)((void *)0));
    }
  }
  return 0;
}

static gboolean pidgin_blist_leave_cb(GtkWidget *w,GdkEventCrossing *e,gpointer n)
{
  if ((gtkblist -> timeout) != 0U) {
    g_source_remove((gtkblist -> timeout));
    gtkblist -> timeout = 0;
  }
  if ((gtkblist -> drag_timeout) != 0U) {
    g_source_remove((gtkblist -> drag_timeout));
    gtkblist -> drag_timeout = 0;
  }
  if (((gtkblist -> mouseover_contact) != 0) && !(((((e -> x) > gtkblist -> contact_rect.x) && ((e -> x) < (gtkblist -> contact_rect.x + gtkblist -> contact_rect.width))) && ((e -> y) > gtkblist -> contact_rect.y)) && ((e -> y) < (gtkblist -> contact_rect.y + gtkblist -> contact_rect.height)))) {
    pidgin_blist_collapse_contact_cb(0,(gtkblist -> mouseover_contact));
    gtkblist -> mouseover_contact = ((PurpleBlistNode *)((void *)0));
  }
  return 0;
}

static void toggle_debug()
{
  purple_prefs_set_bool("/pidgin/debug/enabled",!(purple_prefs_get_bool("/pidgin/debug/enabled") != 0));
}

static char *get_mood_icon_path(const char *mood)
{
  char *path;
  if (!(strcmp(mood,"busy") != 0)) {
    path = g_build_filename("/usr/local/share","pixmaps","pidgin","status","16","busy.png",((void *)((void *)0)));
  }
  else if (!(strcmp(mood,"hiptop") != 0)) {
    path = g_build_filename("/usr/local/share","pixmaps","pidgin","emblems","16","hiptop.png",((void *)((void *)0)));
  }
  else {
    char *filename = g_strdup_printf("%s.png",mood);
    path = g_build_filename("/usr/local/share","pixmaps","pidgin","emotes","small",filename,((void *)((void *)0)));
    g_free(filename);
  }
  return path;
}

static void update_status_with_mood(PurpleAccount *account,const gchar *mood,const gchar *text)
{
  if ((mood != 0) && (( *mood) != 0)) {
    if (text != 0) {
      purple_account_set_status(account,"mood",(!0),"mood",mood,"moodtext",text,((void *)((void *)0)));
    }
    else {
      purple_account_set_status(account,"mood",(!0),"mood",mood,((void *)((void *)0)));
    }
  }
  else {
    purple_account_set_status(account,"mood",0,((void *)((void *)0)));
  }
}

static void edit_mood_cb(PurpleConnection *gc,PurpleRequestFields *fields)
{
  PurpleRequestField *mood_field;
  GList *l;
  mood_field = purple_request_fields_get_field(fields,"mood");
  l = purple_request_field_list_get_selected(mood_field);
  if (l != 0) {
    const char *mood = (purple_request_field_list_get_data(mood_field,(l -> data)));
    if (gc != 0) {
      const char *text;
      PurpleAccount *account = purple_connection_get_account(gc);
      if (((gc -> flags) & PURPLE_CONNECTION_SUPPORT_MOOD_MESSAGES) != 0U) {
        PurpleRequestField *text_field;
        text_field = purple_request_fields_get_field(fields,"text");
        text = purple_request_field_string_get_value(text_field);
      }
      else {
        text = ((const char *)((void *)0));
      }
      update_status_with_mood(account,mood,text);
    }
    else {
      GList *accounts = purple_accounts_get_all_active();
      for (; accounts != 0; accounts = g_list_delete_link(accounts,accounts)) {
        PurpleAccount *account = (PurpleAccount *)(accounts -> data);
        PurpleConnection *gc = purple_account_get_connection(account);
        if ((gc != 0) && (((gc -> flags) & PURPLE_CONNECTION_SUPPORT_MOODS) != 0U)) {
          update_status_with_mood(account,mood,0);
        }
      }
    }
  }
}

static void global_moods_for_each(gpointer key,gpointer value,gpointer user_data)
{
  GList **out_moods = (GList **)user_data;
  PurpleMood *mood = (PurpleMood *)value;
   *out_moods = g_list_append( *out_moods,mood);
}

static PurpleMood *get_global_moods()
{
  GHashTable *global_moods = g_hash_table_new_full(g_str_hash,g_str_equal,0,0);
  GHashTable *mood_counts = g_hash_table_new_full(g_str_hash,g_str_equal,0,0);
  GList *accounts = purple_accounts_get_all_active();
  PurpleMood *result = (PurpleMood *)((void *)0);
  GList *out_moods = (GList *)((void *)0);
  int i = 0;
  int num_accounts = 0;
  for (; accounts != 0; accounts = g_list_delete_link(accounts,accounts)) {
    PurpleAccount *account = (PurpleAccount *)(accounts -> data);
    if (purple_account_is_connected(account) != 0) {
      PurpleConnection *gc = purple_account_get_connection(account);
      if (((gc -> flags) & PURPLE_CONNECTION_SUPPORT_MOODS) != 0U) {
        PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info;
        PurpleMood *mood = (PurpleMood *)((void *)0);
/* PURPLE_CONNECTION_SUPPORT_MOODS would not be set if the prpl doesn't
				 * have get_moods, so using PURPLE_PROTOCOL_PLUGIN_HAS_FUNC isn't necessary
				 * here */
        for (mood = ( *(prpl_info -> get_moods))(account); (mood -> mood) != ((const char *)((void *)0)); mood++) {
          int mood_count = (gint )((glong )(g_hash_table_lookup(mood_counts,(mood -> mood))));
          if (!(g_hash_table_lookup(global_moods,(mood -> mood)) != 0)) {
            g_hash_table_insert(global_moods,((gpointer )(mood -> mood)),mood);
          }
          g_hash_table_insert(mood_counts,((gpointer )(mood -> mood)),((gpointer )((glong )(mood_count + 1))));
        }
        num_accounts++;
      }
    }
  }
  g_hash_table_foreach(global_moods,global_moods_for_each,(&out_moods));
  result = ((PurpleMood *)(g_malloc0_n((g_hash_table_size(global_moods) + 1),(sizeof(PurpleMood )))));
  while(out_moods != 0){
    PurpleMood *mood = (PurpleMood *)(out_moods -> data);
    int in_num_accounts = (gint )((glong )(g_hash_table_lookup(mood_counts,(mood -> mood))));
    if (in_num_accounts == num_accounts) {
/* mood is present in all accounts supporting moods */
      result[i].mood = (mood -> mood);
      result[i].description = (mood -> description);
      i++;
    }
    out_moods = g_list_delete_link(out_moods,out_moods);
  }
  g_hash_table_destroy(global_moods);
  g_hash_table_destroy(mood_counts);
  return result;
}
/* get current set mood for all mood-supporting accounts, or NULL if not set
 or not set to the same on all */

static const gchar *get_global_mood_status()
{
  GList *accounts = purple_accounts_get_all_active();
  const gchar *found_mood = (const gchar *)((void *)0);
{
    for (; accounts != 0; accounts = g_list_delete_link(accounts,accounts)) {
      PurpleAccount *account = (PurpleAccount *)(accounts -> data);
      if ((purple_account_is_connected(account) != 0) && ((( *purple_account_get_connection(account)).flags & PURPLE_CONNECTION_SUPPORT_MOODS) != 0U)) {
        PurplePresence *presence = purple_account_get_presence(account);
        PurpleStatus *status = purple_presence_get_status(presence,"mood");
        const gchar *curr_mood = purple_status_get_attr_string(status,"mood");
        if ((found_mood != ((const gchar *)((void *)0))) && !(purple_strequal(curr_mood,found_mood) != 0)) {
/* found a different mood */
          found_mood = ((const gchar *)((void *)0));
          break; 
        }
        else {
          found_mood = curr_mood;
        }
      }
    }
  }
  return found_mood;
}

static void set_mood_cb(GtkWidget *widget,PurpleAccount *account)
{
  const char *current_mood;
  PurpleRequestFields *fields;
  PurpleRequestFieldGroup *g;
  PurpleRequestField *f;
  PurpleConnection *gc = (PurpleConnection *)((void *)0);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurpleMood *mood;
  PurpleMood *global_moods = get_global_moods();
  if (account != 0) {
    PurplePresence *presence = purple_account_get_presence(account);
    PurpleStatus *status = purple_presence_get_status(presence,"mood");
    gc = purple_account_get_connection(account);
    do {
      if ((gc -> prpl) != ((PurplePlugin *)((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"gc->prpl != NULL");
        return ;
      };
    }while (0);
    prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info);
    current_mood = purple_status_get_attr_string(status,"mood");
  }
  else {
    current_mood = get_global_mood_status();
  }
  fields = purple_request_fields_new();
  g = purple_request_field_group_new(0);
  f = purple_request_field_list_new("mood",((const char *)(dgettext("pidgin","Please select your mood from the list"))));
  purple_request_field_list_add(f,((const char *)(dgettext("pidgin","None"))),"");
  if (current_mood == ((const char *)((void *)0))) 
    purple_request_field_list_add_selected(f,((const char *)(dgettext("pidgin","None"))));
/* TODO: rlaager wants this sorted. */
/* TODO: darkrain wants it sorted post-translation */
  if ((account != 0) && (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).get_moods))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).get_moods))) < (prpl_info -> struct_size))) && ((prpl_info -> get_moods) != ((PurpleMood *(*)(PurpleAccount *))((void *)0))))) 
    mood = ( *(prpl_info -> get_moods))(account);
  else 
    mood = global_moods;
  for (; (mood -> mood) != ((const char *)((void *)0)); mood++) {{
      char *path;
      if (((mood -> mood) == ((const char *)((void *)0))) || ((mood -> description) == ((const char *)((void *)0)))) 
        continue; 
      path = get_mood_icon_path((mood -> mood));
      purple_request_field_list_add_icon(f,((const char *)(dgettext("pidgin",(mood -> description)))),path,((gpointer )(mood -> mood)));
      g_free(path);
      if ((current_mood != 0) && !(strcmp(current_mood,(mood -> mood)) != 0)) 
        purple_request_field_list_add_selected(f,((const char *)(dgettext("pidgin",(mood -> description)))));
    }
  }
  purple_request_field_group_add_field(g,f);
  purple_request_fields_add_group(fields,g);
/* if the connection allows setting a mood message */
  if ((gc != 0) && (((gc -> flags) & PURPLE_CONNECTION_SUPPORT_MOOD_MESSAGES) != 0U)) {
    g = purple_request_field_group_new(0);
    f = purple_request_field_string_new("text",((const char *)(dgettext("pidgin","Message (optional)"))),0,0);
    purple_request_field_group_add_field(g,f);
    purple_request_fields_add_group(fields,g);
  }
  purple_request_fields(gc,((const char *)(dgettext("pidgin","Edit User Mood"))),((const char *)(dgettext("pidgin","Edit User Mood"))),0,fields,((const char *)(dgettext("pidgin","OK"))),((GCallback )edit_mood_cb),((const char *)(dgettext("pidgin","Cancel"))),0,((gc != 0)?purple_connection_get_account(gc) : ((struct _PurpleAccount *)((void *)0))),0,0,gc);
  g_free(global_moods);
}

static void set_mood_show()
{
  set_mood_cb(0,0);
}
/***************************************************
 *            Crap                                 *
 ***************************************************/
static GtkItemFactoryEntry blist_menu[] = {
/* NOTE: Do not set any accelerator to Control+O. It is mapped by
   gtk_blist_key_press_cb to "Get User Info" on the selected buddy. */
/* Buddies menu */
{("/_Buddies"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Branch>"), ((gconstpointer )((void *)0))}, {("/Buddies/New Instant _Message..."), ("<CTL>M"), (pidgin_dialogs_im), (0), ("<StockItem>"), ("pidgin-message-new")}, {("/Buddies/Join a _Chat..."), ("<CTL>C"), (pidgin_blist_joinchat_show), (0), ("<StockItem>"), ("pidgin-chat")}, {("/Buddies/Get User _Info..."), ("<CTL>I"), (pidgin_dialogs_info), (0), ("<StockItem>"), ("pidgin-info")}, {("/Buddies/View User _Log..."), ("<CTL>L"), (pidgin_dialogs_log), (0), ("<Item>"), ((gconstpointer )((void *)0))}, {("/Buddies/sep1"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Separator>"), ((gconstpointer )((void *)0))}, {("/Buddies/Sh_ow"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Branch>"), ((gconstpointer )((void *)0))}, {("/Buddies/Show/_Offline Buddies"), ((gchar *)((void *)0)), (pidgin_blist_edit_mode_cb), (1), ("<CheckItem>"), ((gconstpointer )((void *)0))}, {("/Buddies/Show/_Empty Groups"), ((gchar *)((void *)0)), (pidgin_blist_show_empty_groups_cb), (1), ("<CheckItem>"), ((gconstpointer )((void *)0))}, {("/Buddies/Show/Buddy _Details"), ((gchar *)((void *)0)), (pidgin_blist_buddy_details_cb), (1), ("<CheckItem>"), ((gconstpointer )((void *)0))}, {("/Buddies/Show/Idle _Times"), ((gchar *)((void *)0)), (pidgin_blist_show_idle_time_cb), (1), ("<CheckItem>"), ((gconstpointer )((void *)0))}, {("/Buddies/Show/_Protocol Icons"), ((gchar *)((void *)0)), (pidgin_blist_show_protocol_icons_cb), (1), ("<CheckItem>"), ((gconstpointer )((void *)0))}, {("/Buddies/_Sort Buddies"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Branch>"), ((gconstpointer )((void *)0))}, {("/Buddies/sep2"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Separator>"), ((gconstpointer )((void *)0))}, {("/Buddies/_Add Buddy..."), ("<CTL>B"), (pidgin_blist_add_buddy_cb), (0), ("<StockItem>"), ("gtk-add")}, {("/Buddies/Add C_hat..."), ((gchar *)((void *)0)), (pidgin_blist_add_chat_cb), (0), ("<StockItem>"), ("gtk-add")}, {("/Buddies/Add _Group..."), ((gchar *)((void *)0)), (purple_blist_request_add_group), (0), ("<StockItem>"), ("gtk-add")}, {("/Buddies/sep3"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Separator>"), ((gconstpointer )((void *)0))}, {("/Buddies/_Quit"), ("<CTL>Q"), (purple_core_quit), (0), ("<StockItem>"), ("gtk-quit")}, 
/* Accounts menu */
{("/_Accounts"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Branch>"), ((gconstpointer )((void *)0))}, {("/Accounts/Manage Accounts"), ("<CTL>A"), (pidgin_accounts_window_show), (0), ("<Item>"), ((gconstpointer )((void *)0))}, 
/* Tools */
{("/_Tools"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Branch>"), ((gconstpointer )((void *)0))}, {("/Tools/Buddy _Pounces"), ((gchar *)((void *)0)), (pidgin_pounces_manager_show), (1), ("<Item>"), ((gconstpointer )((void *)0))}, {("/Tools/_Certificates"), ((gchar *)((void *)0)), (pidgin_certmgr_show), (0), ("<Item>"), ((gconstpointer )((void *)0))}, {("/Tools/Custom Smile_ys"), ("<CTL>Y"), (pidgin_smiley_manager_show), (0), ("<StockItem>"), ("pidgin-smiley")}, {("/Tools/Plu_gins"), ("<CTL>U"), (pidgin_plugin_dialog_show), (2), ("<StockItem>"), ("pidgin-plugins")}, {("/Tools/Pr_eferences"), ("<CTL>P"), (pidgin_prefs_show), (0), ("<StockItem>"), ("gtk-preferences")}, {("/Tools/Pr_ivacy"), ((gchar *)((void *)0)), (pidgin_privacy_dialog_show), (0), ("<Item>"), ((gconstpointer )((void *)0))}, {("/Tools/Set _Mood"), ("<CTL>D"), (set_mood_show), (0), ("<Item>"), ((gconstpointer )((void *)0))}, {("/Tools/sep2"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Separator>"), ((gconstpointer )((void *)0))}, {("/Tools/_File Transfers"), ("<CTL>T"), (pidgin_xfer_dialog_show), (0), ("<StockItem>"), ("pidgin-transfer")}, {("/Tools/R_oom List"), ((gchar *)((void *)0)), (pidgin_roomlist_dialog_show), (0), ("<Item>"), ((gconstpointer )((void *)0))}, {("/Tools/System _Log"), ((gchar *)((void *)0)), (gtk_blist_show_systemlog_cb), (3), ("<Item>"), ((gconstpointer )((void *)0))}, {("/Tools/sep3"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Separator>"), ((gconstpointer )((void *)0))}, {("/Tools/Mute _Sounds"), ((gchar *)((void *)0)), (pidgin_blist_mute_sounds_cb), (0), ("<CheckItem>"), ((gconstpointer )((void *)0))}, 
/* Help */
{("/_Help"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Branch>"), ((gconstpointer )((void *)0))}, {("/Help/Online _Help"), ("F1"), (gtk_blist_show_onlinehelp_cb), (0), ("<StockItem>"), ("gtk-help")}, {("/Help/sep1"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Separator>"), ((gconstpointer )((void *)0))}, {("/Help/_Build Information"), ((gchar *)((void *)0)), (pidgin_dialogs_buildinfo), (0), ("<Item>"), ((gconstpointer )((void *)0))}, {("/Help/_Debug Window"), ((gchar *)((void *)0)), (toggle_debug), (0), ("<Item>"), ((gconstpointer )((void *)0))}, {("/Help/De_veloper Information"), ((gchar *)((void *)0)), (pidgin_dialogs_developers), (0), ("<Item>"), ((gconstpointer )((void *)0))}, {("/Help/_Plugin Information"), ((gchar *)((void *)0)), (pidgin_dialogs_plugins_info), (0), ("<Item>"), ((gconstpointer )((void *)0))}, {("/Help/_Translator Information"), ((gchar *)((void *)0)), (pidgin_dialogs_translators), (0), ("<Item>"), ((gconstpointer )((void *)0))}, {("/Help/sep2"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Separator>"), ((gconstpointer )((void *)0))}, {("/Help/_About"), ((gchar *)((void *)0)), (pidgin_dialogs_about), (4), ("<StockItem>"), ("gtk-about")}};
/*********************************************************
 * Private Utility functions                             *
 *********************************************************/

static char *pidgin_get_tooltip_text(PurpleBlistNode *node,gboolean full)
{
  GString *str = g_string_new("");
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  char *tmp;
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
    PurpleChat *chat;
    GList *connections;
    GList *cur;
    struct proto_chat_entry *pce;
    char *name;
    char *value;
    PurpleConversation *conv;
    PidginBlistNode *bnode = (node -> ui_data);
    chat = ((PurpleChat *)node);
    prpl = purple_find_prpl(purple_account_get_protocol_id((chat -> account)));
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
    connections = purple_connections_get_all();
    if ((connections != 0) && ((connections -> next) != 0)) {
      tmp = g_markup_escape_text(( *(chat -> account)).username,(-1));
      g_string_append_printf(str,((const char *)(dgettext("pidgin","<b>Account:</b> %s"))),tmp);
      g_free(tmp);
    }
    if ((bnode != 0) && (bnode -> conv.conv != 0)) {
      conv = bnode -> conv.conv;
    }
    else {
      char *chat_name;
      if ((prpl_info != 0) && ((prpl_info -> get_chat_name) != 0)) 
        chat_name = ( *(prpl_info -> get_chat_name))((chat -> components));
      else 
        chat_name = g_strdup(purple_chat_get_name(chat));
      conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,chat_name,(chat -> account));
      g_free(chat_name);
    }
    if ((conv != 0) && !(purple_conv_chat_has_left(purple_conversation_get_chat_data(conv)) != 0)) {
      g_string_append_printf(str,((const char *)(dgettext("pidgin","\n<b>Occupants:</b> %d"))),g_list_length(purple_conv_chat_get_users((purple_conversation_get_chat_data(conv)))));
      if ((prpl_info != 0) && (((prpl_info -> options) & OPT_PROTO_CHAT_TOPIC) != 0U)) {
        const char *chattopic = purple_conv_chat_get_topic((purple_conversation_get_chat_data(conv)));
        char *topic = (chattopic != 0)?g_markup_escape_text(chattopic,(-1)) : ((char *)((void *)0));
        g_string_append_printf(str,((const char *)(dgettext("pidgin","\n<b>Topic:</b> %s"))),((topic != 0)?topic : ((const char *)(dgettext("pidgin","(no topic set)")))));
        g_free(topic);
      }
    }
    if ((prpl_info != 0) && ((prpl_info -> chat_info) != ((GList *(*)(PurpleConnection *))((void *)0)))) 
      cur = ( *(prpl_info -> chat_info))(( *(chat -> account)).gc);
    else 
      cur = ((GList *)((void *)0));
    while(cur != ((GList *)((void *)0))){
      pce = (cur -> data);
      if (!((pce -> secret) != 0) && (!((pce -> required) != 0) && (g_hash_table_lookup((chat -> components),(pce -> identifier)) == ((void *)((void *)0))))) {
        tmp = purple_text_strip_mnemonic((pce -> label));
        name = g_markup_escape_text(tmp,(-1));
        g_free(tmp);
        value = g_markup_escape_text((g_hash_table_lookup((chat -> components),(pce -> identifier))),(-1));
        g_string_append_printf(str,"\n<b>%s</b> %s",((name != 0)?name : ""),((value != 0)?value : ""));
        g_free(name);
        g_free(value);
      }
      g_free(pce);
      cur = g_list_delete_link(cur,cur);
    }
  }
  else if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)) {
/* NOTE: THIS FUNCTION IS NO LONGER CALLED FOR CONTACTS.
		 * It is only called by create_tip_for_node(), and create_tip_for_node() is never called for a contact.
		 */
    PurpleContact *c;
    PurpleBuddy *b;
    PurplePresence *presence;
    PurpleNotifyUserInfo *user_info;
    GList *connections;
    char *tmp;
    time_t idle_secs;
    time_t signon;
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
      c = ((PurpleContact *)node);
      b = purple_contact_get_priority_buddy(c);
    }
    else {
      b = ((PurpleBuddy *)node);
      c = purple_buddy_get_contact(b);
    }
    prpl = purple_find_prpl(purple_account_get_protocol_id((b -> account)));
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
    presence = purple_buddy_get_presence(b);
    user_info = purple_notify_user_info_new();
/* Account */
    connections = purple_connections_get_all();
    if (((full != 0) && (connections != 0)) && ((connections -> next) != 0)) {
      tmp = g_markup_escape_text(purple_account_get_username((purple_buddy_get_account(b))),(-1));
      purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Account"))),tmp);
      g_free(tmp);
    }
/* Alias */
/* If there's not a contact alias, the node is being displayed with
		 * this alias, so there's no point in showing it in the tooltip. */
    if ((((((full != 0) && (c != 0)) && ((b -> alias) != ((char *)((void *)0)))) && ((b -> alias)[0] != 0)) && (((c -> alias) != ((char *)((void *)0))) && ((c -> alias)[0] != 0))) && (strcmp((c -> alias),(b -> alias)) != 0)) {
      tmp = g_markup_escape_text((b -> alias),(-1));
      purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Buddy Alias"))),tmp);
      g_free(tmp);
    }
/* Nickname/Server Alias */
/* I'd like to only show this if there's a contact or buddy
		 * alias, but many people on MSN set long nicknames, which
		 * get ellipsized, so the only way to see the whole thing is
		 * to look at the tooltip. */
    if (((full != 0) && ((b -> server_alias) != ((char *)((void *)0)))) && ((b -> server_alias)[0] != 0)) {
      tmp = g_markup_escape_text((b -> server_alias),(-1));
      purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Nickname"))),tmp);
      g_free(tmp);
    }
/* Logged In */
    signon = purple_presence_get_login_time(presence);
    if (((full != 0) && (((b != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(b))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(b))) != 0))) && (signon > 0)) {
      if (signon > time(0)) {
/*
				 * They signed on in the future?!  Our local clock
				 * must be wrong, show the actual date instead of
				 * "4 days", etc.
				 */
        tmp = g_strdup(purple_date_format_long((localtime((&signon)))));
      }
      else 
        tmp = purple_str_seconds_to_string((time(0) - signon));
      purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Logged In"))),tmp);
      g_free(tmp);
    }
/* Idle */
    if (purple_presence_is_idle(presence) != 0) {
      idle_secs = purple_presence_get_idle_time(presence);
      if (idle_secs > 0) {
        tmp = purple_str_seconds_to_string((time(0) - idle_secs));
        purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Idle"))),tmp);
        g_free(tmp);
      }
    }
/* Last Seen */
    if (((full != 0) && (c != 0)) && !(((b != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(b))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(b))) != 0))) {
      struct _pidgin_blist_node *gtknode = ( *((PurpleBlistNode *)c)).ui_data;
      PurpleBlistNode *bnode;
      int lastseen = 0;
      if ((gtknode != 0) && (!((gtknode -> contact_expanded) != 0) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE))) {
/* We're either looking at a buddy for a collapsed contact or
				 * an expanded contact itself so we show the most recent
				 * (largest) last_seen time for any of the buddies under
				 * the contact. */
        for (bnode = ( *((PurpleBlistNode *)c)).child; bnode != ((PurpleBlistNode *)((void *)0)); bnode = (bnode -> next)) {
          int value = purple_blist_node_get_int(bnode,"last_seen");
          if (value > lastseen) 
            lastseen = value;
        }
      }
      else {
/* We're dealing with a buddy under an expanded contact,
				 * so we show the last_seen time for the buddy. */
        lastseen = purple_blist_node_get_int(&b -> node,"last_seen");
      }
      if (lastseen > 0) {
        tmp = purple_str_seconds_to_string((time(0) - lastseen));
        purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Last Seen"))),tmp);
        g_free(tmp);
      }
    }
/* Offline? */
/* FIXME: Why is this status special-cased by the core? --rlaager
		 * FIXME: Alternatively, why not have the core do all of them? --rlaager */
    if (!(((b != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(b))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(b))) != 0))) {
      purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Status"))),((const char *)(dgettext("pidgin","Offline"))));
    }
    if (((purple_account_is_connected((b -> account)) != 0) && (prpl_info != 0)) && ((prpl_info -> tooltip_text) != 0)) {
/* Additional text from the PRPL */
      ( *(prpl_info -> tooltip_text))(b,user_info,full);
    }
/* These are Easter Eggs.  Patches to remove them will be rejected. */
    if (!(g_ascii_strcasecmp((b -> name),"robflynn") != 0)) 
      purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Description"))),((const char *)(dgettext("pidgin","Spooky"))));
    if (!(g_ascii_strcasecmp((b -> name),"seanegn") != 0)) 
      purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Status"))),((const char *)(dgettext("pidgin","Awesome"))));
    if (!(g_ascii_strcasecmp((b -> name),"chipx86") != 0)) 
      purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Status"))),((const char *)(dgettext("pidgin","Rockin\'"))));
    tmp = purple_notify_user_info_get_text_with_newline(user_info,"\n");
    g_string_append(str,tmp);
    g_free(tmp);
    purple_notify_user_info_destroy(user_info);
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) {
    gint count;
    PurpleGroup *group = (PurpleGroup *)node;
    PurpleNotifyUserInfo *user_info;
    user_info = purple_notify_user_info_new();
    count = purple_blist_get_group_online_count(group);
    if (count != 0) {
/* Online buddies in group */
      tmp = g_strdup_printf("%d",count);
      purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Online Buddies"))),tmp);
      g_free(tmp);
    }
    count = purple_blist_get_group_size(group,0);
    if (count != 0) {
/* Total buddies (from online accounts) in group */
      tmp = g_strdup_printf("%d",count);
      purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Total Buddies"))),tmp);
      g_free(tmp);
    }
    tmp = purple_notify_user_info_get_text_with_newline(user_info,"\n");
    g_string_append(str,tmp);
    g_free(tmp);
    purple_notify_user_info_destroy(user_info);
  }
  purple_signal_emit(pidgin_blist_get_handle(),"drawing-tooltip",node,str,full);
  return g_string_free(str,0);
}
static GHashTable *cached_emblems;

static void _cleanup_cached_emblem(gpointer data,GObject *obj)
{
  g_hash_table_remove(cached_emblems,data);
}

static GdkPixbuf *_pidgin_blist_get_cached_emblem(gchar *path)
{
  GdkPixbuf *pb = (g_hash_table_lookup(cached_emblems,path));
  if (pb != ((GdkPixbuf *)((void *)0))) {
/* The caller gets a reference */
    g_object_ref(pb);
    g_free(path);
  }
  else {
    pb = pidgin_pixbuf_new_from_file(path);
    if (pb != ((GdkPixbuf *)((void *)0))) {
/* We don't want to own a ref to the pixbuf, but we need to keep clean up. */
/* I'm not sure if it would be better to just keep our ref and not let the emblem ever be destroyed */
      g_object_weak_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pb),((GType )(20 << 2))))),_cleanup_cached_emblem,path);
      g_hash_table_insert(cached_emblems,path,pb);
    }
    else 
      g_free(path);
  }
  return pb;
}

GdkPixbuf *pidgin_blist_get_emblem(PurpleBlistNode *node)
{
  PurpleBuddy *buddy = (PurpleBuddy *)((void *)0);
  struct _pidgin_blist_node *gtknode = (node -> ui_data);
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  const char *name = (const char *)((void *)0);
  char *filename;
  char *path;
  PurplePresence *p = (PurplePresence *)((void *)0);
  PurpleStatus *tune;
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    if (!((gtknode -> contact_expanded) != 0)) {
      buddy = purple_contact_get_priority_buddy(((PurpleContact *)node));
    }
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    buddy = ((PurpleBuddy *)node);
    p = purple_buddy_get_presence(buddy);
    if (purple_presence_is_status_primitive_active(p,PURPLE_STATUS_MOBILE) != 0) {
/* This emblem comes from the small emoticon set now,
			 * to reduce duplication. */
      path = g_build_filename("/usr/local/share","pixmaps","pidgin","emotes","small","mobile.png",((void *)((void *)0)));
      return _pidgin_blist_get_cached_emblem(path);
    }
    if (( *((struct _pidgin_blist_node *)( *(node -> parent)).ui_data)).contact_expanded != 0) {
      if (purple_prefs_get_bool("/pidgin/blist/show_protocol_icons") != 0) 
        return 0;
      return pidgin_create_prpl_icon(( *((PurpleBuddy *)node)).account,PIDGIN_PRPL_ICON_SMALL);
    }
  }
  else {
    return 0;
  }
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return 0;
    };
  }while (0);
  if (!(purple_privacy_check((buddy -> account),purple_buddy_get_name(buddy)) != 0)) {
    path = g_build_filename("/usr/local/share","pixmaps","pidgin","emblems","16","blocked.png",((void *)((void *)0)));
    return _pidgin_blist_get_cached_emblem(path);
  }
/* If we came through the contact code flow above, we didn't need
	 * to get the presence until now. */
  if (p == ((PurplePresence *)((void *)0))) 
    p = purple_buddy_get_presence(buddy);
  if (purple_presence_is_status_primitive_active(p,PURPLE_STATUS_MOBILE) != 0) {
/* This emblem comes from the small emoticon set now, to reduce duplication. */
    path = g_build_filename("/usr/local/share","pixmaps","pidgin","emotes","small","mobile.png",((void *)((void *)0)));
    return _pidgin_blist_get_cached_emblem(path);
  }
  tune = purple_presence_get_status(p,"tune");
  if ((tune != 0) && (purple_status_is_active(tune) != 0)) {
/* Only in MSN.
		 * TODO: Replace "Tune" with generalized "Media" in 3.0. */
    if (purple_status_get_attr_string(tune,"game") != ((const char *)((void *)0))) {
      path = g_build_filename("/usr/local/share","pixmaps","pidgin","emblems","16","game.png",((void *)((void *)0)));
      return _pidgin_blist_get_cached_emblem(path);
    }
/* Only in MSN.
		 * TODO: Replace "Tune" with generalized "Media" in 3.0. */
    if (purple_status_get_attr_string(tune,"office") != ((const char *)((void *)0))) {
      path = g_build_filename("/usr/local/share","pixmaps","pidgin","emblems","16","office.png",((void *)((void *)0)));
      return _pidgin_blist_get_cached_emblem(path);
    }
/* Regular old "tune" is the only one in all protocols. */
/* This emblem comes from the small emoticon set now, to reduce duplication. */
    path = g_build_filename("/usr/local/share","pixmaps","pidgin","emotes","small","music.png",((void *)((void *)0)));
    return _pidgin_blist_get_cached_emblem(path);
  }
  prpl = purple_find_prpl(purple_account_get_protocol_id((buddy -> account)));
  if (!(prpl != 0)) 
    return 0;
  prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if ((prpl_info != 0) && ((prpl_info -> list_emblem) != 0)) 
    name = ( *(prpl_info -> list_emblem))(buddy);
  if (name == ((const char *)((void *)0))) {
    PurpleStatus *status;
    if (!(purple_presence_is_status_primitive_active(p,PURPLE_STATUS_MOOD) != 0)) 
      return 0;
    status = purple_presence_get_status(p,"mood");
    name = purple_status_get_attr_string(status,"mood");
    if (!((name != 0) && (( *name) != 0))) 
      return 0;
    path = get_mood_icon_path(name);
  }
  else {
    filename = g_strdup_printf("%s.png",name);
    path = g_build_filename("/usr/local/share","pixmaps","pidgin","emblems","16",filename,((void *)((void *)0)));
    g_free(filename);
  }
/* _pidgin_blist_get_cached_emblem() assumes ownership of path */
  return _pidgin_blist_get_cached_emblem(path);
}

GdkPixbuf *pidgin_blist_get_status_icon(PurpleBlistNode *node,PidginStatusIconSize size)
{
  GdkPixbuf *ret;
  const char *icon = (const char *)((void *)0);
  struct _pidgin_blist_node *gtknode = (node -> ui_data);
  struct _pidgin_blist_node *gtkbuddynode = (struct _pidgin_blist_node *)((void *)0);
  PurpleBuddy *buddy = (PurpleBuddy *)((void *)0);
  PurpleChat *chat = (PurpleChat *)((void *)0);
  GtkIconSize icon_size = gtk_icon_size_from_name(((size == PIDGIN_STATUS_ICON_LARGE)?"pidgin-icon-size-tango-extra-small" : "pidgin-icon-size-tango-microscopic"));
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    if (!((gtknode -> contact_expanded) != 0)) {
      buddy = purple_contact_get_priority_buddy(((PurpleContact *)node));
      if (buddy != ((PurpleBuddy *)((void *)0))) 
        gtkbuddynode = ( *((PurpleBlistNode *)buddy)).ui_data;
    }
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    buddy = ((PurpleBuddy *)node);
    gtkbuddynode = (node -> ui_data);
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
    chat = ((PurpleChat *)node);
  }
  else {
    return 0;
  }
  if ((buddy != 0) || (chat != 0)) {
    PurpleAccount *account;
    PurplePlugin *prpl;
    if (buddy != 0) 
      account = (buddy -> account);
    else 
      account = (chat -> account);
    prpl = purple_find_prpl(purple_account_get_protocol_id(account));
    if (!(prpl != 0)) 
      return 0;
  }
  if (buddy != 0) {
    PurpleConversation *conv = find_conversation_with_buddy(buddy);
    PurplePresence *p;
    gboolean trans;
    if (conv != ((PurpleConversation *)((void *)0))) {
      PidginConversation *gtkconv = (PidginConversation *)(conv -> ui_data);
      if ((gtkconv == ((PidginConversation *)((void *)0))) && (size == PIDGIN_STATUS_ICON_SMALL)) {
        PidginBlistNode *ui = buddy -> node.ui_data;
        if ((ui == ((PidginBlistNode *)((void *)0))) || ((ui -> conv.flags & PIDGIN_BLIST_NODE_HAS_PENDING_MESSAGE) != 0U)) 
          return gtk_widget_render_icon(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_widget_get_type()))),"pidgin-status-message",icon_size,"GtkTreeView");
      }
    }
    p = purple_buddy_get_presence(buddy);
    trans = purple_presence_is_idle(p);
    if (((((buddy != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0)) && (gtkbuddynode != 0)) && ((gtkbuddynode -> recent_signonoff) != 0)) 
      icon = "pidgin-status-login";
    else if ((gtkbuddynode != 0) && ((gtkbuddynode -> recent_signonoff) != 0)) 
      icon = "pidgin-status-logout";
    else if (purple_presence_is_status_primitive_active(p,PURPLE_STATUS_UNAVAILABLE) != 0) 
      if (trans != 0) 
        icon = "pidgin-status-busy-i";
      else 
        icon = "pidgin-status-busy";
    else if (purple_presence_is_status_primitive_active(p,PURPLE_STATUS_AWAY) != 0) 
      if (trans != 0) 
        icon = "pidgin-status-away-i";
      else 
        icon = "pidgin-status-away";
    else if (purple_presence_is_status_primitive_active(p,PURPLE_STATUS_EXTENDED_AWAY) != 0) 
      if (trans != 0) 
        icon = "pidgin-status-xa-i";
      else 
        icon = "pidgin-status-xa";
    else if (purple_presence_is_status_primitive_active(p,PURPLE_STATUS_OFFLINE) != 0) 
      icon = "pidgin-status-offline";
    else if (trans != 0) 
      icon = "pidgin-status-available-i";
    else if (purple_presence_is_status_primitive_active(p,PURPLE_STATUS_INVISIBLE) != 0) 
      icon = "pidgin-status-invisible";
    else 
      icon = "pidgin-status-available";
  }
  else if (chat != 0) {
    icon = "pidgin-status-chat";
  }
  else {
    icon = "pidgin-status-person";
  }
  ret = gtk_widget_render_icon(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_widget_get_type()))),icon,icon_size,"GtkTreeView");
  return ret;
}

static const char *theme_font_get_color_default(PidginThemeFont *font,const char *def)
{
  const char *ret;
  if (!(font != 0) || !((ret = pidgin_theme_font_get_color_describe(font)) != 0)) 
    ret = def;
  return ret;
}

static const char *theme_font_get_face_default(PidginThemeFont *font,const char *def)
{
  const char *ret;
  if (!(font != 0) || !((ret = pidgin_theme_font_get_font_face(font)) != 0)) 
    ret = def;
  return ret;
}

gchar *pidgin_blist_get_name_markup(PurpleBuddy *b,gboolean selected,gboolean aliased)
{
  const char *name;
  const char *name_color;
  const char *name_font;
  const char *status_color;
  const char *status_font;
  char *text = (char *)((void *)0);
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurpleContact *contact;
  PurplePresence *presence;
  struct _pidgin_blist_node *gtkcontactnode = (struct _pidgin_blist_node *)((void *)0);
  char *idletime = (char *)((void *)0);
  char *statustext = (char *)((void *)0);
  char *nametext = (char *)((void *)0);
  PurpleConversation *conv = find_conversation_with_buddy(b);
  gboolean hidden_conv = 0;
  gboolean biglist = purple_prefs_get_bool("/pidgin/blist/show_buddy_icons");
  PidginThemeFont *statusfont = (PidginThemeFont *)((void *)0);
  PidginThemeFont *namefont = (PidginThemeFont *)((void *)0);
  PidginBlistTheme *theme;
  if (conv != ((PurpleConversation *)((void *)0))) {
    PidginBlistNode *ui = b -> node.ui_data;
    if (ui != 0) {
      if ((ui -> conv.flags & PIDGIN_BLIST_NODE_HAS_PENDING_MESSAGE) != 0U) 
        hidden_conv = (!0);
    }
    else {
      if (((PidginConversation *)(conv -> ui_data)) == ((PidginConversation *)((void *)0))) 
        hidden_conv = (!0);
    }
  }
/* XXX Good luck cleaning up this crap */
  contact = ((PurpleContact *)( *((PurpleBlistNode *)b)).parent);
  if (contact != 0) 
    gtkcontactnode = (purple_blist_node_get_ui_data(((PurpleBlistNode *)contact)));
/* Name */
  if (((gtkcontactnode != 0) && !((gtkcontactnode -> contact_expanded) != 0)) && ((contact -> alias) != 0)) 
    name = (contact -> alias);
  else 
    name = purple_buddy_get_alias(b);
/* Raise a contact pre-draw signal here.  THe callback will return an
	 * escaped version of the name. */
  nametext = (purple_signal_emit_return_1(pidgin_blist_get_handle(),"drawing-buddy",b));
  if (!(nametext != 0)) 
    nametext = g_markup_escape_text(name,(strlen(name)));
  presence = purple_buddy_get_presence(b);
/* Name is all that is needed */
  if (!(aliased != 0) || (biglist != 0)) {
/* Status Info */
    prpl = purple_find_prpl(purple_account_get_protocol_id((b -> account)));
    if (prpl != ((PurplePlugin *)((void *)0))) 
      prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
    if (((prpl_info != 0) && ((prpl_info -> status_text) != 0)) && (( *(b -> account)).gc != 0)) {
      char *tmp = ( *(prpl_info -> status_text))(b);
      const char *end;
      if ((tmp != 0) && !(g_utf8_validate(tmp,(-1),&end) != 0)) {
        char *new = g_strndup(tmp,(g_utf8_pointer_to_offset(tmp,end)));
        g_free(tmp);
        tmp = new;
      }
      if (tmp != 0) {
        g_strdelimit(tmp,"\n",32);
        purple_str_strip_char(tmp,13);
      }
      statustext = tmp;
    }
    if (!(purple_presence_is_online(presence) != 0) && !(statustext != 0)) 
      statustext = g_strdup(((const char *)(dgettext("pidgin","Offline"))));
/* Idle Text */
    if ((purple_presence_is_idle(presence) != 0) && (purple_prefs_get_bool("/pidgin/blist/show_idle_time") != 0)) {
      time_t idle_secs = purple_presence_get_idle_time(presence);
      if (idle_secs > 0) {
        int iday;
        int ihrs;
        int imin;
        time_t t;
        time(&t);
        iday = ((t - idle_secs) / (24 * 60 * 60));
        ihrs = ((((t - idle_secs) / 60) / 60) % 24);
        imin = (((t - idle_secs) / 60) % 60);
        if (iday != 0) 
          idletime = g_strdup_printf(((const char *)(dgettext("pidgin","Idle %dd %dh %02dm"))),iday,ihrs,imin);
        else if (ihrs != 0) 
          idletime = g_strdup_printf(((const char *)(dgettext("pidgin","Idle %dh %02dm"))),ihrs,imin);
        else 
          idletime = g_strdup_printf(((const char *)(dgettext("pidgin","Idle %dm"))),imin);
      }
      else 
        idletime = g_strdup(((const char *)(dgettext("pidgin","Idle"))));
    }
  }
/* choose the colors of the text */
  theme = pidgin_blist_get_theme();
  name_color = ((const char *)((void *)0));
  if (theme != 0) {
    if (purple_presence_is_idle(presence) != 0) {
      namefont = (statusfont = pidgin_blist_theme_get_idle_text_info(theme));
      name_color = "dim grey";
    }
    else if (!(purple_presence_is_online(presence) != 0)) {
      namefont = pidgin_blist_theme_get_offline_text_info(theme);
      name_color = "dim grey";
      statusfont = pidgin_blist_theme_get_status_text_info(theme);
    }
    else if (purple_presence_is_available(presence) != 0) {
      namefont = pidgin_blist_theme_get_online_text_info(theme);
      statusfont = pidgin_blist_theme_get_status_text_info(theme);
    }
    else {
      namefont = pidgin_blist_theme_get_away_text_info(theme);
      statusfont = pidgin_blist_theme_get_status_text_info(theme);
    }
  }
  else {
    if (!(selected != 0) && ((purple_presence_is_idle(presence) != 0) || !(purple_presence_is_online(presence) != 0))) {
      name_color = "dim grey";
    }
  }
  name_color = theme_font_get_color_default(namefont,name_color);
  name_font = theme_font_get_face_default(namefont,"");
  status_color = theme_font_get_color_default(statusfont,"dim grey");
  status_font = theme_font_get_face_default(statusfont,"");
  if ((aliased != 0) && (selected != 0)) {
    if (theme != 0) {
      name_color = "black";
      status_color = "black";
    }
    else {
      name_color = ((const char *)((void *)0));
      status_color = ((const char *)((void *)0));
    }
  }
  if (hidden_conv != 0) {
    char *tmp = nametext;
    nametext = g_strdup_printf("<b>%s</b>",tmp);
    g_free(tmp);
  }
/* Put it all together */
  if ((!(aliased != 0) || (biglist != 0)) && ((statustext != 0) || (idletime != 0))) {
/* using <span size='smaller'> breaks the status, so it must be seperated into <small><span>*/
    if (name_color != 0) {
      text = g_strdup_printf("<span font_desc=\'%s\' foreground=\'%s\'>%s</span>\n<small><span font_desc=\'%s\' foreground=\'%s\'>%s%s%s</span></small>",name_font,name_color,nametext,status_font,status_color,((idletime != ((char *)((void *)0)))?idletime : ""),(((idletime != ((char *)((void *)0))) && (statustext != ((char *)((void *)0))))?" - " : ""),((statustext != ((char *)((void *)0)))?statustext : ""));
    }
    else if (status_color != 0) {
      text = g_strdup_printf("<span font_desc=\'%s\'>%s</span>\n<small><span font_desc=\'%s\' foreground=\'%s\'>%s%s%s</span></small>",name_font,nametext,status_font,status_color,((idletime != ((char *)((void *)0)))?idletime : ""),(((idletime != ((char *)((void *)0))) && (statustext != ((char *)((void *)0))))?" - " : ""),((statustext != ((char *)((void *)0)))?statustext : ""));
    }
    else {
      text = g_strdup_printf("<span font_desc=\'%s\'>%s</span>\n<small><span font_desc=\'%s\'>%s%s%s</span></small>",name_font,nametext,status_font,((idletime != ((char *)((void *)0)))?idletime : ""),(((idletime != ((char *)((void *)0))) && (statustext != ((char *)((void *)0))))?" - " : ""),((statustext != ((char *)((void *)0)))?statustext : ""));
    }
  }
  else {
    if (name_color != 0) {
      text = g_strdup_printf("<span font_desc=\'%s\' color=\'%s\'>%s</span>",name_font,name_color,nametext);
    }
    else {
      text = g_strdup_printf("<span font_desc=\'%s\'>%s</span>",name_font,nametext);
    }
  }
  g_free(nametext);
  g_free(statustext);
  g_free(idletime);
  if (hidden_conv != 0) {
    char *tmp = text;
    text = g_strdup_printf("<b>%s</b>",tmp);
    g_free(tmp);
  }
  return text;
}

static void pidgin_blist_restore_position()
{
  int blist_x;
  int blist_y;
  int blist_width;
  int blist_height;
  blist_width = purple_prefs_get_int("/pidgin/blist/width");
/* if the window exists, is hidden, we're saving positions, and the
	 * position is sane... */
  if ((((gtkblist != 0) && ((gtkblist -> window) != 0)) && !((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_object_get_type())))).flags & GTK_VISIBLE) != 0)) && (blist_width != 0)) {
    blist_x = purple_prefs_get_int("/pidgin/blist/x");
    blist_y = purple_prefs_get_int("/pidgin/blist/y");
    blist_height = purple_prefs_get_int("/pidgin/blist/height");
/* ...check position is on screen... */
    if (blist_x >= gdk_screen_width()) 
      blist_x = (gdk_screen_width() - 100);
    else if ((blist_x + blist_width) < 0) 
      blist_x = 100;
    if (blist_y >= gdk_screen_height()) 
      blist_y = (gdk_screen_height() - 100);
    else if ((blist_y + blist_height) < 0) 
      blist_y = 100;
/* ...and move it back. */
    gtk_window_move(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_window_get_type()))),blist_x,blist_y);
    gtk_window_resize(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_window_get_type()))),blist_width,blist_height);
    if (purple_prefs_get_bool("/pidgin/blist/list_maximized") != 0) 
      gtk_window_maximize(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_window_get_type()))));
  }
}

static gboolean pidgin_blist_refresh_timer(PurpleBuddyList *list)
{
  PurpleBlistNode *gnode;
  PurpleBlistNode *cnode;
  if ((gtk_blist_visibility == GDK_VISIBILITY_FULLY_OBSCURED) || !((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_object_get_type())))).flags & GTK_VISIBLE) != 0)) 
    return (!0);
  for (gnode = (list -> root); gnode != 0; gnode = (gnode -> next)) {
    if (!((purple_blist_node_get_type(gnode)) == PURPLE_BLIST_GROUP_NODE)) 
      continue; 
    for (cnode = (gnode -> child); cnode != 0; cnode = (cnode -> next)) {
      if ((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CONTACT_NODE) {
        PurpleBuddy *buddy;
        buddy = purple_contact_get_priority_buddy(((PurpleContact *)cnode));
        if ((buddy != 0) && (purple_presence_is_idle((purple_buddy_get_presence(buddy))) != 0)) 
          pidgin_blist_update_contact(list,((PurpleBlistNode *)buddy));
      }
    }
  }
/* keep on going */
  return (!0);
}

static void pidgin_blist_hide_node(PurpleBuddyList *list,PurpleBlistNode *node,gboolean update)
{
  struct _pidgin_blist_node *gtknode = (struct _pidgin_blist_node *)(node -> ui_data);
  GtkTreeIter iter;
  if ((!(gtknode != 0) || !((gtknode -> row) != 0)) || !(gtkblist != 0)) 
    return ;
  if ((gtkblist -> selected_node) == node) 
    gtkblist -> selected_node = ((PurpleBlistNode *)((void *)0));
  if (get_iter_from_node(node,&iter) != 0) {
    gtk_tree_store_remove((gtkblist -> treemodel),&iter);
    if ((update != 0) && ((((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE))) {
      pidgin_blist_update(list,(node -> parent));
    }
  }
  gtk_tree_row_reference_free((gtknode -> row));
  gtknode -> row = ((GtkTreeRowReference *)((void *)0));
}
static const char *require_connection[] = {("/Buddies/New Instant Message..."), ("/Buddies/Join a Chat..."), ("/Buddies/Get User Info..."), ("/Buddies/Add Buddy..."), ("/Buddies/Add Chat..."), ("/Buddies/Add Group...")};
static const int require_connection_size = (sizeof(require_connection) / sizeof(( *require_connection)));
/**
 * Rebuild dynamic menus and make menu items sensitive/insensitive
 * where appropriate.
 */

static void update_menu_bar(PidginBuddyList *gtkblist)
{
  GtkWidget *widget;
  gboolean sensitive;
  int i;
  do {
    if (gtkblist != ((PidginBuddyList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gtkblist != NULL");
      return ;
    };
  }while (0);
  pidgin_blist_update_accounts_menu();
  sensitive = (purple_connections_get_all() != ((GList *)((void *)0)));
  for (i = 0; i < require_connection_size; i++) {
    widget = gtk_item_factory_get_widget((gtkblist -> ift),require_connection[i]);
    gtk_widget_set_sensitive(widget,sensitive);
  }
  widget = gtk_item_factory_get_widget((gtkblist -> ift),"/Buddies/Join a Chat...");
  gtk_widget_set_sensitive(widget,pidgin_blist_joinchat_is_showable());
  widget = gtk_item_factory_get_widget((gtkblist -> ift),"/Buddies/Add Chat...");
  gtk_widget_set_sensitive(widget,pidgin_blist_joinchat_is_showable());
  widget = gtk_item_factory_get_widget((gtkblist -> ift),"/Tools/Privacy");
  gtk_widget_set_sensitive(widget,sensitive);
  widget = gtk_item_factory_get_widget((gtkblist -> ift),"/Tools/Room List");
  gtk_widget_set_sensitive(widget,pidgin_roomlist_is_showable());
}

static void sign_on_off_cb(PurpleConnection *gc,PurpleBuddyList *blist)
{
  PidginBuddyList *gtkblist = (PidginBuddyList *)(purple_blist_get_ui_data());
  update_menu_bar(gtkblist);
}

static void plugin_changed_cb(PurplePlugin *p,gpointer data)
{
  pidgin_blist_update_plugin_actions();
}

static void unseen_conv_menu()
{
  static GtkWidget *menu = (GtkWidget *)((void *)0);
  GList *convs = (GList *)((void *)0);
  GList *chats;
  GList *ims;
  if (menu != 0) {
    gtk_widget_destroy(menu);
    menu = ((GtkWidget *)((void *)0));
  }
  ims = pidgin_conversations_find_unseen_list(PURPLE_CONV_TYPE_IM,PIDGIN_UNSEEN_TEXT,0,0);
  chats = pidgin_conversations_find_unseen_list(PURPLE_CONV_TYPE_CHAT,PIDGIN_UNSEEN_NICK,0,0);
  if ((ims != 0) && (chats != 0)) 
    convs = g_list_concat(ims,chats);
  else if ((ims != 0) && !(chats != 0)) 
    convs = ims;
  else if (!(ims != 0) && (chats != 0)) 
    convs = chats;
  if (!(convs != 0)) 
/* no conversations added, don't show the menu */
    return ;
  menu = gtk_menu_new();
  pidgin_conversations_fill_menu(menu,convs);
  g_list_free(convs);
  gtk_widget_show_all(menu);
  gtk_menu_popup(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_get_type()))),0,0,0,0,3,gtk_get_current_event_time());
}

static gboolean menutray_press_cb(GtkWidget *widget,GdkEventButton *event)
{
  GList *convs;
  switch(event -> button){
    case 1:
{
      convs = pidgin_conversations_find_unseen_list(PURPLE_CONV_TYPE_IM,PIDGIN_UNSEEN_TEXT,0,1);
      if (!(convs != 0)) 
        convs = pidgin_conversations_find_unseen_list(PURPLE_CONV_TYPE_CHAT,PIDGIN_UNSEEN_NICK,0,1);
      if (convs != 0) {
        pidgin_conv_present_conversation(((PurpleConversation *)(convs -> data)));
        g_list_free(convs);
      }
      break; 
    }
    case 3:
{
      unseen_conv_menu();
      break; 
    }
  }
  return (!0);
}

static void conversation_updated_cb(PurpleConversation *conv,PurpleConvUpdateType type,PidginBuddyList *gtkblist)
{
  GList *convs = (GList *)((void *)0);
  GList *ims;
  GList *chats;
  GList *l = (GList *)((void *)0);
  if (type != PURPLE_CONV_UPDATE_UNSEEN) 
    return ;
  if (((conv -> account) != ((PurpleAccount *)((void *)0))) && ((conv -> name) != ((char *)((void *)0)))) {
    PurpleBuddy *buddy = purple_find_buddy((conv -> account),(conv -> name));
    if (buddy != ((PurpleBuddy *)((void *)0))) 
      pidgin_blist_update_buddy(0,((PurpleBlistNode *)buddy),(!0));
  }
  if ((gtkblist -> menutrayicon) != 0) {
    gtk_widget_destroy((gtkblist -> menutrayicon));
    gtkblist -> menutrayicon = ((GtkWidget *)((void *)0));
  }
  ims = pidgin_conversations_find_unseen_list(PURPLE_CONV_TYPE_IM,PIDGIN_UNSEEN_TEXT,0,0);
  chats = pidgin_conversations_find_unseen_list(PURPLE_CONV_TYPE_CHAT,PIDGIN_UNSEEN_NICK,0,0);
  if ((ims != 0) && (chats != 0)) 
    convs = g_list_concat(ims,chats);
  else if ((ims != 0) && !(chats != 0)) 
    convs = ims;
  else if (!(ims != 0) && (chats != 0)) 
    convs = chats;
  if (convs != 0) {
    GtkWidget *img = (GtkWidget *)((void *)0);
    GString *tooltip_text = (GString *)((void *)0);
    tooltip_text = g_string_new("");
    l = convs;
    while(l != ((GList *)((void *)0))){
      int count = 0;
      PidginConversation *gtkconv = (PidginConversation *)( *((PurpleConversation *)(l -> data))).ui_data;
      if (gtkconv != 0) 
        count = (gtkconv -> unseen_count);
      else if (purple_conversation_get_data((l -> data),"unseen-count") != 0) 
        count = ((gint )((glong )(purple_conversation_get_data((l -> data),"unseen-count"))));
      g_string_append_printf(tooltip_text,(ngettext("%d unread message from %s\n","%d unread messages from %s\n",count)),count,purple_conversation_get_title((l -> data)));
      l = (l -> next);
    }
    if ((tooltip_text -> len) > 0) {
/* get rid of the last newline */
      g_string_truncate(tooltip_text,((tooltip_text -> len) - 1));
      img = gtk_image_new_from_stock("pidgin-pending",gtk_icon_size_from_name("pidgin-icon-size-tango-extra-small"));
      gtkblist -> menutrayicon = gtk_event_box_new();
      gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> menutrayicon)),gtk_container_get_type()))),img);
      gtk_widget_show(img);
      gtk_widget_show((gtkblist -> menutrayicon));
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> menutrayicon)),((GType )(20 << 2))))),"button-press-event",((GCallback )menutray_press_cb),0,0,((GConnectFlags )0));
      pidgin_menu_tray_append(((PidginMenuTray *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> menutray)),pidgin_menu_tray_get_gtype()))),(gtkblist -> menutrayicon),(tooltip_text -> str));
    }
    g_string_free(tooltip_text,(!0));
    g_list_free(convs);
  }
}

static void conversation_deleting_cb(PurpleConversation *conv,PidginBuddyList *gtkblist)
{
  conversation_updated_cb(conv,PURPLE_CONV_UPDATE_UNSEEN,gtkblist);
}

static void conversation_deleted_update_ui_cb(PurpleConversation *conv,struct _pidgin_blist_node *ui)
{
  if (ui -> conv.conv != conv) 
    return ;
  ui -> conv.conv = ((PurpleConversation *)((void *)0));
  ui -> conv.flags = 0;
  ui -> conv.last_message = 0;
}

static void written_msg_update_ui_cb(PurpleAccount *account,const char *who,const char *message,PurpleConversation *conv,PurpleMessageFlags flag,PurpleBlistNode *node)
{
  PidginBlistNode *ui = (node -> ui_data);
  if (((ui -> conv.conv != conv) || !(pidgin_conv_is_hidden(((PidginConversation *)(conv -> ui_data))) != 0)) || !((flag & (PURPLE_MESSAGE_SEND | PURPLE_MESSAGE_RECV)) != 0U)) 
    return ;
  ui -> conv.flags |= PIDGIN_BLIST_NODE_HAS_PENDING_MESSAGE;
  if (((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) && ((flag & PURPLE_MESSAGE_NICK) != 0U)) 
    ui -> conv.flags |= PIDGIN_BLIST_CHAT_HAS_PENDING_MESSAGE_WITH_NICK;
/* XXX: for lack of better data */
  ui -> conv.last_message = time(0);
  pidgin_blist_update(purple_get_blist(),node);
}

static void displayed_msg_update_ui_cb(PidginConversation *gtkconv,PurpleBlistNode *node)
{
  PidginBlistNode *ui = (node -> ui_data);
  if (ui -> conv.conv != (gtkconv -> active_conv)) 
    return ;
  ui -> conv.flags &= (~(PIDGIN_BLIST_NODE_HAS_PENDING_MESSAGE | PIDGIN_BLIST_CHAT_HAS_PENDING_MESSAGE_WITH_NICK));
  pidgin_blist_update(purple_get_blist(),node);
}

static void conversation_created_cb(PurpleConversation *conv,PidginBuddyList *gtkblist)
{
{
    switch(conv -> type){
      case PURPLE_CONV_TYPE_IM:
{
{
          GSList *buddies = purple_find_buddies((conv -> account),(conv -> name));
          while(buddies != 0){{
              PurpleBlistNode *buddy = (buddies -> data);
              struct _pidgin_blist_node *ui = (buddy -> ui_data);
              buddies = g_slist_delete_link(buddies,buddies);
              if (!(ui != 0)) 
                continue; 
              ui -> conv.conv = conv;
              ui -> conv.flags = 0;
              ui -> conv.last_message = 0;
              purple_signal_connect(purple_conversations_get_handle(),"deleting-conversation",ui,((PurpleCallback )conversation_deleted_update_ui_cb),ui);
              purple_signal_connect(purple_conversations_get_handle(),"wrote-im-msg",ui,((PurpleCallback )written_msg_update_ui_cb),buddy);
              purple_signal_connect(pidgin_conversations_get_handle(),"conversation-displayed",ui,((PurpleCallback )displayed_msg_update_ui_cb),buddy);
            }
          }
        }
        break; 
      }
      case PURPLE_CONV_TYPE_CHAT:
{
{
          PurpleChat *chat = purple_blist_find_chat((conv -> account),(conv -> name));
          struct _pidgin_blist_node *ui;
          if (!(chat != 0)) 
            break; 
          ui = chat -> node.ui_data;
          if (!(ui != 0)) 
            break; 
          ui -> conv.conv = conv;
          ui -> conv.flags = 0;
          ui -> conv.last_message = 0;
          purple_signal_connect(purple_conversations_get_handle(),"deleting-conversation",ui,((PurpleCallback )conversation_deleted_update_ui_cb),ui);
          purple_signal_connect(purple_conversations_get_handle(),"wrote-chat-msg",ui,((PurpleCallback )written_msg_update_ui_cb),chat);
          purple_signal_connect(pidgin_conversations_get_handle(),"conversation-displayed",ui,((PurpleCallback )displayed_msg_update_ui_cb),chat);
        }
        break; 
      }
      default:
{
        break; 
      }
    }
  }
}
/**********************************************************************************
 * Public API Functions                                                           *
 **********************************************************************************/

static void pidgin_blist_new_list(PurpleBuddyList *blist)
{
  PidginBuddyList *gtkblist;
  gtkblist = ((PidginBuddyList *)(g_malloc0_n(1,(sizeof(PidginBuddyList )))));
  gtkblist -> connection_errors = g_hash_table_new_full(g_direct_hash,g_direct_equal,0,g_free);
  gtkblist -> priv = ((PidginBuddyListPrivate *)(g_malloc0_n(1,(sizeof(PidginBuddyListPrivate )))));
  blist -> ui_data = gtkblist;
}

static void pidgin_blist_new_node(PurpleBlistNode *node)
{
  node -> ui_data = ((struct _pidgin_blist_node *)(g_malloc0_n(1,(sizeof(struct _pidgin_blist_node )))));
}

gboolean pidgin_blist_node_is_contact_expanded(PurpleBlistNode *node)
{
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    node = (node -> parent);
    if (node == ((PurpleBlistNode *)((void *)0))) 
      return 0;
  }
  do {
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_BLIST_NODE_IS_CONTACT(node)");
      return 0;
    };
  }while (0);
  return ( *((struct _pidgin_blist_node *)(node -> ui_data))).contact_expanded;
}
enum __unnamed_enum___F0_L4798_C1_DRAG_BUDDY__COMMA__DRAG_ROW__COMMA__DRAG_VCARD__COMMA__DRAG_TEXT__COMMA__DRAG_URI__COMMA__NUM_TARGETS {DRAG_BUDDY,DRAG_ROW,DRAG_VCARD,DRAG_TEXT,DRAG_URI,NUM_TARGETS};

static const char *item_factory_translate_func(const char *path,gpointer func_data)
{
  return (const char *)(dgettext("pidgin",((char *)path)));
}

void pidgin_blist_setup_sort_methods()
{
  const char *id;
  pidgin_blist_sort_method_reg("none",((const char *)(dgettext("pidgin","Manually"))),sort_method_none);
  pidgin_blist_sort_method_reg("alphabetical",((const char *)(dgettext("pidgin","Alphabetically"))),sort_method_alphabetical);
  pidgin_blist_sort_method_reg("status",((const char *)(dgettext("pidgin","By status"))),sort_method_status);
  pidgin_blist_sort_method_reg("log_size",((const char *)(dgettext("pidgin","By recent log activity"))),sort_method_log_activity);
  id = purple_prefs_get_string("/pidgin/blist/sort_type");
  if (id == ((const char *)((void *)0))) {
    purple_debug_warning("gtkblist","Sort method was NULL, resetting to alphabetical\n");
    id = "alphabetical";
  }
  pidgin_blist_sort_method_set(id);
}

static void _prefs_change_redo_list(const char *name,PurplePrefType type,gconstpointer val,gpointer data)
{
  GtkTreeSelection *sel;
  GtkTreeIter iter;
  PurpleBlistNode *node = (PurpleBlistNode *)((void *)0);
  sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type()))));
  if (gtk_tree_selection_get_selected(sel,0,&iter) != 0) {
    gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,NODE_COLUMN,&node,-1);
  }
  redo_buddy_list(purple_get_blist(),0,0);
  gtk_tree_view_columns_autosize(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type()))));
  if (node != 0) {
    struct _pidgin_blist_node *gtknode;
    GtkTreePath *path;
    gtknode = (node -> ui_data);
    if ((gtknode != 0) && ((gtknode -> row) != 0)) {
      path = gtk_tree_row_reference_get_path((gtknode -> row));
      gtk_tree_selection_select_path(sel,path);
      gtk_tree_view_scroll_to_cell(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type()))),path,0,0,0,0);
      gtk_tree_path_free(path);
    }
  }
}

static void _prefs_change_sort_method(const char *pref_name,PurplePrefType type,gconstpointer val,gpointer data)
{
  if (!(strcmp(pref_name,"/pidgin/blist/sort_type") != 0)) 
    pidgin_blist_sort_method_set(val);
}

static gboolean pidgin_blist_select_notebook_page_cb(gpointer user_data)
{
  PidginBuddyList *gtkblist = (PidginBuddyList *)user_data;
  int errors = 0;
  GList *list = (GList *)((void *)0);
  PidginBuddyListPrivate *priv;
  priv = ((PidginBuddyListPrivate *)(gtkblist -> priv));
/* this is far too ugly thanks to me not wanting to fix #3989 properly right now */
  if ((priv -> error_scrollbook) != ((PidginScrollBook *)((void *)0))) {
    errors = gtk_notebook_get_n_pages(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)( *(priv -> error_scrollbook)).notebook),gtk_notebook_get_type()))));
  }
  if (((list = purple_accounts_get_all_active()) != ((GList *)((void *)0))) || (errors != 0)) {
    gtk_notebook_set_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> notebook)),gtk_notebook_get_type()))),1);
    g_list_free(list);
  }
  else 
    gtk_notebook_set_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> notebook)),gtk_notebook_get_type()))),0);
  return 0;
}

static void pidgin_blist_select_notebook_page(PidginBuddyList *gtkblist)
{
  purple_timeout_add(0,pidgin_blist_select_notebook_page_cb,gtkblist);
}

static void account_modified(PurpleAccount *account,PidginBuddyList *gtkblist)
{
  if (!(gtkblist != 0)) 
    return ;
  pidgin_blist_select_notebook_page(gtkblist);
  update_menu_bar(gtkblist);
}

static void account_actions_changed(PurpleAccount *account,gpointer data)
{
  pidgin_blist_update_accounts_menu();
}

static void account_status_changed(PurpleAccount *account,PurpleStatus *old,PurpleStatus *new,PidginBuddyList *gtkblist)
{
  if (!(gtkblist != 0)) 
    return ;
  account_modified(account,gtkblist);
}

static gboolean gtk_blist_window_key_press_cb(GtkWidget *w,GdkEventKey *event,PidginBuddyList *gtkblist)
{
  GtkWidget *widget;
  if (!(gtkblist != 0)) 
    return 0;
/* clear any tooltips */
  pidgin_blist_tooltip_destroy();
  widget = gtk_window_get_focus(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_window_get_type()))));
  if (((({
    GTypeInstance *__inst = (GTypeInstance *)widget;
    GType __t = gtk_imhtml_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) || (((
{
    GTypeInstance *__inst = (GTypeInstance *)widget;
    GType __t = gtk_entry_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) 
{
    if (gtk_bindings_activate(((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_object_get_type()))),(event -> keyval),(event -> state)) != 0) 
      return (!0);
  }
  return 0;
}

static gboolean headline_box_enter_cb(GtkWidget *widget,GdkEventCrossing *event,PidginBuddyList *gtkblist)
{
  gdk_window_set_cursor((widget -> window),(gtkblist -> hand_cursor));
  return 0;
}

static gboolean headline_box_leave_cb(GtkWidget *widget,GdkEventCrossing *event,PidginBuddyList *gtkblist)
{
  gdk_window_set_cursor((widget -> window),(gtkblist -> arrow_cursor));
  return 0;
}

static void reset_headline(PidginBuddyList *gtkblist)
{
  gtkblist -> headline_callback = ((GCallback )((void *)0));
  gtkblist -> headline_data = ((gpointer )((void *)0));
  gtkblist -> headline_destroy = ((GDestroyNotify )((void *)0));
  pidgin_set_urgent(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_window_get_type()))),0);
}

static gboolean headline_click_callback(gpointer unused)
{
  if ((gtkblist -> headline_callback) != 0) 
    ( *((GSourceFunc )(gtkblist -> headline_callback)))((gtkblist -> headline_data));
  reset_headline(gtkblist);
  return 0;
}

static gboolean headline_close_press_cb(GtkButton *button,PidginBuddyList *gtkblist)
{
  gtk_widget_hide((gtkblist -> headline_hbox));
  return 0;
}

static gboolean headline_box_press_cb(GtkWidget *widget,GdkEventButton *event,PidginBuddyList *gtkblist)
{
  gtk_widget_hide((gtkblist -> headline_hbox));
  if ((gtkblist -> headline_callback) != 0) 
    g_idle_add(headline_click_callback,0);
  else {
    if ((gtkblist -> headline_destroy) != 0) 
      ( *(gtkblist -> headline_destroy))((gtkblist -> headline_data));
    reset_headline(gtkblist);
  }
  return (!0);
}
/***********************************/
/* Connection error handling stuff */
/***********************************/
#define OBJECT_DATA_KEY_ACCOUNT "account"
#define DO_NOT_CLEAR_ERROR "do-not-clear-error"

static gboolean find_account_widget(GObject *widget,PurpleAccount *account)
{
  if (g_object_get_data(widget,"account") == account) 
/* found */
    return 0;
  else 
    return 1;
}

static void pack_prpl_icon_start(GtkWidget *box,PurpleAccount *account)
{
  GdkPixbuf *pixbuf;
  GtkWidget *image;
  pixbuf = pidgin_create_prpl_icon(account,PIDGIN_PRPL_ICON_SMALL);
  if (pixbuf != ((GdkPixbuf *)((void *)0))) {
    image = gtk_image_new_from_pixbuf(pixbuf);
    g_object_unref(pixbuf);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gtk_box_get_type()))),image,0,0,0);
  }
}

static void add_error_dialog(PidginBuddyList *gtkblist,GtkWidget *dialog)
{
  PidginBuddyListPrivate *priv = (PidginBuddyListPrivate *)(gtkblist -> priv);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> error_scrollbook)),gtk_container_get_type()))),dialog);
}

static GtkWidget *find_child_widget_by_account(GtkContainer *container,PurpleAccount *account)
{
  GList *l = (GList *)((void *)0);
  GList *children = (GList *)((void *)0);
  GtkWidget *ret = (GtkWidget *)((void *)0);
/* XXX: Workaround for the currently incomplete implementation of PidginScrollBook */
  if ((({
    GTypeInstance *__inst = (GTypeInstance *)container;
    GType __t = pidgin_scroll_book_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
    container = ((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *((PidginScrollBook *)(g_type_check_instance_cast(((GTypeInstance *)container),pidgin_scroll_book_get_type())))).notebook),gtk_container_get_type())));
  children = gtk_container_get_children(container);
  l = g_list_find_custom(children,account,((GCompareFunc )find_account_widget));
  if (l != 0) 
    ret = ((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(l -> data)),gtk_widget_get_type())));
  g_list_free(children);
  return ret;
}

static void remove_child_widget_by_account(GtkContainer *container,PurpleAccount *account)
{
  GtkWidget *widget = find_child_widget_by_account(container,account);
  if (widget != 0) {
/* Since we are destroying the widget in response to a change in
		 * error, we should not clear the error.
		 */
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"do-not-clear-error",((gpointer )((gpointer )((glong )(!0)))));
    gtk_widget_destroy(widget);
  }
}
/* Generic error buttons */

static void generic_error_modify_cb(PurpleAccount *account)
{
  purple_account_clear_current_error(account);
  pidgin_account_dialog_show(PIDGIN_MODIFY_ACCOUNT_DIALOG,account);
}

static void generic_error_enable_cb(PurpleAccount *account)
{
  purple_account_clear_current_error(account);
  purple_account_set_enabled(account,purple_core_get_ui(),(!0));
}

static void generic_error_destroy_cb(GtkObject *dialog,PurpleAccount *account)
{
  g_hash_table_remove((gtkblist -> connection_errors),account);
/* If the error dialog is being destroyed in response to the
	 * account-error-changed signal, we don't want to clear the current
	 * error.
	 */
  if (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dialog),((GType )(20 << 2))))),"do-not-clear-error") == ((void *)((void *)0))) 
    purple_account_clear_current_error(account);
}
#define SSL_FAQ_URI "http://d.pidgin.im/wiki/FAQssl"

static void ssl_faq_clicked_cb(PidginMiniDialog *mini_dialog,GtkButton *button,gpointer ignored)
{
  purple_notify_uri(0,"http://d.pidgin.im/wiki/FAQssl");
}

static void add_generic_error_dialog(PurpleAccount *account,const PurpleConnectionErrorInfo *err)
{
  GtkWidget *mini_dialog;
  const char *username = purple_account_get_username(account);
  gboolean enabled = purple_account_get_enabled(account,purple_core_get_ui());
  char *primary;
  if (enabled != 0) 
    primary = g_strdup_printf(((const char *)(dgettext("pidgin","%s disconnected"))),username);
  else 
    primary = g_strdup_printf(((const char *)(dgettext("pidgin","%s disabled"))),username);
  mini_dialog = pidgin_make_mini_dialog(0,"pidgin-dialog-error",primary,(err -> description),account,((enabled != 0)?((const char *)(dgettext("pidgin","Reconnect"))) : ((const char *)(dgettext("pidgin","Re-enable")))),((enabled != 0)?((PurpleCallback )purple_account_connect) : ((PurpleCallback )generic_error_enable_cb)),((const char *)(dgettext("pidgin","Modify Account"))),((PurpleCallback )generic_error_modify_cb),((void *)((void *)0)));
  g_free(primary);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)mini_dialog),((GType )(20 << 2))))),"account",account);
  if ((err -> type) == PURPLE_CONNECTION_ERROR_NO_SSL_SUPPORT) 
    pidgin_mini_dialog_add_non_closing_button(((PidginMiniDialog *)(g_type_check_instance_cast(((GTypeInstance *)mini_dialog),pidgin_mini_dialog_get_type()))),((const char *)(dgettext("pidgin","SSL FAQs"))),ssl_faq_clicked_cb,0);
  g_signal_connect_data(mini_dialog,"destroy",((GCallback )generic_error_destroy_cb),account,0,G_CONNECT_AFTER);
  add_error_dialog(gtkblist,mini_dialog);
}

static void remove_generic_error_dialog(PurpleAccount *account)
{
  PidginBuddyListPrivate *priv = (PidginBuddyListPrivate *)(gtkblist -> priv);
  remove_child_widget_by_account(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> error_scrollbook)),gtk_container_get_type()))),account);
}

static void update_generic_error_message(PurpleAccount *account,const char *description)
{
  PidginBuddyListPrivate *priv = (PidginBuddyListPrivate *)(gtkblist -> priv);
  GtkWidget *mini_dialog = find_child_widget_by_account(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> error_scrollbook)),gtk_container_get_type()))),account);
  pidgin_mini_dialog_set_description(((PidginMiniDialog *)(g_type_check_instance_cast(((GTypeInstance *)mini_dialog),pidgin_mini_dialog_get_type()))),description);
}
/* Notifications about accounts which were disconnected with
 * PURPLE_CONNECTION_ERROR_NAME_IN_USE
 */
typedef void (*AccountFunction)(PurpleAccount *);

static void elsewhere_foreach_account(PidginMiniDialog *mini_dialog,AccountFunction f)
{
  PurpleAccount *account;
  GList *labels = gtk_container_get_children(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(mini_dialog -> contents)),gtk_container_get_type()))));
  GList *l;
  for (l = labels; l != 0; l = (l -> next)) {
    account = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(l -> data)),((GType )(20 << 2))))),"account"));
    if (account != 0) 
      ( *f)(account);
    else 
      purple_debug_warning("gtkblist","mini_dialog\'s child didn\'t have an account stored in it!");
  }
  g_list_free(labels);
}

static void enable_account(PurpleAccount *account)
{
  purple_account_set_enabled(account,purple_core_get_ui(),(!0));
}

static void reconnect_elsewhere_accounts(PidginMiniDialog *mini_dialog,GtkButton *button,gpointer unused)
{
  elsewhere_foreach_account(mini_dialog,enable_account);
}

static void clear_elsewhere_errors(PidginMiniDialog *mini_dialog,gpointer unused)
{
  elsewhere_foreach_account(mini_dialog,purple_account_clear_current_error);
}

static void ensure_signed_on_elsewhere_minidialog(PidginBuddyList *gtkblist)
{
  PidginBuddyListPrivate *priv = (PidginBuddyListPrivate *)(gtkblist -> priv);
  PidginMiniDialog *mini_dialog;
  if ((priv -> signed_on_elsewhere) != 0) 
    return ;
  mini_dialog = (priv -> signed_on_elsewhere = pidgin_mini_dialog_new(((const char *)(dgettext("pidgin","Welcome back!"))),0,"pidgin-disconnect"));
  pidgin_mini_dialog_add_button(mini_dialog,((const char *)(dgettext("pidgin","Re-enable"))),reconnect_elsewhere_accounts,0);
/* Make dismissing the dialog clear the errors.  The "destroy" signal
	 * does not appear to fire at quit, which is fortunate!
	 */
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)mini_dialog),((GType )(20 << 2))))),"destroy",((GCallback )clear_elsewhere_errors),0,0,((GConnectFlags )0));
  add_error_dialog(gtkblist,((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)mini_dialog),gtk_widget_get_type()))));
/* Set priv->signed_on_elsewhere to NULL when the dialog is destroyed */
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)mini_dialog),((GType )(20 << 2))))),"destroy",((GCallback )gtk_widget_destroyed),(&priv -> signed_on_elsewhere),0,((GConnectFlags )0));
}

static void update_signed_on_elsewhere_minidialog_title()
{
  PidginBuddyListPrivate *priv = (PidginBuddyListPrivate *)(gtkblist -> priv);
  PidginMiniDialog *mini_dialog = (priv -> signed_on_elsewhere);
  guint accounts;
  char *title;
  if (mini_dialog == ((PidginMiniDialog *)((void *)0))) 
    return ;
  accounts = pidgin_mini_dialog_get_num_children(mini_dialog);
  if (accounts == 0) {
    gtk_widget_destroy(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)mini_dialog),gtk_widget_get_type()))));
    return ;
  }
  title = g_strdup_printf((ngettext("%d account was disabled because you signed on from another location:","%d accounts were disabled because you signed on from another location:",accounts)),accounts);
  pidgin_mini_dialog_set_description(mini_dialog,title);
  g_free(title);
}

static GtkWidget *create_account_label(PurpleAccount *account)
{
  GtkWidget *hbox;
  GtkWidget *label;
  const char *username = purple_account_get_username(account);
  char *markup;
  hbox = gtk_hbox_new(0,6);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)hbox),((GType )(20 << 2))))),"account",account);
  pack_prpl_icon_start(hbox,account);
  label = gtk_label_new(0);
  markup = g_strdup_printf("<span size=\"smaller\">%s</span>",username);
  gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),markup);
  g_free(markup);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0);
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)label),((GType )(20 << 2))))),"ellipsize",PANGO_ELLIPSIZE_END,((void *)((void *)0)));
#if GTK_CHECK_VERSION(2,12,0)
/* avoid unused variable warnings on pre-2.12 Gtk */
{
    char *description = ( *purple_account_get_current_error(account)).description;
    if ((description != ((char *)((void *)0))) && (( *description) != 0)) 
      gtk_widget_set_tooltip_text(label,description);
  }
#endif
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,(!0),(!0),0);
  return hbox;
}

static void add_to_signed_on_elsewhere(PurpleAccount *account)
{
  PidginBuddyListPrivate *priv = (PidginBuddyListPrivate *)(gtkblist -> priv);
  PidginMiniDialog *mini_dialog;
  GtkWidget *account_label;
  ensure_signed_on_elsewhere_minidialog(gtkblist);
  mini_dialog = (priv -> signed_on_elsewhere);
  if (find_child_widget_by_account(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(mini_dialog -> contents)),gtk_container_get_type()))),account) != 0) 
    return ;
  account_label = create_account_label(account);
  gtk_box_pack_start((mini_dialog -> contents),account_label,0,0,0);
  gtk_widget_show_all(account_label);
  update_signed_on_elsewhere_minidialog_title();
}

static void remove_from_signed_on_elsewhere(PurpleAccount *account)
{
  PidginBuddyListPrivate *priv = (PidginBuddyListPrivate *)(gtkblist -> priv);
  PidginMiniDialog *mini_dialog = (priv -> signed_on_elsewhere);
  if (mini_dialog == ((PidginMiniDialog *)((void *)0))) 
    return ;
  remove_child_widget_by_account(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(mini_dialog -> contents)),gtk_container_get_type()))),account);
  update_signed_on_elsewhere_minidialog_title();
}

static void update_signed_on_elsewhere_tooltip(PurpleAccount *account,const char *description)
{
#if GTK_CHECK_VERSION(2,12,0)
  PidginBuddyListPrivate *priv = (PidginBuddyListPrivate *)(gtkblist -> priv);
  GtkContainer *c = (GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *(priv -> signed_on_elsewhere)).contents),gtk_container_get_type()));
  GtkWidget *label = find_child_widget_by_account(c,account);
  gtk_widget_set_tooltip_text(label,description);
#endif
}
/* Call appropriate error notification code based on error types */

static void update_account_error_state(PurpleAccount *account,const PurpleConnectionErrorInfo *old,const PurpleConnectionErrorInfo *new,PidginBuddyList *gtkblist)
{
  gboolean descriptions_differ;
  const char *desc;
  if ((old == ((const PurpleConnectionErrorInfo *)((void *)0))) && (new == ((const PurpleConnectionErrorInfo *)((void *)0)))) 
    return ;
/* For backwards compatibility: */
  if (new != 0) 
    pidgin_blist_update_account_error_state(account,(new -> description));
  else 
    pidgin_blist_update_account_error_state(account,0);
  if (new != ((const PurpleConnectionErrorInfo *)((void *)0))) 
    pidgin_blist_select_notebook_page(gtkblist);
  if ((old != ((const PurpleConnectionErrorInfo *)((void *)0))) && (new == ((const PurpleConnectionErrorInfo *)((void *)0)))) {
    if ((old -> type) == PURPLE_CONNECTION_ERROR_NAME_IN_USE) 
      remove_from_signed_on_elsewhere(account);
    else 
      remove_generic_error_dialog(account);
    return ;
  }
  if ((old == ((const PurpleConnectionErrorInfo *)((void *)0))) && (new != ((const PurpleConnectionErrorInfo *)((void *)0)))) {
    if ((new -> type) == PURPLE_CONNECTION_ERROR_NAME_IN_USE) 
      add_to_signed_on_elsewhere(account);
    else 
      add_generic_error_dialog(account,new);
    return ;
  }
/* else, new and old are both non-NULL */
  descriptions_differ = strcmp((old -> description),(new -> description));
  desc = (new -> description);
  switch(new -> type){
    case PURPLE_CONNECTION_ERROR_NAME_IN_USE:
{
      if (((old -> type) == PURPLE_CONNECTION_ERROR_NAME_IN_USE) && (descriptions_differ != 0)) {
        update_signed_on_elsewhere_tooltip(account,desc);
      }
      else {
        remove_generic_error_dialog(account);
        add_to_signed_on_elsewhere(account);
      }
      break; 
    }
    default:
{
      if ((old -> type) == PURPLE_CONNECTION_ERROR_NAME_IN_USE) {
        remove_from_signed_on_elsewhere(account);
        add_generic_error_dialog(account,new);
      }
      else if (descriptions_differ != 0) {
        update_generic_error_message(account,desc);
      }
      break; 
    }
  }
}
/* In case accounts are loaded before the blist (which they currently are),
 * let's call update_account_error_state ourselves on every account's current
 * state when the blist starts.
 */

static void show_initial_account_errors(PidginBuddyList *gtkblist)
{
  GList *l = purple_accounts_get_all();
  PurpleAccount *account;
  const PurpleConnectionErrorInfo *err;
  for (; l != 0; l = (l -> next)) {
    account = (l -> data);
    err = purple_account_get_current_error(account);
    update_account_error_state(account,0,err,gtkblist);
  }
}

void pidgin_blist_update_account_error_state(PurpleAccount *account,const char *text)
{
/* connection_errors isn't actually used anywhere; it's just kept in
	 * sync with reality in case a plugin uses it.
	 */
  if (text == ((const char *)((void *)0))) 
    g_hash_table_remove((gtkblist -> connection_errors),account);
  else 
    g_hash_table_insert((gtkblist -> connection_errors),account,(g_strdup(text)));
}

static gboolean paint_headline_hbox(GtkWidget *widget,GdkEventExpose *event,gpointer user_data)
{
  gtk_paint_flat_box((widget -> style),(widget -> window),GTK_STATE_NORMAL,GTK_SHADOW_OUT,0,widget,"tooltip",(widget -> allocation.x + 1),(widget -> allocation.y + 1),(widget -> allocation.width - 2),(widget -> allocation.height - 2));
  return 0;
}
/* This assumes there are not things like groupless buddies or multi-leveled groups.
 * I'm sure other things in this code assumes that also.
 */

static void treeview_style_set(GtkWidget *widget,GtkStyle *prev_style,gpointer data)
{
  PurpleBuddyList *list = data;
  PurpleBlistNode *node = (list -> root);
  while(node != 0){
    pidgin_blist_update_group(list,node);
    node = (node -> next);
  }
}

static void headline_style_set(GtkWidget *widget,GtkStyle *prev_style)
{
  GtkTooltips *tooltips;
  GtkStyle *style;
  if ((gtkblist -> changing_style) != 0) 
    return ;
  tooltips = gtk_tooltips_new();
  g_object_ref_sink(tooltips);
  gtk_tooltips_force_window(tooltips);
#if GTK_CHECK_VERSION(2, 12, 0)
  gtk_widget_set_name((tooltips -> tip_window),"gtk-tooltips");
#endif
  gtk_widget_ensure_style((tooltips -> tip_window));
  style = gtk_widget_get_style((tooltips -> tip_window));
  gtkblist -> changing_style = (!0);
  gtk_widget_set_style((gtkblist -> headline_hbox),style);
  gtkblist -> changing_style = 0;
  g_object_unref(tooltips);
}
/******************************************/
/* End of connection error handling stuff */
/******************************************/

static int blist_focus_cb(GtkWidget *widget,GdkEventFocus *event,PidginBuddyList *gtkblist)
{
  if ((event -> in) != 0) {
    gtk_blist_focused = (!0);
    pidgin_set_urgent(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_window_get_type()))),0);
  }
  else {
    gtk_blist_focused = 0;
  }
  return 0;
}
#if 0
#endif
/* builds the blist layout according to to the current theme */

static void pidgin_blist_build_layout(PurpleBuddyList *list)
{
  GtkTreeViewColumn *column;
  PidginBlistLayout *layout;
  PidginBlistTheme *theme;
  GtkCellRenderer *rend;
  gint i;
  gint status_icon = 0;
  gint text = 1;
  gint emblem = 2;
  gint protocol_icon = 3;
  gint buddy_icon = 4;
  column = (gtkblist -> text_column);
  if (((theme = pidgin_blist_get_theme()) != ((PidginBlistTheme *)((void *)0))) && ((layout = pidgin_blist_theme_get_layout(theme)) != ((PidginBlistLayout *)((void *)0)))) {
    status_icon = (layout -> status_icon);
    text = (layout -> text);
    emblem = (layout -> emblem);
    protocol_icon = (layout -> protocol_icon);
    buddy_icon = (layout -> buddy_icon);
  }
  gtk_tree_view_column_clear(column);
/* group */
  rend = pidgin_cell_renderer_expander_new();
  gtk_tree_view_column_pack_start(column,rend,0);
  gtk_tree_view_column_set_attributes(column,rend,"visible",GROUP_EXPANDER_VISIBLE_COLUMN,"expander-visible",GROUP_EXPANDER_COLUMN,"sensitive",GROUP_EXPANDER_COLUMN,"cell-background-gdk",BGCOLOR_COLUMN,((void *)((void *)0)));
/* contact */
  rend = pidgin_cell_renderer_expander_new();
  gtk_tree_view_column_pack_start(column,rend,0);
  gtk_tree_view_column_set_attributes(column,rend,"visible",CONTACT_EXPANDER_VISIBLE_COLUMN,"expander-visible",CONTACT_EXPANDER_COLUMN,"sensitive",CONTACT_EXPANDER_COLUMN,"cell-background-gdk",BGCOLOR_COLUMN,((void *)((void *)0)));
  for (i = 0; i < 5; i++) {
    if (status_icon == i) {
/* status icons */
      rend = gtk_cell_renderer_pixbuf_new();
      gtk_tree_view_column_pack_start(column,rend,0);
      gtk_tree_view_column_set_attributes(column,rend,"pixbuf",STATUS_ICON_COLUMN,"visible",STATUS_ICON_VISIBLE_COLUMN,"cell-background-gdk",BGCOLOR_COLUMN,((void *)((void *)0)));
      g_object_set(rend,"xalign",0.0,"xpad",6,"ypad",0,((void *)((void *)0)));
    }
    else if (text == i) {
/* name */
      gtkblist -> text_rend = (rend = gtk_cell_renderer_text_new());
      gtk_tree_view_column_pack_start(column,rend,(!0));
      gtk_tree_view_column_set_attributes(column,rend,"cell-background-gdk",BGCOLOR_COLUMN,"markup",NAME_COLUMN,((void *)((void *)0)));
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)rend),((GType )(20 << 2))))),"editing-started",((GCallback )gtk_blist_renderer_editing_started_cb),0,0,((GConnectFlags )0));
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)rend),((GType )(20 << 2))))),"editing-canceled",((GCallback )gtk_blist_renderer_editing_cancelled_cb),list,0,((GConnectFlags )0));
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)rend),((GType )(20 << 2))))),"edited",((GCallback )gtk_blist_renderer_edited_cb),list,0,((GConnectFlags )0));
      g_object_set(rend,"ypad",0,"yalign",0.5,((void *)((void *)0)));
      g_object_set(rend,"ellipsize",PANGO_ELLIPSIZE_END,((void *)((void *)0)));
/* idle */
      rend = gtk_cell_renderer_text_new();
      g_object_set(rend,"xalign",1.0,"ypad",0,((void *)((void *)0)));
      gtk_tree_view_column_pack_start(column,rend,0);
      gtk_tree_view_column_set_attributes(column,rend,"markup",IDLE_COLUMN,"visible",IDLE_VISIBLE_COLUMN,"cell-background-gdk",BGCOLOR_COLUMN,((void *)((void *)0)));
    }
    else if (emblem == i) {
/* emblem */
      rend = gtk_cell_renderer_pixbuf_new();
      g_object_set(rend,"xalign",1.0,"yalign",0.5,"ypad",0,"xpad",3,((void *)((void *)0)));
      gtk_tree_view_column_pack_start(column,rend,0);
      gtk_tree_view_column_set_attributes(column,rend,"pixbuf",EMBLEM_COLUMN,"cell-background-gdk",BGCOLOR_COLUMN,"visible",EMBLEM_VISIBLE_COLUMN,((void *)((void *)0)));
    }
    else if (protocol_icon == i) {
/* protocol icon */
      rend = gtk_cell_renderer_pixbuf_new();
      gtk_tree_view_column_pack_start(column,rend,0);
      gtk_tree_view_column_set_attributes(column,rend,"pixbuf",PROTOCOL_ICON_COLUMN,"visible",PROTOCOL_ICON_VISIBLE_COLUMN,"cell-background-gdk",BGCOLOR_COLUMN,((void *)((void *)0)));
      g_object_set(rend,"xalign",0.0,"xpad",3,"ypad",0,((void *)((void *)0)));
    }
    else if (buddy_icon == i) {
/* buddy icon */
      rend = gtk_cell_renderer_pixbuf_new();
      g_object_set(rend,"xalign",1.0,"ypad",0,((void *)((void *)0)));
      gtk_tree_view_column_pack_start(column,rend,0);
      gtk_tree_view_column_set_attributes(column,rend,"pixbuf",BUDDY_ICON_COLUMN,"cell-background-gdk",BGCOLOR_COLUMN,"visible",BUDDY_ICON_VISIBLE_COLUMN,((void *)((void *)0)));
    }
/* end for loop */
  }
}

static gboolean pidgin_blist_search_equal_func(GtkTreeModel *model,gint column,const gchar *key,GtkTreeIter *iter,gpointer data)
{
  PurpleBlistNode *node = (PurpleBlistNode *)((void *)0);
  gboolean res = (!0);
  const char *compare = (const char *)((void *)0);
  if (!(pidgin_tree_view_search_equal_func(model,column,key,iter,data) != 0)) 
    return 0;
/* If the search string does not match the displayed label, then look
	 * at the alternate labels for the nodes and search in them. Currently,
	 * alternate labels that make sense are usernames/email addresses for
	 * buddies (but only for the ones who don't have a local alias).
	 */
  gtk_tree_model_get(model,iter,NODE_COLUMN,&node,-1);
  if (!(node != 0)) 
    return (!0);
  compare = ((const char *)((void *)0));
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    PurpleBuddy *b = purple_contact_get_priority_buddy(((PurpleContact *)node));
    if (!(purple_buddy_get_local_buddy_alias(b) != 0)) 
      compare = purple_buddy_get_name(b);
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    if (!(purple_buddy_get_local_buddy_alias(((PurpleBuddy *)node)) != 0)) 
      compare = purple_buddy_get_name(((PurpleBuddy *)node));
  }
  if (compare != 0) {
    char *tmp;
    char *enteredstring;
    tmp = g_utf8_normalize(key,(-1),G_NORMALIZE_DEFAULT);
    enteredstring = g_utf8_casefold(tmp,(-1));
    g_free(tmp);
    if (purple_str_has_prefix(compare,enteredstring) != 0) 
      res = 0;
    g_free(enteredstring);
  }
  return res;
}

static void pidgin_blist_show(PurpleBuddyList *list)
{
  PidginBuddyListPrivate *priv;
  void *handle;
  GtkTreeViewColumn *column;
  GtkWidget *menu;
  GtkWidget *ebox;
  GtkWidget *sep;
  GtkWidget *label;
  GtkWidget *close;
  char *pretty;
  char *tmp;
  const char *theme_name;
  GtkAccelGroup *accel_group;
  GtkTreeSelection *selection;
  GtkTargetEntry dte[] = {{("PURPLE_BLIST_NODE"), (GTK_TARGET_SAME_APP), (DRAG_ROW)}, {("application/x-im-contact"), (0), (DRAG_BUDDY)}, {("text/x-vcard"), (0), (DRAG_VCARD)}, {("text/uri-list"), (0), (DRAG_URI)}, {("text/plain"), (0), (DRAG_TEXT)}};
  GtkTargetEntry ste[] = {{("PURPLE_BLIST_NODE"), (GTK_TARGET_SAME_APP), (DRAG_ROW)}, {("application/x-im-contact"), (0), (DRAG_BUDDY)}, {("text/x-vcard"), (0), (DRAG_VCARD)}};
  if ((gtkblist != 0) && ((gtkblist -> window) != 0)) {
    purple_blist_set_visible(purple_prefs_get_bool("/pidgin/blist/list_visible"));
    return ;
  }
  gtkblist = ((PidginBuddyList *)(purple_blist_get_ui_data()));
  priv = ((PidginBuddyListPrivate *)(gtkblist -> priv));
  if ((priv -> current_theme) != 0) 
    g_object_unref((priv -> current_theme));
  theme_name = purple_prefs_get_string("/pidgin/blist/theme");
  if ((theme_name != 0) && (( *theme_name) != 0)) 
    priv -> current_theme = (g_object_ref(((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)(purple_theme_manager_find_theme(theme_name,"blist"))),pidgin_blist_theme_get_type())))));
  else 
    priv -> current_theme = ((PidginBlistTheme *)((void *)0));
  gtkblist -> empty_avatar = gdk_pixbuf_new(GDK_COLORSPACE_RGB,(!0),8,32,32);
  gdk_pixbuf_fill((gtkblist -> empty_avatar),0);
  gtkblist -> window = pidgin_create_window(((const char *)(dgettext("pidgin","Buddy List"))),0,"buddy_list",(!0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),((GType )(20 << 2))))),"focus-in-event",((GCallback )blist_focus_cb),gtkblist,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),((GType )(20 << 2))))),"focus-out-event",((GCallback )blist_focus_cb),gtkblist,0,((GConnectFlags )0));
  ( *((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_window_get_type())))).allow_shrink = (!0);
  gtkblist -> main_vbox = gtk_vbox_new(0,0);
  gtk_widget_show((gtkblist -> main_vbox));
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_container_get_type()))),(gtkblist -> main_vbox));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),((GType )(20 << 2))))),"delete_event",((GCallback )gtk_blist_delete_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),((GType )(20 << 2))))),"configure_event",((GCallback )gtk_blist_configure_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),((GType )(20 << 2))))),"visibility_notify_event",((GCallback )gtk_blist_visibility_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),((GType )(20 << 2))))),"window_state_event",((GCallback )gtk_blist_window_state_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),((GType )(20 << 2))))),"key_press_event",((GCallback )gtk_blist_window_key_press_cb),gtkblist,0,((GConnectFlags )0));
  gtk_widget_add_events((gtkblist -> window),GDK_VISIBILITY_NOTIFY_MASK);
/******************************* Menu bar *************************************/
  accel_group = gtk_accel_group_new();
  gtk_window_add_accel_group(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_window_get_type()))),accel_group);
  g_object_unref(accel_group);
  gtkblist -> ift = gtk_item_factory_new(gtk_menu_bar_get_type(),"<PurpleMain>",accel_group);
  gtk_item_factory_set_translate_func((gtkblist -> ift),((GtkTranslateFunc )item_factory_translate_func),0,0);
  gtk_item_factory_create_items((gtkblist -> ift),(sizeof(blist_menu) / sizeof(( *blist_menu))),blist_menu,0);
  pidgin_load_accels();
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)accel_group),((GType )(20 << 2))))),"accel-changed",((GCallback )pidgin_save_accels_cb),0,0,((GConnectFlags )0));
  menu = gtk_item_factory_get_widget((gtkblist -> ift),"<PurpleMain>");
  gtkblist -> menutray = pidgin_menu_tray_new();
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),(gtkblist -> menutray));
  gtk_widget_show((gtkblist -> menutray));
  gtk_widget_show(menu);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> main_vbox)),gtk_box_get_type()))),menu,0,0,0);
  accountmenu = gtk_item_factory_get_widget((gtkblist -> ift),"/Accounts");
/****************************** Notebook *************************************/
  gtkblist -> notebook = gtk_notebook_new();
  gtk_notebook_set_show_tabs(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> notebook)),gtk_notebook_get_type()))),0);
  gtk_notebook_set_show_border(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> notebook)),gtk_notebook_get_type()))),0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> main_vbox)),gtk_box_get_type()))),(gtkblist -> notebook),(!0),(!0),0);
#if 0
#endif
/* Translators: Please maintain the use of -> and <- to refer to menu heirarchy */
  tmp = g_strdup_printf(((const char *)(dgettext("pidgin","<span weight=\'bold\' size=\'larger\'>Welcome to %s!</span>\n\nYou have no accounts enabled. Enable your IM accounts from the <b>Accounts</b> window at <b>Accounts->Manage Accounts</b>. Once you enable accounts, you\'ll be able to sign on, set your status, and talk to your friends."))),((const char *)(dgettext("pidgin","Pidgin"))));
  pretty = pidgin_make_pretty_arrows(tmp);
  g_free(tmp);
  label = gtk_label_new(0);
  gtk_widget_set_size_request(label,(purple_prefs_get_int("/pidgin/blist/width") - 12),(-1));
  gtk_label_set_line_wrap(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(!0));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0.5,0.2);
  gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),pretty);
  g_free(pretty);
  gtk_notebook_append_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> notebook)),gtk_notebook_get_type()))),label,0);
  gtkblist -> vbox = gtk_vbox_new(0,0);
  gtk_notebook_append_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> notebook)),gtk_notebook_get_type()))),(gtkblist -> vbox),0);
  gtk_widget_show_all((gtkblist -> notebook));
  pidgin_blist_select_notebook_page(gtkblist);
  ebox = gtk_event_box_new();
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> vbox)),gtk_box_get_type()))),ebox,0,0,0);
  gtkblist -> headline_hbox = gtk_hbox_new(0,3);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> headline_hbox)),gtk_container_get_type()))),6);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ebox),gtk_container_get_type()))),(gtkblist -> headline_hbox));
  gtkblist -> headline_image = gtk_image_new_from_pixbuf(0);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> headline_image)),gtk_misc_get_type()))),0.0,0);
  gtkblist -> headline_label = gtk_label_new(0);
  gtk_widget_set_size_request((gtkblist -> headline_label),(purple_prefs_get_int("/pidgin/blist/width") - 25),(-1));
  gtk_label_set_line_wrap(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> headline_label)),gtk_label_get_type()))),(!0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> headline_hbox)),gtk_box_get_type()))),(gtkblist -> headline_image),0,0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> headline_hbox)),gtk_box_get_type()))),(gtkblist -> headline_label),(!0),(!0),0);
/* connecting on headline_hbox doesn't work, because
	                                                the signal is not emitted when theme is changed */
  g_signal_connect_data((gtkblist -> headline_label),"style-set",((GCallback )headline_style_set),0,0,((GConnectFlags )0));
  g_signal_connect_data((gtkblist -> headline_hbox),"expose_event",((GCallback )paint_headline_hbox),0,0,((GConnectFlags )0));
  gtk_widget_set_name((gtkblist -> headline_hbox),"gtk-tooltips");
  gtkblist -> headline_close = gtk_widget_render_icon(ebox,"gtk-close",gtk_icon_size_from_name("pidgin-icon-size-tango-microscopic"),0);
  gtkblist -> hand_cursor = gdk_cursor_new(GDK_HAND2);
  gtkblist -> arrow_cursor = gdk_cursor_new(GDK_LEFT_PTR);
/* Close button. */
  close = gtk_image_new_from_stock("gtk-close",GTK_ICON_SIZE_MENU);
  close = pidgin_create_small_button(close);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> headline_hbox)),gtk_box_get_type()))),close,0,0,0);
#if GTK_CHECK_VERSION(2,12,0)
  gtk_widget_set_tooltip_text(close,((const char *)(dgettext("pidgin","Close"))));
#endif
  g_signal_connect_data(close,"clicked",((GCallback )headline_close_press_cb),gtkblist,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ebox),((GType )(20 << 2))))),"enter-notify-event",((GCallback )headline_box_enter_cb),gtkblist,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ebox),((GType )(20 << 2))))),"leave-notify-event",((GCallback )headline_box_leave_cb),gtkblist,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ebox),((GType )(20 << 2))))),"button-press-event",((GCallback )headline_box_press_cb),gtkblist,0,((GConnectFlags )0));
/****************************** GtkTreeView **********************************/
  gtkblist -> treemodel = gtk_tree_store_new(BLIST_COLUMNS,gdk_pixbuf_get_type(),((GType )(5 << 2)),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(5 << 2)),gdk_pixbuf_get_type(),((GType )(5 << 2)),((GType )(17 << 2)),gdk_color_get_type(),((GType )(5 << 2)),((GType )(5 << 2)),((GType )(5 << 2)),((GType )(5 << 2)),gdk_pixbuf_get_type(),((GType )(5 << 2)),gdk_pixbuf_get_type(),((GType )(5 << 2)));
/* Status icon */
/* Status icon visible */
/* Name */
/* Idle */
/* Idle visible */
/* Buddy icon */
/* Buddy icon visible */
/* Node */
/* bgcolor */
/* Group expander */
/* Group expander visible */
/* Contact expander */
/* Contact expander visible */
/* Emblem */
/* Emblem visible */
/* Protocol icon */
/* Protocol visible */
  gtkblist -> treeview = gtk_tree_view_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))));
  gtk_widget_show((gtkblist -> treeview));
  gtk_widget_set_name((gtkblist -> treeview),"pidgin_blist_treeview");
  g_signal_connect_data((gtkblist -> treeview),"style-set",((GCallback )treeview_style_set),list,0,((GConnectFlags )0));
/* Set up selection stuff */
  selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type()))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)selection),((GType )(20 << 2))))),"changed",((GCallback )pidgin_blist_selection_changed),0,0,((GConnectFlags )0));
/* Set up dnd */
  gtk_tree_view_enable_model_drag_source(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type()))),GDK_BUTTON1_MASK,ste,3,GDK_ACTION_COPY);
  gtk_tree_view_enable_model_drag_dest(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type()))),dte,5,(GDK_ACTION_COPY | GDK_ACTION_MOVE));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),((GType )(20 << 2))))),"drag-data-received",((GCallback )pidgin_blist_drag_data_rcv_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),((GType )(20 << 2))))),"drag-data-get",((GCallback )pidgin_blist_drag_data_get_cb),0,0,((GConnectFlags )0));
#ifdef _WIN32
#endif
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),((GType )(20 << 2))))),"drag-motion",((GCallback )pidgin_blist_drag_motion_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),((GType )(20 << 2))))),"motion-notify-event",((GCallback )pidgin_blist_motion_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),((GType )(20 << 2))))),"leave-notify-event",((GCallback )pidgin_blist_leave_cb),0,0,((GConnectFlags )0));
/* Tooltips */
  pidgin_tooltip_setup_for_treeview((gtkblist -> treeview),0,pidgin_blist_create_tooltip,pidgin_blist_paint_tip);
  gtk_tree_view_set_headers_visible(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type()))),0);
/* expander columns */
  column = gtk_tree_view_column_new();
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type()))),column);
  gtk_tree_view_column_set_visible(column,0);
  gtk_tree_view_set_expander_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type()))),column);
/* everything else column */
  gtkblist -> text_column = gtk_tree_view_column_new();
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type()))),(gtkblist -> text_column));
  pidgin_blist_build_layout(list);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),((GType )(20 << 2))))),"row-activated",((GCallback )gtk_blist_row_activated_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),((GType )(20 << 2))))),"row-expanded",((GCallback )gtk_blist_row_expanded_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),((GType )(20 << 2))))),"row-collapsed",((GCallback )gtk_blist_row_collapsed_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),((GType )(20 << 2))))),"button-press-event",((GCallback )gtk_blist_button_press_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),((GType )(20 << 2))))),"key-press-event",((GCallback )gtk_blist_key_press_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),((GType )(20 << 2))))),"popup-menu",((GCallback )pidgin_blist_popup_menu_cb),0,0,((GConnectFlags )0));
/* Enable CTRL+F searching */
  gtk_tree_view_set_search_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type()))),NAME_COLUMN);
  gtk_tree_view_set_search_equal_func(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type()))),pidgin_blist_search_equal_func,0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> vbox)),gtk_box_get_type()))),pidgin_make_scrollable((gtkblist -> treeview),GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC,GTK_SHADOW_NONE,-1,-1),(!0),(!0),0);
  sep = gtk_hseparator_new();
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> vbox)),gtk_box_get_type()))),sep,0,0,0);
  gtkblist -> scrollbook = pidgin_scroll_book_new();
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> vbox)),gtk_box_get_type()))),(gtkblist -> scrollbook),0,0,0);
/* Create an vbox which holds the scrollbook which is actually used to
	 * display connection errors.  The vbox needs to still exist for
	 * backwards compatibility.
	 */
  gtkblist -> error_buttons = gtk_vbox_new(0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> vbox)),gtk_box_get_type()))),(gtkblist -> error_buttons),0,0,0);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> error_buttons)),gtk_container_get_type()))),0);
  priv -> error_scrollbook = ((PidginScrollBook *)(g_type_check_instance_cast(((GTypeInstance *)(pidgin_scroll_book_new())),pidgin_scroll_book_get_type())));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> error_buttons)),gtk_box_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> error_scrollbook)),gtk_widget_get_type()))),0,0,0);
/* Add the statusbox */
  gtkblist -> statusbox = pidgin_status_box_new();
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> vbox)),gtk_box_get_type()))),(gtkblist -> statusbox),0,(!0),0);
  gtk_widget_set_name((gtkblist -> statusbox),"pidgin_blist_statusbox");
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> statusbox)),gtk_container_get_type()))),3);
  gtk_widget_show((gtkblist -> statusbox));
/* set the Show Offline Buddies option. must be done
	 * after the treeview or faceprint gets mad. -Robot101
	 */
  gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_item_factory_get_item((gtkblist -> ift),"/Buddies/Show/Offline Buddies"))),gtk_check_menu_item_get_type()))),purple_prefs_get_bool("/pidgin/blist/show_offline_buddies"));
  gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_item_factory_get_item((gtkblist -> ift),"/Buddies/Show/Empty Groups"))),gtk_check_menu_item_get_type()))),purple_prefs_get_bool("/pidgin/blist/show_empty_groups"));
  gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_item_factory_get_item((gtkblist -> ift),"/Tools/Mute Sounds"))),gtk_check_menu_item_get_type()))),purple_prefs_get_bool("/pidgin/sound/mute"));
  gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_item_factory_get_item((gtkblist -> ift),"/Buddies/Show/Buddy Details"))),gtk_check_menu_item_get_type()))),purple_prefs_get_bool("/pidgin/blist/show_buddy_icons"));
  gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_item_factory_get_item((gtkblist -> ift),"/Buddies/Show/Idle Times"))),gtk_check_menu_item_get_type()))),purple_prefs_get_bool("/pidgin/blist/show_idle_time"));
  gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_item_factory_get_item((gtkblist -> ift),"/Buddies/Show/Protocol Icons"))),gtk_check_menu_item_get_type()))),purple_prefs_get_bool("/pidgin/blist/show_protocol_icons"));
  if (!(strcmp(purple_prefs_get_string("/pidgin/sound/method"),"none") != 0)) 
    gtk_widget_set_sensitive(gtk_item_factory_get_widget((gtkblist -> ift),"/Tools/Mute Sounds"),0);
/* Update some dynamic things */
  update_menu_bar(gtkblist);
  pidgin_blist_update_plugin_actions();
  pidgin_blist_update_sort_methods();
/* OK... let's show this bad boy. */
  pidgin_blist_refresh(list);
  pidgin_blist_restore_position();
  gtk_widget_show_all(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> vbox)),gtk_widget_get_type()))));
  gtk_widget_realize(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_widget_get_type()))));
  purple_blist_set_visible(purple_prefs_get_bool("/pidgin/blist/list_visible"));
/* start the refresh timer */
  gtkblist -> refresh_timer = purple_timeout_add_seconds(30,((GSourceFunc )pidgin_blist_refresh_timer),list);
  handle = pidgin_blist_get_handle();
/* things that affect how buddies are displayed */
  purple_prefs_connect_callback(handle,"/pidgin/blist/show_buddy_icons",_prefs_change_redo_list,0);
  purple_prefs_connect_callback(handle,"/pidgin/blist/show_idle_time",_prefs_change_redo_list,0);
  purple_prefs_connect_callback(handle,"/pidgin/blist/show_empty_groups",_prefs_change_redo_list,0);
  purple_prefs_connect_callback(handle,"/pidgin/blist/show_offline_buddies",_prefs_change_redo_list,0);
  purple_prefs_connect_callback(handle,"/pidgin/blist/show_protocol_icons",_prefs_change_redo_list,0);
/* sorting */
  purple_prefs_connect_callback(handle,"/pidgin/blist/sort_type",_prefs_change_sort_method,0);
/* menus */
  purple_prefs_connect_callback(handle,"/pidgin/sound/mute",pidgin_blist_mute_pref_cb,0);
  purple_prefs_connect_callback(handle,"/pidgin/sound/method",pidgin_blist_sound_method_pref_cb,0);
/* Setup some purple signal handlers. */
  handle = purple_accounts_get_handle();
  purple_signal_connect(handle,"account-enabled",gtkblist,((PurpleCallback )account_modified),gtkblist);
  purple_signal_connect(handle,"account-disabled",gtkblist,((PurpleCallback )account_modified),gtkblist);
  purple_signal_connect(handle,"account-removed",gtkblist,((PurpleCallback )account_modified),gtkblist);
  purple_signal_connect(handle,"account-status-changed",gtkblist,((PurpleCallback )account_status_changed),gtkblist);
  purple_signal_connect(handle,"account-error-changed",gtkblist,((PurpleCallback )update_account_error_state),gtkblist);
  purple_signal_connect(handle,"account-actions-changed",gtkblist,((PurpleCallback )account_actions_changed),0);
  handle = pidgin_account_get_handle();
  purple_signal_connect(handle,"account-modified",gtkblist,((PurpleCallback )account_modified),gtkblist);
  handle = purple_connections_get_handle();
  purple_signal_connect(handle,"signed-on",gtkblist,((PurpleCallback )sign_on_off_cb),list);
  purple_signal_connect(handle,"signed-off",gtkblist,((PurpleCallback )sign_on_off_cb),list);
  handle = purple_plugins_get_handle();
  purple_signal_connect(handle,"plugin-load",gtkblist,((PurpleCallback )plugin_changed_cb),0);
  purple_signal_connect(handle,"plugin-unload",gtkblist,((PurpleCallback )plugin_changed_cb),0);
  handle = purple_conversations_get_handle();
  purple_signal_connect(handle,"conversation-updated",gtkblist,((PurpleCallback )conversation_updated_cb),gtkblist);
  purple_signal_connect(handle,"deleting-conversation",gtkblist,((PurpleCallback )conversation_deleting_cb),gtkblist);
  purple_signal_connect(handle,"conversation-created",gtkblist,((PurpleCallback )conversation_created_cb),gtkblist);
  gtk_widget_hide((gtkblist -> headline_hbox));
  show_initial_account_errors(gtkblist);
/* emit our created signal */
  handle = pidgin_blist_get_handle();
  purple_signal_emit(handle,"gtkblist-created",list);
}

static void redo_buddy_list(PurpleBuddyList *list,gboolean remove,gboolean rerender)
{
  PurpleBlistNode *node;
  gtkblist = ((PidginBuddyList *)(purple_blist_get_ui_data()));
  if (!(gtkblist != 0) || !((gtkblist -> treeview) != 0)) 
    return ;
  node = (list -> root);
  while(node != 0){
/* This is only needed when we're reverting to a non-GTK+ sorted
		 * status.  We shouldn't need to remove otherwise.
		 */
    if ((remove != 0) && !((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE)) 
      pidgin_blist_hide_node(list,node,0);
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) 
      pidgin_blist_update_buddy(list,node,rerender);
    else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) 
      pidgin_blist_update(list,node);
    else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) 
      pidgin_blist_update(list,node);
    node = purple_blist_node_next(node,0);
  }
}

void pidgin_blist_refresh(PurpleBuddyList *list)
{
  redo_buddy_list(list,0,(!0));
}

void pidgin_blist_update_refresh_timeout()
{
  PurpleBuddyList *blist;
  PidginBuddyList *gtkblist;
  blist = purple_get_blist();
  gtkblist = ((PidginBuddyList *)(purple_blist_get_ui_data()));
  gtkblist -> refresh_timer = purple_timeout_add_seconds(30,((GSourceFunc )pidgin_blist_refresh_timer),blist);
}

static gboolean get_iter_from_node(PurpleBlistNode *node,GtkTreeIter *iter)
{
  struct _pidgin_blist_node *gtknode = (struct _pidgin_blist_node *)(node -> ui_data);
  GtkTreePath *path;
  if (!(gtknode != 0)) {
    return 0;
  }
  if (!(gtkblist != 0)) {
    purple_debug_error("gtkblist","get_iter_from_node was called, but we don\'t seem to have a blist\n");
    return 0;
  }
  if (!((gtknode -> row) != 0)) 
    return 0;
  if ((path = gtk_tree_row_reference_get_path((gtknode -> row))) == ((GtkTreePath *)((void *)0))) 
    return 0;
  if (!(gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),iter,path) != 0)) {
    gtk_tree_path_free(path);
    return 0;
  }
  gtk_tree_path_free(path);
  return (!0);
}

static void pidgin_blist_remove(PurpleBuddyList *list,PurpleBlistNode *node)
{
  struct _pidgin_blist_node *gtknode = (node -> ui_data);
  purple_request_close_with_handle(node);
  pidgin_blist_hide_node(list,node,(!0));
  if ((node -> parent) != 0) 
    pidgin_blist_update(list,(node -> parent));
/* There's something I don't understand here - Ethan */
/* Ethan said that back in 2003, but this g_free has been left commented
	 * out ever since. I can't find any reason at all why this is bad and
	 * valgrind found several reasons why it's good. If this causes problems
	 * comment it out again. Stu */
/* Of course it still causes problems - this breaks dragging buddies into
	 * contacts, the dragged buddy mysteriously 'disappears'. Stu. */
/* I think it's fixed now. Stu. */
  if (gtknode != 0) {
    if ((gtknode -> recent_signonoff_timer) > 0) 
      purple_timeout_remove((gtknode -> recent_signonoff_timer));
    purple_signals_disconnect_by_handle((node -> ui_data));
    g_free((node -> ui_data));
    node -> ui_data = ((void *)((void *)0));
  }
}

static gboolean do_selection_changed(PurpleBlistNode *new_selection)
{
  PurpleBlistNode *old_selection = (PurpleBlistNode *)((void *)0);
/* test for gtkblist because crazy timeout means we can be called after the blist is gone */
  if ((gtkblist != 0) && (new_selection != (gtkblist -> selected_node))) {
    old_selection = (gtkblist -> selected_node);
    gtkblist -> selected_node = new_selection;
    if (new_selection != 0) 
      pidgin_blist_update(0,new_selection);
    if (old_selection != 0) 
      pidgin_blist_update(0,old_selection);
  }
  return 0;
}

static void pidgin_blist_selection_changed(GtkTreeSelection *selection,gpointer data)
{
  PurpleBlistNode *new_selection = (PurpleBlistNode *)((void *)0);
  GtkTreeIter iter;
  if (gtk_tree_selection_get_selected(selection,0,&iter) != 0) {
    gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,NODE_COLUMN,&new_selection,-1);
  }
/* we set this up as a timeout, otherwise the blist flickers ...
	 * but we don't do it for groups, because it causes total bizarness -
	 * the previously selected buddy node might rendered at half height.
	 */
  if ((new_selection != ((PurpleBlistNode *)((void *)0))) && ((purple_blist_node_get_type(new_selection)) == PURPLE_BLIST_GROUP_NODE)) {
    do_selection_changed(new_selection);
  }
  else {
    g_timeout_add(0,((GSourceFunc )do_selection_changed),new_selection);
  }
}

static gboolean insert_node(PurpleBuddyList *list,PurpleBlistNode *node,GtkTreeIter *iter)
{
  GtkTreeIter parent_iter;
  GtkTreeIter cur;
  GtkTreeIter *curptr = (GtkTreeIter *)((void *)0);
  struct _pidgin_blist_node *gtknode = (node -> ui_data);
  GtkTreePath *newpath;
  if (!(iter != 0)) 
    return 0;
  if (((node -> parent) != 0) && !(get_iter_from_node((node -> parent),&parent_iter) != 0)) 
    return 0;
  if (get_iter_from_node(node,&cur) != 0) 
    curptr = &cur;
  if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE)) {
    ( *(current_sort_method -> func))(node,list,parent_iter,curptr,iter);
  }
  else {
    sort_method_none(node,list,parent_iter,curptr,iter);
  }
  if (gtknode != ((struct _pidgin_blist_node *)((void *)0))) {
    gtk_tree_row_reference_free((gtknode -> row));
  }
  else {
    pidgin_blist_new_node(node);
    gtknode = ((struct _pidgin_blist_node *)(node -> ui_data));
  }
  newpath = gtk_tree_model_get_path(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),iter);
  gtknode -> row = gtk_tree_row_reference_new(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),newpath);
  gtk_tree_path_free(newpath);
  if (!(editing_blist != 0)) 
    gtk_tree_store_set((gtkblist -> treemodel),iter,NODE_COLUMN,node,-1);
  if ((node -> parent) != 0) {
    GtkTreePath *expand = (GtkTreePath *)((void *)0);
    struct _pidgin_blist_node *gtkparentnode = ( *(node -> parent)).ui_data;
    if ((purple_blist_node_get_type((node -> parent))) == PURPLE_BLIST_GROUP_NODE) {
      if (!(purple_blist_node_get_bool((node -> parent),"collapsed") != 0)) 
        expand = gtk_tree_model_get_path(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&parent_iter);
    }
    else if (((purple_blist_node_get_type((node -> parent))) == PURPLE_BLIST_CONTACT_NODE) && ((gtkparentnode -> contact_expanded) != 0)) {
      expand = gtk_tree_model_get_path(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&parent_iter);
    }
    if (expand != 0) {
      gtk_tree_view_expand_row(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type()))),expand,0);
      gtk_tree_path_free(expand);
    }
  }
  return (!0);
}

static gboolean pidgin_blist_group_has_show_offline_buddy(PurpleGroup *group)
{
  PurpleBlistNode *gnode;
  PurpleBlistNode *cnode;
  PurpleBlistNode *bnode;
  gnode = ((PurpleBlistNode *)group);
  for (cnode = (gnode -> child); cnode != 0; cnode = (cnode -> next)) {
    if ((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CONTACT_NODE) {
      for (bnode = (cnode -> child); bnode != 0; bnode = (bnode -> next)) {
        PurpleBuddy *buddy = (PurpleBuddy *)bnode;
        if ((purple_account_is_connected((buddy -> account)) != 0) && (purple_blist_node_get_bool(bnode,"show_offline") != 0)) 
          return (!0);
      }
    }
  }
  return 0;
}
/* This version of pidgin_blist_update_group can take the original buddy or a
 * group, but has much better algorithmic performance with a pre-known buddy.
 */

static void pidgin_blist_update_group(PurpleBuddyList *list,PurpleBlistNode *node)
{
  gint count;
  PurpleGroup *group;
  PurpleBlistNode *gnode;
  gboolean show = 0;
  gboolean show_offline = 0;
  do {
    if (node != ((PurpleBlistNode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return ;
    };
  }while (0);
  if (editing_blist != 0) 
    return ;
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) 
    gnode = node;
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) 
    gnode = ( *(node -> parent)).parent;
  else if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE)) 
    gnode = (node -> parent);
  else 
    return ;
  group = ((PurpleGroup *)gnode);
  show_offline = purple_prefs_get_bool("/pidgin/blist/show_offline_buddies");
  if (show_offline != 0) 
    count = purple_blist_get_group_size(group,0);
  else 
    count = purple_blist_get_group_online_count(group);
  if ((count > 0) || (purple_prefs_get_bool("/pidgin/blist/show_empty_groups") != 0)) 
    show = (!0);
  else 
/* Or chat? */
if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) && (buddy_is_displayable(((PurpleBuddy *)node)) != 0)) {
    show = (!0);
  }
  else if (!(show_offline != 0)) {
    show = pidgin_blist_group_has_show_offline_buddy(group);
  }
  if (show != 0) {
    gchar *title;
    gboolean biglist;
    GtkTreeIter iter;
    GtkTreePath *path;
    gboolean expanded;
    GdkColor *bgcolor = (GdkColor *)((void *)0);
    GdkPixbuf *avatar = (GdkPixbuf *)((void *)0);
    PidginBlistTheme *theme = (PidginBlistTheme *)((void *)0);
    if (!(insert_node(list,gnode,&iter) != 0)) 
      return ;
    if ((theme = pidgin_blist_get_theme()) == ((PidginBlistTheme *)((void *)0))) 
      bgcolor = ((GdkColor *)((void *)0));
    else if ((purple_blist_node_get_bool(gnode,"collapsed") != 0) || (count <= 0)) 
      bgcolor = pidgin_blist_theme_get_collapsed_background_color(theme);
    else 
      bgcolor = pidgin_blist_theme_get_expanded_background_color(theme);
    path = gtk_tree_model_get_path(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter);
    expanded = gtk_tree_view_row_expanded(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type()))),path);
    gtk_tree_path_free(path);
    title = pidgin_get_group_title(gnode,expanded);
    biglist = purple_prefs_get_bool("/pidgin/blist/show_buddy_icons");
    if (biglist != 0) {
      avatar = pidgin_blist_get_buddy_icon(gnode,(!0),(!0));
    }
    gtk_tree_store_set((gtkblist -> treemodel),&iter,STATUS_ICON_VISIBLE_COLUMN,0,STATUS_ICON_COLUMN,((void *)((void *)0)),NAME_COLUMN,title,NODE_COLUMN,gnode,BGCOLOR_COLUMN,bgcolor,GROUP_EXPANDER_COLUMN,!0,GROUP_EXPANDER_VISIBLE_COLUMN,!0,CONTACT_EXPANDER_VISIBLE_COLUMN,0,BUDDY_ICON_COLUMN,avatar,BUDDY_ICON_VISIBLE_COLUMN,biglist,IDLE_VISIBLE_COLUMN,0,EMBLEM_VISIBLE_COLUMN,0,-1);
    g_free(title);
  }
  else {
    pidgin_blist_hide_node(list,gnode,(!0));
  }
}

static char *pidgin_get_group_title(PurpleBlistNode *gnode,gboolean expanded)
{
  PurpleGroup *group;
  gboolean selected;
  char group_count[12UL] = "";
  char *mark;
  char *esc;
  PurpleBlistNode *selected_node = (PurpleBlistNode *)((void *)0);
  GtkTreeIter iter;
  PidginThemeFont *pair;
  const gchar *text_color;
  const gchar *text_font;
  PidginBlistTheme *theme;
  group = ((PurpleGroup *)gnode);
  if (gtk_tree_selection_get_selected(gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treeview)),gtk_tree_view_get_type())))),0,&iter) != 0) {
    gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&iter,NODE_COLUMN,&selected_node,-1);
  }
  selected = (gnode == selected_node);
  if (!(expanded != 0)) {
    g_snprintf(group_count,(sizeof(group_count)),"%d/%d",purple_blist_get_group_online_count(group),purple_blist_get_group_size(group,0));
  }
  theme = pidgin_blist_get_theme();
  if (theme == ((PidginBlistTheme *)((void *)0))) 
    pair = ((PidginThemeFont *)((void *)0));
  else if (expanded != 0) 
    pair = pidgin_blist_theme_get_expanded_text_info(theme);
  else 
    pair = pidgin_blist_theme_get_collapsed_text_info(theme);
  text_color = ((selected != 0)?((const char *)((void *)0)) : theme_font_get_color_default(pair,0));
  text_font = theme_font_get_face_default(pair,"");
  esc = g_markup_escape_text((group -> name),(-1));
  if (text_color != 0) {
    mark = g_strdup_printf("<span foreground=\'%s\' font_desc=\'%s\'><b>%s</b>%s%s%s</span>",text_color,text_font,((esc != 0)?esc : ""),(!(expanded != 0)?" <span weight=\'light\'>(</span>" : ""),group_count,(!(expanded != 0)?"<span weight=\'light\'>)</span>" : ""));
  }
  else {
    mark = g_strdup_printf("<span font_desc=\'%s\'><b>%s</b>%s%s%s</span>",text_font,((esc != 0)?esc : ""),(!(expanded != 0)?" <span weight=\'light\'>(</span>" : ""),group_count,(!(expanded != 0)?"<span weight=\'light\'>)</span>" : ""));
  }
  g_free(esc);
  return mark;
}

static void buddy_node(PurpleBuddy *buddy,GtkTreeIter *iter,PurpleBlistNode *node)
{
  PurplePresence *presence = purple_buddy_get_presence(buddy);
  GdkPixbuf *status;
  GdkPixbuf *avatar;
  GdkPixbuf *emblem;
  GdkPixbuf *prpl_icon;
  GdkColor *color = (GdkColor *)((void *)0);
  char *mark;
  char *idle = (char *)((void *)0);
  gboolean expanded = ( *((struct _pidgin_blist_node *)( *(node -> parent)).ui_data)).contact_expanded;
  gboolean selected = ((gtkblist -> selected_node) == node);
  gboolean biglist = purple_prefs_get_bool("/pidgin/blist/show_buddy_icons");
  PidginBlistTheme *theme;
  if (editing_blist != 0) 
    return ;
  status = pidgin_blist_get_status_icon(((PurpleBlistNode *)buddy),(((biglist != 0)?PIDGIN_STATUS_ICON_LARGE : PIDGIN_STATUS_ICON_SMALL)));
/* Speed it up if we don't want buddy icons. */
  if (biglist != 0) 
    avatar = pidgin_blist_get_buddy_icon(((PurpleBlistNode *)buddy),(!0),(!0));
  else 
    avatar = ((GdkPixbuf *)((void *)0));
  if (!(avatar != 0)) {
    g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> empty_avatar)),((GType )(20 << 2))))));
    avatar = (gtkblist -> empty_avatar);
  }
  else if (!(((buddy != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0)) || (purple_presence_is_idle(presence) != 0)) {
    do_alphashift(avatar,77);
  }
  emblem = pidgin_blist_get_emblem(((PurpleBlistNode *)buddy));
  mark = pidgin_blist_get_name_markup(buddy,selected,(!0));
  theme = pidgin_blist_get_theme();
  if (((purple_prefs_get_bool("/pidgin/blist/show_idle_time") != 0) && (purple_presence_is_idle(presence) != 0)) && !(biglist != 0)) {
    time_t idle_secs = purple_presence_get_idle_time(presence);
    if (idle_secs > 0) {
      PidginThemeFont *pair = (PidginThemeFont *)((void *)0);
      const gchar *textcolor;
      time_t t;
      int ihrs;
      int imin;
      time(&t);
      ihrs = ((t - idle_secs) / 3600);
      imin = (((t - idle_secs) / 60) % 60);
      if (selected != 0) 
        textcolor = ((const gchar *)((void *)0));
      else if ((theme != ((PidginBlistTheme *)((void *)0))) && ((pair = pidgin_blist_theme_get_idle_text_info(theme)) != ((PidginThemeFont *)((void *)0)))) 
        textcolor = pidgin_theme_font_get_color_describe(pair);
      else 
/* If no theme them default to making idle buddy names grey */
        textcolor = "dim grey";
      if (textcolor != 0) {
        idle = g_strdup_printf("<span color=\'%s\' font_desc=\'%s\'>%d:%02d</span>",textcolor,theme_font_get_face_default(pair,""),ihrs,imin);
      }
      else {
        idle = g_strdup_printf("<span font_desc=\'%s\'>%d:%02d</span>",theme_font_get_face_default(pair,""),ihrs,imin);
      }
    }
  }
  prpl_icon = pidgin_create_prpl_icon((buddy -> account),PIDGIN_PRPL_ICON_SMALL);
  if (theme != ((PidginBlistTheme *)((void *)0))) 
    color = pidgin_blist_theme_get_contact_color(theme);
  gtk_tree_store_set((gtkblist -> treemodel),iter,STATUS_ICON_COLUMN,status,STATUS_ICON_VISIBLE_COLUMN,!0,NAME_COLUMN,mark,IDLE_COLUMN,idle,IDLE_VISIBLE_COLUMN,(!(biglist != 0) && (idle != 0)),BUDDY_ICON_COLUMN,avatar,BUDDY_ICON_VISIBLE_COLUMN,biglist,EMBLEM_COLUMN,emblem,EMBLEM_VISIBLE_COLUMN,(emblem != ((GdkPixbuf *)((void *)0))),PROTOCOL_ICON_COLUMN,prpl_icon,PROTOCOL_ICON_VISIBLE_COLUMN,purple_prefs_get_bool("/pidgin/blist/show_protocol_icons"),BGCOLOR_COLUMN,color,CONTACT_EXPANDER_COLUMN,((void *)((void *)0)),CONTACT_EXPANDER_VISIBLE_COLUMN,expanded,GROUP_EXPANDER_VISIBLE_COLUMN,0,-1);
  g_free(mark);
  g_free(idle);
  if (emblem != 0) 
    g_object_unref(emblem);
  if (status != 0) 
    g_object_unref(status);
  if (avatar != 0) 
    g_object_unref(avatar);
  if (prpl_icon != 0) 
    g_object_unref(prpl_icon);
}
/* This is a variation on the original gtk_blist_update_contact. Here we
	can know in advance which buddy has changed so we can just update that */

static void pidgin_blist_update_contact(PurpleBuddyList *list,PurpleBlistNode *node)
{
  PurpleBlistNode *cnode;
  PurpleContact *contact;
  PurpleBuddy *buddy;
  gboolean biglist = purple_prefs_get_bool("/pidgin/blist/show_buddy_icons");
  struct _pidgin_blist_node *gtknode;
  if (editing_blist != 0) 
    return ;
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) 
    cnode = (node -> parent);
  else 
    cnode = node;
  do {
    if ((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CONTACT_NODE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_BLIST_NODE_IS_CONTACT(cnode)");
      return ;
    };
  }while (0);
/* First things first, update the group */
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) 
    pidgin_blist_update_group(list,node);
  else 
    pidgin_blist_update_group(list,(cnode -> parent));
  contact = ((PurpleContact *)cnode);
  buddy = purple_contact_get_priority_buddy(contact);
  if (buddy_is_displayable(buddy) != 0) {
    GtkTreeIter iter;
    if (!(insert_node(list,cnode,&iter) != 0)) 
      return ;
    gtknode = ((struct _pidgin_blist_node *)(cnode -> ui_data));
    if ((gtknode -> contact_expanded) != 0) {
      GdkPixbuf *status;
      gchar *mark;
      gchar *tmp;
      const gchar *fg_color;
      const gchar *font;
      GdkColor *color = (GdkColor *)((void *)0);
      PidginBlistTheme *theme;
      PidginThemeFont *pair;
      gboolean selected = ((gtkblist -> selected_node) == cnode);
      mark = g_markup_escape_text(purple_contact_get_alias(contact),(-1));
      theme = pidgin_blist_get_theme();
      if (theme == ((PidginBlistTheme *)((void *)0))) 
        pair = ((PidginThemeFont *)((void *)0));
      else {
        pair = pidgin_blist_theme_get_contact_text_info(theme);
        color = pidgin_blist_theme_get_contact_color(theme);
      }
      font = theme_font_get_face_default(pair,"");
      fg_color = ((selected != 0)?((const char *)((void *)0)) : theme_font_get_color_default(pair,0));
      if (fg_color != 0) {
        tmp = g_strdup_printf("<span font_desc=\'%s\' color=\'%s\'>%s</span>",font,fg_color,mark);
      }
      else {
        tmp = g_strdup_printf("<span font_desc=\'%s\'>%s</span>",font,mark);
      }
      g_free(mark);
      mark = tmp;
      status = pidgin_blist_get_status_icon(cnode,(((biglist != 0)?PIDGIN_STATUS_ICON_LARGE : PIDGIN_STATUS_ICON_SMALL)));
      gtk_tree_store_set((gtkblist -> treemodel),&iter,STATUS_ICON_COLUMN,status,STATUS_ICON_VISIBLE_COLUMN,!0,NAME_COLUMN,mark,IDLE_COLUMN,((void *)((void *)0)),IDLE_VISIBLE_COLUMN,0,BGCOLOR_COLUMN,color,BUDDY_ICON_COLUMN,((void *)((void *)0)),CONTACT_EXPANDER_COLUMN,!0,CONTACT_EXPANDER_VISIBLE_COLUMN,!0,GROUP_EXPANDER_VISIBLE_COLUMN,0,-1);
      g_free(mark);
      if (status != 0) 
        g_object_unref(status);
    }
    else {
      buddy_node(buddy,&iter,cnode);
    }
  }
  else {
    pidgin_blist_hide_node(list,cnode,(!0));
  }
}

static void pidgin_blist_update_buddy(PurpleBuddyList *list,PurpleBlistNode *node,gboolean status_change)
{
  PurpleBuddy *buddy;
  struct _pidgin_blist_node *gtkparentnode;
  do {
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_BLIST_NODE_IS_BUDDY(node)");
      return ;
    };
  }while (0);
  if ((node -> parent) == ((PurpleBlistNode *)((void *)0))) 
    return ;
  buddy = ((PurpleBuddy *)node);
/* First things first, update the contact */
  pidgin_blist_update_contact(list,node);
  gtkparentnode = ((struct _pidgin_blist_node *)( *(node -> parent)).ui_data);
  if (((gtkparentnode -> contact_expanded) != 0) && (buddy_is_displayable(buddy) != 0)) {
    GtkTreeIter iter;
    if (!(insert_node(list,node,&iter) != 0)) 
      return ;
    buddy_node(buddy,&iter,node);
  }
  else {
    pidgin_blist_hide_node(list,node,(!0));
  }
}

static void pidgin_blist_update_chat(PurpleBuddyList *list,PurpleBlistNode *node)
{
  PurpleChat *chat;
  do {
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_BLIST_NODE_IS_CHAT(node)");
      return ;
    };
  }while (0);
  if (editing_blist != 0) 
    return ;
/* First things first, update the group */
  pidgin_blist_update_group(list,(node -> parent));
  chat = ((PurpleChat *)node);
  if (purple_account_is_connected((chat -> account)) != 0) {
    GtkTreeIter iter;
    GdkPixbuf *status;
    GdkPixbuf *avatar;
    GdkPixbuf *emblem;
    GdkPixbuf *prpl_icon;
    const gchar *color;
    const gchar *font;
    gchar *mark;
    gchar *tmp;
    gboolean showicons = purple_prefs_get_bool("/pidgin/blist/show_buddy_icons");
    gboolean biglist = purple_prefs_get_bool("/pidgin/blist/show_buddy_icons");
    PidginBlistNode *ui;
    PurpleConversation *conv;
    gboolean hidden = 0;
    GdkColor *bgcolor = (GdkColor *)((void *)0);
    PidginThemeFont *pair;
    PidginBlistTheme *theme;
    gboolean selected = ((gtkblist -> selected_node) == node);
    gboolean nick_said = 0;
    if (!(insert_node(list,node,&iter) != 0)) 
      return ;
    ui = (node -> ui_data);
    conv = ui -> conv.conv;
    if ((conv != 0) && (pidgin_conv_is_hidden(((PidginConversation *)(conv -> ui_data))) != 0)) {
      hidden = (ui -> conv.flags & PIDGIN_BLIST_NODE_HAS_PENDING_MESSAGE);
      nick_said = (ui -> conv.flags & PIDGIN_BLIST_CHAT_HAS_PENDING_MESSAGE_WITH_NICK);
    }
    status = pidgin_blist_get_status_icon(node,(((biglist != 0)?PIDGIN_STATUS_ICON_LARGE : PIDGIN_STATUS_ICON_SMALL)));
    emblem = pidgin_blist_get_emblem(node);
/* Speed it up if we don't want buddy icons. */
    if (showicons != 0) 
      avatar = pidgin_blist_get_buddy_icon(node,(!0),0);
    else 
      avatar = ((GdkPixbuf *)((void *)0));
    mark = g_markup_escape_text(purple_chat_get_name(chat),(-1));
    theme = pidgin_blist_get_theme();
    if (theme == ((PidginBlistTheme *)((void *)0))) 
      pair = ((PidginThemeFont *)((void *)0));
    else if (nick_said != 0) 
      pair = pidgin_blist_theme_get_unread_message_nick_said_text_info(theme);
    else if (hidden != 0) 
      pair = pidgin_blist_theme_get_unread_message_text_info(theme);
    else 
      pair = pidgin_blist_theme_get_online_text_info(theme);
    font = theme_font_get_face_default(pair,"");
    if ((selected != 0) || !((color = theme_font_get_color_default(pair,0)) != 0)) 
/* nick_said color is the same as gtkconv:tab-label-attention */
      color = (((nick_said != 0)?"#006aff" : ((char *)((void *)0))));
    if (color != 0) {
      tmp = g_strdup_printf("<span font_desc=\'%s\' color=\'%s\' weight=\'%s\'>%s</span>",font,color,((hidden != 0)?"bold" : "normal"),mark);
    }
    else {
      tmp = g_strdup_printf("<span font_desc=\'%s\' weight=\'%s\'>%s</span>",font,((hidden != 0)?"bold" : "normal"),mark);
    }
    g_free(mark);
    mark = tmp;
    prpl_icon = pidgin_create_prpl_icon((chat -> account),PIDGIN_PRPL_ICON_SMALL);
    if (theme != ((PidginBlistTheme *)((void *)0))) 
      bgcolor = pidgin_blist_theme_get_contact_color(theme);
    gtk_tree_store_set((gtkblist -> treemodel),&iter,STATUS_ICON_COLUMN,status,STATUS_ICON_VISIBLE_COLUMN,!0,BUDDY_ICON_COLUMN,((avatar != 0)?avatar : (gtkblist -> empty_avatar)),BUDDY_ICON_VISIBLE_COLUMN,showicons,EMBLEM_COLUMN,emblem,EMBLEM_VISIBLE_COLUMN,(emblem != ((GdkPixbuf *)((void *)0))),PROTOCOL_ICON_COLUMN,prpl_icon,PROTOCOL_ICON_VISIBLE_COLUMN,purple_prefs_get_bool("/pidgin/blist/show_protocol_icons"),NAME_COLUMN,mark,BGCOLOR_COLUMN,bgcolor,GROUP_EXPANDER_VISIBLE_COLUMN,0,-1);
    g_free(mark);
    if (emblem != 0) 
      g_object_unref(emblem);
    if (status != 0) 
      g_object_unref(status);
    if (avatar != 0) 
      g_object_unref(avatar);
    if (prpl_icon != 0) 
      g_object_unref(prpl_icon);
  }
  else {
    pidgin_blist_hide_node(list,node,(!0));
  }
}

static void pidgin_blist_update(PurpleBuddyList *list,PurpleBlistNode *node)
{
  if (list != 0) 
    gtkblist = ((PidginBuddyList *)(purple_blist_get_ui_data()));
  if ((!(gtkblist != 0) || !((gtkblist -> treeview) != 0)) || !(node != 0)) 
    return ;
  if ((node -> ui_data) == ((void *)((void *)0))) 
    pidgin_blist_new_node(node);
  switch(node -> type){
    case PURPLE_BLIST_GROUP_NODE:
{
      pidgin_blist_update_group(list,node);
      break; 
    }
    case PURPLE_BLIST_CONTACT_NODE:
{
      pidgin_blist_update_contact(list,node);
      break; 
    }
    case PURPLE_BLIST_BUDDY_NODE:
{
      pidgin_blist_update_buddy(list,node,(!0));
      break; 
    }
    case PURPLE_BLIST_CHAT_NODE:
{
      pidgin_blist_update_chat(list,node);
      break; 
    }
    case PURPLE_BLIST_OTHER_NODE:
{
      return ;
    }
  }
}

static void pidgin_blist_destroy(PurpleBuddyList *list)
{
  PidginBuddyListPrivate *priv;
  if (!(list != 0) || !((list -> ui_data) != 0)) 
    return ;
  do {
    if ((list -> ui_data) == gtkblist) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"list->ui_data == gtkblist");
      return ;
    };
  }while (0);
  purple_signals_disconnect_by_handle(gtkblist);
  if ((gtkblist -> headline_close) != 0) 
    gdk_pixbuf_unref((gtkblist -> headline_close));
  gtk_widget_destroy((gtkblist -> window));
  pidgin_blist_tooltip_destroy();
  if ((gtkblist -> refresh_timer) != 0U) 
    purple_timeout_remove((gtkblist -> refresh_timer));
  if ((gtkblist -> timeout) != 0U) 
    g_source_remove((gtkblist -> timeout));
  if ((gtkblist -> drag_timeout) != 0U) 
    g_source_remove((gtkblist -> drag_timeout));
  g_hash_table_destroy((gtkblist -> connection_errors));
  gtkblist -> refresh_timer = 0;
  gtkblist -> timeout = 0;
  gtkblist -> drag_timeout = 0;
  gtkblist -> window = (gtkblist -> vbox = (gtkblist -> treeview = ((GtkWidget *)((void *)0))));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),((GType )(20 << 2))))));
  gtkblist -> treemodel = ((GtkTreeStore *)((void *)0));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> ift)),((GType )(20 << 2))))));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> empty_avatar)),((GType )(20 << 2))))));
  gdk_cursor_unref((gtkblist -> hand_cursor));
  gdk_cursor_unref((gtkblist -> arrow_cursor));
  gtkblist -> hand_cursor = ((GdkCursor *)((void *)0));
  gtkblist -> arrow_cursor = ((GdkCursor *)((void *)0));
  priv = ((PidginBuddyListPrivate *)(gtkblist -> priv));
  if ((priv -> current_theme) != 0) 
    g_object_unref((priv -> current_theme));
  g_free(priv);
  g_free(gtkblist);
  accountmenu = ((GtkWidget *)((void *)0));
  gtkblist = ((PidginBuddyList *)((void *)0));
  purple_prefs_disconnect_by_handle(pidgin_blist_get_handle());
}

static void pidgin_blist_set_visible(PurpleBuddyList *list,gboolean show)
{
  if (!((gtkblist != 0) && ((gtkblist -> window) != 0))) 
    return ;
  if (show != 0) {
    if (!(((gdk_window_get_state(( *((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_widget_get_type())))).window)) & GDK_WINDOW_STATE_ICONIFIED) != 0U) && !((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_object_get_type())))).flags & GTK_VISIBLE) != 0)) 
      purple_signal_emit(pidgin_blist_get_handle(),"gtkblist-unhiding",gtkblist);
    pidgin_blist_restore_position();
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_window_get_type()))));
  }
  else {
    if (visibility_manager_count != 0U) {
      purple_signal_emit(pidgin_blist_get_handle(),"gtkblist-hiding",gtkblist);
      gtk_widget_hide((gtkblist -> window));
    }
    else {
      if (!((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_object_get_type())))).flags & GTK_VISIBLE) != 0)) 
        gtk_widget_show((gtkblist -> window));
      gtk_window_iconify(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_window_get_type()))));
    }
  }
}

static GList *groups_tree()
{
  static GList *list = (GList *)((void *)0);
  char *tmp2;
  PurpleGroup *g;
  PurpleBlistNode *gnode;
  g_list_free(list);
  list = ((GList *)((void *)0));
  if (( *purple_get_blist()).root == ((PurpleBlistNode *)((void *)0))) {
    list = g_list_append(list,((gpointer )((const char *)(dgettext("pidgin","Buddies")))));
  }
  else {
    for (gnode = ( *purple_get_blist()).root; gnode != ((PurpleBlistNode *)((void *)0)); gnode = (gnode -> next)) {
      if ((purple_blist_node_get_type(gnode)) == PURPLE_BLIST_GROUP_NODE) {
        g = ((PurpleGroup *)gnode);
        tmp2 = (g -> name);
        list = g_list_append(list,tmp2);
      }
    }
  }
  return list;
}

static void add_buddy_select_account_cb(GObject *w,PurpleAccount *account,PidginAddBuddyData *data)
{
  PurpleConnection *pc = (PurpleConnection *)((void *)0);
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  gboolean invite_enabled = (!0);
/* Save our account */
  data -> rq_data.account = account;
  if (account != 0) 
    pc = purple_account_get_connection(account);
  if (pc != 0) 
    prpl = purple_connection_get_prpl(pc);
  if (prpl != 0) 
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if ((prpl_info != 0) && !(((prpl_info -> options) & OPT_PROTO_INVITE_MESSAGE) != 0U)) 
    invite_enabled = 0;
  gtk_widget_set_sensitive((data -> entry_for_invite),invite_enabled);
}

static void destroy_add_buddy_dialog_cb(GtkWidget *win,PidginAddBuddyData *data)
{
  g_free(data);
}

static void add_buddy_cb(GtkWidget *w,int resp,PidginAddBuddyData *data)
{
  const char *grp;
  const char *who;
  const char *whoalias;
  const char *invite;
  PurpleAccount *account;
  PurpleGroup *g;
  PurpleBuddy *b;
  PurpleConversation *c;
  PurpleBuddyIcon *icon;
  if (resp == GTK_RESPONSE_OK) {
    who = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(data -> entry)),gtk_entry_get_type()))));
    grp = pidgin_text_combo_box_entry_get_text((data -> combo));
    whoalias = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(data -> entry_for_alias)),gtk_entry_get_type()))));
    if (( *whoalias) == 0) 
      whoalias = ((const char *)((void *)0));
    invite = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(data -> entry_for_invite)),gtk_entry_get_type()))));
    if (( *invite) == 0) 
      invite = ((const char *)((void *)0));
    account = data -> rq_data.account;
    g = ((PurpleGroup *)((void *)0));
    if ((grp != ((const char *)((void *)0))) && (( *grp) != 0)) {
      if ((g = purple_find_group(grp)) == ((PurpleGroup *)((void *)0))) {
        g = purple_group_new(grp);
        purple_blist_add_group(g,0);
      }
      b = purple_find_buddy_in_group(account,who,g);
    }
    else if ((b = purple_find_buddy(account,who)) != ((PurpleBuddy *)((void *)0))) {
      g = purple_buddy_get_group(b);
    }
    if (b == ((PurpleBuddy *)((void *)0))) {
      b = purple_buddy_new(account,who,whoalias);
      purple_blist_add_buddy(b,0,g,0);
    }
    purple_account_add_buddy_with_invite(account,b,invite);
/* Offer to merge people with the same alias. */
    if ((whoalias != ((const char *)((void *)0))) && (g != ((PurpleGroup *)((void *)0)))) 
      gtk_blist_auto_personize(((PurpleBlistNode *)g),whoalias);
/*
		 * XXX
		 * It really seems like it would be better if the call to
		 * purple_account_add_buddy() and purple_conversation_update() were done in
		 * blist.c, possibly in the purple_blist_add_buddy() function.  Maybe
		 * purple_account_add_buddy() should be renamed to
		 * purple_blist_add_new_buddy() or something, and have it call
		 * purple_blist_add_buddy() after it creates it.  --Mark
		 *
		 * No that's not good.  blist.c should only deal with adding nodes to the
		 * local list.  We need a new, non-gtk file that calls both
		 * purple_account_add_buddy and purple_blist_add_buddy().
		 * Or something.  --Mark
		 */
    c = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,who,data -> rq_data.account);
    if (c != ((PurpleConversation *)((void *)0))) {
      icon = purple_conv_im_get_icon((purple_conversation_get_im_data(c)));
      if (icon != ((PurpleBuddyIcon *)((void *)0))) 
        purple_buddy_icon_update(icon);
    }
  }
  gtk_widget_destroy(data -> rq_data.window);
}

static void pidgin_blist_request_add_buddy(PurpleAccount *account,const char *username,const char *group,const char *alias)
{
  PidginAddBuddyData *data = (PidginAddBuddyData *)(g_malloc0_n(1,(sizeof(PidginAddBuddyData ))));
  if (account == ((PurpleAccount *)((void *)0))) 
    account = purple_connection_get_account(( *purple_connections_get_all()).data);
  make_blist_request_dialog(((PidginBlistRequestData *)data),account,((const char *)(dgettext("pidgin","Add Buddy"))),"add_buddy",((const char *)(dgettext("pidgin","Add a buddy.\n"))),((GCallback )add_buddy_select_account_cb),0,((GCallback )add_buddy_cb));
  gtk_dialog_add_buttons(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)data -> rq_data.window),gtk_dialog_get_type()))),"gtk-cancel",GTK_RESPONSE_CANCEL,"gtk-add",GTK_RESPONSE_OK,((void *)((void *)0)));
  gtk_dialog_set_default_response(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)data -> rq_data.window),gtk_dialog_get_type()))),GTK_RESPONSE_OK);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)data -> rq_data.window),((GType )(20 << 2))))),"destroy",((GCallback )destroy_add_buddy_dialog_cb),data,0,((GConnectFlags )0));
  data -> entry = gtk_entry_new();
  pidgin_add_widget_to_vbox(data -> rq_data.vbox,((const char *)(dgettext("pidgin","Buddy\'s _username:"))),data -> rq_data.sg,(data -> entry),(!0),0);
  gtk_widget_grab_focus((data -> entry));
  if (username != ((const char *)((void *)0))) 
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(data -> entry)),gtk_entry_get_type()))),username);
  else 
    gtk_dialog_set_response_sensitive(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)data -> rq_data.window),gtk_dialog_get_type()))),GTK_RESPONSE_OK,0);
  gtk_entry_set_activates_default(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(data -> entry)),gtk_entry_get_type()))),(!0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(data -> entry)),((GType )(20 << 2))))),"changed",((GCallback )pidgin_set_sensitive_if_input),data -> rq_data.window,0,((GConnectFlags )0));
  data -> entry_for_alias = gtk_entry_new();
  pidgin_add_widget_to_vbox(data -> rq_data.vbox,((const char *)(dgettext("pidgin","(Optional) A_lias:"))),data -> rq_data.sg,(data -> entry_for_alias),(!0),0);
  if (alias != ((const char *)((void *)0))) 
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(data -> entry_for_alias)),gtk_entry_get_type()))),alias);
  if (username != ((const char *)((void *)0))) 
    gtk_widget_grab_focus(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(data -> entry_for_alias)),gtk_widget_get_type()))));
  data -> entry_for_invite = gtk_entry_new();
  pidgin_add_widget_to_vbox(data -> rq_data.vbox,((const char *)(dgettext("pidgin","(Optional) _Invite message:"))),data -> rq_data.sg,(data -> entry_for_invite),(!0),0);
  data -> combo = pidgin_text_combo_box_entry_new(group,groups_tree());
  pidgin_add_widget_to_vbox(data -> rq_data.vbox,((const char *)(dgettext("pidgin","Add buddy to _group:"))),data -> rq_data.sg,(data -> combo),(!0),0);
  gtk_widget_show_all(data -> rq_data.window);
/* Force update of invite message entry sensitivity */
  add_buddy_select_account_cb(0,account,data);
}

static void add_chat_cb(GtkWidget *w,PidginAddChatData *data)
{
  GList *tmp;
  PurpleChat *chat;
  GHashTable *components;
  components = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
  for (tmp = data -> chat_data.entries; tmp != 0; tmp = (tmp -> next)) {
    if (g_object_get_data((tmp -> data),"is_spin") != 0) {
      g_hash_table_replace(components,(g_strdup((g_object_get_data((tmp -> data),"identifier")))),(g_strdup_printf("%d",gtk_spin_button_get_value_as_int((tmp -> data)))));
    }
    else {
      const char *value = gtk_entry_get_text((tmp -> data));
      if (( *value) != 0) 
        g_hash_table_replace(components,(g_strdup((g_object_get_data((tmp -> data),"identifier")))),(g_strdup(value)));
    }
  }
  chat = purple_chat_new(data -> chat_data.rq_data.account,gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(data -> alias_entry)),gtk_entry_get_type())))),components);
  if (chat != ((PurpleChat *)((void *)0))) {
    PurpleGroup *group;
    const char *group_name;
    group_name = pidgin_text_combo_box_entry_get_text((data -> group_combo));
    group = ((PurpleGroup *)((void *)0));
    if (((group_name != ((const char *)((void *)0))) && (( *group_name) != 0)) && ((group = purple_find_group(group_name)) == ((PurpleGroup *)((void *)0)))) {
      group = purple_group_new(group_name);
      purple_blist_add_group(group,0);
    }
    purple_blist_add_chat(chat,group,0);
    if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(data -> autojoin)),gtk_toggle_button_get_type())))) != 0) 
      purple_blist_node_set_bool(((PurpleBlistNode *)chat),"gtk-autojoin",(!0));
    if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(data -> persistent)),gtk_toggle_button_get_type())))) != 0) 
      purple_blist_node_set_bool(((PurpleBlistNode *)chat),"gtk-persistent",(!0));
  }
  gtk_widget_destroy(data -> chat_data.rq_data.window);
  g_free(data -> chat_data.default_chat_name);
  g_list_free(data -> chat_data.entries);
  g_free(data);
}

static void add_chat_resp_cb(GtkWidget *w,int resp,PidginAddChatData *data)
{
  if (resp == GTK_RESPONSE_OK) {
    add_chat_cb(0,data);
  }
  else if (resp == 1) {
    pidgin_roomlist_dialog_show_with_account(data -> chat_data.rq_data.account);
  }
  else {
    gtk_widget_destroy(data -> chat_data.rq_data.window);
    g_free(data -> chat_data.default_chat_name);
    g_list_free(data -> chat_data.entries);
    g_free(data);
  }
}

static void pidgin_blist_request_add_chat(PurpleAccount *account,PurpleGroup *group,const char *alias,const char *name)
{
  PidginAddChatData *data;
  GList *l;
  PurpleConnection *gc;
  GtkBox *vbox;
  if (account != ((PurpleAccount *)((void *)0))) {
    gc = purple_account_get_connection(account);
    if (( *((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info)).join_chat == ((void (*)(PurpleConnection *, GHashTable *))((void *)0))) {
      purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","This protocol does not support chat rooms."))),0,0,0);
      return ;
    }
  }
  else {{
/* Find an account with chat capabilities */
      for (l = purple_connections_get_all(); l != ((GList *)((void *)0)); l = (l -> next)) {
        gc = ((PurpleConnection *)(l -> data));
        if (( *((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info)).join_chat != ((void (*)(PurpleConnection *, GHashTable *))((void *)0))) {
          account = purple_connection_get_account(gc);
          break; 
        }
      }
    }
    if (account == ((PurpleAccount *)((void *)0))) {
      purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","You are not currently signed on with any protocols that have the ability to chat."))),0,0,0);
      return ;
    }
  }
  data = ((PidginAddChatData *)(g_malloc0_n(1,(sizeof(PidginAddChatData )))));
  vbox = ((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(make_blist_request_dialog(((PidginBlistRequestData *)data),account,((const char *)(dgettext("pidgin","Add Chat"))),"add_chat",((const char *)(dgettext("pidgin","Please enter an alias, and the appropriate information about the chat you would like to add to your buddy list.\n"))),((GCallback )chat_select_account_cb),chat_account_filter_func,((GCallback )add_chat_resp_cb)))),gtk_box_get_type())));
  gtk_dialog_add_buttons(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)data -> chat_data.rq_data.window),gtk_dialog_get_type()))),((const char *)(dgettext("pidgin","Room List"))),1,"gtk-cancel",GTK_RESPONSE_CANCEL,"gtk-add",GTK_RESPONSE_OK,((void *)((void *)0)));
  gtk_dialog_set_default_response(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)data -> chat_data.rq_data.window),gtk_dialog_get_type()))),GTK_RESPONSE_OK);
  data -> chat_data.default_chat_name = g_strdup(name);
  rebuild_chat_entries(((PidginChatData *)data),name);
  data -> alias_entry = gtk_entry_new();
  if (alias != ((const char *)((void *)0))) 
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(data -> alias_entry)),gtk_entry_get_type()))),alias);
  gtk_entry_set_activates_default(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(data -> alias_entry)),gtk_entry_get_type()))),(!0));
  pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),((const char *)(dgettext("pidgin","A_lias:"))),data -> chat_data.rq_data.sg,(data -> alias_entry),(!0),0);
  if (name != ((const char *)((void *)0))) 
    gtk_widget_grab_focus((data -> alias_entry));
  data -> group_combo = pidgin_text_combo_box_entry_new((((group != 0)?(group -> name) : ((char *)((void *)0)))),groups_tree());
  pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),((const char *)(dgettext("pidgin","_Group:"))),data -> chat_data.rq_data.sg,(data -> group_combo),(!0),0);
  data -> autojoin = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Auto_join when account connects."))));
  data -> persistent = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","_Remain in chat after window is closed."))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),(data -> autojoin),0,0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),(data -> persistent),0,0,0);
  gtk_widget_show_all(data -> chat_data.rq_data.window);
}

static void add_group_cb(PurpleConnection *gc,const char *group_name)
{
  PurpleGroup *group;
  if ((group_name == ((const char *)((void *)0))) || (( *group_name) == 0)) 
    return ;
  group = purple_group_new(group_name);
  purple_blist_add_group(group,0);
}

static void pidgin_blist_request_add_group()
{
  purple_request_input(0,((const char *)(dgettext("pidgin","Add Group"))),0,((const char *)(dgettext("pidgin","Please enter the name of the group to be added."))),0,0,0,0,((const char *)(dgettext("pidgin","Add"))),((GCallback )add_group_cb),((const char *)(dgettext("pidgin","Cancel"))),0,0,0,0,0);
}

void pidgin_blist_toggle_visibility()
{
  if ((gtkblist != 0) && ((gtkblist -> window) != 0)) {
    if ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_object_get_type())))).flags & GTK_VISIBLE) != 0) {
/* make the buddy list visible if it is iconified or if it is
			 * obscured and not currently focused (the focus part ensures
			 * that we do something reasonable if the buddy list is obscured
			 * by a window set to always be on top), otherwise hide the
			 * buddy list
			 */
      purple_blist_set_visible(((((gdk_window_get_state(( *((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_widget_get_type())))).window)) & GDK_WINDOW_STATE_ICONIFIED) != 0U) || ((gtk_blist_visibility != GDK_VISIBILITY_UNOBSCURED) && !(gtk_blist_focused != 0))));
    }
    else {
      purple_blist_set_visible((!0));
    }
  }
}

void pidgin_blist_visibility_manager_add()
{
  visibility_manager_count++;
  purple_debug_info("gtkblist","added visibility manager: %d\n",visibility_manager_count);
}

void pidgin_blist_visibility_manager_remove()
{
  if (visibility_manager_count != 0U) 
    visibility_manager_count--;
  if (!(visibility_manager_count != 0U)) 
    purple_blist_set_visible((!0));
  purple_debug_info("gtkblist","removed visibility manager: %d\n",visibility_manager_count);
}

void pidgin_blist_add_alert(GtkWidget *widget)
{
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> scrollbook)),gtk_container_get_type()))),widget);
  set_urgent();
}

void pidgin_blist_set_headline(const char *text,GdkPixbuf *pixbuf,GCallback callback,gpointer user_data,GDestroyNotify destroy)
{
/* Destroy any existing headline first */
  if ((gtkblist -> headline_destroy) != 0) 
    ( *(gtkblist -> headline_destroy))((gtkblist -> headline_data));
  gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> headline_label)),gtk_label_get_type()))),text);
  gtk_image_set_from_pixbuf(((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> headline_image)),gtk_image_get_type()))),pixbuf);
  gtkblist -> headline_callback = callback;
  gtkblist -> headline_data = user_data;
  gtkblist -> headline_destroy = destroy;
  if ((text != ((const char *)((void *)0))) || (pixbuf != ((GdkPixbuf *)((void *)0)))) {
    set_urgent();
    gtk_widget_show_all((gtkblist -> headline_hbox));
  }
  else {
    gtk_widget_hide((gtkblist -> headline_hbox));
  }
}

static void set_urgent()
{
  if (((gtkblist -> window) != 0) && !((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_object_get_type())))).flags & GTK_HAS_FOCUS) != 0)) 
    pidgin_set_urgent(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> window)),gtk_window_get_type()))),(!0));
}
static PurpleBlistUiOps blist_ui_ops = {(pidgin_blist_new_list), (pidgin_blist_new_node), (pidgin_blist_show), (pidgin_blist_update), (pidgin_blist_remove), (pidgin_blist_destroy), (pidgin_blist_set_visible), (pidgin_blist_request_add_buddy), (pidgin_blist_request_add_chat), (pidgin_blist_request_add_group), ((void (*)(PurpleBlistNode *))((void *)0)), ((void (*)(PurpleBlistNode *))((void *)0)), ((void (*)(PurpleAccount *))((void *)0)), ((void (*)())((void *)0))};

PurpleBlistUiOps *pidgin_blist_get_ui_ops()
{
  return &blist_ui_ops;
}

PidginBuddyList *pidgin_blist_get_default_gtk_blist()
{
  return gtkblist;
}

static gboolean autojoin_cb(PurpleConnection *gc,gpointer data)
{
  PurpleAccount *account = purple_connection_get_account(gc);
  PurpleBlistNode *gnode;
  PurpleBlistNode *cnode;
  for (gnode = ( *purple_get_blist()).root; gnode != 0; gnode = (gnode -> next)) {
    if (!((purple_blist_node_get_type(gnode)) == PURPLE_BLIST_GROUP_NODE)) 
      continue; 
    for (cnode = (gnode -> child); cnode != 0; cnode = (cnode -> next)) {{
        PurpleChat *chat;
        if (!((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CHAT_NODE)) 
          continue; 
        chat = ((PurpleChat *)cnode);
        if ((chat -> account) != account) 
          continue; 
        if (purple_blist_node_get_bool(((PurpleBlistNode *)chat),"gtk-autojoin") != 0) 
          serv_join_chat(gc,(chat -> components));
      }
    }
  }
/* Stop processing; we handled the autojoins. */
  return (!0);
}

void *pidgin_blist_get_handle()
{
  static int handle;
  return (&handle);
}

static gboolean buddy_signonoff_timeout_cb(PurpleBuddy *buddy)
{
  struct _pidgin_blist_node *gtknode = ( *((PurpleBlistNode *)buddy)).ui_data;
  gtknode -> recent_signonoff = 0;
  gtknode -> recent_signonoff_timer = 0;
  pidgin_blist_update(0,((PurpleBlistNode *)buddy));
  return 0;
}

static void buddy_signonoff_cb(PurpleBuddy *buddy)
{
  struct _pidgin_blist_node *gtknode;
  if (!(( *((PurpleBlistNode *)buddy)).ui_data != 0)) {
    pidgin_blist_new_node(((PurpleBlistNode *)buddy));
  }
  gtknode = ( *((PurpleBlistNode *)buddy)).ui_data;
  gtknode -> recent_signonoff = (!0);
  if ((gtknode -> recent_signonoff_timer) > 0) 
    purple_timeout_remove((gtknode -> recent_signonoff_timer));
  gtknode -> recent_signonoff_timer = (purple_timeout_add_seconds(10,((GSourceFunc )buddy_signonoff_timeout_cb),buddy));
}

void pidgin_blist_set_theme(PidginBlistTheme *theme)
{
  PidginBuddyListPrivate *priv = (PidginBuddyListPrivate *)(gtkblist -> priv);
  PurpleBuddyList *list = purple_get_blist();
  if (theme != ((PidginBlistTheme *)((void *)0))) 
    purple_prefs_set_string("/pidgin/blist/theme",purple_theme_get_name(((PurpleTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),purple_theme_get_type())))));
  else 
    purple_prefs_set_string("/pidgin/blist/theme","");
  if ((priv -> current_theme) != 0) 
    g_object_unref((priv -> current_theme));
  priv -> current_theme = (((theme != 0)?g_object_ref(theme) : ((void *)((void *)0))));
  pidgin_blist_build_layout(list);
  pidgin_blist_refresh(list);
}

PidginBlistTheme *pidgin_blist_get_theme()
{
  PidginBuddyListPrivate *priv = (PidginBuddyListPrivate *)(gtkblist -> priv);
  return priv -> current_theme;
}

void pidgin_blist_init()
{
  void *gtk_blist_handle = pidgin_blist_get_handle();
  cached_emblems = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,0);
/* Initialize prefs */
  purple_prefs_add_none("/pidgin/blist");
  purple_prefs_add_bool("/pidgin/blist/show_buddy_icons",(!0));
  purple_prefs_add_bool("/pidgin/blist/show_empty_groups",0);
  purple_prefs_add_bool("/pidgin/blist/show_idle_time",(!0));
  purple_prefs_add_bool("/pidgin/blist/show_offline_buddies",0);
  purple_prefs_add_bool("/pidgin/blist/show_protocol_icons",0);
  purple_prefs_add_bool("/pidgin/blist/list_visible",0);
  purple_prefs_add_bool("/pidgin/blist/list_maximized",0);
  purple_prefs_add_string("/pidgin/blist/sort_type","alphabetical");
  purple_prefs_add_int("/pidgin/blist/x",0);
  purple_prefs_add_int("/pidgin/blist/y",0);
/* Golden ratio, baby */
  purple_prefs_add_int("/pidgin/blist/width",250);
/* Golden ratio, baby */
  purple_prefs_add_int("/pidgin/blist/height",405);
#if !GTK_CHECK_VERSION(2,14,0)
/* This pref is used in pidgintooltip.c. */
#endif
  purple_prefs_add_string("/pidgin/blist/theme","");
  purple_theme_manager_register_type((g_object_new(pidgin_blist_theme_loader_get_type(),"type","blist",((void *)((void *)0)))));
/* Register our signals */
  purple_signal_register(gtk_blist_handle,"gtkblist-hiding",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_BLIST));
  purple_signal_register(gtk_blist_handle,"gtkblist-unhiding",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_BLIST));
  purple_signal_register(gtk_blist_handle,"gtkblist-created",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_BLIST));
  purple_signal_register(gtk_blist_handle,"drawing-tooltip",purple_marshal_VOID__POINTER_POINTER_UINT,0,3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_BLIST_NODE),purple_value_new_outgoing(PURPLE_TYPE_BOXED,"GString *"),purple_value_new(PURPLE_TYPE_BOOLEAN));
  purple_signal_register(gtk_blist_handle,"drawing-buddy",purple_marshal_POINTER__POINTER,purple_value_new(PURPLE_TYPE_STRING),1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_BLIST_BUDDY));
  purple_signal_connect(purple_blist_get_handle(),"buddy-signed-on",gtk_blist_handle,((PurpleCallback )buddy_signonoff_cb),0);
  purple_signal_connect(purple_blist_get_handle(),"buddy-signed-off",gtk_blist_handle,((PurpleCallback )buddy_signonoff_cb),0);
  purple_signal_connect(purple_blist_get_handle(),"buddy-privacy-changed",gtk_blist_handle,((PurpleCallback )pidgin_blist_update_privacy_cb),0);
  purple_signal_connect_priority(purple_connections_get_handle(),"autojoin",gtk_blist_handle,((PurpleCallback )autojoin_cb),0,9999);
}

void pidgin_blist_uninit()
{
  g_hash_table_destroy(cached_emblems);
  purple_signals_unregister_by_instance(pidgin_blist_get_handle());
  purple_signals_disconnect_by_handle(pidgin_blist_get_handle());
}
/*********************************************************************
 * Buddy List sorting functions                                      *
 *********************************************************************/

GList *pidgin_blist_get_sort_methods()
{
  return pidgin_blist_sort_methods;
}

void pidgin_blist_sort_method_reg(const char *id,const char *name,pidgin_blist_sort_function func)
{
  struct pidgin_blist_sort_method *method;
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return ;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return ;
    };
  }while (0);
  do {
    if (func != ((void (*)(PurpleBlistNode *, PurpleBuddyList *, GtkTreeIter , GtkTreeIter *, GtkTreeIter *))((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"func != NULL");
      return ;
    };
  }while (0);
  method = ((struct pidgin_blist_sort_method *)(g_malloc0_n(1,(sizeof(struct pidgin_blist_sort_method )))));
  method -> id = g_strdup(id);
  method -> name = g_strdup(name);
  method -> func = func;
  pidgin_blist_sort_methods = g_list_append(pidgin_blist_sort_methods,method);
  pidgin_blist_update_sort_methods();
}

void pidgin_blist_sort_method_unreg(const char *id)
{
  GList *l = pidgin_blist_sort_methods;
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return ;
    };
  }while (0);
{
    while(l != 0){
      struct pidgin_blist_sort_method *method = (l -> data);
      if (!(strcmp((method -> id),id) != 0)) {
        pidgin_blist_sort_methods = g_list_delete_link(pidgin_blist_sort_methods,l);
        g_free((method -> id));
        g_free((method -> name));
        g_free(method);
        break; 
      }
      l = (l -> next);
    }
  }
  pidgin_blist_update_sort_methods();
}

void pidgin_blist_sort_method_set(const char *id)
{
  GList *l = pidgin_blist_sort_methods;
  if (!(id != 0)) 
    id = "none";
  while((l != 0) && (strcmp(( *((struct pidgin_blist_sort_method *)(l -> data))).id,id) != 0))
    l = (l -> next);
  if (l != 0) {
    current_sort_method = (l -> data);
  }
  else if (!(current_sort_method != 0)) {
    pidgin_blist_sort_method_set("none");
    return ;
  }
  if (!(strcmp(id,"none") != 0)) {
    redo_buddy_list(purple_get_blist(),(!0),0);
  }
  else {
    redo_buddy_list(purple_get_blist(),0,0);
  }
}
/******************************************
 ** Sort Methods
 ******************************************/

static void sort_method_none(PurpleBlistNode *node,PurpleBuddyList *blist,GtkTreeIter parent_iter,GtkTreeIter *cur,GtkTreeIter *iter)
{
  PurpleBlistNode *sibling = (node -> prev);
  GtkTreeIter sibling_iter;
  if (cur != ((GtkTreeIter *)((void *)0))) {
     *iter =  *cur;
    return ;
  }
  while((sibling != 0) && !(get_iter_from_node(sibling,&sibling_iter) != 0)){
    sibling = (sibling -> prev);
  }
  gtk_tree_store_insert_after((gtkblist -> treemodel),iter,(((node -> parent) != 0)?&parent_iter : ((struct _GtkTreeIter *)((void *)0))),((sibling != 0)?&sibling_iter : ((struct _GtkTreeIter *)((void *)0))));
}

static void sort_method_alphabetical(PurpleBlistNode *node,PurpleBuddyList *blist,GtkTreeIter groupiter,GtkTreeIter *cur,GtkTreeIter *iter)
{
  GtkTreeIter more_z;
  const char *my_name;
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    my_name = purple_contact_get_alias(((PurpleContact *)node));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
    my_name = purple_chat_get_name(((PurpleChat *)node));
  }
  else {
    sort_method_none(node,blist,groupiter,cur,iter);
    return ;
  }
  if (!(gtk_tree_model_iter_children(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&more_z,&groupiter) != 0)) {
    gtk_tree_store_insert((gtkblist -> treemodel),iter,&groupiter,0);
    return ;
  }
  do {
    PurpleBlistNode *n;
    const char *this_name;
    int cmp;
    gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&more_z,NODE_COLUMN,&n,-1);
    if ((purple_blist_node_get_type(n)) == PURPLE_BLIST_CONTACT_NODE) {
      this_name = purple_contact_get_alias(((PurpleContact *)n));
    }
    else if ((purple_blist_node_get_type(n)) == PURPLE_BLIST_CHAT_NODE) {
      this_name = purple_chat_get_name(((PurpleChat *)n));
    }
    else {
      this_name = ((const char *)((void *)0));
    }
    cmp = purple_utf8_strcasecmp(my_name,this_name);
    if ((this_name != 0) && ((cmp < 0) || ((cmp == 0) && (node < n)))) {
      if (cur != 0) {
        gtk_tree_store_move_before((gtkblist -> treemodel),cur,&more_z);
         *iter =  *cur;
        return ;
      }
      else {
        gtk_tree_store_insert_before((gtkblist -> treemodel),iter,&groupiter,&more_z);
        return ;
      }
    }
  }while (gtk_tree_model_iter_next(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&more_z) != 0);
  if (cur != 0) {
    gtk_tree_store_move_before((gtkblist -> treemodel),cur,0);
     *iter =  *cur;
    return ;
  }
  else {
    gtk_tree_store_append((gtkblist -> treemodel),iter,&groupiter);
    return ;
  }
}

static void sort_method_status(PurpleBlistNode *node,PurpleBuddyList *blist,GtkTreeIter groupiter,GtkTreeIter *cur,GtkTreeIter *iter)
{
  GtkTreeIter more_z;
  PurpleBuddy *my_buddy;
  PurpleBuddy *this_buddy;
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    my_buddy = purple_contact_get_priority_buddy(((PurpleContact *)node));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
    if (cur != ((GtkTreeIter *)((void *)0))) {
       *iter =  *cur;
      return ;
    }
    gtk_tree_store_append((gtkblist -> treemodel),iter,&groupiter);
    return ;
  }
  else {
    sort_method_alphabetical(node,blist,groupiter,cur,iter);
    return ;
  }
  if (!(gtk_tree_model_iter_children(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&more_z,&groupiter) != 0)) {
    gtk_tree_store_insert((gtkblist -> treemodel),iter,&groupiter,0);
    return ;
  }
  do {
    PurpleBlistNode *n;
    gint name_cmp;
    gint presence_cmp;
    gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&more_z,NODE_COLUMN,&n,-1);
    if ((purple_blist_node_get_type(n)) == PURPLE_BLIST_CONTACT_NODE) {
      this_buddy = purple_contact_get_priority_buddy(((PurpleContact *)n));
    }
    else {
      this_buddy = ((PurpleBuddy *)((void *)0));
    }
    name_cmp = purple_utf8_strcasecmp(purple_contact_get_alias(purple_buddy_get_contact(my_buddy)),((this_buddy != 0)?purple_contact_get_alias(purple_buddy_get_contact(this_buddy)) : ((const char *)((void *)0))));
    presence_cmp = purple_presence_compare((purple_buddy_get_presence(my_buddy)),(((this_buddy != 0)?purple_buddy_get_presence(this_buddy) : ((struct _PurplePresence *)((void *)0)))));
    if ((this_buddy == ((PurpleBuddy *)((void *)0))) || ((presence_cmp < 0) || ((presence_cmp == 0) && ((name_cmp < 0) || ((name_cmp == 0) && (node < n)))))) {
      if (cur != ((GtkTreeIter *)((void *)0))) {
        gtk_tree_store_move_before((gtkblist -> treemodel),cur,&more_z);
         *iter =  *cur;
        return ;
      }
      else {
        gtk_tree_store_insert_before((gtkblist -> treemodel),iter,&groupiter,&more_z);
        return ;
      }
    }
  }while (gtk_tree_model_iter_next(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&more_z) != 0);
  if (cur != 0) {
    gtk_tree_store_move_before((gtkblist -> treemodel),cur,0);
     *iter =  *cur;
    return ;
  }
  else {
    gtk_tree_store_append((gtkblist -> treemodel),iter,&groupiter);
    return ;
  }
}

static void sort_method_log_activity(PurpleBlistNode *node,PurpleBuddyList *blist,GtkTreeIter groupiter,GtkTreeIter *cur,GtkTreeIter *iter)
{
  GtkTreeIter more_z;
  int activity_score = 0;
  int this_log_activity_score = 0;
  const char *buddy_name;
  const char *this_buddy_name;
  if ((cur != 0) && (gtk_tree_model_iter_n_children(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&groupiter) == 1)) {
     *iter =  *cur;
    return ;
  }
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    PurpleBlistNode *n;
    PurpleBuddy *buddy;
    for (n = (node -> child); n != 0; n = (n -> next)) {
      buddy = ((PurpleBuddy *)n);
      activity_score += purple_log_get_activity_score(PURPLE_LOG_IM,(buddy -> name),(buddy -> account));
    }
    buddy_name = purple_contact_get_alias(((PurpleContact *)node));
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
/* we don't have a reliable way of getting the log filename
		 * from the chat info in the blist, yet */
    if (cur != ((GtkTreeIter *)((void *)0))) {
       *iter =  *cur;
      return ;
    }
    gtk_tree_store_append((gtkblist -> treemodel),iter,&groupiter);
    return ;
  }
  else {
    sort_method_none(node,blist,groupiter,cur,iter);
    return ;
  }
  if (!(gtk_tree_model_iter_children(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&more_z,&groupiter) != 0)) {
    gtk_tree_store_insert((gtkblist -> treemodel),iter,&groupiter,0);
    return ;
  }
  do {
    PurpleBlistNode *n;
    PurpleBlistNode *n2;
    PurpleBuddy *buddy;
    int cmp;
    gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&more_z,NODE_COLUMN,&n,-1);
    this_log_activity_score = 0;
    if ((purple_blist_node_get_type(n)) == PURPLE_BLIST_CONTACT_NODE) {
      for (n2 = (n -> child); n2 != 0; n2 = (n2 -> next)) {
        buddy = ((PurpleBuddy *)n2);
        this_log_activity_score += purple_log_get_activity_score(PURPLE_LOG_IM,(buddy -> name),(buddy -> account));
      }
      this_buddy_name = purple_contact_get_alias(((PurpleContact *)n));
    }
    else {
      this_buddy_name = ((const char *)((void *)0));
    }
    cmp = purple_utf8_strcasecmp(buddy_name,this_buddy_name);
    if ((!((purple_blist_node_get_type(n)) == PURPLE_BLIST_CONTACT_NODE) || (activity_score > this_log_activity_score)) || ((activity_score == this_log_activity_score) && ((cmp < 0) || ((cmp == 0) && (node < n))))) {
      if (cur != ((GtkTreeIter *)((void *)0))) {
        gtk_tree_store_move_before((gtkblist -> treemodel),cur,&more_z);
         *iter =  *cur;
        return ;
      }
      else {
        gtk_tree_store_insert_before((gtkblist -> treemodel),iter,&groupiter,&more_z);
        return ;
      }
    }
  }while (gtk_tree_model_iter_next(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkblist -> treemodel)),gtk_tree_model_get_type()))),&more_z) != 0);
  if (cur != ((GtkTreeIter *)((void *)0))) {
    gtk_tree_store_move_before((gtkblist -> treemodel),cur,0);
     *iter =  *cur;
    return ;
  }
  else {
    gtk_tree_store_append((gtkblist -> treemodel),iter,&groupiter);
    return ;
  }
}

static void plugin_act(GtkObject *obj,PurplePluginAction *pam)
{
  if ((pam != 0) && ((pam -> callback) != 0)) 
    ( *(pam -> callback))(pam);
}

static void build_plugin_actions(GtkWidget *menu,PurplePlugin *plugin,gpointer context)
{
  GtkWidget *menuitem;
  PurplePluginAction *action = (PurplePluginAction *)((void *)0);
  GList *actions;
  GList *l;
  actions = ((((plugin -> info) != ((PurplePluginInfo *)((void *)0))) && (( *(plugin -> info)).actions != ((GList *(*)(PurplePlugin *, gpointer ))((void *)0))))?( *( *(plugin -> info)).actions)(plugin,context) : ((struct _GList *)((void *)0)));
  for (l = actions; l != ((GList *)((void *)0)); l = (l -> next)) {
    if ((l -> data) != 0) {
      action = ((PurplePluginAction *)(l -> data));
      action -> plugin = plugin;
      action -> context = context;
      menuitem = gtk_menu_item_new_with_label((action -> label));
      gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem);
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"activate",((GCallback )plugin_act),action,0,((GConnectFlags )0));
      g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"plugin_action",action,((GDestroyNotify )purple_plugin_action_free));
      gtk_widget_show(menuitem);
    }
    else 
      pidgin_separator(menu);
  }
  g_list_free(actions);
}

static void modify_account_cb(GtkWidget *widget,gpointer data)
{
  pidgin_account_dialog_show(PIDGIN_MODIFY_ACCOUNT_DIALOG,data);
}

static void enable_account_cb(GtkCheckMenuItem *widget,gpointer data)
{
  PurpleAccount *account = data;
  const PurpleSavedStatus *saved_status;
  saved_status = (purple_savedstatus_get_current());
  purple_savedstatus_activate_for_account(saved_status,account);
  purple_account_set_enabled(account,"gtk-gaim",(!0));
}

static void disable_account_cb(GtkCheckMenuItem *widget,gpointer data)
{
  PurpleAccount *account = data;
  purple_account_set_enabled(account,"gtk-gaim",0);
}

void pidgin_blist_update_accounts_menu()
{
  GtkWidget *menuitem = (GtkWidget *)((void *)0);
  GtkWidget *submenu = (GtkWidget *)((void *)0);
  GtkAccelGroup *accel_group = (GtkAccelGroup *)((void *)0);
  GList *l = (GList *)((void *)0);
  GList *accounts = (GList *)((void *)0);
  gboolean disabled_accounts = 0;
  gboolean enabled_accounts = 0;
  if (accountmenu == ((GtkWidget *)((void *)0))) 
    return ;
/* Clear the old Accounts menu */
  for (l = gtk_container_get_children(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)accountmenu),gtk_container_get_type())))); l != 0; l = g_list_delete_link(l,l)) {
    menuitem = (l -> data);
    if (menuitem != gtk_item_factory_get_widget((gtkblist -> ift),"/Accounts/Manage Accounts")) 
      gtk_widget_destroy(menuitem);
  }
  for (accounts = purple_accounts_get_all(); accounts != 0; accounts = (accounts -> next)) {
    char *buf = (char *)((void *)0);
    GtkWidget *image = (GtkWidget *)((void *)0);
    PurpleAccount *account = (PurpleAccount *)((void *)0);
    GdkPixbuf *pixbuf = (GdkPixbuf *)((void *)0);
    account = (accounts -> data);
    if (!(purple_account_get_enabled(account,"gtk-gaim") != 0)) {
      if (!(disabled_accounts != 0)) {
        menuitem = gtk_menu_item_new_with_label(((const char *)(dgettext("pidgin","Enable Account"))));
        gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)accountmenu),gtk_menu_shell_get_type()))),menuitem);
        submenu = gtk_menu_new();
        gtk_menu_set_accel_group(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)submenu),gtk_menu_get_type()))),accel_group);
        gtk_menu_set_accel_path(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)submenu),gtk_menu_get_type()))),"<PurpleMain>/Accounts/Enable Account");
        gtk_menu_item_set_submenu(((GtkMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_menu_item_get_type()))),submenu);
        disabled_accounts = (!0);
      }
      buf = g_strconcat(purple_account_get_username(account)," (",purple_account_get_protocol_name(account),")",((void *)((void *)0)));
      menuitem = gtk_image_menu_item_new_with_label(buf);
      g_free(buf);
      pixbuf = pidgin_create_prpl_icon(account,PIDGIN_PRPL_ICON_SMALL);
      if (pixbuf != ((GdkPixbuf *)((void *)0))) {
        if (!(purple_account_is_connected(account) != 0)) 
          gdk_pixbuf_saturate_and_pixelate(pixbuf,pixbuf,0.0,0);
        image = gtk_image_new_from_pixbuf(pixbuf);
        g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
        gtk_widget_show(image);
        gtk_image_menu_item_set_image(((GtkImageMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_image_menu_item_get_type()))),image);
      }
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"activate",((GCallback )enable_account_cb),account,0,((GConnectFlags )0));
      gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)submenu),gtk_menu_shell_get_type()))),menuitem);
    }
    else {
      enabled_accounts = (!0);
    }
  }
  if (!(enabled_accounts != 0)) {
    gtk_widget_show_all(accountmenu);
    return ;
  }
  pidgin_separator(accountmenu);
  accel_group = gtk_menu_get_accel_group(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)accountmenu),gtk_menu_get_type()))));
  for (accounts = purple_accounts_get_all(); accounts != 0; accounts = (accounts -> next)) {{
      char *buf = (char *)((void *)0);
      char *accel_path_buf = (char *)((void *)0);
      GtkWidget *image = (GtkWidget *)((void *)0);
      PurpleConnection *gc = (PurpleConnection *)((void *)0);
      PurpleAccount *account = (PurpleAccount *)((void *)0);
      GdkPixbuf *pixbuf = (GdkPixbuf *)((void *)0);
      PurplePlugin *plugin = (PurplePlugin *)((void *)0);
      PurplePluginProtocolInfo *prpl_info;
      account = (accounts -> data);
      if (!(purple_account_get_enabled(account,"gtk-gaim") != 0)) 
        continue; 
      buf = g_strconcat(purple_account_get_username(account)," (",purple_account_get_protocol_name(account),")",((void *)((void *)0)));
      menuitem = gtk_image_menu_item_new_with_label(buf);
      accel_path_buf = g_strconcat("<PurpleMain>/Accounts/",buf,((void *)((void *)0)));
      g_free(buf);
      pixbuf = pidgin_create_prpl_icon(account,PIDGIN_PRPL_ICON_SMALL);
      if (pixbuf != ((GdkPixbuf *)((void *)0))) {
        if (!(purple_account_is_connected(account) != 0)) 
          gdk_pixbuf_saturate_and_pixelate(pixbuf,pixbuf,0.0,0);
        image = gtk_image_new_from_pixbuf(pixbuf);
        g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
        gtk_widget_show(image);
        gtk_image_menu_item_set_image(((GtkImageMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_image_menu_item_get_type()))),image);
      }
      gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)accountmenu),gtk_menu_shell_get_type()))),menuitem);
      submenu = gtk_menu_new();
      gtk_menu_set_accel_group(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)submenu),gtk_menu_get_type()))),accel_group);
      gtk_menu_set_accel_path(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)submenu),gtk_menu_get_type()))),accel_path_buf);
      g_free(accel_path_buf);
      gtk_menu_item_set_submenu(((GtkMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_menu_item_get_type()))),submenu);
      menuitem = gtk_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","_Edit Account"))));
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"activate",((GCallback )modify_account_cb),account,0,((GConnectFlags )0));
      gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)submenu),gtk_menu_shell_get_type()))),menuitem);
      pidgin_separator(submenu);
      gc = purple_account_get_connection(account);
      plugin = (((gc != 0) && ((purple_connection_get_state(gc)) == PURPLE_CONNECTED))?(gc -> prpl) : ((struct _PurplePlugin *)((void *)0)));
      prpl_info = ((plugin != 0)?((PurplePluginProtocolInfo *)( *(plugin -> info)).extra_info) : ((struct _PurplePluginProtocolInfo *)((void *)0)));
      if ((prpl_info != 0) && ((((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).get_moods))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).get_moods))) < (prpl_info -> struct_size))) && ((prpl_info -> get_moods) != ((PurpleMood *(*)(PurpleAccount *))((void *)0)))) || (((plugin -> info) != ((PurplePluginInfo *)((void *)0))) && (( *(plugin -> info)).actions != ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)))))) {
        if ((((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).get_moods))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).get_moods))) < (prpl_info -> struct_size))) && ((prpl_info -> get_moods) != ((PurpleMood *(*)(PurpleAccount *))((void *)0)))) && (((gc -> flags) & PURPLE_CONNECTION_SUPPORT_MOODS) != 0U)) {
          if (purple_account_get_status(account,"mood") != 0) {
            menuitem = gtk_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","Set _Mood..."))));
            g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"activate",((GCallback )set_mood_cb),account,0,((GConnectFlags )0));
            gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)submenu),gtk_menu_shell_get_type()))),menuitem);
          }
        }
        if (((plugin -> info) != ((PurplePluginInfo *)((void *)0))) && (( *(plugin -> info)).actions != ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)))) {
          build_plugin_actions(submenu,plugin,gc);
        }
      }
      else {
        menuitem = gtk_menu_item_new_with_label(((const char *)(dgettext("pidgin","No actions available"))));
        gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)submenu),gtk_menu_shell_get_type()))),menuitem);
        gtk_widget_set_sensitive(menuitem,0);
      }
      pidgin_separator(submenu);
      menuitem = gtk_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","_Disable"))));
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"activate",((GCallback )disable_account_cb),account,0,((GConnectFlags )0));
      gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)submenu),gtk_menu_shell_get_type()))),menuitem);
    }
  }
  gtk_widget_show_all(accountmenu);
}
static GList *plugin_submenus = (GList *)((void *)0);

void pidgin_blist_update_plugin_actions()
{
  GtkWidget *menuitem;
  GtkWidget *submenu;
  PurplePlugin *plugin = (PurplePlugin *)((void *)0);
  GList *l;
  GtkAccelGroup *accel_group;
  GtkWidget *pluginmenu = gtk_item_factory_get_widget((gtkblist -> ift),"/Tools");
  do {
    if (pluginmenu != ((GtkWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pluginmenu != NULL");
      return ;
    };
  }while (0);
/* Remove old plugin action submenus from the Tools menu */
  for (l = plugin_submenus; l != 0; l = (l -> next)) 
    gtk_widget_destroy(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(l -> data)),gtk_widget_get_type()))));
  g_list_free(plugin_submenus);
  plugin_submenus = ((GList *)((void *)0));
  accel_group = gtk_menu_get_accel_group(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)pluginmenu),gtk_menu_get_type()))));
/* Add a submenu for each plugin with custom actions */
  for (l = purple_plugins_get_loaded(); l != 0; l = (l -> next)) {{
      char *path;
      plugin = ((PurplePlugin *)(l -> data));
      if (( *(plugin -> info)).type == PURPLE_PLUGIN_PROTOCOL) 
        continue; 
      if (!(((plugin -> info) != ((PurplePluginInfo *)((void *)0))) && (( *(plugin -> info)).actions != ((GList *(*)(PurplePlugin *, gpointer ))((void *)0))))) 
        continue; 
      menuitem = gtk_image_menu_item_new_with_label(((const char *)(dgettext("pidgin",( *(plugin -> info)).name))));
      gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)pluginmenu),gtk_menu_shell_get_type()))),menuitem);
      plugin_submenus = g_list_append(plugin_submenus,menuitem);
      submenu = gtk_menu_new();
      gtk_menu_item_set_submenu(((GtkMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_menu_item_get_type()))),submenu);
      gtk_menu_set_accel_group(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)submenu),gtk_menu_get_type()))),accel_group);
      path = g_strdup_printf("%s/Tools/%s",( *(gtkblist -> ift)).path,( *(plugin -> info)).name);
      gtk_menu_set_accel_path(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)submenu),gtk_menu_get_type()))),path);
      g_free(path);
      build_plugin_actions(submenu,plugin,0);
    }
  }
  gtk_widget_show_all(pluginmenu);
}

static void sortmethod_act(GtkCheckMenuItem *checkmenuitem,char *id)
{
  if (gtk_check_menu_item_get_active(checkmenuitem) != 0) {
    pidgin_set_cursor((gtkblist -> window),GDK_WATCH);
/* This is redundant. I think. */
/* pidgin_blist_sort_method_set(id); */
    purple_prefs_set_string("/pidgin/blist/sort_type",id);
    pidgin_clear_cursor((gtkblist -> window));
  }
}

void pidgin_blist_update_sort_methods()
{
  GtkWidget *menuitem = (GtkWidget *)((void *)0);
  GtkWidget *activeitem = (GtkWidget *)((void *)0);
  PidginBlistSortMethod *method = (PidginBlistSortMethod *)((void *)0);
  GList *l;
  GSList *sl = (GSList *)((void *)0);
  GtkWidget *sortmenu;
  const char *m = purple_prefs_get_string("/pidgin/blist/sort_type");
  if ((gtkblist == ((PidginBuddyList *)((void *)0))) || ((gtkblist -> ift) == ((GtkItemFactory *)((void *)0)))) 
    return ;
  do {
    if (m != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"m != NULL");
      return ;
    };
  }while (0);
  sortmenu = gtk_item_factory_get_widget((gtkblist -> ift),"/Buddies/Sort Buddies");
  if (sortmenu == ((GtkWidget *)((void *)0))) 
    return ;
/* Clear the old menu */
  for (l = gtk_container_get_children(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)sortmenu),gtk_container_get_type())))); l != 0; l = g_list_delete_link(l,l)) {
    menuitem = (l -> data);
    gtk_widget_destroy(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_widget_get_type()))));
  }
  for (l = pidgin_blist_sort_methods; l != 0; l = (l -> next)) {
    method = ((PidginBlistSortMethod *)(l -> data));
    menuitem = gtk_radio_menu_item_new_with_label(sl,((const char *)(dgettext("pidgin",(method -> name)))));
    if (g_str_equal(m,(method -> id)) != 0) 
      activeitem = menuitem;
    sl = gtk_radio_menu_item_get_group(((GtkRadioMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_radio_menu_item_get_type()))));
    gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)sortmenu),gtk_menu_shell_get_type()))),menuitem);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"toggled",((GCallback )sortmethod_act),(method -> id),0,((GConnectFlags )0));
    gtk_widget_show(menuitem);
  }
  if (activeitem != 0) 
    gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)activeitem),gtk_check_menu_item_get_type()))),(!0));
}
