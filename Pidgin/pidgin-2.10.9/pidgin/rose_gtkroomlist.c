/**
 * @file gtkroomlist.c GTK+ Room List UI
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "pidgin.h"
#include "gtkutils.h"
#include "pidginstock.h"
#include "pidgintooltip.h"
#include "debug.h"
#include "account.h"
#include "connection.h"
#include "notify.h"
#include "gtkroomlist.h"
typedef struct _PidginRoomlistDialog {
GtkWidget *window;
GtkWidget *account_widget;
GtkWidget *progress;
GtkWidget *sw;
GtkWidget *stop_button;
GtkWidget *list_button;
GtkWidget *add_button;
GtkWidget *join_button;
GtkWidget *close_button;
PurpleAccount *account;
PurpleRoomlist *roomlist;
gboolean pg_needs_pulse;
guint pg_update_to;}PidginRoomlistDialog;
typedef struct _PidginRoomlist {
PidginRoomlistDialog *dialog;
GtkTreeStore *model;
GtkWidget *tree;
/**< Meow. */
GHashTable *cats;
gint num_rooms;
gint total_rooms;
GtkWidget *tipwindow;
GdkRectangle tip_rect;
PangoLayout *tip_layout;
PangoLayout *tip_name_layout;
int tip_height;
int tip_width;
int tip_name_height;
int tip_name_width;}PidginRoomlist;
enum __unnamed_enum___F0_L75_C1_NAME_COLUMN__COMMA__ROOM_COLUMN__COMMA__NUM_OF_COLUMNS {NAME_COLUMN,ROOM_COLUMN,NUM_OF_COLUMNS};
static GList *roomlists = (GList *)((void *)0);

static gint delete_win_cb(GtkWidget *w,GdkEventAny *e,gpointer d)
{
  PidginRoomlistDialog *dialog = d;
  if (((dialog -> roomlist) != 0) && (purple_roomlist_get_in_progress((dialog -> roomlist)) != 0)) 
    purple_roomlist_cancel_get_list((dialog -> roomlist));
  if ((dialog -> pg_update_to) > 0) 
    purple_timeout_remove((dialog -> pg_update_to));
  if ((dialog -> roomlist) != 0) {
    PidginRoomlist *rl = ( *(dialog -> roomlist)).ui_data;
    if ((dialog -> pg_update_to) > 0) 
/* yes, that's right, unref it twice. */
      purple_roomlist_unref((dialog -> roomlist));
    if (rl != 0) 
      rl -> dialog = ((PidginRoomlistDialog *)((void *)0));
    purple_roomlist_unref((dialog -> roomlist));
  }
  dialog -> progress = ((GtkWidget *)((void *)0));
  g_free(dialog);
  return 0;
}

static void dialog_select_account_cb(GObject *w,PurpleAccount *account,PidginRoomlistDialog *dialog)
{
  gboolean change = (account != (dialog -> account));
  dialog -> account = account;
  if ((change != 0) && ((dialog -> roomlist) != 0)) {
    PidginRoomlist *rl = ( *(dialog -> roomlist)).ui_data;
    if ((rl -> tree) != 0) {
      gtk_widget_destroy((rl -> tree));
      rl -> tree = ((GtkWidget *)((void *)0));
    }
    purple_roomlist_unref((dialog -> roomlist));
    dialog -> roomlist = ((PurpleRoomlist *)((void *)0));
  }
}

static void list_button_cb(GtkButton *button,PidginRoomlistDialog *dialog)
{
  PurpleConnection *gc;
  PidginRoomlist *rl;
  gc = purple_account_get_connection((dialog -> account));
  if (!(gc != 0)) 
    return ;
  if ((dialog -> roomlist) != ((PurpleRoomlist *)((void *)0))) {
    rl = ( *(dialog -> roomlist)).ui_data;
    gtk_widget_destroy((rl -> tree));
    purple_roomlist_unref((dialog -> roomlist));
  }
  dialog -> roomlist = purple_roomlist_get_list(gc);
  if (!((dialog -> roomlist) != 0)) 
    return ;
  purple_roomlist_ref((dialog -> roomlist));
  rl = ( *(dialog -> roomlist)).ui_data;
  rl -> dialog = dialog;
  if ((dialog -> account_widget) != 0) 
    gtk_widget_set_sensitive((dialog -> account_widget),0);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> sw)),gtk_container_get_type()))),(rl -> tree));
/* some protocols (not bundled with libpurple) finish getting their
	 * room list immediately */
  if (purple_roomlist_get_in_progress((dialog -> roomlist)) != 0) {
    gtk_widget_set_sensitive((dialog -> stop_button),(!0));
    gtk_widget_set_sensitive((dialog -> list_button),0);
  }
  else {
    gtk_widget_set_sensitive((dialog -> stop_button),0);
    gtk_widget_set_sensitive((dialog -> list_button),(!0));
  }
  gtk_widget_set_sensitive((dialog -> add_button),0);
  gtk_widget_set_sensitive((dialog -> join_button),0);
}

static void stop_button_cb(GtkButton *button,PidginRoomlistDialog *dialog)
{
  purple_roomlist_cancel_get_list((dialog -> roomlist));
  if ((dialog -> account_widget) != 0) 
    gtk_widget_set_sensitive((dialog -> account_widget),(!0));
  gtk_widget_set_sensitive((dialog -> stop_button),0);
  gtk_widget_set_sensitive((dialog -> list_button),(!0));
  gtk_widget_set_sensitive((dialog -> add_button),0);
  gtk_widget_set_sensitive((dialog -> join_button),0);
}

static void close_button_cb(GtkButton *button,PidginRoomlistDialog *dialog)
{
  GtkWidget *window = (dialog -> window);
  delete_win_cb(0,0,dialog);
  gtk_widget_destroy(window);
}

struct _menu_cb_info 
{
  PurpleRoomlist *list;
  PurpleRoomlistRoom *room;
}
;

static void selection_changed_cb(GtkTreeSelection *selection,PidginRoomlist *grl)
{
  GtkTreeIter iter;
  GValue val;
  PurpleRoomlistRoom *room;
  static struct _menu_cb_info *info;
  PidginRoomlistDialog *dialog = (grl -> dialog);
  if (gtk_tree_selection_get_selected(selection,0,&iter) != 0) {
    val.g_type = 0;
    gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(grl -> model)),gtk_tree_model_get_type()))),&iter,ROOM_COLUMN,&val);
    room = (g_value_get_pointer((&val)));
    if (!(room != 0) || !(((room -> type) & PURPLE_ROOMLIST_ROOMTYPE_ROOM) != 0U)) {
      gtk_widget_set_sensitive((dialog -> join_button),0);
      gtk_widget_set_sensitive((dialog -> add_button),0);
      return ;
    }
    info = ((struct _menu_cb_info *)(g_malloc0_n(1,(sizeof(struct _menu_cb_info )))));
    info -> list = (dialog -> roomlist);
    info -> room = room;
    g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> join_button)),((GType )(20 << 2))))),"room-info",info,g_free);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> add_button)),((GType )(20 << 2))))),"room-info",info);
    gtk_widget_set_sensitive((dialog -> add_button),(!0));
    gtk_widget_set_sensitive((dialog -> join_button),(!0));
  }
  else {
    gtk_widget_set_sensitive((dialog -> add_button),0);
    gtk_widget_set_sensitive((dialog -> join_button),0);
  }
}

static void do_add_room_cb(GtkWidget *w,struct _menu_cb_info *info)
{
  char *name;
  PurpleConnection *gc = purple_account_get_connection(( *(info -> list)).account);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  if (gc != ((PurpleConnection *)((void *)0))) 
    prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info);
  if ((prpl_info != ((PurplePluginProtocolInfo *)((void *)0))) && ((prpl_info -> roomlist_room_serialize) != 0)) 
    name = ( *(prpl_info -> roomlist_room_serialize))((info -> room));
  else 
    name = g_strdup(( *(info -> room)).name);
  purple_blist_request_add_chat(( *(info -> list)).account,0,0,name);
  g_free(name);
}

static void add_room_to_blist_cb(GtkButton *button,PidginRoomlistDialog *dialog)
{
  PurpleRoomlist *rl = (dialog -> roomlist);
  PidginRoomlist *grl = (rl -> ui_data);
  struct _menu_cb_info *info = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"room-info"));
  if (info != ((struct _menu_cb_info *)((void *)0))) 
    do_add_room_cb((grl -> tree),info);
}

static void do_join_cb(GtkWidget *w,struct _menu_cb_info *info)
{
  purple_roomlist_room_join((info -> list),(info -> room));
}

static void join_button_cb(GtkButton *button,PidginRoomlistDialog *dialog)
{
  PurpleRoomlist *rl = (dialog -> roomlist);
  PidginRoomlist *grl = (rl -> ui_data);
  struct _menu_cb_info *info = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"room-info"));
  if (info != ((struct _menu_cb_info *)((void *)0))) 
    do_join_cb((grl -> tree),info);
}

static void row_activated_cb(GtkTreeView *tv,GtkTreePath *path,GtkTreeViewColumn *arg2,PurpleRoomlist *list)
{
  PidginRoomlist *grl = (list -> ui_data);
  GtkTreeIter iter;
  PurpleRoomlistRoom *room;
  GValue val;
  struct _menu_cb_info info;
  gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(grl -> model)),gtk_tree_model_get_type()))),&iter,path);
  val.g_type = 0;
  gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(grl -> model)),gtk_tree_model_get_type()))),&iter,ROOM_COLUMN,&val);
  room = (g_value_get_pointer((&val)));
  if (!(room != 0) || !(((room -> type) & PURPLE_ROOMLIST_ROOMTYPE_ROOM) != 0U)) 
    return ;
  info.list = list;
  info.room = room;
  do_join_cb(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)tv),gtk_widget_get_type()))),&info);
}

static gboolean room_click_cb(GtkWidget *tv,GdkEventButton *event,PurpleRoomlist *list)
{
  GtkTreePath *path;
  PidginRoomlist *grl = (list -> ui_data);
  GValue val;
  PurpleRoomlistRoom *room;
  GtkTreeIter iter;
  GtkWidget *menu;
/* XXX? */
  static struct _menu_cb_info info;
  if (((event -> button) != 3) || ((event -> type) != GDK_BUTTON_PRESS)) 
    return 0;
/* Here we figure out which room was clicked */
  if (!(gtk_tree_view_get_path_at_pos(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gtk_tree_view_get_type()))),(event -> x),(event -> y),&path,0,0,0) != 0)) 
    return 0;
  gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(grl -> model)),gtk_tree_model_get_type()))),&iter,path);
  gtk_tree_path_free(path);
  val.g_type = 0;
  gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(grl -> model)),gtk_tree_model_get_type()))),&iter,ROOM_COLUMN,&val);
  room = (g_value_get_pointer((&val)));
  if (!(room != 0) || !(((room -> type) & PURPLE_ROOMLIST_ROOMTYPE_ROOM) != 0U)) 
    return 0;
  info.list = list;
  info.room = room;
  menu = gtk_menu_new();
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Join"))),"pidgin-chat",((GCallback )do_join_cb),(&info),0,0,0);
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Add"))),"gtk-add",((GCallback )do_add_room_cb),(&info),0,0,0);
  gtk_widget_show_all(menu);
  gtk_menu_popup(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_get_type()))),0,0,0,0,3,(event -> time));
  return 0;
}

static void row_expanded_cb(GtkTreeView *treeview,GtkTreeIter *arg1,GtkTreePath *arg2,gpointer user_data)
{
  PurpleRoomlist *list = user_data;
  PurpleRoomlistRoom *category;
  GValue val;
  val.g_type = 0;
  gtk_tree_model_get_value(gtk_tree_view_get_model(treeview),arg1,ROOM_COLUMN,&val);
  category = (g_value_get_pointer((&val)));
  if (!((category -> expanded_once) != 0)) {
    purple_roomlist_expand_category(list,category);
    category -> expanded_once = (!0);
  }
}
#define SMALL_SPACE 6
#define TOOLTIP_BORDER 12

static gboolean pidgin_roomlist_paint_tooltip(GtkWidget *widget,gpointer user_data)
{
  PurpleRoomlist *list = user_data;
  PidginRoomlist *grl = (list -> ui_data);
  GtkStyle *style;
  int current_height;
  int max_width;
  int max_text_width;
  GtkTextDirection dir = gtk_widget_get_direction(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(grl -> tree)),gtk_widget_get_type()))));
  style = ( *(grl -> tipwindow)).style;
  max_text_width = (((grl -> tip_width) > (grl -> tip_name_width))?(grl -> tip_width) : (grl -> tip_name_width));
  max_width = ((12 + 6 + max_text_width) + 12);
  current_height = 12;
  if (dir == GTK_TEXT_DIR_RTL) {
    gtk_paint_layout(style,( *(grl -> tipwindow)).window,GTK_STATE_NORMAL,0,0,(grl -> tipwindow),"tooltip",((max_width - (12 + 6)) - (((int )600000) + 512 >> 10)),current_height,(grl -> tip_name_layout));
  }
  else {
    gtk_paint_layout(style,( *(grl -> tipwindow)).window,GTK_STATE_NORMAL,0,0,(grl -> tipwindow),"tooltip",(12 + 6),current_height,(grl -> tip_name_layout));
  }
  if (dir != GTK_TEXT_DIR_RTL) {
    gtk_paint_layout(style,( *(grl -> tipwindow)).window,GTK_STATE_NORMAL,0,0,(grl -> tipwindow),"tooltip",(12 + 6),(current_height + (grl -> tip_name_height)),(grl -> tip_layout));
  }
  else {
    gtk_paint_layout(style,( *(grl -> tipwindow)).window,GTK_STATE_NORMAL,0,0,(grl -> tipwindow),"tooltip",((max_width - (12 + 6)) - (((int )600000) + 512 >> 10)),(current_height + (grl -> tip_name_height)),(grl -> tip_layout));
  }
  return 0;
}

static gboolean pidgin_roomlist_create_tip(PurpleRoomlist *list,GtkTreePath *path)
{
  PidginRoomlist *grl = (list -> ui_data);
  PurpleRoomlistRoom *room;
  GtkTreeIter iter;
  GValue val;
  gchar *name;
  gchar *tmp;
  gchar *node_name;
  GString *tooltip_text = (GString *)((void *)0);
  GList *l;
  GList *k;
  gint j;
  gboolean first = (!0);
#if 0
#endif
  gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(grl -> model)),gtk_tree_model_get_type()))),&iter,path);
  val.g_type = 0;
  gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(grl -> model)),gtk_tree_model_get_type()))),&iter,ROOM_COLUMN,&val);
  room = (g_value_get_pointer((&val)));
  if (!(room != 0) || !(((room -> type) & PURPLE_ROOMLIST_ROOMTYPE_ROOM) != 0U)) 
    return 0;
  tooltip_text = g_string_new("");
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(grl -> model)),gtk_tree_model_get_type()))),&iter,NAME_COLUMN,&name,-1);
  for (((((j = NUM_OF_COLUMNS) , (l = (room -> fields)))) , (k = (list -> fields))); (l != 0) && (k != 0); (((j++ , (l = (l -> next)))) , (k = (k -> next)))) {{
      PurpleRoomlistField *f = (k -> data);
      gchar *label;
      if ((f -> hidden) != 0) 
        continue; 
      label = g_markup_escape_text((f -> label),(-1));
      switch(f -> type){
        case PURPLE_ROOMLIST_FIELD_BOOL:
{
          g_string_append_printf(tooltip_text,"%s<b>%s:</b> %s",((first != 0)?"" : "\n"),label,(((l -> data) != 0)?"True" : "False"));
          break; 
        }
        case PURPLE_ROOMLIST_FIELD_INT:
{
          g_string_append_printf(tooltip_text,"%s<b>%s:</b> %d",((first != 0)?"" : "\n"),label,((gint )((glong )(l -> data))));
          break; 
        }
        case PURPLE_ROOMLIST_FIELD_STRING:
{
          tmp = g_markup_escape_text(((char *)(l -> data)),(-1));
          g_string_append_printf(tooltip_text,"%s<b>%s:</b> %s",((first != 0)?"" : "\n"),label,tmp);
          g_free(tmp);
          break; 
        }
      }
      first = 0;
      g_free(label);
    }
  }
  grl -> tip_layout = gtk_widget_create_pango_layout((grl -> tipwindow),0);
  grl -> tip_name_layout = gtk_widget_create_pango_layout((grl -> tipwindow),0);
  tmp = g_markup_escape_text(name,(-1));
  g_free(name);
  node_name = g_strdup_printf("<span size=\'x-large\' weight=\'bold\'>%s</span>",tmp);
  g_free(tmp);
  pango_layout_set_markup((grl -> tip_layout),(tooltip_text -> str),-1);
  pango_layout_set_wrap((grl -> tip_layout),PANGO_WRAP_WORD);
  pango_layout_set_width((grl -> tip_layout),600000);
  pango_layout_get_size((grl -> tip_layout),&grl -> tip_width,&grl -> tip_height);
  grl -> tip_width = ((((int )(grl -> tip_width)) + 512) >> 10);
  grl -> tip_height = ((((int )(grl -> tip_height)) + 512) >> 10);
  pango_layout_set_markup((grl -> tip_name_layout),node_name,-1);
  pango_layout_set_wrap((grl -> tip_name_layout),PANGO_WRAP_WORD);
  pango_layout_set_width((grl -> tip_name_layout),600000);
  pango_layout_get_size((grl -> tip_name_layout),&grl -> tip_name_width,&grl -> tip_name_height);
  grl -> tip_name_width = (((((int )(grl -> tip_name_width)) + 512) >> 10) + 6);
  grl -> tip_name_height = ((((((int )(grl -> tip_name_height)) + 512) >> 10) > 6)?((((int )(grl -> tip_name_height)) + 512) >> 10) : 6);
  g_free(node_name);
  g_string_free(tooltip_text,(!0));
  return (!0);
}

static gboolean pidgin_roomlist_create_tooltip(GtkWidget *widget,GtkTreePath *path,gpointer data,int *w,int *h)
{
  PurpleRoomlist *list = data;
  PidginRoomlist *grl = (list -> ui_data);
  grl -> tipwindow = widget;
  if (!(pidgin_roomlist_create_tip(data,path) != 0)) 
    return 0;
  if (w != 0) 
     *w = ((12 + 6 + ((((grl -> tip_width) > (grl -> tip_name_width))?(grl -> tip_width) : (grl -> tip_name_width)))) + 12);
  if (h != 0) 
     *h = (((12 + (grl -> tip_height)) + (grl -> tip_name_height)) + 12);
  return (!0);
}

static gboolean account_filter_func(PurpleAccount *account)
{
  PurpleConnection *conn = purple_account_get_connection(account);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  if ((conn != 0) && ((purple_connection_get_state(conn)) == PURPLE_CONNECTED)) 
    prpl_info = ((PurplePluginProtocolInfo *)( *( *(conn -> prpl)).info).extra_info);
  return (prpl_info != 0) && ((prpl_info -> roomlist_get_list) != ((PurpleRoomlist *(*)(PurpleConnection *))((void *)0)));
}

gboolean pidgin_roomlist_is_showable()
{
  GList *c;
  PurpleConnection *gc;
  for (c = purple_connections_get_all(); c != ((GList *)((void *)0)); c = (c -> next)) {
    gc = (c -> data);
    if (account_filter_func(purple_connection_get_account(gc)) != 0) 
      return (!0);
  }
  return 0;
}

static PidginRoomlistDialog *pidgin_roomlist_dialog_new_with_account(PurpleAccount *account)
{
  PidginRoomlistDialog *dialog;
  GtkWidget *window;
  GtkWidget *vbox;
  GtkWidget *vbox2;
  GtkWidget *bbox;
  dialog = ((PidginRoomlistDialog *)(g_malloc0_n(1,(sizeof(PidginRoomlistDialog )))));
  dialog -> account = account;
/* Create the window. */
  dialog -> window = (window = pidgin_create_dialog(((const char *)(dgettext("pidgin","Room List"))),12,"room list",(!0)));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"delete_event",((GCallback )delete_win_cb),dialog,0,((GConnectFlags )0));
/* Create the parent vbox for everything. */
  vbox = pidgin_dialog_get_vbox_with_properties(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),0,12);
  vbox2 = gtk_vbox_new(0,12);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),vbox2);
  gtk_widget_show(vbox2);
/* accounts dropdown list */
  dialog -> account_widget = pidgin_account_option_menu_new((dialog -> account),0,((GCallback )dialog_select_account_cb),account_filter_func,dialog);
/* this is normally null, and we normally don't care what the first selected item is */
  if (!((dialog -> account) != 0)) 
    dialog -> account = pidgin_account_option_menu_get_selected((dialog -> account_widget));
  pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox2),gtk_box_get_type()))),((const char *)(dgettext("pidgin","_Account:"))),0,(dialog -> account_widget),(!0),0);
/* scrolled window */
  dialog -> sw = pidgin_make_scrollable(0,GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC,GTK_SHADOW_IN,-1,250);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox2),gtk_box_get_type()))),(dialog -> sw),(!0),(!0),0);
/* progress bar */
  dialog -> progress = gtk_progress_bar_new();
  gtk_progress_bar_set_pulse_step(((GtkProgressBar *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> progress)),gtk_progress_bar_get_type()))),0.1);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox2),gtk_box_get_type()))),(dialog -> progress),0,0,0);
  gtk_widget_show((dialog -> progress));
/* button box */
  bbox = pidgin_dialog_get_action_area(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))));
  gtk_box_set_spacing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),6);
  gtk_button_box_set_layout(((GtkButtonBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_button_box_get_type()))),GTK_BUTTONBOX_END);
/* stop button */
  dialog -> stop_button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),"gtk-stop",((GCallback )stop_button_cb),dialog);
  gtk_widget_set_sensitive((dialog -> stop_button),0);
/* list button */
  dialog -> list_button = pidgin_pixbuf_button_from_stock(((const char *)(dgettext("pidgin","_Get List"))),"gtk-refresh",PIDGIN_BUTTON_HORIZONTAL);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),(dialog -> list_button),0,0,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> list_button)),((GType )(20 << 2))))),"clicked",((GCallback )list_button_cb),dialog,0,((GConnectFlags )0));
  gtk_widget_show((dialog -> list_button));
/* add button */
  dialog -> add_button = pidgin_pixbuf_button_from_stock(((const char *)(dgettext("pidgin","_Add Chat"))),"gtk-add",PIDGIN_BUTTON_HORIZONTAL);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),(dialog -> add_button),0,0,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> add_button)),((GType )(20 << 2))))),"clicked",((GCallback )add_room_to_blist_cb),dialog,0,((GConnectFlags )0));
  gtk_widget_set_sensitive((dialog -> add_button),0);
  gtk_widget_show((dialog -> add_button));
/* join button */
  dialog -> join_button = pidgin_pixbuf_button_from_stock(((const char *)(dgettext("pidgin","_Join"))),"pidgin-chat",PIDGIN_BUTTON_HORIZONTAL);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),(dialog -> join_button),0,0,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> join_button)),((GType )(20 << 2))))),"clicked",((GCallback )join_button_cb),dialog,0,((GConnectFlags )0));
  gtk_widget_set_sensitive((dialog -> join_button),0);
  gtk_widget_show((dialog -> join_button));
/* close button */
  dialog -> close_button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),"gtk-close",((GCallback )close_button_cb),dialog);
/* show the dialog window and return the dialog */
  gtk_widget_show((dialog -> window));
  return dialog;
}

void pidgin_roomlist_dialog_show_with_account(PurpleAccount *account)
{
  PidginRoomlistDialog *dialog = pidgin_roomlist_dialog_new_with_account(account);
  if (!(dialog != 0)) 
    return ;
  list_button_cb(((GtkButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> list_button)),gtk_button_get_type()))),dialog);
}

void pidgin_roomlist_dialog_show()
{
  pidgin_roomlist_dialog_new_with_account(0);
}

static void pidgin_roomlist_new(PurpleRoomlist *list)
{
  PidginRoomlist *rl = (PidginRoomlist *)(g_malloc0_n(1,(sizeof(PidginRoomlist ))));
  list -> ui_data = rl;
  rl -> cats = g_hash_table_new_full(0,0,0,((GDestroyNotify )gtk_tree_row_reference_free));
  roomlists = g_list_append(roomlists,list);
}

static void int_cell_data_func(GtkTreeViewColumn *col,GtkCellRenderer *renderer,GtkTreeModel *model,GtkTreeIter *iter,gpointer user_data)
{
  gchar buf[16UL];
  int myint;
  gtk_tree_model_get(model,iter,((gint )((glong )user_data)),&myint,-1);
  if (myint != 0) 
    g_snprintf(buf,(sizeof(buf)),"%d",myint);
  else 
    buf[0] = 0;
  g_object_set(renderer,"text",buf,((void *)((void *)0)));
}
/* this sorts backwards on purpose, so that clicking name sorts a-z, while clicking users sorts
   infinity-0. you can still click again to reverse it on any of them. */

static gint int_sort_func(GtkTreeModel *model,GtkTreeIter *a,GtkTreeIter *b,gpointer user_data)
{
  int c;
  int d;
  c = (d = 0);
  gtk_tree_model_get(model,a,((gint )((glong )user_data)),&c,-1);
  gtk_tree_model_get(model,b,((gint )((glong )user_data)),&d,-1);
  if (c == d) 
    return 0;
  else if (c > d) 
    return (-1);
  else 
    return 1;
}

static gboolean _search_func(GtkTreeModel *model,gint column,const gchar *key,GtkTreeIter *iter,gpointer search_data)
{
  gboolean result;
  gchar *name;
  gchar *fold;
  gchar *fkey;
  gtk_tree_model_get(model,iter,column,&name,-1);
  fold = g_utf8_casefold(name,(-1));
  fkey = g_utf8_casefold(key,(-1));
  result = (g_strstr_len(fold,(strlen(fold)),fkey) == ((gchar *)((void *)0)));
  g_free(fold);
  g_free(fkey);
  g_free(name);
  return result;
}

static void pidgin_roomlist_set_fields(PurpleRoomlist *list,GList *fields)
{
  PidginRoomlist *grl = (list -> ui_data);
  gint columns = NUM_OF_COLUMNS;
  int j;
  GtkTreeStore *model;
  GtkWidget *tree;
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;
  GtkTreeSelection *selection;
  GList *l;
  GType *types;
  do {
    if (grl != ((PidginRoomlist *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"grl != NULL");
      return ;
    };
  }while (0);
  columns += g_list_length(fields);
  types = ((GType *)(g_malloc_n(columns,(sizeof(GType )))));
  types[NAME_COLUMN] = ((GType )(16 << 2));
  types[ROOM_COLUMN] = ((GType )(17 << 2));
  for (((j = NUM_OF_COLUMNS) , (l = fields)); l != 0; ((l = (l -> next)) , j++)) {
    PurpleRoomlistField *f = (l -> data);
    switch(f -> type){
      case PURPLE_ROOMLIST_FIELD_BOOL:
{
        types[j] = ((GType )(5 << 2));
        break; 
      }
      case PURPLE_ROOMLIST_FIELD_INT:
{
        types[j] = ((GType )(6 << 2));
        break; 
      }
      case PURPLE_ROOMLIST_FIELD_STRING:
{
        types[j] = ((GType )(16 << 2));
        break; 
      }
    }
  }
  model = gtk_tree_store_newv(columns,types);
  g_free(types);
  tree = gtk_tree_view_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))));
  gtk_tree_view_set_rules_hint(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type()))),(!0));
  selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type()))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)selection),((GType )(20 << 2))))),"changed",((GCallback )selection_changed_cb),grl,0,((GConnectFlags )0));
  g_object_unref(model);
  grl -> model = model;
  grl -> tree = tree;
  gtk_widget_show((grl -> tree));
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes(((const char *)(dgettext("pidgin","Name"))),renderer,"text",NAME_COLUMN,((void *)((void *)0)));
  gtk_tree_view_column_set_sizing(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),GTK_TREE_VIEW_COLUMN_GROW_ONLY);
  gtk_tree_view_column_set_resizable(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),(!0));
  gtk_tree_view_column_set_sort_column_id(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),NAME_COLUMN);
  gtk_tree_view_column_set_reorderable(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),(!0));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type()))),column);
  for (((j = NUM_OF_COLUMNS) , (l = fields)); l != 0; ((l = (l -> next)) , j++)) {{
      PurpleRoomlistField *f = (l -> data);
      if ((f -> hidden) != 0) 
        continue; 
      renderer = gtk_cell_renderer_text_new();
      column = gtk_tree_view_column_new_with_attributes((f -> label),renderer,"text",j,((void *)((void *)0)));
      gtk_tree_view_column_set_sizing(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),GTK_TREE_VIEW_COLUMN_GROW_ONLY);
      gtk_tree_view_column_set_resizable(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),(!0));
      gtk_tree_view_column_set_sort_column_id(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),j);
      gtk_tree_view_column_set_reorderable(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),(!0));
      if ((f -> type) == PURPLE_ROOMLIST_FIELD_INT) {
        gtk_tree_view_column_set_cell_data_func(column,renderer,int_cell_data_func,((gpointer )((glong )j)),0);
        gtk_tree_sortable_set_sort_func(((GtkTreeSortable *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_sortable_get_type()))),j,int_sort_func,((gpointer )((glong )j)),0);
      }
      gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type()))),column);
    }
  }
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"button-press-event",((GCallback )room_click_cb),list,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"row-expanded",((GCallback )row_expanded_cb),list,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"row-activated",((GCallback )row_activated_cb),list,0,((GConnectFlags )0));
#if 0 /* uncomment this when the tooltips are slightly less annoying and more well behaved */
#endif
  pidgin_tooltip_setup_for_treeview(tree,list,pidgin_roomlist_create_tooltip,pidgin_roomlist_paint_tooltip);
/* Enable CTRL+F searching */
  gtk_tree_view_set_search_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type()))),NAME_COLUMN);
  gtk_tree_view_set_search_equal_func(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type()))),_search_func,0,0);
}

static gboolean pidgin_progress_bar_pulse(gpointer data)
{
  PurpleRoomlist *list = data;
  PidginRoomlist *rl = (list -> ui_data);
  if ((!(rl != 0) || !((rl -> dialog) != 0)) || !(( *(rl -> dialog)).pg_needs_pulse != 0)) {
    if ((rl != 0) && ((rl -> dialog) != 0)) 
      ( *(rl -> dialog)).pg_update_to = 0;
    purple_roomlist_unref(list);
    return 0;
  }
  gtk_progress_bar_pulse(((GtkProgressBar *)(g_type_check_instance_cast(((GTypeInstance *)( *(rl -> dialog)).progress),gtk_progress_bar_get_type()))));
  ( *(rl -> dialog)).pg_needs_pulse = 0;
  return (!0);
}

static void pidgin_roomlist_add_room(PurpleRoomlist *list,PurpleRoomlistRoom *room)
{
  PidginRoomlist *rl = (list -> ui_data);
  GtkTreeRowReference *rr;
  GtkTreeRowReference *parentrr = (GtkTreeRowReference *)((void *)0);
  GtkTreePath *path;
  GtkTreeIter iter;
  GtkTreeIter parent;
  GtkTreeIter child;
  GList *l;
  GList *k;
  int j;
  gboolean append = (!0);
  rl -> total_rooms++;
  if ((room -> type) == PURPLE_ROOMLIST_ROOMTYPE_ROOM) 
    rl -> num_rooms++;
  if ((rl -> dialog) != 0) {
    if (( *(rl -> dialog)).pg_update_to == 0) {
      purple_roomlist_ref(list);
      ( *(rl -> dialog)).pg_update_to = g_timeout_add(100,pidgin_progress_bar_pulse,list);
      gtk_progress_bar_pulse(((GtkProgressBar *)(g_type_check_instance_cast(((GTypeInstance *)( *(rl -> dialog)).progress),gtk_progress_bar_get_type()))));
    }
    else 
      ( *(rl -> dialog)).pg_needs_pulse = (!0);
  }
  if ((room -> parent) != 0) {
    parentrr = (g_hash_table_lookup((rl -> cats),(room -> parent)));
    path = gtk_tree_row_reference_get_path(parentrr);
    if (path != 0) {
      PurpleRoomlistRoom *tmproom = (PurpleRoomlistRoom *)((void *)0);
      gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(rl -> model)),gtk_tree_model_get_type()))),&parent,path);
      gtk_tree_path_free(path);
      if (gtk_tree_model_iter_children(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(rl -> model)),gtk_tree_model_get_type()))),&child,&parent) != 0) {
        gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(rl -> model)),gtk_tree_model_get_type()))),&child,ROOM_COLUMN,&tmproom,-1);
        if (!(tmproom != 0)) 
          append = 0;
      }
    }
  }
  if (append != 0) 
    gtk_tree_store_append((rl -> model),&iter,((parentrr != 0)?&parent : ((struct _GtkTreeIter *)((void *)0))));
  else 
    iter = child;
  if (((room -> type) & PURPLE_ROOMLIST_ROOMTYPE_CATEGORY) != 0U) 
    gtk_tree_store_append((rl -> model),&child,&iter);
  path = gtk_tree_model_get_path(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(rl -> model)),gtk_tree_model_get_type()))),&iter);
  if (((room -> type) & PURPLE_ROOMLIST_ROOMTYPE_CATEGORY) != 0U) {
    rr = gtk_tree_row_reference_new(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(rl -> model)),gtk_tree_model_get_type()))),path);
    g_hash_table_insert((rl -> cats),room,rr);
  }
  gtk_tree_path_free(path);
  gtk_tree_store_set((rl -> model),&iter,NAME_COLUMN,(room -> name),-1);
  gtk_tree_store_set((rl -> model),&iter,ROOM_COLUMN,room,-1);
  for (((((j = NUM_OF_COLUMNS) , (l = (room -> fields)))) , (k = (list -> fields))); (l != 0) && (k != 0); (((j++ , (l = (l -> next)))) , (k = (k -> next)))) {{
      PurpleRoomlistField *f = (k -> data);
      if ((f -> hidden) != 0) 
        continue; 
      gtk_tree_store_set((rl -> model),&iter,j,(l -> data),-1);
    }
  }
}

static void pidgin_roomlist_in_progress(PurpleRoomlist *list,gboolean in_progress)
{
  PidginRoomlist *rl = (list -> ui_data);
  if (!(rl != 0) || !((rl -> dialog) != 0)) 
    return ;
  if (in_progress != 0) {
    if (( *(rl -> dialog)).account_widget != 0) 
      gtk_widget_set_sensitive(( *(rl -> dialog)).account_widget,0);
    gtk_widget_set_sensitive(( *(rl -> dialog)).stop_button,(!0));
    gtk_widget_set_sensitive(( *(rl -> dialog)).list_button,0);
  }
  else {
    ( *(rl -> dialog)).pg_needs_pulse = 0;
    gtk_progress_bar_set_fraction(((GtkProgressBar *)(g_type_check_instance_cast(((GTypeInstance *)( *(rl -> dialog)).progress),gtk_progress_bar_get_type()))),0.0);
    if (( *(rl -> dialog)).account_widget != 0) 
      gtk_widget_set_sensitive(( *(rl -> dialog)).account_widget,(!0));
    gtk_widget_set_sensitive(( *(rl -> dialog)).stop_button,0);
    gtk_widget_set_sensitive(( *(rl -> dialog)).list_button,(!0));
  }
}

static void pidgin_roomlist_destroy(PurpleRoomlist *list)
{
  PidginRoomlist *rl = (list -> ui_data);
  roomlists = g_list_remove(roomlists,list);
  do {
    if (rl != ((PidginRoomlist *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"rl != NULL");
      return ;
    };
  }while (0);
  g_hash_table_destroy((rl -> cats));
  g_free(rl);
  list -> ui_data = ((gpointer )((void *)0));
}
static PurpleRoomlistUiOps ops = {(pidgin_roomlist_dialog_show_with_account), (pidgin_roomlist_new), (pidgin_roomlist_set_fields), (pidgin_roomlist_add_room), (pidgin_roomlist_in_progress), (pidgin_roomlist_destroy), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))};

void pidgin_roomlist_init()
{
  purple_roomlist_set_ui_ops(&ops);
}
