/*
 * @file gtkstatusbox.c GTK+ Status Selection Widget
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
/*
 * The status box is made up of two main pieces:
 *   - The box that displays the current status, which is made
 *     of a GtkListStore ("status_box->store") and GtkCellView
 *     ("status_box->cell_view").  There is always exactly 1 row
 *     in this list store.  Only the TYPE_ICON and TYPE_TEXT
 *     columns are used in this list store.
 *   - The dropdown menu that lets users select a status, which
 *     is made of a GtkComboBox ("status_box") and GtkListStore
 *     ("status_box->dropdown_store").  This dropdown is shown
 *     when the user clicks on the box that displays the current
 *     status.  This list store contains one row for Available,
 *     one row for Away, etc., a few rows for popular statuses,
 *     and the "New..." and "Saved..." options.
 */
#include <gdk/gdkkeysyms.h>
#include "internal.h"
#include "account.h"
#include "buddyicon.h"
#include "core.h"
#include "imgstore.h"
#include "network.h"
#include "request.h"
#include "savedstatuses.h"
#include "status.h"
#include "debug.h"
#include "pidgin.h"
#include "gtksavedstatuses.h"
#include "pidginstock.h"
#include "gtkstatusbox.h"
#include "gtkutils.h"
#ifdef USE_GTKSPELL
#  include <gtkspell/gtkspell.h>
#  ifdef _WIN32
#    include "wspell.h"
#  endif
#endif
/* Timeout for typing notifications in seconds */
#define TYPING_TIMEOUT 4
static void imhtml_changed_cb(GtkTextBuffer *buffer,void *data);
static void imhtml_format_changed_cb(GtkIMHtml *imhtml,GtkIMHtmlButtons buttons,void *data);
static void remove_typing_cb(PidginStatusBox *box);
static void update_size(PidginStatusBox *box);
static gint get_statusbox_index(PidginStatusBox *box,PurpleSavedStatus *saved_status);
static PurpleAccount *check_active_accounts_for_identical_statuses();
static void pidgin_status_box_pulse_typing(PidginStatusBox *status_box);
static void pidgin_status_box_refresh(PidginStatusBox *status_box);
static void status_menu_refresh_iter(PidginStatusBox *status_box,gboolean status_changed);
static void pidgin_status_box_regenerate(PidginStatusBox *status_box,gboolean status_changed);
static void pidgin_status_box_changed(PidginStatusBox *box);
static void pidgin_status_box_size_request(GtkWidget *widget,GtkRequisition *requisition);
static void pidgin_status_box_size_allocate(GtkWidget *widget,GtkAllocation *allocation);
static gboolean pidgin_status_box_expose_event(GtkWidget *widget,GdkEventExpose *event);
static void pidgin_status_box_redisplay_buddy_icon(PidginStatusBox *status_box);
static void pidgin_status_box_forall(GtkContainer *container,gboolean include_internals,GtkCallback callback,gpointer callback_data);
static void pidgin_status_box_popup(PidginStatusBox *box);
static void pidgin_status_box_popdown(PidginStatusBox *box);
static void do_colorshift(GdkPixbuf *dest,GdkPixbuf *src,int shift);
static void icon_choose_cb(const char *filename,gpointer data);
static void remove_buddy_icon_cb(GtkWidget *w,PidginStatusBox *box);
static void choose_buddy_icon_cb(GtkWidget *w,PidginStatusBox *box);
enum __unnamed_enum___F0_L98_C1_TYPE_COLUMN__COMMA__ICON_STOCK_COLUMN__COMMA__ICON_COLUMN__COMMA__TEXT_COLUMN__COMMA__TITLE_COLUMN__COMMA__DESC_COLUMN__COMMA__DATA_COLUMN__COMMA__EMBLEM_COLUMN__COMMA__EMBLEM_VISIBLE_COLUMN__COMMA__NUM_COLUMNS {
/** A PidginStatusBoxItemType */
TYPE_COLUMN,
/** This is the stock-id for the icon. */
ICON_STOCK_COLUMN,
/**
	 * This is a GdkPixbuf (the other columns are strings).
	 * This column is visible.
	 */
ICON_COLUMN,
/** The text displayed on the status box.  This column is visible. */
TEXT_COLUMN,
/** The plain-English title of this item */
TITLE_COLUMN,
/** A plain-English description of this item */
DESC_COLUMN,
/**
	 * This value depends on TYPE_COLUMN.  For POPULAR types,
	 * this is the creation time.  For PRIMITIVE types,
	 * this is the PurpleStatusPrimitive.
	 */
DATA_COLUMN,
/**
 	 * This column stores the GdkPixbuf for the status emblem. Currently only 'saved' is stored.
	 * In the GtkTreeModel for the dropdown, this is the stock-id (gchararray), and for the
	 * GtkTreeModel for the cell_view (for the account-specific statusbox), this is the prpl-icon
	 * (GdkPixbuf) of the account.
 	 */
EMBLEM_COLUMN,
/**
 	* This column stores whether to show the emblem.
 	*/
EMBLEM_VISIBLE_COLUMN,NUM_COLUMNS};
enum __unnamed_enum___F0_L143_C1_PROP_0__COMMA__PROP_ACCOUNT__COMMA__PROP_ICON_SEL {PROP_0,PROP_ACCOUNT,PROP_ICON_SEL};
static char *typing_stock_ids[7UL] = {("pidgin-anim-typing0"), ("pidgin-anim-typing1"), ("pidgin-anim-typing2"), ("pidgin-anim-typing3"), ("pidgin-anim-typing4"), ((char *)((void *)0))};
static char *connecting_stock_ids[] = {("pidgin-anim-connect0"), ("pidgin-anim-connect1"), ("pidgin-anim-connect2"), ("pidgin-anim-connect3"), ("pidgin-anim-connect4"), ("pidgin-anim-connect5"), ("pidgin-anim-connect6"), ("pidgin-anim-connect7"), ("pidgin-anim-connect8"), ("pidgin-anim-connect9"), ("pidgin-anim-connect10"), ("pidgin-anim-connect11"), ("pidgin-anim-connect12"), ("pidgin-anim-connect13"), ("pidgin-anim-connect14"), ("pidgin-anim-connect15"), ("pidgin-anim-connect16"), ("pidgin-anim-connect17"), ("pidgin-anim-connect18"), ("pidgin-anim-connect19"), ("pidgin-anim-connect20"), ("pidgin-anim-connect21"), ("pidgin-anim-connect22"), ("pidgin-anim-connect23"), ("pidgin-anim-connect24"), ("pidgin-anim-connect25"), ("pidgin-anim-connect26"), ("pidgin-anim-connect27"), ("pidgin-anim-connect28"), ("pidgin-anim-connect29"), ("pidgin-anim-connect30"), ((char *)((void *)0))};
static GtkContainerClass *parent_class = (GtkContainerClass *)((void *)0);
static void pidgin_status_box_class_init(PidginStatusBoxClass *klass);
static void pidgin_status_box_init(PidginStatusBox *status_box);

GType pidgin_status_box_get_type()
{
  static GType status_box_type = 0;
  if (!(status_box_type != 0UL)) {
    static const GTypeInfo status_box_info = {((sizeof(PidginStatusBoxClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )pidgin_status_box_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(PidginStatusBox ))), (0), ((GInstanceInitFunc )pidgin_status_box_init), ((const GTypeValueTable *)((void *)0))
/* base_init */
/* base_finalize */
/* class_finalize */
/* class_data */
/* value_table */
};
    status_box_type = g_type_register_static(gtk_container_get_type(),"PidginStatusBox",&status_box_info,0);
  }
  return status_box_type;
}

static void pidgin_status_box_get_property(GObject *object,guint param_id,GValue *value,GParamSpec *psec)
{
  PidginStatusBox *statusbox = (PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)object),pidgin_status_box_get_type()));
  switch(param_id){
    case PROP_ACCOUNT:
{
      g_value_set_pointer(value,(statusbox -> account));
      break; 
    }
    case PROP_ICON_SEL:
{
      g_value_set_boolean(value,((statusbox -> icon_box) != ((GtkWidget *)((void *)0))));
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)psec;
        guint _glib__property_id = param_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","gtkstatusbox.c:242","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void update_to_reflect_account_status(PidginStatusBox *status_box,PurpleAccount *account,PurpleStatus *newstatus)
{
  GList *l;
  int status_no = -1;
  const PurpleStatusType *statustype = (const PurpleStatusType *)((void *)0);
  const char *message;
  statustype = purple_status_type_find_with_id(purple_account_get_status_types(account),((char *)(purple_status_type_get_id((purple_status_get_type(newstatus))))));
{
    for (l = purple_account_get_status_types(account); l != ((GList *)((void *)0)); l = (l -> next)) {{
        PurpleStatusType *status_type = (PurpleStatusType *)(l -> data);
        if (!(purple_status_type_is_user_settable(status_type) != 0) || (purple_status_type_is_independent(status_type) != 0)) 
          continue; 
        status_no++;
        if (statustype == status_type) 
          break; 
      }
    }
  }
  gtk_imhtml_set_populate_primary_clipboard(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_imhtml_get_type()))),(!0));
  if (status_no != -1) {
    GtkTreePath *path;
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type()))),0);
    path = gtk_tree_path_new_from_indices(status_no,-1);
    if ((status_box -> active_row) != 0) 
      gtk_tree_row_reference_free((status_box -> active_row));
    status_box -> active_row = gtk_tree_row_reference_new(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> dropdown_store)),gtk_tree_model_get_type()))),path);
    gtk_tree_path_free(path);
    message = purple_status_get_attr_string(newstatus,"message");
    if (!(message != 0) || !(( *message) != 0)) {
      gtk_widget_hide_all((status_box -> vbox));
      status_box -> imhtml_visible = 0;
    }
    else {
      gtk_widget_show_all((status_box -> vbox));
      status_box -> imhtml_visible = (!0);
      gtk_imhtml_delete(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_imhtml_get_type()))),0,0);
      gtk_imhtml_clear_formatting(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_imhtml_get_type()))));
      gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_imhtml_get_type()))),message,0,0);
    }
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type()))),(!0));
    pidgin_status_box_refresh(status_box);
  }
}

static void account_status_changed_cb(PurpleAccount *account,PurpleStatus *oldstatus,PurpleStatus *newstatus,PidginStatusBox *status_box)
{
  if ((status_box -> account) == account) 
    update_to_reflect_account_status(status_box,account,newstatus);
  else if ((status_box -> token_status_account) == account) 
    status_menu_refresh_iter(status_box,(!0));
}

static gboolean icon_box_press_cb(GtkWidget *widget,GdkEventButton *event,PidginStatusBox *box)
{
  if ((event -> button) == 3) {
    GtkWidget *menu_item;
    const char *path;
    if ((box -> icon_box_menu) != 0) 
      gtk_widget_destroy((box -> icon_box_menu));
    box -> icon_box_menu = gtk_menu_new();
    menu_item = pidgin_new_item_from_stock((box -> icon_box_menu),((const char *)(dgettext("pidgin","Select Buddy Icon"))),"gtk-add",((GCallback )choose_buddy_icon_cb),box,0,0,0);
    menu_item = pidgin_new_item_from_stock((box -> icon_box_menu),((const char *)(dgettext("pidgin","Remove"))),"gtk-remove",((GCallback )remove_buddy_icon_cb),box,0,0,0);
    if (!((path = purple_prefs_get_path("/pidgin/accounts/buddyicon")) != 0) || !(( *path) != 0)) 
      gtk_widget_set_sensitive(menu_item,0);
    gtk_menu_popup(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)(box -> icon_box_menu)),gtk_menu_get_type()))),0,0,0,0,(event -> button),(event -> time));
  }
  else {
    choose_buddy_icon_cb(widget,box);
  }
  return 0;
}

static void icon_box_dnd_cb(GtkWidget *widget,GdkDragContext *dc,gint x,gint y,GtkSelectionData *sd,guint info,guint t,PidginStatusBox *box)
{
  gchar *name = (gchar *)(sd -> data);
  if (((sd -> length) >= 0) && ((sd -> format) == 8)) {
/* Well, it looks like the drag event was cool.
		 * Let's do something with it */
    if (!(g_ascii_strncasecmp(name,"file://",7) != 0)) {
      GError *converr = (GError *)((void *)0);
      gchar *tmp;
      gchar *rtmp;
      if (!((tmp = g_filename_from_uri(name,0,&converr)) != 0)) {
        purple_debug(PURPLE_DEBUG_ERROR,"buddyicon","%s\n",((converr != 0)?(converr -> message) : "g_filename_from_uri error"));
        return ;
      }
      if (((rtmp = strchr(tmp,13)) != 0) || ((rtmp = strchr(tmp,10)) != 0)) 
         *rtmp = 0;
      icon_choose_cb(tmp,box);
      g_free(tmp);
    }
    gtk_drag_finish(dc,(!0),0,t);
  }
  gtk_drag_finish(dc,0,0,t);
}

static void statusbox_got_url(PurpleUtilFetchUrlData *url_data,gpointer user_data,const gchar *themedata,size_t len,const gchar *error_message)
{
  FILE *f;
  gchar *path;
  size_t wc;
  if ((error_message != ((const gchar *)((void *)0))) || (len == 0)) 
    return ;
  f = purple_mkstemp(&path,(!0));
  wc = fwrite(themedata,len,1,f);
  if (wc != 1) {
    purple_debug_warning("theme_got_url","Unable to write theme data.\n");
    fclose(f);
    g_unlink(path);
    g_free(path);
    return ;
  }
  fclose(f);
  icon_choose_cb(path,user_data);
  g_unlink(path);
  g_free(path);
}

static gboolean statusbox_uri_handler(const char *proto,const char *cmd,GHashTable *params,void *data)
{
  const char *src;
  if (g_ascii_strcasecmp(proto,"aim") != 0) 
    return 0;
  if (g_ascii_strcasecmp(cmd,"buddyicon") != 0) 
    return 0;
  src = (g_hash_table_lookup(params,"account"));
  if (src == ((const char *)((void *)0))) 
    return 0;
  purple_util_fetch_url_request(src,(!0),0,0,0,0,statusbox_got_url,data);;
  return (!0);
}

static gboolean icon_box_enter_cb(GtkWidget *widget,GdkEventCrossing *event,PidginStatusBox *box)
{
  gdk_window_set_cursor((widget -> window),(box -> hand_cursor));
  gtk_image_set_from_pixbuf(((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)(box -> icon)),gtk_image_get_type()))),(box -> buddy_icon_hover));
  return 0;
}

static gboolean icon_box_leave_cb(GtkWidget *widget,GdkEventCrossing *event,PidginStatusBox *box)
{
  gdk_window_set_cursor((widget -> window),(box -> arrow_cursor));
  gtk_image_set_from_pixbuf(((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)(box -> icon)),gtk_image_get_type()))),(box -> buddy_icon));
  return 0;
}
static const GtkTargetEntry dnd_targets[] = {{("text/plain"), (0), (0)}, {("text/uri-list"), (0), (1)}, {("STRING"), (0), (2)}};

static void setup_icon_box(PidginStatusBox *status_box)
{
  if ((status_box -> icon_box) != ((GtkWidget *)((void *)0))) 
    return ;
  status_box -> icon = gtk_image_new();
  status_box -> icon_box = gtk_event_box_new();
  gtk_widget_set_parent((status_box -> icon_box),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type()))));
  gtk_widget_show((status_box -> icon_box));
#if GTK_CHECK_VERSION(2,12,0)
  gtk_widget_set_tooltip_text((status_box -> icon_box),(((status_box -> account) != 0)?((const char *)(dgettext("pidgin","Click to change your buddyicon for this account."))) : ((const char *)(dgettext("pidgin","Click to change your buddyicon for all accounts.")))));
#endif
  if (((status_box -> account) != 0) && !(purple_account_get_bool((status_box -> account),"use-global-buddyicon",(!0)) != 0)) {
    PurpleStoredImage *img = purple_buddy_icons_find_account_icon((status_box -> account));
    pidgin_status_box_set_buddy_icon(status_box,img);
    purple_imgstore_unref(img);
  }
  else {
    const char *filename = purple_prefs_get_path("/pidgin/accounts/buddyicon");
    PurpleStoredImage *img = (PurpleStoredImage *)((void *)0);
    if ((filename != 0) && (( *filename) != 0)) 
      img = purple_imgstore_new_from_file(filename);
    pidgin_status_box_set_buddy_icon(status_box,img);
    if (img != 0) 
/*
			 * purple_imgstore_new gives us a reference and
			 * pidgin_status_box_set_buddy_icon also takes one.
			 */
      purple_imgstore_unref(img);
  }
  status_box -> hand_cursor = gdk_cursor_new(GDK_HAND2);
  status_box -> arrow_cursor = gdk_cursor_new(GDK_LEFT_PTR);
/* Set up DND */
  gtk_drag_dest_set((status_box -> icon_box),(GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_DROP),dnd_targets,(sizeof(dnd_targets) / sizeof(GtkTargetEntry )),GDK_ACTION_COPY);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> icon_box)),((GType )(20 << 2))))),"drag_data_received",((GCallback )icon_box_dnd_cb),status_box,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> icon_box)),((GType )(20 << 2))))),"enter-notify-event",((GCallback )icon_box_enter_cb),status_box,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> icon_box)),((GType )(20 << 2))))),"leave-notify-event",((GCallback )icon_box_leave_cb),status_box,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> icon_box)),((GType )(20 << 2))))),"button-press-event",((GCallback )icon_box_press_cb),status_box,0,((GConnectFlags )0));
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> icon_box)),gtk_container_get_type()))),(status_box -> icon));
  gtk_widget_show((status_box -> icon));
}

static void destroy_icon_box(PidginStatusBox *statusbox)
{
  if ((statusbox -> icon_box) == ((GtkWidget *)((void *)0))) 
    return ;
  gtk_widget_destroy((statusbox -> icon_box));
  gdk_cursor_unref((statusbox -> hand_cursor));
  gdk_cursor_unref((statusbox -> arrow_cursor));
  purple_imgstore_unref((statusbox -> buddy_icon_img));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(statusbox -> buddy_icon)),((GType )(20 << 2))))));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(statusbox -> buddy_icon_hover)),((GType )(20 << 2))))));
  if ((statusbox -> buddy_icon_sel) != 0) 
    gtk_widget_destroy((statusbox -> buddy_icon_sel));
  if ((statusbox -> icon_box_menu) != 0) 
    gtk_widget_destroy((statusbox -> icon_box_menu));
  statusbox -> icon = ((GtkWidget *)((void *)0));
  statusbox -> icon_box = ((GtkWidget *)((void *)0));
  statusbox -> icon_box_menu = ((GtkWidget *)((void *)0));
  statusbox -> buddy_icon_img = ((PurpleStoredImage *)((void *)0));
  statusbox -> buddy_icon = ((GdkPixbuf *)((void *)0));
  statusbox -> buddy_icon_hover = ((GdkPixbuf *)((void *)0));
  statusbox -> hand_cursor = ((GdkCursor *)((void *)0));
  statusbox -> arrow_cursor = ((GdkCursor *)((void *)0));
}

static void pidgin_status_box_set_property(GObject *object,guint param_id,const GValue *value,GParamSpec *pspec)
{
  PidginStatusBox *statusbox = (PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)object),pidgin_status_box_get_type()));
  switch(param_id){
    case PROP_ICON_SEL:
{
      if (g_value_get_boolean(value) != 0) {
        if ((statusbox -> account) != 0) {
          PurplePlugin *plug = purple_plugins_find_with_id(purple_account_get_protocol_id((statusbox -> account)));
          if (plug != 0) {
            PurplePluginProtocolInfo *prplinfo = (PurplePluginProtocolInfo *)( *(plug -> info)).extra_info;
            if ((prplinfo != 0) && (prplinfo -> icon_spec.format != ((char *)((void *)0)))) 
              setup_icon_box(statusbox);
          }
        }
        else {
          setup_icon_box(statusbox);
        }
      }
      else {
        destroy_icon_box(statusbox);
      }
      break; 
    }
    case PROP_ACCOUNT:
{
      statusbox -> account = (g_value_get_pointer(value));
      if ((statusbox -> account) != 0) 
        statusbox -> token_status_account = ((PurpleAccount *)((void *)0));
      else 
        statusbox -> token_status_account = check_active_accounts_for_identical_statuses();
      pidgin_status_box_regenerate(statusbox,(!0));
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = param_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","gtkstatusbox.c:568","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void pidgin_status_box_finalize(GObject *obj)
{
  PidginStatusBox *statusbox = (PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)obj),pidgin_status_box_get_type()));
  int i;
  purple_signals_disconnect_by_handle(statusbox);
  purple_prefs_disconnect_by_handle(statusbox);
  destroy_icon_box(statusbox);
  if ((statusbox -> active_row) != 0) 
    gtk_tree_row_reference_free((statusbox -> active_row));
  for (i = 0; i < sizeof(statusbox -> connecting_pixbufs) / sizeof((statusbox -> connecting_pixbufs)[0]); i++) {
    if ((statusbox -> connecting_pixbufs)[i] != ((GdkPixbuf *)((void *)0))) 
      g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(statusbox -> connecting_pixbufs)[i]),((GType )(20 << 2))))));
  }
  for (i = 0; i < sizeof(statusbox -> typing_pixbufs) / sizeof((statusbox -> typing_pixbufs)[0]); i++) {
    if ((statusbox -> typing_pixbufs)[i] != ((GdkPixbuf *)((void *)0))) 
      g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(statusbox -> typing_pixbufs)[i]),((GType )(20 << 2))))));
  }
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(statusbox -> store)),((GType )(20 << 2))))));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(statusbox -> dropdown_store)),((GType )(20 << 2))))));
  ( *( *((GObjectClass *)(g_type_check_class_cast(((GTypeClass *)parent_class),((GType )(20 << 2)))))).finalize)(obj);
}

static GType pidgin_status_box_child_type(GtkContainer *container)
{
  return gtk_widget_get_type();
}

static void pidgin_status_box_class_init(PidginStatusBoxClass *klass)
{
  GObjectClass *object_class;
  GtkWidgetClass *widget_class;
  GtkContainerClass *container_class = (GtkContainerClass *)klass;
  parent_class = (g_type_class_peek_parent(klass));
  widget_class = ((GtkWidgetClass *)klass);
  widget_class -> size_request = pidgin_status_box_size_request;
  widget_class -> size_allocate = pidgin_status_box_size_allocate;
  widget_class -> expose_event = pidgin_status_box_expose_event;
  container_class -> child_type = pidgin_status_box_child_type;
  container_class -> forall = pidgin_status_box_forall;
  container_class -> remove = ((void (*)(GtkContainer *, GtkWidget *))((void *)0));
  object_class = ((GObjectClass *)klass);
  object_class -> finalize = pidgin_status_box_finalize;
  object_class -> get_property = pidgin_status_box_get_property;
  object_class -> set_property = pidgin_status_box_set_property;
  g_object_class_install_property(object_class,PROP_ACCOUNT,g_param_spec_pointer("account","Account","The account, or NULL for all accounts",(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(object_class,PROP_ICON_SEL,g_param_spec_boolean("iconsel","Icon Selector","Whether the icon selector should be displayed or not.",0,(G_PARAM_READABLE | G_PARAM_WRITABLE)));
}
/**
 * This updates the text displayed on the status box so that it shows
 * the current status.  This is the only function in this file that
 * should modify status_box->store
 */

static void pidgin_status_box_refresh(PidginStatusBox *status_box)
{
  GtkStyle *style;
  char aa_color[8UL];
  PurpleSavedStatus *saved_status;
  char *primary;
  char *secondary;
  char *text;
  const char *stock = (const char *)((void *)0);
  GdkPixbuf *emblem = (GdkPixbuf *)((void *)0);
  GtkTreePath *path;
  gboolean account_status = 0;
  PurpleAccount *acct = ((status_box -> account) != 0)?(status_box -> account) : (status_box -> token_status_account);
  style = gtk_widget_get_style(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type()))));
  snprintf(aa_color,(sizeof(aa_color)),"#%02x%02x%02x",((style -> text_aa)[GTK_STATE_NORMAL].red >> 8),((style -> text_aa)[GTK_STATE_NORMAL].green >> 8),((style -> text_aa)[GTK_STATE_NORMAL].blue >> 8));
  saved_status = purple_savedstatus_get_current();
  if (((status_box -> account) != 0) || (((status_box -> token_status_account) != 0) && (purple_savedstatus_is_transient(saved_status) != 0))) 
    account_status = (!0);
/* Primary */
  if ((status_box -> typing) != 0) {
    GtkTreeIter iter;
    PidginStatusBoxItemType type;
    gpointer data;
/* Primary (get the status selected in the dropdown) */
    path = gtk_tree_row_reference_get_path((status_box -> active_row));
    if (!(gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> dropdown_store)),gtk_tree_model_get_type()))),&iter,path) != 0)) 
      return ;
    gtk_tree_path_free(path);
    gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> dropdown_store)),gtk_tree_model_get_type()))),&iter,TYPE_COLUMN,&type,DATA_COLUMN,&data,-1);
    if (type == PIDGIN_STATUS_BOX_TYPE_PRIMITIVE) 
      primary = g_strdup(purple_primitive_get_name_from_type(((gint )((glong )data))));
    else 
/* This should never happen, but just in case... */
      primary = g_strdup("New status");
  }
  else if (account_status != 0) 
    primary = g_strdup(purple_status_get_name((purple_account_get_active_status(acct))));
  else if (purple_savedstatus_is_transient(saved_status) != 0) 
    primary = g_strdup(purple_primitive_get_name_from_type(purple_savedstatus_get_type(saved_status)));
  else 
    primary = g_markup_escape_text(purple_savedstatus_get_title(saved_status),(-1));
/* Secondary */
  if ((status_box -> typing) != 0) 
    secondary = g_strdup(((const char *)(dgettext("pidgin","Typing"))));
  else if ((status_box -> connecting) != 0) 
    secondary = g_strdup(((const char *)(dgettext("pidgin","Connecting"))));
  else if (!((status_box -> network_available) != 0)) 
    secondary = g_strdup(((const char *)(dgettext("pidgin","Waiting for network connection"))));
  else if (purple_savedstatus_is_transient(saved_status) != 0) 
    secondary = ((char *)((void *)0));
  else {
    const char *message;
    char *tmp;
    message = purple_savedstatus_get_message(saved_status);
    if (message != ((const char *)((void *)0))) {
      tmp = purple_markup_strip_html(message);
      purple_util_chrreplace(tmp,10,32);
      secondary = g_markup_escape_text(tmp,(-1));
      g_free(tmp);
    }
    else 
      secondary = ((char *)((void *)0));
  }
/* Pixbuf */
  if ((status_box -> typing) != 0) 
    stock = typing_stock_ids[status_box -> typing_index];
  else if ((status_box -> connecting) != 0) 
    stock = connecting_stock_ids[status_box -> connecting_index];
  else {
    PurpleStatusType *status_type;
    PurpleStatusPrimitive prim;
    if (account_status != 0) {
      status_type = purple_status_get_type((purple_account_get_active_status(acct)));
      prim = purple_status_type_get_primitive(status_type);
    }
    else {
      prim = purple_savedstatus_get_type(saved_status);
    }
    stock = pidgin_stock_id_from_status_primitive(prim);
  }
  if ((status_box -> account) != ((PurpleAccount *)((void *)0))) {
    text = g_strdup_printf("%s - <span size=\"smaller\" color=\"%s\">%s</span>",purple_account_get_username((status_box -> account)),aa_color,((secondary != 0)?secondary : primary));
    emblem = pidgin_create_prpl_icon((status_box -> account),PIDGIN_PRPL_ICON_SMALL);
  }
  else if (secondary != ((char *)((void *)0))) {
    text = g_strdup_printf("%s<span size=\"smaller\" color=\"%s\"> - %s</span>",primary,aa_color,secondary);
  }
  else {
    text = g_strdup(primary);
  }
  g_free(primary);
  g_free(secondary);
/*
	 * Only two columns are used in this list store (does it
	 * really need to be a list store?)
	 */
  gtk_list_store_set((status_box -> store),&status_box -> iter,ICON_STOCK_COLUMN,((gpointer )stock),TEXT_COLUMN,text,EMBLEM_COLUMN,emblem,EMBLEM_VISIBLE_COLUMN,(emblem != ((GdkPixbuf *)((void *)0))),-1);
  g_free(text);
  if (emblem != 0) 
    g_object_unref(emblem);
/* Make sure to activate the only row in the tree view */
  path = gtk_tree_path_new_from_string("0");
  gtk_cell_view_set_displayed_row(((GtkCellView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> cell_view)),gtk_cell_view_get_type()))),path);
  gtk_tree_path_free(path);
  update_size(status_box);
}

static PurpleStatusType *find_status_type_by_index(const PurpleAccount *account,gint active)
{
  GList *l = purple_account_get_status_types(account);
  gint i;
  for (i = 0; l != 0; l = (l -> next)) {{
      PurpleStatusType *status_type = (l -> data);
      if (!(purple_status_type_is_user_settable(status_type) != 0) || (purple_status_type_is_independent(status_type) != 0)) 
        continue; 
      if (active == i) 
        return status_type;
      i++;
    }
  }
  return 0;
}
/**
 * This updates the GtkTreeView so that it correctly shows the state
 * we are currently using.  It is used when the current state is
 * updated from somewhere other than the GtkStatusBox (from a plugin,
 * or when signing on with the "-n" option, for example).  It is
 * also used when the user selects the "New..." option.
 *
 * Maybe we could accomplish this by triggering off the mouse and
 * keyboard signals instead of the changed signal?
 */

static void status_menu_refresh_iter(PidginStatusBox *status_box,gboolean status_changed)
{
  PurpleSavedStatus *saved_status;
  PurpleStatusPrimitive primitive;
  gint index;
  const char *message;
  GtkTreePath *path = (GtkTreePath *)((void *)0);
/* this function is inappropriate for ones with accounts */
  if ((status_box -> account) != 0) 
    return ;
  saved_status = purple_savedstatus_get_current();
/*
	 * Suppress the "changed" signal because the status
	 * was changed programmatically.
	 */
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type()))),0);
/*
	 * If there is a token-account, then select the primitive from the
	 * dropdown using a loop. Otherwise select from the default list.
	 */
  primitive = purple_savedstatus_get_type(saved_status);
  if (((!((status_box -> token_status_account) != 0) && (purple_savedstatus_is_transient(saved_status) != 0)) && (((((primitive == PURPLE_STATUS_AVAILABLE) || (primitive == PURPLE_STATUS_AWAY)) || (primitive == PURPLE_STATUS_INVISIBLE)) || (primitive == PURPLE_STATUS_OFFLINE)) || (primitive == PURPLE_STATUS_UNAVAILABLE))) && !(purple_savedstatus_has_substatuses(saved_status) != 0)) {
    index = get_statusbox_index(status_box,saved_status);
    path = gtk_tree_path_new_from_indices(index,-1);
  }
  else {
    GtkTreeIter iter;
    PidginStatusBoxItemType type;
    gpointer data;
/* If this saved status is in the list store, then set it as the active item */
    if (gtk_tree_model_get_iter_first(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> dropdown_store)),gtk_tree_model_get_type()))),&iter) != 0) {{
        do {
          gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> dropdown_store)),gtk_tree_model_get_type()))),&iter,TYPE_COLUMN,&type,DATA_COLUMN,&data,-1);
/* This is a special case because Primitives for the token_status_account are actually
				 * saved statuses with substatuses for the enabled accounts */
          if (((((status_box -> token_status_account) != 0) && (purple_savedstatus_is_transient(saved_status) != 0)) && (type == PIDGIN_STATUS_BOX_TYPE_PRIMITIVE)) && (primitive == ((gint )((glong )data)))) {
            char *name;
            const char *acct_status_name = purple_status_get_name((purple_account_get_active_status((status_box -> token_status_account))));
            gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> dropdown_store)),gtk_tree_model_get_type()))),&iter,TEXT_COLUMN,&name,-1);
            if (!(purple_savedstatus_has_substatuses(saved_status) != 0) || !(strcmp(name,acct_status_name) != 0)) {
/* Found! */
              path = gtk_tree_model_get_path(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> dropdown_store)),gtk_tree_model_get_type()))),&iter);
              g_free(name);
              break; 
            }
            g_free(name);
          }
          else if ((type == PIDGIN_STATUS_BOX_TYPE_POPULAR) && (((gint )((glong )data)) == purple_savedstatus_get_creation_time(saved_status))) {
/* Found! */
            path = gtk_tree_model_get_path(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> dropdown_store)),gtk_tree_model_get_type()))),&iter);
            break; 
          }
        }while (gtk_tree_model_iter_next(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> dropdown_store)),gtk_tree_model_get_type()))),&iter) != 0);
      }
    }
  }
  if ((status_box -> active_row) != 0) 
    gtk_tree_row_reference_free((status_box -> active_row));
/* path should never be NULL */
  if (path != 0) {
    status_box -> active_row = gtk_tree_row_reference_new(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> dropdown_store)),gtk_tree_model_get_type()))),path);
    gtk_tree_path_free(path);
  }
  else 
    status_box -> active_row = ((GtkTreeRowReference *)((void *)0));
  if (status_changed != 0) {
    message = purple_savedstatus_get_message(saved_status);
/*
		 * If we are going to hide the imhtml, don't retain the
		 * message because showing the old message later is
		 * confusing. If we are going to set the message to a pre-set,
		 * then we need to do this anyway
		 *
		 * Suppress the "changed" signal because the status
		 * was changed programmatically.
		 */
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_widget_get_type()))),0);
    gtk_imhtml_delete(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_imhtml_get_type()))),0,0);
    gtk_imhtml_clear_formatting(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_imhtml_get_type()))));
    if ((!(purple_savedstatus_is_transient(saved_status) != 0) || !(message != 0)) || !(( *message) != 0)) {
      status_box -> imhtml_visible = 0;
      gtk_widget_hide_all((status_box -> vbox));
    }
    else {
      status_box -> imhtml_visible = (!0);
      gtk_widget_show_all((status_box -> vbox));
      gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_imhtml_get_type()))),message,0,0);
    }
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_widget_get_type()))),(!0));
    update_size(status_box);
  }
/* Stop suppressing the "changed" signal. */
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type()))),(!0));
}

static void add_popular_statuses(PidginStatusBox *statusbox)
{
  GList *list;
  GList *cur;
  list = purple_savedstatuses_get_popular(6);
  if (list == ((GList *)((void *)0))) 
/* Odd... oh well, nothing we can do about it. */
    return ;
  pidgin_status_box_add_separator(statusbox);
  for (cur = list; cur != ((GList *)((void *)0)); cur = (cur -> next)) {
    PurpleSavedStatus *saved = (cur -> data);
    const gchar *message;
    gchar *stripped = (gchar *)((void *)0);
    PidginStatusBoxItemType type;
    if (purple_savedstatus_is_transient(saved) != 0) {
/*
			 * Transient statuses do not have a title, so the savedstatus
			 * API returns the message when purple_savedstatus_get_title() is
			 * called, so we don't need to get the message a second time.
			 */
      type = PIDGIN_STATUS_BOX_TYPE_POPULAR;
    }
    else {
      type = PIDGIN_STATUS_BOX_TYPE_SAVED_POPULAR;
      message = purple_savedstatus_get_message(saved);
      if (message != ((const gchar *)((void *)0))) {
        stripped = purple_markup_strip_html(message);
        purple_util_chrreplace(stripped,10,32);
      }
    }
    pidgin_status_box_add(statusbox,type,0,purple_savedstatus_get_title(saved),stripped,((gpointer )(purple_savedstatus_get_creation_time(saved))));
    g_free(stripped);
  }
  g_list_free(list);
}
/* This returns NULL if the active accounts don't have identical
 * statuses and a token account if they do */

static PurpleAccount *check_active_accounts_for_identical_statuses()
{
  GList *iter;
  GList *active_accts = purple_accounts_get_all_active();
  PurpleAccount *acct1 = (PurpleAccount *)((void *)0);
  const char *prpl1 = (const char *)((void *)0);
  if (active_accts != 0) {
    acct1 = (active_accts -> data);
    prpl1 = purple_account_get_protocol_id(acct1);
  }
  else {
/* there's no enabled account */
    return 0;
  }
{
/* start at the second account */
    for (iter = (active_accts -> next); iter != 0; iter = (iter -> next)) {
      PurpleAccount *acct2 = (iter -> data);
      GList *s1;
      GList *s2;
      if (!(g_str_equal(prpl1,(purple_account_get_protocol_id(acct2))) != 0)) {
        acct1 = ((PurpleAccount *)((void *)0));
        break; 
      }
{
        for (((s1 = purple_account_get_status_types(acct1)) , (s2 = purple_account_get_status_types(acct2))); (s1 != 0) && (s2 != 0); ((s1 = (s1 -> next)) , (s2 = (s2 -> next)))) {
          PurpleStatusType *st1 = (s1 -> data);
          PurpleStatusType *st2 = (s2 -> data);
/* TODO: Are these enough to consider the statuses identical? */
          if ((((purple_status_type_get_primitive(st1)) != (purple_status_type_get_primitive(st2))) || (strcmp(purple_status_type_get_id(st1),purple_status_type_get_id(st2)) != 0)) || (strcmp(purple_status_type_get_name(st1),purple_status_type_get_name(st2)) != 0)) {
            acct1 = ((PurpleAccount *)((void *)0));
            break; 
          }
        }
      }
/* Will both be NULL if matched */
      if (s1 != s2) {
        acct1 = ((PurpleAccount *)((void *)0));
        break; 
      }
    }
  }
  g_list_free(active_accts);
  return acct1;
}

static void add_account_statuses(PidginStatusBox *status_box,PurpleAccount *account)
{
/* Per-account */
  GList *l;
  for (l = purple_account_get_status_types(account); l != ((GList *)((void *)0)); l = (l -> next)) {{
      PurpleStatusType *status_type = (PurpleStatusType *)(l -> data);
      PurpleStatusPrimitive prim;
      if (!(purple_status_type_is_user_settable(status_type) != 0) || (purple_status_type_is_independent(status_type) != 0)) 
        continue; 
      prim = purple_status_type_get_primitive(status_type);
      pidgin_status_box_add(((PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)status_box),pidgin_status_box_get_type()))),PIDGIN_STATUS_BOX_TYPE_PRIMITIVE,0,purple_status_type_get_name(status_type),0,((gpointer )((glong )prim)));
    }
  }
}

static void pidgin_status_box_regenerate(PidginStatusBox *status_box,gboolean status_changed)
{
/* Unset the model while clearing it */
  gtk_tree_view_set_model(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> tree_view)),gtk_tree_view_get_type()))),0);
  gtk_list_store_clear((status_box -> dropdown_store));
/* Don't set the model until the new statuses have been added to the box.
	 * What is presumably a bug in Gtk < 2.4 causes things to get all confused
	 * if we do this here. */
/* gtk_combo_box_set_model(GTK_COMBO_BOX(status_box), GTK_TREE_MODEL(status_box->dropdown_store)); */
  if ((status_box -> account) == ((PurpleAccount *)((void *)0))) {
/* Do all the currently enabled accounts have the same statuses?
		 * If so, display them instead of our global list.
		 */
    if ((status_box -> token_status_account) != 0) {
      add_account_statuses(status_box,(status_box -> token_status_account));
    }
    else {
/* Global */
      pidgin_status_box_add(((PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)status_box),pidgin_status_box_get_type()))),PIDGIN_STATUS_BOX_TYPE_PRIMITIVE,0,((const char *)(dgettext("pidgin","Available"))),0,((gpointer )((gpointer )((glong )PURPLE_STATUS_AVAILABLE))));
      pidgin_status_box_add(((PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)status_box),pidgin_status_box_get_type()))),PIDGIN_STATUS_BOX_TYPE_PRIMITIVE,0,((const char *)(dgettext("pidgin","Away"))),0,((gpointer )((gpointer )((glong )PURPLE_STATUS_AWAY))));
      pidgin_status_box_add(((PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)status_box),pidgin_status_box_get_type()))),PIDGIN_STATUS_BOX_TYPE_PRIMITIVE,0,((const char *)(dgettext("pidgin","Do not disturb"))),0,((gpointer )((gpointer )((glong )PURPLE_STATUS_UNAVAILABLE))));
      pidgin_status_box_add(((PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)status_box),pidgin_status_box_get_type()))),PIDGIN_STATUS_BOX_TYPE_PRIMITIVE,0,((const char *)(dgettext("pidgin","Invisible"))),0,((gpointer )((gpointer )((glong )PURPLE_STATUS_INVISIBLE))));
      pidgin_status_box_add(((PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)status_box),pidgin_status_box_get_type()))),PIDGIN_STATUS_BOX_TYPE_PRIMITIVE,0,((const char *)(dgettext("pidgin","Offline"))),0,((gpointer )((gpointer )((glong )PURPLE_STATUS_OFFLINE))));
    }
    add_popular_statuses(status_box);
    pidgin_status_box_add_separator(((PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)status_box),pidgin_status_box_get_type()))));
    pidgin_status_box_add(((PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)status_box),pidgin_status_box_get_type()))),PIDGIN_STATUS_BOX_TYPE_CUSTOM,0,((const char *)(dgettext("pidgin","New status..."))),0,0);
    pidgin_status_box_add(((PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)status_box),pidgin_status_box_get_type()))),PIDGIN_STATUS_BOX_TYPE_SAVED,0,((const char *)(dgettext("pidgin","Saved statuses..."))),0,0);
    status_menu_refresh_iter(status_box,status_changed);
    pidgin_status_box_refresh(status_box);
  }
  else {
    add_account_statuses(status_box,(status_box -> account));
    update_to_reflect_account_status(status_box,(status_box -> account),purple_account_get_active_status((status_box -> account)));
  }
  gtk_tree_view_set_model(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> tree_view)),gtk_tree_view_get_type()))),((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> dropdown_store)),gtk_tree_model_get_type()))));
  gtk_tree_view_set_search_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> tree_view)),gtk_tree_view_get_type()))),TEXT_COLUMN);
}

static gboolean combo_box_scroll_event_cb(GtkWidget *w,GdkEventScroll *event,GtkIMHtml *imhtml)
{
  pidgin_status_box_popup(((PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)w),pidgin_status_box_get_type()))));
  return (!0);
}

static gboolean imhtml_scroll_event_cb(GtkWidget *w,GdkEventScroll *event,GtkIMHtml *imhtml)
{
  if ((event -> direction) == GDK_SCROLL_UP) 
    gtk_imhtml_page_up(imhtml);
  else if ((event -> direction) == GDK_SCROLL_DOWN) 
    gtk_imhtml_page_down(imhtml);
  return (!0);
}

static gboolean imhtml_remove_focus(GtkWidget *w,GdkEventKey *event,PidginStatusBox *status_box)
{
  if ((((event -> keyval) == 0xff09) || ((event -> keyval) == 0xff89)) || ((event -> keyval) == 0xfe20)) {
/* If last inserted character is a tab, then remove the focus from here */
    GtkWidget *top = gtk_widget_get_toplevel(w);
    g_signal_emit_by_name(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)top),((GType )(20 << 2))))),"move_focus",((((event -> state) & GDK_SHIFT_MASK) != 0U)?GTK_DIR_TAB_BACKWARD : GTK_DIR_TAB_FORWARD));
    return (!0);
  }
  if ((status_box -> typing) == 0) 
    return 0;
/* Reset the status if Escape was pressed */
  if ((event -> keyval) == 0xff1b) {
    purple_timeout_remove((status_box -> typing));
    status_box -> typing = 0;
    gtk_imhtml_set_populate_primary_clipboard(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_imhtml_get_type()))),(!0));
    if ((status_box -> account) != ((PurpleAccount *)((void *)0))) 
      update_to_reflect_account_status(status_box,(status_box -> account),purple_account_get_active_status((status_box -> account)));
    else {
      status_menu_refresh_iter(status_box,(!0));
      pidgin_status_box_refresh(status_box);
    }
    return (!0);
  }
  pidgin_status_box_pulse_typing(status_box);
  purple_timeout_remove((status_box -> typing));
  status_box -> typing = purple_timeout_add_seconds(4,((GSourceFunc )remove_typing_cb),status_box);
  return 0;
}

static gboolean dropdown_store_row_separator_func(GtkTreeModel *model,GtkTreeIter *iter,gpointer data)
{
  PidginStatusBoxItemType type;
  gtk_tree_model_get(model,iter,TYPE_COLUMN,&type,-1);
  if (type == PIDGIN_STATUS_BOX_TYPE_SEPARATOR) 
    return (!0);
  return 0;
}

static void cache_pixbufs(PidginStatusBox *status_box)
{
  GtkIconSize icon_size;
  int i;
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> icon_rend)),((GType )(20 << 2))))),"xpad",3,((void *)((void *)0)));
  icon_size = gtk_icon_size_from_name("pidgin-icon-size-tango-extra-small");
  for (i = 0; i < sizeof(status_box -> connecting_pixbufs) / sizeof((status_box -> connecting_pixbufs)[0]); i++) {
    if ((status_box -> connecting_pixbufs)[i] != ((GdkPixbuf *)((void *)0))) 
      g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> connecting_pixbufs)[i]),((GType )(20 << 2))))));
    if (connecting_stock_ids[i] != 0) 
      (status_box -> connecting_pixbufs)[i] = gtk_widget_render_icon(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> vbox)),gtk_widget_get_type()))),connecting_stock_ids[i],icon_size,"PidginStatusBox");
    else 
      (status_box -> connecting_pixbufs)[i] = ((GdkPixbuf *)((void *)0));
  }
  status_box -> connecting_index = 0;
  for (i = 0; i < sizeof(status_box -> typing_pixbufs) / sizeof((status_box -> typing_pixbufs)[0]); i++) {
    if ((status_box -> typing_pixbufs)[i] != ((GdkPixbuf *)((void *)0))) 
      g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> typing_pixbufs)[i]),((GType )(20 << 2))))));
    if (typing_stock_ids[i] != 0) 
      (status_box -> typing_pixbufs)[i] = gtk_widget_render_icon(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> vbox)),gtk_widget_get_type()))),typing_stock_ids[i],icon_size,"PidginStatusBox");
    else 
      (status_box -> typing_pixbufs)[i] = ((GdkPixbuf *)((void *)0));
  }
  status_box -> typing_index = 0;
}

static void account_enabled_cb(PurpleAccount *acct,PidginStatusBox *status_box)
{
  PurpleAccount *initial_token_acct = (status_box -> token_status_account);
  if ((status_box -> account) != 0) 
    return ;
  status_box -> token_status_account = check_active_accounts_for_identical_statuses();
/* Regenerate the list if it has changed */
  if (initial_token_acct != (status_box -> token_status_account)) {
    pidgin_status_box_regenerate(status_box,(!0));
  }
}

static void current_savedstatus_changed_cb(PurpleSavedStatus *now,PurpleSavedStatus *old,PidginStatusBox *status_box)
{
/* Make sure our current status is added to the list of popular statuses */
  pidgin_status_box_regenerate(status_box,(!0));
}

static void saved_status_updated_cb(PurpleSavedStatus *status,PidginStatusBox *status_box)
{
  pidgin_status_box_regenerate(status_box,(purple_savedstatus_get_current() == status));
}

static void spellcheck_prefs_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
#ifdef USE_GTKSPELL
  PidginStatusBox *status_box = (PidginStatusBox *)data;
  if (value != 0) 
    pidgin_setup_gtkspell(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_text_view_get_type()))));
  else {
    GtkSpell *spell;
    spell = gtkspell_get_from_text_view(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_text_view_get_type()))));
    gtkspell_detach(spell);
  }
#endif
}
#if 0
/* Disabled until button_released_cb works */
#if 0
#endif
#endif

static void pidgin_status_box_list_position(PidginStatusBox *status_box,int *x,int *y,int *width,int *height)
{
  GdkScreen *screen;
  gint monitor_num;
  GdkRectangle monitor;
  GtkRequisition popup_req;
  GtkPolicyType hpolicy;
  GtkPolicyType vpolicy;
  gdk_window_get_origin(( *((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type())))).window,x,y);
   *x += ( *((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type())))).allocation.x;
   *y += ( *((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type())))).allocation.y;
   *width = ( *((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type())))).allocation.width;
  hpolicy = (vpolicy = GTK_POLICY_NEVER);
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> scrolled_window)),((GType )(20 << 2))))),"hscrollbar-policy",hpolicy,"vscrollbar-policy",vpolicy,((void *)((void *)0)));
  gtk_widget_size_request((status_box -> popup_frame),&popup_req);
  if (popup_req.width >  *width) {
    hpolicy = GTK_POLICY_ALWAYS;
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> scrolled_window)),((GType )(20 << 2))))),"hscrollbar-policy",hpolicy,"vscrollbar-policy",vpolicy,((void *)((void *)0)));
    gtk_widget_size_request((status_box -> popup_frame),&popup_req);
  }
   *height = popup_req.height;
  screen = gtk_widget_get_screen(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type()))));
  monitor_num = gdk_screen_get_monitor_at_window(screen,( *((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type())))).window);
  gdk_screen_get_monitor_geometry(screen,monitor_num,&monitor);
  if ( *x < monitor.x) 
     *x = monitor.x;
  else if (( *x +  *width) > (monitor.x + monitor.width)) 
     *x = ((monitor.x + monitor.width) -  *width);
  if ((( *y + ( *((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type())))).allocation.height) +  *height) <= (monitor.y + monitor.height)) 
     *y += ( *((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type())))).allocation.height;
  else if (( *y -  *height) >= monitor.y) 
     *y -=  *height;
  else if (((monitor.y + monitor.height) - ( *y + ( *((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type())))).allocation.height)) > ( *y - monitor.y)) {
     *y += ( *((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type())))).allocation.height;
     *height = ((monitor.y + monitor.height) -  *y);
  }
  else {
     *height = ( *y - monitor.y);
     *y = monitor.y;
  }
  if (popup_req.height >  *height) {
    vpolicy = GTK_POLICY_ALWAYS;
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> scrolled_window)),((GType )(20 << 2))))),"hscrollbar-policy",hpolicy,"vscrollbar-policy",vpolicy,((void *)((void *)0)));
  }
}

static gboolean popup_grab_on_window(GdkWindow *window,guint32 activate_time,gboolean grab_keyboard)
{
  if ((gdk_pointer_grab(window,(!0),(GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_POINTER_MOTION_MASK),0,0,activate_time)) == 0) {
    if (!(grab_keyboard != 0) || ((gdk_keyboard_grab(window,(!0),activate_time)) == 0)) 
      return (!0);
    else {
      gdk_display_pointer_ungrab(gdk_drawable_get_display(window),activate_time);
      return 0;
    }
  }
  return 0;
}

static void pidgin_status_box_popup(PidginStatusBox *box)
{
  int width;
  int height;
  int x;
  int y;
  pidgin_status_box_list_position(box,&x,&y,&width,&height);
  gtk_widget_set_size_request((box -> popup_window),width,height);
  gtk_window_move(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(box -> popup_window)),gtk_window_get_type()))),x,y);
  gtk_widget_show((box -> popup_window));
  gtk_widget_grab_focus((box -> tree_view));
  if (!(popup_grab_on_window(( *(box -> popup_window)).window,0L,(!0)) != 0)) {
    gtk_widget_hide((box -> popup_window));
    return ;
  }
  gtk_grab_add((box -> popup_window));
/*box->popup_in_progress = TRUE;*/
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(box -> toggle_button)),gtk_toggle_button_get_type()))),(!0));
  if ((box -> active_row) != 0) {
    GtkTreePath *path = gtk_tree_row_reference_get_path((box -> active_row));
    gtk_tree_view_set_cursor(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(box -> tree_view)),gtk_tree_view_get_type()))),path,0,0);
    gtk_tree_path_free(path);
  }
}

static void pidgin_status_box_popdown(PidginStatusBox *box)
{
  gtk_widget_hide((box -> popup_window));
  box -> popup_in_progress = 0;
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(box -> toggle_button)),gtk_toggle_button_get_type()))),0);
  gtk_grab_remove((box -> popup_window));
}

static gboolean toggle_key_press_cb(GtkWidget *widget,GdkEventKey *event,PidginStatusBox *box)
{
  switch(event -> keyval){
    case 32:
{
    }
    case 0xff0d:
{
    }
    case 0xff80:
{
    }
    case 0xff8d:
{
      if (!((box -> popup_in_progress) != 0)) {
        pidgin_status_box_popup(box);
        box -> popup_in_progress = (!0);
      }
      else {
        pidgin_status_box_popdown(box);
      }
      return (!0);
    }
    default:
{
      return 0;
    }
  }
}

static gboolean toggled_cb(GtkWidget *widget,GdkEventButton *event,PidginStatusBox *box)
{
  if (!((box -> popup_in_progress) != 0)) 
    pidgin_status_box_popup(box);
  else 
    pidgin_status_box_popdown(box);
  return (!0);
}

static void buddy_icon_set_cb(const char *filename,PidginStatusBox *box)
{
  PurpleStoredImage *img = (PurpleStoredImage *)((void *)0);
  if ((box -> account) != 0) {
    PurplePlugin *plug = purple_find_prpl(purple_account_get_protocol_id((box -> account)));
    if (plug != 0) {
      PurplePluginProtocolInfo *prplinfo = (PurplePluginProtocolInfo *)( *(plug -> info)).extra_info;
      if ((prplinfo != 0) && (prplinfo -> icon_spec.format != 0)) {
        gpointer data = (gpointer )((void *)0);
        size_t len = 0;
        if (filename != 0) 
          data = pidgin_convert_buddy_icon(plug,filename,&len);
        img = purple_buddy_icons_set_account_icon((box -> account),data,len);
        if (img != 0) 
/*
					 * set_account_icon doesn't give us a reference, but we
					 * unref one below (for the other code path)
					 */
          purple_imgstore_ref(img);
        purple_account_set_buddy_icon_path((box -> account),filename);
        purple_account_set_bool((box -> account),"use-global-buddyicon",(filename != ((const char *)((void *)0))));
      }
    }
  }
  else {
    GList *accounts;
    for (accounts = purple_accounts_get_all(); accounts != ((GList *)((void *)0)); accounts = (accounts -> next)) {
      PurpleAccount *account = (accounts -> data);
      PurplePlugin *plug = purple_find_prpl(purple_account_get_protocol_id(account));
      if (plug != 0) {
        PurplePluginProtocolInfo *prplinfo = (PurplePluginProtocolInfo *)( *(plug -> info)).extra_info;
        if (((prplinfo != ((PurplePluginProtocolInfo *)((void *)0))) && (purple_account_get_bool(account,"use-global-buddyicon",(!0)) != 0)) && (prplinfo -> icon_spec.format != 0)) {
          gpointer data = (gpointer )((void *)0);
          size_t len = 0;
          if (filename != 0) 
            data = pidgin_convert_buddy_icon(plug,filename,&len);
          purple_buddy_icons_set_account_icon(account,data,len);
          purple_account_set_buddy_icon_path(account,filename);
        }
      }
    }
/* Even if no accounts were processed, load the icon that was set. */
    if (filename != ((const char *)((void *)0))) 
      img = purple_imgstore_new_from_file(filename);
  }
  pidgin_status_box_set_buddy_icon(box,img);
  if (img != 0) 
    purple_imgstore_unref(img);
}

static void remove_buddy_icon_cb(GtkWidget *w,PidginStatusBox *box)
{
  if ((box -> account) == ((PurpleAccount *)((void *)0))) 
/* The pref-connect callback does the actual work */
    purple_prefs_set_path("/pidgin/accounts/buddyicon",0);
  else 
    buddy_icon_set_cb(0,box);
  gtk_widget_destroy((box -> icon_box_menu));
  box -> icon_box_menu = ((GtkWidget *)((void *)0));
}

static void choose_buddy_icon_cb(GtkWidget *w,PidginStatusBox *box)
{
  if ((box -> buddy_icon_sel) != 0) {
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(box -> buddy_icon_sel)),gtk_window_get_type()))));
  }
  else {
    box -> buddy_icon_sel = pidgin_buddy_icon_chooser_new(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_widget_get_toplevel(w))),gtk_window_get_type()))),icon_choose_cb,box);
    gtk_widget_show_all((box -> buddy_icon_sel));
  }
}

static void icon_choose_cb(const char *filename,gpointer data)
{
  PidginStatusBox *box = data;
  if (filename != 0) {
    if ((box -> account) == ((PurpleAccount *)((void *)0))) 
/* The pref-connect callback does the actual work */
      purple_prefs_set_path("/pidgin/accounts/buddyicon",filename);
    else 
      buddy_icon_set_cb(filename,box);
  }
  box -> buddy_icon_sel = ((GtkWidget *)((void *)0));
}

static void update_buddyicon_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  buddy_icon_set_cb(value,((PidginStatusBox *)data));
}

static void treeview_activate_current_selection(PidginStatusBox *status_box,GtkTreePath *path)
{
  if ((status_box -> active_row) != 0) 
    gtk_tree_row_reference_free((status_box -> active_row));
  status_box -> active_row = gtk_tree_row_reference_new(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> dropdown_store)),gtk_tree_model_get_type()))),path);
  pidgin_status_box_popdown(status_box);
  pidgin_status_box_changed(status_box);
}

static void tree_view_delete_current_selection_cb(gpointer data)
{
  PurpleSavedStatus *saved;
  saved = purple_savedstatus_find_by_creation_time(((gint )((glong )data)));
  do {
    if (saved != ((PurpleSavedStatus *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"saved != NULL");
      return ;
    };
  }while (0);
  if (purple_savedstatus_get_current() != saved) 
    purple_savedstatus_delete_by_status(saved);
}

static void tree_view_delete_current_selection(PidginStatusBox *status_box,GtkTreePath *path)
{
  GtkTreeIter iter;
  gpointer data;
  PurpleSavedStatus *saved;
  gchar *msg;
  if ((status_box -> active_row) != 0) {
/* don't delete active status */
    if (gtk_tree_path_compare(path,(gtk_tree_row_reference_get_path((status_box -> active_row)))) == 0) 
      return ;
  }
  if (!(gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> dropdown_store)),gtk_tree_model_get_type()))),&iter,path) != 0)) 
    return ;
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> dropdown_store)),gtk_tree_model_get_type()))),&iter,DATA_COLUMN,&data,-1);
  saved = purple_savedstatus_find_by_creation_time(((gint )((glong )data)));
  do {
    if (saved != ((PurpleSavedStatus *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"saved != NULL");
      return ;
    };
  }while (0);
  if (saved == purple_savedstatus_get_current()) 
    return ;
  msg = g_strdup_printf(((const char *)(dgettext("pidgin","Are you sure you want to delete %s\?"))),purple_savedstatus_get_title(saved));
  purple_request_action(saved,0,msg,0,0,0,0,0,data,2,((const char *)(dgettext("pidgin","Delete"))),tree_view_delete_current_selection_cb,((const char *)(dgettext("pidgin","Cancel"))),((void *)((void *)0)));
  g_free(msg);
  pidgin_status_box_popdown(status_box);
}

static gboolean treeview_button_release_cb(GtkWidget *widget,GdkEventButton *event,PidginStatusBox *status_box)
{
  GtkTreePath *path = (GtkTreePath *)((void *)0);
  int ret;
  GtkWidget *ewidget = gtk_get_event_widget(((GdkEvent *)event));
  if (ewidget != (status_box -> tree_view)) {
    if (((ewidget == (status_box -> toggle_button)) && ((status_box -> popup_in_progress) != 0)) && (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> toggle_button)),gtk_toggle_button_get_type())))) != 0)) {
      pidgin_status_box_popdown(status_box);
      return (!0);
    }
    else if (ewidget == (status_box -> toggle_button)) {
      status_box -> popup_in_progress = (!0);
    }
/* released outside treeview */
    if (ewidget != (status_box -> toggle_button)) {
      pidgin_status_box_popdown(status_box);
      return (!0);
    }
    return 0;
  }
  ret = gtk_tree_view_get_path_at_pos(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> tree_view)),gtk_tree_view_get_type()))),(event -> x),(event -> y),&path,0,0,0);
  if (!(ret != 0)) 
/* clicked outside window? */
    return (!0);
  treeview_activate_current_selection(status_box,path);
  gtk_tree_path_free(path);
  return (!0);
}

static gboolean treeview_key_press_event(GtkWidget *widget,GdkEventKey *event,PidginStatusBox *box)
{
  if ((box -> popup_in_progress) != 0) {
    if ((event -> keyval) == 0xff1b) {
      pidgin_status_box_popdown(box);
      return (!0);
    }
    else {
      GtkTreeSelection *sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(box -> tree_view)),gtk_tree_view_get_type()))));
      GtkTreeIter iter;
      GtkTreePath *path;
      if (gtk_tree_selection_get_selected(sel,0,&iter) != 0) {
        gboolean ret = (!0);
        path = gtk_tree_model_get_path(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(box -> dropdown_store)),gtk_tree_model_get_type()))),&iter);
        if ((event -> keyval) == 0xff0d) {
          treeview_activate_current_selection(box,path);
        }
        else if ((event -> keyval) == 0xffff) {
          tree_view_delete_current_selection(box,path);
        }
        else 
          ret = 0;
        gtk_tree_path_free(path);
        return ret;
      }
    }
  }
  return 0;
}

static void imhtml_cursor_moved_cb(gpointer data,GtkMovementStep step,gint count,gboolean extend,GtkWidget *widget)
{
/* Restart the typing timeout if arrow keys are pressed while editing the message */
  PidginStatusBox *status_box = data;
  if ((status_box -> typing) == 0) 
    return ;
  imhtml_changed_cb(0,status_box);
}

static void treeview_cursor_changed_cb(GtkTreeView *treeview,gpointer data)
{
  GtkTreeSelection *sel = gtk_tree_view_get_selection(treeview);
  GtkTreeModel *model = (GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)data),gtk_tree_model_get_type()));
  GtkTreeIter iter;
  GtkTreePath *cursor;
  GtkTreePath *selection;
  gint cmp;
  if (gtk_tree_selection_get_selected(sel,0,&iter) != 0) {
    if ((selection = gtk_tree_model_get_path(model,&iter)) == ((GtkTreePath *)((void *)0))) {
/* Shouldn't happen, but ignore anyway */
      return ;
    }
  }
  else {
/* I don't think this can happen, but we'll just ignore it */
    return ;
  }
  gtk_tree_view_get_cursor(treeview,&cursor,0);
  if (cursor == ((GtkTreePath *)((void *)0))) {
/* Probably won't happen in a 'cursor-changed' event? */
    gtk_tree_path_free(selection);
    return ;
  }
  cmp = gtk_tree_path_compare(cursor,selection);
  if (cmp < 0) {
/* The cursor moved up without moving the selection, so move it up again */
    gtk_tree_path_prev(cursor);
    gtk_tree_view_set_cursor(treeview,cursor,0,0);
  }
  else if (cmp > 0) {
/* The cursor moved down without moving the selection, so move it down again */
    gtk_tree_path_next(cursor);
    gtk_tree_view_set_cursor(treeview,cursor,0,0);
  }
  gtk_tree_path_free(selection);
  gtk_tree_path_free(cursor);
}

static void pidgin_status_box_init(PidginStatusBox *status_box)
{
  GtkCellRenderer *text_rend;
  GtkCellRenderer *icon_rend;
  GtkCellRenderer *emblem_rend;
  GtkTextBuffer *buffer;
  GtkWidget *toplevel;
  GtkTreeSelection *sel;
  do {
    ( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_object_get_type())))).flags |= GTK_NO_WINDOW;
  }while (0);
  status_box -> imhtml_visible = 0;
  status_box -> network_available = purple_network_is_available();
  status_box -> connecting = 0;
  status_box -> typing = 0;
  status_box -> toggle_button = gtk_toggle_button_new();
  status_box -> hbox = gtk_hbox_new(0,6);
  status_box -> cell_view = gtk_cell_view_new();
  status_box -> vsep = gtk_vseparator_new();
  status_box -> arrow = gtk_arrow_new(GTK_ARROW_DOWN,GTK_SHADOW_NONE);
  status_box -> store = gtk_list_store_new(NUM_COLUMNS,((GType )(6 << 2)),((GType )(16 << 2)),gdk_pixbuf_get_type(),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(17 << 2)),gdk_pixbuf_get_type(),((GType )(5 << 2)));
  status_box -> dropdown_store = gtk_list_store_new(NUM_COLUMNS,((GType )(6 << 2)),((GType )(16 << 2)),gdk_pixbuf_get_type(),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(17 << 2)),((GType )(16 << 2)),((GType )(5 << 2)));
  gtk_cell_view_set_model(((GtkCellView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> cell_view)),gtk_cell_view_get_type()))),((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> store)),gtk_tree_model_get_type()))));
  gtk_list_store_append((status_box -> store),&status_box -> iter);
  atk_object_set_name(gtk_widget_get_accessible((status_box -> toggle_button)),((const char *)(dgettext("pidgin","Status Selector"))));
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> toggle_button)),gtk_container_get_type()))),(status_box -> hbox));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> hbox)),gtk_box_get_type()))),(status_box -> cell_view),(!0),(!0),0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> hbox)),gtk_box_get_type()))),(status_box -> vsep),0,0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> hbox)),gtk_box_get_type()))),(status_box -> arrow),0,0,0);
  gtk_widget_show_all((status_box -> toggle_button));
  gtk_button_set_focus_on_click(((GtkButton *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> toggle_button)),gtk_button_get_type()))),0);
  text_rend = gtk_cell_renderer_text_new();
  icon_rend = gtk_cell_renderer_pixbuf_new();
  emblem_rend = gtk_cell_renderer_pixbuf_new();
  status_box -> popup_window = gtk_window_new(GTK_WINDOW_POPUP);
  toplevel = gtk_widget_get_toplevel(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type()))));
  if ((({
    GTypeInstance *__inst = (GTypeInstance *)toplevel;
    GType __t = gtk_window_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
{
    gtk_window_set_transient_for(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> popup_window)),gtk_window_get_type()))),((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)toplevel),gtk_window_get_type()))));
  }
  gtk_window_set_resizable(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> popup_window)),gtk_window_get_type()))),0);
  gtk_window_set_type_hint(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> popup_window)),gtk_window_get_type()))),GDK_WINDOW_TYPE_HINT_POPUP_MENU);
  gtk_window_set_screen(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> popup_window)),gtk_window_get_type()))),gtk_widget_get_screen(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type())))));
  status_box -> popup_frame = gtk_frame_new(0);
  gtk_frame_set_shadow_type(((GtkFrame *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> popup_frame)),gtk_frame_get_type()))),GTK_SHADOW_ETCHED_IN);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> popup_window)),gtk_container_get_type()))),(status_box -> popup_frame));
  gtk_widget_show((status_box -> popup_frame));
  status_box -> tree_view = gtk_tree_view_new();
  sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> tree_view)),gtk_tree_view_get_type()))));
  gtk_tree_selection_set_mode(sel,GTK_SELECTION_BROWSE);
  gtk_tree_view_set_headers_visible(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> tree_view)),gtk_tree_view_get_type()))),0);
  gtk_tree_view_set_hover_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> tree_view)),gtk_tree_view_get_type()))),(!0));
  gtk_tree_view_set_model(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> tree_view)),gtk_tree_view_get_type()))),((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> dropdown_store)),gtk_tree_model_get_type()))));
  status_box -> column = gtk_tree_view_column_new();
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> tree_view)),gtk_tree_view_get_type()))),(status_box -> column));
  gtk_tree_view_column_pack_start((status_box -> column),icon_rend,0);
  gtk_tree_view_column_pack_start((status_box -> column),text_rend,(!0));
  gtk_tree_view_column_pack_start((status_box -> column),emblem_rend,0);
  gtk_tree_view_column_set_attributes((status_box -> column),icon_rend,"stock-id",ICON_STOCK_COLUMN,((void *)((void *)0)));
  gtk_tree_view_column_set_attributes((status_box -> column),text_rend,"markup",TEXT_COLUMN,((void *)((void *)0)));
  gtk_tree_view_column_set_attributes((status_box -> column),emblem_rend,"stock-id",EMBLEM_COLUMN,"visible",EMBLEM_VISIBLE_COLUMN,((void *)((void *)0)));
  status_box -> scrolled_window = pidgin_make_scrollable((status_box -> tree_view),GTK_POLICY_NEVER,GTK_POLICY_NEVER,GTK_SHADOW_NONE,-1,-1);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> popup_frame)),gtk_container_get_type()))),(status_box -> scrolled_window));
  gtk_widget_show((status_box -> tree_view));
  gtk_tree_view_set_search_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> tree_view)),gtk_tree_view_get_type()))),TEXT_COLUMN);
  gtk_tree_view_set_search_equal_func(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> tree_view)),gtk_tree_view_get_type()))),pidgin_tree_view_search_equal_func,0,0);
  g_object_set(text_rend,"ellipsize",PANGO_ELLIPSIZE_END,((void *)((void *)0)));
  status_box -> icon_rend = gtk_cell_renderer_pixbuf_new();
  status_box -> text_rend = gtk_cell_renderer_text_new();
  emblem_rend = gtk_cell_renderer_pixbuf_new();
  gtk_cell_layout_pack_start(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> cell_view)),gtk_cell_layout_get_type()))),(status_box -> icon_rend),0);
  gtk_cell_layout_pack_start(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> cell_view)),gtk_cell_layout_get_type()))),(status_box -> text_rend),(!0));
  gtk_cell_layout_pack_start(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> cell_view)),gtk_cell_layout_get_type()))),emblem_rend,0);
  gtk_cell_layout_set_attributes(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> cell_view)),gtk_cell_layout_get_type()))),(status_box -> icon_rend),"stock-id",ICON_STOCK_COLUMN,((void *)((void *)0)));
  gtk_cell_layout_set_attributes(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> cell_view)),gtk_cell_layout_get_type()))),(status_box -> text_rend),"markup",TEXT_COLUMN,((void *)((void *)0)));
  gtk_cell_layout_set_attributes(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> cell_view)),gtk_cell_layout_get_type()))),emblem_rend,"pixbuf",EMBLEM_COLUMN,"visible",EMBLEM_VISIBLE_COLUMN,((void *)((void *)0)));
  g_object_set((status_box -> text_rend),"ellipsize",PANGO_ELLIPSIZE_END,((void *)((void *)0)));
  status_box -> vbox = gtk_vbox_new(0,0);
  status_box -> sw = pidgin_create_imhtml(0,&status_box -> imhtml,0,0);
  gtk_imhtml_set_editable(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_imhtml_get_type()))),(!0));
  buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_text_view_get_type()))));
#if 0
#endif
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> toggle_button)),((GType )(20 << 2))))),"key-press-event",((GCallback )toggle_key_press_cb),status_box,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> toggle_button)),((GType )(20 << 2))))),"button-press-event",((GCallback )toggled_cb),status_box,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buffer),((GType )(20 << 2))))),"changed",((GCallback )imhtml_changed_cb),status_box,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),((GType )(20 << 2))))),"format_function_toggle",((GCallback )imhtml_format_changed_cb),status_box,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),((GType )(20 << 2))))),"move_cursor",((GCallback )imhtml_cursor_moved_cb),status_box,0,G_CONNECT_SWAPPED);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),((GType )(20 << 2))))),"key_press_event",((GCallback )imhtml_remove_focus),status_box,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),((GType )(20 << 2))))),"message_send",((GCallback )remove_typing_cb),status_box,0,G_CONNECT_SWAPPED);
#ifdef USE_GTKSPELL
  if (purple_prefs_get_bool("/pidgin/conversations/spellcheck") != 0) 
    pidgin_setup_gtkspell(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_text_view_get_type()))));
#endif
  gtk_widget_set_parent((status_box -> vbox),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type()))));
  gtk_widget_show_all((status_box -> vbox));
  gtk_widget_set_parent((status_box -> toggle_button),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type()))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> vbox)),gtk_box_get_type()))),(status_box -> sw),(!0),(!0),0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)status_box),((GType )(20 << 2))))),"scroll_event",((GCallback )combo_box_scroll_event_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),((GType )(20 << 2))))),"scroll_event",((GCallback )imhtml_scroll_event_cb),(status_box -> imhtml),0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> popup_window)),((GType )(20 << 2))))),"button_release_event",((GCallback )treeview_button_release_cb),status_box,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> popup_window)),((GType )(20 << 2))))),"key_press_event",((GCallback )treeview_key_press_event),status_box,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> tree_view)),((GType )(20 << 2))))),"cursor-changed",((GCallback )treeview_cursor_changed_cb),(status_box -> dropdown_store),0,((GConnectFlags )0));
  gtk_tree_view_set_row_separator_func(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> tree_view)),gtk_tree_view_get_type()))),dropdown_store_row_separator_func,0,0);
  status_box -> token_status_account = check_active_accounts_for_identical_statuses();
  cache_pixbufs(status_box);
  pidgin_status_box_regenerate(status_box,(!0));
  purple_signal_connect(purple_savedstatuses_get_handle(),"savedstatus-changed",status_box,((PurpleCallback )current_savedstatus_changed_cb),status_box);
  purple_signal_connect(purple_savedstatuses_get_handle(),"savedstatus-added",status_box,((PurpleCallback )saved_status_updated_cb),status_box);
  purple_signal_connect(purple_savedstatuses_get_handle(),"savedstatus-deleted",status_box,((PurpleCallback )saved_status_updated_cb),status_box);
  purple_signal_connect(purple_savedstatuses_get_handle(),"savedstatus-modified",status_box,((PurpleCallback )saved_status_updated_cb),status_box);
  purple_signal_connect(purple_accounts_get_handle(),"account-enabled",status_box,((PurpleCallback )account_enabled_cb),status_box);
  purple_signal_connect(purple_accounts_get_handle(),"account-disabled",status_box,((PurpleCallback )account_enabled_cb),status_box);
  purple_signal_connect(purple_accounts_get_handle(),"account-status-changed",status_box,((PurpleCallback )account_status_changed_cb),status_box);
  purple_prefs_connect_callback(status_box,"/pidgin/conversations/spellcheck",spellcheck_prefs_cb,status_box);
  purple_prefs_connect_callback(status_box,"/pidgin/accounts/buddyicon",update_buddyicon_cb,status_box);
  purple_signal_connect((purple_get_core()),"uri-handler",status_box,((PurpleCallback )statusbox_uri_handler),status_box);
}

static void pidgin_status_box_size_request(GtkWidget *widget,GtkRequisition *requisition)
{
  GtkRequisition box_req;
  gint border_width = ( *((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_container_get_type())))).border_width;
  gtk_widget_size_request(( *((PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),pidgin_status_box_get_type())))).toggle_button,requisition);
/* Make this icon the same size as other buddy icons in the list; unless it already wants to be bigger */
  requisition -> height = (((requisition -> height) > 34)?(requisition -> height) : 34);
  requisition -> height += (border_width * 2);
/* If the gtkimhtml is visible, then add some additional padding */
  gtk_widget_size_request(( *((PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),pidgin_status_box_get_type())))).vbox,&box_req);
  if (box_req.height > 1) 
    requisition -> height += (box_req.height + (border_width * 2));
  requisition -> width = 1;
}
/* From gnome-panel */

static void do_colorshift(GdkPixbuf *dest,GdkPixbuf *src,int shift)
{
  gint i;
  gint j;
  gint width;
  gint height;
  gint has_alpha;
  gint srcrowstride;
  gint destrowstride;
  guchar *target_pixels;
  guchar *original_pixels;
  guchar *pixsrc;
  guchar *pixdest;
  int val;
  guchar r;
  guchar g;
  guchar b;
  has_alpha = gdk_pixbuf_get_has_alpha(src);
  width = gdk_pixbuf_get_width(src);
  height = gdk_pixbuf_get_height(src);
  srcrowstride = gdk_pixbuf_get_rowstride(src);
  destrowstride = gdk_pixbuf_get_rowstride(dest);
  target_pixels = gdk_pixbuf_get_pixels(dest);
  original_pixels = gdk_pixbuf_get_pixels(src);
  for (i = 0; i < height; i++) {
    pixdest = (target_pixels + (i * destrowstride));
    pixsrc = (original_pixels + (i * srcrowstride));
    for (j = 0; j < width; j++) {
      r =  *(pixsrc++);
      g =  *(pixsrc++);
      b =  *(pixsrc++);
      val = (r + shift);
       *(pixdest++) = (((val > 255)?255 : (((val < 0)?0 : val))));
      val = (g + shift);
       *(pixdest++) = (((val > 255)?255 : (((val < 0)?0 : val))));
      val = (b + shift);
       *(pixdest++) = (((val > 255)?255 : (((val < 0)?0 : val))));
      if (has_alpha != 0) 
         *(pixdest++) =  *(pixsrc++);
    }
  }
}

static void pidgin_status_box_size_allocate(GtkWidget *widget,GtkAllocation *allocation)
{
  PidginStatusBox *status_box = (PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),pidgin_status_box_get_type()));
  GtkRequisition req = {(0), (0)};
  GtkAllocation parent_alc;
  GtkAllocation box_alc;
  GtkAllocation icon_alc;
  gint border_width = ( *((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_container_get_type())))).border_width;
  gtk_widget_size_request((status_box -> toggle_button),&req);
/* Make this icon the same size as other buddy icons in the list; unless it already wants to be bigger */
  req.height = ((req.height > 34)?req.height : 34);
  req.height += (border_width * 2);
  box_alc =  *allocation;
  box_alc.width -= (border_width * 2);
  box_alc.height = ((1 > (((allocation -> height) - req.height) - (border_width * 2)))?1 : (((allocation -> height) - req.height) - (border_width * 2)));
  box_alc.x += border_width;
  box_alc.y += (req.height + border_width);
  gtk_widget_size_allocate((status_box -> vbox),&box_alc);
  parent_alc =  *allocation;
  parent_alc.height = ((1 > (req.height - (border_width * 2)))?1 : (req.height - (border_width * 2)));
  parent_alc.width -= (border_width * 2);
  parent_alc.x += border_width;
  parent_alc.y += border_width;
  if ((status_box -> icon_box) != 0) {
    parent_alc.width -= (parent_alc.height + border_width);
    icon_alc = parent_alc;
    icon_alc.height = ((((1 > icon_alc.height)?1 : icon_alc.height)) - 2);
    icon_alc.width = icon_alc.height;
    icon_alc.x += ((allocation -> width) - ((icon_alc.width + border_width) + 1));
    icon_alc.y += 1;
    if ((status_box -> icon_size) != icon_alc.height) {
      status_box -> icon_size = icon_alc.height;
      pidgin_status_box_redisplay_buddy_icon(status_box);
    }
    gtk_widget_size_allocate((status_box -> icon_box),&icon_alc);
  }
  gtk_widget_size_allocate((status_box -> toggle_button),&parent_alc);
  widget -> allocation =  *allocation;
}

static gboolean pidgin_status_box_expose_event(GtkWidget *widget,GdkEventExpose *event)
{
  PidginStatusBox *status_box = (PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),pidgin_status_box_get_type()));
  gtk_container_propagate_expose(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_container_get_type()))),(status_box -> vbox),event);
  gtk_container_propagate_expose(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_container_get_type()))),(status_box -> toggle_button),event);
  if (((status_box -> icon_box) != 0) && ((status_box -> icon_opaque) != 0)) {
    gtk_paint_box((widget -> style),(widget -> window),GTK_STATE_NORMAL,GTK_SHADOW_OUT,0,(status_box -> icon_box),"button",(( *(status_box -> icon_box)).allocation.x - 1),(( *(status_box -> icon_box)).allocation.y - 1),34,34);
  }
  return 0;
}

static void pidgin_status_box_forall(GtkContainer *container,gboolean include_internals,GtkCallback callback,gpointer callback_data)
{
  PidginStatusBox *status_box = (PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)container),pidgin_status_box_get_type()));
  if (include_internals != 0) {
    ( *callback)((status_box -> vbox),callback_data);
    ( *callback)((status_box -> toggle_button),callback_data);
    ( *callback)((status_box -> arrow),callback_data);
    if ((status_box -> icon_box) != 0) 
      ( *callback)((status_box -> icon_box),callback_data);
  }
}

GtkWidget *pidgin_status_box_new()
{
  return (g_object_new(pidgin_status_box_get_type(),"account",((void *)((void *)0)),"iconsel",!0,((void *)((void *)0))));
}

GtkWidget *pidgin_status_box_new_with_account(PurpleAccount *account)
{
  return (g_object_new(pidgin_status_box_get_type(),"account",account,"iconsel",!0,((void *)((void *)0))));
}
/**
 * Add a row to the dropdown menu.
 *
 * @param status_box The status box itself.
 * @param type       A PidginStatusBoxItemType.
 * @param pixbuf     The icon to associate with this row in the menu. The
 *                   function will try to decide a pixbuf if none is given.
 * @param title      The title of this item.  For the primitive entries,
 *                   this is something like "Available" or "Away."  For
 *                   the saved statuses, this is something like
 *                   "My favorite away message!"  This should be
 *                   plaintext (non-markedup) (this function escapes it).
 * @param desc       The secondary text for this item.  This will be
 *                   placed on the row below the title, in a dimmer
 *                   font (generally gray).  This text should be plaintext
 *                   (non-markedup) (this function escapes it).
 * @param data       Data to be associated with this row in the dropdown
 *                   menu.  For primitives this is the value of the
 *                   PurpleStatusPrimitive.  For saved statuses this is the
 *                   creation timestamp.
 */

void pidgin_status_box_add(PidginStatusBox *status_box,PidginStatusBoxItemType type,GdkPixbuf *pixbuf,const char *title,const char *desc,gpointer data)
{
  GtkTreeIter iter;
  char *text;
  const char *stock = (const char *)((void *)0);
  if (desc == ((const char *)((void *)0))) {
    text = g_markup_escape_text(title,(-1));
  }
  else {
    GtkStyle *style;
    char aa_color[8UL];
    gchar *escaped_title;
    gchar *escaped_desc;
    style = gtk_widget_get_style(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type()))));
    snprintf(aa_color,(sizeof(aa_color)),"#%02x%02x%02x",((style -> text_aa)[GTK_STATE_NORMAL].red >> 8),((style -> text_aa)[GTK_STATE_NORMAL].green >> 8),((style -> text_aa)[GTK_STATE_NORMAL].blue >> 8));
    escaped_title = g_markup_escape_text(title,(-1));
    escaped_desc = g_markup_escape_text(desc,(-1));
    text = g_strdup_printf("%s - <span color=\"%s\" size=\"smaller\">%s</span>",escaped_title,aa_color,escaped_desc);
    g_free(escaped_title);
    g_free(escaped_desc);
  }
  if (!(pixbuf != 0)) {
    PurpleStatusPrimitive prim = PURPLE_STATUS_UNSET;
    if (type == PIDGIN_STATUS_BOX_TYPE_PRIMITIVE) {
      prim = ((gint )((glong )data));
    }
    else if ((type == PIDGIN_STATUS_BOX_TYPE_SAVED_POPULAR) || (type == PIDGIN_STATUS_BOX_TYPE_POPULAR)) {
      PurpleSavedStatus *saved = purple_savedstatus_find_by_creation_time(((gint )((glong )data)));
      if (saved != 0) {
        prim = purple_savedstatus_get_type(saved);
      }
    }
    stock = pidgin_stock_id_from_status_primitive(prim);
  }
  gtk_list_store_append((status_box -> dropdown_store),&iter);
  gtk_list_store_set((status_box -> dropdown_store),&iter,TYPE_COLUMN,type,ICON_STOCK_COLUMN,stock,TEXT_COLUMN,text,TITLE_COLUMN,title,DESC_COLUMN,desc,DATA_COLUMN,data,EMBLEM_VISIBLE_COLUMN,(type == PIDGIN_STATUS_BOX_TYPE_SAVED_POPULAR),EMBLEM_COLUMN,"gtk-save",-1);
  g_free(text);
}

void pidgin_status_box_add_separator(PidginStatusBox *status_box)
{
/* Don't do anything unless GTK actually supports
	 * gtk_combo_box_set_row_separator_func */
  GtkTreeIter iter;
  gtk_list_store_append((status_box -> dropdown_store),&iter);
  gtk_list_store_set((status_box -> dropdown_store),&iter,TYPE_COLUMN,PIDGIN_STATUS_BOX_TYPE_SEPARATOR,-1);
}

void pidgin_status_box_set_network_available(PidginStatusBox *status_box,gboolean available)
{
  if (!(status_box != 0)) 
    return ;
  status_box -> network_available = available;
  pidgin_status_box_refresh(status_box);
}

void pidgin_status_box_set_connecting(PidginStatusBox *status_box,gboolean connecting)
{
  if (!(status_box != 0)) 
    return ;
  status_box -> connecting = connecting;
  pidgin_status_box_refresh(status_box);
}

static void pixbuf_size_prepared_cb(GdkPixbufLoader *loader,int width,int height,gpointer data)
{
  int w;
  int h;
  GtkIconSize icon_size = gtk_icon_size_from_name("pidgin-icon-size-tango-medium");
  gtk_icon_size_lookup(icon_size,&w,&h);
  if (height > width) 
    w = ((width * h) / height);
  else if (width > height) 
    h = ((height * w) / width);
  gdk_pixbuf_loader_set_size(loader,w,h);
}

static void pidgin_status_box_redisplay_buddy_icon(PidginStatusBox *status_box)
{
/* This is sometimes called before the box is shown, and we will not have a size */
  if ((status_box -> icon_size) <= 0) 
    return ;
  if ((status_box -> buddy_icon) != 0) 
    g_object_unref((status_box -> buddy_icon));
  if ((status_box -> buddy_icon_hover) != 0) 
    g_object_unref((status_box -> buddy_icon_hover));
  status_box -> buddy_icon = ((GdkPixbuf *)((void *)0));
  status_box -> buddy_icon_hover = ((GdkPixbuf *)((void *)0));
  if ((status_box -> buddy_icon_img) != ((PurpleStoredImage *)((void *)0))) {
    GdkPixbufLoader *loader;
    GError *error = (GError *)((void *)0);
    loader = gdk_pixbuf_loader_new();
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)loader),((GType )(20 << 2))))),"size-prepared",((GCallback )pixbuf_size_prepared_cb),0,0,((GConnectFlags )0));
    if (!(gdk_pixbuf_loader_write(loader,(purple_imgstore_get_data((status_box -> buddy_icon_img))),purple_imgstore_get_size((status_box -> buddy_icon_img)),&error) != 0) || (error != 0)) {
      purple_debug_warning("gtkstatusbox","gdk_pixbuf_loader_write() failed with size=%zu: %s\n",purple_imgstore_get_size((status_box -> buddy_icon_img)),((error != 0)?(error -> message) : "(no error message)"));
      if (error != 0) 
        g_error_free(error);
    }
    else if (!(gdk_pixbuf_loader_close(loader,&error) != 0) || (error != 0)) {
      purple_debug_warning("gtkstatusbox","gdk_pixbuf_loader_close() failed for image of size %zu: %s\n",purple_imgstore_get_size((status_box -> buddy_icon_img)),((error != 0)?(error -> message) : "(no error message)"));
      if (error != 0) 
        g_error_free(error);
    }
    else {
      GdkPixbuf *buf;
      GdkPixbuf *scale;
      int scale_width;
      int scale_height;
      buf = gdk_pixbuf_loader_get_pixbuf(loader);
      scale_width = gdk_pixbuf_get_width(buf);
      scale_height = gdk_pixbuf_get_height(buf);
      scale = gdk_pixbuf_new(GDK_COLORSPACE_RGB,(!0),8,scale_width,scale_height);
      gdk_pixbuf_fill(scale,0);
      gdk_pixbuf_copy_area(buf,0,0,scale_width,scale_height,scale,0,0);
      if (pidgin_gdk_pixbuf_is_opaque(scale) != 0) 
        pidgin_gdk_pixbuf_make_round(scale);
      status_box -> buddy_icon = scale;
    }
    g_object_unref(loader);
  }
  if ((status_box -> buddy_icon) == ((GdkPixbuf *)((void *)0))) {
/* Show a placeholder icon */
    GtkIconSize icon_size = gtk_icon_size_from_name("pidgin-icon-size-tango-small");
    status_box -> buddy_icon = gtk_widget_render_icon(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type()))),"pidgin-select-avatar",icon_size,"PidginStatusBox");
  }
  if ((status_box -> buddy_icon) != ((GdkPixbuf *)((void *)0))) {
    status_box -> icon_opaque = pidgin_gdk_pixbuf_is_opaque((status_box -> buddy_icon));
    gtk_image_set_from_pixbuf(((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> icon)),gtk_image_get_type()))),(status_box -> buddy_icon));
    status_box -> buddy_icon_hover = gdk_pixbuf_copy((status_box -> buddy_icon));
    do_colorshift((status_box -> buddy_icon_hover),(status_box -> buddy_icon_hover),32);
    gtk_widget_queue_resize(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type()))));
  }
}

void pidgin_status_box_set_buddy_icon(PidginStatusBox *status_box,PurpleStoredImage *img)
{
  purple_imgstore_unref((status_box -> buddy_icon_img));
  status_box -> buddy_icon_img = img;
  if ((status_box -> buddy_icon_img) != ((PurpleStoredImage *)((void *)0))) 
    purple_imgstore_ref((status_box -> buddy_icon_img));
  pidgin_status_box_redisplay_buddy_icon(status_box);
}

void pidgin_status_box_pulse_connecting(PidginStatusBox *status_box)
{
  if (!(status_box != 0)) 
    return ;
  if (!(connecting_stock_ids[++status_box -> connecting_index] != 0)) 
    status_box -> connecting_index = 0;
  pidgin_status_box_refresh(status_box);
}

static void pidgin_status_box_pulse_typing(PidginStatusBox *status_box)
{
  if (!(typing_stock_ids[++status_box -> typing_index] != 0)) 
    status_box -> typing_index = 0;
  pidgin_status_box_refresh(status_box);
}

static void activate_currently_selected_status(PidginStatusBox *status_box)
{
  PidginStatusBoxItemType type;
  gpointer data;
  gchar *title;
  GtkTreeIter iter;
  GtkTreePath *path;
  char *message;
  PurpleSavedStatus *saved_status = (PurpleSavedStatus *)((void *)0);
  gboolean changed = (!0);
  path = gtk_tree_row_reference_get_path((status_box -> active_row));
  if (!(gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> dropdown_store)),gtk_tree_model_get_type()))),&iter,path) != 0)) 
    return ;
  gtk_tree_path_free(path);
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> dropdown_store)),gtk_tree_model_get_type()))),&iter,TYPE_COLUMN,&type,DATA_COLUMN,&data,-1);
/*
	 * If the currently selected status is "New..." or
	 * "Saved..." or a popular status then do nothing.
	 * Popular statuses are
	 * activated elsewhere, and we update the status_box
	 * accordingly by connecting to the savedstatus-changed
	 * signal and then calling status_menu_refresh_iter()
	 */
  if (type != PIDGIN_STATUS_BOX_TYPE_PRIMITIVE) 
    return ;
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> dropdown_store)),gtk_tree_model_get_type()))),&iter,TITLE_COLUMN,&title,-1);
  message = pidgin_status_box_get_message(status_box);
  if (!(message != 0) || !(( *message) != 0)) {
    gtk_widget_hide_all((status_box -> vbox));
    status_box -> imhtml_visible = 0;
    if (message != ((char *)((void *)0))) {
      g_free(message);
      message = ((char *)((void *)0));
    }
  }
  if ((status_box -> account) == ((PurpleAccount *)((void *)0))) {
    PurpleStatusType *acct_status_type = (PurpleStatusType *)((void *)0);
/* id of acct_status_type */
    const char *id = (const char *)((void *)0);
    PurpleStatusPrimitive primitive = ((gint )((glong )data));
/* Global */
/* Save the newly selected status to prefs.xml and status.xml */
/* Has the status really been changed? */
    if ((status_box -> token_status_account) != 0) {
      gint active;
      PurpleStatus *status;
      GtkTreePath *path = gtk_tree_row_reference_get_path((status_box -> active_row));
      active = gtk_tree_path_get_indices(path)[0];
      gtk_tree_path_free(path);
      status = purple_account_get_active_status((status_box -> token_status_account));
      acct_status_type = find_status_type_by_index((status_box -> token_status_account),active);
      id = purple_status_type_get_id(acct_status_type);
      if ((g_str_equal(id,(purple_status_get_id(status))) != 0) && (purple_strequal(message,purple_status_get_attr_string(status,"message")) != 0)) {
/* Selected status and previous status is the same */
        PurpleSavedStatus *ss = purple_savedstatus_get_current();
/* Make sure that statusbox displays the correct thing.
				 * It can get messed up if the previous selection was a
				 * saved status that wasn't supported by this account */
        if ((((purple_savedstatus_get_type(ss)) == primitive) && (purple_savedstatus_is_transient(ss) != 0)) && (purple_savedstatus_has_substatuses(ss) != 0)) 
          changed = 0;
      }
    }
    else {
      saved_status = purple_savedstatus_get_current();
      if ((((purple_savedstatus_get_type(saved_status)) == primitive) && !(purple_savedstatus_has_substatuses(saved_status) != 0)) && (purple_strequal(purple_savedstatus_get_message(saved_status),message) != 0)) {
        changed = 0;
      }
    }
    if (changed != 0) {
/* Manually find the appropriate transient status */
      if ((status_box -> token_status_account) != 0) {
        GList *iter = purple_savedstatuses_get_all();
        GList *tmp;
        GList *active_accts = purple_accounts_get_all_active();
{
          for (; iter != ((GList *)((void *)0)); iter = (iter -> next)) {
            PurpleSavedStatus *ss = (iter -> data);
            const char *ss_msg = purple_savedstatus_get_message(ss);
/* find a known transient status that is the same as the
					 * new selected one */
            if (((((purple_savedstatus_get_type(ss)) == primitive) && (purple_savedstatus_is_transient(ss) != 0)) && (purple_savedstatus_has_substatuses(ss) != 0)) && (purple_strequal(ss_msg,message) != 0)) 
/* Must have substatuses */
{
              gboolean found = 0;
{
/* this status must have substatuses for all the active accts */
                for (tmp = active_accts; tmp != ((GList *)((void *)0)); tmp = (tmp -> next)) {
                  PurpleAccount *acct = (tmp -> data);
                  PurpleSavedStatusSub *sub = purple_savedstatus_get_substatus(ss,acct);
                  if (sub != 0) {
                    const PurpleStatusType *sub_type = purple_savedstatus_substatus_get_type(sub);
                    const char *subtype_status_id = purple_status_type_get_id(sub_type);
                    if (purple_strequal(subtype_status_id,id) != 0) {
                      found = (!0);
                      break; 
                    }
                  }
                }
              }
              if (found != 0) {
                saved_status = ss;
                break; 
              }
            }
          }
        }
        g_list_free(active_accts);
      }
      else {
/* If we've used this type+message before, lookup the transient status */
        saved_status = purple_savedstatus_find_transient_by_type_and_message(primitive,message);
      }
/* If this type+message is unique then create a new transient saved status */
      if (saved_status == ((PurpleSavedStatus *)((void *)0))) {
        saved_status = purple_savedstatus_new(0,primitive);
        purple_savedstatus_set_message(saved_status,message);
        if ((status_box -> token_status_account) != 0) {
          GList *tmp;
          GList *active_accts = purple_accounts_get_all_active();
          for (tmp = active_accts; tmp != ((GList *)((void *)0)); tmp = (tmp -> next)) {
            purple_savedstatus_set_substatus(saved_status,((PurpleAccount *)(tmp -> data)),acct_status_type,message);
          }
          g_list_free(active_accts);
        }
      }
/* Set the status for each account */
      purple_savedstatus_activate(saved_status);
    }
  }
  else {
/* Per-account */
    gint active;
    PurpleStatusType *status_type;
    PurpleStatus *status;
    const char *id = (const char *)((void *)0);
    status = purple_account_get_active_status((status_box -> account));
    active = ((gint )((glong )(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)status_box),((GType )(20 << 2))))),"active"))));
    status_type = find_status_type_by_index((status_box -> account),active);
    id = purple_status_type_get_id(status_type);
    if ((g_str_equal(id,(purple_status_get_id(status))) != 0) && (purple_strequal(message,purple_status_get_attr_string(status,"message")) != 0)) {
/* Selected status and previous status is the same */
      changed = 0;
    }
    if (changed != 0) {
      if (message != 0) 
        purple_account_set_status((status_box -> account),id,(!0),"message",message,((void *)((void *)0)));
      else 
        purple_account_set_status((status_box -> account),id,(!0),((void *)((void *)0)));
      saved_status = purple_savedstatus_get_current();
      if (purple_savedstatus_is_transient(saved_status) != 0) 
        purple_savedstatus_set_substatus(saved_status,(status_box -> account),status_type,message);
    }
  }
  g_free(title);
  g_free(message);
}

static void update_size(PidginStatusBox *status_box)
{
  GtkTextBuffer *buffer;
  GtkTextIter iter;
  int display_lines;
  int lines;
  GdkRectangle oneline;
  int height;
  int pad_top;
  int pad_inside;
  int pad_bottom;
  gboolean interior_focus;
  int focus_width;
  if (!((status_box -> imhtml_visible) != 0)) {
    if ((status_box -> vbox) != ((GtkWidget *)((void *)0))) 
      gtk_widget_set_size_request((status_box -> vbox),(-1),(-1));
    return ;
  }
  buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_text_view_get_type()))));
  height = 0;
  display_lines = 1;
  gtk_text_buffer_get_start_iter(buffer,&iter);
  do {
    gtk_text_view_get_iter_location(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_text_view_get_type()))),(&iter),&oneline);
    height += oneline.height;
    display_lines++;
  }while ((display_lines <= 4) && (gtk_text_view_forward_display_line(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_text_view_get_type()))),&iter) != 0));
/*
	 * This check fixes the case where the last character entered is a
	 * newline (shift+return).  For some reason the
	 * gtk_text_view_forward_display_line() function doesn't treat this
	 * like a new line, and so we think the input box only needs to be
	 * two lines instead of three, for example.  So we check if the
	 * last character was a newline and add some extra height if so.
	 */
  if (((display_lines <= 4) && (gtk_text_iter_backward_char(&iter) != 0)) && (gtk_text_iter_get_char((&iter)) == 10)) {
    gtk_text_view_get_iter_location(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_text_view_get_type()))),(&iter),&oneline);
    height += oneline.height;
    display_lines++;
  }
  lines = gtk_text_buffer_get_line_count(buffer);
/* Show a maximum of 4 lines */
  lines = ((lines < 4)?lines : 4);
  display_lines = ((display_lines < 4)?display_lines : 4);
  pad_top = gtk_text_view_get_pixels_above_lines(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_text_view_get_type()))));
  pad_bottom = gtk_text_view_get_pixels_below_lines(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_text_view_get_type()))));
  pad_inside = gtk_text_view_get_pixels_inside_wrap(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_text_view_get_type()))));
  height += ((pad_top + pad_bottom) * lines);
  height += (pad_inside * (display_lines - lines));
  gtk_widget_style_get((status_box -> imhtml),"interior-focus",&interior_focus,"focus-line-width",&focus_width,((void *)((void *)0)));
  if (!(interior_focus != 0)) 
    height += (2 * focus_width);
  gtk_widget_set_size_request((status_box -> vbox),(-1),(height + 6));
}

static void remove_typing_cb(PidginStatusBox *status_box)
{
  if ((status_box -> typing) == 0) {
/* Nothing has changed, so we don't need to do anything */
    status_menu_refresh_iter(status_box,0);
    return ;
  }
  gtk_imhtml_set_populate_primary_clipboard(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_imhtml_get_type()))),(!0));
  purple_timeout_remove((status_box -> typing));
  status_box -> typing = 0;
  activate_currently_selected_status(status_box);
  pidgin_status_box_refresh(status_box);
}

static void pidgin_status_box_changed(PidginStatusBox *status_box)
{
  GtkTreePath *path = gtk_tree_row_reference_get_path((status_box -> active_row));
  GtkTreeIter iter;
  PidginStatusBoxItemType type;
  gpointer data;
  GList *accounts = (GList *)((void *)0);
  GList *node;
  int active;
  gboolean wastyping = 0;
  if (!(gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> dropdown_store)),gtk_tree_model_get_type()))),&iter,path) != 0)) 
    return ;
  active = gtk_tree_path_get_indices(path)[0];
  gtk_tree_path_free(path);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)status_box),((GType )(20 << 2))))),"active",((gpointer )((glong )active)));
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> dropdown_store)),gtk_tree_model_get_type()))),&iter,TYPE_COLUMN,&type,DATA_COLUMN,&data,-1);
  if ((wastyping = ((status_box -> typing) != 0)) != 0) 
    purple_timeout_remove((status_box -> typing));
  status_box -> typing = 0;
  if (((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type())))),gtk_object_get_type())))).flags & GTK_SENSITIVE) != 0) && ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type())))),gtk_object_get_type())))).flags & GTK_PARENT_SENSITIVE) != 0)) {
    if ((type == PIDGIN_STATUS_BOX_TYPE_POPULAR) || (type == PIDGIN_STATUS_BOX_TYPE_SAVED_POPULAR)) {
      PurpleSavedStatus *saved;
      saved = purple_savedstatus_find_by_creation_time(((gint )((glong )data)));
      do {
        if (saved != ((PurpleSavedStatus *)((void *)0))) {
        }
        else {
          g_return_if_fail_warning(0,((const char *)__func__),"saved != NULL");
          return ;
        };
      }while (0);
      purple_savedstatus_activate(saved);
      return ;
    }
    if (type == PIDGIN_STATUS_BOX_TYPE_CUSTOM) {
      PurpleSavedStatus *saved_status;
      saved_status = purple_savedstatus_get_current();
      if ((purple_savedstatus_get_type(saved_status)) == PURPLE_STATUS_AVAILABLE) 
        saved_status = purple_savedstatus_new(0,PURPLE_STATUS_AWAY);
      pidgin_status_editor_show(0,((purple_savedstatus_is_transient(saved_status) != 0)?saved_status : ((struct _PurpleSavedStatus *)((void *)0))));
      status_menu_refresh_iter(status_box,wastyping);
      if (wastyping != 0) 
        pidgin_status_box_refresh(status_box);
      return ;
    }
    if (type == PIDGIN_STATUS_BOX_TYPE_SAVED) {
      pidgin_status_window_show();
      status_menu_refresh_iter(status_box,wastyping);
      if (wastyping != 0) 
        pidgin_status_box_refresh(status_box);
      return ;
    }
  }
/*
	 * Show the message box whenever the primitive allows for a
	 * message attribute on any protocol that is enabled,
	 * or our protocol, if we have account set
	 */
  if ((status_box -> account) != 0) 
    accounts = g_list_prepend(accounts,(status_box -> account));
  else 
    accounts = purple_accounts_get_all_active();
  status_box -> imhtml_visible = 0;
{
    for (node = accounts; node != ((GList *)((void *)0)); node = (node -> next)) {
      PurpleAccount *account;
      PurpleStatusType *status_type;
      account = (node -> data);
      status_type = purple_account_get_status_type_with_primitive(account,((gint )((glong )data)));
      if ((status_type != ((PurpleStatusType *)((void *)0))) && (purple_status_type_get_attr(status_type,"message") != ((PurpleStatusAttr *)((void *)0)))) {
        status_box -> imhtml_visible = (!0);
        break; 
      }
    }
  }
  g_list_free(accounts);
  if (((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type())))),gtk_object_get_type())))).flags & GTK_SENSITIVE) != 0) && ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type())))),gtk_object_get_type())))).flags & GTK_PARENT_SENSITIVE) != 0)) {
    if ((status_box -> imhtml_visible) != 0) {
      GtkTextIter start;
      GtkTextIter end;
      GtkTextBuffer *buffer;
      gtk_widget_show_all((status_box -> vbox));
      status_box -> typing = purple_timeout_add_seconds(4,((GSourceFunc )remove_typing_cb),status_box);
      gtk_widget_grab_focus((status_box -> imhtml));
      buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_text_view_get_type()))));
      gtk_imhtml_set_populate_primary_clipboard(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_imhtml_get_type()))),0);
      gtk_text_buffer_get_bounds(buffer,&start,&end);
      gtk_text_buffer_move_mark(buffer,gtk_text_buffer_get_mark(buffer,"insert"),(&end));
      gtk_text_buffer_move_mark(buffer,gtk_text_buffer_get_mark(buffer,"selection_bound"),(&start));
    }
    else {
      gtk_widget_hide_all((status_box -> vbox));
/* This is where we actually set the status */
      activate_currently_selected_status(status_box);
    }
  }
  pidgin_status_box_refresh(status_box);
}

static gint get_statusbox_index(PidginStatusBox *box,PurpleSavedStatus *saved_status)
{
  gint index = (-1);
  switch(purple_savedstatus_get_type(saved_status)){
/* In reverse order */
    case PURPLE_STATUS_OFFLINE:
{
      index++;
    }
    case PURPLE_STATUS_INVISIBLE:
{
      index++;
    }
    case PURPLE_STATUS_UNAVAILABLE:
{
      index++;
    }
    case PURPLE_STATUS_AWAY:
{
      index++;
    }
    case PURPLE_STATUS_AVAILABLE:
{
      index++;
      break; 
    }
    default:
{
      break; 
    }
  }
  return index;
}

static void imhtml_changed_cb(GtkTextBuffer *buffer,void *data)
{
  PidginStatusBox *status_box = (PidginStatusBox *)data;
  if (((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type())))),gtk_object_get_type())))).flags & GTK_SENSITIVE) != 0) && ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)status_box),gtk_widget_get_type())))),gtk_object_get_type())))).flags & GTK_PARENT_SENSITIVE) != 0)) {
    if ((status_box -> typing) != 0) {
      pidgin_status_box_pulse_typing(status_box);
      purple_timeout_remove((status_box -> typing));
    }
    status_box -> typing = purple_timeout_add_seconds(4,((GSourceFunc )remove_typing_cb),status_box);
  }
  pidgin_status_box_refresh(status_box);
}

static void imhtml_format_changed_cb(GtkIMHtml *imhtml,GtkIMHtmlButtons buttons,void *data)
{
  imhtml_changed_cb(0,data);
}

char *pidgin_status_box_get_message(PidginStatusBox *status_box)
{
  if ((status_box -> imhtml_visible) != 0) 
    return gtk_imhtml_get_markup(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(status_box -> imhtml)),gtk_imhtml_get_type()))));
  else 
    return 0;
}
