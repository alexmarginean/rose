/*
 * @file gtkimhtmltoolbar.c GTK+ IMHtml Toolbar
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "pidgin.h"
#include "imgstore.h"
#include "notify.h"
#include "prefs.h"
#include "request.h"
#include "pidginstock.h"
#include "util.h"
#include "debug.h"
#include "gtkdialogs.h"
#include "gtkimhtmltoolbar.h"
#include "gtksmiley.h"
#include "gtkthemes.h"
#include "gtkutils.h"
#include <gdk/gdkkeysyms.h>
static GtkHBoxClass *parent_class = (GtkHBoxClass *)((void *)0);
static void toggle_button_set_active_block(GtkToggleButton *button,gboolean is_active,GtkIMHtmlToolbar *toolbar);
static gboolean gtk_imhtmltoolbar_popup_menu(GtkWidget *widget,GdkEventButton *event,GtkIMHtmlToolbar *toolbar);

static void do_bold(GtkWidget *bold,GtkIMHtmlToolbar *toolbar)
{
  do {
    if (toolbar != ((GtkIMHtmlToolbar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"toolbar != NULL");
      return ;
    };
  }while (0);
  gtk_imhtml_toggle_bold(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))));
  gtk_widget_grab_focus((toolbar -> imhtml));
}

static void do_italic(GtkWidget *italic,GtkIMHtmlToolbar *toolbar)
{
  do {
    if (toolbar != ((GtkIMHtmlToolbar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"toolbar != NULL");
      return ;
    };
  }while (0);
  gtk_imhtml_toggle_italic(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))));
  gtk_widget_grab_focus((toolbar -> imhtml));
}

static void do_underline(GtkWidget *underline,GtkIMHtmlToolbar *toolbar)
{
  do {
    if (toolbar != ((GtkIMHtmlToolbar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"toolbar != NULL");
      return ;
    };
  }while (0);
  gtk_imhtml_toggle_underline(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))));
  gtk_widget_grab_focus((toolbar -> imhtml));
}

static void do_strikethrough(GtkWidget *strikethrough,GtkIMHtmlToolbar *toolbar)
{
  do {
    if (toolbar != ((GtkIMHtmlToolbar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"toolbar != NULL");
      return ;
    };
  }while (0);
  gtk_imhtml_toggle_strike(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))));
  gtk_widget_grab_focus((toolbar -> imhtml));
}

static void do_small(GtkWidget *smalltb,GtkIMHtmlToolbar *toolbar)
{
  do {
    if (toolbar != ((GtkIMHtmlToolbar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"toolbar != NULL");
      return ;
    };
  }while (0);
/* Only shrink the font on activation, not deactivation as well */
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)smalltb),gtk_toggle_button_get_type())))) != 0) 
    gtk_imhtml_font_shrink(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))));
  gtk_widget_grab_focus((toolbar -> imhtml));
}

static void do_big(GtkWidget *large,GtkIMHtmlToolbar *toolbar)
{
  do {
    if (toolbar != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"toolbar");
      return ;
    };
  }while (0);
/* Only grow the font on activation, not deactivation as well */
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)large),gtk_toggle_button_get_type())))) != 0) 
    gtk_imhtml_font_grow(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))));
  gtk_widget_grab_focus((toolbar -> imhtml));
}

static gboolean destroy_toolbar_font(GtkWidget *widget,GdkEvent *event,GtkIMHtmlToolbar *toolbar)
{
  if (widget != ((GtkWidget *)((void *)0))) 
    gtk_imhtml_toggle_fontface(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))),"");
  if ((toolbar -> font_dialog) != ((GtkWidget *)((void *)0))) {
    gtk_widget_destroy((toolbar -> font_dialog));
    toolbar -> font_dialog = ((GtkWidget *)((void *)0));
  }
  return 0;
}

static void realize_toolbar_font(GtkWidget *widget,GtkIMHtmlToolbar *toolbar)
{
  GtkFontSelection *sel;
  sel = ((GtkFontSelection *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkFontSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> font_dialog)),gtk_font_selection_dialog_get_type())))).fontsel),gtk_font_selection_get_type())));
  gtk_widget_hide_all(gtk_widget_get_parent((sel -> size_entry)));
  gtk_widget_show_all((sel -> family_list));
  gtk_widget_show(gtk_widget_get_parent((sel -> family_list)));
  gtk_widget_show(gtk_widget_get_parent(gtk_widget_get_parent((sel -> family_list))));
}

static void cancel_toolbar_font(GtkWidget *widget,GtkIMHtmlToolbar *toolbar)
{
  destroy_toolbar_font(widget,0,toolbar);
}

static void apply_font(GtkWidget *widget,GtkFontSelectionDialog *fontsel)
{
/* this could be expanded to include font size, weight, etc.
	   but for now only works with font face */
  gchar *fontname = gtk_font_selection_dialog_get_font_name(fontsel);
  GtkIMHtmlToolbar *toolbar = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)fontsel),((GType )(20 << 2))))),"purple_toolbar"));
  if (fontname != 0) {
    const gchar *family_name = (const gchar *)((void *)0);
    PangoFontDescription *desc = (PangoFontDescription *)((void *)0);
    desc = pango_font_description_from_string(fontname);
    family_name = pango_font_description_get_family(desc);
    if (family_name != 0) {
      gtk_imhtml_toggle_fontface(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))),family_name);
    }
    pango_font_description_free(desc);
    g_free(fontname);
  }
  cancel_toolbar_font(0,toolbar);
}

static void toggle_font(GtkWidget *font,GtkIMHtmlToolbar *toolbar)
{
  do {
    if (toolbar != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"toolbar");
      return ;
    };
  }while (0);
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)font),gtk_toggle_button_get_type())))) != 0) {
    char *fontname = gtk_imhtml_get_current_fontface(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))));
    if (!((toolbar -> font_dialog) != 0)) {
      toolbar -> font_dialog = gtk_font_selection_dialog_new(((const char *)(dgettext("pidgin","Select Font"))));
      g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> font_dialog)),((GType )(20 << 2))))),"purple_toolbar",toolbar);
      if (fontname != 0) {
        char *fonttif = g_strdup_printf("%s 12",fontname);
        g_free(fontname);
        gtk_font_selection_dialog_set_font_name(((GtkFontSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> font_dialog)),gtk_font_selection_dialog_get_type()))),fonttif);
        g_free(fonttif);
      }
      else {
        gtk_font_selection_dialog_set_font_name(((GtkFontSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> font_dialog)),gtk_font_selection_dialog_get_type()))),"Helvetica 12");
      }
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> font_dialog)),((GType )(20 << 2))))),"delete_event",((GCallback )destroy_toolbar_font),toolbar,0,((GConnectFlags )0));
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkFontSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> font_dialog)),gtk_font_selection_dialog_get_type())))).ok_button),((GType )(20 << 2))))),"clicked",((GCallback )apply_font),(toolbar -> font_dialog),0,((GConnectFlags )0));
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkFontSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> font_dialog)),gtk_font_selection_dialog_get_type())))).cancel_button),((GType )(20 << 2))))),"clicked",((GCallback )cancel_toolbar_font),toolbar,0,((GConnectFlags )0));
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> font_dialog)),((GType )(20 << 2))))),"realize",((GCallback )realize_toolbar_font),toolbar,0,G_CONNECT_AFTER);
    }
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> font_dialog)),gtk_window_get_type()))));
  }
  else {
    cancel_toolbar_font(font,toolbar);
  }
  gtk_widget_grab_focus((toolbar -> imhtml));
}

static gboolean destroy_toolbar_fgcolor(GtkWidget *widget,GdkEvent *event,GtkIMHtmlToolbar *toolbar)
{
  if (widget != ((GtkWidget *)((void *)0))) 
    gtk_imhtml_toggle_forecolor(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))),"");
  if ((toolbar -> fgcolor_dialog) != ((GtkWidget *)((void *)0))) {
    gtk_widget_destroy((toolbar -> fgcolor_dialog));
    toolbar -> fgcolor_dialog = ((GtkWidget *)((void *)0));
  }
  return 0;
}

static void cancel_toolbar_fgcolor(GtkWidget *widget,GtkIMHtmlToolbar *toolbar)
{
  destroy_toolbar_fgcolor(widget,0,toolbar);
}

static void do_fgcolor(GtkWidget *widget,GtkColorSelection *colorsel)
{
  GdkColor text_color;
  GtkIMHtmlToolbar *toolbar = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)colorsel),((GType )(20 << 2))))),"purple_toolbar"));
  char *open_tag;
  open_tag = (g_malloc(30));
  gtk_color_selection_get_current_color(colorsel,&text_color);
  g_snprintf(open_tag,23,"#%02X%02X%02X",(text_color.red / 256),(text_color.green / 256),(text_color.blue / 256));
  gtk_imhtml_toggle_forecolor(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))),open_tag);
  g_free(open_tag);
  cancel_toolbar_fgcolor(0,toolbar);
}

static void toggle_fg_color(GtkWidget *color,GtkIMHtmlToolbar *toolbar)
{
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)color),gtk_toggle_button_get_type())))) != 0) {
    GtkWidget *colorsel;
    GdkColor fgcolor;
    char *color = gtk_imhtml_get_current_forecolor(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))));
    if (!((toolbar -> fgcolor_dialog) != 0)) {
      toolbar -> fgcolor_dialog = gtk_color_selection_dialog_new(((const char *)(dgettext("pidgin","Select Text Color"))));
      colorsel = ( *((GtkColorSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> fgcolor_dialog)),gtk_color_selection_dialog_get_type())))).colorsel;
      if (color != 0) {
        gdk_color_parse(color,&fgcolor);
        gtk_color_selection_set_current_color(((GtkColorSelection *)(g_type_check_instance_cast(((GTypeInstance *)colorsel),gtk_color_selection_get_type()))),(&fgcolor));
        g_free(color);
      }
      g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)colorsel),((GType )(20 << 2))))),"purple_toolbar",toolbar);
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> fgcolor_dialog)),((GType )(20 << 2))))),"delete_event",((GCallback )destroy_toolbar_fgcolor),toolbar,0,((GConnectFlags )0));
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkColorSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> fgcolor_dialog)),gtk_color_selection_dialog_get_type())))).ok_button),((GType )(20 << 2))))),"clicked",((GCallback )do_fgcolor),colorsel,0,((GConnectFlags )0));
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkColorSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> fgcolor_dialog)),gtk_color_selection_dialog_get_type())))).cancel_button),((GType )(20 << 2))))),"clicked",((GCallback )cancel_toolbar_fgcolor),toolbar,0,((GConnectFlags )0));
    }
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> fgcolor_dialog)),gtk_window_get_type()))));
  }
  else {
    cancel_toolbar_fgcolor(color,toolbar);
  }
  gtk_widget_grab_focus((toolbar -> imhtml));
}

static gboolean destroy_toolbar_bgcolor(GtkWidget *widget,GdkEvent *event,GtkIMHtmlToolbar *toolbar)
{
  if (widget != ((GtkWidget *)((void *)0))) {
    if (gtk_text_buffer_get_selection_bounds(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type())))).text_buffer,0,0) != 0) 
      gtk_imhtml_toggle_backcolor(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))),"");
    else 
      gtk_imhtml_toggle_background(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))),"");
  }
  if ((toolbar -> bgcolor_dialog) != ((GtkWidget *)((void *)0))) {
    gtk_widget_destroy((toolbar -> bgcolor_dialog));
    toolbar -> bgcolor_dialog = ((GtkWidget *)((void *)0));
  }
  return 0;
}

static void cancel_toolbar_bgcolor(GtkWidget *widget,GtkIMHtmlToolbar *toolbar)
{
  destroy_toolbar_bgcolor(widget,0,toolbar);
}

static void do_bgcolor(GtkWidget *widget,GtkColorSelection *colorsel)
{
  GdkColor text_color;
  GtkIMHtmlToolbar *toolbar = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)colorsel),((GType )(20 << 2))))),"purple_toolbar"));
  char *open_tag;
  open_tag = (g_malloc(30));
  gtk_color_selection_get_current_color(colorsel,&text_color);
  g_snprintf(open_tag,23,"#%02X%02X%02X",(text_color.red / 256),(text_color.green / 256),(text_color.blue / 256));
  if (gtk_text_buffer_get_selection_bounds(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type())))).text_buffer,0,0) != 0) 
    gtk_imhtml_toggle_backcolor(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))),open_tag);
  else 
    gtk_imhtml_toggle_background(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))),open_tag);
  g_free(open_tag);
  cancel_toolbar_bgcolor(0,toolbar);
}

static void toggle_bg_color(GtkWidget *color,GtkIMHtmlToolbar *toolbar)
{
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)color),gtk_toggle_button_get_type())))) != 0) {
    GtkWidget *colorsel;
    GdkColor bgcolor;
    char *color = gtk_imhtml_get_current_backcolor(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))));
    if (!((toolbar -> bgcolor_dialog) != 0)) {
      toolbar -> bgcolor_dialog = gtk_color_selection_dialog_new(((const char *)(dgettext("pidgin","Select Background Color"))));
      colorsel = ( *((GtkColorSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> bgcolor_dialog)),gtk_color_selection_dialog_get_type())))).colorsel;
      if (color != 0) {
        gdk_color_parse(color,&bgcolor);
        gtk_color_selection_set_current_color(((GtkColorSelection *)(g_type_check_instance_cast(((GTypeInstance *)colorsel),gtk_color_selection_get_type()))),(&bgcolor));
        g_free(color);
      }
      g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)colorsel),((GType )(20 << 2))))),"purple_toolbar",toolbar);
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> bgcolor_dialog)),((GType )(20 << 2))))),"delete_event",((GCallback )destroy_toolbar_bgcolor),toolbar,0,((GConnectFlags )0));
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkColorSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> bgcolor_dialog)),gtk_color_selection_dialog_get_type())))).ok_button),((GType )(20 << 2))))),"clicked",((GCallback )do_bgcolor),colorsel,0,((GConnectFlags )0));
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkColorSelectionDialog *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> bgcolor_dialog)),gtk_color_selection_dialog_get_type())))).cancel_button),((GType )(20 << 2))))),"clicked",((GCallback )cancel_toolbar_bgcolor),toolbar,0,((GConnectFlags )0));
    }
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> bgcolor_dialog)),gtk_window_get_type()))));
  }
  else {
    cancel_toolbar_bgcolor(color,toolbar);
  }
  gtk_widget_grab_focus((toolbar -> imhtml));
}

static void clear_formatting_cb(GtkWidget *clear,GtkIMHtmlToolbar *toolbar)
{
  toggle_button_set_active_block(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> clear)),gtk_toggle_button_get_type()))),0,toolbar);
  gtk_imhtml_clear_formatting(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))));
}

static void cancel_link_cb(GtkIMHtmlToolbar *toolbar,PurpleRequestFields *fields)
{
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> link)),gtk_toggle_button_get_type()))),0);
  toolbar -> link_dialog = ((GtkWidget *)((void *)0));
}

static void close_link_dialog(GtkIMHtmlToolbar *toolbar)
{
  if ((toolbar -> link_dialog) != ((GtkWidget *)((void *)0))) {
    purple_request_close(PURPLE_REQUEST_FIELDS,(toolbar -> link_dialog));
    toolbar -> link_dialog = ((GtkWidget *)((void *)0));
  }
}

static void do_insert_link_cb(GtkIMHtmlToolbar *toolbar,PurpleRequestFields *fields)
{
  const char *url;
  const char *description;
  url = purple_request_fields_get_string(fields,"url");
  if ((( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type())))).format_functions & GTK_IMHTML_LINKDESC) != 0) 
    description = purple_request_fields_get_string(fields,"description");
  else 
    description = ((const char *)((void *)0));
  if (description == ((const char *)((void *)0))) 
    description = url;
  gtk_imhtml_insert_link(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))),gtk_text_buffer_get_insert(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type())))).text_buffer),url,description);
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> link)),gtk_toggle_button_get_type()))),0);
  toolbar -> link_dialog = ((GtkWidget *)((void *)0));
}

static void insert_link_cb(GtkWidget *w,GtkIMHtmlToolbar *toolbar)
{
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> link)),gtk_toggle_button_get_type())))) != 0) {
    PurpleRequestFields *fields;
    PurpleRequestFieldGroup *group;
    PurpleRequestField *field;
    GtkTextIter start;
    GtkTextIter end;
    char *msg;
    char *desc = (char *)((void *)0);
    fields = purple_request_fields_new();
    group = purple_request_field_group_new(0);
    purple_request_fields_add_group(fields,group);
    field = purple_request_field_string_new("url",((const char *)(dgettext("pidgin","_URL"))),0,0);
    purple_request_field_set_required(field,(!0));
    purple_request_field_group_add_field(group,field);
    if ((( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type())))).format_functions & GTK_IMHTML_LINKDESC) != 0) {
      if (gtk_text_buffer_get_selection_bounds(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type())))).text_buffer,&start,&end) != 0) {
        desc = gtk_imhtml_get_text(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))),&start,&end);
      }
      field = purple_request_field_string_new("description",((const char *)(dgettext("pidgin","_Description"))),desc,0);
      purple_request_field_group_add_field(group,field);
      msg = g_strdup(((const char *)(dgettext("pidgin","Please enter the URL and description of the link that you want to insert. The description is optional."))));
    }
    else {
      msg = g_strdup(((const char *)(dgettext("pidgin","Please enter the URL of the link that you want to insert."))));
    }
    toolbar -> link_dialog = (purple_request_fields(toolbar,((const char *)(dgettext("pidgin","Insert Link"))),0,msg,fields,((const char *)(dgettext("pidgin","_Insert"))),((GCallback )do_insert_link_cb),((const char *)(dgettext("pidgin","Cancel"))),((GCallback )cancel_link_cb),0,0,0,toolbar));
    g_free(msg);
    g_free(desc);
  }
  else {
    close_link_dialog(toolbar);
  }
  gtk_widget_grab_focus((toolbar -> imhtml));
}

static void insert_hr_cb(GtkWidget *widget,GtkIMHtmlToolbar *toolbar)
{
  GtkTextIter iter;
  GtkTextMark *ins;
  GtkIMHtmlScalable *hr;
  ins = gtk_text_buffer_get_insert(gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_text_view_get_type())))));
  gtk_text_buffer_get_iter_at_mark(gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_text_view_get_type())))),&iter,ins);
  hr = gtk_imhtml_hr_new();
  gtk_imhtml_hr_add_to(hr,((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))),&iter);
}

static void do_insert_image_cb(GtkWidget *widget,int response,GtkIMHtmlToolbar *toolbar)
{
  gchar *filename;
  gchar *name;
  gchar *buf;
  char *filedata;
  size_t size;
  GError *error = (GError *)((void *)0);
  int id;
  GtkTextIter iter;
  GtkTextMark *ins;
  if (response != GTK_RESPONSE_ACCEPT) {
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> image)),gtk_toggle_button_get_type()))),0);
    return ;
  }
  filename = gtk_file_chooser_get_filename(((GtkFileChooser *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_file_chooser_get_type()))));
  if (filename == ((gchar *)((void *)0))) {
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> image)),gtk_toggle_button_get_type()))),0);
    return ;
  }
/* The following triggers a callback that closes the widget */
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> image)),gtk_toggle_button_get_type()))),0);
  if (!(g_file_get_contents(filename,&filedata,&size,&error) != 0)) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,(error -> message),0,0,0);
    g_error_free(error);
    g_free(filename);
    return ;
  }
  name = (strrchr(filename,'/') + 1);
  id = purple_imgstore_add_with_id(filedata,size,name);
  if (id == 0) {
    buf = g_strdup_printf(((const char *)(dgettext("pidgin","Failed to store image: %s\n"))),filename);
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,buf,0,0,0);
    g_free(buf);
    g_free(filename);
    return ;
  }
  g_free(filename);
  ins = gtk_text_buffer_get_insert(gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_text_view_get_type())))));
  gtk_text_buffer_get_iter_at_mark(gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_text_view_get_type())))),&iter,ins);
  gtk_imhtml_insert_image_at_iter(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))),id,&iter);
  purple_imgstore_unref_by_id(id);
}

static void insert_image_cb(GtkWidget *save,GtkIMHtmlToolbar *toolbar)
{
  GtkWidget *window;
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> image)),gtk_toggle_button_get_type())))) != 0) {
    window = gtk_file_chooser_dialog_new(((const char *)(dgettext("pidgin","Insert Image"))),0,GTK_FILE_CHOOSER_ACTION_OPEN,"gtk-cancel",GTK_RESPONSE_CANCEL,"gtk-open",GTK_RESPONSE_ACCEPT,((void *)((void *)0)));
    gtk_dialog_set_default_response(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),GTK_RESPONSE_ACCEPT);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)((GtkFileChooser *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_file_chooser_get_type())))),((GType )(20 << 2))))),"response",((GCallback )do_insert_image_cb),toolbar,0,((GConnectFlags )0));
    gtk_widget_show(window);
    toolbar -> image_dialog = window;
  }
  else {
    gtk_widget_destroy((toolbar -> image_dialog));
    toolbar -> image_dialog = ((GtkWidget *)((void *)0));
  }
  gtk_widget_grab_focus((toolbar -> imhtml));
}

static void destroy_smiley_dialog(GtkIMHtmlToolbar *toolbar)
{
  if ((toolbar -> smiley_dialog) != ((GtkWidget *)((void *)0))) {
    gtk_widget_destroy((toolbar -> smiley_dialog));
    toolbar -> smiley_dialog = ((GtkWidget *)((void *)0));
  }
}

static gboolean close_smiley_dialog(GtkIMHtmlToolbar *toolbar)
{
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> smiley)),gtk_toggle_button_get_type()))),0);
  return 0;
}

static void insert_smiley_text(GtkWidget *widget,GtkIMHtmlToolbar *toolbar)
{
  char *smiley_text;
  char *escaped_smiley;
  smiley_text = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"smiley_text"));
  escaped_smiley = g_markup_escape_text(smiley_text,(-1));
  gtk_imhtml_insert_smiley(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))),( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type())))).protocol_name,escaped_smiley);
  g_free(escaped_smiley);
  close_smiley_dialog(toolbar);
}
/* smiley buttons list */

struct smiley_button_list 
{
  int width;
  int height;
  GtkWidget *button;
  const GtkIMHtmlSmiley *smiley;
  struct smiley_button_list *next;
}
;

static struct smiley_button_list *sort_smileys(struct smiley_button_list *ls,GtkIMHtmlToolbar *toolbar,int *width,const GtkIMHtmlSmiley *smiley)
{
  GtkWidget *image;
  GtkWidget *button;
  GtkRequisition size;
  struct smiley_button_list *cur;
  struct smiley_button_list *it;
  struct smiley_button_list *it_last;
  const gchar *filename = (smiley -> file);
  gchar *face = (smiley -> smile);
  PurpleSmiley *psmiley = (PurpleSmiley *)((void *)0);
  gboolean supports_custom = ((gtk_imhtml_get_format_functions(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))))) & GTK_IMHTML_CUSTOM_SMILEY);
  cur = ((struct smiley_button_list *)(g_malloc0_n(1,(sizeof(struct smiley_button_list )))));
  it = ls;
/* list iterators*/
  it_last = ls;
  image = gtk_image_new_from_file(filename);
  gtk_widget_size_request(image,&size);
  if ((size.width > 24) && (((smiley -> flags) & GTK_IMHTML_SMILEY_CUSTOM) != 0U)) 
/* This is a custom smiley, let's scale it */
{
    GdkPixbuf *pixbuf = (GdkPixbuf *)((void *)0);
    GtkImageType type;
    type = gtk_image_get_storage_type(((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)image),gtk_image_get_type()))));
    if (type == GTK_IMAGE_PIXBUF) {
      pixbuf = gtk_image_get_pixbuf(((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)image),gtk_image_get_type()))));
    }
    else if (type == GTK_IMAGE_ANIMATION) {
      GdkPixbufAnimation *animation;
      animation = gtk_image_get_animation(((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)image),gtk_image_get_type()))));
      pixbuf = gdk_pixbuf_animation_get_static_image(animation);
    }
    if (pixbuf != ((GdkPixbuf *)((void *)0))) {
      GdkPixbuf *resized;
      resized = gdk_pixbuf_scale_simple(pixbuf,24,24,GDK_INTERP_HYPER);
/* This unrefs pixbuf */
      gtk_image_set_from_pixbuf(((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)image),gtk_image_get_type()))),resized);
      gtk_widget_size_request(image,&size);
      g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)resized),((GType )(20 << 2))))));
    }
  }
   *width += size.width;
  button = gtk_button_new();
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_container_get_type()))),image);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"smiley_text",face);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )insert_smiley_text),toolbar,0,((GConnectFlags )0));
  gtk_tooltips_set_tip((toolbar -> tooltips),button,face,0);
/* these look really weird with borders */
  gtk_button_set_relief(((GtkButton *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_button_get_type()))),GTK_RELIEF_NONE);
  psmiley = purple_smileys_find_by_shortcut((smiley -> smile));
/* If this is a "non-custom" smiley, check to see if its shortcut is
	  "shadowed" by any custom smiley. This can only happen if the connection
	  is custom smiley-enabled */
  if (((supports_custom != 0) && (psmiley != 0)) && !(((smiley -> flags) & GTK_IMHTML_SMILEY_CUSTOM) != 0U)) {
    gchar tip[128UL];
    g_snprintf(tip,(sizeof(tip)),((const char *)(dgettext("pidgin","This smiley is disabled because a custom smiley exists for this shortcut:\n %s"))),face);
    gtk_tooltips_set_tip((toolbar -> tooltips),button,tip,0);
    gtk_widget_set_sensitive(button,0);
  }
  else if (psmiley != 0) {
/* Remove the button if the smiley is destroyed */
    g_signal_connect_object(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)psmiley),((GType )(20 << 2))))),"destroy",((GCallback )gtk_widget_destroy),button,G_CONNECT_SWAPPED);
  }
/* set current element to add */
  cur -> height = size.height;
  cur -> width = size.width;
  cur -> button = button;
  cur -> smiley = smiley;
  cur -> next = ls;
/* check where to insert by height */
  if (ls == ((struct smiley_button_list *)((void *)0))) 
    return cur;
  while(it != ((struct smiley_button_list *)((void *)0))){
    it_last = it;
    it = (it -> next);
  }
  cur -> next = it;
  it_last -> next = cur;
  return ls;
}

static gboolean smiley_is_unique(GSList *list,GtkIMHtmlSmiley *smiley)
{
  while(list != 0){
    GtkIMHtmlSmiley *cur = (GtkIMHtmlSmiley *)(list -> data);
    if (!(strcmp((cur -> file),(smiley -> file)) != 0)) 
      return 0;
    list = (list -> next);
  }
  return (!0);
}

static gboolean smiley_dialog_input_cb(GtkWidget *dialog,GdkEvent *event,GtkIMHtmlToolbar *toolbar)
{
  if ((((event -> type) == GDK_KEY_PRESS) && (event -> key.keyval == 0xff1b)) || (((event -> type) == GDK_BUTTON_PRESS) && (event -> button.button == 1))) {
    close_smiley_dialog(toolbar);
    return (!0);
  }
  return 0;
}

static void add_smiley_list(GtkWidget *container,struct smiley_button_list *list,int max_width,gboolean custom)
{
  GtkWidget *line;
  int line_width = 0;
  if (!(list != 0)) 
    return ;
  line = gtk_hbox_new(0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)container),gtk_box_get_type()))),line,0,0,0);
  for (; list != 0; list = (list -> next)) {
    if (custom != !(!((( *(list -> smiley)).flags & GTK_IMHTML_SMILEY_CUSTOM) != 0U))) 
      continue; 
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)line),gtk_box_get_type()))),(list -> button),0,0,0);
    gtk_widget_show((list -> button));
    line_width += (list -> width);
    if (line_width >= max_width) {
      if ((list -> next) != 0) {
        line = gtk_hbox_new(0,0);
        gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)container),gtk_box_get_type()))),line,0,0,0);
      }
      line_width = 0;
    }
  }
}

static void insert_smiley_cb(GtkWidget *smiley,GtkIMHtmlToolbar *toolbar)
{
  GtkWidget *dialog;
  GtkWidget *vbox;
  GtkWidget *smiley_table = (GtkWidget *)((void *)0);
  GSList *smileys;
  GSList *unique_smileys = (GSList *)((void *)0);
  const GSList *custom_smileys = (const GSList *)((void *)0);
  gboolean supports_custom = 0;
  GtkRequisition req;
  GtkWidget *scrolled;
  GtkWidget *viewport;
  if (!(gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)smiley),gtk_toggle_button_get_type())))) != 0)) {
    destroy_smiley_dialog(toolbar);
    gtk_widget_grab_focus((toolbar -> imhtml));
    return ;
  }
  if ((toolbar -> sml) != 0) 
    smileys = pidgin_themes_get_proto_smileys((toolbar -> sml));
  else 
    smileys = pidgin_themes_get_proto_smileys(0);
/* Note: prepend smileys to list to avoid O(n^2) overhead when there is
	  a large number of smileys... need to revers the list after for the dialog
	  work... */
  while(smileys != 0){
    GtkIMHtmlSmiley *smiley = (GtkIMHtmlSmiley *)(smileys -> data);
    if (!((smiley -> hidden) != 0)) {
      if (smiley_is_unique(unique_smileys,smiley) != 0) {
        unique_smileys = g_slist_prepend(unique_smileys,smiley);
      }
    }
    smileys = (smileys -> next);
  }
  supports_custom = ((gtk_imhtml_get_format_functions(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))))) & GTK_IMHTML_CUSTOM_SMILEY);
  if (((toolbar -> imhtml) != 0) && (supports_custom != 0)) {
    const GSList *iterator = (const GSList *)((void *)0);
    custom_smileys = (pidgin_smileys_get_all());
    for (iterator = custom_smileys; iterator != 0; iterator = (((iterator != 0)?( *((GSList *)iterator)).next : ((struct _GSList *)((void *)0))))) {
      GtkIMHtmlSmiley *smiley = (GtkIMHtmlSmiley *)(iterator -> data);
      unique_smileys = g_slist_prepend(unique_smileys,smiley);
    }
  }
/* we need to reverse the list to get the smileys in the correct order */
  unique_smileys = g_slist_reverse(unique_smileys);
  dialog = pidgin_create_dialog(((const char *)(dgettext("pidgin","Smile!"))),0,"smiley_dialog",0);
  gtk_window_set_position(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_window_get_type()))),GTK_WIN_POS_MOUSE);
  vbox = pidgin_dialog_get_vbox_with_properties(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),0,0);
  if (unique_smileys != ((GSList *)((void *)0))) {
    struct smiley_button_list *ls;
    int max_line_width;
    int num_lines;
    int button_width = 0;
/* We use hboxes packed in a vbox */
    ls = ((struct smiley_button_list *)((void *)0));
    max_line_width = 0;
    num_lines = (floor(sqrt((g_slist_length(unique_smileys)))));
    smiley_table = gtk_vbox_new(0,0);
    if (supports_custom != 0) {
      GtkWidget *manage = gtk_button_new_with_mnemonic(((const char *)(dgettext("pidgin","_Manage custom smileys"))));
      GtkRequisition req;
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)manage),((GType )(20 << 2))))),"clicked",((GCallback )pidgin_smiley_manager_show),0,0,((GConnectFlags )0));
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)manage),((GType )(20 << 2))))),"clicked",((GCallback )gtk_widget_destroy),dialog,0,G_CONNECT_SWAPPED);
      gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),manage,0,(!0),0);
      gtk_widget_size_request(manage,&req);
      button_width = req.width;
    }
/* create list of smileys sorted by height */
    while(unique_smileys != 0){
      GtkIMHtmlSmiley *smiley = (GtkIMHtmlSmiley *)(unique_smileys -> data);
      if (!((smiley -> hidden) != 0)) {
        ls = sort_smileys(ls,toolbar,&max_line_width,smiley);
      }
      unique_smileys = g_slist_delete_link(unique_smileys,unique_smileys);
    }
/* The window will be at least as wide as the 'Manage ..' button */
    max_line_width = ((button_width > (max_line_width / num_lines))?button_width : (max_line_width / num_lines));
/* pack buttons of the list */
    add_smiley_list(smiley_table,ls,max_line_width,0);
    if (supports_custom != 0) {
      gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)smiley_table),gtk_box_get_type()))),gtk_hseparator_new(),(!0),0,0);
      add_smiley_list(smiley_table,ls,max_line_width,(!0));
    }
    while(ls != 0){
      struct smiley_button_list *tmp = (ls -> next);
      g_free(ls);
      ls = tmp;
    }
    gtk_widget_add_events(dialog,GDK_KEY_PRESS_MASK);
  }
  else {
    smiley_table = gtk_label_new(((const char *)(dgettext("pidgin","This theme has no available smileys."))));
    gtk_widget_add_events(dialog,(GDK_KEY_PRESS_MASK | GDK_BUTTON_PRESS_MASK));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dialog),((GType )(20 << 2))))),"button-press-event",((GCallback )smiley_dialog_input_cb),toolbar,0,((GConnectFlags )0));
  }
  scrolled = pidgin_make_scrollable(smiley_table,GTK_POLICY_NEVER,GTK_POLICY_NEVER,GTK_SHADOW_NONE,-1,-1);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),scrolled,(!0),(!0),0);
  gtk_widget_show(smiley_table);
  viewport = gtk_widget_get_parent(smiley_table);
  gtk_viewport_set_shadow_type(((GtkViewport *)(g_type_check_instance_cast(((GTypeInstance *)viewport),gtk_viewport_get_type()))),GTK_SHADOW_NONE);
/* connect signals */
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dialog),((GType )(20 << 2))))),"destroy",((GCallback )close_smiley_dialog),toolbar,0,G_CONNECT_SWAPPED);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dialog),((GType )(20 << 2))))),"key-press-event",((GCallback )smiley_dialog_input_cb),toolbar,0,((GConnectFlags )0));
  gtk_window_set_transient_for(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_window_get_type()))),((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_widget_get_toplevel(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_widget_get_type())))))),gtk_window_get_type()))));
/* show everything */
  gtk_widget_show_all(dialog);
  gtk_widget_size_request(viewport,&req);
  gtk_widget_set_size_request(scrolled,((300 < req.width)?300 : req.width),((290 < req.height)?290 : req.height));
/* The window has to be made resizable, and the scrollbars in the scrolled window
	 * enabled only after setting the desired size of the window. If we do either of
	 * these tasks before now, GTK+ miscalculates the required size, and erronously
	 * makes one or both scrollbars visible (sometimes).
	 * I too think this hack is gross. But I couldn't find a better way -- sadrul */
  gtk_window_set_resizable(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_window_get_type()))),(!0));
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)scrolled),((GType )(20 << 2))))),"hscrollbar-policy",GTK_POLICY_AUTOMATIC,"vscrollbar-policy",GTK_POLICY_AUTOMATIC,((void *)((void *)0)));
#ifdef _WIN32
#endif
  toolbar -> smiley_dialog = dialog;
  gtk_widget_grab_focus((toolbar -> imhtml));
}

static void send_attention_cb(GtkWidget *attention,GtkIMHtmlToolbar *toolbar)
{
  PurpleConversation *conv = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),((GType )(20 << 2))))),"active_conv"));
  const gchar *who = purple_conversation_get_name(conv);
  PurpleConnection *gc = purple_conversation_get_gc(conv);
  toggle_button_set_active_block(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)attention),gtk_toggle_button_get_type()))),0,toolbar);
  purple_prpl_send_attention(gc,who,0);
  gtk_widget_grab_focus((toolbar -> imhtml));
}

static void update_buttons_cb(GtkIMHtml *imhtml,GtkIMHtmlButtons buttons,GtkIMHtmlToolbar *toolbar)
{
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> bold)),gtk_widget_get_type()))),(buttons & GTK_IMHTML_BOLD));
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> italic)),gtk_widget_get_type()))),(buttons & GTK_IMHTML_ITALIC));
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> underline)),gtk_widget_get_type()))),(buttons & GTK_IMHTML_UNDERLINE));
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> strikethrough)),gtk_widget_get_type()))),(buttons & GTK_IMHTML_STRIKE));
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> larger_size)),gtk_widget_get_type()))),(buttons & GTK_IMHTML_GROW));
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> smaller_size)),gtk_widget_get_type()))),(buttons & GTK_IMHTML_SHRINK));
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> font)),gtk_widget_get_type()))),(buttons & GTK_IMHTML_FACE));
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> fgcolor)),gtk_widget_get_type()))),(buttons & GTK_IMHTML_FORECOLOR));
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> bgcolor)),gtk_widget_get_type()))),(buttons & GTK_IMHTML_BACKCOLOR));
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> clear)),gtk_widget_get_type()))),((((((((((buttons & GTK_IMHTML_BOLD) != 0) || ((buttons & GTK_IMHTML_ITALIC) != 0)) || ((buttons & GTK_IMHTML_UNDERLINE) != 0)) || ((buttons & GTK_IMHTML_STRIKE) != 0)) || ((buttons & GTK_IMHTML_GROW) != 0)) || ((buttons & GTK_IMHTML_SHRINK) != 0)) || ((buttons & GTK_IMHTML_FACE) != 0)) || ((buttons & GTK_IMHTML_FORECOLOR) != 0)) || ((buttons & GTK_IMHTML_BACKCOLOR) != 0)));
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> image)),gtk_widget_get_type()))),(buttons & GTK_IMHTML_IMAGE));
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> link)),gtk_widget_get_type()))),(buttons & GTK_IMHTML_LINK));
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> smiley)),gtk_widget_get_type()))),(buttons & GTK_IMHTML_SMILEY));
}
/* we call this when we want to _set_active the toggle button, it'll
 * block the callback thats connected to the button so we don't have to
 * do the double toggling hack
 */

static void toggle_button_set_active_block(GtkToggleButton *button,gboolean is_active,GtkIMHtmlToolbar *toolbar)
{
  GObject *object;
  do {
    if (toolbar != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"toolbar");
      return ;
    };
  }while (0);
  object = (g_object_ref(button));
  g_signal_handlers_block_matched(object,G_SIGNAL_MATCH_DATA,0,0,0,0,toolbar);
  gtk_toggle_button_set_active(button,is_active);
  g_signal_handlers_unblock_matched(object,G_SIGNAL_MATCH_DATA,0,0,0,0,toolbar);
  g_object_unref(object);
}

static void update_buttons(GtkIMHtmlToolbar *toolbar)
{
  gboolean bold;
  gboolean italic;
  gboolean underline;
  gboolean strike;
  char *tmp;
  char *tmp2;
  GtkLabel *label = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),((GType )(20 << 2))))),"font_label"));
  gtk_label_set_label(label,((const char *)(dgettext("pidgin","_Font"))));
  gtk_imhtml_get_current_format(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))),&bold,&italic,&underline);
  strike = ( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type())))).edit.strike;
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> bold)),gtk_toggle_button_get_type())))) != bold) 
    toggle_button_set_active_block(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> bold)),gtk_toggle_button_get_type()))),bold,toolbar);
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> italic)),gtk_toggle_button_get_type())))) != italic) 
    toggle_button_set_active_block(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> italic)),gtk_toggle_button_get_type()))),italic,toolbar);
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> underline)),gtk_toggle_button_get_type())))) != underline) 
    toggle_button_set_active_block(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> underline)),gtk_toggle_button_get_type()))),underline,toolbar);
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> strikethrough)),gtk_toggle_button_get_type())))) != strike) 
    toggle_button_set_active_block(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> strikethrough)),gtk_toggle_button_get_type()))),strike,toolbar);
/* These buttons aren't ever "active". */
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> smaller_size)),gtk_toggle_button_get_type()))),0);
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> larger_size)),gtk_toggle_button_get_type()))),0);
  if (bold != 0) {
    gchar *markup = g_strdup_printf("<b>%s</b>",gtk_label_get_label(label));
    gtk_label_set_markup_with_mnemonic(label,markup);
    g_free(markup);
  }
  if (italic != 0) {
    gchar *markup = g_strdup_printf("<i>%s</i>",gtk_label_get_label(label));
    gtk_label_set_markup_with_mnemonic(label,markup);
    g_free(markup);
  }
  if (underline != 0) {
    gchar *markup = g_strdup_printf("<u>%s</u>",gtk_label_get_label(label));
    gtk_label_set_markup_with_mnemonic(label,markup);
    g_free(markup);
  }
  if (strike != 0) {
    gchar *markup = g_strdup_printf("<s>%s</s>",gtk_label_get_label(label));
    gtk_label_set_markup_with_mnemonic(label,markup);
    g_free(markup);
  }
  tmp = gtk_imhtml_get_current_fontface(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))));
  toggle_button_set_active_block(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> font)),gtk_toggle_button_get_type()))),(tmp != ((char *)((void *)0))),toolbar);
  if (tmp != ((char *)((void *)0))) {
    gchar *markup = g_strdup_printf("<span font_desc=\"%s\">%s</span>",tmp,gtk_label_get_label(label));
    gtk_label_set_markup_with_mnemonic(label,markup);
    g_free(markup);
  }
  g_free(tmp);
  tmp = gtk_imhtml_get_current_forecolor(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))));
  toggle_button_set_active_block(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> fgcolor)),gtk_toggle_button_get_type()))),(tmp != ((char *)((void *)0))),toolbar);
  if (tmp != ((char *)((void *)0))) {
    gchar *markup = g_strdup_printf("<span foreground=\"%s\">%s</span>",tmp,gtk_label_get_label(label));
    gtk_label_set_markup_with_mnemonic(label,markup);
    g_free(markup);
  }
  g_free(tmp);
  tmp = gtk_imhtml_get_current_backcolor(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))));
  tmp2 = gtk_imhtml_get_current_background(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type()))));
  toggle_button_set_active_block(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> bgcolor)),gtk_toggle_button_get_type()))),((tmp != ((char *)((void *)0))) || (tmp2 != ((char *)((void *)0)))),toolbar);
  if (tmp != ((char *)((void *)0))) {
    gchar *markup = g_strdup_printf("<span background=\"%s\">%s</span>",tmp,gtk_label_get_label(label));
    gtk_label_set_markup_with_mnemonic(label,markup);
    g_free(markup);
  }
  g_free(tmp);
  g_free(tmp2);
}

static void toggle_button_cb(GtkIMHtml *imhtml,GtkIMHtmlButtons buttons,GtkIMHtmlToolbar *toolbar)
{
  update_buttons(toolbar);
}

static void update_format_cb(GtkIMHtml *imhtml,GtkIMHtmlToolbar *toolbar)
{
  update_buttons(toolbar);
}

static void mark_set_cb(GtkTextBuffer *buffer,GtkTextIter *location,GtkTextMark *mark,GtkIMHtmlToolbar *toolbar)
{
  if (mark != gtk_text_buffer_get_insert(buffer)) 
    return ;
  update_buttons(toolbar);
}
/* This comes from gtkmenutoolbutton.c from gtk+
 * Copyright (C) 2003 Ricardo Fernandez Pascual
 * Copyright (C) 2004 Paolo Borelli
 */

static void menu_position_func(GtkMenu *menu,int *x,int *y,gboolean *push_in,gpointer data)
{
  GtkWidget *widget = (GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)data),gtk_widget_get_type()));
  GtkRequisition menu_req;
  gint ythickness = ( *(widget -> style)).ythickness;
  int savy;
  gtk_widget_size_request(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_widget_get_type()))),&menu_req);
  gdk_window_get_origin((widget -> window),x,y);
   *x += widget -> allocation.x;
   *y += (widget -> allocation.y + widget -> allocation.height);
  savy =  *y;
  pidgin_menu_position_func_helper(menu,x,y,push_in,data);
  if (savy > (( *y + ythickness) + 1)) 
     *y -= widget -> allocation.height;
}

static gboolean button_activate_on_click(GtkWidget *button,GdkEventButton *event,GtkIMHtmlToolbar *toolbar)
{
  if (((event -> button) == 1) && ((({
    GTypeInstance *__inst = (GTypeInstance *)button;
    GType __t = gtk_toggle_button_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) 
    gtk_widget_activate(button);
  else if ((event -> button) == 3) 
    return gtk_imhtmltoolbar_popup_menu(button,event,toolbar);
  return 0;
}

static void pidgin_menu_clicked(GtkWidget *button,GtkMenu *menu)
{
  gtk_widget_show_all(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_widget_get_type()))));
  gtk_menu_popup(menu,0,0,menu_position_func,button,0,gtk_get_current_event_time());
}

static void pidgin_menu_deactivate(GtkWidget *menu,GtkToggleButton *button)
{
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_toggle_button_get_type()))),0);
}
enum __unnamed_enum___F0_L1123_C1_LAST_SIGNAL {LAST_SIGNAL};
/* static guint signals [LAST_SIGNAL] = { 0 }; */

static void gtk_imhtmltoolbar_finalize(GObject *object)
{
  GtkIMHtmlToolbar *toolbar = (GtkIMHtmlToolbar *)(g_type_check_instance_cast(((GTypeInstance *)object),gtk_imhtmltoolbar_get_type()));
  GtkWidget *menu;
  if ((toolbar -> image_dialog) != ((GtkWidget *)((void *)0))) {
    gtk_widget_destroy((toolbar -> image_dialog));
    toolbar -> image_dialog = ((GtkWidget *)((void *)0));
  }
  destroy_toolbar_font(0,0,toolbar);
  if ((toolbar -> smiley_dialog) != ((GtkWidget *)((void *)0))) {
    g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> smiley_dialog)),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,close_smiley_dialog,toolbar);
    destroy_smiley_dialog(toolbar);
  }
  destroy_toolbar_bgcolor(0,0,toolbar);
  destroy_toolbar_fgcolor(0,0,toolbar);
  close_link_dialog(toolbar);
  if ((toolbar -> imhtml) != 0) {
    g_signal_handlers_disconnect_matched((toolbar -> imhtml),G_SIGNAL_MATCH_DATA,0,0,0,0,toolbar);
    g_signal_handlers_disconnect_matched(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> imhtml)),gtk_imhtml_get_type())))).text_buffer,G_SIGNAL_MATCH_DATA,0,0,0,0,toolbar);
  }
  g_free((toolbar -> sml));
  gtk_object_sink(((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> tooltips)),gtk_object_get_type()))));
  menu = (g_object_get_data(object,"font_menu"));
  if (menu != 0) 
    gtk_widget_destroy(menu);
  menu = (g_object_get_data(object,"insert_menu"));
  if (menu != 0) 
    gtk_widget_destroy(menu);
  purple_prefs_disconnect_by_handle(object);
  ( *( *((GObjectClass *)(g_type_check_class_cast(((GTypeClass *)parent_class),((GType )(20 << 2)))))).finalize)(object);
}

static void switch_toolbar_view(GtkWidget *item,GtkIMHtmlToolbar *toolbar)
{
  purple_prefs_set_bool("/pidgin/conversations/toolbar/wide",!(purple_prefs_get_bool("/pidgin/conversations/toolbar/wide") != 0));
}

static gboolean gtk_imhtmltoolbar_popup_menu(GtkWidget *widget,GdkEventButton *event,GtkIMHtmlToolbar *toolbar)
{
  GtkWidget *menu;
  GtkWidget *item;
  gboolean wide;
  if ((event -> button) != 3) 
    return 0;
  wide = ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> bold)),gtk_object_get_type())))).flags & GTK_VISIBLE) != 0);
  menu = gtk_menu_new();
  item = gtk_menu_item_new_with_mnemonic(((wide != 0)?((const char *)(dgettext("pidgin","Group Items"))) : ((const char *)(dgettext("pidgin","Ungroup Items")))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"activate",((GCallback )switch_toolbar_view),toolbar,0,((GConnectFlags )0));
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item);
  gtk_widget_show(item);
  gtk_menu_popup(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_get_type()))),0,0,pidgin_menu_position_func_helper,widget,(event -> button),(event -> time));
  return (!0);
}
/* Boring GTK+ stuff */

static void gtk_imhtmltoolbar_class_init(GtkIMHtmlToolbarClass *class)
{
  GObjectClass *gobject_class;
  gobject_class = ((GObjectClass *)class);
  parent_class = (g_type_class_ref(gtk_hbox_get_type()));
  gobject_class -> finalize = gtk_imhtmltoolbar_finalize;
  purple_prefs_add_none("/pidgin/conversations/toolbar");
  purple_prefs_add_bool("/pidgin/conversations/toolbar/wide",0);
}

static void gtk_imhtmltoolbar_create_old_buttons(GtkIMHtmlToolbar *toolbar)
{
  GtkWidget *hbox;
  GtkWidget *button;
  struct __unnamed_class___F0_L1218_C2_L1651R__L1652R__scope____SgSS2___variable_declaration__variable_type___Pb__c__Pe___variable_name_L1651R__L1652R__scope____SgSS2____scope__stock__DELIMITER__L1651R__L1652R__scope____SgSS2___variable_declaration__variable_type_L55R_variable_name_L1651R__L1652R__scope____SgSS2____scope__callback__DELIMITER__L1651R__L1652R__scope____SgSS2___variable_declaration__variable_type___Pb____Pb__GtkWidget_GtkWidget__typedef_declaration__Pe____Pe___variable_name_L1651R__L1652R__scope____SgSS2____scope__button__DELIMITER__L1651R__L1652R__scope____SgSS2___variable_declaration__variable_type___Pb__Cc__Pe___variable_name_L1651R__L1652R__scope____SgSS2____scope__tooltip {
  char *stock;
  gpointer callback;
  GtkWidget **button;
  const char *tooltip;}buttons[] = {{("gtk-bold"), (((GCallback )do_bold)), (&toolbar -> bold), ((const char *)(dgettext("pidgin","Bold")))}, {("gtk-italic"), (do_italic), (&toolbar -> italic), ((const char *)(dgettext("pidgin","Italic")))}, {("gtk-underline"), (do_underline), (&toolbar -> underline), ((const char *)(dgettext("pidgin","Underline")))}, {("gtk-strikethrough"), (do_strikethrough), (&toolbar -> strikethrough), ((const char *)(dgettext("pidgin","Strikethrough")))}, {(""), ((gpointer )((void *)0)), ((GtkWidget **)((void *)0)), ((const char *)((void *)0))}, {("pidgin-text-larger"), (do_big), (&toolbar -> larger_size), ((const char *)(dgettext("pidgin","Increase Font Size")))}, {("pidgin-text-smaller"), (do_small), (&toolbar -> smaller_size), ((const char *)(dgettext("pidgin","Decrease Font Size")))}, {(""), ((gpointer )((void *)0)), ((GtkWidget **)((void *)0)), ((const char *)((void *)0))}, {("pidgin-font-face"), (toggle_font), (&toolbar -> font), ((const char *)(dgettext("pidgin","Font Face")))}, {("pidgin-fgcolor"), (toggle_fg_color), (&toolbar -> fgcolor), ((const char *)(dgettext("pidgin","Foreground Color")))}, {("pidgin-bgcolor"), (toggle_bg_color), (&toolbar -> bgcolor), ((const char *)(dgettext("pidgin","Background Color")))}, {(""), ((gpointer )((void *)0)), ((GtkWidget **)((void *)0)), ((const char *)((void *)0))}, {("pidgin-clear"), (clear_formatting_cb), (&toolbar -> clear), ((const char *)(dgettext("pidgin","Reset Formatting")))}, {(""), ((gpointer )((void *)0)), ((GtkWidget **)((void *)0)), ((const char *)((void *)0))}, {("pidgin-insert-image"), (insert_image_cb), (&toolbar -> image), ((const char *)(dgettext("pidgin","Insert IM Image")))}, {("pidgin-insert-link"), (insert_link_cb), (&toolbar -> link), ((const char *)(dgettext("pidgin","Insert Link")))}, {(""), ((gpointer )((void *)0)), ((GtkWidget **)((void *)0)), ((const char *)((void *)0))}, {("pidgin-smiley"), (insert_smiley_cb), (&toolbar -> smiley), ((const char *)(dgettext("pidgin","Insert Smiley")))}, {((char *)((void *)0)), ((gpointer )((void *)0)), ((GtkWidget **)((void *)0)), ((const char *)((void *)0))}};
  int iter;
  hbox = gtk_hbox_new(0,0);
  for (iter = 0; buttons[iter].stock != 0; iter++) {
    if (buttons[iter].stock[0] != 0) {
      button = pidgin_pixbuf_toolbar_button_from_stock(buttons[iter].stock);
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"button-press-event",((GCallback )gtk_imhtmltoolbar_popup_menu),toolbar,0,((GConnectFlags )0));
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )buttons[iter].callback),toolbar,0,((GConnectFlags )0));
       *buttons[iter].button = button;
      gtk_tooltips_set_tip((toolbar -> tooltips),button,buttons[iter].tooltip,0);
    }
    else 
      button = gtk_vseparator_new();
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),button,0,0,0);
  }
/* create the attention button (this is a bit hacky to not break ABI) */
  button = pidgin_pixbuf_toolbar_button_from_stock("pidgin-send-attention");
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"button-press-event",((GCallback )gtk_imhtmltoolbar_popup_menu),toolbar,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )send_attention_cb),toolbar,0,((GConnectFlags )0));
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),((GType )(20 << 2))))),"attention",button);
  gtk_tooltips_set_tip((toolbar -> tooltips),button,((const char *)(dgettext("pidgin","Send Attention"))),0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),button,0,0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_box_get_type()))),hbox,0,0,0);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),((GType )(20 << 2))))),"wide-view",hbox);
}

static void button_visibility_changed(GtkWidget *button,gpointer dontcare,GtkWidget *item)
{
  if ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_object_get_type())))).flags & GTK_VISIBLE) != 0) 
    gtk_widget_hide(item);
  else 
    gtk_widget_show(item);
}

static void button_sensitiveness_changed(GtkWidget *button,gpointer dontcare,GtkWidget *item)
{
  gtk_widget_set_sensitive(item,(((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_object_get_type())))).flags & GTK_SENSITIVE) != 0) && ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_object_get_type())))).flags & GTK_PARENT_SENSITIVE) != 0)));
}

static void update_menuitem(GtkToggleButton *button,GtkCheckMenuItem *item)
{
  g_signal_handlers_block_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )gtk_button_clicked),button);
  gtk_check_menu_item_set_active(item,gtk_toggle_button_get_active(button));
  g_signal_handlers_unblock_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )gtk_button_clicked),button);
}

static void enable_markup(GtkWidget *widget,gpointer null)
{
  if ((({
    GTypeInstance *__inst = (GTypeInstance *)widget;
    GType __t = gtk_label_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"use-markup",!0,((void *)((void *)0)));
}

static void imhtmltoolbar_view_pref_changed(const char *name,PurplePrefType type,gconstpointer value,gpointer toolbar)
{
  if (value != 0) {
    gtk_widget_hide_all((g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),((GType )(20 << 2))))),"lean-view")));
    gtk_widget_show_all((g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),((GType )(20 << 2))))),"wide-view")));
  }
  else {
    gtk_widget_hide_all((g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),((GType )(20 << 2))))),"wide-view")));
    gtk_widget_show_all((g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),((GType )(20 << 2))))),"lean-view")));
  }
}

static void gtk_imhtmltoolbar_init(GtkIMHtmlToolbar *toolbar)
{
  GtkWidget *hbox = (GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_widget_get_type()));
  GtkWidget *event = gtk_event_box_new();
  GtkWidget *bbox;
  GtkWidget *box = gtk_hbox_new(0,0);
  GtkWidget *image;
  GtkWidget *label;
  GtkWidget *insert_button;
  GtkWidget *font_button;
  GtkWidget *smiley_button;
  GtkWidget *attention_button;
  GtkWidget *font_menu;
  GtkWidget *insert_menu;
  GtkWidget *menuitem;
  GtkWidget *sep;
  GObject *wide_attention_button;
  int i;
  struct __unnamed_class___F0_L1332_C2_L1653R__L1654R__scope____SgSS2___variable_declaration__variable_type___Pb__Cc__Pe___variable_name_L1653R__L1654R__scope____SgSS2____scope__label__DELIMITER__L1653R__L1654R__scope____SgSS2___variable_declaration__variable_type___Pb____Pb__GtkWidget_GtkWidget__typedef_declaration__Pe____Pe___variable_name_L1653R__L1654R__scope____SgSS2____scope__button__DELIMITER__L1653R__L1654R__scope____SgSS2___variable_declaration__variable_type_L54R_variable_name_L1653R__L1654R__scope____SgSS2____scope__check {
  const char *label;
  GtkWidget **button;
  gboolean check;}buttons[] = {{((const char *)(dgettext("pidgin","<b>_Bold</b>"))), (&toolbar -> bold), ((!0))}, {((const char *)(dgettext("pidgin","<i>_Italic</i>"))), (&toolbar -> italic), ((!0))}, {((const char *)(dgettext("pidgin","<u>_Underline</u>"))), (&toolbar -> underline), ((!0))}, {((const char *)(dgettext("pidgin","<span strikethrough=\'true\'>Strikethrough</span>"))), (&toolbar -> strikethrough), ((!0))}, {((const char *)(dgettext("pidgin","<span size=\'larger\'>_Larger</span>"))), (&toolbar -> larger_size), ((!0))}, 
#if 0
#endif
{((const char *)(dgettext("pidgin","<span size=\'smaller\'>_Smaller</span>"))), (&toolbar -> smaller_size), ((!0))}, 
/* If we want to show the formatting for the following items, we would
		 * need to update them when formatting changes. The above items don't need
		 * no updating nor nothin' */
{((const char *)(dgettext("pidgin","_Font face"))), (&toolbar -> font), ((!0))}, {((const char *)(dgettext("pidgin","Foreground _color"))), (&toolbar -> fgcolor), ((!0))}, {((const char *)(dgettext("pidgin","Bac_kground color"))), (&toolbar -> bgcolor), ((!0))}, {((const char *)(dgettext("pidgin","_Reset formatting"))), (&toolbar -> clear), (0)}, {((const char *)((void *)0)), ((GtkWidget **)((void *)0)), (0)}};
  toolbar -> imhtml = ((GtkWidget *)((void *)0));
  toolbar -> font_dialog = ((GtkWidget *)((void *)0));
  toolbar -> fgcolor_dialog = ((GtkWidget *)((void *)0));
  toolbar -> bgcolor_dialog = ((GtkWidget *)((void *)0));
  toolbar -> link_dialog = ((GtkWidget *)((void *)0));
  toolbar -> smiley_dialog = ((GtkWidget *)((void *)0));
  toolbar -> image_dialog = ((GtkWidget *)((void *)0));
  toolbar -> tooltips = gtk_tooltips_new();
  gtk_box_set_spacing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_box_get_type()))),3);
  gtk_imhtmltoolbar_create_old_buttons(toolbar);
/* Fonts */
  font_button = gtk_toggle_button_new();
  gtk_button_set_relief(((GtkButton *)(g_type_check_instance_cast(((GTypeInstance *)font_button),gtk_button_get_type()))),GTK_RELIEF_NONE);
  bbox = gtk_hbox_new(0,3);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)font_button),gtk_container_get_type()))),bbox);
  image = gtk_image_new_from_stock("gtk-bold",gtk_icon_size_from_name("pidgin-icon-size-tango-extra-small"));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),image,0,0,0);
  label = gtk_label_new_with_mnemonic(((const char *)(dgettext("pidgin","_Font"))));
  gtk_label_set_use_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(!0));
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)hbox),((GType )(20 << 2))))),"font_label",label);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),label,0,0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gtk_box_get_type()))),font_button,0,0,0);
  gtk_widget_show_all(font_button);
  font_menu = gtk_menu_new();
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),((GType )(20 << 2))))),"font_menu",font_menu);
  for (i = 0; buttons[i].label != 0; i++) {
    GtkWidget *old =  *buttons[i].button;
    if (buttons[i].check != 0) {
      menuitem = gtk_check_menu_item_new_with_mnemonic(buttons[i].label);
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)old),((GType )(20 << 2))))),"toggled",((GCallback )update_menuitem),menuitem,0,G_CONNECT_AFTER);
    }
    else {
      menuitem = gtk_menu_item_new_with_mnemonic(buttons[i].label);
    }
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"activate",((GCallback )gtk_button_clicked),old,0,G_CONNECT_SWAPPED);
    gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)font_menu),gtk_menu_shell_get_type()))),menuitem);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)old),((GType )(20 << 2))))),"notify::sensitive",((GCallback )button_sensitiveness_changed),menuitem,0,((GConnectFlags )0));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)old),((GType )(20 << 2))))),"notify::visible",((GCallback )button_visibility_changed),menuitem,0,((GConnectFlags )0));
    gtk_container_foreach(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_container_get_type()))),((GtkCallback )enable_markup),0);
  }
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)font_button),((GType )(20 << 2))))),"button-press-event",((GCallback )button_activate_on_click),toolbar,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)font_button),((GType )(20 << 2))))),"activate",((GCallback )pidgin_menu_clicked),font_menu,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)font_menu),((GType )(20 << 2))))),"deactivate",((GCallback )pidgin_menu_deactivate),font_button,0,((GConnectFlags )0));
/* Sep */
  sep = gtk_vseparator_new();
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gtk_box_get_type()))),sep,0,0,0);
  gtk_widget_show_all(sep);
/* Insert */
  insert_button = gtk_toggle_button_new();
  gtk_button_set_relief(((GtkButton *)(g_type_check_instance_cast(((GTypeInstance *)insert_button),gtk_button_get_type()))),GTK_RELIEF_NONE);
  bbox = gtk_hbox_new(0,3);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)insert_button),gtk_container_get_type()))),bbox);
  image = gtk_image_new_from_stock("pidgin-insert",gtk_icon_size_from_name("pidgin-icon-size-tango-extra-small"));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),image,0,0,0);
  label = gtk_label_new_with_mnemonic(((const char *)(dgettext("pidgin","_Insert"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),label,0,0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gtk_box_get_type()))),insert_button,0,0,0);
  gtk_widget_show_all(insert_button);
  insert_menu = gtk_menu_new();
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),((GType )(20 << 2))))),"insert_menu",insert_menu);
  menuitem = gtk_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","_Image"))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"activate",((GCallback )gtk_button_clicked),(toolbar -> image),0,G_CONNECT_SWAPPED);
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)insert_menu),gtk_menu_shell_get_type()))),menuitem);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> image)),((GType )(20 << 2))))),"notify::sensitive",((GCallback )button_sensitiveness_changed),menuitem,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> image)),((GType )(20 << 2))))),"notify::visible",((GCallback )button_visibility_changed),menuitem,0,((GConnectFlags )0));
  menuitem = gtk_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","_Link"))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"activate",((GCallback )gtk_button_clicked),(toolbar -> link),0,G_CONNECT_SWAPPED);
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)insert_menu),gtk_menu_shell_get_type()))),menuitem);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> link)),((GType )(20 << 2))))),"notify::sensitive",((GCallback )button_sensitiveness_changed),menuitem,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> link)),((GType )(20 << 2))))),"notify::visible",((GCallback )button_visibility_changed),menuitem,0,((GConnectFlags )0));
  menuitem = gtk_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","_Horizontal rule"))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"activate",((GCallback )insert_hr_cb),toolbar,0,((GConnectFlags )0));
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)insert_menu),gtk_menu_shell_get_type()))),menuitem);
  toolbar -> insert_hr = menuitem;
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)insert_button),((GType )(20 << 2))))),"button-press-event",((GCallback )button_activate_on_click),toolbar,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)insert_button),((GType )(20 << 2))))),"activate",((GCallback )pidgin_menu_clicked),insert_menu,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)insert_menu),((GType )(20 << 2))))),"deactivate",((GCallback )pidgin_menu_deactivate),insert_button,0,((GConnectFlags )0));
  toolbar -> sml = ((char *)((void *)0));
/* Sep */
  sep = gtk_vseparator_new();
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gtk_box_get_type()))),sep,0,0,0);
  gtk_widget_show_all(sep);
/* Smiley */
  smiley_button = gtk_button_new();
  gtk_button_set_relief(((GtkButton *)(g_type_check_instance_cast(((GTypeInstance *)smiley_button),gtk_button_get_type()))),GTK_RELIEF_NONE);
  bbox = gtk_hbox_new(0,3);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)smiley_button),gtk_container_get_type()))),bbox);
  image = gtk_image_new_from_stock("pidgin-smiley",gtk_icon_size_from_name("pidgin-icon-size-tango-extra-small"));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),image,0,0,0);
  label = gtk_label_new_with_mnemonic(((const char *)(dgettext("pidgin","_Smile!"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),label,0,0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gtk_box_get_type()))),smiley_button,0,0,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)smiley_button),((GType )(20 << 2))))),"button-press-event",((GCallback )gtk_imhtmltoolbar_popup_menu),toolbar,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)smiley_button),((GType )(20 << 2))))),"clicked",((GCallback )gtk_button_clicked),(toolbar -> smiley),0,G_CONNECT_SWAPPED);
  gtk_widget_show_all(smiley_button);
/* Sep */
  sep = gtk_vseparator_new();
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gtk_box_get_type()))),sep,0,0,0);
  gtk_widget_show_all(sep);
/* Attention */
  wide_attention_button = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),((GType )(20 << 2))))),"attention"));
  attention_button = gtk_button_new();
  gtk_button_set_relief(((GtkButton *)(g_type_check_instance_cast(((GTypeInstance *)attention_button),gtk_button_get_type()))),GTK_RELIEF_NONE);
  bbox = gtk_hbox_new(0,3);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)attention_button),gtk_container_get_type()))),bbox);
  image = gtk_image_new_from_stock("pidgin-send-attention",gtk_icon_size_from_name("pidgin-icon-size-tango-extra-small"));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),image,0,0,0);
  label = gtk_label_new_with_mnemonic(((const char *)(dgettext("pidgin","_Attention!"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),label,0,0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gtk_box_get_type()))),attention_button,0,0,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)attention_button),((GType )(20 << 2))))),"clicked",((GCallback )gtk_button_clicked),wide_attention_button,0,G_CONNECT_SWAPPED);
  gtk_widget_show_all(attention_button);
  g_signal_connect_data(wide_attention_button,"notify::sensitive",((GCallback )button_sensitiveness_changed),attention_button,0,((GConnectFlags )0));
  g_signal_connect_data(wide_attention_button,"notify::visible",((GCallback )button_visibility_changed),attention_button,0,((GConnectFlags )0));
/* set attention button to be greyed out until we get a conversation */
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)wide_attention_button),gtk_widget_get_type()))),0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),box,0,0,0);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)hbox),((GType )(20 << 2))))),"lean-view",box);
  gtk_widget_show(box);
  purple_prefs_connect_callback(toolbar,"/pidgin/conversations/toolbar/wide",imhtmltoolbar_view_pref_changed,toolbar);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),((GType )(20 << 2))))),"realize",((GCallback )purple_prefs_trigger_callback),"/pidgin/conversations/toolbar/wide",0,(G_CONNECT_AFTER | G_CONNECT_SWAPPED));
  gtk_event_box_set_visible_window(((GtkEventBox *)(g_type_check_instance_cast(((GTypeInstance *)event),gtk_event_box_get_type()))),0);
  gtk_widget_add_events(event,GDK_BUTTON_PRESS_MASK);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),event,(!0),(!0),0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)event),((GType )(20 << 2))))),"button-press-event",((GCallback )gtk_imhtmltoolbar_popup_menu),toolbar,0,((GConnectFlags )0));
  gtk_widget_show(event);
}

GtkWidget *gtk_imhtmltoolbar_new()
{
  return (GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(g_object_new(gtk_imhtmltoolbar_get_type(),0))),gtk_widget_get_type()));
}

GType gtk_imhtmltoolbar_get_type()
{
  static GType imhtmltoolbar_type = 0;
  if (!(imhtmltoolbar_type != 0UL)) {
    static const GTypeInfo imhtmltoolbar_info = {((sizeof(GtkIMHtmlToolbarClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gtk_imhtmltoolbar_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GtkIMHtmlToolbar ))), (0), ((GInstanceInitFunc )gtk_imhtmltoolbar_init), ((const GTypeValueTable *)((void *)0))};
    imhtmltoolbar_type = g_type_register_static(gtk_hbox_get_type(),"GtkIMHtmlToolbar",&imhtmltoolbar_info,0);
  }
  return imhtmltoolbar_type;
}

void gtk_imhtmltoolbar_attach(GtkIMHtmlToolbar *toolbar,GtkWidget *imhtml)
{
  GtkIMHtmlButtons buttons;
  do {
    if (toolbar != ((GtkIMHtmlToolbar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"toolbar != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)toolbar;
      GType __t = gtk_imhtmltoolbar_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_IMHTMLTOOLBAR(toolbar)");
      return ;
    };
  }while (0);
  do {
    if (imhtml != ((GtkWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"imhtml != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)imhtml;
      GType __t = gtk_imhtml_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_IMHTML(imhtml)");
      return ;
    };
  }while (0);
  toolbar -> imhtml = imhtml;
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"format_buttons_update",((GCallback )update_buttons_cb),toolbar,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"format_function_toggle",((GCallback )toggle_button_cb),toolbar,0,G_CONNECT_AFTER);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"format_function_clear",((GCallback )update_format_cb),toolbar,0,G_CONNECT_AFTER);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"format_function_update",((GCallback )update_format_cb),toolbar,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).text_buffer),((GType )(20 << 2))))),"mark-set",((GCallback )mark_set_cb),toolbar,0,G_CONNECT_AFTER);
  buttons = gtk_imhtml_get_format_functions(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))));
  update_buttons_cb(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),buttons,toolbar);
  update_buttons(toolbar);
}

void gtk_imhtmltoolbar_associate_smileys(GtkIMHtmlToolbar *toolbar,const char *proto_id)
{
  g_free((toolbar -> sml));
  toolbar -> sml = g_strdup(proto_id);
}

void gtk_imhtmltoolbar_switch_active_conversation(GtkIMHtmlToolbar *toolbar,PurpleConversation *conv)
{
  PurpleConnection *gc = purple_conversation_get_gc(conv);
  PurplePlugin *prpl = purple_connection_get_prpl(gc);
  GtkWidget *attention = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),((GType )(20 << 2))))),"attention"));
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),((GType )(20 << 2))))),"active_conv",conv);
/* gray out attention button on protocols that don't support it
	 for the time being it is always disabled for chats */
  gtk_widget_set_sensitive(attention,((((conv != 0) && (prpl != 0)) && ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM)) && (( *((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info)).send_attention != ((gboolean (*)(PurpleConnection *, const char *, guint ))((void *)0)))));
}
