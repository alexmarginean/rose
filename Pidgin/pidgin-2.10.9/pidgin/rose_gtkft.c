/**
 * @file gtkft.c GTK+ File Transfer UI
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "pidgin.h"
#include "debug.h"
#include "notify.h"
#include "ft.h"
#include "prpl.h"
#include "util.h"
#include "gtkft.h"
#include "prefs.h"
#include "pidginstock.h"
#include "gtkutils.h"
#define PIDGINXFER(xfer) \
	(PidginXferUiData *)(xfer)->ui_data
/* the maximum size of files we will try to make a thumbnail for */
#define PIDGIN_XFER_MAX_SIZE_IMAGE_THUMBNAIL 10 * 1024 * 1024

struct _PidginXferDialog 
{
  gboolean keep_open;
  gboolean auto_clear;
  gint num_transfers;
  PurpleXfer *selected_xfer;
  GtkWidget *window;
  GtkWidget *tree;
  GtkListStore *model;
  GtkWidget *expander;
  GtkWidget *table;
  GtkWidget *local_user_desc_label;
  GtkWidget *local_user_label;
  GtkWidget *remote_user_desc_label;
  GtkWidget *remote_user_label;
  GtkWidget *protocol_label;
  GtkWidget *filename_label;
  GtkWidget *localfile_label;
  GtkWidget *status_label;
  GtkWidget *speed_label;
  GtkWidget *time_elapsed_label;
  GtkWidget *time_remaining_label;
  GtkWidget *progress;
/* Buttons */
  GtkWidget *open_button;
  GtkWidget *remove_button;
  GtkWidget *stop_button;
  GtkWidget *close_button;
}
;
typedef struct __unnamed_class___F0_L84_C9_unknown_scope_and_name_variable_declaration__variable_type_L1086R_variable_name_unknown_scope_and_name__scope__iter__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L5R_variable_name_unknown_scope_and_name__scope__last_updated_time__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__in_list__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__name {
GtkTreeIter iter;
time_t last_updated_time;
gboolean in_list;
char *name;}PidginXferUiData;
static PidginXferDialog *xfer_dialog = (PidginXferDialog *)((void *)0);
enum __unnamed_enum___F0_L96_C1_COLUMN_STATUS__COMMA__COLUMN_PROGRESS__COMMA__COLUMN_FILENAME__COMMA__COLUMN_SIZE__COMMA__COLUMN_REMAINING__COMMA__COLUMN_DATA__COMMA__NUM_COLUMNS {COLUMN_STATUS,COLUMN_PROGRESS,COLUMN_FILENAME,COLUMN_SIZE,COLUMN_REMAINING,COLUMN_DATA,NUM_COLUMNS};
/**************************************************************************
 * Utility Functions
 **************************************************************************/

static void get_xfer_info_strings(PurpleXfer *xfer,char **kbsec,char **time_elapsed,char **time_remaining)
{
  double kb_sent;
  double kb_rem;
  double kbps = 0.0;
  time_t elapsed;
  time_t now;
  if ((xfer -> end_time) != 0) 
    now = (xfer -> end_time);
  else 
    now = time(0);
  kb_sent = ((purple_xfer_get_bytes_sent(xfer)) / 1024.0);
  kb_rem = ((purple_xfer_get_bytes_remaining(xfer)) / 1024.0);
  elapsed = (((xfer -> start_time) > 0)?(now - (xfer -> start_time)) : 0);
  kbps = ((elapsed > 0)?(kb_sent / elapsed) : 0);
  if (kbsec != ((char **)((void *)0))) {
     *kbsec = g_strdup_printf(((const char *)(dgettext("pidgin","%.2f KiB/s"))),kbps);
  }
  if (time_elapsed != ((char **)((void *)0))) {
    int h;
    int m;
    int s;
    int secs_elapsed;
    if ((xfer -> start_time) > 0) {
      secs_elapsed = (now - (xfer -> start_time));
      h = (secs_elapsed / 3600);
      m = ((secs_elapsed % 3600) / 60);
      s = (secs_elapsed % 60);
       *time_elapsed = g_strdup_printf("%d:%02d:%02d",h,m,s);
    }
    else {
       *time_elapsed = g_strdup(((const char *)(dgettext("pidgin","Not started"))));
    }
  }
  if (time_remaining != ((char **)((void *)0))) {
    if (purple_xfer_is_completed(xfer) != 0) {
       *time_remaining = g_strdup(((const char *)(dgettext("pidgin","Finished"))));
    }
    else if (purple_xfer_is_canceled(xfer) != 0) {
       *time_remaining = g_strdup(((const char *)(dgettext("pidgin","Cancelled"))));
    }
    else if ((purple_xfer_get_size(xfer) == 0) || ((kb_sent > 0) && (kbps == 0))) {
       *time_remaining = g_strdup(((const char *)(dgettext("pidgin","Unknown"))));
    }
    else if (kb_sent <= 0) {
       *time_remaining = g_strdup(((const char *)(dgettext("pidgin","Waiting for transfer to begin"))));
    }
    else {
      int h;
      int m;
      int s;
      int secs_remaining;
      secs_remaining = ((int )(kb_rem / kbps));
      h = (secs_remaining / 3600);
      m = ((secs_remaining % 3600) / 60);
      s = (secs_remaining % 60);
       *time_remaining = g_strdup_printf("%d:%02d:%02d",h,m,s);
    }
  }
}

static void update_title_progress(PidginXferDialog *dialog)
{
  gboolean valid;
  GtkTreeIter iter;
  int num_active_xfers = 0;
  guint64 total_bytes_xferred = 0;
  guint64 total_file_size = 0;
  if ((dialog -> window) == ((GtkWidget *)((void *)0))) 
    return ;
  valid = gtk_tree_model_get_iter_first(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter);
/* Find all active transfers */
  while(valid != 0){
    GValue val;
    PurpleXfer *xfer = (PurpleXfer *)((void *)0);
    val.g_type = 0;
    gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter,COLUMN_DATA,&val);
    xfer = (g_value_get_pointer((&val)));
    if ((purple_xfer_get_status(xfer)) == PURPLE_XFER_STATUS_STARTED) {
      num_active_xfers++;
      total_bytes_xferred += purple_xfer_get_bytes_sent(xfer);
      total_file_size += purple_xfer_get_size(xfer);
    }
    valid = gtk_tree_model_iter_next(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter);
  }
/* Update the title */
  if (num_active_xfers > 0) {
    gchar *title;
    int total_pct = 0;
    if (total_file_size > 0) {
      total_pct = ((100 * total_bytes_xferred) / total_file_size);
    }
    title = g_strdup_printf((ngettext("File Transfers - %d%% of %d file","File Transfers - %d%% of %d files",num_active_xfers)),total_pct,num_active_xfers);
    gtk_window_set_title(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> window)),gtk_window_get_type()))),title);
    g_free(title);
  }
  else {
    gtk_window_set_title(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> window)),gtk_window_get_type()))),((const char *)(dgettext("pidgin","File Transfers"))));
  }
}

static void update_detailed_info(PidginXferDialog *dialog,PurpleXfer *xfer)
{
  PidginXferUiData *data;
  char *kbsec;
  char *time_elapsed;
  char *time_remaining;
  char *status;
  char *utf8;
  if ((dialog == ((PidginXferDialog *)((void *)0))) || (xfer == ((PurpleXfer *)((void *)0)))) 
    return ;
  data = ((PidginXferUiData *)(xfer -> ui_data));
  get_xfer_info_strings(xfer,&kbsec,&time_elapsed,&time_remaining);
  status = g_strdup_printf("%d%% (%lu of %lu bytes)",((int )(purple_xfer_get_progress(xfer) * 100)),purple_xfer_get_bytes_sent(xfer),purple_xfer_get_size(xfer));
  if (purple_xfer_is_completed(xfer) != 0) {
    GdkPixbuf *pixbuf = (GdkPixbuf *)((void *)0);
    pixbuf = gtk_widget_render_icon((xfer_dialog -> window),"pidgin-file-done",GTK_ICON_SIZE_MENU,0);
    gtk_list_store_set(((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)(xfer_dialog -> model)),gtk_list_store_get_type()))),&data -> iter,COLUMN_STATUS,pixbuf,-1);
    g_object_unref(pixbuf);
  }
  if ((purple_xfer_get_type(xfer)) == PURPLE_XFER_RECEIVE) {
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> local_user_desc_label)),gtk_label_get_type()))),((const char *)(dgettext("pidgin","<b>Receiving As:</b>"))));
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> remote_user_desc_label)),gtk_label_get_type()))),((const char *)(dgettext("pidgin","<b>Receiving From:</b>"))));
  }
  else {
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> remote_user_desc_label)),gtk_label_get_type()))),((const char *)(dgettext("pidgin","<b>Sending To:</b>"))));
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> local_user_desc_label)),gtk_label_get_type()))),((const char *)(dgettext("pidgin","<b>Sending As:</b>"))));
  }
  gtk_label_set_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> local_user_label)),gtk_label_get_type()))),purple_account_get_username((xfer -> account)));
  gtk_label_set_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> remote_user_label)),gtk_label_get_type()))),(xfer -> who));
  gtk_label_set_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> protocol_label)),gtk_label_get_type()))),purple_account_get_protocol_name((xfer -> account)));
  if ((purple_xfer_get_type(xfer)) == PURPLE_XFER_RECEIVE) {
    gtk_label_set_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> filename_label)),gtk_label_get_type()))),purple_xfer_get_filename(xfer));
  }
  else {
    char *tmp;
    tmp = g_path_get_basename(purple_xfer_get_local_filename(xfer));
    utf8 = g_filename_to_utf8(tmp,(-1),0,0,0);
    g_free(tmp);
    gtk_label_set_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> filename_label)),gtk_label_get_type()))),utf8);
    g_free(utf8);
  }
  utf8 = g_filename_to_utf8(purple_xfer_get_local_filename(xfer),(-1),0,0,0);
  gtk_label_set_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> localfile_label)),gtk_label_get_type()))),utf8);
  g_free(utf8);
  gtk_label_set_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> status_label)),gtk_label_get_type()))),status);
  gtk_label_set_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> speed_label)),gtk_label_get_type()))),kbsec);
  gtk_label_set_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> time_elapsed_label)),gtk_label_get_type()))),time_elapsed);
  gtk_label_set_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> time_remaining_label)),gtk_label_get_type()))),time_remaining);
  gtk_progress_bar_set_fraction(((GtkProgressBar *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> progress)),gtk_progress_bar_get_type()))),purple_xfer_get_progress(xfer));
  g_free(kbsec);
  g_free(time_elapsed);
  g_free(time_remaining);
  g_free(status);
}

static void update_buttons(PidginXferDialog *dialog,PurpleXfer *xfer)
{
  if ((dialog -> selected_xfer) == ((PurpleXfer *)((void *)0))) {
    gtk_widget_set_sensitive((dialog -> expander),0);
    gtk_widget_set_sensitive((dialog -> open_button),0);
    gtk_widget_set_sensitive((dialog -> stop_button),0);
    gtk_widget_show((dialog -> stop_button));
    gtk_widget_hide((dialog -> remove_button));
    return ;
  }
  if ((dialog -> selected_xfer) != xfer) 
    return ;
  if (purple_xfer_is_completed(xfer) != 0) {
    gtk_widget_hide((dialog -> stop_button));
    gtk_widget_show((dialog -> remove_button));
#ifdef _WIN32
/* If using Win32... */
#else
    if ((purple_xfer_get_type(xfer)) == PURPLE_XFER_RECEIVE) {
      gtk_widget_set_sensitive((dialog -> open_button),(!0));
    }
    else {
      gtk_widget_set_sensitive((dialog -> open_button),0);
    }
#endif
    gtk_widget_set_sensitive((dialog -> remove_button),(!0));
  }
  else if (purple_xfer_is_canceled(xfer) != 0) {
    gtk_widget_hide((dialog -> stop_button));
    gtk_widget_show((dialog -> remove_button));
    gtk_widget_set_sensitive((dialog -> open_button),0);
    gtk_widget_set_sensitive((dialog -> remove_button),(!0));
  }
  else {
    gtk_widget_show((dialog -> stop_button));
    gtk_widget_hide((dialog -> remove_button));
    gtk_widget_set_sensitive((dialog -> open_button),0);
    gtk_widget_set_sensitive((dialog -> stop_button),(!0));
  }
}

static void ensure_row_selected(PidginXferDialog *dialog)
{
  GtkTreeIter iter;
  GtkTreeSelection *selection;
  selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> tree)),gtk_tree_view_get_type()))));
  if (gtk_tree_selection_get_selected(selection,0,&iter) != 0) 
    return ;
  if (gtk_tree_model_get_iter_first(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter) != 0) 
    gtk_tree_selection_select_iter(selection,&iter);
}
/**************************************************************************
 * Callbacks
 **************************************************************************/

static gint delete_win_cb(GtkWidget *w,GdkEventAny *e,gpointer d)
{
  PidginXferDialog *dialog;
  dialog = ((PidginXferDialog *)d);
  pidgin_xfer_dialog_hide(dialog);
  return (!0);
}

static void toggle_keep_open_cb(GtkWidget *w,PidginXferDialog *dialog)
{
  dialog -> keep_open = !((dialog -> keep_open) != 0);
  purple_prefs_set_bool("/pidgin/filetransfer/keep_open",(dialog -> keep_open));
}

static void toggle_clear_finished_cb(GtkWidget *w,PidginXferDialog *dialog)
{
  dialog -> auto_clear = !((dialog -> auto_clear) != 0);
  purple_prefs_set_bool("/pidgin/filetransfer/clear_finished",(dialog -> auto_clear));
}

static void selection_changed_cb(GtkTreeSelection *selection,PidginXferDialog *dialog)
{
  GtkTreeIter iter;
  PurpleXfer *xfer = (PurpleXfer *)((void *)0);
  if (gtk_tree_selection_get_selected(selection,0,&iter) != 0) {
    GValue val;
    gtk_widget_set_sensitive((dialog -> expander),(!0));
    val.g_type = 0;
    gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter,COLUMN_DATA,&val);
    xfer = (g_value_get_pointer((&val)));
    update_detailed_info(dialog,xfer);
    dialog -> selected_xfer = xfer;
  }
  else {
    gtk_expander_set_expanded(((GtkExpander *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> expander)),gtk_expander_get_type()))),0);
    gtk_widget_set_sensitive((dialog -> expander),0);
    dialog -> selected_xfer = ((PurpleXfer *)((void *)0));
  }
  update_buttons(dialog,xfer);
}

static void open_button_cb(GtkButton *button,PidginXferDialog *dialog)
{
#ifdef _WIN32
/* If using Win32... */
#else
  const char *filename = purple_xfer_get_local_filename((dialog -> selected_xfer));
  char *command = (char *)((void *)0);
  char *tmp = (char *)((void *)0);
  GError *error = (GError *)((void *)0);
  if (purple_running_gnome() != 0) {
    char *escaped = g_shell_quote(filename);
    command = g_strdup_printf("gnome-open %s",escaped);
    g_free(escaped);
  }
  else if (purple_running_kde() != 0) {
    char *escaped = g_shell_quote(filename);
    if (purple_str_has_suffix(filename,".desktop") != 0) 
      command = g_strdup_printf("kfmclient openURL %s \'text/plain\'",escaped);
    else 
      command = g_strdup_printf("kfmclient openURL %s",escaped);
    g_free(escaped);
  }
  else {
    purple_notify_uri(0,filename);
    return ;
  }
  if (purple_program_is_valid(command) != 0) {
    gint exit_status;
    if (!(g_spawn_command_line_sync(command,0,0,&exit_status,&error) != 0)) {
      tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Error launching %s: %s"))),purple_xfer_get_local_filename((dialog -> selected_xfer)),(error -> message));
      purple_notify_message(dialog,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Unable to open file."))),tmp,0,0);
      g_free(tmp);
      g_error_free(error);
    }
    if (exit_status != 0) {
      char *primary = g_strdup_printf(((const char *)(dgettext("pidgin","Error running %s"))),command);
      char *secondary = g_strdup_printf(((const char *)(dgettext("pidgin","Process returned error code %d"))),exit_status);
      purple_notify_message(dialog,PURPLE_NOTIFY_MSG_ERROR,0,primary,secondary,0,0);
      g_free(tmp);
    }
  }
#endif
}

static void remove_button_cb(GtkButton *button,PidginXferDialog *dialog)
{
  pidgin_xfer_dialog_remove_xfer(dialog,(dialog -> selected_xfer));
}

static void stop_button_cb(GtkButton *button,PidginXferDialog *dialog)
{
  purple_xfer_cancel_local((dialog -> selected_xfer));
}

static void close_button_cb(GtkButton *button,PidginXferDialog *dialog)
{
  pidgin_xfer_dialog_hide(dialog);
}
/**************************************************************************
 * Dialog Building Functions
 **************************************************************************/

static GtkWidget *setup_tree(PidginXferDialog *dialog)
{
  GtkWidget *tree;
  GtkListStore *model;
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;
  GtkTreeSelection *selection;
/* Build the tree model */
/* Transfer type, Progress Bar, Filename, Size, Remaining */
  model = gtk_list_store_new(NUM_COLUMNS,gdk_pixbuf_get_type(),((GType )(6 << 2)),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(17 << 2)));
  dialog -> model = model;
/* Create the treeview */
  dialog -> tree = (tree = gtk_tree_view_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type())))));
  gtk_tree_view_set_rules_hint(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type()))),(!0));
  selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type()))));
/* gtk_tree_selection_set_mode(selection, GTK_SELECTION_MULTIPLE); */
  gtk_widget_show(tree);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)selection),((GType )(20 << 2))))),"changed",((GCallback )selection_changed_cb),dialog,0,((GConnectFlags )0));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)model),((GType )(20 << 2))))));
/* Columns */
/* Transfer Type column */
  renderer = gtk_cell_renderer_pixbuf_new();
  column = gtk_tree_view_column_new_with_attributes(0,renderer,"pixbuf",COLUMN_STATUS,((void *)((void *)0)));
  gtk_tree_view_column_set_sizing(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_fixed_width(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),25);
  gtk_tree_view_column_set_resizable(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),(!0));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type()))),column);
/* Progress bar column */
  renderer = gtk_cell_renderer_progress_new();
  column = gtk_tree_view_column_new_with_attributes(((const char *)(dgettext("pidgin","Progress"))),renderer,"value",COLUMN_PROGRESS,((void *)((void *)0)));
  gtk_tree_view_column_set_resizable(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),(!0));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type()))),column);
/* Filename column */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes(((const char *)(dgettext("pidgin","Filename"))),renderer,"text",COLUMN_FILENAME,((void *)((void *)0)));
  gtk_tree_view_column_set_resizable(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),(!0));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type()))),column);
/* File Size column */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes(((const char *)(dgettext("pidgin","Size"))),renderer,"text",COLUMN_SIZE,((void *)((void *)0)));
  gtk_tree_view_column_set_resizable(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),(!0));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type()))),column);
/* Bytes Remaining column */
  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes(((const char *)(dgettext("pidgin","Remaining"))),renderer,"text",COLUMN_REMAINING,((void *)((void *)0)));
  gtk_tree_view_column_set_resizable(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),(!0));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type()))),column);
  gtk_tree_view_columns_autosize(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tree),gtk_tree_view_get_type()))));
  gtk_widget_show(tree);
  return tree;
}

static GtkWidget *make_info_table(PidginXferDialog *dialog)
{
  GtkWidget *table;
  GtkWidget *label;
  int i;
  struct __unnamed_class___F0_L641_C2_L1618R__L1619R__scope____SgSS2___variable_declaration__variable_type___Pb____Pb__GtkWidget_GtkWidget__typedef_declaration__Pe____Pe___variable_name_L1618R__L1619R__scope____SgSS2____scope__desc_label__DELIMITER__L1618R__L1619R__scope____SgSS2___variable_declaration__variable_type___Pb____Pb__GtkWidget_GtkWidget__typedef_declaration__Pe____Pe___variable_name_L1618R__L1619R__scope____SgSS2____scope__val_label__DELIMITER__L1618R__L1619R__scope____SgSS2___variable_declaration__variable_type___Pb__Cc__Pe___variable_name_L1618R__L1619R__scope____SgSS2____scope__desc {
  GtkWidget **desc_label;
  GtkWidget **val_label;
  const char *desc;}labels[] = {{(&dialog -> local_user_desc_label), (&dialog -> local_user_label), ((const char *)((void *)0))}, {(&dialog -> remote_user_desc_label), (&dialog -> remote_user_label), ((const char *)((void *)0))}, {(&label), (&dialog -> protocol_label), ((const char *)(dgettext("pidgin","Protocol:")))}, {(&label), (&dialog -> filename_label), ((const char *)(dgettext("pidgin","Filename:")))}, {(&label), (&dialog -> localfile_label), ((const char *)(dgettext("pidgin","Local File:")))}, {(&label), (&dialog -> status_label), ((const char *)(dgettext("pidgin","Status:")))}, {(&label), (&dialog -> speed_label), ((const char *)(dgettext("pidgin","Speed:")))}, {(&label), (&dialog -> time_elapsed_label), ((const char *)(dgettext("pidgin","Time Elapsed:")))}, {(&label), (&dialog -> time_remaining_label), ((const char *)(dgettext("pidgin","Time Remaining:")))}};
/* Setup the initial table */
  dialog -> table = (table = gtk_table_new((sizeof(labels) / sizeof(labels[0]) + 1),2,0));
  gtk_table_set_row_spacings(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),6);
  gtk_table_set_col_spacings(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),6);
/* Setup the labels */
  for (i = 0; i < sizeof(labels) / sizeof(labels[0]); i++) {
    GtkWidget *label;
    char buf[256UL];
    g_snprintf(buf,(sizeof(buf)),"<b>%s</b>",((labels[i].desc != ((const char *)((void *)0)))?labels[i].desc : ""));
     *labels[i].desc_label = (label = gtk_label_new(0));
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),buf);
    gtk_label_set_justify(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),GTK_JUSTIFY_RIGHT);
    gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
    gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),label,0,1,i,(i + 1),GTK_FILL,0,0,0);
    gtk_widget_show(label);
     *labels[i].val_label = (label = gtk_label_new(0));
    gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
    gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),label,1,2,i,(i + 1),(GTK_FILL | GTK_EXPAND),0,0,0);
    gtk_widget_show(label);
  }
/* Setup the progress bar */
  dialog -> progress = gtk_progress_bar_new();
  gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(dialog -> progress),0,2,(sizeof(labels) / sizeof(labels[0])),(sizeof(labels) / sizeof(labels[0]) + 1),GTK_FILL,GTK_FILL,0,0);
  gtk_widget_show((dialog -> progress));
  return table;
}

PidginXferDialog *pidgin_xfer_dialog_new()
{
  PidginXferDialog *dialog;
  GtkWidget *window;
  GtkWidget *vbox1;
  GtkWidget *vbox2;
  GtkWidget *expander;
  GtkWidget *alignment;
  GtkWidget *table;
  GtkWidget *checkbox;
  GtkWidget *bbox;
  dialog = ((PidginXferDialog *)(g_malloc0_n(1,(sizeof(PidginXferDialog )))));
  dialog -> keep_open = purple_prefs_get_bool("/pidgin/filetransfer/keep_open");
  dialog -> auto_clear = purple_prefs_get_bool("/pidgin/filetransfer/clear_finished");
/* Create the window. */
  dialog -> window = (window = pidgin_create_window(((const char *)(dgettext("pidgin","File Transfers"))),12,"file transfer",(!0)));
  gtk_window_set_default_size(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_window_get_type()))),450,250);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"delete_event",((GCallback )delete_win_cb),dialog,0,((GConnectFlags )0));
/* Create the parent vbox for everything. */
  vbox1 = gtk_vbox_new(0,0);
  gtk_widget_show(vbox1);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_container_get_type()))),vbox1);
/* Create the main vbox for top half of the window. */
  vbox2 = gtk_vbox_new(0,6);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox1),gtk_box_get_type()))),vbox2,(!0),(!0),0);
  gtk_widget_show(vbox2);
/* Setup the listbox */
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox2),gtk_box_get_type()))),pidgin_make_scrollable(setup_tree(dialog),GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC,GTK_SHADOW_IN,-1,140),(!0),(!0),0);
/* "Close this window when all transfers finish" */
  checkbox = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Close this window when all transfers _finish"))));
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)checkbox),gtk_toggle_button_get_type()))),!((dialog -> keep_open) != 0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)checkbox),((GType )(20 << 2))))),"toggled",((GCallback )toggle_keep_open_cb),dialog,0,((GConnectFlags )0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox2),gtk_box_get_type()))),checkbox,0,0,0);
  gtk_widget_show(checkbox);
/* "Clear finished transfers" */
  checkbox = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","C_lear finished transfers"))));
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)checkbox),gtk_toggle_button_get_type()))),(dialog -> auto_clear));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)checkbox),((GType )(20 << 2))))),"toggled",((GCallback )toggle_clear_finished_cb),dialog,0,((GConnectFlags )0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox2),gtk_box_get_type()))),checkbox,0,0,0);
  gtk_widget_show(checkbox);
/* "Download Details" arrow */
  expander = gtk_expander_new_with_mnemonic(((const char *)(dgettext("pidgin","File transfer _details"))));
  dialog -> expander = expander;
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox2),gtk_box_get_type()))),expander,0,0,0);
  gtk_widget_show(expander);
  gtk_widget_set_sensitive(expander,0);
/* Small indent make table fall under GtkExpander's label */
  alignment = gtk_alignment_new(1,0,1,1);
  gtk_alignment_set_padding(((GtkAlignment *)(g_type_check_instance_cast(((GTypeInstance *)alignment),gtk_alignment_get_type()))),0,0,20,0);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)expander),gtk_container_get_type()))),alignment);
  gtk_widget_show(alignment);
/* The table of information. */
  table = make_info_table(dialog);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)alignment),gtk_container_get_type()))),table);
  gtk_widget_show(table);
  bbox = gtk_hbutton_box_new();
  gtk_button_box_set_layout(((GtkButtonBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_button_box_get_type()))),GTK_BUTTONBOX_END);
  gtk_box_set_spacing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),6);
  gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox1),gtk_box_get_type()))),bbox,0,(!0),0);
  gtk_widget_show(bbox);
#define ADD_BUTTON(b, label, callback, callbackdata) do { \
		GtkWidget *button = gtk_button_new_from_stock(label); \
		gtk_box_pack_start(GTK_BOX(bbox), button, FALSE, FALSE, 0); \
		g_signal_connect(G_OBJECT(button), "clicked", callback, callbackdata); \
		gtk_widget_show(button); \
		b = button; \
	} while (0)
/* Open button */
  do {
    GtkWidget *button = gtk_button_new_from_stock("gtk-open");
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),button,0,0,0);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )open_button_cb),dialog,0,((GConnectFlags )0));
    gtk_widget_show(button);
    dialog -> open_button = button;
  }while (0);
  gtk_widget_set_sensitive((dialog -> open_button),0);
/* Remove button */
  do {
    GtkWidget *button = gtk_button_new_from_stock("gtk-remove");
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),button,0,0,0);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )remove_button_cb),dialog,0,((GConnectFlags )0));
    gtk_widget_show(button);
    dialog -> remove_button = button;
  }while (0);
  gtk_widget_hide((dialog -> remove_button));
/* Stop button */
  do {
    GtkWidget *button = gtk_button_new_from_stock("gtk-stop");
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),button,0,0,0);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )stop_button_cb),dialog,0,((GConnectFlags )0));
    gtk_widget_show(button);
    dialog -> stop_button = button;
  }while (0);
  gtk_widget_set_sensitive((dialog -> stop_button),0);
/* Close button */
  do {
    GtkWidget *button = gtk_button_new_from_stock("gtk-close");
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),button,0,0,0);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )close_button_cb),dialog,0,((GConnectFlags )0));
    gtk_widget_show(button);
    dialog -> close_button = button;
  }while (0);
#undef ADD_BUTTON
#ifdef _WIN32
#endif
  return dialog;
}

void pidgin_xfer_dialog_destroy(PidginXferDialog *dialog)
{
  do {
    if (dialog != ((PidginXferDialog *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"dialog != NULL");
      return ;
    };
  }while (0);
  purple_notify_close_with_handle(dialog);
  gtk_widget_destroy((dialog -> window));
  g_free(dialog);
}

void pidgin_xfer_dialog_show(PidginXferDialog *dialog)
{
  PidginXferDialog *tmp;
  if (dialog == ((PidginXferDialog *)((void *)0))) {
    tmp = pidgin_get_xfer_dialog();
    if (tmp == ((PidginXferDialog *)((void *)0))) {
      tmp = pidgin_xfer_dialog_new();
      pidgin_set_xfer_dialog(tmp);
    }
    gtk_widget_show((tmp -> window));
  }
  else {
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> window)),gtk_window_get_type()))));
  }
}

void pidgin_xfer_dialog_hide(PidginXferDialog *dialog)
{
  do {
    if (dialog != ((PidginXferDialog *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"dialog != NULL");
      return ;
    };
  }while (0);
  purple_notify_close_with_handle(dialog);
  gtk_widget_hide((dialog -> window));
}

void pidgin_xfer_dialog_add_xfer(PidginXferDialog *dialog,PurpleXfer *xfer)
{
  PidginXferUiData *data;
  PurpleXferType type;
  GdkPixbuf *pixbuf;
  char *size_str;
  char *remaining_str;
  char *lfilename;
  char *utf8;
  do {
    if (dialog != ((PidginXferDialog *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"dialog != NULL");
      return ;
    };
  }while (0);
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  purple_xfer_ref(xfer);
  data = ((PidginXferUiData *)(xfer -> ui_data));
  data -> in_list = (!0);
  pidgin_xfer_dialog_show(dialog);
  data -> last_updated_time = 0;
  type = purple_xfer_get_type(xfer);
  size_str = purple_str_size_to_units(purple_xfer_get_size(xfer));
  remaining_str = purple_str_size_to_units(purple_xfer_get_bytes_remaining(xfer));
  pixbuf = gtk_widget_render_icon((dialog -> window),(((type == PURPLE_XFER_RECEIVE)?"pidgin-download" : "pidgin-upload")),GTK_ICON_SIZE_MENU,0);
  gtk_list_store_append((dialog -> model),&data -> iter);
  lfilename = g_path_get_basename(purple_xfer_get_local_filename(xfer));
  utf8 = g_filename_to_utf8(lfilename,(-1),0,0,0);
  g_free(lfilename);
  lfilename = utf8;
  FILE *testFout = fopen("","w");
  gtk_list_store_set((dialog -> model),&data -> iter,COLUMN_STATUS,pixbuf,COLUMN_PROGRESS,0,COLUMN_FILENAME,((type == PURPLE_XFER_RECEIVE)?purple_xfer_get_filename(xfer) : lfilename),COLUMN_SIZE,size_str,COLUMN_REMAINING,((const char *)(dgettext("pidgin","Waiting for transfer to begin"))),COLUMN_DATA,xfer,-1);
  g_free(lfilename);
  gtk_tree_view_columns_autosize(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> tree)),gtk_tree_view_get_type()))));
  g_object_unref(pixbuf);
  g_free(size_str);
  g_free(remaining_str);
  dialog -> num_transfers++;
  ensure_row_selected(dialog);
  update_title_progress(dialog);
}

void pidgin_xfer_dialog_remove_xfer(PidginXferDialog *dialog,PurpleXfer *xfer)
{
  PidginXferUiData *data;
  do {
    if (dialog != ((PidginXferDialog *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"dialog != NULL");
      return ;
    };
  }while (0);
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  data = ((PidginXferUiData *)(xfer -> ui_data));
  if (data == ((PidginXferUiData *)((void *)0))) 
    return ;
  if (!((data -> in_list) != 0)) 
    return ;
  data -> in_list = 0;
  gtk_list_store_remove(((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_list_store_get_type()))),&data -> iter);
  dialog -> num_transfers--;
  ensure_row_selected(dialog);
  update_title_progress(dialog);
  purple_xfer_unref(xfer);
}

void pidgin_xfer_dialog_cancel_xfer(PidginXferDialog *dialog,PurpleXfer *xfer)
{
  PidginXferUiData *data;
  GdkPixbuf *pixbuf;
  const gchar *status;
  do {
    if (dialog != ((PidginXferDialog *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"dialog != NULL");
      return ;
    };
  }while (0);
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  data = ((PidginXferUiData *)(xfer -> ui_data));
  if (data == ((PidginXferUiData *)((void *)0))) 
    return ;
  if (!((data -> in_list) != 0)) 
    return ;
  if (((purple_xfer_get_status(xfer)) == PURPLE_XFER_STATUS_CANCEL_LOCAL) && ((dialog -> auto_clear) != 0)) {
    pidgin_xfer_dialog_remove_xfer(dialog,xfer);
    return ;
  }
  data = ((PidginXferUiData *)(xfer -> ui_data));
  update_detailed_info(dialog,xfer);
  update_title_progress(dialog);
  pixbuf = gtk_widget_render_icon((dialog -> window),"pidgin-file-cancelled",GTK_ICON_SIZE_MENU,0);
  if (purple_xfer_is_canceled(xfer) != 0) 
    status = ((const char *)(dgettext("pidgin","Cancelled")));
  else 
    status = ((const char *)(dgettext("pidgin","Failed")));
  gtk_list_store_set((dialog -> model),&data -> iter,COLUMN_STATUS,pixbuf,COLUMN_REMAINING,status,-1);
  g_object_unref(pixbuf);
  update_buttons(dialog,xfer);
}

void pidgin_xfer_dialog_update_xfer(PidginXferDialog *dialog,PurpleXfer *xfer)
{
  PidginXferUiData *data;
  char *size_str;
  char *remaining_str;
  time_t current_time;
  GtkTreeIter iter;
  gboolean valid;
  do {
    if (dialog != ((PidginXferDialog *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"dialog != NULL");
      return ;
    };
  }while (0);
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  if ((data = ((PidginXferUiData *)(xfer -> ui_data))) == ((PidginXferUiData *)((void *)0))) 
    return ;
  if ((data -> in_list) == 0) 
    return ;
  current_time = time(0);
  if (((current_time - (data -> last_updated_time)) == 0) && !(purple_xfer_is_completed(xfer) != 0)) {
/* Don't update the window more than once per second */
    return ;
  }
  data -> last_updated_time = current_time;
  size_str = purple_str_size_to_units(purple_xfer_get_size(xfer));
  remaining_str = purple_str_size_to_units(purple_xfer_get_bytes_remaining(xfer));
  gtk_list_store_set((xfer_dialog -> model),&data -> iter,COLUMN_PROGRESS,((gint )(purple_xfer_get_progress(xfer) * 100)),COLUMN_SIZE,size_str,COLUMN_REMAINING,remaining_str,-1);
  g_free(size_str);
  g_free(remaining_str);
  if (purple_xfer_is_completed(xfer) != 0) {
    GdkPixbuf *pixbuf;
    pixbuf = gtk_widget_render_icon((dialog -> window),"pidgin-file-done",GTK_ICON_SIZE_MENU,0);
    gtk_list_store_set(((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)(xfer_dialog -> model)),gtk_list_store_get_type()))),&data -> iter,COLUMN_STATUS,pixbuf,COLUMN_REMAINING,((const char *)(dgettext("pidgin","Finished"))),-1);
    g_object_unref(pixbuf);
  }
  update_title_progress(dialog);
  if (xfer == (dialog -> selected_xfer)) 
    update_detailed_info(xfer_dialog,xfer);
  if ((purple_xfer_is_completed(xfer) != 0) && ((dialog -> auto_clear) != 0)) 
    pidgin_xfer_dialog_remove_xfer(dialog,xfer);
  else 
    update_buttons(dialog,xfer);
/*
	 * If all transfers are finished, and the pref is set, then
	 * close the dialog.  Otherwise just exit this function.
	 */
  if ((dialog -> keep_open) != 0) 
    return ;
  valid = gtk_tree_model_get_iter_first(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter);
  while(valid != 0){
    GValue val;
    PurpleXfer *next;
    val.g_type = 0;
    gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter,COLUMN_DATA,&val);
    next = (g_value_get_pointer((&val)));
    if (!(purple_xfer_is_completed(next) != 0)) 
      return ;
    valid = gtk_tree_model_iter_next(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter);
  }
/* If we got to this point then we know everything is finished */
  pidgin_xfer_dialog_hide(dialog);
}
/**************************************************************************
 * File Transfer UI Ops
 **************************************************************************/

static void pidgin_xfer_new_xfer(PurpleXfer *xfer)
{
  PidginXferUiData *data;
/* This is where we're setting xfer->ui_data for the first time. */
  data = ((PidginXferUiData *)(g_malloc0_n(1,(sizeof(PidginXferUiData )))));
  xfer -> ui_data = data;
}

static void pidgin_xfer_destroy(PurpleXfer *xfer)
{
  PidginXferUiData *data;
  data = ((PidginXferUiData *)(xfer -> ui_data));
  if (data != 0) {
    g_free((data -> name));
    g_free(data);
    xfer -> ui_data = ((void *)((void *)0));
  }
}

static void pidgin_xfer_add_xfer(PurpleXfer *xfer)
{
  if (xfer_dialog == ((PidginXferDialog *)((void *)0))) 
    xfer_dialog = pidgin_xfer_dialog_new();
  pidgin_xfer_dialog_add_xfer(xfer_dialog,xfer);
}

static void pidgin_xfer_update_progress(PurpleXfer *xfer,double percent)
{
  pidgin_xfer_dialog_update_xfer(xfer_dialog,xfer);
}

static void pidgin_xfer_cancel_local(PurpleXfer *xfer)
{
  if (xfer_dialog != 0) 
    pidgin_xfer_dialog_cancel_xfer(xfer_dialog,xfer);
}

static void pidgin_xfer_cancel_remote(PurpleXfer *xfer)
{
  if (xfer_dialog != 0) 
    pidgin_xfer_dialog_cancel_xfer(xfer_dialog,xfer);
}

static void pidgin_xfer_add_thumbnail(PurpleXfer *xfer,const gchar *formats)
{
  purple_debug_info("ft","creating thumbnail for transfer\n");
  if (purple_xfer_get_size(xfer) <= (10 * 1024 * 1024)) {
    GdkPixbuf *thumbnail = pidgin_pixbuf_new_from_file_at_size(purple_xfer_get_local_filename(xfer),128,128);
    if (thumbnail != 0) {
      gchar **formats_split = g_strsplit(formats,",",0);
      gchar *buffer = (gchar *)((void *)0);
      gsize size;
      char *option_keys[2UL] = {((char *)((void *)0)), ((char *)((void *)0))};
      char *option_values[2UL] = {((char *)((void *)0)), ((char *)((void *)0))};
      int i;
      gchar *format = (gchar *)((void *)0);
{
        for (i = 0; formats_split[i] != 0; i++) {
          if (purple_strequal(formats_split[i],"jpeg") != 0) {
            purple_debug_info("ft","creating JPEG thumbnail\n");
            option_keys[0] = "quality";
            option_values[0] = "90";
            format = "jpeg";
            break; 
          }
          else if (purple_strequal(formats_split[i],"png") != 0) {
            purple_debug_info("ft","creating PNG thumbnail\n");
            option_keys[0] = "compression";
            option_values[0] = "9";
            format = "png";
            break; 
          }
        }
      }
/* Try the first format given by the PRPL without options */
      if (format == ((gchar *)((void *)0))) {
        purple_debug_info("ft","creating thumbnail of format %s as demanded by PRPL\n",formats_split[0]);
        format = formats_split[0];
      }
      gdk_pixbuf_save_to_bufferv(thumbnail,&buffer,&size,format,option_keys,option_values,0);
      if (buffer != 0) {
        gchar *mimetype = g_strdup_printf("image/%s",format);
        purple_debug_info("ft","created thumbnail of %lu bytes\n",size);
        purple_xfer_set_thumbnail(xfer,buffer,size,mimetype);
        g_free(buffer);
        g_free(mimetype);
      }
      g_object_unref(thumbnail);
      g_strfreev(formats_split);
    }
  }
}
static PurpleXferUiOps ops = {(pidgin_xfer_new_xfer), (pidgin_xfer_destroy), (pidgin_xfer_add_xfer), (pidgin_xfer_update_progress), (pidgin_xfer_cancel_local), (pidgin_xfer_cancel_remote), ((gssize (*)(PurpleXfer *, const guchar *, gssize ))((void *)0)), ((gssize (*)(PurpleXfer *, guchar **, gssize ))((void *)0)), ((void (*)(PurpleXfer *, const guchar *, gsize ))((void *)0)), (pidgin_xfer_add_thumbnail)};
/**************************************************************************
 * GTK+ File Transfer API
 **************************************************************************/

void pidgin_xfers_init()
{
  purple_prefs_add_none("/pidgin/filetransfer");
  purple_prefs_add_bool("/pidgin/filetransfer/clear_finished",(!0));
  purple_prefs_add_bool("/pidgin/filetransfer/keep_open",0);
}

void pidgin_xfers_uninit()
{
  if (xfer_dialog != ((PidginXferDialog *)((void *)0))) 
    pidgin_xfer_dialog_destroy(xfer_dialog);
}

void pidgin_set_xfer_dialog(PidginXferDialog *dialog)
{
  xfer_dialog = dialog;
}

PidginXferDialog *pidgin_get_xfer_dialog()
{
  return xfer_dialog;
}

PurpleXferUiOps *pidgin_xfers_get_ui_ops()
{
  return &ops;
}
