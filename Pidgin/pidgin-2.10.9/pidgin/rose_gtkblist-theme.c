/*
 * Buddy List Themes for Pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "gtkblist-theme.h"
#define PIDGIN_BLIST_THEME_GET_PRIVATE(Gobject) \
	((PidginBlistThemePrivate *) ((PIDGIN_BLIST_THEME(Gobject))->priv))
/******************************************************************************
 * Structs
 *****************************************************************************/
typedef struct __unnamed_class___F0_L33_C9_unknown_scope_and_name_variable_declaration__variable_type_gdoubled__typedef_declaration_variable_name_unknown_scope_and_name__scope__opacity__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GdkColor_GdkColor__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__bgcolor__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1546R__Pe___variable_name_unknown_scope_and_name__scope__layout__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GdkColor_GdkColor__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__expanded_color__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1519R__Pe___variable_name_unknown_scope_and_name__scope__expanded__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GdkColor_GdkColor__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__collapsed_color__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1519R__Pe___variable_name_unknown_scope_and_name__scope__collapsed__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GdkColor_GdkColor__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__contact_color__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1519R__Pe___variable_name_unknown_scope_and_name__scope__contact__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1519R__Pe___variable_name_unknown_scope_and_name__scope__online__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1519R__Pe___variable_name_unknown_scope_and_name__scope__away__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1519R__Pe___variable_name_unknown_scope_and_name__scope__offline__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1519R__Pe___variable_name_unknown_scope_and_name__scope__idle__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1519R__Pe___variable_name_unknown_scope_and_name__scope__message__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1519R__Pe___variable_name_unknown_scope_and_name__scope__message_nick_said__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1519R__Pe___variable_name_unknown_scope_and_name__scope__status {
/* Buddy list */
gdouble opacity;
GdkColor *bgcolor;
PidginBlistLayout *layout;
/* groups */
GdkColor *expanded_color;
PidginThemeFont *expanded;
GdkColor *collapsed_color;
PidginThemeFont *collapsed;
/* buddy */
GdkColor *contact_color;
PidginThemeFont *contact;
PidginThemeFont *online;
PidginThemeFont *away;
PidginThemeFont *offline;
PidginThemeFont *idle;
PidginThemeFont *message;
PidginThemeFont *message_nick_said;
PidginThemeFont *status;}PidginBlistThemePrivate;

struct _PidginThemeFont 
{
  gchar *font;
  gchar color[10UL];
  GdkColor *gdkcolor;
}
;
/******************************************************************************
 * Globals
 *****************************************************************************/
static GObjectClass *parent_class = (GObjectClass *)((void *)0);
/******************************************************************************
 * Enums
 *****************************************************************************/
enum __unnamed_enum___F0_L79_C1_PROP_ZERO__COMMA__PROP_BACKGROUND_COLOR__COMMA__PROP_OPACITY__COMMA__PROP_LAYOUT__COMMA__PROP_EXPANDED_COLOR__COMMA__PROP_EXPANDED_TEXT__COMMA__PROP_COLLAPSED_COLOR__COMMA__PROP_COLLAPSED_TEXT__COMMA__PROP_CONTACT_COLOR__COMMA__PROP_CONTACT__COMMA__PROP_ONLINE__COMMA__PROP_AWAY__COMMA__PROP_OFFLINE__COMMA__PROP_IDLE__COMMA__PROP_MESSAGE__COMMA__PROP_MESSAGE_NICK_SAID__COMMA__PROP_STATUS {PROP_ZERO,PROP_BACKGROUND_COLOR,PROP_OPACITY,PROP_LAYOUT,PROP_EXPANDED_COLOR,PROP_EXPANDED_TEXT,PROP_COLLAPSED_COLOR,PROP_COLLAPSED_TEXT,PROP_CONTACT_COLOR,PROP_CONTACT,PROP_ONLINE,PROP_AWAY,PROP_OFFLINE,PROP_IDLE,PROP_MESSAGE,PROP_MESSAGE_NICK_SAID,PROP_STATUS};
/******************************************************************************
 * Helpers
 *****************************************************************************/

PidginThemeFont *pidgin_theme_font_new(const gchar *face,GdkColor *color)
{
  PidginThemeFont *font = (PidginThemeFont *)(g_malloc0_n(1,(sizeof(PidginThemeFont ))));
  font -> font = g_strdup(face);
  if (color != 0) 
    pidgin_theme_font_set_color(font,color);
  return font;
}

void pidgin_theme_font_free(PidginThemeFont *pair)
{
  if (pair != ((PidginThemeFont *)((void *)0))) {
    g_free((pair -> font));
    if ((pair -> gdkcolor) != 0) 
      gdk_color_free((pair -> gdkcolor));
    g_free(pair);
  }
}

static PidginThemeFont *copy_font_and_color(const PidginThemeFont *pair)
{
  PidginThemeFont *copy;
  if (pair == ((const PidginThemeFont *)((void *)0))) 
    return 0;
  copy = ((PidginThemeFont *)(g_malloc0_n(1,(sizeof(PidginThemeFont )))));
  copy -> font = g_strdup((pair -> font));
  strncpy((copy -> color),(pair -> color),(sizeof(copy -> color) - 1));
  if ((pair -> gdkcolor) != 0) 
    copy -> gdkcolor = (((pair -> gdkcolor) != 0)?gdk_color_copy((pair -> gdkcolor)) : ((struct _GdkColor *)((void *)0)));
  return copy;
}

void pidgin_theme_font_set_font_face(PidginThemeFont *font,const gchar *face)
{
  do {
    if (font != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"font");
      return ;
    };
  }while (0);
  do {
    if (face != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"face");
      return ;
    };
  }while (0);
  g_free((font -> font));
  font -> font = g_strdup(face);
}

void pidgin_theme_font_set_color(PidginThemeFont *font,const GdkColor *color)
{
  do {
    if (font != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"font");
      return ;
    };
  }while (0);
  if ((font -> gdkcolor) != 0) 
    gdk_color_free((font -> gdkcolor));
  font -> gdkcolor = ((color != 0)?gdk_color_copy(color) : ((struct _GdkColor *)((void *)0)));
  if (color != 0) 
    g_snprintf((font -> color),(sizeof(font -> color)),"#%02x%02x%02x",((color -> red) >> 8),((color -> green) >> 8),((color -> blue) >> 8));
  else 
    (font -> color)[0] = 0;
}

const gchar *pidgin_theme_font_get_font_face(PidginThemeFont *font)
{
  do {
    if (font != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"font");
      return 0;
    };
  }while (0);
  return (font -> font);
}

const GdkColor *pidgin_theme_font_get_color(PidginThemeFont *font)
{
  do {
    if (font != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"font");
      return 0;
    };
  }while (0);
  return (font -> gdkcolor);
}

const gchar *pidgin_theme_font_get_color_describe(PidginThemeFont *font)
{
  do {
    if (font != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"font");
      return 0;
    };
  }while (0);
  return (((font -> color)[0] != 0)?(font -> color) : ((char *)((void *)0)));
}
/******************************************************************************
 * GObject Stuff
 *****************************************************************************/

static void pidgin_blist_theme_init(GTypeInstance *instance,gpointer klass)
{
  ( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)instance),pidgin_blist_theme_get_type())))).priv = ((PidginBlistThemePrivate *)(g_malloc0_n(1,(sizeof(PidginBlistThemePrivate )))));
}

static void pidgin_blist_theme_get_property(GObject *obj,guint param_id,GValue *value,GParamSpec *psec)
{
  PidginBlistTheme *theme = (PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)obj),pidgin_blist_theme_get_type()));
  switch(param_id){
    case PROP_BACKGROUND_COLOR:
{
      g_value_set_boxed(value,(pidgin_blist_theme_get_background_color(theme)));
      break; 
    }
    case PROP_OPACITY:
{
      g_value_set_double(value,pidgin_blist_theme_get_opacity(theme));
      break; 
    }
    case PROP_LAYOUT:
{
      g_value_set_pointer(value,(pidgin_blist_theme_get_layout(theme)));
      break; 
    }
    case PROP_EXPANDED_COLOR:
{
      g_value_set_boxed(value,(pidgin_blist_theme_get_expanded_background_color(theme)));
      break; 
    }
    case PROP_EXPANDED_TEXT:
{
      g_value_set_pointer(value,(pidgin_blist_theme_get_expanded_text_info(theme)));
      break; 
    }
    case PROP_COLLAPSED_COLOR:
{
      g_value_set_boxed(value,(pidgin_blist_theme_get_collapsed_background_color(theme)));
      break; 
    }
    case PROP_COLLAPSED_TEXT:
{
      g_value_set_pointer(value,(pidgin_blist_theme_get_collapsed_text_info(theme)));
      break; 
    }
    case PROP_CONTACT_COLOR:
{
      g_value_set_boxed(value,(pidgin_blist_theme_get_contact_color(theme)));
      break; 
    }
    case PROP_CONTACT:
{
      g_value_set_pointer(value,(pidgin_blist_theme_get_contact_text_info(theme)));
      break; 
    }
    case PROP_ONLINE:
{
      g_value_set_pointer(value,(pidgin_blist_theme_get_online_text_info(theme)));
      break; 
    }
    case PROP_AWAY:
{
      g_value_set_pointer(value,(pidgin_blist_theme_get_away_text_info(theme)));
      break; 
    }
    case PROP_OFFLINE:
{
      g_value_set_pointer(value,(pidgin_blist_theme_get_offline_text_info(theme)));
      break; 
    }
    case PROP_IDLE:
{
      g_value_set_pointer(value,(pidgin_blist_theme_get_idle_text_info(theme)));
      break; 
    }
    case PROP_MESSAGE:
{
      g_value_set_pointer(value,(pidgin_blist_theme_get_unread_message_text_info(theme)));
      break; 
    }
    case PROP_MESSAGE_NICK_SAID:
{
      g_value_set_pointer(value,(pidgin_blist_theme_get_unread_message_nick_said_text_info(theme)));
      break; 
    }
    case PROP_STATUS:
{
      g_value_set_pointer(value,(pidgin_blist_theme_get_status_text_info(theme)));
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)obj;
        GParamSpec *_glib__pspec = (GParamSpec *)psec;
        guint _glib__property_id = param_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","gtkblist-theme.c:254","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void pidgin_blist_theme_set_property(GObject *obj,guint param_id,const GValue *value,GParamSpec *psec)
{
  PidginBlistTheme *theme = (PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)obj),pidgin_blist_theme_get_type()));
  switch(param_id){
    case PROP_BACKGROUND_COLOR:
{
      pidgin_blist_theme_set_background_color(theme,(g_value_get_boxed(value)));
      break; 
    }
    case PROP_OPACITY:
{
      pidgin_blist_theme_set_opacity(theme,g_value_get_double(value));
      break; 
    }
    case PROP_LAYOUT:
{
      pidgin_blist_theme_set_layout(theme,(g_value_get_pointer(value)));
      break; 
    }
    case PROP_EXPANDED_COLOR:
{
      pidgin_blist_theme_set_expanded_background_color(theme,(g_value_get_boxed(value)));
      break; 
    }
    case PROP_EXPANDED_TEXT:
{
      pidgin_blist_theme_set_expanded_text_info(theme,(g_value_get_pointer(value)));
      break; 
    }
    case PROP_COLLAPSED_COLOR:
{
      pidgin_blist_theme_set_collapsed_background_color(theme,(g_value_get_boxed(value)));
      break; 
    }
    case PROP_COLLAPSED_TEXT:
{
      pidgin_blist_theme_set_collapsed_text_info(theme,(g_value_get_pointer(value)));
      break; 
    }
    case PROP_CONTACT_COLOR:
{
      pidgin_blist_theme_set_contact_color(theme,(g_value_get_boxed(value)));
      break; 
    }
    case PROP_CONTACT:
{
      pidgin_blist_theme_set_contact_text_info(theme,(g_value_get_pointer(value)));
      break; 
    }
    case PROP_ONLINE:
{
      pidgin_blist_theme_set_online_text_info(theme,(g_value_get_pointer(value)));
      break; 
    }
    case PROP_AWAY:
{
      pidgin_blist_theme_set_away_text_info(theme,(g_value_get_pointer(value)));
      break; 
    }
    case PROP_OFFLINE:
{
      pidgin_blist_theme_set_offline_text_info(theme,(g_value_get_pointer(value)));
      break; 
    }
    case PROP_IDLE:
{
      pidgin_blist_theme_set_idle_text_info(theme,(g_value_get_pointer(value)));
      break; 
    }
    case PROP_MESSAGE:
{
      pidgin_blist_theme_set_unread_message_text_info(theme,(g_value_get_pointer(value)));
      break; 
    }
    case PROP_MESSAGE_NICK_SAID:
{
      pidgin_blist_theme_set_unread_message_nick_said_text_info(theme,(g_value_get_pointer(value)));
      break; 
    }
    case PROP_STATUS:
{
      pidgin_blist_theme_set_status_text_info(theme,(g_value_get_pointer(value)));
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)obj;
        GParamSpec *_glib__pspec = (GParamSpec *)psec;
        guint _glib__property_id = param_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","gtkblist-theme.c:315","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void pidgin_blist_theme_finalize(GObject *obj)
{
  PidginBlistThemePrivate *priv;
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)obj),pidgin_blist_theme_get_type())))).priv);
/* Buddy List */
  if ((priv -> bgcolor) != 0) 
    gdk_color_free((priv -> bgcolor));
  g_free((priv -> layout));
/* Group */
  if ((priv -> expanded_color) != 0) 
    gdk_color_free((priv -> expanded_color));
  pidgin_theme_font_free((priv -> expanded));
  if ((priv -> collapsed_color) != 0) 
    gdk_color_free((priv -> collapsed_color));
  pidgin_theme_font_free((priv -> collapsed));
/* Buddy */
  if ((priv -> contact_color) != 0) 
    gdk_color_free((priv -> contact_color));
  pidgin_theme_font_free((priv -> contact));
  pidgin_theme_font_free((priv -> online));
  pidgin_theme_font_free((priv -> away));
  pidgin_theme_font_free((priv -> offline));
  pidgin_theme_font_free((priv -> idle));
  pidgin_theme_font_free((priv -> message));
  pidgin_theme_font_free((priv -> message_nick_said));
  pidgin_theme_font_free((priv -> status));
  g_free(priv);
  ( *(parent_class -> finalize))(obj);
}

static void pidgin_blist_theme_class_init(PidginBlistThemeClass *klass)
{
  GObjectClass *obj_class = (GObjectClass *)(g_type_check_class_cast(((GTypeClass *)klass),((GType )(20 << 2))));
  GParamSpec *pspec;
  parent_class = (g_type_class_peek_parent(klass));
  obj_class -> get_property = pidgin_blist_theme_get_property;
  obj_class -> set_property = pidgin_blist_theme_set_property;
  obj_class -> finalize = pidgin_blist_theme_finalize;
/* Buddy List */
  pspec = g_param_spec_boxed("background-color",((const char *)(dgettext("pidgin","Background Color"))),((const char *)(dgettext("pidgin","The background color for the buddy list"))),gdk_color_get_type(),(G_PARAM_READABLE | G_PARAM_WRITABLE));
  g_object_class_install_property(obj_class,PROP_BACKGROUND_COLOR,pspec);
  pspec = g_param_spec_pointer("layout",((const char *)(dgettext("pidgin","Layout"))),((const char *)(dgettext("pidgin","The layout of icons, name, and status of the buddy list"))),(G_PARAM_READABLE | G_PARAM_WRITABLE));
  g_object_class_install_property(obj_class,PROP_LAYOUT,pspec);
/* Group */
/* Note to translators: These two strings refer to the background color
	   of a buddy list group when in its expanded state */
  pspec = g_param_spec_boxed("expanded-color",((const char *)(dgettext("pidgin","Expanded Background Color"))),((const char *)(dgettext("pidgin","The background color of an expanded group"))),gdk_color_get_type(),(G_PARAM_READABLE | G_PARAM_WRITABLE));
  g_object_class_install_property(obj_class,PROP_EXPANDED_COLOR,pspec);
/* Note to translators: These two strings refer to the font and color
	   of a buddy list group when in its expanded state */
  pspec = g_param_spec_pointer("expanded-text",((const char *)(dgettext("pidgin","Expanded Text"))),((const char *)(dgettext("pidgin","The text information for when a group is expanded"))),(G_PARAM_READABLE | G_PARAM_WRITABLE));
  g_object_class_install_property(obj_class,PROP_EXPANDED_TEXT,pspec);
/* Note to translators: These two strings refer to the background color
	   of a buddy list group when in its collapsed state */
  pspec = g_param_spec_boxed("collapsed-color",((const char *)(dgettext("pidgin","Collapsed Background Color"))),((const char *)(dgettext("pidgin","The background color of a collapsed group"))),gdk_color_get_type(),(G_PARAM_READABLE | G_PARAM_WRITABLE));
  g_object_class_install_property(obj_class,PROP_COLLAPSED_COLOR,pspec);
/* Note to translators: These two strings refer to the font and color
	   of a buddy list group when in its collapsed state */
  pspec = g_param_spec_pointer("collapsed-text",((const char *)(dgettext("pidgin","Collapsed Text"))),((const char *)(dgettext("pidgin","The text information for when a group is collapsed"))),(G_PARAM_READABLE | G_PARAM_WRITABLE));
  g_object_class_install_property(obj_class,PROP_COLLAPSED_TEXT,pspec);
/* Buddy */
/* Note to translators: These two strings refer to the background color
	   of a buddy list contact or chat room */
  pspec = g_param_spec_boxed("contact-color",((const char *)(dgettext("pidgin","Contact/Chat Background Color"))),((const char *)(dgettext("pidgin","The background color of a contact or chat"))),gdk_color_get_type(),(G_PARAM_READABLE | G_PARAM_WRITABLE));
  g_object_class_install_property(obj_class,PROP_CONTACT_COLOR,pspec);
/* Note to translators: These two strings refer to the font and color
	   of a buddy list contact when in its expanded state */
  pspec = g_param_spec_pointer("contact",((const char *)(dgettext("pidgin","Contact Text"))),((const char *)(dgettext("pidgin","The text information for when a contact is expanded"))),(G_PARAM_READABLE | G_PARAM_WRITABLE));
  g_object_class_install_property(obj_class,PROP_CONTACT,pspec);
/* Note to translators: These two strings refer to the font and color
	   of a buddy list buddy when it is online */
  pspec = g_param_spec_pointer("online",((const char *)(dgettext("pidgin","Online Text"))),((const char *)(dgettext("pidgin","The text information for when a buddy is online"))),(G_PARAM_READABLE | G_PARAM_WRITABLE));
  g_object_class_install_property(obj_class,PROP_ONLINE,pspec);
/* Note to translators: These two strings refer to the font and color
	   of a buddy list buddy when it is away */
  pspec = g_param_spec_pointer("away",((const char *)(dgettext("pidgin","Away Text"))),((const char *)(dgettext("pidgin","The text information for when a buddy is away"))),(G_PARAM_READABLE | G_PARAM_WRITABLE));
  g_object_class_install_property(obj_class,PROP_AWAY,pspec);
/* Note to translators: These two strings refer to the font and color
	   of a buddy list buddy when it is offline */
  pspec = g_param_spec_pointer("offline",((const char *)(dgettext("pidgin","Offline Text"))),((const char *)(dgettext("pidgin","The text information for when a buddy is offline"))),(G_PARAM_READABLE | G_PARAM_WRITABLE));
  g_object_class_install_property(obj_class,PROP_OFFLINE,pspec);
/* Note to translators: These two strings refer to the font and color
	   of a buddy list buddy when it is idle */
  pspec = g_param_spec_pointer("idle",((const char *)(dgettext("pidgin","Idle Text"))),((const char *)(dgettext("pidgin","The text information for when a buddy is idle"))),(G_PARAM_READABLE | G_PARAM_WRITABLE));
  g_object_class_install_property(obj_class,PROP_IDLE,pspec);
/* Note to translators: These two strings refer to the font and color
	   of a buddy list buddy when they have sent you a new message */
  pspec = g_param_spec_pointer("message",((const char *)(dgettext("pidgin","Message Text"))),((const char *)(dgettext("pidgin","The text information for when a buddy has an unread message"))),(G_PARAM_READABLE | G_PARAM_WRITABLE));
  g_object_class_install_property(obj_class,PROP_MESSAGE,pspec);
/* Note to translators: These two strings refer to the font and color
	   of a buddy list buddy when they have sent you a new message */
  pspec = g_param_spec_pointer("message_nick_said",((const char *)(dgettext("pidgin","Message (Nick Said) Text"))),((const char *)(dgettext("pidgin","The text information for when a chat has an unread message that mentions your nickname"))),(G_PARAM_READABLE | G_PARAM_WRITABLE));
  g_object_class_install_property(obj_class,PROP_MESSAGE_NICK_SAID,pspec);
  pspec = g_param_spec_pointer("status",((const char *)(dgettext("pidgin","Status Text"))),((const char *)(dgettext("pidgin","The text information for a buddy\'s status"))),(G_PARAM_READABLE | G_PARAM_WRITABLE));
  g_object_class_install_property(obj_class,PROP_STATUS,pspec);
}

GType pidgin_blist_theme_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static GTypeInfo info = {((sizeof(PidginBlistThemeClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )pidgin_blist_theme_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(PidginBlistTheme ))), (0), (pidgin_blist_theme_init), ((const GTypeValueTable *)((void *)0))
/* base_init */
/* base_finalize */
/* class_init */
/* class_finalize */
/* class_data */
/* n_preallocs */
/* instance_init */
/* value table */
};
    type = g_type_register_static(purple_theme_get_type(),"PidginBlistTheme",(&info),0);
  }
  return type;
}
/*****************************************************************************
 * Public API functions
 *****************************************************************************/
/* get methods */

GdkColor *pidgin_blist_theme_get_background_color(PidginBlistTheme *theme)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  return priv -> bgcolor;
}

gdouble pidgin_blist_theme_get_opacity(PidginBlistTheme *theme)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return 1.0;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  return priv -> opacity;
}

PidginBlistLayout *pidgin_blist_theme_get_layout(PidginBlistTheme *theme)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  return priv -> layout;
}

GdkColor *pidgin_blist_theme_get_expanded_background_color(PidginBlistTheme *theme)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  return priv -> expanded_color;
}

PidginThemeFont *pidgin_blist_theme_get_expanded_text_info(PidginBlistTheme *theme)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  return priv -> expanded;
}

GdkColor *pidgin_blist_theme_get_collapsed_background_color(PidginBlistTheme *theme)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  return priv -> collapsed_color;
}

PidginThemeFont *pidgin_blist_theme_get_collapsed_text_info(PidginBlistTheme *theme)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  return priv -> collapsed;
}

GdkColor *pidgin_blist_theme_get_contact_color(PidginBlistTheme *theme)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  return priv -> contact_color;
}

PidginThemeFont *pidgin_blist_theme_get_contact_text_info(PidginBlistTheme *theme)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  return priv -> contact;
}

PidginThemeFont *pidgin_blist_theme_get_online_text_info(PidginBlistTheme *theme)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  return priv -> online;
}

PidginThemeFont *pidgin_blist_theme_get_away_text_info(PidginBlistTheme *theme)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  return priv -> away;
}

PidginThemeFont *pidgin_blist_theme_get_offline_text_info(PidginBlistTheme *theme)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  return priv -> offline;
}

PidginThemeFont *pidgin_blist_theme_get_idle_text_info(PidginBlistTheme *theme)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  return priv -> idle;
}

PidginThemeFont *pidgin_blist_theme_get_unread_message_text_info(PidginBlistTheme *theme)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  return priv -> message;
}

PidginThemeFont *pidgin_blist_theme_get_unread_message_nick_said_text_info(PidginBlistTheme *theme)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  return priv -> message_nick_said;
}

PidginThemeFont *pidgin_blist_theme_get_status_text_info(PidginBlistTheme *theme)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  return priv -> status;
}
/* Set Methods */

void pidgin_blist_theme_set_background_color(PidginBlistTheme *theme,const GdkColor *color)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  if ((priv -> bgcolor) != 0) 
    gdk_color_free((priv -> bgcolor));
  priv -> bgcolor = ((color != 0)?gdk_color_copy(color) : ((struct _GdkColor *)((void *)0)));
}

void pidgin_blist_theme_set_opacity(PidginBlistTheme *theme,gdouble opacity)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) || (opacity < 0.0)) || (opacity > 1.0)) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme) || opacity < 0.0 || opacity > 1.0");
      return ;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  priv -> opacity = opacity;
}

void pidgin_blist_theme_set_layout(PidginBlistTheme *theme,const PidginBlistLayout *layout)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  g_free((priv -> layout));
  priv -> layout = (g_memdup(layout,(sizeof(PidginBlistLayout ))));
}

void pidgin_blist_theme_set_expanded_background_color(PidginBlistTheme *theme,const GdkColor *color)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  if ((priv -> expanded_color) != 0) 
    gdk_color_free((priv -> expanded_color));
  priv -> expanded_color = ((color != 0)?gdk_color_copy(color) : ((struct _GdkColor *)((void *)0)));
}

void pidgin_blist_theme_set_expanded_text_info(PidginBlistTheme *theme,const PidginThemeFont *pair)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  pidgin_theme_font_free((priv -> expanded));
  priv -> expanded = copy_font_and_color(pair);
}

void pidgin_blist_theme_set_collapsed_background_color(PidginBlistTheme *theme,const GdkColor *color)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  if ((priv -> collapsed_color) != 0) 
    gdk_color_free((priv -> collapsed_color));
  priv -> collapsed_color = ((color != 0)?gdk_color_copy(color) : ((struct _GdkColor *)((void *)0)));
}

void pidgin_blist_theme_set_collapsed_text_info(PidginBlistTheme *theme,const PidginThemeFont *pair)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  pidgin_theme_font_free((priv -> collapsed));
  priv -> collapsed = copy_font_and_color(pair);
}

void pidgin_blist_theme_set_contact_color(PidginBlistTheme *theme,const GdkColor *color)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  if ((priv -> contact_color) != 0) 
    gdk_color_free((priv -> contact_color));
  priv -> contact_color = ((color != 0)?gdk_color_copy(color) : ((struct _GdkColor *)((void *)0)));
}

void pidgin_blist_theme_set_contact_text_info(PidginBlistTheme *theme,const PidginThemeFont *pair)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  pidgin_theme_font_free((priv -> contact));
  priv -> contact = copy_font_and_color(pair);
}

void pidgin_blist_theme_set_online_text_info(PidginBlistTheme *theme,const PidginThemeFont *pair)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  pidgin_theme_font_free((priv -> online));
  priv -> online = copy_font_and_color(pair);
}

void pidgin_blist_theme_set_away_text_info(PidginBlistTheme *theme,const PidginThemeFont *pair)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  pidgin_theme_font_free((priv -> away));
  priv -> away = copy_font_and_color(pair);
}

void pidgin_blist_theme_set_offline_text_info(PidginBlistTheme *theme,const PidginThemeFont *pair)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  pidgin_theme_font_free((priv -> offline));
  priv -> offline = copy_font_and_color(pair);
}

void pidgin_blist_theme_set_idle_text_info(PidginBlistTheme *theme,const PidginThemeFont *pair)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  pidgin_theme_font_free((priv -> idle));
  priv -> idle = copy_font_and_color(pair);
}

void pidgin_blist_theme_set_unread_message_text_info(PidginBlistTheme *theme,const PidginThemeFont *pair)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  pidgin_theme_font_free((priv -> message));
  priv -> message = copy_font_and_color(pair);
}

void pidgin_blist_theme_set_unread_message_nick_said_text_info(PidginBlistTheme *theme,const PidginThemeFont *pair)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  pidgin_theme_font_free((priv -> message_nick_said));
  priv -> message_nick_said = copy_font_and_color(pair);
}

void pidgin_blist_theme_set_status_text_info(PidginBlistTheme *theme,const PidginThemeFont *pair)
{
  PidginBlistThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_blist_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_BLIST_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PidginBlistThemePrivate *)( *((PidginBlistTheme *)(g_type_check_instance_cast(((GTypeInstance *)((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2)))))),pidgin_blist_theme_get_type())))).priv);
  pidgin_theme_font_free((priv -> status));
  priv -> status = copy_font_and_color(pair);
}
