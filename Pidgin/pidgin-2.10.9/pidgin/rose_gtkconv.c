/**
 * @file gtkconv.c GTK+ Conversation API
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#define _PIDGIN_GTKCONV_C_
#include "internal.h"
#include "pidgin.h"
#ifdef USE_GTKSPELL
# include <gtkspell/gtkspell.h>
# ifdef _WIN32
#  include "wspell.h"
# endif
#endif
#include <gdk/gdkkeysyms.h>
#include "account.h"
#include "cmds.h"
#include "core.h"
#include "debug.h"
#include "idle.h"
#include "imgstore.h"
#include "log.h"
#include "notify.h"
#include "prpl.h"
#include "request.h"
#include "util.h"
#include "version.h"
#include "gtkdnd-hints.h"
#include "gtkblist.h"
#include "gtkconv.h"
#include "gtkconvwin.h"
#include "gtkdialogs.h"
#include "gtkimhtml.h"
#include "gtkimhtmltoolbar.h"
#include "gtklog.h"
#include "gtkmenutray.h"
#include "gtkpounce.h"
#include "gtkprefs.h"
#include "gtkprivacy.h"
#include "gtkthemes.h"
#include "gtkutils.h"
#include "pidginstock.h"
#include "pidgintooltip.h"
#include "gtknickcolors.h"
#define CLOSE_CONV_TIMEOUT_SECS  (10 * 60)
#define AUTO_RESPONSE "&lt;AUTO-REPLY&gt; : "
typedef enum __unnamed_enum___F0_L77_C9_PIDGIN_CONV_SET_TITLE__COMMA__PIDGIN_CONV_BUDDY_ICON__COMMA__PIDGIN_CONV_MENU__COMMA__PIDGIN_CONV_TAB_ICON__COMMA__PIDGIN_CONV_TOPIC__COMMA__PIDGIN_CONV_SMILEY_THEME__COMMA__PIDGIN_CONV_COLORIZE_TITLE {PIDGIN_CONV_SET_TITLE=1,PIDGIN_CONV_BUDDY_ICON,PIDGIN_CONV_MENU=4,PIDGIN_CONV_TAB_ICON=8,PIDGIN_CONV_TOPIC=16,PIDGIN_CONV_SMILEY_THEME=32,PIDGIN_CONV_COLORIZE_TITLE=64}PidginConvFields;
enum __unnamed_enum___F0_L88_C1_CONV_ICON_COLUMN__COMMA__CONV_TEXT_COLUMN__COMMA__CONV_EMBLEM_COLUMN__COMMA__CONV_PROTOCOL_ICON_COLUMN__COMMA__CONV_NUM_COLUMNS {CONV_ICON_COLUMN,CONV_TEXT_COLUMN,CONV_EMBLEM_COLUMN,CONV_PROTOCOL_ICON_COLUMN,CONV_NUM_COLUMNS}PidginInfopaneColumns;
#define	PIDGIN_CONV_ALL	((1 << 7) - 1)
/* XXX: These color defines shouldn't really be here. But the nick-color
 * generation algorithm uses them, so keeping these around until we fix that. */
#define DEFAULT_SEND_COLOR "#204a87"
#define DEFAULT_HIGHLIGHT_COLOR "#AF7F00"
#define BUDDYICON_SIZE_MIN    32
#define BUDDYICON_SIZE_MAX    96
/* Undef this to turn off "custom-smiley" debug messages */
#define DEBUG_CUSTOM_SMILEY
#define LUMINANCE(c) (float)((0.3*(c.red))+(0.59*(c.green))+(0.11*(c.blue)))
/* From http://www.w3.org/TR/AERT#color-contrast */
#define MIN_BRIGHTNESS_CONTRAST 75
#define MIN_COLOR_CONTRAST 200
#define NUM_NICK_COLORS 220
static GdkColor *nick_colors = (GdkColor *)((void *)0);
static guint nbr_nick_colors;
typedef struct __unnamed_class___F0_L119_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__message__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L298R__Pe___variable_name_unknown_scope_and_name__scope__conv {
GtkWidget *window;
GtkWidget *entry;
GtkWidget *message;
PurpleConversation *conv;}InviteBuddyInfo;
static GtkWidget *invite_dialog = (GtkWidget *)((void *)0);
static GtkWidget *warn_close_dialog = (GtkWidget *)((void *)0);
static PidginWindow *hidden_convwin = (PidginWindow *)((void *)0);
static GList *window_list = (GList *)((void *)0);
/* Lists of status icons at all available sizes for use as window icons */
static GList *available_list = (GList *)((void *)0);
static GList *away_list = (GList *)((void *)0);
static GList *busy_list = (GList *)((void *)0);
static GList *xa_list = (GList *)((void *)0);
static GList *offline_list = (GList *)((void *)0);
static GHashTable *prpl_lists = (GHashTable *)((void *)0);
static gboolean update_send_to_selection(PidginWindow *win);
static void generate_send_to_items(PidginWindow *win);
/* Prototypes. <-- because Paco-Paco hates this comment. */
static gboolean infopane_entry_activate(PidginConversation *gtkconv);
static void got_typing_keypress(PidginConversation *gtkconv,gboolean first);
static void gray_stuff_out(PidginConversation *gtkconv);
static void add_chat_buddy_common(PurpleConversation *conv,PurpleConvChatBuddy *cb,const char *old_name);
static gboolean tab_complete(PurpleConversation *conv);
static void pidgin_conv_updated(PurpleConversation *conv,PurpleConvUpdateType type);
static void conv_set_unseen(PurpleConversation *gtkconv,PidginUnseenState state);
static void gtkconv_set_unseen(PidginConversation *gtkconv,PidginUnseenState state);
static void update_typing_icon(PidginConversation *gtkconv);
static void update_typing_message(PidginConversation *gtkconv,const char *message);
static const char *item_factory_translate_func(const char *path,gpointer func_data);
gboolean pidgin_conv_has_focus(PurpleConversation *conv);
static GdkColor *generate_nick_colors(guint *numcolors,GdkColor background);
static gboolean color_is_visible(GdkColor foreground,GdkColor background,int color_contrast,int brightness_contrast);
static GtkTextTag *get_buddy_tag(PurpleConversation *conv,const char *who,PurpleMessageFlags flag,gboolean create);
static void pidgin_conv_update_fields(PurpleConversation *conv,PidginConvFields fields);
static void focus_out_from_menubar(GtkWidget *wid,PidginWindow *win);
static void pidgin_conv_tab_pack(PidginWindow *win,PidginConversation *gtkconv);
static gboolean infopane_press_cb(GtkWidget *widget,GdkEventButton *e,PidginConversation *conv);
static void hide_conv(PidginConversation *gtkconv,gboolean closetimer);
static void pidgin_conv_set_position_size(PidginWindow *win,int x,int y,int width,int height);
static gboolean pidgin_conv_xy_to_right_infopane(PidginWindow *win,int x,int y);

static const GdkColor *get_nick_color(PidginConversation *gtkconv,const char *name)
{
  static GdkColor col;
  GtkStyle *style = gtk_widget_get_style((gtkconv -> imhtml));
  float scale;
  col = nick_colors[g_str_hash(name) % nbr_nick_colors];
  scale = ((1 - (((float )(((0.3 * (style -> base)[GTK_STATE_NORMAL].red) + (0.59 * (style -> base)[GTK_STATE_NORMAL].green)) + (0.11 * (style -> base)[GTK_STATE_NORMAL].blue))) / ((float )(((0.3 * style -> white.red) + (0.59 * style -> white.green)) + (0.11 * style -> white.blue))))) * (((float )(((0.3 * style -> white.red) + (0.59 * style -> white.green)) + (0.11 * style -> white.blue))) / ((((((col.red > col.blue)?col.red : col.blue)) > col.green)?(((col.red > col.blue)?col.red : col.blue)) : col.green))));
/* The colors are chosen to look fine on white; we should never have to darken */
  if (scale > 1) {
    col.red *= scale;
    col.green *= scale;
    col.blue *= scale;
  }
  return (&col);
}

static PurpleBlistNode *get_conversation_blist_node(PurpleConversation *conv)
{
  PurpleBlistNode *node = (PurpleBlistNode *)((void *)0);
  switch(purple_conversation_get_type(conv)){
    case PURPLE_CONV_TYPE_IM:
{
      node = ((PurpleBlistNode *)(purple_find_buddy((conv -> account),(conv -> name))));
      node = ((node != 0)?(node -> parent) : ((struct _PurpleBlistNode *)((void *)0)));
      break; 
    }
    case PURPLE_CONV_TYPE_CHAT:
{
      node = ((PurpleBlistNode *)(purple_blist_find_chat((conv -> account),(conv -> name))));
      break; 
    }
    default:
{
      break; 
    }
  }
  return node;
}
/**************************************************************************
 * Callbacks
 **************************************************************************/

static gboolean close_this_sucker(gpointer data)
{
  PidginConversation *gtkconv = data;
  GList *list = g_list_copy((gtkconv -> convs));
  g_list_foreach(list,((GFunc )purple_conversation_destroy),0);
  g_list_free(list);
  return 0;
}

static gboolean close_conv_cb(GtkButton *button,PidginConversation *gtkconv)
{
/* We are going to destroy the conversations immediately only if the 'close immediately'
	 * preference is selected. Otherwise, close the conversation after a reasonable timeout
	 * (I am going to consider 10 minutes as a 'reasonable timeout' here.
	 * For chats, close immediately if the chat is not in the buddylist, or if the chat is
	 * not marked 'Persistent' */
  PurpleConversation *conv = (gtkconv -> active_conv);
  PurpleAccount *account = purple_conversation_get_account(conv);
  const char *name = purple_conversation_get_name(conv);
{
    switch(purple_conversation_get_type(conv)){
      case PURPLE_CONV_TYPE_IM:
{
{
          if (purple_prefs_get_bool("/pidgin/conversations/im/close_immediately") != 0) 
            close_this_sucker(gtkconv);
          else 
            hide_conv(gtkconv,(!0));
          break; 
        }
      }
      case PURPLE_CONV_TYPE_CHAT:
{
{
          PurpleChat *chat = purple_blist_find_chat(account,name);
          if (!(chat != 0) || !(purple_blist_node_get_bool(&chat -> node,"gtk-persistent") != 0)) 
            close_this_sucker(gtkconv);
          else 
            hide_conv(gtkconv,0);
          break; 
        }
      }
      default:
{;
      }
    }
  }
  return (!0);
}

static gboolean lbox_size_allocate_cb(GtkWidget *w,GtkAllocation *allocation,gpointer data)
{
  purple_prefs_set_int("/pidgin/conversations/chat/userlist_width",(((allocation -> width) == 1)?0 : (allocation -> width)));
  return 0;
}

static void default_formatize(PidginConversation *c)
{
  PurpleConversation *conv = (c -> active_conv);
  gtk_imhtml_setup_entry(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(c -> entry)),gtk_imhtml_get_type()))),(conv -> features));
}

static void conversation_entry_clear(PidginConversation *gtkconv)
{
  GtkIMHtml *imhtml = (GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type()));
  gtk_source_undo_manager_begin_not_undoable_action((imhtml -> undo_manager));
  gtk_imhtml_delete(imhtml,0,0);
  gtk_source_undo_manager_end_not_undoable_action((imhtml -> undo_manager));
}

static void clear_formatting_cb(GtkIMHtml *imhtml,PidginConversation *gtkconv)
{
  default_formatize(gtkconv);
}

static const char *pidgin_get_cmd_prefix()
{
  return "/";
}

static PurpleCmdRet say_command_cb(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) 
    purple_conv_im_send(purple_conversation_get_im_data(conv),args[0]);
  else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) 
    purple_conv_chat_send(purple_conversation_get_chat_data(conv),args[0]);
  return PURPLE_CMD_RET_OK;
}

static PurpleCmdRet me_command_cb(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  char *tmp;
  tmp = g_strdup_printf("/me %s",args[0]);
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) 
    purple_conv_im_send(purple_conversation_get_im_data(conv),tmp);
  else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) 
    purple_conv_chat_send(purple_conversation_get_chat_data(conv),tmp);
  g_free(tmp);
  return PURPLE_CMD_RET_OK;
}

static PurpleCmdRet debug_command_cb(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  char *tmp;
  char *markup;
  if (!(g_ascii_strcasecmp(args[0],"version") != 0)) {
    tmp = g_strdup_printf("Using Pidgin v%s with libpurple v%s.","2.10.9",purple_core_get_version());
  }
  else if (!(g_ascii_strcasecmp(args[0],"plugins") != 0)) {
/* Show all the loaded plugins, including the protocol plugins and plugin loaders.
		 * This is intentional, since third party prpls are often sources of bugs, and some
		 * plugin loaders (e.g. mono) can also be buggy.
		 */
    GString *str = g_string_new("Loaded Plugins: ");
    const GList *plugins = (purple_plugins_get_loaded());
    if (plugins != 0) {
      for (; plugins != 0; plugins = (plugins -> next)) {
        str = g_string_append(str,purple_plugin_get_name((plugins -> data)));
        if ((plugins -> next) != 0) 
          str = g_string_append(str,", ");
      }
    }
    else {
      str = g_string_append(str,"(none)");
    }
    tmp = g_string_free(str,0);
  }
  else {
    purple_conversation_write(conv,0,((const char *)(dgettext("pidgin","Supported debug options are: plugins version"))),(PURPLE_MESSAGE_NO_LOG | PURPLE_MESSAGE_ERROR),time(0));
    return PURPLE_CMD_RET_OK;
  }
  markup = g_markup_escape_text(tmp,(-1));
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) 
    purple_conv_im_send(purple_conversation_get_im_data(conv),markup);
  else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) 
    purple_conv_chat_send(purple_conversation_get_chat_data(conv),markup);
  g_free(tmp);
  g_free(markup);
  return PURPLE_CMD_RET_OK;
}

static void clear_conversation_scrollback_cb(PurpleConversation *conv,void *data)
{
  PidginConversation *gtkconv = (PidginConversation *)((void *)0);
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  if (gtkconv != 0) 
    gtk_imhtml_delete(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),0,0);
}

static PurpleCmdRet clear_command_cb(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  purple_conversation_clear_message_history(conv);
  return PURPLE_CMD_RET_OK;
}

static PurpleCmdRet clearall_command_cb(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  purple_conversation_foreach(purple_conversation_clear_message_history);
  return PURPLE_CMD_RET_OK;
}

static PurpleCmdRet help_command_cb(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  GList *l;
  GList *text;
  GString *s;
  if (args[0] != ((char *)((void *)0))) {
    s = g_string_new("");
    text = purple_cmd_help(conv,args[0]);
    if (text != 0) {
      for (l = text; l != 0; l = (l -> next)) 
        if ((l -> next) != 0) 
          g_string_append_printf(s,"%s\n",((char *)(l -> data)));
        else 
          g_string_append_printf(s,"%s",((char *)(l -> data)));
    }
    else {
      g_string_append(s,((const char *)(dgettext("pidgin","No such command (in this context)."))));
    }
  }
  else {
    s = g_string_new(((const char *)(dgettext("pidgin","Use \"/help &lt;command&gt;\" for help on a specific command.\nThe following commands are available in this context:\n"))));
    text = purple_cmd_list(conv);
    for (l = text; l != 0; l = (l -> next)) 
      if ((l -> next) != 0) 
        g_string_append_printf(s,"%s, ",((char *)(l -> data)));
      else 
        g_string_append_printf(s,"%s.",((char *)(l -> data)));
    g_list_free(text);
  }
  purple_conversation_write(conv,0,(s -> str),PURPLE_MESSAGE_NO_LOG,time(0));
  g_string_free(s,(!0));
  return PURPLE_CMD_RET_OK;
}

static void send_history_add(PidginConversation *gtkconv,const char *message)
{
  GList *first;
  first = g_list_first((gtkconv -> send_history));
  g_free((first -> data));
  first -> data = (g_strdup(message));
  gtkconv -> send_history = g_list_prepend(first,0);
}

static gboolean check_for_and_do_command(PurpleConversation *conv)
{
  PidginConversation *gtkconv;
  char *cmd;
  const char *prefix;
  GtkTextIter start;
  gboolean retval = 0;
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  prefix = pidgin_get_cmd_prefix();
  cmd = gtk_imhtml_get_text(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type()))),0,0);
  gtk_text_buffer_get_start_iter(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type())))).text_buffer,&start);
  if (((cmd != 0) && (strncmp(cmd,prefix,strlen(prefix)) == 0)) && !(gtk_text_iter_get_child_anchor((&start)) != 0)) {
    PurpleCmdStatus status;
    char *error;
    char *cmdline;
    char *markup;
    char *send_history;
    GtkTextIter end;
    send_history = gtk_imhtml_get_markup(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type()))));
    send_history_add(gtkconv,send_history);
    g_free(send_history);
    cmdline = (cmd + strlen(prefix));
    if (strcmp(cmdline,"xyzzy") == 0) {
      purple_conversation_write(conv,"","Nothing happens",PURPLE_MESSAGE_NO_LOG,time(0));
      g_free(cmd);
      return (!0);
    }
    gtk_text_iter_forward_chars(&start,(g_utf8_strlen(prefix,(-1))));
    gtk_text_buffer_get_end_iter(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type())))).text_buffer,&end);
    markup = gtk_imhtml_get_markup_range(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type()))),&start,&end);
    status = purple_cmd_do_command(conv,cmdline,markup,&error);
    g_free(markup);
{
      switch(status){
        case PURPLE_CMD_STATUS_OK:
{
          retval = (!0);
          break; 
        }
        case PURPLE_CMD_STATUS_NOT_FOUND:
{
{
            PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
            PurpleConnection *gc;
            if ((gc = purple_conversation_get_gc(conv)) != 0) 
              prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info);
            if ((prpl_info != ((PurplePluginProtocolInfo *)((void *)0))) && (((prpl_info -> options) & OPT_PROTO_SLASH_COMMANDS_NATIVE) != 0U)) {
              char *spaceslash;
/* If the first word in the entered text has a '/' in it, then the user
						 * probably didn't mean it as a command. So send the text as message. */
              spaceslash = cmdline;
              while(((( *spaceslash) != 0) && (( *spaceslash) != 32)) && (( *spaceslash) != '/'))
                spaceslash++;
              if (( *spaceslash) != '/') {
                purple_conversation_write(conv,"",((const char *)(dgettext("pidgin","Unknown command."))),PURPLE_MESSAGE_NO_LOG,time(0));
                retval = (!0);
              }
            }
            break; 
          }
        }
        case PURPLE_CMD_STATUS_WRONG_ARGS:
{
          purple_conversation_write(conv,"",((const char *)(dgettext("pidgin","Syntax Error:  You typed the wrong number of arguments to that command."))),PURPLE_MESSAGE_NO_LOG,time(0));
          retval = (!0);
          break; 
        }
        case PURPLE_CMD_STATUS_FAILED:
{
          purple_conversation_write(conv,"",((error != 0)?error : ((const char *)(dgettext("pidgin","Your command failed for an unknown reason.")))),PURPLE_MESSAGE_NO_LOG,time(0));
          g_free(error);
          retval = (!0);
          break; 
        }
        case PURPLE_CMD_STATUS_WRONG_TYPE:
{
          if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) 
            purple_conversation_write(conv,"",((const char *)(dgettext("pidgin","That command only works in chats, not IMs."))),PURPLE_MESSAGE_NO_LOG,time(0));
          else 
            purple_conversation_write(conv,"",((const char *)(dgettext("pidgin","That command only works in IMs, not chats."))),PURPLE_MESSAGE_NO_LOG,time(0));
          retval = (!0);
          break; 
        }
        case PURPLE_CMD_STATUS_WRONG_PRPL:
{
          purple_conversation_write(conv,"",((const char *)(dgettext("pidgin","That command doesn\'t work on this protocol."))),PURPLE_MESSAGE_NO_LOG,time(0));
          retval = (!0);
          break; 
        }
      }
    }
  }
  g_free(cmd);
  return retval;
}

static void send_cb(GtkWidget *widget,PidginConversation *gtkconv)
{
  PurpleConversation *conv = (gtkconv -> active_conv);
  PurpleAccount *account;
  PurpleConnection *gc;
  PurpleMessageFlags flags = 0;
  char *buf;
  char *clean;
  account = purple_conversation_get_account(conv);
  if (check_for_and_do_command(conv) != 0) {
    conversation_entry_clear(gtkconv);
    return ;
  }
  if (((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) && (purple_conv_chat_has_left(purple_conversation_get_chat_data(conv)) != 0)) 
    return ;
  if (!(purple_account_is_connected(account) != 0)) 
    return ;
  buf = gtk_imhtml_get_markup(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type()))));
  clean = gtk_imhtml_get_text(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type()))),0,0);
  gtk_widget_grab_focus((gtkconv -> entry));
  if (strlen(clean) == 0) {
    g_free(buf);
    g_free(clean);
    return ;
  }
  purple_idle_touch();
/* XXX: is there a better way to tell if the message has images? */
  if (( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type())))).im_images != ((GSList *)((void *)0))) 
    flags |= PURPLE_MESSAGE_IMAGES;
  gc = purple_account_get_connection(account);
  if ((gc != 0) && (((conv -> features) & PURPLE_CONNECTION_NO_NEWLINES) != 0U)) {
    char **bufs;
    int i;
    bufs = gtk_imhtml_get_markup_lines(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type()))));
    for (i = 0; bufs[i] != 0; i++) {
      send_history_add(gtkconv,bufs[i]);
      if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) 
        purple_conv_im_send_with_flags(purple_conversation_get_im_data(conv),bufs[i],flags);
      else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) 
        purple_conv_chat_send_with_flags(purple_conversation_get_chat_data(conv),bufs[i],flags);
    }
    g_strfreev(bufs);
  }
  else {
    send_history_add(gtkconv,buf);
    if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) 
      purple_conv_im_send_with_flags(purple_conversation_get_im_data(conv),buf,flags);
    else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) 
      purple_conv_chat_send_with_flags(purple_conversation_get_chat_data(conv),buf,flags);
  }
  g_free(clean);
  g_free(buf);
  conversation_entry_clear(gtkconv);
  gtkconv_set_unseen(gtkconv,PIDGIN_UNSEEN_NONE);
}

static void add_remove_cb(GtkWidget *widget,PidginConversation *gtkconv)
{
  PurpleAccount *account;
  const char *name;
  PurpleConversation *conv = (gtkconv -> active_conv);
  account = purple_conversation_get_account(conv);
  name = purple_conversation_get_name(conv);
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
    PurpleBuddy *b;
    b = purple_find_buddy(account,name);
    if (b != ((PurpleBuddy *)((void *)0))) 
      pidgin_dialogs_remove_buddy(b);
    else if ((account != ((PurpleAccount *)((void *)0))) && (purple_account_is_connected(account) != 0)) 
      purple_blist_request_add_buddy(account,((char *)name),0,0);
  }
  else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) {
    PurpleChat *c;
    c = purple_blist_find_chat(account,name);
    if (c != ((PurpleChat *)((void *)0))) 
      pidgin_dialogs_remove_chat(c);
    else if ((account != ((PurpleAccount *)((void *)0))) && (purple_account_is_connected(account) != 0)) 
      purple_blist_request_add_chat(account,0,0,name);
  }
  gtk_widget_grab_focus(( *((PidginConversation *)(conv -> ui_data))).entry);
}

static void chat_do_info(PidginConversation *gtkconv,const char *who)
{
  PurpleConversation *conv = (gtkconv -> active_conv);
  PurpleConnection *gc;
  if ((gc = purple_conversation_get_gc(conv)) != 0) {
    pidgin_retrieve_user_info_in_chat(gc,who,purple_conv_chat_get_id((purple_conversation_get_chat_data(conv))));
  }
}

static void info_cb(GtkWidget *widget,PidginConversation *gtkconv)
{
  PurpleConversation *conv = (gtkconv -> active_conv);
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
    pidgin_retrieve_user_info(purple_conversation_get_gc(conv),purple_conversation_get_name(conv));
    gtk_widget_grab_focus((gtkconv -> entry));
  }
  else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) {
/* Get info of the person currently selected in the GtkTreeView */
    PidginChatPane *gtkchat;
    GtkTreeIter iter;
    GtkTreeModel *model;
    GtkTreeSelection *sel;
    char *name;
    gtkchat = gtkconv -> u.chat;
    model = gtk_tree_view_get_model(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> list)),gtk_tree_view_get_type()))));
    sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> list)),gtk_tree_view_get_type()))));
    if (gtk_tree_selection_get_selected(sel,0,&iter) != 0) 
      gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,CHAT_USERS_NAME_COLUMN,&name,-1);
    else 
      return ;
    chat_do_info(gtkconv,name);
    g_free(name);
  }
}

static void block_cb(GtkWidget *widget,PidginConversation *gtkconv)
{
  PurpleConversation *conv = (gtkconv -> active_conv);
  PurpleAccount *account;
  account = purple_conversation_get_account(conv);
  if ((account != ((PurpleAccount *)((void *)0))) && (purple_account_is_connected(account) != 0)) 
    pidgin_request_add_block(account,purple_conversation_get_name(conv));
  gtk_widget_grab_focus(( *((PidginConversation *)(conv -> ui_data))).entry);
}

static void unblock_cb(GtkWidget *widget,PidginConversation *gtkconv)
{
  PurpleConversation *conv = (gtkconv -> active_conv);
  PurpleAccount *account;
  account = purple_conversation_get_account(conv);
  if ((account != ((PurpleAccount *)((void *)0))) && (purple_account_is_connected(account) != 0)) 
    pidgin_request_add_permit(account,purple_conversation_get_name(conv));
  gtk_widget_grab_focus(( *((PidginConversation *)(conv -> ui_data))).entry);
}

static gboolean chat_invite_filter(const PidginBuddyCompletionEntry *entry,gpointer data)
{
  PurpleAccount *filter_account = data;
  PurpleAccount *account = (PurpleAccount *)((void *)0);
  if ((entry -> is_buddy) != 0) {
    if (((entry -> entry.buddy != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(entry -> entry.buddy))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(entry -> entry.buddy))) != 0)) 
      account = purple_buddy_get_account(entry -> entry.buddy);
    else 
      return 0;
  }
  else {
    account = ( *entry -> entry.logged_buddy).account;
  }
  if (account == filter_account) 
    return (!0);
  return 0;
}

static void do_invite(GtkWidget *w,int resp,InviteBuddyInfo *info)
{
  const char *buddy;
  const char *message;
  PurpleConversation *conv;
  conv = (info -> conv);
  if (resp == GTK_RESPONSE_OK) {
    buddy = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(info -> entry)),gtk_entry_get_type()))));
    message = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(info -> message)),gtk_entry_get_type()))));
    if (!(g_ascii_strcasecmp(buddy,"") != 0)) 
      return ;
    serv_chat_invite(purple_conversation_get_gc(conv),purple_conv_chat_get_id((purple_conversation_get_chat_data(conv))),message,buddy);
  }
  gtk_widget_destroy(invite_dialog);
  invite_dialog = ((GtkWidget *)((void *)0));
  g_free(info);
}

static void invite_dnd_recv(GtkWidget *widget,GdkDragContext *dc,gint x,gint y,GtkSelectionData *sd,guint inf,guint t,gpointer data)
{
  InviteBuddyInfo *info = (InviteBuddyInfo *)data;
  const char *convprotocol;
  gboolean success = (!0);
  convprotocol = purple_account_get_protocol_id((purple_conversation_get_account((info -> conv))));
  if ((sd -> target) == gdk_atom_intern("PURPLE_BLIST_NODE",0)) {
    PurpleBlistNode *node = (PurpleBlistNode *)((void *)0);
    PurpleBuddy *buddy;
    memcpy((&node),(sd -> data),(sizeof(node)));
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) 
      buddy = purple_contact_get_priority_buddy(((PurpleContact *)node));
    else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) 
      buddy = ((PurpleBuddy *)node);
    else 
      return ;
    if (strcmp(convprotocol,purple_account_get_protocol_id((buddy -> account))) != 0) {
      purple_notify_message(((PidginConversation *)( *(info -> conv)).ui_data),PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","That buddy is not on the same protocol as this chat."))),0,0,0);
      success = 0;
    }
    else 
      gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(info -> entry)),gtk_entry_get_type()))),purple_buddy_get_name(buddy));
    gtk_drag_finish(dc,success,((dc -> action) == GDK_ACTION_MOVE),t);
  }
  else if ((sd -> target) == gdk_atom_intern("application/x-im-contact",0)) {
    char *protocol = (char *)((void *)0);
    char *username = (char *)((void *)0);
    PurpleAccount *account;
    if (pidgin_parse_x_im_contact(((const char *)(sd -> data)),0,&account,&protocol,&username,0) != 0) {
      if (account == ((PurpleAccount *)((void *)0))) {
        purple_notify_message(((PidginConversation *)( *(info -> conv)).ui_data),PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","You are not currently signed on with an account that can invite that buddy."))),0,0,0);
      }
      else if (strcmp(convprotocol,purple_account_get_protocol_id(account)) != 0) {
        purple_notify_message(((PidginConversation *)( *(info -> conv)).ui_data),PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","That buddy is not on the same protocol as this chat."))),0,0,0);
        success = 0;
      }
      else {
        gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(info -> entry)),gtk_entry_get_type()))),username);
      }
    }
    g_free(username);
    g_free(protocol);
    gtk_drag_finish(dc,success,((dc -> action) == GDK_ACTION_MOVE),t);
  }
}
static const GtkTargetEntry dnd_targets[] = {{("PURPLE_BLIST_NODE"), (GTK_TARGET_SAME_APP), (0)}, {("application/x-im-contact"), (0), (1)}};

static void invite_cb(GtkWidget *widget,PidginConversation *gtkconv)
{
  PurpleConversation *conv = (gtkconv -> active_conv);
  InviteBuddyInfo *info = (InviteBuddyInfo *)((void *)0);
  if (invite_dialog == ((GtkWidget *)((void *)0))) {
    PidginWindow *gtkwin;
    GtkWidget *label;
    GtkWidget *vbox;
    GtkWidget *hbox;
    GtkWidget *table;
    GtkWidget *img;
    img = gtk_image_new_from_stock("pidgin-dialog-question",gtk_icon_size_from_name("pidgin-icon-size-tango-huge"));
    info = ((InviteBuddyInfo *)(g_malloc0_n(1,(sizeof(InviteBuddyInfo )))));
    info -> conv = conv;
    gtkwin = pidgin_conv_get_window(gtkconv);
/* Create the new dialog. */
    invite_dialog = gtk_dialog_new_with_buttons(((const char *)(dgettext("pidgin","Invite Buddy Into Chat Room"))),((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(gtkwin -> window)),gtk_window_get_type()))),0,"gtk-cancel",GTK_RESPONSE_CANCEL,"pidgin-invite",GTK_RESPONSE_OK,((void *)((void *)0)));
    gtk_dialog_set_default_response(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)invite_dialog),gtk_dialog_get_type()))),GTK_RESPONSE_OK);
    gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)invite_dialog),gtk_container_get_type()))),6);
    gtk_window_set_resizable(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)invite_dialog),gtk_window_get_type()))),0);
    gtk_dialog_set_has_separator(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)invite_dialog),gtk_dialog_get_type()))),0);
    info -> window = ((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)invite_dialog),gtk_widget_get_type())));
/* Setup the outside spacing. */
    vbox = ( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)invite_dialog),gtk_dialog_get_type())))).vbox;
    gtk_box_set_spacing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),12);
    gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),6);
/* Setup the inner hbox and put the dialog's icon in it. */
    hbox = gtk_hbox_new(0,12);
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),hbox);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),img,0,0,0);
    gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)img),gtk_misc_get_type()))),0,0);
/* Setup the right vbox. */
    vbox = gtk_vbox_new(0,0);
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_container_get_type()))),vbox);
/* Put our happy label in it. */
    label = gtk_label_new(((const char *)(dgettext("pidgin","Please enter the name of the user you wish to invite, along with an optional invite message."))));
    gtk_widget_set_size_request(label,350,(-1));
    gtk_label_set_line_wrap(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(!0));
    gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),label,0,0,0);
/* hbox for the table, and to give it some spacing on the left. */
    hbox = gtk_hbox_new(0,6);
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),hbox);
/* Setup the table we're going to use to lay stuff out. */
    table = gtk_table_new(2,2,0);
    gtk_table_set_row_spacings(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),6);
    gtk_table_set_col_spacings(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),6);
    gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_container_get_type()))),12);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),table,0,0,0);
/* Now the Buddy label */
    label = gtk_label_new(0);
    gtk_label_set_markup_with_mnemonic(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),((const char *)(dgettext("pidgin","_Buddy:"))));
    gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0);
    gtk_table_attach_defaults(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),label,0,1,0,1);
/* Now the Buddy drop-down entry field. */
    info -> entry = gtk_entry_new();
    pidgin_setup_screenname_autocomplete_with_filter((info -> entry),0,chat_invite_filter,(purple_conversation_get_account(conv)));
    gtk_table_attach_defaults(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(info -> entry),1,2,0,1);
    gtk_label_set_mnemonic_widget(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(info -> entry));
/* Now the label for "Message" */
    label = gtk_label_new(0);
    gtk_label_set_markup_with_mnemonic(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),((const char *)(dgettext("pidgin","_Message:"))));
    gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0);
    gtk_table_attach_defaults(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),label,0,1,1,2);
/* And finally, the Message entry field. */
    info -> message = gtk_entry_new();
    gtk_entry_set_activates_default(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(info -> message)),gtk_entry_get_type()))),(!0));
    gtk_table_attach_defaults(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),(info -> message),1,2,1,2);
    gtk_label_set_mnemonic_widget(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(info -> message));
/* Connect the signals. */
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)invite_dialog),((GType )(20 << 2))))),"response",((GCallback )do_invite),info,0,((GConnectFlags )0));
/* Setup drag-and-drop */
    gtk_drag_dest_set((info -> window),(GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_DROP),dnd_targets,(sizeof(dnd_targets) / sizeof(GtkTargetEntry )),GDK_ACTION_COPY);
    gtk_drag_dest_set((info -> entry),(GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_DROP),dnd_targets,(sizeof(dnd_targets) / sizeof(GtkTargetEntry )),GDK_ACTION_COPY);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(info -> window)),((GType )(20 << 2))))),"drag_data_received",((GCallback )invite_dnd_recv),info,0,((GConnectFlags )0));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(info -> entry)),((GType )(20 << 2))))),"drag_data_received",((GCallback )invite_dnd_recv),info,0,((GConnectFlags )0));
  }
  gtk_widget_show_all(invite_dialog);
  if (info != ((InviteBuddyInfo *)((void *)0))) 
    gtk_widget_grab_focus((info -> entry));
}

static void menu_new_conv_cb(gpointer data,guint action,GtkWidget *widget)
{
  pidgin_dialogs_im();
}

static void menu_join_chat_cb(gpointer data,guint action,GtkWidget *widget)
{
  pidgin_blist_joinchat_show();
}

static void savelog_writefile_cb(void *user_data,const char *filename)
{
  PurpleConversation *conv = (PurpleConversation *)user_data;
  FILE *fp;
  const char *name;
  char **lines;
  gchar *text;
  if ((fp = fopen(filename,"w+")) == ((FILE *)((void *)0))) {
    purple_notify_message(((PidginConversation *)(conv -> ui_data)),PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Unable to open file."))),0,0,0);
    return ;
  }
  name = purple_conversation_get_name(conv);
  fprintf(fp,"<html>\n<head>\n");
  fprintf(fp,"<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">\n");
  fprintf(fp,"<title>%s</title>\n</head>\n<body>\n",name);
  fprintf(fp,((const char *)(dgettext("pidgin","<h1>Conversation with %s</h1>\n"))),name);
  lines = gtk_imhtml_get_markup_lines(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)( *((PidginConversation *)(conv -> ui_data))).imhtml),gtk_imhtml_get_type()))));
  text = g_strjoinv("<br>\n",lines);
  fprintf(fp,"%s",text);
  g_free(text);
  g_strfreev(lines);
  fprintf(fp,"\n</body>\n</html>\n");
  fclose(fp);
}
/*
 * It would be kinda cool if this gave the option of saving a
 * plaintext v. HTML file.
 */

static void menu_save_as_cb(gpointer data,guint action,GtkWidget *widget)
{
  PidginWindow *win = data;
  PurpleConversation *conv = pidgin_conv_window_get_active_conversation(win);
  PurpleBuddy *buddy = purple_find_buddy((conv -> account),(conv -> name));
  const char *name;
  gchar *buf;
  gchar *c;
  if (buddy != ((PurpleBuddy *)((void *)0))) 
    name = purple_buddy_get_contact_alias(buddy);
  else 
    name = purple_normalize((conv -> account),(conv -> name));
  buf = g_strdup_printf("%s.html",name);
  for (c = buf; ( *c) != 0; c++) {
    if ((( *c) == '/') || (( *c) == '\\')) 
       *c = 32;
  }
  purple_request_file(((PidginConversation *)(conv -> ui_data)),((const char *)(dgettext("pidgin","Save Conversation"))),buf,(!0),((GCallback )savelog_writefile_cb),0,0,0,conv,conv);
  g_free(buf);
}

static void menu_view_log_cb(gpointer data,guint action,GtkWidget *widget)
{
  PidginWindow *win = data;
  PurpleConversation *conv;
  PurpleLogType type;
  PidginBuddyList *gtkblist;
  GdkCursor *cursor;
  const char *name;
  PurpleAccount *account;
  GSList *buddies;
  GSList *cur;
  conv = pidgin_conv_window_get_active_conversation(win);
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) 
    type = PURPLE_LOG_IM;
  else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) 
    type = PURPLE_LOG_CHAT;
  else 
    return ;
  gtkblist = pidgin_blist_get_default_gtk_blist();
  cursor = gdk_cursor_new(GDK_WATCH);
  gdk_window_set_cursor(( *(gtkblist -> window)).window,cursor);
  gdk_window_set_cursor(( *(win -> window)).window,cursor);
  gdk_cursor_unref(cursor);
  gdk_display_flush(gdk_drawable_get_display(((GdkDrawable *)(g_type_check_instance_cast(((GTypeInstance *)(widget -> window)),gdk_drawable_get_type())))));
  name = purple_conversation_get_name(conv);
  account = purple_conversation_get_account(conv);
  buddies = purple_find_buddies(account,name);
  for (cur = buddies; cur != ((GSList *)((void *)0)); cur = (cur -> next)) {
    PurpleBlistNode *node = (cur -> data);
    if ((node != ((PurpleBlistNode *)((void *)0))) && (((node -> prev) != ((PurpleBlistNode *)((void *)0))) || ((node -> next) != ((PurpleBlistNode *)((void *)0))))) {
      pidgin_log_show_contact(((PurpleContact *)(node -> parent)));
      g_slist_free(buddies);
      gdk_window_set_cursor(( *(gtkblist -> window)).window,0);
      gdk_window_set_cursor(( *(win -> window)).window,0);
      return ;
    }
  }
  g_slist_free(buddies);
  pidgin_log_show(type,name,account);
  gdk_window_set_cursor(( *(gtkblist -> window)).window,0);
  gdk_window_set_cursor(( *(win -> window)).window,0);
}

static void menu_clear_cb(gpointer data,guint action,GtkWidget *widget)
{
  PidginWindow *win = data;
  PurpleConversation *conv;
  conv = pidgin_conv_window_get_active_conversation(win);
  purple_conversation_clear_message_history(conv);
}

static void menu_find_cb(gpointer data,guint action,GtkWidget *widget)
{
  PidginWindow *gtkwin = data;
  PidginConversation *gtkconv = pidgin_conv_window_get_active_gtkconv(gtkwin);
  gtk_widget_show_all(gtkconv -> quickfind.container);
  gtk_widget_grab_focus(gtkconv -> quickfind.entry);
}
#ifdef USE_VV

static void menu_initiate_media_call_cb(gpointer data,guint action,GtkWidget *widget)
{
  PidginWindow *win = (PidginWindow *)data;
  PurpleConversation *conv = pidgin_conv_window_get_active_conversation(win);
  PurpleAccount *account = purple_conversation_get_account(conv);
  purple_prpl_initiate_media(account,purple_conversation_get_name(conv),((action == 0)?PURPLE_MEDIA_AUDIO : (((action == 1)?PURPLE_MEDIA_VIDEO : (((action == 2)?PURPLE_MEDIA_AUDIO | PURPLE_MEDIA_VIDEO : PURPLE_MEDIA_NONE))))));
}
#endif

static void menu_send_file_cb(gpointer data,guint action,GtkWidget *widget)
{
  PidginWindow *win = data;
  PurpleConversation *conv = pidgin_conv_window_get_active_conversation(win);
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
    serv_send_file(purple_conversation_get_gc(conv),purple_conversation_get_name(conv),0);
  }
}

static void menu_get_attention_cb(gpointer data,guint action,GtkWidget *widget)
{
  PidginWindow *win = data;
  PurpleConversation *conv = pidgin_conv_window_get_active_conversation(win);
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
    purple_prpl_send_attention(purple_conversation_get_gc(conv),purple_conversation_get_name(conv),0);
  }
}

static void menu_add_pounce_cb(gpointer data,guint action,GtkWidget *widget)
{
  PidginWindow *win = data;
  PurpleConversation *conv;
  conv = ( *pidgin_conv_window_get_active_gtkconv(win)).active_conv;
  pidgin_pounce_editor_show(purple_conversation_get_account(conv),purple_conversation_get_name(conv),0);
}

static void menu_insert_link_cb(gpointer data,guint action,GtkWidget *widget)
{
  PidginWindow *win = data;
  PidginConversation *gtkconv;
  GtkIMHtmlToolbar *toolbar;
  gtkconv = pidgin_conv_window_get_active_gtkconv(win);
  toolbar = ((GtkIMHtmlToolbar *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> toolbar)),gtk_imhtmltoolbar_get_type())));
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> link)),gtk_toggle_button_get_type()))),!(gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> link)),gtk_toggle_button_get_type())))) != 0));
}

static void menu_insert_image_cb(gpointer data,guint action,GtkWidget *widget)
{
  PidginWindow *win = data;
  PidginConversation *gtkconv;
  GtkIMHtmlToolbar *toolbar;
  gtkconv = pidgin_conv_window_get_active_gtkconv(win);
  toolbar = ((GtkIMHtmlToolbar *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> toolbar)),gtk_imhtmltoolbar_get_type())));
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> image)),gtk_toggle_button_get_type()))),!(gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(toolbar -> image)),gtk_toggle_button_get_type())))) != 0));
}

static void menu_alias_cb(gpointer data,guint action,GtkWidget *widget)
{
  PidginWindow *win = data;
  PurpleConversation *conv;
  PurpleAccount *account;
  const char *name;
  conv = pidgin_conv_window_get_active_conversation(win);
  account = purple_conversation_get_account(conv);
  name = purple_conversation_get_name(conv);
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
    PurpleBuddy *b;
    b = purple_find_buddy(account,name);
    if (b != ((PurpleBuddy *)((void *)0))) 
      pidgin_dialogs_alias_buddy(b);
  }
  else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) {
    PurpleChat *c;
    c = purple_blist_find_chat(account,name);
    if (c != ((PurpleChat *)((void *)0))) 
      pidgin_dialogs_alias_chat(c);
  }
}

static void menu_get_info_cb(gpointer data,guint action,GtkWidget *widget)
{
  PidginWindow *win = data;
  PurpleConversation *conv;
  conv = pidgin_conv_window_get_active_conversation(win);
  info_cb(0,((PidginConversation *)(conv -> ui_data)));
}

static void menu_invite_cb(gpointer data,guint action,GtkWidget *widget)
{
  PidginWindow *win = data;
  PurpleConversation *conv;
  conv = pidgin_conv_window_get_active_conversation(win);
  invite_cb(0,((PidginConversation *)(conv -> ui_data)));
}

static void menu_block_cb(gpointer data,guint action,GtkWidget *widget)
{
  PidginWindow *win = data;
  PurpleConversation *conv;
  conv = pidgin_conv_window_get_active_conversation(win);
  block_cb(0,((PidginConversation *)(conv -> ui_data)));
}

static void menu_unblock_cb(gpointer data,guint action,GtkWidget *widget)
{
  PidginWindow *win = data;
  PurpleConversation *conv;
  conv = pidgin_conv_window_get_active_conversation(win);
  unblock_cb(0,((PidginConversation *)(conv -> ui_data)));
}

static void menu_add_remove_cb(gpointer data,guint action,GtkWidget *widget)
{
  PidginWindow *win = data;
  PurpleConversation *conv;
  conv = pidgin_conv_window_get_active_conversation(win);
  add_remove_cb(0,((PidginConversation *)(conv -> ui_data)));
}

static gboolean close_already(gpointer data)
{
  purple_conversation_destroy(data);
  return 0;
}

static void hide_conv(PidginConversation *gtkconv,gboolean closetimer)
{
  GList *list;
  purple_signal_emit(pidgin_conversations_get_handle(),"conversation-hiding",gtkconv);
  for (list = g_list_copy((gtkconv -> convs)); list != 0; list = g_list_delete_link(list,list)) {
    PurpleConversation *conv = (list -> data);
    if (closetimer != 0) {
      guint timer = ((gint )((glong )(purple_conversation_get_data(conv,"close-timer"))));
      if (timer != 0U) 
        purple_timeout_remove(timer);
      timer = purple_timeout_add_seconds((10 * 60),close_already,conv);
      purple_conversation_set_data(conv,"close-timer",((gpointer )((glong )timer)));
    }
#if 0
/* I will miss you */
#else
    pidgin_conv_window_remove_gtkconv((gtkconv -> win),gtkconv);
    pidgin_conv_window_add_gtkconv(hidden_convwin,gtkconv);
#endif
  }
}

static void menu_close_conv_cb(gpointer data,guint action,GtkWidget *widget)
{
  PidginWindow *win = data;
  close_conv_cb(0,((PidginConversation *)( *pidgin_conv_window_get_active_conversation(win)).ui_data));
}

static void menu_logging_cb(gpointer data,guint action,GtkWidget *widget)
{
  PidginWindow *win = data;
  PurpleConversation *conv;
  gboolean logging;
  PurpleBlistNode *node;
  conv = pidgin_conv_window_get_active_conversation(win);
  if (conv == ((PurpleConversation *)((void *)0))) 
    return ;
  logging = gtk_check_menu_item_get_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_check_menu_item_get_type()))));
  if (logging == purple_conversation_is_logging(conv)) 
    return ;
  node = get_conversation_blist_node(conv);
  if (logging != 0) {
/* Enable logging first so the message below can be logged. */
    purple_conversation_set_logging(conv,(!0));
    purple_conversation_write(conv,0,((const char *)(dgettext("pidgin","Logging started. Future messages in this conversation will be logged."))),(((conv -> logs) != 0)?PURPLE_MESSAGE_SYSTEM : PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LOG),time(0));
  }
  else {
    purple_conversation_write(conv,0,((const char *)(dgettext("pidgin","Logging stopped. Future messages in this conversation will not be logged."))),(((conv -> logs) != 0)?PURPLE_MESSAGE_SYSTEM : PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LOG),time(0));
/* Disable the logging second, so that the above message can be logged. */
    purple_conversation_set_logging(conv,0);
  }
/* Save the setting IFF it's different than the pref. */
  switch(conv -> type){
    case PURPLE_CONV_TYPE_IM:
{
      if (logging == purple_prefs_get_bool("/purple/logging/log_ims")) 
        purple_blist_node_remove_setting(node,"enable-logging");
      else 
        purple_blist_node_set_bool(node,"enable-logging",logging);
      break; 
    }
    case PURPLE_CONV_TYPE_CHAT:
{
      if (logging == purple_prefs_get_bool("/purple/logging/log_chats")) 
        purple_blist_node_remove_setting(node,"enable-logging");
      else 
        purple_blist_node_set_bool(node,"enable-logging",logging);
      break; 
    }
    default:
{
      break; 
    }
  }
}

static void menu_toolbar_cb(gpointer data,guint action,GtkWidget *widget)
{
  purple_prefs_set_bool("/pidgin/conversations/show_formatting_toolbar",gtk_check_menu_item_get_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_check_menu_item_get_type())))));
}

static void menu_sounds_cb(gpointer data,guint action,GtkWidget *widget)
{
  PidginWindow *win = data;
  PurpleConversation *conv;
  PidginConversation *gtkconv;
  PurpleBlistNode *node;
  conv = pidgin_conv_window_get_active_conversation(win);
  if (!(conv != 0)) 
    return ;
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  gtkconv -> make_sound = gtk_check_menu_item_get_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_check_menu_item_get_type()))));
  node = get_conversation_blist_node(conv);
  if (node != 0) 
    purple_blist_node_set_bool(node,"gtk-mute-sound",!((gtkconv -> make_sound) != 0));
}

static void menu_timestamps_cb(gpointer data,guint action,GtkWidget *widget)
{
  purple_prefs_set_bool("/pidgin/conversations/show_timestamps",gtk_check_menu_item_get_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_check_menu_item_get_type())))));
}

static void chat_do_im(PidginConversation *gtkconv,const char *who)
{
  PurpleConversation *conv = (gtkconv -> active_conv);
  PurpleAccount *account;
  PurpleConnection *gc;
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  gchar *real_who = (gchar *)((void *)0);
  account = purple_conversation_get_account(conv);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  gc = purple_account_get_connection(account);
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info);
  if ((prpl_info != 0) && ((prpl_info -> get_cb_real_name) != 0)) 
    real_who = ( *(prpl_info -> get_cb_real_name))(gc,purple_conv_chat_get_id((purple_conversation_get_chat_data(conv))),who);
  if (!(who != 0) && !(real_who != 0)) 
    return ;
  pidgin_dialogs_im_with_user(account,((real_who != 0)?real_who : who));
  g_free(real_who);
}
static void pidgin_conv_chat_update_user(PurpleConversation *conv,const char *user);

static void ignore_cb(GtkWidget *w,PidginConversation *gtkconv)
{
  PurpleConversation *conv = (gtkconv -> active_conv);
  PurpleConvChat *chat;
  const char *name;
  chat = purple_conversation_get_chat_data(conv);
  name = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)w),((GType )(20 << 2))))),"user_data"));
  if (name == ((const char *)((void *)0))) 
    return ;
  if (purple_conv_chat_is_user_ignored(chat,name) != 0) 
    purple_conv_chat_unignore(chat,name);
  else 
    purple_conv_chat_ignore(chat,name);
  pidgin_conv_chat_update_user(conv,name);
}

static void menu_chat_im_cb(GtkWidget *w,PidginConversation *gtkconv)
{
  const char *who = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)w),((GType )(20 << 2))))),"user_data"));
  chat_do_im(gtkconv,who);
}

static void menu_chat_send_file_cb(GtkWidget *w,PidginConversation *gtkconv)
{
  PurplePluginProtocolInfo *prpl_info;
  PurpleConversation *conv = (gtkconv -> active_conv);
  const char *who = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)w),((GType )(20 << 2))))),"user_data"));
  PurpleConnection *gc = purple_conversation_get_gc(conv);
  gchar *real_who = (gchar *)((void *)0);
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info);
  if ((prpl_info != 0) && ((prpl_info -> get_cb_real_name) != 0)) 
    real_who = ( *(prpl_info -> get_cb_real_name))(gc,purple_conv_chat_get_id((purple_conversation_get_chat_data(conv))),who);
  serv_send_file(gc,((real_who != 0)?real_who : who),0);
  g_free(real_who);
}

static void menu_chat_info_cb(GtkWidget *w,PidginConversation *gtkconv)
{
  char *who;
  who = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)w),((GType )(20 << 2))))),"user_data"));
  chat_do_info(gtkconv,who);
}

static void menu_chat_get_away_cb(GtkWidget *w,PidginConversation *gtkconv)
{
  PurpleConversation *conv = (gtkconv -> active_conv);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurpleConnection *gc;
  char *who;
  gc = purple_conversation_get_gc(conv);
  who = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)w),((GType )(20 << 2))))),"user_data"));
  if (gc != ((PurpleConnection *)((void *)0))) {
    prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info);
/*
		 * May want to expand this to work similarly to menu_info_cb?
		 */
    if ((prpl_info -> get_cb_away) != ((void (*)(PurpleConnection *, int , const char *))((void *)0))) {
      ( *(prpl_info -> get_cb_away))(gc,purple_conv_chat_get_id((purple_conversation_get_chat_data(conv))),who);
    }
  }
}

static void menu_chat_add_remove_cb(GtkWidget *w,PidginConversation *gtkconv)
{
  PurpleConversation *conv = (gtkconv -> active_conv);
  PurpleAccount *account;
  PurpleBuddy *b;
  char *name;
  account = purple_conversation_get_account(conv);
  name = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)w),((GType )(20 << 2))))),"user_data"));
  b = purple_find_buddy(account,name);
  if (b != ((PurpleBuddy *)((void *)0))) 
    pidgin_dialogs_remove_buddy(b);
  else if ((account != ((PurpleAccount *)((void *)0))) && (purple_account_is_connected(account) != 0)) 
    purple_blist_request_add_buddy(account,name,0,0);
  gtk_widget_grab_focus(( *((PidginConversation *)(conv -> ui_data))).entry);
}

static GtkTextMark *get_mark_for_user(PidginConversation *gtkconv,const char *who)
{
  GtkTextBuffer *buf = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_text_view_get_type()))));
  char *tmp = g_strconcat("user:",who,((void *)((void *)0)));
  GtkTextMark *mark = gtk_text_buffer_get_mark(buf,tmp);
  g_free(tmp);
  return mark;
}

static void menu_last_said_cb(GtkWidget *w,PidginConversation *gtkconv)
{
  GtkTextMark *mark;
  const char *who;
  who = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)w),((GType )(20 << 2))))),"user_data"));
  mark = get_mark_for_user(gtkconv,who);
  if (mark != ((GtkTextMark *)((void *)0))) 
    gtk_text_view_scroll_to_mark(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_text_view_get_type()))),mark,0.1,0,0,0);
  else 
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","gtkconv.c",1589,((const char *)__func__));
      return ;
    }while (0);
}

static GtkWidget *create_chat_menu(PurpleConversation *conv,const char *who,PurpleConnection *gc)
{
  static GtkWidget *menu = (GtkWidget *)((void *)0);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurpleConvChat *chat = purple_conversation_get_chat_data(conv);
  gboolean is_me = 0;
  GtkWidget *button;
  PurpleBuddy *buddy = (PurpleBuddy *)((void *)0);
  if (gc != ((PurpleConnection *)((void *)0))) 
    prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info);
/*
	 * If a menu already exists, destroy it before creating a new one,
	 * thus freeing-up the memory it occupied.
	 */
  if (menu != 0) 
    gtk_widget_destroy(menu);
  if (!(strcmp((chat -> nick),purple_normalize((conv -> account),who)) != 0)) 
    is_me = (!0);
  menu = gtk_menu_new();
  if (!(is_me != 0)) {
    button = pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","IM"))),"pidgin-message-new",((GCallback )menu_chat_im_cb),((PidginConversation *)(conv -> ui_data)),0,0,0);
    if (gc == ((PurpleConnection *)((void *)0))) 
      gtk_widget_set_sensitive(button,0);
    else 
      g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"user_data",(g_strdup(who)),g_free);
    if ((prpl_info != 0) && ((prpl_info -> send_file) != 0)) {
      gboolean can_receive_file = (!0);
      button = pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Send File"))),"pidgin-send-file",((GCallback )menu_chat_send_file_cb),((PidginConversation *)(conv -> ui_data)),0,0,0);
      if ((gc == ((PurpleConnection *)((void *)0))) || (prpl_info == ((PurplePluginProtocolInfo *)((void *)0)))) 
        can_receive_file = 0;
      else {
        gchar *real_who = (gchar *)((void *)0);
        if ((prpl_info -> get_cb_real_name) != 0) 
          real_who = ( *(prpl_info -> get_cb_real_name))(gc,purple_conv_chat_get_id((purple_conversation_get_chat_data(conv))),who);
        if (!(!((prpl_info -> can_receive_file) != 0) || (( *(prpl_info -> can_receive_file))(gc,((real_who != 0)?real_who : who)) != 0))) 
          can_receive_file = 0;
        g_free(real_who);
      }
      if (!(can_receive_file != 0)) 
        gtk_widget_set_sensitive(button,0);
      else 
        g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"user_data",(g_strdup(who)),g_free);
    }
    if (purple_conv_chat_is_user_ignored((purple_conversation_get_chat_data(conv)),who) != 0) 
      button = pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Un-Ignore"))),"pidgin-ignore",((GCallback )ignore_cb),((PidginConversation *)(conv -> ui_data)),0,0,0);
    else 
      button = pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Ignore"))),"pidgin-ignore",((GCallback )ignore_cb),((PidginConversation *)(conv -> ui_data)),0,0,0);
    if (gc == ((PurpleConnection *)((void *)0))) 
      gtk_widget_set_sensitive(button,0);
    else 
      g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"user_data",(g_strdup(who)),g_free);
  }
  if ((prpl_info != 0) && (((prpl_info -> get_info) != 0) || ((prpl_info -> get_cb_info) != 0))) {
    button = pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Info"))),"pidgin-info",((GCallback )menu_chat_info_cb),((PidginConversation *)(conv -> ui_data)),0,0,0);
    if (gc == ((PurpleConnection *)((void *)0))) 
      gtk_widget_set_sensitive(button,0);
    else 
      g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"user_data",(g_strdup(who)),g_free);
  }
  if ((prpl_info != 0) && ((prpl_info -> get_cb_away) != 0)) {
    button = pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Get Away Message"))),"pidgin-away",((GCallback )menu_chat_get_away_cb),((PidginConversation *)(conv -> ui_data)),0,0,0);
    if (gc == ((PurpleConnection *)((void *)0))) 
      gtk_widget_set_sensitive(button,0);
    else 
      g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"user_data",(g_strdup(who)),g_free);
  }
  if ((!(is_me != 0) && (prpl_info != 0)) && !(((prpl_info -> options) & OPT_PROTO_UNIQUE_CHATNAME) != 0U)) {
    if ((buddy = purple_find_buddy((conv -> account),who)) != ((PurpleBuddy *)((void *)0))) 
      button = pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Remove"))),"gtk-remove",((GCallback )menu_chat_add_remove_cb),((PidginConversation *)(conv -> ui_data)),0,0,0);
    else 
      button = pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Add"))),"gtk-add",((GCallback )menu_chat_add_remove_cb),((PidginConversation *)(conv -> ui_data)),0,0,0);
    if (gc == ((PurpleConnection *)((void *)0))) 
      gtk_widget_set_sensitive(button,0);
    else 
      g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"user_data",(g_strdup(who)),g_free);
  }
  button = pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Last Said"))),"gtk-index",((GCallback )menu_last_said_cb),((PidginConversation *)(conv -> ui_data)),0,0,0);
  g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"user_data",(g_strdup(who)),g_free);
  if (!(get_mark_for_user(((PidginConversation *)(conv -> ui_data)),who) != 0)) 
    gtk_widget_set_sensitive(button,0);
  if (buddy != ((PurpleBuddy *)((void *)0))) {
    if (purple_account_is_connected((conv -> account)) != 0) 
      pidgin_append_blist_node_proto_menu(menu,( *(conv -> account)).gc,((PurpleBlistNode *)buddy));
    pidgin_append_blist_node_extended_menu(menu,((PurpleBlistNode *)buddy));
    gtk_widget_show_all(menu);
  }
  return menu;
}

static gint gtkconv_chat_popup_menu_cb(GtkWidget *widget,PidginConversation *gtkconv)
{
  PurpleConversation *conv = (gtkconv -> active_conv);
  PidginChatPane *gtkchat;
  PurpleConnection *gc;
  PurpleAccount *account;
  GtkTreeSelection *sel;
  GtkTreeIter iter;
  GtkTreeModel *model;
  GtkWidget *menu;
  gchar *who;
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  gtkchat = gtkconv -> u.chat;
  account = purple_conversation_get_account(conv);
  gc = (account -> gc);
  model = gtk_tree_view_get_model(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> list)),gtk_tree_view_get_type()))));
  sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> list)),gtk_tree_view_get_type()))));
  if (!(gtk_tree_selection_get_selected(sel,0,&iter) != 0)) 
    return 0;
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,CHAT_USERS_NAME_COLUMN,&who,-1);
  menu = create_chat_menu(conv,who,gc);
  gtk_menu_popup(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_get_type()))),0,0,pidgin_treeview_popup_menu_position_func,widget,0,0L);
  g_free(who);
  return (!0);
}

static gint right_click_chat_cb(GtkWidget *widget,GdkEventButton *event,PidginConversation *gtkconv)
{
  PurpleConversation *conv = (gtkconv -> active_conv);
  PidginChatPane *gtkchat;
  PurpleConnection *gc;
  PurpleAccount *account;
  GtkTreePath *path;
  GtkTreeIter iter;
  GtkTreeModel *model;
  GtkTreeViewColumn *column;
  gchar *who;
  int x;
  int y;
  gtkchat = gtkconv -> u.chat;
  account = purple_conversation_get_account(conv);
  gc = (account -> gc);
  model = gtk_tree_view_get_model(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> list)),gtk_tree_view_get_type()))));
  gtk_tree_view_get_path_at_pos(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> list)),gtk_tree_view_get_type()))),(event -> x),(event -> y),&path,&column,&x,&y);
  if (path == ((GtkTreePath *)((void *)0))) 
    return 0;
  gtk_tree_selection_select_path(((GtkTreeSelection *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> list)),gtk_tree_view_get_type())))))),gtk_tree_selection_get_type()))),path);
  gtk_tree_view_set_cursor(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> list)),gtk_tree_view_get_type()))),path,0,0);
  gtk_widget_grab_focus(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> list)),gtk_widget_get_type()))));
  gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,path);
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,CHAT_USERS_NAME_COLUMN,&who,-1);
/* emit chat-nick-clicked signal */
  if ((event -> type) == GDK_BUTTON_PRESS) {
    gint plugin_return = (gint )((glong )(purple_signal_emit_return_1(pidgin_conversations_get_handle(),"chat-nick-clicked",conv,who,(event -> button))));
    if (plugin_return != 0) 
      goto handled;
  }
  if (((event -> button) == 1) && ((event -> type) == GDK_2BUTTON_PRESS)) {
    chat_do_im(gtkconv,who);
  }
  else if (((event -> button) == 2) && ((event -> type) == GDK_BUTTON_PRESS)) {
/* Move to user's anchor */
    GtkTextMark *mark = get_mark_for_user(gtkconv,who);
    if (mark != ((GtkTextMark *)((void *)0))) 
      gtk_text_view_scroll_to_mark(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_text_view_get_type()))),mark,0.1,0,0,0);
  }
  else if (((event -> button) == 3) && ((event -> type) == GDK_BUTTON_PRESS)) {
    GtkWidget *menu = create_chat_menu(conv,who,gc);
    gtk_menu_popup(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_get_type()))),0,0,0,0,(event -> button),(event -> time));
  }
  handled:
  g_free(who);
  gtk_tree_path_free(path);
  return (!0);
}

static void activate_list_cb(GtkTreeView *list,GtkTreePath *path,GtkTreeViewColumn *column,PidginConversation *gtkconv)
{
  GtkTreeIter iter;
  GtkTreeModel *model;
  gchar *who;
  model = gtk_tree_view_get_model(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)list),gtk_tree_view_get_type()))));
  gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,path);
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,CHAT_USERS_NAME_COLUMN,&who,-1);
  chat_do_im(gtkconv,who);
  g_free(who);
}

static void move_to_next_unread_tab(PidginConversation *gtkconv,gboolean forward)
{
  PidginConversation *next_gtkconv = (PidginConversation *)((void *)0);
  PidginConversation *most_active = (PidginConversation *)((void *)0);
  PidginUnseenState unseen_state = PIDGIN_UNSEEN_NONE;
  PidginWindow *win;
  int initial;
  int i;
  int total;
  int diff;
  win = (gtkconv -> win);
  initial = gtk_notebook_page_num(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),(gtkconv -> tab_cont));
  total = (pidgin_conv_window_get_gtkconv_count(win));
/* By adding total here, the moduli calculated later will always have two
	 * positive arguments. x % y where x < 0 is not guaranteed to return a
	 * positive number.
	 */
  diff = ((((forward != 0)?1 : -1)) + total);
{
    for (i = ((initial + diff) % total); i != initial; i = ((i + diff) % total)) {
      next_gtkconv = pidgin_conv_window_get_gtkconv_at_index(win,i);
      if ((next_gtkconv -> unseen_state) > unseen_state) {
        most_active = next_gtkconv;
        unseen_state = (most_active -> unseen_state);
/* highest possible state */
        if (PIDGIN_UNSEEN_NICK == unseen_state) 
          break; 
      }
    }
  }
/* no new messages */
  if (most_active == ((PidginConversation *)((void *)0))) {
    i = ((i + diff) % total);
    most_active = pidgin_conv_window_get_gtkconv_at_index(win,i);
  }
  if ((most_active != ((PidginConversation *)((void *)0))) && (most_active != gtkconv)) 
    pidgin_conv_window_switch_gtkconv(win,most_active);
}

static gboolean gtkconv_cycle_focus(PidginConversation *gtkconv,GtkDirectionType dir)
{
  PurpleConversation *conv = (gtkconv -> active_conv);
  gboolean chat = ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT);
  GtkWidget *next = (GtkWidget *)((void *)0);
  struct __unnamed_class___F0_L1880_C2_L1670R__L1671R__scope____SgSS2___variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_L1670R__L1671R__scope____SgSS2____scope__from__DELIMITER__L1670R__L1671R__scope____SgSS2___variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_L1670R__L1671R__scope____SgSS2____scope__to {
  GtkWidget *from;
  GtkWidget *to;}transitions[] = {{((gtkconv -> entry)), ((gtkconv -> imhtml))}, {((gtkconv -> imhtml)), ((chat != 0)?( *gtkconv -> u.chat).list : (gtkconv -> entry))}, {((chat != 0)?( *gtkconv -> u.chat).list : ((struct _GtkWidget *)((void *)0))), ((gtkconv -> entry))}, {((GtkWidget *)((void *)0)), ((GtkWidget *)((void *)0))}};
  struct __unnamed_class___F0_L1880_C2_L1670R__L1671R__scope____SgSS2___variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_L1670R__L1671R__scope____SgSS2____scope__from__DELIMITER__L1670R__L1671R__scope____SgSS2___variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_L1670R__L1671R__scope____SgSS2____scope__to *ptr;
  for (ptr = transitions; !(next != 0) && ((ptr -> from) != 0); ptr++) {
    GtkWidget *from;
    GtkWidget *to;
    if (dir == GTK_DIR_TAB_FORWARD) {
      from = (ptr -> from);
      to = (ptr -> to);
    }
    else {
      from = (ptr -> to);
      to = (ptr -> from);
    }
    if (gtk_widget_is_focus(from) != 0) 
      next = to;
  }
  if (next != 0) 
    gtk_widget_grab_focus(next);
  return !(!(next != 0));
}

static gboolean conv_keypress_common(PidginConversation *gtkconv,GdkEventKey *event)
{
  PidginWindow *win;
  int curconv;
  win = (gtkconv -> win);
  curconv = gtk_notebook_get_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))));
/* clear any tooltips */
  pidgin_tooltip_destroy();
/* If CTRL was held down... */
  if (((event -> state) & GDK_CONTROL_MASK) != 0U) {
    switch(event -> keyval){
      case ']':
{
      }
      case 0xff56:
{
      }
      case 0xff9b:
{
        if (!(pidgin_conv_window_get_gtkconv_at_index(win,(curconv + 1)) != 0)) 
          gtk_notebook_set_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),0);
        else 
          gtk_notebook_set_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),(curconv + 1));
        return (!0);
        break; 
      }
      case '[':
{
      }
      case 0xff55:
{
      }
      case 0xff9a:
{
        if (!(pidgin_conv_window_get_gtkconv_at_index(win,(curconv - 1)) != 0)) 
          gtk_notebook_set_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),(-1));
        else 
          gtk_notebook_set_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),(curconv - 1));
        return (!0);
        break; 
      }
      case 0xfe20:
{
      }
      case 0xff09:
{
      }
      case 0xff89:
{
        if (((event -> state) & GDK_SHIFT_MASK) != 0U) {
          move_to_next_unread_tab(gtkconv,0);
        }
        else {
          move_to_next_unread_tab(gtkconv,(!0));
        }
        return (!0);
        break; 
      }
      case 0x02c:
{
        gtk_notebook_reorder_child(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),gtk_notebook_get_nth_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),curconv),(curconv - 1));
        return (!0);
        break; 
      }
      case 0x02e:
{
        gtk_notebook_reorder_child(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),gtk_notebook_get_nth_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),curconv),((curconv + 1) % gtk_notebook_get_n_pages(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))))));
        return (!0);
        break; 
      }
      case 0xffc3:
{
        if (gtkconv_cycle_focus(gtkconv,(((((event -> state) & GDK_SHIFT_MASK) != 0U)?GTK_DIR_TAB_BACKWARD : GTK_DIR_TAB_FORWARD))) != 0) 
          return (!0);
        break; 
      }
/* End of switch */
    }
  }
  else 
/* If ALT (or whatever) was held down... */
if (((event -> state) & GDK_MOD1_MASK) != 0U) {
    if (((event -> keyval) > 48) && ((event -> keyval) <= '9')) {
      guint switchto = ((event -> keyval) - '1');
      if (switchto < pidgin_conv_window_get_gtkconv_count(win)) 
        gtk_notebook_set_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),switchto);
      return (!0);
    }
  }
  else 
/* If neither CTRL nor ALT were held down... */
{
    switch(event -> keyval){
      case 0xffbf:
{
        if (gtk_widget_is_focus(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_widget_get_type())))) != 0) {
          infopane_entry_activate(gtkconv);
          return (!0);
        }
        break; 
      }
      case 0xffc3:
{
        if (gtkconv_cycle_focus(gtkconv,(((((event -> state) & GDK_SHIFT_MASK) != 0U)?GTK_DIR_TAB_BACKWARD : GTK_DIR_TAB_FORWARD))) != 0) 
          return (!0);
        break; 
      }
    }
  }
  return 0;
}

static gboolean entry_key_press_cb(GtkWidget *entry,GdkEventKey *event,gpointer data)
{
  PurpleConversation *conv;
  PidginConversation *gtkconv;
  gtkconv = ((PidginConversation *)data);
  conv = (gtkconv -> active_conv);
  if (conv_keypress_common(gtkconv,event) != 0) 
    return (!0);
/* If CTRL was held down... */
  if (((event -> state) & GDK_CONTROL_MASK) != 0U) {{
      switch(event -> keyval){
        case 0xff52:
{
          if (!((gtkconv -> send_history) != 0)) 
            break; 
          if ((gtkconv -> entry) != entry) 
            break; 
          if (!(( *(gtkconv -> send_history)).prev != 0)) {
            GtkTextIter start;
            GtkTextIter end;
            g_free(( *(gtkconv -> send_history)).data);
            gtk_text_buffer_get_start_iter((gtkconv -> entry_buffer),&start);
            gtk_text_buffer_get_end_iter((gtkconv -> entry_buffer),&end);
            ( *(gtkconv -> send_history)).data = (gtk_imhtml_get_markup(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type())))));
          }
          if ((( *(gtkconv -> send_history)).next != 0) && (( *( *(gtkconv -> send_history)).next).data != 0)) {
            GObject *object;
            GtkTextIter iter;
            GtkTextBuffer *buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_text_view_get_type()))));
            gtkconv -> send_history = ( *(gtkconv -> send_history)).next;
/* Block the signal to prevent application of default formatting. */
            object = (g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),((GType )(20 << 2)))))));
            g_signal_handlers_block_matched(object,G_SIGNAL_MATCH_DATA,0,0,0,0,gtkconv);
/* Clear the formatting. */
            gtk_imhtml_clear_formatting(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type()))));
/* Unblock the signal. */
            g_signal_handlers_unblock_matched(object,G_SIGNAL_MATCH_DATA,0,0,0,0,gtkconv);
            g_object_unref(object);
            gtk_imhtml_delete(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type()))),0,0);
            gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type()))),( *(gtkconv -> send_history)).data,0,0);
/* this is mainly just a hack so the formatting at the
					 * cursor gets picked up. */
            gtk_text_buffer_get_end_iter(buffer,&iter);
            gtk_text_buffer_move_mark_by_name(buffer,"insert",(&iter));
          }
          return (!0);
          break; 
        }
        case 0xff54:
{
          if (!((gtkconv -> send_history) != 0)) 
            break; 
          if ((gtkconv -> entry) != entry) 
            break; 
          if ((( *(gtkconv -> send_history)).prev != 0) && (( *( *(gtkconv -> send_history)).prev).data != 0)) {
            GObject *object;
            GtkTextIter iter;
            GtkTextBuffer *buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_text_view_get_type()))));
            gtkconv -> send_history = ( *(gtkconv -> send_history)).prev;
/* Block the signal to prevent application of default formatting. */
            object = (g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),((GType )(20 << 2)))))));
            g_signal_handlers_block_matched(object,G_SIGNAL_MATCH_DATA,0,0,0,0,gtkconv);
/* Clear the formatting. */
            gtk_imhtml_clear_formatting(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type()))));
/* Unblock the signal. */
            g_signal_handlers_unblock_matched(object,G_SIGNAL_MATCH_DATA,0,0,0,0,gtkconv);
            g_object_unref(object);
            gtk_imhtml_delete(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type()))),0,0);
            gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type()))),( *(gtkconv -> send_history)).data,0,0);
/* this is mainly just a hack so the formatting at the
					 * cursor gets picked up. */
            if (( *((char *)( *(gtkconv -> send_history)).data)) != 0) {
              gtk_text_buffer_get_end_iter(buffer,&iter);
              gtk_text_buffer_move_mark_by_name(buffer,"insert",(&iter));
            }
            else {
/* Restore the default formatting */
              default_formatize(gtkconv);
            }
          }
          return (!0);
          break; 
        }
/* End of switch */
      }
    }
  }
  else 
/* If ALT (or whatever) was held down... */
if (((event -> state) & GDK_MOD1_MASK) != 0U) {
  }
  else 
/* If neither CTRL nor ALT were held down... */
{{
      switch(event -> keyval){
        case 0xfe20:
{
        }
        case 0xff09:
{
        }
        case 0xff89:
{
          if ((gtkconv -> entry) != entry) 
            break; 
{
            gint plugin_return;
            plugin_return = ((gint )((glong )(purple_signal_emit_return_1(pidgin_conversations_get_handle(),"chat-nick-autocomplete",conv,((event -> state) & GDK_SHIFT_MASK)))));
            return (plugin_return != 0)?!0 : tab_complete(conv);
          }
          break; 
        }
        case 0xff55:
{
        }
        case 0xff9a:
{
          gtk_imhtml_page_up(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))));
          return (!0);
          break; 
        }
        case 0xff56:
{
        }
        case 0xff9b:
{
          gtk_imhtml_page_down(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))));
          return (!0);
          break; 
        }
      }
    }
  }
  return 0;
}
/*
 * NOTE:
 *   This guy just kills a single right click from being propagated any
 *   further.  I  have no idea *why* we need this, but we do ...  It
 *   prevents right clicks on the GtkTextView in a convo dialog from
 *   going all the way down to the notebook.  I suspect a bug in
 *   GtkTextView, but I'm not ready to point any fingers yet.
 */

static gboolean entry_stop_rclick_cb(GtkWidget *widget,GdkEventButton *event,gpointer data)
{
  if (((event -> button) == 3) && ((event -> type) == GDK_BUTTON_PRESS)) {
/* Right single click */
    g_signal_stop_emission_by_name(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"button_press_event");
    return (!0);
  }
  return 0;
}
/*
 * If someone tries to type into the conversation backlog of a
 * conversation window then we yank focus from the conversation backlog
 * and give it to the text entry box so that people can type
 * all the live long day and it will get entered into the entry box.
 */

static gboolean refocus_entry_cb(GtkWidget *widget,GdkEventKey *event,gpointer data)
{
  PidginConversation *gtkconv = data;
/* If we have a valid key for the conversation display, then exit */
  if (((((((((((((((((((((((event -> state) & GDK_CONTROL_MASK) != 0U) || ((event -> keyval) == 0xffc3)) || ((event -> keyval) == 0xffc7)) || ((event -> keyval) == 0xffe1)) || ((event -> keyval) == 0xffe2)) || ((event -> keyval) == 0xffe3)) || ((event -> keyval) == 0xffe4)) || ((event -> keyval) == 0xff1b)) || ((event -> keyval) == 0xff52)) || ((event -> keyval) == 0xff54)) || ((event -> keyval) == 0xff51)) || ((event -> keyval) == 0xff53)) || ((event -> keyval) == 0xff55)) || ((event -> keyval) == 0xff9a)) || ((event -> keyval) == 0xff56)) || ((event -> keyval) == 0xff9b)) || ((event -> keyval) == 0xff50)) || ((event -> keyval) == 0xff57)) || ((event -> keyval) == 0xff09)) || ((event -> keyval) == 0xff89)) || ((event -> keyval) == 0xfe20)) {
    if ((event -> type) == GDK_KEY_PRESS) 
      return conv_keypress_common(gtkconv,event);
    return 0;
  }
  if ((event -> type) == GDK_KEY_RELEASE) 
    gtk_widget_grab_focus((gtkconv -> entry));
  gtk_widget_event((gtkconv -> entry),((GdkEvent *)event));
  return (!0);
}
static void regenerate_options_items(PidginWindow *win);

void pidgin_conv_switch_active_conversation(PurpleConversation *conv)
{
  PidginConversation *gtkconv;
  PurpleConversation *old_conv;
  GtkIMHtml *entry;
  const char *protocol_name;
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  old_conv = (gtkconv -> active_conv);
  purple_debug_info("gtkconv","setting active conversation on toolbar %p\n",conv);
  gtk_imhtmltoolbar_switch_active_conversation(((GtkIMHtmlToolbar *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> toolbar)),gtk_imhtmltoolbar_get_type()))),conv);
  if (old_conv == conv) 
    return ;
  purple_conversation_close_logs(old_conv);
  gtkconv -> active_conv = conv;
  purple_conversation_set_logging(conv,gtk_check_menu_item_get_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)( *(gtkconv -> win)).menu.logging),gtk_check_menu_item_get_type())))));
  entry = ((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type())));
  protocol_name = purple_account_get_protocol_name((conv -> account));
  gtk_imhtml_set_protocol_name(entry,protocol_name);
  gtk_imhtml_set_protocol_name(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),protocol_name);
  if (!(((conv -> features) & PURPLE_CONNECTION_HTML) != 0U)) 
    gtk_imhtml_clear_formatting(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type()))));
  else if ((((conv -> features) & PURPLE_CONNECTION_FORMATTING_WBFO) != 0U) && !(((old_conv -> features) & PURPLE_CONNECTION_FORMATTING_WBFO) != 0U)) {
/* The old conversation allowed formatting on parts of the
		 * buffer, but the new one only allows it on the whole
		 * buffer.  This code saves the formatting from the current
		 * position of the cursor, clears the formatting, then
		 * applies the saved formatting to the entire buffer. */
    gboolean bold;
    gboolean italic;
    gboolean underline;
    char *fontface = gtk_imhtml_get_current_fontface(entry);
    char *forecolor = gtk_imhtml_get_current_forecolor(entry);
    char *backcolor = gtk_imhtml_get_current_backcolor(entry);
    char *background = gtk_imhtml_get_current_background(entry);
    gint fontsize = gtk_imhtml_get_current_fontsize(entry);
    gboolean bold2;
    gboolean italic2;
    gboolean underline2;
    gtk_imhtml_get_current_format(entry,&bold,&italic,&underline);
/* Clear existing formatting */
    gtk_imhtml_clear_formatting(entry);
/* Apply saved formatting to the whole buffer. */
    gtk_imhtml_get_current_format(entry,&bold2,&italic2,&underline2);
    if (bold != bold2) 
      gtk_imhtml_toggle_bold(entry);
    if (italic != italic2) 
      gtk_imhtml_toggle_italic(entry);
    if (underline != underline2) 
      gtk_imhtml_toggle_underline(entry);
    gtk_imhtml_toggle_fontface(entry,fontface);
    if (!(((conv -> features) & PURPLE_CONNECTION_NO_FONTSIZE) != 0U)) 
      gtk_imhtml_font_set_size(entry,fontsize);
    gtk_imhtml_toggle_forecolor(entry,forecolor);
    if (!(((conv -> features) & PURPLE_CONNECTION_NO_BGCOLOR) != 0U)) {
      gtk_imhtml_toggle_backcolor(entry,backcolor);
      gtk_imhtml_toggle_background(entry,background);
    }
    g_free(fontface);
    g_free(forecolor);
    g_free(backcolor);
    g_free(background);
  }
  else {
/* This is done in default_formatize, which is called from clear_formatting_cb,
		 * which is (obviously) a clear_formatting signal handler.  However, if we're
		 * here, we didn't call gtk_imhtml_clear_formatting() (because we want to
		 * preserve the formatting exactly as it is), so we have to do this now. */
    gtk_imhtml_set_whole_buffer_formatting_only(entry,((conv -> features) & PURPLE_CONNECTION_FORMATTING_WBFO));
  }
  purple_signal_emit(pidgin_conversations_get_handle(),"conversation-switched",conv);
  gray_stuff_out(gtkconv);
  update_typing_icon(gtkconv);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"transient_buddy",0);
  regenerate_options_items((gtkconv -> win));
  gtk_window_set_title(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)( *(gtkconv -> win)).window),gtk_window_get_type()))),gtk_label_get_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tab_label)),gtk_label_get_type())))));
}

static void menu_conv_sel_send_cb(GObject *m,gpointer data)
{
  PurpleAccount *account = (g_object_get_data(m,"purple_account"));
  gchar *name = (g_object_get_data(m,"purple_buddy_name"));
  PurpleConversation *conv;
  if (gtk_check_menu_item_get_active(((GtkCheckMenuItem *)m)) == 0) 
    return ;
  conv = purple_conversation_new(PURPLE_CONV_TYPE_IM,account,name);
  pidgin_conv_switch_active_conversation(conv);
}

static void insert_text_cb(GtkTextBuffer *textbuffer,GtkTextIter *position,gchar *new_text,gint new_text_length,gpointer user_data)
{
  PidginConversation *gtkconv = (PidginConversation *)user_data;
  do {
    if (gtkconv != ((PidginConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gtkconv != NULL");
      return ;
    };
  }while (0);
  if (!(purple_prefs_get_bool("/purple/conversations/im/send_typing") != 0)) 
    return ;
  got_typing_keypress(gtkconv,((gtk_text_iter_is_start(position) != 0) && (gtk_text_iter_is_end(position) != 0)));
}

static void delete_text_cb(GtkTextBuffer *textbuffer,GtkTextIter *start_pos,GtkTextIter *end_pos,gpointer user_data)
{
  PidginConversation *gtkconv = (PidginConversation *)user_data;
  PurpleConversation *conv;
  PurpleConvIm *im;
  do {
    if (gtkconv != ((PidginConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gtkconv != NULL");
      return ;
    };
  }while (0);
  conv = (gtkconv -> active_conv);
  if (!(purple_prefs_get_bool("/purple/conversations/im/send_typing") != 0)) 
    return ;
  im = purple_conversation_get_im_data(conv);
  if ((gtk_text_iter_is_start(start_pos) != 0) && (gtk_text_iter_is_end(end_pos) != 0)) {
/* We deleted all the text, so turn off typing. */
    purple_conv_im_stop_send_typed_timeout(im);
    serv_send_typing(purple_conversation_get_gc(conv),purple_conversation_get_name(conv),PURPLE_NOT_TYPING);
  }
  else {
/* We're deleting, but not all of it, so it counts as typing. */
    got_typing_keypress(gtkconv,0);
  }
}
/**************************************************************************
 * A bunch of buddy icon functions
 **************************************************************************/

static GList *get_prpl_icon_list(PurpleAccount *account)
{
  GList *l = (GList *)((void *)0);
  PurplePlugin *prpl = purple_find_prpl(purple_account_get_protocol_id(account));
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info;
  const char *prplname = ( *(prpl_info -> list_icon))(account,0);
  l = (g_hash_table_lookup(prpl_lists,prplname));
  if (l != 0) 
    return l;
  l = g_list_prepend(l,(pidgin_create_prpl_icon(account,PIDGIN_PRPL_ICON_LARGE)));
  l = g_list_prepend(l,(pidgin_create_prpl_icon(account,PIDGIN_PRPL_ICON_MEDIUM)));
  l = g_list_prepend(l,(pidgin_create_prpl_icon(account,PIDGIN_PRPL_ICON_SMALL)));
  g_hash_table_insert(prpl_lists,(g_strdup(prplname)),l);
  return l;
}

static GList *pidgin_conv_get_tab_icons(PurpleConversation *conv)
{
  PurpleAccount *account = (PurpleAccount *)((void *)0);
  const char *name = (const char *)((void *)0);
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return 0;
    };
  }while (0);
  account = purple_conversation_get_account(conv);
  name = purple_conversation_get_name(conv);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return 0;
    };
  }while (0);
/* Use the buddy icon, if possible */
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
    PurpleBuddy *b = purple_find_buddy(account,name);
    if (b != ((PurpleBuddy *)((void *)0))) {
      PurplePresence *p;
      p = purple_buddy_get_presence(b);
      if (purple_presence_is_status_primitive_active(p,PURPLE_STATUS_AWAY) != 0) 
        return away_list;
      if (purple_presence_is_status_primitive_active(p,PURPLE_STATUS_UNAVAILABLE) != 0) 
        return busy_list;
      if (purple_presence_is_status_primitive_active(p,PURPLE_STATUS_EXTENDED_AWAY) != 0) 
        return xa_list;
      if (purple_presence_is_status_primitive_active(p,PURPLE_STATUS_OFFLINE) != 0) 
        return offline_list;
      else 
        return available_list;
    }
  }
  return get_prpl_icon_list(account);
}

static const char *pidgin_conv_get_icon_stock(PurpleConversation *conv)
{
  PurpleAccount *account = (PurpleAccount *)((void *)0);
  const char *stock = (const char *)((void *)0);
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return 0;
    };
  }while (0);
  account = purple_conversation_get_account(conv);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
/* Use the buddy icon, if possible */
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
    const char *name = (const char *)((void *)0);
    PurpleBuddy *b;
    name = purple_conversation_get_name(conv);
    b = purple_find_buddy(account,name);
    if (b != ((PurpleBuddy *)((void *)0))) {
      PurplePresence *p = purple_buddy_get_presence(b);
      PurpleStatus *active = purple_presence_get_active_status(p);
      PurpleStatusType *type = purple_status_get_type(active);
      PurpleStatusPrimitive prim = purple_status_type_get_primitive(type);
      stock = pidgin_stock_id_from_status_primitive(prim);
    }
    else {
      stock = "pidgin-status-person";
    }
  }
  else {
    stock = "pidgin-status-chat";
  }
  return stock;
}

static GdkPixbuf *pidgin_conv_get_icon(PurpleConversation *conv,GtkWidget *parent,const char *icon_size)
{
  PurpleAccount *account = (PurpleAccount *)((void *)0);
  const char *name = (const char *)((void *)0);
  const char *stock = (const char *)((void *)0);
  GdkPixbuf *status = (GdkPixbuf *)((void *)0);
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  GtkIconSize size;
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return 0;
    };
  }while (0);
  account = purple_conversation_get_account(conv);
  name = purple_conversation_get_name(conv);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return 0;
    };
  }while (0);
/* Use the buddy icon, if possible */
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
    PurpleBuddy *b = purple_find_buddy(account,name);
    if (b != ((PurpleBuddy *)((void *)0))) {
/* I hate this hack.  It fixes a bug where the pending message icon
			 * displays in the conv tab even though it shouldn't.
			 * A better solution would be great. */
      if ((ops != 0) && ((ops -> update) != 0)) 
        ( *(ops -> update))(0,((PurpleBlistNode *)b));
    }
  }
  stock = pidgin_conv_get_icon_stock(conv);
  size = gtk_icon_size_from_name(icon_size);
  status = gtk_widget_render_icon(parent,stock,size,"GtkWidget");
  return status;
}

GdkPixbuf *pidgin_conv_get_tab_icon(PurpleConversation *conv,gboolean small_icon)
{
  const char *icon_size = ((small_icon != 0)?"pidgin-icon-size-tango-microscopic" : "pidgin-icon-size-tango-extra-small");
  return pidgin_conv_get_icon(conv,( *((PidginConversation *)(conv -> ui_data))).icon,icon_size);
}

static void update_tab_icon(PurpleConversation *conv)
{
  PidginConversation *gtkconv;
  PidginWindow *win;
  GList *l;
  GdkPixbuf *emblem = (GdkPixbuf *)((void *)0);
  const char *status = (const char *)((void *)0);
  const char *infopane_status = (const char *)((void *)0);
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  win = (gtkconv -> win);
  if (conv != (gtkconv -> active_conv)) 
    return ;
  status = (infopane_status = pidgin_conv_get_icon_stock(conv));
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
    PurpleBuddy *b = purple_find_buddy((conv -> account),(conv -> name));
    if (b != 0) 
      emblem = pidgin_blist_get_emblem(((PurpleBlistNode *)b));
  }
  do {
    if (status != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"status != NULL");
      return ;
    };
  }while (0);
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> icon)),((GType )(20 << 2))))),"stock",status,((void *)((void *)0)));
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> menu_icon)),((GType )(20 << 2))))),"stock",status,((void *)((void *)0)));
  gtk_list_store_set(((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> infopane_model)),gtk_list_store_get_type()))),&gtkconv -> infopane_iter,CONV_ICON_COLUMN,infopane_status,-1);
  gtk_list_store_set(((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> infopane_model)),gtk_list_store_get_type()))),&gtkconv -> infopane_iter,CONV_EMBLEM_COLUMN,emblem,-1);
  if (emblem != 0) 
    g_object_unref(emblem);
  if (purple_prefs_get_bool("/pidgin/blist/show_protocol_icons") != 0) {
    emblem = pidgin_create_prpl_icon(( *(gtkconv -> active_conv)).account,PIDGIN_PRPL_ICON_SMALL);
  }
  else {
    emblem = ((GdkPixbuf *)((void *)0));
  }
  gtk_list_store_set(((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> infopane_model)),gtk_list_store_get_type()))),&gtkconv -> infopane_iter,CONV_PROTOCOL_ICON_COLUMN,emblem,-1);
  if (emblem != 0) 
    g_object_unref(emblem);
/* XXX seanegan Why do I have to do this? */
  gtk_widget_queue_resize((gtkconv -> infopane));
  gtk_widget_queue_draw((gtkconv -> infopane));
  if ((pidgin_conv_window_is_active_conversation(conv) != 0) && (((purple_conversation_get_type(conv)) != PURPLE_CONV_TYPE_IM) || (( *gtkconv -> u.im).anim == ((GdkPixbufAnimation *)((void *)0))))) {
    l = pidgin_conv_get_tab_icons(conv);
    gtk_window_set_icon_list(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),gtk_window_get_type()))),l);
  }
}
#if 0
/* This gets added as an idle handler when doing something that
 * redraws the icon. It sets the auto_resize gboolean to TRUE.
 * This way, when the size_allocate callback gets triggered, it notices
 * that this is an autoresize, and after the main loop iterates, it
 * gets set back to FALSE
 */
#endif

static gboolean redraw_icon(gpointer data)
{
  PidginConversation *gtkconv = (PidginConversation *)data;
  PurpleConversation *conv = (gtkconv -> active_conv);
  PurpleAccount *account;
  GdkPixbuf *buf;
  GdkPixbuf *scale;
  gint delay;
  int scale_width;
  int scale_height;
  int size;
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  account = purple_conversation_get_account(conv);
  if (!((account != 0) && ((account -> gc) != 0))) {
    ( *gtkconv -> u.im).icon_timer = 0;
    return 0;
  }
  gdk_pixbuf_animation_iter_advance(( *gtkconv -> u.im).iter,0);
  buf = gdk_pixbuf_animation_iter_get_pixbuf(( *gtkconv -> u.im).iter);
  scale_width = gdk_pixbuf_get_width(buf);
  scale_height = gdk_pixbuf_get_height(buf);
  gtk_widget_get_size_request(( *gtkconv -> u.im).icon_container,0,&size);
  size = ((size < (((scale_width < scale_height)?scale_width : scale_height)))?size : (((scale_width < scale_height)?scale_width : scale_height)));
  size = ((size > 96)?96 : (((size < 32)?32 : size)));
  if (scale_width == scale_height) {
    scale_width = (scale_height = size);
  }
  else if (scale_height > scale_width) {
    scale_width = ((size * scale_width) / scale_height);
    scale_height = size;
  }
  else {
    scale_height = ((size * scale_height) / scale_width);
    scale_width = size;
  }
  scale = gdk_pixbuf_scale_simple(buf,scale_width,scale_height,GDK_INTERP_BILINEAR);
  if (pidgin_gdk_pixbuf_is_opaque(scale) != 0) 
    pidgin_gdk_pixbuf_make_round(scale);
  gtk_image_set_from_pixbuf(((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)( *gtkconv -> u.im).icon),gtk_image_get_type()))),scale);
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)scale),((GType )(20 << 2))))));
  gtk_widget_queue_draw(( *gtkconv -> u.im).icon);
  delay = gdk_pixbuf_animation_iter_get_delay_time(( *gtkconv -> u.im).iter);
  if (delay < 100) 
    delay = 100;
  ( *gtkconv -> u.im).icon_timer = g_timeout_add(delay,redraw_icon,gtkconv);
  return 0;
}

static void start_anim(GtkObject *obj,PidginConversation *gtkconv)
{
  int delay;
  if (( *gtkconv -> u.im).anim == ((GdkPixbufAnimation *)((void *)0))) 
    return ;
  if (( *gtkconv -> u.im).icon_timer != 0) 
    return ;
  if (gdk_pixbuf_animation_is_static_image(( *gtkconv -> u.im).anim) != 0) 
    return ;
  delay = gdk_pixbuf_animation_iter_get_delay_time(( *gtkconv -> u.im).iter);
  if (delay < 100) 
    delay = 100;
  ( *gtkconv -> u.im).icon_timer = g_timeout_add(delay,redraw_icon,gtkconv);
}

static void remove_icon(GtkWidget *widget,PidginConversation *gtkconv)
{
  GList *children;
  GtkWidget *event;
  PurpleConversation *conv = (gtkconv -> active_conv);
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  gtk_widget_set_size_request(( *gtkconv -> u.im).icon_container,(-1),32);
  children = gtk_container_get_children(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *gtkconv -> u.im).icon_container),gtk_container_get_type()))));
  if (children != 0) {
/* We know there's only one child here. It'd be nice to shortcut to the
		   event box, but we can't change the PidginConversation until 3.0 */
    event = ((GtkWidget *)(children -> data));
    gtk_container_remove(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *gtkconv -> u.im).icon_container),gtk_container_get_type()))),event);
    g_list_free(children);
  }
  if (( *gtkconv -> u.im).anim != ((GdkPixbufAnimation *)((void *)0))) 
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *gtkconv -> u.im).anim),((GType )(20 << 2))))));
  if (( *gtkconv -> u.im).icon_timer != 0) 
    g_source_remove(( *gtkconv -> u.im).icon_timer);
  if (( *gtkconv -> u.im).iter != ((GdkPixbufAnimationIter *)((void *)0))) 
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *gtkconv -> u.im).iter),((GType )(20 << 2))))));
  ( *gtkconv -> u.im).icon_timer = 0;
  ( *gtkconv -> u.im).icon = ((GtkWidget *)((void *)0));
  ( *gtkconv -> u.im).anim = ((GdkPixbufAnimation *)((void *)0));
  ( *gtkconv -> u.im).iter = ((GdkPixbufAnimationIter *)((void *)0));
  ( *gtkconv -> u.im).show_icon = 0;
}

static void saveicon_writefile_cb(void *user_data,const char *filename)
{
  PidginConversation *gtkconv = (PidginConversation *)user_data;
  PurpleConversation *conv = (gtkconv -> active_conv);
  PurpleBuddyIcon *icon;
  const void *data;
  size_t len;
  icon = purple_conv_im_get_icon((purple_conversation_get_im_data(conv)));
  data = purple_buddy_icon_get_data(icon,&len);
  if (((len <= 0) || (data == ((const void *)((void *)0)))) || !(purple_util_write_data_to_file_absolute(filename,data,len) != 0)) {
    purple_notify_message(gtkconv,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Unable to save icon file to disk."))),0,0,0);
  }
}

static void custom_icon_sel_cb(const char *filename,gpointer data)
{
  if (filename != 0) {
    const gchar *name;
    PurpleBuddy *buddy;
    PurpleContact *contact;
    PidginConversation *gtkconv = data;
    PurpleConversation *conv = (gtkconv -> active_conv);
    PurpleAccount *account = purple_conversation_get_account(conv);
    name = purple_conversation_get_name(conv);
    buddy = purple_find_buddy(account,name);
    if (!(buddy != 0)) {
      purple_debug_info("custom-icon","You can only set custom icons for people on your buddylist.\n");
      return ;
    }
    contact = purple_buddy_get_contact(buddy);
    purple_buddy_icons_node_set_custom_icon_from_file(((PurpleBlistNode *)contact),filename);
  }
}

static void set_custom_icon_cb(GtkWidget *widget,PidginConversation *gtkconv)
{
  GtkWidget *win = pidgin_buddy_icon_chooser_new(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)( *(gtkconv -> win)).window),gtk_window_get_type()))),custom_icon_sel_cb,gtkconv);
  gtk_widget_show_all(win);
}

static void change_size_cb(GtkWidget *widget,PidginConversation *gtkconv)
{
  int size = 0;
  PurpleConversation *conv = (gtkconv -> active_conv);
  GSList *buddies;
  gtk_widget_get_size_request(( *gtkconv -> u.im).icon_container,0,&size);
  if (size == 96) {
    size = 32;
  }
  else {
    size = 96;
  }
  gtk_widget_set_size_request(( *gtkconv -> u.im).icon_container,(-1),size);
  pidgin_conv_update_buddy_icon(conv);
  buddies = purple_find_buddies(purple_conversation_get_account(conv),purple_conversation_get_name(conv));
  for (; buddies != 0; buddies = g_slist_delete_link(buddies,buddies)) {
    PurpleBuddy *buddy = (buddies -> data);
    PurpleContact *contact = purple_buddy_get_contact(buddy);
    purple_blist_node_set_int(((PurpleBlistNode *)contact),"pidgin-infopane-iconsize",size);
  }
}

static void remove_custom_icon_cb(GtkWidget *widget,PidginConversation *gtkconv)
{
  const gchar *name;
  PurpleBuddy *buddy;
  PurpleAccount *account;
  PurpleContact *contact;
  PurpleConversation *conv = (gtkconv -> active_conv);
  account = purple_conversation_get_account(conv);
  name = purple_conversation_get_name(conv);
  buddy = purple_find_buddy(account,name);
  if (!(buddy != 0)) {
    return ;
  }
  contact = purple_buddy_get_contact(buddy);
  purple_buddy_icons_node_set_custom_icon_from_file(((PurpleBlistNode *)contact),0);
}

static void icon_menu_save_cb(GtkWidget *widget,PidginConversation *gtkconv)
{
  PurpleConversation *conv = (gtkconv -> active_conv);
  const gchar *ext;
  gchar *buf;
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  ext = purple_buddy_icon_get_extension((purple_conv_im_get_icon((purple_conversation_get_im_data(conv)))));
  buf = g_strdup_printf("%s.%s",purple_normalize((conv -> account),(conv -> name)),ext);
  purple_request_file(gtkconv,((const char *)(dgettext("pidgin","Save Icon"))),buf,(!0),((GCallback )saveicon_writefile_cb),0,(conv -> account),0,conv,gtkconv);
  g_free(buf);
}

static void stop_anim(GtkObject *obj,PidginConversation *gtkconv)
{
  if (( *gtkconv -> u.im).icon_timer != 0) 
    g_source_remove(( *gtkconv -> u.im).icon_timer);
  ( *gtkconv -> u.im).icon_timer = 0;
}

static void toggle_icon_animate_cb(GtkWidget *w,PidginConversation *gtkconv)
{
  ( *gtkconv -> u.im).animate = gtk_check_menu_item_get_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)w),gtk_check_menu_item_get_type()))));
  if (( *gtkconv -> u.im).animate != 0) 
    start_anim(0,gtkconv);
  else 
    stop_anim(0,gtkconv);
}

static gboolean icon_menu(GtkObject *obj,GdkEventButton *e,PidginConversation *gtkconv)
{
  static GtkWidget *menu = (GtkWidget *)((void *)0);
  PurpleConversation *conv;
  PurpleBuddy *buddy;
  if (((e -> button) == 1) && ((e -> type) == GDK_BUTTON_PRESS)) {
    change_size_cb(0,gtkconv);
    return (!0);
  }
  if (((e -> button) != 3) || ((e -> type) != GDK_BUTTON_PRESS)) {
    return 0;
  }
/*
	 * If a menu already exists, destroy it before creating a new one,
	 * thus freeing-up the memory it occupied.
	 */
  if (menu != ((GtkWidget *)((void *)0))) 
    gtk_widget_destroy(menu);
  menu = gtk_menu_new();
  if ((( *gtkconv -> u.im).anim != 0) && !(gdk_pixbuf_animation_is_static_image(( *gtkconv -> u.im).anim) != 0)) {
    pidgin_new_check_item(menu,((const char *)(dgettext("pidgin","Animate"))),((GCallback )toggle_icon_animate_cb),gtkconv,( *gtkconv -> u.im).icon_timer);
  }
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Hide Icon"))),0,((GCallback )remove_icon),gtkconv,0,0,0);
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Save Icon As..."))),"gtk-save-as",((GCallback )icon_menu_save_cb),gtkconv,0,0,0);
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Set Custom Icon..."))),0,((GCallback )set_custom_icon_cb),gtkconv,0,0,0);
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Change Size"))),0,((GCallback )change_size_cb),gtkconv,0,0,0);
/* Is there a custom icon for this person? */
  conv = (gtkconv -> active_conv);
  buddy = purple_find_buddy(purple_conversation_get_account(conv),purple_conversation_get_name(conv));
  if (buddy != 0) {
    PurpleContact *contact = purple_buddy_get_contact(buddy);
    if ((contact != 0) && (purple_buddy_icons_node_has_custom_icon(((PurpleBlistNode *)contact)) != 0)) {
      pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Remove Custom Icon"))),0,((GCallback )remove_custom_icon_cb),gtkconv,0,0,0);
    }
  }
  gtk_menu_popup(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_get_type()))),0,0,0,0,(e -> button),(e -> time));
  return (!0);
}
/**************************************************************************
 * End of the bunch of buddy icon functions
 **************************************************************************/

void pidgin_conv_present_conversation(PurpleConversation *conv)
{
  PidginConversation *gtkconv;
  GdkModifierType state;
  pidgin_conv_attach_to_conversation(conv);
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  pidgin_conv_switch_active_conversation(conv);
/* Switch the tab only if the user initiated the event by pressing
	 * a button or hitting a key. */
  if (gtk_get_current_event_state(&state) != 0) 
    pidgin_conv_window_switch_gtkconv((gtkconv -> win),gtkconv);
  gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)( *(gtkconv -> win)).window),gtk_window_get_type()))));
}

GList *pidgin_conversations_find_unseen_list(PurpleConversationType type,PidginUnseenState min_state,gboolean hidden_only,guint max_count)
{
  GList *l;
  GList *r = (GList *)((void *)0);
  guint c = 0;
  if (type == PURPLE_CONV_TYPE_IM) {
    l = purple_get_ims();
  }
  else if (type == PURPLE_CONV_TYPE_CHAT) {
    l = purple_get_chats();
  }
  else {
    l = purple_get_conversations();
  }
  for (; (l != ((GList *)((void *)0))) && ((max_count == 0) || (c < max_count)); l = (l -> next)) {{
      PurpleConversation *conv = (PurpleConversation *)(l -> data);
      PidginConversation *gtkconv = (PidginConversation *)(conv -> ui_data);
      if ((gtkconv == ((PidginConversation *)((void *)0))) || ((gtkconv -> active_conv) != conv)) 
        continue; 
      if (((gtkconv -> unseen_state) >= min_state) && (!(hidden_only != 0) || ((hidden_only != 0) && ((gtkconv -> win) == hidden_convwin)))) {
        r = g_list_prepend(r,conv);
        c++;
      }
    }
  }
  return r;
}

static void unseen_conv_menu_cb(GtkMenuItem *item,PurpleConversation *conv)
{
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  pidgin_conv_present_conversation(conv);
}

static void unseen_all_conv_menu_cb(GtkMenuItem *item,GList *list)
{
  do {
    if (list != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"list != NULL");
      return ;
    };
  }while (0);
/* Do not free the list from here. It will be freed from the
	 * 'destroy' callback on the menuitem. */
  while(list != 0){
    pidgin_conv_present_conversation((list -> data));
    list = (list -> next);
  }
}

guint pidgin_conversations_fill_menu(GtkWidget *menu,GList *convs)
{
  GList *l;
  guint ret = 0;
  do {
    if (menu != ((GtkWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"menu != NULL");
      return 0;
    };
  }while (0);
  do {
    if (convs != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"convs != NULL");
      return 0;
    };
  }while (0);
  for (l = convs; l != ((GList *)((void *)0)); l = (l -> next)) {
    PurpleConversation *conv = (PurpleConversation *)(l -> data);
    PidginConversation *gtkconv = (PidginConversation *)(conv -> ui_data);
    GtkWidget *icon = gtk_image_new_from_stock(pidgin_conv_get_icon_stock(conv),gtk_icon_size_from_name("pidgin-icon-size-tango-microscopic"));
    GtkWidget *item;
    gchar *text = g_strdup_printf("%s (%d)",gtk_label_get_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tab_label)),gtk_label_get_type())))),(gtkconv -> unseen_count));
    item = gtk_image_menu_item_new_with_label(text);
    gtk_image_menu_item_set_image(((GtkImageMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_image_menu_item_get_type()))),icon);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"activate",((GCallback )unseen_conv_menu_cb),conv,0,((GConnectFlags )0));
    gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item);
    g_free(text);
    ret++;
  }
  if ((convs -> next) != 0) {
/* There are more than one conversation. Add an option to show all conversations. */
    GtkWidget *item;
    GList *list = g_list_copy(convs);
    pidgin_separator(menu);
    item = gtk_menu_item_new_with_label(((const char *)(dgettext("pidgin","Show All"))));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"activate",((GCallback )unseen_all_conv_menu_cb),list,0,((GConnectFlags )0));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"destroy",((GCallback )g_list_free),list,0,G_CONNECT_SWAPPED);
    gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item);
  }
  return ret;
}

PidginWindow *pidgin_conv_get_window(PidginConversation *gtkconv)
{
  do {
    if (gtkconv != ((PidginConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gtkconv != NULL");
      return 0;
    };
  }while (0);
  return gtkconv -> win;
}
static GtkItemFactoryEntry menu_items[] = {
/* Conversation menu */
{("/_Conversation"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Branch>"), ((gconstpointer )((void *)0))}, {("/Conversation/New Instant _Message..."), ("<CTL>M"), (menu_new_conv_cb), (0), ("<StockItem>"), ("pidgin-message-new")}, {("/Conversation/Join a _Chat..."), ((gchar *)((void *)0)), (menu_join_chat_cb), (0), ("<StockItem>"), ("pidgin-chat")}, {("/Conversation/sep0"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Separator>"), ((gconstpointer )((void *)0))}, {("/Conversation/_Find..."), ((gchar *)((void *)0)), (menu_find_cb), (0), ("<StockItem>"), ("gtk-find")}, {("/Conversation/View _Log"), ((gchar *)((void *)0)), (menu_view_log_cb), (0), ("<Item>"), ((gconstpointer )((void *)0))}, {("/Conversation/_Save As..."), ((gchar *)((void *)0)), (menu_save_as_cb), (0), ("<StockItem>"), ("gtk-save-as")}, {("/Conversation/Clea_r Scrollback"), ("<CTL>L"), (menu_clear_cb), (0), ("<StockItem>"), ("gtk-clear")}, {("/Conversation/sep1"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Separator>"), ((gconstpointer )((void *)0))}, 
#ifdef USE_VV
{("/Conversation/M_edia"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Branch>"), ((gconstpointer )((void *)0))}, {("/Conversation/Media/_Audio Call"), ((gchar *)((void *)0)), (menu_initiate_media_call_cb), (0), ("<StockItem>"), ("pidgin-audio-call")}, {("/Conversation/Media/_Video Call"), ((gchar *)((void *)0)), (menu_initiate_media_call_cb), (1), ("<StockItem>"), ("pidgin-video-call")}, {("/Conversation/Media/Audio\\/Video _Call"), ((gchar *)((void *)0)), (menu_initiate_media_call_cb), (2), ("<StockItem>"), ("pidgin-video-call")}, 
#endif
{("/Conversation/Se_nd File..."), ((gchar *)((void *)0)), (menu_send_file_cb), (0), ("<StockItem>"), ("pidgin-send-file")}, {("/Conversation/Get _Attention"), ((gchar *)((void *)0)), (menu_get_attention_cb), (0), ("<StockItem>"), ("pidgin-send-attention")}, {("/Conversation/Add Buddy _Pounce..."), ((gchar *)((void *)0)), (menu_add_pounce_cb), (0), ("<Item>"), ((gconstpointer )((void *)0))}, {("/Conversation/_Get Info"), ("<CTL>O"), (menu_get_info_cb), (0), ("<StockItem>"), ("pidgin-info")}, {("/Conversation/In_vite..."), ((gchar *)((void *)0)), (menu_invite_cb), (0), ("<Item>"), ((gconstpointer )((void *)0))}, {("/Conversation/M_ore"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Branch>"), ((gconstpointer )((void *)0))}, {("/Conversation/sep2"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Separator>"), ((gconstpointer )((void *)0))}, {("/Conversation/Al_ias..."), ((gchar *)((void *)0)), (menu_alias_cb), (0), ("<Item>"), ((gconstpointer )((void *)0))}, {("/Conversation/_Block..."), ((gchar *)((void *)0)), (menu_block_cb), (0), ("<StockItem>"), ("pidgin-block")}, {("/Conversation/_Unblock..."), ((gchar *)((void *)0)), (menu_unblock_cb), (0), ("<StockItem>"), ("pidgin-unblock")}, {("/Conversation/_Add..."), ((gchar *)((void *)0)), (menu_add_remove_cb), (0), ("<StockItem>"), ("gtk-add")}, {("/Conversation/_Remove..."), ((gchar *)((void *)0)), (menu_add_remove_cb), (0), ("<StockItem>"), ("gtk-remove")}, {("/Conversation/sep3"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Separator>"), ((gconstpointer )((void *)0))}, {("/Conversation/Insert Lin_k..."), ((gchar *)((void *)0)), (menu_insert_link_cb), (0), ("<StockItem>"), ("pidgin-insert-link")}, {("/Conversation/Insert Imag_e..."), ((gchar *)((void *)0)), (menu_insert_image_cb), (0), ("<StockItem>"), ("pidgin-insert-image")}, {("/Conversation/sep4"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Separator>"), ((gconstpointer )((void *)0))}, {("/Conversation/_Close"), ((gchar *)((void *)0)), (menu_close_conv_cb), (0), ("<StockItem>"), ("gtk-close")}, 
/* Options */
{("/_Options"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Branch>"), ((gconstpointer )((void *)0))}, {("/Options/Enable _Logging"), ((gchar *)((void *)0)), (menu_logging_cb), (0), ("<CheckItem>"), ((gconstpointer )((void *)0))}, {("/Options/Enable _Sounds"), ((gchar *)((void *)0)), (menu_sounds_cb), (0), ("<CheckItem>"), ((gconstpointer )((void *)0))}, {("/Options/sep0"), ((gchar *)((void *)0)), ((GtkItemFactoryCallback )((void *)0)), (0), ("<Separator>"), ((gconstpointer )((void *)0))}, {("/Options/Show Formatting _Toolbars"), ((gchar *)((void *)0)), (menu_toolbar_cb), (0), ("<CheckItem>"), ((gconstpointer )((void *)0))}, {("/Options/Show Ti_mestamps"), ((gchar *)((void *)0)), (menu_timestamps_cb), (0), ("<CheckItem>"), ((gconstpointer )((void *)0))}};
static const int menu_item_count = (sizeof(menu_items) / sizeof(( *menu_items)));

static const char *item_factory_translate_func(const char *path,gpointer func_data)
{
  return (const char *)(dgettext("pidgin",path));
}

static void sound_method_pref_changed_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  PidginWindow *win = data;
  const char *method = value;
  if (!(strcmp(method,"none") != 0)) {
    gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)win -> menu.sounds),gtk_check_menu_item_get_type()))),0);
    gtk_widget_set_sensitive(win -> menu.sounds,0);
  }
  else {
    PidginConversation *gtkconv = pidgin_conv_window_get_active_gtkconv(win);
    if (gtkconv != ((PidginConversation *)((void *)0))) 
      gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)win -> menu.sounds),gtk_check_menu_item_get_type()))),(gtkconv -> make_sound));
    gtk_widget_set_sensitive(win -> menu.sounds,(!0));
  }
}
/* Returns TRUE if some items were added to the menu, FALSE otherwise */

static gboolean populate_menu_with_options(GtkWidget *menu,PidginConversation *gtkconv,gboolean all)
{
  GList *list;
  PurpleConversation *conv;
  PurpleBlistNode *node = (PurpleBlistNode *)((void *)0);
  PurpleChat *chat = (PurpleChat *)((void *)0);
  PurpleBuddy *buddy = (PurpleBuddy *)((void *)0);
  gboolean ret;
  conv = (gtkconv -> active_conv);
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) {
    chat = purple_blist_find_chat((conv -> account),(conv -> name));
    if ((chat == ((PurpleChat *)((void *)0))) && ((gtkconv -> imhtml) != ((GtkWidget *)((void *)0)))) {
      chat = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),((GType )(20 << 2))))),"transient_chat"));
    }
    if ((chat == ((PurpleChat *)((void *)0))) && ((gtkconv -> imhtml) != ((GtkWidget *)((void *)0)))) {
      GHashTable *components;
      PurpleAccount *account = purple_conversation_get_account(conv);
      PurplePlugin *prpl = purple_find_prpl(purple_account_get_protocol_id(account));
      PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info;
      if ((purple_account_get_connection(account) != ((PurpleConnection *)((void *)0))) && (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).chat_info_defaults))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).chat_info_defaults))) < (prpl_info -> struct_size))) && ((prpl_info -> chat_info_defaults) != ((GHashTable *(*)(PurpleConnection *, const char *))((void *)0))))) {
        components = ( *(prpl_info -> chat_info_defaults))(purple_account_get_connection(account),purple_conversation_get_name(conv));
      }
      else {
        components = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
        g_hash_table_replace(components,(g_strdup("channel")),(g_strdup(purple_conversation_get_name(conv))));
      }
      chat = purple_chat_new((conv -> account),0,components);
      purple_blist_node_set_flags(((PurpleBlistNode *)chat),PURPLE_BLIST_NODE_FLAG_NO_SAVE);
      g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),((GType )(20 << 2))))),"transient_chat",chat,((GDestroyNotify )purple_blist_remove_chat));
    }
  }
  else {
    if (!(purple_account_is_connected((conv -> account)) != 0)) 
      return 0;
    buddy = purple_find_buddy((conv -> account),(conv -> name));
/* gotta remain bug-compatible :( libpurple < 2.0.2 didn't handle
		 * removing "isolated" buddy nodes well */
    if (purple_version_check(2,0,2) == ((const char *)((void *)0))) {
      if ((buddy == ((PurpleBuddy *)((void *)0))) && ((gtkconv -> imhtml) != ((GtkWidget *)((void *)0)))) {
        buddy = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),((GType )(20 << 2))))),"transient_buddy"));
      }
      if ((buddy == ((PurpleBuddy *)((void *)0))) && ((gtkconv -> imhtml) != ((GtkWidget *)((void *)0)))) {
        buddy = purple_buddy_new((conv -> account),(conv -> name),0);
        purple_blist_node_set_flags(((PurpleBlistNode *)buddy),PURPLE_BLIST_NODE_FLAG_NO_SAVE);
        g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),((GType )(20 << 2))))),"transient_buddy",buddy,((GDestroyNotify )purple_buddy_destroy));
      }
    }
  }
  if (chat != 0) 
    node = ((PurpleBlistNode *)chat);
  else if (buddy != 0) 
    node = ((PurpleBlistNode *)buddy);
/* Now add the stuff */
  if (all != 0) {
    if (buddy != 0) 
      pidgin_blist_make_buddy_menu(menu,buddy,(!0));
    else if (chat != 0) {
/* XXX: */
    }
  }
  else if (node != 0) {
    if (purple_account_is_connected((conv -> account)) != 0) 
      pidgin_append_blist_node_proto_menu(menu,( *(conv -> account)).gc,node);
    pidgin_append_blist_node_extended_menu(menu,node);
  }
  if ((list = gtk_container_get_children(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_container_get_type()))))) == ((GList *)((void *)0))) {
    ret = 0;
  }
  else {
    g_list_free(list);
    ret = (!0);
  }
  return ret;
}

static void regenerate_media_items(PidginWindow *win)
{
#ifdef USE_VV
  PurpleAccount *account;
  PurpleConversation *conv;
  conv = pidgin_conv_window_get_active_conversation(win);
  if (conv == ((PurpleConversation *)((void *)0))) {
    purple_debug_error("gtkconv","couldn\'t get active conversation when regenerating media items\n");
    return ;
  }
  account = purple_conversation_get_account(conv);
  if (account == ((PurpleAccount *)((void *)0))) {
    purple_debug_error("gtkconv","couldn\'t get account when regenerating media items\n");
    return ;
  }
/*
	 * Check if account support voice and/or calls, and
	 * if the current buddy	supports it.
	 */
  if ((account != ((PurpleAccount *)((void *)0))) && ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM)) {
    PurpleMediaCaps caps = purple_prpl_get_media_caps(account,purple_conversation_get_name(conv));
    gtk_widget_set_sensitive((win -> audio_call),(((caps & PURPLE_MEDIA_CAPS_AUDIO) != 0U)?!0 : 0));
    gtk_widget_set_sensitive((win -> video_call),(((caps & PURPLE_MEDIA_CAPS_VIDEO) != 0U)?!0 : 0));
    gtk_widget_set_sensitive((win -> audio_video_call),(((caps & PURPLE_MEDIA_CAPS_AUDIO_VIDEO) != 0U)?!0 : 0));
  }
  else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) {
/* for now, don't care about chats... */
    gtk_widget_set_sensitive((win -> audio_call),0);
    gtk_widget_set_sensitive((win -> video_call),0);
    gtk_widget_set_sensitive((win -> audio_video_call),0);
  }
  else {
    gtk_widget_set_sensitive((win -> audio_call),0);
    gtk_widget_set_sensitive((win -> video_call),0);
    gtk_widget_set_sensitive((win -> audio_video_call),0);
  }
#endif
}

static void regenerate_options_items(PidginWindow *win)
{
  GtkWidget *menu;
  PidginConversation *gtkconv;
  GList *list;
  gtkconv = pidgin_conv_window_get_active_gtkconv(win);
  menu = gtk_item_factory_get_widget(win -> menu.item_factory,"/Conversation/More");
/* Remove the previous entries */
  for (list = gtk_container_get_children(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_container_get_type())))); list != 0; ) {
    GtkWidget *w = (list -> data);
    list = g_list_delete_link(list,list);
    gtk_widget_destroy(w);
  }
  if (!(populate_menu_with_options(menu,gtkconv,0) != 0)) {
    GtkWidget *item = gtk_menu_item_new_with_label(((const char *)(dgettext("pidgin","No actions available"))));
    gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item);
    gtk_widget_set_sensitive(item,0);
  }
  gtk_widget_show_all(menu);
}

static void remove_from_list(GtkWidget *widget,PidginWindow *win)
{
  GList *list = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),"plugin-actions"));
  list = g_list_remove(list,widget);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),"plugin-actions",list);
}

static void regenerate_plugins_items(PidginWindow *win)
{
  GList *action_items;
  GtkWidget *menu;
  GList *list;
  PidginConversation *gtkconv;
  PurpleConversation *conv;
  GtkWidget *item;
  if (((win -> window) == ((GtkWidget *)((void *)0))) || (win == hidden_convwin)) 
    return ;
  gtkconv = pidgin_conv_window_get_active_gtkconv(win);
  if (gtkconv == ((PidginConversation *)((void *)0))) 
    return ;
  conv = (gtkconv -> active_conv);
  action_items = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),"plugin-actions"));
/* Remove the old menuitems */
  while(action_items != 0){
    g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(action_items -> data)),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )remove_from_list),win);
    gtk_widget_destroy((action_items -> data));
    action_items = g_list_delete_link(action_items,action_items);
  }
  menu = gtk_item_factory_get_widget(win -> menu.item_factory,"/Options");
  list = purple_conversation_get_extended_menu(conv);
  if (list != 0) {
    action_items = g_list_prepend(0,(item = pidgin_separator(menu)));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"destroy",((GCallback )remove_from_list),win,0,((GConnectFlags )0));
  }
  for (; list != 0; list = g_list_delete_link(list,list)) {
    PurpleMenuAction *act = (PurpleMenuAction *)(list -> data);
    item = pidgin_append_menu_action(menu,act,conv);
    action_items = g_list_prepend(action_items,item);
    gtk_widget_show_all(item);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"destroy",((GCallback )remove_from_list),win,0,((GConnectFlags )0));
  }
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),"plugin-actions",action_items);
}

static void menubar_activated(GtkWidget *item,gpointer data)
{
  PidginWindow *win = data;
  regenerate_media_items(win);
  regenerate_options_items(win);
  regenerate_plugins_items(win);
/* The following are to make sure the 'More' submenu is not regenerated every time
	 * the focus shifts from 'Conversations' to some other menu and back. */
  g_signal_handlers_block_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )menubar_activated),data);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win -> menu.menubar),((GType )(20 << 2))))),"deactivate",((GCallback )focus_out_from_menubar),data,0,((GConnectFlags )0));
}

static void focus_out_from_menubar(GtkWidget *wid,PidginWindow *win)
{
/* The menubar has been deactivated. Make sure the 'More' submenu is regenerated next time
	 * the 'Conversation' menu pops up. */
  GtkWidget *menuitem = gtk_item_factory_get_item(win -> menu.item_factory,"/Conversation");
  g_signal_handlers_unblock_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )menubar_activated),win);
  g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win -> menu.menubar),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )focus_out_from_menubar),win);
}

static GtkWidget *setup_menubar(PidginWindow *win)
{
  GtkAccelGroup *accel_group;
  const char *method;
  GtkWidget *menuitem;
  accel_group = gtk_accel_group_new();
  gtk_window_add_accel_group(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),gtk_window_get_type()))),accel_group);
  g_object_unref(accel_group);
  win -> menu.item_factory = gtk_item_factory_new(gtk_menu_bar_get_type(),"<main>",accel_group);
  gtk_item_factory_set_translate_func(win -> menu.item_factory,((GtkTranslateFunc )item_factory_translate_func),0,0);
  gtk_item_factory_create_items(win -> menu.item_factory,menu_item_count,menu_items,win);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)accel_group),((GType )(20 << 2))))),"accel-changed",((GCallback )pidgin_save_accels_cb),0,0,((GConnectFlags )0));
/* Make sure the 'Conversation -> More' menuitems are regenerated whenever
	 * the 'Conversation' menu pops up because the entries can change after the
	 * conversation is created. */
  menuitem = gtk_item_factory_get_item(win -> menu.item_factory,"/Conversation");
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"activate",((GCallback )menubar_activated),win,0,((GConnectFlags )0));
  win -> menu.menubar = gtk_item_factory_get_widget(win -> menu.item_factory,"<main>");
  win -> menu.view_log = gtk_item_factory_get_widget(win -> menu.item_factory,"/Conversation/View Log");
#ifdef USE_VV
  win -> audio_call = gtk_item_factory_get_widget(win -> menu.item_factory,"/Conversation/Media/Audio Call");
  win -> video_call = gtk_item_factory_get_widget(win -> menu.item_factory,"/Conversation/Media/Video Call");
  win -> audio_video_call = gtk_item_factory_get_widget(win -> menu.item_factory,"/Conversation/Media/Audio\\/Video Call");
#endif
/* --- */
  win -> menu.send_file = gtk_item_factory_get_widget(win -> menu.item_factory,"/Conversation/Send File...");
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),"get_attention",(gtk_item_factory_get_widget(win -> menu.item_factory,"/Conversation/Get Attention")));
  win -> menu.add_pounce = gtk_item_factory_get_widget(win -> menu.item_factory,"/Conversation/Add Buddy Pounce...");
/* --- */
  win -> menu.get_info = gtk_item_factory_get_widget(win -> menu.item_factory,"/Conversation/Get Info");
  win -> menu.invite = gtk_item_factory_get_widget(win -> menu.item_factory,"/Conversation/Invite...");
/* --- */
  win -> menu.alias = gtk_item_factory_get_widget(win -> menu.item_factory,"/Conversation/Alias...");
  win -> menu.block = gtk_item_factory_get_widget(win -> menu.item_factory,"/Conversation/Block...");
  win -> menu.unblock = gtk_item_factory_get_widget(win -> menu.item_factory,"/Conversation/Unblock...");
  win -> menu.add = gtk_item_factory_get_widget(win -> menu.item_factory,"/Conversation/Add...");
  win -> menu.remove = gtk_item_factory_get_widget(win -> menu.item_factory,"/Conversation/Remove...");
/* --- */
  win -> menu.insert_link = gtk_item_factory_get_widget(win -> menu.item_factory,"/Conversation/Insert Link...");
  win -> menu.insert_image = gtk_item_factory_get_widget(win -> menu.item_factory,"/Conversation/Insert Image...");
/* --- */
  win -> menu.logging = gtk_item_factory_get_widget(win -> menu.item_factory,"/Options/Enable Logging");
  win -> menu.sounds = gtk_item_factory_get_widget(win -> menu.item_factory,"/Options/Enable Sounds");
  method = purple_prefs_get_string("/pidgin/sound/method");
  if ((method != ((const char *)((void *)0))) && !(strcmp(method,"none") != 0)) {
    gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)win -> menu.sounds),gtk_check_menu_item_get_type()))),0);
    gtk_widget_set_sensitive(win -> menu.sounds,0);
  }
  purple_prefs_connect_callback(win,"/pidgin/sound/method",sound_method_pref_changed_cb,win);
  win -> menu.show_formatting_toolbar = gtk_item_factory_get_widget(win -> menu.item_factory,"/Options/Show Formatting Toolbars");
  win -> menu.show_timestamps = gtk_item_factory_get_widget(win -> menu.item_factory,"/Options/Show Timestamps");
  win -> menu.show_icon = ((GtkWidget *)((void *)0));
  win -> menu.tray = pidgin_menu_tray_new();
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)win -> menu.menubar),gtk_menu_shell_get_type()))),win -> menu.tray);
  gtk_widget_show(win -> menu.tray);
  gtk_widget_show(win -> menu.menubar);
  return win -> menu.menubar;
}
/**************************************************************************
 * Utility functions
 **************************************************************************/

static void got_typing_keypress(PidginConversation *gtkconv,gboolean first)
{
  PurpleConversation *conv = (gtkconv -> active_conv);
  PurpleConvIm *im;
/*
	 * We know we got something, so we at least have to make sure we don't
	 * send PURPLE_TYPED any time soon.
	 */
  im = purple_conversation_get_im_data(conv);
  purple_conv_im_stop_send_typed_timeout(im);
  purple_conv_im_start_send_typed_timeout(im);
/* Check if we need to send another PURPLE_TYPING message */
  if ((first != 0) || ((purple_conv_im_get_type_again(im) != 0) && (time(0) > purple_conv_im_get_type_again(im)))) {
    unsigned int timeout;
    timeout = serv_send_typing(purple_conversation_get_gc(conv),purple_conversation_get_name(conv),PURPLE_TYPING);
    purple_conv_im_set_type_again(im,timeout);
  }
}
#if 0
#endif

static void update_typing_message(PidginConversation *gtkconv,const char *message)
{
  GtkTextBuffer *buffer;
  GtkTextMark *stmark;
  GtkTextMark *enmark;
  if (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),((GType )(20 << 2))))),"disable-typing-notification") != 0) 
    return ;
  buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_text_view_get_type()))));
  stmark = gtk_text_buffer_get_mark(buffer,"typing-notification-start");
  enmark = gtk_text_buffer_get_mark(buffer,"typing-notification-end");
  if ((stmark != 0) && (enmark != 0)) {
    GtkTextIter start;
    GtkTextIter end;
    gtk_text_buffer_get_iter_at_mark(buffer,&start,stmark);
    gtk_text_buffer_get_iter_at_mark(buffer,&end,enmark);
    gtk_text_buffer_delete_mark(buffer,stmark);
    gtk_text_buffer_delete_mark(buffer,enmark);
    gtk_text_buffer_delete(buffer,&start,&end);
  }
  else if ((((message != 0) && (( *message) == 10)) && (message[1] == 32)) && (message[2] == 0)) 
    message = ((const char *)((void *)0));
#ifdef RESERVE_LINE
/* The blank space is required to avoid a GTK+/Pango bug */
#endif
  if (message != 0) {
    GtkTextIter iter;
    gtk_text_buffer_get_end_iter(buffer,&iter);
    gtk_text_buffer_create_mark(buffer,"typing-notification-start",(&iter),(!0));
    gtk_text_buffer_insert_with_tags_by_name(buffer,&iter,message,(-1),"TYPING-NOTIFICATION",((void *)((void *)0)));
    gtk_text_buffer_get_end_iter(buffer,&iter);
    gtk_text_buffer_create_mark(buffer,"typing-notification-end",(&iter),(!0));
  }
}

static void update_typing_icon(PidginConversation *gtkconv)
{
  PurpleConvIm *im = (PurpleConvIm *)((void *)0);
  PurpleConversation *conv = (gtkconv -> active_conv);
  char *message = (char *)((void *)0);
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) 
    im = purple_conversation_get_im_data(conv);
  if (im == ((PurpleConvIm *)((void *)0))) 
    return ;
  if ((purple_conv_im_get_typing_state(im)) == PURPLE_NOT_TYPING) {
#ifdef RESERVE_LINE
#else
    update_typing_message(gtkconv,"\n ");
#endif
    return ;
  }
  if ((purple_conv_im_get_typing_state(im)) == PURPLE_TYPING) {
    message = g_strdup_printf(((const char *)(dgettext("pidgin","\n%s is typing..."))),purple_conversation_get_title(conv));
  }
  else {
    message = g_strdup_printf(((const char *)(dgettext("pidgin","\n%s has stopped typing"))),purple_conversation_get_title(conv));
  }
  update_typing_message(gtkconv,message);
  g_free(message);
}

static gboolean update_send_to_selection(PidginWindow *win)
{
  PurpleAccount *account;
  PurpleConversation *conv;
  GtkWidget *menu;
  GList *child;
  PurpleBuddy *b;
  conv = pidgin_conv_window_get_active_conversation(win);
  if (conv == ((PurpleConversation *)((void *)0))) 
    return 0;
  account = purple_conversation_get_account(conv);
  if (account == ((PurpleAccount *)((void *)0))) 
    return 0;
  if (win -> menu.send_to == ((GtkWidget *)((void *)0))) 
    return 0;
  if (!((b = purple_find_buddy(account,(conv -> name))) != 0)) 
    return 0;
  gtk_widget_show(win -> menu.send_to);
  menu = gtk_menu_item_get_submenu(((GtkMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)win -> menu.send_to),gtk_menu_item_get_type()))));
{
    for (child = gtk_container_get_children(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_container_get_type())))); child != ((GList *)((void *)0)); child = g_list_delete_link(child,child)) {
      GtkWidget *item = (child -> data);
      PurpleBuddy *item_buddy;
      PurpleAccount *item_account = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"purple_account"));
      gchar *buddy_name = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"purple_buddy_name"));
      item_buddy = purple_find_buddy(item_account,buddy_name);
      if (b == item_buddy) {
        gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_check_menu_item_get_type()))),(!0));
        g_list_free(child);
        break; 
      }
    }
  }
  return 0;
}

static gboolean send_to_item_enter_notify_cb(GtkWidget *menuitem,GdkEventCrossing *event,GtkWidget *label)
{
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_widget_get_type()))),(!0));
  return 0;
}

static gboolean send_to_item_leave_notify_cb(GtkWidget *menuitem,GdkEventCrossing *event,GtkWidget *label)
{
  gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_widget_get_type()))),0);
  return 0;
}

static void create_sendto_item(GtkWidget *menu,GtkSizeGroup *sg,GSList **group,PurpleBuddy *buddy,PurpleAccount *account,const char *name)
{
  GtkWidget *box;
  GtkWidget *label;
  GtkWidget *image;
  GtkWidget *menuitem;
  GdkPixbuf *pixbuf;
  gchar *text;
/* Create a pixmap for the protocol icon. */
  pixbuf = pidgin_create_prpl_icon(account,PIDGIN_PRPL_ICON_SMALL);
/* Now convert it to GtkImage */
  if (pixbuf == ((GdkPixbuf *)((void *)0))) 
    image = gtk_image_new();
  else {
    image = gtk_image_new_from_pixbuf(pixbuf);
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
  }
  gtk_size_group_add_widget(sg,image);
/* Make our menu item */
  text = g_strdup_printf("%s (%s)",name,purple_account_get_name_for_display(account));
  menuitem = gtk_radio_menu_item_new_with_label( *group,text);
  g_free(text);
   *group = gtk_radio_menu_item_get_group(((GtkRadioMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_radio_menu_item_get_type()))));
/* Do some evil, see some evil, speak some evil. */
  box = gtk_hbox_new(0,0);
  label = gtk_bin_get_child(((GtkBin *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_bin_get_type()))));
  g_object_ref(label);
  gtk_container_remove(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_container_get_type()))),label);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gtk_box_get_type()))),image,0,0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gtk_box_get_type()))),label,(!0),(!0),4);
  if ((buddy != ((PurpleBuddy *)((void *)0))) && !(purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0)) {
    gtk_widget_set_sensitive(label,0);
/* Set the label sensitive when the menuitem is highlighted and
		 * insensitive again when the mouse leaves it. This way, it
		 * doesn't appear weird from the highlighting of the embossed
		 * (insensitive style) text.*/
    g_signal_connect_data(menuitem,"enter-notify-event",((GCallback )send_to_item_enter_notify_cb),label,0,((GConnectFlags )0));
    g_signal_connect_data(menuitem,"leave-notify-event",((GCallback )send_to_item_leave_notify_cb),label,0,((GConnectFlags )0));
  }
  g_object_unref(label);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_container_get_type()))),box);
  gtk_widget_show(label);
  gtk_widget_show(image);
  gtk_widget_show(box);
/* Set our data and callbacks. */
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"purple_account",account);
  g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"purple_buddy_name",(g_strdup(name)),g_free);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"activate",((GCallback )menu_conv_sel_send_cb),0,0,((GConnectFlags )0));
  gtk_widget_show(menuitem);
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem);
}

static gboolean compare_buddy_presence(PurplePresence *p1,PurplePresence *p2)
{
/* This is necessary because multiple PurpleBuddy's don't share the same
	 * PurplePresence anymore.
	 */
  PurpleBuddy *b1 = purple_presence_get_buddy(p1);
  PurpleBuddy *b2 = purple_presence_get_buddy(p2);
  if ((purple_buddy_get_account(b1) == purple_buddy_get_account(b2)) && (strcmp(purple_buddy_get_name(b1),purple_buddy_get_name(b2)) == 0)) 
    return 0;
  return (!0);
}

static void generate_send_to_items(PidginWindow *win)
{
  GtkWidget *menu;
  GSList *group = (GSList *)((void *)0);
  GtkSizeGroup *sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  PidginConversation *gtkconv;
  GSList *l;
  GSList *buds;
  do {
    if (win != ((PidginWindow *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"win != NULL");
      return ;
    };
  }while (0);
  gtkconv = pidgin_conv_window_get_active_gtkconv(win);
  do {
    if (gtkconv != ((PidginConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gtkconv != NULL");
      return ;
    };
  }while (0);
  if (win -> menu.send_to != ((GtkWidget *)((void *)0))) 
    gtk_widget_destroy(win -> menu.send_to);
/* Build the Send To menu */
  win -> menu.send_to = gtk_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","S_end To"))));
  gtk_widget_show(win -> menu.send_to);
  menu = gtk_menu_new();
  gtk_menu_shell_insert(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)win -> menu.menubar),gtk_menu_shell_get_type()))),win -> menu.send_to,2);
  gtk_menu_item_set_submenu(((GtkMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)win -> menu.send_to),gtk_menu_item_get_type()))),menu);
  gtk_widget_show(menu);
  if (( *(gtkconv -> active_conv)).type == PURPLE_CONV_TYPE_IM) {
    buds = purple_find_buddies(( *(gtkconv -> active_conv)).account,( *(gtkconv -> active_conv)).name);
    if (buds == ((GSList *)((void *)0))) {
/* The user isn't on the buddy list. So we don't create any sendto menu. */
    }
    else {
      GList *list = (GList *)((void *)0);
      GList *iter;
      for (l = buds; l != ((GSList *)((void *)0)); l = (l -> next)) {
        PurpleBlistNode *node;
        node = ((PurpleBlistNode *)(purple_buddy_get_contact(((PurpleBuddy *)(l -> data)))));
        for (node = (node -> child); node != ((PurpleBlistNode *)((void *)0)); node = (node -> next)) {{
            PurpleBuddy *buddy = (PurpleBuddy *)node;
            PurpleAccount *account;
            if (!((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)) 
              continue; 
            account = purple_buddy_get_account(buddy);
            if ((purple_account_is_connected(account) != 0) || (account == ( *(gtkconv -> active_conv)).account)) {
/* Use the PurplePresence to get unique buddies. */
              PurplePresence *presence = purple_buddy_get_presence(buddy);
              if (!(g_list_find_custom(list,presence,((GCompareFunc )compare_buddy_presence)) != 0)) 
                list = g_list_prepend(list,presence);
            }
          }
        }
      }
/* Create the sendto menu only if it has more than one item to show */
      if ((list != 0) && ((list -> next) != 0)) {
/* Loop over the list backwards so we get the items in the right order,
				 * since we did a g_list_prepend() earlier. */
        for (iter = g_list_last(list); iter != ((GList *)((void *)0)); iter = (iter -> prev)) {
          PurplePresence *pre = (iter -> data);
          PurpleBuddy *buddy = purple_presence_get_buddy(pre);
          create_sendto_item(menu,sg,&group,buddy,purple_buddy_get_account(buddy),purple_buddy_get_name(buddy));
        }
      }
      g_list_free(list);
      g_slist_free(buds);
    }
  }
  g_object_unref(sg);
  gtk_widget_show(win -> menu.send_to);
/* TODO: This should never be insensitive.  Possibly hidden or not. */
  if (!(group != 0)) 
    gtk_widget_set_sensitive(win -> menu.send_to,0);
  update_send_to_selection(win);
}

static const char *get_chat_buddy_status_icon(PurpleConvChat *chat,const char *name,PurpleConvChatBuddyFlags flags)
{
  const char *image = (const char *)((void *)0);
  if ((flags & PURPLE_CBFLAGS_FOUNDER) != 0U) {
    image = "pidgin-status-founder";
  }
  else if ((flags & PURPLE_CBFLAGS_OP) != 0U) {
    image = "pidgin-status-operator";
  }
  else if ((flags & PURPLE_CBFLAGS_HALFOP) != 0U) {
    image = "pidgin-status-halfop";
  }
  else if ((flags & PURPLE_CBFLAGS_VOICE) != 0U) {
    image = "pidgin-status-voice";
  }
  else if (!(flags != 0U) && (purple_conv_chat_is_user_ignored(chat,name) != 0)) {
    image = "pidgin-status-ignored";
  }
  else {
    return 0;
  }
  return image;
}

static void deleting_chat_buddy_cb(PurpleConvChatBuddy *cb)
{
  if ((cb -> ui_data) != 0) {
    GtkTreeRowReference *ref = (cb -> ui_data);
    gtk_tree_row_reference_free(ref);
    cb -> ui_data = ((gpointer )((void *)0));
  }
}

static void add_chat_buddy_common(PurpleConversation *conv,PurpleConvChatBuddy *cb,const char *old_name)
{
  PidginConversation *gtkconv;
  PidginChatPane *gtkchat;
  PurpleConvChat *chat;
  PurpleConnection *gc;
  PurplePluginProtocolInfo *prpl_info;
  GtkTreeModel *tm;
  GtkListStore *ls;
  GtkTreePath *newpath;
  const char *stock;
  GtkTreeIter iter;
  gboolean is_me = 0;
  gboolean is_buddy;
  gchar *tmp;
  gchar *alias_key;
  gchar *name;
  gchar *alias;
  PurpleConvChatBuddyFlags flags;
  GdkColor *color = (GdkColor *)((void *)0);
  alias = (cb -> alias);
  name = (cb -> name);
  flags = (cb -> flags);
  chat = purple_conversation_get_chat_data(conv);
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  gtkchat = gtkconv -> u.chat;
  gc = purple_conversation_get_gc(conv);
  if (!(gc != 0) || !((prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info)) != 0)) 
    return ;
  tm = gtk_tree_view_get_model(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> list)),gtk_tree_view_get_type()))));
  ls = ((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)tm),gtk_list_store_get_type())));
  stock = get_chat_buddy_status_icon(chat,name,flags);
  if (!(strcmp((chat -> nick),purple_normalize((conv -> account),((old_name != ((const char *)((void *)0)))?old_name : name))) != 0)) 
    is_me = (!0);
  is_buddy = (cb -> buddy);
  tmp = g_utf8_casefold(alias,(-1));
  alias_key = g_utf8_collate_key(tmp,(-1));
  g_free(tmp);
  if (is_me != 0) {
    GtkTextTag *tag = gtk_text_tag_table_lookup(gtk_text_buffer_get_tag_table(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type())))).text_buffer),"send-name");
    g_object_get(tag,"foreground-gdk",&color,((void *)((void *)0)));
  }
  else {
    GtkTextTag *tag;
    if ((tag = get_buddy_tag(conv,name,0,0)) != 0) 
      g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))),"style",PANGO_STYLE_NORMAL,((void *)((void *)0)));
    if ((tag = get_buddy_tag(conv,name,PURPLE_MESSAGE_NICK,0)) != 0) 
      g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))),"style",PANGO_STYLE_NORMAL,((void *)((void *)0)));
    color = ((GdkColor *)(get_nick_color(gtkconv,name)));
  }
  gtk_list_store_insert_with_values(ls,&iter,(-1),CHAT_USERS_ICON_STOCK_COLUMN,stock,CHAT_USERS_ALIAS_COLUMN,alias,CHAT_USERS_ALIAS_KEY_COLUMN,alias_key,CHAT_USERS_NAME_COLUMN,name,CHAT_USERS_FLAGS_COLUMN,flags,CHAT_USERS_COLOR_COLUMN,color,CHAT_USERS_WEIGHT_COLUMN,((is_buddy != 0)?PANGO_WEIGHT_BOLD : PANGO_WEIGHT_NORMAL),-1);
/*
* The GTK docs are mute about the effects of the "row" value for performance.
* X-Chat hardcodes their value to 0 (prepend) and -1 (append), so we will too.
* It *might* be faster to search the gtk_list_store and set row accurately,
* but no one in #gtk+ seems to know anything about it either.
* Inserting in the "wrong" location has no visible ill effects. - F.P.
*/
/* "row" */
  if ((cb -> ui_data) != 0) {
    GtkTreeRowReference *ref = (cb -> ui_data);
    gtk_tree_row_reference_free(ref);
  }
  newpath = gtk_tree_model_get_path(tm,&iter);
  cb -> ui_data = (gtk_tree_row_reference_new(tm,newpath));
  gtk_tree_path_free(newpath);
  if ((is_me != 0) && (color != 0)) 
    gdk_color_free(color);
  g_free(alias_key);
}
/**
 * @param most_matched Used internally by this function.
 * @param entered The partial string that the user types before hitting the
 *        tab key.
 * @param entered_bytes The length of entered.
 * @param partial This is a return variable.  This will be set to a string
 *        containing the largest common string between all matches.  This will
 *        be inserted into the input box at the start of the word that the
 *        user is tab completing.  For example, if a chat room contains
 *        "AlfFan" and "AlfHater" and the user types "a<TAB>" then this will
 *        contain "Alf"
 * @param nick_partial Used internally by this function.  Shoudl be a
 *        temporary buffer that is entered_bytes+1 bytes long.
 * @param matches This is a return variable.  If the given name is a potential
 *        match for the entered string, then add a copy of the name to this
 *        list.  The caller is responsible for g_free'ing the data in this
 *        list.
 * @param name The buddy name or alias or slash command name that we're
 *        checking for a match.
 */

static void tab_complete_process_item(int *most_matched,const char *entered,gsize entered_bytes,char **partial,char *nick_partial,GList **matches,char *name)
{
  memcpy(nick_partial,name,entered_bytes);
  if (purple_utf8_strcasecmp(nick_partial,entered) != 0) 
    return ;
/* if we're here, it's a possible completion */
  if ( *most_matched == -1) {
/*
		 * this will only get called once, since from now
		 * on *most_matched is >= 0
		 */
     *most_matched = (strlen(name));
     *partial = g_strdup(name);
  }
  else if ( *most_matched != 0) {
    char *tmp = g_strdup(name);
    while(purple_utf8_strcasecmp(tmp,( *partial)) != 0){
      ( *partial)[ *most_matched] = 0;
      if (( *most_matched) < strlen(tmp)) 
        tmp[ *most_matched] = 0;
      ( *most_matched)--;
    }
    ( *most_matched)++;
    g_free(tmp);
  }
   *matches = g_list_insert_sorted( *matches,(g_strdup(name)),((GCompareFunc )purple_utf8_strcasecmp));
}

static gboolean tab_complete(PurpleConversation *conv)
{
  PidginConversation *gtkconv;
  GtkTextIter cursor;
  GtkTextIter word_start;
  GtkTextIter start_buffer;
  int start;
  int most_matched = -1;
  char *entered;
  char *partial = (char *)((void *)0);
  char *text;
  char *nick_partial;
  const char *prefix;
  GList *matches = (GList *)((void *)0);
  gboolean command = 0;
  gsize entered_bytes = 0;
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  gtk_text_buffer_get_start_iter((gtkconv -> entry_buffer),&start_buffer);
  gtk_text_buffer_get_iter_at_mark((gtkconv -> entry_buffer),&cursor,gtk_text_buffer_get_insert((gtkconv -> entry_buffer)));
  word_start = cursor;
/* if there's nothing there just return */
  if (!(gtk_text_iter_compare((&cursor),(&start_buffer)) != 0)) 
    return ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT)?!0 : 0;
  text = gtk_text_buffer_get_text((gtkconv -> entry_buffer),(&start_buffer),(&cursor),0);
/* if we're at the end of ": " we need to move back 2 spaces */
  start = (strlen(text) - 1);
  if ((start >= 1) && !(strncmp((text + (start - 1)),": ",2) != 0)) {
    gtk_text_iter_backward_chars(&word_start,2);
  }
{
/* find the start of the word that we're tabbing.
	 * Using gtk_text_iter_backward_word_start won't work, because a nick can contain
	 * characters (e.g. '.', '/' etc.) that Pango may think are word separators. */
    while(gtk_text_iter_backward_char(&word_start) != 0){
      if (gtk_text_iter_get_char((&word_start)) == 32) {
/* Reached the whitespace before the start of the word. Move forward once */
        gtk_text_iter_forward_char(&word_start);
        break; 
      }
    }
  }
  prefix = pidgin_get_cmd_prefix();
  if (((gtk_text_iter_get_offset((&word_start)) == 0) && (strlen(text) >= strlen(prefix))) && !(strncmp(text,prefix,strlen(prefix)) != 0)) {
    command = (!0);
    gtk_text_iter_forward_chars(&word_start,(strlen(prefix)));
  }
  g_free(text);
  entered = gtk_text_buffer_get_text((gtkconv -> entry_buffer),(&word_start),(&cursor),0);
  entered_bytes = strlen(entered);
  if (!(g_utf8_strlen(entered,(-1)) != 0L)) {
    g_free(entered);
    return ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT)?!0 : 0;
  }
  nick_partial = (g_malloc0((entered_bytes + 1)));
  if (command != 0) {
    GList *list = purple_cmd_list(conv);
    GList *l;
/* Commands */
    for (l = list; l != ((GList *)((void *)0)); l = (l -> next)) {
      tab_complete_process_item(&most_matched,entered,entered_bytes,&partial,nick_partial,&matches,(l -> data));
    }
    g_list_free(list);
  }
  else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) {
    PurpleConvChat *chat = purple_conversation_get_chat_data(conv);
    GList *l = purple_conv_chat_get_users(chat);
    GtkTreeModel *model = gtk_tree_view_get_model(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)( *( *((PidginConversation *)(conv -> ui_data))).u.chat).list),gtk_tree_view_get_type()))));
    GtkTreeIter iter;
    int f;
/* Users */
    for (; l != ((GList *)((void *)0)); l = (l -> next)) {
      tab_complete_process_item(&most_matched,entered,entered_bytes,&partial,nick_partial,&matches,( *((PurpleConvChatBuddy *)(l -> data))).name);
    }
/* Aliases */
    if (gtk_tree_model_get_iter_first(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter) != 0) {
      do {
        char *name;
        char *alias;
        gtk_tree_model_get(model,&iter,CHAT_USERS_NAME_COLUMN,&name,CHAT_USERS_ALIAS_COLUMN,&alias,-1);
        if (((name != 0) && (alias != 0)) && (strcmp(name,alias) != 0)) 
          tab_complete_process_item(&most_matched,entered,entered_bytes,&partial,nick_partial,&matches,alias);
        g_free(name);
        g_free(alias);
        f = gtk_tree_model_iter_next(model,&iter);
      }while (f != 0);
    }
  }
  else {
    g_free(nick_partial);
    g_free(entered);
    return 0;
  }
  g_free(nick_partial);
/* we're only here if we're doing new style */
/* if there weren't any matches, return */
  if (!(matches != 0)) {
/* if matches isn't set partials won't be either */
    g_free(entered);
    return ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT)?!0 : 0;
  }
  gtk_text_buffer_delete((gtkconv -> entry_buffer),&word_start,&cursor);
  if (!((matches -> next) != 0)) {
/* there was only one match. fill it in. */
    gtk_text_buffer_get_start_iter((gtkconv -> entry_buffer),&start_buffer);
    gtk_text_buffer_get_iter_at_mark((gtkconv -> entry_buffer),&cursor,gtk_text_buffer_get_insert((gtkconv -> entry_buffer)));
    if (!(gtk_text_iter_compare((&cursor),(&start_buffer)) != 0)) {
      char *tmp = g_strdup_printf("%s: ",((char *)(matches -> data)));
      gtk_text_buffer_insert_at_cursor((gtkconv -> entry_buffer),tmp,(-1));
      g_free(tmp);
    }
    else 
      gtk_text_buffer_insert_at_cursor((gtkconv -> entry_buffer),(matches -> data),(-1));
    g_free((matches -> data));
    g_list_free(matches);
  }
  else {
/*
		 * there were lots of matches, fill in as much as possible
		 * and display all of them
		 */
    char *addthis = (g_malloc0(1));
    while(matches != 0){
      char *tmp = addthis;
      addthis = g_strconcat(tmp,(matches -> data)," ",((void *)((void *)0)));
      g_free(tmp);
      g_free((matches -> data));
      matches = g_list_remove(matches,(matches -> data));
    }
    purple_conversation_write(conv,0,addthis,PURPLE_MESSAGE_NO_LOG,time(0));
    gtk_text_buffer_insert_at_cursor((gtkconv -> entry_buffer),partial,(-1));
    g_free(addthis);
  }
  g_free(entered);
  g_free(partial);
  return (!0);
}

static void topic_callback(GtkWidget *w,PidginConversation *gtkconv)
{
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurpleConnection *gc;
  PurpleConversation *conv = (gtkconv -> active_conv);
  PidginChatPane *gtkchat;
  char *new_topic;
  const char *current_topic;
  gc = purple_conversation_get_gc(conv);
  if (!(gc != 0) || !((prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info)) != 0)) 
    return ;
  if ((prpl_info -> set_chat_topic) == ((void (*)(PurpleConnection *, int , const char *))((void *)0))) 
    return ;
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  gtkchat = gtkconv -> u.chat;
  new_topic = g_strdup(gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> topic_text)),gtk_entry_get_type())))));
  current_topic = purple_conv_chat_get_topic((purple_conversation_get_chat_data(conv)));
  if ((current_topic != 0) && !(g_utf8_collate(new_topic,current_topic) != 0)) {
    g_free(new_topic);
    return ;
  }
  if (current_topic != 0) 
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> topic_text)),gtk_entry_get_type()))),current_topic);
  else 
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> topic_text)),gtk_entry_get_type()))),"");
  ( *(prpl_info -> set_chat_topic))(gc,purple_conv_chat_get_id((purple_conversation_get_chat_data(conv))),new_topic);
  g_free(new_topic);
}

static gint sort_chat_users(GtkTreeModel *model,GtkTreeIter *a,GtkTreeIter *b,gpointer userdata)
{
  PurpleConvChatBuddyFlags f1 = 0;
  PurpleConvChatBuddyFlags f2 = 0;
  char *user1 = (char *)((void *)0);
  char *user2 = (char *)((void *)0);
  gboolean buddy1 = 0;
  gboolean buddy2 = 0;
  gint ret = 0;
  gtk_tree_model_get(model,a,CHAT_USERS_ALIAS_KEY_COLUMN,&user1,CHAT_USERS_FLAGS_COLUMN,&f1,CHAT_USERS_WEIGHT_COLUMN,&buddy1,-1);
  gtk_tree_model_get(model,b,CHAT_USERS_ALIAS_KEY_COLUMN,&user2,CHAT_USERS_FLAGS_COLUMN,&f2,CHAT_USERS_WEIGHT_COLUMN,&buddy2,-1);
/* Only sort by membership levels */
  f1 &= (PURPLE_CBFLAGS_VOICE | PURPLE_CBFLAGS_HALFOP | PURPLE_CBFLAGS_OP | PURPLE_CBFLAGS_FOUNDER);
  f2 &= (PURPLE_CBFLAGS_VOICE | PURPLE_CBFLAGS_HALFOP | PURPLE_CBFLAGS_OP | PURPLE_CBFLAGS_FOUNDER);
  if ((user1 == ((char *)((void *)0))) || (user2 == ((char *)((void *)0)))) {
    if (!((user1 == ((char *)((void *)0))) && (user2 == ((char *)((void *)0))))) 
      ret = ((user1 == ((char *)((void *)0)))?-1 : 1);
  }
  else if (f1 != f2) {
/* sort more important users first */
    ret = ((f1 > f2)?-1 : 1);
  }
  else if (buddy1 != buddy2) {
    ret = ((buddy1 > buddy2)?-1 : 1);
  }
  else {
    ret = strcmp(user1,user2);
  }
  g_free(user1);
  g_free(user2);
  return ret;
}

static void update_chat_alias(PurpleBuddy *buddy,PurpleConversation *conv,PurpleConnection *gc,PurplePluginProtocolInfo *prpl_info)
{
  PidginConversation *gtkconv = (PidginConversation *)(conv -> ui_data);
  PurpleConvChat *chat = purple_conversation_get_chat_data(conv);
  GtkTreeModel *model;
  char *normalized_name;
  GtkTreeIter iter;
  int f;
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return ;
    };
  }while (0);
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
/* This is safe because this callback is only used in chats, not IMs. */
  model = gtk_tree_view_get_model(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)( *gtkconv -> u.chat).list),gtk_tree_view_get_type()))));
  if (!(gtk_tree_model_get_iter_first(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter) != 0)) 
    return ;
  normalized_name = g_strdup(purple_normalize((conv -> account),(buddy -> name)));
{
    do {
      char *name;
      gtk_tree_model_get(model,&iter,CHAT_USERS_NAME_COLUMN,&name,-1);
      if (!(strcmp(normalized_name,purple_normalize((conv -> account),name)) != 0)) {
        const char *alias = name;
        char *tmp;
        char *alias_key = (char *)((void *)0);
        PurpleBuddy *buddy2;
        if (strcmp((chat -> nick),purple_normalize((conv -> account),name)) != 0) {
/* This user is not me, so look into updating the alias. */
          if ((buddy2 = purple_find_buddy((conv -> account),name)) != ((PurpleBuddy *)((void *)0))) {
            alias = purple_buddy_get_contact_alias(buddy2);
          }
          tmp = g_utf8_casefold(alias,(-1));
          alias_key = g_utf8_collate_key(tmp,(-1));
          g_free(tmp);
          gtk_list_store_set(((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_list_store_get_type()))),&iter,CHAT_USERS_ALIAS_COLUMN,alias,CHAT_USERS_ALIAS_KEY_COLUMN,alias_key,-1);
          g_free(alias_key);
        }
        g_free(name);
        break; 
      }
      f = gtk_tree_model_iter_next(model,&iter);
      g_free(name);
    }while (f != 0);
  }
  g_free(normalized_name);
}

static void blist_node_aliased_cb(PurpleBlistNode *node,const char *old_alias,PurpleConversation *conv)
{
  PurpleConnection *gc;
  PurplePluginProtocolInfo *prpl_info;
  do {
    if (node != ((PurpleBlistNode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return ;
    };
  }while (0);
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  gc = purple_conversation_get_gc(conv);
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  do {
    if ((gc -> prpl) != ((PurplePlugin *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc->prpl != NULL");
      return ;
    };
  }while (0);
  prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info);
  if (((prpl_info -> options) & OPT_PROTO_UNIQUE_CHATNAME) != 0U) 
    return ;
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    PurpleBlistNode *bnode;
    for (bnode = (node -> child); bnode != 0; bnode = (bnode -> next)) {
      if (!((purple_blist_node_get_type(bnode)) == PURPLE_BLIST_BUDDY_NODE)) 
        continue; 
      update_chat_alias(((PurpleBuddy *)bnode),conv,gc,prpl_info);
    }
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) 
    update_chat_alias(((PurpleBuddy *)node),conv,gc,prpl_info);
  else if (((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) && (purple_conversation_get_account(conv) == ( *((PurpleChat *)node)).account)) {
    if ((old_alias == ((const char *)((void *)0))) || (g_utf8_collate(old_alias,purple_conversation_get_title(conv)) == 0)) 
      pidgin_conv_update_fields(conv,PIDGIN_CONV_SET_TITLE);
  }
}

static void buddy_cb_common(PurpleBuddy *buddy,PurpleConversation *conv,gboolean is_buddy)
{
  GtkTreeModel *model;
  char *normalized_name;
  GtkTreeIter iter;
  GtkTextTag *texttag;
  int f;
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return ;
    };
  }while (0);
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
/* Do nothing if the buddy does not belong to the conv's account */
  if (purple_buddy_get_account(buddy) != purple_conversation_get_account(conv)) 
    return ;
/* This is safe because this callback is only used in chats, not IMs. */
  model = gtk_tree_view_get_model(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)( *( *((PidginConversation *)(conv -> ui_data))).u.chat).list),gtk_tree_view_get_type()))));
  if (!(gtk_tree_model_get_iter_first(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter) != 0)) 
    return ;
  normalized_name = g_strdup(purple_normalize((conv -> account),(buddy -> name)));
{
    do {
      char *name;
      gtk_tree_model_get(model,&iter,CHAT_USERS_NAME_COLUMN,&name,-1);
      if (!(strcmp(normalized_name,purple_normalize((conv -> account),name)) != 0)) {
        gtk_list_store_set(((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_list_store_get_type()))),&iter,CHAT_USERS_WEIGHT_COLUMN,((is_buddy != 0)?PANGO_WEIGHT_BOLD : PANGO_WEIGHT_NORMAL),-1);
        g_free(name);
        break; 
      }
      f = gtk_tree_model_iter_next(model,&iter);
      g_free(name);
    }while (f != 0);
  }
  g_free(normalized_name);
  blist_node_aliased_cb(((PurpleBlistNode *)buddy),0,conv);
/* XXX: do we want the normalized name? */
  texttag = get_buddy_tag(conv,purple_buddy_get_name(buddy),0,0);
  if (texttag != 0) {
    g_object_set(texttag,"weight",((is_buddy != 0)?PANGO_WEIGHT_BOLD : PANGO_WEIGHT_NORMAL),((void *)((void *)0)));
  }
}

static void buddy_added_cb(PurpleBlistNode *node,PurpleConversation *conv)
{
  if (!((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)) 
    return ;
  buddy_cb_common(((PurpleBuddy *)node),conv,(!0));
}

static void buddy_removed_cb(PurpleBlistNode *node,PurpleConversation *conv)
{
  if (!((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)) 
    return ;
/* If there's another buddy for the same "dude" on the list, do nothing. */
  if (purple_find_buddy(purple_buddy_get_account(((PurpleBuddy *)node)),purple_buddy_get_name(((PurpleBuddy *)node))) != ((PurpleBuddy *)((void *)0))) 
    return ;
  buddy_cb_common(((PurpleBuddy *)node),conv,0);
}

static void send_menu_cb(GtkWidget *widget,PidginConversation *gtkconv)
{
  g_signal_emit_by_name((gtkconv -> entry),"message_send");
}

static void entry_popup_menu_cb(GtkIMHtml *imhtml,GtkMenu *menu,gpointer data)
{
  GtkWidget *menuitem;
  PidginConversation *gtkconv = data;
  do {
    if (menu != ((GtkMenu *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"menu != NULL");
      return ;
    };
  }while (0);
  do {
    if (gtkconv != ((PidginConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gtkconv != NULL");
      return ;
    };
  }while (0);
  menuitem = pidgin_new_item_from_stock(0,((const char *)(dgettext("pidgin","_Send"))),0,((GCallback )send_menu_cb),gtkconv,0,0,0);
  if (gtk_text_buffer_get_char_count((imhtml -> text_buffer)) == 0) 
    gtk_widget_set_sensitive(menuitem,0);
  gtk_menu_shell_insert(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem,0);
  menuitem = gtk_separator_menu_item_new();
  gtk_widget_show(menuitem);
  gtk_menu_shell_insert(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem,1);
}

static gboolean resize_imhtml_cb(PidginConversation *gtkconv)
{
  GtkTextBuffer *buffer;
  GtkTextIter iter;
  int lines;
  GdkRectangle oneline;
  int height;
  int diff;
  int pad_top;
  int pad_inside;
  int pad_bottom;
  int total_height = (( *(gtkconv -> imhtml)).allocation.height + ( *(gtkconv -> entry)).allocation.height);
  int max_height = (total_height / 2);
  int min_lines = purple_prefs_get_int("/pidgin/conversations/minimum_entry_lines");
  int min_height;
  gboolean interior_focus;
  int focus_width;
  pad_top = gtk_text_view_get_pixels_above_lines(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_text_view_get_type()))));
  pad_bottom = gtk_text_view_get_pixels_below_lines(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_text_view_get_type()))));
  pad_inside = gtk_text_view_get_pixels_inside_wrap(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_text_view_get_type()))));
  buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_text_view_get_type()))));
  gtk_text_buffer_get_start_iter(buffer,&iter);
  gtk_text_view_get_iter_location(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_text_view_get_type()))),(&iter),&oneline);
  lines = gtk_text_buffer_get_line_count(buffer);
  height = 0;
  do {
    int lineheight = 0;
    gtk_text_view_get_line_yrange(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_text_view_get_type()))),(&iter),0,&lineheight);
    height += lineheight;
    lines--;
  }while (gtk_text_iter_forward_line(&iter) != 0);
  height += (lines * ((oneline.height + pad_top) + pad_bottom));
/* Make sure there's enough room for at least min_lines. Allocate enough space to
	 * prevent scrolling when the second line is a continuation of the first line, or
	 * is the beginning of a new paragraph. */
  min_height = (min_lines * (oneline.height + (((pad_inside > (pad_top + pad_bottom))?pad_inside : (pad_top + pad_bottom)))));
  height = ((height > max_height)?max_height : (((height < (((min_height < max_height)?min_height : max_height)))?(((min_height < max_height)?min_height : max_height)) : height)));
  gtk_widget_style_get((gtkconv -> entry),"interior-focus",&interior_focus,"focus-line-width",&focus_width,((void *)((void *)0)));
  if (!(interior_focus != 0)) 
    height += (2 * focus_width);
  diff = (height - ( *(gtkconv -> entry)).allocation.height);
  if ((((diff < 0)?-diff : diff)) < (oneline.height / 2)) 
    return 0;
  gtk_widget_set_size_request((gtkconv -> lower_hbox),(-1),(diff + ( *(gtkconv -> lower_hbox)).allocation.height));
  return 0;
}

static void minimum_entry_lines_pref_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  GList *l = purple_get_conversations();
  PurpleConversation *conv;
  while(l != ((GList *)((void *)0))){
    conv = ((PurpleConversation *)(l -> data));
    if (purple_conversation_get_ui_ops(conv) == pidgin_conversations_get_conv_ui_ops()) 
      resize_imhtml_cb(((PidginConversation *)(conv -> ui_data)));
    l = (l -> next);
  }
}

static void setup_chat_topic(PidginConversation *gtkconv,GtkWidget *vbox)
{
  PurpleConversation *conv = (gtkconv -> active_conv);
  PurpleConnection *gc = purple_conversation_get_gc(conv);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info;
  if (((prpl_info -> options) & OPT_PROTO_CHAT_TOPIC) != 0U) {
    GtkWidget *hbox;
    GtkWidget *label;
    PidginChatPane *gtkchat = gtkconv -> u.chat;
    hbox = gtk_hbox_new(0,6);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
    label = gtk_label_new(((const char *)(dgettext("pidgin","Topic:"))));
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
    gtkchat -> topic_text = gtk_entry_new();
    gtk_widget_set_size_request((gtkchat -> topic_text),(-1),32);
    if ((prpl_info -> set_chat_topic) == ((void (*)(PurpleConnection *, int , const char *))((void *)0))) {
      gtk_editable_set_editable(((GtkEditable *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> topic_text)),gtk_editable_get_type()))),0);
    }
    else {
      g_signal_connect_data(((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> topic_text)),gtk_object_get_type()))),"activate",((GCallback )topic_callback),gtkconv,0,((GConnectFlags )0));
    }
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),(gtkchat -> topic_text),(!0),(!0),0);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> topic_text)),((GType )(20 << 2))))),"key_press_event",((GCallback )entry_key_press_cb),gtkconv,0,((GConnectFlags )0));
  }
}

static gboolean pidgin_conv_userlist_create_tooltip(GtkWidget *tipwindow,GtkTreePath *path,gpointer userdata,int *w,int *h)
{
  PidginConversation *gtkconv = userdata;
  GtkTreeIter iter;
  GtkTreeModel *model = gtk_tree_view_get_model(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)( *gtkconv -> u.chat).list),gtk_tree_view_get_type()))));
  PurpleConversation *conv = (gtkconv -> active_conv);
  PurpleBlistNode *node;
  PurplePluginProtocolInfo *prpl_info;
  PurpleAccount *account = purple_conversation_get_account(conv);
  char *who = (char *)((void *)0);
  if ((account -> gc) == ((PurpleConnection *)((void *)0))) 
    return 0;
  if (!(gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,path) != 0)) 
    return 0;
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,CHAT_USERS_NAME_COLUMN,&who,-1);
  prpl_info = ((PurplePluginProtocolInfo *)( *( *( *(account -> gc)).prpl).info).extra_info);
  node = ((PurpleBlistNode *)(purple_find_buddy((conv -> account),who)));
  if (((node != 0) && (prpl_info != 0)) && (((prpl_info -> options) & OPT_PROTO_UNIQUE_CHATNAME) != 0U)) 
    pidgin_blist_draw_tooltip(node,(gtkconv -> infopane));
  g_free(who);
  return 0;
}

static void setup_chat_userlist(PidginConversation *gtkconv,GtkWidget *hpaned)
{
  PidginChatPane *gtkchat = gtkconv -> u.chat;
  GtkWidget *lbox;
  GtkWidget *list;
  GtkListStore *ls;
  GtkCellRenderer *rend;
  GtkTreeViewColumn *col;
  int ul_width;
  void *blist_handle = purple_blist_get_handle();
  PurpleConversation *conv = (gtkconv -> active_conv);
/* Build the right pane. */
  lbox = gtk_vbox_new(0,6);
  gtk_paned_pack2(((GtkPaned *)(g_type_check_instance_cast(((GTypeInstance *)hpaned),gtk_paned_get_type()))),lbox,0,(!0));
  gtk_widget_show(lbox);
/* Setup the label telling how many people are in the room. */
  gtkchat -> count = gtk_label_new(((const char *)(dgettext("pidgin","0 people in room"))));
  gtk_label_set_ellipsize(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> count)),gtk_label_get_type()))),PANGO_ELLIPSIZE_END);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)lbox),gtk_box_get_type()))),(gtkchat -> count),0,0,0);
  gtk_widget_show((gtkchat -> count));
/* Setup the list of users. */
  ls = gtk_list_store_new(CHAT_USERS_COLUMNS,gdk_pixbuf_get_type(),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(6 << 2)),gdk_color_get_type(),((GType )(6 << 2)),((GType )(16 << 2)));
  gtk_tree_sortable_set_sort_func(((GtkTreeSortable *)(g_type_check_instance_cast(((GTypeInstance *)ls),gtk_tree_sortable_get_type()))),CHAT_USERS_ALIAS_KEY_COLUMN,sort_chat_users,0,0);
  list = gtk_tree_view_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)ls),gtk_tree_model_get_type()))));
/* Allow a user to specify gtkrc settings for the chat userlist only */
  gtk_widget_set_name(list,"pidgin_conv_userlist");
  rend = gtk_cell_renderer_pixbuf_new();
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)rend),((GType )(20 << 2))))),"stock-size",gtk_icon_size_from_name("pidgin-icon-size-tango-extra-small"),((void *)((void *)0)));
  col = gtk_tree_view_column_new_with_attributes(0,rend,"stock-id",CHAT_USERS_ICON_STOCK_COLUMN,((void *)((void *)0)));
  gtk_tree_view_column_set_sizing(col,GTK_TREE_VIEW_COLUMN_AUTOSIZE);
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)list),gtk_tree_view_get_type()))),col);
  ul_width = purple_prefs_get_int("/pidgin/conversations/chat/userlist_width");
  gtk_widget_set_size_request(lbox,ul_width,(-1));
/* Hack to prevent completely collapsed userlist coming back with a 1 pixel width.
	 * I would have liked to use the GtkPaned "max-position", but for some reason that didn't work */
  if (ul_width == 0) 
    gtk_paned_set_position(((GtkPaned *)(g_type_check_instance_cast(((GTypeInstance *)hpaned),gtk_paned_get_type()))),999999);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)list),((GType )(20 << 2))))),"button_press_event",((GCallback )right_click_chat_cb),gtkconv,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)list),((GType )(20 << 2))))),"row-activated",((GCallback )activate_list_cb),gtkconv,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)list),((GType )(20 << 2))))),"popup-menu",((GCallback )gtkconv_chat_popup_menu_cb),gtkconv,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)lbox),((GType )(20 << 2))))),"size-allocate",((GCallback )lbox_size_allocate_cb),gtkconv,0,((GConnectFlags )0));
  pidgin_tooltip_setup_for_treeview(list,gtkconv,pidgin_conv_userlist_create_tooltip,0);
  rend = gtk_cell_renderer_text_new();
  g_object_set(rend,"foreground-set",!0,"weight-set",!0,((void *)((void *)0)));
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)rend),((GType )(20 << 2))))),"editable",!0,((void *)((void *)0)));
  col = gtk_tree_view_column_new_with_attributes(0,rend,"text",CHAT_USERS_ALIAS_COLUMN,"foreground-gdk",CHAT_USERS_COLOR_COLUMN,"weight",CHAT_USERS_WEIGHT_COLUMN,((void *)((void *)0)));
  purple_signal_connect(blist_handle,"blist-node-added",gtkchat,((PurpleCallback )buddy_added_cb),conv);
  purple_signal_connect(blist_handle,"blist-node-removed",gtkchat,((PurpleCallback )buddy_removed_cb),conv);
  purple_signal_connect(blist_handle,"blist-node-aliased",gtkchat,((PurpleCallback )blist_node_aliased_cb),conv);
  gtk_tree_view_column_set_expand(col,(!0));
  g_object_set(rend,"ellipsize",PANGO_ELLIPSIZE_END,((void *)((void *)0)));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)list),gtk_tree_view_get_type()))),col);
  gtk_tree_view_set_headers_visible(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)list),gtk_tree_view_get_type()))),0);
  gtk_widget_show(list);
  gtkchat -> list = list;
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)lbox),gtk_box_get_type()))),pidgin_make_scrollable(list,GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC,GTK_SHADOW_IN,-1,-1),(!0),(!0),0);
}

static gboolean pidgin_conv_create_tooltip(GtkWidget *tipwindow,gpointer userdata,int *w,int *h)
{
  PurpleBlistNode *node = (PurpleBlistNode *)((void *)0);
  PurpleConversation *conv;
  PidginConversation *gtkconv = userdata;
  conv = (gtkconv -> active_conv);
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) {
    node = ((PurpleBlistNode *)(purple_blist_find_chat((conv -> account),(conv -> name))));
    if (!(node != 0)) 
      node = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),((GType )(20 << 2))))),"transient_chat"));
  }
  else {
    node = ((PurpleBlistNode *)(purple_find_buddy((conv -> account),(conv -> name))));
#if 0
/* Using the transient blist nodes to show the tooltip doesn't quite work yet. */
#endif
  }
  if (node != 0) 
    pidgin_blist_draw_tooltip(node,(gtkconv -> infopane));
  return 0;
}
/* Quick Find {{{ */

static gboolean pidgin_conv_end_quickfind(PidginConversation *gtkconv)
{
  gtk_widget_modify_base(gtkconv -> quickfind.entry,GTK_STATE_NORMAL,0);
  gtk_imhtml_search_clear(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))));
  gtk_widget_hide_all(gtkconv -> quickfind.container);
  gtk_widget_grab_focus((gtkconv -> entry));
  return (!0);
}

static gboolean quickfind_process_input(GtkWidget *entry,GdkEventKey *event,PidginConversation *gtkconv)
{
  switch(event -> keyval){
    case 0xff0d:
{
    }
    case 0xff8d:
{
      if (gtk_imhtml_search_find(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))))) != 0) {
        gtk_widget_modify_base(gtkconv -> quickfind.entry,GTK_STATE_NORMAL,0);
      }
      else {
        GdkColor col;
        col.red = 0xffff;
        col.green = 0xafff;
        col.blue = 0xafff;
        gtk_widget_modify_base(gtkconv -> quickfind.entry,GTK_STATE_NORMAL,(&col));
      }
      break; 
    }
    case 0xff1b:
{
      pidgin_conv_end_quickfind(gtkconv);
      break; 
    }
    default:
{
      return 0;
    }
  }
  return (!0);
}

static void pidgin_conv_setup_quickfind(PidginConversation *gtkconv,GtkWidget *container)
{
  GtkWidget *widget = gtk_hbox_new(0,0);
  GtkWidget *label;
  GtkWidget *entry;
  GtkWidget *close;
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)container),gtk_box_get_type()))),widget,0,0,0);
  close = pidgin_create_small_button(gtk_label_new("\303\227"));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_box_get_type()))),close,0,0,0);
  gtk_tooltips_set_tip((gtkconv -> tooltips),close,((const char *)(dgettext("pidgin","Close Find bar"))),0);
  label = gtk_label_new(((const char *)(dgettext("pidgin","Find:"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_box_get_type()))),label,0,0,10);
  entry = gtk_entry_new();
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_box_get_type()))),entry,(!0),(!0),0);
  gtkconv -> quickfind.entry = entry;
  gtkconv -> quickfind.container = widget;
/* Hook to signals and stuff */
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"key_press_event",((GCallback )quickfind_process_input),gtkconv,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)close),((GType )(20 << 2))))),"button-press-event",((GCallback )pidgin_conv_end_quickfind),gtkconv,0,G_CONNECT_SWAPPED);
}
/* }}} */

static GtkWidget *setup_common_pane(PidginConversation *gtkconv)
{
  GtkWidget *vbox;
  GtkWidget *frame;
  GtkWidget *imhtml_sw;
  GtkWidget *event_box;
  GtkCellRenderer *rend;
  GtkTreePath *path;
  PurpleConversation *conv = (gtkconv -> active_conv);
  PurpleBuddy *buddy;
  gboolean chat = ((conv -> type) == PURPLE_CONV_TYPE_CHAT);
  int buddyicon_size = 0;
/* Setup the top part of the pane */
  vbox = gtk_vbox_new(0,6);
  gtk_widget_show(vbox);
/* Setup the info pane */
  event_box = gtk_event_box_new();
  gtk_event_box_set_visible_window(((GtkEventBox *)(g_type_check_instance_cast(((GTypeInstance *)event_box),gtk_event_box_get_type()))),0);
  gtk_widget_show(event_box);
  gtkconv -> infopane_hbox = gtk_hbox_new(0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),event_box,0,0,0);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)event_box),gtk_container_get_type()))),(gtkconv -> infopane_hbox));
  gtk_widget_show((gtkconv -> infopane_hbox));
  gtk_widget_add_events(event_box,(GDK_POINTER_MOTION_MASK | GDK_LEAVE_NOTIFY_MASK));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)event_box),((GType )(20 << 2))))),"button-press-event",((GCallback )infopane_press_cb),gtkconv,0,((GConnectFlags )0));
  pidgin_tooltip_setup_for_widget(event_box,gtkconv,pidgin_conv_create_tooltip,0);
  gtkconv -> infopane = gtk_cell_view_new();
  gtkconv -> infopane_model = gtk_list_store_new(CONV_NUM_COLUMNS,((GType )(16 << 2)),((GType )(16 << 2)),gdk_pixbuf_get_type(),gdk_pixbuf_get_type());
  gtk_cell_view_set_model(((GtkCellView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> infopane)),gtk_cell_view_get_type()))),((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> infopane_model)),gtk_tree_model_get_type()))));
  g_object_unref((gtkconv -> infopane_model));
  gtk_list_store_append((gtkconv -> infopane_model),&gtkconv -> infopane_iter);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> infopane_hbox)),gtk_box_get_type()))),(gtkconv -> infopane),(!0),(!0),0);
  path = gtk_tree_path_new_from_string("0");
  gtk_cell_view_set_displayed_row(((GtkCellView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> infopane)),gtk_cell_view_get_type()))),path);
  gtk_tree_path_free(path);
  if (chat != 0) {
/* This empty widget is used to ensure that the infopane is consistently
		   sized for chat windows. The correct fix is to put an icon in the chat
		   window as well, because that would make "Set Custom Icon" consistent
		   for both the buddy list and the chat window, but PidginConversation
		   is pretty much stuck until 3.0. */
    GtkWidget *sizing_vbox;
    sizing_vbox = gtk_vbox_new(0,0);
    gtk_widget_set_size_request(sizing_vbox,(-1),32);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> infopane_hbox)),gtk_box_get_type()))),sizing_vbox,0,0,0);
    gtk_widget_show(sizing_vbox);
  }
  else {
    ( *gtkconv -> u.im).icon_container = gtk_vbox_new(0,0);
    if ((buddy = purple_find_buddy(purple_conversation_get_account(conv),purple_conversation_get_name(conv))) != ((PurpleBuddy *)((void *)0))) {
      PurpleContact *contact = purple_buddy_get_contact(buddy);
      if (contact != 0) {
        buddyicon_size = purple_blist_node_get_int(((PurpleBlistNode *)contact),"pidgin-infopane-iconsize");
      }
    }
    buddyicon_size = ((buddyicon_size > 96)?96 : (((buddyicon_size < 32)?32 : buddyicon_size)));
    gtk_widget_set_size_request(( *gtkconv -> u.im).icon_container,(-1),buddyicon_size);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> infopane_hbox)),gtk_box_get_type()))),( *gtkconv -> u.im).icon_container,0,0,0);
    gtk_widget_show(( *gtkconv -> u.im).icon_container);
  }
  gtk_widget_show((gtkconv -> infopane));
  rend = gtk_cell_renderer_pixbuf_new();
  gtk_cell_layout_pack_start(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> infopane)),gtk_cell_layout_get_type()))),rend,0);
  gtk_cell_layout_set_attributes(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> infopane)),gtk_cell_layout_get_type()))),rend,"stock-id",CONV_ICON_COLUMN,((void *)((void *)0)));
  g_object_set(rend,"xalign",0.0,"xpad",6,"ypad",0,"stock-size",gtk_icon_size_from_name("pidgin-icon-size-tango-extra-small"),((void *)((void *)0)));
  rend = gtk_cell_renderer_text_new();
  gtk_cell_layout_pack_start(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> infopane)),gtk_cell_layout_get_type()))),rend,(!0));
  gtk_cell_layout_set_attributes(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> infopane)),gtk_cell_layout_get_type()))),rend,"markup",CONV_TEXT_COLUMN,((void *)((void *)0)));
  g_object_set(rend,"ypad",0,"yalign",0.5,((void *)((void *)0)));
  g_object_set(rend,"ellipsize",PANGO_ELLIPSIZE_END,((void *)((void *)0)));
  rend = gtk_cell_renderer_pixbuf_new();
  gtk_cell_layout_pack_start(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> infopane)),gtk_cell_layout_get_type()))),rend,0);
  gtk_cell_layout_set_attributes(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> infopane)),gtk_cell_layout_get_type()))),rend,"pixbuf",CONV_PROTOCOL_ICON_COLUMN,((void *)((void *)0)));
  g_object_set(rend,"xalign",0.0,"xpad",3,"ypad",0,((void *)((void *)0)));
  rend = gtk_cell_renderer_pixbuf_new();
  gtk_cell_layout_pack_start(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> infopane)),gtk_cell_layout_get_type()))),rend,0);
  gtk_cell_layout_set_attributes(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> infopane)),gtk_cell_layout_get_type()))),rend,"pixbuf",CONV_EMBLEM_COLUMN,((void *)((void *)0)));
  g_object_set(rend,"xalign",0.0,"xpad",6,"ypad",0,((void *)((void *)0)));
/* Setup the gtkimhtml widget */
  frame = pidgin_create_imhtml(0,&gtkconv -> imhtml,0,&imhtml_sw);
  gtk_widget_set_size_request((gtkconv -> imhtml),(-1),0);
  if (chat != 0) {
    GtkWidget *hpaned;
/* Add the topic */
    setup_chat_topic(gtkconv,vbox);
/* Add the gtkimhtml frame */
    hpaned = gtk_hpaned_new();
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hpaned,(!0),(!0),0);
    gtk_widget_show(hpaned);
    gtk_paned_pack1(((GtkPaned *)(g_type_check_instance_cast(((GTypeInstance *)hpaned),gtk_paned_get_type()))),frame,(!0),(!0));
/* Now add the userlist */
    setup_chat_userlist(gtkconv,hpaned);
  }
  else {
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),frame,(!0),(!0),0);
  }
  gtk_widget_show(frame);
  gtk_widget_set_name((gtkconv -> imhtml),"pidgin_conv_imhtml");
  gtk_imhtml_show_comments(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),(!0));
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),((GType )(20 << 2))))),"gtkconv",gtkconv);
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml_sw),((GType )(20 << 2))))),"vscrollbar-policy",GTK_POLICY_ALWAYS,((void *)((void *)0)));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),((GType )(20 << 2))))),"button_press_event",((GCallback )entry_stop_rclick_cb),0,0,G_CONNECT_AFTER);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),((GType )(20 << 2))))),"key_press_event",((GCallback )refocus_entry_cb),gtkconv,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),((GType )(20 << 2))))),"key_release_event",((GCallback )refocus_entry_cb),gtkconv,0,((GConnectFlags )0));
  pidgin_conv_setup_quickfind(gtkconv,vbox);
  gtkconv -> lower_hbox = gtk_hbox_new(0,6);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),(gtkconv -> lower_hbox),0,0,0);
  gtk_widget_show((gtkconv -> lower_hbox));
/* Setup the toolbar, entry widget and all signals */
  frame = pidgin_create_imhtml((!0),&gtkconv -> entry,&gtkconv -> toolbar,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> lower_hbox)),gtk_box_get_type()))),frame,(!0),(!0),0);
  gtk_widget_show(frame);
  gtk_widget_set_name((gtkconv -> entry),"pidgin_conv_entry");
  gtk_imhtml_set_protocol_name(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type()))),purple_account_get_protocol_name((conv -> account)));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),((GType )(20 << 2))))),"populate-popup",((GCallback )entry_popup_menu_cb),gtkconv,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),((GType )(20 << 2))))),"key_press_event",((GCallback )entry_key_press_cb),gtkconv,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),((GType )(20 << 2))))),"message_send",((GCallback )send_cb),gtkconv,0,G_CONNECT_AFTER);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),((GType )(20 << 2))))),"button_press_event",((GCallback )entry_stop_rclick_cb),0,0,G_CONNECT_AFTER);
  gtkconv -> entry_buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_text_view_get_type()))));
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry_buffer)),((GType )(20 << 2))))),"user_data",gtkconv);
  if (!(chat != 0)) {
/* For sending typing notifications for IMs */
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry_buffer)),((GType )(20 << 2))))),"insert_text",((GCallback )insert_text_cb),gtkconv,0,((GConnectFlags )0));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry_buffer)),((GType )(20 << 2))))),"delete_range",((GCallback )delete_text_cb),gtkconv,0,((GConnectFlags )0));
    ( *gtkconv -> u.im).typing_timer = 0;
    ( *gtkconv -> u.im).animate = purple_prefs_get_bool("/pidgin/conversations/im/animate_buddy_icons");
    ( *gtkconv -> u.im).show_icon = (!0);
  }
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry_buffer)),((GType )(20 << 2))))),"changed",((GCallback )resize_imhtml_cb),gtkconv,0,G_CONNECT_SWAPPED);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),((GType )(20 << 2))))),"size-allocate",((GCallback )resize_imhtml_cb),gtkconv,0,G_CONNECT_SWAPPED);
  default_formatize(gtkconv);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),((GType )(20 << 2))))),"format_function_clear",((GCallback )clear_formatting_cb),gtkconv,0,G_CONNECT_AFTER);
  return vbox;
}

static void conv_dnd_recv(GtkWidget *widget,GdkDragContext *dc,guint x,guint y,GtkSelectionData *sd,guint info,guint t,PidginConversation *gtkconv)
{
  PurpleConversation *conv = (gtkconv -> active_conv);
  PidginWindow *win = (gtkconv -> win);
  PurpleConversation *c;
  PurpleAccount *convaccount = purple_conversation_get_account(conv);
  PurpleConnection *gc = purple_account_get_connection(convaccount);
  PurplePluginProtocolInfo *prpl_info = (gc != 0)?((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info) : ((struct _PurplePluginProtocolInfo *)((void *)0));
  if ((sd -> target) == gdk_atom_intern("PURPLE_BLIST_NODE",0)) {
    PurpleBlistNode *n = (PurpleBlistNode *)((void *)0);
    PurpleBuddy *b;
    PidginConversation *gtkconv = (PidginConversation *)((void *)0);
    PurpleAccount *buddyaccount;
    const char *buddyname;
    n =  *((PurpleBlistNode **)(sd -> data));
    if ((purple_blist_node_get_type(n)) == PURPLE_BLIST_CONTACT_NODE) 
      b = purple_contact_get_priority_buddy(((PurpleContact *)n));
    else if ((purple_blist_node_get_type(n)) == PURPLE_BLIST_BUDDY_NODE) 
      b = ((PurpleBuddy *)n);
    else 
      return ;
    buddyaccount = purple_buddy_get_account(b);
    buddyname = purple_buddy_get_name(b);
/*
		 * If a buddy is dragged to a chat window of the same protocol,
		 * invite him to the chat.
		 */
    if (((((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) && (prpl_info != 0)) && (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).chat_invite))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).chat_invite))) < (prpl_info -> struct_size))) && ((prpl_info -> chat_invite) != ((void (*)(PurpleConnection *, int , const char *, const char *))((void *)0))))) && (strcmp(purple_account_get_protocol_id(convaccount),purple_account_get_protocol_id(buddyaccount)) == 0)) {
      purple_conv_chat_invite_user(purple_conversation_get_chat_data(conv),buddyname,0,(!0));
    }
    else {
/*
			 * If we already have an open conversation with this buddy, then
			 * just move the conv to this window.  Otherwise, create a new
			 * conv and add it to this window.
			 */
      c = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,buddyname,buddyaccount);
      if (c != ((PurpleConversation *)((void *)0))) {
        PidginWindow *oldwin;
        gtkconv = ((PidginConversation *)(c -> ui_data));
        oldwin = (gtkconv -> win);
        if (oldwin != win) {
          pidgin_conv_window_remove_gtkconv(oldwin,gtkconv);
          pidgin_conv_window_add_gtkconv(win,gtkconv);
        }
      }
      else {
        c = purple_conversation_new(PURPLE_CONV_TYPE_IM,buddyaccount,buddyname);
        gtkconv = ((PidginConversation *)(c -> ui_data));
        if ((gtkconv -> win) != win) {
          pidgin_conv_window_remove_gtkconv((gtkconv -> win),gtkconv);
          pidgin_conv_window_add_gtkconv(win,gtkconv);
        }
      }
/* Make this conversation the active conversation */
      pidgin_conv_window_switch_gtkconv(win,gtkconv);
    }
    gtk_drag_finish(dc,(!0),((dc -> action) == GDK_ACTION_MOVE),t);
  }
  else if ((sd -> target) == gdk_atom_intern("application/x-im-contact",0)) {
    char *protocol = (char *)((void *)0);
    char *username = (char *)((void *)0);
    PurpleAccount *account;
    PidginConversation *gtkconv;
    if (pidgin_parse_x_im_contact(((const char *)(sd -> data)),0,&account,&protocol,&username,0) != 0) {
      if (account == ((PurpleAccount *)((void *)0))) {
        purple_notify_message(win,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","You are not currently signed on with an account that can add that buddy."))),0,0,0);
      }
      else {
/*
				 * If a buddy is dragged to a chat window of the same protocol,
				 * invite him to the chat.
				 */
        if (((((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) && (prpl_info != 0)) && (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).chat_invite))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).chat_invite))) < (prpl_info -> struct_size))) && ((prpl_info -> chat_invite) != ((void (*)(PurpleConnection *, int , const char *, const char *))((void *)0))))) && (strcmp(purple_account_get_protocol_id(convaccount),protocol) == 0)) {
          purple_conv_chat_invite_user(purple_conversation_get_chat_data(conv),username,0,(!0));
        }
        else {
          c = purple_conversation_new(PURPLE_CONV_TYPE_IM,account,username);
          gtkconv = ((PidginConversation *)(c -> ui_data));
          if ((gtkconv -> win) != win) {
            pidgin_conv_window_remove_gtkconv((gtkconv -> win),gtkconv);
            pidgin_conv_window_add_gtkconv(win,gtkconv);
          }
        }
      }
    }
    g_free(username);
    g_free(protocol);
    gtk_drag_finish(dc,(!0),((dc -> action) == GDK_ACTION_MOVE),t);
  }
  else if ((sd -> target) == gdk_atom_intern("text/uri-list",0)) {
    if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) 
      pidgin_dnd_file_manage(sd,convaccount,purple_conversation_get_name(conv));
    gtk_drag_finish(dc,(!0),((dc -> action) == GDK_ACTION_MOVE),t);
  }
  else 
    gtk_drag_finish(dc,0,0,t);
}
static const GtkTargetEntry te[] = {{("text/uri-list"), (0), (GTK_IMHTML_DRAG_URL)}, {("_NETSCAPE_URL"), (0), (GTK_IMHTML_DRAG_URL)}, {("text/html"), (0), (GTK_IMHTML_DRAG_HTML)}, {("x-url/ftp"), (0), (GTK_IMHTML_DRAG_URL)}, {("x-url/http"), (0), (GTK_IMHTML_DRAG_URL)}, {("UTF8_STRING"), (0), (GTK_IMHTML_DRAG_UTF8_STRING)}, {("COMPOUND_TEXT"), (0), (GTK_IMHTML_DRAG_COMPOUND_TEXT)}, {("STRING"), (0), (GTK_IMHTML_DRAG_STRING)}, {("text/plain"), (0), (GTK_IMHTML_DRAG_TEXT)}, {("TEXT"), (0), (GTK_IMHTML_DRAG_TEXT)}, {("PURPLE_BLIST_NODE"), (GTK_TARGET_SAME_APP), (GTK_IMHTML_DRAG_NUM)}, {("application/x-im-contact"), (0), ((GTK_IMHTML_DRAG_NUM + 1))}};

static PidginConversation *pidgin_conv_find_gtkconv(PurpleConversation *conv)
{
  PurpleBuddy *bud = purple_find_buddy((conv -> account),(conv -> name));
  PurpleContact *c;
  PurpleBlistNode *cn;
  PurpleBlistNode *bn;
  if (!(bud != 0)) 
    return 0;
  if (!((c = purple_buddy_get_contact(bud)) != 0)) 
    return 0;
  cn = ((PurpleBlistNode *)c);
  for (bn = purple_blist_node_get_first_child(cn); bn != 0; bn = purple_blist_node_get_sibling_next(bn)) {
    PurpleBuddy *b = (PurpleBuddy *)bn;
    PurpleConversation *conv;
    if ((conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,(b -> name),(b -> account))) != 0) {
      if ((conv -> ui_data) != 0) 
        return (conv -> ui_data);
    }
  }
  return 0;
}

static void buddy_update_cb(PurpleBlistNode *bnode,gpointer null)
{
  GList *list;
  do {
    if (bnode != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"bnode");
      return ;
    };
  }while (0);
  if (!((purple_blist_node_get_type(bnode)) == PURPLE_BLIST_BUDDY_NODE)) 
    return ;
  for (list = pidgin_conv_windows_get_list(); list != 0; list = (list -> next)) {{
      PidginWindow *win = (list -> data);
      PurpleConversation *conv = pidgin_conv_window_get_active_conversation(win);
      if ((purple_conversation_get_type(conv)) != PURPLE_CONV_TYPE_IM) 
        continue; 
      pidgin_conv_update_fields(conv,PIDGIN_CONV_MENU);
    }
  }
}

static gboolean ignore_middle_click(GtkWidget *widget,GdkEventButton *e,gpointer null)
{
/* A click on the pane is propagated to the notebook containing the pane.
	 * So if Stu accidentally aims high and middle clicks on the pane-handle,
	 * it causes a conversation tab to close. Let's stop that from happening.
	 */
  if (((e -> button) == 2) && ((e -> type) == GDK_BUTTON_PRESS)) 
    return (!0);
  return 0;
}

static void set_typing_font(GtkWidget *widget,GtkStyle *style,PidginConversation *gtkconv)
{
  static PangoFontDescription *font_desc = (PangoFontDescription *)((void *)0);
  static GdkColor *color = (GdkColor *)((void *)0);
  static gboolean enable = (!0);
  if (font_desc == ((PangoFontDescription *)((void *)0))) {
    char *string = (char *)((void *)0);
    gtk_widget_style_get(widget,"typing-notification-font",&string,"typing-notification-color",&color,"typing-notification-enable",&enable,((void *)((void *)0)));
    font_desc = pango_font_description_from_string(string);
    g_free(string);
    if (color == ((GdkColor *)((void *)0))) {
      GdkColor def = {(0), (34952), (34952), (34952)};
      color = gdk_color_copy((&def));
    }
  }
  gtk_text_buffer_create_tag(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_imhtml_get_type())))).text_buffer,"TYPING-NOTIFICATION","foreground-gdk",color,"font-desc",font_desc,((void *)((void *)0)));
  if (!(enable != 0)) {
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"disable-typing-notification",((gpointer )((gpointer )((glong )(!0)))));
/* or may be 'gtkconv->disable_typing = TRUE;' instead? */
  }
  g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,set_typing_font,gtkconv);
}
/**************************************************************************
 * Conversation UI operations
 **************************************************************************/

static void private_gtkconv_new(PurpleConversation *conv,gboolean hidden)
{
  PidginConversation *gtkconv;
  PurpleConversationType conv_type = purple_conversation_get_type(conv);
  GtkWidget *pane = (GtkWidget *)((void *)0);
  GtkWidget *tab_cont;
  PurpleBlistNode *convnode;
  PurpleValue *value;
  if ((conv_type == PURPLE_CONV_TYPE_IM) && ((gtkconv = pidgin_conv_find_gtkconv(conv)) != 0)) {
    conv -> ui_data = gtkconv;
    if (!(g_list_find((gtkconv -> convs),conv) != 0)) 
      gtkconv -> convs = g_list_prepend((gtkconv -> convs),conv);
    pidgin_conv_switch_active_conversation(conv);
    return ;
  }
  gtkconv = ((PidginConversation *)(g_malloc0_n(1,(sizeof(PidginConversation )))));
  conv -> ui_data = gtkconv;
  gtkconv -> active_conv = conv;
  gtkconv -> convs = g_list_prepend((gtkconv -> convs),conv);
  gtkconv -> send_history = g_list_append(0,0);
/* Setup some initial variables. */
  gtkconv -> tooltips = gtk_tooltips_new();
  gtkconv -> unseen_state = PIDGIN_UNSEEN_NONE;
  gtkconv -> unseen_count = 0;
  if (conv_type == PURPLE_CONV_TYPE_IM) {
    gtkconv -> u.im = (g_malloc0((sizeof(PidginImPane ))));
  }
  else if (conv_type == PURPLE_CONV_TYPE_CHAT) {
    gtkconv -> u.chat = (g_malloc0((sizeof(PidginChatPane ))));
  }
  pane = setup_common_pane(gtkconv);
  gtk_imhtml_set_format_functions(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),((gtk_imhtml_get_format_functions(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))))) | GTK_IMHTML_IMAGE));
  if (pane == ((GtkWidget *)((void *)0))) {
    if (conv_type == PURPLE_CONV_TYPE_CHAT) 
      g_free(gtkconv -> u.chat);
    else if (conv_type == PURPLE_CONV_TYPE_IM) 
      g_free(gtkconv -> u.im);
    g_free(gtkconv);
    conv -> ui_data = ((void *)((void *)0));
    return ;
  }
/* Setup drag-and-drop */
  gtk_drag_dest_set(pane,(GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_DROP),te,(sizeof(te) / sizeof(GtkTargetEntry )),GDK_ACTION_COPY);
  gtk_drag_dest_set(pane,(GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_DROP),te,(sizeof(te) / sizeof(GtkTargetEntry )),GDK_ACTION_COPY);
  gtk_drag_dest_set((gtkconv -> imhtml),0,te,(sizeof(te) / sizeof(GtkTargetEntry )),GDK_ACTION_COPY);
  gtk_drag_dest_set((gtkconv -> entry),0,te,(sizeof(te) / sizeof(GtkTargetEntry )),GDK_ACTION_COPY);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pane),((GType )(20 << 2))))),"button_press_event",((GCallback )ignore_middle_click),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pane),((GType )(20 << 2))))),"drag_data_received",((GCallback )conv_dnd_recv),gtkconv,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),((GType )(20 << 2))))),"drag_data_received",((GCallback )conv_dnd_recv),gtkconv,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),((GType )(20 << 2))))),"drag_data_received",((GCallback )conv_dnd_recv),gtkconv,0,((GConnectFlags )0));
  g_signal_connect_data((gtkconv -> imhtml),"style-set",((GCallback )set_typing_font),gtkconv,0,((GConnectFlags )0));
/* Setup the container for the tab. */
  gtkconv -> tab_cont = (tab_cont = gtk_vbox_new(0,6));
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tab_cont),((GType )(20 << 2))))),"PidginConversation",gtkconv);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)tab_cont),gtk_container_get_type()))),6);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)tab_cont),gtk_container_get_type()))),pane);
  gtk_widget_show(pane);
  convnode = get_conversation_blist_node(conv);
  if ((convnode == ((PurpleBlistNode *)((void *)0))) || !(purple_blist_node_get_bool(convnode,"gtk-mute-sound") != 0)) 
    gtkconv -> make_sound = (!0);
  if (((convnode != ((PurpleBlistNode *)((void *)0))) && ((value = (g_hash_table_lookup((convnode -> settings),"enable-logging"))) != 0)) && ((purple_value_get_type(value)) == PURPLE_TYPE_BOOLEAN)) {
    purple_conversation_set_logging(conv,purple_value_get_boolean(value));
  }
  if (purple_prefs_get_bool("/pidgin/conversations/show_formatting_toolbar") != 0) 
    gtk_widget_show((gtkconv -> toolbar));
  else 
    gtk_widget_hide((gtkconv -> toolbar));
  if (purple_prefs_get_bool("/pidgin/conversations/im/show_buddy_icons") != 0) 
    gtk_widget_show((gtkconv -> infopane_hbox));
  else 
    gtk_widget_hide((gtkconv -> infopane_hbox));
  gtk_imhtml_show_comments(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),purple_prefs_get_bool("/pidgin/conversations/show_timestamps"));
  gtk_imhtml_set_protocol_name(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),purple_account_get_protocol_name((conv -> account)));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pane),((GType )(20 << 2))))),"focus",((GCallback )gtk_widget_grab_focus),(gtkconv -> entry),0,G_CONNECT_SWAPPED);
  if (hidden != 0) 
    pidgin_conv_window_add_gtkconv(hidden_convwin,gtkconv);
  else 
    pidgin_conv_placement_place(gtkconv);
  if (nick_colors == ((GdkColor *)((void *)0))) {
    nbr_nick_colors = 220;
    nick_colors = generate_nick_colors(&nbr_nick_colors,( *gtk_widget_get_style((gtkconv -> imhtml))).base[GTK_STATE_NORMAL]);
  }
  if (((conv -> features) & PURPLE_CONNECTION_ALLOW_CUSTOM_SMILEY) != 0U) 
    pidgin_themes_smiley_themeize_custom((gtkconv -> entry));
}

static void pidgin_conv_new_hidden(PurpleConversation *conv)
{
  private_gtkconv_new(conv,(!0));
}

void pidgin_conv_new(PurpleConversation *conv)
{
  private_gtkconv_new(conv,0);
  if (purple_conversation_get_ui_ops(conv) == pidgin_conversations_get_conv_ui_ops()) 
    purple_signal_emit(pidgin_conversations_get_handle(),"conversation-displayed",((PidginConversation *)(conv -> ui_data)));
}

static void received_im_msg_cb(PurpleAccount *account,char *sender,char *message,PurpleConversation *conv,PurpleMessageFlags flags)
{
  PurpleConversationUiOps *ui_ops = pidgin_conversations_get_conv_ui_ops();
  gboolean hide = 0;
  guint timer;
/* create hidden conv if hide_new pref is always */
  if (strcmp(purple_prefs_get_string("/pidgin/conversations/im/hide_new"),"always") == 0) 
    hide = (!0);
/* create hidden conv if hide_new pref is away and account is away */
  if ((strcmp(purple_prefs_get_string("/pidgin/conversations/im/hide_new"),"away") == 0) && !(purple_status_is_available((purple_account_get_active_status(account))) != 0)) 
    hide = (!0);
  if (((conv != 0) && (purple_conversation_get_ui_ops(conv) == pidgin_conversations_get_conv_ui_ops())) && !(hide != 0)) {
    PidginConversation *gtkconv = (PidginConversation *)(conv -> ui_data);
    if ((gtkconv -> win) == hidden_convwin) {
      pidgin_conv_attach_to_conversation((gtkconv -> active_conv));
    }
    return ;
  }
  if (hide != 0) {
    ui_ops -> create_conversation = pidgin_conv_new_hidden;
    purple_conversation_new(PURPLE_CONV_TYPE_IM,account,sender);
    ui_ops -> create_conversation = pidgin_conv_new;
  }
/* Somebody wants to keep this conversation around, so don't time it out */
  if (conv != 0) {
    timer = ((gint )((glong )(purple_conversation_get_data(conv,"close-timer"))));
    if (timer != 0U) {
      purple_timeout_remove(timer);
      purple_conversation_set_data(conv,"close-timer",0);
    }
  }
}

static void pidgin_conv_destroy(PurpleConversation *conv)
{
  PidginConversation *gtkconv = (PidginConversation *)(conv -> ui_data);
  gtkconv -> convs = g_list_remove((gtkconv -> convs),conv);
/* Don't destroy ourselves until all our convos are gone */
  if ((gtkconv -> convs) != 0) {
/* Make sure the destroyed conversation is not the active one */
    if ((gtkconv -> active_conv) == conv) {
      gtkconv -> active_conv = ( *(gtkconv -> convs)).data;
      purple_conversation_update((gtkconv -> active_conv),PURPLE_CONV_UPDATE_FEATURES);
    }
    return ;
  }
  pidgin_conv_window_remove_gtkconv((gtkconv -> win),gtkconv);
/* If the "Save Conversation" or "Save Icon" dialogs are open then close them */
  purple_request_close_with_handle(gtkconv);
  purple_notify_close_with_handle(gtkconv);
  gtk_widget_destroy((gtkconv -> tab_cont));
  g_object_unref((gtkconv -> tab_cont));
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
    if (( *gtkconv -> u.im).icon_timer != 0) 
      g_source_remove(( *gtkconv -> u.im).icon_timer);
    if (( *gtkconv -> u.im).anim != ((GdkPixbufAnimation *)((void *)0))) 
      g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *gtkconv -> u.im).anim),((GType )(20 << 2))))));
    if (( *gtkconv -> u.im).typing_timer != 0) 
      g_source_remove(( *gtkconv -> u.im).typing_timer);
    g_free(gtkconv -> u.im);
  }
  else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) {
    purple_signals_disconnect_by_handle(gtkconv -> u.chat);
    g_free(gtkconv -> u.chat);
  }
  gtk_object_sink(((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tooltips)),gtk_object_get_type()))));
  gtkconv -> send_history = g_list_first((gtkconv -> send_history));
  g_list_foreach((gtkconv -> send_history),((GFunc )g_free),0);
  g_list_free((gtkconv -> send_history));
  if (gtkconv -> attach.timer != 0) {
    g_source_remove(gtkconv -> attach.timer);
  }
  g_free(gtkconv);
}

static void pidgin_conv_write_im(PurpleConversation *conv,const char *who,const char *message,PurpleMessageFlags flags,time_t mtime)
{
  PidginConversation *gtkconv;
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  if ((conv != (gtkconv -> active_conv)) && ((flags & PURPLE_MESSAGE_ACTIVE_ONLY) != 0U)) {
/* Plugins that want these messages suppressed should be
		 * calling purple_conv_im_write(), so they get suppressed here,
		 * before being written to the log. */
    purple_debug_info("gtkconv","Suppressing message for an inactive conversation in pidgin_conv_write_im()\n");
    return ;
  }
  purple_conversation_write(conv,who,message,flags,mtime);
}

static const char *get_text_tag_color(GtkTextTag *tag)
{
  GdkColor *color = (GdkColor *)((void *)0);
  gboolean set = 0;
  static char colcode[] = "#XXXXXX";
  if (tag != 0) 
    g_object_get(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))),"foreground-set",&set,"foreground-gdk",&color,((void *)((void *)0)));
  if ((set != 0) && (color != 0)) 
    g_snprintf(colcode,(sizeof(colcode)),"#%02x%02x%02x",((color -> red) >> 8),((color -> green) >> 8),((color -> blue) >> 8));
  else 
    colcode[0] = 0;
  if (color != 0) 
    gdk_color_free(color);
  return colcode;
}
/* The callback for an event on a link tag. */

static gboolean buddytag_event(GtkTextTag *tag,GObject *imhtml,GdkEvent *event,GtkTextIter *arg2,gpointer data)
{
  if (((event -> type) == GDK_BUTTON_PRESS) || ((event -> type) == GDK_2BUTTON_PRESS)) {
    GdkEventButton *btn_event = (GdkEventButton *)event;
    PurpleConversation *conv = data;
    char *buddyname;
/* strlen("BUDDY " or "HILIT ") == 6 */
    do {
      if (((tag -> name) != ((char *)((void *)0))) && (strlen((tag -> name)) > 6)) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"(tag->name != NULL) && (strlen(tag->name) > 6)");
        return 0;
      };
    }while (0);
    buddyname = ((tag -> name) + 6);
/* emit chat-nick-clicked signal */
    if ((event -> type) == GDK_BUTTON_PRESS) {
      gint plugin_return = (gint )((glong )(purple_signal_emit_return_1(pidgin_conversations_get_handle(),"chat-nick-clicked",data,buddyname,(btn_event -> button))));
      if (plugin_return != 0) 
        return (!0);
    }
    if (((btn_event -> button) == 1) && ((event -> type) == GDK_2BUTTON_PRESS)) {
      chat_do_im(((PidginConversation *)(conv -> ui_data)),buddyname);
      return (!0);
    }
    else if (((btn_event -> button) == 2) && ((event -> type) == GDK_2BUTTON_PRESS)) {
      chat_do_info(((PidginConversation *)(conv -> ui_data)),buddyname);
      return (!0);
    }
    else if (((btn_event -> button) == 3) && ((event -> type) == GDK_BUTTON_PRESS)) {
      GtkTextIter start;
      GtkTextIter end;
/* we shouldn't display the popup
			 * if the user has selected something: */
      if (!(gtk_text_buffer_get_selection_bounds(gtk_text_iter_get_buffer(arg2),&start,&end) != 0)) {
        GtkWidget *menu = (GtkWidget *)((void *)0);
        PurpleConnection *gc = purple_conversation_get_gc(conv);
        menu = create_chat_menu(conv,buddyname,gc);
        gtk_menu_popup(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_get_type()))),0,0,0,((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type()))),(btn_event -> button),(btn_event -> time));
/* Don't propagate the event any further */
        return (!0);
      }
    }
  }
  return 0;
}

static GtkTextTag *get_buddy_tag(PurpleConversation *conv,const char *who,PurpleMessageFlags flag,gboolean create)
{
  PidginConversation *gtkconv = (PidginConversation *)(conv -> ui_data);
  GtkTextTag *buddytag;
  gchar *str;
  gboolean highlight = (flag & PURPLE_MESSAGE_NICK);
  GtkTextBuffer *buffer = ( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type())))).text_buffer;
  str = g_strdup_printf((((highlight != 0)?"HILIT %s" : "BUDDY %s")),who);
  buddytag = gtk_text_tag_table_lookup(gtk_text_buffer_get_tag_table(buffer),str);
  if ((buddytag == ((GtkTextTag *)((void *)0))) && (create != 0)) {
    if (highlight != 0) 
      buddytag = gtk_text_buffer_create_tag(buffer,str,"foreground",get_text_tag_color(gtk_text_tag_table_lookup(gtk_text_buffer_get_tag_table(buffer),"highlight-name")),"weight",PANGO_WEIGHT_BOLD,((void *)((void *)0)));
    else 
      buddytag = gtk_text_buffer_create_tag(buffer,str,"foreground-gdk",get_nick_color(gtkconv,who),"weight",((purple_find_buddy(purple_conversation_get_account(conv),who) != 0)?PANGO_WEIGHT_BOLD : PANGO_WEIGHT_NORMAL),((void *)((void *)0)));
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buddytag),((GType )(20 << 2))))),"cursor","");
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buddytag),((GType )(20 << 2))))),"event",((GCallback )buddytag_event),conv,0,((GConnectFlags )0));
  }
  g_free(str);
  return buddytag;
}

static void pidgin_conv_calculate_newday(PidginConversation *gtkconv,time_t mtime)
{
  struct tm *tm = localtime((&mtime));
  tm -> tm_hour = (tm -> tm_min = (tm -> tm_sec = 0));
  tm -> tm_mday++;
  gtkconv -> newday = mktime(tm);
}
/* Detect string direction and encapsulate the string in RLE/LRE/PDF unicode characters
   str - pointer to string (string is re-allocated and the pointer updated) */

static void str_embed_direction_chars(char **str)
{
#ifdef HAVE_PANGO14
  char pre_str[4UL];
  char post_str[10UL];
  char *ret;
  if (PANGO_DIRECTION_RTL == (pango_find_base_dir(( *str),(-1)))) {
    sprintf(pre_str,"%c%c%c",0xE2,128,0xAB);
/* RLE */
    sprintf(post_str,"%c%c%c%c%c%c%c%c%c",0xE2,128,0xAC,0xE2,128,0x8E,0xE2,128,0xAC);
/* PDF */
/* LRM */
/* PDF */
  }
  else {
    sprintf(pre_str,"%c%c%c",0xE2,128,0xAA);
/* LRE */
    sprintf(post_str,"%c%c%c%c%c%c%c%c%c",0xE2,128,0xAC,0xE2,128,0x8F,0xE2,128,0xAC);
/* PDF */
/* RLM */
/* PDF */
  }
  ret = g_strconcat(pre_str, *str,post_str,((void *)((void *)0)));
  g_free(( *str));
   *str = ret;
#endif
}

static void pidgin_conv_write_conv(PurpleConversation *conv,const char *name,const char *alias,const char *message,PurpleMessageFlags flags,time_t mtime)
{
  PidginConversation *gtkconv;
  PurpleConnection *gc;
  PurpleAccount *account;
  int gtk_font_options = 0;
  int gtk_font_options_all = 0;
  int max_scrollback_lines;
  int line_count;
  char buf2[4096UL];
  gboolean show_date;
  char *mdate;
  char *str;
  char *with_font_tag;
  char *sml_attrib = (char *)((void *)0);
  size_t length;
  PurpleConversationType type;
  char *displaying;
  gboolean plugin_return;
  char *bracket;
  int tag_count = 0;
  gboolean is_rtl_message = 0;
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  do {
    if (gtkconv != ((PidginConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gtkconv != NULL");
      return ;
    };
  }while (0);
  if (gtkconv -> attach.timer != 0) {
/* We are currently in the process of filling up the buffer with the message
		 * history of the conversation. So we do not need to add the message here.
		 * Instead, this message will be added to the message-list, which in turn will
		 * be processed and displayed by the attach-callback.
		 */
    return ;
  }
  if (conv != (gtkconv -> active_conv)) {
    if ((flags & PURPLE_MESSAGE_ACTIVE_ONLY) != 0U) {
/* Unless this had PURPLE_MESSAGE_NO_LOG, this message
			 * was logged.  Plugin writers: if this isn't what
			 * you wanted, call purple_conv_im_write() instead of
			 * purple_conversation_write(). */
      purple_debug_info("gtkconv","Suppressing message for an inactive conversation in pidgin_conv_write_conv()\n");
      return ;
    }
/* Set the active conversation to the one that just messaged us. */
/* TODO: consider not doing this if the account is offline or something */
    if ((flags & (PURPLE_MESSAGE_SEND | PURPLE_MESSAGE_RECV)) != 0U) 
      pidgin_conv_switch_active_conversation(conv);
  }
  type = purple_conversation_get_type(conv);
  account = purple_conversation_get_account(conv);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  gc = purple_account_get_connection(account);
  do {
    if ((gc != ((PurpleConnection *)((void *)0))) || !((flags & (PURPLE_MESSAGE_SEND | PURPLE_MESSAGE_RECV)) != 0U)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL || !(flags & (PURPLE_MESSAGE_SEND | PURPLE_MESSAGE_RECV))");
      return ;
    };
  }while (0);
/* Make sure URLs are clickable */
  if ((flags & PURPLE_MESSAGE_NO_LINKIFY) != 0U) 
    displaying = g_strdup(message);
  else 
    displaying = purple_markup_linkify(message);
  plugin_return = ((gint )((glong )(purple_signal_emit_return_1(pidgin_conversations_get_handle(),(((type == PURPLE_CONV_TYPE_IM)?"displaying-im-msg" : "displaying-chat-msg")),account,name,&displaying,conv,flags))));
  if (plugin_return != 0) {
    g_free(displaying);
    return ;
  }
  length = (strlen(displaying) + 1);
/* Awful hack to work around GtkIMHtml's inefficient rendering of messages with lots of formatting changes.
	 * If a message has over 100 '<' characters, strip formatting before appending it. Hopefully nobody actually
	 * needs that much formatting, anyway.
	 */
  for (bracket = strchr(displaying,60); (bracket != 0) && (bracket[1] != 0); bracket = strchr((bracket + 1),60)) 
    tag_count++;
  if (tag_count > 100) {
    char *tmp = displaying;
    displaying = purple_markup_strip_html(tmp);
    g_free(tmp);
  }
  line_count = gtk_text_buffer_get_line_count(gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_text_view_get_type())))));
  max_scrollback_lines = purple_prefs_get_int("/pidgin/conversations/scrollback_lines");
/* If we're sitting at more than 100 lines more than the
	   max scrollback, trim down to max scrollback */
  if ((max_scrollback_lines > 0) && (line_count > (max_scrollback_lines + 100))) {
    GtkTextBuffer *text_buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_text_view_get_type()))));
    GtkTextIter start;
    GtkTextIter end;
    gtk_text_buffer_get_start_iter(text_buffer,&start);
    gtk_text_buffer_get_iter_at_line(text_buffer,&end,(line_count - max_scrollback_lines));
    gtk_imhtml_delete(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),&start,&end);
  }
  if (type == PURPLE_CONV_TYPE_CHAT) {
/* Create anchor for user */
    GtkTextIter iter;
    char *tmp = g_strconcat("user:",name,((void *)((void *)0)));
    gtk_text_buffer_get_end_iter(gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_text_view_get_type())))),&iter);
    gtk_text_buffer_create_mark(gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_text_view_get_type())))),tmp,(&iter),(!0));
    g_free(tmp);
  }
  if (purple_prefs_get_bool("/pidgin/conversations/use_smooth_scrolling") != 0) 
    gtk_font_options_all |= GTK_IMHTML_USE_SMOOTHSCROLLING;
  if (gtk_text_buffer_get_char_count(gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_text_view_get_type()))))) != 0) 
    gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),"<BR>",(gtk_font_options_all | GTK_IMHTML_NO_SCROLL),0);
/* First message in a conversation. */
  if ((gtkconv -> newday) == 0) 
    pidgin_conv_calculate_newday(gtkconv,mtime);
/* Show the date on the first message in a new day, or if the message is
	 * older than 20 minutes. */
  show_date = ((mtime >= (gtkconv -> newday)) || (time(0) > (mtime + (20 * 60))));
  mdate = (purple_signal_emit_return_1(pidgin_conversations_get_handle(),"conversation-timestamp",conv,mtime,show_date));
  if (mdate == ((char *)((void *)0))) {
    struct tm *tm = localtime((&mtime));
    const char *tmp;
    if (show_date != 0) 
      tmp = purple_date_format_long(tm);
    else 
      tmp = purple_time_format(tm);
    mdate = g_strdup_printf("(%s)",tmp);
  }
/* Bi-Directional support - set timestamp direction using unicode characters */
  is_rtl_message = purple_markup_is_rtl(message);
/* Enforce direction only if message is RTL - doesn't effect LTR users */
  if (is_rtl_message != 0) 
    str_embed_direction_chars(&mdate);
  if (mtime >= (gtkconv -> newday)) 
    pidgin_conv_calculate_newday(gtkconv,mtime);
  sml_attrib = g_strdup_printf("sml=\"%s\"",purple_account_get_protocol_name(account));
  gtk_font_options |= GTK_IMHTML_NO_COMMENTS;
  if (((flags & PURPLE_MESSAGE_RECV) != 0U) && !(purple_prefs_get_bool("/pidgin/conversations/show_incoming_formatting") != 0)) 
    gtk_font_options |= GTK_IMHTML_NO_COLOURS | GTK_IMHTML_NO_FONTS | GTK_IMHTML_NO_SIZES | GTK_IMHTML_NO_FORMATTING;
/* this is gonna crash one day, I can feel it. */
  if ((( *((PurplePluginProtocolInfo *)( *( *purple_find_prpl(purple_account_get_protocol_id((conv -> account)))).info).extra_info)).options & OPT_PROTO_USE_POINTSIZE) != 0U) {
    gtk_font_options |= GTK_IMHTML_USE_POINTSIZE;
  }
  if (!((flags & PURPLE_MESSAGE_RECV) != 0U) && (((conv -> features) & PURPLE_CONNECTION_ALLOW_CUSTOM_SMILEY) != 0U)) {
/* We want to see our own smileys. Need to revert it after send*/
    pidgin_themes_smiley_themeize_custom((gtkconv -> imhtml));
  }
/* TODO: These colors should not be hardcoded so log.c can use them */
  if ((flags & PURPLE_MESSAGE_RAW) != 0U) {
    gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),message,gtk_font_options_all,0);
  }
  else if ((flags & PURPLE_MESSAGE_SYSTEM) != 0U) {
    g_snprintf(buf2,(sizeof(buf2)),"<FONT %s><FONT SIZE=\"2\"><!--%s --></FONT><B>%s</B></FONT>",((sml_attrib != 0)?sml_attrib : ""),mdate,displaying);
    gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),buf2,gtk_font_options_all,0);
  }
  else if ((flags & PURPLE_MESSAGE_ERROR) != 0U) {
    g_snprintf(buf2,(sizeof(buf2)),"<FONT COLOR=\"#ff0000\"><FONT %s><FONT SIZE=\"2\"><!--%s --></FONT><B>%s</B></FONT></FONT>",((sml_attrib != 0)?sml_attrib : ""),mdate,displaying);
    gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),buf2,gtk_font_options_all,0);
  }
  else if ((flags & PURPLE_MESSAGE_NO_LOG) != 0U) {
    g_snprintf(buf2,(2048 * 2),"<B><FONT %s COLOR=\"#777777\">%s</FONT></B>",((sml_attrib != 0)?sml_attrib : ""),displaying);
    gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),buf2,gtk_font_options_all,0);
  }
  else {
    char *new_message = (g_memdup(displaying,length));
    char *alias_escaped = (alias != 0)?g_markup_escape_text(alias,(strlen(alias))) : g_strdup("");
/* The initial offset is to deal with
		 * escaped entities making the string longer */
    int tag_start_offset = 0;
    int tag_end_offset = 0;
    const char *tagname = (const char *)((void *)0);
    GtkTextIter start;
    GtkTextIter end;
    GtkTextMark *mark;
    GtkTextTag *tag;
    GtkTextBuffer *buffer = ( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type())))).text_buffer;
/* Enforce direction on alias */
    if (is_rtl_message != 0) 
      str_embed_direction_chars(&alias_escaped);
    str = (g_malloc(1024));
    if ((flags & PURPLE_MESSAGE_WHISPER) != 0U) {
/* If we're whispering, it's not an autoresponse. */
      if (purple_message_meify(new_message,(-1)) != 0) {
        g_snprintf(str,1024,"***%s",alias_escaped);
        tag_start_offset += 3;
        tagname = "whisper-action-name";
      }
      else {
        g_snprintf(str,1024,"*%s*:",alias_escaped);
        tag_start_offset += 1;
        tag_end_offset = 2;
        tagname = "whisper-name";
      }
    }
    else {
      if (purple_message_meify(new_message,(-1)) != 0) {
        if ((flags & PURPLE_MESSAGE_AUTO_RESP) != 0U) {
          g_snprintf(str,1024,"%s ***%s","&lt;AUTO-REPLY&gt; : ",alias_escaped);
          tag_start_offset += ((strlen("&lt;AUTO-REPLY&gt; : ") - 6) + 4);
        }
        else {
          g_snprintf(str,1024,"***%s",alias_escaped);
          tag_start_offset += 3;
        }
        if ((flags & PURPLE_MESSAGE_NICK) != 0U) 
          tagname = "highlight-name";
        else 
          tagname = "action-name";
      }
      else {
        if ((flags & PURPLE_MESSAGE_AUTO_RESP) != 0U) {
          g_snprintf(str,1024,"%s %s",alias_escaped,"&lt;AUTO-REPLY&gt; : ");
          tag_start_offset += ((strlen("&lt;AUTO-REPLY&gt; : ") - 6) + 1);
        }
        else {
          g_snprintf(str,1024,"%s:",alias_escaped);
          tag_end_offset = 1;
        }
        if ((flags & PURPLE_MESSAGE_NICK) != 0U) {
          if (type == PURPLE_CONV_TYPE_IM) {
            tagname = "highlight-name";
          }
        }
        else if ((flags & PURPLE_MESSAGE_RECV) != 0U) {
/* The tagname for chats is handled by get_buddy_tag */
          if (type == PURPLE_CONV_TYPE_IM) {
            tagname = "receive-name";
          }
        }
        else if ((flags & PURPLE_MESSAGE_SEND) != 0U) {
          tagname = "send-name";
        }
        else {
          purple_debug_error("gtkconv","message missing flags\n");
        }
      }
    }
    g_free(alias_escaped);
    if (tagname != 0) 
      tag = gtk_text_tag_table_lookup(gtk_text_buffer_get_tag_table(buffer),tagname);
    else 
      tag = get_buddy_tag(conv,name,flags,(!0));
    if (( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type())))).show_comments != 0) {
/* The color for the timestamp has to be set in the font-tags, unfortunately.
			 * Applying the nick-tag to timestamps would work, but that can make it
			 * bold. I thought applying the "comment" tag again, which has "weight" set
			 * to PANGO_WEIGHT_NORMAL, would remove the boldness. But it doesn't. So
			 * this will have to do. I don't terribly like it.  -- sadrul */
      const char *color = get_text_tag_color(tag);
      g_snprintf(buf2,(2048 * 2),"<FONT %s%s%s SIZE=\"2\"><!--%s --></FONT>",((color != 0)?"COLOR=\"" : ""),((color != 0)?color : ""),((color != 0)?"\"" : ""),mdate);
      gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),buf2,(gtk_font_options_all | GTK_IMHTML_NO_SCROLL),0);
    }
    gtk_text_buffer_get_end_iter(buffer,&end);
    mark = gtk_text_buffer_create_mark(buffer,0,(&end),(!0));
    g_snprintf(buf2,(2048 * 2),"<FONT %s>%s</FONT> ",((sml_attrib != 0)?sml_attrib : ""),str);
    gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),buf2,(gtk_font_options_all | GTK_IMHTML_NO_SCROLL),0);
    gtk_text_buffer_get_end_iter(buffer,&end);
    gtk_text_buffer_get_iter_at_mark(buffer,&start,mark);
    gtk_text_buffer_apply_tag(buffer,tag,(&start),(&end));
    gtk_text_buffer_delete_mark(buffer,mark);
    g_free(str);
    if (gc != 0) {
      char *pre = g_strdup_printf("<font %s>",((sml_attrib != 0)?sml_attrib : ""));
      char *post = "</font>";
      int pre_len = (strlen(pre));
      int post_len = (strlen(post));
      with_font_tag = (g_malloc((((length + pre_len) + post_len) + 1)));
      strcpy(with_font_tag,pre);
      memcpy((with_font_tag + pre_len),new_message,length);
      strcpy(((with_font_tag + pre_len) + length),post);
      length += (pre_len + post_len);
      g_free(pre);
    }
    else 
      with_font_tag = (g_memdup(new_message,length));
    gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),with_font_tag,(gtk_font_options | gtk_font_options_all),0);
    g_free(with_font_tag);
    g_free(new_message);
  }
  g_free(mdate);
  g_free(sml_attrib);
/* Tab highlighting stuff */
  if (!((flags & PURPLE_MESSAGE_SEND) != 0U) && !(pidgin_conv_has_focus(conv) != 0)) {
    PidginUnseenState unseen = PIDGIN_UNSEEN_NONE;
    if ((flags & PURPLE_MESSAGE_NICK) == PURPLE_MESSAGE_NICK) 
      unseen = PIDGIN_UNSEEN_NICK;
    else if (((flags & PURPLE_MESSAGE_SYSTEM) == PURPLE_MESSAGE_SYSTEM) || ((flags & PURPLE_MESSAGE_ERROR) == PURPLE_MESSAGE_ERROR)) 
      unseen = PIDGIN_UNSEEN_EVENT;
    else if ((flags & PURPLE_MESSAGE_NO_LOG) == PURPLE_MESSAGE_NO_LOG) 
      unseen = PIDGIN_UNSEEN_NO_LOG;
    else 
      unseen = PIDGIN_UNSEEN_TEXT;
    gtkconv_set_unseen(gtkconv,unseen);
  }
  if (!((flags & PURPLE_MESSAGE_RECV) != 0U) && (((conv -> features) & PURPLE_CONNECTION_ALLOW_CUSTOM_SMILEY) != 0U)) {
/* Restore the smiley-data */
    pidgin_themes_smiley_themeize((gtkconv -> imhtml));
  }
  purple_signal_emit(pidgin_conversations_get_handle(),((type == PURPLE_CONV_TYPE_IM)?"displayed-im-msg" : "displayed-chat-msg"),account,name,displaying,conv,flags);
  g_free(displaying);
  update_typing_message(gtkconv,0);
}

static gboolean get_iter_from_chatbuddy(PurpleConvChatBuddy *cb,GtkTreeIter *iter)
{
  GtkTreeRowReference *ref;
  GtkTreePath *path;
  GtkTreeModel *model;
  do {
    if (cb != ((PurpleConvChatBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cb != NULL");
      return 0;
    };
  }while (0);
  ref = (cb -> ui_data);
  if (!(ref != 0)) 
    return 0;
  if ((path = gtk_tree_row_reference_get_path(ref)) == ((GtkTreePath *)((void *)0))) 
    return 0;
  model = gtk_tree_row_reference_get_model(ref);
  if (!(gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),iter,path) != 0)) {
    gtk_tree_path_free(path);
    return 0;
  }
  gtk_tree_path_free(path);
  return (!0);
}

static void pidgin_conv_chat_add_users(PurpleConversation *conv,GList *cbuddies,gboolean new_arrivals)
{
  PurpleConvChat *chat;
  PidginConversation *gtkconv;
  PidginChatPane *gtkchat;
  GtkListStore *ls;
  GList *l;
  char tmp[4096UL];
  int num_users;
  chat = purple_conversation_get_chat_data(conv);
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  gtkchat = gtkconv -> u.chat;
  num_users = (g_list_length(purple_conv_chat_get_users(chat)));
  g_snprintf(tmp,(sizeof(tmp)),(ngettext("%d person in room","%d people in room",num_users)),num_users);
  gtk_label_set_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> count)),gtk_label_get_type()))),tmp);
  ls = ((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_tree_view_get_model(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> list)),gtk_tree_view_get_type())))))),gtk_list_store_get_type())));
  gtk_tree_sortable_set_sort_column_id(((GtkTreeSortable *)(g_type_check_instance_cast(((GTypeInstance *)ls),gtk_tree_sortable_get_type()))),GTK_TREE_SORTABLE_UNSORTED_SORT_COLUMN_ID,GTK_TREE_SORTABLE_UNSORTED_SORT_COLUMN_ID);
  l = cbuddies;
  while(l != ((GList *)((void *)0))){
    add_chat_buddy_common(conv,((PurpleConvChatBuddy *)(l -> data)),0);
    l = (l -> next);
  }
/* Currently GTK+ maintains our sorted list after it's in the tree.
	 * This may change if it turns out we can manage it faster ourselves.
	 */
  gtk_tree_sortable_set_sort_column_id(((GtkTreeSortable *)(g_type_check_instance_cast(((GTypeInstance *)ls),gtk_tree_sortable_get_type()))),CHAT_USERS_ALIAS_KEY_COLUMN,GTK_SORT_ASCENDING);
}

static void pidgin_conv_chat_rename_user(PurpleConversation *conv,const char *old_name,const char *new_name,const char *new_alias)
{
  PurpleConvChat *chat;
  PidginConversation *gtkconv;
  PidginChatPane *gtkchat;
  PurpleConvChatBuddy *old_cbuddy;
  PurpleConvChatBuddy *new_cbuddy;
  GtkTreeIter iter;
  GtkTreeModel *model;
  GtkTextTag *tag;
  chat = purple_conversation_get_chat_data(conv);
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  gtkchat = gtkconv -> u.chat;
  model = gtk_tree_view_get_model(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> list)),gtk_tree_view_get_type()))));
  if (!(gtk_tree_model_get_iter_first(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter) != 0)) 
    return ;
  if ((tag = get_buddy_tag(conv,old_name,0,0)) != 0) 
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))),"style",PANGO_STYLE_ITALIC,((void *)((void *)0)));
  if ((tag = get_buddy_tag(conv,old_name,PURPLE_MESSAGE_NICK,0)) != 0) 
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))),"style",PANGO_STYLE_ITALIC,((void *)((void *)0)));
  old_cbuddy = purple_conv_chat_cb_find(chat,old_name);
  if (!(old_cbuddy != 0)) 
    return ;
  if (get_iter_from_chatbuddy(old_cbuddy,&iter) != 0) {
    GtkTreeRowReference *ref = (old_cbuddy -> ui_data);
    gtk_list_store_remove(((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_list_store_get_type()))),&iter);
    gtk_tree_row_reference_free(ref);
    old_cbuddy -> ui_data = ((gpointer )((void *)0));
  }
  do {
    if (new_alias != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"new_alias != NULL");
      return ;
    };
  }while (0);
  new_cbuddy = purple_conv_chat_cb_find(chat,new_name);
  add_chat_buddy_common(conv,new_cbuddy,old_name);
}

static void pidgin_conv_chat_remove_users(PurpleConversation *conv,GList *users)
{
  PurpleConvChat *chat;
  PidginConversation *gtkconv;
  PidginChatPane *gtkchat;
  GtkTreeIter iter;
  GtkTreeModel *model;
  GList *l;
  char tmp[4096UL];
  int num_users;
  gboolean f;
  GtkTextTag *tag;
  chat = purple_conversation_get_chat_data(conv);
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  gtkchat = gtkconv -> u.chat;
  num_users = (g_list_length(purple_conv_chat_get_users(chat)));
  for (l = users; l != ((GList *)((void *)0)); l = (l -> next)) {
    model = gtk_tree_view_get_model(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> list)),gtk_tree_view_get_type()))));
    if (!(gtk_tree_model_get_iter_first(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter) != 0)) 
/* XXX: Break? */
      continue; 
    do {
      char *val;
      gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter,CHAT_USERS_NAME_COLUMN,&val,-1);
      if (!(purple_utf8_strcasecmp(((char *)(l -> data)),val) != 0)) {
        f = gtk_list_store_remove(((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_list_store_get_type()))),&iter);
      }
      else 
        f = gtk_tree_model_iter_next(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter);
      g_free(val);
    }while (f != 0);
    if ((tag = get_buddy_tag(conv,(l -> data),0,0)) != 0) 
      g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))),"style",PANGO_STYLE_ITALIC,((void *)((void *)0)));
    if ((tag = get_buddy_tag(conv,(l -> data),PURPLE_MESSAGE_NICK,0)) != 0) 
      g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))),"style",PANGO_STYLE_ITALIC,((void *)((void *)0)));
  }
  g_snprintf(tmp,(sizeof(tmp)),(ngettext("%d person in room","%d people in room",num_users)),num_users);
  gtk_label_set_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> count)),gtk_label_get_type()))),tmp);
}

static void pidgin_conv_chat_update_user(PurpleConversation *conv,const char *user)
{
  PurpleConvChat *chat;
  PurpleConvChatBuddy *cbuddy;
  PidginConversation *gtkconv;
  PidginChatPane *gtkchat;
  GtkTreeIter iter;
  GtkTreeModel *model;
  chat = purple_conversation_get_chat_data(conv);
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  gtkchat = gtkconv -> u.chat;
  model = gtk_tree_view_get_model(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> list)),gtk_tree_view_get_type()))));
  if (!(gtk_tree_model_get_iter_first(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))),&iter) != 0)) 
    return ;
  cbuddy = purple_conv_chat_cb_find(chat,user);
  if (!(cbuddy != 0)) 
    return ;
  if (get_iter_from_chatbuddy(cbuddy,&iter) != 0) {
    GtkTreeRowReference *ref = (cbuddy -> ui_data);
    gtk_list_store_remove(((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_list_store_get_type()))),&iter);
    gtk_tree_row_reference_free(ref);
    cbuddy -> ui_data = ((gpointer )((void *)0));
  }
  if (cbuddy != 0) 
    add_chat_buddy_common(conv,cbuddy,0);
}

gboolean pidgin_conv_has_focus(PurpleConversation *conv)
{
  PidginConversation *gtkconv = (PidginConversation *)(conv -> ui_data);
  PidginWindow *win;
  gboolean has_focus;
  win = (gtkconv -> win);
  g_object_get(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),"has-toplevel-focus",&has_focus,((void *)((void *)0)));
  if ((has_focus != 0) && (pidgin_conv_window_is_active_conversation(conv) != 0)) 
    return (!0);
  return 0;
}

static gboolean add_custom_smiley_for_imhtml(GtkIMHtml *imhtml,const char *sml,const char *smile)
{
  GtkIMHtmlSmiley *smiley;
  smiley = gtk_imhtml_smiley_get(imhtml,sml,smile);
  if (smiley != 0) {
    if (!(((smiley -> flags) & GTK_IMHTML_SMILEY_CUSTOM) != 0U)) {
      return 0;
    }
    gtk_imhtml_smiley_reload(smiley);
    return (!0);
  }
  smiley = gtk_imhtml_smiley_create(0,smile,0,GTK_IMHTML_SMILEY_CUSTOM);
  gtk_imhtml_associate_smiley(imhtml,sml,smiley);
  g_signal_connect_data(imhtml,"destroy",((GCallback )gtk_imhtml_smiley_destroy),smiley,0,G_CONNECT_SWAPPED);
  return (!0);
}

static gboolean pidgin_conv_custom_smiley_add(PurpleConversation *conv,const char *smile,gboolean remote)
{
  PidginConversation *gtkconv;
  struct smiley_list *list;
  const char *sml = (const char *)((void *)0);
  const char *conv_sml;
  if ((!(conv != 0) || !(smile != 0)) || !(( *smile) != 0)) {
    return 0;
  }
/* If smileys are off, return false */
  if (pidgin_themes_smileys_disabled() != 0) 
    return 0;
/* If possible add this smiley to the current theme.
	 * The addition is only temporary: custom smilies aren't saved to disk. */
  conv_sml = purple_account_get_protocol_name((conv -> account));
  gtkconv = ((PidginConversation *)(conv -> ui_data));
{
    for (list = ((struct smiley_list *)(current_smiley_theme -> list)); list != 0; list = (list -> next)) {
      if (!(strcmp((list -> sml),conv_sml) != 0)) {
        sml = (list -> sml);
        break; 
      }
    }
  }
  if (!(add_custom_smiley_for_imhtml(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),sml,smile) != 0)) 
    return 0;
/* If it's a local custom smiley, then add it for the entry */
  if (!(remote != 0)) 
    if (!(add_custom_smiley_for_imhtml(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type()))),sml,smile) != 0)) 
      return 0;
  return (!0);
}

static void pidgin_conv_custom_smiley_write(PurpleConversation *conv,const char *smile,const guchar *data,gsize size)
{
  PidginConversation *gtkconv;
  GtkIMHtmlSmiley *smiley;
  const char *sml;
  GError *error = (GError *)((void *)0);
  sml = purple_account_get_protocol_name((conv -> account));
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  smiley = gtk_imhtml_smiley_get(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),sml,smile);
  if (!(smiley != 0)) 
    return ;
  smiley -> data = g_realloc((smiley -> data),((smiley -> datasize) + size));
  do {
    memmove((((guchar *)(smiley -> data)) + (smiley -> datasize)),data,size);
  }while (0);
  smiley -> datasize += size;
  if (!((smiley -> loader) != 0)) 
    return ;
  if (!(gdk_pixbuf_loader_write((smiley -> loader),data,size,&error) != 0) || (error != 0)) {
    purple_debug_warning("gtkconv","gdk_pixbuf_loader_write() failed with size=%zu: %s\n",size,((error != 0)?(error -> message) : "(no error message)"));
    if (error != 0) 
      g_error_free(error);
/* We must stop using the GdkPixbufLoader because trying to load
		   certain invalid GIFs with at least gdk-pixbuf 2.23.3 can return
		   a GdkPixbuf that will cause some operations (like
		   gdk_pixbuf_scale_simple()) to consume memory in an infinite loop.
		   But we also don't want to set smiley->loader to NULL because our
		   code might expect it to be set.  So create a new loader. */
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(smiley -> loader)),((GType )(20 << 2))))));
    smiley -> loader = gdk_pixbuf_loader_new();
  }
}

static void pidgin_conv_custom_smiley_close(PurpleConversation *conv,const char *smile)
{
  PidginConversation *gtkconv;
  GtkIMHtmlSmiley *smiley;
  const char *sml;
  GError *error = (GError *)((void *)0);
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  do {
    if (smile != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"smile != NULL");
      return ;
    };
  }while (0);
  sml = purple_account_get_protocol_name((conv -> account));
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  smiley = gtk_imhtml_smiley_get(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),sml,smile);
  if (!(smiley != 0)) 
    return ;
  if (!((smiley -> loader) != 0)) 
    return ;
  purple_debug_info("gtkconv","About to close the smiley pixbuf\n");
  if (!(gdk_pixbuf_loader_close((smiley -> loader),&error) != 0) || (error != 0)) {
    purple_debug_warning("gtkconv","gdk_pixbuf_loader_close() failed: %s\n",((error != 0)?(error -> message) : "(no error message)"));
    if (error != 0) 
      g_error_free(error);
/* We must stop using the GdkPixbufLoader because if we tried to
		   load certain invalid GIFs with all current versions of GDK (as
		   of 2011-06-15) then it's possible the loader will contain data
		   that could cause some operations (like gdk_pixbuf_scale_simple())
		   to consume memory in an infinite loop.  But we also don't want
		   to set smiley->loader to NULL because our code might expect it
		   to be set.  So create a new loader. */
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(smiley -> loader)),((GType )(20 << 2))))));
    smiley -> loader = gdk_pixbuf_loader_new();
  }
}

static void pidgin_conv_send_confirm(PurpleConversation *conv,const char *message)
{
  PidginConversation *gtkconv = (PidginConversation *)(conv -> ui_data);
  gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type()))),message,0,0);
}
/*
 * Makes sure all the menu items and all the buttons are hidden/shown and
 * sensitive/insensitive.  This is called after changing tabs and when an
 * account signs on or off.
 */

static void gray_stuff_out(PidginConversation *gtkconv)
{
  PidginWindow *win;
  PurpleConversation *conv = (gtkconv -> active_conv);
  PurpleConnection *gc;
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  GdkPixbuf *window_icon = (GdkPixbuf *)((void *)0);
  GtkIMHtmlButtons buttons;
  PurpleAccount *account;
  win = pidgin_conv_get_window(gtkconv);
  gc = purple_conversation_get_gc(conv);
  account = purple_conversation_get_account(conv);
  if (gc != ((PurpleConnection *)((void *)0))) 
    prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info);
  if (win -> menu.send_to != ((GtkWidget *)((void *)0))) 
    update_send_to_selection(win);
/*
	 * Handle hiding and showing stuff based on what type of conv this is.
	 * Stuff that Purple IMs support in general should be shown for IM
	 * conversations.  Stuff that Purple chats support in general should be
	 * shown for chat conversations.  It doesn't matter whether the PRPL
	 * supports it or not--that only affects if the button or menu item
	 * is sensitive or not.
	 */
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
/* Show stuff that applies to IMs, hide stuff that applies to chats */
/* Deal with menu items */
    gtk_widget_show(win -> menu.view_log);
    gtk_widget_show(win -> menu.send_file);
    gtk_widget_show((g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),"get_attention")));
    gtk_widget_show(win -> menu.add_pounce);
    gtk_widget_show(win -> menu.get_info);
    gtk_widget_hide(win -> menu.invite);
    gtk_widget_show(win -> menu.alias);
    if (purple_privacy_check(account,purple_conversation_get_name(conv)) != 0) {
      gtk_widget_hide(win -> menu.unblock);
      gtk_widget_show(win -> menu.block);
    }
    else {
      gtk_widget_hide(win -> menu.block);
      gtk_widget_show(win -> menu.unblock);
    }
    if (purple_find_buddy(account,purple_conversation_get_name(conv)) == ((PurpleBuddy *)((void *)0))) {
      gtk_widget_show(win -> menu.add);
      gtk_widget_hide(win -> menu.remove);
    }
    else {
      gtk_widget_show(win -> menu.remove);
      gtk_widget_hide(win -> menu.add);
    }
    gtk_widget_show(win -> menu.insert_link);
    gtk_widget_show(win -> menu.insert_image);
  }
  else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) {
/* Show stuff that applies to Chats, hide stuff that applies to IMs */
/* Deal with menu items */
    gtk_widget_show(win -> menu.view_log);
    gtk_widget_hide(win -> menu.send_file);
    gtk_widget_hide((g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),"get_attention")));
    gtk_widget_hide(win -> menu.add_pounce);
    gtk_widget_hide(win -> menu.get_info);
    gtk_widget_show(win -> menu.invite);
    gtk_widget_show(win -> menu.alias);
    gtk_widget_hide(win -> menu.block);
    gtk_widget_hide(win -> menu.unblock);
    if ((account == ((PurpleAccount *)((void *)0))) || (purple_blist_find_chat(account,purple_conversation_get_name(conv)) == ((PurpleChat *)((void *)0)))) {
/* If the chat is NOT in the buddy list */
      gtk_widget_show(win -> menu.add);
      gtk_widget_hide(win -> menu.remove);
    }
    else {
/* If the chat IS in the buddy list */
      gtk_widget_hide(win -> menu.add);
      gtk_widget_show(win -> menu.remove);
    }
    gtk_widget_show(win -> menu.insert_link);
    gtk_widget_show(win -> menu.insert_image);
  }
/*
	 * Handle graying stuff out based on whether an account is connected
	 * and what features that account supports.
	 */
  if ((gc != ((PurpleConnection *)((void *)0))) && (((purple_conversation_get_type(conv)) != PURPLE_CONV_TYPE_CHAT) || !(purple_conv_chat_has_left(purple_conversation_get_chat_data(conv)) != 0))) {
/* Account is online */
/* Deal with the toolbar */
    if (((conv -> features) & PURPLE_CONNECTION_HTML) != 0U) {
/* Everything on */
      buttons = GTK_IMHTML_ALL;
      if (((conv -> features) & PURPLE_CONNECTION_NO_BGCOLOR) != 0U) 
        buttons &= (~GTK_IMHTML_BACKCOLOR);
      if (((conv -> features) & PURPLE_CONNECTION_NO_FONTSIZE) != 0U) {
        buttons &= (~GTK_IMHTML_GROW);
        buttons &= (~GTK_IMHTML_SHRINK);
      }
      if (((conv -> features) & PURPLE_CONNECTION_NO_URLDESC) != 0U) 
        buttons &= (~GTK_IMHTML_LINKDESC);
    }
    else {
      buttons = (GTK_IMHTML_SMILEY | GTK_IMHTML_IMAGE);
    }
    if (!(((prpl_info -> options) & OPT_PROTO_IM_IMAGE) != 0U)) 
      conv -> features |= PURPLE_CONNECTION_NO_IMAGES;
    if (((conv -> features) & PURPLE_CONNECTION_NO_IMAGES) != 0U) 
      buttons &= (~GTK_IMHTML_IMAGE);
    if (((conv -> features) & PURPLE_CONNECTION_ALLOW_CUSTOM_SMILEY) != 0U) 
      buttons |= GTK_IMHTML_CUSTOM_SMILEY;
    else 
      buttons &= (~GTK_IMHTML_CUSTOM_SMILEY);
    gtk_imhtml_set_format_functions(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type()))),buttons);
    if (account != ((PurpleAccount *)((void *)0))) 
      gtk_imhtmltoolbar_associate_smileys(((GtkIMHtmlToolbar *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> toolbar)),gtk_imhtmltoolbar_get_type()))),purple_account_get_protocol_id(account));
/* Deal with menu items */
    gtk_widget_set_sensitive(win -> menu.view_log,(!0));
    gtk_widget_set_sensitive(win -> menu.add_pounce,(!0));
    gtk_widget_set_sensitive(win -> menu.get_info,((prpl_info -> get_info) != ((void (*)(PurpleConnection *, const char *))((void *)0))));
    gtk_widget_set_sensitive(win -> menu.invite,((prpl_info -> chat_invite) != ((void (*)(PurpleConnection *, int , const char *, const char *))((void *)0))));
    gtk_widget_set_sensitive(win -> menu.insert_link,((conv -> features) & PURPLE_CONNECTION_HTML));
    gtk_widget_set_sensitive(win -> menu.insert_image,!(((conv -> features) & PURPLE_CONNECTION_NO_IMAGES) != 0U));
    if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
      gtk_widget_set_sensitive(win -> menu.add,(((prpl_info -> add_buddy) != ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *))((void *)0))) || ((prpl_info -> add_buddy_with_invite) != ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *, const char *))((void *)0)))));
      gtk_widget_set_sensitive(win -> menu.remove,((prpl_info -> remove_buddy) != ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *))((void *)0))));
      gtk_widget_set_sensitive(win -> menu.send_file,(((prpl_info -> send_file) != ((void (*)(PurpleConnection *, const char *, const char *))((void *)0))) && (!((prpl_info -> can_receive_file) != 0) || (( *(prpl_info -> can_receive_file))(gc,purple_conversation_get_name(conv)) != 0))));
      gtk_widget_set_sensitive((g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),"get_attention")),((prpl_info -> send_attention) != ((gboolean (*)(PurpleConnection *, const char *, guint ))((void *)0))));
      gtk_widget_set_sensitive(win -> menu.alias,((account != ((PurpleAccount *)((void *)0))) && (purple_find_buddy(account,purple_conversation_get_name(conv)) != ((PurpleBuddy *)((void *)0)))));
    }
    else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) {
      gtk_widget_set_sensitive(win -> menu.add,((prpl_info -> join_chat) != ((void (*)(PurpleConnection *, GHashTable *))((void *)0))));
      gtk_widget_set_sensitive(win -> menu.remove,((prpl_info -> join_chat) != ((void (*)(PurpleConnection *, GHashTable *))((void *)0))));
      gtk_widget_set_sensitive(win -> menu.alias,((account != ((PurpleAccount *)((void *)0))) && (purple_blist_find_chat(account,purple_conversation_get_name(conv)) != ((PurpleChat *)((void *)0)))));
    }
  }
  else {
/* Account is offline */
/* Or it's a chat that we've left. */
/* Then deal with menu items */
    gtk_widget_set_sensitive(win -> menu.view_log,(!0));
    gtk_widget_set_sensitive(win -> menu.send_file,0);
    gtk_widget_set_sensitive((g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),"get_attention")),0);
    gtk_widget_set_sensitive(win -> menu.add_pounce,(!0));
    gtk_widget_set_sensitive(win -> menu.get_info,0);
    gtk_widget_set_sensitive(win -> menu.invite,0);
    gtk_widget_set_sensitive(win -> menu.alias,0);
    gtk_widget_set_sensitive(win -> menu.add,0);
    gtk_widget_set_sensitive(win -> menu.remove,0);
    gtk_widget_set_sensitive(win -> menu.insert_link,(!0));
    gtk_widget_set_sensitive(win -> menu.insert_image,0);
  }
/*
	 * Update the window's icon
	 */
  if (pidgin_conv_window_is_active_conversation(conv) != 0) {
    GList *l = (GList *)((void *)0);
    if (((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) && (( *gtkconv -> u.im).anim != 0)) {
      PurpleBuddy *buddy = purple_find_buddy((conv -> account),(conv -> name));
      window_icon = gdk_pixbuf_animation_get_static_image(( *gtkconv -> u.im).anim);
      if ((buddy != 0) && !(((buddy != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0))) 
        gdk_pixbuf_saturate_and_pixelate(window_icon,window_icon,0.0,0);
      g_object_ref(window_icon);
      l = g_list_append(l,window_icon);
    }
    else {
      l = pidgin_conv_get_tab_icons(conv);
    }
    gtk_window_set_icon_list(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),gtk_window_get_type()))),l);
    if (window_icon != ((GdkPixbuf *)((void *)0))) {
      g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window_icon),((GType )(20 << 2))))));
      g_list_free(l);
    }
  }
}

static void pidgin_conv_update_fields(PurpleConversation *conv,PidginConvFields fields)
{
  PidginConversation *gtkconv;
  PidginWindow *win;
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  if (!(gtkconv != 0)) 
    return ;
  win = pidgin_conv_get_window(gtkconv);
  if (!(win != 0)) 
    return ;
  if ((fields & PIDGIN_CONV_SET_TITLE) != 0U) {
    purple_conversation_autoset_title(conv);
  }
  if ((fields & PIDGIN_CONV_BUDDY_ICON) != 0U) {
    if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) 
      pidgin_conv_update_buddy_icon(conv);
  }
  if ((fields & PIDGIN_CONV_MENU) != 0U) {
    gray_stuff_out(((PidginConversation *)(conv -> ui_data)));
    generate_send_to_items(win);
  }
  if ((fields & PIDGIN_CONV_TAB_ICON) != 0U) {
    update_tab_icon(conv);
/* To update the icons in SendTo menu */
    generate_send_to_items(win);
  }
  if (((fields & PIDGIN_CONV_TOPIC) != 0U) && ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT)) {
    const char *topic;
    PurpleConvChat *chat = purple_conversation_get_chat_data(conv);
    PidginChatPane *gtkchat = gtkconv -> u.chat;
    if ((gtkchat -> topic_text) != ((GtkWidget *)((void *)0))) {
      topic = purple_conv_chat_get_topic(chat);
      gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(gtkchat -> topic_text)),gtk_entry_get_type()))),((topic != 0)?topic : ""));
      gtk_tooltips_set_tip((gtkconv -> tooltips),(gtkchat -> topic_text),((topic != 0)?topic : ""),0);
    }
  }
  if ((fields & PIDGIN_CONV_SMILEY_THEME) != 0U) 
    pidgin_themes_smiley_themeize(( *((PidginConversation *)(conv -> ui_data))).imhtml);
  if ((((fields & PIDGIN_CONV_COLORIZE_TITLE) != 0U) || ((fields & PIDGIN_CONV_SET_TITLE) != 0U)) || ((fields & PIDGIN_CONV_TOPIC) != 0U)) {
    char *title;
    PurpleConvIm *im = (PurpleConvIm *)((void *)0);
    PurpleAccount *account = purple_conversation_get_account(conv);
    PurpleBuddy *buddy = (PurpleBuddy *)((void *)0);
    char *markup = (char *)((void *)0);
    AtkObject *accessibility_obj;
/* I think this is a little longer than it needs to be but I'm lazy. */
    char *style;
    if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) 
      im = purple_conversation_get_im_data(conv);
    if (((account == ((PurpleAccount *)((void *)0))) || !(purple_account_is_connected(account) != 0)) || (((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) && (purple_conv_chat_has_left(purple_conversation_get_chat_data(conv)) != 0))) 
      title = g_strdup_printf("(%s)",purple_conversation_get_title(conv));
    else 
      title = g_strdup(purple_conversation_get_title(conv));
    if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
      buddy = purple_find_buddy(account,(conv -> name));
      if (buddy != 0) {
        markup = pidgin_blist_get_name_markup(buddy,0,0);
      }
      else {
        markup = title;
      }
    }
    else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) {
      const char *topic = (( *gtkconv -> u.chat).topic_text != 0)?gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)( *gtkconv -> u.chat).topic_text),gtk_entry_get_type())))) : ((const char *)((void *)0));
      char *esc = (char *)((void *)0);
      char *tmp;
      esc = ((topic != 0)?g_markup_escape_text(topic,(-1)) : ((char *)((void *)0)));
      tmp = g_markup_escape_text(purple_conversation_get_title(conv),(-1));
      markup = g_strdup_printf("%s%s<span color=\'%s\' size=\'smaller\'>%s</span>",tmp,(((esc != 0) && (( *esc) != 0))?"\n" : ""),pidgin_get_dim_grey_string((gtkconv -> infopane)),((esc != 0)?esc : ""));
      g_free(tmp);
      g_free(esc);
    }
    gtk_list_store_set((gtkconv -> infopane_model),&gtkconv -> infopane_iter,CONV_TEXT_COLUMN,markup,-1);
/* XXX seanegan Why do I have to do this? */
    gtk_widget_queue_draw((gtkconv -> infopane));
    if (title != markup) 
      g_free(markup);
    if (!((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tab_label)),gtk_object_get_type())))).flags & GTK_REALIZED) != 0)) 
      gtk_widget_realize((gtkconv -> tab_label));
    accessibility_obj = gtk_widget_get_accessible((gtkconv -> tab_cont));
    if ((im != ((PurpleConvIm *)((void *)0))) && ((purple_conv_im_get_typing_state(im)) == PURPLE_TYPING)) {
      atk_object_set_description(accessibility_obj,((const char *)(dgettext("pidgin","Typing"))));
      style = "tab-label-typing";
    }
    else if ((im != ((PurpleConvIm *)((void *)0))) && ((purple_conv_im_get_typing_state(im)) == PURPLE_TYPED)) {
      atk_object_set_description(accessibility_obj,((const char *)(dgettext("pidgin","Stopped Typing"))));
      style = "tab-label-typed";
    }
    else if ((gtkconv -> unseen_state) == PIDGIN_UNSEEN_NICK) {
      atk_object_set_description(accessibility_obj,((const char *)(dgettext("pidgin","Nick Said"))));
      style = "tab-label-attention";
    }
    else if ((gtkconv -> unseen_state) == PIDGIN_UNSEEN_TEXT) {
      atk_object_set_description(accessibility_obj,((const char *)(dgettext("pidgin","Unread Messages"))));
      if (( *(gtkconv -> active_conv)).type == PURPLE_CONV_TYPE_CHAT) 
        style = "tab-label-unreadchat";
      else 
        style = "tab-label-attention";
    }
    else if ((gtkconv -> unseen_state) == PIDGIN_UNSEEN_EVENT) {
      atk_object_set_description(accessibility_obj,((const char *)(dgettext("pidgin","New Event"))));
      style = "tab-label-event";
    }
    else {
      style = "tab-label";
    }
    gtk_widget_set_name((gtkconv -> tab_label),style);
    gtk_label_set_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tab_label)),gtk_label_get_type()))),title);
    gtk_widget_set_state((gtkconv -> tab_label),GTK_STATE_ACTIVE);
    if ((((gtkconv -> unseen_state) == PIDGIN_UNSEEN_TEXT) || ((gtkconv -> unseen_state) == PIDGIN_UNSEEN_NICK)) || ((gtkconv -> unseen_state) == PIDGIN_UNSEEN_EVENT)) {
      PangoAttrList *list = pango_attr_list_new();
      PangoAttribute *attr = pango_attr_weight_new(PANGO_WEIGHT_BOLD);
      attr -> start_index = 0;
      attr -> end_index = (-1);
      pango_attr_list_insert(list,attr);
      gtk_label_set_attributes(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tab_label)),gtk_label_get_type()))),list);
      pango_attr_list_unref(list);
    }
    else 
      gtk_label_set_attributes(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tab_label)),gtk_label_get_type()))),0);
    if (pidgin_conv_window_is_active_conversation(conv) != 0) 
      update_typing_icon(gtkconv);
    gtk_label_set_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> menu_label)),gtk_label_get_type()))),title);
    if (pidgin_conv_window_is_active_conversation(conv) != 0) {
      const char *current_title = gtk_window_get_title(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),gtk_window_get_type()))));
      if ((current_title == ((const char *)((void *)0))) || (strcmp(current_title,title) != 0)) 
        gtk_window_set_title(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),gtk_window_get_type()))),title);
    }
    g_free(title);
  }
}

static void pidgin_conv_updated(PurpleConversation *conv,PurpleConvUpdateType type)
{
  PidginConvFields flags = 0;
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  if (type == PURPLE_CONV_UPDATE_ACCOUNT) {
    flags = ((1 << 7) - 1);
  }
  else if (((type == PURPLE_CONV_UPDATE_TYPING) || (type == PURPLE_CONV_UPDATE_UNSEEN)) || (type == PURPLE_CONV_UPDATE_TITLE)) {
    flags = PIDGIN_CONV_COLORIZE_TITLE;
  }
  else if (type == PURPLE_CONV_UPDATE_TOPIC) {
    flags = PIDGIN_CONV_TOPIC;
  }
  else if ((type == PURPLE_CONV_ACCOUNT_ONLINE) || (type == PURPLE_CONV_ACCOUNT_OFFLINE)) {
    flags = (PIDGIN_CONV_MENU | PIDGIN_CONV_TAB_ICON | PIDGIN_CONV_SET_TITLE);
  }
  else if (type == PURPLE_CONV_UPDATE_AWAY) {
    flags = PIDGIN_CONV_TAB_ICON;
  }
  else if (((type == PURPLE_CONV_UPDATE_ADD) || (type == PURPLE_CONV_UPDATE_REMOVE)) || (type == PURPLE_CONV_UPDATE_CHATLEFT)) {
    flags = (PIDGIN_CONV_SET_TITLE | PIDGIN_CONV_MENU);
  }
  else if (type == PURPLE_CONV_UPDATE_ICON) {
    flags = PIDGIN_CONV_BUDDY_ICON;
  }
  else if (type == PURPLE_CONV_UPDATE_FEATURES) {
    flags = PIDGIN_CONV_MENU;
  }
  pidgin_conv_update_fields(conv,flags);
}

static void wrote_msg_update_unseen_cb(PurpleAccount *account,const char *who,const char *message,PurpleConversation *conv,PurpleMessageFlags flags,gpointer null)
{
  PidginConversation *gtkconv = (conv != 0)?((PidginConversation *)(conv -> ui_data)) : ((struct _PidginConversation *)((void *)0));
  if ((conv == ((PurpleConversation *)((void *)0))) || ((gtkconv != 0) && ((gtkconv -> win) != hidden_convwin))) 
    return ;
  if ((flags & (PURPLE_MESSAGE_SEND | PURPLE_MESSAGE_RECV)) != 0U) {
    PidginUnseenState unseen = PIDGIN_UNSEEN_NONE;
    if ((flags & PURPLE_MESSAGE_NICK) == PURPLE_MESSAGE_NICK) 
      unseen = PIDGIN_UNSEEN_NICK;
    else if (((flags & PURPLE_MESSAGE_SYSTEM) == PURPLE_MESSAGE_SYSTEM) || ((flags & PURPLE_MESSAGE_ERROR) == PURPLE_MESSAGE_ERROR)) 
      unseen = PIDGIN_UNSEEN_EVENT;
    else if ((flags & PURPLE_MESSAGE_NO_LOG) == PURPLE_MESSAGE_NO_LOG) 
      unseen = PIDGIN_UNSEEN_NO_LOG;
    else 
      unseen = PIDGIN_UNSEEN_TEXT;
    conv_set_unseen(conv,unseen);
  }
}
static PurpleConversationUiOps conversation_ui_ops = {(pidgin_conv_new), (pidgin_conv_destroy), ((void (*)(PurpleConversation *, const char *, const char *, PurpleMessageFlags , time_t ))((void *)0)), (pidgin_conv_write_im), (pidgin_conv_write_conv), (pidgin_conv_chat_add_users), (pidgin_conv_chat_rename_user), (pidgin_conv_chat_remove_users), (pidgin_conv_chat_update_user), (pidgin_conv_present_conversation), (pidgin_conv_has_focus), (pidgin_conv_custom_smiley_add), (pidgin_conv_custom_smiley_write), (pidgin_conv_custom_smiley_close), (pidgin_conv_send_confirm), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* destroy_conversation */
/* write_chat           */
/* write_im             */
/* write_conv           */
/* chat_add_users       */
/* chat_rename_user     */
/* chat_remove_users    */
/* chat_update_user     */
/* present              */
/* has_focus            */
/* custom_smiley_add    */
/* custom_smiley_write  */
/* custom_smiley_close  */
/* send_confirm         */
};

PurpleConversationUiOps *pidgin_conversations_get_conv_ui_ops()
{
  return &conversation_ui_ops;
}
/**************************************************************************
 * Public conversation utility functions
 **************************************************************************/

void pidgin_conv_update_buddy_icon(PurpleConversation *conv)
{
  PidginConversation *gtkconv;
  PidginWindow *win;
  PurpleBuddy *buddy;
  PurpleStoredImage *custom_img = (PurpleStoredImage *)((void *)0);
  gconstpointer data = (gconstpointer )((void *)0);
  size_t len;
  GdkPixbuf *buf;
  GList *children;
  GtkWidget *event;
  GdkPixbuf *scale;
  int scale_width;
  int scale_height;
  int size = 0;
  PurpleAccount *account;
  PurpleBuddyIcon *icon;
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  do {
    if (purple_conversation_get_ui_ops(conv) == pidgin_conversations_get_conv_ui_ops()) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_PIDGIN_CONVERSATION(conv)");
      return ;
    };
  }while (0);
  do {
    if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_conversation_get_type(conv) == PURPLE_CONV_TYPE_IM");
      return ;
    };
  }while (0);
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  win = (gtkconv -> win);
  if (conv != (gtkconv -> active_conv)) 
    return ;
  if (!(( *gtkconv -> u.im).show_icon != 0)) 
    return ;
  account = purple_conversation_get_account(conv);
/* Remove the current icon stuff */
  children = gtk_container_get_children(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *gtkconv -> u.im).icon_container),gtk_container_get_type()))));
  if (children != 0) {
/* We know there's only one child here. It'd be nice to shortcut to the
		   event box, but we can't change the PidginConversation until 3.0 */
    event = ((GtkWidget *)(children -> data));
    gtk_container_remove(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *gtkconv -> u.im).icon_container),gtk_container_get_type()))),event);
    g_list_free(children);
  }
  if (( *gtkconv -> u.im).anim != ((GdkPixbufAnimation *)((void *)0))) 
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *gtkconv -> u.im).anim),((GType )(20 << 2))))));
  ( *gtkconv -> u.im).anim = ((GdkPixbufAnimation *)((void *)0));
  if (( *gtkconv -> u.im).icon_timer != 0) 
    g_source_remove(( *gtkconv -> u.im).icon_timer);
  ( *gtkconv -> u.im).icon_timer = 0;
  if (( *gtkconv -> u.im).iter != ((GdkPixbufAnimationIter *)((void *)0))) 
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *gtkconv -> u.im).iter),((GType )(20 << 2))))));
  ( *gtkconv -> u.im).iter = ((GdkPixbufAnimationIter *)((void *)0));
  if (!(purple_prefs_get_bool("/pidgin/conversations/im/show_buddy_icons") != 0)) 
    return ;
  if (purple_conversation_get_gc(conv) == ((PurpleConnection *)((void *)0))) 
    return ;
  buddy = purple_find_buddy(account,purple_conversation_get_name(conv));
  if (buddy != 0) {
    PurpleContact *contact = purple_buddy_get_contact(buddy);
    if (contact != 0) {
      custom_img = purple_buddy_icons_node_find_custom_icon(((PurpleBlistNode *)contact));
      if (custom_img != 0) {
/* There is a custom icon for this user */
        data = purple_imgstore_get_data(custom_img);
        len = purple_imgstore_get_size(custom_img);
      }
    }
  }
  if (data == ((const void *)((void *)0))) {
    icon = purple_conv_im_get_icon((purple_conversation_get_im_data(conv)));
    if (icon == ((PurpleBuddyIcon *)((void *)0))) {
      gtk_widget_set_size_request(( *gtkconv -> u.im).icon_container,(-1),32);
      return ;
    }
    data = purple_buddy_icon_get_data(icon,&len);
    if (data == ((const void *)((void *)0))) {
      gtk_widget_set_size_request(( *gtkconv -> u.im).icon_container,(-1),32);
      return ;
    }
  }
  ( *gtkconv -> u.im).anim = pidgin_pixbuf_anim_from_data(data,len);
  purple_imgstore_unref(custom_img);
  if (!(( *gtkconv -> u.im).anim != 0)) {
    purple_debug_error("gtkconv","Couldn\'t load icon for conv %s\n",purple_conversation_get_name(conv));
    return ;
  }
  if (gdk_pixbuf_animation_is_static_image(( *gtkconv -> u.im).anim) != 0) {
    GdkPixbuf *stat;
    ( *gtkconv -> u.im).iter = ((GdkPixbufAnimationIter *)((void *)0));
    stat = gdk_pixbuf_animation_get_static_image(( *gtkconv -> u.im).anim);
    buf = gdk_pixbuf_add_alpha(stat,0,0,0,0);
  }
  else {
    GdkPixbuf *stat;
    ( *gtkconv -> u.im).iter = gdk_pixbuf_animation_get_iter(( *gtkconv -> u.im).anim,0);
/* LEAK */
    stat = gdk_pixbuf_animation_iter_get_pixbuf(( *gtkconv -> u.im).iter);
    buf = gdk_pixbuf_add_alpha(stat,0,0,0,0);
    if (( *gtkconv -> u.im).animate != 0) 
      start_anim(0,gtkconv);
  }
  scale_width = gdk_pixbuf_get_width(buf);
  scale_height = gdk_pixbuf_get_height(buf);
  gtk_widget_get_size_request(( *gtkconv -> u.im).icon_container,0,&size);
  size = ((size < (((scale_width < scale_height)?scale_width : scale_height)))?size : (((scale_width < scale_height)?scale_width : scale_height)));
/* Some sanity checks */
  size = ((size > 96)?96 : (((size < 32)?32 : size)));
  if (scale_width == scale_height) {
    scale_width = (scale_height = size);
  }
  else if (scale_height > scale_width) {
    scale_width = ((size * scale_width) / scale_height);
    scale_height = size;
  }
  else {
    scale_height = ((size * scale_height) / scale_width);
    scale_width = size;
  }
  scale = gdk_pixbuf_scale_simple(buf,scale_width,scale_height,GDK_INTERP_BILINEAR);
  g_object_unref(buf);
  if (pidgin_gdk_pixbuf_is_opaque(scale) != 0) 
    pidgin_gdk_pixbuf_make_round(scale);
  event = gtk_event_box_new();
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *gtkconv -> u.im).icon_container),gtk_container_get_type()))),event);
  gtk_event_box_set_visible_window(((GtkEventBox *)(g_type_check_instance_cast(((GTypeInstance *)event),gtk_event_box_get_type()))),0);
  gtk_widget_add_events(event,(GDK_POINTER_MOTION_MASK | GDK_LEAVE_NOTIFY_MASK));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)event),((GType )(20 << 2))))),"button-press-event",((GCallback )icon_menu),gtkconv,0,((GConnectFlags )0));
  pidgin_tooltip_setup_for_widget(event,gtkconv,pidgin_conv_create_tooltip,0);
  gtk_widget_show(event);
  ( *gtkconv -> u.im).icon = gtk_image_new_from_pixbuf(scale);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)event),gtk_container_get_type()))),( *gtkconv -> u.im).icon);
  gtk_widget_show(( *gtkconv -> u.im).icon);
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)scale),((GType )(20 << 2))))));
/* The buddy icon code needs badly to be fixed. */
  if (pidgin_conv_window_is_active_conversation(conv) != 0) {
    buf = gdk_pixbuf_animation_get_static_image(( *gtkconv -> u.im).anim);
    if ((buddy != 0) && !(((buddy != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0))) 
      gdk_pixbuf_saturate_and_pixelate(buf,buf,0.0,0);
    gtk_window_set_icon(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),gtk_window_get_type()))),buf);
  }
}

void pidgin_conv_update_buttons_by_protocol(PurpleConversation *conv)
{
  PidginWindow *win;
  if (!(purple_conversation_get_ui_ops(conv) == pidgin_conversations_get_conv_ui_ops())) 
    return ;
  win = ( *((PidginConversation *)(conv -> ui_data))).win;
  if ((win != ((PidginWindow *)((void *)0))) && (pidgin_conv_window_is_active_conversation(conv) != 0)) 
    gray_stuff_out(((PidginConversation *)(conv -> ui_data)));
}

static gboolean pidgin_conv_xy_to_right_infopane(PidginWindow *win,int x,int y)
{
  gint pane_x;
  gint pane_y;
  gint x_rel;
  PidginConversation *gtkconv;
  gdk_window_get_origin(( *(win -> notebook)).window,&pane_x,&pane_y);
  x_rel = (x - pane_x);
  gtkconv = pidgin_conv_window_get_active_gtkconv(win);
  return x_rel > (( *(gtkconv -> infopane)).allocation.x + (( *(gtkconv -> infopane)).allocation.width / 2));
}

int pidgin_conv_get_tab_at_xy(PidginWindow *win,int x,int y,gboolean *to_right)
{
  gint nb_x;
  gint nb_y;
  gint x_rel;
  gint y_rel;
  GtkNotebook *notebook;
  GtkWidget *page;
  GtkWidget *tab;
  gint i;
  gint page_num = (-1);
  gint count;
  gboolean horiz;
  if (to_right != 0) 
     *to_right = 0;
  notebook = ((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type())));
  gdk_window_get_origin(( *(win -> notebook)).window,&nb_x,&nb_y);
  x_rel = (x - nb_x);
  y_rel = (y - nb_y);
  horiz = (((gtk_notebook_get_tab_pos(notebook)) == GTK_POS_TOP) || ((gtk_notebook_get_tab_pos(notebook)) == GTK_POS_BOTTOM));
  count = gtk_notebook_get_n_pages(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)notebook),gtk_notebook_get_type()))));
{
    for (i = 0; i < count; i++) {
      page = gtk_notebook_get_nth_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)notebook),gtk_notebook_get_type()))),i);
      tab = gtk_notebook_get_tab_label(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)notebook),gtk_notebook_get_type()))),page);
/* Make sure the tab is not hidden beyond an arrow */
      if (!(((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)tab),gtk_object_get_type())))).flags & GTK_VISIBLE) != 0) && ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)tab),gtk_object_get_type())))).flags & GTK_MAPPED) != 0)) && (gtk_notebook_get_show_tabs(notebook) != 0)) 
        continue; 
      if (horiz != 0) {
        if ((x_rel >= (tab -> allocation.x - 6)) && (x_rel <= ((tab -> allocation.x + tab -> allocation.width) + 6))) {
          page_num = i;
          if ((to_right != 0) && (x_rel >= (tab -> allocation.x + (tab -> allocation.width / 2)))) 
             *to_right = (!0);
          break; 
        }
      }
      else {
        if ((y_rel >= (tab -> allocation.y - 6)) && (y_rel <= ((tab -> allocation.y + tab -> allocation.height) + 6))) {
          page_num = i;
          if ((to_right != 0) && (y_rel >= (tab -> allocation.y + (tab -> allocation.height / 2)))) 
             *to_right = (!0);
          break; 
        }
      }
    }
  }
  if (page_num == -1) {
/* Add after the last tab */
    page_num = (count - 1);
  }
  return page_num;
}

static void close_on_tabs_pref_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  GList *l;
  PurpleConversation *conv;
  PidginConversation *gtkconv;
  for (l = purple_get_conversations(); l != ((GList *)((void *)0)); l = (l -> next)) {
    conv = ((PurpleConversation *)(l -> data));
    if (!(purple_conversation_get_ui_ops(conv) == pidgin_conversations_get_conv_ui_ops())) 
      continue; 
    gtkconv = ((PidginConversation *)(conv -> ui_data));
    if (value != 0) 
      gtk_widget_show((gtkconv -> close));
    else 
      gtk_widget_hide((gtkconv -> close));
  }
}

static void spellcheck_pref_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
#ifdef USE_GTKSPELL
  GList *cl;
  PurpleConversation *conv;
  PidginConversation *gtkconv;
  GtkSpell *spell;
  for (cl = purple_get_conversations(); cl != ((GList *)((void *)0)); cl = (cl -> next)) {
    conv = ((PurpleConversation *)(cl -> data));
    if (!(purple_conversation_get_ui_ops(conv) == pidgin_conversations_get_conv_ui_ops())) 
      continue; 
    gtkconv = ((PidginConversation *)(conv -> ui_data));
    if (value != 0) 
      pidgin_setup_gtkspell(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_text_view_get_type()))));
    else {
      spell = gtkspell_get_from_text_view(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_text_view_get_type()))));
      if (spell != 0) 
        gtkspell_detach(spell);
    }
  }
#endif
}

static void tab_side_pref_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  GList *gtkwins;
  GList *gtkconvs;
  GtkPositionType pos;
  PidginWindow *gtkwin;
  pos = ((gint )((glong )value));
  for (gtkwins = pidgin_conv_windows_get_list(); gtkwins != ((GList *)((void *)0)); gtkwins = (gtkwins -> next)) {
    gtkwin = (gtkwins -> data);
    gtk_notebook_set_tab_pos(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(gtkwin -> notebook)),gtk_notebook_get_type()))),(pos & (~8)));
    for (gtkconvs = (gtkwin -> gtkconvs); gtkconvs != ((GList *)((void *)0)); gtkconvs = (gtkconvs -> next)) {
      pidgin_conv_tab_pack(gtkwin,(gtkconvs -> data));
    }
  }
}

static void show_timestamps_pref_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  GList *l;
  PurpleConversation *conv;
  PidginConversation *gtkconv;
  PidginWindow *win;
  for (l = purple_get_conversations(); l != ((GList *)((void *)0)); l = (l -> next)) {
    conv = ((PurpleConversation *)(l -> data));
    if (!(purple_conversation_get_ui_ops(conv) == pidgin_conversations_get_conv_ui_ops())) 
      continue; 
    gtkconv = ((PidginConversation *)(conv -> ui_data));
    win = (gtkconv -> win);
    gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)win -> menu.show_timestamps),gtk_check_menu_item_get_type()))),((gboolean )((glong )value)));
    gtk_imhtml_show_comments(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),((gboolean )((glong )value)));
  }
}

static void show_formatting_toolbar_pref_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  GList *l;
  PurpleConversation *conv;
  PidginConversation *gtkconv;
  PidginWindow *win;
  for (l = purple_get_conversations(); l != ((GList *)((void *)0)); l = (l -> next)) {
    conv = ((PurpleConversation *)(l -> data));
    if (!(purple_conversation_get_ui_ops(conv) == pidgin_conversations_get_conv_ui_ops())) 
      continue; 
    gtkconv = ((PidginConversation *)(conv -> ui_data));
    win = (gtkconv -> win);
    gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)win -> menu.show_formatting_toolbar),gtk_check_menu_item_get_type()))),((gboolean )((glong )value)));
    if (((gboolean )((glong )value)) != 0) 
      gtk_widget_show((gtkconv -> toolbar));
    else 
      gtk_widget_hide((gtkconv -> toolbar));
    g_idle_add(((GSourceFunc )resize_imhtml_cb),gtkconv);
  }
}

static void animate_buddy_icons_pref_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  GList *l;
  PurpleConversation *conv;
  PidginConversation *gtkconv;
  PidginWindow *win;
  if (!(purple_prefs_get_bool("/pidgin/conversations/im/show_buddy_icons") != 0)) 
    return ;
/* Set the "animate" flag for each icon based on the new preference */
  for (l = purple_get_ims(); l != ((GList *)((void *)0)); l = (l -> next)) {
    conv = ((PurpleConversation *)(l -> data));
    gtkconv = ((PidginConversation *)(conv -> ui_data));
    if (gtkconv != 0) 
      ( *gtkconv -> u.im).animate = ((gint )((glong )value));
  }
/* Now either stop or start animation for the active conversation in each window */
  for (l = pidgin_conv_windows_get_list(); l != ((GList *)((void *)0)); l = (l -> next)) {
    win = (l -> data);
    conv = pidgin_conv_window_get_active_conversation(win);
    pidgin_conv_update_buddy_icon(conv);
  }
}

static void show_buddy_icons_pref_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  GList *l;
  for (l = purple_get_conversations(); l != ((GList *)((void *)0)); l = (l -> next)) {{
      PurpleConversation *conv = (l -> data);
      if (!(((PidginConversation *)(conv -> ui_data)) != 0)) 
        continue; 
      if (((gint )((glong )value)) != 0) 
        gtk_widget_show(( *((PidginConversation *)(conv -> ui_data))).infopane_hbox);
      else 
        gtk_widget_hide(( *((PidginConversation *)(conv -> ui_data))).infopane_hbox);
      if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
        pidgin_conv_update_buddy_icon(conv);
      }
    }
  }
/* Make the tabs show/hide correctly */
  for (l = pidgin_conv_windows_get_list(); l != ((GList *)((void *)0)); l = (l -> next)) {
    PidginWindow *win = (l -> data);
    if (pidgin_conv_window_get_gtkconv_count(win) == 1) 
      gtk_notebook_set_show_tabs(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),(((gint )((glong )value)) == 0));
  }
}

static void show_protocol_icons_pref_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  GList *l;
  for (l = purple_get_conversations(); l != ((GList *)((void *)0)); l = (l -> next)) {
    PurpleConversation *conv = (l -> data);
    if (((PidginConversation *)(conv -> ui_data)) != 0) 
      update_tab_icon(conv);
  }
}

static void conv_placement_usetabs_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  purple_prefs_trigger_callback("/pidgin/conversations/placement");
}

static void account_status_changed_cb(PurpleAccount *account,PurpleStatus *oldstatus,PurpleStatus *newstatus)
{
  GList *l;
  PurpleConversation *conv = (PurpleConversation *)((void *)0);
  PidginConversation *gtkconv;
  if (strcmp(purple_prefs_get_string("/pidgin/conversations/im/hide_new"),"away") != 0) 
    return ;
  if ((purple_status_is_available(oldstatus) != 0) || !(purple_status_is_available(newstatus) != 0)) 
    return ;
  for (l = (hidden_convwin -> gtkconvs); l != 0; ) {
    gtkconv = (l -> data);
    l = (l -> next);
    conv = (gtkconv -> active_conv);
    if (((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) || (account != purple_conversation_get_account(conv))) 
      continue; 
    pidgin_conv_attach_to_conversation(conv);
/* TODO: do we need to do anything for any other conversations that are in the same gtkconv here?
		 * I'm a little concerned that not doing so will cause the "pending" indicator in the gtkblist not to be cleared. -DAA*/
    purple_conversation_update(conv,PURPLE_CONV_UPDATE_UNSEEN);
  }
}

static void hide_new_pref_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  GList *l;
  PurpleConversation *conv = (PurpleConversation *)((void *)0);
  PidginConversation *gtkconv;
  gboolean when_away = 0;
  if (!(hidden_convwin != 0)) 
    return ;
  if (strcmp(purple_prefs_get_string("/pidgin/conversations/im/hide_new"),"always") == 0) 
    return ;
  if (strcmp(purple_prefs_get_string("/pidgin/conversations/im/hide_new"),"away") == 0) 
    when_away = (!0);
  for (l = (hidden_convwin -> gtkconvs); l != 0; ) {
    gtkconv = (l -> data);
    l = (l -> next);
    conv = (gtkconv -> active_conv);
    if ((((conv -> type) == PURPLE_CONV_TYPE_CHAT) || ((gtkconv -> unseen_count) == 0)) || ((when_away != 0) && !(purple_status_is_available((purple_account_get_active_status((purple_conversation_get_account(conv))))) != 0))) 
      continue; 
    pidgin_conv_attach_to_conversation(conv);
  }
}

static void conv_placement_pref_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  PidginConvPlacementFunc func;
  if (strcmp(name,"/pidgin/conversations/placement") != 0) 
    return ;
  func = pidgin_conv_placement_get_fnc(value);
  if (func == ((void (*)(PidginConversation *))((void *)0))) 
    return ;
  pidgin_conv_placement_set_current_func(func);
}

static PidginConversation *get_gtkconv_with_contact(PurpleContact *contact)
{
  PurpleBlistNode *node;
  node = ( *((PurpleBlistNode *)contact)).child;
  for (; node != 0; node = (node -> next)) {
    PurpleBuddy *buddy = (PurpleBuddy *)node;
    PurpleConversation *conv;
    conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,(buddy -> name),(buddy -> account));
    if (conv != 0) 
      return (PidginConversation *)(conv -> ui_data);
  }
  return 0;
}

static void account_signed_off_cb(PurpleConnection *gc,gpointer event)
{
  GList *iter;
  for (iter = purple_get_conversations(); iter != 0; iter = (iter -> next)) {
    PurpleConversation *conv = (iter -> data);
/* This seems fine in theory, but we also need to cover the
		 * case of this account matching one of the other buddies in
		 * one of the contacts containing the buddy corresponding to
		 * a conversation.  It's easier to just update them all. */
/* if (purple_conversation_get_account(conv) == account) */
    pidgin_conv_update_fields(conv,(PIDGIN_CONV_TAB_ICON | PIDGIN_CONV_MENU | PIDGIN_CONV_COLORIZE_TITLE));
    if (((((purple_connection_get_state(gc)) == PURPLE_CONNECTED) && ((conv -> type) == PURPLE_CONV_TYPE_CHAT)) && ((conv -> account) == (gc -> account))) && (purple_conversation_get_data(conv,"want-to-rejoin") != 0)) {
      GHashTable *comps = (GHashTable *)((void *)0);
      PurpleChat *chat = purple_blist_find_chat((conv -> account),(conv -> name));
      if (chat == ((PurpleChat *)((void *)0))) {
        if (( *((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info)).chat_info_defaults != ((GHashTable *(*)(PurpleConnection *, const char *))((void *)0))) 
          comps = ( *( *((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info)).chat_info_defaults)(gc,(conv -> name));
      }
      else {
        comps = (chat -> components);
      }
      serv_join_chat(gc,comps);
      if ((chat == ((PurpleChat *)((void *)0))) && (comps != ((GHashTable *)((void *)0)))) 
        g_hash_table_destroy(comps);
    }
  }
}

static void account_signing_off(PurpleConnection *gc)
{
  GList *list = purple_get_chats();
  PurpleAccount *account = purple_connection_get_account(gc);
/* We are about to sign off. See which chats we are currently in, and mark
	 * them for rejoin on reconnect. */
  while(list != 0){
    PurpleConversation *conv = (list -> data);
    if (!(purple_conv_chat_has_left(purple_conversation_get_chat_data(conv)) != 0) && (purple_conversation_get_account(conv) == account)) {
      purple_conversation_set_data(conv,"want-to-rejoin",((gpointer )((gpointer )((glong )(!0)))));
      purple_conversation_write(conv,0,((const char *)(dgettext("pidgin","The account has disconnected and you are no longer in this chat. You will automatically rejoin the chat when the account reconnects."))),PURPLE_MESSAGE_SYSTEM,time(0));
    }
    list = (list -> next);
  }
}

static void update_buddy_status_changed(PurpleBuddy *buddy,PurpleStatus *old,PurpleStatus *newstatus)
{
  PidginConversation *gtkconv;
  PurpleConversation *conv;
  gtkconv = get_gtkconv_with_contact(purple_buddy_get_contact(buddy));
  if (gtkconv != 0) {
    conv = (gtkconv -> active_conv);
    pidgin_conv_update_fields(conv,(PIDGIN_CONV_TAB_ICON | PIDGIN_CONV_COLORIZE_TITLE | PIDGIN_CONV_BUDDY_ICON));
    if ((purple_status_is_online(old) ^ purple_status_is_online(newstatus)) != 0) 
      pidgin_conv_update_fields(conv,PIDGIN_CONV_MENU);
  }
}

static void update_buddy_privacy_changed(PurpleBuddy *buddy)
{
  PidginConversation *gtkconv;
  PurpleConversation *conv;
  gtkconv = get_gtkconv_with_contact(purple_buddy_get_contact(buddy));
  if (gtkconv != 0) {
    conv = (gtkconv -> active_conv);
    pidgin_conv_update_fields(conv,(PIDGIN_CONV_TAB_ICON | PIDGIN_CONV_MENU));
  }
}

static void update_buddy_idle_changed(PurpleBuddy *buddy,gboolean old,gboolean newidle)
{
  PurpleConversation *conv;
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,(buddy -> name),(buddy -> account));
  if (conv != 0) 
    pidgin_conv_update_fields(conv,PIDGIN_CONV_TAB_ICON);
}

static void update_buddy_icon(PurpleBuddy *buddy)
{
  PurpleConversation *conv;
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,(buddy -> name),(buddy -> account));
  if (conv != 0) 
    pidgin_conv_update_fields(conv,PIDGIN_CONV_BUDDY_ICON);
}

static void update_buddy_sign(PurpleBuddy *buddy,const char *which)
{
  PurplePresence *presence;
  PurpleStatus *on;
  PurpleStatus *off;
  presence = purple_buddy_get_presence(buddy);
  if (!(presence != 0)) 
    return ;
  off = purple_presence_get_status(presence,"offline");
  on = purple_presence_get_status(presence,"available");
  if (which[1] == 'f') 
    update_buddy_status_changed(buddy,on,off);
  else 
    update_buddy_status_changed(buddy,off,on);
}

static void update_conversation_switched(PurpleConversation *conv)
{
  pidgin_conv_update_fields(conv,(PIDGIN_CONV_TAB_ICON | PIDGIN_CONV_SET_TITLE | PIDGIN_CONV_MENU | PIDGIN_CONV_BUDDY_ICON));
}

static void update_buddy_typing(PurpleAccount *account,const char *who)
{
  PurpleConversation *conv;
  PidginConversation *gtkconv;
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,who,account);
  if (!(conv != 0)) 
    return ;
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  if ((gtkconv != 0) && ((gtkconv -> active_conv) == conv)) 
    pidgin_conv_update_fields(conv,PIDGIN_CONV_COLORIZE_TITLE);
}

static void update_chat(PurpleConversation *conv)
{
  pidgin_conv_update_fields(conv,(PIDGIN_CONV_TOPIC | PIDGIN_CONV_MENU | PIDGIN_CONV_SET_TITLE));
}

static void update_chat_topic(PurpleConversation *conv,const char *old,const char *new)
{
  pidgin_conv_update_fields(conv,PIDGIN_CONV_TOPIC);
}
/* Message history stuff */
/* Compare two PurpleConvMessage's, according to time in ascending order. */

static int message_compare(gconstpointer p1,gconstpointer p2)
{
  const PurpleConvMessage *m1 = p1;
  const PurpleConvMessage *m2 = p2;
  return (m1 -> when) > (m2 -> when);
}
/* Adds some message history to the gtkconv. This happens in a idle-callback. */

static gboolean add_message_history_to_gtkconv(gpointer data)
{
  PidginConversation *gtkconv = data;
  int count = 0;
  int timer = gtkconv -> attach.timer;
  time_t when = ((gint )((glong )(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),((GType )(20 << 2))))),"attach-start-time"))));
  gboolean im = (( *(gtkconv -> active_conv)).type == PURPLE_CONV_TYPE_IM);
  gtkconv -> attach.timer = 0;
/* XXX: 100 is a random value here */
  while((gtkconv -> attach.current != 0) && (count < 100)){
    PurpleConvMessage *msg = ( *gtkconv -> attach.current).data;
    if ((!(im != 0) && (when != 0L)) && (when < (msg -> when))) {
      gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),"<BR><HR>",0,0);
      g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),((GType )(20 << 2))))),"attach-start-time",0);
    }
    pidgin_conv_write_conv((msg -> conv),(msg -> who),(msg -> alias),(msg -> what),(msg -> flags),(msg -> when));
    if (im != 0) {
      gtkconv -> attach.current = g_list_delete_link(gtkconv -> attach.current,gtkconv -> attach.current);
    }
    else {
      gtkconv -> attach.current = ( *gtkconv -> attach.current).prev;
    }
    count++;
  }
  gtkconv -> attach.timer = timer;
  if (gtkconv -> attach.current != 0) 
    return (!0);
  g_source_remove(gtkconv -> attach.timer);
  gtkconv -> attach.timer = 0;
  if (im != 0) {
/* Print any message that was sent while the old history was being added back. */
    GList *msgs = (GList *)((void *)0);
    GList *iter = (gtkconv -> convs);
    for (; iter != 0; iter = (iter -> next)) {
      PurpleConversation *conv = (iter -> data);
      GList *history = purple_conversation_get_message_history(conv);
      for (; history != 0; history = (history -> next)) {
        PurpleConvMessage *msg = (history -> data);
        if ((msg -> when) > when) 
          msgs = g_list_prepend(msgs,msg);
      }
    }
    msgs = g_list_sort(msgs,message_compare);
    for (; msgs != 0; msgs = g_list_delete_link(msgs,msgs)) {
      PurpleConvMessage *msg = (msgs -> data);
      pidgin_conv_write_conv((msg -> conv),(msg -> who),(msg -> alias),(msg -> what),(msg -> flags),(msg -> when));
    }
    gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> imhtml)),gtk_imhtml_get_type()))),"<BR><HR>",0,0);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),((GType )(20 << 2))))),"attach-start-time",0);
  }
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),((GType )(20 << 2))))),"attach-start-time",0);
  purple_signal_emit(pidgin_conversations_get_handle(),"conversation-displayed",gtkconv);
  return 0;
}

static void pidgin_conv_attach(PurpleConversation *conv)
{
  int timer;
  purple_conversation_set_data(conv,"unseen-count",0);
  purple_conversation_set_data(conv,"unseen-state",0);
  purple_conversation_set_ui_ops(conv,pidgin_conversations_get_conv_ui_ops());
  if (!(((PidginConversation *)(conv -> ui_data)) != 0)) 
    private_gtkconv_new(conv,0);
  timer = ((gint )((glong )(purple_conversation_get_data(conv,"close-timer"))));
  if (timer != 0) {
    purple_timeout_remove(timer);
    purple_conversation_set_data(conv,"close-timer",0);
  }
}

gboolean pidgin_conv_attach_to_conversation(PurpleConversation *conv)
{
  GList *list;
  PidginConversation *gtkconv;
  if (purple_conversation_get_ui_ops(conv) == pidgin_conversations_get_conv_ui_ops()) {
/* This is pretty much always the case now. */
    gtkconv = ((PidginConversation *)(conv -> ui_data));
    if ((gtkconv -> win) != hidden_convwin) 
      return 0;
    pidgin_conv_window_remove_gtkconv(hidden_convwin,gtkconv);
    pidgin_conv_placement_place(gtkconv);
    purple_signal_emit(pidgin_conversations_get_handle(),"conversation-displayed",gtkconv);
    list = (gtkconv -> convs);
    while(list != 0){
      pidgin_conv_attach((list -> data));
      list = (list -> next);
    }
    return (!0);
  }
  pidgin_conv_attach(conv);
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  list = purple_conversation_get_message_history(conv);
  if (list != 0) {{
      switch(purple_conversation_get_type(conv)){
        case PURPLE_CONV_TYPE_IM:
{
{
            GList *convs;
            list = g_list_copy(list);
            for (convs = purple_get_ims(); convs != 0; convs = (convs -> next)) 
              if (((convs -> data) != conv) && (pidgin_conv_find_gtkconv((convs -> data)) == gtkconv)) {
                pidgin_conv_attach((convs -> data));
                list = g_list_concat(list,g_list_copy(purple_conversation_get_message_history((convs -> data))));
              }
            list = g_list_sort(list,message_compare);
            gtkconv -> attach.current = list;
            list = g_list_last(list);
            break; 
          }
        }
        case PURPLE_CONV_TYPE_CHAT:
{
          gtkconv -> attach.current = g_list_last(list);
          break; 
        }
        default:
{
          do {
            g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","gtkconv.c",7847,((const char *)__func__));
            return (!0);
          }while (0);
        }
      }
    }
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),((GType )(20 << 2))))),"attach-start-time",((gpointer )((glong )( *((PurpleConvMessage *)(list -> data))).when)));
    gtkconv -> attach.timer = (g_idle_add(add_message_history_to_gtkconv,gtkconv));
  }
  else {
    purple_signal_emit(pidgin_conversations_get_handle(),"conversation-displayed",gtkconv);
  }
  if ((conv -> type) == PURPLE_CONV_TYPE_CHAT) {
    pidgin_conv_update_fields(conv,PIDGIN_CONV_TOPIC);
    pidgin_conv_chat_add_users(conv,( *purple_conversation_get_chat_data(conv)).in_room,(!0));
  }
  return (!0);
}

void *pidgin_conversations_get_handle()
{
  static int handle;
  return (&handle);
}

void pidgin_conversations_init()
{
  void *handle = pidgin_conversations_get_handle();
  void *blist_handle = purple_blist_get_handle();
/* Conversations */
  purple_prefs_add_none("/pidgin/conversations");
  purple_prefs_add_bool("/pidgin/conversations/use_smooth_scrolling",(!0));
  purple_prefs_add_bool("/pidgin/conversations/close_on_tabs",(!0));
  purple_prefs_add_bool("/pidgin/conversations/send_bold",0);
  purple_prefs_add_bool("/pidgin/conversations/send_italic",0);
  purple_prefs_add_bool("/pidgin/conversations/send_underline",0);
  purple_prefs_add_bool("/pidgin/conversations/spellcheck",(!0));
  purple_prefs_add_bool("/pidgin/conversations/show_incoming_formatting",(!0));
  purple_prefs_add_bool("/pidgin/conversations/resize_custom_smileys",(!0));
  purple_prefs_add_int("/pidgin/conversations/custom_smileys_size",96);
  purple_prefs_add_int("/pidgin/conversations/minimum_entry_lines",2);
  purple_prefs_add_bool("/pidgin/conversations/show_timestamps",(!0));
  purple_prefs_add_bool("/pidgin/conversations/show_formatting_toolbar",(!0));
  purple_prefs_add_string("/pidgin/conversations/placement","last");
  purple_prefs_add_int("/pidgin/conversations/placement_number",1);
  purple_prefs_add_string("/pidgin/conversations/bgcolor","");
  purple_prefs_add_string("/pidgin/conversations/fgcolor","");
  purple_prefs_add_string("/pidgin/conversations/font_face","");
  purple_prefs_add_int("/pidgin/conversations/font_size",3);
  purple_prefs_add_bool("/pidgin/conversations/tabs",(!0));
  purple_prefs_add_int("/pidgin/conversations/tab_side",GTK_POS_TOP);
  purple_prefs_add_int("/pidgin/conversations/scrollback_lines",4000);
#ifdef _WIN32
#endif
/* Conversations -> Chat */
  purple_prefs_add_none("/pidgin/conversations/chat");
  purple_prefs_add_int("/pidgin/conversations/chat/entry_height",54);
  purple_prefs_add_int("/pidgin/conversations/chat/userlist_width",80);
  purple_prefs_add_int("/pidgin/conversations/chat/x",0);
  purple_prefs_add_int("/pidgin/conversations/chat/y",0);
  purple_prefs_add_int("/pidgin/conversations/chat/width",340);
  purple_prefs_add_int("/pidgin/conversations/chat/height",390);
/* Conversations -> IM */
  purple_prefs_add_none("/pidgin/conversations/im");
  purple_prefs_add_int("/pidgin/conversations/im/x",0);
  purple_prefs_add_int("/pidgin/conversations/im/y",0);
  purple_prefs_add_int("/pidgin/conversations/im/width",340);
  purple_prefs_add_int("/pidgin/conversations/im/height",390);
  purple_prefs_add_bool("/pidgin/conversations/im/animate_buddy_icons",(!0));
  purple_prefs_add_int("/pidgin/conversations/im/entry_height",54);
  purple_prefs_add_bool("/pidgin/conversations/im/show_buddy_icons",(!0));
  purple_prefs_add_string("/pidgin/conversations/im/hide_new","never");
  purple_prefs_add_bool("/pidgin/conversations/im/close_immediately",(!0));
#ifdef _WIN32
#endif
/* Connect callbacks. */
  purple_prefs_connect_callback(handle,"/pidgin/conversations/close_on_tabs",close_on_tabs_pref_cb,0);
  purple_prefs_connect_callback(handle,"/pidgin/conversations/show_timestamps",show_timestamps_pref_cb,0);
  purple_prefs_connect_callback(handle,"/pidgin/conversations/show_formatting_toolbar",show_formatting_toolbar_pref_cb,0);
  purple_prefs_connect_callback(handle,"/pidgin/conversations/spellcheck",spellcheck_pref_cb,0);
  purple_prefs_connect_callback(handle,"/pidgin/conversations/tab_side",tab_side_pref_cb,0);
  purple_prefs_connect_callback(handle,"/pidgin/conversations/tabs",conv_placement_usetabs_cb,0);
  purple_prefs_connect_callback(handle,"/pidgin/conversations/placement",conv_placement_pref_cb,0);
  purple_prefs_trigger_callback("/pidgin/conversations/placement");
  purple_prefs_connect_callback(handle,"/pidgin/conversations/minimum_entry_lines",minimum_entry_lines_pref_cb,0);
/* IM callbacks */
  purple_prefs_connect_callback(handle,"/pidgin/conversations/im/animate_buddy_icons",animate_buddy_icons_pref_cb,0);
  purple_prefs_connect_callback(handle,"/pidgin/conversations/im/show_buddy_icons",show_buddy_icons_pref_cb,0);
  purple_prefs_connect_callback(handle,"/pidgin/blist/show_protocol_icons",show_protocol_icons_pref_cb,0);
  purple_prefs_connect_callback(handle,"/pidgin/conversations/im/hide_new",hide_new_pref_cb,0);
/**********************************************************************
	 * Register signals
	 **********************************************************************/
  purple_signal_register(handle,"conversation-dragging",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_BOXED,"PidginWindow *"),purple_value_new(PURPLE_TYPE_BOXED,"PidginWindow *"));
  purple_signal_register(handle,"conversation-timestamp",purple_marshal_POINTER__POINTER_INT64_BOOLEAN,purple_value_new(PURPLE_TYPE_STRING),3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_INT64),purple_value_new(PURPLE_TYPE_BOOLEAN));
#if SIZEOF_TIME_T == 4
#elif SIZEOF_TIME_T == 8
#else
#error Unkown size of time_t
#endif
#if SIZEOF_TIME_T == 4
#elif SIZEOF_TIME_T == 8
#else
# error Unknown size of time_t
#endif
  purple_signal_register(handle,"displaying-im-msg",purple_marshal_BOOLEAN__POINTER_POINTER_POINTER_POINTER_POINTER,purple_value_new(PURPLE_TYPE_BOOLEAN),5,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING),purple_value_new_outgoing(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_INT));
  purple_signal_register(handle,"displayed-im-msg",purple_marshal_VOID__POINTER_POINTER_POINTER_POINTER_UINT,0,5,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_INT));
  purple_signal_register(handle,"displaying-chat-msg",purple_marshal_BOOLEAN__POINTER_POINTER_POINTER_POINTER_POINTER,purple_value_new(PURPLE_TYPE_BOOLEAN),5,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING),purple_value_new_outgoing(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_INT));
  purple_signal_register(handle,"displayed-chat-msg",purple_marshal_VOID__POINTER_POINTER_POINTER_POINTER_UINT,0,5,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_INT));
  purple_signal_register(handle,"conversation-switched",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION));
  purple_signal_register(handle,"conversation-hiding",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_BOXED,"PidginConversation *"));
  purple_signal_register(handle,"conversation-displayed",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_BOXED,"PidginConversation *"));
  purple_signal_register(handle,"chat-nick-autocomplete",purple_marshal_BOOLEAN__POINTER_BOOLEAN,purple_value_new(PURPLE_TYPE_BOOLEAN),1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION));
  purple_signal_register(handle,"chat-nick-clicked",purple_marshal_BOOLEAN__POINTER_POINTER_UINT,purple_value_new(PURPLE_TYPE_BOOLEAN),3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_UINT));
/**********************************************************************
	 * Register commands
	 **********************************************************************/
  purple_cmd_register("say","S",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM),0,say_command_cb,((const char *)(dgettext("pidgin","say &lt;message&gt;:  Send a message normally as if you weren\'t using a command."))),0);
  purple_cmd_register("me","S",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM),0,me_command_cb,((const char *)(dgettext("pidgin","me &lt;action&gt;:  Send an IRC style action to a buddy or chat."))),0);
  purple_cmd_register("debug","w",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM),0,debug_command_cb,((const char *)(dgettext("pidgin","debug &lt;option&gt;:  Send various debug information to the current conversation."))),0);
  purple_cmd_register("clear","",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM),0,clear_command_cb,((const char *)(dgettext("pidgin","clear: Clears the conversation scrollback."))),0);
  purple_cmd_register("clearall","",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM),0,clearall_command_cb,((const char *)(dgettext("pidgin","clear: Clears all conversation scrollbacks."))),0);
  purple_cmd_register("help","w",PURPLE_CMD_P_DEFAULT,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM | PURPLE_CMD_FLAG_ALLOW_WRONG_ARGS),0,help_command_cb,((const char *)(dgettext("pidgin","help &lt;command&gt;:  Help on a specific command."))),0);
/**********************************************************************
	 * UI operations
	 **********************************************************************/
  purple_signal_connect(purple_connections_get_handle(),"signed-on",handle,((GCallback )account_signed_off_cb),((void *)((gpointer )((glong )PURPLE_CONV_ACCOUNT_ONLINE))));
  purple_signal_connect(purple_connections_get_handle(),"signed-off",handle,((GCallback )account_signed_off_cb),((void *)((gpointer )((glong )PURPLE_CONV_ACCOUNT_OFFLINE))));
  purple_signal_connect(purple_connections_get_handle(),"signing-off",handle,((GCallback )account_signing_off),0);
  purple_signal_connect(purple_conversations_get_handle(),"received-im-msg",handle,((GCallback )received_im_msg_cb),0);
  purple_signal_connect(purple_conversations_get_handle(),"cleared-message-history",handle,((GCallback )clear_conversation_scrollback_cb),0);
  purple_signal_connect(purple_conversations_get_handle(),"deleting-chat-buddy",handle,((GCallback )deleting_chat_buddy_cb),0);
  purple_conversations_set_ui_ops(&conversation_ui_ops);
  hidden_convwin = pidgin_conv_window_new();
  window_list = g_list_remove(window_list,hidden_convwin);
  purple_signal_connect(purple_accounts_get_handle(),"account-status-changed",handle,((PurpleCallback )account_status_changed_cb),0);
/* Callbacks to update a conversation */
  purple_signal_connect(blist_handle,"blist-node-added",handle,((GCallback )buddy_update_cb),0);
  purple_signal_connect(blist_handle,"blist-node-removed",handle,((GCallback )buddy_update_cb),0);
  purple_signal_connect(blist_handle,"buddy-signed-on",handle,((PurpleCallback )update_buddy_sign),"on");
  purple_signal_connect(blist_handle,"buddy-signed-off",handle,((PurpleCallback )update_buddy_sign),"off");
  purple_signal_connect(blist_handle,"buddy-status-changed",handle,((PurpleCallback )update_buddy_status_changed),0);
  purple_signal_connect(blist_handle,"buddy-privacy-changed",handle,((PurpleCallback )update_buddy_privacy_changed),0);
  purple_signal_connect(blist_handle,"buddy-idle-changed",handle,((PurpleCallback )update_buddy_idle_changed),0);
  purple_signal_connect(blist_handle,"buddy-icon-changed",handle,((PurpleCallback )update_buddy_icon),0);
  purple_signal_connect(purple_conversations_get_handle(),"buddy-typing",handle,((PurpleCallback )update_buddy_typing),0);
  purple_signal_connect(purple_conversations_get_handle(),"buddy-typing-stopped",handle,((PurpleCallback )update_buddy_typing),0);
  purple_signal_connect(pidgin_conversations_get_handle(),"conversation-switched",handle,((PurpleCallback )update_conversation_switched),0);
  purple_signal_connect(purple_conversations_get_handle(),"chat-left",handle,((PurpleCallback )update_chat),0);
  purple_signal_connect(purple_conversations_get_handle(),"chat-joined",handle,((PurpleCallback )update_chat),0);
  purple_signal_connect(purple_conversations_get_handle(),"chat-topic-changed",handle,((PurpleCallback )update_chat_topic),0);
  purple_signal_connect_priority(purple_conversations_get_handle(),"conversation-updated",handle,((PurpleCallback )pidgin_conv_updated),0,-9999);
  purple_signal_connect(purple_conversations_get_handle(),"wrote-im-msg",handle,((PurpleCallback )wrote_msg_update_unseen_cb),0);
  purple_signal_connect(purple_conversations_get_handle(),"wrote-chat-msg",handle,((PurpleCallback )wrote_msg_update_unseen_cb),0);
{
/* Set default tab colors */
    GString *str = g_string_new(0);
    GtkSettings *settings = gtk_settings_get_default();
    GtkStyle *parent = gtk_rc_get_style_by_paths(settings,"tab-container.tab-label*",0,((GType )(1 << 2)));
    GtkStyle *now;
    struct __unnamed_class___F0_L8169_C3_L1673R_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_L1673R__scope__stylename__DELIMITER__L1673R_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_L1673R__scope__labelname__DELIMITER__L1673R_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_L1673R__scope__color {
    const char *stylename;
    const char *labelname;
    const char *color;}styles[] = {{("pidgin_tab_label_typing_default"), ("tab-label-typing"), ("#4e9a06")}, {("pidgin_tab_label_typed_default"), ("tab-label-typed"), ("#c4a000")}, {("pidgin_tab_label_attention_default"), ("tab-label-attention"), ("#006aff")}, {("pidgin_tab_label_unreadchat_default"), ("tab-label-unreadchat"), ("#cc0000")}, {("pidgin_tab_label_event_default"), ("tab-label-event"), ("#888a85")}, {((const char *)((void *)0)), ((const char *)((void *)0)), ((const char *)((void *)0))}};
    int iter;
    for (iter = 0; styles[iter].stylename != 0; iter++) {
      now = gtk_rc_get_style_by_paths(settings,styles[iter].labelname,0,((GType )(1 << 2)));
      if ((parent == now) || (((parent != 0) && (now != 0)) && ((parent -> rc_style) == (now -> rc_style)))) {
        g_string_append_printf(str,"style \"%s\" {\nfg[ACTIVE] = \"%s\"\n}\nwidget \"*%s\" style \"%s\"\n",styles[iter].stylename,styles[iter].color,styles[iter].labelname,styles[iter].stylename);
      }
    }
    gtk_rc_parse_string((str -> str));
    g_string_free(str,(!0));
    gtk_rc_reset_styles(settings);
  }
}

void pidgin_conversations_uninit()
{
  purple_prefs_disconnect_by_handle(pidgin_conversations_get_handle());
  purple_signals_disconnect_by_handle(pidgin_conversations_get_handle());
  purple_signals_unregister_by_instance(pidgin_conversations_get_handle());
}
/* down here is where gtkconvwin.c ought to start. except they share like every freaking function,
 * and touch each others' private members all day long */
/**
 * @file gtkconvwin.c GTK+ Conversation Window API
 * @ingroup pidgin
 *
 * pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "pidgin.h"
#include <gdk/gdkkeysyms.h>
#include "account.h"
#include "cmds.h"
#include "debug.h"
#include "imgstore.h"
#include "log.h"
#include "notify.h"
#include "prpl.h"
#include "request.h"
#include "util.h"
#include "gtkdnd-hints.h"
#include "gtkblist.h"
#include "gtkconv.h"
#include "gtkdialogs.h"
#include "gtkmenutray.h"
#include "gtkpounce.h"
#include "gtkprefs.h"
#include "gtkprivacy.h"
#include "gtkutils.h"
#include "pidginstock.h"
#include "gtkimhtml.h"
#include "gtkimhtmltoolbar.h"

static void do_close(GtkWidget *w,int resp,PidginWindow *win)
{
  gtk_widget_destroy(warn_close_dialog);
  warn_close_dialog = ((GtkWidget *)((void *)0));
  if (resp == GTK_RESPONSE_OK) 
    pidgin_conv_window_destroy(win);
}

static void build_warn_close_dialog(PidginWindow *gtkwin)
{
  GtkWidget *label;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *img;
  do {
    if (warn_close_dialog == ((GtkWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"warn_close_dialog == NULL");
      return ;
    };
  }while (0);
  warn_close_dialog = gtk_dialog_new_with_buttons(((const char *)(dgettext("pidgin","Confirm close"))),((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(gtkwin -> window)),gtk_window_get_type()))),GTK_DIALOG_MODAL,"gtk-cancel",GTK_RESPONSE_CANCEL,"gtk-close",GTK_RESPONSE_OK,((void *)((void *)0)));
  gtk_dialog_set_default_response(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)warn_close_dialog),gtk_dialog_get_type()))),GTK_RESPONSE_OK);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)warn_close_dialog),gtk_container_get_type()))),6);
  gtk_window_set_resizable(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)warn_close_dialog),gtk_window_get_type()))),0);
  gtk_dialog_set_has_separator(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)warn_close_dialog),gtk_dialog_get_type()))),0);
/* Setup the outside spacing. */
  vbox = ( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)warn_close_dialog),gtk_dialog_get_type())))).vbox;
  gtk_box_set_spacing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),12);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),6);
  img = gtk_image_new_from_stock("pidgin-dialog-warning",gtk_icon_size_from_name("pidgin-icon-size-tango-huge"));
/* Setup the inner hbox and put the dialog's icon in it. */
  hbox = gtk_hbox_new(0,12);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),hbox);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),img,0,0,0);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)img),gtk_misc_get_type()))),0,0);
/* Setup the right vbox. */
  vbox = gtk_vbox_new(0,12);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_container_get_type()))),vbox);
  label = gtk_label_new(((const char *)(dgettext("pidgin","You have unread messages. Are you sure you want to close the window\?"))));
  gtk_widget_set_size_request(label,350,(-1));
  gtk_label_set_line_wrap(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(!0));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),label,0,0,0);
/* Connect the signals. */
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)warn_close_dialog),((GType )(20 << 2))))),"response",((GCallback )do_close),gtkwin,0,((GConnectFlags )0));
}
/**************************************************************************
 * Callbacks
 **************************************************************************/

static gboolean close_win_cb(GtkWidget *w,GdkEventAny *e,gpointer d)
{
  PidginWindow *win = d;
  GList *l;
/* If there are unread messages then show a warning dialog */
  for (l = pidgin_conv_window_get_gtkconvs(win); l != ((GList *)((void *)0)); l = (l -> next)) {
    PidginConversation *gtkconv = (l -> data);
    if (((purple_conversation_get_type((gtkconv -> active_conv))) == PURPLE_CONV_TYPE_IM) && ((gtkconv -> unseen_state) >= PIDGIN_UNSEEN_TEXT)) {
      build_warn_close_dialog(win);
      gtk_widget_show_all(warn_close_dialog);
      return (!0);
    }
  }
  pidgin_conv_window_destroy(win);
  return (!0);
}

static void conv_set_unseen(PurpleConversation *conv,PidginUnseenState state)
{
  int unseen_count = 0;
  PidginUnseenState unseen_state = PIDGIN_UNSEEN_NONE;
  if (purple_conversation_get_data(conv,"unseen-count") != 0) 
    unseen_count = ((gint )((glong )(purple_conversation_get_data(conv,"unseen-count"))));
  if (purple_conversation_get_data(conv,"unseen-state") != 0) 
    unseen_state = ((gint )((glong )(purple_conversation_get_data(conv,"unseen-state"))));
  if (state == PIDGIN_UNSEEN_NONE) {
    unseen_count = 0;
    unseen_state = PIDGIN_UNSEEN_NONE;
  }
  else {
    if (state >= PIDGIN_UNSEEN_TEXT) 
      unseen_count++;
    if (state > unseen_state) 
      unseen_state = state;
  }
  purple_conversation_set_data(conv,"unseen-count",((gpointer )((glong )unseen_count)));
  purple_conversation_set_data(conv,"unseen-state",((gpointer )((glong )unseen_state)));
  purple_conversation_update(conv,PURPLE_CONV_UPDATE_UNSEEN);
}

static void gtkconv_set_unseen(PidginConversation *gtkconv,PidginUnseenState state)
{
  if (state == PIDGIN_UNSEEN_NONE) {
    gtkconv -> unseen_count = 0;
    gtkconv -> unseen_state = PIDGIN_UNSEEN_NONE;
  }
  else {
    if (state >= PIDGIN_UNSEEN_TEXT) 
      gtkconv -> unseen_count++;
    if (state > (gtkconv -> unseen_state)) 
      gtkconv -> unseen_state = state;
  }
  purple_conversation_set_data((gtkconv -> active_conv),"unseen-count",((gpointer )((glong )(gtkconv -> unseen_count))));
  purple_conversation_set_data((gtkconv -> active_conv),"unseen-state",((gpointer )((glong )(gtkconv -> unseen_state))));
  purple_conversation_update((gtkconv -> active_conv),PURPLE_CONV_UPDATE_UNSEEN);
}
/*
 * When a conversation window is focused, we know the user
 * has looked at it so we know there are no longer unseen
 * messages.
 */

static gboolean focus_win_cb(GtkWidget *w,GdkEventFocus *e,gpointer d)
{
  PidginWindow *win = d;
  PidginConversation *gtkconv = pidgin_conv_window_get_active_gtkconv(win);
  if (gtkconv != 0) 
    gtkconv_set_unseen(gtkconv,PIDGIN_UNSEEN_NONE);
  return 0;
}

static void notebook_init_grab(PidginWindow *gtkwin,GtkWidget *widget)
{
  static GdkCursor *cursor = (GdkCursor *)((void *)0);
  gtkwin -> in_drag = (!0);
  if ((gtkwin -> drag_leave_signal) != 0) {
    g_signal_handler_disconnect(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),(gtkwin -> drag_leave_signal));
    gtkwin -> drag_leave_signal = 0;
  }
  if (cursor == ((GdkCursor *)((void *)0))) 
    cursor = gdk_cursor_new(GDK_FLEUR);
/* Grab the pointer */
  gtk_grab_add((gtkwin -> notebook));
#ifndef _WIN32
/* Currently for win32 GTK+ (as of 2.2.1), gdk_pointer_is_grabbed will
	   always be true after a button press. */
  if (!(gdk_pointer_is_grabbed() != 0)) 
#endif
    gdk_pointer_grab(( *(gtkwin -> notebook)).window,0,(GDK_BUTTON1_MOTION_MASK | GDK_BUTTON_RELEASE_MASK),0,cursor,0L);
}

static gboolean notebook_motion_cb(GtkWidget *widget,GdkEventButton *e,PidginWindow *win)
{
/*
	* Make sure the user moved the mouse far enough for the
	* drag to be initiated.
	*/
  if ((win -> in_predrag) != 0) {
    if (((((e -> x_root) < (win -> drag_min_x)) || ((e -> x_root) >= (win -> drag_max_x))) || ((e -> y_root) < (win -> drag_min_y))) || ((e -> y_root) >= (win -> drag_max_y))) {
      win -> in_predrag = 0;
      notebook_init_grab(win,widget);
    }
  }
  else 
/* Otherwise, draw the arrows. */
{
    PidginWindow *dest_win;
    GtkNotebook *dest_notebook;
    GtkWidget *tab;
    gint page_num;
    gboolean horiz_tabs = 0;
    gboolean to_right = 0;
/* Get the window that the cursor is over. */
    dest_win = pidgin_conv_window_get_at_xy((e -> x_root),(e -> y_root));
    if (dest_win == ((PidginWindow *)((void *)0))) {
      dnd_hints_hide_all();
      return (!0);
    }
    dest_notebook = ((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(dest_win -> notebook)),gtk_notebook_get_type())));
    if (gtk_notebook_get_show_tabs(dest_notebook) != 0) {
      page_num = pidgin_conv_get_tab_at_xy(dest_win,(e -> x_root),(e -> y_root),&to_right);
      to_right = ((to_right != 0) && (win != dest_win));
      tab = ( *pidgin_conv_window_get_gtkconv_at_index(dest_win,page_num)).tabby;
    }
    else {
      page_num = 0;
      to_right = pidgin_conv_xy_to_right_infopane(dest_win,(e -> x_root),(e -> y_root));
      tab = ( *pidgin_conv_window_get_gtkconv_at_index(dest_win,page_num)).infopane_hbox;
    }
    if (((gtk_notebook_get_tab_pos(dest_notebook)) == GTK_POS_TOP) || ((gtk_notebook_get_tab_pos(dest_notebook)) == GTK_POS_BOTTOM)) {
      horiz_tabs = (!0);
    }
    if ((gtk_notebook_get_show_tabs(dest_notebook) == 0) && (win == dest_win)) {
/* dragging a tab from a single-tabbed window over its own window */
      dnd_hints_hide_all();
      return (!0);
    }
    else if (horiz_tabs != 0) {
      if (((((gpointer )win) == ((gpointer )dest_win)) && ((win -> drag_tab) < page_num)) || (to_right != 0)) {
        dnd_hints_show_relative(HINT_ARROW_DOWN,tab,HINT_POSITION_RIGHT,HINT_POSITION_TOP);
        dnd_hints_show_relative(HINT_ARROW_UP,tab,HINT_POSITION_RIGHT,HINT_POSITION_BOTTOM);
      }
      else {
        dnd_hints_show_relative(HINT_ARROW_DOWN,tab,HINT_POSITION_LEFT,HINT_POSITION_TOP);
        dnd_hints_show_relative(HINT_ARROW_UP,tab,HINT_POSITION_LEFT,HINT_POSITION_BOTTOM);
      }
    }
    else {
      if (((((gpointer )win) == ((gpointer )dest_win)) && ((win -> drag_tab) < page_num)) || (to_right != 0)) {
        dnd_hints_show_relative(HINT_ARROW_RIGHT,tab,HINT_POSITION_LEFT,HINT_POSITION_BOTTOM);
        dnd_hints_show_relative(HINT_ARROW_LEFT,tab,HINT_POSITION_RIGHT,HINT_POSITION_BOTTOM);
      }
      else {
        dnd_hints_show_relative(HINT_ARROW_RIGHT,tab,HINT_POSITION_LEFT,HINT_POSITION_TOP);
        dnd_hints_show_relative(HINT_ARROW_LEFT,tab,HINT_POSITION_RIGHT,HINT_POSITION_TOP);
      }
    }
  }
  return (!0);
}

static gboolean notebook_leave_cb(GtkWidget *widget,GdkEventCrossing *e,PidginWindow *win)
{
  if ((win -> in_drag) != 0) 
    return 0;
  if (((((e -> x_root) < (win -> drag_min_x)) || ((e -> x_root) >= (win -> drag_max_x))) || ((e -> y_root) < (win -> drag_min_y))) || ((e -> y_root) >= (win -> drag_max_y))) {
    win -> in_predrag = 0;
    notebook_init_grab(win,widget);
  }
  return (!0);
}
/*
 * THANK YOU GALEON!
 */

static gboolean infopane_press_cb(GtkWidget *widget,GdkEventButton *e,PidginConversation *gtkconv)
{
  if (((e -> type) == GDK_2BUTTON_PRESS) && ((e -> button) == 1)) {
    if (infopane_entry_activate(gtkconv) != 0) 
      return (!0);
  }
  if ((e -> type) != GDK_BUTTON_PRESS) 
    return 0;
  if ((e -> button) == 1) {
    int nb_x;
    int nb_y;
    if (( *(gtkconv -> win)).in_drag != 0) 
      return (!0);
    ( *(gtkconv -> win)).in_predrag = (!0);
    ( *(gtkconv -> win)).drag_tab = gtk_notebook_page_num(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)( *(gtkconv -> win)).notebook),gtk_notebook_get_type()))),(gtkconv -> tab_cont));
    gdk_window_get_origin(( *(gtkconv -> infopane_hbox)).window,&nb_x,&nb_y);
    ( *(gtkconv -> win)).drag_min_x = (( *(gtkconv -> infopane_hbox)).allocation.x + nb_x);
    ( *(gtkconv -> win)).drag_min_y = (( *(gtkconv -> infopane_hbox)).allocation.y + nb_y);
    ( *(gtkconv -> win)).drag_max_x = (( *(gtkconv -> infopane_hbox)).allocation.width + ( *(gtkconv -> win)).drag_min_x);
    ( *(gtkconv -> win)).drag_max_y = (( *(gtkconv -> infopane_hbox)).allocation.height + ( *(gtkconv -> win)).drag_min_y);
    ( *(gtkconv -> win)).drag_motion_signal = (g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *(gtkconv -> win)).notebook),((GType )(20 << 2))))),"motion_notify_event",((GCallback )notebook_motion_cb),(gtkconv -> win),0,((GConnectFlags )0)));
    ( *(gtkconv -> win)).drag_leave_signal = (g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *(gtkconv -> win)).notebook),((GType )(20 << 2))))),"leave_notify_event",((GCallback )notebook_leave_cb),(gtkconv -> win),0,((GConnectFlags )0)));
    return 0;
  }
  if ((e -> button) == 3) {
/* Right click was pressed. Popup the context menu. */
    GtkWidget *menu = gtk_menu_new();
    GtkWidget *sub;
    gboolean populated = populate_menu_with_options(menu,gtkconv,(!0));
    sub = gtk_menu_item_get_submenu(((GtkMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)( *(gtkconv -> win)).menu.send_to),gtk_menu_item_get_type()))));
    if ((sub != 0) && (((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)( *(gtkconv -> win)).menu.send_to),gtk_object_get_type())))).flags & GTK_SENSITIVE) != 0) && ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)( *(gtkconv -> win)).menu.send_to),gtk_object_get_type())))).flags & GTK_PARENT_SENSITIVE) != 0))) {
      GtkWidget *item = gtk_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","S_end To"))));
      if (populated != 0) 
        pidgin_separator(menu);
      gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item);
      gtk_menu_item_set_submenu(((GtkMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_menu_item_get_type()))),sub);
      gtk_widget_show(item);
      gtk_widget_show_all(sub);
    }
    else if (!(populated != 0)) {
      gtk_widget_destroy(menu);
      return 0;
    }
    gtk_widget_show_all(menu);
    gtk_menu_popup(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_get_type()))),0,0,0,0,(e -> button),(e -> time));
    return (!0);
  }
  return 0;
}

static gboolean notebook_press_cb(GtkWidget *widget,GdkEventButton *e,PidginWindow *win)
{
  gint nb_x;
  gint nb_y;
  int tab_clicked;
  GtkWidget *page;
  GtkWidget *tab;
  if (((e -> button) == 2) && ((e -> type) == GDK_BUTTON_PRESS)) {
    PidginConversation *gtkconv;
    tab_clicked = pidgin_conv_get_tab_at_xy(win,(e -> x_root),(e -> y_root),0);
    if (tab_clicked == -1) 
      return 0;
    gtkconv = pidgin_conv_window_get_gtkconv_at_index(win,tab_clicked);
    close_conv_cb(0,gtkconv);
    return (!0);
  }
  if (((e -> button) != 1) || ((e -> type) != GDK_BUTTON_PRESS)) 
    return 0;
  if ((win -> in_drag) != 0) {
    purple_debug(PURPLE_DEBUG_WARNING,"gtkconv","Already in the middle of a window drag at tab_press_cb\n");
    return (!0);
  }
/*
	* Make sure a tab was actually clicked. The arrow buttons
	* mess things up.
	*/
  tab_clicked = pidgin_conv_get_tab_at_xy(win,(e -> x_root),(e -> y_root),0);
  if (tab_clicked == -1) 
    return 0;
/*
	* Get the relative position of the press event, with regards to
	* the position of the notebook.
	*/
  gdk_window_get_origin(( *(win -> notebook)).window,&nb_x,&nb_y);
/* Reset the min/max x/y */
  win -> drag_min_x = 0;
  win -> drag_min_y = 0;
  win -> drag_max_x = 0;
  win -> drag_max_y = 0;
/* Find out which tab was dragged. */
  page = gtk_notebook_get_nth_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),tab_clicked);
  tab = gtk_notebook_get_tab_label(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),page);
  win -> drag_min_x = (tab -> allocation.x + nb_x);
  win -> drag_min_y = (tab -> allocation.y + nb_y);
  win -> drag_max_x = (tab -> allocation.width + (win -> drag_min_x));
  win -> drag_max_y = (tab -> allocation.height + (win -> drag_min_y));
/* Make sure the click occurred in the tab. */
  if (((((e -> x_root) < (win -> drag_min_x)) || ((e -> x_root) >= (win -> drag_max_x))) || ((e -> y_root) < (win -> drag_min_y))) || ((e -> y_root) >= (win -> drag_max_y))) {
    return 0;
  }
  win -> in_predrag = (!0);
  win -> drag_tab = tab_clicked;
/* Connect the new motion signals. */
  win -> drag_motion_signal = (g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"motion_notify_event",((GCallback )notebook_motion_cb),win,0,((GConnectFlags )0)));
  win -> drag_leave_signal = (g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"leave_notify_event",((GCallback )notebook_leave_cb),win,0,((GConnectFlags )0)));
  return 0;
}

static gboolean notebook_release_cb(GtkWidget *widget,GdkEventButton *e,PidginWindow *win)
{
  PidginWindow *dest_win;
  GtkNotebook *dest_notebook;
  PidginConversation *active_gtkconv;
  PidginConversation *gtkconv;
  gint dest_page_num = 0;
  gboolean new_window = 0;
  gboolean to_right = 0;
/*
	* Don't check to make sure that the event's window matches the
	* widget's, because we may be getting an event passed on from the
	* close button.
	*/
  if (((e -> button) != 1) && ((e -> type) != GDK_BUTTON_RELEASE)) 
    return 0;
  if (gdk_pointer_is_grabbed() != 0) {
    gdk_pointer_ungrab(0L);
    gtk_grab_remove(widget);
  }
  if (!((win -> in_predrag) != 0) && !((win -> in_drag) != 0)) 
    return 0;
/* Disconnect the motion signal. */
  if ((win -> drag_motion_signal) != 0) {
    g_signal_handler_disconnect(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),(win -> drag_motion_signal));
    win -> drag_motion_signal = 0;
  }
/*
	* If we're in a pre-drag, we'll also need to disconnect the leave
	* signal.
	*/
  if ((win -> in_predrag) != 0) {
    win -> in_predrag = 0;
    if ((win -> drag_leave_signal) != 0) {
      g_signal_handler_disconnect(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),(win -> drag_leave_signal));
      win -> drag_leave_signal = 0;
    }
  }
/* If we're not in drag...        */
/* We're perfectly normal people! */
  if (!((win -> in_drag) != 0)) 
    return 0;
  win -> in_drag = 0;
  dnd_hints_hide_all();
  dest_win = pidgin_conv_window_get_at_xy((e -> x_root),(e -> y_root));
  active_gtkconv = pidgin_conv_window_get_active_gtkconv(win);
  if (dest_win == ((PidginWindow *)((void *)0))) {
/* If the current window doesn't have any other conversations,
		* there isn't much point transferring the conv to a new window. */
    if (pidgin_conv_window_get_gtkconv_count(win) > 1) {
/* Make a new window to stick this to. */
      dest_win = pidgin_conv_window_new();
      new_window = (!0);
    }
  }
  if (dest_win == ((PidginWindow *)((void *)0))) 
    return 0;
  purple_signal_emit(pidgin_conversations_get_handle(),"conversation-dragging",win,dest_win);
/* Get the destination page number. */
  if (!(new_window != 0)) {
    dest_notebook = ((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(dest_win -> notebook)),gtk_notebook_get_type())));
    if (gtk_notebook_get_show_tabs(dest_notebook) != 0) {
      dest_page_num = pidgin_conv_get_tab_at_xy(dest_win,(e -> x_root),(e -> y_root),&to_right);
    }
    else {
      dest_page_num = 0;
      to_right = pidgin_conv_xy_to_right_infopane(dest_win,(e -> x_root),(e -> y_root));
    }
  }
  gtkconv = pidgin_conv_window_get_gtkconv_at_index(win,(win -> drag_tab));
  if (win == dest_win) {
    gtk_notebook_reorder_child(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),(gtkconv -> tab_cont),dest_page_num);
  }
  else {
    pidgin_conv_window_remove_gtkconv(win,gtkconv);
    pidgin_conv_window_add_gtkconv(dest_win,gtkconv);
    gtk_notebook_reorder_child(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(dest_win -> notebook)),gtk_notebook_get_type()))),(gtkconv -> tab_cont),(dest_page_num + to_right));
    pidgin_conv_window_switch_gtkconv(dest_win,gtkconv);
    if (new_window != 0) {
      gint win_width;
      gint win_height;
      gtk_window_get_size(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(dest_win -> window)),gtk_window_get_type()))),&win_width,&win_height);
#ifdef _WIN32  /* only override window manager placement on Windows */
#endif
      pidgin_conv_window_show(dest_win);
    }
  }
  gtk_widget_grab_focus((active_gtkconv -> entry));
  return (!0);
}

static void before_switch_conv_cb(GtkNotebook *notebook,GtkWidget *page,gint page_num,gpointer user_data)
{
  PidginWindow *win;
  PurpleConversation *conv;
  PidginConversation *gtkconv;
  win = user_data;
  conv = pidgin_conv_window_get_active_conversation(win);
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  if ((purple_conversation_get_type(conv)) != PURPLE_CONV_TYPE_IM) 
    return ;
  gtkconv = ((PidginConversation *)(conv -> ui_data));
  if (( *gtkconv -> u.im).typing_timer != 0) {
    g_source_remove(( *gtkconv -> u.im).typing_timer);
    ( *gtkconv -> u.im).typing_timer = 0;
  }
  stop_anim(0,gtkconv);
}

static void close_window(GtkWidget *w,PidginWindow *win)
{
  close_win_cb(w,0,win);
}

static void detach_tab_cb(GtkWidget *w,GObject *menu)
{
  PidginWindow *win;
  PidginWindow *new_window;
  PidginConversation *gtkconv;
  gtkconv = (g_object_get_data(menu,"clicked_tab"));
  if (!(gtkconv != 0)) 
    return ;
  win = pidgin_conv_get_window(gtkconv);
/* Nothing to do if there's only one tab in the window */
  if (pidgin_conv_window_get_gtkconv_count(win) == 1) 
    return ;
  pidgin_conv_window_remove_gtkconv(win,gtkconv);
  new_window = pidgin_conv_window_new();
  pidgin_conv_window_add_gtkconv(new_window,gtkconv);
  pidgin_conv_window_show(new_window);
}

static void close_others_cb(GtkWidget *w,GObject *menu)
{
  GList *iter;
  PidginConversation *gtkconv;
  PidginWindow *win;
  gtkconv = (g_object_get_data(menu,"clicked_tab"));
  if (!(gtkconv != 0)) 
    return ;
  win = pidgin_conv_get_window(gtkconv);
  for (iter = pidgin_conv_window_get_gtkconvs(win); iter != 0; ) {
    PidginConversation *gconv = (iter -> data);
    iter = (iter -> next);
    if (gconv != gtkconv) {
      close_conv_cb(0,gconv);
    }
  }
}

static void close_tab_cb(GtkWidget *w,GObject *menu)
{
  PidginConversation *gtkconv;
  gtkconv = (g_object_get_data(menu,"clicked_tab"));
  if (gtkconv != 0) 
    close_conv_cb(0,gtkconv);
}

static gboolean right_click_menu_cb(GtkNotebook *notebook,GdkEventButton *event,PidginWindow *win)
{
  GtkWidget *item;
  GtkWidget *menu;
  PidginConversation *gtkconv;
  if (((event -> type) != GDK_BUTTON_PRESS) || ((event -> button) != 3)) 
    return 0;
  gtkconv = pidgin_conv_window_get_gtkconv_at_index(win,pidgin_conv_get_tab_at_xy(win,(event -> x_root),(event -> y_root),0));
  if (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(notebook -> menu)),((GType )(20 << 2))))),"clicked_tab") != 0) {
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(notebook -> menu)),((GType )(20 << 2))))),"clicked_tab",gtkconv);
    return 0;
  }
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(notebook -> menu)),((GType )(20 << 2))))),"clicked_tab",gtkconv);
  menu = (notebook -> menu);
  pidgin_separator(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_widget_get_type()))));
  item = gtk_menu_item_new_with_label(((const char *)(dgettext("pidgin","Close other tabs"))));
  gtk_widget_show(item);
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"activate",((GCallback )close_others_cb),menu,0,((GConnectFlags )0));
  item = gtk_menu_item_new_with_label(((const char *)(dgettext("pidgin","Close all tabs"))));
  gtk_widget_show(item);
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"activate",((GCallback )close_window),win,0,((GConnectFlags )0));
  pidgin_separator(menu);
  item = gtk_menu_item_new_with_label(((const char *)(dgettext("pidgin","Detach this tab"))));
  gtk_widget_show(item);
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"activate",((GCallback )detach_tab_cb),menu,0,((GConnectFlags )0));
  item = gtk_menu_item_new_with_label(((const char *)(dgettext("pidgin","Close this tab"))));
  gtk_widget_show(item);
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"activate",((GCallback )close_tab_cb),menu,0,((GConnectFlags )0));
  return 0;
}

static void remove_edit_entry(PidginConversation *gtkconv,GtkWidget *entry)
{
  g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),G_SIGNAL_MATCH_DATA,0,0,0,0,gtkconv);
  gtk_widget_show((gtkconv -> infopane));
  gtk_widget_grab_focus((gtkconv -> entry));
  gtk_widget_destroy(entry);
}

static gboolean alias_focus_cb(GtkWidget *widget,GdkEventFocus *event,gpointer user_data)
{
  remove_edit_entry(user_data,widget);
  return 0;
}

static gboolean alias_key_press_cb(GtkWidget *widget,GdkEventKey *event,gpointer user_data)
{
  if ((event -> keyval) == 0xff1b) {
    remove_edit_entry(user_data,widget);
    return (!0);
  }
  return 0;
}

static void alias_cb(GtkEntry *entry,gpointer user_data)
{
  PidginConversation *gtkconv;
  PurpleConversation *conv;
  PurpleAccount *account;
  const char *name;
  gtkconv = ((PidginConversation *)user_data);
  if (gtkconv == ((PidginConversation *)((void *)0))) {
    return ;
  }
  conv = (gtkconv -> active_conv);
  account = purple_conversation_get_account(conv);
  name = purple_conversation_get_name(conv);
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
    PurpleBuddy *buddy;
    buddy = purple_find_buddy(account,name);
    if (buddy != ((PurpleBuddy *)((void *)0))) {
      purple_blist_alias_buddy(buddy,gtk_entry_get_text(entry));
    }
    serv_alias_buddy(buddy);
  }
  else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) {
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)( *gtkconv -> u.chat).topic_text),gtk_entry_get_type()))),gtk_entry_get_text(entry));
    topic_callback(0,gtkconv);
  }
  remove_edit_entry(user_data,((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_widget_get_type()))));
}

static gboolean infopane_entry_activate(PidginConversation *gtkconv)
{
  GtkWidget *entry = (GtkWidget *)((void *)0);
  PurpleConversation *conv = (gtkconv -> active_conv);
  const char *text = (const char *)((void *)0);
  if (!((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> infopane)),gtk_object_get_type())))).flags & GTK_VISIBLE) != 0)) {
/* There's already an entry for alias. Let's not create another one. */
    return 0;
  }
  if (!(purple_account_is_connected(( *(gtkconv -> active_conv)).account) != 0)) {
/* Do not allow aliasing someone on a disconnected account. */
    return 0;
  }
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
    PurpleBuddy *buddy = purple_find_buddy(( *(gtkconv -> active_conv)).account,( *(gtkconv -> active_conv)).name);
    if (!(buddy != 0)) 
/* This buddy isn't in your buddy list, so we can't alias him */
      return 0;
    text = purple_buddy_get_contact_alias(buddy);
  }
  else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) {
    PurpleConnection *gc;
    PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
    gc = purple_conversation_get_gc(conv);
    if (gc != ((PurpleConnection *)((void *)0))) 
      prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info);
    if ((prpl_info != 0) && ((prpl_info -> set_chat_topic) == ((void (*)(PurpleConnection *, int , const char *))((void *)0)))) 
/* This protocol doesn't support setting the chat room topic */
      return 0;
    text = purple_conv_chat_get_topic((purple_conversation_get_chat_data(conv)));
  }
/* alias label */
  entry = gtk_entry_new();
  gtk_entry_set_has_frame(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),0);
  gtk_entry_set_width_chars(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),10);
  gtk_entry_set_alignment(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),0.5);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> infopane_hbox)),gtk_box_get_type()))),entry,(!0),(!0),0);
/* after the tab label */
  gtk_box_reorder_child(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> infopane_hbox)),gtk_box_get_type()))),entry,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"activate",((GCallback )alias_cb),gtkconv,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"focus-out-event",((GCallback )alias_focus_cb),gtkconv,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"key-press-event",((GCallback )alias_key_press_cb),gtkconv,0,((GConnectFlags )0));
  if (text != ((const char *)((void *)0))) 
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),text);
  gtk_widget_show(entry);
  gtk_widget_hide((gtkconv -> infopane));
  gtk_widget_grab_focus(entry);
  return (!0);
}

static gboolean window_keypress_cb(GtkWidget *widget,GdkEventKey *event,PidginWindow *win)
{
  PidginConversation *gtkconv = pidgin_conv_window_get_active_gtkconv(win);
  return conv_keypress_common(gtkconv,event);
}

static void switch_conv_cb(GtkNotebook *notebook,GtkWidget *page,gint page_num,gpointer user_data)
{
  PidginWindow *win;
  PurpleConversation *conv;
  PidginConversation *gtkconv;
  const char *sound_method;
  win = user_data;
  gtkconv = pidgin_conv_window_get_gtkconv_at_index(win,page_num);
  conv = (gtkconv -> active_conv);
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
/* clear unseen flag if conversation is not hidden */
  if (!(pidgin_conv_is_hidden(gtkconv) != 0)) {
    gtkconv_set_unseen(gtkconv,PIDGIN_UNSEEN_NONE);
  }
/* Update the menubar */
  gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)( *(gtkconv -> win)).menu.logging),gtk_check_menu_item_get_type()))),purple_conversation_is_logging(conv));
  generate_send_to_items(win);
  regenerate_options_items(win);
  regenerate_plugins_items(win);
  pidgin_conv_switch_active_conversation(conv);
  sound_method = purple_prefs_get_string("/pidgin/sound/method");
  if (strcmp(sound_method,"none") != 0) 
    gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)win -> menu.sounds),gtk_check_menu_item_get_type()))),(gtkconv -> make_sound));
  gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)win -> menu.show_formatting_toolbar),gtk_check_menu_item_get_type()))),purple_prefs_get_bool("/pidgin/conversations/show_formatting_toolbar"));
  gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)win -> menu.show_timestamps),gtk_check_menu_item_get_type()))),purple_prefs_get_bool("/pidgin/conversations/show_timestamps"));
/*
	 * We pause icons when they are not visible.  If this icon should
	 * be animated then start it back up again.
	 */
  if (((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) && (( *gtkconv -> u.im).animate != 0)) 
    start_anim(0,gtkconv);
  purple_signal_emit(pidgin_conversations_get_handle(),"conversation-switched",conv);
}
/**************************************************************************
 * GTK+ window ops
 **************************************************************************/

GList *pidgin_conv_windows_get_list()
{
  return window_list;
}

static GList *make_status_icon_list(const char *stock,GtkWidget *w)
{
  GList *l = (GList *)((void *)0);
  l = g_list_append(l,(gtk_widget_render_icon(w,stock,gtk_icon_size_from_name("pidgin-icon-size-tango-extra-small"),"GtkWindow")));
  l = g_list_append(l,(gtk_widget_render_icon(w,stock,gtk_icon_size_from_name("pidgin-icon-size-tango-small"),"GtkWindow")));
  l = g_list_append(l,(gtk_widget_render_icon(w,stock,gtk_icon_size_from_name("pidgin-icon-size-tango-medium"),"GtkWindow")));
  l = g_list_append(l,(gtk_widget_render_icon(w,stock,gtk_icon_size_from_name("pidgin-icon-size-tango-large"),"GtkWindow")));
  return l;
}

static void create_icon_lists(GtkWidget *w)
{
  available_list = make_status_icon_list("pidgin-status-available",w);
  busy_list = make_status_icon_list("pidgin-status-busy",w);
  xa_list = make_status_icon_list("pidgin-status-xa",w);
  offline_list = make_status_icon_list("pidgin-status-offline",w);
  away_list = make_status_icon_list("pidgin-status-away",w);
  prpl_lists = g_hash_table_new(g_str_hash,g_str_equal);
}

static void plugin_changed_cb(PurplePlugin *p,gpointer data)
{
  regenerate_plugins_items(data);
}

static gboolean gtk_conv_configure_cb(GtkWidget *w,GdkEventConfigure *event,gpointer data)
{
  int x;
  int y;
  if ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)w),gtk_object_get_type())))).flags & GTK_VISIBLE) != 0) 
    gtk_window_get_position(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)w),gtk_window_get_type()))),&x,&y);
  else 
/* carry on normally */
    return 0;
/* Workaround for GTK+ bug # 169811 - "configure_event" is fired
	* when the window is being maximized */
  if (((gdk_window_get_state((w -> window))) & GDK_WINDOW_STATE_MAXIMIZED) != 0U) 
    return 0;
/* don't save off-screen positioning */
  if (((((x + (event -> width)) < 0) || ((y + (event -> height)) < 0)) || (x > gdk_screen_width())) || (y > gdk_screen_height())) 
/* carry on normally */
    return 0;
/* store the position */
  purple_prefs_set_int("/pidgin/conversations/im/x",x);
  purple_prefs_set_int("/pidgin/conversations/im/y",y);
  purple_prefs_set_int("/pidgin/conversations/im/width",(event -> width));
  purple_prefs_set_int("/pidgin/conversations/im/height",(event -> height));
/* continue to handle event normally */
  return 0;
}

static void pidgin_conv_set_position_size(PidginWindow *win,int conv_x,int conv_y,int conv_width,int conv_height)
{
/* if the window exists, is hidden, we're saving positions, and the
          * position is sane... */
  if ((((win != 0) && ((win -> window) != 0)) && !((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),gtk_object_get_type())))).flags & GTK_VISIBLE) != 0)) && (conv_width != 0)) {
#ifdef _WIN32  /* only override window manager placement on Windows */
/* ...check position is on screen... */
/* ...and move it back. */
#endif
    gtk_window_resize(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),gtk_window_get_type()))),conv_width,conv_height);
  }
}

static void pidgin_conv_restore_position(PidginWindow *win)
{
  pidgin_conv_set_position_size(win,purple_prefs_get_int("/pidgin/conversations/im/x"),purple_prefs_get_int("/pidgin/conversations/im/y"),purple_prefs_get_int("/pidgin/conversations/im/width"),purple_prefs_get_int("/pidgin/conversations/im/height"));
}

PidginWindow *pidgin_conv_window_new()
{
  PidginWindow *win;
  GtkPositionType pos;
  GtkWidget *testidea;
  GtkWidget *menubar;
  GdkModifierType state;
  win = (g_malloc0((sizeof(PidginWindow ))));
  window_list = g_list_append(window_list,win);
/* Create the window. */
  win -> window = pidgin_create_window(0,0,"conversation",(!0));
  if (!(gtk_get_current_event_state(&state) != 0)) 
    gtk_window_set_focus_on_map(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),gtk_window_get_type()))),0);
/* Etan: I really think this entire function call should happen only
	 * when we are on Windows but I was informed that back before we used
	 * to save the window position we stored the window size, so I'm
	 * leaving it for now. */
#if TRUE || defined(_WIN32)
  pidgin_conv_restore_position(win);
#endif
  if (available_list == ((GList *)((void *)0))) {
    create_icon_lists((win -> window));
  }
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),"delete_event",((GCallback )close_win_cb),win,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),"focus_in_event",((GCallback )focus_win_cb),win,0,((GConnectFlags )0));
/* Intercept keystrokes from the menu items */
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),"key_press_event",((GCallback )window_keypress_cb),win,0,((GConnectFlags )0));
/* Create the notebook. */
  win -> notebook = gtk_notebook_new();
  pos = (purple_prefs_get_int("/pidgin/conversations/tab_side"));
#if 0
#endif
  gtk_notebook_set_tab_pos(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),pos);
  gtk_notebook_set_scrollable(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),(!0));
  gtk_notebook_popup_enable(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))));
  gtk_notebook_set_show_tabs(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),0);
  gtk_notebook_set_show_border(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),(!0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),((GType )(20 << 2))))),"button-press-event",((GCallback )right_click_menu_cb),win,0,((GConnectFlags )0));
  gtk_widget_show((win -> notebook));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),((GType )(20 << 2))))),"switch_page",((GCallback )before_switch_conv_cb),win,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),((GType )(20 << 2))))),"switch_page",((GCallback )switch_conv_cb),win,0,G_CONNECT_AFTER);
/* Setup the tab drag and drop signals. */
  gtk_widget_add_events((win -> notebook),(GDK_BUTTON1_MOTION_MASK | GDK_LEAVE_NOTIFY_MASK));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),((GType )(20 << 2))))),"button_press_event",((GCallback )notebook_press_cb),win,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),((GType )(20 << 2))))),"button_release_event",((GCallback )notebook_release_cb),win,0,((GConnectFlags )0));
  testidea = gtk_vbox_new(0,0);
/* Setup the menubar. */
  menubar = setup_menubar(win);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)testidea),gtk_box_get_type()))),menubar,0,(!0),0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)testidea),gtk_box_get_type()))),(win -> notebook),(!0),(!0),0);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),gtk_container_get_type()))),testidea);
  gtk_widget_show(testidea);
/* Update the plugin actions when plugins are (un)loaded */
  purple_signal_connect(purple_plugins_get_handle(),"plugin-load",win,((PurpleCallback )plugin_changed_cb),win);
  purple_signal_connect(purple_plugins_get_handle(),"plugin-unload",win,((PurpleCallback )plugin_changed_cb),win);
#ifdef _WIN32
#endif
  return win;
}

void pidgin_conv_window_destroy(PidginWindow *win)
{
  if ((win -> gtkconvs) != 0) {
    GList *iter = (win -> gtkconvs);
    while(iter != 0){
      PidginConversation *gtkconv = (iter -> data);
      iter = (iter -> next);
      close_conv_cb(0,gtkconv);
    }
    return ;
  }
  purple_prefs_disconnect_by_handle(win);
  window_list = g_list_remove(window_list,win);
/* Close the "Find" dialog if it's open */
  if (win -> dialogs.search != 0) 
    gtk_widget_destroy(win -> dialogs.search);
  gtk_widget_destroy((win -> window));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win -> menu.item_factory),((GType )(20 << 2))))));
  purple_notify_close_with_handle(win);
  purple_signals_disconnect_by_handle(win);
  g_free(win);
}

void pidgin_conv_window_show(PidginWindow *win)
{
  gtk_widget_show((win -> window));
}

void pidgin_conv_window_hide(PidginWindow *win)
{
  gtk_widget_hide((win -> window));
}

void pidgin_conv_window_raise(PidginWindow *win)
{
  gdk_window_raise(((GdkWindow *)(g_type_check_instance_cast(((GTypeInstance *)( *(win -> window)).window),gdk_window_object_get_type()))));
}

void pidgin_conv_window_switch_gtkconv(PidginWindow *win,PidginConversation *gtkconv)
{
  gtk_notebook_set_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),gtk_notebook_page_num(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),(gtkconv -> tab_cont)));
}

static gboolean gtkconv_tab_set_tip(GtkWidget *widget,GdkEventCrossing *event,PidginConversation *gtkconv)
{
#if GTK_CHECK_VERSION(2, 12, 0)
#define gtk_tooltips_set_tip(tips, w, l, p)  gtk_widget_set_tooltip_text(w, l)
#endif
/* PANGO_VERSION_CHECK macro was introduced in 1.15. So we need this double check. */
#ifndef PANGO_VERSION_CHECK
#define pango_layout_is_ellipsized(l) TRUE
#elif !PANGO_VERSION_CHECK(1,16,0)
#define pango_layout_is_ellipsized(l) TRUE
#endif
  PangoLayout *layout;
  layout = gtk_label_get_layout(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tab_label)),gtk_label_get_type()))));
  gtk_widget_set_tooltip_text(widget,((pango_layout_is_ellipsized(layout) != 0)?gtk_label_get_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tab_label)),gtk_label_get_type())))) : ((const char *)((void *)0))));
  return 0;
#if GTK_CHECK_VERSION(2, 12, 0)
#undef gtk_tooltips_set_tip
#endif
}

void pidgin_conv_window_add_gtkconv(PidginWindow *win,PidginConversation *gtkconv)
{
  PurpleConversation *conv = (gtkconv -> active_conv);
  PidginConversation *focus_gtkconv;
  GtkWidget *tab_cont = (gtkconv -> tab_cont);
  PurpleConversationType conv_type;
  const gchar *tmp_lab;
  conv_type = purple_conversation_get_type(conv);
  win -> gtkconvs = g_list_append((win -> gtkconvs),gtkconv);
  gtkconv -> win = win;
  if ((((win -> gtkconvs) != 0) && (( *(win -> gtkconvs)).next != 0)) && (( *( *(win -> gtkconvs)).next).next == ((GList *)((void *)0)))) 
    pidgin_conv_tab_pack(win,((PidginConversation *)( *(win -> gtkconvs)).data));
/* Close button. */
  gtkconv -> close = pidgin_create_small_button(gtk_label_new("\303\227"));
  gtk_tooltips_set_tip((gtkconv -> tooltips),(gtkconv -> close),((const char *)(dgettext("pidgin","Close conversation"))),0);
  g_signal_connect_data((gtkconv -> close),"clicked",((GCallback )close_conv_cb),gtkconv,0,((GConnectFlags )0));
/* Status icon. */
  gtkconv -> icon = gtk_image_new();
  gtkconv -> menu_icon = gtk_image_new();
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> icon)),((GType )(20 << 2))))),"icon-size",gtk_icon_size_from_name("pidgin-icon-size-tango-microscopic"),((void *)((void *)0)));
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> menu_icon)),((GType )(20 << 2))))),"icon-size",gtk_icon_size_from_name("pidgin-icon-size-tango-microscopic"),((void *)((void *)0)));
  gtk_widget_show((gtkconv -> icon));
  update_tab_icon(conv);
/* Tab label. */
  gtkconv -> tab_label = gtk_label_new((tmp_lab = purple_conversation_get_title(conv)));
  gtk_widget_set_name((gtkconv -> tab_label),"tab-label");
  gtkconv -> menu_tabby = gtk_hbox_new(0,6);
  gtkconv -> menu_label = gtk_label_new(tmp_lab);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> menu_tabby)),gtk_box_get_type()))),(gtkconv -> menu_icon),0,0,0);
  gtk_widget_show_all((gtkconv -> menu_icon));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> menu_tabby)),gtk_box_get_type()))),(gtkconv -> menu_label),(!0),(!0),0);
  gtk_widget_show((gtkconv -> menu_label));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> menu_label)),gtk_misc_get_type()))),0,0);
  gtk_widget_show((gtkconv -> menu_tabby));
  if (conv_type == PURPLE_CONV_TYPE_IM) 
    pidgin_conv_update_buddy_icon(conv);
/* Build and set conversations tab */
  pidgin_conv_tab_pack(win,gtkconv);
  gtk_notebook_set_menu_label(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),tab_cont,(gtkconv -> menu_tabby));
  gtk_widget_show(tab_cont);
  if (pidgin_conv_window_get_gtkconv_count(win) == 1) {
/* Er, bug in notebooks? Switch to the page manually. */
    gtk_notebook_set_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),0);
  }
  else {
    gtk_notebook_set_show_tabs(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),(!0));
  }
  focus_gtkconv = (g_list_nth_data(pidgin_conv_window_get_gtkconvs(win),(gtk_notebook_get_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type())))))));
  gtk_widget_grab_focus((focus_gtkconv -> entry));
  if (pidgin_conv_window_get_gtkconv_count(win) == 1) 
    update_send_to_selection(win);
}

static void pidgin_conv_tab_pack(PidginWindow *win,PidginConversation *gtkconv)
{
  gboolean tabs_side = 0;
  gint angle = 0;
  GtkWidget *first;
  GtkWidget *third;
  GtkWidget *ebox;
  if ((purple_prefs_get_int("/pidgin/conversations/tab_side") == GTK_POS_LEFT) || (purple_prefs_get_int("/pidgin/conversations/tab_side") == GTK_POS_RIGHT)) 
    tabs_side = (!0);
  else if (purple_prefs_get_int("/pidgin/conversations/tab_side") == (GTK_POS_LEFT | 8)) 
    angle = 90;
  else if (purple_prefs_get_int("/pidgin/conversations/tab_side") == (GTK_POS_RIGHT | 8)) 
    angle = 270;
  if (!(angle != 0)) {
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tab_label)),((GType )(20 << 2))))),"ellipsize",PANGO_ELLIPSIZE_END,((void *)((void *)0)));
    gtk_label_set_width_chars(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tab_label)),gtk_label_get_type()))),4);
  }
  else {
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tab_label)),((GType )(20 << 2))))),"ellipsize",PANGO_ELLIPSIZE_NONE,((void *)((void *)0)));
    gtk_label_set_width_chars(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tab_label)),gtk_label_get_type()))),(-1));
  }
  if (tabs_side != 0) {
    gtk_label_set_width_chars(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tab_label)),gtk_label_get_type()))),((g_utf8_strlen(gtk_label_get_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tab_label)),gtk_label_get_type())))),(-1)) < 12)?g_utf8_strlen(gtk_label_get_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tab_label)),gtk_label_get_type())))),(-1)) : 12));
  }
  gtk_label_set_angle(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tab_label)),gtk_label_get_type()))),angle);
#if 0
#endif
  if (angle != 0) 
    gtkconv -> tabby = gtk_vbox_new(0,6);
  else 
    gtkconv -> tabby = gtk_hbox_new(0,6);
  gtk_widget_set_name((gtkconv -> tabby),"tab-container");
/* select the correct ordering for verticle tabs */
  if (angle == 90) {
    first = (gtkconv -> close);
    third = (gtkconv -> icon);
  }
  else {
    first = (gtkconv -> icon);
    third = (gtkconv -> close);
  }
  ebox = gtk_event_box_new();
  gtk_event_box_set_visible_window(((GtkEventBox *)(g_type_check_instance_cast(((GTypeInstance *)ebox),gtk_event_box_get_type()))),0);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ebox),gtk_container_get_type()))),(gtkconv -> tabby));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ebox),((GType )(20 << 2))))),"enter-notify-event",((GCallback )gtkconv_tab_set_tip),gtkconv,0,((GConnectFlags )0));
  if (( *(gtkconv -> tab_label)).parent == ((GtkWidget *)((void *)0))) {
/* Pack if it's a new widget */
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tabby)),gtk_box_get_type()))),first,0,0,0);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tabby)),gtk_box_get_type()))),(gtkconv -> tab_label),(!0),(!0),0);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tabby)),gtk_box_get_type()))),third,0,0,0);
/* Add this pane to the conversation's notebook. */
    gtk_notebook_append_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),(gtkconv -> tab_cont),ebox);
  }
  else {
/* reparent old widgets on preference changes */
    gtk_widget_reparent(first,(gtkconv -> tabby));
    gtk_widget_reparent((gtkconv -> tab_label),(gtkconv -> tabby));
    gtk_widget_reparent(third,(gtkconv -> tabby));
    gtk_box_set_child_packing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tabby)),gtk_box_get_type()))),first,0,0,0,GTK_PACK_START);
    gtk_box_set_child_packing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tabby)),gtk_box_get_type()))),(gtkconv -> tab_label),(!0),(!0),0,GTK_PACK_START);
    gtk_box_set_child_packing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tabby)),gtk_box_get_type()))),third,0,0,0,GTK_PACK_START);
/* Reset the tabs label to the new version */
    gtk_notebook_set_tab_label(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),(gtkconv -> tab_cont),ebox);
  }
  gtk_notebook_set_tab_label_packing(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),(gtkconv -> tab_cont),(!(tabs_side != 0) && !(angle != 0)),(!0),GTK_PACK_START);
  if (pidgin_conv_window_get_gtkconv_count(win) == 1) 
    gtk_notebook_set_show_tabs(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),((purple_prefs_get_bool("/pidgin/conversations/tabs") != 0) && (!(purple_prefs_get_bool("/pidgin/conversations/im/show_buddy_icons") != 0) || (purple_prefs_get_int("/pidgin/conversations/tab_side") != GTK_POS_TOP))));
/* show the widgets */
/*	gtk_widget_show(gtkconv->icon); */
  gtk_widget_show((gtkconv -> tab_label));
  if (purple_prefs_get_bool("/pidgin/conversations/close_on_tabs") != 0) 
    gtk_widget_show((gtkconv -> close));
  gtk_widget_show((gtkconv -> tabby));
  gtk_widget_show(ebox);
}

void pidgin_conv_window_remove_gtkconv(PidginWindow *win,PidginConversation *gtkconv)
{
  unsigned int index;
  index = (gtk_notebook_page_num(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),(gtkconv -> tab_cont)));
  g_object_ref((gtkconv -> tab_cont));
  gtk_object_sink(((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> tab_cont)),gtk_object_get_type()))));
  gtk_notebook_remove_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),index);
  win -> gtkconvs = g_list_remove((win -> gtkconvs),gtkconv);
  g_signal_handlers_disconnect_matched((win -> window),G_SIGNAL_MATCH_DATA,0,0,0,0,gtkconv);
  if (((win -> gtkconvs) != 0) && (( *(win -> gtkconvs)).next == ((GList *)((void *)0)))) 
    pidgin_conv_tab_pack(win,( *(win -> gtkconvs)).data);
  if (!((win -> gtkconvs) != 0) && (win != hidden_convwin)) 
    pidgin_conv_window_destroy(win);
}

PidginConversation *pidgin_conv_window_get_gtkconv_at_index(const PidginWindow *win,int index)
{
  GtkWidget *tab_cont;
  if (index == -1) 
    index = 0;
  tab_cont = gtk_notebook_get_nth_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),index);
  return ((tab_cont != 0)?g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tab_cont),((GType )(20 << 2))))),"PidginConversation") : ((void *)((void *)0)));
}

PidginConversation *pidgin_conv_window_get_active_gtkconv(const PidginWindow *win)
{
  int index;
  GtkWidget *tab_cont;
  index = gtk_notebook_get_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))));
  if (index == -1) 
    index = 0;
  tab_cont = gtk_notebook_get_nth_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(win -> notebook)),gtk_notebook_get_type()))),index);
  if (!(tab_cont != 0)) 
    return 0;
  return (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tab_cont),((GType )(20 << 2))))),"PidginConversation"));
}

PurpleConversation *pidgin_conv_window_get_active_conversation(const PidginWindow *win)
{
  PidginConversation *gtkconv;
  gtkconv = pidgin_conv_window_get_active_gtkconv(win);
  return (gtkconv != 0)?(gtkconv -> active_conv) : ((struct _PurpleConversation *)((void *)0));
}

gboolean pidgin_conv_window_is_active_conversation(const PurpleConversation *conv)
{
  return conv == (pidgin_conv_window_get_active_conversation(( *((PidginConversation *)(conv -> ui_data))).win));
}

gboolean pidgin_conv_window_has_focus(PidginWindow *win)
{
  gboolean has_focus = 0;
  g_object_get(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),"has-toplevel-focus",&has_focus,((void *)((void *)0)));
  return has_focus;
}

PidginWindow *pidgin_conv_window_get_at_xy(int x,int y)
{
  PidginWindow *win;
  GdkWindow *gdkwin;
  GList *l;
  gdkwin = gdk_window_at_pointer(&x,&y);
  if (gdkwin != 0) 
    gdkwin = gdk_window_get_toplevel(gdkwin);
  for (l = pidgin_conv_windows_get_list(); l != ((GList *)((void *)0)); l = (l -> next)) {
    win = (l -> data);
    if (gdkwin == ( *(win -> window)).window) 
      return win;
  }
  return 0;
}

GList *pidgin_conv_window_get_gtkconvs(PidginWindow *win)
{
  return win -> gtkconvs;
}

guint pidgin_conv_window_get_gtkconv_count(PidginWindow *win)
{
  return g_list_length((win -> gtkconvs));
}

PidginWindow *pidgin_conv_window_first_with_type(PurpleConversationType type)
{
  GList *wins;
  GList *convs;
  PidginWindow *win;
  PidginConversation *conv;
  if (type == PURPLE_CONV_TYPE_UNKNOWN) 
    return 0;
  for (wins = pidgin_conv_windows_get_list(); wins != ((GList *)((void *)0)); wins = (wins -> next)) {
    win = (wins -> data);
    for (convs = (win -> gtkconvs); convs != ((GList *)((void *)0)); convs = (convs -> next)) {
      conv = (convs -> data);
      if ((purple_conversation_get_type((conv -> active_conv))) == type) 
        return win;
    }
  }
  return 0;
}

PidginWindow *pidgin_conv_window_last_with_type(PurpleConversationType type)
{
  GList *wins;
  GList *convs;
  PidginWindow *win;
  PidginConversation *conv;
  if (type == PURPLE_CONV_TYPE_UNKNOWN) 
    return 0;
  for (wins = g_list_last(pidgin_conv_windows_get_list()); wins != ((GList *)((void *)0)); wins = (wins -> prev)) {
    win = (wins -> data);
    for (convs = (win -> gtkconvs); convs != ((GList *)((void *)0)); convs = (convs -> next)) {
      conv = (convs -> data);
      if ((purple_conversation_get_type((conv -> active_conv))) == type) 
        return win;
    }
  }
  return 0;
}
/**************************************************************************
 * Conversation placement functions
 **************************************************************************/
typedef struct __unnamed_class___F0_L9804_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__id__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__name__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L1591R_variable_name_unknown_scope_and_name__scope__fnc {
char *id;
char *name;
PidginConvPlacementFunc fnc;}ConvPlacementData;
static GList *conv_placement_fncs = (GList *)((void *)0);
static PidginConvPlacementFunc place_conv = (PidginConvPlacementFunc )((void *)0);
/* This one places conversations in the last made window. */

static void conv_placement_last_created_win(PidginConversation *conv)
{
  PidginWindow *win;
  GList *l = g_list_last(pidgin_conv_windows_get_list());
  win = (((l != 0)?(l -> data) : ((void *)((void *)0))));;
  if (win == ((PidginWindow *)((void *)0))) {
    win = pidgin_conv_window_new();
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),"configure_event",((GCallback )gtk_conv_configure_cb),0,0,((GConnectFlags )0));
    pidgin_conv_window_add_gtkconv(win,conv);
    pidgin_conv_window_show(win);
  }
  else {
    pidgin_conv_window_add_gtkconv(win,conv);
  }
}
/* This one places conversations in the last made window of the same type. */

static gboolean conv_placement_last_created_win_type_configured_cb(GtkWidget *w,GdkEventConfigure *event,PidginConversation *conv)
{
  int x;
  int y;
  PurpleConversationType type = purple_conversation_get_type((conv -> active_conv));
  GList *all;
  if ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)w),gtk_object_get_type())))).flags & GTK_VISIBLE) != 0) 
    gtk_window_get_position(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)w),gtk_window_get_type()))),&x,&y);
  else 
/* carry on normally */
    return 0;
/* Workaround for GTK+ bug # 169811 - "configure_event" is fired
	* when the window is being maximized */
  if (((gdk_window_get_state((w -> window))) & GDK_WINDOW_STATE_MAXIMIZED) != 0U) 
    return 0;
/* don't save off-screen positioning */
  if (((((x + (event -> width)) < 0) || ((y + (event -> height)) < 0)) || (x > gdk_screen_width())) || (y > gdk_screen_height())) 
/* carry on normally */
    return 0;
  for (all = (conv -> convs); all != ((GList *)((void *)0)); all = (all -> next)) {
    if (type != (purple_conversation_get_type((all -> data)))) {
/* this window has different types of conversation, don't save */
      return 0;
    }
  }
  if (type == PURPLE_CONV_TYPE_IM) {
    purple_prefs_set_int("/pidgin/conversations/im/x",x);
    purple_prefs_set_int("/pidgin/conversations/im/y",y);
    purple_prefs_set_int("/pidgin/conversations/im/width",(event -> width));
    purple_prefs_set_int("/pidgin/conversations/im/height",(event -> height));
  }
  else if (type == PURPLE_CONV_TYPE_CHAT) {
    purple_prefs_set_int("/pidgin/conversations/chat/x",x);
    purple_prefs_set_int("/pidgin/conversations/chat/y",y);
    purple_prefs_set_int("/pidgin/conversations/chat/width",(event -> width));
    purple_prefs_set_int("/pidgin/conversations/chat/height",(event -> height));
  }
  return 0;
}

static void conv_placement_last_created_win_type(PidginConversation *conv)
{
  PidginWindow *win;
  win = pidgin_conv_window_last_with_type(purple_conversation_get_type((conv -> active_conv)));
  if (win == ((PidginWindow *)((void *)0))) {
    win = pidgin_conv_window_new();
    if ((PURPLE_CONV_TYPE_IM == (purple_conversation_get_type((conv -> active_conv)))) || (purple_prefs_get_int("/pidgin/conversations/chat/width") == 0)) {
      pidgin_conv_set_position_size(win,purple_prefs_get_int("/pidgin/conversations/im/x"),purple_prefs_get_int("/pidgin/conversations/im/y"),purple_prefs_get_int("/pidgin/conversations/im/width"),purple_prefs_get_int("/pidgin/conversations/im/height"));
    }
    else if (PURPLE_CONV_TYPE_CHAT == (purple_conversation_get_type((conv -> active_conv)))) {
      pidgin_conv_set_position_size(win,purple_prefs_get_int("/pidgin/conversations/chat/x"),purple_prefs_get_int("/pidgin/conversations/chat/y"),purple_prefs_get_int("/pidgin/conversations/chat/width"),purple_prefs_get_int("/pidgin/conversations/chat/height"));
    }
    pidgin_conv_window_add_gtkconv(win,conv);
    pidgin_conv_window_show(win);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),"configure_event",((GCallback )conv_placement_last_created_win_type_configured_cb),conv,0,((GConnectFlags )0));
  }
  else 
    pidgin_conv_window_add_gtkconv(win,conv);
}
/* This one places each conversation in its own window. */

static void conv_placement_new_window(PidginConversation *conv)
{
  PidginWindow *win;
  win = pidgin_conv_window_new();
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(win -> window)),((GType )(20 << 2))))),"configure_event",((GCallback )gtk_conv_configure_cb),0,0,((GConnectFlags )0));
  pidgin_conv_window_add_gtkconv(win,conv);
  pidgin_conv_window_show(win);
}

static PurpleGroup *conv_get_group(PidginConversation *conv)
{
  PurpleGroup *group = (PurpleGroup *)((void *)0);
  if ((purple_conversation_get_type((conv -> active_conv))) == PURPLE_CONV_TYPE_IM) {
    PurpleBuddy *buddy;
    buddy = purple_find_buddy(purple_conversation_get_account((conv -> active_conv)),purple_conversation_get_name((conv -> active_conv)));
    if (buddy != ((PurpleBuddy *)((void *)0))) 
      group = purple_buddy_get_group(buddy);
  }
  else if ((purple_conversation_get_type((conv -> active_conv))) == PURPLE_CONV_TYPE_CHAT) {
    PurpleChat *chat;
    chat = purple_blist_find_chat(purple_conversation_get_account((conv -> active_conv)),purple_conversation_get_name((conv -> active_conv)));
    if (chat != ((PurpleChat *)((void *)0))) 
      group = purple_chat_get_group(chat);
  }
  return group;
}
/*
 * This groups things by, well, group. Buddies from groups will always be
 * grouped together, and a buddy from a group not belonging to any currently
 * open windows will get a new window.
 */

static void conv_placement_by_group(PidginConversation *conv)
{
  PurpleGroup *group = (PurpleGroup *)((void *)0);
  GList *wl;
  GList *cl;
  group = conv_get_group(conv);
/* Go through the list of IMs and find one with this group. */
  for (wl = pidgin_conv_windows_get_list(); wl != ((GList *)((void *)0)); wl = (wl -> next)) {
    PidginWindow *win2;
    PidginConversation *conv2;
    PurpleGroup *group2 = (PurpleGroup *)((void *)0);
    win2 = (wl -> data);
    for (cl = (win2 -> gtkconvs); cl != ((GList *)((void *)0)); cl = (cl -> next)) {
      conv2 = (cl -> data);
      group2 = conv_get_group(conv2);
      if (group == group2) {
        pidgin_conv_window_add_gtkconv(win2,conv);
        return ;
      }
    }
  }
/* Make a new window. */
  conv_placement_new_window(conv);
}
/* This groups things by account.  Otherwise, the same semantics as above */

static void conv_placement_by_account(PidginConversation *conv)
{
  GList *wins;
  GList *convs;
  PurpleAccount *account;
  account = purple_conversation_get_account((conv -> active_conv));
/* Go through the list of IMs and find one with this group. */
  for (wins = pidgin_conv_windows_get_list(); wins != ((GList *)((void *)0)); wins = (wins -> next)) {
    PidginWindow *win2;
    PidginConversation *conv2;
    win2 = (wins -> data);
    for (convs = (win2 -> gtkconvs); convs != ((GList *)((void *)0)); convs = (convs -> next)) {
      conv2 = (convs -> data);
      if (account == purple_conversation_get_account((conv2 -> active_conv))) {
        pidgin_conv_window_add_gtkconv(win2,conv);
        return ;
      }
    }
  }
/* Make a new window. */
  conv_placement_new_window(conv);
}

static ConvPlacementData *get_conv_placement_data(const char *id)
{
  ConvPlacementData *data = (ConvPlacementData *)((void *)0);
  GList *n;
  for (n = conv_placement_fncs; n != 0; n = (n -> next)) {
    data = (n -> data);
    if (!(strcmp((data -> id),id) != 0)) 
      return data;
  }
  return 0;
}

static void add_conv_placement_fnc(const char *id,const char *name,PidginConvPlacementFunc fnc)
{
  ConvPlacementData *data;
  data = ((ConvPlacementData *)(g_malloc_n(1,(sizeof(ConvPlacementData )))));
  data -> id = g_strdup(id);
  data -> name = g_strdup(name);
  data -> fnc = fnc;
  conv_placement_fncs = g_list_append(conv_placement_fncs,data);
}

static void ensure_default_funcs()
{
  if (conv_placement_fncs == ((GList *)((void *)0))) {
    add_conv_placement_fnc("last",((const char *)(dgettext("pidgin","Last created window"))),conv_placement_last_created_win);
    add_conv_placement_fnc("im_chat",((const char *)(dgettext("pidgin","Separate IM and Chat windows"))),conv_placement_last_created_win_type);
    add_conv_placement_fnc("new",((const char *)(dgettext("pidgin","New window"))),conv_placement_new_window);
    add_conv_placement_fnc("group",((const char *)(dgettext("pidgin","By group"))),conv_placement_by_group);
    add_conv_placement_fnc("account",((const char *)(dgettext("pidgin","By account"))),conv_placement_by_account);
  }
}

GList *pidgin_conv_placement_get_options()
{
  GList *n;
  GList *list = (GList *)((void *)0);
  ConvPlacementData *data;
  ensure_default_funcs();
  for (n = conv_placement_fncs; n != 0; n = (n -> next)) {
    data = (n -> data);
    list = g_list_append(list,(data -> name));
    list = g_list_append(list,(data -> id));
  }
  return list;
}

void pidgin_conv_placement_add_fnc(const char *id,const char *name,PidginConvPlacementFunc fnc)
{
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return ;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return ;
    };
  }while (0);
  do {
    if (fnc != ((void (*)(PidginConversation *))((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"fnc != NULL");
      return ;
    };
  }while (0);
  ensure_default_funcs();
  add_conv_placement_fnc(id,name,fnc);
}

void pidgin_conv_placement_remove_fnc(const char *id)
{
  ConvPlacementData *data = get_conv_placement_data(id);
  if (data == ((ConvPlacementData *)((void *)0))) 
    return ;
  conv_placement_fncs = g_list_remove(conv_placement_fncs,data);
  g_free((data -> id));
  g_free((data -> name));
  g_free(data);
}

const char *pidgin_conv_placement_get_name(const char *id)
{
  ConvPlacementData *data;
  ensure_default_funcs();
  data = get_conv_placement_data(id);
  if (data == ((ConvPlacementData *)((void *)0))) 
    return 0;
  return (data -> name);
}

PidginConvPlacementFunc pidgin_conv_placement_get_fnc(const char *id)
{
  ConvPlacementData *data;
  ensure_default_funcs();
  data = get_conv_placement_data(id);
  if (data == ((ConvPlacementData *)((void *)0))) 
    return 0;
  return data -> fnc;
}

void pidgin_conv_placement_set_current_func(PidginConvPlacementFunc func)
{
  do {
    if (func != ((void (*)(PidginConversation *))((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"func != NULL");
      return ;
    };
  }while (0);
/* If tabs are enabled, set the function, otherwise, NULL it out. */
  if (purple_prefs_get_bool("/pidgin/conversations/tabs") != 0) 
    place_conv = func;
  else 
    place_conv = ((PidginConvPlacementFunc )((void *)0));
}

PidginConvPlacementFunc pidgin_conv_placement_get_current_func()
{
  return place_conv;
}

void pidgin_conv_placement_place(PidginConversation *gtkconv)
{
  if (place_conv != 0) 
    ( *place_conv)(gtkconv);
  else 
    conv_placement_new_window(gtkconv);
}

gboolean pidgin_conv_is_hidden(PidginConversation *gtkconv)
{
  do {
    if (gtkconv != ((PidginConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gtkconv != NULL");
      return 0;
    };
  }while (0);
  return (gtkconv -> win) == hidden_convwin;
}
/* Algorithm from http://www.w3.org/TR/AERT#color-contrast */

static gboolean color_is_visible(GdkColor foreground,GdkColor background,int color_contrast,int brightness_contrast)
{
  gulong fg_brightness;
  gulong bg_brightness;
  gulong br_diff;
  gulong col_diff;
  int fred;
  int fgreen;
  int fblue;
  int bred;
  int bgreen;
  int bblue;
/* this algorithm expects colors between 0 and 255 for each of red green and blue.
	 * GTK on the other hand has values between 0 and 65535
	 * Err suggested I >> 8, which grabbed the high bits.
	 */
  fred = (foreground.red >> 8);
  fgreen = (foreground.green >> 8);
  fblue = (foreground.blue >> 8);
  bred = (background.red >> 8);
  bgreen = (background.green >> 8);
  bblue = (background.blue >> 8);
  fg_brightness = ((((fred * 299) + (fgreen * 587)) + (fblue * 114)) / 1000);
  bg_brightness = ((((bred * 299) + (bgreen * 587)) + (bblue * 114)) / 1000);
  br_diff = (abs((fg_brightness - bg_brightness)));
  col_diff = ((abs((fred - bred)) + abs((fgreen - bgreen))) + abs((fblue - bblue)));
  return (col_diff > color_contrast) && (br_diff > brightness_contrast);
}

static GdkColor *generate_nick_colors(guint *color_count,GdkColor background)
{
  guint numcolors =  *color_count;
  guint i = 0;
  guint j = 0;
  GdkColor *colors = (GdkColor *)(g_malloc_n(numcolors,(sizeof(GdkColor ))));
  GdkColor nick_highlight;
  GdkColor send_color;
  time_t breakout_time;
  gdk_color_parse("#AF7F00",&nick_highlight);
  gdk_color_parse("#204a87",&send_color);
  srand((((background.red + background.green) + background.blue) + 1));
  breakout_time = (time(0) + 3);
/* first we look through the list of "good" colors: colors that differ from every other color in the
	 * list.  only some of them will differ from the background color though. lets see if we can find
	 * numcolors of them that do
	 */
  while(((i < numcolors) && (j < sizeof(nick_seed_colors) / sizeof(nick_seed_colors[0]))) && (time(0) < breakout_time)){
    GdkColor color = nick_seed_colors[j];
    if (((color_is_visible(color,background,200,75) != 0) && (color_is_visible(color,nick_highlight,200 / 2,0) != 0)) && (color_is_visible(color,send_color,200 / 4,0) != 0)) {
      colors[i] = color;
      i++;
    }
    j++;
  }
/* we might not have found numcolors in the last loop.  if we did, we'll never enter this one.
	 * if we did not, lets just find some colors that don't conflict with the background.  its
	 * expensive to find colors that not only don't conflict with the background, but also do not
	 * conflict with each other.
	 */
  while((i < numcolors) && (time(0) < breakout_time)){
    GdkColor color = {(0), ((rand() % 65536)), ((rand() % 65536)), ((rand() % 65536))};
    if (((color_is_visible(color,background,200,75) != 0) && (color_is_visible(color,nick_highlight,200 / 2,0) != 0)) && (color_is_visible(color,send_color,200 / 4,0) != 0)) {
      colors[i] = color;
      i++;
    }
  }
  if (i < numcolors) {
    GdkColor *c = colors;
    purple_debug_warning("gtkconv","Unable to generate enough random colors before timeout. %u colors found.\n",i);
    colors = (g_memdup(c,(i * sizeof(GdkColor ))));
    g_free(c);
     *color_count = i;
  }
  return colors;
}
