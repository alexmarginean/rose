/*
 * @file gtkcertmgr.c GTK+ Certificate Manager API
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "core.h"
#include "pidgin.h"
#include "pidginstock.h"
#include "certificate.h"
#include "debug.h"
#include "notify.h"
#include "request.h"
#include "gtkblist.h"
#include "gtkutils.h"
#include "gtkcertmgr.h"
/*****************************************************************************
 * X.509 tls_peers management interface                                      *
 *****************************************************************************/
typedef struct __unnamed_class___F0_L47_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__mgmt_widget__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1187R__Pe___variable_name_unknown_scope_and_name__scope__listview__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1190R__Pe___variable_name_unknown_scope_and_name__scope__listselect__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__importbutton__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__exportbutton__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__infobutton__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__deletebutton__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L351R__Pe___variable_name_unknown_scope_and_name__scope__tls_peers {
GtkWidget *mgmt_widget;
GtkTreeView *listview;
GtkTreeSelection *listselect;
GtkWidget *importbutton;
GtkWidget *exportbutton;
GtkWidget *infobutton;
GtkWidget *deletebutton;
PurpleCertificatePool *tls_peers;}tls_peers_mgmt_data;
tls_peers_mgmt_data *tpm_dat = (tls_peers_mgmt_data *)((void *)0);
/* Columns
   See http://developer.gnome.org/doc/API/2.0/gtk/TreeWidget.html */
enum __unnamed_enum___F0_L62_C1_TPM_HOSTNAME_COLUMN__COMMA__TPM_N_COLUMNS {TPM_HOSTNAME_COLUMN,TPM_N_COLUMNS};

static void tls_peers_mgmt_destroy(GtkWidget *mgmt_widget,gpointer data)
{
  purple_debug_info("certmgr","tls peers self-destructs\n");
  purple_signals_disconnect_by_handle(tpm_dat);
  purple_request_close_with_handle(tpm_dat);
  g_free(tpm_dat);
  tpm_dat = ((tls_peers_mgmt_data *)((void *)0));
}

static void tls_peers_mgmt_repopulate_list()
{
  GtkTreeView *listview = (tpm_dat -> listview);
  PurpleCertificatePool *tls_peers;
  GList *idlist;
  GList *l;
  GtkListStore *store = (GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_tree_view_get_model(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)listview),gtk_tree_view_get_type())))))),gtk_list_store_get_type()));
/* First, delete everything in the list */
  gtk_list_store_clear(store);
/* Locate the "tls_peers" pool */
  tls_peers = purple_certificate_find_pool("x509","tls_peers");
  do {
    if (tls_peers != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"tls_peers");
      return ;
    };
  }while (0);
/* Grab the loaded certificates */
  idlist = purple_certificate_pool_get_idlist(tls_peers);
/* Populate the listview */
  for (l = idlist; l != 0; l = (l -> next)) {
    GtkTreeIter iter;
    gtk_list_store_append(store,&iter);
    gtk_list_store_set(((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)store),gtk_list_store_get_type()))),&iter,TPM_HOSTNAME_COLUMN,(l -> data),-1);
  }
  purple_certificate_pool_destroy_idlist(idlist);
}

static void tls_peers_mgmt_mod_cb(PurpleCertificatePool *pool,const gchar *id,gpointer data)
{
  do {
    if (pool == (tpm_dat -> tls_peers)) ;
    else 
      g_assertion_message_expr(0,"gtkcertmgr.c",114,((const char *)__func__),"pool == tpm_dat->tls_peers");
  }while (0);
  tls_peers_mgmt_repopulate_list();
}

static void tls_peers_mgmt_select_chg_cb(GtkTreeSelection *ignored,gpointer data)
{
  GtkTreeSelection *select = (tpm_dat -> listselect);
  GtkTreeIter iter;
  GtkTreeModel *model;
/* See if things are selected */
  if (gtk_tree_selection_get_selected(select,&model,&iter) != 0) {
/* Enable buttons if something is selected */
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(tpm_dat -> exportbutton)),gtk_widget_get_type()))),(!0));
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(tpm_dat -> infobutton)),gtk_widget_get_type()))),(!0));
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(tpm_dat -> deletebutton)),gtk_widget_get_type()))),(!0));
  }
  else {
/* Otherwise, disable them */
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(tpm_dat -> exportbutton)),gtk_widget_get_type()))),0);
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(tpm_dat -> infobutton)),gtk_widget_get_type()))),0);
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(tpm_dat -> deletebutton)),gtk_widget_get_type()))),0);
  }
}

static void tls_peers_mgmt_import_ok2_cb(gpointer data,const char *result)
{
  PurpleCertificate *crt = (PurpleCertificate *)data;
/* TODO: Perhaps prompt if you're overwriting a cert? */
/* Drop the certificate into the pool */
  if ((result != 0) && (( *result) != 0)) 
    purple_certificate_pool_store((tpm_dat -> tls_peers),result,crt);
/* And this certificate is not needed any more */
  purple_certificate_destroy(crt);
}

static void tls_peers_mgmt_import_cancel2_cb(gpointer data,const char *result)
{
  PurpleCertificate *crt = (PurpleCertificate *)data;
  purple_certificate_destroy(crt);
}

static void tls_peers_mgmt_import_ok_cb(gpointer data,const char *filename)
{
  PurpleCertificateScheme *x509;
  PurpleCertificate *crt;
/* Load the scheme of our tls_peers pool (ought to be x509) */
  x509 = purple_certificate_pool_get_scheme((tpm_dat -> tls_peers));
/* Now load the certificate from disk */
  crt = purple_certificate_import(x509,filename);
/* Did it work? */
  if (crt != ((PurpleCertificate *)((void *)0))) {
    gchar *default_hostname;
/* Get name to add to pool as */
/* Make a guess about what the hostname should be */
    default_hostname = purple_certificate_get_subject_name(crt);
/* TODO: Find a way to make sure that crt gets destroyed
		   if the window gets closed unusually, such as by handle
		   deletion */
/* TODO: Display some more information on the certificate? */
    purple_request_input(tpm_dat,((const char *)(dgettext("pidgin","Certificate Import"))),((const char *)(dgettext("pidgin","Specify a hostname"))),((const char *)(dgettext("pidgin","Type the host name for this certificate."))),default_hostname,0,0,0,((const char *)(dgettext("pidgin","OK"))),((GCallback )tls_peers_mgmt_import_ok2_cb),((const char *)(dgettext("pidgin","Cancel"))),((GCallback )tls_peers_mgmt_import_cancel2_cb),0,0,0,crt);
/* Not multiline */
/* Not masked? */
/* No hints? */
/* No account/who/conv*/
/* Pass cert instance to callback*/
    g_free(default_hostname);
  }
  else {
/* Errors! Oh no! */
/* TODO: Perhaps find a way to be specific about what just
		   went wrong? */
    gchar *secondary;
    secondary = g_strdup_printf(((const char *)(dgettext("pidgin","File %s could not be imported.\nMake sure that the file is readable and in PEM format.\n"))),filename);
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Certificate Import Error"))),((const char *)(dgettext("pidgin","X.509 certificate import failed"))),secondary,0,0);
    g_free(secondary);
  }
}

static void tls_peers_mgmt_import_cb(GtkWidget *button,gpointer data)
{
/* TODO: need to tell the user that we want a .PEM file! */
  purple_request_file(tpm_dat,((const char *)(dgettext("pidgin","Select a PEM certificate"))),"certificate.pem",0,((GCallback )tls_peers_mgmt_import_ok_cb),0,0,0,0,0);
/* Not a save dialog */
/* Do nothing if cancelled */
/* No account,conv,etc. */
}

static void tls_peers_mgmt_export_ok_cb(gpointer data,const char *filename)
{
  PurpleCertificate *crt = (PurpleCertificate *)data;
  do {
    if (filename != 0) ;
    else 
      g_assertion_message_expr(0,"gtkcertmgr.c",235,((const char *)__func__),"filename");
  }while (0);
  if (!(purple_certificate_export(filename,crt) != 0)) {
/* Errors! Oh no! */
/* TODO: Perhaps find a way to be specific about what just
		   went wrong? */
    gchar *secondary;
    secondary = g_strdup_printf(((const char *)(dgettext("pidgin","Export to file %s failed.\nCheck that you have write permission to the target path\n"))),filename);
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Certificate Export Error"))),((const char *)(dgettext("pidgin","X.509 certificate export failed"))),secondary,0,0);
    g_free(secondary);
  }
  purple_certificate_destroy(crt);
}

static void tls_peers_mgmt_export_cancel_cb(gpointer data,const char *filename)
{
  PurpleCertificate *crt = (PurpleCertificate *)data;
/* Pressing cancel just frees the duplicated certificate */
  purple_certificate_destroy(crt);
}

static void tls_peers_mgmt_export_cb(GtkWidget *button,gpointer data)
{
  PurpleCertificate *crt;
  GtkTreeSelection *select = (tpm_dat -> listselect);
  GtkTreeIter iter;
  GtkTreeModel *model;
  gchar *id;
/* See if things are selected */
  if (!(gtk_tree_selection_get_selected(select,&model,&iter) != 0)) {
    purple_debug_warning("gtkcertmgr/tls_peers_mgmt","Export clicked with no selection\?\n");
    return ;
  }
/* Retrieve the selected hostname */
  gtk_tree_model_get(model,&iter,TPM_HOSTNAME_COLUMN,&id,-1);
/* Extract the certificate from the pool now to make sure it doesn't
	   get deleted out from under us */
  crt = purple_certificate_pool_retrieve((tpm_dat -> tls_peers),id);
  if (((PurpleCertificate *)((void *)0)) == crt) {
    purple_debug_error("gtkcertmgr/tls_peers_mgmt","Id %s was not in the peers cache\?!\n",id);
    g_free(id);
    return ;
  }
  g_free(id);
/* TODO: inform user that it will be a PEM? */
  purple_request_file(tpm_dat,((const char *)(dgettext("pidgin","PEM X.509 Certificate Export"))),"certificate.pem",(!0),((GCallback )tls_peers_mgmt_export_ok_cb),((GCallback )tls_peers_mgmt_export_cancel_cb),0,0,0,crt);
/* Is a save dialog */
/* No account,conv,etc. */
/* Pass the certificate on to the callback */
}

static void tls_peers_mgmt_info_cb(GtkWidget *button,gpointer data)
{
  GtkTreeSelection *select = (tpm_dat -> listselect);
  GtkTreeIter iter;
  GtkTreeModel *model;
  gchar *id;
  PurpleCertificate *crt;
/* See if things are selected */
  if (!(gtk_tree_selection_get_selected(select,&model,&iter) != 0)) {
    purple_debug_warning("gtkcertmgr/tls_peers_mgmt","Info clicked with no selection\?\n");
    return ;
  }
/* Retrieve the selected hostname */
  gtk_tree_model_get(model,&iter,TPM_HOSTNAME_COLUMN,&id,-1);
/* Now retrieve the certificate */
  crt = purple_certificate_pool_retrieve((tpm_dat -> tls_peers),id);
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return ;
    };
  }while (0);
/* Fire the notification */
  purple_certificate_display_x509(crt);
  g_free(id);
  purple_certificate_destroy(crt);
}

static void tls_peers_mgmt_delete_confirm_cb(gchar *id,gint choice)
{
  if (1 == choice) {
/* Yes, delete was confirmed */
/* Now delete the thing */
    if (!(purple_certificate_pool_delete((tpm_dat -> tls_peers),id) != 0)) {
      purple_debug_warning("gtkcertmgr/tls_peers_mgmt","Deletion failed on id %s\n",id);
    };
  }
  g_free(id);
}

static void tls_peers_mgmt_delete_cb(GtkWidget *button,gpointer data)
{
  GtkTreeSelection *select = (tpm_dat -> listselect);
  GtkTreeIter iter;
  GtkTreeModel *model;
/* See if things are selected */
  if (gtk_tree_selection_get_selected(select,&model,&iter) != 0) {
    gchar *id;
    gchar *primary;
/* Retrieve the selected hostname */
    gtk_tree_model_get(model,&iter,TPM_HOSTNAME_COLUMN,&id,-1);
/* Prompt to confirm deletion */
    primary = g_strdup_printf(((const char *)(dgettext("pidgin","Really delete certificate for %s\?"))),id);
    purple_request_action(tpm_dat,((const char *)(dgettext("pidgin","Confirm certificate delete"))),primary,0,0,0,0,0,id,2,((const char *)(dgettext("pidgin","_Yes"))),tls_peers_mgmt_delete_confirm_cb,((const char *)(dgettext("pidgin","_No"))),tls_peers_mgmt_delete_confirm_cb);
/* Can this be NULL? */
/* "yes" is the default action */
/* id ownership passed to callback */
    g_free(primary);
  }
  else {
    purple_debug_warning("gtkcertmgr/tls_peers_mgmt","Delete clicked with no selection\?\n");
    return ;
  }
}

static GtkWidget *tls_peers_mgmt_build()
{
  GtkWidget *bbox;
  GtkListStore *store;
/* This block of variables will end up in tpm_dat */
  GtkTreeView *listview;
  GtkTreeSelection *select;
  GtkWidget *importbutton;
  GtkWidget *exportbutton;
  GtkWidget *infobutton;
  GtkWidget *deletebutton;
/** Element to return to the Certmgr window to put in the Notebook */
  GtkWidget *mgmt_widget;
/* Create a struct to store context information about this window */
  tpm_dat = ((tls_peers_mgmt_data *)(g_malloc0_n(1,(sizeof(tls_peers_mgmt_data )))));
  tpm_dat -> mgmt_widget = (mgmt_widget = gtk_hbox_new(0,6));
/* Non-homogeneous */
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)mgmt_widget),gtk_container_get_type()))),6);
  gtk_widget_show(mgmt_widget);
/* Ensure that everything gets cleaned up when the dialog box
	   is closed */
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)mgmt_widget),((GType )(20 << 2))))),"destroy",((GCallback )tls_peers_mgmt_destroy),0,0,((GConnectFlags )0));
/* List view */
  store = gtk_list_store_new(TPM_N_COLUMNS,((GType )(16 << 2)));
  tpm_dat -> listview = (listview = ((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_tree_view_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)store),gtk_tree_model_get_type())))))),gtk_tree_view_get_type()))));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)store),((GType )(20 << 2))))));
{
    GtkCellRenderer *renderer;
    GtkTreeViewColumn *column;
/* Set up the display columns */
    renderer = gtk_cell_renderer_text_new();
    column = gtk_tree_view_column_new_with_attributes(((const char *)(dgettext("pidgin","Hostname"))),renderer,"text",TPM_HOSTNAME_COLUMN,((void *)((void *)0)));
    gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)listview),gtk_tree_view_get_type()))),column);
    gtk_tree_sortable_set_sort_column_id(((GtkTreeSortable *)(g_type_check_instance_cast(((GTypeInstance *)store),gtk_tree_sortable_get_type()))),TPM_HOSTNAME_COLUMN,GTK_SORT_ASCENDING);
  }
/* Get the treeview selector into the struct */
  tpm_dat -> listselect = (select = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)listview),gtk_tree_view_get_type())))));
/* Force the selection mode */
  gtk_tree_selection_set_mode(select,GTK_SELECTION_SINGLE);
/* Use a callback to enable/disable the buttons based on whether
	   something is selected */
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)select),((GType )(20 << 2))))),"changed",((GCallback )tls_peers_mgmt_select_chg_cb),0,0,((GConnectFlags )0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)mgmt_widget),gtk_box_get_type()))),pidgin_make_scrollable(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)listview),gtk_widget_get_type()))),GTK_POLICY_AUTOMATIC,GTK_POLICY_ALWAYS,GTK_SHADOW_IN,-1,-1),(!0),(!0),0);
/* Take up lots of space */
  gtk_widget_show(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)listview),gtk_widget_get_type()))));
/* Fill the list for the first time */
  tls_peers_mgmt_repopulate_list();
/* Right-hand side controls box */
  bbox = gtk_vbutton_box_new();
  gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)mgmt_widget),gtk_box_get_type()))),bbox,0,0,0);
/* Do not take up space */
  gtk_box_set_spacing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),6);
  gtk_button_box_set_layout(((GtkButtonBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_button_box_get_type()))),GTK_BUTTONBOX_START);
  gtk_widget_show(bbox);
/* Import button */
/* TODO: This is the wrong stock button */
  tpm_dat -> importbutton = (importbutton = gtk_button_new_from_stock("gtk-add"));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),importbutton,0,0,0);
  gtk_widget_show(importbutton);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)importbutton),((GType )(20 << 2))))),"clicked",((GCallback )tls_peers_mgmt_import_cb),0,0,((GConnectFlags )0));
/* Export button */
/* TODO: This is the wrong stock button */
  tpm_dat -> exportbutton = (exportbutton = gtk_button_new_from_stock("gtk-save"));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),exportbutton,0,0,0);
  gtk_widget_show(exportbutton);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)exportbutton),((GType )(20 << 2))))),"clicked",((GCallback )tls_peers_mgmt_export_cb),0,0,((GConnectFlags )0));
/* Info button */
  tpm_dat -> infobutton = (infobutton = gtk_button_new_from_stock("pidgin-info"));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),infobutton,0,0,0);
  gtk_widget_show(infobutton);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)infobutton),((GType )(20 << 2))))),"clicked",((GCallback )tls_peers_mgmt_info_cb),0,0,((GConnectFlags )0));
/* Delete button */
  tpm_dat -> deletebutton = (deletebutton = gtk_button_new_from_stock("gtk-delete"));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),deletebutton,0,0,0);
  gtk_widget_show(deletebutton);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)deletebutton),((GType )(20 << 2))))),"clicked",((GCallback )tls_peers_mgmt_delete_cb),0,0,((GConnectFlags )0));
/* Call the "selection changed" callback, which will probably disable
	   all the buttons since nothing is selected yet */
  tls_peers_mgmt_select_chg_cb(select,0);
/* Bind us to the tls_peers pool */
  tpm_dat -> tls_peers = purple_certificate_find_pool("x509","tls_peers");
/**** libpurple signals ****/
/* Respond to certificate add/remove by just reloading everything */
  purple_signal_connect((tpm_dat -> tls_peers),"certificate-stored",tpm_dat,((PurpleCallback )tls_peers_mgmt_mod_cb),0);
  purple_signal_connect((tpm_dat -> tls_peers),"certificate-deleted",tpm_dat,((PurpleCallback )tls_peers_mgmt_mod_cb),0);
  return mgmt_widget;
}
const PidginCertificateManager tls_peers_mgmt = {(tls_peers_mgmt_build), ("SSL Servers")
/* Widget creation function */
};
/*****************************************************************************
 * GTK+ main certificate manager                                             *
 *****************************************************************************/
typedef struct __unnamed_class___F0_L537_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__notebook__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__closebutton {
GtkWidget *window;
GtkWidget *notebook;
GtkWidget *closebutton;}CertMgrDialog;
/* If a certificate manager window is open, this will point to it.
   So if it is set, don't open another one! */
CertMgrDialog *certmgr_dialog = (CertMgrDialog *)((void *)0);

static gboolean certmgr_close_cb(GtkWidget *w,CertMgrDialog *dlg)
{
/* TODO: Ignoring the arguments to this function may not be ideal,
	   but there *should* only be "one dialog to rule them all" at a time*/
  pidgin_certmgr_hide();
  return 0;
}

void pidgin_certmgr_show()
{
  CertMgrDialog *dlg;
  GtkWidget *win;
  GtkWidget *vbox;
/* Enumerate all the certificates on file */
{
    GList *idlist;
    GList *poollist;
    for (poollist = purple_certificate_get_pools(); poollist != 0; poollist = (poollist -> next)) {
      PurpleCertificatePool *pool = (poollist -> data);
      GList *l;
      purple_debug_info("gtkcertmgr","Pool %s found for scheme %s -Enumerating certificates:\n",(pool -> name),(pool -> scheme_name));
      idlist = purple_certificate_pool_get_idlist(pool);
      for (l = idlist; l != 0; l = (l -> next)) {
        purple_debug_info("gtkcertmgr","- %s\n",(((l -> data) != 0)?((gchar *)(l -> data)) : "(null)"));
/* idlist */
      }
      purple_certificate_pool_destroy_idlist(idlist);
/* poollist */
    }
  }
/* If the manager is already open, bring it to the front */
  if (certmgr_dialog != ((CertMgrDialog *)((void *)0))) {
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(certmgr_dialog -> window)),gtk_window_get_type()))));
    return ;
  }
/* Create the dialog, and set certmgr_dialog so we never create
	   more than one at a time */
  dlg = (certmgr_dialog = ((CertMgrDialog *)(g_malloc0_n(1,(sizeof(CertMgrDialog ))))));
  win = (dlg -> window = pidgin_create_dialog(((const char *)(dgettext("pidgin","Certificate Manager"))),12,"certmgr",(!0)));
/* Title */
/*Window border*/
/* Role */
/* Allow resizing */
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"delete_event",((GCallback )certmgr_close_cb),dlg,0,((GConnectFlags )0));
/* TODO: Retrieve the user-set window size and use it */
  gtk_window_set_default_size(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_window_get_type()))),400,400);
/* Main vbox */
  vbox = pidgin_dialog_get_vbox_with_properties(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),0,12);
/* Notebook of various certificate managers */
  dlg -> notebook = gtk_notebook_new();
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),(dlg -> notebook),(!0),(!0),0);
/* Notebook should take extra space */
  gtk_widget_show((dlg -> notebook));
/* Close button */
  dlg -> closebutton = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),"gtk-close",((GCallback )certmgr_close_cb),dlg);
/* Add the defined certificate managers */
/* TODO: Find a way of determining whether each is shown or not */
/* TODO: Implement this correctly */
  gtk_notebook_append_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(dlg -> notebook)),gtk_notebook_get_type()))),( *tls_peers_mgmt.build)(),gtk_label_new(((const char *)(dgettext("pidgin",tls_peers_mgmt.label)))));
  gtk_widget_show(win);
}

void pidgin_certmgr_hide()
{
/* If it isn't open, do nothing */
  if (certmgr_dialog == ((CertMgrDialog *)((void *)0))) {
    return ;
  }
  purple_signals_disconnect_by_handle(certmgr_dialog);
  purple_prefs_disconnect_by_handle(certmgr_dialog);
  gtk_widget_destroy((certmgr_dialog -> window));
  g_free(certmgr_dialog);
  certmgr_dialog = ((CertMgrDialog *)((void *)0));
}
