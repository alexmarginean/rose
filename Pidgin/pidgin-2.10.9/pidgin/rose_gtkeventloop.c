/**
 * @file gtk_eventloop.c Purple Event Loop API (gtk implementation)
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <glib.h>
#include "gtkeventloop.h"
#include "eventloop.h"
#ifdef _WIN32
#include "win32dep.h"
#endif
#define PIDGIN_READ_COND  (G_IO_IN | G_IO_HUP | G_IO_ERR)
#define PIDGIN_WRITE_COND (G_IO_OUT | G_IO_HUP | G_IO_ERR | G_IO_NVAL)
typedef struct _PidginIOClosure {
PurpleInputFunction function;
guint result;
gpointer data;}PidginIOClosure;

static gboolean pidgin_io_invoke(GIOChannel *source,GIOCondition condition,gpointer data)
{
  PidginIOClosure *closure = data;
  PurpleInputCondition purple_cond = 0;
  if ((condition & (G_IO_IN | G_IO_HUP | G_IO_ERR)) != 0U) 
    purple_cond |= PURPLE_INPUT_READ;
  if ((condition & (G_IO_OUT | G_IO_HUP | G_IO_ERR | G_IO_NVAL)) != 0U) 
    purple_cond |= PURPLE_INPUT_WRITE;
#if 0
#endif
#ifdef _WIN32
#ifdef DEBUG
#endif /* DEBUG */
#endif /* _WIN32 */
  ( *(closure -> function))((closure -> data),g_io_channel_unix_get_fd(source),purple_cond);
  return (!0);
}

static guint pidgin_input_add(gint fd,PurpleInputCondition condition,PurpleInputFunction function,gpointer data)
{
  PidginIOClosure *closure = (PidginIOClosure *)(g_malloc0_n(1,(sizeof(PidginIOClosure ))));
  GIOChannel *channel;
  GIOCondition cond = 0;
#ifdef _WIN32
#endif
  closure -> function = function;
  closure -> data = data;
  if ((condition & PURPLE_INPUT_READ) != 0U) 
    cond |= (G_IO_IN | G_IO_HUP | G_IO_ERR);
  if ((condition & PURPLE_INPUT_WRITE) != 0U) 
    cond |= (G_IO_OUT | G_IO_HUP | G_IO_ERR | G_IO_NVAL);
#ifdef _WIN32
#endif
  channel = g_io_channel_unix_new(fd);
  closure -> result = g_io_add_watch_full(channel,0,cond,pidgin_io_invoke,closure,g_free);
#if 0
#endif
  g_io_channel_unref(channel);
  return closure -> result;
}
static PurpleEventLoopUiOps eventloop_ops = {(g_timeout_add), (g_source_remove), (pidgin_input_add), (g_source_remove), ((int (*)(int , int *))((void *)0)), (g_timeout_add_seconds), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* input_get_error */
#if GLIB_CHECK_VERSION(2,14,0)
#else
#endif
};

PurpleEventLoopUiOps *pidgin_eventloop_get_ui_ops()
{
  return &eventloop_ops;
}
