/**
 * @file gtkaccount.c GTK+ Account Editor UI
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "pidgin.h"
#include "account.h"
#include "accountopt.h"
#include "core.h"
#include "debug.h"
#include "notify.h"
#include "plugin.h"
#include "prefs.h"
#include "prpl.h"
#include "request.h"
#include "savedstatuses.h"
#include "signals.h"
#include "util.h"
#include "gtkaccount.h"
#include "gtkblist.h"
#include "gtkdialogs.h"
#include "gtkutils.h"
#include "gtkstatusbox.h"
#include "pidginstock.h"
#include "minidialog.h"
enum __unnamed_enum___F0_L51_C1_COLUMN_ICON__COMMA__COLUMN_BUDDYICON__COMMA__COLUMN_USERNAME__COMMA__COLUMN_ENABLED__COMMA__COLUMN_PROTOCOL__COMMA__COLUMN_DATA__COMMA__NUM_COLUMNS {COLUMN_ICON,COLUMN_BUDDYICON,COLUMN_USERNAME,COLUMN_ENABLED,COLUMN_PROTOCOL,COLUMN_DATA,NUM_COLUMNS};
typedef struct __unnamed_class___F0_L62_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L247R__Pe___variable_name_unknown_scope_and_name__scope__account__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__username__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__alias {
PurpleAccount *account;
char *username;
char *alias;}PidginAccountAddUserData;
typedef struct __unnamed_class___F0_L70_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__treeview__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__modify_button__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__delete_button__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__notebook__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1172R__Pe___variable_name_unknown_scope_and_name__scope__model__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L1086R_variable_name_unknown_scope_and_name__scope__drag_iter__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1096R__Pe___variable_name_unknown_scope_and_name__scope__username_col {
GtkWidget *window;
GtkWidget *treeview;
GtkWidget *modify_button;
GtkWidget *delete_button;
GtkWidget *notebook;
GtkListStore *model;
GtkTreeIter drag_iter;
GtkTreeViewColumn *username_col;}AccountsWindow;
typedef struct __unnamed_class___F0_L86_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__widget__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__setting__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L273R_variable_name_unknown_scope_and_name__scope__type {
GtkWidget *widget;
gchar *setting;
PurplePrefType type;}ProtocolOptEntry;
typedef struct __unnamed_class___F0_L93_C9_unknown_scope_and_name_variable_declaration__variable_type_L1666R_variable_name_unknown_scope_and_name__scope__type__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L247R__Pe___variable_name_unknown_scope_and_name__scope__account__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__protocol_id__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L263R__Pe___variable_name_unknown_scope_and_name__scope__plugin__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L292R__Pe___variable_name_unknown_scope_and_name__scope__prpl_info__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L1646R_variable_name_unknown_scope_and_name__scope__new_proxy_type__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GList_GList__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__user_split_entries__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GList_GList__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__protocol_opt_entries__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1295R__Pe___variable_name_unknown_scope_and_name__scope__sg__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__notebook__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__top_vbox__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__ok_button__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__register_button__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__login_frame__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__protocol_menu__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__password_box__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__username_entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__password_entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__alias_entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__remember_pass_check__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__user_frame__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__new_mail_check__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__icon_hbox__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__icon_check__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__icon_entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__icon_filesel__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__icon_preview__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__icon_text__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L291R__Pe___variable_name_unknown_scope_and_name__scope__icon_img__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__protocol_frame__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__proxy_frame__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__proxy_vbox__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__proxy_dropdown__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__proxy_host_entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__proxy_port_entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__proxy_user_entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__proxy_pass_entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__voice_frame__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__suppression_check {
PidginAccountDialogType type;
PurpleAccount *account;
char *protocol_id;
PurplePlugin *plugin;
PurplePluginProtocolInfo *prpl_info;
PurpleProxyType new_proxy_type;
GList *user_split_entries;
GList *protocol_opt_entries;
GtkSizeGroup *sg;
GtkWidget *window;
GtkWidget *notebook;
GtkWidget *top_vbox;
GtkWidget *ok_button;
GtkWidget *register_button;
/* Login Options */
GtkWidget *login_frame;
GtkWidget *protocol_menu;
GtkWidget *password_box;
GtkWidget *username_entry;
GtkWidget *password_entry;
GtkWidget *alias_entry;
GtkWidget *remember_pass_check;
/* User Options */
GtkWidget *user_frame;
GtkWidget *new_mail_check;
GtkWidget *icon_hbox;
GtkWidget *icon_check;
GtkWidget *icon_entry;
GtkWidget *icon_filesel;
GtkWidget *icon_preview;
GtkWidget *icon_text;
PurpleStoredImage *icon_img;
/* Protocol Options */
GtkWidget *protocol_frame;
/* Proxy Options */
GtkWidget *proxy_frame;
GtkWidget *proxy_vbox;
GtkWidget *proxy_dropdown;
GtkWidget *proxy_host_entry;
GtkWidget *proxy_port_entry;
GtkWidget *proxy_user_entry;
GtkWidget *proxy_pass_entry;
/* Voice & Video Options*/
GtkWidget *voice_frame;
GtkWidget *suppression_check;}AccountPrefsDialog;
static AccountsWindow *accounts_window = (AccountsWindow *)((void *)0);
static GHashTable *account_pref_wins;
static void add_account_to_liststore(PurpleAccount *account,gpointer user_data);
static void set_account(GtkListStore *store,GtkTreeIter *iter,PurpleAccount *account,GdkPixbuf *global_buddyicon);
/**************************************************************************
 * Add/Modify Account dialog
 **************************************************************************/
static void add_login_options(AccountPrefsDialog *dialog,GtkWidget *parent);
static void add_user_options(AccountPrefsDialog *dialog,GtkWidget *parent);
static void add_protocol_options(AccountPrefsDialog *dialog);
static void add_proxy_options(AccountPrefsDialog *dialog,GtkWidget *parent);
static void add_voice_options(AccountPrefsDialog *dialog);

static GtkWidget *add_pref_box(AccountPrefsDialog *dialog,GtkWidget *parent,const char *text,GtkWidget *widget)
{
  return pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)parent),gtk_box_get_type()))),text,(dialog -> sg),widget,(!0),0);
}

static void set_dialog_icon(AccountPrefsDialog *dialog,gpointer data,size_t len,gchar *new_icon_path)
{
  GdkPixbuf *pixbuf = (GdkPixbuf *)((void *)0);
  dialog -> icon_img = purple_imgstore_unref((dialog -> icon_img));
  if (data != ((void *)((void *)0))) {
    if (len > 0) 
      dialog -> icon_img = purple_imgstore_add(data,len,new_icon_path);
    else 
      g_free(data);
  }
  if ((dialog -> icon_img) != ((PurpleStoredImage *)((void *)0))) {
    pixbuf = pidgin_pixbuf_from_imgstore((dialog -> icon_img));
  }
  if (((pixbuf != 0) && ((dialog -> prpl_info) != 0)) && ((( *(dialog -> prpl_info)).icon_spec.scale_rules & PURPLE_ICON_SCALE_DISPLAY) != 0U)) {
/* Scale the icon to something reasonable */
    int width;
    int height;
    GdkPixbuf *scale;
    pidgin_buddy_icon_get_scale_size(pixbuf,&( *(dialog -> prpl_info)).icon_spec,PURPLE_ICON_SCALE_DISPLAY,&width,&height);
    scale = gdk_pixbuf_scale_simple(pixbuf,width,height,GDK_INTERP_BILINEAR);
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
    pixbuf = scale;
  }
  if (pixbuf == ((GdkPixbuf *)((void *)0))) {
/* Show a placeholder icon */
    GtkIconSize icon_size = gtk_icon_size_from_name("pidgin-icon-size-tango-small");
    pixbuf = gtk_widget_render_icon((dialog -> window),"pidgin-select-avatar",icon_size,"PidginAccount");
  }
  gtk_image_set_from_pixbuf(((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_entry)),gtk_image_get_type()))),pixbuf);
  if (pixbuf != ((GdkPixbuf *)((void *)0))) 
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
}

static void set_account_protocol_cb(GtkWidget *item,const char *id,AccountPrefsDialog *dialog)
{
  PurplePlugin *new_plugin;
  new_plugin = purple_find_prpl(id);
  dialog -> plugin = new_plugin;
  if ((dialog -> plugin) != ((PurplePlugin *)((void *)0))) {
    dialog -> prpl_info = ((PurplePluginProtocolInfo *)( *( *(dialog -> plugin)).info).extra_info);
    g_free((dialog -> protocol_id));
    dialog -> protocol_id = g_strdup(( *( *(dialog -> plugin)).info).id);
  }
  if ((dialog -> account) != ((PurpleAccount *)((void *)0))) 
    purple_account_clear_settings((dialog -> account));
  add_login_options(dialog,(dialog -> top_vbox));
  add_user_options(dialog,(dialog -> top_vbox));
  add_protocol_options(dialog);
  add_voice_options(dialog);
  gtk_widget_grab_focus((dialog -> protocol_menu));
  if ((!((dialog -> prpl_info) != 0) || !(( *(dialog -> prpl_info)).register_user != 0)) || (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"fake") != 0)) {
    gtk_widget_hide((dialog -> register_button));
  }
  else {
    if (((dialog -> prpl_info) != ((PurplePluginProtocolInfo *)((void *)0))) && ((( *(dialog -> prpl_info)).options & OPT_PROTO_REGISTER_NOSCREENNAME) != 0U)) {
      gtk_widget_set_sensitive((dialog -> register_button),(!0));
    }
    else {
      gtk_widget_set_sensitive((dialog -> register_button),0);
    }
    gtk_widget_show((dialog -> register_button));
  }
}

static gboolean username_focus_cb(GtkWidget *widget,GdkEventFocus *event,AccountPrefsDialog *dialog)
{
  GHashTable *table;
  const char *label;
  table = ( *( *(dialog -> prpl_info)).get_account_text_table)(0);
  label = (g_hash_table_lookup(table,"login_label"));
  if (!(strcmp(gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_entry_get_type())))),label) != 0)) {
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_entry_get_type()))),"");
    gtk_widget_modify_text(widget,GTK_STATE_NORMAL,0);
  }
  g_hash_table_destroy(table);
  return 0;
}

static void username_changed_cb(GtkEntry *entry,AccountPrefsDialog *dialog)
{
  if ((dialog -> ok_button) != 0) 
    gtk_widget_set_sensitive((dialog -> ok_button),(( *gtk_entry_get_text(entry)) != 0));
  if ((dialog -> register_button) != 0) {
    if (((dialog -> prpl_info) != ((PurplePluginProtocolInfo *)((void *)0))) && ((( *(dialog -> prpl_info)).options & OPT_PROTO_REGISTER_NOSCREENNAME) != 0U)) 
      gtk_widget_set_sensitive((dialog -> register_button),(!0));
    else 
      gtk_widget_set_sensitive((dialog -> register_button),(( *gtk_entry_get_text(entry)) != 0));
  }
}

static gboolean username_nofocus_cb(GtkWidget *widget,GdkEventFocus *event,AccountPrefsDialog *dialog)
{
  GdkColor color = {(0), (34952), (35466), (34181)};
  GHashTable *table = (GHashTable *)((void *)0);
  const char *label = (const char *)((void *)0);
  if (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).get_account_text_table))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).get_account_text_table))) < ( *(dialog -> prpl_info)).struct_size)) && (( *(dialog -> prpl_info)).get_account_text_table != ((GHashTable *(*)(PurpleAccount *))((void *)0)))) {
    table = ( *( *(dialog -> prpl_info)).get_account_text_table)(0);
    label = (g_hash_table_lookup(table,"login_label"));
    if (( *gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_entry_get_type()))))) == 0) {
/* We have to avoid hitting the username_changed_cb function
			 * because it enables buttons we don't want enabled yet ;)
			 */
      g_signal_handlers_block_matched(widget,((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )username_changed_cb),dialog);
      gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_entry_get_type()))),label);
/* Make sure we can hit it again */
      g_signal_handlers_unblock_matched(widget,((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )username_changed_cb),dialog);
      gtk_widget_modify_text(widget,GTK_STATE_NORMAL,(&color));
    }
    g_hash_table_destroy(table);
  }
  return 0;
}

static void icon_filesel_choose_cb(const char *filename,gpointer data)
{
  AccountPrefsDialog *dialog = data;
  if (filename != ((const char *)((void *)0))) {
    size_t len;
    gpointer data = pidgin_convert_buddy_icon((dialog -> plugin),filename,&len);
    set_dialog_icon(dialog,data,len,g_strdup(filename));
  }
  dialog -> icon_filesel = ((GtkWidget *)((void *)0));
}

static void icon_select_cb(GtkWidget *button,AccountPrefsDialog *dialog)
{
  dialog -> icon_filesel = pidgin_buddy_icon_chooser_new(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> window)),gtk_window_get_type()))),icon_filesel_choose_cb,dialog);
  gtk_widget_show_all((dialog -> icon_filesel));
}

static void icon_reset_cb(GtkWidget *button,AccountPrefsDialog *dialog)
{
  set_dialog_icon(dialog,0,0,0);
}

static void account_dnd_recv(GtkWidget *widget,GdkDragContext *dc,gint x,gint y,GtkSelectionData *sd,guint info,guint t,AccountPrefsDialog *dialog)
{
  gchar *name = (gchar *)(sd -> data);
  if (((sd -> length) >= 0) && ((sd -> format) == 8)) {
/* Well, it looks like the drag event was cool.
		 * Let's do something with it */
    if (!(g_ascii_strncasecmp(name,"file://",7) != 0)) {
      GError *converr = (GError *)((void *)0);
      gchar *tmp;
      gchar *rtmp;
      gpointer data;
      size_t len;
/* It looks like we're dealing with a local file. */
      if (!((tmp = g_filename_from_uri(name,0,&converr)) != 0)) {
        purple_debug(PURPLE_DEBUG_ERROR,"buddyicon","%s\n",((converr != 0)?(converr -> message) : "g_filename_from_uri error"));
        return ;
      }
      if (((rtmp = strchr(tmp,13)) != 0) || ((rtmp = strchr(tmp,10)) != 0)) 
         *rtmp = 0;
      data = pidgin_convert_buddy_icon((dialog -> plugin),tmp,&len);
/* This takes ownership of tmp */
      set_dialog_icon(dialog,data,len,tmp);
    }
    gtk_drag_finish(dc,(!0),0,t);
  }
  gtk_drag_finish(dc,0,0,t);
}

static void update_editable(PurpleConnection *gc,AccountPrefsDialog *dialog)
{
  GtkStyle *style;
  gboolean set;
  GList *l;
  if ((dialog -> account) == ((PurpleAccount *)((void *)0))) 
    return ;
  if ((gc != ((PurpleConnection *)((void *)0))) && ((dialog -> account) != purple_connection_get_account(gc))) 
    return ;
  set = !((purple_account_is_connected((dialog -> account)) != 0) || (purple_account_is_connecting((dialog -> account)) != 0));
  gtk_widget_set_sensitive((dialog -> protocol_menu),set);
  gtk_editable_set_editable(((GtkEditable *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> username_entry)),gtk_editable_get_type()))),set);
  style = ((set != 0)?((struct _GtkStyle *)((void *)0)) : gtk_widget_get_style((dialog -> username_entry)));
  gtk_widget_modify_base((dialog -> username_entry),GTK_STATE_NORMAL,((style != 0)?((style -> base) + GTK_STATE_INSENSITIVE) : ((struct _GdkColor *)((void *)0))));
  for (l = (dialog -> user_split_entries); l != ((GList *)((void *)0)); l = (l -> next)) {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)(l -> data);
      GType __t = gtk_editable_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
      gtk_editable_set_editable(((GtkEditable *)(g_type_check_instance_cast(((GTypeInstance *)(l -> data)),gtk_editable_get_type()))),set);
      style = ((set != 0)?((struct _GtkStyle *)((void *)0)) : gtk_widget_get_style(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(l -> data)),gtk_widget_get_type())))));
      gtk_widget_modify_base(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(l -> data)),gtk_widget_get_type()))),GTK_STATE_NORMAL,((style != 0)?((style -> base) + GTK_STATE_INSENSITIVE) : ((struct _GdkColor *)((void *)0))));
    }
    else {
      gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(l -> data)),gtk_widget_get_type()))),set);
    }
  }
}

static void add_login_options(AccountPrefsDialog *dialog,GtkWidget *parent)
{
  GtkWidget *frame;
  GtkWidget *hbox;
  GtkWidget *vbox;
  GtkWidget *entry;
  GtkWidget *menu;
  GtkWidget *item;
  GList *user_splits;
  GList *l;
  GList *l2;
  char *username = (char *)((void *)0);
  if ((dialog -> protocol_menu) != ((GtkWidget *)((void *)0))) {
#if GTK_CHECK_VERSION(2,12,0)
    g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> protocol_menu)),((GType )(20 << 2))))));
#else
#endif
    hbox = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> protocol_menu)),((GType )(20 << 2))))),"container"));
    gtk_container_remove(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_container_get_type()))),(dialog -> protocol_menu));
  }
  if ((dialog -> login_frame) != ((GtkWidget *)((void *)0))) 
    gtk_widget_destroy((dialog -> login_frame));
/* Build the login options frame. */
  frame = pidgin_make_frame(parent,((const char *)(dgettext("pidgin","Login Options"))));
/* cringe */
  dialog -> login_frame = gtk_widget_get_parent(gtk_widget_get_parent(frame));
  gtk_box_reorder_child(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)parent),gtk_box_get_type()))),(dialog -> login_frame),0);
  gtk_widget_show((dialog -> login_frame));
/* Main vbox */
  vbox = gtk_vbox_new(0,6);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)frame),gtk_container_get_type()))),vbox);
  gtk_widget_show(vbox);
/* Protocol */
  if ((dialog -> protocol_menu) == ((GtkWidget *)((void *)0))) {
    dialog -> protocol_menu = pidgin_protocol_option_menu_new((dialog -> protocol_id),((GCallback )set_account_protocol_cb),dialog);
#if GTK_CHECK_VERSION(2,12,0)
    g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> protocol_menu)),((GType )(20 << 2))))));
#else
#endif
  }
  hbox = add_pref_box(dialog,vbox,((const char *)(dgettext("pidgin","Pro_tocol:"))),(dialog -> protocol_menu));
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> protocol_menu)),((GType )(20 << 2))))),"container",hbox);
#if GTK_CHECK_VERSION(2,12,0)
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> protocol_menu)),((GType )(20 << 2))))));
#else
#endif
/* Username */
  dialog -> username_entry = gtk_entry_new();
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> username_entry)),((GType )(20 << 2))))),"truncate-multiline",!0,((void *)((void *)0)));
  add_pref_box(dialog,vbox,((const char *)(dgettext("pidgin","_Username:"))),(dialog -> username_entry));
  if ((dialog -> account) != ((PurpleAccount *)((void *)0))) 
    username = g_strdup(purple_account_get_username((dialog -> account)));
  if ((!(username != 0) && ((dialog -> prpl_info) != 0)) && (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).get_account_text_table))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).get_account_text_table))) < ( *(dialog -> prpl_info)).struct_size)) && (( *(dialog -> prpl_info)).get_account_text_table != ((GHashTable *(*)(PurpleAccount *))((void *)0))))) {
    GdkColor color = {(0), (34952), (35466), (34181)};
    GHashTable *table;
    const char *label;
    table = ( *( *(dialog -> prpl_info)).get_account_text_table)(0);
    label = (g_hash_table_lookup(table,"login_label"));
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> username_entry)),gtk_entry_get_type()))),label);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> username_entry)),((GType )(20 << 2))))),"focus-in-event",((GCallback )username_focus_cb),dialog,0,((GConnectFlags )0));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> username_entry)),((GType )(20 << 2))))),"focus-out-event",((GCallback )username_nofocus_cb),dialog,0,((GConnectFlags )0));
    gtk_widget_modify_text((dialog -> username_entry),GTK_STATE_NORMAL,(&color));
    g_hash_table_destroy(table);
  }
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> username_entry)),((GType )(20 << 2))))),"changed",((GCallback )username_changed_cb),dialog,0,((GConnectFlags )0));
/* Do the user split thang */
  if ((dialog -> prpl_info) == ((PurplePluginProtocolInfo *)((void *)0))) 
    user_splits = ((GList *)((void *)0));
  else 
    user_splits = ( *(dialog -> prpl_info)).user_splits;
  if ((dialog -> user_split_entries) != ((GList *)((void *)0))) {
    g_list_free((dialog -> user_split_entries));
    dialog -> user_split_entries = ((GList *)((void *)0));
  }
  for (l = user_splits; l != ((GList *)((void *)0)); l = (l -> next)) {
    PurpleAccountUserSplit *split = (l -> data);
    char *buf;
    buf = g_strdup_printf("_%s:",purple_account_user_split_get_text(split));
    entry = gtk_entry_new();
    add_pref_box(dialog,vbox,buf,entry);
    g_free(buf);
    dialog -> user_split_entries = g_list_append((dialog -> user_split_entries),entry);
  }
  for (((l = g_list_last((dialog -> user_split_entries))) , (l2 = g_list_last(user_splits))); (l != ((GList *)((void *)0))) && (l2 != ((GList *)((void *)0))); ((l = (l -> prev)) , (l2 = (l2 -> prev)))) {
    GtkWidget *entry = (l -> data);
    PurpleAccountUserSplit *split = (l2 -> data);
    const char *value = (const char *)((void *)0);
    char *c;
    if ((dialog -> account) != ((PurpleAccount *)((void *)0))) {
      if (purple_account_user_split_get_reverse(split) != 0) 
        c = strrchr(username,(purple_account_user_split_get_separator(split)));
      else 
        c = strchr(username,(purple_account_user_split_get_separator(split)));
      if (c != ((char *)((void *)0))) {
         *c = 0;
        c++;
        value = c;
      }
    }
    if (value == ((const char *)((void *)0))) 
      value = purple_account_user_split_get_default_value(split);
/* Google Talk default domain hackery! */
    menu = gtk_option_menu_get_menu(((GtkOptionMenu *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> protocol_menu)),gtk_option_menu_get_type()))));
    item = gtk_menu_get_active(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_get_type()))));
    if (((value == ((const char *)((void *)0))) && (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"fakegoogle") != 0)) && !(strcmp(purple_account_user_split_get_text(split),((const char *)(dgettext("pidgin","Domain")))) != 0)) 
      value = "gmail.com";
    if (((value == ((const char *)((void *)0))) && (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"fakefacebook") != 0)) && !(strcmp(purple_account_user_split_get_text(split),((const char *)(dgettext("pidgin","Domain")))) != 0)) 
      value = "chat.facebook.com";
    if (value != ((const char *)((void *)0))) 
      gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),value);
  }
  if (username != ((char *)((void *)0))) 
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> username_entry)),gtk_entry_get_type()))),username);
  g_free(username);
/* Password */
  dialog -> password_entry = gtk_entry_new();
  gtk_entry_set_visibility(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> password_entry)),gtk_entry_get_type()))),0);
#if !GTK_CHECK_VERSION(2,16,0)
#endif /* Less than GTK+ 2.16 */
  dialog -> password_box = add_pref_box(dialog,vbox,((const char *)(dgettext("pidgin","_Password:"))),(dialog -> password_entry));
/* Remember Password */
  dialog -> remember_pass_check = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Remember pass_word"))));
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> remember_pass_check)),gtk_toggle_button_get_type()))),0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),(dialog -> remember_pass_check),0,0,0);
  gtk_widget_show((dialog -> remember_pass_check));
/* Set the fields. */
  if ((dialog -> account) != ((PurpleAccount *)((void *)0))) {
    if ((purple_account_get_password((dialog -> account)) != 0) && (purple_account_get_remember_password((dialog -> account)) != 0)) 
      gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> password_entry)),gtk_entry_get_type()))),purple_account_get_password((dialog -> account)));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> remember_pass_check)),gtk_toggle_button_get_type()))),purple_account_get_remember_password((dialog -> account)));
  }
  if (((dialog -> prpl_info) != ((PurplePluginProtocolInfo *)((void *)0))) && ((( *(dialog -> prpl_info)).options & OPT_PROTO_NO_PASSWORD) != 0U)) {
    gtk_widget_hide((dialog -> password_box));
    gtk_widget_hide((dialog -> remember_pass_check));
  }
/* Do not let the user change the protocol/username while connected. */
  update_editable(0,dialog);
  purple_signal_connect(purple_connections_get_handle(),"signing-on",dialog,((GCallback )update_editable),dialog);
  purple_signal_connect(purple_connections_get_handle(),"signed-off",dialog,((GCallback )update_editable),dialog);
}

static void icon_check_cb(GtkWidget *checkbox,AccountPrefsDialog *dialog)
{
  gtk_widget_set_sensitive((dialog -> icon_hbox),gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_check)),gtk_toggle_button_get_type())))));
}

static void add_user_options(AccountPrefsDialog *dialog,GtkWidget *parent)
{
  GtkWidget *frame;
  GtkWidget *vbox;
  GtkWidget *vbox2;
  GtkWidget *hbox;
  GtkWidget *hbox2;
  GtkWidget *button;
  GtkWidget *label;
  if ((dialog -> user_frame) != ((GtkWidget *)((void *)0))) 
    gtk_widget_destroy((dialog -> user_frame));
/* Build the user options frame. */
  frame = pidgin_make_frame(parent,((const char *)(dgettext("pidgin","User Options"))));
  dialog -> user_frame = gtk_widget_get_parent(gtk_widget_get_parent(frame));
  gtk_box_reorder_child(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)parent),gtk_box_get_type()))),(dialog -> user_frame),1);
  gtk_widget_show((dialog -> user_frame));
/* Main vbox */
  vbox = gtk_vbox_new(0,6);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)frame),gtk_container_get_type()))),vbox);
  gtk_widget_show(vbox);
/* Alias */
  dialog -> alias_entry = gtk_entry_new();
  add_pref_box(dialog,vbox,((const char *)(dgettext("pidgin","_Local alias:"))),(dialog -> alias_entry));
/* New mail notifications */
  dialog -> new_mail_check = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","New _mail notifications"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),(dialog -> new_mail_check),0,0,0);
  gtk_widget_show((dialog -> new_mail_check));
/* Buddy icon */
  dialog -> icon_check = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Use this buddy _icon for this account:"))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_check)),((GType )(20 << 2))))),"toggled",((GCallback )icon_check_cb),dialog,0,((GConnectFlags )0));
  gtk_widget_show((dialog -> icon_check));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),(dialog -> icon_check),0,0,0);
  dialog -> icon_hbox = (hbox = gtk_hbox_new(0,6));
  gtk_widget_set_sensitive(hbox,gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_check)),gtk_toggle_button_get_type())))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  gtk_widget_show(hbox);
  label = gtk_label_new("    ");
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  gtk_widget_show(label);
  button = gtk_button_new();
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),button,0,0,0);
  gtk_widget_show(button);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )icon_select_cb),dialog,0,((GConnectFlags )0));
  dialog -> icon_entry = gtk_image_new();
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_container_get_type()))),(dialog -> icon_entry));
  gtk_widget_show((dialog -> icon_entry));
/* TODO: Uh, isn't this next line pretty useless? */
  pidgin_set_accessible_label((dialog -> icon_entry),label);
  purple_imgstore_unref((dialog -> icon_img));
  dialog -> icon_img = ((PurpleStoredImage *)((void *)0));
  vbox2 = gtk_vbox_new(0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),vbox2,(!0),(!0),0);
  gtk_widget_show(vbox2);
  hbox2 = gtk_hbox_new(0,6);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox2),gtk_box_get_type()))),hbox2,0,0,12);
  gtk_widget_show(hbox2);
  button = gtk_button_new_from_stock("gtk-remove");
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )icon_reset_cb),dialog,0,((GConnectFlags )0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox2),gtk_box_get_type()))),button,0,0,0);
  gtk_widget_show(button);
  if ((dialog -> prpl_info) != ((PurplePluginProtocolInfo *)((void *)0))) {
    if (!((( *(dialog -> prpl_info)).options & OPT_PROTO_MAIL_CHECK) != 0U)) 
      gtk_widget_hide((dialog -> new_mail_check));
    if (( *(dialog -> prpl_info)).icon_spec.format == ((char *)((void *)0))) {
      gtk_widget_hide((dialog -> icon_check));
      gtk_widget_hide((dialog -> icon_hbox));
    }
  }
  if ((dialog -> account) != ((PurpleAccount *)((void *)0))) {
    PurpleStoredImage *img;
    gpointer data = (gpointer )((void *)0);
    size_t len = 0;
    if (purple_account_get_alias((dialog -> account)) != 0) 
      gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> alias_entry)),gtk_entry_get_type()))),purple_account_get_alias((dialog -> account)));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> new_mail_check)),gtk_toggle_button_get_type()))),purple_account_get_check_mail((dialog -> account)));
    gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_check)),gtk_toggle_button_get_type()))),!(purple_account_get_bool((dialog -> account),"use-global-buddyicon",(!0)) != 0));
    img = purple_buddy_icons_find_account_icon((dialog -> account));
    if (img != 0) {
      len = purple_imgstore_get_size(img);
      data = g_memdup(purple_imgstore_get_data(img),len);
    }
    set_dialog_icon(dialog,data,len,g_strdup(purple_account_get_buddy_icon_path((dialog -> account))));
  }
  else {
    set_dialog_icon(dialog,0,0,0);
  }
#if 0
/* Nothing to see :( aww. */
#endif
}

static void add_protocol_options(AccountPrefsDialog *dialog)
{
  PurpleAccountOption *option;
  PurpleAccount *account;
  GtkWidget *vbox;
  GtkWidget *check;
  GtkWidget *entry;
  GtkWidget *combo;
  GtkWidget *menu;
  GtkWidget *item;
  GList *list;
  GList *node;
  gint i;
  gint idx;
  gint int_value;
  GtkListStore *model;
  GtkTreeIter iter;
  GtkCellRenderer *renderer;
  PurpleKeyValuePair *kvp;
  GList *l;
  char buf[1024UL];
  char *title;
  char *tmp;
  const char *str_value;
  gboolean bool_value;
  ProtocolOptEntry *opt_entry;
  if ((dialog -> protocol_frame) != ((GtkWidget *)((void *)0))) {
    gtk_notebook_remove_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> notebook)),gtk_notebook_get_type()))),1);
    dialog -> protocol_frame = ((GtkWidget *)((void *)0));
  }
  while((dialog -> protocol_opt_entries) != ((GList *)((void *)0))){
    ProtocolOptEntry *opt_entry = ( *(dialog -> protocol_opt_entries)).data;
    g_free((opt_entry -> setting));
    g_free(opt_entry);
    dialog -> protocol_opt_entries = g_list_delete_link((dialog -> protocol_opt_entries),(dialog -> protocol_opt_entries));
  }
  if (((dialog -> prpl_info) == ((PurplePluginProtocolInfo *)((void *)0))) || (( *(dialog -> prpl_info)).protocol_options == ((GList *)((void *)0)))) 
    return ;
  account = (dialog -> account);
/* Main vbox */
  dialog -> protocol_frame = (vbox = gtk_vbox_new(0,6));
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),12);
  gtk_notebook_insert_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> notebook)),gtk_notebook_get_type()))),vbox,gtk_label_new_with_mnemonic(((const char *)(dgettext("pidgin","Ad_vanced")))),1);
  gtk_widget_show(vbox);
  menu = gtk_option_menu_get_menu(((GtkOptionMenu *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> protocol_menu)),gtk_option_menu_get_type()))));
  item = gtk_menu_get_active(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_get_type()))));
  for (l = ( *(dialog -> prpl_info)).protocol_options; l != ((GList *)((void *)0)); l = (l -> next)) {
    option = ((PurpleAccountOption *)(l -> data));
    opt_entry = ((ProtocolOptEntry *)(g_malloc0_n(1,(sizeof(ProtocolOptEntry )))));
    opt_entry -> type = purple_account_option_get_type(option);
    opt_entry -> setting = g_strdup(purple_account_option_get_setting(option));
    switch(opt_entry -> type){
      case PURPLE_PREF_BOOLEAN:
{
        if ((account == ((PurpleAccount *)((void *)0))) || (strcmp(purple_account_get_protocol_id(account),(dialog -> protocol_id)) != 0)) {
          bool_value = purple_account_option_get_default_bool(option);
        }
        else {
          bool_value = purple_account_get_bool(account,purple_account_option_get_setting(option),purple_account_option_get_default_bool(option));
        }
        tmp = g_strconcat("_",purple_account_option_get_text(option),((void *)((void *)0)));
        opt_entry -> widget = (check = gtk_check_button_new_with_mnemonic(tmp));
        g_free(tmp);
        gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)check),gtk_toggle_button_get_type()))),bool_value);
        gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),check,0,0,0);
        gtk_widget_show(check);
        break; 
      }
      case PURPLE_PREF_INT:
{
        if ((account == ((PurpleAccount *)((void *)0))) || (strcmp(purple_account_get_protocol_id(account),(dialog -> protocol_id)) != 0)) {
          int_value = purple_account_option_get_default_int(option);
        }
        else {
          int_value = purple_account_get_int(account,purple_account_option_get_setting(option),purple_account_option_get_default_int(option));
        }
        g_snprintf(buf,(sizeof(buf)),"%d",int_value);
        opt_entry -> widget = (entry = gtk_entry_new());
        gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),buf);
        title = g_strdup_printf("_%s:",purple_account_option_get_text(option));
        add_pref_box(dialog,vbox,title,entry);
        g_free(title);
        break; 
      }
      case PURPLE_PREF_STRING:
{
        if ((account == ((PurpleAccount *)((void *)0))) || (strcmp(purple_account_get_protocol_id(account),(dialog -> protocol_id)) != 0)) {
          str_value = purple_account_option_get_default_string(option);
        }
        else {
          str_value = purple_account_get_string(account,purple_account_option_get_setting(option),purple_account_option_get_default_string(option));
        }
        opt_entry -> widget = (entry = gtk_entry_new());
        if (purple_account_option_get_masked(option) != 0) {
          gtk_entry_set_visibility(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),0);
#if !GTK_CHECK_VERSION(2,16,0)
#endif /* Less than GTK+ 2.16 */
        }
        if (str_value != ((const char *)((void *)0))) 
          gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),str_value);
        title = g_strdup_printf("_%s:",purple_account_option_get_text(option));
        add_pref_box(dialog,vbox,title,entry);
        g_free(title);
        break; 
      }
      case PURPLE_PREF_STRING_LIST:
{
        i = 0;
        idx = 0;
        if ((account == ((PurpleAccount *)((void *)0))) || (strcmp(purple_account_get_protocol_id(account),(dialog -> protocol_id)) != 0)) {
          str_value = purple_account_option_get_default_list_value(option);
        }
        else {
          str_value = purple_account_get_string(account,purple_account_option_get_setting(option),purple_account_option_get_default_list_value(option));
        }
        list = purple_account_option_get_list(option);
        model = gtk_list_store_new(2,((GType )(16 << 2)),((GType )(17 << 2)));
        opt_entry -> widget = (combo = gtk_combo_box_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type())))));
        if ((g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"fakefacebook") != 0) && !(strcmp((opt_entry -> setting),"connection_security") != 0)) 
          str_value = "opportunistic_tls";
/* Loop through list of PurpleKeyValuePair items */
        for (node = list; node != ((GList *)((void *)0)); node = (node -> next)) {
          if ((node -> data) != ((void *)((void *)0))) {
            kvp = ((PurpleKeyValuePair *)(node -> data));
            if ((((kvp -> value) != ((void *)((void *)0))) && (str_value != ((const char *)((void *)0)))) && !(g_utf8_collate((kvp -> value),str_value) != 0)) 
              idx = i;
            gtk_list_store_append(model,&iter);
            gtk_list_store_set(model,&iter,0,(kvp -> key),1,(kvp -> value),-1);
          }
          i++;
        }
/* Set default */
        gtk_combo_box_set_active(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)combo),gtk_combo_box_get_type()))),idx);
/* Define renderer */
        renderer = gtk_cell_renderer_text_new();
        gtk_cell_layout_pack_start(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)combo),gtk_cell_layout_get_type()))),renderer,(!0));
        gtk_cell_layout_set_attributes(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)combo),gtk_cell_layout_get_type()))),renderer,"text",0,((void *)((void *)0)));
        title = g_strdup_printf("_%s:",purple_account_option_get_text(option));
        add_pref_box(dialog,vbox,title,combo);
        g_free(title);
        break; 
      }
      default:
{
        purple_debug_error("gtkaccount","Invalid Account Option pref type (%d)\n",(opt_entry -> type));
        g_free((opt_entry -> setting));
        g_free(opt_entry);
        continue; 
      }
    }
    dialog -> protocol_opt_entries = g_list_append((dialog -> protocol_opt_entries),opt_entry);
  }
}

static GtkWidget *make_proxy_dropdown()
{
  GtkWidget *dropdown;
  GtkListStore *model;
  GtkTreeIter iter;
  GtkCellRenderer *renderer;
  model = gtk_list_store_new(2,((GType )(16 << 2)),((GType )(6 << 2)));
  dropdown = gtk_combo_box_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))));
  gtk_list_store_append(model,&iter);
  gtk_list_store_set(model,&iter,0,((purple_running_gnome() != 0)?((const char *)(dgettext("pidgin","Use GNOME Proxy Settings"))) : ((const char *)(dgettext("pidgin","Use Global Proxy Settings")))),1,PURPLE_PROXY_USE_GLOBAL,-1);
  gtk_list_store_append(model,&iter);
  gtk_list_store_set(model,&iter,0,((const char *)(dgettext("pidgin","No Proxy"))),1,PURPLE_PROXY_NONE,-1);
  gtk_list_store_append(model,&iter);
  gtk_list_store_set(model,&iter,0,((const char *)(dgettext("pidgin","SOCKS 4"))),1,PURPLE_PROXY_SOCKS4,-1);
  gtk_list_store_append(model,&iter);
  gtk_list_store_set(model,&iter,0,((const char *)(dgettext("pidgin","SOCKS 5"))),1,PURPLE_PROXY_SOCKS5,-1);
  gtk_list_store_append(model,&iter);
  gtk_list_store_set(model,&iter,0,((const char *)(dgettext("pidgin","Tor/Privacy (SOCKS5)"))),1,PURPLE_PROXY_TOR,-1);
  gtk_list_store_append(model,&iter);
  gtk_list_store_set(model,&iter,0,((const char *)(dgettext("pidgin","HTTP"))),1,PURPLE_PROXY_HTTP,-1);
  gtk_list_store_append(model,&iter);
  gtk_list_store_set(model,&iter,0,((const char *)(dgettext("pidgin","Use Environmental Settings"))),1,PURPLE_PROXY_USE_ENVVAR,-1);
  renderer = gtk_cell_renderer_text_new();
  gtk_cell_layout_pack_start(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)dropdown),gtk_cell_layout_get_type()))),renderer,(!0));
  gtk_cell_layout_set_attributes(((GtkCellLayout *)(g_type_check_instance_cast(((GTypeInstance *)dropdown),gtk_cell_layout_get_type()))),renderer,"text",0,((void *)((void *)0)));
  return dropdown;
}

static void proxy_type_changed_cb(GtkWidget *menu,AccountPrefsDialog *dialog)
{
  GtkTreeIter iter;
  if (gtk_combo_box_get_active_iter(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_combo_box_get_type()))),&iter) != 0) {
    int int_value;
    gtk_tree_model_get(gtk_combo_box_get_model(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_combo_box_get_type())))),&iter,1,&int_value,-1);
    dialog -> new_proxy_type = int_value;
  }
  if ((((dialog -> new_proxy_type) == PURPLE_PROXY_USE_GLOBAL) || ((dialog -> new_proxy_type) == PURPLE_PROXY_NONE)) || ((dialog -> new_proxy_type) == PURPLE_PROXY_USE_ENVVAR)) {
    gtk_widget_hide_all((dialog -> proxy_vbox));
  }
  else 
    gtk_widget_show_all((dialog -> proxy_vbox));
}

static void port_popup_cb(GtkWidget *w,GtkMenu *menu,gpointer data)
{
  GtkWidget *item1;
  GtkWidget *item2;
/* This is an easter egg.
	   It means one of two things, both intended as humourus:
	   A) your network is really slow and you have nothing better to do than
	      look at butterflies.
	   B)You are looking really closely at something that shouldn't matter. */
  item1 = gtk_menu_item_new_with_label(((const char *)(dgettext("pidgin","If you look real closely"))));
/* This is an easter egg. See the comment on the previous line in the source. */
  item2 = gtk_menu_item_new_with_label(((const char *)(dgettext("pidgin","you can see the butterflies mating"))));
  gtk_widget_show(item1);
  gtk_widget_show(item2);
/* Prepend these in reverse order so they appear correctly. */
  gtk_menu_shell_prepend(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item2);
  gtk_menu_shell_prepend(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item1);
}

static void add_proxy_options(AccountPrefsDialog *dialog,GtkWidget *parent)
{
  PurpleProxyInfo *proxy_info;
  GtkWidget *vbox;
  GtkWidget *vbox2;
  GtkTreeIter iter;
  GtkTreeModel *proxy_model;
  if ((dialog -> proxy_frame) != ((GtkWidget *)((void *)0))) 
    gtk_widget_destroy((dialog -> proxy_frame));
/* Main vbox */
  dialog -> proxy_frame = (vbox = gtk_vbox_new(0,6));
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)parent),gtk_container_get_type()))),vbox);
  gtk_widget_show(vbox);
/* Proxy Type drop-down. */
  dialog -> proxy_dropdown = make_proxy_dropdown();
  add_pref_box(dialog,vbox,((const char *)(dgettext("pidgin","Proxy _type:"))),(dialog -> proxy_dropdown));
/* Setup the second vbox, which may be hidden at times. */
  dialog -> proxy_vbox = (vbox2 = gtk_vbox_new(0,6));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),vbox2,0,0,12);
  gtk_widget_show(vbox2);
/* Host */
  dialog -> proxy_host_entry = gtk_entry_new();
  add_pref_box(dialog,vbox2,((const char *)(dgettext("pidgin","_Host:"))),(dialog -> proxy_host_entry));
/* Port */
  dialog -> proxy_port_entry = gtk_entry_new();
  add_pref_box(dialog,vbox2,((const char *)(dgettext("pidgin","_Port:"))),(dialog -> proxy_port_entry));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> proxy_port_entry)),((GType )(20 << 2))))),"populate-popup",((GCallback )port_popup_cb),0,0,((GConnectFlags )0));
/* User */
  dialog -> proxy_user_entry = gtk_entry_new();
  add_pref_box(dialog,vbox2,((const char *)(dgettext("pidgin","_Username:"))),(dialog -> proxy_user_entry));
/* Password */
  dialog -> proxy_pass_entry = gtk_entry_new();
  gtk_entry_set_visibility(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> proxy_pass_entry)),gtk_entry_get_type()))),0);
#if !GTK_CHECK_VERSION(2,16,0)
#endif /* Less than GTK+ 2.16 */
  add_pref_box(dialog,vbox2,((const char *)(dgettext("pidgin","Pa_ssword:"))),(dialog -> proxy_pass_entry));
  if (((dialog -> account) != ((PurpleAccount *)((void *)0))) && ((proxy_info = purple_account_get_proxy_info((dialog -> account))) != ((PurpleProxyInfo *)((void *)0)))) {
    const char *value;
    int int_val;
    dialog -> new_proxy_type = purple_proxy_info_get_type(proxy_info);
    if ((value = purple_proxy_info_get_host(proxy_info)) != ((const char *)((void *)0))) 
      gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> proxy_host_entry)),gtk_entry_get_type()))),value);
    if ((int_val = purple_proxy_info_get_port(proxy_info)) != 0) {
      char buf[11UL];
      g_snprintf(buf,(sizeof(buf)),"%d",int_val);
      gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> proxy_port_entry)),gtk_entry_get_type()))),buf);
    }
    if ((value = purple_proxy_info_get_username(proxy_info)) != ((const char *)((void *)0))) 
      gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> proxy_user_entry)),gtk_entry_get_type()))),value);
    if ((value = purple_proxy_info_get_password(proxy_info)) != ((const char *)((void *)0))) 
      gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> proxy_pass_entry)),gtk_entry_get_type()))),value);
  }
  else 
    dialog -> new_proxy_type = PURPLE_PROXY_USE_GLOBAL;
  proxy_model = gtk_combo_box_get_model(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> proxy_dropdown)),gtk_combo_box_get_type()))));
  if (gtk_tree_model_get_iter_first(proxy_model,&iter) != 0) {
    int int_val;
{
      do {
        gtk_tree_model_get(proxy_model,&iter,1,&int_val,-1);
        if (int_val == (dialog -> new_proxy_type)) {
          gtk_combo_box_set_active_iter(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> proxy_dropdown)),gtk_combo_box_get_type()))),&iter);
          break; 
        }
      }while (gtk_tree_model_iter_next(proxy_model,&iter) != 0);
    }
  }
  proxy_type_changed_cb((dialog -> proxy_dropdown),dialog);
/* Connect signals. */
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> proxy_dropdown)),((GType )(20 << 2))))),"changed",((GCallback )proxy_type_changed_cb),dialog,0,((GConnectFlags )0));
}

static void add_voice_options(AccountPrefsDialog *dialog)
{
#ifdef USE_VV
  if (!((dialog -> prpl_info) != 0) || !(((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).initiate_media))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).initiate_media))) < ( *(dialog -> prpl_info)).struct_size)) && (( *(dialog -> prpl_info)).initiate_media != ((gboolean (*)(PurpleAccount *, const char *, PurpleMediaSessionType ))((void *)0))))) {
    if ((dialog -> voice_frame) != 0) {
      gtk_widget_destroy((dialog -> voice_frame));
      dialog -> voice_frame = ((GtkWidget *)((void *)0));
      dialog -> suppression_check = ((GtkWidget *)((void *)0));
    }
    return ;
  }
  if (!((dialog -> voice_frame) != 0)) {
    dialog -> voice_frame = gtk_vbox_new(0,12);
    gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> voice_frame)),gtk_container_get_type()))),12);
    dialog -> suppression_check = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Use _silence suppression"))));
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> voice_frame)),gtk_box_get_type()))),(dialog -> suppression_check),0,0,0);
    gtk_notebook_append_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> notebook)),gtk_notebook_get_type()))),(dialog -> voice_frame),gtk_label_new_with_mnemonic(((const char *)(dgettext("pidgin","_Voice and Video")))));
    gtk_widget_show_all((dialog -> voice_frame));
  }
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> suppression_check)),gtk_toggle_button_get_type()))),purple_account_get_silence_suppression((dialog -> account)));
#endif
}

static gboolean account_win_destroy_cb(GtkWidget *w,GdkEvent *event,AccountPrefsDialog *dialog)
{
  g_hash_table_remove(account_pref_wins,(dialog -> account));
  gtk_widget_destroy((dialog -> window));
  g_list_free((dialog -> user_split_entries));
  while((dialog -> protocol_opt_entries) != ((GList *)((void *)0))){
    ProtocolOptEntry *opt_entry = ( *(dialog -> protocol_opt_entries)).data;
    g_free((opt_entry -> setting));
    g_free(opt_entry);
    dialog -> protocol_opt_entries = g_list_delete_link((dialog -> protocol_opt_entries),(dialog -> protocol_opt_entries));
  }
  g_free((dialog -> protocol_id));
  g_object_unref((dialog -> sg));
  purple_imgstore_unref((dialog -> icon_img));
  if ((dialog -> icon_filesel) != 0) 
    gtk_widget_destroy((dialog -> icon_filesel));
  purple_signals_disconnect_by_handle(dialog);
  g_free(dialog);
  return 0;
}

static void cancel_account_prefs_cb(GtkWidget *w,AccountPrefsDialog *dialog)
{
  account_win_destroy_cb(0,0,dialog);
}

static void ok_account_prefs_cb(GtkWidget *w,AccountPrefsDialog *dialog)
{
  PurpleProxyInfo *proxy_info = (PurpleProxyInfo *)((void *)0);
  GList *l;
  GList *l2;
  const char *value;
  char *username;
  char *tmp;
  gboolean new_acct = 0;
  gboolean icon_change = 0;
  PurpleAccount *account;
/* Build the username string. */
  username = g_strdup(gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> username_entry)),gtk_entry_get_type())))));
  if ((dialog -> prpl_info) != ((PurplePluginProtocolInfo *)((void *)0))) {
    for (((l = ( *(dialog -> prpl_info)).user_splits) , (l2 = (dialog -> user_split_entries))); (l != ((GList *)((void *)0))) && (l2 != ((GList *)((void *)0))); ((l = (l -> next)) , (l2 = (l2 -> next)))) {
      PurpleAccountUserSplit *split = (l -> data);
      GtkEntry *entry = (l2 -> data);
      char sep[2UL] = " ";
      value = gtk_entry_get_text(entry);
       *sep = purple_account_user_split_get_separator(split);
      tmp = g_strconcat(username,sep,((( *value) != 0)?value : purple_account_user_split_get_default_value(split)),((void *)((void *)0)));
      g_free(username);
      username = tmp;
    }
  }
  if ((dialog -> account) == ((PurpleAccount *)((void *)0))) {
    if (purple_accounts_find(username,(dialog -> protocol_id)) != ((PurpleAccount *)((void *)0))) {
      purple_debug_warning("gtkaccount","Trying to add a duplicate %s account (%s).\n",(dialog -> protocol_id),username);
      purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Unable to save new account"))),((const char *)(dgettext("pidgin","An account already exists with the specified criteria."))),0,0);
      g_free(username);
      return ;
    }
    if (purple_accounts_get_all() == ((GList *)((void *)0))) {
/* We're adding our first account.  Be polite and show the buddy list */
      purple_blist_set_visible((!0));
    }
    account = purple_account_new(username,(dialog -> protocol_id));
    new_acct = (!0);
  }
  else {
    account = (dialog -> account);
/* Protocol */
    purple_account_set_protocol_id(account,(dialog -> protocol_id));
  }
/* Alias */
  value = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> alias_entry)),gtk_entry_get_type()))));
  if (( *value) != 0) 
    purple_account_set_alias(account,value);
  else 
    purple_account_set_alias(account,0);
/* Buddy Icon */
  if (((dialog -> prpl_info) != ((PurplePluginProtocolInfo *)((void *)0))) && (( *(dialog -> prpl_info)).icon_spec.format != ((char *)((void *)0)))) {
    const char *filename;
    if ((new_acct != 0) || (purple_account_get_bool(account,"use-global-buddyicon",(!0)) == gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_check)),gtk_toggle_button_get_type())))))) {
      icon_change = (!0);
    }
    purple_account_set_bool(account,"use-global-buddyicon",!(gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_check)),gtk_toggle_button_get_type())))) != 0));
    if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_check)),gtk_toggle_button_get_type())))) != 0) {
      if ((dialog -> icon_img) != 0) {
        size_t len = purple_imgstore_get_size((dialog -> icon_img));
        purple_buddy_icons_set_account_icon(account,(g_memdup(purple_imgstore_get_data((dialog -> icon_img)),len)),len);
        purple_account_set_buddy_icon_path(account,purple_imgstore_get_filename((dialog -> icon_img)));
      }
      else {
        purple_buddy_icons_set_account_icon(account,0,0);
        purple_account_set_buddy_icon_path(account,0);
      }
    }
    else if (((filename = purple_prefs_get_path("/pidgin/accounts/buddyicon")) != 0) && (icon_change != 0)) {
      size_t len;
      gpointer data = pidgin_convert_buddy_icon((dialog -> plugin),filename,&len);
      purple_account_set_buddy_icon_path(account,filename);
      purple_buddy_icons_set_account_icon(account,data,len);
    }
  }
/* Remember Password */
  purple_account_set_remember_password(account,gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> remember_pass_check)),gtk_toggle_button_get_type())))));
/* Check Mail */
  if (((dialog -> prpl_info) != 0) && ((( *(dialog -> prpl_info)).options & OPT_PROTO_MAIL_CHECK) != 0U)) 
    purple_account_set_check_mail(account,gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> new_mail_check)),gtk_toggle_button_get_type())))));
/* Password */
  value = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> password_entry)),gtk_entry_get_type()))));
/*
	 * We set the password if this is a new account because new accounts
	 * will be set to online, and if the user has entered a password into
	 * the account editor (but has not checked the 'save' box), then we
	 * don't want to prompt them.
	 */
  if (((purple_account_get_remember_password(account) != 0) || (new_acct != 0)) && (( *value) != 0)) 
    purple_account_set_password(account,value);
  else 
    purple_account_set_password(account,0);
  purple_account_set_username(account,username);
  g_free(username);
/* Add the protocol settings */
  if ((dialog -> prpl_info) != 0) {
    ProtocolOptEntry *opt_entry;
    GtkTreeIter iter;
    char *value2;
    int int_value;
    gboolean bool_value;
    for (l2 = (dialog -> protocol_opt_entries); l2 != 0; l2 = (l2 -> next)) {
      opt_entry = (l2 -> data);
      switch(opt_entry -> type){
        case PURPLE_PREF_STRING:
{
          value = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(opt_entry -> widget)),gtk_entry_get_type()))));
          purple_account_set_string(account,(opt_entry -> setting),value);
          break; 
        }
        case PURPLE_PREF_INT:
{
          int_value = atoi(gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(opt_entry -> widget)),gtk_entry_get_type())))));
          purple_account_set_int(account,(opt_entry -> setting),int_value);
          break; 
        }
        case PURPLE_PREF_BOOLEAN:
{
          bool_value = gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(opt_entry -> widget)),gtk_toggle_button_get_type()))));
          purple_account_set_bool(account,(opt_entry -> setting),bool_value);
          break; 
        }
        case PURPLE_PREF_STRING_LIST:
{
          value2 = ((char *)((void *)0));
          if (gtk_combo_box_get_active_iter(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(opt_entry -> widget)),gtk_combo_box_get_type()))),&iter) != 0) 
            gtk_tree_model_get(gtk_combo_box_get_model(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(opt_entry -> widget)),gtk_combo_box_get_type())))),&iter,1,&value2,-1);
          purple_account_set_string(account,(opt_entry -> setting),value2);
          break; 
        }
        default:
{
          break; 
        }
      }
    }
  }
/* Set the proxy stuff. */
  proxy_info = purple_account_get_proxy_info(account);
/* Create the proxy info if it doesn't exist. */
  if (proxy_info == ((PurpleProxyInfo *)((void *)0))) {
    proxy_info = purple_proxy_info_new();
    purple_account_set_proxy_info(account,proxy_info);
  }
/* Set the proxy info type. */
  purple_proxy_info_set_type(proxy_info,(dialog -> new_proxy_type));
/* Host */
  value = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> proxy_host_entry)),gtk_entry_get_type()))));
  if (( *value) != 0) 
    purple_proxy_info_set_host(proxy_info,value);
  else 
    purple_proxy_info_set_host(proxy_info,0);
/* Port */
  value = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> proxy_port_entry)),gtk_entry_get_type()))));
  if (( *value) != 0) 
    purple_proxy_info_set_port(proxy_info,atoi(value));
  else 
    purple_proxy_info_set_port(proxy_info,0);
/* Username */
  value = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> proxy_user_entry)),gtk_entry_get_type()))));
  if (( *value) != 0) 
    purple_proxy_info_set_username(proxy_info,value);
  else 
    purple_proxy_info_set_username(proxy_info,0);
/* Password */
  value = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> proxy_pass_entry)),gtk_entry_get_type()))));
  if (( *value) != 0) 
    purple_proxy_info_set_password(proxy_info,value);
  else 
    purple_proxy_info_set_password(proxy_info,0);
/* If there are no values set then proxy_info NULL */
  if ((((((purple_proxy_info_get_type(proxy_info)) == PURPLE_PROXY_USE_GLOBAL) && (purple_proxy_info_get_host(proxy_info) == ((const char *)((void *)0)))) && (purple_proxy_info_get_port(proxy_info) == 0)) && (purple_proxy_info_get_username(proxy_info) == ((const char *)((void *)0)))) && (purple_proxy_info_get_password(proxy_info) == ((const char *)((void *)0)))) {
    purple_account_set_proxy_info(account,0);
    proxy_info = ((PurpleProxyInfo *)((void *)0));
  }
/* Voice and Video settings */
  if ((dialog -> voice_frame) != 0) {
    purple_account_set_silence_suppression(account,gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> suppression_check)),gtk_toggle_button_get_type())))));
  }
/* If this is a new account, add it to our list */
  if (new_acct != 0) 
    purple_accounts_add(account);
  else 
    purple_signal_emit(pidgin_account_get_handle(),"account-modified",account);
/* If this is a new account, then sign on! */
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> register_button)),gtk_toggle_button_get_type())))) != 0) {
    purple_account_register(account);
  }
  else if (new_acct != 0) {
    const PurpleSavedStatus *saved_status;
    saved_status = (purple_savedstatus_get_current());
    if (saved_status != ((const PurpleSavedStatus *)((void *)0))) {
      purple_savedstatus_activate_for_account(saved_status,account);
      purple_account_set_enabled(account,"gtk-gaim",(!0));
    }
  }
/* We no longer need the data from the dialog window */
  account_win_destroy_cb(0,0,dialog);
}
static const GtkTargetEntry dnd_targets[] = {{("text/plain"), (0), (0)}, {("text/uri-list"), (0), (1)}, {("STRING"), (0), (2)}};

void pidgin_account_dialog_show(PidginAccountDialogType type,PurpleAccount *account)
{
  AccountPrefsDialog *dialog;
  GtkWidget *win;
  GtkWidget *main_vbox;
  GtkWidget *vbox;
  GtkWidget *dbox;
  GtkWidget *notebook;
  GtkWidget *button;
  if (((accounts_window != ((AccountsWindow *)((void *)0))) && (account != ((PurpleAccount *)((void *)0)))) && ((dialog = (g_hash_table_lookup(account_pref_wins,account))) != ((AccountPrefsDialog *)((void *)0)))) {
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> window)),gtk_window_get_type()))));
    return ;
  }
  dialog = ((AccountPrefsDialog *)(g_malloc0_n(1,(sizeof(AccountPrefsDialog )))));
  if ((accounts_window != ((AccountsWindow *)((void *)0))) && (account != ((PurpleAccount *)((void *)0)))) {
    g_hash_table_insert(account_pref_wins,account,dialog);
  }
  dialog -> account = account;
  dialog -> type = type;
  dialog -> sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  if ((dialog -> account) == ((PurpleAccount *)((void *)0))) {
/* Select the first prpl in the list*/
    GList *prpl_list = purple_plugins_get_protocols();
    if (prpl_list != ((GList *)((void *)0))) 
      dialog -> protocol_id = g_strdup(( *( *((PurplePlugin *)(prpl_list -> data))).info).id);
  }
  else {
    dialog -> protocol_id = g_strdup(purple_account_get_protocol_id((dialog -> account)));
  }
  if ((dialog -> plugin = purple_find_prpl((dialog -> protocol_id))) != ((PurplePlugin *)((void *)0))) 
    dialog -> prpl_info = ((PurplePluginProtocolInfo *)( *( *(dialog -> plugin)).info).extra_info);
  dialog -> window = (win = pidgin_create_dialog(((type == PIDGIN_ADD_ACCOUNT_DIALOG)?((const char *)(dgettext("pidgin","Add Account"))) : ((const char *)(dgettext("pidgin","Modify Account")))),6,"account",0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"delete_event",((GCallback )account_win_destroy_cb),dialog,0,((GConnectFlags )0));
/* Setup the vbox */
  main_vbox = pidgin_dialog_get_vbox_with_properties(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),0,6);
  dialog -> notebook = (notebook = gtk_notebook_new());
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)main_vbox),gtk_box_get_type()))),notebook,0,0,0);
  gtk_widget_show(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)notebook),gtk_widget_get_type()))));
/* Setup the inner vbox */
  dialog -> top_vbox = (vbox = gtk_vbox_new(0,12));
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_container_get_type()))),12);
  gtk_notebook_append_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)notebook),gtk_notebook_get_type()))),vbox,gtk_label_new_with_mnemonic(((const char *)(dgettext("pidgin","_Basic")))));
  gtk_widget_show(vbox);
/* Setup the top frames. */
  add_login_options(dialog,vbox);
  add_user_options(dialog,vbox);
  button = gtk_check_button_new_with_mnemonic(((const char *)(dgettext("pidgin","Create _this new account on the server"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)main_vbox),gtk_box_get_type()))),button,0,0,0);
  gtk_widget_show(button);
  dialog -> register_button = button;
  if ((dialog -> account) == ((PurpleAccount *)((void *)0))) 
    gtk_widget_set_sensitive(button,0);
  if (!((dialog -> prpl_info) != 0) || !(( *(dialog -> prpl_info)).register_user != 0)) 
    gtk_widget_hide(button);
/* Setup the page with 'Advanced' (protocol options). */
  add_protocol_options(dialog);
/* Setup the page with 'Proxy'. */
  dbox = gtk_vbox_new(0,12);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)dbox),gtk_container_get_type()))),12);
  gtk_notebook_append_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)notebook),gtk_notebook_get_type()))),dbox,gtk_label_new_with_mnemonic(((const char *)(dgettext("pidgin","P_roxy")))));
  gtk_widget_show(dbox);
  add_proxy_options(dialog,dbox);
  add_voice_options(dialog);
/* Cancel button */
  pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),"gtk-cancel",((GCallback )cancel_account_prefs_cb),dialog);
/* Save button */
  button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),(((type == PIDGIN_ADD_ACCOUNT_DIALOG)?"gtk-add" : "gtk-save")),((GCallback )ok_account_prefs_cb),dialog);
  if ((dialog -> account) == ((PurpleAccount *)((void *)0))) 
    gtk_widget_set_sensitive(button,0);
  dialog -> ok_button = button;
/* Set up DND */
  gtk_drag_dest_set((dialog -> window),(GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_DROP),dnd_targets,(sizeof(dnd_targets) / sizeof(GtkTargetEntry )),GDK_ACTION_COPY);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> window)),((GType )(20 << 2))))),"drag_data_received",((GCallback )account_dnd_recv),dialog,0,((GConnectFlags )0));
/* Show the window. */
  gtk_widget_show(win);
  if (!(account != 0)) 
    gtk_widget_grab_focus((dialog -> protocol_menu));
}
/**************************************************************************
 * Accounts Dialog
 **************************************************************************/

static void signed_on_off_cb(PurpleConnection *gc,gpointer user_data)
{
  PurpleAccount *account;
  GtkTreeModel *model;
  GtkTreeIter iter;
  GdkPixbuf *pixbuf;
  size_t index;
/* Don't need to do anything if the accounts window is not visible */
  if (accounts_window == ((AccountsWindow *)((void *)0))) 
    return ;
  account = purple_connection_get_account(gc);
  model = ((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(accounts_window -> model)),gtk_tree_model_get_type())));
  index = (g_list_index(purple_accounts_get_all(),account));
  if (gtk_tree_model_iter_nth_child(model,&iter,0,index) != 0) {
    pixbuf = pidgin_create_prpl_icon(account,PIDGIN_PRPL_ICON_MEDIUM);
    if ((pixbuf != ((GdkPixbuf *)((void *)0))) && (purple_account_is_disconnected(account) != 0)) 
      gdk_pixbuf_saturate_and_pixelate(pixbuf,pixbuf,0.0,0);
    gtk_list_store_set((accounts_window -> model),&iter,COLUMN_ICON,pixbuf,-1);
    if (pixbuf != ((GdkPixbuf *)((void *)0))) 
      g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
  }
}
/*
 * Get the GtkTreeIter of the specified account in the
 * GtkListStore
 */

static gboolean accounts_window_find_account_in_treemodel(GtkTreeIter *iter,PurpleAccount *account)
{
  GtkTreeModel *model;
  PurpleAccount *cur;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  do {
    if (accounts_window != ((AccountsWindow *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"accounts_window != NULL");
      return 0;
    };
  }while (0);
  model = ((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(accounts_window -> model)),gtk_tree_model_get_type())));
  if (!(gtk_tree_model_get_iter_first(model,iter) != 0)) 
    return 0;
  gtk_tree_model_get(model,iter,COLUMN_DATA,&cur,-1);
  if (cur == account) 
    return (!0);
  while(gtk_tree_model_iter_next(model,iter) != 0){
    gtk_tree_model_get(model,iter,COLUMN_DATA,&cur,-1);
    if (cur == account) 
      return (!0);
  }
  return 0;
}

static void account_removed_cb(PurpleAccount *account,gpointer user_data)
{
  AccountPrefsDialog *dialog;
  GtkTreeIter iter;
/* If the account was being modified, close the edit window */
  if ((dialog = (g_hash_table_lookup(account_pref_wins,account))) != ((AccountPrefsDialog *)((void *)0))) 
    account_win_destroy_cb(0,0,dialog);
  if (accounts_window == ((AccountsWindow *)((void *)0))) 
    return ;
/* Remove the account from the GtkListStore */
  if (accounts_window_find_account_in_treemodel(&iter,account) != 0) 
    gtk_list_store_remove((accounts_window -> model),&iter);
  if (purple_accounts_get_all() == ((GList *)((void *)0))) 
    gtk_notebook_set_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(accounts_window -> notebook)),gtk_notebook_get_type()))),0);
}

static void account_abled_cb(PurpleAccount *account,gpointer user_data)
{
  GtkTreeIter iter;
  if (accounts_window == ((AccountsWindow *)((void *)0))) 
    return ;
/* update the account in the GtkListStore */
  if (accounts_window_find_account_in_treemodel(&iter,account) != 0) 
    gtk_list_store_set((accounts_window -> model),&iter,COLUMN_ENABLED,((gint )((glong )user_data)),-1);
}

static void drag_data_get_cb(GtkWidget *widget,GdkDragContext *ctx,GtkSelectionData *data,guint info,guint time,AccountsWindow *dialog)
{
  if ((data -> target) == gdk_atom_intern("PURPLE_ACCOUNT",0)) {
    GtkTreeRowReference *ref;
    GtkTreePath *source_row;
    GtkTreeIter iter;
    PurpleAccount *account = (PurpleAccount *)((void *)0);
    GValue val;
    ref = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ctx),((GType )(20 << 2))))),"gtk-tree-view-source-row"));
    source_row = gtk_tree_row_reference_get_path(ref);
    if (source_row == ((GtkTreePath *)((void *)0))) 
      return ;
    gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter,source_row);
    val.g_type = 0;
    gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter,COLUMN_DATA,&val);
    dialog -> drag_iter = iter;
    account = (g_value_get_pointer((&val)));
    gtk_selection_data_set(data,gdk_atom_intern("PURPLE_ACCOUNT",0),8,((void *)(&account)),(sizeof(account)));
    gtk_tree_path_free(source_row);
  }
}

static void move_account_after(GtkListStore *store,GtkTreeIter *iter,GtkTreeIter *position)
{
  GtkTreeIter new_iter;
  PurpleAccount *account;
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)store),gtk_tree_model_get_type()))),iter,COLUMN_DATA,&account,-1);
  gtk_list_store_insert_after(store,&new_iter,position);
  set_account(store,&new_iter,account,0);
  gtk_list_store_remove(store,iter);
}

static void move_account_before(GtkListStore *store,GtkTreeIter *iter,GtkTreeIter *position)
{
  GtkTreeIter new_iter;
  PurpleAccount *account;
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)store),gtk_tree_model_get_type()))),iter,COLUMN_DATA,&account,-1);
  gtk_list_store_insert_before(store,&new_iter,position);
  set_account(store,&new_iter,account,0);
  gtk_list_store_remove(store,iter);
}

static void drag_data_received_cb(GtkWidget *widget,GdkDragContext *ctx,guint x,guint y,GtkSelectionData *sd,guint info,guint t,AccountsWindow *dialog)
{
  if (((sd -> target) == gdk_atom_intern("PURPLE_ACCOUNT",0)) && ((sd -> data) != 0)) {
    gint dest_index;
    PurpleAccount *a = (PurpleAccount *)((void *)0);
    GtkTreePath *path = (GtkTreePath *)((void *)0);
    GtkTreeViewDropPosition position;
    memcpy((&a),(sd -> data),(sizeof(a)));
    if (gtk_tree_view_get_dest_row_at_pos(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_tree_view_get_type()))),x,y,&path,&position) != 0) {
      GtkTreeIter iter;
      PurpleAccount *account;
      GValue val;
      gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter,path);
      val.g_type = 0;
      gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter,COLUMN_DATA,&val);
      account = (g_value_get_pointer((&val)));
      switch(position){
        case GTK_TREE_VIEW_DROP_AFTER:
{
        }
        case GTK_TREE_VIEW_DROP_INTO_OR_AFTER:
{
          move_account_after((dialog -> model),&dialog -> drag_iter,&iter);
          dest_index = (g_list_index(purple_accounts_get_all(),account) + 1);
          break; 
        }
        case GTK_TREE_VIEW_DROP_BEFORE:
{
        }
        case GTK_TREE_VIEW_DROP_INTO_OR_BEFORE:
{
          dest_index = g_list_index(purple_accounts_get_all(),account);
          move_account_before((dialog -> model),&dialog -> drag_iter,&iter);
          break; 
        }
        default:
{
          return ;
        }
      }
      purple_accounts_reorder(a,dest_index);
    }
  }
}

static gboolean accedit_win_destroy_cb(GtkWidget *w,GdkEvent *event,AccountsWindow *dialog)
{
  dialog -> window = ((GtkWidget *)((void *)0));
  pidgin_accounts_window_hide();
  return 0;
}

static void add_account_cb(GtkWidget *w,AccountsWindow *dialog)
{
  pidgin_account_dialog_show(PIDGIN_ADD_ACCOUNT_DIALOG,0);
}

static void modify_account_sel(GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter,gpointer data)
{
  PurpleAccount *account;
  gtk_tree_model_get(model,iter,COLUMN_DATA,&account,-1);
  if (account != ((PurpleAccount *)((void *)0))) 
    pidgin_account_dialog_show(PIDGIN_MODIFY_ACCOUNT_DIALOG,account);
}

static void modify_account_cb(GtkWidget *w,AccountsWindow *dialog)
{
  GtkTreeSelection *selection;
  selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treeview)),gtk_tree_view_get_type()))));
  gtk_tree_selection_selected_foreach(selection,modify_account_sel,dialog);
}

static void delete_account_cb(PurpleAccount *account)
{
  purple_accounts_delete(account);
}

static void ask_delete_account_sel(GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter,gpointer data)
{
  PurpleAccount *account;
  gtk_tree_model_get(model,iter,COLUMN_DATA,&account,-1);
  if (account != ((PurpleAccount *)((void *)0))) {
    char *buf;
    buf = g_strdup_printf(((const char *)(dgettext("pidgin","Are you sure you want to delete %s\?"))),purple_account_get_username(account));
    purple_request_close_with_handle(account);
    purple_request_action(account,0,buf,0,-1,account,0,0,account,2,((const char *)(dgettext("pidgin","Delete"))),delete_account_cb,((const char *)(dgettext("pidgin","Cancel"))),((void *)((void *)0)));
    g_free(buf);
  }
}

static void ask_delete_account_cb(GtkWidget *w,AccountsWindow *dialog)
{
  GtkTreeSelection *selection;
  selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treeview)),gtk_tree_view_get_type()))));
  gtk_tree_selection_selected_foreach(selection,ask_delete_account_sel,dialog);
}

static void close_accounts_cb(GtkWidget *w,AccountsWindow *dialog)
{
  pidgin_accounts_window_hide();
}

static void enabled_cb(GtkCellRendererToggle *renderer,gchar *path_str,gpointer data)
{
  AccountsWindow *dialog = (AccountsWindow *)data;
  PurpleAccount *account;
  GtkTreeModel *model = (GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()));
  GtkTreeIter iter;
  gboolean enabled;
  const PurpleSavedStatus *saved_status;
  gtk_tree_model_get_iter_from_string(model,&iter,path_str);
  gtk_tree_model_get(model,&iter,COLUMN_DATA,&account,COLUMN_ENABLED,&enabled,-1);
/*
	 * If we just enabled the account, then set the statuses
	 * to the current status.
	 */
  if (!(enabled != 0)) {
    saved_status = (purple_savedstatus_get_current());
    purple_savedstatus_activate_for_account(saved_status,account);
  }
  purple_account_set_enabled(account,"gtk-gaim",!(enabled != 0));
}

static void add_columns(GtkWidget *treeview,AccountsWindow *dialog)
{
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;
/* Enabled */
  renderer = gtk_cell_renderer_toggle_new();
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)renderer),((GType )(20 << 2))))),"toggled",((GCallback )enabled_cb),dialog,0,((GConnectFlags )0));
  column = gtk_tree_view_column_new_with_attributes(((const char *)(dgettext("pidgin","Enabled"))),renderer,"active",COLUMN_ENABLED,((void *)((void *)0)));
  gtk_tree_view_column_set_resizable(column,0);
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),column);
/* Username column */
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_title(column,((const char *)(dgettext("pidgin","Username"))));
  gtk_tree_view_column_set_resizable(column,(!0));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),column);
/* Buddy Icon */
  renderer = gtk_cell_renderer_pixbuf_new();
  gtk_tree_view_column_pack_start(column,renderer,0);
  gtk_tree_view_column_add_attribute(column,renderer,"pixbuf",COLUMN_BUDDYICON);
/* Username */
  renderer = gtk_cell_renderer_text_new();
  gtk_tree_view_column_pack_start(column,renderer,(!0));
  gtk_tree_view_column_add_attribute(column,renderer,"text",COLUMN_USERNAME);
  dialog -> username_col = column;
/* Protocol name */
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_title(column,((const char *)(dgettext("pidgin","Protocol"))));
  gtk_tree_view_column_set_resizable(column,0);
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),column);
/* Icon */
  renderer = gtk_cell_renderer_pixbuf_new();
  gtk_tree_view_column_pack_start(column,renderer,0);
  gtk_tree_view_column_add_attribute(column,renderer,"pixbuf",COLUMN_ICON);
  renderer = gtk_cell_renderer_text_new();
  gtk_tree_view_column_pack_start(column,renderer,(!0));
  gtk_tree_view_column_add_attribute(column,renderer,"text",COLUMN_PROTOCOL);
}

static void set_account(GtkListStore *store,GtkTreeIter *iter,PurpleAccount *account,GdkPixbuf *global_buddyicon)
{
  GdkPixbuf *pixbuf;
  GdkPixbuf *buddyicon = (GdkPixbuf *)((void *)0);
  PurpleStoredImage *img = (PurpleStoredImage *)((void *)0);
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  pixbuf = pidgin_create_prpl_icon(account,PIDGIN_PRPL_ICON_MEDIUM);
  if ((pixbuf != ((GdkPixbuf *)((void *)0))) && (purple_account_is_disconnected(account) != 0)) 
    gdk_pixbuf_saturate_and_pixelate(pixbuf,pixbuf,0.0,0);
  prpl = purple_find_prpl(purple_account_get_protocol_id(account));
  if (prpl != ((PurplePlugin *)((void *)0))) 
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if ((prpl_info != ((PurplePluginProtocolInfo *)((void *)0))) && (prpl_info -> icon_spec.format != ((char *)((void *)0)))) {
    if (purple_account_get_bool(account,"use-global-buddyicon",(!0)) != 0) {
      if (global_buddyicon != ((GdkPixbuf *)((void *)0))) 
        buddyicon = (g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)global_buddyicon),((GType )(20 << 2)))))));
      else {
/* This is for when set_account() is called for a single account */
        const char *path;
        path = purple_prefs_get_path("/pidgin/accounts/buddyicon");
        if ((path != ((const char *)((void *)0))) && (( *path) != 0)) {
          img = purple_imgstore_new_from_file(path);
        }
      }
    }
    else {
      img = purple_buddy_icons_find_account_icon(account);
    }
  }
  if (img != ((PurpleStoredImage *)((void *)0))) {
    GdkPixbuf *buddyicon_pixbuf;
    buddyicon_pixbuf = pidgin_pixbuf_from_imgstore(img);
    purple_imgstore_unref(img);
    if (buddyicon_pixbuf != ((GdkPixbuf *)((void *)0))) {
      buddyicon = gdk_pixbuf_scale_simple(buddyicon_pixbuf,22,22,GDK_INTERP_HYPER);
      g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buddyicon_pixbuf),((GType )(20 << 2))))));
    }
  }
  gtk_list_store_set(store,iter,COLUMN_ICON,pixbuf,COLUMN_BUDDYICON,buddyicon,COLUMN_USERNAME,purple_account_get_username(account),COLUMN_ENABLED,purple_account_get_enabled(account,"gtk-gaim"),COLUMN_PROTOCOL,purple_account_get_protocol_name(account),COLUMN_DATA,account,-1);
  if (pixbuf != ((GdkPixbuf *)((void *)0))) 
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
  if (buddyicon != ((GdkPixbuf *)((void *)0))) 
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buddyicon),((GType )(20 << 2))))));
}

static void add_account_to_liststore(PurpleAccount *account,gpointer user_data)
{
  GtkTreeIter iter;
  GdkPixbuf *global_buddyicon = user_data;
  if (accounts_window == ((AccountsWindow *)((void *)0))) 
    return ;
  gtk_list_store_append((accounts_window -> model),&iter);
  gtk_notebook_set_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(accounts_window -> notebook)),gtk_notebook_get_type()))),1);
  set_account((accounts_window -> model),&iter,account,global_buddyicon);
}

static gboolean populate_accounts_list(AccountsWindow *dialog)
{
  GList *l;
  gboolean ret = 0;
  GdkPixbuf *global_buddyicon = (GdkPixbuf *)((void *)0);
  const char *path;
  gtk_list_store_clear((dialog -> model));
  if ((path = purple_prefs_get_path("/pidgin/accounts/buddyicon")) != ((const char *)((void *)0))) {
    GdkPixbuf *pixbuf = pidgin_pixbuf_new_from_file(path);
    if (pixbuf != ((GdkPixbuf *)((void *)0))) {
      global_buddyicon = gdk_pixbuf_scale_simple(pixbuf,22,22,GDK_INTERP_HYPER);
      g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
    }
  }
  for (l = purple_accounts_get_all(); l != ((GList *)((void *)0)); l = (l -> next)) {
    ret = (!0);
    add_account_to_liststore(((PurpleAccount *)(l -> data)),global_buddyicon);
  }
  if (global_buddyicon != ((GdkPixbuf *)((void *)0))) 
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)global_buddyicon),((GType )(20 << 2))))));
  return ret;
}

static void account_selected_cb(GtkTreeSelection *sel,AccountsWindow *dialog)
{
  gboolean selected = 0;
  selected = (gtk_tree_selection_count_selected_rows(sel) > 0);
  gtk_widget_set_sensitive((dialog -> modify_button),selected);
  gtk_widget_set_sensitive((dialog -> delete_button),selected);
}

static gboolean account_treeview_double_click_cb(GtkTreeView *treeview,GdkEventButton *event,gpointer user_data)
{
  AccountsWindow *dialog;
  GtkTreePath *path;
  GtkTreeViewColumn *column;
  GtkTreeIter iter;
  PurpleAccount *account;
  dialog = ((AccountsWindow *)user_data);
  if ((event -> window) != gtk_tree_view_get_bin_window(treeview)) 
    return 0;
/* Figure out which node was clicked */
  if (!(gtk_tree_view_get_path_at_pos(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treeview)),gtk_tree_view_get_type()))),(event -> x),(event -> y),&path,&column,0,0) != 0)) 
    return 0;
  if (column == gtk_tree_view_get_column(treeview,0)) {
    gtk_tree_path_free(path);
    return 0;
  }
  gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter,path);
  gtk_tree_path_free(path);
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter,COLUMN_DATA,&account,-1);
  if (((account != ((PurpleAccount *)((void *)0))) && ((event -> button) == 1)) && ((event -> type) == GDK_2BUTTON_PRESS)) {
    pidgin_account_dialog_show(PIDGIN_MODIFY_ACCOUNT_DIALOG,account);
    return (!0);
  }
  return 0;
}

static GtkWidget *create_accounts_list(AccountsWindow *dialog)
{
  GtkWidget *frame;
  GtkWidget *label;
  GtkWidget *treeview;
  GtkTreeSelection *sel;
  GtkTargetEntry gte[] = {{("PURPLE_ACCOUNT"), (GTK_TARGET_SAME_APP), (0)}};
  char *pretty;
  char *tmp;
  frame = gtk_frame_new(0);
  gtk_frame_set_shadow_type(((GtkFrame *)(g_type_check_instance_cast(((GTypeInstance *)frame),gtk_frame_get_type()))),GTK_SHADOW_IN);
  accounts_window -> notebook = gtk_notebook_new();
  gtk_notebook_set_show_tabs(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(accounts_window -> notebook)),gtk_notebook_get_type()))),0);
  gtk_notebook_set_show_border(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(accounts_window -> notebook)),gtk_notebook_get_type()))),0);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)frame),gtk_container_get_type()))),(accounts_window -> notebook));
/* Create a helpful first-time-use label */
  label = gtk_label_new(0);
/* Translators: Please maintain the use of -> or <- to represent the menu heirarchy */
  tmp = g_strdup_printf(((const char *)(dgettext("pidgin","<span size=\'larger\' weight=\'bold\'>Welcome to %s!</span>\n\nYou have no IM accounts configured. To start connecting with %s press the <b>Add...</b> button below and configure your first account. If you want %s to connect to multiple IM accounts, press <b>Add...</b> again to configure them all.\n\nYou can come back to this window to add, edit, or remove accounts from <b>Accounts->Manage Accounts</b> in the Buddy List window"))),((const char *)(dgettext("pidgin","Pidgin"))),((const char *)(dgettext("pidgin","Pidgin"))),((const char *)(dgettext("pidgin","Pidgin"))));
  pretty = pidgin_make_pretty_arrows(tmp);
  g_free(tmp);
  gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),pretty);
  g_free(pretty);
  gtk_label_set_line_wrap(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(!0));
  gtk_widget_show(label);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0.5,0.5);
  gtk_notebook_append_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(accounts_window -> notebook)),gtk_notebook_get_type()))),label,0);
/* Create the list model. */
  dialog -> model = gtk_list_store_new(NUM_COLUMNS,gdk_pixbuf_get_type(),gdk_pixbuf_get_type(),((GType )(16 << 2)),((GType )(5 << 2)),((GType )(16 << 2)),((GType )(17 << 2)));
/* COLUMN_ICON */
/* COLUMN_BUDDYICON */
/* COLUMN_USERNAME */
/* COLUMN_ENABLED */
/* COLUMN_PROTOCOL */
/* COLUMN_DATA */
/* And now the actual treeview */
  treeview = gtk_tree_view_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))));
  dialog -> treeview = treeview;
  gtk_tree_view_set_rules_hint(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),(!0));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),((GType )(20 << 2))))));
  sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))));
  gtk_tree_selection_set_mode(sel,GTK_SELECTION_MULTIPLE);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)sel),((GType )(20 << 2))))),"changed",((GCallback )account_selected_cb),dialog,0,((GConnectFlags )0));
/* Handle double-clicking */
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)treeview),((GType )(20 << 2))))),"button_press_event",((GCallback )account_treeview_double_click_cb),dialog,0,((GConnectFlags )0));
  gtk_notebook_append_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(accounts_window -> notebook)),gtk_notebook_get_type()))),pidgin_make_scrollable(treeview,GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC,GTK_SHADOW_NONE,-1,-1),0);
  add_columns(treeview,dialog);
  gtk_tree_view_columns_autosize(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))));
  if (populate_accounts_list(dialog) != 0) 
    gtk_notebook_set_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(accounts_window -> notebook)),gtk_notebook_get_type()))),1);
  else 
    gtk_notebook_set_current_page(((GtkNotebook *)(g_type_check_instance_cast(((GTypeInstance *)(accounts_window -> notebook)),gtk_notebook_get_type()))),0);
/* Setup DND. I wanna be an orc! */
  gtk_tree_view_enable_model_drag_source(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),GDK_BUTTON1_MASK,gte,1,GDK_ACTION_COPY);
  gtk_tree_view_enable_model_drag_dest(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),gte,1,(GDK_ACTION_COPY | GDK_ACTION_MOVE));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)treeview),((GType )(20 << 2))))),"drag-data-received",((GCallback )drag_data_received_cb),dialog,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)treeview),((GType )(20 << 2))))),"drag-data-get",((GCallback )drag_data_get_cb),dialog,0,((GConnectFlags )0));
  gtk_widget_show_all(frame);
  return frame;
}

static void account_modified_cb(PurpleAccount *account,AccountsWindow *window)
{
  GtkTreeIter iter;
  if (!(accounts_window_find_account_in_treemodel(&iter,account) != 0)) 
    return ;
  set_account((window -> model),&iter,account,0);
}

static void global_buddyicon_changed(const char *name,PurplePrefType type,gconstpointer value,gpointer window)
{
  GList *list;
  for (list = purple_accounts_get_all(); list != 0; list = (list -> next)) {
    account_modified_cb((list -> data),window);
  }
}

void pidgin_accounts_window_show()
{
  AccountsWindow *dialog;
  GtkWidget *win;
  GtkWidget *vbox;
  GtkWidget *sw;
  GtkWidget *button;
  int width;
  int height;
  if (accounts_window != ((AccountsWindow *)((void *)0))) {
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(accounts_window -> window)),gtk_window_get_type()))));
    return ;
  }
  accounts_window = (dialog = ((AccountsWindow *)(g_malloc0_n(1,(sizeof(AccountsWindow ))))));
  width = purple_prefs_get_int("/pidgin/accounts/dialog/width");
  height = purple_prefs_get_int("/pidgin/accounts/dialog/height");
  dialog -> window = (win = pidgin_create_dialog(((const char *)(dgettext("pidgin","Accounts"))),12,"accounts",(!0)));
  gtk_window_set_default_size(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_window_get_type()))),width,height);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"delete_event",((GCallback )accedit_win_destroy_cb),accounts_window,0,((GConnectFlags )0));
/* Setup the vbox */
  vbox = pidgin_dialog_get_vbox_with_properties(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),0,12);
/* Setup the scrolled window that will contain the list of accounts. */
  sw = create_accounts_list(dialog);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),sw,(!0),(!0),0);
  gtk_widget_show(sw);
/* Add button */
  pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),"pidgin-add",((GCallback )add_account_cb),dialog);
/* Modify button */
  button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),"pidgin-modify",((GCallback )modify_account_cb),dialog);
  dialog -> modify_button = button;
  gtk_widget_set_sensitive(button,0);
/* Delete button */
  button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),"gtk-delete",((GCallback )ask_delete_account_cb),dialog);
  dialog -> delete_button = button;
  gtk_widget_set_sensitive(button,0);
/* Close button */
  pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),"gtk-close",((GCallback )close_accounts_cb),dialog);
  purple_signal_connect(pidgin_account_get_handle(),"account-modified",accounts_window,((PurpleCallback )account_modified_cb),accounts_window);
  purple_prefs_connect_callback(accounts_window,"/pidgin/accounts/buddyicon",global_buddyicon_changed,accounts_window);
  gtk_widget_show(win);
}

void pidgin_accounts_window_hide()
{
  if (accounts_window == ((AccountsWindow *)((void *)0))) 
    return ;
  if ((accounts_window -> window) != ((GtkWidget *)((void *)0))) 
    gtk_widget_destroy((accounts_window -> window));
  purple_signals_disconnect_by_handle(accounts_window);
  purple_prefs_disconnect_by_handle(accounts_window);
  g_free(accounts_window);
  accounts_window = ((AccountsWindow *)((void *)0));
}

static void free_add_user_data(PidginAccountAddUserData *data)
{
  g_free((data -> username));
  g_free((data -> alias));
  g_free(data);
}

static void add_user_cb(PidginAccountAddUserData *data)
{
  PurpleConnection *gc = purple_account_get_connection((data -> account));
  if (g_list_find(purple_connections_get_all(),gc) != 0) {
    purple_blist_request_add_buddy((data -> account),(data -> username),0,(data -> alias));
  }
  free_add_user_data(data);
}

static char *make_info(PurpleAccount *account,PurpleConnection *gc,const char *remote_user,const char *id,const char *alias,const char *msg)
{
  if ((msg != ((const char *)((void *)0))) && (( *msg) == 0)) 
    msg = ((const char *)((void *)0));
  return g_strdup_printf(((const char *)(dgettext("pidgin","%s%s%s%s has made %s his or her buddy%s%s"))),remote_user,((alias != ((const char *)((void *)0)))?" (" : ""),((alias != ((const char *)((void *)0)))?alias : ""),((alias != ((const char *)((void *)0)))?")" : ""),((id != ((const char *)((void *)0)))?id : (((purple_connection_get_display_name(gc) != ((const char *)((void *)0)))?purple_connection_get_display_name(gc) : purple_account_get_username(account)))),((msg != ((const char *)((void *)0)))?": " : "."),((msg != ((const char *)((void *)0)))?msg : ""));
}

static void pidgin_accounts_notify_added(PurpleAccount *account,const char *remote_user,const char *id,const char *alias,const char *msg)
{
  char *buffer;
  PurpleConnection *gc;
  GtkWidget *alert;
  gc = purple_account_get_connection(account);
  buffer = make_info(account,gc,remote_user,id,alias,msg);
  alert = pidgin_make_mini_dialog(gc,"pidgin-dialog-info",buffer,0,0,((const char *)(dgettext("pidgin","Close"))),((void *)((void *)0)),((void *)((void *)0)));
  pidgin_blist_add_alert(alert);
  g_free(buffer);
}

static void pidgin_accounts_request_add(PurpleAccount *account,const char *remote_user,const char *id,const char *alias,const char *msg)
{
  char *buffer;
  PurpleConnection *gc;
  PidginAccountAddUserData *data;
  GtkWidget *alert;
  gc = purple_account_get_connection(account);
  data = ((PidginAccountAddUserData *)(g_malloc0_n(1,(sizeof(PidginAccountAddUserData )))));
  data -> account = account;
  data -> username = g_strdup(remote_user);
  data -> alias = g_strdup(alias);
  buffer = make_info(account,gc,remote_user,id,alias,msg);
  alert = pidgin_make_mini_dialog(gc,"pidgin-dialog-question",((const char *)(dgettext("pidgin","Add buddy to your list\?"))),buffer,data,((const char *)(dgettext("pidgin","Add"))),((GCallback )add_user_cb),((const char *)(dgettext("pidgin","Cancel"))),((GCallback )free_add_user_data),((void *)((void *)0)));
  pidgin_blist_add_alert(alert);
  g_free(buffer);
}

struct auth_request 
{
  PurpleAccountRequestAuthorizationCb auth_cb;
  PurpleAccountRequestAuthorizationCb deny_cb;
  void *data;
  char *username;
  char *alias;
  PurpleAccount *account;
  gboolean add_buddy_after_auth;
}
;

static void free_auth_request(struct auth_request *ar)
{
  g_free((ar -> username));
  g_free((ar -> alias));
  g_free(ar);
}

static void authorize_and_add_cb(struct auth_request *ar)
{
  ( *(ar -> auth_cb))((ar -> data));
  if ((ar -> add_buddy_after_auth) != 0) {
    purple_blist_request_add_buddy((ar -> account),(ar -> username),0,(ar -> alias));
  }
}

static void deny_no_add_cb(struct auth_request *ar)
{
  ( *(ar -> deny_cb))((ar -> data));
}

static gboolean get_user_info_cb(GtkWidget *label,const gchar *uri,gpointer data)
{
  struct auth_request *ar = data;
  if (!(strcmp(uri,"viewinfo") != 0)) {
    pidgin_retrieve_user_info(purple_account_get_connection((ar -> account)),(ar -> username));
    return (!0);
  }
  return 0;
}

static void send_im_cb(PidginMiniDialog *mini_dialog,GtkButton *button,gpointer data)
{
  struct auth_request *ar = data;
  pidgin_dialogs_im_with_user((ar -> account),(ar -> username));
}

static void *pidgin_accounts_request_authorization(PurpleAccount *account,const char *remote_user,const char *id,const char *alias,const char *message,gboolean on_list,PurpleAccountRequestAuthorizationCb auth_cb,PurpleAccountRequestAuthorizationCb deny_cb,void *user_data)
{
  char *buffer;
  PurpleConnection *gc;
  GtkWidget *alert;
  PidginMiniDialog *dialog;
  GdkPixbuf *prpl_icon;
  struct auth_request *aa;
  const char *our_name;
  gboolean have_valid_alias = ((alias != 0) && (( *alias) != 0));
  gc = purple_account_get_connection(account);
  if ((message != ((const char *)((void *)0))) && (( *message) == 0)) 
    message = ((const char *)((void *)0));
  our_name = ((id != ((const char *)((void *)0)))?id : (((purple_connection_get_display_name(gc) != ((const char *)((void *)0)))?purple_connection_get_display_name(gc) : purple_account_get_username(account))));
  if (pidgin_mini_dialog_links_supported() != 0) {
    char *escaped_remote_user = g_markup_escape_text(remote_user,(-1));
    char *escaped_alias = (alias != ((const char *)((void *)0)))?g_markup_escape_text(alias,(-1)) : g_strdup("");
    char *escaped_our_name = g_markup_escape_text(our_name,(-1));
    char *escaped_message = (message != ((const char *)((void *)0)))?g_markup_escape_text(message,(-1)) : g_strdup("");
    buffer = g_strdup_printf(((const char *)(dgettext("pidgin","<a href=\"viewinfo\">%s</a>%s%s%s wants to add you (%s) to his or her buddy list%s%s"))),escaped_remote_user,((have_valid_alias != 0)?" (" : ""),escaped_alias,((have_valid_alias != 0)?")" : ""),escaped_our_name,((have_valid_alias != 0)?": " : "."),escaped_message);
    g_free(escaped_remote_user);
    g_free(escaped_alias);
    g_free(escaped_our_name);
    g_free(escaped_message);
  }
  else {
    buffer = g_strdup_printf(((const char *)(dgettext("pidgin","%s%s%s%s wants to add you (%s) to his or her buddy list%s%s"))),remote_user,((have_valid_alias != 0)?" (" : ""),((have_valid_alias != 0)?alias : ""),((have_valid_alias != 0)?")" : ""),our_name,((message != ((const char *)((void *)0)))?": " : "."),((message != ((const char *)((void *)0)))?message : ""));
  }
  prpl_icon = pidgin_create_prpl_icon(account,PIDGIN_PRPL_ICON_SMALL);
  aa = ((struct auth_request *)(g_malloc0_n(1,(sizeof(struct auth_request )))));
  aa -> auth_cb = auth_cb;
  aa -> deny_cb = deny_cb;
  aa -> data = user_data;
  aa -> username = g_strdup(remote_user);
  aa -> alias = g_strdup(alias);
  aa -> account = account;
  aa -> add_buddy_after_auth = !(on_list != 0);
  alert = pidgin_make_mini_dialog_with_custom_icon(gc,prpl_icon,((const char *)(dgettext("pidgin","Authorize buddy\?"))),0,aa,((const char *)(dgettext("pidgin","Authorize"))),authorize_and_add_cb,((const char *)(dgettext("pidgin","Deny"))),deny_no_add_cb,((void *)((void *)0)));
  dialog = ((PidginMiniDialog *)(g_type_check_instance_cast(((GTypeInstance *)alert),pidgin_mini_dialog_get_type())));
  if (pidgin_mini_dialog_links_supported() != 0) {
    pidgin_mini_dialog_enable_description_markup(dialog);
    pidgin_mini_dialog_set_link_callback(dialog,((GCallback )get_user_info_cb),aa);
  }
  pidgin_mini_dialog_set_description(dialog,buffer);
  pidgin_mini_dialog_add_non_closing_button(dialog,((const char *)(dgettext("pidgin","Send Instant Message"))),send_im_cb,aa);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)alert),((GType )(20 << 2))))),"destroy",((GCallback )free_auth_request),aa,0,G_CONNECT_SWAPPED);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)alert),((GType )(20 << 2))))),"destroy",((GCallback )purple_account_request_close),0,0,((GConnectFlags )0));
  pidgin_blist_add_alert(alert);
  g_free(buffer);
  return alert;
}

static void pidgin_accounts_request_close(void *ui_handle)
{
  gtk_widget_destroy(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)ui_handle),gtk_widget_get_type()))));
}
static PurpleAccountUiOps ui_ops = {(pidgin_accounts_notify_added), ((void (*)(PurpleAccount *, PurpleStatus *))((void *)0)), (pidgin_accounts_request_add), (pidgin_accounts_request_authorization), (pidgin_accounts_request_close), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))};

PurpleAccountUiOps *pidgin_accounts_get_ui_ops()
{
  return &ui_ops;
}

void *pidgin_account_get_handle()
{
  static int handle;
  return (&handle);
}

void pidgin_account_init()
{
  char *default_avatar = (char *)((void *)0);
  purple_prefs_add_none("/pidgin/accounts");
  purple_prefs_add_none("/pidgin/accounts/dialog");
  purple_prefs_add_int("/pidgin/accounts/dialog/width",520);
  purple_prefs_add_int("/pidgin/accounts/dialog/height",321);
  default_avatar = g_build_filename(g_get_home_dir(),".face.icon",((void *)((void *)0)));
  if (!(g_file_test(default_avatar,G_FILE_TEST_EXISTS) != 0)) {
    g_free(default_avatar);
    default_avatar = g_build_filename(g_get_home_dir(),".face",((void *)((void *)0)));
    if (!(g_file_test(default_avatar,G_FILE_TEST_EXISTS) != 0)) {
      g_free(default_avatar);
      default_avatar = ((char *)((void *)0));
    }
  }
  purple_prefs_add_path("/pidgin/accounts/buddyicon",default_avatar);
  g_free(default_avatar);
  purple_signal_register(pidgin_account_get_handle(),"account-modified",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT));
/* Setup some purple signal handlers. */
  purple_signal_connect(purple_connections_get_handle(),"signed-on",pidgin_account_get_handle(),((PurpleCallback )signed_on_off_cb),0);
  purple_signal_connect(purple_connections_get_handle(),"signed-off",pidgin_account_get_handle(),((PurpleCallback )signed_on_off_cb),0);
  purple_signal_connect(purple_accounts_get_handle(),"account-added",pidgin_account_get_handle(),((PurpleCallback )add_account_to_liststore),0);
  purple_signal_connect(purple_accounts_get_handle(),"account-removed",pidgin_account_get_handle(),((PurpleCallback )account_removed_cb),0);
  purple_signal_connect(purple_accounts_get_handle(),"account-disabled",pidgin_account_get_handle(),((PurpleCallback )account_abled_cb),0);
  purple_signal_connect(purple_accounts_get_handle(),"account-enabled",pidgin_account_get_handle(),((PurpleCallback )account_abled_cb),((void *)((gpointer )((glong )(!0)))));
  account_pref_wins = g_hash_table_new_full(g_direct_hash,g_direct_equal,0,0);
}

void pidgin_account_uninit()
{
/*
	 * TODO: Need to free all the dialogs in here.  Could probably create
	 *       a callback function to use for the free-some-data-function
	 *       parameter of g_hash_table_new_full, above.
	 */
  g_hash_table_destroy(account_pref_wins);
  purple_signals_disconnect_by_handle(pidgin_account_get_handle());
  purple_signals_unregister_by_instance(pidgin_account_get_handle());
}
