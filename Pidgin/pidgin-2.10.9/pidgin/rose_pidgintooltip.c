/**
 * @file pidgintooltip.c Pidgin Tooltip API
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "prefs.h"
#include "pidgin.h"
#include "pidgintooltip.h"
#include "debug.h"
static gboolean enable_tooltips;
static int tooltip_delay = -1;
struct __unnamed_class___F0_L36_C1_unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__widget__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__timeout__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L811R_variable_name_unknown_scope_and_name__scope__tip_rect__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__tipwindow__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L1565R_variable_name_unknown_scope_and_name__scope__paint_tooltip {
GtkWidget *widget;
int timeout;
GdkRectangle tip_rect;
GtkWidget *tipwindow;
PidginTooltipPaint paint_tooltip;}pidgin_tooltip;
typedef struct __unnamed_class___F0_L45_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__widget__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L55R_variable_name_unknown_scope_and_name__scope__userdata__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L1565R_variable_name_unknown_scope_and_name__scope__paint_tooltip__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__variable_name_unknown_scope_and_name__scope__common {
GtkWidget *widget;
gpointer userdata;
PidginTooltipPaint paint_tooltip;
union __unnamed_class___F0_L50_C2_L1602R_variable_declaration__variable_type__variable_name_L1602R__scope__treeview__DELIMITER__L1602R_variable_declaration__variable_type__variable_name_L1602R__scope__widget {
struct __unnamed_class___F0_L51_C3_L1603R_variable_declaration__variable_type_L1563R_variable_name_L1603R__scope__create_tooltip__DELIMITER__L1603R_variable_declaration__variable_type___Pb__L1087R__Pe___variable_name_L1603R__scope__path {
PidginTooltipCreateForTree create_tooltip;
GtkTreePath *path;}treeview;
struct __unnamed_class___F0_L55_C3_L1603R_variable_declaration__variable_type_L1564R_variable_name_L1603R__scope__create_tooltip {
PidginTooltipCreate create_tooltip;}widget;}common;}PidginTooltipData;

static void initialize_tooltip_delay()
{
#if GTK_CHECK_VERSION(2,14,0)
  GtkSettings *settings;
#endif
  if (tooltip_delay != -1) 
    return ;
#if GTK_CHECK_VERSION(2,14,0)
  settings = gtk_settings_get_default();
  g_object_get(settings,"gtk-enable-tooltips",&enable_tooltips,((void *)((void *)0)));
  g_object_get(settings,"gtk-tooltip-timeout",&tooltip_delay,((void *)((void *)0)));
#else
#endif
}

static void destroy_tooltip_data(PidginTooltipData *data)
{
  if (data -> common.treeview.path != 0) 
    gtk_tree_path_free(data -> common.treeview.path);
  pidgin_tooltip_destroy();
  g_free(data);
}

void pidgin_tooltip_destroy()
{
  if (pidgin_tooltip.timeout > 0) {
    g_source_remove(pidgin_tooltip.timeout);
    pidgin_tooltip.timeout = 0;
  }
  if (pidgin_tooltip.tipwindow != 0) {
    gtk_widget_destroy(pidgin_tooltip.tipwindow);
    pidgin_tooltip.tipwindow = ((GtkWidget *)((void *)0));
  }
}

static gboolean pidgin_tooltip_expose_event(GtkWidget *widget,GdkEventExpose *event,gpointer data)
{
  if (pidgin_tooltip.paint_tooltip != 0) {
    gtk_paint_flat_box((widget -> style),(widget -> window),GTK_STATE_NORMAL,GTK_SHADOW_OUT,0,widget,"tooltip",0,0,(-1),(-1));
    ( *pidgin_tooltip.paint_tooltip)(widget,data);
  }
  return 0;
}

static GtkWidget *setup_tooltip_window()
{
  const char *name;
  GtkWidget *tipwindow;
  tipwindow = gtk_window_new(GTK_WINDOW_POPUP);
  name = gtk_window_get_title(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)pidgin_tooltip.widget),gtk_window_get_type()))));
  gtk_window_set_type_hint(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)tipwindow),gtk_window_get_type()))),GDK_WINDOW_TYPE_HINT_TOOLTIP);
  gtk_widget_set_app_paintable(tipwindow,(!0));
  gtk_window_set_title(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)tipwindow),gtk_window_get_type()))),((name != 0)?name : ((const char *)(dgettext("pidgin","Pidgin Tooltip")))));
  gtk_window_set_resizable(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)tipwindow),gtk_window_get_type()))),0);
  gtk_widget_set_name(tipwindow,"gtk-tooltips");
  gtk_widget_ensure_style(tipwindow);
  gtk_widget_realize(tipwindow);
  return tipwindow;
}

static void setup_tooltip_window_position(gpointer data,int w,int h)
{
  int sig;
  int scr_w;
  int scr_h;
  int x;
  int y;
  int dy;
  int preserved_x;
  int preserved_y;
  int mon_num;
  GdkScreen *screen = (GdkScreen *)((void *)0);
  GdkRectangle mon_size;
  GtkWidget *tipwindow = pidgin_tooltip.tipwindow;
  gdk_display_get_pointer(gdk_display_get_default(),&screen,&x,&y,0);
  mon_num = gdk_screen_get_monitor_at_point(screen,x,y);
  gdk_screen_get_monitor_geometry(screen,mon_num,&mon_size);
  scr_w = (mon_size.width + mon_size.x);
  scr_h = (mon_size.height + mon_size.y);
  dy = (gdk_display_get_default_cursor_size(gdk_display_get_default()) / 2);
  if (w > mon_size.width) 
    w = (mon_size.width - 10);
  if (h > mon_size.height) 
    h = (mon_size.height - 10);
  preserved_x = x;
  preserved_y = y;
  x -= ((w >> 1) + 4);
  if (((y + h) + 4) > scr_h) 
    y = (((y - h) - dy) - 5);
  else 
    y = ((y + dy) + 6);
  if (y < mon_size.y) 
    y = mon_size.y;
  if (y != mon_size.y) {
    if ((x + w) > scr_w) 
      x -= (((x + w) + 5) - scr_w);
    else if (x < mon_size.x) 
      x = mon_size.x;
  }
  else {
    x -= ((w / 2) + 10);
    if (x < mon_size.x) 
      x = mon_size.x;
  }
/* If the mouse covered by the tipwindow, move the tipwindow
	 * to the righ side of the it */
  if ((((preserved_x >= x) && (preserved_x <= (x + w))) && (preserved_y >= y)) && (preserved_y <= (y + h))) 
    x = (preserved_x + dy);
  gtk_widget_set_size_request(tipwindow,w,h);
  gtk_window_move(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)tipwindow),gtk_window_get_type()))),x,y);
  gtk_widget_show(tipwindow);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tipwindow),((GType )(20 << 2))))),"expose_event",((GCallback )pidgin_tooltip_expose_event),data,0,((GConnectFlags )0));
/* Hide the tooltip when the widget is destroyed */
  sig = (g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pidgin_tooltip.widget),((GType )(20 << 2))))),"destroy",((GCallback )pidgin_tooltip_destroy),0,0,((GConnectFlags )0)));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tipwindow),((GType )(20 << 2))))),"destroy",((GCallback )g_source_remove),((gpointer )((glong )sig)),0,G_CONNECT_SWAPPED);
}

void pidgin_tooltip_show(GtkWidget *widget,gpointer userdata,PidginTooltipCreate create_tooltip,PidginTooltipPaint paint_tooltip)
{
  GtkWidget *tipwindow;
  int w;
  int h;
  pidgin_tooltip_destroy();
  pidgin_tooltip.widget = gtk_widget_get_toplevel(widget);
  pidgin_tooltip.tipwindow = (tipwindow = setup_tooltip_window());
  pidgin_tooltip.paint_tooltip = paint_tooltip;
  if (!(( *create_tooltip)(tipwindow,userdata,&w,&h) != 0)) {
    pidgin_tooltip_destroy();
    return ;
  }
  setup_tooltip_window_position(userdata,w,h);
}

static void reset_data_treepath(PidginTooltipData *data)
{
  gtk_tree_path_free(data -> common.treeview.path);
  data -> common.treeview.path = ((GtkTreePath *)((void *)0));
}

static void pidgin_tooltip_draw(PidginTooltipData *data)
{
  GtkWidget *tipwindow;
  int w;
  int h;
  pidgin_tooltip_destroy();
  pidgin_tooltip.widget = gtk_widget_get_toplevel((data -> widget));
  pidgin_tooltip.tipwindow = (tipwindow = setup_tooltip_window());
  pidgin_tooltip.paint_tooltip = (data -> paint_tooltip);
  if (!(( *data -> common.widget.create_tooltip)(tipwindow,(data -> userdata),&w,&h) != 0)) {
    if (tipwindow == pidgin_tooltip.tipwindow) 
      pidgin_tooltip_destroy();
    return ;
  }
  setup_tooltip_window_position((data -> userdata),w,h);
}

static void pidgin_tooltip_draw_tree(PidginTooltipData *data)
{
  GtkWidget *tipwindow;
  GtkTreePath *path = (GtkTreePath *)((void *)0);
  int w;
  int h;
  if (!(gtk_tree_view_get_path_at_pos(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(data -> widget)),gtk_tree_view_get_type()))),pidgin_tooltip.tip_rect.x,(pidgin_tooltip.tip_rect.y + (pidgin_tooltip.tip_rect.height / 2)),&path,0,0,0) != 0)) {
    pidgin_tooltip_destroy();
    return ;
  }
  if (data -> common.treeview.path != 0) {
    if (gtk_tree_path_compare(data -> common.treeview.path,path) == 0) {
      gtk_tree_path_free(path);
      return ;
    }
    gtk_tree_path_free(data -> common.treeview.path);
    data -> common.treeview.path = ((GtkTreePath *)((void *)0));
  }
  pidgin_tooltip_destroy();
  pidgin_tooltip.widget = gtk_widget_get_toplevel((data -> widget));
  pidgin_tooltip.tipwindow = (tipwindow = setup_tooltip_window());
  pidgin_tooltip.paint_tooltip = (data -> paint_tooltip);
  if (!(( *data -> common.treeview.create_tooltip)(tipwindow,path,(data -> userdata),&w,&h) != 0)) {
    if (tipwindow == pidgin_tooltip.tipwindow) 
      pidgin_tooltip_destroy();
    gtk_tree_path_free(path);
    return ;
  }
  setup_tooltip_window_position((data -> userdata),w,h);
  data -> common.treeview.path = path;
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pidgin_tooltip.tipwindow),((GType )(20 << 2))))),"destroy",((GCallback )reset_data_treepath),data,0,G_CONNECT_SWAPPED);
}

static gboolean pidgin_tooltip_timeout(gpointer data)
{
  PidginTooltipData *tdata = data;
  pidgin_tooltip.timeout = 0;
  if ((({
    GTypeInstance *__inst = (GTypeInstance *)(tdata -> widget);
    GType __t = gtk_tree_view_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
    pidgin_tooltip_draw_tree(data);
  else 
    pidgin_tooltip_draw(data);
  return 0;
}

static gboolean row_motion_cb(GtkWidget *tv,GdkEventMotion *event,gpointer userdata)
{
  GtkTreePath *path;
  if ((event -> window) != gtk_tree_view_get_bin_window(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gtk_tree_view_get_type()))))) 
/* The cursor is probably on the TreeView's header. */
    return 0;
  initialize_tooltip_delay();
  if (!(enable_tooltips != 0)) 
    return 0;
  if (pidgin_tooltip.timeout != 0) {
    if (((event -> y) >= pidgin_tooltip.tip_rect.y) && (((event -> y) - pidgin_tooltip.tip_rect.height) <= pidgin_tooltip.tip_rect.y)) 
      return 0;
/* We've left the cell.  Remove the timeout and create a new one below */
    pidgin_tooltip_destroy();
  }
  gtk_tree_view_get_path_at_pos(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gtk_tree_view_get_type()))),(event -> x),(event -> y),&path,0,0,0);
  if (path == ((GtkTreePath *)((void *)0))) {
    pidgin_tooltip_destroy();
    return 0;
  }
  gtk_tree_view_get_cell_area(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)tv),gtk_tree_view_get_type()))),path,0,&pidgin_tooltip.tip_rect);
  gtk_tree_path_free(path);
  pidgin_tooltip.timeout = (g_timeout_add(tooltip_delay,((GSourceFunc )pidgin_tooltip_timeout),userdata));
  return 0;
}

static gboolean widget_leave_cb(GtkWidget *tv,GdkEvent *event,gpointer userdata)
{
  pidgin_tooltip_destroy();
  return 0;
}

gboolean pidgin_tooltip_setup_for_treeview(GtkWidget *tree,gpointer userdata,PidginTooltipCreateForTree create_tooltip,PidginTooltipPaint paint_tooltip)
{
  PidginTooltipData *tdata = (PidginTooltipData *)(g_malloc0_n(1,(sizeof(PidginTooltipData ))));
  tdata -> widget = tree;
  tdata -> userdata = userdata;
  tdata -> common.treeview.create_tooltip = create_tooltip;
  tdata -> paint_tooltip = paint_tooltip;
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"motion-notify-event",((GCallback )row_motion_cb),tdata,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"leave-notify-event",((GCallback )widget_leave_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"scroll-event",((GCallback )widget_leave_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tree),((GType )(20 << 2))))),"destroy",((GCallback )destroy_tooltip_data),tdata,0,G_CONNECT_SWAPPED);
  return (!0);
}

static gboolean widget_motion_cb(GtkWidget *widget,GdkEvent *event,gpointer data)
{
  initialize_tooltip_delay();
  pidgin_tooltip_destroy();
  if (!(enable_tooltips != 0)) 
    return 0;
  pidgin_tooltip.timeout = (g_timeout_add(tooltip_delay,((GSourceFunc )pidgin_tooltip_timeout),data));
  return 0;
}

gboolean pidgin_tooltip_setup_for_widget(GtkWidget *widget,gpointer userdata,PidginTooltipCreate create_tooltip,PidginTooltipPaint paint_tooltip)
{
  PidginTooltipData *wdata = (PidginTooltipData *)(g_malloc0_n(1,(sizeof(PidginTooltipData ))));
  wdata -> widget = widget;
  wdata -> userdata = userdata;
  wdata -> common.widget.create_tooltip = create_tooltip;
  wdata -> paint_tooltip = paint_tooltip;
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"motion-notify-event",((GCallback )widget_motion_cb),wdata,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"leave-notify-event",((GCallback )widget_leave_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"scroll-event",((GCallback )widget_leave_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"destroy",((GCallback )destroy_tooltip_data),wdata,0,G_CONNECT_SWAPPED);
  return (!0);
}
