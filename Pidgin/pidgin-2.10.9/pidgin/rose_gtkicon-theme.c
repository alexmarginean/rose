/*
 * Icon Themes for Pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "gtkicon-theme.h"
#include "pidginstock.h"
#include <gtk/gtk.h>
#define PIDGIN_ICON_THEME_GET_PRIVATE(Gobject) \
	((PidginIconThemePrivate *) ((PIDGIN_ICON_THEME(Gobject))->priv))
/******************************************************************************
 * Structs
 *****************************************************************************/
typedef struct __unnamed_class___F0_L35_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L54R__Pe___variable_name_unknown_scope_and_name__scope__icon_files {
/* used to store filenames of diffrent icons */
GHashTable *icon_files;}PidginIconThemePrivate;
/******************************************************************************
 * Globals
 *****************************************************************************/
static GObjectClass *parent_class = (GObjectClass *)((void *)0);
/******************************************************************************
 * GObject Stuff
 *****************************************************************************/

static void pidgin_icon_theme_init(GTypeInstance *instance,gpointer klass)
{
  PidginIconThemePrivate *priv;
  ( *((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)instance),pidgin_icon_theme_get_type())))).priv = ((PidginIconThemePrivate *)(g_malloc0_n(1,(sizeof(PidginIconThemePrivate )))));
  priv = ((PidginIconThemePrivate *)( *((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)instance),pidgin_icon_theme_get_type())))).priv);
  priv -> icon_files = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
}

static void pidgin_icon_theme_finalize(GObject *obj)
{
  PidginIconThemePrivate *priv;
  priv = ((PidginIconThemePrivate *)( *((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)obj),pidgin_icon_theme_get_type())))).priv);
  g_hash_table_destroy((priv -> icon_files));
  g_free(priv);
  ( *(parent_class -> finalize))(obj);
}

static void pidgin_icon_theme_class_init(PidginIconThemeClass *klass)
{
  GObjectClass *obj_class = (GObjectClass *)(g_type_check_class_cast(((GTypeClass *)klass),((GType )(20 << 2))));
  parent_class = (g_type_class_peek_parent(klass));
  obj_class -> finalize = pidgin_icon_theme_finalize;
}

GType pidgin_icon_theme_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(PidginIconThemeClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )pidgin_icon_theme_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(PidginIconTheme ))), (0), (pidgin_icon_theme_init), ((const GTypeValueTable *)((void *)0))
/* base_init */
/* base_finalize */
/* class_init */
/* class_finalize */
/* class_data */
/* n_preallocs */
/* instance_init */
/* value table */
};
    type = g_type_register_static(purple_theme_get_type(),"PidginIconTheme",&info,G_TYPE_FLAG_ABSTRACT);
  }
  return type;
}
/*****************************************************************************
 * Public API functions
 *****************************************************************************/

const gchar *pidgin_icon_theme_get_icon(PidginIconTheme *theme,const gchar *id)
{
  PidginIconThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_icon_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_ICON_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PidginIconThemePrivate *)( *((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type())))).priv);
  return (g_hash_table_lookup((priv -> icon_files),id));
}

void pidgin_icon_theme_set_icon(PidginIconTheme *theme,const gchar *id,const gchar *filename)
{
  PidginIconThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = pidgin_icon_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_ICON_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PidginIconThemePrivate *)( *((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type())))).priv);
  if (filename != ((const gchar *)((void *)0))) 
    g_hash_table_replace((priv -> icon_files),(g_strdup(id)),(g_strdup(filename)));
  else 
    g_hash_table_remove((priv -> icon_files),id);
}
