/**
 * @file gtksmiley.c GTK+ Smiley Manager API
 * @ingroup pidgin
 */
/*
 * pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "pidgin.h"
#include "debug.h"
#include "notify.h"
#include "smiley.h"
#include "gtkimhtml.h"
#include "gtksmiley.h"
#include "gtkutils.h"
#include "pidginstock.h"
#define PIDGIN_RESPONSE_MODIFY 1000

struct _PidginSmiley 
{
  PurpleSmiley *smiley;
  GtkWidget *parent;
  GtkWidget *smile;
  GtkWidget *smiley_image;
  gchar *filename;
  GdkPixbuf *custom_pixbuf;
/** @since 2.6.0 */
  gpointer data;
/** @since 2.6.0 */
  gsize datasize;
/** @since 2.6.0 */
  gint entry_len;
}
;
typedef struct __unnamed_class___F0_L55_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__treeview__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1172R__Pe___variable_name_unknown_scope_and_name__scope__model {
GtkWidget *window;
GtkWidget *treeview;
GtkListStore *model;}SmileyManager;
enum __unnamed_enum___F0_L63_C1_ICON__COMMA__SHORTCUT__COMMA__SMILEY__COMMA__N_COL {ICON,SHORTCUT,SMILEY,N_COL};
static SmileyManager *smiley_manager = (SmileyManager *)((void *)0);
static GSList *gtk_smileys = (GSList *)((void *)0);

static void pidgin_smiley_destroy(PidginSmiley *smiley)
{
  if ((smiley -> smiley) != 0) 
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(smiley -> smiley)),((GType )(20 << 2))))),"edit-dialog",0);
  gtk_widget_destroy((smiley -> parent));
  g_free((smiley -> filename));
  if ((smiley -> custom_pixbuf) != 0) 
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(smiley -> custom_pixbuf)),((GType )(20 << 2))))));
  g_free(smiley);
}
/******************************************************************************
 * GtkIMHtmlSmileys stuff
 *****************************************************************************/
/* Perhaps these should be in gtkimhtml.c instead. -- sadrul */

static void add_gtkimhtml_to_list(GtkIMHtmlSmiley *gtksmiley)
{
  gtk_smileys = g_slist_prepend(gtk_smileys,gtksmiley);
  purple_debug_info("gtksmiley","adding %s to gtk_smileys\n",(gtksmiley -> smile));
}

static void shortcut_changed_cb(PurpleSmiley *smiley,gpointer dontcare,GtkIMHtmlSmiley *gtksmiley)
{
  g_free((gtksmiley -> smile));
  gtksmiley -> smile = g_strdup(purple_smiley_get_shortcut(smiley));
}

static void image_changed_cb(PurpleSmiley *smiley,gpointer dontcare,GtkIMHtmlSmiley *gtksmiley)
{
  const char *file;
  g_free((gtksmiley -> file));
  file = purple_imgstore_get_filename((purple_smiley_get_stored_image(smiley)));
  gtksmiley -> file = g_build_filename(purple_smileys_get_storing_dir(),file,((void *)((void *)0)));
  gtk_imhtml_smiley_reload(gtksmiley);
}

static GtkIMHtmlSmiley *smiley_purple_to_gtkimhtml(PurpleSmiley *smiley)
{
  GtkIMHtmlSmiley *gtksmiley;
  gchar *filename;
  const gchar *file;
  file = purple_imgstore_get_filename((purple_smiley_get_stored_image(smiley)));
  filename = g_build_filename(purple_smileys_get_storing_dir(),file,((void *)((void *)0)));
  gtksmiley = gtk_imhtml_smiley_create(filename,purple_smiley_get_shortcut(smiley),0,GTK_IMHTML_SMILEY_CUSTOM);
  g_free(filename);
/* Make sure the shortcut for the GtkIMHtmlSmiley is updated with the PurpleSmiley */
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)smiley),((GType )(20 << 2))))),"notify::shortcut",((GCallback )shortcut_changed_cb),gtksmiley,0,((GConnectFlags )0));
/* And update the pixbuf too when the image is changed */
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)smiley),((GType )(20 << 2))))),"notify::image",((GCallback )image_changed_cb),gtksmiley,0,((GConnectFlags )0));
  return gtksmiley;
}

void pidgin_smiley_del_from_list(PurpleSmiley *smiley)
{
  GSList *list = (GSList *)((void *)0);
  GtkIMHtmlSmiley *gtksmiley;
  if (gtk_smileys == ((GSList *)((void *)0))) 
    return ;
  list = gtk_smileys;
{
    for (; list != 0; list = (list -> next)) {
      gtksmiley = ((GtkIMHtmlSmiley *)(list -> data));
      if (strcmp((gtksmiley -> smile),purple_smiley_get_shortcut(smiley)) != 0) 
        continue; 
      gtk_imhtml_smiley_destroy(gtksmiley);
      g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)smiley),((GType )(20 << 2))))),G_SIGNAL_MATCH_DATA,0,0,0,0,gtksmiley);
      break; 
    }
  }
  if (list != 0) 
    gtk_smileys = g_slist_delete_link(gtk_smileys,list);
}

void pidgin_smiley_add_to_list(PurpleSmiley *smiley)
{
  GtkIMHtmlSmiley *gtksmiley;
  gtksmiley = smiley_purple_to_gtkimhtml(smiley);
  add_gtkimhtml_to_list(gtksmiley);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)smiley),((GType )(20 << 2))))),"destroy",((GCallback )pidgin_smiley_del_from_list),0,0,((GConnectFlags )0));
}

void pidgin_smileys_init()
{
  GList *smileys;
  PurpleSmiley *smiley;
  if (gtk_smileys != ((GSList *)((void *)0))) 
    return ;
  smileys = purple_smileys_get_all();
  for (; smileys != 0; smileys = g_list_delete_link(smileys,smileys)) {
    smiley = ((PurpleSmiley *)(smileys -> data));
    pidgin_smiley_add_to_list(smiley);
  }
}

void pidgin_smileys_uninit()
{
  GSList *list;
  GtkIMHtmlSmiley *gtksmiley;
  list = gtk_smileys;
  if (list == ((GSList *)((void *)0))) 
    return ;
  for (; list != 0; list = g_slist_delete_link(list,list)) {
    gtksmiley = ((GtkIMHtmlSmiley *)(list -> data));
    gtk_imhtml_smiley_destroy(gtksmiley);
  }
  gtk_smileys = ((GSList *)((void *)0));
}

GSList *pidgin_smileys_get_all()
{
  return gtk_smileys;
}
/******************************************************************************
 * Manager stuff
 *****************************************************************************/
static void refresh_list();
/******************************************************************************
 * The Add dialog
 ******************************************************************************/

static void do_add(GtkWidget *widget,PidginSmiley *s)
{
  const gchar *entry;
  PurpleSmiley *emoticon;
  entry = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(s -> smile)),gtk_entry_get_type()))));
  emoticon = purple_smileys_find_by_shortcut(entry);
  if ((emoticon != 0) && (emoticon != (s -> smiley))) {
    gchar *msg;
    msg = g_strdup_printf(((const char *)(dgettext("pidgin","A custom smiley for \'%s\' already exists.  Please use a different shortcut."))),entry);
    purple_notify_message((s -> parent),PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Custom Smiley"))),((const char *)(dgettext("pidgin","Duplicate Shortcut"))),msg,0,0);
    g_free(msg);
    return ;
  }
  if ((s -> smiley) != 0) {
    if ((s -> filename) != 0) {
      gchar *data = (gchar *)((void *)0);
      size_t len;
      GError *err = (GError *)((void *)0);
      if (!(g_file_get_contents((s -> filename),&data,&len,&err) != 0)) {
        purple_debug_error("gtksmiley","Error reading %s: %s\n",(s -> filename),(err -> message));
        g_error_free(err);
        return ;
      }
      purple_smiley_set_data((s -> smiley),((guchar *)data),len);
    }
    purple_smiley_set_shortcut((s -> smiley),entry);
  }
  else {
    purple_debug_info("gtksmiley","adding a new smiley\n");
    if ((s -> filename) == ((gchar *)((void *)0))) {
      gchar *buffer = (gchar *)((void *)0);
      gsize size = 0;
      gchar *filename;
      const gchar *dirname = purple_smileys_get_storing_dir();
/* since this may be called before purple_smiley_new_* has ever been
			 called, we create the storing dir, if it doesn't exist yet, to be
			 able to save the pixbuf before adding the smiley */
      if (!(g_file_test(dirname,G_FILE_TEST_IS_DIR) != 0)) {
        purple_debug_info("gtksmiley","Creating smileys directory.\n");
        if (mkdir(dirname,(256 | 128 | 64)) < 0) {
          purple_debug_error("gtksmiley","Unable to create directory %s: %s\n",dirname,g_strerror( *__errno_location()));
        }
      }
      if (((s -> data) != 0) && ((s -> datasize) != 0UL)) {
/* Cached data & size in memory */
        buffer = (s -> data);
        size = (s -> datasize);
      }
      else {
/* Get the smiley from the custom pixbuf */
        gdk_pixbuf_save_to_buffer((s -> custom_pixbuf),&buffer,&size,"png",0,"compression","9",((void *)((void *)0)),((void *)((void *)0)));
      }
      filename = purple_util_get_image_filename(buffer,size);
      s -> filename = g_build_filename(dirname,filename,((void *)((void *)0)));
      purple_util_write_data_to_file_absolute((s -> filename),buffer,size);
      g_free(filename);
      g_free(buffer);
    }
    emoticon = purple_smiley_new_from_file(entry,(s -> filename));
    if (emoticon != 0) 
      pidgin_smiley_add_to_list(emoticon);
  }
  if (smiley_manager != ((SmileyManager *)((void *)0))) 
    refresh_list();
  gtk_widget_destroy((s -> parent));
}

static void do_add_select_cb(GtkWidget *widget,gint resp,PidginSmiley *s)
{
  switch(resp){
    case -3:
{
      do_add(widget,s);
      break; 
    }
    case -6:
{
    }
    case -4:
{
      gtk_widget_destroy((s -> parent));
      break; 
    }
    default:
{
      purple_debug_error("gtksmiley","no valid response\n");
      break; 
    }
  }
}

static void do_add_file_cb(const char *filename,gpointer data)
{
  PidginSmiley *s = data;
  GdkPixbuf *pixbuf;
  if (!(filename != 0)) 
    return ;
  g_free((s -> filename));
  s -> filename = g_strdup(filename);
  pixbuf = pidgin_pixbuf_new_from_file_at_scale(filename,64,64,0);
  gtk_image_set_from_pixbuf(((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)(s -> smiley_image)),gtk_image_get_type()))),pixbuf);
  if (pixbuf != 0) 
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
  gtk_widget_grab_focus((s -> smile));
  if ((s -> entry_len) > 0) 
    gtk_dialog_set_response_sensitive(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(s -> parent)),gtk_dialog_get_type()))),GTK_RESPONSE_ACCEPT,(!0));
}

static void open_image_selector(GtkWidget *widget,PidginSmiley *psmiley)
{
  GtkWidget *file_chooser;
  file_chooser = pidgin_buddy_icon_chooser_new(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_widget_get_toplevel(widget))),gtk_window_get_type()))),do_add_file_cb,psmiley);
  gtk_window_set_title(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)file_chooser),gtk_window_get_type()))),((const char *)(dgettext("pidgin","Custom Smiley"))));
  gtk_window_set_role(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)file_chooser),gtk_window_get_type()))),"file-selector-custom-smiley");
  gtk_widget_show_all(file_chooser);
}

static void smiley_name_insert_cb(GtkEditable *editable,gchar *new_text,gint new_text_length,gint *position,gpointer user_data)
{
  PidginSmiley *s = user_data;
  if (new_text_length != -1) 
    s -> entry_len += new_text_length;
  else 
    s -> entry_len += strlen(new_text);
  if ((((s -> filename) != ((gchar *)((void *)0))) || ((s -> custom_pixbuf) != ((GdkPixbuf *)((void *)0)))) || ((s -> smiley) != ((PurpleSmiley *)((void *)0)))) 
    gtk_dialog_set_response_sensitive(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(s -> parent)),gtk_dialog_get_type()))),GTK_RESPONSE_ACCEPT,(!0));
}

static void smiley_name_delete_cb(GtkEditable *editable,gint start_pos,gint end_pos,gpointer user_data)
{
  PidginSmiley *s = user_data;
  s -> entry_len -= (end_pos - start_pos);
  if ((s -> entry_len) <= 0) 
    gtk_dialog_set_response_sensitive(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(s -> parent)),gtk_dialog_get_type()))),GTK_RESPONSE_ACCEPT,0);
}

PidginSmiley *pidgin_smiley_edit(GtkWidget *widget,PurpleSmiley *smiley)
{
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *filech;
  GtkWidget *window;
  GdkPixbuf *pixbuf = (GdkPixbuf *)((void *)0);
  PurpleStoredImage *stored_img;
  PidginSmiley *s = (PidginSmiley *)(g_malloc0_n(1,(sizeof(PidginSmiley ))));
  s -> smiley = smiley;
  window = gtk_dialog_new_with_buttons(((smiley != 0)?((const char *)(dgettext("pidgin","Edit Smiley"))) : ((const char *)(dgettext("pidgin","Add Smiley")))),((widget != 0)?((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_window_get_type()))) : ((struct _GtkWindow *)((void *)0))),(GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_NO_SEPARATOR),"gtk-cancel",GTK_RESPONSE_CANCEL,((smiley != 0)?"gtk-save" : "gtk-add"),GTK_RESPONSE_ACCEPT,((void *)((void *)0)));
  s -> parent = window;
  if (smiley != 0) 
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)smiley),((GType )(20 << 2))))),"edit-dialog",window);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_container_get_type()))),12);
  gtk_dialog_set_default_response(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),GTK_RESPONSE_ACCEPT);
  g_signal_connect_data(window,"response",((GCallback )do_add_select_cb),s,0,((GConnectFlags )0));
/* The vbox */
  vbox = gtk_vbox_new(0,12);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type())))).vbox),gtk_container_get_type()))),vbox);
  gtk_widget_show(vbox);
/* The hbox */
  hbox = gtk_hbox_new(0,12);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)((GtkVBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_vbox_get_type())))),gtk_container_get_type()))),hbox);
  label = gtk_label_new_with_mnemonic(((const char *)(dgettext("pidgin","_Image:"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  gtk_widget_show(label);
  filech = gtk_button_new();
  gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),filech,0,0,0);
  pidgin_set_accessible_label(filech,label);
  s -> smiley_image = gtk_image_new();
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)filech),gtk_container_get_type()))),(s -> smiley_image));
  if ((smiley != 0) && ((stored_img = purple_smiley_get_stored_image(smiley)) != 0)) {
    pixbuf = pidgin_pixbuf_from_imgstore(stored_img);
    purple_imgstore_unref(stored_img);
  }
  else {
    GtkIconSize icon_size = gtk_icon_size_from_name("pidgin-icon-size-tango-small");
    pixbuf = gtk_widget_render_icon(window,"pidgin-select-avatar",icon_size,"PidginSmiley");
  }
  gtk_image_set_from_pixbuf(((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)(s -> smiley_image)),gtk_image_get_type()))),pixbuf);
  if (pixbuf != ((GdkPixbuf *)((void *)0))) 
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)filech),((GType )(20 << 2))))),"clicked",((GCallback )open_image_selector),s,0,((GConnectFlags )0));
  gtk_widget_show_all(hbox);
/* info */
  hbox = gtk_hbox_new(0,12);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)((GtkVBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_vbox_get_type())))),gtk_container_get_type()))),hbox);
/* Shortcut text */
  label = gtk_label_new_with_mnemonic(((const char *)(dgettext("pidgin","S_hortcut text:"))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  gtk_widget_show(label);
  s -> smile = gtk_entry_new();
  gtk_entry_set_activates_default(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(s -> smile)),gtk_entry_get_type()))),(!0));
  pidgin_set_accessible_label((s -> smile),label);
  if (smiley != 0) {
    const char *shortcut = purple_smiley_get_shortcut(smiley);
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(s -> smile)),gtk_entry_get_type()))),shortcut);
    s -> entry_len = (strlen(shortcut));
  }
  else 
    gtk_dialog_set_response_sensitive(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),GTK_RESPONSE_ACCEPT,0);
/* gtk_entry_get_text_length is 2.14+, so we'll just keep track ourselves */
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(s -> smile)),((GType )(20 << 2))))),"insert-text",((GCallback )smiley_name_insert_cb),s,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(s -> smile)),((GType )(20 << 2))))),"delete-text",((GCallback )smiley_name_delete_cb),s,0,((GConnectFlags )0));
  gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),(s -> smile),0,0,0);
  gtk_widget_show((s -> smile));
  gtk_widget_show(hbox);
  gtk_widget_show(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_widget_get_type()))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"destroy",((GCallback )pidgin_smiley_destroy),s,0,G_CONNECT_SWAPPED);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"destroy",((GCallback )purple_notify_close_with_handle),s,0,((GConnectFlags )0));
  return s;
}

void pidgin_smiley_editor_set_shortcut(PidginSmiley *editor,const gchar *shortcut)
{
  gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(editor -> smile)),gtk_entry_get_type()))),((shortcut != 0)?shortcut : ""));
}

void pidgin_smiley_editor_set_image(PidginSmiley *editor,GdkPixbuf *image)
{
  if ((editor -> custom_pixbuf) != 0) 
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(editor -> custom_pixbuf)),((GType )(20 << 2))))));
  editor -> custom_pixbuf = (((image != 0)?g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)image),((GType )(20 << 2)))))) : ((void *)((void *)0))));
  if (image != 0) {
    gtk_image_set_from_pixbuf(((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)(editor -> smiley_image)),gtk_image_get_type()))),image);
    if ((editor -> entry_len) > 0) 
      gtk_dialog_set_response_sensitive(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(editor -> parent)),gtk_dialog_get_type()))),GTK_RESPONSE_ACCEPT,(!0));
  }
  else 
    gtk_dialog_set_response_sensitive(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(editor -> parent)),gtk_dialog_get_type()))),GTK_RESPONSE_ACCEPT,0);
}

void pidgin_smiley_editor_set_data(PidginSmiley *editor,gpointer data,gsize datasize)
{
  editor -> data = data;
  editor -> datasize = datasize;
}
/******************************************************************************
 * Delete smiley
 *****************************************************************************/

static void delete_foreach(GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter,gpointer data)
{
  PurpleSmiley *smiley = (PurpleSmiley *)((void *)0);
  gtk_tree_model_get(model,iter,SMILEY,&smiley,-1);
  if (smiley != ((PurpleSmiley *)((void *)0))) {
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)smiley),((GType )(20 << 2))))));
    pidgin_smiley_del_from_list(smiley);
    purple_smiley_delete(smiley);
  }
}

static void append_to_list(GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter,gpointer data)
{
  GList **list = data;
   *list = g_list_prepend( *list,(gtk_tree_path_copy(path)));
}

static void smiley_delete(SmileyManager *dialog)
{
  GtkTreeSelection *selection;
  GList *list = (GList *)((void *)0);
  selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treeview)),gtk_tree_view_get_type()))));
  gtk_tree_selection_selected_foreach(selection,delete_foreach,dialog);
  gtk_tree_selection_selected_foreach(selection,append_to_list,(&list));
  while(list != 0){
    GtkTreeIter iter;
    if (gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter,(list -> data)) != 0) 
      gtk_list_store_remove(((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_list_store_get_type()))),&iter);
    gtk_tree_path_free((list -> data));
    list = g_list_delete_link(list,list);
  }
}
/******************************************************************************
 * The Smiley Manager
 *****************************************************************************/

static void add_columns(GtkWidget *treeview,SmileyManager *dialog)
{
  GtkCellRenderer *rend;
  GtkTreeViewColumn *column;
/* Icon */
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_title(column,((const char *)(dgettext("pidgin","Smiley"))));
  gtk_tree_view_column_set_resizable(column,(!0));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),column);
  rend = gtk_cell_renderer_pixbuf_new();
  gtk_tree_view_column_pack_start(column,rend,0);
  gtk_tree_view_column_add_attribute(column,rend,"pixbuf",ICON);
/* Shortcut Text */
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_set_title(column,((const char *)(dgettext("pidgin","Shortcut Text"))));
  gtk_tree_view_column_set_resizable(column,(!0));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),column);
  rend = gtk_cell_renderer_text_new();
  gtk_tree_view_column_pack_start(column,rend,(!0));
  gtk_tree_view_column_add_attribute(column,rend,"text",SHORTCUT);
}

static void store_smiley_add(PurpleSmiley *smiley)
{
  GtkTreeIter iter;
  PurpleStoredImage *img;
  GdkPixbuf *sized_smiley = (GdkPixbuf *)((void *)0);
  if (smiley_manager == ((SmileyManager *)((void *)0))) 
    return ;
  img = purple_smiley_get_stored_image(smiley);
  if (img != ((PurpleStoredImage *)((void *)0))) {
    GdkPixbuf *smiley_image = pidgin_pixbuf_from_imgstore(img);
    purple_imgstore_unref(img);
    if (smiley_image != ((GdkPixbuf *)((void *)0))) {
      if ((gdk_pixbuf_get_width(smiley_image) > 22) || (gdk_pixbuf_get_height(smiley_image) > 22)) {
        sized_smiley = gdk_pixbuf_scale_simple(smiley_image,22,22,GDK_INTERP_HYPER);
        g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)smiley_image),((GType )(20 << 2))))));
      }
      else {
/* don't scale up smaller smileys, avoid blurryness */
        sized_smiley = smiley_image;
      }
    }
  }
  gtk_list_store_append((smiley_manager -> model),&iter);
  gtk_list_store_set((smiley_manager -> model),&iter,ICON,sized_smiley,SHORTCUT,purple_smiley_get_shortcut(smiley),SMILEY,smiley,-1);
  if (sized_smiley != ((GdkPixbuf *)((void *)0))) 
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)sized_smiley),((GType )(20 << 2))))));
}

static void populate_smiley_list(SmileyManager *dialog)
{
  GList *list;
  PurpleSmiley *emoticon;
  gtk_list_store_clear((dialog -> model));
  for (list = purple_smileys_get_all(); list != ((GList *)((void *)0)); list = g_list_delete_link(list,list)) {
    emoticon = ((PurpleSmiley *)(list -> data));
    store_smiley_add(emoticon);
  }
}

static void smile_selected_cb(GtkTreeSelection *sel,SmileyManager *dialog)
{
  gint selected;
  selected = gtk_tree_selection_count_selected_rows(sel);
  gtk_dialog_set_response_sensitive(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> window)),gtk_dialog_get_type()))),GTK_RESPONSE_NO,(selected > 0));
  gtk_dialog_set_response_sensitive(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> window)),gtk_dialog_get_type()))),1000,(selected > 0));
}

static void smiley_edit_iter(SmileyManager *dialog,GtkTreeIter *iter)
{
  PurpleSmiley *smiley = (PurpleSmiley *)((void *)0);
  GtkWidget *window = (GtkWidget *)((void *)0);
  gtk_tree_model_get(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),iter,SMILEY,&smiley,-1);
  if ((window = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)smiley),((GType )(20 << 2))))),"edit-dialog"))) != ((GtkWidget *)((void *)0))) 
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_window_get_type()))));
  else 
    pidgin_smiley_edit(gtk_widget_get_toplevel(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treeview)),gtk_widget_get_type())))),smiley);
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)smiley),((GType )(20 << 2))))));
}

static void smiley_edit_cb(GtkTreeView *treeview,GtkTreePath *path,GtkTreeViewColumn *col,gpointer data)
{
  GtkTreeIter iter;
  SmileyManager *dialog = data;
  gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))),&iter,path);
  smiley_edit_iter(dialog,&iter);
}

static void edit_selected_cb(GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter,gpointer data)
{
  smiley_edit_iter(data,iter);
}

static void smiley_got_url(PurpleUtilFetchUrlData *url_data,gpointer user_data,const gchar *smileydata,size_t len,const gchar *error_message)
{
  SmileyManager *dialog = user_data;
  FILE *f;
  gchar *path;
  size_t wc;
  PidginSmiley *ps;
  GdkPixbuf *image;
  if ((error_message != ((const gchar *)((void *)0))) || (len == 0)) {
    return ;
  }
  f = purple_mkstemp(&path,(!0));
  wc = fwrite(smileydata,len,1,f);
  if (wc != 1) {
    purple_debug_warning("smiley_got_url","Unable to write smiley data.\n");
    fclose(f);
    g_unlink(path);
    g_free(path);
    return ;
  }
  fclose(f);
  image = pidgin_pixbuf_new_from_file(path);
  g_unlink(path);
  g_free(path);
  if (!(image != 0)) 
    return ;
  ps = pidgin_smiley_edit((dialog -> window),0);
  pidgin_smiley_editor_set_image(ps,image);
  pidgin_smiley_editor_set_data(ps,g_memdup(smileydata,len),len);
}

static void smiley_dnd_recv(GtkWidget *widget,GdkDragContext *dc,guint x,guint y,GtkSelectionData *sd,guint info,guint t,gpointer user_data)
{
  SmileyManager *dialog = user_data;
  gchar *name = g_strchomp(((gchar *)(sd -> data)));
  if (((sd -> length) >= 0) && ((sd -> format) == 8)) {
/* Well, it looks like the drag event was cool.
		 * Let's do something with it */
    if (!(g_ascii_strncasecmp(name,"file://",7) != 0)) {
      GError *converr = (GError *)((void *)0);
      gchar *tmp;
      PidginSmiley *ps;
/* It looks like we're dealing with a local file. Let's
			 * just try and read it */
      if (!((tmp = g_filename_from_uri(name,0,&converr)) != 0)) {
        purple_debug_error("smiley dnd","%s\n",((converr != 0)?(converr -> message) : "g_filename_from_uri error"));
        return ;
      }
      ps = pidgin_smiley_edit((dialog -> window),0);
      do_add_file_cb(tmp,ps);
      if (gtk_image_get_pixbuf(((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)(ps -> smiley_image)),gtk_image_get_type())))) == ((GdkPixbuf *)((void *)0))) 
        gtk_dialog_response(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(ps -> parent)),gtk_dialog_get_type()))),GTK_RESPONSE_CANCEL);
      g_free(tmp);
    }
    else if (!(g_ascii_strncasecmp(name,"http://",7) != 0)) {
/* Oo, a web drag and drop. This is where things
			 * will start to get interesting */
      purple_util_fetch_url_request(name,(!0),0,0,0,0,smiley_got_url,dialog);;
    }
    else if (!(g_ascii_strncasecmp(name,"https://",8) != 0)) {
/* purple_util_fetch_url() doesn't support HTTPS */
      char *tmp = g_strdup((name + 1));
      tmp[0] = 'h';
      tmp[1] = 't';
      tmp[2] = 't';
      tmp[3] = 'p';
      purple_util_fetch_url_request(tmp,(!0),0,0,0,0,smiley_got_url,dialog);;
      g_free(tmp);
    }
    gtk_drag_finish(dc,(!0),0,t);
  }
  gtk_drag_finish(dc,0,0,t);
}

static GtkWidget *smiley_list_create(SmileyManager *dialog)
{
  GtkWidget *treeview;
  GtkTreeSelection *sel;
  GtkTargetEntry te[3UL] = {{("text/plain"), (0), (0)}, {("text/uri-list"), (0), (1)}, {("STRING"), (0), (2)}};
/* Create the list model */
  dialog -> model = gtk_list_store_new(N_COL,gdk_pixbuf_get_type(),((GType )(16 << 2)),((GType )(20 << 2)));
/* ICON */
/* SHORTCUT */
/* SMILEY */
/* the actual treeview */
  treeview = gtk_tree_view_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_model_get_type()))));
  dialog -> treeview = treeview;
  gtk_tree_view_set_rules_hint(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),(!0));
  gtk_tree_sortable_set_sort_column_id(((GtkTreeSortable *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),gtk_tree_sortable_get_type()))),SHORTCUT,GTK_SORT_ASCENDING);
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> model)),((GType )(20 << 2))))));
  sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))));
  gtk_tree_selection_set_mode(sel,GTK_SELECTION_MULTIPLE);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)sel),((GType )(20 << 2))))),"changed",((GCallback )smile_selected_cb),dialog,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)treeview),((GType )(20 << 2))))),"row_activated",((GCallback )smiley_edit_cb),dialog,0,((GConnectFlags )0));
  gtk_drag_dest_set(treeview,(GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_HIGHLIGHT | GTK_DEST_DEFAULT_DROP),te,(sizeof(te) / sizeof(te[0])),(GDK_ACTION_COPY | GDK_ACTION_MOVE));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)treeview),((GType )(20 << 2))))),"drag_data_received",((GCallback )smiley_dnd_recv),dialog,0,((GConnectFlags )0));
  gtk_widget_show(treeview);
  add_columns(treeview,dialog);
  populate_smiley_list(dialog);
  return pidgin_make_scrollable(treeview,GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC,GTK_SHADOW_IN,-1,-1);
}

static void refresh_list()
{
  populate_smiley_list(smiley_manager);
}

static void smiley_manager_select_cb(GtkWidget *widget,gint resp,SmileyManager *dialog)
{
  GtkTreeSelection *selection = (GtkTreeSelection *)((void *)0);
  switch(resp){
    case -8:
{
      pidgin_smiley_edit((dialog -> window),0);
      break; 
    }
    case -9:
{
      smiley_delete(dialog);
      break; 
    }
    case -7:
{
    }
    case -4:
{
      gtk_widget_destroy((dialog -> window));
      g_free(smiley_manager);
      smiley_manager = ((SmileyManager *)((void *)0));
      break; 
    }
    case 1000:
{
/* Find smiley of selection... */
      selection = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> treeview)),gtk_tree_view_get_type()))));
      gtk_tree_selection_selected_foreach(selection,edit_selected_cb,dialog);
      break; 
    }
    default:
{
      purple_debug_info("gtksmiley","No valid selection\n");
      break; 
    }
  }
}

void pidgin_smiley_manager_show()
{
  SmileyManager *dialog;
  GtkWidget *win;
  GtkWidget *sw;
  GtkWidget *vbox;
  if (smiley_manager != 0) {
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(smiley_manager -> window)),gtk_window_get_type()))));
    return ;
  }
  dialog = ((SmileyManager *)(g_malloc0_n(1,(sizeof(SmileyManager )))));
  smiley_manager = dialog;
  dialog -> window = (win = gtk_dialog_new_with_buttons(((const char *)(dgettext("pidgin","Custom Smiley Manager"))),0,GTK_DIALOG_DESTROY_WITH_PARENT,"pidgin-add",GTK_RESPONSE_YES,"pidgin-modify",1000,"gtk-delete",GTK_RESPONSE_NO,"gtk-close",GTK_RESPONSE_CLOSE,((void *)((void *)0))));
  gtk_window_set_default_size(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_window_get_type()))),50,400);
  gtk_window_set_role(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_window_get_type()))),"custom_smiley_manager");
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_container_get_type()))),12);
  gtk_dialog_set_response_sensitive(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),GTK_RESPONSE_NO,0);
  gtk_dialog_set_response_sensitive(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),1000,0);
  g_signal_connect_data(win,"response",((GCallback )smiley_manager_select_cb),dialog,0,((GConnectFlags )0));
/* The vbox */
  vbox = gtk_vbox_new(0,12);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type())))).vbox),gtk_container_get_type()))),vbox);
  gtk_widget_show(vbox);
/* get the scrolled window with all stuff */
  sw = smiley_list_create(dialog);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),sw,(!0),(!0),0);
  gtk_widget_show(sw);
  gtk_widget_show(win);
}
