/**
 * @file gtklog.c GTK+ Log viewer
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "pidgin.h"
#include "account.h"
#include "debug.h"
#include "log.h"
#include "notify.h"
#include "request.h"
#include "util.h"
#include "pidginstock.h"
#include "gtkblist.h"
#include "gtkimhtml.h"
#include "gtklog.h"
#include "gtkutils.h"
static GHashTable *log_viewers = (GHashTable *)((void *)0);
static void populate_log_tree(PidginLogViewer *lv);
static PidginLogViewer *syslog_viewer = (PidginLogViewer *)((void *)0);

struct log_viewer_hash_t 
{
  PurpleLogType type;
  char *buddyname;
  PurpleAccount *account;
  PurpleContact *contact;
}
;

static guint log_viewer_hash(gconstpointer data)
{
  const struct log_viewer_hash_t *viewer = data;
  if ((viewer -> contact) != ((PurpleContact *)((void *)0))) 
    return g_direct_hash((viewer -> contact));
  return g_str_hash((viewer -> buddyname)) + g_str_hash((purple_account_get_username((viewer -> account))));
}

static gboolean log_viewer_equal(gconstpointer y,gconstpointer z)
{
  const struct log_viewer_hash_t *a;
  const struct log_viewer_hash_t *b;
  int ret;
  char *normal;
  a = y;
  b = z;
  if ((a -> contact) != ((PurpleContact *)((void *)0))) {
    if ((b -> contact) != ((PurpleContact *)((void *)0))) 
      return (a -> contact) == (b -> contact);
    else 
      return 0;
  }
  else {
    if ((b -> contact) != ((PurpleContact *)((void *)0))) 
      return 0;
  }
  normal = g_strdup(purple_normalize((a -> account),(a -> buddyname)));
  ret = (((a -> account) == (b -> account)) && !(strcmp(normal,purple_normalize((b -> account),(b -> buddyname))) != 0));
  g_free(normal);
  return ret;
}

static void select_first_log(PidginLogViewer *lv)
{
  GtkTreeModel *model;
  GtkTreeIter iter;
  GtkTreeIter it;
  GtkTreePath *path;
  model = ((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> treestore)),gtk_tree_model_get_type())));
  if (!(gtk_tree_model_get_iter_first(model,&iter) != 0)) 
    return ;
  path = gtk_tree_model_get_path(model,&iter);
  if (gtk_tree_model_iter_children(model,&it,&iter) != 0) {
    gtk_tree_view_expand_row(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> treeview)),gtk_tree_view_get_type()))),path,(!0));
    path = gtk_tree_model_get_path(model,&it);
  }
  gtk_tree_selection_select_path(gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> treeview)),gtk_tree_view_get_type())))),path);
  gtk_tree_path_free(path);
}

static const char *log_get_date(PurpleLog *log)
{
  if ((log -> tm) != 0) 
    return purple_date_format_full((log -> tm));
  else 
    return purple_date_format_full((localtime((&log -> time))));
}

static void search_cb(GtkWidget *button,PidginLogViewer *lv)
{
  const char *search_term = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> entry)),gtk_entry_get_type()))));
  GList *logs;
  if (!(( *search_term) != 0)) {
/* reset the tree */
    gtk_tree_store_clear((lv -> treestore));
    populate_log_tree(lv);
    g_free((lv -> search));
    lv -> search = ((char *)((void *)0));
    gtk_imhtml_search_clear(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> imhtml)),gtk_imhtml_get_type()))));
    select_first_log(lv);
    return ;
  }
  if (((lv -> search) != ((char *)((void *)0))) && !(strcmp((lv -> search),search_term) != 0)) {
/* Searching for the same term acts as "Find Next" */
    gtk_imhtml_search_find(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> imhtml)),gtk_imhtml_get_type()))),(lv -> search));
    return ;
  }
  pidgin_set_cursor((lv -> window),GDK_WATCH);
  g_free((lv -> search));
  lv -> search = g_strdup(search_term);
  gtk_tree_store_clear((lv -> treestore));
  gtk_imhtml_delete(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> imhtml)),gtk_imhtml_get_type()))),0,0);
  for (logs = (lv -> logs); logs != ((GList *)((void *)0)); logs = (logs -> next)) {
    char *read = purple_log_read(((PurpleLog *)(logs -> data)),0);
    if (((read != 0) && (( *read) != 0)) && (purple_strcasestr(read,search_term) != 0)) {
      GtkTreeIter iter;
      PurpleLog *log = (logs -> data);
      gtk_tree_store_append((lv -> treestore),&iter,0);
      gtk_tree_store_set((lv -> treestore),&iter,0,log_get_date(log),1,log,-1);
    }
    g_free(read);
  }
  select_first_log(lv);
  pidgin_clear_cursor((lv -> window));
}

static void destroy_cb(GtkWidget *w,gint resp,struct log_viewer_hash_t *ht)
{
  PidginLogViewer *lv = syslog_viewer;
#ifdef _WIN32
#endif
  if (ht != ((struct log_viewer_hash_t *)((void *)0))) {
    lv = (g_hash_table_lookup(log_viewers,ht));
    g_hash_table_remove(log_viewers,ht);
    g_free((ht -> buddyname));
    g_free(ht);
  }
  else 
    syslog_viewer = ((PidginLogViewer *)((void *)0));
  purple_request_close_with_handle(lv);
  g_list_foreach((lv -> logs),((GFunc )purple_log_free),0);
  g_list_free((lv -> logs));
  g_free((lv -> search));
  g_free(lv);
  gtk_widget_destroy(w);
}

static void log_row_activated_cb(GtkTreeView *tv,GtkTreePath *path,GtkTreeViewColumn *col,PidginLogViewer *viewer)
{
  if (gtk_tree_view_row_expanded(tv,path) != 0) 
    gtk_tree_view_collapse_row(tv,path);
  else 
    gtk_tree_view_expand_row(tv,path,0);
}

static void delete_log_cleanup_cb(gpointer *data)
{
/* iter */
  g_free(data[1]);
  g_free(data);
}

static void delete_log_cb(gpointer *data)
{
  if (!(purple_log_delete(((PurpleLog *)data[2])) != 0)) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Log Deletion Failed"))),((const char *)(dgettext("pidgin","Check permissions and try again."))),0,0);
  }
  else {
    GtkTreeStore *treestore = data[0];
    GtkTreeIter *iter = (GtkTreeIter *)data[1];
    GtkTreePath *path = gtk_tree_model_get_path(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)treestore),gtk_tree_model_get_type()))),iter);
    gboolean first = !(gtk_tree_path_prev(path) != 0);
    if (!(gtk_tree_store_remove(treestore,iter) != 0) && (first != 0)) {
/* iter was the last child at its level */
      if (gtk_tree_path_up(path) != 0) {
        gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)treestore),gtk_tree_model_get_type()))),iter,path);
        gtk_tree_store_remove(treestore,iter);
      }
    }
    gtk_tree_path_free(path);
  }
  delete_log_cleanup_cb(data);
}

static void log_delete_log_cb(GtkWidget *menuitem,gpointer *data)
{
  PidginLogViewer *lv = data[0];
  PurpleLog *log = data[1];
  const char *time = log_get_date(log);
  const char *name;
  char *tmp;
  gpointer *data2;
  if ((log -> type) == PURPLE_LOG_IM) {
    PurpleBuddy *buddy = purple_find_buddy((log -> account),(log -> name));
    if (buddy != ((PurpleBuddy *)((void *)0))) 
      name = purple_buddy_get_contact_alias(buddy);
    else 
      name = (log -> name);
    tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Are you sure you want to permanently delete the log of the conversation with %s which started at %s\?"))),name,time);
  }
  else if ((log -> type) == PURPLE_LOG_CHAT) {
    PurpleChat *chat = purple_blist_find_chat((log -> account),(log -> name));
    if (chat != ((PurpleChat *)((void *)0))) 
      name = purple_chat_get_name(chat);
    else 
      name = (log -> name);
    tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Are you sure you want to permanently delete the log of the conversation in %s which started at %s\?"))),name,time);
  }
  else if ((log -> type) == PURPLE_LOG_SYSTEM) {
    tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Are you sure you want to permanently delete the system log which started at %s\?"))),time);
  }
  else 
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","gtklog.c",309,((const char *)__func__));
      return ;
    }while (0);
/* The only way to free data in all cases is to tie it to the menuitem with
	 * g_object_set_data_full().  But, since we need to get some data down to
	 * delete_log_cb() to delete the log from the log viewer after the file is
	 * deleted, we have to allocate a new data array and make sure it gets freed
	 * either way. */
  data2 = ((gpointer *)(g_malloc_n(3,(sizeof(gpointer )))));
  data2[0] = (lv -> treestore);
/* iter */
  data2[1] = data[3];
  data2[2] = log;
  purple_request_action(lv,0,((const char *)(dgettext("pidgin","Delete Log\?"))),tmp,0,0,0,0,data2,2,((const char *)(dgettext("pidgin","Delete"))),delete_log_cb,((const char *)(dgettext("pidgin","Cancel"))),delete_log_cleanup_cb);
  g_free(tmp);
}

static void log_show_popup_menu(GtkWidget *treeview,GdkEventButton *event,gpointer *data)
{
  GtkWidget *menu = gtk_menu_new();
  GtkWidget *menuitem = gtk_menu_item_new_with_label(((const char *)(dgettext("pidgin","Delete Log..."))));
  if (!(purple_log_is_deletable(((PurpleLog *)data[1])) != 0)) 
    gtk_widget_set_sensitive(menuitem,0);
  g_signal_connect_data(menuitem,"activate",((GCallback )log_delete_log_cb),data,0,((GConnectFlags )0));
  g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"log-viewer-data",data,g_free);
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem);
  gtk_widget_show_all(menu);
  gtk_menu_popup(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_get_type()))),0,0,((GtkMenuPositionFunc )data[2]),0,((event != ((GdkEventButton *)((void *)0)))?(event -> button) : 0),gdk_event_get_time(((GdkEvent *)event)));
}

static gboolean log_button_press_cb(GtkWidget *treeview,GdkEventButton *event,PidginLogViewer *lv)
{
  if (((event -> type) == GDK_BUTTON_PRESS) && ((event -> button) == 3)) {
    GtkTreePath *path;
    GtkTreeIter *iter;
    GValue val;
    PurpleLog *log;
    gpointer *data;
    if (!(gtk_tree_view_get_path_at_pos(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),(event -> x),(event -> y),&path,0,0,0) != 0)) 
      return 0;
    iter = ((GtkTreeIter *)(g_malloc_n(1,(sizeof(GtkTreeIter )))));
    gtk_tree_model_get_iter(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> treestore)),gtk_tree_model_get_type()))),iter,path);
    val.g_type = 0;
    gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> treestore)),gtk_tree_model_get_type()))),iter,1,&val);
    gtk_tree_path_free(path);
    log = (g_value_get_pointer((&val)));
    if (log == ((PurpleLog *)((void *)0))) {
      g_free(iter);
      return 0;
    }
    data = ((gpointer *)(g_malloc_n(4,(sizeof(gpointer )))));
    data[0] = lv;
    data[1] = log;
    data[2] = ((gpointer )((void *)0));
    data[3] = iter;
    log_show_popup_menu(treeview,event,data);
    return (!0);
  }
  return 0;
}

static gboolean log_popup_menu_cb(GtkWidget *treeview,PidginLogViewer *lv)
{
  GtkTreeSelection *sel;
  GtkTreeIter *iter;
  GValue val;
  PurpleLog *log;
  gpointer *data;
  iter = ((GtkTreeIter *)(g_malloc_n(1,(sizeof(GtkTreeIter )))));
  sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> treeview)),gtk_tree_view_get_type()))));
  if (!(gtk_tree_selection_get_selected(sel,0,iter) != 0)) {
    return 0;
  }
  val.g_type = 0;
  gtk_tree_model_get_value(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> treestore)),gtk_tree_model_get_type()))),iter,NODE_COLUMN,&val);
  log = (g_value_get_pointer((&val)));
  if (log == ((PurpleLog *)((void *)0))) 
    return 0;
  data = ((gpointer *)(g_malloc_n(4,(sizeof(gpointer )))));
  data[0] = lv;
  data[1] = log;
  data[2] = pidgin_treeview_popup_menu_position_func;
  data[3] = iter;
  log_show_popup_menu(treeview,0,data);
  return (!0);
}

static gboolean search_find_cb(gpointer data)
{
  PidginLogViewer *viewer = data;
  gtk_imhtml_search_find(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(viewer -> imhtml)),gtk_imhtml_get_type()))),(viewer -> search));
  g_object_steal_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(viewer -> entry)),((GType )(20 << 2))))),"search-find-cb");
  return 0;
}

static void log_select_cb(GtkTreeSelection *sel,PidginLogViewer *viewer)
{
  GtkTreeIter iter;
  GValue val;
  GtkTreeModel *model = (GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(viewer -> treestore)),gtk_tree_model_get_type()));
  PurpleLog *log = (PurpleLog *)((void *)0);
  PurpleLogReadFlags flags;
  char *read = (char *)((void *)0);
  if (!(gtk_tree_selection_get_selected(sel,&model,&iter) != 0)) 
    return ;
  val.g_type = 0;
  gtk_tree_model_get_value(model,&iter,1,&val);
  log = (g_value_get_pointer((&val)));
  g_value_unset(&val);
  if (log == ((PurpleLog *)((void *)0))) 
    return ;
  pidgin_set_cursor((viewer -> window),GDK_WATCH);
  if ((log -> type) != PURPLE_LOG_SYSTEM) {
    char *title;
    if ((log -> type) == PURPLE_LOG_CHAT) 
      title = g_strdup_printf(((const char *)(dgettext("pidgin","<span size=\'larger\' weight=\'bold\'>Conversation in %s on %s</span>"))),(log -> name),log_get_date(log));
    else 
      title = g_strdup_printf(((const char *)(dgettext("pidgin","<span size=\'larger\' weight=\'bold\'>Conversation with %s on %s</span>"))),(log -> name),log_get_date(log));
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(viewer -> label)),gtk_label_get_type()))),title);
    g_free(title);
  }
  read = purple_log_read(log,&flags);
  viewer -> flags = flags;
  gtk_imhtml_delete(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(viewer -> imhtml)),gtk_imhtml_get_type()))),0,0);
  gtk_imhtml_set_protocol_name(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(viewer -> imhtml)),gtk_imhtml_get_type()))),purple_account_get_protocol_name((log -> account)));
  purple_signal_emit(pidgin_log_get_handle(),"log-displaying",viewer,log);
  gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(viewer -> imhtml)),gtk_imhtml_get_type()))),read,(GTK_IMHTML_NO_COMMENTS | GTK_IMHTML_NO_TITLE | GTK_IMHTML_NO_SCROLL | ((((flags & PURPLE_LOG_READ_NO_NEWLINE) != 0U)?GTK_IMHTML_NO_NEWLINE : 0))),0);
  g_free(read);
  if ((viewer -> search) != ((char *)((void *)0))) {
    guint source;
    gtk_imhtml_search_clear(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(viewer -> imhtml)),gtk_imhtml_get_type()))));
    source = g_idle_add(search_find_cb,viewer);
    g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(viewer -> entry)),((GType )(20 << 2))))),"search-find-cb",((gpointer )((glong )source)),((GDestroyNotify )g_source_remove));
  }
  pidgin_clear_cursor((viewer -> window));
}
/* I want to make this smarter, but haven't come up with a cool algorithm to do so, yet.
 * I want the tree to be divided into groups like "Today," "Yesterday," "Last week,"
 * "August," "2002," etc. based on how many conversation took place in each subdivision.
 *
 * For now, I'll just make it a flat list.
 */

static void populate_log_tree(PidginLogViewer *lv)
/* Logs are made from trees in real life.
        This is a tree made from logs */
{
  const char *month;
  char prev_top_month[30UL] = "";
  GtkTreeIter toplevel;
  GtkTreeIter child;
  GList *logs = (lv -> logs);
  while(logs != ((GList *)((void *)0))){
    PurpleLog *log = (logs -> data);
    month = purple_utf8_strftime(((const char *)(dgettext("pidgin","%B %Y"))),((((log -> tm) != 0)?(log -> tm) : localtime((&log -> time)))));
    if (strcmp(month,prev_top_month) != 0) {
/* top level */
      gtk_tree_store_append((lv -> treestore),&toplevel,0);
      gtk_tree_store_set((lv -> treestore),&toplevel,0,month,1,((void *)((void *)0)),-1);
      g_strlcpy(prev_top_month,month,(sizeof(prev_top_month)));
    }
/* sub */
    gtk_tree_store_append((lv -> treestore),&child,&toplevel);
    gtk_tree_store_set((lv -> treestore),&child,0,log_get_date(log),1,log,-1);
    logs = (logs -> next);
  }
}

static PidginLogViewer *display_log_viewer(struct log_viewer_hash_t *ht,GList *logs,const char *title,GtkWidget *icon,int log_size)
{
  PidginLogViewer *lv;
  GtkWidget *title_box;
  char *text;
  GtkWidget *pane;
  GtkCellRenderer *rend;
  GtkTreeViewColumn *col;
  GtkTreeSelection *sel;
  GtkWidget *vbox;
  GtkWidget *frame;
  GtkWidget *hbox;
  GtkWidget *find_button;
  GtkWidget *size_label;
  if (logs == ((GList *)((void *)0))) {
/* No logs were found. */
    const char *log_preferences = (const char *)((void *)0);
    if (ht == ((struct log_viewer_hash_t *)((void *)0))) {
      if (!(purple_prefs_get_bool("/purple/logging/log_system") != 0)) 
        log_preferences = ((const char *)(dgettext("pidgin","System events will only be logged if the \"Log all status changes to system log\" preference is enabled.")));
    }
    else {
      if ((ht -> type) == PURPLE_LOG_IM) {
        if (!(purple_prefs_get_bool("/purple/logging/log_ims") != 0)) 
          log_preferences = ((const char *)(dgettext("pidgin","Instant messages will only be logged if the \"Log all instant messages\" preference is enabled.")));
      }
      else if ((ht -> type) == PURPLE_LOG_CHAT) {
        if (!(purple_prefs_get_bool("/purple/logging/log_chats") != 0)) 
          log_preferences = ((const char *)(dgettext("pidgin","Chats will only be logged if the \"Log all chats\" preference is enabled.")));
      }
      g_free((ht -> buddyname));
      g_free(ht);
    }
    if (icon != ((GtkWidget *)((void *)0))) 
      gtk_widget_destroy(icon);
    purple_notify_message(0,PURPLE_NOTIFY_MSG_INFO,title,((const char *)(dgettext("pidgin","No logs were found"))),log_preferences,0,0);
    return 0;
  }
  lv = ((PidginLogViewer *)(g_malloc0_n(1,(sizeof(PidginLogViewer )))));
  lv -> logs = logs;
  if (ht != ((struct log_viewer_hash_t *)((void *)0))) 
    g_hash_table_insert(log_viewers,ht,lv);
/* Window ***********/
  lv -> window = gtk_dialog_new_with_buttons(title,0,0,"gtk-close",GTK_RESPONSE_CLOSE,((void *)((void *)0)));
#ifdef _WIN32
/* Steal the "HELP" response and use it to trigger browsing to the logs folder */
#endif
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> window)),gtk_container_get_type()))),6);
  gtk_dialog_set_has_separator(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> window)),gtk_dialog_get_type()))),0);
  gtk_box_set_spacing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> window)),gtk_dialog_get_type())))).vbox),gtk_box_get_type()))),0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> window)),((GType )(20 << 2))))),"response",((GCallback )destroy_cb),ht,0,((GConnectFlags )0));
  gtk_window_set_role(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> window)),gtk_window_get_type()))),"log_viewer");
/* Icon *************/
  if (icon != ((GtkWidget *)((void *)0))) {
    title_box = gtk_hbox_new(0,6);
    gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)title_box),gtk_container_get_type()))),6);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> window)),gtk_dialog_get_type())))).vbox),gtk_box_get_type()))),title_box,0,0,0);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)title_box),gtk_box_get_type()))),icon,0,0,0);
  }
  else 
    title_box = ( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> window)),gtk_dialog_get_type())))).vbox;
/* Label ************/
  lv -> label = gtk_label_new(0);
  text = g_strdup_printf("<span size=\'larger\' weight=\'bold\'>%s</span>",title);
  gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> label)),gtk_label_get_type()))),text);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> label)),gtk_misc_get_type()))),0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)title_box),gtk_box_get_type()))),(lv -> label),0,0,0);
  g_free(text);
/* Pane *************/
  pane = gtk_hpaned_new();
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)pane),gtk_container_get_type()))),6);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> window)),gtk_dialog_get_type())))).vbox),gtk_box_get_type()))),pane,(!0),(!0),0);
/* List *************/
  lv -> treestore = gtk_tree_store_new(2,((GType )(16 << 2)),((GType )(17 << 2)));
  lv -> treeview = gtk_tree_view_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> treestore)),gtk_tree_model_get_type()))));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> treestore)),((GType )(20 << 2))))));
  rend = gtk_cell_renderer_text_new();
  col = gtk_tree_view_column_new_with_attributes("time",rend,"markup",0,((void *)((void *)0)));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> treeview)),gtk_tree_view_get_type()))),col);
  gtk_tree_view_set_headers_visible(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> treeview)),gtk_tree_view_get_type()))),0);
  gtk_paned_add1(((GtkPaned *)(g_type_check_instance_cast(((GTypeInstance *)pane),gtk_paned_get_type()))),pidgin_make_scrollable((lv -> treeview),GTK_POLICY_NEVER,GTK_POLICY_ALWAYS,GTK_SHADOW_IN,-1,-1));
  populate_log_tree(lv);
  sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> treeview)),gtk_tree_view_get_type()))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)sel),((GType )(20 << 2))))),"changed",((GCallback )log_select_cb),lv,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> treeview)),((GType )(20 << 2))))),"row-activated",((GCallback )log_row_activated_cb),lv,0,((GConnectFlags )0));
  pidgin_set_accessible_label((lv -> treeview),(lv -> label));
  g_signal_connect_data((lv -> treeview),"button-press-event",((GCallback )log_button_press_cb),lv,0,((GConnectFlags )0));
  g_signal_connect_data((lv -> treeview),"popup-menu",((GCallback )log_popup_menu_cb),lv,0,((GConnectFlags )0));
/* Log size ************/
  if (log_size != 0) {
    char *sz_txt = purple_str_size_to_units(log_size);
    text = g_strdup_printf("<span weight=\'bold\'>%s</span> %s",((const char *)(dgettext("pidgin","Total log size:"))),sz_txt);
    size_label = gtk_label_new(0);
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)size_label),gtk_label_get_type()))),text);
/*		gtk_paned_add1(GTK_PANED(pane), size_label); */
    gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)size_label),gtk_misc_get_type()))),0,0);
    gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> window)),gtk_dialog_get_type())))).vbox),gtk_box_get_type()))),size_label,0,0,0);
    g_free(sz_txt);
    g_free(text);
  }
/* A fancy little box ************/
  vbox = gtk_vbox_new(0,6);
  gtk_paned_add2(((GtkPaned *)(g_type_check_instance_cast(((GTypeInstance *)pane),gtk_paned_get_type()))),vbox);
/* Viewer ************/
  frame = pidgin_create_imhtml(0,&lv -> imhtml,0,0);
  gtk_widget_set_name((lv -> imhtml),"pidgin_log_imhtml");
  gtk_widget_set_size_request((lv -> imhtml),320,200);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),frame,(!0),(!0),0);
  gtk_widget_show(frame);
/* Search box **********/
  hbox = gtk_hbox_new(0,6);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  lv -> entry = gtk_entry_new();
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),(lv -> entry),(!0),(!0),0);
  find_button = gtk_button_new_from_stock("gtk-find");
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),find_button,0,0,0);
  g_signal_connect_data(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> entry)),gtk_entry_get_type()))),"activate",((GCallback )search_cb),lv,0,((GConnectFlags )0));
  g_signal_connect_data(((GtkButton *)(g_type_check_instance_cast(((GTypeInstance *)find_button),gtk_button_get_type()))),"clicked",((GCallback )search_cb),lv,0,((GConnectFlags )0));
  select_first_log(lv);
  gtk_widget_show_all((lv -> window));
  return lv;
}

void pidgin_log_show(PurpleLogType type,const char *buddyname,PurpleAccount *account)
{
  struct log_viewer_hash_t *ht;
  PidginLogViewer *lv = (PidginLogViewer *)((void *)0);
  const char *name = buddyname;
  char *title;
  GdkPixbuf *prpl_icon;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (buddyname != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddyname != NULL");
      return ;
    };
  }while (0);
  ht = ((struct log_viewer_hash_t *)(g_malloc0_n(1,(sizeof(struct log_viewer_hash_t )))));
  ht -> type = type;
  ht -> buddyname = g_strdup(buddyname);
  ht -> account = account;
  if (log_viewers == ((GHashTable *)((void *)0))) {
    log_viewers = g_hash_table_new(log_viewer_hash,log_viewer_equal);
  }
  else if ((lv = (g_hash_table_lookup(log_viewers,ht))) != 0) {
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> window)),gtk_window_get_type()))));
    g_free((ht -> buddyname));
    g_free(ht);
    return ;
  }
  if (type == PURPLE_LOG_CHAT) {
    PurpleChat *chat;
    chat = purple_blist_find_chat(account,buddyname);
    if (chat != ((PurpleChat *)((void *)0))) 
      name = purple_chat_get_name(chat);
    title = g_strdup_printf(((const char *)(dgettext("pidgin","Conversations in %s"))),name);
  }
  else {
    PurpleBuddy *buddy;
    buddy = purple_find_buddy(account,buddyname);
    if (buddy != ((PurpleBuddy *)((void *)0))) 
      name = purple_buddy_get_contact_alias(buddy);
    title = g_strdup_printf(((const char *)(dgettext("pidgin","Conversations with %s"))),name);
  }
  prpl_icon = pidgin_create_prpl_icon(account,PIDGIN_PRPL_ICON_MEDIUM);
  display_log_viewer(ht,purple_log_get_logs(type,buddyname,account),title,gtk_image_new_from_pixbuf(prpl_icon),purple_log_get_total_size(type,buddyname,account));
  if (prpl_icon != 0) 
    g_object_unref(prpl_icon);
  g_free(title);
}

void pidgin_log_show_contact(PurpleContact *contact)
{
  struct log_viewer_hash_t *ht;
  PurpleBlistNode *child;
  PidginLogViewer *lv = (PidginLogViewer *)((void *)0);
  GList *logs = (GList *)((void *)0);
  GdkPixbuf *pixbuf;
  GtkWidget *image;
  const char *name = (const char *)((void *)0);
  char *title;
  int total_log_size = 0;
  do {
    if (contact != ((PurpleContact *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"contact != NULL");
      return ;
    };
  }while (0);
  ht = ((struct log_viewer_hash_t *)(g_malloc0_n(1,(sizeof(struct log_viewer_hash_t )))));
  ht -> type = PURPLE_LOG_IM;
  ht -> contact = contact;
  if (log_viewers == ((GHashTable *)((void *)0))) {
    log_viewers = g_hash_table_new(log_viewer_hash,log_viewer_equal);
  }
  else if ((lv = (g_hash_table_lookup(log_viewers,ht))) != 0) {
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(lv -> window)),gtk_window_get_type()))));
    g_free(ht);
    return ;
  }
  for (child = purple_blist_node_get_first_child(((PurpleBlistNode *)contact)); child != ((PurpleBlistNode *)((void *)0)); child = purple_blist_node_get_sibling_next(child)) {{
      const char *buddy_name;
      PurpleAccount *account;
      if (!((purple_blist_node_get_type(child)) == PURPLE_BLIST_BUDDY_NODE)) 
        continue; 
      buddy_name = purple_buddy_get_name(((PurpleBuddy *)child));
      account = purple_buddy_get_account(((PurpleBuddy *)child));
      logs = g_list_concat(purple_log_get_logs(PURPLE_LOG_IM,buddy_name,account),logs);
      total_log_size += purple_log_get_total_size(PURPLE_LOG_IM,buddy_name,account);
    }
  }
  logs = g_list_sort(logs,purple_log_compare);
  image = gtk_image_new();
  pixbuf = gtk_widget_render_icon(image,"pidgin-status-person",gtk_icon_size_from_name("pidgin-icon-size-tango-small"),"GtkWindow");
  if (pixbuf != 0) {
    gtk_image_set_from_pixbuf(((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)image),gtk_image_get_type()))),pixbuf);
    g_object_unref(pixbuf);
  }
  else {
    gtk_widget_destroy(image);
    image = ((GtkWidget *)((void *)0));
  }
  if ((contact -> alias) != ((char *)((void *)0))) 
    name = (contact -> alias);
  else if ((contact -> priority) != ((PurpleBuddy *)((void *)0))) 
    name = purple_buddy_get_contact_alias((contact -> priority));
/* This will happen if the contact doesn't have an alias,
	 * and none of the contact's buddies are online.
	 * There is probably a better way to deal with this. */
  if (name == ((const char *)((void *)0))) {
    if ((contact -> node.child != ((PurpleBlistNode *)((void *)0))) && ((purple_blist_node_get_type(contact -> node.child)) == PURPLE_BLIST_BUDDY_NODE)) 
      name = purple_buddy_get_contact_alias(((PurpleBuddy *)contact -> node.child));
    if (name == ((const char *)((void *)0))) 
      name = "";
  }
  title = g_strdup_printf(((const char *)(dgettext("pidgin","Conversations with %s"))),name);
  display_log_viewer(ht,logs,title,image,total_log_size);
  g_free(title);
}

void pidgin_syslog_show()
{
  GList *accounts = (GList *)((void *)0);
  GList *logs = (GList *)((void *)0);
  if (syslog_viewer != ((PidginLogViewer *)((void *)0))) {
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(syslog_viewer -> window)),gtk_window_get_type()))));
    return ;
  }
  for (accounts = purple_accounts_get_all(); accounts != ((GList *)((void *)0)); accounts = (accounts -> next)) {{
      PurpleAccount *account = (PurpleAccount *)(accounts -> data);
      if (purple_find_prpl(purple_account_get_protocol_id(account)) == ((PurplePlugin *)((void *)0))) 
        continue; 
      logs = g_list_concat(purple_log_get_system_logs(account),logs);
    }
  }
  logs = g_list_sort(logs,purple_log_compare);
  syslog_viewer = display_log_viewer(0,logs,((const char *)(dgettext("pidgin","System Log"))),0,0);
}
/****************************************************************************
 * GTK+ LOG SUBSYSTEM *******************************************************
 ****************************************************************************/

void *pidgin_log_get_handle()
{
  static int handle;
  return (&handle);
}

void pidgin_log_init()
{
  void *handle = pidgin_log_get_handle();
  purple_signal_register(handle,"log-displaying",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_BOXED,"PidginLogViewer *"),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_LOG));
}

void pidgin_log_uninit()
{
  purple_signals_unregister_by_instance(pidgin_log_get_handle());
}
