/*
 * @file gtkcellrendererexpander.c GTK+ Cell Renderer Expander
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
/* This is taken largely from GtkCellRenderer[Text|Pixbuf|Toggle] by
 * Jonathon Blandford <jrb@redhat.com> for RedHat, Inc.
 */
#include <gtk/gtk.h>
#include "gtkcellrendererexpander.h"
static void pidgin_cell_renderer_expander_get_property(GObject *object,guint param_id,GValue *value,GParamSpec *pspec);
static void pidgin_cell_renderer_expander_set_property(GObject *object,guint param_id,const GValue *value,GParamSpec *pspec);
static void pidgin_cell_renderer_expander_init(PidginCellRendererExpander *cellexpander);
static void pidgin_cell_renderer_expander_class_init(PidginCellRendererExpanderClass *class);
static void pidgin_cell_renderer_expander_get_size(GtkCellRenderer *cell,GtkWidget *widget,GdkRectangle *cell_area,gint *x_offset,gint *y_offset,gint *width,gint *height);
static void pidgin_cell_renderer_expander_render(GtkCellRenderer *cell,GdkWindow *window,GtkWidget *widget,GdkRectangle *background_area,GdkRectangle *cell_area,GdkRectangle *expose_area,guint flags);
static gboolean pidgin_cell_renderer_expander_activate(GtkCellRenderer *r,GdkEvent *event,GtkWidget *widget,const gchar *p,GdkRectangle *bg,GdkRectangle *cell,GtkCellRendererState flags);
static void pidgin_cell_renderer_expander_finalize(GObject *gobject);
enum __unnamed_enum___F0_L68_C1_LAST_SIGNAL {LAST_SIGNAL};
enum __unnamed_enum___F0_L72_C1_PROP_0__COMMA__PROP_IS_EXPANDER {PROP_0,PROP_IS_EXPANDER};
static gpointer parent_class;
/* static guint expander_cell_renderer_signals [LAST_SIGNAL]; */

GType pidgin_cell_renderer_expander_get_type()
{
  static GType cell_expander_type = 0;
  if (!(cell_expander_type != 0UL)) {
    static const GTypeInfo cell_expander_info = {((sizeof(PidginCellRendererExpanderClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )pidgin_cell_renderer_expander_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(PidginCellRendererExpander ))), (0), ((GInstanceInitFunc )pidgin_cell_renderer_expander_init), ((const GTypeValueTable *)((void *)0))
/* base_init */
/* base_finalize */
/* class_finalize */
/* class_data */
/* n_preallocs */
/* value_table */
};
    cell_expander_type = g_type_register_static(gtk_cell_renderer_get_type(),"PidginCellRendererExpander",&cell_expander_info,0);
  }
  return cell_expander_type;
}

static void pidgin_cell_renderer_expander_init(PidginCellRendererExpander *cellexpander)
{
  ( *((GtkCellRenderer *)(g_type_check_instance_cast(((GTypeInstance *)cellexpander),gtk_cell_renderer_get_type())))).mode = GTK_CELL_RENDERER_MODE_ACTIVATABLE;
  ( *((GtkCellRenderer *)(g_type_check_instance_cast(((GTypeInstance *)cellexpander),gtk_cell_renderer_get_type())))).xpad = 0;
  ( *((GtkCellRenderer *)(g_type_check_instance_cast(((GTypeInstance *)cellexpander),gtk_cell_renderer_get_type())))).ypad = 2;
}

static void pidgin_cell_renderer_expander_class_init(PidginCellRendererExpanderClass *class)
{
  GObjectClass *object_class = (GObjectClass *)(g_type_check_class_cast(((GTypeClass *)class),((GType )(20 << 2))));
  GtkCellRendererClass *cell_class = (GtkCellRendererClass *)(g_type_check_class_cast(((GTypeClass *)class),gtk_cell_renderer_get_type()));
  parent_class = g_type_class_peek_parent(class);
  object_class -> finalize = pidgin_cell_renderer_expander_finalize;
  object_class -> get_property = pidgin_cell_renderer_expander_get_property;
  object_class -> set_property = pidgin_cell_renderer_expander_set_property;
  cell_class -> get_size = pidgin_cell_renderer_expander_get_size;
  cell_class -> render = pidgin_cell_renderer_expander_render;
  cell_class -> activate = pidgin_cell_renderer_expander_activate;
  g_object_class_install_property(object_class,PROP_IS_EXPANDER,g_param_spec_boolean("expander-visible","Is Expander","True if the renderer should draw an expander",0,(G_PARAM_READABLE | G_PARAM_WRITABLE)));
}

static void pidgin_cell_renderer_expander_finalize(GObject *object)
{
/*
	PidginCellRendererExpander *cellexpander = PIDGIN_CELL_RENDERER_EXPANDER(object);
*/
  ( *( *((GObjectClass *)(g_type_check_class_cast(((GTypeClass *)parent_class),((GType )(20 << 2)))))).finalize)(object);
}

static void pidgin_cell_renderer_expander_get_property(GObject *object,guint param_id,GValue *value,GParamSpec *psec)
{
  PidginCellRendererExpander *cellexpander = (PidginCellRendererExpander *)(g_type_check_instance_cast(((GTypeInstance *)object),pidgin_cell_renderer_expander_get_type()));
  switch(param_id){
    case PROP_IS_EXPANDER:
{
      g_value_set_boolean(value,(cellexpander -> is_expander));
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)psec;
        guint _glib__property_id = param_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","gtkcellrendererexpander.c:162","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void pidgin_cell_renderer_expander_set_property(GObject *object,guint param_id,const GValue *value,GParamSpec *pspec)
{
  PidginCellRendererExpander *cellexpander = (PidginCellRendererExpander *)(g_type_check_instance_cast(((GTypeInstance *)object),pidgin_cell_renderer_expander_get_type()));
  switch(param_id){
    case PROP_IS_EXPANDER:
{
      cellexpander -> is_expander = g_value_get_boolean(value);
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = param_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","gtkcellrendererexpander.c:181","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

GtkCellRenderer *pidgin_cell_renderer_expander_new()
{
  return (g_object_new(pidgin_cell_renderer_expander_get_type(),0));
}

static void pidgin_cell_renderer_expander_get_size(GtkCellRenderer *cell,GtkWidget *widget,GdkRectangle *cell_area,gint *x_offset,gint *y_offset,gint *width,gint *height)
{
  gint calc_width;
  gint calc_height;
  gint expander_size;
  gtk_widget_style_get(widget,"expander-size",&expander_size,((void *)((void *)0)));
  calc_width = ((((gint )(cell -> xpad)) * 2) + expander_size);
  calc_height = ((((gint )(cell -> ypad)) * 2) + expander_size);
  if (width != 0) 
     *width = calc_width;
  if (height != 0) 
     *height = calc_height;
  if (cell_area != 0) {
    if (x_offset != 0) {
       *x_offset = ((cell -> xalign) * ((cell_area -> width) - calc_width));
       *x_offset = (( *x_offset > 0)? *x_offset : 0);
    }
    if (y_offset != 0) {
       *y_offset = ((cell -> yalign) * ((cell_area -> height) - calc_height));
       *y_offset = (( *y_offset > 0)? *y_offset : 0);
    }
  }
}

static void pidgin_cell_renderer_expander_render(GtkCellRenderer *cell,GdkWindow *window,GtkWidget *widget,GdkRectangle *background_area,GdkRectangle *cell_area,GdkRectangle *expose_area,guint flags)
{
  PidginCellRendererExpander *cellexpander = (PidginCellRendererExpander *)cell;
  gboolean set;
  gint width;
  gint height;
  GtkStateType state;
  if (!((cellexpander -> is_expander) != 0)) 
    return ;
  width = (cell_area -> width);
  height = (cell_area -> height);
  if (!((cell -> sensitive) != 0)) 
    state = GTK_STATE_INSENSITIVE;
  else if ((flags & GTK_CELL_RENDERER_PRELIT) != 0U) 
    state = GTK_STATE_PRELIGHT;
  else if (((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_object_get_type())))).flags & GTK_HAS_FOCUS) != 0) && ((flags & GTK_CELL_RENDERER_SELECTED) != 0U)) 
    state = GTK_STATE_ACTIVE;
  else 
    state = GTK_STATE_NORMAL;
  width -= ((cell -> xpad) * 2);
  height -= ((cell -> ypad) * 2);
  gtk_paint_expander((widget -> style),window,state,0,widget,"treeview",(((cell_area -> x) + (cell -> xpad)) + (width / 2)),(((cell_area -> y) + (cell -> ypad)) + (height / 2)),(((cell -> is_expanded) != 0)?GTK_EXPANDER_EXPANDED : GTK_EXPANDER_COLLAPSED));
/* only draw the line if the color isn't set - this prevents a bug where the hline appears only under the expander */
  g_object_get(cellexpander,"cell-background-set",&set,((void *)((void *)0)));
  if (((cell -> is_expanded) != 0) && !(set != 0)) 
    gtk_paint_hline((widget -> style),window,state,0,widget,0,0,widget -> allocation.width,((cell_area -> y) + (cell_area -> height)));
}

static gboolean pidgin_cell_renderer_expander_activate(GtkCellRenderer *r,GdkEvent *event,GtkWidget *widget,const gchar *p,GdkRectangle *bg,GdkRectangle *cell,GtkCellRendererState flags)
{
  GtkTreePath *path = gtk_tree_path_new_from_string(p);
  if (gtk_tree_view_row_expanded(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_tree_view_get_type()))),path) != 0) 
    gtk_tree_view_collapse_row(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_tree_view_get_type()))),path);
  else 
    gtk_tree_view_expand_row(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_tree_view_get_type()))),path,0);
  gtk_tree_path_free(path);
  return 0;
}
