/*
 * @file gtkdialogs.c GTK+ Dialogs
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#define _PIDGIN_GTKDIALOGS_C_
#include "internal.h"
#include "pidgin.h"
#include "package_revision.h"
#include "debug.h"
#include "notify.h"
#include "plugin.h"
#include "prpl.h"
#include "request.h"
#include "util.h"
#include "core.h"
#include "gtkblist.h"
#include "gtkdialogs.h"
#include "gtkimhtml.h"
#include "gtkimhtmltoolbar.h"
#include "gtklog.h"
#include "gtkutils.h"
#include "pidginstock.h"
static GList *dialogwindows = (GList *)((void *)0);

struct _PidginGroupMergeObject 
{
  PurpleGroup *parent;
  char *new_name;
}
;

struct developer 
{
  char *name;
  char *role;
  char *email;
}
;

struct translator 
{
  char *language;
  char *abbr;
  char *name;
  char *email;
}
;

struct artist 
{
  char *name;
  char *email;
}
;
/* Order: Alphabetical by Last Name */
static const struct developer developers[] = {{("Daniel \'datallah\' Atallah"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Paul \'darkrain42\' Aurich"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Ethan \'Paco-Paco\' Blanton"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Hylke Bons"), ("artist"), ("hylkebons@gmail.com")}, {("Sadrul Habib Chowdhury"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Mark \'KingAnt\' Doliner"), ((char *)((void *)0)), ("mark@kingant.net")}, {("Gary \'grim\' Kramlich"), ((char *)((void *)0)), ("grim@pidgin.im")}, {("Richard \'rlaager\' Laager"), ((char *)((void *)0)), ("rlaager@pidgin.im")}, {("Marcus \'malu\' Lundblad"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Sulabh \'sulabh_m\' Mahajan"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Richard \'wabz\' Nelson"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Etan \'deryni\' Reisner"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Michael \'Maiku\' Ruprecht"), ("voice and video"), ((char *)((void *)0))}, {("Elliott \'QuLogic\' Sales de Andrade"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Luke \'LSchiere\' Schierer"), ("support"), ("lschiere@users.sf.net")}, {("Evan Schoenberg"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Kevin \'SimGuy\' Stange"), ("webmaster"), ((char *)((void *)0))}, {("Will \'resiak\' Thompson"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Stu \'nosnilmot\' Tomlinson"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Jorge \'Masca\' Villase\303\261or"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Tomasz Wasilczyk"), ((char *)((void *)0)), ("https://www.wasilczyk.pl")}, {((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0))}};
/* Order: Alphabetical by Last Name */
static const struct developer patch_writers[] = {{("Jakub \'haakon\' Adam"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Krzysztof Klinikowski"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Eion Robb"), ((char *)((void *)0)), ((char *)((void *)0))}, {((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0))}};
/* Order: Alphabetical by Last Name */
static const struct developer retired_developers[] = {{("John \'rekkanoryo\' Bailey"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Herman Bloggs"), ("win32 port"), ("herman@bluedigits.com")}, {("Thomas Butter"), ((char *)((void *)0)), ((char *)((void *)0))}, 
/* Translators: This is a person's name. For most languages we recommend
	   not translating it. */
{("Ka-Hing Cheung"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Jim Duchek"), ("maintainer"), ("jim@linuxpimps.com")}, {("Sean Egan"), ((char *)((void *)0)), ("sean.egan@gmail.com")}, {("Rob Flynn"), ("maintainer"), ((char *)((void *)0))}, {("Adam Fritzler"), ("libfaim maintainer"), ((char *)((void *)0))}, {("Christian \'ChipX86\' Hammond"), ("webmaster"), ((char *)((void *)0))}, {("Casey Harkins"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Ivan Komarov"), ((char *)((void *)0)), ("ivan.komarov@pidgin.im")}, 
/* If "lazy bum" translates literally into a serious insult, use something else or omit it. */
{("Syd Logan"), ("hacker and designated driver [lazy bum]"), ((char *)((void *)0))}, {("Christopher \'siege\' O\'Brien"), ((char *)((void *)0)), ("taliesein@users.sf.net")}, {("Bartosz Oler"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Tim \'marv\' Ringenbach"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Megan \'Cae\' Schneider"), ("support/QA"), ((char *)((void *)0))}, {("Jim Seymour"), ("XMPP"), ((char *)((void *)0))}, {("Mark Spencer"), ("original author"), ("markster@marko.net")}, {("Nathan \'faceprint\' Walp"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Eric Warmenhoven"), ("lead developer"), ("warmenhoven@yahoo.com")}, {((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0))}};
/* Order: Alphabetical by Last Name */
static const struct developer retired_patch_writers[] = {{("Felipe \'shx\' Contreras"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Decklin Foster"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Peter \'Bleeter\' Lawler"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Robert \'Robot101\' McQueen"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Benjamin Miller"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Dennis \'EvilDennisR\' Ristuccia"), ("Senior Contributor/QA"), ((char *)((void *)0))}, {("Peter \'Fmoo\' Ruibal"), ((char *)((void *)0)), ((char *)((void *)0))}, {("Gabriel \'Nix\' Schulhof"), ((char *)((void *)0)), ((char *)((void *)0))}, {((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0))}};
/* Order: Code, then Alphabetical by Last Name */
static const struct translator translators[] = {{("Afrikaans"), ("af"), ("Samuel Murray"), ("afrikaans@gmail.com")}, {("Afrikaans"), ("af"), ("Friedel Wolff"), ("friedel@translate.org.za")}, {("Arabic"), ("ar"), ("Khaled Hosny"), ("khaledhosny@eglug.org")}, {("Assamese"), ("as"), ("Amitakhya Phukan"), ("aphukan@fedoraproject.org")}, {("Asturian"), ("ast"), ("Llumex03"), ("l.lumex03.tornes@gmail.com")}, {("Belarusian Latin"), ("be@latin"), ("Ihar Hrachyshka"), ("ihar.hrachyshka@gmail.com")}, {("Bulgarian"), ("bg"), ("Vladimira Girginova"), ("missing@here.is")}, {("Bulgarian"), ("bg"), ("Vladimir (Kaladan) Petkov"), ("kaladan@gmail.com")}, {("Bengali"), ("bn"), ("Jamil Ahmed"), ("jamil@bengalinux.org")}, {("Bengali"), ("bn"), ("Israt Jahan"), ("israt@ankur.org.bd")}, {("Bengali"), ("bn"), ("Samia Nimatullah"), ("mailsamia2001@yahoo.com")}, {("Bengali-India"), ("bn_IN"), ("Runa Bhattacharjee"), ("runab@fedoraproject.org")}, {("Bosnian"), ("bs"), ("Lejla Hadzialic"), ("lejlah@gmail.com")}, {("Catalan"), ("ca"), ("Josep Puigdemont"), ("josep.puigdemont@gmail.com")}, {("Valencian-Catalan"), ("ca@valencia"), ("Toni Hermoso"), ("toniher@softcatala.org")}, {("Valencian-Catalan"), ("ca@valencia"), ("Josep Puigdemont"), ("tradgnome@softcatala.org")}, {("Czech"), ("cs"), ("David Vachulka"), ("david@konstrukce-cad.com")}, {("Danish"), ("da"), ("Peter Bach"), ("bach.peter@gmail.com")}, {("Danish"), ("da"), ("Morten Brix Pedersen"), ("morten@wtf.dk")}, {("German"), ("de"), ("Bj\303\266rn Voigt"), ("bjoernv@arcor.de")}, {("Dzongkha"), ("dz"), ("Norbu"), ("nor_den@hotmail.com")}, {("Dzongkha"), ("dz"), ("Jurmey Rabgay"), ("jur_gay@yahoo.com")}, {("Dzongkha"), ("dz"), ("Wangmo Sherpa"), ("rinwanshe@yahoo.com")}, {("Greek"), ("el"), ("Katsaloulis Panayotis"), ("panayotis@panayotis.com")}, {("Greek"), ("el"), ("Panos Bouklis"), ("panos@echidna-band.com")}, {("Australian English"), ("en_AU"), ("Peter Lawler"), ("trans@six-by-nine.com.au")}, {("British English"), ("en_GB"), ("Phil Hannent"), ("phil@hannent.co.uk")}, {("Canadian English"), ("en_CA"), ("Adam Weinberger"), ("adamw@gnome.org")}, {("Esperanto"), ("eo"), ("St\303\251phane Fillod"), ("fillods@users.sourceforge.net")}, {("Spanish"), ("es"), ("Javier Fern\303\241ndez-Sanguino Pe\303\261a"), ("jfs@debian.org")}, {("Estonian"), ("et"), ("Ivar Smolin"), ("okul@linux.ee")}, {("Basque"), ("eu"), ("Mikel Pascual Aldabaldetreku"), ("mikel.paskual@gmail.com")}, {("Persian"), ("fa"), ("Elnaz Sarbar"), ("elnaz@farsiweb.info")}, {("Persian"), ("fa"), ("Roozbeh Pournader"), ("roozbeh@farsiweb.info")}, {("Persian"), ("fa"), ("Meelad Zakaria"), ("meelad@farsiweb.info")}, {("Finnish"), ("fi"), ("Timo Jyrinki"), ("timo.jyrinki@iki.fi")}, {("French"), ("fr"), ("\303\211ric Boumaour"), ("zongo_fr@users.sourceforge.net")}, {("Irish"), ("ga"), ("Aaron Kearns"), ("ajkearns6@gmail.com")}, {("Irish"), ("ga"), ("Kevin Scannell"), ("NULL")}, {("Galician"), ("gl"), ("Mar Castro"), ("mariamarcp@gmail.com")}, {("Galician"), ("gl"), ("Frco. Javier Rial"), ("fjrial@cesga.es")}, {("Gujarati"), ("gu"), ("Ankit Patel"), ("ankit_patel@users.sf.net")}, {("Gujarati"), ("gu"), ("Gujarati Language Team"), ("indianoss-gujarati@lists.sourceforge.net")}, {("Hebrew"), ("he"), ("Shalom Craimer"), ("scraimer@gmail.com")}, {("Hindi"), ("hi"), ("Sangeeta Kumari"), ("sangeeta_0975@yahoo.com")}, {("Hindi"), ("hi"), ("Rajesh Ranjan"), ("rajeshkajha@yahoo.com")}, {("Croatian"), ("hr"), ("Sabina Drempeti\304\207"), ("bina91991@googlemail.com")}, {("Hungarian"), ("hu"), ("Kelemen G\303\241bor"), ("kelemeng@gnome.hu")}, {("Armenian"), ("hy"), ("David Avsharyan"), ("avsharyan@gmail.com")}, {("Indonesian"), ("id"), ("Rai S. Regawa"), ("raireg@yahoo.com")}, {("Italian"), ("it"), ("Claudio Satriano"), ("satriano@gmail.com")}, {("Japanese"), ("ja"), ("Takayuki Kusano"), ("AE5T-KSN@asahi-net.or.jp")}, {("Georgian"), ("ka"), ("Ubuntu Georgian Translators"), ("alexander.didebulidze@stusta.mhn.de")}, {("Khmer"), ("km"), ("Khoem Sokhem"), ("khoemsokhem@khmeros.info")}, {("Kannada"), ("kn"), ("Kannada Translation team"), ("translation@sampada.info")}, {("Korean"), ("ko"), ("Sushizang"), ("sushizang@empal.com")}, {("Kurdish"), ("ku"), ("Amed \303\207. Jiyan"), ("amedcj@hotmail.com")}, {("Kurdish"), ("ku"), ("Erdal Ronahi"), ("erdal.ronahi@gmail.com")}, {("Kurdish"), ("ku"), ("Rizoy\303\252 Xerz\303\256"), ("rizoxerzi@hotmail.com")}, {("Lao"), ("lo"), ("Anousak Souphavah"), ("anousak@gmail.com")}, {("Lithuanian"), ("lt"), ("Algimantas Margevi\304\215ius"), ("margevicius.algimantas@gmail.com")}, {("Maithili"), ("mai"), ("Sangeeta Kumari"), ("sangeeta_0975@yahoo.com")}, {("Maithili"), ("mai"), ("Rajesh Ranjan"), ("rajeshkajha@yahoo.com")}, {("Meadow Mari"), ("mhr"), ("David Preece"), ("davidpreece1@gmail.com")}, {("Macedonian"), ("mk"), ("Arangel Angov "), ("arangel@linux.net.mk")}, {("Macedonian"), ("mk"), ("Ivana Kirkovska"), ("ivana.kirkovska@gmail.com")}, {("Macedonian"), ("mk"), ("Jovan Naumovski"), ("jovan@lugola.net")}, {("Malayalam"), ("ml"), ("Ani Peter"), ("apeter@redhat.com")}, {("Mongolian"), ("mn"), ("gooyo"), ((char *)((void *)0))}, {("Marathi"), ("mr"), ("Sandeep Shedmake"), ("sandeep.shedmake@gmail.com")}, {("Malay"), ("ms_MY"), ("Muhammad Najmi bin Ahmad Zabidi"), ("najmi.zabidi@gmail.com")}, {("Burmese"), ("my_MM"), ("Thura Hlaing"), ("trhura@gmail.com")}, {("Bokm\303\245l Norwegian"), ("nb"), ("Hans Fredrik Nordhaug"), ("hans@nordhaug.priv.no")}, {("Nepali"), ("ne"), ("Shyam Krishna Bal"), ((char *)((void *)0))}, {("Dutch, Flemish"), ("nl"), ("Gideon van Melle"), ("translations@gvmelle.com")}, {("Norwegian Nynorsk"), ("nn"), ("Yngve Spjeld Landro"), ("l10n@landro.net")}, {("Occitan"), ("oc"), ("Yannig Marchegay"), ("yannig@marchegay.org")}, {("Oriya"), ("or"), ("Manoj Kumar Giri"), ("giri.manojkr@gmail.com")}, {("Punjabi"), ("pa"), ("Amanpreet Singh Alam"), ("aalam@users.sf.net")}, {("Polish"), ("pl"), ("Piotr Dr\304\205g"), ("piotrdrag@gmail.com")}, {("Portuguese"), ("pt"), ("Paulo Ribeiro"), ("paulo@diffraction.pt")}, {("Portuguese-Brazil"), ("pt_BR"), ("Renato Silva"), ("br.renatosilva@gmail.com")}, {("Pashto"), ("ps"), ("Kashif Masood"), ("masudmails@yahoo.com")}, {("Romanian"), ("ro"), ("Mi\310\231u Moldovan"), ("dumol@gnome.org")}, {("Romanian"), ("ro"), ("Andrei Popescu"), ("andreimpopescu@gmail.com")}, {("Russian"), ("ru"), ("\320\220\320\275\321\202\320\276\320\275 \320\241\320\260\320\274\320\276\321\205\320\262\320\260\320\273\320\276\320\262"), ("samant.ua@mail.ru")}, {("Slovak"), ("sk"), ("Jozef K\303\241\304\215er"), ("quickparser@gmail.com")}, {("Slovak"), ("sk"), ("loptosko"), ("loptosko@gmail.com")}, {("Slovenian"), ("sl"), ("Martin Srebotnjak"), ("miles@filmsi.net")}, {("Albanian"), ("sq"), ("Besnik Bleta"), ("besnik@programeshqip.org")}, {("Serbian"), ("sr"), ("Milo\305\241 Popovi\304\207"), ("gpopac@gmail.com")}, {("Serbian Latin"), ("sr@latin"), ("Milo\305\241 Popovi\304\207"), ("gpopac@gmail.com")}, {("Sinhala"), ("si"), ("Yajith Ajantha Dayarathna"), ("yajith@gmail.com")}, {("Sinhala"), ("si"), ("Danishka Navin"), ("snavin@redhat.com")}, {("Swedish"), ("sv"), ("Peter Hjalmarsson"), ("xake@telia.com")}, {("Swahili"), ("sw"), ("Paul Msegeya"), ("msegeya@gmail.com")}, {("Tamil"), ("ta"), ("I. Felix"), ("ifelix25@gmail.com")}, {("Tamil"), ("ta"), ("Viveka Nathan K"), ("vivekanathan@users.sourceforge.net")}, {("Telugu"), ("te"), ("Krishnababu Krottapalli"), ("krottapalli@ymail.com")}, {("Thai"), ("th"), ("Isriya Paireepairit"), ("markpeak@gmail.com")}, {("Ukranian"), ("uk"), ("Oleksandr Kovalenko"), ("alx.kovalenko@gmail.com")}, {("Urdu"), ("ur"), ("RKVS Raman"), ("rkvsraman@gmail.com")}, {("Vietnamese"), ("vi"), ("Nguy\341\273\205n V\305\251 H\306\260ng"), ("vuhung16plus@gmail.com")}, {("Simplified Chinese"), ("zh_CN"), ("Aron Xu"), ("happyaron.xu@gmail.com")}, {("Hong Kong Chinese"), ("zh_HK"), ("Abel Cheung"), ("abelindsay@gmail.com")}, {("Hong Kong Chinese"), ("zh_HK"), ("Ambrose C. Li"), ("acli@ada.dhs.org")}, {("Hong Kong Chinese"), ("zh_HK"), ("Paladin R. Liu"), ("paladin@ms1.hinet.net")}, {("Traditional Chinese"), ("zh_TW"), ("Ambrose C. Li"), ("acli@ada.dhs.org")}, {("Traditional Chinese"), ("zh_TW"), ("Paladin R. Liu"), ("paladin@ms1.hinet.net")}, {((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0))}};
static const struct translator past_translators[] = {{("Amharic"), ("am"), ("Daniel Yacob"), ((char *)((void *)0))}, {("Arabic"), ("ar"), ("Mohamed Magdy"), ("alnokta@yahoo.com")}, {("Bulgarian"), ("bg"), ("Hristo Todorov"), ((char *)((void *)0))}, {("Bengali"), ("bn"), ("Indranil Das Gupta"), ("indradg@l2c2.org")}, {("Bengali"), ("bn"), ("Tisa Nafisa"), ("tisa_nafisa@yahoo.com")}, {("Catalan"), ("ca"), ("JM P\303\251rez C\303\241ncer"), ((char *)((void *)0))}, {("Catalan"), ("ca"), ("Robert Millan"), ((char *)((void *)0))}, {("Czech"), ("cs"), ("Honza Kr\303\241l"), ((char *)((void *)0))}, {("Czech"), ("cs"), ("Miloslav Trmac"), ("mitr@volny.cz")}, {("German"), ("de"), ("Daniel Seifert, Karsten Weiss"), ((char *)((void *)0))}, {("German"), ("de"), ("Jochen Kemnade"), ("jochenkemnade@web.de")}, {("British English"), ("en_GB"), ("Luke Ross"), ("luke@lukeross.name")}, {("Spanish"), ("es"), ("JM P\303\251rez C\303\241ncer"), ((char *)((void *)0))}, {("Spanish"), ("es"), ("Nicol\303\241s Lichtmaier"), ((char *)((void *)0))}, {("Spanish"), ("es"), ("Amaya Rodrigo"), ((char *)((void *)0))}, {("Spanish"), ("es"), ("Alejandro G Villar"), ((char *)((void *)0))}, {("Basque"), ("eu"), ("I\303\261aki Larra\303\261aga Murgoitio"), ("dooteo@zundan.com")}, {("Basque"), ("eu"), ("Hizkuntza Politikarako Sailburuordetza"), ("hizkpol@ej-gv.es")}, {("Finnish"), ("fi"), ("Arto Alakulju"), ((char *)((void *)0))}, {("Finnish"), ("fi"), ("Tero Kuusela"), ((char *)((void *)0))}, {("French"), ("fr"), ("S\303\251bastien Fran\303\247ois"), ((char *)((void *)0))}, {("French"), ("fr"), ("Lo\303\257c Jeannin"), ((char *)((void *)0))}, {("French"), ("fr"), ("St\303\251phane Pontier"), ((char *)((void *)0))}, {("French"), ("fr"), ("St\303\251phane Wirtel"), ((char *)((void *)0))}, {("Galician"), ("gl"), ("Ignacio Casal Quinteiro"), ((char *)((void *)0))}, {("Hebrew"), ("he"), ("Pavel Bibergal"), ((char *)((void *)0))}, {("Hindi"), ("hi"), ("Ravishankar Shrivastava"), ((char *)((void *)0))}, {("Hungarian"), ("hu"), ("Zoltan Sutto"), ((char *)((void *)0))}, {("Italian"), ("it"), ("Salvatore di Maggio"), ((char *)((void *)0))}, {("Japanese"), ("ja"), ("Takashi Aihana"), ((char *)((void *)0))}, {("Japanese"), ("ja"), ("Ryosuke Kutsuna"), ((char *)((void *)0))}, {("Japanese"), ("ja"), ("Junichi Uekawa"), ((char *)((void *)0))}, {("Japanese"), ("ja"), ("Taku Yasui"), ((char *)((void *)0))}, {("Georgian"), ("ka"), ("Temuri Doghonadze"), ((char *)((void *)0))}, {("Korean"), ("ko"), ("Sang-hyun S, A Ho-seok Lee"), ((char *)((void *)0))}, {("Korean"), ("ko"), ("Kyeong-uk Son"), ((char *)((void *)0))}, {("Lithuanian"), ("lt"), ("Laurynas Biveinis"), ("laurynas.biveinis@gmail.com")}, {("Lithuanian"), ("lt"), ("Gediminas \304\214i\304\215inskas"), ((char *)((void *)0))}, {("Lithuanian"), ("lt"), ("Andrius \305\240tikonas"), ((char *)((void *)0))}, {("Macedonian"), ("mk"), ("Tomislav Markovski"), ((char *)((void *)0))}, {("Bokm\303\245l Norwegian"), ("nb"), ("Hallvard Glad"), ("hallvard.glad@gmail.com")}, {("Bokm\303\245l Norwegian"), ("nb"), ("Petter Johan Olsen"), ((char *)((void *)0))}, {("Bokm\303\245l Norwegian"), ("nb"), ("Espen Stefansen"), ("espenas@gmail.com")}, {("Dutch, Flemish"), ("nl"), ("Vincent van Adrighem"), ("V.vanAdrighem@dirck.mine.nu")}, {("Polish"), ("pl"), ("Krzysztof Foltman"), ("krzysztof@foltman.com")}, {("Polish"), ("pl"), ("Pawe\305\202 Godlewski"), ("pawel@bajk.pl")}, {("Polish"), ("pl"), ("Piotr Makowski"), ((char *)((void *)0))}, {("Polish"), ("pl"), ("Emil Nowak"), ("emil5@go2.pl")}, {("Polish"), ("pl"), ("Przemys\305\202aw Su\305\202ek"), ((char *)((void *)0))}, {("Portuguese"), ("pt"), ("Duarte Henriques"), ((char *)((void *)0))}, {("Portuguese-Brazil"), ("pt_BR"), ("Maur\303\255cio de Lemos Rodrigues Collares Neto"), ("mauricioc@gmail.com")}, {("Portuguese-Brazil"), ("pt_BR"), ("Rodrigo Luiz Marques Flores"), ("rodrigomarquesflores@gmail.com")}, {("Russian"), ("ru"), ("Dmitry Beloglazov"), ("dmaa@users.sf.net")}, {("Russian"), ("ru"), ("Alexandre Prokoudine"), ((char *)((void *)0))}, {("Russian"), ("ru"), ("Sergey Volozhanin"), ((char *)((void *)0))}, {("Slovak"), ("sk"), ("Daniel Re\305\276n\303\275"), ((char *)((void *)0))}, {("Slovak"), ("sk"), ("Richard Golier"), ((char *)((void *)0))}, {("Slovak"), ("sk"), ("helix84"), ((char *)((void *)0))}, {("Slovenian"), ("sl"), ("Matjaz Horvat"), ((char *)((void *)0))}, {("Serbian"), ("sr"), ("Danilo \305\240egan"), ("dsegan@gmx.net")}, {("Serbian"), ("sr"), ("Aleksandar Urosevic"), ("urke@users.sourceforge.net")}, {("Swedish"), ("sv"), ("Tore Lundqvist"), ((char *)((void *)0))}, {("Swedish"), ("sv"), ("Christian Rose"), ((char *)((void *)0))}, {("Telugu"), ("te"), ("Mr. Subbaramaih"), ("info.gist@cdac.in")}, {("Turkish"), ("tr"), ("Serdar Soytetir"), ("tulliana@gmail.com")}, {("Turkish"), ("tr"), ("Ahmet Alp Balkan"), ((char *)((void *)0))}, {("Vietnamese"), ("vi"), ("T.M.Thanh and the Gnome-Vi Team"), ("gnomevi-list@lists.sf.net")}, {("Simplified Chinese"), ("zh_CN"), ("Hashao, Rocky S. Lee"), ((char *)((void *)0))}, {("Simplified Chinese"), ("zh_CN"), ("Funda Wang"), ("fundawang@linux.net.cn")}, {("Traditional Chinese"), ("zh_TW"), ("Hashao, Rocky S. Lee"), ((char *)((void *)0))}, {((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0))}};

static void add_developers(GString *str,const struct developer *list)
{
  for (; (list -> name) != ((char *)((void *)0)); list++) {
    if ((list -> email) != ((char *)((void *)0))) {
      const gchar *proto = "mailto:";
      if (strchr((list -> email),':') != ((char *)((void *)0))) 
        proto = "";
      g_string_append_printf(str,"  <a href=\"%s%s\">%s</a>%s%s%s<br/>",proto,(list -> email),((const char *)(dgettext("pidgin",(list -> name)))),(((list -> role) != 0)?" (" : ""),(((list -> role) != 0)?((const char *)(dgettext("pidgin",(list -> role)))) : ""),(((list -> role) != 0)?")" : ""));
    }
    else {
      g_string_append_printf(str,"  %s%s%s%s<br/>",((const char *)(dgettext("pidgin",(list -> name)))),(((list -> role) != 0)?" (" : ""),(((list -> role) != 0)?((const char *)(dgettext("pidgin",(list -> role)))) : ""),(((list -> role) != 0)?")" : ""));
    }
  }
}

static void add_translators(GString *str,const struct translator *list)
{
  for (; (list -> language) != ((char *)((void *)0)); list++) {
    if ((list -> email) != ((char *)((void *)0))) {
      g_string_append_printf(str,"  <b>%s (%s)</b> - <a href=\"mailto:%s\">%s</a><br/>",((const char *)(dgettext("pidgin",(list -> language)))),(list -> abbr),(list -> email),((const char *)(dgettext("pidgin",(list -> name)))));
    }
    else {
      g_string_append_printf(str,"  <b>%s (%s)</b> - %s<br/>",((const char *)(dgettext("pidgin",(list -> language)))),(list -> abbr),((const char *)(dgettext("pidgin",(list -> name)))));
    }
  }
}

void pidgin_dialogs_destroy_all()
{
  while(dialogwindows != 0){
    gtk_widget_destroy((dialogwindows -> data));
    dialogwindows = g_list_remove(dialogwindows,(dialogwindows -> data));
  }
}

static void destroy_win(GtkWidget *button,GtkWidget *win)
{
  gtk_widget_destroy(win);
}
#if 0
/* This function puts the version number onto the pixmap we use in the 'about'
 * screen in Pidgin. */
#endif
/* Note: Frees 'string' */

static GtkWidget *pidgin_build_help_dialog(const char *title,const char *role,GString *string)
{
  GtkWidget *win;
  GtkWidget *vbox;
  GtkWidget *frame;
  GtkWidget *logo;
  GtkWidget *imhtml;
  GtkWidget *button;
  GdkPixbuf *pixbuf;
  GtkTextIter iter;
  AtkObject *obj;
  char *filename;
  char *tmp;
  win = pidgin_create_dialog(title,12,role,(!0));
  vbox = pidgin_dialog_get_vbox_with_properties(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),0,12);
  gtk_window_set_default_size(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_window_get_type()))),450,450);
/* Generate a logo with a version number */
  filename = g_build_filename("/usr/local/share","pixmaps","pidgin","logo.png",((void *)((void *)0)));
  pixbuf = pidgin_pixbuf_new_from_file(filename);
  g_free(filename);
#if 0  /* Don't versionize the logo when the logo has the version in it */
#endif
/* Insert the logo */
  logo = gtk_image_new_from_pixbuf(pixbuf);
  if (pixbuf != 0) 
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
  obj = gtk_widget_get_accessible(logo);
  tmp = g_strconcat(((const char *)(dgettext("pidgin","Pidgin")))," 2.10.9",((void *)((void *)0)));
  atk_object_set_description(obj,tmp);
  g_free(tmp);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),logo,0,0,0);
  frame = pidgin_create_imhtml(0,&imhtml,0,0);
  gtk_imhtml_set_format_functions(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),(GTK_IMHTML_ALL ^ GTK_IMHTML_SMILEY));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),frame,(!0),(!0),0);
  gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),(string -> str),GTK_IMHTML_NO_SCROLL,0);
  gtk_text_buffer_get_start_iter(gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type())))),&iter);
  gtk_text_buffer_place_cursor(gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type())))),(&iter));
  button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),"gtk-close",((GCallback )destroy_win),win);
  do {
    ( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_object_get_type())))).flags |= GTK_CAN_DEFAULT;
  }while (0);
  gtk_widget_grab_default(button);
  gtk_widget_show_all(win);
  gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_window_get_type()))));
  g_string_free(string,(!0));
  return win;
}

void pidgin_dialogs_about()
{
  GString *str;
  char *tmp;
  static GtkWidget *about = (GtkWidget *)((void *)0);
  if (about != ((GtkWidget *)((void *)0))) {
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)about),gtk_window_get_type()))));
    return ;
  }
  str = g_string_sized_new(4096);
  g_string_append_printf(str,"<CENTER><FONT SIZE=\"4\"><B>%s %s</B></FONT></CENTER> (libpurple %s)<BR>%s<BR><BR>",((const char *)(dgettext("pidgin","Pidgin"))),"2.10.9",purple_core_get_version(),"unknown");
  g_string_append_printf(str,((const char *)(dgettext("pidgin","%s is a messaging client based on libpurple which is capable of connecting to multiple messaging services at once.  %s is written in C using GTK+.  %s is released, and may be modified and redistributed,  under the terms of the GPL version 2 (or later).  A copy of the GPL is distributed with %s.  %s is copyrighted by its contributors, a list of whom is also distributed with %s.  There is no warranty for %s.<BR><BR>"))),((const char *)(dgettext("pidgin","Pidgin"))),((const char *)(dgettext("pidgin","Pidgin"))),((const char *)(dgettext("pidgin","Pidgin"))),((const char *)(dgettext("pidgin","Pidgin"))),((const char *)(dgettext("pidgin","Pidgin"))),((const char *)(dgettext("pidgin","Pidgin"))),((const char *)(dgettext("pidgin","Pidgin"))));
  g_string_append_printf(str,((const char *)(dgettext("pidgin","<FONT SIZE=\"4\"><B>Helpful Resources</B></FONT><BR>\t<A HREF=\"%s\">Website</A><BR>\t<A HREF=\"%s\">Frequently Asked Questions</A><BR>\tIRC Channel: #pidgin on irc.freenode.net<BR>\tXMPP MUC: devel@conference.pidgin.im<BR><BR>"))),"http://pidgin.im/","http://developer.pidgin.im/wiki/FAQ");
  g_string_append(str,"<font size=\"4\"><b>Help for Oracle Employees</b></font> is available from your normal internal helpdesk or IT department.  The Pidgin developer and user communities cannot assist you in the configuration or use of Pidgin within Oracle, as we know nothing of Oracle\'s infrastructure.<br/><br/>");
  g_string_append(str,((const char *)(dgettext("pidgin","<font size=\"4\"><b>Help from other Pidgin users</b></font> is available by e-mailing <a href=\"mailto:support@pidgin.im\">support@pidgin.im</a><br/>This is a <b>public</b> mailing list! (<a href=\"http://pidgin.im/pipermail/support/\">archive</a>)<br/>We can\'t help with third-party protocols or plugins!<br/>This list\'s primary language is <b>English</b>.  You are welcome to post in another language, but the responses may be less helpful.<br/>"))));
  tmp = g_strdup_printf(((const char *)(dgettext("pidgin","About %s"))),((const char *)(dgettext("pidgin","Pidgin"))));
  about = pidgin_build_help_dialog(tmp,"about",str);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)about),((GType )(20 << 2))))),"destroy",((GCallback )gtk_widget_destroyed),(&about),0,((GConnectFlags )0));
  g_free(tmp);
}

void pidgin_dialogs_buildinfo()
{
  GString *str;
  char *tmp;
  static GtkWidget *buildinfo = (GtkWidget *)((void *)0);
  if (buildinfo != ((GtkWidget *)((void *)0))) {
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)buildinfo),gtk_window_get_type()))));
    return ;
  }
  str = g_string_sized_new(4096);
  g_string_append_printf(str,"<FONT SIZE=\"4\"><B>%s %s</B></FONT> (libpurple %s)<BR>%s<BR><BR>",((const char *)(dgettext("pidgin","Pidgin"))),"2.10.9",purple_core_get_version(),"unknown");
  g_string_append_printf(str,"<FONT SIZE=\"4\"><B>%s</B></FONT><br/>",((const char *)(dgettext("pidgin","Build Information"))));
/* The following primarly intented for user/developer interaction and thus
	   ought not be translated */
#ifdef CONFIG_ARGS /* win32 build doesn't use configure */
  g_string_append(str,"  <b>Arguments to <i>./configure</i>:</b>   \'CC=exampleTranslator\' \'--disable-perl\'<br/>");
#endif
#ifndef _WIN32
#ifdef DEBUG
#else
  g_string_append(str,"  <b>Print debugging messages:</b> No<br/>");
#endif
#endif
#ifdef PURPLE_PLUGINS
  g_string_append(str,"  <b>Plugins:</b> Enabled<br/>");
#else
#endif
#ifdef HAVE_SSL
  g_string_append(str,"  <b>SSL:</b> SSL support is present.<br/>");
#else
#endif
/* This might be useful elsewhere too, but it is particularly useful for
 * debugging stuff known to be GTK+/Glib bugs on Windows */
#ifdef _WIN32
#endif
  g_string_append(str,"<br/>  <b>Library Support</b><br/>");
#ifdef HAVE_CYRUS_SASL
#else
  g_string_append_printf(str,"    <b>Cyrus SASL:</b> Disabled<br/>");
#endif
#ifndef _WIN32
#ifdef HAVE_DBUS
  g_string_append_printf(str,"    <b>D-Bus:</b> Enabled<br/>");
#else
#endif
#ifdef HAVE_EVOLUTION_ADDRESSBOOK
#else
  g_string_append_printf(str,"    <b>Evolution Addressbook:</b> Disabled<br/>");
#endif
#endif
#if defined(_WIN32) || defined(USE_INTERNAL_LIBGADU)
#else
#ifdef HAVE_LIBGADU
  g_string_append(str,"    <b>Gadu-Gadu library (libgadu):</b> Enabled<br/>");
#else
#endif
#endif
#ifdef USE_GTKSPELL
  g_string_append(str,"    <b>GtkSpell:</b> Enabled<br/>");
#else
#endif
#ifdef HAVE_GNUTLS
  g_string_append(str,"    <b>GnuTLS:</b> Enabled<br/>");
#else
#endif
#ifndef _WIN32
#ifdef USE_GSTREAMER
  g_string_append(str,"    <b>GStreamer:</b> Enabled<br/>");
#else
#endif
#endif
#ifndef _WIN32
#ifdef ENABLE_MONO
#else
  g_string_append(str,"    <b>Mono:</b> Disabled<br/>");
#endif
#endif
#ifndef _WIN32
#ifdef HAVE_NETWORKMANAGER
  g_string_append(str,"    <b>NetworkManager:</b> Enabled<br/>");
#else
#endif
#endif
#ifdef HAVE_NSS
  g_string_append(str,"    <b>Network Security Services (NSS):</b> Enabled<br/>");
#else
#endif
  if (purple_plugins_find_with_id("core-perl") != ((PurplePlugin *)((void *)0))) 
    g_string_append(str,"    <b>Perl:</b> Enabled<br/>");
  else 
    g_string_append(str,"    <b>Perl:</b> Disabled<br/>");
  if (purple_plugins_find_with_id("core-tcl") != ((PurplePlugin *)((void *)0))) {
    g_string_append(str,"    <b>Tcl:</b> Enabled<br/>");
#ifdef HAVE_TK
    g_string_append(str,"    <b>Tk:</b> Enabled<br/>");
#else
#endif
  }
  else {
    g_string_append(str,"    <b>Tcl:</b> Disabled<br/>");
    g_string_append(str,"    <b>Tk:</b> Disabled<br/>");
  }
#ifdef USE_IDN
  g_string_append(str,"    <b>UTF-8 DNS (IDN):</b> Enabled<br/>");
#else
#endif
#ifdef USE_VV
  g_string_append(str,"    <b>Voice and Video:</b> Enabled<br/>");
#else
#endif
#ifndef _WIN32
#ifdef USE_SM
  g_string_append(str,"    <b>X Session Management:</b> Enabled<br/>");
#else
#endif
#ifdef USE_SCREENSAVER
  g_string_append(str,"    <b>XScreenSaver:</b> Enabled<br/>");
#else
#endif
#ifdef LIBZEPHYR_EXT
#else
  g_string_append(str,"    <b>Zephyr library (libzephyr):</b> Internal<br/>");
#endif
#ifdef ZEPHYR_USES_KERBEROS
#else
  g_string_append(str,"    <b>Zephyr uses Kerberos:</b> No<br/>");
#endif
#endif
/* End of not to be translated section */
  tmp = g_strdup_printf(((const char *)(dgettext("pidgin","%s Build Information"))),((const char *)(dgettext("pidgin","Pidgin"))));
  buildinfo = pidgin_build_help_dialog(tmp,"buildinfo",str);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buildinfo),((GType )(20 << 2))))),"destroy",((GCallback )gtk_widget_destroyed),(&buildinfo),0,((GConnectFlags )0));
  g_free(tmp);
}

void pidgin_dialogs_developers()
{
  GString *str;
  char *tmp;
  static GtkWidget *developer_info = (GtkWidget *)((void *)0);
  if (developer_info != ((GtkWidget *)((void *)0))) {
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)developer_info),gtk_window_get_type()))));
    return ;
  }
  str = g_string_sized_new(4096);
/* Current Developers */
  g_string_append_printf(str,"<FONT SIZE=\"4\"><B>%s:</B></FONT><BR/>",((const char *)(dgettext("pidgin","Current Developers"))));
  add_developers(str,developers);
  g_string_append(str,"<BR/>");
/* Crazy Patch Writers */
  g_string_append_printf(str,"<FONT SIZE=\"4\"><B>%s:</B></FONT><BR/>",((const char *)(dgettext("pidgin","Crazy Patch Writers"))));
  add_developers(str,patch_writers);
  g_string_append(str,"<BR/>");
/* Retired Developers */
  g_string_append_printf(str,"<FONT SIZE=\"4\"><B>%s:</B></FONT><BR/>",((const char *)(dgettext("pidgin","Retired Developers"))));
  add_developers(str,retired_developers);
  g_string_append(str,"<BR/>");
/* Retired Crazy Patch Writers */
  g_string_append_printf(str,"<FONT SIZE=\"4\"><B>%s:</B></FONT><BR/>",((const char *)(dgettext("pidgin","Retired Crazy Patch Writers"))));
  add_developers(str,retired_patch_writers);
  tmp = g_strdup_printf(((const char *)(dgettext("pidgin","%s Developer Information"))),((const char *)(dgettext("pidgin","Pidgin"))));
  developer_info = pidgin_build_help_dialog(tmp,"developer_info",str);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)developer_info),((GType )(20 << 2))))),"destroy",((GCallback )gtk_widget_destroyed),(&developer_info),0,((GConnectFlags )0));
  g_free(tmp);
}

void pidgin_dialogs_translators()
{
  GString *str;
  char *tmp;
  static GtkWidget *translator_info = (GtkWidget *)((void *)0);
  if (translator_info != ((GtkWidget *)((void *)0))) {
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)translator_info),gtk_window_get_type()))));
    return ;
  }
  str = g_string_sized_new(4096);
/* Current Translators */
  g_string_append_printf(str,"<FONT SIZE=\"4\">%s:</FONT><BR/>",((const char *)(dgettext("pidgin","Current Translators"))));
  add_translators(str,translators);
  g_string_append(str,"<BR/>");
/* Past Translators */
  g_string_append_printf(str,"<FONT SIZE=\"4\">%s:</FONT><BR/>",((const char *)(dgettext("pidgin","Past Translators"))));
  add_translators(str,past_translators);
  tmp = g_strdup_printf(((const char *)(dgettext("pidgin","%s Translator Information"))),((const char *)(dgettext("pidgin","Pidgin"))));
  translator_info = pidgin_build_help_dialog(tmp,"translator_info",str);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)translator_info),((GType )(20 << 2))))),"destroy",((GCallback )gtk_widget_destroyed),(&translator_info),0,((GConnectFlags )0));
  g_free(tmp);
}

void pidgin_dialogs_plugins_info()
{
  GString *str;
  GList *l = (GList *)((void *)0);
  PurplePlugin *plugin = (PurplePlugin *)((void *)0);
  char *title = g_strdup_printf(((const char *)(dgettext("pidgin","%s Plugin Information"))),((const char *)(dgettext("pidgin","Pidgin"))));
  char *pname = (char *)((void *)0);
  char *pauthor = (char *)((void *)0);
  const char *pver;
  const char *pwebsite;
  const char *pid;
  gboolean ploaded;
  gboolean punloadable;
  static GtkWidget *plugins_info = (GtkWidget *)((void *)0);
  str = g_string_sized_new(4096);
  g_string_append_printf(str,"<FONT SIZE=\"4\">%s</FONT><BR/>",((const char *)(dgettext("pidgin","Plugin Information"))));
  for (l = purple_plugins_get_all(); l != 0; l = (l -> next)) {
    plugin = ((PurplePlugin *)(l -> data));
    pname = g_markup_escape_text(purple_plugin_get_name(plugin),(-1));
    pauthor = g_markup_escape_text(purple_plugin_get_author(plugin),(-1));
    pver = purple_plugin_get_version(plugin);
    pwebsite = purple_plugin_get_homepage(plugin);
    pid = purple_plugin_get_id(plugin);
    punloadable = purple_plugin_is_unloadable(plugin);
    ploaded = purple_plugin_is_loaded(plugin);
    g_string_append_printf(str,"<FONT SIZE=\"3\"><B>%s</B></FONT><BR/><FONT SIZE=\"2\">\t<B>Author:</B> %s<BR/>\t<B>Version:</B> %s<BR/>\t<B>Website:</B> %s<BR/>\t<B>ID String:</B> %s<BR/>\t<B>Loadable:</B> %s<BR/>\t<B>Loaded:</B> %s<BR/><BR/></FONT>",pname,((pauthor != 0)?pauthor : "(null)"),pver,pwebsite,pid,((punloadable != 0)?"<FONT COLOR=\"#FF0000\"><B>No</B></FONT>" : "Yes"),((ploaded != 0)?"Yes" : "No"));
  }
  plugins_info = pidgin_build_help_dialog(title,"plugins_info",str);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)plugins_info),((GType )(20 << 2))))),"destroy",((GCallback )gtk_widget_destroyed),(&plugins_info),0,((GConnectFlags )0));
  g_free(title);
  g_free(pname);
  g_free(pauthor);
}

static void pidgin_dialogs_im_cb(gpointer data,PurpleRequestFields *fields)
{
  PurpleAccount *account;
  const char *username;
  account = purple_request_fields_get_account(fields,"account");
  username = purple_request_fields_get_string(fields,"screenname");
  pidgin_dialogs_im_with_user(account,username);
}

void pidgin_dialogs_im()
{
  PurpleRequestFields *fields;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  fields = purple_request_fields_new();
  group = purple_request_field_group_new(0);
  purple_request_fields_add_group(fields,group);
  field = purple_request_field_string_new("screenname",((const char *)(dgettext("pidgin","_Name"))),0,0);
  purple_request_field_set_type_hint(field,"screenname");
  purple_request_field_set_required(field,(!0));
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_account_new("account",((const char *)(dgettext("pidgin","_Account"))),0);
  purple_request_field_set_type_hint(field,"account");
  purple_request_field_set_visible(field,((purple_connections_get_all() != ((GList *)((void *)0))) && (( *purple_connections_get_all()).next != ((GList *)((void *)0)))));
  purple_request_field_set_required(field,(!0));
  purple_request_field_group_add_field(group,field);
  purple_request_fields((purple_get_blist()),((const char *)(dgettext("pidgin","New Instant Message"))),0,((const char *)(dgettext("pidgin","Please enter the username or alias of the person you would like to IM."))),fields,((const char *)(dgettext("pidgin","OK"))),((GCallback )pidgin_dialogs_im_cb),((const char *)(dgettext("pidgin","Cancel"))),0,0,0,0,0);
}

void pidgin_dialogs_im_with_user(PurpleAccount *account,const char *username)
{
  PurpleConversation *conv;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (username != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"username != NULL");
      return ;
    };
  }while (0);
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,username,account);
  if (conv == ((PurpleConversation *)((void *)0))) 
    conv = purple_conversation_new(PURPLE_CONV_TYPE_IM,account,username);
  pidgin_conv_attach_to_conversation(conv);
  purple_conversation_present(conv);
}

static gboolean pidgin_dialogs_ee(const char *ee)
{
  GtkWidget *window;
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *img;
  gchar *norm = purple_strreplace(ee,"rocksmyworld","");
  label = gtk_label_new(0);
  if (!(strcmp(norm,"zilding") != 0)) 
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),"<span weight=\"bold\" size=\"large\" foreground=\"purple\">Amazing!  Simply Amazing!</span>");
  else if (!(strcmp(norm,"robflynn") != 0)) 
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),"<span weight=\"bold\" size=\"large\" foreground=\"#1f6bad\">Pimpin\' Penguin Style! *Waddle Waddle*</span>");
  else if (!(strcmp(norm,"flynorange") != 0)) 
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),"<span weight=\"bold\" size=\"large\" foreground=\"blue\">You should be me.  I\'m so cute!</span>");
  else if (!(strcmp(norm,"ewarmenhoven") != 0)) 
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),"<span weight=\"bold\" size=\"large\" foreground=\"orange\">Now that\'s what I like!</span>");
  else if (!(strcmp(norm,"markster97") != 0)) 
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),"<span weight=\"bold\" size=\"large\" foreground=\"brown\">Ahh, and excellent choice!</span>");
  else if (!(strcmp(norm,"seanegn") != 0)) 
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),"<span weight=\"bold\" size=\"large\" foreground=\"#009900\">Everytime you click my name, an angel gets its wings.</span>");
  else if (!(strcmp(norm,"chipx86") != 0)) 
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),"<span weight=\"bold\" size=\"large\" foreground=\"red\">This sunflower seed taste like pizza.</span>");
  else if (!(strcmp(norm,"markdoliner") != 0)) 
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),"<span weight=\"bold\" size=\"large\" foreground=\"#6364B1\">Hey!  I was in that tumbleweed!</span>");
  else if (!(strcmp(norm,"lschiere") != 0)) 
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),"<span weight=\"bold\" size=\"large\" foreground=\"gray\">I\'m not anything.</span>");
  g_free(norm);
  if (strlen(gtk_label_get_label(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))))) <= 0) 
    return 0;
  window = gtk_dialog_new_with_buttons("",0,0,"gtk-close",GTK_RESPONSE_OK,((void *)((void *)0)));
  gtk_dialog_set_default_response(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),GTK_RESPONSE_OK);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)window),((GType )(20 << 2))))),"response",((GCallback )gtk_widget_destroy),0,0,((GConnectFlags )0));
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_container_get_type()))),6);
  gtk_window_set_resizable(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_window_get_type()))),0);
  gtk_dialog_set_has_separator(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type()))),0);
  gtk_box_set_spacing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type())))).vbox),gtk_box_get_type()))),12);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type())))).vbox),gtk_container_get_type()))),6);
  hbox = gtk_hbox_new(0,12);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_dialog_get_type())))).vbox),gtk_container_get_type()))),hbox);
  img = gtk_image_new_from_stock("pidgin-dialog-cool",gtk_icon_size_from_name("pidgin-icon-size-tango-huge"));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),img,0,0,0);
  gtk_label_set_line_wrap(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(!0));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  gtk_widget_show_all(window);
  return (!0);
}

static void pidgin_dialogs_info_cb(gpointer data,PurpleRequestFields *fields)
{
  char *username;
  gboolean found = 0;
  PurpleAccount *account;
  account = purple_request_fields_get_account(fields,"account");
  username = g_strdup(purple_normalize(account,purple_request_fields_get_string(fields,"screenname")));
  if ((username != ((char *)((void *)0))) && (purple_str_has_suffix(username,"rocksmyworld") != 0)) 
    found = pidgin_dialogs_ee(username);
  if (((!(found != 0) && (username != ((char *)((void *)0)))) && (( *username) != 0)) && (account != ((PurpleAccount *)((void *)0)))) 
    pidgin_retrieve_user_info(purple_account_get_connection(account),username);
  g_free(username);
}

void pidgin_dialogs_info()
{
  PurpleRequestFields *fields;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  fields = purple_request_fields_new();
  group = purple_request_field_group_new(0);
  purple_request_fields_add_group(fields,group);
  field = purple_request_field_string_new("screenname",((const char *)(dgettext("pidgin","_Name"))),0,0);
  purple_request_field_set_type_hint(field,"screenname");
  purple_request_field_set_required(field,(!0));
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_account_new("account",((const char *)(dgettext("pidgin","_Account"))),0);
  purple_request_field_set_type_hint(field,"account");
  purple_request_field_set_visible(field,((purple_connections_get_all() != ((GList *)((void *)0))) && (( *purple_connections_get_all()).next != ((GList *)((void *)0)))));
  purple_request_field_set_required(field,(!0));
  purple_request_field_group_add_field(group,field);
  purple_request_fields((purple_get_blist()),((const char *)(dgettext("pidgin","Get User Info"))),0,((const char *)(dgettext("pidgin","Please enter the username or alias of the person whose info you would like to view."))),fields,((const char *)(dgettext("pidgin","OK"))),((GCallback )pidgin_dialogs_info_cb),((const char *)(dgettext("pidgin","Cancel"))),0,0,0,0,0);
}

static void pidgin_dialogs_log_cb(gpointer data,PurpleRequestFields *fields)
{
  char *username;
  PurpleAccount *account;
  GSList *cur;
  account = purple_request_fields_get_account(fields,"account");
  username = g_strdup(purple_normalize(account,purple_request_fields_get_string(fields,"screenname")));
  if (((username != ((char *)((void *)0))) && (( *username) != 0)) && (account != ((PurpleAccount *)((void *)0)))) {
    PidginBuddyList *gtkblist = pidgin_blist_get_default_gtk_blist();
    GSList *buddies;
    pidgin_set_cursor((gtkblist -> window),GDK_WATCH);
    buddies = purple_find_buddies(account,username);
    for (cur = buddies; cur != ((GSList *)((void *)0)); cur = (cur -> next)) {
      PurpleBlistNode *node = (cur -> data);
      if ((node != ((PurpleBlistNode *)((void *)0))) && (((node -> prev) != ((PurpleBlistNode *)((void *)0))) || ((node -> next) != ((PurpleBlistNode *)((void *)0))))) {
        pidgin_log_show_contact(((PurpleContact *)(node -> parent)));
        g_slist_free(buddies);
        pidgin_clear_cursor((gtkblist -> window));
        g_free(username);
        return ;
      }
    }
    g_slist_free(buddies);
    pidgin_log_show(PURPLE_LOG_IM,username,account);
    pidgin_clear_cursor((gtkblist -> window));
  }
  g_free(username);
}
/*
 * TODO - This needs to deal with logs of all types, not just IM logs.
 */

void pidgin_dialogs_log()
{
  PurpleRequestFields *fields;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  fields = purple_request_fields_new();
  group = purple_request_field_group_new(0);
  purple_request_fields_add_group(fields,group);
  field = purple_request_field_string_new("screenname",((const char *)(dgettext("pidgin","_Name"))),0,0);
  purple_request_field_set_type_hint(field,"screenname-all");
  purple_request_field_set_required(field,(!0));
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_account_new("account",((const char *)(dgettext("pidgin","_Account"))),0);
/* purple_request_field_account_new() only sets a default value if you're
	 * connected, and it sets it from the list of connected accounts.  Since
	 * we're going to set show_all here, it makes sense to use the first
	 * account, not the first connected account. */
  if (purple_accounts_get_all() != ((GList *)((void *)0))) {
    purple_request_field_account_set_default_value(field,( *purple_accounts_get_all()).data);
    purple_request_field_account_set_value(field,( *purple_accounts_get_all()).data);
  }
  purple_request_field_set_type_hint(field,"account");
  purple_request_field_account_set_show_all(field,(!0));
  purple_request_field_set_visible(field,((purple_accounts_get_all() != ((GList *)((void *)0))) && (( *purple_accounts_get_all()).next != ((GList *)((void *)0)))));
  purple_request_field_set_required(field,(!0));
  purple_request_field_group_add_field(group,field);
  purple_request_fields((purple_get_blist()),((const char *)(dgettext("pidgin","View User Log"))),0,((const char *)(dgettext("pidgin","Please enter the username or alias of the person whose log you would like to view."))),fields,((const char *)(dgettext("pidgin","OK"))),((GCallback )pidgin_dialogs_log_cb),((const char *)(dgettext("pidgin","Cancel"))),0,0,0,0,0);
}

static void pidgin_dialogs_alias_contact_cb(PurpleContact *contact,const char *new_alias)
{
  purple_blist_alias_contact(contact,new_alias);
}

void pidgin_dialogs_alias_contact(PurpleContact *contact)
{
  do {
    if (contact != ((PurpleContact *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"contact != NULL");
      return ;
    };
  }while (0);
  purple_request_input(0,((const char *)(dgettext("pidgin","Alias Contact"))),0,((const char *)(dgettext("pidgin","Enter an alias for this contact."))),(contact -> alias),0,0,0,((const char *)(dgettext("pidgin","Alias"))),((GCallback )pidgin_dialogs_alias_contact_cb),((const char *)(dgettext("pidgin","Cancel"))),0,0,purple_contact_get_alias(contact),0,contact);
}

static void pidgin_dialogs_alias_buddy_cb(PurpleBuddy *buddy,const char *new_alias)
{
  purple_blist_alias_buddy(buddy,new_alias);
  serv_alias_buddy(buddy);
}

void pidgin_dialogs_alias_buddy(PurpleBuddy *buddy)
{
  gchar *secondary;
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return ;
    };
  }while (0);
  secondary = g_strdup_printf(((const char *)(dgettext("pidgin","Enter an alias for %s."))),(buddy -> name));
  purple_request_input(0,((const char *)(dgettext("pidgin","Alias Buddy"))),0,secondary,(buddy -> alias),0,0,0,((const char *)(dgettext("pidgin","Alias"))),((GCallback )pidgin_dialogs_alias_buddy_cb),((const char *)(dgettext("pidgin","Cancel"))),0,purple_buddy_get_account(buddy),purple_buddy_get_name(buddy),0,buddy);
  g_free(secondary);
}

static void pidgin_dialogs_alias_chat_cb(PurpleChat *chat,const char *new_alias)
{
  purple_blist_alias_chat(chat,new_alias);
}

void pidgin_dialogs_alias_chat(PurpleChat *chat)
{
  do {
    if (chat != ((PurpleChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return ;
    };
  }while (0);
  purple_request_input(0,((const char *)(dgettext("pidgin","Alias Chat"))),0,((const char *)(dgettext("pidgin","Enter an alias for this chat."))),(chat -> alias),0,0,0,((const char *)(dgettext("pidgin","Alias"))),((GCallback )pidgin_dialogs_alias_chat_cb),((const char *)(dgettext("pidgin","Cancel"))),0,(chat -> account),0,0,chat);
}

static void pidgin_dialogs_remove_contact_cb(PurpleContact *contact)
{
  PurpleBlistNode *bnode;
  PurpleBlistNode *cnode;
  PurpleGroup *group;
  cnode = ((PurpleBlistNode *)contact);
  group = ((PurpleGroup *)(cnode -> parent));
  for (bnode = (cnode -> child); bnode != 0; bnode = (bnode -> next)) {
    PurpleBuddy *buddy = (PurpleBuddy *)bnode;
    if (purple_account_is_connected((buddy -> account)) != 0) 
      purple_account_remove_buddy((buddy -> account),buddy,group);
  }
  purple_blist_remove_contact(contact);
}

void pidgin_dialogs_remove_contact(PurpleContact *contact)
{
  PurpleBuddy *buddy = purple_contact_get_priority_buddy(contact);
  do {
    if (contact != ((PurpleContact *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"contact != NULL");
      return ;
    };
  }while (0);
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return ;
    };
  }while (0);
  if ((( *((PurpleBlistNode *)contact)).child == ((PurpleBlistNode *)buddy)) && (( *((PurpleBlistNode *)buddy)).next == ((PurpleBlistNode *)((void *)0)))) {
    pidgin_dialogs_remove_buddy(buddy);
  }
  else {
    gchar *text;
    text = g_strdup_printf((ngettext("You are about to remove the contact containing %s and %d other buddy from your buddy list.  Do you want to continue\?","You are about to remove the contact containing %s and %d other buddies from your buddy list.  Do you want to continue\?",((contact -> totalsize) - 1))),(buddy -> name),((contact -> totalsize) - 1));
    purple_request_action(contact,0,((const char *)(dgettext("pidgin","Remove Contact"))),text,0,0,purple_contact_get_alias(contact),0,contact,2,((const char *)(dgettext("pidgin","_Remove Contact"))),((GCallback )pidgin_dialogs_remove_contact_cb),((const char *)(dgettext("pidgin","Cancel"))),((void *)((void *)0)));
    g_free(text);
  }
}

static void free_ggmo(struct _PidginGroupMergeObject *ggp)
{
  g_free((ggp -> new_name));
  g_free(ggp);
}

static void pidgin_dialogs_merge_groups_cb(struct _PidginGroupMergeObject *GGP)
{
  purple_blist_rename_group((GGP -> parent),(GGP -> new_name));
  free_ggmo(GGP);
}

void pidgin_dialogs_merge_groups(PurpleGroup *source,const char *new_name)
{
  gchar *text;
  struct _PidginGroupMergeObject *ggp;
  do {
    if (source != ((PurpleGroup *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"source != NULL");
      return ;
    };
  }while (0);
  do {
    if (new_name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"new_name != NULL");
      return ;
    };
  }while (0);
  text = g_strdup_printf(((const char *)(dgettext("pidgin","You are about to merge the group called %s into the group called %s. Do you want to continue\?"))),(source -> name),new_name);
  ggp = ((struct _PidginGroupMergeObject *)(g_malloc_n(1,(sizeof(struct _PidginGroupMergeObject )))));
  ggp -> parent = source;
  ggp -> new_name = g_strdup(new_name);
  purple_request_action(source,0,((const char *)(dgettext("pidgin","Merge Groups"))),text,0,0,0,0,ggp,2,((const char *)(dgettext("pidgin","_Merge Groups"))),((GCallback )pidgin_dialogs_merge_groups_cb),((const char *)(dgettext("pidgin","Cancel"))),((GCallback )free_ggmo));
  g_free(text);
}

static void pidgin_dialogs_remove_group_cb(PurpleGroup *group)
{
  PurpleBlistNode *cnode;
  PurpleBlistNode *bnode;
  cnode = ( *((PurpleBlistNode *)group)).child;
  while(cnode != 0){
    if ((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CONTACT_NODE) {
      bnode = (cnode -> child);
      cnode = (cnode -> next);
      while(bnode != 0){
        PurpleBuddy *buddy;
        if ((purple_blist_node_get_type(bnode)) == PURPLE_BLIST_BUDDY_NODE) {
          buddy = ((PurpleBuddy *)bnode);
          bnode = (bnode -> next);
          if (purple_account_is_connected((buddy -> account)) != 0) {
            purple_account_remove_buddy((buddy -> account),buddy,group);
            purple_blist_remove_buddy(buddy);
          }
        }
        else {
          bnode = (bnode -> next);
        }
      }
    }
    else if ((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CHAT_NODE) {
      PurpleChat *chat = (PurpleChat *)cnode;
      cnode = (cnode -> next);
      if (purple_account_is_connected((chat -> account)) != 0) 
        purple_blist_remove_chat(chat);
    }
    else {
      cnode = (cnode -> next);
    }
  }
  purple_blist_remove_group(group);
}

void pidgin_dialogs_remove_group(PurpleGroup *group)
{
  gchar *text;
  do {
    if (group != ((PurpleGroup *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"group != NULL");
      return ;
    };
  }while (0);
  text = g_strdup_printf(((const char *)(dgettext("pidgin","You are about to remove the group %s and all its members from your buddy list.  Do you want to continue\?"))),(group -> name));
  purple_request_action(group,0,((const char *)(dgettext("pidgin","Remove Group"))),text,0,0,0,0,group,2,((const char *)(dgettext("pidgin","_Remove Group"))),((GCallback )pidgin_dialogs_remove_group_cb),((const char *)(dgettext("pidgin","Cancel"))),((void *)((void *)0)));
  g_free(text);
}
/* XXX - Some of this should be moved into the core, methinks. */

static void pidgin_dialogs_remove_buddy_cb(PurpleBuddy *buddy)
{
  PurpleGroup *group;
  gchar *name;
  PurpleAccount *account;
  group = purple_buddy_get_group(buddy);
/* b->name is a crasher after remove_buddy */
  name = g_strdup((buddy -> name));
  account = (buddy -> account);
  purple_debug_info("blist","Removing \'%s\' from buddy list.\n",(buddy -> name));
/* TODO - Should remove from blist first... then call purple_account_remove_buddy()? */
  purple_account_remove_buddy(account,buddy,group);
  purple_blist_remove_buddy(buddy);
  g_free(name);
}

void pidgin_dialogs_remove_buddy(PurpleBuddy *buddy)
{
  gchar *text;
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return ;
    };
  }while (0);
  text = g_strdup_printf(((const char *)(dgettext("pidgin","You are about to remove %s from your buddy list.  Do you want to continue\?"))),(buddy -> name));
  purple_request_action(buddy,0,((const char *)(dgettext("pidgin","Remove Buddy"))),text,0,purple_buddy_get_account(buddy),purple_buddy_get_name(buddy),0,buddy,2,((const char *)(dgettext("pidgin","_Remove Buddy"))),((GCallback )pidgin_dialogs_remove_buddy_cb),((const char *)(dgettext("pidgin","Cancel"))),((void *)((void *)0)));
  g_free(text);
}

static void pidgin_dialogs_remove_chat_cb(PurpleChat *chat)
{
  purple_blist_remove_chat(chat);
}

void pidgin_dialogs_remove_chat(PurpleChat *chat)
{
  const gchar *name;
  gchar *text;
  do {
    if (chat != ((PurpleChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return ;
    };
  }while (0);
  name = purple_chat_get_name(chat);
  text = g_strdup_printf(((const char *)(dgettext("pidgin","You are about to remove the chat %s from your buddy list.  Do you want to continue\?"))),((name != 0)?name : ""));
  purple_request_action(chat,0,((const char *)(dgettext("pidgin","Remove Chat"))),text,0,(chat -> account),0,0,chat,2,((const char *)(dgettext("pidgin","_Remove Chat"))),((GCallback )pidgin_dialogs_remove_chat_cb),((const char *)(dgettext("pidgin","Cancel"))),((void *)((void *)0)));
  g_free(text);
}
