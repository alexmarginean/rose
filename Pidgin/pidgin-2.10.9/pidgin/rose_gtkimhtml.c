/*
 * @file gtkimhtml.c GTK+ IMHtml
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#define _PIDGIN_GTKIMHTML_C_
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "internal.h"
#include "pidgin.h"
#include "pidginstock.h"
#include "gtkutils.h"
#include "smiley.h"
#include "imgstore.h"
#include "debug.h"
#include "util.h"
#include "gtkimhtml.h"
#include "gtksmiley.h"
#include "gtksourceiter.h"
#include "gtksourceundomanager.h"
#include "gtksourceview-marshal.h"
#include <gtk/gtk.h>
#include <glib.h>
#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#ifdef HAVE_LANGINFO_CODESET
#include <langinfo.h>
#include <locale.h>
#endif
#ifdef _WIN32
#include <gdk/gdkwin32.h>
#include <windows.h>
#endif
#include <pango/pango-font.h>
#define TOOLTIP_TIMEOUT 500
static GtkTextViewClass *parent_class = (GtkTextViewClass *)((void *)0);

struct scalable_data 
{
  GtkIMHtmlScalable *scalable;
  GtkTextMark *mark;
}
;
typedef struct __unnamed_class___F0_L75_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L1588R__Pe___variable_name_unknown_scope_and_name__scope__image__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L55R_variable_name_unknown_scope_and_name__scope__data__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_gsizeUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__datasize {
GtkIMHtmlScalable *image;
gpointer data;
gsize datasize;}GtkIMHtmlImageSave;

struct im_image_data 
{
  int id;
  GtkTextMark *mark;
}
;

struct _GtkIMHtmlLink 
{
  GtkIMHtml *imhtml;
  gchar *url;
  GtkTextTag *tag;
}
;
typedef struct _GtkIMHtmlProtocol {
char *name;
int length;
gboolean (*activate)(GtkIMHtml *, GtkIMHtmlLink *);
gboolean (*context_menu)(GtkIMHtml *, GtkIMHtmlLink *, GtkWidget *);}GtkIMHtmlProtocol;
typedef struct _GtkIMHtmlFontDetail {
gushort size;
gchar *face;
gchar *fore;
gchar *back;
gchar *bg;
gchar *sml;
gboolean underline;
gboolean strike;
gshort bold;}GtkIMHtmlFontDetail;
static gboolean gtk_text_view_drag_motion(GtkWidget *widget,GdkDragContext *context,gint x,gint y,guint time);
static void preinsert_cb(GtkTextBuffer *buffer,GtkTextIter *iter,gchar *text,gint len,GtkIMHtml *imhtml);
static void insert_cb(GtkTextBuffer *buffer,GtkTextIter *iter,gchar *text,gint len,GtkIMHtml *imhtml);
static void delete_cb(GtkTextBuffer *buffer,GtkTextIter *iter,GtkTextIter *end,GtkIMHtml *imhtml);
static void insert_ca_cb(GtkTextBuffer *buffer,GtkTextIter *arg1,GtkTextChildAnchor *arg2,gpointer user_data);
static void gtk_imhtml_apply_tags_on_insert(GtkIMHtml *imhtml,GtkTextIter *start,GtkTextIter *end);
void gtk_imhtml_close_tags(GtkIMHtml *imhtml,GtkTextIter *iter);
static void gtk_imhtml_link_drop_cb(GtkWidget *widget,GdkDragContext *context,gint x,gint y,guint time,gpointer user_data);
static void gtk_imhtml_link_drag_rcv_cb(GtkWidget *widget,GdkDragContext *dc,guint x,guint y,GtkSelectionData *sd,guint info,guint t,GtkIMHtml *imhtml);
static void mark_set_cb(GtkTextBuffer *buffer,GtkTextIter *arg1,GtkTextMark *mark,GtkIMHtml *imhtml);
static void hijack_menu_cb(GtkIMHtml *imhtml,GtkMenu *menu,gpointer data);
static void paste_received_cb(GtkClipboard *clipboard,GtkSelectionData *selection_data,gpointer data);
static void paste_plaintext_received_cb(GtkClipboard *clipboard,const gchar *text,gpointer data);
static void imhtml_paste_insert(GtkIMHtml *imhtml,const char *text,gboolean plaintext);
static void imhtml_toggle_bold(GtkIMHtml *imhtml);
static void imhtml_toggle_italic(GtkIMHtml *imhtml);
static void imhtml_toggle_strike(GtkIMHtml *imhtml);
static void imhtml_toggle_underline(GtkIMHtml *imhtml);
static void imhtml_font_grow(GtkIMHtml *imhtml);
static void imhtml_font_shrink(GtkIMHtml *imhtml);
static void imhtml_clear_formatting(GtkIMHtml *imhtml);
static int gtk_imhtml_is_protocol(const char *text);
static void gtk_imhtml_activate_tag(GtkIMHtml *imhtml,GtkTextTag *tag);
static void gtk_imhtml_link_destroy(GtkIMHtmlLink *link);
/* POINT_SIZE converts from AIM font sizes to a point size scale factor. */
#define MAX_FONT_SIZE 7
#define POINT_SIZE(x) (_point_sizes [MIN ((x > 0 ? x : 1), MAX_FONT_SIZE) - 1])
static const gdouble _point_sizes[] = {(.85), (.95), (1), (1.2), (1.44), (1.728), (2.0736)};
enum __unnamed_enum___F0_L150_C1_TARGET_HTML__COMMA__TARGET_UTF8_STRING__COMMA__TARGET_COMPOUND_TEXT__COMMA__TARGET_STRING__COMMA__TARGET_TEXT {TARGET_HTML,TARGET_UTF8_STRING,TARGET_COMPOUND_TEXT,TARGET_STRING,TARGET_TEXT};
enum __unnamed_enum___F0_L158_C1_URL_CLICKED__COMMA__BUTTONS_UPDATE__COMMA__TOGGLE_FORMAT__COMMA__CLEAR_FORMAT__COMMA__UPDATE_FORMAT__COMMA__MESSAGE_SEND__COMMA__UNDO__COMMA__REDO__COMMA__PASTE__COMMA__LAST_SIGNAL {URL_CLICKED,BUTTONS_UPDATE,TOGGLE_FORMAT,CLEAR_FORMAT,UPDATE_FORMAT,MESSAGE_SEND,UNDO,REDO,PASTE,LAST_SIGNAL};
static guint signals[9UL] = {(0)};
static char *html_clipboard = (char *)((void *)0);
static char *text_clipboard = (char *)((void *)0);
static GtkClipboard *clipboard_selection = (GtkClipboard *)((void *)0);
static const GtkTargetEntry selection_targets[] = {
#ifndef _WIN32
{("text/html"), (0), (TARGET_HTML)}, 
#else
#endif
{("UTF8_STRING"), (0), (TARGET_UTF8_STRING)}, {("COMPOUND_TEXT"), (0), (TARGET_COMPOUND_TEXT)}, {("STRING"), (0), (TARGET_STRING)}, {("TEXT"), (0), (TARGET_TEXT)}};
static const GtkTargetEntry link_drag_drop_targets[] = {{("text/uri-list"), (0), (GTK_IMHTML_DRAG_URL)}, {("_NETSCAPE_URL"), (0), (GTK_IMHTML_DRAG_URL)}, {("text/html"), (0), (GTK_IMHTML_DRAG_HTML)}, {("x-url/ftp"), (0), (GTK_IMHTML_DRAG_URL)}, {("x-url/http"), (0), (GTK_IMHTML_DRAG_URL)}, {("UTF8_STRING"), (0), (GTK_IMHTML_DRAG_UTF8_STRING)}, {("COMPOUND_TEXT"), (0), (GTK_IMHTML_DRAG_COMPOUND_TEXT)}, {("STRING"), (0), (GTK_IMHTML_DRAG_STRING)}, {("text/plain"), (0), (GTK_IMHTML_DRAG_TEXT)}, {("TEXT"), (0), (GTK_IMHTML_DRAG_TEXT)}};
#ifdef _WIN32
#if 0 /* Debugging for Windows clipboard */
#endif
/* any newlines in the string will now be \r\n, so we need to strip out the \r */
#if 0 /* Debugging for Windows clipboard */
#endif
/* Win32 clipboard format value, and functions to convert back and
	 * forth between HTML and the clipboard format.
	 */
/* Register HTML Format as desired clipboard format */
#endif

static GtkSmileyTree *gtk_smiley_tree_new()
{
  return (GtkSmileyTree *)(g_malloc0_n(1,(sizeof(GtkSmileyTree ))));
}

static void gtk_smiley_tree_insert(GtkSmileyTree *tree,GtkIMHtmlSmiley *smiley)
{
  GtkSmileyTree *t = tree;
  const gchar *x = (smiley -> smile);
  if (!(( *x) != 0)) 
    return ;
  do {
    gchar *pos;
    gint index;
    if (!((t -> values) != 0)) 
      t -> values = g_string_new("");
    pos = strchr(( *(t -> values)).str,( *x));
    if (!(pos != 0)) {
      t -> values = g_string_append_c_inline((t -> values), *x);
      index = (( *(t -> values)).len - 1);
      t -> children = (g_realloc((t -> children),(( *(t -> values)).len * sizeof(GtkSmileyTree *))));
      (t -> children)[index] = ((GtkSmileyTree *)(g_malloc0_n(1,(sizeof(GtkSmileyTree )))));
    }
    else 
      index = (((gint )((glong )pos)) - ((gint )((glong )( *(t -> values)).str)));
    t = (t -> children)[index];
    x++;
  }while (( *x) != 0);
  t -> image = smiley;
}

static void gtk_smiley_tree_destroy(GtkSmileyTree *tree)
{
  GSList *list = g_slist_prepend(0,tree);
  while(list != 0){
    GtkSmileyTree *t = (list -> data);
    gsize i;
    list = g_slist_remove(list,t);
    if ((t != 0) && ((t -> values) != 0)) {
      for (i = 0; i < ( *(t -> values)).len; i++) 
        list = g_slist_prepend(list,(t -> children)[i]);
      g_string_free((t -> values),(!0));
      g_free((t -> children));
    }
    g_free(t);
  }
}
static void (*parent_size_allocate)(GtkWidget *, GtkAllocation *);

static void gtk_imhtml_size_allocate(GtkWidget *widget,GtkAllocation *alloc)
{
  GtkIMHtml *imhtml = (GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_imhtml_get_type()));
  GdkRectangle rect;
  int xminus;
  int height = 0;
  int y = 0;
  GtkTextIter iter;
  gboolean scroll = (!0);
  gtk_text_buffer_get_end_iter((imhtml -> text_buffer),&iter);
  gtk_text_view_get_visible_rect(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type()))),&rect);
  gtk_text_view_get_line_yrange(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),(&iter),&y,&height);
  if ((((y + height) - (rect.y + rect.height)) > height) && (gtk_text_buffer_get_char_count((imhtml -> text_buffer)) != 0)) {
    scroll = 0;
  }
  if ((imhtml -> old_rect.width != rect.width) || (imhtml -> old_rect.height != rect.height)) {
    GList *iter = ( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_imhtml_get_type())))).scalables;
    xminus = (gtk_text_view_get_left_margin(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type())))) + gtk_text_view_get_right_margin(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type())))));
    while(iter != 0){
      struct scalable_data *sd = (iter -> data);
      GtkIMHtmlScalable *scale = (GtkIMHtmlScalable *)(sd -> scalable);
      ( *(scale -> scale))(scale,(rect.width - xminus),rect.height);
      iter = (iter -> next);
    }
  }
  imhtml -> old_rect = rect;
  ( *parent_size_allocate)(widget,alloc);
/* Don't scroll here if we're in the middle of a smooth scroll */
  if (((scroll != 0) && ((imhtml -> scroll_time) == ((GTimer *)((void *)0)))) && ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_object_get_type())))).flags & GTK_REALIZED) != 0)) 
    gtk_imhtml_scroll_to_end(imhtml,0);
}
#define DEFAULT_SEND_COLOR "#204a87"
#define DEFAULT_RECV_COLOR "#cc0000"
#define DEFAULT_HIGHLIGHT_COLOR "#AF7F00"
#define DEFAULT_ACTION_COLOR "#062585"
#define DEFAULT_WHISPER_ACTION_COLOR "#6C2585"
#define DEFAULT_WHISPER_COLOR "#00FF00"
static void (*parent_style_set)(GtkWidget *, GtkStyle *);

static void gtk_imhtml_style_set(GtkWidget *widget,GtkStyle *prev_style)
{
  int i;
  struct __unnamed_class___F0_L442_C2_L1642R__L1643R__scope____SgSS2___variable_declaration__variable_type___Pb__Cc__Pe___variable_name_L1642R__L1643R__scope____SgSS2____scope__tag__DELIMITER__L1642R__L1643R__scope____SgSS2___variable_declaration__variable_type___Pb__Cc__Pe___variable_name_L1642R__L1643R__scope____SgSS2____scope__color__DELIMITER__L1642R__L1643R__scope____SgSS2___variable_declaration__variable_type___Pb__Cc__Pe___variable_name_L1642R__L1643R__scope____SgSS2____scope__def {
  const char *tag;
  const char *color;
  const char *def;}styles[] = {{("send-name"), ("send-name-color"), ("#204a87")}, {("receive-name"), ("receive-name-color"), ("#cc0000")}, {("highlight-name"), ("highlight-name-color"), ("#AF7F00")}, {("action-name"), ("action-name-color"), ("#062585")}, {("whisper-action-name"), ("whisper-action-name-color"), ("#6C2585")}, {("whisper-name"), ("whisper-name-color"), ("#00FF00")}, {((const char *)((void *)0)), ((const char *)((void *)0)), ((const char *)((void *)0))}};
  GtkIMHtml *imhtml = (GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_imhtml_get_type()));
  GtkTextTagTable *table = gtk_text_buffer_get_tag_table((imhtml -> text_buffer));
  for (i = 0; styles[i].tag != 0; i++) {{
      GdkColor *color = (GdkColor *)((void *)0);
      GtkTextTag *tag = gtk_text_tag_table_lookup(table,styles[i].tag);
      if (!(tag != 0)) {
        purple_debug_warning("gtkimhtml","Cannot find tag \'%s\'. This should never happen. Please file a bug.\n",styles[i].tag);
        continue; 
      }
      gtk_widget_style_get(widget,styles[i].color,&color,((void *)((void *)0)));
      if (color != 0) {
        g_object_set(tag,"foreground-gdk",color,((void *)((void *)0)));
        gdk_color_free(color);
      }
      else {
        GdkColor defcolor;
        gdk_color_parse(styles[i].def,&defcolor);
        g_object_set(tag,"foreground-gdk",&defcolor,((void *)((void *)0)));
      }
    }
  }
  ( *parent_style_set)(widget,prev_style);
}

static gboolean imhtml_get_iter_bounds(GtkIMHtml *imhtml,GtkTextIter *start,GtkTextIter *end)
{
  if ((imhtml -> wbfo) != 0) {
    gtk_text_buffer_get_bounds((imhtml -> text_buffer),start,end);
    return (!0);
  }
  else if ((imhtml -> editable) != 0) {
    if (!(gtk_text_buffer_get_selection_bounds((imhtml -> text_buffer),start,end) != 0)) {
      GtkTextMark *mark = gtk_text_buffer_get_insert((imhtml -> text_buffer));
      gtk_text_buffer_get_iter_at_mark((imhtml -> text_buffer),start,mark);
       *end =  *start;
    }
    return (!0);
  }
  return 0;
}

static void gtk_imhtml_set_link_color(GtkIMHtml *imhtml,GtkTextTag *tag)
{
  GdkColor *color = (GdkColor *)((void *)0);
  gboolean visited = !(!(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))),"visited") != 0));
  gtk_widget_style_get(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type()))),((visited != 0)?"hyperlink-visited-color" : "hyperlink-color"),&color,((void *)((void *)0)));
  if (color != 0) {
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))),"foreground-gdk",color,((void *)((void *)0)));
    gdk_color_free(color);
  }
  else {
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))),"foreground",((visited != 0)?"#800000" : "blue"),((void *)((void *)0)));
  }
}

static gint gtk_imhtml_tip_paint(GtkIMHtml *imhtml)
{
  PangoLayout *layout;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)imhtml;
      GType __t = gtk_imhtml_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_IMHTML(imhtml)");
      return 0;
    };
  }while (0);
/* We set the text in a separate function call so we can specify a
	   max length.  This is important so the tooltip isn't too wide for
	   the screen, and also because some X library function exits the
	   process when it can't allocate enough memory for a super wide
	   tooltip. */
  layout = gtk_widget_create_pango_layout((imhtml -> tip_window),0);
  pango_layout_set_text(layout,(imhtml -> tip),200);
  gtk_paint_flat_box(( *(imhtml -> tip_window)).style,( *(imhtml -> tip_window)).window,GTK_STATE_NORMAL,GTK_SHADOW_OUT,0,(imhtml -> tip_window),"tooltip",0,0,(-1),(-1));
  gtk_paint_layout(( *(imhtml -> tip_window)).style,( *(imhtml -> tip_window)).window,GTK_STATE_NORMAL,0,0,(imhtml -> tip_window),0,4,4,layout);
  g_object_unref(layout);
  return 0;
}

static gint gtk_imhtml_tip(gpointer data)
{
  GtkIMHtml *imhtml = data;
  PangoFontMetrics *font_metrics;
  PangoLayout *layout;
  PangoFont *font;
  gint gap;
  gint x;
  gint y;
  gint h;
  gint w;
  gint scr_w;
  gint baseline_skip;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)imhtml;
      GType __t = gtk_imhtml_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_IMHTML(imhtml)");
      return 0;
    };
  }while (0);
  if (!((imhtml -> tip) != 0) || !(((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type())))),gtk_object_get_type())))).flags & GTK_VISIBLE) != 0) && ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type())))),gtk_object_get_type())))).flags & GTK_MAPPED) != 0))) {
    imhtml -> tip_timer = 0;
    return 0;
  }
  if ((imhtml -> tip_window) != 0) {
    gtk_widget_destroy((imhtml -> tip_window));
    imhtml -> tip_window = ((GtkWidget *)((void *)0));
  }
  imhtml -> tip_timer = 0;
  imhtml -> tip_window = gtk_window_new(GTK_WINDOW_POPUP);
  gtk_widget_set_app_paintable((imhtml -> tip_window),(!0));
  gtk_window_set_title(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(imhtml -> tip_window)),gtk_window_get_type()))),"GtkIMHtml");
  gtk_window_set_resizable(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(imhtml -> tip_window)),gtk_window_get_type()))),0);
  gtk_widget_set_name((imhtml -> tip_window),"gtk-tooltips");
  gtk_window_set_type_hint(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(imhtml -> tip_window)),gtk_window_get_type()))),GDK_WINDOW_TYPE_HINT_TOOLTIP);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(imhtml -> tip_window)),((GType )(20 << 2))))),"expose_event",((GCallback )gtk_imhtml_tip_paint),imhtml,0,G_CONNECT_SWAPPED);
  gtk_widget_ensure_style((imhtml -> tip_window));
/* We set the text in a separate function call so we can specify a
	   max length.  This is important so the tooltip isn't too wide for
	   the screen, and also because some X library function exits the
	   process when it can't allocate enough memory for a super wide
	   tooltip. */
  layout = gtk_widget_create_pango_layout((imhtml -> tip_window),0);
  pango_layout_set_text(layout,(imhtml -> tip),200);
  font = pango_context_load_font(pango_layout_get_context(layout),( *( *(imhtml -> tip_window)).style).font_desc);
  if (font == ((PangoFont *)((void *)0))) {
    char *tmp = pango_font_description_to_string(( *( *(imhtml -> tip_window)).style).font_desc);
    purple_debug(PURPLE_DEBUG_ERROR,"gtk_imhtml_tip","pango_context_load_font() couldn\'t load font: \'%s\'\n",tmp);
    g_free(tmp);
    g_object_unref(layout);
    return 0;
  }
  font_metrics = pango_font_get_metrics(font,0);
  pango_layout_get_pixel_size(layout,&scr_w,0);
  gap = ((((pango_font_metrics_get_ascent(font_metrics) + pango_font_metrics_get_descent(font_metrics)) / 4) + 512) >> 10);
  if (gap < 2) 
    gap = 2;
  baseline_skip = (((pango_font_metrics_get_ascent(font_metrics) + pango_font_metrics_get_descent(font_metrics)) + 512) >> 10);
  w = (8 + scr_w);
  h = (8 + baseline_skip);
  gdk_window_get_pointer(0,&x,&y,0);
  if ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type())))),gtk_object_get_type())))).flags & GTK_NO_WINDOW) != 0) 
    y += ( *((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type())))).allocation.y;
  scr_w = gdk_screen_width();
  x -= ((w >> 1) + 4);
  if ((x + w) > scr_w) 
    x -= ((x + w) - scr_w);
  else if (x < 0) 
    x = 0;
  y = (y + (((pango_font_metrics_get_ascent(font_metrics) + pango_font_metrics_get_descent(font_metrics)) + 512) >> 10));
  gtk_widget_set_size_request((imhtml -> tip_window),w,h);
  gtk_window_move(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(imhtml -> tip_window)),gtk_window_get_type()))),x,y);
  gtk_widget_show((imhtml -> tip_window));
  pango_font_metrics_unref(font_metrics);
  g_object_unref(font);
  g_object_unref(layout);
  return 0;
}

static gboolean gtk_motion_event_notify(GtkWidget *imhtml,GdkEventMotion *event,gpointer data)
{
  GtkTextIter iter;
  GdkWindow *win = (event -> window);
  int x;
  int y;
  char *tip = (char *)((void *)0);
  GSList *tags = (GSList *)((void *)0);
  GSList *templist = (GSList *)((void *)0);
  GtkTextTag *tag = (GtkTextTag *)((void *)0);
  GtkTextTag *oldprelit_tag;
  GtkTextChildAnchor *anchor;
  gboolean hand = (!0);
  GdkCursor *cursor = (GdkCursor *)((void *)0);
  oldprelit_tag = ( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).prelit_tag;
  gdk_window_get_pointer(( *((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type())))).window,0,0,0);
  gtk_text_view_window_to_buffer_coords(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),GTK_TEXT_WINDOW_WIDGET,(event -> x),(event -> y),&x,&y);
  gtk_text_view_get_iter_at_location(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),&iter,x,y);
  tags = gtk_text_iter_get_tags((&iter));
  templist = tags;
{
    while(templist != 0){
      tag = (templist -> data);
      tip = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))),"link_url"));
      if (tip != 0) 
        break; 
      templist = (templist -> next);
    }
  }
  if ((tip != 0) && (!(tag != 0) || !(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))),"visited") != 0))) {
    ( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).prelit_tag = tag;
    if (tag != oldprelit_tag) {
      GdkColor *pre = (GdkColor *)((void *)0);
      gtk_widget_style_get(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type()))),"hyperlink-prelight-color",&pre,((void *)((void *)0)));
      if (pre != 0) {
        g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))),"foreground-gdk",pre,((void *)((void *)0)));
        gdk_color_free(pre);
      }
      else 
        g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))),"foreground","#70a0ff",((void *)((void *)0)));
    }
  }
  else {
    ( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).prelit_tag = ((GtkTextTag *)((void *)0));
  }
  if ((oldprelit_tag != ((GtkTextTag *)((void *)0))) && (( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).prelit_tag != oldprelit_tag)) {
    gtk_imhtml_set_link_color(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),oldprelit_tag);
  }
  if (( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip != 0) {
    if (tip == ( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip) {
      g_slist_free(tags);
      return 0;
    }
/* We've left the cell.  Remove the timeout and create a new one below */
    if (( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip_window != 0) {
      gtk_widget_destroy(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip_window);
      ( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip_window = ((GtkWidget *)((void *)0));
    }
    if (( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).editable != 0) 
      cursor = ( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).text_cursor;
    else 
      cursor = ( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).arrow_cursor;
    if (( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip_timer != 0U) 
      g_source_remove(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip_timer);
    ( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip_timer = 0;
  }
/* If we don't have a tip from a URL, let's see if we have a tip from a smiley */
  anchor = gtk_text_iter_get_child_anchor((&iter));
  if (anchor != 0) {
    tip = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)anchor),((GType )(20 << 2))))),"gtkimhtml_tiptext"));
    hand = 0;
  }
  if ((tip != 0) && (( *tip) != 0)) {
    ( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip_timer = g_timeout_add(500,gtk_imhtml_tip,imhtml);
  }
  else if (!(tip != 0)) {
    hand = 0;
{
      for (templist = tags; templist != 0; templist = (templist -> next)) {
        tag = (templist -> data);
        if ((tip = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))),"cursor"))) != 0) {
          hand = (!0);
          break; 
        }
      }
    }
  }
  if ((hand != 0) && !(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).editable != 0)) 
    cursor = ( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).hand_cursor;
  if (cursor != 0) 
    gdk_window_set_cursor(win,cursor);
  ( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip = tip;
  g_slist_free(tags);
  return 0;
}

static gboolean gtk_enter_event_notify(GtkWidget *imhtml,GdkEventCrossing *event,gpointer data)
{
  if (( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).editable != 0) 
    gdk_window_set_cursor(gtk_text_view_get_window(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),GTK_TEXT_WINDOW_TEXT),( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).text_cursor);
  else 
    gdk_window_set_cursor(gtk_text_view_get_window(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),GTK_TEXT_WINDOW_TEXT),( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).arrow_cursor);
/* propagate the event normally */
  return 0;
}

static gboolean gtk_leave_event_notify(GtkWidget *imhtml,GdkEventCrossing *event,gpointer data)
{
/* when leaving the widget, clear any current & pending tooltips and restore the cursor */
  if (( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).prelit_tag != 0) {
    gtk_imhtml_set_link_color(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).prelit_tag);
    ( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).prelit_tag = ((GtkTextTag *)((void *)0));
  }
  if (( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip_window != 0) {
    gtk_widget_destroy(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip_window);
    ( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip_window = ((GtkWidget *)((void *)0));
  }
  if (( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip_timer != 0U) {
    g_source_remove(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip_timer);
    ( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip_timer = 0;
  }
  gdk_window_set_cursor(gtk_text_view_get_window(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),GTK_TEXT_WINDOW_TEXT),0);
/* propagate the event normally */
  return 0;
}

static gint gtk_imhtml_expose_event(GtkWidget *widget,GdkEventExpose *event)
{
  GtkTextIter start;
  GtkTextIter end;
  GtkTextIter cur;
  int buf_x;
  int buf_y;
  GdkRectangle visible_rect;
  cairo_t *cr = gdk_cairo_create(((GdkDrawable *)(g_type_check_instance_cast(((GTypeInstance *)(event -> window)),gdk_drawable_get_type()))));
  GdkColor gcolor;
  gtk_text_view_get_visible_rect(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type()))),&visible_rect);
  gtk_text_view_buffer_to_window_coords(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type()))),GTK_TEXT_WINDOW_TEXT,visible_rect.x,visible_rect.y,&visible_rect.x,&visible_rect.y);
  gtk_text_view_window_to_buffer_coords(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type()))),GTK_TEXT_WINDOW_TEXT,event -> area.x,event -> area.y,&buf_x,&buf_y);
  if ((( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_imhtml_get_type())))).editable != 0) || (( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_imhtml_get_type())))).wbfo != 0)) {
    if (( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_imhtml_get_type())))).edit.background != 0) {
      gdk_color_parse(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_imhtml_get_type())))).edit.background,&gcolor);
      gdk_cairo_set_source_color(cr,(&gcolor));
    }
    else {
      gdk_cairo_set_source_color(cr,(( *(widget -> style)).base + ( *((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_widget_get_type())))).state));
    }
    cairo_rectangle(cr,visible_rect.x,visible_rect.y,visible_rect.width,visible_rect.height);
    cairo_fill(cr);
    cairo_destroy(cr);
    if (( *((GtkWidgetClass *)(g_type_check_class_cast(((GTypeClass *)parent_class),gtk_widget_get_type())))).expose_event != 0) 
      return ( *( *((GtkWidgetClass *)(g_type_check_class_cast(((GTypeClass *)parent_class),gtk_widget_get_type())))).expose_event)(widget,event);
    return 0;
  }
  gtk_text_view_get_iter_at_location(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type()))),&start,buf_x,buf_y);
  gtk_text_view_get_iter_at_location(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type()))),&end,(buf_x + event -> area.width),(buf_y + event -> area.height));
  gtk_text_iter_order(&start,&end);
  cur = start;
  while(gtk_text_iter_in_range((&cur),(&start),(&end)) != 0){
    GSList *tags = gtk_text_iter_get_tags((&cur));
    GSList *l;
{
      for (l = tags; l != 0; l = (l -> next)) {{
          GtkTextTag *tag = (l -> data);
          GdkRectangle rect;
          GdkRectangle tag_area;
          const char *color;
          if (strncmp((tag -> name),"BACKGROUND ",11) != 0) 
            continue; 
          if (gtk_text_iter_ends_tag((&cur),tag) != 0) 
            continue; 
          gtk_text_view_get_iter_location(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type()))),(&cur),&tag_area);
          gtk_text_view_buffer_to_window_coords(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type()))),GTK_TEXT_WINDOW_TEXT,tag_area.x,tag_area.y,&tag_area.x,&tag_area.y);
          rect.x = visible_rect.x;
          rect.y = tag_area.y;
          rect.width = visible_rect.width;
          do 
            gtk_text_iter_forward_to_tag_toggle(&cur,tag);while (!(gtk_text_iter_is_end((&cur)) != 0) && (gtk_text_iter_begins_tag((&cur),tag) != 0));
          gtk_text_view_get_iter_location(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type()))),(&cur),&tag_area);
          gtk_text_view_buffer_to_window_coords(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type()))),GTK_TEXT_WINDOW_TEXT,tag_area.x,tag_area.y,&tag_area.x,&tag_area.y);
          rect.height = ((((tag_area.y + tag_area.height) - rect.y) + gtk_text_view_get_pixels_above_lines(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type()))))) + gtk_text_view_get_pixels_below_lines(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type())))));
          color = ((tag -> name) + 11);
          if (!(gdk_color_parse(color,&gcolor) != 0)) {
            gchar tmp[8UL];
            tmp[0] = '#';
            strncpy((tmp + 1),color,7);
            tmp[7] = 0;
            if (!(gdk_color_parse(tmp,&gcolor) != 0)) 
              gdk_color_parse("white",&gcolor);
          }
          gdk_cairo_set_source_color(cr,(&gcolor));
          cairo_rectangle(cr,rect.x,rect.y,rect.width,rect.height);
          cairo_fill(cr);
/* go back one, in case the end is the begining is the end
			                                    * note that above, we always moved cur ahead by at least
			                                    * one character */
          gtk_text_iter_backward_char(&cur);
          break; 
        }
      }
    }
    g_slist_free(tags);
/* loop until another tag begins, or no tag begins */
    while(((gtk_text_iter_forward_to_tag_toggle(&cur,0) != 0) && !(gtk_text_iter_is_end((&cur)) != 0)) && !(gtk_text_iter_begins_tag((&cur),0) != 0));
  }
  cairo_destroy(cr);
  if (( *((GtkWidgetClass *)(g_type_check_class_cast(((GTypeClass *)parent_class),gtk_widget_get_type())))).expose_event != 0) 
    return ( *( *((GtkWidgetClass *)(g_type_check_class_cast(((GTypeClass *)parent_class),gtk_widget_get_type())))).expose_event)(widget,event);
  return 0;
}

static void paste_unformatted_cb(GtkMenuItem *menu,GtkIMHtml *imhtml)
{
  GtkClipboard *clipboard = gtk_widget_get_clipboard(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type()))),((GdkAtom )((GdkAtom )((gpointer )((gulong )69)))));
  gtk_clipboard_request_text(clipboard,paste_plaintext_received_cb,imhtml);
}

static void clear_formatting_cb(GtkMenuItem *menu,GtkIMHtml *imhtml)
{
  gtk_imhtml_clear_formatting(imhtml);
}

static void disable_smiley_selected(GtkMenuItem *item,GtkIMHtml *imhtml)
{
  GtkTextIter start;
  GtkTextIter end;
  GtkTextMark *mark;
  char *text;
  if (!(gtk_text_buffer_get_selection_bounds((imhtml -> text_buffer),&start,&end) != 0)) 
    return ;
  text = gtk_imhtml_get_markup_range(imhtml,&start,&end);
  mark = gtk_text_buffer_get_selection_bound((imhtml -> text_buffer));
  gtk_text_buffer_delete_selection((imhtml -> text_buffer),0,0);
  gtk_text_buffer_get_iter_at_mark((imhtml -> text_buffer),&start,mark);
  gtk_imhtml_insert_html_at_iter(imhtml,text,(GTK_IMHTML_NO_NEWLINE | GTK_IMHTML_NO_SMILEY),&start);
  g_free(text);
}

static void hijack_menu_cb(GtkIMHtml *imhtml,GtkMenu *menu,gpointer data)
{
  GtkWidget *menuitem;
  GtkTextIter start;
  GtkTextIter end;
  menuitem = gtk_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","Paste as Plain _Text"))));
  gtk_widget_show(menuitem);
/*
	 * TODO: gtk_clipboard_wait_is_text_available() iterates the glib
	 *       mainloop, which tends to be a source of bugs.  It would
	 *       be good to audit this or change it to not wait.
	 */
  gtk_widget_set_sensitive(menuitem,(((imhtml -> editable) != 0) && (gtk_clipboard_wait_is_text_available(gtk_widget_get_clipboard(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type()))),((GdkAtom )((GdkAtom )((gpointer )((gulong )69)))))) != 0)));
/* put it after "Paste" */
  gtk_menu_shell_insert(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem,3);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"activate",((GCallback )paste_unformatted_cb),imhtml,0,((GConnectFlags )0));
  menuitem = gtk_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","_Reset formatting"))));
  gtk_widget_show(menuitem);
  gtk_widget_set_sensitive(menuitem,(imhtml -> editable));
/* put it after Delete */
  gtk_menu_shell_insert(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem,5);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"activate",((GCallback )clear_formatting_cb),imhtml,0,((GConnectFlags )0));
  menuitem = gtk_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","Disable _smileys in selected text"))));
  gtk_widget_show(menuitem);
  if (gtk_text_buffer_get_selection_bounds((imhtml -> text_buffer),&start,&end) != 0) {
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"activate",((GCallback )disable_smiley_selected),imhtml,0,((GConnectFlags )0));
  }
  else {
    gtk_widget_set_sensitive(menuitem,0);
  }
  gtk_menu_shell_insert(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem,6);
}

static char *ucs2_order(gboolean swap)
{
  gboolean be;
  be = (1234 == 4321);
  be = ((swap != 0)?be : !(be != 0));
  if (be != 0) 
    return "UTF-16BE";
  else 
    return "UTF-16LE";
}
/* Convert from UTF-16LE to UTF-8, stripping the BOM if one is present.*/

static gchar *utf16_to_utf8_with_bom_check(gchar *data,guint len)
{
  char *fromcode = (char *)((void *)0);
  GError *error = (GError *)((void *)0);
  guint16 c;
  gchar *utf8_ret;
/*
	 * Unicode Techinical Report 20
	 * ( http://www.unicode.org/unicode/reports/tr20/ ) says to treat an
	 * initial 0xfeff (ZWNBSP) as a byte order indicator so that is
	 * what we do.  If there is no indicator assume it is in the default
	 * order
	 */
  memcpy((&c),data,2);
  switch(c){
    case 0xfeff:
{
    }
    case 0xfffe:
{
      fromcode = ucs2_order((c == 0xfeff));
      data += 2;
      len -= 2;
      break; 
    }
    default:
{
      fromcode = "UTF-16";
      break; 
    }
  }
  utf8_ret = g_convert(data,len,"UTF-8",fromcode,0,0,&error);
  if (error != 0) {
    purple_debug_warning("gtkimhtml","g_convert error: %s\n",(error -> message));
    g_error_free(error);
  }
  return utf8_ret;
}

static void gtk_imhtml_clipboard_get(GtkClipboard *clipboard,GtkSelectionData *selection_data,guint info,GtkIMHtml *imhtml)
{
  char *text = (char *)((void *)0);
  gboolean primary = (clipboard != clipboard_selection);
  GtkTextIter start;
  GtkTextIter end;
  if (primary != 0) {
    GtkTextMark *sel = (GtkTextMark *)((void *)0);
    GtkTextMark *ins = (GtkTextMark *)((void *)0);
    do {
      if (imhtml != ((GtkIMHtml *)((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"imhtml != NULL");
        return ;
      };
    }while (0);
    ins = gtk_text_buffer_get_insert((imhtml -> text_buffer));
    sel = gtk_text_buffer_get_selection_bound((imhtml -> text_buffer));
    gtk_text_buffer_get_iter_at_mark((imhtml -> text_buffer),&start,sel);
    gtk_text_buffer_get_iter_at_mark((imhtml -> text_buffer),&end,ins);
  }
  if (info == TARGET_HTML) {
    char *selection;
#ifndef _WIN32
    gsize len;
    if (primary != 0) {
      text = gtk_imhtml_get_markup_range(imhtml,&start,&end);
    }
    else 
      text = html_clipboard;
/* Mozilla asks that we start our text/html with the Unicode byte order mark */
    selection = g_convert(text,(-1),"UTF-16","UTF-8",0,&len,0);
    gtk_selection_data_set(selection_data,gdk_atom_intern("text/html",0),16,((const guchar *)selection),len);
#else
#endif
    g_free(selection);
  }
  else {
    if (primary != 0) {
      text = gtk_imhtml_get_text(imhtml,&start,&end);
    }
    else 
      text = text_clipboard;
    gtk_selection_data_set_text(selection_data,text,(strlen(text)));
  }
/* This was allocated here */
  if (primary != 0) 
    g_free(text);
}

static void gtk_imhtml_primary_clipboard_clear(GtkClipboard *clipboard,GtkIMHtml *imhtml)
{
  GtkTextIter insert;
  GtkTextIter selection_bound;
  gtk_text_buffer_get_iter_at_mark((imhtml -> text_buffer),&insert,gtk_text_buffer_get_mark((imhtml -> text_buffer),"insert"));
  gtk_text_buffer_get_iter_at_mark((imhtml -> text_buffer),&selection_bound,gtk_text_buffer_get_mark((imhtml -> text_buffer),"selection_bound"));
  if (!(gtk_text_iter_equal((&insert),(&selection_bound)) != 0)) 
    gtk_text_buffer_move_mark((imhtml -> text_buffer),gtk_text_buffer_get_mark((imhtml -> text_buffer),"selection_bound"),(&insert));
}

static void gtk_imhtml_clipboard_clear(GtkClipboard *clipboard,GtkSelectionData *sel_data,guint info,gpointer user_data_or_owner)
{
}

static void copy_clipboard_cb(GtkIMHtml *imhtml,gpointer unused)
{
  GtkTextIter start;
  GtkTextIter end;
  if (gtk_text_buffer_get_selection_bounds((imhtml -> text_buffer),&start,&end) != 0) {
    if (!(clipboard_selection != 0)) 
      clipboard_selection = gtk_widget_get_clipboard(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type()))),((GdkAtom )((GdkAtom )((gpointer )((gulong )69)))));
    gtk_clipboard_set_with_data(clipboard_selection,selection_targets,(sizeof(selection_targets) / sizeof(GtkTargetEntry )),((GtkClipboardGetFunc )gtk_imhtml_clipboard_get),((GtkClipboardClearFunc )gtk_imhtml_clipboard_clear),0);
    g_free(html_clipboard);
    g_free(text_clipboard);
    html_clipboard = gtk_imhtml_get_markup_range(imhtml,&start,&end);
    text_clipboard = gtk_imhtml_get_text(imhtml,&start,&end);
  }
  g_signal_stop_emission_by_name(imhtml,"copy-clipboard");
}

static void cut_clipboard_cb(GtkIMHtml *imhtml,gpointer unused)
{
  GtkTextIter start;
  GtkTextIter end;
  if (gtk_text_buffer_get_selection_bounds((imhtml -> text_buffer),&start,&end) != 0) {
    if (!(clipboard_selection != 0)) 
      clipboard_selection = gtk_widget_get_clipboard(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type()))),((GdkAtom )((GdkAtom )((gpointer )((gulong )69)))));
    gtk_clipboard_set_with_data(clipboard_selection,selection_targets,(sizeof(selection_targets) / sizeof(GtkTargetEntry )),((GtkClipboardGetFunc )gtk_imhtml_clipboard_get),((GtkClipboardClearFunc )gtk_imhtml_clipboard_clear),0);
    g_free(html_clipboard);
    g_free(text_clipboard);
    html_clipboard = gtk_imhtml_get_markup_range(imhtml,&start,&end);
    text_clipboard = gtk_imhtml_get_text(imhtml,&start,&end);
    if ((imhtml -> editable) != 0) 
      gtk_text_buffer_delete_selection((imhtml -> text_buffer),0,0);
  }
  g_signal_stop_emission_by_name(imhtml,"cut-clipboard");
}

static void imhtml_paste_insert(GtkIMHtml *imhtml,const char *text,gboolean plaintext)
{
  GtkTextIter iter;
  GtkIMHtmlOptions flags = ((plaintext != 0)?GTK_IMHTML_NO_SMILEY : GTK_IMHTML_NO_NEWLINE | GTK_IMHTML_NO_COMMENTS);
/* Delete any currently selected text */
  gtk_text_buffer_delete_selection((imhtml -> text_buffer),(!0),(!0));
  gtk_text_buffer_get_iter_at_mark((imhtml -> text_buffer),&iter,gtk_text_buffer_get_insert((imhtml -> text_buffer)));
  if (!((imhtml -> wbfo) != 0) && !(plaintext != 0)) 
    gtk_imhtml_close_tags(imhtml,&iter);
  gtk_imhtml_insert_html_at_iter(imhtml,text,flags,&iter);
  gtk_text_buffer_move_mark_by_name((imhtml -> text_buffer),"insert",(&iter));
  gtk_text_view_scroll_to_mark(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),gtk_text_buffer_get_insert((imhtml -> text_buffer)),0,0,0.0,0.0);
  if (!((imhtml -> wbfo) != 0) && !(plaintext != 0)) 
    gtk_imhtml_close_tags(imhtml,&iter);
}

static void paste_plaintext_received_cb(GtkClipboard *clipboard,const gchar *text,gpointer data)
{
  char *tmp;
  if ((text == ((const gchar *)((void *)0))) || !(( *text) != 0)) 
    return ;
  tmp = g_markup_escape_text(text,(-1));
  imhtml_paste_insert(data,tmp,(!0));
  g_free(tmp);
}

static void paste_received_cb(GtkClipboard *clipboard,GtkSelectionData *selection_data,gpointer data)
{
  char *text;
  GtkIMHtml *imhtml = data;
  if (!(gtk_text_view_get_editable(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type())))) != 0)) 
    return ;
  if (((imhtml -> wbfo) != 0) || ((selection_data -> length) <= 0)) {
    gtk_clipboard_request_text(clipboard,paste_plaintext_received_cb,imhtml);
    return ;
  }
  else {
#if 0
/* Here's some debug code, for figuring out what sent to us over the clipboard. */
/*(selection_data->format / 8) **/
#endif
    text = (g_malloc(((selection_data -> length) + 1)));
    memcpy(text,(selection_data -> data),(selection_data -> length));
/* Make sure the paste data is null-terminated.  Given that
		 * we're passed length (but assume later that it is
		 * null-terminated), this seems sensible to me.
		 */
    text[selection_data -> length] = 0;
  }
#ifdef _WIN32
#endif
  if (((selection_data -> length) >= 2) && ((( *((guint16 *)text)) == 0xfeff) || (( *((guint16 *)text)) == 0xfffe))) {
/* This is UTF-16 */
    char *utf8 = utf16_to_utf8_with_bom_check(text,(selection_data -> length));
    g_free(text);
    text = utf8;
    if (!(text != 0)) {
      purple_debug_warning("gtkimhtml","g_convert from UTF-16 failed in paste_received_cb\n");
      return ;
    }
  }
  if (!(( *text) != 0) || !(g_utf8_validate(text,(-1),0) != 0)) {
    purple_debug_warning("gtkimhtml","empty string or invalid UTF-8 in paste_received_cb\n");
    g_free(text);
    return ;
  }
  imhtml_paste_insert(imhtml,text,0);
  g_free(text);
}

static void smart_backspace_cb(GtkIMHtml *imhtml,gpointer blah)
{
  GtkTextIter iter;
  GtkTextChildAnchor *anchor;
  char *text;
  gint offset;
  if (!((imhtml -> editable) != 0)) 
    return ;
  gtk_text_buffer_get_iter_at_mark((imhtml -> text_buffer),&iter,gtk_text_buffer_get_insert((imhtml -> text_buffer)));
/* Get the character before the insertion point */
  offset = gtk_text_iter_get_offset((&iter));
  if (offset <= 0) 
    return ;
  gtk_text_iter_backward_char(&iter);
  anchor = gtk_text_iter_get_child_anchor((&iter));
  if (!(anchor != 0)) 
/* No smiley here */
    return ;
  text = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)anchor),((GType )(20 << 2))))),"gtkimhtml_plaintext"));
  if (!(text != 0)) 
    return ;
/* ok, then we need to insert the image buffer text before the anchor */
  gtk_text_buffer_insert((imhtml -> text_buffer),&iter,text,(-1));
}

static void paste_clipboard_cb(GtkIMHtml *imhtml,gpointer blah)
{
#ifdef _WIN32
/* If we're on windows, let's see if we can get data from the HTML Format
	   clipboard before we try to paste from the GTK buffer */
#else
  GtkClipboard *clipboard = gtk_widget_get_clipboard(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type()))),((GdkAtom )((GdkAtom )((gpointer )((gulong )69)))));
  gtk_clipboard_request_contents(clipboard,gdk_atom_intern("text/html",0),paste_received_cb,imhtml);
#endif
  g_signal_stop_emission_by_name(imhtml,"paste-clipboard");
}

static void imhtml_realized_remove_primary(GtkIMHtml *imhtml,gpointer unused)
{
  gtk_text_buffer_remove_selection_clipboard(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).text_buffer,gtk_widget_get_clipboard(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type()))),((GdkAtom )((GdkAtom )((gpointer )((gulong )1))))));
}

static void imhtml_destroy_add_primary(GtkIMHtml *imhtml,gpointer unused)
{
  gtk_text_buffer_add_selection_clipboard(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).text_buffer,gtk_widget_get_clipboard(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type()))),((GdkAtom )((GdkAtom )((gpointer )((gulong )1))))));
}

static void mark_set_so_update_selection_cb(GtkTextBuffer *buffer,GtkTextIter *arg1,GtkTextMark *mark,GtkIMHtml *imhtml)
{
  if (gtk_text_buffer_get_selection_bounds(buffer,0,0) != 0) {
    gtk_clipboard_set_with_owner(gtk_widget_get_clipboard(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type()))),((GdkAtom )((GdkAtom )((gpointer )((gulong )1))))),selection_targets,(sizeof(selection_targets) / sizeof(GtkTargetEntry )),((GtkClipboardGetFunc )gtk_imhtml_clipboard_get),((GtkClipboardClearFunc )gtk_imhtml_primary_clipboard_clear),((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))));
  }
}

static gboolean gtk_imhtml_button_press_event(GtkIMHtml *imhtml,GdkEventButton *event,gpointer unused)
{
  if ((event -> button) == 2) {
    int x;
    int y;
    GtkTextIter iter;
    GtkClipboard *clipboard = gtk_widget_get_clipboard(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type()))),((GdkAtom )((GdkAtom )((gpointer )((gulong )1)))));
    if (!((imhtml -> editable) != 0)) 
      return 0;
    gtk_text_view_window_to_buffer_coords(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),GTK_TEXT_WINDOW_TEXT,(event -> x),(event -> y),&x,&y);
    gtk_text_view_get_iter_at_location(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),&iter,x,y);
    gtk_text_buffer_place_cursor((imhtml -> text_buffer),(&iter));
    gtk_clipboard_request_contents(clipboard,gdk_atom_intern("text/html",0),paste_received_cb,imhtml);
    return (!0);
  }
  return 0;
}

static void gtk_imhtml_undo(GtkIMHtml *imhtml)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)imhtml;
      GType __t = gtk_imhtml_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_IMHTML(imhtml)");
      return ;
    };
  }while (0);
  if (((imhtml -> editable) != 0) && (gtk_source_undo_manager_can_undo((imhtml -> undo_manager)) != 0)) 
    gtk_source_undo_manager_undo((imhtml -> undo_manager));
}

static void gtk_imhtml_redo(GtkIMHtml *imhtml)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)imhtml;
      GType __t = gtk_imhtml_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_IMHTML(imhtml)");
      return ;
    };
  }while (0);
  if (((imhtml -> editable) != 0) && (gtk_source_undo_manager_can_redo((imhtml -> undo_manager)) != 0)) 
    gtk_source_undo_manager_redo((imhtml -> undo_manager));
}

static gboolean imhtml_message_send(GtkIMHtml *imhtml)
{
  return 0;
}

static void imhtml_paste_cb(GtkIMHtml *imhtml,const char *str)
{
  if (!(gtk_text_view_get_editable(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type())))) != 0)) 
    return ;
  if ((!(str != 0) || !(( *str) != 0)) || !(strcmp(str,"html") != 0)) 
    g_signal_emit_by_name(imhtml,"paste_clipboard");
  else if (!(strcmp(str,"text") != 0)) 
    paste_unformatted_cb(0,imhtml);
}

static void imhtml_toggle_format(GtkIMHtml *imhtml,GtkIMHtmlButtons buttons)
{
/* since this function is the handler for the formatting keystrokes,
	   we need to check here that the formatting attempted is permitted */
  buttons &= (imhtml -> format_functions);
  switch(buttons){
    case GTK_IMHTML_BOLD:
{
      imhtml_toggle_bold(imhtml);
      break; 
    }
    case GTK_IMHTML_ITALIC:
{
      imhtml_toggle_italic(imhtml);
      break; 
    }
    case GTK_IMHTML_UNDERLINE:
{
      imhtml_toggle_underline(imhtml);
      break; 
    }
    case GTK_IMHTML_STRIKE:
{
      imhtml_toggle_strike(imhtml);
      break; 
    }
    case GTK_IMHTML_SHRINK:
{
      imhtml_font_shrink(imhtml);
      break; 
    }
    case GTK_IMHTML_GROW:
{
      imhtml_font_grow(imhtml);
      break; 
    }
    default:
{
      break; 
    }
  }
}

static void gtk_imhtml_finalize(GObject *object)
{
  GtkIMHtml *imhtml = (GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)object),gtk_imhtml_get_type()));
  GList *scalables;
  GSList *l;
  if ((imhtml -> scroll_src) != 0U) 
    g_source_remove((imhtml -> scroll_src));
  if ((imhtml -> scroll_time) != 0) 
    g_timer_destroy((imhtml -> scroll_time));
  g_hash_table_destroy((imhtml -> smiley_data));
  gtk_smiley_tree_destroy((imhtml -> default_smilies));
  gdk_cursor_unref((imhtml -> hand_cursor));
  gdk_cursor_unref((imhtml -> arrow_cursor));
  gdk_cursor_unref((imhtml -> text_cursor));
  if ((imhtml -> tip_window) != 0) {
    gtk_widget_destroy((imhtml -> tip_window));
  }
  if ((imhtml -> tip_timer) != 0U) 
    g_source_remove((imhtml -> tip_timer));
  for (scalables = (imhtml -> scalables); scalables != 0; scalables = (scalables -> next)) {
    struct scalable_data *sd = (scalables -> data);
    GtkIMHtmlScalable *scale = (GtkIMHtmlScalable *)(sd -> scalable);
    ( *(scale -> free))(scale);
    g_free(sd);
  }
  for (l = (imhtml -> im_images); l != 0; l = (l -> next)) {
    struct im_image_data *img_data = (l -> data);
    if (( *(imhtml -> funcs)).image_unref != 0) 
      ( *( *(imhtml -> funcs)).image_unref)((img_data -> id));
    g_free(img_data);
  }
  g_list_free((imhtml -> scalables));
  g_slist_free((imhtml -> im_images));
  g_queue_free((imhtml -> animations));
  g_free((imhtml -> protocol_name));
  g_free((imhtml -> search_string));
  g_object_unref((imhtml -> undo_manager));
  ( *( *((GObjectClass *)(g_type_check_class_cast(((GTypeClass *)parent_class),((GType )(20 << 2)))))).finalize)(object);
}

static GtkIMHtmlProtocol *imhtml_find_protocol(const char *url,gboolean reverse)
{
  GtkIMHtmlClass *klass;
  GList *iter;
  GtkIMHtmlProtocol *proto = (GtkIMHtmlProtocol *)((void *)0);
  int length = ((reverse != 0)?strlen(url) : (-1));
  klass = (g_type_class_ref(gtk_imhtml_get_type()));
  for (iter = (klass -> protocols); iter != 0; iter = (iter -> next)) {
    proto = (iter -> data);
    if (g_ascii_strncasecmp(url,(proto -> name),(((reverse != 0)?(((length < (proto -> length))?length : (proto -> length))) : (proto -> length)))) == 0) {
      return proto;
    }
  }
  return 0;
}

static void imhtml_url_clicked(GtkIMHtml *imhtml,const char *url)
{
  GtkIMHtmlProtocol *proto = imhtml_find_protocol(url,0);
  GtkIMHtmlLink *link;
  if (!(proto != 0)) 
    return ;
  link = ((GtkIMHtmlLink *)(g_malloc0_n(1,(sizeof(GtkIMHtmlLink )))));
  link -> imhtml = (g_object_ref(imhtml));
  link -> url = g_strdup(url);
/* XXX: Do something with the return value? */
  ( *(proto -> activate))(imhtml,link);
  gtk_imhtml_link_destroy(link);
}
/* Boring GTK+ stuff */

static void gtk_imhtml_class_init(GtkIMHtmlClass *klass)
{
  GtkWidgetClass *widget_class = (GtkWidgetClass *)klass;
  GtkBindingSet *binding_set;
  GObjectClass *gobject_class;
  gobject_class = ((GObjectClass *)klass);
  parent_class = (g_type_class_ref(gtk_text_view_get_type()));
  signals[URL_CLICKED] = g_signal_new("url_clicked",( *((GTypeClass *)gobject_class)).g_type,G_SIGNAL_RUN_FIRST,((glong )((size_t )(&( *((GtkIMHtmlClass *)((GtkIMHtmlClass *)0))).url_clicked))),0,0,g_cclosure_marshal_VOID__POINTER,((GType )(1 << 2)),1,((GType )(17 << 2)));
  signals[BUTTONS_UPDATE] = g_signal_new("format_buttons_update",( *((GTypeClass *)gobject_class)).g_type,G_SIGNAL_RUN_FIRST,((glong )((size_t )(&( *((GtkIMHtmlClass *)((GtkIMHtmlClass *)0))).buttons_update))),0,0,g_cclosure_marshal_VOID__INT,((GType )(1 << 2)),1,((GType )(6 << 2)));
  signals[TOGGLE_FORMAT] = g_signal_new("format_function_toggle",( *((GTypeClass *)gobject_class)).g_type,(G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION),((glong )((size_t )(&( *((GtkIMHtmlClass *)((GtkIMHtmlClass *)0))).toggle_format))),0,0,g_cclosure_marshal_VOID__INT,((GType )(1 << 2)),1,((GType )(6 << 2)));
  signals[CLEAR_FORMAT] = g_signal_new("format_function_clear",( *((GTypeClass *)gobject_class)).g_type,(G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION),((glong )((size_t )(&( *((GtkIMHtmlClass *)((GtkIMHtmlClass *)0))).clear_format))),0,0,g_cclosure_marshal_VOID__VOID,((GType )(1 << 2)),0);
  signals[UPDATE_FORMAT] = g_signal_new("format_function_update",( *((GTypeClass *)gobject_class)).g_type,G_SIGNAL_RUN_FIRST,((glong )((size_t )(&( *((GtkIMHtmlClass *)((GtkIMHtmlClass *)0))).update_format))),0,0,g_cclosure_marshal_VOID__VOID,((GType )(1 << 2)),0);
  signals[MESSAGE_SEND] = g_signal_new("message_send",( *((GTypeClass *)gobject_class)).g_type,(G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION),((glong )((size_t )(&( *((GtkIMHtmlClass *)((GtkIMHtmlClass *)0))).message_send))),0,0,g_cclosure_marshal_VOID__VOID,((GType )(1 << 2)),0);
  signals[PASTE] = g_signal_new("paste",( *((GTypeClass *)gobject_class)).g_type,(G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION),0,0,0,g_cclosure_marshal_VOID__STRING,((GType )(1 << 2)),1,((GType )(16 << 2)));
  signals[UNDO] = g_signal_new("undo",( *((GTypeClass *)klass)).g_type,(G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION),((glong )((size_t )(&( *((GtkIMHtmlClass *)((GtkIMHtmlClass *)0))).undo))),0,0,g_cclosure_marshal_VOID__VOID,((GType )(1 << 2)),0);
  signals[REDO] = g_signal_new("redo",( *((GTypeClass *)klass)).g_type,(G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION),((glong )((size_t )(&( *((GtkIMHtmlClass *)((GtkIMHtmlClass *)0))).redo))),0,0,g_cclosure_marshal_VOID__VOID,((GType )(1 << 2)),0);
  klass -> toggle_format = imhtml_toggle_format;
  klass -> message_send = imhtml_message_send;
  klass -> clear_format = imhtml_clear_formatting;
  klass -> url_clicked = imhtml_url_clicked;
  klass -> undo = gtk_imhtml_undo;
  klass -> redo = gtk_imhtml_redo;
  gobject_class -> finalize = gtk_imhtml_finalize;
  widget_class -> drag_motion = gtk_text_view_drag_motion;
  widget_class -> expose_event = gtk_imhtml_expose_event;
  parent_size_allocate = (widget_class -> size_allocate);
  widget_class -> size_allocate = gtk_imhtml_size_allocate;
  parent_style_set = (widget_class -> style_set);
  widget_class -> style_set = gtk_imhtml_style_set;
  gtk_widget_class_install_style_property(widget_class,g_param_spec_boxed("hyperlink-color",((const char *)(dgettext("pidgin","Hyperlink color"))),((const char *)(dgettext("pidgin","Color to draw hyperlinks."))),gdk_color_get_type(),G_PARAM_READABLE));
  gtk_widget_class_install_style_property(widget_class,g_param_spec_boxed("hyperlink-visited-color",((const char *)(dgettext("pidgin","Hyperlink visited color"))),((const char *)(dgettext("pidgin","Color to draw hyperlink after it has been visited (or activated)."))),gdk_color_get_type(),G_PARAM_READABLE));
  gtk_widget_class_install_style_property(widget_class,g_param_spec_boxed("hyperlink-prelight-color",((const char *)(dgettext("pidgin","Hyperlink prelight color"))),((const char *)(dgettext("pidgin","Color to draw hyperlinks when mouse is over them."))),gdk_color_get_type(),G_PARAM_READABLE));
  gtk_widget_class_install_style_property(widget_class,g_param_spec_boxed("send-name-color",((const char *)(dgettext("pidgin","Sent Message Name Color"))),((const char *)(dgettext("pidgin","Color to draw the name of a message you sent."))),gdk_color_get_type(),G_PARAM_READABLE));
  gtk_widget_class_install_style_property(widget_class,g_param_spec_boxed("receive-name-color",((const char *)(dgettext("pidgin","Received Message Name Color"))),((const char *)(dgettext("pidgin","Color to draw the name of a message you received."))),gdk_color_get_type(),G_PARAM_READABLE));
  gtk_widget_class_install_style_property(widget_class,g_param_spec_boxed("highlight-name-color",((const char *)(dgettext("pidgin","\"Attention\" Name Color"))),((const char *)(dgettext("pidgin","Color to draw the name of a message you received containing your name."))),gdk_color_get_type(),G_PARAM_READABLE));
  gtk_widget_class_install_style_property(widget_class,g_param_spec_boxed("action-name-color",((const char *)(dgettext("pidgin","Action Message Name Color"))),((const char *)(dgettext("pidgin","Color to draw the name of an action message."))),gdk_color_get_type(),G_PARAM_READABLE));
  gtk_widget_class_install_style_property(widget_class,g_param_spec_boxed("whisper-action-name-color",((const char *)(dgettext("pidgin","Action Message Name Color for Whispered Message"))),((const char *)(dgettext("pidgin","Color to draw the name of a whispered action message."))),gdk_color_get_type(),G_PARAM_READABLE));
  gtk_widget_class_install_style_property(widget_class,g_param_spec_boxed("whisper-name-color",((const char *)(dgettext("pidgin","Whisper Message Name Color"))),((const char *)(dgettext("pidgin","Color to draw the name of a whispered message."))),gdk_color_get_type(),G_PARAM_READABLE));
/* Customizable typing notification ... sort of. Example:
	 *   GtkIMHtml::typing-notification-font = "monospace italic light 8.0"
	 *   GtkIMHtml::typing-notification-color = "#ff0000"
	 *   GtkIMHtml::typing-notification-enable = 1
	 */
  gtk_widget_class_install_style_property(widget_class,g_param_spec_boxed("typing-notification-color",((const char *)(dgettext("pidgin","Typing notification color"))),((const char *)(dgettext("pidgin","The color to use for the typing notification"))),gdk_color_get_type(),G_PARAM_READABLE));
  gtk_widget_class_install_style_property(widget_class,g_param_spec_string("typing-notification-font",((const char *)(dgettext("pidgin","Typing notification font"))),((const char *)(dgettext("pidgin","The font to use for the typing notification"))),"light 8.0",G_PARAM_READABLE));
  gtk_widget_class_install_style_property(widget_class,g_param_spec_boolean("typing-notification-enable",((const char *)(dgettext("pidgin","Enable typing notification"))),((const char *)(dgettext("pidgin","Enable typing notification"))),(!0),G_PARAM_READABLE));
  binding_set = gtk_binding_set_by_class(parent_class);
  gtk_binding_entry_add_signal(binding_set,0x062,GDK_CONTROL_MASK,"format_function_toggle",1,((GType )(6 << 2)),GTK_IMHTML_BOLD);
  gtk_binding_entry_add_signal(binding_set,0x069,GDK_CONTROL_MASK,"format_function_toggle",1,((GType )(6 << 2)),GTK_IMHTML_ITALIC);
  gtk_binding_entry_add_signal(binding_set,0x075,GDK_CONTROL_MASK,"format_function_toggle",1,((GType )(6 << 2)),GTK_IMHTML_UNDERLINE);
  gtk_binding_entry_add_signal(binding_set,0x02b,GDK_CONTROL_MASK,"format_function_toggle",1,((GType )(6 << 2)),GTK_IMHTML_GROW);
  gtk_binding_entry_add_signal(binding_set,0x03d,GDK_CONTROL_MASK,"format_function_toggle",1,((GType )(6 << 2)),GTK_IMHTML_GROW);
  gtk_binding_entry_add_signal(binding_set,0x02d,GDK_CONTROL_MASK,"format_function_toggle",1,((GType )(6 << 2)),GTK_IMHTML_SHRINK);
  binding_set = gtk_binding_set_by_class(klass);
  gtk_binding_entry_add_signal(binding_set,0x072,GDK_CONTROL_MASK,"format_function_clear",0);
  gtk_binding_entry_add_signal(binding_set,0xff8d,0,"message_send",0);
  gtk_binding_entry_add_signal(binding_set,0xff0d,0,"message_send",0);
  gtk_binding_entry_add_signal(binding_set,0x07a,GDK_CONTROL_MASK,"undo",0);
  gtk_binding_entry_add_signal(binding_set,0x07a,(GDK_CONTROL_MASK | GDK_SHIFT_MASK),"redo",0);
  gtk_binding_entry_add_signal(binding_set,0xffcb,0,"undo",0);
  gtk_binding_entry_add_signal(binding_set,0x076,(GDK_CONTROL_MASK | GDK_SHIFT_MASK),"paste",1,((GType )(16 << 2)),"text");
}

static void gtk_imhtml_init(GtkIMHtml *imhtml)
{
  imhtml -> text_buffer = gtk_text_buffer_new(0);
  imhtml -> undo_manager = gtk_source_undo_manager_new((imhtml -> text_buffer));
  gtk_text_view_set_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),(imhtml -> text_buffer));
  gtk_text_view_set_wrap_mode(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),GTK_WRAP_WORD_CHAR);
  gtk_text_view_set_pixels_above_lines(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),2);
  gtk_text_view_set_pixels_below_lines(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),3);
  gtk_text_view_set_left_margin(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),2);
  gtk_text_view_set_right_margin(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),2);
/*gtk_text_view_set_indent(GTK_TEXT_VIEW(imhtml), -15);*/
/*gtk_text_view_set_justification(GTK_TEXT_VIEW(imhtml), GTK_JUSTIFY_FILL);*/
/* These tags will be used often and can be reused--we create them on init and then apply them by name
	 * other tags (color, size, face, etc.) will have to be created and applied dynamically
	 * Note that even though we created SUB, SUP, and PRE tags here, we don't really
	 * apply them anywhere yet. */
  gtk_text_buffer_create_tag((imhtml -> text_buffer),"BOLD","weight",PANGO_WEIGHT_BOLD,((void *)((void *)0)));
  gtk_text_buffer_create_tag((imhtml -> text_buffer),"ITALICS","style",PANGO_STYLE_ITALIC,((void *)((void *)0)));
  gtk_text_buffer_create_tag((imhtml -> text_buffer),"UNDERLINE","underline",PANGO_UNDERLINE_SINGLE,((void *)((void *)0)));
  gtk_text_buffer_create_tag((imhtml -> text_buffer),"STRIKE","strikethrough",!0,((void *)((void *)0)));
  gtk_text_buffer_create_tag((imhtml -> text_buffer),"SUB","rise",-5000,((void *)((void *)0)));
  gtk_text_buffer_create_tag((imhtml -> text_buffer),"SUP","rise",5000,((void *)((void *)0)));
  gtk_text_buffer_create_tag((imhtml -> text_buffer),"PRE","family","Monospace",((void *)((void *)0)));
  gtk_text_buffer_create_tag((imhtml -> text_buffer),"search","background","#22ff00","weight","bold",((void *)((void *)0)));
  gtk_text_buffer_create_tag((imhtml -> text_buffer),"comment","weight",PANGO_WEIGHT_NORMAL,((void *)((void *)0)));
#if FALSE && GTK_CHECK_VERSION(2,10,10)
#endif
  gtk_text_buffer_create_tag((imhtml -> text_buffer),"send-name","weight",PANGO_WEIGHT_BOLD,((void *)((void *)0)));
  gtk_text_buffer_create_tag((imhtml -> text_buffer),"receive-name","weight",PANGO_WEIGHT_BOLD,((void *)((void *)0)));
  gtk_text_buffer_create_tag((imhtml -> text_buffer),"highlight-name","weight",PANGO_WEIGHT_BOLD,((void *)((void *)0)));
  gtk_text_buffer_create_tag((imhtml -> text_buffer),"action-name","weight",PANGO_WEIGHT_BOLD,((void *)((void *)0)));
  gtk_text_buffer_create_tag((imhtml -> text_buffer),"whisper-action-name","weight",PANGO_WEIGHT_BOLD,((void *)((void *)0)));
  gtk_text_buffer_create_tag((imhtml -> text_buffer),"whisper-name","weight",PANGO_WEIGHT_BOLD,((void *)((void *)0)));
/* When hovering over a link, we show the hand cursor--elsewhere we show the plain ol' pointer cursor */
  imhtml -> hand_cursor = gdk_cursor_new(GDK_HAND2);
  imhtml -> arrow_cursor = gdk_cursor_new(GDK_LEFT_PTR);
  imhtml -> text_cursor = gdk_cursor_new(GDK_XTERM);
  imhtml -> show_comments = (!0);
  imhtml -> smiley_data = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,((GDestroyNotify )gtk_smiley_tree_destroy));
  imhtml -> default_smilies = gtk_smiley_tree_new();
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"motion-notify-event",((GCallback )gtk_motion_event_notify),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"leave-notify-event",((GCallback )gtk_leave_event_notify),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"enter-notify-event",((GCallback )gtk_enter_event_notify),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"button_press_event",((GCallback )gtk_imhtml_button_press_event),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(imhtml -> text_buffer)),((GType )(20 << 2))))),"insert-text",((GCallback )preinsert_cb),imhtml,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(imhtml -> text_buffer)),((GType )(20 << 2))))),"delete_range",((GCallback )delete_cb),imhtml,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(imhtml -> text_buffer)),((GType )(20 << 2))))),"insert-text",((GCallback )insert_cb),imhtml,0,G_CONNECT_AFTER);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(imhtml -> text_buffer)),((GType )(20 << 2))))),"insert-child-anchor",((GCallback )insert_ca_cb),imhtml,0,G_CONNECT_AFTER);
  gtk_drag_dest_set(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type()))),0,link_drag_drop_targets,(sizeof(link_drag_drop_targets) / sizeof(GtkTargetEntry )),GDK_ACTION_COPY);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"drag_data_received",((GCallback )gtk_imhtml_link_drag_rcv_cb),imhtml,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"drag_drop",((GCallback )gtk_imhtml_link_drop_cb),imhtml,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"copy-clipboard",((GCallback )copy_clipboard_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"cut-clipboard",((GCallback )cut_clipboard_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"paste-clipboard",((GCallback )paste_clipboard_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"realize",((GCallback )imhtml_realized_remove_primary),0,0,G_CONNECT_AFTER);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"unrealize",((GCallback )imhtml_destroy_add_primary),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"paste",((GCallback )imhtml_paste_cb),0,0,((GConnectFlags )0));
#ifndef _WIN32
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).text_buffer),((GType )(20 << 2))))),"mark-set",((GCallback )mark_set_so_update_selection_cb),imhtml,0,G_CONNECT_AFTER);
#endif
  gtk_widget_add_events(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type()))),(GDK_LEAVE_NOTIFY_MASK | GDK_ENTER_NOTIFY_MASK));
  imhtml -> tip = ((char *)((void *)0));
  imhtml -> tip_timer = 0;
  imhtml -> tip_window = ((GtkWidget *)((void *)0));
  imhtml -> edit.bold = 0;
  imhtml -> edit.italic = 0;
  imhtml -> edit.underline = 0;
  imhtml -> edit.forecolor = ((gchar *)((void *)0));
  imhtml -> edit.backcolor = ((gchar *)((void *)0));
  imhtml -> edit.fontface = ((gchar *)((void *)0));
  imhtml -> edit.fontsize = 0;
  imhtml -> edit.link = ((GtkTextTag *)((void *)0));
  imhtml -> scalables = ((GList *)((void *)0));
  imhtml -> animations = g_queue_new();
  gtk_imhtml_set_editable(imhtml,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"populate-popup",((GCallback )hijack_menu_cb),0,0,((GConnectFlags )0));
}

GtkWidget *gtk_imhtml_new(void *a,void *b)
{
  return (GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(g_object_new(gtk_imhtml_get_type(),0))),gtk_widget_get_type()));
}

GType gtk_imhtml_get_type()
{
  static GType imhtml_type = 0;
  if (!(imhtml_type != 0UL)) {
    static const GTypeInfo imhtml_info = {((sizeof(GtkIMHtmlClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )gtk_imhtml_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(GtkIMHtml ))), (0), ((GInstanceInitFunc )gtk_imhtml_init), ((const GTypeValueTable *)((void *)0))};
    imhtml_type = g_type_register_static(gtk_text_view_get_type(),"GtkIMHtml",&imhtml_info,0);
  }
  return imhtml_type;
}

static void gtk_imhtml_link_destroy(GtkIMHtmlLink *link)
{
  if ((link -> imhtml) != 0) 
    g_object_unref((link -> imhtml));
  if ((link -> tag) != 0) 
    g_object_unref((link -> tag));
  g_free((link -> url));
  g_free(link);
}
/* The callback for an event on a link tag. */

static gboolean tag_event(GtkTextTag *tag,GObject *imhtml,GdkEvent *event,GtkTextIter *arg2,gpointer unused)
{
  GdkEventButton *event_button = (GdkEventButton *)event;
  if (( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).editable != 0) 
    return 0;
  if ((event -> type) == GDK_BUTTON_RELEASE) {
    if (((event_button -> button) == 1) || ((event_button -> button) == 2)) {
      GtkTextIter start;
      GtkTextIter end;
/* we shouldn't open a URL if the user has selected something: */
      if (gtk_text_buffer_get_selection_bounds(gtk_text_iter_get_buffer(arg2),&start,&end) != 0) 
        return 0;
      gtk_imhtml_activate_tag(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),tag);
      return 0;
    }
    else if ((event_button -> button) == 3) {
      GList *children;
      GtkWidget *menu;
      GtkIMHtmlProtocol *proto;
      GtkIMHtmlLink *link = (GtkIMHtmlLink *)(g_malloc_n(1,(sizeof(GtkIMHtmlLink ))));
      link -> imhtml = (g_object_ref(imhtml));
      link -> url = g_strdup((g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))),"link_url")));
      link -> tag = (g_object_ref(tag));
/* Don't want the tooltip around if user right-clicked on link */
      if (( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip_window != 0) {
        gtk_widget_destroy(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip_window);
        ( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip_window = ((GtkWidget *)((void *)0));
      }
      if (( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip_timer != 0U) {
        g_source_remove(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip_timer);
        ( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).tip_timer = 0;
      }
      if (( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).editable != 0) 
        gdk_window_set_cursor((event_button -> window),( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).text_cursor);
      else 
        gdk_window_set_cursor((event_button -> window),( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).arrow_cursor);
      menu = gtk_menu_new();
      g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menu),((GType )(20 << 2))))),"x-imhtml-url-data",link,((GDestroyNotify )gtk_imhtml_link_destroy));
      proto = imhtml_find_protocol((link -> url),0);
      if ((proto != 0) && ((proto -> context_menu) != 0)) {
        ( *(proto -> context_menu))(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(link -> imhtml)),gtk_imhtml_get_type()))),link,menu);
      }
      children = gtk_container_get_children(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_container_get_type()))));
      if (!(children != 0)) {
        GtkWidget *item = gtk_menu_item_new_with_label(((const char *)(dgettext("pidgin","No actions available"))));
        gtk_widget_show(item);
        gtk_widget_set_sensitive(item,0);
        gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item);
      }
      else {
        g_list_free(children);
      }
      gtk_widget_show_all(menu);
      gtk_menu_popup(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_get_type()))),0,0,0,0,(event_button -> button),(event_button -> time));
      return (!0);
    }
  }
  if (((event -> type) == GDK_BUTTON_PRESS) && ((event_button -> button) == 3)) 
/* Clicking the right mouse button on a link shouldn't
						be caught by the regular GtkTextView menu */
    return (!0);
  else 
/* Let clicks go through if we didn't catch anything */
    return 0;
}

static gboolean gtk_text_view_drag_motion(GtkWidget *widget,GdkDragContext *context,gint x,gint y,guint time)
{
  GdkDragAction suggested_action = 0;
  if (gtk_drag_dest_find_target(widget,context,0) == ((GdkAtom )((GdkAtom )((gpointer )((gulong )0))))) {
/* can't accept any of the offered targets */
  }
  else {
    GtkWidget *source_widget;
    suggested_action = (context -> suggested_action);
    source_widget = gtk_drag_get_source_widget(context);
    if (source_widget == widget) {
/* Default to MOVE, unless the user has
			 * pressed ctrl or alt to affect available actions
			 */
      if (((context -> actions) & GDK_ACTION_MOVE) != 0) 
        suggested_action = GDK_ACTION_MOVE;
    }
  }
  gdk_drag_status(context,suggested_action,time);
/* TRUE return means don't propagate the drag motion to parent
   * widgets that may also be drop sites.
   */
  return (!0);
}

static void gtk_imhtml_link_drop_cb(GtkWidget *widget,GdkDragContext *context,gint x,gint y,guint time,gpointer user_data)
{
  GdkAtom target = gtk_drag_dest_find_target(widget,context,0);
  if (target != ((GdkAtom )((GdkAtom )((gpointer )((gulong )0))))) 
    gtk_drag_get_data(widget,context,target,time);
  else 
    gtk_drag_finish(context,0,0,time);
}

static void gtk_imhtml_link_drag_rcv_cb(GtkWidget *widget,GdkDragContext *dc,guint x,guint y,GtkSelectionData *sd,guint info,guint t,GtkIMHtml *imhtml)
{
  gchar **links;
  gchar *link;
  char *text = (char *)(sd -> data);
  GtkTextMark *mark = gtk_text_buffer_get_insert((imhtml -> text_buffer));
  GtkTextIter iter;
  gint i = 0;
  gtk_text_buffer_get_iter_at_mark((imhtml -> text_buffer),&iter,mark);
  if ((gtk_imhtml_get_editable(imhtml) != 0) && ((sd -> data) != 0)) {{
      switch(info){
        case GTK_IMHTML_DRAG_URL:
{
/* TODO: Is it really ok to change sd->data...? */
          purple_str_strip_char(((char *)(sd -> data)),13);
          links = g_strsplit(((char *)(sd -> data)),"\n",0);
          while((link = links[i]) != ((gchar *)((void *)0))){
            if (gtk_imhtml_is_protocol(link) != 0) {
              gchar *label;
              if (links[i + 1] != 0) 
                i++;
              label = links[i];
              gtk_imhtml_insert_link(imhtml,mark,link,label);
            }
            else if (( *link) == 0) {
/* Ignore blank lines */
            }
            else {
/* Special reasons, aka images being put in via other tag, etc. */
/* ... don't pretend we handled it if we didn't */
              gtk_drag_finish(dc,0,0,t);
              g_strfreev(links);
              return ;
            }
            i++;
          }
          g_strfreev(links);
          break; 
        }
        case GTK_IMHTML_DRAG_HTML:
{
{
            char *utf8 = (char *)((void *)0);
/* Ewww. This is all because mozilla thinks that text/html is 'for internal use only.'
			 * as explained by this comment in gtkhtml:
			 *
			 * FIXME This hack decides the charset of the selection.  It seems that
			 * mozilla/netscape alway use ucs2 for text/html
			 * and openoffice.org seems to always use utf8 so we try to validate
			 * the string as utf8 and if that fails we assume it is ucs2
			 *
			 * See also the comment on text/html here:
			 * http://mail.gnome.org/archives/gtk-devel-list/2001-September/msg00114.html
			 */
            if (((sd -> length) >= 2) && !(g_utf8_validate(text,((sd -> length) - 1),0) != 0)) {
              utf8 = utf16_to_utf8_with_bom_check(text,(sd -> length));
              if (!(utf8 != 0)) {
                purple_debug_warning("gtkimhtml","g_convert from UTF-16 failed in drag_rcv_cb\n");
                return ;
              }
            }
            else if (!(( *text) != 0) || !(g_utf8_validate(text,(-1),0) != 0)) {
              purple_debug_warning("gtkimhtml","empty string or invalid UTF-8 in drag_rcv_cb\n");
              return ;
            }
            gtk_imhtml_insert_html_at_iter(imhtml,((utf8 != 0)?utf8 : text),0,&iter);
            g_free(utf8);
            break; 
          }
        }
        case GTK_IMHTML_DRAG_TEXT:
{
          if (!(( *text) != 0) || !(g_utf8_validate(text,(-1),0) != 0)) {
            purple_debug_warning("gtkimhtml","empty string or invalid UTF-8 in drag_rcv_cb\n");
            return ;
          }
          else {
            char *tmp = g_markup_escape_text(text,(-1));
            gtk_imhtml_insert_html_at_iter(imhtml,tmp,0,&iter);
            g_free(tmp);
          }
          break; 
        }
        default:
{
          gtk_drag_finish(dc,0,0,t);
          return ;
        }
      }
    }
    gtk_drag_finish(dc,(!0),((dc -> action) == GDK_ACTION_MOVE),t);
  }
  else {
    gtk_drag_finish(dc,0,0,t);
  }
}

static void gtk_smiley_tree_remove(GtkSmileyTree *tree,GtkIMHtmlSmiley *smiley)
{
  GtkSmileyTree *t = tree;
  const gchar *x = (smiley -> smile);
  gint len = 0;
  while(( *x) != 0){
    gchar *pos;
    if (!((t -> values) != 0)) 
      return ;
    pos = strchr(( *(t -> values)).str,( *x));
    if (pos != 0) 
      t = (t -> children)[pos - ( *(t -> values)).str];
    else 
      return ;
    x++;
    len++;
  }
  if ((t -> image) != 0) {
    t -> image = ((GtkIMHtmlSmiley *)((void *)0));
  }
}

static gint gtk_smiley_tree_lookup(GtkSmileyTree *tree,const gchar *text)
{
  GtkSmileyTree *t = tree;
  const gchar *x = text;
  gint len = 0;
  const gchar *amp;
  gint alen;
{
    while(( *x) != 0){
      gchar *pos;
      if (!((t -> values) != 0)) 
        break; 
      if ((( *x) == '&') && ((amp = purple_markup_unescape_entity(x,&alen)) != 0)) {
        gboolean matched = (!0);
{
/* Make sure all chars of the unescaped value match */
          while(amp[1] != 0){
            pos = strchr(( *(t -> values)).str,( *amp));
            if (pos != 0) 
              t = (t -> children)[((gint )((glong )pos)) - ((gint )((glong )( *(t -> values)).str))];
            else {
              matched = 0;
              break; 
            }
            amp++;
          }
        }
        if (!(matched != 0)) 
          break; 
        pos = strchr(( *(t -> values)).str,( *amp));
      }
      else 
/* Because we're all WYSIWYG now, a '<'
				     * char should only appear as the start of a tag.  Perhaps a safer (but costlier)
				     * check would be to call gtk_imhtml_is_tag on it */
if (( *x) == 60) 
        break; 
      else {
        alen = 1;
        pos = strchr(( *(t -> values)).str,( *x));
      }
      if (pos != 0) 
        t = (t -> children)[((gint )((glong )pos)) - ((gint )((glong )( *(t -> values)).str))];
      else 
        break; 
      x += alen;
      len += alen;
    }
  }
  if ((t -> image) != 0) 
    return len;
  return 0;
}

static void gtk_imhtml_disassociate_smiley_foreach(gpointer key,gpointer value,gpointer user_data)
{
  GtkSmileyTree *tree = (GtkSmileyTree *)value;
  GtkIMHtmlSmiley *smiley = (GtkIMHtmlSmiley *)user_data;
  gtk_smiley_tree_remove(tree,smiley);
}

static void gtk_imhtml_disconnect_smiley(GtkIMHtml *imhtml,GtkIMHtmlSmiley *smiley)
{
  smiley -> imhtml = ((GtkIMHtml *)((void *)0));
  g_signal_handlers_disconnect_matched(imhtml,G_SIGNAL_MATCH_DATA,0,0,0,0,smiley);
}

static void gtk_imhtml_disassociate_smiley(GtkIMHtmlSmiley *smiley)
{
  if ((smiley -> imhtml) != 0) {
    gtk_smiley_tree_remove(( *(smiley -> imhtml)).default_smilies,smiley);
    g_hash_table_foreach(( *(smiley -> imhtml)).smiley_data,gtk_imhtml_disassociate_smiley_foreach,smiley);
    g_signal_handlers_disconnect_matched((smiley -> imhtml),G_SIGNAL_MATCH_DATA,0,0,0,0,smiley);
    smiley -> imhtml = ((GtkIMHtml *)((void *)0));
  }
}

void gtk_imhtml_associate_smiley(GtkIMHtml *imhtml,const gchar *sml,GtkIMHtmlSmiley *smiley)
{
  GtkSmileyTree *tree;
  do {
    if (imhtml != ((GtkIMHtml *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"imhtml != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)imhtml;
      GType __t = gtk_imhtml_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_IMHTML (imhtml)");
      return ;
    };
  }while (0);
  if (sml == ((const gchar *)((void *)0))) 
    tree = (imhtml -> default_smilies);
  else if (!((tree = (g_hash_table_lookup((imhtml -> smiley_data),sml))) != 0)) {
    tree = gtk_smiley_tree_new();
    g_hash_table_insert((imhtml -> smiley_data),(g_strdup(sml)),tree);
  }
/* need to disconnect old imhtml, if there is one */
  if ((smiley -> imhtml) != 0) {
    g_signal_handlers_disconnect_matched((smiley -> imhtml),G_SIGNAL_MATCH_DATA,0,0,0,0,smiley);
  }
  smiley -> imhtml = imhtml;
  gtk_smiley_tree_insert(tree,smiley);
/* connect destroy signal for the imhtml */
  g_signal_connect_data(imhtml,"destroy",((GCallback )gtk_imhtml_disconnect_smiley),smiley,0,((GConnectFlags )0));
}

static gboolean gtk_imhtml_is_smiley(GtkIMHtml *imhtml,GSList *fonts,const gchar *text,gint *len)
{
  GtkSmileyTree *tree;
  GtkIMHtmlFontDetail *font;
  char *sml = (char *)((void *)0);
  if (fonts != 0) {
    font = (fonts -> data);
    sml = (font -> sml);
  }
  if (!(sml != 0)) 
    sml = (imhtml -> protocol_name);
  if (!(sml != 0) || !((tree = (g_hash_table_lookup((imhtml -> smiley_data),sml))) != 0)) 
    tree = (imhtml -> default_smilies);
  if (tree == ((GtkSmileyTree *)((void *)0))) 
    return 0;
   *len = gtk_smiley_tree_lookup(tree,text);
  return  *len > 0;
}

static GtkIMHtmlSmiley *gtk_imhtml_smiley_get_from_tree(GtkSmileyTree *t,const gchar *text)
{
  const gchar *x = text;
  gchar *pos;
  if (t == ((GtkSmileyTree *)((void *)0))) 
    return 0;
  while(( *x) != 0){
    if (!((t -> values) != 0)) 
      return 0;
    pos = strchr(( *(t -> values)).str,( *x));
    if (!(pos != 0)) 
      return 0;
    t = (t -> children)[((gint )((glong )pos)) - ((gint )((glong )( *(t -> values)).str))];
    x++;
  }
  return t -> image;
}

GtkIMHtmlSmiley *gtk_imhtml_smiley_get(GtkIMHtml *imhtml,const gchar *sml,const gchar *text)
{
  GtkIMHtmlSmiley *ret;
/* Look for custom smileys first */
  if (sml != ((const gchar *)((void *)0))) {
    ret = gtk_imhtml_smiley_get_from_tree((g_hash_table_lookup((imhtml -> smiley_data),sml)),text);
    if (ret != ((GtkIMHtmlSmiley *)((void *)0))) 
      return ret;
  }
/* Fall back to check for default smileys */
  return gtk_imhtml_smiley_get_from_tree((imhtml -> default_smilies),text);
}

static GdkPixbufAnimation *gtk_smiley_get_image(GtkIMHtmlSmiley *smiley)
{
  if (!((smiley -> icon) != 0)) {
    if ((smiley -> file) != 0) {
      smiley -> icon = gdk_pixbuf_animation_new_from_file((smiley -> file),0);
    }
    else if ((smiley -> loader) != 0) {
      smiley -> icon = gdk_pixbuf_loader_get_animation((smiley -> loader));
      if ((smiley -> icon) != 0) 
        g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(smiley -> icon)),((GType )(20 << 2))))));
    }
  }
  return smiley -> icon;
}
#define VALID_TAG(x)	do { \
			if (!g_ascii_strncasecmp (string, x ">", strlen (x ">"))) {	\
				if (tag) *tag = g_strndup (string, strlen (x));		\
				if (len) *len = strlen (x) + 1;				\
				return TRUE;					\
			}							\
			if (type) (*type)++; \
		} while (0)
#define VALID_OPT_TAG(x)	do { \
				if (!g_ascii_strncasecmp (string, x " ", strlen (x " "))) {	\
					const gchar *c = string + strlen (x " ");	\
					gchar e = '"';					\
					gboolean quote = FALSE;				\
					while (*c) {					\
						if (*c == '"' || *c == '\'') {		\
							if (quote && (*c == e))		\
								quote = !quote;		\
							else if (!quote) {		\
								quote = !quote;		\
								e = *c;			\
							}				\
						} else if (!quote && (*c == '>'))	\
							break;				\
						c++;					\
					}						\
					if (*c) {					\
						if (tag) *tag = g_strndup (string, c - string);	\
						if (len) *len = c - string + 1;			\
						return TRUE;				\
					}						\
				}							\
				if (type) (*type)++; \
			} while (0)

static gboolean gtk_imhtml_is_tag(const gchar *string,gchar **tag,gint *len,gint *type)
{
  char *close;
  if (type != 0) 
     *type = 1;
  if (!((close = strchr(string,'>')) != 0)) 
    return 0;
  do {
    if (!(g_ascii_strncasecmp(string,"B>",strlen("B>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("B"));
      if (len != 0) 
         *len = (strlen("B") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"BOLD>",strlen("BOLD>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("BOLD"));
      if (len != 0) 
         *len = (strlen("BOLD") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/B>",strlen("/B>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/B"));
      if (len != 0) 
         *len = (strlen("/B") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/BOLD>",strlen("/BOLD>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/BOLD"));
      if (len != 0) 
         *len = (strlen("/BOLD") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"I>",strlen("I>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("I"));
      if (len != 0) 
         *len = (strlen("I") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"ITALIC>",strlen("ITALIC>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("ITALIC"));
      if (len != 0) 
         *len = (strlen("ITALIC") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/I>",strlen("/I>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/I"));
      if (len != 0) 
         *len = (strlen("/I") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/ITALIC>",strlen("/ITALIC>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/ITALIC"));
      if (len != 0) 
         *len = (strlen("/ITALIC") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"U>",strlen("U>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("U"));
      if (len != 0) 
         *len = (strlen("U") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"UNDERLINE>",strlen("UNDERLINE>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("UNDERLINE"));
      if (len != 0) 
         *len = (strlen("UNDERLINE") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/U>",strlen("/U>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/U"));
      if (len != 0) 
         *len = (strlen("/U") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/UNDERLINE>",strlen("/UNDERLINE>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/UNDERLINE"));
      if (len != 0) 
         *len = (strlen("/UNDERLINE") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"S>",strlen("S>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("S"));
      if (len != 0) 
         *len = (strlen("S") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"STRIKE>",strlen("STRIKE>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("STRIKE"));
      if (len != 0) 
         *len = (strlen("STRIKE") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/S>",strlen("/S>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/S"));
      if (len != 0) 
         *len = (strlen("/S") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/STRIKE>",strlen("/STRIKE>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/STRIKE"));
      if (len != 0) 
         *len = (strlen("/STRIKE") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"SUB>",strlen("SUB>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("SUB"));
      if (len != 0) 
         *len = (strlen("SUB") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/SUB>",strlen("/SUB>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/SUB"));
      if (len != 0) 
         *len = (strlen("/SUB") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"SUP>",strlen("SUP>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("SUP"));
      if (len != 0) 
         *len = (strlen("SUP") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/SUP>",strlen("/SUP>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/SUP"));
      if (len != 0) 
         *len = (strlen("/SUP") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"PRE>",strlen("PRE>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("PRE"));
      if (len != 0) 
         *len = (strlen("PRE") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/PRE>",strlen("/PRE>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/PRE"));
      if (len != 0) 
         *len = (strlen("/PRE") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"TITLE>",strlen("TITLE>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("TITLE"));
      if (len != 0) 
         *len = (strlen("TITLE") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/TITLE>",strlen("/TITLE>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/TITLE"));
      if (len != 0) 
         *len = (strlen("/TITLE") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"BR>",strlen("BR>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("BR"));
      if (len != 0) 
         *len = (strlen("BR") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"HR>",strlen("HR>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("HR"));
      if (len != 0) 
         *len = (strlen("HR") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/FONT>",strlen("/FONT>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/FONT"));
      if (len != 0) 
         *len = (strlen("/FONT") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/A>",strlen("/A>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/A"));
      if (len != 0) 
         *len = (strlen("/A") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"P>",strlen("P>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("P"));
      if (len != 0) 
         *len = (strlen("P") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/P>",strlen("/P>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/P"));
      if (len != 0) 
         *len = (strlen("/P") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"H3>",strlen("H3>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("H3"));
      if (len != 0) 
         *len = (strlen("H3") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/H3>",strlen("/H3>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/H3"));
      if (len != 0) 
         *len = (strlen("/H3") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"HTML>",strlen("HTML>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("HTML"));
      if (len != 0) 
         *len = (strlen("HTML") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/HTML>",strlen("/HTML>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/HTML"));
      if (len != 0) 
         *len = (strlen("/HTML") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"BODY>",strlen("BODY>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("BODY"));
      if (len != 0) 
         *len = (strlen("BODY") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/BODY>",strlen("/BODY>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/BODY"));
      if (len != 0) 
         *len = (strlen("/BODY") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"FONT>",strlen("FONT>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("FONT"));
      if (len != 0) 
         *len = (strlen("FONT") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"HEAD>",strlen("HEAD>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("HEAD"));
      if (len != 0) 
         *len = (strlen("HEAD") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/HEAD>",strlen("/HEAD>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/HEAD"));
      if (len != 0) 
         *len = (strlen("/HEAD") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"BINARY>",strlen("BINARY>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("BINARY"));
      if (len != 0) 
         *len = (strlen("BINARY") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/BINARY>",strlen("/BINARY>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/BINARY"));
      if (len != 0) 
         *len = (strlen("/BINARY") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"HR ",strlen("HR ")) != 0)) {
      const gchar *c = (string + strlen("HR "));
      gchar e = '"';
      gboolean quote = 0;
{
        while(( *c) != 0){
          if ((( *c) == '"') || (( *c) == '\'')) {
            if ((quote != 0) && (( *c) == e)) 
              quote = !(quote != 0);
            else if (!(quote != 0)) {
              quote = !(quote != 0);
              e =  *c;
            }
          }
          else if (!(quote != 0) && (( *c) == '>')) 
            break; 
          c++;
        }
      }
      if (( *c) != 0) {
        if (tag != 0) 
           *tag = g_strndup(string,(c - string));
        if (len != 0) 
           *len = ((c - string) + 1);
        return (!0);
      }
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"FONT ",strlen("FONT ")) != 0)) {
      const gchar *c = (string + strlen("FONT "));
      gchar e = '"';
      gboolean quote = 0;
{
        while(( *c) != 0){
          if ((( *c) == '"') || (( *c) == '\'')) {
            if ((quote != 0) && (( *c) == e)) 
              quote = !(quote != 0);
            else if (!(quote != 0)) {
              quote = !(quote != 0);
              e =  *c;
            }
          }
          else if (!(quote != 0) && (( *c) == '>')) 
            break; 
          c++;
        }
      }
      if (( *c) != 0) {
        if (tag != 0) 
           *tag = g_strndup(string,(c - string));
        if (len != 0) 
           *len = ((c - string) + 1);
        return (!0);
      }
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"BODY ",strlen("BODY ")) != 0)) {
      const gchar *c = (string + strlen("BODY "));
      gchar e = '"';
      gboolean quote = 0;
{
        while(( *c) != 0){
          if ((( *c) == '"') || (( *c) == '\'')) {
            if ((quote != 0) && (( *c) == e)) 
              quote = !(quote != 0);
            else if (!(quote != 0)) {
              quote = !(quote != 0);
              e =  *c;
            }
          }
          else if (!(quote != 0) && (( *c) == '>')) 
            break; 
          c++;
        }
      }
      if (( *c) != 0) {
        if (tag != 0) 
           *tag = g_strndup(string,(c - string));
        if (len != 0) 
           *len = ((c - string) + 1);
        return (!0);
      }
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"A ",strlen("A ")) != 0)) {
      const gchar *c = (string + strlen("A "));
      gchar e = '"';
      gboolean quote = 0;
{
        while(( *c) != 0){
          if ((( *c) == '"') || (( *c) == '\'')) {
            if ((quote != 0) && (( *c) == e)) 
              quote = !(quote != 0);
            else if (!(quote != 0)) {
              quote = !(quote != 0);
              e =  *c;
            }
          }
          else if (!(quote != 0) && (( *c) == '>')) 
            break; 
          c++;
        }
      }
      if (( *c) != 0) {
        if (tag != 0) 
           *tag = g_strndup(string,(c - string));
        if (len != 0) 
           *len = ((c - string) + 1);
        return (!0);
      }
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"IMG ",strlen("IMG ")) != 0)) {
      const gchar *c = (string + strlen("IMG "));
      gchar e = '"';
      gboolean quote = 0;
{
        while(( *c) != 0){
          if ((( *c) == '"') || (( *c) == '\'')) {
            if ((quote != 0) && (( *c) == e)) 
              quote = !(quote != 0);
            else if (!(quote != 0)) {
              quote = !(quote != 0);
              e =  *c;
            }
          }
          else if (!(quote != 0) && (( *c) == '>')) 
            break; 
          c++;
        }
      }
      if (( *c) != 0) {
        if (tag != 0) 
           *tag = g_strndup(string,(c - string));
        if (len != 0) 
           *len = ((c - string) + 1);
        return (!0);
      }
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"P ",strlen("P ")) != 0)) {
      const gchar *c = (string + strlen("P "));
      gchar e = '"';
      gboolean quote = 0;
{
        while(( *c) != 0){
          if ((( *c) == '"') || (( *c) == '\'')) {
            if ((quote != 0) && (( *c) == e)) 
              quote = !(quote != 0);
            else if (!(quote != 0)) {
              quote = !(quote != 0);
              e =  *c;
            }
          }
          else if (!(quote != 0) && (( *c) == '>')) 
            break; 
          c++;
        }
      }
      if (( *c) != 0) {
        if (tag != 0) 
           *tag = g_strndup(string,(c - string));
        if (len != 0) 
           *len = ((c - string) + 1);
        return (!0);
      }
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"H3 ",strlen("H3 ")) != 0)) {
      const gchar *c = (string + strlen("H3 "));
      gchar e = '"';
      gboolean quote = 0;
{
        while(( *c) != 0){
          if ((( *c) == '"') || (( *c) == '\'')) {
            if ((quote != 0) && (( *c) == e)) 
              quote = !(quote != 0);
            else if (!(quote != 0)) {
              quote = !(quote != 0);
              e =  *c;
            }
          }
          else if (!(quote != 0) && (( *c) == '>')) 
            break; 
          c++;
        }
      }
      if (( *c) != 0) {
        if (tag != 0) 
           *tag = g_strndup(string,(c - string));
        if (len != 0) 
           *len = ((c - string) + 1);
        return (!0);
      }
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"HTML ",strlen("HTML ")) != 0)) {
      const gchar *c = (string + strlen("HTML "));
      gchar e = '"';
      gboolean quote = 0;
{
        while(( *c) != 0){
          if ((( *c) == '"') || (( *c) == '\'')) {
            if ((quote != 0) && (( *c) == e)) 
              quote = !(quote != 0);
            else if (!(quote != 0)) {
              quote = !(quote != 0);
              e =  *c;
            }
          }
          else if (!(quote != 0) && (( *c) == '>')) 
            break; 
          c++;
        }
      }
      if (( *c) != 0) {
        if (tag != 0) 
           *tag = g_strndup(string,(c - string));
        if (len != 0) 
           *len = ((c - string) + 1);
        return (!0);
      }
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"CITE>",strlen("CITE>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("CITE"));
      if (len != 0) 
         *len = (strlen("CITE") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/CITE>",strlen("/CITE>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/CITE"));
      if (len != 0) 
         *len = (strlen("/CITE") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"EM>",strlen("EM>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("EM"));
      if (len != 0) 
         *len = (strlen("EM") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/EM>",strlen("/EM>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/EM"));
      if (len != 0) 
         *len = (strlen("/EM") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"STRONG>",strlen("STRONG>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("STRONG"));
      if (len != 0) 
         *len = (strlen("STRONG") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/STRONG>",strlen("/STRONG>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/STRONG"));
      if (len != 0) 
         *len = (strlen("/STRONG") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"SPAN ",strlen("SPAN ")) != 0)) {
      const gchar *c = (string + strlen("SPAN "));
      gchar e = '"';
      gboolean quote = 0;
{
        while(( *c) != 0){
          if ((( *c) == '"') || (( *c) == '\'')) {
            if ((quote != 0) && (( *c) == e)) 
              quote = !(quote != 0);
            else if (!(quote != 0)) {
              quote = !(quote != 0);
              e =  *c;
            }
          }
          else if (!(quote != 0) && (( *c) == '>')) 
            break; 
          c++;
        }
      }
      if (( *c) != 0) {
        if (tag != 0) 
           *tag = g_strndup(string,(c - string));
        if (len != 0) 
           *len = ((c - string) + 1);
        return (!0);
      }
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"/SPAN>",strlen("/SPAN>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("/SPAN"));
      if (len != 0) 
         *len = (strlen("/SPAN") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
/* hack until gtkimhtml handles things better */
  do {
    if (!(g_ascii_strncasecmp(string,"BR/>",strlen("BR/>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("BR/"));
      if (len != 0) 
         *len = (strlen("BR/") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"IMG>",strlen("IMG>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("IMG"));
      if (len != 0) 
         *len = (strlen("IMG") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"SPAN>",strlen("SPAN>")) != 0)) {
      if (tag != 0) 
         *tag = g_strndup(string,strlen("SPAN"));
      if (len != 0) 
         *len = (strlen("SPAN") + 1);
      return (!0);
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  do {
    if (!(g_ascii_strncasecmp(string,"BR ",strlen("BR ")) != 0)) {
      const gchar *c = (string + strlen("BR "));
      gchar e = '"';
      gboolean quote = 0;
{
        while(( *c) != 0){
          if ((( *c) == '"') || (( *c) == '\'')) {
            if ((quote != 0) && (( *c) == e)) 
              quote = !(quote != 0);
            else if (!(quote != 0)) {
              quote = !(quote != 0);
              e =  *c;
            }
          }
          else if (!(quote != 0) && (( *c) == '>')) 
            break; 
          c++;
        }
      }
      if (( *c) != 0) {
        if (tag != 0) 
           *tag = g_strndup(string,(c - string));
        if (len != 0) 
           *len = ((c - string) + 1);
        return (!0);
      }
    }
    if (type != 0) 
      ( *type)++;
  }while (0);
  if (!(g_ascii_strncasecmp(string,"!--",strlen("!--")) != 0)) {
    gchar *e = strstr((string + strlen("!--")),"-->");
    if (e != 0) {
      if (len != 0) {
         *len = ((e - string) + (strlen("-->")));
        if (tag != 0) 
           *tag = g_strndup((string + strlen("!--")),(( *len) - strlen("!---->")));
      }
      return (!0);
    }
  }
  if (type != 0) 
     *type = (-1);
  if (len != 0) 
     *len = ((close - string) + 1);
  if (tag != 0) 
     *tag = g_strndup(string,(close - string));
  return (!0);
}

static gchar *gtk_imhtml_get_html_opt(gchar *tag,const gchar *opt)
{
  gchar *t = tag;
  gchar *e;
  gchar *a;
  gchar *val;
  gint len;
  const gchar *c;
  GString *ret;
{
    while(g_ascii_strncasecmp(t,opt,strlen(opt)) != 0){
      gboolean quote = 0;
      if (( *t) == 0) 
        break; 
      while((( *t) != 0) && !((( *t) == 32) && !(quote != 0))){
        if (( *t) == '"') 
          quote = !(quote != 0);
        t++;
      }
      while((( *t) != 0) && (( *t) == 32))
        t++;
    }
  }
  if (!(g_ascii_strncasecmp(t,opt,strlen(opt)) != 0)) {
    t += strlen(opt);
  }
  else {
    return 0;
  }
  if ((( *t) == '"') || (( *t) == '\'')) {
    e = (a = ++t);
    while((( *e) != 0) && (( *e) != ( *(t - 1))))
      e++;
    if (( *e) == 0) {
      return 0;
    }
    else 
      val = g_strndup(a,(e - a));
  }
  else {
    e = (a = t);
    while((( *e) != 0) && !((( *__ctype_b_loc())[(int )( *e)] & ((unsigned short )_ISspace)) != 0))
      e++;
    val = g_strndup(a,(e - a));
  }
  ret = g_string_new("");
  e = val;
  while(( *e) != 0){
    if ((c = purple_markup_unescape_entity(e,&len)) != 0) {
      ret = g_string_append(ret,c);
      e += len;
    }
    else {
      gunichar uni = g_utf8_get_char(e);
      ret = g_string_append_unichar(ret,uni);
      e = (e + g_utf8_skip[ *((const guchar *)e)]);
    }
  }
  g_free(val);
  return g_string_free(ret,0);
}
/* returns if the beginning of the text is a protocol. If it is the protocol, returns the length so
   the caller knows how long the protocol string is. */

static int gtk_imhtml_is_protocol(const char *text)
{
  GtkIMHtmlProtocol *proto = imhtml_find_protocol(text,0);
  return (proto != 0)?(proto -> length) : 0;
}
static gboolean smooth_scroll_cb(gpointer data);
/*
 <KingAnt> marv: The two IM image functions in oscar are purple_odc_send_im and purple_odc_incoming
[19:58] <Robot101> marv: images go into the imgstore, a refcounted... well.. hash. :)
[19:59] <KingAnt> marv: I think the image tag used by the core is something like <img id="#"/>
[19:59] Ro0tSiEgE robert42 RobFlynn Robot101 ross22 roz
[20:00] <KingAnt> marv: Where the ID is the what is returned when you add the image to the imgstore using purple_imgstore_add
[20:00] <marv> Robot101: so how does the image get passed to serv_got_im() and serv_send_im()? just as the <img id="#" and then the prpl looks it up from the store?
[20:00] <KingAnt> marv: Right
[20:00] <marv> alright
Here's my plan with IMImages. make gtk_imhtml_[append|insert]_text_with_images instead just
gtkimhtml_[append|insert]_text (hrm maybe it should be called html instead of text), add a
function for purple to register for look up images, i.e. gtk_imhtml_set_get_img_fnc, so that
images can be looked up like that, instead of passing a GSList of them.
 */

void gtk_imhtml_append_text_with_images(GtkIMHtml *imhtml,const gchar *text,GtkIMHtmlOptions options,GSList *unused)
{
  GtkTextIter iter;
  GtkTextIter ins;
  GtkTextIter sel;
  int ins_offset = 0;
  int sel_offset = 0;
  gboolean fixins = 0;
  gboolean fixsel = 0;
  do {
    if (imhtml != ((GtkIMHtml *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"imhtml != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)imhtml;
      GType __t = gtk_imhtml_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_IMHTML (imhtml)");
      return ;
    };
  }while (0);
  do {
    if (text != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"text != NULL");
      return ;
    };
  }while (0);
  gtk_text_buffer_get_end_iter((imhtml -> text_buffer),&iter);
  gtk_text_buffer_get_iter_at_mark((imhtml -> text_buffer),&ins,gtk_text_buffer_get_insert((imhtml -> text_buffer)));
  if ((gtk_text_iter_equal((&iter),(&ins)) != 0) && (gtk_text_buffer_get_selection_bounds((imhtml -> text_buffer),0,0) != 0)) {
    fixins = (!0);
    ins_offset = gtk_text_iter_get_offset((&ins));
  }
  gtk_text_buffer_get_iter_at_mark((imhtml -> text_buffer),&sel,gtk_text_buffer_get_selection_bound((imhtml -> text_buffer)));
  if ((gtk_text_iter_equal((&iter),(&sel)) != 0) && (gtk_text_buffer_get_selection_bounds((imhtml -> text_buffer),0,0) != 0)) {
    fixsel = (!0);
    sel_offset = gtk_text_iter_get_offset((&sel));
  }
  if (!((options & GTK_IMHTML_NO_SCROLL) != 0U)) {
    GdkRectangle rect;
    int y;
    int height;
    gtk_text_view_get_visible_rect(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),&rect);
    gtk_text_view_get_line_yrange(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),(&iter),&y,&height);
    if ((((y + height) - (rect.y + rect.height)) > height) && (gtk_text_buffer_get_char_count((imhtml -> text_buffer)) != 0)) {
/* If we are in the middle of smooth-scrolling, then take a scroll step.
			 * If we are not in the middle of smooth-scrolling, that means we were
			 * not looking at the end of the buffer before the new text was added,
			 * so do not scroll. */
      if ((imhtml -> scroll_time) != 0) 
        smooth_scroll_cb(imhtml);
      else 
        options |= GTK_IMHTML_NO_SCROLL;
    }
  }
  gtk_imhtml_insert_html_at_iter(imhtml,text,options,&iter);
  if (fixins != 0) {
    gtk_text_buffer_get_iter_at_offset((imhtml -> text_buffer),&ins,ins_offset);
    gtk_text_buffer_move_mark((imhtml -> text_buffer),gtk_text_buffer_get_insert((imhtml -> text_buffer)),(&ins));
  }
  if (fixsel != 0) {
    gtk_text_buffer_get_iter_at_offset((imhtml -> text_buffer),&sel,sel_offset);
    gtk_text_buffer_move_mark((imhtml -> text_buffer),gtk_text_buffer_get_selection_bound((imhtml -> text_buffer)),(&sel));
  }
  if (!((options & GTK_IMHTML_NO_SCROLL) != 0U)) {
    gtk_imhtml_scroll_to_end(imhtml,(options & GTK_IMHTML_USE_SMOOTHSCROLLING));
  }
}
#define MAX_SCROLL_TIME 0.4 /* seconds */
#define SCROLL_DELAY 33 /* milliseconds */
/*
 * Smoothly scroll a GtkIMHtml.
 *
 * @return TRUE if the window needs to be scrolled further, FALSE if we're at the bottom.
 */

static gboolean smooth_scroll_cb(gpointer data)
{
  GtkIMHtml *imhtml = data;
  GtkAdjustment *adj = ( *((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type())))).vadjustment;
  gdouble max_val = ((adj -> upper) - (adj -> page_size));
  gdouble scroll_val = (gtk_adjustment_get_value(adj) + ((max_val - gtk_adjustment_get_value(adj)) / 3));
  do {
    if ((imhtml -> scroll_time) != ((GTimer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"imhtml->scroll_time != NULL");
      return 0;
    };
  }while (0);
  if ((g_timer_elapsed((imhtml -> scroll_time),0) > 0.4) || (scroll_val >= max_val)) {
/* time's up. jump to the end and kill the timer */
    gtk_adjustment_set_value(adj,max_val);
    g_timer_destroy((imhtml -> scroll_time));
    imhtml -> scroll_time = ((GTimer *)((void *)0));
    g_source_remove((imhtml -> scroll_src));
    imhtml -> scroll_src = 0;
    return 0;
  }
/* scroll by 1/3rd the remaining distance */
  gtk_adjustment_set_value(adj,scroll_val);
  return (!0);
}

static gboolean scroll_idle_cb(gpointer data)
{
  GtkIMHtml *imhtml = data;
  GtkAdjustment *adj = ( *((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type())))).vadjustment;
  if (adj != 0) {
    gtk_adjustment_set_value(adj,((adj -> upper) - (adj -> page_size)));
  }
  imhtml -> scroll_src = 0;
  return 0;
}

void gtk_imhtml_scroll_to_end(GtkIMHtml *imhtml,gboolean smooth)
{
  if ((imhtml -> scroll_time) != 0) 
    g_timer_destroy((imhtml -> scroll_time));
  if ((imhtml -> scroll_src) != 0U) 
    g_source_remove((imhtml -> scroll_src));
  if (smooth != 0) {
    imhtml -> scroll_time = g_timer_new();
    imhtml -> scroll_src = g_timeout_add_full(300,33,smooth_scroll_cb,imhtml,0);
  }
  else {
    imhtml -> scroll_time = ((GTimer *)((void *)0));
    imhtml -> scroll_src = g_idle_add_full(300,scroll_idle_cb,imhtml,0);
  }
}
/* CSS colors are either rgb (x,y,z) or #hex
 * we need to convert to hex if it is RGB */

static gchar *parse_css_color(gchar *in_color)
{
  char *tmp = in_color;
  if ((((( *tmp) == 0x072) && (( *(++tmp)) == 'g')) && (( *(++tmp)) == 0x062)) && (( *(++tmp)) != 0)) {
    int rgbval[] = {(0), (0), (0)};
    int count = 0;
    const char *v_start;
    while((( *tmp) != 0) && ((g_ascii_table[(guchar )( *tmp)] & G_ASCII_SPACE) != 0))
      tmp++;
    if (( *tmp) != 40) {
/* We don't support rgba() */
      purple_debug_warning("gtkimhtml","Invalid rgb CSS color in \'%s\'!\n",in_color);
      return in_color;
    }
    tmp++;
    while(count < 3){
/* Skip any leading spaces */
      while((( *tmp) != 0) && ((g_ascii_table[(guchar )( *tmp)] & G_ASCII_SPACE) != 0))
        tmp++;
/* Find the subsequent contiguous digits */
      v_start = tmp;
      if (( *v_start) == 0x02d) 
        tmp++;
      while((( *tmp) != 0) && ((g_ascii_table[(guchar )( *tmp)] & G_ASCII_DIGIT) != 0))
        tmp++;
      if (tmp != v_start) {
        char prev =  *tmp;
         *tmp = 0;
        rgbval[count] = atoi(v_start);
         *tmp = prev;
/* deal with % */
        while((( *tmp) != 0) && ((g_ascii_table[(guchar )( *tmp)] & G_ASCII_SPACE) != 0))
          tmp++;
        if (( *tmp) == '%') {
          rgbval[count] = ((rgbval[count] / 100.0) * 255);
          tmp++;
        }
      }
      else {
        purple_debug_warning("gtkimhtml","Invalid rgb CSS color in \'%s\'!\n",in_color);
        return in_color;
      }
      if (rgbval[count] > 255) {
        rgbval[count] = 255;
      }
      else if (rgbval[count] < 0) {
        rgbval[count] = 0;
      }
      while((( *tmp) != 0) && ((g_ascii_table[(guchar )( *tmp)] & G_ASCII_SPACE) != 0))
        tmp++;
      if (( *tmp) == ',') 
        tmp++;
      count++;
    }
    g_free(in_color);
    return g_strdup_printf("#%02X%02X%02X",rgbval[0],rgbval[1],rgbval[2]);
  }
  return in_color;
}

void gtk_imhtml_insert_html_at_iter(GtkIMHtml *imhtml,const gchar *text,GtkIMHtmlOptions options,GtkTextIter *iter)
{
  GdkRectangle rect;
  gint pos = 0;
  gchar *ws;
  gchar *tag;
  gchar *bg = (gchar *)((void *)0);
  gint len;
  gint tlen;
  gint smilelen;
  gint wpos = 0;
  gint type;
  const gchar *c;
  const gchar *amp;
  gint len_protocol;
  guint bold = 0;
  guint italics = 0;
  guint underline = 0;
  guint strike = 0;
  guint sub = 0;
  guint sup = 0;
  guint title = 0;
  guint pre = 0;
  gboolean br = 0;
  gboolean align_right = 0;
  gboolean rtl_direction = 0;
  gint align_line = 0;
  GSList *fonts = (GSList *)((void *)0);
  GObject *object;
  GtkIMHtmlScalable *scalable = (GtkIMHtmlScalable *)((void *)0);
  do {
    if (imhtml != ((GtkIMHtml *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"imhtml != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)imhtml;
      GType __t = gtk_imhtml_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_IMHTML (imhtml)");
      return ;
    };
  }while (0);
  do {
    if (text != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"text != NULL");
      return ;
    };
  }while (0);
  c = text;
  len = (strlen(text));
  ws = (g_malloc((len + 1)));
  ws[0] = 0;
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"gtkimhtml_numsmileys_thismsg",0);
  gtk_text_buffer_begin_user_action((imhtml -> text_buffer));
{
    while(pos < len){
      if ((( *c) == 60) && (gtk_imhtml_is_tag((c + 1),&tag,&tlen,&type) != 0)) {
        c++;
        pos++;
        ws[wpos] = 0;
        br = 0;
{
          switch(type){
/* B */
            case 1:
{
            }
/* BOLD */
            case 2:
{
            }
/* STRONG */
            case 54:
{
              if (!((options & GTK_IMHTML_NO_FORMATTING) != 0U)) {
                gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
                if ((bold == 0) && (((imhtml -> format_functions) & GTK_IMHTML_BOLD) != 0)) 
                  gtk_imhtml_toggle_bold(imhtml);
                bold++;
                ws[0] = 0;
                wpos = 0;
              }
              break; 
            }
/* /B */
            case 3:
{
            }
/* /BOLD */
            case 4:
{
            }
/* /STRONG */
            case 55:
{
              if (!((options & GTK_IMHTML_NO_FORMATTING) != 0U)) {
                gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
                ws[0] = 0;
                wpos = 0;
                if (bold != 0U) {
                  bold--;
                  if (((bold == 0) && (((imhtml -> format_functions) & GTK_IMHTML_BOLD) != 0)) && !((imhtml -> wbfo) != 0)) 
                    gtk_imhtml_toggle_bold(imhtml);
                }
              }
              break; 
            }
/* I */
            case 5:
{
            }
/* ITALIC */
            case 6:
{
            }
/* EM */
            case 52:
{
              if (!((options & GTK_IMHTML_NO_FORMATTING) != 0U)) {
                gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
                ws[0] = 0;
                wpos = 0;
                if ((italics == 0) && (((imhtml -> format_functions) & GTK_IMHTML_ITALIC) != 0)) 
                  gtk_imhtml_toggle_italic(imhtml);
                italics++;
              }
              break; 
            }
/* /I */
            case 7:
{
            }
/* /ITALIC */
            case 8:
{
            }
/* /EM */
            case 53:
{
              if (!((options & GTK_IMHTML_NO_FORMATTING) != 0U)) {
                gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
                ws[0] = 0;
                wpos = 0;
                if (italics != 0U) {
                  italics--;
                  if (((italics == 0) && (((imhtml -> format_functions) & GTK_IMHTML_ITALIC) != 0)) && !((imhtml -> wbfo) != 0)) 
                    gtk_imhtml_toggle_italic(imhtml);
                }
              }
              break; 
            }
/* U */
            case 9:
{
            }
/* UNDERLINE */
            case 10:
{
              if (!((options & GTK_IMHTML_NO_FORMATTING) != 0U)) {
                gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
                ws[0] = 0;
                wpos = 0;
                if ((underline == 0) && (((imhtml -> format_functions) & GTK_IMHTML_UNDERLINE) != 0)) 
                  gtk_imhtml_toggle_underline(imhtml);
                underline++;
              }
              break; 
            }
/* /U */
            case 11:
{
            }
/* /UNDERLINE */
            case 12:
{
              if (!((options & GTK_IMHTML_NO_FORMATTING) != 0U)) {
                gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
                ws[0] = 0;
                wpos = 0;
                if (underline != 0U) {
                  underline--;
                  if (((underline == 0) && (((imhtml -> format_functions) & GTK_IMHTML_UNDERLINE) != 0)) && !((imhtml -> wbfo) != 0)) 
                    gtk_imhtml_toggle_underline(imhtml);
                }
              }
              break; 
            }
/* S */
            case 13:
{
            }
/* STRIKE */
            case 14:
{
              gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
              ws[0] = 0;
              wpos = 0;
              if ((strike == 0) && (((imhtml -> format_functions) & GTK_IMHTML_STRIKE) != 0)) 
                gtk_imhtml_toggle_strike(imhtml);
              strike++;
              break; 
            }
/* /S */
            case 15:
{
            }
/* /STRIKE */
            case 16:
{
              gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
              ws[0] = 0;
              wpos = 0;
              if (strike != 0U) 
                strike--;
              if (((strike == 0) && (((imhtml -> format_functions) & GTK_IMHTML_STRIKE) != 0)) && !((imhtml -> wbfo) != 0)) 
                gtk_imhtml_toggle_strike(imhtml);
              break; 
            }
/* SUB */
            case 17:
{
/* FIXME: reimpliment this */
              sub++;
              break; 
            }
/* /SUB */
            case 18:
{
/* FIXME: reimpliment this */
              if (sub != 0U) 
                sub--;
              break; 
            }
/* SUP */
            case 19:
{
/* FIXME: reimplement this */
              sup++;
              break; 
            }
/* /SUP */
            case 20:
{
/* FIXME: reimplement this */
              if (sup != 0U) 
                sup--;
              break; 
            }
/* PRE */
            case 21:
{
/* FIXME: reimplement this */
              pre++;
              break; 
            }
/* /PRE */
            case 22:
{
/* FIXME: reimplement this */
              if (pre != 0U) 
                pre--;
              break; 
            }
/* TITLE */
            case 23:
{
/* FIXME: what was this supposed to do anyway? */
              title++;
              break; 
            }
/* /TITLE */
            case 24:
{
/* FIXME: make this undo whatever 23 was supposed to do */
              if (title != 0U) {
                if ((options & GTK_IMHTML_NO_TITLE) != 0U) {
                  wpos = 0;
                  ws[wpos] = 0;
                }
                title--;
              }
              break; 
            }
/* BR */
            case 25:
{
            }
/* BR/ */
            case 58:
{
            }
/* BR (opt) */
            case 61:
{
              ws[wpos] = 10;
              wpos++;
              br = (!0);
              break; 
            }
/* HR */
            case 26:
{
            }
/* HR (opt) */
            case 42:
{
{
                int minus;
                struct scalable_data *sd = (struct scalable_data *)(g_malloc_n(1,(sizeof(struct scalable_data ))));
                ws[wpos++] = 10;
                gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
                sd -> scalable = (scalable = gtk_imhtml_hr_new());
                sd -> mark = gtk_text_buffer_create_mark((imhtml -> text_buffer),0,iter,(!0));
                gtk_text_view_get_visible_rect(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),&rect);
                ( *(scalable -> add_to))(scalable,imhtml,iter);
                minus = (gtk_text_view_get_left_margin(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type())))) + gtk_text_view_get_right_margin(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type())))));
                ( *(scalable -> scale))(scalable,(rect.width - minus),rect.height);
                imhtml -> scalables = g_list_append((imhtml -> scalables),sd);
                ws[0] = 0;
                wpos = 0;
                ws[wpos++] = 10;
                break; 
              }
            }
/* /FONT */
            case 27:
{
              if ((fonts != 0) && !((imhtml -> wbfo) != 0)) {
                GtkIMHtmlFontDetail *font = (fonts -> data);
                gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
                ws[0] = 0;
                wpos = 0;
/* NEW_BIT (NEW_TEXT_BIT); */
                if (((font -> face) != 0) && (((imhtml -> format_functions) & GTK_IMHTML_FACE) != 0)) {
                  gtk_imhtml_toggle_fontface(imhtml,0);
                }
                g_free((font -> face));
                if (((font -> fore) != 0) && (((imhtml -> format_functions) & GTK_IMHTML_FORECOLOR) != 0)) {
                  gtk_imhtml_toggle_forecolor(imhtml,0);
                }
                g_free((font -> fore));
                if (((font -> back) != 0) && (((imhtml -> format_functions) & GTK_IMHTML_BACKCOLOR) != 0)) {
                  gtk_imhtml_toggle_backcolor(imhtml,0);
                }
                g_free((font -> back));
                g_free((font -> sml));
                if (((font -> size) != 3) && (((imhtml -> format_functions) & (GTK_IMHTML_GROW | GTK_IMHTML_SHRINK)) != 0)) 
                  gtk_imhtml_font_set_size(imhtml,3);
                fonts = g_slist_remove(fonts,font);
                g_free(font);
                if (fonts != 0) {
                  GtkIMHtmlFontDetail *font = (fonts -> data);
                  if (((font -> face) != 0) && (((imhtml -> format_functions) & GTK_IMHTML_FACE) != 0)) 
                    gtk_imhtml_toggle_fontface(imhtml,(font -> face));
                  if (((font -> fore) != 0) && (((imhtml -> format_functions) & GTK_IMHTML_FORECOLOR) != 0)) 
                    gtk_imhtml_toggle_forecolor(imhtml,(font -> fore));
                  if (((font -> back) != 0) && (((imhtml -> format_functions) & GTK_IMHTML_BACKCOLOR) != 0)) 
                    gtk_imhtml_toggle_backcolor(imhtml,(font -> back));
                  if (((font -> size) != 3) && (((imhtml -> format_functions) & (GTK_IMHTML_GROW | GTK_IMHTML_SHRINK)) != 0)) 
                    gtk_imhtml_font_set_size(imhtml,(font -> size));
                }
              }
              break; 
            }
/* /A    */
            case 28:
{
              gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
              gtk_imhtml_toggle_link(imhtml,0);
              ws[0] = 0;
              wpos = 0;
              break; 
            }
/* P */
            case 29:
{
            }
/* /P */
            case 30:
{
            }
/* H3 */
            case 31:
{
            }
/* /H3 */
            case 32:
{
            }
/* HTML */
            case 33:
{
            }
/* /HTML */
            case 34:
{
            }
/* BODY */
            case 35:
{
              break; 
            }
/* /BODY */
            case 36:
{
              gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
              ws[0] = 0;
              wpos = 0;
              gtk_imhtml_toggle_background(imhtml,0);
              break; 
            }
/* FONT */
            case 37:
{
            }
/* HEAD */
            case 38:
{
            }
/* /HEAD */
            case 39:
{
            }
/* BINARY */
            case 40:
{
            }
/* /BINARY */
            case 41:
{
              break; 
            }
/* FONT (opt) */
            case 43:
{
{
                gchar *color;
                gchar *back;
                gchar *face;
                gchar *size;
                gchar *sml;
                GtkIMHtmlFontDetail *font;
                GtkIMHtmlFontDetail *oldfont = (GtkIMHtmlFontDetail *)((void *)0);
                color = gtk_imhtml_get_html_opt(tag,"COLOR=");
                back = gtk_imhtml_get_html_opt(tag,"BACK=");
                face = gtk_imhtml_get_html_opt(tag,"FACE=");
                size = gtk_imhtml_get_html_opt(tag,"SIZE=");
                sml = gtk_imhtml_get_html_opt(tag,"SML=");
                if (!(((((color != 0) || (back != 0)) || (face != 0)) || (size != 0)) || (sml != 0))) 
                  break; 
                gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
                ws[0] = 0;
                wpos = 0;
                font = ((GtkIMHtmlFontDetail *)(g_malloc0_n(1,(sizeof(GtkIMHtmlFontDetail )))));
                if (fonts != 0) 
                  oldfont = (fonts -> data);
                if (((color != 0) && !((options & GTK_IMHTML_NO_COLOURS) != 0U)) && (((imhtml -> format_functions) & GTK_IMHTML_FORECOLOR) != 0)) {
                  font -> fore = color;
                  gtk_imhtml_toggle_forecolor(imhtml,(font -> fore));
                }
                else 
                  g_free(color);
                if (((back != 0) && !((options & GTK_IMHTML_NO_COLOURS) != 0U)) && (((imhtml -> format_functions) & GTK_IMHTML_BACKCOLOR) != 0)) {
                  font -> back = back;
                  gtk_imhtml_toggle_backcolor(imhtml,(font -> back));
                }
                else 
                  g_free(back);
                if (((face != 0) && !((options & GTK_IMHTML_NO_FONTS) != 0U)) && (((imhtml -> format_functions) & GTK_IMHTML_FACE) != 0)) {
                  font -> face = face;
                  gtk_imhtml_toggle_fontface(imhtml,(font -> face));
                }
                else 
                  g_free(face);
                if (sml != 0) 
                  font -> sml = sml;
                else {
                  g_free(sml);
                  if ((oldfont != 0) && ((oldfont -> sml) != 0)) 
                    font -> sml = g_strdup((oldfont -> sml));
                }
                if (((size != 0) && !((options & GTK_IMHTML_NO_SIZES) != 0U)) && (((imhtml -> format_functions) & (GTK_IMHTML_GROW | GTK_IMHTML_SHRINK)) != 0)) {
                  if (( *size) == 0x02b) {
                    sscanf((size + 1),"%hd",&font -> size);
                    font -> size += 3;
                  }
                  else if (( *size) == 0x02d) {
                    sscanf((size + 1),"%hd",&font -> size);
                    font -> size = (((0 > (3 - (font -> size)))?0 : (3 - (font -> size))));
                  }
                  else if ((( *__ctype_b_loc())[(int )( *size)] & ((unsigned short )_ISdigit)) != 0) {
                    sscanf(size,"%hd",&font -> size);
                  }
                  if ((font -> size) > 100) 
                    font -> size = 100;
                }
                else if (oldfont != 0) 
                  font -> size = (oldfont -> size);
                else 
                  font -> size = 3;
                if ((((imhtml -> format_functions) & (GTK_IMHTML_GROW | GTK_IMHTML_SHRINK)) != 0) && (((font -> size) != 3) || ((oldfont != 0) && ((oldfont -> size) == 3)))) 
                  gtk_imhtml_font_set_size(imhtml,(font -> size));
                g_free(size);
                fonts = g_slist_prepend(fonts,font);
              }
              break; 
            }
/* BODY (opt) */
            case 44:
{
              if (!((options & GTK_IMHTML_NO_COLOURS) != 0U)) {
                char *bgcolor = gtk_imhtml_get_html_opt(tag,"BGCOLOR=");
                if ((bgcolor != 0) && (((imhtml -> format_functions) & GTK_IMHTML_BACKCOLOR) != 0)) {
                  gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
                  ws[0] = 0;
                  wpos = 0;
/* NEW_BIT(NEW_TEXT_BIT); */
                  g_free(bg);
                  bg = bgcolor;
                  gtk_imhtml_toggle_background(imhtml,bg);
                }
                else 
                  g_free(bgcolor);
              }
              break; 
            }
/* A (opt) */
            case 45:
{
{
                gchar *href = gtk_imhtml_get_html_opt(tag,"HREF=");
                if ((href != 0) && (((imhtml -> format_functions) & GTK_IMHTML_LINK) != 0)) {
                  gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
                  ws[0] = 0;
                  wpos = 0;
                  gtk_imhtml_toggle_link(imhtml,href);
                }
                g_free(href);
              }
              break; 
            }
/* IMG (opt) */
            case 46:
{
            }
/* IMG */
            case 59:
{
{
                char *id;
                gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
                ws[0] = 0;
                wpos = 0;
                if (!(((imhtml -> format_functions) & GTK_IMHTML_IMAGE) != 0)) 
                  break; 
                id = gtk_imhtml_get_html_opt(tag,"ID=");
                if (id != 0) {
                  gtk_imhtml_insert_image_at_iter(imhtml,atoi(id),iter);
                  g_free(id);
                }
                else {
                  char *src;
                  char *alt;
                  src = gtk_imhtml_get_html_opt(tag,"SRC=");
                  alt = gtk_imhtml_get_html_opt(tag,"ALT=");
                  if (src != 0) {
                    gtk_imhtml_toggle_link(imhtml,src);
                    gtk_text_buffer_insert((imhtml -> text_buffer),iter,((alt != 0)?alt : src),(-1));
                    gtk_imhtml_toggle_link(imhtml,0);
                  }
                  g_free(src);
                  g_free(alt);
                }
                break; 
              }
            }
/* P (opt) */
            case 47:
{
            }
/* H3 (opt) */
            case 48:
{
            }
/* HTML (opt) */
            case 49:
{
            }
/* CITE */
            case 50:
{
            }
/* /CITE */
            case 51:
{
            }
/* SPAN (opt) */
            case 56:
{
/* Inline CSS Support - Douglas Thrift
					 *
					 * color
					 * background
					 * font-family
					 * font-size
					 * text-decoration: underline
					 * font-weight: bold
					 * direction: rtl
					 * text-align: right
					 *
					 * TODO:
					 * background-color
					 * font-style
					 */
{
                gchar *style;
                gchar *color;
                gchar *background;
                gchar *family;
                gchar *size;
                gchar *direction;
                gchar *alignment;
                gchar *textdec;
                gchar *weight;
                GtkIMHtmlFontDetail *font;
                GtkIMHtmlFontDetail *oldfont = (GtkIMHtmlFontDetail *)((void *)0);
                style = gtk_imhtml_get_html_opt(tag,"style=");
                if (!(style != 0)) 
                  break; 
                color = purple_markup_get_css_property(style,"color");
                background = purple_markup_get_css_property(style,"background");
                family = purple_markup_get_css_property(style,"font-family");
                size = purple_markup_get_css_property(style,"font-size");
                textdec = purple_markup_get_css_property(style,"text-decoration");
                weight = purple_markup_get_css_property(style,"font-weight");
                direction = purple_markup_get_css_property(style,"direction");
                alignment = purple_markup_get_css_property(style,"text-align");
                if (!((((((((color != 0) || (family != 0)) || (size != 0)) || (background != 0)) || (textdec != 0)) || (weight != 0)) || (direction != 0)) || (alignment != 0))) {
                  g_free(style);
                  break; 
                }
                gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
                ws[0] = 0;
                wpos = 0;
/* NEW_BIT (NEW_TEXT_BIT); */
/* Bi-Directional text support */
                if ((direction != 0) && !(g_ascii_strncasecmp(direction,"RTL",3) != 0)) {
                  rtl_direction = (!0);
/* insert RLE character to set direction */
                  ws[wpos++] = 0xE2;
                  ws[wpos++] = 128;
                  ws[wpos++] = 0xAB;
                  ws[wpos] = 0;
                  gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
                  ws[0] = 0;
                  wpos = 0;
                }
                g_free(direction);
                if ((alignment != 0) && !(g_ascii_strncasecmp(alignment,"RIGHT",5) != 0)) {
                  align_right = (!0);
                  align_line = gtk_text_iter_get_line(iter);
                }
                g_free(alignment);
                font = ((GtkIMHtmlFontDetail *)(g_malloc0_n(1,(sizeof(GtkIMHtmlFontDetail )))));
                if (fonts != 0) 
                  oldfont = (fonts -> data);
                if (((color != 0) && !((options & GTK_IMHTML_NO_COLOURS) != 0U)) && (((imhtml -> format_functions) & GTK_IMHTML_FORECOLOR) != 0)) {
                  font -> fore = parse_css_color(color);
                  gtk_imhtml_toggle_forecolor(imhtml,(font -> fore));
                }
                else {
                  if ((oldfont != 0) && ((oldfont -> fore) != 0)) 
                    font -> fore = g_strdup((oldfont -> fore));
                  g_free(color);
                }
                if (((background != 0) && !((options & GTK_IMHTML_NO_COLOURS) != 0U)) && (((imhtml -> format_functions) & GTK_IMHTML_BACKCOLOR) != 0)) {
                  font -> back = parse_css_color(background);
                  gtk_imhtml_toggle_backcolor(imhtml,(font -> back));
                }
                else {
                  if ((oldfont != 0) && ((oldfont -> back) != 0)) 
                    font -> back = g_strdup((oldfont -> back));
                  g_free(background);
                }
                if (((family != 0) && !((options & GTK_IMHTML_NO_FONTS) != 0U)) && (((imhtml -> format_functions) & GTK_IMHTML_FACE) != 0)) {
                  font -> face = family;
                  gtk_imhtml_toggle_fontface(imhtml,(font -> face));
                }
                else {
                  if ((oldfont != 0) && ((oldfont -> face) != 0)) 
                    font -> face = g_strdup((oldfont -> face));
                  g_free(family);
                }
                if (((font -> face) != 0) && (atoi((font -> face)) > 100)) {
/* WTF is this? */
/* Maybe it sets a max size on the font face?  I seem to
							 * remember bad things happening if the font size was
							 * 2 billion */
                  g_free((font -> face));
                  font -> face = g_strdup("100");
                }
                if ((oldfont != 0) && ((oldfont -> sml) != 0)) 
                  font -> sml = g_strdup((oldfont -> sml));
                if (((size != 0) && !((options & GTK_IMHTML_NO_SIZES) != 0U)) && (((imhtml -> format_functions) & (GTK_IMHTML_SHRINK | GTK_IMHTML_GROW)) != 0)) {
                  if (g_ascii_strcasecmp(size,"xx-small") == 0) 
                    font -> size = 1;
                  else if ((g_ascii_strcasecmp(size,"smaller") == 0) || (g_ascii_strcasecmp(size,"x-small") == 0)) 
                    font -> size = 2;
                  else if (g_ascii_strcasecmp(size,"medium") == 0) 
                    font -> size = 3;
                  else if ((g_ascii_strcasecmp(size,"large") == 0) || (g_ascii_strcasecmp(size,"larger") == 0)) 
                    font -> size = 4;
                  else if (g_ascii_strcasecmp(size,"x-large") == 0) 
                    font -> size = 5;
                  else if (g_ascii_strcasecmp(size,"xx-large") == 0) 
                    font -> size = 6;
/*
							 * TODO: Handle other values, like percentages, or
							 * lengths specified as em, ex, px, in, cm, mm, pt
							 * or pc.  Or even better, use an actual HTML
							 * renderer like webkit.
							 */
                  if ((font -> size) > 0) 
                    gtk_imhtml_font_set_size(imhtml,(font -> size));
                }
                else if (oldfont != 0) {
                  font -> size = (oldfont -> size);
                }
                if (oldfont != 0) {
                  font -> underline = (oldfont -> underline);
                }
                if (((((textdec != 0) && ((font -> underline) != 1)) && (g_ascii_strcasecmp(textdec,"underline") == 0)) && (((imhtml -> format_functions) & GTK_IMHTML_UNDERLINE) != 0)) && !((options & GTK_IMHTML_NO_FORMATTING) != 0U)) {
                  gtk_imhtml_toggle_underline(imhtml);
                  font -> underline = 1;
                }
                if (oldfont != 0) {
                  font -> strike = (oldfont -> strike);
                }
                if (((((textdec != 0) && ((font -> strike) != 1)) && (g_ascii_strcasecmp(textdec,"line-through") == 0)) && (((imhtml -> format_functions) & GTK_IMHTML_STRIKE) != 0)) && !((options & GTK_IMHTML_NO_FORMATTING) != 0U)) {
                  gtk_imhtml_toggle_strike(imhtml);
                  font -> strike = 1;
                }
                g_free(textdec);
                if (oldfont != 0) {
                  font -> bold = (oldfont -> bold);
                }
                if (weight != 0) {
                  if (!(g_ascii_strcasecmp(weight,"normal") != 0)) {
                    font -> bold = 0;
                  }
                  else if (!(g_ascii_strcasecmp(weight,"bold") != 0)) {
                    font -> bold = 1;
                  }
                  else if (!(g_ascii_strcasecmp(weight,"bolder") != 0)) {
                    font -> bold++;
                  }
                  else if (!(g_ascii_strcasecmp(weight,"lighter") != 0)) {
                    if ((font -> bold) > 0) 
                      font -> bold--;
                  }
                  else {
                    int num = atoi(weight);
                    if (num >= 700) 
                      font -> bold = 1;
                    else 
                      font -> bold = 0;
                  }
                  if (((((((font -> bold) != 0) && (oldfont != 0)) && !((oldfont -> bold) != 0)) || (((oldfont != 0) && ((oldfont -> bold) != 0)) && !((font -> bold) != 0))) || (((font -> bold) != 0) && !(oldfont != 0))) && !((options & GTK_IMHTML_NO_FORMATTING) != 0U)) {
                    gtk_imhtml_toggle_bold(imhtml);
                  }
                  g_free(weight);
                }
                g_free(style);
                g_free(size);
                fonts = g_slist_prepend(fonts,font);
              }
              break; 
            }
/* /SPAN */
            case 57:
{
/* Inline CSS Support - Douglas Thrift */
              if ((fonts != 0) && !((imhtml -> wbfo) != 0)) {
                GtkIMHtmlFontDetail *oldfont = (GtkIMHtmlFontDetail *)((void *)0);
                GtkIMHtmlFontDetail *font = (fonts -> data);
                gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
                ws[0] = 0;
                wpos = 0;
/* NEW_BIT (NEW_TEXT_BIT); */
                fonts = g_slist_remove(fonts,font);
                if (fonts != 0) 
                  oldfont = (fonts -> data);
                if (!(oldfont != 0)) {
                  gtk_imhtml_font_set_size(imhtml,3);
                  if (((font -> underline) != 0) && !((options & GTK_IMHTML_NO_FORMATTING) != 0U)) 
                    gtk_imhtml_toggle_underline(imhtml);
                  if (((font -> strike) != 0) && !((options & GTK_IMHTML_NO_FORMATTING) != 0U)) 
                    gtk_imhtml_toggle_strike(imhtml);
                  if (((font -> bold) != 0) && !((options & GTK_IMHTML_NO_FORMATTING) != 0U)) 
                    gtk_imhtml_toggle_bold(imhtml);
                  if (!((options & GTK_IMHTML_NO_FONTS) != 0U)) 
                    gtk_imhtml_toggle_fontface(imhtml,0);
                  if (!((options & GTK_IMHTML_NO_COLOURS) != 0U)) 
                    gtk_imhtml_toggle_forecolor(imhtml,0);
                  if (!((options & GTK_IMHTML_NO_COLOURS) != 0U)) 
                    gtk_imhtml_toggle_backcolor(imhtml,0);
                }
                else {
                  if (((font -> size) != (oldfont -> size)) && !((options & GTK_IMHTML_NO_SIZES) != 0U)) 
                    gtk_imhtml_font_set_size(imhtml,(oldfont -> size));
                  if (((font -> underline) != (oldfont -> underline)) && !((options & GTK_IMHTML_NO_FORMATTING) != 0U)) 
                    gtk_imhtml_toggle_underline(imhtml);
                  if (((font -> strike) != (oldfont -> strike)) && !((options & GTK_IMHTML_NO_FORMATTING) != 0U)) 
                    gtk_imhtml_toggle_strike(imhtml);
                  if (((((font -> bold) != 0) && !((oldfont -> bold) != 0)) || (((oldfont -> bold) != 0) && !((font -> bold) != 0))) && !((options & GTK_IMHTML_NO_FORMATTING) != 0U)) 
                    gtk_imhtml_toggle_bold(imhtml);
                  if ((((font -> face) != 0) && (!((oldfont -> face) != 0) || (strcmp((font -> face),(oldfont -> face)) != 0))) && !((options & GTK_IMHTML_NO_FONTS) != 0U)) 
                    gtk_imhtml_toggle_fontface(imhtml,(oldfont -> face));
                  if ((((font -> fore) != 0) && (!((oldfont -> fore) != 0) || (strcmp((font -> fore),(oldfont -> fore)) != 0))) && !((options & GTK_IMHTML_NO_COLOURS) != 0U)) 
                    gtk_imhtml_toggle_forecolor(imhtml,(oldfont -> fore));
                  if ((((font -> back) != 0) && (!((oldfont -> back) != 0) || (strcmp((font -> back),(oldfont -> back)) != 0))) && !((options & GTK_IMHTML_NO_COLOURS) != 0U)) 
                    gtk_imhtml_toggle_backcolor(imhtml,(oldfont -> back));
                }
                g_free((font -> face));
                g_free((font -> fore));
                g_free((font -> back));
                g_free((font -> sml));
                g_free(font);
              }
              break; 
            }
/* SPAN */
            case 60:
{
              break; 
            }
/* comment */
            case 62:
{
/* NEW_BIT (NEW_TEXT_BIT); */
              ws[wpos] = 0;
              gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
#if FALSE && GTK_CHECK_VERSION(2,10,10)
#else
              if (((imhtml -> show_comments) != 0) && !((options & GTK_IMHTML_NO_COMMENTS) != 0U)) {
                wpos = g_snprintf(ws,len,"%s",tag);
                gtk_text_buffer_insert_with_tags_by_name((imhtml -> text_buffer),iter,ws,wpos,"comment",((void *)((void *)0)));
              }
#endif
              ws[0] = 0;
              wpos = 0;
/* NEW_BIT (NEW_COMMENT_BIT); */
              break; 
            }
            default:
{
              break; 
            }
          }
        }
        c += tlen;
        pos += tlen;
/* This was allocated back in VALID_TAG() */
        g_free(tag);
      }
      else if (((imhtml -> edit.link == ((GtkTextTag *)((void *)0))) && !((options & GTK_IMHTML_NO_SMILEY) != 0U)) && (gtk_imhtml_is_smiley(imhtml,fonts,c,&smilelen) != 0)) {
        GtkIMHtmlFontDetail *fd;
        gchar *sml = (gchar *)((void *)0);
        br = 0;
        if (fonts != 0) {
          fd = (fonts -> data);
          sml = (fd -> sml);
        }
        if (!(sml != 0)) 
          sml = (imhtml -> protocol_name);
        gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
        wpos = g_snprintf(ws,(smilelen + 1),"%s",c);
        gtk_imhtml_insert_smiley_at_iter(imhtml,sml,ws,iter);
        c += smilelen;
        pos += smilelen;
        wpos = 0;
        ws[0] = 0;
      }
      else if ((( *c) == '&') && ((amp = purple_markup_unescape_entity(c,&tlen)) != 0)) {
        br = 0;
        while(( *amp) != 0){
          ws[wpos++] =  *(amp++);
        }
        c += tlen;
        pos += tlen;
      }
      else if (( *c) == 10) {
        if (!((options & GTK_IMHTML_NO_NEWLINE) != 0U)) {
          ws[wpos] = 10;
          wpos++;
          gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
          ws[0] = 0;
          wpos = 0;
/* NEW_BIT (NEW_TEXT_BIT); */
/* Don't insert a space immediately after an HTML break */
        }
        else if (!(br != 0)) {
/* A newline is defined by HTML as whitespace, which means we have to replace it with a word boundary.
				 * word breaks vary depending on the language used, so the correct thing to do is to use Pango to determine
				 * what language this is, determine the proper word boundary to use, and insert that. I'm just going to insert
				 * a space instead.  What are the non-English speakers going to do?  Complain in a language I'll understand?
				 * Bu-wahaha! */
          ws[wpos] = 32;
          wpos++;
          gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
          ws[0] = 0;
          wpos = 0;
        }
        c++;
        pos++;
      }
      else if (((((((pos == 0) || (wpos == 0)) || ((( *__ctype_b_loc())[(int )( *(c - 1))] & ((unsigned short )_ISspace)) != 0)) && ((len_protocol = gtk_imhtml_is_protocol(c)) > 0)) && (c[len_protocol] != 0)) && !((( *__ctype_b_loc())[(int )c[len_protocol]] & ((unsigned short )_ISspace)) != 0)) && ((c[len_protocol] != 60) || !(gtk_imhtml_is_tag((c + 1),0,0,0) != 0))) {
        br = 0;
        if (wpos > 0) {
          gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
          ws[0] = 0;
          wpos = 0;
        }
        while(len_protocol-- != 0){
/* Skip the next len_protocol characters, but
				 * make sure they're copied into the ws array.
				 */
          ws[wpos++] =  *(c++);
          pos++;
        }
        if (!(imhtml -> edit.link != 0) && (((imhtml -> format_functions) & GTK_IMHTML_LINK) != 0)) {
          while(((( *c) != 0) && !((( *__ctype_b_loc())[(int )( *c)] & ((unsigned short )_ISspace)) != 0)) && ((( *c) != 60) || !(gtk_imhtml_is_tag((c + 1),0,0,0) != 0))){
            if ((( *c) == '&') && ((amp = purple_markup_unescape_entity(c,&tlen)) != 0)) {
              while(( *amp) != 0)
                ws[wpos++] =  *(amp++);
              c += tlen;
              pos += tlen;
            }
            else {
              ws[wpos++] =  *(c++);
              pos++;
            }
          }
          ws[wpos] = 0;
          gtk_imhtml_toggle_link(imhtml,ws);
          gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
          ws[0] = 0;
          wpos = 0;
          gtk_imhtml_toggle_link(imhtml,0);
        }
      }
      else if (( *c) != 0) {
        br = 0;
        ws[wpos++] =  *(c++);
        pos++;
      }
      else {
        break; 
      }
    }
  }
  gtk_text_buffer_insert((imhtml -> text_buffer),iter,ws,wpos);
  ws[0] = 0;
  wpos = 0;
/* NEW_BIT(NEW_TEXT_BIT); */
  if (align_right != 0) {
/* insert RLM+LRM at beginning of the line to set alignment */
    GtkTextIter line_iter;
    line_iter =  *iter;
    gtk_text_iter_set_line(&line_iter,align_line);
/* insert RLM character to set alignment */
    ws[wpos++] = 0xE2;
    ws[wpos++] = 128;
    ws[wpos++] = 0x8F;
    if (!(rtl_direction != 0)) {
/* insert LRM character to set direction */
/* (alignment=right and direction=LTR) */
      ws[wpos++] = 0xE2;
      ws[wpos++] = 128;
      ws[wpos++] = 0x8E;
    }
    ws[wpos] = 0;
    gtk_text_buffer_insert((imhtml -> text_buffer),&line_iter,ws,wpos);
    gtk_text_buffer_get_end_iter(gtk_text_iter_get_buffer((&line_iter)),iter);
    ws[0] = 0;
    wpos = 0;
  }
  while(fonts != 0){
    GtkIMHtmlFontDetail *font = (fonts -> data);
    fonts = g_slist_remove(fonts,font);
    g_free((font -> face));
    g_free((font -> fore));
    g_free((font -> back));
    g_free((font -> sml));
    g_free(font);
  }
  g_free(ws);
  g_free(bg);
  if (!((imhtml -> wbfo) != 0)) 
    gtk_imhtml_close_tags(imhtml,iter);
  object = (g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2)))))));
  g_signal_emit(object,signals[UPDATE_FORMAT],0);
  g_object_unref(object);
  gtk_text_buffer_end_user_action((imhtml -> text_buffer));
}

void gtk_imhtml_remove_smileys(GtkIMHtml *imhtml)
{
  g_hash_table_destroy((imhtml -> smiley_data));
  gtk_smiley_tree_destroy((imhtml -> default_smilies));
  imhtml -> smiley_data = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,((GDestroyNotify )gtk_smiley_tree_destroy));
  imhtml -> default_smilies = gtk_smiley_tree_new();
}

void gtk_imhtml_show_comments(GtkIMHtml *imhtml,gboolean show)
{
#if FALSE && GTK_CHECK_VERSION(2,10,10)
#endif
  imhtml -> show_comments = show;
}

const char *gtk_imhtml_get_protocol_name(GtkIMHtml *imhtml)
{
  return (imhtml -> protocol_name);
}

void gtk_imhtml_set_protocol_name(GtkIMHtml *imhtml,const gchar *protocol_name)
{
  g_free((imhtml -> protocol_name));
  imhtml -> protocol_name = g_strdup(protocol_name);
}

void gtk_imhtml_delete(GtkIMHtml *imhtml,GtkTextIter *start,GtkTextIter *end)
{
  GList *l;
  GSList *sl;
  GtkTextIter i;
  GtkTextIter i_s;
  GtkTextIter i_e;
  GObject *object = (g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2)))))));
  if (start == ((GtkTextIter *)((void *)0))) {
    gtk_text_buffer_get_start_iter((imhtml -> text_buffer),&i_s);
    start = &i_s;
  }
  if (end == ((GtkTextIter *)((void *)0))) {
    gtk_text_buffer_get_end_iter((imhtml -> text_buffer),&i_e);
    end = &i_e;
  }
  l = (imhtml -> scalables);
  while(l != 0){
    GList *next = (l -> next);
    struct scalable_data *sd = (l -> data);
    gtk_text_buffer_get_iter_at_mark((imhtml -> text_buffer),&i,(sd -> mark));
    if (gtk_text_iter_in_range((&i),start,end) != 0) {
      GtkIMHtmlScalable *scale = (GtkIMHtmlScalable *)(sd -> scalable);
      ( *(scale -> free))(scale);
      g_free(sd);
      imhtml -> scalables = g_list_delete_link((imhtml -> scalables),l);
    }
    l = next;
  }
  sl = (imhtml -> im_images);
  while(sl != 0){
    GSList *next = (sl -> next);
    struct im_image_data *img_data = (sl -> data);
    gtk_text_buffer_get_iter_at_mark((imhtml -> text_buffer),&i,(img_data -> mark));
    if (gtk_text_iter_in_range((&i),start,end) != 0) {
      if (( *(imhtml -> funcs)).image_unref != 0) 
        ( *( *(imhtml -> funcs)).image_unref)((img_data -> id));
      imhtml -> im_images = g_slist_delete_link((imhtml -> im_images),sl);
      g_free(img_data);
    }
    sl = next;
  }
  gtk_text_buffer_delete((imhtml -> text_buffer),start,end);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"gtkimhtml_numsmileys_total",0);
  g_object_unref(object);
}

void gtk_imhtml_page_up(GtkIMHtml *imhtml)
{
  GdkRectangle rect;
  GtkTextIter iter;
  gtk_text_view_get_visible_rect(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),&rect);
  gtk_text_view_get_iter_at_location(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),&iter,rect.x,(rect.y - rect.height));
  gtk_text_view_scroll_to_iter(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),&iter,0,(!0),0,0);
}

void gtk_imhtml_page_down(GtkIMHtml *imhtml)
{
  GdkRectangle rect;
  GtkTextIter iter;
  gtk_text_view_get_visible_rect(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),&rect);
  gtk_text_view_get_iter_at_location(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),&iter,rect.x,(rect.y + rect.height));
  gtk_text_view_scroll_to_iter(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),&iter,0,(!0),0,0);
}
/* GtkIMHtmlScalable, gtk_imhtml_image, gtk_imhtml_hr */

GtkIMHtmlScalable *gtk_imhtml_image_new(GdkPixbuf *img,const gchar *filename,int id)
{
  GtkIMHtmlImage *im_image = (g_malloc((sizeof(GtkIMHtmlImage ))));
  ( *((GtkIMHtmlScalable *)im_image)).scale = gtk_imhtml_image_scale;
  ( *((GtkIMHtmlScalable *)im_image)).add_to = gtk_imhtml_image_add_to;
  ( *((GtkIMHtmlScalable *)im_image)).free = gtk_imhtml_image_free;
  im_image -> pixbuf = img;
  im_image -> image = ((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_image_new_from_pixbuf((im_image -> pixbuf)))),gtk_image_get_type())));
  im_image -> width = gdk_pixbuf_get_width(img);
  im_image -> height = gdk_pixbuf_get_height(img);
  im_image -> mark = ((GtkTextMark *)((void *)0));
  im_image -> filename = g_strdup(filename);
  im_image -> id = id;
  im_image -> filesel = ((GtkWidget *)((void *)0));
  g_object_ref(img);
  return (GtkIMHtmlScalable *)im_image;
}

static gboolean animate_image_cb(gpointer data)
{
  GtkIMHtmlImage *im_image;
  int width;
  int height;
  int delay;
  im_image = data;
/* Update the pointer to this GdkPixbuf frame of the animation */
  if (gdk_pixbuf_animation_iter_advance(( *((GtkIMHtmlAnimation *)im_image)).iter,0) != 0) {
    GdkPixbuf *pb = gdk_pixbuf_animation_iter_get_pixbuf(( *((GtkIMHtmlAnimation *)im_image)).iter);
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(im_image -> pixbuf)),((GType )(20 << 2))))));
    im_image -> pixbuf = gdk_pixbuf_copy(pb);
/* Update the displayed GtkImage */
    width = gdk_pixbuf_get_width((gtk_image_get_pixbuf((im_image -> image))));
    height = gdk_pixbuf_get_height((gtk_image_get_pixbuf((im_image -> image))));
    if ((width > 0) && (height > 0)) {
/* Need to scale the new frame to the same size as the old frame */
      GdkPixbuf *tmp;
      tmp = gdk_pixbuf_scale_simple((im_image -> pixbuf),width,height,GDK_INTERP_BILINEAR);
      gtk_image_set_from_pixbuf((im_image -> image),tmp);
      g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tmp),((GType )(20 << 2))))));
    }
    else {
/* Display at full-size */
      gtk_image_set_from_pixbuf((im_image -> image),(im_image -> pixbuf));
    }
  }
  delay = ((gdk_pixbuf_animation_iter_get_delay_time(( *((GtkIMHtmlAnimation *)im_image)).iter) < 100)?gdk_pixbuf_animation_iter_get_delay_time(( *((GtkIMHtmlAnimation *)im_image)).iter) : 100);
  ( *((GtkIMHtmlAnimation *)im_image)).timer = g_timeout_add(delay,animate_image_cb,im_image);
  return 0;
}

GtkIMHtmlScalable *gtk_imhtml_animation_new(GdkPixbufAnimation *anim,const gchar *filename,int id)
{
  GtkIMHtmlImage *im_image = (GtkIMHtmlImage *)((GtkIMHtmlAnimation *)(g_malloc0_n(1,(sizeof(GtkIMHtmlAnimation )))));
  ( *((GtkIMHtmlScalable *)im_image)).scale = gtk_imhtml_image_scale;
  ( *((GtkIMHtmlScalable *)im_image)).add_to = gtk_imhtml_image_add_to;
  ( *((GtkIMHtmlScalable *)im_image)).free = gtk_imhtml_animation_free;
  ( *((GtkIMHtmlAnimation *)im_image)).anim = anim;
  if (gdk_pixbuf_animation_is_static_image(anim) != 0) {
    im_image -> pixbuf = gdk_pixbuf_animation_get_static_image(anim);
    g_object_ref((im_image -> pixbuf));
  }
  else {
    int delay;
    GdkPixbuf *pb;
    ( *((GtkIMHtmlAnimation *)im_image)).iter = gdk_pixbuf_animation_get_iter(anim,0);
    pb = gdk_pixbuf_animation_iter_get_pixbuf(( *((GtkIMHtmlAnimation *)im_image)).iter);
    im_image -> pixbuf = gdk_pixbuf_copy(pb);
    delay = ((gdk_pixbuf_animation_iter_get_delay_time(( *((GtkIMHtmlAnimation *)im_image)).iter) < 100)?gdk_pixbuf_animation_iter_get_delay_time(( *((GtkIMHtmlAnimation *)im_image)).iter) : 100);
    ( *((GtkIMHtmlAnimation *)im_image)).timer = g_timeout_add(delay,animate_image_cb,im_image);
  }
  im_image -> image = ((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_image_new_from_pixbuf((im_image -> pixbuf)))),gtk_image_get_type())));
  im_image -> width = gdk_pixbuf_animation_get_width(anim);
  im_image -> height = gdk_pixbuf_animation_get_height(anim);
  im_image -> filename = g_strdup(filename);
  im_image -> id = id;
  g_object_ref(anim);
  return (GtkIMHtmlScalable *)im_image;
}

void gtk_imhtml_image_scale(GtkIMHtmlScalable *scale,int width,int height)
{
  GtkIMHtmlImage *im_image = (GtkIMHtmlImage *)scale;
  if (((im_image -> width) > width) || ((im_image -> height) > height)) {
    double ratio_w;
    double ratio_h;
    double ratio;
    int new_h;
    int new_w;
    GdkPixbuf *new_image = (GdkPixbuf *)((void *)0);
    ratio_w = ((((double )width) - 2) / (im_image -> width));
    ratio_h = ((((double )height) - 2) / (im_image -> height));
    ratio = ((ratio_w < ratio_h)?ratio_w : ratio_h);
    new_w = ((int )((im_image -> width) * ratio));
    new_h = ((int )((im_image -> height) * ratio));
    new_image = gdk_pixbuf_scale_simple((im_image -> pixbuf),new_w,new_h,GDK_INTERP_BILINEAR);
    gtk_image_set_from_pixbuf((im_image -> image),new_image);
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)new_image),((GType )(20 << 2))))));
  }
  else if (gdk_pixbuf_get_width((gtk_image_get_pixbuf((im_image -> image)))) != (im_image -> width)) {
/* Enough space to show the full-size of the image. */
    GdkPixbuf *new_image;
    new_image = gdk_pixbuf_scale_simple((im_image -> pixbuf),(im_image -> width),(im_image -> height),GDK_INTERP_BILINEAR);
    gtk_image_set_from_pixbuf((im_image -> image),new_image);
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)new_image),((GType )(20 << 2))))));
  }
}

static void image_save_yes_cb(GtkIMHtmlImageSave *save,const char *filename)
{
  GError *error = (GError *)((void *)0);
  GtkIMHtmlImage *image = (GtkIMHtmlImage *)(save -> image);
  gtk_widget_destroy((image -> filesel));
  image -> filesel = ((GtkWidget *)((void *)0));
  if (((save -> data) != 0) && ((save -> datasize) != 0UL)) {
    g_file_set_contents(filename,(save -> data),(save -> datasize),&error);
  }
  else {
    gchar *type = (gchar *)((void *)0);
    GSList *formats = gdk_pixbuf_get_formats();
    char *newfilename;
{
      while(formats != 0){
        GdkPixbufFormat *format = (formats -> data);
        gchar **extensions = gdk_pixbuf_format_get_extensions(format);
        gpointer p = extensions;
{
          while(((gdk_pixbuf_format_is_writable(format) != 0) && (extensions != 0)) && (extensions[0] != 0)){
            gchar *fmt_ext = extensions[0];
            const gchar *file_ext = ((filename + strlen(filename)) - strlen(fmt_ext));
            if (!(g_ascii_strcasecmp(fmt_ext,file_ext) != 0)) {
              type = gdk_pixbuf_format_get_name(format);
              break; 
            }
            extensions++;
          }
        }
        g_strfreev(p);
        if (type != 0) 
          break; 
        formats = (formats -> next);
      }
    }
    g_slist_free(formats);
/* If I can't find a valid type, I will just tell the user about it and then assume
		   it's a png */
    if (!(type != 0)) {
      char *basename;
      char *tmp;
      char *dirname;
      GtkWidget *dialog = gtk_message_dialog_new_with_markup(0,0,GTK_MESSAGE_ERROR,GTK_BUTTONS_OK,((const char *)(dgettext("pidgin","<span size=\'larger\' weight=\'bold\'>Unrecognized file type</span>\n\nDefaulting to PNG."))));
      g_signal_connect_data(dialog,"response",((GCallback )gtk_widget_destroy),dialog,0,G_CONNECT_SWAPPED);
      gtk_widget_show(dialog);
      type = g_strdup("png");
      dirname = g_path_get_dirname(filename);
      basename = g_path_get_basename(filename);
      tmp = strrchr(basename,'.');
      if (tmp != ((char *)((void *)0))) 
        tmp[0] = 0;
      newfilename = g_strdup_printf("%s/%s.png",dirname,basename);
      g_free(dirname);
      g_free(basename);
    }
    else {
/*
			 * We're able to save the file in it's original format, so we
			 * can use the original file name.
			 */
      newfilename = g_strdup(filename);
    }
    gdk_pixbuf_save((image -> pixbuf),newfilename,type,&error,((void *)((void *)0)));
    g_free(newfilename);
    g_free(type);
  }
  if (error != 0) {
    GtkWidget *dialog = gtk_message_dialog_new_with_markup(0,0,GTK_MESSAGE_ERROR,GTK_BUTTONS_OK,((const char *)(dgettext("pidgin","<span size=\'larger\' weight=\'bold\'>Error saving image</span>\n\n%s"))),(error -> message));
    g_signal_connect_data(dialog,"response",((GCallback )gtk_widget_destroy),dialog,0,G_CONNECT_SWAPPED);
    gtk_widget_show(dialog);
    g_error_free(error);
  }
}

static void image_save_check_if_exists_cb(GtkWidget *widget,gint response,GtkIMHtmlImageSave *save)
{
  gchar *filename;
  GtkIMHtmlImage *image = (GtkIMHtmlImage *)(save -> image);
  if (response != GTK_RESPONSE_ACCEPT) {
    gtk_widget_destroy(widget);
    image -> filesel = ((GtkWidget *)((void *)0));
    return ;
  }
  filename = gtk_file_chooser_get_filename(((GtkFileChooser *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_file_chooser_get_type()))));
/*
	 * XXX - We should probably prompt the user to determine if they really
	 * want to overwrite the file or not.  However, I don't feel like doing
	 * that, so we're just always going to overwrite if the file exists.
	 */
/*
	if (g_file_test(filename, G_FILE_TEST_EXISTS)) {
	} else
		image_save_yes_cb(image, filename);
	*/
  image_save_yes_cb(save,filename);
  g_free(filename);
}

static void gtk_imhtml_image_save(GtkWidget *w,GtkIMHtmlImageSave *save)
{
  GtkIMHtmlImage *image = (GtkIMHtmlImage *)(save -> image);
  if ((image -> filesel) != ((GtkWidget *)((void *)0))) {
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(image -> filesel)),gtk_window_get_type()))));
    return ;
  }
  image -> filesel = gtk_file_chooser_dialog_new(((const char *)(dgettext("pidgin","Save Image"))),0,GTK_FILE_CHOOSER_ACTION_SAVE,"gtk-cancel",GTK_RESPONSE_CANCEL,"gtk-save",GTK_RESPONSE_ACCEPT,((void *)((void *)0)));
  gtk_dialog_set_default_response(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(image -> filesel)),gtk_dialog_get_type()))),GTK_RESPONSE_ACCEPT);
  if ((image -> filename) != ((gchar *)((void *)0))) 
    gtk_file_chooser_set_current_name(((GtkFileChooser *)(g_type_check_instance_cast(((GTypeInstance *)(image -> filesel)),gtk_file_chooser_get_type()))),(image -> filename));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)((GtkFileChooser *)(g_type_check_instance_cast(((GTypeInstance *)(image -> filesel)),gtk_file_chooser_get_type())))),((GType )(20 << 2))))),"response",((GCallback )image_save_check_if_exists_cb),save,0,((GConnectFlags )0));
  gtk_widget_show((image -> filesel));
}

static void gtk_imhtml_custom_smiley_save(GtkWidget *w,GtkIMHtmlImageSave *save)
{
  GtkIMHtmlImage *image = (GtkIMHtmlImage *)(save -> image);
/* Create an add dialog */
  PidginSmiley *editor = pidgin_smiley_edit(0,0);
  pidgin_smiley_editor_set_shortcut(editor,(image -> filename));
  pidgin_smiley_editor_set_image(editor,(image -> pixbuf));
  pidgin_smiley_editor_set_data(editor,(save -> data),(save -> datasize));
}
/*
 * So, um, AIM Direct IM lets you send any file, not just images.  You can
 * just insert a sound or a file or whatever in a conversation.  It's
 * basically like file transfer, except there is an icon to open the file
 * embedded in the conversation.  Someone should make the Purple core handle
 * all of that.
 */

static gboolean gtk_imhtml_image_clicked(GtkWidget *w,GdkEvent *event,GtkIMHtmlImageSave *save)
{
  GdkEventButton *event_button = (GdkEventButton *)event;
  GtkIMHtmlImage *image = (GtkIMHtmlImage *)(save -> image);
  if ((event -> type) == GDK_BUTTON_RELEASE) {
    if ((event_button -> button) == 3) {
      GtkWidget *img;
      GtkWidget *item;
      GtkWidget *menu;
      menu = gtk_menu_new();
/* buttons and such */
      img = gtk_image_new_from_stock("gtk-save",GTK_ICON_SIZE_MENU);
      item = gtk_image_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","_Save Image..."))));
      gtk_image_menu_item_set_image(((GtkImageMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_image_menu_item_get_type()))),img);
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"activate",((GCallback )gtk_imhtml_image_save),save,0,((GConnectFlags )0));
      gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item);
/* Add menu item for adding custom smiley to local smileys */
/* we only add the menu if the image is of "custom smiley size"
			  <= 96x96 pixels */
      if (((image -> width) <= 96) && ((image -> height) <= 96)) {
        img = gtk_image_new_from_stock("gtk-add",GTK_ICON_SIZE_MENU);
        item = gtk_image_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","_Add Custom Smiley..."))));
        gtk_image_menu_item_set_image(((GtkImageMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_image_menu_item_get_type()))),img);
        g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"activate",((GCallback )gtk_imhtml_custom_smiley_save),save,0,((GConnectFlags )0));
        gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item);
      }
      gtk_widget_show_all(menu);
      gtk_menu_popup(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_get_type()))),0,0,0,0,(event_button -> button),(event_button -> time));
      return (!0);
    }
  }
  if (((event -> type) == GDK_BUTTON_PRESS) && ((event_button -> button) == 3)) 
/* Clicking the right mouse button on a link shouldn't
						be caught by the regular GtkTextView menu */
    return (!0);
  else 
/* Let clicks go through if we didn't catch anything */
    return 0;
}

static gboolean gtk_imhtml_smiley_clicked(GtkWidget *w,GdkEvent *event,GtkIMHtmlSmiley *smiley)
{
  GdkPixbufAnimation *anim = (GdkPixbufAnimation *)((void *)0);
  GtkIMHtmlImageSave *save = (GtkIMHtmlImageSave *)((void *)0);
  gboolean ret;
  if (((event -> type) != GDK_BUTTON_RELEASE) || (( *((GdkEventButton *)event)).button != 3)) 
    return 0;
  anim = gtk_smiley_get_image(smiley);
  if (!(anim != 0)) 
    return 0;
  save = ((GtkIMHtmlImageSave *)(g_malloc0_n(1,(sizeof(GtkIMHtmlImageSave )))));
  save -> image = gtk_imhtml_animation_new(anim,(smiley -> smile),0);
/* Do not need to memdup here, since the smiley is not
	                                     destroyed before this GtkIMHtmlImageSave */
  save -> data = (smiley -> data);
  save -> datasize = (smiley -> datasize);
  ret = gtk_imhtml_image_clicked(w,event,save);
  g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)w),((GType )(20 << 2))))),"image-data",(save -> image),((GDestroyNotify )gtk_imhtml_animation_free));
  g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)w),((GType )(20 << 2))))),"image-save-data",save,((GDestroyNotify )g_free));
  return ret;
}

void gtk_imhtml_image_free(GtkIMHtmlScalable *scale)
{
  GtkIMHtmlImage *image = (GtkIMHtmlImage *)scale;
  g_object_unref((image -> pixbuf));
  g_free((image -> filename));
  if ((image -> filesel) != 0) 
    gtk_widget_destroy((image -> filesel));
  g_free(scale);
}

void gtk_imhtml_animation_free(GtkIMHtmlScalable *scale)
{
  GtkIMHtmlAnimation *animation = (GtkIMHtmlAnimation *)scale;
  if ((animation -> timer) > 0) 
    g_source_remove((animation -> timer));
  if ((animation -> iter) != ((GdkPixbufAnimationIter *)((void *)0))) 
    g_object_unref((animation -> iter));
  g_object_unref((animation -> anim));
  gtk_imhtml_image_free(scale);
}

void gtk_imhtml_image_add_to(GtkIMHtmlScalable *scale,GtkIMHtml *imhtml,GtkTextIter *iter)
{
  GtkIMHtmlImage *image = (GtkIMHtmlImage *)scale;
  GtkWidget *box = gtk_event_box_new();
  char *tag;
  GtkTextChildAnchor *anchor = gtk_text_buffer_create_child_anchor((imhtml -> text_buffer),iter);
  GtkIMHtmlImageSave *save;
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)box),gtk_container_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(image -> image)),gtk_widget_get_type()))));
  if (!(gtk_check_version(2,4,0) != 0)) 
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)box),((GType )(20 << 2))))),"visible-window",0,((void *)((void *)0)));
  gtk_widget_show(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(image -> image)),gtk_widget_get_type()))));
  gtk_widget_show(box);
  tag = g_strdup_printf("<IMG ID=\"%d\">",(image -> id));
  g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)anchor),((GType )(20 << 2))))),"gtkimhtml_htmltext",tag,g_free);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)anchor),((GType )(20 << 2))))),"gtkimhtml_plaintext","[Image]");
  gtk_text_view_add_child_at_anchor(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),box,anchor);
  save = ((GtkIMHtmlImageSave *)(g_malloc0_n(1,(sizeof(GtkIMHtmlImageSave )))));
  save -> image = scale;
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)box),((GType )(20 << 2))))),"event",((GCallback )gtk_imhtml_image_clicked),save,0,((GConnectFlags )0));
  g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)box),((GType )(20 << 2))))),"image-save-data",save,((GDestroyNotify )g_free));
}

GtkIMHtmlScalable *gtk_imhtml_hr_new()
{
  GtkIMHtmlHr *hr = (g_malloc((sizeof(GtkIMHtmlHr ))));
  ( *((GtkIMHtmlScalable *)hr)).scale = gtk_imhtml_hr_scale;
  ( *((GtkIMHtmlScalable *)hr)).add_to = gtk_imhtml_hr_add_to;
  ( *((GtkIMHtmlScalable *)hr)).free = gtk_imhtml_hr_free;
  hr -> sep = gtk_hseparator_new();
  gtk_widget_set_size_request((hr -> sep),5000,2);
  gtk_widget_show((hr -> sep));
  return (GtkIMHtmlScalable *)hr;
}

void gtk_imhtml_hr_scale(GtkIMHtmlScalable *scale,int width,int height)
{
  gtk_widget_set_size_request(( *((GtkIMHtmlHr *)scale)).sep,(width - 2),2);
}

void gtk_imhtml_hr_add_to(GtkIMHtmlScalable *scale,GtkIMHtml *imhtml,GtkTextIter *iter)
{
  GtkIMHtmlHr *hr = (GtkIMHtmlHr *)scale;
  GtkTextChildAnchor *anchor = gtk_text_buffer_create_child_anchor((imhtml -> text_buffer),iter);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)anchor),((GType )(20 << 2))))),"gtkimhtml_htmltext","<hr>");
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)anchor),((GType )(20 << 2))))),"gtkimhtml_plaintext","\n---\n");
  gtk_text_view_add_child_at_anchor(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),(hr -> sep),anchor);
}

void gtk_imhtml_hr_free(GtkIMHtmlScalable *scale)
{
  g_free(scale);
}

gboolean gtk_imhtml_search_find(GtkIMHtml *imhtml,const gchar *text)
{
  GtkTextIter iter;
  GtkTextIter start;
  GtkTextIter end;
  gboolean new_search = (!0);
  GtkTextMark *start_mark;
  do {
    if (imhtml != ((GtkIMHtml *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"imhtml != NULL");
      return 0;
    };
  }while (0);
  do {
    if (text != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"text != NULL");
      return 0;
    };
  }while (0);
  start_mark = gtk_text_buffer_get_mark((imhtml -> text_buffer),"search");
  if (((start_mark != 0) && ((imhtml -> search_string) != 0)) && !(strcmp(text,(imhtml -> search_string)) != 0)) 
    new_search = 0;
  if (new_search != 0) {
    gtk_imhtml_search_clear(imhtml);
    g_free((imhtml -> search_string));
    imhtml -> search_string = g_strdup(text);
    gtk_text_buffer_get_end_iter((imhtml -> text_buffer),&iter);
  }
  else {
    gtk_text_buffer_get_iter_at_mark((imhtml -> text_buffer),&iter,start_mark);
  }
  if (gtk_source_iter_backward_search((&iter),(imhtml -> search_string),(GTK_SOURCE_SEARCH_VISIBLE_ONLY | GTK_SOURCE_SEARCH_CASE_INSENSITIVE),&start,&end,0) != 0) {
    gtk_text_view_scroll_to_iter(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),&start,0,(!0),0,0);
    gtk_text_buffer_create_mark((imhtml -> text_buffer),"search",(&start),0);
    if (new_search != 0) {
      gtk_text_buffer_remove_tag_by_name((imhtml -> text_buffer),"search",(&iter),(&end));
      do 
        gtk_text_buffer_apply_tag_by_name((imhtml -> text_buffer),"search",(&start),(&end));while (gtk_source_iter_backward_search((&start),(imhtml -> search_string),(GTK_SOURCE_SEARCH_VISIBLE_ONLY | GTK_SOURCE_SEARCH_CASE_INSENSITIVE),&start,&end,0) != 0);
    }
    return (!0);
  }
  else if (!(new_search != 0)) {
/* We hit the end, so start at the beginning again. */
    gtk_text_buffer_get_end_iter((imhtml -> text_buffer),&iter);
    if (gtk_source_iter_backward_search((&iter),(imhtml -> search_string),(GTK_SOURCE_SEARCH_VISIBLE_ONLY | GTK_SOURCE_SEARCH_CASE_INSENSITIVE),&start,&end,0) != 0) {
      gtk_text_view_scroll_to_iter(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),&start,0,(!0),0,0);
      gtk_text_buffer_create_mark((imhtml -> text_buffer),"search",(&start),0);
      return (!0);
    }
  }
  return 0;
}

void gtk_imhtml_search_clear(GtkIMHtml *imhtml)
{
  GtkTextIter start;
  GtkTextIter end;
  do {
    if (imhtml != ((GtkIMHtml *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"imhtml != NULL");
      return ;
    };
  }while (0);
  gtk_text_buffer_get_start_iter((imhtml -> text_buffer),&start);
  gtk_text_buffer_get_end_iter((imhtml -> text_buffer),&end);
  gtk_text_buffer_remove_tag_by_name((imhtml -> text_buffer),"search",(&start),(&end));
  g_free((imhtml -> search_string));
  imhtml -> search_string = ((gchar *)((void *)0));
}

static GtkTextTag *find_font_forecolor_tag(GtkIMHtml *imhtml,gchar *color)
{
  gchar str[18UL];
  GtkTextTag *tag;
  g_snprintf(str,(sizeof(str)),"FORECOLOR %s",color);
  tag = gtk_text_tag_table_lookup(gtk_text_buffer_get_tag_table((imhtml -> text_buffer)),str);
  if (!(tag != 0)) {
    GdkColor gcolor;
    if (!(gdk_color_parse(color,&gcolor) != 0)) {
      gchar tmp[8UL];
      tmp[0] = '#';
      strncpy((tmp + 1),color,7);
      tmp[7] = 0;
      if (!(gdk_color_parse(tmp,&gcolor) != 0)) 
        gdk_color_parse("black",&gcolor);
    }
    tag = gtk_text_buffer_create_tag((imhtml -> text_buffer),str,"foreground-gdk",&gcolor,((void *)((void *)0)));
  }
  return tag;
}

static GtkTextTag *find_font_backcolor_tag(GtkIMHtml *imhtml,gchar *color)
{
  gchar str[18UL];
  GtkTextTag *tag;
  g_snprintf(str,(sizeof(str)),"BACKCOLOR %s",color);
  tag = gtk_text_tag_table_lookup(gtk_text_buffer_get_tag_table((imhtml -> text_buffer)),str);
  if (!(tag != 0)) {
    GdkColor gcolor;
    if (!(gdk_color_parse(color,&gcolor) != 0)) {
      gchar tmp[8UL];
      tmp[0] = '#';
      strncpy((tmp + 1),color,7);
      tmp[7] = 0;
      if (!(gdk_color_parse(tmp,&gcolor) != 0)) 
        gdk_color_parse("white",&gcolor);
    }
    tag = gtk_text_buffer_create_tag((imhtml -> text_buffer),str,"background-gdk",&gcolor,((void *)((void *)0)));
  }
  return tag;
}

static GtkTextTag *find_font_background_tag(GtkIMHtml *imhtml,gchar *color)
{
  gchar str[19UL];
  GtkTextTag *tag;
  g_snprintf(str,(sizeof(str)),"BACKGROUND %s",color);
  tag = gtk_text_tag_table_lookup(gtk_text_buffer_get_tag_table((imhtml -> text_buffer)),str);
  if (!(tag != 0)) 
    tag = gtk_text_buffer_create_tag((imhtml -> text_buffer),str,0);
  return tag;
}

static GtkTextTag *find_font_face_tag(GtkIMHtml *imhtml,gchar *face)
{
  gchar str[256UL];
  GtkTextTag *tag;
  g_snprintf(str,(sizeof(str)),"FONT FACE %s",face);
  str[255] = 0;
  tag = gtk_text_tag_table_lookup(gtk_text_buffer_get_tag_table((imhtml -> text_buffer)),str);
  if (!(tag != 0)) 
    tag = gtk_text_buffer_create_tag((imhtml -> text_buffer),str,"family",face,((void *)((void *)0)));
  return tag;
}

static GtkTextTag *find_font_size_tag(GtkIMHtml *imhtml,int size)
{
  gchar str[24UL];
  GtkTextTag *tag;
  g_snprintf(str,(sizeof(str)),"FONT SIZE %d",size);
  str[23] = 0;
  tag = gtk_text_tag_table_lookup(gtk_text_buffer_get_tag_table((imhtml -> text_buffer)),str);
  if (!(tag != 0)) {
/* For reasons I don't understand, setting "scale" here scaled
		 * based on some default size other than my theme's default
		 * size. Our size 4 was actually smaller than our size 3 for
		 * me. So this works around that oddity.
		 */
    GtkTextAttributes *attr = gtk_text_view_get_default_attributes(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))));
    tag = gtk_text_buffer_create_tag((imhtml -> text_buffer),str,"size",((gint )((pango_font_description_get_size((attr -> font))) * ((double )_point_sizes[((((((size > 0)?size : 1)) < 7)?(((size > 0)?size : 1)) : 7)) - 1]))),((void *)((void *)0)));
    gtk_text_attributes_unref(attr);
  }
  return tag;
}

static void remove_tag_by_prefix(GtkIMHtml *imhtml,const GtkTextIter *i,const GtkTextIter *e,const char *prefix,guint len,gboolean homo)
{
  GSList *tags;
  GSList *l;
  GtkTextIter iter;
  tags = gtk_text_iter_get_tags(i);
  for (l = tags; l != 0; l = (l -> next)) {
    GtkTextTag *tag = (l -> data);
    if (((tag -> name) != 0) && !(strncmp((tag -> name),prefix,len) != 0)) 
      gtk_text_buffer_remove_tag((imhtml -> text_buffer),tag,i,e);
  }
  g_slist_free(tags);
  if (homo != 0) 
    return ;
  iter =  *i;
  while((gtk_text_iter_forward_char(&iter) != 0) && !(gtk_text_iter_equal((&iter),e) != 0)){
    if (gtk_text_iter_begins_tag((&iter),0) != 0) {
      tags = gtk_text_iter_get_toggled_tags((&iter),(!0));
      for (l = tags; l != 0; l = (l -> next)) {
        GtkTextTag *tag = (l -> data);
        if (((tag -> name) != 0) && !(strncmp((tag -> name),prefix,len) != 0)) 
          gtk_text_buffer_remove_tag((imhtml -> text_buffer),tag,(&iter),e);
      }
      g_slist_free(tags);
    }
  }
}

static void remove_font_size(GtkIMHtml *imhtml,GtkTextIter *i,GtkTextIter *e,gboolean homo)
{
  remove_tag_by_prefix(imhtml,i,e,"FONT SIZE ",10,homo);
}

static void remove_font_face(GtkIMHtml *imhtml,GtkTextIter *i,GtkTextIter *e,gboolean homo)
{
  remove_tag_by_prefix(imhtml,i,e,"FONT FACE ",10,homo);
}

static void remove_font_forecolor(GtkIMHtml *imhtml,GtkTextIter *i,GtkTextIter *e,gboolean homo)
{
  remove_tag_by_prefix(imhtml,i,e,"FORECOLOR ",10,homo);
}

static void remove_font_backcolor(GtkIMHtml *imhtml,GtkTextIter *i,GtkTextIter *e,gboolean homo)
{
  remove_tag_by_prefix(imhtml,i,e,"BACKCOLOR ",10,homo);
}

static void remove_font_background(GtkIMHtml *imhtml,GtkTextIter *i,GtkTextIter *e,gboolean homo)
{
  remove_tag_by_prefix(imhtml,i,e,"BACKGROUND ",10,homo);
}

static void remove_font_link(GtkIMHtml *imhtml,GtkTextIter *i,GtkTextIter *e,gboolean homo)
{
  remove_tag_by_prefix(imhtml,i,e,"LINK ",5,homo);
}

static void imhtml_clear_formatting(GtkIMHtml *imhtml)
{
  GtkTextIter start;
  GtkTextIter end;
  if (!((imhtml -> editable) != 0)) 
    return ;
  if (!(imhtml_get_iter_bounds(imhtml,&start,&end) != 0)) 
    return ;
  gtk_text_buffer_remove_tag_by_name((imhtml -> text_buffer),"BOLD",(&start),(&end));
  gtk_text_buffer_remove_tag_by_name((imhtml -> text_buffer),"ITALICS",(&start),(&end));
  gtk_text_buffer_remove_tag_by_name((imhtml -> text_buffer),"UNDERLINE",(&start),(&end));
  gtk_text_buffer_remove_tag_by_name((imhtml -> text_buffer),"STRIKE",(&start),(&end));
  remove_font_size(imhtml,&start,&end,0);
  remove_font_face(imhtml,&start,&end,0);
  remove_font_forecolor(imhtml,&start,&end,0);
  remove_font_backcolor(imhtml,&start,&end,0);
  remove_font_background(imhtml,&start,&end,0);
  remove_font_link(imhtml,&start,&end,0);
  imhtml -> edit.bold = 0;
  imhtml -> edit.italic = 0;
  imhtml -> edit.underline = 0;
  imhtml -> edit.strike = 0;
  imhtml -> edit.fontsize = 0;
  g_free(imhtml -> edit.fontface);
  imhtml -> edit.fontface = ((gchar *)((void *)0));
  g_free(imhtml -> edit.forecolor);
  imhtml -> edit.forecolor = ((gchar *)((void *)0));
  g_free(imhtml -> edit.backcolor);
  imhtml -> edit.backcolor = ((gchar *)((void *)0));
  g_free(imhtml -> edit.background);
  imhtml -> edit.background = ((gchar *)((void *)0));
}
/* Editable stuff */

static void preinsert_cb(GtkTextBuffer *buffer,GtkTextIter *iter,gchar *text,gint len,GtkIMHtml *imhtml)
{
  imhtml -> insert_offset = gtk_text_iter_get_offset(iter);
}

static void insert_ca_cb(GtkTextBuffer *buffer,GtkTextIter *arg1,GtkTextChildAnchor *arg2,gpointer user_data)
{
  GtkTextIter start;
  start =  *arg1;
  gtk_text_iter_backward_char(&start);
  gtk_imhtml_apply_tags_on_insert(user_data,&start,arg1);
}

static void insert_cb(GtkTextBuffer *buffer,GtkTextIter *end,gchar *text,gint len,GtkIMHtml *imhtml)
{
  GtkTextIter start;
  if (!(len != 0)) 
    return ;
  start =  *end;
  gtk_text_iter_set_offset(&start,(imhtml -> insert_offset));
  gtk_imhtml_apply_tags_on_insert(imhtml,&start,end);
}

static void delete_cb(GtkTextBuffer *buffer,GtkTextIter *start,GtkTextIter *end,GtkIMHtml *imhtml)
{
  GSList *tags;
  GSList *l;
  tags = gtk_text_iter_get_tags(start);
  for (l = tags; l != ((GSList *)((void *)0)); l = (l -> next)) {
    GtkTextTag *tag = (GtkTextTag *)(g_type_check_instance_cast(((GTypeInstance *)(l -> data)),gtk_text_tag_get_type()));
/* Remove the formatting only if */
    if ((((tag != 0) && (gtk_text_iter_starts_word(start) != 0)) && (gtk_text_iter_begins_tag(start,tag) != 0)) && (!(gtk_text_iter_has_tag(end,tag) != 0) || (gtk_text_iter_ends_tag(end,tag) != 0))) 
/* beginning of a word */
/* the tag starts with the selection */
/* the tag ends within the selection */
{
      gtk_text_buffer_remove_tag((imhtml -> text_buffer),tag,start,end);
      if ((((tag -> name) != 0) && (strncmp((tag -> name),"LINK ",5) == 0)) && (imhtml -> edit.link != 0)) {
        gtk_imhtml_toggle_link(imhtml,0);
      }
    }
  }
  g_slist_free(tags);
}

static void gtk_imhtml_apply_tags_on_insert(GtkIMHtml *imhtml,GtkTextIter *start,GtkTextIter *end)
{
  if (imhtml -> edit.bold != 0) 
    gtk_text_buffer_apply_tag_by_name((imhtml -> text_buffer),"BOLD",start,end);
  else 
    gtk_text_buffer_remove_tag_by_name((imhtml -> text_buffer),"BOLD",start,end);
  if (imhtml -> edit.italic != 0) 
    gtk_text_buffer_apply_tag_by_name((imhtml -> text_buffer),"ITALICS",start,end);
  else 
    gtk_text_buffer_remove_tag_by_name((imhtml -> text_buffer),"ITALICS",start,end);
  if (imhtml -> edit.underline != 0) 
    gtk_text_buffer_apply_tag_by_name((imhtml -> text_buffer),"UNDERLINE",start,end);
  else 
    gtk_text_buffer_remove_tag_by_name((imhtml -> text_buffer),"UNDERLINE",start,end);
  if (imhtml -> edit.strike != 0) 
    gtk_text_buffer_apply_tag_by_name((imhtml -> text_buffer),"STRIKE",start,end);
  else 
    gtk_text_buffer_remove_tag_by_name((imhtml -> text_buffer),"STRIKE",start,end);
  if (imhtml -> edit.forecolor != 0) {
    remove_font_forecolor(imhtml,start,end,(!0));
    gtk_text_buffer_apply_tag((imhtml -> text_buffer),find_font_forecolor_tag(imhtml,imhtml -> edit.forecolor),start,end);
  }
  if (imhtml -> edit.backcolor != 0) {
    remove_font_backcolor(imhtml,start,end,(!0));
    gtk_text_buffer_apply_tag((imhtml -> text_buffer),find_font_backcolor_tag(imhtml,imhtml -> edit.backcolor),start,end);
  }
  if (imhtml -> edit.background != 0) {
    remove_font_background(imhtml,start,end,(!0));
    gtk_text_buffer_apply_tag((imhtml -> text_buffer),find_font_background_tag(imhtml,imhtml -> edit.background),start,end);
  }
  if (imhtml -> edit.fontface != 0) {
    remove_font_face(imhtml,start,end,(!0));
    gtk_text_buffer_apply_tag((imhtml -> text_buffer),find_font_face_tag(imhtml,imhtml -> edit.fontface),start,end);
  }
  if (imhtml -> edit.fontsize != 0) {
    remove_font_size(imhtml,start,end,(!0));
    gtk_text_buffer_apply_tag((imhtml -> text_buffer),find_font_size_tag(imhtml,imhtml -> edit.fontsize),start,end);
  }
  if (imhtml -> edit.link != 0) {
    remove_font_link(imhtml,start,end,(!0));
    gtk_text_buffer_apply_tag((imhtml -> text_buffer),imhtml -> edit.link,start,end);
  }
}

void gtk_imhtml_set_editable(GtkIMHtml *imhtml,gboolean editable)
{
  gtk_text_view_set_editable(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),editable);
/*
	 * We need a visible caret for accessibility, so mouseless
	 * people can highlight stuff.
	 */
/* gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(imhtml), editable); */
  if ((editable != 0) && !((imhtml -> editable) != 0)) {
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).text_buffer),((GType )(20 << 2))))),"mark-set",((GCallback )mark_set_cb),imhtml,0,G_CONNECT_AFTER);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"backspace",((GCallback )smart_backspace_cb),0,0,((GConnectFlags )0));
  }
  else if (!(editable != 0) && ((imhtml -> editable) != 0)) {
    g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type())))).text_buffer),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,mark_set_cb,imhtml);
    g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,smart_backspace_cb,0);
  }
  imhtml -> editable = editable;
  imhtml -> format_functions = GTK_IMHTML_ALL;
}

void gtk_imhtml_set_whole_buffer_formatting_only(GtkIMHtml *imhtml,gboolean wbfo)
{
  do {
    if (imhtml != ((GtkIMHtml *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"imhtml != NULL");
      return ;
    };
  }while (0);
  imhtml -> wbfo = wbfo;
}

void gtk_imhtml_set_format_functions(GtkIMHtml *imhtml,GtkIMHtmlButtons buttons)
{
  GObject *object = (g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2)))))));
  imhtml -> format_functions = buttons;
  g_signal_emit(object,signals[BUTTONS_UPDATE],0,buttons);
  g_object_unref(object);
}

GtkIMHtmlButtons gtk_imhtml_get_format_functions(GtkIMHtml *imhtml)
{
  return imhtml -> format_functions;
}

void gtk_imhtml_get_current_format(GtkIMHtml *imhtml,gboolean *bold,gboolean *italic,gboolean *underline)
{
  if (bold != ((gboolean *)((void *)0))) 
     *bold = imhtml -> edit.bold;
  if (italic != ((gboolean *)((void *)0))) 
     *italic = imhtml -> edit.italic;
  if (underline != ((gboolean *)((void *)0))) 
     *underline = imhtml -> edit.underline;
}

char *gtk_imhtml_get_current_fontface(GtkIMHtml *imhtml)
{
  return g_strdup(imhtml -> edit.fontface);
}

char *gtk_imhtml_get_current_forecolor(GtkIMHtml *imhtml)
{
  return g_strdup(imhtml -> edit.forecolor);
}

char *gtk_imhtml_get_current_backcolor(GtkIMHtml *imhtml)
{
  return g_strdup(imhtml -> edit.backcolor);
}

char *gtk_imhtml_get_current_background(GtkIMHtml *imhtml)
{
  return g_strdup(imhtml -> edit.background);
}

gint gtk_imhtml_get_current_fontsize(GtkIMHtml *imhtml)
{
  return imhtml -> edit.fontsize;
}

gboolean gtk_imhtml_get_editable(GtkIMHtml *imhtml)
{
  return imhtml -> editable;
}

void gtk_imhtml_clear_formatting(GtkIMHtml *imhtml)
{
  GObject *object;
  object = (g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2)))))));
  g_signal_emit(object,signals[CLEAR_FORMAT],0);
  gtk_widget_grab_focus(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type()))));
  g_object_unref(object);
}
/*
 * I had this crazy idea about changing the text cursor color to reflex the foreground color
 * of the text about to be entered. This is the place you'd do it, along with the place where
 * we actually set a new foreground color.
 * I may not do this, because people will bitch about Purple overriding their gtk theme's cursor
 * colors.
 *
 * Just in case I do do this, I asked about what to set the secondary text cursor to.
 *
 * (12:45:27) ?? ???: secondary_cursor_color = (rgb(background) + rgb(primary_cursor_color) ) / 2
 * (12:45:55) ?? ???: understand?
 * (12:46:14) Tim: yeah. i didn't know there was an exact formula
 * (12:46:56) ?? ???: u might need to extract separate each color from RGB
 */

static void mark_set_cb(GtkTextBuffer *buffer,GtkTextIter *arg1,GtkTextMark *mark,GtkIMHtml *imhtml)
{
  GSList *tags;
  GSList *l;
  GtkTextIter iter;
  if (mark != gtk_text_buffer_get_insert(buffer)) 
    return ;
  if (!(gtk_text_buffer_get_char_count(buffer) != 0)) 
    return ;
  imhtml -> edit.bold = (imhtml -> edit.italic = (imhtml -> edit.underline = (imhtml -> edit.strike = 0)));
  g_free(imhtml -> edit.forecolor);
  imhtml -> edit.forecolor = ((gchar *)((void *)0));
  g_free(imhtml -> edit.backcolor);
  imhtml -> edit.backcolor = ((gchar *)((void *)0));
  g_free(imhtml -> edit.fontface);
  imhtml -> edit.fontface = ((gchar *)((void *)0));
  imhtml -> edit.fontsize = 0;
  imhtml -> edit.link = ((GtkTextTag *)((void *)0));
  gtk_text_buffer_get_iter_at_mark((imhtml -> text_buffer),&iter,mark);
  if (gtk_text_iter_is_end((&iter)) != 0) 
    tags = gtk_text_iter_get_toggled_tags((&iter),0);
  else 
    tags = gtk_text_iter_get_tags((&iter));
  for (l = tags; l != ((GSList *)((void *)0)); l = (l -> next)) {
    GtkTextTag *tag = (GtkTextTag *)(g_type_check_instance_cast(((GTypeInstance *)(l -> data)),gtk_text_tag_get_type()));
    if ((tag -> name) != 0) {
      if (strcmp((tag -> name),"BOLD") == 0) 
        imhtml -> edit.bold = (!0);
      else if (strcmp((tag -> name),"ITALICS") == 0) 
        imhtml -> edit.italic = (!0);
      else if (strcmp((tag -> name),"UNDERLINE") == 0) 
        imhtml -> edit.underline = (!0);
      else if (strcmp((tag -> name),"STRIKE") == 0) 
        imhtml -> edit.strike = (!0);
      else if (strncmp((tag -> name),"FORECOLOR ",10) == 0) 
        imhtml -> edit.forecolor = g_strdup(((tag -> name) + 10));
      else if (strncmp((tag -> name),"BACKCOLOR ",10) == 0) 
        imhtml -> edit.backcolor = g_strdup(((tag -> name) + 10));
      else if (strncmp((tag -> name),"FONT FACE ",10) == 0) 
        imhtml -> edit.fontface = g_strdup(((tag -> name) + 10));
      else if (strncmp((tag -> name),"FONT SIZE ",10) == 0) 
        imhtml -> edit.fontsize = (strtol(((tag -> name) + 10),0,10));
      else if ((strncmp((tag -> name),"LINK ",5) == 0) && !(gtk_text_iter_is_end((&iter)) != 0)) 
        imhtml -> edit.link = tag;
    }
  }
  g_slist_free(tags);
}

static void imhtml_emit_signal_for_format(GtkIMHtml *imhtml,GtkIMHtmlButtons button)
{
  GObject *object;
  do {
    if (imhtml != ((GtkIMHtml *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"imhtml != NULL");
      return ;
    };
  }while (0);
  object = (g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2)))))));
  g_signal_emit(object,signals[TOGGLE_FORMAT],0,button);
  g_object_unref(object);
}

static void imhtml_toggle_bold(GtkIMHtml *imhtml)
{
  GtkTextIter start;
  GtkTextIter end;
  imhtml -> edit.bold = !(imhtml -> edit.bold != 0);
  if (!(imhtml_get_iter_bounds(imhtml,&start,&end) != 0)) 
    return ;
  if (imhtml -> edit.bold != 0) 
    gtk_text_buffer_apply_tag_by_name((imhtml -> text_buffer),"BOLD",(&start),(&end));
  else 
    gtk_text_buffer_remove_tag_by_name((imhtml -> text_buffer),"BOLD",(&start),(&end));
}

void gtk_imhtml_toggle_bold(GtkIMHtml *imhtml)
{
  imhtml_emit_signal_for_format(imhtml,GTK_IMHTML_BOLD);
}

static void imhtml_toggle_italic(GtkIMHtml *imhtml)
{
  GtkTextIter start;
  GtkTextIter end;
  imhtml -> edit.italic = !(imhtml -> edit.italic != 0);
  if (!(imhtml_get_iter_bounds(imhtml,&start,&end) != 0)) 
    return ;
  if (imhtml -> edit.italic != 0) 
    gtk_text_buffer_apply_tag_by_name((imhtml -> text_buffer),"ITALICS",(&start),(&end));
  else 
    gtk_text_buffer_remove_tag_by_name((imhtml -> text_buffer),"ITALICS",(&start),(&end));
}

void gtk_imhtml_toggle_italic(GtkIMHtml *imhtml)
{
  imhtml_emit_signal_for_format(imhtml,GTK_IMHTML_ITALIC);
}

static void imhtml_toggle_underline(GtkIMHtml *imhtml)
{
  GtkTextIter start;
  GtkTextIter end;
  imhtml -> edit.underline = !(imhtml -> edit.underline != 0);
  if (!(imhtml_get_iter_bounds(imhtml,&start,&end) != 0)) 
    return ;
  if (imhtml -> edit.underline != 0) 
    gtk_text_buffer_apply_tag_by_name((imhtml -> text_buffer),"UNDERLINE",(&start),(&end));
  else 
    gtk_text_buffer_remove_tag_by_name((imhtml -> text_buffer),"UNDERLINE",(&start),(&end));
}

void gtk_imhtml_toggle_underline(GtkIMHtml *imhtml)
{
  imhtml_emit_signal_for_format(imhtml,GTK_IMHTML_UNDERLINE);
}

static void imhtml_toggle_strike(GtkIMHtml *imhtml)
{
  GtkTextIter start;
  GtkTextIter end;
  imhtml -> edit.strike = !(imhtml -> edit.strike != 0);
  if (!(imhtml_get_iter_bounds(imhtml,&start,&end) != 0)) 
    return ;
  if (imhtml -> edit.strike != 0) 
    gtk_text_buffer_apply_tag_by_name((imhtml -> text_buffer),"STRIKE",(&start),(&end));
  else 
    gtk_text_buffer_remove_tag_by_name((imhtml -> text_buffer),"STRIKE",(&start),(&end));
}

void gtk_imhtml_toggle_strike(GtkIMHtml *imhtml)
{
  imhtml_emit_signal_for_format(imhtml,GTK_IMHTML_STRIKE);
}

void gtk_imhtml_font_set_size(GtkIMHtml *imhtml,gint size)
{
  GObject *object;
  GtkTextIter start;
  GtkTextIter end;
  imhtml -> edit.fontsize = size;
  if (!(imhtml_get_iter_bounds(imhtml,&start,&end) != 0)) 
    return ;
  remove_font_size(imhtml,&start,&end,(imhtml -> wbfo));
  gtk_text_buffer_apply_tag((imhtml -> text_buffer),find_font_size_tag(imhtml,imhtml -> edit.fontsize),(&start),(&end));
  object = (g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2)))))));
  g_signal_emit(object,signals[TOGGLE_FORMAT],0,GTK_IMHTML_SHRINK | GTK_IMHTML_GROW);
  g_object_unref(object);
}

static void imhtml_font_shrink(GtkIMHtml *imhtml)
{
  GtkTextIter start;
  GtkTextIter end;
  if (imhtml -> edit.fontsize == 1) 
    return ;
  if (!(imhtml -> edit.fontsize != 0)) 
    imhtml -> edit.fontsize = 2;
  else 
    imhtml -> edit.fontsize--;
  if (!(imhtml_get_iter_bounds(imhtml,&start,&end) != 0)) 
    return ;
  remove_font_size(imhtml,&start,&end,(imhtml -> wbfo));
  gtk_text_buffer_apply_tag((imhtml -> text_buffer),find_font_size_tag(imhtml,imhtml -> edit.fontsize),(&start),(&end));
}

void gtk_imhtml_font_shrink(GtkIMHtml *imhtml)
{
  imhtml_emit_signal_for_format(imhtml,GTK_IMHTML_SHRINK);
}

static void imhtml_font_grow(GtkIMHtml *imhtml)
{
  GtkTextIter start;
  GtkTextIter end;
  if (imhtml -> edit.fontsize == 7) 
    return ;
  if (!(imhtml -> edit.fontsize != 0)) 
    imhtml -> edit.fontsize = 4;
  else 
    imhtml -> edit.fontsize++;
  if (!(imhtml_get_iter_bounds(imhtml,&start,&end) != 0)) 
    return ;
  remove_font_size(imhtml,&start,&end,(imhtml -> wbfo));
  gtk_text_buffer_apply_tag((imhtml -> text_buffer),find_font_size_tag(imhtml,imhtml -> edit.fontsize),(&start),(&end));
}

void gtk_imhtml_font_grow(GtkIMHtml *imhtml)
{
  imhtml_emit_signal_for_format(imhtml,GTK_IMHTML_GROW);
}

static gboolean gtk_imhtml_toggle_str_tag(GtkIMHtml *imhtml,const char *value,char **edit_field,void (*remove_func)(GtkIMHtml *, GtkTextIter *, GtkTextIter *, gboolean ),GtkTextTag *find_func(GtkIMHtml *, gchar *),GtkIMHtmlButtons button)
{
  GObject *object;
  GtkTextIter start;
  GtkTextIter end;
  g_free(( *edit_field));
   *edit_field = ((char *)((void *)0));
  if ((value != 0) && (strcmp(value,"") != 0)) {
     *edit_field = g_strdup(value);
    if (imhtml_get_iter_bounds(imhtml,&start,&end) != 0) {
      ( *remove_func)(imhtml,&start,&end,(imhtml -> wbfo));
      gtk_text_buffer_apply_tag((imhtml -> text_buffer),( *find_func)(imhtml, *edit_field),(&start),(&end));
    }
  }
  else {
    if (imhtml_get_iter_bounds(imhtml,&start,&end) != 0) 
/* 'TRUE' or 'imhtml->wbfo'? */
      ( *remove_func)(imhtml,&start,&end,(!0));
  }
  object = (g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2)))))));
  g_signal_emit(object,signals[TOGGLE_FORMAT],0,button);
  g_object_unref(object);
  return  *edit_field != ((char *)((void *)0));
}

gboolean gtk_imhtml_toggle_forecolor(GtkIMHtml *imhtml,const char *color)
{
  return gtk_imhtml_toggle_str_tag(imhtml,color,&imhtml -> edit.forecolor,remove_font_forecolor,find_font_forecolor_tag,GTK_IMHTML_FORECOLOR);
}

gboolean gtk_imhtml_toggle_backcolor(GtkIMHtml *imhtml,const char *color)
{
  return gtk_imhtml_toggle_str_tag(imhtml,color,&imhtml -> edit.backcolor,remove_font_backcolor,find_font_backcolor_tag,GTK_IMHTML_BACKCOLOR);
}

gboolean gtk_imhtml_toggle_background(GtkIMHtml *imhtml,const char *color)
{
  return gtk_imhtml_toggle_str_tag(imhtml,color,&imhtml -> edit.background,remove_font_background,find_font_background_tag,GTK_IMHTML_BACKGROUND);
}

gboolean gtk_imhtml_toggle_fontface(GtkIMHtml *imhtml,const char *face)
{
  return gtk_imhtml_toggle_str_tag(imhtml,face,&imhtml -> edit.fontface,remove_font_face,find_font_face_tag,GTK_IMHTML_FACE);
}

void gtk_imhtml_toggle_link(GtkIMHtml *imhtml,const char *url)
{
  GObject *object;
  GtkTextIter start;
  GtkTextIter end;
  GtkTextTag *linktag;
  static guint linkno = 0;
  gchar str[48UL];
  GdkColor *color = (GdkColor *)((void *)0);
  imhtml -> edit.link = ((GtkTextTag *)((void *)0));
  if (url != 0) {
    g_snprintf(str,(sizeof(str)),"LINK %d",linkno++);
    str[47] = 0;
    gtk_widget_style_get(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type()))),"hyperlink-color",&color,((void *)((void *)0)));
    if (color != 0) {
      imhtml -> edit.link = (linktag = gtk_text_buffer_create_tag((imhtml -> text_buffer),str,"foreground-gdk",color,"underline",PANGO_UNDERLINE_SINGLE,((void *)((void *)0))));
      gdk_color_free(color);
    }
    else {
      imhtml -> edit.link = (linktag = gtk_text_buffer_create_tag((imhtml -> text_buffer),str,"foreground","blue","underline",PANGO_UNDERLINE_SINGLE,((void *)((void *)0))));
    }
    g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)linktag),((GType )(20 << 2))))),"link_url",(g_strdup(url)),g_free);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)linktag),((GType )(20 << 2))))),"event",((GCallback )tag_event),0,0,((GConnectFlags )0));
    if (((imhtml -> editable) != 0) && (gtk_text_buffer_get_selection_bounds((imhtml -> text_buffer),&start,&end) != 0)) {
      remove_font_link(imhtml,&start,&end,0);
      gtk_text_buffer_apply_tag((imhtml -> text_buffer),linktag,(&start),(&end));
    }
  }
  object = (g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2)))))));
  g_signal_emit(object,signals[TOGGLE_FORMAT],0,GTK_IMHTML_LINK);
  g_object_unref(object);
}

void gtk_imhtml_insert_link(GtkIMHtml *imhtml,GtkTextMark *mark,const char *url,const char *text)
{
  GtkTextIter iter;
/* Delete any currently selected text */
  gtk_text_buffer_delete_selection((imhtml -> text_buffer),(!0),(!0));
  gtk_imhtml_toggle_link(imhtml,url);
  gtk_text_buffer_get_iter_at_mark((imhtml -> text_buffer),&iter,mark);
  gtk_text_buffer_insert((imhtml -> text_buffer),&iter,text,(-1));
  gtk_imhtml_toggle_link(imhtml,0);
}

void gtk_imhtml_insert_smiley(GtkIMHtml *imhtml,const char *sml,char *smiley)
{
  GtkTextMark *mark;
  GtkTextIter iter;
/* Delete any currently selected text */
  gtk_text_buffer_delete_selection((imhtml -> text_buffer),(!0),(!0));
  mark = gtk_text_buffer_get_insert((imhtml -> text_buffer));
  gtk_text_buffer_get_iter_at_mark((imhtml -> text_buffer),&iter,mark);
  gtk_text_buffer_begin_user_action((imhtml -> text_buffer));
  gtk_imhtml_insert_smiley_at_iter(imhtml,sml,smiley,&iter);
  gtk_text_buffer_end_user_action((imhtml -> text_buffer));
}

static gboolean image_expose(GtkWidget *widget,GdkEventExpose *event,gpointer user_data)
{
  ( *( *((GtkWidgetClass *)(g_type_check_class_cast(((GTypeClass *)((GtkWidgetClass *)( *((GTypeInstance *)widget)).g_class)),gtk_widget_get_type())))).expose_event)(widget,event);
  return (!0);
}
/* In case the smiley gets removed from the imhtml before it gets removed from the queue */

static void animated_smiley_destroy_cb(GtkObject *widget,GtkIMHtml *imhtml)
{
  GList *l = ( *(imhtml -> animations)).head;
  while(l != 0){
    GList *next = (l -> next);
    if ((l -> data) == widget) {
      if (l == ( *(imhtml -> animations)).tail) 
        ( *(imhtml -> animations)).tail = ( *( *(imhtml -> animations)).tail).prev;
      ( *(imhtml -> animations)).head = g_list_delete_link(( *(imhtml -> animations)).head,l);
      imhtml -> num_animations--;
    }
    l = next;
  }
}

void gtk_imhtml_insert_smiley_at_iter(GtkIMHtml *imhtml,const char *sml,char *smiley,GtkTextIter *iter)
{
  GdkPixbuf *pixbuf = (GdkPixbuf *)((void *)0);
  GdkPixbufAnimation *annipixbuf = (GdkPixbufAnimation *)((void *)0);
  GtkWidget *icon = (GtkWidget *)((void *)0);
  GtkTextChildAnchor *anchor = (GtkTextChildAnchor *)((void *)0);
  char *unescaped;
  GtkIMHtmlSmiley *imhtml_smiley;
  GtkWidget *ebox = (GtkWidget *)((void *)0);
  int numsmileys_thismsg;
  int numsmileys_total;
/*
	 * This GtkIMHtml has the maximum number of smileys allowed, so don't
	 * add any more.  We do this for performance reasons, because smileys
	 * are apparently pretty inefficient.  Hopefully we can remove this
	 * restriction when we're using a better HTML widget.
	 */
  unescaped = purple_unescape_html(smiley);
  numsmileys_thismsg = ((gint )((glong )(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"gtkimhtml_numsmileys_thismsg"))));
  if (numsmileys_thismsg >= 30) {
    gtk_text_buffer_insert((imhtml -> text_buffer),iter,unescaped,(-1));
    g_free(unescaped);
    return ;
  }
  numsmileys_total = ((gint )((glong )(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"gtkimhtml_numsmileys_total"))));
  if (numsmileys_total >= 300) {
    gtk_text_buffer_insert((imhtml -> text_buffer),iter,unescaped,(-1));
    g_free(unescaped);
    return ;
  }
  imhtml_smiley = gtk_imhtml_smiley_get(imhtml,sml,unescaped);
  if (((imhtml -> format_functions) & GTK_IMHTML_SMILEY) != 0) {
    annipixbuf = ((imhtml_smiley != 0)?gtk_smiley_get_image(imhtml_smiley) : ((struct _GdkPixbufAnimation *)((void *)0)));
    if (annipixbuf != 0) {
      if (gdk_pixbuf_animation_is_static_image(annipixbuf) != 0) {
        pixbuf = gdk_pixbuf_animation_get_static_image(annipixbuf);
        if (pixbuf != 0) 
          icon = gtk_image_new_from_pixbuf(pixbuf);
      }
      else {
        icon = gtk_image_new_from_animation(annipixbuf);
        if ((imhtml -> num_animations) == 20) {
          GtkImage *image = (GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)(g_queue_pop_head((imhtml -> animations)))),gtk_image_get_type()));
          GdkPixbufAnimation *anim = gtk_image_get_animation(image);
          g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)image),((GType )(20 << 2))))),G_SIGNAL_MATCH_FUNC,0,0,0,((GCallback )animated_smiley_destroy_cb),0);
          if (anim != 0) {
            GdkPixbuf *pb = gdk_pixbuf_animation_get_static_image(anim);
            if (pb != ((GdkPixbuf *)((void *)0))) {
              GdkPixbuf *copy = gdk_pixbuf_copy(pb);
              gtk_image_set_from_pixbuf(image,copy);
              g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)copy),((GType )(20 << 2))))));
            }
          }
        }
        else {
          imhtml -> num_animations++;
        }
        g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)icon),((GType )(20 << 2))))),"destroy",((GCallback )animated_smiley_destroy_cb),imhtml,0,((GConnectFlags )0));
        g_queue_push_tail((imhtml -> animations),icon);
      }
    }
  }
  if ((imhtml_smiley != 0) && (((imhtml_smiley -> flags) & GTK_IMHTML_SMILEY_CUSTOM) != 0U)) {
    ebox = gtk_event_box_new();
    gtk_event_box_set_visible_window(((GtkEventBox *)(g_type_check_instance_cast(((GTypeInstance *)ebox),gtk_event_box_get_type()))),0);
    gtk_widget_show(ebox);
  }
  if (icon != 0) {
    anchor = gtk_text_buffer_create_child_anchor((imhtml -> text_buffer),iter);
    g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)anchor),((GType )(20 << 2))))),"gtkimhtml_plaintext",(g_strdup(unescaped)),g_free);
    g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)anchor),((GType )(20 << 2))))),"gtkimhtml_tiptext",(g_strdup(unescaped)),g_free);
    g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)anchor),((GType )(20 << 2))))),"gtkimhtml_htmltext",(g_strdup(smiley)),g_free);
/* This catches the expose events generated by animated
		 * images, and ensures that they are handled by the image
		 * itself, without propagating to the textview and causing
		 * a complete refresh */
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)icon),((GType )(20 << 2))))),"expose-event",((GCallback )image_expose),0,0,((GConnectFlags )0));
    gtk_widget_show(icon);
    if (ebox != 0) 
      gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ebox),gtk_container_get_type()))),icon);
    gtk_text_view_add_child_at_anchor(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),((ebox != 0)?ebox : icon),anchor);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"gtkimhtml_numsmileys_thismsg",((gpointer )((glong )(numsmileys_thismsg + 1))));
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"gtkimhtml_numsmileys_total",((gpointer )((glong )(numsmileys_total + 1))));
  }
  else if ((imhtml_smiley != ((GtkIMHtmlSmiley *)((void *)0))) && (((imhtml -> format_functions) & GTK_IMHTML_SMILEY) != 0)) {
    anchor = gtk_text_buffer_create_child_anchor((imhtml -> text_buffer),iter);
    imhtml_smiley -> anchors = g_slist_append((imhtml_smiley -> anchors),g_object_ref(anchor));
    if (ebox != 0) {
      GtkWidget *img = gtk_image_new_from_stock("gtk-missing-image",GTK_ICON_SIZE_MENU);
      gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ebox),gtk_container_get_type()))),img);
      gtk_widget_show(img);
      g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)anchor),((GType )(20 << 2))))),"gtkimhtml_plaintext",(g_strdup(unescaped)),g_free);
      g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)anchor),((GType )(20 << 2))))),"gtkimhtml_tiptext",(g_strdup(unescaped)),g_free);
      g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)anchor),((GType )(20 << 2))))),"gtkimhtml_htmltext",(g_strdup(smiley)),g_free);
      gtk_text_view_add_child_at_anchor(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),ebox,anchor);
    }
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"gtkimhtml_numsmileys_thismsg",((gpointer )((glong )(numsmileys_thismsg + 1))));
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"gtkimhtml_numsmileys_total",((gpointer )((glong )(numsmileys_total + 1))));
  }
  else {
    gtk_text_buffer_insert((imhtml -> text_buffer),iter,unescaped,(-1));
  }
  if (ebox != 0) {
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ebox),((GType )(20 << 2))))),"event",((GCallback )gtk_imhtml_smiley_clicked),imhtml_smiley,0,((GConnectFlags )0));
  }
  g_free(unescaped);
}

void gtk_imhtml_insert_image_at_iter(GtkIMHtml *imhtml,int id,GtkTextIter *iter)
{
  GdkPixbufAnimation *anim = (GdkPixbufAnimation *)((void *)0);
  const char *filename = (const char *)((void *)0);
  gpointer image;
  GdkRectangle rect;
  GtkIMHtmlScalable *scalable = (GtkIMHtmlScalable *)((void *)0);
  struct scalable_data *sd;
  int minus;
  if ((((((!((imhtml -> funcs) != 0) || !(( *(imhtml -> funcs)).image_get != 0)) || !(( *(imhtml -> funcs)).image_get_size != 0)) || !(( *(imhtml -> funcs)).image_get_data != 0)) || !(( *(imhtml -> funcs)).image_get_filename != 0)) || !(( *(imhtml -> funcs)).image_ref != 0)) || !(( *(imhtml -> funcs)).image_unref != 0)) 
    return ;
  image = ( *( *(imhtml -> funcs)).image_get)(id);
  if (image != 0) {
    gpointer data;
    size_t len;
    data = ( *( *(imhtml -> funcs)).image_get_data)(image);
    len = ( *( *(imhtml -> funcs)).image_get_size)(image);
    if ((data != 0) && (len != 0UL)) 
      anim = pidgin_pixbuf_anim_from_data(data,len);
  }
  if (anim != 0) {
    struct im_image_data *t = (struct im_image_data *)(g_malloc_n(1,(sizeof(struct im_image_data ))));
    filename = ( *( *(imhtml -> funcs)).image_get_filename)(image);
    ( *( *(imhtml -> funcs)).image_ref)(id);
    t -> id = id;
    t -> mark = gtk_text_buffer_create_mark((imhtml -> text_buffer),0,iter,(!0));
    imhtml -> im_images = g_slist_prepend((imhtml -> im_images),t);
    scalable = gtk_imhtml_animation_new(anim,filename,id);
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)anim),((GType )(20 << 2))))));
  }
  else {
    GdkPixbuf *pixbuf;
    pixbuf = gtk_widget_render_icon(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_widget_get_type()))),"gtk-missing-image",GTK_ICON_SIZE_BUTTON,"gtkimhtml-missing-image");
    scalable = gtk_imhtml_image_new(pixbuf,filename,id);
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
  }
  sd = ((struct scalable_data *)(g_malloc_n(1,(sizeof(struct scalable_data )))));
  sd -> scalable = scalable;
  sd -> mark = gtk_text_buffer_create_mark((imhtml -> text_buffer),0,iter,(!0));
  gtk_text_view_get_visible_rect(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),&rect);
  ( *(scalable -> add_to))(scalable,imhtml,iter);
  minus = (gtk_text_view_get_left_margin(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type())))) + gtk_text_view_get_right_margin(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type())))));
  ( *(scalable -> scale))(scalable,(rect.width - minus),rect.height);
  imhtml -> scalables = g_list_append((imhtml -> scalables),sd);
}

static const gchar *tag_to_html_start(GtkTextTag *tag)
{
  const gchar *name;
  static gchar buf[16384UL];
  name = (tag -> name);
  do {
    if (name != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return "";
    };
  }while (0);
  if (strcmp(name,"BOLD") == 0) {
    return "<b>";
  }
  else if (strcmp(name,"ITALICS") == 0) {
    return "<i>";
  }
  else if (strcmp(name,"UNDERLINE") == 0) {
    return "<u>";
  }
  else if (strcmp(name,"STRIKE") == 0) {
    return "<s>";
  }
  else if (strncmp(name,"LINK ",5) == 0) {
    char *tmp = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))),"link_url"));
    if (tmp != 0) {
      gchar *escaped = purple_markup_escape_text(tmp,(-1));
      g_snprintf(buf,(sizeof(buf)),"<a href=\"%s\">",escaped);
      buf[sizeof(buf) - 1] = 0;
      g_free(escaped);
      return buf;
    }
    else {
      return "";
    }
  }
  else if (strncmp(name,"FORECOLOR ",10) == 0) {
    g_snprintf(buf,(sizeof(buf)),"<font color=\"%s\">",(name + 10));
    return buf;
  }
  else if (strncmp(name,"BACKCOLOR ",10) == 0) {
    g_snprintf(buf,(sizeof(buf)),"<font back=\"%s\">",(name + 10));
    return buf;
  }
  else if (strncmp(name,"BACKGROUND ",10) == 0) {
    g_snprintf(buf,(sizeof(buf)),"<body bgcolor=\"%s\">",(name + 11));
    return buf;
  }
  else if (strncmp(name,"FONT FACE ",10) == 0) {
    g_snprintf(buf,(sizeof(buf)),"<font face=\"%s\">",(name + 10));
    return buf;
  }
  else if (strncmp(name,"FONT SIZE ",10) == 0) {
    g_snprintf(buf,(sizeof(buf)),"<font size=\"%s\">",(name + 10));
    return buf;
  }
  else {
    char *str = buf;
    gboolean isset;
    int ivalue = 0;
    GdkColor *color = (GdkColor *)((void *)0);
    GObject *obj = (GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))));
    gboolean empty = (!0);
    str += g_snprintf(str,(sizeof(buf) - (str - buf)),"<span style=\'");
/* Weight */
    g_object_get(obj,"weight-set",&isset,"weight",&ivalue,((void *)((void *)0)));
    if (isset != 0) {
      const char *weight = "";
      if (ivalue >= PANGO_WEIGHT_ULTRABOLD) 
        weight = "bolder";
      else if (ivalue >= PANGO_WEIGHT_BOLD) 
        weight = "bold";
      else if (ivalue >= PANGO_WEIGHT_NORMAL) 
        weight = "normal";
      else 
        weight = "lighter";
      str += g_snprintf(str,(sizeof(buf) - (str - buf)),"font-weight: %s;",weight);
      empty = 0;
    }
/* Foreground color */
    g_object_get(obj,"foreground-set",&isset,"foreground-gdk",&color,((void *)((void *)0)));
    if ((isset != 0) && (color != 0)) {
      str += g_snprintf(str,(sizeof(buf) - (str - buf)),"color: #%02x%02x%02x;",((color -> red) >> 8),((color -> green) >> 8),((color -> blue) >> 8));
      empty = 0;
    }
    gdk_color_free(color);
/* Background color */
    g_object_get(obj,"background-set",&isset,"background-gdk",&color,((void *)((void *)0)));
    if ((isset != 0) && (color != 0)) {
      str += g_snprintf(str,(sizeof(buf) - (str - buf)),"background: #%02x%02x%02x;",((color -> red) >> 8),((color -> green) >> 8),((color -> blue) >> 8));
      empty = 0;
    }
    gdk_color_free(color);
/* Underline */
    g_object_get(obj,"underline-set",&isset,"underline",&ivalue,((void *)((void *)0)));
    if (isset != 0) {
      switch(ivalue){
        case 0:
{
        }
        case 4:
{
          break; 
        }
        default:
{
          str += g_snprintf(str,(sizeof(buf) - (str - buf)),"text-decoration: underline;");
          empty = 0;
        }
      }
    }
    g_snprintf(str,(sizeof(buf) - (str - buf)),"\'>");
    return ((empty != 0)?"" : buf);
  }
}

static const gchar *tag_to_html_end(GtkTextTag *tag)
{
  const gchar *name;
  name = (tag -> name);
  do {
    if (name != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return "";
    };
  }while (0);
  if (strcmp(name,"BOLD") == 0) {
    return "</b>";
  }
  else if (strcmp(name,"ITALICS") == 0) {
    return "</i>";
  }
  else if (strcmp(name,"UNDERLINE") == 0) {
    return "</u>";
  }
  else if (strcmp(name,"STRIKE") == 0) {
    return "</s>";
  }
  else if (strncmp(name,"LINK ",5) == 0) {
    return "</a>";
  }
  else if (strncmp(name,"FORECOLOR ",10) == 0) {
    return "</font>";
  }
  else if (strncmp(name,"BACKCOLOR ",10) == 0) {
    return "</font>";
  }
  else if (strncmp(name,"BACKGROUND ",10) == 0) {
    return "</body>";
  }
  else if (strncmp(name,"FONT FACE ",10) == 0) {
    return "</font>";
  }
  else if (strncmp(name,"FONT SIZE ",10) == 0) {
    return "</font>";
  }
  else {
    const char *props[] = {("weight-set"), ("foreground-set"), ("background-set"), ("size-set"), ("underline-set"), ((const char *)((void *)0))};
    int i;
    for (i = 0; props[i] != 0; i++) {
      gboolean set = 0;
      g_object_get(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))),props[i],&set,((void *)((void *)0)));
      if (set != 0) 
        return "</span>";
    }
    return "";
  }
}
typedef struct __unnamed_class___F0_L5258_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L1137R__Pe___variable_name_unknown_scope_and_name__scope__tag__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__end__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__start {
GtkTextTag *tag;
char *end;
char *start;}PidginTextTagData;

static PidginTextTagData *text_tag_data_new(GtkTextTag *tag)
{
  const char *start;
  const char *end;
  PidginTextTagData *ret = (PidginTextTagData *)((void *)0);
  start = tag_to_html_start(tag);
  if (!(start != 0) || !(( *start) != 0)) 
    return 0;
  end = tag_to_html_end(tag);
  if (!(end != 0) || !(( *end) != 0)) 
    return 0;
  ret = ((PidginTextTagData *)(g_malloc0_n(1,(sizeof(PidginTextTagData )))));
  ret -> start = g_strdup(start);
  ret -> end = g_strdup(end);
  ret -> tag = tag;
  return ret;
}

static void text_tag_data_destroy(PidginTextTagData *data)
{
  g_free((data -> start));
  g_free((data -> end));
  g_free(data);
}

static gboolean tag_ends_here(GtkTextTag *tag,GtkTextIter *iter,GtkTextIter *niter)
{
  return ((gtk_text_iter_has_tag(iter,((GtkTextTag *)(g_type_check_instance_cast(((GTypeInstance *)tag),gtk_text_tag_get_type())))) != 0) && !(gtk_text_iter_has_tag(niter,((GtkTextTag *)(g_type_check_instance_cast(((GTypeInstance *)tag),gtk_text_tag_get_type())))) != 0)) || (gtk_text_iter_is_end(niter) != 0);
}
/* Basic notion here: traverse through the text buffer one-by-one, non-character elements, such
 * as smileys and IM images are represented by the Unicode "unknown" character.  Handle them.  Else
 * check for tags that are toggled on, insert their html form, and  push them on the queue. Then insert
 * the actual text. Then check for tags that are toggled off and insert them, after checking the queue.
 * Finally, replace <, >, &, and " with their HTML equivalent.
 */

char *gtk_imhtml_get_markup_range(GtkIMHtml *imhtml,GtkTextIter *start,GtkTextIter *end)
{
  gunichar c;
  GtkTextIter iter;
  GtkTextIter next_iter;
  GtkTextIter non_neutral_iter;
  gboolean is_rtl_message = 0;
  GString *str = g_string_new("");
  GSList *tags;
  GSList *sl;
  GQueue *q;
  GtkTextTag *tag;
  PidginTextTagData *tagdata;
  q = g_queue_new();
  gtk_text_iter_order(start,end);
  non_neutral_iter = (next_iter = (iter =  *start));
  gtk_text_iter_forward_char(&next_iter);
/* Bi-directional text support */
/* Get to the first non-neutral character */
#ifdef HAVE_PANGO14
  while((PANGO_DIRECTION_NEUTRAL == (pango_unichar_direction(gtk_text_iter_get_char((&non_neutral_iter))))) && (gtk_text_iter_forward_char(&non_neutral_iter) != 0));
  if (PANGO_DIRECTION_RTL == (pango_unichar_direction(gtk_text_iter_get_char((&non_neutral_iter))))) {
    is_rtl_message = (!0);
    g_string_append(str,"<SPAN style=\"direction:rtl;text-align:right;\">");
  }
#endif
/* First add the tags that are already in progress (we don't care about non-printing tags)*/
  tags = gtk_text_iter_get_tags(start);
  for (sl = tags; sl != 0; sl = (sl -> next)) {
    tag = (sl -> data);
    if (!(gtk_text_iter_toggles_tag(start,((GtkTextTag *)(g_type_check_instance_cast(((GTypeInstance *)tag),gtk_text_tag_get_type())))) != 0)) {
      PidginTextTagData *data = text_tag_data_new(tag);
      if (data != 0) {
        g_string_append(str,(data -> start));
        g_queue_push_tail(q,data);
      }
    }
  }
  g_slist_free(tags);
  while(((c = gtk_text_iter_get_char((&iter))) != 0) && !(gtk_text_iter_equal((&iter),end) != 0)){
    tags = gtk_text_iter_get_tags((&iter));
    for (sl = tags; sl != 0; sl = (sl -> next)) {
      tag = (sl -> data);
      if (gtk_text_iter_begins_tag((&iter),((GtkTextTag *)(g_type_check_instance_cast(((GTypeInstance *)tag),gtk_text_tag_get_type())))) != 0) {
        PidginTextTagData *data = text_tag_data_new(tag);
        if (data != 0) {
          g_string_append(str,(data -> start));
          g_queue_push_tail(q,data);
        }
      }
    }
    if (c == 0xFFFC) {
      GtkTextChildAnchor *anchor = gtk_text_iter_get_child_anchor((&iter));
      if (anchor != 0) {
        char *text = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)anchor),((GType )(20 << 2))))),"gtkimhtml_htmltext"));
        if (text != 0) 
          str = g_string_append(str,text);
      }
    }
    else if (c == 60) {
      str = g_string_append(str,"&lt;");
    }
    else if (c == '>') {
      str = g_string_append(str,"&gt;");
    }
    else if (c == '&') {
      str = g_string_append(str,"&amp;");
    }
    else if (c == '"') {
      str = g_string_append(str,"&quot;");
    }
    else if (c == 10) {
      str = g_string_append(str,"<br>");
    }
    else {
      str = g_string_append_unichar(str,c);
    }
    tags = g_slist_reverse(tags);
    for (sl = tags; sl != 0; sl = (sl -> next)) {
      tag = (sl -> data);
/** don't worry about non-printing tags ending */
      if (((tag_ends_here(tag,&iter,&next_iter) != 0) && (strlen(tag_to_html_end(tag)) > 0)) && (strlen(tag_to_html_start(tag)) > 0)) {
        PidginTextTagData *tmp;
        GQueue *r = g_queue_new();
        while(((tmp = (g_queue_pop_tail(q))) != 0) && ((tmp -> tag) != tag)){
          g_string_append(str,(tmp -> end));
          if (!(tag_ends_here((tmp -> tag),&iter,&next_iter) != 0)) 
            g_queue_push_tail(r,tmp);
          else 
            text_tag_data_destroy(tmp);
        }
        if (tmp != ((PidginTextTagData *)((void *)0))) {
          g_string_append(str,(tmp -> end));
          text_tag_data_destroy(tmp);
        }
#if 0 /* This can't be allowed to happen because it causes the iters to be invalidated in the debug window imhtml during text copying */
#endif
        while((tmp = (g_queue_pop_head(r))) != 0){
          g_string_append(str,(tmp -> start));
          g_queue_push_tail(q,tmp);
        }
        g_queue_free(r);
      }
    }
    g_slist_free(tags);
    gtk_text_iter_forward_char(&iter);
    gtk_text_iter_forward_char(&next_iter);
  }
  while((tagdata = (g_queue_pop_tail(q))) != 0){
    g_string_append(str,(tagdata -> end));
    text_tag_data_destroy(tagdata);
  }
/* Bi-directional text support - close tags */
  if (is_rtl_message != 0) 
    g_string_append(str,"</SPAN>");
  g_queue_free(q);
  return g_string_free(str,0);
}

void gtk_imhtml_close_tags(GtkIMHtml *imhtml,GtkTextIter *iter)
{
  if (imhtml -> edit.bold != 0) 
    gtk_imhtml_toggle_bold(imhtml);
  if (imhtml -> edit.italic != 0) 
    gtk_imhtml_toggle_italic(imhtml);
  if (imhtml -> edit.underline != 0) 
    gtk_imhtml_toggle_underline(imhtml);
  if (imhtml -> edit.strike != 0) 
    gtk_imhtml_toggle_strike(imhtml);
  if (imhtml -> edit.forecolor != 0) 
    gtk_imhtml_toggle_forecolor(imhtml,0);
  if (imhtml -> edit.backcolor != 0) 
    gtk_imhtml_toggle_backcolor(imhtml,0);
  if (imhtml -> edit.fontface != 0) 
    gtk_imhtml_toggle_fontface(imhtml,0);
  imhtml -> edit.fontsize = 0;
  if (imhtml -> edit.link != 0) 
    gtk_imhtml_toggle_link(imhtml,0);
}

char *gtk_imhtml_get_markup(GtkIMHtml *imhtml)
{
  GtkTextIter start;
  GtkTextIter end;
  gtk_text_buffer_get_start_iter((imhtml -> text_buffer),&start);
  gtk_text_buffer_get_end_iter((imhtml -> text_buffer),&end);
  return gtk_imhtml_get_markup_range(imhtml,&start,&end);
}

char **gtk_imhtml_get_markup_lines(GtkIMHtml *imhtml)
{
  int i;
  int j;
  int lines;
  GtkTextIter start;
  GtkTextIter end;
  char **ret;
  lines = gtk_text_buffer_get_line_count((imhtml -> text_buffer));
  ret = ((char **)(g_malloc0_n((lines + 1),(sizeof(char *)))));
  gtk_text_buffer_get_start_iter((imhtml -> text_buffer),&start);
  end = start;
  gtk_text_iter_forward_to_line_end(&end);
  for (((i = 0) , (j = 0)); i < lines; i++) {
    if (gtk_text_iter_get_char((&start)) != 10) {
      ret[j] = gtk_imhtml_get_markup_range(imhtml,&start,&end);
      if (ret[j] != ((char *)((void *)0))) 
        j++;
    }
    gtk_text_iter_forward_line(&start);
    end = start;
    gtk_text_iter_forward_to_line_end(&end);
  }
  return ret;
}

char *gtk_imhtml_get_text(GtkIMHtml *imhtml,GtkTextIter *start,GtkTextIter *stop)
{
  GString *str = g_string_new("");
  GtkTextIter iter;
  GtkTextIter end;
  gunichar c;
  if (start == ((GtkTextIter *)((void *)0))) 
    gtk_text_buffer_get_start_iter((imhtml -> text_buffer),&iter);
  else 
    iter =  *start;
  if (stop == ((GtkTextIter *)((void *)0))) 
    gtk_text_buffer_get_end_iter((imhtml -> text_buffer),&end);
  else 
    end =  *stop;
  gtk_text_iter_order(&iter,&end);
  while(((c = gtk_text_iter_get_char((&iter))) != 0) && !(gtk_text_iter_equal((&iter),(&end)) != 0)){
    if (c == 0xFFFC) {
      GtkTextChildAnchor *anchor;
      char *text = (char *)((void *)0);
      anchor = gtk_text_iter_get_child_anchor((&iter));
      if (anchor != 0) 
        text = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)anchor),((GType )(20 << 2))))),"gtkimhtml_plaintext"));
      if (text != 0) 
        str = g_string_append(str,text);
    }
    else {
      g_string_append_unichar(str,c);
    }
    gtk_text_iter_forward_char(&iter);
  }
  return g_string_free(str,0);
}

void gtk_imhtml_set_funcs(GtkIMHtml *imhtml,GtkIMHtmlFuncs *f)
{
  do {
    if (imhtml != ((GtkIMHtml *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"imhtml != NULL");
      return ;
    };
  }while (0);
  imhtml -> funcs = f;
}

void gtk_imhtml_setup_entry(GtkIMHtml *imhtml,PurpleConnectionFlags flags)
{
  GtkIMHtmlButtons buttons;
  if ((flags & PURPLE_CONNECTION_HTML) != 0U) {
    char color[8UL];
    GdkColor fg_color;
    GdkColor bg_color;
    buttons = GTK_IMHTML_ALL;
    if ((flags & PURPLE_CONNECTION_NO_BGCOLOR) != 0U) 
      buttons &= (~GTK_IMHTML_BACKCOLOR);
    if ((flags & PURPLE_CONNECTION_NO_FONTSIZE) != 0U) {
      buttons &= (~GTK_IMHTML_GROW);
      buttons &= (~GTK_IMHTML_SHRINK);
    }
    if ((flags & PURPLE_CONNECTION_NO_URLDESC) != 0U) 
      buttons &= (~GTK_IMHTML_LINKDESC);
    gtk_imhtml_set_format_functions(imhtml,GTK_IMHTML_ALL);
    if (purple_prefs_get_bool("/pidgin/conversations/send_bold") != imhtml -> edit.bold) 
      gtk_imhtml_toggle_bold(imhtml);
    if (purple_prefs_get_bool("/pidgin/conversations/send_italic") != imhtml -> edit.italic) 
      gtk_imhtml_toggle_italic(imhtml);
    if (purple_prefs_get_bool("/pidgin/conversations/send_underline") != imhtml -> edit.underline) 
      gtk_imhtml_toggle_underline(imhtml);
    gtk_imhtml_toggle_fontface(imhtml,purple_prefs_get_string("/pidgin/conversations/font_face"));
    if (!((flags & PURPLE_CONNECTION_NO_FONTSIZE) != 0U)) {
      int size = purple_prefs_get_int("/pidgin/conversations/font_size");
/* 3 is the default. */
      if (size != 3) 
        gtk_imhtml_font_set_size(imhtml,size);
    }
    if (strcmp(purple_prefs_get_string("/pidgin/conversations/fgcolor"),"") != 0) {
      gdk_color_parse(purple_prefs_get_string("/pidgin/conversations/fgcolor"),&fg_color);
      g_snprintf(color,(sizeof(color)),"#%02x%02x%02x",(fg_color.red / 256),(fg_color.green / 256),(fg_color.blue / 256));
    }
    else 
      strcpy(color,"");
    gtk_imhtml_toggle_forecolor(imhtml,color);
    if (!((flags & PURPLE_CONNECTION_NO_BGCOLOR) != 0U) && (strcmp(purple_prefs_get_string("/pidgin/conversations/bgcolor"),"") != 0)) {
      gdk_color_parse(purple_prefs_get_string("/pidgin/conversations/bgcolor"),&bg_color);
      g_snprintf(color,(sizeof(color)),"#%02x%02x%02x",(bg_color.red / 256),(bg_color.green / 256),(bg_color.blue / 256));
    }
    else 
      strcpy(color,"");
    gtk_imhtml_toggle_background(imhtml,color);
    if ((flags & PURPLE_CONNECTION_FORMATTING_WBFO) != 0U) 
      gtk_imhtml_set_whole_buffer_formatting_only(imhtml,(!0));
    else 
      gtk_imhtml_set_whole_buffer_formatting_only(imhtml,0);
  }
  else {
    buttons = (GTK_IMHTML_SMILEY | GTK_IMHTML_IMAGE);
    imhtml_clear_formatting(imhtml);
  }
  if ((flags & PURPLE_CONNECTION_NO_IMAGES) != 0U) 
    buttons &= (~GTK_IMHTML_IMAGE);
  if ((flags & PURPLE_CONNECTION_ALLOW_CUSTOM_SMILEY) != 0U) 
    buttons |= GTK_IMHTML_CUSTOM_SMILEY;
  else 
    buttons &= (~GTK_IMHTML_CUSTOM_SMILEY);
  gtk_imhtml_set_format_functions(imhtml,buttons);
}
/*******
 * GtkIMHtmlSmiley functions
 *******/

static void gtk_custom_smiley_allocated(GdkPixbufLoader *loader,gpointer user_data)
{
  GtkIMHtmlSmiley *smiley;
  smiley = ((GtkIMHtmlSmiley *)user_data);
  smiley -> icon = gdk_pixbuf_loader_get_animation(loader);
  if ((smiley -> icon) != 0) 
    g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(smiley -> icon)),((GType )(20 << 2))))));
#ifdef DEBUG_CUSTOM_SMILEY
#endif
}

static void gtk_custom_smiley_closed(GdkPixbufLoader *loader,gpointer user_data)
{
  GtkIMHtmlSmiley *smiley;
  GtkWidget *icon = (GtkWidget *)((void *)0);
  GtkTextChildAnchor *anchor = (GtkTextChildAnchor *)((void *)0);
  GSList *current = (GSList *)((void *)0);
  smiley = ((GtkIMHtmlSmiley *)user_data);
  if (!((smiley -> imhtml) != 0)) {
#ifdef DEBUG_CUSTOM_SMILEY
#endif
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)loader),((GType )(20 << 2))))));
    smiley -> loader = ((GdkPixbufLoader *)((void *)0));
    return ;
  }
  for (current = (smiley -> anchors); current != 0; current = ((current != 0)?( *((GSList *)current)).next : ((struct _GSList *)((void *)0)))) {
    anchor = ((GtkTextChildAnchor *)(g_type_check_instance_cast(((GTypeInstance *)(current -> data)),gtk_text_child_anchor_get_type())));
    if (gtk_text_child_anchor_get_deleted(anchor) != 0) 
      icon = ((GtkWidget *)((void *)0));
    else 
      icon = gtk_image_new_from_animation((smiley -> icon));
#ifdef DEBUG_CUSTOM_SMILEY
#endif
    if (icon != 0) {
      GList *wids;
      gtk_widget_show(icon);
      wids = gtk_text_child_anchor_get_widgets(anchor);
      g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)anchor),((GType )(20 << 2))))),"gtkimhtml_plaintext",(purple_unescape_html((smiley -> smile))),g_free);
      g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)anchor),((GType )(20 << 2))))),"gtkimhtml_htmltext",(g_strdup((smiley -> smile))),g_free);
      if ((smiley -> imhtml) != 0) {
        if (wids != 0) {
          GList *children = gtk_container_get_children(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(wids -> data)),gtk_container_get_type()))));
          g_list_foreach(children,((GFunc )gtk_widget_destroy),0);
          g_list_free(children);
          gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(wids -> data)),gtk_container_get_type()))),icon);
        }
        else 
          gtk_text_view_add_child_at_anchor(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)(smiley -> imhtml)),gtk_text_view_get_type()))),icon,anchor);
      }
      g_list_free(wids);
    }
    g_object_unref(anchor);
  }
  g_slist_free((smiley -> anchors));
  smiley -> anchors = ((GSList *)((void *)0));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)loader),((GType )(20 << 2))))));
  smiley -> loader = ((GdkPixbufLoader *)((void *)0));
}

static void gtk_custom_smiley_size_prepared(GdkPixbufLoader *loader,gint width,gint height,gpointer data)
{
  if (purple_prefs_get_bool("/pidgin/conversations/resize_custom_smileys") != 0) {
    int custom_smileys_size = purple_prefs_get_int("/pidgin/conversations/custom_smileys_size");
    if ((width <= custom_smileys_size) && (height <= custom_smileys_size)) 
      return ;
    if (width >= height) {
      height = ((height * custom_smileys_size) / width);
      width = custom_smileys_size;
    }
    else {
      width = ((width * custom_smileys_size) / height);
      height = custom_smileys_size;
    }
  }
  gdk_pixbuf_loader_set_size(loader,width,height);
}

void gtk_imhtml_smiley_reload(GtkIMHtmlSmiley *smiley)
{
  if ((smiley -> icon) != 0) 
    g_object_unref((smiley -> icon));
  if ((smiley -> loader) != 0) 
/* XXX: does this crash? */
    g_object_unref((smiley -> loader));
  smiley -> icon = ((GdkPixbufAnimation *)((void *)0));
  smiley -> loader = ((GdkPixbufLoader *)((void *)0));
  if ((smiley -> file) != 0) {
/* We do not use the pixbuf loader for a smiley that can be loaded
		 * from a file. (e.g., local custom smileys)
		 */
    return ;
  }
  smiley -> loader = gdk_pixbuf_loader_new();
  g_signal_connect_data((smiley -> loader),"area_prepared",((GCallback )gtk_custom_smiley_allocated),smiley,0,((GConnectFlags )0));
  g_signal_connect_data((smiley -> loader),"closed",((GCallback )gtk_custom_smiley_closed),smiley,0,((GConnectFlags )0));
  g_signal_connect_data((smiley -> loader),"size_prepared",((GCallback )gtk_custom_smiley_size_prepared),smiley,0,((GConnectFlags )0));
}

GtkIMHtmlSmiley *gtk_imhtml_smiley_create(const char *file,const char *shortcut,gboolean hide,GtkIMHtmlSmileyFlags flags)
{
  GtkIMHtmlSmiley *smiley = (GtkIMHtmlSmiley *)(g_malloc0_n(1,(sizeof(GtkIMHtmlSmiley ))));
  smiley -> file = g_strdup(file);
  smiley -> smile = g_strdup(shortcut);
  smiley -> hidden = hide;
  smiley -> flags = flags;
  smiley -> imhtml = ((GtkIMHtml *)((void *)0));
  gtk_imhtml_smiley_reload(smiley);
  return smiley;
}

void gtk_imhtml_smiley_destroy(GtkIMHtmlSmiley *smiley)
{
  gtk_imhtml_disassociate_smiley(smiley);
  g_free((smiley -> smile));
  g_free((smiley -> file));
  if ((smiley -> icon) != 0) 
    g_object_unref((smiley -> icon));
  if ((smiley -> loader) != 0) 
    g_object_unref((smiley -> loader));
  g_free((smiley -> data));
  g_free(smiley);
}

gboolean gtk_imhtml_class_register_protocol(const char *name,gboolean (*activate)(GtkIMHtml *, GtkIMHtmlLink *),gboolean (*context_menu)(GtkIMHtml *, GtkIMHtmlLink *, GtkWidget *))
{
  GtkIMHtmlClass *klass;
  GtkIMHtmlProtocol *proto;
  do {
    if (name != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name");
      return 0;
    };
  }while (0);
  klass = (g_type_class_ref(gtk_imhtml_get_type()));
  do {
    if (klass != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"klass");
      return 0;
    };
  }while (0);
  if ((proto = imhtml_find_protocol(name,(!0))) != 0) {
    if (activate != 0) {
      return 0;
    }
    klass -> protocols = g_list_remove((klass -> protocols),proto);
    g_free((proto -> name));
    g_free(proto);
    return (!0);
  }
  else if (!(activate != 0)) {
    return 0;
  }
  proto = ((GtkIMHtmlProtocol *)(g_malloc0_n(1,(sizeof(GtkIMHtmlProtocol )))));
  proto -> name = g_strdup(name);
  proto -> length = (strlen(name));
  proto -> activate = activate;
  proto -> context_menu = context_menu;
  klass -> protocols = g_list_prepend((klass -> protocols),proto);
  return (!0);
}

static void gtk_imhtml_activate_tag(GtkIMHtml *imhtml,GtkTextTag *tag)
{
/* A link was clicked--we emit the "url_clicked" signal
	 * with the URL as the argument */
  g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))));
  g_signal_emit(imhtml,signals[URL_CLICKED],0,g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))),"link_url"));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))));
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tag),((GType )(20 << 2))))),"visited",((gpointer )((gpointer )((glong )(!0)))));
  gtk_imhtml_set_link_color(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),tag);
}

gboolean gtk_imhtml_link_activate(GtkIMHtmlLink *link)
{
  do {
    if (link != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"link");
      return 0;
    };
  }while (0);
  if ((link -> tag) != 0) {
    gtk_imhtml_activate_tag((link -> imhtml),(link -> tag));
  }
  else if ((link -> url) != 0) {
    g_signal_emit((link -> imhtml),signals[URL_CLICKED],0,(link -> url));
  }
  else 
    return 0;
  return (!0);
}

const char *gtk_imhtml_link_get_url(GtkIMHtmlLink *link)
{
  return (link -> url);
}

const GtkTextTag *gtk_imhtml_link_get_text_tag(GtkIMHtmlLink *link)
{
  return (link -> tag);
}

static gboolean return_add_newline_cb(GtkWidget *widget,gpointer data)
{
  GtkTextBuffer *buffer;
  GtkTextMark *mark;
  GtkTextIter iter;
  buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type()))));
/* Delete any currently selected text */
  gtk_text_buffer_delete_selection(buffer,(!0),(!0));
/* Insert a newline at the current cursor position */
  mark = gtk_text_buffer_get_insert(buffer);
  gtk_text_buffer_get_iter_at_mark(buffer,&iter,mark);
  gtk_imhtml_insert_html_at_iter(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_imhtml_get_type()))),"\n",0,&iter);
/*
	 * If we just newlined ourselves past the end of the visible area
	 * then scroll down so the cursor is in view.
	 */
  gtk_text_view_scroll_to_mark(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_text_view_get_type()))),gtk_text_buffer_get_insert(buffer),0,0,0.0,0.0);
  return (!0);
}
/*
 * It's kind of a pain that we need this function and the above just
 * to reinstate the default GtkTextView behavior.  It might be better
 * if GtkIMHtml didn't intercept the enter key and just required the
 * application to deal with it--it's really not much more work than it
 * is to connect to the current "message_send" signal.
 */

void gtk_imhtml_set_return_inserts_newline(GtkIMHtml *imhtml)
{
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"message_send",((GCallback )return_add_newline_cb),0,0,((GConnectFlags )0));
}

void gtk_imhtml_set_populate_primary_clipboard(GtkIMHtml *imhtml,gboolean populate)
{
  gulong signal_id;
  signal_id = g_signal_handler_find((imhtml -> text_buffer),(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_UNBLOCKED),0,0,0,mark_set_so_update_selection_cb,0);
  if (populate != 0) {
    if (!(signal_id != 0UL)) {
/* We didn't find an unblocked signal handler, which means there
			   is a blocked handler. Now unblock it.
			   This is necessary to avoid a mutex-lock when the debug message
			   saying 'no handler is blocked' is printed in the debug window.
				-- sad
			 */
      g_signal_handlers_unblock_matched((imhtml -> text_buffer),G_SIGNAL_MATCH_FUNC,0,0,0,mark_set_so_update_selection_cb,0);
    }
  }
  else {
/* Block only if we found an unblocked handler */
    if (signal_id != 0UL) 
      g_signal_handler_block((imhtml -> text_buffer),signal_id);
  }
}
