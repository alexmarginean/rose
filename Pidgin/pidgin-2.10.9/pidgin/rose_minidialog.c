/**
 * @file minidialog.c Implementation of the #PidginMiniDialog Gtk widget.
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include <gtk/gtk.h>
#include "libpurple/prefs.h"
#include "pidgin/minidialog.h"
#include "pidgin/pidgin.h"
#include "pidgin/pidginstock.h"
static void pidgin_mini_dialog_init(PidginMiniDialog *self);
static void pidgin_mini_dialog_class_init(PidginMiniDialogClass *klass);
static gpointer pidgin_mini_dialog_parent_class = (gpointer )((void *)0);

static void pidgin_mini_dialog_class_intern_init(gpointer klass)
{
  pidgin_mini_dialog_parent_class = g_type_class_peek_parent(klass);
  pidgin_mini_dialog_class_init(((PidginMiniDialogClass *)klass));
}

GType pidgin_mini_dialog_get_type()
{
  static GType g_define_type_id = 0;
  if (g_define_type_id == 0) {
    static const GTypeInfo g_define_type_info = {((sizeof(PidginMiniDialogClass ))), ((GBaseInitFunc )((GBaseInitFunc )((void *)0))), ((GBaseFinalizeFunc )((GBaseFinalizeFunc )((void *)0))), ((GClassInitFunc )pidgin_mini_dialog_class_intern_init), ((GClassFinalizeFunc )((GClassFinalizeFunc )((void *)0))), ((gconstpointer )((void *)0)), ((sizeof(PidginMiniDialog ))), (0), ((GInstanceInitFunc )pidgin_mini_dialog_init), ((const GTypeValueTable *)((void *)0))
/* class_data */
/* n_preallocs */
};
    g_define_type_id = g_type_register_static(gtk_vbox_get_type(),"PidginMiniDialog",&g_define_type_info,0);
  }
  return g_define_type_id;
}
enum __unnamed_enum___F0_L73_C1_PROP_TITLE__COMMA__PROP_DESCRIPTION__COMMA__PROP_ICON_NAME__COMMA__PROP_CUSTOM_ICON__COMMA__PROP_ENABLE_DESCRIPTION_MARKUP__COMMA__LAST_PROPERTY {PROP_TITLE=1,PROP_DESCRIPTION,PROP_ICON_NAME,PROP_CUSTOM_ICON,PROP_ENABLE_DESCRIPTION_MARKUP,LAST_PROPERTY}HazeConnectionProperties;
typedef struct _PidginMiniDialogPrivate {
GtkImage *icon;
GtkBox *title_box;
GtkLabel *title;
GtkLabel *desc;
GtkBox *buttons;
gboolean enable_description_markup;
guint idle_destroy_cb_id;}PidginMiniDialogPrivate;
#define PIDGIN_MINI_DIALOG_GET_PRIVATE(dialog) \
	((PidginMiniDialogPrivate *) ((dialog)->priv))

static PidginMiniDialog *mini_dialog_new(const gchar *title,const gchar *description)
{
  return (g_object_new(pidgin_mini_dialog_get_type(),"title",title,"description",description,((void *)((void *)0))));
}

PidginMiniDialog *pidgin_mini_dialog_new(const gchar *title,const gchar *description,const gchar *icon_name)
{
  PidginMiniDialog *mini_dialog = mini_dialog_new(title,description);
  pidgin_mini_dialog_set_icon_name(mini_dialog,icon_name);
  return mini_dialog;
}

PidginMiniDialog *pidgin_mini_dialog_new_with_custom_icon(const gchar *title,const gchar *description,GdkPixbuf *custom_icon)
{
  PidginMiniDialog *mini_dialog = mini_dialog_new(title,description);
  pidgin_mini_dialog_set_custom_icon(mini_dialog,custom_icon);
  return mini_dialog;
}

void pidgin_mini_dialog_set_title(PidginMiniDialog *mini_dialog,const char *title)
{
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)mini_dialog),((GType )(20 << 2))))),"title",title,((void *)((void *)0)));
}

void pidgin_mini_dialog_set_description(PidginMiniDialog *mini_dialog,const char *description)
{
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)mini_dialog),((GType )(20 << 2))))),"description",description,((void *)((void *)0)));
}

void pidgin_mini_dialog_enable_description_markup(PidginMiniDialog *mini_dialog)
{
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)mini_dialog),((GType )(20 << 2))))),"enable-description-markup",!0,((void *)((void *)0)));
}

gboolean pidgin_mini_dialog_links_supported()
{
#if GTK_CHECK_VERSION(2,18,0)
  return (!0);
#else
#endif
}

void pidgin_mini_dialog_set_link_callback(PidginMiniDialog *mini_dialog,GCallback cb,gpointer user_data)
{
  g_signal_connect_data(( *((PidginMiniDialogPrivate *)(mini_dialog -> priv))).desc,"activate-link",cb,user_data,0,((GConnectFlags )0));
}

void pidgin_mini_dialog_set_icon_name(PidginMiniDialog *mini_dialog,const char *icon_name)
{
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)mini_dialog),((GType )(20 << 2))))),"icon-name",icon_name,((void *)((void *)0)));
}

void pidgin_mini_dialog_set_custom_icon(PidginMiniDialog *mini_dialog,GdkPixbuf *custom_icon)
{
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)mini_dialog),((GType )(20 << 2))))),"custom-icon",custom_icon,((void *)((void *)0)));
}

struct _mini_dialog_button_clicked_cb_data 
{
  PidginMiniDialog *mini_dialog;
  PidginMiniDialogCallback callback;
  gpointer user_data;
  gboolean close_dialog_after_click;
}
;

guint pidgin_mini_dialog_get_num_children(PidginMiniDialog *mini_dialog)
{
  return g_list_length(( *(mini_dialog -> contents)).children);
}

static gboolean idle_destroy_cb(GtkWidget *mini_dialog)
{
  gtk_widget_destroy(mini_dialog);
  return 0;
}

static void mini_dialog_button_clicked_cb(GtkButton *button,gpointer user_data)
{
  struct _mini_dialog_button_clicked_cb_data *data = user_data;
  PidginMiniDialogPrivate *priv = (PidginMiniDialogPrivate *)( *(data -> mini_dialog)).priv;
  if ((data -> close_dialog_after_click) != 0) {
/* Set up the destruction callback before calling the clicked callback,
		 * so that if the mini-dialog gets destroyed during the clicked callback
		 * the idle_destroy_cb is correctly removed by _finalize.
		 */
    priv -> idle_destroy_cb_id = g_idle_add(((GSourceFunc )idle_destroy_cb),(data -> mini_dialog));
  }
  if ((data -> callback) != ((void (*)(PidginMiniDialog *, GtkButton *, gpointer ))((void *)0))) 
    ( *(data -> callback))((data -> mini_dialog),button,(data -> user_data));
}

static void mini_dialog_button_destroy_cb(GtkButton *button,gpointer user_data)
{
  struct _mini_dialog_button_clicked_cb_data *data = user_data;
  g_free(data);
}

static void mini_dialog_add_button(PidginMiniDialog *self,const char *text,PidginMiniDialogCallback clicked_cb,gpointer user_data,gboolean close_dialog_after_click)
{
  PidginMiniDialogPrivate *priv = (PidginMiniDialogPrivate *)(self -> priv);
  struct _mini_dialog_button_clicked_cb_data *callback_data = (struct _mini_dialog_button_clicked_cb_data *)(g_malloc0_n(1,(sizeof(struct _mini_dialog_button_clicked_cb_data ))));
  GtkWidget *button = gtk_button_new();
  GtkWidget *label = gtk_label_new(0);
  char *button_text = g_strdup_printf("<span size=\"smaller\">%s</span>",text);
  gtk_label_set_markup_with_mnemonic(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),button_text);
  g_free(button_text);
  callback_data -> mini_dialog = self;
  callback_data -> callback = clicked_cb;
  callback_data -> user_data = user_data;
  callback_data -> close_dialog_after_click = close_dialog_after_click;
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",((GCallback )mini_dialog_button_clicked_cb),callback_data,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"destroy",((GCallback )mini_dialog_button_destroy_cb),callback_data,0,((GConnectFlags )0));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0.5,0.5);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_container_get_type()))),label);
  gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> buttons)),gtk_box_get_type()))),button,0,0,0);
  gtk_widget_show_all(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_widget_get_type()))));
}

void pidgin_mini_dialog_add_button(PidginMiniDialog *self,const char *text,PidginMiniDialogCallback clicked_cb,gpointer user_data)
{
  mini_dialog_add_button(self,text,clicked_cb,user_data,(!0));
}

void pidgin_mini_dialog_add_non_closing_button(PidginMiniDialog *self,const char *text,PidginMiniDialogCallback clicked_cb,gpointer user_data)
{
  mini_dialog_add_button(self,text,clicked_cb,user_data,0);
}

static void pidgin_mini_dialog_get_property(GObject *object,guint property_id,GValue *value,GParamSpec *pspec)
{
  PidginMiniDialog *self = (PidginMiniDialog *)(g_type_check_instance_cast(((GTypeInstance *)object),pidgin_mini_dialog_get_type()));
  PidginMiniDialogPrivate *priv = (PidginMiniDialogPrivate *)(self -> priv);
{
    switch(property_id){
      case PROP_TITLE:
{
        g_value_set_string(value,gtk_label_get_text((priv -> title)));
        break; 
      }
      case PROP_DESCRIPTION:
{
        g_value_set_string(value,gtk_label_get_text((priv -> desc)));
        break; 
      }
      case PROP_ICON_NAME:
{
{
          gchar *icon_name = (gchar *)((void *)0);
          GtkIconSize size;
          gtk_image_get_stock((priv -> icon),&icon_name,&size);
          g_value_set_string(value,icon_name);
          break; 
        }
      }
      case PROP_CUSTOM_ICON:
{
        g_value_set_object(value,(gtk_image_get_pixbuf((priv -> icon))));
        break; 
      }
      case PROP_ENABLE_DESCRIPTION_MARKUP:
{
        g_value_set_boolean(value,(priv -> enable_description_markup));
        break; 
      }
      default:
{
        do {
          GObject *_glib__object = (GObject *)object;
          GParamSpec *_glib__pspec = (GParamSpec *)pspec;
          guint _glib__property_id = property_id;
          g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","minidialog.c:311","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
        }while (0);
      }
    }
  }
}

static void mini_dialog_set_title(PidginMiniDialog *self,const char *title)
{
  PidginMiniDialogPrivate *priv = (PidginMiniDialogPrivate *)(self -> priv);
  char *title_esc = g_markup_escape_text(title,(-1));
  char *title_markup = g_strdup_printf("<span weight=\"bold\" size=\"smaller\">%s</span>",((title_esc != 0)?title_esc : ""));
  gtk_label_set_markup((priv -> title),title_markup);
  g_free(title_esc);
  g_free(title_markup);
}

static void mini_dialog_set_description(PidginMiniDialog *self,const char *description)
{
  PidginMiniDialogPrivate *priv = (PidginMiniDialogPrivate *)(self -> priv);
  if (description != 0) {
    char *desc_esc = ((priv -> enable_description_markup) != 0)?g_strdup(description) : g_markup_escape_text(description,(-1));
    char *desc_markup = g_strdup_printf("<span size=\"smaller\">%s</span>",desc_esc);
    gtk_label_set_markup((priv -> desc),desc_markup);
    g_free(desc_esc);
    g_free(desc_markup);
    gtk_widget_show(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> desc)),gtk_widget_get_type()))));
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> desc)),((GType )(20 << 2))))),"no-show-all",0,((void *)((void *)0)));
  }
  else {
    gtk_label_set_text((priv -> desc),0);
    gtk_widget_hide(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> desc)),gtk_widget_get_type()))));
/* make calling show_all() on the minidialog not affect desc
		 * even though it's packed inside it.
	 	 */
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> desc)),((GType )(20 << 2))))),"no-show-all",!0,((void *)((void *)0)));
  }
}

static void pidgin_mini_dialog_set_property(GObject *object,guint property_id,const GValue *value,GParamSpec *pspec)
{
  PidginMiniDialog *self = (PidginMiniDialog *)(g_type_check_instance_cast(((GTypeInstance *)object),pidgin_mini_dialog_get_type()));
  PidginMiniDialogPrivate *priv = (PidginMiniDialogPrivate *)(self -> priv);
  switch(property_id){
    case PROP_TITLE:
{
      mini_dialog_set_title(self,g_value_get_string(value));
      break; 
    }
    case PROP_DESCRIPTION:
{
      mini_dialog_set_description(self,g_value_get_string(value));
      break; 
    }
    case PROP_ICON_NAME:
{
      gtk_image_set_from_stock((priv -> icon),g_value_get_string(value),gtk_icon_size_from_name("pidgin-icon-size-tango-extra-small"));
      break; 
    }
    case PROP_CUSTOM_ICON:
{
      gtk_image_set_from_pixbuf((priv -> icon),(g_value_get_object(value)));
      break; 
    }
    case PROP_ENABLE_DESCRIPTION_MARKUP:
{
      priv -> enable_description_markup = g_value_get_boolean(value);
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = property_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","minidialog.c:389","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
    }
  }
}

static void pidgin_mini_dialog_finalize(GObject *object)
{
  PidginMiniDialog *self = (PidginMiniDialog *)(g_type_check_instance_cast(((GTypeInstance *)object),pidgin_mini_dialog_get_type()));
  PidginMiniDialogPrivate *priv = (PidginMiniDialogPrivate *)(self -> priv);
  if ((priv -> idle_destroy_cb_id) != 0U) 
    g_source_remove((priv -> idle_destroy_cb_id));
  g_free(priv);
  self -> priv = ((gpointer )((void *)0));
  purple_prefs_disconnect_by_handle(self);
  ( *( *((GObjectClass *)(g_type_check_class_cast(((GTypeClass *)pidgin_mini_dialog_parent_class),((GType )(20 << 2)))))).finalize)(object);
}

static void pidgin_mini_dialog_class_init(PidginMiniDialogClass *klass)
{
  GObjectClass *object_class = (GObjectClass *)(g_type_check_class_cast(((GTypeClass *)klass),((GType )(20 << 2))));
  GParamSpec *param_spec;
  object_class -> get_property = pidgin_mini_dialog_get_property;
  object_class -> set_property = pidgin_mini_dialog_set_property;
  object_class -> finalize = pidgin_mini_dialog_finalize;
  param_spec = g_param_spec_string("title","title","String specifying the mini-dialog\'s title",0,(G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | (G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(object_class,PROP_TITLE,param_spec);
  param_spec = g_param_spec_string("description","description","Description text for the mini-dialog, if desired",0,(G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | (G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(object_class,PROP_DESCRIPTION,param_spec);
  param_spec = g_param_spec_string("icon-name","icon-name","String specifying the Gtk stock name of the dialog\'s icon",0,(G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | (G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(object_class,PROP_ICON_NAME,param_spec);
  param_spec = g_param_spec_object("custom-icon","custom-icon","Pixbuf to use as the dialog\'s icon",gdk_pixbuf_get_type(),(G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | (G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(object_class,PROP_CUSTOM_ICON,param_spec);
  param_spec = g_param_spec_boolean("enable-description-markup","enable-description-markup","Use GMarkup in the description text",0,(G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | (G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(object_class,PROP_ENABLE_DESCRIPTION_MARKUP,param_spec);
}
/* 16 is the width of the icon, due to PIDGIN_ICON_SIZE_TANGO_EXTRA_SMALL */
#define BLIST_WIDTH_OTHER_THAN_LABEL \
	((PIDGIN_HIG_BOX_SPACE * 3) + 16)
#define BLIST_WIDTH_PREF \
	(PIDGIN_PREFS_ROOT "/blist/width")

static void blist_width_changed_cb(const char *name,PurplePrefType type,gconstpointer val,gpointer data)
{
  PidginMiniDialog *self = (PidginMiniDialog *)(g_type_check_instance_cast(((GTypeInstance *)data),pidgin_mini_dialog_get_type()));
  PidginMiniDialogPrivate *priv = (PidginMiniDialogPrivate *)(self -> priv);
  guint blist_width = ((gint )((glong )val));
  guint label_width = (blist_width - (6 * 3 + 16));
  gtk_widget_set_size_request(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> title)),gtk_widget_get_type()))),label_width,(-1));
  gtk_widget_set_size_request(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> desc)),gtk_widget_get_type()))),label_width,(-1));
}

static void pidgin_mini_dialog_init(PidginMiniDialog *self)
{
  GtkBox *self_box = (GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)self),gtk_box_get_type()));
  guint blist_width = (purple_prefs_get_int("/pidgin/blist/width"));
  guint label_width = (blist_width - (6 * 3 + 16));
  PidginMiniDialogPrivate *priv = (PidginMiniDialogPrivate *)(g_malloc0_n(1,(sizeof(PidginMiniDialogPrivate ))));
  self -> priv = priv;
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)self),gtk_container_get_type()))),6);
  priv -> title_box = ((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_hbox_new(0,6))),gtk_box_get_type())));
  priv -> icon = ((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_image_new())),gtk_image_get_type())));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> icon)),gtk_misc_get_type()))),0,0);
  priv -> title = ((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_label_new(0))),gtk_label_get_type())));
  gtk_widget_set_size_request(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> title)),gtk_widget_get_type()))),label_width,(-1));
  gtk_label_set_line_wrap((priv -> title),(!0));
  gtk_label_set_selectable((priv -> title),(!0));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> title)),gtk_misc_get_type()))),0,0);
  gtk_box_pack_start((priv -> title_box),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> icon)),gtk_widget_get_type()))),0,0,0);
  gtk_box_pack_start((priv -> title_box),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> title)),gtk_widget_get_type()))),(!0),(!0),0);
  priv -> desc = ((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_label_new(0))),gtk_label_get_type())));
  gtk_widget_set_size_request(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> desc)),gtk_widget_get_type()))),label_width,(-1));
  gtk_label_set_line_wrap((priv -> desc),(!0));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> desc)),gtk_misc_get_type()))),0,0);
  gtk_label_set_selectable((priv -> desc),(!0));
/* make calling show_all() on the minidialog not affect desc even though
	 * it's packed inside it.
	 */
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> desc)),((GType )(20 << 2))))),"no-show-all",!0,((void *)((void *)0)));
  purple_prefs_connect_callback(self,"/pidgin/blist/width",blist_width_changed_cb,self);
  self -> contents = ((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_vbox_new(0,0))),gtk_box_get_type())));
  priv -> buttons = ((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_hbox_new(0,0))),gtk_box_get_type())));
  gtk_box_pack_start(self_box,((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> title_box)),gtk_widget_get_type()))),0,0,0);
  gtk_box_pack_start(self_box,((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> desc)),gtk_widget_get_type()))),0,0,0);
  gtk_box_pack_start(self_box,((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(self -> contents)),gtk_widget_get_type()))),(!0),(!0),0);
  gtk_box_pack_start(self_box,((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> buttons)),gtk_widget_get_type()))),0,0,0);
  gtk_widget_show_all(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)self),gtk_widget_get_type()))));
}
