/*
 * @file gtksound.c GTK+ Sound
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "pidgin.h"
#ifdef _WIN32
#include <windows.h>
#include <mmsystem.h>
#endif
#ifdef USE_GSTREAMER
# include <gst/gst.h>
#endif /* USE_GSTREAMER */
#include "debug.h"
#include "notify.h"
#include "prefs.h"
#include "sound.h"
#include "sound-theme.h"
#include "theme-manager.h"
#include "util.h"
#include "gtkconv.h"
#include "gtksound.h"

struct pidgin_sound_event 
{
  char *label;
  char *pref;
  char *def;
}
;
static guint mute_login_sounds_timeout = 0;
static gboolean mute_login_sounds = 0;
#ifdef USE_GSTREAMER
static gboolean gst_init_failed;
#endif /* USE_GSTREAMER */
static const struct pidgin_sound_event sounds[12UL] = {{("Buddy logs in"), ("login"), ("login.wav")}, {("Buddy logs out"), ("logout"), ("logout.wav")}, {("Message received"), ("im_recv"), ("receive.wav")}, {("Message received begins conversation"), ("first_im_recv"), ("receive.wav")}, {("Message sent"), ("send_im"), ("send.wav")}, {("Person enters chat"), ("join_chat"), ("login.wav")}, {("Person leaves chat"), ("left_chat"), ("logout.wav")}, {("You talk in chat"), ("send_chat_msg"), ("send.wav")}, {("Others talk in chat"), ("chat_msg_recv"), ("receive.wav")}, 
/* this isn't a terminator, it's the buddy pounce default sound event ;-) */
{((char *)((void *)0)), ("pounce_default"), ("alert.wav")}, {("Someone says your username in chat"), ("nick_said"), ("alert.wav")}, {("Attention received"), ("got_attention"), ("alert.wav")}};

static gboolean unmute_login_sounds_cb(gpointer data)
{
  mute_login_sounds = 0;
  mute_login_sounds_timeout = 0;
  return 0;
}

static gboolean chat_nick_matches_name(PurpleConversation *conv,const char *aname)
{
  PurpleConvChat *chat = (PurpleConvChat *)((void *)0);
  char *nick = (char *)((void *)0);
  char *name = (char *)((void *)0);
  gboolean ret = 0;
  chat = purple_conversation_get_chat_data(conv);
  if (chat == ((PurpleConvChat *)((void *)0))) 
    return ret;
  nick = g_strdup(purple_normalize((conv -> account),(chat -> nick)));
  name = g_strdup(purple_normalize((conv -> account),aname));
  if (g_utf8_collate(nick,name) == 0) 
    ret = (!0);
  g_free(nick);
  g_free(name);
  return ret;
}
/*
 * play a sound event for a conversation, honoring make_sound flag
 * of conversation and checking for focus if conv_focus pref is set
 */

static void play_conv_event(PurpleConversation *conv,PurpleSoundEventID event)
{
/* If we should not play the sound for some reason, then exit early */
  if ((conv != ((PurpleConversation *)((void *)0))) && (purple_conversation_get_ui_ops(conv) == pidgin_conversations_get_conv_ui_ops())) {
    PidginConversation *gtkconv;
    gboolean has_focus;
    gtkconv = ((PidginConversation *)(conv -> ui_data));
    has_focus = purple_conversation_has_focus(conv);
    if (!((gtkconv -> make_sound) != 0) || ((has_focus != 0) && !(purple_prefs_get_bool("/pidgin/sound/conv_focus") != 0))) {
      return ;
    }
  }
  purple_sound_play_event(event,((conv != 0)?purple_conversation_get_account(conv) : ((struct _PurpleAccount *)((void *)0))));
}

static void buddy_state_cb(PurpleBuddy *buddy,PurpleSoundEventID event)
{
  purple_sound_play_event(event,(purple_buddy_get_account(buddy)));
}

static void im_msg_received_cb(PurpleAccount *account,char *sender,char *message,PurpleConversation *conv,PurpleMessageFlags flags,PurpleSoundEventID event)
{
  if (((flags & PURPLE_MESSAGE_DELAYED) != 0U) || ((flags & PURPLE_MESSAGE_NOTIFY) != 0U)) 
    return ;
  if (conv == ((PurpleConversation *)((void *)0))) 
    purple_sound_play_event(PURPLE_SOUND_FIRST_RECEIVE,account);
  else 
    play_conv_event(conv,event);
}

static void im_msg_sent_cb(PurpleAccount *account,const char *receiver,const char *message,PurpleSoundEventID event)
{
  PurpleConversation *conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,receiver,account);
  play_conv_event(conv,event);
}

static void chat_buddy_join_cb(PurpleConversation *conv,const char *name,PurpleConvChatBuddyFlags flags,gboolean new_arrival,PurpleSoundEventID event)
{
  if ((new_arrival != 0) && !(chat_nick_matches_name(conv,name) != 0)) 
    play_conv_event(conv,event);
}

static void chat_buddy_left_cb(PurpleConversation *conv,const char *name,const char *reason,PurpleSoundEventID event)
{
  if (!(chat_nick_matches_name(conv,name) != 0)) 
    play_conv_event(conv,event);
}

static void chat_msg_sent_cb(PurpleAccount *account,const char *message,int id,PurpleSoundEventID event)
{
  PurpleConnection *conn = purple_account_get_connection(account);
  PurpleConversation *conv = (PurpleConversation *)((void *)0);
  if (conn != ((PurpleConnection *)((void *)0))) 
    conv = purple_find_chat(conn,id);
  play_conv_event(conv,event);
}

static void chat_msg_received_cb(PurpleAccount *account,char *sender,char *message,PurpleConversation *conv,PurpleMessageFlags flags,PurpleSoundEventID event)
{
  PurpleConvChat *chat;
  if (((flags & PURPLE_MESSAGE_DELAYED) != 0U) || ((flags & PURPLE_MESSAGE_NOTIFY) != 0U)) 
    return ;
  chat = purple_conversation_get_chat_data(conv);
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return ;
    };
  }while (0);
  if (purple_conv_chat_is_user_ignored(chat,sender) != 0) 
    return ;
  if (chat_nick_matches_name(conv,sender) != 0) 
    return ;
  if (((flags & PURPLE_MESSAGE_NICK) != 0U) || (purple_utf8_has_word(message,(chat -> nick)) != 0)) 
/* This isn't quite right; if you have the PURPLE_SOUND_CHAT_NICK event disabled
		 * and the PURPLE_SOUND_CHAT_SAY event enabled, you won't get a sound at all */
    play_conv_event(conv,PURPLE_SOUND_CHAT_NICK);
  else 
    play_conv_event(conv,event);
}

static void got_attention_cb(PurpleAccount *account,const char *who,PurpleConversation *conv,guint type,PurpleSoundEventID event)
{
  play_conv_event(conv,event);
}
/*
 * We mute sounds for the 10 seconds after you log in so that
 * you don't get flooded with sounds when the blist shows all
 * your buddies logging in.
 */

static void account_signon_cb(PurpleConnection *gc,gpointer data)
{
  if (mute_login_sounds_timeout != 0) 
    purple_timeout_remove(mute_login_sounds_timeout);
  mute_login_sounds = (!0);
  mute_login_sounds_timeout = purple_timeout_add_seconds(10,unmute_login_sounds_cb,0);
}

const char *pidgin_sound_get_event_option(PurpleSoundEventID event)
{
  if (event >= PURPLE_NUM_SOUNDS) 
    return 0;
  return sounds[event].pref;
}

const char *pidgin_sound_get_event_label(PurpleSoundEventID event)
{
  if (event >= PURPLE_NUM_SOUNDS) 
    return 0;
  return sounds[event].label;
}

void *pidgin_sound_get_handle()
{
  static int handle;
  return (&handle);
}

static void pidgin_sound_init()
{
  void *gtk_sound_handle = pidgin_sound_get_handle();
  void *blist_handle = purple_blist_get_handle();
  void *conv_handle = purple_conversations_get_handle();
#ifdef USE_GSTREAMER
  GError *error = (GError *)((void *)0);
#endif
  purple_signal_connect(purple_connections_get_handle(),"signed-on",gtk_sound_handle,((PurpleCallback )account_signon_cb),0);
  purple_prefs_add_none("/pidgin/sound");
  purple_prefs_add_none("/pidgin/sound/enabled");
  purple_prefs_add_none("/pidgin/sound/file");
  purple_prefs_add_bool("/pidgin/sound/enabled/login",(!0));
  purple_prefs_add_path("/pidgin/sound/file/login","");
  purple_prefs_add_bool("/pidgin/sound/enabled/logout",(!0));
  purple_prefs_add_path("/pidgin/sound/file/logout","");
  purple_prefs_add_bool("/pidgin/sound/enabled/im_recv",(!0));
  purple_prefs_add_path("/pidgin/sound/file/im_recv","");
  purple_prefs_add_bool("/pidgin/sound/enabled/first_im_recv",0);
  purple_prefs_add_path("/pidgin/sound/file/first_im_recv","");
  purple_prefs_add_bool("/pidgin/sound/enabled/send_im",(!0));
  purple_prefs_add_path("/pidgin/sound/file/send_im","");
  purple_prefs_add_bool("/pidgin/sound/enabled/join_chat",0);
  purple_prefs_add_path("/pidgin/sound/file/join_chat","");
  purple_prefs_add_bool("/pidgin/sound/enabled/left_chat",0);
  purple_prefs_add_path("/pidgin/sound/file/left_chat","");
  purple_prefs_add_bool("/pidgin/sound/enabled/send_chat_msg",0);
  purple_prefs_add_path("/pidgin/sound/file/send_chat_msg","");
  purple_prefs_add_bool("/pidgin/sound/enabled/chat_msg_recv",0);
  purple_prefs_add_path("/pidgin/sound/file/chat_msg_recv","");
  purple_prefs_add_bool("/pidgin/sound/enabled/nick_said",0);
  purple_prefs_add_path("/pidgin/sound/file/nick_said","");
  purple_prefs_add_bool("/pidgin/sound/enabled/pounce_default",(!0));
  purple_prefs_add_path("/pidgin/sound/file/pounce_default","");
  purple_prefs_add_string("/pidgin/sound/theme","");
  purple_prefs_add_bool("/pidgin/sound/enabled/sent_attention",(!0));
  purple_prefs_add_path("/pidgin/sound/file/sent_attention","");
  purple_prefs_add_bool("/pidgin/sound/enabled/got_attention",(!0));
  purple_prefs_add_path("/pidgin/sound/file/got_attention","");
  purple_prefs_add_bool("/pidgin/sound/conv_focus",(!0));
  purple_prefs_add_bool("/pidgin/sound/mute",0);
  purple_prefs_add_path("/pidgin/sound/command","");
  purple_prefs_add_string("/pidgin/sound/method","automatic");
  purple_prefs_add_int("/pidgin/sound/volume",50);
#ifdef USE_GSTREAMER
  purple_debug_info("sound","Initializing sound output drivers.\n");
#ifdef GST_CAN_DISABLE_FORKING
  gst_registry_fork_set_enabled(0);
#endif
  if ((gst_init_failed = !(gst_init_check(0,0,&error) != 0)) != 0) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","GStreamer Failure"))),((const char *)(dgettext("pidgin","GStreamer failed to initialize."))),((error != 0)?(error -> message) : ""),0,0);
    if (error != 0) {
      g_error_free(error);
      error = ((GError *)((void *)0));
    }
  }
#endif /* USE_GSTREAMER */
  purple_signal_connect(blist_handle,"buddy-signed-on",gtk_sound_handle,((PurpleCallback )buddy_state_cb),0);
  purple_signal_connect(blist_handle,"buddy-signed-off",gtk_sound_handle,((PurpleCallback )buddy_state_cb),((void *)((gpointer )((glong )PURPLE_SOUND_BUDDY_LEAVE))));
  purple_signal_connect(conv_handle,"received-im-msg",gtk_sound_handle,((PurpleCallback )im_msg_received_cb),((void *)((gpointer )((glong )PURPLE_SOUND_RECEIVE))));
  purple_signal_connect(conv_handle,"sent-im-msg",gtk_sound_handle,((PurpleCallback )im_msg_sent_cb),((void *)((gpointer )((glong )PURPLE_SOUND_SEND))));
  purple_signal_connect(conv_handle,"chat-buddy-joined",gtk_sound_handle,((PurpleCallback )chat_buddy_join_cb),((void *)((gpointer )((glong )PURPLE_SOUND_CHAT_JOIN))));
  purple_signal_connect(conv_handle,"chat-buddy-left",gtk_sound_handle,((PurpleCallback )chat_buddy_left_cb),((void *)((gpointer )((glong )PURPLE_SOUND_CHAT_LEAVE))));
  purple_signal_connect(conv_handle,"sent-chat-msg",gtk_sound_handle,((PurpleCallback )chat_msg_sent_cb),((void *)((gpointer )((glong )PURPLE_SOUND_CHAT_YOU_SAY))));
  purple_signal_connect(conv_handle,"received-chat-msg",gtk_sound_handle,((PurpleCallback )chat_msg_received_cb),((void *)((gpointer )((glong )PURPLE_SOUND_CHAT_SAY))));
  purple_signal_connect(conv_handle,"got-attention",gtk_sound_handle,((PurpleCallback )got_attention_cb),((void *)((gpointer )((glong )PURPLE_SOUND_GOT_ATTENTION))));
/* for the time being, don't handle sent-attention here, since playing a
	 sound would result induplicate sounds. And fixing that would require changing the
	 conversation signal for msg-recv */
}

static void pidgin_sound_uninit()
{
#ifdef USE_GSTREAMER
  if (!(gst_init_failed != 0)) 
    gst_deinit();
#endif
  purple_signals_disconnect_by_handle(pidgin_sound_get_handle());
}
#ifdef USE_GSTREAMER

static gboolean bus_call(GstBus *bus,GstMessage *msg,gpointer data)
{
  GstElement *play = data;
  GError *err = (GError *)((void *)0);
  switch(( *((GstMessage *)msg)).type){
    case GST_MESSAGE_ERROR:
{
      gst_message_parse_error(msg,&err,0);
      purple_debug_error("gstreamer","%s\n",(err -> message));
      g_error_free(err);
    }
/* fall-through and clean up */
    case GST_MESSAGE_EOS:
{
      gst_element_set_state(play,GST_STATE_NULL);
      gst_object_unref(((GstObject *)(g_type_check_instance_cast(((GTypeInstance *)play),gst_object_get_type()))));
      break; 
    }
    case GST_MESSAGE_WARNING:
{
      gst_message_parse_warning(msg,&err,0);
      purple_debug_warning("gstreamer","%s\n",(err -> message));
      g_error_free(err);
      break; 
    }
    default:
{
      break; 
    }
  }
  return (!0);
}
#endif
#ifndef _WIN32

static gboolean expire_old_child(gpointer data)
{
  pid_t pid = (gint )((glong )data);
  if (waitpid(pid,0,1 | 2) < 0) {
    if ( *__errno_location() == 10) 
      return 0;
    else 
      purple_debug_warning("gtksound","Child is ill, pid: %d (%s)\n",pid,strerror( *__errno_location()));
  }
  if (kill(pid,9) < 0) 
    purple_debug_error("gtksound","Killing process %d failed (%s)\n",pid,strerror( *__errno_location()));
  return 0;
}
#endif

static void pidgin_sound_play_file(const char *filename)
{
  const char *method;
#ifdef USE_GSTREAMER
  float volume;
  char *uri;
  GstElement *sink = (GstElement *)((void *)0);
  GstElement *play = (GstElement *)((void *)0);
  GstBus *bus = (GstBus *)((void *)0);
#endif
  if (purple_prefs_get_bool("/pidgin/sound/mute") != 0) 
    return ;
  method = purple_prefs_get_string("/pidgin/sound/method");
  if (!(strcmp(method,"none") != 0)) {
    return ;
  }
  else if (!(strcmp(method,"beep") != 0)) {
    gdk_beep();
    return ;
  }
  if (!(g_file_test(filename,G_FILE_TEST_EXISTS) != 0)) {
    purple_debug_error("gtksound","sound file (%s) does not exist.\n",filename);
    return ;
  }
#ifndef _WIN32
  if (!(strcmp(method,"custom") != 0)) {
    const char *sound_cmd;
    char *command;
    char *esc_filename;
    char **argv = (char **)((void *)0);
    GError *error = (GError *)((void *)0);
    GPid pid;
    sound_cmd = purple_prefs_get_path("/pidgin/sound/command");
    if (!(sound_cmd != 0) || (( *sound_cmd) == 0)) {
      purple_debug_error("gtksound","\'Command\' sound method has been chosen, but no command has been set.\n");
      return ;
    }
    esc_filename = g_shell_quote(filename);
    if (strstr(sound_cmd,"%s") != 0) 
      command = purple_strreplace(sound_cmd,"%s",esc_filename);
    else 
      command = g_strdup_printf("%s %s",sound_cmd,esc_filename);
    if (!(g_shell_parse_argv(command,0,&argv,&error) != 0)) {
      purple_debug_error("gtksound","error parsing command %s (%s)\n",command,(error -> message));
      g_error_free(error);
      g_free(esc_filename);
      g_free(command);
      return ;
    }
    if (!(g_spawn_async(0,argv,0,(G_SPAWN_SEARCH_PATH | G_SPAWN_DO_NOT_REAP_CHILD),0,0,&pid,&error) != 0)) {
      purple_debug_error("gtksound","sound command could not be launched: %s\n",(error -> message));
      g_error_free(error);
    }
    else {
      purple_timeout_add_seconds(15,expire_old_child,((gpointer )((glong )pid)));
    }
    g_strfreev(argv);
    g_free(esc_filename);
    g_free(command);
    return ;
  }
#endif /* _WIN32 */
#ifdef USE_GSTREAMER
/* Perhaps do gdk_beep instead? */
  if (gst_init_failed != 0) 
    return ;
  volume = (((float )(((purple_prefs_get_int("/pidgin/sound/volume") > 100)?100 : (((purple_prefs_get_int("/pidgin/sound/volume") < 0)?0 : purple_prefs_get_int("/pidgin/sound/volume")))))) / 50);
  if (!(strcmp(method,"automatic") != 0)) {
    sink = gst_element_factory_make("gconfaudiosink","sink");
  }
  else 
#ifndef _WIN32
if (!(strcmp(method,"esd") != 0)) {
    sink = gst_element_factory_make("esdsink","sink");
  }
  else if (!(strcmp(method,"alsa") != 0)) {
    sink = gst_element_factory_make("alsasink","sink");
  }
  else 
#endif
{
    purple_debug_error("sound","Unknown sound method \'%s\'\n",method);
    return ;
  }
  if ((strcmp(method,"automatic") != 0) && !(sink != 0)) {
    purple_debug_error("sound","Unable to create GStreamer audiosink.\n");
    return ;
  }
  play = gst_element_factory_make("playbin","play");
  if (play == ((GstElement *)((void *)0))) {
    return ;
  }
  uri = g_strdup_printf("file://%s",filename);
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)play),((GType )(20 << 2))))),"uri",uri,"volume",volume,"audio-sink",sink,((void *)((void *)0)));
  bus = gst_pipeline_get_bus(((GstPipeline *)(g_type_check_instance_cast(((GTypeInstance *)play),gst_pipeline_get_type()))));
  gst_bus_add_watch(bus,bus_call,play);
  gst_element_set_state(play,GST_STATE_PLAYING);
  gst_object_unref(bus);
  g_free(uri);
#else /* #ifdef USE_GSTREAMER */
#ifndef _WIN32
#else /* _WIN32 */
#endif /* _WIN32 */
#endif /* USE_GSTREAMER */
}

static void pidgin_sound_play_event(PurpleSoundEventID event)
{
  char *enable_pref;
  char *file_pref;
  const char *theme_name;
  PurpleSoundTheme *theme;
  if ((event == PURPLE_SOUND_BUDDY_ARRIVE) && (mute_login_sounds != 0)) 
    return ;
  if (event >= PURPLE_NUM_SOUNDS) {
    purple_debug_error("sound","got request for unknown sound: %d\n",event);
    return ;
  }
  enable_pref = g_strdup_printf("/pidgin/sound/enabled/%s",sounds[event].pref);
  file_pref = g_strdup_printf("/pidgin/sound/file/%s",sounds[event].pref);
/* check NULL for sounds that don't have an option, ie buddy pounce */
  if (purple_prefs_get_bool(enable_pref) != 0) {
    char *filename = g_strdup(purple_prefs_get_path(file_pref));
    theme_name = purple_prefs_get_string("/pidgin/sound/theme");
    if (((theme_name != 0) && (( *theme_name) != 0)) && (!(filename != 0) || !(( *filename) != 0))) {
/* Use theme */
      g_free(filename);
      theme = ((PurpleSoundTheme *)(g_type_check_instance_cast(((GTypeInstance *)(purple_theme_manager_find_theme(theme_name,"sound"))),purple_sound_theme_get_type())));
      filename = purple_sound_theme_get_file_full(theme,sounds[event].pref);
/* Use Default sound in this case */
      if (!(g_file_test(filename,G_FILE_TEST_IS_REGULAR) != 0)) {
        purple_debug_error("sound","The file: (%s) %s\n from theme: %s, was not found or wasn\'t readable\n",sounds[event].pref,filename,theme_name);
        g_free(filename);
        filename = ((char *)((void *)0));
      }
    }
/* Use Default sounds */
    if (!(filename != 0) || !(strlen(filename) != 0UL)) {
      g_free(filename);
/* XXX Consider creating a constant for "sounds/purple" to be shared with Finch */
      filename = g_build_filename("/usr/local/share","sounds","purple",sounds[event].def,((void *)((void *)0)));
    }
    purple_sound_play_file(filename,0);
    g_free(filename);
  }
  g_free(enable_pref);
  g_free(file_pref);
}

gboolean pidgin_sound_is_customized()
{
  gint i;
  gchar *path;
  const char *file;
  for (i = 0; i < PURPLE_NUM_SOUNDS; i++) {
    path = g_strdup_printf("/pidgin/sound/file/%s",sounds[i].pref);
    file = purple_prefs_get_path(path);
    g_free(path);
    if ((file != 0) && (file[0] != 0)) 
      return (!0);
  }
  return 0;
}
static PurpleSoundUiOps sound_ui_ops = {(pidgin_sound_init), (pidgin_sound_uninit), (pidgin_sound_play_file), (pidgin_sound_play_event), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))};

PurpleSoundUiOps *pidgin_sound_get_ui_ops()
{
  return &sound_ui_ops;
}
