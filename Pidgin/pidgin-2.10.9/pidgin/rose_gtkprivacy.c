/**
 * @file gtkprivacy.c GTK+ Privacy UI
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "pidgin.h"
#include "connection.h"
#include "debug.h"
#include "privacy.h"
#include "request.h"
#include "util.h"
#include "gtkblist.h"
#include "gtkprivacy.h"
#include "gtkutils.h"
typedef struct __unnamed_class___F0_L39_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__win__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__type_menu__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__add_button__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__remove_button__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__removeall_button__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__close_button__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__button_box__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__allow_widget__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__block_widget__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1172R__Pe___variable_name_unknown_scope_and_name__scope__allow_store__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1172R__Pe___variable_name_unknown_scope_and_name__scope__block_store__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__allow_list__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__block_list__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__in_allow_list__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L247R__Pe___variable_name_unknown_scope_and_name__scope__account {
GtkWidget *win;
GtkWidget *type_menu;
GtkWidget *add_button;
GtkWidget *remove_button;
GtkWidget *removeall_button;
GtkWidget *close_button;
GtkWidget *button_box;
GtkWidget *allow_widget;
GtkWidget *block_widget;
GtkListStore *allow_store;
GtkListStore *block_store;
GtkWidget *allow_list;
GtkWidget *block_list;
gboolean in_allow_list;
PurpleAccount *account;}PidginPrivacyDialog;
typedef struct __unnamed_class___F0_L66_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L247R__Pe___variable_name_unknown_scope_and_name__scope__account__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__name__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__block {
PurpleAccount *account;
char *name;
gboolean block;}PidginPrivacyRequestData;
static const struct __unnamed_class___F0_L74_C8_unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__text__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__num {
const char *text;
int num;}menu_entries[] = {{("Allow all users to contact me"), (PURPLE_PRIVACY_ALLOW_ALL)}, {("Allow only the users on my buddy list"), (PURPLE_PRIVACY_ALLOW_BUDDYLIST)}, {("Allow only the users below"), (PURPLE_PRIVACY_ALLOW_USERS)}, {("Block all users"), (PURPLE_PRIVACY_DENY_ALL)}, {("Block only the users below"), (PURPLE_PRIVACY_DENY_USERS)}};
static const size_t menu_entry_count = (sizeof(menu_entries) / sizeof(( *menu_entries)));
static PidginPrivacyDialog *privacy_dialog = (PidginPrivacyDialog *)((void *)0);

static void rebuild_allow_list(PidginPrivacyDialog *dialog)
{
  GSList *l;
  GtkTreeIter iter;
  gtk_list_store_clear((dialog -> allow_store));
  for (l = ( *(dialog -> account)).permit; l != ((GSList *)((void *)0)); l = (l -> next)) {
    gtk_list_store_append((dialog -> allow_store),&iter);
    gtk_list_store_set((dialog -> allow_store),&iter,0,(l -> data),-1);
  }
}

static void rebuild_block_list(PidginPrivacyDialog *dialog)
{
  GSList *l;
  GtkTreeIter iter;
  gtk_list_store_clear((dialog -> block_store));
  for (l = ( *(dialog -> account)).deny; l != ((GSList *)((void *)0)); l = (l -> next)) {
    gtk_list_store_append((dialog -> block_store),&iter);
    gtk_list_store_set((dialog -> block_store),&iter,0,(l -> data),-1);
  }
}

static void user_selected_cb(GtkTreeSelection *sel,PidginPrivacyDialog *dialog)
{
  gtk_widget_set_sensitive((dialog -> remove_button),(!0));
}

static GtkWidget *build_list(PidginPrivacyDialog *dialog,GtkListStore *model,GtkWidget **ret_treeview)
{
  GtkWidget *sw;
  GtkWidget *treeview;
  GtkCellRenderer *rend;
  GtkTreeViewColumn *column;
  GtkTreeSelection *sel;
  treeview = gtk_tree_view_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_tree_model_get_type()))));
   *ret_treeview = treeview;
  rend = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes(0,rend,"text",0,((void *)((void *)0)));
  gtk_tree_view_column_set_clickable(((GtkTreeViewColumn *)(g_type_check_instance_cast(((GTypeInstance *)column),gtk_tree_view_column_get_type()))),(!0));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),column);
  gtk_tree_view_set_headers_visible(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),0);
  sw = pidgin_make_scrollable(treeview,GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC,GTK_SHADOW_IN,-1,200);
  gtk_widget_show(treeview);
  sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)sel),((GType )(20 << 2))))),"changed",((GCallback )user_selected_cb),dialog,0,((GConnectFlags )0));
  return sw;
}

static GtkWidget *build_allow_list(PidginPrivacyDialog *dialog)
{
  GtkWidget *widget;
  GtkWidget *list;
  dialog -> allow_store = gtk_list_store_new(1,((GType )(16 << 2)));
  gtk_tree_sortable_set_sort_column_id(((GtkTreeSortable *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> allow_store)),gtk_tree_sortable_get_type()))),0,GTK_SORT_ASCENDING);
  widget = build_list(dialog,(dialog -> allow_store),&list);
  dialog -> allow_list = list;
  rebuild_allow_list(dialog);
  return widget;
}

static GtkWidget *build_block_list(PidginPrivacyDialog *dialog)
{
  GtkWidget *widget;
  GtkWidget *list;
  dialog -> block_store = gtk_list_store_new(1,((GType )(16 << 2)));
  gtk_tree_sortable_set_sort_column_id(((GtkTreeSortable *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> block_store)),gtk_tree_sortable_get_type()))),0,GTK_SORT_ASCENDING);
  widget = build_list(dialog,(dialog -> block_store),&list);
  dialog -> block_list = list;
  rebuild_block_list(dialog);
  return widget;
}

static gint destroy_cb(GtkWidget *w,GdkEvent *event,PidginPrivacyDialog *dialog)
{
  pidgin_privacy_dialog_hide();
  return 0;
}

static void select_account_cb(GtkWidget *dropdown,PurpleAccount *account,PidginPrivacyDialog *dialog)
{
  int i;
  dialog -> account = account;
{
    for (i = 0; i < menu_entry_count; i++) {
      if (menu_entries[i].num == (account -> perm_deny)) {
        gtk_combo_box_set_active(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> type_menu)),gtk_combo_box_get_type()))),i);
        break; 
      }
    }
  }
  rebuild_allow_list(dialog);
  rebuild_block_list(dialog);
}
/*
 * TODO: Setting the permit/deny setting needs to go through privacy.c
 *       Even better: the privacy API needs to not suck.
 */

static void type_changed_cb(GtkComboBox *combo,PidginPrivacyDialog *dialog)
{
  int new_type = menu_entries[gtk_combo_box_get_active(combo)].num;
  ( *(dialog -> account)).perm_deny = new_type;
  serv_set_permit_deny(purple_account_get_connection((dialog -> account)));
  gtk_widget_hide((dialog -> allow_widget));
  gtk_widget_hide((dialog -> block_widget));
  gtk_widget_hide_all((dialog -> button_box));
  if (new_type == PURPLE_PRIVACY_ALLOW_USERS) {
    gtk_widget_show((dialog -> allow_widget));
    gtk_widget_show_all((dialog -> button_box));
    dialog -> in_allow_list = (!0);
  }
  else if (new_type == PURPLE_PRIVACY_DENY_USERS) {
    gtk_widget_show((dialog -> block_widget));
    gtk_widget_show_all((dialog -> button_box));
    dialog -> in_allow_list = 0;
  }
  gtk_widget_show_all((dialog -> close_button));
  gtk_widget_show((dialog -> button_box));
  purple_blist_schedule_save();
  pidgin_blist_refresh(purple_get_blist());
}

static void add_cb(GtkWidget *button,PidginPrivacyDialog *dialog)
{
  if ((dialog -> in_allow_list) != 0) 
    pidgin_request_add_permit((dialog -> account),0);
  else 
    pidgin_request_add_block((dialog -> account),0);
}

static void remove_cb(GtkWidget *button,PidginPrivacyDialog *dialog)
{
  GtkTreeIter iter;
  GtkTreeModel *model;
  GtkTreeSelection *sel;
  char *name;
  if (((dialog -> in_allow_list) != 0) && ((dialog -> allow_store) == ((GtkListStore *)((void *)0)))) 
    return ;
  if (!((dialog -> in_allow_list) != 0) && ((dialog -> block_store) == ((GtkListStore *)((void *)0)))) 
    return ;
  if ((dialog -> in_allow_list) != 0) {
    model = ((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> allow_store)),gtk_tree_model_get_type())));
    sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> allow_list)),gtk_tree_view_get_type()))));
  }
  else {
    model = ((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> block_store)),gtk_tree_model_get_type())));
    sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> block_list)),gtk_tree_view_get_type()))));
  }
  if (gtk_tree_selection_get_selected(sel,0,&iter) != 0) 
    gtk_tree_model_get(model,&iter,0,&name,-1);
  else 
    return ;
  if ((dialog -> in_allow_list) != 0) 
    purple_privacy_permit_remove((dialog -> account),name,0);
  else 
    purple_privacy_deny_remove((dialog -> account),name,0);
  g_free(name);
}

static void removeall_cb(GtkWidget *button,PidginPrivacyDialog *dialog)
{
  GSList *l;
  if ((dialog -> in_allow_list) != 0) 
    l = ( *(dialog -> account)).permit;
  else 
    l = ( *(dialog -> account)).deny;
  while(l != 0){
    char *user;
    user = (l -> data);
    l = (l -> next);
    if ((dialog -> in_allow_list) != 0) 
      purple_privacy_permit_remove((dialog -> account),user,0);
    else 
      purple_privacy_deny_remove((dialog -> account),user,0);
  }
}

static void close_cb(GtkWidget *button,PidginPrivacyDialog *dialog)
{
  gtk_widget_destroy((dialog -> win));
  pidgin_privacy_dialog_hide();
}

static PidginPrivacyDialog *privacy_dialog_new()
{
  PidginPrivacyDialog *dialog;
  GtkWidget *vbox;
  GtkWidget *button;
  GtkWidget *dropdown;
  GtkWidget *label;
  int selected = -1;
  int i;
  dialog = ((PidginPrivacyDialog *)(g_malloc0_n(1,(sizeof(PidginPrivacyDialog )))));
  dialog -> win = pidgin_create_dialog(((const char *)(dgettext("pidgin","Privacy"))),12,"privacy",(!0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> win)),((GType )(20 << 2))))),"delete_event",((GCallback )destroy_cb),dialog,0,((GConnectFlags )0));
/* Main vbox */
  vbox = pidgin_dialog_get_vbox_with_properties(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> win)),gtk_dialog_get_type()))),0,12);
/* Description label */
  label = gtk_label_new(((const char *)(dgettext("pidgin","Changes to privacy settings take effect immediately."))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),label,0,0,0);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_widget_show(label);
/* Accounts drop-down */
  dropdown = pidgin_account_option_menu_new(0,0,((GCallback )select_account_cb),0,dialog);
  pidgin_add_widget_to_vbox(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),((const char *)(dgettext("pidgin","Set privacy for:"))),0,dropdown,(!0),0);
  dialog -> account = pidgin_account_option_menu_get_selected(dropdown);
/* Add the drop-down list with the allow/block types. */
  dialog -> type_menu = gtk_combo_box_new_text();
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),(dialog -> type_menu),0,0,0);
  gtk_widget_show((dialog -> type_menu));
  for (i = 0; i < menu_entry_count; i++) {
    gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> type_menu)),gtk_combo_box_get_type()))),((const char *)(dgettext("pidgin",menu_entries[i].text))));
    if (menu_entries[i].num == ( *(dialog -> account)).perm_deny) 
      selected = i;
  }
  gtk_combo_box_set_active(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> type_menu)),gtk_combo_box_get_type()))),selected);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> type_menu)),((GType )(20 << 2))))),"changed",((GCallback )type_changed_cb),dialog,0,((GConnectFlags )0));
/* Build the treeview for the allow list. */
  dialog -> allow_widget = build_allow_list(dialog);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),(dialog -> allow_widget),(!0),(!0),0);
/* Build the treeview for the block list. */
  dialog -> block_widget = build_block_list(dialog);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),(dialog -> block_widget),(!0),(!0),0);
/* Add the button box for Add, Remove, Remove All */
  dialog -> button_box = pidgin_dialog_get_action_area(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> win)),gtk_dialog_get_type()))));
/* Add button */
  button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> win)),gtk_dialog_get_type()))),"gtk-add",((GCallback )add_cb),dialog);
  dialog -> add_button = button;
/* Remove button */
  button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> win)),gtk_dialog_get_type()))),"gtk-remove",((GCallback )remove_cb),dialog);
  dialog -> remove_button = button;
/* TODO: This button should be sensitive/invisitive more cleverly */
  gtk_widget_set_sensitive(button,0);
/* Remove All button */
  button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> win)),gtk_dialog_get_type()))),((const char *)(dgettext("pidgin","Remove Al_l"))),((GCallback )removeall_cb),dialog);
  dialog -> removeall_button = button;
/* Close button */
  button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> win)),gtk_dialog_get_type()))),"gtk-close",((GCallback )close_cb),dialog);
  dialog -> close_button = button;
  type_changed_cb(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> type_menu)),gtk_combo_box_get_type()))),dialog);
#if 0
#endif
  return dialog;
}

void pidgin_privacy_dialog_show()
{
  do {
    if (purple_connections_get_all() != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_connections_get_all() != NULL");
      return ;
    };
  }while (0);
  if (privacy_dialog == ((PidginPrivacyDialog *)((void *)0))) 
    privacy_dialog = privacy_dialog_new();
  gtk_widget_show((privacy_dialog -> win));
  gdk_window_raise(( *(privacy_dialog -> win)).window);
}

void pidgin_privacy_dialog_hide()
{
  if (privacy_dialog == ((PidginPrivacyDialog *)((void *)0))) 
    return ;
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(privacy_dialog -> allow_store)),((GType )(20 << 2))))));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(privacy_dialog -> block_store)),((GType )(20 << 2))))));
  g_free(privacy_dialog);
  privacy_dialog = ((PidginPrivacyDialog *)((void *)0));
}

static void destroy_request_data(PidginPrivacyRequestData *data)
{
  g_free((data -> name));
  g_free(data);
}

static void confirm_permit_block_cb(PidginPrivacyRequestData *data,int option)
{
  if ((data -> block) != 0) 
    purple_privacy_deny((data -> account),(data -> name),0,0);
  else 
    purple_privacy_allow((data -> account),(data -> name),0,0);
  destroy_request_data(data);
}

static void add_permit_block_cb(PidginPrivacyRequestData *data,const char *name)
{
  data -> name = g_strdup(name);
  confirm_permit_block_cb(data,0);
}

void pidgin_request_add_permit(PurpleAccount *account,const char *name)
{
  PidginPrivacyRequestData *data;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  data = ((PidginPrivacyRequestData *)(g_malloc0_n(1,(sizeof(PidginPrivacyRequestData )))));
  data -> account = account;
  data -> name = g_strdup(name);
  data -> block = 0;
  if (name == ((const char *)((void *)0))) {
    purple_request_input(account,((const char *)(dgettext("pidgin","Permit User"))),((const char *)(dgettext("pidgin","Type a user you permit to contact you."))),((const char *)(dgettext("pidgin","Please enter the name of the user you wish to be able to contact you."))),0,0,0,0,((const char *)(dgettext("pidgin","_Permit"))),((GCallback )add_permit_block_cb),((const char *)(dgettext("pidgin","Cancel"))),((GCallback )destroy_request_data),account,name,0,data);
  }
  else {
    char *primary = g_strdup_printf(((const char *)(dgettext("pidgin","Allow %s to contact you\?"))),name);
    char *secondary = g_strdup_printf(((const char *)(dgettext("pidgin","Are you sure you wish to allow %s to contact you\?"))),name);
    purple_request_action(account,((const char *)(dgettext("pidgin","Permit User"))),primary,secondary,0,account,name,0,data,2,((const char *)(dgettext("pidgin","_Permit"))),((GCallback )confirm_permit_block_cb),((const char *)(dgettext("pidgin","Cancel"))),((GCallback )destroy_request_data));
    g_free(primary);
    g_free(secondary);
  }
}

void pidgin_request_add_block(PurpleAccount *account,const char *name)
{
  PidginPrivacyRequestData *data;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  data = ((PidginPrivacyRequestData *)(g_malloc0_n(1,(sizeof(PidginPrivacyRequestData )))));
  data -> account = account;
  data -> name = g_strdup(name);
  data -> block = (!0);
  if (name == ((const char *)((void *)0))) {
    purple_request_input(account,((const char *)(dgettext("pidgin","Block User"))),((const char *)(dgettext("pidgin","Type a user to block."))),((const char *)(dgettext("pidgin","Please enter the name of the user you wish to block."))),0,0,0,0,((const char *)(dgettext("pidgin","_Block"))),((GCallback )add_permit_block_cb),((const char *)(dgettext("pidgin","Cancel"))),((GCallback )destroy_request_data),account,name,0,data);
  }
  else {
    char *primary = g_strdup_printf(((const char *)(dgettext("pidgin","Block %s\?"))),name);
    char *secondary = g_strdup_printf(((const char *)(dgettext("pidgin","Are you sure you want to block %s\?"))),name);
    purple_request_action(account,((const char *)(dgettext("pidgin","Block User"))),primary,secondary,0,account,name,0,data,2,((const char *)(dgettext("pidgin","_Block"))),((GCallback )confirm_permit_block_cb),((const char *)(dgettext("pidgin","Cancel"))),((GCallback )destroy_request_data));
    g_free(primary);
    g_free(secondary);
  }
}

static void pidgin_permit_added_removed(PurpleAccount *account,const char *name)
{
  if (privacy_dialog != ((PidginPrivacyDialog *)((void *)0))) 
    rebuild_allow_list(privacy_dialog);
}

static void pidgin_deny_added_removed(PurpleAccount *account,const char *name)
{
  if (privacy_dialog != ((PidginPrivacyDialog *)((void *)0))) 
    rebuild_block_list(privacy_dialog);
}
static PurplePrivacyUiOps privacy_ops = {(pidgin_permit_added_removed), (pidgin_permit_added_removed), (pidgin_deny_added_removed), (pidgin_deny_added_removed), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))};

PurplePrivacyUiOps *pidgin_privacy_get_ui_ops()
{
  return &privacy_ops;
}

void pidgin_privacy_init()
{
}
