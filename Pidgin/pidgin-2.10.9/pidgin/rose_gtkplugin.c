/**
 * @file gtkplugin.c GTK+ Plugins support
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "pidgin.h"
#include "gtkplugin.h"
#include "gtkpluginpref.h"
#include "gtkutils.h"
#include "debug.h"
#include "prefs.h"
#include "request.h"
#include "pidgintooltip.h"
#include <string.h>
#define PIDGIN_RESPONSE_CONFIGURE 98121
static void plugin_toggled_stage_two(PurplePlugin *plug,GtkTreeModel *model,GtkTreeIter *iter,gboolean unload);
static GtkWidget *expander = (GtkWidget *)((void *)0);
static GtkWidget *plugin_dialog = (GtkWidget *)((void *)0);
static GtkLabel *plugin_name = (GtkLabel *)((void *)0);
static GtkTextBuffer *plugin_desc = (GtkTextBuffer *)((void *)0);
static GtkLabel *plugin_error = (GtkLabel *)((void *)0);
static GtkLabel *plugin_author = (GtkLabel *)((void *)0);
static GtkLabel *plugin_website = (GtkLabel *)((void *)0);
static gchar *plugin_website_uri = (gchar *)((void *)0);
static GtkLabel *plugin_filename = (GtkLabel *)((void *)0);
static GtkWidget *pref_button = (GtkWidget *)((void *)0);
static GHashTable *plugin_pref_dialogs = (GHashTable *)((void *)0);

GtkWidget *pidgin_plugin_get_config_frame(PurplePlugin *plugin)
{
  GtkWidget *config = (GtkWidget *)((void *)0);
  do {
    if (plugin != ((PurplePlugin *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin != NULL");
      return 0;
    };
  }while (0);
  if ((((((plugin -> info) != ((PurplePluginInfo *)((void *)0))) && (( *(plugin -> info)).ui_info != ((void *)((void *)0)))) && !(strcmp(( *(plugin -> info)).ui_requirement,"gtk-gaim") != 0)) && (( *(plugin -> info)).ui_info != 0)) && (( *((PidginPluginUiInfo *)( *(plugin -> info)).ui_info)).get_config_frame != 0)) {
    PidginPluginUiInfo *ui_info;
    ui_info = ((PidginPluginUiInfo *)( *(plugin -> info)).ui_info);
    config = ( *(ui_info -> get_config_frame))(plugin);
    if ((( *(plugin -> info)).prefs_info != 0) && (( *( *(plugin -> info)).prefs_info).get_plugin_pref_frame != 0)) {
      purple_debug_warning("gtkplugin","Plugin %s contains both, ui_info and prefs_info preferences; prefs_info will be ignored.",( *(plugin -> info)).name);
    }
  }
  if (((config == ((GtkWidget *)((void *)0))) && (( *(plugin -> info)).prefs_info != 0)) && (( *( *(plugin -> info)).prefs_info).get_plugin_pref_frame != 0)) {
    PurplePluginPrefFrame *frame;
    frame = ( *( *( *(plugin -> info)).prefs_info).get_plugin_pref_frame)(plugin);
    config = pidgin_plugin_pref_create_frame(frame);
    ( *( *(plugin -> info)).prefs_info).frame = frame;
  }
  return config;
}

void pidgin_plugins_save()
{
  purple_plugins_save_loaded("/pidgin/plugins/loaded");
}

static void update_plugin_list(void *data)
{
  GtkListStore *ls = (GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)data),gtk_list_store_get_type()));
  GtkTreeIter iter;
  GList *probes;
  PurplePlugin *plug;
  gtk_list_store_clear(ls);
  purple_plugins_probe("so");
  for (probes = purple_plugins_get_all(); probes != ((GList *)((void *)0)); probes = (probes -> next)) {{
      char *name;
      char *version;
      char *summary;
      char *desc;
      plug = (probes -> data);
      if (( *(plug -> info)).type == PURPLE_PLUGIN_LOADER) {
        GList *cur;
        for (cur = ( *((PurplePluginLoaderInfo *)( *(plug -> info)).extra_info)).exts; cur != ((GList *)((void *)0)); cur = (cur -> next)) 
          purple_plugins_probe((cur -> data));
        continue; 
      }
      else if ((( *(plug -> info)).type != PURPLE_PLUGIN_STANDARD) || ((( *(plug -> info)).flags & 1) != 0UL)) {
        continue; 
      }
      gtk_list_store_append(ls,&iter);
      if (( *(plug -> info)).name != 0) {
        name = g_markup_escape_text(((const char *)(dgettext("pidgin",( *(plug -> info)).name))),(-1));
      }
      else {
        char *tmp = g_path_get_basename((plug -> path));
        name = g_markup_escape_text(tmp,(-1));
        g_free(tmp);
      }
      version = g_markup_escape_text(purple_plugin_get_version(plug),(-1));
      summary = g_markup_escape_text(purple_plugin_get_summary(plug),(-1));
      desc = g_strdup_printf("<b>%s</b> %s\n%s",name,version,summary);
      g_free(name);
      g_free(version);
      g_free(summary);
      gtk_list_store_set(ls,&iter,0,purple_plugin_is_loaded(plug),1,desc,2,plug,3,purple_plugin_is_unloadable(plug),-1);
      g_free(desc);
    }
  }
}

static void plugin_loading_common(PurplePlugin *plugin,GtkTreeView *view,gboolean loaded)
{
  GtkTreeIter iter;
  GtkTreeModel *model = gtk_tree_view_get_model(view);
  if (gtk_tree_model_get_iter_first(model,&iter) != 0) {{
      do {{
          PurplePlugin *plug;
          GtkTreeSelection *sel;
          gtk_tree_model_get(model,&iter,2,&plug,-1);
          if (plug != plugin) 
            continue; 
          gtk_list_store_set(((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_list_store_get_type()))),&iter,0,loaded,-1);
/* If the loaded/unloaded plugin is the selected row,
			 * update the pref_button. */
          sel = gtk_tree_view_get_selection(view);
          if (gtk_tree_selection_get_selected(sel,&model,&iter) != 0) {
            gtk_tree_model_get(model,&iter,2,&plug,-1);
            if (plug == plugin) {
              gtk_widget_set_sensitive(pref_button,((loaded != 0) && (((((((plug -> info) != ((PurplePluginInfo *)((void *)0))) && (( *(plug -> info)).ui_info != ((void *)((void *)0)))) && !(strcmp(( *(plug -> info)).ui_requirement,"gtk-gaim") != 0)) && (( *(plug -> info)).ui_info != 0)) && (( *((PidginPluginUiInfo *)( *(plug -> info)).ui_info)).get_config_frame != 0)) || ((( *(plug -> info)).prefs_info != 0) && (( *( *(plug -> info)).prefs_info).get_plugin_pref_frame != 0)))));
            }
          }
          break; 
        }
      }while (gtk_tree_model_iter_next(model,&iter) != 0);
    }
  }
}

static void plugin_load_cb(PurplePlugin *plugin,gpointer data)
{
  GtkTreeView *view = (GtkTreeView *)data;
  plugin_loading_common(plugin,view,(!0));
}

static void plugin_unload_cb(PurplePlugin *plugin,gpointer data)
{
  GtkTreeView *view = (GtkTreeView *)data;
  plugin_loading_common(plugin,view,0);
}

static void pref_dialog_response_cb(GtkWidget *d,int response,PurplePlugin *plug)
{
  switch(response){
    case -7:
{
    }
    case -4:
{
      g_hash_table_remove(plugin_pref_dialogs,plug);
      if (g_hash_table_size(plugin_pref_dialogs) == 0) {
        g_hash_table_destroy(plugin_pref_dialogs);
        plugin_pref_dialogs = ((GHashTable *)((void *)0));
      }
      gtk_widget_destroy(d);
      if ((( *(plug -> info)).prefs_info != 0) && (( *( *(plug -> info)).prefs_info).frame != 0)) {
        purple_plugin_pref_frame_destroy(( *( *(plug -> info)).prefs_info).frame);
        ( *( *(plug -> info)).prefs_info).frame = ((PurplePluginPrefFrame *)((void *)0));
      }
      break; 
    }
  }
}

static void plugin_unload_confirm_cb(gpointer *data)
{
  PurplePlugin *plugin = (PurplePlugin *)data[0];
  GtkTreeModel *model = (GtkTreeModel *)data[1];
  GtkTreeIter *iter = (GtkTreeIter *)data[2];
  plugin_toggled_stage_two(plugin,model,iter,(!0));
  g_free(data);
}

static void plugin_toggled(GtkCellRendererToggle *cell,gchar *pth,gpointer data)
{
  GtkTreeModel *model = (GtkTreeModel *)data;
  GtkTreeIter *iter = (GtkTreeIter *)(g_malloc_n(1,(sizeof(GtkTreeIter ))));
  GtkTreePath *path = gtk_tree_path_new_from_string(pth);
  PurplePlugin *plug;
  GtkWidget *dialog = (GtkWidget *)((void *)0);
  gtk_tree_model_get_iter(model,iter,path);
  gtk_tree_path_free(path);
  gtk_tree_model_get(model,iter,2,&plug,-1);
/* Apparently, GTK+ won't honor the sensitive flag on cell renderers for booleans. */
  if (purple_plugin_is_unloadable(plug) != 0) {
    g_free(iter);
    return ;
  }
  if (!(purple_plugin_is_loaded(plug) != 0)) {
    pidgin_set_cursor(plugin_dialog,GDK_WATCH);
    purple_plugin_load(plug);
    plugin_toggled_stage_two(plug,model,iter,0);
    pidgin_clear_cursor(plugin_dialog);
  }
  else {
    if ((plugin_pref_dialogs != ((GHashTable *)((void *)0))) && ((dialog = (g_hash_table_lookup(plugin_pref_dialogs,plug))) != 0)) 
      pref_dialog_response_cb(dialog,GTK_RESPONSE_DELETE_EVENT,plug);
    if ((plug -> dependent_plugins) != ((GList *)((void *)0))) {
      GString *tmp = g_string_new(((const char *)(dgettext("pidgin","The following plugins will be unloaded."))));
      GList *l;
      gpointer *cb_data;
      for (l = (plug -> dependent_plugins); l != ((GList *)((void *)0)); l = (l -> next)) {
        const char *dep_name = (const char *)(l -> data);
        PurplePlugin *dep_plugin = purple_plugins_find_with_id(dep_name);
        do {
          if (dep_plugin != ((PurplePlugin *)((void *)0))) {
          }
          else {
            g_return_if_fail_warning(0,((const char *)__func__),"dep_plugin != NULL");
            return ;
          };
        }while (0);
        g_string_append_printf(tmp,"\n\t%s\n",purple_plugin_get_name(dep_plugin));
      }
      cb_data = ((gpointer *)(g_malloc_n(3,(sizeof(gpointer )))));
      cb_data[0] = plug;
      cb_data[1] = model;
      cb_data[2] = iter;
      purple_request_action(plugin_dialog,0,((const char *)(dgettext("pidgin","Multiple plugins will be unloaded."))),(tmp -> str),0,0,0,0,cb_data,2,((const char *)(dgettext("pidgin","Unload Plugins"))),((GCallback )plugin_unload_confirm_cb),((const char *)(dgettext("pidgin","Cancel"))),g_free);
      g_string_free(tmp,(!0));
    }
    else 
      plugin_toggled_stage_two(plug,model,iter,(!0));
  }
}

static void plugin_toggled_stage_two(PurplePlugin *plug,GtkTreeModel *model,GtkTreeIter *iter,gboolean unload)
{
  if (unload != 0) {
    pidgin_set_cursor(plugin_dialog,GDK_WATCH);
    if (!(purple_plugin_unload(plug) != 0)) {
      const char *primary = (const char *)(dgettext("pidgin","Could not unload plugin"));
      const char *reload = (const char *)(dgettext("pidgin","The plugin could not be unloaded now, but will be disabled at the next startup."));
      if (!((plug -> error) != 0)) {
        purple_notify_message(0,PURPLE_NOTIFY_MSG_WARNING,0,primary,reload,0,0);
      }
      else {
        char *tmp = g_strdup_printf("%s\n\n%s",reload,(plug -> error));
        purple_notify_message(0,PURPLE_NOTIFY_MSG_WARNING,0,primary,tmp,0,0);
        g_free(tmp);
      }
      purple_plugin_disable(plug);
    }
    pidgin_clear_cursor(plugin_dialog);
  }
  gtk_widget_set_sensitive(pref_button,((purple_plugin_is_loaded(plug) != 0) && (((((((plug -> info) != ((PurplePluginInfo *)((void *)0))) && (( *(plug -> info)).ui_info != ((void *)((void *)0)))) && !(strcmp(( *(plug -> info)).ui_requirement,"gtk-gaim") != 0)) && (( *(plug -> info)).ui_info != 0)) && (( *((PidginPluginUiInfo *)( *(plug -> info)).ui_info)).get_config_frame != 0)) || ((( *(plug -> info)).prefs_info != 0) && (( *( *(plug -> info)).prefs_info).get_plugin_pref_frame != 0)))));
  if ((plug -> error) != ((char *)((void *)0))) {
    gchar *name = g_markup_escape_text(purple_plugin_get_name(plug),(-1));
    gchar *error = g_markup_escape_text((plug -> error),(-1));
    gchar *text;
    text = g_strdup_printf("<b>%s</b> %s\n<span weight=\"bold\" color=\"red\"%s</span>",purple_plugin_get_name(plug),purple_plugin_get_version(plug),error);
    gtk_list_store_set(((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_list_store_get_type()))),iter,1,text,-1);
    g_free(text);
    text = g_strdup_printf("<span weight=\"bold\" color=\"red\">%s</span>",error);
    gtk_label_set_markup(plugin_error,text);
    g_free(text);
    g_free(error);
    g_free(name);
  }
  gtk_list_store_set(((GtkListStore *)(g_type_check_instance_cast(((GTypeInstance *)model),gtk_list_store_get_type()))),iter,0,purple_plugin_is_loaded(plug),-1);
  g_free(iter);
  pidgin_plugins_save();
}

static gboolean ensure_plugin_visible(void *data)
{
  GtkTreeSelection *sel = (GtkTreeSelection *)(g_type_check_instance_cast(((GTypeInstance *)data),gtk_tree_selection_get_type()));
  GtkTreeView *tv = gtk_tree_selection_get_tree_view(sel);
  GtkTreeModel *model = gtk_tree_view_get_model(tv);
  GtkTreePath *path;
  GtkTreeIter iter;
  if (!(gtk_tree_selection_get_selected(sel,&model,&iter) != 0)) 
    return 0;
  path = gtk_tree_model_get_path(model,&iter);
  gtk_tree_view_scroll_to_cell(gtk_tree_selection_get_tree_view(sel),path,0,0,0,0);
  gtk_tree_path_free(path);
  return 0;
}

static void prefs_plugin_sel(GtkTreeSelection *sel,GtkTreeModel *model)
{
  gchar *buf;
  gchar *tmp;
  gchar *name;
  gchar *version;
  GtkTreeIter iter;
  GValue val;
  PurplePlugin *plug;
  if (!(gtk_tree_selection_get_selected(sel,&model,&iter) != 0)) {
    gtk_widget_set_sensitive(pref_button,0);
/* Collapse and disable the expander widget */
    gtk_expander_set_expanded(((GtkExpander *)(g_type_check_instance_cast(((GTypeInstance *)expander),gtk_expander_get_type()))),0);
    gtk_widget_set_sensitive(expander,0);
    return ;
  }
  gtk_widget_set_sensitive(expander,(!0));
  val.g_type = 0;
  gtk_tree_model_get_value(model,&iter,2,&val);
  plug = (g_value_get_pointer((&val)));
  name = g_markup_escape_text(purple_plugin_get_name(plug),(-1));
  version = g_markup_escape_text(purple_plugin_get_version(plug),(-1));
  buf = g_strdup_printf("<span size=\"larger\" weight=\"bold\">%s</span> <span size=\"smaller\">%s</span>",name,version);
  gtk_label_set_markup(plugin_name,buf);
  g_free(name);
  g_free(version);
  g_free(buf);
  gtk_text_buffer_set_text(plugin_desc,purple_plugin_get_description(plug),(-1));
  gtk_label_set_text(plugin_author,purple_plugin_get_author(plug));
  gtk_label_set_text(plugin_filename,(plug -> path));
  g_free(plugin_website_uri);
  plugin_website_uri = g_strdup(purple_plugin_get_homepage(plug));
  if (plugin_website_uri != 0) {
    tmp = g_markup_escape_text(plugin_website_uri,(-1));
    buf = g_strdup_printf("<span underline=\"single\" foreground=\"blue\">%s</span>",tmp);
    gtk_label_set_markup(plugin_website,buf);
    g_free(tmp);
    g_free(buf);
  }
  else {
    gtk_label_set_text(plugin_website,0);
  }
  if ((plug -> error) == ((char *)((void *)0))) {
    gtk_label_set_text(plugin_error,0);
  }
  else {
    tmp = g_markup_escape_text((plug -> error),(-1));
    buf = g_strdup_printf(((const char *)(dgettext("pidgin","<span foreground=\"red\" weight=\"bold\">Error: %s\nCheck the plugin website for an update.</span>"))),tmp);
    gtk_label_set_markup(plugin_error,buf);
    g_free(buf);
    g_free(tmp);
  }
  gtk_widget_set_sensitive(pref_button,((purple_plugin_is_loaded(plug) != 0) && (((((((plug -> info) != ((PurplePluginInfo *)((void *)0))) && (( *(plug -> info)).ui_info != ((void *)((void *)0)))) && !(strcmp(( *(plug -> info)).ui_requirement,"gtk-gaim") != 0)) && (( *(plug -> info)).ui_info != 0)) && (( *((PidginPluginUiInfo *)( *(plug -> info)).ui_info)).get_config_frame != 0)) || ((( *(plug -> info)).prefs_info != 0) && (( *( *(plug -> info)).prefs_info).get_plugin_pref_frame != 0)))));
/* Make sure the selected plugin is still visible */
  g_idle_add(ensure_plugin_visible,sel);
  g_value_unset(&val);
}

static void plugin_dialog_response_cb(GtkWidget *d,int response,GtkTreeSelection *sel)
{
  PurplePlugin *plug;
  GtkWidget *dialog;
  GtkWidget *box;
  GtkTreeModel *model;
  GValue val;
  GtkTreeIter iter;
{
    switch(response){
      case -7:
{
      }
      case -4:
{
        purple_request_close_with_handle(plugin_dialog);
        purple_signals_disconnect_by_handle(plugin_dialog);
        gtk_widget_destroy(d);
        if (plugin_pref_dialogs != ((GHashTable *)((void *)0))) {
          g_hash_table_destroy(plugin_pref_dialogs);
          plugin_pref_dialogs = ((GHashTable *)((void *)0));
        }
        plugin_dialog = ((GtkWidget *)((void *)0));
        break; 
      }
      case 98121:
{
        if (!(gtk_tree_selection_get_selected(sel,&model,&iter) != 0)) 
          return ;
        val.g_type = 0;
        gtk_tree_model_get_value(model,&iter,2,&val);
        plug = (g_value_get_pointer((&val)));
        if (plug == ((PurplePlugin *)((void *)0))) 
          break; 
        if ((plugin_pref_dialogs != ((GHashTable *)((void *)0))) && (g_hash_table_lookup(plugin_pref_dialogs,plug) != 0)) 
          break; 
        box = pidgin_plugin_get_config_frame(plug);
        if (box == ((GtkWidget *)((void *)0))) 
          break; 
        dialog = gtk_dialog_new_with_buttons("",((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)d),gtk_window_get_type()))),(GTK_DIALOG_NO_SEPARATOR | GTK_DIALOG_DESTROY_WITH_PARENT),"gtk-close",GTK_RESPONSE_CLOSE,((void *)((void *)0)));
        if (plugin_pref_dialogs == ((GHashTable *)((void *)0))) 
          plugin_pref_dialogs = g_hash_table_new(0,0);
        g_hash_table_insert(plugin_pref_dialogs,plug,dialog);
        g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dialog),((GType )(20 << 2))))),"response",((GCallback )pref_dialog_response_cb),plug,0,((GConnectFlags )0));
        gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type())))).vbox),gtk_container_get_type()))),box);
        gtk_window_set_role(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_window_get_type()))),"plugin_config");
        gtk_window_set_title(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_window_get_type()))),((const char *)(dgettext("pidgin",purple_plugin_get_name(plug)))));
        gtk_widget_show_all(dialog);
        g_value_unset(&val);
        break; 
      }
    }
  }
}

static void show_plugin_prefs_cb(GtkTreeView *view,GtkTreePath *path,GtkTreeViewColumn *column,GtkWidget *dialog)
{
  GtkTreeSelection *sel;
  GtkTreeIter iter;
  PurplePlugin *plugin;
  GtkTreeModel *model;
  sel = gtk_tree_view_get_selection(view);
  if (!(gtk_tree_selection_get_selected(sel,&model,&iter) != 0)) 
    return ;
  gtk_tree_model_get(model,&iter,2,&plugin,-1);
  if (!(purple_plugin_is_loaded(plugin) != 0)) 
    return ;
/* Now show the pref-dialog for the plugin */
  plugin_dialog_response_cb(dialog,98121,sel);
}

static gboolean pidgin_plugins_paint_tooltip(GtkWidget *tipwindow,gpointer data)
{
  PangoLayout *layout = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tipwindow),((GType )(20 << 2))))),"tooltip-plugin"));
  gtk_paint_layout((tipwindow -> style),(tipwindow -> window),GTK_STATE_NORMAL,0,0,tipwindow,"tooltip",6,6,layout);
  return (!0);
}

static gboolean pidgin_plugins_create_tooltip(GtkWidget *tipwindow,GtkTreePath *path,gpointer data,int *w,int *h)
{
  GtkTreeIter iter;
  GtkTreeView *treeview = (GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)data),gtk_tree_view_get_type()));
  PurplePlugin *plugin = (PurplePlugin *)((void *)0);
  GtkTreeModel *model = gtk_tree_view_get_model(treeview);
  PangoLayout *layout;
  int width;
  int height;
  char *markup;
  char *name;
  char *desc;
  char *author;
  if (!(gtk_tree_model_get_iter(model,&iter,path) != 0)) 
    return 0;
  gtk_tree_model_get(model,&iter,2,&plugin,-1);
  markup = g_strdup_printf("<span size=\'x-large\' weight=\'bold\'>%s</span>\n<b>%s:</b> %s\n<b>%s:</b> %s",(name = g_markup_escape_text(purple_plugin_get_name(plugin),(-1))),((const char *)(dgettext("pidgin","Description"))),(desc = g_markup_escape_text(purple_plugin_get_description(plugin),(-1))),((const char *)(dgettext("pidgin","Author"))),(author = g_markup_escape_text(purple_plugin_get_author(plugin),(-1))));
  layout = gtk_widget_create_pango_layout(tipwindow,0);
  pango_layout_set_markup(layout,markup,-1);
  pango_layout_set_wrap(layout,PANGO_WRAP_WORD);
  pango_layout_set_width(layout,600000);
  pango_layout_get_size(layout,&width,&height);
  g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)tipwindow),((GType )(20 << 2))))),"tooltip-plugin",layout,g_object_unref);
  if (w != 0) 
     *w = (((((int )width) + 512) >> 10) + 12);
  if (h != 0) 
     *h = (((((int )height) + 512) >> 10) + 12);
  g_free(markup);
  g_free(name);
  g_free(desc);
  g_free(author);
  return (!0);
}

static gboolean website_button_motion_cb(GtkWidget *button,GdkEventCrossing *event,gpointer unused)
{
  if (plugin_website_uri != 0) {
    pidgin_set_cursor(button,GDK_HAND2);
    return (!0);
  }
  return 0;
}

static gboolean website_button_clicked_cb(GtkButton *button,GdkEventButton *event,gpointer unused)
{
  if (plugin_website_uri != 0) {
    purple_notify_uri(0,plugin_website_uri);
    return (!0);
  }
  return 0;
}

static GtkWidget *create_details()
{
  GtkBox *vbox = (GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_vbox_new(0,3))),gtk_box_get_type()));
  GtkSizeGroup *sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  GtkWidget *label;
  GtkWidget *view;
  GtkWidget *website_button;
  plugin_name = ((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_label_new(0))),gtk_label_get_type())));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)plugin_name),gtk_misc_get_type()))),0,0);
  gtk_label_set_line_wrap(plugin_name,0);
  gtk_label_set_selectable(plugin_name,(!0));
  gtk_box_pack_start(vbox,((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)plugin_name),gtk_widget_get_type()))),0,0,0);
  view = gtk_text_view_new();
  plugin_desc = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)view),gtk_text_view_get_type()))));
  g_object_set(view,"wrap-mode",GTK_WRAP_WORD,"editable",0,"left-margin",18,"right-margin",18,((void *)((void *)0)));
  gtk_box_pack_start(vbox,view,(!0),(!0),0);
  plugin_error = ((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_label_new(0))),gtk_label_get_type())));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)plugin_error),gtk_misc_get_type()))),0,0);
  gtk_label_set_line_wrap(plugin_error,0);
  gtk_label_set_selectable(plugin_error,(!0));
  gtk_box_pack_start(vbox,((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)plugin_error),gtk_widget_get_type()))),0,0,0);
  plugin_author = ((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_label_new(0))),gtk_label_get_type())));
  gtk_label_set_line_wrap(plugin_author,0);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)plugin_author),gtk_misc_get_type()))),0,0);
  gtk_label_set_selectable(plugin_author,(!0));
  pidgin_add_widget_to_vbox(vbox,"",sg,((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)plugin_author),gtk_widget_get_type()))),(!0),&label);
  gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),((const char *)(dgettext("pidgin","<b>Written by:</b>"))));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0);
  website_button = gtk_event_box_new();
  gtk_event_box_set_visible_window(((GtkEventBox *)(g_type_check_instance_cast(((GTypeInstance *)website_button),gtk_event_box_get_type()))),0);
  plugin_website = ((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_label_new(0))),gtk_label_get_type())));
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)plugin_website),((GType )(20 << 2))))),"ellipsize",PANGO_ELLIPSIZE_MIDDLE,((void *)((void *)0)));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)plugin_website),gtk_misc_get_type()))),0,0);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)website_button),gtk_container_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)plugin_website),gtk_widget_get_type()))));
  g_signal_connect_data(website_button,"button-release-event",((GCallback )website_button_clicked_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(website_button,"enter-notify-event",((GCallback )website_button_motion_cb),0,0,((GConnectFlags )0));
  g_signal_connect_data(website_button,"leave-notify-event",((GCallback )pidgin_clear_cursor),0,0,((GConnectFlags )0));
  pidgin_add_widget_to_vbox(vbox,"",sg,website_button,(!0),&label);
  gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),((const char *)(dgettext("pidgin","<b>Web site:</b>"))));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  plugin_filename = ((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_label_new(0))),gtk_label_get_type())));
  gtk_label_set_line_wrap(plugin_filename,0);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)plugin_filename),gtk_misc_get_type()))),0,0);
  gtk_label_set_selectable(plugin_filename,(!0));
  pidgin_add_widget_to_vbox(vbox,"",sg,((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)plugin_filename),gtk_widget_get_type()))),(!0),&label);
  gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),((const char *)(dgettext("pidgin","<b>Filename:</b>"))));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0);
  g_object_unref(sg);
  return (GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_widget_get_type()));
}

void pidgin_plugin_dialog_show()
{
  GtkWidget *event_view;
  GtkListStore *ls;
  GtkCellRenderer *rend;
  GtkCellRenderer *rendt;
  GtkTreeViewColumn *col;
  GtkTreeSelection *sel;
  if (plugin_dialog != ((GtkWidget *)((void *)0))) {
    gtk_window_present(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)plugin_dialog),gtk_window_get_type()))));
    return ;
  }
  plugin_dialog = gtk_dialog_new_with_buttons(((const char *)(dgettext("pidgin","Plugins"))),0,GTK_DIALOG_NO_SEPARATOR,0);
  pref_button = gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)plugin_dialog),gtk_dialog_get_type()))),((const char *)(dgettext("pidgin","Configure Pl_ugin"))),98121);
  gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)plugin_dialog),gtk_dialog_get_type()))),"gtk-close",GTK_RESPONSE_CLOSE);
  gtk_widget_set_sensitive(pref_button,0);
  gtk_window_set_role(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)plugin_dialog),gtk_window_get_type()))),"plugins");
  ls = gtk_list_store_new(4,((GType )(5 << 2)),((GType )(16 << 2)),((GType )(17 << 2)),((GType )(5 << 2)));
  gtk_tree_sortable_set_sort_column_id(((GtkTreeSortable *)(g_type_check_instance_cast(((GTypeInstance *)ls),gtk_tree_sortable_get_type()))),1,GTK_SORT_ASCENDING);
  update_plugin_list(ls);
  event_view = gtk_tree_view_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)ls),gtk_tree_model_get_type()))));
  gtk_tree_view_set_rules_hint(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)event_view),gtk_tree_view_get_type()))),(!0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)event_view),((GType )(20 << 2))))),"row-activated",((GCallback )show_plugin_prefs_cb),plugin_dialog,0,((GConnectFlags )0));
  purple_signal_connect(purple_plugins_get_handle(),"plugin-load",plugin_dialog,((PurpleCallback )plugin_load_cb),event_view);
  purple_signal_connect(purple_plugins_get_handle(),"plugin-unload",plugin_dialog,((PurpleCallback )plugin_unload_cb),event_view);
  rend = gtk_cell_renderer_toggle_new();
  sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)event_view),gtk_tree_view_get_type()))));
  col = gtk_tree_view_column_new_with_attributes(((const char *)(dgettext("pidgin","Enabled"))),rend,"active",0,((void *)((void *)0)));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)event_view),gtk_tree_view_get_type()))),col);
  gtk_tree_view_column_set_sort_column_id(col,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)rend),((GType )(20 << 2))))),"toggled",((GCallback )plugin_toggled),ls,0,((GConnectFlags )0));
  rendt = gtk_cell_renderer_text_new();
  g_object_set(rendt,"foreground","#c0c0c0",((void *)((void *)0)));
  col = gtk_tree_view_column_new_with_attributes(((const char *)(dgettext("pidgin","Name"))),rendt,"markup",1,"foreground-set",3,((void *)((void *)0)));
  gtk_tree_view_column_set_expand(col,(!0));
  g_object_set(rendt,"ellipsize",PANGO_ELLIPSIZE_END,((void *)((void *)0)));
  gtk_tree_view_append_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)event_view),gtk_tree_view_get_type()))),col);
  gtk_tree_view_column_set_sort_column_id(col,1);
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ls),((GType )(20 << 2))))));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)plugin_dialog),gtk_dialog_get_type())))).vbox),gtk_box_get_type()))),pidgin_make_scrollable(event_view,GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC,GTK_SHADOW_IN,-1,-1),(!0),(!0),0);
  gtk_tree_view_set_search_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)event_view),gtk_tree_view_get_type()))),1);
  gtk_tree_view_set_search_equal_func(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)event_view),gtk_tree_view_get_type()))),pidgin_tree_view_search_equal_func,0,0);
  pidgin_tooltip_setup_for_treeview(event_view,event_view,pidgin_plugins_create_tooltip,pidgin_plugins_paint_tooltip);
  expander = gtk_expander_new(((const char *)(dgettext("pidgin","<b>Plugin Details</b>"))));
  gtk_expander_set_use_markup(((GtkExpander *)(g_type_check_instance_cast(((GTypeInstance *)expander),gtk_expander_get_type()))),(!0));
  gtk_widget_set_sensitive(expander,0);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)expander),gtk_container_get_type()))),create_details());
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)plugin_dialog),gtk_dialog_get_type())))).vbox),gtk_box_get_type()))),expander,0,0,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)sel),((GType )(20 << 2))))),"changed",((GCallback )prefs_plugin_sel),0,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)plugin_dialog),((GType )(20 << 2))))),"response",((GCallback )plugin_dialog_response_cb),sel,0,((GConnectFlags )0));
  gtk_window_set_default_size(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)plugin_dialog),gtk_window_get_type()))),430,530);
  pidgin_auto_parent_window(plugin_dialog);
  gtk_widget_show_all(plugin_dialog);
}
