/**
 * @file media.c Account API
 * @ingroup core
 *
 * Pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "debug.h"
#include "connection.h"
#include "media.h"
#include "mediamanager.h"
#include "pidgin.h"
#include "request.h"
#include "gtkmedia.h"
#include "gtkutils.h"
#include "pidginstock.h"
#ifdef USE_VV
#include "media-gst.h"
#ifdef _WIN32
#include <gdk/gdkwin32.h>
#endif
#include <gst/interfaces/xoverlay.h>
#define PIDGIN_TYPE_MEDIA            (pidgin_media_get_type())
#define PIDGIN_MEDIA(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), PIDGIN_TYPE_MEDIA, PidginMedia))
#define PIDGIN_MEDIA_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass), PIDGIN_TYPE_MEDIA, PidginMediaClass))
#define PIDGIN_IS_MEDIA(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), PIDGIN_TYPE_MEDIA))
#define PIDGIN_IS_MEDIA_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), PIDGIN_TYPE_MEDIA))
#define PIDGIN_MEDIA_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), PIDGIN_TYPE_MEDIA, PidginMediaClass))
typedef struct _PidginMedia PidginMedia;
typedef struct _PidginMediaClass PidginMediaClass;
typedef struct _PidginMediaPrivate PidginMediaPrivate;
typedef enum __unnamed_enum___F0_L58_C9_PIDGIN_MEDIA_WAITING__COMMA__PIDGIN_MEDIA_REQUESTED__COMMA__PIDGIN_MEDIA_ACCEPTED__COMMA__PIDGIN_MEDIA_REJECTED {
/* Waiting for response */
PIDGIN_MEDIA_WAITING=1,
/* Got request */
PIDGIN_MEDIA_REQUESTED,
/* Accepted call */
PIDGIN_MEDIA_ACCEPTED,
/* Rejected call */
PIDGIN_MEDIA_REJECTED}PidginMediaState;

struct _PidginMediaClass 
{
  GtkWindowClass parent_class;
}
;

struct _PidginMedia 
{
  GtkWindow parent;
  PidginMediaPrivate *priv;
}
;

struct _PidginMediaPrivate 
{
  PurpleMedia *media;
  gchar *screenname;
  gulong level_handler_id;
  GtkUIManager *ui;
  GtkWidget *menubar;
  GtkWidget *statusbar;
  GtkWidget *hold;
  GtkWidget *mute;
  GtkWidget *pause;
  GtkWidget *send_progress;
  GHashTable *recv_progressbars;
  PidginMediaState state;
  GtkWidget *display;
  GtkWidget *send_widget;
  GtkWidget *recv_widget;
  GtkWidget *button_widget;
  GtkWidget *local_video;
  GHashTable *remote_videos;
  guint timeout_id;
  PurpleMediaSessionType request_type;
}
;
#define PIDGIN_MEDIA_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE((obj), PIDGIN_TYPE_MEDIA, PidginMediaPrivate))
static void pidgin_media_class_init(PidginMediaClass *klass);
static void pidgin_media_init(PidginMedia *media);
static void pidgin_media_dispose(GObject *object);
static void pidgin_media_finalize(GObject *object);
static void pidgin_media_get_property(GObject *object,guint prop_id,GValue *value,GParamSpec *pspec);
static void pidgin_media_set_property(GObject *object,guint prop_id,const GValue *value,GParamSpec *pspec);
static void pidgin_media_set_state(PidginMedia *gtkmedia,PidginMediaState state);
static GtkWindowClass *parent_class = (GtkWindowClass *)((void *)0);
#if 0
#endif
enum __unnamed_enum___F0_L131_C1_PROP_0__COMMA__PROP_MEDIA__COMMA__PROP_SCREENNAME {PROP_0,PROP_MEDIA,PROP_SCREENNAME};

static GType pidgin_media_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(PidginMediaClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )pidgin_media_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(PidginMedia ))), (0), ((GInstanceInitFunc )pidgin_media_init), ((const GTypeValueTable *)((void *)0))};
    type = g_type_register_static(gtk_window_get_type(),"PidginMedia",&info,0);
  }
  return type;
}

static void pidgin_media_class_init(PidginMediaClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *)klass;
/*	GtkContainerClass *container_class = (GtkContainerClass*)klass; */
  parent_class = (g_type_class_peek_parent(klass));
  gobject_class -> dispose = pidgin_media_dispose;
  gobject_class -> finalize = pidgin_media_finalize;
  gobject_class -> set_property = pidgin_media_set_property;
  gobject_class -> get_property = pidgin_media_get_property;
  g_object_class_install_property(gobject_class,PROP_MEDIA,g_param_spec_object("media","PurpleMedia","The PurpleMedia associated with this media.",purple_media_get_type(),(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_class_install_property(gobject_class,PROP_SCREENNAME,g_param_spec_string("screenname","Screenname","The screenname of the user this session is with.",0,(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_type_class_add_private(klass,(sizeof(PidginMediaPrivate )));
}

static void pidgin_media_hold_toggled(GtkToggleButton *toggle,PidginMedia *media)
{
  purple_media_stream_info(( *(media -> priv)).media,((gtk_toggle_button_get_active(toggle) != 0)?PURPLE_MEDIA_INFO_HOLD : PURPLE_MEDIA_INFO_UNHOLD),0,0,(!0));
}

static void pidgin_media_mute_toggled(GtkToggleButton *toggle,PidginMedia *media)
{
  purple_media_stream_info(( *(media -> priv)).media,((gtk_toggle_button_get_active(toggle) != 0)?PURPLE_MEDIA_INFO_MUTE : PURPLE_MEDIA_INFO_UNMUTE),0,0,(!0));
}

static void pidgin_media_pause_toggled(GtkToggleButton *toggle,PidginMedia *media)
{
  purple_media_stream_info(( *(media -> priv)).media,((gtk_toggle_button_get_active(toggle) != 0)?PURPLE_MEDIA_INFO_PAUSE : PURPLE_MEDIA_INFO_UNPAUSE),0,0,(!0));
}

static gboolean pidgin_media_delete_event_cb(GtkWidget *widget,GdkEvent *event,PidginMedia *media)
{
  if (( *(media -> priv)).media != 0) 
    purple_media_stream_info(( *(media -> priv)).media,PURPLE_MEDIA_INFO_HANGUP,0,0,(!0));
  return 0;
}
#ifdef HAVE_X11

static int pidgin_x_error_handler(Display *display,XErrorEvent *event)
{
  const gchar *error_type;
  switch((event -> error_code)){
#define XERRORCASE(type) case type: error_type = #type; break
    case 10:
{
      error_type = "BadAccess";
      break; 
    }
    case 11:
{
      error_type = "BadAlloc";
      break; 
    }
    case 5:
{
      error_type = "BadAtom";
      break; 
    }
    case 12:
{
      error_type = "BadColor";
      break; 
    }
    case 6:
{
      error_type = "BadCursor";
      break; 
    }
    case 9:
{
      error_type = "BadDrawable";
      break; 
    }
    case 7:
{
      error_type = "BadFont";
      break; 
    }
    case 13:
{
      error_type = "BadGC";
      break; 
    }
    case 14:
{
      error_type = "BadIDChoice";
      break; 
    }
    case 17:
{
      error_type = "BadImplementation";
      break; 
    }
    case 16:
{
      error_type = "BadLength";
      break; 
    }
    case 8:
{
      error_type = "BadMatch";
      break; 
    }
    case 15:
{
      error_type = "BadName";
      break; 
    }
    case 4:
{
      error_type = "BadPixmap";
      break; 
    }
    case 1:
{
      error_type = "BadRequest";
      break; 
    }
    case 2:
{
      error_type = "BadValue";
      break; 
    }
    case 3:
{
      error_type = "BadWindow";
      break; 
    }
#undef XERRORCASE
    default:
{
      error_type = "unknown";
      break; 
    }
  }
  purple_debug_error("media","A %s Xlib error has occurred. The program would normally crash now.\n",error_type);
  return 0;
}
#endif

static void menu_hangup(GtkAction *action,gpointer data)
{
  PidginMedia *gtkmedia = (PidginMedia *)(g_type_check_instance_cast(((GTypeInstance *)data),pidgin_media_get_type()));
  purple_media_stream_info(( *(gtkmedia -> priv)).media,PURPLE_MEDIA_INFO_HANGUP,0,0,(!0));
}
static const GtkActionEntry menu_entries[] = {{("MediaMenu"), ((const gchar *)((void *)0)), ("_Media"), ((const gchar *)((void *)0)), ((const gchar *)((void *)0)), ((GCallback )((void *)0))}, {("Hangup"), ((const gchar *)((void *)0)), ("_Hangup"), ((const gchar *)((void *)0)), ((const gchar *)((void *)0)), ((GCallback )menu_hangup)}};
static const char *media_menu = "<ui><menubar name=\'Media\'><menu action=\'MediaMenu\'><menuitem action=\'Hangup\'/></menu></menubar></ui>";

static GtkWidget *setup_menubar(PidginMedia *window)
{
  GtkActionGroup *action_group;
  GError *error;
  GtkAccelGroup *accel_group;
  GtkWidget *menu;
  action_group = gtk_action_group_new("MediaActions");
#ifdef ENABLE_NLS
  gtk_action_group_set_translation_domain(action_group,"pidgin");
#endif
  gtk_action_group_add_actions(action_group,menu_entries,(sizeof(menu_entries) / sizeof(menu_entries[0])),((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_window_get_type()))));
  ( *(window -> priv)).ui = gtk_ui_manager_new();
  gtk_ui_manager_insert_action_group(( *(window -> priv)).ui,action_group,0);
  accel_group = gtk_ui_manager_get_accel_group(( *(window -> priv)).ui);
  gtk_window_add_accel_group(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_window_get_type()))),accel_group);
  error = ((GError *)((void *)0));
  if (!(gtk_ui_manager_add_ui_from_string(( *(window -> priv)).ui,media_menu,(-1),&error) != 0U)) {
    g_log(0,G_LOG_LEVEL_MESSAGE,"building menus failed: %s",(error -> message));
    g_error_free(error);
    exit(1);
  }
  menu = gtk_ui_manager_get_widget(( *(window -> priv)).ui,"/Media");
  gtk_widget_show(menu);
  return menu;
}

static void pidgin_media_init(PidginMedia *media)
{
  GtkWidget *vbox;
  media -> priv = ((PidginMediaPrivate *)(g_type_instance_get_private(((GTypeInstance *)media),pidgin_media_get_type())));
#ifdef HAVE_X11
  XSetErrorHandler(pidgin_x_error_handler);
#endif
  vbox = gtk_vbox_new(0,0);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)media),gtk_container_get_type()))),vbox);
  ( *(media -> priv)).statusbar = gtk_statusbar_new();
  gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),( *(media -> priv)).statusbar,0,0,0);
  gtk_statusbar_push(((GtkStatusbar *)(g_type_check_instance_cast(((GTypeInstance *)( *(media -> priv)).statusbar),gtk_statusbar_get_type()))),0,((const char *)(dgettext("pidgin","Calling..."))));
  gtk_widget_show(( *(media -> priv)).statusbar);
  ( *(media -> priv)).menubar = setup_menubar(media);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),( *(media -> priv)).menubar,0,(!0),0);
  ( *(media -> priv)).display = gtk_hbox_new(0,6);
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *(media -> priv)).display),gtk_container_get_type()))),6);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),( *(media -> priv)).display,(!0),(!0),6);
  gtk_widget_show(vbox);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)media),((GType )(20 << 2))))),"delete-event",((GCallback )pidgin_media_delete_event_cb),media,0,((GConnectFlags )0));
  ( *(media -> priv)).recv_progressbars = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,0);
  ( *(media -> priv)).remote_videos = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,0);
}

static gchar *create_key(const gchar *session_id,const gchar *participant)
{
  return g_strdup_printf("%s_%s",session_id,participant);
}

static void pidgin_media_insert_widget(PidginMedia *gtkmedia,GtkWidget *widget,const gchar *session_id,const gchar *participant)
{
  gchar *key = create_key(session_id,participant);
  PurpleMediaSessionType type = purple_media_get_session_type(( *(gtkmedia -> priv)).media,session_id);
  if ((type & PURPLE_MEDIA_AUDIO) != 0U) 
    g_hash_table_insert(( *(gtkmedia -> priv)).recv_progressbars,key,widget);
  else if ((type & PURPLE_MEDIA_VIDEO) != 0U) 
    g_hash_table_insert(( *(gtkmedia -> priv)).remote_videos,key,widget);
}

static GtkWidget *pidgin_media_get_widget(PidginMedia *gtkmedia,const gchar *session_id,const gchar *participant)
{
  GtkWidget *widget = (GtkWidget *)((void *)0);
  gchar *key = create_key(session_id,participant);
  PurpleMediaSessionType type = purple_media_get_session_type(( *(gtkmedia -> priv)).media,session_id);
  if ((type & PURPLE_MEDIA_AUDIO) != 0U) 
    widget = (g_hash_table_lookup(( *(gtkmedia -> priv)).recv_progressbars,key));
  else if ((type & PURPLE_MEDIA_VIDEO) != 0U) 
    widget = (g_hash_table_lookup(( *(gtkmedia -> priv)).remote_videos,key));
  g_free(key);
  return widget;
}

static void pidgin_media_remove_widget(PidginMedia *gtkmedia,const gchar *session_id,const gchar *participant)
{
  GtkWidget *widget = pidgin_media_get_widget(gtkmedia,session_id,participant);
  if (widget != 0) {
    PurpleMediaSessionType type = purple_media_get_session_type(( *(gtkmedia -> priv)).media,session_id);
    gchar *key = create_key(session_id,participant);
    GtkRequisition req;
    if ((type & PURPLE_MEDIA_AUDIO) != 0U) {
      g_hash_table_remove(( *(gtkmedia -> priv)).recv_progressbars,key);
      if ((g_hash_table_size(( *(gtkmedia -> priv)).recv_progressbars) == 0) && (( *(gtkmedia -> priv)).send_progress != 0)) {
        gtk_widget_destroy(( *(gtkmedia -> priv)).send_progress);
        ( *(gtkmedia -> priv)).send_progress = ((GtkWidget *)((void *)0));
        gtk_widget_destroy(( *(gtkmedia -> priv)).mute);
        ( *(gtkmedia -> priv)).mute = ((GtkWidget *)((void *)0));
      }
    }
    else if ((type & PURPLE_MEDIA_VIDEO) != 0U) {
      g_hash_table_remove(( *(gtkmedia -> priv)).remote_videos,key);
      if ((g_hash_table_size(( *(gtkmedia -> priv)).remote_videos) == 0) && (( *(gtkmedia -> priv)).local_video != 0)) {
        gtk_widget_destroy(( *(gtkmedia -> priv)).local_video);
        ( *(gtkmedia -> priv)).local_video = ((GtkWidget *)((void *)0));
        gtk_widget_destroy(( *(gtkmedia -> priv)).pause);
        ( *(gtkmedia -> priv)).pause = ((GtkWidget *)((void *)0));
      }
    }
    g_free(key);
    gtk_widget_destroy(widget);
    gtk_widget_size_request(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)gtkmedia),gtk_widget_get_type()))),&req);
    gtk_window_resize(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)gtkmedia),gtk_window_get_type()))),req.width,req.height);
  }
}

static void level_message_cb(PurpleMedia *media,gchar *session_id,gchar *participant,double level,PidginMedia *gtkmedia)
{
  GtkWidget *progress = (GtkWidget *)((void *)0);
  if (participant == ((gchar *)((void *)0))) 
    progress = ( *(gtkmedia -> priv)).send_progress;
  else 
    progress = pidgin_media_get_widget(gtkmedia,session_id,participant);
  if (progress != 0) 
    gtk_progress_bar_set_fraction(((GtkProgressBar *)(g_type_check_instance_cast(((GTypeInstance *)progress),gtk_progress_bar_get_type()))),(level * 5));
}

static void pidgin_media_disconnect_levels(PurpleMedia *media,PidginMedia *gtkmedia)
{
  PurpleMediaManager *manager = purple_media_get_manager(media);
  GstElement *element = purple_media_manager_get_pipeline(manager);
  gulong handler_id = g_signal_handler_find(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gst_pipeline_get_bus(((GstPipeline *)(g_type_check_instance_cast(((GTypeInstance *)element),gst_pipeline_get_type())))))),((GType )(20 << 2))))),(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA),0,0,0,((GCallback )level_message_cb),gtkmedia);
  if (handler_id != 0UL) 
    g_signal_handler_disconnect(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gst_pipeline_get_bus(((GstPipeline *)(g_type_check_instance_cast(((GTypeInstance *)element),gst_pipeline_get_type())))))),((GType )(20 << 2))))),handler_id);
}

static void pidgin_media_dispose(GObject *media)
{
  PidginMedia *gtkmedia = (PidginMedia *)(g_type_check_instance_cast(((GTypeInstance *)media),pidgin_media_get_type()));
  purple_debug_info("gtkmedia","pidgin_media_dispose\n");
  if (( *(gtkmedia -> priv)).media != 0) {
    purple_request_close_with_handle(gtkmedia);
    purple_media_remove_output_windows(( *(gtkmedia -> priv)).media);
    pidgin_media_disconnect_levels(( *(gtkmedia -> priv)).media,gtkmedia);
    g_object_unref(( *(gtkmedia -> priv)).media);
    ( *(gtkmedia -> priv)).media = ((PurpleMedia *)((void *)0));
  }
  if (( *(gtkmedia -> priv)).ui != 0) {
    g_object_unref(( *(gtkmedia -> priv)).ui);
    ( *(gtkmedia -> priv)).ui = ((GtkUIManager *)((void *)0));
  }
  if (( *(gtkmedia -> priv)).timeout_id != 0) 
    g_source_remove(( *(gtkmedia -> priv)).timeout_id);
  if (( *(gtkmedia -> priv)).recv_progressbars != 0) {
    g_hash_table_destroy(( *(gtkmedia -> priv)).recv_progressbars);
    g_hash_table_destroy(( *(gtkmedia -> priv)).remote_videos);
    ( *(gtkmedia -> priv)).recv_progressbars = ((GHashTable *)((void *)0));
    ( *(gtkmedia -> priv)).remote_videos = ((GHashTable *)((void *)0));
  }
  ( *( *((GObjectClass *)(g_type_check_class_cast(((GTypeClass *)parent_class),((GType )(20 << 2)))))).dispose)(media);
}

static void pidgin_media_finalize(GObject *media)
{
/* PidginMedia *gtkmedia = PIDGIN_MEDIA(media); */
  purple_debug_info("gtkmedia","pidgin_media_finalize\n");
  ( *( *((GObjectClass *)(g_type_check_class_cast(((GTypeClass *)parent_class),((GType )(20 << 2)))))).finalize)(media);
}

static void pidgin_media_emit_message(PidginMedia *gtkmedia,const char *msg)
{
  PurpleConversation *conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_ANY,( *(gtkmedia -> priv)).screenname,(purple_media_get_account(( *(gtkmedia -> priv)).media)));
  if (conv != ((PurpleConversation *)((void *)0))) 
    purple_conversation_write(conv,0,msg,PURPLE_MESSAGE_SYSTEM,time(0));
}
typedef struct __unnamed_class___F0_L527_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L1909R__Pe___variable_name_unknown_scope_and_name__scope__gtkmedia__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__session_id__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__participant {
PidginMedia *gtkmedia;
gchar *session_id;
gchar *participant;}PidginMediaRealizeData;

static gboolean realize_cb_cb(PidginMediaRealizeData *data)
{
  PidginMediaPrivate *priv = ( *(data -> gtkmedia)).priv;
  GdkWindow *window = (GdkWindow *)((void *)0);
  if ((data -> participant) == ((gchar *)((void *)0))) 
#if GTK_CHECK_VERSION(2, 14, 0)
    window = gtk_widget_get_window((priv -> local_video));
  else 
#else
#endif
{
    GtkWidget *widget = pidgin_media_get_widget((data -> gtkmedia),(data -> session_id),(data -> participant));
    if (widget != 0) 
#if GTK_CHECK_VERSION(2, 14, 0)
      window = gtk_widget_get_window(widget);
#else
#endif
  }
  if (window != 0) {
    gulong window_id;
#ifdef _WIN32
#elif defined(HAVE_X11)
    window_id = gdk_x11_drawable_get_xid(window);
#else
#		error "Unsupported windowing system"
#endif
    purple_media_set_output_window((priv -> media),(data -> session_id),(data -> participant),window_id);
  }
  g_free((data -> session_id));
  g_free((data -> participant));
  g_free(data);
  return 0;
}

static void realize_cb(GtkWidget *widget,PidginMediaRealizeData *data)
{
  g_timeout_add(0,((GSourceFunc )realize_cb_cb),data);
}

static void pidgin_media_error_cb(PidginMedia *media,const char *error,PidginMedia *gtkmedia)
{
  PurpleConversation *conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_ANY,( *(gtkmedia -> priv)).screenname,(purple_media_get_account(( *(gtkmedia -> priv)).media)));
  if (conv != ((PurpleConversation *)((void *)0))) 
    purple_conversation_write(conv,0,error,PURPLE_MESSAGE_ERROR,time(0));
  gtk_statusbar_push(((GtkStatusbar *)(g_type_check_instance_cast(((GTypeInstance *)( *(gtkmedia -> priv)).statusbar),gtk_statusbar_get_type()))),0,error);
}

static void pidgin_media_accept_cb(PurpleMedia *media,int index)
{
  purple_media_stream_info(media,PURPLE_MEDIA_INFO_ACCEPT,0,0,(!0));
}

static void pidgin_media_reject_cb(PurpleMedia *media,int index)
{
  GList *iter = purple_media_get_session_ids(media);
  for (; iter != 0; iter = g_list_delete_link(iter,iter)) {
    const gchar *sessionid = (iter -> data);
    if (!(purple_media_accepted(media,sessionid,0) != 0)) 
      purple_media_stream_info(media,PURPLE_MEDIA_INFO_REJECT,sessionid,0,(!0));
  }
}

static gboolean pidgin_request_timeout_cb(PidginMedia *gtkmedia)
{
  PurpleAccount *account;
  PurpleBuddy *buddy;
  const gchar *alias;
  PurpleMediaSessionType type;
  gchar *message = (gchar *)((void *)0);
  account = purple_media_get_account(( *(gtkmedia -> priv)).media);
  buddy = purple_find_buddy(account,( *(gtkmedia -> priv)).screenname);
  alias = ((buddy != 0)?purple_buddy_get_contact_alias(buddy) : ( *(gtkmedia -> priv)).screenname);
  type = ( *(gtkmedia -> priv)).request_type;
  ( *(gtkmedia -> priv)).timeout_id = 0;
  if (((type & PURPLE_MEDIA_AUDIO) != 0U) && ((type & PURPLE_MEDIA_VIDEO) != 0U)) {
    message = g_strdup_printf(((const char *)(dgettext("pidgin","%s wishes to start an audio/video session with you."))),alias);
  }
  else if ((type & PURPLE_MEDIA_AUDIO) != 0U) {
    message = g_strdup_printf(((const char *)(dgettext("pidgin","%s wishes to start an audio session with you."))),alias);
  }
  else if ((type & PURPLE_MEDIA_VIDEO) != 0U) {
    message = g_strdup_printf(((const char *)(dgettext("pidgin","%s wishes to start a video session with you."))),alias);
  }
  ( *(gtkmedia -> priv)).request_type = PURPLE_MEDIA_NONE;
  if (!(purple_media_accepted(( *(gtkmedia -> priv)).media,0,0) != 0)) {
    purple_request_action(gtkmedia,((const char *)(dgettext("pidgin","Incoming Call"))),message,0,-1,((void *)account),( *(gtkmedia -> priv)).screenname,0,( *(gtkmedia -> priv)).media,2,((const char *)(dgettext("pidgin","_Accept"))),pidgin_media_accept_cb,((const char *)(dgettext("pidgin","_Cancel"))),pidgin_media_reject_cb);
  }
  pidgin_media_emit_message(gtkmedia,message);
  g_free(message);
  return 0;
}

static void pidgin_media_input_volume_changed(
#if GTK_CHECK_VERSION(2,12,0)
GtkScaleButton *range,double value,PurpleMedia *media)
{
  double val = (((double )value) * 100.0);
#else
#endif
  purple_media_set_input_volume(media,0,val);
}

static void pidgin_media_output_volume_changed(
#if GTK_CHECK_VERSION(2,12,0)
GtkScaleButton *range,double value,PurpleMedia *media)
{
  double val = (((double )value) * 100.0);
#else
#endif
  purple_media_set_output_volume(media,0,0,val);
}

static void destroy_parent_widget_cb(GtkWidget *widget,GtkWidget *parent)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)parent;
      GType __t = gtk_widget_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_WIDGET(parent)");
      return ;
    };
  }while (0);
  gtk_widget_destroy(parent);
}

static GtkWidget *pidgin_media_add_audio_widget(PidginMedia *gtkmedia,PurpleMediaSessionType type,const gchar *sid)
{
  GtkWidget *volume_widget;
  GtkWidget *progress_parent;
  GtkWidget *volume;
  GtkWidget *progress;
  double value;
  if ((type & PURPLE_MEDIA_SEND_AUDIO) != 0U) {
    value = (purple_prefs_get_int("/purple/media/audio/volume/input"));
  }
  else if ((type & PURPLE_MEDIA_RECV_AUDIO) != 0U) {
    value = (purple_prefs_get_int("/purple/media/audio/volume/output"));
  }
  else 
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","gtkmedia.c",706,((const char *)__func__));
      return 0;
    }while (0);
#if GTK_CHECK_VERSION(2,12,0)
/* Setup widget structure */
  volume_widget = gtk_hbox_new(0,6);
  progress_parent = gtk_vbox_new(0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)volume_widget),gtk_box_get_type()))),progress_parent,(!0),(!0),0);
/* Volume button */
  volume = gtk_volume_button_new();
  gtk_scale_button_set_value(((GtkScaleButton *)(g_type_check_instance_cast(((GTypeInstance *)volume),gtk_scale_button_get_type()))),(value / 100.0));
  gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)volume_widget),gtk_box_get_type()))),volume,0,0,0);
#else
/* Setup widget structure */
/* Volume slider */
#endif
/* Volume level indicator */
  progress = gtk_progress_bar_new();
  gtk_widget_set_size_request(progress,250,10);
  gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)progress_parent),gtk_box_get_type()))),progress,(!0),0,0);
  if ((type & PURPLE_MEDIA_SEND_AUDIO) != 0U) {
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)volume),((GType )(20 << 2))))),"value-changed",((GCallback )pidgin_media_input_volume_changed),( *(gtkmedia -> priv)).media,0,((GConnectFlags )0));
    ( *(gtkmedia -> priv)).send_progress = progress;
  }
  else if ((type & PURPLE_MEDIA_RECV_AUDIO) != 0U) {
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)volume),((GType )(20 << 2))))),"value-changed",((GCallback )pidgin_media_output_volume_changed),( *(gtkmedia -> priv)).media,0,((GConnectFlags )0));
    pidgin_media_insert_widget(gtkmedia,progress,sid,( *(gtkmedia -> priv)).screenname);
  }
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)progress),((GType )(20 << 2))))),"destroy",((GCallback )destroy_parent_widget_cb),volume_widget,0,((GConnectFlags )0));
  gtk_widget_show_all(volume_widget);
  return volume_widget;
}

static void pidgin_media_ready_cb(PurpleMedia *media,PidginMedia *gtkmedia,const gchar *sid)
{
  GtkWidget *send_widget = (GtkWidget *)((void *)0);
  GtkWidget *recv_widget = (GtkWidget *)((void *)0);
  GtkWidget *button_widget = (GtkWidget *)((void *)0);
  PurpleMediaSessionType type = purple_media_get_session_type(media,sid);
  GdkPixbuf *icon = (GdkPixbuf *)((void *)0);
  if ((( *(gtkmedia -> priv)).recv_widget == ((GtkWidget *)((void *)0))) && ((type & (PURPLE_MEDIA_RECV_VIDEO | PURPLE_MEDIA_RECV_AUDIO)) != 0U)) {
    recv_widget = gtk_vbox_new(0,6);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)( *(gtkmedia -> priv)).display),gtk_box_get_type()))),recv_widget,(!0),(!0),0);
    gtk_widget_show(recv_widget);
  }
  else {
    recv_widget = ( *(gtkmedia -> priv)).recv_widget;
  }
  if ((( *(gtkmedia -> priv)).send_widget == ((GtkWidget *)((void *)0))) && ((type & (PURPLE_MEDIA_SEND_VIDEO | PURPLE_MEDIA_SEND_AUDIO)) != 0U)) {
    send_widget = gtk_vbox_new(0,6);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)( *(gtkmedia -> priv)).display),gtk_box_get_type()))),send_widget,0,(!0),0);
    button_widget = gtk_hbox_new(0,6);
    gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)recv_widget),gtk_box_get_type()))),button_widget,0,(!0),0);
    gtk_widget_show(send_widget);
/* Hold button */
    ( *(gtkmedia -> priv)).hold = gtk_toggle_button_new_with_mnemonic(((const char *)(dgettext("pidgin","_Hold"))));
    gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)button_widget),gtk_box_get_type()))),( *(gtkmedia -> priv)).hold,0,0,0);
    gtk_widget_show(( *(gtkmedia -> priv)).hold);
    g_signal_connect_data(( *(gtkmedia -> priv)).hold,"toggled",((GCallback )pidgin_media_hold_toggled),gtkmedia,0,((GConnectFlags )0));
  }
  else {
    send_widget = ( *(gtkmedia -> priv)).send_widget;
    button_widget = ( *(gtkmedia -> priv)).button_widget;
  }
  if ((type & PURPLE_MEDIA_RECV_VIDEO) != 0U) {
    PidginMediaRealizeData *data;
    GtkWidget *aspect;
    GtkWidget *remote_video;
    GdkColor color = {(0), (0), (0), (0)};
    aspect = gtk_aspect_frame_new(0,0,0,(4.0 / 3.0),0);
    gtk_frame_set_shadow_type(((GtkFrame *)(g_type_check_instance_cast(((GTypeInstance *)aspect),gtk_frame_get_type()))),GTK_SHADOW_IN);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)recv_widget),gtk_box_get_type()))),aspect,(!0),(!0),0);
    data = ((PidginMediaRealizeData *)(g_malloc0_n(1,(sizeof(PidginMediaRealizeData )))));
    data -> gtkmedia = gtkmedia;
    data -> session_id = g_strdup(sid);
    data -> participant = g_strdup(( *(gtkmedia -> priv)).screenname);
    remote_video = gtk_drawing_area_new();
    gtk_widget_modify_bg(remote_video,GTK_STATE_NORMAL,(&color));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)remote_video),((GType )(20 << 2))))),"realize",((GCallback )realize_cb),data,0,((GConnectFlags )0));
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)aspect),gtk_container_get_type()))),remote_video);
    gtk_widget_set_size_request(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)remote_video),gtk_widget_get_type()))),320,240);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)remote_video),((GType )(20 << 2))))),"destroy",((GCallback )destroy_parent_widget_cb),aspect,0,((GConnectFlags )0));
    gtk_widget_show(remote_video);
    gtk_widget_show(aspect);
    pidgin_media_insert_widget(gtkmedia,remote_video,(data -> session_id),(data -> participant));
  }
  if (((type & PURPLE_MEDIA_SEND_VIDEO) != 0U) && !(( *(gtkmedia -> priv)).local_video != 0)) {
    PidginMediaRealizeData *data;
    GtkWidget *aspect;
    GtkWidget *local_video;
    GdkColor color = {(0), (0), (0), (0)};
    aspect = gtk_aspect_frame_new(0,0,0,(4.0 / 3.0),(!0));
    gtk_frame_set_shadow_type(((GtkFrame *)(g_type_check_instance_cast(((GTypeInstance *)aspect),gtk_frame_get_type()))),GTK_SHADOW_IN);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)send_widget),gtk_box_get_type()))),aspect,0,(!0),0);
    data = ((PidginMediaRealizeData *)(g_malloc0_n(1,(sizeof(PidginMediaRealizeData )))));
    data -> gtkmedia = gtkmedia;
    data -> session_id = g_strdup(sid);
    data -> participant = ((gchar *)((void *)0));
    local_video = gtk_drawing_area_new();
    gtk_widget_modify_bg(local_video,GTK_STATE_NORMAL,(&color));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)local_video),((GType )(20 << 2))))),"realize",((GCallback )realize_cb),data,0,((GConnectFlags )0));
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)aspect),gtk_container_get_type()))),local_video);
    gtk_widget_set_size_request(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)local_video),gtk_widget_get_type()))),80,60);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)local_video),((GType )(20 << 2))))),"destroy",((GCallback )destroy_parent_widget_cb),aspect,0,((GConnectFlags )0));
    gtk_widget_show(local_video);
    gtk_widget_show(aspect);
    ( *(gtkmedia -> priv)).pause = gtk_toggle_button_new_with_mnemonic(((const char *)(dgettext("pidgin","_Pause"))));
    gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)button_widget),gtk_box_get_type()))),( *(gtkmedia -> priv)).pause,0,0,0);
    gtk_widget_show(( *(gtkmedia -> priv)).pause);
    g_signal_connect_data(( *(gtkmedia -> priv)).pause,"toggled",((GCallback )pidgin_media_pause_toggled),gtkmedia,0,((GConnectFlags )0));
    ( *(gtkmedia -> priv)).local_video = local_video;
  }
  if ((type & PURPLE_MEDIA_RECV_AUDIO) != 0U) {
    gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)recv_widget),gtk_box_get_type()))),pidgin_media_add_audio_widget(gtkmedia,PURPLE_MEDIA_RECV_AUDIO,sid),0,0,0);
  }
  if ((type & PURPLE_MEDIA_SEND_AUDIO) != 0U) {
    ( *(gtkmedia -> priv)).mute = gtk_toggle_button_new_with_mnemonic(((const char *)(dgettext("pidgin","_Mute"))));
    gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)button_widget),gtk_box_get_type()))),( *(gtkmedia -> priv)).mute,0,0,0);
    gtk_widget_show(( *(gtkmedia -> priv)).mute);
    g_signal_connect_data(( *(gtkmedia -> priv)).mute,"toggled",((GCallback )pidgin_media_mute_toggled),gtkmedia,0,((GConnectFlags )0));
    gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)recv_widget),gtk_box_get_type()))),pidgin_media_add_audio_widget(gtkmedia,PURPLE_MEDIA_SEND_AUDIO,0),0,0,0);
  }
  if (((type & PURPLE_MEDIA_AUDIO) != 0U) && (( *(gtkmedia -> priv)).level_handler_id == 0)) {
    ( *(gtkmedia -> priv)).level_handler_id = g_signal_connect_data(media,"level",((GCallback )level_message_cb),gtkmedia,0,((GConnectFlags )0));
  }
  if (send_widget != ((GtkWidget *)((void *)0))) 
    ( *(gtkmedia -> priv)).send_widget = send_widget;
  if (recv_widget != ((GtkWidget *)((void *)0))) 
    ( *(gtkmedia -> priv)).recv_widget = recv_widget;
  if (button_widget != ((GtkWidget *)((void *)0))) {
    ( *(gtkmedia -> priv)).button_widget = button_widget;
    gtk_widget_show(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)button_widget),gtk_widget_get_type()))));
  }
  if (purple_media_is_initiator(media,sid,0) == 0) {
    if (( *(gtkmedia -> priv)).timeout_id != 0) 
      g_source_remove(( *(gtkmedia -> priv)).timeout_id);
    ( *(gtkmedia -> priv)).request_type |= type;
    ( *(gtkmedia -> priv)).timeout_id = g_timeout_add(500,((GSourceFunc )pidgin_request_timeout_cb),gtkmedia);
  }
/* set the window icon according to the type */
  if ((type & PURPLE_MEDIA_VIDEO) != 0U) {
    icon = gtk_widget_render_icon(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)gtkmedia),gtk_widget_get_type()))),"pidgin-video-call",gtk_icon_size_from_name("pidgin-icon-size-tango-large"),0);
  }
  else if ((type & PURPLE_MEDIA_AUDIO) != 0U) {
    icon = gtk_widget_render_icon(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)gtkmedia),gtk_widget_get_type()))),"pidgin-audio-call",gtk_icon_size_from_name("pidgin-icon-size-tango-large"),0);
  }
  if (icon != 0) {
    gtk_window_set_icon(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)gtkmedia),gtk_window_get_type()))),icon);
    g_object_unref(icon);
  }
  gtk_widget_show(( *(gtkmedia -> priv)).display);
}

static void pidgin_media_state_changed_cb(PurpleMedia *media,PurpleMediaState state,gchar *sid,gchar *name,PidginMedia *gtkmedia)
{
  purple_debug_info("gtkmedia","state: %d sid: %s name: %s\n",state,((sid != 0)?sid : "(null)"),((name != 0)?name : "(null)"));
  if (state == PURPLE_MEDIA_STATE_END) {
    if ((sid != ((gchar *)((void *)0))) && (name != ((gchar *)((void *)0)))) {
      pidgin_media_remove_widget(gtkmedia,sid,name);
    }
    else if ((sid == ((gchar *)((void *)0))) && (name == ((gchar *)((void *)0)))) {
      pidgin_media_emit_message(gtkmedia,((const char *)(dgettext("pidgin","The call has been terminated."))));
      gtk_widget_destroy(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)gtkmedia),gtk_widget_get_type()))));
    }
  }
  else if (((state == PURPLE_MEDIA_STATE_NEW) && (sid != ((gchar *)((void *)0)))) && (name != ((gchar *)((void *)0)))) {
    pidgin_media_ready_cb(media,gtkmedia,sid);
  }
}

static void pidgin_media_stream_info_cb(PurpleMedia *media,PurpleMediaInfoType type,gchar *sid,gchar *name,gboolean local,PidginMedia *gtkmedia)
{
  if (type == PURPLE_MEDIA_INFO_REJECT) {
    pidgin_media_emit_message(gtkmedia,((const char *)(dgettext("pidgin","You have rejected the call."))));
  }
  else if (type == PURPLE_MEDIA_INFO_ACCEPT) {
    if (local == !0) 
      purple_request_close_with_handle(gtkmedia);
    pidgin_media_set_state(gtkmedia,PIDGIN_MEDIA_ACCEPTED);
    pidgin_media_emit_message(gtkmedia,((const char *)(dgettext("pidgin","Call in progress."))));
    gtk_statusbar_push(((GtkStatusbar *)(g_type_check_instance_cast(((GTypeInstance *)( *(gtkmedia -> priv)).statusbar),gtk_statusbar_get_type()))),0,((const char *)(dgettext("pidgin","Call in progress."))));
    gtk_widget_show(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)gtkmedia),gtk_widget_get_type()))));
  }
}

static void pidgin_media_set_property(GObject *object,guint prop_id,const GValue *value,GParamSpec *pspec)
{
  PidginMedia *media;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = pidgin_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_MEDIA(object)");
      return ;
    };
  }while (0);
  media = ((PidginMedia *)(g_type_check_instance_cast(((GTypeInstance *)object),pidgin_media_get_type())));
{
    switch(prop_id){
      case PROP_MEDIA:
{
{
          if (( *(media -> priv)).media != 0) 
            g_object_unref(( *(media -> priv)).media);
          ( *(media -> priv)).media = (g_value_get_object(value));
          g_object_ref(( *(media -> priv)).media);
          if (purple_media_is_initiator(( *(media -> priv)).media,0,0) == !0) 
            pidgin_media_set_state(media,PIDGIN_MEDIA_WAITING);
          else 
            pidgin_media_set_state(media,PIDGIN_MEDIA_REQUESTED);
          g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *(media -> priv)).media),((GType )(20 << 2))))),"error",((GCallback )pidgin_media_error_cb),media,0,((GConnectFlags )0));
          g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *(media -> priv)).media),((GType )(20 << 2))))),"state-changed",((GCallback )pidgin_media_state_changed_cb),media,0,((GConnectFlags )0));
          g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)( *(media -> priv)).media),((GType )(20 << 2))))),"stream-info",((GCallback )pidgin_media_stream_info_cb),media,0,((GConnectFlags )0));
          break; 
        }
      }
      case PROP_SCREENNAME:
{
        if (( *(media -> priv)).screenname != 0) 
          g_free(( *(media -> priv)).screenname);
        ( *(media -> priv)).screenname = g_value_dup_string(value);
        break; 
      }
      default:
{
        do {
          GObject *_glib__object = (GObject *)object;
          GParamSpec *_glib__pspec = (GParamSpec *)pspec;
          guint _glib__property_id = prop_id;
          g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","gtkmedia.c:1012","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
        }while (0);
        break; 
      }
    }
  }
}

static void pidgin_media_get_property(GObject *object,guint prop_id,GValue *value,GParamSpec *pspec)
{
  PidginMedia *media;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = pidgin_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PIDGIN_IS_MEDIA(object)");
      return ;
    };
  }while (0);
  media = ((PidginMedia *)(g_type_check_instance_cast(((GTypeInstance *)object),pidgin_media_get_type())));
  switch(prop_id){
    case PROP_MEDIA:
{
      g_value_set_object(value,( *(media -> priv)).media);
      break; 
    }
    case PROP_SCREENNAME:
{
      g_value_set_string(value,( *(media -> priv)).screenname);
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = prop_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","gtkmedia.c:1033","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static GtkWidget *pidgin_media_new(PurpleMedia *media,const gchar *screenname)
{
  PidginMedia *gtkmedia = (g_object_new(pidgin_media_get_type(),"media",media,"screenname",screenname,((void *)((void *)0))));
  return (GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)gtkmedia),gtk_widget_get_type()));
}

static void pidgin_media_set_state(PidginMedia *gtkmedia,PidginMediaState state)
{
  ( *(gtkmedia -> priv)).state = state;
}

static gboolean pidgin_media_new_cb(PurpleMediaManager *manager,PurpleMedia *media,PurpleAccount *account,gchar *screenname,gpointer nul)
{
  PidginMedia *gtkmedia = (PidginMedia *)(g_type_check_instance_cast(((GTypeInstance *)(pidgin_media_new(media,screenname))),pidgin_media_get_type()));
  PurpleBuddy *buddy = purple_find_buddy(account,screenname);
  const gchar *alias = (buddy != 0)?purple_buddy_get_contact_alias(buddy) : screenname;
  gtk_window_set_title(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)gtkmedia),gtk_window_get_type()))),alias);
  if (purple_media_is_initiator(media,0,0) == !0) 
    gtk_widget_show(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)gtkmedia),gtk_widget_get_type()))));
  return (!0);
}

static GstElement *create_default_video_src(PurpleMedia *media,const gchar *session_id,const gchar *participant)
{
  GstElement *sendbin;
  GstElement *src;
  GstPad *pad;
  GstPad *ghost;
#ifdef _WIN32
/* autovideosrc doesn't pick ksvideosrc for some reason */
#else
  src = gst_element_factory_make("gconfvideosrc",0);
  if (src == ((GstElement *)((void *)0))) 
    src = gst_element_factory_make("autovideosrc",0);
  if (src == ((GstElement *)((void *)0))) 
    src = gst_element_factory_make("v4l2src",0);
  if (src == ((GstElement *)((void *)0))) 
    src = gst_element_factory_make("v4lsrc",0);
#endif
  if (src == ((GstElement *)((void *)0))) {
    purple_debug_error("gtkmedia","Unable to find a suitable element for the default video source.\n");
    return 0;
  }
  sendbin = gst_bin_new("pidgindefaultvideosrc");
  gst_bin_add(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)sendbin),gst_bin_get_type()))),src);
  pad = gst_element_get_static_pad(src,"src");
  ghost = gst_ghost_pad_new("ghostsrc",pad);
  gst_object_unref(pad);
  gst_element_add_pad(sendbin,ghost);
  return sendbin;
}

static GstElement *create_default_video_sink(PurpleMedia *media,const gchar *session_id,const gchar *participant)
{
  GstElement *sink = gst_element_factory_make("gconfvideosink",0);
  if (sink == ((GstElement *)((void *)0))) 
    sink = gst_element_factory_make("autovideosink",0);
  if (sink == ((GstElement *)((void *)0))) 
    purple_debug_error("gtkmedia","Unable to find a suitable element for the default video sink.\n");
  return sink;
}

static GstElement *create_default_audio_src(PurpleMedia *media,const gchar *session_id,const gchar *participant)
{
  GstElement *src;
  src = gst_element_factory_make("gconfaudiosrc",0);
  if (src == ((GstElement *)((void *)0))) 
    src = gst_element_factory_make("autoaudiosrc",0);
  if (src == ((GstElement *)((void *)0))) 
    src = gst_element_factory_make("alsasrc",0);
  if (src == ((GstElement *)((void *)0))) 
    src = gst_element_factory_make("osssrc",0);
  if (src == ((GstElement *)((void *)0))) 
    src = gst_element_factory_make("dshowaudiosrc",0);
  if (src == ((GstElement *)((void *)0))) {
    purple_debug_error("gtkmedia","Unable to find a suitable element for the default audio source.\n");
    return 0;
  }
  gst_object_set_name(((GstObject *)src),"pidgindefaultaudiosrc");
  return src;
}

static GstElement *create_default_audio_sink(PurpleMedia *media,const gchar *session_id,const gchar *participant)
{
  GstElement *sink;
  sink = gst_element_factory_make("gconfaudiosink",0);
  if (sink == ((GstElement *)((void *)0))) 
    sink = gst_element_factory_make("autoaudiosink",0);
  if (sink == ((GstElement *)((void *)0))) {
    purple_debug_error("gtkmedia","Unable to find a suitable element for the default audio sink.\n");
    return 0;
  }
  return sink;
}
#endif  /* USE_VV */

void pidgin_medias_init()
{
#ifdef USE_VV
  PurpleMediaManager *manager = purple_media_manager_get();
  PurpleMediaElementInfo *default_video_src = (g_object_new(purple_media_element_info_get_type(),"id","pidgindefaultvideosrc","name","Pidgin Default Video Source","type",PURPLE_MEDIA_ELEMENT_VIDEO | PURPLE_MEDIA_ELEMENT_SRC | PURPLE_MEDIA_ELEMENT_ONE_SRC | PURPLE_MEDIA_ELEMENT_UNIQUE,"create-cb",create_default_video_src,((void *)((void *)0))));
  PurpleMediaElementInfo *default_video_sink = (g_object_new(purple_media_element_info_get_type(),"id","pidgindefaultvideosink","name","Pidgin Default Video Sink","type",PURPLE_MEDIA_ELEMENT_VIDEO | PURPLE_MEDIA_ELEMENT_SINK | PURPLE_MEDIA_ELEMENT_ONE_SINK,"create-cb",create_default_video_sink,((void *)((void *)0))));
  PurpleMediaElementInfo *default_audio_src = (g_object_new(purple_media_element_info_get_type(),"id","pidgindefaultaudiosrc","name","Pidgin Default Audio Source","type",PURPLE_MEDIA_ELEMENT_AUDIO | PURPLE_MEDIA_ELEMENT_SRC | PURPLE_MEDIA_ELEMENT_ONE_SRC | PURPLE_MEDIA_ELEMENT_UNIQUE,"create-cb",create_default_audio_src,((void *)((void *)0))));
  PurpleMediaElementInfo *default_audio_sink = (g_object_new(purple_media_element_info_get_type(),"id","pidgindefaultaudiosink","name","Pidgin Default Audio Sink","type",PURPLE_MEDIA_ELEMENT_AUDIO | PURPLE_MEDIA_ELEMENT_SINK | PURPLE_MEDIA_ELEMENT_ONE_SINK,"create-cb",create_default_audio_sink,((void *)((void *)0))));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)manager),((GType )(20 << 2))))),"init-media",((GCallback )pidgin_media_new_cb),0,0,((GConnectFlags )0));
  purple_media_manager_set_ui_caps(manager,(PURPLE_MEDIA_CAPS_AUDIO | PURPLE_MEDIA_CAPS_AUDIO_SINGLE_DIRECTION | PURPLE_MEDIA_CAPS_VIDEO | PURPLE_MEDIA_CAPS_VIDEO_SINGLE_DIRECTION | PURPLE_MEDIA_CAPS_AUDIO_VIDEO));
  purple_debug_info("gtkmedia","Registering media element types\n");
  purple_media_manager_set_active_element(manager,default_video_src);
  purple_media_manager_set_active_element(manager,default_video_sink);
  purple_media_manager_set_active_element(manager,default_audio_src);
  purple_media_manager_set_active_element(manager,default_audio_sink);
#endif
}
