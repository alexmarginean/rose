/*
 * System tray icon (aka docklet) plugin for Purple
 *
 * Copyright (C) 2002-3 Robert McQueen <robot101@debian.org>
 * Copyright (C) 2003 Herman Bloggs <hermanator12002@yahoo.com>
 * Inspired by a similar plugin by:
 *  John (J5) Palmieri <johnp@martianrock.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02111-1301, USA.
 */
#include "internal.h"
#include "pidgin.h"
#include "core.h"
#include "conversation.h"
#include "debug.h"
#include "prefs.h"
#include "signals.h"
#include "sound.h"
#include "gtkaccount.h"
#include "gtkblist.h"
#include "gtkconv.h"
#include "gtkplugin.h"
#include "gtkprefs.h"
#include "gtksavedstatuses.h"
#include "gtksound.h"
#include "gtkstatusbox.h"
#include "gtkutils.h"
#include "pidginstock.h"
#include "gtkdocklet.h"
#include "gtkdialogs.h"
#ifndef DOCKLET_TOOLTIP_LINE_LIMIT
#define DOCKLET_TOOLTIP_LINE_LIMIT 5
#endif
/* globals */
static struct docklet_ui_ops *ui_ops = (struct docklet_ui_ops *)((void *)0);
static PurpleStatusPrimitive status = PURPLE_STATUS_OFFLINE;
static gboolean pending = 0;
static gboolean connecting = 0;
static gboolean enable_join_chat = 0;
static guint docklet_blinking_timer = 0;
static gboolean visible = 0;
static gboolean visibility_manager = 0;
/**************************************************************************
 * docklet status and utility functions
 **************************************************************************/

static gboolean docklet_blink_icon(gpointer data)
{
  static gboolean blinked = 0;
/* by default, don't keep blinking */
  gboolean ret = 0;
  blinked = !(blinked != 0);
  if ((pending != 0) && !(connecting != 0)) {
    if (blinked != 0) {
      if ((ui_ops != 0) && ((ui_ops -> blank_icon) != 0)) 
        ( *(ui_ops -> blank_icon))();
    }
    else {
      pidgin_docklet_update_icon();
    }
/* keep blinking */
    ret = (!0);
  }
  else {
    docklet_blinking_timer = 0;
    blinked = 0;
  }
  return ret;
}

static GList *get_pending_list(guint max)
{
  GList *l_im;
  GList *l_chat;
  l_im = pidgin_conversations_find_unseen_list(PURPLE_CONV_TYPE_IM,PIDGIN_UNSEEN_TEXT,0,max);
/* Short circuit if we have our information already */
  if ((max == 1) && (l_im != ((GList *)((void *)0)))) 
    return l_im;
  l_chat = pidgin_conversations_find_unseen_list(PURPLE_CONV_TYPE_CHAT,PIDGIN_UNSEEN_NICK,0,max);
  if ((l_im != ((GList *)((void *)0))) && (l_chat != ((GList *)((void *)0)))) 
    return g_list_concat(l_im,l_chat);
  else if (l_im != ((GList *)((void *)0))) 
    return l_im;
  else 
    return l_chat;
}

static gboolean docklet_update_status()
{
  GList *convs;
  GList *l;
  int count;
  PurpleSavedStatus *saved_status;
  PurpleStatusPrimitive newstatus = PURPLE_STATUS_OFFLINE;
  gboolean newpending = 0;
  gboolean newconnecting = 0;
/* get the current savedstatus */
  saved_status = purple_savedstatus_get_current();
/* determine if any ims have unseen messages */
  convs = get_pending_list(5);
  if (!(strcmp(purple_prefs_get_string("/pidgin/docklet/show"),"pending") != 0)) {
    if (((convs != 0) && ((ui_ops -> create) != 0)) && !(visible != 0)) {
      g_list_free(convs);
      ( *(ui_ops -> create))();
      return 0;
    }
    else if ((!(convs != 0) && ((ui_ops -> destroy) != 0)) && (visible != 0)) {
      ( *(ui_ops -> destroy))();
      return 0;
    }
  }
  if (!(visible != 0)) {
    g_list_free(convs);
    return 0;
  }
  if (convs != ((GList *)((void *)0))) {
    newpending = (!0);
/* set tooltip if messages are pending */
    if ((ui_ops -> set_tooltip) != 0) {
      GString *tooltip_text = g_string_new("");
      for (((l = convs) , (count = 0)); l != ((GList *)((void *)0)); ((l = (l -> next)) , count++)) {
        PurpleConversation *conv = (PurpleConversation *)(l -> data);
        PidginConversation *gtkconv = (PidginConversation *)(conv -> ui_data);
        if (count == 5 - 1) {
          g_string_append(tooltip_text,((const char *)(dgettext("pidgin","Right-click for more unread messages...\n"))));
        }
        else if (gtkconv != 0) {
          g_string_append_printf(tooltip_text,(ngettext("%d unread message from %s\n","%d unread messages from %s\n",(gtkconv -> unseen_count))),(gtkconv -> unseen_count),purple_conversation_get_title(conv));
        }
        else {
          g_string_append_printf(tooltip_text,(ngettext("%d unread message from %s\n","%d unread messages from %s\n",((gint )((glong )(purple_conversation_get_data(conv,"unseen-count")))))),((gint )((glong )(purple_conversation_get_data(conv,"unseen-count")))),purple_conversation_get_title(conv));
        }
      }
/* get rid of the last newline */
      if ((tooltip_text -> len) > 0) 
        tooltip_text = g_string_truncate(tooltip_text,((tooltip_text -> len) - 1));
      ( *(ui_ops -> set_tooltip))((tooltip_text -> str));
      g_string_free(tooltip_text,(!0));
    }
    g_list_free(convs);
  }
  else if ((ui_ops -> set_tooltip) != 0) {
    char *tooltip_text = g_strconcat(((const char *)(dgettext("pidgin","Pidgin")))," - ",purple_savedstatus_get_title(saved_status),((void *)((void *)0)));
    ( *(ui_ops -> set_tooltip))(tooltip_text);
    g_free(tooltip_text);
  }
  for (l = purple_accounts_get_all(); l != ((GList *)((void *)0)); l = (l -> next)) {{
      PurpleAccount *account = (PurpleAccount *)(l -> data);
      if (!(purple_account_get_enabled(account,"gtk-gaim") != 0)) 
        continue; 
      if (purple_account_is_disconnected(account) != 0) 
        continue; 
      if (purple_account_is_connecting(account) != 0) 
        newconnecting = (!0);
    }
  }
  newstatus = purple_savedstatus_get_type(saved_status);
/* update the icon if we changed status */
  if (((status != newstatus) || (pending != newpending)) || (connecting != newconnecting)) {
    status = newstatus;
    pending = newpending;
    connecting = newconnecting;
    pidgin_docklet_update_icon();
/* and schedule the blinker function if messages are pending */
    if ((((purple_prefs_get_bool("/pidgin/docklet/blink") != 0) && (pending != 0)) && !(connecting != 0)) && (docklet_blinking_timer == 0)) {
      docklet_blinking_timer = g_timeout_add(500,docklet_blink_icon,0);
    }
  }
/* for when we're called by the glib idle handler */
  return 0;
}

static gboolean online_account_supports_chat()
{
  GList *c = (GList *)((void *)0);
  c = purple_connections_get_all();
  while(c != ((GList *)((void *)0))){
    PurpleConnection *gc = (c -> data);
    PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info;
    if ((prpl_info != ((PurplePluginProtocolInfo *)((void *)0))) && ((prpl_info -> chat_info) != ((GList *(*)(PurpleConnection *))((void *)0)))) 
      return (!0);
    c = (c -> next);
  }
  return 0;
}
/**************************************************************************
 * callbacks and signal handlers
 **************************************************************************/
#if 0
/* TODO: confirm quit while pending */
#endif

static void docklet_update_status_cb(void *data)
{
  docklet_update_status();
}

static void docklet_conv_updated_cb(PurpleConversation *conv,PurpleConvUpdateType type)
{
  if (type == PURPLE_CONV_UPDATE_UNSEEN) 
    docklet_update_status();
}

static void docklet_signed_on_cb(PurpleConnection *gc)
{
  if (!(enable_join_chat != 0)) {
    if (( *((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info)).chat_info != ((GList *(*)(PurpleConnection *))((void *)0))) 
      enable_join_chat = (!0);
  }
  docklet_update_status();
}

static void docklet_signed_off_cb(PurpleConnection *gc)
{
  if (enable_join_chat != 0) {
    if (( *((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info)).chat_info != ((GList *(*)(PurpleConnection *))((void *)0))) 
      enable_join_chat = online_account_supports_chat();
  }
  docklet_update_status();
}

static void docklet_show_pref_changed_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  const char *val = value;
  if (!(strcmp(val,"always") != 0)) {
    if ((ui_ops -> create) != 0) {
      if (!(visible != 0)) 
        ( *(ui_ops -> create))();
      else if (!(visibility_manager != 0)) {
        pidgin_blist_visibility_manager_add();
        visibility_manager = (!0);
      }
    }
  }
  else if (!(strcmp(val,"never") != 0)) {
    if ((visible != 0) && ((ui_ops -> destroy) != 0)) 
      ( *(ui_ops -> destroy))();
  }
  else {
    if (visibility_manager != 0) {
      pidgin_blist_visibility_manager_remove();
      visibility_manager = 0;
    }
    docklet_update_status();
  }
}
/**************************************************************************
 * docklet pop-up menu
 **************************************************************************/

static void docklet_toggle_mute(GtkWidget *toggle,void *data)
{
  purple_prefs_set_bool("/pidgin/sound/mute",( *((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)toggle),gtk_check_menu_item_get_type())))).active);
}

static void docklet_toggle_blink(GtkWidget *toggle,void *data)
{
  purple_prefs_set_bool("/pidgin/docklet/blink",( *((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)toggle),gtk_check_menu_item_get_type())))).active);
}

static void docklet_toggle_blist(GtkWidget *toggle,void *data)
{
  purple_blist_set_visible(( *((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)toggle),gtk_check_menu_item_get_type())))).active);
}
#ifdef _WIN32
/* This is a workaround for a bug in windows GTK+. Clicking outside of the
   menu does not get rid of it, so instead we get rid of it as soon as the
   pointer leaves the menu. */
/* Add some slop so that the menu doesn't annoyingly disappear when mousing around */
/* Cancel the hiding if we reenter */
#endif
/* There is a lot of code here for handling the status submenu, much of
 * which is duplicated from the gtkstatusbox. It'd be nice to add API
 * somewhere to simplify this (either in the statusbox, or in libpurple).
 */

static void show_custom_status_editor_cb(GtkMenuItem *menuitem,gpointer user_data)
{
  PurpleSavedStatus *saved_status;
  saved_status = purple_savedstatus_get_current();
  if ((purple_savedstatus_get_type(saved_status)) == PURPLE_STATUS_AVAILABLE) 
    saved_status = purple_savedstatus_new(0,PURPLE_STATUS_AWAY);
  pidgin_status_editor_show(0,((purple_savedstatus_is_transient(saved_status) != 0)?saved_status : ((struct _PurpleSavedStatus *)((void *)0))));
}

static PurpleSavedStatus *create_transient_status(PurpleStatusPrimitive primitive,PurpleStatusType *status_type)
{
  PurpleSavedStatus *saved_status = purple_savedstatus_new(0,primitive);
  if (status_type != ((PurpleStatusType *)((void *)0))) {
    GList *tmp;
    GList *active_accts = purple_accounts_get_all_active();
    for (tmp = active_accts; tmp != ((GList *)((void *)0)); tmp = (tmp -> next)) {
      purple_savedstatus_set_substatus(saved_status,((PurpleAccount *)(tmp -> data)),status_type,0);
    }
    g_list_free(active_accts);
  }
  return saved_status;
}

static void activate_status_account_cb(GtkMenuItem *menuitem,gpointer user_data)
{
  PurpleStatusType *status_type;
  PurpleStatusPrimitive primitive;
  PurpleSavedStatus *saved_status = (PurpleSavedStatus *)((void *)0);
  GList *iter = purple_savedstatuses_get_all();
  GList *tmp;
  GList *active_accts = purple_accounts_get_all_active();
  status_type = ((PurpleStatusType *)user_data);
  primitive = purple_status_type_get_primitive(status_type);
{
    for (; iter != ((GList *)((void *)0)); iter = (iter -> next)) {{
        PurpleSavedStatus *ss = (iter -> data);
        if ((((purple_savedstatus_get_type(ss)) == primitive) && (purple_savedstatus_is_transient(ss) != 0)) && (purple_savedstatus_has_substatuses(ss) != 0)) {
          gboolean found = 0;
/* The currently enabled accounts must have substatuses for all the active accts */
          for (tmp = active_accts; tmp != ((GList *)((void *)0)); tmp = (tmp -> next)) {
            PurpleAccount *acct = (tmp -> data);
            PurpleSavedStatusSub *sub = purple_savedstatus_get_substatus(ss,acct);
            if (sub != 0) {
              const PurpleStatusType *sub_type = purple_savedstatus_substatus_get_type(sub);
              const char *subtype_status_id = purple_status_type_get_id(sub_type);
              if ((subtype_status_id != 0) && !(strcmp(subtype_status_id,purple_status_type_get_id(status_type)) != 0)) 
                found = (!0);
            }
          }
          if (!(found != 0)) 
            continue; 
          saved_status = ss;
          break; 
        }
      }
    }
  }
  g_list_free(active_accts);
/* Create a new transient saved status if we weren't able to find one */
  if (saved_status == ((PurpleSavedStatus *)((void *)0))) 
    saved_status = create_transient_status(primitive,status_type);
/* Set the status for each account */
  purple_savedstatus_activate(saved_status);
}

static void activate_status_primitive_cb(GtkMenuItem *menuitem,gpointer user_data)
{
  PurpleStatusPrimitive primitive;
  PurpleSavedStatus *saved_status;
  primitive = ((gint )((glong )user_data));
/* Try to lookup an already existing transient saved status */
  saved_status = purple_savedstatus_find_transient_by_type_and_message(primitive,0);
/* Create a new transient saved status if we weren't able to find one */
  if (saved_status == ((PurpleSavedStatus *)((void *)0))) 
    saved_status = create_transient_status(primitive,0);
/* Set the status for each account */
  purple_savedstatus_activate(saved_status);
}

static void activate_saved_status_cb(GtkMenuItem *menuitem,gpointer user_data)
{
  time_t creation_time;
  PurpleSavedStatus *saved_status;
  creation_time = ((gint )((glong )user_data));
  saved_status = purple_savedstatus_find_by_creation_time(creation_time);
  if (saved_status != ((PurpleSavedStatus *)((void *)0))) 
    purple_savedstatus_activate(saved_status);
}

static GtkWidget *new_menu_item_with_status_icon(GtkWidget *menu,const char *str,PurpleStatusPrimitive primitive,GCallback cb,gpointer data,guint accel_key,guint accel_mods,char *mod)
{
  GtkWidget *menuitem;
  GdkPixbuf *pixbuf;
  GtkWidget *image;
  menuitem = gtk_image_menu_item_new_with_label(str);
  if (menu != 0) 
    gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem);
  if (cb != 0) 
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"activate",cb,data,0,((GConnectFlags )0));
  pixbuf = pidgin_create_status_icon(primitive,menu,"pidgin-icon-size-tango-extra-small");
  image = gtk_image_new_from_pixbuf(pixbuf);
  g_object_unref(pixbuf);
  gtk_image_menu_item_set_image(((GtkImageMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_image_menu_item_get_type()))),image);
  gtk_widget_show_all(menuitem);
  return menuitem;
}

static void add_account_statuses(GtkWidget *menu,PurpleAccount *account)
{
  GList *l;
  for (l = purple_account_get_status_types(account); l != ((GList *)((void *)0)); l = (l -> next)) {{
      PurpleStatusType *status_type = (PurpleStatusType *)(l -> data);
      PurpleStatusPrimitive prim;
      if (!(purple_status_type_is_user_settable(status_type) != 0)) 
        continue; 
      prim = purple_status_type_get_primitive(status_type);
      new_menu_item_with_status_icon(menu,purple_status_type_get_name(status_type),prim,((GCallback )activate_status_account_cb),status_type,0,0,0);
    }
  }
}

static GtkWidget *docklet_status_submenu()
{
  GtkWidget *submenu;
  GtkWidget *menuitem;
  GList *popular_statuses;
  GList *cur;
  PidginStatusBox *statusbox = (PidginStatusBox *)((void *)0);
  submenu = gtk_menu_new();
  menuitem = gtk_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","_Change Status"))));
  gtk_menu_item_set_submenu(((GtkMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_menu_item_get_type()))),submenu);
  if (pidgin_blist_get_default_gtk_blist() != ((PidginBuddyList *)((void *)0))) {
    statusbox = ((PidginStatusBox *)(g_type_check_instance_cast(((GTypeInstance *)( *pidgin_blist_get_default_gtk_blist()).statusbox),pidgin_status_box_get_type())));
  }
  if ((statusbox != 0) && ((statusbox -> account) != ((PurpleAccount *)((void *)0)))) {
    add_account_statuses(submenu,(statusbox -> account));
  }
  else if ((statusbox != 0) && ((statusbox -> token_status_account) != ((PurpleAccount *)((void *)0)))) {
    add_account_statuses(submenu,(statusbox -> token_status_account));
  }
  else {
    new_menu_item_with_status_icon(submenu,((const char *)(dgettext("pidgin","Available"))),PURPLE_STATUS_AVAILABLE,((GCallback )activate_status_primitive_cb),((gpointer )((gpointer )((glong )PURPLE_STATUS_AVAILABLE))),0,0,0);
    new_menu_item_with_status_icon(submenu,((const char *)(dgettext("pidgin","Away"))),PURPLE_STATUS_AWAY,((GCallback )activate_status_primitive_cb),((gpointer )((gpointer )((glong )PURPLE_STATUS_AWAY))),0,0,0);
    new_menu_item_with_status_icon(submenu,((const char *)(dgettext("pidgin","Do not disturb"))),PURPLE_STATUS_UNAVAILABLE,((GCallback )activate_status_primitive_cb),((gpointer )((gpointer )((glong )PURPLE_STATUS_UNAVAILABLE))),0,0,0);
    new_menu_item_with_status_icon(submenu,((const char *)(dgettext("pidgin","Invisible"))),PURPLE_STATUS_INVISIBLE,((GCallback )activate_status_primitive_cb),((gpointer )((gpointer )((glong )PURPLE_STATUS_INVISIBLE))),0,0,0);
    new_menu_item_with_status_icon(submenu,((const char *)(dgettext("pidgin","Offline"))),PURPLE_STATUS_OFFLINE,((GCallback )activate_status_primitive_cb),((gpointer )((gpointer )((glong )PURPLE_STATUS_OFFLINE))),0,0,0);
  }
  popular_statuses = purple_savedstatuses_get_popular(6);
  if (popular_statuses != ((GList *)((void *)0))) 
    pidgin_separator(submenu);
  for (cur = popular_statuses; cur != ((GList *)((void *)0)); cur = (cur -> next)) {
    PurpleSavedStatus *saved_status = (cur -> data);
    time_t creation_time = purple_savedstatus_get_creation_time(saved_status);
    new_menu_item_with_status_icon(submenu,purple_savedstatus_get_title(saved_status),purple_savedstatus_get_type(saved_status),((GCallback )activate_saved_status_cb),((gpointer )((glong )creation_time)),0,0,0);
  }
  g_list_free(popular_statuses);
  pidgin_separator(submenu);
  pidgin_new_item_from_stock(submenu,((const char *)(dgettext("pidgin","New..."))),0,((GCallback )show_custom_status_editor_cb),0,0,0,0);
  pidgin_new_item_from_stock(submenu,((const char *)(dgettext("pidgin","Saved..."))),0,((GCallback )pidgin_status_window_show),0,0,0,0);
  return menuitem;
}

static void plugin_act(GtkObject *obj,PurplePluginAction *pam)
{
  if ((pam != 0) && ((pam -> callback) != 0)) 
    ( *(pam -> callback))(pam);
}

static void build_plugin_actions(GtkWidget *menu,PurplePlugin *plugin,gpointer context)
{
  GtkWidget *menuitem;
  PurplePluginAction *action = (PurplePluginAction *)((void *)0);
  GList *actions;
  GList *l;
  actions = ((((plugin -> info) != ((PurplePluginInfo *)((void *)0))) && (( *(plugin -> info)).actions != ((GList *(*)(PurplePlugin *, gpointer ))((void *)0))))?( *( *(plugin -> info)).actions)(plugin,context) : ((struct _GList *)((void *)0)));
  for (l = actions; l != ((GList *)((void *)0)); l = (l -> next)) {
    if ((l -> data) != 0) {
      action = ((PurplePluginAction *)(l -> data));
      action -> plugin = plugin;
      action -> context = context;
      menuitem = gtk_menu_item_new_with_label((action -> label));
      gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem);
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"activate",((GCallback )plugin_act),action,0,((GConnectFlags )0));
      g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"plugin_action",action,((GDestroyNotify )purple_plugin_action_free));
      gtk_widget_show(menuitem);
    }
    else 
      pidgin_separator(menu);
  }
  g_list_free(actions);
}

static void docklet_plugin_actions(GtkWidget *menu)
{
  GtkWidget *menuitem;
  GtkWidget *submenu;
  PurplePlugin *plugin = (PurplePlugin *)((void *)0);
  GList *l;
  int c = 0;
  do {
    if (menu != ((GtkWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"menu != NULL");
      return ;
    };
  }while (0);
/* Add a submenu for each plugin with custom actions */
  for (l = purple_plugins_get_loaded(); l != 0; l = (l -> next)) {
    plugin = ((PurplePlugin *)(l -> data));
    if (( *(plugin -> info)).type == PURPLE_PLUGIN_PROTOCOL) 
      continue; 
    if (!(((plugin -> info) != ((PurplePluginInfo *)((void *)0))) && (( *(plugin -> info)).actions != ((GList *(*)(PurplePlugin *, gpointer ))((void *)0))))) 
      continue; 
    menuitem = gtk_image_menu_item_new_with_label(((const char *)(dgettext("pidgin",( *(plugin -> info)).name))));
    gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem);
    submenu = gtk_menu_new();
    gtk_menu_item_set_submenu(((GtkMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_menu_item_get_type()))),submenu);
    build_plugin_actions(submenu,plugin,0);
    c++;
  }
  if (c > 0) 
    pidgin_separator(menu);
}

static void docklet_menu()
{
  static GtkWidget *menu = (GtkWidget *)((void *)0);
  GtkWidget *menuitem;
  if (menu != 0) {
    gtk_widget_destroy(menu);
  }
  menu = gtk_menu_new();
  menuitem = gtk_check_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","Show Buddy _List"))));
  gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_check_menu_item_get_type()))),purple_prefs_get_bool("/pidgin/blist/list_visible"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"toggled",((GCallback )docklet_toggle_blist),0,0,((GConnectFlags )0));
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem);
  menuitem = gtk_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","_Unread Messages"))));
  if (pending != 0) {
    GtkWidget *submenu = gtk_menu_new();
    GList *l = get_pending_list(0);
    if (l == ((GList *)((void *)0))) {
      gtk_widget_set_sensitive(menuitem,0);
      purple_debug_warning("docklet","status indicates messages pending, but no conversations with unseen messages were found.");
    }
    else {
      pidgin_conversations_fill_menu(submenu,l);
      g_list_free(l);
      gtk_menu_item_set_submenu(((GtkMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_menu_item_get_type()))),submenu);
    }
  }
  else {
    gtk_widget_set_sensitive(menuitem,0);
  }
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem);
  pidgin_separator(menu);
  menuitem = pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","New _Message..."))),"pidgin-message-new",((GCallback )pidgin_dialogs_im),0,0,0,0);
  if (status == PURPLE_STATUS_OFFLINE) 
    gtk_widget_set_sensitive(menuitem,0);
  menuitem = pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Join Chat..."))),"pidgin-chat",((GCallback )pidgin_blist_joinchat_show),0,0,0,0);
  if (status == PURPLE_STATUS_OFFLINE) 
    gtk_widget_set_sensitive(menuitem,0);
  menuitem = docklet_status_submenu();
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem);
  pidgin_separator(menu);
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Accounts"))),0,((GCallback )pidgin_accounts_window_show),0,0,0,0);
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Plu_gins"))),"pidgin-plugins",((GCallback )pidgin_plugin_dialog_show),0,0,0,0);
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","Pr_eferences"))),"gtk-preferences",((GCallback )pidgin_prefs_show),0,0,0,0);
  pidgin_separator(menu);
  menuitem = gtk_check_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","Mute _Sounds"))));
  gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_check_menu_item_get_type()))),purple_prefs_get_bool("/pidgin/sound/mute"));
  if (!(strcmp(purple_prefs_get_string("/pidgin/sound/method"),"none") != 0)) 
    gtk_widget_set_sensitive(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_widget_get_type()))),0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"toggled",((GCallback )docklet_toggle_mute),0,0,((GConnectFlags )0));
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem);
  menuitem = gtk_check_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","_Blink on New Message"))));
  gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_check_menu_item_get_type()))),purple_prefs_get_bool("/pidgin/docklet/blink"));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"toggled",((GCallback )docklet_toggle_blink),0,0,((GConnectFlags )0));
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem);
  pidgin_separator(menu);
/* add plugin actions */
  docklet_plugin_actions(menu);
  pidgin_new_item_from_stock(menu,((const char *)(dgettext("pidgin","_Quit"))),"gtk-quit",((GCallback )purple_core_quit),0,0,0,0);
#ifdef _WIN32
#endif
  gtk_widget_show_all(menu);
  gtk_menu_popup(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_get_type()))),0,0,(ui_ops -> position_menu),0,0,gtk_get_current_event_time());
}
/**************************************************************************
 * public api for ui_ops
 **************************************************************************/

void pidgin_docklet_update_icon()
{
  if ((ui_ops != 0) && ((ui_ops -> update_icon) != 0)) 
    ( *(ui_ops -> update_icon))(status,connecting,pending);
}

void pidgin_docklet_clicked(int button_type)
{
  switch(button_type){
    case 1:
{
      if (pending != 0) {
        GList *l = get_pending_list(1);
        if (l != ((GList *)((void *)0))) {
          pidgin_conv_present_conversation(((PurpleConversation *)(l -> data)));
          g_list_free(l);
        }
      }
      else {
        pidgin_blist_toggle_visibility();
      }
      break; 
    }
    case 3:
{
      docklet_menu();
      break; 
    }
  }
}

void pidgin_docklet_embedded()
{
  if (!(visibility_manager != 0) && (strcmp(purple_prefs_get_string("/pidgin/docklet/show"),"pending") != 0)) {
    pidgin_blist_visibility_manager_add();
    visibility_manager = (!0);
  }
  visible = (!0);
  docklet_update_status();
  pidgin_docklet_update_icon();
}

void pidgin_docklet_remove()
{
  if (visible != 0) {
    if (visibility_manager != 0) {
      pidgin_blist_visibility_manager_remove();
      visibility_manager = 0;
    }
    if (docklet_blinking_timer != 0U) {
      g_source_remove(docklet_blinking_timer);
      docklet_blinking_timer = 0;
    }
    visible = 0;
    status = PURPLE_STATUS_OFFLINE;
  }
}

void pidgin_docklet_set_ui_ops(struct docklet_ui_ops *ops)
{
  ui_ops = ops;
}

void *pidgin_docklet_get_handle()
{
  static int i;
  return (&i);
}

void pidgin_docklet_init()
{
  void *conn_handle = purple_connections_get_handle();
  void *conv_handle = purple_conversations_get_handle();
  void *accounts_handle = purple_accounts_get_handle();
  void *status_handle = purple_savedstatuses_get_handle();
  void *docklet_handle = pidgin_docklet_get_handle();
  purple_prefs_add_none("/pidgin/docklet");
  purple_prefs_add_bool("/pidgin/docklet/blink",0);
  purple_prefs_add_string("/pidgin/docklet/show","always");
  purple_prefs_connect_callback(docklet_handle,"/pidgin/docklet/show",docklet_show_pref_changed_cb,0);
  docklet_ui_init();
  if ((!(strcmp(purple_prefs_get_string("/pidgin/docklet/show"),"always") != 0) && (ui_ops != 0)) && ((ui_ops -> create) != 0)) 
    ( *(ui_ops -> create))();
  purple_signal_connect(conn_handle,"signed-on",docklet_handle,((PurpleCallback )docklet_signed_on_cb),0);
  purple_signal_connect(conn_handle,"signed-off",docklet_handle,((PurpleCallback )docklet_signed_off_cb),0);
  purple_signal_connect(accounts_handle,"account-connecting",docklet_handle,((PurpleCallback )docklet_update_status_cb),0);
  purple_signal_connect(conv_handle,"received-im-msg",docklet_handle,((PurpleCallback )docklet_update_status_cb),0);
  purple_signal_connect(conv_handle,"conversation-created",docklet_handle,((PurpleCallback )docklet_update_status_cb),0);
  purple_signal_connect(conv_handle,"deleting-conversation",docklet_handle,((PurpleCallback )docklet_update_status_cb),0);
  purple_signal_connect(conv_handle,"conversation-updated",docklet_handle,((PurpleCallback )docklet_conv_updated_cb),0);
  purple_signal_connect(status_handle,"savedstatus-changed",docklet_handle,((PurpleCallback )docklet_update_status_cb),0);
#if 0
#endif
  enable_join_chat = online_account_supports_chat();
}

void pidgin_docklet_uninit()
{
  if (((visible != 0) && (ui_ops != 0)) && ((ui_ops -> destroy) != 0)) 
    ( *(ui_ops -> destroy))();
}
