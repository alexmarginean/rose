/**
 * @file gtkrequest.c GTK+ Request API
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "pidgin.h"
#include "debug.h"
#include "prefs.h"
#include "util.h"
#include "gtkimhtml.h"
#include "gtkimhtmltoolbar.h"
#include "gtkrequest.h"
#include "gtkutils.h"
#include "pidginstock.h"
#include "gtkblist.h"
#include <gdk/gdkkeysyms.h>
static GtkWidget *create_account_field(PurpleRequestField *field);
typedef struct __unnamed_class___F0_L44_C9_unknown_scope_and_name_variable_declaration__variable_type_L1655R_variable_name_unknown_scope_and_name__scope__type__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__v__Pe___variable_name_unknown_scope_and_name__scope__user_data__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__dialog__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__ok_button__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__cb_count__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L187R__Pe___variable_name_unknown_scope_and_name__scope__cbs__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__variable_name_unknown_scope_and_name__scope__u {
PurpleRequestType type;
void *user_data;
GtkWidget *dialog;
GtkWidget *ok_button;
size_t cb_count;
GCallback *cbs;
union __unnamed_class___F0_L56_C2_L1657R_variable_declaration__variable_type__variable_name_L1657R__scope__input__DELIMITER__L1657R_variable_declaration__variable_type__variable_name_L1657R__scope__multifield__DELIMITER__L1657R_variable_declaration__variable_type__variable_name_L1657R__scope__file {
struct __unnamed_class___F0_L58_C3_L1658R_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_L1658R__scope__entry__DELIMITER__L1658R_variable_declaration__variable_type_L54R_variable_name_L1658R__scope__multiline__DELIMITER__L1658R_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_L1658R__scope__hint {
GtkWidget *entry;
gboolean multiline;
gchar *hint;}input;
struct __unnamed_class___F0_L67_C3_L1658R_variable_declaration__variable_type___Pb__L1653R__Pe___variable_name_L1658R__scope__fields {
PurpleRequestFields *fields;}multifield;
struct __unnamed_class___F0_L73_C3_L1658R_variable_declaration__variable_type_L54R_variable_name_L1658R__scope__savedialog__DELIMITER__L1658R_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_L1658R__scope__name {
gboolean savedialog;
gchar *name;}file;}u;}PidginRequestData;

static void pidgin_widget_decorate_account(GtkWidget *cont,PurpleAccount *account)
{
  GtkWidget *image;
  GdkPixbuf *pixbuf;
  GtkTooltips *tips;
  if (!(account != 0)) 
    return ;
  pixbuf = pidgin_create_prpl_icon(account,PIDGIN_PRPL_ICON_SMALL);
  image = gtk_image_new_from_pixbuf(pixbuf);
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
  tips = gtk_tooltips_new();
  gtk_tooltips_set_tip(tips,image,purple_account_get_username(account),0);
  if ((({
    GTypeInstance *__inst = (GTypeInstance *)cont;
    GType __t = gtk_dialog_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
{
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)cont),gtk_dialog_get_type())))).action_area),gtk_box_get_type()))),image,0,(!0),0);
    gtk_box_reorder_child(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)cont),gtk_dialog_get_type())))).action_area),gtk_box_get_type()))),image,0);
  }
  else if ((({
    GTypeInstance *__inst = (GTypeInstance *)cont;
    GType __t = gtk_hbox_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
{
    gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)image),gtk_misc_get_type()))),0,0);
    gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)cont),gtk_box_get_type()))),image,0,(!0),0);
  }
  gtk_widget_show(image);
}

static void generic_response_start(PidginRequestData *data)
{
  do {
    if (data != ((PidginRequestData *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"data != NULL");
      return ;
    };
  }while (0);
/* Tell the user we're doing something. */
  pidgin_set_cursor(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(data -> dialog)),gtk_widget_get_type()))),GDK_WATCH);
}

static void input_response_cb(GtkDialog *dialog,gint id,PidginRequestData *data)
{
  const char *value;
  char *multiline_value = (char *)((void *)0);
  generic_response_start(data);
  if (data -> u.input.multiline != 0) {
    GtkTextIter start_iter;
    GtkTextIter end_iter;
    GtkTextBuffer *buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)data -> u.input.entry),gtk_text_view_get_type()))));
    gtk_text_buffer_get_start_iter(buffer,&start_iter);
    gtk_text_buffer_get_end_iter(buffer,&end_iter);
    if ((data -> u.input.hint != ((gchar *)((void *)0))) && !(strcmp(data -> u.input.hint,"html") != 0)) 
      multiline_value = gtk_imhtml_get_markup(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)data -> u.input.entry),gtk_imhtml_get_type()))));
    else 
      multiline_value = gtk_text_buffer_get_text(buffer,(&start_iter),(&end_iter),0);
    value = multiline_value;
  }
  else 
    value = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)data -> u.input.entry),gtk_entry_get_type()))));
  if ((id < (data -> cb_count)) && ((data -> cbs)[id] != ((void (*)())((void *)0)))) 
    ( *((PurpleRequestInputCb )(data -> cbs)[id]))((data -> user_data),value);
  else if ((data -> cbs)[1] != ((void (*)())((void *)0))) 
    ( *((PurpleRequestInputCb )(data -> cbs)[1]))((data -> user_data),value);
  if (data -> u.input.multiline != 0) 
    g_free(multiline_value);
  purple_request_close(PURPLE_REQUEST_INPUT,data);
}

static void action_response_cb(GtkDialog *dialog,gint id,PidginRequestData *data)
{
  generic_response_start(data);
  if ((id < (data -> cb_count)) && ((data -> cbs)[id] != ((void (*)())((void *)0)))) 
    ( *((PurpleRequestActionCb )(data -> cbs)[id]))((data -> user_data),id);
  purple_request_close(PURPLE_REQUEST_INPUT,data);
}

static void choice_response_cb(GtkDialog *dialog,gint id,PidginRequestData *data)
{
  GtkWidget *radio = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dialog),((GType )(20 << 2))))),"radio"));
  GSList *group = gtk_radio_button_get_group(((GtkRadioButton *)(g_type_check_instance_cast(((GTypeInstance *)radio),gtk_radio_button_get_type()))));
  generic_response_start(data);
  if ((id < (data -> cb_count)) && ((data -> cbs)[id] != ((void (*)())((void *)0)))) {
    while(group != 0){
      if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)(group -> data)),gtk_toggle_button_get_type())))) != 0) {
        ( *((PurpleRequestChoiceCb )(data -> cbs)[id]))((data -> user_data),((gint )((glong )(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(group -> data)),((GType )(20 << 2))))),"choice_id")))));
        break; 
      }
      group = (group -> next);
    }
  }
  purple_request_close(PURPLE_REQUEST_INPUT,data);
}

static gboolean field_string_focus_out_cb(GtkWidget *entry,GdkEventFocus *event,PurpleRequestField *field)
{
  const char *value;
  if (purple_request_field_string_is_multiline(field) != 0) {
    GtkTextBuffer *buffer;
    GtkTextIter start_iter;
    GtkTextIter end_iter;
    buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_text_view_get_type()))));
    gtk_text_buffer_get_start_iter(buffer,&start_iter);
    gtk_text_buffer_get_end_iter(buffer,&end_iter);
    value = (gtk_text_buffer_get_text(buffer,(&start_iter),(&end_iter),0));
  }
  else 
    value = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))));
  purple_request_field_string_set_value(field,((( *value) == 0)?((const char *)((void *)0)) : value));
  return 0;
}

static gboolean field_int_focus_out_cb(GtkEntry *entry,GdkEventFocus *event,PurpleRequestField *field)
{
  purple_request_field_int_set_value(field,atoi(gtk_entry_get_text(entry)));
  return 0;
}

static void field_bool_cb(GtkToggleButton *button,PurpleRequestField *field)
{
  purple_request_field_bool_set_value(field,gtk_toggle_button_get_active(button));
}

static void field_choice_menu_cb(GtkComboBox *menu,PurpleRequestField *field)
{
  purple_request_field_choice_set_value(field,gtk_combo_box_get_active(menu));
}

static void field_choice_option_cb(GtkRadioButton *button,PurpleRequestField *field)
{
  if (gtk_toggle_button_get_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_toggle_button_get_type())))) != 0) 
    purple_request_field_choice_set_value(field,((g_slist_length(gtk_radio_button_get_group(button)) - (g_slist_index(gtk_radio_button_get_group(button),button))) - 1));
}

static void field_account_cb(GObject *w,PurpleAccount *account,PurpleRequestField *field)
{
  purple_request_field_account_set_value(field,account);
}

static void multifield_ok_cb(GtkWidget *button,PidginRequestData *data)
{
  generic_response_start(data);
  if (!((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_object_get_type())))).flags & GTK_HAS_FOCUS) != 0)) 
    gtk_widget_grab_focus(button);
  if ((data -> cbs)[0] != ((void (*)())((void *)0))) 
    ( *((PurpleRequestFieldsCb )(data -> cbs)[0]))((data -> user_data),data -> u.multifield.fields);
  purple_request_close(PURPLE_REQUEST_FIELDS,data);
}

static void multifield_cancel_cb(GtkWidget *button,PidginRequestData *data)
{
  generic_response_start(data);
  if ((data -> cbs)[1] != ((void (*)())((void *)0))) 
    ( *((PurpleRequestFieldsCb )(data -> cbs)[1]))((data -> user_data),data -> u.multifield.fields);
  purple_request_close(PURPLE_REQUEST_FIELDS,data);
}

static gboolean destroy_multifield_cb(GtkWidget *dialog,GdkEvent *event,PidginRequestData *data)
{
  multifield_cancel_cb(0,data);
  return 0;
}
#define STOCK_ITEMIZE(r, l) \
	if (!strcmp((r), text)) \
		return (l);

static const char *text_to_stock(const char *text)
{
  if (!(strcmp(((const char *)(dgettext("pidgin","Yes"))),text) != 0)) 
    return "gtk-yes";;
  if (!(strcmp(((const char *)(dgettext("pidgin","No"))),text) != 0)) 
    return "gtk-no";;
  if (!(strcmp(((const char *)(dgettext("pidgin","OK"))),text) != 0)) 
    return "gtk-ok";;
  if (!(strcmp(((const char *)(dgettext("pidgin","Cancel"))),text) != 0)) 
    return "gtk-cancel";;
  if (!(strcmp(((const char *)(dgettext("pidgin","Apply"))),text) != 0)) 
    return "gtk-apply";;
  if (!(strcmp(((const char *)(dgettext("pidgin","Close"))),text) != 0)) 
    return "gtk-close";;
  if (!(strcmp(((const char *)(dgettext("pidgin","Delete"))),text) != 0)) 
    return "gtk-delete";;
  if (!(strcmp(((const char *)(dgettext("pidgin","Add"))),text) != 0)) 
    return "gtk-add";;
  if (!(strcmp(((const char *)(dgettext("pidgin","Remove"))),text) != 0)) 
    return "gtk-remove";;
  if (!(strcmp(((const char *)(dgettext("pidgin","Save"))),text) != 0)) 
    return "gtk-save";;
  if (!(strcmp(((const char *)(dgettext("pidgin","Alias"))),text) != 0)) 
    return "pidgin-alias";;
  return text;
}

static void *pidgin_request_input(const char *title,const char *primary,const char *secondary,const char *default_value,gboolean multiline,gboolean masked,gchar *hint,const char *ok_text,GCallback ok_cb,const char *cancel_text,GCallback cancel_cb,PurpleAccount *account,const char *who,PurpleConversation *conv,void *user_data)
{
  PidginRequestData *data;
  GtkWidget *dialog;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *entry;
  GtkWidget *img;
  GtkWidget *toolbar;
  char *label_text;
  char *primary_esc;
  char *secondary_esc;
  data = ((PidginRequestData *)(g_malloc0_n(1,(sizeof(PidginRequestData )))));
  data -> type = PURPLE_REQUEST_INPUT;
  data -> user_data = user_data;
  data -> cb_count = 2;
  data -> cbs = ((GCallback *)(g_malloc0_n(2,(sizeof(GCallback )))));
  (data -> cbs)[0] = ok_cb;
  (data -> cbs)[1] = cancel_cb;
/* Create the dialog. */
  dialog = gtk_dialog_new_with_buttons(((title != 0)?title : ""),0,0,text_to_stock(cancel_text),1,text_to_stock(ok_text),0,((void *)((void *)0)));
  data -> dialog = dialog;
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dialog),((GType )(20 << 2))))),"response",((GCallback )input_response_cb),data,0,((GConnectFlags )0));
/* Setup the dialog */
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_container_get_type()))),(12 / 2));
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type())))).vbox),gtk_container_get_type()))),(12 / 2));
  if (!(multiline != 0)) 
    gtk_window_set_resizable(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_window_get_type()))),0);
  gtk_dialog_set_has_separator(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),0);
  gtk_dialog_set_default_response(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),0);
  gtk_box_set_spacing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type())))).vbox),gtk_box_get_type()))),12);
/* Setup the main horizontal box */
  hbox = gtk_hbox_new(0,12);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type())))).vbox),gtk_container_get_type()))),hbox);
/* Dialog icon. */
  img = gtk_image_new_from_stock("pidgin-dialog-question",gtk_icon_size_from_name("pidgin-icon-size-tango-huge"));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)img),gtk_misc_get_type()))),0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),img,0,0,0);
/* Vertical box */
  vbox = gtk_vbox_new(0,12);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),vbox,(!0),(!0),0);
  pidgin_widget_decorate_account(hbox,account);
/* Descriptive label */
  primary_esc = ((primary != ((const char *)((void *)0)))?g_markup_escape_text(primary,(-1)) : ((char *)((void *)0)));
  secondary_esc = ((secondary != ((const char *)((void *)0)))?g_markup_escape_text(secondary,(-1)) : ((char *)((void *)0)));
  label_text = g_strdup_printf((((primary != 0)?"<span weight=\"bold\" size=\"larger\">%s</span>%s%s" : "%s%s%s")),((primary != 0)?primary_esc : ""),(((primary != 0) && (secondary != 0))?"\n\n" : ""),((secondary != 0)?secondary_esc : ""));
  g_free(primary_esc);
  g_free(secondary_esc);
  label = gtk_label_new(0);
  gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),label_text);
  gtk_label_set_line_wrap(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(!0));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),label,0,0,0);
  g_free(label_text);
/* Entry field. */
  data -> u.input.multiline = multiline;
  data -> u.input.hint = g_strdup(hint);
  gtk_widget_show_all(hbox);
  if ((data -> u.input.hint != ((gchar *)((void *)0))) && !(strcmp(data -> u.input.hint,"html") != 0)) {
    GtkWidget *frame;
/* imhtml */
    frame = pidgin_create_imhtml((!0),&entry,&toolbar,0);
    gtk_widget_set_size_request(entry,320,130);
    gtk_widget_set_name(entry,"pidgin_request_imhtml");
    if (default_value != ((const char *)((void *)0))) 
      gtk_imhtml_append_text_with_images(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_imhtml_get_type()))),default_value,GTK_IMHTML_NO_SCROLL,0);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),frame,(!0),(!0),0);
    gtk_widget_show(frame);
    gtk_imhtml_set_return_inserts_newline(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_imhtml_get_type()))));
  }
  else {
    if (multiline != 0) {
/* GtkTextView */
      entry = gtk_text_view_new();
      gtk_text_view_set_editable(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_text_view_get_type()))),(!0));
      if (default_value != ((const char *)((void *)0))) {
        GtkTextBuffer *buffer;
        buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_text_view_get_type()))));
        gtk_text_buffer_set_text(buffer,default_value,(-1));
      }
      gtk_text_view_set_wrap_mode(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_text_view_get_type()))),GTK_WRAP_WORD_CHAR);
      if (purple_prefs_get_bool("/pidgin/conversations/spellcheck") != 0) 
        pidgin_setup_gtkspell(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_text_view_get_type()))));
      gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),pidgin_make_scrollable(entry,GTK_POLICY_NEVER,GTK_POLICY_ALWAYS,GTK_SHADOW_IN,320,130),(!0),(!0),0);
    }
    else {
      entry = gtk_entry_new();
      gtk_entry_set_activates_default(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),(!0));
      gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),entry,0,0,0);
      if (default_value != ((const char *)((void *)0))) 
        gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),default_value);
      if (masked != 0) {
        gtk_entry_set_visibility(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),0);
#if !GTK_CHECK_VERSION(2,16,0)
#endif /* Less than GTK+ 2.16 */
      }
    }
    gtk_widget_show_all(vbox);
  }
  pidgin_set_accessible_label(entry,label);
  data -> u.input.entry = entry;
  pidgin_auto_parent_window(dialog);
/* Show everything. */
  gtk_widget_show(dialog);
  return data;
}

static void *pidgin_request_choice(const char *title,const char *primary,const char *secondary,int default_value,const char *ok_text,GCallback ok_cb,const char *cancel_text,GCallback cancel_cb,PurpleAccount *account,const char *who,PurpleConversation *conv,void *user_data,va_list args)
{
  PidginRequestData *data;
  GtkWidget *dialog;
  GtkWidget *vbox;
  GtkWidget *vbox2;
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *img;
  GtkWidget *radio = (GtkWidget *)((void *)0);
  char *label_text;
  char *radio_text;
  char *primary_esc;
  char *secondary_esc;
  data = ((PidginRequestData *)(g_malloc0_n(1,(sizeof(PidginRequestData )))));
  data -> type = PURPLE_REQUEST_ACTION;
  data -> user_data = user_data;
  data -> cb_count = 2;
  data -> cbs = ((GCallback *)(g_malloc0_n(2,(sizeof(GCallback )))));
  (data -> cbs)[0] = cancel_cb;
  (data -> cbs)[1] = ok_cb;
/* Create the dialog. */
  data -> dialog = (dialog = gtk_dialog_new());
  if (title != ((const char *)((void *)0))) 
    gtk_window_set_title(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_window_get_type()))),title);
#ifdef _WIN32
#endif
  gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),text_to_stock(cancel_text),0);
  gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),text_to_stock(ok_text),1);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dialog),((GType )(20 << 2))))),"response",((GCallback )choice_response_cb),data,0,((GConnectFlags )0));
/* Setup the dialog */
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_container_get_type()))),(12 / 2));
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type())))).vbox),gtk_container_get_type()))),(12 / 2));
  gtk_window_set_resizable(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_window_get_type()))),0);
  gtk_dialog_set_has_separator(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),0);
  gtk_box_set_spacing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type())))).vbox),gtk_box_get_type()))),12);
/* Setup the main horizontal box */
  hbox = gtk_hbox_new(0,12);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type())))).vbox),gtk_container_get_type()))),hbox);
/* Dialog icon. */
  img = gtk_image_new_from_stock("pidgin-dialog-question",gtk_icon_size_from_name("pidgin-icon-size-tango-huge"));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)img),gtk_misc_get_type()))),0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),img,0,0,0);
  pidgin_widget_decorate_account(hbox,account);
/* Vertical box */
  vbox = gtk_vbox_new(0,12);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),vbox,0,0,0);
/* Descriptive label */
  primary_esc = ((primary != ((const char *)((void *)0)))?g_markup_escape_text(primary,(-1)) : ((char *)((void *)0)));
  secondary_esc = ((secondary != ((const char *)((void *)0)))?g_markup_escape_text(secondary,(-1)) : ((char *)((void *)0)));
  label_text = g_strdup_printf((((primary != 0)?"<span weight=\"bold\" size=\"larger\">%s</span>%s%s" : "%s%s%s")),((primary != 0)?primary_esc : ""),(((primary != 0) && (secondary != 0))?"\n\n" : ""),((secondary != 0)?secondary_esc : ""));
  g_free(primary_esc);
  g_free(secondary_esc);
  label = gtk_label_new(0);
  gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),label_text);
  gtk_label_set_line_wrap(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(!0));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),label,(!0),(!0),0);
  g_free(label_text);
  vbox2 = gtk_vbox_new(0,6);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),vbox2,0,0,0);
  while((radio_text = (va_arg(args,char *))) != 0){
    int resp = va_arg(args,int );
    radio = gtk_radio_button_new_with_label_from_widget(((GtkRadioButton *)(g_type_check_instance_cast(((GTypeInstance *)radio),gtk_radio_button_get_type()))),radio_text);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox2),gtk_box_get_type()))),radio,0,0,0);
    g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)radio),((GType )(20 << 2))))),"choice_id",((gpointer )((glong )resp)));
    if (resp == default_value) 
      gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)radio),gtk_toggle_button_get_type()))),(!0));
  }
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dialog),((GType )(20 << 2))))),"radio",radio);
/* Show everything. */
  pidgin_auto_parent_window(dialog);
  gtk_widget_show_all(dialog);
  return data;
}

static void *pidgin_request_action_with_icon(const char *title,const char *primary,const char *secondary,int default_action,PurpleAccount *account,const char *who,PurpleConversation *conv,gconstpointer icon_data,gsize icon_size,void *user_data,size_t action_count,va_list actions)
{
  PidginRequestData *data;
  GtkWidget *dialog;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *img = (GtkWidget *)((void *)0);
  void **buttons;
  char *label_text;
  char *primary_esc;
  char *secondary_esc;
  int i;
  data = ((PidginRequestData *)(g_malloc0_n(1,(sizeof(PidginRequestData )))));
  data -> type = PURPLE_REQUEST_ACTION;
  data -> user_data = user_data;
  data -> cb_count = action_count;
  data -> cbs = ((GCallback *)(g_malloc0_n(action_count,(sizeof(GCallback )))));
/* Reverse the buttons */
  buttons = ((void **)(g_malloc0_n((action_count * 2),(sizeof(void *)))));
  for (i = 0; i < (action_count * 2); i += 2) {
    buttons[((action_count * 2) - i) - 2] = (va_arg(actions,char *));
    buttons[((action_count * 2) - i) - 1] = (va_arg(actions,GCallback ));
  }
/* Create the dialog. */
  data -> dialog = (dialog = gtk_dialog_new());
  gtk_window_set_deletable(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(data -> dialog)),gtk_window_get_type()))),0);
  if (title != ((const char *)((void *)0))) 
    gtk_window_set_title(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_window_get_type()))),title);
#ifdef _WIN32
#endif
  for (i = 0; i < action_count; i++) {
    gtk_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),text_to_stock(buttons[2 * i]),i);
    (data -> cbs)[i] = buttons[(2 * i) + 1];
  }
  g_free(buttons);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dialog),((GType )(20 << 2))))),"response",((GCallback )action_response_cb),data,0,((GConnectFlags )0));
/* Setup the dialog */
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_container_get_type()))),(12 / 2));
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type())))).vbox),gtk_container_get_type()))),(12 / 2));
  gtk_window_set_resizable(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_window_get_type()))),0);
  gtk_dialog_set_has_separator(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),0);
  gtk_box_set_spacing(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type())))).vbox),gtk_box_get_type()))),12);
/* Setup the main horizontal box */
  hbox = gtk_hbox_new(0,12);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type())))).vbox),gtk_container_get_type()))),hbox);
/* Dialog icon. */
  if (icon_data != 0) {
    GdkPixbuf *pixbuf = pidgin_pixbuf_from_data(icon_data,icon_size);
    if (pixbuf != 0) {
/* scale the image if it is too large */
      int width = gdk_pixbuf_get_width(pixbuf);
      int height = gdk_pixbuf_get_height(pixbuf);
      if ((width > 128) || (height > 128)) {
        int scaled_width = (width > height)?128 : ((128 * width) / height);
        int scaled_height = (height > width)?128 : ((128 * height) / width);
        GdkPixbuf *scaled = gdk_pixbuf_scale_simple(pixbuf,scaled_width,scaled_height,GDK_INTERP_BILINEAR);
        purple_debug_info("pidgin","dialog icon was too large, scaled it down\n");
        if (scaled != 0) {
          g_object_unref(pixbuf);
          pixbuf = scaled;
        }
      }
      img = gtk_image_new_from_pixbuf(pixbuf);
      g_object_unref(pixbuf);
    }
    else {
      purple_debug_info("pidgin","failed to parse dialog icon\n");
    }
  }
  if (!(img != 0)) {
    img = gtk_image_new_from_stock("pidgin-dialog-question",gtk_icon_size_from_name("pidgin-icon-size-tango-huge"));
  }
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)img),gtk_misc_get_type()))),0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),img,0,0,0);
/* Vertical box */
  vbox = gtk_vbox_new(0,12);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),vbox,0,0,0);
  pidgin_widget_decorate_account(hbox,account);
/* Descriptive label */
  primary_esc = ((primary != ((const char *)((void *)0)))?g_markup_escape_text(primary,(-1)) : ((char *)((void *)0)));
  secondary_esc = ((secondary != ((const char *)((void *)0)))?g_markup_escape_text(secondary,(-1)) : ((char *)((void *)0)));
  label_text = g_strdup_printf((((primary != 0)?"<span weight=\"bold\" size=\"larger\">%s</span>%s%s" : "%s%s%s")),((primary != 0)?primary_esc : ""),(((primary != 0) && (secondary != 0))?"\n\n" : ""),((secondary != 0)?secondary_esc : ""));
  g_free(primary_esc);
  g_free(secondary_esc);
  label = gtk_label_new(0);
  gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),label_text);
  gtk_label_set_line_wrap(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(!0));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0);
  gtk_label_set_selectable(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(!0));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),label,(!0),(!0),0);
  g_free(label_text);
  if (default_action == -1) {
    do {
      ( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)img),gtk_object_get_type())))).flags |= GTK_CAN_DEFAULT;
    }while (0);
    do {
      ( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)img),gtk_object_get_type())))).flags |= GTK_CAN_FOCUS;
    }while (0);
    gtk_widget_grab_focus(img);
    gtk_widget_grab_default(img);
  }
  else 
/*
		 * Need to invert the default_action number because the
		 * buttons are added to the dialog in reverse order.
		 */
    gtk_dialog_set_default_response(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),((action_count - 1) - default_action));
/* Show everything. */
  pidgin_auto_parent_window(dialog);
  gtk_widget_show_all(dialog);
  return data;
}

static void *pidgin_request_action(const char *title,const char *primary,const char *secondary,int default_action,PurpleAccount *account,const char *who,PurpleConversation *conv,void *user_data,size_t action_count,va_list actions)
{
  return pidgin_request_action_with_icon(title,primary,secondary,default_action,account,who,conv,0,0,user_data,action_count,actions);
}

static void req_entry_field_changed_cb(GtkWidget *entry,PurpleRequestField *field)
{
  PurpleRequestFieldGroup *group;
  PidginRequestData *req_data;
  if (purple_request_field_string_is_multiline(field) != 0) {
    char *text;
    GtkTextIter start_iter;
    GtkTextIter end_iter;
    gtk_text_buffer_get_start_iter(((GtkTextBuffer *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_text_buffer_get_type()))),&start_iter);
    gtk_text_buffer_get_end_iter(((GtkTextBuffer *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_text_buffer_get_type()))),&end_iter);
    text = gtk_text_buffer_get_text(((GtkTextBuffer *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_text_buffer_get_type()))),(&start_iter),(&end_iter),0);
    purple_request_field_string_set_value(field,((!(text != 0) || !(( *text) != 0))?((char *)((void *)0)) : text));
    g_free(text);
  }
  else {
    const char *text = (const char *)((void *)0);
    text = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))));
    purple_request_field_string_set_value(field,((( *text) == 0)?((const char *)((void *)0)) : text));
  }
  group = purple_request_field_get_group(field);
  req_data = ((PidginRequestData *)( *(group -> fields_list)).ui_data);
  gtk_widget_set_sensitive((req_data -> ok_button),purple_request_fields_all_required_filled((group -> fields_list)));
}

static void setup_entry_field(GtkWidget *entry,PurpleRequestField *field)
{
  const char *type_hint;
  gtk_entry_set_activates_default(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),(!0));
  if (purple_request_field_is_required(field) != 0) {
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"changed",((GCallback )req_entry_field_changed_cb),field,0,((GConnectFlags )0));
  }
  if ((type_hint = purple_request_field_get_type_hint(field)) != ((const char *)((void *)0))) {
    if (purple_str_has_prefix(type_hint,"screenname") != 0) {
      GtkWidget *optmenu = (GtkWidget *)((void *)0);
      PurpleRequestFieldGroup *group = purple_request_field_get_group(field);
      GList *fields = (group -> fields);
{
/* Ensure the account option menu is created (if the widget hasn't
			 * been initialized already) for username auto-completion. */
        while(fields != 0){
          PurpleRequestField *fld = (fields -> data);
          fields = (fields -> next);
          if (((purple_request_field_get_type(fld)) == PURPLE_REQUEST_FIELD_ACCOUNT) && (purple_request_field_is_visible(fld) != 0)) {
            const char *type_hint = purple_request_field_get_type_hint(fld);
            if ((type_hint != ((const char *)((void *)0))) && (strcmp(type_hint,"account") == 0)) {
              optmenu = ((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(purple_request_field_get_ui_data(fld))),gtk_widget_get_type())));
              if (optmenu == ((GtkWidget *)((void *)0))) {
                optmenu = ((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(create_account_field(fld))),gtk_widget_get_type())));
                purple_request_field_set_ui_data(fld,optmenu);
              }
              break; 
            }
          }
        }
      }
      pidgin_setup_screenname_autocomplete_with_filter(entry,optmenu,pidgin_screenname_autocomplete_default_filter,((gpointer )((glong )(!(strcmp(type_hint,"screenname-all") != 0)))));
    }
  }
}

static GtkWidget *create_string_field(PurpleRequestField *field)
{
  const char *value;
  GtkWidget *widget;
  value = purple_request_field_string_get_default_value(field);
  if (purple_request_field_string_is_multiline(field) != 0) {
    GtkWidget *textview;
    textview = gtk_text_view_new();
    gtk_text_view_set_editable(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)textview),gtk_text_view_get_type()))),(!0));
    gtk_text_view_set_wrap_mode(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)textview),gtk_text_view_get_type()))),GTK_WRAP_WORD_CHAR);
    if (purple_prefs_get_bool("/pidgin/conversations/spellcheck") != 0) 
      pidgin_setup_gtkspell(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)textview),gtk_text_view_get_type()))));
    gtk_widget_show(textview);
    if (value != ((const char *)((void *)0))) {
      GtkTextBuffer *buffer;
      buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)textview),gtk_text_view_get_type()))));
      gtk_text_buffer_set_text(buffer,value,(-1));
    }
    gtk_text_view_set_editable(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)textview),gtk_text_view_get_type()))),purple_request_field_string_is_editable(field));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)textview),((GType )(20 << 2))))),"focus-out-event",((GCallback )field_string_focus_out_cb),field,0,((GConnectFlags )0));
    if (purple_request_field_is_required(field) != 0) {
      GtkTextBuffer *buffer = gtk_text_view_get_buffer(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)textview),gtk_text_view_get_type()))));
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buffer),((GType )(20 << 2))))),"changed",((GCallback )req_entry_field_changed_cb),field,0,((GConnectFlags )0));
    }
    widget = pidgin_make_scrollable(textview,GTK_POLICY_NEVER,GTK_POLICY_ALWAYS,GTK_SHADOW_IN,-1,75);
  }
  else {
    widget = gtk_entry_new();
    setup_entry_field(widget,field);
    if (value != ((const char *)((void *)0))) 
      gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_entry_get_type()))),value);
    if (purple_request_field_string_is_masked(field) != 0) {
      gtk_entry_set_visibility(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_entry_get_type()))),0);
#if !GTK_CHECK_VERSION(2,16,0)
#endif /* Less than GTK+ 2.16 */
    }
    gtk_editable_set_editable(((GtkEditable *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_editable_get_type()))),purple_request_field_string_is_editable(field));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"focus-out-event",((GCallback )field_string_focus_out_cb),field,0,((GConnectFlags )0));
  }
  return widget;
}

static GtkWidget *create_int_field(PurpleRequestField *field)
{
  int value;
  GtkWidget *widget;
  widget = gtk_entry_new();
  setup_entry_field(widget,field);
  value = purple_request_field_int_get_default_value(field);
  if (value != 0) {
    char buf[32UL];
    g_snprintf(buf,(sizeof(buf)),"%d",value);
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_entry_get_type()))),buf);
  }
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"focus-out-event",((GCallback )field_int_focus_out_cb),field,0,((GConnectFlags )0));
  return widget;
}

static GtkWidget *create_bool_field(PurpleRequestField *field)
{
  GtkWidget *widget;
  widget = gtk_check_button_new_with_label(purple_request_field_get_label(field));
  gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_toggle_button_get_type()))),purple_request_field_bool_get_default_value(field));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"toggled",((GCallback )field_bool_cb),field,0,((GConnectFlags )0));
  return widget;
}

static GtkWidget *create_choice_field(PurpleRequestField *field)
{
  GtkWidget *widget;
  GList *labels = purple_request_field_choice_get_labels(field);
  int num_labels = (g_list_length(labels));
  GList *l;
  if (num_labels > 5) {
    widget = gtk_combo_box_new_text();
    for (l = labels; l != ((GList *)((void *)0)); l = (l -> next)) {
      const char *text = (l -> data);
      gtk_combo_box_append_text(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_combo_box_get_type()))),text);
    }
    gtk_combo_box_set_active(((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_combo_box_get_type()))),purple_request_field_choice_get_default_value(field));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)widget),((GType )(20 << 2))))),"changed",((GCallback )field_choice_menu_cb),field,0,((GConnectFlags )0));
  }
  else {
    GtkWidget *box;
    GtkWidget *first_radio = (GtkWidget *)((void *)0);
    GtkWidget *radio;
    gint i;
    if (num_labels == 2) 
      box = gtk_hbox_new(0,6);
    else 
      box = gtk_vbox_new(0,0);
    widget = box;
    for (((l = labels) , (i = 0)); l != ((GList *)((void *)0)); ((l = (l -> next)) , i++)) {
      const char *text = (l -> data);
      radio = gtk_radio_button_new_with_label_from_widget(((GtkRadioButton *)(g_type_check_instance_cast(((GTypeInstance *)first_radio),gtk_radio_button_get_type()))),text);
      if (first_radio == ((GtkWidget *)((void *)0))) 
        first_radio = radio;
      if (i == purple_request_field_choice_get_default_value(field)) 
        gtk_toggle_button_set_active(((GtkToggleButton *)(g_type_check_instance_cast(((GTypeInstance *)radio),gtk_toggle_button_get_type()))),(!0));
      gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)box),gtk_box_get_type()))),radio,(!0),(!0),0);
      gtk_widget_show(radio);
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)radio),((GType )(20 << 2))))),"toggled",((GCallback )field_choice_option_cb),field,0,((GConnectFlags )0));
    }
  }
  return widget;
}

static GtkWidget *create_image_field(PurpleRequestField *field)
{
  GtkWidget *widget;
  GdkPixbuf *buf;
  GdkPixbuf *scale;
  buf = pidgin_pixbuf_from_data(((const guchar *)(purple_request_field_image_get_buffer(field))),purple_request_field_image_get_size(field));
  scale = gdk_pixbuf_scale_simple(buf,(purple_request_field_image_get_scale_x(field) * (gdk_pixbuf_get_width(buf))),(purple_request_field_image_get_scale_y(field) * (gdk_pixbuf_get_height(buf))),GDK_INTERP_BILINEAR);
  widget = gtk_image_new_from_pixbuf(scale);
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)buf),((GType )(20 << 2))))));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)scale),((GType )(20 << 2))))));
  return widget;
}

static GtkWidget *create_account_field(PurpleRequestField *field)
{
  GtkWidget *widget;
  widget = pidgin_account_option_menu_new(purple_request_field_account_get_default_value(field),purple_request_field_account_get_show_all(field),((GCallback )field_account_cb),purple_request_field_account_get_filter(field),field);
  return widget;
}

static void select_field_list_item(GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter,gpointer data)
{
  PurpleRequestField *field = (PurpleRequestField *)data;
  char *text;
  gtk_tree_model_get(model,iter,1,&text,-1);
  purple_request_field_list_add_selected(field,text);
  g_free(text);
}

static void list_field_select_changed_cb(GtkTreeSelection *sel,PurpleRequestField *field)
{
  purple_request_field_list_clear_selected(field);
  gtk_tree_selection_selected_foreach(sel,select_field_list_item,field);
}

static GtkWidget *create_list_field(PurpleRequestField *field)
{
  GtkWidget *treeview;
  GtkListStore *store;
  GtkCellRenderer *renderer;
  GtkTreeSelection *sel;
  GtkTreeViewColumn *column;
  GtkTreeIter iter;
  GList *l;
  GList *icons = (GList *)((void *)0);
  icons = purple_request_field_list_get_icons(field);
/* Create the list store */
  if (icons != 0) 
    store = gtk_list_store_new(3,((GType )(17 << 2)),((GType )(16 << 2)),gdk_pixbuf_get_type());
  else 
    store = gtk_list_store_new(2,((GType )(17 << 2)),((GType )(16 << 2)));
/* Create the tree view */
  treeview = gtk_tree_view_new_with_model(((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)store),gtk_tree_model_get_type()))));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)store),((GType )(20 << 2))))));
  gtk_tree_view_set_headers_visible(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),0);
  sel = gtk_tree_view_get_selection(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))));
  if (purple_request_field_list_get_multi_select(field) != 0) 
    gtk_tree_selection_set_mode(sel,GTK_SELECTION_MULTIPLE);
  column = gtk_tree_view_column_new();
  gtk_tree_view_insert_column(((GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)treeview),gtk_tree_view_get_type()))),column,(-1));
  renderer = gtk_cell_renderer_text_new();
  gtk_tree_view_column_pack_start(column,renderer,(!0));
  gtk_tree_view_column_add_attribute(column,renderer,"text",1);
  if (icons != 0) {
    renderer = gtk_cell_renderer_pixbuf_new();
    gtk_tree_view_column_pack_start(column,renderer,(!0));
    gtk_tree_view_column_add_attribute(column,renderer,"pixbuf",2);
    gtk_widget_set_size_request(treeview,200,400);
  }
  for (l = purple_request_field_list_get_items(field); l != ((GList *)((void *)0)); l = (l -> next)) {
    const char *text = (const char *)(l -> data);
    gtk_list_store_append(store,&iter);
    if (icons != 0) {
      const char *icon_path = (const char *)(icons -> data);
      GdkPixbuf *pixbuf = (GdkPixbuf *)((void *)0);
      if (icon_path != 0) 
        pixbuf = pidgin_pixbuf_new_from_file(icon_path);
      gtk_list_store_set(store,&iter,0,purple_request_field_list_get_data(field,text),1,text,2,pixbuf,-1);
      icons = (icons -> next);
    }
    else 
      gtk_list_store_set(store,&iter,0,purple_request_field_list_get_data(field,text),1,text,-1);
    if (purple_request_field_list_is_selected(field,text) != 0) 
      gtk_tree_selection_select_iter(sel,&iter);
  }
/*
	 * We only want to catch changes made by the user, so it's important
	 * that we wait until after the list is created to connect this
	 * handler.  If we connect the handler before the loop above and
	 * there are multiple items selected, then selecting the first iter
	 * in the tree causes list_field_select_changed_cb to be triggered
	 * which clears out the rest of the list of selected items.
	 */
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)sel),((GType )(20 << 2))))),"changed",((GCallback )list_field_select_changed_cb),field,0,((GConnectFlags )0));
  gtk_widget_show(treeview);
  return pidgin_make_scrollable(treeview,GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC,GTK_SHADOW_IN,-1,-1);
}

static void *pidgin_request_fields(const char *title,const char *primary,const char *secondary,PurpleRequestFields *fields,const char *ok_text,GCallback ok_cb,const char *cancel_text,GCallback cancel_cb,PurpleAccount *account,const char *who,PurpleConversation *conv,void *user_data)
{
  PidginRequestData *data;
  GtkWidget *win;
  GtkWidget *vbox;
  GtkWidget *vbox2;
  GtkWidget *hbox;
  GtkWidget *frame;
  GtkWidget *label;
  GtkWidget *table;
  GtkWidget *button;
  GtkWidget *img;
  GtkSizeGroup *sg;
  GList *gl;
  GList *fl;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  char *label_text;
  char *primary_esc;
  char *secondary_esc;
  int total_fields = 0;
  data = ((PidginRequestData *)(g_malloc0_n(1,(sizeof(PidginRequestData )))));
  data -> type = PURPLE_REQUEST_FIELDS;
  data -> user_data = user_data;
  data -> u.multifield.fields = fields;
  fields -> ui_data = data;
  data -> cb_count = 2;
  data -> cbs = ((GCallback *)(g_malloc0_n(2,(sizeof(GCallback )))));
  (data -> cbs)[0] = ok_cb;
  (data -> cbs)[1] = cancel_cb;
#ifdef _WIN32
#else /* !_WIN32 */
  data -> dialog = (win = pidgin_create_dialog(title,12,"multifield",(!0)));
#endif /* _WIN32 */
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)win),((GType )(20 << 2))))),"delete_event",((GCallback )destroy_multifield_cb),data,0,((GConnectFlags )0));
/* Setup the main horizontal box */
  hbox = gtk_hbox_new(0,12);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)(pidgin_dialog_get_vbox(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type())))))),gtk_container_get_type()))),hbox);
  gtk_widget_show(hbox);
/* Dialog icon. */
  img = gtk_image_new_from_stock("pidgin-dialog-question",gtk_icon_size_from_name("pidgin-icon-size-tango-huge"));
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)img),gtk_misc_get_type()))),0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),img,0,0,0);
  gtk_widget_show(img);
/* Cancel button */
  button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),text_to_stock(cancel_text),((GCallback )multifield_cancel_cb),data);
  do {
    ( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_object_get_type())))).flags |= GTK_CAN_DEFAULT;
  }while (0);
/* OK button */
  button = pidgin_dialog_add_button(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_dialog_get_type()))),text_to_stock(ok_text),((GCallback )multifield_ok_cb),data);
  data -> ok_button = button;
  do {
    ( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_object_get_type())))).flags |= GTK_CAN_DEFAULT;
  }while (0);
  gtk_window_set_default(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_window_get_type()))),button);
  pidgin_widget_decorate_account(hbox,account);
/* Setup the vbox */
  vbox = gtk_vbox_new(0,12);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),vbox,(!0),(!0),0);
  gtk_widget_show(vbox);
  sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  if (primary != 0) {
    primary_esc = g_markup_escape_text(primary,(-1));
    label_text = g_strdup_printf("<span weight=\"bold\" size=\"larger\">%s</span>",primary_esc);
    g_free(primary_esc);
    label = gtk_label_new(0);
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),label_text);
    gtk_label_set_line_wrap(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(!0));
    gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),label,0,0,0);
    gtk_widget_show(label);
    g_free(label_text);
  }
  for (gl = purple_request_fields_get_groups(fields); gl != ((GList *)((void *)0)); gl = (gl -> next)) 
    total_fields += g_list_length(purple_request_field_group_get_fields((gl -> data)));
  if (total_fields > 9) {
    GtkWidget *hbox_for_spacing;
    GtkWidget *vbox_for_spacing;
    hbox_for_spacing = gtk_hbox_new(0,12);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),pidgin_make_scrollable(hbox_for_spacing,GTK_POLICY_NEVER,GTK_POLICY_AUTOMATIC,GTK_SHADOW_NONE,-1,200),(!0),(!0),0);
    gtk_widget_show(hbox_for_spacing);
    vbox_for_spacing = gtk_vbox_new(0,12);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox_for_spacing),gtk_box_get_type()))),vbox_for_spacing,(!0),(!0),6);
    gtk_widget_show(vbox_for_spacing);
    vbox2 = gtk_vbox_new(0,12);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox_for_spacing),gtk_box_get_type()))),vbox2,(!0),(!0),6);
    gtk_widget_show(vbox2);
  }
  else {
    vbox2 = vbox;
  }
  if (secondary != 0) {
    secondary_esc = g_markup_escape_text(secondary,(-1));
    label = gtk_label_new(0);
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),secondary_esc);
    g_free(secondary_esc);
    gtk_label_set_line_wrap(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),(!0));
    gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox2),gtk_box_get_type()))),label,(!0),(!0),0);
    gtk_widget_show(label);
  }
  for (gl = purple_request_fields_get_groups(fields); gl != ((GList *)((void *)0)); gl = (gl -> next)) {
    GList *field_list;
    size_t field_count = 0;
    size_t cols = 1;
    size_t rows;
    size_t col_num;
    size_t row_num = 0;
    group = (gl -> data);
    field_list = purple_request_field_group_get_fields(group);
    if (purple_request_field_group_get_title(group) != ((const char *)((void *)0))) {
      frame = pidgin_make_frame(vbox2,purple_request_field_group_get_title(group));
    }
    else 
      frame = vbox2;
    field_count = (g_list_length(field_list));
/*
		if (field_count > 9)
		{
			rows = field_count / 2;
			cols++;
		}
		else
		*/
    rows = field_count;
    col_num = 0;
    for (fl = field_list; fl != ((GList *)((void *)0)); fl = (fl -> next)) {
      PurpleRequestFieldType type;
      field = ((PurpleRequestField *)(fl -> data));
      type = purple_request_field_get_type(field);
      if (type == PURPLE_REQUEST_FIELD_LABEL) {
        if (col_num > 0) 
          rows++;
        rows++;
      }
      else if ((type == PURPLE_REQUEST_FIELD_LIST) || ((type == PURPLE_REQUEST_FIELD_STRING) && (purple_request_field_string_is_multiline(field) != 0))) {
        if (col_num > 0) 
          rows++;
        rows += 2;
      }
      col_num++;
      if (col_num >= cols) 
        col_num = 0;
    }
    table = gtk_table_new(rows,(2 * cols),0);
    gtk_table_set_row_spacings(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),6);
    gtk_table_set_col_spacings(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),6);
    gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)frame),gtk_container_get_type()))),table);
    gtk_widget_show(table);
    for (((row_num = 0) , (fl = field_list)); (row_num < rows) && (fl != ((GList *)((void *)0))); row_num++) {
      for (col_num = 0; (col_num < cols) && (fl != ((GList *)((void *)0))); (col_num++ , (fl = (fl -> next)))) {{
          size_t col_offset = (col_num * 2);
          PurpleRequestFieldType type;
          GtkWidget *widget = (GtkWidget *)((void *)0);
          const char *field_label;
          label = ((GtkWidget *)((void *)0));
          field = (fl -> data);
          if (!(purple_request_field_is_visible(field) != 0)) {
            col_num--;
            continue; 
          }
          type = purple_request_field_get_type(field);
          field_label = purple_request_field_get_label(field);
          if ((type != PURPLE_REQUEST_FIELD_BOOLEAN) && (field_label != 0)) {
            char *text = (char *)((void *)0);
            if (field_label[strlen(field_label) - 1] != ':') 
              text = g_strdup_printf("%s:",field_label);
            label = gtk_label_new(0);
            gtk_label_set_markup_with_mnemonic(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),((text != 0)?text : field_label));
            g_free(text);
            gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
            gtk_size_group_add_widget(sg,label);
            if (((type == PURPLE_REQUEST_FIELD_LABEL) || (type == PURPLE_REQUEST_FIELD_LIST)) || ((type == PURPLE_REQUEST_FIELD_STRING) && (purple_request_field_string_is_multiline(field) != 0))) {
              if (col_num > 0) 
                row_num++;
              gtk_table_attach_defaults(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),label,0,(2 * cols),row_num,(row_num + 1));
              row_num++;
              col_num = cols;
            }
            else {
              gtk_table_attach_defaults(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),label,col_offset,(col_offset + 1),row_num,(row_num + 1));
            }
            gtk_widget_show(label);
          }
          widget = ((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(purple_request_field_get_ui_data(field))),gtk_widget_get_type())));
          if (widget == ((GtkWidget *)((void *)0))) {
            if (type == PURPLE_REQUEST_FIELD_STRING) 
              widget = create_string_field(field);
            else if (type == PURPLE_REQUEST_FIELD_INTEGER) 
              widget = create_int_field(field);
            else if (type == PURPLE_REQUEST_FIELD_BOOLEAN) 
              widget = create_bool_field(field);
            else if (type == PURPLE_REQUEST_FIELD_CHOICE) 
              widget = create_choice_field(field);
            else if (type == PURPLE_REQUEST_FIELD_LIST) 
              widget = create_list_field(field);
            else if (type == PURPLE_REQUEST_FIELD_IMAGE) 
              widget = create_image_field(field);
            else if (type == PURPLE_REQUEST_FIELD_ACCOUNT) 
              widget = create_account_field(field);
            else 
              continue; 
          }
          if (label != 0) 
            gtk_label_set_mnemonic_widget(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),widget);
          if ((type == PURPLE_REQUEST_FIELD_STRING) && (purple_request_field_string_is_multiline(field) != 0)) {
            gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),widget,0,(2 * cols),row_num,(row_num + 1),(GTK_FILL | GTK_EXPAND),(GTK_FILL | GTK_EXPAND),5,0);
          }
          else if (type == PURPLE_REQUEST_FIELD_LIST) {
            gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),widget,0,(2 * cols),row_num,(row_num + 1),(GTK_FILL | GTK_EXPAND),(GTK_FILL | GTK_EXPAND),5,0);
          }
          else if (type == PURPLE_REQUEST_FIELD_BOOLEAN) {
            gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),widget,col_offset,(col_offset + 1),row_num,(row_num + 1),(GTK_FILL | GTK_EXPAND),(GTK_FILL | GTK_EXPAND),5,0);
          }
          else {
            gtk_table_attach(((GtkTable *)(g_type_check_instance_cast(((GTypeInstance *)table),gtk_table_get_type()))),widget,1,(2 * cols),row_num,(row_num + 1),(GTK_FILL | GTK_EXPAND),(GTK_FILL | GTK_EXPAND),5,0);
          }
          gtk_widget_show(widget);
          purple_request_field_set_ui_data(field,widget);
        }
      }
    }
  }
  g_object_unref(sg);
  if (!(purple_request_fields_all_required_filled(fields) != 0)) 
    gtk_widget_set_sensitive((data -> ok_button),0);
  pidgin_auto_parent_window(win);
  gtk_widget_show(win);
  return data;
}

static void file_yes_no_cb(PidginRequestData *data,gint id)
{
/* Only call the callback if yes was selected, otherwise the request
	 * (eg. file transfer) will be cancelled, then when a new filename is chosen
	 * things go BOOM */
  if (id == 1) {
    if ((data -> cbs)[1] != ((void (*)())((void *)0))) 
      ( *((PurpleRequestFileCb )(data -> cbs)[1]))((data -> user_data),data -> u.file.name);
    purple_request_close((data -> type),data);
  }
  else {
    pidgin_clear_cursor(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(data -> dialog)),gtk_widget_get_type()))));
  }
}

static void file_ok_check_if_exists_cb(GtkWidget *widget,gint response,PidginRequestData *data)
{
  gchar *current_folder;
  generic_response_start(data);
  if (response != GTK_RESPONSE_ACCEPT) {
    if ((data -> cbs)[0] != ((void (*)())((void *)0))) 
      ( *((PurpleRequestFileCb )(data -> cbs)[0]))((data -> user_data),0);
    purple_request_close((data -> type),data);
    return ;
  }
  data -> u.file.name = gtk_file_chooser_get_filename(((GtkFileChooser *)(g_type_check_instance_cast(((GTypeInstance *)(data -> dialog)),gtk_file_chooser_get_type()))));
  current_folder = gtk_file_chooser_get_current_folder(((GtkFileChooser *)(g_type_check_instance_cast(((GTypeInstance *)(data -> dialog)),gtk_file_chooser_get_type()))));
  if (current_folder != ((gchar *)((void *)0))) {
    if (data -> u.file.savedialog != 0) {
      purple_prefs_set_path("/pidgin/filelocations/last_save_folder",current_folder);
    }
    else {
      purple_prefs_set_path("/pidgin/filelocations/last_open_folder",current_folder);
    }
    g_free(current_folder);
  }
  if ((data -> u.file.savedialog == !0) && (g_file_test(data -> u.file.name,G_FILE_TEST_EXISTS) != 0)) {
    purple_request_action(data,0,((const char *)(dgettext("pidgin","That file already exists"))),((const char *)(dgettext("pidgin","Would you like to overwrite it\?"))),0,0,0,0,data,2,((const char *)(dgettext("pidgin","Overwrite"))),((GCallback )file_yes_no_cb),((const char *)(dgettext("pidgin","Choose New Name"))),((GCallback )file_yes_no_cb));
  }
  else 
    file_yes_no_cb(data,1);
}

static void *pidgin_request_file(const char *title,const char *filename,gboolean savedialog,GCallback ok_cb,GCallback cancel_cb,PurpleAccount *account,const char *who,PurpleConversation *conv,void *user_data)
{
  PidginRequestData *data;
  GtkWidget *filesel;
  const gchar *current_folder;
  gboolean folder_set = 0;
  data = ((PidginRequestData *)(g_malloc0_n(1,(sizeof(PidginRequestData )))));
  data -> type = PURPLE_REQUEST_FILE;
  data -> user_data = user_data;
  data -> cb_count = 2;
  data -> cbs = ((GCallback *)(g_malloc0_n(2,(sizeof(GCallback )))));
  (data -> cbs)[0] = cancel_cb;
  (data -> cbs)[1] = ok_cb;
  data -> u.file.savedialog = savedialog;
  filesel = gtk_file_chooser_dialog_new(((title != 0)?title : (((savedialog != 0)?((const char *)(dgettext("pidgin","Save File..."))) : ((const char *)(dgettext("pidgin","Open File...")))))),0,(((savedialog != 0)?GTK_FILE_CHOOSER_ACTION_SAVE : GTK_FILE_CHOOSER_ACTION_OPEN)),"gtk-cancel",GTK_RESPONSE_CANCEL,((savedialog != 0)?"gtk-save" : "gtk-open"),GTK_RESPONSE_ACCEPT,((void *)((void *)0)));
  gtk_dialog_set_default_response(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)filesel),gtk_dialog_get_type()))),GTK_RESPONSE_ACCEPT);
  if (savedialog != 0) {
    current_folder = purple_prefs_get_path("/pidgin/filelocations/last_save_folder");
  }
  else {
    current_folder = purple_prefs_get_path("/pidgin/filelocations/last_open_folder");
  }
  if ((filename != ((const char *)((void *)0))) && (( *filename) != 0)) {
    if (savedialog != 0) 
      gtk_file_chooser_set_current_name(((GtkFileChooser *)(g_type_check_instance_cast(((GTypeInstance *)filesel),gtk_file_chooser_get_type()))),filename);
    else if (g_file_test(filename,G_FILE_TEST_EXISTS) != 0) 
      gtk_file_chooser_set_filename(((GtkFileChooser *)(g_type_check_instance_cast(((GTypeInstance *)filesel),gtk_file_chooser_get_type()))),filename);
  }
  if (((((filename == ((const char *)((void *)0))) || (( *filename) == 0)) || !(g_file_test(filename,G_FILE_TEST_EXISTS) != 0)) && (current_folder != ((const gchar *)((void *)0)))) && (( *current_folder) != 0)) {
    folder_set = gtk_file_chooser_set_current_folder(((GtkFileChooser *)(g_type_check_instance_cast(((GTypeInstance *)filesel),gtk_file_chooser_get_type()))),current_folder);
  }
#ifdef _WIN32
#endif
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)((GtkFileChooser *)(g_type_check_instance_cast(((GTypeInstance *)filesel),gtk_file_chooser_get_type())))),((GType )(20 << 2))))),"response",((GCallback )file_ok_check_if_exists_cb),data,0,((GConnectFlags )0));
  pidgin_auto_parent_window(filesel);
  data -> dialog = filesel;
  gtk_widget_show(filesel);
  return (void *)data;
}

static void *pidgin_request_folder(const char *title,const char *dirname,GCallback ok_cb,GCallback cancel_cb,PurpleAccount *account,const char *who,PurpleConversation *conv,void *user_data)
{
  PidginRequestData *data;
  GtkWidget *dirsel;
  data = ((PidginRequestData *)(g_malloc0_n(1,(sizeof(PidginRequestData )))));
  data -> type = PURPLE_REQUEST_FOLDER;
  data -> user_data = user_data;
  data -> cb_count = 2;
  data -> cbs = ((GCallback *)(g_malloc0_n(2,(sizeof(GCallback )))));
  (data -> cbs)[0] = cancel_cb;
  (data -> cbs)[1] = ok_cb;
  data -> u.file.savedialog = 0;
  dirsel = gtk_file_chooser_dialog_new(((title != 0)?title : ((const char *)(dgettext("pidgin","Select Folder...")))),0,GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,"gtk-cancel",GTK_RESPONSE_CANCEL,"gtk-ok",GTK_RESPONSE_ACCEPT,((void *)((void *)0)));
  gtk_dialog_set_default_response(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dirsel),gtk_dialog_get_type()))),GTK_RESPONSE_ACCEPT);
  if ((dirname != ((const char *)((void *)0))) && (( *dirname) != 0)) 
    gtk_file_chooser_set_current_folder(((GtkFileChooser *)(g_type_check_instance_cast(((GTypeInstance *)dirsel),gtk_file_chooser_get_type()))),dirname);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)((GtkFileChooser *)(g_type_check_instance_cast(((GTypeInstance *)dirsel),gtk_file_chooser_get_type())))),((GType )(20 << 2))))),"response",((GCallback )file_ok_check_if_exists_cb),data,0,((GConnectFlags )0));
  data -> dialog = dirsel;
  pidgin_auto_parent_window(dirsel);
  gtk_widget_show(dirsel);
  return (void *)data;
}

static void pidgin_close_request(PurpleRequestType type,void *ui_handle)
{
  PidginRequestData *data = (PidginRequestData *)ui_handle;
  g_free((data -> cbs));
  gtk_widget_destroy((data -> dialog));
  if (type == PURPLE_REQUEST_FIELDS) 
    purple_request_fields_destroy(data -> u.multifield.fields);
  else if (type == PURPLE_REQUEST_FILE) 
    g_free(data -> u.file.name);
  g_free(data);
}
static PurpleRequestUiOps ops = {(pidgin_request_input), (pidgin_request_choice), (pidgin_request_action), (pidgin_request_fields), (pidgin_request_file), (pidgin_close_request), (pidgin_request_folder), (pidgin_request_action_with_icon), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))};

PurpleRequestUiOps *pidgin_request_get_ui_ops()
{
  return &ops;
}
