/**
 * @file gtkutils.c GTK+ utility functions
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#define _PIDGIN_GTKUTILS_C_
#include "internal.h"
#include "pidgin.h"
#ifdef _WIN32
# ifdef small
#  undef small
# endif
#endif /*_WIN32*/
#ifdef USE_GTKSPELL
# include <gtkspell/gtkspell.h>
# ifdef _WIN32
#  include "wspell.h"
# endif
#endif
#include <gdk/gdkkeysyms.h>
#include "conversation.h"
#include "debug.h"
#include "desktopitem.h"
#include "imgstore.h"
#include "notify.h"
#include "prefs.h"
#include "prpl.h"
#include "request.h"
#include "signals.h"
#include "sound.h"
#include "util.h"
#include "gtkaccount.h"
#include "gtkprefs.h"
#include "gtkconv.h"
#include "gtkdialogs.h"
#include "gtkimhtml.h"
#include "gtkimhtmltoolbar.h"
#include "pidginstock.h"
#include "gtkthemes.h"
#include "gtkutils.h"
#include "pidgin/minidialog.h"
typedef struct __unnamed_class___F0_L70_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__menu__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_ginti__typedef_declaration_variable_name_unknown_scope_and_name__scope__default_item {
GtkWidget *menu;
gint default_item;}AopMenu;
static guint accels_save_timer = 0;
static GSList *registered_url_handlers = (GSList *)((void *)0);

static gboolean url_clicked_idle_cb(gpointer data)
{
  purple_notify_uri(0,data);
  g_free(data);
  return 0;
}

static gboolean url_clicked_cb(GtkIMHtml *unused,GtkIMHtmlLink *link)
{
  const char *uri = gtk_imhtml_link_get_url(link);
  g_idle_add(url_clicked_idle_cb,(g_strdup(uri)));
  return (!0);
}
static GtkIMHtmlFuncs gtkimhtml_cbs = {((GtkIMHtmlGetImageFunc )purple_imgstore_find_by_id), ((GtkIMHtmlGetImageDataFunc )purple_imgstore_get_data), ((GtkIMHtmlGetImageSizeFunc )purple_imgstore_get_size), ((GtkIMHtmlGetImageFilenameFunc )purple_imgstore_get_filename), (purple_imgstore_ref_by_id), (purple_imgstore_unref_by_id)};

void pidgin_setup_imhtml(GtkWidget *imhtml)
{
  do {
    if (imhtml != ((GtkWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"imhtml != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)imhtml;
      GType __t = gtk_imhtml_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_IMHTML(imhtml)");
      return ;
    };
  }while (0);
  pidgin_themes_smiley_themeize(imhtml);
  gtk_imhtml_set_funcs(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),&gtkimhtml_cbs);
#ifdef _WIN32
#endif
}

static void pidgin_window_init(GtkWindow *wnd,const char *title,guint border_width,const char *role,gboolean resizable)
{
  if (title != 0) 
    gtk_window_set_title(wnd,title);
#ifdef _WIN32
#endif
  gtk_container_set_border_width(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)wnd),gtk_container_get_type()))),border_width);
  if (role != 0) 
    gtk_window_set_role(wnd,role);
  gtk_window_set_resizable(wnd,resizable);
}

GtkWidget *pidgin_create_window(const char *title,guint border_width,const char *role,gboolean resizable)
{
  GtkWindow *wnd = (GtkWindow *)((void *)0);
  wnd = ((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_window_new(GTK_WINDOW_TOPLEVEL))),gtk_window_get_type())));
  pidgin_window_init(wnd,title,border_width,role,resizable);
  return (GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)wnd),gtk_widget_get_type()));
}

GtkWidget *pidgin_create_small_button(GtkWidget *image)
{
  GtkWidget *button;
  button = gtk_button_new();
  gtk_button_set_relief(((GtkButton *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_button_get_type()))),GTK_RELIEF_NONE);
/* don't allow focus on the close button */
  gtk_button_set_focus_on_click(((GtkButton *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_button_get_type()))),0);
/* set style to make it as small as possible */
  gtk_widget_set_name(button,"pidgin-small-close-button");
  gtk_widget_show(image);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_container_get_type()))),image);
  return button;
}

GtkWidget *pidgin_create_dialog(const char *title,guint border_width,const char *role,gboolean resizable)
{
  GtkWindow *wnd = (GtkWindow *)((void *)0);
  wnd = ((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_dialog_new())),gtk_window_get_type())));
  pidgin_window_init(wnd,title,border_width,role,resizable);
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)wnd),((GType )(20 << 2))))),"has-separator",0,((void *)((void *)0)));
  return (GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)wnd),gtk_widget_get_type()));
}

GtkWidget *pidgin_dialog_get_vbox_with_properties(GtkDialog *dialog,gboolean homogeneous,gint spacing)
{
  GtkBox *vbox = (GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type())))).vbox),gtk_box_get_type()));
  gtk_box_set_homogeneous(vbox,homogeneous);
  gtk_box_set_spacing(vbox,spacing);
  return (GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_widget_get_type()));
}

GtkWidget *pidgin_dialog_get_vbox(GtkDialog *dialog)
{
  return ( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type())))).vbox;
}

GtkWidget *pidgin_dialog_get_action_area(GtkDialog *dialog)
{
  return ( *((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type())))).action_area;
}

GtkWidget *pidgin_dialog_add_button(GtkDialog *dialog,const char *label,GCallback callback,gpointer callbackdata)
{
  GtkWidget *button = gtk_button_new_from_stock(label);
  GtkWidget *bbox = pidgin_dialog_get_action_area(dialog);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),button,0,0,0);
  if (callback != 0) 
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)button),((GType )(20 << 2))))),"clicked",callback,callbackdata,0,((GConnectFlags )0));
  gtk_widget_show(button);
  return button;
}

GtkWidget *pidgin_create_imhtml(gboolean editable,GtkWidget **imhtml_ret,GtkWidget **toolbar_ret,GtkWidget **sw_ret)
{
  GtkWidget *frame;
  GtkWidget *imhtml;
  GtkWidget *sep;
  GtkWidget *sw;
  GtkWidget *toolbar = (GtkWidget *)((void *)0);
  GtkWidget *vbox;
  frame = gtk_frame_new(0);
  gtk_frame_set_shadow_type(((GtkFrame *)(g_type_check_instance_cast(((GTypeInstance *)frame),gtk_frame_get_type()))),GTK_SHADOW_IN);
  vbox = gtk_vbox_new(0,0);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)frame),gtk_container_get_type()))),vbox);
  gtk_widget_show(vbox);
  if (editable != 0) {
    toolbar = gtk_imhtmltoolbar_new();
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),toolbar,0,0,0);
    gtk_widget_show(toolbar);
    sep = gtk_hseparator_new();
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),sep,0,0,0);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),((GType )(20 << 2))))),"show",((GCallback )gtk_widget_show),sep,0,G_CONNECT_SWAPPED);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),((GType )(20 << 2))))),"hide",((GCallback )gtk_widget_hide),sep,0,G_CONNECT_SWAPPED);
    gtk_widget_show(sep);
  }
  imhtml = gtk_imhtml_new(0,0);
  gtk_imhtml_set_editable(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),editable);
  gtk_imhtml_set_format_functions(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_imhtml_get_type()))),(GTK_IMHTML_ALL ^ GTK_IMHTML_IMAGE));
  gtk_text_view_set_wrap_mode(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))),GTK_WRAP_WORD_CHAR);
#ifdef USE_GTKSPELL
  if ((editable != 0) && (purple_prefs_get_bool("/pidgin/conversations/spellcheck") != 0)) 
    pidgin_setup_gtkspell(((GtkTextView *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),gtk_text_view_get_type()))));
#endif
  gtk_widget_show(imhtml);
  if (editable != 0) {
    gtk_imhtmltoolbar_attach(((GtkIMHtmlToolbar *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_imhtmltoolbar_get_type()))),imhtml);
    gtk_imhtmltoolbar_associate_smileys(((GtkIMHtmlToolbar *)(g_type_check_instance_cast(((GTypeInstance *)toolbar),gtk_imhtmltoolbar_get_type()))),"default");
  }
  pidgin_setup_imhtml(imhtml);
  sw = pidgin_make_scrollable(imhtml,GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC,GTK_SHADOW_NONE,-1,-1);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),sw,(!0),(!0),0);
  if (imhtml_ret != ((GtkWidget **)((void *)0))) 
     *imhtml_ret = imhtml;
  if ((editable != 0) && (toolbar_ret != ((GtkWidget **)((void *)0)))) 
     *toolbar_ret = toolbar;
  if (sw_ret != ((GtkWidget **)((void *)0))) 
     *sw_ret = sw;
  return frame;
}

void pidgin_set_sensitive_if_input(GtkWidget *entry,GtkWidget *dialog)
{
  const char *text = gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))));
  gtk_dialog_set_response_sensitive(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)dialog),gtk_dialog_get_type()))),GTK_RESPONSE_OK,(( *text) != 0));
}

void pidgin_toggle_sensitive(GtkWidget *widget,GtkWidget *to_toggle)
{
  gboolean sensitivity;
  if (to_toggle == ((GtkWidget *)((void *)0))) 
    return ;
  sensitivity = (((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)to_toggle),gtk_object_get_type())))).flags & GTK_SENSITIVE) != 0) && ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)to_toggle),gtk_object_get_type())))).flags & GTK_PARENT_SENSITIVE) != 0));
  gtk_widget_set_sensitive(to_toggle,!(sensitivity != 0));
}

void pidgin_toggle_sensitive_array(GtkWidget *w,GPtrArray *data)
{
  gboolean sensitivity;
  gpointer element;
  int i;
  for (i = 0; i < (data -> len); i++) {
    element = (data -> pdata)[i];
    if (element == ((void *)((void *)0))) 
      continue; 
    sensitivity = (((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)element),gtk_object_get_type())))).flags & GTK_SENSITIVE) != 0) && ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)element),gtk_object_get_type())))).flags & GTK_PARENT_SENSITIVE) != 0));
    gtk_widget_set_sensitive(element,!(sensitivity != 0));
  }
}

void pidgin_toggle_showhide(GtkWidget *widget,GtkWidget *to_toggle)
{
  if (to_toggle == ((GtkWidget *)((void *)0))) 
    return ;
  if ((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)to_toggle),gtk_object_get_type())))).flags & GTK_VISIBLE) != 0) 
    gtk_widget_hide(to_toggle);
  else 
    gtk_widget_show(to_toggle);
}

GtkWidget *pidgin_separator(GtkWidget *menu)
{
  GtkWidget *menuitem;
  menuitem = gtk_separator_menu_item_new();
  gtk_widget_show(menuitem);
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem);
  return menuitem;
}

GtkWidget *pidgin_new_item(GtkWidget *menu,const char *str)
{
  GtkWidget *menuitem;
  GtkWidget *label;
  menuitem = gtk_menu_item_new();
  if (menu != 0) 
    gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem);
  gtk_widget_show(menuitem);
  label = gtk_label_new(str);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
  gtk_label_set_pattern(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),"_");
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_container_get_type()))),label);
  gtk_widget_show(label);
/* FIXME: Go back and fix this
	gtk_widget_add_accelerator(menuitem, "activate", accel, str[0],
				   GDK_MOD1_MASK, GTK_ACCEL_LOCKED);
*/
  pidgin_set_accessible_label(menuitem,label);
  return menuitem;
}

GtkWidget *pidgin_new_check_item(GtkWidget *menu,const char *str,GCallback cb,gpointer data,gboolean checked)
{
  GtkWidget *menuitem;
  menuitem = gtk_check_menu_item_new_with_mnemonic(str);
  if (menu != 0) 
    gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem);
  gtk_check_menu_item_set_active(((GtkCheckMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_check_menu_item_get_type()))),checked);
  if (cb != 0) 
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"activate",cb,data,0,((GConnectFlags )0));
  gtk_widget_show_all(menuitem);
  return menuitem;
}

GtkWidget *pidgin_pixbuf_toolbar_button_from_stock(const char *icon)
{
  GtkWidget *button;
  GtkWidget *image;
  GtkWidget *bbox;
  button = gtk_toggle_button_new();
  gtk_button_set_relief(((GtkButton *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_button_get_type()))),GTK_RELIEF_NONE);
  bbox = gtk_vbox_new(0,0);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_container_get_type()))),bbox);
  image = gtk_image_new_from_stock(icon,gtk_icon_size_from_name("pidgin-icon-size-tango-extra-small"));
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),image,0,0,0);
  gtk_widget_show_all(bbox);
  return button;
}

GtkWidget *pidgin_pixbuf_button_from_stock(const char *text,const char *icon,PidginButtonOrientation style)
{
  GtkWidget *button;
  GtkWidget *image;
  GtkWidget *label;
  GtkWidget *bbox;
  GtkWidget *ibox;
  GtkWidget *lbox = (GtkWidget *)((void *)0);
  button = gtk_button_new();
  if (style == PIDGIN_BUTTON_HORIZONTAL) {
    bbox = gtk_hbox_new(0,0);
    ibox = gtk_hbox_new(0,0);
    if (text != 0) 
      lbox = gtk_hbox_new(0,0);
  }
  else {
    bbox = gtk_vbox_new(0,0);
    ibox = gtk_vbox_new(0,0);
    if (text != 0) 
      lbox = gtk_vbox_new(0,0);
  }
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)button),gtk_container_get_type()))),bbox);
  if (icon != 0) {
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),ibox,(!0),(!0),0);
    image = gtk_image_new_from_stock(icon,GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)ibox),gtk_box_get_type()))),image,0,(!0),0);
  }
  if (text != 0) {
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)bbox),gtk_box_get_type()))),lbox,(!0),(!0),0);
    label = gtk_label_new(0);
    gtk_label_set_text_with_mnemonic(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),text);
    gtk_label_set_mnemonic_widget(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),button);
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)lbox),gtk_box_get_type()))),label,0,(!0),0);
    pidgin_set_accessible_label(button,label);
  }
  gtk_widget_show_all(bbox);
  return button;
}

GtkWidget *pidgin_new_item_from_stock(GtkWidget *menu,const char *str,const char *icon,GCallback cb,gpointer data,guint accel_key,guint accel_mods,char *mod)
{
  GtkWidget *menuitem;
/*
	GtkWidget *hbox;
	GtkWidget *label;
	*/
  GtkWidget *image;
  if (icon == ((const char *)((void *)0))) 
    menuitem = gtk_menu_item_new_with_mnemonic(str);
  else 
    menuitem = gtk_image_menu_item_new_with_mnemonic(str);
  if (menu != 0) 
    gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem);
  if (cb != 0) 
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"activate",cb,data,0,((GConnectFlags )0));
  if (icon != ((const char *)((void *)0))) {
    image = gtk_image_new_from_stock(icon,gtk_icon_size_from_name("pidgin-icon-size-tango-extra-small"));
    gtk_image_menu_item_set_image(((GtkImageMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_image_menu_item_get_type()))),image);
  }
/* FIXME: this isn't right
	if (mod) {
		label = gtk_label_new(mod);
		gtk_box_pack_end(GTK_BOX(hbox), label, FALSE, FALSE, 2);
		gtk_widget_show(label);
	}
*/
/*
	if (accel_key) {
		gtk_widget_add_accelerator(menuitem, "activate", accel, accel_key,
					   accel_mods, GTK_ACCEL_LOCKED);
	}
*/
  gtk_widget_show_all(menuitem);
  return menuitem;
}

GtkWidget *pidgin_make_frame(GtkWidget *parent,const char *title)
{
  GtkWidget *vbox;
  GtkWidget *label;
  GtkWidget *hbox;
  char *labeltitle;
  vbox = gtk_vbox_new(0,6);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)parent),gtk_box_get_type()))),vbox,0,0,0);
  gtk_widget_show(vbox);
  label = gtk_label_new(0);
  labeltitle = g_strdup_printf("<span weight=\"bold\">%s</span>",title);
  gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),labeltitle);
  g_free(labeltitle);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),label,0,0,0);
  gtk_widget_show(label);
  pidgin_set_accessible_label(vbox,label);
  hbox = gtk_hbox_new(0,6);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),hbox,0,0,0);
  gtk_widget_show(hbox);
  label = gtk_label_new("    ");
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  gtk_widget_show(label);
  vbox = gtk_vbox_new(0,6);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),vbox,0,0,0);
  gtk_widget_show(vbox);
  return vbox;
}

static gpointer aop_option_menu_get_selected(GtkWidget *optmenu,GtkWidget **p_item)
{
  GtkWidget *menu = gtk_option_menu_get_menu(((GtkOptionMenu *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),gtk_option_menu_get_type()))));
  GtkWidget *item = gtk_menu_get_active(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_get_type()))));
  if (p_item != 0) 
     *p_item = item;
  return (item != 0)?g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"aop_per_item_data") : ((void *)((void *)0));
}

static void aop_menu_cb(GtkWidget *optmenu,GCallback cb)
{
  GtkWidget *item;
  gpointer per_item_data;
  per_item_data = aop_option_menu_get_selected(optmenu,&item);
  if (cb != ((void (*)())((void *)0))) {
    ( *((void (*)(GtkWidget *, gpointer , gpointer ))cb))(item,per_item_data,g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),((GType )(20 << 2))))),"user_data"));
  }
}

static GtkWidget *aop_menu_item_new(GtkSizeGroup *sg,GdkPixbuf *pixbuf,const char *lbl,gpointer per_item_data,const char *data)
{
  GtkWidget *item;
  GtkWidget *hbox;
  GtkWidget *image;
  GtkWidget *label;
  item = gtk_menu_item_new();
  gtk_widget_show(item);
  hbox = gtk_hbox_new(0,4);
  gtk_widget_show(hbox);
/* Create the image */
  if (pixbuf == ((GdkPixbuf *)((void *)0))) 
    image = gtk_image_new();
  else 
    image = gtk_image_new_from_pixbuf(pixbuf);
  gtk_widget_show(image);
  if (sg != 0) 
    gtk_size_group_add_widget(sg,image);
/* Create the label */
  label = gtk_label_new(lbl);
  gtk_widget_show(label);
  gtk_label_set_justify(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0.0,0.5);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_container_get_type()))),hbox);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),image,0,0,0);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,(!0),(!0),0);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),data,per_item_data);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"aop_per_item_data",per_item_data);
  pidgin_set_accessible_label(item,label);
  return item;
}

static GdkPixbuf *pidgin_create_prpl_icon_from_prpl(PurplePlugin *prpl,PidginPrplIconSize size,PurpleAccount *account)
{
  PurplePluginProtocolInfo *prpl_info;
  const char *protoname = (const char *)((void *)0);
  char *tmp;
  char *filename = (char *)((void *)0);
  GdkPixbuf *pixbuf;
  prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if ((prpl_info -> list_icon) == ((const char *(*)(PurpleAccount *, PurpleBuddy *))((void *)0))) 
    return 0;
  protoname = ( *(prpl_info -> list_icon))(account,0);
  if (protoname == ((const char *)((void *)0))) 
    return 0;
/*
	 * Status icons will be themeable too, and then it will look up
	 * protoname from the theme
	 */
  tmp = g_strconcat(protoname,".png",((void *)((void *)0)));
  filename = g_build_filename("/usr/local/share","pixmaps","pidgin","protocols",((size == PIDGIN_PRPL_ICON_SMALL)?"16" : (((size == PIDGIN_PRPL_ICON_MEDIUM)?"22" : "48"))),tmp,((void *)((void *)0)));
  g_free(tmp);
  pixbuf = pidgin_pixbuf_new_from_file(filename);
  g_free(filename);
  return pixbuf;
}

static GtkWidget *aop_option_menu_new(AopMenu *aop_menu,GCallback cb,gpointer user_data)
{
  GtkWidget *optmenu;
  optmenu = gtk_option_menu_new();
  gtk_widget_show(optmenu);
  gtk_option_menu_set_menu(((GtkOptionMenu *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),gtk_option_menu_get_type()))),(aop_menu -> menu));
  if ((aop_menu -> default_item) != -1) 
    gtk_option_menu_set_history(((GtkOptionMenu *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),gtk_option_menu_get_type()))),(aop_menu -> default_item));
  g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),((GType )(20 << 2))))),"aop_menu",aop_menu,((GDestroyNotify )g_free));
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),((GType )(20 << 2))))),"user_data",user_data);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),((GType )(20 << 2))))),"changed",((GCallback )aop_menu_cb),cb,0,((GConnectFlags )0));
  return optmenu;
}

static void aop_option_menu_replace_menu(GtkWidget *optmenu,AopMenu *new_aop_menu)
{
  if (gtk_option_menu_get_menu(((GtkOptionMenu *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),gtk_option_menu_get_type())))) != 0) 
    gtk_option_menu_remove_menu(((GtkOptionMenu *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),gtk_option_menu_get_type()))));
  gtk_option_menu_set_menu(((GtkOptionMenu *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),gtk_option_menu_get_type()))),(new_aop_menu -> menu));
  if ((new_aop_menu -> default_item) != -1) 
    gtk_option_menu_set_history(((GtkOptionMenu *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),gtk_option_menu_get_type()))),(new_aop_menu -> default_item));
  g_object_set_data_full(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),((GType )(20 << 2))))),"aop_menu",new_aop_menu,((GDestroyNotify )g_free));
}

static void aop_option_menu_select_by_data(GtkWidget *optmenu,gpointer data)
{
  guint idx;
  GList *llItr = (GList *)((void *)0);
{
    for (((idx = 0) , (llItr = ( *((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_option_menu_get_menu(((GtkOptionMenu *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),gtk_option_menu_get_type())))))),gtk_menu_shell_get_type())))).children)); llItr != ((GList *)((void *)0)); ((llItr = (llItr -> next)) , idx++)) {
      if (data == g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(llItr -> data)),((GType )(20 << 2))))),"aop_per_item_data")) {
        gtk_option_menu_set_history(((GtkOptionMenu *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),gtk_option_menu_get_type()))),idx);
        break; 
      }
    }
  }
}

static AopMenu *create_protocols_menu(const char *default_proto_id)
{
  AopMenu *aop_menu = (AopMenu *)((void *)0);
  PurplePlugin *plugin;
  GdkPixbuf *pixbuf = (GdkPixbuf *)((void *)0);
  GtkSizeGroup *sg;
  GList *p;
  const char *gtalk_name = (const char *)((void *)0);
  const char *facebook_name = (const char *)((void *)0);
  int i;
  aop_menu = (g_malloc0((sizeof(AopMenu ))));
  aop_menu -> default_item = (-1);
  aop_menu -> menu = gtk_menu_new();
  gtk_widget_show((aop_menu -> menu));
  sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  if (purple_find_prpl("prpl-jabber") != 0) {
    gtalk_name = ((const char *)(dgettext("pidgin","Google Talk")));
    facebook_name = ((const char *)(dgettext("pidgin","Facebook (XMPP)")));
  }
  for (((p = purple_plugins_get_protocols()) , (i = 0)); p != ((GList *)((void *)0)); ((p = (p -> next)) , i++)) {
    plugin = ((PurplePlugin *)(p -> data));
    if ((gtalk_name != 0) && (strcmp(gtalk_name,( *(plugin -> info)).name) < 0)) {
      char *filename = g_build_filename("/usr/local/share","pixmaps","pidgin","protocols","16","google-talk.png",((void *)((void *)0)));
      GtkWidget *item;
      pixbuf = pidgin_pixbuf_new_from_file(filename);
      g_free(filename);
      gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)(aop_menu -> menu)),gtk_menu_shell_get_type()))),(item = aop_menu_item_new(sg,pixbuf,gtalk_name,"prpl-jabber","protocol")));
      g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"fakegoogle",((gpointer )((gpointer )((glong )1))));
      if (pixbuf != 0) 
        g_object_unref(pixbuf);
      gtalk_name = ((const char *)((void *)0));
      i++;
    }
    if ((facebook_name != 0) && (strcmp(facebook_name,( *(plugin -> info)).name) < 0)) {
      char *filename = g_build_filename("/usr/local/share","pixmaps","pidgin","protocols","16","facebook.png",((void *)((void *)0)));
      GtkWidget *item;
      pixbuf = pidgin_pixbuf_new_from_file(filename);
      g_free(filename);
      gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)(aop_menu -> menu)),gtk_menu_shell_get_type()))),(item = aop_menu_item_new(sg,pixbuf,facebook_name,"prpl-jabber","protocol")));
      g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"fakefacebook",((gpointer )((gpointer )((glong )1))));
      if (pixbuf != 0) 
        g_object_unref(pixbuf);
      facebook_name = ((const char *)((void *)0));
      i++;
    }
    pixbuf = pidgin_create_prpl_icon_from_prpl(plugin,PIDGIN_PRPL_ICON_SMALL,0);
    gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)(aop_menu -> menu)),gtk_menu_shell_get_type()))),aop_menu_item_new(sg,pixbuf,( *(plugin -> info)).name,( *(plugin -> info)).id,"protocol"));
    if (pixbuf != 0) 
      g_object_unref(pixbuf);
    if ((default_proto_id != ((const char *)((void *)0))) && !(strcmp(( *(plugin -> info)).id,default_proto_id) != 0)) 
      aop_menu -> default_item = i;
  }
  g_object_unref(sg);
  return aop_menu;
}

GtkWidget *pidgin_protocol_option_menu_new(const char *id,GCallback cb,gpointer user_data)
{
  return aop_option_menu_new(create_protocols_menu(id),cb,user_data);
}

const char *pidgin_protocol_option_menu_get_selected(GtkWidget *optmenu)
{
  return (const char *)(aop_option_menu_get_selected(optmenu,0));
}

PurpleAccount *pidgin_account_option_menu_get_selected(GtkWidget *optmenu)
{
  return (PurpleAccount *)(aop_option_menu_get_selected(optmenu,0));
}

static AopMenu *create_account_menu(PurpleAccount *default_account,PurpleFilterAccountFunc filter_func,gboolean show_all)
{
  AopMenu *aop_menu = (AopMenu *)((void *)0);
  PurpleAccount *account;
  GdkPixbuf *pixbuf = (GdkPixbuf *)((void *)0);
  GList *list;
  GList *p;
  GtkSizeGroup *sg;
  int i;
  char buf[256UL];
  if (show_all != 0) 
    list = purple_accounts_get_all();
  else 
    list = purple_connections_get_all();
  aop_menu = (g_malloc0((sizeof(AopMenu ))));
  aop_menu -> default_item = (-1);
  aop_menu -> menu = gtk_menu_new();
  gtk_widget_show((aop_menu -> menu));
  sg = gtk_size_group_new(GTK_SIZE_GROUP_HORIZONTAL);
  for (((p = list) , (i = 0)); p != ((GList *)((void *)0)); ((p = (p -> next)) , i++)) {
    if (show_all != 0) 
      account = ((PurpleAccount *)(p -> data));
    else {
      PurpleConnection *gc = (PurpleConnection *)(p -> data);
      account = purple_connection_get_account(gc);
    }
    if ((filter_func != 0) && !(( *filter_func)(account) != 0)) {
      i--;
      continue; 
    }
    pixbuf = pidgin_create_prpl_icon(account,PIDGIN_PRPL_ICON_SMALL);
    if (pixbuf != 0) {
      if (((purple_account_is_disconnected(account) != 0) && (show_all != 0)) && (purple_connections_get_all() != 0)) 
        gdk_pixbuf_saturate_and_pixelate(pixbuf,pixbuf,0.0,0);
    }
    if (purple_account_get_alias(account) != 0) {
      g_snprintf(buf,(sizeof(buf)),"%s (%s) (%s)",purple_account_get_username(account),purple_account_get_alias(account),purple_account_get_protocol_name(account));
    }
    else {
      g_snprintf(buf,(sizeof(buf)),"%s (%s)",purple_account_get_username(account),purple_account_get_protocol_name(account));
    }
    gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)(aop_menu -> menu)),gtk_menu_shell_get_type()))),aop_menu_item_new(sg,pixbuf,buf,account,"account"));
    if (pixbuf != 0) 
      g_object_unref(pixbuf);
    if ((default_account != 0) && (account == default_account)) 
      aop_menu -> default_item = i;
  }
  g_object_unref(sg);
  return aop_menu;
}

static void regenerate_account_menu(GtkWidget *optmenu)
{
  gboolean show_all;
  PurpleAccount *account;
  PurpleFilterAccountFunc filter_func;
  account = ((PurpleAccount *)(aop_option_menu_get_selected(optmenu,0)));
  show_all = ((gint )((glong )(g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),((GType )(20 << 2))))),"show_all"))));
  filter_func = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),((GType )(20 << 2))))),"filter_func"));
  aop_option_menu_replace_menu(optmenu,create_account_menu(account,filter_func,show_all));
}

static void account_menu_sign_on_off_cb(PurpleConnection *gc,GtkWidget *optmenu)
{
  regenerate_account_menu(optmenu);
}

static void account_menu_added_removed_cb(PurpleAccount *account,GtkWidget *optmenu)
{
  regenerate_account_menu(optmenu);
}

static gboolean account_menu_destroyed_cb(GtkWidget *optmenu,GdkEvent *event,void *user_data)
{
  purple_signals_disconnect_by_handle(optmenu);
  return 0;
}

void pidgin_account_option_menu_set_selected(GtkWidget *optmenu,PurpleAccount *account)
{
  aop_option_menu_select_by_data(optmenu,account);
}

GtkWidget *pidgin_account_option_menu_new(PurpleAccount *default_account,gboolean show_all,GCallback cb,PurpleFilterAccountFunc filter_func,gpointer user_data)
{
  GtkWidget *optmenu;
/* Create the option menu */
  optmenu = aop_option_menu_new(create_account_menu(default_account,filter_func,show_all),cb,user_data);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),((GType )(20 << 2))))),"destroy",((GCallback )account_menu_destroyed_cb),0,0,((GConnectFlags )0));
/* Register the purple sign on/off event callbacks. */
  purple_signal_connect(purple_connections_get_handle(),"signed-on",optmenu,((PurpleCallback )account_menu_sign_on_off_cb),optmenu);
  purple_signal_connect(purple_connections_get_handle(),"signed-off",optmenu,((PurpleCallback )account_menu_sign_on_off_cb),optmenu);
  purple_signal_connect(purple_accounts_get_handle(),"account-added",optmenu,((PurpleCallback )account_menu_added_removed_cb),optmenu);
  purple_signal_connect(purple_accounts_get_handle(),"account-removed",optmenu,((PurpleCallback )account_menu_added_removed_cb),optmenu);
/* Set some data. */
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),((GType )(20 << 2))))),"user_data",user_data);
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),((GType )(20 << 2))))),"show_all",((gpointer )((glong )show_all)));
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)optmenu),((GType )(20 << 2))))),"filter_func",filter_func);
  return optmenu;
}

gboolean pidgin_check_if_dir(const char *path,GtkFileSelection *filesel)
{
  char *dirname = (char *)((void *)0);
  if (g_file_test(path,G_FILE_TEST_IS_DIR) != 0) {
/* append a / if needed */
    if (path[strlen(path) - 1] != '/') {
      dirname = g_strconcat(path,"/",((void *)((void *)0)));
    }
    gtk_file_selection_set_filename(filesel,((dirname != ((char *)((void *)0)))?dirname : path));
    g_free(dirname);
    return (!0);
  }
  return 0;
}

void pidgin_setup_gtkspell(GtkTextView *textview)
{
#ifdef USE_GTKSPELL
  GError *error = (GError *)((void *)0);
  char *locale = (char *)((void *)0);
  do {
    if (textview != ((GtkTextView *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"textview != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)textview;
      GType __t = gtk_text_view_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_TEXT_VIEW(textview)");
      return ;
    };
  }while (0);
  if ((gtkspell_new_attach(textview,locale,&error) == ((GtkSpell *)((void *)0))) && (error != 0)) {
    purple_debug_warning("gtkspell","Failed to setup GtkSpell: %s\n",(error -> message));
    g_error_free(error);
  }
#endif /* USE_GTKSPELL */
}

void pidgin_save_accels_cb(GtkAccelGroup *accel_group,guint arg1,GdkModifierType arg2,GClosure *arg3,gpointer data)
{
  purple_debug(PURPLE_DEBUG_MISC,"accels","accel changed, scheduling save.\n");
  if (!(accels_save_timer != 0U)) 
    accels_save_timer = purple_timeout_add_seconds(5,pidgin_save_accels,0);
}

gboolean pidgin_save_accels(gpointer data)
{
  char *filename = (char *)((void *)0);
  filename = g_build_filename(purple_user_dir(),"/","accels",((void *)((void *)0)));
  purple_debug(PURPLE_DEBUG_MISC,"accels","saving accels to %s\n",filename);
  gtk_accel_map_save(filename);
  g_free(filename);
  accels_save_timer = 0;
  return 0;
}

void pidgin_load_accels()
{
  char *filename = (char *)((void *)0);
  filename = g_build_filename(purple_user_dir(),"/","accels",((void *)((void *)0)));
  gtk_accel_map_load(filename);
  g_free(filename);
}

static void show_retrieveing_info(PurpleConnection *conn,const char *name)
{
  PurpleNotifyUserInfo *info = purple_notify_user_info_new();
  purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Information"))),((const char *)(dgettext("pidgin","Retrieving..."))));
  purple_notify_userinfo(conn,name,info,0,0);
  purple_notify_user_info_destroy(info);
}

void pidgin_retrieve_user_info(PurpleConnection *conn,const char *name)
{
  show_retrieveing_info(conn,name);
  serv_get_info(conn,name);
}

void pidgin_retrieve_user_info_in_chat(PurpleConnection *conn,const char *name,int chat)
{
  char *who = (char *)((void *)0);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  if (chat < 0) {
    pidgin_retrieve_user_info(conn,name);
    return ;
  }
  prpl_info = ((PurplePluginProtocolInfo *)( *( *(conn -> prpl)).info).extra_info);
  if ((prpl_info != ((PurplePluginProtocolInfo *)((void *)0))) && ((prpl_info -> get_cb_real_name) != 0)) 
    who = ( *(prpl_info -> get_cb_real_name))(conn,chat,name);
  if ((prpl_info == ((PurplePluginProtocolInfo *)((void *)0))) || ((prpl_info -> get_cb_info) == ((void (*)(PurpleConnection *, int , const char *))((void *)0)))) {
    pidgin_retrieve_user_info(conn,((who != 0)?who : name));
    g_free(who);
    return ;
  }
  show_retrieveing_info(conn,((who != 0)?who : name));
  ( *(prpl_info -> get_cb_info))(conn,chat,name);
  g_free(who);
}

gboolean pidgin_parse_x_im_contact(const char *msg,gboolean all_accounts,PurpleAccount **ret_account,char **ret_protocol,char **ret_username,char **ret_alias)
{
  char *protocol = (char *)((void *)0);
  char *username = (char *)((void *)0);
  char *alias = (char *)((void *)0);
  char *str;
  char *s;
  gboolean valid;
  do {
    if (msg != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg != NULL");
      return 0;
    };
  }while (0);
  do {
    if (ret_protocol != ((char **)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ret_protocol != NULL");
      return 0;
    };
  }while (0);
  do {
    if (ret_username != ((char **)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ret_username != NULL");
      return 0;
    };
  }while (0);
  s = (str = g_strdup(msg));
  while(((( *s) != 13) && (( *s) != 10)) && (( *s) != 0)){{
      char *key;
      char *value;
      key = s;
/* Grab the key */
      while((((( *s) != 13) && (( *s) != 10)) && (( *s) != 0)) && (( *s) != 32))
        s++;
      if (( *s) == 13) 
        s++;
      if (( *s) == 10) {
        s++;
        continue; 
      }
      if (( *s) != 0) 
         *(s++) = 0;
/* Clear past any whitespace */
      while((( *s) != 0) && (( *s) == 32))
        s++;
/* Now let's grab until the end of the line. */
      value = s;
      while(((( *s) != 13) && (( *s) != 10)) && (( *s) != 0))
        s++;
      if (( *s) == 13) 
         *(s++) = 0;
      if (( *s) == 10) 
         *(s++) = 0;
      if (strchr(key,':') != ((char *)((void *)0))) {
        if (!(g_ascii_strcasecmp(key,"X-IM-Username:") != 0)) 
          username = g_strdup(value);
        else if (!(g_ascii_strcasecmp(key,"X-IM-Protocol:") != 0)) 
          protocol = g_strdup(value);
        else if (!(g_ascii_strcasecmp(key,"X-IM-Alias:") != 0)) 
          alias = g_strdup(value);
      }
    }
  }
  if ((username != ((char *)((void *)0))) && (protocol != ((char *)((void *)0)))) {
    valid = (!0);
     *ret_username = username;
     *ret_protocol = protocol;
    if (ret_alias != ((char **)((void *)0))) 
       *ret_alias = alias;
/* Check for a compatible account. */
    if (ret_account != ((PurpleAccount **)((void *)0))) {
      GList *list;
      PurpleAccount *account = (PurpleAccount *)((void *)0);
      GList *l;
      const char *protoname;
      if (all_accounts != 0) 
        list = purple_accounts_get_all();
      else 
        list = purple_connections_get_all();
{
        for (l = list; l != ((GList *)((void *)0)); l = (l -> next)) {{
            PurpleConnection *gc;
            PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
            PurplePlugin *plugin;
            if (all_accounts != 0) {
              account = ((PurpleAccount *)(l -> data));
              plugin = purple_plugins_find_with_id(purple_account_get_protocol_id(account));
              if (plugin == ((PurplePlugin *)((void *)0))) {
                account = ((PurpleAccount *)((void *)0));
                continue; 
              }
              prpl_info = ((PurplePluginProtocolInfo *)( *(plugin -> info)).extra_info);
            }
            else {
              gc = ((PurpleConnection *)(l -> data));
              account = purple_connection_get_account(gc);
              prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info);
            }
            protoname = ( *(prpl_info -> list_icon))(account,0);
            if (!(strcmp(protoname,protocol) != 0)) 
              break; 
            account = ((PurpleAccount *)((void *)0));
          }
        }
      }
/* Special case for AIM and ICQ */
      if ((account == ((PurpleAccount *)((void *)0))) && (!(strcmp(protocol,"aim") != 0) || !(strcmp(protocol,"icq") != 0))) {{
          for (l = list; l != ((GList *)((void *)0)); l = (l -> next)) {{
              PurpleConnection *gc;
              PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
              PurplePlugin *plugin;
              if (all_accounts != 0) {
                account = ((PurpleAccount *)(l -> data));
                plugin = purple_plugins_find_with_id(purple_account_get_protocol_id(account));
                if (plugin == ((PurplePlugin *)((void *)0))) {
                  account = ((PurpleAccount *)((void *)0));
                  continue; 
                }
                prpl_info = ((PurplePluginProtocolInfo *)( *(plugin -> info)).extra_info);
              }
              else {
                gc = ((PurpleConnection *)(l -> data));
                account = purple_connection_get_account(gc);
                prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info);
              }
              protoname = ( *(prpl_info -> list_icon))(account,0);
              if (!(strcmp(protoname,"aim") != 0) || !(strcmp(protoname,"icq") != 0)) 
                break; 
              account = ((PurpleAccount *)((void *)0));
            }
          }
        }
      }
       *ret_account = account;
    }
  }
  else {
    valid = 0;
    g_free(username);
    g_free(protocol);
    g_free(alias);
  }
  g_free(str);
  return valid;
}

void pidgin_set_accessible_label(GtkWidget *w,GtkWidget *l)
{
  AtkObject *acc;
  const gchar *label_text;
  const gchar *existing_name;
  acc = gtk_widget_get_accessible(w);
/* If this object has no name, set it's name with the label text */
  existing_name = atk_object_get_name(acc);
  if (!(existing_name != 0)) {
    label_text = gtk_label_get_text(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)l),gtk_label_get_type()))));
    if (label_text != 0) 
      atk_object_set_name(acc,label_text);
  }
  pidgin_set_accessible_relations(w,l);
}

void pidgin_set_accessible_relations(GtkWidget *w,GtkWidget *l)
{
  AtkObject *acc;
  AtkObject *label;
  AtkObject *rel_obj[1UL];
  AtkRelationSet *set;
  AtkRelation *relation;
  acc = gtk_widget_get_accessible(w);
  label = gtk_widget_get_accessible(l);
/* Make sure mnemonics work */
  gtk_label_set_mnemonic_widget(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)l),gtk_label_get_type()))),w);
/* Create the labeled-by relation */
  set = atk_object_ref_relation_set(acc);
  rel_obj[0] = label;
  relation = atk_relation_new(rel_obj,1,ATK_RELATION_LABELLED_BY);
  atk_relation_set_add(set,relation);
  g_object_unref(relation);
  g_object_unref(set);
/* Create the label-for relation */
  set = atk_object_ref_relation_set(label);
  rel_obj[0] = acc;
  relation = atk_relation_new(rel_obj,1,ATK_RELATION_LABEL_FOR);
  atk_relation_set_add(set,relation);
  g_object_unref(relation);
  g_object_unref(set);
}

void pidgin_menu_position_func_helper(GtkMenu *menu,gint *x,gint *y,gboolean *push_in,gpointer data)
{
  GtkWidget *widget;
  GtkRequisition requisition;
  GdkScreen *screen;
  GdkRectangle monitor;
  gint monitor_num;
  gint space_left;
  gint space_right;
  gint space_above;
  gint space_below;
  gint needed_width;
  gint needed_height;
  gint xthickness;
  gint ythickness;
  gboolean rtl;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)menu;
      GType __t = gtk_menu_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"GTK_IS_MENU(menu)");
      return ;
    };
  }while (0);
  widget = ((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_widget_get_type())));
  screen = gtk_widget_get_screen(widget);
  xthickness = ( *(widget -> style)).xthickness;
  ythickness = ( *(widget -> style)).ythickness;
  rtl = ((gtk_widget_get_direction(widget)) == GTK_TEXT_DIR_RTL);
/*
	 * We need the requisition to figure out the right place to
	 * popup the menu. In fact, we always need to ask here, since
	 * if a size_request was queued while we weren't popped up,
	 * the requisition won't have been recomputed yet.
	 */
  gtk_widget_size_request(widget,&requisition);
  monitor_num = gdk_screen_get_monitor_at_point(screen, *x, *y);
  push_in = 0;
/*
	 * The placement of popup menus horizontally works like this (with
	 * RTL in parentheses)
	 *
	 * - If there is enough room to the right (left) of the mouse cursor,
	 *   position the menu there.
	 *
	 * - Otherwise, if if there is enough room to the left (right) of the
	 *   mouse cursor, position the menu there.
	 *
	 * - Otherwise if the menu is smaller than the monitor, position it
	 *   on the side of the mouse cursor that has the most space available
	 *
	 * - Otherwise (if there is simply not enough room for the menu on the
	 *   monitor), position it as far left (right) as possible.
	 *
	 * Positioning in the vertical direction is similar: first try below
	 * mouse cursor, then above.
	 */
  gdk_screen_get_monitor_geometry(screen,monitor_num,&monitor);
  space_left = ( *x - monitor.x);
  space_right = (((monitor.x + monitor.width) -  *x) - 1);
  space_above = ( *y - monitor.y);
  space_below = (((monitor.y + monitor.height) -  *y) - 1);
/* position horizontally */
/* the amount of space we need to position the menu. Note the
	 * menu is offset "xthickness" pixels
	 */
  needed_width = (requisition.width - xthickness);
  if ((needed_width <= space_left) || (needed_width <= space_right)) {
    if (((rtl != 0) && (needed_width <= space_left)) || (!(rtl != 0) && (needed_width > space_right))) {
/* position left */
       *x = ((( *x + xthickness) - requisition.width) + 1);
    }
    else {
/* position right */
       *x = ( *x - xthickness);
    }
/* x is clamped on-screen further down */
  }
  else if (requisition.width <= monitor.width) {
/* the menu is too big to fit on either side of the mouse
		 * cursor, but smaller than the monitor. Position it on
		 * the side that has the most space
		 */
    if (space_left > space_right) {
/* left justify */
       *x = monitor.x;
    }
    else {
/* right justify */
       *x = ((monitor.x + monitor.width) - requisition.width);
    }
  }
  else 
/* menu is simply too big for the monitor */
{
    if (rtl != 0) {
/* right justify */
       *x = ((monitor.x + monitor.width) - requisition.width);
    }
    else {
/* left justify */
       *x = monitor.x;
    }
  }
/* Position vertically. The algorithm is the same as above, but
	 * simpler because we don't have to take RTL into account.
	 */
  needed_height = (requisition.height - ythickness);
  if ((needed_height <= space_above) || (needed_height <= space_below)) {
    if (needed_height <= space_below) 
       *y = ( *y - ythickness);
    else 
       *y = ((( *y + ythickness) - requisition.height) + 1);
     *y = (( *y > ((monitor.y + monitor.height) - requisition.height))?((monitor.y + monitor.height) - requisition.height) : ((( *y < monitor.y)?monitor.y :  *y)));
  }
  else if ((needed_height > space_below) && (needed_height > space_above)) {
    if (space_below >= space_above) 
       *y = ((monitor.y + monitor.height) - requisition.height);
    else 
       *y = monitor.y;
  }
  else {
     *y = monitor.y;
  }
}

void pidgin_treeview_popup_menu_position_func(GtkMenu *menu,gint *x,gint *y,gboolean *push_in,gpointer data)
{
  GtkWidget *widget = (GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)data),gtk_widget_get_type()));
  GtkTreeView *tv = (GtkTreeView *)(g_type_check_instance_cast(((GTypeInstance *)data),gtk_tree_view_get_type()));
  GtkTreePath *path;
  GtkTreeViewColumn *col;
  GdkRectangle rect;
  gint ythickness = ( *( *((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_widget_get_type())))).style).ythickness;
  gdk_window_get_origin((widget -> window),x,y);
  gtk_tree_view_get_cursor(tv,&path,&col);
  gtk_tree_view_get_cell_area(tv,path,col,&rect);
   *x += (rect.x + rect.width);
   *y += ((rect.y + rect.height) + ythickness);
  pidgin_menu_position_func_helper(menu,x,y,push_in,data);
}
enum __unnamed_enum___F0_L1445_C1_DND_FILE_TRANSFER__COMMA__DND_IM_IMAGE__COMMA__DND_BUDDY_ICON {DND_FILE_TRANSFER,DND_IM_IMAGE,DND_BUDDY_ICON};
typedef struct __unnamed_class___F0_L1451_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__filename__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L247R__Pe___variable_name_unknown_scope_and_name__scope__account__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__who {
char *filename;
PurpleAccount *account;
char *who;}_DndData;

static void dnd_image_ok_callback(_DndData *data,int choice)
{
  const gchar *shortname;
  gchar *filedata;
  size_t size;
  struct stat st;
  GError *err = (GError *)((void *)0);
  PurpleConversation *conv;
  PidginConversation *gtkconv;
  GtkTextIter iter;
  int id;
  PurpleBuddy *buddy;
  PurpleContact *contact;
{
    switch(choice){
      case 2:
{
        if (stat((data -> filename),&st) != 0) {
          char *str;
          str = g_strdup_printf(((const char *)(dgettext("pidgin","The following error has occurred loading %s: %s"))),(data -> filename),g_strerror( *__errno_location()));
          purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Failed to load image"))),str,0,0);
          g_free(str);
          break; 
        }
        buddy = purple_find_buddy((data -> account),(data -> who));
        if (!(buddy != 0)) {
          purple_debug_info("custom-icon","You can only set custom icons for people on your buddylist.\n");
          break; 
        }
        contact = purple_buddy_get_contact(buddy);
        purple_buddy_icons_node_set_custom_icon_from_file(((PurpleBlistNode *)contact),(data -> filename));
        break; 
      }
      case 0:
{
        serv_send_file(purple_account_get_connection((data -> account)),(data -> who),(data -> filename));
        break; 
      }
      case 1:
{
        conv = purple_conversation_new(PURPLE_CONV_TYPE_IM,(data -> account),(data -> who));
        gtkconv = ((PidginConversation *)(conv -> ui_data));
        if (!(g_file_get_contents((data -> filename),&filedata,&size,&err) != 0)) {
          char *str;
          str = g_strdup_printf(((const char *)(dgettext("pidgin","The following error has occurred loading %s: %s"))),(data -> filename),(err -> message));
          purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Failed to load image"))),str,0,0);
          g_error_free(err);
          g_free(str);
          break; 
        }
        shortname = (strrchr((data -> filename),'/'));
        shortname = ((shortname != 0)?(shortname + 1) : (data -> filename));
        id = purple_imgstore_add_with_id(filedata,size,shortname);
        gtk_text_buffer_get_iter_at_mark(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type())))).text_buffer,&iter,gtk_text_buffer_get_insert(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type())))).text_buffer));
        gtk_imhtml_insert_image_at_iter(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type()))),id,&iter);
        purple_imgstore_unref_by_id(id);
        break; 
      }
    }
  }
  g_free((data -> filename));
  g_free((data -> who));
  g_free(data);
}

static void dnd_image_cancel_callback(_DndData *data,int choice)
{
  g_free((data -> filename));
  g_free((data -> who));
  g_free(data);
}

static void dnd_set_icon_ok_cb(_DndData *data)
{
  dnd_image_ok_callback(data,DND_BUDDY_ICON);
}

static void dnd_set_icon_cancel_cb(_DndData *data)
{
  g_free((data -> filename));
  g_free((data -> who));
  g_free(data);
}

void pidgin_dnd_file_manage(GtkSelectionData *sd,PurpleAccount *account,const char *who)
{
  GdkPixbuf *pb;
  GList *files = purple_uri_list_extract_filenames(((const gchar *)(sd -> data)));
  PurpleConnection *gc = purple_account_get_connection(account);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
#ifndef _WIN32
  PurpleDesktopItem *item;
#endif
  gchar *filename = (gchar *)((void *)0);
  gchar *basename = (gchar *)((void *)0);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (who != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"who != NULL");
      return ;
    };
  }while (0);
  for (; files != 0; files = g_list_delete_link(files,files)) {
    g_free(filename);
    g_free(basename);
    filename = (files -> data);
    basename = g_path_get_basename(filename);
/* XXX - Make ft API support creating a transfer with more than one file */
    if (!(g_file_test(filename,G_FILE_TEST_EXISTS) != 0)) {
      continue; 
    }
/* XXX - make ft api suupport sending a directory */
/* Are we dealing with a directory? */
    if (g_file_test(filename,G_FILE_TEST_IS_DIR) != 0) {
      char *str;
      char *str2;
      str = g_strdup_printf(((const char *)(dgettext("pidgin","Cannot send folder %s."))),basename);
      str2 = g_strdup_printf(((const char *)(dgettext("pidgin","%s cannot transfer a folder. You will need to send the files within individually."))),((const char *)(dgettext("pidgin","Pidgin"))));
      purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,str,str2,0,0);
      g_free(str);
      g_free(str2);
      continue; 
    }
/* Are we dealing with an image? */
    pb = pidgin_pixbuf_new_from_file(filename);
    if (pb != 0) {
      _DndData *data = (g_malloc((sizeof(_DndData ))));
      gboolean ft = 0;
      gboolean im = 0;
      data -> who = g_strdup(who);
      data -> filename = g_strdup(filename);
      data -> account = account;
      if (gc != 0) 
        prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info);
      if ((prpl_info != 0) && (((prpl_info -> options) & OPT_PROTO_IM_IMAGE) != 0U)) 
        im = (!0);
      if ((prpl_info != 0) && ((prpl_info -> can_receive_file) != 0)) 
        ft = ( *(prpl_info -> can_receive_file))(gc,who);
      else if ((prpl_info != 0) && ((prpl_info -> send_file) != 0)) 
        ft = (!0);
      if ((im != 0) && (ft != 0)) 
        purple_request_choice(0,0,((const char *)(dgettext("pidgin","You have dragged an image"))),((const char *)(dgettext("pidgin","You can send this image as a file transfer, embed it into this message, or use it as the buddy icon for this user."))),DND_FILE_TRANSFER,((const char *)(dgettext("pidgin","OK"))),((GCallback )dnd_image_ok_callback),((const char *)(dgettext("pidgin","Cancel"))),((GCallback )dnd_image_cancel_callback),account,who,0,data,((const char *)(dgettext("pidgin","Set as buddy icon"))),DND_BUDDY_ICON,((const char *)(dgettext("pidgin","Send image file"))),DND_FILE_TRANSFER,((const char *)(dgettext("pidgin","Insert in message"))),DND_IM_IMAGE,((void *)((void *)0)));
      else if (!((im != 0) || (ft != 0))) 
        purple_request_action(0,0,((const char *)(dgettext("pidgin","You have dragged an image"))),((const char *)(dgettext("pidgin","Would you like to set it as the buddy icon for this user\?"))),-1,account,who,0,data,2,((const char *)(dgettext("pidgin","_Yes"))),((GCallback )dnd_set_icon_ok_cb),((const char *)(dgettext("pidgin","_No"))),((GCallback )dnd_set_icon_cancel_cb));
      else 
        purple_request_choice(0,0,((const char *)(dgettext("pidgin","You have dragged an image"))),((ft != 0)?((const char *)(dgettext("pidgin","You can send this image as a file transfer, or use it as the buddy icon for this user."))) : ((const char *)(dgettext("pidgin","You can insert this image into this message, or use it as the buddy icon for this user")))),((ft != 0)?DND_FILE_TRANSFER : DND_IM_IMAGE),((const char *)(dgettext("pidgin","OK"))),((GCallback )dnd_image_ok_callback),((const char *)(dgettext("pidgin","Cancel"))),((GCallback )dnd_image_cancel_callback),account,who,0,data,((const char *)(dgettext("pidgin","Set as buddy icon"))),DND_BUDDY_ICON,((ft != 0)?((const char *)(dgettext("pidgin","Send image file"))) : ((const char *)(dgettext("pidgin","Insert in message")))),((ft != 0)?DND_FILE_TRANSFER : DND_IM_IMAGE),((void *)((void *)0)));
      g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pb),((GType )(20 << 2))))));
      g_free(basename);
      while(files != 0){
        g_free((files -> data));
        files = g_list_delete_link(files,files);
      }
      return ;
    }
    else 
#ifndef _WIN32
/* Are we trying to send a .desktop file? */
if ((purple_str_has_suffix(basename,".desktop") != 0) && ((item = purple_desktop_item_new_from_file(filename)) != 0)) {
      PurpleDesktopItemType dtype;
      char key[64UL];
      const char *itemname = (const char *)((void *)0);
      const char *const *langs;
      langs = g_get_language_names();
      if (langs[0] != 0) {
        g_snprintf(key,(sizeof(key)),"Name[%s]",langs[0]);
        itemname = purple_desktop_item_get_string(item,key);
      }
      if (!(itemname != 0)) 
        itemname = purple_desktop_item_get_string(item,"Name");
      dtype = purple_desktop_item_get_entry_type(item);
      switch(dtype){
        PurpleConversation *conv;
        PidginConversation *gtkconv;
        case PURPLE_DESKTOP_ITEM_TYPE_LINK:
{
          conv = purple_conversation_new(PURPLE_CONV_TYPE_IM,account,who);
          gtkconv = ((PidginConversation *)(conv -> ui_data));
          gtk_imhtml_insert_link(((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type()))),gtk_text_buffer_get_insert(( *((GtkIMHtml *)(g_type_check_instance_cast(((GTypeInstance *)(gtkconv -> entry)),gtk_imhtml_get_type())))).text_buffer),purple_desktop_item_get_string(item,"URL"),itemname);
          break; 
        }
        default:
{
/* I don't know if we really want to do anything here.  Most of
				 * the desktop item types are crap like "MIME Type" (I have no
				 * clue how that would be a desktop item) and "Comment"...
				 * nothing we can really send.  The only logical one is
				 * "Application," but do we really want to send a binary and
				 * nothing else? Probably not.  I'll just give an error and
				 * return. */
/* The original patch sent the icon used by the launcher.  That's probably wrong */
          purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Cannot send launcher"))),((const char *)(dgettext("pidgin","You dragged a desktop launcher. Most likely you wanted to send the target of this launcher instead of this launcher itself."))),0,0);
          break; 
        }
      }
      purple_desktop_item_unref(item);
      g_free(basename);
      while(files != 0){
        g_free((files -> data));
        files = g_list_delete_link(files,files);
      }
      return ;
    }
#endif /* _WIN32 */
/* Everything is fine, let's send */
    serv_send_file(gc,who,filename);
  }
  g_free(filename);
  g_free(basename);
}

void pidgin_buddy_icon_get_scale_size(GdkPixbuf *buf,PurpleBuddyIconSpec *spec,PurpleIconScaleRules rules,int *width,int *height)
{
   *width = gdk_pixbuf_get_width(buf);
   *height = gdk_pixbuf_get_height(buf);
  if ((spec == ((PurpleBuddyIconSpec *)((void *)0))) || !(((spec -> scale_rules) & rules) != 0U)) 
    return ;
  purple_buddy_icon_get_scale_size(spec,width,height);
/* and now for some arbitrary sanity checks */
  if ( *width > 100) 
     *width = 100;
  if ( *height > 100) 
     *height = 100;
}

GdkPixbuf *pidgin_create_status_icon(PurpleStatusPrimitive prim,GtkWidget *w,const char *size)
{
  GtkIconSize icon_size = gtk_icon_size_from_name(size);
  GdkPixbuf *pixbuf = (GdkPixbuf *)((void *)0);
  const char *stock = pidgin_stock_id_from_status_primitive(prim);
  pixbuf = gtk_widget_render_icon(w,((stock != 0)?stock : "pidgin-status-available"),icon_size,"GtkWidget");
  return pixbuf;
}

static const char *stock_id_from_status_primitive_idle(PurpleStatusPrimitive prim,gboolean idle)
{
  const char *stock = (const char *)((void *)0);
  switch(prim){
    case PURPLE_STATUS_UNSET:
{
      stock = ((const char *)((void *)0));
      break; 
    }
    case PURPLE_STATUS_UNAVAILABLE:
{
      stock = (((idle != 0)?"pidgin-status-busy-i" : "pidgin-status-busy"));
      break; 
    }
    case PURPLE_STATUS_AWAY:
{
      stock = (((idle != 0)?"pidgin-status-away-i" : "pidgin-status-away"));
      break; 
    }
    case PURPLE_STATUS_EXTENDED_AWAY:
{
      stock = (((idle != 0)?"pidgin-status-xa-i" : "pidgin-status-xa"));
      break; 
    }
    case PURPLE_STATUS_INVISIBLE:
{
      stock = "pidgin-status-invisible";
      break; 
    }
    case PURPLE_STATUS_OFFLINE:
{
      stock = (((idle != 0)?"pidgin-status-offline" : "pidgin-status-offline"));
      break; 
    }
    default:
{
      stock = (((idle != 0)?"pidgin-status-available-i" : "pidgin-status-available"));
      break; 
    }
  }
  return stock;
}

const char *pidgin_stock_id_from_status_primitive(PurpleStatusPrimitive prim)
{
  return stock_id_from_status_primitive_idle(prim,0);
}

const char *pidgin_stock_id_from_presence(PurplePresence *presence)
{
  PurpleStatus *status;
  PurpleStatusType *type;
  PurpleStatusPrimitive prim;
  gboolean idle;
  do {
    if (presence != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"presence");
      return 0;
    };
  }while (0);
  status = purple_presence_get_active_status(presence);
  type = purple_status_get_type(status);
  prim = purple_status_type_get_primitive(type);
  idle = purple_presence_is_idle(presence);
  return stock_id_from_status_primitive_idle(prim,idle);
}

GdkPixbuf *pidgin_create_prpl_icon(PurpleAccount *account,PidginPrplIconSize size)
{
  PurplePlugin *prpl;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  prpl = purple_find_prpl(purple_account_get_protocol_id(account));
  if (prpl == ((PurplePlugin *)((void *)0))) 
    return 0;
  return pidgin_create_prpl_icon_from_prpl(prpl,size,account);
}

static void menu_action_cb(GtkMenuItem *item,gpointer object)
{
  gpointer data;
  void (*callback)(gpointer , gpointer );
  callback = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"purplecallback"));
  data = g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"purplecallbackdata");
  if (callback != 0) 
    ( *callback)(object,data);
}

GtkWidget *pidgin_append_menu_action(GtkWidget *menu,PurpleMenuAction *act,gpointer object)
{
  GtkWidget *menuitem;
  if (act == ((PurpleMenuAction *)((void *)0))) {
    return pidgin_separator(menu);
  }
  if ((act -> children) == ((GList *)((void *)0))) {
    menuitem = gtk_menu_item_new_with_mnemonic((act -> label));
    if ((act -> callback) != ((void (*)())((void *)0))) {
      g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"purplecallback",(act -> callback));
      g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"purplecallbackdata",(act -> data));
      g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),((GType )(20 << 2))))),"activate",((GCallback )menu_action_cb),object,0,((GConnectFlags )0));
    }
    else {
      gtk_widget_set_sensitive(menuitem,0);
    }
    gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem);
  }
  else {
    GList *l = (GList *)((void *)0);
    GtkWidget *submenu = (GtkWidget *)((void *)0);
    GtkAccelGroup *group;
    menuitem = gtk_menu_item_new_with_mnemonic((act -> label));
    gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),menuitem);
    submenu = gtk_menu_new();
    gtk_menu_item_set_submenu(((GtkMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_menu_item_get_type()))),submenu);
    group = gtk_menu_get_accel_group(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_get_type()))));
    if (group != 0) {
      char *path = g_strdup_printf("%s/%s",( *((GtkMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)menuitem),gtk_menu_item_get_type())))).accel_path,(act -> label));
      gtk_menu_set_accel_path(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)submenu),gtk_menu_get_type()))),path);
      g_free(path);
      gtk_menu_set_accel_group(((GtkMenu *)(g_type_check_instance_cast(((GTypeInstance *)submenu),gtk_menu_get_type()))),group);
    }
    for (l = (act -> children); l != 0; l = (l -> next)) {
      PurpleMenuAction *act = (PurpleMenuAction *)(l -> data);
      pidgin_append_menu_action(submenu,act,object);
    }
    g_list_free((act -> children));
    act -> children = ((GList *)((void *)0));
  }
  purple_menu_action_free(act);
  return menuitem;
}
typedef struct __unnamed_class___F0_L1887_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__entry__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__accountopt__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L1657R_variable_name_unknown_scope_and_name__scope__filter_func__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L55R_variable_name_unknown_scope_and_name__scope__filter_func_user_data__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L1172R__Pe___variable_name_unknown_scope_and_name__scope__store {
GtkWidget *entry;
GtkWidget *accountopt;
PidginFilterBuddyCompletionEntryFunc filter_func;
gpointer filter_func_user_data;
GtkListStore *store;}PidginCompletionData;

static gboolean buddyname_completion_match_func(GtkEntryCompletion *completion,const gchar *key,GtkTreeIter *iter,gpointer user_data)
{
  GtkTreeModel *model;
  GValue val1;
  GValue val2;
  const char *tmp;
  model = gtk_entry_completion_get_model(completion);
  val1.g_type = 0;
  gtk_tree_model_get_value(model,iter,2,&val1);
  tmp = g_value_get_string((&val1));
  if ((tmp != ((const char *)((void *)0))) && (purple_str_has_prefix(tmp,key) != 0)) {
    g_value_unset(&val1);
    return (!0);
  }
  g_value_unset(&val1);
  val2.g_type = 0;
  gtk_tree_model_get_value(model,iter,3,&val2);
  tmp = g_value_get_string((&val2));
  if ((tmp != ((const char *)((void *)0))) && (purple_str_has_prefix(tmp,key) != 0)) {
    g_value_unset(&val2);
    return (!0);
  }
  g_value_unset(&val2);
  return 0;
}

static gboolean buddyname_completion_match_selected_cb(GtkEntryCompletion *completion,GtkTreeModel *model,GtkTreeIter *iter,PidginCompletionData *data)
{
  GValue val;
  GtkWidget *optmenu = (data -> accountopt);
  PurpleAccount *account;
  val.g_type = 0;
  gtk_tree_model_get_value(model,iter,1,&val);
  gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)(data -> entry)),gtk_entry_get_type()))),g_value_get_string((&val)));
  g_value_unset(&val);
  gtk_tree_model_get_value(model,iter,4,&val);
  account = (g_value_get_pointer((&val)));
  g_value_unset(&val);
  if (account == ((PurpleAccount *)((void *)0))) 
    return (!0);
  if (optmenu != ((GtkWidget *)((void *)0))) 
    aop_option_menu_select_by_data(optmenu,account);
  return (!0);
}

static void add_buddyname_autocomplete_entry(GtkListStore *store,const char *buddy_alias,const char *contact_alias,const PurpleAccount *account,const char *buddyname)
{
  GtkTreeIter iter;
  gboolean completion_added = 0;
  gchar *normalized_buddyname;
  gchar *tmp;
  tmp = g_utf8_normalize(buddyname,(-1),G_NORMALIZE_DEFAULT);
  normalized_buddyname = g_utf8_casefold(tmp,(-1));
  g_free(tmp);
/* There's no sense listing things like: 'xxx "xxx"'
	   when the name and buddy alias match. */
  if ((buddy_alias != 0) && (strcmp(buddy_alias,buddyname) != 0)) {
    char *completion_entry = g_strdup_printf("%s \"%s\"",buddyname,buddy_alias);
    char *tmp2 = g_utf8_normalize(buddy_alias,(-1),G_NORMALIZE_DEFAULT);
    tmp = g_utf8_casefold(tmp2,(-1));
    g_free(tmp2);
    gtk_list_store_append(store,&iter);
    gtk_list_store_set(store,&iter,0,completion_entry,1,buddyname,2,normalized_buddyname,3,tmp,4,account,-1);
    g_free(completion_entry);
    g_free(tmp);
    completion_added = (!0);
  }
/* There's no sense listing things like: 'xxx "xxx"'
	   when the name and contact alias match. */
  if ((contact_alias != 0) && (strcmp(contact_alias,buddyname) != 0)) {
/* We don't want duplicates when the contact and buddy alias match. */
    if (!(buddy_alias != 0) || (strcmp(contact_alias,buddy_alias) != 0)) {
      char *completion_entry = g_strdup_printf("%s \"%s\"",buddyname,contact_alias);
      char *tmp2 = g_utf8_normalize(contact_alias,(-1),G_NORMALIZE_DEFAULT);
      tmp = g_utf8_casefold(tmp2,(-1));
      g_free(tmp2);
      gtk_list_store_append(store,&iter);
      gtk_list_store_set(store,&iter,0,completion_entry,1,buddyname,2,normalized_buddyname,3,tmp,4,account,-1);
      g_free(completion_entry);
      g_free(tmp);
      completion_added = (!0);
    }
  }
  if (completion_added == 0) {
/* Add the buddy's name. */
    gtk_list_store_append(store,&iter);
    gtk_list_store_set(store,&iter,0,buddyname,1,buddyname,2,normalized_buddyname,3,((void *)((void *)0)),4,account,-1);
  }
  g_free(normalized_buddyname);
}

static void get_log_set_name(PurpleLogSet *set,gpointer value,PidginCompletionData *data)
{
  PidginFilterBuddyCompletionEntryFunc filter_func = (data -> filter_func);
  gpointer user_data = (data -> filter_func_user_data);
/* 1. Don't show buddies because we will have gotten them already.
	 * 2. The boxes that use this autocomplete code handle only IMs. */
  if (!((set -> buddy) != 0) && ((set -> type) == PURPLE_LOG_IM)) {
    PidginBuddyCompletionEntry entry;
    entry.is_buddy = 0;
    entry.entry.logged_buddy = set;
    if (( *filter_func)((&entry),user_data) != 0) {
      add_buddyname_autocomplete_entry((data -> store),0,0,(set -> account),(set -> name));
    }
  }
}

static void add_completion_list(PidginCompletionData *data)
{
  PurpleBlistNode *gnode;
  PurpleBlistNode *cnode;
  PurpleBlistNode *bnode;
  PidginFilterBuddyCompletionEntryFunc filter_func = (data -> filter_func);
  gpointer user_data = (data -> filter_func_user_data);
  GHashTable *sets;
  gtk_list_store_clear((data -> store));
  for (gnode = ( *purple_get_blist()).root; gnode != ((PurpleBlistNode *)((void *)0)); gnode = (gnode -> next)) {
    if (!((purple_blist_node_get_type(gnode)) == PURPLE_BLIST_GROUP_NODE)) 
      continue; 
    for (cnode = (gnode -> child); cnode != ((PurpleBlistNode *)((void *)0)); cnode = (cnode -> next)) {
      if (!((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CONTACT_NODE)) 
        continue; 
      for (bnode = (cnode -> child); bnode != ((PurpleBlistNode *)((void *)0)); bnode = (bnode -> next)) {
        PidginBuddyCompletionEntry entry;
        entry.is_buddy = (!0);
        entry.entry.buddy = ((PurpleBuddy *)bnode);
        if (( *filter_func)((&entry),user_data) != 0) {
          add_buddyname_autocomplete_entry((data -> store),( *((PurpleContact *)cnode)).alias,purple_buddy_get_contact_alias(entry.entry.buddy),( *entry.entry.buddy).account,( *entry.entry.buddy).name);
        }
      }
    }
  }
  sets = purple_log_get_log_sets();
  g_hash_table_foreach(sets,((GHFunc )get_log_set_name),data);
  g_hash_table_destroy(sets);
}

static void buddyname_autocomplete_destroyed_cb(GtkWidget *widget,gpointer data)
{
  g_free(data);
  purple_signals_disconnect_by_handle(widget);
}

static void repopulate_autocomplete(gpointer something,gpointer data)
{
  add_completion_list(data);
}

void pidgin_setup_screenname_autocomplete_with_filter(GtkWidget *entry,GtkWidget *accountopt,PidginFilterBuddyCompletionEntryFunc filter_func,gpointer user_data)
{
  PidginCompletionData *data;
/*
	 * Store the displayed completion value, the buddy name, the UTF-8
	 * normalized & casefolded buddy name, the UTF-8 normalized &
	 * casefolded value for comparison, and the account.
	 */
  GtkListStore *store;
  GtkEntryCompletion *completion;
  data = ((PidginCompletionData *)(g_malloc0_n(1,(sizeof(PidginCompletionData )))));
  store = gtk_list_store_new(5,((GType )(16 << 2)),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(17 << 2)));
  data -> entry = entry;
  data -> accountopt = accountopt;
  if (filter_func == ((gboolean (*)(const PidginBuddyCompletionEntry *, gpointer ))((void *)0))) {
    data -> filter_func = pidgin_screenname_autocomplete_default_filter;
    data -> filter_func_user_data = ((gpointer )((void *)0));
  }
  else {
    data -> filter_func = filter_func;
    data -> filter_func_user_data = user_data;
  }
  data -> store = store;
  add_completion_list(data);
/* Sort the completion list by buddy name */
  gtk_tree_sortable_set_sort_column_id(((GtkTreeSortable *)(g_type_check_instance_cast(((GTypeInstance *)store),gtk_tree_sortable_get_type()))),1,GTK_SORT_ASCENDING);
  completion = gtk_entry_completion_new();
  gtk_entry_completion_set_match_func(completion,buddyname_completion_match_func,0,0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)completion),((GType )(20 << 2))))),"match-selected",((GCallback )buddyname_completion_match_selected_cb),data,0,((GConnectFlags )0));
  gtk_entry_set_completion(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)entry),gtk_entry_get_type()))),completion);
  g_object_unref(completion);
  gtk_entry_completion_set_model(completion,((GtkTreeModel *)(g_type_check_instance_cast(((GTypeInstance *)store),gtk_tree_model_get_type()))));
  g_object_unref(store);
  gtk_entry_completion_set_text_column(completion,0);
  purple_signal_connect(purple_connections_get_handle(),"signed-on",entry,((PurpleCallback )repopulate_autocomplete),data);
  purple_signal_connect(purple_connections_get_handle(),"signed-off",entry,((PurpleCallback )repopulate_autocomplete),data);
  purple_signal_connect(purple_accounts_get_handle(),"account-added",entry,((PurpleCallback )repopulate_autocomplete),data);
  purple_signal_connect(purple_accounts_get_handle(),"account-removed",entry,((PurpleCallback )repopulate_autocomplete),data);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)entry),((GType )(20 << 2))))),"destroy",((GCallback )buddyname_autocomplete_destroyed_cb),data,0,((GConnectFlags )0));
}

gboolean pidgin_screenname_autocomplete_default_filter(const PidginBuddyCompletionEntry *completion_entry,gpointer all_accounts)
{
  gboolean all = (gint )((glong )all_accounts);
  if ((completion_entry -> is_buddy) != 0) {
    return (all != 0) || (purple_account_is_connected(( *completion_entry -> entry.buddy).account) != 0);
  }
  else {
    return (all != 0) || ((( *completion_entry -> entry.logged_buddy).account != ((PurpleAccount *)((void *)0))) && (purple_account_is_connected(( *completion_entry -> entry.logged_buddy).account) != 0));
  }
}

void pidgin_setup_screenname_autocomplete(GtkWidget *entry,GtkWidget *accountopt,gboolean all)
{
  pidgin_setup_screenname_autocomplete_with_filter(entry,accountopt,pidgin_screenname_autocomplete_default_filter,((gpointer )((glong )all)));
}

void pidgin_set_cursor(GtkWidget *widget,GdkCursorType cursor_type)
{
  GdkCursor *cursor;
  do {
    if (widget != ((GtkWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"widget != NULL");
      return ;
    };
  }while (0);
  if ((widget -> window) == ((GdkWindow *)((void *)0))) 
    return ;
  cursor = gdk_cursor_new(cursor_type);
  gdk_window_set_cursor((widget -> window),cursor);
  gdk_cursor_unref(cursor);
  gdk_display_flush(gdk_drawable_get_display(((GdkDrawable *)(g_type_check_instance_cast(((GTypeInstance *)(widget -> window)),gdk_drawable_get_type())))));
}

void pidgin_clear_cursor(GtkWidget *widget)
{
  do {
    if (widget != ((GtkWidget *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"widget != NULL");
      return ;
    };
  }while (0);
  if ((widget -> window) == ((GdkWindow *)((void *)0))) 
    return ;
  gdk_window_set_cursor((widget -> window),0);
}

struct _icon_chooser 
{
  GtkWidget *icon_filesel;
  GtkWidget *icon_preview;
  GtkWidget *icon_text;
  void (*callback)(const char *, gpointer );
  gpointer data;
}
;

static void icon_filesel_choose_cb(GtkWidget *widget,gint response,struct _icon_chooser *dialog)
{
  char *filename;
  char *current_folder;
  if (response != GTK_RESPONSE_ACCEPT) {
    if (response == GTK_RESPONSE_CANCEL) {
      gtk_widget_destroy((dialog -> icon_filesel));
    }
    dialog -> icon_filesel = ((GtkWidget *)((void *)0));
    if ((dialog -> callback) != 0) 
      ( *(dialog -> callback))(0,(dialog -> data));
    g_free(dialog);
    return ;
  }
  filename = gtk_file_chooser_get_filename(((GtkFileChooser *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_filesel)),gtk_file_chooser_get_type()))));
  current_folder = gtk_file_chooser_get_current_folder(((GtkFileChooser *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_filesel)),gtk_file_chooser_get_type()))));
  if (current_folder != ((char *)((void *)0))) {
    purple_prefs_set_path("/pidgin/filelocations/last_icon_folder",current_folder);
    g_free(current_folder);
  }
  if ((dialog -> callback) != 0) 
    ( *(dialog -> callback))(filename,(dialog -> data));
  gtk_widget_destroy((dialog -> icon_filesel));
  g_free(filename);
  g_free(dialog);
}

static void icon_preview_change_cb(GtkFileChooser *widget,struct _icon_chooser *dialog)
{
  GdkPixbuf *pixbuf;
  int height;
  int width;
  char *basename;
  char *markup;
  char *size;
  struct stat st;
  char *filename;
  filename = gtk_file_chooser_get_preview_filename(((GtkFileChooser *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_filesel)),gtk_file_chooser_get_type()))));
  if ((!(filename != 0) || (stat(filename,&st) != 0)) || !((pixbuf = pidgin_pixbuf_new_from_file_at_size(filename,128,128)) != 0)) {
    gtk_image_set_from_pixbuf(((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_preview)),gtk_image_get_type()))),0);
    gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_text)),gtk_label_get_type()))),"");
    g_free(filename);
    return ;
  }
  gdk_pixbuf_get_file_info(filename,&width,&height);
  basename = g_path_get_basename(filename);
  size = purple_str_size_to_units(st.st_size);
  markup = g_strdup_printf(((const char *)(dgettext("pidgin","<b>File:</b> %s\n<b>File size:</b> %s\n<b>Image size:</b> %dx%d"))),basename,size,width,height);
  gtk_image_set_from_pixbuf(((GtkImage *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_preview)),gtk_image_get_type()))),pixbuf);
  gtk_label_set_markup(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_text)),gtk_label_get_type()))),markup);
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
  g_free(filename);
  g_free(basename);
  g_free(size);
  g_free(markup);
}

GtkWidget *pidgin_buddy_icon_chooser_new(GtkWindow *parent,void (*callback)(const char *, gpointer ),gpointer data)
{
  struct _icon_chooser *dialog = (struct _icon_chooser *)(g_malloc0_n(1,(sizeof(struct _icon_chooser ))));
  GtkWidget *vbox;
  const char *current_folder;
  dialog -> callback = callback;
  dialog -> data = data;
  current_folder = purple_prefs_get_path("/pidgin/filelocations/last_icon_folder");
  dialog -> icon_filesel = gtk_file_chooser_dialog_new(((const char *)(dgettext("pidgin","Buddy Icon"))),parent,GTK_FILE_CHOOSER_ACTION_OPEN,"gtk-cancel",GTK_RESPONSE_CANCEL,"gtk-open",GTK_RESPONSE_ACCEPT,((void *)((void *)0)));
  gtk_dialog_set_default_response(((GtkDialog *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_filesel)),gtk_dialog_get_type()))),GTK_RESPONSE_ACCEPT);
  if ((current_folder != ((const char *)((void *)0))) && (( *current_folder) != 0)) 
    gtk_file_chooser_set_current_folder(((GtkFileChooser *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_filesel)),gtk_file_chooser_get_type()))),current_folder);
  dialog -> icon_preview = gtk_image_new();
  dialog -> icon_text = gtk_label_new(0);
  vbox = gtk_vbox_new(0,6);
  gtk_widget_set_size_request(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_widget_get_type()))),(-1),50);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_preview)),gtk_widget_get_type()))),(!0),0,0);
  gtk_box_pack_end(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_box_get_type()))),((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_text)),gtk_widget_get_type()))),0,0,0);
  gtk_widget_show_all(vbox);
  gtk_file_chooser_set_preview_widget(((GtkFileChooser *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_filesel)),gtk_file_chooser_get_type()))),vbox);
  gtk_file_chooser_set_preview_widget_active(((GtkFileChooser *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_filesel)),gtk_file_chooser_get_type()))),(!0));
  gtk_file_chooser_set_use_preview_label(((GtkFileChooser *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_filesel)),gtk_file_chooser_get_type()))),0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_filesel)),((GType )(20 << 2))))),"update-preview",((GCallback )icon_preview_change_cb),dialog,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(dialog -> icon_filesel)),((GType )(20 << 2))))),"response",((GCallback )icon_filesel_choose_cb),dialog,0,((GConnectFlags )0));
  icon_preview_change_cb(0,dialog);
#ifdef _WIN32
#endif
  return dialog -> icon_filesel;
}
/**
 * @return True if any string from array a exists in array b.
 */

static gboolean str_array_match(char **a,char **b)
{
  int i;
  int j;
  if (!(a != 0) || !(b != 0)) 
    return 0;
  for (i = 0; a[i] != ((char *)((void *)0)); i++) 
    for (j = 0; b[j] != ((char *)((void *)0)); j++) 
      if (!(g_ascii_strcasecmp(a[i],b[j]) != 0)) 
        return (!0);
  return 0;
}

gpointer pidgin_convert_buddy_icon(PurplePlugin *plugin,const char *path,size_t *len)
{
  PurplePluginProtocolInfo *prpl_info;
  PurpleBuddyIconSpec *spec;
  int orig_width;
  int orig_height;
  int new_width;
  int new_height;
  GdkPixbufFormat *format;
  char **pixbuf_formats;
  char **prpl_formats;
  GError *error = (GError *)((void *)0);
  gchar *contents;
  gsize length;
  GdkPixbuf *pixbuf;
  GdkPixbuf *original;
  float scale_factor;
  int i;
  gchar *tmp;
  prpl_info = ((PurplePluginProtocolInfo *)( *(plugin -> info)).extra_info);
  spec = &prpl_info -> icon_spec;
  do {
    if ((spec -> format) != ((char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"spec->format != NULL");
      return 0;
    };
  }while (0);
  format = gdk_pixbuf_get_file_info(path,&orig_width,&orig_height);
  if (format == ((GdkPixbufFormat *)((void *)0))) {
    purple_debug_warning("buddyicon","Could not get file info of %s\n",path);
    return 0;
  }
  pixbuf_formats = gdk_pixbuf_format_get_extensions(format);
  prpl_formats = g_strsplit((spec -> format),",",0);
/* This is an acceptable format AND */
  if ((str_array_match(pixbuf_formats,prpl_formats) != 0) && (!(((spec -> scale_rules) & PURPLE_ICON_SCALE_SEND) != 0U) || (((((spec -> min_width) <= orig_width) && ((spec -> max_width) >= orig_width)) && ((spec -> min_height) <= orig_height)) && ((spec -> max_height) >= orig_height)))) 
/* The prpl doesn't scale before it sends OR */
/* The icon is the correct size */
{
    g_strfreev(pixbuf_formats);
    if (!(g_file_get_contents(path,&contents,&length,&error) != 0)) {
      purple_debug_warning("buddyicon","Could not get file contents of %s: %s\n",path,(error -> message));
      g_strfreev(prpl_formats);
      return 0;
    }
    if (((spec -> max_filesize) == 0) || (length < (spec -> max_filesize))) {
/* The supplied image fits the file size, dimensions and type
			   constraints.  Great!  Return it without making any changes. */
      if (len != 0) 
         *len = length;
      g_strfreev(prpl_formats);
      return contents;
    }
/* The image was too big.  Fall-through and try scaling it down. */
    g_free(contents);
  }
  else {
    g_strfreev(pixbuf_formats);
  }
/* The original image wasn't compatible.  Scale it or convert file type. */
  pixbuf = gdk_pixbuf_new_from_file(path,&error);
  if (error != 0) {
    purple_debug_warning("buddyicon","Could not open icon \'%s\' for conversion: %s\n",path,(error -> message));
    g_error_free(error);
    g_strfreev(prpl_formats);
    return 0;
  }
  original = (g_object_ref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2)))))));
  new_width = orig_width;
  new_height = orig_height;
/* Make sure the image is the correct dimensions */
  if ((((spec -> scale_rules) & PURPLE_ICON_SCALE_SEND) != 0U) && ((((orig_width < (spec -> min_width)) || (orig_width > (spec -> max_width))) || (orig_height < (spec -> min_height))) || (orig_height > (spec -> max_height)))) {
    purple_buddy_icon_get_scale_size(spec,&new_width,&new_height);
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
    pixbuf = gdk_pixbuf_scale_simple(original,new_width,new_height,GDK_INTERP_HYPER);
  }
  scale_factor = 1;
  do {
    for (i = 0; prpl_formats[i] != 0; i++) {
      int quality = 100;
{
        do {
          const char *key = (const char *)((void *)0);
          const char *value = (const char *)((void *)0);
          gchar tmp_buf[4UL];
          purple_debug_info("buddyicon","Converting buddy icon to %s\n",prpl_formats[i]);
          if (g_str_equal(prpl_formats[i],"png") != 0) {
            key = "compression";
            value = "9";
          }
          else if (g_str_equal(prpl_formats[i],"jpeg") != 0) {
            sprintf(tmp_buf,"%u",quality);
            key = "quality";
            value = tmp_buf;
          }
          if (!(gdk_pixbuf_save_to_buffer(pixbuf,&contents,&length,prpl_formats[i],&error,key,value,((void *)((void *)0))) != 0)) {
/* The NULL checking of error is necessary due to this bug:
					 * http://bugzilla.gnome.org/show_bug.cgi?id=405539 */
            purple_debug_warning("buddyicon","Could not convert to %s: %s\n",prpl_formats[i],(((error != 0) && ((error -> message) != 0))?(error -> message) : "Unknown error"));
            g_error_free(error);
            error = ((GError *)((void *)0));
/* We couldn't convert to this image type.  Try the next
					   image type. */
            break; 
          }
          if (((spec -> max_filesize) == 0) || (length <= (spec -> max_filesize))) {
/* We were able to save the image as this image type and
					   have it be within the size constraints.  Great!  Return
					   the image. */
            purple_debug_info("buddyicon","Converted image from %dx%d to %dx%d, format=%s, quality=%u, filesize=%zu\n",orig_width,orig_height,new_width,new_height,prpl_formats[i],quality,length);
            if (len != 0) 
               *len = length;
            g_strfreev(prpl_formats);
            g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
            g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)original),((GType )(20 << 2))))));
            return contents;
          }
          g_free(contents);
          if (!(g_str_equal(prpl_formats[i],"jpeg") != 0)) {
/* File size was too big and we can't lower the quality,
					   so skip to the next image type. */
            break; 
          }
/* File size was too big, but we're dealing with jpeg so try
				   lowering the quality. */
          quality -= 5;
        }while (quality >= 70);
      }
    }
/* We couldn't save the image in any format that was below the max
		   file size.  Maybe we can reduce the image dimensions? */
    scale_factor *= 0.8;
    new_width = (orig_width * scale_factor);
    new_height = (orig_height * scale_factor);
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
    pixbuf = gdk_pixbuf_scale_simple(original,new_width,new_height,GDK_INTERP_HYPER);
  }while ((((new_width > 10) || (new_height > 10)) && (new_width > (spec -> min_width))) && (new_height > (spec -> min_height)));
  g_strfreev(prpl_formats);
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)original),((GType )(20 << 2))))));
  tmp = g_strdup_printf(((const char *)(dgettext("pidgin","The file \'%s\' is too large for %s.  Please try a smaller image.\n"))),path,( *(plugin -> info)).name);
  purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Icon Error"))),((const char *)(dgettext("pidgin","Could not set icon"))),tmp,0,0);
  g_free(tmp);
  return 0;
}

void pidgin_set_custom_buddy_icon(PurpleAccount *account,const char *who,const char *filename)
{
  PurpleBuddy *buddy;
  PurpleContact *contact;
  buddy = purple_find_buddy(account,who);
  if (!(buddy != 0)) {
    purple_debug_info("custom-icon","You can only set custom icon for someone in your buddylist.\n");
    return ;
  }
  contact = purple_buddy_get_contact(buddy);
  purple_buddy_icons_node_set_custom_icon_from_file(((PurpleBlistNode *)contact),filename);
}

char *pidgin_make_pretty_arrows(const char *str)
{
  char *ret;
  char **split = g_strsplit(str,"->",(-1));
  ret = g_strjoinv("\342\207\250",split);
  g_strfreev(split);
  split = g_strsplit(ret,"<-",(-1));
  g_free(ret);
  ret = g_strjoinv("\342\207\246",split);
  g_strfreev(split);
  return ret;
}

void pidgin_set_urgent(GtkWindow *window,gboolean urgent)
{
#if defined _WIN32
#else
  gtk_window_set_urgency_hint(window,urgent);
#endif
}
static GSList *minidialogs = (GSList *)((void *)0);

static void *pidgin_utils_get_handle()
{
  static int handle;
  return (&handle);
}

static void connection_signed_off_cb(PurpleConnection *gc)
{
  GSList *list;
  GSList *l_next;
  for (list = minidialogs; list != 0; list = l_next) {
    l_next = (list -> next);
    if (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(list -> data)),((GType )(20 << 2))))),"gc") == gc) {
      gtk_widget_destroy(((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)(list -> data)),gtk_widget_get_type()))));
    }
  }
}

static void alert_killed_cb(GtkWidget *widget)
{
  minidialogs = g_slist_remove(minidialogs,widget);
}

struct _old_button_clicked_cb_data 
{
  PidginUtilMiniDialogCallback cb;
  gpointer data;
}
;

static void old_mini_dialog_button_clicked_cb(PidginMiniDialog *mini_dialog,GtkButton *button,gpointer user_data)
{
  struct _old_button_clicked_cb_data *data = user_data;
  ( *(data -> cb))((data -> data),button);
}

static void old_mini_dialog_destroy_cb(GtkWidget *dialog,GList *cb_datas)
{
  while(cb_datas != ((GList *)((void *)0))){
    g_free((cb_datas -> data));
    cb_datas = g_list_delete_link(cb_datas,cb_datas);
  }
}

static void mini_dialog_init(PidginMiniDialog *mini_dialog,PurpleConnection *gc,void *user_data,va_list args)
{
  const char *button_text;
  GList *cb_datas = (GList *)((void *)0);
  static gboolean first_call = (!0);
  if (first_call != 0) {
    first_call = 0;
    purple_signal_connect(purple_connections_get_handle(),"signed-off",pidgin_utils_get_handle(),((PurpleCallback )connection_signed_off_cb),0);
  }
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)mini_dialog),((GType )(20 << 2))))),"gc",gc);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)mini_dialog),((GType )(20 << 2))))),"destroy",((GCallback )alert_killed_cb),0,0,((GConnectFlags )0));
  while((button_text = (va_arg(args,char *))) != 0){
    struct _old_button_clicked_cb_data *data = (struct _old_button_clicked_cb_data *)((void *)0);
    PidginMiniDialogCallback wrapper_cb = (PidginMiniDialogCallback )((void *)0);
    PidginUtilMiniDialogCallback callback = va_arg(args,PidginUtilMiniDialogCallback );
    if (callback != ((void (*)(gpointer , GtkButton *))((void *)0))) {
      data = ((struct _old_button_clicked_cb_data *)(g_malloc0_n(1,(sizeof(struct _old_button_clicked_cb_data )))));
      data -> cb = callback;
      data -> data = user_data;
      wrapper_cb = old_mini_dialog_button_clicked_cb;
    }
    pidgin_mini_dialog_add_button(mini_dialog,button_text,wrapper_cb,data);
    cb_datas = g_list_append(cb_datas,data);
  }
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)mini_dialog),((GType )(20 << 2))))),"destroy",((GCallback )old_mini_dialog_destroy_cb),cb_datas,0,((GConnectFlags )0));
}
#define INIT_AND_RETURN_MINI_DIALOG(mini_dialog) \
	va_list args; \
	va_start(args, user_data); \
	mini_dialog_init(mini_dialog, gc, user_data, args); \
	va_end(args); \
	return GTK_WIDGET(mini_dialog);

GtkWidget *pidgin_make_mini_dialog(PurpleConnection *gc,const char *icon_name,const char *primary,const char *secondary,void *user_data,... )
{
  PidginMiniDialog *mini_dialog = pidgin_mini_dialog_new(primary,secondary,icon_name);
  va_list args;
  va_start(args,user_data);
  mini_dialog_init(mini_dialog,gc,user_data,args);
  va_end(args);
  return (GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)mini_dialog),gtk_widget_get_type()));;
}

GtkWidget *pidgin_make_mini_dialog_with_custom_icon(PurpleConnection *gc,GdkPixbuf *custom_icon,const char *primary,const char *secondary,void *user_data,... )
{
  PidginMiniDialog *mini_dialog = pidgin_mini_dialog_new_with_custom_icon(primary,secondary,custom_icon);
  va_list args;
  va_start(args,user_data);
  mini_dialog_init(mini_dialog,gc,user_data,args);
  va_end(args);
  return (GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)mini_dialog),gtk_widget_get_type()));;
}
/*
 * "This is so dead sexy."
 * "Two thumbs up."
 * "Best movie of the year."
 *
 * This is the function that handles CTRL+F searching in the buddy list.
 * It finds the top-most buddy/group/chat/whatever containing the
 * entered string.
 *
 * It's somewhat ineffecient, because we strip all the HTML from the
 * "name" column of the buddy list (because the GtkTreeModel does not
 * contain the screen name in a non-markedup format).  But the alternative
 * is to add an extra column to the GtkTreeModel.  And this function is
 * used rarely, so it shouldn't matter TOO much.
 */

gboolean pidgin_tree_view_search_equal_func(GtkTreeModel *model,gint column,const gchar *key,GtkTreeIter *iter,gpointer data)
{
  gchar *enteredstring;
  gchar *tmp;
  gchar *withmarkup;
  gchar *nomarkup;
  gchar *normalized;
  gboolean result;
  size_t i;
  size_t len;
  PangoLogAttr *log_attrs;
  gchar *word;
  if (g_ascii_strcasecmp(key,"Global Thermonuclear War") == 0) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_INFO,"WOPR","Wouldn\'t you prefer a nice game of chess\?",0,0,0);
    return 0;
  }
  gtk_tree_model_get(model,iter,column,&withmarkup,-1);
/* This is probably a separator */
  if (withmarkup == ((gchar *)((void *)0))) 
    return (!0);
  tmp = g_utf8_normalize(key,(-1),G_NORMALIZE_DEFAULT);
  enteredstring = g_utf8_casefold(tmp,(-1));
  g_free(tmp);
  nomarkup = purple_markup_strip_html(withmarkup);
  tmp = g_utf8_normalize(nomarkup,(-1),G_NORMALIZE_DEFAULT);
  g_free(nomarkup);
  normalized = g_utf8_casefold(tmp,(-1));
  g_free(tmp);
  if (purple_str_has_prefix(normalized,enteredstring) != 0) {
    g_free(withmarkup);
    g_free(enteredstring);
    g_free(normalized);
    return 0;
  }
/* Use Pango to separate by words. */
  len = (g_utf8_strlen(normalized,(-1)));
  log_attrs = ((PangoLogAttr *)(g_malloc_n((len + 1),(sizeof(PangoLogAttr )))));
  pango_get_log_attrs(normalized,(strlen(normalized)),-1,0,log_attrs,(len + 1));
  word = normalized;
  result = (!0);
{
    for (i = 0; i < (len - 1); i++) {
      if ((log_attrs[i].is_word_start != 0) && (purple_str_has_prefix(word,enteredstring) != 0)) {
        result = 0;
        break; 
      }
      word = (word + g_utf8_skip[ *((const guchar *)word)]);
    }
  }
  g_free(log_attrs);
/* The non-Pango version. */
#if 0
#endif
  g_free(withmarkup);
  g_free(enteredstring);
  g_free(normalized);
  return result;
}

gboolean pidgin_gdk_pixbuf_is_opaque(GdkPixbuf *pixbuf)
{
  int height;
  int rowstride;
  int i;
  unsigned char *pixels;
  unsigned char *row;
  if (!(gdk_pixbuf_get_has_alpha(pixbuf) != 0)) 
    return (!0);
  height = gdk_pixbuf_get_height(pixbuf);
  rowstride = gdk_pixbuf_get_rowstride(pixbuf);
  pixels = gdk_pixbuf_get_pixels(pixbuf);
  row = pixels;
  for (i = 3; i < rowstride; i += 4) {
    if (row[i] < 0xfe) 
      return 0;
  }
  for (i = 1; i < (height - 1); i++) {
    row = (pixels + (i * rowstride));
    if ((row[3] < 0xfe) || (row[rowstride - 1] < 0xfe)) {
      return 0;
    }
  }
  row = (pixels + ((height - 1) * rowstride));
  for (i = 3; i < rowstride; i += 4) {
    if (row[i] < 0xfe) 
      return 0;
  }
  return (!0);
}

void pidgin_gdk_pixbuf_make_round(GdkPixbuf *pixbuf)
{
  int width;
  int height;
  int rowstride;
  guchar *pixels;
  if (!(gdk_pixbuf_get_has_alpha(pixbuf) != 0)) 
    return ;
  width = gdk_pixbuf_get_width(pixbuf);
  height = gdk_pixbuf_get_height(pixbuf);
  rowstride = gdk_pixbuf_get_rowstride(pixbuf);
  pixels = gdk_pixbuf_get_pixels(pixbuf);
  if ((width < 6) || (height < 6)) 
    return ;
/* Top left */
  pixels[3] = 0;
  pixels[7] = 128;
  pixels[11] = 0xC0;
  pixels[rowstride + 3] = 128;
  pixels[(rowstride * 2) + 3] = 0xC0;
/* Top right */
  pixels[(width * 4) - 1] = 0;
  pixels[(width * 4) - 5] = 128;
  pixels[(width * 4) - 9] = 0xC0;
  pixels[(rowstride + (width * 4)) - 1] = 128;
  pixels[((2 * rowstride) + (width * 4)) - 1] = 0xC0;
/* Bottom left */
  pixels[((height - 1) * rowstride) + 3] = 0;
  pixels[((height - 1) * rowstride) + 7] = 128;
  pixels[((height - 1) * rowstride) + 11] = 0xC0;
  pixels[((height - 2) * rowstride) + 3] = 128;
  pixels[((height - 3) * rowstride) + 3] = 0xC0;
/* Bottom right */
  pixels[(height * rowstride) - 1] = 0;
  pixels[((height - 1) * rowstride) - 1] = 128;
  pixels[((height - 2) * rowstride) - 1] = 0xC0;
  pixels[(height * rowstride) - 5] = 128;
  pixels[(height * rowstride) - 9] = 0xC0;
}

const char *pidgin_get_dim_grey_string(GtkWidget *widget)
{
  static char dim_grey_string[8UL] = "";
  GtkStyle *style;
  if (!(widget != 0)) 
    return "dim grey";
  style = gtk_widget_get_style(widget);
  if (!(style != 0)) 
    return "dim grey";
  snprintf(dim_grey_string,(sizeof(dim_grey_string)),"#%02x%02x%02x",((style -> text_aa)[GTK_STATE_NORMAL].red >> 8),((style -> text_aa)[GTK_STATE_NORMAL].green >> 8),((style -> text_aa)[GTK_STATE_NORMAL].blue >> 8));
  return dim_grey_string;
}

static void combo_box_changed_cb(GtkComboBox *combo_box,GtkEntry *entry)
{
  char *text = gtk_combo_box_get_active_text(combo_box);
  gtk_entry_set_text(entry,((text != 0)?text : ""));
  g_free(text);
}

static gboolean entry_key_pressed_cb(GtkWidget *entry,GdkEventKey *key,GtkComboBox *combo)
{
  if (((key -> keyval) == 0xff54) || ((key -> keyval) == 0xff52)) {
    gtk_combo_box_popup(combo);
    return (!0);
  }
  return 0;
}

GtkWidget *pidgin_text_combo_box_entry_new(const char *default_item,GList *items)
{
  GtkComboBox *ret = (GtkComboBox *)((void *)0);
  GtkWidget *the_entry = (GtkWidget *)((void *)0);
  ret = ((GtkComboBox *)(g_type_check_instance_cast(((GTypeInstance *)(gtk_combo_box_entry_new_text())),gtk_combo_box_get_type())));
  the_entry = gtk_entry_new();
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_container_get_type()))),the_entry);
  if (default_item != 0) 
    gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)the_entry),gtk_entry_get_type()))),default_item);
  for (; items != ((GList *)((void *)0)); items = (items -> next)) {
    char *text = (items -> data);
    if ((text != 0) && (( *text) != 0)) 
      gtk_combo_box_append_text(ret,text);
  }
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)ret),((GType )(20 << 2))))),"changed",((GCallback )combo_box_changed_cb),the_entry,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)the_entry),((GType )(20 << 2))))),"key-press-event",((GCallback )entry_key_pressed_cb),ret,0,G_CONNECT_AFTER);
  return (GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)ret),gtk_widget_get_type()));
}

const char *pidgin_text_combo_box_entry_get_text(GtkWidget *widget)
{
  return gtk_entry_get_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkBin *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_bin_get_type())))).child),gtk_entry_get_type()))));
}

void pidgin_text_combo_box_entry_set_text(GtkWidget *widget,const char *text)
{
  gtk_entry_set_text(((GtkEntry *)(g_type_check_instance_cast(((GTypeInstance *)( *((GtkBin *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_bin_get_type())))).child),gtk_entry_get_type()))),text);
}

GtkWidget *pidgin_add_widget_to_vbox(GtkBox *vbox,const char *widget_label,GtkSizeGroup *sg,GtkWidget *widget,gboolean expand,GtkWidget **p_label)
{
  GtkWidget *hbox;
  GtkWidget *label = (GtkWidget *)((void *)0);
  if (widget_label != 0) {
    hbox = gtk_hbox_new(0,5);
    gtk_widget_show(hbox);
    gtk_box_pack_start(vbox,hbox,0,0,0);
    label = gtk_label_new_with_mnemonic(widget_label);
    gtk_widget_show(label);
    if (sg != 0) {
      gtk_misc_set_alignment(((GtkMisc *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_misc_get_type()))),0,0.5);
      gtk_size_group_add_widget(sg,label);
    }
    gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),label,0,0,0);
  }
  else {
    hbox = ((GtkWidget *)(g_type_check_instance_cast(((GTypeInstance *)vbox),gtk_widget_get_type())));
  }
  gtk_widget_show(widget);
  gtk_box_pack_start(((GtkBox *)(g_type_check_instance_cast(((GTypeInstance *)hbox),gtk_box_get_type()))),widget,expand,(!0),0);
  if (label != 0) {
    gtk_label_set_mnemonic_widget(((GtkLabel *)(g_type_check_instance_cast(((GTypeInstance *)label),gtk_label_get_type()))),widget);
    pidgin_set_accessible_label(widget,label);
  }
  if (p_label != 0) 
     *p_label = label;
  return hbox;
}

gboolean pidgin_auto_parent_window(GtkWidget *widget)
{
#if 0
/* This looks at the most recent window that received focus, and makes
	 * that the parent window. */
#ifndef _WIN32
/* The window is in focus, and the new window was not triggered by a keypress/click
			 * event. So do not set it transient, to avoid focus stealing and all that.
			 */
#endif
#else
/* This finds the currently active window and makes that the parent window. */
  GList *windows = (GList *)((void *)0);
  GtkWidget *parent = (GtkWidget *)((void *)0);
  GdkEvent *event = gtk_get_current_event();
  GdkWindow *menu = (GdkWindow *)((void *)0);
  if (event == ((GdkEvent *)((void *)0))) 
/* The window was not triggered by a user action. */
    return 0;
/* We need to special case events from a popup menu. */
  if ((event -> type) == GDK_BUTTON_RELEASE) {
/* XXX: Neither of the following works:
			menu = event->button.window;
			menu = gdk_window_get_parent(event->button.window);
			menu = gdk_window_get_toplevel(event->button.window);
		*/
  }
  else if ((event -> type) == GDK_KEY_PRESS) 
    menu = event -> key.window;
  windows = gtk_window_list_toplevels();
{
    while(windows != 0){{
        GtkWidget *window = (windows -> data);
        windows = g_list_delete_link(windows,windows);
        if ((window == widget) || !((( *((GtkObject *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_object_get_type())))).flags & GTK_VISIBLE) != 0)) {
          continue; 
        }
        if ((gtk_window_has_toplevel_focus(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)window),gtk_window_get_type())))) != 0) || ((menu != 0) && (menu == (window -> window)))) {
          parent = window;
          break; 
        }
      }
    }
  }
  if (windows != 0) 
    g_list_free(windows);
  if (parent != 0) {
    gtk_window_set_transient_for(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)widget),gtk_window_get_type()))),((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)parent),gtk_window_get_type()))));
    return (!0);
  }
  return 0;
#endif
}

static GObject *pidgin_pixbuf_from_data_helper(const guchar *buf,gsize count,gboolean animated)
{
  GObject *pixbuf;
  GdkPixbufLoader *loader;
  GError *error = (GError *)((void *)0);
  loader = gdk_pixbuf_loader_new();
  if (!(gdk_pixbuf_loader_write(loader,buf,count,&error) != 0) || (error != 0)) {
    purple_debug_warning("gtkutils","gdk_pixbuf_loader_write() failed with size=%zu: %s\n",count,((error != 0)?(error -> message) : "(no error message)"));
    if (error != 0) 
      g_error_free(error);
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)loader),((GType )(20 << 2))))));
    return 0;
  }
  if (!(gdk_pixbuf_loader_close(loader,&error) != 0) || (error != 0)) {
    purple_debug_warning("gtkutils","gdk_pixbuf_loader_close() failed for image of size %zu: %s\n",count,((error != 0)?(error -> message) : "(no error message)"));
    if (error != 0) 
      g_error_free(error);
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)loader),((GType )(20 << 2))))));
    return 0;
  }
  if (animated != 0) 
    pixbuf = ((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gdk_pixbuf_loader_get_animation(loader))),((GType )(20 << 2)))));
  else 
    pixbuf = ((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(gdk_pixbuf_loader_get_pixbuf(loader))),((GType )(20 << 2)))));
  if (!(pixbuf != 0)) {
    purple_debug_warning("gtkutils","%s() returned NULL for image of size %zu\n",((animated != 0)?"gdk_pixbuf_loader_get_animation" : "gdk_pixbuf_loader_get_pixbuf"),count);
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)loader),((GType )(20 << 2))))));
    return 0;
  }
  g_object_ref(pixbuf);
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)loader),((GType )(20 << 2))))));
  return pixbuf;
}

GdkPixbuf *pidgin_pixbuf_from_data(const guchar *buf,gsize count)
{
  return (GdkPixbuf *)(g_type_check_instance_cast(((GTypeInstance *)(pidgin_pixbuf_from_data_helper(buf,count,0))),gdk_pixbuf_get_type()));
}

GdkPixbufAnimation *pidgin_pixbuf_anim_from_data(const guchar *buf,gsize count)
{
  return (GdkPixbufAnimation *)(g_type_check_instance_cast(((GTypeInstance *)(pidgin_pixbuf_from_data_helper(buf,count,(!0)))),gdk_pixbuf_animation_get_type()));
}

GdkPixbuf *pidgin_pixbuf_from_imgstore(PurpleStoredImage *image)
{
  return pidgin_pixbuf_from_data((purple_imgstore_get_data(image)),purple_imgstore_get_size(image));
}

GdkPixbuf *pidgin_pixbuf_new_from_file(const gchar *filename)
{
  GdkPixbuf *pixbuf;
  GError *error = (GError *)((void *)0);
  pixbuf = gdk_pixbuf_new_from_file(filename,&error);
  if (!(pixbuf != 0) || (error != 0)) {
    purple_debug_warning("gtkutils","gdk_pixbuf_new_from_file() returned %s for file %s: %s\n",((pixbuf != 0)?"something" : "nothing"),filename,((error != 0)?(error -> message) : "(no error message)"));
    if (error != 0) 
      g_error_free(error);
    if (pixbuf != 0) 
      g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
    return 0;
  }
  return pixbuf;
}

GdkPixbuf *pidgin_pixbuf_new_from_file_at_size(const char *filename,int width,int height)
{
  GdkPixbuf *pixbuf;
  GError *error = (GError *)((void *)0);
  pixbuf = gdk_pixbuf_new_from_file_at_size(filename,width,height,&error);
  if (!(pixbuf != 0) || (error != 0)) {
    purple_debug_warning("gtkutils","gdk_pixbuf_new_from_file_at_size() returned %s for file %s: %s\n",((pixbuf != 0)?"something" : "nothing"),filename,((error != 0)?(error -> message) : "(no error message)"));
    if (error != 0) 
      g_error_free(error);
    if (pixbuf != 0) 
      g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
    return 0;
  }
  return pixbuf;
}

GdkPixbuf *pidgin_pixbuf_new_from_file_at_scale(const char *filename,int width,int height,gboolean preserve_aspect_ratio)
{
  GdkPixbuf *pixbuf;
  GError *error = (GError *)((void *)0);
  pixbuf = gdk_pixbuf_new_from_file_at_scale(filename,width,height,preserve_aspect_ratio,&error);
  if (!(pixbuf != 0) || (error != 0)) {
    purple_debug_warning("gtkutils","gdk_pixbuf_new_from_file_at_scale() returned %s for file %s: %s\n",((pixbuf != 0)?"something" : "nothing"),filename,((error != 0)?(error -> message) : "(no error message)"));
    if (error != 0) 
      g_error_free(error);
    if (pixbuf != 0) 
      g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
    return 0;
  }
  return pixbuf;
}

static void url_copy(GtkWidget *w,gchar *url)
{
  GtkClipboard *clipboard;
  clipboard = gtk_widget_get_clipboard(w,((GdkAtom )((GdkAtom )((gpointer )((gulong )1)))));
  gtk_clipboard_set_text(clipboard,url,(-1));
  clipboard = gtk_widget_get_clipboard(w,((GdkAtom )((GdkAtom )((gpointer )((gulong )69)))));
  gtk_clipboard_set_text(clipboard,url,(-1));
}

static gboolean link_context_menu(GtkIMHtml *imhtml,GtkIMHtmlLink *link,GtkWidget *menu)
{
  GtkWidget *img;
  GtkWidget *item;
  const char *url;
  url = gtk_imhtml_link_get_url(link);
/* Open Link */
  img = gtk_image_new_from_stock("gtk-jump-to",GTK_ICON_SIZE_MENU);
  item = gtk_image_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","_Open Link"))));
  gtk_image_menu_item_set_image(((GtkImageMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_image_menu_item_get_type()))),img);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"activate",((GCallback )gtk_imhtml_link_activate),link,0,G_CONNECT_SWAPPED);
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item);
/* Copy Link Location */
  img = gtk_image_new_from_stock("gtk-copy",GTK_ICON_SIZE_MENU);
  item = gtk_image_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","_Copy Link Location"))));
  gtk_image_menu_item_set_image(((GtkImageMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_image_menu_item_get_type()))),img);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"activate",((GCallback )url_copy),((gpointer )url),0,((GConnectFlags )0));
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item);
  return (!0);
}

static gboolean copy_email_address(GtkIMHtml *imhtml,GtkIMHtmlLink *link,GtkWidget *menu)
{
  GtkWidget *img;
  GtkWidget *item;
  const char *text;
  char *address;
#define MAILTOSIZE  (sizeof("mailto:") - 1)
  text = gtk_imhtml_link_get_url(link);
  do {
    if ((text != 0) && ((strlen(text)) > sizeof(( *((char (*)[8UL])"mailto:"))) - 1)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"text && strlen(text) > MAILTOSIZE");
      return 0;
    };
  }while (0);
  address = (((char *)text) + (sizeof(( *((char (*)[8UL])"mailto:"))) - 1));
/* Copy Email Address */
  img = gtk_image_new_from_stock("gtk-copy",GTK_ICON_SIZE_MENU);
  item = gtk_image_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","_Copy Email Address"))));
  gtk_image_menu_item_set_image(((GtkImageMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_image_menu_item_get_type()))),img);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"activate",((GCallback )url_copy),address,0,((GConnectFlags )0));
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item);
  return (!0);
}
/**
 * @param filename The path to a file. Specifically this is the link target
 *        from a link in an IM window with the leading "file://" removed.
 */

static void open_file(GtkIMHtml *imhtml,const char *filename)
{
/* Copied from gtkft.c:open_button_cb */
#ifdef _WIN32
/* If using Win32... */
/* Escape URI by replacing double-quote with 2 double-quotes. */
/* TODO: Better to use SHOpenFolderAndSelectItems()? */
#else
  char *command = (char *)((void *)0);
  char *tmp = (char *)((void *)0);
  GError *error = (GError *)((void *)0);
  if (purple_running_gnome() != 0) {
    char *escaped = g_shell_quote(filename);
    command = g_strdup_printf("gnome-open %s",escaped);
    g_free(escaped);
  }
  else if (purple_running_kde() != 0) {
    char *escaped = g_shell_quote(filename);
    if (purple_str_has_suffix(filename,".desktop") != 0) 
      command = g_strdup_printf("kfmclient openURL %s \'text/plain\'",escaped);
    else 
      command = g_strdup_printf("kfmclient openURL %s",escaped);
    g_free(escaped);
  }
  else {
    purple_notify_uri(0,filename);
    return ;
  }
  if (purple_program_is_valid(command) != 0) {
    gint exit_status;
    if (!(g_spawn_command_line_sync(command,0,0,&exit_status,&error) != 0)) {
      tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Error launching %s: %s"))),filename,(error -> message));
      purple_notify_message(imhtml,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Unable to open file."))),tmp,0,0);
      g_free(tmp);
      g_error_free(error);
    }
    if (exit_status != 0) {
      char *primary = g_strdup_printf(((const char *)(dgettext("pidgin","Error running %s"))),command);
      char *secondary = g_strdup_printf(((const char *)(dgettext("pidgin","Process returned error code %d"))),exit_status);
      purple_notify_message(imhtml,PURPLE_NOTIFY_MSG_ERROR,0,primary,secondary,0,0);
      g_free(tmp);
    }
  }
#endif
}
#define FILELINKSIZE  (sizeof("file://") - 1)

static gboolean file_clicked_cb(GtkIMHtml *imhtml,GtkIMHtmlLink *link)
{
/* Strip "file://" from the URI. */
  const char *filename = (gtk_imhtml_link_get_url(link) + (sizeof(( *((char (*)[8UL])"file://"))) - 1));
  open_file(imhtml,filename);
  return (!0);
}

static gboolean open_containing_cb(GtkIMHtml *imhtml,const char *url)
{
  char *dir = g_path_get_dirname((url + (sizeof(( *((char (*)[8UL])"file://"))) - 1)));
  open_file(imhtml,dir);
  g_free(dir);
  return (!0);
}

static gboolean file_context_menu(GtkIMHtml *imhtml,GtkIMHtmlLink *link,GtkWidget *menu)
{
  GtkWidget *img;
  GtkWidget *item;
  const char *url;
  url = gtk_imhtml_link_get_url(link);
/* Open File */
  img = gtk_image_new_from_stock("gtk-jump-to",GTK_ICON_SIZE_MENU);
  item = gtk_image_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","_Open File"))));
  gtk_image_menu_item_set_image(((GtkImageMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_image_menu_item_get_type()))),img);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"activate",((GCallback )gtk_imhtml_link_activate),link,0,G_CONNECT_SWAPPED);
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item);
/* Open Containing Directory */
  img = gtk_image_new_from_stock("gtk-directory",GTK_ICON_SIZE_MENU);
  item = gtk_image_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","Open _Containing Directory"))));
  gtk_image_menu_item_set_image(((GtkImageMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_image_menu_item_get_type()))),img);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"activate",((GCallback )open_containing_cb),((gpointer )url),0,((GConnectFlags )0));
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item);
  return (!0);
}
#define AUDIOLINKSIZE  (sizeof("audio://") - 1)

static gboolean audio_clicked_cb(GtkIMHtml *imhtml,GtkIMHtmlLink *link)
{
  const char *uri;
  PidginConversation *conv = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"gtkconv"));
/* no playback in debug window */
  if (!(conv != 0)) 
    return (!0);
  uri = (gtk_imhtml_link_get_url(link) + (sizeof(( *((char (*)[9UL])"audio://"))) - 1));
  purple_sound_play_file(uri,0);
  return (!0);
}

static void savefile_write_cb(gpointer user_data,char *file)
{
  char *temp_file = user_data;
  gchar *contents;
  gsize length;
  GError *error = (GError *)((void *)0);
  if (!(g_file_get_contents(temp_file,&contents,&length,&error) != 0)) {
    purple_debug_error("gtkutils","Unable to read contents of %s: %s\n",temp_file,(error -> message));
    g_error_free(error);
    return ;
  }
  if (!(purple_util_write_data_to_file_absolute(file,contents,length) != 0)) {
    purple_debug_error("gtkutils","Unable to write contents to %s\n",file);
  }
}

static gboolean save_file_cb(GtkWidget *item,const char *url)
{
  PidginConversation *conv = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"gtkconv"));
  if (!(conv != 0)) 
    return (!0);
  purple_request_file((conv -> active_conv),((const char *)(dgettext("pidgin","Save File"))),0,(!0),((GCallback )savefile_write_cb),0,( *(conv -> active_conv)).account,0,(conv -> active_conv),((void *)url));
  return (!0);
}

static gboolean audio_context_menu(GtkIMHtml *imhtml,GtkIMHtmlLink *link,GtkWidget *menu)
{
  GtkWidget *img;
  GtkWidget *item;
  const char *url;
  PidginConversation *conv = (g_object_get_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)imhtml),((GType )(20 << 2))))),"gtkconv"));
/* No menu in debug window */
  if (!(conv != 0)) 
    return (!0);
  url = gtk_imhtml_link_get_url(link);
/* Play Sound */
  img = gtk_image_new_from_stock("gtk-media-play",GTK_ICON_SIZE_MENU);
  item = gtk_image_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","_Play Sound"))));
  gtk_image_menu_item_set_image(((GtkImageMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_image_menu_item_get_type()))),img);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"activate",((GCallback )gtk_imhtml_link_activate),link,0,G_CONNECT_SWAPPED);
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item);
/* Save File */
  img = gtk_image_new_from_stock("gtk-save",GTK_ICON_SIZE_MENU);
  item = gtk_image_menu_item_new_with_mnemonic(((const char *)(dgettext("pidgin","_Save File"))));
  gtk_image_menu_item_set_image(((GtkImageMenuItem *)(g_type_check_instance_cast(((GTypeInstance *)item),gtk_image_menu_item_get_type()))),img);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"activate",((GCallback )save_file_cb),((gpointer )(url + (sizeof(( *((char (*)[9UL])"audio://"))) - 1))),0,((GConnectFlags )0));
  g_object_set_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)item),((GType )(20 << 2))))),"gtkconv",conv);
  gtk_menu_shell_append(((GtkMenuShell *)(g_type_check_instance_cast(((GTypeInstance *)menu),gtk_menu_shell_get_type()))),item);
  return (!0);
}
/* XXX: The following two functions are for demonstration purposes only! */

static gboolean open_dialog(GtkIMHtml *imhtml,GtkIMHtmlLink *link)
{
  const char *url;
  const char *str;
  url = gtk_imhtml_link_get_url(link);
  if (!(url != 0) || ((strlen(url)) < sizeof(( *((char (*)[8UL])"open://"))))) 
    return 0;
  str = ((url + sizeof(( *((char (*)[8UL])"open://")))) - 1);
  if (strcmp(str,"accounts") == 0) 
    pidgin_accounts_window_show();
  else if (strcmp(str,"prefs") == 0) 
    pidgin_prefs_show();
  else 
    return 0;
  return (!0);
}

static gboolean dummy(GtkIMHtml *imhtml,GtkIMHtmlLink *link,GtkWidget *menu)
{
  return (!0);
}

static gboolean register_gnome_url_handlers()
{
  char *tmp;
  char *err;
  char *c;
  char *start;
  tmp = g_find_program_in_path("gconftool-2");
  if (tmp == ((char *)((void *)0))) 
    return 0;
  g_free(tmp);
  tmp = ((char *)((void *)0));
  if (!(g_spawn_command_line_sync("gconftool-2 --all-dirs /desktop/gnome/url-handlers",&tmp,&err,0,0) != 0)) {
    g_free(tmp);
    g_free(err);
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","gtkutils.c",3525,((const char *)__func__));
      return 0;
    }while (0);
  }
  g_free(err);
  err = ((char *)((void *)0));
  for (c = (start = tmp); ( *c) != 0; c++) {
/* Skip leading spaces. */
    if ((c == start) && (( *c) == 32)) 
      start = (c + 1);
    else if (( *c) == 10) {
       *c = 0;
      if (g_str_has_prefix(start,"/desktop/gnome/url-handlers/") != 0) {
        char *cmd;
        char *tmp2 = (char *)((void *)0);
        char *protocol;
/* If there is an enabled boolean, honor it. */
        cmd = g_strdup_printf("gconftool-2 -g %s/enabled",start);
        if (g_spawn_command_line_sync(cmd,&tmp2,&err,0,0) != 0) {
          g_free(err);
          err = ((char *)((void *)0));
          if (!(strcmp(tmp2,"false\n") != 0)) {
            g_free(tmp2);
            g_free(cmd);
            start = (c + 1);
            continue; 
          }
        }
        g_free(cmd);
        g_free(tmp2);
        start += sizeof(( *((char (*)[29UL])"/desktop/gnome/url-handlers/"))) - 1;
        protocol = g_strdup_printf("%s:",start);
        registered_url_handlers = g_slist_prepend(registered_url_handlers,protocol);
        gtk_imhtml_class_register_protocol(protocol,url_clicked_cb,link_context_menu);
      }
      start = (c + 1);
    }
  }
  g_free(tmp);
  return registered_url_handlers != ((GSList *)((void *)0));
}
#ifdef _WIN32
/* We still pass everything to the "http" "open" handler for security reasons */
#endif

GtkWidget *pidgin_make_scrollable(GtkWidget *child,GtkPolicyType hscrollbar_policy,GtkPolicyType vscrollbar_policy,GtkShadowType shadow_type,int width,int height)
{
  GtkWidget *sw = gtk_scrolled_window_new(0,0);
  if (sw != 0) {
    gtk_widget_show(sw);
    gtk_scrolled_window_set_policy(((GtkScrolledWindow *)(g_type_check_instance_cast(((GTypeInstance *)sw),gtk_scrolled_window_get_type()))),hscrollbar_policy,vscrollbar_policy);
    gtk_scrolled_window_set_shadow_type(((GtkScrolledWindow *)(g_type_check_instance_cast(((GTypeInstance *)sw),gtk_scrolled_window_get_type()))),shadow_type);
    if ((width != -1) || (height != -1)) 
      gtk_widget_set_size_request(sw,width,height);
    if (child != 0) {
      if (( *((GtkWidgetClass *)( *((GTypeInstance *)child)).g_class)).set_scroll_adjustments_signal != 0U) 
        gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)sw),gtk_container_get_type()))),child);
      else 
        gtk_scrolled_window_add_with_viewport(((GtkScrolledWindow *)(g_type_check_instance_cast(((GTypeInstance *)sw),gtk_scrolled_window_get_type()))),child);
    }
    return sw;
  }
  return child;
}

void pidgin_utils_init()
{
  gtk_imhtml_class_register_protocol("http://",url_clicked_cb,link_context_menu);
  gtk_imhtml_class_register_protocol("https://",url_clicked_cb,link_context_menu);
  gtk_imhtml_class_register_protocol("ftp://",url_clicked_cb,link_context_menu);
  gtk_imhtml_class_register_protocol("gopher://",url_clicked_cb,link_context_menu);
  gtk_imhtml_class_register_protocol("mailto:",url_clicked_cb,copy_email_address);
  gtk_imhtml_class_register_protocol("file://",file_clicked_cb,file_context_menu);
  gtk_imhtml_class_register_protocol("audio://",audio_clicked_cb,audio_context_menu);
/* Example custom URL handler. */
  gtk_imhtml_class_register_protocol("open://",open_dialog,dummy);
/* If we're under GNOME, try registering the system URL handlers. */
  if (purple_running_gnome() != 0) 
    register_gnome_url_handlers();
/* Used to make small buttons */
  gtk_rc_parse_string("style \"pidgin-small-close-button\"\n{\nGtkWidget::focus-padding = 0\nGtkWidget::focus-line-width = 0\nxthickness = 0\nythickness = 0\nGtkContainer::border-width = 0\nGtkButton::inner-border = {0, 0, 0, 0}\nGtkButton::default-border = {0, 0, 0, 0}\n}\nwidget \"*.pidgin-small-close-button\" style \"pidgin-small-close-button\"");
#ifdef _WIN32
#endif
}

void pidgin_utils_uninit()
{
  gtk_imhtml_class_register_protocol("open://",0,0);
/* If we have GNOME handlers registered, unregister them. */
  if (registered_url_handlers != 0) {
    GSList *l;
    for (l = registered_url_handlers; l != 0; l = (l -> next)) {
      gtk_imhtml_class_register_protocol(((char *)(l -> data)),0,0);
      g_free((l -> data));
    }
    g_slist_free(registered_url_handlers);
    registered_url_handlers = ((GSList *)((void *)0));
    return ;
  }
  gtk_imhtml_class_register_protocol("audio://",0,0);
  gtk_imhtml_class_register_protocol("file://",0,0);
  gtk_imhtml_class_register_protocol("http://",0,0);
  gtk_imhtml_class_register_protocol("https://",0,0);
  gtk_imhtml_class_register_protocol("ftp://",0,0);
  gtk_imhtml_class_register_protocol("mailto:",0,0);
  gtk_imhtml_class_register_protocol("gopher://",0,0);
}
