/*
 * @file gtkdnd-hints.c GTK+ Drag-and-Drop arrow hints
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or(at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301, USA.
 */
#include "gtkdnd-hints.h"
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#ifdef _WIN32
#include "win32dep.h"
#endif
typedef struct __unnamed_class___F0_L37_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__GtkWidget_GtkWidget__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__widget__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__filename__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_ginti__typedef_declaration_variable_name_unknown_scope_and_name__scope__ox__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_ginti__typedef_declaration_variable_name_unknown_scope_and_name__scope__oy {
GtkWidget *widget;
gchar *filename;
gint ox;
gint oy;}HintWindowInfo;
/**
 * Info about each hint widget. See DndHintWindowId enum.
 */
static HintWindowInfo hint_windows[] = {{((GtkWidget *)((void *)0)), ("arrow-up.xpm"), ((-13 / 2)), (0)}, {((GtkWidget *)((void *)0)), ("arrow-down.xpm"), ((-13 / 2)), ((-16))}, {((GtkWidget *)((void *)0)), ("arrow-left.xpm"), (0), ((-13 / 2))}, {((GtkWidget *)((void *)0)), ("arrow-right.xpm"), ((-16)), ((-13 / 2))}, {((GtkWidget *)((void *)0)), ((gchar *)((void *)0)), (0), (0)}};

static GtkWidget *dnd_hints_init_window(const gchar *fname)
{
  GdkPixbuf *pixbuf;
  GdkPixmap *pixmap;
  GdkBitmap *bitmap;
  GtkWidget *pix;
  GtkWidget *win;
  pixbuf = gdk_pixbuf_new_from_file(fname,0);
  do {
    if (pixbuf != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pixbuf");
      return 0;
    };
  }while (0);
  gdk_pixbuf_render_pixmap_and_mask(pixbuf,&pixmap,&bitmap,128);
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixbuf),((GType )(20 << 2))))));
  gtk_widget_push_colormap(gdk_rgb_get_colormap());
  win = gtk_window_new(GTK_WINDOW_POPUP);
  pix = gtk_image_new_from_pixmap(pixmap,bitmap);
  gtk_container_add(((GtkContainer *)(g_type_check_instance_cast(((GTypeInstance *)win),gtk_container_get_type()))),pix);
  gtk_widget_shape_combine_mask(win,bitmap,0,0);
  gtk_widget_pop_colormap();
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)pixmap),((GType )(20 << 2))))));
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)bitmap),((GType )(20 << 2))))));
  gtk_widget_show_all(pix);
  return win;
}

static void get_widget_coords(GtkWidget *w,gint *x1,gint *y1,gint *x2,gint *y2)
{
  gint ox;
  gint oy;
  gint width;
  gint height;
  if (((w -> parent) != 0) && (( *(w -> parent)).window == (w -> window))) {
    get_widget_coords((w -> parent),&ox,&oy,0,0);
    height = w -> allocation.height;
    width = w -> allocation.width;
  }
  else {
    gdk_window_get_origin((w -> window),&ox,&oy);
    gdk_drawable_get_size((w -> window),&width,&height);
  }
  if (x1 != 0) 
     *x1 = ox;
  if (y1 != 0) 
     *y1 = oy;
  if (x2 != 0) 
     *x2 = (ox + width);
  if (y2 != 0) 
     *y2 = (oy + height);
}

static void dnd_hints_init()
{
  static gboolean done = 0;
  gint i;
  if (done != 0) 
    return ;
  done = (!0);
  for (i = 0; hint_windows[i].filename != ((gchar *)((void *)0)); i++) {
    gchar *fname;
    fname = g_build_filename("/usr/local/share","pixmaps","pidgin",hint_windows[i].filename,((void *)((void *)0)));
    hint_windows[i].widget = dnd_hints_init_window(fname);
    g_free(fname);
  }
}

void dnd_hints_hide_all()
{
  gint i;
  for (i = 0; hint_windows[i].filename != ((gchar *)((void *)0)); i++) 
    dnd_hints_hide(i);
}

void dnd_hints_hide(DndHintWindowId i)
{
  GtkWidget *w = hint_windows[i].widget;
  if ((w != 0) && ((({
    GTypeInstance *__inst = (GTypeInstance *)w;
    GType __t = gtk_widget_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) 
    gtk_widget_hide(w);
}

void dnd_hints_show(DndHintWindowId id,gint x,gint y)
{
  GtkWidget *w;
  dnd_hints_init();
  w = hint_windows[id].widget;
  if ((w != 0) && ((({
    GTypeInstance *__inst = (GTypeInstance *)w;
    GType __t = gtk_widget_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) 
{
    gtk_window_move(((GtkWindow *)(g_type_check_instance_cast(((GTypeInstance *)w),gtk_window_get_type()))),(hint_windows[id].ox + x),(hint_windows[id].oy + y));
    gtk_widget_show(w);
  }
}

void dnd_hints_show_relative(DndHintWindowId id,GtkWidget *widget,DndHintPosition horiz,DndHintPosition vert)
{
  gint x1;
  gint x2;
  gint y1;
  gint y2;
  gint x = 0;
  gint y = 0;
  get_widget_coords(widget,&x1,&y1,&x2,&y2);
  x1 += widget -> allocation.x;
  x2 += widget -> allocation.x;
  y1 += widget -> allocation.y;
  y2 += widget -> allocation.y;
  switch(horiz){
    case HINT_POSITION_RIGHT:
{
      x = x2;
      break; 
    }
    case HINT_POSITION_LEFT:
{
      x = x1;
      break; 
    }
    case HINT_POSITION_CENTER:
{
      x = ((x1 + x2) / 2);
      break; 
    }
    default:
{
/* should not happen */
      g_log(0,G_LOG_LEVEL_WARNING,"Invalid parameter to dnd_hints_show_relative");
      break; 
    }
  }
  switch(vert){
    case HINT_POSITION_TOP:
{
      y = y1;
      break; 
    }
    case HINT_POSITION_BOTTOM:
{
      y = y2;
      break; 
    }
    case HINT_POSITION_CENTER:
{
      y = ((y1 + y2) / 2);
      break; 
    }
    default:
{
/* should not happen */
      g_log(0,G_LOG_LEVEL_WARNING,"Invalid parameter to dnd_hints_show_relative");
      break; 
    }
  }
  dnd_hints_show(id,x,y);
}
