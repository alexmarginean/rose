/**
 * @file pidginstock.c GTK+ Stock resources
 * @ingroup pidgin
 */
/* pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "pidgin.h"
#include "prefs.h"
#include "gtkicon-theme-loader.h"
#include "theme-manager.h"
#include "pidginstock.h"
/**************************************************************************
 * Globals
 **************************************************************************/
static gboolean stock_initted = 0;
static GtkIconSize microscopic;
static GtkIconSize extra_small;
static GtkIconSize small;
static GtkIconSize medium;
static GtkIconSize large;
static GtkIconSize huge;
/**************************************************************************
 * Structures
 **************************************************************************/
static const struct StockIcon {
const char *name;
const char *dir;
const char *filename;}stock_icons[] = {{("pidgin-action"), ((const char *)((void *)0)), ("gtk-execute")}, {("pidgin-alias"), ((const char *)((void *)0)), ("gtk-edit")}, {("pidgin-chat"), ((const char *)((void *)0)), ("gtk-jump-to")}, {("pidgin-clear"), ((const char *)((void *)0)), ("gtk-clear")}, {("pidgin-close-tab"), ((const char *)((void *)0)), ("gtk-close")}, {("pidgin-debug"), ((const char *)((void *)0)), ("gtk-properties")}, {("pidgin-download"), ((const char *)((void *)0)), ("gtk-go-down")}, {("pidgin-disconnect"), ((const char *)((void *)0)), ("gtk-disconnect")}, {("pidgin-fgcolor"), ("buttons"), ("change-fgcolor-small.png")}, {("pidgin-edit"), ((const char *)((void *)0)), ("gtk-edit")}, {("pidgin-file-cancelled"), ((const char *)((void *)0)), ("gtk-cancel")}, {("pidgin-file-done"), ((const char *)((void *)0)), ("gtk-apply")}, {("pidgin-ignore"), ((const char *)((void *)0)), ("gtk-dialog-error")}, {("pidgin-invite"), ((const char *)((void *)0)), ("gtk-jump-to")}, {("pidgin-modify"), ((const char *)((void *)0)), ("gtk-preferences")}, {("pidgin-add"), ((const char *)((void *)0)), ("gtk-add")}, {("pidgin-pause"), ((const char *)((void *)0)), ("gtk-media-pause")}, {("pidgin-pounce"), ((const char *)((void *)0)), ("gtk-redo")}, {("pidgin-stock-open-mail"), ((const char *)((void *)0)), ("gtk-jump-to")}, {("pidgin-sign-on"), ((const char *)((void *)0)), ("gtk-execute")}, {("pidgin-sign-off"), ((const char *)((void *)0)), ("gtk-close")}, {("pidgin-typed"), ("pidgin"), ("typed.png")}, {("pidgin-upload"), ((const char *)((void *)0)), ("gtk-go-up")}, {("pidgin-info"), ((const char *)((void *)0)), ("gtk-info")}};
static const GtkStockItem stock_items[] = {{("pidgin-alias"), ("_Alias"), (0), (0), ("pidgin")}, {("pidgin-chat"), ("_Join"), (0), (0), ("pidgin")}, {("pidgin-close-tab"), ("Close _tabs"), (0), (0), ("pidgin")}, {("pidgin-message-new"), ("I_M"), (0), (0), ("pidgin")}, {("pidgin-info"), ("_Get Info"), (0), (0), ("pidgin")}, {("pidgin-invite"), ("_Invite"), (0), (0), ("pidgin")}, {("pidgin-modify"), ("_Modify..."), (0), (0), ("pidgin")}, {("pidgin-add"), ("_Add..."), (0), (0), ("pidgin")}, {("pidgin-stock-open-mail"), ("_Open Mail"), (0), (0), ("pidgin")}, {("pidgin-pause"), ("_Pause"), (0), (0), ("pidgin")}, {("pidgin-edit"), ("_Edit"), (0), (0), ("pidgin")}};
typedef struct __unnamed_class___F0_L96_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__name__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__dir__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__filename__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__microscopic__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__extra_small__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__small__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__medium__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__large__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__huge__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__rtl__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__translucent_name {
const char *name;
const char *dir;
const char *filename;
gboolean microscopic;
gboolean extra_small;
gboolean small;
gboolean medium;
gboolean large;
gboolean huge;
gboolean rtl;
const char *translucent_name;}SizedStockIcon;
const SizedStockIcon sized_stock_icons[] = {{("pidgin-status-ignored"), ("emblems"), ("blocked.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-status-founder"), ("emblems"), ("founder.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-status-operator"), ("emblems"), ("operator.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-status-halfop"), ("emblems"), ("half-operator.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-status-voice"), ("emblems"), ("voice.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-dialog-auth"), ("dialogs"), ("auth.png"), (0), ((!0)), (0), (0), (0), ((!0)), (0), ((const char *)((void *)0))}, {("pidgin-dialog-cool"), ("dialogs"), ("cool.png"), (0), (0), (0), (0), (0), ((!0)), (0), ((const char *)((void *)0))}, {("pidgin-dialog-error"), ("dialogs"), ("error.png"), (0), ((!0)), (0), (0), (0), ((!0)), (0), ((const char *)((void *)0))}, {("pidgin-dialog-info"), ("dialogs"), ("info.png"), (0), ((!0)), (0), (0), (0), ((!0)), (0), ((const char *)((void *)0))}, {("pidgin-dialog-mail"), ("dialogs"), ("mail.png"), (0), ((!0)), (0), (0), (0), ((!0)), (0), ((const char *)((void *)0))}, {("pidgin-dialog-question"), ("dialogs"), ("question.png"), (0), ((!0)), (0), (0), (0), ((!0)), (0), ((const char *)((void *)0))}, {("pidgin-dialog-warning"), ("dialogs"), ("warning.png"), (0), (0), (0), (0), (0), ((!0)), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect0"), ("animations"), ("process-working0.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect1"), ("animations"), ("process-working1.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect2"), ("animations"), ("process-working2.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect3"), ("animations"), ("process-working3.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect4"), ("animations"), ("process-working4.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect5"), ("animations"), ("process-working5.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect6"), ("animations"), ("process-working6.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect7"), ("animations"), ("process-working7.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect8"), ("animations"), ("process-working8.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect9"), ("animations"), ("process-working9.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect10"), ("animations"), ("process-working10.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect11"), ("animations"), ("process-working11.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect12"), ("animations"), ("process-working12.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect13"), ("animations"), ("process-working13.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect14"), ("animations"), ("process-working14.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect15"), ("animations"), ("process-working15.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect16"), ("animations"), ("process-working16.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect17"), ("animations"), ("process-working17.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect18"), ("animations"), ("process-working18.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect19"), ("animations"), ("process-working19.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect20"), ("animations"), ("process-working20.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect21"), ("animations"), ("process-working21.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect22"), ("animations"), ("process-working22.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect23"), ("animations"), ("process-working23.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect24"), ("animations"), ("process-working24.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect25"), ("animations"), ("process-working25.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect26"), ("animations"), ("process-working26.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect27"), ("animations"), ("process-working27.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect28"), ("animations"), ("process-working28.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect29"), ("animations"), ("process-working29.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-connect30"), ("animations"), ("process-working30.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-typing0"), ("animations"), ("typing0.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-typing1"), ("animations"), ("typing1.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-typing2"), ("animations"), ("typing2.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-typing3"), ("animations"), ("typing3.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-typing4"), ("animations"), ("typing4.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-anim-typing5"), ("animations"), ("typing5.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-bgcolor"), ("toolbar"), ("change-bgcolor.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-block"), ("emblems"), ("blocked.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-fgcolor"), ("toolbar"), ("change-fgcolor.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-smiley"), ("toolbar"), ("emote-select.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-font-face"), ("toolbar"), ("font-face.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-text-smaller"), ("toolbar"), ("font-size-down.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-text-larger"), ("toolbar"), ("font-size-up.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-insert"), ("toolbar"), ("insert.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-insert-image"), ("toolbar"), ("insert-image.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-insert-link"), ("toolbar"), ("insert-link.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-message-new"), ("toolbar"), ("message-new.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-pending"), ("toolbar"), ("message-new.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-plugins"), ("toolbar"), ("plugins.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-unblock"), ("toolbar"), ("unblock.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-select-avatar"), ("toolbar"), ("select-avatar.png"), (0), (0), ((!0)), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-send-file"), ("toolbar"), ("send-file.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, {("pidgin-transfer"), ("toolbar"), ("transfer.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, 
#ifdef USE_VV
{("pidgin-audio-call"), ("toolbar"), ("audio-call.png"), (0), ((!0)), ((!0)), ((!0)), ((!0)), (0), (0), ((const char *)((void *)0))}, {("pidgin-video-call"), ("toolbar"), ("video-call.png"), (0), ((!0)), ((!0)), ((!0)), ((!0)), (0), (0), ((const char *)((void *)0))}, {("pidgin-audio-video-call"), ("toolbar"), ("audio-video-call.png"), (0), (0), (0), (0), (0), (0), (0), ((const char *)((void *)0))}, 
#endif
{("pidgin-send-attention"), ("toolbar"), ("get-attention.png"), (0), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}};
const SizedStockIcon sized_status_icons[] = {{("pidgin-status-available"), ("status"), ("available.png"), ((!0)), ((!0)), ((!0)), ((!0)), ((!0)), (0), (0), ("pidgin-status-available-i")}, {("pidgin-status-away"), ("status"), ("away.png"), ((!0)), ((!0)), ((!0)), ((!0)), ((!0)), (0), (0), ("pidgin-status-away-i")}, {("pidgin-status-busy"), ("status"), ("busy.png"), ((!0)), ((!0)), ((!0)), ((!0)), ((!0)), (0), (0), ("pidgin-status-busy-i")}, {("pidgin-status-chat"), ("status"), ("chat.png"), ((!0)), ((!0)), ((!0)), ((!0)), ((!0)), (0), (0), ((const char *)((void *)0))}, {("pidgin-status-invisible"), ("status"), ("invisible.png"), ((!0)), ((!0)), ((!0)), ((!0)), ((!0)), (0), (0), ((const char *)((void *)0))}, {("pidgin-status-xa"), ("status"), ("extended-away.png"), ((!0)), ((!0)), ((!0)), ((!0)), ((!0)), (0), ((!0)), ("pidgin-status-xa-i")}, {("pidgin-status-login"), ("status"), ("log-in.png"), ((!0)), ((!0)), ((!0)), ((!0)), ((!0)), (0), ((!0)), ((const char *)((void *)0))}, {("pidgin-status-logout"), ("status"), ("log-out.png"), ((!0)), ((!0)), ((!0)), ((!0)), ((!0)), (0), ((!0)), ((const char *)((void *)0))}, {("pidgin-status-offline"), ("status"), ("offline.png"), ((!0)), ((!0)), ((!0)), ((!0)), ((!0)), (0), (0), ("pidgin-status-offline")}, {("pidgin-status-person"), ("status"), ("person.png"), ((!0)), ((!0)), ((!0)), ((!0)), ((!0)), (0), (0), ((const char *)((void *)0))}, {("pidgin-status-message"), ("toolbar"), ("message-new.png"), ((!0)), ((!0)), (0), (0), (0), (0), (0), ((const char *)((void *)0))}};
const SizedStockIcon sized_tray_icons[] = {
#define SIZED_TRAY_ICON(name) \
	{ name, "tray/hicolor", "status/" name ".png", FALSE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, NULL }
{("pidgin-tray-available"), ("tray/hicolor"), ("status/pidgin-tray-available.png"), (0), ((!0)), ((!0)), ((!0)), ((!0)), (0), (0), ((const char *)((void *)0))}, {("pidgin-tray-invisible"), ("tray/hicolor"), ("status/pidgin-tray-invisible.png"), (0), ((!0)), ((!0)), ((!0)), ((!0)), (0), (0), ((const char *)((void *)0))}, {("pidgin-tray-away"), ("tray/hicolor"), ("status/pidgin-tray-away.png"), (0), ((!0)), ((!0)), ((!0)), ((!0)), (0), (0), ((const char *)((void *)0))}, {("pidgin-tray-busy"), ("tray/hicolor"), ("status/pidgin-tray-busy.png"), (0), ((!0)), ((!0)), ((!0)), ((!0)), (0), (0), ((const char *)((void *)0))}, {("pidgin-tray-xa"), ("tray/hicolor"), ("status/pidgin-tray-xa.png"), (0), ((!0)), ((!0)), ((!0)), ((!0)), (0), (0), ((const char *)((void *)0))}, {("pidgin-tray-offline"), ("tray/hicolor"), ("status/pidgin-tray-offline.png"), (0), ((!0)), ((!0)), ((!0)), ((!0)), (0), (0), ((const char *)((void *)0))}, {("pidgin-tray-connect"), ("tray/hicolor"), ("status/pidgin-tray-connect.png"), (0), ((!0)), ((!0)), ((!0)), ((!0)), (0), (0), ((const char *)((void *)0))}, {("pidgin-tray-pending"), ("tray/hicolor"), ("status/pidgin-tray-pending.png"), (0), ((!0)), ((!0)), ((!0)), ((!0)), (0), (0), ((const char *)((void *)0))}, 
#undef SIZED_TRAY_ICON
{("pidgin-tray-email"), ("tray/hicolor"), ("status/pidgin-tray-email.png"), (0), ((!0)), ((!0)), ((!0)), ((!0)), (0), (0), ((const char *)((void *)0))}};
/*****************************************************************************
 * Private functions
 *****************************************************************************/

static gchar *find_file_common(const char *name)
{
  gchar *filename;
  const gchar *userdir;
  const gchar *const *sysdirs;
  userdir = g_get_user_data_dir();
  filename = g_build_filename(userdir,name,((void *)((void *)0)));
  if (g_file_test(filename,G_FILE_TEST_EXISTS) != 0) 
    return filename;
  g_free(filename);
  sysdirs = g_get_system_data_dirs();
  for (;  *sysdirs != 0; sysdirs++) {
    filename = g_build_filename( *sysdirs,name,((void *)((void *)0)));
    if (g_file_test(filename,G_FILE_TEST_EXISTS) != 0) 
      return filename;
    g_free(filename);
  }
  filename = g_build_filename("/usr/local/share",name,((void *)((void *)0)));
  if (g_file_test(filename,G_FILE_TEST_EXISTS) != 0) 
    return filename;
  g_free(filename);
  return 0;
}

static gchar *find_file(const char *dir,const char *base)
{
  char *filename;
  char *ret;
  if (base == ((const char *)((void *)0))) 
    return 0;
  if (!(strcmp(dir,"pidgin") != 0)) 
    filename = g_build_filename("pixmaps","pidgin",base,((void *)((void *)0)));
  else 
    filename = g_build_filename("pixmaps","pidgin",dir,base,((void *)((void *)0)));
  ret = find_file_common(filename);
  g_free(filename);
  return ret;
}
/* Altered from do_colorshift in gnome-panel */

static void do_alphashift(GdkPixbuf *pixbuf)
{
  gint i;
  gint j;
  gint width;
  gint height;
  gint padding;
  guchar *pixels;
  guchar a;
  if (!(gdk_pixbuf_get_has_alpha(pixbuf) != 0)) 
    return ;
  width = gdk_pixbuf_get_width(pixbuf);
  height = gdk_pixbuf_get_height(pixbuf);
  padding = (gdk_pixbuf_get_rowstride(pixbuf) - (width * 4));
  pixels = gdk_pixbuf_get_pixels(pixbuf);
  for (i = 0; i < height; i++) {
    for (j = 0; j < width; j++) {
      pixels++;
      pixels++;
      pixels++;
      a =  *pixels;
       *(pixels++) = (a / 2);
    }
    pixels += padding;
  }
}

static gchar *find_icon_file(PidginIconTheme *theme,const gchar *size,SizedStockIcon sized_icon,gboolean rtl)
{
  const gchar *file;
  const gchar *dir;
  gchar *file_full = (gchar *)((void *)0);
  gchar *tmp;
  if (theme != ((PidginIconTheme *)((void *)0))) {
    file = pidgin_icon_theme_get_icon(((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),sized_icon.name);
    dir = purple_theme_get_dir(((PurpleTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),purple_theme_get_type()))));
    if (rtl != 0) 
      file_full = g_build_filename(dir,size,"rtl",file,((void *)((void *)0)));
    else 
      file_full = g_build_filename(dir,size,file,((void *)((void *)0)));
    if (g_file_test(file_full,G_FILE_TEST_IS_REGULAR) != 0) 
      return file_full;
    g_free(file_full);
  }
  if (rtl != 0) 
    tmp = g_build_filename("pixmaps","pidgin",sized_icon.dir,size,"rtl",sized_icon.filename,((void *)((void *)0)));
  else 
    tmp = g_build_filename("pixmaps","pidgin",sized_icon.dir,size,sized_icon.filename,((void *)((void *)0)));
  file_full = find_file_common(tmp);
  g_free(tmp);
  return file_full;
}

static void add_sized_icon(GtkIconSet *iconset,GtkIconSize sizeid,PidginIconTheme *theme,const char *size,SizedStockIcon sized_icon,gboolean translucent)
{
  char *filename;
  GtkIconSource *source;
  GdkPixbuf *pixbuf;
  filename = find_icon_file(theme,size,sized_icon,0);
  do {
    if (filename != ((char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"filename != NULL");
      return ;
    };
  }while (0);
  pixbuf = gdk_pixbuf_new_from_file(filename,0);
  if (translucent != 0) 
    do_alphashift(pixbuf);
  source = gtk_icon_source_new();
  gtk_icon_source_set_pixbuf(source,pixbuf);
  gtk_icon_source_set_direction(source,GTK_TEXT_DIR_LTR);
  gtk_icon_source_set_direction_wildcarded(source,!(sized_icon.rtl != 0));
  gtk_icon_source_set_size(source,sizeid);
  gtk_icon_source_set_size_wildcarded(source,0);
  gtk_icon_source_set_state_wildcarded(source,(!0));
  gtk_icon_set_add_source(iconset,source);
  gtk_icon_source_free(source);
  if (sizeid == (gtk_icon_size_from_name("pidgin-icon-size-tango-extra-small"))) {
    source = gtk_icon_source_new();
    gtk_icon_source_set_pixbuf(source,pixbuf);
    gtk_icon_source_set_direction_wildcarded(source,(!0));
    gtk_icon_source_set_size(source,GTK_ICON_SIZE_MENU);
    gtk_icon_source_set_size_wildcarded(source,0);
    gtk_icon_source_set_state_wildcarded(source,(!0));
    gtk_icon_set_add_source(iconset,source);
    gtk_icon_source_free(source);
  }
  g_free(filename);
  g_object_unref(pixbuf);
  if (sized_icon.rtl != 0) {
    filename = find_icon_file(theme,size,sized_icon,(!0));
    do {
      if (filename != ((char *)((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"filename != NULL");
        return ;
      };
    }while (0);
    pixbuf = gdk_pixbuf_new_from_file(filename,0);
    if (translucent != 0) 
      do_alphashift(pixbuf);
    source = gtk_icon_source_new();
    gtk_icon_source_set_pixbuf(source,pixbuf);
    gtk_icon_source_set_filename(source,filename);
    gtk_icon_source_set_direction(source,GTK_TEXT_DIR_RTL);
    gtk_icon_source_set_size(source,sizeid);
    gtk_icon_source_set_size_wildcarded(source,0);
    gtk_icon_source_set_state_wildcarded(source,(!0));
    gtk_icon_set_add_source(iconset,source);
    g_free(filename);
    g_object_unref(pixbuf);
    gtk_icon_source_free(source);
  }
}

static void reload_settings()
{
  GtkSettings *setting = (GtkSettings *)((void *)0);
  setting = gtk_settings_get_default();
  gtk_rc_reset_styles(setting);
}
/*****************************************************************************
 * Public API functions
 *****************************************************************************/

void pidgin_stock_load_status_icon_theme(PidginStatusIconTheme *theme)
{
  GtkIconFactory *icon_factory;
  gint i;
  GtkIconSet *normal;
  GtkIconSet *translucent = (GtkIconSet *)((void *)0);
  GtkWidget *win;
  if (theme != ((PidginStatusIconTheme *)((void *)0))) {
    purple_prefs_set_string("/pidgin/status/icon-theme",purple_theme_get_name(((PurpleTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),purple_theme_get_type())))));
    purple_prefs_set_path("/pidgin/status/icon-theme-dir",purple_theme_get_dir(((PurpleTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),purple_theme_get_type())))));
  }
  else {
    purple_prefs_set_string("/pidgin/status/icon-theme","");
    purple_prefs_set_path("/pidgin/status/icon-theme-dir","");
  }
  icon_factory = gtk_icon_factory_new();
  gtk_icon_factory_add_default(icon_factory);
  win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_widget_realize(win);
  for (i = 0; i < sizeof(sized_status_icons) / sizeof(sized_status_icons[0]); i++) {
    normal = gtk_icon_set_new();
    if (sized_status_icons[i].translucent_name != 0) 
      translucent = gtk_icon_set_new();
#define ADD_SIZED_ICON(name, size) \
		if (sized_status_icons[i].name) { \
			add_sized_icon(normal, name, PIDGIN_ICON_THEME(theme), size, sized_status_icons[i], FALSE); \
			if (sized_status_icons[i].translucent_name) \
				add_sized_icon(translucent, name, PIDGIN_ICON_THEME(theme), size, sized_status_icons[i], TRUE); \
		}
    if (sized_status_icons[i].microscopic != 0) {
      add_sized_icon(normal,microscopic,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"11",sized_status_icons[i],0);
      if (sized_status_icons[i].translucent_name != 0) 
        add_sized_icon(translucent,microscopic,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"11",sized_status_icons[i],(!0));
    };
    if (sized_status_icons[i].extra_small != 0) {
      add_sized_icon(normal,extra_small,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"16",sized_status_icons[i],0);
      if (sized_status_icons[i].translucent_name != 0) 
        add_sized_icon(translucent,extra_small,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"16",sized_status_icons[i],(!0));
    };
    if (sized_status_icons[i].small != 0) {
      add_sized_icon(normal,small,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"22",sized_status_icons[i],0);
      if (sized_status_icons[i].translucent_name != 0) 
        add_sized_icon(translucent,small,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"22",sized_status_icons[i],(!0));
    };
    if (sized_status_icons[i].medium != 0) {
      add_sized_icon(normal,medium,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"32",sized_status_icons[i],0);
      if (sized_status_icons[i].translucent_name != 0) 
        add_sized_icon(translucent,medium,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"32",sized_status_icons[i],(!0));
    };
    if (sized_status_icons[i].large != 0) {
      add_sized_icon(normal,large,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"48",sized_status_icons[i],0);
      if (sized_status_icons[i].translucent_name != 0) 
        add_sized_icon(translucent,large,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"48",sized_status_icons[i],(!0));
    };
    if (sized_status_icons[i].huge != 0) {
      add_sized_icon(normal,huge,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"64",sized_status_icons[i],0);
      if (sized_status_icons[i].translucent_name != 0) 
        add_sized_icon(translucent,huge,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"64",sized_status_icons[i],(!0));
    };
#undef ADD_SIZED_ICON
    gtk_icon_factory_add(icon_factory,sized_status_icons[i].name,normal);
    gtk_icon_set_unref(normal);
    if (sized_status_icons[i].translucent_name != 0) {
      gtk_icon_factory_add(icon_factory,sized_status_icons[i].translucent_name,translucent);
      gtk_icon_set_unref(translucent);
    }
  }
  for (i = 0; i < sizeof(sized_tray_icons) / sizeof(sized_tray_icons[0]); i++) {
    normal = gtk_icon_set_new();
    if (sized_tray_icons[i].translucent_name != 0) 
      translucent = gtk_icon_set_new();
#define ADD_SIZED_ICON(name, size) \
		if (sized_tray_icons[i].name) { \
			add_sized_icon(normal, name, PIDGIN_ICON_THEME(theme), size, sized_tray_icons[i], FALSE); \
			if (sized_tray_icons[i].translucent_name) \
				add_sized_icon(translucent, name, PIDGIN_ICON_THEME(theme), size, sized_tray_icons[i], TRUE); \
		}
    if (sized_tray_icons[i].extra_small != 0) {
      add_sized_icon(normal,extra_small,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"16x16",sized_tray_icons[i],0);
      if (sized_tray_icons[i].translucent_name != 0) 
        add_sized_icon(translucent,extra_small,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"16x16",sized_tray_icons[i],(!0));
    };
    if (sized_tray_icons[i].small != 0) {
      add_sized_icon(normal,small,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"22x22",sized_tray_icons[i],0);
      if (sized_tray_icons[i].translucent_name != 0) 
        add_sized_icon(translucent,small,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"22x22",sized_tray_icons[i],(!0));
    };
    if (sized_tray_icons[i].medium != 0) {
      add_sized_icon(normal,medium,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"32x32",sized_tray_icons[i],0);
      if (sized_tray_icons[i].translucent_name != 0) 
        add_sized_icon(translucent,medium,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"32x32",sized_tray_icons[i],(!0));
    };
    if (sized_tray_icons[i].large != 0) {
      add_sized_icon(normal,large,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"48x48",sized_tray_icons[i],0);
      if (sized_tray_icons[i].translucent_name != 0) 
        add_sized_icon(translucent,large,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"48x48",sized_tray_icons[i],(!0));
    };
#undef ADD_SIZED_ICON
    gtk_icon_factory_add(icon_factory,sized_tray_icons[i].name,normal);
    gtk_icon_set_unref(normal);
    if (sized_tray_icons[i].translucent_name != 0) {
      gtk_icon_factory_add(icon_factory,sized_tray_icons[i].translucent_name,translucent);
      gtk_icon_set_unref(translucent);
    }
  }
  gtk_widget_destroy(win);
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)icon_factory),((GType )(20 << 2))))));
  reload_settings();
}

void pidgin_stock_load_stock_icon_theme(PidginStockIconTheme *theme)
{
  GtkIconFactory *icon_factory;
  gint i;
  GtkWidget *win;
  if (theme != ((PidginStockIconTheme *)((void *)0))) {
    purple_prefs_set_string("/pidgin/stock/icon-theme",purple_theme_get_name(((PurpleTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),purple_theme_get_type())))));
    purple_prefs_set_path("/pidgin/stock/icon-theme-dir",purple_theme_get_dir(((PurpleTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),purple_theme_get_type())))));
  }
  else {
    purple_prefs_set_string("/pidgin/stock/icon-theme","");
    purple_prefs_set_path("/pidgin/stock/icon-theme-dir","");
  }
  icon_factory = gtk_icon_factory_new();
  gtk_icon_factory_add_default(icon_factory);
  win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_widget_realize(win);
/* All non-sized icons */
  for (i = 0; i < sizeof(stock_icons) / sizeof(stock_icons[0]); i++) {{
      GtkIconSource *source;
      GtkIconSet *iconset;
      gchar *filename;
      if (stock_icons[i].dir == ((const char *)((void *)0))) {
/* GTK+ Stock icon */
        iconset = gtk_style_lookup_icon_set(gtk_widget_get_style(win),stock_icons[i].filename);
      }
      else {
        filename = find_file(stock_icons[i].dir,stock_icons[i].filename);
        if (filename == ((gchar *)((void *)0))) 
          continue; 
        source = gtk_icon_source_new();
        gtk_icon_source_set_filename(source,filename);
        gtk_icon_source_set_direction_wildcarded(source,(!0));
        gtk_icon_source_set_size_wildcarded(source,(!0));
        gtk_icon_source_set_state_wildcarded(source,(!0));
        iconset = gtk_icon_set_new();
        gtk_icon_set_add_source(iconset,source);
        gtk_icon_source_free(source);
        g_free(filename);
      }
      gtk_icon_factory_add(icon_factory,stock_icons[i].name,iconset);
      gtk_icon_set_unref(iconset);
    }
  }
/* All non-status sized icons */
  for (i = 0; i < sizeof(sized_stock_icons) / sizeof(sized_stock_icons[0]); i++) {
    GtkIconSet *iconset = gtk_icon_set_new();
#define ADD_SIZED_ICON(name, size) \
		if (sized_stock_icons[i].name) \
			add_sized_icon(iconset, name, PIDGIN_ICON_THEME(theme), size, sized_stock_icons[i], FALSE);
    if (sized_stock_icons[i].microscopic != 0) 
      add_sized_icon(iconset,microscopic,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"11",sized_stock_icons[i],0);;
    if (sized_stock_icons[i].extra_small != 0) 
      add_sized_icon(iconset,extra_small,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"16",sized_stock_icons[i],0);;
    if (sized_stock_icons[i].small != 0) 
      add_sized_icon(iconset,small,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"22",sized_stock_icons[i],0);;
    if (sized_stock_icons[i].medium != 0) 
      add_sized_icon(iconset,medium,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"32",sized_stock_icons[i],0);;
    if (sized_stock_icons[i].large != 0) 
      add_sized_icon(iconset,large,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"48",sized_stock_icons[i],0);;
    if (sized_stock_icons[i].huge != 0) 
      add_sized_icon(iconset,huge,((PidginIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),pidgin_icon_theme_get_type()))),"64",sized_stock_icons[i],0);;
#undef ADD_SIZED_ICON
    gtk_icon_factory_add(icon_factory,sized_stock_icons[i].name,iconset);
    gtk_icon_set_unref(iconset);
  }
  gtk_widget_destroy(win);
  g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)icon_factory),((GType )(20 << 2))))));
  reload_settings();
}

void pidgin_stock_init()
{
  PidginIconThemeLoader *loader;
  PidginIconThemeLoader *stockloader;
  const gchar *path = (const gchar *)((void *)0);
  if (stock_initted != 0) 
    return ;
  stock_initted = (!0);
/* Setup the status icon theme */
  loader = (g_object_new(pidgin_icon_theme_loader_get_type(),"type","status-icon",((void *)((void *)0))));
  purple_theme_manager_register_type(((PurpleThemeLoader *)(g_type_check_instance_cast(((GTypeInstance *)loader),purple_theme_loader_get_type()))));
  purple_prefs_add_none("/pidgin/status");
  purple_prefs_add_string("/pidgin/status/icon-theme","");
  purple_prefs_add_path("/pidgin/status/icon-theme-dir","");
  stockloader = (g_object_new(pidgin_icon_theme_loader_get_type(),"type","stock-icon",((void *)((void *)0))));
  purple_theme_manager_register_type(((PurpleThemeLoader *)(g_type_check_instance_cast(((GTypeInstance *)stockloader),purple_theme_loader_get_type()))));
  purple_prefs_add_none("/pidgin/stock");
  purple_prefs_add_string("/pidgin/stock/icon-theme","");
  purple_prefs_add_path("/pidgin/stock/icon-theme-dir","");
/* register custom icon sizes */
  microscopic = gtk_icon_size_register("pidgin-icon-size-tango-microscopic",11,11);
  extra_small = gtk_icon_size_register("pidgin-icon-size-tango-extra-small",16,16);
  small = gtk_icon_size_register("pidgin-icon-size-tango-small",22,22);
  medium = gtk_icon_size_register("pidgin-icon-size-tango-medium",32,32);
  large = gtk_icon_size_register("pidgin-icon-size-tango-large",48,48);
  huge = gtk_icon_size_register("pidgin-icon-size-tango-huge",64,64);
  pidgin_stock_load_stock_icon_theme(0);
/* Pre-load Status icon theme - this avoids a bug with displaying the correct icon in the tray, theme is destroyed after*/
  if ((purple_prefs_get_string("/pidgin/status/icon-theme") != 0) && ((path = purple_prefs_get_path("/pidgin/status/icon-theme-dir")) != 0)) {
    PidginStatusIconTheme *theme = (PidginStatusIconTheme *)(g_type_check_instance_cast(((GTypeInstance *)(purple_theme_loader_build(((PurpleThemeLoader *)(g_type_check_instance_cast(((GTypeInstance *)loader),purple_theme_loader_get_type()))),path))),pidgin_status_icon_theme_get_type()));
    pidgin_stock_load_status_icon_theme(theme);
    if (theme != 0) 
      g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)theme),((GType )(20 << 2))))));
  }
  else 
    pidgin_stock_load_status_icon_theme(0);
/* Register the stock items. */
  gtk_stock_add_static(stock_items,(sizeof(stock_items) / sizeof(stock_items[0])));
}

static void pidgin_stock_icon_theme_class_init(PidginStockIconThemeClass *klass)
{
}

GType pidgin_stock_icon_theme_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(PidginStockIconThemeClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )pidgin_stock_icon_theme_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(PidginStockIconTheme ))), (0), ((GInstanceInitFunc )((void *)0)), ((const GTypeValueTable *)((void *)0))
/* base_init */
/* base_finalize */
/* class_init */
/* class_finalize */
/* class_data */
/* n_preallocs */
/* value table */
};
    type = g_type_register_static(pidgin_icon_theme_get_type(),"PidginStockIconTheme",&info,0);
  }
  return type;
}
