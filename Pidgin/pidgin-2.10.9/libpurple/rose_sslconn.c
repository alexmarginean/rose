/**
 * @file sslconn.c SSL API
 * @ingroup core
 */
/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#define _PURPLE_SSLCONN_C_
#include "internal.h"
#include "certificate.h"
#include "debug.h"
#include "request.h"
#include "sslconn.h"
static gboolean _ssl_initialized = 0;
static PurpleSslOps *_ssl_ops = (PurpleSslOps *)((void *)0);

static gboolean ssl_init()
{
  PurplePlugin *plugin;
  PurpleSslOps *ops;
  if (_ssl_initialized != 0) 
    return 0;
  plugin = purple_plugins_find_with_id("core-ssl");
  if ((plugin != ((PurplePlugin *)((void *)0))) && !(purple_plugin_is_loaded(plugin) != 0)) 
    purple_plugin_load(plugin);
  ops = purple_ssl_get_ops();
  if (((((((ops == ((PurpleSslOps *)((void *)0))) || ((ops -> init) == ((gboolean (*)())((void *)0)))) || ((ops -> uninit) == ((void (*)())((void *)0)))) || ((ops -> connectfunc) == ((void (*)(PurpleSslConnection *))((void *)0)))) || ((ops -> close) == ((void (*)(PurpleSslConnection *))((void *)0)))) || ((ops -> read) == ((size_t (*)(PurpleSslConnection *, void *, size_t ))((void *)0)))) || ((ops -> write) == ((size_t (*)(PurpleSslConnection *, const void *, size_t ))((void *)0)))) {
    return 0;
  }
  return _ssl_initialized = ( *(ops -> init))();
}

gboolean purple_ssl_is_supported()
{
#ifdef HAVE_SSL
  ssl_init();
  return purple_ssl_get_ops() != ((PurpleSslOps *)((void *)0));
#else
#endif
}

static void purple_ssl_connect_cb(gpointer data,gint source,const gchar *error_message)
{
  PurpleSslConnection *gsc;
  PurpleSslOps *ops;
  gsc = data;
  gsc -> connect_data = ((PurpleProxyConnectData *)((void *)0));
  if (source < 0) {
    if ((gsc -> error_cb) != ((void (*)(PurpleSslConnection *, PurpleSslErrorType , gpointer ))((void *)0))) 
      ( *(gsc -> error_cb))(gsc,PURPLE_SSL_CONNECT_FAILED,(gsc -> connect_cb_data));
    purple_ssl_close(gsc);
    return ;
  }
  gsc -> fd = source;
  ops = purple_ssl_get_ops();
  ( *(ops -> connectfunc))(gsc);
}

PurpleSslConnection *purple_ssl_connect(PurpleAccount *account,const char *host,int port,PurpleSslInputFunction func,PurpleSslErrorFunction error_func,void *data)
{
  return purple_ssl_connect_with_ssl_cn(account,host,port,func,error_func,0,data);
}

PurpleSslConnection *purple_ssl_connect_with_ssl_cn(PurpleAccount *account,const char *host,int port,PurpleSslInputFunction func,PurpleSslErrorFunction error_func,const char *ssl_cn,void *data)
{
  PurpleSslConnection *gsc;
  do {
    if (host != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"host != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((port != 0) && (port != -1)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"port != 0 && port != -1");
      return 0;
    };
  }while (0);
  do {
    if (func != ((void (*)(gpointer , PurpleSslConnection *, PurpleInputCondition ))((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"func != NULL");
      return 0;
    };
  }while (0);
  do {
    if (purple_ssl_is_supported() != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_ssl_is_supported()");
      return 0;
    };
  }while (0);
  if (!(_ssl_initialized != 0)) {
    if (!(ssl_init() != 0)) 
      return 0;
  }
  gsc = ((PurpleSslConnection *)(g_malloc0_n(1,(sizeof(PurpleSslConnection )))));
  gsc -> fd = -1;
  gsc -> host = ((ssl_cn != 0)?g_strdup(ssl_cn) : g_strdup(host));
  gsc -> port = port;
  gsc -> connect_cb_data = data;
  gsc -> connect_cb = func;
  gsc -> error_cb = error_func;
/* TODO: Move this elsewhere */
  gsc -> verifier = purple_certificate_find_verifier("x509","tls_cached");
  gsc -> connect_data = purple_proxy_connect(0,account,host,port,purple_ssl_connect_cb,gsc);
  if ((gsc -> connect_data) == ((PurpleProxyConnectData *)((void *)0))) {
    g_free((gsc -> host));
    g_free(gsc);
    return 0;
  }
  return (PurpleSslConnection *)gsc;
}

static void recv_cb(gpointer data,gint source,PurpleInputCondition cond)
{
  PurpleSslConnection *gsc = data;
  ( *(gsc -> recv_cb))((gsc -> recv_cb_data),gsc,cond);
}

void purple_ssl_input_add(PurpleSslConnection *gsc,PurpleSslInputFunction func,void *data)
{
  do {
    if (func != ((void (*)(gpointer , PurpleSslConnection *, PurpleInputCondition ))((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"func != NULL");
      return ;
    };
  }while (0);
  do {
    if (purple_ssl_is_supported() != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_ssl_is_supported()");
      return ;
    };
  }while (0);
  gsc -> recv_cb_data = data;
  gsc -> recv_cb = func;
  gsc -> inpa = purple_input_add((gsc -> fd),PURPLE_INPUT_READ,recv_cb,gsc);
}

const gchar *purple_ssl_strerror(PurpleSslErrorType error)
{
  switch(error){
    case PURPLE_SSL_CONNECT_FAILED:
{
      return (const char *)(dgettext("pidgin","SSL Connection Failed"));
    }
    case PURPLE_SSL_HANDSHAKE_FAILED:
{
      return (const char *)(dgettext("pidgin","SSL Handshake Failed"));
    }
    case PURPLE_SSL_CERTIFICATE_INVALID:
{
      return (const char *)(dgettext("pidgin","SSL peer presented an invalid certificate"));
    }
    default:
{
      purple_debug_warning("sslconn","Unknown SSL error code %d\n",error);
      return (const char *)(dgettext("pidgin","Unknown SSL error"));
    }
  }
}

PurpleSslConnection *purple_ssl_connect_fd(PurpleAccount *account,int fd,PurpleSslInputFunction func,PurpleSslErrorFunction error_func,void *data)
{
  return purple_ssl_connect_with_host_fd(account,fd,func,error_func,0,data);
}

PurpleSslConnection *purple_ssl_connect_with_host_fd(PurpleAccount *account,int fd,PurpleSslInputFunction func,PurpleSslErrorFunction error_func,const char *host,void *data)
{
  PurpleSslConnection *gsc;
  PurpleSslOps *ops;
  do {
    if (fd != -1) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"fd != -1");
      return 0;
    };
  }while (0);
  do {
    if (func != ((void (*)(gpointer , PurpleSslConnection *, PurpleInputCondition ))((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"func != NULL");
      return 0;
    };
  }while (0);
  do {
    if (purple_ssl_is_supported() != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_ssl_is_supported()");
      return 0;
    };
  }while (0);
  if (!(_ssl_initialized != 0)) {
    if (!(ssl_init() != 0)) 
      return 0;
  }
  gsc = ((PurpleSslConnection *)(g_malloc0_n(1,(sizeof(PurpleSslConnection )))));
  gsc -> connect_cb_data = data;
  gsc -> connect_cb = func;
  gsc -> error_cb = error_func;
  gsc -> fd = fd;
  if (host != 0) 
    gsc -> host = g_strdup(host);
/* TODO: Move this elsewhere */
  gsc -> verifier = purple_certificate_find_verifier("x509","tls_cached");
  ops = purple_ssl_get_ops();
  ( *(ops -> connectfunc))(gsc);
  return (PurpleSslConnection *)gsc;
}

void purple_ssl_close(PurpleSslConnection *gsc)
{
  PurpleSslOps *ops;
  do {
    if (gsc != ((PurpleSslConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gsc != NULL");
      return ;
    };
  }while (0);
  purple_request_close_with_handle(gsc);
  purple_notify_close_with_handle(gsc);
  ops = purple_ssl_get_ops();
  ( *(ops -> close))(gsc);
  if ((gsc -> connect_data) != ((PurpleProxyConnectData *)((void *)0))) 
    purple_proxy_connect_cancel((gsc -> connect_data));
  if ((gsc -> inpa) > 0) 
    purple_input_remove((gsc -> inpa));
  if ((gsc -> fd) >= 0) 
    close((gsc -> fd));
  g_free((gsc -> host));
  g_free(gsc);
}

size_t purple_ssl_read(PurpleSslConnection *gsc,void *data,size_t len)
{
  PurpleSslOps *ops;
  do {
    if (gsc != ((PurpleSslConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gsc != NULL");
      return 0;
    };
  }while (0);
  do {
    if (data != ((void *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"data != NULL");
      return 0;
    };
  }while (0);
  do {
    if (len > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"len > 0");
      return 0;
    };
  }while (0);
  ops = purple_ssl_get_ops();
  return ( *(ops -> read))(gsc,data,len);
}

size_t purple_ssl_write(PurpleSslConnection *gsc,const void *data,size_t len)
{
  PurpleSslOps *ops;
  do {
    if (gsc != ((PurpleSslConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gsc != NULL");
      return 0;
    };
  }while (0);
  do {
    if (data != ((const void *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"data != NULL");
      return 0;
    };
  }while (0);
  do {
    if (len > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"len > 0");
      return 0;
    };
  }while (0);
  ops = purple_ssl_get_ops();
  return ( *(ops -> write))(gsc,data,len);
}

GList *purple_ssl_get_peer_certificates(PurpleSslConnection *gsc)
{
  PurpleSslOps *ops;
  do {
    if (gsc != ((PurpleSslConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gsc != NULL");
      return 0;
    };
  }while (0);
  ops = purple_ssl_get_ops();
  return ( *(ops -> get_peer_certificates))(gsc);
}

void purple_ssl_set_ops(PurpleSslOps *ops)
{
  _ssl_ops = ops;
}

PurpleSslOps *purple_ssl_get_ops()
{
  return _ssl_ops;
}

void purple_ssl_init()
{
/* Although purple_ssl_is_supported will do the initialization on
	   command, SSL plugins tend to register CertificateSchemes as well
	   as providing SSL ops. */
  if (!(ssl_init() != 0)) {
    purple_debug_error("sslconn","Unable to initialize SSL.\n");
  }
}

void purple_ssl_uninit()
{
  PurpleSslOps *ops;
  if (!(_ssl_initialized != 0)) 
    return ;
  ops = purple_ssl_get_ops();
  ( *(ops -> uninit))();
  _ssl_initialized = 0;
}
