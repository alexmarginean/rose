/**
 * @file network.c Network Implementation
 * @ingroup core
 */
/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#ifndef _WIN32
#include <arpa/nameser.h>
#include <resolv.h>
#include <netinet/in.h>
#include <net/if.h>
#include <sys/ioctl.h>
#ifdef HAVE_GETIFADDRS
#include <ifaddrs.h>
#endif
#else
#include <nspapi.h>
#endif
/* Solaris */
#if defined (__SVR4) && defined (__sun)
#include <sys/sockio.h>
#endif
#include "debug.h"
#include "account.h"
#include "nat-pmp.h"
#include "network.h"
#include "prefs.h"
#include "stun.h"
#include "upnp.h"
#include "dnsquery.h"
#ifdef USE_IDN
#include <idna.h>
#endif
/*
 * Calling sizeof(struct ifreq) isn't always correct on
 * Mac OS X (and maybe others).
 */
#ifdef _SIZEOF_ADDR_IFREQ
#  define HX_SIZE_OF_IFREQ(a) _SIZEOF_ADDR_IFREQ(a)
#else
#  define HX_SIZE_OF_IFREQ(a) sizeof(a)
#endif
#ifdef HAVE_NETWORKMANAGER
#include <dbus/dbus-glib.h>
#include <NetworkManager.h>
#if !defined(NM_CHECK_VERSION)
#define NM_CHECK_VERSION(x,y,z) 0
#endif
static DBusGConnection *nm_conn = (DBusGConnection *)((void *)0);
static DBusGProxy *nm_proxy = (DBusGProxy *)((void *)0);
static DBusGProxy *dbus_proxy = (DBusGProxy *)((void *)0);
static NMState nm_state = NM_STATE_UNKNOWN;
static gboolean have_nm_state = 0;
#elif defined _WIN32
/* Mutex for the other global vars */
#endif

struct _PurpleNetworkListenData 
{
  int listenfd;
  int socket_type;
  gboolean retry;
  gboolean adding;
  PurpleNetworkListenCallback cb;
  gpointer cb_data;
  UPnPMappingAddRemove *mapping_data;
  int timer;
}
;
#ifdef HAVE_NETWORKMANAGER
static NMState nm_get_network_state();
#endif
#if defined(HAVE_NETWORKMANAGER) || defined(_WIN32)
static gboolean force_online;
#endif
/* Cached IP addresses for STUN and TURN servers (set globally in prefs) */
static gchar *stun_ip = (gchar *)((void *)0);
static gchar *turn_ip = (gchar *)((void *)0);
/* Keep track of port mappings done with UPnP and NAT-PMP */
static GHashTable *upnp_port_mappings = (GHashTable *)((void *)0);
static GHashTable *nat_pmp_port_mappings = (GHashTable *)((void *)0);

const unsigned char *purple_network_ip_atoi(const char *ip)
{
  static unsigned char ret[4UL];
  gchar *delimiter = ".";
  gchar **split;
  int i;
  do {
    if (ip != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ip != NULL");
      return 0;
    };
  }while (0);
  split = g_strsplit(ip,delimiter,4);
  for (i = 0; split[i] != ((gchar *)((void *)0)); i++) 
    ret[i] = (atoi(split[i]));
  g_strfreev(split);
/* i should always be 4 */
  if (i != 4) 
    return 0;
  return ret;
}

void purple_network_set_public_ip(const char *ip)
{
  do {
    if (ip != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ip != NULL");
      return ;
    };
  }while (0);
/* XXX - Ensure the IP address is valid */
  purple_prefs_set_string("/purple/network/public_ip",ip);
}

const char *purple_network_get_public_ip()
{
  return purple_prefs_get_string("/purple/network/public_ip");
}

const char *purple_network_get_local_system_ip(int fd)
{
  char buffer[1024UL];
  static char ip[16UL];
  char *tmp;
  struct ifconf ifc;
  struct ifreq *ifr;
  struct sockaddr_in *sinptr;
/* 127.0.0.1 */
  guint32 lhost = htonl(((127 << 24) + 1));
  unsigned long add;
  int source = fd;
  if (fd < 0) 
    source = socket(2,SOCK_STREAM,0);
  ifc.ifc_len = (sizeof(buffer));
  ifc.ifc_ifcu.ifcu_req = ((struct ifreq *)buffer);
  ioctl(source,0x8912,&ifc);
  if (fd < 0) 
    close(source);
  tmp = buffer;
  while(tmp < (buffer + ifc.ifc_len)){
    ifr = ((struct ifreq *)tmp);
    tmp += sizeof(( *ifr));
    if (ifr -> ifr_ifru.ifru_addr.sa_family == 2) {
      sinptr = ((struct sockaddr_in *)(&ifr -> ifr_ifru.ifru_addr));
      if (sinptr -> sin_addr.s_addr != lhost) {
        add = (ntohl(sinptr -> sin_addr.s_addr));
        g_snprintf(ip,16,"%lu.%lu.%lu.%lu",((add >> 24) & 255),((add >> 16) & 255),((add >> 8) & 255),(add & 255));
        return ip;
      }
    }
  }
  return "0.0.0.0";
}

GList *purple_network_get_all_local_system_ips()
{
#if defined(HAVE_GETIFADDRS) && defined(HAVE_INET_NTOP)
  GList *result = (GList *)((void *)0);
  struct ifaddrs *start;
  struct ifaddrs *ifa;
  int ret;
  ret = getifaddrs(&start);
  if (ret < 0) {
    purple_debug_warning("network","getifaddrs() failed: %s\n",g_strerror( *__errno_location()));
    return 0;
  }
  for (ifa = start; ifa != 0; ifa = (ifa -> ifa_next)) {{
      int family = ((ifa -> ifa_addr) != 0)?( *(ifa -> ifa_addr)).sa_family : 0;
      char host[46UL];
      const char *tmp = (const char *)((void *)0);
      if (((family != 2) && (family != 10)) || (((ifa -> ifa_flags) & IFF_LOOPBACK) != 0U)) 
        continue; 
      if (family == 2) 
        tmp = inet_ntop(family,(&( *((struct sockaddr_in *)(ifa -> ifa_addr))).sin_addr),host,(sizeof(host)));
      else {
        struct sockaddr_in6 *sockaddr = (struct sockaddr_in6 *)(ifa -> ifa_addr);
/* Peer-peer link-local communication is a big TODO.  I am not sure
			 * how communicating link-local addresses is supposed to work, and
			 * it seems like it would require attempting the cartesian product
			 * of the local and remote interfaces to see if any match (eww).
			 */
        if (!((({
          const struct in6_addr *__a = (const struct in6_addr *)(&sockaddr -> sin6_addr);
          (__a -> __in6_u.__u6_addr32[0] & htonl(0xffc00000)) == htonl(0xfe800000);
        })) != 0)) 
          tmp = inet_ntop(family,(&sockaddr -> sin6_addr),host,(sizeof(host)));
      }
      if (tmp != ((const char *)((void *)0))) 
        result = g_list_prepend(result,(g_strdup(tmp)));
    }
  }
  freeifaddrs(start);
  return g_list_reverse(result);
#else /* HAVE_GETIFADDRS && HAVE_INET_NTOP */
#endif /* HAVE_GETIFADDRS && HAVE_INET_NTOP */
}

const char *purple_network_get_my_ip(int fd)
{
  const char *ip = (const char *)((void *)0);
  PurpleStunNatDiscovery *stun;
/* Check if the user specified an IP manually */
  if (!(purple_prefs_get_bool("/purple/network/auto_ip") != 0)) {
    ip = purple_network_get_public_ip();
/* Make sure the IP address entered by the user is valid */
    if ((ip != ((const char *)((void *)0))) && (purple_network_ip_atoi(ip) != ((const unsigned char *)((void *)0)))) 
      return ip;
  }
  else {
/* Check if STUN discovery was already done */
    stun = purple_stun_discover(0);
    if ((stun != ((PurpleStunNatDiscovery *)((void *)0))) && ((stun -> status) == PURPLE_STUN_STATUS_DISCOVERED)) 
      return (stun -> publicip);
/* Attempt to get the IP from a NAT device using UPnP */
    ip = purple_upnp_get_public_ip();
    if (ip != ((const char *)((void *)0))) 
      return ip;
/* Attempt to get the IP from a NAT device using NAT-PMP */
    ip = (purple_pmp_get_public_ip());
    if (ip != ((const char *)((void *)0))) 
      return ip;
  }
/* Just fetch the IP of the local system */
  return purple_network_get_local_system_ip(fd);
}

static void purple_network_set_upnp_port_mapping_cb(gboolean success,gpointer data)
{
  PurpleNetworkListenData *listen_data;
  listen_data = data;
/* TODO: Once we're keeping track of upnp requests... */
/* listen_data->pnp_data = NULL; */
  if (!(success != 0)) {
    purple_debug_warning("network","Couldn\'t create UPnP mapping\n");
    if ((listen_data -> retry) != 0) {
      listen_data -> retry = 0;
      listen_data -> adding = 0;
      listen_data -> mapping_data = purple_upnp_remove_port_mapping(purple_network_get_port_from_fd((listen_data -> listenfd)),((((listen_data -> socket_type) == SOCK_STREAM)?"TCP" : "UDP")),purple_network_set_upnp_port_mapping_cb,listen_data);
      return ;
    }
  }
  else if (!((listen_data -> adding) != 0)) {
/* We've tried successfully to remove the port mapping.
		 * Try to add it again */
    listen_data -> adding = (!0);
    listen_data -> mapping_data = purple_upnp_set_port_mapping(purple_network_get_port_from_fd((listen_data -> listenfd)),((((listen_data -> socket_type) == SOCK_STREAM)?"TCP" : "UDP")),purple_network_set_upnp_port_mapping_cb,listen_data);
    return ;
  }
  if (success != 0) {
/* add port mapping to hash table */
    gint key = (purple_network_get_port_from_fd((listen_data -> listenfd)));
    gint value = (listen_data -> socket_type);
    g_hash_table_insert(upnp_port_mappings,((gpointer )((glong )key)),((gpointer )((glong )value)));
  }
  if ((listen_data -> cb) != 0) 
    ( *(listen_data -> cb))((listen_data -> listenfd),(listen_data -> cb_data));
/* Clear the UPnP mapping data, since it's complete and purple_network_listen_cancel() will try to cancel
	 * it otherwise. */
  listen_data -> mapping_data = ((UPnPMappingAddRemove *)((void *)0));
  purple_network_listen_cancel(listen_data);
}

static gboolean purple_network_finish_pmp_map_cb(gpointer data)
{
  PurpleNetworkListenData *listen_data;
  gint key;
  gint value;
  listen_data = data;
  listen_data -> timer = 0;
/* add port mapping to hash table */
  key = (purple_network_get_port_from_fd((listen_data -> listenfd)));
  value = (listen_data -> socket_type);
  g_hash_table_insert(nat_pmp_port_mappings,((gpointer )((glong )key)),((gpointer )((glong )value)));
  if ((listen_data -> cb) != 0) 
    ( *(listen_data -> cb))((listen_data -> listenfd),(listen_data -> cb_data));
  purple_network_listen_cancel(listen_data);
  return 0;
}
static gboolean listen_map_external = (!0);

void purple_network_listen_map_external(gboolean map_external)
{
  listen_map_external = map_external;
}

static PurpleNetworkListenData *purple_network_do_listen(unsigned short port,int socket_family,int socket_type,PurpleNetworkListenCallback cb,gpointer cb_data)
{
  int listenfd = -1;
  int flags;
  const int on = 1;
  PurpleNetworkListenData *listen_data;
  unsigned short actual_port;
#ifdef HAVE_GETADDRINFO
  int errnum;
  struct addrinfo hints;
  struct addrinfo *res;
  struct addrinfo *next;
  char serv[6UL];
/*
	 * Get a list of addresses on this machine.
	 */
  g_snprintf(serv,(sizeof(serv)),"%hu",port);
  memset((&hints),0,(sizeof(struct addrinfo )));
  hints.ai_flags = 1;
  hints.ai_family = socket_family;
  hints.ai_socktype = socket_type;
/* any IP */
  errnum = getaddrinfo(0,serv,(&hints),&res);
  if (errnum != 0) {
#ifndef _WIN32
    purple_debug_warning("network","getaddrinfo: %s\n",purple_gai_strerror(errnum));
    if (errnum == -11) 
      purple_debug_warning("network","getaddrinfo: system error: %s\n",g_strerror( *__errno_location()));
#else
#endif
    return 0;
  }
{
/*
	 * Go through the list of addresses and attempt to listen on
	 * one of them.
	 * XXX - Try IPv6 addresses first?
	 */
    for (next = res; next != ((struct addrinfo *)((void *)0)); next = (next -> ai_next)) {
      listenfd = socket((next -> ai_family),(next -> ai_socktype),(next -> ai_protocol));
      if (listenfd < 0) 
        continue; 
      if (setsockopt(listenfd,1,2,(&on),(sizeof(on))) != 0) 
        purple_debug_warning("network","setsockopt(SO_REUSEADDR): %s\n",g_strerror( *__errno_location()));
      if (bind(listenfd,(next -> ai_addr),(next -> ai_addrlen)) == 0) 
/* success */
        break; 
/* XXX - It is unclear to me (datallah) whether we need to be
		   using a new socket each time */
      close(listenfd);
    }
  }
  freeaddrinfo(res);
  if (next == ((struct addrinfo *)((void *)0))) 
    return 0;
#else
#endif
  if ((socket_type == SOCK_STREAM) && (listen(listenfd,4) != 0)) {
    purple_debug_warning("network","listen: %s\n",g_strerror( *__errno_location()));
    close(listenfd);
    return 0;
  }
  flags = fcntl(listenfd,3);
  fcntl(listenfd,4,(flags | 04000));
#ifndef _WIN32
  fcntl(listenfd,2,1);
#endif
  actual_port = purple_network_get_port_from_fd(listenfd);
  purple_debug_info("network","Listening on port: %hu\n",actual_port);
  listen_data = ((PurpleNetworkListenData *)(g_malloc0_n(1,(sizeof(PurpleNetworkListenData )))));
  listen_data -> listenfd = listenfd;
  listen_data -> adding = (!0);
  listen_data -> retry = (!0);
  listen_data -> cb = cb;
  listen_data -> cb_data = cb_data;
  listen_data -> socket_type = socket_type;
  if ((!(purple_socket_speaks_ipv4(listenfd) != 0) || !(listen_map_external != 0)) || !(purple_prefs_get_bool("/purple/network/map_ports") != 0)) {
    purple_debug_info("network","Skipping external port mapping.\n");
/* The pmp_map_cb does what we want to do */
    listen_data -> timer = (purple_timeout_add(0,purple_network_finish_pmp_map_cb,listen_data));
  }
  else 
/* Attempt a NAT-PMP Mapping, which will return immediately */
if (purple_pmp_create_map((((socket_type == SOCK_STREAM)?PURPLE_PMP_TYPE_TCP : PURPLE_PMP_TYPE_UDP)),actual_port,actual_port,3600) != 0) {
    purple_debug_info("network","Created NAT-PMP mapping on port %i\n",actual_port);
/* We want to return listen_data now, and on the next run loop trigger the cb and destroy listen_data */
    listen_data -> timer = (purple_timeout_add(0,purple_network_finish_pmp_map_cb,listen_data));
  }
  else {
/* Attempt a UPnP Mapping */
    listen_data -> mapping_data = purple_upnp_set_port_mapping(actual_port,(((socket_type == SOCK_STREAM)?"TCP" : "UDP")),purple_network_set_upnp_port_mapping_cb,listen_data);
  }
  return listen_data;
}

PurpleNetworkListenData *purple_network_listen_family(unsigned short port,int socket_family,int socket_type,PurpleNetworkListenCallback cb,gpointer cb_data)
{
  do {
    if (port != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"port != 0");
      return 0;
    };
  }while (0);
  return purple_network_do_listen(port,socket_family,socket_type,cb,cb_data);
}

PurpleNetworkListenData *purple_network_listen(unsigned short port,int socket_type,PurpleNetworkListenCallback cb,gpointer cb_data)
{
  return purple_network_listen_family(port,0,socket_type,cb,cb_data);
}

PurpleNetworkListenData *purple_network_listen_range_family(unsigned short start,unsigned short end,int socket_family,int socket_type,PurpleNetworkListenCallback cb,gpointer cb_data)
{
  PurpleNetworkListenData *ret = (PurpleNetworkListenData *)((void *)0);
  if (purple_prefs_get_bool("/purple/network/ports_range_use") != 0) {
    start = (purple_prefs_get_int("/purple/network/ports_range_start"));
    end = (purple_prefs_get_int("/purple/network/ports_range_end"));
  }
  else {
    if (end < start) 
      end = start;
  }
{
    for (; start <= end; start++) {
      ret = purple_network_do_listen(start,0,socket_type,cb,cb_data);
      if (ret != ((PurpleNetworkListenData *)((void *)0))) 
        break; 
    }
  }
  return ret;
}

PurpleNetworkListenData *purple_network_listen_range(unsigned short start,unsigned short end,int socket_type,PurpleNetworkListenCallback cb,gpointer cb_data)
{
  return purple_network_listen_range_family(start,end,0,socket_type,cb,cb_data);
}

void purple_network_listen_cancel(PurpleNetworkListenData *listen_data)
{
  if ((listen_data -> mapping_data) != ((UPnPMappingAddRemove *)((void *)0))) 
    purple_upnp_cancel_port_mapping((listen_data -> mapping_data));
  if ((listen_data -> timer) > 0) 
    purple_timeout_remove((listen_data -> timer));
  g_free(listen_data);
}

unsigned short purple_network_get_port_from_fd(int fd)
{
  struct sockaddr_in addr;
  socklen_t len;
  do {
    if (fd >= 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"fd >= 0");
      return 0;
    };
  }while (0);
  len = (sizeof(addr));
  if (getsockname(fd,((struct sockaddr *)(&addr)),&len) == -1) {
    purple_debug_warning("network","getsockname: %s\n",g_strerror( *__errno_location()));
    return 0;
  }
  return ntohs(addr.sin_port);
}
#ifdef _WIN32
#ifndef NS_NLA
#define NS_NLA 15
#endif
/* purple_network_uninit has been called */
/* WSA_IO_PENDING indicates successful async notification will happen */
/* Make sure at least 30 seconds have elapsed since the last
		 * notification so we don't peg the cpu if this keeps changing. */
/* This will block until NLA notifies us */
/* Time to die */
/*purple_timeout_add(0, _print_debug_msg,
							   g_strdup_printf("thread found network '%s'\n",
											   res->lpszServiceInstanceName ? res->lpszServiceInstanceName : "(NULL)"));*/
#endif

gboolean purple_network_is_available()
{
#ifdef HAVE_NETWORKMANAGER
  if (force_online != 0) 
    return (!0);
  if (!(have_nm_state != 0)) {
    have_nm_state = (!0);
    nm_state = nm_get_network_state();
    if (nm_state == NM_STATE_UNKNOWN) 
      purple_debug_warning("network","NetworkManager not active. Assuming connection exists.\n");
  }
  switch(nm_state){
    case NM_STATE_UNKNOWN:
{
    }
#if NM_CHECK_VERSION(0,8,992)
    case NM_STATE_CONNECTED_LOCAL:
{
    }
    case NM_STATE_CONNECTED_SITE:
{
    }
    case NM_STATE_CONNECTED_GLOBAL:
{
#else
#endif
      return (!0);
    }
    default:
{
      break; 
    }
  }
  return 0;
#elif defined _WIN32
#else
#endif
}

void purple_network_force_online()
{
#if defined(HAVE_NETWORKMANAGER) || defined(_WIN32)
  force_online = (!0);
#endif
}
#ifdef HAVE_NETWORKMANAGER

static void nm_update_state(NMState state)
{
  NMState prev = nm_state;
  PurpleConnectionUiOps *ui_ops = purple_connections_get_ui_ops();
  have_nm_state = (!0);
  nm_state = state;
  purple_signal_emit(purple_network_get_handle(),"network-configuration-changed",((void *)((void *)0)));
{
    switch(state){
#if NM_CHECK_VERSION(0,8,992)
      case NM_STATE_CONNECTED_LOCAL:
{
      }
      case NM_STATE_CONNECTED_SITE:
{
      }
      case NM_STATE_CONNECTED_GLOBAL:
{
#else
#endif
/* Call res_init in case DNS servers have changed */
        __res_init();
/* update STUN IP in case we it changed (theoretically we could
			   have gone from IPv4 to IPv6, f.ex. or we were previously
			   offline */
        purple_network_set_stun_server(purple_prefs_get_string("/purple/network/stun_server"));
        purple_network_set_turn_server(purple_prefs_get_string("/purple/network/turn_server"));
        if ((ui_ops != ((PurpleConnectionUiOps *)((void *)0))) && ((ui_ops -> network_connected) != ((void (*)())((void *)0)))) 
          ( *(ui_ops -> network_connected))();
        break; 
      }
      case NM_STATE_ASLEEP:
{
      }
      case NM_STATE_DISCONNECTED:
{
      }
#if NM_CHECK_VERSION(0,8,992)
      case NM_STATE_DISCONNECTING:
{
      }
      case NM_STATE_CONNECTING:
{
#endif
        if ((prev != NM_STATE_CONNECTED_GLOBAL) && (prev != NM_STATE_UNKNOWN)) 
          break; 
        if ((ui_ops != ((PurpleConnectionUiOps *)((void *)0))) && ((ui_ops -> network_disconnected) != ((void (*)())((void *)0)))) 
          ( *(ui_ops -> network_disconnected))();
        break; 
      }
      default:
{
        break; 
      }
    }
  }
}

static void nm_state_change_cb(DBusGProxy *proxy,NMState state,gpointer user_data)
{
  purple_debug_info("network","Got StateChange from NetworkManager: %d.\n",state);
  nm_update_state(state);
}

static NMState nm_get_network_state()
{
  GError *err = (GError *)((void *)0);
  NMState state = NM_STATE_UNKNOWN;
  if (!(nm_proxy != 0)) 
    return NM_STATE_UNKNOWN;
  if (!(dbus_g_proxy_call(nm_proxy,"state",&err,((GType )(0 << 2)),((GType )(7 << 2)),&state,((GType )(0 << 2))) != 0)) {
    g_error_free(err);
    return NM_STATE_UNKNOWN;
  }
  return state;
}

static void nm_dbus_name_owner_changed_cb(DBusGProxy *proxy,char *service,char *old_owner,char *new_owner,gpointer user_data)
{
  if (g_str_equal(service,"org.freedesktop.NetworkManager") != 0) {
    gboolean old_owner_good = ((old_owner != 0) && (old_owner[0] != 0));
    gboolean new_owner_good = ((new_owner != 0) && (new_owner[0] != 0));
    purple_debug_info("network","Got NameOwnerChanged signal, service = \'%s\', old_owner = \'%s\', new_owner = \'%s\'\n",service,old_owner,new_owner);
/* Equivalent to old ServiceCreated signal */
    if (!(old_owner_good != 0) && (new_owner_good != 0)) {
      purple_debug_info("network","NetworkManager has started.\n");
      nm_update_state(nm_get_network_state());
/* Equivalent to old ServiceDeleted signal */
    }
    else if ((old_owner_good != 0) && !(new_owner_good != 0)) {
      purple_debug_info("network","NetworkManager has gone away.\n");
      nm_update_state(NM_STATE_UNKNOWN);
    }
  }
}
#endif

static void purple_network_ip_lookup_cb(GSList *hosts,gpointer data,const char *error_message)
{
  const gchar **ip = (const gchar **)data;
  if (error_message != 0) {
    purple_debug_error("network","lookup of IP address failed: %s\n",error_message);
    g_slist_free(hosts);
    return ;
  }
  if ((hosts != 0) && ((((hosts != 0)?( *((GSList *)hosts)).next : ((struct _GSList *)((void *)0)))) != 0)) {
    struct sockaddr *addr = ( *(((hosts != 0)?( *((GSList *)hosts)).next : ((struct _GSList *)((void *)0))))).data;
    char dst[46UL];
    if ((addr -> sa_family) == 10) {
      inet_ntop((addr -> sa_family),(&( *((struct sockaddr_in6 *)addr)).sin6_addr),dst,(sizeof(dst)));
    }
    else {
      inet_ntop((addr -> sa_family),(&( *((struct sockaddr_in *)addr)).sin_addr),dst,(sizeof(dst)));
    }
     *ip = (g_strdup(dst));
    purple_debug_info("network","set IP address: %s\n", *ip);
  }
  while(hosts != ((GSList *)((void *)0))){
    hosts = g_slist_delete_link(hosts,hosts);
/* Free the address */
    g_free((hosts -> data));
    hosts = g_slist_delete_link(hosts,hosts);
  }
}

void purple_network_set_stun_server(const gchar *stun_server)
{
  if ((stun_server != 0) && (stun_server[0] != 0)) {
    if (purple_network_is_available() != 0) {
      purple_debug_info("network","running DNS query for STUN server\n");
      purple_dnsquery_a_account(0,stun_server,3478,purple_network_ip_lookup_cb,(&stun_ip));
    }
    else {
      purple_debug_info("network","network is unavailable, don\'t try to update STUN IP");
    }
  }
  else if (stun_ip != 0) {
    g_free(stun_ip);
    stun_ip = ((gchar *)((void *)0));
  }
}

void purple_network_set_turn_server(const gchar *turn_server)
{
  if ((turn_server != 0) && (turn_server[0] != 0)) {
    if (purple_network_is_available() != 0) {
      purple_debug_info("network","running DNS query for TURN server\n");
      purple_dnsquery_a_account(0,turn_server,purple_prefs_get_int("/purple/network/turn_port"),purple_network_ip_lookup_cb,(&turn_ip));
    }
    else {
      purple_debug_info("network","network is unavailable, don\'t try to update TURN IP");
    }
  }
  else if (turn_ip != 0) {
    g_free(turn_ip);
    turn_ip = ((gchar *)((void *)0));
  }
}

const gchar *purple_network_get_stun_ip()
{
  return stun_ip;
}

const gchar *purple_network_get_turn_ip()
{
  return turn_ip;
}

void *purple_network_get_handle()
{
  static int handle;
  return (&handle);
}

static void purple_network_upnp_mapping_remove_cb(gboolean sucess,gpointer data)
{
  purple_debug_info("network","done removing UPnP port mapping\n");
}
/* the reason for these functions to have these signatures is to be able to
 use them for g_hash_table_foreach to clean remaining port mappings, which is
 not yet done */

static void purple_network_upnp_mapping_remove(gpointer key,gpointer value,gpointer user_data)
{
  gint port = (gint )((glong )key);
  gint protocol = (gint )((glong )value);
  purple_debug_info("network","removing UPnP port mapping for port %d\n",port);
  purple_upnp_remove_port_mapping(port,((protocol == SOCK_STREAM)?"TCP" : "UDP"),purple_network_upnp_mapping_remove_cb,0);
  g_hash_table_remove(upnp_port_mappings,((gpointer )((glong )port)));
}

static void purple_network_nat_pmp_mapping_remove(gpointer key,gpointer value,gpointer user_data)
{
  gint port = (gint )((glong )key);
  gint protocol = (gint )((glong )value);
  purple_debug_info("network","removing NAT-PMP port mapping for port %d\n",port);
  purple_pmp_destroy_map(((protocol == SOCK_STREAM)?PURPLE_PMP_TYPE_TCP : PURPLE_PMP_TYPE_UDP),port);
  g_hash_table_remove(nat_pmp_port_mappings,((gpointer )((glong )port)));
}

void purple_network_remove_port_mapping(gint fd)
{
  int port = (purple_network_get_port_from_fd(fd));
  gint protocol = (gint )((glong )(g_hash_table_lookup(upnp_port_mappings,((gpointer )((glong )port)))));
  if (protocol != 0) {
    purple_network_upnp_mapping_remove(((gpointer )((glong )port)),((gpointer )((glong )protocol)),0);
  }
  else {
    protocol = ((gint )((glong )(g_hash_table_lookup(nat_pmp_port_mappings,((gpointer )((glong )port))))));
    if (protocol != 0) {
      purple_network_nat_pmp_mapping_remove(((gpointer )((glong )port)),((gpointer )((glong )protocol)),0);
    }
  }
}

int purple_network_convert_idn_to_ascii(const gchar *in,gchar **out)
{
#ifdef USE_IDN
  char *tmp;
  int ret;
  do {
    if (out != ((gchar **)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"out != NULL");
      return -1;
    };
  }while (0);
  ret = idna_to_ascii_8z(in,&tmp,IDNA_USE_STD3_ASCII_RULES);
  if (ret != IDNA_SUCCESS) {
     *out = ((gchar *)((void *)0));
    return ret;
  }
   *out = g_strdup(tmp);
/* This *MUST* be freed with free, not g_free */
  free(tmp);
  return 0;
#else
#endif
}

void purple_network_init()
{
#ifdef HAVE_NETWORKMANAGER
  GError *error = (GError *)((void *)0);
#endif
#ifdef _WIN32
/* Assume there is a network */
/* Don't listen for network changes if we can't tell anyway */
#endif
  purple_prefs_add_none("/purple/network");
  purple_prefs_add_string("/purple/network/stun_server","");
  purple_prefs_add_string("/purple/network/turn_server","");
  purple_prefs_add_int("/purple/network/turn_port",3478);
  purple_prefs_add_int("/purple/network/turn_port_tcp",3478);
  purple_prefs_add_string("/purple/network/turn_username","");
  purple_prefs_add_string("/purple/network/turn_password","");
  purple_prefs_add_bool("/purple/network/auto_ip",(!0));
  purple_prefs_add_string("/purple/network/public_ip","");
  purple_prefs_add_bool("/purple/network/map_ports",(!0));
  purple_prefs_add_bool("/purple/network/ports_range_use",0);
  purple_prefs_add_int("/purple/network/ports_range_start",1024);
  purple_prefs_add_int("/purple/network/ports_range_end",04000);
  if ((purple_prefs_get_bool("/purple/network/map_ports") != 0) || (purple_prefs_get_bool("/purple/network/auto_ip") != 0)) 
    purple_upnp_discover(0,0);
#ifdef HAVE_NETWORKMANAGER
  nm_conn = dbus_g_bus_get(DBUS_BUS_SYSTEM,&error);
  if (!(nm_conn != 0)) {
    purple_debug_warning("network","Error connecting to DBus System service: %s.\n",(error -> message));
  }
  else {
    nm_proxy = dbus_g_proxy_new_for_name(nm_conn,"org.freedesktop.NetworkManager","/org/freedesktop/NetworkManager","org.freedesktop.NetworkManager");
/* NM 0.6 signal */
    dbus_g_proxy_add_signal(nm_proxy,"StateChange",((GType )(7 << 2)),((GType )(0 << 2)));
    dbus_g_proxy_connect_signal(nm_proxy,"StateChange",((GCallback )nm_state_change_cb),0,0);
/* NM 0.7 and later signal */
    dbus_g_proxy_add_signal(nm_proxy,"StateChanged",((GType )(7 << 2)),((GType )(0 << 2)));
    dbus_g_proxy_connect_signal(nm_proxy,"StateChanged",((GCallback )nm_state_change_cb),0,0);
    dbus_proxy = dbus_g_proxy_new_for_name(nm_conn,"org.freedesktop.DBus","/org/freedesktop/DBus","org.freedesktop.DBus");
    dbus_g_proxy_add_signal(dbus_proxy,"NameOwnerChanged",((GType )(16 << 2)),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(0 << 2)));
    dbus_g_proxy_connect_signal(dbus_proxy,"NameOwnerChanged",((GCallback )nm_dbus_name_owner_changed_cb),0,0);
  }
#endif
  purple_signal_register(purple_network_get_handle(),"network-configuration-changed",purple_marshal_VOID,0,0);
  purple_pmp_init();
  purple_upnp_init();
  purple_network_set_stun_server(purple_prefs_get_string("/purple/network/stun_server"));
  purple_network_set_turn_server(purple_prefs_get_string("/purple/network/turn_server"));
  upnp_port_mappings = g_hash_table_new(g_direct_hash,g_direct_equal);
  nat_pmp_port_mappings = g_hash_table_new(g_direct_hash,g_direct_equal);
}

void purple_network_uninit()
{
#ifdef HAVE_NETWORKMANAGER
  if (nm_proxy != 0) {
    dbus_g_proxy_disconnect_signal(nm_proxy,"StateChange",((GCallback )nm_state_change_cb),0);
    dbus_g_proxy_disconnect_signal(nm_proxy,"StateChanged",((GCallback )nm_state_change_cb),0);
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)nm_proxy),((GType )(20 << 2))))));
  }
  if (dbus_proxy != 0) {
    dbus_g_proxy_disconnect_signal(dbus_proxy,"NameOwnerChanged",((GCallback )nm_dbus_name_owner_changed_cb),0);
    g_object_unref(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)dbus_proxy),((GType )(20 << 2))))));
  }
  if (nm_conn != 0) 
    dbus_g_connection_unref(nm_conn);
#endif
#ifdef _WIN32
/* Trigger the NLA thread to stop waiting for network changes. Not
		 * doing this can cause hangs on WSACleanup. */
#endif
  purple_signal_unregister(purple_network_get_handle(),"network-configuration-changed");
  if (stun_ip != 0) 
    g_free(stun_ip);
  g_hash_table_destroy(upnp_port_mappings);
  g_hash_table_destroy(nat_pmp_port_mappings);
/* TODO: clean up remaining port mappings, note calling
	 purple_upnp_remove_port_mapping from here doesn't quite work... */
}
