/**
 * @file xmlnode.c XML DOM functions
 */
/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
/* A lot of this code at least resembles the code in libxode, but since
 * libxode uses memory pools that we simply have no need for, I decided to
 * write my own stuff.  Also, re-writing this lets me be as lightweight
 * as I want to be.  Thank you libxode for giving me a good starting point */
#define _PURPLE_XMLNODE_C_
#include "internal.h"
#include "debug.h"
#include <libxml/parser.h>
#include <string.h>
#include <glib.h>
#include "dbus-maybe.h"
#include "util.h"
#include "xmlnode.h"
#ifdef _WIN32
# define NEWLINE_S "\r\n"
#else
# define NEWLINE_S "\n"
#endif

static xmlnode *new_node(const char *name,XMLNodeType type)
{
  xmlnode *node = (xmlnode *)(g_malloc0_n(1,(sizeof(xmlnode ))));
  node -> name = g_strdup(name);
  node -> type = type;
{
    xmlnode *typed_ptr = node;
    purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_xmlnode);
  };
  return node;
}

xmlnode *xmlnode_new(const char *name)
{
  do {
    if ((name != ((const char *)((void *)0))) && (( *name) != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL && *name != \'\\0\'");
      return 0;
    };
  }while (0);
  return new_node(name,XMLNODE_TYPE_TAG);
}

xmlnode *xmlnode_new_child(xmlnode *parent,const char *name)
{
  xmlnode *node;
  do {
    if (parent != ((xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"parent != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((name != ((const char *)((void *)0))) && (( *name) != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL && *name != \'\\0\'");
      return 0;
    };
  }while (0);
  node = new_node(name,XMLNODE_TYPE_TAG);
  xmlnode_insert_child(parent,node);
  return node;
}

void xmlnode_insert_child(xmlnode *parent,xmlnode *child)
{
  do {
    if (parent != ((xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"parent != NULL");
      return ;
    };
  }while (0);
  do {
    if (child != ((xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"child != NULL");
      return ;
    };
  }while (0);
  child -> parent = parent;
  if ((parent -> lastchild) != 0) {
    ( *(parent -> lastchild)).next = child;
  }
  else {
    parent -> child = child;
  }
  parent -> lastchild = child;
}

void xmlnode_insert_data(xmlnode *node,const char *data,gssize size)
{
  xmlnode *child;
  gsize real_size;
  do {
    if (node != ((xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return ;
    };
  }while (0);
  do {
    if (data != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"data != NULL");
      return ;
    };
  }while (0);
  do {
    if (size != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"size != 0");
      return ;
    };
  }while (0);
  real_size = ((size == (-1))?strlen(data) : size);
  child = new_node(0,XMLNODE_TYPE_DATA);
  child -> data = (g_memdup(data,real_size));
  child -> data_sz = real_size;
  xmlnode_insert_child(node,child);
}

void xmlnode_remove_attrib(xmlnode *node,const char *attr)
{
  xmlnode *attr_node;
  xmlnode *sibling = (xmlnode *)((void *)0);
  do {
    if (node != ((xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return ;
    };
  }while (0);
  do {
    if (attr != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"attr != NULL");
      return ;
    };
  }while (0);
  attr_node = (node -> child);
  while(attr_node != 0){
    if (((attr_node -> type) == XMLNODE_TYPE_ATTRIB) && (purple_strequal((attr_node -> name),attr) != 0)) {
      if ((node -> lastchild) == attr_node) {
        node -> lastchild = sibling;
      }
      if (sibling == ((xmlnode *)((void *)0))) {
        node -> child = (attr_node -> next);
        xmlnode_free(attr_node);
        attr_node = (node -> child);
      }
      else {
        sibling -> next = (attr_node -> next);
        sibling = (attr_node -> next);
        xmlnode_free(attr_node);
        attr_node = sibling;
      }
    }
    else {
      attr_node = (attr_node -> next);
    }
    sibling = attr_node;
  }
}

void xmlnode_remove_attrib_with_namespace(xmlnode *node,const char *attr,const char *xmlns)
{
  xmlnode *attr_node;
  xmlnode *sibling = (xmlnode *)((void *)0);
  do {
    if (node != ((xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return ;
    };
  }while (0);
  do {
    if (attr != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"attr != NULL");
      return ;
    };
  }while (0);
  for (attr_node = (node -> child); attr_node != 0; attr_node = (attr_node -> next)) {
    if ((((attr_node -> type) == XMLNODE_TYPE_ATTRIB) && (purple_strequal(attr,(attr_node -> name)) != 0)) && (purple_strequal(xmlns,(attr_node -> xmlns)) != 0)) {
      if (sibling == ((xmlnode *)((void *)0))) {
        node -> child = (attr_node -> next);
      }
      else {
        sibling -> next = (attr_node -> next);
      }
      if ((node -> lastchild) == attr_node) {
        node -> lastchild = sibling;
      }
      xmlnode_free(attr_node);
      return ;
    }
    sibling = attr_node;
  }
}

void xmlnode_set_attrib(xmlnode *node,const char *attr,const char *value)
{
  xmlnode_remove_attrib(node,attr);
  xmlnode_set_attrib_full(node,attr,0,0,value);
}

void xmlnode_set_attrib_with_namespace(xmlnode *node,const char *attr,const char *xmlns,const char *value)
{
  xmlnode_set_attrib_full(node,attr,xmlns,0,value);
}

void xmlnode_set_attrib_with_prefix(xmlnode *node,const char *attr,const char *prefix,const char *value)
{
  xmlnode_set_attrib_full(node,attr,0,prefix,value);
}

void xmlnode_set_attrib_full(xmlnode *node,const char *attr,const char *xmlns,const char *prefix,const char *value)
{
  xmlnode *attrib_node;
  do {
    if (node != ((xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return ;
    };
  }while (0);
  do {
    if (attr != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"attr != NULL");
      return ;
    };
  }while (0);
  do {
    if (value != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"value != NULL");
      return ;
    };
  }while (0);
  xmlnode_remove_attrib_with_namespace(node,attr,xmlns);
  attrib_node = new_node(attr,XMLNODE_TYPE_ATTRIB);
  attrib_node -> data = g_strdup(value);
  attrib_node -> xmlns = g_strdup(xmlns);
  attrib_node -> prefix = g_strdup(prefix);
  xmlnode_insert_child(node,attrib_node);
}

const char *xmlnode_get_attrib(const xmlnode *node,const char *attr)
{
  xmlnode *x;
  do {
    if (node != ((const xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return 0;
    };
  }while (0);
  do {
    if (attr != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"attr != NULL");
      return 0;
    };
  }while (0);
  for (x = (node -> child); x != 0; x = (x -> next)) {
    if (((x -> type) == XMLNODE_TYPE_ATTRIB) && (purple_strequal(attr,(x -> name)) != 0)) {
      return (x -> data);
    }
  }
  return 0;
}

const char *xmlnode_get_attrib_with_namespace(const xmlnode *node,const char *attr,const char *xmlns)
{
  const xmlnode *x;
  do {
    if (node != ((const xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return 0;
    };
  }while (0);
  do {
    if (attr != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"attr != NULL");
      return 0;
    };
  }while (0);
  for (x = (node -> child); x != 0; x = (x -> next)) {
    if ((((x -> type) == XMLNODE_TYPE_ATTRIB) && (purple_strequal(attr,(x -> name)) != 0)) && (purple_strequal(xmlns,(x -> xmlns)) != 0)) {
      return (x -> data);
    }
  }
  return 0;
}

void xmlnode_set_namespace(xmlnode *node,const char *xmlns)
{
  do {
    if (node != ((xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return ;
    };
  }while (0);
  g_free((node -> xmlns));
  node -> xmlns = g_strdup(xmlns);
}

const char *xmlnode_get_namespace(xmlnode *node)
{
  do {
    if (node != ((xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return 0;
    };
  }while (0);
  return (node -> xmlns);
}

void xmlnode_set_prefix(xmlnode *node,const char *prefix)
{
  do {
    if (node != ((xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return ;
    };
  }while (0);
  g_free((node -> prefix));
  node -> prefix = g_strdup(prefix);
}

const char *xmlnode_get_prefix(const xmlnode *node)
{
  do {
    if (node != ((const xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return 0;
    };
  }while (0);
  return (node -> prefix);
}

xmlnode *xmlnode_get_parent(const xmlnode *child)
{
  do {
    if (child != ((const xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"child != NULL");
      return 0;
    };
  }while (0);
  return child -> parent;
}

void xmlnode_free(xmlnode *node)
{
  xmlnode *x;
  xmlnode *y;
  do {
    if (node != ((xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return ;
    };
  }while (0);
/* if we're part of a tree, remove ourselves from the tree first */
  if (((xmlnode *)((void *)0)) != (node -> parent)) {
    if (( *(node -> parent)).child == node) {
      ( *(node -> parent)).child = (node -> next);
      if (( *(node -> parent)).lastchild == node) 
        ( *(node -> parent)).lastchild = (node -> next);
    }
    else {
      xmlnode *prev = ( *(node -> parent)).child;
      while((prev != 0) && ((prev -> next) != node)){
        prev = (prev -> next);
      }
      if (prev != 0) {
        prev -> next = (node -> next);
        if (( *(node -> parent)).lastchild == node) 
          ( *(node -> parent)).lastchild = prev;
      }
    }
  }
/* now free our children */
  x = (node -> child);
  while(x != 0){
    y = (x -> next);
    xmlnode_free(x);
    x = y;
  }
/* now dispose of ourselves */
  g_free((node -> name));
  g_free((node -> data));
  g_free((node -> xmlns));
  g_free((node -> prefix));
  if ((node -> namespace_map) != 0) 
    g_hash_table_destroy((node -> namespace_map));
  purple_dbus_unregister_pointer(node);
  g_free(node);
}

xmlnode *xmlnode_get_child(const xmlnode *parent,const char *name)
{
  return xmlnode_get_child_with_namespace(parent,name,0);
}

xmlnode *xmlnode_get_child_with_namespace(const xmlnode *parent,const char *name,const char *ns)
{
  xmlnode *x;
  xmlnode *ret = (xmlnode *)((void *)0);
  char **names;
  char *parent_name;
  char *child_name;
  do {
    if (parent != ((const xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"parent != NULL");
      return 0;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return 0;
    };
  }while (0);
  names = g_strsplit(name,"/",2);
  parent_name = names[0];
  child_name = names[1];
{
    for (x = (parent -> child); x != 0; x = (x -> next)) {
/* XXX: Is it correct to ignore the namespace for the match if none was specified? */
      const char *xmlns = (const char *)((void *)0);
      if (ns != 0) 
        xmlns = xmlnode_get_namespace(x);
      if ((((x -> type) == XMLNODE_TYPE_TAG) && (purple_strequal(parent_name,(x -> name)) != 0)) && (purple_strequal(ns,xmlns) != 0)) {
        ret = x;
        break; 
      }
    }
  }
  if ((child_name != 0) && (ret != 0)) 
    ret = xmlnode_get_child(ret,child_name);
  g_strfreev(names);
  return ret;
}

char *xmlnode_get_data(const xmlnode *node)
{
  GString *str = (GString *)((void *)0);
  xmlnode *c;
  do {
    if (node != ((const xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return 0;
    };
  }while (0);
  for (c = (node -> child); c != 0; c = (c -> next)) {
    if ((c -> type) == XMLNODE_TYPE_DATA) {
      if (!(str != 0)) 
        str = g_string_new_len((c -> data),(c -> data_sz));
      else 
        str = g_string_append_len(str,(c -> data),(c -> data_sz));
    }
  }
  if (str == ((GString *)((void *)0))) 
    return 0;
  return g_string_free(str,0);
}

char *xmlnode_get_data_unescaped(const xmlnode *node)
{
  char *escaped = xmlnode_get_data(node);
  char *unescaped = (escaped != 0)?purple_unescape_html(escaped) : ((char *)((void *)0));
  g_free(escaped);
  return unescaped;
}

static void xmlnode_to_str_foreach_append_ns(const char *key,const char *value,GString *buf)
{
  if (( *key) != 0) {
    g_string_append_printf(buf," xmlns:%s=\'%s\'",key,value);
  }
  else {
    g_string_append_printf(buf," xmlns=\'%s\'",value);
  }
}

static char *xmlnode_to_str_helper(const xmlnode *node,int *len,gboolean formatting,int depth)
{
  GString *text = g_string_new("");
  const char *prefix;
  const xmlnode *c;
  char *node_name;
  char *esc;
  char *esc2;
  char *tab = (char *)((void *)0);
  gboolean need_end = 0;
  gboolean pretty = formatting;
  do {
    if (node != ((const xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return 0;
    };
  }while (0);
  if ((pretty != 0) && (depth != 0)) {
    tab = g_strnfill(depth,9);
    text = g_string_append(text,tab);
  }
  node_name = g_markup_escape_text((node -> name),(-1));
  prefix = xmlnode_get_prefix(node);
  if (prefix != 0) {
    g_string_append_printf(text,"<%s:%s",prefix,node_name);
  }
  else {
    g_string_append_printf(text,"<%s",node_name);
  }
  if ((node -> namespace_map) != 0) {
    g_hash_table_foreach((node -> namespace_map),((GHFunc )xmlnode_to_str_foreach_append_ns),text);
  }
  else if ((node -> xmlns) != 0) {
    if (!((node -> parent) != 0) || !(purple_strequal((node -> xmlns),( *(node -> parent)).xmlns) != 0)) {
      char *xmlns = g_markup_escape_text((node -> xmlns),(-1));
      g_string_append_printf(text," xmlns=\'%s\'",xmlns);
      g_free(xmlns);
    }
  }
  for (c = (node -> child); c != 0; c = (c -> next)) {
    if ((c -> type) == XMLNODE_TYPE_ATTRIB) {
      const char *aprefix = xmlnode_get_prefix(c);
      esc = g_markup_escape_text((c -> name),(-1));
      esc2 = g_markup_escape_text((c -> data),(-1));
      if (aprefix != 0) {
        g_string_append_printf(text," %s:%s=\'%s\'",aprefix,esc,esc2);
      }
      else {
        g_string_append_printf(text," %s=\'%s\'",esc,esc2);
      }
      g_free(esc);
      g_free(esc2);
    }
    else if (((c -> type) == XMLNODE_TYPE_TAG) || ((c -> type) == XMLNODE_TYPE_DATA)) {
      if ((c -> type) == XMLNODE_TYPE_DATA) 
        pretty = 0;
      need_end = (!0);
    }
  }
  if (need_end != 0) {
    g_string_append_printf(text,">%s",((pretty != 0)?"\n" : ""));
    for (c = (node -> child); c != 0; c = (c -> next)) {
      if ((c -> type) == XMLNODE_TYPE_TAG) {
        int esc_len;
        esc = xmlnode_to_str_helper(c,&esc_len,pretty,(depth + 1));
        text = g_string_append_len(text,esc,esc_len);
        g_free(esc);
      }
      else if (((c -> type) == XMLNODE_TYPE_DATA) && ((c -> data_sz) > 0)) {
        esc = g_markup_escape_text((c -> data),(c -> data_sz));
        text = g_string_append(text,esc);
        g_free(esc);
      }
    }
    if ((tab != 0) && (pretty != 0)) 
      text = g_string_append(text,tab);
    if (prefix != 0) {
      g_string_append_printf(text,"</%s:%s>%s",prefix,node_name,((formatting != 0)?"\n" : ""));
    }
    else {
      g_string_append_printf(text,"</%s>%s",node_name,((formatting != 0)?"\n" : ""));
    }
  }
  else {
    g_string_append_printf(text,"/>%s",((formatting != 0)?"\n" : ""));
  }
  g_free(node_name);
  g_free(tab);
  if (len != 0) 
     *len = (text -> len);
  return g_string_free(text,0);
}

char *xmlnode_to_str(const xmlnode *node,int *len)
{
  return xmlnode_to_str_helper(node,len,0,0);
}

char *xmlnode_to_formatted_str(const xmlnode *node,int *len)
{
  char *xml;
  char *xml_with_declaration;
  do {
    if (node != ((const xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return 0;
    };
  }while (0);
  xml = xmlnode_to_str_helper(node,len,(!0),0);
  xml_with_declaration = g_strdup_printf("<\?xml version=\'1.0\' encoding=\'UTF-8\' \?>\n\n%s",xml);
  g_free(xml);
  if (len != 0) 
     *len += sizeof(( *((char (*)[42UL])"<\?xml version=\'1.0\' encoding=\'UTF-8\' \?>\n\n"))) - 1;
  return xml_with_declaration;
}

struct _xmlnode_parser_data 
{
  xmlnode *current;
  gboolean error;
}
;

static void xmlnode_parser_element_start_libxml(void *user_data,const xmlChar *element_name,const xmlChar *prefix,const xmlChar *xmlns,int nb_namespaces,const xmlChar **namespaces,int nb_attributes,int nb_defaulted,const xmlChar **attributes)
{
  struct _xmlnode_parser_data *xpd = user_data;
  xmlnode *node;
  int i;
  int j;
  if (!(element_name != 0) || ((xpd -> error) != 0)) {
    return ;
  }
  else {
    if ((xpd -> current) != 0) 
      node = xmlnode_new_child((xpd -> current),((const char *)element_name));
    else 
      node = xmlnode_new(((const char *)element_name));
    xmlnode_set_namespace(node,((const char *)xmlns));
    xmlnode_set_prefix(node,((const char *)prefix));
    if (nb_namespaces != 0) {
      node -> namespace_map = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
      for (((i = 0) , (j = 0)); i < nb_namespaces; (i++ , (j += 2))) {
        const char *key = (const char *)namespaces[j];
        const char *val = (const char *)namespaces[j + 1];
        g_hash_table_insert((node -> namespace_map),(g_strdup(((key != 0)?key : ""))),(g_strdup(((val != 0)?val : ""))));
      }
    }
    for (i = 0; i < (nb_attributes * 5); i += 5) {
      const char *name = (const char *)attributes[i];
      const char *prefix = (const char *)attributes[i + 1];
      char *txt;
      int attrib_len = (attributes[i + 4] - attributes[i + 3]);
      char *attrib = g_strndup(((const char *)attributes[i + 3]),attrib_len);
      txt = attrib;
      attrib = purple_unescape_text(txt);
      g_free(txt);
      xmlnode_set_attrib_full(node,name,0,prefix,attrib);
      g_free(attrib);
    }
    xpd -> current = node;
  }
}

static void xmlnode_parser_element_end_libxml(void *user_data,const xmlChar *element_name,const xmlChar *prefix,const xmlChar *xmlns)
{
  struct _xmlnode_parser_data *xpd = user_data;
  if ((!(element_name != 0) || !((xpd -> current) != 0)) || ((xpd -> error) != 0)) 
    return ;
  if (( *(xpd -> current)).parent != 0) {
    if (!(xmlStrcmp(((xmlChar *)( *(xpd -> current)).name),element_name) != 0)) 
      xpd -> current = ( *(xpd -> current)).parent;
  }
}

static void xmlnode_parser_element_text_libxml(void *user_data,const xmlChar *text,int text_len)
{
  struct _xmlnode_parser_data *xpd = user_data;
  if (!((xpd -> current) != 0) || ((xpd -> error) != 0)) 
    return ;
  if (!(text != 0) || !(text_len != 0)) 
    return ;
  xmlnode_insert_data((xpd -> current),((const char *)text),text_len);
}

static void xmlnode_parser_error_libxml(void *user_data,const char *msg,... )
{
  struct _xmlnode_parser_data *xpd = user_data;
  char errmsg[2048UL];
  va_list args;
  xpd -> error = (!0);
  va_start(args,msg);
  vsnprintf(errmsg,(sizeof(errmsg)),msg,args);
  va_end(args);
  purple_debug_error("xmlnode","Error parsing xml file: %s",errmsg);
}

static void xmlnode_parser_structural_error_libxml(void *user_data,xmlErrorPtr error)
{
  struct _xmlnode_parser_data *xpd = user_data;
  if ((error != 0) && (((error -> level) == XML_ERR_ERROR) || ((error -> level) == XML_ERR_FATAL))) {
    xpd -> error = (!0);
    purple_debug_error("xmlnode","XML parser error for xmlnode %p: Domain %i, code %i, level %i: %s",user_data,(error -> domain),(error -> code),(error -> level),(((error -> message) != 0)?(error -> message) : "(null)\n"));
  }
  else if (error != 0) 
    purple_debug_warning("xmlnode","XML parser error for xmlnode %p: Domain %i, code %i, level %i: %s",user_data,(error -> domain),(error -> code),(error -> level),(((error -> message) != 0)?(error -> message) : "(null)\n"));
  else 
    purple_debug_warning("xmlnode","XML parser error for xmlnode %p\n",user_data);
}
static xmlSAXHandler xmlnode_parser_libxml = {((internalSubsetSAXFunc )((void *)0)), ((isStandaloneSAXFunc )((void *)0)), ((hasInternalSubsetSAXFunc )((void *)0)), ((hasExternalSubsetSAXFunc )((void *)0)), ((resolveEntitySAXFunc )((void *)0)), ((getEntitySAXFunc )((void *)0)), ((entityDeclSAXFunc )((void *)0)), ((notationDeclSAXFunc )((void *)0)), ((attributeDeclSAXFunc )((void *)0)), ((elementDeclSAXFunc )((void *)0)), ((unparsedEntityDeclSAXFunc )((void *)0)), ((setDocumentLocatorSAXFunc )((void *)0)), ((startDocumentSAXFunc )((void *)0)), ((endDocumentSAXFunc )((void *)0)), ((startElementSAXFunc )((void *)0)), ((endElementSAXFunc )((void *)0)), ((referenceSAXFunc )((void *)0)), (xmlnode_parser_element_text_libxml), ((ignorableWhitespaceSAXFunc )((void *)0)), ((processingInstructionSAXFunc )((void *)0)), ((commentSAXFunc )((void *)0)), ((warningSAXFunc )((void *)0)), (xmlnode_parser_error_libxml), ((fatalErrorSAXFunc )((void *)0)), ((getParameterEntitySAXFunc )((void *)0)), ((cdataBlockSAXFunc )((void *)0)), ((externalSubsetSAXFunc )((void *)0)), (0xDEEDBEAF), ((void *)((void *)0)), (xmlnode_parser_element_start_libxml), (xmlnode_parser_element_end_libxml), (xmlnode_parser_structural_error_libxml)
/* internalSubset */
/* isStandalone */
/* hasInternalSubset */
/* hasExternalSubset */
/* resolveEntity */
/* getEntity */
/* entityDecl */
/* notationDecl */
/* attributeDecl */
/* elementDecl */
/* unparsedEntityDecl */
/* setDocumentLocator */
/* startDocument */
/* endDocument */
/* startElement */
/* endElement */
/* reference */
/* characters */
/* ignorableWhitespace */
/* processingInstruction */
/* comment */
/* warning */
/* error */
/* fatalError */
/* getParameterEntity */
/* cdataBlock */
/* externalSubset */
/* initialized */
/* _private */
/* startElementNs */
/* endElementNs   */
/* serror */
};

xmlnode *xmlnode_from_str(const char *str,gssize size)
{
  struct _xmlnode_parser_data *xpd;
  xmlnode *ret;
  gsize real_size;
  do {
    if (str != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"str != NULL");
      return 0;
    };
  }while (0);
  real_size = ((size < 0)?strlen(str) : size);
  xpd = ((struct _xmlnode_parser_data *)(g_malloc0_n(1,(sizeof(struct _xmlnode_parser_data )))));
  if (xmlSAXUserParseMemory(&xmlnode_parser_libxml,xpd,str,real_size) < 0) {
    while(((xpd -> current) != 0) && (( *(xpd -> current)).parent != 0))
      xpd -> current = ( *(xpd -> current)).parent;
    if ((xpd -> current) != 0) 
      xmlnode_free((xpd -> current));
    xpd -> current = ((xmlnode *)((void *)0));
  }
  ret = (xpd -> current);
  if ((xpd -> error) != 0) {
    ret = ((xmlnode *)((void *)0));
    if ((xpd -> current) != 0) 
      xmlnode_free((xpd -> current));
  }
  g_free(xpd);
  return ret;
}

xmlnode *xmlnode_from_file(const char *dir,const char *filename,const char *description,const char *process)
{
  gchar *filename_full;
  GError *error = (GError *)((void *)0);
  gchar *contents = (gchar *)((void *)0);
  gsize length;
  xmlnode *node = (xmlnode *)((void *)0);
  do {
    if (dir != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"dir != NULL");
      return 0;
    };
  }while (0);
  purple_debug_info(process,"Reading file %s from directory %s\n",filename,dir);
  filename_full = g_build_filename(dir,filename,((void *)((void *)0)));
  if (!(g_file_test(filename_full,G_FILE_TEST_EXISTS) != 0)) {
    purple_debug_info(process,"File %s does not exist (this is not necessarily an error)\n",filename_full);
    g_free(filename_full);
    return 0;
  }
  if (!(g_file_get_contents(filename_full,&contents,&length,&error) != 0)) {
    purple_debug_error(process,"Error reading file %s: %s\n",filename_full,(error -> message));
    g_error_free(error);
  }
  if ((contents != ((gchar *)((void *)0))) && (length > 0)) {
    node = xmlnode_from_str(contents,length);
/* If we were unable to parse the file then save its contents to a backup file */
    if (node == ((xmlnode *)((void *)0))) {
      gchar *filename_temp;
      gchar *filename_temp_full;
      filename_temp = g_strdup_printf("%s~",filename);
      filename_temp_full = g_build_filename(dir,filename_temp,((void *)((void *)0)));
      purple_debug_error("util","Error parsing file %s.  Renaming old file to %s\n",filename_full,filename_temp);
      purple_util_write_data_to_file_absolute(filename_temp_full,contents,length);
      g_free(filename_temp_full);
      g_free(filename_temp);
    }
    g_free(contents);
  }
/* If we could not parse the file then show the user an error message */
  if (node == ((xmlnode *)((void *)0))) {
    gchar *title;
    gchar *msg;
    title = g_strdup_printf(((const char *)(dgettext("pidgin","Error Reading %s"))),filename);
    msg = g_strdup_printf(((const char *)(dgettext("pidgin","An error was encountered reading your %s.  The file has not been loaded, and the old file has been renamed to %s~."))),description,filename_full);
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,title,msg,0,0);
    g_free(title);
    g_free(msg);
  }
  g_free(filename_full);
  return node;
}

static void xmlnode_copy_foreach_ns(gpointer key,gpointer value,gpointer user_data)
{
  GHashTable *ret = (GHashTable *)user_data;
  g_hash_table_insert(ret,(g_strdup(key)),(g_strdup(value)));
}

xmlnode *xmlnode_copy(const xmlnode *src)
{
  xmlnode *ret;
  xmlnode *child;
  xmlnode *sibling = (xmlnode *)((void *)0);
  do {
    if (src != ((const xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"src != NULL");
      return 0;
    };
  }while (0);
  ret = new_node((src -> name),(src -> type));
  ret -> xmlns = g_strdup((src -> xmlns));
  if ((src -> data) != 0) {
    if ((src -> data_sz) != 0UL) {
      ret -> data = (g_memdup((src -> data),(src -> data_sz)));
      ret -> data_sz = (src -> data_sz);
    }
    else {
      ret -> data = g_strdup((src -> data));
    }
  }
  ret -> prefix = g_strdup((src -> prefix));
  if ((src -> namespace_map) != 0) {
    ret -> namespace_map = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
    g_hash_table_foreach((src -> namespace_map),xmlnode_copy_foreach_ns,(ret -> namespace_map));
  }
  for (child = (src -> child); child != 0; child = (child -> next)) {
    if (sibling != 0) {
      sibling -> next = xmlnode_copy(child);
      sibling = (sibling -> next);
    }
    else {
      ret -> child = xmlnode_copy(child);
      sibling = (ret -> child);
    }
    sibling -> parent = ret;
  }
  ret -> lastchild = sibling;
  return ret;
}

xmlnode *xmlnode_get_next_twin(xmlnode *node)
{
  xmlnode *sibling;
  const char *ns = xmlnode_get_namespace(node);
  do {
    if (node != ((xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((node -> type) == XMLNODE_TYPE_TAG) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node->type == XMLNODE_TYPE_TAG");
      return 0;
    };
  }while (0);
  for (sibling = (node -> next); sibling != 0; sibling = (sibling -> next)) {
/* XXX: Is it correct to ignore the namespace for the match if none was specified? */
    const char *xmlns = (const char *)((void *)0);
    if (ns != 0) 
      xmlns = xmlnode_get_namespace(sibling);
    if ((((sibling -> type) == XMLNODE_TYPE_TAG) && (purple_strequal((node -> name),(sibling -> name)) != 0)) && (purple_strequal(ns,xmlns) != 0)) 
      return sibling;
  }
  return 0;
}
