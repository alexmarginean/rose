/**
 * @file candidate.c Candidate for Media API
 * @ingroup core
 */
/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "candidate.h"
/** @copydoc _PurpleMediaCandidateClass */
typedef struct _PurpleMediaCandidateClass PurpleMediaCandidateClass;
/** @copydoc _PurpleMediaCandidatePrivate */
typedef struct _PurpleMediaCandidatePrivate PurpleMediaCandidatePrivate;
#define PURPLE_MEDIA_CANDIDATE_GET_PRIVATE(obj) \
		(G_TYPE_INSTANCE_GET_PRIVATE((obj), \
		PURPLE_TYPE_MEDIA_CANDIDATE, \
		PurpleMediaCandidatePrivate))

struct _PurpleMediaCandidateClass 
{
  GObjectClass parent_class;
}
;

struct _PurpleMediaCandidate 
{
  GObject parent;
}
;
static void purple_media_candidate_init(PurpleMediaCandidate *self);
static void purple_media_candidate_class_init(PurpleMediaCandidateClass *klass);
static gpointer purple_media_candidate_parent_class = (gpointer )((void *)0);
static gint PurpleMediaCandidate_private_offset;

static void purple_media_candidate_class_intern_init(gpointer klass)
{
  purple_media_candidate_parent_class = g_type_class_peek_parent(klass);
  if (PurpleMediaCandidate_private_offset != 0) 
    g_type_class_adjust_private_offset(klass,&PurpleMediaCandidate_private_offset);
  purple_media_candidate_class_init(((PurpleMediaCandidateClass *)klass));
}

inline static gpointer purple_media_candidate_get_instance_private(PurpleMediaCandidate *self)
{
  return (gpointer )(((guint8 *)self) + ((glong )PurpleMediaCandidate_private_offset));
}

GType purple_media_candidate_get_type()
{
  static volatile gsize g_define_type_id__volatile = 0;
  if ((({
    typedef char _GStaticAssertCompileTimeAssertion_50[1UL];
    (0?((gpointer )g_define_type_id__volatile) : 0);
    !(((
{
      typedef char _GStaticAssertCompileTimeAssertion_50[1UL];
      __sync_synchronize();
      (gpointer )g_define_type_id__volatile;
    })) != 0) && (g_once_init_enter((&g_define_type_id__volatile)) != 0);
  })) != 0) 
{
    GType g_define_type_id = g_type_register_static_simple(((GType )(20 << 2)),g_intern_static_string("PurpleMediaCandidate"),(sizeof(PurpleMediaCandidateClass )),((GClassInitFunc )purple_media_candidate_class_intern_init),(sizeof(PurpleMediaCandidate )),((GInstanceInitFunc )purple_media_candidate_init),((GTypeFlags )0));
{
{
{
        };
      }
    }
    (
{
      typedef char _GStaticAssertCompileTimeAssertion_50[1UL];
      0?(g_define_type_id__volatile = g_define_type_id) : 0;
      g_once_init_leave((&g_define_type_id__volatile),((gsize )g_define_type_id));
    });
  }
  return g_define_type_id__volatile;
}

struct _PurpleMediaCandidatePrivate 
{
  gchar *foundation;
  guint component_id;
  gchar *ip;
  guint16 port;
  gchar *base_ip;
  guint16 base_port;
  PurpleMediaNetworkProtocol proto;
  guint32 priority;
  PurpleMediaCandidateType type;
  gchar *username;
  gchar *password;
  guint ttl;
}
;
enum __unnamed_enum___F0_L68_C1_PROP_CANDIDATE_0__COMMA__PROP_FOUNDATION__COMMA__PROP_COMPONENT_ID__COMMA__PROP_IP__COMMA__PROP_PORT__COMMA__PROP_BASE_IP__COMMA__PROP_BASE_PORT__COMMA__PROP_PROTOCOL__COMMA__PROP_PRIORITY__COMMA__PROP_TYPE__COMMA__PROP_USERNAME__COMMA__PROP_PASSWORD__COMMA__PROP_TTL {PROP_CANDIDATE_0,PROP_FOUNDATION,PROP_COMPONENT_ID,PROP_IP,PROP_PORT,PROP_BASE_IP,PROP_BASE_PORT,PROP_PROTOCOL,PROP_PRIORITY,PROP_TYPE,PROP_USERNAME,PROP_PASSWORD,PROP_TTL};

static void purple_media_candidate_init(PurpleMediaCandidate *info)
{
  PurpleMediaCandidatePrivate *priv = (PurpleMediaCandidatePrivate *)(g_type_instance_get_private(((GTypeInstance *)info),purple_media_candidate_get_type()));
  priv -> foundation = ((gchar *)((void *)0));
  priv -> component_id = 0;
  priv -> ip = ((gchar *)((void *)0));
  priv -> port = 0;
  priv -> base_ip = ((gchar *)((void *)0));
  priv -> proto = PURPLE_MEDIA_NETWORK_PROTOCOL_UDP;
  priv -> priority = 0;
  priv -> type = PURPLE_MEDIA_CANDIDATE_TYPE_HOST;
  priv -> username = ((gchar *)((void *)0));
  priv -> password = ((gchar *)((void *)0));
  priv -> ttl = 0;
}

static void purple_media_candidate_finalize(GObject *info)
{
  PurpleMediaCandidatePrivate *priv = (PurpleMediaCandidatePrivate *)(g_type_instance_get_private(((GTypeInstance *)info),purple_media_candidate_get_type()));
  g_free((priv -> foundation));
  g_free((priv -> ip));
  g_free((priv -> base_ip));
  g_free((priv -> username));
  g_free((priv -> password));
}

static void purple_media_candidate_set_property(GObject *object,guint prop_id,const GValue *value,GParamSpec *pspec)
{
  PurpleMediaCandidatePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = purple_media_candidate_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CANDIDATE(object)");
      return ;
    };
  }while (0);
  priv = ((PurpleMediaCandidatePrivate *)(g_type_instance_get_private(((GTypeInstance *)object),purple_media_candidate_get_type())));
  switch(prop_id){
    case PROP_FOUNDATION:
{
      g_free((priv -> foundation));
      priv -> foundation = g_value_dup_string(value);
      break; 
    }
    case PROP_COMPONENT_ID:
{
      priv -> component_id = g_value_get_uint(value);
      break; 
    }
    case PROP_IP:
{
      g_free((priv -> ip));
      priv -> ip = g_value_dup_string(value);
      break; 
    }
    case PROP_PORT:
{
      priv -> port = (g_value_get_uint(value));
      break; 
    }
    case PROP_BASE_IP:
{
      g_free((priv -> base_ip));
      priv -> base_ip = g_value_dup_string(value);
      break; 
    }
    case PROP_BASE_PORT:
{
      priv -> base_port = (g_value_get_uint(value));
      break; 
    }
    case PROP_PROTOCOL:
{
      priv -> proto = (g_value_get_enum(value));
      break; 
    }
    case PROP_PRIORITY:
{
      priv -> priority = g_value_get_uint(value);
      break; 
    }
    case PROP_TYPE:
{
      priv -> type = (g_value_get_enum(value));
      break; 
    }
    case PROP_USERNAME:
{
      g_free((priv -> username));
      priv -> username = g_value_dup_string(value);
      break; 
    }
    case PROP_PASSWORD:
{
      g_free((priv -> password));
      priv -> password = g_value_dup_string(value);
      break; 
    }
    case PROP_TTL:
{
      priv -> ttl = g_value_get_uint(value);
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = prop_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","candidate.c:167","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void purple_media_candidate_get_property(GObject *object,guint prop_id,GValue *value,GParamSpec *pspec)
{
  PurpleMediaCandidatePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = purple_media_candidate_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CANDIDATE(object)");
      return ;
    };
  }while (0);
  priv = ((PurpleMediaCandidatePrivate *)(g_type_instance_get_private(((GTypeInstance *)object),purple_media_candidate_get_type())));
  switch(prop_id){
    case PROP_FOUNDATION:
{
      g_value_set_string(value,(priv -> foundation));
      break; 
    }
    case PROP_COMPONENT_ID:
{
      g_value_set_uint(value,(priv -> component_id));
      break; 
    }
    case PROP_IP:
{
      g_value_set_string(value,(priv -> ip));
      break; 
    }
    case PROP_PORT:
{
      g_value_set_uint(value,(priv -> port));
      break; 
    }
    case PROP_BASE_IP:
{
      g_value_set_string(value,(priv -> base_ip));
      break; 
    }
    case PROP_BASE_PORT:
{
      g_value_set_uint(value,(priv -> base_port));
      break; 
    }
    case PROP_PROTOCOL:
{
      g_value_set_enum(value,(priv -> proto));
      break; 
    }
    case PROP_PRIORITY:
{
      g_value_set_uint(value,(priv -> priority));
      break; 
    }
    case PROP_TYPE:
{
      g_value_set_enum(value,(priv -> type));
      break; 
    }
    case PROP_USERNAME:
{
      g_value_set_string(value,(priv -> username));
      break; 
    }
    case PROP_PASSWORD:
{
      g_value_set_string(value,(priv -> password));
      break; 
    }
    case PROP_TTL:
{
      g_value_set_uint(value,(priv -> ttl));
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = prop_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","candidate.c:220","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void purple_media_candidate_class_init(PurpleMediaCandidateClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *)klass;
  gobject_class -> finalize = purple_media_candidate_finalize;
  gobject_class -> set_property = purple_media_candidate_set_property;
  gobject_class -> get_property = purple_media_candidate_get_property;
  g_object_class_install_property(gobject_class,PROP_FOUNDATION,g_param_spec_string("foundation","Foundation","The foundation of the candidate.",0,(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(gobject_class,PROP_COMPONENT_ID,g_param_spec_uint("component-id","Component ID","The component id of the candidate.",0,(2147483647 * 2U + 1U),0,(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(gobject_class,PROP_IP,g_param_spec_string("ip","IP Address","The IP address of the candidate.",0,(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(gobject_class,PROP_PORT,g_param_spec_uint("port","Port","The port of the candidate.",0,((guint16 )0xffff),0,(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(gobject_class,PROP_BASE_IP,g_param_spec_string("base-ip","Base IP","The internal IP address of the candidate.",0,(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(gobject_class,PROP_BASE_PORT,g_param_spec_uint("base-port","Base Port","The internal port of the candidate.",0,((guint16 )0xffff),0,(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(gobject_class,PROP_PROTOCOL,g_param_spec_enum("protocol","Protocol","The protocol of the candidate.",purple_media_network_protocol_get_type(),PURPLE_MEDIA_NETWORK_PROTOCOL_UDP,(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(gobject_class,PROP_PRIORITY,g_param_spec_uint("priority","Priority","The priority of the candidate.",0,((guint32 )0xffffffff),0,(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(gobject_class,PROP_TYPE,g_param_spec_enum("type","Type","The type of the candidate.",purple_media_candidate_type_get_type(),PURPLE_MEDIA_CANDIDATE_TYPE_HOST,(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(gobject_class,PROP_USERNAME,g_param_spec_string("username","Username","The username used to connect to the candidate.",0,(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(gobject_class,PROP_PASSWORD,g_param_spec_string("password","Password","The password use to connect to the candidate.",0,(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(gobject_class,PROP_TTL,g_param_spec_uint("ttl","TTL","The TTL of the candidate.",0,(2147483647 * 2U + 1U),0,(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_type_class_add_private(klass,(sizeof(PurpleMediaCandidatePrivate )));
}

PurpleMediaCandidate *purple_media_candidate_new(const gchar *foundation,guint component_id,PurpleMediaCandidateType type,PurpleMediaNetworkProtocol proto,const gchar *ip,guint port)
{
  return (g_object_new(purple_media_candidate_get_type(),"foundation",foundation,"component-id",component_id,"type",type,"protocol",proto,"ip",ip,"port",port,((void *)((void *)0))));
}

PurpleMediaCandidate *purple_media_candidate_copy(PurpleMediaCandidate *candidate)
{
  PurpleMediaCandidatePrivate *priv;
  PurpleMediaCandidate *new_candidate;
  if (candidate == ((PurpleMediaCandidate *)((void *)0))) 
    return 0;
  priv = ((PurpleMediaCandidatePrivate *)(g_type_instance_get_private(((GTypeInstance *)candidate),purple_media_candidate_get_type())));
  new_candidate = purple_media_candidate_new((priv -> foundation),(priv -> component_id),(priv -> type),(priv -> proto),(priv -> ip),(priv -> port));
  g_object_set(new_candidate,"base-ip",(priv -> base_ip),"base-port",(priv -> base_port),"priority",(priv -> priority),"username",(priv -> username),"password",(priv -> password),"ttl",(priv -> ttl),((void *)((void *)0)));
  return new_candidate;
}

GList *purple_media_candidate_list_copy(GList *candidates)
{
  GList *new_list = (GList *)((void *)0);
  for (; candidates != 0; candidates = ((candidates != 0)?( *((GList *)candidates)).next : ((struct _GList *)((void *)0)))) {
    new_list = g_list_prepend(new_list,(purple_media_candidate_copy((candidates -> data))));
  }
  new_list = g_list_reverse(new_list);
  return new_list;
}

void purple_media_candidate_list_free(GList *candidates)
{
  for (; candidates != 0; candidates = g_list_delete_link(candidates,candidates)) {
    g_object_unref((candidates -> data));
  }
}

gchar *purple_media_candidate_get_foundation(PurpleMediaCandidate *candidate)
{
  gchar *foundation;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)candidate;
      GType __t = purple_media_candidate_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CANDIDATE(candidate)");
      return 0;
    };
  }while (0);
  g_object_get(candidate,"foundation",&foundation,((void *)((void *)0)));
  return foundation;
}

guint purple_media_candidate_get_component_id(PurpleMediaCandidate *candidate)
{
  guint component_id;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)candidate;
      GType __t = purple_media_candidate_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CANDIDATE(candidate)");
      return 0;
    };
  }while (0);
  g_object_get(candidate,"component-id",&component_id,((void *)((void *)0)));
  return component_id;
}

gchar *purple_media_candidate_get_ip(PurpleMediaCandidate *candidate)
{
  gchar *ip;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)candidate;
      GType __t = purple_media_candidate_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CANDIDATE(candidate)");
      return 0;
    };
  }while (0);
  g_object_get(candidate,"ip",&ip,((void *)((void *)0)));
  return ip;
}

guint16 purple_media_candidate_get_port(PurpleMediaCandidate *candidate)
{
  guint port;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)candidate;
      GType __t = purple_media_candidate_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CANDIDATE(candidate)");
      return 0;
    };
  }while (0);
  g_object_get(candidate,"port",&port,((void *)((void *)0)));
  return port;
}

gchar *purple_media_candidate_get_base_ip(PurpleMediaCandidate *candidate)
{
  gchar *base_ip;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)candidate;
      GType __t = purple_media_candidate_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CANDIDATE(candidate)");
      return 0;
    };
  }while (0);
  g_object_get(candidate,"base-ip",&base_ip,((void *)((void *)0)));
  return base_ip;
}

guint16 purple_media_candidate_get_base_port(PurpleMediaCandidate *candidate)
{
  guint base_port;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)candidate;
      GType __t = purple_media_candidate_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CANDIDATE(candidate)");
      return 0;
    };
  }while (0);
  g_object_get(candidate,"base_port",&base_port,((void *)((void *)0)));
  return base_port;
}

PurpleMediaNetworkProtocol purple_media_candidate_get_protocol(PurpleMediaCandidate *candidate)
{
  PurpleMediaNetworkProtocol protocol;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)candidate;
      GType __t = purple_media_candidate_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CANDIDATE(candidate)");
      return PURPLE_MEDIA_NETWORK_PROTOCOL_UDP;
    };
  }while (0);
  g_object_get(candidate,"protocol",&protocol,((void *)((void *)0)));
  return protocol;
}

guint32 purple_media_candidate_get_priority(PurpleMediaCandidate *candidate)
{
  guint priority;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)candidate;
      GType __t = purple_media_candidate_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CANDIDATE(candidate)");
      return 0;
    };
  }while (0);
  g_object_get(candidate,"priority",&priority,((void *)((void *)0)));
  return priority;
}

PurpleMediaCandidateType purple_media_candidate_get_candidate_type(PurpleMediaCandidate *candidate)
{
  PurpleMediaCandidateType type;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)candidate;
      GType __t = purple_media_candidate_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CANDIDATE(candidate)");
      return PURPLE_MEDIA_CANDIDATE_TYPE_HOST;
    };
  }while (0);
  g_object_get(candidate,"type",&type,((void *)((void *)0)));
  return type;
}

gchar *purple_media_candidate_get_username(PurpleMediaCandidate *candidate)
{
  gchar *username;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)candidate;
      GType __t = purple_media_candidate_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CANDIDATE(candidate)");
      return 0;
    };
  }while (0);
  g_object_get(candidate,"username",&username,((void *)((void *)0)));
  return username;
}

gchar *purple_media_candidate_get_password(PurpleMediaCandidate *candidate)
{
  gchar *password;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)candidate;
      GType __t = purple_media_candidate_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CANDIDATE(candidate)");
      return 0;
    };
  }while (0);
  g_object_get(candidate,"password",&password,((void *)((void *)0)));
  return password;
}

guint purple_media_candidate_get_ttl(PurpleMediaCandidate *candidate)
{
  guint ttl;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)candidate;
      GType __t = purple_media_candidate_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CANDIDATE(candidate)");
      return 0;
    };
  }while (0);
  g_object_get(candidate,"ttl",&ttl,((void *)((void *)0)));
  return ttl;
}
