/*
 * Sound Themes for libpurple
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "sound-theme.h"
#define PURPLE_SOUND_THEME_GET_PRIVATE(Gobject) \
	((PurpleSoundThemePrivate *) ((PURPLE_SOUND_THEME(Gobject))->priv))
/******************************************************************************
 * Structs
 *****************************************************************************/
typedef struct __unnamed_class___F0_L33_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L84R__Pe___variable_name_unknown_scope_and_name__scope__sound_files {
/* used to store filenames of diffrent sounds */
GHashTable *sound_files;}PurpleSoundThemePrivate;
/******************************************************************************
 * Globals
 *****************************************************************************/
static GObjectClass *parent_class = (GObjectClass *)((void *)0);
/******************************************************************************
 * Enums
 *****************************************************************************/
/******************************************************************************
 * GObject Stuff
 *****************************************************************************/

static void purple_sound_theme_init(GTypeInstance *instance,gpointer klass)
{
  PurpleSoundThemePrivate *priv;
  ( *((PurpleSoundTheme *)(g_type_check_instance_cast(((GTypeInstance *)instance),purple_sound_theme_get_type())))).priv = ((PurpleSoundThemePrivate *)(g_malloc0_n(1,(sizeof(PurpleSoundThemePrivate )))));
  priv = ((PurpleSoundThemePrivate *)( *((PurpleSoundTheme *)(g_type_check_instance_cast(((GTypeInstance *)instance),purple_sound_theme_get_type())))).priv);
  priv -> sound_files = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
}

static void purple_sound_theme_finalize(GObject *obj)
{
  PurpleSoundThemePrivate *priv;
  priv = ((PurpleSoundThemePrivate *)( *((PurpleSoundTheme *)(g_type_check_instance_cast(((GTypeInstance *)obj),purple_sound_theme_get_type())))).priv);
  g_hash_table_destroy((priv -> sound_files));
  ( *(parent_class -> finalize))(obj);
}

static void purple_sound_theme_class_init(PurpleSoundThemeClass *klass)
{
  GObjectClass *obj_class = (GObjectClass *)(g_type_check_class_cast(((GTypeClass *)klass),((GType )(20 << 2))));
  parent_class = (g_type_class_peek_parent(klass));
  obj_class -> finalize = purple_sound_theme_finalize;
}

GType purple_sound_theme_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(PurpleSoundThemeClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )purple_sound_theme_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(PurpleSoundTheme ))), (0), (purple_sound_theme_init), ((const GTypeValueTable *)((void *)0))
/* base_init */
/* base_finalize */
/* class_init */
/* class_finalize */
/* class_data */
/* n_preallocs */
/* instance_init */
/* value table */
};
    type = g_type_register_static(purple_theme_get_type(),"PurpleSoundTheme",&info,0);
  }
  return type;
}
/*****************************************************************************
 * Public API functions
 *****************************************************************************/

const gchar *purple_sound_theme_get_file(PurpleSoundTheme *theme,const gchar *event)
{
  PurpleSoundThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = purple_sound_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_SOUND_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PurpleSoundThemePrivate *)( *((PurpleSoundTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),purple_sound_theme_get_type())))).priv);
  return (g_hash_table_lookup((priv -> sound_files),event));
}

gchar *purple_sound_theme_get_file_full(PurpleSoundTheme *theme,const gchar *event)
{
  const gchar *filename;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = purple_sound_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_SOUND_THEME(theme)");
      return 0;
    };
  }while (0);
  filename = purple_sound_theme_get_file(theme,event);
  do {
    if (filename != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"filename");
      return 0;
    };
  }while (0);
  return g_build_filename(purple_theme_get_dir(((PurpleTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),purple_theme_get_type())))),filename,((void *)((void *)0)));
}

void purple_sound_theme_set_file(PurpleSoundTheme *theme,const gchar *event,const gchar *filename)
{
  PurpleSoundThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = purple_sound_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_SOUND_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PurpleSoundThemePrivate *)( *((PurpleSoundTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),purple_sound_theme_get_type())))).priv);
  if (filename != ((const gchar *)((void *)0))) 
    g_hash_table_replace((priv -> sound_files),(g_strdup(event)),(g_strdup(filename)));
  else 
    g_hash_table_remove((priv -> sound_files),event);
}
