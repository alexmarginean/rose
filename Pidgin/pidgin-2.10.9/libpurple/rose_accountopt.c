/**
 * @file accountopt.c Account Options API
 * @ingroup core
 */
/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "accountopt.h"
#include "util.h"

PurpleAccountOption *purple_account_option_new(PurplePrefType type,const char *text,const char *pref_name)
{
  PurpleAccountOption *option;
  do {
    if (type != PURPLE_PREF_NONE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type != PURPLE_PREF_NONE");
      return 0;
    };
  }while (0);
  do {
    if (text != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"text != NULL");
      return 0;
    };
  }while (0);
  do {
    if (pref_name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pref_name != NULL");
      return 0;
    };
  }while (0);
  option = ((PurpleAccountOption *)(g_malloc0_n(1,(sizeof(PurpleAccountOption )))));
  option -> type = type;
  option -> text = g_strdup(text);
  option -> pref_name = g_strdup(pref_name);
  return option;
}

PurpleAccountOption *purple_account_option_bool_new(const char *text,const char *pref_name,gboolean default_value)
{
  PurpleAccountOption *option;
  option = purple_account_option_new(PURPLE_PREF_BOOLEAN,text,pref_name);
  if (option == ((PurpleAccountOption *)((void *)0))) 
    return 0;
  option -> default_value.boolean = default_value;
  return option;
}

PurpleAccountOption *purple_account_option_int_new(const char *text,const char *pref_name,int default_value)
{
  PurpleAccountOption *option;
  option = purple_account_option_new(PURPLE_PREF_INT,text,pref_name);
  if (option == ((PurpleAccountOption *)((void *)0))) 
    return 0;
  option -> default_value.integer = default_value;
  return option;
}

PurpleAccountOption *purple_account_option_string_new(const char *text,const char *pref_name,const char *default_value)
{
  PurpleAccountOption *option;
  option = purple_account_option_new(PURPLE_PREF_STRING,text,pref_name);
  if (option == ((PurpleAccountOption *)((void *)0))) 
    return 0;
  option -> default_value.string = g_strdup(default_value);
  return option;
}

PurpleAccountOption *purple_account_option_list_new(const char *text,const char *pref_name,GList *list)
{
  PurpleAccountOption *option;
  option = purple_account_option_new(PURPLE_PREF_STRING_LIST,text,pref_name);
  if (option == ((PurpleAccountOption *)((void *)0))) 
    return 0;
  option -> default_value.list = list;
  return option;
}

static void purple_account_option_list_free(gpointer data,gpointer user_data)
{
  PurpleKeyValuePair *kvp = data;
  g_free((kvp -> value));
  g_free((kvp -> key));
  g_free(kvp);
}

void purple_account_option_destroy(PurpleAccountOption *option)
{
  do {
    if (option != ((PurpleAccountOption *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option != NULL");
      return ;
    };
  }while (0);
  g_free((option -> text));
  g_free((option -> pref_name));
  if ((option -> type) == PURPLE_PREF_STRING) {
    g_free(option -> default_value.string);
  }
  else if ((option -> type) == PURPLE_PREF_STRING_LIST) {
    if (option -> default_value.list != ((GList *)((void *)0))) {
      g_list_foreach(option -> default_value.list,purple_account_option_list_free,0);
      g_list_free(option -> default_value.list);
    }
  }
  g_free(option);
}

void purple_account_option_set_default_bool(PurpleAccountOption *option,gboolean value)
{
  do {
    if (option != ((PurpleAccountOption *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option != NULL");
      return ;
    };
  }while (0);
  do {
    if ((option -> type) == PURPLE_PREF_BOOLEAN) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option->type == PURPLE_PREF_BOOLEAN");
      return ;
    };
  }while (0);
  option -> default_value.boolean = value;
}

void purple_account_option_set_default_int(PurpleAccountOption *option,int value)
{
  do {
    if (option != ((PurpleAccountOption *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option != NULL");
      return ;
    };
  }while (0);
  do {
    if ((option -> type) == PURPLE_PREF_INT) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option->type == PURPLE_PREF_INT");
      return ;
    };
  }while (0);
  option -> default_value.integer = value;
}

void purple_account_option_set_default_string(PurpleAccountOption *option,const char *value)
{
  do {
    if (option != ((PurpleAccountOption *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option != NULL");
      return ;
    };
  }while (0);
  do {
    if ((option -> type) == PURPLE_PREF_STRING) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option->type == PURPLE_PREF_STRING");
      return ;
    };
  }while (0);
  g_free(option -> default_value.string);
  option -> default_value.string = g_strdup(value);
}

void purple_account_option_set_masked(PurpleAccountOption *option,gboolean masked)
{
  do {
    if (option != ((PurpleAccountOption *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option != NULL");
      return ;
    };
  }while (0);
  do {
    if ((option -> type) == PURPLE_PREF_STRING) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option->type == PURPLE_PREF_STRING");
      return ;
    };
  }while (0);
  option -> masked = masked;
}

void purple_account_option_set_list(PurpleAccountOption *option,GList *values)
{
  do {
    if (option != ((PurpleAccountOption *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option != NULL");
      return ;
    };
  }while (0);
  do {
    if ((option -> type) == PURPLE_PREF_STRING_LIST) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option->type == PURPLE_PREF_STRING_LIST");
      return ;
    };
  }while (0);
  if (option -> default_value.list != ((GList *)((void *)0))) {
    g_list_foreach(option -> default_value.list,purple_account_option_list_free,0);
    g_list_free(option -> default_value.list);
  }
  option -> default_value.list = values;
}

void purple_account_option_add_list_item(PurpleAccountOption *option,const char *key,const char *value)
{
  PurpleKeyValuePair *kvp;
  do {
    if (option != ((PurpleAccountOption *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option != NULL");
      return ;
    };
  }while (0);
  do {
    if (key != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"key != NULL");
      return ;
    };
  }while (0);
  do {
    if (value != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"value != NULL");
      return ;
    };
  }while (0);
  do {
    if ((option -> type) == PURPLE_PREF_STRING_LIST) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option->type == PURPLE_PREF_STRING_LIST");
      return ;
    };
  }while (0);
  kvp = ((PurpleKeyValuePair *)(g_malloc0_n(1,(sizeof(PurpleKeyValuePair )))));
  kvp -> key = g_strdup(key);
  kvp -> value = (g_strdup(value));
  option -> default_value.list = g_list_append(option -> default_value.list,kvp);
}

PurplePrefType purple_account_option_get_type(const PurpleAccountOption *option)
{
  do {
    if (option != ((const PurpleAccountOption *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option != NULL");
      return PURPLE_PREF_NONE;
    };
  }while (0);
  return option -> type;
}

const char *purple_account_option_get_text(const PurpleAccountOption *option)
{
  do {
    if (option != ((const PurpleAccountOption *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option != NULL");
      return 0;
    };
  }while (0);
  return (option -> text);
}

const char *purple_account_option_get_setting(const PurpleAccountOption *option)
{
  do {
    if (option != ((const PurpleAccountOption *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option != NULL");
      return 0;
    };
  }while (0);
  return (option -> pref_name);
}

gboolean purple_account_option_get_default_bool(const PurpleAccountOption *option)
{
  do {
    if (option != ((const PurpleAccountOption *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((option -> type) == PURPLE_PREF_BOOLEAN) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option->type == PURPLE_PREF_BOOLEAN");
      return 0;
    };
  }while (0);
  return option -> default_value.boolean;
}

int purple_account_option_get_default_int(const PurpleAccountOption *option)
{
  do {
    if (option != ((const PurpleAccountOption *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option != NULL");
      return -1;
    };
  }while (0);
  do {
    if ((option -> type) == PURPLE_PREF_INT) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option->type == PURPLE_PREF_INT");
      return -1;
    };
  }while (0);
  return option -> default_value.integer;
}

const char *purple_account_option_get_default_string(const PurpleAccountOption *option)
{
  do {
    if (option != ((const PurpleAccountOption *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((option -> type) == PURPLE_PREF_STRING) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option->type == PURPLE_PREF_STRING");
      return 0;
    };
  }while (0);
  return option -> default_value.string;
}

const char *purple_account_option_get_default_list_value(const PurpleAccountOption *option)
{
  PurpleKeyValuePair *kvp;
  do {
    if (option != ((const PurpleAccountOption *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((option -> type) == PURPLE_PREF_STRING_LIST) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option->type == PURPLE_PREF_STRING_LIST");
      return 0;
    };
  }while (0);
  if (option -> default_value.list == ((GList *)((void *)0))) 
    return 0;
  kvp = ( *option -> default_value.list).data;
  return ((kvp != 0)?(kvp -> value) : ((void *)((void *)0)));
}

gboolean purple_account_option_get_masked(const PurpleAccountOption *option)
{
  do {
    if (option != ((const PurpleAccountOption *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((option -> type) == PURPLE_PREF_STRING) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option->type == PURPLE_PREF_STRING");
      return 0;
    };
  }while (0);
  return option -> masked;
}

GList *purple_account_option_get_list(const PurpleAccountOption *option)
{
  do {
    if (option != ((const PurpleAccountOption *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((option -> type) == PURPLE_PREF_STRING_LIST) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"option->type == PURPLE_PREF_STRING_LIST");
      return 0;
    };
  }while (0);
  return option -> default_value.list;
}
/**************************************************************************
 * Account User Split API
 **************************************************************************/

PurpleAccountUserSplit *purple_account_user_split_new(const char *text,const char *default_value,char sep)
{
  PurpleAccountUserSplit *split;
  do {
    if (text != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"text != NULL");
      return 0;
    };
  }while (0);
  do {
    if (sep != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"sep != 0");
      return 0;
    };
  }while (0);
  split = ((PurpleAccountUserSplit *)(g_malloc0_n(1,(sizeof(PurpleAccountUserSplit )))));
  split -> text = g_strdup(text);
  split -> field_sep = sep;
  split -> default_value = g_strdup(default_value);
  split -> reverse = (!0);
  return split;
}

void purple_account_user_split_destroy(PurpleAccountUserSplit *split)
{
  do {
    if (split != ((PurpleAccountUserSplit *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"split != NULL");
      return ;
    };
  }while (0);
  g_free((split -> text));
  g_free((split -> default_value));
  g_free(split);
}

const char *purple_account_user_split_get_text(const PurpleAccountUserSplit *split)
{
  do {
    if (split != ((const PurpleAccountUserSplit *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"split != NULL");
      return 0;
    };
  }while (0);
  return (split -> text);
}

const char *purple_account_user_split_get_default_value(const PurpleAccountUserSplit *split)
{
  do {
    if (split != ((const PurpleAccountUserSplit *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"split != NULL");
      return 0;
    };
  }while (0);
  return (split -> default_value);
}

char purple_account_user_split_get_separator(const PurpleAccountUserSplit *split)
{
  do {
    if (split != ((const PurpleAccountUserSplit *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"split != NULL");
      return 0;
    };
  }while (0);
  return split -> field_sep;
}

gboolean purple_account_user_split_get_reverse(const PurpleAccountUserSplit *split)
{
  do {
    if (split != ((const PurpleAccountUserSplit *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"split != NULL");
      return 0;
    };
  }while (0);
  return split -> reverse;
}

void purple_account_user_split_set_reverse(PurpleAccountUserSplit *split,gboolean reverse)
{
  do {
    if (split != ((PurpleAccountUserSplit *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"split != NULL");
      return ;
    };
  }while (0);
  split -> reverse = reverse;
}
