/*
 * ThemeLoaders for libpurple
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "theme-loader.h"
#define PURPLE_THEME_LOADER_GET_PRIVATE(PurpleThemeLoader) \
	((PurpleThemeLoaderPrivate *) ((PurpleThemeLoader)->priv))
void purple_theme_loader_set_type_string(PurpleThemeLoader *loader,const gchar *type);
/******************************************************************************
 * Structs
 *****************************************************************************/
typedef struct __unnamed_class___F0_L34_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__type {
gchar *type;}PurpleThemeLoaderPrivate;
/******************************************************************************
 * Globals
 *****************************************************************************/
static GObjectClass *parent_class = (GObjectClass *)((void *)0);
/******************************************************************************
 * Enums
 *****************************************************************************/
enum __unnamed_enum___F0_L48_C1_PROP_ZERO__COMMA__PROP_TYPE {PROP_ZERO,PROP_TYPE};
/******************************************************************************
 * GObject Stuff                                                              *
 *****************************************************************************/

static void purple_theme_loader_get_property(GObject *obj,guint param_id,GValue *value,GParamSpec *psec)
{
  PurpleThemeLoader *theme_loader = (PurpleThemeLoader *)(g_type_check_instance_cast(((GTypeInstance *)obj),purple_theme_loader_get_type()));
  switch(param_id){
    case PROP_TYPE:
{
      g_value_set_string(value,purple_theme_loader_get_type_string(theme_loader));
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)obj;
        GParamSpec *_glib__pspec = (GParamSpec *)psec;
        guint _glib__property_id = param_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","theme-loader.c:68","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void purple_theme_loader_set_property(GObject *obj,guint param_id,const GValue *value,GParamSpec *psec)
{
  PurpleThemeLoader *loader = (PurpleThemeLoader *)(g_type_check_instance_cast(((GTypeInstance *)obj),purple_theme_loader_get_type()));
  switch(param_id){
    case PROP_TYPE:
{
      purple_theme_loader_set_type_string(loader,g_value_get_string(value));
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)obj;
        GParamSpec *_glib__pspec = (GParamSpec *)psec;
        guint _glib__property_id = param_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","theme-loader.c:84","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void purple_theme_loader_init(GTypeInstance *instance,gpointer klass)
{
  PurpleThemeLoader *loader = (PurpleThemeLoader *)(g_type_check_instance_cast(((GTypeInstance *)instance),purple_theme_loader_get_type()));
  loader -> priv = ((PurpleThemeLoaderPrivate *)(g_malloc0_n(1,(sizeof(PurpleThemeLoaderPrivate )))));
}

static void purple_theme_loader_finalize(GObject *obj)
{
  PurpleThemeLoader *loader = (PurpleThemeLoader *)(g_type_check_instance_cast(((GTypeInstance *)obj),purple_theme_loader_get_type()));
  PurpleThemeLoaderPrivate *priv = (PurpleThemeLoaderPrivate *)(loader -> priv);
  g_free((priv -> type));
  g_free(priv);
  ( *(parent_class -> finalize))(obj);
}

static void purple_theme_loader_class_init(PurpleThemeLoaderClass *klass)
{
  GObjectClass *obj_class = (GObjectClass *)(g_type_check_class_cast(((GTypeClass *)klass),((GType )(20 << 2))));
  GParamSpec *pspec;
  parent_class = (g_type_class_peek_parent(klass));
  obj_class -> get_property = purple_theme_loader_get_property;
  obj_class -> set_property = purple_theme_loader_set_property;
  obj_class -> finalize = purple_theme_loader_finalize;
/* TYPE STRING (read only) */
  pspec = g_param_spec_string("type","Type","The string representing the type of the theme",0,(G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));
  g_object_class_install_property(obj_class,PROP_TYPE,pspec);
}

GType purple_theme_loader_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(PurpleThemeLoaderClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )purple_theme_loader_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(PurpleThemeLoader ))), (0), (purple_theme_loader_init), ((const GTypeValueTable *)((void *)0))
/* base_init */
/* base_finalize */
/* class_init */
/* class_finalize */
/* class_data */
/* n_preallocs */
/* instance_init */
/* value table */
};
    type = g_type_register_static(((GType )(20 << 2)),"PurpleThemeLoader",&info,G_TYPE_FLAG_ABSTRACT);
  }
  return type;
}
/*****************************************************************************
 * Public API functions
 *****************************************************************************/

const gchar *purple_theme_loader_get_type_string(PurpleThemeLoader *theme_loader)
{
  PurpleThemeLoaderPrivate *priv = (PurpleThemeLoaderPrivate *)((void *)0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme_loader;
      GType __t = purple_theme_loader_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_THEME_LOADER(theme_loader)");
      return 0;
    };
  }while (0);
  priv = ((PurpleThemeLoaderPrivate *)(theme_loader -> priv));
  return (priv -> type);
}
/* < private > */

void purple_theme_loader_set_type_string(PurpleThemeLoader *loader,const gchar *type)
{
  PurpleThemeLoaderPrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)loader;
      GType __t = purple_theme_loader_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_THEME_LOADER(loader)");
      return ;
    };
  }while (0);
  priv = ((PurpleThemeLoaderPrivate *)(loader -> priv));
  g_free((priv -> type));
  priv -> type = g_strdup(type);
}

PurpleTheme *purple_theme_loader_build(PurpleThemeLoader *loader,const gchar *dir)
{
  return ( *( *((PurpleThemeLoaderClass *)( *((GTypeInstance *)loader)).g_class)).purple_theme_loader_build)(dir);
}
