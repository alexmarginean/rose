/**
 * @file core.c Purple Core API
 * @ingroup core
 */
/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "cipher.h"
#include "certificate.h"
#include "cmds.h"
#include "connection.h"
#include "conversation.h"
#include "core.h"
#include "debug.h"
#include "dnsquery.h"
#include "ft.h"
#include "idle.h"
#include "imgstore.h"
#include "network.h"
#include "notify.h"
#include "plugin.h"
#include "pounce.h"
#include "prefs.h"
#include "privacy.h"
#include "proxy.h"
#include "savedstatuses.h"
#include "signals.h"
#include "smiley.h"
#include "sound.h"
#include "sound-theme-loader.h"
#include "sslconn.h"
#include "status.h"
#include "stun.h"
#include "theme-manager.h"
#include "util.h"
#ifdef HAVE_DBUS
#  ifndef DBUS_API_SUBJECT_TO_CHANGE
#    define DBUS_API_SUBJECT_TO_CHANGE
#  endif
#  include <dbus/dbus.h>
#  include "dbus-purple.h"
#  include "dbus-server.h"
#  include "dbus-bindings.h"
#endif

struct PurpleCore 
{
  char *ui;
  void *reserved;
}
;
static PurpleCoreUiOps *_ops = (PurpleCoreUiOps *)((void *)0);
static PurpleCore *_core = (PurpleCore *)((void *)0);

static void static_proto_init()
{
}

gboolean purple_core_init(const char *ui)
{
  PurpleCoreUiOps *ops;
  PurpleCore *core;
  do {
    if (ui != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ui != NULL");
      return 0;
    };
  }while (0);
  do {
    if (purple_get_core() == ((PurpleCore *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_get_core() == NULL");
      return 0;
    };
  }while (0);
#ifdef ENABLE_NLS
  bindtextdomain("pidgin","/usr/local/share/locale");
#endif
#ifdef _WIN32
#endif
  g_type_init();
  _core = (core = ((PurpleCore *)(g_malloc0_n(1,(sizeof(PurpleCore ))))));
  core -> ui = g_strdup(ui);
  core -> reserved = ((void *)((void *)0));
  ops = purple_core_get_ui_ops();
/* The signals subsystem is important and should be first. */
  purple_signals_init();
  purple_util_init();
  purple_signal_register(core,"uri-handler",purple_marshal_BOOLEAN__POINTER_POINTER_POINTER,purple_value_new(PURPLE_TYPE_BOOLEAN),3,purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_BOXED,"GHashTable *"));
/* Protocol */
/* Command */
/* Parameters */
  purple_signal_register(core,"quitting",purple_marshal_VOID,0,0);
/* The prefs subsystem needs to be initialized before static protocols
	 * for protocol prefs to work. */
  purple_prefs_init();
  purple_debug_init();
  if (ops != ((PurpleCoreUiOps *)((void *)0))) {
    if ((ops -> ui_prefs_init) != ((void (*)())((void *)0))) 
      ( *(ops -> ui_prefs_init))();
    if ((ops -> debug_ui_init) != ((void (*)())((void *)0))) 
      ( *(ops -> debug_ui_init))();
  }
#ifdef HAVE_DBUS
  purple_dbus_init();
#endif
  purple_ciphers_init();
  purple_cmds_init();
/* Since plugins get probed so early we should probably initialize their
	 * subsystem right away too.
	 */
  purple_plugins_init();
/* Initialize all static protocols. */
  static_proto_init();
  purple_plugins_probe("so");
  purple_theme_manager_init();
/* The buddy icon code uses the imgstore, so init it early. */
  purple_imgstore_init();
/* Accounts use status, buddy icons and connection signals, so
	 * initialize these before accounts
	 */
  purple_status_init();
  purple_buddy_icons_init();
  purple_connections_init();
  purple_accounts_init();
  purple_savedstatuses_init();
  purple_notify_init();
  purple_certificate_init();
  purple_conversations_init();
  purple_blist_init();
  purple_log_init();
  purple_network_init();
  purple_privacy_init();
  purple_pounces_init();
  purple_proxy_init();
  purple_dnsquery_init();
  purple_sound_init();
  purple_ssl_init();
  purple_stun_init();
  purple_xfers_init();
  purple_idle_init();
  purple_smileys_init();
/*
	 * Call this early on to try to auto-detect our IP address and
	 * hopefully save some time later.
	 */
  purple_network_get_my_ip(-1);
  if ((ops != ((PurpleCoreUiOps *)((void *)0))) && ((ops -> ui_init) != ((void (*)())((void *)0)))) 
    ( *(ops -> ui_init))();
/* The UI may have registered some theme types, so refresh them */
  purple_theme_manager_refresh();
  return (!0);
}

void purple_core_quit()
{
  PurpleCoreUiOps *ops;
  PurpleCore *core = purple_get_core();
  do {
    if (core != ((PurpleCore *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"core != NULL");
      return ;
    };
  }while (0);
/* The self destruct sequence has been initiated */
  purple_signal_emit((purple_get_core()),"quitting");
/* Transmission ends */
  purple_connections_disconnect_all();
/*
	 * Certificates must be destroyed before the SSL plugins, because
	 * PurpleCertificates contain pointers to PurpleCertificateSchemes,
	 * and the PurpleCertificateSchemes will be unregistered when the
	 * SSL plugin is uninit.
	 */
  purple_certificate_uninit();
/* The SSL plugins must be uninit before they're unloaded */
  purple_ssl_uninit();
/* Unload all non-loader, non-prpl plugins before shutting down
	 * subsystems. */
  purple_debug_info("main","Unloading normal plugins\n");
  purple_plugins_unload(PURPLE_PLUGIN_STANDARD);
/* Save .xml files, remove signals, etc. */
  purple_smileys_uninit();
  purple_idle_uninit();
  purple_pounces_uninit();
  purple_blist_uninit();
  purple_ciphers_uninit();
  purple_notify_uninit();
  purple_conversations_uninit();
  purple_connections_uninit();
  purple_buddy_icons_uninit();
  purple_savedstatuses_uninit();
  purple_status_uninit();
  purple_accounts_uninit();
  purple_sound_uninit();
  purple_theme_manager_uninit();
  purple_xfers_uninit();
  purple_proxy_uninit();
  purple_dnsquery_uninit();
  purple_imgstore_uninit();
  purple_network_uninit();
/* Everything after unloading all plugins must not fail if prpls aren't
	 * around */
  purple_debug_info("main","Unloading all plugins\n");
  purple_plugins_destroy_all();
  ops = purple_core_get_ui_ops();
  if ((ops != ((PurpleCoreUiOps *)((void *)0))) && ((ops -> quit) != ((void (*)())((void *)0)))) 
    ( *(ops -> quit))();
/* Everything after prefs_uninit must not try to read any prefs */
  purple_prefs_uninit();
  purple_plugins_uninit();
#ifdef HAVE_DBUS
  purple_dbus_uninit();
#endif
  purple_cmds_uninit();
/* Everything after util_uninit cannot try to write things to the confdir */
  purple_util_uninit();
  purple_log_uninit();
  purple_signals_uninit();
  g_free((core -> ui));
  g_free(core);
#ifdef _WIN32
#endif
  _core = ((PurpleCore *)((void *)0));
}

gboolean purple_core_quit_cb(gpointer unused)
{
  purple_core_quit();
  return 0;
}

const char *purple_core_get_version()
{
  return "2.10.9";
}

const char *purple_core_get_ui()
{
  PurpleCore *core = purple_get_core();
  do {
    if (core != ((PurpleCore *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"core != NULL");
      return 0;
    };
  }while (0);
  return (core -> ui);
}

PurpleCore *purple_get_core()
{
  return _core;
}

void purple_core_set_ui_ops(PurpleCoreUiOps *ops)
{
  _ops = ops;
}

PurpleCoreUiOps *purple_core_get_ui_ops()
{
  return _ops;
}
#ifdef HAVE_DBUS

static char *purple_dbus_owner_user_dir()
{
  DBusMessage *msg = (DBusMessage *)((void *)0);
  DBusMessage *reply = (DBusMessage *)((void *)0);
  DBusConnection *dbus_connection = (DBusConnection *)((void *)0);
  DBusError dbus_error;
  char *remote_user_dir = (char *)((void *)0);
  if ((dbus_connection = purple_dbus_get_connection()) == ((DBusConnection *)((void *)0))) 
    return 0;
  if ((msg = dbus_message_new_method_call("im.pidgin.purple.PurpleService","/im/pidgin/purple/PurpleObject","im.pidgin.purple.PurpleInterface","PurpleUserDir")) == ((DBusMessage *)((void *)0))) 
    return 0;
  dbus_error_init(&dbus_error);
  reply = dbus_connection_send_with_reply_and_block(dbus_connection,msg,5000,&dbus_error);
  dbus_message_unref(msg);
  dbus_error_free(&dbus_error);
  if (reply != 0) {
    dbus_error_init(&dbus_error);
    dbus_message_get_args(reply,&dbus_error,((int )'s'),&remote_user_dir,((int )0));
    remote_user_dir = g_strdup(remote_user_dir);
    dbus_error_free(&dbus_error);
    dbus_message_unref(reply);
  }
  return remote_user_dir;
}
#endif /* HAVE_DBUS */

gboolean purple_core_ensure_single_instance()
{
  gboolean is_single_instance = (!0);
#ifdef HAVE_DBUS
/* in the future, other mechanisms might have already set this to FALSE */
  if (is_single_instance != 0) {
    if (!(purple_dbus_is_owner() != 0)) {
      const char *user_dir = purple_user_dir();
      char *dbus_owner_user_dir = purple_dbus_owner_user_dir();
      is_single_instance = !(purple_strequal(dbus_owner_user_dir,user_dir) != 0);
      g_free(dbus_owner_user_dir);
    }
  }
#endif /* HAVE_DBUS */
  return is_single_instance;
}

static gboolean move_and_symlink_dir(const char *path,const char *basename,const char *old_base,const char *new_base,const char *relative)
{
  char *new_name = g_build_filename(new_base,basename,((void *)((void *)0)));
#ifndef _WIN32
  char *old_name;
#endif
  if (rename(path,new_name) != 0) {
    purple_debug_error("core","Error renaming %s to %s: %s. Please report this at http://developer.pidgin.im/\n",path,new_name,g_strerror( *__errno_location()));
    g_free(new_name);
    return 0;
  }
  g_free(new_name);
#ifndef _WIN32
/* NOTE: This new_name is relative. */
  new_name = g_build_filename(relative,basename,((void *)((void *)0)));
  old_name = g_build_filename(old_base,basename,((void *)((void *)0)));
  if (symlink(new_name,old_name) != 0) {
    purple_debug_warning("core","Error symlinking %s to %s: %s. Please report this at http://developer.pidgin.im/\n",old_name,new_name,g_strerror( *__errno_location()));
  }
  g_free(old_name);
  g_free(new_name);
#endif
  return (!0);
}

gboolean purple_core_migrate()
{
  const char *user_dir = purple_user_dir();
  char *old_user_dir = g_strconcat(purple_home_dir(),"/.gaim",((void *)((void *)0)));
  char *status_file;
  FILE *fp;
  GDir *dir;
  GError *err;
  const char *entry;
#ifndef _WIN32
  char *logs_dir;
#endif
  char *old_icons_dir;
  if (!(g_file_test(old_user_dir,G_FILE_TEST_EXISTS) != 0)) {
/* ~/.gaim doesn't exist, so there's nothing to migrate. */
    g_free(old_user_dir);
    return (!0);
  }
  status_file = g_strconcat(user_dir,"/migrating",((void *)((void *)0)));
  if (g_file_test(user_dir,G_FILE_TEST_EXISTS) != 0) {
/* If we're here, we have both ~/.gaim and .purple. */
    if (!(g_file_test(status_file,G_FILE_TEST_EXISTS) != 0)) {
/* There's no "migrating" status file,
			 * so ~/.purple is all up to date. */
      g_free(status_file);
      g_free(old_user_dir);
      return (!0);
    }
  }
/* If we're here, it's time to migrate from ~/.gaim to ~/.purple. */
/* Ensure the user directory exists */
  if (!(g_file_test(user_dir,G_FILE_TEST_IS_DIR) != 0)) {
    if (mkdir(user_dir,(256 | 128 | 64)) == -1) {
      purple_debug_error("core","Error creating directory %s: %s. Please report this at http://developer.pidgin.im/\n",user_dir,g_strerror( *__errno_location()));
      g_free(status_file);
      g_free(old_user_dir);
      return 0;
    }
  }
/* This writes ~/.purple/migrating, which allows us to detect
	 * incomplete migrations and properly retry. */
  if (!((fp = fopen(status_file,"w")) != 0)) {
    purple_debug_error("core","Error opening file %s for writing: %s. Please report this at http://developer.pidgin.im/\n",status_file,g_strerror( *__errno_location()));
    g_free(status_file);
    g_free(old_user_dir);
    return 0;
  }
  fclose(fp);
/* Open ~/.gaim so we can loop over its contents. */
  err = ((GError *)((void *)0));
  if (!((dir = g_dir_open(old_user_dir,0,&err)) != 0)) {
    purple_debug_error("core","Error opening directory %s: %s. Please report this at http://developer.pidgin.im/\n",status_file,((err != 0)?(err -> message) : "Unknown error"));
    if (err != 0) 
      g_error_free(err);
    g_free(status_file);
    g_free(old_user_dir);
    return 0;
  }
/* Loop over the contents of ~/.gaim */
  while((entry = g_dir_read_name(dir)) != 0){{
      char *name = g_build_filename(old_user_dir,entry,((void *)((void *)0)));
#ifndef _WIN32
/* Deal with symlinks... */
      if (g_file_test(name,G_FILE_TEST_IS_SYMLINK) != 0) {
/* We're only going to duplicate a logs symlink. */
        if (purple_strequal(entry,"logs") != 0) {
          char *link;
          err = ((GError *)((void *)0));
          if ((link = g_file_read_link(name,&err)) == ((char *)((void *)0))) {
            char *name_utf8 = g_filename_to_utf8(name,(-1),0,0,0);
            purple_debug_error("core","Error reading symlink %s: %s. Please report this at http://developer.pidgin.im/\n",((name_utf8 != 0)?name_utf8 : name),(err -> message));
            g_free(name_utf8);
            g_error_free(err);
            g_free(name);
            g_dir_close(dir);
            g_free(status_file);
            g_free(old_user_dir);
            return 0;
          }
          logs_dir = g_build_filename(user_dir,"logs",((void *)((void *)0)));
          if ((purple_strequal(link,"../.purple/logs") != 0) || (purple_strequal(link,logs_dir) != 0)) {
/* If the symlink points to the new directory, we're
					 * likely just trying again after a failed migration,
					 * so there's no need to fail here. */
            g_free(link);
            g_free(logs_dir);
            continue; 
          }
/* In case we are trying again after a failed migration, we need
				 * to unlink any existing symlink.  If it's a directory, this
				 * will fail, and so will the symlink below, which is good
				 * because the user should sort things out. */
          g_unlink(logs_dir);
/* Relative links will most likely still be
				 * valid from ~/.purple, though it's not
				 * guaranteed.  Oh well. */
          if (symlink(link,logs_dir) != 0) {
            purple_debug_error("core","Error symlinking %s to %s: %s. Please report this at http://developer.pidgin.im/\n",logs_dir,link,g_strerror( *__errno_location()));
            g_free(link);
            g_free(name);
            g_free(logs_dir);
            g_dir_close(dir);
            g_free(status_file);
            g_free(old_user_dir);
            return 0;
          }
          g_free(link);
          g_free(logs_dir);
          continue; 
        }
/* Ignore all other symlinks. */
        continue; 
      }
#endif
/* Deal with directories... */
      if (g_file_test(name,G_FILE_TEST_IS_DIR) != 0) {
        if (purple_strequal(entry,"icons") != 0) {
/* This is a special case for the Album plugin, which
				 * stores data in the icons folder.  We're not copying
				 * the icons directory over because previous bugs
				 * meant that it filled up with junk for many users.
				 * This is a great time to purge it. */
          GDir *icons_dir;
          char *new_icons_dir;
          const char *icons_entry;
          err = ((GError *)((void *)0));
          if (!((icons_dir = g_dir_open(name,0,&err)) != 0)) {
            purple_debug_error("core","Error opening directory %s: %s. Please report this at http://developer.pidgin.im/\n",name,((err != 0)?(err -> message) : "Unknown error"));
            if (err != 0) 
              g_error_free(err);
            g_free(name);
            g_dir_close(dir);
            g_free(status_file);
            g_free(old_user_dir);
            return 0;
          }
          new_icons_dir = g_build_filename(user_dir,"icons",((void *)((void *)0)));
/* Ensure the new icon directory exists */
          if (!(g_file_test(new_icons_dir,G_FILE_TEST_IS_DIR) != 0)) {
            if (mkdir(new_icons_dir,(256 | 128 | 64)) == -1) {
              purple_debug_error("core","Error creating directory %s: %s. Please report this at http://developer.pidgin.im/\n",new_icons_dir,g_strerror( *__errno_location()));
              g_free(new_icons_dir);
              g_dir_close(icons_dir);
              g_free(name);
              g_dir_close(dir);
              g_free(status_file);
              g_free(old_user_dir);
              return 0;
            }
          }
          while((icons_entry = g_dir_read_name(icons_dir)) != 0){
            char *icons_name = g_build_filename(name,icons_entry,((void *)((void *)0)));
            if (g_file_test(icons_name,G_FILE_TEST_IS_DIR) != 0) {
              if (!(move_and_symlink_dir(icons_name,icons_entry,name,new_icons_dir,"../../.purple/icons") != 0)) {
                g_free(icons_name);
                g_free(new_icons_dir);
                g_dir_close(icons_dir);
                g_free(name);
                g_dir_close(dir);
                g_free(status_file);
                g_free(old_user_dir);
                return 0;
              }
            }
            g_free(icons_name);
          }
          g_dir_close(icons_dir);
        }
        else if (purple_strequal(entry,"plugins") != 0) {
/* Do nothing, because we broke plugin compatibility.
				 * This means that the plugins directory gets left behind. */
        }
        else {
/* All other directories are moved and symlinked. */
          if (!(move_and_symlink_dir(name,entry,old_user_dir,user_dir,"../.purple") != 0)) {
            g_free(name);
            g_dir_close(dir);
            g_free(status_file);
            g_free(old_user_dir);
            return 0;
          }
        }
      }
      else if (g_file_test(name,G_FILE_TEST_IS_REGULAR) != 0) {
/* Regular files are copied. */
        char *new_name;
        FILE *new_file;
        if (!((fp = fopen(name,"rb")) != 0)) {
          purple_debug_error("core","Error opening file %s for reading: %s. Please report this at http://developer.pidgin.im/\n",name,g_strerror( *__errno_location()));
          g_free(name);
          g_dir_close(dir);
          g_free(status_file);
          g_free(old_user_dir);
          return 0;
        }
        new_name = g_build_filename(user_dir,entry,((void *)((void *)0)));
        if (!((new_file = fopen(new_name,"wb")) != 0)) {
          purple_debug_error("core","Error opening file %s for writing: %s. Please report this at http://developer.pidgin.im/\n",new_name,g_strerror( *__errno_location()));
          fclose(fp);
          g_free(new_name);
          g_free(name);
          g_dir_close(dir);
          g_free(status_file);
          g_free(old_user_dir);
          return 0;
        }
        while(!(feof(fp) != 0)){
          unsigned char buf[256UL];
          size_t size;
          size = fread(buf,1,(sizeof(buf)),fp);
          if ((size != sizeof(buf)) && !(feof(fp) != 0)) {
            purple_debug_error("core","Error reading %s: %s. Please report this at http://developer.pidgin.im/\n",name,g_strerror( *__errno_location()));
            fclose(new_file);
            fclose(fp);
            g_free(new_name);
            g_free(name);
            g_dir_close(dir);
            g_free(status_file);
            g_free(old_user_dir);
            return 0;
          }
          if (!(fwrite(buf,size,1,new_file) != 0UL) && (ferror(new_file) != 0)) {
            purple_debug_error("core","Error writing %s: %s. Please report this at http://developer.pidgin.im/\n",new_name,g_strerror( *__errno_location()));
            fclose(new_file);
            fclose(fp);
            g_free(new_name);
            g_free(name);
            g_dir_close(dir);
            g_free(status_file);
            g_free(old_user_dir);
            return 0;
          }
        }
        if (fclose(new_file) != 0) {
          purple_debug_error("core","Error writing: %s: %s. Please report this at http://developer.pidgin.im/\n",new_name,g_strerror( *__errno_location()));
        }
        if (fclose(fp) != 0) {
          purple_debug_warning("core","Error closing %s: %s\n",name,g_strerror( *__errno_location()));
        }
        g_free(new_name);
      }
      else 
        purple_debug_warning("core","Not a regular file or directory: %s\n",name);
      g_free(name);
    }
  }
/* The migration was successful, so delete the status file. */
  if (g_unlink(status_file) != 0) {
    purple_debug_error("core","Error unlinking file %s: %s. Please report this at http://developer.pidgin.im/\n",status_file,g_strerror( *__errno_location()));
    g_free(status_file);
    return 0;
  }
  old_icons_dir = g_build_filename(old_user_dir,"icons",((void *)((void *)0)));
  _purple_buddy_icon_set_old_icons_dir(old_icons_dir);
  g_free(old_icons_dir);
  g_free(old_user_dir);
  g_free(status_file);
  return (!0);
}

GHashTable *purple_core_get_ui_info()
{
  PurpleCoreUiOps *ops = purple_core_get_ui_ops();
  if ((((PurpleCoreUiOps *)((void *)0)) == ops) || (((GHashTable *(*)())((void *)0)) == (ops -> get_ui_info))) 
    return 0;
  return ( *(ops -> get_ui_info))();
}
