/*
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
/* This file is the fullcrap */
#include "internal.h"
#include "blist.h"
#include "conversation.h"
#include "debug.h"
#include "log.h"
#include "notify.h"
#include "prefs.h"
#include "privacy.h"
#include "prpl.h"
#include "request.h"
#include "signals.h"
#include "server.h"
#include "status.h"
#include "util.h"
#define SECS_BEFORE_RESENDING_AUTORESPONSE 600
#define SEX_BEFORE_RESENDING_AUTORESPONSE "Only after you're married"

unsigned int serv_send_typing(PurpleConnection *gc,const char *name,PurpleTypingState state)
{
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  if (gc != 0) {
    prpl = purple_connection_get_prpl(gc);
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
    if ((prpl_info -> send_typing) != 0) 
      return ( *(prpl_info -> send_typing))(gc,name,state);
  }
  return 0;
}
static GSList *last_auto_responses = (GSList *)((void *)0);

struct last_auto_response 
{
  PurpleConnection *gc;
  char name[80UL];
  time_t sent;
}
;

static gboolean expire_last_auto_responses(gpointer data)
{
  GSList *tmp;
  GSList *cur;
  struct last_auto_response *lar;
  tmp = last_auto_responses;
  while(tmp != 0){
    cur = tmp;
    tmp = (tmp -> next);
    lar = ((struct last_auto_response *)(cur -> data));
    if ((time(0) - (lar -> sent)) > 600) {
      last_auto_responses = g_slist_remove(last_auto_responses,lar);
      g_free(lar);
    }
  }
/* do not run again */
  return 0;
}

static struct last_auto_response *get_last_auto_response(PurpleConnection *gc,const char *name)
{
  GSList *tmp;
  struct last_auto_response *lar;
/* because we're modifying or creating a lar, schedule the
	 * function to expire them as the pref dictates */
  purple_timeout_add_seconds((600 + 1),expire_last_auto_responses,0);
  tmp = last_auto_responses;
  while(tmp != 0){
    lar = ((struct last_auto_response *)(tmp -> data));
    if ((gc == (lar -> gc)) && !(strncmp(name,(lar -> name),(sizeof(lar -> name))) != 0)) 
      return lar;
    tmp = (tmp -> next);
  }
  lar = ((struct last_auto_response *)(g_malloc0_n(1,(sizeof(struct last_auto_response )))));
  g_snprintf((lar -> name),(sizeof(lar -> name)),"%s",name);
  lar -> gc = gc;
  lar -> sent = 0;
  last_auto_responses = g_slist_prepend(last_auto_responses,lar);
  return lar;
}

int serv_send_im(PurpleConnection *gc,const char *name,const char *message,PurpleMessageFlags flags)
{
  PurpleConversation *conv = (PurpleConversation *)((void *)0);
  PurpleAccount *account = (PurpleAccount *)((void *)0);
  PurplePresence *presence = (PurplePresence *)((void *)0);
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  int val = -22;
  const gchar *auto_reply_pref = (const gchar *)((void *)0);
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return val;
    };
  }while (0);
  prpl = purple_connection_get_prpl(gc);
  do {
    if (prpl != ((PurplePlugin *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"prpl != NULL");
      return val;
    };
  }while (0);
  prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  account = purple_connection_get_account(gc);
  presence = purple_account_get_presence(account);
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,name,account);
  if ((prpl_info -> send_im) != 0) 
    val = ( *(prpl_info -> send_im))(gc,name,message,flags);
/*
	 * XXX - If "only auto-reply when away & idle" is set, then shouldn't
	 * this only reset lar->sent if we're away AND idle?
	 */
  auto_reply_pref = purple_prefs_get_string("/purple/away/auto_reply");
  if (((((gc -> flags) & PURPLE_CONNECTION_AUTO_RESP) != 0U) && !(purple_presence_is_available(presence) != 0)) && !(purple_strequal(auto_reply_pref,"never") != 0)) {
    struct last_auto_response *lar;
    lar = get_last_auto_response(gc,name);
    lar -> sent = time(0);
  }
  if ((conv != 0) && (purple_conv_im_get_send_typed_timeout((purple_conversation_get_im_data(conv))) != 0U)) 
    purple_conv_im_stop_send_typed_timeout(purple_conversation_get_im_data(conv));
  return val;
}

void serv_get_info(PurpleConnection *gc,const char *name)
{
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  if (gc != 0) {
    prpl = purple_connection_get_prpl(gc);
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
    if ((prpl_info -> get_info) != 0) 
      ( *(prpl_info -> get_info))(gc,name);
  }
}

void serv_set_info(PurpleConnection *gc,const char *info)
{
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  PurpleAccount *account;
  if (gc != 0) {
    prpl = purple_connection_get_prpl(gc);
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
    if ((prpl_info -> set_info) != 0) {
      account = purple_connection_get_account(gc);
      if (purple_signal_emit_return_1(purple_accounts_get_handle(),"account-setting-info",account,info) != 0) 
        return ;
      ( *(prpl_info -> set_info))(gc,info);
      purple_signal_emit(purple_accounts_get_handle(),"account-set-info",account,info);
    }
  }
}
/*
 * Set buddy's alias on server roster/list
 */

void serv_alias_buddy(PurpleBuddy *b)
{
  PurpleAccount *account;
  PurpleConnection *gc;
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  if (b != 0) {
    account = purple_buddy_get_account(b);
    if (account != 0) {
      gc = purple_account_get_connection(account);
      if (gc != 0) {
        prpl = purple_connection_get_prpl(gc);
        prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
        if ((prpl_info -> alias_buddy) != 0) 
          ( *(prpl_info -> alias_buddy))(gc,purple_buddy_get_name(b),purple_buddy_get_local_buddy_alias(b));
      }
    }
  }
}

void serv_got_alias(PurpleConnection *gc,const char *who,const char *alias)
{
  PurpleAccount *account;
  GSList *buddies;
  PurpleBuddy *b;
  PurpleConversation *conv;
  account = purple_connection_get_account(gc);
  buddies = purple_find_buddies(account,who);
  while(buddies != ((GSList *)((void *)0))){{
      const char *server_alias;
      b = (buddies -> data);
      buddies = g_slist_delete_link(buddies,buddies);
      server_alias = purple_buddy_get_server_alias(b);
      if (purple_strequal(server_alias,alias) != 0) 
        continue; 
      purple_blist_server_alias_buddy(b,alias);
      conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,purple_buddy_get_name(b),account);
      if (((conv != ((PurpleConversation *)((void *)0))) && (alias != ((const char *)((void *)0)))) && !(purple_strequal(alias,who) != 0)) {
        char *escaped = g_markup_escape_text(who,(-1));
        char *escaped2 = g_markup_escape_text(alias,(-1));
        char *tmp = g_strdup_printf(((const char *)(dgettext("pidgin","%s is now known as %s.\n"))),escaped,escaped2);
        purple_conversation_write(conv,0,tmp,(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LINKIFY),time(0));
        g_free(tmp);
        g_free(escaped2);
        g_free(escaped);
      }
    }
  }
}

void purple_serv_got_private_alias(PurpleConnection *gc,const char *who,const char *alias)
{
  PurpleAccount *account = (PurpleAccount *)((void *)0);
  GSList *buddies = (GSList *)((void *)0);
  PurpleBuddy *b = (PurpleBuddy *)((void *)0);
  account = purple_connection_get_account(gc);
  buddies = purple_find_buddies(account,who);
  while(buddies != ((GSList *)((void *)0))){{
      const char *balias;
      b = (buddies -> data);
      buddies = g_slist_delete_link(buddies,buddies);
      balias = purple_buddy_get_local_buddy_alias(b);
      if (purple_strequal(balias,alias) != 0) 
        continue; 
      purple_blist_alias_buddy(b,alias);
    }
  }
}

PurpleAttentionType *purple_get_attention_type_from_code(PurpleAccount *account,guint type_code)
{
  PurplePlugin *prpl;
  PurpleAttentionType *attn;
  GList *(*get_attention_types)(PurpleAccount *);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  prpl = purple_find_prpl(purple_account_get_protocol_id(account));
/* Lookup the attention type in the protocol's attention_types list, if any. */
  get_attention_types = ( *((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info)).get_attention_types;
  if (get_attention_types != 0) {
    GList *attention_types;
    attention_types = ( *get_attention_types)(account);
    attn = ((PurpleAttentionType *)(g_list_nth_data(attention_types,type_code)));
  }
  else {
    attn = ((PurpleAttentionType *)((void *)0));
  }
  return attn;
}

void serv_send_attention(PurpleConnection *gc,const char *who,guint type_code)
{
  purple_prpl_send_attention(gc,who,type_code);
}

void serv_got_attention(PurpleConnection *gc,const char *who,guint type_code)
{
  purple_prpl_got_attention(gc,who,type_code);
}
/*
 * Move a buddy from one group to another on server.
 *
 * Note: For now we'll not deal with changing gc's at the same time, but
 * it should be possible.  Probably needs to be done, someday.  Although,
 * the UI for that would be difficult, because groups are Purple-wide.
 */

void serv_move_buddy(PurpleBuddy *b,PurpleGroup *og,PurpleGroup *ng)
{
  PurpleAccount *account;
  PurpleConnection *gc;
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  do {
    if (b != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"b != NULL");
      return ;
    };
  }while (0);
  do {
    if (og != ((PurpleGroup *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"og != NULL");
      return ;
    };
  }while (0);
  do {
    if (ng != ((PurpleGroup *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ng != NULL");
      return ;
    };
  }while (0);
  account = purple_buddy_get_account(b);
  gc = purple_account_get_connection(account);
  if (gc != 0) {
    prpl = purple_connection_get_prpl(gc);
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
    if ((prpl_info -> group_buddy) != 0) 
      ( *(prpl_info -> group_buddy))(gc,purple_buddy_get_name(b),purple_group_get_name(og),purple_group_get_name(ng));
  }
}

void serv_add_permit(PurpleConnection *gc,const char *name)
{
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  if (gc != 0) {
    prpl = purple_connection_get_prpl(gc);
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
    if ((prpl_info -> add_permit) != 0) 
      ( *(prpl_info -> add_permit))(gc,name);
  }
}

void serv_add_deny(PurpleConnection *gc,const char *name)
{
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  if (gc != 0) {
    prpl = purple_connection_get_prpl(gc);
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
    if ((prpl_info -> add_deny) != 0) 
      ( *(prpl_info -> add_deny))(gc,name);
  }
}

void serv_rem_permit(PurpleConnection *gc,const char *name)
{
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  if (gc != 0) {
    prpl = purple_connection_get_prpl(gc);
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
    if ((prpl_info -> rem_permit) != 0) 
      ( *(prpl_info -> rem_permit))(gc,name);
  }
}

void serv_rem_deny(PurpleConnection *gc,const char *name)
{
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  if (gc != 0) {
    prpl = purple_connection_get_prpl(gc);
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
    if ((prpl_info -> rem_deny) != 0) 
      ( *(prpl_info -> rem_deny))(gc,name);
  }
}

void serv_set_permit_deny(PurpleConnection *gc)
{
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  if (gc != 0) {
    prpl = purple_connection_get_prpl(gc);
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
/*
		 * this is called when either you import a buddy list, and make lots
		 * of changes that way, or when the user toggles the permit/deny mode
		 * in the prefs. In either case you should probably be resetting and
		 * resending the permit/deny info when you get this.
		 */
    if ((prpl_info -> set_permit_deny) != 0) 
      ( *(prpl_info -> set_permit_deny))(gc);
  }
}

void serv_join_chat(PurpleConnection *gc,GHashTable *data)
{
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  if (gc != 0) {
    prpl = purple_connection_get_prpl(gc);
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
    if ((prpl_info -> join_chat) != 0) 
      ( *(prpl_info -> join_chat))(gc,data);
  }
}

void serv_reject_chat(PurpleConnection *gc,GHashTable *data)
{
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  if (gc != 0) {
    prpl = purple_connection_get_prpl(gc);
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
    if ((prpl_info -> reject_chat) != 0) 
      ( *(prpl_info -> reject_chat))(gc,data);
  }
}

void serv_chat_invite(PurpleConnection *gc,int id,const char *message,const char *name)
{
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurpleConversation *conv;
  char *buffy = ((message != 0) && (( *message) != 0))?g_strdup(message) : ((char *)((void *)0));
  conv = purple_find_chat(gc,id);
  if (conv == ((PurpleConversation *)((void *)0))) 
    return ;
  if (gc != 0) 
    prpl = purple_connection_get_prpl(gc);
  if (prpl != 0) 
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  purple_signal_emit(purple_conversations_get_handle(),"chat-inviting-user",conv,name,&buffy);
  if ((prpl_info != 0) && ((prpl_info -> chat_invite) != 0)) 
    ( *(prpl_info -> chat_invite))(gc,id,buffy,name);
  purple_signal_emit(purple_conversations_get_handle(),"chat-invited-user",conv,name,buffy);
  g_free(buffy);
}
/* Ya know, nothing uses this except purple_conversation_destroy(),
 * I think I'll just merge it into that later...
 * Then again, something might want to use this, from outside prpl-land
 * to leave a chat without destroying the conversation.
 */

void serv_chat_leave(PurpleConnection *gc,int id)
{
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  prpl = purple_connection_get_prpl(gc);
  prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if ((prpl_info -> chat_leave) != 0) 
    ( *(prpl_info -> chat_leave))(gc,id);
}

void serv_chat_whisper(PurpleConnection *gc,int id,const char *who,const char *message)
{
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  if (gc != 0) {
    prpl = purple_connection_get_prpl(gc);
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
    if ((prpl_info -> chat_whisper) != 0) 
      ( *(prpl_info -> chat_whisper))(gc,id,who,message);
  }
}

int serv_chat_send(PurpleConnection *gc,int id,const char *message,PurpleMessageFlags flags)
{
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  prpl = purple_connection_get_prpl(gc);
  prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if ((prpl_info -> chat_send) != 0) 
    return ( *(prpl_info -> chat_send))(gc,id,message,flags);
  return -22;
}
/*
 * woo. i'm actually going to comment this function. isn't that fun. make
 * sure to follow along, kids
 */

void serv_got_im(PurpleConnection *gc,const char *who,const char *msg,PurpleMessageFlags flags,time_t mtime)
{
  PurpleAccount *account;
  PurpleConversation *conv;
  char *message;
  char *name;
  char *angel;
  char *buffy;
  int plugin_return;
  do {
    if (msg != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg != NULL");
      return ;
    };
  }while (0);
  account = purple_connection_get_account(gc);
  if (mtime < 0) {
    purple_debug_error("server","serv_got_im ignoring negative timestamp\n");
/* TODO: Would be more appropriate to use a value that indicates
		   that the timestamp is unknown, and surface that in the UI. */
    mtime = time(0);
  }
/*
	 * XXX: Should we be setting this here, or relying on prpls to set it?
	 */
  flags |= PURPLE_MESSAGE_RECV;
  if (!(purple_privacy_check(account,who) != 0)) {
    purple_signal_emit(purple_conversations_get_handle(),"blocked-im-msg",account,who,msg,flags,((unsigned int )mtime));
    return ;
  }
/*
	 * We should update the conversation window buttons and menu,
	 * if it exists.
	 */
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,who,(gc -> account));
/*
	 * Make copies of the message and the sender in case plugins want
	 * to free these strings and replace them with a modifed version.
	 */
  buffy = g_strdup(msg);
  angel = g_strdup(who);
  plugin_return = ((gint )((glong )(purple_signal_emit_return_1(purple_conversations_get_handle(),"receiving-im-msg",(gc -> account),&angel,&buffy,conv,&flags))));
  if ((!(buffy != 0) || !(angel != 0)) || (plugin_return != 0)) {
    g_free(buffy);
    g_free(angel);
    return ;
  }
  name = angel;
  message = buffy;
  purple_signal_emit(purple_conversations_get_handle(),"received-im-msg",(gc -> account),name,message,conv,flags);
/* search for conversation again in case it was created by received-im-msg handler */
  if (conv == ((PurpleConversation *)((void *)0))) 
    conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,name,(gc -> account));
  if (conv == ((PurpleConversation *)((void *)0))) 
    conv = purple_conversation_new(PURPLE_CONV_TYPE_IM,account,name);
  purple_conv_im_write(purple_conversation_get_im_data(conv),name,message,flags,mtime);
  g_free(message);
/*
	 * Don't autorespond if:
	 *
	 *  - it's not supported on this connection
	 *  - we are available
	 *  - or it's disabled
	 *  - or we're not idle and the 'only auto respond if idle' pref
	 *    is set
	 */
  if (((gc -> flags) & PURPLE_CONNECTION_AUTO_RESP) != 0U) {
    PurplePresence *presence;
    PurpleStatus *status;
    PurpleStatusType *status_type;
    PurpleStatusPrimitive primitive;
    const gchar *auto_reply_pref;
    const char *away_msg = (const char *)((void *)0);
    gboolean mobile = 0;
    auto_reply_pref = purple_prefs_get_string("/purple/away/auto_reply");
    presence = purple_account_get_presence(account);
    status = purple_presence_get_active_status(presence);
    status_type = purple_status_get_type(status);
    primitive = purple_status_type_get_primitive(status_type);
    mobile = purple_presence_is_status_primitive_active(presence,PURPLE_STATUS_MOBILE);
    if (((((primitive == PURPLE_STATUS_AVAILABLE) || (primitive == PURPLE_STATUS_INVISIBLE)) || (mobile != 0)) || (purple_strequal(auto_reply_pref,"never") != 0)) || (!(purple_presence_is_idle(presence) != 0) && (purple_strequal(auto_reply_pref,"awayidle") != 0))) {
      g_free(name);
      return ;
    }
    away_msg = purple_value_get_string((purple_status_get_attr_value(status,"message")));
    if ((away_msg != ((const char *)((void *)0))) && (( *away_msg) != 0)) {
      struct last_auto_response *lar;
      time_t now = time(0);
/*
			 * This used to be based on the conversation window. But um, if
			 * you went away, and someone sent you a message and got your
			 * auto-response, and then you closed the window, and then they
			 * sent you another one, they'd get the auto-response back too
			 * soon. Besides that, we need to keep track of this even if we've
			 * got a queue. So the rest of this block is just the auto-response,
			 * if necessary.
			 */
      lar = get_last_auto_response(gc,name);
      if ((now - (lar -> sent)) >= 600) {
/*
				 * We don't want to send an autoresponse in response to the other user's
				 * autoresponse.  We do, however, not want to then send one in response to the
				 * _next_ message, so we still set lar->sent to now.
				 */
        lar -> sent = now;
        if (!((flags & PURPLE_MESSAGE_AUTO_RESP) != 0U)) {
          serv_send_im(gc,name,away_msg,PURPLE_MESSAGE_AUTO_RESP);
          purple_conv_im_write(purple_conversation_get_im_data(conv),0,away_msg,(PURPLE_MESSAGE_SEND | PURPLE_MESSAGE_AUTO_RESP),mtime);
        }
      }
    }
  }
  g_free(name);
}

void serv_got_typing(PurpleConnection *gc,const char *name,int timeout,PurpleTypingState state)
{
  PurpleConversation *conv;
  PurpleConvIm *im = (PurpleConvIm *)((void *)0);
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,name,(gc -> account));
  if (conv != ((PurpleConversation *)((void *)0))) {
    im = purple_conversation_get_im_data(conv);
    purple_conv_im_set_typing_state(im,state);
  }
  else {
    switch(state){
      case PURPLE_TYPING:
{
        purple_signal_emit(purple_conversations_get_handle(),"buddy-typing",(gc -> account),name);
        break; 
      }
      case PURPLE_TYPED:
{
        purple_signal_emit(purple_conversations_get_handle(),"buddy-typed",(gc -> account),name);
        break; 
      }
      case PURPLE_NOT_TYPING:
{
        purple_signal_emit(purple_conversations_get_handle(),"buddy-typing-stopped",(gc -> account),name);
        break; 
      }
    }
  }
  if ((conv != ((PurpleConversation *)((void *)0))) && (timeout > 0)) 
    purple_conv_im_start_typing_timeout(im,timeout);
}

void serv_got_typing_stopped(PurpleConnection *gc,const char *name)
{
  PurpleConversation *conv;
  PurpleConvIm *im;
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,name,(gc -> account));
  if (conv != ((PurpleConversation *)((void *)0))) {
    im = purple_conversation_get_im_data(conv);
    if ((im -> typing_state) == PURPLE_NOT_TYPING) 
      return ;
    purple_conv_im_stop_typing_timeout(im);
    purple_conv_im_set_typing_state(im,PURPLE_NOT_TYPING);
  }
  else {
    purple_signal_emit(purple_conversations_get_handle(),"buddy-typing-stopped",(gc -> account),name);
  }
}

struct chat_invite_data 
{
  PurpleConnection *gc;
  GHashTable *components;
}
;

static void chat_invite_data_free(struct chat_invite_data *cid)
{
  if ((cid -> components) != 0) 
    g_hash_table_destroy((cid -> components));
  g_free(cid);
}

static void chat_invite_reject(struct chat_invite_data *cid)
{
  serv_reject_chat((cid -> gc),(cid -> components));
  chat_invite_data_free(cid);
}

static void chat_invite_accept(struct chat_invite_data *cid)
{
  serv_join_chat((cid -> gc),(cid -> components));
  chat_invite_data_free(cid);
}

void serv_got_chat_invite(PurpleConnection *gc,const char *name,const char *who,const char *message,GHashTable *data)
{
  PurpleAccount *account;
  char buf2[4096UL];
  struct chat_invite_data *cid;
  int plugin_return;
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return ;
    };
  }while (0);
  do {
    if (who != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"who != NULL");
      return ;
    };
  }while (0);
  account = purple_connection_get_account(gc);
  if (!(purple_privacy_check(account,who) != 0)) {
    purple_signal_emit(purple_conversations_get_handle(),"chat-invite-blocked",account,who,name,message,data);
    return ;
  }
  cid = ((struct chat_invite_data *)(g_malloc0_n(1,(sizeof(struct chat_invite_data )))));
  plugin_return = ((gint )((glong )(purple_signal_emit_return_1(purple_conversations_get_handle(),"chat-invited",account,who,name,message,data))));
  cid -> gc = gc;
  cid -> components = data;
  if (plugin_return == 0) {
    if (message != ((const char *)((void *)0))) {
      g_snprintf(buf2,(sizeof(buf2)),((const char *)(dgettext("pidgin","%s has invited %s to the chat room %s:\n%s"))),who,purple_account_get_username(account),name,message);
    }
    else 
      g_snprintf(buf2,(sizeof(buf2)),((const char *)(dgettext("pidgin","%s has invited %s to the chat room %s\n"))),who,purple_account_get_username(account),name);
    purple_request_action(gc,0,((const char *)(dgettext("pidgin","Accept chat invitation\?"))),buf2,-1,account,who,0,cid,2,((const char *)(dgettext("pidgin","_Accept"))),((GCallback )chat_invite_accept),((const char *)(dgettext("pidgin","_Cancel"))),((GCallback )chat_invite_reject));
  }
  else if (plugin_return > 0) 
    chat_invite_accept(cid);
  else 
    chat_invite_reject(cid);
}

PurpleConversation *serv_got_joined_chat(PurpleConnection *gc,int id,const char *name)
{
  PurpleConversation *conv;
  PurpleConvChat *chat;
  PurpleAccount *account;
  account = purple_connection_get_account(gc);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return 0;
    };
  }while (0);
  conv = purple_conversation_new(PURPLE_CONV_TYPE_CHAT,account,name);
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return 0;
    };
  }while (0);
  chat = purple_conversation_get_chat_data(conv);
  if (!(g_slist_find((gc -> buddy_chats),conv) != 0)) 
    gc -> buddy_chats = g_slist_append((gc -> buddy_chats),conv);
  purple_conv_chat_set_id(chat,id);
  purple_signal_emit(purple_conversations_get_handle(),"chat-joined",conv);
  return conv;
}

void serv_got_chat_left(PurpleConnection *g,int id)
{
  GSList *bcs;
  PurpleConversation *conv = (PurpleConversation *)((void *)0);
  PurpleConvChat *chat = (PurpleConvChat *)((void *)0);
{
    for (bcs = (g -> buddy_chats); bcs != ((GSList *)((void *)0)); bcs = (bcs -> next)) {
      conv = ((PurpleConversation *)(bcs -> data));
      chat = purple_conversation_get_chat_data(conv);
      if (purple_conv_chat_get_id(chat) == id) 
        break; 
      conv = ((PurpleConversation *)((void *)0));
    }
  }
  if (!(conv != 0)) 
    return ;
  purple_debug(PURPLE_DEBUG_INFO,"server","Leaving room: %s\n",purple_conversation_get_name(conv));
  g -> buddy_chats = g_slist_remove((g -> buddy_chats),conv);
  purple_conv_chat_left(purple_conversation_get_chat_data(conv));
  purple_signal_emit(purple_conversations_get_handle(),"chat-left",conv);
}

void purple_serv_got_join_chat_failed(PurpleConnection *gc,GHashTable *data)
{
  purple_signal_emit(purple_conversations_get_handle(),"chat-join-failed",gc,data);
}

void serv_got_chat_in(PurpleConnection *g,int id,const char *who,PurpleMessageFlags flags,const char *message,time_t mtime)
{
  GSList *bcs;
  PurpleConversation *conv = (PurpleConversation *)((void *)0);
  PurpleConvChat *chat = (PurpleConvChat *)((void *)0);
  char *buffy;
  char *angel;
  int plugin_return;
  do {
    if (who != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"who != NULL");
      return ;
    };
  }while (0);
  do {
    if (message != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"message != NULL");
      return ;
    };
  }while (0);
  if (mtime < 0) {
    purple_debug_error("server","serv_got_chat_in ignoring negative timestamp\n");
/* TODO: Would be more appropriate to use a value that indicates
		   that the timestamp is unknown, and surface that in the UI. */
    mtime = time(0);
  }
{
    for (bcs = (g -> buddy_chats); bcs != ((GSList *)((void *)0)); bcs = (bcs -> next)) {
      conv = ((PurpleConversation *)(bcs -> data));
      chat = purple_conversation_get_chat_data(conv);
      if (purple_conv_chat_get_id(chat) == id) 
        break; 
      conv = ((PurpleConversation *)((void *)0));
    }
  }
  if (!(conv != 0)) 
    return ;
/* Did I send the message? */
  if (purple_strequal(purple_conv_chat_get_nick(chat),purple_normalize((purple_conversation_get_account(conv)),who)) != 0) {
    flags |= PURPLE_MESSAGE_SEND;
/* Just in case some prpl sets it! */
    flags &= (~PURPLE_MESSAGE_RECV);
  }
  else {
    flags |= PURPLE_MESSAGE_RECV;
  }
/*
	 * Make copies of the message and the sender in case plugins want
	 * to free these strings and replace them with a modifed version.
	 */
  buffy = g_strdup(message);
  angel = g_strdup(who);
  plugin_return = ((gint )((glong )(purple_signal_emit_return_1(purple_conversations_get_handle(),"receiving-chat-msg",(g -> account),&angel,&buffy,conv,&flags))));
  if ((!(buffy != 0) || !(angel != 0)) || (plugin_return != 0)) {
    g_free(buffy);
    g_free(angel);
    return ;
  }
  who = angel;
  message = buffy;
  purple_signal_emit(purple_conversations_get_handle(),"received-chat-msg",(g -> account),who,message,conv,flags);
  purple_conv_chat_write(chat,who,message,flags,mtime);
  g_free(angel);
  g_free(buffy);
}

void serv_send_file(PurpleConnection *gc,const char *who,const char *file)
{
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  if (gc != 0) {
    prpl = purple_connection_get_prpl(gc);
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
    if (((prpl_info -> send_file) != 0) && (!((prpl_info -> can_receive_file) != 0) || (( *(prpl_info -> can_receive_file))(gc,who) != 0))) 
      ( *(prpl_info -> send_file))(gc,who,file);
  }
}
