/**
 * @file connection.c Connection API
 * @ingroup core
 */
/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#define _PURPLE_CONNECTION_C_
#include "internal.h"
#include "account.h"
#include "blist.h"
#include "connection.h"
#include "dbus-maybe.h"
#include "debug.h"
#include "log.h"
#include "notify.h"
#include "prefs.h"
#include "proxy.h"
#include "request.h"
#include "server.h"
#include "signals.h"
#include "util.h"
#define KEEPALIVE_INTERVAL 30
static GList *connections = (GList *)((void *)0);
static GList *connections_connecting = (GList *)((void *)0);
static PurpleConnectionUiOps *connection_ui_ops = (PurpleConnectionUiOps *)((void *)0);
static int connections_handle;

static gboolean send_keepalive(gpointer data)
{
  PurpleConnection *gc = data;
  PurplePluginProtocolInfo *prpl_info;
/* Only send keep-alives if we haven't heard from the
	 * server in a while.
	 */
  if ((time(0) - (gc -> last_received)) < 30) 
    return (!0);
  prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info);
  if ((prpl_info -> keepalive) != 0) 
    ( *(prpl_info -> keepalive))(gc);
  return (!0);
}

static void update_keepalive(PurpleConnection *gc,gboolean on)
{
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  if ((gc != ((PurpleConnection *)((void *)0))) && ((gc -> prpl) != ((PurplePlugin *)((void *)0)))) 
    prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info);
  if (!(prpl_info != 0) || !((prpl_info -> keepalive) != 0)) 
    return ;
  if ((on != 0) && !((gc -> keepalive) != 0U)) {
    purple_debug_info("connection","Activating keepalive.\n");
    gc -> keepalive = purple_timeout_add_seconds(30,send_keepalive,gc);
  }
  else if (!(on != 0) && ((gc -> keepalive) > 0)) {
    purple_debug_info("connection","Deactivating keepalive.\n");
    purple_timeout_remove((gc -> keepalive));
    gc -> keepalive = 0;
  }
}

void purple_connection_new(PurpleAccount *account,gboolean regist,const char *password)
{
  _purple_connection_new(account,regist,password);
}

void _purple_connection_new(PurpleAccount *account,gboolean regist,const char *password)
{
  PurpleConnection *gc;
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  if (!(purple_account_is_disconnected(account) != 0)) 
    return ;
  prpl = purple_find_prpl(purple_account_get_protocol_id(account));
  if (prpl != ((PurplePlugin *)((void *)0))) 
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  else {
    gchar *message;
    message = g_strdup_printf(((const char *)(dgettext("pidgin","Missing protocol plugin for %s"))),purple_account_get_username(account));
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((regist != 0)?((const char *)(dgettext("pidgin","Registration Error"))) : ((const char *)(dgettext("pidgin","Connection Error")))),message,0,0,0);
    g_free(message);
    return ;
  }
  if (regist != 0) {
    if ((prpl_info -> register_user) == ((void (*)(PurpleAccount *))((void *)0))) 
      return ;
  }
  else {
    if ((((password == ((const char *)((void *)0))) || (( *password) == 0)) && !(((prpl_info -> options) & OPT_PROTO_NO_PASSWORD) != 0U)) && !(((prpl_info -> options) & OPT_PROTO_PASSWORD_OPTIONAL) != 0U)) {
      purple_debug_error("connection","Cannot connect to account %s without a password.\n",purple_account_get_username(account));
      return ;
    }
  }
  gc = ((PurpleConnection *)(g_malloc0_n(1,(sizeof(PurpleConnection )))));
{
    PurpleConnection *typed_ptr = gc;
    purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurpleConnection);
  };
  gc -> prpl = prpl;
  if ((password != ((const char *)((void *)0))) && (( *password) != 0)) 
    gc -> password = g_strdup(password);
  purple_connection_set_account(gc,account);
  purple_connection_set_state(gc,PURPLE_CONNECTING);
  connections = g_list_append(connections,gc);
  purple_account_set_connection(account,gc);
  purple_signal_emit(purple_connections_get_handle(),"signing-on",gc);
  if (regist != 0) {
    purple_debug_info("connection","Registering.  gc = %p\n",gc);
/* set this so we don't auto-reconnect after registering */
    gc -> wants_to_die = (!0);
    ( *(prpl_info -> register_user))(account);
  }
  else {
    purple_debug_info("connection","Connecting. gc = %p\n",gc);
    purple_signal_emit(purple_accounts_get_handle(),"account-connecting",account);
    ( *(prpl_info -> login))(account);
  }
}

void purple_connection_new_unregister(PurpleAccount *account,const char *password,PurpleAccountUnregistrationCb cb,void *user_data)
{
  _purple_connection_new_unregister(account,password,cb,user_data);
}

void _purple_connection_new_unregister(PurpleAccount *account,const char *password,PurpleAccountUnregistrationCb cb,void *user_data)
{
/* Lots of copy/pasted code to avoid API changes. You might want to integrate that into the previous function when posssible. */
  PurpleConnection *gc;
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  prpl = purple_find_prpl(purple_account_get_protocol_id(account));
  if (prpl != ((PurplePlugin *)((void *)0))) 
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  else {
    gchar *message;
    message = g_strdup_printf(((const char *)(dgettext("pidgin","Missing protocol plugin for %s"))),purple_account_get_username(account));
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Unregistration Error"))),message,0,0,0);
    g_free(message);
    return ;
  }
  if (!(purple_account_is_disconnected(account) != 0)) {
    ( *(prpl_info -> unregister_user))(account,cb,user_data);
    return ;
  }
  if ((((password == ((const char *)((void *)0))) || (( *password) == 0)) && !(((prpl_info -> options) & OPT_PROTO_NO_PASSWORD) != 0U)) && !(((prpl_info -> options) & OPT_PROTO_PASSWORD_OPTIONAL) != 0U)) {
    purple_debug_error("connection","Cannot connect to account %s without a password.\n",purple_account_get_username(account));
    return ;
  }
  gc = ((PurpleConnection *)(g_malloc0_n(1,(sizeof(PurpleConnection )))));
{
    PurpleConnection *typed_ptr = gc;
    purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurpleConnection);
  };
  gc -> prpl = prpl;
  if ((password != ((const char *)((void *)0))) && (( *password) != 0)) 
    gc -> password = g_strdup(password);
  purple_connection_set_account(gc,account);
  purple_connection_set_state(gc,PURPLE_CONNECTING);
  connections = g_list_append(connections,gc);
  purple_account_set_connection(account,gc);
  purple_signal_emit(purple_connections_get_handle(),"signing-on",gc);
  purple_debug_info("connection","Unregistering.  gc = %p\n",gc);
  ( *(prpl_info -> unregister_user))(account,cb,user_data);
}

void purple_connection_destroy(PurpleConnection *gc)
{
  _purple_connection_destroy(gc);
}

void _purple_connection_destroy(PurpleConnection *gc)
{
  PurpleAccount *account;
  GSList *buddies;
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  gboolean remove = 0;
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  account = purple_connection_get_account(gc);
  purple_debug_info("connection","Disconnecting connection %p\n",gc);
  if ((purple_connection_get_state(gc)) != PURPLE_CONNECTING) 
    remove = (!0);
  purple_signal_emit(purple_connections_get_handle(),"signing-off",gc);
  while((gc -> buddy_chats) != 0){
    PurpleConversation *b = ( *(gc -> buddy_chats)).data;
    gc -> buddy_chats = g_slist_remove((gc -> buddy_chats),b);
    purple_conv_chat_left(purple_conversation_get_chat_data(b));
  }
  update_keepalive(gc,0);
  purple_proxy_connect_cancel_with_handle(gc);
  prpl_info = ((PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info);
  if ((prpl_info -> close) != 0) 
    ( *(prpl_info -> close))(gc);
/* Clear out the proto data that was freed in the prpl close method*/
  buddies = purple_find_buddies(account,0);
  while(buddies != ((GSList *)((void *)0))){
    PurpleBuddy *buddy = (buddies -> data);
    purple_buddy_set_protocol_data(buddy,0);
    buddies = g_slist_delete_link(buddies,buddies);
  }
  connections = g_list_remove(connections,gc);
  purple_connection_set_state(gc,PURPLE_DISCONNECTED);
  if (remove != 0) 
    purple_blist_remove_account(account);
  purple_signal_emit(purple_connections_get_handle(),"signed-off",gc);
  purple_account_request_close_with_account(account);
  purple_request_close_with_handle(gc);
  purple_notify_close_with_handle(gc);
  purple_debug_info("connection","Destroying connection %p\n",gc);
  purple_account_set_connection(account,0);
  g_free((gc -> password));
  g_free((gc -> display_name));
  if ((gc -> disconnect_timeout) > 0) 
    purple_timeout_remove((gc -> disconnect_timeout));
  purple_dbus_unregister_pointer(gc);
  g_free(gc);
}
/*
 * d:)->-<
 *
 * d:O-\-<
 *
 * d:D-/-<
 *
 * d8D->-< DANCE!
 */

void purple_connection_set_state(PurpleConnection *gc,PurpleConnectionState state)
{
  PurpleConnectionUiOps *ops;
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  if ((gc -> state) == state) 
    return ;
  gc -> state = state;
  ops = purple_connections_get_ui_ops();
  if ((gc -> state) == PURPLE_CONNECTING) {
    connections_connecting = g_list_append(connections_connecting,gc);
  }
  else {
    connections_connecting = g_list_remove(connections_connecting,gc);
  }
  if ((gc -> state) == PURPLE_CONNECTED) {
    PurpleAccount *account;
    PurplePresence *presence;
    account = purple_connection_get_account(gc);
    presence = purple_account_get_presence(account);
/* Set the time the account came online */
    purple_presence_set_login_time(presence,time(0));
    if (purple_prefs_get_bool("/purple/logging/log_system") != 0) {
      PurpleLog *log = purple_account_get_log(account,(!0));
      if (log != ((PurpleLog *)((void *)0))) {
        char *msg = g_strdup_printf(((const char *)(dgettext("pidgin","+++ %s signed on"))),purple_account_get_username(account));
        purple_log_write(log,PURPLE_MESSAGE_SYSTEM,purple_account_get_username(account),purple_presence_get_login_time(presence),msg);
        g_free(msg);
      }
    }
    if ((ops != ((PurpleConnectionUiOps *)((void *)0))) && ((ops -> connected) != ((void (*)(PurpleConnection *))((void *)0)))) 
      ( *(ops -> connected))(gc);
    purple_blist_add_account(account);
    purple_signal_emit(purple_connections_get_handle(),"signed-on",gc);
    purple_signal_emit_return_1(purple_connections_get_handle(),"autojoin",gc);
    serv_set_permit_deny(gc);
    update_keepalive(gc,(!0));
  }
  else if ((gc -> state) == PURPLE_DISCONNECTED) {
    PurpleAccount *account = purple_connection_get_account(gc);
    if (purple_prefs_get_bool("/purple/logging/log_system") != 0) {
      PurpleLog *log = purple_account_get_log(account,0);
      if (log != ((PurpleLog *)((void *)0))) {
        char *msg = g_strdup_printf(((const char *)(dgettext("pidgin","+++ %s signed off"))),purple_account_get_username(account));
        purple_log_write(log,PURPLE_MESSAGE_SYSTEM,purple_account_get_username(account),time(0),msg);
        g_free(msg);
      }
    }
    purple_account_destroy_log(account);
    if ((ops != ((PurpleConnectionUiOps *)((void *)0))) && ((ops -> disconnected) != ((void (*)(PurpleConnection *))((void *)0)))) 
      ( *(ops -> disconnected))(gc);
  }
}

void purple_connection_set_account(PurpleConnection *gc,PurpleAccount *account)
{
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  gc -> account = account;
}

void purple_connection_set_display_name(PurpleConnection *gc,const char *name)
{
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  g_free((gc -> display_name));
  gc -> display_name = g_strdup(name);
}

void purple_connection_set_protocol_data(PurpleConnection *connection,void *proto_data)
{
  do {
    if (connection != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"connection != NULL");
      return ;
    };
  }while (0);
  connection -> proto_data = proto_data;
}

PurpleConnectionState purple_connection_get_state(const PurpleConnection *gc)
{
  do {
    if (gc != ((const PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return PURPLE_DISCONNECTED;
    };
  }while (0);
  return gc -> state;
}

PurpleAccount *purple_connection_get_account(const PurpleConnection *gc)
{
  do {
    if (gc != ((const PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return 0;
    };
  }while (0);
  return gc -> account;
}

PurplePlugin *purple_connection_get_prpl(const PurpleConnection *gc)
{
  do {
    if (gc != ((const PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return 0;
    };
  }while (0);
  return gc -> prpl;
}

const char *purple_connection_get_password(const PurpleConnection *gc)
{
  do {
    if (gc != ((const PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return 0;
    };
  }while (0);
  return (((gc -> password) != 0)?(gc -> password) : ( *(gc -> account)).password);
}

const char *purple_connection_get_display_name(const PurpleConnection *gc)
{
  do {
    if (gc != ((const PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return 0;
    };
  }while (0);
  return (gc -> display_name);
}

void *purple_connection_get_protocol_data(const PurpleConnection *connection)
{
  do {
    if (connection != ((const PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"connection != NULL");
      return 0;
    };
  }while (0);
  return connection -> proto_data;
}

void purple_connection_update_progress(PurpleConnection *gc,const char *text,size_t step,size_t count)
{
  PurpleConnectionUiOps *ops;
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  do {
    if (text != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"text != NULL");
      return ;
    };
  }while (0);
  do {
    if (step < count) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"step < count");
      return ;
    };
  }while (0);
  do {
    if (count > 1) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"count > 1");
      return ;
    };
  }while (0);
  ops = purple_connections_get_ui_ops();
  if ((ops != ((PurpleConnectionUiOps *)((void *)0))) && ((ops -> connect_progress) != ((void (*)(PurpleConnection *, const char *, size_t , size_t ))((void *)0)))) 
    ( *(ops -> connect_progress))(gc,text,step,count);
}

void purple_connection_notice(PurpleConnection *gc,const char *text)
{
  PurpleConnectionUiOps *ops;
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  do {
    if (text != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"text != NULL");
      return ;
    };
  }while (0);
  ops = purple_connections_get_ui_ops();
  if ((ops != ((PurpleConnectionUiOps *)((void *)0))) && ((ops -> notice) != ((void (*)(PurpleConnection *, const char *))((void *)0)))) 
    ( *(ops -> notice))(gc,text);
}

static gboolean purple_connection_disconnect_cb(gpointer data)
{
  PurpleAccount *account;
  PurpleConnection *gc;
  char *password;
  account = data;
  gc = purple_account_get_connection(account);
  if (gc != ((PurpleConnection *)((void *)0))) 
    gc -> disconnect_timeout = 0;
  password = g_strdup(purple_account_get_password(account));
  purple_account_disconnect(account);
  purple_account_set_password(account,password);
  g_free(password);
  return 0;
}

void purple_connection_error(PurpleConnection *gc,const char *text)
{
/* prpls that have not been updated to use disconnection reasons will
	 * be setting wants_to_die before calling this function, so choose
	 * PURPLE_CONNECTION_ERROR_OTHER_ERROR (which is fatal) if it's true,
	 * and PURPLE_CONNECTION_ERROR_NETWORK_ERROR (which isn't) if not.  See
	 * the documentation in connection.h.
	 */
  PurpleConnectionError reason = (((gc -> wants_to_die) != 0)?PURPLE_CONNECTION_ERROR_OTHER_ERROR : PURPLE_CONNECTION_ERROR_NETWORK_ERROR);
  purple_connection_error_reason(gc,reason,text);
}

void purple_connection_error_reason(PurpleConnection *gc,PurpleConnectionError reason,const char *description)
{
  PurpleConnectionUiOps *ops;
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
/* This sanity check relies on PURPLE_CONNECTION_ERROR_OTHER_ERROR
	 * being the last member of the PurpleConnectionError enum in
	 * connection.h; if other reasons are added after it, this check should
	 * be updated.
	 */
  if (reason > PURPLE_CONNECTION_ERROR_OTHER_ERROR) {
    purple_debug_error("connection","purple_connection_error_reason: reason %u isn\'t a valid reason\n",reason);
    reason = PURPLE_CONNECTION_ERROR_OTHER_ERROR;
  }
  if (description == ((const char *)((void *)0))) {
    purple_debug_error("connection","purple_connection_error_reason called with NULL description\n");
    description = ((const char *)(dgettext("pidgin","Unknown error")));
  }
/* If we've already got one error, we don't need any more */
  if ((gc -> disconnect_timeout) > 0) 
    return ;
  gc -> wants_to_die = purple_connection_error_is_fatal(reason);
  purple_debug_info("connection","Connection error on %p (reason: %u description: %s)\n",gc,reason,description);
  ops = purple_connections_get_ui_ops();
  if (ops != ((PurpleConnectionUiOps *)((void *)0))) {
    if ((ops -> report_disconnect_reason) != ((void (*)(PurpleConnection *, PurpleConnectionError , const char *))((void *)0))) 
      ( *(ops -> report_disconnect_reason))(gc,reason,description);
    if ((ops -> report_disconnect) != ((void (*)(PurpleConnection *, const char *))((void *)0))) 
      ( *(ops -> report_disconnect))(gc,description);
  }
  purple_signal_emit(purple_connections_get_handle(),"connection-error",gc,reason,description);
  gc -> disconnect_timeout = purple_timeout_add(0,purple_connection_disconnect_cb,(purple_connection_get_account(gc)));
}

void purple_connection_ssl_error(PurpleConnection *gc,PurpleSslErrorType ssl_error)
{
  PurpleConnectionError reason;
  switch(ssl_error){
    case PURPLE_SSL_HANDSHAKE_FAILED:
{
      reason = PURPLE_CONNECTION_ERROR_ENCRYPTION_ERROR;
      break; 
    }
    case PURPLE_SSL_CONNECT_FAILED:
{
      reason = PURPLE_CONNECTION_ERROR_NETWORK_ERROR;
      break; 
    }
    case PURPLE_SSL_CERTIFICATE_INVALID:
{
/* TODO: maybe PURPLE_SSL_* should be more specific? */
      reason = PURPLE_CONNECTION_ERROR_CERT_OTHER_ERROR;
      break; 
    }
    default:
{
      do {
        g_assertion_message_expr(0,"connection.c",614,((const char *)__func__),0);
      }while (0);
      reason = PURPLE_CONNECTION_ERROR_CERT_OTHER_ERROR;
    }
  }
  purple_connection_error_reason(gc,reason,purple_ssl_strerror(ssl_error));
}

gboolean purple_connection_error_is_fatal(PurpleConnectionError reason)
{
  switch(reason){
    case PURPLE_CONNECTION_ERROR_NETWORK_ERROR:
{
    }
    case PURPLE_CONNECTION_ERROR_ENCRYPTION_ERROR:
{
      return 0;
    }
    case PURPLE_CONNECTION_ERROR_INVALID_USERNAME:
{
    }
    case PURPLE_CONNECTION_ERROR_AUTHENTICATION_FAILED:
{
    }
    case PURPLE_CONNECTION_ERROR_AUTHENTICATION_IMPOSSIBLE:
{
    }
    case PURPLE_CONNECTION_ERROR_NO_SSL_SUPPORT:
{
    }
    case PURPLE_CONNECTION_ERROR_NAME_IN_USE:
{
    }
    case PURPLE_CONNECTION_ERROR_INVALID_SETTINGS:
{
    }
    case PURPLE_CONNECTION_ERROR_CERT_NOT_PROVIDED:
{
    }
    case PURPLE_CONNECTION_ERROR_CERT_UNTRUSTED:
{
    }
    case PURPLE_CONNECTION_ERROR_CERT_EXPIRED:
{
    }
    case PURPLE_CONNECTION_ERROR_CERT_NOT_ACTIVATED:
{
    }
    case PURPLE_CONNECTION_ERROR_CERT_HOSTNAME_MISMATCH:
{
    }
    case PURPLE_CONNECTION_ERROR_CERT_FINGERPRINT_MISMATCH:
{
    }
    case PURPLE_CONNECTION_ERROR_CERT_SELF_SIGNED:
{
    }
    case PURPLE_CONNECTION_ERROR_CERT_OTHER_ERROR:
{
    }
    case PURPLE_CONNECTION_ERROR_OTHER_ERROR:
{
      return (!0);
    }
    default:
{
      do {
        g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","connection.c",647,((const char *)__func__));
        return (!0);
      }while (0);
    }
  }
}

void purple_connections_disconnect_all()
{
  GList *l;
  PurpleConnection *gc;
  while((l = purple_connections_get_all()) != ((GList *)((void *)0))){
    gc = (l -> data);
    gc -> wants_to_die = (!0);
    purple_account_disconnect((gc -> account));
  }
}

GList *purple_connections_get_all()
{
  return connections;
}

GList *purple_connections_get_connecting()
{
  return connections_connecting;
}

void purple_connections_set_ui_ops(PurpleConnectionUiOps *ops)
{
  connection_ui_ops = ops;
}

PurpleConnectionUiOps *purple_connections_get_ui_ops()
{
  return connection_ui_ops;
}

void purple_connections_init()
{
  void *handle = purple_connections_get_handle();
  purple_signal_register(handle,"signing-on",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONNECTION));
  purple_signal_register(handle,"signed-on",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONNECTION));
  purple_signal_register(handle,"signing-off",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONNECTION));
  purple_signal_register(handle,"signed-off",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONNECTION));
  purple_signal_register(handle,"connection-error",purple_marshal_VOID__POINTER_INT_POINTER,0,3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONNECTION),purple_value_new(PURPLE_TYPE_ENUM),purple_value_new(PURPLE_TYPE_STRING));
  purple_signal_register(handle,"autojoin",purple_marshal_BOOLEAN__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONNECTION));
}

void purple_connections_uninit()
{
  purple_signals_unregister_by_instance(purple_connections_get_handle());
}

void *purple_connections_get_handle()
{
  return (&connections_handle);
}
