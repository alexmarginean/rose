/**
 * @file smiley.c Simley API
 * @ingroup core
 */
/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "dbus-maybe.h"
#include "debug.h"
#include "imgstore.h"
#include "smiley.h"
#include "util.h"
#include "xmlnode.h"
/**************************************************************************/
/* Main structures, members and constants                                 */
/**************************************************************************/

struct _PurpleSmiley 
{
  GObject parent;
/**< The id of the stored image with the
	                                    the smiley data.        */
  PurpleStoredImage *img;
/**< Shortcut associated with the custom
	                                    smiley. This field will work as a
	                                    unique key by this API. */
  char *shortcut;
/**< The smiley checksum.        */
  char *checksum;
}
;

struct _PurpleSmileyClass 
{
  GObjectClass parent_class;
}
;
/* shortcut (char *) => smiley (PurpleSmiley*) */
static GHashTable *smiley_shortcut_index = (GHashTable *)((void *)0);
/* checksum (char *) => smiley (PurpleSmiley*) */
static GHashTable *smiley_checksum_index = (GHashTable *)((void *)0);
static guint save_timer = 0;
static gboolean smileys_loaded = 0;
static char *smileys_dir = (char *)((void *)0);
#define SMILEYS_DEFAULT_FOLDER			"custom_smiley"
#define SMILEYS_LOG_ID				"smileys"
#define XML_FILE_NAME				"smileys.xml"
#define XML_ROOT_TAG				"smileys"
#define XML_PROFILE_TAG			"profile"
#define XML_PROFILE_NAME_ATTRIB_TAG		"name"
#define XML_ACCOUNT_TAG			"account"
#define XML_ACCOUNT_USERID_ATTRIB_TAG		"userid"
#define XML_SMILEY_SET_TAG			"smiley_set"
#define XML_SMILEY_TAG				"smiley"
#define XML_SHORTCUT_ATTRIB_TAG		"shortcut"
#define XML_CHECKSUM_ATRIB_TAG			"checksum"
#define XML_FILENAME_ATRIB_TAG			"filename"
/******************************************************************************
 * XML descriptor file layout                                                 *
 ******************************************************************************
 *
 * Although we are creating the profile XML structure here, now we
 * won't handle it.
 * So, we just add one profile named "default" that has no associated
 * account elements, and have only the smiley_set that will contain
 * all existent custom smiley.
 *
 * It's our "Highlander Profile" :-)
 *
 ******************************************************************************
 *
 * <smileys>
 *   <profile name="john.doe">
 *     <account userid="john.doe@jabber.org">
 *     <account userid="john.doe@gmail.com">
 *     <smiley_set>
 *       <smiley shortcut="aaa" checksum="xxxxxxxx" filename="file_name1.gif"/>
 *       <smiley shortcut="bbb" checksum="yyyyyyy" filename="file_name2.gif"/>
 *     </smiley_set>
 *   </profile>
 * </smiley>
 *
 *****************************************************************************/
/*********************************************************************
 * Forward declarations                                              *
 *********************************************************************/
static gboolean read_smiley_file(const char *path,guchar **data,size_t *len);
static char *get_file_full_path(const char *filename);
static PurpleSmiley *purple_smiley_create(const char *shortcut);
static void purple_smiley_load_file(const char *shortcut,const char *checksum,const char *filename);
static void purple_smiley_set_data_impl(PurpleSmiley *smiley,guchar *smiley_data,size_t smiley_data_len);
static void purple_smiley_data_store(PurpleStoredImage *stored_img);
static void purple_smiley_data_unstore(const char *filename);
/*********************************************************************
 * Writing to disk                                                   *
 *********************************************************************/

static xmlnode *smiley_to_xmlnode(PurpleSmiley *smiley)
{
  xmlnode *smiley_node = (xmlnode *)((void *)0);
  smiley_node = xmlnode_new("smiley");
  if (!(smiley_node != 0)) 
    return 0;
  xmlnode_set_attrib(smiley_node,"shortcut",(smiley -> shortcut));
  xmlnode_set_attrib(smiley_node,"checksum",(smiley -> checksum));
  xmlnode_set_attrib(smiley_node,"filename",purple_imgstore_get_filename((smiley -> img)));
  return smiley_node;
}

static void add_smiley_to_main_node(gpointer key,gpointer value,gpointer user_data)
{
  xmlnode *child_node;
  child_node = smiley_to_xmlnode(value);
  xmlnode_insert_child(((xmlnode *)user_data),child_node);
}

static xmlnode *smileys_to_xmlnode()
{
  xmlnode *root_node;
  xmlnode *profile_node;
  xmlnode *smileyset_node;
  root_node = xmlnode_new("smileys");
  xmlnode_set_attrib(root_node,"version","1.0");
/* See the top comments above to understand why initial tag elements
	 * are not being considered by now. */
  profile_node = xmlnode_new("profile");
  if (profile_node != 0) {
    xmlnode_set_attrib(profile_node,"name","Default");
    xmlnode_insert_child(root_node,profile_node);
    smileyset_node = xmlnode_new("smiley_set");
    if (smileyset_node != 0) {
      xmlnode_insert_child(profile_node,smileyset_node);
      g_hash_table_foreach(smiley_shortcut_index,add_smiley_to_main_node,smileyset_node);
    }
  }
  return root_node;
}

static void sync_smileys()
{
  xmlnode *root_node;
  char *data;
  if (!(smileys_loaded != 0)) {
    purple_debug_error("smileys","Attempted to save smileys before it was read!\n");
    return ;
  }
  root_node = smileys_to_xmlnode();
  data = xmlnode_to_formatted_str(root_node,0);
  purple_util_write_data_to_file("smileys.xml",data,(-1));
  g_free(data);
  xmlnode_free(root_node);
}

static gboolean save_smileys_cb(gpointer data)
{
  sync_smileys();
  save_timer = 0;
  return 0;
}

static void purple_smileys_save()
{
  if (save_timer == 0) 
    save_timer = purple_timeout_add_seconds(5,save_smileys_cb,0);
}
/*********************************************************************
 * Reading from disk                                                 *
 *********************************************************************/

static void parse_smiley(xmlnode *smiley_node)
{
  const char *shortcut = (const char *)((void *)0);
  const char *checksum = (const char *)((void *)0);
  const char *filename = (const char *)((void *)0);
  shortcut = xmlnode_get_attrib(smiley_node,"shortcut");
  checksum = xmlnode_get_attrib(smiley_node,"checksum");
  filename = xmlnode_get_attrib(smiley_node,"filename");
  if (((shortcut == ((const char *)((void *)0))) || (checksum == ((const char *)((void *)0)))) || (filename == ((const char *)((void *)0)))) 
    return ;
  purple_smiley_load_file(shortcut,checksum,filename);
}

static void purple_smileys_load()
{
  xmlnode *root_node;
  xmlnode *profile_node;
  xmlnode *smileyset_node = (xmlnode *)((void *)0);
  xmlnode *smiley_node;
  smileys_loaded = (!0);
  root_node = purple_util_read_xml_from_file("smileys.xml",((const char *)(dgettext("pidgin","smileys"))));
  if (root_node == ((xmlnode *)((void *)0))) 
    return ;
/* See the top comments above to understand why initial tag elements
	 * are not being considered by now. */
  profile_node = xmlnode_get_child(root_node,"profile");
  if (profile_node != 0) 
    smileyset_node = xmlnode_get_child(profile_node,"smiley_set");
  if (smileyset_node != 0) {
    smiley_node = xmlnode_get_child(smileyset_node,"smiley");
    for (; smiley_node != ((xmlnode *)((void *)0)); smiley_node = xmlnode_get_next_twin(smiley_node)) {
      parse_smiley(smiley_node);
    }
  }
  xmlnode_free(root_node);
}
/*********************************************************************
 * GObject Stuff                                                     *
 *********************************************************************/
enum __unnamed_enum___F0_L282_C1_PROP_0__COMMA__PROP_SHORTCUT__COMMA__PROP_IMGSTORE {PROP_0,PROP_SHORTCUT,PROP_IMGSTORE};
#define PROP_SHORTCUT_S "shortcut"
#define PROP_IMGSTORE_S "image"
enum __unnamed_enum___F0_L292_C1_SIG_DESTROY__COMMA__SIG_LAST {SIG_DESTROY,SIG_LAST};
static guint signals[1UL];
static GObjectClass *parent_class;

static void purple_smiley_init(GTypeInstance *instance,gpointer klass)
{
  PurpleSmiley *smiley = (PurpleSmiley *)(g_type_check_instance_cast(((GTypeInstance *)instance),purple_smiley_get_type()));
{
    PurpleSmiley *typed_ptr = smiley;
    purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurpleSmiley);
  };
}

static void purple_smiley_get_property(GObject *object,guint param_id,GValue *value,GParamSpec *spec)
{
  PurpleSmiley *smiley = (PurpleSmiley *)(g_type_check_instance_cast(((GTypeInstance *)object),purple_smiley_get_type()));
  switch(param_id){
    case PROP_SHORTCUT:
{
      g_value_set_string(value,(smiley -> shortcut));
      break; 
    }
    case PROP_IMGSTORE:
{
      g_value_set_pointer(value,(smiley -> img));
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)spec;
        guint _glib__property_id = param_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","smiley.c:321","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void purple_smiley_set_property(GObject *object,guint param_id,const GValue *value,GParamSpec *spec)
{
  PurpleSmiley *smiley = (PurpleSmiley *)(g_type_check_instance_cast(((GTypeInstance *)object),purple_smiley_get_type()));
  switch(param_id){
    case PROP_SHORTCUT:
{
{
        const char *shortcut = g_value_get_string(value);
        purple_smiley_set_shortcut(smiley,shortcut);
      }
      break; 
    }
    case PROP_IMGSTORE:
{
{
        PurpleStoredImage *img = (g_value_get_pointer(value));
        purple_imgstore_unref((smiley -> img));
        g_free((smiley -> checksum));
        smiley -> img = img;
        if (img != 0) {
          smiley -> checksum = purple_util_get_image_checksum(purple_imgstore_get_data(img),purple_imgstore_get_size(img));
          purple_smiley_data_store(img);
        }
        else {
          smiley -> checksum = ((char *)((void *)0));
        }
        g_object_notify(object,"image");
      }
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)spec;
        guint _glib__property_id = param_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","smiley.c:359","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void purple_smiley_finalize(GObject *obj)
{
  PurpleSmiley *smiley = (PurpleSmiley *)(g_type_check_instance_cast(((GTypeInstance *)obj),purple_smiley_get_type()));
  if (g_hash_table_lookup(smiley_shortcut_index,(smiley -> shortcut)) != 0) {
    g_hash_table_remove(smiley_shortcut_index,(smiley -> shortcut));
    g_hash_table_remove(smiley_checksum_index,(smiley -> checksum));
  }
  g_free((smiley -> shortcut));
  g_free((smiley -> checksum));
  if ((smiley -> img) != 0) 
    purple_smiley_data_unstore(purple_imgstore_get_filename((smiley -> img)));
  purple_imgstore_unref((smiley -> img));
  purple_dbus_unregister_pointer(smiley);
  purple_smileys_save();
}

static void purple_smiley_dispose(GObject *gobj)
{
  g_signal_emit(gobj,signals[SIG_DESTROY],0);
  ( *(parent_class -> dispose))(gobj);
}

static void purple_smiley_class_init(PurpleSmileyClass *klass)
{
  GObjectClass *gobj_class = (GObjectClass *)(g_type_check_class_cast(((GTypeClass *)klass),((GType )(20 << 2))));
  GParamSpec *pspec;
  parent_class = (g_type_class_peek_parent(klass));
  gobj_class -> get_property = purple_smiley_get_property;
  gobj_class -> set_property = purple_smiley_set_property;
  gobj_class -> finalize = purple_smiley_finalize;
  gobj_class -> dispose = purple_smiley_dispose;
/* Shortcut */
  pspec = g_param_spec_string("shortcut",((const char *)(dgettext("pidgin","Shortcut"))),((const char *)(dgettext("pidgin","The text-shortcut for the smiley"))),0,(G_PARAM_READABLE | G_PARAM_WRITABLE));
  g_object_class_install_property(gobj_class,PROP_SHORTCUT,pspec);
/* Stored Image */
  pspec = g_param_spec_pointer("image",((const char *)(dgettext("pidgin","Stored Image"))),((const char *)(dgettext("pidgin","Stored Image. (that\'ll have to do for now)"))),(G_PARAM_READABLE | G_PARAM_WRITABLE));
  g_object_class_install_property(gobj_class,PROP_IMGSTORE,pspec);
  signals[SIG_DESTROY] = g_signal_new("destroy",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,0,0,0,g_cclosure_marshal_VOID__VOID,((GType )(1 << 2)),0);
}

GType purple_smiley_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(PurpleSmileyClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )purple_smiley_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(PurpleSmiley ))), (0), (purple_smiley_init), ((const GTypeValueTable *)((void *)0))};
    type = g_type_register_static(((GType )(20 << 2)),"PurpleSmiley",&info,0);
  }
  return type;
}
/*********************************************************************
 * Other Stuff                                                       *
 *********************************************************************/

static char *get_file_full_path(const char *filename)
{
  char *path;
  path = g_build_filename(purple_smileys_get_storing_dir(),filename,((void *)((void *)0)));
  if (!(g_file_test(path,G_FILE_TEST_EXISTS) != 0)) {
    g_free(path);
    return 0;
  }
  return path;
}

static void purple_smiley_load_file(const char *shortcut,const char *checksum,const char *filename)
{
  PurpleSmiley *smiley = (PurpleSmiley *)((void *)0);
  guchar *smiley_data;
  size_t smiley_data_len;
  char *fullpath = (char *)((void *)0);
  do {
    if (shortcut != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"shortcut != NULL");
      return ;
    };
  }while (0);
  do {
    if (checksum != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"checksum != NULL");
      return ;
    };
  }while (0);
  do {
    if (filename != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"filename != NULL");
      return ;
    };
  }while (0);
  fullpath = get_file_full_path(filename);
  if (!(fullpath != 0)) {
    purple_debug_error("smileys","Path for filename %s doesn\'t exist\n",filename);
    return ;
  }
  smiley = purple_smiley_create(shortcut);
  if (!(smiley != 0)) {
    g_free(fullpath);
    return ;
  }
  smiley -> checksum = g_strdup(checksum);
  if (read_smiley_file(fullpath,&smiley_data,&smiley_data_len) != 0) 
    purple_smiley_set_data_impl(smiley,smiley_data,smiley_data_len);
  else 
    purple_smiley_delete(smiley);
  g_free(fullpath);
}

static void purple_smiley_data_store(PurpleStoredImage *stored_img)
{
  const char *dirname;
  char *path;
  FILE *file = (FILE *)((void *)0);
  do {
    if (stored_img != ((PurpleStoredImage *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"stored_img != NULL");
      return ;
    };
  }while (0);
  if (!(smileys_loaded != 0)) 
    return ;
  dirname = purple_smileys_get_storing_dir();
  path = g_build_filename(dirname,purple_imgstore_get_filename(stored_img),((void *)((void *)0)));
  if (!(g_file_test(dirname,G_FILE_TEST_IS_DIR) != 0)) {
    purple_debug_info("smileys","Creating smileys directory.\n");
    if (mkdir(dirname,(256 | 128 | 64)) < 0) {
      purple_debug_error("smileys","Unable to create directory %s: %s\n",dirname,g_strerror( *__errno_location()));
    }
  }
  if ((file = fopen(path,"wb")) != ((FILE *)((void *)0))) {
    if (!(fwrite(purple_imgstore_get_data(stored_img),purple_imgstore_get_size(stored_img),1,file) != 0UL)) {
      purple_debug_error("smileys","Error writing %s: %s\n",path,g_strerror( *__errno_location()));
    }
    else {
      purple_debug_info("smileys","Wrote cache file: %s\n",path);
    }
    fclose(file);
  }
  else {
    purple_debug_error("smileys","Unable to create file %s: %s\n",path,g_strerror( *__errno_location()));
    g_free(path);
    return ;
  }
  g_free(path);
}

static void purple_smiley_data_unstore(const char *filename)
{
  const char *dirname;
  char *path;
  do {
    if (filename != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"filename != NULL");
      return ;
    };
  }while (0);
  dirname = purple_smileys_get_storing_dir();
  path = g_build_filename(dirname,filename,((void *)((void *)0)));
  if (g_file_test(path,G_FILE_TEST_EXISTS) != 0) {
    if (g_unlink(path) != 0) 
      purple_debug_error("smileys","Failed to delete %s: %s\n",path,g_strerror( *__errno_location()));
    else 
      purple_debug_info("smileys","Deleted cache file: %s\n",path);
  }
  g_free(path);
}

static gboolean read_smiley_file(const char *path,guchar **data,size_t *len)
{
  GError *err = (GError *)((void *)0);
  if (!(g_file_get_contents(path,((gchar **)data),len,&err) != 0)) {
    purple_debug_error("smileys","Error reading %s: %s\n",path,(err -> message));
    g_error_free(err);
    return 0;
  }
  return (!0);
}

static PurpleStoredImage *purple_smiley_data_new(guchar *smiley_data,size_t smiley_data_len)
{
  char *filename;
  PurpleStoredImage *stored_img;
  do {
    if (smiley_data != ((guchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"smiley_data != NULL");
      return 0;
    };
  }while (0);
  do {
    if (smiley_data_len > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"smiley_data_len > 0");
      return 0;
    };
  }while (0);
  filename = purple_util_get_image_filename(smiley_data,smiley_data_len);
  if (filename == ((char *)((void *)0))) {
    g_free(smiley_data);
    return 0;
  }
  stored_img = purple_imgstore_add(smiley_data,smiley_data_len,filename);
  g_free(filename);
  return stored_img;
}

static void purple_smiley_set_data_impl(PurpleSmiley *smiley,guchar *smiley_data,size_t smiley_data_len)
{
  PurpleStoredImage *old_img;
  PurpleStoredImage *new_img;
  const char *old_filename = (const char *)((void *)0);
  const char *new_filename = (const char *)((void *)0);
  do {
    if (smiley != ((PurpleSmiley *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"smiley != NULL");
      return ;
    };
  }while (0);
  do {
    if (smiley_data != ((guchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"smiley_data != NULL");
      return ;
    };
  }while (0);
  do {
    if (smiley_data_len > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"smiley_data_len > 0");
      return ;
    };
  }while (0);
  old_img = (smiley -> img);
  new_img = purple_smiley_data_new(smiley_data,smiley_data_len);
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)smiley),((GType )(20 << 2))))),"image",new_img,((void *)((void *)0)));
/* If the old and new image files have different names we need
	 * to unstore old image file. */
  if (!(old_img != 0)) 
    return ;
  old_filename = purple_imgstore_get_filename(old_img);
  new_filename = purple_imgstore_get_filename((smiley -> img));
  if (g_ascii_strcasecmp(old_filename,new_filename) != 0) 
    purple_smiley_data_unstore(old_filename);
  purple_imgstore_unref(old_img);
}
/*****************************************************************************
 * Public API functions                                                      *
 *****************************************************************************/

static PurpleSmiley *purple_smiley_create(const char *shortcut)
{
  PurpleSmiley *smiley;
  smiley = ((PurpleSmiley *)(g_type_check_instance_cast(((GTypeInstance *)(g_object_new(purple_smiley_get_type(),"shortcut",shortcut,((void *)((void *)0))))),purple_smiley_get_type())));
  return smiley;
}

PurpleSmiley *purple_smiley_new(PurpleStoredImage *img,const char *shortcut)
{
  PurpleSmiley *smiley = (PurpleSmiley *)((void *)0);
  do {
    if (shortcut != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"shortcut != NULL");
      return 0;
    };
  }while (0);
  do {
    if (img != ((PurpleStoredImage *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"img != NULL");
      return 0;
    };
  }while (0);
  smiley = purple_smileys_find_by_shortcut(shortcut);
  if (smiley != 0) 
    return smiley;
  smiley = purple_smiley_create(shortcut);
  if (!(smiley != 0)) 
    return 0;
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)smiley),((GType )(20 << 2))))),"image",img,((void *)((void *)0)));
  return smiley;
}

static PurpleSmiley *purple_smiley_new_from_stream(const char *shortcut,guchar *smiley_data,size_t smiley_data_len)
{
  PurpleSmiley *smiley;
  do {
    if (shortcut != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"shortcut != NULL");
      return 0;
    };
  }while (0);
  do {
    if (smiley_data != ((guchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"smiley_data != NULL");
      return 0;
    };
  }while (0);
  do {
    if (smiley_data_len > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"smiley_data_len > 0");
      return 0;
    };
  }while (0);
  smiley = purple_smileys_find_by_shortcut(shortcut);
  if (smiley != 0) 
    return smiley;
/* purple_smiley_create() sets shortcut */
  smiley = purple_smiley_create(shortcut);
  if (!(smiley != 0)) 
    return 0;
  purple_smiley_set_data_impl(smiley,smiley_data,smiley_data_len);
  purple_smiley_data_store((smiley -> img));
  return smiley;
}

PurpleSmiley *purple_smiley_new_from_file(const char *shortcut,const char *filepath)
{
  PurpleSmiley *smiley = (PurpleSmiley *)((void *)0);
  guchar *smiley_data;
  size_t smiley_data_len;
  do {
    if (shortcut != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"shortcut != NULL");
      return 0;
    };
  }while (0);
  do {
    if (filepath != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"filepath != NULL");
      return 0;
    };
  }while (0);
  if (read_smiley_file(filepath,&smiley_data,&smiley_data_len) != 0) {
    smiley = purple_smiley_new_from_stream(shortcut,smiley_data,smiley_data_len);
  }
  return smiley;
}

void purple_smiley_delete(PurpleSmiley *smiley)
{
  do {
    if (smiley != ((PurpleSmiley *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"smiley != NULL");
      return ;
    };
  }while (0);
  g_object_unref(smiley);
}

gboolean purple_smiley_set_shortcut(PurpleSmiley *smiley,const char *shortcut)
{
  do {
    if (smiley != ((PurpleSmiley *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"smiley != NULL");
      return 0;
    };
  }while (0);
  do {
    if (shortcut != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"shortcut != NULL");
      return 0;
    };
  }while (0);
/* Check out whether the new shortcut is already being used. */
  if (g_hash_table_lookup(smiley_shortcut_index,shortcut) != 0) 
    return 0;
/* Remove the old shortcut. */
  if ((smiley -> shortcut) != 0) 
    g_hash_table_remove(smiley_shortcut_index,(smiley -> shortcut));
/* Insert the new shortcut. */
  g_hash_table_insert(smiley_shortcut_index,(g_strdup(shortcut)),smiley);
  g_free((smiley -> shortcut));
  smiley -> shortcut = g_strdup(shortcut);
  g_object_notify(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)smiley),((GType )(20 << 2))))),"shortcut");
  purple_smileys_save();
  return (!0);
}

void purple_smiley_set_data(PurpleSmiley *smiley,guchar *smiley_data,size_t smiley_data_len)
{
  do {
    if (smiley != ((PurpleSmiley *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"smiley != NULL");
      return ;
    };
  }while (0);
  do {
    if (smiley_data != ((guchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"smiley_data != NULL");
      return ;
    };
  }while (0);
  do {
    if (smiley_data_len > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"smiley_data_len > 0");
      return ;
    };
  }while (0);
/* Remove the previous entry */
  g_hash_table_remove(smiley_checksum_index,(smiley -> checksum));
/* Update the file data. This also updates the checksum. */
  purple_smiley_set_data_impl(smiley,smiley_data,smiley_data_len);
/* Reinsert the index item. */
  g_hash_table_insert(smiley_checksum_index,(g_strdup((smiley -> checksum))),smiley);
  purple_smileys_save();
}

PurpleStoredImage *purple_smiley_get_stored_image(const PurpleSmiley *smiley)
{
  return purple_imgstore_ref((smiley -> img));
}

const char *purple_smiley_get_shortcut(const PurpleSmiley *smiley)
{
  do {
    if (smiley != ((const PurpleSmiley *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"smiley != NULL");
      return 0;
    };
  }while (0);
  return (smiley -> shortcut);
}

const char *purple_smiley_get_checksum(const PurpleSmiley *smiley)
{
  do {
    if (smiley != ((const PurpleSmiley *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"smiley != NULL");
      return 0;
    };
  }while (0);
  return (smiley -> checksum);
}

gconstpointer purple_smiley_get_data(const PurpleSmiley *smiley,size_t *len)
{
  do {
    if (smiley != ((const PurpleSmiley *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"smiley != NULL");
      return 0;
    };
  }while (0);
  if ((smiley -> img) != 0) {
    if (len != ((size_t *)((void *)0))) 
       *len = purple_imgstore_get_size((smiley -> img));
    return purple_imgstore_get_data((smiley -> img));
  }
  return 0;
}

const char *purple_smiley_get_extension(const PurpleSmiley *smiley)
{
  if ((smiley -> img) != ((PurpleStoredImage *)((void *)0))) 
    return purple_imgstore_get_extension((smiley -> img));
  return 0;
}

char *purple_smiley_get_full_path(PurpleSmiley *smiley)
{
  do {
    if (smiley != ((PurpleSmiley *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"smiley != NULL");
      return 0;
    };
  }while (0);
  if ((smiley -> img) == ((PurpleStoredImage *)((void *)0))) 
    return 0;
  return get_file_full_path(purple_imgstore_get_filename((smiley -> img)));
}

static void add_smiley_to_list(gpointer key,gpointer value,gpointer user_data)
{
  GList **returninglist = (GList **)user_data;
   *returninglist = g_list_append( *returninglist,value);
}

GList *purple_smileys_get_all()
{
  GList *returninglist = (GList *)((void *)0);
  g_hash_table_foreach(smiley_shortcut_index,add_smiley_to_list,(&returninglist));
  return returninglist;
}

PurpleSmiley *purple_smileys_find_by_shortcut(const char *shortcut)
{
  do {
    if (shortcut != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"shortcut != NULL");
      return 0;
    };
  }while (0);
  return (g_hash_table_lookup(smiley_shortcut_index,shortcut));
}

PurpleSmiley *purple_smileys_find_by_checksum(const char *checksum)
{
  do {
    if (checksum != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"checksum != NULL");
      return 0;
    };
  }while (0);
  return (g_hash_table_lookup(smiley_checksum_index,checksum));
}

const char *purple_smileys_get_storing_dir()
{
  return smileys_dir;
}

void purple_smileys_init()
{
  smiley_shortcut_index = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,0);
  smiley_checksum_index = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,0);
  smileys_dir = g_build_filename(purple_user_dir(),"custom_smiley",((void *)((void *)0)));
  purple_smileys_load();
}

void purple_smileys_uninit()
{
  if (save_timer != 0) {
    purple_timeout_remove(save_timer);
    save_timer = 0;
    sync_smileys();
  }
  g_hash_table_destroy(smiley_shortcut_index);
  g_hash_table_destroy(smiley_checksum_index);
  g_free(smileys_dir);
}
