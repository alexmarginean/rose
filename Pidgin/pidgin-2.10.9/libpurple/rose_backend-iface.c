/**
 * @file backend-iface.c Interface for media backend
 * @ingroup core
 */
/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "backend-iface.h"
#include "marshallers.h"
enum __unnamed_enum___F0_L31_C1_S_ERROR__COMMA__CANDIDATES_PREPARED__COMMA__CODECS_CHANGED__COMMA__NEW_CANDIDATE__COMMA__ACTIVE_CANDIDATE_PAIR__COMMA__LAST_SIGNAL {S_ERROR,CANDIDATES_PREPARED,CODECS_CHANGED,NEW_CANDIDATE,ACTIVE_CANDIDATE_PAIR,LAST_SIGNAL};
static guint purple_media_backend_signals[5UL] = {(0)};

static void purple_media_backend_base_init(gpointer iface)
{
  static gboolean is_initialized = 0;
  if (is_initialized != 0) 
    return ;
  g_object_interface_install_property(iface,g_param_spec_string("conference-type","Conference Type","The type of conference that this backend has been created to provide.",0,(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_interface_install_property(iface,g_param_spec_object("media","Purple Media","The media object that this backend is bound to.",purple_media_get_type(),(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  purple_media_backend_signals[S_ERROR] = g_signal_new("error",( *((GTypeClass *)iface)).g_type,G_SIGNAL_RUN_LAST,0,0,0,g_cclosure_marshal_VOID__STRING,((GType )(1 << 2)),1,((GType )(16 << 2)));
  purple_media_backend_signals[CANDIDATES_PREPARED] = g_signal_new("candidates-prepared",( *((GTypeClass *)iface)).g_type,G_SIGNAL_RUN_LAST,0,0,0,purple_smarshal_VOID__STRING_STRING,((GType )(1 << 2)),2,((GType )(16 << 2)),((GType )(16 << 2)));
  purple_media_backend_signals[CODECS_CHANGED] = g_signal_new("codecs-changed",( *((GTypeClass *)iface)).g_type,G_SIGNAL_RUN_LAST,0,0,0,g_cclosure_marshal_VOID__STRING,((GType )(1 << 2)),1,((GType )(16 << 2)));
  purple_media_backend_signals[NEW_CANDIDATE] = g_signal_new("new-candidate",( *((GTypeClass *)iface)).g_type,G_SIGNAL_RUN_LAST,0,0,0,purple_smarshal_VOID__POINTER_POINTER_OBJECT,((GType )(1 << 2)),3,((GType )(17 << 2)),((GType )(17 << 2)),purple_media_candidate_get_type());
  purple_media_backend_signals[ACTIVE_CANDIDATE_PAIR] = g_signal_new("active-candidate-pair",( *((GTypeClass *)iface)).g_type,G_SIGNAL_RUN_LAST,0,0,0,purple_smarshal_VOID__STRING_STRING_OBJECT_OBJECT,((GType )(1 << 2)),4,((GType )(16 << 2)),((GType )(16 << 2)),purple_media_candidate_get_type(),purple_media_candidate_get_type());
  is_initialized = (!0);
}

GType purple_media_backend_get_type()
{
  static GType iface_type = 0;
  if (iface_type == 0) {
    static const GTypeInfo info = {((sizeof(PurpleMediaBackendIface ))), (purple_media_backend_base_init), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )((void *)0)), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), (0), (0), ((GInstanceInitFunc )((void *)0)), ((const GTypeValueTable *)((void *)0))};
    iface_type = g_type_register_static(((GType )(2 << 2)),"PurpleMediaBackend",&info,0);
  }
  return iface_type;
}

gboolean purple_media_backend_add_stream(PurpleMediaBackend *self,const gchar *sess_id,const gchar *who,PurpleMediaSessionType type,gboolean initiator,const gchar *transmitter,guint num_params,GParameter *params)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND(self)");
      return 0;
    };
  }while (0);
  return ( *( *((PurpleMediaBackendIface *)(g_type_interface_peek(( *((GTypeInstance *)self)).g_class,purple_media_backend_get_type())))).add_stream)(self,sess_id,who,type,initiator,transmitter,num_params,params);
}

void purple_media_backend_add_remote_candidates(PurpleMediaBackend *self,const gchar *sess_id,const gchar *participant,GList *remote_candidates)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND(self)");
      return ;
    };
  }while (0);
  ( *( *((PurpleMediaBackendIface *)(g_type_interface_peek(( *((GTypeInstance *)self)).g_class,purple_media_backend_get_type())))).add_remote_candidates)(self,sess_id,participant,remote_candidates);
}

gboolean purple_media_backend_codecs_ready(PurpleMediaBackend *self,const gchar *sess_id)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND(self)");
      return 0;
    };
  }while (0);
  return ( *( *((PurpleMediaBackendIface *)(g_type_interface_peek(( *((GTypeInstance *)self)).g_class,purple_media_backend_get_type())))).codecs_ready)(self,sess_id);
}

GList *purple_media_backend_get_codecs(PurpleMediaBackend *self,const gchar *sess_id)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND(self)");
      return 0;
    };
  }while (0);
  return ( *( *((PurpleMediaBackendIface *)(g_type_interface_peek(( *((GTypeInstance *)self)).g_class,purple_media_backend_get_type())))).get_codecs)(self,sess_id);
}

GList *purple_media_backend_get_local_candidates(PurpleMediaBackend *self,const gchar *sess_id,const gchar *participant)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND(self)");
      return 0;
    };
  }while (0);
  return ( *( *((PurpleMediaBackendIface *)(g_type_interface_peek(( *((GTypeInstance *)self)).g_class,purple_media_backend_get_type())))).get_local_candidates)(self,sess_id,participant);
}

gboolean purple_media_backend_set_remote_codecs(PurpleMediaBackend *self,const gchar *sess_id,const gchar *participant,GList *codecs)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND(self)");
      return 0;
    };
  }while (0);
  return ( *( *((PurpleMediaBackendIface *)(g_type_interface_peek(( *((GTypeInstance *)self)).g_class,purple_media_backend_get_type())))).set_remote_codecs)(self,sess_id,participant,codecs);
}

gboolean purple_media_backend_set_send_codec(PurpleMediaBackend *self,const gchar *sess_id,PurpleMediaCodec *codec)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND(self)");
      return 0;
    };
  }while (0);
  return ( *( *((PurpleMediaBackendIface *)(g_type_interface_peek(( *((GTypeInstance *)self)).g_class,purple_media_backend_get_type())))).set_send_codec)(self,sess_id,codec);
}

void purple_media_backend_set_params(PurpleMediaBackend *self,guint num_params,GParameter *params)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND(self)");
      return ;
    };
  }while (0);
  ( *( *((PurpleMediaBackendIface *)(g_type_interface_peek(( *((GTypeInstance *)self)).g_class,purple_media_backend_get_type())))).set_params)(self,num_params,params);
}

const gchar **purple_media_backend_get_available_params(PurpleMediaBackend *self)
{
  static const gchar *NULL_ARRAY[] = {((const gchar *)((void *)0))};
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND(self)");
      return NULL_ARRAY;
    };
  }while (0);
  return ( *( *((PurpleMediaBackendIface *)(g_type_interface_peek(( *((GTypeInstance *)self)).g_class,purple_media_backend_get_type())))).get_available_params)();
}
