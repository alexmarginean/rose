/*
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <cipher.h>
#include <util.h>
#if !GLIB_CHECK_VERSION(2,16,0)
#define SHA1_HMAC_BLOCK_SIZE    64
#define SHA1_ROTL(X,n) ((((X) << (n)) | ((X) >> (32-(n)))) & 0xFFFFFFFF)
/* This does not change (in this case) */
/* pad with a 1, then zeroes, then length */
/* Set Option		*/
/* Get Option		*/
/* init				*/
/* reset			*/
/* uninit			*/
/* set iv			*/
/* append			*/
/* digest			*/
/* encrypt			*/
/* decrypt			*/
/* set salt			*/
/* get salt size	*/
/* set key			*/
/* get key size		*/
/* set batch mode */
/* get batch mode */
/* get block size */
/* set key with len */
#endif /* !GLIB_CHECK_VERSION(2,16,0) */
