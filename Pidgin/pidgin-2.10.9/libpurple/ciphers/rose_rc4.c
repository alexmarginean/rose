/*
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <cipher.h>
#include <util.h>

struct RC4Context 
{
  guchar state[256UL];
  guchar x;
  guchar y;
  gint key_len;
}
;

static void rc4_init(PurpleCipherContext *context,void *extra)
{
  struct RC4Context *rc4_ctx;
  rc4_ctx = ((struct RC4Context *)(g_malloc0_n(1,(sizeof(struct RC4Context )))));
  purple_cipher_context_set_data(context,rc4_ctx);
  purple_cipher_context_reset(context,extra);
}

static void rc4_reset(PurpleCipherContext *context,void *extra)
{
  struct RC4Context *rc4_ctx;
  guint i;
  rc4_ctx = (purple_cipher_context_get_data(context));
  do {
    if (rc4_ctx != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"rc4_ctx");
      return ;
    };
  }while (0);
  for (i = 0; i < 256; i++) 
    (rc4_ctx -> state)[i] = i;
  rc4_ctx -> x = 0;
  rc4_ctx -> y = 0;
/* default is 5 bytes (40bit key) */
  rc4_ctx -> key_len = 5;
}

static void rc4_uninit(PurpleCipherContext *context)
{
  struct RC4Context *rc4_ctx;
  rc4_ctx = (purple_cipher_context_get_data(context));
  memset(rc4_ctx,0,(sizeof(( *rc4_ctx))));
  g_free(rc4_ctx);
  rc4_ctx = ((struct RC4Context *)((void *)0));
}

static void rc4_set_key(PurpleCipherContext *context,const guchar *key)
{
  struct RC4Context *ctx;
  guchar *state;
  guchar temp_swap;
  guchar x;
  guchar y;
  guint i;
  ctx = (purple_cipher_context_get_data(context));
  x = 0;
  y = 0;
  state = ((ctx -> state) + 0);
  for (i = 0; i < 256; i++) {
    y = (((key[x] + state[i]) + y) % 256);
    temp_swap = state[i];
    state[i] = state[y];
    state[y] = temp_swap;
    x = ((x + 1) % (ctx -> key_len));
  }
}

static void rc4_set_opt(PurpleCipherContext *context,const gchar *name,void *value)
{
  struct RC4Context *ctx;
  ctx = (purple_cipher_context_get_data(context));
  if (purple_strequal(name,"key_len") != 0) {
    ctx -> key_len = ((gint )((glong )value));
  }
}

static size_t rc4_get_key_size(PurpleCipherContext *context)
{
  struct RC4Context *ctx;
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return (-1);
    };
  }while (0);
  ctx = (purple_cipher_context_get_data(context));
  do {
    if (ctx != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ctx");
      return (-1);
    };
  }while (0);
  return (ctx -> key_len);
}

static void *rc4_get_opt(PurpleCipherContext *context,const gchar *name)
{
  struct RC4Context *ctx;
  ctx = (purple_cipher_context_get_data(context));
  if (purple_strequal(name,"key_len") != 0) {
    return (gpointer )((glong )(ctx -> key_len));
  }
  return 0;
}

static gint rc4_encrypt(PurpleCipherContext *context,const guchar data[],size_t len,guchar output[],size_t *outlen)
{
  struct RC4Context *ctx;
  guchar temp_swap;
  guchar x;
  guchar y;
  guchar z;
  guchar *state;
  guint i;
  ctx = (purple_cipher_context_get_data(context));
  x = (ctx -> x);
  y = (ctx -> y);
  state = ((ctx -> state) + 0);
  for (i = 0; i < len; i++) {
    x = ((x + 1) % 256);
    y = ((state[x] + y) % 256);
    temp_swap = state[x];
    state[x] = state[y];
    state[y] = temp_swap;
    z = (state[x] + (state[y] % 256));
    output[i] = (data[i] ^ state[z]);
  }
  ctx -> x = x;
  ctx -> y = y;
  if (outlen != 0) 
     *outlen = len;
  return 0;
}
static PurpleCipherOps RC4Ops = {(rc4_set_opt), (rc4_get_opt), (rc4_init), (rc4_reset), (rc4_uninit), ((void (*)(PurpleCipherContext *, guchar *, size_t ))((void *)0)), ((void (*)(PurpleCipherContext *, const guchar *, size_t ))((void *)0)), ((gboolean (*)(PurpleCipherContext *, size_t , guchar *, size_t *))((void *)0)), (rc4_encrypt), ((int (*)(PurpleCipherContext *, const guchar *, size_t , guchar *, size_t *))((void *)0)), ((void (*)(PurpleCipherContext *, guchar *))((void *)0)), ((size_t (*)(PurpleCipherContext *))((void *)0)), (rc4_set_key), (rc4_get_key_size), ((void (*)(PurpleCipherContext *, PurpleCipherBatchMode ))((void *)0)), ((PurpleCipherBatchMode (*)(PurpleCipherContext *))((void *)0)), ((size_t (*)(PurpleCipherContext *))((void *)0)), ((void (*)(PurpleCipherContext *, const guchar *, size_t ))((void *)0))
/* Set Option    */
/* Get Option    */
/* init          */
/* reset         */
/* uninit        */
/* set iv        */
/* append        */
/* digest        */
/* encrypt       */
/* decrypt       */
/* set salt      */
/* get salt size */
/* set key       */
/* get key size  */
/* set batch mode */
/* get batch mode */
/* get block size */
/* set key with len */
};

PurpleCipherOps *purple_rc4_cipher_get_ops()
{
  return &RC4Ops;
}
