/*
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * Original md5
 * Copyright (C) 2001-2003  Christophe Devine <c.devine@cr0.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <cipher.h>
#if !GLIB_CHECK_VERSION(2,16,0)
#define MD5_HMAC_BLOCK_SIZE 64
#define MD5_GET_GUINT32(n,b,i) {            \
	(n) = ((guint32)(b) [(i)    ]      )    \
	| ((guint32)(b) [(i) + 1] <<  8)    \
	| ((guint32)(b) [(i) + 2] << 16)    \
	| ((guint32)(b) [(i) + 3] << 24);   \
}
#define MD5_PUT_GUINT32(n,b,i) {            \
	(b)[(i)    ] = (guchar)((n)      );     \
	(b)[(i) + 1] = (guchar)((n) >>  8);     \
	(b)[(i) + 2] = (guchar)((n) >> 16);     \
	(b)[(i) + 3] = (guchar)((n) >> 24);     \
}
/* This does not change (in this case) */
#define S(x,n) ((x << n) | ((x & 0xFFFFFFFF) >> (32 - n)))
#define P(a,b,c,d,k,s,t) {      \
	a += F(b,c,d) + X[k] + t;   \
	a = S(a,s) + b;             \
}
/* first pass */
#define F(x,y,z) (z ^ (x & (y ^ z)))
#undef F
/* second pass */
#define F(x,y,z) (y ^ (z & (x ^ y)))
#undef F
/* third pass */
#define F(x,y,z) (x ^ y ^ z)
#undef F
/* forth pass */
#define F(x,y,z) (y ^ (x | ~z))
#undef F
#undef P
#undef S
/* Set Option		*/
/* Get Option		*/
/* init				*/
/* reset			*/
/* uninit			*/
/* set iv			*/
/* append			*/
/* digest			*/
/* encrypt			*/
/* decrypt			*/
/* set salt			*/
/* get salt size	*/
/* set key			*/
/* get key size		*/
/* set batch mode */
/* get batch mode */
/* get block size */
/* set key with len */
#endif /* !GLIB_CHECK_VERSION(2,16,0) */
