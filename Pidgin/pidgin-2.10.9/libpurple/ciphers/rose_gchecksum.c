#include <cipher.h>
#if GLIB_CHECK_VERSION(2,16,0)

static void purple_g_checksum_init(PurpleCipherContext *context,GChecksumType type)
{
  GChecksum *checksum;
  checksum = g_checksum_new(type);
  purple_cipher_context_set_data(context,checksum);
}

static void purple_g_checksum_reset(PurpleCipherContext *context,GChecksumType type)
{
  GChecksum *checksum;
  checksum = (purple_cipher_context_get_data(context));
  do {
    if (checksum != ((GChecksum *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"checksum != NULL");
      return ;
    };
  }while (0);
#if GLIB_CHECK_VERSION(2,18,0)
  g_checksum_reset(checksum);
#else
#endif
}

static void purple_g_checksum_uninit(PurpleCipherContext *context)
{
  GChecksum *checksum;
  checksum = (purple_cipher_context_get_data(context));
  do {
    if (checksum != ((GChecksum *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"checksum != NULL");
      return ;
    };
  }while (0);
  g_checksum_free(checksum);
}

static void purple_g_checksum_append(PurpleCipherContext *context,const guchar *data,gsize len)
{
  GChecksum *checksum;
  checksum = (purple_cipher_context_get_data(context));
  do {
    if (checksum != ((GChecksum *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"checksum != NULL");
      return ;
    };
  }while (0);
  while(len >= 9223372036854775807L){
    g_checksum_update(checksum,data,9223372036854775807L);
    len -= 9223372036854775807L;
    data += 9223372036854775807L;
  }
  if (len != 0UL) 
    g_checksum_update(checksum,data,len);
}

static gboolean purple_g_checksum_digest(PurpleCipherContext *context,GChecksumType type,gsize len,guchar *digest,gsize *out_len)
{
  GChecksum *checksum;
  const gssize required_length = g_checksum_type_get_length(type);
  checksum = (purple_cipher_context_get_data(context));
  do {
    if (len >= required_length) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"len >= required_length");
      return 0;
    };
  }while (0);
  do {
    if (checksum != ((GChecksum *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"checksum != NULL");
      return 0;
    };
  }while (0);
  g_checksum_get_digest(checksum,digest,&len);
  purple_cipher_context_reset(context,0);
  if (out_len != 0) 
     *out_len = len;
  return (!0);
}
/******************************************************************************
 * Macros
 *****************************************************************************/
#define PURPLE_G_CHECKSUM_IMPLEMENTATION(lower, camel, type, block_size) \
	static size_t \
	lower##_get_block_size(PurpleCipherContext *context) { \
		return (block_size); \
	} \
	\
	static void \
	lower##_init(PurpleCipherContext *context, gpointer extra) { \
		purple_g_checksum_init(context, (type)); \
	} \
	\
	static void \
	lower##_reset(PurpleCipherContext *context, gpointer extra) { \
		purple_g_checksum_reset(context, (type)); \
	} \
	\
	static gboolean \
	lower##_digest(PurpleCipherContext *context, gsize in_len, \
	                 guchar digest[], size_t *out_len) \
	{ \
		return purple_g_checksum_digest(context, (type), in_len, digest, \
		                                out_len); \
	} \
	\
	static PurpleCipherOps camel##Ops = { \
		NULL,                     /* Set option */       \
		NULL,                     /* Get option */       \
		lower##_init,             /* init */             \
		lower##_reset,            /* reset */            \
		purple_g_checksum_uninit, /* uninit */           \
		NULL,                     /* set iv */           \
		purple_g_checksum_append, /* append */           \
		lower##_digest,           /* digest */           \
		NULL,                     /* encrypt */          \
		NULL,                     /* decrypt */          \
		NULL,                     /* set salt */         \
		NULL,                     /* get salt size */    \
		NULL,                     /* set key */          \
		NULL,                     /* get key size */     \
		NULL,                     /* set batch mode */   \
		NULL,                     /* get batch mode */   \
		lower##_get_block_size,   /* get block size */   \
		NULL                      /* set key with len */ \
	}; \
	\
	PurpleCipherOps * \
	purple_##lower##_cipher_get_ops(void) { \
		return &camel##Ops; \
	}
/******************************************************************************
 * Macro Expansion
 *****************************************************************************/

static size_t md5_get_block_size(PurpleCipherContext *context)
{
  return 64;
}

static void md5_init(PurpleCipherContext *context,gpointer extra)
{
  purple_g_checksum_init(context,G_CHECKSUM_MD5);
}

static void md5_reset(PurpleCipherContext *context,gpointer extra)
{
  purple_g_checksum_reset(context,G_CHECKSUM_MD5);
}

static gboolean md5_digest(PurpleCipherContext *context,gsize in_len,guchar digest[],size_t *out_len)
{
  return purple_g_checksum_digest(context,G_CHECKSUM_MD5,in_len,digest,out_len);
}
static PurpleCipherOps MD5Ops = {((void (*)(PurpleCipherContext *, const gchar *, void *))((void *)0)), ((void *(*)(PurpleCipherContext *, const gchar *))((void *)0)), (md5_init), (md5_reset), (purple_g_checksum_uninit), ((void (*)(PurpleCipherContext *, guchar *, size_t ))((void *)0)), (purple_g_checksum_append), (md5_digest), ((int (*)(PurpleCipherContext *, const guchar *, size_t , guchar *, size_t *))((void *)0)), ((int (*)(PurpleCipherContext *, const guchar *, size_t , guchar *, size_t *))((void *)0)), ((void (*)(PurpleCipherContext *, guchar *))((void *)0)), ((size_t (*)(PurpleCipherContext *))((void *)0)), ((void (*)(PurpleCipherContext *, const guchar *))((void *)0)), ((size_t (*)(PurpleCipherContext *))((void *)0)), ((void (*)(PurpleCipherContext *, PurpleCipherBatchMode ))((void *)0)), ((PurpleCipherBatchMode (*)(PurpleCipherContext *))((void *)0)), (md5_get_block_size), ((void (*)(PurpleCipherContext *, const guchar *, size_t ))((void *)0))};

PurpleCipherOps *purple_md5_cipher_get_ops()
{
  return &MD5Ops;
}

static size_t sha1_get_block_size(PurpleCipherContext *context)
{
  return 64;
}

static void sha1_init(PurpleCipherContext *context,gpointer extra)
{
  purple_g_checksum_init(context,G_CHECKSUM_SHA1);
}

static void sha1_reset(PurpleCipherContext *context,gpointer extra)
{
  purple_g_checksum_reset(context,G_CHECKSUM_SHA1);
}

static gboolean sha1_digest(PurpleCipherContext *context,gsize in_len,guchar digest[],size_t *out_len)
{
  return purple_g_checksum_digest(context,G_CHECKSUM_SHA1,in_len,digest,out_len);
}
static PurpleCipherOps SHA1Ops = {((void (*)(PurpleCipherContext *, const gchar *, void *))((void *)0)), ((void *(*)(PurpleCipherContext *, const gchar *))((void *)0)), (sha1_init), (sha1_reset), (purple_g_checksum_uninit), ((void (*)(PurpleCipherContext *, guchar *, size_t ))((void *)0)), (purple_g_checksum_append), (sha1_digest), ((int (*)(PurpleCipherContext *, const guchar *, size_t , guchar *, size_t *))((void *)0)), ((int (*)(PurpleCipherContext *, const guchar *, size_t , guchar *, size_t *))((void *)0)), ((void (*)(PurpleCipherContext *, guchar *))((void *)0)), ((size_t (*)(PurpleCipherContext *))((void *)0)), ((void (*)(PurpleCipherContext *, const guchar *))((void *)0)), ((size_t (*)(PurpleCipherContext *))((void *)0)), ((void (*)(PurpleCipherContext *, PurpleCipherBatchMode ))((void *)0)), ((PurpleCipherBatchMode (*)(PurpleCipherContext *))((void *)0)), (sha1_get_block_size), ((void (*)(PurpleCipherContext *, const guchar *, size_t ))((void *)0))};

PurpleCipherOps *purple_sha1_cipher_get_ops()
{
  return &SHA1Ops;
}

static size_t sha256_get_block_size(PurpleCipherContext *context)
{
  return 64;
}

static void sha256_init(PurpleCipherContext *context,gpointer extra)
{
  purple_g_checksum_init(context,G_CHECKSUM_SHA256);
}

static void sha256_reset(PurpleCipherContext *context,gpointer extra)
{
  purple_g_checksum_reset(context,G_CHECKSUM_SHA256);
}

static gboolean sha256_digest(PurpleCipherContext *context,gsize in_len,guchar digest[],size_t *out_len)
{
  return purple_g_checksum_digest(context,G_CHECKSUM_SHA256,in_len,digest,out_len);
}
static PurpleCipherOps SHA256Ops = {((void (*)(PurpleCipherContext *, const gchar *, void *))((void *)0)), ((void *(*)(PurpleCipherContext *, const gchar *))((void *)0)), (sha256_init), (sha256_reset), (purple_g_checksum_uninit), ((void (*)(PurpleCipherContext *, guchar *, size_t ))((void *)0)), (purple_g_checksum_append), (sha256_digest), ((int (*)(PurpleCipherContext *, const guchar *, size_t , guchar *, size_t *))((void *)0)), ((int (*)(PurpleCipherContext *, const guchar *, size_t , guchar *, size_t *))((void *)0)), ((void (*)(PurpleCipherContext *, guchar *))((void *)0)), ((size_t (*)(PurpleCipherContext *))((void *)0)), ((void (*)(PurpleCipherContext *, const guchar *))((void *)0)), ((size_t (*)(PurpleCipherContext *))((void *)0)), ((void (*)(PurpleCipherContext *, PurpleCipherBatchMode ))((void *)0)), ((PurpleCipherBatchMode (*)(PurpleCipherContext *))((void *)0)), (sha256_get_block_size), ((void (*)(PurpleCipherContext *, const guchar *, size_t ))((void *)0))};

PurpleCipherOps *purple_sha256_cipher_get_ops()
{
  return &SHA256Ops;
}
#endif /* GLIB_CHECK_VERSION(2,16,0) */
