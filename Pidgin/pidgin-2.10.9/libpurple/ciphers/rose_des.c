/*
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * Original des taken from gpg
 *
 * des.c - DES and Triple-DES encryption/decryption Algorithm
 *  Copyright (C) 1998 Free Software Foundation, Inc.
 *
 *  Please see below for more legal information!
 *
 *   According to the definition of DES in FIPS PUB 46-2 from December 1993.
 *   For a description of triple encryption, see:
 *     Bruce Schneier: Applied Cryptography. Second Edition.
 *     John Wiley & Sons, 1996. ISBN 0-471-12845-7. Pages 358 ff.
 *
 *   This file is part of GnuPG.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <cipher.h>
/******************************************************************************
 * DES
 *****************************************************************************/
typedef struct _des_ctx {
guint32 encrypt_subkeys[32UL];
guint32 decrypt_subkeys[32UL];}des_ctx[1UL];
/*
 *  The s-box values are permuted according to the 'primitive function P'
 */
static const guint32 sbox1[64UL] = {(0x00808200), (0), (0x00008000), (0x00808202), (0x00808002), (0x00008202), (2), (0x00008000), (0x00000200), (0x00808200), (0x00808202), (0x00000200), (0x00800202), (0x00808002), (0x00800000), (2), (0x00000202), (0x00800200), (0x00800200), (0x00008200), (0x00008200), (0x00808000), (0x00808000), (0x00800202), (0x00008002), (0x00800002), (0x00800002), (0x00008002), (0), (0x00000202), (0x00008202), (0x00800000), (0x00008000), (0x00808202), (2), (0x00808000), (0x00808200), (0x00800000), (0x00800000), (0x00000200), (0x00808002), (0x00008000), (0x00008200), (0x00800002), (0x00000200), (2), (0x00800202), (0x00008202), (0x00808202), (0x00008002), (0x00808000), (0x00800202), (0x00800002), (0x00000202), (0x00008202), (0x00808200), (0x00000202), (0x00800200), (0x00800200), (0), (0x00008002), (0x00008200), (0), (0x00808002)};
static const guint32 sbox2[64UL] = {(0x40084010), (0x40004000), (0x00004000), (0x00084010), (0x00080000), (16), (0x40080010), (0x40004010), (0x40000010), (0x40084010), (0x40084000), (0x40000000), (0x40004000), (0x00080000), (16), (0x40080010), (0x00084000), (0x00080010), (0x40004010), (0), (0x40000000), (0x00004000), (0x00084010), (0x40080000), (0x00080010), (0x40000010), (0), (0x00084000), (0x00004010), (0x40084000), (0x40080000), (0x00004010), (0), (0x00084010), (0x40080010), (0x00080000), (0x40004010), (0x40080000), (0x40084000), (0x00004000), (0x40080000), (0x40004000), (16), (0x40084010), (0x00084010), (16), (0x00004000), (0x40000000), (0x00004010), (0x40084000), (0x00080000), (0x40000010), (0x00080010), (0x40004010), (0x40000010), (0x00080010), (0x00084000), (0), (0x40004000), (0x00004010), (0x40000000), (0x40080010), (0x40084010), (0x00084000)};
static const guint32 sbox3[64UL] = {(0x00000104), (0x04010100), (0), (0x04010004), (0x04000100), (0), (0x00010104), (0x04000100), (0x00010004), (0x04000004), (0x04000004), (0x00010000), (0x04010104), (0x00010004), (0x04010000), (0x00000104), (0x04000000), (4), (0x04010100), (256), (0x00010100), (0x04010000), (0x04010004), (0x00010104), (0x04000104), (0x00010100), (0x00010000), (0x04000104), (4), (0x04010104), (256), (0x04000000), (0x04010100), (0x04000000), (0x00010004), (0x00000104), (0x00010000), (0x04010100), (0x04000100), (0), (256), (0x00010004), (0x04010104), (0x04000100), (0x04000004), (256), (0), (0x04010004), (0x04000104), (0x00010000), (0x04000000), (0x04010104), (4), (0x00010104), (0x00010100), (0x04000004), (0x04010000), (0x04000104), (0x00000104), (0x04010000), (0x00010104), (4), (0x04010004), (0x00010100)};
static const guint32 sbox4[64UL] = {(0x80401000), (0x80001040), (0x80001040), (64), (0x00401040), (0x80400040), (0x80400000), (0x80001000), (0), (0x00401000), (0x00401000), (0x80401040), (0x80000040), (0), (0x00400040), (0x80400000), (0x80000000), (0x00001000), (0x00400000), (0x80401000), (64), (0x00400000), (0x80001000), (0x00001040), (0x80400040), (0x80000000), (0x00001040), (0x00400040), (0x00001000), (0x00401040), (0x80401040), (0x80000040), (0x00400040), (0x80400000), (0x00401000), (0x80401040), (0x80000040), (0), (0), (0x00401000), (0x00001040), (0x00400040), (0x80400040), (0x80000000), (0x80401000), (0x80001040), (0x80001040), (64), (0x80401040), (0x80000040), (0x80000000), (0x00001000), (0x80400000), (0x80001000), (0x00401040), (0x80400040), (0x80001000), (0x00001040), (0x00400000), (0x80401000), (64), (0x00400000), (0x00001000), (0x00401040)};
static const guint32 sbox5[64UL] = {(128), (0x01040080), (0x01040000), (0x21000080), (0x00040000), (128), (0x20000000), (0x01040000), (0x20040080), (0x00040000), (0x01000080), (0x20040080), (0x21000080), (0x21040000), (0x00040080), (0x20000000), (0x01000000), (0x20040000), (0x20040000), (0), (0x20000080), (0x21040080), (0x21040080), (0x01000080), (0x21040000), (0x20000080), (0), (0x21000000), (0x01040080), (0x01000000), (0x21000000), (0x00040080), (0x00040000), (0x21000080), (128), (0x01000000), (0x20000000), (0x01040000), (0x21000080), (0x20040080), (0x01000080), (0x20000000), (0x21040000), (0x01040080), (0x20040080), (128), (0x01000000), (0x21040000), (0x21040080), (0x00040080), (0x21000000), (0x21040080), (0x01040000), (0), (0x20040000), (0x21000000), (0x00040080), (0x01000080), (0x20000080), (0x00040000), (0), (0x20040000), (0x01040080), (0x20000080)};
static const guint32 sbox6[64UL] = {(0x10000008), (0x10200000), (0x00002000), (0x10202008), (0x10200000), (8), (0x10202008), (0x00200000), (0x10002000), (0x00202008), (0x00200000), (0x10000008), (0x00200008), (0x10002000), (0x10000000), (0x00002008), (0), (0x00200008), (0x10002008), (0x00002000), (0x00202000), (0x10002008), (8), (0x10200008), (0x10200008), (0), (0x00202008), (0x10202000), (0x00002008), (0x00202000), (0x10202000), (0x10000000), (0x10002000), (8), (0x10200008), (0x00202000), (0x10202008), (0x00200000), (0x00002008), (0x10000008), (0x00200000), (0x10002000), (0x10000000), (0x00002008), (0x10000008), (0x10202008), (0x00202000), (0x10200000), (0x00202008), (0x10202000), (0), (0x10200008), (8), (0x00002000), (0x10200000), (0x00202008), (0x00002000), (0x00200008), (0x10002008), (0), (0x10202000), (0x10000000), (0x00200008), (0x10002008)};
static const guint32 sbox7[64UL] = {(0x00100000), (0x02100001), (0x02000401), (0), (1024), (0x02000401), (0x00100401), (0x02100400), (0x02100401), (0x00100000), (0), (0x02000001), (1), (0x02000000), (0x02100001), (0x00000401), (0x02000400), (0x00100401), (0x00100001), (0x02000400), (0x02000001), (0x02100000), (0x02100400), (0x00100001), (0x02100000), (1024), (0x00000401), (0x02100401), (0x00100400), (1), (0x02000000), (0x00100400), (0x02000000), (0x00100400), (0x00100000), (0x02000401), (0x02000401), (0x02100001), (0x02100001), (1), (0x00100001), (0x02000000), (0x02000400), (0x00100000), (0x02100400), (0x00000401), (0x00100401), (0x02100400), (0x00000401), (0x02000001), (0x02100401), (0x02100000), (0x00100400), (0), (1), (0x02100401), (0), (0x00100401), (0x02100000), (1024), (0x02000001), (0x02000400), (1024), (0x00100001)};
static const guint32 sbox8[64UL] = {(0x08000820), (0x00000800), (0x00020000), (0x08020820), (0x08000000), (0x08000820), (32), (0x08000000), (0x00020020), (0x08020000), (0x08020820), (0x00020800), (0x08020800), (0x00020820), (0x00000800), (32), (0x08020000), (0x08000020), (0x08000800), (0x00000820), (0x00020800), (0x00020020), (0x08020020), (0x08020800), (0x00000820), (0), (0), (0x08020020), (0x08000020), (0x08000800), (0x00020820), (0x00020000), (0x00020820), (0x00020000), (0x08020800), (0x00000800), (32), (0x08020020), (0x00000800), (0x00020820), (0x08000800), (32), (0x08000020), (0x08020000), (0x08020020), (0x08000000), (0x00020000), (0x08000820), (0), (0x08020820), (0x00020020), (0x08000020), (0x08020000), (0x08000800), (0x08000820), (0), (0x08020820), (0x00020800), (0x00020800), (0x00000820), (0x00000820), (0x00020020), (0x08000000), (0x08020800)};
/*
 *  * These two tables are part of the 'permuted choice 1' function.
 *   * In this implementation several speed improvements are done.
 *    */
static const guint32 leftkey_swap[16UL] = {(0), (1), (256), (0x00000101), (0x00010000), (0x00010001), (0x00010100), (0x00010101), (0x01000000), (0x01000001), (0x01000100), (0x01000101), (0x01010000), (0x01010001), (0x01010100), (0x01010101)};
static const guint32 rightkey_swap[16UL] = {(0), (0x01000000), (0x00010000), (0x01010000), (256), (0x01000100), (0x00010100), (0x01010100), (1), (0x01000001), (0x00010001), (0x01010001), (0x00000101), (0x01000101), (0x00010101), (0x01010101)};
/*
 *  Numbers of left shifts per round for encryption subkey schedule
 *  To calculate the decryption key scheduling we just reverse the
 *  ordering of the subkeys so we can omit the table for decryption
 *  subkey schedule.
 */
static const guint8 encrypt_rotate_tab[16UL] = {(1), (1), (2), (2), (2), (2), (2), (2), (1), (2), (2), (2), (2), (2), (2), (1)};
/*
 *  Macro to swap bits across two words
 **/
#define DO_PERMUTATION(a, temp, b, offset, mask)    \
	temp = ((a>>offset) ^ b) & mask;            \
	b ^= temp;                      \
	a ^= temp<<offset;
/*
 *  This performs the 'initial permutation' for the data to be encrypted or decrypted
 **/
#define INITIAL_PERMUTATION(left, temp, right)      \
	DO_PERMUTATION(left, temp, right, 4, 0x0f0f0f0f)    \
	DO_PERMUTATION(left, temp, right, 16, 0x0000ffff)   \
	DO_PERMUTATION(right, temp, left, 2, 0x33333333)    \
	DO_PERMUTATION(right, temp, left, 8, 0x00ff00ff)    \
	DO_PERMUTATION(left, temp, right, 1, 0x55555555)
/*
 * The 'inverse initial permutation'
 **/
#define FINAL_PERMUTATION(left, temp, right)        \
	DO_PERMUTATION(left, temp, right, 1, 0x55555555)    \
	DO_PERMUTATION(right, temp, left, 8, 0x00ff00ff)    \
	DO_PERMUTATION(right, temp, left, 2, 0x33333333)    \
	DO_PERMUTATION(left, temp, right, 16, 0x0000ffff)   \
	DO_PERMUTATION(left, temp, right, 4, 0x0f0f0f0f)
/*
 * A full DES round including 'expansion function', 'sbox substitution'
 * and 'primitive function P' but without swapping the left and right word.
 **/
#define DES_ROUND(from, to, work, subkey)       \
	work = ((from<<1) | (from>>31)) ^ *subkey++;    \
	to ^= sbox8[  work      & 0x3f ];           \
	to ^= sbox6[ (work>>8)  & 0x3f ];           \
	to ^= sbox4[ (work>>16) & 0x3f ];           \
	to ^= sbox2[ (work>>24) & 0x3f ];           \
	work = ((from>>3) | (from<<29)) ^ *subkey++;    \
	to ^= sbox7[  work      & 0x3f ];           \
	to ^= sbox5[ (work>>8)  & 0x3f ];           \
	to ^= sbox3[ (work>>16) & 0x3f ];           \
	to ^= sbox1[ (work>>24) & 0x3f ];
/*
 * Macros to convert 8 bytes from/to 32bit words
 **/
#define READ_64BIT_DATA(data, left, right)                  \
	left  = (data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3];   \
	right = (data[4] << 24) | (data[5] << 16) | (data[6] << 8) | data[7];
#define WRITE_64BIT_DATA(data, left, right)                 \
	data[0] = (left >> 24) &0xff; data[1] = (left >> 16) &0xff;         \
	data[2] = (left >> 8) &0xff; data[3] = left &0xff;              \
	data[4] = (right >> 24) &0xff; data[5] = (right >> 16) &0xff;       \
	data[6] = (right >> 8) &0xff; data[7] = right &0xff;
/*
 * des_key_schedule():    Calculate 16 subkeys pairs (even/odd) for
 *            16 encryption rounds.
 *            To calculate subkeys for decryption the caller
 *                have to reorder the generated subkeys.
 *
 *        rawkey:       8 Bytes of key data
 *        subkey:       Array of at least 32 guint32s. Will be filled
 *              with calculated subkeys.
 *
 **/

static void des_key_schedule(const guint8 *rawkey,guint32 *subkey)
{
  guint32 left;
  guint32 right;
  guint32 work;
  int round;
  left = ((((rawkey[0] << 24) | (rawkey[1] << 16)) | (rawkey[2] << 8)) | rawkey[3]);
  right = ((((rawkey[4] << 24) | (rawkey[5] << 16)) | (rawkey[6] << 8)) | rawkey[7]);
  work = (((right >> 4) ^ left) & 0x0f0f0f0f);
  left ^= work;
  right ^= (work << 4);
  work = (((right >> 0) ^ left) & 0x10101010);
  left ^= work;
  right ^= (work << 0);
  left = ((((((((leftkey_swap[(left >> 0) & 15] << 3) | (leftkey_swap[(left >> 8) & 15] << 2)) | (leftkey_swap[(left >> 16) & 15] << 1)) | leftkey_swap[(left >> 24) & 15]) | (leftkey_swap[(left >> 5) & 15] << 7)) | (leftkey_swap[(left >> 13) & 15] << 6)) | (leftkey_swap[(left >> 21) & 15] << 5)) | (leftkey_swap[(left >> 29) & 15] << 4));
  left &= 0x0fffffff;
  right = ((((((((rightkey_swap[(right >> 1) & 15] << 3) | (rightkey_swap[(right >> 9) & 15] << 2)) | (rightkey_swap[(right >> 17) & 15] << 1)) | rightkey_swap[(right >> 25) & 15]) | (rightkey_swap[(right >> 4) & 15] << 7)) | (rightkey_swap[(right >> 12) & 15] << 6)) | (rightkey_swap[(right >> 20) & 15] << 5)) | (rightkey_swap[(right >> 28) & 15] << 4));
  right &= 0x0fffffff;
  for (round = 0; round < 16; ++round) {
    left = (((left << encrypt_rotate_tab[round]) | (left >> (28 - encrypt_rotate_tab[round]))) & 0x0fffffff);
    right = (((right << encrypt_rotate_tab[round]) | (right >> (28 - encrypt_rotate_tab[round]))) & 0x0fffffff);
     *(subkey++) = (((((((((((((((((((((((left << 4) & 0x24000000) | ((left << 28) & 0x10000000)) | ((left << 14) & 0x08000000)) | ((left << 18) & 0x02080000)) | ((left << 6) & 0x01000000)) | ((left << 9) & 0x00200000)) | ((left >> 1) & 0x00100000)) | ((left << 10) & 0x00040000)) | ((left << 2) & 0x00020000)) | ((left >> 10) & 0x00010000)) | ((right >> 13) & 0x00002000)) | ((right >> 4) & 0x00001000)) | ((right << 6) & 0x00000800)) | ((right >> 1) & 1024)) | ((right >> 14) & 0x00000200)) | (right & 256)) | ((right >> 5) & 32)) | ((right >> 10) & 16)) | ((right >> 3) & 8)) | ((right >> 18) & 4)) | ((right >> 26) & 2)) | ((right >> 24) & 1));
     *(subkey++) = (((((((((((((((((((((((left << 15) & 0x20000000) | ((left << 17) & 0x10000000)) | ((left << 10) & 0x08000000)) | ((left << 22) & 0x04000000)) | ((left >> 2) & 0x02000000)) | ((left << 1) & 0x01000000)) | ((left << 16) & 0x00200000)) | ((left << 11) & 0x00100000)) | ((left << 3) & 0x00080000)) | ((left >> 6) & 0x00040000)) | ((left << 15) & 0x00020000)) | ((left >> 4) & 0x00010000)) | ((right >> 2) & 0x00002000)) | ((right << 8) & 0x00001000)) | ((right >> 14) & 0x00000808)) | ((right >> 9) & 1024)) | (right & 0x00000200)) | ((right << 7) & 256)) | ((right >> 7) & 32)) | ((right >> 3) & 17)) | ((right << 2) & 4)) | ((right >> 21) & 2));
  }
}
/*
 *  Fill a DES context with subkeys calculated from a 64bit key.
 *  Does not check parity bits, but simply ignore them.
 *  Does not check for weak keys.
 **/

static void des_set_key(PurpleCipherContext *context,const guchar *key)
{
  struct _des_ctx *ctx = (purple_cipher_context_get_data(context));
  int i;
  des_key_schedule(key,(ctx -> encrypt_subkeys));
  for (i = 0; i < 32; i += 2) {
    (ctx -> decrypt_subkeys)[i] = (ctx -> encrypt_subkeys)[30 - i];
    (ctx -> decrypt_subkeys)[i + 1] = (ctx -> encrypt_subkeys)[31 - i];
  }
}
/*
 *  Electronic Codebook Mode DES encryption/decryption of data according
 *  to 'mode'.
 **/

static int des_ecb_crypt(struct _des_ctx *ctx,const guint8 *from,guint8 *to,int mode)
{
  guint32 left;
  guint32 right;
  guint32 work;
  guint32 *keys;
  keys = ((mode != 0)?(ctx -> decrypt_subkeys) : (ctx -> encrypt_subkeys));
  left = ((((from[0] << 24) | (from[1] << 16)) | (from[2] << 8)) | from[3]);
  right = ((((from[4] << 24) | (from[5] << 16)) | (from[6] << 8)) | from[7]);
  work = (((left >> 4) ^ right) & 0x0f0f0f0f);
  right ^= work;
  left ^= (work << 4);
  work = (((left >> 16) ^ right) & 0x0000ffff);
  right ^= work;
  left ^= (work << 16);
  work = (((right >> 2) ^ left) & 0x33333333);
  left ^= work;
  right ^= (work << 2);
  work = (((right >> 8) ^ left) & 0x00ff00ff);
  left ^= work;
  right ^= (work << 8);
  work = (((left >> 1) ^ right) & 0x55555555);
  right ^= work;
  left ^= (work << 1);
  work = (((right << 1) | (right >> 31)) ^  *(keys++));
  left ^= sbox8[work & 0x3f];
  left ^= sbox6[(work >> 8) & 0x3f];
  left ^= sbox4[(work >> 16) & 0x3f];
  left ^= sbox2[(work >> 24) & 0x3f];
  work = (((right >> 3) | (right << 29)) ^  *(keys++));
  left ^= sbox7[work & 0x3f];
  left ^= sbox5[(work >> 8) & 0x3f];
  left ^= sbox3[(work >> 16) & 0x3f];
  left ^= sbox1[(work >> 24) & 0x3f];
  work = (((left << 1) | (left >> 31)) ^  *(keys++));
  right ^= sbox8[work & 0x3f];
  right ^= sbox6[(work >> 8) & 0x3f];
  right ^= sbox4[(work >> 16) & 0x3f];
  right ^= sbox2[(work >> 24) & 0x3f];
  work = (((left >> 3) | (left << 29)) ^  *(keys++));
  right ^= sbox7[work & 0x3f];
  right ^= sbox5[(work >> 8) & 0x3f];
  right ^= sbox3[(work >> 16) & 0x3f];
  right ^= sbox1[(work >> 24) & 0x3f];
  work = (((right << 1) | (right >> 31)) ^  *(keys++));
  left ^= sbox8[work & 0x3f];
  left ^= sbox6[(work >> 8) & 0x3f];
  left ^= sbox4[(work >> 16) & 0x3f];
  left ^= sbox2[(work >> 24) & 0x3f];
  work = (((right >> 3) | (right << 29)) ^  *(keys++));
  left ^= sbox7[work & 0x3f];
  left ^= sbox5[(work >> 8) & 0x3f];
  left ^= sbox3[(work >> 16) & 0x3f];
  left ^= sbox1[(work >> 24) & 0x3f];
  work = (((left << 1) | (left >> 31)) ^  *(keys++));
  right ^= sbox8[work & 0x3f];
  right ^= sbox6[(work >> 8) & 0x3f];
  right ^= sbox4[(work >> 16) & 0x3f];
  right ^= sbox2[(work >> 24) & 0x3f];
  work = (((left >> 3) | (left << 29)) ^  *(keys++));
  right ^= sbox7[work & 0x3f];
  right ^= sbox5[(work >> 8) & 0x3f];
  right ^= sbox3[(work >> 16) & 0x3f];
  right ^= sbox1[(work >> 24) & 0x3f];
  work = (((right << 1) | (right >> 31)) ^  *(keys++));
  left ^= sbox8[work & 0x3f];
  left ^= sbox6[(work >> 8) & 0x3f];
  left ^= sbox4[(work >> 16) & 0x3f];
  left ^= sbox2[(work >> 24) & 0x3f];
  work = (((right >> 3) | (right << 29)) ^  *(keys++));
  left ^= sbox7[work & 0x3f];
  left ^= sbox5[(work >> 8) & 0x3f];
  left ^= sbox3[(work >> 16) & 0x3f];
  left ^= sbox1[(work >> 24) & 0x3f];
  work = (((left << 1) | (left >> 31)) ^  *(keys++));
  right ^= sbox8[work & 0x3f];
  right ^= sbox6[(work >> 8) & 0x3f];
  right ^= sbox4[(work >> 16) & 0x3f];
  right ^= sbox2[(work >> 24) & 0x3f];
  work = (((left >> 3) | (left << 29)) ^  *(keys++));
  right ^= sbox7[work & 0x3f];
  right ^= sbox5[(work >> 8) & 0x3f];
  right ^= sbox3[(work >> 16) & 0x3f];
  right ^= sbox1[(work >> 24) & 0x3f];
  work = (((right << 1) | (right >> 31)) ^  *(keys++));
  left ^= sbox8[work & 0x3f];
  left ^= sbox6[(work >> 8) & 0x3f];
  left ^= sbox4[(work >> 16) & 0x3f];
  left ^= sbox2[(work >> 24) & 0x3f];
  work = (((right >> 3) | (right << 29)) ^  *(keys++));
  left ^= sbox7[work & 0x3f];
  left ^= sbox5[(work >> 8) & 0x3f];
  left ^= sbox3[(work >> 16) & 0x3f];
  left ^= sbox1[(work >> 24) & 0x3f];
  work = (((left << 1) | (left >> 31)) ^  *(keys++));
  right ^= sbox8[work & 0x3f];
  right ^= sbox6[(work >> 8) & 0x3f];
  right ^= sbox4[(work >> 16) & 0x3f];
  right ^= sbox2[(work >> 24) & 0x3f];
  work = (((left >> 3) | (left << 29)) ^  *(keys++));
  right ^= sbox7[work & 0x3f];
  right ^= sbox5[(work >> 8) & 0x3f];
  right ^= sbox3[(work >> 16) & 0x3f];
  right ^= sbox1[(work >> 24) & 0x3f];
  work = (((right << 1) | (right >> 31)) ^  *(keys++));
  left ^= sbox8[work & 0x3f];
  left ^= sbox6[(work >> 8) & 0x3f];
  left ^= sbox4[(work >> 16) & 0x3f];
  left ^= sbox2[(work >> 24) & 0x3f];
  work = (((right >> 3) | (right << 29)) ^  *(keys++));
  left ^= sbox7[work & 0x3f];
  left ^= sbox5[(work >> 8) & 0x3f];
  left ^= sbox3[(work >> 16) & 0x3f];
  left ^= sbox1[(work >> 24) & 0x3f];
  work = (((left << 1) | (left >> 31)) ^  *(keys++));
  right ^= sbox8[work & 0x3f];
  right ^= sbox6[(work >> 8) & 0x3f];
  right ^= sbox4[(work >> 16) & 0x3f];
  right ^= sbox2[(work >> 24) & 0x3f];
  work = (((left >> 3) | (left << 29)) ^  *(keys++));
  right ^= sbox7[work & 0x3f];
  right ^= sbox5[(work >> 8) & 0x3f];
  right ^= sbox3[(work >> 16) & 0x3f];
  right ^= sbox1[(work >> 24) & 0x3f];
  work = (((right << 1) | (right >> 31)) ^  *(keys++));
  left ^= sbox8[work & 0x3f];
  left ^= sbox6[(work >> 8) & 0x3f];
  left ^= sbox4[(work >> 16) & 0x3f];
  left ^= sbox2[(work >> 24) & 0x3f];
  work = (((right >> 3) | (right << 29)) ^  *(keys++));
  left ^= sbox7[work & 0x3f];
  left ^= sbox5[(work >> 8) & 0x3f];
  left ^= sbox3[(work >> 16) & 0x3f];
  left ^= sbox1[(work >> 24) & 0x3f];
  work = (((left << 1) | (left >> 31)) ^  *(keys++));
  right ^= sbox8[work & 0x3f];
  right ^= sbox6[(work >> 8) & 0x3f];
  right ^= sbox4[(work >> 16) & 0x3f];
  right ^= sbox2[(work >> 24) & 0x3f];
  work = (((left >> 3) | (left << 29)) ^  *(keys++));
  right ^= sbox7[work & 0x3f];
  right ^= sbox5[(work >> 8) & 0x3f];
  right ^= sbox3[(work >> 16) & 0x3f];
  right ^= sbox1[(work >> 24) & 0x3f];
  work = (((right << 1) | (right >> 31)) ^  *(keys++));
  left ^= sbox8[work & 0x3f];
  left ^= sbox6[(work >> 8) & 0x3f];
  left ^= sbox4[(work >> 16) & 0x3f];
  left ^= sbox2[(work >> 24) & 0x3f];
  work = (((right >> 3) | (right << 29)) ^  *(keys++));
  left ^= sbox7[work & 0x3f];
  left ^= sbox5[(work >> 8) & 0x3f];
  left ^= sbox3[(work >> 16) & 0x3f];
  left ^= sbox1[(work >> 24) & 0x3f];
  work = (((left << 1) | (left >> 31)) ^  *(keys++));
  right ^= sbox8[work & 0x3f];
  right ^= sbox6[(work >> 8) & 0x3f];
  right ^= sbox4[(work >> 16) & 0x3f];
  right ^= sbox2[(work >> 24) & 0x3f];
  work = (((left >> 3) | (left << 29)) ^  *(keys++));
  right ^= sbox7[work & 0x3f];
  right ^= sbox5[(work >> 8) & 0x3f];
  right ^= sbox3[(work >> 16) & 0x3f];
  right ^= sbox1[(work >> 24) & 0x3f];
  work = (((right << 1) | (right >> 31)) ^  *(keys++));
  left ^= sbox8[work & 0x3f];
  left ^= sbox6[(work >> 8) & 0x3f];
  left ^= sbox4[(work >> 16) & 0x3f];
  left ^= sbox2[(work >> 24) & 0x3f];
  work = (((right >> 3) | (right << 29)) ^  *(keys++));
  left ^= sbox7[work & 0x3f];
  left ^= sbox5[(work >> 8) & 0x3f];
  left ^= sbox3[(work >> 16) & 0x3f];
  left ^= sbox1[(work >> 24) & 0x3f];
  work = (((left << 1) | (left >> 31)) ^  *(keys++));
  right ^= sbox8[work & 0x3f];
  right ^= sbox6[(work >> 8) & 0x3f];
  right ^= sbox4[(work >> 16) & 0x3f];
  right ^= sbox2[(work >> 24) & 0x3f];
  work = (((left >> 3) | (left << 29)) ^  *(keys++));
  right ^= sbox7[work & 0x3f];
  right ^= sbox5[(work >> 8) & 0x3f];
  right ^= sbox3[(work >> 16) & 0x3f];
  right ^= sbox1[(work >> 24) & 0x3f];
  work = (((right >> 1) ^ left) & 0x55555555);
  left ^= work;
  right ^= (work << 1);
  work = (((left >> 8) ^ right) & 0x00ff00ff);
  right ^= work;
  left ^= (work << 8);
  work = (((left >> 2) ^ right) & 0x33333333);
  right ^= work;
  left ^= (work << 2);
  work = (((right >> 16) ^ left) & 0x0000ffff);
  left ^= work;
  right ^= (work << 16);
  work = (((right >> 4) ^ left) & 0x0f0f0f0f);
  left ^= work;
  right ^= (work << 4);
  to[0] = ((right >> 24) & 0xff);
  to[1] = ((right >> 16) & 0xff);
  to[2] = ((right >> 8) & 0xff);
  to[3] = (right & 0xff);
  to[4] = ((left >> 24) & 0xff);
  to[5] = ((left >> 16) & 0xff);
  to[6] = ((left >> 8) & 0xff);
  to[7] = (left & 0xff);
  return 0;
}

static gint des_encrypt(PurpleCipherContext *context,const guchar data[],size_t len,guchar output[],size_t *outlen)
{
  int offset = 0;
  int i = 0;
  int tmp;
  guint8 buf[8UL] = {(0), (0), (0), (0), (0), (0), (0), (0)};
  while((offset + 8) <= len){
    des_ecb_crypt((purple_cipher_context_get_data(context)),(data + offset),(output + offset),0);
    offset += 8;
  }
   *outlen = len;
  if (offset < len) {
     *outlen += (len - offset);
    tmp = offset;
    while(tmp < len){
      buf[i++] = data[tmp];
      tmp++;
    }
    des_ecb_crypt((purple_cipher_context_get_data(context)),buf,(output + offset),0);
  }
  return 0;
}

static gint des_decrypt(PurpleCipherContext *context,const guchar data[],size_t len,guchar output[],size_t *outlen)
{
  int offset = 0;
  int i = 0;
  int tmp;
  guint8 buf[8UL] = {(0), (0), (0), (0), (0), (0), (0), (0)};
  while((offset + 8) <= len){
    des_ecb_crypt((purple_cipher_context_get_data(context)),(data + offset),(output + offset),1);
    offset += 8;
  }
   *outlen = len;
  if (offset < len) {
     *outlen += (len - offset);
    tmp = offset;
    while(tmp < len){
      buf[i++] = data[tmp];
      tmp++;
    }
    des_ecb_crypt((purple_cipher_context_get_data(context)),buf,(output + offset),1);
  }
  return 0;
}

static void des_init(PurpleCipherContext *context,gpointer extra)
{
  struct _des_ctx *mctx;
  mctx = ((struct _des_ctx *)(g_malloc0_n(1,(sizeof(struct _des_ctx )))));
  purple_cipher_context_set_data(context,mctx);
}

static void des_uninit(PurpleCipherContext *context)
{
  struct _des_ctx *des_context;
  des_context = (purple_cipher_context_get_data(context));
  memset(des_context,0,(sizeof(( *des_context))));
  g_free(des_context);
  des_context = ((struct _des_ctx *)((void *)0));
}
static PurpleCipherOps DESOps = {((void (*)(PurpleCipherContext *, const gchar *, void *))((void *)0)), ((void *(*)(PurpleCipherContext *, const gchar *))((void *)0)), (des_init), ((void (*)(PurpleCipherContext *, void *))((void *)0)), (des_uninit), ((void (*)(PurpleCipherContext *, guchar *, size_t ))((void *)0)), ((void (*)(PurpleCipherContext *, const guchar *, size_t ))((void *)0)), ((gboolean (*)(PurpleCipherContext *, size_t , guchar *, size_t *))((void *)0)), (des_encrypt), (des_decrypt), ((void (*)(PurpleCipherContext *, guchar *))((void *)0)), ((size_t (*)(PurpleCipherContext *))((void *)0)), (des_set_key), ((size_t (*)(PurpleCipherContext *))((void *)0)), ((void (*)(PurpleCipherContext *, PurpleCipherBatchMode ))((void *)0)), ((PurpleCipherBatchMode (*)(PurpleCipherContext *))((void *)0)), ((size_t (*)(PurpleCipherContext *))((void *)0)), ((void (*)(PurpleCipherContext *, const guchar *, size_t ))((void *)0))
/* Set option */
/* Get option */
/* init */
/* reset */
/* uninit */
/* set iv */
/* append */
/* digest */
/* encrypt */
/* decrypt */
/* set salt */
/* get salt size */
/* set key */
/* get key size */
/* set batch mode */
/* get batch mode */
/* get block size */
/* set key with len */
};
/******************************************************************************
 * Triple-DES
 *****************************************************************************/
typedef struct _des3_ctx {
PurpleCipherBatchMode mode;
guchar iv[8UL];
/* First key for encryption */
struct _des_ctx key1;
/* Second key for decryption */
struct _des_ctx key2;
/* Third key for encryption */
struct _des_ctx key3;}des3_ctx[1UL];
/*
 *  Fill a DES3 context with subkeys calculated from 3 64bit key.
 *  Does not check parity bits, but simply ignore them.
 *  Does not check for weak keys.
 **/

static void des3_set_key(PurpleCipherContext *context,const guchar *key)
{
  struct _des3_ctx *ctx = (purple_cipher_context_get_data(context));
  int i;
  des_key_schedule((key + 0),ctx -> key1.encrypt_subkeys);
  des_key_schedule((key + 8),ctx -> key2.encrypt_subkeys);
  des_key_schedule((key + 16),ctx -> key3.encrypt_subkeys);
  for (i = 0; i < 32; i += 2) {
    ctx -> key1.decrypt_subkeys[i] = ctx -> key1.encrypt_subkeys[30 - i];
    ctx -> key1.decrypt_subkeys[i + 1] = ctx -> key1.encrypt_subkeys[31 - i];
    ctx -> key2.decrypt_subkeys[i] = ctx -> key2.encrypt_subkeys[30 - i];
    ctx -> key2.decrypt_subkeys[i + 1] = ctx -> key2.encrypt_subkeys[31 - i];
    ctx -> key3.decrypt_subkeys[i] = ctx -> key3.encrypt_subkeys[30 - i];
    ctx -> key3.decrypt_subkeys[i + 1] = ctx -> key3.encrypt_subkeys[31 - i];
  }
}

static gint des3_ecb_encrypt(struct _des3_ctx *ctx,const guchar data[],size_t len,guchar output[],size_t *outlen)
{
  int offset = 0;
  int i = 0;
  int tmp;
  guint8 buf[8UL] = {(0), (0), (0), (0), (0), (0), (0), (0)};
  while((offset + 8) <= len){
    des_ecb_crypt(&ctx -> key1,(data + offset),(output + offset),0);
    des_ecb_crypt(&ctx -> key2,(output + offset),buf,1);
    des_ecb_crypt(&ctx -> key3,buf,(output + offset),0);
    offset += 8;
  }
   *outlen = len;
  if (offset < len) {
     *outlen += (len - offset);
    tmp = offset;
    memset(buf,0,8);
    while(tmp < len){
      buf[i++] = data[tmp];
      tmp++;
    }
    des_ecb_crypt(&ctx -> key1,buf,(output + offset),0);
    des_ecb_crypt(&ctx -> key2,(output + offset),buf,1);
    des_ecb_crypt(&ctx -> key3,buf,(output + offset),0);
  }
  return 0;
}

static gint des3_cbc_encrypt(struct _des3_ctx *ctx,const guchar data[],size_t len,guchar output[],size_t *outlen)
{
  int offset = 0;
  int i = 0;
  int tmp;
  guint8 buf[8UL];
  memcpy(buf,(ctx -> iv),8);
  while((offset + 8) <= len){
    for (i = 0; i < 8; i++) 
      buf[i] ^= data[offset + i];
    des_ecb_crypt(&ctx -> key1,buf,(output + offset),0);
    des_ecb_crypt(&ctx -> key2,(output + offset),buf,1);
    des_ecb_crypt(&ctx -> key3,buf,(output + offset),0);
    memcpy(buf,(output + offset),8);
    offset += 8;
  }
   *outlen = len;
  if (offset < len) {
     *outlen += (len - offset);
    tmp = offset;
    i = 0;
    while(tmp < len){
      buf[i++] ^= data[tmp];
      tmp++;
    }
    des_ecb_crypt(&ctx -> key1,buf,(output + offset),0);
    des_ecb_crypt(&ctx -> key2,(output + offset),buf,1);
    des_ecb_crypt(&ctx -> key3,buf,(output + offset),0);
  }
  return 0;
}

static gint des3_encrypt(PurpleCipherContext *context,const guchar data[],size_t len,guchar output[],size_t *outlen)
{
  struct _des3_ctx *ctx = (purple_cipher_context_get_data(context));
  if ((ctx -> mode) == PURPLE_CIPHER_BATCH_MODE_ECB) {
    return des3_ecb_encrypt(ctx,data,len,output,outlen);
  }
  else if ((ctx -> mode) == PURPLE_CIPHER_BATCH_MODE_CBC) {
    return des3_cbc_encrypt(ctx,data,len,output,outlen);
  }
  else {
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","des.c",637,((const char *)__func__));
      return 0;
    }while (0);
  }
  return 0;
}

static gint des3_ecb_decrypt(struct _des3_ctx *ctx,const guchar data[],size_t len,guchar output[],size_t *outlen)
{
  int offset = 0;
  int i = 0;
  int tmp;
  guint8 buf[8UL] = {(0), (0), (0), (0), (0), (0), (0), (0)};
  while((offset + 8) <= len){
/* NOTE: Apply key in reverse */
    des_ecb_crypt(&ctx -> key3,(data + offset),(output + offset),1);
    des_ecb_crypt(&ctx -> key2,(output + offset),buf,0);
    des_ecb_crypt(&ctx -> key1,buf,(output + offset),1);
    offset += 8;
  }
   *outlen = len;
  if (offset < len) {
     *outlen += (len - offset);
    tmp = offset;
    memset(buf,0,8);
    while(tmp < len){
      buf[i++] = data[tmp];
      tmp++;
    }
    des_ecb_crypt(&ctx -> key3,buf,(output + offset),1);
    des_ecb_crypt(&ctx -> key2,(output + offset),buf,0);
    des_ecb_crypt(&ctx -> key1,buf,(output + offset),1);
  }
  return 0;
}

static gint des3_cbc_decrypt(struct _des3_ctx *ctx,const guchar data[],size_t len,guchar output[],size_t *outlen)
{
  int offset = 0;
  int i = 0;
  int tmp;
  guint8 buf[8UL] = {(0), (0), (0), (0), (0), (0), (0), (0)};
  guint8 link[8UL];
  memcpy(link,(ctx -> iv),8);
  while((offset + 8) <= len){
    des_ecb_crypt(&ctx -> key3,(data + offset),(output + offset),1);
    des_ecb_crypt(&ctx -> key2,(output + offset),buf,0);
    des_ecb_crypt(&ctx -> key1,buf,(output + offset),1);
    for (i = 0; i < 8; i++) 
      output[offset + i] ^= link[i];
    memcpy(link,(data + offset),8);
    offset += 8;
  }
   *outlen = len;
  if (offset < len) {
     *outlen += (len - offset);
    tmp = offset;
    memset(buf,0,8);
    i = 0;
    while(tmp < len){
      buf[i++] = data[tmp];
      tmp++;
    }
    des_ecb_crypt(&ctx -> key3,buf,(output + offset),1);
    des_ecb_crypt(&ctx -> key2,(output + offset),buf,0);
    des_ecb_crypt(&ctx -> key1,buf,(output + offset),1);
    for (i = 0; i < 8; i++) 
      output[offset + i] ^= link[i];
  }
  return 0;
}

static gint des3_decrypt(PurpleCipherContext *context,const guchar data[],size_t len,guchar output[],size_t *outlen)
{
  struct _des3_ctx *ctx = (purple_cipher_context_get_data(context));
  if ((ctx -> mode) == PURPLE_CIPHER_BATCH_MODE_ECB) {
    return des3_ecb_decrypt(ctx,data,len,output,outlen);
  }
  else if ((ctx -> mode) == PURPLE_CIPHER_BATCH_MODE_CBC) {
    return des3_cbc_decrypt(ctx,data,len,output,outlen);
  }
  else {
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","des.c",759,((const char *)__func__));
      return 0;
    }while (0);
  }
  return 0;
}

static void des3_set_batch(PurpleCipherContext *context,PurpleCipherBatchMode mode)
{
  struct _des3_ctx *ctx = (purple_cipher_context_get_data(context));
  ctx -> mode = mode;
}

static PurpleCipherBatchMode des3_get_batch(PurpleCipherContext *context)
{
  struct _des3_ctx *ctx = (purple_cipher_context_get_data(context));
  return ctx -> mode;
}

static void des3_set_iv(PurpleCipherContext *context,guchar *iv,size_t len)
{
  struct _des3_ctx *ctx;
  do {
    if (len == 8) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"len == 8");
      return ;
    };
  }while (0);
  ctx = (purple_cipher_context_get_data(context));
  memcpy((ctx -> iv),iv,len);
}

static void des3_init(PurpleCipherContext *context,gpointer extra)
{
  struct _des3_ctx *mctx;
  mctx = ((struct _des3_ctx *)(g_malloc0_n(1,(sizeof(struct _des3_ctx )))));
  purple_cipher_context_set_data(context,mctx);
}

static void des3_uninit(PurpleCipherContext *context)
{
  struct _des3_ctx *des3_context;
  des3_context = (purple_cipher_context_get_data(context));
  memset(des3_context,0,(sizeof(( *des3_context))));
  g_free(des3_context);
  des3_context = ((struct _des3_ctx *)((void *)0));
}
static PurpleCipherOps DES3Ops = {((void (*)(PurpleCipherContext *, const gchar *, void *))((void *)0)), ((void *(*)(PurpleCipherContext *, const gchar *))((void *)0)), (des3_init), ((void (*)(PurpleCipherContext *, void *))((void *)0)), (des3_uninit), (des3_set_iv), ((void (*)(PurpleCipherContext *, const guchar *, size_t ))((void *)0)), ((gboolean (*)(PurpleCipherContext *, size_t , guchar *, size_t *))((void *)0)), (des3_encrypt), (des3_decrypt), ((void (*)(PurpleCipherContext *, guchar *))((void *)0)), ((size_t (*)(PurpleCipherContext *))((void *)0)), (des3_set_key), ((size_t (*)(PurpleCipherContext *))((void *)0)), (des3_set_batch), (des3_get_batch), ((size_t (*)(PurpleCipherContext *))((void *)0)), ((void (*)(PurpleCipherContext *, const guchar *, size_t ))((void *)0))
/* Set option */
/* Get option */
/* init */
/* reset */
/* uninit */
/* set iv */
/* append */
/* digest */
/* encrypt */
/* decrypt */
/* set salt */
/* get salt size */
/* set key */
/* get key size */
/* set batch mode */
/* get batch mode */
/* get block size */
/* set key with len */
};
/******************************************************************************
 * Registration
 *****************************************************************************/

PurpleCipherOps *purple_des_cipher_get_ops()
{
  return &DESOps;
}

PurpleCipherOps *purple_des3_cipher_get_ops()
{
  return &DES3Ops;
}
