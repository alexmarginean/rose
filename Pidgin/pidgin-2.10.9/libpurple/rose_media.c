/**
 * @file media.c Media API
 * @ingroup core
 */
/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "account.h"
#include "media.h"
#include "media/backend-iface.h"
#include "mediamanager.h"
#include "debug.h"
#ifdef USE_GSTREAMER
#include "media/backend-fs2.h"
#include "marshallers.h"
#include "media-gst.h"
#endif
#ifdef USE_VV
/** @copydoc _PurpleMediaSession */
typedef struct _PurpleMediaSession PurpleMediaSession;
/** @copydoc _PurpleMediaStream */
typedef struct _PurpleMediaStream PurpleMediaStream;
/** @copydoc _PurpleMediaClass */
typedef struct _PurpleMediaClass PurpleMediaClass;
/** @copydoc _PurpleMediaPrivate */
typedef struct _PurpleMediaPrivate PurpleMediaPrivate;
/** The media class */

struct _PurpleMediaClass 
{
/**< The parent class. */
  GObjectClass parent_class;
}
;
/** The media class's private data */

struct _PurpleMedia 
{
/**< The parent of this object. */
  GObject parent;
/**< The private data of this object. */
  PurpleMediaPrivate *priv;
}
;

struct _PurpleMediaSession 
{
  gchar *id;
  PurpleMedia *media;
  PurpleMediaSessionType type;
  gboolean initiator;
}
;

struct _PurpleMediaStream 
{
  PurpleMediaSession *session;
  gchar *participant;
  GList *local_candidates;
  GList *remote_candidates;
  gboolean initiator;
  gboolean accepted;
  gboolean candidates_prepared;
  GList *active_local_candidates;
  GList *active_remote_candidates;
}
;
#endif

struct _PurpleMediaPrivate 
{
#ifdef USE_VV
  PurpleMediaManager *manager;
  PurpleAccount *account;
  PurpleMediaBackend *backend;
  gchar *conference_type;
  gboolean initiator;
  gpointer prpl_data;
/* PurpleMediaSession table */
  GHashTable *sessions;
  GList *participants;
/* PurpleMediaStream table */
  GList *streams;
#else
#endif
}
;
#ifdef USE_VV
#define PURPLE_MEDIA_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE((obj), PURPLE_TYPE_MEDIA, PurpleMediaPrivate))
static void purple_media_class_init(PurpleMediaClass *klass);
static void purple_media_init(PurpleMedia *media);
static void purple_media_dispose(GObject *object);
static void purple_media_finalize(GObject *object);
static void purple_media_get_property(GObject *object,guint prop_id,GValue *value,GParamSpec *pspec);
static void purple_media_set_property(GObject *object,guint prop_id,const GValue *value,GParamSpec *pspec);
static void purple_media_new_local_candidate_cb(PurpleMediaBackend *backend,const gchar *sess_id,const gchar *participant,PurpleMediaCandidate *candidate,PurpleMedia *media);
static void purple_media_candidates_prepared_cb(PurpleMediaBackend *backend,const gchar *sess_id,const gchar *name,PurpleMedia *media);
static void purple_media_candidate_pair_established_cb(PurpleMediaBackend *backend,const gchar *sess_id,const gchar *name,PurpleMediaCandidate *local_candidate,PurpleMediaCandidate *remote_candidate,PurpleMedia *media);
static void purple_media_codecs_changed_cb(PurpleMediaBackend *backend,const gchar *sess_id,PurpleMedia *media);
static GObjectClass *parent_class = (GObjectClass *)((void *)0);
enum __unnamed_enum___F0_L136_C1_S_ERROR__COMMA__CANDIDATES_PREPARED__COMMA__CODECS_CHANGED__COMMA__LEVEL__COMMA__NEW_CANDIDATE__COMMA__STATE_CHANGED__COMMA__STREAM_INFO__COMMA__LAST_SIGNAL {S_ERROR,CANDIDATES_PREPARED,CODECS_CHANGED,LEVEL,NEW_CANDIDATE,STATE_CHANGED,STREAM_INFO,LAST_SIGNAL};
static guint purple_media_signals[7UL] = {(0)};
enum __unnamed_enum___F0_L148_C1_PROP_0__COMMA__PROP_MANAGER__COMMA__PROP_BACKEND__COMMA__PROP_ACCOUNT__COMMA__PROP_CONFERENCE_TYPE__COMMA__PROP_INITIATOR__COMMA__PROP_PRPL_DATA {PROP_0,PROP_MANAGER,PROP_BACKEND,PROP_ACCOUNT,PROP_CONFERENCE_TYPE,PROP_INITIATOR,PROP_PRPL_DATA};
#endif

GType purple_media_get_type()
{
#ifdef USE_VV
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(PurpleMediaClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )purple_media_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(PurpleMedia ))), (0), ((GInstanceInitFunc )purple_media_init), ((const GTypeValueTable *)((void *)0))};
    type = g_type_register_static(((GType )(20 << 2)),"PurpleMedia",&info,0);
  }
  return type;
#else
#endif
}
#ifdef USE_VV

static void purple_media_class_init(PurpleMediaClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *)klass;
  parent_class = (g_type_class_peek_parent(klass));
  gobject_class -> dispose = purple_media_dispose;
  gobject_class -> finalize = purple_media_finalize;
  gobject_class -> set_property = purple_media_set_property;
  gobject_class -> get_property = purple_media_get_property;
  g_object_class_install_property(gobject_class,PROP_MANAGER,g_param_spec_object("manager","Purple Media Manager","The media manager that contains this media session.",purple_media_manager_get_type(),(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
/*
	 * This one should be PURPLE_TYPE_MEDIA_BACKEND, but it doesn't
	 * like interfaces because they "aren't GObjects"
	 */
  g_object_class_install_property(gobject_class,PROP_BACKEND,g_param_spec_object("backend","Purple Media Backend","The backend object this media object uses.",((GType )(20 << 2)),G_PARAM_READABLE));
  g_object_class_install_property(gobject_class,PROP_ACCOUNT,g_param_spec_pointer("account","PurpleAccount","The account this media session is on.",(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_class_install_property(gobject_class,PROP_CONFERENCE_TYPE,g_param_spec_string("conference-type","Conference Type","The type of conference that this media object has been created to provide.",0,(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_class_install_property(gobject_class,PROP_INITIATOR,g_param_spec_boolean("initiator","initiator","If the local user initiated the conference.",0,(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_class_install_property(gobject_class,PROP_PRPL_DATA,g_param_spec_pointer("prpl-data","gpointer","Data the prpl plugin set on the media session.",(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  purple_media_signals[S_ERROR] = g_signal_new("error",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,0,0,0,g_cclosure_marshal_VOID__STRING,((GType )(1 << 2)),1,((GType )(16 << 2)));
  purple_media_signals[CANDIDATES_PREPARED] = g_signal_new("candidates-prepared",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,0,0,0,purple_smarshal_VOID__STRING_STRING,((GType )(1 << 2)),2,((GType )(16 << 2)),((GType )(16 << 2)));
  purple_media_signals[CODECS_CHANGED] = g_signal_new("codecs-changed",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,0,0,0,g_cclosure_marshal_VOID__STRING,((GType )(1 << 2)),1,((GType )(16 << 2)));
  purple_media_signals[LEVEL] = g_signal_new("level",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,0,0,0,purple_smarshal_VOID__STRING_STRING_DOUBLE,((GType )(1 << 2)),3,((GType )(16 << 2)),((GType )(16 << 2)),((GType )(15 << 2)));
  purple_media_signals[NEW_CANDIDATE] = g_signal_new("new-candidate",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,0,0,0,purple_smarshal_VOID__POINTER_POINTER_OBJECT,((GType )(1 << 2)),3,((GType )(17 << 2)),((GType )(17 << 2)),purple_media_candidate_get_type());
  purple_media_signals[STATE_CHANGED] = g_signal_new("state-changed",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,0,0,0,purple_smarshal_VOID__ENUM_STRING_STRING,((GType )(1 << 2)),3,purple_media_state_changed_get_type(),((GType )(16 << 2)),((GType )(16 << 2)));
  purple_media_signals[STREAM_INFO] = g_signal_new("stream-info",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,0,0,0,purple_smarshal_VOID__ENUM_STRING_STRING_BOOLEAN,((GType )(1 << 2)),4,purple_media_info_type_get_type(),((GType )(16 << 2)),((GType )(16 << 2)),((GType )(5 << 2)));
  g_type_class_add_private(klass,(sizeof(PurpleMediaPrivate )));
}

static void purple_media_init(PurpleMedia *media)
{
  media -> priv = ((PurpleMediaPrivate *)(g_type_instance_get_private(((GTypeInstance *)media),purple_media_get_type())));
  memset((media -> priv),0,(sizeof(( *(media -> priv)))));
}

static void purple_media_stream_free(PurpleMediaStream *stream)
{
  if (stream == ((PurpleMediaStream *)((void *)0))) 
    return ;
  g_free((stream -> participant));
  if ((stream -> local_candidates) != 0) 
    purple_media_candidate_list_free((stream -> local_candidates));
  if ((stream -> remote_candidates) != 0) 
    purple_media_candidate_list_free((stream -> remote_candidates));
  if ((stream -> active_local_candidates) != 0) 
    purple_media_candidate_list_free((stream -> active_local_candidates));
  if ((stream -> active_remote_candidates) != 0) 
    purple_media_candidate_list_free((stream -> active_remote_candidates));
  g_free(stream);
}

static void purple_media_session_free(PurpleMediaSession *session)
{
  if (session == ((PurpleMediaSession *)((void *)0))) 
    return ;
  g_free((session -> id));
  g_free(session);
}

static void purple_media_dispose(GObject *media)
{
  PurpleMediaPrivate *priv = (PurpleMediaPrivate *)(g_type_instance_get_private(((GTypeInstance *)media),purple_media_get_type()));
  purple_debug_info("media","purple_media_dispose\n");
  purple_media_manager_remove_media((priv -> manager),((PurpleMedia *)(g_type_check_instance_cast(((GTypeInstance *)media),purple_media_get_type()))));
  if ((priv -> backend) != 0) {
    g_object_unref((priv -> backend));
    priv -> backend = ((PurpleMediaBackend *)((void *)0));
  }
  if ((priv -> manager) != 0) {
    g_object_unref((priv -> manager));
    priv -> manager = ((PurpleMediaManager *)((void *)0));
  }
  ( *( *((GObjectClass *)(g_type_check_class_cast(((GTypeClass *)parent_class),((GType )(20 << 2)))))).dispose)(media);
}

static void purple_media_finalize(GObject *media)
{
  PurpleMediaPrivate *priv = (PurpleMediaPrivate *)(g_type_instance_get_private(((GTypeInstance *)media),purple_media_get_type()));
  purple_debug_info("media","purple_media_finalize\n");
  for (; (priv -> streams) != 0; priv -> streams = g_list_delete_link((priv -> streams),(priv -> streams))) 
    purple_media_stream_free(( *(priv -> streams)).data);
  for (; (priv -> participants) != 0; priv -> participants = g_list_delete_link((priv -> participants),(priv -> participants))) 
    g_free(( *(priv -> participants)).data);
  if ((priv -> sessions) != 0) {
    GList *sessions = g_hash_table_get_values((priv -> sessions));
    for (; sessions != 0; sessions = g_list_delete_link(sessions,sessions)) {
      purple_media_session_free((sessions -> data));
    }
    g_hash_table_destroy((priv -> sessions));
  }
  ( *( *((GObjectClass *)(g_type_check_class_cast(((GTypeClass *)parent_class),((GType )(20 << 2)))))).finalize)(media);
}

static void purple_media_set_property(GObject *object,guint prop_id,const GValue *value,GParamSpec *pspec)
{
  PurpleMedia *media;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(object)");
      return ;
    };
  }while (0);
  media = ((PurpleMedia *)(g_type_check_instance_cast(((GTypeInstance *)object),purple_media_get_type())));
  switch(prop_id){
    case PROP_MANAGER:
{
      ( *(media -> priv)).manager = (g_value_dup_object(value));
      break; 
    }
    case PROP_ACCOUNT:
{
      ( *(media -> priv)).account = (g_value_get_pointer(value));
      break; 
    }
    case PROP_CONFERENCE_TYPE:
{
      ( *(media -> priv)).conference_type = g_value_dup_string(value);
      ( *(media -> priv)).backend = (g_object_new(purple_media_manager_get_backend_type(purple_media_manager_get()),"conference-type",( *(media -> priv)).conference_type,"media",media,((void *)((void *)0))));
      g_signal_connect_data(( *(media -> priv)).backend,"active-candidate-pair",((GCallback )purple_media_candidate_pair_established_cb),media,0,((GConnectFlags )0));
      g_signal_connect_data(( *(media -> priv)).backend,"candidates-prepared",((GCallback )purple_media_candidates_prepared_cb),media,0,((GConnectFlags )0));
      g_signal_connect_data(( *(media -> priv)).backend,"codecs-changed",((GCallback )purple_media_codecs_changed_cb),media,0,((GConnectFlags )0));
      g_signal_connect_data(( *(media -> priv)).backend,"new-candidate",((GCallback )purple_media_new_local_candidate_cb),media,0,((GConnectFlags )0));
      break; 
    }
    case PROP_INITIATOR:
{
      ( *(media -> priv)).initiator = g_value_get_boolean(value);
      break; 
    }
    case PROP_PRPL_DATA:
{
      ( *(media -> priv)).prpl_data = g_value_get_pointer(value);
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = prop_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","media.c:420","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void purple_media_get_property(GObject *object,guint prop_id,GValue *value,GParamSpec *pspec)
{
  PurpleMedia *media;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(object)");
      return ;
    };
  }while (0);
  media = ((PurpleMedia *)(g_type_check_instance_cast(((GTypeInstance *)object),purple_media_get_type())));
  switch(prop_id){
    case PROP_MANAGER:
{
      g_value_set_object(value,( *(media -> priv)).manager);
      break; 
    }
    case PROP_BACKEND:
{
      g_value_set_object(value,( *(media -> priv)).backend);
      break; 
    }
    case PROP_ACCOUNT:
{
      g_value_set_pointer(value,( *(media -> priv)).account);
      break; 
    }
    case PROP_CONFERENCE_TYPE:
{
      g_value_set_string(value,( *(media -> priv)).conference_type);
      break; 
    }
    case PROP_INITIATOR:
{
      g_value_set_boolean(value,( *(media -> priv)).initiator);
      break; 
    }
    case PROP_PRPL_DATA:
{
      g_value_set_pointer(value,( *(media -> priv)).prpl_data);
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = prop_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","media.c:454","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static PurpleMediaSession *purple_media_get_session(PurpleMedia *media,const gchar *sess_id)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  return ((((PurpleMediaSession *)( *(media -> priv)).sessions) != 0)?g_hash_table_lookup(( *(media -> priv)).sessions,sess_id) : ((void *)((void *)0)));
}

static PurpleMediaStream *purple_media_get_stream(PurpleMedia *media,const gchar *session,const gchar *participant)
{
  GList *streams;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  streams = ( *(media -> priv)).streams;
  for (; streams != 0; streams = ((streams != 0)?( *((GList *)streams)).next : ((struct _GList *)((void *)0)))) {
    PurpleMediaStream *stream = (streams -> data);
    if (!(strcmp(( *(stream -> session)).id,session) != 0) && !(strcmp((stream -> participant),participant) != 0)) 
      return stream;
  }
  return 0;
}

static GList *purple_media_get_streams(PurpleMedia *media,const gchar *session,const gchar *participant)
{
  GList *streams;
  GList *ret = (GList *)((void *)0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  streams = ( *(media -> priv)).streams;
  for (; streams != 0; streams = ((streams != 0)?( *((GList *)streams)).next : ((struct _GList *)((void *)0)))) {
    PurpleMediaStream *stream = (streams -> data);
    if (((session == ((const gchar *)((void *)0))) || !(strcmp(( *(stream -> session)).id,session) != 0)) && ((participant == ((const gchar *)((void *)0))) || !(strcmp((stream -> participant),participant) != 0))) 
      ret = g_list_append(ret,stream);
  }
  return ret;
}

static void purple_media_add_session(PurpleMedia *media,PurpleMediaSession *session)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return ;
    };
  }while (0);
  do {
    if (session != ((PurpleMediaSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return ;
    };
  }while (0);
  if (!(( *(media -> priv)).sessions != 0)) {
    purple_debug_info("media","Creating hash table for sessions\n");
    ( *(media -> priv)).sessions = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,0);
  }
  g_hash_table_insert(( *(media -> priv)).sessions,(g_strdup((session -> id))),session);
}
#if 0
#endif

static PurpleMediaStream *purple_media_insert_stream(PurpleMediaSession *session,const gchar *name,gboolean initiator)
{
  PurpleMediaStream *media_stream;
  do {
    if (session != ((PurpleMediaSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return 0;
    };
  }while (0);
  media_stream = ((PurpleMediaStream *)(g_malloc0_n(1,(sizeof(PurpleMediaStream )))));
  media_stream -> participant = g_strdup(name);
  media_stream -> session = session;
  media_stream -> initiator = initiator;
  ( *( *(session -> media)).priv).streams = g_list_append(( *( *(session -> media)).priv).streams,media_stream);
  return media_stream;
}

static void purple_media_insert_local_candidate(PurpleMediaSession *session,const gchar *name,PurpleMediaCandidate *candidate)
{
  PurpleMediaStream *stream;
  do {
    if (session != ((PurpleMediaSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return ;
    };
  }while (0);
  stream = purple_media_get_stream((session -> media),(session -> id),name);
  stream -> local_candidates = g_list_append((stream -> local_candidates),candidate);
}
#endif

GList *purple_media_get_session_ids(PurpleMedia *media)
{
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  return (( *(media -> priv)).sessions != ((GHashTable *)((void *)0)))?g_hash_table_get_keys(( *(media -> priv)).sessions) : ((struct _GList *)((void *)0));
#else
#endif
}
#ifdef USE_GSTREAMER

GstElement *purple_media_get_src(PurpleMedia *media,const gchar *sess_id)
{
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  if ((({
    GTypeInstance *__inst = (GTypeInstance *)( *(media -> priv)).backend;
    GType __t = purple_media_backend_fs2_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
    return purple_media_backend_fs2_get_src(((PurpleMediaBackendFs2 *)(g_type_check_instance_cast(((GTypeInstance *)( *(media -> priv)).backend),purple_media_backend_fs2_get_type()))),sess_id);
  do {
    g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","media.c",589,((const char *)__func__));
    return 0;
  }while (0);
#else
#endif
}
#endif /* USE_GSTREAMER */

PurpleAccount *purple_media_get_account(PurpleMedia *media)
{
#ifdef USE_VV
  PurpleAccount *account;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  g_object_get(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)media),((GType )(20 << 2))))),"account",&account,((void *)((void *)0)));
  return account;
#else
#endif
}

gpointer purple_media_get_prpl_data(PurpleMedia *media)
{
#ifdef USE_VV
  gpointer prpl_data;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  g_object_get(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)media),((GType )(20 << 2))))),"prpl-data",&prpl_data,((void *)((void *)0)));
  return prpl_data;
#else
#endif
}

void purple_media_set_prpl_data(PurpleMedia *media,gpointer prpl_data)
{
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return ;
    };
  }while (0);
  g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)media),((GType )(20 << 2))))),"prpl-data",prpl_data,((void *)((void *)0)));
#endif
}

void purple_media_error(PurpleMedia *media,const gchar *error,... )
{
#ifdef USE_VV
  va_list args;
  gchar *message;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return ;
    };
  }while (0);
  va_start(args,error);
  message = g_strdup_vprintf(error,args);
  va_end(args);
  purple_debug_error("media","%s\n",message);
  g_signal_emit(media,purple_media_signals[S_ERROR],0,message);
  g_free(message);
#endif
}

void purple_media_end(PurpleMedia *media,const gchar *session_id,const gchar *participant)
{
#ifdef USE_VV
  GList *iter;
  GList *sessions = (GList *)((void *)0);
  GList *participants = (GList *)((void *)0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return ;
    };
  }while (0);
  iter = purple_media_get_streams(media,session_id,participant);
/* Free matching streams */
  for (; iter != 0; iter = g_list_delete_link(iter,iter)) {
    PurpleMediaStream *stream = (iter -> data);
    g_signal_emit(media,purple_media_signals[STATE_CHANGED],0,PURPLE_MEDIA_STATE_END,( *(stream -> session)).id,(stream -> participant));
    ( *(media -> priv)).streams = g_list_remove(( *(media -> priv)).streams,stream);
    if (g_list_find(sessions,(stream -> session)) == ((GList *)((void *)0))) 
      sessions = g_list_prepend(sessions,(stream -> session));
    if (g_list_find_custom(participants,(stream -> participant),((GCompareFunc )strcmp)) == ((GList *)((void *)0))) 
      participants = g_list_prepend(participants,(g_strdup((stream -> participant))));
    purple_media_stream_free(stream);
  }
  iter = ( *(media -> priv)).streams;
/* Reduce to list of sessions to remove */
  for (; iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
    PurpleMediaStream *stream = (iter -> data);
    sessions = g_list_remove(sessions,(stream -> session));
  }
/* Free sessions with no streams left */
  for (; sessions != 0; sessions = g_list_delete_link(sessions,sessions)) {
    PurpleMediaSession *session = (sessions -> data);
    g_signal_emit(media,purple_media_signals[STATE_CHANGED],0,PURPLE_MEDIA_STATE_END,(session -> id),((void *)((void *)0)));
    g_hash_table_remove(( *(media -> priv)).sessions,(session -> id));
    purple_media_session_free(session);
  }
  iter = ( *(media -> priv)).streams;
/* Reduce to list of participants to remove */
  for (; iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
    PurpleMediaStream *stream = (iter -> data);
    GList *tmp;
    tmp = g_list_find_custom(participants,(stream -> participant),((GCompareFunc )strcmp));
    if (tmp != ((GList *)((void *)0))) {
      g_free((tmp -> data));
      participants = g_list_delete_link(participants,tmp);
    }
  }
/* Remove participants with no streams left (just emit the signal) */
  for (; participants != 0; participants = g_list_delete_link(participants,participants)) {
    gchar *participant = (participants -> data);
    GList *link = g_list_find_custom(( *(media -> priv)).participants,participant,((GCompareFunc )strcmp));
    g_signal_emit(media,purple_media_signals[STATE_CHANGED],0,PURPLE_MEDIA_STATE_END,((void *)((void *)0)),participant);
    if (link != ((GList *)((void *)0))) {
      g_free((link -> data));
      ( *(media -> priv)).participants = g_list_delete_link(( *(media -> priv)).participants,link);
    }
    g_free(participant);
  }
/* Free the conference if no sessions left */
  if ((( *(media -> priv)).sessions != ((GHashTable *)((void *)0))) && (g_hash_table_size(( *(media -> priv)).sessions) == 0)) {
    g_signal_emit(media,purple_media_signals[STATE_CHANGED],0,PURPLE_MEDIA_STATE_END,((void *)((void *)0)),((void *)((void *)0)));
    g_object_unref(media);
    return ;
  }
#endif
}

void purple_media_stream_info(PurpleMedia *media,PurpleMediaInfoType type,const gchar *session_id,const gchar *participant,gboolean local)
{
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return ;
    };
  }while (0);
  if (type == PURPLE_MEDIA_INFO_ACCEPT) {
    GList *streams;
    GList *sessions = (GList *)((void *)0);
    GList *participants = (GList *)((void *)0);
    do {
      if ((({
        GTypeInstance *__inst = (GTypeInstance *)media;
        GType __t = purple_media_get_type();
        gboolean __r;
        if (!(__inst != 0)) 
          __r = 0;
        else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
          __r = (!0);
        else 
          __r = g_type_check_instance_is_a(__inst,__t);
        __r;
      })) != 0) 
{
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
        return ;
      };
    }while (0);
    streams = purple_media_get_streams(media,session_id,participant);
/* Emit stream acceptance */
    for (; streams != 0; streams = g_list_delete_link(streams,streams)) {
      PurpleMediaStream *stream = (streams -> data);
      stream -> accepted = (!0);
      g_signal_emit(media,purple_media_signals[STREAM_INFO],0,type,( *(stream -> session)).id,(stream -> participant),local);
      if (g_list_find(sessions,(stream -> session)) == ((GList *)((void *)0))) 
        sessions = g_list_prepend(sessions,(stream -> session));
      if (g_list_find_custom(participants,(stream -> participant),((GCompareFunc )strcmp)) == ((GList *)((void *)0))) 
        participants = g_list_prepend(participants,(g_strdup((stream -> participant))));
    }
/* Emit session acceptance */
    for (; sessions != 0; sessions = g_list_delete_link(sessions,sessions)) {
      PurpleMediaSession *session = (sessions -> data);
      if (purple_media_accepted(media,(session -> id),0) != 0) 
        g_signal_emit(media,purple_media_signals[STREAM_INFO],0,PURPLE_MEDIA_INFO_ACCEPT,(session -> id),((void *)((void *)0)),local);
    }
/* Emit participant acceptance */
    for (; participants != 0; participants = g_list_delete_link(participants,participants)) {
      gchar *participant = (participants -> data);
      if (purple_media_accepted(media,0,participant) != 0) 
        g_signal_emit(media,purple_media_signals[STREAM_INFO],0,PURPLE_MEDIA_INFO_ACCEPT,((void *)((void *)0)),participant,local);
      g_free(participant);
    }
/* Emit conference acceptance */
    if (purple_media_accepted(media,0,0) != 0) 
      g_signal_emit(media,purple_media_signals[STREAM_INFO],0,PURPLE_MEDIA_INFO_ACCEPT,((void *)((void *)0)),((void *)((void *)0)),local);
    return ;
  }
  else if ((type == PURPLE_MEDIA_INFO_HANGUP) || (type == PURPLE_MEDIA_INFO_REJECT)) {
    GList *streams;
    do {
      if ((({
        GTypeInstance *__inst = (GTypeInstance *)media;
        GType __t = purple_media_get_type();
        gboolean __r;
        if (!(__inst != 0)) 
          __r = 0;
        else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
          __r = (!0);
        else 
          __r = g_type_check_instance_is_a(__inst,__t);
        __r;
      })) != 0) 
{
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
        return ;
      };
    }while (0);
    streams = purple_media_get_streams(media,session_id,participant);
/* Emit for stream */
    for (; streams != 0; streams = g_list_delete_link(streams,streams)) {
      PurpleMediaStream *stream = (streams -> data);
      g_signal_emit(media,purple_media_signals[STREAM_INFO],0,type,( *(stream -> session)).id,(stream -> participant),local);
    }
    if ((session_id != ((const gchar *)((void *)0))) && (participant != ((const gchar *)((void *)0)))) {
/* Everything that needs to be emitted has been */
    }
    else if ((session_id == ((const gchar *)((void *)0))) && (participant == ((const gchar *)((void *)0)))) {
/* Emit for everything in the conference */
      GList *sessions = (GList *)((void *)0);
      GList *participants = ( *(media -> priv)).participants;
      if (( *(media -> priv)).sessions != ((GHashTable *)((void *)0))) 
        sessions = g_hash_table_get_values(( *(media -> priv)).sessions);
/* Emit for sessions */
      for (; sessions != 0; sessions = g_list_delete_link(sessions,sessions)) {
        PurpleMediaSession *session = (sessions -> data);
        g_signal_emit(media,purple_media_signals[STREAM_INFO],0,type,(session -> id),((void *)((void *)0)),local);
      }
/* Emit for participants */
      for (; participants != 0; participants = ((participants != 0)?( *((GList *)participants)).next : ((struct _GList *)((void *)0)))) {
        gchar *participant = (participants -> data);
        g_signal_emit(media,purple_media_signals[STREAM_INFO],0,type,((void *)((void *)0)),participant,local);
      }
/* Emit for conference */
      g_signal_emit(media,purple_media_signals[STREAM_INFO],0,type,((void *)((void *)0)),((void *)((void *)0)),local);
    }
    else if (session_id != ((const gchar *)((void *)0))) {
/* Emit just the specific session */
      PurpleMediaSession *session = purple_media_get_session(media,session_id);
      if (session == ((PurpleMediaSession *)((void *)0))) {
        purple_debug_warning("media","Couldn\'t find session to hangup/reject.\n");
      }
      else {
        g_signal_emit(media,purple_media_signals[STREAM_INFO],0,type,(session -> id),((void *)((void *)0)),local);
      }
    }
    else if (participant != ((const gchar *)((void *)0))) {
/* Emit just the specific participant */
      if (!(g_list_find_custom(( *(media -> priv)).participants,participant,((GCompareFunc )strcmp)) != 0)) {
        purple_debug_warning("media","Couldn\'t find participant to hangup/reject.\n");
      }
      else {
        g_signal_emit(media,purple_media_signals[STREAM_INFO],0,type,((void *)((void *)0)),participant,local);
      }
    }
    purple_media_end(media,session_id,participant);
    return ;
  }
  g_signal_emit(media,purple_media_signals[STREAM_INFO],0,type,session_id,participant,local);
#endif
}

void purple_media_set_params(PurpleMedia *media,guint num_params,GParameter *params)
{
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return ;
    };
  }while (0);
  purple_media_backend_set_params(( *(media -> priv)).backend,num_params,params);
#endif
}

const gchar **purple_media_get_available_params(PurpleMedia *media)
{
  static const gchar *NULL_ARRAY[] = {((const gchar *)((void *)0))};
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return NULL_ARRAY;
    };
  }while (0);
  return purple_media_backend_get_available_params(( *(media -> priv)).backend);
#else
#endif
}

gboolean purple_media_param_is_supported(PurpleMedia *media,const gchar *param)
{
#ifdef USE_VV
  const gchar **params;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  do {
    if (param != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"param != NULL");
      return 0;
    };
  }while (0);
  params = purple_media_backend_get_available_params(( *(media -> priv)).backend);
  for (;  *params != ((const gchar *)((void *)0)); ++params) 
    if (!(strcmp( *params,param) != 0)) 
      return (!0);
#endif
  return 0;
}
#ifdef USE_VV

static void purple_media_new_local_candidate_cb(PurpleMediaBackend *backend,const gchar *sess_id,const gchar *participant,PurpleMediaCandidate *candidate,PurpleMedia *media)
{
  PurpleMediaSession *session = purple_media_get_session(media,sess_id);
  purple_media_insert_local_candidate(session,participant,purple_media_candidate_copy(candidate));
  g_signal_emit((session -> media),purple_media_signals[NEW_CANDIDATE],0,(session -> id),participant,candidate);
}

static void purple_media_candidates_prepared_cb(PurpleMediaBackend *backend,const gchar *sess_id,const gchar *name,PurpleMedia *media)
{
  PurpleMediaStream *stream_data;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return ;
    };
  }while (0);
  stream_data = purple_media_get_stream(media,sess_id,name);
  stream_data -> candidates_prepared = (!0);
  g_signal_emit(media,purple_media_signals[CANDIDATES_PREPARED],0,sess_id,name);
}
/* callback called when a pair of transport candidates (local and remote)
 * has been established */

static void purple_media_candidate_pair_established_cb(PurpleMediaBackend *backend,const gchar *sess_id,const gchar *name,PurpleMediaCandidate *local_candidate,PurpleMediaCandidate *remote_candidate,PurpleMedia *media)
{
  PurpleMediaStream *stream;
  GList *iter;
  guint id;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return ;
    };
  }while (0);
  stream = purple_media_get_stream(media,sess_id,name);
  id = purple_media_candidate_get_component_id(local_candidate);
  iter = (stream -> active_local_candidates);
{
    for (; iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
      PurpleMediaCandidate *c = (iter -> data);
      if (id == purple_media_candidate_get_component_id(c)) {
        g_object_unref(c);
        stream -> active_local_candidates = g_list_delete_link(iter,iter);
        stream -> active_local_candidates = g_list_prepend((stream -> active_local_candidates),(purple_media_candidate_copy(local_candidate)));
        break; 
      }
    }
  }
  if (iter == ((GList *)((void *)0))) 
    stream -> active_local_candidates = g_list_prepend((stream -> active_local_candidates),(purple_media_candidate_copy(local_candidate)));
  id = purple_media_candidate_get_component_id(local_candidate);
  iter = (stream -> active_remote_candidates);
{
    for (; iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
      PurpleMediaCandidate *c = (iter -> data);
      if (id == purple_media_candidate_get_component_id(c)) {
        g_object_unref(c);
        stream -> active_remote_candidates = g_list_delete_link(iter,iter);
        stream -> active_remote_candidates = g_list_prepend((stream -> active_remote_candidates),(purple_media_candidate_copy(remote_candidate)));
        break; 
      }
    }
  }
  if (iter == ((GList *)((void *)0))) 
    stream -> active_remote_candidates = g_list_prepend((stream -> active_remote_candidates),(purple_media_candidate_copy(remote_candidate)));
  purple_debug_info("media","candidate pair established\n");
}

static void purple_media_codecs_changed_cb(PurpleMediaBackend *backend,const gchar *sess_id,PurpleMedia *media)
{
  g_signal_emit(media,purple_media_signals[CODECS_CHANGED],0,sess_id);
}
#endif  /* USE_VV */

gboolean purple_media_add_stream(PurpleMedia *media,const gchar *sess_id,const gchar *who,PurpleMediaSessionType type,gboolean initiator,const gchar *transmitter,guint num_params,GParameter *params)
{
#ifdef USE_VV
  PurpleMediaSession *session;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  if (!(purple_media_backend_add_stream(( *(media -> priv)).backend,sess_id,who,type,initiator,transmitter,num_params,params) != 0)) {
    purple_debug_error("media","Error adding stream.\n");
    return 0;
  }
  session = purple_media_get_session(media,sess_id);
  if (!(session != 0)) {
    session = ((PurpleMediaSession *)(g_malloc0_n(1,(sizeof(PurpleMediaSession )))));
    session -> id = g_strdup(sess_id);
    session -> media = media;
    session -> type = type;
    session -> initiator = initiator;
    purple_media_add_session(media,session);
    g_signal_emit(media,purple_media_signals[STATE_CHANGED],0,PURPLE_MEDIA_STATE_NEW,(session -> id),((void *)((void *)0)));
  }
  if (!(g_list_find_custom(( *(media -> priv)).participants,who,((GCompareFunc )strcmp)) != 0)) {
    ( *(media -> priv)).participants = g_list_prepend(( *(media -> priv)).participants,(g_strdup(who)));
    g_signal_emit_by_name(media,"state-changed",PURPLE_MEDIA_STATE_NEW,((void *)((void *)0)),who);
  }
  if (purple_media_get_stream(media,sess_id,who) == ((PurpleMediaStream *)((void *)0))) {
    purple_media_insert_stream(session,who,initiator);
    g_signal_emit(media,purple_media_signals[STATE_CHANGED],0,PURPLE_MEDIA_STATE_NEW,(session -> id),who);
  }
  return (!0);
#else
#endif  /* USE_VV */
}

struct _PurpleMediaManager *purple_media_get_manager(PurpleMedia *media)
{
  PurpleMediaManager *ret;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  g_object_get(media,"manager",&ret,((void *)((void *)0)));
  return ret;
}

PurpleMediaSessionType purple_media_get_session_type(PurpleMedia *media,const gchar *sess_id)
{
#ifdef USE_VV
  PurpleMediaSession *session;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return PURPLE_MEDIA_NONE;
    };
  }while (0);
  session = purple_media_get_session(media,sess_id);
  return session -> type;
#else
#endif
}
/* XXX: Should wait until codecs-ready is TRUE before using this function */

GList *purple_media_get_codecs(PurpleMedia *media,const gchar *sess_id)
{
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  return purple_media_backend_get_codecs(( *(media -> priv)).backend,sess_id);
#else
#endif
}

GList *purple_media_get_local_candidates(PurpleMedia *media,const gchar *sess_id,const gchar *participant)
{
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  return purple_media_backend_get_local_candidates(( *(media -> priv)).backend,sess_id,participant);
#else
#endif
}

void purple_media_add_remote_candidates(PurpleMedia *media,const gchar *sess_id,const gchar *participant,GList *remote_candidates)
{
#ifdef USE_VV
  PurpleMediaStream *stream;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return ;
    };
  }while (0);
  stream = purple_media_get_stream(media,sess_id,participant);
  if (stream == ((PurpleMediaStream *)((void *)0))) {
    purple_debug_error("media","purple_media_add_remote_candidates: couldn\'t find stream %s %s.\n",((sess_id != 0)?sess_id : "(null)"),((participant != 0)?participant : "(null)"));
    return ;
  }
  stream -> remote_candidates = g_list_concat((stream -> remote_candidates),purple_media_candidate_list_copy(remote_candidates));
  purple_media_backend_add_remote_candidates(( *(media -> priv)).backend,sess_id,participant,remote_candidates);
#endif
}

GList *purple_media_get_active_local_candidates(PurpleMedia *media,const gchar *sess_id,const gchar *participant)
{
#ifdef USE_VV
  PurpleMediaStream *stream;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  stream = purple_media_get_stream(media,sess_id,participant);
  return purple_media_candidate_list_copy((stream -> active_local_candidates));
#else
#endif
}

GList *purple_media_get_active_remote_candidates(PurpleMedia *media,const gchar *sess_id,const gchar *participant)
{
#ifdef USE_VV
  PurpleMediaStream *stream;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  stream = purple_media_get_stream(media,sess_id,participant);
  return purple_media_candidate_list_copy((stream -> active_remote_candidates));
#else
#endif
}

gboolean purple_media_set_remote_codecs(PurpleMedia *media,const gchar *sess_id,const gchar *participant,GList *codecs)
{
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  return purple_media_backend_set_remote_codecs(( *(media -> priv)).backend,sess_id,participant,codecs);
#else
#endif
}

gboolean purple_media_candidates_prepared(PurpleMedia *media,const gchar *session_id,const gchar *participant)
{
#ifdef USE_VV
  GList *streams;
  gboolean prepared = (!0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  streams = purple_media_get_streams(media,session_id,participant);
{
    for (; streams != 0; streams = g_list_delete_link(streams,streams)) {
      PurpleMediaStream *stream = (streams -> data);
      if ((stream -> candidates_prepared) == 0) {
        g_list_free(streams);
        prepared = 0;
        break; 
      }
    }
  }
  return prepared;
#else
#endif
}

gboolean purple_media_set_send_codec(PurpleMedia *media,const gchar *sess_id,PurpleMediaCodec *codec)
{
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  return purple_media_backend_set_send_codec(( *(media -> priv)).backend,sess_id,codec);
#else
#endif
}

gboolean purple_media_codecs_ready(PurpleMedia *media,const gchar *sess_id)
{
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  return purple_media_backend_codecs_ready(( *(media -> priv)).backend,sess_id);
#else
#endif
}

gboolean purple_media_is_initiator(PurpleMedia *media,const gchar *sess_id,const gchar *participant)
{
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  if ((sess_id == ((const gchar *)((void *)0))) && (participant == ((const gchar *)((void *)0)))) 
    return ( *(media -> priv)).initiator;
  else if ((sess_id != ((const gchar *)((void *)0))) && (participant == ((const gchar *)((void *)0)))) {
    PurpleMediaSession *session = purple_media_get_session(media,sess_id);
    return (session != ((PurpleMediaSession *)((void *)0)))?(session -> initiator) : 0;
  }
  else if ((sess_id != ((const gchar *)((void *)0))) && (participant != ((const gchar *)((void *)0)))) {
    PurpleMediaStream *stream = purple_media_get_stream(media,sess_id,participant);
    return (stream != ((PurpleMediaStream *)((void *)0)))?(stream -> initiator) : 0;
  }
#endif
  return 0;
}

gboolean purple_media_accepted(PurpleMedia *media,const gchar *sess_id,const gchar *participant)
{
#ifdef USE_VV
  gboolean accepted = (!0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  if ((sess_id == ((const gchar *)((void *)0))) && (participant == ((const gchar *)((void *)0)))) {
    GList *streams = ( *(media -> priv)).streams;
{
      for (; streams != 0; streams = ((streams != 0)?( *((GList *)streams)).next : ((struct _GList *)((void *)0)))) {
        PurpleMediaStream *stream = (streams -> data);
        if ((stream -> accepted) == 0) {
          accepted = 0;
          break; 
        }
      }
    }
  }
  else if ((sess_id != ((const gchar *)((void *)0))) && (participant == ((const gchar *)((void *)0)))) {
    GList *streams = purple_media_get_streams(media,sess_id,0);
{
      for (; streams != 0; streams = g_list_delete_link(streams,streams)) {
        PurpleMediaStream *stream = (streams -> data);
        if ((stream -> accepted) == 0) {
          g_list_free(streams);
          accepted = 0;
          break; 
        }
      }
    }
  }
  else if ((sess_id != ((const gchar *)((void *)0))) && (participant != ((const gchar *)((void *)0)))) {
    PurpleMediaStream *stream = purple_media_get_stream(media,sess_id,participant);
    if ((stream == ((PurpleMediaStream *)((void *)0))) || ((stream -> accepted) == 0)) 
      accepted = 0;
  }
  return accepted;
#else
#endif
}

void purple_media_set_input_volume(PurpleMedia *media,const gchar *session_id,double level)
{
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)( *(media -> priv)).backend;
      GType __t = purple_media_backend_fs2_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND_FS2(media->priv->backend)");
      return ;
    };
  }while (0);
  purple_media_backend_fs2_set_input_volume(((PurpleMediaBackendFs2 *)(g_type_check_instance_cast(((GTypeInstance *)( *(media -> priv)).backend),purple_media_backend_fs2_get_type()))),session_id,level);
#endif
}

void purple_media_set_output_volume(PurpleMedia *media,const gchar *session_id,const gchar *participant,double level)
{
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)( *(media -> priv)).backend;
      GType __t = purple_media_backend_fs2_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND_FS2(media->priv->backend)");
      return ;
    };
  }while (0);
  purple_media_backend_fs2_set_output_volume(((PurpleMediaBackendFs2 *)(g_type_check_instance_cast(((GTypeInstance *)( *(media -> priv)).backend),purple_media_backend_fs2_get_type()))),session_id,participant,level);
#endif
}

gulong purple_media_set_output_window(PurpleMedia *media,const gchar *session_id,const gchar *participant,gulong window_id)
{
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  return purple_media_manager_set_output_window(( *(media -> priv)).manager,media,session_id,participant,window_id);
#else
#endif
}

void purple_media_remove_output_windows(PurpleMedia *media)
{
#ifdef USE_VV
  GList *iter = ( *(media -> priv)).streams;
  for (; iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
    PurpleMediaStream *stream = (iter -> data);
    purple_media_manager_remove_output_windows(( *(media -> priv)).manager,media,( *(stream -> session)).id,(stream -> participant));
  }
  iter = purple_media_get_session_ids(media);
  for (; iter != 0; iter = g_list_delete_link(iter,iter)) {
    gchar *session_name = (iter -> data);
    purple_media_manager_remove_output_windows(( *(media -> priv)).manager,media,session_name,0);
  }
#endif
}
#ifdef USE_GSTREAMER

GstElement *purple_media_get_tee(PurpleMedia *media,const gchar *session_id,const gchar *participant)
{
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  if ((({
    GTypeInstance *__inst = (GTypeInstance *)( *(media -> priv)).backend;
    GType __t = purple_media_backend_fs2_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
    return purple_media_backend_fs2_get_tee(((PurpleMediaBackendFs2 *)(g_type_check_instance_cast(((GTypeInstance *)( *(media -> priv)).backend),purple_media_backend_fs2_get_type()))),session_id,participant);
  do {
    g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","media.c",1435,((const char *)__func__));
    return 0;
  }while (0);
#else
#endif
}
#endif /* USE_GSTREAMER */
