/*
 * pidgin
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "purple.h"
#include <glib.h>
#include <signal.h>
#include <string.h>
#ifndef _WIN32
#include <unistd.h>
#else
#include "win32/win32dep.h"
#endif
#include "defines.h"
/**
 * The following eventloop functions are used in both pidgin and purple-text. If your
 * application uses glib mainloop, you can safely use this verbatim.
 */
#define PURPLE_GLIB_READ_COND  (G_IO_IN | G_IO_HUP | G_IO_ERR)
#define PURPLE_GLIB_WRITE_COND (G_IO_OUT | G_IO_HUP | G_IO_ERR | G_IO_NVAL)
typedef struct _PurpleGLibIOClosure {
PurpleInputFunction function;
guint result;
gpointer data;}PurpleGLibIOClosure;

static void purple_glib_io_destroy(gpointer data)
{
  g_free(data);
}

static gboolean purple_glib_io_invoke(GIOChannel *source,GIOCondition condition,gpointer data)
{
  PurpleGLibIOClosure *closure = data;
  PurpleInputCondition purple_cond = 0;
  if ((condition & (G_IO_IN | G_IO_HUP | G_IO_ERR)) != 0U) 
    purple_cond |= PURPLE_INPUT_READ;
  if ((condition & (G_IO_OUT | G_IO_HUP | G_IO_ERR | G_IO_NVAL)) != 0U) 
    purple_cond |= PURPLE_INPUT_WRITE;
  ( *(closure -> function))((closure -> data),g_io_channel_unix_get_fd(source),purple_cond);
  return (!0);
}

static guint glib_input_add(gint fd,PurpleInputCondition condition,PurpleInputFunction function,gpointer data)
{
  PurpleGLibIOClosure *closure = (PurpleGLibIOClosure *)(g_malloc0_n(1,(sizeof(PurpleGLibIOClosure ))));
  GIOChannel *channel;
  GIOCondition cond = 0;
  closure -> function = function;
  closure -> data = data;
  if ((condition & PURPLE_INPUT_READ) != 0U) 
    cond |= (G_IO_IN | G_IO_HUP | G_IO_ERR);
  if ((condition & PURPLE_INPUT_WRITE) != 0U) 
    cond |= (G_IO_OUT | G_IO_HUP | G_IO_ERR | G_IO_NVAL);
#if defined _WIN32 && !defined WINPIDGIN_USE_GLIB_IO_CHANNEL
#else
  channel = g_io_channel_unix_new(fd);
#endif
  closure -> result = g_io_add_watch_full(channel,0,cond,purple_glib_io_invoke,closure,purple_glib_io_destroy);
  g_io_channel_unref(channel);
  return closure -> result;
}
static PurpleEventLoopUiOps glib_eventloops = {(g_timeout_add), (g_source_remove), (glib_input_add), (g_source_remove), ((int (*)(int , int *))((void *)0)), (g_timeout_add_seconds), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
#if GLIB_CHECK_VERSION(2,14,0)
#else
#endif
/* padding */
};
/*** End of the eventloop functions. ***/
/*** Conversation uiops ***/

static void null_write_conv(PurpleConversation *conv,const char *who,const char *alias,const char *message,PurpleMessageFlags flags,time_t mtime)
{
  const char *name;
  if ((alias != 0) && (( *alias) != 0)) 
    name = alias;
  else if ((who != 0) && (( *who) != 0)) 
    name = who;
  else 
    name = ((const char *)((void *)0));
  printf("(%s) %s %s: %s\n",purple_conversation_get_name(conv),purple_utf8_strftime("(%H:%M:%S)",(localtime((&mtime)))),name,message);
}
static PurpleConversationUiOps null_conv_uiops = {((void (*)(PurpleConversation *))((void *)0)), ((void (*)(PurpleConversation *))((void *)0)), ((void (*)(PurpleConversation *, const char *, const char *, PurpleMessageFlags , time_t ))((void *)0)), ((void (*)(PurpleConversation *, const char *, const char *, PurpleMessageFlags , time_t ))((void *)0)), (null_write_conv), ((void (*)(PurpleConversation *, GList *, gboolean ))((void *)0)), ((void (*)(PurpleConversation *, const char *, const char *, const char *))((void *)0)), ((void (*)(PurpleConversation *, GList *))((void *)0)), ((void (*)(PurpleConversation *, const char *))((void *)0)), ((void (*)(PurpleConversation *))((void *)0)), ((gboolean (*)(PurpleConversation *))((void *)0)), ((gboolean (*)(PurpleConversation *, const char *, gboolean ))((void *)0)), ((void (*)(PurpleConversation *, const char *, const guchar *, gsize ))((void *)0)), ((void (*)(PurpleConversation *, const char *))((void *)0)), ((void (*)(PurpleConversation *, const char *))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* create_conversation  */
/* destroy_conversation */
/* write_chat           */
/* write_im             */
/* write_conv           */
/* chat_add_users       */
/* chat_rename_user     */
/* chat_remove_users    */
/* chat_update_user     */
/* present              */
/* has_focus            */
/* custom_smiley_add    */
/* custom_smiley_write  */
/* custom_smiley_close  */
/* send_confirm         */
};

static void null_ui_init()
{
/**
	 * This should initialize the UI components for all the modules. Here we
	 * just initialize the UI for conversations.
	 */
  purple_conversations_set_ui_ops(&null_conv_uiops);
}
static PurpleCoreUiOps null_core_uiops = {((void (*)())((void *)0)), ((void (*)())((void *)0)), (null_ui_init), ((void (*)())((void *)0)), ((GHashTable *(*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* padding */
};

static void init_libpurple()
{
/* Set a custom user directory (optional) */
  purple_util_set_user_dir("/dev/null");
/* We do not want any debugging for now to keep the noise to a minimum. */
  purple_debug_set_enabled(0);
/* Set the core-uiops, which is used to
	 * 	- initialize the ui specific preferences.
	 * 	- initialize the debug ui.
	 * 	- initialize the ui components for all the modules.
	 * 	- uninitialize the ui components for all the modules when the core terminates.
	 */
  purple_core_set_ui_ops(&null_core_uiops);
/* Set the uiops for the eventloop. If your client is glib-based, you can safely
	 * copy this verbatim. */
  purple_eventloop_set_ui_ops(&glib_eventloops);
/* Set path to search for plugins. The core (libpurple) takes care of loading the
	 * core-plugins, which includes the protocol-plugins. So it is not essential to add
	 * any path here, but it might be desired, especially for ui-specific plugins. */
  purple_plugins_add_search_path("");
/* Now that all the essential stuff has been set, let's try to init the core. It's
	 * necessary to provide a non-NULL name for the current ui to the core. This name
	 * is used by stuff that depends on this ui, for example the ui-specific plugins. */
  if (!(purple_core_init("nullclient") != 0)) {
/* Initializing the core failed. Terminate. */
    fprintf(stderr,"libpurple initialization failed. Dumping core.\nPlease report this!\n");
    abort();
  }
/* Create and load the buddylist. */
  purple_set_blist(purple_blist_new());
  purple_blist_load();
/* Load the preferences. */
  purple_prefs_load();
/* Load the desired plugins. The client should save the list of loaded plugins in
	 * the preferences using purple_plugins_save_loaded(PLUGIN_SAVE_PREF) */
  purple_plugins_load_saved("/purple/nullclient/plugins/saved");
/* Load the pounces. */
  purple_pounces_load();
}

static void signed_on(PurpleConnection *gc,gpointer null)
{
  PurpleAccount *account = purple_connection_get_account(gc);
  printf("Account connected: %s %s\n",(account -> username),(account -> protocol_id));
}

static void connect_to_signals_for_demonstration_purposes_only()
{
  static int handle;
  purple_signal_connect(purple_connections_get_handle(),"signed-on",(&handle),((PurpleCallback )signed_on),0);
}

int main(int argc,char *argv[])
{
  GList *iter;
  int i;
  int num;
  GList *names = (GList *)((void *)0);
  const char *prpl = (const char *)((void *)0);
  char name[128UL];
  char *password;
  GMainLoop *loop = g_main_loop_new(0,0);
  PurpleAccount *account;
  PurpleSavedStatus *status;
  char *res;
#ifndef _WIN32
/* libpurple's built-in DNS resolution forks processes to perform
	 * blocking lookups without blocking the main process.  It does not
	 * handle SIGCHLD itself, so if the UI does not you quickly get an army
	 * of zombie subprocesses marching around.
	 */
  signal(17,((__sighandler_t )((__sighandler_t )1)));
#endif
  init_libpurple();
  printf("libpurple initialized.\n");
  iter = purple_plugins_get_protocols();
  for (i = 0; iter != 0; iter = (iter -> next)) {
    PurplePlugin *plugin = (iter -> data);
    PurplePluginInfo *info = (plugin -> info);
    if ((info != 0) && ((info -> name) != 0)) {
      printf("\t%d: %s\n",i++,(info -> name));
      names = g_list_append(names,(info -> id));
    }
  }
  printf("Select the protocol [0-%d]: ",(i - 1));
  res = fgets(name,(sizeof(name)),stdin);
  if (!(res != 0)) {
    fprintf(stderr,"Failed to gets protocol selection.");
    abort();
  }
  if (sscanf(name,"%d",&num) == 1) 
    prpl = (g_list_nth_data(names,num));
  if (!(prpl != 0)) {
    fprintf(stderr,"Failed to gets protocol.");
    abort();
  }
  printf("Username: ");
  res = fgets(name,(sizeof(name)),stdin);
  if (!(res != 0)) {
    fprintf(stderr,"Failed to read user name.");
    abort();
  }
/* strip the \n at the end */
  name[strlen(name) - 1] = 0;
/* Create the account */
  account = purple_account_new(name,prpl);
/* Get the password for the account */
  password = getpass("Password: ");
  purple_account_set_password(account,password);
/* It's necessary to enable the account first. */
  purple_account_set_enabled(account,"nullclient",(!0));
/* Now, to connect the account(s), create a status and activate it. */
  status = purple_savedstatus_new(0,PURPLE_STATUS_AVAILABLE);
  purple_savedstatus_activate(status);
  connect_to_signals_for_demonstration_purposes_only();
  g_main_loop_run(loop);
  return 0;
}
