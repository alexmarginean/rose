/**
 * @file certificate.c Public-Key Certificate API
 * @ingroup core
 */
/*
 *
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "certificate.h"
#include "dbus-maybe.h"
#include "debug.h"
#include "request.h"
#include "signals.h"
#include "util.h"
/** List holding pointers to all registered certificate schemes */
static GList *cert_schemes = (GList *)((void *)0);
/** List of registered Verifiers */
static GList *cert_verifiers = (GList *)((void *)0);
/** List of registered Pools */
static GList *cert_pools = (GList *)((void *)0);
/*
 * TODO: Merge this with PurpleCertificateVerificationStatus for 3.0.0 */
typedef enum __unnamed_enum___F0_L46_C9_PURPLE_CERTIFICATE_UNKNOWN_ERROR__COMMA__PURPLE_CERTIFICATE_NO_PROBLEMS__COMMA__PURPLE_CERTIFICATE_NON_FATALS_MASK__COMMA__PURPLE_CERTIFICATE_SELF_SIGNED__COMMA__PURPLE_CERTIFICATE_CA_UNKNOWN__COMMA__PURPLE_CERTIFICATE_NOT_ACTIVATED__COMMA__PURPLE_CERTIFICATE_EXPIRED__COMMA__PURPLE_CERTIFICATE_NAME_MISMATCH__COMMA__PURPLE_CERTIFICATE_NO_CA_POOL__COMMA__PURPLE_CERTIFICATE_FATALS_MASK__COMMA__PURPLE_CERTIFICATE_INVALID_CHAIN__COMMA__PURPLE_CERTIFICATE_REVOKED__COMMA__PURPLE_CERTIFICATE_LAST {PURPLE_CERTIFICATE_UNKNOWN_ERROR=-1L,
/* Not an error */
PURPLE_CERTIFICATE_NO_PROBLEMS,
/* Non-fatal */
PURPLE_CERTIFICATE_NON_FATALS_MASK=0x0000FFFF,
/* The certificate is self-signed. */
PURPLE_CERTIFICATE_SELF_SIGNED=0x01,
/* The CA is not in libpurple's pool of certificates. */
PURPLE_CERTIFICATE_CA_UNKNOWN,
/* The current time is before the certificate's specified
	 * activation time.
	 */
PURPLE_CERTIFICATE_NOT_ACTIVATED=0x04,
/* The current time is after the certificate's specified expiration time */
PURPLE_CERTIFICATE_EXPIRED=0x08,
/* The certificate's subject name doesn't match the expected */
PURPLE_CERTIFICATE_NAME_MISMATCH=0x10,
/* No CA pool was found. This shouldn't happen... */
PURPLE_CERTIFICATE_NO_CA_POOL=0x20,
/* Fatal */
PURPLE_CERTIFICATE_FATALS_MASK=0xFFFF0000,
/* The signature chain could not be validated. Due to limitations in the
	 * the current API, this also indicates one of the CA certificates in the
	 * chain is expired (or not yet activated). FIXME 3.0.0 */
PURPLE_CERTIFICATE_INVALID_CHAIN=0x10000,
/* The signature has been revoked. */
PURPLE_CERTIFICATE_REVOKED=0x20000,PURPLE_CERTIFICATE_LAST=0x40000}PurpleCertificateInvalidityFlags;

static const gchar *invalidity_reason_to_string(PurpleCertificateInvalidityFlags flag)
{
  switch(flag){
    case PURPLE_CERTIFICATE_SELF_SIGNED:
{
      return (const char *)(dgettext("pidgin","The certificate is self-signed and cannot be automatically checked."));
      break; 
    }
    case PURPLE_CERTIFICATE_CA_UNKNOWN:
{
      return (const char *)(dgettext("pidgin","The certificate is not trusted because no certificate that can verify it is currently trusted."));
      break; 
    }
    case PURPLE_CERTIFICATE_NOT_ACTIVATED:
{
      return (const char *)(dgettext("pidgin","The certificate is not valid yet.  Check that your computer\'s date and time are accurate."));
      break; 
    }
    case PURPLE_CERTIFICATE_EXPIRED:
{
      return (const char *)(dgettext("pidgin","The certificate has expired and should not be considered valid.  Check that your computer\'s date and time are accurate."));
      break; 
    }
    case PURPLE_CERTIFICATE_NAME_MISMATCH:
{
/* Translators: "domain" refers to a DNS domain (e.g. talk.google.com) */
      return (const char *)(dgettext("pidgin","The certificate presented is not issued to this domain."));
      break; 
    }
    case PURPLE_CERTIFICATE_NO_CA_POOL:
{
      return (const char *)(dgettext("pidgin","You have no database of root certificates, so this certificate cannot be validated."));
      break; 
    }
    case PURPLE_CERTIFICATE_INVALID_CHAIN:
{
      return (const char *)(dgettext("pidgin","The certificate chain presented is invalid."));
      break; 
    }
    case PURPLE_CERTIFICATE_REVOKED:
{
      return (const char *)(dgettext("pidgin","The certificate has been revoked."));
      break; 
    }
    default:
{
      return (const char *)(dgettext("pidgin","An unknown certificate error occurred."));
      break; 
    }
  }
}

void purple_certificate_verify(PurpleCertificateVerifier *verifier,const gchar *subject_name,GList *cert_chain,PurpleCertificateVerifiedCallback cb,gpointer cb_data)
{
  PurpleCertificateVerificationRequest *vrq;
  PurpleCertificateScheme *scheme;
  do {
    if (subject_name != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"subject_name != NULL");
      return ;
    };
  }while (0);
/* If you don't have a cert to check, why are you requesting that it
	   be verified? */
  do {
    if (cert_chain != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cert_chain != NULL");
      return ;
    };
  }while (0);
  do {
    if (cb != ((void (*)(PurpleCertificateVerificationStatus , gpointer ))((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cb != NULL");
      return ;
    };
  }while (0);
/* Look up the CertificateScheme */
  scheme = purple_certificate_find_scheme((verifier -> scheme_name));
  do {
    if (scheme != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"scheme");
      return ;
    };
  }while (0);
/* Check that at least the first cert in the chain matches the
	   Verifier scheme */
  do {
    if (scheme == ( *((PurpleCertificate *)(cert_chain -> data))).scheme) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"scheme == ((PurpleCertificate *) (cert_chain->data))->scheme");
      return ;
    };
  }while (0);
/* Construct and fill in the request fields */
  vrq = ((PurpleCertificateVerificationRequest *)(g_malloc0_n(1,(sizeof(PurpleCertificateVerificationRequest )))));
  vrq -> verifier = verifier;
  vrq -> scheme = scheme;
  vrq -> subject_name = g_strdup(subject_name);
  vrq -> cert_chain = purple_certificate_copy_list(cert_chain);
  vrq -> cb = cb;
  vrq -> cb_data = cb_data;
/* Initiate verification */
  ( *(verifier -> start_verification))(vrq);
}

void purple_certificate_verify_complete(PurpleCertificateVerificationRequest *vrq,PurpleCertificateVerificationStatus st)
{
  PurpleCertificateVerifier *vr;
  do {
    if (vrq != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"vrq");
      return ;
    };
  }while (0);
  if (st == PURPLE_CERTIFICATE_VALID) {
    purple_debug_info("certificate","Successfully verified certificate for %s\n",(vrq -> subject_name));
  }
  else {
    purple_debug_error("certificate","Failed to verify certificate for %s\n",(vrq -> subject_name));
  }
/* Pass the results on to the request's callback */
  ( *(vrq -> cb))(st,(vrq -> cb_data));
/* And now to eliminate the request */
/* Fetch the Verifier responsible... */
  vr = (vrq -> verifier);
/* ...and order it to KILL */
  ( *(vr -> destroy_request))(vrq);
/* Now the internals have been cleaned up, so clean up the libpurple-
	   created elements */
  g_free((vrq -> subject_name));
  purple_certificate_destroy_list((vrq -> cert_chain));
/*  A structure born
	 *          to much ado
	 *                   and with so much within.
	 * It reaches now
	 *             its quiet end. */
  g_free(vrq);
}

PurpleCertificate *purple_certificate_copy(PurpleCertificate *crt)
{
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return 0;
    };
  }while (0);
  do {
    if ((crt -> scheme) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme");
      return 0;
    };
  }while (0);
  do {
    if (( *(crt -> scheme)).copy_certificate != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme->copy_certificate");
      return 0;
    };
  }while (0);
  return ( *( *(crt -> scheme)).copy_certificate)(crt);
}

GList *purple_certificate_copy_list(GList *crt_list)
{
  GList *new_l;
  GList *l;
/* First, make a shallow copy of the list */
  new_l = g_list_copy(crt_list);
/* Now go through and actually duplicate each certificate */
  for (l = new_l; l != 0; l = (l -> next)) {
    l -> data = (purple_certificate_copy((l -> data)));
  }
  return new_l;
}

void purple_certificate_destroy(PurpleCertificate *crt)
{
  PurpleCertificateScheme *scheme;
  if (((PurpleCertificate *)((void *)0)) == crt) 
    return ;
  scheme = (crt -> scheme);
  ( *(scheme -> destroy_certificate))(crt);
}

void purple_certificate_destroy_list(GList *crt_list)
{
  PurpleCertificate *crt;
  GList *l;
  for (l = crt_list; l != 0; l = (l -> next)) {
    crt = ((PurpleCertificate *)(l -> data));
    purple_certificate_destroy(crt);
  }
  g_list_free(crt_list);
}

gboolean purple_certificate_signed_by(PurpleCertificate *crt,PurpleCertificate *issuer)
{
  PurpleCertificateScheme *scheme;
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return 0;
    };
  }while (0);
  do {
    if (issuer != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"issuer");
      return 0;
    };
  }while (0);
  scheme = (crt -> scheme);
  do {
    if (scheme != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"scheme");
      return 0;
    };
  }while (0);
/* We can't compare two certs of unrelated schemes, obviously */
  do {
    if ((issuer -> scheme) == scheme) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"issuer->scheme == scheme");
      return 0;
    };
  }while (0);
  return ( *(scheme -> signed_by))(crt,issuer);
}

gboolean purple_certificate_check_signature_chain_with_failing(GList *chain,PurpleCertificate **failing)
{
  GList *cur;
  PurpleCertificate *crt;
  PurpleCertificate *issuer;
  gchar *uid;
  time_t now;
  time_t activation;
  time_t expiration;
  gboolean ret;
  do {
    if (chain != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chain");
      return 0;
    };
  }while (0);
  if (failing != 0) 
     *failing = ((PurpleCertificate *)((void *)0));
  uid = purple_certificate_get_unique_id(((PurpleCertificate *)(chain -> data)));
  purple_debug_info("certificate","Checking signature chain for uid=%s\n",uid);
  g_free(uid);
/* If this is a single-certificate chain, say that it is valid */
  if ((chain -> next) == ((GList *)((void *)0))) {
    purple_debug_info("certificate","...Singleton. We\'ll say it\'s valid.\n");
    return (!0);
  }
  now = time(0);
/* Load crt with the first certificate */
  crt = ((PurpleCertificate *)(chain -> data));
/* And start with the second certificate in the chain */
  for (cur = (chain -> next); cur != 0; cur = (cur -> next)) {
    issuer = ((PurpleCertificate *)(cur -> data));
    uid = purple_certificate_get_unique_id(issuer);
    ret = purple_certificate_get_times(issuer,&activation,&expiration);
    if ((!(ret != 0) || (now < activation)) || (now > expiration)) {
      if (!(ret != 0)) 
        purple_debug_error("certificate","...Failed to get validity times for certificate %s\nChain is INVALID\n",uid);
      else if (now > expiration) 
        purple_debug_error("certificate","...Issuer %s expired at %s\nChain is INVALID\n",uid,ctime((&expiration)));
      else 
        purple_debug_error("certificate","...Not-yet-activated issuer %s will be valid at %s\nChain is INVALID\n",uid,ctime((&activation)));
      if (failing != 0) 
         *failing = crt;
      g_free(uid);
      return 0;
    }
/* Check the signature for this link */
    if (!(purple_certificate_signed_by(crt,issuer) != 0)) {
      purple_debug_error("certificate","...Bad or missing signature by %s\nChain is INVALID\n",uid);
      g_free(uid);
      if (failing != 0) 
         *failing = crt;
      return 0;
    }
    purple_debug_info("certificate","...Good signature by %s\n",uid);
    g_free(uid);
/* The issuer is now the next crt whose signature is to be
		   checked */
    crt = issuer;
  }
/* If control reaches this point, the chain is valid */
  purple_debug_info("certificate","Chain is VALID\n");
  return (!0);
}

gboolean purple_certificate_check_signature_chain(GList *chain)
{
  return purple_certificate_check_signature_chain_with_failing(chain,0);
}

PurpleCertificate *purple_certificate_import(PurpleCertificateScheme *scheme,const gchar *filename)
{
  do {
    if (scheme != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"scheme");
      return 0;
    };
  }while (0);
  do {
    if ((scheme -> import_certificate) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"scheme->import_certificate");
      return 0;
    };
  }while (0);
  do {
    if (filename != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"filename");
      return 0;
    };
  }while (0);
  return ( *(scheme -> import_certificate))(filename);
}

GSList *purple_certificates_import(PurpleCertificateScheme *scheme,const gchar *filename)
{
  do {
    if (scheme != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"scheme");
      return 0;
    };
  }while (0);
  do {
    if ((scheme -> import_certificates) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"scheme->import_certificates");
      return 0;
    };
  }while (0);
  do {
    if (filename != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"filename");
      return 0;
    };
  }while (0);
  return ( *(scheme -> import_certificates))(filename);
}

gboolean purple_certificate_export(const gchar *filename,PurpleCertificate *crt)
{
  PurpleCertificateScheme *scheme;
  do {
    if (filename != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"filename");
      return 0;
    };
  }while (0);
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return 0;
    };
  }while (0);
  do {
    if ((crt -> scheme) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme");
      return 0;
    };
  }while (0);
  scheme = (crt -> scheme);
  do {
    if ((scheme -> export_certificate) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"scheme->export_certificate");
      return 0;
    };
  }while (0);
  return ( *(scheme -> export_certificate))(filename,crt);
}

static gboolean byte_arrays_equal(const GByteArray *array1,const GByteArray *array2)
{
  do {
    if (array1 != ((const GByteArray *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"array1 != NULL");
      return 0;
    };
  }while (0);
  do {
    if (array2 != ((const GByteArray *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"array2 != NULL");
      return 0;
    };
  }while (0);
  return ((array1 -> len) == (array2 -> len)) && (0 == memcmp((array1 -> data),(array2 -> data),(array1 -> len)));
}

GByteArray *purple_certificate_get_fingerprint_sha1(PurpleCertificate *crt)
{
  PurpleCertificateScheme *scheme;
  GByteArray *fpr;
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return 0;
    };
  }while (0);
  do {
    if ((crt -> scheme) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme");
      return 0;
    };
  }while (0);
  scheme = (crt -> scheme);
  do {
    if ((scheme -> get_fingerprint_sha1) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"scheme->get_fingerprint_sha1");
      return 0;
    };
  }while (0);
  fpr = ( *(scheme -> get_fingerprint_sha1))(crt);
  return fpr;
}

gchar *purple_certificate_get_unique_id(PurpleCertificate *crt)
{
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return 0;
    };
  }while (0);
  do {
    if ((crt -> scheme) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme");
      return 0;
    };
  }while (0);
  do {
    if (( *(crt -> scheme)).get_unique_id != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme->get_unique_id");
      return 0;
    };
  }while (0);
  return ( *( *(crt -> scheme)).get_unique_id)(crt);
}

gchar *purple_certificate_get_issuer_unique_id(PurpleCertificate *crt)
{
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return 0;
    };
  }while (0);
  do {
    if ((crt -> scheme) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme");
      return 0;
    };
  }while (0);
  do {
    if (( *(crt -> scheme)).get_issuer_unique_id != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme->get_issuer_unique_id");
      return 0;
    };
  }while (0);
  return ( *( *(crt -> scheme)).get_issuer_unique_id)(crt);
}

gchar *purple_certificate_get_subject_name(PurpleCertificate *crt)
{
  PurpleCertificateScheme *scheme;
  gchar *subject_name;
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return 0;
    };
  }while (0);
  do {
    if ((crt -> scheme) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme");
      return 0;
    };
  }while (0);
  scheme = (crt -> scheme);
  do {
    if ((scheme -> get_subject_name) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"scheme->get_subject_name");
      return 0;
    };
  }while (0);
  subject_name = ( *(scheme -> get_subject_name))(crt);
  return subject_name;
}

gboolean purple_certificate_check_subject_name(PurpleCertificate *crt,const gchar *name)
{
  PurpleCertificateScheme *scheme;
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return 0;
    };
  }while (0);
  do {
    if ((crt -> scheme) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme");
      return 0;
    };
  }while (0);
  do {
    if (name != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name");
      return 0;
    };
  }while (0);
  scheme = (crt -> scheme);
  do {
    if ((scheme -> check_subject_name) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"scheme->check_subject_name");
      return 0;
    };
  }while (0);
  return ( *(scheme -> check_subject_name))(crt,name);
}

gboolean purple_certificate_get_times(PurpleCertificate *crt,time_t *activation,time_t *expiration)
{
  PurpleCertificateScheme *scheme;
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return 0;
    };
  }while (0);
  scheme = (crt -> scheme);
  do {
    if (scheme != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"scheme");
      return 0;
    };
  }while (0);
/* If both provided references are NULL, what are you doing calling
	   this? */
  do {
    if ((activation != ((time_t *)((void *)0))) || (expiration != ((time_t *)((void *)0)))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"(activation != NULL) || (expiration != NULL)");
      return 0;
    };
  }while (0);
/* Throw the request on down to the certscheme */
  return ( *(scheme -> get_times))(crt,activation,expiration);
}

gchar *purple_certificate_pool_mkpath(PurpleCertificatePool *pool,const gchar *id)
{
  gchar *path;
  gchar *esc_scheme_name;
  gchar *esc_name;
  gchar *esc_id;
  do {
    if (pool != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool");
      return 0;
    };
  }while (0);
  do {
    if ((pool -> scheme_name) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool->scheme_name");
      return 0;
    };
  }while (0);
  do {
    if ((pool -> name) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool->name");
      return 0;
    };
  }while (0);
/* Escape all the elements for filesystem-friendliness */
  esc_scheme_name = g_strdup(purple_escape_filename((pool -> scheme_name)));
  esc_name = g_strdup(purple_escape_filename((pool -> name)));
  esc_id = ((id != 0)?g_strdup(purple_escape_filename(id)) : ((char *)((void *)0)));
  path = g_build_filename(purple_user_dir(),"certificates",esc_scheme_name,esc_name,esc_id,((void *)((void *)0)));
/* TODO: constantize this? */
  g_free(esc_scheme_name);
  g_free(esc_name);
  g_free(esc_id);
  return path;
}

gboolean purple_certificate_pool_usable(PurpleCertificatePool *pool)
{
  do {
    if (pool != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool");
      return 0;
    };
  }while (0);
  do {
    if ((pool -> scheme_name) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool->scheme_name");
      return 0;
    };
  }while (0);
/* Check that the pool's scheme is loaded */
  if (purple_certificate_find_scheme((pool -> scheme_name)) == ((PurpleCertificateScheme *)((void *)0))) {
    return 0;
  }
  return (!0);
}

PurpleCertificateScheme *purple_certificate_pool_get_scheme(PurpleCertificatePool *pool)
{
  do {
    if (pool != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool");
      return 0;
    };
  }while (0);
  do {
    if ((pool -> scheme_name) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool->scheme_name");
      return 0;
    };
  }while (0);
  return purple_certificate_find_scheme((pool -> scheme_name));
}

gboolean purple_certificate_pool_contains(PurpleCertificatePool *pool,const gchar *id)
{
  do {
    if (pool != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool");
      return 0;
    };
  }while (0);
  do {
    if (id != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id");
      return 0;
    };
  }while (0);
  do {
    if ((pool -> cert_in_pool) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool->cert_in_pool");
      return 0;
    };
  }while (0);
  return ( *(pool -> cert_in_pool))(id);
}

PurpleCertificate *purple_certificate_pool_retrieve(PurpleCertificatePool *pool,const gchar *id)
{
  do {
    if (pool != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool");
      return 0;
    };
  }while (0);
  do {
    if (id != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id");
      return 0;
    };
  }while (0);
  do {
    if ((pool -> get_cert) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool->get_cert");
      return 0;
    };
  }while (0);
  return ( *(pool -> get_cert))(id);
}

gboolean purple_certificate_pool_store(PurpleCertificatePool *pool,const gchar *id,PurpleCertificate *crt)
{
  gboolean ret = 0;
  do {
    if (pool != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool");
      return 0;
    };
  }while (0);
  do {
    if (id != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id");
      return 0;
    };
  }while (0);
  do {
    if ((pool -> put_cert) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool->put_cert");
      return 0;
    };
  }while (0);
/* Whether crt->scheme matches find_scheme(pool->scheme_name) is not
	   relevant... I think... */
  do {
    if (g_ascii_strcasecmp((pool -> scheme_name),( *(crt -> scheme)).name) == 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"g_ascii_strcasecmp(pool->scheme_name, crt->scheme->name) == 0");
      return 0;
    };
  }while (0);
  ret = ( *(pool -> put_cert))(id,crt);
/* Signal that the certificate was stored if success*/
  if (ret != 0) {
    purple_signal_emit(pool,"certificate-stored",pool,id);
  }
  return ret;
}

gboolean purple_certificate_pool_delete(PurpleCertificatePool *pool,const gchar *id)
{
  gboolean ret = 0;
  do {
    if (pool != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool");
      return 0;
    };
  }while (0);
  do {
    if (id != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id");
      return 0;
    };
  }while (0);
  do {
    if ((pool -> delete_cert) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool->delete_cert");
      return 0;
    };
  }while (0);
  ret = ( *(pool -> delete_cert))(id);
/* Signal that the certificate was deleted if success */
  if (ret != 0) {
    purple_signal_emit(pool,"certificate-deleted",pool,id);
  }
  return ret;
}

GList *purple_certificate_pool_get_idlist(PurpleCertificatePool *pool)
{
  do {
    if (pool != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool");
      return 0;
    };
  }while (0);
  do {
    if ((pool -> get_idlist) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool->get_idlist");
      return 0;
    };
  }while (0);
  return ( *(pool -> get_idlist))();
}

void purple_certificate_pool_destroy_idlist(GList *idlist)
{
  GList *l;
/* Iterate through and free them strings */
  for (l = idlist; l != 0; l = (l -> next)) {
    g_free((l -> data));
  }
  g_list_free(idlist);
}
/****************************************************************************/
/* Builtin Verifiers, Pools, etc.                                           */
/****************************************************************************/

static void x509_singleuse_verify_cb(PurpleCertificateVerificationRequest *vrq,gint id)
{
  do {
    if (vrq != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"vrq");
      return ;
    };
  }while (0);
  purple_debug_info("certificate/x509_singleuse","VRQ on cert from %s gave %d\n",(vrq -> subject_name),id);
/* Signal what happened back to the caller */
  if (1 == id) {
/* Accepted! */
    purple_certificate_verify_complete(vrq,PURPLE_CERTIFICATE_VALID);
  }
  else {
/* Not accepted */
    purple_certificate_verify_complete(vrq,PURPLE_CERTIFICATE_INVALID);
  }
}

static void x509_singleuse_start_verify(PurpleCertificateVerificationRequest *vrq)
{
  gchar *sha_asc;
  GByteArray *sha_bin;
  gchar *cn;
  const gchar *cn_match;
  gchar *primary;
  gchar *secondary;
  PurpleCertificate *crt = (PurpleCertificate *)( *(vrq -> cert_chain)).data;
/* Pull out the SHA1 checksum */
  sha_bin = purple_certificate_get_fingerprint_sha1(crt);
/* Now decode it for display */
  sha_asc = purple_base16_encode_chunked((sha_bin -> data),(sha_bin -> len));
/* Get the cert Common Name */
  cn = purple_certificate_get_subject_name(crt);
/* Determine whether the name matches */
  if (purple_certificate_check_subject_name(crt,(vrq -> subject_name)) != 0) {
    cn_match = "";
  }
  else {
    cn_match = ((const char *)(dgettext("pidgin","(DOES NOT MATCH)")));
  }
/* Make messages */
  primary = g_strdup_printf(((const char *)(dgettext("pidgin","%s has presented the following certificate for just-this-once use:"))),(vrq -> subject_name));
  secondary = g_strdup_printf(((const char *)(dgettext("pidgin","Common name: %s %s\nFingerprint (SHA1): %s"))),cn,cn_match,sha_asc);
/* Make a semi-pretty display */
  purple_request_action((vrq -> cb_data),((const char *)(dgettext("pidgin","Single-use Certificate Verification"))),primary,secondary,0,0,0,0,vrq,2,((const char *)(dgettext("pidgin","_Accept"))),x509_singleuse_verify_cb,((const char *)(dgettext("pidgin","_Cancel"))),x509_singleuse_verify_cb);
/* TODO: Find what the handle ought to be */
/* Accept by default */
/* No account */
/* No other user */
/* No associated conversation */
/* Cleanup */
  g_free(cn);
  g_free(primary);
  g_free(secondary);
  g_free(sha_asc);
  g_byte_array_free(sha_bin,(!0));
}

static void x509_singleuse_destroy_request(PurpleCertificateVerificationRequest *vrq)
{
/* I don't do anything! */
}
static PurpleCertificateVerifier x509_singleuse = {("x509"), ("singleuse"), (x509_singleuse_start_verify), (x509_singleuse_destroy_request), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* Scheme name */
/* Verifier name */
/* start_verification function */
/* Request cleanup operation */
};
/***** X.509 Certificate Authority pool, keyed by Distinguished Name *****/
/* This is implemented in what may be the most inefficient and bugprone way
   possible; however, future optimizations should not be difficult. */
extern PurpleCertificatePool x509_ca;
/** Holds a key-value pair for quickish certificate lookup */
typedef struct __unnamed_class___F0_L753_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__dn__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L350R__Pe___variable_name_unknown_scope_and_name__scope__crt {
gchar *dn;
PurpleCertificate *crt;}x509_ca_element;

static void x509_ca_element_free(x509_ca_element *el)
{
  if (((x509_ca_element *)((void *)0)) == el) 
    return ;
  g_free((el -> dn));
  purple_certificate_destroy((el -> crt));
  g_free(el);
}
/** System directory to probe for CA certificates */
/* This is set in the lazy_init function */
static GList *x509_ca_paths = (GList *)((void *)0);
/** A list of loaded CAs, populated from the above path whenever the lazy_init
    happens. Contains pointers to x509_ca_elements */
static GList *x509_ca_certs = (GList *)((void *)0);
/** Used for lazy initialization purposes. */
static gboolean x509_ca_initialized = 0;
/** Adds a certificate to the in-memory cache, doing nothing else */

static gboolean x509_ca_quiet_put_cert(PurpleCertificate *crt)
{
  x509_ca_element *el;
/* lazy_init calls this function, so calling lazy_init here is a
	   Bad Thing */
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return 0;
    };
  }while (0);
  do {
    if ((crt -> scheme) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme");
      return 0;
    };
  }while (0);
/* Make sure that this is some kind of X.509 certificate */
/* TODO: Perhaps just check crt->scheme->name instead? */
  do {
    if ((crt -> scheme) == purple_certificate_find_scheme(x509_ca.scheme_name)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme == purple_certificate_find_scheme(x509_ca.scheme_name)");
      return 0;
    };
  }while (0);
  el = ((x509_ca_element *)(g_malloc0_n(1,(sizeof(x509_ca_element )))));
  el -> dn = purple_certificate_get_unique_id(crt);
  el -> crt = purple_certificate_copy(crt);
  x509_ca_certs = g_list_prepend(x509_ca_certs,el);
  return (!0);
}
/* Since the libpurple CertificatePools get registered before plugins are
   loaded, an X.509 Scheme is generally not available when x509_ca_init is
   called, but x509_ca requires X.509 operations in order to properly load.
   To solve this, I present the lazy_init function. It attempts to finish
   initialization of the Pool, but it usually fails when it is called from
   x509_ca_init. However, this is OK; initialization is then simply deferred
   until someone tries to use functions from the pool. */

static gboolean x509_ca_lazy_init()
{
  PurpleCertificateScheme *x509;
  GDir *certdir;
  const gchar *entry;
  GPatternSpec *pempat;
  GPatternSpec *crtpat;
  GList *iter = (GList *)((void *)0);
  GSList *crts = (GSList *)((void *)0);
  if (x509_ca_initialized != 0) 
    return (!0);
/* Check that X.509 is registered */
  x509 = purple_certificate_find_scheme(x509_ca.scheme_name);
  if (!(x509 != 0)) {
    purple_debug_warning("certificate/x509/ca","Lazy init failed because an X.509 Scheme is not yet registered. Maybe it will be better later.\n");
    return 0;
  }
/* Use a glob to only read .pem files */
  pempat = g_pattern_spec_new("*.pem");
  crtpat = g_pattern_spec_new("*.crt");
/* Populate the certificates pool from the search path(s) */
  for (iter = x509_ca_paths; iter != 0; iter = (iter -> next)) {
    certdir = g_dir_open((iter -> data),0,0);
    if (!(certdir != 0)) {
      purple_debug_error("certificate/x509/ca","Couldn\'t open location \'%s\'\n",((const char *)(iter -> data)));
      continue; 
    }
    while((entry = g_dir_read_name(certdir)) != 0){{
        gchar *fullpath;
        PurpleCertificate *crt;
        if (!(g_pattern_match_string(pempat,entry) != 0) && !(g_pattern_match_string(crtpat,entry) != 0)) {
          continue; 
        }
        fullpath = g_build_filename((iter -> data),entry,((void *)((void *)0)));
/* TODO: Respond to a failure in the following? */
        crts = purple_certificates_import(x509,fullpath);
        while((crts != 0) && ((crts -> data) != 0)){
          crt = (crts -> data);
          if (x509_ca_quiet_put_cert(crt) != 0) {
            gchar *name;
            name = purple_certificate_get_subject_name(crt);
            purple_debug_info("certificate/x509/ca","Loaded %s from %s\n",((name != 0)?name : "(unknown)"),fullpath);
            g_free(name);
          }
          else {
            purple_debug_error("certificate/x509/ca","Failed to load certificate from %s\n",fullpath);
          }
          purple_certificate_destroy(crt);
          crts = g_slist_delete_link(crts,crts);
        }
        g_free(fullpath);
      }
    }
    g_dir_close(certdir);
  }
  g_pattern_spec_free(pempat);
  g_pattern_spec_free(crtpat);
  purple_debug_info("certificate/x509/ca","Lazy init completed.\n");
  x509_ca_initialized = (!0);
  return (!0);
}

static gboolean x509_ca_init()
{
/* Attempt to point at the appropriate system path */
  if (((GList *)((void *)0)) == x509_ca_paths) {
#ifdef _WIN32
#else
# ifdef SSL_CERTIFICATES_DIR
# endif
    x509_ca_paths = g_list_append(x509_ca_paths,(g_build_filename("/usr/local/share","purple","ca-certs",((void *)((void *)0)))));
#endif
  }
/* Attempt to initialize now, but if it doesn't work, that's OK;
	   it will get done later */
  if (!(x509_ca_lazy_init() != 0)) {
    purple_debug_info("certificate/x509/ca","Init failed, probably because a dependency is not yet registered. It has been deferred to later.\n");
  }
  return (!0);
}

static void x509_ca_uninit()
{
  GList *l;
  for (l = x509_ca_certs; l != 0; l = (l -> next)) {
    x509_ca_element *el = (l -> data);
    x509_ca_element_free(el);
  }
  g_list_free(x509_ca_certs);
  x509_ca_certs = ((GList *)((void *)0));
  x509_ca_initialized = 0;
  g_list_foreach(x509_ca_paths,((GFunc )g_free),0);
  g_list_free(x509_ca_paths);
  x509_ca_paths = ((GList *)((void *)0));
}
/** Look up a ca_element by dn */

static x509_ca_element *x509_ca_locate_cert(GList *lst,const gchar *dn)
{
  GList *cur;
  for (cur = lst; cur != 0; cur = (cur -> next)) {
    x509_ca_element *el = (cur -> data);
    if (purple_strequal(dn,(el -> dn)) != 0) {
      return el;
    }
  }
  return 0;
}

static GSList *x509_ca_locate_certs(GList *lst,const gchar *dn)
{
  GList *cur;
  GSList *crts = (GSList *)((void *)0);
  for (cur = lst; cur != 0; cur = (cur -> next)) {
    x509_ca_element *el = (cur -> data);
    if (purple_strequal(dn,(el -> dn)) != 0) {
      crts = g_slist_prepend(crts,el);
    }
  }
  return crts;
}

static gboolean x509_ca_cert_in_pool(const gchar *id)
{
  do {
    if (x509_ca_lazy_init() != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"x509_ca_lazy_init()");
      return 0;
    };
  }while (0);
  do {
    if (id != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id");
      return 0;
    };
  }while (0);
  if (x509_ca_locate_cert(x509_ca_certs,id) != ((x509_ca_element *)((void *)0))) {
    return (!0);
  }
  else {
    return 0;
  }
  return 0;
}

static PurpleCertificate *x509_ca_get_cert(const gchar *id)
{
  PurpleCertificate *crt = (PurpleCertificate *)((void *)0);
  x509_ca_element *el;
  do {
    if (x509_ca_lazy_init() != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"x509_ca_lazy_init()");
      return 0;
    };
  }while (0);
  do {
    if (id != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id");
      return 0;
    };
  }while (0);
/* Search the memory-cached pool */
  el = x509_ca_locate_cert(x509_ca_certs,id);
  if (el != ((x509_ca_element *)((void *)0))) {
/* Make a copy of the memcached one for the function caller
		   to play with */
    crt = purple_certificate_copy((el -> crt));
  }
  else {
    crt = ((PurpleCertificate *)((void *)0));
  }
  return crt;
}

static GSList *x509_ca_get_certs(const gchar *id)
{
  GSList *crts = (GSList *)((void *)0);
  GSList *els = (GSList *)((void *)0);
  do {
    if (x509_ca_lazy_init() != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"x509_ca_lazy_init()");
      return 0;
    };
  }while (0);
  do {
    if (id != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id");
      return 0;
    };
  }while (0);
/* Search the memory-cached pool */
  els = x509_ca_locate_certs(x509_ca_certs,id);
  if (els != ((GSList *)((void *)0))) {
    GSList *cur;
/* Make a copy of the memcached ones for the function caller
		   to play with */
    for (cur = els; cur != 0; cur = (cur -> next)) {
      x509_ca_element *el = (cur -> data);
      crts = g_slist_prepend(crts,(purple_certificate_copy((el -> crt))));
    }
    g_slist_free(els);
  }
  return crts;
}

static gboolean x509_ca_put_cert(const gchar *id,PurpleCertificate *crt)
{
  gboolean ret = 0;
  do {
    if (x509_ca_lazy_init() != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"x509_ca_lazy_init()");
      return 0;
    };
  }while (0);
/* TODO: This is a quick way of doing this. At some point the change
	   ought to be flushed to disk somehow. */
  ret = x509_ca_quiet_put_cert(crt);
  return ret;
}

static gboolean x509_ca_delete_cert(const gchar *id)
{
  x509_ca_element *el;
  do {
    if (x509_ca_lazy_init() != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"x509_ca_lazy_init()");
      return 0;
    };
  }while (0);
  do {
    if (id != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id");
      return 0;
    };
  }while (0);
/* Is the id even in the pool? */
  el = x509_ca_locate_cert(x509_ca_certs,id);
  if (el == ((x509_ca_element *)((void *)0))) {
    purple_debug_warning("certificate/x509/ca","Id %s wasn\'t in the pool\n",id);
    return 0;
  }
/* Unlink it from the memory cache and destroy it */
  x509_ca_certs = g_list_remove(x509_ca_certs,el);
  x509_ca_element_free(el);
  return (!0);
}

static GList *x509_ca_get_idlist()
{
  GList *l;
  GList *idlist;
  do {
    if (x509_ca_lazy_init() != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"x509_ca_lazy_init()");
      return 0;
    };
  }while (0);
  idlist = ((GList *)((void *)0));
  for (l = x509_ca_certs; l != 0; l = (l -> next)) {
    x509_ca_element *el = (l -> data);
    idlist = g_list_prepend(idlist,(g_strdup((el -> dn))));
  }
  return idlist;
}
static PurpleCertificatePool x509_ca = {("x509"), ("ca"), ("Certificate Authorities"), ((gpointer )((void *)0)), (x509_ca_init), (x509_ca_uninit), (x509_ca_cert_in_pool), (x509_ca_get_cert), (x509_ca_put_cert), (x509_ca_delete_cert), (x509_ca_get_idlist), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* Scheme name */
/* Pool name */
/* User-friendly name */
/* Internal data */
/* init */
/* uninit */
/* Certificate exists? */
/* Cert retriever */
/* Cert writer */
/* Cert remover */
/* idlist retriever */
};
/***** Cache of certificates given by TLS/SSL peers *****/
extern PurpleCertificatePool x509_tls_peers;

static gboolean x509_tls_peers_init()
{
  gchar *poolpath;
  int ret;
/* Set up key cache here if it isn't already done */
  poolpath = purple_certificate_pool_mkpath(&x509_tls_peers,0);
/* Make it this user only */
  ret = purple_build_dir(poolpath,0700);
  if (ret != 0) 
    purple_debug_info("certificate/tls_peers","Could not create %s.  Certificates will not be cached.\n",poolpath);
  g_free(poolpath);
  return (!0);
}

static gboolean x509_tls_peers_cert_in_pool(const gchar *id)
{
  gchar *keypath;
  gboolean ret = 0;
  do {
    if (id != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id");
      return 0;
    };
  }while (0);
  keypath = purple_certificate_pool_mkpath(&x509_tls_peers,id);
  ret = g_file_test(keypath,G_FILE_TEST_IS_REGULAR);
  g_free(keypath);
  return ret;
}

static PurpleCertificate *x509_tls_peers_get_cert(const gchar *id)
{
  PurpleCertificateScheme *x509;
  PurpleCertificate *crt;
  gchar *keypath;
  do {
    if (id != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id");
      return 0;
    };
  }while (0);
/* Is it in the pool? */
  if (!(x509_tls_peers_cert_in_pool(id) != 0)) {
    return 0;
  }
/* Look up the X.509 scheme */
  x509 = purple_certificate_find_scheme("x509");
  do {
    if (x509 != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"x509");
      return 0;
    };
  }while (0);
/* Okay, now find and load that key */
  keypath = purple_certificate_pool_mkpath(&x509_tls_peers,id);
  crt = purple_certificate_import(x509,keypath);
  g_free(keypath);
  return crt;
}

static gboolean x509_tls_peers_put_cert(const gchar *id,PurpleCertificate *crt)
{
  gboolean ret = 0;
  gchar *keypath;
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return 0;
    };
  }while (0);
  do {
    if ((crt -> scheme) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme");
      return 0;
    };
  }while (0);
/* Make sure that this is some kind of X.509 certificate */
/* TODO: Perhaps just check crt->scheme->name instead? */
  do {
    if ((crt -> scheme) == purple_certificate_find_scheme(x509_tls_peers.scheme_name)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme == purple_certificate_find_scheme(x509_tls_peers.scheme_name)");
      return 0;
    };
  }while (0);
/* Work out the filename and export */
  keypath = purple_certificate_pool_mkpath(&x509_tls_peers,id);
  ret = purple_certificate_export(keypath,crt);
  g_free(keypath);
  return ret;
}

static gboolean x509_tls_peers_delete_cert(const gchar *id)
{
  gboolean ret = 0;
  gchar *keypath;
  do {
    if (id != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id");
      return 0;
    };
  }while (0);
/* Is the id even in the pool? */
  if (!(x509_tls_peers_cert_in_pool(id) != 0)) {
    purple_debug_warning("certificate/tls_peers","Id %s wasn\'t in the pool\n",id);
    return 0;
  }
/* OK, so work out the keypath and delete the thing */
  keypath = purple_certificate_pool_mkpath(&x509_tls_peers,id);
  if (unlink(keypath) != 0) {
    purple_debug_error("certificate/tls_peers","Unlink of %s failed!\n",keypath);
    ret = 0;
  }
  else {
    ret = (!0);
  }
  g_free(keypath);
  return ret;
}

static GList *x509_tls_peers_get_idlist()
{
  GList *idlist = (GList *)((void *)0);
  GDir *dir;
  const gchar *entry;
  gchar *poolpath;
/* Get a handle on the pool directory */
  poolpath = purple_certificate_pool_mkpath(&x509_tls_peers,0);
  dir = g_dir_open(poolpath,0,0);
/* No flags */
/* Not interested in what the error is */
  g_free(poolpath);
  do {
    if (dir != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"dir");
      return 0;
    };
  }while (0);
/* Traverse the directory listing and create an idlist */
  while((entry = g_dir_read_name(dir)) != ((const gchar *)((void *)0))){
/* Unescape the filename */
    const char *unescaped = purple_unescape_filename(entry);
/* Copy the entry name into our list (GLib owns the original
		   string) */
    idlist = g_list_prepend(idlist,(g_strdup(unescaped)));
  }
/* Release the directory */
  g_dir_close(dir);
  return idlist;
}
static PurpleCertificatePool x509_tls_peers = {("x509"), ("tls_peers"), ("SSL Peers Cache"), ((gpointer )((void *)0)), (x509_tls_peers_init), ((void (*)())((void *)0)), (x509_tls_peers_cert_in_pool), (x509_tls_peers_get_cert), (x509_tls_peers_put_cert), (x509_tls_peers_delete_cert), (x509_tls_peers_get_idlist), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* Scheme name */
/* Pool name */
/* User-friendly name */
/* Internal data */
/* init */
/* uninit not required */
/* Certificate exists? */
/* Cert retriever */
/* Cert writer */
/* Cert remover */
/* idlist retriever */
};
/***** A Verifier that uses the tls_peers cache and the CA pool to validate certificates *****/
extern PurpleCertificateVerifier x509_tls_cached;
/* The following is several hacks piled together and needs to be fixed.
 * It exists because show_cert (see its comments) needs the original reason
 * given to user_auth in order to rebuild the dialog.
 */
/* TODO: This will cause a ua_ctx to become memleaked if the request(s) get
   closed by handle or otherwise abnormally. */
typedef struct __unnamed_class___F0_L1286_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L354R__Pe___variable_name_unknown_scope_and_name__scope__vrq__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__reason {
PurpleCertificateVerificationRequest *vrq;
gchar *reason;}x509_tls_cached_ua_ctx;

static x509_tls_cached_ua_ctx *x509_tls_cached_ua_ctx_new(PurpleCertificateVerificationRequest *vrq,const gchar *reason)
{
  x509_tls_cached_ua_ctx *c;
  c = ((x509_tls_cached_ua_ctx *)(g_malloc0_n(1,(sizeof(x509_tls_cached_ua_ctx )))));
  c -> vrq = vrq;
  c -> reason = g_strdup(reason);
  return c;
}

static void x509_tls_cached_ua_ctx_free(x509_tls_cached_ua_ctx *c)
{
  do {
    if (c != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"c");
      return ;
    };
  }while (0);
  g_free((c -> reason));
  g_free(c);
}
static void x509_tls_cached_user_auth(PurpleCertificateVerificationRequest *vrq,const gchar *reason);

static void x509_tls_cached_show_cert(x509_tls_cached_ua_ctx *c,gint id)
{
  PurpleCertificate *disp_crt = ( *( *(c -> vrq)).cert_chain).data;
/* Since clicking a button closes the request, show it again */
  x509_tls_cached_user_auth((c -> vrq),(c -> reason));
/* Show the certificate AFTER re-opening the dialog so that this
	   appears above the other */
  purple_certificate_display_x509(disp_crt);
  x509_tls_cached_ua_ctx_free(c);
}

static void x509_tls_cached_user_auth_cb(x509_tls_cached_ua_ctx *c,gint id)
{
  PurpleCertificateVerificationRequest *vrq;
  PurpleCertificatePool *tls_peers;
  do {
    if (c != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"c");
      return ;
    };
  }while (0);
  do {
    if ((c -> vrq) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"c->vrq");
      return ;
    };
  }while (0);
  vrq = (c -> vrq);
  x509_tls_cached_ua_ctx_free(c);
  tls_peers = purple_certificate_find_pool("x509","tls_peers");
  if (2 == id) {
    gchar *cache_id = (vrq -> subject_name);
    purple_debug_info("certificate/x509/tls_cached","User ACCEPTED cert\nCaching first in chain for future use as %s...\n",cache_id);
    purple_certificate_pool_store(tls_peers,cache_id,( *(vrq -> cert_chain)).data);
    purple_certificate_verify_complete(vrq,PURPLE_CERTIFICATE_VALID);
  }
  else {
    purple_debug_warning("certificate/x509/tls_cached","User REJECTED cert\n");
    purple_certificate_verify_complete(vrq,PURPLE_CERTIFICATE_INVALID);
  }
}

static void x509_tls_cached_user_auth_accept_cb(x509_tls_cached_ua_ctx *c,gint ignore)
{
  x509_tls_cached_user_auth_cb(c,2);
}

static void x509_tls_cached_user_auth_reject_cb(x509_tls_cached_ua_ctx *c,gint ignore)
{
  x509_tls_cached_user_auth_cb(c,1);
}
/** Validates a certificate by asking the user
 * @param reason    String to explain why the user needs to accept/refuse the
 *                  certificate.
 * @todo Needs a handle argument
 */

static void x509_tls_cached_user_auth(PurpleCertificateVerificationRequest *vrq,const gchar *reason)
{
  gchar *primary;
/* Make messages */
  primary = g_strdup_printf(((const char *)(dgettext("pidgin","Accept certificate for %s\?"))),(vrq -> subject_name));
/* Make a semi-pretty display */
  purple_request_action((vrq -> cb_data),((const char *)(dgettext("pidgin","SSL Certificate Verification"))),primary,reason,0,0,0,0,(x509_tls_cached_ua_ctx_new(vrq,reason)),3,((const char *)(dgettext("pidgin","Accept"))),x509_tls_cached_user_auth_accept_cb,((const char *)(dgettext("pidgin","Reject"))),x509_tls_cached_user_auth_reject_cb,((const char *)(dgettext("pidgin","_View Certificate..."))),x509_tls_cached_show_cert);
/* TODO: Find what the handle ought to be */
/* Accept by default */
/* No account */
/* No other user */
/* No associated conversation */
/* Number of actions */
/* Cleanup */
  g_free(primary);
}
static void x509_tls_cached_unknown_peer(PurpleCertificateVerificationRequest *vrq,PurpleCertificateInvalidityFlags flags);

static void x509_tls_cached_complete(PurpleCertificateVerificationRequest *vrq,PurpleCertificateInvalidityFlags flags)
{
  PurpleCertificatePool *tls_peers;
  PurpleCertificate *peer_crt = ( *(vrq -> cert_chain)).data;
  if ((flags & PURPLE_CERTIFICATE_FATALS_MASK) != 0L) {
/* TODO: Also print any other warnings? */
    const gchar *error;
    gchar *tmp;
    gchar *secondary;
    if ((flags & PURPLE_CERTIFICATE_INVALID_CHAIN) != 0L) 
      error = invalidity_reason_to_string(PURPLE_CERTIFICATE_INVALID_CHAIN);
    else if ((flags & PURPLE_CERTIFICATE_REVOKED) != 0L) 
      error = invalidity_reason_to_string(PURPLE_CERTIFICATE_REVOKED);
    else 
      error = invalidity_reason_to_string(PURPLE_CERTIFICATE_UNKNOWN_ERROR);
    tmp = g_strdup_printf(((const char *)(dgettext("pidgin","The certificate for %s could not be validated."))),(vrq -> subject_name));
    secondary = g_strconcat(tmp," ",error,((void *)((void *)0)));
    g_free(tmp);
/* TODO: Probably wrong. */
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","SSL Certificate Error"))),((const char *)(dgettext("pidgin","Unable to validate certificate"))),secondary,0,0);
    g_free(secondary);
    purple_certificate_verify_complete(vrq,PURPLE_CERTIFICATE_INVALID);
    return ;
  }
  else if ((flags & PURPLE_CERTIFICATE_NON_FATALS_MASK) != 0L) {
/* Non-fatal error. Prompt the user. */
    gchar *tmp;
    GString *errors;
    guint32 i = 1;
    tmp = g_strdup_printf(((const char *)(dgettext("pidgin","The certificate for %s could not be validated."))),(vrq -> subject_name));
    errors = g_string_new(tmp);
    g_free(tmp);
    errors = g_string_append_c_inline(errors,10);
/* Special case a name mismatch because we want to display the two names... */
    if ((flags & PURPLE_CERTIFICATE_NAME_MISMATCH) != 0L) {
      gchar *sn = purple_certificate_get_subject_name(peer_crt);
      if (sn != 0) {
        g_string_append_printf(errors,((const char *)(dgettext("pidgin","The certificate claims to be from \"%s\" instead. This could mean that you are not connecting to the service you believe you are."))),sn);
        g_free(sn);
        flags &= (~PURPLE_CERTIFICATE_NAME_MISMATCH);
      }
    }
    while(i != PURPLE_CERTIFICATE_LAST){
      if ((flags & i) != 0L) {
        errors = g_string_append_c_inline(errors,10);
        g_string_append(errors,invalidity_reason_to_string(i));
      }
      i <<= 1;
    }
    x509_tls_cached_user_auth(vrq,(errors -> str));
    g_string_free(errors,(!0));
    return ;
  }
/* If we reach this point, the certificate is good. */
/* Look up the local cache and store it there for future use */
  tls_peers = purple_certificate_find_pool(x509_tls_cached.scheme_name,"tls_peers");
  if (tls_peers != 0) {
    if (!(purple_certificate_pool_store(tls_peers,(vrq -> subject_name),peer_crt) != 0)) {
      purple_debug_error("certificate/x509/tls_cached","FAILED to cache peer certificate\n");
    }
  }
  else {
    purple_debug_error("certificate/x509/tls_cached","Unable to locate tls_peers certificate cache.\n");
  }
  purple_certificate_verify_complete(vrq,PURPLE_CERTIFICATE_VALID);
}

static void x509_tls_cached_cert_in_cache(PurpleCertificateVerificationRequest *vrq,PurpleCertificateInvalidityFlags flags)
{
/* TODO: Looking this up by name over and over is expensive.
	   Fix, please! */
  PurpleCertificatePool *tls_peers = purple_certificate_find_pool(x509_tls_cached.scheme_name,"tls_peers");
/* The peer's certificate should be the first in the list */
  PurpleCertificate *peer_crt = (PurpleCertificate *)( *(vrq -> cert_chain)).data;
  PurpleCertificate *cached_crt;
  GByteArray *peer_fpr;
  GByteArray *cached_fpr;
/* Load up the cached certificate */
  cached_crt = purple_certificate_pool_retrieve(tls_peers,(vrq -> subject_name));
  if (!(cached_crt != 0)) {
    purple_debug_warning("certificate/x509/tls_cached","Lookup failed on cached certificate!\nFalling back to full verification.\n");
/* vrq now becomes the problem of unknown_peer */
    x509_tls_cached_unknown_peer(vrq,flags);
    return ;
  }
/* Now get SHA1 sums for both and compare them */
/* TODO: This is not an elegant way to compare certs */
  peer_fpr = purple_certificate_get_fingerprint_sha1(peer_crt);
  cached_fpr = purple_certificate_get_fingerprint_sha1(cached_crt);
  if (!(memcmp((peer_fpr -> data),(cached_fpr -> data),(peer_fpr -> len)) != 0)) {
    purple_debug_info("certificate/x509/tls_cached","Peer cert matched cached\n");
    x509_tls_cached_complete(vrq,flags);
  }
  else {
    purple_debug_error("certificate/x509/tls_cached","Peer cert did NOT match cached\n");
/* vrq now becomes the problem of the user */
    x509_tls_cached_unknown_peer(vrq,flags);
  }
  purple_certificate_destroy(cached_crt);
  g_byte_array_free(peer_fpr,(!0));
  g_byte_array_free(cached_fpr,(!0));
}
/*
 * This is called from two points in x509_tls_cached_unknown_peer below
 * once we've verified the signature chain is valid. Now we need to verify
 * the subject name of the certificate.
 */

static void x509_tls_cached_check_subject_name(PurpleCertificateVerificationRequest *vrq,PurpleCertificateInvalidityFlags flags)
{
  PurpleCertificate *peer_crt;
  GList *chain = (vrq -> cert_chain);
  peer_crt = ((PurpleCertificate *)(chain -> data));
/* Last, check that the hostname matches */
  if (!(purple_certificate_check_subject_name(peer_crt,(vrq -> subject_name)) != 0)) {
    gchar *sn = purple_certificate_get_subject_name(peer_crt);
    flags |= PURPLE_CERTIFICATE_NAME_MISMATCH;
    purple_debug_error("certificate/x509/tls_cached","Name mismatch: Certificate given for %s has a name of %s\n",(vrq -> subject_name),sn);
    g_free(sn);
  }
  x509_tls_cached_complete(vrq,flags);
}
/* For when we've never communicated with this party before */
/* TODO: Need ways to specify possibly multiple problems with a cert, or at
   least  reprioritize them.
 */

static void x509_tls_cached_unknown_peer(PurpleCertificateVerificationRequest *vrq,PurpleCertificateInvalidityFlags flags)
{
  PurpleCertificatePool *ca;
  PurpleCertificate *peer_crt;
  PurpleCertificate *ca_crt;
  PurpleCertificate *end_crt;
  PurpleCertificate *failing_crt;
  GList *chain = (vrq -> cert_chain);
  GSList *ca_crts;
  GSList *cur;
  GByteArray *last_fpr;
  GByteArray *ca_fpr;
  gboolean valid = 0;
  gchar *ca_id;
  gchar *ca2_id;
  peer_crt = ((PurpleCertificate *)(chain -> data));
/* TODO: Figure out a way to check for a bad signature, as opposed to
	   "not self-signed" */
  if (purple_certificate_signed_by(peer_crt,peer_crt) != 0) {
    flags |= PURPLE_CERTIFICATE_SELF_SIGNED;
    purple_debug_info("certificate/x509/tls_cached","Certificate for %s is self-signed.\n",(vrq -> subject_name));
    x509_tls_cached_check_subject_name(vrq,flags);
    return ;
/* if (self signed) */
  }
  ca = purple_certificate_find_pool(x509_tls_cached.scheme_name,"ca");
/* Next, check that the certificate chain is valid */
  if (!(purple_certificate_check_signature_chain_with_failing(chain,&failing_crt) != 0)) {
    gboolean chain_validated = 0;
/*
		 * Check if the failing certificate is in the CA store. If it is, then
		 * consider this fully validated. This works around issues with some
		 * prominent intermediate CAs whose signature is md5WithRSAEncryption.
		 * I'm looking at CACert Class 3 here. See #4458 for details.
		 */
    if (ca != 0) {
      gchar *uid = purple_certificate_get_unique_id(failing_crt);
      PurpleCertificate *ca_crt = purple_certificate_pool_retrieve(ca,uid);
      if (ca_crt != ((PurpleCertificate *)((void *)0))) {
        GByteArray *failing_fpr;
        GByteArray *ca_fpr;
        failing_fpr = purple_certificate_get_fingerprint_sha1(failing_crt);
        ca_fpr = purple_certificate_get_fingerprint_sha1(ca_crt);
        if (byte_arrays_equal(failing_fpr,ca_fpr) != 0) {
          purple_debug_info("certificate/x509/tls_cached","Full chain verification failed (probably a bad signature algorithm), but found the last certificate %s in the CA pool.\n",uid);
          chain_validated = (!0);
        }
        g_byte_array_free(failing_fpr,(!0));
        g_byte_array_free(ca_fpr,(!0));
      }
      purple_certificate_destroy(ca_crt);
      g_free(uid);
    }
/*
		 * If we get here, either the cert matched the stuff right above
		 * or it didn't, in which case we give up and complain to the user.
		 */
    if (!(chain_validated != 0)) 
/* TODO: Tell the user where the chain broke? */
      flags |= PURPLE_CERTIFICATE_INVALID_CHAIN;
    x509_tls_cached_check_subject_name(vrq,flags);
    return ;
/* if (signature chain not good) */
  }
/* Next, attempt to verify the last certificate is signed by a trusted
	 * CA, or is a trusted CA (based on fingerprint).
	 */
/* If, for whatever reason, there is no Certificate Authority pool
	   loaded, we'll verify the subject name and then warn about thsi. */
  if (!(ca != 0)) {
    purple_debug_error("certificate/x509/tls_cached","No X.509 Certificate Authority pool could be found!\n");
    flags |= PURPLE_CERTIFICATE_NO_CA_POOL;
    x509_tls_cached_check_subject_name(vrq,flags);
    return ;
  }
  end_crt = ( *g_list_last(chain)).data;
/* Attempt to look up the last certificate, and the last certificate's
	 * issuer. 
	 */
  ca_id = purple_certificate_get_issuer_unique_id(end_crt);
  ca2_id = purple_certificate_get_unique_id(end_crt);
  purple_debug_info("certificate/x509/tls_cached","Checking for a CA with DN=%s\n",ca_id);
  purple_debug_info("certificate/x509/tls_cached","Also checking for a CA with DN=%s\n",ca2_id);
  ca_crts = g_slist_concat(x509_ca_get_certs(ca_id),x509_ca_get_certs(ca2_id));
  g_free(ca_id);
  g_free(ca2_id);
  if (((GSList *)((void *)0)) == ca_crts) {
    flags |= PURPLE_CERTIFICATE_CA_UNKNOWN;
    purple_debug_warning("certificate/x509/tls_cached","No Certificate Authorities with either DN found found. I\'ll prompt the user, I guess.\n");
    x509_tls_cached_check_subject_name(vrq,flags);
    return ;
  }
/*
	 * Check the fingerprints; if they match, then this certificate *is* one
	 * of the designated "trusted roots", and we don't need to verify the
	 * signature. This is good because some of the older roots are self-signed
	 * with bad hash algorithms that we don't want to allow in any other
	 * circumstances (one of Verisign's root CAs is self-signed with MD2).
	 *
	 * If the fingerprints don't match, we'll fall back to checking the
	 * signature.
	 */
  last_fpr = purple_certificate_get_fingerprint_sha1(end_crt);
{
    for (cur = ca_crts; cur != 0; cur = (cur -> next)) {
      ca_crt = (cur -> data);
      ca_fpr = purple_certificate_get_fingerprint_sha1(ca_crt);
      if ((byte_arrays_equal(last_fpr,ca_fpr) != 0) || (purple_certificate_signed_by(end_crt,ca_crt) != 0)) {
/* TODO: If signed_by ever returns a reason, maybe mention
			   that, too. */
/* TODO: Also mention the CA involved. While I could do this
			   now, a full DN is a little much with which to assault the
			   user's poor, leaky eyes. */
        valid = (!0);
        g_byte_array_free(ca_fpr,(!0));
        break; 
      }
      g_byte_array_free(ca_fpr,(!0));
    }
  }
  if (valid == 0) 
    flags |= PURPLE_CERTIFICATE_INVALID_CHAIN;
  g_slist_foreach(ca_crts,((GFunc )purple_certificate_destroy),0);
  g_slist_free(ca_crts);
  g_byte_array_free(last_fpr,(!0));
  x509_tls_cached_check_subject_name(vrq,flags);
}

static void x509_tls_cached_start_verify(PurpleCertificateVerificationRequest *vrq)
{
/* Name of local cache */
  const gchar *tls_peers_name = "tls_peers";
  PurpleCertificatePool *tls_peers;
  time_t now;
  time_t activation;
  time_t expiration;
  PurpleCertificateInvalidityFlags flags = PURPLE_CERTIFICATE_NO_PROBLEMS;
  gboolean ret;
  do {
    if (vrq != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"vrq");
      return ;
    };
  }while (0);
  purple_debug_info("certificate/x509/tls_cached","Starting verify for %s\n",(vrq -> subject_name));
/*
	 * Verify the first certificate (the main one) has been activated and
	 * isn't expired, i.e. activation < now < expiration.
	 */
  now = time(0);
  ret = purple_certificate_get_times(( *(vrq -> cert_chain)).data,&activation,&expiration);
  if (!(ret != 0)) {
    flags |= PURPLE_CERTIFICATE_EXPIRED | PURPLE_CERTIFICATE_NOT_ACTIVATED;
    purple_debug_error("certificate/x509/tls_cached","Failed to get validity times for certificate %s\n",(vrq -> subject_name));
  }
  else if (now > expiration) {
    flags |= PURPLE_CERTIFICATE_EXPIRED;
    purple_debug_error("certificate/x509/tls_cached","Certificate %s expired at %s\n",(vrq -> subject_name),ctime((&expiration)));
  }
  else if (now < activation) {
    flags |= PURPLE_CERTIFICATE_NOT_ACTIVATED;
    purple_debug_error("certificate/x509/tls_cached","Certificate %s is not yet valid, will be at %s\n",(vrq -> subject_name),ctime((&activation)));
  }
  tls_peers = purple_certificate_find_pool(x509_tls_cached.scheme_name,tls_peers_name);
  if (!(tls_peers != 0)) {
    purple_debug_error("certificate/x509/tls_cached","Couldn\'t find local peers cache %s\n",tls_peers_name);
/* vrq now becomes the problem of unknown_peer */
    x509_tls_cached_unknown_peer(vrq,flags);
    return ;
  }
/* Check if the peer has a certificate cached already */
  purple_debug_info("certificate/x509/tls_cached","Checking for cached cert...\n");
  if (purple_certificate_pool_contains(tls_peers,(vrq -> subject_name)) != 0) {
    purple_debug_info("certificate/x509/tls_cached","...Found cached cert\n");
/* vrq is now the responsibility of cert_in_cache */
    x509_tls_cached_cert_in_cache(vrq,flags);
  }
  else {
    purple_debug_warning("certificate/x509/tls_cached","...Not in cache\n");
/* vrq now becomes the problem of unknown_peer */
    x509_tls_cached_unknown_peer(vrq,flags);
  }
}

static void x509_tls_cached_destroy_request(PurpleCertificateVerificationRequest *vrq)
{
  do {
    if (vrq != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"vrq");
      return ;
    };
  }while (0);
}
static PurpleCertificateVerifier x509_tls_cached = {("x509"), ("tls_cached"), (x509_tls_cached_start_verify), (x509_tls_cached_destroy_request), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* Scheme name */
/* Verifier name */
/* Verification begin */
/* Request cleanup */
};
/****************************************************************************/
/* Subsystem                                                                */
/****************************************************************************/

void purple_certificate_init()
{
/* Register builtins */
  purple_certificate_register_verifier(&x509_singleuse);
  purple_certificate_register_pool(&x509_ca);
  purple_certificate_register_pool(&x509_tls_peers);
  purple_certificate_register_verifier(&x509_tls_cached);
}

void purple_certificate_uninit()
{
/* Unregister all Verifiers */
  g_list_foreach(cert_verifiers,((GFunc )purple_certificate_unregister_verifier),0);
/* Unregister all Pools */
  g_list_foreach(cert_pools,((GFunc )purple_certificate_unregister_pool),0);
}

gpointer purple_certificate_get_handle()
{
  static gint handle;
  return (&handle);
}

PurpleCertificateScheme *purple_certificate_find_scheme(const gchar *name)
{
  PurpleCertificateScheme *scheme = (PurpleCertificateScheme *)((void *)0);
  GList *l;
  do {
    if (name != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name");
      return 0;
    };
  }while (0);
/* Traverse the list of registered schemes and locate the
	   one whose name matches */
  for (l = cert_schemes; l != 0; l = (l -> next)) {
    scheme = ((PurpleCertificateScheme *)(l -> data));
/* Name matches? that's our man */
    if (!(g_ascii_strcasecmp((scheme -> name),name) != 0)) 
      return scheme;
  }
  purple_debug_warning("certificate","CertificateScheme %s requested but not found.\n",name);
/* TODO: Signalling and such? */
  return 0;
}

GList *purple_certificate_get_schemes()
{
  return cert_schemes;
}

gboolean purple_certificate_register_scheme(PurpleCertificateScheme *scheme)
{
  do {
    if (scheme != ((PurpleCertificateScheme *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"scheme != NULL");
      return 0;
    };
  }while (0);
/* Make sure no scheme is registered with the same name */
  if (purple_certificate_find_scheme((scheme -> name)) != ((PurpleCertificateScheme *)((void *)0))) {
    return 0;
  }
/* Okay, we're golden. Register it. */
  cert_schemes = g_list_prepend(cert_schemes,scheme);
/* TODO: Signalling and such? */
  purple_debug_info("certificate","CertificateScheme %s registered\n",(scheme -> name));
  return (!0);
}

gboolean purple_certificate_unregister_scheme(PurpleCertificateScheme *scheme)
{
  if (((PurpleCertificateScheme *)((void *)0)) == scheme) {
    purple_debug_warning("certificate","Attempting to unregister NULL scheme\n");
    return 0;
  }
/* TODO: signalling? */
/* TODO: unregister all CertificateVerifiers for this scheme?*/
/* TODO: unregister all CertificatePools for this scheme? */
/* Neither of the above should be necessary, though */
  cert_schemes = g_list_remove(cert_schemes,scheme);
  purple_debug_info("certificate","CertificateScheme %s unregistered\n",(scheme -> name));
  return (!0);
}

PurpleCertificateVerifier *purple_certificate_find_verifier(const gchar *scheme_name,const gchar *ver_name)
{
  PurpleCertificateVerifier *vr = (PurpleCertificateVerifier *)((void *)0);
  GList *l;
  do {
    if (scheme_name != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"scheme_name");
      return 0;
    };
  }while (0);
  do {
    if (ver_name != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ver_name");
      return 0;
    };
  }while (0);
/* Traverse the list of registered verifiers and locate the
	   one whose name matches */
  for (l = cert_verifiers; l != 0; l = (l -> next)) {
    vr = ((PurpleCertificateVerifier *)(l -> data));
/* Scheme and name match? */
    if (!(g_ascii_strcasecmp((vr -> scheme_name),scheme_name) != 0) && !(g_ascii_strcasecmp((vr -> name),ver_name) != 0)) 
      return vr;
  }
  purple_debug_warning("certificate","CertificateVerifier %s, %s requested but not found.\n",scheme_name,ver_name);
/* TODO: Signalling and such? */
  return 0;
}

GList *purple_certificate_get_verifiers()
{
  return cert_verifiers;
}

gboolean purple_certificate_register_verifier(PurpleCertificateVerifier *vr)
{
  do {
    if (vr != ((PurpleCertificateVerifier *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"vr != NULL");
      return 0;
    };
  }while (0);
/* Make sure no verifier is registered with the same scheme/name */
  if (purple_certificate_find_verifier((vr -> scheme_name),(vr -> name)) != ((PurpleCertificateVerifier *)((void *)0))) {
    return 0;
  }
/* Okay, we're golden. Register it. */
  cert_verifiers = g_list_prepend(cert_verifiers,vr);
/* TODO: Signalling and such? */
  purple_debug_info("certificate","CertificateVerifier %s registered\n",(vr -> name));
  return (!0);
}

gboolean purple_certificate_unregister_verifier(PurpleCertificateVerifier *vr)
{
  if (((PurpleCertificateVerifier *)((void *)0)) == vr) {
    purple_debug_warning("certificate","Attempting to unregister NULL verifier\n");
    return 0;
  }
/* TODO: signalling? */
  cert_verifiers = g_list_remove(cert_verifiers,vr);
  purple_debug_info("certificate","CertificateVerifier %s unregistered\n",(vr -> name));
  return (!0);
}

PurpleCertificatePool *purple_certificate_find_pool(const gchar *scheme_name,const gchar *pool_name)
{
  PurpleCertificatePool *pool = (PurpleCertificatePool *)((void *)0);
  GList *l;
  do {
    if (scheme_name != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"scheme_name");
      return 0;
    };
  }while (0);
  do {
    if (pool_name != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool_name");
      return 0;
    };
  }while (0);
/* Traverse the list of registered pools and locate the
	   one whose name matches */
  for (l = cert_pools; l != 0; l = (l -> next)) {
    pool = ((PurpleCertificatePool *)(l -> data));
/* Scheme and name match? */
    if (!(g_ascii_strcasecmp((pool -> scheme_name),scheme_name) != 0) && !(g_ascii_strcasecmp((pool -> name),pool_name) != 0)) 
      return pool;
  }
  purple_debug_warning("certificate","CertificatePool %s, %s requested but not found.\n",scheme_name,pool_name);
/* TODO: Signalling and such? */
  return 0;
}

GList *purple_certificate_get_pools()
{
  return cert_pools;
}

gboolean purple_certificate_register_pool(PurpleCertificatePool *pool)
{
  do {
    if (pool != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool");
      return 0;
    };
  }while (0);
  do {
    if ((pool -> scheme_name) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool->scheme_name");
      return 0;
    };
  }while (0);
  do {
    if ((pool -> name) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool->name");
      return 0;
    };
  }while (0);
  do {
    if ((pool -> fullname) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"pool->fullname");
      return 0;
    };
  }while (0);
/* Make sure no pools are registered under this name */
  if (purple_certificate_find_pool((pool -> scheme_name),(pool -> name)) != 0) {
    return 0;
  }
/* Initialize the pool if needed */
  if ((pool -> init) != 0) {
    gboolean success;
    success = ( *(pool -> init))();
    if (!(success != 0)) 
      return 0;
  }
/* Register the Pool */
  cert_pools = g_list_prepend(cert_pools,pool);
/* TODO: Emit a signal that the pool got registered */
{
    PurpleCertificatePool *typed_ptr = pool;
    purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurpleCertificatePool);
  };
/* Signals emitted from pool */
  purple_signal_register(pool,"certificate-stored",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CERTIFICATEPOOL),purple_value_new(PURPLE_TYPE_STRING));
/* No callback return value */
/* Two non-data arguments */
/* Signals emitted from pool */
  purple_signal_register(pool,"certificate-deleted",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CERTIFICATEPOOL),purple_value_new(PURPLE_TYPE_STRING));
/* No callback return value */
/* Two non-data arguments */
  purple_debug_info("certificate","CertificatePool %s registered\n",(pool -> name));
  return (!0);
}

gboolean purple_certificate_unregister_pool(PurpleCertificatePool *pool)
{
  if (((PurpleCertificatePool *)((void *)0)) == pool) {
    purple_debug_warning("certificate","Attempting to unregister NULL pool\n");
    return 0;
  }
/* Check that the pool is registered */
  if (!(g_list_find(cert_pools,pool) != 0)) {
    purple_debug_warning("certificate","Pool to unregister isn\'t registered!\n");
    return 0;
  }
/* Uninit the pool if needed */
  purple_dbus_unregister_pointer(pool);
  if ((pool -> uninit) != 0) {
    ( *(pool -> uninit))();
  }
  cert_pools = g_list_remove(cert_pools,pool);
/* TODO: Signalling? */
  purple_signal_unregister(pool,"certificate-stored");
  purple_signal_unregister(pool,"certificate-deleted");
  purple_debug_info("certificate","CertificatePool %s unregistered\n",(pool -> name));
  return (!0);
}
/****************************************************************************/
/* Scheme-specific functions                                                */
/****************************************************************************/

void purple_certificate_display_x509(PurpleCertificate *crt)
{
  gchar *sha_asc;
  GByteArray *sha_bin;
  gchar *cn;
  time_t activation;
  time_t expiration;
  gchar *activ_str;
  gchar *expir_str;
  gchar *secondary;
/* Pull out the SHA1 checksum */
  sha_bin = purple_certificate_get_fingerprint_sha1(crt);
/* Now decode it for display */
  sha_asc = purple_base16_encode_chunked((sha_bin -> data),(sha_bin -> len));
/* Get the cert Common Name */
/* TODO: Will break on CA certs */
  cn = purple_certificate_get_subject_name(crt);
/* Get the certificate times */
/* TODO: Check the times against localtime */
/* TODO: errorcheck? */
  if (!(purple_certificate_get_times(crt,&activation,&expiration) != 0)) {
    purple_debug_error("certificate","Failed to get certificate times!\n");
    activation = (expiration = 0);
  }
  activ_str = g_strdup((ctime((&activation))));
  expir_str = g_strdup((ctime((&expiration))));
/* Make messages */
  secondary = g_strdup_printf(((const char *)(dgettext("pidgin","Common name: %s\n\nFingerprint (SHA1): %s\n\nActivation date: %s\nExpiration date: %s\n"))),((cn != 0)?cn : "(null)"),((sha_asc != 0)?sha_asc : "(null)"),((activ_str != 0)?activ_str : "(null)"),((expir_str != 0)?expir_str : "(null)"));
/* Make a semi-pretty display */
  purple_notify_message(0,PURPLE_NOTIFY_MSG_INFO,((const char *)(dgettext("pidgin","Certificate Information"))),"",secondary,0,0);
/* TODO: Find what the handle ought to be */
/* Cleanup */
  g_free(cn);
  g_free(secondary);
  g_free(sha_asc);
  g_free(activ_str);
  g_free(expir_str);
  g_byte_array_free(sha_bin,(!0));
}

void purple_certificate_add_ca_search_path(const char *path)
{
  if (g_list_find_custom(x509_ca_paths,path,((GCompareFunc )strcmp)) != 0) 
    return ;
  x509_ca_paths = g_list_append(x509_ca_paths,(g_strdup(path)));
}
