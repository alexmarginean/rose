/**
 * @file roomlist.c Room List API
 * @ingroup core
 */
/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "account.h"
#include "connection.h"
#include "debug.h"
#include "roomlist.h"
#include "server.h"
static PurpleRoomlistUiOps *ops = (PurpleRoomlistUiOps *)((void *)0);
/**************************************************************************/
/** @name Room List API                                                   */
/**************************************************************************/
/*@{*/

void purple_roomlist_show_with_account(PurpleAccount *account)
{
  if ((ops != 0) && ((ops -> show_with_account) != 0)) 
    ( *(ops -> show_with_account))(account);
}

PurpleRoomlist *purple_roomlist_new(PurpleAccount *account)
{
  PurpleRoomlist *list;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  list = ((PurpleRoomlist *)(g_malloc0_n(1,(sizeof(PurpleRoomlist )))));
  list -> account = account;
  list -> rooms = ((GList *)((void *)0));
  list -> fields = ((GList *)((void *)0));
  list -> ref = 1;
  if ((ops != 0) && ((ops -> create) != 0)) 
    ( *(ops -> create))(list);
  return list;
}

void purple_roomlist_ref(PurpleRoomlist *list)
{
  do {
    if (list != ((PurpleRoomlist *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"list != NULL");
      return ;
    };
  }while (0);
  list -> ref++;
  purple_debug_misc("roomlist","reffing list, ref count now %d\n",(list -> ref));
}

static void purple_roomlist_room_destroy(PurpleRoomlist *list,PurpleRoomlistRoom *r)
{
  GList *l;
  GList *j;
  for (((l = (list -> fields)) , (j = (r -> fields))); (l != 0) && (j != 0); ((l = (l -> next)) , (j = (j -> next)))) {
    PurpleRoomlistField *f = (l -> data);
    if ((f -> type) == PURPLE_ROOMLIST_FIELD_STRING) 
      g_free((j -> data));
  }
  g_list_free((r -> fields));
  g_free((r -> name));
  g_free(r);
}

static void purple_roomlist_field_destroy(PurpleRoomlistField *f)
{
  g_free((f -> label));
  g_free((f -> name));
  g_free(f);
}

static void purple_roomlist_destroy(PurpleRoomlist *list)
{
  GList *l;
  purple_debug_misc("roomlist","destroying list %p\n",list);
  if ((ops != 0) && ((ops -> destroy) != 0)) 
    ( *(ops -> destroy))(list);
  for (l = (list -> rooms); l != 0; l = (l -> next)) {
    PurpleRoomlistRoom *r = (l -> data);
    purple_roomlist_room_destroy(list,r);
  }
  g_list_free((list -> rooms));
  g_list_foreach((list -> fields),((GFunc )purple_roomlist_field_destroy),0);
  g_list_free((list -> fields));
  g_free(list);
}

void purple_roomlist_unref(PurpleRoomlist *list)
{
  do {
    if (list != ((PurpleRoomlist *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"list != NULL");
      return ;
    };
  }while (0);
  do {
    if ((list -> ref) > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"list->ref > 0");
      return ;
    };
  }while (0);
  list -> ref--;
  purple_debug_misc("roomlist","unreffing list, ref count now %d\n",(list -> ref));
  if ((list -> ref) == 0) 
    purple_roomlist_destroy(list);
}

void purple_roomlist_set_fields(PurpleRoomlist *list,GList *fields)
{
  do {
    if (list != ((PurpleRoomlist *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"list != NULL");
      return ;
    };
  }while (0);
  list -> fields = fields;
  if ((ops != 0) && ((ops -> set_fields) != 0)) 
    ( *(ops -> set_fields))(list,fields);
}

void purple_roomlist_set_in_progress(PurpleRoomlist *list,gboolean in_progress)
{
  do {
    if (list != ((PurpleRoomlist *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"list != NULL");
      return ;
    };
  }while (0);
  list -> in_progress = in_progress;
  if ((ops != 0) && ((ops -> in_progress) != 0)) 
    ( *(ops -> in_progress))(list,in_progress);
}

gboolean purple_roomlist_get_in_progress(PurpleRoomlist *list)
{
  do {
    if (list != ((PurpleRoomlist *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"list != NULL");
      return 0;
    };
  }while (0);
  return list -> in_progress;
}

void purple_roomlist_room_add(PurpleRoomlist *list,PurpleRoomlistRoom *room)
{
  do {
    if (list != ((PurpleRoomlist *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"list != NULL");
      return ;
    };
  }while (0);
  do {
    if (room != ((PurpleRoomlistRoom *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"room != NULL");
      return ;
    };
  }while (0);
  list -> rooms = g_list_append((list -> rooms),room);
  if ((ops != 0) && ((ops -> add_room) != 0)) 
    ( *(ops -> add_room))(list,room);
}

PurpleRoomlist *purple_roomlist_get_list(PurpleConnection *gc)
{
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((purple_connection_get_state(gc)) == PURPLE_CONNECTED) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_CONNECTION_IS_CONNECTED(gc)");
      return 0;
    };
  }while (0);
  prpl = purple_connection_get_prpl(gc);
  if (prpl != ((PurplePlugin *)((void *)0))) 
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if ((prpl_info != 0) && ((prpl_info -> roomlist_get_list) != 0)) 
    return ( *(prpl_info -> roomlist_get_list))(gc);
  return 0;
}

void purple_roomlist_cancel_get_list(PurpleRoomlist *list)
{
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurpleConnection *gc;
  do {
    if (list != ((PurpleRoomlist *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"list != NULL");
      return ;
    };
  }while (0);
  gc = purple_account_get_connection((list -> account));
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  if (gc != 0) 
    prpl = purple_connection_get_prpl(gc);
  if (prpl != 0) 
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if ((prpl_info != 0) && ((prpl_info -> roomlist_cancel) != 0)) 
    ( *(prpl_info -> roomlist_cancel))(list);
}

void purple_roomlist_expand_category(PurpleRoomlist *list,PurpleRoomlistRoom *category)
{
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurpleConnection *gc;
  do {
    if (list != ((PurpleRoomlist *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"list != NULL");
      return ;
    };
  }while (0);
  do {
    if (category != ((PurpleRoomlistRoom *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"category != NULL");
      return ;
    };
  }while (0);
  do {
    if (((category -> type) & PURPLE_ROOMLIST_ROOMTYPE_CATEGORY) != 0U) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"category->type & PURPLE_ROOMLIST_ROOMTYPE_CATEGORY");
      return ;
    };
  }while (0);
  gc = purple_account_get_connection((list -> account));
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  if (gc != 0) 
    prpl = purple_connection_get_prpl(gc);
  if (prpl != 0) 
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if ((prpl_info != 0) && ((prpl_info -> roomlist_expand_category) != 0)) 
    ( *(prpl_info -> roomlist_expand_category))(list,category);
}

GList *purple_roomlist_get_fields(PurpleRoomlist *list)
{
  return list -> fields;
}
/*@}*/
/**************************************************************************/
/** @name Room API                                                        */
/**************************************************************************/
/*@{*/

PurpleRoomlistRoom *purple_roomlist_room_new(PurpleRoomlistRoomType type,const gchar *name,PurpleRoomlistRoom *parent)
{
  PurpleRoomlistRoom *room;
  do {
    if (name != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return 0;
    };
  }while (0);
  room = ((PurpleRoomlistRoom *)(g_malloc0_n(1,(sizeof(PurpleRoomlistRoom )))));
  room -> type = type;
  room -> name = g_strdup(name);
  room -> parent = parent;
  return room;
}

void purple_roomlist_room_add_field(PurpleRoomlist *list,PurpleRoomlistRoom *room,gconstpointer field)
{
  PurpleRoomlistField *f;
  do {
    if (list != ((PurpleRoomlist *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"list != NULL");
      return ;
    };
  }while (0);
  do {
    if (room != ((PurpleRoomlistRoom *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"room != NULL");
      return ;
    };
  }while (0);
  do {
    if ((list -> fields) != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"list->fields != NULL");
      return ;
    };
  }while (0);
/* If this is the first call for this room, grab the first field in
	 * the Roomlist's fields.  Otherwise, grab the field that is one
	 * more than the number of fields already present for the room.
         * (This works because g_list_nth_data() is zero-indexed and
         * g_list_length() is one-indexed.) */
  if (!((room -> fields) != 0)) 
    f = ( *(list -> fields)).data;
  else 
    f = (g_list_nth_data((list -> fields),g_list_length((room -> fields))));
  do {
    if (f != ((PurpleRoomlistField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"f != NULL");
      return ;
    };
  }while (0);
  switch(f -> type){
    case PURPLE_ROOMLIST_FIELD_STRING:
{
      room -> fields = g_list_append((room -> fields),(g_strdup(field)));
      break; 
    }
    case PURPLE_ROOMLIST_FIELD_BOOL:
{
    }
    case PURPLE_ROOMLIST_FIELD_INT:
{
      room -> fields = g_list_append((room -> fields),((gpointer )((glong )field)));
      break; 
    }
  }
}

void purple_roomlist_room_join(PurpleRoomlist *list,PurpleRoomlistRoom *room)
{
  GHashTable *components;
  GList *l;
  GList *j;
  PurpleConnection *gc;
  do {
    if (list != ((PurpleRoomlist *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"list != NULL");
      return ;
    };
  }while (0);
  do {
    if (room != ((PurpleRoomlistRoom *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"room != NULL");
      return ;
    };
  }while (0);
  gc = purple_account_get_connection((list -> account));
  if (!(gc != 0)) 
    return ;
  components = g_hash_table_new(g_str_hash,g_str_equal);
  g_hash_table_replace(components,"name",(room -> name));
  for (((l = (list -> fields)) , (j = (room -> fields))); (l != 0) && (j != 0); ((l = (l -> next)) , (j = (j -> next)))) {
    PurpleRoomlistField *f = (l -> data);
    g_hash_table_replace(components,(f -> name),(j -> data));
  }
  serv_join_chat(gc,components);
  g_hash_table_destroy(components);
}

PurpleRoomlistRoomType purple_roomlist_room_get_type(PurpleRoomlistRoom *room)
{
  return room -> type;
}

const char *purple_roomlist_room_get_name(PurpleRoomlistRoom *room)
{
  return (room -> name);
}

PurpleRoomlistRoom *purple_roomlist_room_get_parent(PurpleRoomlistRoom *room)
{
  return room -> parent;
}

GList *purple_roomlist_room_get_fields(PurpleRoomlistRoom *room)
{
  return room -> fields;
}
/*@}*/
/**************************************************************************/
/** @name Room Field API                                                  */
/**************************************************************************/
/*@{*/

PurpleRoomlistField *purple_roomlist_field_new(PurpleRoomlistFieldType type,const gchar *label,const gchar *name,gboolean hidden)
{
  PurpleRoomlistField *f;
  do {
    if (label != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"label != NULL");
      return 0;
    };
  }while (0);
  do {
    if (name != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return 0;
    };
  }while (0);
  f = ((PurpleRoomlistField *)(g_malloc0_n(1,(sizeof(PurpleRoomlistField )))));
  f -> type = type;
  f -> label = g_strdup(label);
  f -> name = g_strdup(name);
  f -> hidden = hidden;
  return f;
}

PurpleRoomlistFieldType purple_roomlist_field_get_type(PurpleRoomlistField *field)
{
  return field -> type;
}

const char *purple_roomlist_field_get_label(PurpleRoomlistField *field)
{
  return (field -> label);
}

gboolean purple_roomlist_field_get_hidden(PurpleRoomlistField *field)
{
  return field -> hidden;
}
/*@}*/
/**************************************************************************/
/** @name UI Registration Functions                                       */
/**************************************************************************/
/*@{*/

void purple_roomlist_set_ui_ops(PurpleRoomlistUiOps *ui_ops)
{
  ops = ui_ops;
}

PurpleRoomlistUiOps *purple_roomlist_get_ui_ops()
{
  return ops;
}
/*@}*/
