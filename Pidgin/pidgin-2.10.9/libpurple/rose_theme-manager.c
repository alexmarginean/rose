/*
 * Themes for libpurple
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "theme-manager.h"
#include "util.h"
/******************************************************************************
 * Globals
 *****************************************************************************/
static GHashTable *theme_table = (GHashTable *)((void *)0);
/*****************************************************************************
 * GObject Stuff
 ****************************************************************************/

GType purple_theme_manager_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(PurpleThemeManagerClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )((void *)0)), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(PurpleThemeManager ))), (0), ((GInstanceInitFunc )((void *)0)), ((const GTypeValueTable *)((void *)0))
/* base_init */
/* base_finalize */
/* class_init */
/* class_finalize */
/* class_data */
/* n_preallocs */
/* instance_init */
/* Value Table */
};
    type = g_type_register_static(((GType )(20 << 2)),"PurpleThemeManager",&info,0);
  }
  return type;
}
/******************************************************************************
 * Helpers
 *****************************************************************************/
/* makes a key of <type> + '/' + <name> */

static gchar *purple_theme_manager_make_key(const gchar *name,const gchar *type)
{
  do {
    if ((name != 0) && (( *name) != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name && *name");
      return 0;
    };
  }while (0);
  do {
    if ((type != 0) && (( *type) != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type && *type");
      return 0;
    };
  }while (0);
  return g_strconcat(type,"/",name,((void *)((void *)0)));
}
/* returns TRUE if theme is of type "user_data" */

static gboolean purple_theme_manager_is_theme_type(gchar *key,gpointer value,gchar *user_data)
{
  return g_str_has_prefix(key,(g_strconcat(user_data,"/",((void *)((void *)0)))));
}

static gboolean purple_theme_manager_is_theme(gchar *key,gpointer value,gchar *user_data)
{
  return (
{
    GTypeInstance *__inst = (GTypeInstance *)value;
    GType __t = purple_theme_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  });
}

static void purple_theme_manager_function_wrapper(gchar *key,gpointer value,PTFunc user_data)
{
  if ((({
    GTypeInstance *__inst = (GTypeInstance *)value;
    GType __t = purple_theme_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
    ( *user_data)(value);
}

static void purple_theme_manager_build_dir(const gchar *root)
{
  gchar *purple_dir;
  gchar *theme_dir;
  const gchar *name = (const gchar *)((void *)0);
  const gchar *type = (const gchar *)((void *)0);
  GDir *rdir;
  GDir *tdir;
  PurpleThemeLoader *loader;
  rdir = g_dir_open(root,0,0);
  if (!(rdir != 0)) 
    return ;
/* Parses directory by root/name/purple/type */
  while((name = g_dir_read_name(rdir)) != 0){
    purple_dir = g_build_filename(root,name,"purple",((void *)((void *)0)));
    tdir = g_dir_open(purple_dir,0,0);
    if (!(tdir != 0)) {
      g_free(purple_dir);
      continue; 
    }
    while((type = g_dir_read_name(tdir)) != 0){
      if ((loader = (g_hash_table_lookup(theme_table,type))) != 0) {
        PurpleTheme *theme = (PurpleTheme *)((void *)0);
        theme_dir = g_build_filename(purple_dir,type,((void *)((void *)0)));
        theme = purple_theme_loader_build(loader,theme_dir);
        g_free(theme_dir);
        if ((({
          GTypeInstance *__inst = (GTypeInstance *)theme;
          GType __t = purple_theme_get_type();
          gboolean __r;
          if (!(__inst != 0)) 
            __r = 0;
          else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
            __r = (!0);
          else 
            __r = g_type_check_instance_is_a(__inst,__t);
          __r;
        })) != 0) 
          purple_theme_manager_add_theme(theme);
      }
    }
    g_dir_close(tdir);
    g_free(purple_dir);
  }
  g_dir_close(rdir);
}
/*****************************************************************************
 * Public API functions
 *****************************************************************************/

void purple_theme_manager_init()
{
  theme_table = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_object_unref);
}

void purple_theme_manager_refresh()
{
  gchar *path = (gchar *)((void *)0);
  const gchar *xdg = (const gchar *)((void *)0);
  gint i = 0;
  g_hash_table_foreach_remove(theme_table,((GHRFunc )purple_theme_manager_is_theme),0);
/* Add themes from ~/.purple */
  path = g_build_filename(purple_user_dir(),"themes",((void *)((void *)0)));
  purple_theme_manager_build_dir(path);
  g_free(path);
/* look for XDG_DATA_HOME.  If we don't have it use ~/.local, and add it */
  if ((xdg = g_getenv("XDG_DATA_HOME")) != ((const gchar *)((void *)0))) 
    path = g_build_filename(xdg,"themes",((void *)((void *)0)));
  else 
    path = g_build_filename(purple_home_dir(),".local","themes",((void *)((void *)0)));
  purple_theme_manager_build_dir(path);
  g_free(path);
/* now dig through XDG_DATA_DIRS and add those too */
  xdg = g_getenv("XDG_DATA_DIRS");
  if (xdg != 0) {
    gchar **xdg_dirs = g_strsplit(xdg,":",0);
    for (i = 0; xdg_dirs[i] != 0; i++) {
      path = g_build_filename(xdg_dirs[i],"themes",((void *)((void *)0)));
      purple_theme_manager_build_dir(path);
      g_free(path);
    }
    g_strfreev(xdg_dirs);
  }
}

void purple_theme_manager_uninit()
{
  g_hash_table_destroy(theme_table);
}

void purple_theme_manager_register_type(PurpleThemeLoader *loader)
{
  gchar *type;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)loader;
      GType __t = purple_theme_loader_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_THEME_LOADER(loader)");
      return ;
    };
  }while (0);
  type = g_strdup(purple_theme_loader_get_type_string(loader));
  do {
    if (type != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type");
      return ;
    };
  }while (0);
/* if something is already there do nothing */
  if (!(g_hash_table_lookup(theme_table,type) != 0)) 
    g_hash_table_insert(theme_table,type,loader);
}

void purple_theme_manager_unregister_type(PurpleThemeLoader *loader)
{
  const gchar *type;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)loader;
      GType __t = purple_theme_loader_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_THEME_LOADER(loader)");
      return ;
    };
  }while (0);
  type = purple_theme_loader_get_type_string(loader);
  do {
    if (type != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type");
      return ;
    };
  }while (0);
  if (g_hash_table_lookup(theme_table,type) == loader) {
    g_hash_table_remove(theme_table,type);
    g_hash_table_foreach_remove(theme_table,((GHRFunc )purple_theme_manager_is_theme_type),((gpointer )type));
/* only free if given registered loader */
  }
}

PurpleTheme *purple_theme_manager_find_theme(const gchar *name,const gchar *type)
{
  gchar *key;
  PurpleTheme *theme;
  key = purple_theme_manager_make_key(name,type);
  do {
    if (key != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"key");
      return 0;
    };
  }while (0);
  theme = (g_hash_table_lookup(theme_table,key));
  g_free(key);
  return theme;
}

void purple_theme_manager_add_theme(PurpleTheme *theme)
{
  gchar *key;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = purple_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_THEME(theme)");
      return ;
    };
  }while (0);
  key = purple_theme_manager_make_key(purple_theme_get_name(theme),purple_theme_get_type_string(theme));
  do {
    if (key != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"key");
      return ;
    };
  }while (0);
/* if something is already there do nothing */
  if (g_hash_table_lookup(theme_table,key) == ((void *)((void *)0))) 
    g_hash_table_insert(theme_table,key,theme);
}

void purple_theme_manager_remove_theme(PurpleTheme *theme)
{
  gchar *key;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = purple_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_THEME(theme)");
      return ;
    };
  }while (0);
  key = purple_theme_manager_make_key(purple_theme_get_name(theme),purple_theme_get_type_string(theme));
  do {
    if (key != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"key");
      return ;
    };
  }while (0);
  g_hash_table_remove(theme_table,key);
  g_free(key);
}

void purple_theme_manager_for_each_theme(PTFunc func)
{
  do {
    if (func != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"func");
      return ;
    };
  }while (0);
  g_hash_table_foreach(theme_table,((GHFunc )purple_theme_manager_function_wrapper),func);
}

PurpleTheme *purple_theme_manager_load_theme(const gchar *theme_dir,const gchar *type)
{
  PurpleThemeLoader *loader;
  do {
    if ((theme_dir != ((const gchar *)((void *)0))) && (type != ((const gchar *)((void *)0)))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"theme_dir != NULL && type != NULL");
      return 0;
    };
  }while (0);
  loader = (g_hash_table_lookup(theme_table,type));
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)loader;
      GType __t = purple_theme_loader_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_THEME_LOADER(loader)");
      return 0;
    };
  }while (0);
  return purple_theme_loader_build(loader,theme_dir);
}
