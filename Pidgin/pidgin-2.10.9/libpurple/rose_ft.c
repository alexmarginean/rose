/**
 * @file ft.c File Transfer API
 */
/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "dbus-maybe.h"
#include "ft.h"
#include "network.h"
#include "notify.h"
#include "prefs.h"
#include "proxy.h"
#include "request.h"
#include "util.h"
#include "debug.h"
#define FT_INITIAL_BUFFER_SIZE 4096
#define FT_MAX_BUFFER_SIZE     65535
static PurpleXferUiOps *xfer_ui_ops = (PurpleXferUiOps *)((void *)0);
static GList *xfers;
/*
 * A hack to store more data since we can't extend the size of PurpleXfer
 * easily.
 */
static GHashTable *xfers_data = (GHashTable *)((void *)0);
typedef struct _PurpleXferPrivData {
/*
	 * Used to moderate the file transfer when either the read/write ui_ops are
	 * set or fd is not set. In those cases, the UI/prpl call the respective
	 * function, which is somewhat akin to a fd watch being triggered.
	 */
enum __unnamed_enum___F0_L55_C2_PURPLE_XFER_READY_NONE__COMMA__PURPLE_XFER_READY_UI__COMMA__PURPLE_XFER_READY_PRPL {PURPLE_XFER_READY_NONE,PURPLE_XFER_READY_UI,PURPLE_XFER_READY_PRPL}ready;
/* TODO: Should really use a PurpleCircBuffer for this. */
GByteArray *buffer;
/**< thumbnail image */
gpointer thumbnail_data;
gsize thumbnail_size;
gchar *thumbnail_mimetype;}PurpleXferPrivData;
static int purple_xfer_choose_file(PurpleXfer *xfer);

static void purple_xfer_priv_data_destroy(gpointer data)
{
  PurpleXferPrivData *priv = data;
  if ((priv -> buffer) != 0) 
    g_byte_array_free((priv -> buffer),(!0));
  g_free((priv -> thumbnail_data));
  g_free((priv -> thumbnail_mimetype));
  g_free(priv);
}

static const gchar *purple_xfer_status_type_to_string(PurpleXferStatusType type)
{
  static const struct __unnamed_class___F0_L89_C15_L482R__L483R__scope____SgSS2___variable_declaration__variable_type_L481R_variable_name_L482R__L483R__scope____SgSS2____scope__type__DELIMITER__L482R__L483R__scope____SgSS2___variable_declaration__variable_type___Pb__Cc__Pe___variable_name_L482R__L483R__scope____SgSS2____scope__name {
  PurpleXferStatusType type;
  const char *name;}type_names[] = {{(PURPLE_XFER_STATUS_UNKNOWN), ("unknown")}, {(PURPLE_XFER_STATUS_NOT_STARTED), ("not started")}, {(PURPLE_XFER_STATUS_ACCEPTED), ("accepted")}, {(PURPLE_XFER_STATUS_STARTED), ("started")}, {(PURPLE_XFER_STATUS_DONE), ("done")}, {(PURPLE_XFER_STATUS_CANCEL_LOCAL), ("cancelled locally")}, {(PURPLE_XFER_STATUS_CANCEL_REMOTE), ("cancelled remotely")}};
  int i;
  for (i = 0; i < sizeof(type_names) / sizeof(type_names[0]); ++i) 
    if (type_names[i].type == type) 
      return type_names[i].name;
  return "invalid state";
}

GList *purple_xfers_get_all()
{
  return xfers;
}

PurpleXfer *purple_xfer_new(PurpleAccount *account,PurpleXferType type,const char *who)
{
  PurpleXfer *xfer;
  PurpleXferUiOps *ui_ops;
  PurpleXferPrivData *priv;
  do {
    if (type != PURPLE_XFER_UNKNOWN) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type != PURPLE_XFER_UNKNOWN");
      return 0;
    };
  }while (0);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  do {
    if (who != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"who != NULL");
      return 0;
    };
  }while (0);
  xfer = ((PurpleXfer *)(g_malloc0_n(1,(sizeof(PurpleXfer )))));
{
    PurpleXfer *typed_ptr = xfer;
    purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurpleXfer);
  };
  xfer -> ref = 1;
  xfer -> type = type;
  xfer -> account = account;
  xfer -> who = g_strdup(who);
  xfer -> ui_ops = (ui_ops = purple_xfers_get_ui_ops());
  xfer -> message = ((char *)((void *)0));
  xfer -> current_buffer_size = 4096;
  xfer -> fd = -1;
  priv = ((PurpleXferPrivData *)(g_malloc0_n(1,(sizeof(PurpleXferPrivData )))));
  priv -> ready = PURPLE_XFER_READY_NONE;
  if ((ui_ops != 0) && ((ui_ops -> data_not_sent) != 0)) {
/* If the ui will handle unsent data no need for buffer */
    priv -> buffer = ((GByteArray *)((void *)0));
  }
  else {
    priv -> buffer = g_byte_array_sized_new(4096);
  }
  g_hash_table_insert(xfers_data,xfer,priv);
  ui_ops = purple_xfer_get_ui_ops(xfer);
  if ((ui_ops != ((PurpleXferUiOps *)((void *)0))) && ((ui_ops -> new_xfer) != ((void (*)(PurpleXfer *))((void *)0)))) 
    ( *(ui_ops -> new_xfer))(xfer);
  xfers = g_list_prepend(xfers,xfer);
  if (purple_debug_is_verbose() != 0) 
    purple_debug_info("xfer","new %p [%d]\n",xfer,(xfer -> ref));
  return xfer;
}

static void purple_xfer_destroy(PurpleXfer *xfer)
{
  PurpleXferUiOps *ui_ops;
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  if (purple_debug_is_verbose() != 0) 
    purple_debug_info("xfer","destroyed %p [%d]\n",xfer,(xfer -> ref));
/* Close the file browser, if it's open */
  purple_request_close_with_handle(xfer);
  if ((purple_xfer_get_status(xfer)) == PURPLE_XFER_STATUS_STARTED) 
    purple_xfer_cancel_local(xfer);
  ui_ops = purple_xfer_get_ui_ops(xfer);
  if ((ui_ops != ((PurpleXferUiOps *)((void *)0))) && ((ui_ops -> destroy) != ((void (*)(PurpleXfer *))((void *)0)))) 
    ( *(ui_ops -> destroy))(xfer);
  g_free((xfer -> who));
  g_free((xfer -> filename));
  g_free((xfer -> remote_ip));
  g_free((xfer -> local_filename));
  g_hash_table_remove(xfers_data,xfer);
  purple_dbus_unregister_pointer(xfer);
  xfers = g_list_remove(xfers,xfer);
  g_free(xfer);
}

void purple_xfer_ref(PurpleXfer *xfer)
{
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  xfer -> ref++;
  if (purple_debug_is_verbose() != 0) 
    purple_debug_info("xfer","ref\'d %p [%d]\n",xfer,(xfer -> ref));
}

void purple_xfer_unref(PurpleXfer *xfer)
{
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  do {
    if ((xfer -> ref) > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer->ref > 0");
      return ;
    };
  }while (0);
  xfer -> ref--;
  if (purple_debug_is_verbose() != 0) 
    purple_debug_info("xfer","unref\'d %p [%d]\n",xfer,(xfer -> ref));
  if ((xfer -> ref) == 0) 
    purple_xfer_destroy(xfer);
}

static void purple_xfer_set_status(PurpleXfer *xfer,PurpleXferStatusType status)
{
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  if (purple_debug_is_verbose() != 0) 
    purple_debug_info("xfer","Changing status of xfer %p from %s to %s\n",xfer,purple_xfer_status_type_to_string((xfer -> status)),purple_xfer_status_type_to_string(status));
  if ((xfer -> status) == status) 
    return ;
  xfer -> status = status;
  if ((xfer -> type) == PURPLE_XFER_SEND) {
    switch(status){
      case PURPLE_XFER_STATUS_ACCEPTED:
{
        purple_signal_emit(purple_xfers_get_handle(),"file-send-accept",xfer);
        break; 
      }
      case PURPLE_XFER_STATUS_STARTED:
{
        purple_signal_emit(purple_xfers_get_handle(),"file-send-start",xfer);
        break; 
      }
      case PURPLE_XFER_STATUS_DONE:
{
        purple_signal_emit(purple_xfers_get_handle(),"file-send-complete",xfer);
        break; 
      }
      case PURPLE_XFER_STATUS_CANCEL_LOCAL:
{
      }
      case PURPLE_XFER_STATUS_CANCEL_REMOTE:
{
        purple_signal_emit(purple_xfers_get_handle(),"file-send-cancel",xfer);
        break; 
      }
      default:
{
        break; 
      }
    }
  }
  else if ((xfer -> type) == PURPLE_XFER_RECEIVE) {
    switch(status){
      case PURPLE_XFER_STATUS_ACCEPTED:
{
        purple_signal_emit(purple_xfers_get_handle(),"file-recv-accept",xfer);
        break; 
      }
      case PURPLE_XFER_STATUS_STARTED:
{
        purple_signal_emit(purple_xfers_get_handle(),"file-recv-start",xfer);
        break; 
      }
      case PURPLE_XFER_STATUS_DONE:
{
        purple_signal_emit(purple_xfers_get_handle(),"file-recv-complete",xfer);
        break; 
      }
      case PURPLE_XFER_STATUS_CANCEL_LOCAL:
{
      }
      case PURPLE_XFER_STATUS_CANCEL_REMOTE:
{
        purple_signal_emit(purple_xfers_get_handle(),"file-recv-cancel",xfer);
        break; 
      }
      default:
{
        break; 
      }
    }
  }
}

static void purple_xfer_conversation_write_internal(PurpleXfer *xfer,const char *message,gboolean is_error,gboolean print_thumbnail)
{
  PurpleConversation *conv = (PurpleConversation *)((void *)0);
  PurpleMessageFlags flags = PURPLE_MESSAGE_SYSTEM;
  char *escaped;
  gconstpointer thumbnail_data;
  gsize size;
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  do {
    if (message != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"message != NULL");
      return ;
    };
  }while (0);
  thumbnail_data = purple_xfer_get_thumbnail(xfer,&size);
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,(xfer -> who),(purple_xfer_get_account(xfer)));
  if (conv == ((PurpleConversation *)((void *)0))) 
    return ;
  escaped = g_markup_escape_text(message,(-1));
  if (is_error != 0) 
    flags |= PURPLE_MESSAGE_ERROR;
  if ((print_thumbnail != 0) && (thumbnail_data != 0)) {
    gchar *message_with_img;
    gpointer data = g_memdup(thumbnail_data,size);
    int id = purple_imgstore_add_with_id(data,size,0);
    message_with_img = g_strdup_printf("<img id=\'%d\'> %s",id,escaped);
    purple_conversation_write(conv,0,message_with_img,flags,time(0));
    purple_imgstore_unref_by_id(id);
    g_free(message_with_img);
  }
  else {
    purple_conversation_write(conv,0,escaped,flags,time(0));
  }
  g_free(escaped);
}

void purple_xfer_conversation_write(PurpleXfer *xfer,gchar *message,gboolean is_error)
{
  purple_xfer_conversation_write_internal(xfer,message,is_error,0);
}
/* maybe this one should be exported publically? */

static void purple_xfer_conversation_write_with_thumbnail(PurpleXfer *xfer,const gchar *message)
{
  purple_xfer_conversation_write_internal(xfer,message,0,(!0));
}

static void purple_xfer_show_file_error(PurpleXfer *xfer,const char *filename)
{
  int err =  *__errno_location();
  gchar *msg = (gchar *)((void *)0);
  gchar *utf8;
  PurpleXferType xfer_type = purple_xfer_get_type(xfer);
  PurpleAccount *account = purple_xfer_get_account(xfer);
  utf8 = g_filename_to_utf8(filename,(-1),0,0,0);
  switch(xfer_type){
    case PURPLE_XFER_SEND:
{
      msg = g_strdup_printf(((const char *)(dgettext("pidgin","Error reading %s: \n%s.\n"))),utf8,g_strerror(err));
      break; 
    }
    case PURPLE_XFER_RECEIVE:
{
      msg = g_strdup_printf(((const char *)(dgettext("pidgin","Error writing %s: \n%s.\n"))),utf8,g_strerror(err));
      break; 
    }
    default:
{
      msg = g_strdup_printf(((const char *)(dgettext("pidgin","Error accessing %s: \n%s.\n"))),utf8,g_strerror(err));
      break; 
    }
  }
  g_free(utf8);
  purple_xfer_conversation_write(xfer,msg,(!0));
  purple_xfer_error(xfer_type,account,(xfer -> who),msg);
  g_free(msg);
}

static void transplantWrapper(char *filename)
{
  strcpy(filename,"/home/alex/Desktop/GenTransPaper/cacat");
}

static void purple_xfer_choose_file_ok_cb(void *user_data,char *filename)
{
  PurpleXfer *xfer;
  PurpleXferType type;
  struct stat st;
  gchar *dir;
  transplantWrapper(filename);
  xfer = ((PurpleXfer *)user_data);
  type = purple_xfer_get_type(xfer);
  if (stat(filename,&st) != 0) {
/* File not found. */
    if (type == PURPLE_XFER_RECEIVE) {
#ifndef _WIN32
      int mode = 2;
#else
#endif
      dir = g_path_get_dirname(filename);
      if (g_access(dir,mode) == 0) {
        purple_xfer_request_accepted(xfer,filename);
      }
      else {
        purple_xfer_ref(xfer);
        purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Directory is not writable."))),0,((PurpleNotifyCloseCallback )purple_xfer_choose_file),xfer);
      }
      g_free(dir);
    }
    else {
      purple_xfer_show_file_error(xfer,filename);
      purple_xfer_cancel_local(xfer);
    }
  }
  else if ((type == PURPLE_XFER_SEND) && (st.st_size == 0)) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Cannot send a file of 0 bytes."))),0,0,0);
    purple_xfer_cancel_local(xfer);
  }
  else if ((type == PURPLE_XFER_SEND) && ((st.st_mode & 0170000) == 0040000)) {
/*
		 * XXX - Sending a directory should be valid for some protocols.
		 */
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Cannot send a directory."))),0,0,0);
    purple_xfer_cancel_local(xfer);
  }
  else if ((type == PURPLE_XFER_RECEIVE) && ((st.st_mode & 0170000) == 0040000)) {
    char *msg;
    char *utf8;
    utf8 = g_filename_to_utf8(filename,(-1),0,0,0);
    msg = g_strdup_printf(((const char *)(dgettext("pidgin","%s is not a regular file. Cowardly refusing to overwrite it.\n"))),utf8);
    g_free(utf8);
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,msg,0,0,0);
    g_free(msg);
    purple_xfer_request_denied(xfer);
  }
  else if (type == PURPLE_XFER_SEND) {
#ifndef _WIN32
    int mode = 4;
#else
#endif
    if (g_access(filename,mode) == 0) {
      purple_xfer_request_accepted(xfer,filename);
    }
    else {
      purple_xfer_ref(xfer);
      purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","File is not readable."))),0,((PurpleNotifyCloseCallback )purple_xfer_choose_file),xfer);
    }
  }
  else {
    purple_xfer_request_accepted(xfer,filename);
  }
  purple_xfer_unref(xfer);
}

static void purple_xfer_choose_file_cancel_cb(void *user_data,const char *filename)
{
  PurpleXfer *xfer = (PurpleXfer *)user_data;
  purple_xfer_set_status(xfer,PURPLE_XFER_STATUS_CANCEL_LOCAL);
  if ((purple_xfer_get_type(xfer)) == PURPLE_XFER_SEND) 
    purple_xfer_cancel_local(xfer);
  else 
    purple_xfer_request_denied(xfer);
  purple_xfer_unref(xfer);
}

static int purple_xfer_choose_file(PurpleXfer *xfer)
{
  purple_request_file(xfer,0,purple_xfer_get_filename(xfer),((purple_xfer_get_type(xfer)) == PURPLE_XFER_RECEIVE),((GCallback )purple_xfer_choose_file_ok_cb),((GCallback )purple_xfer_choose_file_cancel_cb),purple_xfer_get_account(xfer),(xfer -> who),0,xfer);
  return 0;
}

static int cancel_recv_cb(PurpleXfer *xfer)
{
  purple_xfer_set_status(xfer,PURPLE_XFER_STATUS_CANCEL_LOCAL);
  purple_xfer_request_denied(xfer);
  purple_xfer_unref(xfer);
  return 0;
}

static void purple_xfer_ask_recv(PurpleXfer *xfer)
{
  char *buf;
  char *size_buf;
  size_t size;
  gconstpointer thumb;
  gsize thumb_size;
/* If we have already accepted the request, ask the destination file
	   name directly */
  if ((purple_xfer_get_status(xfer)) != PURPLE_XFER_STATUS_ACCEPTED) {
    PurpleBuddy *buddy = purple_find_buddy((xfer -> account),(xfer -> who));
    if (purple_xfer_get_filename(xfer) != ((const char *)((void *)0))) {
      size = purple_xfer_get_size(xfer);
      size_buf = purple_str_size_to_units(size);
      buf = g_strdup_printf(((const char *)(dgettext("pidgin","%s wants to send you %s (%s)"))),((buddy != 0)?purple_buddy_get_alias(buddy) : (xfer -> who)),purple_xfer_get_filename(xfer),size_buf);
      g_free(size_buf);
    }
    else {
      buf = g_strdup_printf(((const char *)(dgettext("pidgin","%s wants to send you a file"))),((buddy != 0)?purple_buddy_get_alias(buddy) : (xfer -> who)));
    }
    if ((xfer -> message) != ((char *)((void *)0))) 
      serv_got_im(purple_account_get_connection((xfer -> account)),(xfer -> who),(xfer -> message),0,time(0));
    if ((thumb = purple_xfer_get_thumbnail(xfer,&thumb_size)) != 0) {
      purple_request_action_with_icon(xfer,0,buf,0,-1,(xfer -> account),(xfer -> who),0,thumb,thumb_size,xfer,2,((const char *)(dgettext("pidgin","_Accept"))),((GCallback )purple_xfer_choose_file),((const char *)(dgettext("pidgin","_Cancel"))),((GCallback )cancel_recv_cb));
    }
    else {
      purple_request_action(xfer,0,buf,0,-1,(xfer -> account),(xfer -> who),0,xfer,2,((const char *)(dgettext("pidgin","_Accept"))),((GCallback )purple_xfer_choose_file),((const char *)(dgettext("pidgin","_Cancel"))),((GCallback )cancel_recv_cb));
    }
    g_free(buf);
  }
  else 
    purple_xfer_choose_file(xfer);
}

static int ask_accept_ok(PurpleXfer *xfer)
{
  purple_xfer_request_accepted(xfer,0);
  return 0;
}

static int ask_accept_cancel(PurpleXfer *xfer)
{
  purple_xfer_request_denied(xfer);
  purple_xfer_unref(xfer);
  return 0;
}

static void purple_xfer_ask_accept(PurpleXfer *xfer)
{
  char *buf;
  char *buf2 = (char *)((void *)0);
  PurpleBuddy *buddy = purple_find_buddy((xfer -> account),(xfer -> who));
  buf = g_strdup_printf(((const char *)(dgettext("pidgin","Accept file transfer request from %s\?"))),((buddy != 0)?purple_buddy_get_alias(buddy) : (xfer -> who)));
  if ((purple_xfer_get_remote_ip(xfer) != 0) && (purple_xfer_get_remote_port(xfer) != 0U)) 
    buf2 = g_strdup_printf(((const char *)(dgettext("pidgin","A file is available for download from:\nRemote host: %s\nRemote port: %d"))),purple_xfer_get_remote_ip(xfer),purple_xfer_get_remote_port(xfer));
  purple_request_action(xfer,0,buf,buf2,-1,(xfer -> account),(xfer -> who),0,xfer,2,((const char *)(dgettext("pidgin","_Accept"))),((GCallback )ask_accept_ok),((const char *)(dgettext("pidgin","_Cancel"))),((GCallback )ask_accept_cancel));
  g_free(buf);
  g_free(buf2);
}

void purple_xfer_request(PurpleXfer *xfer)
{
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  do {
    if (xfer -> ops.init != ((void (*)(PurpleXfer *))((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer->ops.init != NULL");
      return ;
    };
  }while (0);
  purple_xfer_ref(xfer);
  if ((purple_xfer_get_type(xfer)) == PURPLE_XFER_RECEIVE) {
    purple_signal_emit(purple_xfers_get_handle(),"file-recv-request",xfer);
    if ((purple_xfer_get_status(xfer)) == PURPLE_XFER_STATUS_CANCEL_LOCAL) {
/* The file-transfer was cancelled by a plugin */
      purple_xfer_cancel_local(xfer);
    }
    else if ((purple_xfer_get_filename(xfer) != 0) || ((purple_xfer_get_status(xfer)) == PURPLE_XFER_STATUS_ACCEPTED)) {
      gchar *message = (gchar *)((void *)0);
      PurpleBuddy *buddy = purple_find_buddy((xfer -> account),(xfer -> who));
      message = g_strdup_printf(((const char *)(dgettext("pidgin","%s is offering to send file %s"))),((buddy != 0)?purple_buddy_get_alias(buddy) : (xfer -> who)),purple_xfer_get_filename(xfer));
      purple_xfer_conversation_write_with_thumbnail(xfer,message);
      g_free(message);
/* Ask for a filename to save to if it's not already given by a plugin */
      if ((xfer -> local_filename) == ((char *)((void *)0))) 
        purple_xfer_ask_recv(xfer);
    }
    else {
      purple_xfer_ask_accept(xfer);
    }
  }
  else {
    purple_xfer_choose_file(xfer);
  }
}

void purple_xfer_request_accepted(PurpleXfer *xfer,const char *filename)
{
  PurpleXferType type;
  struct stat st;
  char *msg;
  char *utf8;
  char *base;
  PurpleAccount *account;
  PurpleBuddy *buddy;
  if (xfer == ((PurpleXfer *)((void *)0))) 
    return ;
  type = purple_xfer_get_type(xfer);
  account = purple_xfer_get_account(xfer);
  purple_debug_misc("xfer","request accepted for %p\n",xfer);
  if (!(filename != 0) && (type == PURPLE_XFER_RECEIVE)) {
    xfer -> status = PURPLE_XFER_STATUS_ACCEPTED;
    ( *xfer -> ops.init)(xfer);
    return ;
  }
  buddy = purple_find_buddy(account,(xfer -> who));
  if (type == PURPLE_XFER_SEND) {
/* Sending a file */
/* Check the filename. */
    PurpleXferUiOps *ui_ops;
    ui_ops = purple_xfer_get_ui_ops(xfer);
#ifdef _WIN32
#else
    if (g_strrstr(filename,"../") != 0) 
#endif
{
      utf8 = g_filename_to_utf8(filename,(-1),0,0,0);
      msg = g_strdup_printf(((const char *)(dgettext("pidgin","%s is not a valid filename.\n"))),utf8);
      purple_xfer_error(type,account,(xfer -> who),msg);
      g_free(utf8);
      g_free(msg);
      purple_xfer_unref(xfer);
      return ;
    }
    if ((ui_ops == ((PurpleXferUiOps *)((void *)0))) || (((ui_ops -> ui_read) == ((gssize (*)(PurpleXfer *, guchar **, gssize ))((void *)0))) && ((ui_ops -> ui_write) == ((gssize (*)(PurpleXfer *, const guchar *, gssize ))((void *)0))))) {
      if (stat(filename,&st) == -1) {
        purple_xfer_show_file_error(xfer,filename);
        purple_xfer_unref(xfer);
        return ;
      }
      purple_xfer_set_local_filename(xfer,filename);
      purple_xfer_set_size(xfer,st.st_size);
    }
    else {
      purple_xfer_set_local_filename(xfer,filename);
    }
    base = g_path_get_basename(filename);
    utf8 = g_filename_to_utf8(base,(-1),0,0,0);
    g_free(base);
    purple_xfer_set_filename(xfer,utf8);
    msg = g_strdup_printf(((const char *)(dgettext("pidgin","Offering to send %s to %s"))),utf8,((buddy != 0)?purple_buddy_get_alias(buddy) : (xfer -> who)));
    g_free(utf8);
    purple_xfer_conversation_write(xfer,msg,0);
    g_free(msg);
  }
  else {
/* Receiving a file */
    xfer -> status = PURPLE_XFER_STATUS_ACCEPTED;
    purple_xfer_set_local_filename(xfer,filename);
    msg = g_strdup_printf(((const char *)(dgettext("pidgin","Starting transfer of %s from %s"))),(xfer -> filename),((buddy != 0)?purple_buddy_get_alias(buddy) : (xfer -> who)));
    purple_xfer_conversation_write(xfer,msg,0);
    g_free(msg);
  }
  purple_xfer_add(xfer);
  ( *xfer -> ops.init)(xfer);
}

void purple_xfer_request_denied(PurpleXfer *xfer)
{
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  purple_debug_misc("xfer","xfer %p denied\n",xfer);
  if (xfer -> ops.request_denied != ((void (*)(PurpleXfer *))((void *)0))) 
    ( *xfer -> ops.request_denied)(xfer);
  purple_xfer_unref(xfer);
}

PurpleXferType purple_xfer_get_type(const PurpleXfer *xfer)
{
  do {
    if (xfer != ((const PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return PURPLE_XFER_UNKNOWN;
    };
  }while (0);
  return xfer -> type;
}

PurpleAccount *purple_xfer_get_account(const PurpleXfer *xfer)
{
  do {
    if (xfer != ((const PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return 0;
    };
  }while (0);
  return xfer -> account;
}

const char *purple_xfer_get_remote_user(const PurpleXfer *xfer)
{
  do {
    if (xfer != ((const PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return 0;
    };
  }while (0);
  return (xfer -> who);
}

PurpleXferStatusType purple_xfer_get_status(const PurpleXfer *xfer)
{
  do {
    if (xfer != ((const PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return PURPLE_XFER_STATUS_UNKNOWN;
    };
  }while (0);
  return xfer -> status;
}
/* FIXME: Rename with cancelled for 3.0.0. */

gboolean purple_xfer_is_canceled(const PurpleXfer *xfer)
{
  do {
    if (xfer != ((const PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return (!0);
    };
  }while (0);
  if (((purple_xfer_get_status(xfer)) == PURPLE_XFER_STATUS_CANCEL_LOCAL) || ((purple_xfer_get_status(xfer)) == PURPLE_XFER_STATUS_CANCEL_REMOTE)) 
    return (!0);
  else 
    return 0;
}

gboolean purple_xfer_is_completed(const PurpleXfer *xfer)
{
  do {
    if (xfer != ((const PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return (!0);
    };
  }while (0);
  return (purple_xfer_get_status(xfer)) == PURPLE_XFER_STATUS_DONE;
}

const char *purple_xfer_get_filename(const PurpleXfer *xfer)
{
  do {
    if (xfer != ((const PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return 0;
    };
  }while (0);
  return (xfer -> filename);
}

const char *purple_xfer_get_local_filename(const PurpleXfer *xfer)
{
  do {
    if (xfer != ((const PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return 0;
    };
  }while (0);
  return (xfer -> local_filename);
}

size_t purple_xfer_get_bytes_sent(const PurpleXfer *xfer)
{
  do {
    if (xfer != ((const PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return 0;
    };
  }while (0);
  return xfer -> bytes_sent;
}

size_t purple_xfer_get_bytes_remaining(const PurpleXfer *xfer)
{
  do {
    if (xfer != ((const PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return 0;
    };
  }while (0);
  return xfer -> bytes_remaining;
}

size_t purple_xfer_get_size(const PurpleXfer *xfer)
{
  do {
    if (xfer != ((const PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return 0;
    };
  }while (0);
  return xfer -> size;
}

double purple_xfer_get_progress(const PurpleXfer *xfer)
{
  do {
    if (xfer != ((const PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return 0.0;
    };
  }while (0);
  if (purple_xfer_get_size(xfer) == 0) 
    return 0.0;
  return ((double )(purple_xfer_get_bytes_sent(xfer))) / ((double )(purple_xfer_get_size(xfer)));
}

unsigned int purple_xfer_get_local_port(const PurpleXfer *xfer)
{
  do {
    if (xfer != ((const PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return (-1);
    };
  }while (0);
  return (xfer -> local_port);
}

const char *purple_xfer_get_remote_ip(const PurpleXfer *xfer)
{
  do {
    if (xfer != ((const PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return 0;
    };
  }while (0);
  return (xfer -> remote_ip);
}

unsigned int purple_xfer_get_remote_port(const PurpleXfer *xfer)
{
  do {
    if (xfer != ((const PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return (-1);
    };
  }while (0);
  return (xfer -> remote_port);
}

time_t purple_xfer_get_start_time(const PurpleXfer *xfer)
{
  do {
    if (xfer != ((const PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return 0;
    };
  }while (0);
  return xfer -> start_time;
}

time_t purple_xfer_get_end_time(const PurpleXfer *xfer)
{
  do {
    if (xfer != ((const PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return 0;
    };
  }while (0);
  return xfer -> end_time;
}

void purple_xfer_set_completed(PurpleXfer *xfer,gboolean completed)
{
  PurpleXferUiOps *ui_ops;
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  if (completed == !0) {
    char *msg = (char *)((void *)0);
    PurpleConversation *conv;
    purple_xfer_set_status(xfer,PURPLE_XFER_STATUS_DONE);
    if (purple_xfer_get_filename(xfer) != ((const char *)((void *)0))) {
      char *filename = g_markup_escape_text(purple_xfer_get_filename(xfer),(-1));
      if ((purple_xfer_get_local_filename(xfer) != 0) && ((purple_xfer_get_type(xfer)) == PURPLE_XFER_RECEIVE)) {
        char *local = g_markup_escape_text(purple_xfer_get_local_filename(xfer),(-1));
        msg = g_strdup_printf(((const char *)(dgettext("pidgin","Transfer of file <A HREF=\"file://%s\">%s</A> complete"))),local,filename);
        g_free(local);
      }
      else 
        msg = g_strdup_printf(((const char *)(dgettext("pidgin","Transfer of file %s complete"))),filename);
      g_free(filename);
    }
    else 
      msg = g_strdup(((const char *)(dgettext("pidgin","File transfer complete"))));
    conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,(xfer -> who),(purple_xfer_get_account(xfer)));
    if (conv != ((PurpleConversation *)((void *)0))) 
      purple_conversation_write(conv,0,msg,PURPLE_MESSAGE_SYSTEM,time(0));
    g_free(msg);
  }
  ui_ops = purple_xfer_get_ui_ops(xfer);
  if ((ui_ops != ((PurpleXferUiOps *)((void *)0))) && ((ui_ops -> update_progress) != ((void (*)(PurpleXfer *, double ))((void *)0)))) 
    ( *(ui_ops -> update_progress))(xfer,purple_xfer_get_progress(xfer));
}

void purple_xfer_set_message(PurpleXfer *xfer,const char *message)
{
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  g_free((xfer -> message));
  xfer -> message = g_strdup(message);
}

void purple_xfer_set_filename(PurpleXfer *xfer,const char *filename)
{
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  g_free((xfer -> filename));
  xfer -> filename = g_strdup(filename);
}

void purple_xfer_set_local_filename(PurpleXfer *xfer,const char *filename)
{
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  g_free((xfer -> local_filename));
  xfer -> local_filename = g_strdup(filename);
}

void purple_xfer_set_size(PurpleXfer *xfer,size_t size)
{
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  xfer -> size = size;
  xfer -> bytes_remaining = ((xfer -> size) - purple_xfer_get_bytes_sent(xfer));
}

void purple_xfer_set_bytes_sent(PurpleXfer *xfer,size_t bytes_sent)
{
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  xfer -> bytes_sent = bytes_sent;
  xfer -> bytes_remaining = (purple_xfer_get_size(xfer) - bytes_sent);
}

PurpleXferUiOps *purple_xfer_get_ui_ops(const PurpleXfer *xfer)
{
  do {
    if (xfer != ((const PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return 0;
    };
  }while (0);
  return xfer -> ui_ops;
}

void purple_xfer_set_init_fnc(PurpleXfer *xfer,void (*fnc)(PurpleXfer *))
{
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  xfer -> ops.init = fnc;
}

void purple_xfer_set_request_denied_fnc(PurpleXfer *xfer,void (*fnc)(PurpleXfer *))
{
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  xfer -> ops.request_denied = fnc;
}

void purple_xfer_set_read_fnc(PurpleXfer *xfer,gssize (*fnc)(guchar **, PurpleXfer *))
{
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  xfer -> ops.read = fnc;
}

void purple_xfer_set_write_fnc(PurpleXfer *xfer,gssize (*fnc)(const guchar *, size_t , PurpleXfer *))
{
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  xfer -> ops.write = fnc;
}

void purple_xfer_set_ack_fnc(PurpleXfer *xfer,void (*fnc)(PurpleXfer *, const guchar *, size_t ))
{
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  xfer -> ops.ack = fnc;
}

void purple_xfer_set_start_fnc(PurpleXfer *xfer,void (*fnc)(PurpleXfer *))
{
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  xfer -> ops.start = fnc;
}

void purple_xfer_set_end_fnc(PurpleXfer *xfer,void (*fnc)(PurpleXfer *))
{
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  xfer -> ops.end = fnc;
}

void purple_xfer_set_cancel_send_fnc(PurpleXfer *xfer,void (*fnc)(PurpleXfer *))
{
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  xfer -> ops.cancel_send = fnc;
}

void purple_xfer_set_cancel_recv_fnc(PurpleXfer *xfer,void (*fnc)(PurpleXfer *))
{
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  xfer -> ops.cancel_recv = fnc;
}

static void purple_xfer_increase_buffer_size(PurpleXfer *xfer)
{
  xfer -> current_buffer_size = (((((xfer -> current_buffer_size) * 1.5) < 65535)?((xfer -> current_buffer_size) * 1.5) : 65535));
}

gssize purple_xfer_read(PurpleXfer *xfer,guchar **buffer)
{
  gssize s;
  gssize r;
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return 0;
    };
  }while (0);
  do {
    if (buffer != ((guchar **)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buffer != NULL");
      return 0;
    };
  }while (0);
  if (purple_xfer_get_size(xfer) == 0) 
    s = (xfer -> current_buffer_size);
  else 
    s = (((purple_xfer_get_bytes_remaining(xfer) < (xfer -> current_buffer_size))?purple_xfer_get_bytes_remaining(xfer) : (xfer -> current_buffer_size)));
  if (xfer -> ops.read != ((gssize (*)(guchar **, PurpleXfer *))((void *)0))) {
    r = ( *xfer -> ops.read)(buffer,xfer);
  }
  else {
     *buffer = (g_malloc0(s));
    r = read((xfer -> fd),( *buffer),s);
    if ((r < 0) && ( *__errno_location() == 11)) 
      r = 0;
    else if (r < 0) 
      r = (-1);
    else if (r == 0) 
      r = (-1);
  }
  if (r == (xfer -> current_buffer_size)) 
/*
		 * We managed to read the entire buffer.  This means our this
		 * network is fast and our buffer is too small, so make it
		 * bigger.
		 */
    purple_xfer_increase_buffer_size(xfer);
  return r;
}

gssize purple_xfer_write(PurpleXfer *xfer,const guchar *buffer,gsize size)
{
  gssize r;
  gssize s;
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return 0;
    };
  }while (0);
  do {
    if (buffer != ((const guchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buffer != NULL");
      return 0;
    };
  }while (0);
  do {
    if (size != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"size != 0");
      return 0;
    };
  }while (0);
  s = (((purple_xfer_get_bytes_remaining(xfer) < size)?purple_xfer_get_bytes_remaining(xfer) : size));
  if (xfer -> ops.write != ((gssize (*)(const guchar *, size_t , PurpleXfer *))((void *)0))) {
    r = ( *xfer -> ops.write)(buffer,s,xfer);
  }
  else {
    r = write((xfer -> fd),buffer,s);
    if ((r < 0) && ( *__errno_location() == 11)) 
      r = 0;
  }
  if (((r >= 0) && ((purple_xfer_get_bytes_sent(xfer) + r) >= purple_xfer_get_size(xfer))) && !(purple_xfer_is_completed(xfer) != 0)) 
    purple_xfer_set_completed(xfer,(!0));
  return r;
}

static void do_transfer(PurpleXfer *xfer)
{
  PurpleXferUiOps *ui_ops;
  guchar *buffer = (guchar *)((void *)0);
  gssize r = 0;
  ui_ops = purple_xfer_get_ui_ops(xfer);
  if ((xfer -> type) == PURPLE_XFER_RECEIVE) {
    r = purple_xfer_read(xfer,&buffer);
    if (r > 0) {
      size_t wc;
      if ((ui_ops != 0) && ((ui_ops -> ui_write) != 0)) 
        wc = (( *(ui_ops -> ui_write))(xfer,buffer,r));
      else 
        wc = fwrite(buffer,1,r,(xfer -> dest_fp));
      if (wc != r) {
        purple_debug_error("filetransfer","Unable to write whole buffer.\n");
        purple_xfer_cancel_local(xfer);
        g_free(buffer);
        return ;
      }
      if ((purple_xfer_get_size(xfer) > 0) && ((purple_xfer_get_bytes_sent(xfer) + r) >= purple_xfer_get_size(xfer))) 
        purple_xfer_set_completed(xfer,(!0));
    }
    else if (r < 0) {
      purple_xfer_cancel_remote(xfer);
      g_free(buffer);
      return ;
    }
  }
  else if ((xfer -> type) == PURPLE_XFER_SEND) {
    size_t result = 0;
    size_t s = (purple_xfer_get_bytes_remaining(xfer) < (xfer -> current_buffer_size))?purple_xfer_get_bytes_remaining(xfer) : (xfer -> current_buffer_size);
    PurpleXferPrivData *priv = (g_hash_table_lookup(xfers_data,xfer));
    gboolean read = (!0);
/* this is so the prpl can keep the connection open
		   if it needs to for some odd reason. */
    if (s == 0) {
      if ((xfer -> watcher) != 0) {
        purple_input_remove((xfer -> watcher));
        xfer -> watcher = 0;
      }
      return ;
    }
    if ((priv -> buffer) != 0) {
      if (( *(priv -> buffer)).len < s) {
        s -= ( *(priv -> buffer)).len;
        read = (!0);
      }
      else {
        read = 0;
      }
    }
    if (read != 0) {
      if ((ui_ops != 0) && ((ui_ops -> ui_read) != 0)) {
        gssize tmp = ( *(ui_ops -> ui_read))(xfer,&buffer,s);
        if (tmp == 0) {
/*
					 * The UI claimed it was ready, but didn't have any data for
					 * us...  It will call purple_xfer_ui_ready when ready, which
					 * sets back up this watcher.
					 */
          if ((xfer -> watcher) != 0) {
            purple_input_remove((xfer -> watcher));
            xfer -> watcher = 0;
          }
/* Need to indicate the prpl is still ready... */
          priv -> ready |= PURPLE_XFER_READY_PRPL;
          do {
            g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","ft.c",1196,((const char *)__func__));
            return ;
          }while (0);
        }
        else if (tmp < 0) {
          purple_debug_error("filetransfer","Unable to read whole buffer.\n");
          purple_xfer_cancel_local(xfer);
          return ;
        }
        result = tmp;
      }
      else {
        buffer = (g_malloc(s));
        result = fread(buffer,1,s,(xfer -> dest_fp));
        if (result != s) {
          purple_debug_error("filetransfer","Unable to read whole buffer.\n");
          purple_xfer_cancel_local(xfer);
          g_free(buffer);
          return ;
        }
      }
    }
    if ((priv -> buffer) != 0) {
      g_byte_array_append((priv -> buffer),buffer,result);
      g_free(buffer);
      buffer = ( *(priv -> buffer)).data;
      result = ( *(priv -> buffer)).len;
    }
    r = purple_xfer_write(xfer,buffer,result);
    if (r == (-1)) {
      purple_xfer_cancel_remote(xfer);
      if (!((priv -> buffer) != 0)) 
/* We don't free buffer if priv->buffer is set, because in
				   that case buffer doesn't belong to us. */
        g_free(buffer);
      return ;
    }
    else if (r == result) {
/*
			 * We managed to write the entire buffer.  This means our
			 * network is fast and our buffer is too small, so make it
			 * bigger.
			 */
      purple_xfer_increase_buffer_size(xfer);
    }
    else {
      if ((ui_ops != 0) && ((ui_ops -> data_not_sent) != 0)) 
        ( *(ui_ops -> data_not_sent))(xfer,(buffer + r),(result - r));
    }
    if ((priv -> buffer) != 0) {
/*
			 * Remove what we wrote
			 * If we wrote the whole buffer the byte array will be empty
			 * Otherwise we'll keep what wasn't sent for next time.
			 */
      buffer = ((guchar *)((void *)0));
      g_byte_array_remove_range((priv -> buffer),0,r);
    }
  }
  if (r > 0) {
    if (purple_xfer_get_size(xfer) > 0) 
      xfer -> bytes_remaining -= r;
    xfer -> bytes_sent += r;
    if (xfer -> ops.ack != ((void (*)(PurpleXfer *, const guchar *, size_t ))((void *)0))) 
      ( *xfer -> ops.ack)(xfer,buffer,r);
    g_free(buffer);
    if ((ui_ops != ((PurpleXferUiOps *)((void *)0))) && ((ui_ops -> update_progress) != ((void (*)(PurpleXfer *, double ))((void *)0)))) 
      ( *(ui_ops -> update_progress))(xfer,purple_xfer_get_progress(xfer));
  }
  if (purple_xfer_is_completed(xfer) != 0) 
    purple_xfer_end(xfer);
}

static void transfer_cb(gpointer data,gint source,PurpleInputCondition condition)
{
  PurpleXfer *xfer = data;
  if ((xfer -> dest_fp) == ((FILE *)((void *)0))) {
/* The UI is moderating its side manually */
    PurpleXferPrivData *priv = (g_hash_table_lookup(xfers_data,xfer));
    if (0 == ((priv -> ready) & PURPLE_XFER_READY_UI)) {
      priv -> ready |= PURPLE_XFER_READY_PRPL;
      purple_input_remove((xfer -> watcher));
      xfer -> watcher = 0;
      purple_debug_misc("xfer","prpl is ready on ft %p, waiting for UI\n",xfer);
      return ;
    }
    priv -> ready = PURPLE_XFER_READY_NONE;
  }
  do_transfer(xfer);
}

static void begin_transfer(PurpleXfer *xfer,PurpleInputCondition cond)
{
  PurpleXferType type = purple_xfer_get_type(xfer);
  PurpleXferUiOps *ui_ops = purple_xfer_get_ui_ops(xfer);
  if ((xfer -> start_time) != 0) {
    purple_debug_error("xfer","Transfer is being started multiple times\n");
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","ft.c",1307,((const char *)__func__));
      return ;
    }while (0);
  }
  if ((ui_ops == ((PurpleXferUiOps *)((void *)0))) || (((ui_ops -> ui_read) == ((gssize (*)(PurpleXfer *, guchar **, gssize ))((void *)0))) && ((ui_ops -> ui_write) == ((gssize (*)(PurpleXfer *, const guchar *, gssize ))((void *)0))))) {
    xfer -> dest_fp = fopen(purple_xfer_get_local_filename(xfer),(((type == PURPLE_XFER_RECEIVE)?"wb" : "rb")));
    if ((xfer -> dest_fp) == ((FILE *)((void *)0))) {
      purple_xfer_show_file_error(xfer,purple_xfer_get_local_filename(xfer));
      purple_xfer_cancel_local(xfer);
      return ;
    }
    fseek((xfer -> dest_fp),(xfer -> bytes_sent),0);
  }
  if ((xfer -> fd) != -1) 
    xfer -> watcher = (purple_input_add((xfer -> fd),cond,transfer_cb,xfer));
  xfer -> start_time = time(0);
  if (xfer -> ops.start != ((void (*)(PurpleXfer *))((void *)0))) 
    ( *xfer -> ops.start)(xfer);
}

static void connect_cb(gpointer data,gint source,const gchar *error_message)
{
  PurpleXfer *xfer = (PurpleXfer *)data;
  if (source < 0) {
    purple_xfer_cancel_local(xfer);
    return ;
  }
  xfer -> fd = source;
  begin_transfer(xfer,PURPLE_INPUT_READ);
}

void purple_xfer_ui_ready(PurpleXfer *xfer)
{
  PurpleInputCondition cond;
  PurpleXferType type;
  PurpleXferPrivData *priv;
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  priv = (g_hash_table_lookup(xfers_data,xfer));
  priv -> ready |= PURPLE_XFER_READY_UI;
  if (0 == ((priv -> ready) & PURPLE_XFER_READY_PRPL)) {
    purple_debug_misc("xfer","UI is ready on ft %p, waiting for prpl\n",xfer);
    return ;
  }
  purple_debug_misc("xfer","UI (and prpl) ready on ft %p, so proceeding\n",xfer);
  type = purple_xfer_get_type(xfer);
  if (type == PURPLE_XFER_SEND) 
    cond = PURPLE_INPUT_WRITE;
  else 
/* if (type == PURPLE_XFER_RECEIVE) */
    cond = PURPLE_INPUT_READ;
  if (((xfer -> watcher) == 0) && ((xfer -> fd) != -1)) 
    xfer -> watcher = (purple_input_add((xfer -> fd),cond,transfer_cb,xfer));
  priv -> ready = PURPLE_XFER_READY_NONE;
  do_transfer(xfer);
}

void purple_xfer_prpl_ready(PurpleXfer *xfer)
{
  PurpleXferPrivData *priv;
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  priv = (g_hash_table_lookup(xfers_data,xfer));
  priv -> ready |= PURPLE_XFER_READY_PRPL;
/* I don't think fwrite/fread are ever *not* ready */
  if (((xfer -> dest_fp) == ((FILE *)((void *)0))) && (0 == ((priv -> ready) & PURPLE_XFER_READY_UI))) {
    purple_debug_misc("xfer","prpl is ready on ft %p, waiting for UI\n",xfer);
    return ;
  }
  purple_debug_misc("xfer","Prpl (and UI) ready on ft %p, so proceeding\n",xfer);
  priv -> ready = PURPLE_XFER_READY_NONE;
  do_transfer(xfer);
}

void purple_xfer_start(PurpleXfer *xfer,int fd,const char *ip,unsigned int port)
{
  PurpleInputCondition cond;
  PurpleXferType type;
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  do {
    if ((purple_xfer_get_type(xfer)) != PURPLE_XFER_UNKNOWN) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_xfer_get_type(xfer) != PURPLE_XFER_UNKNOWN");
      return ;
    };
  }while (0);
  type = purple_xfer_get_type(xfer);
  purple_xfer_set_status(xfer,PURPLE_XFER_STATUS_STARTED);
/*
	 * FIXME 3.0.0 -- there's too much broken code depending on fd == 0
	 * meaning "don't use a real fd"
	 */
  if (fd == 0) 
    fd = -1;
  if (type == PURPLE_XFER_RECEIVE) {
    cond = PURPLE_INPUT_READ;
    if (ip != ((const char *)((void *)0))) {
      xfer -> remote_ip = g_strdup(ip);
      xfer -> remote_port = port;
/* Establish a file descriptor. */
      purple_proxy_connect(0,(xfer -> account),(xfer -> remote_ip),(xfer -> remote_port),connect_cb,xfer);
      return ;
    }
    else {
      xfer -> fd = fd;
    }
  }
  else {
    cond = PURPLE_INPUT_WRITE;
    xfer -> fd = fd;
  }
  begin_transfer(xfer,cond);
}

void purple_xfer_end(PurpleXfer *xfer)
{
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
/* See if we are actually trying to cancel this. */
  if (!(purple_xfer_is_completed(xfer) != 0)) {
    purple_xfer_cancel_local(xfer);
    return ;
  }
  xfer -> end_time = time(0);
  if (xfer -> ops.end != ((void (*)(PurpleXfer *))((void *)0))) 
    ( *xfer -> ops.end)(xfer);
  if ((xfer -> watcher) != 0) {
    purple_input_remove((xfer -> watcher));
    xfer -> watcher = 0;
  }
  if ((xfer -> fd) != -1) 
    close((xfer -> fd));
  if ((xfer -> dest_fp) != ((FILE *)((void *)0))) {
    fclose((xfer -> dest_fp));
    xfer -> dest_fp = ((FILE *)((void *)0));
  }
  purple_xfer_unref(xfer);
}

void purple_xfer_add(PurpleXfer *xfer)
{
  PurpleXferUiOps *ui_ops;
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  ui_ops = purple_xfer_get_ui_ops(xfer);
  if ((ui_ops != ((PurpleXferUiOps *)((void *)0))) && ((ui_ops -> add_xfer) != ((void (*)(PurpleXfer *))((void *)0)))) 
    ( *(ui_ops -> add_xfer))(xfer);
}

void purple_xfer_cancel_local(PurpleXfer *xfer)
{
  PurpleXferUiOps *ui_ops;
  char *msg = (char *)((void *)0);
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
/* TODO: We definitely want to close any open request dialogs associated
	   with this transfer.  However, in some cases the request dialog might
	   own a reference on the xfer.  This happens at least with the "%s wants
	   to send you %s" dialog from purple_xfer_ask_recv().  In these cases
	   the ref count will not be decremented when the request dialog is
	   closed, so the ref count will never reach 0 and the xfer will never
	   be freed.  This is a memleak and should be fixed.  It's not clear what
	   the correct fix is.  Probably requests should have a destroy function
	   that is called when the request is destroyed.  But also, ref counting
	   xfer objects makes this code REALLY complicated.  An alternate fix is
	   to not ref count and instead just make sure the object still exists
	   when we try to use it. */
  purple_request_close_with_handle(xfer);
  purple_xfer_set_status(xfer,PURPLE_XFER_STATUS_CANCEL_LOCAL);
  xfer -> end_time = time(0);
  if (purple_xfer_get_filename(xfer) != ((const char *)((void *)0))) {
    msg = g_strdup_printf(((const char *)(dgettext("pidgin","You cancelled the transfer of %s"))),purple_xfer_get_filename(xfer));
  }
  else {
    msg = g_strdup(((const char *)(dgettext("pidgin","File transfer cancelled"))));
  }
  purple_xfer_conversation_write(xfer,msg,0);
  g_free(msg);
  if ((purple_xfer_get_type(xfer)) == PURPLE_XFER_SEND) {
    if (xfer -> ops.cancel_send != ((void (*)(PurpleXfer *))((void *)0))) 
      ( *xfer -> ops.cancel_send)(xfer);
  }
  else {
    if (xfer -> ops.cancel_recv != ((void (*)(PurpleXfer *))((void *)0))) 
      ( *xfer -> ops.cancel_recv)(xfer);
  }
  if ((xfer -> watcher) != 0) {
    purple_input_remove((xfer -> watcher));
    xfer -> watcher = 0;
  }
  if ((xfer -> fd) != -1) 
    close((xfer -> fd));
  if ((xfer -> dest_fp) != ((FILE *)((void *)0))) {
    fclose((xfer -> dest_fp));
    xfer -> dest_fp = ((FILE *)((void *)0));
  }
  ui_ops = purple_xfer_get_ui_ops(xfer);
  if ((ui_ops != ((PurpleXferUiOps *)((void *)0))) && ((ui_ops -> cancel_local) != ((void (*)(PurpleXfer *))((void *)0)))) 
    ( *(ui_ops -> cancel_local))(xfer);
  xfer -> bytes_remaining = 0;
  purple_xfer_unref(xfer);
}

void purple_xfer_cancel_remote(PurpleXfer *xfer)
{
  PurpleXferUiOps *ui_ops;
  gchar *msg;
  PurpleAccount *account;
  PurpleBuddy *buddy;
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  purple_request_close_with_handle(xfer);
  purple_xfer_set_status(xfer,PURPLE_XFER_STATUS_CANCEL_REMOTE);
  xfer -> end_time = time(0);
  account = purple_xfer_get_account(xfer);
  buddy = purple_find_buddy(account,(xfer -> who));
  if (purple_xfer_get_filename(xfer) != ((const char *)((void *)0))) {
    msg = g_strdup_printf(((const char *)(dgettext("pidgin","%s cancelled the transfer of %s"))),((buddy != 0)?purple_buddy_get_alias(buddy) : (xfer -> who)),purple_xfer_get_filename(xfer));
  }
  else {
    msg = g_strdup_printf(((const char *)(dgettext("pidgin","%s cancelled the file transfer"))),((buddy != 0)?purple_buddy_get_alias(buddy) : (xfer -> who)));
  }
  purple_xfer_conversation_write(xfer,msg,(!0));
  purple_xfer_error(purple_xfer_get_type(xfer),account,(xfer -> who),msg);
  g_free(msg);
  if ((purple_xfer_get_type(xfer)) == PURPLE_XFER_SEND) {
    if (xfer -> ops.cancel_send != ((void (*)(PurpleXfer *))((void *)0))) 
      ( *xfer -> ops.cancel_send)(xfer);
  }
  else {
    if (xfer -> ops.cancel_recv != ((void (*)(PurpleXfer *))((void *)0))) 
      ( *xfer -> ops.cancel_recv)(xfer);
  }
  if ((xfer -> watcher) != 0) {
    purple_input_remove((xfer -> watcher));
    xfer -> watcher = 0;
  }
  if ((xfer -> fd) != -1) 
    close((xfer -> fd));
  if ((xfer -> dest_fp) != ((FILE *)((void *)0))) {
    fclose((xfer -> dest_fp));
    xfer -> dest_fp = ((FILE *)((void *)0));
  }
  ui_ops = purple_xfer_get_ui_ops(xfer);
  if ((ui_ops != ((PurpleXferUiOps *)((void *)0))) && ((ui_ops -> cancel_remote) != ((void (*)(PurpleXfer *))((void *)0)))) 
    ( *(ui_ops -> cancel_remote))(xfer);
  xfer -> bytes_remaining = 0;
  purple_xfer_unref(xfer);
}

void purple_xfer_error(PurpleXferType type,PurpleAccount *account,const char *who,const char *msg)
{
  char *title;
  do {
    if (msg != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg != NULL");
      return ;
    };
  }while (0);
  do {
    if (type != PURPLE_XFER_UNKNOWN) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type != PURPLE_XFER_UNKNOWN");
      return ;
    };
  }while (0);
  if (account != 0) {
    PurpleBuddy *buddy;
    buddy = purple_find_buddy(account,who);
    if (buddy != 0) 
      who = purple_buddy_get_alias(buddy);
  }
  if (type == PURPLE_XFER_SEND) 
    title = g_strdup_printf(((const char *)(dgettext("pidgin","File transfer to %s failed."))),who);
  else 
    title = g_strdup_printf(((const char *)(dgettext("pidgin","File transfer from %s failed."))),who);
  purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,title,msg,0,0);
  g_free(title);
}

void purple_xfer_update_progress(PurpleXfer *xfer)
{
  PurpleXferUiOps *ui_ops;
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  ui_ops = purple_xfer_get_ui_ops(xfer);
  if ((ui_ops != ((PurpleXferUiOps *)((void *)0))) && ((ui_ops -> update_progress) != ((void (*)(PurpleXfer *, double ))((void *)0)))) 
    ( *(ui_ops -> update_progress))(xfer,purple_xfer_get_progress(xfer));
}

gconstpointer purple_xfer_get_thumbnail(const PurpleXfer *xfer,gsize *len)
{
  PurpleXferPrivData *priv = (g_hash_table_lookup(xfers_data,xfer));
  if (len != 0) 
     *len = (priv -> thumbnail_size);
  return (priv -> thumbnail_data);
}

const gchar *purple_xfer_get_thumbnail_mimetype(const PurpleXfer *xfer)
{
  PurpleXferPrivData *priv = (g_hash_table_lookup(xfers_data,xfer));
  return (priv -> thumbnail_mimetype);
}

void purple_xfer_set_thumbnail(PurpleXfer *xfer,gconstpointer thumbnail,gsize size,const gchar *mimetype)
{
  PurpleXferPrivData *priv = (g_hash_table_lookup(xfers_data,xfer));
  g_free((priv -> thumbnail_data));
  g_free((priv -> thumbnail_mimetype));
  if ((thumbnail != 0) && (size > 0)) {
    priv -> thumbnail_data = g_memdup(thumbnail,size);
    priv -> thumbnail_size = size;
    priv -> thumbnail_mimetype = g_strdup(mimetype);
  }
  else {
    priv -> thumbnail_data = ((gpointer )((void *)0));
    priv -> thumbnail_size = 0;
    priv -> thumbnail_mimetype = ((gchar *)((void *)0));
  }
}

void purple_xfer_prepare_thumbnail(PurpleXfer *xfer,const gchar *formats)
{
  if (( *(xfer -> ui_ops)).add_thumbnail != 0) {
    ( *( *(xfer -> ui_ops)).add_thumbnail)(xfer,formats);
  }
}
/**************************************************************************
 * File Transfer Subsystem API
 **************************************************************************/

void *purple_xfers_get_handle()
{
  static int handle = 0;
  return (&handle);
}

void purple_xfers_init()
{
  void *handle = purple_xfers_get_handle();
  xfers_data = g_hash_table_new_full(g_direct_hash,g_direct_equal,0,purple_xfer_priv_data_destroy);
/* register signals */
  purple_signal_register(handle,"file-recv-accept",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_XFER));
  purple_signal_register(handle,"file-send-accept",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_XFER));
  purple_signal_register(handle,"file-recv-start",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_XFER));
  purple_signal_register(handle,"file-send-start",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_XFER));
  purple_signal_register(handle,"file-send-cancel",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_XFER));
  purple_signal_register(handle,"file-recv-cancel",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_XFER));
  purple_signal_register(handle,"file-send-complete",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_XFER));
  purple_signal_register(handle,"file-recv-complete",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_XFER));
  purple_signal_register(handle,"file-recv-request",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_XFER));
}

void purple_xfers_uninit()
{
  void *handle = purple_xfers_get_handle();
  purple_signals_disconnect_by_handle(handle);
  purple_signals_unregister_by_instance(handle);
  g_hash_table_destroy(xfers_data);
  xfers_data = ((GHashTable *)((void *)0));
}

void purple_xfers_set_ui_ops(PurpleXferUiOps *ops)
{
  xfer_ui_ops = ops;
}

PurpleXferUiOps *purple_xfers_get_ui_ops()
{
  return xfer_ui_ops;
}
