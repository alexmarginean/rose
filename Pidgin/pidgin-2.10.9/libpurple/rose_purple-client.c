#ifndef DBUS_API_SUBJECT_TO_CHANGE
#define DBUS_API_SUBJECT_TO_CHANGE
#endif
#include <dbus/dbus-glib.h>
#include <stdio.h>
#include <stdlib.h>
#include "dbus-purple.h"
#include "purple-client.h"
static DBusGConnection *bus;
static DBusGProxy *purple_proxy;

static GList *garray_int_to_glist(GArray *array)
{
  GList *list = (GList *)((void *)0);
  int i;
  for (i = 0; i < (array -> len); i++) 
    list = g_list_append(list,((gpointer )((glong )((gint *)((void *)(array -> data)))[i])));
  g_array_free(array,(!0));
  return list;
}

static GSList *garray_int_to_gslist(GArray *array)
{
  GSList *list = (GSList *)((void *)0);
  int i;
  for (i = 0; i < (array -> len); i++) 
    list = g_slist_append(list,((gpointer )((glong )((gint *)((void *)(array -> data)))[i])));
  g_array_free(array,(!0));
  return list;
}
#include "purple-client-bindings.c"
static void lose(const char *fmt,... );
static void lose_gerror(const char *prefix,GError *error);

static void lose(const char *str,... )
{
  va_list args;
  va_start(args,str);
  vfprintf(stderr,str,args);
  fputc(10,stderr);
  va_end(args);
  exit(1);
}

static void lose_gerror(const char *prefix,GError *error)
{
  lose("%s: %s",prefix,(error -> message));
}

void purple_init()
{
  GError *error = (GError *)((void *)0);
  g_type_init();
  bus = dbus_g_bus_get(DBUS_BUS_SESSION,&error);
  if (!(bus != 0)) 
    lose_gerror("Couldn\'t connect to session bus",error);
  purple_proxy = dbus_g_proxy_new_for_name(bus,"im.pidgin.purple.PurpleService","/im/pidgin/purple/PurpleObject","im.pidgin.purple.PurpleInterface");
  if (!(purple_proxy != 0)) 
    lose_gerror("Couldn\'t connect to the Purple Service",error);
}
