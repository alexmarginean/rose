/*
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "conversation.h"
#include "debug.h"
#include "network.h"
#include "notify.h"
#include "prpl.h"
#include "request.h"
#include "util.h"
/**************************************************************************/
/** @name Attention Type API                                              */
/**************************************************************************/

PurpleAttentionType *purple_attention_type_new(const char *ulname,const char *name,const char *inc_desc,const char *out_desc)
{
  PurpleAttentionType *attn = (PurpleAttentionType *)(g_malloc0_n(1,(sizeof(PurpleAttentionType ))));
  purple_attention_type_set_name(attn,name);
  purple_attention_type_set_incoming_desc(attn,inc_desc);
  purple_attention_type_set_outgoing_desc(attn,out_desc);
  purple_attention_type_set_unlocalized_name(attn,ulname);
  return attn;
}

void purple_attention_type_set_name(PurpleAttentionType *type,const char *name)
{
  do {
    if (type != ((PurpleAttentionType *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type != NULL");
      return ;
    };
  }while (0);
  type -> name = name;
}

void purple_attention_type_set_incoming_desc(PurpleAttentionType *type,const char *desc)
{
  do {
    if (type != ((PurpleAttentionType *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type != NULL");
      return ;
    };
  }while (0);
  type -> incoming_description = desc;
}

void purple_attention_type_set_outgoing_desc(PurpleAttentionType *type,const char *desc)
{
  do {
    if (type != ((PurpleAttentionType *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type != NULL");
      return ;
    };
  }while (0);
  type -> outgoing_description = desc;
}

void purple_attention_type_set_icon_name(PurpleAttentionType *type,const char *name)
{
  do {
    if (type != ((PurpleAttentionType *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type != NULL");
      return ;
    };
  }while (0);
  type -> icon_name = name;
}

void purple_attention_type_set_unlocalized_name(PurpleAttentionType *type,const char *ulname)
{
  do {
    if (type != ((PurpleAttentionType *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type != NULL");
      return ;
    };
  }while (0);
  type -> unlocalized_name = ulname;
}

const char *purple_attention_type_get_name(const PurpleAttentionType *type)
{
  do {
    if (type != ((const PurpleAttentionType *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type != NULL");
      return 0;
    };
  }while (0);
  return type -> name;
}

const char *purple_attention_type_get_incoming_desc(const PurpleAttentionType *type)
{
  do {
    if (type != ((const PurpleAttentionType *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type != NULL");
      return 0;
    };
  }while (0);
  return type -> incoming_description;
}

const char *purple_attention_type_get_outgoing_desc(const PurpleAttentionType *type)
{
  do {
    if (type != ((const PurpleAttentionType *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type != NULL");
      return 0;
    };
  }while (0);
  return type -> outgoing_description;
}

const char *purple_attention_type_get_icon_name(const PurpleAttentionType *type)
{
  do {
    if (type != ((const PurpleAttentionType *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type != NULL");
      return 0;
    };
  }while (0);
  if (((type -> icon_name) == ((const char *)((void *)0))) || (( *(type -> icon_name)) == 0)) 
    return 0;
  return type -> icon_name;
}

const char *purple_attention_type_get_unlocalized_name(const PurpleAttentionType *type)
{
  do {
    if (type != ((const PurpleAttentionType *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type != NULL");
      return 0;
    };
  }while (0);
  return type -> unlocalized_name;
}
/**************************************************************************/
/** @name Protocol Plugin API  */
/**************************************************************************/

void purple_prpl_got_account_idle(PurpleAccount *account,gboolean idle,time_t idle_time)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (purple_account_is_connected(account) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_account_is_connected(account)");
      return ;
    };
  }while (0);
  purple_presence_set_idle(purple_account_get_presence(account),idle,idle_time);
}

void purple_prpl_got_account_login_time(PurpleAccount *account,time_t login_time)
{
  PurplePresence *presence;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (purple_account_is_connected(account) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_account_is_connected(account)");
      return ;
    };
  }while (0);
  if (login_time == 0) 
    login_time = time(0);
  presence = purple_account_get_presence(account);
  purple_presence_set_login_time(presence,login_time);
}

void purple_prpl_got_account_status(PurpleAccount *account,const char *status_id,... )
{
  PurplePresence *presence;
  PurpleStatus *status;
  va_list args;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (status_id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"status_id != NULL");
      return ;
    };
  }while (0);
  do {
    if (purple_account_is_connected(account) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_account_is_connected(account)");
      return ;
    };
  }while (0);
  presence = purple_account_get_presence(account);
  status = purple_presence_get_status(presence,status_id);
  do {
    if (status != ((PurpleStatus *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"status != NULL");
      return ;
    };
  }while (0);
  va_start(args,status_id);
  purple_status_set_active_with_attrs(status,(!0),args);
  va_end(args);
}

void purple_prpl_got_account_actions(PurpleAccount *account)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (purple_account_is_connected(account) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_account_is_connected(account)");
      return ;
    };
  }while (0);
  purple_signal_emit(purple_accounts_get_handle(),"account-actions-changed",account);
}

void purple_prpl_got_user_idle(PurpleAccount *account,const char *name,gboolean idle,time_t idle_time)
{
  PurplePresence *presence;
  GSList *list;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return ;
    };
  }while (0);
  do {
    if ((purple_account_is_connected(account) != 0) || (purple_account_is_connecting(account) != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_account_is_connected(account) || purple_account_is_connecting(account)");
      return ;
    };
  }while (0);
  if ((list = purple_find_buddies(account,name)) == ((GSList *)((void *)0))) 
    return ;
  while(list != 0){
    presence = purple_buddy_get_presence((list -> data));
    list = g_slist_delete_link(list,list);
    purple_presence_set_idle(presence,idle,idle_time);
  }
}

void purple_prpl_got_user_login_time(PurpleAccount *account,const char *name,time_t login_time)
{
  GSList *list;
  PurplePresence *presence;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return ;
    };
  }while (0);
  if ((list = purple_find_buddies(account,name)) == ((GSList *)((void *)0))) 
    return ;
  if (login_time == 0) 
    login_time = time(0);
  while(list != 0){
    PurpleBuddy *buddy = (list -> data);
    presence = purple_buddy_get_presence(buddy);
    list = g_slist_delete_link(list,list);
    if (purple_presence_get_login_time(presence) != login_time) {
      purple_presence_set_login_time(presence,login_time);
      purple_signal_emit(purple_blist_get_handle(),"buddy-got-login-time",buddy);
    }
  }
}

void purple_prpl_got_user_status(PurpleAccount *account,const char *name,const char *status_id,... )
{
  GSList *list;
  GSList *l;
  PurpleBuddy *buddy;
  PurplePresence *presence;
  PurpleStatus *status;
  PurpleStatus *old_status;
  va_list args;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return ;
    };
  }while (0);
  do {
    if (status_id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"status_id != NULL");
      return ;
    };
  }while (0);
  do {
    if ((purple_account_is_connected(account) != 0) || (purple_account_is_connecting(account) != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_account_is_connected(account) || purple_account_is_connecting(account)");
      return ;
    };
  }while (0);
  if ((list = purple_find_buddies(account,name)) == ((GSList *)((void *)0))) 
    return ;
  for (l = list; l != ((GSList *)((void *)0)); l = (l -> next)) {
    buddy = (l -> data);
    presence = purple_buddy_get_presence(buddy);
    status = purple_presence_get_status(presence,status_id);
    if (((PurpleStatus *)((void *)0)) == status) 
/*
			 * TODO: This should never happen, right?  We should call
			 *       g_warning() or something.
			 */
      continue; 
    old_status = purple_presence_get_active_status(presence);
    va_start(args,status_id);
    purple_status_set_active_with_attrs(status,(!0),args);
    va_end(args);
    purple_blist_update_buddy_status(buddy,old_status);
  }
  g_slist_free(list);
/* The buddy is no longer online, they are therefore by definition not
	 * still typing to us. */
  if (!(purple_status_is_online(status) != 0)) {
    serv_got_typing_stopped(purple_account_get_connection(account),name);
    purple_prpl_got_media_caps(account,name);
  }
}

void purple_prpl_got_user_status_deactive(PurpleAccount *account,const char *name,const char *status_id)
{
  GSList *list;
  GSList *l;
  PurpleBuddy *buddy;
  PurplePresence *presence;
  PurpleStatus *status;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return ;
    };
  }while (0);
  do {
    if (status_id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"status_id != NULL");
      return ;
    };
  }while (0);
  do {
    if ((purple_account_is_connected(account) != 0) || (purple_account_is_connecting(account) != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_account_is_connected(account) || purple_account_is_connecting(account)");
      return ;
    };
  }while (0);
  if ((list = purple_find_buddies(account,name)) == ((GSList *)((void *)0))) 
    return ;
  for (l = list; l != ((GSList *)((void *)0)); l = (l -> next)) {
    buddy = (l -> data);
    presence = purple_buddy_get_presence(buddy);
    status = purple_presence_get_status(presence,status_id);
    if (((PurpleStatus *)((void *)0)) == status) 
      continue; 
    if (purple_status_is_active(status) != 0) {
      purple_status_set_active(status,0);
      purple_blist_update_buddy_status(buddy,status);
    }
  }
  g_slist_free(list);
}

static void do_prpl_change_account_status(PurpleAccount *account,PurpleStatus *old_status,PurpleStatus *new_status)
{
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  if (((purple_status_is_online(new_status) != 0) && (purple_account_is_disconnected(account) != 0)) && (purple_network_is_available() != 0)) {
    purple_account_connect(account);
    return ;
  }
  if (!(purple_status_is_online(new_status) != 0)) {
    if (!(purple_account_is_disconnected(account) != 0)) 
      purple_account_disconnect(account);
    else 
/* Clear out the unsaved password if we're already disconnected and we switch to offline status */
if (!(purple_account_get_remember_password(account) != 0)) 
      purple_account_set_password(account,0);
    return ;
  }
  if (purple_account_is_connecting(account) != 0) 
/*
		 * We don't need to call the set_status PRPL function because
		 * the PRPL will take care of setting its status during the
		 * connection process.
		 */
    return ;
  prpl = purple_find_prpl(purple_account_get_protocol_id(account));
  if (prpl == ((PurplePlugin *)((void *)0))) 
    return ;
  prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if (!(purple_account_is_disconnected(account) != 0) && ((prpl_info -> set_status) != ((void (*)(PurpleAccount *, PurpleStatus *))((void *)0)))) {
    ( *(prpl_info -> set_status))(account,new_status);
  }
}

void purple_prpl_change_account_status(PurpleAccount *account,PurpleStatus *old_status,PurpleStatus *new_status)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (new_status != ((PurpleStatus *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"new_status != NULL");
      return ;
    };
  }while (0);
  do {
    if (!(purple_status_is_exclusive(new_status) != 0) || (old_status != ((PurpleStatus *)((void *)0)))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"!purple_status_is_exclusive(new_status) || old_status != NULL");
      return ;
    };
  }while (0);
  do_prpl_change_account_status(account,old_status,new_status);
  purple_signal_emit(purple_accounts_get_handle(),"account-status-changed",account,old_status,new_status);
}

GList *purple_prpl_get_statuses(PurpleAccount *account,PurplePresence *presence)
{
  GList *statuses = (GList *)((void *)0);
  GList *l;
  PurpleStatus *status;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  do {
    if (presence != ((PurplePresence *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"presence != NULL");
      return 0;
    };
  }while (0);
  for (l = purple_account_get_status_types(account); l != ((GList *)((void *)0)); l = (l -> next)) {
    status = purple_status_new(((PurpleStatusType *)(l -> data)),presence);
    statuses = g_list_prepend(statuses,status);
  }
  statuses = g_list_reverse(statuses);
  return statuses;
}

static void purple_prpl_attention(PurpleConversation *conv,const char *who,guint type,PurpleMessageFlags flags,time_t mtime)
{
  PurpleAccount *account = purple_conversation_get_account(conv);
  purple_signal_emit(purple_conversations_get_handle(),((flags == PURPLE_MESSAGE_SEND)?"sent-attention" : "got-attention"),account,who,conv,type);
}

void purple_prpl_send_attention(PurpleConnection *gc,const char *who,guint type_code)
{
  PurpleAttentionType *attn;
  PurpleMessageFlags flags;
  PurplePlugin *prpl;
  PurpleConversation *conv;
  gboolean (*send_attention)(PurpleConnection *, const char *, guint );
  PurpleBuddy *buddy;
  const char *alias;
  gchar *description;
  time_t mtime;
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  do {
    if (who != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"who != NULL");
      return ;
    };
  }while (0);
  prpl = purple_find_prpl(purple_account_get_protocol_id((gc -> account)));
  send_attention = ( *((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info)).send_attention;
  do {
    if (send_attention != ((gboolean (*)(PurpleConnection *, const char *, guint ))((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"send_attention != NULL");
      return ;
    };
  }while (0);
  mtime = time(0);
  attn = purple_get_attention_type_from_code((gc -> account),type_code);
  if ((buddy = purple_find_buddy(purple_connection_get_account(gc),who)) != ((PurpleBuddy *)((void *)0))) 
    alias = purple_buddy_get_contact_alias(buddy);
  else 
    alias = who;
  if ((attn != 0) && (purple_attention_type_get_outgoing_desc(attn) != 0)) {
    description = g_strdup_printf(purple_attention_type_get_outgoing_desc(attn),alias);
  }
  else {
    description = g_strdup_printf(((const char *)(dgettext("pidgin","Requesting %s\'s attention..."))),alias);
  }
  flags = (PURPLE_MESSAGE_SEND | PURPLE_MESSAGE_NOTIFY | PURPLE_MESSAGE_SYSTEM);
  purple_debug_info("server","serv_send_attention: sending \'%s\' to %s\n",description,who);
  if (!(( *send_attention)(gc,who,type_code) != 0)) 
    return ;
  conv = purple_conversation_new(PURPLE_CONV_TYPE_IM,(gc -> account),who);
  purple_conv_im_write(purple_conversation_get_im_data(conv),0,description,flags,mtime);
  purple_prpl_attention(conv,who,type_code,PURPLE_MESSAGE_SEND,time(0));
  g_free(description);
}

static void got_attention(PurpleConnection *gc,int id,const char *who,guint type_code)
{
  PurpleMessageFlags flags;
  PurpleAttentionType *attn;
  PurpleBuddy *buddy;
  const char *alias;
  gchar *description;
  time_t mtime;
  mtime = time(0);
  attn = purple_get_attention_type_from_code((gc -> account),type_code);
/* PURPLE_MESSAGE_NOTIFY is for attention messages. */
  flags = (PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NOTIFY | PURPLE_MESSAGE_RECV);
/* TODO: if (attn->icon_name) is non-null, use it to lookup an emoticon and display
	 * it next to the attention command. And if it is null, display a generic icon. */
  if ((buddy = purple_find_buddy(purple_connection_get_account(gc),who)) != ((PurpleBuddy *)((void *)0))) 
    alias = purple_buddy_get_contact_alias(buddy);
  else 
    alias = who;
  if ((attn != 0) && (purple_attention_type_get_incoming_desc(attn) != 0)) {
    description = g_strdup_printf(purple_attention_type_get_incoming_desc(attn),alias);
  }
  else {
    description = g_strdup_printf(((const char *)(dgettext("pidgin","%s has requested your attention!"))),alias);
  }
  purple_debug_info("server","got_attention: got \'%s\' from %s\n",description,who);
  if (id == -1) 
    serv_got_im(gc,who,description,flags,mtime);
  else 
    serv_got_chat_in(gc,id,who,flags,description,mtime);
/* TODO: sounds (depending on PurpleAttentionType), shaking, etc. */
  g_free(description);
}

void purple_prpl_got_attention(PurpleConnection *gc,const char *who,guint type_code)
{
  PurpleConversation *conv = (PurpleConversation *)((void *)0);
  PurpleAccount *account = purple_connection_get_account(gc);
  got_attention(gc,-1,who,type_code);
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_ANY,who,account);
  if (conv != 0) 
    purple_prpl_attention(conv,who,type_code,PURPLE_MESSAGE_RECV,time(0));
}

void purple_prpl_got_attention_in_chat(PurpleConnection *gc,int id,const char *who,guint type_code)
{
  got_attention(gc,id,who,type_code);
}

gboolean purple_prpl_initiate_media(PurpleAccount *account,const char *who,PurpleMediaSessionType type)
{
#ifdef USE_VV
  PurpleConnection *gc = (PurpleConnection *)((void *)0);
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  if (account != 0) 
    gc = purple_account_get_connection(account);
  if (gc != 0) 
    prpl = purple_connection_get_prpl(gc);
  if (prpl != 0) 
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if ((prpl_info != 0) && (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).initiate_media))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).initiate_media))) < (prpl_info -> struct_size))) && ((prpl_info -> initiate_media) != ((gboolean (*)(PurpleAccount *, const char *, PurpleMediaSessionType ))((void *)0))))) {
/* should check that the protocol supports this media type here? */
    return ( *(prpl_info -> initiate_media))(account,who,type);
  }
  else 
#endif
    return 0;
}

PurpleMediaCaps purple_prpl_get_media_caps(PurpleAccount *account,const char *who)
{
#ifdef USE_VV
  PurpleConnection *gc = (PurpleConnection *)((void *)0);
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  if (account != 0) 
    gc = purple_account_get_connection(account);
  if (gc != 0) 
    prpl = purple_connection_get_prpl(gc);
  if (prpl != 0) 
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if ((prpl_info != 0) && (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).get_media_caps))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).get_media_caps))) < (prpl_info -> struct_size))) && ((prpl_info -> get_media_caps) != ((PurpleMediaCaps (*)(PurpleAccount *, const char *))((void *)0))))) {
    return ( *(prpl_info -> get_media_caps))(account,who);
  }
#endif
  return PURPLE_MEDIA_CAPS_NONE;
}

void purple_prpl_got_media_caps(PurpleAccount *account,const char *name)
{
#ifdef USE_VV
  GSList *list;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return ;
    };
  }while (0);
  if ((list = purple_find_buddies(account,name)) == ((GSList *)((void *)0))) 
    return ;
  while(list != 0){{
      PurpleBuddy *buddy = (list -> data);
      PurpleMediaCaps oldcaps = purple_buddy_get_media_caps(buddy);
      PurpleMediaCaps newcaps = 0;
      const gchar *bname = purple_buddy_get_name(buddy);
      list = g_slist_delete_link(list,list);
      newcaps = purple_prpl_get_media_caps(account,bname);
      purple_buddy_set_media_caps(buddy,newcaps);
      if (oldcaps == newcaps) 
        continue; 
      purple_signal_emit(purple_blist_get_handle(),"buddy-caps-changed",buddy,newcaps,oldcaps);
    }
  }
#endif
}
/**************************************************************************
 * Protocol Plugin Subsystem API
 **************************************************************************/

PurplePlugin *purple_find_prpl(const char *id)
{
  GList *l;
  PurplePlugin *plugin;
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  for (l = purple_plugins_get_protocols(); l != ((GList *)((void *)0)); l = (l -> next)) {
    plugin = ((PurplePlugin *)(l -> data));
    if (purple_strequal(( *(plugin -> info)).id,id) != 0) 
      return plugin;
  }
  return 0;
}
