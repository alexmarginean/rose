/**
 * @file log.c Logging API
 * @ingroup core
 */
/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "account.h"
#include "dbus-maybe.h"
#include "debug.h"
#include "internal.h"
#include "log.h"
#include "prefs.h"
#include "util.h"
#include "stringref.h"
#include "imgstore.h"
#include "time.h"
static GSList *loggers = (GSList *)((void *)0);
static PurpleLogLogger *html_logger;
static PurpleLogLogger *txt_logger;
static PurpleLogLogger *old_logger;

struct _purple_logsize_user 
{
  char *name;
  PurpleAccount *account;
}
;
static GHashTable *logsize_users = (GHashTable *)((void *)0);
static GHashTable *logsize_users_decayed = (GHashTable *)((void *)0);
static void log_get_log_sets_common(GHashTable *sets);
static gsize html_logger_write(PurpleLog *log,PurpleMessageFlags type,const char *from,time_t time,const char *message);
static void html_logger_finalize(PurpleLog *log);
static GList *html_logger_list(PurpleLogType type,const char *sn,PurpleAccount *account);
static GList *html_logger_list_syslog(PurpleAccount *account);
static char *html_logger_read(PurpleLog *log,PurpleLogReadFlags *flags);
static int html_logger_total_size(PurpleLogType type,const char *name,PurpleAccount *account);
static GList *old_logger_list(PurpleLogType type,const char *sn,PurpleAccount *account);
static int old_logger_total_size(PurpleLogType type,const char *name,PurpleAccount *account);
static char *old_logger_read(PurpleLog *log,PurpleLogReadFlags *flags);
static int old_logger_size(PurpleLog *log);
static void old_logger_get_log_sets(PurpleLogSetCallback cb,GHashTable *sets);
static void old_logger_finalize(PurpleLog *log);
static gsize txt_logger_write(PurpleLog *log,PurpleMessageFlags type,const char *from,time_t time,const char *message);
static void txt_logger_finalize(PurpleLog *log);
static GList *txt_logger_list(PurpleLogType type,const char *sn,PurpleAccount *account);
static GList *txt_logger_list_syslog(PurpleAccount *account);
static char *txt_logger_read(PurpleLog *log,PurpleLogReadFlags *flags);
static int txt_logger_total_size(PurpleLogType type,const char *name,PurpleAccount *account);
/**************************************************************************
 * PUBLIC LOGGING FUNCTIONS ***********************************************
 **************************************************************************/

PurpleLog *purple_log_new(PurpleLogType type,const char *name,PurpleAccount *account,PurpleConversation *conv,time_t time,const struct tm *tm)
{
  PurpleLog *log;
/* IMPORTANT: Make sure to initialize all the members of PurpleLog */
  log = ((PurpleLog *)(g_slice_alloc((sizeof(PurpleLog )))));
{
    PurpleLog *typed_ptr = log;
    purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurpleLog);
  };
  log -> type = type;
  log -> name = g_strdup(purple_normalize(account,name));
  log -> account = account;
  log -> conv = conv;
  log -> time = time;
  log -> logger = purple_log_logger_get();
  log -> logger_data = ((void *)((void *)0));
  if (tm == ((const struct tm *)((void *)0))) 
    log -> tm = ((struct tm *)((void *)0));
  else {
/* There's no need to zero this as we immediately do a direct copy. */
    log -> tm = ((struct tm *)(g_slice_alloc((sizeof(struct tm )))));
     *(log -> tm) =  *tm;
#ifdef HAVE_STRUCT_TM_TM_ZONE
/* XXX: This is so wrong... */
    if (( *(log -> tm)).tm_zone != ((const char *)((void *)0))) {
      char *tmp = g_locale_from_utf8(( *(log -> tm)).tm_zone,(-1),0,0,0);
      if (tmp != ((char *)((void *)0))) 
        ( *(log -> tm)).tm_zone = tmp;
      else 
/* Just shove the UTF-8 bytes in and hope... */
        ( *(log -> tm)).tm_zone = (g_strdup(( *(log -> tm)).tm_zone));
    }
#endif
  }
  if (((log -> logger) != 0) && (( *(log -> logger)).create != 0)) 
    ( *( *(log -> logger)).create)(log);
  return log;
}

void purple_log_free(PurpleLog *log)
{
  do {
    if (log != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"log");
      return ;
    };
  }while (0);
  if (((log -> logger) != 0) && (( *(log -> logger)).finalize != 0)) 
    ( *( *(log -> logger)).finalize)(log);
  g_free((log -> name));
  if ((log -> tm) != ((struct tm *)((void *)0))) {
#ifdef HAVE_STRUCT_TM_TM_ZONE
/* XXX: This is so wrong... */
    g_free(((char *)( *(log -> tm)).tm_zone));
#endif
    do {
      if (1) 
        g_slice_free1((sizeof(struct tm )),(log -> tm));
      else 
        ((struct tm *)((struct tm *)0)) == (log -> tm);
    }while (0);
  }
  purple_dbus_unregister_pointer(log);
  do {
    if (1) 
      g_slice_free1((sizeof(PurpleLog )),log);
    else 
      ((PurpleLog *)((PurpleLog *)0)) == log;
  }while (0);
}

void purple_log_write(PurpleLog *log,PurpleMessageFlags type,const char *from,time_t time,const char *message)
{
  struct _purple_logsize_user *lu;
  gsize written;
  gsize total = 0;
  gpointer ptrsize;
  do {
    if (log != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"log");
      return ;
    };
  }while (0);
  do {
    if ((log -> logger) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"log->logger");
      return ;
    };
  }while (0);
  do {
    if (( *(log -> logger)).write != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"log->logger->write");
      return ;
    };
  }while (0);
  written = ( *( *(log -> logger)).write)(log,type,from,time,message);
  lu = ((struct _purple_logsize_user *)(g_malloc_n(1,(sizeof(struct _purple_logsize_user )))));
  lu -> name = g_strdup(purple_normalize((log -> account),(log -> name)));
  lu -> account = (log -> account);
  if (g_hash_table_lookup_extended(logsize_users,lu,0,&ptrsize) != 0) {
    char *tmp = (lu -> name);
    total = ((gint )((glong )ptrsize));
    total += written;
    g_hash_table_replace(logsize_users,lu,((gpointer )((glong )total)));
/* The hash table takes ownership of lu, so create a new one
		 * for the logsize_users_decayed check below. */
    lu = ((struct _purple_logsize_user *)(g_malloc_n(1,(sizeof(struct _purple_logsize_user )))));
    lu -> name = g_strdup(tmp);
    lu -> account = (log -> account);
  }
  if (g_hash_table_lookup_extended(logsize_users_decayed,lu,0,&ptrsize) != 0) {
    total = ((gint )((glong )ptrsize));
    total += written;
    g_hash_table_replace(logsize_users_decayed,lu,((gpointer )((glong )total)));
  }
  else {
    g_free((lu -> name));
    g_free(lu);
  }
}

char *purple_log_read(PurpleLog *log,PurpleLogReadFlags *flags)
{
  PurpleLogReadFlags mflags;
  do {
    if ((log != 0) && ((log -> logger) != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"log && log->logger");
      return 0;
    };
  }while (0);
  if (( *(log -> logger)).read != 0) {
    char *ret = ( *( *(log -> logger)).read)(log,((flags != 0)?flags : &mflags));
    purple_str_strip_char(ret,13);
    return ret;
  }
  return g_strdup(((const char *)(dgettext("pidgin","<b><font color=\"red\">The logger has no read function</font></b>"))));
}

int purple_log_get_size(PurpleLog *log)
{
  do {
    if ((log != 0) && ((log -> logger) != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"log && log->logger");
      return 0;
    };
  }while (0);
  if (( *(log -> logger)).size != 0) 
    return ( *( *(log -> logger)).size)(log);
  return 0;
}

static guint _purple_logsize_user_hash(struct _purple_logsize_user *lu)
{
  return g_str_hash((lu -> name));
}

static guint _purple_logsize_user_equal(struct _purple_logsize_user *lu1,struct _purple_logsize_user *lu2)
{
  return (((lu1 -> account) == (lu2 -> account)) && (purple_strequal((lu1 -> name),(lu2 -> name)) != 0));
}

static void _purple_logsize_user_free_key(struct _purple_logsize_user *lu)
{
  g_free((lu -> name));
  g_free(lu);
}

int purple_log_get_total_size(PurpleLogType type,const char *name,PurpleAccount *account)
{
  gpointer ptrsize;
  int size = 0;
  GSList *n;
  struct _purple_logsize_user *lu;
  lu = ((struct _purple_logsize_user *)(g_malloc_n(1,(sizeof(struct _purple_logsize_user )))));
  lu -> name = g_strdup(purple_normalize(account,name));
  lu -> account = account;
  if (g_hash_table_lookup_extended(logsize_users,lu,0,&ptrsize) != 0) {
    size = ((gint )((glong )ptrsize));
    g_free((lu -> name));
    g_free(lu);
  }
  else {
    for (n = loggers; n != 0; n = (n -> next)) {
      PurpleLogLogger *logger = (n -> data);
      if ((logger -> total_size) != 0) {
        size += ( *(logger -> total_size))(type,name,account);
      }
      else if ((logger -> list) != 0) {
        GList *logs = ( *(logger -> list))(type,name,account);
        int this_size = 0;
        while(logs != 0){
          PurpleLog *log = (PurpleLog *)(logs -> data);
          this_size += purple_log_get_size(log);
          purple_log_free(log);
          logs = g_list_delete_link(logs,logs);
        }
        size += this_size;
      }
    }
    g_hash_table_replace(logsize_users,lu,((gpointer )((glong )size)));
  }
  return size;
}

int purple_log_get_activity_score(PurpleLogType type,const char *name,PurpleAccount *account)
{
  gpointer ptrscore;
  int score;
  GSList *n;
  struct _purple_logsize_user *lu;
  time_t now;
  time(&now);
  lu = ((struct _purple_logsize_user *)(g_malloc_n(1,(sizeof(struct _purple_logsize_user )))));
  lu -> name = g_strdup(purple_normalize(account,name));
  lu -> account = account;
  if (g_hash_table_lookup_extended(logsize_users_decayed,lu,0,&ptrscore) != 0) {
    score = ((gint )((glong )ptrscore));
    g_free((lu -> name));
    g_free(lu);
  }
  else {
    double score_double = 0.0;
    for (n = loggers; n != 0; n = (n -> next)) {
      PurpleLogLogger *logger = (n -> data);
      if ((logger -> list) != 0) {
        GList *logs = ( *(logger -> list))(type,name,account);
        while(logs != 0){
          PurpleLog *log = (PurpleLog *)(logs -> data);
/* Activity score counts bytes in the log, exponentially
					   decayed with a half-life of 14 days. */
          score_double += ((purple_log_get_size(log)) * pow(0.5,(difftime(now,(log -> time)) / 1209600.0)));
          purple_log_free(log);
          logs = g_list_delete_link(logs,logs);
        }
      }
    }
    score = ((gint )(ceil(score_double)));
    g_hash_table_replace(logsize_users_decayed,lu,((gpointer )((glong )score)));
  }
  return score;
}

gboolean purple_log_is_deletable(PurpleLog *log)
{
  do {
    if (log != ((PurpleLog *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"log != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((log -> logger) != ((PurpleLogLogger *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"log->logger != NULL");
      return 0;
    };
  }while (0);
  if (( *(log -> logger)).remove == ((gboolean (*)(PurpleLog *))((void *)0))) 
    return 0;
  if (( *(log -> logger)).is_deletable != ((gboolean (*)(PurpleLog *))((void *)0))) 
    return ( *( *(log -> logger)).is_deletable)(log);
  return (!0);
}

gboolean purple_log_delete(PurpleLog *log)
{
  do {
    if (log != ((PurpleLog *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"log != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((log -> logger) != ((PurpleLogLogger *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"log->logger != NULL");
      return 0;
    };
  }while (0);
  if (( *(log -> logger)).remove != ((gboolean (*)(PurpleLog *))((void *)0))) 
    return ( *( *(log -> logger)).remove)(log);
  return 0;
}

char *purple_log_get_log_dir(PurpleLogType type,const char *name,PurpleAccount *account)
{
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  const char *prpl_name;
  char *acct_name;
  const char *target;
  char *dir;
  prpl = purple_find_prpl(purple_account_get_protocol_id(account));
  if (!(prpl != 0)) 
    return 0;
  prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  prpl_name = ( *(prpl_info -> list_icon))(account,0);
  acct_name = g_strdup(purple_escape_filename(purple_normalize(account,purple_account_get_username(account))));
  if (type == PURPLE_LOG_CHAT) {
    char *temp = g_strdup_printf("%s.chat",purple_normalize(account,name));
    target = purple_escape_filename(temp);
    g_free(temp);
  }
  else if (type == PURPLE_LOG_SYSTEM) {
    target = ".system";
  }
  else {
    target = purple_escape_filename(purple_normalize(account,name));
  }
  dir = g_build_filename(purple_user_dir(),"logs",prpl_name,acct_name,target,((void *)((void *)0)));
  g_free(acct_name);
  return dir;
}
/****************************************************************************
 * LOGGER FUNCTIONS *********************************************************
 ****************************************************************************/
static PurpleLogLogger *current_logger = (PurpleLogLogger *)((void *)0);

static void logger_pref_cb(const char *name,PurplePrefType type,gconstpointer value,gpointer data)
{
  PurpleLogLogger *logger;
  GSList *l = loggers;
  while(l != 0){
    logger = (l -> data);
    if (purple_strequal((logger -> id),value) != 0) {
      purple_log_logger_set(logger);
      return ;
    }
    l = (l -> next);
  }
  purple_log_logger_set(txt_logger);
}

PurpleLogLogger *purple_log_logger_new(const char *id,const char *name,int functions,... )
{
#if 0
#endif
  PurpleLogLogger *logger;
  va_list args;
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return 0;
    };
  }while (0);
  do {
    if (functions >= 1) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"functions >= 1");
      return 0;
    };
  }while (0);
  logger = ((PurpleLogLogger *)(g_malloc0_n(1,(sizeof(PurpleLogLogger )))));
  logger -> id = g_strdup(id);
  logger -> name = g_strdup(name);
  va_start(args,functions);
  if (functions >= 1) 
    logger -> create = (va_arg(args,void *));
  if (functions >= 2) 
    logger -> write = (va_arg(args,void *));
  if (functions >= 3) 
    logger -> finalize = (va_arg(args,void *));
  if (functions >= 4) 
    logger -> list = (va_arg(args,void *));
  if (functions >= 5) 
    logger -> read = (va_arg(args,void *));
  if (functions >= 6) 
    logger -> size = (va_arg(args,void *));
  if (functions >= 7) 
    logger -> total_size = (va_arg(args,void *));
  if (functions >= 8) 
    logger -> list_syslog = (va_arg(args,void *));
  if (functions >= 9) 
    logger -> get_log_sets = (va_arg(args,void *));
  if (functions >= 10) 
    logger -> remove = (va_arg(args,void *));
  if (functions >= 11) 
    logger -> is_deletable = (va_arg(args,void *));
  if (functions >= 12) 
    purple_debug_info("log","Dropping new functions for logger: %s (%s)\n",name,id);
  va_end(args);
  return logger;
}

void purple_log_logger_free(PurpleLogLogger *logger)
{
  g_free((logger -> name));
  g_free((logger -> id));
  g_free(logger);
}

void purple_log_logger_add(PurpleLogLogger *logger)
{
  do {
    if (logger != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"logger");
      return ;
    };
  }while (0);
  if (g_slist_find(loggers,logger) != 0) 
    return ;
  loggers = g_slist_append(loggers,logger);
  if (purple_strequal(purple_prefs_get_string("/purple/logging/format"),(logger -> id)) != 0) {
    purple_prefs_trigger_callback("/purple/logging/format");
  }
}

void purple_log_logger_remove(PurpleLogLogger *logger)
{
  do {
    if (logger != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"logger");
      return ;
    };
  }while (0);
  loggers = g_slist_remove(loggers,logger);
}

void purple_log_logger_set(PurpleLogLogger *logger)
{
  do {
    if (logger != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"logger");
      return ;
    };
  }while (0);
  current_logger = logger;
}

PurpleLogLogger *purple_log_logger_get()
{
  return current_logger;
}

GList *purple_log_logger_get_options()
{
  GSList *n;
  GList *list = (GList *)((void *)0);
  PurpleLogLogger *data;
  for (n = loggers; n != 0; n = (n -> next)) {
    data = (n -> data);
    if (!((data -> write) != 0)) 
      continue; 
    list = g_list_append(list,(data -> name));
    list = g_list_append(list,(data -> id));
  }
  return list;
}

gint purple_log_compare(gconstpointer y,gconstpointer z)
{
  const PurpleLog *a = y;
  const PurpleLog *b = z;
  return ((b -> time) - (a -> time));
}

GList *purple_log_get_logs(PurpleLogType type,const char *name,PurpleAccount *account)
{
  GList *logs = (GList *)((void *)0);
  GSList *n;
  for (n = loggers; n != 0; n = (n -> next)) {{
      PurpleLogLogger *logger = (n -> data);
      if (!((logger -> list) != 0)) 
        continue; 
      logs = g_list_concat(( *(logger -> list))(type,name,account),logs);
    }
  }
  return g_list_sort(logs,purple_log_compare);
}

gint purple_log_set_compare(gconstpointer y,gconstpointer z)
{
  const PurpleLogSet *a = y;
  const PurpleLogSet *b = z;
  gint ret = 0;
/* This logic seems weird at first...
	 * If either account is NULL, we pretend the accounts are
	 * equal. This allows us to detect duplicates that will
	 * exist if one logger knows the account and another
	 * doesn't. */
  if (((a -> account) != ((PurpleAccount *)((void *)0))) && ((b -> account) != ((PurpleAccount *)((void *)0)))) {
    ret = strcmp(purple_account_get_username((a -> account)),purple_account_get_username((b -> account)));
    if (ret != 0) 
      return ret;
  }
  ret = strcmp((a -> normalized_name),(b -> normalized_name));
  if (ret != 0) 
    return ret;
  return ((gint )(b -> type)) - ((gint )(a -> type));
}

static guint log_set_hash(gconstpointer key)
{
  const PurpleLogSet *set = key;
/* The account isn't hashed because we need PurpleLogSets with NULL accounts
	 * to be found when we search by a PurpleLogSet that has a non-NULL account
	 * but the same type and name. */
  return g_int_hash((&set -> type)) + g_str_hash((set -> name));
}

static gboolean log_set_equal(gconstpointer a,gconstpointer b)
{
/* I realize that the choices made for GList and GHashTable
	 * make sense for those data types, but I wish the comparison
	 * functions were compatible. */
  return !(purple_log_set_compare(a,b) != 0);
}

static void log_add_log_set_to_hash(GHashTable *sets,PurpleLogSet *set)
{
  PurpleLogSet *existing_set = (g_hash_table_lookup(sets,set));
  if (existing_set == ((PurpleLogSet *)((void *)0))) 
    g_hash_table_insert(sets,set,set);
  else if (((existing_set -> account) == ((PurpleAccount *)((void *)0))) && ((set -> account) != ((PurpleAccount *)((void *)0)))) 
    g_hash_table_replace(sets,set,set);
  else 
    purple_log_set_free(set);
}

GHashTable *purple_log_get_log_sets()
{
  GSList *n;
  GHashTable *sets = g_hash_table_new_full(log_set_hash,log_set_equal,((GDestroyNotify )purple_log_set_free),0);
/* Get the log sets from all the loggers. */
  for (n = loggers; n != 0; n = (n -> next)) {{
      PurpleLogLogger *logger = (n -> data);
      if (!((logger -> get_log_sets) != 0)) 
        continue; 
      ( *(logger -> get_log_sets))(log_add_log_set_to_hash,sets);
    }
  }
  log_get_log_sets_common(sets);
/* Return the GHashTable of unique PurpleLogSets. */
  return sets;
}

void purple_log_set_free(PurpleLogSet *set)
{
  do {
    if (set != ((PurpleLogSet *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"set != NULL");
      return ;
    };
  }while (0);
  g_free((set -> name));
  if ((set -> normalized_name) != (set -> name)) 
    g_free((set -> normalized_name));
  do {
    if (1) 
      g_slice_free1((sizeof(PurpleLogSet )),set);
    else 
      ((PurpleLogSet *)((PurpleLogSet *)0)) == set;
  }while (0);
}

GList *purple_log_get_system_logs(PurpleAccount *account)
{
  GList *logs = (GList *)((void *)0);
  GSList *n;
  for (n = loggers; n != 0; n = (n -> next)) {{
      PurpleLogLogger *logger = (n -> data);
      if (!((logger -> list_syslog) != 0)) 
        continue; 
      logs = g_list_concat(( *(logger -> list_syslog))(account),logs);
    }
  }
  return g_list_sort(logs,purple_log_compare);
}
/****************************************************************************
 * LOG SUBSYSTEM ************************************************************
 ****************************************************************************/

void *purple_log_get_handle()
{
  static int handle;
  return (&handle);
}

void purple_log_init()
{
  void *handle = purple_log_get_handle();
  purple_prefs_add_none("/purple/logging");
  purple_prefs_add_bool("/purple/logging/log_ims",(!0));
  purple_prefs_add_bool("/purple/logging/log_chats",(!0));
  purple_prefs_add_bool("/purple/logging/log_system",0);
  purple_prefs_add_string("/purple/logging/format","html");
  html_logger = purple_log_logger_new("html",((const char *)(dgettext("pidgin","HTML"))),11,((void *)((void *)0)),html_logger_write,html_logger_finalize,html_logger_list,html_logger_read,purple_log_common_sizer,html_logger_total_size,html_logger_list_syslog,((void *)((void *)0)),purple_log_common_deleter,purple_log_common_is_deletable);
  purple_log_logger_add(html_logger);
  txt_logger = purple_log_logger_new("txt",((const char *)(dgettext("pidgin","Plain text"))),11,((void *)((void *)0)),txt_logger_write,txt_logger_finalize,txt_logger_list,txt_logger_read,purple_log_common_sizer,txt_logger_total_size,txt_logger_list_syslog,((void *)((void *)0)),purple_log_common_deleter,purple_log_common_is_deletable);
  purple_log_logger_add(txt_logger);
  old_logger = purple_log_logger_new("old",((const char *)(dgettext("pidgin","Old flat format"))),9,((void *)((void *)0)),((void *)((void *)0)),old_logger_finalize,old_logger_list,old_logger_read,old_logger_size,old_logger_total_size,((void *)((void *)0)),old_logger_get_log_sets);
  purple_log_logger_add(old_logger);
  purple_signal_register(handle,"log-timestamp",purple_marshal_POINTER__POINTER_INT64_BOOLEAN,purple_value_new(PURPLE_TYPE_STRING),3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_LOG),purple_value_new(PURPLE_TYPE_INT64),purple_value_new(PURPLE_TYPE_BOOLEAN));
#if SIZEOF_TIME_T == 4
#elif SIZEOF_TIME_T == 8
#else
#error Unknown size of time_t
#endif
#if SIZEOF_TIME_T == 4
#elif SIZEOF_TIME_T == 8
#else
# error Unknown size of time_t
#endif
  purple_prefs_connect_callback(0,"/purple/logging/format",logger_pref_cb,0);
  purple_prefs_trigger_callback("/purple/logging/format");
  logsize_users = g_hash_table_new_full(((GHashFunc )_purple_logsize_user_hash),((GEqualFunc )_purple_logsize_user_equal),((GDestroyNotify )_purple_logsize_user_free_key),0);
  logsize_users_decayed = g_hash_table_new_full(((GHashFunc )_purple_logsize_user_hash),((GEqualFunc )_purple_logsize_user_equal),((GDestroyNotify )_purple_logsize_user_free_key),0);
}

void purple_log_uninit()
{
  purple_signals_unregister_by_instance(purple_log_get_handle());
  purple_log_logger_remove(html_logger);
  purple_log_logger_free(html_logger);
  html_logger = ((PurpleLogLogger *)((void *)0));
  purple_log_logger_remove(txt_logger);
  purple_log_logger_free(txt_logger);
  txt_logger = ((PurpleLogLogger *)((void *)0));
  purple_log_logger_remove(old_logger);
  purple_log_logger_free(old_logger);
  old_logger = ((PurpleLogLogger *)((void *)0));
  g_hash_table_destroy(logsize_users);
  g_hash_table_destroy(logsize_users_decayed);
}
/****************************************************************************
 * LOGGERS ******************************************************************
 ****************************************************************************/

static char *log_get_timestamp(PurpleLog *log,time_t when)
{
  gboolean show_date;
  char *date;
  struct tm *tm;
  show_date = (((log -> type) == PURPLE_LOG_SYSTEM) || (time(0) > (when + (20 * 60))));
  date = (purple_signal_emit_return_1(purple_log_get_handle(),"log-timestamp",log,when,show_date));
  if (date != ((char *)((void *)0))) 
    return date;
  tm = localtime((&when));
  if (show_date != 0) 
    return g_strdup(purple_date_format_long(tm));
  else 
    return g_strdup(purple_time_format(tm));
}
/* NOTE: This can return msg (which you may or may not want to g_free())
 * NOTE: or a newly allocated string which you MUST g_free(). */

static char *convert_image_tags(const PurpleLog *log,const char *msg)
{
  const char *tmp;
  const char *start;
  const char *end;
  GData *attributes;
  GString *newmsg = (GString *)((void *)0);
  tmp = msg;
  while(purple_markup_find_tag("img",tmp,&start,&end,&attributes) != 0){
    int imgid = 0;
    char *idstr = (char *)((void *)0);
    if (newmsg == ((GString *)((void *)0))) 
      newmsg = g_string_new("");
/* copy any text before the img tag */
    if (tmp < start) 
      g_string_append_len(newmsg,tmp,(start - tmp));
    if ((idstr = (g_datalist_get_data(&attributes,"id"))) != ((char *)((void *)0))) 
      imgid = atoi(idstr);
    if (imgid != 0) {
      FILE *image_file;
      char *dir;
      PurpleStoredImage *image;
      gconstpointer image_data;
      char *new_filename = (char *)((void *)0);
      char *path = (char *)((void *)0);
      size_t image_byte_count;
      image = purple_imgstore_find_by_id(imgid);
      if (image == ((PurpleStoredImage *)((void *)0))) {
/* This should never happen. */
/* This *does* happen for failed Direct-IMs -DAA */
        g_string_free(newmsg,(!0));
        do {
          g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","log.c",816,((const char *)__func__));
          return (char *)msg;
        }while (0);
      }
      image_data = purple_imgstore_get_data(image);
      image_byte_count = purple_imgstore_get_size(image);
      dir = purple_log_get_log_dir((log -> type),(log -> name),(log -> account));
      new_filename = purple_util_get_image_filename(image_data,image_byte_count);
      path = g_build_filename(dir,new_filename,((void *)((void *)0)));
/* Only save unique files. */
      if (!(g_file_test(path,G_FILE_TEST_EXISTS) != 0)) {
        if ((image_file = fopen(path,"wb")) != ((FILE *)((void *)0))) {
          if (!(fwrite(image_data,image_byte_count,1,image_file) != 0UL)) {
            purple_debug_error("log","Error writing %s: %s\n",path,g_strerror( *__errno_location()));
            fclose(image_file);
/* Attempt to not leave half-written files around. */
            unlink(path);
          }
          else {
            purple_debug_info("log","Wrote image file: %s\n",path);
            fclose(image_file);
          }
        }
        else {
          purple_debug_error("log","Unable to create file %s: %s\n",path,g_strerror( *__errno_location()));
        }
      }
/* Write the new image tag */
      g_string_append_printf(newmsg,"<IMG SRC=\"%s\">",new_filename);
      g_free(new_filename);
      g_free(path);
    }
/* Continue from the end of the tag */
    tmp = (end + 1);
  }
  if (newmsg == ((GString *)((void *)0))) {
/* No images were found to change. */
    return (char *)msg;
  }
/* Append any remaining message data */
  g_string_append(newmsg,tmp);
  return g_string_free(newmsg,0);
}

void purple_log_common_writer(PurpleLog *log,const char *ext)
{
  PurpleLogCommonLoggerData *data = (log -> logger_data);
  if (data == ((PurpleLogCommonLoggerData *)((void *)0))) {
/* This log is new */
    char *dir;
    struct tm *tm;
    const char *tz;
    const char *date;
    char *filename;
    char *path;
    dir = purple_log_get_log_dir((log -> type),(log -> name),(log -> account));
    if (dir == ((char *)((void *)0))) 
      return ;
    purple_build_dir(dir,256 | 128 | 64);
    tm = localtime((&log -> time));
    tz = purple_escape_filename(purple_utf8_strftime("%Z",tm));
    date = purple_utf8_strftime("%Y-%m-%d.%H%M%S%z",tm);
    filename = g_strdup_printf("%s%s%s",date,tz,((ext != 0)?ext : ""));
    path = g_build_filename(dir,filename,((void *)((void *)0)));
    g_free(dir);
    g_free(filename);
    log -> logger_data = (data = ((PurpleLogCommonLoggerData *)(g_slice_alloc0((sizeof(PurpleLogCommonLoggerData ))))));
    data -> file = fopen(path,"a");
    if ((data -> file) == ((FILE *)((void *)0))) {
      purple_debug(PURPLE_DEBUG_ERROR,"log","Could not create log file %s\n",path);
      if ((log -> conv) != ((PurpleConversation *)((void *)0))) 
        purple_conversation_write((log -> conv),0,((const char *)(dgettext("pidgin","Logging of this conversation failed."))),PURPLE_MESSAGE_ERROR,time(0));
      g_free(path);
      return ;
    }
    g_free(path);
  }
}

GList *purple_log_common_lister(PurpleLogType type,const char *name,PurpleAccount *account,const char *ext,PurpleLogLogger *logger)
{
  GDir *dir;
  GList *list = (GList *)((void *)0);
  const char *filename;
  char *path;
  if (!(account != 0)) 
    return 0;
  path = purple_log_get_log_dir(type,name,account);
  if (path == ((char *)((void *)0))) 
    return 0;
  if (!((dir = g_dir_open(path,0,0)) != 0)) {
    g_free(path);
    return 0;
  }
  while((filename = g_dir_read_name(dir)) != 0){
    if ((purple_str_has_suffix(filename,ext) != 0) && (strlen(filename) >= (17 + strlen(ext)))) {
      PurpleLog *log;
      PurpleLogCommonLoggerData *data;
      struct tm tm;
#if defined (HAVE_TM_GMTOFF) && defined (HAVE_STRUCT_TM_TM_ZONE)
      long tz_off;
      const char *rest;
      const char *end;
      time_t stamp = purple_str_to_time(purple_unescape_filename(filename),0,&tm,&tz_off,&rest);
/* As zero is a valid offset, PURPLE_NO_TZ_OFF means no offset was
			 * provided. See util.h. Yes, it's kinda ugly. */
      if (tz_off != (-500000)) 
        tm.tm_gmtoff = (tz_off - tm.tm_gmtoff);
      if ((((stamp == 0) || (rest == ((const char *)((void *)0)))) || ((end = (strchr(rest,'.'))) == ((const char *)((void *)0)))) || (strchr(rest,32) != ((char *)((void *)0)))) {
        log = purple_log_new(type,name,account,0,stamp,0);
      }
      else {
        char *tmp = g_strndup(rest,(end - rest));
        tm.tm_zone = tmp;
        log = purple_log_new(type,name,account,0,stamp,(&tm));
        g_free(tmp);
      }
#else
#endif
      log -> logger = logger;
      log -> logger_data = (data = ((PurpleLogCommonLoggerData *)(g_slice_alloc0((sizeof(PurpleLogCommonLoggerData ))))));
      data -> path = g_build_filename(path,filename,((void *)((void *)0)));
      list = g_list_prepend(list,log);
    }
  }
  g_dir_close(dir);
  g_free(path);
  return list;
}

int purple_log_common_total_sizer(PurpleLogType type,const char *name,PurpleAccount *account,const char *ext)
{
  GDir *dir;
  int size = 0;
  const char *filename;
  char *path;
  if (!(account != 0)) 
    return 0;
  path = purple_log_get_log_dir(type,name,account);
  if (path == ((char *)((void *)0))) 
    return 0;
  if (!((dir = g_dir_open(path,0,0)) != 0)) {
    g_free(path);
    return 0;
  }
  while((filename = g_dir_read_name(dir)) != 0){
    if ((purple_str_has_suffix(filename,ext) != 0) && (strlen(filename) >= (17 + strlen(ext)))) {
      char *tmp = g_build_filename(path,filename,((void *)((void *)0)));
      struct stat st;
      if (stat(tmp,&st) != 0) {
        purple_debug_error("log","Error stating log file: %s\n",tmp);
        g_free(tmp);
        continue; 
      }
      g_free(tmp);
      size += st.st_size;
    }
  }
  g_dir_close(dir);
  g_free(path);
  return size;
}

int purple_log_common_sizer(PurpleLog *log)
{
  struct stat st;
  PurpleLogCommonLoggerData *data = (log -> logger_data);
  do {
    if (data != ((PurpleLogCommonLoggerData *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"data != NULL");
      return 0;
    };
  }while (0);
  if (!((data -> path) != 0) || (stat((data -> path),&st) != 0)) 
    st.st_size = 0;
  return st.st_size;
}
/* This will build log sets for all loggers that use the common logger
 * functions because they use the same directory structure. */

static void log_get_log_sets_common(GHashTable *sets)
{
  gchar *log_path = g_build_filename(purple_user_dir(),"logs",((void *)((void *)0)));
  GDir *log_dir = g_dir_open(log_path,0,0);
  const gchar *protocol;
  if (log_dir == ((GDir *)((void *)0))) {
    g_free(log_path);
    return ;
  }
  while((protocol = g_dir_read_name(log_dir)) != ((const gchar *)((void *)0))){{
      gchar *protocol_path = g_build_filename(log_path,protocol,((void *)((void *)0)));
      GDir *protocol_dir;
      const gchar *username;
      gchar *protocol_unescaped;
      GList *account_iter;
      GList *accounts = (GList *)((void *)0);
      if ((protocol_dir = g_dir_open(protocol_path,0,0)) == ((GDir *)((void *)0))) {
        g_free(protocol_path);
        continue; 
      }
/* Using g_strdup() to cover the one-in-a-million chance that a
		 * prpl's list_icon function uses purple_unescape_filename(). */
      protocol_unescaped = g_strdup(purple_unescape_filename(protocol));
/* Find all the accounts for protocol. */
      for (account_iter = purple_accounts_get_all(); account_iter != ((GList *)((void *)0)); account_iter = (account_iter -> next)) {{
          PurplePlugin *prpl;
          PurplePluginProtocolInfo *prpl_info;
          prpl = purple_find_prpl(purple_account_get_protocol_id(((PurpleAccount *)(account_iter -> data))));
          if (!(prpl != 0)) 
            continue; 
          prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
          if (purple_strequal(protocol_unescaped,( *(prpl_info -> list_icon))(((PurpleAccount *)(account_iter -> data)),0)) != 0) 
            accounts = g_list_prepend(accounts,(account_iter -> data));
        }
      }
      g_free(protocol_unescaped);
      while((username = g_dir_read_name(protocol_dir)) != ((const gchar *)((void *)0))){{
          gchar *username_path = g_build_filename(protocol_path,username,((void *)((void *)0)));
          GDir *username_dir;
          const gchar *username_unescaped;
          PurpleAccount *account = (PurpleAccount *)((void *)0);
          gchar *name;
          if ((username_dir = g_dir_open(username_path,0,0)) == ((GDir *)((void *)0))) {
            g_free(username_path);
            continue; 
          }
/* Find the account for username in the list of accounts for protocol. */
          username_unescaped = purple_unescape_filename(username);
{
            for (account_iter = g_list_first(accounts); account_iter != ((GList *)((void *)0)); account_iter = (account_iter -> next)) {
              if (purple_strequal(( *((PurpleAccount *)(account_iter -> data))).username,username_unescaped) != 0) {
                account = (account_iter -> data);
                break; 
              }
            }
          }
/* Don't worry about the cast, name will point to dynamically allocated memory shortly. */
          while((name = ((gchar *)(g_dir_read_name(username_dir)))) != ((gchar *)((void *)0))){
            size_t len;
            PurpleLogSet *set;
/* IMPORTANT: Always initialize all members of PurpleLogSet */
            set = ((PurpleLogSet *)(g_slice_alloc((sizeof(PurpleLogSet )))));
/* Unescape the filename. */
            name = g_strdup(purple_unescape_filename(name));
/* Get the (possibly new) length of name. */
            len = strlen(name);
            set -> type = PURPLE_LOG_IM;
            set -> name = name;
            set -> account = account;
/* set->buddy is always set below */
            set -> normalized_name = g_strdup(purple_normalize(account,name));
/* Check for .chat or .system at the end of the name to determine the type. */
            if (len >= 7) {
              gchar *tmp = (name + (len - 7));
              if (purple_strequal(tmp,".system") != 0) {
                set -> type = PURPLE_LOG_SYSTEM;
                 *tmp = 0;
              }
            }
            if (len > 5) {
              gchar *tmp = (name + (len - 5));
              if (purple_strequal(tmp,".chat") != 0) {
                set -> type = PURPLE_LOG_CHAT;
                 *tmp = 0;
              }
            }
/* Determine if this (account, name) combination exists as a buddy. */
            if ((account != ((PurpleAccount *)((void *)0))) && (( *name) != 0)) 
              set -> buddy = (purple_find_buddy(account,name) != ((PurpleBuddy *)((void *)0)));
            else 
              set -> buddy = 0;
            log_add_log_set_to_hash(sets,set);
          }
          g_free(username_path);
          g_dir_close(username_dir);
        }
      }
      g_free(protocol_path);
      g_list_free(accounts);
      g_dir_close(protocol_dir);
    }
  }
  g_free(log_path);
  g_dir_close(log_dir);
}

gboolean purple_log_common_deleter(PurpleLog *log)
{
  PurpleLogCommonLoggerData *data;
  int ret;
  do {
    if (log != ((PurpleLog *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"log != NULL");
      return 0;
    };
  }while (0);
  data = (log -> logger_data);
  if (data == ((PurpleLogCommonLoggerData *)((void *)0))) 
    return 0;
  if ((data -> path) == ((char *)((void *)0))) 
    return 0;
  ret = g_unlink((data -> path));
  if (ret == 0) 
    return (!0);
  else if (ret == -1) {
    purple_debug_error("log","Failed to delete: %s - %s\n",(data -> path),g_strerror( *__errno_location()));
  }
  else {
/* I'm not sure that g_unlink() will ever return
		 * something other than 0 or -1. -- rlaager */
    purple_debug_error("log","Failed to delete: %s\n",(data -> path));
  }
  return 0;
}

gboolean purple_log_common_is_deletable(PurpleLog *log)
{
  PurpleLogCommonLoggerData *data;
#ifndef _WIN32
  gchar *dirname;
#endif
  do {
    if (log != ((PurpleLog *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"log != NULL");
      return 0;
    };
  }while (0);
  data = (log -> logger_data);
  if (data == ((PurpleLogCommonLoggerData *)((void *)0))) 
    return 0;
  if ((data -> path) == ((char *)((void *)0))) 
    return 0;
#ifndef _WIN32
  dirname = g_path_get_dirname((data -> path));
  if (g_access(dirname,2) == 0) {
    g_free(dirname);
    return (!0);
  }
  purple_debug_info("log","access(%s) failed: %s\n",dirname,g_strerror( *__errno_location()));
  g_free(dirname);
#else
/* Unless and until someone writes equivalent win32 code,
	 * we'll assume the file is deletable. */
#endif
  return 0;
}

static char *process_txt_log(char *txt,char *to_free)
{
  char *tmp;
/* The to_free argument allows us to save a
	 * g_strdup() in some cases. */
  if (to_free == ((char *)((void *)0))) 
    to_free = txt;
/* g_markup_escape_text requires valid UTF-8 */
  if (!(g_utf8_validate(txt,(-1),0) != 0)) {
    tmp = purple_utf8_salvage(txt);
    g_free(to_free);
    to_free = (txt = tmp);
  }
  tmp = g_markup_escape_text(txt,(-1));
  g_free(to_free);
  txt = purple_markup_linkify(tmp);
  g_free(tmp);
  return txt;
}
#if 0 /* Maybe some other time. */
/****************
 ** XML LOGGER **
 ****************/
/* This log is new.  We could use the loggers 'new' function, but
		 * creating a new file there would result in empty files in the case
		 * that you open a convo with someone, but don't say anything.
		 */
/* if we can't write to the file, give up before we hurt ourselves */
#endif
/****************************
 ** HTML LOGGER *************
 ****************************/

static gsize html_logger_write(PurpleLog *log,PurpleMessageFlags type,const char *from,time_t time,const char *message)
{
  char *msg_fixed;
  char *image_corrected_msg;
  char *date;
  char *header;
  char *escaped_from;
  PurplePlugin *plugin = purple_find_prpl(purple_account_get_protocol_id((log -> account)));
  PurpleLogCommonLoggerData *data = (log -> logger_data);
  gsize written = 0;
  if (!(data != 0)) {
    const char *prpl = ( *( *((PurplePluginProtocolInfo *)( *(plugin -> info)).extra_info)).list_icon)((log -> account),0);
    const char *date;
    purple_log_common_writer(log,".html");
    data = (log -> logger_data);
/* if we can't write to the file, give up before we hurt ourselves */
    if (!((data -> file) != 0)) 
      return 0;
    date = purple_date_format_full((localtime((&log -> time))));
    written += (fprintf((data -> file),"<html><head>"));
    written += (fprintf((data -> file),"<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">"));
    written += (fprintf((data -> file),"<title>"));
    if ((log -> type) == PURPLE_LOG_SYSTEM) 
      header = g_strdup_printf("System log for account %s (%s) connected at %s",purple_account_get_username((log -> account)),prpl,date);
    else 
      header = g_strdup_printf("Conversation with %s at %s on %s (%s)",(log -> name),date,purple_account_get_username((log -> account)),prpl);
    written += (fprintf((data -> file),"%s",header));
    written += (fprintf((data -> file),"</title></head><body>"));
    written += (fprintf((data -> file),"<h3>%s</h3>\n",header));
    g_free(header);
  }
/* if we can't write to the file, give up before we hurt ourselves */
  if (!((data -> file) != 0)) 
    return 0;
  escaped_from = g_markup_escape_text(from,(-1));
  image_corrected_msg = convert_image_tags(log,message);
  purple_markup_html_to_xhtml(image_corrected_msg,&msg_fixed,0);
/* Yes, this breaks encapsulation.  But it's a static function and
	 * this saves a needless strdup(). */
  if (image_corrected_msg != message) 
    g_free(image_corrected_msg);
  date = log_get_timestamp(log,time);
  if ((log -> type) == PURPLE_LOG_SYSTEM) {
    written += (fprintf((data -> file),"---- %s @ %s ----<br/>\n",msg_fixed,date));
  }
  else {
    if ((type & PURPLE_MESSAGE_SYSTEM) != 0U) 
      written += (fprintf((data -> file),"<font size=\"2\">(%s)</font><b> %s</b><br/>\n",date,msg_fixed));
    else if ((type & PURPLE_MESSAGE_RAW) != 0U) 
      written += (fprintf((data -> file),"<font size=\"2\">(%s)</font> %s<br/>\n",date,msg_fixed));
    else if ((type & PURPLE_MESSAGE_ERROR) != 0U) 
      written += (fprintf((data -> file),"<font color=\"#FF0000\"><font size=\"2\">(%s)</font><b> %s</b></font><br/>\n",date,msg_fixed));
    else if ((type & PURPLE_MESSAGE_WHISPER) != 0U) 
      written += (fprintf((data -> file),"<font color=\"#6C2585\"><font size=\"2\">(%s)</font><b> %s:</b></font> %s<br/>\n",date,escaped_from,msg_fixed));
    else if ((type & PURPLE_MESSAGE_AUTO_RESP) != 0U) {
      if ((type & PURPLE_MESSAGE_SEND) != 0U) 
        written += (fprintf((data -> file),((const char *)(dgettext("pidgin","<font color=\"#16569E\"><font size=\"2\">(%s)</font> <b>%s &lt;AUTO-REPLY&gt;:</b></font> %s<br/>\n"))),date,escaped_from,msg_fixed));
      else if ((type & PURPLE_MESSAGE_RECV) != 0U) 
        written += (fprintf((data -> file),((const char *)(dgettext("pidgin","<font color=\"#A82F2F\"><font size=\"2\">(%s)</font> <b>%s &lt;AUTO-REPLY&gt;:</b></font> %s<br/>\n"))),date,escaped_from,msg_fixed));
    }
    else if ((type & PURPLE_MESSAGE_RECV) != 0U) {
      if (purple_message_meify(msg_fixed,(-1)) != 0) 
        written += (fprintf((data -> file),"<font color=\"#062585\"><font size=\"2\">(%s)</font> <b>***%s</b></font> %s<br/>\n",date,escaped_from,msg_fixed));
      else 
        written += (fprintf((data -> file),"<font color=\"#A82F2F\"><font size=\"2\">(%s)</font> <b>%s:</b></font> %s<br/>\n",date,escaped_from,msg_fixed));
    }
    else if ((type & PURPLE_MESSAGE_SEND) != 0U) {
      if (purple_message_meify(msg_fixed,(-1)) != 0) 
        written += (fprintf((data -> file),"<font color=\"#062585\"><font size=\"2\">(%s)</font> <b>***%s</b></font> %s<br/>\n",date,escaped_from,msg_fixed));
      else 
        written += (fprintf((data -> file),"<font color=\"#16569E\"><font size=\"2\">(%s)</font> <b>%s:</b></font> %s<br/>\n",date,escaped_from,msg_fixed));
    }
    else {
      purple_debug_error("log","Unhandled message type.\n");
      written += (fprintf((data -> file),"<font size=\"2\">(%s)</font><b> %s:</b></font> %s<br/>\n",date,escaped_from,msg_fixed));
    }
  }
  g_free(date);
  g_free(msg_fixed);
  g_free(escaped_from);
  fflush((data -> file));
  return written;
}

static void html_logger_finalize(PurpleLog *log)
{
  PurpleLogCommonLoggerData *data = (log -> logger_data);
  if (data != 0) {
    if ((data -> file) != 0) {
      fprintf((data -> file),"</body></html>\n");
      fclose((data -> file));
    }
    g_free((data -> path));
    do {
      if (1) 
        g_slice_free1((sizeof(PurpleLogCommonLoggerData )),data);
      else 
        ((PurpleLogCommonLoggerData *)((PurpleLogCommonLoggerData *)0)) == data;
    }while (0);
  }
}

static GList *html_logger_list(PurpleLogType type,const char *sn,PurpleAccount *account)
{
  return purple_log_common_lister(type,sn,account,".html",html_logger);
}

static GList *html_logger_list_syslog(PurpleAccount *account)
{
  return purple_log_common_lister(PURPLE_LOG_SYSTEM,".system",account,".html",html_logger);
}

static char *html_logger_read(PurpleLog *log,PurpleLogReadFlags *flags)
{
  char *read;
  PurpleLogCommonLoggerData *data = (log -> logger_data);
   *flags = PURPLE_LOG_READ_NO_NEWLINE;
  if (!(data != 0) || !((data -> path) != 0)) 
    return g_strdup(((const char *)(dgettext("pidgin","<font color=\"red\"><b>Unable to find log path!</b></font>"))));
  if (g_file_get_contents((data -> path),&read,0,0) != 0) {
    char *minus_header = strchr(read,10);
    if (!(minus_header != 0)) 
      return read;
    minus_header = g_strdup((minus_header + 1));
    g_free(read);
    return minus_header;
  }
  return g_strdup_printf(((const char *)(dgettext("pidgin","<font color=\"red\"><b>Could not read file: %s</b></font>"))),(data -> path));
}

static int html_logger_total_size(PurpleLogType type,const char *name,PurpleAccount *account)
{
  return purple_log_common_total_sizer(type,name,account,".html");
}
/****************************
 ** PLAIN TEXT LOGGER *******
 ****************************/

static gsize txt_logger_write(PurpleLog *log,PurpleMessageFlags type,const char *from,time_t time,const char *message)
{
  char *date;
  PurplePlugin *plugin = purple_find_prpl(purple_account_get_protocol_id((log -> account)));
  PurpleLogCommonLoggerData *data = (log -> logger_data);
  char *stripped = (char *)((void *)0);
  gsize written = 0;
  if (data == ((PurpleLogCommonLoggerData *)((void *)0))) {
/* This log is new.  We could use the loggers 'new' function, but
		 * creating a new file there would result in empty files in the case
		 * that you open a convo with someone, but don't say anything.
		 */
    const char *prpl = ( *( *((PurplePluginProtocolInfo *)( *(plugin -> info)).extra_info)).list_icon)((log -> account),0);
    purple_log_common_writer(log,".txt");
    data = (log -> logger_data);
/* if we can't write to the file, give up before we hurt ourselves */
    if (!((data -> file) != 0)) 
      return 0;
    if ((log -> type) == PURPLE_LOG_SYSTEM) 
      written += (fprintf((data -> file),"System log for account %s (%s) connected at %s\n",purple_account_get_username((log -> account)),prpl,purple_date_format_full((localtime((&log -> time))))));
    else 
      written += (fprintf((data -> file),"Conversation with %s at %s on %s (%s)\n",(log -> name),purple_date_format_full((localtime((&log -> time)))),purple_account_get_username((log -> account)),prpl));
  }
/* if we can't write to the file, give up before we hurt ourselves */
  if (!((data -> file) != 0)) 
    return 0;
  stripped = purple_markup_strip_html(message);
  date = log_get_timestamp(log,time);
  if ((log -> type) == PURPLE_LOG_SYSTEM) {
    written += (fprintf((data -> file),"---- %s @ %s ----\n",stripped,date));
  }
  else {
    if (((type & PURPLE_MESSAGE_SEND) != 0U) || ((type & PURPLE_MESSAGE_RECV) != 0U)) {
      if ((type & PURPLE_MESSAGE_AUTO_RESP) != 0U) {
        written += (fprintf((data -> file),((const char *)(dgettext("pidgin","(%s) %s <AUTO-REPLY>: %s\n"))),date,from,stripped));
      }
      else {
        if (purple_message_meify(stripped,(-1)) != 0) 
          written += (fprintf((data -> file),"(%s) ***%s %s\n",date,from,stripped));
        else 
          written += (fprintf((data -> file),"(%s) %s: %s\n",date,from,stripped));
      }
    }
    else if ((((type & PURPLE_MESSAGE_SYSTEM) != 0U) || ((type & PURPLE_MESSAGE_ERROR) != 0U)) || ((type & PURPLE_MESSAGE_RAW) != 0U)) 
      written += (fprintf((data -> file),"(%s) %s\n",date,stripped));
    else if ((type & PURPLE_MESSAGE_NO_LOG) != 0U) {
/* This shouldn't happen */
      g_free(stripped);
      return written;
    }
    else if ((type & PURPLE_MESSAGE_WHISPER) != 0U) 
      written += (fprintf((data -> file),"(%s) *%s* %s",date,from,stripped));
    else 
      written += (fprintf((data -> file),"(%s) %s%s %s\n",date,((from != 0)?from : ""),((from != 0)?":" : ""),stripped));
  }
  g_free(date);
  g_free(stripped);
  fflush((data -> file));
  return written;
}

static void txt_logger_finalize(PurpleLog *log)
{
  PurpleLogCommonLoggerData *data = (log -> logger_data);
  if (data != 0) {
    if ((data -> file) != 0) 
      fclose((data -> file));
    g_free((data -> path));
    do {
      if (1) 
        g_slice_free1((sizeof(PurpleLogCommonLoggerData )),data);
      else 
        ((PurpleLogCommonLoggerData *)((PurpleLogCommonLoggerData *)0)) == data;
    }while (0);
  }
}

static GList *txt_logger_list(PurpleLogType type,const char *sn,PurpleAccount *account)
{
  return purple_log_common_lister(type,sn,account,".txt",txt_logger);
}

static GList *txt_logger_list_syslog(PurpleAccount *account)
{
  return purple_log_common_lister(PURPLE_LOG_SYSTEM,".system",account,".txt",txt_logger);
}

static char *txt_logger_read(PurpleLog *log,PurpleLogReadFlags *flags)
{
  char *read;
  char *minus_header;
  PurpleLogCommonLoggerData *data = (log -> logger_data);
   *flags = 0;
  if (!(data != 0) || !((data -> path) != 0)) 
    return g_strdup(((const char *)(dgettext("pidgin","<font color=\"red\"><b>Unable to find log path!</b></font>"))));
  if (g_file_get_contents((data -> path),&read,0,0) != 0) {
    minus_header = strchr(read,10);
    if (minus_header != 0) 
      return process_txt_log((minus_header + 1),read);
    else 
      return process_txt_log(read,0);
  }
  return g_strdup_printf(((const char *)(dgettext("pidgin","<font color=\"red\"><b>Could not read file: %s</b></font>"))),(data -> path));
}

static int txt_logger_total_size(PurpleLogType type,const char *name,PurpleAccount *account)
{
  return purple_log_common_total_sizer(type,name,account,".txt");
}
/****************
 * OLD LOGGER ***
 ****************/
/* The old logger doesn't write logs, only reads them.  This is to include
 * old logs in the log viewer transparently.
 */

struct old_logger_data 
{
  PurpleStringref *pathref;
  int offset;
  int length;
}
;

static GList *old_logger_list(PurpleLogType type,const char *sn,PurpleAccount *account)
{
  char *logfile = g_strdup_printf("%s.log",purple_normalize(account,sn));
  char *pathstr = g_build_filename(purple_user_dir(),"logs",logfile,((void *)((void *)0)));
  PurpleStringref *pathref = purple_stringref_new(pathstr);
  struct stat st;
  time_t log_last_modified;
  FILE *index;
  FILE *file;
  int index_fd;
  char *index_tmp;
  char buf[4096UL];
  struct tm tm;
  char month[4UL];
  struct old_logger_data *data = (struct old_logger_data *)((void *)0);
  int logfound = 0;
  int lastoff = 0;
  int newlen;
  time_t lasttime = 0;
  PurpleLog *log = (PurpleLog *)((void *)0);
  GList *list = (GList *)((void *)0);
  g_free(logfile);
  if (stat(purple_stringref_value(pathref),&st) != 0) {
    purple_stringref_unref(pathref);
    g_free(pathstr);
    return 0;
  }
  else 
    log_last_modified = st.st_mtim.tv_sec;
/* Change the .log extension to .idx */
  strcpy(((pathstr + strlen(pathstr)) - 3),"idx");
  if (stat(pathstr,&st) == 0) {
    if (st.st_mtim.tv_sec < log_last_modified) {
      purple_debug_warning("log","Index \"%s\" exists, but is older than the log.\n",pathstr);
    }
    else {
/* The index file exists and is at least as new as the log, so open it. */
      if (!((index = fopen(pathstr,"rb")) != 0)) {
        purple_debug_error("log","Failed to open index file \"%s\" for reading: %s\n",pathstr,g_strerror( *__errno_location()));
/* Fall through so that we'll parse the log file. */
      }
      else {
        purple_debug_info("log","Using index: %s\n",pathstr);
        g_free(pathstr);
        while(fgets(buf,2048 * 2,index) != 0){
          unsigned long idx_time;
          if (sscanf(buf,"%d\t%d\t%lu",&lastoff,&newlen,&idx_time) == 3) {
            log = purple_log_new(PURPLE_LOG_IM,sn,account,0,(-1),0);
            log -> logger = old_logger;
            log -> time = ((time_t )idx_time);
/* IMPORTANT: Always set all members of struct old_logger_data */
            data = ((struct old_logger_data *)(g_slice_alloc((sizeof(struct old_logger_data )))));
            data -> pathref = purple_stringref_ref(pathref);
            data -> offset = lastoff;
            data -> length = newlen;
            log -> logger_data = data;
            list = g_list_prepend(list,log);
          }
        }
        fclose(index);
        purple_stringref_unref(pathref);
        return list;
      }
    }
  }
  if (!((file = fopen(purple_stringref_value(pathref),"rb")) != 0)) {
    purple_debug_error("log","Failed to open log file \"%s\" for reading: %s\n",purple_stringref_value(pathref),g_strerror( *__errno_location()));
    purple_stringref_unref(pathref);
    g_free(pathstr);
    return 0;
  }
  index_tmp = g_strdup_printf("%s.XXXXXX",pathstr);
  if ((index_fd = g_mkstemp(index_tmp)) == -1) {
    purple_debug_error("log","Failed to open index temp file: %s\n",g_strerror( *__errno_location()));
    g_free(pathstr);
    g_free(index_tmp);
    index = ((FILE *)((void *)0));
  }
  else {
    if ((index = fdopen(index_fd,"wb")) == ((FILE *)((void *)0))) {
      purple_debug_error("log","Failed to fdopen() index temp file: %s\n",g_strerror( *__errno_location()));
      close(index_fd);
      if (index_tmp != ((char *)((void *)0))) {
        g_unlink(index_tmp);
        g_free(index_tmp);
      }
      g_free(pathstr);
    }
  }
  while(fgets(buf,2048 * 2,file) != 0){
    if (strstr(buf,"---- New C") != ((char *)((void *)0))) {
      int length;
      int offset;
      char convostart[32UL];
      char *temp = strchr(buf,64);
      if ((temp == ((char *)((void *)0))) || (strlen(temp) < 2)) 
        continue; 
      temp++;
      length = (strcspn(temp,"-"));
      if (length > 31) 
        length = 31;
      offset = (ftell(file));
      if (logfound != 0) {
        newlen = ((offset - lastoff) - length);
        if (strstr(buf,"----</H3><BR>") != 0) {
          newlen -= sizeof(( *((char (*)[51UL])"<HR><BR><H3 Align=Center> ---- New Conversation @ "))) + sizeof(( *((char (*)[14UL])"----</H3><BR>"))) - 2;
        }
        else {
          newlen -= sizeof(( *((char (*)[25UL])"---- New Conversation @ "))) + sizeof(( *((char (*)[5UL])"----"))) - 2;
        }
        if (strchr(buf,13) != 0) 
          newlen--;
        if (newlen != 0) {
          log = purple_log_new(PURPLE_LOG_IM,sn,account,0,(-1),0);
          log -> logger = old_logger;
          log -> time = lasttime;
/* IMPORTANT: Always set all members of struct old_logger_data */
          data = ((struct old_logger_data *)(g_slice_alloc((sizeof(struct old_logger_data )))));
          data -> pathref = purple_stringref_ref(pathref);
          data -> offset = lastoff;
          data -> length = newlen;
          log -> logger_data = data;
          list = g_list_prepend(list,log);
          if (index != ((FILE *)((void *)0))) 
            fprintf(index,"%d\t%d\t%lu\n",(data -> offset),(data -> length),((unsigned long )(log -> time)));
        }
      }
      logfound = 1;
      lastoff = offset;
      g_snprintf(convostart,length,"%s",temp);
      memset((&tm),0,(sizeof(tm)));
      sscanf(convostart,"%*s %3s %d %d:%d:%d %d",month,&tm.tm_mday,&tm.tm_hour,&tm.tm_min,&tm.tm_sec,&tm.tm_year);
/* Ugly hack, in case current locale is not English */
      if (purple_strequal(month,"Jan") != 0) {
        tm.tm_mon = 0;
      }
      else if (purple_strequal(month,"Feb") != 0) {
        tm.tm_mon = 1;
      }
      else if (purple_strequal(month,"Mar") != 0) {
        tm.tm_mon = 2;
      }
      else if (purple_strequal(month,"Apr") != 0) {
        tm.tm_mon = 3;
      }
      else if (purple_strequal(month,"May") != 0) {
        tm.tm_mon = 4;
      }
      else if (purple_strequal(month,"Jun") != 0) {
        tm.tm_mon = 5;
      }
      else if (purple_strequal(month,"Jul") != 0) {
        tm.tm_mon = 6;
      }
      else if (purple_strequal(month,"Aug") != 0) {
        tm.tm_mon = 7;
      }
      else if (purple_strequal(month,"Sep") != 0) {
        tm.tm_mon = 8;
      }
      else if (purple_strequal(month,"Oct") != 0) {
        tm.tm_mon = 9;
      }
      else if (purple_strequal(month,"Nov") != 0) {
        tm.tm_mon = 10;
      }
      else if (purple_strequal(month,"Dec") != 0) {
        tm.tm_mon = 11;
      }
      tm.tm_year -= 1900;
      lasttime = mktime(&tm);
    }
  }
  if (logfound != 0) {
    if ((newlen = (ftell(file) - lastoff)) != 0) {
      log = purple_log_new(PURPLE_LOG_IM,sn,account,0,(-1),0);
      log -> logger = old_logger;
      log -> time = lasttime;
/* IMPORTANT: Always set all members of struct old_logger_data */
      data = ((struct old_logger_data *)(g_slice_alloc((sizeof(struct old_logger_data )))));
      data -> pathref = purple_stringref_ref(pathref);
      data -> offset = lastoff;
      data -> length = newlen;
      log -> logger_data = data;
      list = g_list_prepend(list,log);
      if (index != ((FILE *)((void *)0))) 
        fprintf(index,"%d\t%d\t%lu\n",(data -> offset),(data -> length),((unsigned long )(log -> time)));
    }
  }
  purple_stringref_unref(pathref);
  fclose(file);
  if (index != ((FILE *)((void *)0))) {
    fclose(index);
    if (index_tmp == ((char *)((void *)0))) {
      g_free(pathstr);
      do {
        g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","log.c",1903,((const char *)__func__));
        return list;
      }while (0);
    }
    if (rename(index_tmp,pathstr) != 0) {
      purple_debug_warning("log","Failed to rename index temp file \"%s\" to \"%s\": %s\n",index_tmp,pathstr,g_strerror( *__errno_location()));
      g_unlink(index_tmp);
    }
    else 
      purple_debug_info("log","Built index: %s\n",pathstr);
    g_free(index_tmp);
    g_free(pathstr);
  }
  return list;
}

static int old_logger_total_size(PurpleLogType type,const char *name,PurpleAccount *account)
{
  char *logfile = g_strdup_printf("%s.log",purple_normalize(account,name));
  char *pathstr = g_build_filename(purple_user_dir(),"logs",logfile,((void *)((void *)0)));
  int size;
  struct stat st;
  if (stat(pathstr,&st) != 0) 
    size = 0;
  else 
    size = st.st_size;
  g_free(logfile);
  g_free(pathstr);
  return size;
}

static char *old_logger_read(PurpleLog *log,PurpleLogReadFlags *flags)
{
  size_t result;
  struct old_logger_data *data = (log -> logger_data);
  const char *path = purple_stringref_value((data -> pathref));
  FILE *file = fopen(path,"rb");
  char *read = (g_malloc(((data -> length) + 1)));
  fseek(file,(data -> offset),0);
  result = fread(read,(data -> length),1,file);
  if (result != 1) 
    purple_debug_error("log","Unable to read from log file: %s\n",path);
  fclose(file);
  read[data -> length] = 0;
   *flags = 0;
  if (strstr(read,"<BR>") != 0) {
     *flags |= PURPLE_LOG_READ_NO_NEWLINE;
    return read;
  }
  return process_txt_log(read,0);
}

static int old_logger_size(PurpleLog *log)
{
  struct old_logger_data *data = (log -> logger_data);
  return (data != 0)?(data -> length) : 0;
}

static void old_logger_get_log_sets(PurpleLogSetCallback cb,GHashTable *sets)
{
  char *log_path = g_build_filename(purple_user_dir(),"logs",((void *)((void *)0)));
  GDir *log_dir = g_dir_open(log_path,0,0);
  gchar *name;
  PurpleBlistNode *gnode;
  PurpleBlistNode *cnode;
  PurpleBlistNode *bnode;
  g_free(log_path);
  if (log_dir == ((GDir *)((void *)0))) 
    return ;
/* Don't worry about the cast, name will be filled with a dynamically allocated data shortly. */
  while((name = ((gchar *)(g_dir_read_name(log_dir)))) != ((gchar *)((void *)0))){{
      size_t len;
      gchar *ext;
      PurpleLogSet *set;
      gboolean found = 0;
/* Unescape the filename. */
      name = g_strdup(purple_unescape_filename(name));
/* Get the (possibly new) length of name. */
      len = strlen(name);
      if (len < 5) {
        g_free(name);
        continue; 
      }
/* Make sure we're dealing with a log file. */
      ext = (name + (len - 4));
      if (!(purple_strequal(ext,".log") != 0)) {
        g_free(name);
        continue; 
      }
/* IMPORTANT: Always set all members of PurpleLogSet */
      set = ((PurpleLogSet *)(g_slice_alloc((sizeof(PurpleLogSet )))));
/* Chat for .chat at the end of the name to determine the type. */
       *ext = 0;
      set -> type = PURPLE_LOG_IM;
      if (len > 9) {
        char *tmp = (name + (len - 9));
        if (purple_strequal(tmp,".chat") != 0) {
          set -> type = PURPLE_LOG_CHAT;
           *tmp = 0;
        }
      }
      set -> name = (set -> normalized_name = name);
/* Search the buddy list to find the account and to determine if this is a buddy. */
      for (gnode = purple_blist_get_root(); !(found != 0) && (gnode != ((PurpleBlistNode *)((void *)0))); gnode = purple_blist_node_get_sibling_next(gnode)) {
        if (!((purple_blist_node_get_type(gnode)) == PURPLE_BLIST_GROUP_NODE)) 
          continue; 
        for (cnode = purple_blist_node_get_first_child(gnode); !(found != 0) && (cnode != ((PurpleBlistNode *)((void *)0))); cnode = purple_blist_node_get_sibling_next(cnode)) {
          if (!((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CONTACT_NODE)) 
            continue; 
          for (bnode = purple_blist_node_get_first_child(cnode); !(found != 0) && (bnode != ((PurpleBlistNode *)((void *)0))); bnode = purple_blist_node_get_sibling_next(bnode)) {
            PurpleBuddy *buddy = (PurpleBuddy *)bnode;
            if (purple_strequal(purple_buddy_get_name(buddy),name) != 0) {
              set -> account = purple_buddy_get_account(buddy);
              set -> buddy = (!0);
              found = (!0);
            }
          }
        }
      }
      if (!(found != 0)) {
        set -> account = ((PurpleAccount *)((void *)0));
        set -> buddy = 0;
      }
      ( *cb)(sets,set);
    }
  }
  g_dir_close(log_dir);
}

static void old_logger_finalize(PurpleLog *log)
{
  struct old_logger_data *data = (log -> logger_data);
  purple_stringref_unref((data -> pathref));
  do {
    if (1) 
      g_slice_free1((sizeof(struct old_logger_data )),data);
    else 
      ((struct old_logger_data *)((struct old_logger_data *)0)) == data;
  }while (0);
}
