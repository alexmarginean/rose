#include <string.h>
#include <glib.h>
#include "dbus-useful.h"
#include "conversation.h"
#include "util.h"

PurpleAccount *purple_accounts_find_ext(const char *name,const char *protocol_id,gboolean (*account_test)(const PurpleAccount *))
{
  PurpleAccount *result = (PurpleAccount *)((void *)0);
  GList *l;
  char *who;
  if (name != 0) 
    who = g_strdup(purple_normalize(0,name));
  else 
    who = ((char *)((void *)0));
{
    for (l = purple_accounts_get_all(); l != ((GList *)((void *)0)); l = (l -> next)) {{
        PurpleAccount *account = (PurpleAccount *)(l -> data);
        if ((who != 0) && (strcmp(purple_normalize(0,purple_account_get_username(account)),who) != 0)) 
          continue; 
        if ((protocol_id != 0) && (strcmp((account -> protocol_id),protocol_id) != 0)) 
          continue; 
        if ((account_test != 0) && !(( *account_test)(account) != 0)) 
          continue; 
        result = account;
        break; 
      }
    }
  }
  g_free(who);
  return result;
}

PurpleAccount *purple_accounts_find_any(const char *name,const char *protocol)
{
  return purple_accounts_find_ext(name,protocol,0);
}

PurpleAccount *purple_accounts_find_connected(const char *name,const char *protocol)
{
  return purple_accounts_find_ext(name,protocol,purple_account_is_connected);
}
