/**
 * @file purple-desktop-item.c Functions for managing .desktop files
 * @ingroup core
 */
/* Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
/*
 * The following code has been adapted from gnome-desktop-item.[ch],
 * as found on gnome-desktop-2.8.1.
 *
 *   Copyright (C) 2004 by Alceste Scalas <alceste.scalas@gmx.net>.
 *
 * Original copyright notice:
 *
 * Copyright (C) 1999, 2000 Red Hat Inc.
 * Copyright (C) 2001 Sid Vicious
 * All rights reserved.
 *
 * This file is part of the Gnome Library.
 *
 * The Gnome Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The Gnome Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02111-1301, USA.
 */
#include "internal.h"
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "desktopitem.h"

struct _PurpleDesktopItem 
{
  int refcount;
/* all languages used */
  GList *languages;
  PurpleDesktopItemType type;
/* `modified' means that the ditem has been
	 * modified since the last save. */
  gboolean modified;
/* Keys of the main section only */
  GList *keys;
  GList *sections;
/* This includes ALL keys, including
	 * other sections, separated by '/' */
  GHashTable *main_hash;
  char *location;
  time_t mtime;
}
;
typedef struct __unnamed_class___F0_L89_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__name__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GList_GList__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__keys {
char *name;
GList *keys;}Section;
typedef enum __unnamed_enum___F0_L94_C9_ENCODING_UNKNOWN__COMMA__ENCODING_UTF8__COMMA__ENCODING_LEGACY_MIXED {ENCODING_UNKNOWN,ENCODING_UTF8,ENCODING_LEGACY_MIXED}Encoding;
/**************************************************************************
 * Private utility functions
 **************************************************************************/

static PurpleDesktopItemType type_from_string(const char *type)
{
  if (!(type != 0)) 
    return PURPLE_DESKTOP_ITEM_TYPE_NULL;
  switch(type[0]){
    case 'A':
{
      if (purple_strequal(type,"Application") != 0) 
        return PURPLE_DESKTOP_ITEM_TYPE_APPLICATION;
      break; 
    }
    case 'L':
{
      if (purple_strequal(type,"Link") != 0) 
        return PURPLE_DESKTOP_ITEM_TYPE_LINK;
      break; 
    }
    case 'F':
{
      if (purple_strequal(type,"FSDevice") != 0) 
        return PURPLE_DESKTOP_ITEM_TYPE_FSDEVICE;
      break; 
    }
    case 'M':
{
      if (purple_strequal(type,"MimeType") != 0) 
        return PURPLE_DESKTOP_ITEM_TYPE_MIME_TYPE;
      break; 
    }
    case 'D':
{
      if (purple_strequal(type,"Directory") != 0) 
        return PURPLE_DESKTOP_ITEM_TYPE_DIRECTORY;
      break; 
    }
    case 'S':
{
      if (purple_strequal(type,"Service") != 0) 
        return PURPLE_DESKTOP_ITEM_TYPE_SERVICE;
      else if (purple_strequal(type,"ServiceType") != 0) 
        return PURPLE_DESKTOP_ITEM_TYPE_SERVICE_TYPE;
      break; 
    }
    default:
{
      break; 
    }
  }
  return PURPLE_DESKTOP_ITEM_TYPE_OTHER;
}

static Section *find_section(PurpleDesktopItem *item,const char *section)
{
  GList *li;
  Section *sec;
  if (section == ((const char *)((void *)0))) 
    return 0;
  if (purple_strequal(section,"Desktop Entry") != 0) 
    return 0;
  for (li = (item -> sections); li != ((GList *)((void *)0)); li = (li -> next)) {
    sec = (li -> data);
    if (purple_strequal((sec -> name),section) != 0) 
      return sec;
  }
  sec = ((Section *)(g_malloc0_n(1,(sizeof(Section )))));
  sec -> name = g_strdup(section);
  sec -> keys = ((GList *)((void *)0));
  item -> sections = g_list_append((item -> sections),sec);
/* Don't mark the item modified, this is just an empty section,
	 * it won't be saved even */
  return sec;
}

static Section *section_from_key(PurpleDesktopItem *item,const char *key)
{
  char *p;
  char *name;
  Section *sec;
  if (key == ((const char *)((void *)0))) 
    return 0;
  p = strchr(key,'/');
  if (p == ((char *)((void *)0))) 
    return 0;
  name = g_strndup(key,(p - key));
  sec = find_section(item,name);
  g_free(name);
  return sec;
}

static const char *key_basename(const char *key)
{
  char *p = strrchr(key,'/');
  if (p != ((char *)((void *)0))) 
    return (p + 1);
  else 
    return key;
}

static void set(PurpleDesktopItem *item,const char *key,const char *value)
{
  Section *sec = section_from_key(item,key);
  if (sec != ((Section *)((void *)0))) {
    if (value != ((const char *)((void *)0))) {
      if (g_hash_table_lookup((item -> main_hash),key) == ((void *)((void *)0))) 
        sec -> keys = g_list_append((sec -> keys),(g_strdup(key_basename(key))));
      g_hash_table_replace((item -> main_hash),(g_strdup(key)),(g_strdup(value)));
    }
    else {
      GList *list = g_list_find_custom((sec -> keys),(key_basename(key)),((GCompareFunc )strcmp));
      if (list != ((GList *)((void *)0))) {
        g_free((list -> data));
        sec -> keys = g_list_delete_link((sec -> keys),list);
      }
      g_hash_table_remove((item -> main_hash),key);
    }
  }
  else {
    if (value != ((const char *)((void *)0))) {
      if (g_hash_table_lookup((item -> main_hash),key) == ((void *)((void *)0))) 
        item -> keys = g_list_append((item -> keys),(g_strdup(key)));
      g_hash_table_replace((item -> main_hash),(g_strdup(key)),(g_strdup(value)));
    }
    else {
      GList *list = g_list_find_custom((item -> keys),key,((GCompareFunc )strcmp));
      if (list != ((GList *)((void *)0))) {
        g_free((list -> data));
        item -> keys = g_list_delete_link((item -> keys),list);
      }
      g_hash_table_remove((item -> main_hash),key);
    }
  }
  item -> modified = (!0);
}

static void _purple_desktop_item_set_string(PurpleDesktopItem *item,const char *attr,const char *value)
{
  do {
    if (item != ((PurpleDesktopItem *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"item != NULL");
      return ;
    };
  }while (0);
  do {
    if ((item -> refcount) > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"item->refcount > 0");
      return ;
    };
  }while (0);
  do {
    if (attr != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"attr != NULL");
      return ;
    };
  }while (0);
  set(item,attr,value);
  if (purple_strequal(attr,"Type") != 0) 
    item -> type = type_from_string(value);
}

static PurpleDesktopItem *_purple_desktop_item_new()
{
  PurpleDesktopItem *retval;
  retval = ((PurpleDesktopItem *)(g_malloc0_n(1,(sizeof(PurpleDesktopItem )))));
  retval -> refcount++;
  retval -> main_hash = g_hash_table_new_full(g_str_hash,g_str_equal,((GDestroyNotify )g_free),((GDestroyNotify )g_free));
/* These are guaranteed to be set */
  _purple_desktop_item_set_string(retval,"Name",((const char *)(dgettext("pidgin","No name"))));
  _purple_desktop_item_set_string(retval,"Encoding","UTF-8");
  _purple_desktop_item_set_string(retval,"Version","1.0");
  return retval;
}

static gpointer _purple_desktop_item_copy(gpointer boxed)
{
  return (purple_desktop_item_copy(boxed));
}

static void _purple_desktop_item_free(gpointer boxed)
{
  purple_desktop_item_unref(boxed);
}
/* Note, does not include the trailing \n */

static char *my_fgets(char *buf,gsize bufsize,FILE *df)
{
  int c;
  gsize pos;
  do {
    if (buf != ((char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buf != NULL");
      return 0;
    };
  }while (0);
  do {
    if (df != ((FILE *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"df != NULL");
      return 0;
    };
  }while (0);
  pos = 0;
  buf[0] = 0;
{
    do {
      c = _IO_getc(df);
      if ((c == -1) || (c == 10)) 
        break; 
      buf[pos++] = c;
    }while (pos < (bufsize - 1));
  }
  if ((c == -1) && (pos == 0)) 
    return 0;
  buf[pos] = 0;
  return buf;
}

static Encoding get_encoding(FILE *df)
{
  gboolean old_kde = 0;
  char buf[8192UL];
  gboolean all_valid_utf8 = (!0);
  while(my_fgets(buf,(sizeof(buf)),df) != ((char *)((void *)0))){
    if (strncmp("Encoding",buf,strlen("Encoding")) == 0) {
      char *p = (buf + strlen("Encoding"));
      if (( *p) == 32) 
        p++;
      if (( *p) != '=') 
        continue; 
      p++;
      if (( *p) == 32) 
        p++;
      if (purple_strequal(p,"UTF-8") != 0) {
        return ENCODING_UTF8;
      }
      else if (purple_strequal(p,"Legacy-Mixed") != 0) {
        return ENCODING_LEGACY_MIXED;
      }
      else {
/* According to the spec we're not supposed
				 * to read a file like this */
        return ENCODING_UNKNOWN;
      }
    }
    else if (purple_strequal("[KDE Desktop Entry]",buf) != 0) {
      old_kde = (!0);
/* don't break yet, we still want to support
			 * Encoding even here */
    }
    if ((all_valid_utf8 != 0) && !(g_utf8_validate(buf,(-1),0) != 0)) 
      all_valid_utf8 = 0;
  }
  if (old_kde != 0) 
    return ENCODING_LEGACY_MIXED;
/* A dilemma, new KDE files are in UTF-8 but have no Encoding
	 * info, at this time we really can't tell.  The best thing to
	 * do right now is to just assume UTF-8 if the whole file
	 * validates as utf8 I suppose */
  if (all_valid_utf8 != 0) 
    return ENCODING_UTF8;
  else 
    return ENCODING_LEGACY_MIXED;
}

static char *snarf_locale_from_key(const char *key)
{
  const char *brace;
  char *locale;
  char *p;
  brace = (strchr(key,'['));
  if (brace == ((const char *)((void *)0))) 
    return 0;
  locale = g_strdup((brace + 1));
  if (( *locale) == 0) {
    g_free(locale);
    return 0;
  }
  p = strchr(locale,']');
  if (p == ((char *)((void *)0))) {
    g_free(locale);
    return 0;
  }
   *p = 0;
  return locale;
}

static gboolean check_locale(const char *locale)
{
  GIConv cd = g_iconv_open("UTF-8",locale);
  if (((GIConv )((GIConv )(-1))) == cd) 
    return 0;
  g_iconv_close(cd);
  return (!0);
}

static void insert_locales(GHashTable *encodings,char *enc,... )
{
  va_list args;
  char *s;
  va_start(args,enc);
{
    for (; ; ) {
      s = (va_arg(args,char *));
      if (s == ((char *)((void *)0))) 
        break; 
      g_hash_table_insert(encodings,s,enc);
    }
  }
  va_end(args);
}
/* make a standard conversion table from the desktop standard spec */

static GHashTable *init_encodings()
{
  GHashTable *encodings = g_hash_table_new(g_str_hash,g_str_equal);
/* "C" is plain ascii */
  insert_locales(encodings,"ASCII","C",((void *)((void *)0)));
  insert_locales(encodings,"ARMSCII-8","by",((void *)((void *)0)));
  insert_locales(encodings,"BIG5","zh_TW",((void *)((void *)0)));
  insert_locales(encodings,"CP1251","be","bg",((void *)((void *)0)));
  if (check_locale("EUC-CN") != 0) {
    insert_locales(encodings,"EUC-CN","zh_CN",((void *)((void *)0)));
  }
  else {
    insert_locales(encodings,"GB2312","zh_CN",((void *)((void *)0)));
  }
  insert_locales(encodings,"EUC-JP","ja",((void *)((void *)0)));
  insert_locales(encodings,"EUC-KR","ko",((void *)((void *)0)));
/*insert_locales (encodings, "GEORGIAN-ACADEMY", NULL);*/
  insert_locales(encodings,"GEORGIAN-PS","ka",((void *)((void *)0)));
  insert_locales(encodings,"ISO-8859-1","br","ca","da","de","en","es","eu","fi","fr","gl","it","nl","wa","no","pt","pt","sv",((void *)((void *)0)));
  insert_locales(encodings,"ISO-8859-2","cs","hr","hu","pl","ro","sk","sl","sq","sr",((void *)((void *)0)));
  insert_locales(encodings,"ISO-8859-3","eo",((void *)((void *)0)));
  insert_locales(encodings,"ISO-8859-5","mk","sp",((void *)((void *)0)));
  insert_locales(encodings,"ISO-8859-7","el",((void *)((void *)0)));
  insert_locales(encodings,"ISO-8859-9","tr",((void *)((void *)0)));
  insert_locales(encodings,"ISO-8859-13","lt","lv","mi",((void *)((void *)0)));
  insert_locales(encodings,"ISO-8859-14","ga","cy",((void *)((void *)0)));
  insert_locales(encodings,"ISO-8859-15","et",((void *)((void *)0)));
  insert_locales(encodings,"KOI8-R","ru",((void *)((void *)0)));
  insert_locales(encodings,"KOI8-U","uk",((void *)((void *)0)));
  if (check_locale("TCVN-5712") != 0) {
    insert_locales(encodings,"TCVN-5712","vi",((void *)((void *)0)));
  }
  else {
    insert_locales(encodings,"TCVN","vi",((void *)((void *)0)));
  }
  insert_locales(encodings,"TIS-620","th",((void *)((void *)0)));
/*insert_locales (encodings, "VISCII", NULL);*/
  return encodings;
}

static const char *get_encoding_from_locale(const char *locale)
{
  char lang[3UL];
  const char *encoding;
  static GHashTable *encodings = (GHashTable *)((void *)0);
  if (locale == ((const char *)((void *)0))) 
    return 0;
/* if locale includes encoding, use it */
  encoding = (strchr(locale,'.'));
  if (encoding != ((const char *)((void *)0))) {
    return encoding + 1;
  }
  if (encodings == ((GHashTable *)((void *)0))) 
    encodings = init_encodings();
/* first try the entire locale (at this point ll_CC) */
  encoding = (g_hash_table_lookup(encodings,locale));
  if (encoding != ((const char *)((void *)0))) 
    return encoding;
/* Try just the language */
  strncpy(lang,locale,2);
  lang[2] = 0;
  return (g_hash_table_lookup(encodings,lang));
}

static char *decode_string_and_dup(const char *s)
{
  char *p = (g_malloc((strlen(s) + 1)));
  char *q = p;
  do {
    if (( *s) == '\\') {
      switch(( *(++s))){
        case 's':
{
           *(p++) = 32;
          break; 
        }
        case 't':
{
           *(p++) = 9;
          break; 
        }
        case 'n':
{
           *(p++) = 10;
          break; 
        }
        case '\\':
{
           *(p++) = '\\';
          break; 
        }
        case 'r':
{
           *(p++) = 13;
          break; 
        }
        default:
{
           *(p++) = '\\';
           *(p++) =  *s;
          break; 
        }
      }
    }
    else {
       *(p++) =  *s;
    }
  }while (( *(s++)) != 0);
  return q;
}

static char *decode_string(const char *value,Encoding encoding,const char *locale)
{
  char *retval = (char *)((void *)0);
/* if legacy mixed, then convert */
  if ((locale != ((const char *)((void *)0))) && (encoding == ENCODING_LEGACY_MIXED)) {
    const char *char_encoding = get_encoding_from_locale(locale);
    char *utf8_string;
    if (char_encoding == ((const char *)((void *)0))) 
      return 0;
    if (purple_strequal(char_encoding,"ASCII") != 0) {
      return decode_string_and_dup(value);
    }
    utf8_string = g_convert(value,(-1),"UTF-8",char_encoding,0,0,0);
    if (utf8_string == ((char *)((void *)0))) 
      return 0;
    retval = decode_string_and_dup(utf8_string);
    g_free(utf8_string);
    return retval;
/* if utf8, then validate */
  }
  else if ((locale != ((const char *)((void *)0))) && (encoding == ENCODING_UTF8)) {
    if (!(g_utf8_validate(value,(-1),0) != 0)) 
/* invalid utf8, ignore this key */
      return 0;
    return decode_string_and_dup(value);
  }
  else {
/* Meaning this is not a localized string */
    return decode_string_and_dup(value);
  }
}
/************************************************************
 * Parser:                                                  *
 ************************************************************/

static gboolean standard_is_boolean(const char *key)
{
  static GHashTable *bools = (GHashTable *)((void *)0);
  if (bools == ((GHashTable *)((void *)0))) {
    bools = g_hash_table_new(g_str_hash,g_str_equal);
    g_hash_table_insert(bools,"NoDisplay","NoDisplay");
    g_hash_table_insert(bools,"Hidden","Hidden");
    g_hash_table_insert(bools,"Terminal","Terminal");
    g_hash_table_insert(bools,"ReadOnly","ReadOnly");
  }
  return g_hash_table_lookup(bools,key) != ((void *)((void *)0));
}

static gboolean standard_is_strings(const char *key)
{
  static GHashTable *strings = (GHashTable *)((void *)0);
  if (strings == ((GHashTable *)((void *)0))) {
    strings = g_hash_table_new(g_str_hash,g_str_equal);
    g_hash_table_insert(strings,"FilePattern","FilePattern");
    g_hash_table_insert(strings,"Actions","Actions");
    g_hash_table_insert(strings,"MimeType","MimeType");
    g_hash_table_insert(strings,"Patterns","Patterns");
    g_hash_table_insert(strings,"SortOrder","SortOrder");
  }
  return g_hash_table_lookup(strings,key) != ((void *)((void *)0));
}
/* If no need to cannonize, returns NULL */

static char *cannonize(const char *key,const char *value)
{
  if (standard_is_boolean(key) != 0) {
    if (((((value[0] == 'T') || (value[0] == 't')) || (value[0] == 'Y')) || (value[0] == 'y')) || (atoi(value) != 0)) {
      return g_strdup("true");
    }
    else {
      return g_strdup("false");
    }
  }
  else if (standard_is_strings(key) != 0) {
    int len = (strlen(value));
    if ((len == 0) || (value[len - 1] != ';')) {
      return g_strconcat(value,";",((void *)((void *)0)));
    }
  }
/* XXX: Perhaps we should canonize numeric values as well, but this
	 * has caused some subtle problems before so it needs to be done
	 * carefully if at all */
  return 0;
}

static void insert_key(PurpleDesktopItem *item,Section *cur_section,Encoding encoding,const char *key,const char *value,gboolean old_kde,gboolean no_translations)
{
  char *k;
  char *val;
/* we always store everything in UTF-8 */
  if ((cur_section == ((Section *)((void *)0))) && (purple_strequal(key,"Encoding") != 0)) {
    k = g_strdup(key);
    val = g_strdup("UTF-8");
  }
  else {
    char *locale = snarf_locale_from_key(key);
/* If we're ignoring translations */
    if ((no_translations != 0) && (locale != ((char *)((void *)0)))) {
      g_free(locale);
      return ;
    }
    val = decode_string(value,encoding,locale);
/* Ignore this key, it's whacked */
    if (val == ((char *)((void *)0))) {
      g_free(locale);
      return ;
    }
    g_strchomp(val);
/* For old KDE entries, we can also split by a comma
		 * on sort order, so convert to semicolons */
    if ((((old_kde != 0) && (cur_section == ((Section *)((void *)0)))) && (purple_strequal(key,"SortOrder") != 0)) && (strchr(val,';') == ((char *)((void *)0)))) {
      int i;
      for (i = 0; val[i] != 0; i++) {
        if (val[i] == ',') 
          val[i] = ';';
      }
    }
/* Check some types, not perfect, but catches a lot
		 * of things */
    if (cur_section == ((Section *)((void *)0))) {
      char *cannon = cannonize(key,val);
      if (cannon != ((char *)((void *)0))) {
        g_free(val);
        val = cannon;
      }
    }
    k = g_strdup(key);
/* Take care of the language part */
    if ((locale != ((char *)((void *)0))) && (purple_strequal(locale,"C") != 0)) {
      char *p;
/* Whack C locale */
      p = strchr(k,'[');
      if (p != 0) 
         *p = 0;
      g_free(locale);
    }
    else if (locale != ((char *)((void *)0))) {
      char *p;
      char *brace;
/* Whack the encoding part */
      p = strchr(locale,'.');
      if (p != ((char *)((void *)0))) 
         *p = 0;
      if (g_list_find_custom((item -> languages),locale,((GCompareFunc )strcmp)) == ((GList *)((void *)0))) {
        item -> languages = g_list_prepend((item -> languages),locale);
      }
      else {
        g_free(locale);
      }
/* Whack encoding from encoding in the key */
      brace = strchr(k,'[');
      if (brace != ((char *)((void *)0))) {
        p = strchr(brace,'.');
        if (p != ((char *)((void *)0))) {
           *p = ']';
           *(p + 1) = 0;
        }
      }
    }
  }
  if (cur_section == ((Section *)((void *)0))) {
/* only add to list if we haven't seen it before */
    if (g_hash_table_lookup((item -> main_hash),k) == ((void *)((void *)0))) {
      item -> keys = g_list_prepend((item -> keys),(g_strdup(k)));
    }
/* later duplicates override earlier ones */
    g_hash_table_replace((item -> main_hash),k,val);
  }
  else {
    char *full = g_strdup_printf("%s/%s",(cur_section -> name),k);
/* only add to list if we haven't seen it before */
    if (g_hash_table_lookup((item -> main_hash),full) == ((void *)((void *)0))) {
      cur_section -> keys = g_list_prepend((cur_section -> keys),k);
    }
/* later duplicates override earlier ones */
    g_hash_table_replace((item -> main_hash),full,val);
  }
}

static const char *lookup(const PurpleDesktopItem *item,const char *key)
{
  return (g_hash_table_lookup((item -> main_hash),key));
}

static void setup_type(PurpleDesktopItem *item,const char *uri)
{
  const char *type = (g_hash_table_lookup((item -> main_hash),"Type"));
  if ((type == ((const char *)((void *)0))) && (uri != ((const char *)((void *)0)))) {
    char *base = g_path_get_basename(uri);
    if (purple_strequal(base,".directory") != 0) {
/* This gotta be a directory */
      g_hash_table_replace((item -> main_hash),(g_strdup("Type")),(g_strdup("Directory")));
      item -> keys = g_list_prepend((item -> keys),(g_strdup("Type")));
      item -> type = PURPLE_DESKTOP_ITEM_TYPE_DIRECTORY;
    }
    else {
      item -> type = PURPLE_DESKTOP_ITEM_TYPE_NULL;
    }
    g_free(base);
  }
  else {
    item -> type = type_from_string(type);
  }
}

static const char *lookup_locale(const PurpleDesktopItem *item,const char *key,const char *locale)
{
  if ((locale == ((const char *)((void *)0))) || (purple_strequal(locale,"C") != 0)) {
    return lookup(item,key);
  }
  else {
    const char *ret;
    char *full = g_strdup_printf("%s[%s]",key,locale);
    ret = lookup(item,full);
    g_free(full);
    return ret;
  }
}
/**
 * Fallback to find something suitable for C locale.
 *
 * @return A newly allocated string which should be g_freed by the caller.
 */

static char *try_english_key(PurpleDesktopItem *item,const char *key)
{
  char *str = (char *)((void *)0);
  char *locales[] = {("en_US"), ("en_GB"), ("en_AU"), ("en"), ((char *)((void *)0))};
  int i;
  for (i = 0; (locales[i] != ((char *)((void *)0))) && (str == ((char *)((void *)0))); i++) {
    str = g_strdup(lookup_locale(item,key,locales[i]));
  }
  if (str != ((char *)((void *)0))) {
/* We need a 7-bit ascii string, so whack all
		 * above 127 chars */
    guchar *p;
    for (p = ((guchar *)str); ( *p) != 0; p++) {
      if (( *p) > 127) 
         *p = '?';
    }
  }
  return str;
}

static void sanitize(PurpleDesktopItem *item,const char *uri)
{
  const char *type;
  type = lookup(item,"Type");
/* understand old gnome style url exec thingies */
  if (purple_strequal(type,"URL") != 0) {
    const char *exec = lookup(item,"Exec");
    set(item,"Type","Link");
    if (exec != ((const char *)((void *)0))) {
/* Note, this must be in this order */
      set(item,"URL",exec);
      set(item,"Exec",0);
    }
  }
/* we make sure we have Name, Encoding and Version */
  if (lookup(item,"Name") == ((const char *)((void *)0))) {
    char *name = try_english_key(item,"Name");
/* If no name, use the basename */
    if ((name == ((char *)((void *)0))) && (uri != ((const char *)((void *)0)))) 
      name = g_path_get_basename(uri);
/* If no uri either, use same default as gnome_desktop_item_new */
    if (name == ((char *)((void *)0))) 
      name = g_strdup(((const char *)(dgettext("pidgin","No name"))));
    g_hash_table_replace((item -> main_hash),(g_strdup("Name")),name);
    item -> keys = g_list_prepend((item -> keys),(g_strdup("Name")));
  }
  if (lookup(item,"Encoding") == ((const char *)((void *)0))) {
/* We store everything in UTF-8 so write that down */
    g_hash_table_replace((item -> main_hash),(g_strdup("Encoding")),(g_strdup("UTF-8")));
    item -> keys = g_list_prepend((item -> keys),(g_strdup("Encoding")));
  }
  if (lookup(item,"Version") == ((const char *)((void *)0))) {
/* this is the version that we follow, so write it down */
    g_hash_table_replace((item -> main_hash),(g_strdup("Version")),(g_strdup("1.0")));
    item -> keys = g_list_prepend((item -> keys),(g_strdup("Version")));
  }
}
enum __unnamed_enum___F0_L905_C1_FirstBrace__COMMA__OnSecHeader__COMMA__IgnoreToEOL__COMMA__IgnoreToEOLFirst__COMMA__KeyDef__COMMA__KeyDefOnKey__COMMA__KeyValue {FirstBrace,OnSecHeader,IgnoreToEOL,IgnoreToEOLFirst,KeyDef,KeyDefOnKey,KeyValue};

static PurpleDesktopItem *ditem_load(FILE *df,gboolean no_translations,const char *uri)
{
  int state;
  char CharBuffer[1024UL];
  char *next = CharBuffer;
  int c;
  Encoding encoding;
  PurpleDesktopItem *item;
  Section *cur_section = (Section *)((void *)0);
  char *key = (char *)((void *)0);
  gboolean old_kde = 0;
  encoding = get_encoding(df);
  if (encoding == ENCODING_UNKNOWN) {
    fclose(df);
/* spec says, don't read this file */
    printf("Unknown encoding of .desktop file");
    return 0;
  }
/* Rewind since get_encoding goes through the file */
  if (fseek(df,0L,0) != 0) {
    fclose(df);
/* spec says, don't read this file */
    printf("fseek() error on .desktop file");
    return 0;
  }
  item = _purple_desktop_item_new();
  item -> modified = 0;
/* Note: location and mtime are filled in by the new_from_file
	 * function since it has those values */
#define PURPLE_DESKTOP_ITEM_OVERFLOW (next == &CharBuffer [sizeof(CharBuffer)-1])
  state = FirstBrace;
  while((c = _IO_getc(df)) != -1){
/* Ignore Carriage Return */
    if (c == 13) 
      continue; 
{
      switch(state){
        case 1:
{
          if ((c == ']') || (next == (CharBuffer + (sizeof(CharBuffer) - 1)))) {
             *next = 0;
            next = CharBuffer;
/* keys were inserted in reverse */
            if ((cur_section != ((Section *)((void *)0))) && ((cur_section -> keys) != ((GList *)((void *)0)))) {
              cur_section -> keys = g_list_reverse((cur_section -> keys));
            }
            if (purple_strequal(CharBuffer,"KDE Desktop Entry") != 0) {
/* Main section */
              cur_section = ((Section *)((void *)0));
              old_kde = (!0);
            }
            else if (purple_strequal(CharBuffer,"Desktop Entry") != 0) {
/* Main section */
              cur_section = ((Section *)((void *)0));
            }
            else {
              cur_section = ((Section *)(g_malloc0_n(1,(sizeof(Section )))));
              cur_section -> name = g_strdup(CharBuffer);
              cur_section -> keys = ((GList *)((void *)0));
              item -> sections = g_list_prepend((item -> sections),cur_section);
            }
            state = IgnoreToEOL;
          }
          else if (c == '[') {
/* FIXME: probably error out instead of ignoring this */
          }
          else {
             *(next++) = c;
          }
          break; 
        }
        case 2:
{
        }
        case 3:
{
          if (c == 10) {
            if (state == IgnoreToEOLFirst) 
              state = FirstBrace;
            else 
              state = KeyDef;
            next = CharBuffer;
          }
          break; 
        }
        case 0:
{
        }
        case 4:
{
        }
        case 5:
{
          if (c == '#') {
            if (state == FirstBrace) 
              state = IgnoreToEOLFirst;
            else 
              state = IgnoreToEOL;
            break; 
          }
          if ((c == '[') && (state != KeyDefOnKey)) {
            state = OnSecHeader;
            next = CharBuffer;
            g_free(key);
            key = ((char *)((void *)0));
            break; 
          }
/* On first pass, don't allow dangling keys */
          if (state == FirstBrace) 
            break; 
          if (((c == 32) && (state != KeyDefOnKey)) || (c == 9)) 
            break; 
/* Abort Definition */
          if ((c == 10) || (next == (CharBuffer + (sizeof(CharBuffer) - 1)))) {
            next = CharBuffer;
            state = KeyDef;
            break; 
          }
          if ((c == '=') || (next == (CharBuffer + (sizeof(CharBuffer) - 1)))) {
             *next = 0;
            g_free(key);
            key = g_strdup(CharBuffer);
            state = KeyValue;
            next = CharBuffer;
          }
          else {
             *(next++) = c;
            state = KeyDefOnKey;
          }
          break; 
        }
        case 6:
{
          if ((next == (CharBuffer + (sizeof(CharBuffer) - 1))) || (c == 10)) {
             *next = 0;
            insert_key(item,cur_section,encoding,key,CharBuffer,old_kde,no_translations);
            g_free(key);
            key = ((char *)((void *)0));
            state = ((c == 10)?KeyDef : IgnoreToEOL);
            next = CharBuffer;
          }
          else {
             *(next++) = c;
          }
          break; 
        }
/* switch */
      }
    }
/* while ((c = getc_unlocked (f)) != EOF) */
  }
  if ((c == -1) && (state == KeyValue)) {
     *next = 0;
    insert_key(item,cur_section,encoding,key,CharBuffer,old_kde,no_translations);
    g_free(key);
    key = ((char *)((void *)0));
  }
#undef PURPLE_DESKTOP_ITEM_OVERFLOW
/* keys were inserted in reverse */
  if ((cur_section != ((Section *)((void *)0))) && ((cur_section -> keys) != ((GList *)((void *)0)))) {
    cur_section -> keys = g_list_reverse((cur_section -> keys));
  }
/* keys were inserted in reverse */
  item -> keys = g_list_reverse((item -> keys));
/* sections were inserted in reverse */
  item -> sections = g_list_reverse((item -> sections));
/* sanitize some things */
  sanitize(item,uri);
/* make sure that we set up the type */
  setup_type(item,uri);
  fclose(df);
  return item;
}

static void copy_string_hash(gpointer key,gpointer value,gpointer user_data)
{
  GHashTable *copy = user_data;
  g_hash_table_replace(copy,(g_strdup(key)),(g_strdup(value)));
}

static void free_section(gpointer data,gpointer user_data)
{
  Section *section = data;
  g_free((section -> name));
  section -> name = ((char *)((void *)0));
  g_list_foreach((section -> keys),((GFunc )g_free),0);
  g_list_free((section -> keys));
  section -> keys = ((GList *)((void *)0));
  g_free(section);
}

static Section *dup_section(Section *sec)
{
  GList *li;
  Section *retval = (Section *)(g_malloc0_n(1,(sizeof(Section ))));
  retval -> name = g_strdup((sec -> name));
  retval -> keys = g_list_copy((sec -> keys));
  for (li = (retval -> keys); li != ((GList *)((void *)0)); li = (li -> next)) 
    li -> data = (g_strdup((li -> data)));
  return retval;
}
/**************************************************************************
 * Public functions
 **************************************************************************/

PurpleDesktopItem *purple_desktop_item_new_from_file(const char *filename)
{
  PurpleDesktopItem *retval;
  FILE *dfile;
  do {
    if (filename != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"filename != NULL");
      return 0;
    };
  }while (0);
  dfile = fopen(filename,"r");
  if (!(dfile != 0)) {
    printf("Can\'t open %s: %s",filename,g_strerror( *__errno_location()));
    return 0;
  }
  retval = ditem_load(dfile,0,filename);
  return retval;
}

PurpleDesktopItemType purple_desktop_item_get_entry_type(const PurpleDesktopItem *item)
{
  do {
    if (item != ((const PurpleDesktopItem *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"item != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((item -> refcount) > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"item->refcount > 0");
      return 0;
    };
  }while (0);
  return item -> type;
}

const char *purple_desktop_item_get_string(const PurpleDesktopItem *item,const char *attr)
{
  do {
    if (item != ((const PurpleDesktopItem *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"item != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((item -> refcount) > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"item->refcount > 0");
      return 0;
    };
  }while (0);
  do {
    if (attr != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"attr != NULL");
      return 0;
    };
  }while (0);
  return lookup(item,attr);
}

PurpleDesktopItem *purple_desktop_item_copy(const PurpleDesktopItem *item)
{
  GList *li;
  PurpleDesktopItem *retval;
  do {
    if (item != ((const PurpleDesktopItem *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"item != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((item -> refcount) > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"item->refcount > 0");
      return 0;
    };
  }while (0);
  retval = _purple_desktop_item_new();
  retval -> type = (item -> type);
  retval -> modified = (item -> modified);
  retval -> location = g_strdup((item -> location));
  retval -> mtime = (item -> mtime);
/* Languages */
  retval -> languages = g_list_copy((item -> languages));
  for (li = (retval -> languages); li != ((GList *)((void *)0)); li = (li -> next)) 
    li -> data = (g_strdup((li -> data)));
/* Keys */
  retval -> keys = g_list_copy((item -> keys));
  for (li = (retval -> keys); li != ((GList *)((void *)0)); li = (li -> next)) 
    li -> data = (g_strdup((li -> data)));
/* Sections */
  retval -> sections = g_list_copy((item -> sections));
  for (li = (retval -> sections); li != ((GList *)((void *)0)); li = (li -> next)) 
    li -> data = (dup_section((li -> data)));
  retval -> main_hash = g_hash_table_new_full(g_str_hash,g_str_equal,((GDestroyNotify )g_free),((GDestroyNotify )g_free));
  g_hash_table_foreach((item -> main_hash),copy_string_hash,(retval -> main_hash));
  return retval;
}

void purple_desktop_item_unref(PurpleDesktopItem *item)
{
  do {
    if (item != ((PurpleDesktopItem *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"item != NULL");
      return ;
    };
  }while (0);
  do {
    if ((item -> refcount) > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"item->refcount > 0");
      return ;
    };
  }while (0);
  item -> refcount--;
  if ((item -> refcount) != 0) 
    return ;
  g_list_foreach((item -> languages),((GFunc )g_free),0);
  g_list_free((item -> languages));
  item -> languages = ((GList *)((void *)0));
  g_list_foreach((item -> keys),((GFunc )g_free),0);
  g_list_free((item -> keys));
  item -> keys = ((GList *)((void *)0));
  g_list_foreach((item -> sections),free_section,0);
  g_list_free((item -> sections));
  item -> sections = ((GList *)((void *)0));
  g_hash_table_destroy((item -> main_hash));
  item -> main_hash = ((GHashTable *)((void *)0));
  g_free((item -> location));
  item -> location = ((char *)((void *)0));
  g_free(item);
}

GType purple_desktop_item_get_type()
{
  static GType type = 0;
  if (type == 0) {
    type = g_boxed_type_register_static("PurpleDesktopItem",_purple_desktop_item_copy,_purple_desktop_item_free);
  }
  return type;
}
