/**
 * @file simple.c
 *
 * purple
 *
 * Copyright (C) 2005 Thomas Butter <butter@uni-mannheim.de>
 *
 * ***
 * Thanks to Google's Summer of Code Program and the helpful mentors
 * ***
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "accountopt.h"
#include "blist.h"
#include "conversation.h"
#include "dnsquery.h"
#include "debug.h"
#include "notify.h"
#include "privacy.h"
#include "prpl.h"
#include "plugin.h"
#include "util.h"
#include "version.h"
#include "network.h"
#include "xmlnode.h"
#include "simple.h"
#include "sipmsg.h"
#include "dnssrv.h"
#include "ntlm.h"

static char *gentag()
{
  return g_strdup_printf("%04d%04d",(rand() & 0xFFFF),(rand() & 0xFFFF));
}

static char *genbranch()
{
  return g_strdup_printf("z9hG4bK%04X%04X%04X%04X%04X",(rand() & 0xFFFF),(rand() & 0xFFFF),(rand() & 0xFFFF),(rand() & 0xFFFF),(rand() & 0xFFFF));
}

static char *gencallid()
{
  return g_strdup_printf("%04Xg%04Xa%04Xi%04Xm%04Xt%04Xb%04Xx%04Xx",(rand() & 0xFFFF),(rand() & 0xFFFF),(rand() & 0xFFFF),(rand() & 0xFFFF),(rand() & 0xFFFF),(rand() & 0xFFFF),(rand() & 0xFFFF),(rand() & 0xFFFF));
}

static const char *simple_list_icon(PurpleAccount *a,PurpleBuddy *b)
{
  return "simple";
}

static void simple_keep_alive(PurpleConnection *gc)
{
  struct simple_account_data *sip = (gc -> proto_data);
/* in case of UDP send a packet only with a 0 byte to
			 remain in the NAT table */
  if ((sip -> udp) != 0) {
    gchar buf[2UL] = {(0), (0)};
    purple_debug_info("simple","sending keep alive\n");
    sendto((sip -> fd),buf,1,0,((struct sockaddr *)(&sip -> serveraddr)),(sizeof(struct sockaddr_in )));
  }
}
static gboolean process_register_response(struct simple_account_data *sip,struct sipmsg *msg,struct transaction *tc);
static void send_notify(struct simple_account_data *sip,struct simple_watcher *);
static void send_open_publish(struct simple_account_data *sip);
static void send_closed_publish(struct simple_account_data *sip);

static void do_notifies(struct simple_account_data *sip)
{
  GSList *tmp = (sip -> watcher);
  purple_debug_info("simple","do_notifies()\n");
  if (((sip -> republish) != (-1)) || ((sip -> republish) < time(0))) {
    if (purple_account_get_bool((sip -> account),"dopublish",(!0)) != 0) {
      send_open_publish(sip);
    }
  }
  while(tmp != 0){
    purple_debug_info("simple","notifying %s\n",( *((struct simple_watcher *)(tmp -> data))).name);
    send_notify(sip,(tmp -> data));
    tmp = (tmp -> next);
  }
}

static void simple_set_status(PurpleAccount *account,PurpleStatus *status)
{
  PurpleStatusPrimitive primitive = purple_status_type_get_primitive((purple_status_get_type(status)));
  struct simple_account_data *sip = (struct simple_account_data *)((void *)0);
  if (!(purple_status_is_active(status) != 0)) 
    return ;
  if ((account -> gc) != 0) 
    sip = ( *(account -> gc)).proto_data;
  if (sip != 0) {
    g_free((sip -> status));
    if (primitive == PURPLE_STATUS_AVAILABLE) 
      sip -> status = g_strdup("available");
    else 
      sip -> status = g_strdup("busy");
    do_notifies(sip);
  }
}

static struct sip_connection *connection_find(struct simple_account_data *sip,int fd)
{
  struct sip_connection *ret = (struct sip_connection *)((void *)0);
  GSList *entry = (sip -> openconns);
  while(entry != 0){
    ret = (entry -> data);
    if ((ret -> fd) == fd) 
      return ret;
    entry = (entry -> next);
  }
  return 0;
}

static struct simple_watcher *watcher_find(struct simple_account_data *sip,const gchar *name)
{
  struct simple_watcher *watcher;
  GSList *entry = (sip -> watcher);
  while(entry != 0){
    watcher = (entry -> data);
    if (!(strcmp(name,(watcher -> name)) != 0)) 
      return watcher;
    entry = (entry -> next);
  }
  return 0;
}

static struct simple_watcher *watcher_create(struct simple_account_data *sip,const gchar *name,const gchar *callid,const gchar *ourtag,const gchar *theirtag,gboolean needsxpidf)
{
  struct simple_watcher *watcher = (struct simple_watcher *)(g_malloc0_n(1,(sizeof(struct simple_watcher ))));
  watcher -> name = g_strdup(name);
  watcher -> dialog.callid = g_strdup(callid);
  watcher -> dialog.ourtag = g_strdup(ourtag);
  watcher -> dialog.theirtag = g_strdup(theirtag);
  watcher -> needsxpidf = needsxpidf;
  sip -> watcher = g_slist_append((sip -> watcher),watcher);
  return watcher;
}

static void watcher_remove(struct simple_account_data *sip,const gchar *name)
{
  struct simple_watcher *watcher = watcher_find(sip,name);
  sip -> watcher = g_slist_remove((sip -> watcher),watcher);
  g_free((watcher -> name));
  g_free(watcher -> dialog.callid);
  g_free(watcher -> dialog.ourtag);
  g_free(watcher -> dialog.theirtag);
  g_free(watcher);
}

static struct sip_connection *connection_create(struct simple_account_data *sip,int fd)
{
  struct sip_connection *ret = (struct sip_connection *)(g_malloc0_n(1,(sizeof(struct sip_connection ))));
  ret -> fd = fd;
  sip -> openconns = g_slist_append((sip -> openconns),ret);
  return ret;
}

static void connection_remove(struct simple_account_data *sip,int fd)
{
  struct sip_connection *conn = connection_find(sip,fd);
  sip -> openconns = g_slist_remove((sip -> openconns),conn);
  if ((conn -> inputhandler) != 0) 
    purple_input_remove((conn -> inputhandler));
  g_free((conn -> inbuf));
  g_free(conn);
}

static void connection_free_all(struct simple_account_data *sip)
{
  struct sip_connection *ret = (struct sip_connection *)((void *)0);
  GSList *entry = (sip -> openconns);
  while(entry != 0){
    ret = (entry -> data);
    connection_remove(sip,(ret -> fd));
    entry = (sip -> openconns);
  }
}

static void simple_add_buddy(PurpleConnection *gc,PurpleBuddy *buddy,PurpleGroup *group)
{
  struct simple_account_data *sip = (struct simple_account_data *)(gc -> proto_data);
  struct simple_buddy *b;
  const char *name = purple_buddy_get_name(buddy);
  if (strncmp(name,"sip:",4) != 0) {
    gchar *buf = g_strdup_printf("sip:%s",name);
    purple_blist_rename_buddy(buddy,buf);
    g_free(buf);
  }
  if (!(g_hash_table_lookup((sip -> buddies),name) != 0)) {
    b = ((struct simple_buddy *)(g_malloc0_n(1,(sizeof(struct simple_buddy )))));
    purple_debug_info("simple","simple_add_buddy %s\n",name);
    b -> name = g_strdup(name);
    g_hash_table_insert((sip -> buddies),(b -> name),b);
  }
  else {
    purple_debug_info("simple","buddy %s already in internal list\n",name);
  }
}

static void simple_get_buddies(PurpleConnection *gc)
{
  GSList *buddies;
  PurpleAccount *account;
  purple_debug_info("simple","simple_get_buddies\n");
  account = purple_connection_get_account(gc);
  buddies = purple_find_buddies(account,0);
  while(buddies != 0){
    PurpleBuddy *buddy = (buddies -> data);
    simple_add_buddy(gc,buddy,purple_buddy_get_group(buddy));
    buddies = g_slist_delete_link(buddies,buddies);
  }
}

static void simple_remove_buddy(PurpleConnection *gc,PurpleBuddy *buddy,PurpleGroup *group)
{
  const char *name = purple_buddy_get_name(buddy);
  struct simple_account_data *sip = (struct simple_account_data *)(gc -> proto_data);
  struct simple_buddy *b = (g_hash_table_lookup((sip -> buddies),name));
  g_hash_table_remove((sip -> buddies),name);
  g_free((b -> name));
  g_free(b);
}

static GList *simple_status_types(PurpleAccount *acc)
{
  PurpleStatusType *type;
  GList *types = (GList *)((void *)0);
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_AVAILABLE,0,0,(!0),(!0),0,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  types = g_list_append(types,type);
  type = purple_status_type_new_full(PURPLE_STATUS_OFFLINE,0,0,(!0),(!0),0);
  types = g_list_append(types,type);
  return types;
}

static gchar *auth_header(struct simple_account_data *sip,struct sip_auth *auth,const gchar *method,const gchar *target)
{
  gchar noncecount[9UL];
  gchar *response;
  gchar *ret;
  gchar *tmp;
  const char *authdomain;
  const char *authuser;
  authdomain = purple_account_get_string((sip -> account),"authdomain","");
  authuser = purple_account_get_string((sip -> account),"authuser",(sip -> username));
  if (!(authuser != 0) || (strlen(authuser) < 1)) {
    authuser = (sip -> username);
  }
/* Digest */
  if ((auth -> type) == 1) {
    sprintf(noncecount,"%08d",auth -> nc++);
    response = purple_cipher_http_digest_calculate_response("md5",method,target,0,0,(auth -> nonce),noncecount,0,(auth -> digest_session_key));
    purple_debug(PURPLE_DEBUG_MISC,"simple","response %s\n",response);
    ret = g_strdup_printf("Digest username=\"%s\", realm=\"%s\", nonce=\"%s\", uri=\"%s\", nc=\"%s\", response=\"%s\"",authuser,(auth -> realm),(auth -> nonce),target,noncecount,response);
    g_free(response);
    return ret;
/* NTLM */
  }
  else if ((auth -> type) == 2) {
    if (((auth -> nc) == 3) && ((auth -> nonce) != 0)) {
/* TODO: Don't hardcode "purple" as the hostname */
      ret = purple_ntlm_gen_type3(authuser,(sip -> password),"purple",authdomain,((const guint8 *)(auth -> nonce)),&auth -> flags);
      tmp = g_strdup_printf("NTLM qop=\"auth\", opaque=\"%s\", realm=\"%s\", targetname=\"%s\", gssapi-data=\"%s\"",(auth -> opaque),(auth -> realm),(auth -> target),ret);
      g_free(ret);
      return tmp;
    }
    tmp = g_strdup_printf("NTLM qop=\"auth\", realm=\"%s\", targetname=\"%s\", gssapi-data=\"\"",(auth -> realm),(auth -> target));
    return tmp;
  }
  sprintf(noncecount,"%08d",auth -> nc++);
  response = purple_cipher_http_digest_calculate_response("md5",method,target,0,0,(auth -> nonce),noncecount,0,(auth -> digest_session_key));
  purple_debug(PURPLE_DEBUG_MISC,"simple","response %s\n",response);
  ret = g_strdup_printf("Digest username=\"%s\", realm=\"%s\", nonce=\"%s\", uri=\"%s\", nc=\"%s\", response=\"%s\"",authuser,(auth -> realm),(auth -> nonce),target,noncecount,response);
  g_free(response);
  return ret;
}

static char *parse_attribute(const char *attrname,const char *source)
{
  const char *tmp;
  const char *tmp2;
  char *retval = (char *)((void *)0);
  int len = (strlen(attrname));
/* we know that source is NULL-terminated.
	 * Therefore this loop won't be infinite.
	 */
  while(source[0] == 32)
    source++;
  if (!(strncmp(source,attrname,len) != 0)) {
    tmp = (source + len);
    tmp2 = (g_strstr_len(tmp,(strlen(tmp)),"\""));
    if (tmp2 != 0) 
      retval = g_strndup(tmp,(tmp2 - tmp));
    else 
      retval = g_strdup(tmp);
  }
  return retval;
}

static void fill_auth(struct simple_account_data *sip,const gchar *hdr,struct sip_auth *auth)
{
  int i = 0;
  const char *authuser;
  char *tmp;
  gchar **parts;
  authuser = purple_account_get_string((sip -> account),"authuser",(sip -> username));
  if (!(authuser != 0) || (strlen(authuser) < 1)) {
    authuser = (sip -> username);
  }
  if (!(hdr != 0)) {
    purple_debug_error("simple","fill_auth: hdr==NULL\n");
    return ;
  }
  if (!(g_ascii_strncasecmp(hdr,"NTLM",4) != 0)) {
    purple_debug_info("simple","found NTLM\n");
    auth -> type = 2;
    parts = g_strsplit((hdr + 5),"\",",0);
    i = 0;
    while(parts[i] != 0){
      purple_debug_info("simple","parts[i] %s\n",parts[i]);
      if ((tmp = parse_attribute("gssapi-data=\"",parts[i])) != 0) {
        auth -> nonce = (g_memdup((purple_ntlm_parse_type2(tmp,&auth -> flags)),8));
        g_free(tmp);
      }
      if ((tmp = parse_attribute("targetname=\"",parts[i])) != 0) {
        auth -> target = tmp;
      }
      else if ((tmp = parse_attribute("realm=\"",parts[i])) != 0) {
        auth -> realm = tmp;
      }
      else if ((tmp = parse_attribute("opaque=\"",parts[i])) != 0) {
        auth -> opaque = tmp;
      }
      i++;
    }
    g_strfreev(parts);
    auth -> nc = 1;
    if (!(strstr(hdr,"gssapi-data") != 0)) {
      auth -> nc = 1;
    }
    else {
      auth -> nc = 3;
    }
    return ;
  }
  else if (!(g_ascii_strncasecmp(hdr,"DIGEST",6) != 0)) {
    purple_debug_info("simple","found DIGEST\n");
    auth -> type = 1;
    parts = g_strsplit((hdr + 7),",",0);
    while(parts[i] != 0){
      if ((tmp = parse_attribute("nonce=\"",parts[i])) != 0) {
        auth -> nonce = tmp;
      }
      else if ((tmp = parse_attribute("realm=\"",parts[i])) != 0) {
        auth -> realm = tmp;
      }
      i++;
    }
    g_strfreev(parts);
    purple_debug(PURPLE_DEBUG_MISC,"simple","nonce: %s realm: %s\n",(((auth -> nonce) != 0)?(auth -> nonce) : "(null)"),(((auth -> realm) != 0)?(auth -> realm) : "(null)"));
    if ((auth -> realm) != 0) {
      auth -> digest_session_key = purple_cipher_http_digest_calculate_session_key("md5",authuser,(auth -> realm),(sip -> password),(auth -> nonce),0);
      auth -> nc = 1;
    }
  }
  else {
    purple_debug_error("simple","Unsupported or bad WWW-Authenticate header (%s).\n",hdr);
  }
}

static void simple_canwrite_cb(gpointer data,gint source,PurpleInputCondition cond)
{
  PurpleConnection *gc = data;
  struct simple_account_data *sip = (gc -> proto_data);
  gsize max_write;
  gssize written;
  max_write = purple_circ_buffer_get_max_read((sip -> txbuf));
  if (max_write == 0) {
    purple_input_remove((sip -> tx_handler));
    sip -> tx_handler = 0;
    return ;
  }
  written = write((sip -> fd),( *(sip -> txbuf)).outptr,max_write);
  if ((written < 0) && ( *__errno_location() == 11)) 
    written = 0;
  else if (written <= 0) {
/*TODO: do we really want to disconnect on a failure to write?*/
    gchar *tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Lost connection with server: %s"))),g_strerror( *__errno_location()));
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,tmp);
    g_free(tmp);
    return ;
  }
  purple_circ_buffer_mark_read((sip -> txbuf),written);
}
static void simple_input_cb(gpointer data,gint source,PurpleInputCondition cond);

static void send_later_cb(gpointer data,gint source,const gchar *error_message)
{
  PurpleConnection *gc = data;
  struct simple_account_data *sip;
  struct sip_connection *conn;
  if (source < 0) {
    gchar *tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to connect: %s"))),error_message);
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,tmp);
    g_free(tmp);
    return ;
  }
  sip = (gc -> proto_data);
  sip -> fd = source;
  sip -> connecting = 0;
  simple_canwrite_cb(gc,(sip -> fd),PURPLE_INPUT_WRITE);
/* If there is more to write now, we need to register a handler */
  if (( *(sip -> txbuf)).bufused > 0) 
    sip -> tx_handler = purple_input_add((sip -> fd),PURPLE_INPUT_WRITE,simple_canwrite_cb,gc);
  conn = connection_create(sip,source);
  conn -> inputhandler = (purple_input_add((sip -> fd),PURPLE_INPUT_READ,simple_input_cb,gc));
}

static void sendlater(PurpleConnection *gc,const char *buf)
{
  struct simple_account_data *sip = (gc -> proto_data);
  if (!((sip -> connecting) != 0)) {
    purple_debug_info("simple","connecting to %s port %d\n",(((sip -> realhostname) != 0)?(sip -> realhostname) : "{NULL}"),(sip -> realport));
    if (purple_proxy_connect(gc,(sip -> account),(sip -> realhostname),(sip -> realport),send_later_cb,gc) == ((PurpleProxyConnectData *)((void *)0))) {
      purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Unable to connect"))));
    }
    sip -> connecting = (!0);
  }
  if (purple_circ_buffer_get_max_read((sip -> txbuf)) > 0) 
    purple_circ_buffer_append((sip -> txbuf),"\r\n",2);
  purple_circ_buffer_append((sip -> txbuf),buf,strlen(buf));
}

static void sendout_pkt(PurpleConnection *gc,const char *buf)
{
  struct simple_account_data *sip = (gc -> proto_data);
  time_t currtime = time(0);
  int writelen = (strlen(buf));
  purple_debug(PURPLE_DEBUG_MISC,"simple","\n\nsending - %s\n######\n%s\n######\n\n",ctime((&currtime)),buf);
  if ((sip -> udp) != 0) {
    if (sendto((sip -> fd),buf,writelen,0,((struct sockaddr *)(&sip -> serveraddr)),(sizeof(struct sockaddr_in ))) < writelen) {
      purple_debug_info("simple","could not send packet\n");
    }
  }
  else {
    int ret;
    if ((sip -> fd) < 0) {
      sendlater(gc,buf);
      return ;
    }
    if ((sip -> tx_handler) != 0U) {
      ret = -1;
       *__errno_location() = 11;
    }
    else 
      ret = (write((sip -> fd),buf,writelen));
    if ((ret < 0) && ( *__errno_location() == 11)) 
      ret = 0;
    else 
/* XXX: When does this happen legitimately? */
if (ret <= 0) {
      sendlater(gc,buf);
      return ;
    }
    if (ret < writelen) {
      if (!((sip -> tx_handler) != 0U)) 
        sip -> tx_handler = purple_input_add((sip -> fd),PURPLE_INPUT_WRITE,simple_canwrite_cb,gc);
/* XXX: is it OK to do this? You might get part of a request sent
			   with part of another. */
      if (( *(sip -> txbuf)).bufused > 0) 
        purple_circ_buffer_append((sip -> txbuf),"\r\n",2);
      purple_circ_buffer_append((sip -> txbuf),(buf + ret),(writelen - ret));
    }
  }
}

static int simple_send_raw(PurpleConnection *gc,const char *buf,int len)
{
  sendout_pkt(gc,buf);
  return len;
}

static void sendout_sipmsg(struct simple_account_data *sip,struct sipmsg *msg)
{
  GSList *tmp = (msg -> headers);
  gchar *name;
  gchar *value;
  GString *outstr = g_string_new("");
  g_string_append_printf(outstr,"%s %s SIP/2.0\r\n",(msg -> method),(msg -> target));
  while(tmp != 0){
    name = ( *((struct siphdrelement *)(tmp -> data))).name;
    value = ( *((struct siphdrelement *)(tmp -> data))).value;
    g_string_append_printf(outstr,"%s: %s\r\n",name,value);
    tmp = ((tmp != 0)?( *((GSList *)tmp)).next : ((struct _GSList *)((void *)0)));
  }
  g_string_append_printf(outstr,"\r\n%s",(((msg -> body) != 0)?(msg -> body) : ""));
  sendout_pkt((sip -> gc),(outstr -> str));
  g_string_free(outstr,(!0));
}

static void send_sip_response(PurpleConnection *gc,struct sipmsg *msg,int code,const char *text,const char *body)
{
  GSList *tmp = (msg -> headers);
  gchar *name;
  gchar *value;
  GString *outstr = g_string_new("");
/* When sending the acknowlegements and errors, the content length from the original
	   message is still here, but there is no body; we need to make sure we're sending the
	   correct content length */
  sipmsg_remove_header(msg,"Content-Length");
  if (body != 0) {
    gchar len[12UL];
    sprintf(len,"%lu",strlen(body));
    sipmsg_add_header(msg,"Content-Length",len);
  }
  else 
    sipmsg_add_header(msg,"Content-Length","0");
  g_string_append_printf(outstr,"SIP/2.0 %d %s\r\n",code,text);
  while(tmp != 0){
    name = ( *((struct siphdrelement *)(tmp -> data))).name;
    value = ( *((struct siphdrelement *)(tmp -> data))).value;
    g_string_append_printf(outstr,"%s: %s\r\n",name,value);
    tmp = ((tmp != 0)?( *((GSList *)tmp)).next : ((struct _GSList *)((void *)0)));
  }
  g_string_append_printf(outstr,"\r\n%s",((body != 0)?body : ""));
  sendout_pkt(gc,(outstr -> str));
  g_string_free(outstr,(!0));
}

static void transactions_remove(struct simple_account_data *sip,struct transaction *trans)
{
  if ((trans -> msg) != 0) 
    sipmsg_free((trans -> msg));
  sip -> transactions = g_slist_remove((sip -> transactions),trans);
  g_free(trans);
}

static void transactions_add_buf(struct simple_account_data *sip,const gchar *buf,void *callback)
{
  struct transaction *trans = (struct transaction *)(g_malloc0_n(1,(sizeof(struct transaction ))));
  trans -> time = time(0);
  trans -> msg = sipmsg_parse_msg(buf);
  trans -> cseq = sipmsg_find_header((trans -> msg),"CSeq");
  trans -> callback = callback;
  sip -> transactions = g_slist_append((sip -> transactions),trans);
}

static struct transaction *transactions_find(struct simple_account_data *sip,struct sipmsg *msg)
{
  struct transaction *trans;
  GSList *transactions = (sip -> transactions);
  const gchar *cseq = sipmsg_find_header(msg,"CSeq");
  if (cseq != 0) {
    while(transactions != 0){
      trans = (transactions -> data);
      if (!(strcmp((trans -> cseq),cseq) != 0)) {
        return trans;
      }
      transactions = (transactions -> next);
    }
  }
  else {
    purple_debug(PURPLE_DEBUG_MISC,"simple","Received message contains no CSeq header.\n");
  }
  return 0;
}

static void send_sip_request(PurpleConnection *gc,const gchar *method,const gchar *url,const gchar *to,const gchar *addheaders,const gchar *body,struct sip_dialog *dialog,TransCallback tc)
{
  struct simple_account_data *sip = (gc -> proto_data);
  char *callid = (dialog != 0)?g_strdup((dialog -> callid)) : gencallid();
  char *auth = (char *)((void *)0);
  const char *addh = "";
  gchar *branch = genbranch();
  gchar *tag = (gchar *)((void *)0);
  char *buf;
  if (!(strcmp(method,"REGISTER") != 0)) {
    if ((sip -> regcallid) != 0) {
      g_free(callid);
      callid = g_strdup((sip -> regcallid));
    }
    else 
      sip -> regcallid = g_strdup(callid);
  }
  if (addheaders != 0) 
    addh = addheaders;
  if ((sip -> registrar.type != 0) && !(strcmp(method,"REGISTER") != 0)) {
    buf = auth_header(sip,&sip -> registrar,method,url);
    auth = g_strdup_printf("Authorization: %s\r\n",buf);
    g_free(buf);
    purple_debug(PURPLE_DEBUG_MISC,"simple","header %s",auth);
  }
  else if ((sip -> proxy.type != 0) && (strcmp(method,"REGISTER") != 0)) {
    buf = auth_header(sip,&sip -> proxy,method,url);
    auth = g_strdup_printf("Proxy-Authorization: %s\r\n",buf);
    g_free(buf);
    purple_debug(PURPLE_DEBUG_MISC,"simple","header %s",auth);
  }
  if (!(dialog != 0)) 
    tag = gentag();
  buf = g_strdup_printf("%s %s SIP/2.0\r\nVia: SIP/2.0/%s %s:%d;branch=%s\r\nFrom: <sip:%s@%s>;tag=%s;epid=1234567890\r\nTo: <%s>%s%s\r\nMax-Forwards: 10\r\nCSeq: %d %s\r\nUser-Agent: Purple/2.10.9\r\nCall-ID: %s\r\n%s%sContent-Length: %lu\r\n\r\n%s",method,url,(((sip -> udp) != 0)?"UDP" : "TCP"),purple_network_get_my_ip(-1),(sip -> listenport),branch,(sip -> username),(sip -> servername),((dialog != 0)?(dialog -> ourtag) : tag),to,((dialog != 0)?";tag=" : ""),((dialog != 0)?(dialog -> theirtag) : ""),++sip -> cseq,method,callid,((auth != 0)?auth : ""),addh,strlen(body),body);
/* Don't know what epid is, but LCS wants it */
  g_free(tag);
  g_free(auth);
  g_free(branch);
  g_free(callid);
/* add to ongoing transactions */
  transactions_add_buf(sip,buf,tc);
  sendout_pkt(gc,buf);
  g_free(buf);
}

static char *get_contact(struct simple_account_data *sip)
{
  return g_strdup_printf("<sip:%s@%s:%d;transport=%s>;methods=\"MESSAGE, SUBSCRIBE, NOTIFY\"",(sip -> username),purple_network_get_my_ip(-1),(sip -> listenport),(((sip -> udp) != 0)?"udp" : "tcp"));
}

static void do_register_exp(struct simple_account_data *sip,int expire)
{
  char *uri;
  char *to;
  char *contact;
  char *hdr;
  sip -> reregister = ((time(0) + expire) - 50);
  uri = g_strdup_printf("sip:%s",(sip -> servername));
  to = g_strdup_printf("sip:%s@%s",(sip -> username),(sip -> servername));
  contact = get_contact(sip);
  hdr = g_strdup_printf("Contact: %s\r\nExpires: %d\r\n",contact,expire);
  g_free(contact);
  sip -> registerstatus = 1;
  send_sip_request((sip -> gc),"REGISTER",uri,to,hdr,"",0,process_register_response);
  g_free(hdr);
  g_free(uri);
  g_free(to);
}

static void do_register(struct simple_account_data *sip)
{
  do_register_exp(sip,(sip -> registerexpire));
}

static gchar *parse_from(const gchar *hdr)
{
  gchar *from;
  const gchar *tmp;
  const gchar *tmp2 = hdr;
  if (!(hdr != 0)) 
    return 0;
  purple_debug_info("simple","parsing address out of %s\n",hdr);
  tmp = (strchr(hdr,60));
/* i hate the different SIP UA behaviours... */
/* sip address in <...> */
  if (tmp != 0) {
    tmp2 = (tmp + 1);
    tmp = (strchr(tmp2,'>'));
    if (tmp != 0) {
      from = g_strndup(tmp2,(tmp - tmp2));
    }
    else {
      purple_debug_info("simple","found < without > in From\n");
      return 0;
    }
  }
  else {
    tmp = (strchr(tmp2,';'));
    if (tmp != 0) {
      from = g_strndup(tmp2,(tmp - tmp2));
    }
    else {
      from = g_strdup(tmp2);
    }
  }
  purple_debug_info("simple","got %s\n",from);
  return from;
}
static gchar *find_tag(const gchar *);

static gboolean process_subscribe_response(struct simple_account_data *sip,struct sipmsg *msg,struct transaction *tc)
{
  gchar *to = (gchar *)((void *)0);
  struct simple_buddy *b = (struct simple_buddy *)((void *)0);
  gchar *theirtag = (gchar *)((void *)0);
  gchar *ourtag = (gchar *)((void *)0);
  const gchar *callid = (const gchar *)((void *)0);
  purple_debug_info("simple","process subscribe response\n");
  if (((msg -> response) == 200) || ((msg -> response) == 202)) {
    if ((((to = parse_from(sipmsg_find_header(msg,"To"))) != 0) && ((b = (g_hash_table_lookup((sip -> buddies),to))) != 0)) && !((b -> dialog) != 0)) {
      purple_debug_info("simple","creating dialog information for a subscription.\n");
      theirtag = find_tag(sipmsg_find_header(msg,"To"));
      ourtag = find_tag(sipmsg_find_header(msg,"From"));
      callid = sipmsg_find_header(msg,"Call-ID");
      if (((theirtag != 0) && (ourtag != 0)) && (callid != 0)) {
        b -> dialog = ((struct sip_dialog *)(g_malloc0_n(1,(sizeof(struct sip_dialog )))));
        ( *(b -> dialog)).ourtag = g_strdup(ourtag);
        ( *(b -> dialog)).theirtag = g_strdup(theirtag);
        ( *(b -> dialog)).callid = g_strdup(callid);
        purple_debug_info("simple","ourtag: %s\n",ourtag);
        purple_debug_info("simple","theirtag: %s\n",theirtag);
        purple_debug_info("simple","callid: %s\n",callid);
        g_free(theirtag);
        g_free(ourtag);
      }
    }
    else {
      purple_debug_info("simple","cannot create dialog!\n");
    }
    return (!0);
  }
/* cant be NULL since it is our own msg */
  to = parse_from(sipmsg_find_header((tc -> msg),"To"));
/* we can not subscribe -> user is offline (TODO unknown status?) */
  purple_prpl_got_user_status((sip -> account),to,"offline",((void *)((void *)0)));
  g_free(to);
  return (!0);
}

static void simple_subscribe_exp(struct simple_account_data *sip,struct simple_buddy *buddy,int expiration)
{
  gchar *contact;
  gchar *to;
  gchar *tmp;
  gchar *tmp2;
  tmp2 = g_strdup_printf("Expires: %d\r\nAccept: application/pidf+xml, application/xpidf+xml\r\nEvent: presence\r\n",expiration);
  if (strncmp((buddy -> name),"sip:",4) != 0) 
    to = g_strdup_printf("sip:%s",(buddy -> name));
  else 
    to = g_strdup((buddy -> name));
  tmp = get_contact(sip);
  contact = g_strdup_printf("%sContact: %s\r\n",tmp2,tmp);
  g_free(tmp);
  g_free(tmp2);
  send_sip_request((sip -> gc),"SUBSCRIBE",to,to,contact,"",(buddy -> dialog),((expiration > 0)?process_subscribe_response : ((gboolean (*)(struct simple_account_data *, struct sipmsg *, struct transaction *))((void *)0))));
  g_free(to);
  g_free(contact);
/* resubscribe before subscription expires */
/* add some jitter */
  if (expiration > 60) 
    buddy -> resubscribe = ((time(0) + (expiration - 60)) + (rand() % 50));
  else if (expiration > 0) 
    buddy -> resubscribe = (time(0) + (expiration / 2));
}

static void simple_subscribe(struct simple_account_data *sip,struct simple_buddy *buddy)
{
  simple_subscribe_exp(sip,buddy,1200);
}

static void simple_unsubscribe(char *name,struct simple_buddy *buddy,struct simple_account_data *sip)
{
  if ((buddy -> dialog) != 0) {
    purple_debug_info("simple","Unsubscribing from %s\n",name);
    simple_subscribe_exp(sip,buddy,0);
  }
}

static gboolean simple_add_lcs_contacts(struct simple_account_data *sip,struct sipmsg *msg,struct transaction *tc)
{
  const gchar *tmp;
  xmlnode *item;
  xmlnode *group;
  xmlnode *isc;
  const char *name_group;
  PurpleBuddy *b;
  PurpleGroup *g = (PurpleGroup *)((void *)0);
  struct simple_buddy *bs;
  int len = (msg -> bodylen);
  tmp = sipmsg_find_header(msg,"Event");
  if ((tmp != 0) && !(strncmp(tmp,"vnd-microsoft-roaming-contacts",30) != 0)) {
    purple_debug_info("simple","simple_add_lcs_contacts->%s-%d\n",(msg -> body),len);
/*Convert the contact from XML to Purple Buddies*/
    isc = xmlnode_from_str((msg -> body),len);
/* ToDo. Find for all groups */
    if ((group = xmlnode_get_child(isc,"group")) != 0) {
      name_group = xmlnode_get_attrib(group,"name");
      purple_debug_info("simple","name_group->%s\n",name_group);
      g = purple_find_group(name_group);
      if (!(g != 0)) 
        g = purple_group_new(name_group);
    }
    if (!(g != 0)) {
      g = purple_find_group("Buddies");
      if (!(g != 0)) 
        g = purple_group_new("Buddies");
    }
    for (item = xmlnode_get_child(isc,"contact"); item != 0; item = xmlnode_get_next_twin(item)) {
      const char *uri;
      char *buddy_name;
      uri = xmlnode_get_attrib(item,"uri");
/*name = xmlnode_get_attrib(item, "name");
			groups = xmlnode_get_attrib(item, "groups");*/
      purple_debug_info("simple","URI->%s\n",uri);
      buddy_name = g_strdup_printf("sip:%s",uri);
      b = purple_find_buddy((sip -> account),buddy_name);
      if (!(b != 0)) {
        b = purple_buddy_new((sip -> account),buddy_name,uri);
      }
      g_free(buddy_name);
      purple_blist_add_buddy(b,0,g,0);
      purple_blist_alias_buddy(b,uri);
      bs = ((struct simple_buddy *)(g_malloc0_n(1,(sizeof(struct simple_buddy )))));
      bs -> name = g_strdup(purple_buddy_get_name(b));
      g_hash_table_insert((sip -> buddies),(bs -> name),bs);
    }
    xmlnode_free(isc);
  }
  return 0;
}

static void simple_subscribe_buddylist(struct simple_account_data *sip)
{
  gchar *contact = "Event: vnd-microsoft-roaming-contacts\r\nAccept: application/vnd-microsoft-roaming-contacts+xml\r\nSupported: com.microsoft.autoextend\r\nSupported: ms-benotify\r\nProxy-Require: ms-benotify\r\nSupported: ms-piggyback-first-notify\r\n";
  gchar *to;
  gchar *tmp;
  to = g_strdup_printf("sip:%s@%s",(sip -> username),(sip -> servername));
  tmp = get_contact(sip);
  contact = g_strdup_printf("%sContact: %s\r\n",contact,tmp);
  g_free(tmp);
  send_sip_request((sip -> gc),"SUBSCRIBE",to,to,contact,"",0,simple_add_lcs_contacts);
  g_free(to);
  g_free(contact);
}

static void simple_buddy_resub(char *name,struct simple_buddy *buddy,struct simple_account_data *sip)
{
  time_t curtime = time(0);
  purple_debug_info("simple","buddy resub\n");
  if ((buddy -> resubscribe) < curtime) {
    purple_debug(PURPLE_DEBUG_MISC,"simple","simple_buddy_resub %s\n",name);
    simple_subscribe(sip,buddy);
  }
}

static gboolean resend_timeout(struct simple_account_data *sip)
{
  GSList *tmp = (sip -> transactions);
  time_t currtime = time(0);
  while(tmp != 0){
    struct transaction *trans = (tmp -> data);
    tmp = (tmp -> next);
    purple_debug_info("simple","have open transaction age: %lu\n",(currtime - (trans -> time)));
    if (((currtime - (trans -> time)) > 5) && ((trans -> retries) >= 1)) {
/* TODO 408 */
    }
    else {
      if (((currtime - (trans -> time)) > 2) && ((trans -> retries) == 0)) {
        trans -> retries++;
        sendout_sipmsg(sip,(trans -> msg));
      }
    }
  }
  return (!0);
}

static gboolean subscribe_timeout(struct simple_account_data *sip)
{
  GSList *tmp;
  time_t curtime = time(0);
/* register again if first registration expires */
  if ((sip -> reregister) < curtime) {
    do_register(sip);
  }
/* publish status again if our last update is about to expire. */
  if ((((sip -> republish) != (-1)) && ((sip -> republish) < curtime)) && (purple_account_get_bool((sip -> account),"dopublish",(!0)) != 0)) {
    purple_debug_info("simple","subscribe_timeout: republishing status.\n");
    send_open_publish(sip);
  }
/* check for every subscription if we need to resubscribe */
  g_hash_table_foreach((sip -> buddies),((GHFunc )simple_buddy_resub),((gpointer )sip));
/* remove a timed out suscriber */
  tmp = (sip -> watcher);
  while(tmp != 0){
    struct simple_watcher *watcher = (tmp -> data);
    if ((watcher -> expire) < curtime) {
      watcher_remove(sip,(watcher -> name));
      tmp = (sip -> watcher);
    }
    if (tmp != 0) 
      tmp = (tmp -> next);
  }
  return (!0);
}

static void simple_send_message(struct simple_account_data *sip,const char *to,const char *msg,const char *type)
{
  gchar *hdr;
  gchar *fullto;
  if (strncmp(to,"sip:",4) != 0) 
    fullto = g_strdup_printf("sip:%s",to);
  else 
    fullto = g_strdup(to);
  if (type != 0) {
    hdr = g_strdup_printf("Content-Type: %s\r\n",type);
  }
  else {
    hdr = g_strdup("Content-Type: text/plain\r\n");
  }
  send_sip_request((sip -> gc),"MESSAGE",fullto,fullto,hdr,msg,0,0);
  g_free(hdr);
  g_free(fullto);
}

static int simple_im_send(PurpleConnection *gc,const char *who,const char *what,PurpleMessageFlags flags)
{
  struct simple_account_data *sip = (gc -> proto_data);
  char *to = g_strdup(who);
  char *text = purple_unescape_html(what);
  simple_send_message(sip,to,text,0);
  g_free(to);
  g_free(text);
  return 1;
}

static void process_incoming_message(struct simple_account_data *sip,struct sipmsg *msg)
{
  gchar *from;
  const gchar *contenttype;
  gboolean found = 0;
  from = parse_from(sipmsg_find_header(msg,"From"));
  if (!(from != 0)) 
    return ;
  purple_debug(PURPLE_DEBUG_MISC,"simple","got message from %s: %s\n",from,(msg -> body));
  contenttype = sipmsg_find_header(msg,"Content-Type");
  if ((!(contenttype != 0) || !(strncmp(contenttype,"text/plain",10) != 0)) || !(strncmp(contenttype,"text/html",9) != 0)) {
    serv_got_im((sip -> gc),from,(msg -> body),0,time(0));
    send_sip_response((sip -> gc),msg,200,"OK",0);
    found = (!0);
  }
  else if (!(strncmp(contenttype,"application/im-iscomposing+xml",30) != 0)) {
    xmlnode *isc = xmlnode_from_str((msg -> body),(msg -> bodylen));
    xmlnode *state;
    gchar *statedata;
    if (!(isc != 0)) {
      purple_debug_info("simple","process_incoming_message: can not parse iscomposing\n");
      g_free(from);
      return ;
    }
    state = xmlnode_get_child(isc,"state");
    if (!(state != 0)) {
      purple_debug_info("simple","process_incoming_message: no state found\n");
      xmlnode_free(isc);
      g_free(from);
      return ;
    }
    statedata = xmlnode_get_data(state);
    if (statedata != 0) {
      if (strstr(statedata,"active") != 0) 
        serv_got_typing((sip -> gc),from,0,PURPLE_TYPING);
      else 
        serv_got_typing_stopped((sip -> gc),from);
      g_free(statedata);
    }
    xmlnode_free(isc);
    send_sip_response((sip -> gc),msg,200,"OK",0);
    found = (!0);
  }
  if (!(found != 0)) {
    purple_debug_info("simple","got unknown mime-type\n");
    send_sip_response((sip -> gc),msg,415,"Unsupported media type",0);
  }
  g_free(from);
}

static gboolean process_register_response(struct simple_account_data *sip,struct sipmsg *msg,struct transaction *tc)
{
  const gchar *tmp;
  purple_debug(PURPLE_DEBUG_MISC,"simple","in process register response response: %d\n",(msg -> response));
  switch(msg -> response){
    case 200:
{
/* registered */
      if ((sip -> registerstatus) < 3) {
        if (purple_account_get_bool((sip -> account),"dopublish",(!0)) != 0) {
          send_open_publish(sip);
        }
      }
      sip -> registerstatus = 3;
      purple_connection_set_state((sip -> gc),PURPLE_CONNECTED);
/* get buddies from blist */
      simple_get_buddies((sip -> gc));
      subscribe_timeout(sip);
      tmp = sipmsg_find_header(msg,"Allow-Events");
      if ((tmp != 0) && (strstr(tmp,"vnd-microsoft-provisioning") != 0)) {
        simple_subscribe_buddylist(sip);
      }
      break; 
    }
    case 401:
{
      if ((sip -> registerstatus) != 2) {
        purple_debug_info("simple","REGISTER retries %d\n",sip -> registrar.retries);
        if (sip -> registrar.retries > 2) {
          if (!(purple_account_get_remember_password(( *(sip -> gc)).account) != 0)) 
            purple_account_set_password(( *(sip -> gc)).account,0);
          purple_connection_error_reason((sip -> gc),PURPLE_CONNECTION_ERROR_AUTHENTICATION_FAILED,((const char *)(dgettext("pidgin","Incorrect password"))));
          return (!0);
        }
        tmp = sipmsg_find_header(msg,"WWW-Authenticate");
        fill_auth(sip,tmp,&sip -> registrar);
        sip -> registerstatus = 2;
        do_register(sip);
      }
      break; 
    }
    default:
{
      if ((sip -> registerstatus) != 2) {
        purple_debug_info("simple","Unrecognized return code for REGISTER.\n");
        if (sip -> registrar.retries > 2) {
          purple_connection_error_reason((sip -> gc),PURPLE_CONNECTION_ERROR_OTHER_ERROR,((const char *)(dgettext("pidgin","Unknown server response"))));
          return (!0);
        }
        sip -> registerstatus = 2;
        do_register(sip);
      }
      break; 
    }
  }
  return (!0);
}

static gboolean dialog_match(struct sip_dialog *dialog,struct sipmsg *msg)
{
  const gchar *fromhdr;
  const gchar *tohdr;
  const gchar *callid;
  gchar *ourtag;
  gchar *theirtag;
  gboolean match = 0;
  fromhdr = sipmsg_find_header(msg,"From");
  tohdr = sipmsg_find_header(msg,"To");
  callid = sipmsg_find_header(msg,"Call-ID");
  if ((!(fromhdr != 0) || !(tohdr != 0)) || !(callid != 0)) 
    return 0;
  ourtag = find_tag(tohdr);
  theirtag = find_tag(fromhdr);
  if (((((ourtag != 0) && (theirtag != 0)) && !(strcmp((dialog -> callid),callid) != 0)) && !(strcmp((dialog -> ourtag),ourtag) != 0)) && !(strcmp((dialog -> theirtag),theirtag) != 0)) 
    match = (!0);
  g_free(ourtag);
  g_free(theirtag);
  return match;
}

static void process_incoming_notify(struct simple_account_data *sip,struct sipmsg *msg)
{
  gchar *from;
  const gchar *fromhdr;
  gchar *basicstatus_data;
  xmlnode *pidf;
  xmlnode *basicstatus = (xmlnode *)((void *)0);
  xmlnode *tuple;
  xmlnode *status;
  gboolean isonline = 0;
  struct simple_buddy *b = (struct simple_buddy *)((void *)0);
  const gchar *sshdr = (const gchar *)((void *)0);
  fromhdr = sipmsg_find_header(msg,"From");
  from = parse_from(fromhdr);
  if (!(from != 0)) 
    return ;
  b = (g_hash_table_lookup((sip -> buddies),from));
  if (!(b != 0)) {
    g_free(from);
    purple_debug_info("simple","Could not find the buddy.\n");
    return ;
  }
  if (((b -> dialog) != 0) && !(dialog_match((b -> dialog),msg) != 0)) {
/* We only accept notifies from people that
		 * we already have a dialog with.
		 */
    purple_debug_info("simple","No corresponding dialog for notify--discard\n");
    g_free(from);
    return ;
  }
  pidf = xmlnode_from_str((msg -> body),(msg -> bodylen));
  if (!(pidf != 0)) {
    purple_debug_info("simple","process_incoming_notify: no parseable pidf\n");
    sshdr = sipmsg_find_header(msg,"Subscription-State");
    if (sshdr != 0) {
      int i = 0;
      gchar **ssparts = g_strsplit(sshdr,":",0);
{
        while(ssparts[i] != 0){
          g_strchug(ssparts[i]);
          if (purple_str_has_prefix(ssparts[i],"terminated") != 0) {
            purple_debug_info("simple","Subscription expired!");
            if ((b -> dialog) != 0) {
              g_free(( *(b -> dialog)).ourtag);
              g_free(( *(b -> dialog)).theirtag);
              g_free(( *(b -> dialog)).callid);
              g_free((b -> dialog));
              b -> dialog = ((struct sip_dialog *)((void *)0));
            }
            purple_prpl_got_user_status((sip -> account),from,"offline",((void *)((void *)0)));
            break; 
          }
          i++;
        }
      }
      g_strfreev(ssparts);
    }
    send_sip_response((sip -> gc),msg,200,"OK",0);
    g_free(from);
    return ;
  }
  if ((tuple = xmlnode_get_child(pidf,"tuple")) != 0) 
    if ((status = xmlnode_get_child(tuple,"status")) != 0) 
      basicstatus = xmlnode_get_child(status,"basic");
  if (!(basicstatus != 0)) {
    purple_debug_info("simple","process_incoming_notify: no basic found\n");
    xmlnode_free(pidf);
    g_free(from);
    return ;
  }
  basicstatus_data = xmlnode_get_data(basicstatus);
  if (!(basicstatus_data != 0)) {
    purple_debug_info("simple","process_incoming_notify: no basic data found\n");
    xmlnode_free(pidf);
    g_free(from);
    return ;
  }
  if (strstr(basicstatus_data,"open") != 0) 
    isonline = (!0);
  if (isonline != 0) 
    purple_prpl_got_user_status((sip -> account),from,"available",((void *)((void *)0)));
  else 
    purple_prpl_got_user_status((sip -> account),from,"offline",((void *)((void *)0)));
  xmlnode_free(pidf);
  g_free(from);
  g_free(basicstatus_data);
  send_sip_response((sip -> gc),msg,200,"OK",0);
}

static unsigned int simple_typing(PurpleConnection *gc,const char *name,PurpleTypingState state)
{
  struct simple_account_data *sip = (gc -> proto_data);
  gchar *xml = "<\?xml version=\"1.0\" encoding=\"UTF-8\"\?>\n<isComposing xmlns=\"urn:ietf:params:xml:ns:im-iscomposing\"\nxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\nxsi:schemaLocation=\"urn:ietf:params:xml:ns:im-composing iscomposing.xsd\">\n<state>%s</state>\n<contenttype>text/plain</contenttype>\n<refresh>60</refresh>\n</isComposing>";
  gchar *recv = g_strdup(name);
  if (state == PURPLE_TYPING) {
    gchar *msg = g_strdup_printf(xml,"active");
    simple_send_message(sip,recv,msg,"application/im-iscomposing+xml");
    g_free(msg);
/* TODO: Only if (state == PURPLE_TYPED) ? */
  }
  else {
    gchar *msg = g_strdup_printf(xml,"idle");
    simple_send_message(sip,recv,msg,"application/im-iscomposing+xml");
    g_free(msg);
  }
  g_free(recv);
/*
	 * TODO: Is this right?  It will cause the core to call
	 *       serv_send_typing(gc, who, PURPLE_TYPING) once every second
	 *       until the user stops typing.  If that's not desired,
	 *       then return 0 instead.
	 */
  return 1;
}

static gchar *find_tag(const gchar *hdr)
{
  const gchar *tmp = (strstr(hdr,";tag="));
  const gchar *tmp2;
  if (!(tmp != 0)) 
    return 0;
  tmp += 5;
  if ((tmp2 = (strchr(tmp,';'))) != 0) {
    return g_strndup(tmp,(tmp2 - tmp));
  }
  return g_strdup(tmp);
}

static gchar *gen_xpidf(struct simple_account_data *sip)
{
  gchar *doc = g_strdup_printf("<\?xml version=\"1.0\" encoding=\"UTF-8\"\?>\n<presence>\n<presentity uri=\"sip:%s@%s;method=SUBSCRIBE\"/>\n<display name=\"sip:%s@%s\"/>\n<atom id=\"1234\">\n<address uri=\"sip:%s@%s\">\n<status status=\"%s\"/>\n</address>\n</atom>\n</presence>\n",(sip -> username),(sip -> servername),(sip -> username),(sip -> servername),(sip -> username),(sip -> servername),(sip -> status));
  return doc;
}

static gchar *gen_pidf(struct simple_account_data *sip,gboolean open)
{
  gchar *doc = g_strdup_printf("<\?xml version=\"1.0\" encoding=\"UTF-8\"\?>\n<presence xmlns=\"urn:ietf:params:xml:ns:pidf\"\nxmlns:im=\"urn:ietf:params:xml:ns:pidf:im\"\nentity=\"sip:%s@%s\">\n<tuple id=\"bs35r9f\">\n<status>\n<basic>%s</basic>\n</status>\n<note>%s</note>\n</tuple>\n</presence>",(sip -> username),(sip -> servername),((open == !0)?"open" : "closed"),((open == !0)?(sip -> status) : ""));
  return doc;
}

static void send_notify(struct simple_account_data *sip,struct simple_watcher *watcher)
{
  gchar *doc = ((watcher -> needsxpidf) != 0)?gen_xpidf(sip) : gen_pidf(sip,(!0));
  gchar *hdr = ((watcher -> needsxpidf) != 0)?"Event: presence\r\nContent-Type: application/xpidf+xml\r\n" : "Event: presence\r\nContent-Type: application/pidf+xml\r\n";
  send_sip_request((sip -> gc),"NOTIFY",(watcher -> name),(watcher -> name),hdr,doc,&watcher -> dialog,0);
  g_free(doc);
}

static gboolean process_publish_response(struct simple_account_data *sip,struct sipmsg *msg,struct transaction *tc)
{
  const gchar *etag = (const gchar *)((void *)0);
  if (((msg -> response) != 200) && ((msg -> response) != 408)) {
/* never send again */
    sip -> republish = (-1);
  }
  etag = sipmsg_find_header(msg,"SIP-Etag");
  if (etag != 0) {
/* we must store the etag somewhere. */
    g_free((sip -> publish_etag));
    sip -> publish_etag = g_strdup(etag);
  }
  return (!0);
}

static void send_open_publish(struct simple_account_data *sip)
{
  gchar *add_headers = (gchar *)((void *)0);
  gchar *uri = g_strdup_printf("sip:%s@%s",(sip -> username),(sip -> servername));
  gchar *doc = gen_pidf(sip,(!0));
  add_headers = g_strdup_printf("%s%s%s%s%d\r\n%s",(((sip -> publish_etag) != 0)?"SIP-If-Match: " : ""),(((sip -> publish_etag) != 0)?(sip -> publish_etag) : ""),(((sip -> publish_etag) != 0)?"\r\n" : ""),"Expires: ",600,"Event: presence\r\nContent-Type: application/pidf+xml\r\n");
  send_sip_request((sip -> gc),"PUBLISH",uri,uri,add_headers,doc,0,process_publish_response);
  sip -> republish = ((time(0) + 600) - 50);
  g_free(uri);
  g_free(doc);
  g_free(add_headers);
}

static void send_closed_publish(struct simple_account_data *sip)
{
  gchar *uri = g_strdup_printf("sip:%s@%s",(sip -> username),(sip -> servername));
  gchar *add_headers;
  gchar *doc;
  add_headers = g_strdup_printf("%s%s%s%s",(((sip -> publish_etag) != 0)?"SIP-If-Match: " : ""),(((sip -> publish_etag) != 0)?(sip -> publish_etag) : ""),(((sip -> publish_etag) != 0)?"\r\n" : ""),"Expires: 600\r\nEvent: presence\r\nContent-Type: application/pidf+xml\r\n");
  doc = gen_pidf(sip,0);
  send_sip_request((sip -> gc),"PUBLISH",uri,uri,add_headers,doc,0,process_publish_response);
/*sip->republish = time(NULL) + 500;*/
  g_free(uri);
  g_free(doc);
  g_free(add_headers);
}

static void process_incoming_subscribe(struct simple_account_data *sip,struct sipmsg *msg)
{
  const char *from_hdr = sipmsg_find_header(msg,"From");
  gchar *from = parse_from(from_hdr);
  gchar *theirtag = find_tag(from_hdr);
  gchar *ourtag = find_tag(sipmsg_find_header(msg,"To"));
  gboolean tagadded = 0;
  const gchar *callid = sipmsg_find_header(msg,"Call-ID");
  const gchar *expire = sipmsg_find_header(msg,"Expire");
  gchar *tmp;
  struct simple_watcher *watcher = watcher_find(sip,from);
  if (!(ourtag != 0)) {
    tagadded = (!0);
    ourtag = gentag();
  }
/* new subscription */
  if (!(watcher != 0)) {
    const gchar *acceptheader = sipmsg_find_header(msg,"Accept");
    gboolean needsxpidf = 0;
    if (!(purple_privacy_check((sip -> account),from) != 0)) {
      send_sip_response((sip -> gc),msg,202,"Ok",0);
      goto privend;
    }
    if (acceptheader != 0) {
      const gchar *tmp = acceptheader;
      gboolean foundpidf = 0;
      gboolean foundxpidf = 0;
      while((tmp != 0) && (tmp < (acceptheader + strlen(acceptheader)))){
        gchar *tmp2 = strchr(tmp,',');
        if (tmp2 != 0) 
           *tmp2 = 0;
        if (!(g_ascii_strcasecmp("application/pidf+xml",tmp) != 0)) 
          foundpidf = (!0);
        if (!(g_ascii_strcasecmp("application/xpidf+xml",tmp) != 0)) 
          foundxpidf = (!0);
        if (tmp2 != 0) {
           *tmp2 = ',';
          tmp = (tmp2 + 1);
          while(( *tmp) == 32)
            tmp++;
        }
        else 
          tmp = 0;
      }
      if (!(foundpidf != 0) && (foundxpidf != 0)) 
        needsxpidf = (!0);
    }
    watcher = watcher_create(sip,from,callid,ourtag,theirtag,needsxpidf);
  }
  if (tagadded != 0) {
    gchar *to = g_strdup_printf("%s;tag=%s",sipmsg_find_header(msg,"To"),ourtag);
    sipmsg_remove_header(msg,"To");
    sipmsg_add_header(msg,"To",to);
    g_free(to);
  }
  if (expire != 0) 
    watcher -> expire = (time(0) + strtol(expire,0,10));
  else 
    watcher -> expire = (time(0) + 600);
  sipmsg_remove_header(msg,"Contact");
  tmp = get_contact(sip);
  sipmsg_add_header(msg,"Contact",tmp);
  g_free(tmp);
  purple_debug_info("simple","got subscribe: name %s ourtag %s theirtag %s callid %s\n",(watcher -> name),watcher -> dialog.ourtag,watcher -> dialog.theirtag,watcher -> dialog.callid);
  send_sip_response((sip -> gc),msg,200,"Ok",0);
  send_notify(sip,watcher);
  privend:
  g_free(from);
  g_free(theirtag);
  g_free(ourtag);
}

static void process_input_message(struct simple_account_data *sip,struct sipmsg *msg)
{
  gboolean found = 0;
/* request */
  if ((msg -> response) == 0) {
    if (!(strcmp((msg -> method),"MESSAGE") != 0)) {
      process_incoming_message(sip,msg);
      found = (!0);
    }
    else if (!(strcmp((msg -> method),"NOTIFY") != 0)) {
      process_incoming_notify(sip,msg);
      found = (!0);
    }
    else if (!(strcmp((msg -> method),"SUBSCRIBE") != 0)) {
      process_incoming_subscribe(sip,msg);
      found = (!0);
    }
    else {
      send_sip_response((sip -> gc),msg,501,"Not implemented",0);
    }
/* response */
  }
  else {
    struct transaction *trans = transactions_find(sip,msg);
    if (trans != 0) {
      if ((msg -> response) == 407) {
        gchar *resend;
        gchar *auth;
        const gchar *ptmp;
        if (sip -> proxy.retries > 3) 
          return ;
        sip -> proxy.retries++;
/* do proxy authentication */
        ptmp = sipmsg_find_header(msg,"Proxy-Authenticate");
        fill_auth(sip,ptmp,&sip -> proxy);
        auth = auth_header(sip,&sip -> proxy,( *(trans -> msg)).method,( *(trans -> msg)).target);
        sipmsg_remove_header((trans -> msg),"Proxy-Authorization");
        sipmsg_add_header((trans -> msg),"Proxy-Authorization",auth);
        g_free(auth);
        resend = sipmsg_to_string((trans -> msg));
/* resend request */
        sendout_pkt((sip -> gc),resend);
        g_free(resend);
      }
      else {
        if ((msg -> response) == 100) {
/* ignore provisional response */
          purple_debug_info("simple","got trying response\n");
        }
        else {
          sip -> proxy.retries = 0;
          if (!(strcmp(( *(trans -> msg)).method,"REGISTER") != 0)) {
/* This is encountered when a REGISTER request was ...
						 */
            if ((msg -> response) == 401) {
/* denied until further authentication was provided. */
              sip -> registrar.retries++;
            }
            else if ((msg -> response) != 200) {
/* denied for some other reason! */
              sip -> registrar.retries++;
            }
            else {
/* accepted! */
              sip -> registrar.retries = 0;
            }
          }
          else {
            if ((msg -> response) == 401) {
/* This is encountered when a generic (MESSAGE, NOTIFY, etc)
							 * was denied until further authorization is provided.
							 */
              gchar *resend;
              gchar *auth;
              const gchar *ptmp;
              if (sip -> registrar.retries > 2) 
                return ;
              sip -> registrar.retries++;
              ptmp = sipmsg_find_header(msg,"WWW-Authenticate");
              fill_auth(sip,ptmp,&sip -> registrar);
              auth = auth_header(sip,&sip -> registrar,( *(trans -> msg)).method,( *(trans -> msg)).target);
              sipmsg_remove_header((trans -> msg),"Authorization");
              sipmsg_add_header((trans -> msg),"Authorization",auth);
              g_free(auth);
              resend = sipmsg_to_string((trans -> msg));
/* resend request */
              sendout_pkt((sip -> gc),resend);
              g_free(resend);
            }
            else {
/* Reset any count of retries that may have
							 * accumulated in the above branch.
							 */
              sip -> registrar.retries = 0;
            }
          }
          if ((trans -> callback) != 0) {
/* call the callback to process response*/
            ( *(trans -> callback))(sip,msg,trans);
          }
          transactions_remove(sip,trans);
        }
      }
      found = (!0);
    }
    else {
      purple_debug(PURPLE_DEBUG_MISC,"simple","received response to unknown transaction");
    }
  }
  if (!(found != 0)) {
    purple_debug(PURPLE_DEBUG_MISC,"simple","received a unknown sip message with method %s and response %d\n",(msg -> method),(msg -> response));
  }
}

static void process_input(struct simple_account_data *sip,struct sip_connection *conn)
{
  char *cur;
  char *dummy;
  struct sipmsg *msg;
  int restlen;
  cur = (conn -> inbuf);
/* according to the RFC remove CRLF at the beginning */
  while((( *cur) == 13) || (( *cur) == 10)){
    cur++;
  }
  if (cur != (conn -> inbuf)) {
    memmove((conn -> inbuf),cur,((conn -> inbufused) - (cur - (conn -> inbuf))));
    conn -> inbufused = (strlen((conn -> inbuf)));
  }
/* Received a full Header? */
  if ((cur = strstr((conn -> inbuf),"\r\n\r\n")) != ((char *)((void *)0))) {
    time_t currtime = time(0);
    cur += 2;
    cur[0] = 0;
    purple_debug_info("simple","\n\nreceived - %s\n######\n%s\n#######\n\n",ctime((&currtime)),(conn -> inbuf));
    msg = sipmsg_parse_header((conn -> inbuf));
    if (!(msg != 0)) {
/* Should we re-use this error message (from lower in the function)? */
      purple_debug_misc("simple","received a incomplete sip msg: %s\n",(conn -> inbuf));
      return ;
    }
    cur[0] = 13;
    cur += 2;
    restlen = ((conn -> inbufused) - (cur - (conn -> inbuf)));
    if (restlen >= (msg -> bodylen)) {
      dummy = ((char *)(g_malloc_n(((msg -> bodylen) + 1),(sizeof(char )))));
      memcpy(dummy,cur,(msg -> bodylen));
      dummy[msg -> bodylen] = 0;
      msg -> body = dummy;
      cur += (msg -> bodylen);
      memmove((conn -> inbuf),cur,((conn -> inbuflen) - (cur - (conn -> inbuf))));
      conn -> inbufused = (strlen((conn -> inbuf)));
    }
    else {
      sipmsg_free(msg);
      return ;
    }
    purple_debug(PURPLE_DEBUG_MISC,"simple","in process response response: %d\n",(msg -> response));
    process_input_message(sip,msg);
    sipmsg_free(msg);
  }
  else {
    purple_debug(PURPLE_DEBUG_MISC,"simple","received a incomplete sip msg: %s\n",(conn -> inbuf));
  }
}

static void simple_udp_process(gpointer data,gint source,PurpleInputCondition con)
{
  PurpleConnection *gc = data;
  struct simple_account_data *sip = (gc -> proto_data);
  struct sipmsg *msg;
  int len;
  time_t currtime = time(0);
  static char buffer[65536UL];
  if ((len = (recv(source,buffer,(sizeof(buffer) - 1),0))) > 0) {
    buffer[len] = 0;
    purple_debug_info("simple","\n\nreceived - %s\n######\n%s\n#######\n\n",ctime((&currtime)),buffer);
    msg = sipmsg_parse_msg(buffer);
    if (msg != 0) {
      process_input_message(sip,msg);
      sipmsg_free(msg);
    }
  }
}

static void simple_input_cb(gpointer data,gint source,PurpleInputCondition cond)
{
  PurpleConnection *gc = data;
  struct simple_account_data *sip = (gc -> proto_data);
  int len;
  struct sip_connection *conn = connection_find(sip,source);
  if (!(conn != 0)) {
    purple_debug_error("simple","Connection not found!\n");
    return ;
  }
  if ((conn -> inbuflen) < ((conn -> inbufused) + 1024)) {
    conn -> inbuflen += 1024;
    conn -> inbuf = (g_realloc((conn -> inbuf),(conn -> inbuflen)));
  }
  len = (read(source,((conn -> inbuf) + (conn -> inbufused)),(1024 - 1)));
  if ((len < 0) && ( *__errno_location() == 11)) 
    return ;
  else if (len <= 0) {
    purple_debug_info("simple","simple_input_cb: read error\n");
    connection_remove(sip,source);
    if ((sip -> fd) == source) 
      sip -> fd = -1;
    return ;
  }
  gc -> last_received = time(0);
  conn -> inbufused += len;
  (conn -> inbuf)[conn -> inbufused] = 0;
  process_input(sip,conn);
}
/* Callback for new connections on incoming TCP port */

static void simple_newconn_cb(gpointer data,gint source,PurpleInputCondition cond)
{
  PurpleConnection *gc = data;
  struct simple_account_data *sip = (gc -> proto_data);
  struct sip_connection *conn;
  int newfd;
  int flags;
  newfd = accept(source,0,0);
  flags = fcntl(newfd,3);
  fcntl(newfd,4,(flags | 04000));
#ifndef _WIN32
  fcntl(newfd,2,1);
#endif
  conn = connection_create(sip,newfd);
  conn -> inputhandler = (purple_input_add(newfd,PURPLE_INPUT_READ,simple_input_cb,gc));
}

static void login_cb(gpointer data,gint source,const gchar *error_message)
{
  PurpleConnection *gc = data;
  struct simple_account_data *sip;
  struct sip_connection *conn;
  if (source < 0) {
    gchar *tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to connect: %s"))),error_message);
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,tmp);
    g_free(tmp);
    return ;
  }
  sip = (gc -> proto_data);
  sip -> fd = source;
  conn = connection_create(sip,source);
  sip -> registertimeout = purple_timeout_add(((rand() % 100) + 10 * 1000),((GSourceFunc )subscribe_timeout),sip);
  do_register(sip);
  conn -> inputhandler = (purple_input_add((sip -> fd),PURPLE_INPUT_READ,simple_input_cb,gc));
}

static guint simple_ht_hash_nick(const char *nick)
{
  char *lc = g_utf8_strdown(nick,(-1));
  guint bucket = g_str_hash(lc);
  g_free(lc);
  return bucket;
}

static gboolean simple_ht_equals_nick(const char *nick1,const char *nick2)
{
  return purple_utf8_strcasecmp(nick1,nick2) == 0;
}

static void simple_udp_host_resolved_listen_cb(int listenfd,gpointer data)
{
  struct simple_account_data *sip = (struct simple_account_data *)data;
  sip -> listen_data = ((PurpleNetworkListenData *)((void *)0));
  if (listenfd == -1) {
    purple_connection_error_reason((sip -> gc),PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Unable to create listen socket"))));
    return ;
  }
/*
	 * TODO: Is it correct to set sip->fd to the listenfd?  For the TCP
	 *       listener we set sip->listenfd, but maybe UDP is different?
	 *       Maybe we use the same fd for outgoing data or something?
	 */
  sip -> fd = listenfd;
  sip -> listenport = (purple_network_get_port_from_fd((sip -> fd)));
  sip -> listenpa = (purple_input_add((sip -> fd),PURPLE_INPUT_READ,simple_udp_process,(sip -> gc)));
  sip -> resendtimeout = purple_timeout_add(2500,((GSourceFunc )resend_timeout),sip);
  sip -> registertimeout = purple_timeout_add(((rand() % 100) + 10 * 1000),((GSourceFunc )subscribe_timeout),sip);
  do_register(sip);
}

static void simple_udp_host_resolved(GSList *hosts,gpointer data,const char *error_message)
{
  struct simple_account_data *sip = (struct simple_account_data *)data;
  int addr_size;
  sip -> query_data = ((PurpleDnsQueryData *)((void *)0));
  if (!(hosts != 0) || !((hosts -> data) != 0)) {
    purple_connection_error_reason((sip -> gc),PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Unable to resolve hostname"))));
    return ;
  }
  addr_size = ((gint )((glong )(hosts -> data)));
  hosts = g_slist_remove(hosts,(hosts -> data));
  memcpy((&sip -> serveraddr),(hosts -> data),addr_size);
  g_free((hosts -> data));
  hosts = g_slist_remove(hosts,(hosts -> data));
  while(hosts != 0){
    hosts = g_slist_remove(hosts,(hosts -> data));
    g_free((hosts -> data));
    hosts = g_slist_remove(hosts,(hosts -> data));
  }
/* create socket for incoming connections */
  sip -> listen_data = purple_network_listen_range(5060,5160,SOCK_DGRAM,simple_udp_host_resolved_listen_cb,sip);
  if ((sip -> listen_data) == ((PurpleNetworkListenData *)((void *)0))) {
    purple_connection_error_reason((sip -> gc),PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Unable to create listen socket"))));
    return ;
  }
}

static void simple_tcp_connect_listen_cb(int listenfd,gpointer data)
{
  struct simple_account_data *sip = (struct simple_account_data *)data;
  sip -> listen_data = ((PurpleNetworkListenData *)((void *)0));
  sip -> listenfd = listenfd;
  if ((sip -> listenfd) == -1) {
    purple_connection_error_reason((sip -> gc),PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Unable to create listen socket"))));
    return ;
  }
  purple_debug_info("simple","listenfd: %d\n",(sip -> listenfd));
  sip -> listenport = (purple_network_get_port_from_fd((sip -> listenfd)));
  sip -> listenpa = (purple_input_add((sip -> listenfd),PURPLE_INPUT_READ,simple_newconn_cb,(sip -> gc)));
  purple_debug_info("simple","connecting to %s port %d\n",(sip -> realhostname),(sip -> realport));
/* open tcp connection to the server */
  if (purple_proxy_connect((sip -> gc),(sip -> account),(sip -> realhostname),(sip -> realport),login_cb,(sip -> gc)) == ((PurpleProxyConnectData *)((void *)0))) {
    purple_connection_error_reason((sip -> gc),PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Unable to connect"))));
  }
}

static void srvresolved(PurpleSrvResponse *resp,int results,gpointer data)
{
  struct simple_account_data *sip;
  gchar *hostname;
  int port;
  sip = data;
  sip -> srv_query_data = ((PurpleSrvTxtQueryData *)((void *)0));
  port = purple_account_get_int((sip -> account),"port",0);
/* find the host to connect to */
  if (results != 0) {
    hostname = g_strdup((resp -> hostname));
    if (!(port != 0)) 
      port = (resp -> port);
    g_free(resp);
  }
  else {
    if (!(purple_account_get_bool((sip -> account),"useproxy",0) != 0)) {
      hostname = g_strdup((sip -> servername));
    }
    else {
      hostname = g_strdup(purple_account_get_string((sip -> account),"proxy",(sip -> servername)));
    }
  }
  sip -> realhostname = hostname;
  sip -> realport = port;
  if (!((sip -> realport) != 0)) 
    sip -> realport = 5060;
/* TCP case */
  if (!((sip -> udp) != 0)) {
/* create socket for incoming connections */
    sip -> listen_data = purple_network_listen_range(5060,5160,SOCK_STREAM,simple_tcp_connect_listen_cb,sip);
    if ((sip -> listen_data) == ((PurpleNetworkListenData *)((void *)0))) {
      purple_connection_error_reason((sip -> gc),PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Unable to create listen socket"))));
      return ;
    }
/* UDP */
  }
  else {
    purple_debug_info("simple","using udp with server %s and port %d\n",hostname,port);
    sip -> query_data = purple_dnsquery_a_account((sip -> account),hostname,port,simple_udp_host_resolved,sip);
    if ((sip -> query_data) == ((PurpleDnsQueryData *)((void *)0))) {
      purple_connection_error_reason((sip -> gc),PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Unable to resolve hostname"))));
    }
  }
}

static void simple_login(PurpleAccount *account)
{
  PurpleConnection *gc;
  struct simple_account_data *sip;
  gchar **userserver;
  const gchar *hosttoconnect;
  const char *username = purple_account_get_username(account);
  gc = purple_account_get_connection(account);
  if (strpbrk(username," \t\v\r\n") != ((char *)((void *)0))) {
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_INVALID_SETTINGS,((const char *)(dgettext("pidgin","SIP usernames may not contain whitespaces or @ symbols"))));
    return ;
  }
  gc -> proto_data = (sip = ((struct simple_account_data *)(g_malloc0_n(1,(sizeof(struct simple_account_data ))))));
  sip -> gc = gc;
  sip -> fd = -1;
  sip -> listenfd = -1;
  sip -> account = account;
  sip -> registerexpire = 900;
  sip -> udp = purple_account_get_bool(account,"udp",0);
/* TODO: is there a good default grow size? */
  if (!((sip -> udp) != 0)) 
    sip -> txbuf = purple_circ_buffer_new(0);
  userserver = g_strsplit(username,"@",2);
  if ((userserver[1] == ((gchar *)((void *)0))) || (userserver[1][0] == 0)) {
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_INVALID_SETTINGS,((const char *)(dgettext("pidgin","SIP connect server not specified"))));
    return ;
  }
  purple_connection_set_display_name(gc,userserver[0]);
  sip -> username = g_strdup(userserver[0]);
  sip -> servername = g_strdup(userserver[1]);
  sip -> password = g_strdup(purple_connection_get_password(gc));
  g_strfreev(userserver);
  sip -> buddies = g_hash_table_new(((GHashFunc )simple_ht_hash_nick),((GEqualFunc )simple_ht_equals_nick));
  purple_connection_update_progress(gc,((const char *)(dgettext("pidgin","Connecting"))),1,2);
/* TODO: Set the status correctly. */
  sip -> status = g_strdup("available");
  if (!(purple_account_get_bool(account,"useproxy",0) != 0)) {
    hosttoconnect = (sip -> servername);
  }
  else {
    hosttoconnect = purple_account_get_string(account,"proxy",(sip -> servername));
  }
  sip -> srv_query_data = purple_srv_resolve_account(account,"sip",((((sip -> udp) != 0)?"udp" : "tcp")),hosttoconnect,srvresolved,sip);
}

static void simple_close(PurpleConnection *gc)
{
  struct simple_account_data *sip = (gc -> proto_data);
  if (!(sip != 0)) 
    return ;
/* unregister */
  if ((sip -> registerstatus) == 3) {
    g_hash_table_foreach((sip -> buddies),((GHFunc )simple_unsubscribe),((gpointer )sip));
    if (purple_account_get_bool((sip -> account),"dopublish",(!0)) != 0) 
      send_closed_publish(sip);
    do_register_exp(sip,0);
  }
  connection_free_all(sip);
  if ((sip -> listenpa) != 0) 
    purple_input_remove((sip -> listenpa));
  if ((sip -> tx_handler) != 0U) 
    purple_input_remove((sip -> tx_handler));
  if ((sip -> resendtimeout) != 0U) 
    purple_timeout_remove((sip -> resendtimeout));
  if ((sip -> registertimeout) != 0U) 
    purple_timeout_remove((sip -> registertimeout));
  if ((sip -> query_data) != ((PurpleDnsQueryData *)((void *)0))) 
    purple_dnsquery_destroy((sip -> query_data));
  if ((sip -> srv_query_data) != ((PurpleSrvTxtQueryData *)((void *)0))) 
    purple_srv_cancel((sip -> srv_query_data));
  if ((sip -> listen_data) != ((PurpleNetworkListenData *)((void *)0))) 
    purple_network_listen_cancel((sip -> listen_data));
  if ((sip -> fd) >= 0) 
    close((sip -> fd));
  if ((sip -> listenfd) >= 0) 
    close((sip -> listenfd));
  g_free((sip -> servername));
  g_free((sip -> username));
  g_free((sip -> password));
  g_free(sip -> registrar.nonce);
  g_free(sip -> registrar.opaque);
  g_free(sip -> registrar.target);
  g_free(sip -> registrar.realm);
  g_free(sip -> registrar.digest_session_key);
  g_free(sip -> proxy.nonce);
  g_free(sip -> proxy.opaque);
  g_free(sip -> proxy.target);
  g_free(sip -> proxy.realm);
  g_free(sip -> proxy.digest_session_key);
  g_free((sip -> status));
  g_hash_table_destroy((sip -> buddies));
  g_free((sip -> regcallid));
  while((sip -> transactions) != 0)
    transactions_remove(sip,( *(sip -> transactions)).data);
  g_free((sip -> publish_etag));
  if ((sip -> txbuf) != 0) 
    purple_circ_buffer_destroy((sip -> txbuf));
  g_free((sip -> realhostname));
  g_free(sip);
  gc -> proto_data = ((void *)((void *)0));
}
static PurplePluginProtocolInfo prpl_info = {(0), ((GList *)((void *)0)), ((GList *)((void *)0)), 
/* user_splits */
/* protocol_options */
/* icon_spec */
{((char *)((void *)0)), (0), (0), (0), (0), (0), (0)}, (simple_list_icon), ((const char *(*)(PurpleBuddy *))((void *)0)), ((char *(*)(PurpleBuddy *))((void *)0)), ((void (*)(PurpleBuddy *, PurpleNotifyUserInfo *, gboolean ))((void *)0)), (simple_status_types), ((GList *(*)(PurpleBlistNode *))((void *)0)), ((GList *(*)(PurpleConnection *))((void *)0)), ((GHashTable *(*)(PurpleConnection *, const char *))((void *)0)), (simple_login), (simple_close), (simple_im_send), ((void (*)(PurpleConnection *, const char *))((void *)0)), (simple_typing), ((void (*)(PurpleConnection *, const char *))((void *)0)), (simple_set_status), ((void (*)(PurpleConnection *, int ))((void *)0)), ((void (*)(PurpleConnection *, const char *, const char *))((void *)0)), (simple_add_buddy), ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)), (simple_remove_buddy), ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *))((void *)0)), ((void (*)(PurpleConnection *, GHashTable *))((void *)0)), ((void (*)(PurpleConnection *, GHashTable *))((void *)0)), ((char *(*)(GHashTable *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *, const char *))((void *)0)), ((void (*)(PurpleConnection *, int ))((void *)0)), ((void (*)(PurpleConnection *, int , const char *, const char *))((void *)0)), ((int (*)(PurpleConnection *, int , const char *, PurpleMessageFlags ))((void *)0)), (simple_keep_alive), ((void (*)(PurpleAccount *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *, const char *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *, PurpleGroup *, GList *))((void *)0)), ((void (*)(PurpleBuddy *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((const char *(*)(const PurpleAccount *, const char *))((void *)0)), ((void (*)(PurpleConnection *, PurpleStoredImage *))((void *)0)), ((void (*)(PurpleConnection *, PurpleGroup *))((void *)0)), ((char *(*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((PurpleChat *(*)(PurpleAccount *, const char *))((void *)0)), ((PurpleRoomlist *(*)(PurpleConnection *))((void *)0)), ((void (*)(PurpleRoomlist *))((void *)0)), ((void (*)(PurpleRoomlist *, PurpleRoomlistRoom *))((void *)0)), ((gboolean (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *, const char *))((void *)0)), ((PurpleXfer *(*)(PurpleConnection *, const char *))((void *)0)), ((gboolean (*)(const PurpleBuddy *))((void *)0)), ((PurpleWhiteboardPrplOps *)((void *)0)), (simple_send_raw), ((char *(*)(PurpleRoomlistRoom *))((void *)0)), ((void (*)(PurpleAccount *, PurpleAccountUnregistrationCb , void *))((void *)0)), ((gboolean (*)(PurpleConnection *, const char *, guint ))((void *)0)), ((GList *(*)(PurpleAccount *))((void *)0)), ((sizeof(PurplePluginProtocolInfo ))), ((GHashTable *(*)(PurpleAccount *))((void *)0)), ((gboolean (*)(PurpleAccount *, const char *, PurpleMediaSessionType ))((void *)0)), ((PurpleMediaCaps (*)(PurpleAccount *, const char *))((void *)0)), ((PurpleMood *(*)(PurpleAccount *))((void *)0)), ((void (*)(PurpleConnection *, const char *, PurpleSetPublicAliasSuccessCallback , PurpleSetPublicAliasFailureCallback ))((void *)0)), ((void (*)(PurpleConnection *, PurpleGetPublicAliasSuccessCallback , PurpleGetPublicAliasFailureCallback ))((void *)0)), ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *, const char *))((void *)0)), ((void (*)(PurpleConnection *, GList *, GList *, const char *))((void *)0))
/* list_icon */
/* list_emblems */
/* status_text */
/* tooltip_text */
/* away_states */
/* blist_node_menu */
/* chat_info */
/* chat_info_defaults */
/* login */
/* close */
/* send_im */
/* set_info */
/* send_typing */
/* get_info */
/* set_status */
/* set_idle */
/* change_passwd */
/* add_buddy */
/* add_buddies */
/* remove_buddy */
/* remove_buddies */
/* add_permit */
/* add_deny */
/* rem_permit */
/* rem_deny */
/* set_permit_deny */
/* join_chat */
/* reject_chat */
/* get_chat_name */
/* chat_invite */
/* chat_leave */
/* chat_whisper */
/* chat_send */
/* keepalive */
/* register_user */
/* get_cb_info */
/* get_cb_away */
/* alias_buddy */
/* group_buddy */
/* rename_group */
/* buddy_free */
/* convo_closed */
/* normalize */
/* set_buddy_icon */
/* remove_group */
/* get_cb_real_name */
/* set_chat_topic */
/* find_blist_chat */
/* roomlist_get_list */
/* roomlist_cancel */
/* roomlist_expand_category */
/* can_receive_file */
/* send_file */
/* new_xfer */
/* offline_message */
/* whiteboard_prpl_ops */
/* send_raw */
/* roomlist_room_serialize */
/* unregister_user */
/* send_attention */
/* get_attention_types */
/* struct_size */
/* get_account_text_table */
/* initiate_media */
/* get_media_caps */
/* get_moods */
/* set_public_alias */
/* get_public_alias */
/* add_buddy_with_invite */
/* add_buddies_with_invite */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_PROTOCOL), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("prpl-simple"), ("SIMPLE"), ("2.10.9"), ("SIP/SIMPLE Protocol Plugin"), ("The SIP/SIMPLE Protocol Plugin"), ("Thomas Butter <butter@uni-mannheim.de>"), ("http://pidgin.im/"), ((gboolean (*)(PurplePlugin *))((void *)0)), ((gboolean (*)(PurplePlugin *))((void *)0)), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((&prpl_info)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/* padding */
};

static void _init_plugin(PurplePlugin *plugin)
{
  PurpleAccountUserSplit *split;
  PurpleAccountOption *option;
  split = purple_account_user_split_new(((const char *)(dgettext("pidgin","Server"))),"",64);
  prpl_info.user_splits = g_list_append(prpl_info.user_splits,split);
  option = purple_account_option_bool_new(((const char *)(dgettext("pidgin","Publish status (note: everyone may watch you)"))),"dopublish",(!0));
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_int_new(((const char *)(dgettext("pidgin","Connect port"))),"port",0);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_bool_new(((const char *)(dgettext("pidgin","Use UDP"))),"udp",0);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_bool_new(((const char *)(dgettext("pidgin","Use proxy"))),"useproxy",0);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","Proxy"))),"proxy","");
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","Auth User"))),"authuser","");
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","Auth Domain"))),"authdomain","");
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  _init_plugin(plugin);
  return purple_plugin_register(plugin);
}
