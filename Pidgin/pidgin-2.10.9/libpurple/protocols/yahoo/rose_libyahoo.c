/*
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include <account.h>
#include <core.h>
#include "libymsg.h"
#include "yahoochat.h"
#include "yahoo_aliases.h"
#include "yahoo_doodle.h"
#include "yahoo_filexfer.h"
#include "yahoo_picture.h"
static PurplePlugin *my_protocol = (PurplePlugin *)((void *)0);

static void yahoo_register_commands()
{
  purple_cmd_register("join","s",PURPLE_CMD_P_PRPL,(PURPLE_CMD_FLAG_IM | PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_PRPL_ONLY),"prpl-yahoo",yahoopurple_cmd_chat_join,((const char *)(dgettext("pidgin","join &lt;room&gt;:  Join a chat room on the Yahoo network"))),0);
  purple_cmd_register("list","",PURPLE_CMD_P_PRPL,(PURPLE_CMD_FLAG_IM | PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_PRPL_ONLY),"prpl-yahoo",yahoopurple_cmd_chat_list,((const char *)(dgettext("pidgin","list: List rooms on the Yahoo network"))),0);
  purple_cmd_register("buzz","",PURPLE_CMD_P_PRPL,(PURPLE_CMD_FLAG_IM | PURPLE_CMD_FLAG_PRPL_ONLY),"prpl-yahoo",yahoopurple_cmd_buzz,((const char *)(dgettext("pidgin","buzz: Buzz a user to get their attention"))),0);
  purple_cmd_register("doodle","",PURPLE_CMD_P_PRPL,(PURPLE_CMD_FLAG_IM | PURPLE_CMD_FLAG_PRPL_ONLY),"prpl-yahoo",yahoo_doodle_purple_cmd_start,((const char *)(dgettext("pidgin","doodle: Request user to start a Doodle session"))),0);
}

static PurpleAccount *find_acct(const char *prpl,const char *acct_id)
{
  PurpleAccount *acct = (PurpleAccount *)((void *)0);
/* If we have a specific acct, use it */
  if (acct_id != 0) {
    acct = purple_accounts_find(acct_id,prpl);
    if ((acct != 0) && !(purple_account_is_connected(acct) != 0)) 
      acct = ((PurpleAccount *)((void *)0));
/* Otherwise find an active account for the protocol */
  }
  else {
    GList *l = purple_accounts_get_all();
{
      while(l != 0){
        if (!(strcmp(prpl,purple_account_get_protocol_id((l -> data))) != 0) && (purple_account_is_connected((l -> data)) != 0)) {
          acct = (l -> data);
          break; 
        }
        l = (l -> next);
      }
    }
  }
  return acct;
}
/* This may not be the best way to do this, but we find the first key w/o a value
 * and assume it is the buddy name */

static void yahoo_find_uri_novalue_param(gpointer key,gpointer value,gpointer user_data)
{
  char **retval = user_data;
  if ((value == ((void *)((void *)0))) && ( *retval == ((char *)((void *)0)))) {
     *retval = key;
  }
}

static gboolean yahoo_uri_handler(const char *proto,const char *cmd,GHashTable *params)
{
  char *acct_id = (g_hash_table_lookup(params,"account"));
  PurpleAccount *acct;
  if (g_ascii_strcasecmp(proto,"ymsgr") != 0) 
    return 0;
  acct = find_acct(purple_plugin_get_id(my_protocol),acct_id);
  if (!(acct != 0)) 
    return 0;
/* ymsgr:SendIM?screename&m=The+Message */
  if (!(g_ascii_strcasecmp(cmd,"SendIM") != 0)) {
    char *sname = (char *)((void *)0);
    g_hash_table_foreach(params,yahoo_find_uri_novalue_param,(&sname));
    if (sname != 0) {
      char *message = (g_hash_table_lookup(params,"m"));
      PurpleConversation *conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,sname,acct);
      if (conv == ((PurpleConversation *)((void *)0))) 
        conv = purple_conversation_new(PURPLE_CONV_TYPE_IM,acct,sname);
      purple_conversation_present(conv);
      if (message != 0) {
/* Spaces are encoded as '+' */
        g_strdelimit(message,"+",32);
        purple_conv_send_confirm(conv,message);
      }
    }
/* else
			**If pidgindialogs_im() was in the core, we could use it here.
			 * It is all purple_request_* based, but I'm not sure it really belongs in the core
			pidgindialogs_im(); */
    return (!0);
  }
  else 
/* ymsgr:Chat?roomname */
if (!(g_ascii_strcasecmp(cmd,"Chat") != 0)) {
    char *rname = (char *)((void *)0);
    g_hash_table_foreach(params,yahoo_find_uri_novalue_param,(&rname));
    if (rname != 0) {
/* This is somewhat hacky, but the params aren't useful after this command */
      g_hash_table_insert(params,(g_strdup("room")),(g_strdup(rname)));
      g_hash_table_insert(params,(g_strdup("type")),(g_strdup("Chat")));
      serv_join_chat(purple_account_get_connection(acct),params);
    }
/* else
			** Same as above (except that this would have to be re-written using purple_request_*)
			pidgin_blist_joinchat_show(); */
    return (!0);
  }
  else 
/* ymsgr:AddFriend?name */
if (!(g_ascii_strcasecmp(cmd,"AddFriend") != 0)) {
    char *name = (char *)((void *)0);
    g_hash_table_foreach(params,yahoo_find_uri_novalue_param,(&name));
    purple_blist_request_add_buddy(acct,name,0,0);
    return (!0);
  }
  return 0;
}

static GHashTable *yahoo_get_account_text_table(PurpleAccount *account)
{
  GHashTable *table;
  table = g_hash_table_new(g_str_hash,g_str_equal);
  g_hash_table_insert(table,"login_label",((gpointer )((const char *)(dgettext("pidgin","Yahoo ID...")))));
  return table;
}

static gboolean yahoo_unload_plugin(PurplePlugin *plugin)
{
  yahoo_dest_colorht();
  return (!0);
}
static PurpleWhiteboardPrplOps yahoo_whiteboard_prpl_ops = {(yahoo_doodle_start), (yahoo_doodle_end), (yahoo_doodle_get_dimensions), ((void (*)(PurpleWhiteboard *, int , int ))((void *)0)), (yahoo_doodle_get_brush), (yahoo_doodle_set_brush), (yahoo_doodle_send_draw_list), (yahoo_doodle_clear), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* padding */
};
static PurplePluginProtocolInfo prpl_info = {((OPT_PROTO_MAIL_CHECK | OPT_PROTO_CHAT_TOPIC)), ((GList *)((void *)0)), ((GList *)((void *)0)), 
/* user_splits */
/* protocol_options */
{("png,gif,jpeg"), (96), (96), (96), (96), (0), (PURPLE_ICON_SCALE_SEND)}, (yahoo_list_icon), (yahoo_list_emblem), (yahoo_status_text), (yahoo_tooltip_text), (yahoo_status_types), (yahoo_blist_node_menu), (yahoo_c_info), (yahoo_c_info_defaults), (yahoo_login), (yahoo_close), (yahoo_send_im), ((void (*)(PurpleConnection *, const char *))((void *)0)), (yahoo_send_typing), (yahoo_get_info), (yahoo_set_status), (yahoo_set_idle), ((void (*)(PurpleConnection *, const char *, const char *))((void *)0)), (yahoo_add_buddy), ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)), (yahoo_remove_buddy), ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), (yahoo_add_deny), ((void (*)(PurpleConnection *, const char *))((void *)0)), (yahoo_rem_deny), (yahoo_set_permit_deny), (yahoo_c_join), ((void (*)(PurpleConnection *, GHashTable *))((void *)0)), (yahoo_get_chat_name), (yahoo_c_invite), (yahoo_c_leave), ((void (*)(PurpleConnection *, int , const char *, const char *))((void *)0)), (yahoo_c_send), (yahoo_keepalive), ((void (*)(PurpleAccount *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), (yahoo_update_alias), (yahoo_change_buddys_group), (yahoo_rename_group), ((void (*)(PurpleBuddy *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), (purple_normalize_nocase), (yahoo_set_buddy_icon), ((void (*)(PurpleConnection *, PurpleGroup *))((void *)0)), ((char *(*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((PurpleChat *(*)(PurpleAccount *, const char *))((void *)0)), (yahoo_roomlist_get_list), (yahoo_roomlist_cancel), (yahoo_roomlist_expand_category), (yahoo_can_receive_file), (yahoo_send_file), (yahoo_new_xfer), (yahoo_offline_message), (&yahoo_whiteboard_prpl_ops), ((int (*)(PurpleConnection *, const char *, int ))((void *)0)), ((char *(*)(PurpleRoomlistRoom *))((void *)0)), ((void (*)(PurpleAccount *, PurpleAccountUnregistrationCb , void *))((void *)0)), (yahoo_send_attention), (yahoo_attention_types), ((sizeof(PurplePluginProtocolInfo ))), (yahoo_get_account_text_table), ((gboolean (*)(PurpleAccount *, const char *, PurpleMediaSessionType ))((void *)0)), ((PurpleMediaCaps (*)(PurpleAccount *, const char *))((void *)0)), ((PurpleMood *(*)(PurpleAccount *))((void *)0)), ((void (*)(PurpleConnection *, const char *, PurpleSetPublicAliasSuccessCallback , PurpleSetPublicAliasFailureCallback ))((void *)0)), ((void (*)(PurpleConnection *, PurpleGetPublicAliasSuccessCallback , PurpleGetPublicAliasFailureCallback ))((void *)0)), ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *, const char *))((void *)0)), ((void (*)(PurpleConnection *, GList *, GList *, const char *))((void *)0))
/* set info */
/* change_passwd*/
/* add_buddies */
/* remove_buddies */
/* add_permit */
/* rem_permit */
/* reject chat invite */
/* chat whisper */
/* register_user */
/* get_cb_info */
/* get_cb_away */
/* alias_buddy */
/* buddy_free */
/* convo_closed */
/* normalize */
/* void (*remove_group)(PurpleConnection *gc, const char *group);*/
/* char *(*get_cb_real_name)(PurpleConnection *gc, int id, const char *who); */
/* set_chat_topic */
/* find_blist_chat */
/* can_receive_file */
/* offline_message */
/* send_raw */
/* roomlist_room_serialize */
/* unregister_user */
/* struct_size */
/* get_account_text_table */
/* initiate_media */
/* get_media_caps */
/* get_moods */
/* set_public_alias */
/* get_public_alias */
/* add_buddy_with_invite */
/* add_buddies_with_invite */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_PROTOCOL), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("prpl-yahoo"), ("Yahoo"), ("2.10.9"), ("Yahoo! Protocol Plugin"), ("Yahoo! Protocol Plugin"), ((char *)((void *)0)), ("http://pidgin.im/"), ((gboolean (*)(PurplePlugin *))((void *)0)), (yahoo_unload_plugin), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((&prpl_info)), ((PurplePluginUiInfo *)((void *)0)), (yahoo_actions), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
  PurpleAccountOption *option;
  option = purple_account_option_int_new(((const char *)(dgettext("pidgin","Pager port"))),"port",5050);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","File transfer server"))),"xfer_host","filetransfer.msg.yahoo.com");
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_int_new(((const char *)(dgettext("pidgin","File transfer port"))),"xfer_port",80);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","Chat room locale"))),"room_list_locale","us");
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","Encoding"))),"local_charset","UTF-8");
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_bool_new(((const char *)(dgettext("pidgin","Ignore conference and chatroom invitations"))),"ignore_invites",0);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_bool_new(((const char *)(dgettext("pidgin","Use account proxy for HTTP and HTTPS connections"))),"proxy_ssl",0);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
#if 0
#endif
  my_protocol = plugin;
  yahoo_register_commands();
  yahoo_init_colorht();
  purple_signal_connect((purple_get_core()),"uri-handler",plugin,((PurpleCallback )yahoo_uri_handler),0);
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
