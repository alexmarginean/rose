/*
 * @file yahoo_filexfer.c Yahoo Filetransfer
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "dnsquery.h"
#include "prpl.h"
#include "util.h"
#include "debug.h"
#include "network.h"
#include "notify.h"
#include "proxy.h"
#include "ft.h"
#include "libymsg.h"
#include "yahoo_packet.h"
#include "yahoo_filexfer.h"
#include "yahoo_doodle.h"
#include "yahoo_friend.h"

struct yahoo_xfer_data 
{
  gchar *host;
  gchar *path;
  int port;
  PurpleConnection *gc;
  long expires;
  gboolean started;
  gchar *txbuf;
  gsize txbuflen;
  gsize txbuf_written;
  guint tx_handler;
  gchar *rxqueue;
  guint rxlen;
  gchar *xfer_peer_idstring;
  gchar *xfer_idstring_for_relay;
/* 0 for old, 15 for Y7(YMSG 15) */
  int version;
  int info_val_249;
  enum __unnamed_enum___F0_L57_C2_STARTED__COMMA__HEAD_REQUESTED__COMMA__HEAD_REPLY_RECEIVED__COMMA__TRANSFER_PHASE__COMMA__ACCEPTED__COMMA__P2P_HEAD_REQUESTED__COMMA__P2P_HEAD_REPLIED__COMMA__P2P_GET_REQUESTED {STARTED,HEAD_REQUESTED,HEAD_REPLY_RECEIVED,TRANSFER_PHASE,ACCEPTED,P2P_HEAD_REQUESTED,P2P_HEAD_REPLIED,P2P_GET_REQUESTED}status_15;
/* contains all filenames, in case of multiple transfers, with the first
	 * one in the list being the current file's name (ymsg15) */
  GSList *filename_list;
/* corresponds to filename_list, with size as **STRING** */
  GSList *size_list;
  gboolean firstoflist;
/* url of the file, used when we are p2p server */
  gchar *xfer_url;
  int yahoo_local_p2p_ft_server_fd;
  int yahoo_local_p2p_ft_server_port;
  int yahoo_p2p_ft_server_watcher;
  int input_event;
}
;

static void yahoo_xfer_data_free(struct yahoo_xfer_data *xd)
{
  PurpleConnection *gc;
  YahooData *yd;
  PurpleXfer *xfer;
  GSList *l;
  gc = (xd -> gc);
  yd = (gc -> proto_data);
/* remove entry from map */
  if ((xd -> xfer_peer_idstring) != 0) {
    xfer = (g_hash_table_lookup((yd -> xfer_peer_idstring_map),(xd -> xfer_peer_idstring)));
    if (xfer != 0) 
      g_hash_table_remove((yd -> xfer_peer_idstring_map),(xd -> xfer_peer_idstring));
  }
/* empty file & filesize list */
  for (l = (xd -> filename_list); l != 0; l = (l -> next)) {
    g_free((l -> data));
    l -> data = ((gpointer )((void *)0));
  }
  for (l = (xd -> size_list); l != 0; l = (l -> next)) {
    g_free((l -> data));
    l -> data = ((gpointer )((void *)0));
  }
  g_slist_free((xd -> filename_list));
  g_slist_free((xd -> size_list));
  g_free((xd -> host));
  g_free((xd -> path));
  g_free((xd -> txbuf));
  g_free((xd -> xfer_peer_idstring));
  g_free((xd -> xfer_idstring_for_relay));
  if ((xd -> tx_handler) != 0U) 
    purple_input_remove((xd -> tx_handler));
  g_free(xd);
}

static void yahoo_receivefile_send_cb(gpointer data,gint source,PurpleInputCondition condition)
{
  PurpleXfer *xfer;
  struct yahoo_xfer_data *xd;
  int remaining;
  int written;
  xfer = data;
  xd = (xfer -> data);
  remaining = ((xd -> txbuflen) - (xd -> txbuf_written));
  written = (write((xfer -> fd),((xd -> txbuf) + (xd -> txbuf_written)),remaining));
  if ((written < 0) && ( *__errno_location() == 11)) 
    written = 0;
  else if (written <= 0) {
    purple_debug_error("yahoo","Unable to write in order to start ft errno = %d\n", *__errno_location());
    purple_xfer_cancel_remote(xfer);
    return ;
  }
  if (written < remaining) {
    xd -> txbuf_written += written;
    return ;
  }
  purple_input_remove((xd -> tx_handler));
  xd -> tx_handler = 0;
  g_free((xd -> txbuf));
  xd -> txbuf = ((gchar *)((void *)0));
  xd -> txbuflen = 0;
  purple_xfer_start(xfer,source,0,0);
}

static void yahoo_receivefile_connected(gpointer data,gint source,const gchar *error_message)
{
  PurpleXfer *xfer;
  struct yahoo_xfer_data *xd;
  purple_debug_info("yahoo","in yahoo_receivefile_connected\n");
  if (!((xfer = data) != 0)) 
    return ;
  if (!((xd = (xfer -> data)) != 0)) 
    return ;
  if (((source < 0) || ((xd -> path) == ((gchar *)((void *)0)))) || ((xd -> host) == ((gchar *)((void *)0)))) {
    purple_xfer_error(PURPLE_XFER_RECEIVE,purple_xfer_get_account(xfer),(xfer -> who),((const char *)(dgettext("pidgin","Unable to connect."))));
    purple_xfer_cancel_remote(xfer);
    return ;
  }
  xfer -> fd = source;
/* The first time we get here, assemble the tx buffer */
  if ((xd -> txbuflen) == 0) {
    xd -> txbuf = g_strdup_printf("GET /%s HTTP/1.0\r\nHost: %s\r\n\r\n",(xd -> path),(xd -> host));
    xd -> txbuflen = strlen((xd -> txbuf));
    xd -> txbuf_written = 0;
  }
  if (!((xd -> tx_handler) != 0U)) {
    xd -> tx_handler = purple_input_add(source,PURPLE_INPUT_WRITE,yahoo_receivefile_send_cb,xfer);
    yahoo_receivefile_send_cb(xfer,source,PURPLE_INPUT_WRITE);
  }
}

static void yahoo_sendfile_send_cb(gpointer data,gint source,PurpleInputCondition condition)
{
  PurpleXfer *xfer;
  struct yahoo_xfer_data *xd;
  int written;
  int remaining;
  xfer = data;
  xd = (xfer -> data);
  remaining = ((xd -> txbuflen) - (xd -> txbuf_written));
  written = (write((xfer -> fd),((xd -> txbuf) + (xd -> txbuf_written)),remaining));
  if ((written < 0) && ( *__errno_location() == 11)) 
    written = 0;
  else if (written <= 0) {
    purple_debug_error("yahoo","Unable to write in order to start ft errno = %d\n", *__errno_location());
    purple_xfer_cancel_remote(xfer);
    return ;
  }
  if (written < remaining) {
    xd -> txbuf_written += written;
    return ;
  }
  purple_input_remove((xd -> tx_handler));
  xd -> tx_handler = 0;
  g_free((xd -> txbuf));
  xd -> txbuf = ((gchar *)((void *)0));
  xd -> txbuflen = 0;
  purple_xfer_start(xfer,source,0,0);
}

static void yahoo_sendfile_connected(gpointer data,gint source,const gchar *error_message)
{
  PurpleXfer *xfer;
  struct yahoo_xfer_data *xd;
  struct yahoo_packet *pkt;
  gchar *size;
  gchar *filename;
  gchar *encoded_filename;
  gchar *header;
  guchar *pkt_buf;
  const char *host;
  int port;
  size_t content_length;
  size_t header_len;
  size_t pkt_buf_len;
  PurpleConnection *gc;
  PurpleAccount *account;
  YahooData *yd;
  purple_debug_info("yahoo","in yahoo_sendfile_connected\n");
  if (!((xfer = data) != 0)) 
    return ;
  if (!((xd = (xfer -> data)) != 0)) 
    return ;
  if (source < 0) {
    purple_xfer_error(PURPLE_XFER_RECEIVE,purple_xfer_get_account(xfer),(xfer -> who),((const char *)(dgettext("pidgin","Unable to connect."))));
    purple_xfer_cancel_remote(xfer);
    return ;
  }
  xfer -> fd = source;
/* Assemble the tx buffer */
  gc = (xd -> gc);
  account = purple_connection_get_account(gc);
  yd = (gc -> proto_data);
  pkt = yahoo_packet_new(YAHOO_SERVICE_FILETRANSFER,YAHOO_STATUS_AVAILABLE,(yd -> session_id));
  size = g_strdup_printf("%lu",purple_xfer_get_size(xfer));
  filename = g_path_get_basename(purple_xfer_get_local_filename(xfer));
  encoded_filename = yahoo_string_encode(gc,filename,0);
  yahoo_packet_hash(pkt,"sssss",0,purple_connection_get_display_name(gc),5,(xfer -> who),14,"",27,encoded_filename,28,size);
  g_free(size);
  g_free(encoded_filename);
  g_free(filename);
  content_length = ((4 + 2 + 2 + 2 + 2 + 4 + 4) + yahoo_packet_length(pkt));
  pkt_buf_len = yahoo_packet_build(pkt,4,0,(yd -> jp),&pkt_buf);
  yahoo_packet_free(pkt);
  host = purple_account_get_string(account,"xfer_host","filetransfer.msg.yahoo.com");
  port = purple_account_get_int(account,"xfer_port",80);
  header = g_strdup_printf("POST http://%s:%d/notifyft HTTP/1.0\r\nContent-length: %lu\r\nHost: %s:%d\r\nCookie: Y=%s; T=%s\r\n\r\n",host,port,((content_length + 4) + purple_xfer_get_size(xfer)),host,port,(yd -> cookie_y),(yd -> cookie_t));
  header_len = strlen(header);
  xd -> txbuflen = ((header_len + pkt_buf_len) + 4);
  xd -> txbuf = (g_malloc((xd -> txbuflen)));
  memcpy((xd -> txbuf),header,header_len);
  g_free(header);
  memcpy(((xd -> txbuf) + header_len),pkt_buf,pkt_buf_len);
  g_free(pkt_buf);
  memcpy((((xd -> txbuf) + header_len) + pkt_buf_len),"29\300\200",4);
  xd -> txbuf_written = 0;
  if ((xd -> tx_handler) == 0) {
    xd -> tx_handler = purple_input_add(source,PURPLE_INPUT_WRITE,yahoo_sendfile_send_cb,xfer);
    yahoo_sendfile_send_cb(xfer,source,PURPLE_INPUT_WRITE);
  }
}

static void yahoo_xfer_init(PurpleXfer *xfer)
{
  struct yahoo_xfer_data *xfer_data;
  PurpleConnection *gc;
  PurpleAccount *account;
  YahooData *yd;
  xfer_data = (xfer -> data);
  gc = (xfer_data -> gc);
  yd = (gc -> proto_data);
  account = purple_connection_get_account(gc);
  if ((purple_xfer_get_type(xfer)) == PURPLE_XFER_SEND) {
    if ((yd -> jp) != 0) {
      if (purple_proxy_connect(gc,account,purple_account_get_string(account,"xferjp_host","filetransfer.msg.yahoo.co.jp"),purple_account_get_int(account,"xfer_port",80),yahoo_sendfile_connected,xfer) == ((PurpleProxyConnectData *)((void *)0))) {
        purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","File Transfer Failed"))),((const char *)(dgettext("pidgin","Unable to establish file descriptor."))),0,0);
        purple_xfer_cancel_remote(xfer);
      }
    }
    else {
      if (purple_proxy_connect(gc,account,purple_account_get_string(account,"xfer_host","filetransfer.msg.yahoo.com"),purple_account_get_int(account,"xfer_port",80),yahoo_sendfile_connected,xfer) == ((PurpleProxyConnectData *)((void *)0))) {
        purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","File Transfer Failed"))),((const char *)(dgettext("pidgin","Unable to establish file descriptor."))),0,0);
        purple_xfer_cancel_remote(xfer);
      }
    }
  }
  else {
    xfer -> fd = -1;
    if (purple_proxy_connect(gc,account,(xfer_data -> host),(xfer_data -> port),yahoo_receivefile_connected,xfer) == ((PurpleProxyConnectData *)((void *)0))) {
      purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","File Transfer Failed"))),((const char *)(dgettext("pidgin","Unable to establish file descriptor."))),0,0);
      purple_xfer_cancel_remote(xfer);
    }
  }
}

static void yahoo_xfer_init_15(PurpleXfer *xfer)
{
  struct yahoo_xfer_data *xfer_data;
  PurpleConnection *gc;
  PurpleAccount *account;
  YahooData *yd;
  struct yahoo_packet *pkt;
  xfer_data = (xfer -> data);
  gc = (xfer_data -> gc);
  yd = (gc -> proto_data);
  account = purple_connection_get_account(gc);
  if ((purple_xfer_get_type(xfer)) == PURPLE_XFER_SEND) {
    gchar *filename;
    filename = g_path_get_basename(purple_xfer_get_local_filename(xfer));
    pkt = yahoo_packet_new(YAHOO_SERVICE_FILETRANS_15,YAHOO_STATUS_AVAILABLE,(yd -> session_id));
    yahoo_packet_hash(pkt,"sssiiiisiii",1,purple_normalize(account,purple_account_get_username(account)),5,(xfer -> who),265,(xfer_data -> xfer_peer_idstring),222,1,266,1,302,268,300,268,27,filename,28,(xfer -> size),301,268,303,268);
    g_free(filename);
  }
  else {
    if ((xfer_data -> firstoflist) == !0) {
      pkt = yahoo_packet_new(YAHOO_SERVICE_FILETRANS_15,YAHOO_STATUS_AVAILABLE,(yd -> session_id));
      yahoo_packet_hash(pkt,"sssi",1,purple_normalize(account,purple_account_get_username(account)),5,(xfer -> who),265,(xfer_data -> xfer_peer_idstring),222,3);
    }
    else {
      pkt = yahoo_packet_new(YAHOO_SERVICE_FILETRANS_ACC_15,YAHOO_STATUS_AVAILABLE,(yd -> session_id));
      yahoo_packet_hash(pkt,"sssi",1,purple_normalize(account,purple_account_get_username(account)),5,(xfer -> who),265,(xfer_data -> xfer_peer_idstring),271,1);
    }
  }
  yahoo_packet_send_and_free(pkt,yd);
}

static void yahoo_xfer_start(PurpleXfer *xfer)
{
/* We don't need to do anything here, do we? */
}

static guint calculate_length(const gchar *l,size_t len)
{
  int i;
  for (i = 0; i < len; i++) {
    if (!((g_ascii_table[(guchar )l[i]] & G_ASCII_DIGIT) != 0)) 
      continue; 
    return (strtol((l + i),0,10));
  }
  return 0;
}

static gssize yahoo_xfer_read(guchar **buffer,PurpleXfer *xfer)
{
  gchar buf[4096UL];
  gssize len;
  gchar *start = (gchar *)((void *)0);
  gchar *length;
  gchar *end;
  int filelen;
  struct yahoo_xfer_data *xd = (xfer -> data);
  if ((purple_xfer_get_type(xfer)) != PURPLE_XFER_RECEIVE) {
    return 0;
  }
  len = read((xfer -> fd),buf,(sizeof(buf)));
  if (len <= 0) {
    if ((purple_xfer_get_size(xfer) > 0) && (purple_xfer_get_bytes_sent(xfer) >= purple_xfer_get_size(xfer))) {
      purple_xfer_set_completed(xfer,(!0));
      return 0;
    }
    else 
      return (-1);
  }
  if (!((xd -> started) != 0)) {
    xd -> rxqueue = (g_realloc((xd -> rxqueue),(len + (xd -> rxlen))));
    memcpy(((xd -> rxqueue) + (xd -> rxlen)),buf,len);
    xd -> rxlen += len;
    length = g_strstr_len((xd -> rxqueue),len,"Content-length:");
/* some proxies re-write this header, changing the capitalization :(
		 * technically that's allowed since headers are case-insensitive
		 * [RFC 2616, section 4.2] */
    if (length == ((gchar *)((void *)0))) 
      length = g_strstr_len((xd -> rxqueue),len,"Content-Length:");
    if (length != 0) {
      end = g_strstr_len(length,(length - (xd -> rxqueue)),"\r\n");
      if (!(end != 0)) 
        return 0;
      if ((filelen = (calculate_length(length,(len - (length - (xd -> rxqueue)))))) != 0) 
        purple_xfer_set_size(xfer,filelen);
    }
    start = g_strstr_len((xd -> rxqueue),len,"\r\n\r\n");
    if (start != 0) 
      start += 4;
    if (!(start != 0) || (start > ((xd -> rxqueue) + len))) 
      return 0;
    xd -> started = (!0);
    len -= (start - (xd -> rxqueue));
     *buffer = (g_malloc(len));
    memcpy(( *buffer),start,len);
    g_free((xd -> rxqueue));
    xd -> rxqueue = ((gchar *)((void *)0));
    xd -> rxlen = 0;
  }
  else {
     *buffer = (g_malloc(len));
    memcpy(( *buffer),buf,len);
  }
  return len;
}

static gssize yahoo_xfer_write(const guchar *buffer,size_t size,PurpleXfer *xfer)
{
  gssize len;
  struct yahoo_xfer_data *xd = (xfer -> data);
  if (!(xd != 0)) 
    return (-1);
  if ((purple_xfer_get_type(xfer)) != PURPLE_XFER_SEND) {
    return (-1);
  }
  len = write((xfer -> fd),buffer,size);
  if (len == (-1)) {
    if (purple_xfer_get_bytes_sent(xfer) >= purple_xfer_get_size(xfer)) 
      purple_xfer_set_completed(xfer,(!0));
    if (( *__errno_location() != 11) && ( *__errno_location() != 4)) 
      return (-1);
    return 0;
  }
  return len;
}

static void yahoo_xfer_cancel_send(PurpleXfer *xfer)
{
  struct yahoo_xfer_data *xfer_data;
  xfer_data = (xfer -> data);
  if (((purple_xfer_get_status(xfer)) == PURPLE_XFER_STATUS_CANCEL_LOCAL) && ((xfer_data -> version) == 15)) {
    PurpleConnection *gc;
    PurpleAccount *account;
    YahooData *yd;
    struct yahoo_packet *pkt;
    gc = (xfer_data -> gc);
    yd = (gc -> proto_data);
    account = purple_connection_get_account(gc);
/* hack to see if file trans acc/info packet has been received */
    if ((xfer_data -> xfer_idstring_for_relay) != 0) {
      pkt = yahoo_packet_new(YAHOO_SERVICE_FILETRANS_INFO_15,YAHOO_STATUS_DISCONNECTED,(yd -> session_id));
      yahoo_packet_hash(pkt,"sssi",1,purple_normalize(account,purple_account_get_username(account)),5,(xfer -> who),265,(xfer_data -> xfer_peer_idstring),66,-1);
    }
    else {
      pkt = yahoo_packet_new(YAHOO_SERVICE_FILETRANS_15,YAHOO_STATUS_AVAILABLE,(yd -> session_id));
      yahoo_packet_hash(pkt,"sssi",1,purple_normalize(account,purple_account_get_username(account)),5,(xfer -> who),265,(xfer_data -> xfer_peer_idstring),222,2);
    }
    yahoo_packet_send_and_free(pkt,yd);
  }
  if (xfer_data != 0) 
    yahoo_xfer_data_free(xfer_data);
  xfer -> data = ((void *)((void *)0));
}

static void yahoo_xfer_cancel_recv(PurpleXfer *xfer)
{
  struct yahoo_xfer_data *xfer_data;
  xfer_data = (xfer -> data);
  if (((purple_xfer_get_status(xfer)) == PURPLE_XFER_STATUS_CANCEL_LOCAL) && ((xfer_data -> version) == 15)) {
    PurpleConnection *gc;
    PurpleAccount *account;
    YahooData *yd;
    struct yahoo_packet *pkt;
    gc = (xfer_data -> gc);
    yd = (gc -> proto_data);
    account = purple_connection_get_account(gc);
/* hack to see if file trans acc/info packet has been received */
    if (!((xfer_data -> xfer_idstring_for_relay) != 0)) {
      pkt = yahoo_packet_new(YAHOO_SERVICE_FILETRANS_15,YAHOO_STATUS_AVAILABLE,(yd -> session_id));
      yahoo_packet_hash(pkt,"sssi",1,purple_normalize(account,purple_account_get_username(account)),5,(xfer -> who),265,(xfer_data -> xfer_peer_idstring),222,4);
    }
    else {
      pkt = yahoo_packet_new(YAHOO_SERVICE_FILETRANS_15,YAHOO_STATUS_DISCONNECTED,(yd -> session_id));
      yahoo_packet_hash(pkt,"sssi",1,purple_normalize(account,purple_account_get_username(account)),5,(xfer -> who),265,(xfer_data -> xfer_peer_idstring),66,-1);
    }
    yahoo_packet_send_and_free(pkt,yd);
  }
  if (xfer_data != 0) 
    yahoo_xfer_data_free(xfer_data);
  xfer -> data = ((void *)((void *)0));
}
/* Send HTTP OK after receiving file */

static void yahoo_p2p_ft_server_send_OK(PurpleXfer *xfer)
{
  char *tx = (char *)((void *)0);
  int written;
  tx = g_strdup_printf("HTTP/1.1 200 OK\r\nContent-Length: 0\r\nContent-Type: application/octet-stream\r\nConnection: close\r\n\r\n");
  written = (write((xfer -> fd),tx,strlen(tx)));
  if ((written < 0) && ( *__errno_location() == 11)) 
    written = 0;
  else if (written <= 0) 
    purple_debug_info("yahoo","p2p filetransfer: Unable to write HTTP OK");
/* close connection */
  close((xfer -> fd));
  xfer -> fd = -1;
  g_free(tx);
}

static void yahoo_xfer_end(PurpleXfer *xfer_old)
{
  struct yahoo_xfer_data *xfer_data;
  PurpleXfer *xfer = (PurpleXfer *)((void *)0);
  PurpleConnection *gc;
  YahooData *yd;
  xfer_data = (xfer_old -> data);
  if ((((xfer_data != 0) && ((xfer_data -> version) == 15)) && ((purple_xfer_get_type(xfer_old)) == PURPLE_XFER_RECEIVE)) && ((xfer_data -> filename_list) != 0)) {
/* Send HTTP OK in case of p2p transfer, when we act as server */
    if ((((xfer_data -> xfer_url) != ((gchar *)((void *)0))) && ((xfer_old -> fd) >= 0)) && ((purple_xfer_get_status(xfer_old)) == PURPLE_XFER_STATUS_DONE)) 
      yahoo_p2p_ft_server_send_OK(xfer_old);
/* removing top of filename & size list completely */
    g_free(( *(xfer_data -> filename_list)).data);
    g_free(( *(xfer_data -> size_list)).data);
    ( *(xfer_data -> filename_list)).data = ((gpointer )((void *)0));
    ( *(xfer_data -> size_list)).data = ((gpointer )((void *)0));
    xfer_data -> filename_list = g_slist_delete_link((xfer_data -> filename_list),(xfer_data -> filename_list));
    xfer_data -> size_list = g_slist_delete_link((xfer_data -> size_list),(xfer_data -> size_list));
/* if there are still more files */
    if ((xfer_data -> filename_list) != 0) {
      gchar *filename;
      long filesize;
      filename = ( *(xfer_data -> filename_list)).data;
      filesize = atol(( *(xfer_data -> size_list)).data);
      gc = (xfer_data -> gc);
      yd = (gc -> proto_data);
/* setting up xfer_data for next file's tranfer */
      g_free((xfer_data -> host));
      g_free((xfer_data -> path));
      g_free((xfer_data -> txbuf));
      g_free((xfer_data -> rxqueue));
      g_free((xfer_data -> xfer_idstring_for_relay));
      if ((xfer_data -> tx_handler) != 0U) 
        purple_input_remove((xfer_data -> tx_handler));
      xfer_data -> host = ((gchar *)((void *)0));
      xfer_data -> host = ((gchar *)((void *)0));
      xfer_data -> port = 0;
      xfer_data -> expires = 0;
      xfer_data -> started = 0;
      xfer_data -> txbuf = ((gchar *)((void *)0));
      xfer_data -> txbuflen = 0;
      xfer_data -> txbuf_written = 0;
      xfer_data -> tx_handler = 0;
      xfer_data -> rxqueue = ((gchar *)((void *)0));
      xfer_data -> rxlen = 0;
      xfer_data -> xfer_idstring_for_relay = ((gchar *)((void *)0));
      xfer_data -> info_val_249 = 0;
      xfer_data -> status_15 = STARTED;
      xfer_data -> firstoflist = 0;
/* Dereference xfer_data from old xfer */
      xfer_old -> data = ((void *)((void *)0));
/* Build the file transfer handle. */
      xfer = purple_xfer_new((gc -> account),PURPLE_XFER_RECEIVE,(xfer_old -> who));
      if (xfer != 0) {
/* Set the info about the incoming file. */
        char *utf8_filename = yahoo_string_decode(gc,filename,(!0));
        purple_xfer_set_filename(xfer,utf8_filename);
        g_free(utf8_filename);
        purple_xfer_set_size(xfer,filesize);
        xfer -> data = xfer_data;
/* Setup our I/O op functions */
        purple_xfer_set_init_fnc(xfer,yahoo_xfer_init_15);
        purple_xfer_set_start_fnc(xfer,yahoo_xfer_start);
        purple_xfer_set_end_fnc(xfer,yahoo_xfer_end);
        purple_xfer_set_cancel_send_fnc(xfer,yahoo_xfer_cancel_send);
        purple_xfer_set_cancel_recv_fnc(xfer,yahoo_xfer_cancel_recv);
        purple_xfer_set_read_fnc(xfer,yahoo_xfer_read);
        purple_xfer_set_write_fnc(xfer,yahoo_xfer_write);
        purple_xfer_set_request_denied_fnc(xfer,yahoo_xfer_cancel_recv);
/* update map to current xfer */
        g_hash_table_remove((yd -> xfer_peer_idstring_map),(xfer_data -> xfer_peer_idstring));
        g_hash_table_insert((yd -> xfer_peer_idstring_map),(xfer_data -> xfer_peer_idstring),xfer);
/* Now perform the request */
        purple_xfer_request(xfer);
      }
      return ;
    }
  }
  if (xfer_data != 0) 
    yahoo_xfer_data_free(xfer_data);
  xfer_old -> data = ((void *)((void *)0));
}

void yahoo_process_p2pfilexfer(PurpleConnection *gc,struct yahoo_packet *pkt)
{
  GSList *l = (pkt -> hash);
  char *me = (char *)((void *)0);
  char *from = (char *)((void *)0);
  char *service = (char *)((void *)0);
  char *message = (char *)((void *)0);
  char *command = (char *)((void *)0);
  char *imv = (char *)((void *)0);
  char *unknown = (char *)((void *)0);
/* Get all the necessary values from this new packet */
  while(l != ((GSList *)((void *)0))){
    struct yahoo_pair *pair = (l -> data);
    switch(pair -> key){
/* Get who the packet is for */
      case 5:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          me = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_p2pfilexfer got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
/* Get who the packet is from */
      case 4:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          from = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_p2pfilexfer got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
/* Get the type of service */
      case 49:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          service = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_p2pfilexfer got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
/* Get the 'message' of the packet */
      case 14:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          message = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_p2pfilexfer got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
/* Get the command associated with this packet */
      case 13:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          command = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_p2pfilexfer got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
/* IMVironment name and version */
      case 63:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          imv = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_p2pfilexfer got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
/* Not sure, but it does vary with initialization of Doodle */
      case 64:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
/* So, I'll keep it (for a little while atleast) */
          unknown = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_p2pfilexfer got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
    }
    l = (l -> next);
  }
/* If this packet is an IMVIRONMENT, handle it accordingly */
  if (((service != ((char *)((void *)0))) && (imv != ((char *)((void *)0)))) && !(strcmp(service,"IMVIRONMENT") != 0)) {
/* Check for a Doodle packet and handle it accordingly */
    if (strstr(imv,"doodle;") != ((char *)((void *)0))) 
      yahoo_doodle_process(gc,me,from,command,message,imv);
/* If an IMVIRONMENT packet comes without a specific imviroment name */
    if (!(strcmp(imv,";0") != 0)) {
/* It is unfortunately time to close all IMVironments with the remote client */
      yahoo_doodle_command_got_shutdown(gc,from);
    }
  }
}

void yahoo_process_filetransfer(PurpleConnection *gc,struct yahoo_packet *pkt)
{
  char *from = (char *)((void *)0);
  char *to = (char *)((void *)0);
  char *msg = (char *)((void *)0);
  char *url = (char *)((void *)0);
  char *imv = (char *)((void *)0);
  long expires = 0;
  PurpleXfer *xfer;
  YahooData *yd;
  struct yahoo_xfer_data *xfer_data;
  char *service = (char *)((void *)0);
  char *filename = (char *)((void *)0);
  unsigned long filesize = 0L;
  GSList *l;
  yd = (gc -> proto_data);
  for (l = (pkt -> hash); l != 0; l = (l -> next)) {
    struct yahoo_pair *pair = (l -> data);
    switch(pair -> key){
      case 4:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          from = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_filetransfer got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
      case 5:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          to = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_filetransfer got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
      case 14:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          msg = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_filetransfer got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
      case 20:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          url = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_filetransfer got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
      case 38:
{
        expires = strtol((pair -> value),0,10);
        break; 
      }
      case 27:
{
        filename = (pair -> value);
        break; 
      }
      case 28:
{
        filesize = (atol((pair -> value)));
        break; 
      }
      case 49:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          service = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_filetransfer got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
      case 63:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          imv = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_filetransfer got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
    }
  }
/*
	 * The remote user has changed their IMVironment.  We
	 * record it for later use.
	 */
  if ((((from != 0) && (imv != 0)) && (service != 0)) && (strcmp("IMVIRONMENT",service) == 0)) {
    g_hash_table_replace((yd -> imvironments),(g_strdup(from)),(g_strdup(imv)));
    return ;
  }
  if ((pkt -> service) == YAHOO_SERVICE_P2PFILEXFER) {
    if ((service != 0) && (strcmp("FILEXFER",service) != 0)) {
      purple_debug_misc("yahoo","unhandled service 0x%02x\n",(pkt -> service));
      return ;
    }
  }
  if (msg != 0) {
    char *tmp;
    tmp = strchr(msg,6);
    if (tmp != 0) 
       *tmp = 0;
  }
  if (!(url != 0) || !(from != 0)) 
    return ;
/* Setup the Yahoo-specific file transfer data */
  xfer_data = ((struct yahoo_xfer_data *)(g_malloc0_n(1,(sizeof(struct yahoo_xfer_data )))));
  xfer_data -> gc = gc;
  if (!(purple_url_parse(url,&xfer_data -> host,&xfer_data -> port,&xfer_data -> path,0,0) != 0)) {
    g_free(xfer_data);
    return ;
  }
  purple_debug_misc("yahoo_filexfer","Host is %s, port is %d, path is %s, and the full url was %s.\n",(xfer_data -> host),(xfer_data -> port),(xfer_data -> path),url);
/* Build the file transfer handle. */
  xfer = purple_xfer_new((gc -> account),PURPLE_XFER_RECEIVE,from);
  if (xfer == ((PurpleXfer *)((void *)0))) {
    g_free(xfer_data);
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","yahoo_filexfer.c",951,((const char *)__func__));
      return ;
    }while (0);
  }
  xfer -> data = xfer_data;
/* Set the info about the incoming file. */
  if (filename != 0) {
    char *utf8_filename = yahoo_string_decode(gc,filename,(!0));
    purple_xfer_set_filename(xfer,utf8_filename);
    g_free(utf8_filename);
  }
  else {
    gchar *start;
    gchar *end;
    start = g_strrstr((xfer_data -> path),"/");
    if (start != 0) 
      start++;
    end = g_strrstr((xfer_data -> path),"\?");
    if (((start != 0) && (( *start) != 0)) && (end != 0)) {
      char *utf8_filename;
      filename = g_strndup(start,(end - start));
      utf8_filename = yahoo_string_decode(gc,filename,(!0));
      g_free(filename);
      purple_xfer_set_filename(xfer,utf8_filename);
      g_free(utf8_filename);
      filename = ((char *)((void *)0));
    }
  }
  purple_xfer_set_size(xfer,filesize);
/* Setup our I/O op functions */
  purple_xfer_set_init_fnc(xfer,yahoo_xfer_init);
  purple_xfer_set_start_fnc(xfer,yahoo_xfer_start);
  purple_xfer_set_end_fnc(xfer,yahoo_xfer_end);
  purple_xfer_set_cancel_send_fnc(xfer,yahoo_xfer_cancel_send);
  purple_xfer_set_cancel_recv_fnc(xfer,yahoo_xfer_cancel_recv);
  purple_xfer_set_read_fnc(xfer,yahoo_xfer_read);
  purple_xfer_set_write_fnc(xfer,yahoo_xfer_write);
/* Now perform the request */
  purple_xfer_request(xfer);
}

PurpleXfer *yahoo_new_xfer(PurpleConnection *gc,const char *who)
{
  PurpleXfer *xfer;
  struct yahoo_xfer_data *xfer_data;
  do {
    if (who != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"who != NULL");
      return 0;
    };
  }while (0);
  xfer_data = ((struct yahoo_xfer_data *)(g_malloc0_n(1,(sizeof(struct yahoo_xfer_data )))));
  xfer_data -> gc = gc;
/* Build the file transfer handle. */
  xfer = purple_xfer_new((gc -> account),PURPLE_XFER_SEND,who);
  if (xfer == ((PurpleXfer *)((void *)0))) {
    g_free(xfer_data);
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","yahoo_filexfer.c",1008,((const char *)__func__));
      return 0;
    }while (0);
  }
  xfer -> data = xfer_data;
/* Setup our I/O op functions */
  purple_xfer_set_init_fnc(xfer,yahoo_xfer_init);
  purple_xfer_set_start_fnc(xfer,yahoo_xfer_start);
  purple_xfer_set_end_fnc(xfer,yahoo_xfer_end);
  purple_xfer_set_cancel_send_fnc(xfer,yahoo_xfer_cancel_send);
  purple_xfer_set_cancel_recv_fnc(xfer,yahoo_xfer_cancel_recv);
  purple_xfer_set_read_fnc(xfer,yahoo_xfer_read);
  purple_xfer_set_write_fnc(xfer,yahoo_xfer_write);
  return xfer;
}

static gchar *yahoo_xfer_new_xfer_id()
{
  gchar *ans;
  int i;
  int j;
  ans = g_strnfill(24,32);
  ans[23] = '$';
  ans[22] = '$';
  for (i = 0; i < 22; i++) {
    j = g_random_int_range(0,61);
    if (j < 26) 
      ans[i] = (j + 'a');
    else if (j < 52) 
      ans[i] = ((j - 26) + 65);
    else 
      ans[i] = ((j - 52) + 48);
  }
  return ans;
}

static void yahoo_xfer_dns_connected_15(GSList *hosts,gpointer data,const char *error_message)
{
  PurpleXfer *xfer;
  struct yahoo_xfer_data *xd;
  struct sockaddr_in *addr;
  struct yahoo_packet *pkt;
  unsigned long actaddr;
  unsigned char a;
  unsigned char b;
  unsigned char c;
  unsigned char d;
  PurpleConnection *gc;
  PurpleAccount *account;
  YahooData *yd;
  gchar *url;
  gchar *filename;
  if (!((xfer = data) != 0)) 
    return ;
  if (!((xd = (xfer -> data)) != 0)) 
    return ;
  gc = (xd -> gc);
  account = purple_connection_get_account(gc);
  yd = (gc -> proto_data);
  if (!(hosts != 0)) {
    purple_debug_error("yahoo","Unable to find an IP address for relay.msg.yahoo.com\n");
    purple_xfer_cancel_remote(xfer);
    return ;
  }
/* Discard the length... */
  hosts = g_slist_remove(hosts,(hosts -> data));
  if (!(hosts != 0)) {
    purple_debug_error("yahoo","Unable to find an IP address for relay.msg.yahoo.com\n");
    purple_xfer_cancel_remote(xfer);
    return ;
  }
/* TODO:actually, u must try with addr no.1 , if its not working addr no.2 ..... */
  addr = (hosts -> data);
  actaddr = addr -> sin_addr.s_addr;
  d = (actaddr & 0xff);
  actaddr >>= 8;
  c = (actaddr & 0xff);
  actaddr >>= 8;
  b = (actaddr & 0xff);
  actaddr >>= 8;
  a = (actaddr & 0xff);
  if ((yd -> jp) != 0) 
    xd -> port = 80;
  else 
    xd -> port = 80;
  url = g_strdup_printf("%u.%u.%u.%u",d,c,b,a);
/* Free the address... */
  g_free((hosts -> data));
  hosts = g_slist_remove(hosts,(hosts -> data));
  addr = ((struct sockaddr_in *)((void *)0));
  while(hosts != ((GSList *)((void *)0))){
/* Discard the length... */
    hosts = g_slist_remove(hosts,(hosts -> data));
/* Free the address... */
    g_free((hosts -> data));
    hosts = g_slist_remove(hosts,(hosts -> data));
  }
  if (!(purple_url_parse(url,&xd -> host,&xd -> port,&xd -> path,0,0) != 0)) {
    purple_xfer_cancel_remote(xfer);
    g_free(url);
    return ;
  }
  g_free(url);
  pkt = yahoo_packet_new(YAHOO_SERVICE_FILETRANS_INFO_15,YAHOO_STATUS_AVAILABLE,(yd -> session_id));
  filename = g_path_get_basename(purple_xfer_get_local_filename(xfer));
  yahoo_packet_hash(pkt,"ssssis",1,purple_normalize(account,purple_account_get_username(account)),5,(xfer -> who),265,(xd -> xfer_peer_idstring),27,filename,249,3,250,(xd -> host));
  g_free(filename);
  yahoo_packet_send_and_free(pkt,yd);
}

gboolean yahoo_can_receive_file(PurpleConnection *gc,const char *who)
{
  if (!(who != 0) || ((yahoo_get_federation_from_name(who)) != YAHOO_FEDERATION_NONE)) 
    return 0;
  return (!0);
}

void yahoo_send_file(PurpleConnection *gc,const char *who,const char *file)
{
  struct yahoo_xfer_data *xfer_data;
  YahooData *yd = (gc -> proto_data);
  PurpleXfer *xfer = yahoo_new_xfer(gc,who);
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
/* if we don't have a p2p connection, try establishing it now */
  if (!(g_hash_table_lookup((yd -> peers),who) != 0)) 
    yahoo_send_p2p_pkt(gc,who,0);
  xfer_data = (xfer -> data);
  xfer_data -> status_15 = STARTED;
  purple_xfer_set_init_fnc(xfer,yahoo_xfer_init_15);
  xfer_data -> version = 15;
  xfer_data -> xfer_peer_idstring = yahoo_xfer_new_xfer_id();
  g_hash_table_insert((yd -> xfer_peer_idstring_map),(xfer_data -> xfer_peer_idstring),xfer);
/* Now perform the request */
  if (file != 0) 
    purple_xfer_request_accepted(xfer,file);
  else 
    purple_xfer_request(xfer);
}
/* using this in yahoo_xfer_send_cb_15 */
static void yahoo_p2p_ft_server_listen_cb(int listenfd,gpointer data);
/* using this in recv_cb */
static void yahoo_xfer_connected_15(gpointer data,gint source,const gchar *error_message);

static void yahoo_xfer_recv_cb_15(gpointer data,gint source,PurpleInputCondition condition)
{
  PurpleXfer *xfer;
  struct yahoo_xfer_data *xd;
  int did;
  gchar *buf;
  gchar *t;
  PurpleAccount *account;
  PurpleConnection *gc;
  xfer = data;
  xd = (xfer -> data);
  account = purple_connection_get_account((xd -> gc));
  gc = (xd -> gc);
  buf = g_strnfill(1000,0);
  while((did = (read(source,buf,998))) > 0){
    xd -> txbuflen += did;
    buf[did] = 0;
    t = (xd -> txbuf);
    xd -> txbuf = g_strconcat(t,buf,((void *)((void *)0)));
    g_free(t);
  }
  g_free(buf);
  if ((did < 0) && ( *__errno_location() == 11)) 
    return ;
  else if (did < 0) {
    purple_debug_error("yahoo","Unable to write in order to start ft errno = %d\n", *__errno_location());
    purple_xfer_cancel_remote(xfer);
    return ;
  }
  purple_input_remove((xd -> tx_handler));
  xd -> tx_handler = 0;
  xd -> txbuflen = 0;
  if ((xd -> status_15) == HEAD_REQUESTED) {
    xd -> status_15 = HEAD_REPLY_RECEIVED;
/* Is this required? */
    close(source);
    g_free((xd -> txbuf));
    xd -> txbuf = ((gchar *)((void *)0));
    if (purple_proxy_connect(gc,account,(xd -> host),(xd -> port),yahoo_xfer_connected_15,xfer) == ((PurpleProxyConnectData *)((void *)0))) {
      purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","File Transfer Failed"))),((const char *)(dgettext("pidgin","Unable to establish file descriptor."))),0,0);
      purple_xfer_cancel_remote(xfer);
    }
  }
  else {
    purple_debug_error("yahoo","Unrecognized yahoo file transfer mode and stage (ymsg15):%d,%d\n",purple_xfer_get_type(xfer),(xd -> status_15));
    return ;
  }
}

static void yahoo_xfer_send_cb_15(gpointer data,gint source,PurpleInputCondition condition)
{
  PurpleXfer *xfer;
  struct yahoo_xfer_data *xd;
  int remaining;
  int written;
  xfer = data;
  xd = (xfer -> data);
  remaining = ((xd -> txbuflen) - (xd -> txbuf_written));
  written = (write(source,((xd -> txbuf) + (xd -> txbuf_written)),remaining));
  if ((written < 0) && ( *__errno_location() == 11)) 
    written = 0;
  else if (written <= 0) {
    purple_debug_error("yahoo","Unable to write in order to start ft errno = %d\n", *__errno_location());
    purple_xfer_cancel_remote(xfer);
    return ;
  }
  if (written < remaining) {
    xd -> txbuf_written += written;
    return ;
  }
  purple_input_remove((xd -> tx_handler));
  xd -> tx_handler = 0;
  g_free((xd -> txbuf));
  xd -> txbuf = ((gchar *)((void *)0));
  xd -> txbuflen = 0;
  xd -> txbuf_written = 0;
  if (((purple_xfer_get_type(xfer)) == PURPLE_XFER_RECEIVE) && ((xd -> status_15) == STARTED)) {
    xd -> status_15 = HEAD_REQUESTED;
    xd -> tx_handler = purple_input_add(source,PURPLE_INPUT_READ,yahoo_xfer_recv_cb_15,xfer);
    yahoo_xfer_recv_cb_15(xfer,source,PURPLE_INPUT_READ);
  }
  else if (((purple_xfer_get_type(xfer)) == PURPLE_XFER_RECEIVE) && ((xd -> status_15) == HEAD_REPLY_RECEIVED)) {
    xd -> status_15 = TRANSFER_PHASE;
    xfer -> fd = source;
    purple_xfer_start(xfer,source,0,0);
  }
  else if (((purple_xfer_get_type(xfer)) == PURPLE_XFER_SEND) && (((xd -> status_15) == ACCEPTED) || ((xd -> status_15) == P2P_GET_REQUESTED))) {
    xd -> status_15 = TRANSFER_PHASE;
    xfer -> fd = source;
/* Remove Read event */
    purple_input_remove((xd -> input_event));
    xd -> input_event = 0;
    purple_xfer_start(xfer,source,0,0);
  }
  else if (((purple_xfer_get_type(xfer)) == PURPLE_XFER_SEND) && ((xd -> status_15) == P2P_HEAD_REQUESTED)) {
    xd -> status_15 = P2P_HEAD_REPLIED;
/* Remove Read event and close descriptor */
    purple_input_remove((xd -> input_event));
    xd -> input_event = 0;
    close(source);
    xfer -> fd = -1;
/* start local server, listen for connections */
    purple_network_listen((xd -> yahoo_local_p2p_ft_server_port),SOCK_STREAM,yahoo_p2p_ft_server_listen_cb,xfer);
  }
  else {
    purple_debug_error("yahoo","Unrecognized yahoo file transfer mode and stage (ymsg15):%d,%d\n",purple_xfer_get_type(xfer),(xd -> status_15));
    return ;
  }
}

static void yahoo_xfer_connected_15(gpointer data,gint source,const gchar *error_message)
{
  PurpleXfer *xfer;
  struct yahoo_xfer_data *xd;
  PurpleAccount *account;
  PurpleConnection *gc;
  if (!((xfer = data) != 0)) 
    return ;
  if (!((xd = (xfer -> data)) != 0)) 
    return ;
  gc = (xd -> gc);
  account = purple_connection_get_account(gc);
  if (((source < 0) || ((xd -> path) == ((gchar *)((void *)0)))) || ((xd -> host) == ((gchar *)((void *)0)))) {
    purple_xfer_error(PURPLE_XFER_RECEIVE,purple_xfer_get_account(xfer),(xfer -> who),((const char *)(dgettext("pidgin","Unable to connect."))));
    purple_xfer_cancel_remote(xfer);
    return ;
  }
/* The first time we get here, assemble the tx buffer */
  if ((xd -> txbuflen) == 0) {
    gchar *cookies;
    YahooData *yd = (gc -> proto_data);
/* cookies = yahoo_get_cookies(gc);
		 * This doesn't seem to be working. The function is returning NULL, which yahoo servers don't like
		 * For now let us not use this function */
    cookies = g_strdup_printf("Y=%s; T=%s",(yd -> cookie_y),(yd -> cookie_t));
    if (((purple_xfer_get_type(xfer)) == PURPLE_XFER_SEND) && ((xd -> status_15) == ACCEPTED)) {
      if ((xd -> info_val_249) == 2) {
/* sending file via p2p, we are connected as client */
        xd -> txbuf = g_strdup_printf("POST /%s HTTP/1.1\r\nUser-Agent: Mozilla/5.0\r\nHost: %s\r\nContent-Length: %ld\r\nCache-Control: no-cache\r\n\r\n",(xd -> path),(xd -> host),((long )(xfer -> size)));
/* to do, add Referer */
      }
      else {
/* sending file via relaying */
        xd -> txbuf = g_strdup_printf("POST /relay\?token=%s&sender=%s&recver=%s HTTP/1.1\r\nCookie:%s\r\nUser-Agent: Mozilla/5.0\r\nHost: %s\r\nContent-Length: %ld\r\nCache-Control: no-cache\r\n\r\n",purple_url_encode((xd -> xfer_idstring_for_relay)),purple_normalize(account,purple_account_get_username(account)),(xfer -> who),cookies,(xd -> host),((long )(xfer -> size)));
      }
    }
    else if (((purple_xfer_get_type(xfer)) == PURPLE_XFER_RECEIVE) && ((xd -> status_15) == STARTED)) {
      if ((xd -> info_val_249) == 1) {
/* receiving file via p2p, connected as client */
        xd -> txbuf = g_strdup_printf("HEAD /%s HTTP/1.1\r\nAccept: */*\r\nUser-Agent: Mozilla/5.0\r\nHost: %s\r\nContent-Length: 0\r\nCache-Control: no-cache\r\n\r\n",(xd -> path),(xd -> host));
      }
      else {
/* receiving file via relaying */
        xd -> txbuf = g_strdup_printf("HEAD /relay\?token=%s&sender=%s&recver=%s HTTP/1.1\r\nAccept: */*\r\nCookie: %s\r\nUser-Agent: Mozilla/5.0\r\nHost: %s\r\nContent-Length: 0\r\nCache-Control: no-cache\r\n\r\n",purple_url_encode((xd -> xfer_idstring_for_relay)),purple_normalize(account,purple_account_get_username(account)),(xfer -> who),cookies,(xd -> host));
      }
    }
    else if (((purple_xfer_get_type(xfer)) == PURPLE_XFER_RECEIVE) && ((xd -> status_15) == HEAD_REPLY_RECEIVED)) {
      if ((xd -> info_val_249) == 1) {
/* receiving file via p2p, connected as client */
        xd -> txbuf = g_strdup_printf("GET /%s HTTP/1.1\r\nUser-Agent: Mozilla/5.0\r\nHost: %s\r\nConnection: Keep-Alive\r\n\r\n",(xd -> path),(xd -> host));
      }
      else {
/* receiving file via relaying */
        xd -> txbuf = g_strdup_printf("GET /relay\?token=%s&sender=%s&recver=%s HTTP/1.1\r\nCookie: %s\r\nUser-Agent: Mozilla/5.0\r\nHost: %s\r\nConnection: Keep-Alive\r\n\r\n",purple_url_encode((xd -> xfer_idstring_for_relay)),purple_normalize(account,purple_account_get_username(account)),(xfer -> who),cookies,(xd -> host));
      }
    }
    else {
      purple_debug_error("yahoo","Unrecognized yahoo file transfer mode and stage (ymsg15):%d,%d\n",purple_xfer_get_type(xfer),(xd -> status_15));
      g_free(cookies);
      return ;
    }
    xd -> txbuflen = strlen((xd -> txbuf));
    xd -> txbuf_written = 0;
    g_free(cookies);
  }
  if (!((xd -> tx_handler) != 0U)) {
    xd -> tx_handler = purple_input_add(source,PURPLE_INPUT_WRITE,yahoo_xfer_send_cb_15,xfer);
    yahoo_xfer_send_cb_15(xfer,source,PURPLE_INPUT_WRITE);
  }
}

static void yahoo_p2p_ft_POST_cb(gpointer data,gint source,PurpleInputCondition cond)
{
  PurpleXfer *xfer;
  struct yahoo_xfer_data *xd;
  xfer = data;
  if (!((xd = (xfer -> data)) != 0)) {
    purple_xfer_cancel_remote(xfer);
    return ;
  }
  purple_input_remove((xd -> input_event));
  xd -> status_15 = TRANSFER_PHASE;
  xfer -> fd = source;
  purple_xfer_start(xfer,source,0,0);
}

static void yahoo_p2p_ft_HEAD_GET_cb(gpointer data,gint source,PurpleInputCondition cond)
{
  PurpleXfer *xfer;
  struct yahoo_xfer_data *xd;
  guchar buf[1024UL];
  int len;
  char *url_head;
  char *url_get;
  time_t unix_time;
  char *time_str;
  xfer = data;
  if (!((xd = (xfer -> data)) != 0)) {
    purple_xfer_cancel_remote(xfer);
    return ;
  }
  len = (read(source,buf,(sizeof(buf))));
  if ((len < 0) && (( *__errno_location() == 11) || ( *__errno_location() == 11))) 
/* No Worries*/
    return ;
  else if (len <= 0) {
    purple_debug_warning("yahoo","p2p-ft: Error in connection, or host disconnected\n");
    purple_input_remove((xd -> input_event));
    purple_xfer_cancel_remote(xfer);
    return ;
  }
  url_head = g_strdup_printf("HEAD %s",(xd -> xfer_url));
  url_get = g_strdup_printf("GET %s",(xd -> xfer_url));
  if (strncmp(url_head,((char *)buf),strlen(url_head)) == 0) 
    xd -> status_15 = P2P_HEAD_REQUESTED;
  else if (strncmp(url_get,((char *)buf),strlen(url_get)) == 0) 
    xd -> status_15 = P2P_GET_REQUESTED;
  else {
    purple_debug_warning("yahoo","p2p-ft: Wrong HEAD/GET request from peer, disconnecting host\n");
    purple_input_remove((xd -> input_event));
    purple_xfer_cancel_remote(xfer);
    g_free(url_head);
    return ;
  }
  unix_time = time(0);
  time_str = ctime((&unix_time));
  time_str[strlen(time_str) - 1] = 0;
  if ((xd -> txbuflen) == 0) {
    xd -> txbuf = g_strdup_printf("HTTP/1.0 200 OK\r\nDate: %s GMT\r\nServer: Y!/1.0\r\nMIME-version: 1.0\r\nLast-modified: %s GMT\r\nContent-length: %lu\r\n\r\n",time_str,time_str,(xfer -> size));
    xd -> txbuflen = strlen((xd -> txbuf));
    xd -> txbuf_written = 0;
  }
  if (!((xd -> tx_handler) != 0U)) {
    xd -> tx_handler = purple_input_add(source,PURPLE_INPUT_WRITE,yahoo_xfer_send_cb_15,xfer);
    yahoo_xfer_send_cb_15(xfer,source,PURPLE_INPUT_WRITE);
  }
  g_free(url_head);
  g_free(url_get);
}

static void yahoo_p2p_ft_server_send_connected_cb(gpointer data,gint source,PurpleInputCondition cond)
{
  int acceptfd;
  PurpleXfer *xfer;
  struct yahoo_xfer_data *xd;
  xfer = data;
  if (!((xd = (xfer -> data)) != 0)) {
    purple_xfer_cancel_remote(xfer);
    return ;
  }
  acceptfd = accept(source,0,0);
  if ((acceptfd == -1) && (( *__errno_location() == 11) || ( *__errno_location() == 11))) 
    return ;
  else if (acceptfd == -1) {
    purple_debug_warning("yahoo","yahoo_p2p_server_send_connected_cb: accept: %s\n",g_strerror( *__errno_location()));
    purple_xfer_cancel_remote(xfer);
/* remove watcher and close p2p ft server */
    purple_input_remove((xd -> yahoo_p2p_ft_server_watcher));
    close((xd -> yahoo_local_p2p_ft_server_fd));
    return ;
  }
/* remove watcher and close p2p ft server */
  purple_input_remove((xd -> yahoo_p2p_ft_server_watcher));
  close((xd -> yahoo_local_p2p_ft_server_fd));
/* Add an Input Read event to the file descriptor */
  xfer -> fd = acceptfd;
  if ((xfer -> type) == PURPLE_XFER_RECEIVE) 
    xd -> input_event = (purple_input_add(acceptfd,PURPLE_INPUT_READ,yahoo_p2p_ft_POST_cb,data));
  else 
    xd -> input_event = (purple_input_add(acceptfd,PURPLE_INPUT_READ,yahoo_p2p_ft_HEAD_GET_cb,data));
}

static void yahoo_p2p_ft_server_listen_cb(int listenfd,gpointer data)
{
  PurpleXfer *xfer;
  struct yahoo_xfer_data *xd;
  struct yahoo_packet *pkt;
  PurpleAccount *account;
  YahooData *yd;
  gchar *filename;
  const char *local_ip;
  gchar *url_to_send = (gchar *)((void *)0);
  char *filename_without_spaces = (char *)((void *)0);
  xfer = data;
  if (!((xd = (xfer -> data)) != 0) || (listenfd == -1)) {
    purple_debug_warning("yahoo","p2p: error starting server for p2p file transfer\n");
    purple_xfer_cancel_remote(xfer);
    return ;
  }
  if (((xfer -> type) == PURPLE_XFER_RECEIVE) || ((xd -> status_15) != P2P_HEAD_REPLIED)) {
    yd = ( *(xd -> gc)).proto_data;
    account = purple_connection_get_account((xd -> gc));
    local_ip = purple_network_get_my_ip(listenfd);
    xd -> yahoo_local_p2p_ft_server_port = (purple_network_get_port_from_fd(listenfd));
    filename = g_path_get_basename(purple_xfer_get_local_filename(xfer));
    filename_without_spaces = g_strdup(filename);
    purple_util_chrreplace(filename_without_spaces,32,'+');
    xd -> xfer_url = g_strdup_printf("/Messenger.%s.%d000%s\?AppID=Messenger&UserID=%s&K=lc9lu2u89gz1llmplwksajkjx",(xfer -> who),((int )(time(0))),filename_without_spaces,(xfer -> who));
    url_to_send = g_strdup_printf("http://%s:%d%s",local_ip,(xd -> yahoo_local_p2p_ft_server_port),(xd -> xfer_url));
    if ((xfer -> type) == PURPLE_XFER_RECEIVE) {
/* 249=2: we are p2p server, and receiving file */
      xd -> info_val_249 = 2;
      pkt = yahoo_packet_new(YAHOO_SERVICE_FILETRANS_ACC_15,YAHOO_STATUS_AVAILABLE,(yd -> session_id));
      yahoo_packet_hash(pkt,"ssssis",1,purple_normalize(account,purple_account_get_username(account)),5,(xfer -> who),265,(xd -> xfer_peer_idstring),27,(xfer -> filename),249,2,250,url_to_send);
    }
    else {
/* 249=1: we are p2p server, and sending file */
      xd -> info_val_249 = 1;
      pkt = yahoo_packet_new(YAHOO_SERVICE_FILETRANS_INFO_15,YAHOO_STATUS_AVAILABLE,(yd -> session_id));
      yahoo_packet_hash(pkt,"ssssis",1,purple_normalize(account,purple_account_get_username(account)),5,(xfer -> who),265,(xd -> xfer_peer_idstring),27,filename,249,1,250,url_to_send);
    }
    yahoo_packet_send_and_free(pkt,yd);
    g_free(filename);
    g_free(url_to_send);
    g_free(filename_without_spaces);
  }
/* Add an Input Read event to the file descriptor */
  xd -> yahoo_local_p2p_ft_server_fd = listenfd;
  xd -> yahoo_p2p_ft_server_watcher = (purple_input_add(listenfd,PURPLE_INPUT_READ,yahoo_p2p_ft_server_send_connected_cb,data));
}
/* send (p2p) file transfer information */

static void yahoo_p2p_client_send_ft_info(PurpleConnection *gc,PurpleXfer *xfer)
{
  struct yahoo_xfer_data *xd;
  struct yahoo_packet *pkt;
  PurpleAccount *account;
  YahooData *yd;
  gchar *filename;
  struct yahoo_p2p_data *p2p_data;
  if (!((xd = (xfer -> data)) != 0)) 
    return ;
  account = purple_connection_get_account(gc);
  yd = (gc -> proto_data);
  p2p_data = (g_hash_table_lookup((yd -> peers),(xfer -> who)));
  if ((p2p_data -> connection_type) == YAHOO_P2P_WE_ARE_SERVER) 
    if (purple_network_listen_range(0,0,SOCK_STREAM,yahoo_p2p_ft_server_listen_cb,xfer) != 0) 
      return ;
  pkt = yahoo_packet_new(YAHOO_SERVICE_FILETRANS_INFO_15,YAHOO_STATUS_AVAILABLE,(yd -> session_id));
  filename = g_path_get_basename(purple_xfer_get_local_filename(xfer));
  yahoo_packet_hash(pkt,"ssssi",1,purple_normalize(account,purple_account_get_username(account)),5,(xfer -> who),265,(xd -> xfer_peer_idstring),27,filename,249,2);
/* 249=2: we are p2p client */
  xd -> info_val_249 = 2;
  yahoo_packet_send_and_free(pkt,yd);
  g_free(filename);
}

void yahoo_process_filetrans_15(PurpleConnection *gc,struct yahoo_packet *pkt)
{
  char *from = (char *)((void *)0);
  char *to = (char *)((void *)0);
  char *imv = (char *)((void *)0);
  long val_222 = 0L;
  PurpleXfer *xfer;
  YahooData *yd;
  struct yahoo_xfer_data *xfer_data;
  char *service = (char *)((void *)0);
  char *filename = (char *)((void *)0);
  char *xfer_peer_idstring = (char *)((void *)0);
  char *utf8_filename;
  unsigned long filesize = 0L;
  GSList *l;
  GSList *filename_list = (GSList *)((void *)0);
  GSList *size_list = (GSList *)((void *)0);
  int nooffiles = 0;
  yd = (gc -> proto_data);
  for (l = (pkt -> hash); l != 0; l = (l -> next)) {
    struct yahoo_pair *pair = (l -> data);
    switch(pair -> key){
      case 4:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          from = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_filetrans_15 got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
      case 5:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          to = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_filetrans_15 got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
      case 265:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          xfer_peer_idstring = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_filetrans_15 got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
      case 27:
{
        filename_list = g_slist_prepend(filename_list,(g_strdup((pair -> value))));
        nooffiles++;
        break; 
      }
      case 28:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          size_list = g_slist_prepend(size_list,(g_strdup((pair -> value))));
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_filetrans_15 got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
      case 222:
{
        val_222 = atol((pair -> value));
/* 1=send, 2=cancel, 3=accept, 4=reject */
        break; 
      }
/* check for p2p and imviron .... not sure it comes by this service packet. Since it was bundled with filexfer in old ymsg version, still keeping it. */
      case 49:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          service = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_filetrans_15 got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
      case 63:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          imv = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_filetrans_15 got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
/* end check */
    }
  }
  if (!(xfer_peer_idstring != 0)) 
    return ;
  if ((val_222 == 2) || (val_222 == 4)) {
    xfer = (g_hash_table_lookup((yd -> xfer_peer_idstring_map),xfer_peer_idstring));
    if (!(xfer != 0)) 
      return ;
    purple_xfer_cancel_remote(xfer);
    return ;
  }
  if (val_222 == 3) {
    PurpleAccount *account;
    xfer = (g_hash_table_lookup((yd -> xfer_peer_idstring_map),xfer_peer_idstring));
    if (!(xfer != 0)) 
      return ;
/*
		*	In the file trans info packet that we must reply with, we are
		*	supposed to mention the ip address...
		*	purple connect does not give me a way of finding the ip address...
		*	so, purple dnsquery is used... but retries, trying with next ip
		*	address etc. is not implemented..TODO
		*/
/* To send through p2p */
    if (g_hash_table_lookup((yd -> peers),from) != 0) {
/* send p2p file transfer information */
      yahoo_p2p_client_send_ft_info(gc,xfer);
      return ;
    }
    account = purple_connection_get_account(gc);
    if ((yd -> jp) != 0) {
      purple_dnsquery_a_account(account,"relay.msg.yahoo.co.jp",80,yahoo_xfer_dns_connected_15,xfer);
    }
    else {
      purple_dnsquery_a_account(account,"relay.msg.yahoo.com",80,yahoo_xfer_dns_connected_15,xfer);
    }
    return ;
  }
/* processing for p2p and imviron .... not sure it comes by this service packet. Since it was bundled with filexfer in old ymsg version, still keeping it. */
/*
	* The remote user has changed their IMVironment.  We
	* record it for later use.
	*/
  if ((((from != 0) && (imv != 0)) && (service != 0)) && (strcmp("IMVIRONMENT",service) == 0)) {
    g_hash_table_replace((yd -> imvironments),(g_strdup(from)),(g_strdup(imv)));
    return ;
  }
  if ((pkt -> service) == YAHOO_SERVICE_P2PFILEXFER) {
    if ((service != 0) && (strcmp("FILEXFER",service) != 0)) {
      purple_debug_misc("yahoo","unhandled service 0x%02x\n",(pkt -> service));
      return ;
    }
  }
/* end processing */
  if (!(filename_list != 0)) 
    return ;
/* have to change list into order in which client at other end sends */
  filename_list = g_slist_reverse(filename_list);
  size_list = g_slist_reverse(size_list);
  filename = (filename_list -> data);
  filesize = (atol((size_list -> data)));
  if (!(from != 0)) 
    return ;
  xfer_data = ((struct yahoo_xfer_data *)(g_malloc0_n(1,(sizeof(struct yahoo_xfer_data )))));
  xfer_data -> version = 15;
  xfer_data -> firstoflist = (!0);
  xfer_data -> gc = gc;
  xfer_data -> xfer_peer_idstring = g_strdup(xfer_peer_idstring);
  xfer_data -> filename_list = filename_list;
  xfer_data -> size_list = size_list;
/* Build the file transfer handle. */
  xfer = purple_xfer_new((gc -> account),PURPLE_XFER_RECEIVE,from);
  if (xfer == ((PurpleXfer *)((void *)0))) {
    g_free(xfer_data);
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","yahoo_filexfer.c",1834,((const char *)__func__));
      return ;
    }while (0);
  }
  xfer -> message = ((char *)((void *)0));
/* Set the info about the incoming file. */
  utf8_filename = yahoo_string_decode(gc,filename,(!0));
  purple_xfer_set_filename(xfer,utf8_filename);
  g_free(utf8_filename);
  purple_xfer_set_size(xfer,filesize);
  xfer -> data = xfer_data;
/* Setup our I/O op functions */
  purple_xfer_set_init_fnc(xfer,yahoo_xfer_init_15);
  purple_xfer_set_start_fnc(xfer,yahoo_xfer_start);
  purple_xfer_set_end_fnc(xfer,yahoo_xfer_end);
  purple_xfer_set_cancel_send_fnc(xfer,yahoo_xfer_cancel_send);
  purple_xfer_set_cancel_recv_fnc(xfer,yahoo_xfer_cancel_recv);
  purple_xfer_set_read_fnc(xfer,yahoo_xfer_read);
  purple_xfer_set_write_fnc(xfer,yahoo_xfer_write);
  purple_xfer_set_request_denied_fnc(xfer,yahoo_xfer_cancel_recv);
  g_hash_table_insert((yd -> xfer_peer_idstring_map),(xfer_data -> xfer_peer_idstring),xfer);
  if (nooffiles > 1) {
    gchar *message;
    message = g_strdup_printf(((const char *)(dgettext("pidgin","%s is trying to send you a group of %d files.\n"))),(xfer -> who),nooffiles);
    purple_xfer_conversation_write(xfer,message,0);
    g_free(message);
  }
/* Now perform the request */
  purple_xfer_request(xfer);
}

void yahoo_process_filetrans_info_15(PurpleConnection *gc,struct yahoo_packet *pkt)
{
  char *from = (char *)((void *)0);
  char *to = (char *)((void *)0);
  char *url = (char *)((void *)0);
  long val_249 = 0;
  long val_66 = 0;
  PurpleXfer *xfer;
  YahooData *yd;
  struct yahoo_xfer_data *xfer_data;
  char *filename = (char *)((void *)0);
  char *xfer_peer_idstring = (char *)((void *)0);
  char *xfer_idstring_for_relay = (char *)((void *)0);
  GSList *l;
  struct yahoo_packet *pkt_to_send;
  struct yahoo_p2p_data *p2p_data;
  yd = (gc -> proto_data);
  for (l = (pkt -> hash); l != 0; l = (l -> next)) {
    struct yahoo_pair *pair = (l -> data);
    switch(pair -> key){
      case 4:
{
        from = (pair -> value);
        break; 
      }
      case 5:
{
        to = (pair -> value);
        break; 
      }
      case 265:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          xfer_peer_idstring = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_filetrans_info_15 got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
      case 27:
{
        filename = (pair -> value);
        break; 
      }
      case 66:
{
        val_66 = strtol((pair -> value),0,10);
        break; 
      }
      case 249:
{
        val_249 = strtol((pair -> value),0,10);
/* 249 has value 1 or 2 when doing p2p transfer and value 3 when relaying through yahoo server */
        break; 
      }
      case 250:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          url = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_filetrans_info_15 got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
      case 251:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          xfer_idstring_for_relay = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_filetrans_info_15 got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
    }
  }
  if (!(xfer_peer_idstring != 0)) 
    return ;
  xfer = (g_hash_table_lookup((yd -> xfer_peer_idstring_map),xfer_peer_idstring));
  if (!(xfer != 0)) 
    return ;
  if (val_66 == (-1)) {
    purple_xfer_cancel_remote(xfer);
    return ;
  }
  xfer_data = (xfer -> data);
  xfer_data -> info_val_249 = val_249;
  xfer_data -> xfer_idstring_for_relay = g_strdup(xfer_idstring_for_relay);
  if ((val_249 == 1) || (val_249 == 3)) {
    PurpleAccount *account;
    if (!(purple_url_parse(url,&xfer_data -> host,&xfer_data -> port,&xfer_data -> path,0,0) != 0)) {
      purple_xfer_cancel_remote(xfer);
      return ;
    }
    account = purple_connection_get_account((xfer_data -> gc));
    pkt_to_send = yahoo_packet_new(YAHOO_SERVICE_FILETRANS_ACC_15,YAHOO_STATUS_AVAILABLE,(yd -> session_id));
    yahoo_packet_hash(pkt_to_send,"ssssis",1,purple_normalize(account,purple_account_get_username(account)),5,(xfer -> who),265,(xfer_data -> xfer_peer_idstring),27,(xfer -> filename),249,(xfer_data -> info_val_249),251,(xfer_data -> xfer_idstring_for_relay));
    yahoo_packet_send_and_free(pkt_to_send,yd);
    if (purple_proxy_connect(gc,account,(xfer_data -> host),(xfer_data -> port),yahoo_xfer_connected_15,xfer) == ((PurpleProxyConnectData *)((void *)0))) {
      purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","File Transfer Failed"))),((const char *)(dgettext("pidgin","Unable to establish file descriptor."))),0,0);
      purple_xfer_cancel_remote(xfer);
    }
  }
  else if (val_249 == 2) {
    p2p_data = (g_hash_table_lookup((yd -> peers),(xfer -> who)));
    if (!((p2p_data != 0) && ((p2p_data -> connection_type) == YAHOO_P2P_WE_ARE_SERVER))) {
      purple_xfer_cancel_remote(xfer);
      return ;
    }
    if (!(purple_network_listen_range(0,0,SOCK_STREAM,yahoo_p2p_ft_server_listen_cb,xfer) != 0)) {
      purple_xfer_cancel_remote(xfer);
      return ;
    }
  }
}
/* TODO: Check filename etc. No probs till some hacker comes in the way */

void yahoo_process_filetrans_acc_15(PurpleConnection *gc,struct yahoo_packet *pkt)
{
  gchar *xfer_peer_idstring = (gchar *)((void *)0);
  gchar *xfer_idstring_for_relay = (gchar *)((void *)0);
  PurpleXfer *xfer;
  YahooData *yd;
  struct yahoo_xfer_data *xfer_data;
  GSList *l;
  PurpleAccount *account;
  long val_66 = 0;
  gchar *url = (gchar *)((void *)0);
  int val_249 = 0;
  yd = (gc -> proto_data);
  for (l = (pkt -> hash); l != 0; l = (l -> next)) {
    struct yahoo_pair *pair = (l -> data);
    switch(pair -> key){
      case 251:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          xfer_idstring_for_relay = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_filetrans_acc_15 got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
      case 265:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          xfer_peer_idstring = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_filetrans_acc_15 got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
      case 66:
{
        val_66 = atol((pair -> value));
        break; 
      }
      case 249:
{
        val_249 = (atol((pair -> value)));
        break; 
      }
      case 250:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
/* we get a p2p url here when sending file, connected as client */
          url = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_filetrans_acc_15 got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
    }
  }
  xfer = (g_hash_table_lookup((yd -> xfer_peer_idstring_map),xfer_peer_idstring));
  if (!(xfer != 0)) 
    return ;
  if ((val_66 == (-1)) || (!(xfer_idstring_for_relay != 0) && (val_249 != 2))) {
    purple_xfer_cancel_remote(xfer);
    return ;
  }
  if ((val_249 == 2) && !(url != 0)) {
    purple_xfer_cancel_remote(xfer);
    return ;
  }
  xfer_data = (xfer -> data);
  if (url != 0) 
    purple_url_parse(url,&xfer_data -> host,&xfer_data -> port,&xfer_data -> path,0,0);
  xfer_data -> xfer_idstring_for_relay = g_strdup(xfer_idstring_for_relay);
  xfer_data -> status_15 = ACCEPTED;
  account = purple_connection_get_account(gc);
  if (purple_proxy_connect(gc,account,(xfer_data -> host),(xfer_data -> port),yahoo_xfer_connected_15,xfer) == ((PurpleProxyConnectData *)((void *)0))) {
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","File Transfer Failed"))),((const char *)(dgettext("pidgin","Unable to connect"))),0,0);
    purple_xfer_cancel_remote(xfer);
  }
}
