/*
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "account.h"
#include "accountopt.h"
#include "blist.h"
#include "debug.h"
#include "util.h"
#include "request.h"
#include "version.h"
#include "libymsg.h"
#include "yahoo_aliases.h"
#include "yahoo_friend.h"
#include "yahoo_packet.h"
/* I hate hardcoding this stuff, but Yahoo never sends us anything to use.  Someone in the know may be able to tweak this URL */
#define YAHOO_ALIAS_FETCH_URL "http://address.yahoo.com/yab/us?v=XM&prog=ymsgr&.intl=us&diffs=1&t=0&tags=short&rt=0&prog-ver=" YAHOO_CLIENT_VERSION "&useutf8=1&legenc=codepage-1252"
#define YAHOO_ALIAS_UPDATE_URL "http://address.yahoo.com/yab/us?v=XM&prog=ymsgr&.intl=us&sync=1&tags=short&noclear=1&useutf8=1&legenc=codepage-1252"
#define YAHOOJP_ALIAS_FETCH_URL "http://address.yahoo.co.jp/yab/jp?v=XM&prog=ymsgr&.intl=jp&diffs=1&t=0&tags=short&rt=0&prog-ver=" YAHOOJP_CLIENT_VERSION
#define YAHOOJP_ALIAS_UPDATE_URL "http://address.yahoo.co.jp/yab/jp?v=XM&prog=ymsgr&.intl=jp&sync=1&tags=short&noclear=1"
void yahoo_update_alias(PurpleConnection *gc,const char *who,const char *alias);
/**
 * Stuff we want passed to the callback function
 */

struct callback_data 
{
  PurpleConnection *gc;
  gchar *id;
  gchar *who;
}
;

void yahoo_personal_details_reset(YahooPersonalDetails *ypd,gboolean all)
{
  if (all != 0) 
    g_free((ypd -> id));
  g_free(ypd -> names.first);
  g_free(ypd -> names.last);
  g_free(ypd -> names.middle);
  g_free(ypd -> names.nick);
  g_free(ypd -> phone.work);
  g_free(ypd -> phone.home);
  g_free(ypd -> phone.mobile);
}
/**************************************************************************
 * Alias Fetch Functions
 **************************************************************************/

static void yahoo_fetch_aliases_cb(PurpleUtilFetchUrlData *url_data,gpointer user_data,const gchar *url_text,size_t len,const gchar *error_message)
{
  PurpleConnection *gc = user_data;
  YahooData *yd = (gc -> proto_data);
  yd -> url_datas = g_slist_remove((yd -> url_datas),url_data);
  if (len == 0) {
    purple_debug_info("yahoo","No Aliases to process.%s%s\n",((error_message != 0)?" Error:" : ""),((error_message != 0)?error_message : ""));
  }
  else {
    gchar *full_name;
    gchar *nick_name;
    const char *yid;
    const char *id;
    const char *fn;
    const char *ln;
    const char *nn;
    const char *alias;
    const char *mn;
    const char *hp;
    const char *wp;
    const char *mo;
    YahooFriend *f;
    PurpleBuddy *b;
    xmlnode *item;
    xmlnode *contacts;
    PurpleAccount *account;
    account = purple_connection_get_account(gc);
/* Put our web response into a xmlnode for easy management */
    contacts = xmlnode_from_str(url_text,(-1));
    if (contacts == ((xmlnode *)((void *)0))) {
      purple_debug_error("yahoo","Badly formed Alias XML\n");
      return ;
    }
    purple_debug_info("yahoo","Fetched %lu bytes of alias data\n",len);
/* Loop around and around and around until we have gone through all the received aliases  */
    for (item = xmlnode_get_child(contacts,"ct"); item != 0; item = xmlnode_get_next_twin(item)) {
/* Yahoo replies with two types of contact (ct) record, we are only interested in the alias ones */
      if ((yid = xmlnode_get_attrib(item,"yi")) != 0) {
        YahooPersonalDetails *ypd = (YahooPersonalDetails *)((void *)0);
/* Grab all the bits of information we can */
        fn = xmlnode_get_attrib(item,"fn");
        ln = xmlnode_get_attrib(item,"ln");
        nn = xmlnode_get_attrib(item,"nn");
        mn = xmlnode_get_attrib(item,"mn");
        id = xmlnode_get_attrib(item,"id");
        hp = xmlnode_get_attrib(item,"hp");
        wp = xmlnode_get_attrib(item,"wp");
        mo = xmlnode_get_attrib(item,"mo");
        full_name = (nick_name = ((gchar *)((void *)0)));
        alias = ((const char *)((void *)0));
/* Yahoo stores first and last names separately, lets put them together into a full name */
        if ((yd -> jp) != 0) 
          full_name = g_strchomp(g_strchug(g_strdup_printf("%s %s",((ln != ((const char *)((void *)0)))?ln : ""),((fn != ((const char *)((void *)0)))?fn : ""))));
        else 
          full_name = g_strchomp(g_strchug(g_strdup_printf("%s %s",((fn != ((const char *)((void *)0)))?fn : ""),((ln != ((const char *)((void *)0)))?ln : ""))));
        nick_name = ((nn != ((const char *)((void *)0)))?g_strchomp(g_strchug(g_strdup(nn))) : ((char *)((void *)0)));
        if (nick_name != ((gchar *)((void *)0))) 
/* If we have a nickname from Yahoo, let's use it */
          alias = nick_name;
        else if (strlen(full_name) != 0) 
/* If no Yahoo nickname, we can use the full_name created above */
          alias = full_name;
/*  Find the local buddy that matches */
        f = yahoo_friend_find(gc,yid);
        b = purple_find_buddy(account,yid);
/*  If we don't find a matching buddy, ignore the alias !!  */
        if ((f != ((YahooFriend *)((void *)0))) && (b != ((PurpleBuddy *)((void *)0)))) {
          const char *buddy_alias = purple_buddy_get_alias(b);
          yahoo_friend_set_alias_id(f,id);
/* Finally, if we received an alias, we better update the buddy list */
          if (alias != ((const char *)((void *)0))) {
            serv_got_alias(gc,yid,alias);
            purple_debug_info("yahoo","Fetched alias \'%s\' (%s)\n",alias,id);
          }
          else if (((buddy_alias != 0) && (( *buddy_alias) != 0)) && !(g_str_equal(buddy_alias,yid) != 0)) {
/* Or if we have an alias that Yahoo doesn't, send it up */
            yahoo_update_alias(gc,yid,buddy_alias);
            purple_debug_info("yahoo","Sent updated alias \'%s\'\n",buddy_alias);
          }
        }
        if (f != ((YahooFriend *)((void *)0))) 
          ypd = &f -> ypd;
        else {
/* May be the alias is for the account? */
          const char *yidn = purple_normalize(account,yid);
          if (purple_strequal(yidn,purple_connection_get_display_name(gc)) != 0) {
            ypd = &yd -> ypd;
          }
        }
        if (ypd != 0) {
          yahoo_personal_details_reset(ypd,(!0));
          ypd -> id = g_strdup(id);
          ypd -> names.first = g_strdup(fn);
          ypd -> names.middle = g_strdup(mn);
          ypd -> names.last = g_strdup(ln);
          ypd -> names.nick = g_strdup(nn);
          ypd -> phone.work = g_strdup(wp);
          ypd -> phone.home = g_strdup(hp);
          ypd -> phone.mobile = g_strdup(mo);
        }
        g_free(full_name);
        g_free(nick_name);
      }
    }
    xmlnode_free(contacts);
  }
}

void yahoo_fetch_aliases(PurpleConnection *gc)
{
  YahooData *yd = (gc -> proto_data);
  const char *url;
  gchar *request;
  gchar *webpage;
  gchar *webaddress;
  PurpleUtilFetchUrlData *url_data;
/* use whole URL if using HTTP Proxy */
  gboolean use_whole_url = yahoo_account_use_http_proxy(gc);
/*  Build all the info to make the web request */
  url = ((((yd -> jp) != 0)?"http://address.yahoo.co.jp/yab/jp\?v=XM&prog=ymsgr&.intl=jp&diffs=1&t=0&tags=short&rt=0&prog-ver=9.0.0.1727" : "http://address.yahoo.com/yab/us\?v=XM&prog=ymsgr&.intl=us&diffs=1&t=0&tags=short&rt=0&prog-ver=9.0.0.2162&useutf8=1&legenc=codepage-1252"));
  purple_url_parse(url,&webaddress,0,&webpage,0,0);
  request = g_strdup_printf("GET %s%s/%s HTTP/1.1\r\nUser-Agent: Mozilla/4.0 (compatible; MSIE 5.5)\r\nCookie: T=%s; Y=%s\r\nHost: %s\r\nCache-Control: no-cache\r\n\r\n",((use_whole_url != 0)?"http://" : ""),((use_whole_url != 0)?webaddress : ""),webpage,(yd -> cookie_t),(yd -> cookie_y),webaddress);
/* We have a URL and some header information, let's connect and get some aliases  */
  url_data = purple_util_fetch_url_request_len_with_account(purple_connection_get_account(gc),url,use_whole_url,0,(!0),request,0,(-1),yahoo_fetch_aliases_cb,gc);
  if (url_data != ((PurpleUtilFetchUrlData *)((void *)0))) 
    yd -> url_datas = g_slist_prepend((yd -> url_datas),url_data);
  g_free(webaddress);
  g_free(webpage);
  g_free(request);
}
/**************************************************************************
 * Alias Update Functions
 **************************************************************************/

static void yahoo_update_alias_cb(PurpleUtilFetchUrlData *url_data,gpointer user_data,const gchar *url_text,size_t len,const gchar *error_message)
{
  xmlnode *node;
  xmlnode *result;
  struct callback_data *cb = user_data;
  PurpleConnection *gc = (cb -> gc);
  YahooData *yd;
  yd = (gc -> proto_data);
  yd -> url_datas = g_slist_remove((yd -> url_datas),url_data);
  if ((len == 0) || (error_message != ((const gchar *)((void *)0)))) {
    purple_debug_info("yahoo","Error updating alias for %s: %s\n",(cb -> who),((error_message != 0)?error_message : ""));
    g_free((cb -> who));
    g_free((cb -> id));
    g_free(cb);
    return ;
  }
  result = xmlnode_from_str(url_text,(-1));
  if (result == ((xmlnode *)((void *)0))) {
    purple_debug_error("yahoo","Alias update for %s failed: Badly formed response\n",(cb -> who));
    g_free((cb -> who));
    g_free((cb -> id));
    g_free(cb);
    return ;
  }
  if ((node = xmlnode_get_child(result,"ct")) != 0) {
    if ((cb -> id) == ((gchar *)((void *)0))) {
      const char *new_id = xmlnode_get_attrib(node,"id");
      if (new_id != ((const char *)((void *)0))) {
/* We now have an addressbook id for the friend; we should save it */
        YahooFriend *f = yahoo_friend_find((cb -> gc),(cb -> who));
        purple_debug_info("yahoo","Alias creation for %s succeeded\n",(cb -> who));
        if (f != 0) 
          yahoo_friend_set_alias_id(f,new_id);
        else 
          purple_debug_error("yahoo","Missing YahooFriend. Unable to store new addressbook id.\n");
      }
      else 
        purple_debug_error("yahoo","Missing new addressbook id in add response for %s (weird).\n",(cb -> who));
    }
    else {
      if (g_ascii_strncasecmp(xmlnode_get_attrib(node,"id"),(cb -> id),strlen((cb -> id))) == 0) 
        purple_debug_info("yahoo","Alias update for %s succeeded\n",(cb -> who));
      else 
        purple_debug_error("yahoo","Alias update for %s failed (Contact record return mismatch)\n",(cb -> who));
    }
  }
  else 
    purple_debug_info("yahoo","Alias update for %s failed (No contact record returned)\n",(cb -> who));
  g_free((cb -> who));
  g_free((cb -> id));
  g_free(cb);
  xmlnode_free(result);
}

void yahoo_update_alias(PurpleConnection *gc,const char *who,const char *alias)
{
  YahooData *yd;
  const char *url;
  gchar *content;
  gchar *request;
  gchar *webpage;
  gchar *webaddress;
  struct callback_data *cb;
  PurpleUtilFetchUrlData *url_data;
  YahooFriend *f;
/* use whole URL if using HTTP Proxy */
  gboolean use_whole_url = yahoo_account_use_http_proxy(gc);
  do {
    if (who != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"who != NULL");
      return ;
    };
  }while (0);
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  if (alias == ((const char *)((void *)0))) 
    alias = "";
  f = yahoo_friend_find(gc,who);
  if (f == ((YahooFriend *)((void *)0))) {
    purple_debug_error("yahoo","Missing YahooFriend. Unable to set server alias.\n");
    return ;
  }
  yd = (gc -> proto_data);
/* Using callback_data so I have access to gc in the callback function */
  cb = ((struct callback_data *)(g_malloc0_n(1,(sizeof(struct callback_data )))));
  cb -> who = g_strdup(who);
  cb -> id = g_strdup(yahoo_friend_get_alias_id(f));
  cb -> gc = gc;
/*  Build all the info to make the web request */
  url = ((((yd -> jp) != 0)?"http://address.yahoo.co.jp/yab/jp\?v=XM&prog=ymsgr&.intl=jp&sync=1&tags=short&noclear=1" : "http://address.yahoo.com/yab/us\?v=XM&prog=ymsgr&.intl=us&sync=1&tags=short&noclear=1&useutf8=1&legenc=codepage-1252"));
  purple_url_parse(url,&webaddress,0,&webpage,0,0);
  if ((cb -> id) == ((gchar *)((void *)0))) {
/* No id for this buddy, so create an address book entry */
    purple_debug_info("yahoo","Creating \'%s\' as new alias for user \'%s\'\n",alias,who);
    if ((yd -> jp) != 0) {
      gchar *alias_jp = g_convert(alias,(-1),"EUC-JP","UTF-8",0,0,0);
      gchar *converted_alias_jp = yahoo_convert_to_numeric(alias_jp);
      content = g_strdup_printf("<ab k=\"%s\" cc=\"9\">\n<ct a=\"1\" yi=\'%s\' nn=\'%s\' />\n</ab>\r\n",purple_account_get_username((gc -> account)),who,converted_alias_jp);
      g_free(converted_alias_jp);
      g_free(alias_jp);
    }
    else {
      gchar *escaped_alias = g_markup_escape_text(alias,(-1));
      content = g_strdup_printf("<\?xml version=\"1.0\" encoding=\"utf-8\"\?><ab k=\"%s\" cc=\"9\">\n<ct a=\"1\" yi=\'%s\' nn=\'%s\' />\n</ab>\r\n",purple_account_get_username((gc -> account)),who,escaped_alias);
      g_free(escaped_alias);
    }
  }
  else {
    purple_debug_info("yahoo","Updating \'%s\' as new alias for user \'%s\'\n",alias,who);
    if ((yd -> jp) != 0) {
      gchar *alias_jp = g_convert(alias,(-1),"EUC-JP","UTF-8",0,0,0);
      gchar *converted_alias_jp = yahoo_convert_to_numeric(alias_jp);
      content = g_strdup_printf("<ab k=\"%s\" cc=\"1\">\n<ct e=\"1\"  yi=\'%s\' id=\'%s\' nn=\'%s\' pr=\'0\' />\n</ab>\r\n",purple_account_get_username((gc -> account)),who,(cb -> id),converted_alias_jp);
      g_free(converted_alias_jp);
      g_free(alias_jp);
    }
    else {
      gchar *escaped_alias = g_markup_escape_text(alias,(-1));
      content = g_strdup_printf("<\?xml version=\"1.0\" encoding=\"utf-8\"\?><ab k=\"%s\" cc=\"1\">\n<ct e=\"1\"  yi=\'%s\' id=\'%s\' nn=\'%s\' pr=\'0\' />\n</ab>\r\n",purple_account_get_username((gc -> account)),who,(cb -> id),escaped_alias);
      g_free(escaped_alias);
    }
  }
  request = g_strdup_printf("POST %s%s/%s HTTP/1.1\r\nUser-Agent: Mozilla/4.0 (compatible; MSIE 5.5)\r\nCookie: T=%s; Y=%s\r\nHost: %s\r\nContent-Length: %lu\r\nCache-Control: no-cache\r\n\r\n%s",((use_whole_url != 0)?"http://" : ""),((use_whole_url != 0)?webaddress : ""),webpage,(yd -> cookie_t),(yd -> cookie_y),webaddress,strlen(content),content);
/* We have a URL and some header information, let's connect and update the alias  */
  url_data = purple_util_fetch_url_request_len_with_account(purple_connection_get_account(gc),url,use_whole_url,0,(!0),request,0,(-1),yahoo_update_alias_cb,cb);
  if (url_data != ((PurpleUtilFetchUrlData *)((void *)0))) 
    yd -> url_datas = g_slist_prepend((yd -> url_datas),url_data);
  g_free(webpage);
  g_free(webaddress);
  g_free(content);
  g_free(request);
}
/**************************************************************************
 * User Info Update Functions
 **************************************************************************/
#if 0
/* This block of code can be used to send our contact details to
 * everyone in the buddylist. But with the official messenger,
 * doing this pops a conversation window at the receiver's end,
 * which is stupid, and thus not really surprising. */
/* This creates a conversation window in the official client */
#endif

static void yahoo_set_userinfo_cb(PurpleConnection *gc,PurpleRequestFields *fields)
{
  xmlnode *node = xmlnode_new("ab");
  xmlnode *ct = xmlnode_new_child(node,"ct");
  YahooData *yd = (purple_connection_get_protocol_data(gc));
  PurpleAccount *account;
  PurpleUtilFetchUrlData *url_data;
  char *webaddress;
  char *webpage;
  char *request;
  char *content;
  int len;
  int i;
  char *yfields[] = {("fn"), ("ln"), ("nn"), ("mn"), ("hp"), ("wp"), ("mo"), ((char *)((void *)0))};
  account = purple_connection_get_account(gc);
  xmlnode_set_attrib(node,"k",purple_connection_get_display_name(gc));
/* XXX: ? */
  xmlnode_set_attrib(node,"cc","1");
  xmlnode_set_attrib(ct,"e","1");
  xmlnode_set_attrib(ct,"yi",purple_request_fields_get_string(fields,"yname"));
  xmlnode_set_attrib(ct,"id",purple_request_fields_get_string(fields,"yid"));
  xmlnode_set_attrib(ct,"pr","0");
  for (i = 0; yfields[i] != 0; i++) {
    const char *v = purple_request_fields_get_string(fields,yfields[i]);
    xmlnode_set_attrib(ct,yfields[i],((v != 0)?v : ""));
  }
  content = xmlnode_to_formatted_str(node,&len);
  xmlnode_free(node);
  purple_url_parse((((yd -> jp) != 0)?"http://address.yahoo.co.jp/yab/jp\?v=XM&sync=1&tags=short&useutf8=1&noclear=1&legenc=codepage-1252" : "http://address.yahoo.com/yab/us\?v=XM&sync=1&tags=short&useutf8=1&noclear=1&legenc=codepage-1252"),&webaddress,0,&webpage,0,0);
  request = g_strdup_printf("POST %s HTTP/1.1\r\nUser-Agent: Mozilla/4.0 (compatible; MSIE 5.5)\r\nCookie: T=%s; path=/; domain=.yahoo.com; Y=%s;\r\nHost: %s\r\nContent-Length: %d\r\nCache-Control: no-cache\r\n\r\n%s\r\n\r\n",webpage,(yd -> cookie_t),(yd -> cookie_y),webaddress,(len + 4),content);
#if 0
/* This is if we wanted to send our contact details to everyone
		 * in the buddylist. But this cannot be done now, because in the
		 * official messenger, doing this pops a conversation window at
		 * the receiver's end, which is stupid, and thus not really
		 * surprising. */
#endif
  url_data = purple_util_fetch_url_request_len_with_account(account,webaddress,0,"Mozilla/4.0 (compatible; MSIE 5.5)",(!0),request,0,(-1),yahoo_fetch_aliases_cb,gc);
  if (url_data != ((PurpleUtilFetchUrlData *)((void *)0))) 
    yd -> url_datas = g_slist_prepend((yd -> url_datas),url_data);
  g_free(webaddress);
  g_free(webpage);
  g_free(content);
  g_free(request);
}

static PurpleRequestFields *request_fields_from_personal_details(YahooPersonalDetails *ypd,const char *id)
{
  PurpleRequestFields *fields;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  int i;
  struct __unnamed_class___F0_L539_C2_L410R__L411R__scope____SgSS2___variable_declaration__variable_type___Pb__c__Pe___variable_name_L410R__L411R__scope____SgSS2____scope__id__DELIMITER__L410R__L411R__scope____SgSS2___variable_declaration__variable_type___Pb__c__Pe___variable_name_L410R__L411R__scope____SgSS2____scope__text__DELIMITER__L410R__L411R__scope____SgSS2___variable_declaration__variable_type___Pb__c__Pe___variable_name_L410R__L411R__scope____SgSS2____scope__value {
  char *id;
  char *text;
  char *value;}yfields[] = {{("fn"), ("First Name"), (ypd -> names.first)}, {("ln"), ("Last Name"), (ypd -> names.last)}, {("nn"), ("Nickname"), (ypd -> names.nick)}, {("mn"), ("Middle Name"), (ypd -> names.middle)}, {("hp"), ("Home Phone Number"), (ypd -> phone.home)}, {("wp"), ("Work Phone Number"), (ypd -> phone.work)}, {("mo"), ("Mobile Phone Number"), (ypd -> phone.mobile)}, {((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0))}};
  fields = purple_request_fields_new();
  group = purple_request_field_group_new(0);
  purple_request_fields_add_group(fields,group);
  field = purple_request_field_string_new("yname","",id,0);
  purple_request_field_set_visible(field,0);
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_string_new("yid","",(ypd -> id),0);
  purple_request_field_set_visible(field,0);
  purple_request_field_group_add_field(group,field);
  for (i = 0; yfields[i].id != 0; i++) {
    field = purple_request_field_string_new(yfields[i].id,((const char *)(dgettext("pidgin",yfields[i].text))),yfields[i].value,0);
    purple_request_field_group_add_field(group,field);
  }
  return fields;
}

void yahoo_set_userinfo_for_buddy(PurpleConnection *gc,PurpleBuddy *buddy)
{
  PurpleRequestFields *fields;
  YahooFriend *f;
  const char *name;
  name = purple_buddy_get_name(buddy);
  f = yahoo_friend_find(gc,name);
  if (!(f != 0)) 
    return ;
  fields = request_fields_from_personal_details(&f -> ypd,name);
  purple_request_fields(gc,0,((const char *)(dgettext("pidgin","Set User Info"))),0,fields,((const char *)(dgettext("pidgin","OK"))),((GCallback )yahoo_set_userinfo_cb),((const char *)(dgettext("pidgin","Cancel"))),0,purple_connection_get_account(gc),0,0,gc);
}

void yahoo_set_userinfo(PurpleConnection *gc)
{
  YahooData *yd = (purple_connection_get_protocol_data(gc));
  PurpleRequestFields *fields = request_fields_from_personal_details(&yd -> ypd,purple_connection_get_display_name(gc));
  purple_request_fields(gc,0,((const char *)(dgettext("pidgin","Set User Info"))),0,fields,((const char *)(dgettext("pidgin","OK"))),((GCallback )yahoo_set_userinfo_cb),((const char *)(dgettext("pidgin","Cancel"))),0,purple_connection_get_account(gc),0,0,gc);
}

static gboolean parse_contact_details(YahooData *yd,const char *who,const char *xml)
{
  xmlnode *node;
  xmlnode *nd;
  YahooFriend *f;
  char *yid;
  node = xmlnode_from_str(xml,(-1));
  if (!(node != 0)) {
    purple_debug_info("yahoo","Received malformed XML for contact details from \'%s\':\n%s\n",who,xml);
    return 0;
  }
  nd = xmlnode_get_child(node,"yi");
  if (!(nd != 0) || !((yid = xmlnode_get_data(nd)) != 0)) {
    xmlnode_free(node);
    return 0;
  }
  if (!(purple_strequal(yid,who) != 0)) {
/* The user may not want to set the contact details about folks in the buddylist
		   to what some random dude might have sent. So it would be good if we popped
		   up a prompt requiring the user to confirm the details before we set them.
		   However, someone could send details about hundreds of users at the same time,
		   which would make things really bad. So for now, until we have a better way of
		   dealing with this, ignore this details. */
    purple_debug_info("yahoo","Ignoring contact details sent by %s about %s\n",who,yid);
    g_free(yid);
    xmlnode_free(node);
    return 0;
  }
  f = yahoo_friend_find((yd -> gc),yid);
  if (!(f != 0)) {
    g_free(yid);
    xmlnode_free(node);
    return 0;
  }
  else {
    int i;
    YahooPersonalDetails *ypd = &f -> ypd;
    char *alias = (char *)((void *)0);
    struct __unnamed_class___F0_L647_C3_L415R_variable_declaration__variable_type___Pb__c__Pe___variable_name_L415R__scope__id__DELIMITER__L415R_variable_declaration__variable_type___Pb____Pb__c__Pe____Pe___variable_name_L415R__scope__field {
    char *id;
    char **field;}details[] = {{("fn"), (&ypd -> names.first)}, {("mn"), (&ypd -> names.middle)}, {("ln"), (&ypd -> names.last)}, {("nn"), (&ypd -> names.nick)}, {("wp"), (&ypd -> phone.work)}, {("hp"), (&ypd -> phone.home)}, {("mo"), (&ypd -> phone.mobile)}, {((char *)((void *)0)), ((char **)((void *)0))}};
    yahoo_personal_details_reset(ypd,0);
    for (i = 0; details[i].id != 0; i++) {
      nd = xmlnode_get_child(node,details[i].id);
       *details[i].field = ((nd != 0)?xmlnode_get_data(nd) : ((char *)((void *)0)));
    }
    if (ypd -> names.nick != 0) 
      alias = ypd -> names.nick;
    else if ((ypd -> names.first != 0) || (ypd -> names.last != 0)) {
      alias = g_strchomp(g_strchug(g_strdup_printf("%s %s",((ypd -> names.first != 0)?ypd -> names.first : ""),((ypd -> names.last != 0)?ypd -> names.last : ""))));
    }
    if (alias != 0) {
      serv_got_alias((yd -> gc),yid,alias);
      if (alias != ypd -> names.nick) 
        g_free(alias);
    }
  }
  xmlnode_free(node);
  g_free(yid);
  return (!0);
}
/* I don't think this happens for MSN buddies. -- sad */

void yahoo_process_contact_details(PurpleConnection *gc,struct yahoo_packet *pkt)
{
  GSList *l = (pkt -> hash);
  const char *who = (const char *)((void *)0);
  const char *xml = (const char *)((void *)0);
  YahooData *yd = (purple_connection_get_protocol_data(gc));
  for (; l != 0; l = (l -> next)) {
    struct yahoo_pair *pair = (l -> data);
    switch(pair -> key){
      case 4:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
/* This is the person who sent us the details.
					   But not necessarily about himself. */
          who = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_contact_details got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
      case 5:
{
        break; 
      }
      case 13:
{
/* This is '1' if 'who' is sending the contact details about herself,
				   '0' if 'who' is sending the contact details she has about buddies
				   in her list. However, in all cases, the xml in key 280 always seems
				   to contain the yid of the person, so we may as well ignore this field
				   and look into the xml instead to see who the information is about. */
        break; 
      }
      case 280:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          xml = (pair -> value);
          parse_contact_details(yd,who,xml);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_contact_details got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
    }
  }
}
