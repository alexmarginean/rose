/*
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#define PHOTO_SUPPORT 1
#include "internal.h"
#include "debug.h"
#include "notify.h"
#include "util.h"
#if PHOTO_SUPPORT
#include "imgstore.h"
#endif /* PHOTO_SUPPORT */
#include "libymsg.h"
#include "yahoo_friend.h"
typedef struct __unnamed_class___F0_L37_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L256R__Pe___variable_name_unknown_scope_and_name__scope__gc__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__name {
PurpleConnection *gc;
char *name;}YahooGetInfoData;
typedef enum profile_lang_id {XX,DA,DE,EL,EN,EN_GB,ES_AR,ES_ES,ES_MX,ES_US,FR_CA,FR_FR,IT,JA,KO,NO,PT,SV,ZH_CN,ZH_HK,ZH_TW,ZH_US,PT_BR}profile_lang_id_t;
typedef struct profile_lang_node {
profile_lang_id_t lang;
char *last_updated_string;
char *det;}profile_lang_node_t;
typedef struct profile_strings_node {
profile_lang_id_t lang;
/* Only to make debugging output saner */
char *lang_string;
char *charset;
char *yahoo_id_string;
char *private_string;
char *no_answer_string;
char *my_email_string;
char *realname_string;
char *location_string;
char *age_string;
char *maritalstatus_string;
char *gender_string;
char *occupation_string;
char *hobbies_string;
char *latest_news_string;
char *favorite_quote_string;
char *links_string;
char *no_home_page_specified_string;
char *home_page_string;
char *no_cool_link_specified_string;
char *cool_link_1_string;
char *cool_link_2_string;
char *cool_link_3_string;
char *dummy;}profile_strings_node_t;
typedef enum profile_state {PROFILE_STATE_DEFAULT,PROFILE_STATE_NOT_FOUND,PROFILE_STATE_UNKNOWN_LANGUAGE}profile_state_t;
typedef struct __unnamed_class___F0_L90_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L404R__Pe___variable_name_unknown_scope_and_name__scope__info_data__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L323R__Pe___variable_name_unknown_scope_and_name__scope__user_info__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__url_buffer__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__photo_url_text__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__profile_url_text__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__CL378R__Pe___variable_name_unknown_scope_and_name__scope__strings__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__last_updated_string__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__title__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L379R_variable_name_unknown_scope_and_name__scope__profile_state {
YahooGetInfoData *info_data;
PurpleNotifyUserInfo *user_info;
char *url_buffer;
char *photo_url_text;
char *profile_url_text;
const profile_strings_node_t *strings;
const char *last_updated_string;
const char *title;
profile_state_t profile_state;}YahooGetInfoStepTwoData;
/* Strings to determine the profile "language" (more accurately "locale").
 * Strings in this list must be in the original charset in the profile.
 * The "Last Updated" string is used, but sometimes is not sufficient to
 * distinguish 2 locales with this (e.g., ES_ES from ES_US, or FR_CA from
 * FR_FR, or EL from EN_GB), in which case a second string is added and
 * such special cases must be placed before the more general case.
 */
static const profile_lang_node_t profile_langs[] = {{(DA), ("Opdateret sidste gang&nbsp;"), ((char *)((void *)0))}, {(DE), ("Letzter Update&nbsp;"), ((char *)((void *)0))}, {(EL), ("Last Updated:"), ("http://gr.profiles.yahoo.com")}, {(EN_GB), ("Last Update&nbsp;"), ("Favourite Quote")}, {(EN), ("Last Update:"), ((char *)((void *)0))}, {(EN), ("Last Update&nbsp;"), ((char *)((void *)0))}, {(ES_AR), ("\332ltima actualizaci\363n&nbsp;"), ((char *)((void *)0))}, {(ES_ES), ("Actualizada el&nbsp;"), ("http://es.profiles.yahoo.com")}, {(ES_MX), ("Actualizada el &nbsp;"), ("http://mx.profiles.yahoo.com")}, {(ES_US), ("Actualizada el &nbsp;"), ((char *)((void *)0))}, {(FR_CA), ("Derni\350re mise \340 jour"), ("http://cf.profiles.yahoo.com")}, {(FR_FR), ("Derni\350re mise \340 jour"), ((char *)((void *)0))}, {(IT), ("Ultimo aggiornamento:"), ((char *)((void *)0))}, {(JA), ("\272\307\275\252\271\271\277\267\306\374\241\247"), ((char *)((void *)0))}, {(KO), ("\260\273\275\305 \263\257\302\245&nbsp;"), ((char *)((void *)0))}, {(NO), ("Sist oppdatert&nbsp;"), ((char *)((void *)0))}, {(PT), ("\332ltima atualiza\347\343o&nbsp;"), ((char *)((void *)0))}, {(PT_BR), ("\332ltima atualiza\347\343o:"), ((char *)((void *)0))}, {(SV), ("Senast uppdaterad&nbsp;"), ((char *)((void *)0))}, {(ZH_CN), ("\327\356\272\363\320\336\270\304\310\325\306\332"), ((char *)((void *)0))}, {(ZH_HK), ("\263\314\252\361\247\363\267s\256\311\266\241"), ((char *)((void *)0))}, {(ZH_US), ("\263\314\253\341\255\327\247\357\244\351\264\301"), ("http://chinese.profiles.yahoo.com")}, {(ZH_TW), ("\263\314\253\341\255\327\247\357\244\351\264\301"), ((char *)((void *)0))}, {(XX), ((char *)((void *)0)), ((char *)((void *)0))}};
/* Strings in this list must be in UTF-8; &nbsp;'s should be specified as spaces. */
static const profile_strings_node_t profile_strings[] = {{(DA), ("da"), ("ISO-8859-1"), ("Yahoo! ID:"), ("Privat"), ("Intet svar"), ("Min Email"), ("Rigtige navn:"), ("Opholdssted:"), ("Alder:"), ("\303\206gteskabelig status:"), ("K\303\270n:"), ("Erhverv:"), ("Hobbyer:"), ("Sidste nyt:"), ("Favoritcitat"), ("Links"), ("Ingen hjemmeside specificeret"), ("Forside:"), ("Intet cool link specificeret"), ("Cool link 1:"), ("Cool link 2:"), ("Cool link 3:"), ((char *)((void *)0))}, {(DE), ("de"), ("ISO-8859-1"), ("Yahoo!-ID:"), ("Privat"), ("Keine Antwort"), ("Meine E-Mail"), ("Realer Name:"), ("Ort:"), ("Alter:"), ("Familienstand:"), ("Geschlecht:"), ("Beruf:"), ("Hobbys:"), ("Neuste Nachrichten:"), ("Mein Lieblingsspruch"), ("Links"), ("Keine Homepage angegeben"), ("Homepage:"), ("Keinen coolen Link angegeben"), ("Cooler Link 1:"), ("Cooler Link 2:"), ("Cooler Link 3:"), ((char *)((void *)0))}, 
/* EL is identical to EN, except no_answer_string */
{(EL), ("el"), ("ISO-8859-7"), ("Yahoo! ID:"), ("Private"), ("\316\232\316\261\316\274\316\257\316\261 \316\261\317\200\316\254\316\275\317\204\316\267\317\203\316\267"), ("My Email"), ("Real Name:"), ("Location:"), ("Age:"), ("Marital Status:"), ("Gender:"), ("Occupation:"), ("Hobbies:"), ("Latest News"), ("Favorite Quote"), ("Links"), ("No home page specified"), ("Home Page:"), ("No cool link specified"), ("Cool Link 1:"), ("Cool Link 2:"), ("Cool Link 3:"), ((char *)((void *)0))}, {(EN), ("en"), ("ISO-8859-1"), ("Yahoo! ID:"), ("Private"), ("No Answer"), ("My Email:"), ("Real Name:"), ("Location:"), ("Age:"), ("Marital Status:"), ("Sex:"), ("Occupation:"), ("Hobbies"), ("Latest News"), ("Favorite Quote"), ("Links"), ("No home page specified"), ("Home Page:"), ("No cool link specified"), ("Cool Link 1"), ("Cool Link 2"), ("Cool Link 3"), ((char *)((void *)0))}, 
/* Same as EN except spelling of "Favourite" */
{(EN_GB), ("en_GB"), ("ISO-8859-1"), ("Yahoo! ID:"), ("Private"), ("No Answer"), ("My Email:"), ("Real Name:"), ("Location:"), ("Age:"), ("Marital Status:"), ("Sex:"), ("Occupation:"), ("Hobbies"), ("Latest News"), ("Favourite Quote"), ("Links"), ("No home page specified"), ("Home Page:"), ("No cool link specified"), ("Cool Link 1"), ("Cool Link 2"), ("Cool Link 3"), ((char *)((void *)0))}, {(ES_AR), ("es_AR"), ("ISO-8859-1"), ("Usuario de Yahoo!:"), ("Privado"), ("No introdujiste una respuesta"), ("Mi direcci\303\263n de correo electr\303\263nico"), ("Nombre real:"), ("Ubicaci\303\263n:"), ("Edad:"), ("Estado civil:"), ("Sexo:"), ("Ocupaci\303\263n:"), ("Pasatiempos:"), ("\303\232ltimas noticias:"), ("Tu cita favorita"), ("Enlaces"), ("Ninguna p\303\241gina de inicio especificada"), ("P\303\241gina de inicio:"), ("Ning\303\272n enlace preferido"), ("Enlace genial 1:"), ("Enlace genial 2:"), ("Enlace genial 3:"), ((char *)((void *)0))}, {(ES_ES), ("es_ES"), ("ISO-8859-1"), ("ID de Yahoo!:"), ("Privado"), ("Sin respuesta"), ("Mi correo-e"), ("Nombre verdadero:"), ("Lugar:"), ("Edad:"), ("Estado civil:"), ("Sexo:"), ("Ocupaci\303\263n:"), ("Aficiones:"), ("Ultimas Noticias:"), ("Tu cita Favorita"), ("Enlace"), ("Ninguna p\303\241gina personal especificada"), ("P\303\241gina de Inicio:"), ("Ning\303\272n enlace preferido"), ("Enlaces Preferidos 1:"), ("Enlaces Preferidos 2:"), ("Enlaces Preferidos 3:"), ((char *)((void *)0))}, {(ES_MX), ("es_MX"), ("ISO-8859-1"), ("ID de Yahoo!:"), ("Privado"), ("Sin responder"), ("Mi Direcci\303\263n de correo-e"), ("Nombre real:"), ("Ubicaci\303\263n:"), ("Edad:"), ("Estado civil:"), ("Sexo:"), ("Ocupaci\303\263n:"), ("Pasatiempos:"), ("Ultimas Noticias:"), ("Su cita favorita"), ("Enlaces"), ("Ninguna P\303\241gina predefinida"), ("P\303\241gina web:"), ("Ning\303\272n Enlace preferido"), ("Enlaces Preferidos 1:"), ("Enlaces Preferidos 2:"), ("Enlaces Preferidos 3:"), ((char *)((void *)0))}, {(ES_US), ("es_US"), ("ISO-8859-1"), ("ID de Yahoo!:"), ("Privado"), ("No introdujo una respuesta"), ("Mi Direcci\303\263n de correo-e"), ("Nombre real:"), ("Localidad:"), ("Edad:"), ("Estado civil:"), ("Sexo:"), ("Ocupaci\303\263n:"), ("Pasatiempos:"), ("Ultimas Noticias:"), ("Su cita Favorita"), ("Enlaces"), ("Ninguna P\303\241gina de inicio predefinida"), ("P\303\241gina de inicio:"), ("Ning\303\272n Enlace preferido"), ("Enlaces Preferidos 1:"), ("Enlaces Preferidos 2:"), ("Enlaces Preferidos 3:"), ((char *)((void *)0))}, {(FR_CA), ("fr_CA"), ("ISO-8859-1"), ("Compte Yahoo!:"), ("Priv\303\251"), ("Sans r\303\251ponse"), ("Mon courriel"), ("Nom r\303\251el:"), ("Lieu:"), ("\303\202ge:"), ("\303\211tat civil:"), ("Sexe:"), ("Profession:"), ("Passe-temps:"), ("Actualit\303\251s:"), ("Citation pr\303\251f\303\251r\303\251e"), ("Liens"), ("Pas de mention d\'une page personnelle"), ("Page personnelle:"), ("Pas de mention d\'un lien favori"), ("Lien pr\303\251f\303\251r\303\251 1:"), ("Lien pr\303\251f\303\251r\303\251 2:"), ("Lien pr\303\251f\303\251r\303\251 3:"), ((char *)((void *)0))}, {(FR_FR), ("fr_FR"), ("ISO-8859-1"), ("Compte Yahoo!:"), ("Priv\303\251"), ("Sans r\303\251ponse"), ("Mon E-mail"), ("Nom r\303\251el:"), ("Lieu:"), ("\303\202ge:"), ("Situation de famille:"), ("Sexe:"), ("Profession:"), ("Centres d\'int\303\251r\303\252ts:"), ("Actualit\303\251s:"), ("Citation pr\303\251f\303\251r\303\251e"), ("Liens"), ("Pas de mention d\'une page perso"), ("Page perso:"), ("Pas de mention d\'un lien favori"), ("Lien pr\303\251f\303\251r\303\251 1:"), ("Lien pr\303\251f\303\251r\303\251 2:"), ("Lien pr\303\251f\303\251r\303\251 3:"), ((char *)((void *)0))}, {(IT), ("it"), ("ISO-8859-1"), ("Yahoo! ID:"), ("Non pubblica"), ("Nessuna risposta"), ("La mia e-mail:"), ("Nome vero:"), ("Localit\303\240:"), ("Et\303\240:"), ("Stato civile:"), ("Sesso:"), ("Occupazione:"), ("Hobby"), ("Ultime notizie"), ("Citazione preferita"), ("Link"), ("Nessuna home page specificata"), ("Inizio:"), ("Nessun link specificato"), ("Cool Link 1"), ("Cool Link 2"), ("Cool Link 3"), ((char *)((void *)0))}, 
#if 0
#else
/* "Self description" comes before "Links" for yahoo.co.jp */
#endif
{(JA), ("ja"), ("EUC-JP"), ("Yahoo! JAPAN ID\357\274\232"), ("\351\235\236\345\205\254\351\226\213"), ("\347\204\241\345\233\236\347\255\224"), ("\343\203\241\343\203\274\343\203\253\357\274\232"), ("\345\220\215\345\211\215\357\274\232"), ("\344\275\217\346\211\200\357\274\232"), ("\345\271\264\351\275\242\357\274\232"), ("\346\234\252\345\251\232/\346\227\242\345\251\232\357\274\232"), ("\346\200\247\345\210\245\357\274\232"), ("\350\201\267\346\245\255\357\274\232"), ("\350\266\243\345\221\263\357\274\232"), ("\346\234\200\350\277\221\343\201\256\345\207\272\346\235\245\344\272\213\357\274\232"), ((char *)((void *)0)), ("\350\207\252\345\267\261PR"), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ("\343\201\212\343\201\231\343\201\231\343\202\201\343\202\265\343\202\244\343\203\2101\357\274\232"), ("\343\201\212\343\201\231\343\201\231\343\202\201\343\202\265\343\202\244\343\203\2102\357\274\232"), ("\343\201\212\343\201\231\343\201\231\343\202\201\343\202\265\343\202\244\343\203\2103\357\274\232"), ((char *)((void *)0))}, {(KO), ("ko"), ("EUC-KR"), ("\354\225\274\355\233\204! ID:"), ("\353\271\204\352\263\265\352\260\234"), ("\353\271\204\352\263\265\352\260\234"), ("My Email"), ("\354\213\244\353\252\205:"), ("\352\261\260\354\243\274\354\247\200:"), ("\353\202\230\354\235\264:"), ("\352\262\260\355\230\274 \354\227\254\353\266\200:"), ("\354\204\261\353\263\204:"), ("\354\247\201\354\227\205:"), ("\354\267\250\353\257\270:"), ("\354\236\220\352\270\260 \354\206\214\352\260\234:"), ("\354\242\213\354\225\204\355\225\230\353\212\224 \353\252\205\354\226\270"), ("\353\247\201\355\201\254"), ("\355\231\210\355\216\230\354\235\264\354\247\200\353\245\274 \354\247\200\354\240\225\355\225\230\354\247\200 \354\225\212\354\225\230\354\212\265\353\213\210\353\213\244."), ("\355\231\210\355\216\230\354\235\264\354\247\200:"), ("\354\266\224\354\262\234 \354\202\254\354\235\264\355\212\270\352\260\200 \354\227\206\354\212\265\353\213\210\353\213\244."), ("\354\266\224\354\262\234 \354\202\254\354\235\264\355\212\270 1:"), ("\354\266\224\354\262\234 \354\202\254\354\235\264\355\212\270 2:"), ("\354\266\224\354\262\234 \354\202\254\354\235\264\355\212\270 3:"), ((char *)((void *)0))}, {(NO), ("no"), ("ISO-8859-1"), ("Yahoo! ID:"), ("Privat"), ("Ikke noe svar"), ("Min e-post"), ("Virkelig navn:"), ("Sted:"), ("Alder:"), ("Sivilstatus:"), ("Kj\303\270nn:"), ("Yrke:"), ("Hobbyer:"), ("Siste nytt:"), ("Yndlingssitat"), ("Lenker"), ("Ingen hjemmeside angitt"), ("Hjemmeside:"), ("No cool link specified"), ("Bra lenke 1:"), ("Bra lenke 2:"), ("Bra lenke 3:"), ((char *)((void *)0))}, {(PT), ("pt"), ("ISO-8859-1"), ("ID Yahoo!:"), ("Particular"), ("Sem resposta"), ("Meu e-mail"), ("Nome verdadeiro:"), ("Local:"), ("Idade:"), ("Estado civil:"), ("Sexo:"), ("Ocupa\303\247\303\243o:"), ("Hobbies:"), ("\303\232ltimas not\303\255cias:"), ("Frase favorita"), ("Links"), ("Nenhuma p\303\241gina pessoal especificada"), ("P\303\241gina pessoal:"), ("Nenhum site legal especificado"), ("Site legal 1:"), ("Site legal 2:"), ("Site legal 3:"), ((char *)((void *)0))}, {(PT_BR), ("pt_br"), ("ISO-8859-1"), ("ID Yahoo!:"), ("Particular"), ("Sem resposta"), ("Meu e-mail"), ("Nome verdadeiro:"), ("Localiza\303\247\303\243o:"), ("Idade:"), ("Estado civil:"), ("Sexo:"), ("Ocupa\303\247\303\243o:"), ("Pasatiempos:"), ("\303\232ltimas novidades:"), ("Frase preferida:"), ("Links"), ("Nenhuma home page especificada"), ("P\303\241gina Web:"), ("Nenhum site legal especificado"), ("Link legal 1"), ("Link legal 2"), ("Link legal 3"), ((char *)((void *)0))}, {(SV), ("sv"), ("ISO-8859-1"), ("Yahoo!-ID:"), ("Privat"), ("Inget svar"), ("Min mail"), ("Riktigt namn:"), ("Plats:"), ("\303\205lder:"), ("Civilst\303\245nd:"), ("K\303\266n:"), ("Yrke:"), ("Hobby:"), ("Senaste nytt:"), ("Favoritcitat"), ("L\303\244nkar"), ("Ingen hemsida specificerad"), ("Hemsida:"), ("Ingen cool l\303\244nk specificerad"), ("Coola l\303\244nkar 1:"), ("Coola l\303\244nkar 2:"), ("Coola l\303\244nkar 3:"), ((char *)((void *)0))}, {(ZH_CN), ("zh_CN"), ("GB2312"), ("Yahoo! ID:"), ("\346\262\241\346\234\211\346\217\220\344\276\233"), ("\346\262\241\346\234\211\345\233\236\347\255\224"), ("\344\270\252\344\272\272\347\224\265\351\202\256\345\234\260\345\235\200"), ("\347\234\237\345\256\236\345\247\223\345\220\215:"), ("\346\211\200\345\234\250\345\234\260\347\202\271:"), ("\345\271\264\351\276\204:"), ("\345\251\232\345\247\273\347\212\266\345\206\265:"), ("\346\200\247\345\210\253:"), ("\350\201\214\344\270\232:"), ("\344\270\232\344\275\231\347\210\261\345\245\275:"), ("\344\270\252\344\272\272\350\277\221\345\206\265:"), ("\345\226\234\346\254\242\347\232\204\345\274\225\350\250\200"), ("\351\223\276\346\216\245"), ("\346\262\241\346\234\211\344\270\252\344\272\272\344\270\273\351\241\265"), ("\344\270\252\344\272\272\344\270\273\351\241\265:"), ("\346\262\241\346\234\211\346\216\250\350\215\220\347\275\221\347\253\231\351\223\276\346\216\245"), ("\346\216\250\350\215\220\347\275\221\347\253\231\351\223\276\346\216\245 1:"), ("\346\216\250\350\215\220\347\275\221\347\253\231\351\223\276\346\216\245 2:"), ("\346\216\250\350\215\220\347\275\221\347\253\231\351\223\276\346\216\245 3:"), ((char *)((void *)0))}, 
/* [sic] Yahoo!'s translators don't check context */
/* [sic] */
/* [sic] */
/* TODO */
/* TODO */
/* TODO */
{(ZH_HK), ("zh_HK"), ("Big5"), ("Yahoo! ID:"), ("\347\247\201\344\272\272\347\232\204"), ("\346\262\222\346\234\211\345\233\236\347\255\224"), ("\351\233\273\345\255\220\344\277\241\347\256\261"), ("\347\234\237\345\257\246\345\247\223\345\220\215:"), ("\345\234\260\351\273\236:"), ("\345\271\264\351\275\241:"), ("\345\251\232\345\247\273\347\213\200\346\263\201:"), ("\346\200\247\345\210\245:"), ("\350\201\267\346\245\255:"), ("\345\227\234\345\245\275:"), ("\346\234\200\346\226\260\346\266\210\346\201\257:"), ("\346\234\200\345\226\234\346\204\233\347\232\204\350\202\241\347\245\250\345\217\253\345\203\271"), ("\351\200\243\347\265\220"), ("\346\262\222\346\234\211\346\263\250\346\230\216\345\200\213\344\272\272\347\266\262\351\240\201"), ("\345\200\213\344\272\272\347\266\262\351\240\201:"), ("\346\262\222\346\234\211\346\263\250\346\230\216 Cool \351\200\243\347\265\220"), ("Cool \351\200\243\347\265\220 1:"), ("Cool \351\200\243\347\265\220 2:"), ("Cool \351\200\243\347\265\220 3:"), ((char *)((void *)0))}, {(ZH_TW), ("zh_TW"), ("Big5"), ("\345\270\263 \350\231\237:"), ("\346\262\222\346\234\211\346\217\220\344\276\233"), ("\346\262\222\346\234\211\345\233\236\346\207\211"), ("\351\233\273\345\255\220\344\277\241\347\256\261"), ("\345\247\223\345\220\215:"), ("\345\234\260\351\273\236:"), ("\345\271\264\351\275\241:"), ("\345\251\232\345\247\273\347\213\200\346\205\213:"), ("\346\200\247\345\210\245:"), ("\350\201\267\346\245\255:"), ("\350\210\210\350\266\243:"), ("\345\200\213\344\272\272\350\277\221\346\263\201:"), ("\345\226\234\346\255\241\347\232\204\345\220\215\345\217\245"), ("\351\200\243\347\265\220"), ("\346\262\222\346\234\211\345\200\213\344\272\272\347\266\262\351\240\201"), ("\345\200\213\344\272\272\347\266\262\351\240\201:"), ("\346\262\222\346\234\211\346\216\250\350\226\246\347\266\262\347\253\231\351\200\243\347\265\220"), ("\346\216\250\350\226\246\347\266\262\347\253\231\351\200\243\347\265\220 1:"), ("\346\216\250\350\226\246\347\266\262\347\253\231\351\200\243\347\265\220 2:"), ("\346\216\250\350\226\246\347\266\262\347\253\231\351\200\243\347\265\220 3:"), ((char *)((void *)0))}, 
/* ZH_US is like ZH_TW, but also a bit like ZH_HK */
/* TODO */
/* TODO */
/* TODO */
{(ZH_US), ("zh_US"), ("Big5"), ("Yahoo! ID:"), ("\346\262\222\346\234\211\346\217\220\344\276\233"), ("\346\262\222\346\234\211\345\233\236\347\255\224"), ("\345\200\213\344\272\272Email\345\234\260\345\235\200"), ("\347\234\237\345\257\246\345\247\223\345\220\215:"), ("\345\234\260\351\273\236:"), ("\345\271\264\351\275\241:"), ("\345\251\232\345\247\273\347\213\200\346\205\213:"), ("\346\200\247\345\210\245:"), ("\350\201\267\346\245\255:"), ("\345\227\234\345\245\275:"), ("\345\200\213\344\272\272\350\277\221\346\263\201:"), ("\345\226\234\346\255\241\347\232\204\345\220\215\345\217\245"), ("\351\200\243\347\265\220"), ("\346\262\222\346\234\211\345\200\213\344\272\272\347\266\262\351\240\201"), ("\345\200\213\344\272\272\347\266\262\351\240\201:"), ("\346\262\222\346\234\211\346\216\250\350\226\246\347\266\262\347\253\231\351\200\243\347\265\220"), ("\346\216\250\350\226\246\347\266\262\347\253\231\351\200\243\347\265\220 1:"), ("\346\216\250\350\226\246\347\266\262\347\253\231\351\200\243\347\265\220 2:"), ("\346\216\250\350\226\246\347\266\262\347\253\231\351\200\243\347\265\220 3:"), ((char *)((void *)0))}, {(XX), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0))}};

static char *yahoo_info_date_reformat(const char *field,size_t len)
{
  char *tmp = g_strndup(field,len);
  time_t t = purple_str_to_time(tmp,0,0,0,0);
  g_free(tmp);
  return g_strdup(purple_date_format_short((localtime((&t)))));
}

static char *yahoo_remove_nonbreaking_spaces(char *str)
{
  char *p;
  while((p = strstr(str,"&nbsp;")) != ((char *)((void *)0))){
/* Turn &nbsp;'s into ordinary blanks */
     *p = 32;
    p += 1;
    memmove(p,(p + 5),strlen((p + 5)));
    str[strlen(str) - 5] = 0;
  }
  return str;
}

static void yahoo_extract_user_info_text(PurpleNotifyUserInfo *user_info,YahooGetInfoData *info_data)
{
  PurpleBuddy *b;
  YahooFriend *f;
  b = purple_find_buddy(purple_connection_get_account((info_data -> gc)),(info_data -> name));
  if (b != 0) {
    const char *balias = purple_buddy_get_local_buddy_alias(b);
    if ((balias != 0) && (balias[0] != 0)) {
      purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Alias"))),balias);
    }
		#if 0
		#endif
/* Add the normal tooltip pairs */
    yahoo_tooltip_text(b,user_info,(!0));
    if ((f = yahoo_friend_find((info_data -> gc),purple_buddy_get_name(b))) != 0) {
      const char *ip;
      if ((ip = yahoo_friend_get_ip(f)) != 0) 
        purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","IP Address"))),ip);
    }
  }
}
#if PHOTO_SUPPORT

static char *yahoo_get_photo_url(const char *url_text,const char *name)
{
  GString *s = g_string_sized_new((strlen(name) + 8));
  char *p;
  char *it = (char *)((void *)0);
/*g_string_printf(s, " alt=\"%s\">", name);*/
/* Y! newformat */
  g_string_printf(s," alt=%s>",name);
  p = strstr(url_text,(s -> str));
  if (p != 0) {
/* Search backwards for "http://". This is stupid, but it works. */
    for (; !(it != 0) && (p > url_text); p -= 1) {
/*if (strncmp(p, "\"http://", 8) == 0) {*/
/* Y! newformat*/
      if (strncmp(p,"=http://",8) == 0) {
        char *q;
/* skip only the ' ' */
        p += 1;
        q = strchr(p,32);
        if (q != 0) {
          g_free(it);
          it = g_strndup(p,(q - p));
        }
      }
    }
  }
  g_string_free(s,(!0));
  return it;
}
static void yahoo_got_photo(PurpleUtilFetchUrlData *url_data,gpointer data,const gchar *url_text,size_t len,const gchar *error_message);
#endif /* PHOTO_SUPPORT */

static void yahoo_got_info(PurpleUtilFetchUrlData *url_data,gpointer user_data,const gchar *url_text,size_t len,const gchar *error_message)
{
  YahooGetInfoData *info_data = (YahooGetInfoData *)user_data;
  PurpleNotifyUserInfo *user_info;
  char *p;
#if PHOTO_SUPPORT
  YahooGetInfoStepTwoData *info2_data;
  char *photo_url_text = (char *)((void *)0);
#else
#endif /* !PHOTO_SUPPORT */
  const char *last_updated_string = (const char *)((void *)0);
  char *url_buffer;
  GString *s;
  char *tmp;
  char *profile_url_text = (char *)((void *)0);
  int lang;
  int strid;
  YahooData *yd;
  const profile_strings_node_t *strings = (const profile_strings_node_t *)((void *)0);
  const char *title;
  profile_state_t profile_state = PROFILE_STATE_DEFAULT;
  purple_debug_info("yahoo","In yahoo_got_info\n");
  yd = ( *(info_data -> gc)).proto_data;
  yd -> url_datas = g_slist_remove((yd -> url_datas),url_data);
  user_info = purple_notify_user_info_new();
  title = (((yd -> jp) != 0)?((const char *)(dgettext("pidgin","Yahoo! Japan Profile"))) : ((const char *)(dgettext("pidgin","Yahoo! Profile"))));
/* Get the tooltip info string */
  yahoo_extract_user_info_text(user_info,info_data);
/* We failed to grab the profile URL.  This is not expected to actually
	 * happen except under unusual error conditions, as Yahoo is observed
	 * to send back HTML, with a 200 status code.
	 */
  if (((error_message != ((const gchar *)((void *)0))) || (url_text == ((const gchar *)((void *)0)))) || (strcmp(url_text,"") == 0)) {
    purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Error retrieving profile"))),0);
    purple_notify_userinfo((info_data -> gc),(info_data -> name),user_info,0,0);
    purple_notify_user_info_destroy(user_info);
    g_free(profile_url_text);
    g_free((info_data -> name));
    g_free(info_data);
    return ;
  }
/* Construct the correct profile URL */
/* wild guess */
  s = g_string_sized_new(80);
  g_string_printf(s,"%s%s",(((yd -> jp) != 0)?"http://profiles.yahoo.co.jp/" : "http://profiles.yahoo.com/"),(info_data -> name));
  profile_url_text = g_string_free(s,0);
  s = ((GString *)((void *)0));
/* We don't yet support the multiple link level of the warning page for
	 * 'adult' profiles, not to mention the fact that yahoo wants you to be
	 * logged in (on the website) to be able to view an 'adult' profile.  For
	 * now, just tell them that we can't help them, and provide a link to the
	 * profile if they want to do the web browser thing.
	 */
  p = strstr(url_text,"Adult Profiles Warning Message");
  if (!(p != 0)) {
/* TITLE element */
    p = strstr(url_text,"Adult Content Warning");
  }
  if (p != 0) {
    tmp = g_strdup_printf("<b>%s</b><br><br>%s<br><a href=\"%s\">%s</a>",((const char *)(dgettext("pidgin","Sorry, profiles marked as containing adult content are not supported at this time."))),((const char *)(dgettext("pidgin","If you wish to view this profile, you will need to visit this link in your web browser:"))),profile_url_text,profile_url_text);
    purple_notify_user_info_add_pair(user_info,0,tmp);
    g_free(tmp);
    purple_notify_userinfo((info_data -> gc),(info_data -> name),user_info,0,0);
    g_free(profile_url_text);
    purple_notify_user_info_destroy(user_info);
    g_free((info_data -> name));
    g_free(info_data);
    return ;
  }
{
/* Check whether the profile is written in a supported language */
    for (lang = 0; ; lang += 1) {
      last_updated_string = profile_langs[lang].last_updated_string;
      if (!(last_updated_string != 0)) 
        break; 
      p = strstr(url_text,last_updated_string);
      if (p != 0) {
        if ((profile_langs[lang].det != 0) && !(strstr(url_text,profile_langs[lang].det) != 0)) 
          p = ((char *)((void *)0));
        else 
          break; 
      }
    }
  }
  if (p != 0) {{
      for (strid = 0; profile_strings[strid].lang != XX; strid += 1) {
        if (profile_strings[strid].lang == profile_langs[lang].lang) 
          break; 
      }
    }
    strings = (profile_strings + strid);
    purple_debug_info("yahoo","detected profile lang = %s (%d)\n",profile_strings[strid].lang_string,lang);
  }
/* Every user may choose his/her own profile language, and this language
	 * has nothing to do with the preferences of the user which looks at the
	 * profile. We try to support all languages, but nothing is guaranteed.
	 * If we cannot determine the language, it means either (1) the profile
	 * is written in an unsupported language, (2) our language support is
	 * out of date, or (3) the user is not found, or (4) Y! have changed their
	 * webpage layout
	 */
  if (!(p != 0) || ((strings -> lang) == XX)) {
    if ((!(strstr(url_text,"Yahoo! Member Directory - User not found") != 0) && !(strstr(url_text,"was not found on this server.") != 0)) && !(strstr(url_text,"\270\370\263\253\245\327\245\355\245\325\245\243\241\274\245\353\244\254\270\253\244\304\244\253\244\352\244\336\244\273\244\363") != 0)) {
      profile_state = PROFILE_STATE_UNKNOWN_LANGUAGE;
    }
    else {
      profile_state = PROFILE_STATE_NOT_FOUND;
    }
  }
#if PHOTO_SUPPORT
  photo_url_text = yahoo_get_photo_url(url_text,(info_data -> name));
#endif /* PHOTO_SUPPORT */
  url_buffer = g_strdup(url_text);
/*
	 * purple_markup_strip_html() doesn't strip out character entities like &nbsp;
	 * and &#183;
	*/
  yahoo_remove_nonbreaking_spaces(url_buffer);
#if 1
  while((p = strstr(url_buffer,"&#183;")) != ((char *)((void *)0))){
    memmove(p,(p + 6),strlen((p + 6)));
    url_buffer[strlen(url_buffer) - 6] = 0;
  }
#endif
/* nuke the nasty \r's */
  purple_str_strip_char(url_buffer,13);
#if PHOTO_SUPPORT
/* Marshall the existing state */
  info2_data = (g_malloc((sizeof(YahooGetInfoStepTwoData ))));
  info2_data -> info_data = info_data;
  info2_data -> url_buffer = url_buffer;
  info2_data -> photo_url_text = photo_url_text;
  info2_data -> profile_url_text = profile_url_text;
  info2_data -> strings = strings;
  info2_data -> last_updated_string = last_updated_string;
  info2_data -> title = title;
  info2_data -> profile_state = profile_state;
  info2_data -> user_info = user_info;
/* Try to put the photo in there too, if there's one */
  if (photo_url_text != 0) {
    PurpleUtilFetchUrlData *url_data;
/* use whole URL if using HTTP Proxy */
    gboolean use_whole_url = yahoo_account_use_http_proxy((info_data -> gc));
/* User-uploaded photos use a different server that requires the Host
		 * header, but Yahoo Japan will use the "chunked" content encoding if
		 * we specify HTTP 1.1. So we have to specify 1.0 & fix purple_util_fetch_url
		 */
    url_data = purple_util_fetch_url_request(photo_url_text,use_whole_url,0,0,0,0,yahoo_got_photo,info2_data);;
    if (url_data != ((PurpleUtilFetchUrlData *)((void *)0))) 
      yd -> url_datas = g_slist_prepend((yd -> url_datas),url_data);
  }
  else {
/* Emulate a callback */
    yahoo_got_photo(0,info2_data,0,0,0);
  }
}

static void yahoo_got_photo(PurpleUtilFetchUrlData *url_data,gpointer data,const gchar *url_text,size_t len,const gchar *error_message)
{
  YahooGetInfoStepTwoData *info2_data = (YahooGetInfoStepTwoData *)data;
  YahooData *yd;
  gboolean found = 0;
  int id = -1;
/* Temporary variables */
  char *p = (char *)((void *)0);
  char *stripped;
  int stripped_len;
  char *last_updated_utf8_string = (char *)((void *)0);
  char *tmp;
/* Unmarshall the saved state */
  YahooGetInfoData *info_data = (info2_data -> info_data);
  char *url_buffer = (info2_data -> url_buffer);
  PurpleNotifyUserInfo *user_info = (info2_data -> user_info);
  char *photo_url_text = (info2_data -> photo_url_text);
  char *profile_url_text = (info2_data -> profile_url_text);
  const profile_strings_node_t *strings = (info2_data -> strings);
  const char *last_updated_string = (info2_data -> last_updated_string);
  profile_state_t profile_state = (info2_data -> profile_state);
/* We continue here from yahoo_got_info, as if nothing has happened */
#endif /* PHOTO_SUPPORT */
/* Jun 29 05 Bleeter: Y! changed their profile pages. Terminators now seem to be */
/* </dd> and not \n. The prpl's need to be audited before it can be moved */
/* in to purple_markup_strip_html*/
  char *fudged_buffer;
  yd = ( *(info_data -> gc)).proto_data;
  yd -> url_datas = g_slist_remove((yd -> url_datas),url_data);
  fudged_buffer = purple_strcasereplace(url_buffer,"</dd>","</dd><br>");
/* nuke the html, it's easier than trying to parse the horrid stuff */
  stripped = purple_markup_strip_html(fudged_buffer);
  stripped_len = (strlen(stripped));
  purple_debug_misc("yahoo","stripped = %p\n",stripped);
  purple_debug_misc("yahoo","url_buffer = %p\n",url_buffer);
/* convert to utf8 */
  if ((strings != 0) && ((strings -> charset) != 0)) {
    p = g_convert(stripped,(-1),"utf-8",(strings -> charset),0,0,0);
    if (!(p != 0)) {
      p = g_locale_to_utf8(stripped,(-1),0,0,0);
      if (!(p != 0)) {
        p = g_convert(stripped,(-1),"utf-8","windows-1252",0,0,0);
      }
    }
    if (p != 0) {
      g_free(stripped);
      stripped = purple_utf8_ncr_decode(p);
      stripped_len = (strlen(stripped));
      g_free(p);
    }
  }
  p = ((char *)((void *)0));
/* "Last updated" should also be converted to utf8 and with &nbsp; killed */
  if ((strings != 0) && ((strings -> charset) != 0)) {
    last_updated_utf8_string = g_convert(last_updated_string,(-1),"utf-8",(strings -> charset),0,0,0);
    yahoo_remove_nonbreaking_spaces(last_updated_utf8_string);
    purple_debug_misc("yahoo","after utf8 conversion: stripped = (%s)\n",stripped);
  }
  if (profile_state == PROFILE_STATE_DEFAULT) {
#if 0
/* extract their Yahoo! ID and put it in. Don't bother marking has_info as
	 * true, since the Yahoo! ID will always be there */
#endif
#if PHOTO_SUPPORT
/* Try to put the photo in there too, if there's one and is readable */
    if ((url_text != 0) && (len != 0)) {
      if (((strstr(url_text,"400 Bad Request") != 0) || (strstr(url_text,"403 Forbidden") != 0)) || (strstr(url_text,"404 Not Found") != 0)) {
        purple_debug_info("yahoo","Error getting %s: %s\n",photo_url_text,url_text);
      }
      else {
        purple_debug_info("yahoo","%s is %lu bytes\n",photo_url_text,len);
        id = purple_imgstore_add_with_id(g_memdup(url_text,len),len,0);
        tmp = g_strdup_printf("<img id=\"%d\"><br>",id);
        purple_notify_user_info_add_pair(user_info,0,tmp);
        g_free(tmp);
      }
    }
#endif /* PHOTO_SUPPORT */
/* extract their Email address and put it in */
    found |= purple_markup_extract_info_field(stripped,stripped_len,user_info,(strings -> my_email_string),(((yd -> jp) != 0)?4 : 1)," ",0,(strings -> private_string),((const char *)(dgettext("pidgin","Email"))),0,0,0);
/* extract the Nickname if it exists */
    found |= purple_markup_extract_info_field(stripped,stripped_len,user_info,"Nickname:",1,"\n",10,0,((const char *)(dgettext("pidgin","Nickname"))),0,0,0);
/* extract their RealName and put it in */
    found |= purple_markup_extract_info_field(stripped,stripped_len,user_info,(strings -> realname_string),(((yd -> jp) != 0)?3 : 1),"\n",10,0,((const char *)(dgettext("pidgin","Real Name"))),0,0,0);
/* extract their Location and put it in */
    found |= purple_markup_extract_info_field(stripped,stripped_len,user_info,(strings -> location_string),(((yd -> jp) != 0)?4 : 2),"\n",10,0,((const char *)(dgettext("pidgin","Location"))),0,0,0);
/* extract their Age and put it in */
    found |= purple_markup_extract_info_field(stripped,stripped_len,user_info,(strings -> age_string),(((yd -> jp) != 0)?2 : 3),"\n",10,0,((const char *)(dgettext("pidgin","Age"))),0,0,0);
/* extract their MaritalStatus and put it in */
    found |= purple_markup_extract_info_field(stripped,stripped_len,user_info,(strings -> maritalstatus_string),(((yd -> jp) != 0)?2 : 3),"\n",10,(strings -> no_answer_string),((const char *)(dgettext("pidgin","Marital Status"))),0,0,0);
/* extract their Gender and put it in */
    found |= purple_markup_extract_info_field(stripped,stripped_len,user_info,(strings -> gender_string),(((yd -> jp) != 0)?2 : 3),"\n",10,(strings -> no_answer_string),((const char *)(dgettext("pidgin","Gender"))),0,0,0);
/* extract their Occupation and put it in */
    found |= purple_markup_extract_info_field(stripped,stripped_len,user_info,(strings -> occupation_string),2,"\n",10,0,((const char *)(dgettext("pidgin","Occupation"))),0,0,0);
/* Hobbies, Latest News, and Favorite Quote are a bit different, since
	 * the values can contain embedded newlines... but any or all of them
	 * can also not appear.  The way we delimit them is to successively
	 * look for the next one that _could_ appear, and if all else fails,
	 * we end the section by looking for the 'Links' heading, which is the
	 * next thing to follow this bunch.  (For Yahoo Japan, we check for
	 * the "Description" ("Self PR") heading instead of "Links".)
	 */
    if (!(purple_markup_extract_info_field(stripped,stripped_len,user_info,(strings -> hobbies_string),(((yd -> jp) != 0)?3 : 1),(strings -> latest_news_string),10,"\n",((const char *)(dgettext("pidgin","Hobbies"))),0,0,0) != 0)) {
      if (!(purple_markup_extract_info_field(stripped,stripped_len,user_info,(strings -> hobbies_string),1,(strings -> favorite_quote_string),10,"\n",((const char *)(dgettext("pidgin","Hobbies"))),0,0,0) != 0)) {
        found |= purple_markup_extract_info_field(stripped,stripped_len,user_info,(strings -> hobbies_string),1,(strings -> links_string),10,"\n",((const char *)(dgettext("pidgin","Hobbies"))),0,0,0);
      }
      else 
        found = (!0);
    }
    else 
      found = (!0);
    if (!(purple_markup_extract_info_field(stripped,stripped_len,user_info,(strings -> latest_news_string),1,(strings -> favorite_quote_string),10,"\n",((const char *)(dgettext("pidgin","Latest News"))),0,0,0) != 0)) {
      found |= purple_markup_extract_info_field(stripped,stripped_len,user_info,(strings -> latest_news_string),(((yd -> jp) != 0)?2 : 1),(strings -> links_string),10,"\n",((const char *)(dgettext("pidgin","Latest News"))),0,0,0);
    }
    else 
      found = (!0);
    found |= purple_markup_extract_info_field(stripped,stripped_len,user_info,(strings -> favorite_quote_string),1,(strings -> links_string),10,"\n",((const char *)(dgettext("pidgin","Favorite Quote"))),0,0,0);
/* Home Page will either be "No home page specified",
	 * or "Home Page: " and a link.
	 * For Yahoo! Japan, if there is no home page specified,
	 * neither "No home page specified" nor "Home Page:" is shown.
	 */
    if ((strings -> home_page_string) != 0) {
      p = (!((strings -> no_home_page_specified_string) != 0)?((char *)((void *)0)) : strstr(stripped,(strings -> no_home_page_specified_string)));
      if (!(p != 0)) {
        found |= purple_markup_extract_info_field(stripped,stripped_len,user_info,(strings -> home_page_string),1,"\n",0,0,((const char *)(dgettext("pidgin","Home Page"))),1,0,0);
      }
    }
/* Cool Link {1,2,3} is also different.  If "No cool link specified"
	 * exists, then we have none.  If we have one however, we'll need to
	 * check and see if we have a second one.  If we have a second one,
	 * we have to check to see if we have a third one.
	 */
    p = (!((strings -> no_cool_link_specified_string) != 0)?((char *)((void *)0)) : strstr(stripped,(strings -> no_cool_link_specified_string)));
    if (!(p != 0)) {
      if (purple_markup_extract_info_field(stripped,stripped_len,user_info,(strings -> cool_link_1_string),1,"\n",0,0,((const char *)(dgettext("pidgin","Cool Link 1"))),1,0,0) != 0) {
        found = (!0);
        if (purple_markup_extract_info_field(stripped,stripped_len,user_info,(strings -> cool_link_2_string),1,"\n",0,0,((const char *)(dgettext("pidgin","Cool Link 2"))),1,0,0) != 0) {
          purple_markup_extract_info_field(stripped,stripped_len,user_info,(strings -> cool_link_3_string),1,"\n",0,0,((const char *)(dgettext("pidgin","Cool Link 3"))),1,0,0);
        }
      }
    }
    if (last_updated_utf8_string != ((char *)((void *)0))) {
/* see if Member Since is there, and if so, extract it. */
      found |= purple_markup_extract_info_field(stripped,stripped_len,user_info,"Member Since:",1,last_updated_utf8_string,10,0,((const char *)(dgettext("pidgin","Member Since"))),0,0,yahoo_info_date_reformat);
/* extract the Last Updated date and put it in */
      found |= purple_markup_extract_info_field(stripped,stripped_len,user_info,last_updated_utf8_string,(((yd -> jp) != 0)?2 : 1),((((yd -> jp) != 0)?"\n" : " ")),((((yd -> jp) != 0)?0 : 10)),0,((const char *)(dgettext("pidgin","Last Update"))),0,0,(((yd -> jp) != 0)?((char *(*)(const char *, size_t ))((void *)0)) : yahoo_info_date_reformat));
    }
/* if (profile_state == PROFILE_STATE_DEFAULT) */
  }
  if (!(found != 0)) {
    const gchar *str;
    purple_notify_user_info_add_section_break(user_info);
    purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Error retrieving profile"))),0);
    if (profile_state == PROFILE_STATE_UNKNOWN_LANGUAGE) {
      str = ((const char *)(dgettext("pidgin","This profile is in a language or format that is not supported at this time.")));
    }
    else if (profile_state == PROFILE_STATE_NOT_FOUND) {
      PurpleBuddy *b = purple_find_buddy(purple_connection_get_account((info_data -> gc)),(info_data -> name));
      YahooFriend *f = (YahooFriend *)((void *)0);
      if (b != 0) {
/* Someone on the buddy list can be "not on server list",
				 * in which case the user may or may not actually exist.
				 * Hence this extra step.
				 */
        PurpleAccount *account = purple_buddy_get_account(b);
        f = yahoo_friend_find(purple_account_get_connection(account),purple_buddy_get_name(b));
      }
      str = ((f != 0)?((const char *)(dgettext("pidgin","Could not retrieve the user\'s profile. This most likely is a temporary server-side problem. Please try again later."))) : ((const char *)(dgettext("pidgin","Could not retrieve the user\'s profile. This most likely means that the user does not exist; however, Yahoo! sometimes does fail to find a user\'s profile. If you know that the user exists, please try again later."))));
    }
    else {
      str = ((const char *)(dgettext("pidgin","The user\'s profile is empty.")));
    }
    purple_notify_user_info_add_pair(user_info,0,str);
  }
/* put a link to the actual profile URL */
  purple_notify_user_info_add_section_break(user_info);
  tmp = g_strdup_printf("<a href=\"%s\">%s</a>",profile_url_text,((const char *)(dgettext("pidgin","View web profile"))));
  purple_notify_user_info_add_pair(user_info,0,tmp);
  g_free(tmp);
  g_free(stripped);
/* show it to the user */
  purple_notify_userinfo((info_data -> gc),(info_data -> name),user_info,0,0);
  purple_notify_user_info_destroy(user_info);
  g_free(last_updated_utf8_string);
  g_free(url_buffer);
  g_free(fudged_buffer);
  g_free(profile_url_text);
  g_free((info_data -> name));
  g_free(info_data);
#if PHOTO_SUPPORT
  g_free(photo_url_text);
  g_free(info2_data);
  if (id != -1) 
    purple_imgstore_unref_by_id(id);
#endif /* PHOTO_SUPPORT */
}

void yahoo_get_info(PurpleConnection *gc,const char *name)
{
  YahooData *yd = (gc -> proto_data);
  YahooGetInfoData *data;
  char *url;
  PurpleUtilFetchUrlData *url_data;
  data = ((YahooGetInfoData *)(g_malloc0_n(1,(sizeof(YahooGetInfoData )))));
  data -> gc = gc;
  data -> name = g_strdup(name);
  url = g_strdup_printf("%s%s",(((yd -> jp) != 0)?"http://profiles.yahoo.co.jp/" : "http://profiles.yahoo.com/"),name);
  url_data = purple_util_fetch_url_request(url,(!0),0,0,0,0,yahoo_got_info,data);;
  if (url_data != ((PurpleUtilFetchUrlData *)((void *)0))) 
    yd -> url_datas = g_slist_prepend((yd -> url_datas),url_data);
  g_free(url);
}
