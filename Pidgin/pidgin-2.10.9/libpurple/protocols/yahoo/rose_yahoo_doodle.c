/*
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
/******************************************************************************
 * INCLUDES
 *****************************************************************************/
#include "internal.h"
#include "account.h"
#include "accountopt.h"
#include "blist.h"
#include "cipher.h"
#include "cmds.h"
#include "debug.h"
#include "notify.h"
#include "privacy.h"
#include "prpl.h"
#include "proxy.h"
#include "request.h"
#include "server.h"
#include "util.h"
#include "version.h"
#include "libymsg.h"
#include "yahoo_packet.h"
#include "yahoo_friend.h"
#include "yahoochat.h"
#include "ycht.h"
#include "yahoo_filexfer.h"
#include "yahoo_picture.h"
#include "whiteboard.h"
#include "yahoo_doodle.h"
/******************************************************************************
 * Globals
 *****************************************************************************/
#if 0
#endif
/******************************************************************************
 * Functions
 *****************************************************************************/

PurpleCmdRet yahoo_doodle_purple_cmd_start(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  PurpleAccount *account;
  PurpleConnection *gc;
  const gchar *name;
  if (( *args != 0) && (args[0] != 0)) 
    return PURPLE_CMD_RET_FAILED;
  account = purple_conversation_get_account(conv);
  gc = purple_account_get_connection(account);
  name = purple_conversation_get_name(conv);
  yahoo_doodle_initiate(gc,name);
/* Write a local message to this conversation showing that a request for a
	 * Doodle session has been made
	 */
  purple_conv_im_write(purple_conversation_get_im_data(conv),"",((const char *)(dgettext("pidgin","Sent Doodle request."))),(PURPLE_MESSAGE_NICK | PURPLE_MESSAGE_RECV),time(0));
  return PURPLE_CMD_RET_OK;
}

void yahoo_doodle_initiate(PurpleConnection *gc,const char *name)
{
  PurpleAccount *account;
  char *to = (char *)name;
  do {
    if (gc != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc");
      return ;
    };
  }while (0);
  do {
    if (name != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name");
      return ;
    };
  }while (0);
  account = purple_connection_get_account(gc);
  if (purple_whiteboard_get_session(account,to) == ((PurpleWhiteboard *)((void *)0))) {
/* Insert this 'session' in the list.  At this point, it's only a
		 * requested session.
		 */
    purple_whiteboard_create(account,to,0);
  }
/* NOTE Perhaps some careful handling of remote assumed established
	 * sessions
	 */
  yahoo_doodle_command_send_ready(gc,to,"doodle;106");
  yahoo_doodle_command_send_request(gc,to,"doodle;106");
}

static void yahoo_doodle_command_got_request(PurpleConnection *gc,const char *from,const char *imv_key)
{
  PurpleAccount *account;
  PurpleWhiteboard *wb;
  purple_debug_info("yahoo","doodle: Got Request (%s)\n",from);
  account = purple_connection_get_account(gc);
/* Only handle this if local client requested Doodle session (else local
	 * client would have sent one)
	 */
  wb = purple_whiteboard_get_session(account,from);
/* If a session with the remote user doesn't exist */
  if (wb == ((PurpleWhiteboard *)((void *)0))) {
    doodle_session *ds;
/* Ask user if they wish to accept the request for a doodle session */
/* TODO Ask local user to start Doodle session with remote user */
/* NOTE This if/else statement won't work right--must use dialog
		 * results
		 */
/* char dialog_message[64];
		g_sprintf(dialog_message, "%s is requesting to start a Doodle session with you.", from);
		purple_notify_message(NULL, PURPLE_NOTIFY_MSG_INFO, "Doodle",
		dialog_message, NULL, NULL, NULL);
		*/
    wb = purple_whiteboard_create(account,from,1);
    ds = (wb -> proto_data);
    ds -> imv_key = g_strdup(imv_key);
    yahoo_doodle_command_send_ready(gc,from,imv_key);
  }
/* TODO Might be required to clear the canvas of an existing doodle
	 * session at this point
	 */
}

static void yahoo_doodle_command_got_ready(PurpleConnection *gc,const char *from,const char *imv_key)
{
  PurpleAccount *account;
  PurpleWhiteboard *wb;
  purple_debug_info("yahoo","doodle: Got Ready(%s)\n",from);
  account = purple_connection_get_account(gc);
/* Only handle this if local client requested Doodle session (else local
	 * client would have sent one)
	 */
  wb = purple_whiteboard_get_session(account,from);
  if (wb == ((PurpleWhiteboard *)((void *)0))) 
    return ;
  if ((wb -> state) == 0) {
    doodle_session *ds = (wb -> proto_data);
    purple_whiteboard_start(wb);
    wb -> state = 2;
    yahoo_doodle_command_send_confirm(gc,from,imv_key);
/* Let's steal the imv_key and reuse it */
    g_free((ds -> imv_key));
    ds -> imv_key = g_strdup(imv_key);
  }
  else if ((wb -> state) == 2) {
/* TODO Ask whether to save picture too */
    purple_whiteboard_clear(wb);
  }
  else 
/* NOTE Not sure about this... I am trying to handle if the remote user
	 * already thinks we're in a session with them (when their chat message
	 * contains the doodle imv key)
	 */
if ((wb -> state) == 1) {
/* purple_whiteboard_start(wb); */
    yahoo_doodle_command_send_ready(gc,from,imv_key);
  }
}

static void yahoo_doodle_command_got_draw(PurpleConnection *gc,const char *from,const char *message)
{
  PurpleAccount *account;
  PurpleWhiteboard *wb;
  char **tokens;
  int i;
/* a local list of drawing info */
  GList *d_list = (GList *)((void *)0);
  do {
    if (message != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"message != NULL");
      return ;
    };
  }while (0);
  purple_debug_info("yahoo","doodle: Got Draw (%s)\n",from);
  purple_debug_info("yahoo","doodle: Draw message: %s\n",message);
  account = purple_connection_get_account(gc);
/* Only handle this if local client requested Doodle session (else local
	 * client would have sent one)
	 */
  wb = purple_whiteboard_get_session(account,from);
  if (wb == ((PurpleWhiteboard *)((void *)0))) 
    return ;
/* TODO Functionalize
	 * Convert drawing packet message to an integer list
	 */
/* Check to see if the message begans and ends with quotes */
  if ((message[0] != '\"') || (message[strlen(message) - 1] != '\"')) 
    return ;
/* Ignore the inital quotation mark. */
  message += 1;
  tokens = g_strsplit(message,",",0);
/* Traverse and extract all integers divided by commas */
  for (i = 0; tokens[i] != ((char *)((void *)0)); i++) {
    int last = (strlen(tokens[i]) - 1);
    if (tokens[i][last] == '\"') 
      tokens[i][last] = 0;
    d_list = g_list_prepend(d_list,((gpointer )((glong )(atoi(tokens[i])))));
  }
  d_list = g_list_reverse(d_list);
  g_strfreev(tokens);
  yahoo_doodle_draw_stroke(wb,d_list);
/* goodle_doodle_session_set_canvas_as_icon(ds); */
  g_list_free(d_list);
}

static void yahoo_doodle_command_got_clear(PurpleConnection *gc,const char *from)
{
  PurpleAccount *account;
  PurpleWhiteboard *wb;
  purple_debug_info("yahoo","doodle: Got Clear (%s)\n",from);
  account = purple_connection_get_account(gc);
/* Only handle this if local client requested Doodle session (else local
	 * client would have sent one)
	 */
  wb = purple_whiteboard_get_session(account,from);
  if (wb == ((PurpleWhiteboard *)((void *)0))) 
    return ;
  if ((wb -> state) == 2) {
/* TODO Ask user whether to save the image before clearing it */
    purple_whiteboard_clear(wb);
  }
}

static void yahoo_doodle_command_got_extra(PurpleConnection *gc,const char *from,const char *message,const char *imv_key)
{
  purple_debug_info("yahoo","doodle: Got Extra (%s)\n",from);
/* I do not like these 'extra' features, so I'll only handle them in one
	 * way, which is returning them with the command/packet to turn them off
	 */
  yahoo_doodle_command_send_extra(gc,from,"\"1\"",imv_key);
}

static void yahoo_doodle_command_got_confirm(PurpleConnection *gc,const char *from)
{
  PurpleAccount *account;
  PurpleWhiteboard *wb;
  purple_debug_info("yahoo","doodle: Got Confirm (%s)\n",from);
/* Get the doodle session */
  account = purple_connection_get_account(gc);
/* Only handle this if local client requested Doodle session (else local
	 * client would have sent one)
	 */
  wb = purple_whiteboard_get_session(account,from);
  if (wb == ((PurpleWhiteboard *)((void *)0))) 
    return ;
/* TODO Combine the following IF's? */
/* Check if we requested a doodle session */
/*if(wb->state == DOODLE_STATE_REQUESTING)
	{
		wb->state = DOODLE_STATE_ESTABLISHED;
		purple_whiteboard_start(wb);
		yahoo_doodle_command_send_confirm(gc, from);
	}*/
/* Check if we accepted a request for a doodle session */
  if ((wb -> state) == 1) {
    wb -> state = 2;
    purple_whiteboard_start(wb);
  }
}

void yahoo_doodle_command_got_shutdown(PurpleConnection *gc,const char *from)
{
  PurpleAccount *account;
  PurpleWhiteboard *wb;
  do {
    if (from != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"from != NULL");
      return ;
    };
  }while (0);
  purple_debug_info("yahoo","doodle: Got Shutdown (%s)\n",from);
  account = purple_connection_get_account(gc);
/* Only handle this if local client requested Doodle session (else local
	 * client would have sent one)
	 */
  wb = purple_whiteboard_get_session(account,from);
  if (wb == ((PurpleWhiteboard *)((void *)0))) 
    return ;
/* TODO Ask if user wants to save picture before the session is closed */
  wb -> state = 3;
  purple_whiteboard_destroy(wb);
}

static void yahoo_doodle_command_send_generic(const char *type,PurpleConnection *gc,const char *to,const char *message,int command,const char *imv,const char *sixtyfour)
{
  YahooData *yd;
  struct yahoo_packet *pkt;
  purple_debug_info("yahoo","doodle: Sent %s (%s)\n",type,to);
  yd = (gc -> proto_data);
/* Make and send an acknowledge (ready) Doodle packet */
  pkt = yahoo_packet_new(YAHOO_SERVICE_P2PFILEXFER,YAHOO_STATUS_AVAILABLE,(yd -> session_id));
  yahoo_packet_hash_str(pkt,49,"IMVIRONMENT");
  yahoo_packet_hash_str(pkt,1,purple_account_get_username((gc -> account)));
  yahoo_packet_hash_str(pkt,14,message);
  yahoo_packet_hash_int(pkt,13,command);
  yahoo_packet_hash_str(pkt,5,to);
  yahoo_packet_hash_str(pkt,63,((imv != 0)?imv : "doodle;106"));
  yahoo_packet_hash_str(pkt,64,sixtyfour);
  yahoo_packet_hash_str(pkt,1002,"1");
  yahoo_packet_send_and_free(pkt,yd);
}

void yahoo_doodle_command_send_ready(PurpleConnection *gc,const char *to,const char *imv_key)
{
  yahoo_doodle_command_send_generic("Ready",gc,to,"1",4,imv_key,"1");
}

void yahoo_doodle_command_send_request(PurpleConnection *gc,const char *to,const char *imv_key)
{
  yahoo_doodle_command_send_generic("Request",gc,to,"",0,imv_key,"0");
}

void yahoo_doodle_command_send_draw(PurpleConnection *gc,const char *to,const char *message,const char *imv_key)
{
  yahoo_doodle_command_send_generic("Draw",gc,to,message,2,imv_key,"1");
}

void yahoo_doodle_command_send_clear(PurpleConnection *gc,const char *to,const char *imv_key)
{
  yahoo_doodle_command_send_generic("Clear",gc,to," ",1,imv_key,"1");
}

void yahoo_doodle_command_send_extra(PurpleConnection *gc,const char *to,const char *message,const char *imv_key)
{
  yahoo_doodle_command_send_generic("Extra",gc,to,message,3,imv_key,"1");
}

void yahoo_doodle_command_send_confirm(PurpleConnection *gc,const char *to,const char *imv_key)
{
  yahoo_doodle_command_send_generic("Confirm",gc,to,"1",5,imv_key,"1");
}

void yahoo_doodle_command_send_shutdown(PurpleConnection *gc,const char *to)
{
  yahoo_doodle_command_send_generic("Shutdown",gc,to,"",0,";0","0");
}

void yahoo_doodle_start(PurpleWhiteboard *wb)
{
  doodle_session *ds = (doodle_session *)(g_malloc0_n(1,(sizeof(doodle_session ))));
/* purple_debug_debug("yahoo", "doodle: yahoo_doodle_start()\n"); */
/* Set default brush size and color */
  ds -> brush_size = 2;
  ds -> brush_color = 13369344;
  wb -> proto_data = ds;
}

void yahoo_doodle_end(PurpleWhiteboard *wb)
{
  PurpleConnection *gc = purple_account_get_connection((wb -> account));
  doodle_session *ds = (wb -> proto_data);
/* g_debug_debug("yahoo", "doodle: yahoo_doodle_end()\n"); */
  if ((gc != 0) && ((wb -> state) != 3)) 
    yahoo_doodle_command_send_shutdown(gc,(wb -> who));
  g_free((ds -> imv_key));
  g_free((wb -> proto_data));
}

void yahoo_doodle_get_dimensions(const PurpleWhiteboard *wb,int *width,int *height)
{
/* standard Doodle canvases are of one size:  368x256 */
   *width = 368;
   *height = 256;
}

static char *yahoo_doodle_build_draw_string(doodle_session *ds,GList *draw_list)
{
  GString *message;
  do {
    if (draw_list != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"draw_list != NULL");
      return 0;
    };
  }while (0);
  message = g_string_new("");
  g_string_printf(message,"\"%d,%d",(ds -> brush_color),(ds -> brush_size));
  for (; draw_list != ((GList *)((void *)0)); draw_list = (draw_list -> next)) {
    g_string_append_printf(message,",%d",((gint )((glong )(draw_list -> data))));
  }
  g_string_append_c_inline(message,'\"');
  return g_string_free(message,0);
}

void yahoo_doodle_send_draw_list(PurpleWhiteboard *wb,GList *draw_list)
{
  doodle_session *ds = (wb -> proto_data);
  char *message;
  do {
    if (draw_list != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"draw_list != NULL");
      return ;
    };
  }while (0);
  message = yahoo_doodle_build_draw_string(ds,draw_list);
  yahoo_doodle_command_send_draw(( *(wb -> account)).gc,(wb -> who),message,(ds -> imv_key));
  g_free(message);
}

void yahoo_doodle_clear(PurpleWhiteboard *wb)
{
  doodle_session *ds = (wb -> proto_data);
  yahoo_doodle_command_send_clear(( *(wb -> account)).gc,(wb -> who),(ds -> imv_key));
}
/* Traverse through the list and draw the points and lines */

void yahoo_doodle_draw_stroke(PurpleWhiteboard *wb,GList *draw_list)
{
  int brush_color;
  int brush_size;
  int x;
  int y;
  do {
    if (draw_list != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"draw_list != NULL");
      return ;
    };
  }while (0);
  brush_color = ((gint )((glong )(draw_list -> data)));
  draw_list = (draw_list -> next);
  do {
    if (draw_list != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"draw_list != NULL");
      return ;
    };
  }while (0);
  brush_size = ((gint )((glong )(draw_list -> data)));
  draw_list = (draw_list -> next);
  do {
    if (draw_list != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"draw_list != NULL");
      return ;
    };
  }while (0);
  x = ((gint )((glong )(draw_list -> data)));
  draw_list = (draw_list -> next);
  do {
    if (draw_list != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"draw_list != NULL");
      return ;
    };
  }while (0);
  y = ((gint )((glong )(draw_list -> data)));
  draw_list = (draw_list -> next);
  do {
    if (draw_list != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"draw_list != NULL");
      return ;
    };
  }while (0);
/*
	purple_debug_debug("yahoo", "doodle: Drawing: color=%d, size=%d, (%d,%d)\n", brush_color, brush_size, x, y);
	*/
  while((draw_list != ((GList *)((void *)0))) && ((draw_list -> next) != ((GList *)((void *)0)))){
    int dx = (gint )((glong )(draw_list -> data));
    int dy = (gint )((glong )( *(draw_list -> next)).data);
    purple_whiteboard_draw_line(wb,x,y,(x + dx),(y + dy),brush_color,brush_size);
    x += dx;
    y += dy;
    draw_list = ( *(draw_list -> next)).next;
  }
}

void yahoo_doodle_get_brush(const PurpleWhiteboard *wb,int *size,int *color)
{
  doodle_session *ds = (wb -> proto_data);
   *size = (ds -> brush_size);
   *color = (ds -> brush_color);
}

void yahoo_doodle_set_brush(PurpleWhiteboard *wb,int size,int color)
{
  doodle_session *ds = (wb -> proto_data);
  ds -> brush_size = size;
  ds -> brush_color = color;
/* Notify the core about the changes */
  purple_whiteboard_set_brush(wb,size,color);
}

void yahoo_doodle_process(PurpleConnection *gc,const char *me,const char *from,const char *command,const char *message,const char *imv_key)
{
  if (!(command != 0)) 
    return ;
/* Now check to see what sort of Doodle message it is */
  switch(atoi(command)){
    case 0:
{
      yahoo_doodle_command_got_request(gc,from,imv_key);
      break; 
    }
    case 4:
{
      yahoo_doodle_command_got_ready(gc,from,imv_key);
      break; 
    }
    case 1:
{
      yahoo_doodle_command_got_clear(gc,from);
      break; 
    }
    case 2:
{
      yahoo_doodle_command_got_draw(gc,from,message);
      break; 
    }
    case 3:
{
      yahoo_doodle_command_got_extra(gc,from,message,imv_key);
      break; 
    }
    case 5:
{
      yahoo_doodle_command_got_confirm(gc,from);
      break; 
    }
  }
}
