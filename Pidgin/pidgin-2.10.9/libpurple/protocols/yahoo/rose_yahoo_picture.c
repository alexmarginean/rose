/*
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "account.h"
#include "accountopt.h"
#include "blist.h"
#include "debug.h"
#include "privacy.h"
#include "prpl.h"
#include "proxy.h"
#include "util.h"
#include "libymsg.h"
#include "yahoo_packet.h"
#include "yahoo_friend.h"
#include "yahoo_picture.h"

struct yahoo_fetch_picture_data 
{
  PurpleConnection *gc;
  char *who;
  int checksum;
}
;

static void yahoo_fetch_picture_cb(PurpleUtilFetchUrlData *url_data,gpointer user_data,const gchar *pic_data,size_t len,const gchar *error_message)
{
  struct yahoo_fetch_picture_data *d;
  YahooData *yd;
  d = user_data;
  yd = ( *(d -> gc)).proto_data;
  yd -> url_datas = g_slist_remove((yd -> url_datas),url_data);
  if (error_message != ((const gchar *)((void *)0))) {
    purple_debug_error("yahoo","Fetching buddy icon failed: %s\n",error_message);
  }
  else if (len == 0) {
    purple_debug_error("yahoo","Fetched an icon with length 0.  Strange.\n");
  }
  else {
    char *checksum = g_strdup_printf("%i",(d -> checksum));
    purple_buddy_icons_set_for_user(purple_connection_get_account((d -> gc)),(d -> who),g_memdup(pic_data,len),len,checksum);
    g_free(checksum);
  }
  g_free((d -> who));
  g_free(d);
}

void yahoo_process_picture(PurpleConnection *gc,struct yahoo_packet *pkt)
{
  YahooData *yd;
  GSList *l = (pkt -> hash);
  char *who = (char *)((void *)0);
  char *us = (char *)((void *)0);
  gboolean got_icon_info = 0;
  gboolean send_icon_info = 0;
  char *url = (char *)((void *)0);
  int checksum = 0;
  while(l != 0){
    struct yahoo_pair *pair = (l -> data);
{
      switch(pair -> key){
        case 1:
{
        }
        case 4:
{
          if (g_utf8_validate((pair -> value),(-1),0) != 0) {
            who = (pair -> value);
          }
          else {
            purple_debug_warning("yahoo","yahoo_process_picture got non-UTF-8 string for key %d\n",(pair -> key));
          }
          break; 
        }
        case 5:
{
          if (g_utf8_validate((pair -> value),(-1),0) != 0) {
            us = (pair -> value);
          }
          else {
            purple_debug_warning("yahoo","yahoo_process_picture got non-UTF-8 string for key %d\n",(pair -> key));
          }
          break; 
        }
        case 13:
{
{
            int tmp;
            tmp = (strtol((pair -> value),0,10));
            if (tmp == 1) {
              send_icon_info = (!0);
            }
            else if (tmp == 2) {
              got_icon_info = (!0);
            }
            break; 
          }
        }
        case 20:
{
          if (g_utf8_validate((pair -> value),(-1),0) != 0) {
            url = (pair -> value);
          }
          else {
            purple_debug_warning("yahoo","yahoo_process_picture got non-UTF-8 string for key %d\n",(pair -> key));
          }
          break; 
        }
        case 192:
{
          checksum = (strtol((pair -> value),0,10));
          break; 
        }
      }
    }
    l = (l -> next);
  }
  if (!(who != 0)) 
    return ;
  if (!(purple_privacy_check(purple_connection_get_account(gc),who) != 0)) {
    purple_debug_info("yahoo","Picture packet from %s dropped.\n",who);
    return ;
  }
/* Yahoo IM 6 spits out 0.png as the URL if the buddy icon is not set */
  if ((((who != 0) && (got_icon_info != 0)) && (url != 0)) && !(g_ascii_strncasecmp(url,"http://",7) != 0)) {
/* TODO: make this work p2p, try p2p before the url */
    PurpleUtilFetchUrlData *url_data;
    struct yahoo_fetch_picture_data *data;
/* use whole URL if using HTTP Proxy */
    gboolean use_whole_url = yahoo_account_use_http_proxy(gc);
    data = ((struct yahoo_fetch_picture_data *)(g_malloc0_n(1,(sizeof(struct yahoo_fetch_picture_data )))));
    data -> gc = gc;
    data -> who = g_strdup(who);
    data -> checksum = checksum;
/* TODO: Does this need to be MSIE 5.0? */
    url_data = purple_util_fetch_url_request(url,use_whole_url,"Mozilla/4.0 (compatible; MSIE 5.5)",0,0,0,yahoo_fetch_picture_cb,data);;
    if (url_data != ((PurpleUtilFetchUrlData *)((void *)0))) {
      yd = (gc -> proto_data);
      yd -> url_datas = g_slist_prepend((yd -> url_datas),url_data);
    }
  }
  else if ((who != 0) && (send_icon_info != 0)) {
    yahoo_send_picture_info(gc,who);
  }
}

void yahoo_process_picture_checksum(PurpleConnection *gc,struct yahoo_packet *pkt)
{
  GSList *l = (pkt -> hash);
  char *who = (char *)((void *)0);
  int checksum = 0;
  while(l != 0){
    struct yahoo_pair *pair = (l -> data);
    switch(pair -> key){
      case 4:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          who = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_picture_checksum got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
      case 5:
{
/* us */
        break; 
      }
      case 192:
{
        checksum = (strtol((pair -> value),0,10));
        break; 
      }
    }
    l = (l -> next);
  }
  if (who != 0) {
    PurpleBuddy *b = purple_find_buddy((gc -> account),who);
    const char *locksum = (const char *)((void *)0);
/* FIXME: Cleanup this strtol() stuff if possible. */
    if (b != 0) {
      locksum = purple_buddy_icons_get_checksum_for_user(b);
      if (!(locksum != 0) || (checksum != strtol(locksum,0,10))) 
        yahoo_send_picture_request(gc,who);
    }
  }
}

void yahoo_process_picture_upload(PurpleConnection *gc,struct yahoo_packet *pkt)
{
  PurpleAccount *account = purple_connection_get_account(gc);
  YahooData *yd = (gc -> proto_data);
  GSList *l = (pkt -> hash);
  char *url = (char *)((void *)0);
  while(l != 0){
    struct yahoo_pair *pair = (l -> data);
    switch(pair -> key){
      case 5:
{
/* us */
        break; 
      }
      case 27:
{
/* filename on our computer. */
        break; 
      }
/* url at yahoo */
      case 20:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          url = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_picture_upload got non-UTF-8 string for key %d\n",(pair -> key));
        }
      }
/* timestamp */
      case 38:
{
        break; 
      }
    }
    l = (l -> next);
  }
  if (url != 0) {
    g_free((yd -> picture_url));
    yd -> picture_url = g_strdup(url);
    purple_account_set_string(account,"picture_url",url);
    purple_account_set_int(account,"picture_checksum",(yd -> picture_checksum));
    yahoo_send_picture_checksum(gc);
    yahoo_send_picture_update(gc,2);
  }
}

void yahoo_process_avatar_update(PurpleConnection *gc,struct yahoo_packet *pkt)
{
  GSList *l = (pkt -> hash);
  char *who = (char *)((void *)0);
  int avatar = 0;
  while(l != 0){
    struct yahoo_pair *pair = (l -> data);
    switch(pair -> key){
      case 4:
{
        if (g_utf8_validate((pair -> value),(-1),0) != 0) {
          who = (pair -> value);
        }
        else {
          purple_debug_warning("yahoo","yahoo_process_avatar_upload got non-UTF-8 string for key %d\n",(pair -> key));
        }
        break; 
      }
      case 5:
{
/* us */
        break; 
      }
/* Older versions. Still needed? */
      case 206:
{
      }
/* Newer versions */
      case 213:
{
/*
			 * 0 - No icon or avatar
			 * 1 - Using an avatar
			 * 2 - Using an icon
			 */
        avatar = (strtol((pair -> value),0,10));
        break; 
      }
    }
    l = (l -> next);
  }
  if (who != 0) {
    if (avatar == 2) 
      yahoo_send_picture_request(gc,who);
    else if ((avatar == 0) || (avatar == 1)) {
      YahooFriend *f;
      purple_buddy_icons_set_for_user((gc -> account),who,0,0,0);
      if ((f = yahoo_friend_find(gc,who)) != 0) 
        yahoo_friend_set_buddy_icon_need_request(f,(!0));
      purple_debug_misc("yahoo","Setting user %s\'s icon to NULL.\n",who);
    }
  }
}

void yahoo_send_picture_info(PurpleConnection *gc,const char *who)
{
  YahooData *yd = (gc -> proto_data);
  struct yahoo_packet *pkt;
  if (!((yd -> picture_url) != 0)) {
    purple_debug_warning("yahoo","Attempted to send picture info without a picture\n");
    return ;
  }
  pkt = yahoo_packet_new(YAHOO_SERVICE_PICTURE,YAHOO_STATUS_AVAILABLE,(yd -> session_id));
  yahoo_packet_hash(pkt,"ssssi",1,purple_connection_get_display_name(gc),5,who,13,"2",20,(yd -> picture_url),192,(yd -> picture_checksum));
  yahoo_packet_send_and_free(pkt,yd);
}

void yahoo_send_picture_request(PurpleConnection *gc,const char *who)
{
  YahooData *yd = (gc -> proto_data);
  struct yahoo_packet *pkt;
  pkt = yahoo_packet_new(YAHOO_SERVICE_PICTURE,YAHOO_STATUS_AVAILABLE,(yd -> session_id));
/* me */
  yahoo_packet_hash_str(pkt,1,purple_connection_get_display_name(gc));
/* the other guy */
  yahoo_packet_hash_str(pkt,5,who);
/* 1 = request, 2 = reply */
  yahoo_packet_hash_str(pkt,13,"1");
  yahoo_packet_send_and_free(pkt,yd);
}

void yahoo_send_picture_checksum(PurpleConnection *gc)
{
  YahooData *yd = (gc -> proto_data);
  struct yahoo_packet *pkt;
  pkt = yahoo_packet_new(YAHOO_SERVICE_PICTURE_CHECKSUM,YAHOO_STATUS_AVAILABLE,(yd -> session_id));
  yahoo_packet_hash(pkt,"ssi",1,purple_connection_get_display_name(gc),212,"1",192,(yd -> picture_checksum));
  yahoo_packet_send_and_free(pkt,yd);
}

void yahoo_send_picture_update_to_user(PurpleConnection *gc,const char *who,int type)
{
  YahooData *yd = (gc -> proto_data);
  struct yahoo_packet *pkt;
  pkt = yahoo_packet_new(YAHOO_SERVICE_AVATAR_UPDATE,YAHOO_STATUS_AVAILABLE,(yd -> session_id));
  yahoo_packet_hash(pkt,"si",3,who,213,type);
  yahoo_packet_send_and_free(pkt,yd);
}

struct yspufe 
{
  PurpleConnection *gc;
  int type;
}
;

static void yahoo_send_picture_update_foreach(gpointer key,gpointer value,gpointer data)
{
  const char *who = key;
  YahooFriend *f = value;
  struct yspufe *d = data;
  if ((f -> status) != YAHOO_STATUS_OFFLINE) 
    yahoo_send_picture_update_to_user((d -> gc),who,(d -> type));
}

void yahoo_send_picture_update(PurpleConnection *gc,int type)
{
  YahooData *yd = (gc -> proto_data);
  struct yspufe data;
  data.gc = gc;
  data.type = type;
  g_hash_table_foreach((yd -> friends),yahoo_send_picture_update_foreach,(&data));
}

void yahoo_buddy_icon_upload_data_free(struct yahoo_buddy_icon_upload_data *d)
{
  purple_debug_misc("yahoo","In yahoo_buddy_icon_upload_data_free()\n");
  if ((d -> str) != 0) 
    g_string_free((d -> str),(!0));
  g_free((d -> filename));
  if ((d -> watcher) != 0U) 
    purple_input_remove((d -> watcher));
  if ((d -> fd) != -1) 
    close((d -> fd));
  g_free(d);
}
/* we couldn't care less about the server's response, but yahoo gets grumpy if we close before it sends it */

static void yahoo_buddy_icon_upload_reading(gpointer data,gint source,PurpleInputCondition condition)
{
  struct yahoo_buddy_icon_upload_data *d = data;
  PurpleConnection *gc = (d -> gc);
  char buf[1024UL];
  int ret;
  if (!(g_list_find(purple_connections_get_all(),gc) != ((GList *)((void *)0)))) {
    yahoo_buddy_icon_upload_data_free(d);
    return ;
  }
  ret = (read((d -> fd),buf,(sizeof(buf))));
  if ((ret < 0) && ( *__errno_location() == 11)) 
    return ;
  else if (ret <= 0) {
/* There are other problems if d->str->len overflows, so shut up the
		 * warning on 64-bit. */
    purple_debug_info("yahoo","Buddy icon upload response (%lu) bytes (> ~400 indicates failure):\n%.*s\n",( *(d -> str)).len,((guint )( *(d -> str)).len),( *(d -> str)).str);
    yahoo_buddy_icon_upload_data_free(d);
    return ;
  }
  g_string_append_len((d -> str),buf,ret);
}

static void yahoo_buddy_icon_upload_pending(gpointer data,gint source,PurpleInputCondition condition)
{
  struct yahoo_buddy_icon_upload_data *d = data;
  PurpleConnection *gc = (d -> gc);
  gssize wrote;
  if (!(g_list_find(purple_connections_get_all(),gc) != ((GList *)((void *)0)))) {
    yahoo_buddy_icon_upload_data_free(d);
    return ;
  }
  wrote = write((d -> fd),(( *(d -> str)).str + (d -> pos)),(( *(d -> str)).len - (d -> pos)));
  if ((wrote < 0) && ( *__errno_location() == 11)) 
    return ;
  if (wrote <= 0) {
    purple_debug_info("yahoo","Error uploading buddy icon.\n");
    yahoo_buddy_icon_upload_data_free(d);
    return ;
  }
  d -> pos += wrote;
  if ((d -> pos) >= ( *(d -> str)).len) {
    purple_debug_misc("yahoo","Finished uploading buddy icon.\n");
    purple_input_remove((d -> watcher));
/* Clean out the sent buffer and reuse it to read the result */
    g_string_free((d -> str),(!0));
    d -> str = g_string_new("");
    d -> watcher = purple_input_add((d -> fd),PURPLE_INPUT_READ,yahoo_buddy_icon_upload_reading,d);
  }
}

static void yahoo_buddy_icon_upload_connected(gpointer data,gint source,const gchar *error_message)
{
  struct yahoo_buddy_icon_upload_data *d = data;
  struct yahoo_packet *pkt;
  gchar *tmp;
  gchar *header;
  guchar *pkt_buf;
  const char *host;
  int port;
  gsize pkt_buf_len;
  PurpleConnection *gc = (d -> gc);
  PurpleAccount *account;
  YahooData *yd;
/* use whole URL if using HTTP Proxy */
  gboolean use_whole_url = yahoo_account_use_http_proxy(gc);
  account = purple_connection_get_account(gc);
  yd = (gc -> proto_data);
/* Buddy icon connect is now complete; clear the PurpleProxyConnectData */
  yd -> buddy_icon_connect_data = ((PurpleProxyConnectData *)((void *)0));
  if (source < 0) {
    purple_debug_error("yahoo","Buddy icon upload failed: %s\n",error_message);
    yahoo_buddy_icon_upload_data_free(d);
    return ;
  }
  pkt = yahoo_packet_new(YAHOO_SERVICE_PICTURE_UPLOAD,YAHOO_STATUS_AVAILABLE,(yd -> session_id));
  tmp = g_strdup_printf("%lu",( *(d -> str)).len);
/* 1 = me, 38 = expire time(?), 0 = me, 28 = size, 27 = filename, 14 = NULL, 29 = data */
  yahoo_packet_hash_str(pkt,1,purple_connection_get_display_name(gc));
/* time til expire */
  yahoo_packet_hash_str(pkt,38,"604800");
  purple_account_set_int(account,"picture_expire",(time(0) + 604800));
  yahoo_packet_hash_str(pkt,0,purple_connection_get_display_name(gc));
  yahoo_packet_hash_str(pkt,28,tmp);
  g_free(tmp);
  yahoo_packet_hash_str(pkt,27,(d -> filename));
  yahoo_packet_hash_str(pkt,14,"");
/* 4 padding for the 29 key name */
  pkt_buf_len = yahoo_packet_build(pkt,4,0,(yd -> jp),&pkt_buf);
  yahoo_packet_free(pkt);
/* header + packet + "29" + 0xc0 + 0x80) + pictureblob */
  host = purple_account_get_string(account,"xfer_host",((((yd -> jp) != 0)?"filetransfer.msg.yahoo.co.jp" : "filetransfer.msg.yahoo.com")));
  port = purple_account_get_int(account,"xfer_port",80);
  tmp = g_strdup_printf("%s:%d",host,port);
  header = g_strdup_printf("POST %s%s/notifyft HTTP/1.1\r\nUser-Agent: Mozilla/5.0\r\nCookie: T=%s; Y=%s\r\nHost: %s\r\nContent-Length: %lu\r\nCache-Control: no-cache\r\n\r\n",((use_whole_url != 0)?"http://" : ""),((use_whole_url != 0)?tmp : ""),(yd -> cookie_t),(yd -> cookie_y),tmp,((pkt_buf_len + 4) + ( *(d -> str)).len));
  g_free(tmp);
/* There's no magic here, we just need to prepend in reverse order */
  g_string_prepend((d -> str),"29\300\200");
  g_string_prepend_len((d -> str),((char *)pkt_buf),pkt_buf_len);
  g_free(pkt_buf);
  g_string_prepend((d -> str),header);
  g_free(header);
/* There are other problems if we're uploading over 4GB of data */
  purple_debug_info("yahoo","Buddy icon upload data:\n%.*s\n",((guint )( *(d -> str)).len),( *(d -> str)).str);
  d -> fd = source;
  d -> watcher = purple_input_add((d -> fd),PURPLE_INPUT_WRITE,yahoo_buddy_icon_upload_pending,d);
  yahoo_buddy_icon_upload_pending(d,(d -> fd),PURPLE_INPUT_WRITE);
}

void yahoo_buddy_icon_upload(PurpleConnection *gc,struct yahoo_buddy_icon_upload_data *d)
{
  PurpleAccount *account = purple_connection_get_account(gc);
  YahooData *yd = (gc -> proto_data);
  if ((yd -> buddy_icon_connect_data) != ((PurpleProxyConnectData *)((void *)0))) {
/* Cancel any in-progress buddy icon upload */
    purple_proxy_connect_cancel((yd -> buddy_icon_connect_data));
    yd -> buddy_icon_connect_data = ((PurpleProxyConnectData *)((void *)0));
  }
  yd -> buddy_icon_connect_data = purple_proxy_connect(0,account,purple_account_get_string(account,"xfer_host",((((yd -> jp) != 0)?"filetransfer.msg.yahoo.co.jp" : "filetransfer.msg.yahoo.com"))),purple_account_get_int(account,"xfer_port",80),yahoo_buddy_icon_upload_connected,d);
  if ((yd -> buddy_icon_connect_data) == ((PurpleProxyConnectData *)((void *)0))) {
    purple_debug_error("yahoo","Uploading our buddy icon failed to connect.\n");
    yahoo_buddy_icon_upload_data_free(d);
  }
}

static int yahoo_buddy_icon_calculate_checksum(const guchar *data,gsize len)
{
/* This code is borrowed from Kopete, which seems to be managing to calculate
	   checksums in such a manner that Yahoo!'s servers are happy */
  const guchar *p = data;
  int checksum = 0;
  int g;
  int i = len;
  while(i-- != 0){
    checksum = ((checksum << 4) + ( *(p++)));
    if ((g = (checksum & 0xf0000000)) != 0) 
      checksum ^= (g >> 23);
    checksum &= ~g;
  }
  purple_debug_misc("yahoo","Calculated buddy icon checksum: %d\n",checksum);
  return checksum;
}

void yahoo_set_buddy_icon(PurpleConnection *gc,PurpleStoredImage *img)
{
  YahooData *yd = (gc -> proto_data);
  PurpleAccount *account = (gc -> account);
  if (img == ((PurpleStoredImage *)((void *)0))) {
    g_free((yd -> picture_url));
    yd -> picture_url = ((char *)((void *)0));
/* TODO: don't we have to clear it on the server too?! */
    purple_account_set_string(account,"picture_url",0);
    purple_account_set_int(account,"picture_checksum",0);
    purple_account_set_int(account,"picture_expire",0);
    if ((yd -> logged_in) != 0) 
/* Tell everyone we ain't got one no more */
      yahoo_send_picture_update(gc,0);
  }
  else {
    gconstpointer data = purple_imgstore_get_data(img);
    size_t len = purple_imgstore_get_size(img);
    GString *s = g_string_new_len(data,len);
    struct yahoo_buddy_icon_upload_data *d;
    int oldcksum = purple_account_get_int(account,"picture_checksum",0);
    int expire = purple_account_get_int(account,"picture_expire",0);
    const char *oldurl = purple_account_get_string(account,"picture_url",0);
    yd -> picture_checksum = yahoo_buddy_icon_calculate_checksum(data,len);
    if ((((yd -> picture_checksum) == oldcksum) && (expire > (time(0) + (60 * 60 * 24)))) && (oldurl != 0)) {
      purple_debug_misc("yahoo","buddy icon is up to date. Not reuploading.\n");
      g_string_free(s,(!0));
      g_free((yd -> picture_url));
      yd -> picture_url = g_strdup(oldurl);
      return ;
    }
/* We use this solely for sending a filename to the server */
    d = ((struct yahoo_buddy_icon_upload_data *)(g_malloc0_n(1,(sizeof(struct yahoo_buddy_icon_upload_data )))));
    d -> gc = gc;
    d -> str = s;
    d -> fd = -1;
    d -> filename = g_strdup(purple_imgstore_get_filename(img));
    if (!((yd -> logged_in) != 0)) {
      yd -> picture_upload_todo = d;
      return ;
    }
    yahoo_buddy_icon_upload(gc,d);
  }
}
