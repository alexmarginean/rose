/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
/* libicq is the ICQ protocol plugin. It is linked against liboscar,
 * which contains all the shared implementation code with libaim
 */
#include "oscarcommon.h"

static GHashTable *icq_get_account_text_table(PurpleAccount *account)
{
  GHashTable *table;
  table = g_hash_table_new(g_str_hash,g_str_equal);
  g_hash_table_insert(table,"login_label",((gpointer )((const char *)(dgettext("pidgin","ICQ UIN...")))));
  return table;
}
static PurplePluginProtocolInfo prpl_info = {((OPT_PROTO_MAIL_CHECK | OPT_PROTO_IM_IMAGE | OPT_PROTO_INVITE_MESSAGE)), ((GList *)((void *)0)), ((GList *)((void *)0)), 
/* user_splits */
/* protocol_options */
/* icon_spec */
{("gif,jpeg,bmp,ico"), (0), (0), (64), (64), (7168), ((PURPLE_ICON_SCALE_SEND | PURPLE_ICON_SCALE_DISPLAY))}, (oscar_list_icon_icq), (oscar_list_emblem), (oscar_status_text), (oscar_tooltip_text), (oscar_status_types), (oscar_blist_node_menu), (oscar_chat_info), (oscar_chat_info_defaults), (oscar_login), (oscar_close), (oscar_send_im), (oscar_set_info), (oscar_send_typing), (oscar_get_info), (oscar_set_status), (oscar_set_idle), (oscar_change_passwd), ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *))((void *)0)), ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)), (oscar_remove_buddy), ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), (oscar_add_deny), ((void (*)(PurpleConnection *, const char *))((void *)0)), (oscar_rem_deny), ((void (*)(PurpleConnection *))((void *)0)), (oscar_join_chat), ((void (*)(PurpleConnection *, GHashTable *))((void *)0)), (oscar_get_chat_name), (oscar_chat_invite), (oscar_chat_leave), ((void (*)(PurpleConnection *, int , const char *, const char *))((void *)0)), (oscar_send_chat), (oscar_keepalive), ((void (*)(PurpleAccount *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), (oscar_alias_buddy), (oscar_move_buddy), (oscar_rename_group), ((void (*)(PurpleBuddy *))((void *)0)), (oscar_convo_closed), (oscar_normalize), (oscar_set_icon), (oscar_remove_group), ((char *(*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((PurpleChat *(*)(PurpleAccount *, const char *))((void *)0)), ((PurpleRoomlist *(*)(PurpleConnection *))((void *)0)), ((void (*)(PurpleRoomlist *))((void *)0)), ((void (*)(PurpleRoomlist *, PurpleRoomlistRoom *))((void *)0)), (oscar_can_receive_file), (oscar_send_file), (oscar_new_xfer), (oscar_offline_message), ((PurpleWhiteboardPrplOps *)((void *)0)), ((int (*)(PurpleConnection *, const char *, int ))((void *)0)), ((char *(*)(PurpleRoomlistRoom *))((void *)0)), ((void (*)(PurpleAccount *, PurpleAccountUnregistrationCb , void *))((void *)0)), ((gboolean (*)(PurpleConnection *, const char *, guint ))((void *)0)), ((GList *(*)(PurpleAccount *))((void *)0)), ((sizeof(PurplePluginProtocolInfo ))), (icq_get_account_text_table), ((gboolean (*)(PurpleAccount *, const char *, PurpleMediaSessionType ))((void *)0)), ((PurpleMediaCaps (*)(PurpleAccount *, const char *))((void *)0)), (oscar_get_purple_moods), ((void (*)(PurpleConnection *, const char *, PurpleSetPublicAliasSuccessCallback , PurpleSetPublicAliasFailureCallback ))((void *)0)), ((void (*)(PurpleConnection *, PurpleGetPublicAliasSuccessCallback , PurpleGetPublicAliasFailureCallback ))((void *)0)), (oscar_add_buddy), ((void (*)(PurpleConnection *, GList *, GList *, const char *))((void *)0))
/* list_icon */
/* list_emblems */
/* status_text */
/* tooltip_text */
/* status_types */
/* blist_node_menu */
/* chat_info */
/* chat_info_defaults */
/* login */
/* close */
/* send_im */
/* set_info */
/* send_typing */
/* get_info */
/* set_status */
/* set_idle */
/* change_passwd */
/* add_buddy */
/* add_buddies */
/* remove_buddy */
/* remove_buddies */
/* add_permit */
/* add_deny */
/* rem_permit */
/* rem_deny */
/* set_permit_deny */
/* join_chat */
/* reject_chat */
/* get_chat_name */
/* chat_invite */
/* chat_leave */
/* chat_whisper */
/* chat_send */
/* keepalive */
/* register_user */
/* get_cb_info */
/* get_cb_away */
/* alias_buddy */
/* group_buddy */
/* rename_group */
/* buddy_free */
/* convo_closed */
/* normalize */
/* set_buddy_icon */
/* remove_group */
/* get_cb_real_name */
/* set_chat_topic */
/* find_blist_chat */
/* roomlist_get_list */
/* roomlist_cancel */
/* roomlist_expand_category */
/* can_receive_file */
/* send_file */
/* new_xfer */
/* offline_message */
/* whiteboard_prpl_ops */
/* send_raw */
/* roomlist_room_serialize */
/* unregister_user */
/* send_attention */
/* get_attention_types */
/* struct_size */
/* get_account_text_table */
/* initiate_media */
/* can_do_media */
/* get_moods */
/* set_public_alias */
/* get_public_alias */
/* add_buddy_with_invite */
/* add_buddies_with_invite */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_PROTOCOL), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("prpl-icq"), ("ICQ"), ("2.10.9"), ("ICQ Protocol Plugin"), ("ICQ Protocol Plugin"), ((char *)((void *)0)), ("http://pidgin.im/"), ((gboolean (*)(PurplePlugin *))((void *)0)), ((gboolean (*)(PurplePlugin *))((void *)0)), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((&prpl_info)), ((PurplePluginUiInfo *)((void *)0)), (oscar_actions), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
  PurpleAccountOption *option;
  oscar_init(plugin,(!0));
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","Encoding"))),"encoding","ISO-8859-1");
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
