/*
 * Purple's oscar protocol plugin
 * This file is the legal property of its developers.
 * Please see the AUTHORS file distributed alongside this file.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
*/
/*
 * Family 0x0002 - Locate.
 *
 * The functions here are responsible for requesting and parsing information-
 * gathering SNACs.  Or something like that.  This family contains the SNACs
 * for getting and setting info, away messages, directory profile thingy, etc.
 */
#include "oscar.h"
#ifdef _WIN32
#include "win32dep.h"
#endif
/* Define to log unknown TLVs */
/* #define LOG_UNKNOWN_TLV */
/*
 * Capability blocks.
 *
 * These are CLSIDs. They should actually be of the form:
 *
 * {0x0946134b, 0x4c7f, 0x11d1,
 *  {0x82, 0x22, 0x44, 0x45, 0x53, 0x54, 0x00, 0x00}}},
 *
 * But, eh.
 */
static const struct __unnamed_class___F0_L47_C14_unknown_scope_and_name_variable_declaration__variable_type_guint64Ul__typedef_declaration_variable_name_unknown_scope_and_name__scope__flag__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_guint8Uc__typedef_declaration_index_16_Ae__variable_name_unknown_scope_and_name__scope__data {
guint64 flag;
guint8 data[16UL];}aim_caps[] = {
/*
	 * These are in ascending numerical order.
	 */
/* Client understands short caps, a UUID of the form
	 * 0946XXYY-4C7F-11D1-8222-444553540000 where XXYY is the short cap. */
{(0x0000000000080000LL), {(9), (0x46), (0), (0), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, {(0x0000000000200000LL), {(9), (0x46), (0), (1), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, 
/* OSCAR_CAPABILITY_XHTML_IM */
{(0x0000000080000000LL), {(9), (0x46), (0), (2), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, {(0x0000000000800000LL), {(9), (0x46), (1), (0), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, 
/* "Live Video" (SIP/RTC Video) support in Windows AIM 5.5.3501 and newer */
{(0x0000000002000000LL), {(9), (0x46), (1), (1), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, 
/* "Camera" support in Windows AIM 5.5.3501 and newer */
{(0x0000000004000000LL), {(9), (0x46), (1), (2), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, 
/* "Microphone" support in Windows AIM 5.5.3501 and newer */
/* OSCAR_CAPABILITY_MICROPHONE */
{(0x0000000080000000LL), {(9), (0x46), (1), (3), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, 
/* Supports RTC Audio */
/* OSCAR_CAPABILITY_RTCAUDIO */
{(0x0000000080000000LL), {(9), (0x46), (1), (4), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, 
/* In iChatAV (version numbers...?) */
{(0x0000000001000000LL), {(9), (0x46), (1), (5), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (69), (0x53), (0x54), (0)}}, 
/* Supports "new status message features" (Who advertises this one?) */
/* OSCAR_CAPABILITY_HOST_STATUS_TEXT_AWARE */
{(0x0000000080000000LL), {(9), (0x46), (1), (10), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, 
/* Supports "see as I type" (Who advertises this one?) */
/* OSCAR_CAPABILITY_SEE_AS_I_TYPE */
{(0x0000000080000000LL), {(9), (0x46), (1), (11), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, 
/* Client only asserts caps for services in which it is participating */
/* OSCAR_CAPABILITY_SMARTCAPS */
{(0x0000000080000000LL), {(9), (0x46), (1), (0xff), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, {(0x0000000000100000LL), {(9), (0x46), (19), (0x23), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, {(0x0000000000000002LL), {(9), (0x46), (19), (65), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, {(0x0000000000000020LL), {(9), (0x46), (19), (0x43), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, {(0x0000000000000400LL), {(9), (0x46), (19), (0x44), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, {(0x0000000000000004LL), {(9), (0x46), (19), (69), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, {(0x0000000000000001LL), {(9), (0x46), (19), (0x46), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, {(0x0000000000000080LL), {(9), (0x46), (19), (0x47), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, {(0x0000000000000010LL), {(9), (0x46), (19), (0x48), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, {(0x0000000000004000LL), {(9), (0x46), (19), (0x49), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, 
/*
	 * Indeed, there are two of these.  The former appears to be correct,
	 * but in some versions of winaim, the second one is set.  Either they
	 * forgot to fix endianness, or they made a typo. It really doesn't
	 * matter which.
	 */
{(0x0000000000000040LL), {(9), (0x46), (19), (0x4a), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, {(0x0000000000000200LL), {(9), (0x46), (19), (0x4a), (0x4c), (0x7f), (0x11), (0xd1), (0x22), (0x82), (0x44), (69), (0x53), (0x54), (0), (0)}}, 
/* New format of caps (xtraz icons) */
{(0x0000000020000000LL), {(9), (0x46), (0), (0), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, 
/* Support xtraz statuses */
{(0x0000000040000000LL), {(0x1a), (9), (60), (108), (0xd7), (0xFD), (0x4e), (0xc5), (0x9d), (0x51), (0xa6), (0x47), (0x4e), (0x34), (0xf5), (0xa0)}}, {(0x0000000000000100LL), {(9), (0x46), (19), (0x4b), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, 
/*
	 * Setting this lets AIM users receive messages from ICQ users, and ICQ
	 * users receive messages from AIM users.  It also lets ICQ users show
	 * up in buddy lists for AIM users, and AIM users show up in buddy lists
	 * for ICQ users.  And ICQ privacy/invisibility acts like AIM privacy,
	 * in that if you add a user to your deny list, you will not be able to
	 * see them as online (previous you could still see them, but they
	 * couldn't see you.
	 */
{(0x0000000000040000LL), {(9), (0x46), (19), (0x4d), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, {(0x0000000000020000LL), {(9), (0x46), (19), (0x4e), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, {(0x0000000080000000LL), {(9), (0x46), (0xf0), (3), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, {(0x0000000008000000LL), {(9), (0x46), (0xf0), (4), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, {(0x0000000080000000LL), {(9), (0x46), (0xf0), (5), (0x4c), (0x7f), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, {(0x0000000000008000LL), {(0x2e), (0x7a), (0x64), (0x75), (0xfa), (0xdf), (0x4d), (0xc8), (0x88), (0x6f), (0xea), (0x35), (0x95), (0xFD), (0xb6), (0xdf)}}, {(0x0000000010000000LL), {(0x56), (0x3f), (0xc8), (9), (11), (0x6f), (65), (0xbd), (0x9f), (0x79), (0x42), (0x26), (9), (0xdf), (0xa2), (0xf3)}}, 
/*
	 * Chat is oddball.
	 */
{(0x0000000000000008LL), {(0x74), (0x8f), (0x24), (32), (0x62), (0x87), (0x11), (0xd1), (0x82), (0x22), (0x44), (69), (0x53), (0x54), (0), (0)}}, 
/* This is added by the servers and it only shows up for ourselves... */
{(0x0000000080000000LL), {(0x97), (0xb1), (0x27), (0x51), (0x24), (60), (0x43), (0x34), (0xad), (0x22), (0xd6), (0xab), (0xf7), (0x3f), (20), (9)}}, {(0x0000000000001000LL), {(0x97), (0xb1), (0x27), (0x51), (0x24), (60), (0x43), (0x34), (0xad), (0x22), (0xd6), (0xab), (0xf7), (0x3f), (20), (0x92)}}, {(0x0000000000000800LL), {(0xaa), (0x4a), (50), (0xb5), (0xf8), (0x84), (0x48), (0xc6), (0xa3), (0xd7), (0x8c), (0x50), (0x97), (25), (0xFD), (0x5b)}}, {(0x0000000000010000LL), {(0xf2), (0xe7), (0xc7), (0xf4), (0xfe), (0xad), (0x4d), (0xfb), (0xb2), (0x35), (0x36), (0x79), (0x8b), (0xdf), (0), (0)}}, {(0x0000000000002000LL), {(0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0)}}, {(0x0000000100000000LL), {(1), (56), (0xca), (0x7b), (0x76), (0x9a), (0x49), (21), (0x88), (0xf2), (19), (0xfc), (0), (0x97), (0x9e), (0xa8)}}, {(0x0000000200000000LL), {(0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0)}}};
/* Keep this array synchronized with icq_purple_moods. */
static const struct __unnamed_class___F0_L258_C14_unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__mood__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_guint8Uc__typedef_declaration_index_16_Ae__variable_name_unknown_scope_and_name__scope__data {
const char *mood;
guint8 data[16UL];}icq_custom_icons[] = {{("thinking"), {(0x3f), (0xb0), (0xbd), (0x36), (0xaf), (0x3b), (0x4a), (0x60), (0x9e), (0xef), (0xcf), (25), (15), (0x6a), (0x5a), (0x7f)}}, {("busy"), {(0x48), (0x8e), (20), (0x89), (0x8a), (0xca), (0x4a), (8), (0x82), (0xaa), (0x77), (0xce), (0x7a), (22), (0x52), (8)}}, {("shopping"), {(0x63), (0x62), (0x73), (0x37), (0xa0), (0x3f), (0x49), (0xff), (128), (0xe5), (0xf7), (9), (0xcd), (0xe0), (0xa4), (0xee)}}, 
/* This was in the original patch, but isn't what the official client
	 * (ICQ 6) sets when you choose its typewriter icon. */
{("typing"), {(0x63), (0x4f), (0x6b), (0xd8), (0xad), (0xd2), (0x4a), (0xa1), (0xaa), (0xb9), (0x11), (0x5b), (0xc2), (0x6d), (5), (0xa1)}}, {("question"), {(0x63), (20), (0x36), (0xff), (0x3f), (0x8a), (64), (0xd0), (0xa5), (0xcb), (0x7b), (0x66), (0xe0), (0x51), (0xb3), (0x64)}}, {("angry"), {(1), (0xd8), (0xd7), (0xee), (0xac), (0x3b), (0x49), (0x2a), (0xa5), (0x8d), (0xd3), (0xd8), (0x77), (0xe6), (0x6b), (0x92)}}, {("plate"), {(0xf8), (0xe8), (0xd7), (0xb2), (0x82), (0xc4), (65), (0x42), (0x90), (0xf8), (16), (0xc6), (0xce), (10), (0x89), (0xa6)}}, {("cinema"), {(16), (0x7a), (0x9a), (24), (18), (50), (0x4d), (0xa4), (0xb6), (0xcd), (8), (0x79), (0xdb), (0x78), (15), (9)}}, {("sick"), {(31), (0x7a), (64), (0x71), (0xbf), (0x3b), (0x4e), (0x60), (0xbc), (50), (0x4c), (0x57), (0x87), (0xb0), (0x4c), (0xf1)}}, {("typing"), {(0x2c), (0xe0), (0xe4), (0xe5), (0x7c), (0x64), (0x43), (0x70), (0x9c), (0x3a), (0x7a), (28), (0xe8), (0x78), (0xa7), (0xdc)}}, {("suit"), {(0xb7), (8), (0x67), (0xf5), (56), (0x25), (0x43), (0x27), (0xa1), (0xff), (0xcf), (0x4c), (0xc1), (0x93), (0x97), (0x97)}}, {("bathing"), {(0x5a), (0x58), (0x1e), (0xa1), (0xe5), (128), (0x43), (12), (0xa0), (0x6f), (0x61), (0x22), (0x98), (0xb7), (0xe4), (0xc7)}}, {("tv"), {(128), (0x53), (0x7d), (0xe2), (0xa4), (0x67), (0x4a), (0x76), (0xb3), (0x54), (0x6d), (0xFD), (7), (0x5f), (0x5e), (0xc6)}}, {("excited"), {(0x6f), (0x49), (48), (0x98), (0x4f), (0x7c), (0x4a), (0xff), (0xa2), (0x76), (0x34), (0xa0), (0x3b), (0xce), (0xae), (0xa7)}}, {("sleeping"), {(0x78), (0x5e), (0x8c), (0x48), (64), (0xd3), (0x4c), (0x65), (0x88), (0x6f), (4), (0xcf), (0x3f), (0x3f), (0x43), (0xdf)}}, {("hiptop"), {(16), (0x11), (23), (0xc9), (0xa3), (0xb0), (64), (0xf9), (0x81), (0xac), (0x49), (0xe1), (0x59), (0xfb), (0xd5), (0xd4)}}, {("in_love"), {(0xdd), (0xcf), (14), (0xa9), (0x71), (0x95), (64), (0x48), (0xa9), (0xc6), (65), (50), (6), (0xd6), (0xf2), (128)}}, {("sleepy"), {(0x83), (0xc9), (0xb7), (0x8e), (0x77), (0xe7), (0x43), (0x78), (0xb2), (0xc5), (0xfb), (108), (0xfc), (0xc3), (0x5b), (0xec)}}, {("meeting"), {(0xf1), (0x8a), (0xb5), (0x2e), (0xdc), (0x57), (0x49), (0x1d), (0x99), (0xdc), (0x64), (0x44), (0x50), (0x24), (0x57), (0xaf)}}, {("phone"), {(18), (0x92), (0xe5), (0x50), (27), (0x64), (0x4f), (0x66), (0xb2), (6), (0xb2), (0x9a), (0xf3), (0x78), (0xe4), (0x8d)}}, {("surfing"), {(0xa6), (0xed), (0x55), (0x7e), (0x6b), (0xf7), (0x44), (0xd4), (0xa5), (0xd4), (0xd2), (0xe7), (0xd9), (0x5c), (0xe8), (31)}}, {("mobile"), {(22), (12), (0x60), (0xbb), (0xdd), (0x44), (0x43), (0xf3), (0x91), (64), (5), (15), (0), (0xe6), (0xc0), (9)}}, {("search"), {(0xd4), (0xe2), (0xb0), (0xba), (0x33), (0x4e), (0x4f), (0xa5), (0x98), (0xd0), (0x11), (0x7d), (0xbf), (0x4d), (60), (0xc8)}}, {("party"), {(0xe6), (1), (0xe4), (28), (0x33), (0x73), (0x4b), (0xd1), (0xbc), (6), (0x81), (0x1d), (108), (50), (0x3d), (0x81)}}, {("coffee"), {(27), (0x78), (0xae), (0x31), (0xfa), (11), (0x4d), (56), (0x93), (0xd1), (0x99), (0x7e), (0xee), (0xaf), (0xb2), (24)}}, {("console"), {(0xd4), (0xa6), (0x11), (0xd0), (0x8f), (1), (0x4e), (0xc0), (0x92), (0x23), (0xc5), (0xb6), (0xbe), (0xc6), (0xcc), (0xf0)}}, {("internet"), {(18), (0xd0), (0x7e), (0x3e), (0xf8), (0x85), (0x48), (0x9e), (0x8e), (0x97), (0xa7), (0x2a), (0x65), (0x51), (0xe5), (0x8d)}}, {("cigarette"), {(0x64), (0x43), (0xc6), (0xaf), (0x22), (0x60), (69), (23), (0xb5), (0x8c), (0xd7), (0xdf), (0x8e), (0x29), (3), (0x52)}}, {("writing"), {(0), (0x72), (0xd9), (8), (0x4a), (0xd1), (0x43), (0xdd), (0x91), (0x99), (0x6f), (2), (0x69), (0x66), (2), (0x6f)}}, {("beer"), {(0x8c), (0x50), (0xdb), (0xae), (0x81), (0xed), (0x47), (0x86), (0xac), (0xca), (22), (0xcc), (50), (19), (0xc7), (0xb7)}}, {("music"), {(0x61), (0xbe), (0xe0), (0xdd), (0x8b), (0xdd), (0x47), (0x5d), (0x8d), (0xee), (0x5f), (0x4b), (0xaa), (0xcf), (25), (0xa7)}}, {("studying"), {(0x60), (0x9d), (0x52), (0xf8), (0xa2), (0x9a), (0x49), (0xa6), (0xb2), (0xa0), (0x25), (0x24), (0xc5), (0xe9), (0xd2), (0x60)}}, {("working"), {(0xba), (0x74), (0xdb), (0x3e), (0x9e), (0x24), (0x43), (0x4b), (0x87), (0xb6), (0x2f), (0x6b), (0x8d), (0xfe), (0xe5), (15)}}, {("restroom"), {(22), (0xf5), (0xb7), (0x6f), (0xa9), (0xd2), (64), (0x35), (0x8c), (0xc5), (0xc0), (0x84), (0x70), (60), (0x98), (0xfa)}}, {((const char *)((void *)0)), {(0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0)}}};
/* Keep this array synchronized with icq_custom_icons. */
static PurpleMood icq_purple_moods[] = {{("thinking"), ("Thinking"), ((gpointer *)((void *)0))}, {("busy"), ("Busy"), ((gpointer *)((void *)0))}, {("shopping"), ("Shopping"), ((gpointer *)((void *)0))}, 
/* This was in the original patch, but isn't what the official client
	 * (ICQ 6) sets when you choose its typewriter icon. */
{("typing"), ((const char *)((void *)0)), ((gpointer *)((void *)0))}, {("question"), ("Questioning"), ((gpointer *)((void *)0))}, {("angry"), ("Angry"), ((gpointer *)((void *)0))}, {("plate"), ("Eating"), ((gpointer *)((void *)0))}, {("cinema"), ("Watching a movie"), ((gpointer *)((void *)0))}, {("sick"), ("Sick"), ((gpointer *)((void *)0))}, {("typing"), ("Typing"), ((gpointer *)((void *)0))}, {("suit"), ("At the office"), ((gpointer *)((void *)0))}, {("bathing"), ("Taking a bath"), ((gpointer *)((void *)0))}, {("tv"), ("Watching TV"), ((gpointer *)((void *)0))}, {("excited"), ("Having fun"), ((gpointer *)((void *)0))}, {("sleeping"), ("Sleeping"), ((gpointer *)((void *)0))}, {("hiptop"), ("Using a PDA"), ((gpointer *)((void *)0))}, {("in_love"), ("In love"), ((gpointer *)((void *)0))}, 
/* Sleepy / Tired */
{("sleepy"), ("Sleepy"), ((gpointer *)((void *)0))}, {("meeting"), ("Meeting friends"), ((gpointer *)((void *)0))}, {("phone"), ("On the phone"), ((gpointer *)((void *)0))}, {("surfing"), ("Surfing"), ((gpointer *)((void *)0))}, 
/* "I am mobile." / "John is mobile." */
{("mobile"), ("Mobile"), ((gpointer *)((void *)0))}, {("search"), ("Searching the web"), ((gpointer *)((void *)0))}, {("party"), ("At a party"), ((gpointer *)((void *)0))}, {("coffee"), ("Having Coffee"), ((gpointer *)((void *)0))}, 
/* Playing video games */
{("console"), ("Gaming"), ((gpointer *)((void *)0))}, {("internet"), ("Browsing the web"), ((gpointer *)((void *)0))}, {("cigarette"), ("Smoking"), ((gpointer *)((void *)0))}, {("writing"), ("Writing"), ((gpointer *)((void *)0))}, 
/* Drinking [Alcohol] */
{("beer"), ("Drinking"), ((gpointer *)((void *)0))}, {("music"), ("Listening to music"), ((gpointer *)((void *)0))}, {("studying"), ("Studying"), ((gpointer *)((void *)0))}, {("working"), ("Working"), ((gpointer *)((void *)0))}, {("restroom"), ("In the restroom"), ((gpointer *)((void *)0))}, 
/* Mark the last record. */
{((const char *)((void *)0)), ((const char *)((void *)0)), ((gpointer *)((void *)0))}};
/*
 * Add the userinfo to our linked list.  If we already have userinfo
 * for this buddy, then just overwrite parts of the old data.
 *
 * @param userinfo Contains the new information for the buddy.
 */

static void aim_locate_adduserinfo(OscarData *od,aim_userinfo_t *userinfo)
{
  aim_userinfo_t *cur;
  cur = aim_locate_finduserinfo(od,(userinfo -> bn));
  if (cur == ((aim_userinfo_t *)((void *)0))) {
    cur = ((aim_userinfo_t *)(g_malloc0_n(1,(sizeof(aim_userinfo_t )))));
    cur -> bn = g_strdup((userinfo -> bn));
    cur -> next = od -> locate.userinfo;
    od -> locate.userinfo = cur;
  }
  cur -> warnlevel = (userinfo -> warnlevel);
  cur -> idletime = (userinfo -> idletime);
  if ((userinfo -> flags) != 0) 
    cur -> flags = (userinfo -> flags);
  if ((userinfo -> createtime) != 0) 
    cur -> createtime = (userinfo -> createtime);
  if ((userinfo -> membersince) != 0) 
    cur -> membersince = (userinfo -> membersince);
  if ((userinfo -> onlinesince) != 0) 
    cur -> onlinesince = (userinfo -> onlinesince);
  if ((userinfo -> sessionlen) != 0) 
    cur -> sessionlen = (userinfo -> sessionlen);
  if ((userinfo -> capabilities) != 0) 
    cur -> capabilities = (userinfo -> capabilities);
  cur -> present |= (userinfo -> present);
  if ((userinfo -> iconcsumlen) > 0) {
    g_free((cur -> iconcsum));
    cur -> iconcsum = ((guint8 *)(g_malloc((userinfo -> iconcsumlen))));
    memcpy((cur -> iconcsum),(userinfo -> iconcsum),(userinfo -> iconcsumlen));
    cur -> iconcsumlen = (userinfo -> iconcsumlen);
  }
  if ((userinfo -> info) != ((char *)((void *)0))) {
    g_free((cur -> info));
    g_free((cur -> info_encoding));
    if ((userinfo -> info_len) > 0) {
      cur -> info = ((char *)(g_malloc((userinfo -> info_len))));
      memcpy((cur -> info),(userinfo -> info),(userinfo -> info_len));
    }
    else 
      cur -> info = ((char *)((void *)0));
    cur -> info_encoding = g_strdup((userinfo -> info_encoding));
    cur -> info_len = (userinfo -> info_len);
  }
  if ((userinfo -> status) != ((char *)((void *)0))) {
    g_free((cur -> status));
    g_free((cur -> status_encoding));
    if ((userinfo -> status_len) > 0) {
      cur -> status = ((char *)(g_malloc((userinfo -> status_len))));
      memcpy((cur -> status),(userinfo -> status),(userinfo -> status_len));
    }
    else 
      cur -> status = ((char *)((void *)0));
    if ((userinfo -> status_encoding) != ((char *)((void *)0))) 
      cur -> status_encoding = g_strdup((userinfo -> status_encoding));
    else 
      cur -> status_encoding = ((char *)((void *)0));
    cur -> status_len = (userinfo -> status_len);
  }
  if ((userinfo -> itmsurl) != ((char *)((void *)0))) {
    g_free((cur -> itmsurl));
    g_free((cur -> itmsurl_encoding));
    if ((userinfo -> itmsurl_len) > 0) {
      cur -> itmsurl = ((char *)(g_malloc((userinfo -> itmsurl_len))));
      memcpy((cur -> itmsurl),(userinfo -> itmsurl),(userinfo -> itmsurl_len));
    }
    else 
      cur -> itmsurl = ((char *)((void *)0));
    if ((userinfo -> itmsurl_encoding) != ((char *)((void *)0))) 
      cur -> itmsurl_encoding = g_strdup((userinfo -> itmsurl_encoding));
    else 
      cur -> itmsurl_encoding = ((char *)((void *)0));
    cur -> itmsurl_len = (userinfo -> itmsurl_len);
  }
  if ((userinfo -> away) != ((char *)((void *)0))) {
    g_free((cur -> away));
    g_free((cur -> away_encoding));
    if ((userinfo -> away_len) > 0) {
      cur -> away = ((char *)(g_malloc((userinfo -> away_len))));
      memcpy((cur -> away),(userinfo -> away),(userinfo -> away_len));
    }
    else 
      cur -> away = ((char *)((void *)0));
    cur -> away_encoding = g_strdup((userinfo -> away_encoding));
    cur -> away_len = (userinfo -> away_len);
  }
  else {
/*
		 * We don't have an away message specified in this user_info
		 * block, so clear any cached away message now.
		 */
    if ((cur -> away) != 0) {
      g_free((cur -> away));
      cur -> away = ((char *)((void *)0));
    }
    if ((cur -> away_encoding) != 0) {
      g_free((cur -> away_encoding));
      cur -> away_encoding = ((char *)((void *)0));
    }
    cur -> away_len = 0;
  }
}

aim_userinfo_t *aim_locate_finduserinfo(OscarData *od,const char *bn)
{
  aim_userinfo_t *cur = (aim_userinfo_t *)((void *)0);
  if (bn == ((const char *)((void *)0))) 
    return 0;
  cur = od -> locate.userinfo;
  while(cur != ((aim_userinfo_t *)((void *)0))){
    if (oscar_util_name_compare((cur -> bn),bn) == 0) 
      return cur;
    cur = (cur -> next);
  }
  return 0;
}

guint64 aim_locate_getcaps(OscarData *od,ByteStream *bs,int len)
{
  guint64 flags = 0;
  int offset;
  for (offset = 0; (byte_stream_bytes_left(bs) != 0) && (offset < len); offset += 16) {
    guint8 *cap;
    int i;
    int identified;
    cap = byte_stream_getraw(bs,16);
{
      for (((i = 0) , (identified = 0)); !((aim_caps[i].flag & 0x0000000200000000LL) != 0ULL); i++) {
        if (memcmp((&aim_caps[i].data),cap,16) == 0) {
          flags |= aim_caps[i].flag;
          identified++;
/* should only match once... */
          break; 
        }
      }
    }
    if (!(identified != 0)) 
      purple_debug_misc("oscar","unknown capability: {%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x}\n",cap[0],cap[1],cap[2],cap[3],cap[4],cap[5],cap[6],cap[7],cap[8],cap[9],cap[10],cap[11],cap[12],cap[13],cap[14],cap[15]);
    g_free(cap);
  }
  return flags;
}

static const char *aim_receive_custom_icon(OscarData *od,ByteStream *bs,int len)
{
  int offset;
  const char *result = (const char *)((void *)0);
  for (offset = 0; (byte_stream_bytes_left(bs) != 0) && (offset < len); offset += 16) {
/* check wheather this capability is a custom user icon */
    guint8 *cap;
    int i;
    cap = byte_stream_getraw(bs,16);
{
      for (i = 0; icq_custom_icons[i].mood != 0; i++) {
        if (memcmp((&icq_custom_icons[i].data),cap,16) == 0) {
          purple_debug_misc("oscar","Custom status icon: %s\n",icq_purple_moods[i].description);
          result = icq_custom_icons[i].mood;
/* should only match once... */
          break; 
        }
      }
    }
    g_free(cap);
  }
  return result;
}

guint64 aim_locate_getcaps_short(OscarData *od,ByteStream *bs,int len)
{
  guint64 flags = 0;
  int offset;
  for (offset = 0; (byte_stream_bytes_left(bs) != 0) && (offset < len); offset += 2) {
    guint8 *cap;
    int i;
    int identified;
    cap = byte_stream_getraw(bs,2);
{
      for (((i = 0) , (identified = 0)); !((aim_caps[i].flag & 0x0000000200000000LL) != 0ULL); i++) {
        if (memcmp((aim_caps[i].data + 2),cap,2) == 0) {
          flags |= aim_caps[i].flag;
          identified++;
/* should only match once... */
          break; 
        }
      }
    }
    if (!(identified != 0)) 
      purple_debug_misc("oscar","unknown short capability: {%02x%02x}\n",cap[0],cap[1]);
    g_free(cap);
  }
  return flags;
}

int byte_stream_putcaps(ByteStream *bs,guint64 caps)
{
  int i;
  if (!(bs != 0)) 
    return -22;
{
    for (i = 0; byte_stream_bytes_left(bs) != 0; i++) {
      if (aim_caps[i].flag == 0x0000000200000000LL) 
        break; 
      if ((caps & aim_caps[i].flag) != 0UL) 
        byte_stream_putraw(bs,aim_caps[i].data,16);
    }
  }
  return 0;
}
#ifdef LOG_UNKNOWN_TLV
#endif

void aim_info_free(aim_userinfo_t *info)
{
  g_free((info -> bn));
  g_free((info -> iconcsum));
  g_free((info -> info));
  g_free((info -> info_encoding));
  g_free((info -> status));
  g_free((info -> status_encoding));
  g_free((info -> itmsurl));
  g_free((info -> itmsurl_encoding));
  g_free((info -> away));
  g_free((info -> away_encoding));
}
static const struct __unnamed_class___F0_L731_C14_unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__icqmood__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__mood {
char *icqmood;
const char *mood;}icqmoods[] = {{("icqmood0"), ("shopping")}, {("icqmood1"), ("bathing")}, {("icqmood2"), ("sleepy")}, {("icqmood3"), ("party")}, {("icqmood4"), ("beer")}, {("icqmood5"), ("thinking")}, {("icqmood6"), ("plate")}, {("icqmood7"), ("tv")}, {("icqmood8"), ("meeting")}, {("icqmood9"), ("coffee")}, {("icqmood10"), ("music")}, {("icqmood11"), ("suit")}, {("icqmood12"), ("cinema")}, {("icqmood13"), ("smile-big")}, {("icqmood14"), ("phone")}, {("icqmood15"), ("console")}, {("icqmood16"), ("studying")}, {("icqmood17"), ("sick")}, {("icqmood18"), ("sleeping")}, {("icqmood19"), ("surfing")}, {("icqmood20"), ("internet")}, {("icqmood21"), ("working")}, {("icqmood22"), ("typing")}, {("icqmood23"), ("angry")}, {((char *)((void *)0)), (0)}};
/*
 * AIM is fairly regular about providing user info.  This is a generic
 * routine to extract it in its standard form.
 */

int aim_info_extract(OscarData *od,ByteStream *bs,aim_userinfo_t *outinfo)
{
  int curtlv;
  int tlvcnt;
  guint8 bnlen;
  if (!(bs != 0) || !(outinfo != 0)) 
    return -22;
/* Clear out old data first */
  memset(outinfo,0,(sizeof(aim_userinfo_t )));
/*
	 * Username.  Stored as an unterminated string prepended with a
	 * byte containing its length.
	 */
  bnlen = byte_stream_get8(bs);
  outinfo -> bn = byte_stream_getstr(bs,bnlen);
/*
	 * Warning Level.  Stored as an unsigned short.
	 */
  outinfo -> warnlevel = byte_stream_get16(bs);
/*
	 * TLV Count. Unsigned short representing the number of
	 * Type-Length-Value triples that follow.
	 */
  tlvcnt = (byte_stream_get16(bs));
/*
	 * Parse out the Type-Length-Value triples as they're found.
	 */
  for (curtlv = 0; curtlv < tlvcnt; curtlv++) {
    guint16 type;
    guint16 length;
    int endpos;
    int curpos;
    type = byte_stream_get16(bs);
    length = byte_stream_get16(bs);
    curpos = byte_stream_curpos(bs);
    endpos = (curpos + (((length < byte_stream_bytes_left(bs))?length : byte_stream_bytes_left(bs))));
    if (type == 1) {
/*
			 * User flags
			 *
			 * Specified as any of the following ORed together:
			 *      0x0001  Unconfirmed account
			 *      0x0002  Unknown bit 2
			 *      0x0004  AOL Main Service user
			 *      0x0008  Unknown bit 4
			 *      0x0010  Free (AIM) user
			 *      0x0020  Away
			 *      0x0040  ICQ user (AIM bit also set)
			 *      0x0080  Mobile device
			 *      0x0400  Bot (like ActiveBuddy)
			 */
      outinfo -> flags = byte_stream_get16(bs);
      outinfo -> present |= 1;
    }
    else if (type == 2) {
/*
			 * Account creation time
			 *
			 * The time/date that the user originally registered for
			 * the service, stored in time_t format.
			 *
			 * I'm not sure how this differs from type 5 ("member
			 * since").
			 *
			 * Note: This is the field formerly known as "member
			 * since".  All these years and I finally found out
			 * that I got the name wrong.
			 */
      outinfo -> createtime = byte_stream_get32(bs);
      outinfo -> present |= 0x00000200;
    }
    else if (type == 3) {
/*
			 * On-Since date
			 *
			 * The time/date that the user started their current
			 * session, stored in time_t format.
			 */
      outinfo -> onlinesince = byte_stream_get32(bs);
      outinfo -> present |= 4;
    }
    else if (type == 4) {
/*
			 * Idle time
			 *
			 * Number of minutes since the user actively used the
			 * service.
			 *
			 * Note that the client tells the server when to start
			 * counting idle times, so this may or may not be
			 * related to reality.
			 */
      outinfo -> idletime = byte_stream_get16(bs);
      outinfo -> present |= 8;
    }
    else if (type == 5) {
/*
			 * Member since date
			 *
			 * The time/date that the user originally registered for
			 * the service, stored in time_t format.
			 *
			 * This is sometimes sent instead of type 2 ("account
			 * creation time"), particularly in the self-info.
			 * And particularly for ICQ?
			 */
      outinfo -> membersince = byte_stream_get32(bs);
      outinfo -> present |= 2;
    }
    else if (type == 6) {
/*
			 * ICQ Online Status
			 *
			 * ICQ's Away/DND/etc "enriched" status. Some decoding
			 * of values done by Scott <darkagl@pcnet.com>
			 */
      byte_stream_get16(bs);
      outinfo -> icqinfo.status = (byte_stream_get16(bs));
      outinfo -> present |= 16;
    }
    else if (type == 8) {
/*
			 * Client type, or some such.
			 */
    }
    else if (type == 10) {
/*
			 * ICQ User IP Address
			 *
			 * Ahh, the joy of ICQ security.
			 */
      outinfo -> icqinfo.ipaddr = byte_stream_get32(bs);
      outinfo -> present |= 32;
    }
    else if (type == 12) {
/*
			 * Random crap containing the IP address,
			 * apparently a port number, and some Other Stuff.
			 *
			 * Format is:
			 * 4 bytes - Our IP address, 0xc0 a8 01 2b for 192.168.1.43
			 */
      byte_stream_getrawbuf(bs,outinfo -> icqinfo.crap,0x25);
      outinfo -> present |= 64;
    }
    else if (type == 13) {
      PurpleAccount *account = purple_connection_get_account((od -> gc));
      const char *mood;
/*
			 * OSCAR Capability information
			 */
      outinfo -> capabilities |= aim_locate_getcaps(od,bs,length);
      outinfo -> present |= 128;
      byte_stream_setpos(bs,curpos);
      mood = aim_receive_custom_icon(od,bs,length);
      if (mood != 0) 
        purple_prpl_got_user_status(account,(outinfo -> bn),"mood","mood",mood,((void *)((void *)0)));
      else 
        purple_prpl_got_user_status_deactive(account,(outinfo -> bn),"mood");
    }
    else if (type == 14) {
/*
			 * AOL capability information
			 */
    }
    else if ((type == 15) || (type == 16)) {
/*
			 * Type = 0x000f: Session Length. (AIM)
			 * Type = 0x0010: Session Length. (AOL)
			 *
			 * The duration, in seconds, of the user's current
			 * session.
			 *
			 * Which TLV type this comes in depends on the
			 * service the user is using (AIM or AOL).
			 */
      outinfo -> sessionlen = byte_stream_get32(bs);
      outinfo -> present |= 256;
    }
    else if (type == 20) {
/*
			 * My instance number.
			 */
      guint8 instance_number;
      instance_number = byte_stream_get8(bs);
    }
    else if (type == 25) {
/*
			 * OSCAR short capability information.  A shortened
			 * form of the normal capabilities.
			 */
      outinfo -> capabilities |= aim_locate_getcaps_short(od,bs,length);
      outinfo -> present |= 128;
    }
    else if (type == 0x1a) {
/*
			 * Type = 0x001a
			 *
			 * AOL short capability information.  A shortened
			 * form of the normal capabilities.
			 */
    }
    else if (type == 27) {
/*
			 * Encryption certification MD5 checksum.
			 */
    }
    else if (type == 0x1d) {
/*
			 * Buddy icon information and status/available messages.
			 *
			 * This almost seems like the AIM protocol guys gave
			 * the iChat guys a Type, and the iChat guys tried to
			 * cram as much cool shit into it as possible.  Then
			 * the Windows AIM guys were like, "hey, that's
			 * pretty neat, let's copy those prawns."
			 *
			 * In that spirit, this can contain a custom message,
			 * kind of like an away message, but you're not away
			 * (it's called an "available" message).  Or it can
			 * contain information about the buddy icon the user
			 * has stored on the server.
			 */
      guint16 type2;
      guint8 number2;
      guint8 length2;
      int endpos2;
/*
			 * Continue looping as long as we're able to read type2,
			 * number2, and length2.
			 */
      while((byte_stream_curpos(bs) + 4) <= endpos){
        type2 = byte_stream_get16(bs);
        number2 = byte_stream_get8(bs);
        length2 = byte_stream_get8(bs);
        endpos2 = (byte_stream_curpos(bs) + (((length2 < byte_stream_bytes_left(bs))?length2 : byte_stream_bytes_left(bs))));
        switch(type2){
/* This is an official buddy icon? */
          case 0x0000:
{
{
/* This is always 5 bytes of "0x02 01 d2 04 72"? */
            }
            break; 
          }
/* A buddy icon checksum */
          case 0x0001:
{
{
              if ((length2 > 0) && ((number2 == 0) || (number2 == 1))) {
                g_free((outinfo -> iconcsum));
                outinfo -> iconcsumtype = number2;
                outinfo -> iconcsum = byte_stream_getraw(bs,length2);
                outinfo -> iconcsumlen = length2;
              }
            }
            break; 
          }
/* A status/available message */
          case 0x0002:
{
{
              g_free((outinfo -> status));
              g_free((outinfo -> status_encoding));
              if (length2 >= 4) {
                outinfo -> status_len = byte_stream_get16(bs);
                outinfo -> status = byte_stream_getstr(bs,(outinfo -> status_len));
/* We have an encoding */
                if ((byte_stream_get16(bs)) == 1) {
                  byte_stream_get16(bs);
                  outinfo -> status_encoding = byte_stream_getstr(bs,(byte_stream_get16(bs)));
                }
                else {
/* No explicit encoding, client should use UTF-8 */
                  outinfo -> status_encoding = ((char *)((void *)0));
                }
              }
              else {
                byte_stream_advance(bs,length2);
                outinfo -> status_len = 0;
                outinfo -> status = g_strdup("");
                outinfo -> status_encoding = ((char *)((void *)0));
              }
            }
            break; 
          }
/* An iTunes Music Store link */
          case 0x0009:
{
{
              g_free((outinfo -> itmsurl));
              g_free((outinfo -> itmsurl_encoding));
              if (length2 >= 4) {
                outinfo -> itmsurl_len = byte_stream_get16(bs);
                outinfo -> itmsurl = byte_stream_getstr(bs,(outinfo -> itmsurl_len));
                if ((byte_stream_get16(bs)) == 1) {
/* We have an encoding */
                  byte_stream_get16(bs);
                  outinfo -> itmsurl_encoding = byte_stream_getstr(bs,(byte_stream_get16(bs)));
                }
                else {
/* No explicit encoding, client should use UTF-8 */
                  outinfo -> itmsurl_encoding = ((char *)((void *)0));
                }
              }
              else {
                byte_stream_advance(bs,length2);
                outinfo -> itmsurl_len = 0;
                outinfo -> itmsurl = g_strdup("");
                outinfo -> itmsurl_encoding = ((char *)((void *)0));
              }
            }
            break; 
          }
/* ICQ mood */
          case 0x000e:
{
{
              PurpleAccount *account = purple_connection_get_account((od -> gc));
              char *icqmood;
              gint32 i;
              const char *mood = (const char *)((void *)0);
              icqmood = byte_stream_getstr(bs,length2);
/* icqmood = "" means X-Status
						 * with no mood icon. */
              if (( *icqmood) != 0) {{
                  for (i = 0; icqmoods[i].icqmood != 0; i++) {
                    if (!(strcmp(icqmood,icqmoods[i].icqmood) != 0)) {
                      mood = icqmoods[i].mood;
/* should only match once... */
                      break; 
                    }
                  }
                }
                if (!(mood != 0)) 
                  purple_debug_warning("oscar","Unknown icqmood: %s\n",icqmood);
              }
              g_free(icqmood);
              if (mood != 0) 
                purple_prpl_got_user_status(account,(outinfo -> bn),"mood","mood",mood,((void *)((void *)0)));
              else 
                purple_prpl_got_user_status_deactive(account,(outinfo -> bn),"mood");
            }
            break; 
          }
        }
/* Save ourselves. */
        byte_stream_setpos(bs,endpos2);
      }
    }
    else if (type == 0x1e) {
/*
			 * Always four bytes, but it doesn't look like an int.
			 */
    }
    else if (type == 31) {
/*
			 * Upper bytes of user flags.  Can be any size
			 *
			 * Seen on a buddy using DeadAIM.  Data was 4 bytes:
			 * 0x00 00 00 10
			 */
    }
    else if (type == 0x23) {
/*
			 * Last Buddy Feed update time, in seconds since the epoch.
			 */
    }
    else if (type == 0x26) {
/*
			 * Time that the profile was set, in seconds since the epoch.
			 */
    }
    else if (type == 0x27) {
/*
			 * Time that the away message was set, in seconds since the epoch.
			 */
    }
    else if (type == 0x2a) {
/*
			 * Country code based on GeoIP data.
			 */
    }
    else {
/*
			 * Reaching here indicates that either AOL has
			 * added yet another TLV for us to deal with,
			 * or the parsing has gone Terribly Wrong.
			 *
			 * Either way, inform the owner and attempt
			 * recovery.
			 *
			 */
#ifdef LOG_UNKNOWN_TLV
#endif
    }
/* Save ourselves. */
    byte_stream_setpos(bs,endpos);
  }
  aim_locate_adduserinfo(od,outinfo);
  return 0;
}
/*
 * Subtype 0x0001
 */

static int error(OscarData *od,FlapConnection *conn,aim_module_t *mod,FlapFrame *frame,aim_modsnac_t *snac,ByteStream *bs)
{
  aim_snac_t *snac2;
  guint16 reason;
  char *bn;
  snac2 = aim_remsnac(od,(snac -> id));
  if (!(snac2 != 0)) {
    purple_debug_misc("oscar","locate error: received response from unknown request!\n");
    return 0;
  }
  if (((snac2 -> family) != 2) && ((snac2 -> type) != 21)) {
    purple_debug_misc("oscar","locate error: received response from invalid request! %d\n",(snac2 -> family));
    g_free((snac2 -> data));
    g_free(snac2);
    return 0;
  }
  bn = (snac2 -> data);
  if (!(bn != 0)) {
    purple_debug_misc("oscar","locate error: received response from request without a buddy name!\n");
    g_free(snac2);
    return 0;
  }
  reason = byte_stream_get16(bs);
  oscar_user_info_display_error(od,reason,bn);
  g_free((snac2 -> data));
  g_free(snac2);
  return 1;
}
/*
 * Subtype 0x0002
 *
 * Request Location services rights.
 *
 */

int aim_locate_reqrights(OscarData *od)
{
  FlapConnection *conn;
  if (!(od != 0) || !((conn = flap_connection_findbygroup(od,2)) != 0)) 
    return -22;
  aim_genericreq_n_snacid(od,conn,2,2);
  return 0;
}
/*
 * Subtype 0x0003
 *
 * Normally contains:
 *   t(0001)  - short containing max profile length (value = 1024)
 *   t(0002)  - short - unknown (value = 16) [max MIME type length?]
 *   t(0003)  - short - unknown (value = 10)
 *   t(0004)  - short - unknown (value = 2048) [ICQ only?]
 */

static int rights(OscarData *od,FlapConnection *conn,aim_module_t *mod,FlapFrame *frame,aim_modsnac_t *snac,ByteStream *bs)
{
  GSList *tlvlist;
  aim_rxcallback_t userfunc;
  int ret = 0;
  guint16 maxsiglen = 0;
  tlvlist = aim_tlvlist_read(bs);
  if (aim_tlv_gettlv(tlvlist,1,1) != 0) 
    maxsiglen = aim_tlv_get16(tlvlist,1,1);
  if ((userfunc = aim_callhandler(od,(snac -> family),(snac -> subtype))) != 0) 
    ret = ( *userfunc)(od,conn,frame,maxsiglen);
  aim_tlvlist_free(tlvlist);
  return ret;
}
/*
 * Subtype 0x0004
 *
 * Gives BOS your profile.
 *
 * profile_encoding and awaymsg_encoding MUST be set if profile or
 * away are set, respectively, and their value may or may not be
 * restricted to a few choices.  I am currently aware of:
 *
 * us-ascii		Just that
 * unicode-2-0		UTF-16BE
 *
 * profile_len and awaymsg_len MUST be set similarly, and they MUST
 * be the length of their respective strings in bytes.
 *
 * To get the previous behavior of awaymsg == "" un-setting the away
 * message, set awaymsg non-NULL and awaymsg_len to 0 (this is the
 * obvious equivalent).
 *
 */

int aim_locate_setprofile(OscarData *od,const char *profile_encoding,const gchar *profile,const int profile_len,const char *awaymsg_encoding,const gchar *awaymsg,const int awaymsg_len)
{
  FlapConnection *conn;
  ByteStream bs;
  aim_snacid_t snacid;
  GSList *tlvlist = (GSList *)((void *)0);
  char *encoding;
  static const char defencoding[] = "text/aolrtf; charset=\"%s\"";
  if (!(od != 0) || !((conn = flap_connection_findbygroup(od,2)) != 0)) 
    return -22;
  if (!(profile != 0) && !(awaymsg != 0)) 
    return -22;
  if (((profile != 0) && (profile_encoding == ((const char *)((void *)0)))) || (((awaymsg != 0) && (awaymsg_len != 0)) && (awaymsg_encoding == ((const char *)((void *)0))))) {
    return -22;
  }
/* Build the packet first to get real length */
  if (profile != 0) {
/* no + 1 here because of %s */
    encoding = (g_malloc((strlen(defencoding) + strlen(profile_encoding))));
    snprintf(encoding,(strlen(defencoding) + strlen(profile_encoding)),defencoding,profile_encoding);
    aim_tlvlist_add_str(&tlvlist,1,encoding);
    aim_tlvlist_add_raw(&tlvlist,2,profile_len,((const guchar *)profile));
    g_free(encoding);
  }
/*
	 * So here's how this works:
	 *   - You are away when you have a non-zero-length type 4 TLV stored.
	 *   - You become unaway when you clear the TLV with a zero-length
	 *       type 4 TLV.
	 *   - If you do not send the type 4 TLV, your status does not change
	 *       (that is, if you were away, you'll remain away).
	 */
  if (awaymsg != 0) {
    if (awaymsg_len != 0) {
      encoding = (g_malloc((strlen(defencoding) + strlen(awaymsg_encoding))));
      snprintf(encoding,(strlen(defencoding) + strlen(awaymsg_encoding)),defencoding,awaymsg_encoding);
      aim_tlvlist_add_str(&tlvlist,3,encoding);
      aim_tlvlist_add_raw(&tlvlist,4,awaymsg_len,((const guchar *)awaymsg));
      g_free(encoding);
    }
    else 
      aim_tlvlist_add_noval(&tlvlist,4);
  }
  byte_stream_new(&bs,(aim_tlvlist_size(tlvlist)));
  snacid = aim_cachesnac(od,2,4,0,0,0);
  aim_tlvlist_write(&bs,&tlvlist);
  aim_tlvlist_free(tlvlist);
  flap_connection_send_snac(od,conn,2,4,snacid,&bs);
  byte_stream_destroy(&bs);
  return 0;
}
/*
 * Subtype 0x0004 - Set your client's capabilities.
 */

int aim_locate_setcaps(OscarData *od,guint64 caps)
{
  FlapConnection *conn;
  PurpleAccount *account = purple_connection_get_account((od -> gc));
  PurplePresence *presence = purple_account_get_presence(account);
  PurpleStatus *status = purple_presence_get_status(presence,"mood");
  const char *mood = purple_status_get_attr_string(status,"mood");
  ByteStream bs;
  aim_snacid_t snacid;
  GSList *tlvlist = (GSList *)((void *)0);
  if (!((conn = flap_connection_findbygroup(od,2)) != 0)) 
    return -22;
  aim_tlvlist_add_caps(&tlvlist,5,caps,mood);
  byte_stream_new(&bs,(aim_tlvlist_size(tlvlist)));
  snacid = aim_cachesnac(od,2,4,0,0,0);
  aim_tlvlist_write(&bs,&tlvlist);
  aim_tlvlist_free(tlvlist);
  flap_connection_send_snac(od,conn,2,4,snacid,&bs);
  byte_stream_destroy(&bs);
  return 0;
}
/* Subtype 0x0006 */

static int userinfo(OscarData *od,FlapConnection *conn,aim_module_t *mod,FlapFrame *frame,aim_modsnac_t *snac,ByteStream *bs)
{
  int ret = 0;
  aim_userinfo_t *userinfo;
  aim_userinfo_t *userinfo2;
  GSList *tlvlist;
  aim_tlv_t *tlv = (aim_tlv_t *)((void *)0);
  userinfo = ((aim_userinfo_t *)(g_malloc((sizeof(aim_userinfo_t )))));
  aim_info_extract(od,bs,userinfo);
  tlvlist = aim_tlvlist_read(bs);
/* Profile will be 1 and 2 */
  userinfo -> info_encoding = aim_tlv_getstr(tlvlist,1,1);
  if ((tlv = aim_tlv_gettlv(tlvlist,2,1)) != 0) {
    userinfo -> info = ((char *)(g_malloc((tlv -> length))));
    memcpy((userinfo -> info),(tlv -> value),(tlv -> length));
    userinfo -> info_len = (tlv -> length);
  }
/* Away message will be 3 and 4 */
  userinfo -> away_encoding = aim_tlv_getstr(tlvlist,3,1);
  if ((tlv = aim_tlv_gettlv(tlvlist,4,1)) != 0) {
    userinfo -> away = ((char *)(g_malloc((tlv -> length))));
    memcpy((userinfo -> away),(tlv -> value),(tlv -> length));
    userinfo -> away_len = (tlv -> length);
  }
/* Caps will be 5 */
  if ((tlv = aim_tlv_gettlv(tlvlist,5,1)) != 0) {
    ByteStream cbs;
    PurpleAccount *account = purple_connection_get_account((od -> gc));
    const char *mood;
    byte_stream_init(&cbs,(tlv -> value),(tlv -> length));
    userinfo -> capabilities = aim_locate_getcaps(od,&cbs,(tlv -> length));
    byte_stream_rewind(&cbs);
    userinfo -> present = 128;
    mood = aim_receive_custom_icon(od,&cbs,(tlv -> length));
    if (mood != 0) 
      purple_prpl_got_user_status(account,(userinfo -> bn),"mood","mood",mood,((void *)((void *)0)));
    else 
      purple_prpl_got_user_status_deactive(account,(userinfo -> bn),"mood");
  }
  aim_tlvlist_free(tlvlist);
  aim_locate_adduserinfo(od,userinfo);
  userinfo2 = aim_locate_finduserinfo(od,(userinfo -> bn));
  aim_info_free(userinfo);
  g_free(userinfo);
/* Show the info to the user */
  oscar_user_info_display_aim(od,userinfo2);
  return ret;
}
/*
 * Subtype 0x0015 - Request the info of a user using the short method.  This is
 * what iChat uses.  It normally is VERY leniently rate limited.
 *
 * @param bn The buddy name whose info you wish to request.
 * @param flags The bitmask which specifies the type of info you wish to request.
 *        0x00000001 - Info/profile.
 *        0x00000002 - Away message.
 *        0x00000004 - Capabilities.
 *        0x00000008 - Certification.
 * @return Return 0 if no errors, otherwise return the error number.
 */

int aim_locate_getinfoshort(OscarData *od,const char *bn,guint32 flags)
{
  FlapConnection *conn;
  ByteStream bs;
  aim_snacid_t snacid;
  if ((!(od != 0) || !((conn = flap_connection_findbygroup(od,2)) != 0)) || !(bn != 0)) 
    return -22;
  byte_stream_new(&bs,((4 + 1) + strlen(bn)));
  byte_stream_put32(&bs,flags);
  byte_stream_put8(&bs,(strlen(bn)));
  byte_stream_putstr(&bs,bn);
  snacid = aim_cachesnac(od,2,21,0,bn,(strlen(bn) + 1));
  flap_connection_send_snac_with_priority(od,conn,2,21,snacid,&bs,0);
  byte_stream_destroy(&bs);
  return 0;
}

static int snachandler(OscarData *od,FlapConnection *conn,aim_module_t *mod,FlapFrame *frame,aim_modsnac_t *snac,ByteStream *bs)
{
  if ((snac -> subtype) == 1) 
    return error(od,conn,mod,frame,snac,bs);
  else if ((snac -> subtype) == 3) 
    return rights(od,conn,mod,frame,snac,bs);
  else if ((snac -> subtype) == 6) 
    return userinfo(od,conn,mod,frame,snac,bs);
  return 0;
}

static void locate_shutdown(OscarData *od,aim_module_t *mod)
{
  aim_userinfo_t *del;
  while(od -> locate.userinfo != 0){
    del = od -> locate.userinfo;
    od -> locate.userinfo = ( *od -> locate.userinfo).next;
    aim_info_free(del);
    g_free(del);
  }
}

int locate_modfirst(OscarData *od,aim_module_t *mod)
{
  mod -> family = 2;
  mod -> version = 1;
  mod -> toolid = 0x0110;
  mod -> toolversion = 0x0629;
  mod -> flags = 0;
  strncpy((mod -> name),"locate",(sizeof(mod -> name)));
  mod -> snachandler = snachandler;
  mod -> shutdown = locate_shutdown;
  return 0;
}

const char *icq_get_custom_icon_description(const char *mood)
{
  int i;
  if (!((mood != 0) && (( *mood) != 0))) 
    return 0;
  for (i = 0; icq_custom_icons[i].mood != 0; i++) {
/* We check that description is not NULL to exclude
		 * duplicates, like the typing duplicate. */
    if ((icq_purple_moods[i].description != 0) && !(strcmp(mood,icq_custom_icons[i].mood) != 0)) {
      return icq_purple_moods[i].description;
    }
  }
  return 0;
}

guint8 *icq_get_custom_icon_data(const char *mood)
{
  int i;
  if (!((mood != 0) && (( *mood) != 0))) 
    return 0;
  for (i = 0; icq_custom_icons[i].mood != 0; i++) {
/* We check that description is not NULL to exclude
		 * duplicates, like the typing duplicate. */
    if ((icq_purple_moods[i].description != 0) && !(strcmp(mood,icq_custom_icons[i].mood) != 0)) {
      return (guint8 *)icq_custom_icons[i].data;
    }
  }
  return 0;
}

PurpleMood *icq_get_purple_moods(PurpleAccount *account)
{
  return icq_purple_moods;
}
