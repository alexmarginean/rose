/*
 * Purple's oscar protocol plugin
 * This file is the legal property of its developers.
 * Please see the AUTHORS file distributed alongside this file.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
*/
/*
 * Family 0x0013 - Server-Side/Stored Information.
 *
 * Relatively new facility that allows certain types of information, such as
 * a user's buddy list, permit/deny list, and permit/deny preferences, to be
 * stored on the server, so that they can be accessed from any client.
 *
 * We keep 2 copies of SSI data:
 * 1) An exact copy of what is stored on the AIM servers.
 * 2) A local copy that we make changes to, and then send diffs
 *    between this and the exact copy to keep them in sync.
 *
 * All the "aim_ssi_itemlist_bleh" functions near the top just modify the list
 * that is given to them (i.e. they don't send SNACs).
 *
 * The SNAC sending and receiving functions are lower down in the file, and
 * they're simpler.  They are in the order of the subtypes they deal with,
 * starting with the request rights function (subtype 0x0002), then parse
 * rights (subtype 0x0003), then--well, you get the idea.
 *
 * This is entirely too complicated.
 * You don't know the half of it.
 *
 */
#include "oscar.h"
#include "debug.h"
static int aim_ssi_addmoddel(OscarData *od);
/**
 * List types based on http://dev.aol.com/aim/oscar/#FEEDBAG (archive.org)
 * and http://iserverd.khstu.ru/oscar/ssi_item.html
 *
 * @param type The type of a list item as integer number, as provided by an aim_ssi_item struct.
 * @return Returns the name of the item type as a character string.
 */

static const gchar *aim_ssi_type_to_string(guint16 type)
{
  

  struct TypeStringPair 
{
    guint16 type;
    const gchar *string;
  }
;
  static const struct TypeStringPair type_strings[] = {{(0), ("Buddy")}, {(1), ("Group")}, {(2), ("Permit/Visible")}, {(3), ("Deny/Invisible")}, {(4), ("PDInfo")}, {(5), ("PresencePrefs")}, {(6), ("Non-Buddy Info")}, {(9), ("ClientPrefs")}, {(14), ("ICQDeny/Ignore")}, {(20), ("Buddy Icon")}, {(21), ("Recent Buddies")}, {(25), ("Non-Buddy")}, {(0x001d), ("Vanity Info")}, {(32), ("ICQ-MDir")}, {(0x0029), ("Facebook")}};
  int i;
  for (i = 0; i < sizeof(type_strings) / sizeof(type_strings[0]); i++) {
    if (type_strings[i].type == type) {
      return type_strings[i].string;
    }
  }
  return "unknown";
}
/** For debug log output: Appends a line containing information about a given list item to a string.
 *
 * @param str String to which the line will be appended.
 * @param prefix A string which will be prepended to the line.
 * @param item List item from which information is extracted.
 */

static void aim_ssi_item_debug_append(GString *str,char *prefix,struct aim_ssi_item *item)
{
  g_string_append_printf(str,"%s gid=0x%04hx, bid=0x%04hx, list_type=0x%04hx [%s], name=%s.\n",prefix,(item -> gid),(item -> bid),(item -> type),aim_ssi_type_to_string((item -> type)),(((item -> name) != 0)?(item -> name) : "(null)"));
}
/**
 * Locally rebuild the 0x00c8 TLV in the additional data of the given group.
 *
 * @param list A pointer to a pointer to the current list of items.
 * @param name A null terminated string containing the group name, or NULL
 *        if you want to modify the master group.
 * @return Return a pointer to the modified item.
 */

static void aim_ssi_itemlist_rebuildgroup(struct aim_ssi_item *list,const char *name)
{
  int newlen;
  struct aim_ssi_item *cur;
  struct aim_ssi_item *group;
/* Find the group */
  if (!((group = aim_ssi_itemlist_finditem(list,name,0,1)) != 0)) 
    return ;
/* Find the length for the new additional data */
  newlen = 0;
  if ((group -> gid) == 0) {
    for (cur = list; cur != 0; cur = (cur -> next)) 
      if (((cur -> type) == 1) && ((cur -> gid) != 0)) 
        newlen += 2;
  }
  else {
    for (cur = list; cur != 0; cur = (cur -> next)) 
      if (((cur -> gid) == (group -> gid)) && ((cur -> type) == 0)) 
        newlen += 2;
  }
/* Build the new TLV list */
  if (newlen > 0) {
    guint8 *newdata;
    newdata = ((guint8 *)(g_malloc((newlen * sizeof(guint8 )))));
    newlen = 0;
    if ((group -> gid) == 0) {
      for (cur = list; cur != 0; cur = (cur -> next)) 
        if (((cur -> type) == 1) && ((cur -> gid) != 0)) 
          newlen += ((((( *(newdata + newlen) = (((guint8 )((cur -> gid) >> 8)) & 0xff)) , ( *((newdata + newlen) + 1) = (((guint8 )(cur -> gid)) & 0xff)))) , 2));
    }
    else {
      for (cur = list; cur != 0; cur = (cur -> next)) 
        if (((cur -> gid) == (group -> gid)) && ((cur -> type) == 0)) 
          newlen += ((((( *(newdata + newlen) = (((guint8 )((cur -> bid) >> 8)) & 0xff)) , ( *((newdata + newlen) + 1) = (((guint8 )(cur -> bid)) & 0xff)))) , 2));
    }
    aim_tlvlist_replace_raw(&group -> data,0x00c8,newlen,newdata);
    g_free(newdata);
  }
}
/**
 * Locally add a new item to the given item list.
 *
 * @param list A pointer to a pointer to the current list of items.
 * @param name A null terminated string of the name of the new item, or NULL if the
 *        item should have no name.
 * @param gid The group ID# you want the new item to have, or 0xFFFF if we should pick something.
 * @param bid The buddy ID# you want the new item to have, or 0xFFFF if we should pick something.
 * @param type The type of the item, 0x0000 for a contact, 0x0001 for a group, etc.
 * @param data The additional data for the new item.
 * @return A pointer to the newly created item.
 */

static struct aim_ssi_item *aim_ssi_itemlist_add(struct aim_ssi_item **list,const char *name,guint16 gid,guint16 bid,guint16 type,GSList *data)
{
  gboolean exists;
  struct aim_ssi_item *cur;
  struct aim_ssi_item *new;
  new = ((struct aim_ssi_item *)(g_malloc_n(1,(sizeof(struct aim_ssi_item )))));
/* Set the name */
  new -> name = g_strdup(name);
/* Set the group ID# and buddy ID# */
  new -> gid = gid;
  new -> bid = bid;
  if (type == 1) {
    if (((new -> gid) == 0xFFFF) && (name != 0)) {
      do {
        new -> gid += 1;
        exists = 0;
{
          for (cur =  *list; cur != ((struct aim_ssi_item *)((void *)0)); cur = (cur -> next)) 
            if (((cur -> type) == 1) && ((cur -> gid) == (new -> gid))) {
              exists = (!0);
              break; 
            }
        }
      }while (exists != 0);
    }
  }
  else if ((new -> gid) == 0) {
/*
		 * This is weird, but apparently items in the root group can't
		 * have a buddy ID equal to any group ID.  You'll get error
		 * 0x0003 when trying to add, which is "item already exists"
		 */
    if ((new -> bid) == 0xFFFF) {
      do {
        new -> bid += 1;
        exists = 0;
{
          for (cur =  *list; cur != ((struct aim_ssi_item *)((void *)0)); cur = (cur -> next)) 
            if (((cur -> bid) == (new -> bid)) || ((cur -> gid) == (new -> bid))) {
              exists = (!0);
              break; 
            }
        }
      }while (exists != 0);
    }
  }
  else {
    if ((new -> bid) == 0xFFFF) {
      do {
        new -> bid += 1;
        exists = 0;
{
          for (cur =  *list; cur != ((struct aim_ssi_item *)((void *)0)); cur = (cur -> next)) 
            if (((cur -> bid) == (new -> bid)) && ((cur -> gid) == (new -> gid))) {
              exists = (!0);
              break; 
            }
        }
      }while (exists != 0);
    }
  }
/* Set the type */
  new -> type = type;
/* Set the TLV list */
  new -> data = aim_tlvlist_copy(data);
/* Add the item to the list in the correct numerical position.  Fancy, eh? */
  if ( *list != 0) {
    if (((new -> gid) < ( *( *list)).gid) || (((new -> gid) == ( *( *list)).gid) && ((new -> bid) < ( *( *list)).bid))) {
      new -> next =  *list;
       *list = new;
    }
    else {
      struct aim_ssi_item *prev;
      for (((prev =  *list) , (cur = ( *( *list)).next)); (cur != 0) && (((new -> gid) > (cur -> gid)) || (((new -> gid) == (cur -> gid)) && ((new -> bid) > (cur -> bid)))); ((prev = cur) , (cur = (cur -> next)))) ;
      new -> next = (prev -> next);
      prev -> next = new;
    }
  }
  else {
    new -> next =  *list;
     *list = new;
  }
  return new;
}
/**
 * Locally delete an item from the given item list.
 *
 * @param list A pointer to a pointer to the current list of items.
 * @param del A pointer to the item you want to remove from the list.
 * @return Return 0 if no errors, otherwise return the error number.
 */

static int aim_ssi_itemlist_del(struct aim_ssi_item **list,struct aim_ssi_item *del)
{
  if (!( *list != 0) || !(del != 0)) 
    return -22;
/* Remove the item from the list */
  if ( *list == del) {
     *list = ( *( *list)).next;
  }
  else {
    struct aim_ssi_item *cur;
    for (cur =  *list; ((cur -> next) != 0) && ((cur -> next) != del); cur = (cur -> next)) ;
    if ((cur -> next) != 0) 
      cur -> next = (del -> next);
  }
/* Free the removed item */
  g_free((del -> name));
  aim_tlvlist_free((del -> data));
  g_free(del);
  return 0;
}
/**
 * Compare two items to see if they have the same data.
 *
 * @param cur1 A pointer to a pointer to the first item.
 * @param cur2 A pointer to a pointer to the second item.
 * @return Return 0 if no differences, or a number if there are differences.
 */

static int aim_ssi_itemlist_cmp(struct aim_ssi_item *cur1,struct aim_ssi_item *cur2)
{
  if (!(cur1 != 0) || !(cur2 != 0)) 
    return 1;
  if (((cur1 -> data) != 0) && !((cur2 -> data) != 0)) 
    return 2;
  if (!((cur1 -> data) != 0) && ((cur2 -> data) != 0)) 
    return 3;
  if ((((cur1 -> data) != 0) && ((cur2 -> data) != 0)) && (aim_tlvlist_cmp((cur1 -> data),(cur2 -> data)) != 0)) 
    return 4;
  if (((cur1 -> name) != 0) && !((cur2 -> name) != 0)) 
    return 5;
  if (!((cur1 -> name) != 0) && ((cur2 -> name) != 0)) 
    return 6;
  if ((((cur1 -> name) != 0) && ((cur2 -> name) != 0)) && (oscar_util_name_compare((cur1 -> name),(cur2 -> name)) != 0)) 
    return 7;
  if ((cur1 -> gid) != (cur2 -> gid)) 
    return 8;
  if ((cur1 -> bid) != (cur2 -> bid)) 
    return 9;
  if ((cur1 -> type) != (cur2 -> type)) 
    return 10;
  return 0;
}

static gboolean aim_ssi_itemlist_valid(struct aim_ssi_item *list,struct aim_ssi_item *item)
{
  struct aim_ssi_item *cur;
  for (cur = list; cur != 0; cur = (cur -> next)) 
    if (cur == item) 
      return (!0);
  return 0;
}
/**
 * Locally find an item given a group ID# and a buddy ID#.
 *
 * @param list A pointer to the current list of items.
 * @param gid The group ID# of the desired item.
 * @param bid The buddy ID# of the desired item.
 * @return Return a pointer to the item if found, else return NULL;
 */

struct aim_ssi_item *aim_ssi_itemlist_find(struct aim_ssi_item *list,guint16 gid,guint16 bid)
{
  struct aim_ssi_item *cur;
  for (cur = list; cur != 0; cur = (cur -> next)) 
    if (((cur -> gid) == gid) && ((cur -> bid) == bid)) 
      return cur;
  return 0;
}
/**
 * Locally find an item given a group name, buddy name, and type.  If group name
 * and buddy name are null, then just return the first item of the given type.
 *
 * @param list A pointer to the current list of items.
 * @param gn The group name of the desired item.
 * @param bn The buddy name of the desired item.
 * @param type The type of the desired item.
 * @return Return a pointer to the item if found, else return NULL.
 */

struct aim_ssi_item *aim_ssi_itemlist_finditem(struct aim_ssi_item *list,const char *gn,const char *bn,guint16 type)
{
  struct aim_ssi_item *cur;
  if (!(list != 0)) 
    return 0;
/* For finding buddies in groups */
  if ((gn != 0) && (bn != 0)) {
    for (cur = list; cur != 0; cur = (cur -> next)) 
      if ((((cur -> type) == type) && ((cur -> name) != 0)) && !(oscar_util_name_compare((cur -> name),bn) != 0)) {
        struct aim_ssi_item *curg;
        for (curg = list; curg != 0; curg = (curg -> next)) 
          if (((((curg -> type) == 1) && ((curg -> gid) == (cur -> gid))) && ((curg -> name) != 0)) && !(oscar_util_name_compare((curg -> name),gn) != 0)) 
            return cur;
      }
/* For finding groups */
  }
  else if (gn != 0) {
    for (cur = list; cur != 0; cur = (cur -> next)) {
      if (((((cur -> type) == type) && ((cur -> bid) == 0)) && ((cur -> name) != 0)) && !(oscar_util_name_compare((cur -> name),gn) != 0)) {
        return cur;
      }
    }
/* For finding permits, denies, and ignores */
  }
  else if (bn != 0) {
    for (cur = list; cur != 0; cur = (cur -> next)) {
      if ((((cur -> type) == type) && ((cur -> name) != 0)) && !(oscar_util_name_compare((cur -> name),bn) != 0)) {
        return cur;
      }
    }
/* For stuff without names--permit deny setting, visibility mask, etc. */
  }
  else 
    for (cur = list; cur != 0; cur = (cur -> next)) {
      if (((cur -> type) == type) && !((cur -> name) != 0)) 
        return cur;
    }
  return 0;
}
/**
 * Check if the given buddy exists in any group in the buddy list.
 *
 * @param list A pointer to the current list of items.
 * @param bn The group name of the desired item.
 * @return Return a pointer to the name of the item if found, else return NULL;
 */

struct aim_ssi_item *aim_ssi_itemlist_exists(struct aim_ssi_item *list,const char *bn)
{
  if (!(bn != 0)) 
    return 0;
  return aim_ssi_itemlist_finditem(list,0,bn,0);
}
/**
 * Locally find the parent item of the given buddy name.
 *
 * @param list A pointer to the current list of items.
 * @param bn The buddy name of the desired item.
 * @return Return a pointer to the name of the item if found, else return NULL;
 */

char *aim_ssi_itemlist_findparentname(struct aim_ssi_item *list,const char *bn)
{
  struct aim_ssi_item *cur;
  struct aim_ssi_item *curg;
  if (!(list != 0) || !(bn != 0)) 
    return 0;
  if (!((cur = aim_ssi_itemlist_exists(list,bn)) != 0)) 
    return 0;
  if (!((curg = aim_ssi_itemlist_find(list,(cur -> gid),0)) != 0)) 
    return 0;
  return curg -> name;
}
/**
 * Locally find the permit/deny setting item, and return the setting.
 *
 * @param list A pointer to the current list of items.
 * @return Return the current SSI permit deny setting, or 0 if no setting was found.
 */

int aim_ssi_getpermdeny(struct aim_ssi_item *list)
{
  struct aim_ssi_item *cur = aim_ssi_itemlist_finditem(list,0,0,4);
  if (cur != 0) {
    aim_tlv_t *tlv = aim_tlv_gettlv((cur -> data),0x00ca,1);
    if ((tlv != 0) && ((tlv -> value) != 0)) 
      return ( *(tlv -> value)) & 0xff;
  }
  return 0;
}
/**
 * Locally find the presence flag item, and return the setting.  The returned setting is a
 * bitmask of the preferences.  See the AIM_SSI_PRESENCE_FLAG_* #defines in oscar.h.
 *
 * @param list A pointer to the current list of items.
 * @return Return the current set of preferences.
 */

guint32 aim_ssi_getpresence(struct aim_ssi_item *list)
{
  struct aim_ssi_item *cur = aim_ssi_itemlist_finditem(list,0,0,5);
  if (cur != 0) {
    aim_tlv_t *tlv = aim_tlv_gettlv((cur -> data),0x00c9,1);
    if ((tlv != 0) && ((tlv -> length) != 0)) 
      return ((((( *(tlv -> value)) << 24) & 0xff000000) + (((tlv -> value)[1] << 16) & 0x00ff0000)) + (((tlv -> value)[2] << 8) & 0x0000ff00)) + ((tlv -> value)[3] & 0xff);
  }
  return 0xFFFFFFFF;
}
/**
 * Locally find the alias of the given buddy.
 *
 * @param list A pointer to the current list of items.
 * @param gn The group of the buddy.
 * @param bn The name of the buddy.
 * @return A pointer to a NULL terminated string that is the buddy's
 *         alias, or NULL if the buddy has no alias.  You should free
 *         this returned value!
 */

char *aim_ssi_getalias(struct aim_ssi_item *list,const char *gn,const char *bn)
{
  struct aim_ssi_item *cur = aim_ssi_itemlist_finditem(list,gn,bn,0);
  if (cur != 0) {
    aim_tlv_t *tlv = aim_tlv_gettlv((cur -> data),0x0131,1);
    if ((tlv != 0) && ((tlv -> length) != 0)) 
      return g_strndup(((const gchar *)(tlv -> value)),(tlv -> length));
  }
  return 0;
}
/**
 * Locally find the comment of the given buddy.
 *
 * @param list A pointer to the current list of items.
 * @param gn The group of the buddy.
 * @param bn The name of the buddy.
 * @return A pointer to a NULL terminated string that is the buddy's
 *         comment, or NULL if the buddy has no comment.  You should free
 *         this returned value!
 */

char *aim_ssi_getcomment(struct aim_ssi_item *list,const char *gn,const char *bn)
{
  struct aim_ssi_item *cur = aim_ssi_itemlist_finditem(list,gn,bn,0);
  if (cur != 0) {
    aim_tlv_t *tlv = aim_tlv_gettlv((cur -> data),0x013c,1);
    if ((tlv != 0) && ((tlv -> length) != 0)) {
      return g_strndup(((const gchar *)(tlv -> value)),(tlv -> length));
    }
  }
  return 0;
}
/**
 * Locally find if you are waiting for authorization for a buddy.
 *
 * @param list A pointer to the current list of items.
 * @param gn The group of the buddy.
 * @param bn The name of the buddy.
 * @return 1 if you are waiting for authorization; 0 if you are not
 */

gboolean aim_ssi_waitingforauth(struct aim_ssi_item *list,const char *gn,const char *bn)
{
  struct aim_ssi_item *cur = aim_ssi_itemlist_finditem(list,gn,bn,0);
  if (cur != 0) {
    if (aim_tlv_gettlv((cur -> data),0x0066,1) != 0) 
      return (!0);
  }
  return 0;
}
/**
 * If there are changes, then create temporary items and
 * call addmoddel.
 *
 * @param od The oscar session.
 * @return Return 0 if no errors, otherwise return the error number.
 */

static int aim_ssi_sync(OscarData *od)
{
  struct aim_ssi_item *cur1;
  struct aim_ssi_item *cur2;
  struct aim_ssi_tmp *cur;
  struct aim_ssi_tmp *new;
  int n = 0;
  GString *debugstr = g_string_new("");
/*
	 * The variable "n" is used to limit the number of addmoddel's that
	 * are performed in a single SNAC.  It will hopefully keep the size
	 * of the SNAC below the maximum SNAC size.
	 */
  if (!(od != 0)) 
    return -22;
/* If we're waiting for an ack, we shouldn't do anything else */
  if (od -> ssi.waiting_for_ack != 0) 
    return 0;
/*
	 * Compare the 2 lists and create an aim_ssi_tmp for each difference.
	 * We should only send either additions, modifications, or deletions
	 * before waiting for an acknowledgement.  So first do deletions, then
	 * additions, then modifications.  Also, both the official and the local
	 * list should be in ascending numerical order for the group ID#s and the
	 * buddy ID#s, which makes things more efficient.  I think.
	 */
/* Deletions */
  if (!(od -> ssi.pending != 0)) {
    for (cur1 = od -> ssi.official; (cur1 != 0) && (n < 15); cur1 = (cur1 -> next)) {
      if (!(aim_ssi_itemlist_find(od -> ssi.local,(cur1 -> gid),(cur1 -> bid)) != 0)) {
        n++;
        new = ((struct aim_ssi_tmp *)(g_malloc_n(1,(sizeof(struct aim_ssi_tmp )))));
        new -> action = 10;
        new -> ack = 0xFFFF;
        new -> name = ((char *)((void *)0));
        new -> item = cur1;
        new -> next = ((struct aim_ssi_tmp *)((void *)0));
        if (od -> ssi.pending != 0) {
          for (cur = od -> ssi.pending; (cur -> next) != 0; cur = (cur -> next)) ;
          cur -> next = new;
        }
        else 
          od -> ssi.pending = new;
        aim_ssi_item_debug_append(debugstr,"Deleting item ",cur1);
      }
    }
  }
/* Additions */
  if (!(od -> ssi.pending != 0)) {
    for (cur1 = od -> ssi.local; (cur1 != 0) && (n < 15); cur1 = (cur1 -> next)) {
      if (!(aim_ssi_itemlist_find(od -> ssi.official,(cur1 -> gid),(cur1 -> bid)) != 0)) {
        n++;
        new = ((struct aim_ssi_tmp *)(g_malloc_n(1,(sizeof(struct aim_ssi_tmp )))));
        new -> action = 8;
        new -> ack = 0xFFFF;
        new -> name = ((char *)((void *)0));
        new -> item = cur1;
        new -> next = ((struct aim_ssi_tmp *)((void *)0));
        if (od -> ssi.pending != 0) {
          for (cur = od -> ssi.pending; (cur -> next) != 0; cur = (cur -> next)) ;
          cur -> next = new;
        }
        else 
          od -> ssi.pending = new;
        aim_ssi_item_debug_append(debugstr,"Adding item ",cur1);
      }
    }
  }
/* Modifications */
  if (!(od -> ssi.pending != 0)) {
    for (cur1 = od -> ssi.local; (cur1 != 0) && (n < 15); cur1 = (cur1 -> next)) {
      cur2 = aim_ssi_itemlist_find(od -> ssi.official,(cur1 -> gid),(cur1 -> bid));
      if ((cur2 != 0) && (aim_ssi_itemlist_cmp(cur1,cur2) != 0)) {
        n++;
        new = ((struct aim_ssi_tmp *)(g_malloc_n(1,(sizeof(struct aim_ssi_tmp )))));
        new -> action = 9;
        new -> ack = 0xFFFF;
        new -> name = ((char *)((void *)0));
        new -> item = cur1;
        new -> next = ((struct aim_ssi_tmp *)((void *)0));
        if (od -> ssi.pending != 0) {
          for (cur = od -> ssi.pending; (cur -> next) != 0; cur = (cur -> next)) ;
          cur -> next = new;
        }
        else 
          od -> ssi.pending = new;
        aim_ssi_item_debug_append(debugstr,"Modifying item ",cur1);
      }
    }
  }
  if ((debugstr -> len) > 0) {
    purple_debug_info("oscar","%s",(debugstr -> str));
    if (purple_debug_is_verbose() != 0) {
      g_string_truncate(debugstr,0);
      for (cur1 = od -> ssi.local; cur1 != 0; cur1 = (cur1 -> next)) 
        aim_ssi_item_debug_append(debugstr,"\t",cur1);
      purple_debug_misc("oscar","Dumping item list of account %s:\n%s",( *purple_connection_get_account((od -> gc))).username,(debugstr -> str));
    }
  }
  g_string_free(debugstr,(!0));
/* We're out of stuff to do, so tell the AIM servers we're done and exit */
  if (!(od -> ssi.pending != 0)) {
    if (od -> ssi.in_transaction != 0) {
      aim_ssi_modend(od);
      od -> ssi.in_transaction = 0;
    }
    return 0;
  }
/* If this is the first in a series of add/mod/del
	 * requests then send the "begin transaction" message. */
  if (!(od -> ssi.in_transaction != 0)) {
    aim_ssi_modbegin(od);
    od -> ssi.in_transaction = (!0);
  }
/* Make sure we don't send anything else between now
	 * and when we receive the ack for the following operation */
  od -> ssi.waiting_for_ack = (!0);
/* Now go mail off our data and wait 4 to 6 weeks */
  return aim_ssi_addmoddel(od);;
}
/**
 * Free all SSI data.
 *
 * This doesn't remove it from the server, that's different.
 *
 * @param od The oscar odion.
 * @return Return 0 if no errors, otherwise return the error number.
 */

static void aim_ssi_freelist(OscarData *od)
{
  struct aim_ssi_item *cur;
  struct aim_ssi_item *del;
  struct aim_ssi_tmp *curtmp;
  struct aim_ssi_tmp *deltmp;
  cur = od -> ssi.official;
  while(cur != 0){
    del = cur;
    cur = (cur -> next);
    g_free((del -> name));
    aim_tlvlist_free((del -> data));
    g_free(del);
  }
  cur = od -> ssi.local;
  while(cur != 0){
    del = cur;
    cur = (cur -> next);
    g_free((del -> name));
    aim_tlvlist_free((del -> data));
    g_free(del);
  }
  curtmp = od -> ssi.pending;
  while(curtmp != 0){
    deltmp = curtmp;
    curtmp = (curtmp -> next);
    g_free(deltmp);
  }
  od -> ssi.numitems = 0;
  od -> ssi.official = ((struct aim_ssi_item *)((void *)0));
  od -> ssi.local = ((struct aim_ssi_item *)((void *)0));
  od -> ssi.pending = ((struct aim_ssi_tmp *)((void *)0));
  od -> ssi.timestamp = ((time_t )0);
}
/**
 * This "cleans" the ssi list.  It does the following:
 * 1) Makes sure all buddies, permits, and denies have names.
 * 2) Makes sure that all buddies are in a group that exist.
 * 3) Deletes any empty groups
 *
 * @param od The oscar odion.
 * @return Return 0 if no errors, otherwise return the error number.
 */

int aim_ssi_cleanlist(OscarData *od)
{
  struct aim_ssi_item *cur;
  struct aim_ssi_item *next;
  if (!(od != 0)) 
    return -22;
/* Delete any buddies, permits, or denies with empty names. */
/* If there are any buddies directly in the master group, add them to a real group. */
/* DESTROY any buddies that are directly in the master group. */
/* Do the same for buddies that are in a non-existant group. */
/* This will kind of mess up if you hit the item limit, but this function isn't too critical */
  cur = od -> ssi.local;
  while(cur != 0){
    next = (cur -> next);
    if (!((cur -> name) != 0)) {
      if ((cur -> type) == 0) 
        aim_ssi_delbuddy(od,0,0);
      else if ((((cur -> type) == 2) || ((cur -> type) == 3)) || ((cur -> type) == 14)) 
        aim_ssi_del_from_private_list(od,0,(cur -> type));
    }
    else if (((cur -> type) == 0) && (((cur -> gid) == 0) || !(aim_ssi_itemlist_find(od -> ssi.local,(cur -> gid),0) != 0))) {
      char *alias = aim_ssi_getalias(od -> ssi.local,0,(cur -> name));
      aim_ssi_addbuddy(od,(cur -> name),"orphans",0,alias,0,0,0);
      aim_ssi_delbuddy(od,(cur -> name),0);
      g_free(alias);
    }
    cur = next;
  }
/* Make sure there aren't any duplicate buddies in a group, or duplicate permits or denies */
  cur = od -> ssi.local;
  while(cur != 0){
    if ((((cur -> type) == 0) || ((cur -> type) == 2)) || ((cur -> type) == 3)) {
      struct aim_ssi_item *cur2;
      struct aim_ssi_item *next2;
      cur2 = (cur -> next);
      while(cur2 != 0){
        next2 = (cur2 -> next);
        if ((((((cur -> type) == (cur2 -> type)) && ((cur -> gid) == (cur2 -> gid))) && ((cur -> name) != ((char *)((void *)0)))) && ((cur2 -> name) != ((char *)((void *)0)))) && !(oscar_util_name_compare((cur -> name),(cur2 -> name)) != 0)) {
          aim_ssi_itemlist_del(&od -> ssi.local,cur2);
        }
        cur2 = next2;
      }
    }
    cur = (cur -> next);
  }
/* If we've made any changes then sync our list with the server's */
  return aim_ssi_sync(od);
}
/**
 * Add a buddy to the list.
 *
 * @param od The oscar odion.
 * @param name The name of the item.
 * @param group The group of the item.
 * @param data A TLV list to use as the additional data for this item.
 * @param alias The alias/nickname of the item, or NULL.
 * @param comment The buddy comment for the item, or NULL.
 * @param smsnum The locally assigned SMS number, or NULL.
 * @return Return 0 if no errors, otherwise return the error number.
 */

int aim_ssi_addbuddy(OscarData *od,const char *name,const char *group,GSList *data,const char *alias,const char *comment,const char *smsnum,gboolean needauth)
{
  struct aim_ssi_item *parent;
  if ((!(od != 0) || !(name != 0)) || !(group != 0)) 
    return -22;
/* Find the parent */
  if (!((parent = aim_ssi_itemlist_finditem(od -> ssi.local,group,0,1)) != 0)) {
/* Find the parent's parent (the master group) */
    if (aim_ssi_itemlist_find(od -> ssi.local,0,0) == ((struct aim_ssi_item *)((void *)0))) 
      aim_ssi_itemlist_add(&od -> ssi.local,0,0,0,1,0);
/* Add the parent */
    parent = aim_ssi_itemlist_add(&od -> ssi.local,group,0xFFFF,0,1,0);
/* Modify the parent's parent (the master group) */
    aim_ssi_itemlist_rebuildgroup(od -> ssi.local,0);
  }
/* Create a TLV list for the new buddy */
  if (needauth != 0) 
    aim_tlvlist_add_noval(&data,0x0066);
  if (alias != ((const char *)((void *)0))) 
    aim_tlvlist_add_str(&data,0x0131,alias);
  if (smsnum != ((const char *)((void *)0))) 
    aim_tlvlist_add_str(&data,0x013a,smsnum);
  if (comment != ((const char *)((void *)0))) 
    aim_tlvlist_add_str(&data,0x013c,comment);
/* Add that bad boy */
  aim_ssi_itemlist_add(&od -> ssi.local,name,(parent -> gid),0xFFFF,0,data);
  aim_tlvlist_free(data);
/* Modify the parent group */
  aim_ssi_itemlist_rebuildgroup(od -> ssi.local,group);
/* Sync our local list with the server list */
  return aim_ssi_sync(od);
}

int aim_ssi_add_to_private_list(OscarData *od,const char *name,guint16 list_type)
{
  if ((!(od != 0) || !(name != 0)) || !(od -> ssi.received_data != 0)) 
    return -22;
  if (aim_ssi_itemlist_find(od -> ssi.local,0,0) == ((struct aim_ssi_item *)((void *)0))) 
    aim_ssi_itemlist_add(&od -> ssi.local,0,0,0,1,0);
  aim_ssi_itemlist_add(&od -> ssi.local,name,0,0xFFFF,list_type,0);
  return aim_ssi_sync(od);
}

int aim_ssi_del_from_private_list(OscarData *od,const char *name,guint16 list_type)
{
  struct aim_ssi_item *del;
  if (!(od != 0)) 
    return -22;
  if (!((del = aim_ssi_itemlist_finditem(od -> ssi.local,0,name,list_type)) != 0)) 
    return -22;
  aim_ssi_itemlist_del(&od -> ssi.local,del);
  return aim_ssi_sync(od);
}
/**
 * Deletes a buddy from the list.
 *
 * @param od The oscar odion.
 * @param name The name of the item, or NULL.
 * @param group The group of the item, or NULL.
 * @return Return 0 if no errors, otherwise return the error number.
 */

int aim_ssi_delbuddy(OscarData *od,const char *name,const char *group)
{
  struct aim_ssi_item *del;
  if (!(od != 0)) 
    return -22;
/* Find the buddy */
  if (!((del = aim_ssi_itemlist_finditem(od -> ssi.local,group,name,0)) != 0)) 
    return -22;
/* Remove the item from the list */
  aim_ssi_itemlist_del(&od -> ssi.local,del);
/* Modify the parent group */
  aim_ssi_itemlist_rebuildgroup(od -> ssi.local,group);
/* Sync our local list with the server list */
  return aim_ssi_sync(od);
}
/**
 * Deletes a group from the list.
 *
 * @param od The oscar odion.
 * @param group The name of the group.
 * @return Return 0 if no errors, otherwise return the error number.
 */

int aim_ssi_delgroup(OscarData *od,const char *group)
{
  struct aim_ssi_item *del;
  aim_tlv_t *tlv;
  if (!(od != 0)) 
    return -22;
/* Find the group */
  if (!((del = aim_ssi_itemlist_finditem(od -> ssi.local,group,0,1)) != 0)) 
    return -22;
/* Don't delete the group if it's not empty */
  tlv = aim_tlv_gettlv((del -> data),0x00c8,1);
  if ((tlv != 0) && ((tlv -> length) > 0)) 
    return -22;
/* Remove the item from the list */
  aim_ssi_itemlist_del(&od -> ssi.local,del);
/* Modify the parent group */
  aim_ssi_itemlist_rebuildgroup(od -> ssi.local,group);
/* Sync our local list with the server list */
  return aim_ssi_sync(od);
}
/**
 * Move a buddy from one group to another group.  This basically just deletes the
 * buddy and re-adds it.
 *
 * @param od The oscar odion.
 * @param oldgn The group that the buddy is currently in.
 * @param newgn The group that the buddy should be moved in to.
 * @param bn The name of the buddy to be moved.
 * @return Return 0 if no errors, otherwise return the error number.
 */

int aim_ssi_movebuddy(OscarData *od,const char *oldgn,const char *newgn,const char *bn)
{
  struct aim_ssi_item *buddy;
  GSList *data;
/* Find the buddy */
  buddy = aim_ssi_itemlist_finditem(od -> ssi.local,oldgn,bn,0);
  if (buddy == ((struct aim_ssi_item *)((void *)0))) 
    return -22;
/* Make a copy of the buddy's TLV list */
  data = aim_tlvlist_copy((buddy -> data));
/* Delete the old item */
  aim_ssi_delbuddy(od,bn,oldgn);
/* Add the new item using the EXACT SAME TLV list */
  aim_ssi_addbuddy(od,bn,newgn,data,0,0,0,0);
  return 0;
}
/**
 * Change the alias stored on the server for a given buddy.
 *
 * @param od The oscar odion.
 * @param gn The group that the buddy is currently in.
 * @param bn The name of the buddy.
 * @param alias The new alias for the buddy, or NULL if you want to remove
 *        a buddy's comment.
 * @return Return 0 if no errors, otherwise return the error number.
 */

int aim_ssi_aliasbuddy(OscarData *od,const char *gn,const char *bn,const char *alias)
{
  struct aim_ssi_item *tmp;
  if ((!(od != 0) || !(gn != 0)) || !(bn != 0)) 
    return -22;
  if (!((tmp = aim_ssi_itemlist_finditem(od -> ssi.local,gn,bn,0)) != 0)) 
    return -22;
/* Either add or remove the 0x0131 TLV from the TLV chain */
  if ((alias != ((const char *)((void *)0))) && (strlen(alias) > 0)) 
    aim_tlvlist_replace_str(&tmp -> data,0x0131,alias);
  else 
    aim_tlvlist_remove(&tmp -> data,0x0131);
/* Sync our local list with the server list */
  return aim_ssi_sync(od);
}
/**
 * Change the comment stored on the server for a given buddy.
 *
 * @param od The oscar odion.
 * @param gn The group that the buddy is currently in.
 * @param bn The name of the buddy.
 * @param alias The new comment for the buddy, or NULL if you want to remove
 *        a buddy's comment.
 * @return Return 0 if no errors, otherwise return the error number.
 */

int aim_ssi_editcomment(OscarData *od,const char *gn,const char *bn,const char *comment)
{
  struct aim_ssi_item *tmp;
  if ((!(od != 0) || !(gn != 0)) || !(bn != 0)) 
    return -22;
  if (!((tmp = aim_ssi_itemlist_finditem(od -> ssi.local,gn,bn,0)) != 0)) 
    return -22;
/* Either add or remove the 0x0131 TLV from the TLV chain */
  if ((comment != ((const char *)((void *)0))) && (strlen(comment) > 0)) 
    aim_tlvlist_replace_str(&tmp -> data,0x013c,comment);
  else 
    aim_tlvlist_remove(&tmp -> data,0x013c);
/* Sync our local list with the server list */
  return aim_ssi_sync(od);
}
/**
 * Rename a group.
 *
 * @param od The oscar odion.
 * @param oldgn The old group name.
 * @param newgn The new group name.
 * @return Return 0 if no errors, otherwise return the error number.
 */

int aim_ssi_rename_group(OscarData *od,const char *oldgn,const char *newgn)
{
  struct aim_ssi_item *group;
  if ((!(od != 0) || !(oldgn != 0)) || !(newgn != 0)) 
    return -22;
  if (!((group = aim_ssi_itemlist_finditem(od -> ssi.local,oldgn,0,1)) != 0)) 
    return -22;
  g_free((group -> name));
  group -> name = g_strdup(newgn);
/* Sync our local list with the server list */
  return aim_ssi_sync(od);
}
/**
 * Stores your permit/deny setting on the server, and starts using it.
 *
 * @param od The oscar odion.
 * @param permdeny Your permit/deny setting. For ICQ accounts, it actually affects your visibility
 *        and has nothing to do with blocking. Can be one of the following:
 *        1 - Allow all users
 *        2 - Block all users
 *        3 - Allow only the users below
 *        4 - Block only the users below
 *        5 - Allow only users on my buddy list
 * @return Return 0 if no errors, otherwise return the error number.
 */

int aim_ssi_setpermdeny(OscarData *od,guint8 permdeny)
{
  struct aim_ssi_item *tmp;
  if (!(od != 0) || !(od -> ssi.received_data != 0)) 
    return -22;
/* Find the PDINFO item, or add it if it does not exist */
  if (!((tmp = aim_ssi_itemlist_finditem(od -> ssi.local,0,0,4)) != 0)) {
/* Make sure the master group exists */
    if (aim_ssi_itemlist_find(od -> ssi.local,0,0) == ((struct aim_ssi_item *)((void *)0))) 
      aim_ssi_itemlist_add(&od -> ssi.local,0,0,0,1,0);
    tmp = aim_ssi_itemlist_add(&od -> ssi.local,0,0,0xFFFF,4,0);
  }
/* Need to add the 0x00ca TLV to the TLV chain */
  aim_tlvlist_replace_8(&tmp -> data,0x00ca,permdeny);
/* Sync our local list with the server list */
  return aim_ssi_sync(od);
}
/**
 * Set buddy icon information
 *
 * @param od The oscar odion.
 * @param iconcsum The MD5 checksum of the icon you are using.
 * @param iconcsumlen Length of the MD5 checksum given above.  Should be 0x10 bytes.
 * @return Return 0 if no errors, otherwise return the error number.
 */

int aim_ssi_seticon(OscarData *od,const guint8 *iconsum,guint8 iconsumlen)
{
  struct aim_ssi_item *tmp;
  guint8 *csumdata;
  if (((!(od != 0) || !(iconsum != 0)) || !(iconsumlen != 0)) || !(od -> ssi.received_data != 0)) 
    return -22;
/* Find the ICONINFO item, or add it if it does not exist */
  if (!((tmp = aim_ssi_itemlist_finditem(od -> ssi.local,0,"1",20)) != 0)) {
/* Make sure the master group exists */
    if (aim_ssi_itemlist_find(od -> ssi.local,0,0) == ((struct aim_ssi_item *)((void *)0))) 
      aim_ssi_itemlist_add(&od -> ssi.local,0,0,0,1,0);
    tmp = aim_ssi_itemlist_add(&od -> ssi.local,"1",0,0xFFFF,20,0);
  }
/* Need to add the 0x00d5 TLV to the TLV chain */
  csumdata = ((guint8 *)(g_malloc(((iconsumlen + 2) * sizeof(guint8 )))));
  ((csumdata[0] = (((guint8 )0) & 0xff)) , 1);
  ((csumdata[1] = (((guint8 )iconsumlen) & 0xff)) , 1);
  memcpy((csumdata + 2),iconsum,iconsumlen);
  aim_tlvlist_replace_raw(&tmp -> data,0x00d5,((iconsumlen + 2) * sizeof(guint8 )),csumdata);
  g_free(csumdata);
/* Need to add the 0x0131 TLV to the TLV chain, used to cache the icon */
  aim_tlvlist_replace_noval(&tmp -> data,0x0131);
/* Sync our local list with the server list */
  aim_ssi_sync(od);
  return 0;
}
/**
 * Remove a reference to a server stored buddy icon.  This will make your
 * icon stop showing up to other people.
 *
 * Really this function just sets the icon to a dummy value.  It's weird...
 * but I think the dummy value basically means "I don't have an icon!"
 *
 * @param od The oscar session.
 * @return Return 0 if no errors, otherwise return the error number.
 */

int aim_ssi_delicon(OscarData *od)
{
  const guint8 csumdata[] = {(2), (1), (0xd2), (4), (0x72)};
  return aim_ssi_seticon(od,csumdata,5);
}
/**
 * Stores your setting for various SSI settings.  Whether you
 * should show up as idle or not, etc.
 *
 * @param od The oscar odion.
 * @param presence A bitmask of the first 32 entries [0-31] from
 *        http://dev.aol.com/aim/oscar/#FEEDBAG__BUDDY_PREFS
 *        0x00000002 - Hide "eBuddy group" (whatever that is)
 *        0x00000400 - Allow others to see your idle time
 *        0x00020000 - Don't show Recent Buddies
 * @return Return 0 if no errors, otherwise return the error number.
 */

int aim_ssi_setpresence(OscarData *od,guint32 presence)
{
  struct aim_ssi_item *tmp;
  if (!(od != 0) || !(od -> ssi.received_data != 0)) 
    return -22;
/* Find the PRESENCEPREFS item, or add it if it does not exist */
  if (!((tmp = aim_ssi_itemlist_finditem(od -> ssi.local,0,0,5)) != 0)) {
/* Make sure the master group exists */
    if (aim_ssi_itemlist_find(od -> ssi.local,0,0) == ((struct aim_ssi_item *)((void *)0))) 
      aim_ssi_itemlist_add(&od -> ssi.local,0,0,0,1,0);
    tmp = aim_ssi_itemlist_add(&od -> ssi.local,0,0,0xFFFF,5,0);
  }
/* Need to add the x00c9 TLV to the TLV chain */
  aim_tlvlist_replace_32(&tmp -> data,0x00c9,presence);
/* Sync our local list with the server list */
  return aim_ssi_sync(od);
}
/*
 * Subtype 0x0002 - Request SSI Rights.
 */

int aim_ssi_reqrights(OscarData *od)
{
  FlapConnection *conn;
  if (!(od != 0) || !((conn = flap_connection_findbygroup(od,19)) != 0)) 
    return -22;
  aim_genericreq_n_snacid(od,conn,19,2);
  return 0;
}
/*
 * Subtype 0x0003 - SSI Rights Information.
 */

static int parserights(OscarData *od,FlapConnection *conn,aim_module_t *mod,FlapFrame *frame,aim_modsnac_t *snac,ByteStream *bs)
{
  int ret = 0;
  int i;
  aim_rxcallback_t userfunc;
  GSList *tlvlist;
  aim_tlv_t *tlv;
  ByteStream bstream;
  guint16 *maxitems;
/* This SNAC is made up of a bunch of TLVs */
  tlvlist = aim_tlvlist_read(bs);
/* TLV 0x0004 contains the maximum number of each item */
  if (!((tlv = aim_tlv_gettlv(tlvlist,4,1)) != 0)) {
    aim_tlvlist_free(tlvlist);
    return 0;
  }
  byte_stream_init(&bstream,(tlv -> value),(tlv -> length));
  maxitems = ((guint16 *)(g_malloc((((tlv -> length) / 2) * sizeof(guint16 )))));
  for (i = 0; i < ((tlv -> length) / 2); i++) 
    maxitems[i] = byte_stream_get16(&bstream);
  if ((userfunc = aim_callhandler(od,(snac -> family),(snac -> subtype))) != 0) 
    ret = ( *userfunc)(od,conn,frame,((tlv -> length) / 2),maxitems);
  aim_tlvlist_free(tlvlist);
  g_free(maxitems);
  return ret;
}
/*
 * Subtype 0x0004 - Request SSI Data when you don't have a timestamp and
 * revision number.
 *
 */

int aim_ssi_reqdata(OscarData *od)
{
  FlapConnection *conn;
  if (!(od != 0) || !((conn = flap_connection_findbygroup(od,19)) != 0)) 
    return -22;
/* Free any current data, just in case */
  aim_ssi_freelist(od);
  aim_genericreq_n_snacid(od,conn,19,4);
  return 0;
}
/*
 * Subtype 0x0006 - SSI Data.
 */

static int parsedata(OscarData *od,FlapConnection *conn,aim_module_t *mod,FlapFrame *frame,aim_modsnac_t *snac,ByteStream *bs)
{
  int ret = 0;
  aim_rxcallback_t userfunc;
/* guess */
  guint8 fmtver;
  guint16 namelen;
  guint16 gid;
  guint16 bid;
  guint16 type;
  char *name;
  GSList *data;
  GString *debugstr = g_string_new("");
/* Version of ssi data.  Should be 0x00 */
  fmtver = byte_stream_get8(bs);
/* # of items in this SSI SNAC */
  od -> ssi.numitems += (byte_stream_get16(bs));
/* Read in the list */
/* last four bytes are timestamp */
  while(byte_stream_bytes_left(bs) > 4){
    if ((namelen = byte_stream_get16(bs)) != 0) 
      name = byte_stream_getstr(bs,namelen);
    else 
      name = ((char *)((void *)0));
    gid = byte_stream_get16(bs);
    bid = byte_stream_get16(bs);
    type = byte_stream_get16(bs);
    data = aim_tlvlist_readlen(bs,byte_stream_get16(bs));
    aim_ssi_item_debug_append(debugstr,"\t",aim_ssi_itemlist_add(&od -> ssi.official,name,gid,bid,type,data));
    g_free(name);
    aim_tlvlist_free(data);
  }
  purple_debug_misc("oscar","Reading items from tlvlist for account %s:\n%s",( *purple_connection_get_account((od -> gc))).username,(debugstr -> str));
  g_string_free(debugstr,(!0));
/* Read in the timestamp */
  od -> ssi.timestamp = (byte_stream_get32(bs));
  if (!(((snac -> flags) & 1) != 0)) {
/* Make a copy of the list */
    struct aim_ssi_item *cur;
    for (cur = od -> ssi.official; cur != 0; cur = (cur -> next)) 
      aim_ssi_itemlist_add(&od -> ssi.local,(cur -> name),(cur -> gid),(cur -> bid),(cur -> type),(cur -> data));
    od -> ssi.received_data = (!0);
    if ((userfunc = aim_callhandler(od,(snac -> family),(snac -> subtype))) != 0) 
      ret = ( *userfunc)(od,conn,frame,fmtver,od -> ssi.numitems,od -> ssi.timestamp);
  }
  return ret;
}
/*
 * Subtype 0x0007 - SSI Activate Data.
 *
 * Should be sent after receiving 13/6 or 13/f to tell the server you
 * are ready to begin using the list.  It will promptly give you the
 * presence information for everyone in your list and put your permit/deny
 * settings into effect.
 *
 */

int aim_ssi_enable(OscarData *od)
{
  FlapConnection *conn;
  if (!(od != 0) || !((conn = flap_connection_findbygroup(od,19)) != 0)) 
    return -22;
  aim_genericreq_n(od,conn,19,7);
  return 0;
}
/*
 * Subtype 0x0008/0x0009/0x000a - SSI Add/Mod/Del Item(s).
 *
 * Sends the SNAC to add, modify, or delete items from the server-stored
 * information.  These 3 SNACs all have an identical structure.  The only
 * difference is the subtype that is set for the SNAC.
 *
 */

static int aim_ssi_addmoddel(OscarData *od)
{
  FlapConnection *conn;
  ByteStream bs;
  aim_snacid_t snacid;
  int bslen;
  struct aim_ssi_tmp *cur;
  if (((!(od != 0) || !((conn = flap_connection_findbygroup(od,19)) != 0)) || !(od -> ssi.pending != 0)) || !(( *od -> ssi.pending).item != 0)) 
    return -22;
/* Calculate total SNAC size */
  bslen = 0;
  for (cur = od -> ssi.pending; cur != 0; cur = (cur -> next)) {
/* For length, GID, BID, type, and length */
    bslen += 10;
    if (( *(cur -> item)).name != 0) 
      bslen += strlen(( *(cur -> item)).name);
    if (( *(cur -> item)).data != 0) 
      bslen += aim_tlvlist_size(( *(cur -> item)).data);
  }
  byte_stream_new(&bs,bslen);
  for (cur = od -> ssi.pending; cur != 0; cur = (cur -> next)) {
    byte_stream_put16(&bs,((( *(cur -> item)).name != 0)?strlen(( *(cur -> item)).name) : 0));
    if (( *(cur -> item)).name != 0) 
      byte_stream_putstr(&bs,( *(cur -> item)).name);
    byte_stream_put16(&bs,( *(cur -> item)).gid);
    byte_stream_put16(&bs,( *(cur -> item)).bid);
    byte_stream_put16(&bs,( *(cur -> item)).type);
    byte_stream_put16(&bs,((( *(cur -> item)).data != 0)?aim_tlvlist_size(( *(cur -> item)).data) : 0));
    if (( *(cur -> item)).data != 0) 
      aim_tlvlist_write(&bs,&( *(cur -> item)).data);
  }
  snacid = aim_cachesnac(od,19,( *od -> ssi.pending).action,0,0,0);
  flap_connection_send_snac(od,conn,19,( *od -> ssi.pending).action,snacid,&bs);
  byte_stream_destroy(&bs);
  return 0;
}
/*
 * Subtype 0x0008 - Incoming SSI add.
 *
 * Sent by the server, for example, when someone is added to
 * your "Recent Buddies" group.
 */

static int parseadd(OscarData *od,FlapConnection *conn,aim_module_t *mod,FlapFrame *frame,aim_modsnac_t *snac,ByteStream *bs)
{
  int ret = 0;
  aim_rxcallback_t userfunc;
  char *name;
  guint16 len;
  guint16 gid;
  guint16 bid;
  guint16 type;
  GSList *data;
  while(byte_stream_bytes_left(bs) != 0){
    if ((len = byte_stream_get16(bs)) != 0) 
      name = byte_stream_getstr(bs,len);
    else 
      name = ((char *)((void *)0));
    gid = byte_stream_get16(bs);
    bid = byte_stream_get16(bs);
    type = byte_stream_get16(bs);
    if ((len = byte_stream_get16(bs)) != 0) 
      data = aim_tlvlist_readlen(bs,len);
    else 
      data = ((GSList *)((void *)0));
    aim_ssi_itemlist_add(&od -> ssi.local,name,gid,bid,type,data);
    aim_ssi_itemlist_add(&od -> ssi.official,name,gid,bid,type,data);
    aim_tlvlist_free(data);
    if ((userfunc = aim_callhandler(od,(snac -> family),(snac -> subtype))) != 0) 
      ret = ( *userfunc)(od,conn,frame,(snac -> subtype),type,name);
    g_free(name);
  }
  return ret;
}
/*
 * Subtype 0x0009 - Incoming SSI mod.
 */

static int parsemod(OscarData *od,FlapConnection *conn,aim_module_t *mod,FlapFrame *frame,aim_modsnac_t *snac,ByteStream *bs)
{
  int ret = 0;
  aim_rxcallback_t userfunc;
  char *name;
  guint16 len;
  guint16 gid;
  guint16 bid;
  guint16 type;
  GSList *data;
  struct aim_ssi_item *item;
  while(byte_stream_bytes_left(bs) != 0){
    if ((len = byte_stream_get16(bs)) != 0) 
      name = byte_stream_getstr(bs,len);
    else 
      name = ((char *)((void *)0));
    gid = byte_stream_get16(bs);
    bid = byte_stream_get16(bs);
    type = byte_stream_get16(bs);
    if ((len = byte_stream_get16(bs)) != 0) 
      data = aim_tlvlist_readlen(bs,len);
    else 
      data = ((GSList *)((void *)0));
/* Replace the 2 local items with the given one */
    if ((item = aim_ssi_itemlist_find(od -> ssi.local,gid,bid)) != 0) {
      item -> type = type;
      g_free((item -> name));
      item -> name = g_strdup(name);
      aim_tlvlist_free((item -> data));
      item -> data = aim_tlvlist_copy(data);
    }
    if ((item = aim_ssi_itemlist_find(od -> ssi.official,gid,bid)) != 0) {
      item -> type = type;
      g_free((item -> name));
      item -> name = g_strdup(name);
      aim_tlvlist_free((item -> data));
      item -> data = aim_tlvlist_copy(data);
    }
    if ((userfunc = aim_callhandler(od,(snac -> family),(snac -> subtype))) != 0) 
      ret = ( *userfunc)(od,conn,frame,(snac -> subtype),type,name);
    g_free(name);
    aim_tlvlist_free(data);
  }
  return ret;
}
/*
 * Subtype 0x000a - Incoming SSI del.
 *
 * XXX - It would probably be good for the client to actually do something when it gets this.
 */

static int parsedel(OscarData *od,FlapConnection *conn,aim_module_t *mod,FlapFrame *frame,aim_modsnac_t *snac,ByteStream *bs)
{
  int ret = 0;
  aim_rxcallback_t userfunc;
  guint16 gid;
  guint16 bid;
  struct aim_ssi_item *del;
  while(byte_stream_bytes_left(bs) != 0){
    byte_stream_advance(bs,(byte_stream_get16(bs)));
    gid = byte_stream_get16(bs);
    bid = byte_stream_get16(bs);
    byte_stream_get16(bs);
    byte_stream_advance(bs,(byte_stream_get16(bs)));
    if ((del = aim_ssi_itemlist_find(od -> ssi.local,gid,bid)) != 0) 
      aim_ssi_itemlist_del(&od -> ssi.local,del);
    if ((del = aim_ssi_itemlist_find(od -> ssi.official,gid,bid)) != 0) 
      aim_ssi_itemlist_del(&od -> ssi.official,del);
    if ((userfunc = aim_callhandler(od,(snac -> family),(snac -> subtype))) != 0) 
      ret = ( *userfunc)(od,conn,frame);
  }
  return ret;
}
/*
 * Subtype 0x000e - SSI Add/Mod/Del Ack.
 *
 * Response to add, modify, or delete SNAC (sent with aim_ssi_addmoddel).
 *
 */

static int parseack(OscarData *od,FlapConnection *conn,aim_module_t *mod,FlapFrame *frame,aim_modsnac_t *snac,ByteStream *bs)
{
  int ret = 0;
  aim_rxcallback_t userfunc;
  struct aim_ssi_tmp *cur;
  struct aim_ssi_tmp *del;
/* Read in the success/failure flags from the ack SNAC */
  cur = od -> ssi.pending;
  while((cur != 0) && (byte_stream_bytes_left(bs) > 0)){
    cur -> ack = byte_stream_get16(bs);
    cur = (cur -> next);
  }
/*
	 * If outcome is 0, then add the item to the item list, or replace the other item,
	 * or remove the old item.  If outcome is non-zero, then remove the item from the
	 * local list, or unmodify it, or add it.
	 */
  for (cur = od -> ssi.pending; (cur != 0) && ((cur -> ack) != 0xFFFF); cur = (cur -> next)) {
    if ((cur -> item) != 0) {
      if ((cur -> ack) != 0) {
/* Our action was unsuccessful, so change the local list back to how it was */
        if ((cur -> action) == 8) {
/* Remove the item from the local list */
/* Make sure cur->item is still valid memory */
          if (aim_ssi_itemlist_valid(od -> ssi.local,(cur -> item)) != 0) {
            cur -> name = g_strdup(( *(cur -> item)).name);
            aim_ssi_itemlist_del(&od -> ssi.local,(cur -> item));
          }
          cur -> item = ((struct aim_ssi_item *)((void *)0));
        }
        else if ((cur -> action) == 9) {
/* Replace the local item with the item from the official list */
          if (aim_ssi_itemlist_valid(od -> ssi.local,(cur -> item)) != 0) {
            struct aim_ssi_item *cur1;
            if ((cur1 = aim_ssi_itemlist_find(od -> ssi.official,( *(cur -> item)).gid,( *(cur -> item)).bid)) != 0) {
              g_free(( *(cur -> item)).name);
              ( *(cur -> item)).name = g_strdup((cur1 -> name));
              aim_tlvlist_free(( *(cur -> item)).data);
              ( *(cur -> item)).data = aim_tlvlist_copy((cur1 -> data));
            }
          }
          else 
            cur -> item = ((struct aim_ssi_item *)((void *)0));
        }
        else if ((cur -> action) == 10) {
/* Add the item back into the local list */
          if (aim_ssi_itemlist_valid(od -> ssi.official,(cur -> item)) != 0) {
            aim_ssi_itemlist_add(&od -> ssi.local,( *(cur -> item)).name,( *(cur -> item)).gid,( *(cur -> item)).bid,( *(cur -> item)).type,( *(cur -> item)).data);
          }
          else 
            cur -> item = ((struct aim_ssi_item *)((void *)0));
        }
      }
      else {
/* Do the exact opposite */
        if ((cur -> action) == 8) {
/* Add the local item to the official list */
          if (aim_ssi_itemlist_valid(od -> ssi.local,(cur -> item)) != 0) {
            aim_ssi_itemlist_add(&od -> ssi.official,( *(cur -> item)).name,( *(cur -> item)).gid,( *(cur -> item)).bid,( *(cur -> item)).type,( *(cur -> item)).data);
          }
          else 
            cur -> item = ((struct aim_ssi_item *)((void *)0));
        }
        else if ((cur -> action) == 9) {
/* Replace the official item with the item from the local list */
          if (aim_ssi_itemlist_valid(od -> ssi.local,(cur -> item)) != 0) {
            struct aim_ssi_item *cur1;
            if ((cur1 = aim_ssi_itemlist_find(od -> ssi.official,( *(cur -> item)).gid,( *(cur -> item)).bid)) != 0) {
              g_free((cur1 -> name));
              cur1 -> name = g_strdup(( *(cur -> item)).name);
              aim_tlvlist_free((cur1 -> data));
              cur1 -> data = aim_tlvlist_copy(( *(cur -> item)).data);
            }
          }
          else 
            cur -> item = ((struct aim_ssi_item *)((void *)0));
        }
        else if ((cur -> action) == 10) {
/* Remove the item from the official list */
          if (aim_ssi_itemlist_valid(od -> ssi.official,(cur -> item)) != 0) 
            aim_ssi_itemlist_del(&od -> ssi.official,(cur -> item));
          cur -> item = ((struct aim_ssi_item *)((void *)0));
        }
      }
/* End if (cur->item) */
    }
/* End for loop */
  }
  if ((userfunc = aim_callhandler(od,(snac -> family),(snac -> subtype))) != 0) 
    ret = ( *userfunc)(od,conn,frame,od -> ssi.pending);
/* Free all aim_ssi_tmp's with an outcome */
  cur = od -> ssi.pending;
  while((cur != 0) && ((cur -> ack) != 0xFFFF)){
    del = cur;
    cur = (cur -> next);
    g_free((del -> name));
    g_free(del);
  }
  od -> ssi.pending = cur;
/* If we're not waiting for any more acks, then send more SNACs */
  if (!(od -> ssi.pending != 0)) {
    od -> ssi.waiting_for_ack = 0;
    aim_ssi_sync(od);
  }
  return ret;
}
/*
 * Subtype 0x000f - SSI Data Unchanged.
 *
 * Response to aim_ssi_reqifchanged() if the server-side data is not newer than
 * posted local stamp/revision.
 *
 */

static int parsedataunchanged(OscarData *od,FlapConnection *conn,aim_module_t *mod,FlapFrame *frame,aim_modsnac_t *snac,ByteStream *bs)
{
  int ret = 0;
  aim_rxcallback_t userfunc;
  od -> ssi.received_data = (!0);
  if ((userfunc = aim_callhandler(od,(snac -> family),(snac -> subtype))) != 0) 
    ret = ( *userfunc)(od,conn,frame);
  return ret;
}
/*
 * Subtype 0x0011 - SSI Begin Data Modification.
 *
 * Tell the server you're going to start modifying data.  This marks
 * the beginning of a transaction.
 */

int aim_ssi_modbegin(OscarData *od)
{
  FlapConnection *conn;
  if (!(od != 0) || !((conn = flap_connection_findbygroup(od,19)) != 0)) 
    return -22;
  aim_genericreq_n(od,conn,19,0x0011);
  return 0;
}
/*
 * Subtype 0x0012 - SSI End Data Modification.
 *
 * Tell the server you're finished modifying data.  The marks the end
 * of a transaction.
 */

int aim_ssi_modend(OscarData *od)
{
  FlapConnection *conn;
  if (!(od != 0) || !((conn = flap_connection_findbygroup(od,19)) != 0)) 
    return -22;
  aim_genericreq_n(od,conn,19,18);
  return 0;
}
/*
 * Subtype 0x0015 - Receive an authorization grant
 */

static int receiveauthgrant(OscarData *od,FlapConnection *conn,aim_module_t *mod,FlapFrame *frame,aim_modsnac_t *snac,ByteStream *bs)
{
  int ret = 0;
  aim_rxcallback_t userfunc;
  guint16 tmp;
  char *bn;
  char *msg;
  char *tmpstr;
/* Read buddy name */
  tmp = (byte_stream_get8(bs));
  if (!(tmp != 0)) {
    purple_debug_warning("oscar","Dropping auth grant SNAC because username was empty\n");
    return 0;
  }
  bn = byte_stream_getstr(bs,tmp);
  if (!(g_utf8_validate(bn,(-1),0) != 0)) {
    purple_debug_warning("oscar","Dropping auth grant SNAC because the username was not valid UTF-8\n");
    g_free(bn);
  }
/* Read message */
  tmp = byte_stream_get16(bs);
  if (tmp != 0) {
    msg = byte_stream_getstr(bs,tmp);
    if (!(g_utf8_validate(msg,(-1),0) != 0)) {
/* Ugh, msg isn't UTF8.  Let's salvage. */
      purple_debug_warning("oscar","Got non-UTF8 message in auth grant from %s\n",bn);
      tmpstr = purple_utf8_salvage(msg);
      g_free(msg);
      msg = tmpstr;
    }
  }
  else 
    msg = ((char *)((void *)0));
/* Unknown */
  tmp = byte_stream_get16(bs);
  if ((userfunc = aim_callhandler(od,(snac -> family),(snac -> subtype))) != 0) 
    ret = ( *userfunc)(od,conn,frame,bn,msg);
  g_free(bn);
  g_free(msg);
  return ret;
}
/*
 * Subtype 0x0018 - Send authorization request
 *
 * Sends a request for authorization to the given contact.  The request will either be
 * granted, denied, or dropped.
 *
 */

int aim_ssi_sendauthrequest(OscarData *od,const char *bn,const char *msg)
{
  FlapConnection *conn;
  ByteStream bs;
  aim_snacid_t snacid;
  if ((!(od != 0) || !((conn = flap_connection_findbygroup(od,19)) != 0)) || !(bn != 0)) 
    return -22;
  byte_stream_new(&bs,((((1 + strlen(bn)) + 2) + (((msg != 0)?(strlen(msg) + 1) : 0))) + 2));
/* Username */
  byte_stream_put8(&bs,(strlen(bn)));
  byte_stream_putstr(&bs,bn);
/* Message (null terminated) */
  byte_stream_put16(&bs,((msg != 0)?strlen(msg) : 0));
  if (msg != 0) {
    byte_stream_putstr(&bs,msg);
    byte_stream_put8(&bs,0);
  }
/* Unknown */
  byte_stream_put16(&bs,0);
  snacid = aim_cachesnac(od,19,24,0,0,0);
  flap_connection_send_snac(od,conn,19,24,snacid,&bs);
  byte_stream_destroy(&bs);
  return 0;
}
/*
 * Subtype 0x0019 - Receive an authorization request
 */

static int receiveauthrequest(OscarData *od,FlapConnection *conn,aim_module_t *mod,FlapFrame *frame,aim_modsnac_t *snac,ByteStream *bs)
{
  int ret = 0;
  aim_rxcallback_t userfunc;
  guint16 tmp;
  char *bn;
  char *msg;
  char *tmpstr;
/* Read buddy name */
  tmp = (byte_stream_get8(bs));
  if (!(tmp != 0)) {
    purple_debug_warning("oscar","Dropping auth request SNAC because username was empty\n");
    return 0;
  }
  bn = byte_stream_getstr(bs,tmp);
  if (!(g_utf8_validate(bn,(-1),0) != 0)) {
    purple_debug_warning("oscar","Dropping auth request SNAC because the username was not valid UTF-8\n");
    g_free(bn);
  }
/* Read message */
  tmp = byte_stream_get16(bs);
  if (tmp != 0) {
    msg = byte_stream_getstr(bs,tmp);
    if (!(g_utf8_validate(msg,(-1),0) != 0)) {
/* Ugh, msg isn't UTF8.  Let's salvage. */
      purple_debug_warning("oscar","Got non-UTF8 message in auth request from %s\n",bn);
      tmpstr = purple_utf8_salvage(msg);
      g_free(msg);
      msg = tmpstr;
    }
  }
  else 
    msg = ((char *)((void *)0));
/* Unknown */
  tmp = byte_stream_get16(bs);
  if ((userfunc = aim_callhandler(od,(snac -> family),(snac -> subtype))) != 0) 
    ret = ( *userfunc)(od,conn,frame,bn,msg);
  g_free(bn);
  g_free(msg);
  return ret;
}
/*
 * Subtype 0x001a - Send authorization reply
 *
 * Sends a reply to a request for authorization.  The reply can either
 * grant authorization or deny authorization.
 *
 * if reply=0x00 then deny
 * if reply=0x01 then grant
 *
 */

int aim_ssi_sendauthreply(OscarData *od,const char *bn,guint8 reply,const char *msg)
{
  FlapConnection *conn;
  ByteStream bs;
  aim_snacid_t snacid;
  if ((!(od != 0) || !((conn = flap_connection_findbygroup(od,19)) != 0)) || !(bn != 0)) 
    return -22;
  byte_stream_new(&bs,(((((1 + strlen(bn)) + 1) + 2) + (((msg != 0)?(strlen(msg) + 1) : 0))) + 2));
/* Username */
  byte_stream_put8(&bs,(strlen(bn)));
  byte_stream_putstr(&bs,bn);
/* Grant or deny */
  byte_stream_put8(&bs,reply);
/* Message (null terminated) */
  byte_stream_put16(&bs,((msg != 0)?(strlen(msg) + 1) : 0));
  if (msg != 0) {
    byte_stream_putstr(&bs,msg);
    byte_stream_put8(&bs,0);
  }
/* Unknown */
  byte_stream_put16(&bs,0);
  snacid = aim_cachesnac(od,19,0x001a,0,0,0);
  flap_connection_send_snac(od,conn,19,0x001a,snacid,&bs);
  byte_stream_destroy(&bs);
  return 0;
}
/*
 * Subtype 0x001b - Receive an authorization reply
 *
 * You get this bad boy when other people respond to the authorization
 * request that you have previously sent them.
 */

static int receiveauthreply(OscarData *od,FlapConnection *conn,aim_module_t *mod,FlapFrame *frame,aim_modsnac_t *snac,ByteStream *bs)
{
  int ret = 0;
  aim_rxcallback_t userfunc;
  guint16 tmp;
  guint8 reply;
  char *bn;
  char *msg;
  char *tmpstr;
/* Read buddy name */
  tmp = (byte_stream_get8(bs));
  if (!(tmp != 0)) {
    purple_debug_warning("oscar","Dropping auth reply SNAC because username was empty\n");
    return 0;
  }
  bn = byte_stream_getstr(bs,tmp);
  if (!(g_utf8_validate(bn,(-1),0) != 0)) {
    purple_debug_warning("oscar","Dropping auth reply SNAC because the username was not valid UTF-8\n");
    g_free(bn);
  }
/* Read reply */
  reply = byte_stream_get8(bs);
/* Read message */
  tmp = byte_stream_get16(bs);
  if (tmp != 0) {
    msg = byte_stream_getstr(bs,tmp);
    if (!(g_utf8_validate(msg,(-1),0) != 0)) {
/* Ugh, msg isn't UTF8.  Let's salvage. */
      purple_debug_warning("oscar","Got non-UTF8 message in auth reply from %s\n",bn);
      tmpstr = purple_utf8_salvage(msg);
      g_free(msg);
      msg = tmpstr;
    }
  }
  else 
    msg = ((char *)((void *)0));
/* Unknown */
  tmp = byte_stream_get16(bs);
  if ((userfunc = aim_callhandler(od,(snac -> family),(snac -> subtype))) != 0) 
    ret = ( *userfunc)(od,conn,frame,bn,reply,msg);
  g_free(bn);
  g_free(msg);
  return ret;
}
/*
 * Subtype 0x001c - Receive a message telling you someone added you to their list.
 */

static int receiveadded(OscarData *od,FlapConnection *conn,aim_module_t *mod,FlapFrame *frame,aim_modsnac_t *snac,ByteStream *bs)
{
  int ret = 0;
  aim_rxcallback_t userfunc;
  guint16 tmp;
  char *bn;
/* Read buddy name */
  tmp = (byte_stream_get8(bs));
  if (!(tmp != 0)) {
    purple_debug_warning("oscar","Dropping \'you were added\' SNAC because username was empty\n");
    return 0;
  }
  bn = byte_stream_getstr(bs,tmp);
  if (!(g_utf8_validate(bn,(-1),0) != 0)) {
    purple_debug_warning("oscar","Dropping \'you were added\' SNAC because the username was not valid UTF-8\n");
    g_free(bn);
  }
  if ((userfunc = aim_callhandler(od,(snac -> family),(snac -> subtype))) != 0) 
    ret = ( *userfunc)(od,conn,frame,bn);
  g_free(bn);
  return ret;
}
/*
 * If we're on ICQ, then AIM_SSI_TYPE_DENY is used for the "permanently invisible" list.
 * AIM_SSI_TYPE_ICQDENY is used for blocking users instead.
 */

guint16 aim_ssi_getdenyentrytype(OscarData *od)
{
  return (((od -> icq) != 0)?14 : 3);
}

static int snachandler(OscarData *od,FlapConnection *conn,aim_module_t *mod,FlapFrame *frame,aim_modsnac_t *snac,ByteStream *bs)
{
  if ((snac -> subtype) == 3) 
    return parserights(od,conn,mod,frame,snac,bs);
  else if ((snac -> subtype) == 6) 
    return parsedata(od,conn,mod,frame,snac,bs);
  else if ((snac -> subtype) == 8) 
    return parseadd(od,conn,mod,frame,snac,bs);
  else if ((snac -> subtype) == 9) 
    return parsemod(od,conn,mod,frame,snac,bs);
  else if ((snac -> subtype) == 10) 
    return parsedel(od,conn,mod,frame,snac,bs);
  else if ((snac -> subtype) == 14) 
    return parseack(od,conn,mod,frame,snac,bs);
  else if ((snac -> subtype) == 15) 
    return parsedataunchanged(od,conn,mod,frame,snac,bs);
  else if ((snac -> subtype) == 21) 
    return receiveauthgrant(od,conn,mod,frame,snac,bs);
  else if ((snac -> subtype) == 25) 
    return receiveauthrequest(od,conn,mod,frame,snac,bs);
  else if ((snac -> subtype) == 27) 
    return receiveauthreply(od,conn,mod,frame,snac,bs);
  else if ((snac -> subtype) == 28) 
    return receiveadded(od,conn,mod,frame,snac,bs);
  return 0;
}

static void ssi_shutdown(OscarData *od,aim_module_t *mod)
{
  aim_ssi_freelist(od);
}

int ssi_modfirst(OscarData *od,aim_module_t *mod)
{
  mod -> family = 19;
  mod -> version = 4;
  mod -> toolid = 0x0110;
  mod -> toolversion = 0x0629;
  mod -> flags = 0;
  strncpy((mod -> name),"feedbag",(sizeof(mod -> name)));
  mod -> snachandler = snachandler;
  mod -> shutdown = ssi_shutdown;
  return 0;
}
