/*
 * Purple's oscar protocol plugin
 * This file is the legal property of its developers.
 * Please see the AUTHORS file distributed alongside this file.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
*/
/*
 * Family 0x0017 - Authentication.
 *
 * Deals with the authorizer for SNAC-based login, and also old-style
 * non-SNAC login.
 *
 */
#include "oscar.h"
#include <ctype.h>
#include "cipher.h"
/* #define USE_XOR_FOR_ICQ */
#ifdef USE_XOR_FOR_ICQ
/**
 * Encode a password using old XOR method
 *
 * This takes a const pointer to a (null terminated) string
 * containing the unencoded password.  It also gets passed
 * an already allocated buffer to store the encoded password.
 * This buffer should be the exact length of the password without
 * the null.  The encoded password buffer /is not %NULL terminated/.
 *
 * The encoding_table seems to be a fixed set of values.  We'll
 * hope it doesn't change over time!
 *
 * This is only used for the XOR method, not the better MD5 method.
 *
 * @param password Incoming password.
 * @param encoded Buffer to put encoded password.
 */
#endif
#ifdef USE_OLD_MD5
#else

static int aim_encode_password_md5(const char *password,size_t password_len,const char *key,guint8 *digest)
{
  PurpleCipher *cipher;
  PurpleCipherContext *context;
  guchar passdigest[16UL];
  cipher = purple_ciphers_find_cipher("md5");
  context = purple_cipher_context_new(cipher,0);
  purple_cipher_context_append(context,((const guchar *)password),password_len);
  purple_cipher_context_digest(context,16,passdigest,0);
  purple_cipher_context_destroy(context);
  context = purple_cipher_context_new(cipher,0);
  purple_cipher_context_append(context,((const guchar *)key),strlen(key));
  purple_cipher_context_append(context,passdigest,16);
  purple_cipher_context_append(context,((const guchar *)"AOL Instant Messenger (SM)"),strlen("AOL Instant Messenger (SM)"));
  purple_cipher_context_digest(context,16,digest,0);
  purple_cipher_context_destroy(context);
  return 0;
}
#endif
#ifdef USE_XOR_FOR_ICQ
/*
 * Part two of the ICQ hack.  Note the ignoring of the key.
 */
/* FLAP Version */
/* distribution chan */
#endif
/*
 * Subtype 0x0002
 *
 * This is the initial login request packet.
 *
 * NOTE!! If you want/need to make use of the aim_sendmemblock() function,
 * then the client information you send here must exactly match the
 * executable that you're pulling the data from.
 *
 * Java AIM 1.1.19:
 *   clientstring = "AOL Instant Messenger (TM) version 1.1.19 for Java built 03/24/98, freeMem 215871 totalMem 1048567, i686, Linus, #2 SMP Sun Feb 11 03:41:17 UTC 2001 2.4.1-ac9, IBM Corporation, 1.1.8, 45.3, Tue Mar 27 12:09:17 PST 2001"
 *   clientid = 0x0001
 *   major  = 0x0001
 *   minor  = 0x0001
 *   point = (not sent)
 *   build  = 0x0013
 *   unknown= (not sent)
 *
 * AIM for Linux 1.1.112:
 *   clientstring = "AOL Instant Messenger (SM)"
 *   clientid = 0x1d09
 *   major  = 0x0001
 *   minor  = 0x0001
 *   point = 0x0001
 *   build  = 0x0070
 *   unknown= 0x0000008b
 *   serverstore = 0x01
 *
 * @param truncate_pass Truncate the password to 8 characters.  This
 *        usually happens for AOL accounts.  We are told that we
 *        should truncate it if the 0x0017/0x0007 SNAC contains
 *        a TLV of type 0x0026 with data 0x0000.
 * @param allow_multiple_logins Allow multiple logins? If TRUE, the AIM
 *        server will prompt the user when multiple logins occur. If
 *        FALSE, existing connections (on other clients) will be
 *        disconnected automatically as we connect.
 */

int aim_send_login(OscarData *od,FlapConnection *conn,const char *sn,const char *password,gboolean truncate_pass,ClientInfo *ci,const char *key,gboolean allow_multiple_logins)
{
  FlapFrame *frame;
  GSList *tlvlist = (GSList *)((void *)0);
  guint8 digest[16UL];
  aim_snacid_t snacid;
  size_t password_len;
  guint32 distrib;
  if ((!(ci != 0) || !(sn != 0)) || !(password != 0)) 
    return -22;
#ifdef USE_XOR_FOR_ICQ
/* If we're signing on an ICQ account then use the older, XOR login method */
#endif
  frame = flap_frame_new(od,2,1152);
  snacid = aim_cachesnac(od,23,2,0,0,0);
  aim_putsnac(&frame -> data,23,2,snacid);
  aim_tlvlist_add_str(&tlvlist,1,sn);
/* Truncate ICQ and AOL passwords, if necessary */
  password_len = strlen(password);
  if ((oscar_util_valid_name_icq(sn) != 0) && (password_len > 8)) 
    password_len = 8;
  else if ((truncate_pass != 0) && (password_len > 8)) 
    password_len = 8;
  aim_encode_password_md5(password,password_len,key,digest);
  distrib = (oscar_get_ui_info_int(((((od -> icq) != 0)?"prpl-icq-distid" : "prpl-aim-distid")),(ci -> distrib)));
  aim_tlvlist_add_raw(&tlvlist,0x25,16,digest);
#ifndef USE_OLD_MD5
  aim_tlvlist_add_noval(&tlvlist,0x004c);
#endif
  if ((ci -> clientstring) != ((const char *)((void *)0))) 
    aim_tlvlist_add_str(&tlvlist,3,(ci -> clientstring));
  else {
    gchar *clientstring = oscar_get_clientstring();
    aim_tlvlist_add_str(&tlvlist,3,clientstring);
    g_free(clientstring);
  }
  aim_tlvlist_add_16(&tlvlist,22,((guint16 )(ci -> clientid)));
  aim_tlvlist_add_16(&tlvlist,23,((guint16 )(ci -> major)));
  aim_tlvlist_add_16(&tlvlist,24,((guint16 )(ci -> minor)));
  aim_tlvlist_add_16(&tlvlist,25,((guint16 )(ci -> point)));
  aim_tlvlist_add_16(&tlvlist,0x001a,((guint16 )(ci -> build)));
  aim_tlvlist_add_32(&tlvlist,20,distrib);
  aim_tlvlist_add_str(&tlvlist,15,(ci -> lang));
  aim_tlvlist_add_str(&tlvlist,14,(ci -> country));
/*
	 * If set, old-fashioned buddy lists will not work. You will need
	 * to use SSI.
	 */
  aim_tlvlist_add_8(&tlvlist,0x004a,((allow_multiple_logins != 0)?1 : 3));
  aim_tlvlist_write(&frame -> data,&tlvlist);
  aim_tlvlist_free(tlvlist);
  flap_connection_send(conn,frame);
  return 0;
}
/*
 * This is sent back as a general response to the login command.
 * It can be either an error or a success, depending on the
 * presence of certain TLVs.
 *
 * The client should check the value passed as errorcode. If
 * its nonzero, there was an error.
 */

static int parse(OscarData *od,FlapConnection *conn,aim_module_t *mod,FlapFrame *frame,aim_modsnac_t *snac,ByteStream *bs)
{
  GSList *tlvlist;
  aim_rxcallback_t userfunc;
  struct aim_authresp_info *info;
  int ret = 0;
  info = ((struct aim_authresp_info *)(g_malloc0_n(1,(sizeof(struct aim_authresp_info )))));
/*
	 * Read block of TLVs.  All further data is derived
	 * from what is parsed here.
	 */
  tlvlist = aim_tlvlist_read(bs);
/*
	 * No matter what, we should have a username.
	 */
  if (aim_tlv_gettlv(tlvlist,1,1) != 0) {
    info -> bn = aim_tlv_getstr(tlvlist,1,1);
    purple_connection_set_display_name((od -> gc),(info -> bn));
  }
/*
	 * Check for an error code.  If so, we should also
	 * have an error url.
	 */
  if (aim_tlv_gettlv(tlvlist,8,1) != 0) 
    info -> errorcode = aim_tlv_get16(tlvlist,8,1);
  if (aim_tlv_gettlv(tlvlist,4,1) != 0) 
    info -> errorurl = aim_tlv_getstr(tlvlist,4,1);
/*
	 * BOS server address.
	 */
  if (aim_tlv_gettlv(tlvlist,5,1) != 0) 
    info -> bosip = aim_tlv_getstr(tlvlist,5,1);
/*
	 * Authorization cookie.
	 */
  if (aim_tlv_gettlv(tlvlist,6,1) != 0) {
    aim_tlv_t *tmptlv;
    tmptlv = aim_tlv_gettlv(tlvlist,6,1);
    if (tmptlv != ((aim_tlv_t *)((void *)0))) {
      info -> cookielen = (tmptlv -> length);
      info -> cookie = (tmptlv -> value);
    }
  }
/*
	 * The email address attached to this account
	 *   Not available for ICQ or @mac.com logins.
	 *   If you receive this TLV, then you are allowed to use
	 *   family 0x0018 to check the status of your email.
	 * XXX - Not really true!
	 */
  if (aim_tlv_gettlv(tlvlist,17,1) != 0) 
    info -> email = aim_tlv_getstr(tlvlist,17,1);
/*
	 * The registration status.  (Not real sure what it means.)
	 *   Not available for ICQ or @mac.com logins.
	 *
	 *   1 = No disclosure
	 *   2 = Limited disclosure
	 *   3 = Full disclosure
	 *
	 * This has to do with whether your email address is available
	 * to other users or not.  AFAIK, this feature is no longer used.
	 *
	 * Means you can use the admin family? (0x0007)
	 *
	 */
  if (aim_tlv_gettlv(tlvlist,19,1) != 0) 
    info -> regstatus = aim_tlv_get16(tlvlist,19,1);
  if (aim_tlv_gettlv(tlvlist,64,1) != 0) 
    info -> latestbeta.build = aim_tlv_get32(tlvlist,64,1);
  if (aim_tlv_gettlv(tlvlist,65,1) != 0) 
    info -> latestbeta.url = aim_tlv_getstr(tlvlist,65,1);
  if (aim_tlv_gettlv(tlvlist,0x0042,1) != 0) 
    info -> latestbeta.info = aim_tlv_getstr(tlvlist,0x0042,1);
  if (aim_tlv_gettlv(tlvlist,0x0043,1) != 0) 
    info -> latestbeta.name = aim_tlv_getstr(tlvlist,0x0043,1);
  if (aim_tlv_gettlv(tlvlist,0x0044,1) != 0) 
    info -> latestrelease.build = aim_tlv_get32(tlvlist,0x0044,1);
  if (aim_tlv_gettlv(tlvlist,69,1) != 0) 
    info -> latestrelease.url = aim_tlv_getstr(tlvlist,69,1);
  if (aim_tlv_gettlv(tlvlist,0x0046,1) != 0) 
    info -> latestrelease.info = aim_tlv_getstr(tlvlist,0x0046,1);
  if (aim_tlv_gettlv(tlvlist,0x0047,1) != 0) 
    info -> latestrelease.name = aim_tlv_getstr(tlvlist,0x0047,1);
/*
	 * URL to change password.
	 */
  if (aim_tlv_gettlv(tlvlist,0x0054,1) != 0) 
    info -> chpassurl = aim_tlv_getstr(tlvlist,0x0054,1);
  od -> authinfo = info;
  if ((userfunc = aim_callhandler(od,(((snac != 0)?(snac -> family) : 23)),(((snac != 0)?(snac -> subtype) : 3)))) != 0) 
    ret = ( *userfunc)(od,conn,frame,info);
  aim_tlvlist_free(tlvlist);
  return ret;
}
#ifdef USE_XOR_FOR_ICQ
/*
 * Subtype 0x0007 (kind of) - Send a fake type 0x0007 SNAC to the client
 *
 * This is a bit confusing.
 *
 * Normal SNAC login goes like this:
 *   - connect
 *   - server sends flap version
 *   - client sends flap version
 *   - client sends username (17/6)
 *   - server sends hash key (17/7)
 *   - client sends auth request (17/2 -- aim_send_login)
 *   - server yells
 *
 * XOR login (for ICQ) goes like this:
 *   - connect
 *   - server sends flap version
 *   - client sends auth request which contains flap version (aim_send_login)
 *   - server yells
 *
 * For the client API, we make them implement the most complicated version,
 * and for the simpler version, we fake it and make it look like the more
 * complicated process.
 *
 * This is done by giving the client a faked key, just so we can convince
 * them to call aim_send_login right away, which will detect the session
 * flag that says this is XOR login and ignore the key, sending an ICQ
 * login request instead of the normal SNAC one.
 *
 * As soon as AOL makes ICQ log in the same way as AIM, this is /gone/.
 */
#endif
/*
 * Subtype 0x0006
 *
 * In AIM 3.5 protocol, the first stage of login is to request login from the
 * Authorizer, passing it the username for verification.  If the name is
 * invalid, a 0017/0003 is spit back, with the standard error contents.  If
 * valid, a 0017/0007 comes back, which is the signal to send it the main
 * login command (0017/0002).
 *
 */

int aim_request_login(OscarData *od,FlapConnection *conn,const char *sn)
{
  FlapFrame *frame;
  aim_snacid_t snacid;
  GSList *tlvlist = (GSList *)((void *)0);
  if ((!(od != 0) || !(conn != 0)) || !(sn != 0)) 
    return -22;
#ifdef USE_XOR_FOR_ICQ
#endif
  frame = flap_frame_new(od,2,(((10 + 2 + 2) + strlen(sn)) + 8));
  snacid = aim_cachesnac(od,23,6,0,0,0);
  aim_putsnac(&frame -> data,23,6,snacid);
  aim_tlvlist_add_str(&tlvlist,1,sn);
/* Tell the server we support SecurID logins. */
  aim_tlvlist_add_noval(&tlvlist,0x004b);
/* Unknown.  Sent in recent WinAIM clients.*/
  aim_tlvlist_add_noval(&tlvlist,0x005a);
  aim_tlvlist_write(&frame -> data,&tlvlist);
  aim_tlvlist_free(tlvlist);
  flap_connection_send(conn,frame);
  return 0;
}
/*
 * Subtype 0x0007
 *
 * Middle handler for 0017/0007 SNACs.  Contains the auth key prefixed
 * by only its length in a two byte word.
 *
 * Calls the client, which should then use the value to call aim_send_login.
 *
 */

static int keyparse(OscarData *od,FlapConnection *conn,aim_module_t *mod,FlapFrame *frame,aim_modsnac_t *snac,ByteStream *bs)
{
  int keylen;
  int ret = 1;
  aim_rxcallback_t userfunc;
  char *keystr;
  GSList *tlvlist;
  gboolean truncate_pass;
  keylen = (byte_stream_get16(bs));
  keystr = byte_stream_getstr(bs,keylen);
  tlvlist = aim_tlvlist_read(bs);
/*
	 * If the truncate_pass TLV exists then we should truncate the
	 * user's password to 8 characters.  This flag is sent to us
	 * when logging in with an AOL user's username.
	 */
  truncate_pass = (aim_tlv_gettlv(tlvlist,0x0026,1) != ((aim_tlv_t *)((void *)0)));
/* XXX - When GiantGrayPanda signed on AIM I got a thing asking me to register
	 * for the netscape network.  This SNAC had a type 0x0058 TLV with length 10.
	 * Data is 0x0007 0004 3e19 ae1e 0006 0004 0000 0005 */
  if ((userfunc = aim_callhandler(od,(snac -> family),(snac -> subtype))) != 0) 
    ret = ( *userfunc)(od,conn,frame,keystr,((int )truncate_pass));
  g_free(keystr);
  aim_tlvlist_free(tlvlist);
  return ret;
}
/**
 * Subtype 0x000a
 *
 * Receive SecurID request.
 */

static int got_securid_request(OscarData *od,FlapConnection *conn,aim_module_t *mod,FlapFrame *frame,aim_modsnac_t *snac,ByteStream *bs)
{
  int ret = 0;
  aim_rxcallback_t userfunc;
  if ((userfunc = aim_callhandler(od,(snac -> family),(snac -> subtype))) != 0) 
    ret = ( *userfunc)(od,conn,frame);
  return ret;
}
/**
 * Subtype 0x000b
 *
 * Send SecurID response.
 */

int aim_auth_securid_send(OscarData *od,const char *securid)
{
  FlapConnection *conn;
  FlapFrame *frame;
  aim_snacid_t snacid;
  int len;
  if ((!(od != 0) || !((conn = flap_connection_getbytype_all(od,23)) != 0)) || !(securid != 0)) 
    return -22;
  len = (strlen(securid));
  frame = flap_frame_new(od,2,(10 + 2 + len));
  snacid = aim_cachesnac(od,23,11,0,0,0);
  aim_putsnac(&frame -> data,23,11,0);
  byte_stream_put16(&frame -> data,len);
  byte_stream_putstr(&frame -> data,securid);
  flap_connection_send(conn,frame);
  return 0;
}

static void auth_shutdown(OscarData *od,aim_module_t *mod)
{
  if ((od -> authinfo) != ((struct aim_authresp_info *)((void *)0))) {
    g_free(( *(od -> authinfo)).bn);
    g_free(( *(od -> authinfo)).bosip);
    g_free(( *(od -> authinfo)).errorurl);
    g_free(( *(od -> authinfo)).email);
    g_free(( *(od -> authinfo)).chpassurl);
    g_free(( *(od -> authinfo)).latestrelease.name);
    g_free(( *(od -> authinfo)).latestrelease.url);
    g_free(( *(od -> authinfo)).latestrelease.info);
    g_free(( *(od -> authinfo)).latestbeta.name);
    g_free(( *(od -> authinfo)).latestbeta.url);
    g_free(( *(od -> authinfo)).latestbeta.info);
    g_free((od -> authinfo));
  }
}

static int snachandler(OscarData *od,FlapConnection *conn,aim_module_t *mod,FlapFrame *frame,aim_modsnac_t *snac,ByteStream *bs)
{
  if ((snac -> subtype) == 3) 
    return parse(od,conn,mod,frame,snac,bs);
  else if ((snac -> subtype) == 7) 
    return keyparse(od,conn,mod,frame,snac,bs);
  else if ((snac -> subtype) == 10) 
    return got_securid_request(od,conn,mod,frame,snac,bs);
  return 0;
}

int auth_modfirst(OscarData *od,aim_module_t *mod)
{
  mod -> family = 23;
  mod -> version = 0;
  mod -> flags = 0;
  strncpy((mod -> name),"auth",(sizeof(mod -> name)));
  mod -> snachandler = snachandler;
  mod -> shutdown = auth_shutdown;
  return 0;
}
