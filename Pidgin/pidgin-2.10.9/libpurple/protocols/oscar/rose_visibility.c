/*
 * Purple's oscar protocol plugin
 * This file is the legal property of its developers.
 * Please see the AUTHORS file distributed alongside this file.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
*/
#include "visibility.h"
/* Translators: This string is a menu option that, if selected, will cause
   you to appear online to the chosen user even when your status is set to
   Invisible. */
#define APPEAR_ONLINE		N_("Appear Online")
/* Translators: This string is a menu option that, if selected, will cause
   you to appear offline to the chosen user when your status is set to
   Invisible (this is the default). */
#define DONT_APPEAR_ONLINE	N_("Don't Appear Online")
/* Translators: This string is a menu option that, if selected, will cause
   you to always appear offline to the chosen user (even when your status
   isn't Invisible). */
#define APPEAR_OFFLINE		N_("Appear Offline")
/* Translators: This string is a menu option that, if selected, will cause
   you to appear offline to the chosen user if you are invisible, and
   appear online to the chosen user if you are not invisible (this is the
   default). */
#define DONT_APPEAR_OFFLINE	N_("Don't Appear Offline")

static guint16 get_buddy_list_type(OscarData *od)
{
  PurpleAccount *account = purple_connection_get_account((od -> gc));
  return ((purple_account_is_status_active(account,"invisible") != 0)?2 : 3);
}

static gboolean is_buddy_on_list(OscarData *od,const char *bname)
{
  return aim_ssi_itemlist_finditem(od -> ssi.local,0,bname,get_buddy_list_type(od)) != ((struct aim_ssi_item *)((void *)0));
}

static void visibility_cb(PurpleBlistNode *node,gpointer whatever)
{
  PurpleBuddy *buddy = (PurpleBuddy *)node;
  const char *bname = purple_buddy_get_name(buddy);
  OscarData *od = (purple_connection_get_protocol_data((purple_account_get_connection((purple_buddy_get_account(buddy))))));
  guint16 list_type = get_buddy_list_type(od);
  if (!(is_buddy_on_list(od,bname) != 0)) {
    aim_ssi_add_to_private_list(od,bname,list_type);
  }
  else {
    aim_ssi_del_from_private_list(od,bname,list_type);
  }
}

PurpleMenuAction *create_visibility_menu_item(OscarData *od,const char *bname)
{
  PurpleAccount *account = purple_connection_get_account((od -> gc));
  gboolean invisible = purple_account_is_status_active(account,"invisible");
  gboolean on_list = is_buddy_on_list(od,bname);
  const gchar *label;
  if (invisible != 0) {
    label = ((on_list != 0)?((const char *)(dgettext("pidgin","Don\'t Appear Online"))) : ((const char *)(dgettext("pidgin","Appear Online"))));
  }
  else {
    label = ((on_list != 0)?((const char *)(dgettext("pidgin","Don\'t Appear Offline"))) : ((const char *)(dgettext("pidgin","Appear Offline"))));
  }
  return purple_menu_action_new(label,((PurpleCallback )visibility_cb),0,0);
}

static void show_private_list(PurplePluginAction *action,guint16 list_type,const gchar *title,const gchar *list_description,const gchar *menu_action_name)
{
  PurpleConnection *gc = (PurpleConnection *)(action -> context);
  OscarData *od = (purple_connection_get_protocol_data(gc));
  PurpleAccount *account = purple_connection_get_account(gc);
  GSList *buddies;
  GSList *filtered_buddies;
  GSList *cur;
  gchar *text;
  gchar *secondary;
  buddies = purple_find_buddies(account,0);
  filtered_buddies = ((GSList *)((void *)0));
  for (cur = buddies; cur != ((GSList *)((void *)0)); cur = (cur -> next)) {
    PurpleBuddy *buddy;
    const gchar *bname;
    buddy = (cur -> data);
    bname = purple_buddy_get_name(buddy);
    if (aim_ssi_itemlist_finditem(od -> ssi.local,0,bname,list_type) != 0) {
      filtered_buddies = g_slist_prepend(filtered_buddies,buddy);
    }
  }
  g_slist_free(buddies);
  filtered_buddies = g_slist_reverse(filtered_buddies);
  text = oscar_format_buddies(filtered_buddies,((const char *)(dgettext("pidgin","you have no buddies on this list"))));
  g_slist_free(filtered_buddies);
  secondary = g_strdup_printf(((const char *)(dgettext("pidgin","You can add a buddy to this list by right-clicking on them and selecting \"%s\""))),menu_action_name);
  purple_notify_formatted(gc,title,list_description,secondary,text,0,0);
  g_free(secondary);
  g_free(text);
}

void oscar_show_visible_list(PurplePluginAction *action)
{
  show_private_list(action,2,((const char *)(dgettext("pidgin","Visible List"))),((const char *)(dgettext("pidgin","These buddies will see your status when you switch to \"Invisible\""))),((const char *)(dgettext("pidgin","Appear Online"))));
}

void oscar_show_invisible_list(PurplePluginAction *action)
{
  show_private_list(action,3,((const char *)(dgettext("pidgin","Invisible List"))),((const char *)(dgettext("pidgin","These buddies will always see you as offline"))),((const char *)(dgettext("pidgin","Appear Offline"))));
}
