/*
 * Purple's oscar protocol plugin
 * This file is the legal property of its developers.
 * Please see the AUTHORS file distributed alongside this file.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
*/
#include "encoding.h"

static gchar *encoding_multi_convert_to_utf8(const gchar *text,gssize textlen,const gchar *encodings,GError **error,gboolean fallback)
{
  gchar *utf8 = (gchar *)((void *)0);
  const gchar *begin = encodings;
  const gchar *end = (const gchar *)((void *)0);
/* allocated buffer for encoding name */
  gchar *curr_encoding = (gchar *)((void *)0);
/* read-only encoding name */
  const gchar *curr_encoding_ro = (const gchar *)((void *)0);
  if (!(encodings != 0)) {
    purple_debug_error("oscar","encodings is NULL");
    return 0;
  }
{
    for (; ; ) {
/* extract next encoding */
      end = (strchr(begin,','));
      if (!(end != 0)) {
        curr_encoding_ro = begin;
/* allocate buffer for encoding */
      }
      else {
        curr_encoding = g_strndup(begin,(end - begin));
        if (!(curr_encoding != 0)) {
          purple_debug_error("oscar","Error allocating memory for encoding");
          break; 
        }
        curr_encoding_ro = curr_encoding;
      }
      if (!(g_ascii_strcasecmp(curr_encoding_ro,"utf-8") != 0) && (g_utf8_validate(text,textlen,0) != 0)) {
        break; 
      }
      utf8 = g_convert(text,textlen,"UTF-8",curr_encoding_ro,0,0,0);
/* last occurence. do not free curr_encoding: buffer was'nt allocated */
      if (!(end != 0)) 
        break; 
/* free allocated buffer for encoding here */
      g_free(curr_encoding);
/* text was successfully converted */
      if (utf8 != 0) 
        break; 
      begin = (end + 1);
    }
  }
  if (!(utf8 != 0) && (fallback != 0)) 
/* "begin" points to last encoding */
{
    utf8 = g_convert_with_fallback(text,textlen,"UTF-8",begin,"\?",0,0,error);
  }
  return utf8;
}

static gchar *encoding_extract(const char *encoding)
{
  char *begin;
  char *end;
  if (encoding == ((const char *)((void *)0))) {
    return 0;
  }
  if ((!(g_str_has_prefix(encoding,"text/aolrtf; charset=") != 0) && !(g_str_has_prefix(encoding,"text/x-aolrtf; charset=") != 0)) && !(g_str_has_prefix(encoding,"text/plain; charset=") != 0)) {
    return g_strdup(encoding);
  }
  begin = strchr(encoding,'"');
  end = strrchr(encoding,'"');
  if (((begin == ((char *)((void *)0))) || (end == ((char *)((void *)0)))) || (begin >= end)) {
    return g_strdup(encoding);
  }
  return g_strndup((begin + 1),((end - 1) - begin));
}

gchar *oscar_encoding_to_utf8(const char *encoding,const char *text,int textlen)
{
  gchar *utf8 = (gchar *)((void *)0);
  const gchar *glib_encoding = (const gchar *)((void *)0);
  gchar *extracted_encoding = encoding_extract(encoding);
  if ((extracted_encoding == ((gchar *)((void *)0))) || (( *extracted_encoding) == 0)) {
    purple_debug_info("oscar","Empty encoding, assuming UTF-8\n");
  }
  else if (!(g_ascii_strcasecmp(extracted_encoding,"iso-8859-1") != 0)) {
    glib_encoding = "iso-8859-1";
  }
  else if (!(g_ascii_strcasecmp(extracted_encoding,"ISO-8859-1-Windows-3.1-Latin-1") != 0) || !(g_ascii_strcasecmp(extracted_encoding,"us-ascii") != 0)) {
    glib_encoding = "Windows-1252";
  }
  else if (!(g_ascii_strcasecmp(extracted_encoding,"unicode-2-0") != 0)) {
    glib_encoding = "UTF-16BE";
  }
  else if (g_ascii_strcasecmp(extracted_encoding,"utf-8") != 0) {
    glib_encoding = extracted_encoding;
  }
  if (glib_encoding != ((const gchar *)((void *)0))) {
    utf8 = encoding_multi_convert_to_utf8(text,textlen,glib_encoding,0,0);
  }
/*
	 * If utf8 is still NULL then either the encoding is utf-8 or
	 * we have been unable to convert the text to utf-8 from the encoding
	 * that was specified.  So we check if the text is valid utf-8 then
	 * just copy it.
	 */
  if (utf8 == ((gchar *)((void *)0))) {
    if (((textlen != 0) && (( *text) != 0)) && !(g_utf8_validate(text,textlen,0) != 0)) 
      utf8 = g_strdup(((const char *)(dgettext("pidgin","(There was an error receiving this message.  The buddy you are speaking with is probably using a different encoding than expected.  If you know what encoding he is using, you can specify it in the advanced account options for your AIM/ICQ account.)"))));
    else 
      utf8 = g_strndup(text,textlen);
  }
  g_free(extracted_encoding);
  return utf8;
}

gchar *oscar_utf8_try_convert(PurpleAccount *account,OscarData *od,const gchar *msg)
{
  const char *charset = (const char *)((void *)0);
  char *ret = (char *)((void *)0);
  if (msg == ((const gchar *)((void *)0))) 
    return 0;
  if (g_utf8_validate(msg,(-1),0) != 0) 
    return g_strdup(msg);
  if ((od -> icq) != 0) 
    charset = purple_account_get_string(account,"encoding",0);
  if ((charset != 0) && (( *charset) != 0)) 
    ret = encoding_multi_convert_to_utf8(msg,(-1),charset,0,0);
  if (!(ret != 0)) 
    ret = purple_utf8_try_convert(msg);
  return ret;
}

static gchar *oscar_convert_to_utf8(const gchar *data,gsize datalen,const char *charsetstr,gboolean fallback)
{
  gchar *ret = (gchar *)((void *)0);
  GError *err = (GError *)((void *)0);
  if ((charsetstr == ((const char *)((void *)0))) || (( *charsetstr) == 0)) 
    return 0;
  if (g_ascii_strcasecmp("UTF-8",charsetstr) != 0) {
    ret = encoding_multi_convert_to_utf8(data,datalen,charsetstr,&err,fallback);
    if (err != ((GError *)((void *)0))) {
      purple_debug_warning("oscar","Conversion from %s failed: %s.\n",charsetstr,(err -> message));
      g_error_free(err);
    }
  }
  else {
    if (g_utf8_validate(data,datalen,0) != 0) 
      ret = g_strndup(data,datalen);
    else 
      purple_debug_warning("oscar","String is not valid UTF-8.\n");
  }
  return ret;
}

gchar *oscar_decode_im(PurpleAccount *account,const char *sourcebn,guint16 charset,const gchar *data,gsize datalen)
{
  gchar *ret = (gchar *)((void *)0);
/* charsetstr1 is always set to what the correct encoding should be. */
  const gchar *charsetstr1;
  const gchar *charsetstr2;
  const gchar *charsetstr3 = (const gchar *)((void *)0);
  if ((datalen == 0) || (data == ((const gchar *)((void *)0)))) 
    return 0;
  if (charset == 2) {
    charsetstr1 = "UTF-16BE";
    charsetstr2 = "UTF-8";
  }
  else if (charset == 3) {
    if ((sourcebn != ((const char *)((void *)0))) && (oscar_util_valid_name_icq(sourcebn) != 0)) 
      charsetstr1 = purple_account_get_string(account,"encoding","ISO-8859-1");
    else 
      charsetstr1 = "ISO-8859-1";
    charsetstr2 = "UTF-8";
  }
  else if (charset == 0) {
/* Should just be "ASCII" */
    charsetstr1 = "ASCII";
    charsetstr2 = purple_account_get_string(account,"encoding","ISO-8859-1");
  }
  else if (charset == 13) {
/* iChat sending unicode over a Direct IM connection = UTF-8 */
/* Mobile AIM client on multiple devices (including Blackberry Tour, Nokia 3100, and LG VX6000) = ISO-8859-1 */
    charsetstr1 = "UTF-8";
    charsetstr2 = "ISO-8859-1";
    charsetstr3 = purple_account_get_string(account,"encoding","ISO-8859-1");
  }
  else {
/* Unknown, hope for valid UTF-8... */
    charsetstr1 = "UTF-8";
    charsetstr2 = purple_account_get_string(account,"encoding","ISO-8859-1");
  }
  purple_debug_info("oscar","Parsing IM, charset=0x%04hx, datalen=%lu, choice1=%s, choice2=%s, choice3=%s\n",charset,datalen,charsetstr1,charsetstr2,((charsetstr3 != 0)?charsetstr3 : ""));
  ret = oscar_convert_to_utf8(data,datalen,charsetstr1,0);
  if (ret == ((gchar *)((void *)0))) {
    if (charsetstr3 != ((const gchar *)((void *)0))) {
/* Try charsetstr2 without allowing substitutions, then fall through to charsetstr3 if needed */
      ret = oscar_convert_to_utf8(data,datalen,charsetstr2,0);
      if (ret == ((gchar *)((void *)0))) 
        ret = oscar_convert_to_utf8(data,datalen,charsetstr3,(!0));
    }
    else {
/* Try charsetstr2, allowing substitutions */
      ret = oscar_convert_to_utf8(data,datalen,charsetstr2,(!0));
    }
  }
  if (ret == ((gchar *)((void *)0))) {
    char *str;
    char *salvage;
    char *tmp;
    str = (g_malloc((datalen + 1)));
    strncpy(str,data,datalen);
    str[datalen] = 0;
    salvage = purple_utf8_salvage(str);
    tmp = g_strdup_printf(((const char *)(dgettext("pidgin","(There was an error receiving this message.  Either you and %s have different encodings selected, or %s has a buggy client.)"))),sourcebn,sourcebn);
    ret = g_strdup_printf("%s %s",salvage,tmp);
    g_free(tmp);
    g_free(str);
    g_free(salvage);
  }
  return ret;
}

static guint16 get_simplest_charset(const char *utf8)
{
  while(( *utf8) != 0){
    if (((unsigned char )( *utf8)) > 0x7f) {
/* not ASCII! */
      return 2;
    }
    utf8++;
  }
  return 0;
}

gchar *oscar_encode_im(const gchar *msg,gsize *result_len,guint16 *charset,gchar **charsetstr)
{
  guint16 msg_charset = get_simplest_charset(msg);
  if (charset != ((guint16 *)((void *)0))) {
     *charset = msg_charset;
  }
  if (charsetstr != ((gchar **)((void *)0))) {
     *charsetstr = ((msg_charset == 0)?"us-ascii" : "unicode-2-0");
  }
  return g_convert(msg,(-1),((msg_charset == 0)?"ASCII" : "UTF-16BE"),"UTF-8",0,result_len,0);
}
