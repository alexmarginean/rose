/*
 * Purple's oscar protocol plugin
 * This file is the legal property of its developers.
 * Please see the AUTHORS file distributed alongside this file.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
*/
/*
 * Displaying various information about buddies.
 */
#include "encoding.h"
#include "oscar.h"

static gchar *oscar_caps_to_string(guint64 caps)
{
  GString *str;
  const gchar *tmp;
  guint64 bit = 1;
  str = g_string_new("");
  if (!(caps != 0UL)) {
    return 0;
  }
  else 
    while(bit <= 0x0000000200000000LL){
      if ((bit & caps) != 0UL) {
        switch(bit){
          case 0x0000000000000001LL:
{
            tmp = ((const char *)(dgettext("pidgin","Buddy Icon")));
            break; 
          }
          case 0x0000000000000002LL:
{
            tmp = ((const char *)(dgettext("pidgin","Voice")));
            break; 
          }
          case 0x0000000000000004LL:
{
            tmp = ((const char *)(dgettext("pidgin","AIM Direct IM")));
            break; 
          }
          case 0x0000000000000008LL:
{
            tmp = ((const char *)(dgettext("pidgin","Chat")));
            break; 
          }
          case 0x0000000000000010LL:
{
            tmp = ((const char *)(dgettext("pidgin","Get File")));
            break; 
          }
          case 0x0000000000000020LL:
{
            tmp = ((const char *)(dgettext("pidgin","Send File")));
            break; 
          }
          case 0x0000000000000040LL:
{
          }
          case 0x0000000000000200LL:
{
            tmp = ((const char *)(dgettext("pidgin","Games")));
            break; 
          }
          case 0x0000000020000000LL:
{
          }
          case 0x0000000040000000LL:
{
            tmp = ((const char *)(dgettext("pidgin","ICQ Xtraz")));
            break; 
          }
          case 0x0000000000000080LL:
{
            tmp = ((const char *)(dgettext("pidgin","Add-Ins")));
            break; 
          }
          case 0x0000000000000100LL:
{
            tmp = ((const char *)(dgettext("pidgin","Send Buddy List")));
            break; 
          }
          case 0x0000000000000400LL:
{
            tmp = ((const char *)(dgettext("pidgin","ICQ Direct Connect")));
            break; 
          }
          case 0x0000000000000800LL:
{
            tmp = ((const char *)(dgettext("pidgin","AP User")));
            break; 
          }
          case 0x0000000000001000LL:
{
            tmp = ((const char *)(dgettext("pidgin","ICQ RTF")));
            break; 
          }
          case 0x0000000000002000LL:
{
            tmp = ((const char *)(dgettext("pidgin","Nihilist")));
            break; 
          }
          case 0x0000000000004000LL:
{
            tmp = ((const char *)(dgettext("pidgin","ICQ Server Relay")));
            break; 
          }
          case 0x0000000000008000LL:
{
            tmp = ((const char *)(dgettext("pidgin","Old ICQ UTF8")));
            break; 
          }
          case 0x0000000000010000LL:
{
            tmp = ((const char *)(dgettext("pidgin","Trillian Encryption")));
            break; 
          }
          case 0x0000000000020000LL:
{
            tmp = ((const char *)(dgettext("pidgin","ICQ UTF8")));
            break; 
          }
          case 0x0000000000100000LL:
{
            tmp = ((const char *)(dgettext("pidgin","Hiptop")));
            break; 
          }
          case 0x0000000000200000LL:
{
            tmp = ((const char *)(dgettext("pidgin","Security Enabled")));
            break; 
          }
          case 0x0000000000800000LL:
{
            tmp = ((const char *)(dgettext("pidgin","Video Chat")));
            break; 
          }
/* Not actually sure about this one... WinAIM doesn't show anything */
          case 0x0000000001000000LL:
{
            tmp = ((const char *)(dgettext("pidgin","iChat AV")));
            break; 
          }
          case 0x0000000002000000LL:
{
            tmp = ((const char *)(dgettext("pidgin","Live Video")));
            break; 
          }
          case 0x0000000004000000LL:
{
            tmp = ((const char *)(dgettext("pidgin","Camera")));
            break; 
          }
          case 0x0000000008000000LL:
{
            tmp = ((const char *)(dgettext("pidgin","Screen Sharing")));
            break; 
          }
          default:
{
            tmp = ((const gchar *)((void *)0));
            break; 
          }
        }
        if (tmp != 0) 
          g_string_append_printf(str,"%s%s",((( *(str -> str)) == 0)?"" : ", "),tmp);
      }
      bit <<= 1;
    }
  return g_string_free(str,0);
}

static void oscar_user_info_add_pair(PurpleNotifyUserInfo *user_info,const char *name,const char *value)
{
  if ((value != 0) && (value[0] != 0)) {
    purple_notify_user_info_add_pair(user_info,name,value);
  }
}

static void oscar_user_info_convert_and_add(PurpleAccount *account,OscarData *od,PurpleNotifyUserInfo *user_info,const char *name,const char *value)
{
  gchar *utf8;
  if (((value != 0) && (value[0] != 0)) && ((utf8 = oscar_utf8_try_convert(account,od,value)) != 0)) {
    purple_notify_user_info_add_pair(user_info,name,utf8);
    g_free(utf8);
  }
}

static void oscar_user_info_convert_and_add_hyperlink(PurpleAccount *account,OscarData *od,PurpleNotifyUserInfo *user_info,const char *name,const char *value,const char *url_prefix)
{
  gchar *utf8;
  if (((value != 0) && (value[0] != 0)) && ((utf8 = oscar_utf8_try_convert(account,od,value)) != 0)) {
    gchar *tmp = g_strdup_printf("<a href=\"%s%s\">%s</a>",url_prefix,utf8,utf8);
    purple_notify_user_info_add_pair(user_info,name,tmp);
    g_free(utf8);
    g_free(tmp);
  }
}
/**
 * @brief Append the status information to a user_info struct
 *
 * The returned information is HTML-ready, appropriately escaped, as all information in a user_info struct should be HTML.
 *
 * @param gc The PurpleConnection
 * @param user_info A PurpleNotifyUserInfo object to which status information will be added
 * @param b The PurpleBuddy whose status is desired. This or the aim_userinfo_t (or both) must be passed to oscar_user_info_append_status().
 * @param userinfo The aim_userinfo_t of the buddy whose status is desired. This or the PurpleBuddy (or both) must be passed to oscar_user_info_append_status().
 * @param use_html_status If TRUE, prefer HTML-formatted away message over plaintext available message.
 */

void oscar_user_info_append_status(PurpleConnection *gc,PurpleNotifyUserInfo *user_info,PurpleBuddy *b,aim_userinfo_t *userinfo,gboolean use_html_status)
{
  PurpleAccount *account = purple_connection_get_account(gc);
  OscarData *od;
  PurplePresence *presence = (PurplePresence *)((void *)0);
  PurpleStatus *status = (PurpleStatus *)((void *)0);
  gchar *message = (gchar *)((void *)0);
  gchar *itmsurl = (gchar *)((void *)0);
  gchar *tmp;
  gboolean escaping_needed = (!0);
  od = (purple_connection_get_protocol_data(gc));
  if ((b == ((PurpleBuddy *)((void *)0))) && (userinfo == ((aim_userinfo_t *)((void *)0)))) 
    return ;
  if (b == ((PurpleBuddy *)((void *)0))) 
    b = purple_find_buddy(purple_connection_get_account(gc),(userinfo -> bn));
  else 
    userinfo = aim_locate_finduserinfo(od,purple_buddy_get_name(b));
  if (b != 0) {
    presence = purple_buddy_get_presence(b);
    status = purple_presence_get_active_status(presence);
  }
/* If we have both b and userinfo we favor userinfo, because if we're
	   viewing someone's profile then we want the HTML away message, and
	   the "message" attribute of the status contains only the plaintext
	   message. */
  if (userinfo != 0) {
    if (((((((userinfo -> flags) & 32) != 0) && (use_html_status != 0)) && ((userinfo -> away_len) > 0)) && ((userinfo -> away) != ((char *)((void *)0)))) && ((userinfo -> away_encoding) != ((char *)((void *)0)))) {
/* Away message */
      message = oscar_encoding_to_utf8((userinfo -> away_encoding),(userinfo -> away),(userinfo -> away_len));
      escaping_needed = 0;
    }
    else {
/*
			 * Available message or non-HTML away message (because that's
			 * all we have right now.
			 */
      if (((userinfo -> status) != ((char *)((void *)0))) && ((userinfo -> status)[0] != 0)) {
        message = oscar_encoding_to_utf8((userinfo -> status_encoding),(userinfo -> status),(userinfo -> status_len));
      }
#if defined (_WIN32) || defined (__APPLE__)
#endif
    }
  }
  else {
    message = g_strdup(purple_status_get_attr_string(status,"message"));
    itmsurl = g_strdup(purple_status_get_attr_string(status,"itmsurl"));
  }
  if (message != 0) {
    tmp = oscar_util_format_string(message,purple_account_get_username(account));
    g_free(message);
    message = tmp;
    if (escaping_needed != 0) {
      tmp = purple_markup_escape_text(message,(-1));
      g_free(message);
      message = tmp;
    }
  }
  if ((use_html_status != 0) && (itmsurl != 0)) {
    tmp = g_strdup_printf("<a href=\"%s\">%s</a>",itmsurl,message);
    g_free(message);
    message = tmp;
  }
  if (b != 0) {
    if (purple_presence_is_online(presence) != 0) {
      gboolean is_away = (((status != 0) && !(purple_status_is_available(status) != 0)) || ((userinfo != 0) && (((userinfo -> flags) & 32) != 0)));
      if ((((oscar_util_valid_name_icq(purple_buddy_get_name(b)) != 0) || (is_away != 0)) || !(message != 0)) || !(( *message) != 0)) {
/* Append the status name for online ICQ statuses, away AIM statuses, and for all buddies with no message.
				 * If the status name and the message are the same, only show one. */
        const char *status_name = purple_status_get_name(status);
        if (((status_name != 0) && (message != 0)) && !(strcmp(status_name,message) != 0)) 
          status_name = ((const char *)((void *)0));
        tmp = g_strdup_printf("%s%s%s",((status_name != 0)?status_name : ""),((((status_name != 0) && (message != 0)) && (( *message) != 0))?": " : ""),(((message != 0) && (( *message) != 0))?message : ""));
        g_free(message);
        message = tmp;
      }
    }
    else if (aim_ssi_waitingforauth(od -> ssi.local,(aim_ssi_itemlist_findparentname(od -> ssi.local,purple_buddy_get_name(b))),purple_buddy_get_name(b)) != 0) {
/* Note if an offline buddy is not authorized */
      tmp = g_strdup_printf("%s%s%s",((const char *)(dgettext("pidgin","Not Authorized"))),(((message != 0) && (( *message) != 0))?": " : ""),(((message != 0) && (( *message) != 0))?message : ""));
      g_free(message);
      message = tmp;
    }
    else {
      g_free(message);
      message = g_strdup(((const char *)(dgettext("pidgin","Offline"))));
    }
  }
  if (presence != 0) {
    const char *mood;
    const char *comment;
    char *description;
    status = purple_presence_get_status(presence,"mood");
    mood = icq_get_custom_icon_description(purple_status_get_attr_string(status,"mood"));
    if (mood != 0) {
      comment = purple_status_get_attr_string(status,"moodtext");
      if (comment != 0) {
        char *escaped_comment = purple_markup_escape_text(comment,(-1));
        description = g_strdup_printf("%s (%s)",((const char *)(dgettext("pidgin",mood))),escaped_comment);
        g_free(escaped_comment);
      }
      else {
        description = g_strdup(((const char *)(dgettext("pidgin",mood))));
      }
      purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Mood"))),description);
      g_free(description);
    }
  }
  purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Status"))),message);
  g_free(message);
}

void oscar_user_info_append_extra_info(PurpleConnection *gc,PurpleNotifyUserInfo *user_info,PurpleBuddy *b,aim_userinfo_t *userinfo)
{
  OscarData *od;
  PurpleAccount *account;
  PurpleGroup *g = (PurpleGroup *)((void *)0);
  struct buddyinfo *bi = (struct buddyinfo *)((void *)0);
  char *tmp;
  const char *bname = (const char *)((void *)0);
  const char *gname = (const char *)((void *)0);
  od = (purple_connection_get_protocol_data(gc));
  account = purple_connection_get_account(gc);
  if ((user_info == ((PurpleNotifyUserInfo *)((void *)0))) || ((b == ((PurpleBuddy *)((void *)0))) && (userinfo == ((aim_userinfo_t *)((void *)0))))) 
    return ;
  if (userinfo == ((aim_userinfo_t *)((void *)0))) 
    userinfo = aim_locate_finduserinfo(od,purple_buddy_get_name(b));
  if (b == ((PurpleBuddy *)((void *)0))) 
    b = purple_find_buddy(account,(userinfo -> bn));
  if (b != ((PurpleBuddy *)((void *)0))) {
    bname = purple_buddy_get_name(b);
    g = purple_buddy_get_group(b);
    gname = purple_group_get_name(g);
  }
  if (userinfo != ((aim_userinfo_t *)((void *)0))) 
    bi = (g_hash_table_lookup((od -> buddyinfo),(purple_normalize(account,(userinfo -> bn)))));
  if ((bi != ((struct buddyinfo *)((void *)0))) && ((bi -> ipaddr) != 0)) {
    tmp = g_strdup_printf("%hhu.%hhu.%hhu.%hhu",(((bi -> ipaddr) & 0xff000000) >> 24),(((bi -> ipaddr) & 0x00ff0000) >> 16),(((bi -> ipaddr) & 0x0000ff00) >> 8),((bi -> ipaddr) & 0x000000ff));
    oscar_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","IP Address"))),tmp);
    g_free(tmp);
  }
  if ((userinfo != ((aim_userinfo_t *)((void *)0))) && ((userinfo -> warnlevel) != 0)) {
    tmp = g_strdup_printf("%d",((int )(((userinfo -> warnlevel) / 10.0) + .5)));
    oscar_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Warning Level"))),tmp);
    g_free(tmp);
  }
  if ((((b != ((PurpleBuddy *)((void *)0))) && (bname != ((const char *)((void *)0)))) && (g != ((PurpleGroup *)((void *)0)))) && (gname != ((const char *)((void *)0)))) {
    tmp = aim_ssi_getcomment(od -> ssi.local,gname,bname);
    if (tmp != ((char *)((void *)0))) {
      char *tmp2 = g_markup_escape_text(tmp,(strlen(tmp)));
      g_free(tmp);
      oscar_user_info_convert_and_add(account,od,user_info,((const char *)(dgettext("pidgin","Buddy Comment"))),tmp2);
      g_free(tmp2);
    }
  }
}

void oscar_user_info_display_error(OscarData *od,guint16 error_reason,gchar *buddy)
{
  PurpleNotifyUserInfo *user_info = purple_notify_user_info_new();
  gchar *buf = g_strdup_printf(((const char *)(dgettext("pidgin","User information not available: %s"))),oscar_get_msgerr_reason(error_reason));
  purple_notify_user_info_add_pair(user_info,0,buf);
  purple_notify_userinfo((od -> gc),buddy,user_info,0,0);
  purple_notify_user_info_destroy(user_info);
  purple_conv_present_error(buddy,purple_connection_get_account((od -> gc)),buf);
  g_free(buf);
}

void oscar_user_info_display_icq(OscarData *od,struct aim_icq_info *info)
{
  PurpleConnection *gc = (od -> gc);
  PurpleAccount *account = purple_connection_get_account(gc);
  PurpleBuddy *buddy;
  struct buddyinfo *bi;
  gchar who[16UL];
  PurpleNotifyUserInfo *user_info;
  if (!((info -> uin) != 0U)) 
    return ;
  user_info = purple_notify_user_info_new();
  g_snprintf(who,(sizeof(who)),"%u",(info -> uin));
  buddy = purple_find_buddy(account,who);
  if (buddy != ((PurpleBuddy *)((void *)0))) 
    bi = (g_hash_table_lookup((od -> buddyinfo),(purple_normalize(account,purple_buddy_get_name(buddy)))));
  else 
    bi = ((struct buddyinfo *)((void *)0));
  purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","UIN"))),who);
  oscar_user_info_convert_and_add(account,od,user_info,((const char *)(dgettext("pidgin","Nick"))),(info -> nick));
  if ((bi != ((struct buddyinfo *)((void *)0))) && ((bi -> ipaddr) != 0)) {
    char *tstr = g_strdup_printf("%hhu.%hhu.%hhu.%hhu",(((bi -> ipaddr) & 0xff000000) >> 24),(((bi -> ipaddr) & 0x00ff0000) >> 16),(((bi -> ipaddr) & 0x0000ff00) >> 8),((bi -> ipaddr) & 0x000000ff));
    purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","IP Address"))),tstr);
    g_free(tstr);
  }
  oscar_user_info_convert_and_add(account,od,user_info,((const char *)(dgettext("pidgin","First Name"))),(info -> first));
  oscar_user_info_convert_and_add(account,od,user_info,((const char *)(dgettext("pidgin","Last Name"))),(info -> last));
  oscar_user_info_convert_and_add_hyperlink(account,od,user_info,((const char *)(dgettext("pidgin","Email Address"))),(info -> email),"mailto:");
  if (((info -> numaddresses) != 0) && ((info -> email2) != 0)) {
    int i;
    for (i = 0; i < (info -> numaddresses); i++) {
      oscar_user_info_convert_and_add_hyperlink(account,od,user_info,((const char *)(dgettext("pidgin","Email Address"))),(info -> email2)[i],"mailto:");
    }
  }
  oscar_user_info_convert_and_add(account,od,user_info,((const char *)(dgettext("pidgin","Mobile Phone"))),(info -> mobile));
  if ((info -> gender) != 0) 
    purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Gender"))),(((info -> gender) == 1)?((const char *)(dgettext("pidgin","Female"))) : ((const char *)(dgettext("pidgin","Male")))));
  if ((((info -> birthyear) > 1900) && ((info -> birthmonth) > 0)) && ((info -> birthday) > 0)) {
/* Initialize the struct properly or strftime() will crash
		 * under some conditions (e.g. Debian sarge w/ LANG=en_HK). */
    time_t t = time(0);
    struct tm *tm = localtime((&t));
    tm -> tm_mday = ((int )(info -> birthday));
    tm -> tm_mon = (((int )(info -> birthmonth)) - 1);
    tm -> tm_year = (((int )(info -> birthyear)) - 1900);
/* Ignore dst setting of today to avoid timezone shift between 
		 * dates in summer and winter time. */
    tm -> tm_isdst = -1;
/* To be 100% sure that the fields are re-normalized.
		 * If you're sure strftime() ALWAYS does this EVERYWHERE,
		 * feel free to remove it.  --rlaager */
    mktime(tm);
    oscar_user_info_convert_and_add(account,od,user_info,((const char *)(dgettext("pidgin","Birthday"))),purple_date_format_short(tm));
  }
  if (((info -> age) > 0) && ((info -> age) < 0x000000ff)) {
    char age[5UL];
    snprintf(age,(sizeof(age)),"%hhd",(info -> age));
    purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Age"))),age);
  }
  oscar_user_info_convert_and_add_hyperlink(account,od,user_info,((const char *)(dgettext("pidgin","Personal Web Page"))),(info -> email),"");
  if (buddy != ((PurpleBuddy *)((void *)0))) 
/* aim_userinfo_t */
/* use_html_status */
    oscar_user_info_append_status(gc,user_info,buddy,0,(!0));
  oscar_user_info_convert_and_add(account,od,user_info,((const char *)(dgettext("pidgin","Additional Information"))),(info -> info));
  purple_notify_user_info_add_section_break(user_info);
  if ((((((info -> homeaddr) != 0) && ((info -> homeaddr)[0] != 0)) || (((info -> homecity) != 0) && ((info -> homecity)[0] != 0))) || (((info -> homestate) != 0) && ((info -> homestate)[0] != 0))) || (((info -> homezip) != 0) && ((info -> homezip)[0] != 0))) {
    purple_notify_user_info_add_section_header(user_info,((const char *)(dgettext("pidgin","Home Address"))));
    oscar_user_info_convert_and_add(account,od,user_info,((const char *)(dgettext("pidgin","Address"))),(info -> homeaddr));
    oscar_user_info_convert_and_add(account,od,user_info,((const char *)(dgettext("pidgin","City"))),(info -> homecity));
    oscar_user_info_convert_and_add(account,od,user_info,((const char *)(dgettext("pidgin","State"))),(info -> homestate));
    oscar_user_info_convert_and_add(account,od,user_info,((const char *)(dgettext("pidgin","Zip Code"))),(info -> homezip));
  }
  if ((((((info -> workaddr) != 0) && ((info -> workaddr)[0] != 0)) || (((info -> workcity) != 0) && ((info -> workcity)[0] != 0))) || (((info -> workstate) != 0) && ((info -> workstate)[0] != 0))) || (((info -> workzip) != 0) && ((info -> workzip)[0] != 0))) {
    purple_notify_user_info_add_section_header(user_info,((const char *)(dgettext("pidgin","Work Address"))));
    oscar_user_info_convert_and_add(account,od,user_info,((const char *)(dgettext("pidgin","Address"))),(info -> workaddr));
    oscar_user_info_convert_and_add(account,od,user_info,((const char *)(dgettext("pidgin","City"))),(info -> workcity));
    oscar_user_info_convert_and_add(account,od,user_info,((const char *)(dgettext("pidgin","State"))),(info -> workstate));
    oscar_user_info_convert_and_add(account,od,user_info,((const char *)(dgettext("pidgin","Zip Code"))),(info -> workzip));
  }
  if ((((((info -> workcompany) != 0) && ((info -> workcompany)[0] != 0)) || (((info -> workdivision) != 0) && ((info -> workdivision)[0] != 0))) || (((info -> workposition) != 0) && ((info -> workposition)[0] != 0))) || (((info -> workwebpage) != 0) && ((info -> workwebpage)[0] != 0))) {
    purple_notify_user_info_add_section_header(user_info,((const char *)(dgettext("pidgin","Work Information"))));
    oscar_user_info_convert_and_add(account,od,user_info,((const char *)(dgettext("pidgin","Company"))),(info -> workcompany));
    oscar_user_info_convert_and_add(account,od,user_info,((const char *)(dgettext("pidgin","Division"))),(info -> workdivision));
    oscar_user_info_convert_and_add(account,od,user_info,((const char *)(dgettext("pidgin","Position"))),(info -> workposition));
    oscar_user_info_convert_and_add_hyperlink(account,od,user_info,((const char *)(dgettext("pidgin","Web Page"))),(info -> email),"");
  }
  purple_notify_userinfo(gc,who,user_info,0,0);
  purple_notify_user_info_destroy(user_info);
}

void oscar_user_info_display_aim(OscarData *od,aim_userinfo_t *userinfo)
{
  PurpleConnection *gc = (od -> gc);
  PurpleAccount *account = purple_connection_get_account(gc);
  PurpleNotifyUserInfo *user_info = purple_notify_user_info_new();
  gchar *tmp = (gchar *)((void *)0);
  gchar *info_utf8 = (gchar *)((void *)0);
  gchar *base_profile_url = (gchar *)((void *)0);
/* PurpleBuddy */
/* use_html_status */
  oscar_user_info_append_status(gc,user_info,0,userinfo,(!0));
  if ((((userinfo -> present) & 8) != 0U) && ((userinfo -> idletime) != 0)) {
    tmp = purple_str_seconds_to_string(((userinfo -> idletime) * 60));
    oscar_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Idle"))),tmp);
    g_free(tmp);
  }
  oscar_user_info_append_extra_info(gc,user_info,0,userinfo);
  if ((((userinfo -> present) & 4) != 0U) && !(oscar_util_valid_name_sms((userinfo -> bn)) != 0)) {
/* An SMS contact is always online; its Online Since value is not useful */
    time_t t = (userinfo -> onlinesince);
    oscar_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Online Since"))),purple_date_format_full((localtime((&t)))));
  }
  if (((userinfo -> present) & 2) != 0U) {
    time_t t = (userinfo -> membersince);
    oscar_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Member Since"))),purple_date_format_full((localtime((&t)))));
  }
  if ((userinfo -> capabilities) != 0) {
    tmp = oscar_caps_to_string((userinfo -> capabilities));
    oscar_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Capabilities"))),tmp);
    g_free(tmp);
  }
/* Info */
  if ((((userinfo -> info_len) > 0) && ((userinfo -> info) != ((char *)((void *)0)))) && ((userinfo -> info_encoding) != ((char *)((void *)0)))) {
    info_utf8 = oscar_encoding_to_utf8((userinfo -> info_encoding),(userinfo -> info),(userinfo -> info_len));
    tmp = oscar_util_format_string(info_utf8,purple_account_get_username(account));
    purple_notify_user_info_add_section_break(user_info);
    oscar_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Profile"))),tmp);
    g_free(tmp);
    g_free(info_utf8);
  }
  purple_notify_user_info_add_section_break(user_info);
  base_profile_url = ((oscar_util_valid_name_icq((userinfo -> bn)) != 0)?"http://www.icq.com/people" : "http://profiles.aim.com");
  tmp = g_strdup_printf("<a href=\"%s/%s\">%s</a>",base_profile_url,purple_normalize(account,(userinfo -> bn)),((const char *)(dgettext("pidgin","View web profile"))));
  purple_notify_user_info_add_pair(user_info,0,tmp);
  g_free(tmp);
  purple_notify_userinfo(gc,(userinfo -> bn),user_info,0,0);
  purple_notify_user_info_destroy(user_info);
}
