/*
 * Purple's oscar protocol plugin
 * This file is the legal property of its developers.
 * Please see the AUTHORS file distributed alongside this file.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
*/
/*
 * A little bit of this
 * A little bit of that
 * It started with a kiss
 * Now we're up to bat
 */
#include "oscar.h"
#include "core.h"
#include <ctype.h>
#ifdef _WIN32
#include "win32dep.h"
#endif
static const char *const msgerrreason[] = {("Invalid error"), ("Invalid SNAC"), ("Server rate limit exceeded"), ("Client rate limit exceeded"), ("Not logged in"), ("Service unavailable"), ("Service not defined"), ("Obsolete SNAC"), ("Not supported by host"), ("Not supported by client"), ("Refused by client"), ("Reply too big"), ("Responses lost"), ("Request denied"), ("Busted SNAC payload"), ("Insufficient rights"), ("In local permit/deny"), ("Warning level too high (sender)"), ("Warning level too high (receiver)"), ("User temporarily unavailable"), ("No match"), ("List overflow"), ("Request ambiguous"), ("Queue full"), ("Not while on AOL")};
static const int msgerrreasonlen = (sizeof(msgerrreason) / sizeof(msgerrreason[0]));

const char *oscar_get_msgerr_reason(size_t reason)
{
  return (reason < msgerrreasonlen)?((const char *)(dgettext("pidgin",msgerrreason[reason]))) : ((const char *)(dgettext("pidgin","Unknown reason")));
}

int oscar_get_ui_info_int(const char *str,int default_value)
{
  GHashTable *ui_info;
  ui_info = purple_core_get_ui_info();
  if (ui_info != ((GHashTable *)((void *)0))) {
    gpointer value;
    if (g_hash_table_lookup_extended(ui_info,str,0,&value) != 0) 
      return (gint )((glong )value);
  }
  return default_value;
}

const char *oscar_get_ui_info_string(const char *str,const char *default_value)
{
  GHashTable *ui_info;
  const char *value = (const char *)((void *)0);
  ui_info = purple_core_get_ui_info();
  if (ui_info != ((GHashTable *)((void *)0))) 
    value = (g_hash_table_lookup(ui_info,str));
  if (value == ((const char *)((void *)0))) 
    value = default_value;
  return value;
}

gchar *oscar_get_clientstring()
{
  const char *name;
  const char *version;
  name = oscar_get_ui_info_string("name","Purple");
  version = oscar_get_ui_info_string("version","2.10.9");
  return g_strdup_printf("%s/%s",name,version);;
}
/**
 * Calculate the checksum of a given icon.
 */

guint16 aimutil_iconsum(const guint8 *buf,int buflen)
{
  guint32 sum;
  int i;
  for (((i = 0) , (sum = 0)); (i + 1) < buflen; i += 2) 
    sum += ((buf[i + 1] << 8) + buf[i]);
  if (i < buflen) 
    sum += buf[i];
  sum = (((sum & 0xffff0000) >> 16) + (sum & 0x0000ffff));
  return sum;
}
/**
 * Check if the given name is a valid AIM username.
 * Example: BobDole
 * Example: Henry_Ford@mac.com
 * Example: 1KrazyKat@example.com
 *
 * @return TRUE if the name is valid, FALSE if not.
 */

static gboolean oscar_util_valid_name_aim(const char *name)
{
  int i;
  if (purple_email_is_valid(name) != 0) 
    return (!0);
/* Normal AIM usernames can't start with a number, period or underscore */
  if ((( *__ctype_b_loc())[(int )name[0]] & ((unsigned short )_ISalnum)) == 0) 
    return 0;
  for (i = 0; name[i] != 0; i++) {
    if (((!((( *__ctype_b_loc())[(int )name[i]] & ((unsigned short )_ISalnum)) != 0) && (name[i] != 32)) && (name[i] != '.')) && (name[i] != '_')) 
      return 0;
  }
  return (!0);
}
/**
 * Check if the given name is a valid ICQ username.
 * Example: 1234567
 *
 * @return TRUE if the name is valid, FALSE if not.
 */

gboolean oscar_util_valid_name_icq(const char *name)
{
  int i;
  for (i = 0; name[i] != 0; i++) {
    if (!((( *__ctype_b_loc())[(int )name[i]] & ((unsigned short )_ISdigit)) != 0)) 
      return 0;
  }
  return (!0);
}
/**
 * Check if the given name is a valid SMS username.
 * Example: +19195551234
 *
 * @return TRUE if the name is valid, FALSE if not.
 */

gboolean oscar_util_valid_name_sms(const char *name)
{
  int i;
  if (name[0] != '+') 
    return 0;
  for (i = 1; name[i] != 0; i++) {
    if (!((( *__ctype_b_loc())[(int )name[i]] & ((unsigned short )_ISdigit)) != 0)) 
      return 0;
  }
  return (!0);
}
/**
 * Check if the given name is a valid oscar username.
 *
 * @return TRUE if the name is valid, FALSE if not.
 */

gboolean oscar_util_valid_name(const char *name)
{
  if ((name == ((const char *)((void *)0))) || (( *name) == 0)) 
    return 0;
  return ((oscar_util_valid_name_icq(name) != 0) || (oscar_util_valid_name_sms(name) != 0)) || (oscar_util_valid_name_aim(name) != 0);
}
/**
 * This takes two names and compares them using the rules
 * on usernames for AIM/AOL.  Mainly, this means case and space
 * insensitivity (all case differences and spacing differences are
 * ignored, with the exception that usernames can not start with
 * a space).
 *
 * @return 0 if equal, non-0 if different
 */
/* TODO: Do something different for email addresses. */

int oscar_util_name_compare(const char *name1,const char *name2)
{
  if ((name1 == ((const char *)((void *)0))) || (name2 == ((const char *)((void *)0)))) 
    return -1;
  do {
    while(( *name2) == 32)
      name2++;
    while(( *name1) == 32)
      name1++;
    if (toupper(( *name1)) != toupper(( *name2))) 
      return 1;
  }while (((( *name1) != 0) && (name1++ != 0)) && (name2++ != 0));
  return 0;
}
/**
 * Looks for %n, %d, or %t in a string, and replaces them with the
 * specified name, date, and time, respectively.
 *
 * @param str  The string that may contain the special variables.
 * @param name The sender name.
 *
 * @return A newly allocated string where the special variables are
 *         expanded.  This should be g_free'd by the caller.
 */

gchar *oscar_util_format_string(const char *str,const char *name)
{
  char *c;
  GString *cpy;
  time_t t;
  struct tm *tme;
  do {
    if (str != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"str != NULL");
      return 0;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return 0;
    };
  }while (0);
/* Create an empty GString that is hopefully big enough for most messages */
  cpy = g_string_sized_new(1024);
  t = time(0);
  tme = localtime((&t));
  c = ((char *)str);
  while(( *c) != 0){
    switch(( *c)){
      case '%':
{
        if (c[1] != 0) {
          switch(c[1]){
            case 'n':
{
/* append name */
              g_string_append(cpy,name);
              c++;
              break; 
            }
            case 'd':
{
/* append date */
              g_string_append(cpy,purple_date_format_short(tme));
              c++;
              break; 
            }
            case 't':
{
/* append time */
              g_string_append(cpy,purple_time_format(tme));
              c++;
              break; 
            }
            default:
{
              g_string_append_c_inline(cpy, *c);
            }
          }
        }
        else {
          g_string_append_c_inline(cpy, *c);
        }
        break; 
      }
      default:
{
        g_string_append_c_inline(cpy, *c);
      }
    }
    c++;
  }
  return g_string_free(cpy,0);
}

gchar *oscar_format_buddies(GSList *buddies,const gchar *no_buddies_message)
{
  GSList *cur;
  GString *result;
  if (!(buddies != 0)) {
    return g_strdup_printf("<i>%s</i>",no_buddies_message);
  }
  result = g_string_new("");
  for (cur = buddies; cur != ((GSList *)((void *)0)); cur = (cur -> next)) {
    PurpleBuddy *buddy = (cur -> data);
    const gchar *bname = purple_buddy_get_name(buddy);
    const gchar *alias = purple_buddy_get_alias_only(buddy);
    g_string_append(result,bname);
    if (alias != 0) {
      g_string_append_printf(result," (%s)",alias);
    }
    g_string_append(result,"<br>");
  }
  return g_string_free(result,0);
}
