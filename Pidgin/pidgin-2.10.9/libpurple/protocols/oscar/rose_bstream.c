/*
 * Purple's oscar protocol plugin
 * This file is the legal property of its developers.
 * Please see the AUTHORS file distributed alongside this file.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
*/
/*
 * This file contains all functions needed to use bstreams.
 */
#include "oscar.h"

int byte_stream_new(ByteStream *bs,size_t len)
{
  if (bs == ((ByteStream *)((void *)0))) 
    return -1;
  return byte_stream_init(bs,(g_malloc(len)),len);
}

int byte_stream_init(ByteStream *bs,guint8 *data,size_t len)
{
  if (bs == ((ByteStream *)((void *)0))) 
    return -1;
  bs -> data = data;
  bs -> len = len;
  bs -> offset = 0;
  return 0;
}

void byte_stream_destroy(ByteStream *bs)
{
  g_free((bs -> data));
}

int byte_stream_bytes_left(ByteStream *bs)
{
  return ((bs -> len) - (bs -> offset));
}

int byte_stream_curpos(ByteStream *bs)
{
  return (bs -> offset);
}

int byte_stream_setpos(ByteStream *bs,size_t off)
{
  do {
    if (off <= (bs -> len)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"off <= bs->len");
      return -1;
    };
  }while (0);
  bs -> offset = off;
  return off;
}

void byte_stream_rewind(ByteStream *bs)
{
  byte_stream_setpos(bs,0);
}
/*
 * N can be negative, which can be used for going backwards
 * in a bstream.
 */

int byte_stream_advance(ByteStream *bs,int n)
{
  do {
    if ((byte_stream_curpos(bs) + n) >= 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"byte_stream_curpos(bs) + n >= 0");
      return 0;
    };
  }while (0);
  do {
    if (n <= byte_stream_bytes_left(bs)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"n <= byte_stream_bytes_left(bs)");
      return 0;
    };
  }while (0);
  bs -> offset += n;
  return n;
}

guint8 byte_stream_get8(ByteStream *bs)
{
  do {
    if (byte_stream_bytes_left(bs) >= 1) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"byte_stream_bytes_left(bs) >= 1");
      return 0;
    };
  }while (0);
  bs -> offset++;
  return (( *(((bs -> data) + (bs -> offset)) - 1)) & 0xff);
}

guint16 byte_stream_get16(ByteStream *bs)
{
  do {
    if (byte_stream_bytes_left(bs) >= 2) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"byte_stream_bytes_left(bs) >= 2");
      return 0;
    };
  }while (0);
  bs -> offset += 2;
  return (((( *(((bs -> data) + (bs -> offset)) - 2)) << 8) & 0xff00) + ((((bs -> data) + (bs -> offset)) - 2)[1] & 0xff));
}

guint32 byte_stream_get32(ByteStream *bs)
{
  do {
    if (byte_stream_bytes_left(bs) >= 4) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"byte_stream_bytes_left(bs) >= 4");
      return 0;
    };
  }while (0);
  bs -> offset += 4;
  return ((((( *(((bs -> data) + (bs -> offset)) - 4)) << 24) & 0xff000000) + (((((bs -> data) + (bs -> offset)) - 4)[1] << 16) & 0x00ff0000)) + (((((bs -> data) + (bs -> offset)) - 4)[2] << 8) & 0xff00)) + ((((bs -> data) + (bs -> offset)) - 4)[3] & 0xff);
}

guint8 byte_stream_getle8(ByteStream *bs)
{
  do {
    if (byte_stream_bytes_left(bs) >= 1) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"byte_stream_bytes_left(bs) >= 1");
      return 0;
    };
  }while (0);
  bs -> offset++;
  return (( *(((bs -> data) + (bs -> offset)) - 1)) & 0xff);
}

guint16 byte_stream_getle16(ByteStream *bs)
{
  do {
    if (byte_stream_bytes_left(bs) >= 2) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"byte_stream_bytes_left(bs) >= 2");
      return 0;
    };
  }while (0);
  bs -> offset += 2;
  return ((((((bs -> data) + (bs -> offset)) - 2)[0] << 0) & 0xff) + (((((bs -> data) + (bs -> offset)) - 2)[1] << 8) & 0xff00));
}

guint32 byte_stream_getle32(ByteStream *bs)
{
  do {
    if (byte_stream_bytes_left(bs) >= 4) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"byte_stream_bytes_left(bs) >= 4");
      return 0;
    };
  }while (0);
  bs -> offset += 4;
  return (((((((bs -> data) + (bs -> offset)) - 4)[0] << 0) & 0xff) + (((((bs -> data) + (bs -> offset)) - 4)[1] << 8) & 0xff00)) + (((((bs -> data) + (bs -> offset)) - 4)[2] << 16) & 0x00ff0000)) + (((((bs -> data) + (bs -> offset)) - 4)[3] << 24) & 0xff000000);
}

static void byte_stream_getrawbuf_nocheck(ByteStream *bs,guint8 *buf,size_t len)
{
  memcpy(buf,((bs -> data) + (bs -> offset)),len);
  bs -> offset += len;
}

int byte_stream_getrawbuf(ByteStream *bs,guint8 *buf,size_t len)
{
  do {
    if ((byte_stream_bytes_left(bs)) >= len) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"byte_stream_bytes_left(bs) >= len");
      return 0;
    };
  }while (0);
  byte_stream_getrawbuf_nocheck(bs,buf,len);
  return len;
}

guint8 *byte_stream_getraw(ByteStream *bs,size_t len)
{
  guint8 *ob;
  do {
    if ((byte_stream_bytes_left(bs)) >= len) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"byte_stream_bytes_left(bs) >= len");
      return 0;
    };
  }while (0);
  ob = (g_malloc(len));
  byte_stream_getrawbuf_nocheck(bs,ob,len);
  return ob;
}

char *byte_stream_getstr(ByteStream *bs,size_t len)
{
  char *ob;
  do {
    if ((byte_stream_bytes_left(bs)) >= len) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"byte_stream_bytes_left(bs) >= len");
      return 0;
    };
  }while (0);
  ob = (g_malloc((len + 1)));
  byte_stream_getrawbuf_nocheck(bs,((guint8 *)ob),len);
  ob[len] = 0;
  return ob;
}

int byte_stream_put8(ByteStream *bs,guint8 v)
{
  do {
    if (byte_stream_bytes_left(bs) >= 1) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"byte_stream_bytes_left(bs) >= 1");
      return 0;
    };
  }while (0);
  bs -> offset += ((( *((bs -> data) + (bs -> offset)) = (((guint8 )v) & 0xff)) , 1));
  return 1;
}

int byte_stream_put16(ByteStream *bs,guint16 v)
{
  do {
    if (byte_stream_bytes_left(bs) >= 2) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"byte_stream_bytes_left(bs) >= 2");
      return 0;
    };
  }while (0);
  bs -> offset += ((((( *((bs -> data) + (bs -> offset)) = (((guint8 )(v >> 8)) & 0xff)) , ( *(((bs -> data) + (bs -> offset)) + 1) = (((guint8 )v) & 0xff)))) , 2));
  return 2;
}

int byte_stream_put32(ByteStream *bs,guint32 v)
{
  do {
    if (byte_stream_bytes_left(bs) >= 4) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"byte_stream_bytes_left(bs) >= 4");
      return 0;
    };
  }while (0);
  bs -> offset += ((((((((( *((bs -> data) + (bs -> offset)) = (((guint8 )(v >> 24)) & 0xff)) , ( *(((bs -> data) + (bs -> offset)) + 1) = (((guint8 )(v >> 16)) & 0xff)))) , ( *(((bs -> data) + (bs -> offset)) + 2) = (((guint8 )(v >> 8)) & 0xff)))) , ( *(((bs -> data) + (bs -> offset)) + 3) = (((guint8 )v) & 0xff)))) , 4));
  return 1;
}

int byte_stream_putle8(ByteStream *bs,guint8 v)
{
  do {
    if (byte_stream_bytes_left(bs) >= 1) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"byte_stream_bytes_left(bs) >= 1");
      return 0;
    };
  }while (0);
  bs -> offset += ((( *((bs -> data) + (bs -> offset)) = (((guint8 )v) & 0xff)) , 1));
  return 1;
}

int byte_stream_putle16(ByteStream *bs,guint16 v)
{
  do {
    if (byte_stream_bytes_left(bs) >= 2) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"byte_stream_bytes_left(bs) >= 2");
      return 0;
    };
  }while (0);
  bs -> offset += ((((( *(((bs -> data) + (bs -> offset)) + 0) = (((guint8 )(v >> 0)) & 0xff)) , ( *(((bs -> data) + (bs -> offset)) + 1) = (((guint8 )(v >> 8)) & 0xff)))) , 2));
  return 2;
}

int byte_stream_putle32(ByteStream *bs,guint32 v)
{
  do {
    if (byte_stream_bytes_left(bs) >= 4) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"byte_stream_bytes_left(bs) >= 4");
      return 0;
    };
  }while (0);
  bs -> offset += ((((((((( *(((bs -> data) + (bs -> offset)) + 0) = (((guint8 )(v >> 0)) & 0xff)) , ( *(((bs -> data) + (bs -> offset)) + 1) = (((guint8 )(v >> 8)) & 0xff)))) , ( *(((bs -> data) + (bs -> offset)) + 2) = (((guint8 )(v >> 16)) & 0xff)))) , ( *(((bs -> data) + (bs -> offset)) + 3) = (((guint8 )(v >> 24)) & 0xff)))) , 4));
  return 1;
}

int byte_stream_putraw(ByteStream *bs,const guint8 *v,size_t len)
{
  do {
    if ((byte_stream_bytes_left(bs)) >= len) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"byte_stream_bytes_left(bs) >= len");
      return 0;
    };
  }while (0);
  memcpy(((bs -> data) + (bs -> offset)),v,len);
  bs -> offset += len;
  return len;
}

int byte_stream_putstr(ByteStream *bs,const char *str)
{
  return byte_stream_putraw(bs,((guint8 *)str),strlen(str));
}

int byte_stream_putbs(ByteStream *bs,ByteStream *srcbs,size_t len)
{
  do {
    if ((byte_stream_bytes_left(srcbs)) >= len) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"byte_stream_bytes_left(srcbs) >= len");
      return 0;
    };
  }while (0);
  do {
    if ((byte_stream_bytes_left(bs)) >= len) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"byte_stream_bytes_left(bs) >= len");
      return 0;
    };
  }while (0);
  memcpy(((bs -> data) + (bs -> offset)),((srcbs -> data) + (srcbs -> offset)),len);
  bs -> offset += len;
  srcbs -> offset += len;
  return len;
}

int byte_stream_putuid(ByteStream *bs,OscarData *od)
{
  PurpleAccount *account;
  account = purple_connection_get_account((od -> gc));
  return byte_stream_putle32(bs,(atoi(purple_account_get_username(account))));
}

void byte_stream_put_bart_asset(ByteStream *bs,guint16 type,ByteStream *data)
{
  byte_stream_put16(bs,type);
  if ((data != ((ByteStream *)((void *)0))) && ((data -> len) > 0)) {
/* Flags. 0x04 means "this asset has data attached to it" */
/* Flags */
    byte_stream_put8(bs,4);
/* Length */
    byte_stream_put8(bs,(data -> len));
    byte_stream_rewind(data);
/* Data */
    byte_stream_putbs(bs,data,(data -> len));
  }
  else {
/* No flags */
    byte_stream_put8(bs,0);
/* Length */
    byte_stream_put8(bs,0);
/* No data */
  }
}

void byte_stream_put_bart_asset_str(ByteStream *bs,guint16 type,const char *datastr)
{
  ByteStream data;
  size_t len = (datastr != ((const char *)((void *)0)))?strlen(datastr) : 0;
  if (len > 0) {
    byte_stream_new(&data,((2 + len) + 2));
/* Length */
    byte_stream_put16(&data,len);
/* String */
    byte_stream_putstr(&data,datastr);
/* Unknown */
    byte_stream_put16(&data,0);
    byte_stream_put_bart_asset(bs,type,&data);
    byte_stream_destroy(&data);
  }
  else {
    byte_stream_put_bart_asset(bs,type,0);
  }
}
