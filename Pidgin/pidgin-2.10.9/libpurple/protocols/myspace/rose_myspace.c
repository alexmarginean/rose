/**
 * MySpaceIM Protocol Plugin
 *
 * \author Jeff Connelly
 *
 * Copyright (C) 2007, Jeff Connelly <jeff2@soc.pidgin.im>
 *
 * Based on Purple's "C Plugin HOWTO" hello world example.
 *
 * Code also drawn from mockprpl:
 *  http://snarfed.org/space/purple+mock+protocol+plugin
 *  Copyright (C) 2004-2007, Ryan Barrett <mockprpl@ryanb.org>
 *
 * and some constructs also based on existing Purple plugins, which are:
 *   Copyright (C) 2003, Robbert Haarman <purple@inglorion.net>
 *   Copyright (C) 2003, Ethan Blanton <eblanton@cs.purdue.edu>
 *   Copyright (C) 2000-2003, Rob Flynn <rob@tgflinux.com>
 *   Copyright (C) 1998-1999, Mark Spencer <markster@marko.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#define PURPLE_PLUGIN
#include "myspace.h"
#include "privacy.h"
static void msim_set_status(PurpleAccount *account,PurpleStatus *status);
static void msim_set_idle(PurpleConnection *gc,int time);
/**
 * Perform actual postprocessing on a message, adding userid as specified.
 *
 * @param msg The message to postprocess.
 * @param uid_before Name of field where to insert new field before, or NULL for end.
 * @param uid_field_name Name of field to add uid to.
 * @param uid The userid to insert.
 *
 * If the field named by uid_field_name already exists, then its string contents will
 * be used for the field, except "<uid>" will be replaced by the userid.
 *
 * If the field named by uid_field_name does not exist, it will be added before the
 * field named by uid_before, as an integer, with the userid.
 *
 * Does not handle sending, or scheduling userid lookup. For that, see msim_postprocess_outgoing().
 */

static GList *msim_do_postprocessing(GList *msg,const gchar *uid_before,const gchar *uid_field_name,guint uid)
{
  MsimMessageElement *elem;
/* First, check - if the field already exists, replace <uid> within it */
  if ((elem = msim_msg_get(msg,uid_field_name)) != ((MsimMessageElement *)((void *)0))) {
    gchar *fmt_string;
    gchar *uid_str;
    gchar *new_str;
/* Get the packed element, flattening it. This allows <uid> to be
		 * replaced within nested data structures, since the replacement is done
		 * on the linear, packed data, not on a complicated data structure.
		 *
		 * For example, if the field was originally a dictionary or a list, you
		 * would have to iterate over all the items in it to see what needs to
		 * be replaced. But by packing it first, the <uid> marker is easily replaced
		 * just by a string replacement.
		 */
    fmt_string = msim_msg_pack_element_data(elem);
    uid_str = g_strdup_printf("%d",uid);
    new_str = purple_strreplace(fmt_string,"<uid>",uid_str);
    g_free(uid_str);
    g_free(fmt_string);
/* Free the old element data */
    msim_msg_free_element_data((elem -> data));
/* Replace it with our new data */
    elem -> data = new_str;
    elem -> type = '-';
  }
  else {
/* Otherwise, insert new field into outgoing message. */
    msg = msim_msg_insert_before(msg,uid_before,uid_field_name,'i',((gpointer )((gulong )uid)));
  }
  return msg;
}
/**
 * Callback for msim_postprocess_outgoing() to add a userid to a message, and send it (once receiving userid).
 *
 * @param session
 * @param userinfo The user information reply message, containing the user ID
 * @param data The message to postprocess and send.
 *
 * The data message should contain these fields:
 *
 *  _uid_field_name: string, name of field to add with userid from userinfo message
 *  _uid_before: string, name of field before field to insert, or NULL for end
 */

static void msim_postprocess_outgoing_cb(MsimSession *session,const GList *userinfo,gpointer data)
{
  gchar *uid_field_name;
  gchar *uid_before;
  gchar *username;
  guint uid;
  GList *msg;
  GList *body;
  msg = ((GList *)data);
/* Obtain userid from userinfo message. */
  body = msim_msg_get_dictionary(userinfo,"body");
  do {
    if (body != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"body != NULL");
      return ;
    };
  }while (0);
  uid = msim_msg_get_integer(body,"UserID");
  msim_msg_free(body);
  username = msim_msg_get_string(msg,"_username");
  if (!(uid != 0U)) {
    gchar *msg;
    msg = g_strdup_printf(((const char *)(dgettext("pidgin","No such user: %s"))),username);
    if (!(purple_conv_present_error(username,(session -> account),msg) != 0)) {
      purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","User lookup"))),msg,0,0);
    }
    g_free(msg);
    g_free(username);
/* TODO: free
		 * msim_msg_free(msg);
		 */
    return ;
  }
  uid_field_name = msim_msg_get_string(msg,"_uid_field_name");
  uid_before = msim_msg_get_string(msg,"_uid_before");
  msg = msim_do_postprocessing(msg,uid_before,uid_field_name,uid);
/* Send */
  if (!(msim_msg_send(session,msg) != 0)) {
    msim_msg_dump("msim_postprocess_outgoing_cb: sending failed for message: %s\n",msg);
  }
/* Free field names AFTER sending message, because MsimMessage does NOT copy
	 * field names - instead, treats them as static strings (which they usually are).
	 */
  g_free(uid_field_name);
  g_free(uid_before);
  g_free(username);
/* TODO: free
	 * msim_msg_free(msg);
	 */
}
/**
 * Postprocess and send a message.
 *
 * @param session
 * @param msg Message to postprocess. Will NOT be freed.
 * @param username Username to resolve. Assumed to be a static string (will not be freed or copied).
 * @param uid_field_name Name of new field to add, containing uid of username. Static string.
 * @param uid_before Name of existing field to insert username field before. Static string.
 *
 * @return TRUE if successful.
 */

static gboolean msim_postprocess_outgoing(MsimSession *session,GList *msg,const gchar *username,const gchar *uid_field_name,const gchar *uid_before)
{
  PurpleBuddy *buddy;
  guint uid;
  gboolean rc;
  do {
    if (msg != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg != NULL");
      return 0;
    };
  }while (0);
/* Store information for msim_postprocess_outgoing_cb(). */
  msg = msim_msg_append(msg,"_username",'s',(g_strdup(username)));
  msg = msim_msg_append(msg,"_uid_field_name",'s',(g_strdup(uid_field_name)));
  msg = msim_msg_append(msg,"_uid_before",'s',(g_strdup(uid_before)));
/* First, try the most obvious. If numeric userid is given, use that directly. */
  if (msim_is_userid(username) != 0) {
    uid = (atol(username));
  }
  else {
/* Next, see if on buddy list and know uid. */
    buddy = purple_find_buddy((session -> account),username);
    if (buddy != 0) {
      uid = (purple_blist_node_get_int(((PurpleBlistNode *)buddy),"UserID"));
    }
    else {
      uid = 0;
    }
    if (!(buddy != 0) || !(uid != 0U)) {
/* Don't have uid offhand - need to ask for it, and wait until hear back before sending. */
      purple_debug_info("msim",">>> msim_postprocess_outgoing: couldn\'t find username %s in blist\n",((username != 0)?username : "(NULL)"));
      msim_lookup_user(session,username,msim_postprocess_outgoing_cb,(msim_msg_clone(msg)));
/* not sure of status yet - haven't sent! */
      return (!0);
    }
  }
/* Already have uid, postprocess and send msg immediately. */
  purple_debug_info("msim","msim_postprocess_outgoing: found username %s has uid %d\n",((username != 0)?username : "(NULL)"),uid);
  msg = msim_do_postprocessing(msg,uid_before,uid_field_name,uid);
  rc = msim_msg_send(session,msg);
/* TODO: free
	 * msim_msg_free(msg);
	 */
  return rc;
}
/**
 * Send a buddy message of a given type.
 *
 * @param session
 * @param who Username to send message to.
 * @param text Message text to send. Not freed; will be copied.
 * @param type A MSIM_BM_* constant.
 *
 * @return TRUE if success, FALSE if fail.
 *
 * Buddy messages ('bm') include instant messages, action messages, status messages, etc.
 */

gboolean msim_send_bm(MsimSession *session,const gchar *who,const gchar *text,int type)
{
  gboolean rc;
  GList *msg;
  const gchar *from_username;
  do {
    if (who != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"who != NULL");
      return 0;
    };
  }while (0);
  do {
    if (text != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"text != NULL");
      return 0;
    };
  }while (0);
  from_username = ( *(session -> account)).username;
  do {
    if (from_username != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"from_username != NULL");
      return 0;
    };
  }while (0);
  purple_debug_info("msim","sending %d message from %s to %s: %s\n",type,from_username,who,text);
  msg = msim_msg_new("bm",'i',((gpointer )((gulong )type)),"sesskey",'i',((gpointer )((gulong )(session -> sesskey))),"cv",'i',((gpointer )((gpointer )((gulong )697))),"msg",'s',g_strdup(text),((void *)((void *)0)));
/* 't' will be inserted here */
  rc = msim_postprocess_outgoing(session,msg,who,"t","cv");
  msim_msg_free(msg);
  return rc;
}
/**
 * Lookup a username by userid, from buddy list.
 *
 * @param wanted_uid
 *
 * @return Username of wanted_uid, if on blist, or NULL.
 *         This is a static string, so don't free it. Copy it if needed.
 *
 */

static const gchar *msim_uid2username_from_blist(PurpleAccount *account,guint wanted_uid)
{
  GSList *buddies;
  GSList *cur;
  const gchar *ret;
  buddies = purple_find_buddies(account,0);
  if (!(buddies != 0)) {
    purple_debug_info("msim","msim_uid2username_from_blist: no buddies\?\n");
    return 0;
  }
  ret = ((const gchar *)((void *)0));
{
    for (cur = buddies; cur != ((GSList *)((void *)0)); cur = ((cur != 0)?( *((GSList *)cur)).next : ((struct _GSList *)((void *)0)))) {
      PurpleBuddy *buddy;
      guint uid;
      const gchar *name;
/* See finch/gnthistory.c */
      buddy = (cur -> data);
      uid = (purple_blist_node_get_int(((PurpleBlistNode *)buddy),"UserID"));
      name = purple_buddy_get_name(buddy);
      if (uid == wanted_uid) {
        ret = name;
        break; 
      }
    }
  }
  g_slist_free(buddies);
  return ret;
}
/**
 * Setup a callback, to be called when a reply is received with the returned rid.
 *
 * @param cb The callback, an MSIM_USER_LOOKUP_CB.
 * @param data Arbitrary user data to be passed to callback (probably an MsimMessage *).
 *
 * @return The request/reply ID, used to link replies with requests, or -1.
 *          Put the rid in your request, 'rid' field.
 *
 * TODO: Make more generic and more specific:
 * 1) MSIM_USER_LOOKUP_CB - make it for PERSIST_REPLY, not just user lookup
 * 2) data - make it an MsimMessage?
 */

guint msim_new_reply_callback(MsimSession *session,MSIM_USER_LOOKUP_CB cb,gpointer data)
{
  guint rid;
  rid = session -> next_rid++;
  g_hash_table_insert((session -> user_lookup_cb),((gpointer )((gulong )rid)),cb);
  g_hash_table_insert((session -> user_lookup_cb_data),((gpointer )((gulong )rid)),data);
  return rid;
}
/**
 * Return the icon name for a buddy and account.
 *
 * @param acct The account to find the icon for, or NULL for protocol icon.
 * @param buddy The buddy to find the icon for, or NULL for the account icon.
 *
 * @return The base icon name string.
 */

static const gchar *msim_list_icon(PurpleAccount *acct,PurpleBuddy *buddy)
{
/* Use a MySpace icon submitted by hbons at
	 * http://developer.pidgin.im/wiki/MySpaceIM. */
  return "myspace";
}
/**
 * Obtain the status text for a buddy.
 *
 * @param buddy The buddy to obtain status text for.
 *
 * @return Status text, or NULL if error. Caller g_free()'s.
 */

static char *msim_status_text(PurpleBuddy *buddy)
{
  MsimUser *user;
  const gchar *display_name = (const gchar *)((void *)0);
  const gchar *headline = (const gchar *)((void *)0);
  PurpleAccount *account;
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return 0;
    };
  }while (0);
  account = purple_buddy_get_account(buddy);
  user = msim_get_user_from_buddy(buddy,0);
  if (user != ((MsimUser *)((void *)0))) {
/* Retrieve display name and/or headline, depending on user preference. */
    if (purple_account_get_bool(account,"show_headline",(!0)) != 0) {
      headline = (user -> headline);
    }
    if (purple_account_get_bool(account,"show_display_name",0) != 0) {
      display_name = (user -> display_name);
    }
  }
/* Return appropriate combination of display name and/or headline, or neither. */
  if ((display_name != 0) && (headline != 0)) {
    return g_strconcat(display_name," ",headline,((void *)((void *)0)));
  }
  else if (display_name != 0) {
    return g_strdup(display_name);
  }
  else if (headline != 0) {
    return g_strdup(headline);
  }
  return 0;
}
/**
 * Obtain the tooltip text for a buddy.
 *
 * @param buddy Buddy to obtain tooltip text on.
 * @param user_info Variable modified to have the tooltip text.
 * @param full TRUE if should obtain full tooltip text.
 */

static void msim_tooltip_text(PurpleBuddy *buddy,PurpleNotifyUserInfo *user_info,gboolean full)
{
  MsimUser *user;
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return ;
    };
  }while (0);
  do {
    if (user_info != ((PurpleNotifyUserInfo *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user_info != NULL");
      return ;
    };
  }while (0);
  user = msim_get_user_from_buddy(buddy,(!0));
  if (((buddy != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0)) {
    MsimSession *session;
    PurpleAccount *account = purple_buddy_get_account(buddy);
    PurpleConnection *gc = purple_account_get_connection(account);
    session = ((MsimSession *)(gc -> proto_data));
/* TODO: if (full), do something different? */
/* TODO: request information? have to figure out how to do
		 * the asynchronous lookup like oscar does (tooltip shows
		 * 'retrieving...' if not yet available, then changes when it is).
		 *
		 * Right now, only show what we have on hand.
		 */
/* Show abbreviated user info. */
    msim_append_user_info(session,user_info,user,0);
  }
}
/**
 * Get possible user status types. Based on mockprpl.
 *
 * @return GList of status types.
 */

static GList *msim_status_types(PurpleAccount *acct)
{
  GList *types;
  PurpleStatusType *status;
  purple_debug_info("myspace","returning status types\n");
  types = ((GList *)((void *)0));
/* Statuses are almost all the same. Define a macro to reduce code repetition. */
#define _MSIM_ADD_NEW_STATUS(prim) status =                         \
	purple_status_type_new_with_attrs(                          \
	prim,   /* PurpleStatusPrimitive */                         \
	NULL,   /* id - use default */                              \
	NULL,   /* name - use default */                            \
	TRUE,   /* saveable */                                      \
	TRUE,   /* user_settable */                                 \
	FALSE,  /* not independent */                               \
	                                                            \
	/* Attributes - each status can have a message. */          \
	"message",                                                  \
	_("Message"),                                               \
	purple_value_new(PURPLE_TYPE_STRING),                       \
	NULL);                                                      \
	                                                            \
	                                                            \
	types = g_list_append(types, status)
  status = purple_status_type_new_with_attrs(PURPLE_STATUS_AVAILABLE,0,0,(!0),(!0),0,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  types = g_list_append(types,status);
  status = purple_status_type_new_with_attrs(PURPLE_STATUS_AWAY,0,0,(!0),(!0),0,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  types = g_list_append(types,status);
  status = purple_status_type_new_with_attrs(PURPLE_STATUS_OFFLINE,0,0,(!0),(!0),0,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  types = g_list_append(types,status);
  status = purple_status_type_new_with_attrs(PURPLE_STATUS_INVISIBLE,0,0,(!0),(!0),0,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  types = g_list_append(types,status);
/* Except tune status is different... */
  status = purple_status_type_new_with_attrs(PURPLE_STATUS_TUNE,"tune",0,0,(!0),(!0),"tune_artist",((const char *)(dgettext("pidgin","Tune Artist"))),purple_value_new(PURPLE_TYPE_STRING),"tune_title",((const char *)(dgettext("pidgin","Tune Title"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
/* primitive */
/* ID */
/* name - use default */
/* saveable */
/* should be user_settable some day */
/* independent */
  types = g_list_append(types,status);
  return types;
}
/*
 * TODO: This define is stolen from oscar.h.
 *       It's also in yahoo.h.
 *       It should be in libpurple/util.c
 */
#define msim_put32(buf, data) ( \
		(*((buf)) = (unsigned char)((data)>>24)&0xff), \
		(*((buf)+1) = (unsigned char)((data)>>16)&0xff), \
		(*((buf)+2) = (unsigned char)((data)>>8)&0xff), \
		(*((buf)+3) = (unsigned char)(data)&0xff), \
		4)
/**
 * Compute the base64'd login challenge response based on username, password, nonce, and IPs.
 *
 * @param nonce The base64 encoded nonce ('nc') field from the server.
 * @param email User's email address (used as login name).
 * @param password User's cleartext password.
 * @param response_len Will be written with response length.
 *
 * @return Binary login challenge response, ready to send to the server.
 * Must be g_free()'d when finished. NULL if error.
 */

static gchar *msim_compute_login_response(const gchar nonce[2 * 32],const gchar *email,const gchar *password,guint *response_len)
{
  PurpleCipherContext *key_context;
  PurpleCipher *sha1;
  PurpleCipherContext *rc4;
  guchar hash_pw[20UL];
  guchar key[20UL];
  gchar *password_truncated;
  gchar *password_utf16le;
  gchar *password_utf8_lc;
  GString *data;
  guchar *data_out;
  size_t data_out_len;
  gsize conv_bytes_read;
  gsize conv_bytes_written;
  GError *conv_error;
#ifdef MSIM_DEBUG_LOGIN_CHALLENGE
#endif
  do {
    if (nonce != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"nonce != NULL");
      return 0;
    };
  }while (0);
  do {
    if (email != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"email != NULL");
      return 0;
    };
  }while (0);
  do {
    if (password != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"password != NULL");
      return 0;
    };
  }while (0);
  do {
    if (response_len != ((guint *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"response_len != NULL");
      return 0;
    };
  }while (0);
/*
	 * Truncate password to 10 characters.  Their "change password"
	 * web page doesn't let you enter more than 10 characters, but you
	 * can enter more than 10 when logging in on myspace.com and they
	 * truncate it.
	 */
  password_truncated = g_strndup(password,10);
/* Convert password to lowercase (required for passwords containing
	 * uppercase characters). MySpace passwords are lowercase,
	 * see ticket #2066. */
  password_utf8_lc = g_utf8_strdown(password_truncated,(-1));
  g_free(password_truncated);
/* Convert ASCII password to UTF16 little endian */
  purple_debug_info("msim","converting password to UTF-16LE\n");
  conv_error = ((GError *)((void *)0));
  password_utf16le = g_convert(password_utf8_lc,(-1),"UTF-16LE","UTF-8",&conv_bytes_read,&conv_bytes_written,&conv_error);
  g_free(password_utf8_lc);
  if (conv_error != ((GError *)((void *)0))) {
    purple_debug_error("msim","g_convert password UTF8->UTF16LE failed: %s",(conv_error -> message));
    g_error_free(conv_error);
    return 0;
  }
/* Compute password hash */
  purple_cipher_digest_region("sha1",((guchar *)password_utf16le),conv_bytes_written,(sizeof(hash_pw)),hash_pw,0);
  g_free(password_utf16le);
#ifdef MSIM_DEBUG_LOGIN_CHALLENGE
#endif
/* key = sha1(sha1(pw) + nonce2) */
  sha1 = purple_ciphers_find_cipher("sha1");
  key_context = purple_cipher_context_new(sha1,0);
  purple_cipher_context_append(key_context,hash_pw,20);
  purple_cipher_context_append(key_context,((guchar *)(nonce + 32)),32);
  purple_cipher_context_digest(key_context,(sizeof(key)),key,0);
  purple_cipher_context_destroy(key_context);
#ifdef MSIM_DEBUG_LOGIN_CHALLENGE
#endif
  rc4 = purple_cipher_context_new_by_name("rc4",0);
/* Note: 'key' variable is 0x14 bytes (from SHA-1 hash),
	 * but only first 0x10 used for the RC4 key. */
  purple_cipher_context_set_option(rc4,"key_len",((gpointer )((gpointer )16)));
  purple_cipher_context_set_key(rc4,key);
/* rc4 encrypt:
	 * nonce1+email+IP list */
  data = g_string_new(0);
  g_string_append_len(data,nonce,32);
/* Include the null terminator */
  g_string_append_len(data,email,(strlen(email) + 1));
  while(((data -> len) % 4) != 0)
    g_string_append_c_inline(data,0xfb);
#ifdef SEND_OUR_IP_ADDRESSES
/* TODO: Obtain IPs of network interfaces instead of using this hardcoded value */
#else
  g_string_set_size(data,((data -> len) + 4));
  (((((((( *(((data -> str) + (data -> len)) - 4) = (((unsigned char )(0 >> 24)) & 0xff)) , ( *((((data -> str) + (data -> len)) - 4) + 1) = (((unsigned char )(0 >> 16)) & 0xff)))) , ( *((((data -> str) + (data -> len)) - 4) + 2) = (((unsigned char )(0 >> 8)) & 0xff)))) , ( *((((data -> str) + (data -> len)) - 4) + 3) = (((unsigned char )0) & 0xff)))) , 4);
#endif /* !SEND_OUR_IP_ADDRESSES */
  data_out = ((guchar *)(g_malloc0_n((data -> len),(sizeof(guchar )))));
  purple_cipher_context_encrypt(rc4,((const guchar *)(data -> str)),(data -> len),data_out,&data_out_len);
  purple_cipher_context_destroy(rc4);
  if (data_out_len != (data -> len)) {
    purple_debug_info("msim","msim_compute_login_response: data length mismatch: %lu != %lu\n",data_out_len,(data -> len));
  }
  g_string_free(data,(!0));
#ifdef MSIM_DEBUG_LOGIN_CHALLENGE
#endif
   *response_len = data_out_len;
  return (gchar *)data_out;
}
/**
 * Process a login challenge, sending a response.
 *
 * @param session
 * @param msg Login challenge message.
 *
 * @return TRUE if successful, FALSE if not
 */

static gboolean msim_login_challenge(MsimSession *session,GList *msg)
{
  PurpleAccount *account;
  gchar *response;
  guint response_len;
  gchar *nc;
  gsize nc_len;
  gboolean ret;
  do {
    if (msg != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg != NULL");
      return 0;
    };
  }while (0);
  do {
    if (msim_msg_get_binary(msg,"nc",&nc,&nc_len) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msim_msg_get_binary(msg, \"nc\", &nc, &nc_len)");
      return 0;
    };
  }while (0);
  account = (session -> account);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  purple_connection_update_progress((session -> gc),((const char *)(dgettext("pidgin","Reading challenge"))),1,4);
  purple_debug_info("msim","nc is %lu bytes, decoded\n",nc_len);
  if (nc_len != 64) {
    purple_debug_info("msim","bad nc length: %lx != 0x%x\n",nc_len,64);
    purple_connection_error_reason((session -> gc),PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Unexpected challenge length from server"))));
    return 0;
  }
  purple_connection_update_progress((session -> gc),((const char *)(dgettext("pidgin","Logging in"))),2,4);
  response_len = 0;
  response = msim_compute_login_response(nc,(account -> username),(account -> password),&response_len);
  g_free(nc);
  ret = msim_send(session,"login2",'i',196610,"username",'s',g_strdup((account -> username)),"response",'b',g_string_new_len(response,response_len),"clientver",'i',697,"langid",'i',1033,"imlang",'s',g_strdup("ENGLISH"),"reconn",'i',0,"status",'i',100,"id",'i',1,((void *)((void *)0)));
/* This is actually user's email address. */
/* GString will be freed in msim_msg_free() in msim_send(). */
  g_free(response);
  return ret;
}
/**
 * Process unrecognized information.
 *
 * @param session
 * @param msg An MsimMessage that was unrecognized, or NULL.
 * @param note Information on what was unrecognized, or NULL.
 */

void msim_unrecognized(MsimSession *session,GList *msg,gchar *note)
{
/* TODO: Some more context, outwardly equivalent to a backtrace,
	 * for helping figure out what this msg is for. What was going on?
	 * But not too much information so that a user
	 * posting this dump reveals confidential information.
	 */
/* TODO: dump unknown msgs to file, so user can send them to me
	 * if they wish, to help add support for new messages (inspired
	 * by Alexandr Shutko, who maintains OSCAR protocol documentation).
	 *
	 * Filed enhancement ticket for libpurple as #4688.
	 */
  purple_debug_info("msim","Unrecognized data on account for %s\n",((((session != 0) && ((session -> account) != 0)) && (( *(session -> account)).username != 0))?( *(session -> account)).username : "(NULL)"));
  if (note != 0) {
    purple_debug_info("msim","(Note: %s)\n",note);
  }
  if (msg != 0) {
    msim_msg_dump("Unrecognized message dump: %s\n",msg);
  }
}
/** Called when the session key arrives to check whether the user
 * has a username, and set one if desired. */

static gboolean msim_is_username_set(MsimSession *session,GList *msg)
{
  do {
    if (msg != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((session -> gc) != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session->gc != NULL");
      return 0;
    };
  }while (0);
  session -> sesskey = msim_msg_get_integer(msg,"sesskey");
  purple_debug_info("msim","SESSKEY=<%d>\n",(session -> sesskey));
/* What is proof? Used to be uid, but now is 52 base64'd bytes... */
/* Comes with: proof,profileid,userid,uniquenick -- all same values
	 * some of the time, but can vary. This is our own user ID. */
  session -> userid = msim_msg_get_integer(msg,"userid");
/* Save uid to account so this account can be looked up by uid. */
  purple_account_set_int((session -> account),"uid",(session -> userid));
/* Not sure what profileid is used for. */
  if (msim_msg_get_integer(msg,"profileid") != (session -> userid)) {
    msim_unrecognized(session,msg,"Profile ID didn\'t match user ID, don\'t know why");
  }
/* We now know are our own username, only after we're logged in..
	 * which is weird, but happens because you login with your email
	 * address and not username. Will be freed in msim_session_destroy(). */
  session -> username = msim_msg_get_string(msg,"uniquenick");
/* If user lacks a username, help them get one. */
  if (msim_msg_get_integer(msg,"uniquenick") == (session -> userid)) {
    purple_debug_info("msim_is_username_set","no username is set\n");
    purple_request_action((session -> gc),((const char *)(dgettext("pidgin","MySpaceIM - No Username Set"))),((const char *)(dgettext("pidgin","You appear to have no MySpace username."))),((const char *)(dgettext("pidgin","Would you like to set one now\? (Note: THIS CANNOT BE CHANGED!)"))),0,(session -> account),0,0,(session -> gc),2,((const char *)(dgettext("pidgin","_Yes"))),((GCallback )msim_set_username_cb),((const char *)(dgettext("pidgin","_No"))),((GCallback )msim_do_not_set_username_cb));
    purple_debug_info("msim_is_username_set","\'username not set\' alert prompted\n");
    return 0;
  }
  return (!0);
}
#ifdef MSIM_USE_KEEPALIVE
/**
 * Check if the connection is still alive, based on last communication.
 */
/* purple_debug_info("msim", "msim_check_alive: delta=%d\n", delta); */
#endif
/**
 * Handle mail reply checks.
 */

static void msim_check_inbox_cb(MsimSession *session,const GList *reply,gpointer data)
{
  GList *body;
  guint i;
  guint n;
/* Information for each new inbox message type. */
  static struct __unnamed_class___F0_L858_C9_L407R__L408R__scope____SgSS2___variable_declaration__variable_type___Pb__Cgcharc__typedef_declaration__Pe___variable_name_L407R__L408R__scope____SgSS2____scope__key__DELIMITER__L407R__L408R__scope____SgSS2___variable_declaration__variable_type_guintUi__typedef_declaration_variable_name_L407R__L408R__scope____SgSS2____scope__bit__DELIMITER__L407R__L408R__scope____SgSS2___variable_declaration__variable_type___Pb__Cgcharc__typedef_declaration__Pe___variable_name_L407R__L408R__scope____SgSS2____scope__url__DELIMITER__L407R__L408R__scope____SgSS2___variable_declaration__variable_type___Pb__Cgcharc__typedef_declaration__Pe___variable_name_L407R__L408R__scope____SgSS2____scope__text {
  const gchar *key;
  guint bit;
  const gchar *url;
  const gchar *text;}message_types[] = {{("Mail"), ((1 << 0)), ("http://messaging.myspace.com/index.cfm\?fuseaction=mail.inbox"), ((const gchar *)((void *)0))}, {("BlogComment"), ((1 << 1)), ("http://blog.myspace.com/index.cfm\?fuseaction=blog"), ((const gchar *)((void *)0))}, {("ProfileComment"), ((1 << 2)), ("http://home.myspace.com/index.cfm\?fuseaction=user"), ((const gchar *)((void *)0))}, {("FriendRequest"), ((1 << 3)), ("http://messaging.myspace.com/index.cfm\?fuseaction=mail.friendRequests"), ((const gchar *)((void *)0))}, {("PictureComment"), ((1 << 4)), ("http://home.myspace.com/index.cfm\?fuseaction=user"), ((const gchar *)((void *)0))}};
  const gchar *froms[6UL] = {("")};
  const gchar *tos[6UL] = {("")};
  const gchar *urls[6UL] = {("")};
  const gchar *subjects[6UL] = {("")};
  do {
    if (reply != ((const GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"reply != NULL");
      return ;
    };
  }while (0);
/* Can't write _()'d strings in array initializers. Workaround. */
/* khc: then use N_() in the array initializer and use _() when they are
	   used */
  message_types[0].text = ((const char *)(dgettext("pidgin","New mail messages")));
  message_types[1].text = ((const char *)(dgettext("pidgin","New blog comments")));
  message_types[2].text = ((const char *)(dgettext("pidgin","New profile comments")));
  message_types[3].text = ((const char *)(dgettext("pidgin","New friend requests!")));
  message_types[4].text = ((const char *)(dgettext("pidgin","New picture comments")));
  body = msim_msg_get_dictionary(reply,"body");
  if (body == ((GList *)((void *)0))) 
    return ;
  n = 0;
  for (i = 0; i < sizeof(message_types) / sizeof(message_types[0]); ++i) {
    const gchar *key;
    guint bit;
    key = message_types[i].key;
    bit = message_types[i].bit;
    if (msim_msg_get(body,key) != 0) {
/* Notify only on when _changes_ from no mail -> has mail
			 * (edge triggered) */
      if (!(((session -> inbox_status) & bit) != 0U)) {
        purple_debug_info("msim","msim_check_inbox_cb: got %s, at %d\n",((key != 0)?key : "(NULL)"),n);
        subjects[n] = message_types[i].text;
        froms[n] = ((const char *)(dgettext("pidgin","MySpace")));
        tos[n] = (session -> username);
/* TODO: append token, web challenge, so automatically logs in.
				 * Would also need to free strings because they won't be static
				 */
        urls[n] = message_types[i].url;
        ++n;
      }
      else {
        purple_debug_info("msim","msim_check_inbox_cb: already notified of %s\n",((key != 0)?key : "(NULL)"));
      }
      session -> inbox_status |= bit;
    }
  }
  if (n != 0U) {
    purple_debug_info("msim","msim_check_inbox_cb: notifying of %d\n",n);
/* TODO: free strings with callback _if_ change to dynamic (w/ token) */
/* handle */
    purple_notify_emails((session -> gc),n,(!0),subjects,froms,tos,urls,0,0);
/* count */
/* detailed */
/* PurpleNotifyCloseCallback cb */
/* gpointer user_data */
  }
  msim_msg_free(body);
}
/**
 * Send request to check if there is new mail.
 */

static gboolean msim_check_inbox(gpointer data)
{
  MsimSession *session;
  session = ((MsimSession *)data);
  purple_debug_info("msim","msim_check_inbox: checking mail\n");
  do {
    if (msim_send(session,"persist",'i',1,"sesskey",'i',(session -> sesskey),"cmd",'i',1,"dsn",'i',MG_CHECK_MAIL_DSN,"lid",'i',MG_CHECK_MAIL_LID,"uid",'i',(session -> userid),"rid",'i',msim_new_reply_callback(session,msim_check_inbox_cb,0),"body",'s',g_strdup(""),((void *)((void *)0))) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msim_send(session, \"persist\", MSIM_TYPE_INTEGER, 1, \"sesskey\", MSIM_TYPE_INTEGER, session->sesskey, \"cmd\", MSIM_TYPE_INTEGER, MSIM_CMD_GET, \"dsn\", MSIM_TYPE_INTEGER, MG_CHECK_MAIL_DSN, \"lid\", MSIM_TYPE_INTEGER, MG_CHECK_MAIL_LID, \"uid\", MSIM_TYPE_INTEGER, session->userid, \"rid\", MSIM_TYPE_INTEGER, msim_new_reply_callback(session, msim_check_inbox_cb, NULL), \"body\", MSIM_TYPE_STRING, g_strdup(\"\"), NULL)");
      return (!0);
    };
  }while (0);
/* Always return true, so that we keep checking for mail. */
  return (!0);
}
/**
 * Add contact from server to buddy list, after looking up username.
 * Callback from msim_add_contact_from_server().
 *
 * @param data An MsimMessage * of the contact information. Will be freed.
 */

static void msim_add_contact_from_server_cb(MsimSession *session,const GList *user_lookup_info,gpointer data)
{
  GList *contact_info;
  GList *user_lookup_info_body;
  PurpleGroup *group;
  PurpleBuddy *buddy;
  MsimUser *user;
  gchar *username;
  gchar *group_name;
  gchar *display_name;
  guint uid;
  guint visibility;
  contact_info = ((GList *)data);
  purple_debug_info("msim_add_contact_from_server_cb","contact_info addr=%p\n",contact_info);
  uid = msim_msg_get_integer(contact_info,"ContactID");
  if (!(user_lookup_info != 0)) {
    username = g_strdup(msim_uid2username_from_blist((session -> account),uid));
    display_name = ((gchar *)((void *)0));
    do {
      if (username != ((gchar *)((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"username != NULL");
        return ;
      };
    }while (0);
  }
  else {
    user_lookup_info_body = msim_msg_get_dictionary(user_lookup_info,"body");
    username = msim_msg_get_string(user_lookup_info_body,"UserName");
    display_name = msim_msg_get_string(user_lookup_info_body,"DisplayName");
    msim_msg_free(user_lookup_info_body);
    do {
      if (username != ((gchar *)((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"username != NULL");
        return ;
      };
    }while (0);
  }
  purple_debug_info("msim_add_contact_from_server_cb","*** about to add/update username=%s\n",username);
/* 1. Creates a new group, or gets existing group if it exists (or so
	 * the documentation claims). */
  group_name = msim_msg_get_string(contact_info,"GroupName");
  if (!(group_name != 0) || (( *group_name) == 0)) {
    g_free(group_name);
    group_name = g_strdup(((const char *)(dgettext("pidgin","IM Friends"))));
    purple_debug_info("myspace","No GroupName specified, defaulting to \'%s\'.\n",group_name);
  }
  group = purple_find_group(group_name);
  if (!(group != 0)) {
    group = purple_group_new(group_name);
/* Add group to beginning. See #2752. */
    purple_blist_add_group(group,0);
  }
  g_free(group_name);
  visibility = msim_msg_get_integer(contact_info,"Visibility");
  if (visibility == 2) {
/* This buddy is blocked (and therefore not on our buddy list */
    purple_privacy_deny_add((session -> account),username,(!0));
    msim_msg_free(contact_info);
    g_free(username);
    g_free(display_name);
    return ;
  }
/* 2. Get or create buddy */
  buddy = purple_find_buddy((session -> account),username);
  if (!(buddy != 0)) {
    purple_debug_info("msim_add_contact_from_server_cb","creating new buddy: %s\n",username);
    buddy = purple_buddy_new((session -> account),username,0);
  }
/* TODO: use 'Position' in contact_info to take into account where buddy is */
/* insertion point */
  purple_blist_add_buddy(buddy,0,group,0);
  if (strtol(username,0,10) == uid) {
/*
		 * This user has not set their username!  Set their server
		 * alias to their display name so that we don't see a bunch
		 * of numbers in the buddy list.
		 */
    if (display_name != ((gchar *)((void *)0))) {
      purple_blist_node_set_string(((PurpleBlistNode *)buddy),"DisplayName",display_name);
      serv_got_alias((session -> gc),username,display_name);
    }
    else {
      serv_got_alias((session -> gc),username,purple_blist_node_get_string(((PurpleBlistNode *)buddy),"DisplayName"));
    }
  }
  g_free(display_name);
/* 3. Update buddy information */
  user = msim_get_user_from_buddy(buddy,(!0));
  user -> id = uid;
/* Keep track of the user ID across sessions */
  purple_blist_node_set_int(((PurpleBlistNode *)buddy),"UserID",uid);
/* Stores a few fields in the MsimUser, relevant to the buddy itself.
	 * AvatarURL, Headline, ContactID. */
  msim_store_user_info(session,contact_info,0);
/* TODO: other fields, store in 'user' */
  msim_msg_free(contact_info);
  g_free(username);
}
/**
 * Add first ContactID in contact_info to buddy's list. Used to add
 * server-side buddies to client-side list.
 *
 * @return TRUE if added.
 */

static gboolean msim_add_contact_from_server(MsimSession *session,GList *contact_info)
{
  guint uid;
  const gchar *username;
  uid = msim_msg_get_integer(contact_info,"ContactID");
  do {
    if (uid != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"uid != 0");
      return 0;
    };
  }while (0);
/* Lookup the username, since NickName and IMName is unreliable */
  username = msim_uid2username_from_blist((session -> account),uid);
  if (!(username != 0)) {
    gchar *uid_str;
    uid_str = g_strdup_printf("%d",uid);
    purple_debug_info("msim_add_contact_from_server","contact_info addr=%p\n",contact_info);
    msim_lookup_user(session,uid_str,msim_add_contact_from_server_cb,((gpointer )(msim_msg_clone(contact_info))));
    g_free(uid_str);
  }
  else {
    msim_add_contact_from_server_cb(session,0,((gpointer )(msim_msg_clone(contact_info))));
  }
/* Say that the contact was added, even if we're still looking up
	 * their username. */
  return (!0);
}
/**
 * Called when contact list is received from server.
 */

static void msim_got_contact_list(MsimSession *session,const GList *reply,gpointer user_data)
{
  GList *body;
  GList *body_node;
  gchar *msg;
  guint buddy_count;
  body = msim_msg_get_dictionary(reply,"body");
  buddy_count = 0;
  for (body_node = body; body_node != ((GList *)((void *)0)); body_node = ((GList *)(body_node -> next))) {
    MsimMessageElement *elem;
    elem = ((MsimMessageElement *)(body_node -> data));
    if (g_str_equal((elem -> name),"ContactID") != 0) {
/* Will look for first contact in body_node */
      if (msim_add_contact_from_server(session,body_node) != 0) {
        ++buddy_count;
      }
    }
  }
  switch((guint )((gulong )user_data)){
    case 1:
{
      msg = g_strdup_printf((ngettext("%d buddy was added or updated from the server (including buddies already on the server-side list)","%d buddies were added or updated from the server (including buddies already on the server-side list)",buddy_count)),buddy_count);
      purple_notify_message((session -> account),PURPLE_NOTIFY_MSG_INFO,((const char *)(dgettext("pidgin","Add contacts from server"))),msg,0,0,0);
      g_free(msg);
      break; 
    }
    case 2:
{
/* TODO */
      break; 
    }
    case 0:
{
/* The session is now set up, ready to be connected. This emits the
			 * signedOn signal, so clients can now do anything with msimprpl, and
			 * we're ready for it (session key, userid, username all setup). */
      purple_connection_update_progress((session -> gc),((const char *)(dgettext("pidgin","Connected"))),3,4);
      purple_connection_set_state((session -> gc),PURPLE_CONNECTED);
      break; 
    }
  }
  msim_msg_free(body);
}
/**
 * Get contact list, calling msim_got_contact_list() with
 * what_to_do_after as user_data gpointer.
 *
 * @param what_to_do_after should be one of the MSIM_CONTACT_LIST_* #defines.
 */

static gboolean msim_get_contact_list(MsimSession *session,int what_to_do_after)
{
  return msim_send(session,"persist",'i',1,"sesskey",'i',(session -> sesskey),"cmd",'i',1,"dsn",'i',MG_LIST_ALL_CONTACTS_DSN,"lid",'i',MG_LIST_ALL_CONTACTS_LID,"uid",'i',(session -> userid),"rid",'i',msim_new_reply_callback(session,msim_got_contact_list,((gpointer )((gulong )what_to_do_after))),"body",'s',g_strdup(""),((void *)((void *)0)));
}
/** Called after username is set, if necessary and we're open for business. */

gboolean msim_we_are_logged_on(MsimSession *session)
{
  GList *body;
/* Set display name to username (otherwise will show email address) */
  purple_connection_set_display_name((session -> gc),(session -> username));
  body = msim_msg_new("UserID",'i',(session -> userid),((void *)((void *)0)));
/* Request IM info about ourself. */
  msim_send(session,"persist",'i',1,"sesskey",'i',(session -> sesskey),"cmd",'i',1,"dsn",'i',MG_OWN_MYSPACE_INFO_DSN,"lid",'i',MG_OWN_MYSPACE_INFO_LID,"rid",'i',session -> next_rid++,"UserID",'i',(session -> userid),"body",100,body,((void *)((void *)0)));
/* Request MySpace info about ourself. */
  msim_send(session,"persist",'i',1,"sesskey",'i',(session -> sesskey),"cmd",'i',1,"dsn",'i',MG_OWN_IM_INFO_DSN,"lid",'i',MG_OWN_IM_INFO_LID,"rid",'i',session -> next_rid++,"body",'s',g_strdup(""),((void *)((void *)0)));
/* TODO: set options (persist cmd=514,dsn=1,lid=10) */
/* TODO: set blocklist */
/* Notify servers of our current status. */
  purple_debug_info("msim","msim_we_are_logged_on: notifying servers of status\n");
  msim_set_status((session -> account),purple_account_get_active_status((session -> account)));
/* TODO: setinfo */
/*
	body = msim_msg_new(
		"TotalFriends", MSIM_TYPE_INTEGER, 666,
		NULL);
	msim_send(session,
			"setinfo", MSIM_TYPE_BOOLEAN, TRUE,
			"sesskey", MSIM_TYPE_INTEGER, session->sesskey,
			"info", MSIM_TYPE_DICTIONARY, body,
			NULL);
			*/
/* Disable due to problems with timeouts. TODO: fix. */
#ifdef MSIM_USE_KEEPALIVE
#endif
/* Check mail if they want to. */
  if (purple_account_get_check_mail((session -> account)) != 0) {
    session -> inbox_handle = purple_timeout_add((60 * 1000),((GSourceFunc )msim_check_inbox),session);
    msim_check_inbox(session);
  }
  msim_get_contact_list(session,0);
  return (!0);
}
/**
 * Record the client version in the buddy list, from an incoming message.
 */

static gboolean msim_incoming_bm_record_cv(MsimSession *session,GList *msg)
{
  gchar *username;
  gchar *cv;
  gboolean ret;
  MsimUser *user;
  username = msim_msg_get_string(msg,"_username");
  cv = msim_msg_get_string(msg,"cv");
  do {
    if (username != ((gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"username != NULL");
      return 0;
    };
  }while (0);
  if (!(cv != 0)) {
/* No client version to record, don't worry about it. */
    g_free(username);
    return 0;
  }
  user = msim_find_user(session,username);
  if (user != 0) {
    user -> client_cv = (atol(cv));
    ret = (!0);
  }
  else {
    ret = 0;
  }
  g_free(username);
  g_free(cv);
  return ret;
}
#ifdef MSIM_SEND_CLIENT_VERSION
/**
 * Send our client version to another unofficial client that understands it.
 */
#endif
/**
 * Process incoming status mood messages.
 *
 * @param session
 * @param msg Status mood update message. Caller frees.
 *
 * @return TRUE if successful.
 */

static gboolean msim_incoming_status_mood(MsimSession *session,GList *msg)
{
/* TODO: I dont know too much about this yet,
	 * so until I see how the official client handles
	 * this and decide if libpurple should as well,
	 * well just say we used it
	 */
  gchar *ss;
  ss = msim_msg_get_string(msg,"msg");
  purple_debug_info("msim","Incoming Status Message: %s",((ss != 0)?ss : "(NULL)"));
  g_free(ss);
  return (!0);
}
/**
 * Process incoming status messages.
 *
 * @param session
 * @param msg Status update message. Caller frees.
 *
 * @return TRUE if successful.
 */

static gboolean msim_incoming_status(MsimSession *session,GList *msg)
{
  MsimUser *user;
  GList *list;
  gchar *status_headline;
  gchar *status_headline_escaped;
  gint status_code;
  gint purple_status_code;
  gchar *username;
  gchar *unrecognized_msg;
  do {
    if (msg != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg != NULL");
      return 0;
    };
  }while (0);
/* Helpfully looked up by msim_incoming_resolve() for us. */
  username = msim_msg_get_string(msg,"_username");
  do {
    if (username != ((gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"username != NULL");
      return 0;
    };
  }while (0);
{
    gchar *ss;
    ss = msim_msg_get_string(msg,"msg");
    purple_debug_info("msim","msim_status: updating status for <%s> to <%s>\n",username,((ss != 0)?ss : "(NULL)"));
    g_free(ss);
  }
/* Example fields:
	 *  |s|0|ss|Offline
	 *  |s|1|ss|:-)|ls||ip|0|p|0
	 */
  list = msim_msg_get_list(msg,"msg");
  status_code = (msim_msg_get_integer_from_element((g_list_nth_data(list,2))));
  purple_debug_info("msim","msim_status: %s\'s status code = %d\n",username,status_code);
  status_headline = msim_msg_get_string_from_element((g_list_nth_data(list,4)));
/* Add buddy if not found.
	 * TODO: Could this be responsible for #3444? */
  user = msim_find_user(session,username);
  if (!(user != 0)) {
    PurpleBuddy *buddy;
    purple_debug_info("msim","msim_status: making new buddy for %s\n",username);
    buddy = purple_buddy_new((session -> account),username,0);
    purple_blist_add_buddy(buddy,0,0,0);
    user = msim_get_user_from_buddy(buddy,(!0));
    user -> id = (msim_msg_get_integer(msg,"f"));
/* Keep track of the user ID across sessions */
    purple_blist_node_set_int(((PurpleBlistNode *)buddy),"UserID",(user -> id));
    msim_store_user_info(session,msg,0);
  }
  else {
    purple_debug_info("msim","msim_status: found buddy %s\n",username);
  }
  if ((status_headline != 0) && (strcmp(status_headline,"") != 0)) {
/* The status headline is plaintext, but libpurple treats it as HTML,
		 * so escape any HTML characters to their entity equivalents. */
    status_headline_escaped = g_markup_escape_text(status_headline,(-1));
  }
  else {
    status_headline_escaped = ((gchar *)((void *)0));
  }
  g_free(status_headline);
/* don't copy; let the MsimUser own the headline, memory-wise */
  g_free((user -> headline));
  user -> headline = status_headline_escaped;
/* Set user status */
  switch(status_code){
    case 0:
{
      purple_status_code = PURPLE_STATUS_OFFLINE;
      break; 
    }
    case 1:
{
      purple_status_code = PURPLE_STATUS_AVAILABLE;
      break; 
    }
    case 5:
{
      purple_status_code = PURPLE_STATUS_AWAY;
      break; 
    }
    case 2:
{
/* Treat idle as an available status. */
      purple_status_code = PURPLE_STATUS_AVAILABLE;
      break; 
    }
    default:
{
      purple_debug_info("msim","msim_incoming_status for %s, unknown status code %d, treating as available\n",username,status_code);
      purple_status_code = PURPLE_STATUS_AVAILABLE;
      unrecognized_msg = g_strdup_printf("msim_incoming_status, unrecognized status code: %d\n",status_code);
      msim_unrecognized(session,0,unrecognized_msg);
      g_free(unrecognized_msg);
    }
  }
  purple_prpl_got_user_status((session -> account),username,purple_primitive_get_id_from_type(purple_status_code),((void *)((void *)0)));
  if (status_code == 2) {
    purple_debug_info("msim","msim_status: got idle: %s\n",username);
    purple_prpl_got_user_idle((session -> account),username,(!0),0);
  }
  else {
/* All other statuses indicate going back to non-idle. */
    purple_prpl_got_user_idle((session -> account),username,0,0);
  }
#ifdef MSIM_SEND_CLIENT_VERSION
/* Secretly whisper to unofficial clients our own version as they come online */
#endif
  if (status_code != 0) {
/* Get information when they come online.
		 * TODO: periodically refresh?
		 */
    purple_debug_info("msim_incoming_status","%s came online, looking up\n",username);
    msim_lookup_user(session,username,0,0);
  }
  g_free(username);
  msim_msg_list_free(list);
  return (!0);
}
/**
 * Handle an incoming instant message.
 *
 * @param session The session
 * @param msg Message from the server, containing 'f' (userid from) and 'msg'.
 *               Should also contain username in _username from preprocessing.
 *
 * @return TRUE if successful.
 */

static gboolean msim_incoming_im(MsimSession *session,GList *msg,const gchar *username)
{
  gchar *msg_msim_markup;
  gchar *msg_purple_markup;
  gchar *userid;
  time_t time_received;
  PurpleConversation *conv;
/* I know this isn't really a string... but we need it to be one for
	 * purple_find_conversation_with_account(). */
  userid = msim_msg_get_string(msg,"f");
  purple_debug_info("msim_incoming_im","UserID is %s",userid);
  if (msim_is_userid(username) != 0) {
    purple_debug_info("msim","Ignoring message from spambot (%s) on account %s\n",username,purple_account_get_username((session -> account)));
    return 0;
  }
/* See if a conversation with their UID already exists...*/
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,userid,(session -> account));
  if (conv != 0) {
/* Since the conversation exists... We need to normalize it */
    purple_conversation_set_name(conv,username);
  }
  msg_msim_markup = msim_msg_get_string(msg,"msg");
  do {
    if (msg_msim_markup != ((gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg_msim_markup != NULL");
      return 0;
    };
  }while (0);
  msg_purple_markup = msim_markup_to_html(session,msg_msim_markup);
  g_free(msg_msim_markup);
  time_received = (msim_msg_get_integer(msg,"date"));
  if (!(time_received != 0L)) {
    purple_debug_info("msim_incoming_im","date in message not set.\n");
    time_received = time(0);
  }
  serv_got_im((session -> gc),username,msg_purple_markup,PURPLE_MESSAGE_RECV,time_received);
  g_free(msg_purple_markup);
  return (!0);
}
/**
 * Handle an incoming action message or an IM.
 *
 * @param session
 * @param msg
 *
 * @return TRUE if successful.
 */

static gboolean msim_incoming_action_or_im(MsimSession *session,GList *msg)
{
  gchar *msg_text;
  gchar *username;
  gboolean rc;
  do {
    if (msg != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg != NULL");
      return 0;
    };
  }while (0);
  msg_text = msim_msg_get_string(msg,"msg");
  do {
    if (msg_text != ((gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg_text != NULL");
      return 0;
    };
  }while (0);
  username = msim_msg_get_string(msg,"_username");
  do {
    if (username != ((gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"username != NULL");
      return 0;
    };
  }while (0);
  purple_debug_info("msim","msim_incoming_action_or_im: action <%s> from <%s>\n",msg_text,username);
  if (g_str_equal(msg_text,"%typing%") != 0) {
    serv_got_typing((session -> gc),username,0,PURPLE_TYPING);
    rc = (!0);
  }
  else if (g_str_equal(msg_text,"%stoptyping%") != 0) {
    serv_got_typing_stopped((session -> gc),username);
    rc = (!0);
  }
  else if (strstr(msg_text,"!!!ZAP_SEND!!!=RTE_BTN_ZAPS_") != 0) {
    rc = msim_incoming_zap(session,msg);
  }
  else if (strstr(msg_text,"!!!GroupCount=") != 0) {
/* TODO: support group chats. I think the number in msg_text has
		 * something to do with the 'gid' field. */
    purple_debug_info("msim","msim_incoming_action_or_im: TODO: implement #4691, group chats: %s\n",msg_text);
    rc = (!0);
  }
  else if (strstr(msg_text,"!!!Offline=") != 0) {
/* TODO: support group chats. This one might mean a user
		 * went offline or exited the chat. */
    purple_debug_info("msim","msim_incoming_action_or_im: TODO: implement #4691, group chats: %s\n",msg_text);
    rc = (!0);
  }
  else if (msim_msg_get_integer(msg,"aid") != 0) {
    purple_debug_info("msim","TODO: implement #4691, group chat from %d on %d: %s\n",msim_msg_get_integer(msg,"aid"),msim_msg_get_integer(msg,"f"),msg_text);
    rc = (!0);
  }
  else {
    rc = msim_incoming_im(session,msg,username);
  }
  g_free(msg_text);
  g_free(username);
  return rc;
}
/**
 * Process an incoming media (message background?) message.
 */

static gboolean msim_incoming_media(MsimSession *session,GList *msg)
{
  gchar *username;
  gchar *text;
  username = msim_msg_get_string(msg,"_username");
  text = msim_msg_get_string(msg,"msg");
  do {
    if (username != ((gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"username != NULL");
      return 0;
    };
  }while (0);
  do {
    if (text != ((gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"text != NULL");
      return 0;
    };
  }while (0);
  purple_debug_info("msim","msim_incoming_media: from %s, got msg=%s\n",username,text);
/* Media messages are sent when the user opens a window to someone.
	 * Tell libpurple they started typing and stopped typing, to inform the Psychic
	 * Mode plugin so it too can open a window to the user. */
  serv_got_typing((session -> gc),username,0,PURPLE_TYPING);
  serv_got_typing_stopped((session -> gc),username);
  g_free(username);
  return (!0);
}
/**
 * Process an incoming "unofficial client" message. The plugin for
 * Miranda IM sends this message with the plugin information.
 */

static gboolean msim_incoming_unofficial_client(MsimSession *session,GList *msg)
{
  MsimUser *user;
  gchar *username;
  gchar *client_info;
  username = msim_msg_get_string(msg,"_username");
  client_info = msim_msg_get_string(msg,"msg");
  do {
    if (username != ((gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"username != NULL");
      return 0;
    };
  }while (0);
  do {
    if (client_info != ((gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"client_info != NULL");
      return 0;
    };
  }while (0);
  purple_debug_info("msim","msim_incoming_unofficial_client: %s is using client %s\n",username,client_info);
  user = msim_find_user(session,username);
  do {
    if (user != ((MsimUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return 0;
    };
  }while (0);
  if ((user -> client_info) != 0) {
    g_free((user -> client_info));
  }
  user -> client_info = client_info;
  g_free(username);
/* Do not free client_info - the MsimUser now owns it. */
  return (!0);
}
/**
 * Handle an incoming buddy message.
 */

static gboolean msim_incoming_bm(MsimSession *session,GList *msg)
{
  guint bm;
  bm = msim_msg_get_integer(msg,"bm");
  msim_incoming_bm_record_cv(session,msg);
  switch(bm){
    case 100:
{
      return msim_incoming_status(session,msg);
    }
    case 1:
{
    }
    case 121:
{
      return msim_incoming_action_or_im(session,msg);
    }
    case 122:
{
      return msim_incoming_media(session,msg);
    }
    case 200:
{
      return msim_incoming_unofficial_client(session,msg);
    }
    case 126:
{
      return msim_incoming_status_mood(session,msg);
    }
    default:
{
/*
			 * Unknown message type!  We used to call
			 *   msim_incoming_action_or_im(session, msg);
			 * for these, but that doesn't help anything, and it means
			 * we'll show broken gibberish if MySpace starts sending us
			 * other message types.
			 */
      purple_debug_warning("myspace","Received unknown imcoming message, bm=%u\n",bm);
      return (!0);
    }
  }
}
/**
 * Process the initial server information from the server.
 */

static gboolean msim_process_server_info(MsimSession *session,GList *msg)
{
  GList *body;
  body = msim_msg_get_dictionary(msg,"body");
  do {
    if (body != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"body != NULL");
      return 0;
    };
  }while (0);
/* Example body:
AdUnitRefreshInterval=10.
AlertPollInterval=360.
AllowChatRoomEmoticonSharing=False.
ChatRoomUserIDs=78744676;163733130;1300326231;123521495;142663391.
CurClientVersion=673.
EnableIMBrowse=True.
EnableIMStuffAvatars=False.
EnableIMStuffZaps=False.
MaxAddAllFriends=100.
MaxContacts=1000.
MinClientVersion=594.
MySpaceIM_ENGLISH=78744676.
MySpaceNowTimer=720.
PersistenceDataTimeout=900.
UseWebChallenge=1.
WebTicketGoHome=False
	Anything useful? TODO: use what is useful, and use it.
*/
  purple_debug_info("msim_process_server_info","maximum contacts: %d\n",msim_msg_get_integer(body,"MaxContacts"));
  session -> server_info = body;
/* session->server_info freed in msim_session_destroy */
  return (!0);
}
/**
 * Process a web challenge, used to login to the web site.
 */

static gboolean msim_web_challenge(MsimSession *session,GList *msg)
{
/* TODO: web challenge, store token. #2659. */
  return 0;
}
/**
 * Process a persistance message reply from the server.
 *
 * @param session
 * @param msg Message reply from server.
 *
 * @return TRUE if successful.
 *
 * msim_lookup_user sets callback for here
 */

static gboolean msim_process_reply(MsimSession *session,GList *msg)
{
  MSIM_USER_LOOKUP_CB cb;
  gpointer data;
  guint rid;
  guint cmd;
  guint dsn;
  guint lid;
  do {
    if (msg != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg != NULL");
      return 0;
    };
  }while (0);
  msim_store_user_info(session,msg,0);
  rid = msim_msg_get_integer(msg,"rid");
  cmd = msim_msg_get_integer(msg,"cmd");
  dsn = msim_msg_get_integer(msg,"dsn");
  lid = msim_msg_get_integer(msg,"lid");
/* Unsolicited messages */
  if (cmd == (256 | 1)) {
    if ((dsn == MG_SERVER_INFO_DSN) && (lid == MG_SERVER_INFO_LID)) {
      return msim_process_server_info(session,msg);
    }
    else if ((dsn == MG_WEB_CHALLENGE_DSN) && (lid == MG_WEB_CHALLENGE_LID)) {
      return msim_web_challenge(session,msg);
    }
  }
/* If a callback is registered for this userid lookup, call it. */
  cb = (g_hash_table_lookup((session -> user_lookup_cb),((gpointer )((gulong )rid))));
  data = g_hash_table_lookup((session -> user_lookup_cb_data),((gpointer )((gulong )rid)));
  if (cb != 0) {
    purple_debug_info("msim","msim_process_reply: calling callback now\n");
/* Clone message, so that the callback 'cb' can use it (needs to free it also). */
    ( *cb)(session,msg,data);
    g_hash_table_remove((session -> user_lookup_cb),((gpointer )((gulong )rid)));
    g_hash_table_remove((session -> user_lookup_cb_data),((gpointer )((gulong )rid)));
  }
  else {
    purple_debug_info("msim","msim_process_reply: no callback for rid %d\n",rid);
  }
  return (!0);
}
/**
 * Handle an error from the server.
 *
 * @param session
 * @param msg The message.
 *
 * @return TRUE if successfully reported error.
 */

static gboolean msim_error(MsimSession *session,GList *msg)
{
  gchar *errmsg;
  gchar *full_errmsg;
  guint err;
  do {
    if (msg != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg != NULL");
      return 0;
    };
  }while (0);
  err = msim_msg_get_integer(msg,"err");
  errmsg = msim_msg_get_string(msg,"errmsg");
  full_errmsg = g_strdup_printf(((const char *)(dgettext("pidgin","Protocol error, code %d: %s"))),err,((errmsg != 0)?errmsg : "no \'errmsg\' given"));
  g_free(errmsg);
  purple_debug_info("msim","msim_error (sesskey=%d): %s\n",(session -> sesskey),full_errmsg);
/* Destroy session if fatal. */
  if (msim_msg_get(msg,"fatal") != 0) {
    PurpleConnectionError reason = PURPLE_CONNECTION_ERROR_NETWORK_ERROR;
    purple_debug_info("msim","fatal error, closing\n");
    switch(err){
/* Incorrect password */
      case 260:
{
        reason = PURPLE_CONNECTION_ERROR_AUTHENTICATION_FAILED;
        if (!(purple_account_get_remember_password((session -> account)) != 0)) 
          purple_account_set_password((session -> account),0);
#ifdef MSIM_MAX_PASSWORD_LENGTH
        if ((( *(session -> account)).password != 0) && (strlen(( *(session -> account)).password) > 10)) {
          gchar *suggestion;
          suggestion = g_strdup_printf(((const char *)(dgettext("pidgin","%s Your password is %zu characters, which is longer than the maximum length of %d.  Please shorten your password at http://profileedit.myspace.com/index.cfm\?fuseaction=accountSettings.changePassword and try again."))),full_errmsg,strlen(( *(session -> account)).password),10);
/* Replace full_errmsg. */
          g_free(full_errmsg);
          full_errmsg = suggestion;
        }
        else {
          g_free(full_errmsg);
          full_errmsg = g_strdup(((const char *)(dgettext("pidgin","Incorrect username or password"))));
        }
#endif
        break; 
      }
/* Logged in elsewhere */
      case 6:
{
        reason = PURPLE_CONNECTION_ERROR_NAME_IN_USE;
        if (!(purple_account_get_remember_password((session -> account)) != 0)) 
          purple_account_set_password((session -> account),0);
        break; 
      }
    }
    purple_connection_error_reason((session -> gc),reason,full_errmsg);
  }
  else {
    purple_notify_message((session -> account),PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","MySpaceIM Error"))),full_errmsg,0,0,0);
  }
  g_free(full_errmsg);
  return (!0);
}
/**
 * Process a message.
 *
 * @param session
 * @param msg A message from the server, ready for processing (possibly with resolved username information attached). Caller frees.
 *
 * @return TRUE if successful. FALSE if processing failed.
 */

static gboolean msim_process(MsimSession *session,GList *msg)
{
  do {
    if (session != ((MsimSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return 0;
    };
  }while (0);
  do {
    if (msg != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg != NULL");
      return 0;
    };
  }while (0);
  if (msim_msg_get_integer(msg,"lc") == 1) {
    return msim_login_challenge(session,msg);
  }
  else if (msim_msg_get_integer(msg,"lc") == 2) {
/* return msim_we_are_logged_on(session, msg); */
    if (msim_is_username_set(session,msg) != 0) {
      return msim_we_are_logged_on(session);
    }
    else {
/* No username is set... We'll wait for the callbacks to do their work */
/* When they're all done, the last one will call msim_we_are_logged_on() and pick up where we left off */
      return 0;
    }
  }
  else if (msim_msg_get(msg,"bm") != 0) {
    return msim_incoming_bm(session,msg);
  }
  else if (msim_msg_get(msg,"rid") != 0) {
    return msim_process_reply(session,msg);
  }
  else if (msim_msg_get(msg,"error") != 0) {
    return msim_error(session,msg);
  }
  else if (msim_msg_get(msg,"ka") != 0) {
    return (!0);
  }
  else {
    msim_unrecognized(session,msg,"in msim_process");
    return 0;
  }
}
/**
 * After a uid is resolved to username, tag it with the username and submit for processing.
 *
 * @param session
 * @param userinfo Response messsage to resolving request.
 * @param data MsimMessage *, the message to attach information to.
 */

static void msim_incoming_resolved(MsimSession *session,const GList *userinfo,gpointer data)
{
  gchar *username;
  GList *msg;
  GList *body;
  do {
    if (userinfo != ((const GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"userinfo != NULL");
      return ;
    };
  }while (0);
  body = msim_msg_get_dictionary(userinfo,"body");
  do {
    if (body != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"body != NULL");
      return ;
    };
  }while (0);
  username = msim_msg_get_string(body,"UserName");
  do {
    if (username != ((gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"username != NULL");
      return ;
    };
  }while (0);
/* Note: username will be owned by 'msg' below. */
  msg = ((GList *)data);
  do {
    if (msg != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg != NULL");
      return ;
    };
  }while (0);
/* TODO: more elegant solution than below. attach whole message? */
/* Special elements name beginning with '_', we'll use internally within the
	 * program (did not come directly from the wire). */
/* This makes 'msg' the owner of 'username' */
  msg = msim_msg_append(msg,"_username",'s',username);
/* TODO: attach more useful information, like ImageURL */
  msim_process(session,msg);
  msim_msg_free(msg);
  msim_msg_free(body);
}
/**
 * Preprocess incoming messages, resolving as needed, calling
 * msim_process() when ready to process.
 *
 * @param session
 * @param msg MsimMessage *, freed by caller.
 */

static gboolean msim_preprocess_incoming(MsimSession *session,GList *msg)
{
  do {
    if (msg != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg != NULL");
      return 0;
    };
  }while (0);
  if ((msim_msg_get(msg,"bm") != 0) && (msim_msg_get(msg,"f") != 0)) {
    guint uid;
    const gchar *username;
/* 'f' = userid message is from, in buddy messages */
    uid = msim_msg_get_integer(msg,"f");
    username = msim_uid2username_from_blist((session -> account),uid);
    if (username != 0) {
/* Know username already, use it. */
      purple_debug_info("msim","msim_preprocess_incoming: tagging with _username=%s\n",username);
      msg = msim_msg_append(msg,"_username",'s',(g_strdup(username)));
      return msim_process(session,msg);
    }
    else {
      gchar *from;
/* Send lookup request. */
/* XXX: where is msim_msg_get_string() freed? make _strdup and _nonstrdup. */
      purple_debug_info("msim","msim_incoming: sending lookup, setting up callback\n");
      from = msim_msg_get_string(msg,"f");
      msim_lookup_user(session,from,msim_incoming_resolved,(msim_msg_clone(msg)));
      g_free(from);
/* indeterminate */
      return (!0);
    }
  }
  else {
/* Nothing to resolve - send directly to processing. */
    return msim_process(session,msg);
  }
}
/**
 * Callback when input available.
 *
 * @param gc_uncasted A PurpleConnection pointer.
 * @param source File descriptor.
 * @param cond PURPLE_INPUT_READ
 *
 * Reads the input, and calls msim_preprocess_incoming() to handle it.
 */

static void msim_input_cb(gpointer gc_uncasted,gint source,PurpleInputCondition cond)
{
  PurpleConnection *gc;
  MsimSession *session;
  gchar *end;
  int n;
  do {
    if (gc_uncasted != ((void *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc_uncasted != NULL");
      return ;
    };
  }while (0);
/* Note: 0 is a valid fd */
  do {
    if (source >= 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"source >= 0");
      return ;
    };
  }while (0);
  gc = ((PurpleConnection *)gc_uncasted);
  session = (gc -> proto_data);
/* libpurple/eventloop.h only defines these two */
  if ((cond != PURPLE_INPUT_READ) && (cond != PURPLE_INPUT_WRITE)) {
    purple_debug_info("msim_input_cb","unknown condition=%d\n",cond);
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Invalid input condition"))));
    return ;
  }
  do {
    if (cond == PURPLE_INPUT_READ) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cond == PURPLE_INPUT_READ");
      return ;
    };
  }while (0);
/* Mark down that we got data, so we don't timeout. */
  session -> last_comm = time(0);
/* If approaching end of buffer, reallocate some more memory. */
  if ((session -> rxsize) < ((session -> rxoff) + (15 * 1024))) {
    purple_debug_info("msim","msim_input_cb: %d-byte read buffer full, rxoff=%d, growing by %d bytes\n",(session -> rxsize),(session -> rxoff),15 * 1024);
    session -> rxsize += (15 * 1024);
    session -> rxbuf = (g_realloc((session -> rxbuf),(session -> rxsize)));
    return ;
  }
  purple_debug_info("msim","dynamic buffer at %d (max %d), reading up to %d\n",(session -> rxoff),(session -> rxsize),(((15 * 1024) - (session -> rxoff)) - 1));
/* Read into buffer. On Win32, need recv() not read(). session->fd also holds
	 * the file descriptor, but it sometimes differs from the 'source' parameter.
	 */
  n = (recv((session -> fd),((session -> rxbuf) + (session -> rxoff)),(((session -> rxsize) - (session -> rxoff)) - 1),0));
  if (n < 0) {
    gchar *tmp;
    if ( *__errno_location() == 11) 
/* No worries */
      return ;
    tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Lost connection with server: %s"))),g_strerror( *__errno_location()));
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,tmp);
    g_free(tmp);
    return ;
  }
  else if (n == 0) {
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Server closed the connection"))));
    return ;
  }
/* Null terminate */
  purple_debug_info("msim","msim_input_cb: going to null terminate at n=%d\n",n);
  (session -> rxbuf)[(session -> rxoff) + n] = 0;
#ifdef MSIM_CHECK_EMBEDDED_NULLS
/* Check for embedded NULs. I don't handle them, and they shouldn't occur. */
/* Occurs after login, but it is not a null byte. */
/*purple_connection_error_reason (gc,
				PURPLE_CONNECTION_ERROR_NETWORK_ERROR,
				"Invalid message - null byte on input"); */
#endif
  session -> rxoff += n;
  purple_debug_info("msim","msim_input_cb: read=%d\n",n);
{
#ifdef MSIM_DEBUG_RXBUF
#endif
/* Look for \\final\\ end markers. If found, process message. */
    while((end = strstr((session -> rxbuf),"\\final\\")) != 0){
      GList *msg;
#ifdef MSIM_DEBUG_RXBUF
#endif
       *end = 0;
      msg = msim_parse((session -> rxbuf));
      if (!(msg != 0)) {
        purple_debug_info("msim","msim_input_cb: couldn\'t parse rxbuf\n");
        purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Unable to parse message"))));
        break; 
      }
      else {
/* Process message and then free it (processing function should
			 * clone message if it wants to keep it afterwards.) */
        if (!(msim_preprocess_incoming(session,msg) != 0)) {
          msim_msg_dump("msim_input_cb: preprocessing message failed on msg: %s\n",msg);
        }
        msim_msg_free(msg);
      }
/* Move remaining part of buffer to beginning. */
      session -> rxoff -= (strlen((session -> rxbuf)) + strlen("\\final\\"));
      memmove((session -> rxbuf),(end + strlen("\\final\\")),((session -> rxsize) - ((end + strlen("\\final\\")) - (session -> rxbuf))));
/* Clear end of buffer
		 * memset(end, 0, MSIM_READ_BUF_SIZE - (end - session->rxbuf));
		 */
    }
  }
}
/**
 * Callback when connected. Sets up input handlers.
 *
 * @param data A PurpleConnection pointer.
 * @param source File descriptor.
 * @param error_message
 */

static void msim_connect_cb(gpointer data,gint source,const gchar *error_message)
{
  PurpleConnection *gc;
  MsimSession *session;
  do {
    if (data != ((void *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"data != NULL");
      return ;
    };
  }while (0);
  gc = ((PurpleConnection *)data);
  session = ((MsimSession *)(gc -> proto_data));
  if (source < 0) {
    gchar *tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to connect: %s"))),error_message);
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,tmp);
    g_free(tmp);
    return ;
  }
  session -> fd = source;
  gc -> inpa = (purple_input_add(source,PURPLE_INPUT_READ,msim_input_cb,gc));
}
/**
 * Start logging in to the MSIM servers.
 *
 * @param acct Account information to use to login.
 */

static void msim_login(PurpleAccount *acct)
{
  PurpleConnection *gc;
  const gchar *host;
  int port;
  do {
    if (acct != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"acct != NULL");
      return ;
    };
  }while (0);
  do {
    if ((acct -> username) != ((char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"acct->username != NULL");
      return ;
    };
  }while (0);
  purple_debug_info("msim","logging in %s\n",(acct -> username));
  gc = purple_account_get_connection(acct);
  gc -> proto_data = (msim_session_new(acct));
  gc -> flags |= (PURPLE_CONNECTION_HTML | PURPLE_CONNECTION_NO_URLDESC);
/*
	 * Lets wipe out our local list of blocked buddies.  We'll get a
	 * list of all blocked buddies from the server, and we shouldn't
	 * have stuff in the local list that isn't on the server list.
	 */
  while((acct -> deny) != ((GSList *)((void *)0)))
    purple_privacy_deny_remove(acct,( *(acct -> deny)).data,(!0));
/* 1. connect to server */
  purple_connection_update_progress(gc,((const char *)(dgettext("pidgin","Connecting"))),0,4);
/* which connection step this is */
/* total number of steps */
  host = purple_account_get_string(acct,"server","im.myspace.akadns.net");
  port = purple_account_get_int(acct,"port",1863);
/* From purple.sf.net/api:
	 * """Note that this function name can be misleading--although it is called
	 * "proxy connect," it is used for establishing any outgoing TCP connection,
	 * whether through a proxy or not.""" */
/* Calls msim_connect_cb when connected. */
  if (!(purple_proxy_connect(gc,acct,host,port,msim_connect_cb,gc) != 0)) {
/* TODO: try other ports if in auto mode, then save
		 * working port and try that first next time. */
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Unable to connect"))));
    return ;
  }
}

static void msim_buddy_free(PurpleBuddy *buddy)
{
  msim_user_free((purple_buddy_get_protocol_data(buddy)));
  purple_buddy_set_protocol_data(buddy,0);
}
/**
 * Close the connection.
 *
 * @param gc The connection.
 */

static void msim_close(PurpleConnection *gc)
{
  GSList *buddies;
  MsimSession *session;
  if (gc == ((PurpleConnection *)((void *)0))) {
    return ;
  }
/*
	 * Free our protocol-specific buddy data.  It almost seems like libpurple
	 * should call our buddy_free prpl callback so that we don't need to do
	 * this... but it doesn't, so we do.
	 */
  buddies = purple_find_buddies(purple_connection_get_account(gc),0);
  while(buddies != ((GSList *)((void *)0))){
    msim_buddy_free((buddies -> data));
    buddies = g_slist_delete_link(buddies,buddies);
  }
  session = ((MsimSession *)(gc -> proto_data));
  if (session == ((MsimSession *)((void *)0))) 
    return ;
  gc -> proto_data = ((void *)((void *)0));
  if (( *(session -> gc)).inpa != 0) {
    purple_input_remove(( *(session -> gc)).inpa);
  }
  if ((session -> fd) >= 0) {
    close((session -> fd));
    session -> fd = (-1);
  }
  msim_session_destroy(session);
}
/**
 * Schedule an IM to be sent once the user ID is looked up.
 *
 * @param gc Connection.
 * @param who A user id, email, or username to send the message to.
 * @param message Instant message text to send.
 * @param flags Flags.
 *
 * @return 1 if successful or postponed, -1 if failed
 *
 * Allows sending to a user by username, email address, or userid. If
 * a username or email address is given, the userid must be looked up.
 * This function does that by calling msim_postprocess_outgoing().
 */

static int msim_send_im(PurpleConnection *gc,const gchar *who,const gchar *message,PurpleMessageFlags flags)
{
  MsimSession *session;
  gchar *message_msim;
  int rc;
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return -1;
    };
  }while (0);
  do {
    if (who != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"who != NULL");
      return -1;
    };
  }while (0);
  do {
    if (message != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"message != NULL");
      return -1;
    };
  }while (0);
/* 'flags' has many options, not used here. */
  session = ((MsimSession *)(gc -> proto_data));
  message_msim = html_to_msim_markup(session,message);
  if (msim_send_bm(session,who,message_msim,1) != 0) {
/* Return 1 to have Purple show this IM as being sent, 0 to not. I always
		 * return 1 even if the message could not be sent, since I don't know if
		 * it has failed yet--because the IM is only sent after the userid is
		 * retrieved from the server (which happens after this function returns).
		 * If an error does occur, it should be logged to the IM window.
		 */
    rc = 1;
  }
  else {
    rc = -1;
  }
  g_free(message_msim);
  return rc;
}
/**
 * Handle when our user starts or stops typing to another user.
 *
 * @param gc
 * @param name The buddy name to which our user is typing to
 * @param state PURPLE_TYPING, PURPLE_TYPED, PURPLE_NOT_TYPING
 *
 * @return 0
 */

static unsigned int msim_send_typing(PurpleConnection *gc,const gchar *name,PurpleTypingState state)
{
  const gchar *typing_str;
  MsimSession *session;
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return 0;
    };
  }while (0);
  do {
    if (name != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return 0;
    };
  }while (0);
  session = ((MsimSession *)(gc -> proto_data));
  switch(state){
    case PURPLE_TYPING:
{
      typing_str = "%typing%";
      break; 
    }
    default:
{
      typing_str = "%stoptyping%";
      break; 
    }
  }
  purple_debug_info("msim","msim_send_typing(%s): %d (%s)\n",name,state,typing_str);
  msim_send_bm(session,name,typing_str,121);
  return 0;
}
/**
 * Callback for msim_get_info(), for when user info is received.
 */

static void msim_get_info_cb(MsimSession *session,const GList *user_info_msg,gpointer data)
{
  GList *msg;
  gchar *username;
  PurpleNotifyUserInfo *user_info;
  MsimUser *user;
/* Get user{name,id} from msim_get_info, passed as an MsimMessage for
	   orthogonality. */
  msg = ((GList *)data);
  do {
    if (msg != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg != NULL");
      return ;
    };
  }while (0);
  username = msim_msg_get_string(msg,"user");
  if (!(username != 0)) {
    purple_debug_info("msim","msim_get_info_cb: no \'user\' in msg\n");
    return ;
  }
  msim_msg_free(msg);
  purple_debug_info("msim","msim_get_info_cb: got for user: %s\n",username);
  user = msim_find_user(session,username);
  if (!(user != 0)) {
/* User isn't on blist, create a temporary user to store info. */
    user = ((MsimUser *)(g_malloc0_n(1,(sizeof(MsimUser )))));
    user -> temporary_user = (!0);
  }
/* Update user structure with new information */
  msim_store_user_info(session,user_info_msg,user);
  user_info = purple_notify_user_info_new();
/* Append data from MsimUser to PurpleNotifyUserInfo for display, full */
  msim_append_user_info(session,user_info,user,(!0));
  purple_notify_userinfo((session -> gc),username,user_info,0,0);
  purple_debug_info("msim","msim_get_info_cb: username=%s\n",username);
  purple_notify_user_info_destroy(user_info);
  if ((user -> temporary_user) != 0) 
    msim_user_free(user);
  g_free(username);
}
/**
 * Retrieve a user's profile.
 * @param username Username, user ID, or email address to lookup.
 */

static void msim_get_info(PurpleConnection *gc,const gchar *username)
{
  MsimSession *session;
  MsimUser *user;
  gchar *user_to_lookup;
  GList *user_msg;
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  do {
    if (username != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"username != NULL");
      return ;
    };
  }while (0);
  session = ((MsimSession *)(gc -> proto_data));
/* Obtain uid of buddy. */
  user = msim_find_user(session,username);
/* If is on buddy list, lookup by uid since it is faster. */
  if ((user != 0) && ((user -> id) != 0)) {
    user_to_lookup = g_strdup_printf("%d",(user -> id));
  }
  else {
/* Looking up buddy not on blist. Lookup by whatever user entered. */
    user_to_lookup = g_strdup(username);
  }
/* Pass the username to msim_get_info_cb(), because since we lookup
	 * by userid, the userinfo message will only contain the uid (not
	 * the username) but it would be useful to display the username too.
	 */
  user_msg = msim_msg_new("user",'s',g_strdup(username),((void *)((void *)0)));
  purple_debug_info("msim","msim_get_info, setting up lookup, user=%s\n",username);
  msim_lookup_user(session,user_to_lookup,msim_get_info_cb,user_msg);
  g_free(user_to_lookup);
}
/**
 * Set status using an MSIM_STATUS_CODE_* value.
 * @param status_code An MSIM_STATUS_CODE_* value.
 * @param statstring Status string, must be a dynamic string (will be freed by msim_send).
 */

static void msim_set_status_code(MsimSession *session,guint status_code,gchar *statstring)
{
  do {
    if (statstring != ((gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"statstring != NULL");
      return ;
    };
  }while (0);
  purple_debug_info("msim","msim_set_status_code: going to set status to code=%d,str=%s\n",status_code,statstring);
  if (!(msim_send(session,"status",'i',status_code,"sesskey",'i',(session -> sesskey),"statstring",'s',statstring,"locstring",'s',g_strdup(""),((void *)((void *)0))) != 0)) {
    purple_debug_info("msim","msim_set_status: failed to set status\n");
  }
}
/**
 * Set your status - callback for when user manually sets it.
 */

static void msim_set_status(PurpleAccount *account,PurpleStatus *status)
{
  PurpleStatusType *type;
  PurplePresence *pres;
  MsimSession *session;
  guint status_code;
  const gchar *message;
  gchar *stripped;
  gchar *unrecognized_msg;
  session = ((MsimSession *)( *(account -> gc)).proto_data);
  type = purple_status_get_type(status);
  pres = purple_status_get_presence(status);
  switch(purple_status_type_get_primitive(type)){
    case PURPLE_STATUS_AVAILABLE:
{
      purple_debug_info("msim","msim_set_status: available (%d->%d)\n",PURPLE_STATUS_AVAILABLE,1);
      status_code = 1;
      break; 
    }
    case PURPLE_STATUS_INVISIBLE:
{
      purple_debug_info("msim","msim_set_status: invisible (%d->%d)\n",PURPLE_STATUS_INVISIBLE,0);
      status_code = 0;
      break; 
    }
    case PURPLE_STATUS_AWAY:
{
      purple_debug_info("msim","msim_set_status: away (%d->%d)\n",PURPLE_STATUS_AWAY,5);
      status_code = 5;
      break; 
    }
    default:
{
      purple_debug_info("msim","msim_set_status: unknown status interpreting as online");
      status_code = 1;
      unrecognized_msg = g_strdup_printf("msim_set_status, unrecognized status type: %d\n",purple_status_type_get_primitive(type));
      msim_unrecognized(session,0,unrecognized_msg);
      g_free(unrecognized_msg);
      break; 
    }
  }
  message = purple_status_get_attr_string(status,"message");
/* Status strings are plain text. */
  if (message != ((const gchar *)((void *)0))) 
    stripped = purple_markup_strip_html(message);
  else 
    stripped = g_strdup("");
  msim_set_status_code(session,status_code,stripped);
/* If we should be idle, set that status. Time is irrelevant here. */
  if ((purple_presence_is_idle(pres) != 0) && (status_code != 0)) 
    msim_set_idle((account -> gc),1);
}
/**
 * Go idle.
 */

static void msim_set_idle(PurpleConnection *gc,int time)
{
  MsimSession *session;
  PurpleStatus *status;
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  session = ((MsimSession *)(gc -> proto_data));
  status = purple_account_get_active_status((session -> account));
  if (time == 0) {
/* Going back from idle. In msim, idle is mutually exclusive
		 * from the other states (you can only be away or idle, but not
		 * both, for example), so by going non-idle I go back to what
		 * libpurple says I should be.
		 */
    msim_set_status((session -> account),status);
  }
  else {
    const gchar *message;
    gchar *stripped;
/* Set the idle message to the status message from the real
		 * current status.
		 */
    message = purple_status_get_attr_string(status,"message");
    if (message != ((const gchar *)((void *)0))) 
      stripped = purple_markup_strip_html(message);
    else 
      stripped = g_strdup("");
/* msim doesn't support idle time, so just go idle */
    msim_set_status_code(session,2,stripped);
  }
}
/**
 * @return TRUE if everything was ok, FALSE if something went awry.
 */

static gboolean msim_update_blocklist_for_buddy(MsimSession *session,const char *name,gboolean allow,gboolean block)
{
  GList *msg;
  GList *list;
  list = ((GList *)((void *)0));
  list = g_list_prepend(list,(((allow != 0)?"a+" : "a-")));
  list = g_list_prepend(list,"<uid>");
  list = g_list_prepend(list,(((block != 0)?"b+" : "b-")));
  list = g_list_prepend(list,"<uid>");
  list = g_list_reverse(list);
  msg = msim_msg_new("blocklist",'f',!0,"sesskey",'i',(session -> sesskey),"idlist",108,list,((void *)((void *)0)));
/* TODO: MsimMessage lists. Currently <uid> isn't replaced in lists. */
/* "idlist", MSIM_TYPE_STRING, g_strdup("a-|<uid>|b-|<uid>"), */
  if (!(msim_postprocess_outgoing(session,msg,name,"idlist",0) != 0)) {
    purple_debug_error("myspace","blocklist command failed for %s, allow=%d, block=%d\n",name,allow,block);
    msim_msg_free(msg);
    return 0;
  }
  msim_msg_free(msg);
  return (!0);
}
/**
 * Add a buddy to user's buddy list.
 */

static void msim_add_buddy(PurpleConnection *gc,PurpleBuddy *buddy,PurpleGroup *group)
{
  MsimSession *session;
  GList *msg;
  GList *msg_persist;
  GList *body;
  const char *name;
  const char *gname;
  session = ((MsimSession *)(gc -> proto_data));
  name = purple_buddy_get_name(buddy);
  gname = ((group != 0)?purple_group_get_name(group) : ((const char *)((void *)0)));
  if (msim_get_user_from_buddy(buddy,0) != ((MsimUser *)((void *)0))) 
    return ;
  purple_debug_info("msim","msim_add_buddy: want to add %s to %s\n",name,((gname != 0)?gname : "(no group)"));
  msg = msim_msg_new("addbuddy",'f',!0,"sesskey",'i',(session -> sesskey),"reason",'s',g_strdup(""),((void *)((void *)0)));
/* "newprofileid" will be inserted here with uid. */
  if (!(msim_postprocess_outgoing(session,msg,name,"newprofileid","reason") != 0)) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Failed to add buddy"))),((const char *)(dgettext("pidgin","\'addbuddy\' command failed."))),0,0);
    msim_msg_free(msg);
    return ;
  }
  msim_msg_free(msg);
/* TODO: if addbuddy fails ('error' message is returned), delete added buddy from
	 * buddy list since Purple adds it locally. */
  body = msim_msg_new("ContactID",'s',g_strdup("<uid>"),"GroupName",'s',g_strdup(gname),"Position",'i',1000,"Visibility",'i',1,"NickName",'s',g_strdup(""),"NameSelect",'i',0,((void *)((void *)0)));
/* TODO: Update blocklist. */
  msg_persist = msim_msg_new("persist",'i',1,"sesskey",'i',(session -> sesskey),"cmd",'i',512 | 2,"dsn",'i',MC_CONTACT_INFO_DSN,"uid",'i',(session -> userid),"lid",'i',MC_CONTACT_INFO_LID,"rid",'i',session -> next_rid++,"body",100,body,((void *)((void *)0)));
/* TODO: Use msim_new_reply_callback to get rid. */
  if (!(msim_postprocess_outgoing(session,msg_persist,name,"body",0) != 0)) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Failed to add buddy"))),((const char *)(dgettext("pidgin","persist command failed"))),0,0);
    msim_msg_free(msg_persist);
    return ;
  }
  msim_msg_free(msg_persist);
/* Add to allow list, remove from block list */
  msim_update_blocklist_for_buddy(session,name,(!0),0);
}
/**
 * Remove a buddy from the user's buddy list.
 */

static void msim_remove_buddy(PurpleConnection *gc,PurpleBuddy *buddy,PurpleGroup *group)
{
  MsimSession *session;
  GList *delbuddy_msg;
  GList *persist_msg;
  const char *name;
  session = ((MsimSession *)(gc -> proto_data));
  name = purple_buddy_get_name(buddy);
  delbuddy_msg = msim_msg_new("delbuddy",'f',!0,"sesskey",'i',(session -> sesskey),((void *)((void *)0)));
/* 'delprofileid' with uid will be inserted here. */
  if (!(msim_postprocess_outgoing(session,delbuddy_msg,name,"delprofileid",0) != 0)) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Failed to remove buddy"))),((const char *)(dgettext("pidgin","\'delbuddy\' command failed"))),0,0);
    msim_msg_free(delbuddy_msg);
    return ;
  }
  msim_msg_free(delbuddy_msg);
  persist_msg = msim_msg_new("persist",'i',1,"sesskey",'i',(session -> sesskey),"cmd",'i',512 | 3,"dsn",'i',MD_DELETE_BUDDY_DSN,"lid",'i',MD_DELETE_BUDDY_LID,"uid",'i',(session -> userid),"rid",'i',session -> next_rid++,"body",'s',g_strdup("ContactID=<uid>"),((void *)((void *)0)));
/* <uid> will be replaced by postprocessing */
  if (!(msim_postprocess_outgoing(session,persist_msg,name,"body",0) != 0)) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Failed to remove buddy"))),((const char *)(dgettext("pidgin","persist command failed"))),0,0);
    msim_msg_free(persist_msg);
    return ;
  }
  msim_msg_free(persist_msg);
/*
	 * Remove from our approve list and from our block list (this
	 * doesn't seem like it would be necessary, but the official client
	 * does it)
	 */
  if (!(msim_update_blocklist_for_buddy(session,name,0,0) != 0)) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Failed to remove buddy"))),((const char *)(dgettext("pidgin","blocklist command failed"))),0,0);
    return ;
  }
  msim_buddy_free(buddy);
}
/**
 * Remove a buddy from the user's buddy list and add them to the block list.
 */

static void msim_add_deny(PurpleConnection *gc,const char *name)
{
  MsimSession *session;
  GList *msg;
  GList *body;
  session = ((MsimSession *)(gc -> proto_data));
/* Remove from buddy list */
  msg = msim_msg_new("delbuddy",'f',!0,"sesskey",'i',(session -> sesskey),((void *)((void *)0)));
/* 'delprofileid' with uid will be inserted here. */
  if (!(msim_postprocess_outgoing(session,msg,name,"delprofileid",0) != 0)) 
    purple_debug_error("myspace","delbuddy command failed\n");
  msim_msg_free(msg);
/* Remove from our approve list and add to our block list */
  msim_update_blocklist_for_buddy(session,name,0,(!0));
/*
	 * Add the buddy to our list of blocked contacts, so we know they
	 * are blocked if we log in with another client
	 */
  body = msim_msg_new("ContactID",'s',g_strdup("<uid>"),"Visibility",'i',2,((void *)((void *)0)));
  msg = msim_msg_new("persist",'i',1,"sesskey",'i',(session -> sesskey),"cmd",'i',512 | 2,"dsn",'i',MC_CONTACT_INFO_DSN,"lid",'i',MC_CONTACT_INFO_LID,"rid",'i',session -> next_rid++,"body",100,body,((void *)((void *)0)));
  if (!(msim_postprocess_outgoing(session,msg,name,"body",0) != 0)) 
    purple_debug_error("myspace","add to block list command failed\n");
  msim_msg_free(msg);
/*
	 * TODO: MySpace doesn't allow blocked buddies on our buddy list,
	 *       do they?  If not then we need to remove the buddy from
	 *       libpurple's buddy list.
	 */
}
/**
 * Remove a buddy from the user's block list.
 */

static void msim_rem_deny(PurpleConnection *gc,const char *name)
{
  MsimSession *session;
  GList *msg;
  GList *body;
  session = ((MsimSession *)(gc -> proto_data));
/*
	 * Remove from our list of blocked contacts, so we know they
	 * are no longer blocked if we log in with another client
	 */
  body = msim_msg_new("ContactID",'s',g_strdup("<uid>"),((void *)((void *)0)));
  msg = msim_msg_new("persist",'i',1,"sesskey",'i',(session -> sesskey),"cmd",'i',512 | 3,"dsn",'i',MC_DELETE_CONTACT_INFO_DSN,"lid",'i',MC_DELETE_CONTACT_INFO_LID,"rid",'i',session -> next_rid++,"body",100,body,((void *)((void *)0)));
  if (!(msim_postprocess_outgoing(session,msg,name,"body",0) != 0)) 
    purple_debug_error("myspace","remove from block list command failed\n");
  msim_msg_free(msg);
/* Remove from our approve list and our block list */
  msim_update_blocklist_for_buddy(session,name,0,0);
}
/**
 * Returns a string of a username in canonical form. Basically removes all the
 * spaces, lowercases the string, and looks up user IDs to usernames.
 * Normalizing tom, TOM, Tom, and 6221 wil all return 'tom'.
 *
 * Borrowed this code from oscar_normalize. Added checking for
 * "if userid, get name before normalizing"
 */

static const char *msim_normalize(const PurpleAccount *account,const char *str)
{
  static char normalized[2048UL];
  char *tmp1;
  char *tmp2;
  int i;
  int j;
  guint id;
  do {
    if (str != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"str != NULL");
      return 0;
    };
  }while (0);
  if (msim_is_userid(str) != 0) {
/* Have user ID, we need to get their username first :) */
    const char *username;
/* If the account does not exist, we can't look up the user. */
    if (!(account != 0) || !((account -> gc) != 0)) 
      return str;
    id = (atol(str));
    username = msim_uid2username_from_blist(((PurpleAccount *)account),id);
    if (!(username != 0)) {
/* Not in buddy list... scheisse... TODO: Manual Lookup! Bug #4631 */
/* Note: manual lookup using msim_lookup_user() is a problem inside
			 * msim_normalize(), because msim_lookup_user() calls a callback function
			 * when the user information has been looked up, but msim_normalize() expects
			 * the result immediately. */
      strncpy(normalized,str,2048);
    }
    else {
      strncpy(normalized,username,2048);
    }
  }
  else {
/* Have username. */
    strncpy(normalized,str,2048);
  }
/* Strip spaces. */
  for (((i = 0) , (j = 0)); normalized[j] != 0; j++) {
    if (normalized[j] != 32) 
      normalized[i++] = normalized[j];
  }
  normalized[i] = 0;
/* Lowercase and perform UTF-8 normalization. */
  tmp1 = g_utf8_strdown(normalized,(-1));
  tmp2 = g_utf8_normalize(tmp1,(-1),G_NORMALIZE_DEFAULT);
  g_snprintf(normalized,(sizeof(normalized)),"%s",tmp2);
  g_free(tmp2);
  g_free(tmp1);
/* TODO: re-add caps and spacing back to what the user wanted.
	 * User can format their own names, for example 'msimprpl' is shown
	 * as 'MsIm PrPl' in the official client.
	 *
	 * TODO: file a ticket to add this enhancement.
	 */
  return normalized;
}
/**
 * Return whether the buddy can be messaged while offline.
 *
 * The protocol supports offline messages in just the same way as online
 * messages.
 */

static gboolean msim_offline_message(const PurpleBuddy *buddy)
{
  return (!0);
}
/**
 * Send raw data to the server, possibly with embedded NULs.
 *
 * Used in prpl_info struct, so that plugins can have the most possible
 * control of what is sent over the connection. Inside this prpl,
 * msim_send_raw() is used, since it sends NUL-terminated strings (easier).
 *
 * @param gc PurpleConnection
 * @param buf Buffer to send
 * @param total_bytes Size of buffer to send
 *
 * @return Bytes successfully sent, or -1 on error.
 */
/*
 * TODO: This needs to do non-blocking writes and use a watcher to check
  *      when the fd is available to be written to.
 */

static int msim_send_really_raw(PurpleConnection *gc,const char *buf,int total_bytes)
{
  int total_bytes_sent;
  MsimSession *session;
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return -1;
    };
  }while (0);
  do {
    if (buf != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buf != NULL");
      return -1;
    };
  }while (0);
  do {
    if (total_bytes >= 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"total_bytes >= 0");
      return -1;
    };
  }while (0);
  session = ((MsimSession *)(gc -> proto_data));
/* Loop until all data is sent, or a failure occurs. */
  total_bytes_sent = 0;
  do {
    int bytes_sent;
    bytes_sent = (send((session -> fd),(buf + total_bytes_sent),(total_bytes - total_bytes_sent),0));
    if (bytes_sent < 0) {
      purple_debug_info("msim","msim_send_raw(%s): send() failed: %s\n",buf,g_strerror( *__errno_location()));
      return total_bytes_sent;
    }
    total_bytes_sent += bytes_sent;
  }while (total_bytes_sent < total_bytes);
  return total_bytes_sent;
}
/**
 * Send raw data (given as a NUL-terminated string) to the server.
 *
 * @param session
 * @param msg The raw data to send, in a NUL-terminated string.
 *
 * @return TRUE if succeeded, FALSE if not.
 *
 */

gboolean msim_send_raw(MsimSession *session,const gchar *msg)
{
  size_t len;
  do {
    if (msg != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg != NULL");
      return 0;
    };
  }while (0);
  purple_debug_info("msim","msim_send_raw: writing <%s>\n",msg);
  len = strlen(msg);
  return (msim_send_really_raw((session -> gc),msg,len)) == len;
}

static GHashTable *msim_get_account_text_table(PurpleAccount *unused)
{
  GHashTable *table;
  table = g_hash_table_new(g_str_hash,g_str_equal);
  g_hash_table_insert(table,"login_label",((gpointer )((const char *)(dgettext("pidgin","Email Address...")))));
  return table;
}
/**
 * Callbacks called by Purple, to access this plugin.
 */
static PurplePluginProtocolInfo prpl_info = {((OPT_PROTO_USE_POINTSIZE | OPT_PROTO_MAIL_CHECK)), ((GList *)((void *)0)), ((GList *)((void *)0)), 
/* options */
/* specify font size in sane point size */
/* | OPT_PROTO_IM_IMAGE - TODO: direct images. */
/* user_splits */
/* protocol_options */
/* icon_spec - TODO: eventually should add this */
{((char *)((void *)0)), (0), (0), (0), (0), (0), (0)}, (msim_list_icon), ((const char *(*)(PurpleBuddy *))((void *)0)), (msim_status_text), (msim_tooltip_text), (msim_status_types), (msim_blist_node_menu), ((GList *(*)(PurpleConnection *))((void *)0)), ((GHashTable *(*)(PurpleConnection *, const char *))((void *)0)), (msim_login), (msim_close), (msim_send_im), ((void (*)(PurpleConnection *, const char *))((void *)0)), (msim_send_typing), (msim_get_info), (msim_set_status), (msim_set_idle), ((void (*)(PurpleConnection *, const char *, const char *))((void *)0)), (msim_add_buddy), ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)), (msim_remove_buddy), ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), (msim_add_deny), ((void (*)(PurpleConnection *, const char *))((void *)0)), (msim_rem_deny), ((void (*)(PurpleConnection *))((void *)0)), ((void (*)(PurpleConnection *, GHashTable *))((void *)0)), ((void (*)(PurpleConnection *, GHashTable *))((void *)0)), ((char *(*)(GHashTable *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *, const char *))((void *)0)), ((void (*)(PurpleConnection *, int ))((void *)0)), ((void (*)(PurpleConnection *, int , const char *, const char *))((void *)0)), ((int (*)(PurpleConnection *, int , const char *, PurpleMessageFlags ))((void *)0)), ((void (*)(PurpleConnection *))((void *)0)), ((void (*)(PurpleAccount *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *, const char *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *, PurpleGroup *, GList *))((void *)0)), (msim_buddy_free), ((void (*)(PurpleConnection *, const char *))((void *)0)), (msim_normalize), ((void (*)(PurpleConnection *, PurpleStoredImage *))((void *)0)), ((void (*)(PurpleConnection *, PurpleGroup *))((void *)0)), ((char *(*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((PurpleChat *(*)(PurpleAccount *, const char *))((void *)0)), ((PurpleRoomlist *(*)(PurpleConnection *))((void *)0)), ((void (*)(PurpleRoomlist *))((void *)0)), ((void (*)(PurpleRoomlist *, PurpleRoomlistRoom *))((void *)0)), ((gboolean (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *, const char *))((void *)0)), ((PurpleXfer *(*)(PurpleConnection *, const char *))((void *)0)), (msim_offline_message), ((PurpleWhiteboardPrplOps *)((void *)0)), (msim_send_really_raw), ((char *(*)(PurpleRoomlistRoom *))((void *)0)), ((void (*)(PurpleAccount *, PurpleAccountUnregistrationCb , void *))((void *)0)), (msim_send_attention), (msim_attention_types), ((sizeof(PurplePluginProtocolInfo ))), (msim_get_account_text_table), ((gboolean (*)(PurpleAccount *, const char *, PurpleMediaSessionType ))((void *)0)), ((PurpleMediaCaps (*)(PurpleAccount *, const char *))((void *)0)), ((PurpleMood *(*)(PurpleAccount *))((void *)0)), ((void (*)(PurpleConnection *, const char *, PurpleSetPublicAliasSuccessCallback , PurpleSetPublicAliasFailureCallback ))((void *)0)), ((void (*)(PurpleConnection *, PurpleGetPublicAliasSuccessCallback , PurpleGetPublicAliasFailureCallback ))((void *)0)), ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *, const char *))((void *)0)), ((void (*)(PurpleConnection *, GList *, GList *, const char *))((void *)0))
/* list_icon */
/* list_emblems */
/* status_text */
/* tooltip_text */
/* status_types */
/* blist_node_menu */
/* chat_info */
/* chat_info_defaults */
/* login */
/* close */
/* send_im */
/* set_info */
/* send_typing */
/* get_info */
/* set_status */
/* set_idle */
/* change_passwd */
/* add_buddy */
/* add_buddies */
/* remove_buddy */
/* remove_buddies */
/* add_permit */
/* add_deny */
/* rem_permit */
/* rem_deny */
/* set_permit_deny */
/* join_chat */
/* reject chat invite */
/* get_chat_name */
/* chat_invite */
/* chat_leave */
/* chat_whisper */
/* chat_send */
/* keepalive */
/* register_user */
/* get_cb_info */
/* get_cb_away */
/* alias_buddy */
/* group_buddy */
/* rename_group */
/* buddy_free */
/* convo_closed */
/* normalize */
/* set_buddy_icon */
/* remove_group */
/* get_cb_real_name */
/* set_chat_topic */
/* find_blist_chat */
/* roomlist_get_list */
/* roomlist_cancel */
/* roomlist_expand_category */
/* can_receive_file */
/* send_file */
/* new_xfer */
/* offline_message */
/* whiteboard_prpl_ops */
/* send_raw */
/* roomlist_room_serialize */
/* unregister_user */
/* send_attention */
/* attention_types */
/* struct_size */
/* get_account_text_table */
/* initiate_media */
/* get_media_caps */
/* get_moods */
/* set_public_alias */
/* get_public_alias */
/* add_buddy_with_invite */
/* add_buddies_with_invite */
};
/**
 * Load the plugin.
 */

static gboolean msim_load(PurplePlugin *plugin)
{
/* If compiled to use RC4 from libpurple, check if it is really there. */
  if (!(purple_ciphers_find_cipher("rc4") != 0)) {
    purple_debug_error("msim","rc4 not in libpurple, but it is required - not loading MySpaceIM plugin!\n");
    purple_notify_message(plugin,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Missing Cipher"))),((const char *)(dgettext("pidgin","The RC4 cipher could not be found"))),((const char *)(dgettext("pidgin","Upgrade to a libpurple with RC4 support (>= 2.0.1). MySpaceIM plugin will not be loaded."))),0,0);
    return 0;
  }
  return (!0);
}
/**
 * Called when friends have been imported to buddy list on server.
 */

static void msim_import_friends_cb(MsimSession *session,const GList *reply,gpointer user_data)
{
  GList *body;
  gchar *completed;
/* Check if the friends were imported successfully. */
  body = msim_msg_get_dictionary(reply,"body");
  do {
    if (body != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"body != NULL");
      return ;
    };
  }while (0);
  completed = msim_msg_get_string(body,"Completed");
  msim_msg_free(body);
  do {
    if (completed != ((gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"completed != NULL");
      return ;
    };
  }while (0);
  if (!(g_str_equal(completed,"True") != 0)) {
    purple_debug_info("msim_import_friends_cb","failed to import friends: %s",completed);
    purple_notify_message((session -> account),PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Add friends from MySpace.com"))),((const char *)(dgettext("pidgin","Importing friends failed"))),0,0,0);
    g_free(completed);
    return ;
  }
  g_free(completed);
  purple_debug_info("msim_import_friends_cb","added friends to server-side buddy list, requesting new contacts from server");
  msim_get_contact_list(session,1);
/* TODO: show, X friends have been added */
}
/**
 * Import friends from myspace.com.
 */

static void msim_import_friends(PurplePluginAction *action)
{
  PurpleConnection *gc;
  MsimSession *session;
  gchar *group_name;
  gc = ((PurpleConnection *)(action -> context));
  session = ((MsimSession *)(gc -> proto_data));
  group_name = "MySpace Friends";
  do {
    if (msim_send(session,"persist",'i',1,"sesskey",'i',(session -> sesskey),"cmd",'i',2,"dsn",'i',MC_IMPORT_ALL_FRIENDS_DSN,"lid",'i',MC_IMPORT_ALL_FRIENDS_LID,"uid",'i',(session -> userid),"rid",'i',msim_new_reply_callback(session,msim_import_friends_cb,0),"body",'s',g_strdup_printf("GroupName=%s",group_name),((void *)((void *)0))) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msim_send(session, \"persist\", MSIM_TYPE_INTEGER, 1, \"sesskey\", MSIM_TYPE_INTEGER, session->sesskey, \"cmd\", MSIM_TYPE_INTEGER, MSIM_CMD_PUT, \"dsn\", MSIM_TYPE_INTEGER, MC_IMPORT_ALL_FRIENDS_DSN, \"lid\", MSIM_TYPE_INTEGER, MC_IMPORT_ALL_FRIENDS_LID, \"uid\", MSIM_TYPE_INTEGER, session->userid, \"rid\", MSIM_TYPE_INTEGER, msim_new_reply_callback(session, msim_import_friends_cb, NULL), \"body\", MSIM_TYPE_STRING, g_strdup_printf(\"GroupName=%s\", group_name), NULL)");
      return ;
    };
  }while (0);
}
/**
 * Actions menu for account.
 */

static GList *msim_actions(
/* PurpleConnection* */
PurplePlugin *plugin,gpointer context)
{
  GList *menu;
  PurplePluginAction *act;
  menu = ((GList *)((void *)0));
#if 0
/* TODO: find out how */
#endif
  act = purple_plugin_action_new(((const char *)(dgettext("pidgin","Add friends from MySpace.com"))),msim_import_friends);
  menu = g_list_append(menu,act);
  return menu;
}
/**
 * Based on MSN's plugin info comments.
 */
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_PROTOCOL), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("prpl-myspace"), ("MySpaceIM"), ("0.18"), ("MySpaceIM Protocol Plugin"), ("MySpaceIM Protocol Plugin"), ("Jeff Connelly <jeff2@soc.pidgin.im>"), ("http://developer.pidgin.im/wiki/MySpaceIM/"), (msim_load), ((gboolean (*)(PurplePlugin *))((void *)0)), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((&prpl_info)), ((PurplePluginUiInfo *)((void *)0)), (msim_actions), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/**< prefs_info     */
/**< msim_actions   */
/**< reserved1      */
/**< reserved2      */
/**< reserved3      */
/**< reserved4      */
};
#ifdef MSIM_SELF_TEST
/*
 * Test functions.
 * Used to test or try out the internal workings of msimprpl. If you're reading
 * this code for the first time, these functions can be instructive in learning
 * how msimprpl is architected.
 */
/**
 * Test MsimMessage for basic functionality.
 */
/* Create a new, empty message. */
/* Append some new elements. */
/* Try some of the more advanced functionality */
/* msg2 now 'owns' msg */
/**
 * Test protocol-level escaping/unescaping.
 */
#endif
#ifdef MSIM_CHECK_NEWER_VERSION
/**
 * Callback for when a currentversion.txt has been downloaded.
 */
/* Prepend [group] so that GKeyFile can parse it (requires a group). */
/* url_text is variable=data\n...†*/
/* Check FILEVER, 1.0.716.0. 716 is build, MSIM_CLIENT_VERSION */
/* New (english) version can be downloaded from SETUPURL+SETUPFILE */
/* Default list seperator is ;, but currentversion.txt doesn't have
	 * these, so set to an unused character to avoid parsing problems. */
#endif
/**
 Handle a myim:addContact command, after username has been looked up.
 */

static void msim_uri_handler_addContact_cb(MsimSession *session,GList *userinfo,gpointer data)
{
  GList *body;
  gchar *username;
  body = msim_msg_get_dictionary(userinfo,"body");
  username = msim_msg_get_string(body,"UserName");
  msim_msg_free(body);
  if (!(username != 0)) {
    guint uid;
    uid = msim_msg_get_integer(userinfo,"UserID");
    do {
      if (uid != 0) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"uid != 0");
        return ;
      };
    }while (0);
    username = g_strdup_printf("%d",uid);
  }
  purple_blist_request_add_buddy((session -> account),username,((const char *)(dgettext("pidgin","Buddies"))),0);
  g_free(username);
}
/* TODO: move uid->username resolving to IM sending and buddy adding functions,
 * so that user can manually add or IM by userid and username automatically
 * looked up if possible? */
/**
 * Handle a myim:sendIM URI command, after username has been looked up.
 */

static void msim_uri_handler_sendIM_cb(MsimSession *session,GList *userinfo,gpointer data)
{
  PurpleConversation *conv;
  GList *body;
  gchar *username;
  body = msim_msg_get_dictionary(userinfo,"body");
  username = msim_msg_get_string(body,"UserName");
  msim_msg_free(body);
  if (!(username != 0)) {
    guint uid;
    uid = msim_msg_get_integer(userinfo,"UserID");
    do {
      if (uid != 0) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"uid != 0");
        return ;
      };
    }while (0);
    username = g_strdup_printf("%d",uid);
  }
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,username,(session -> account));
  if (!(conv != 0)) {
    purple_debug_info("msim_uri_handler","creating new conversation for %s\n",username);
    conv = purple_conversation_new(PURPLE_CONV_TYPE_IM,(session -> account),username);
  }
/* Just open the window so the user can send an IM. */
  purple_conversation_present(conv);
  g_free(username);
}

static gboolean msim_uri_handler(const gchar *proto,const gchar *cmd,GHashTable *params)
{
  PurpleAccount *account;
  MsimSession *session;
  GList *l;
  gchar *uid_str;
  gchar *cid_str;
  guint uid;
  guint cid;
  if (g_ascii_strcasecmp(proto,"myim") != 0) 
    return 0;
/* Parameters are case-insensitive. */
  uid_str = (g_hash_table_lookup(params,"uid"));
  cid_str = (g_hash_table_lookup(params,"cid"));
  uid = (((uid_str != 0)?atol(uid_str) : 0));
  cid = (((cid_str != 0)?atol(cid_str) : 0));
/* Need a contact. */
  do {
    if (cid != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cid != 0");
      return 0;
    };
  }while (0);
/* TODO: if auto=true, "Add all the people on this page to my IM List!", on
	 * http://collect.myspace.com/index.cfm?fuseaction=im.friendslist. Don't need a cid. */
/* Convert numeric contact ID back to a string. Needed for looking up. Don't just
	 * directly use cid directly from parameters, because it might not be numeric.
	 * It is trivial to change this to allow cID to be a username, but that's not how
	 * the official MySpaceIM client works, so don't provide that functionality. */
  cid_str = g_strdup_printf("%d",cid);
/* Find our account with specified user id, or use first connected account if uid=0. */
  account = ((PurpleAccount *)((void *)0));
  l = purple_accounts_get_all();
{
    while(l != 0){
      if ((purple_account_is_connected((l -> data)) != 0) && ((uid == 0) || ((purple_account_get_int((l -> data),"uid",0)) == uid))) {
        account = (l -> data);
        break; 
      }
      l = (l -> next);
    }
  }
  if (!(account != 0)) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","myim URL handler"))),((const char *)(dgettext("pidgin","No suitable MySpaceIM account could be found to open this myim URL."))),((const char *)(dgettext("pidgin","Enable the proper MySpaceIM account and try again."))),0,0);
    g_free(cid_str);
    return 0;
  }
  session = ((MsimSession *)( *(account -> gc)).proto_data);
  do {
    if (session != ((MsimSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return 0;
    };
  }while (0);
/* Lookup userid to username. TODO: push this down, to IM sending/contact
	 * adding functions. */
/* myim:sendIM?uID=USERID&cID=CONTACTID */
  if (!(g_ascii_strcasecmp(cmd,"sendIM") != 0)) {
    msim_lookup_user(session,cid_str,((MSIM_USER_LOOKUP_CB )msim_uri_handler_sendIM_cb),0);
    g_free(cid_str);
    return (!0);
/* myim:addContact?uID=USERID&cID=CONTACTID */
  }
  else if (!(g_ascii_strcasecmp(cmd,"addContact") != 0)) {
    msim_lookup_user(session,cid_str,((MSIM_USER_LOOKUP_CB )msim_uri_handler_addContact_cb),0);
    g_free(cid_str);
    return (!0);
  }
  return 0;
}
/**
 * Initialize plugin.
 */

static void init_plugin(PurplePlugin *plugin)
{
#ifdef MSIM_SELF_TEST
#endif /* MSIM_SELF_TEST */
  PurpleAccountOption *option;
  static gboolean initialized = 0;
#ifdef MSIM_CHECK_NEWER_VERSION
/* PROBLEM: MySpace's servers always return Content-Location, and
	 * libpurple redirects to it, infinitely, even though it is the same
	 * location we requested! */
/* not full URL */
/* user agent */
/* use HTTP/1.1 */
#endif
/* TODO: default to automatically try different ports. Make the user be
	 * able to set the first port to try (like LastConnectedPort in Windows client).  */
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","Connect server"))),"server","im.myspace.akadns.net");
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_int_new(((const char *)(dgettext("pidgin","Connect port"))),"port",1863);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
#ifdef MSIM_USER_WANTS_TO_CONFIGURE_STATUS_TEXT
#endif
#ifdef MSIM_USER_WANTS_TO_DISABLE_EMOTICONS
#endif
#ifdef MSIM_USER_REALLY_CARES_ABOUT_PRECISE_FONT_SIZES
#endif
/* Code below only runs once. Based on oscar.c's oscar_init(). */
  if (initialized != 0) 
    return ;
  initialized = (!0);
  purple_signal_connect((purple_get_core()),"uri-handler",(&initialized),((PurpleCallback )msim_uri_handler),0);
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
