/** MySpaceIM protocol messages
 *
 * \author Jeff Connelly
 *
 * Copyright (C) 2007, Jeff Connelly <jeff2@soc.pidgin.im>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "myspace.h"
#include "message.h"
static void msim_msg_debug_string_element(gpointer data,gpointer user_data);
/**
 * Escape codes and associated replacement text, used for protocol message
 * escaping and unescaping.
 */
static struct MSIM_ESCAPE_REPLACEMENT {
gchar *code;
gchar text;}msim_escape_replacements[] = {{("/1"), ('/')}, {("/2"), ('\\')}, 
/* { "/3", "|" }, */
/* Not used here -- only for within arrays */
{((gchar *)((void *)0)), (0)}};
/**
 * Escape a protocol message.
 *
 * @return The escaped message. Caller must g_free().
 */

gchar *msim_escape(const gchar *msg)
{
  GString *gs;
  guint i;
  guint j;
  guint msg_len;
  gs = g_string_new("");
  msg_len = (strlen(msg));
  for (i = 0; i < msg_len; ++i) {
    struct MSIM_ESCAPE_REPLACEMENT *replacement;
    gchar *replace;
    replace = ((gchar *)((void *)0));
{
/* Check for characters that need to be escaped, and escape them. */
      for (j = 0; ((replacement = (msim_escape_replacements + j)) != 0) && ((replacement -> code) != ((gchar *)((void *)0))); ++j) {
        if (msg[i] == (replacement -> text)) {
          replace = (replacement -> code);
          break; 
        }
      }
    }
    if (replace != 0) {
      g_string_append(gs,replace);
    }
    else {
      g_string_append_c_inline(gs,msg[i]);
    }
  }
#ifdef MSIM_DEBUG_ESCAPE
#endif
  return g_string_free(gs,0);
}
/**
 * Unescape a protocol message.
 *
 * @return The unescaped message, caller must g_free().
 */

gchar *msim_unescape(const gchar *msg)
{
  GString *gs;
  guint i;
  guint j;
  guint msg_len;
  gs = g_string_new("");
  msg_len = (strlen(msg));
  for (i = 0; i < msg_len; ++i) {
    struct MSIM_ESCAPE_REPLACEMENT *replacement;
    gchar replace;
    replace = msg[i];
{
      for (j = 0; ((replacement = (msim_escape_replacements + j)) != 0) && ((replacement -> code) != ((gchar *)((void *)0))); ++j) {
        if (((msg[i] == (replacement -> code)[0]) && ((i + 1) < msg_len)) && (msg[i + 1] == (replacement -> code)[1])) {
          replace = (replacement -> text);
          ++i;
          break; 
        }
      }
    }
    g_string_append_c_inline(gs,replace);
  }
#ifdef MSIM_DEBUG_ESCAPE
#endif
  return g_string_free(gs,0);
}
/**
 * Create a new message from va_list and its first argument.
 *
 * @param first_key The first argument (a key), or NULL to take all arguments
 *    from argp.
 * @param argp A va_list of variadic arguments, already started with va_start().
 * @return New MsimMessage *, must be freed with msim_msg_free().
 *
 * For internal use - users probably want msim_msg_new() or msim_send().
 */

static GList *msim_msg_new_v(gchar *first_key,va_list argp)
{
  gchar *key;
  gchar *value;
  MsimMessageType type;
  GList *msg;
  gboolean first;
  GString *gs;
  GList *gl;
  GList *dict;
/* Begin with an empty message. */
  msg = ((GList *)((void *)0));
/* First parameter can be given explicitly. */
  first = (first_key != ((gchar *)((void *)0)));
{
/* Read key, type, value triplets until NULL. */
    do {
      if (first != 0) {
        key = first_key;
        first = 0;
      }
      else {
        key = (va_arg(argp,gchar *));
        if (!(key != 0)) {
          break; 
        }
      }
      type = (va_arg(argp,int ));
/* Interpret variadic arguments. */
      switch(type){
        case 'f':
{
        }
        case 'i':
{
          msg = msim_msg_append(msg,key,type,((gpointer )((gulong )(va_arg(argp,int )))));
          break; 
        }
        case 's':
{
          value = (va_arg(argp,char *));
          do {
            if (value != ((gchar *)((void *)0))) {
            }
            else {
              g_return_if_fail_warning(0,((const char *)__func__),"value != NULL");
              return 0;
            };
          }while (0);
          msg = msim_msg_append(msg,key,type,value);
          break; 
        }
        case 'b':
{
          gs = (va_arg(argp,GString *));
          do {
            if (gs != ((GString *)((void *)0))) {
            }
            else {
              g_return_if_fail_warning(0,((const char *)__func__),"gs != NULL");
              return 0;
            };
          }while (0);
/* msim_msg_free() will free this GString the caller created. */
          msg = msim_msg_append(msg,key,type,gs);
          break; 
        }
        case 'l':
{
          gl = (va_arg(argp,GList *));
          do {
            if (gl != ((GList *)((void *)0))) {
            }
            else {
              g_return_if_fail_warning(0,((const char *)__func__),"gl != NULL");
              return 0;
            };
          }while (0);
          msg = msim_msg_append(msg,key,type,gl);
          break; 
        }
        case 'd':
{
          dict = (va_arg(argp,GList *));
          do {
            if (dict != ((GList *)((void *)0))) {
            }
            else {
              g_return_if_fail_warning(0,((const char *)__func__),"dict != NULL");
              return 0;
            };
          }while (0);
          msg = msim_msg_append(msg,key,type,dict);
          break; 
        }
        default:
{
          purple_debug_info("msim","msim_send: unknown type %d\n",type);
          break; 
        }
      }
    }while (key != 0);
  }
  return msg;
}
/**
 * Create a new MsimMessage.
 *
 * @param first_key The first key in the sequence, or NULL for an empty message.
 * @param ... A sequence of gchar* key/type/value triplets, terminated with NULL.
 *
 * See msim_msg_append() documentation for details on types.
 */

GList *msim_msg_new(gchar *first_key,... )
{
  GList *ret = (GList *)((void *)0);
  va_list argp;
  if (first_key != 0) {
    va_start(argp,first_key);
    ret = msim_msg_new_v(first_key,argp);
    va_end(argp);
  }
  return ret;
}
/**
 * Pack a string using the given GFunc and seperator.
 * Used by msim_msg_dump() and msim_msg_pack().
 */

static gchar *msim_msg_pack_using(GList *msg,GFunc gf,const gchar *sep,const gchar *begin,const gchar *end)
{
  int num_items;
  gchar **strings;
  gchar **strings_tmp;
  gchar *joined;
  gchar *final;
  int i;
  do {
    if (msg != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg != NULL");
      return 0;
    };
  }while (0);
  num_items = (g_list_length(msg));
/* Add one for NULL terminator for g_strjoinv(). */
  strings = ((gchar **)(g_malloc0_n((num_items + 1),(sizeof(gchar *)))));
  strings_tmp = strings;
  g_list_foreach(msg,gf,(&strings_tmp));
  joined = g_strjoinv(sep,strings);
  final = g_strconcat(begin,joined,end,((void *)((void *)0)));
  g_free(joined);
/* Clean up. */
  for (i = 0; i < num_items; ++i) {
    g_free(strings[i]);
  }
  g_free(strings);
  return final;
}
/**
 * Return a human-readable string of the message.
 *
 * @return A new gchar *, must be g_free()'d.
 */

static gchar *msim_msg_dump_to_str(GList *msg)
{
  gchar *debug_str;
  if (!(msg != 0)) {
    debug_str = g_strdup("<MsimMessage: empty>");
  }
  else {
    debug_str = msim_msg_pack_using(msg,msim_msg_debug_string_element,"\n","<MsimMessage: \n","\n/MsimMessage>");
  }
  return debug_str;
}
/**
 * Store a human-readable string describing the element.
 *
 * @param data Pointer to an MsimMessageElement.
 * @param user_data
 */

static void msim_msg_debug_string_element(gpointer data,gpointer user_data)
{
  MsimMessageElement *elem;
  gchar *string;
  GString *gs;
  gchar *binary;
/* wow, a pointer to a pointer to a pointer */
  gchar ***items;
  gchar *s;
  GList *gl;
  guint i;
  elem = ((MsimMessageElement *)data);
  items = user_data;
  switch(elem -> type){
    case 'i':
{
      string = g_strdup_printf("%s(integer): %d",(elem -> name),((guint )((gulong )(elem -> data))));
      break; 
    }
    case '-':
{
      string = g_strdup_printf("%s(raw): %s",(elem -> name),(((elem -> data) != 0)?((gchar *)(elem -> data)) : "(NULL)"));
      break; 
    }
    case 's':
{
      string = g_strdup_printf("%s(string): %s",(elem -> name),(((elem -> data) != 0)?((gchar *)(elem -> data)) : "(NULL)"));
      break; 
    }
    case 'b':
{
      gs = ((GString *)(elem -> data));
      binary = purple_base64_encode(((guchar *)(gs -> str)),(gs -> len));
      string = g_strdup_printf("%s(binary, %d bytes): %s",(elem -> name),((int )(gs -> len)),binary);
      g_free(binary);
      break; 
    }
    case 'f':
{
      string = g_strdup_printf("%s(boolean): %s",(elem -> name),(((elem -> data) != 0)?"TRUE" : "FALSE"));
      break; 
    }
    case 'd':
{
      if (!((elem -> data) != 0)) {
        s = g_strdup("(NULL)");
      }
      else {
        s = msim_msg_dump_to_str(((GList *)(elem -> data)));
      }
      if (!(s != 0)) {
        s = g_strdup("(NULL, couldn\'t msim_msg_dump_to_str)");
      }
      string = g_strdup_printf("%s(dict): %s",(elem -> name),s);
      g_free(s);
      break; 
    }
    case 108:
{
      gs = g_string_new("");
      g_string_append_printf(gs,"%s(list): \n",(elem -> name));
      i = 0;
      for (gl = ((GList *)(elem -> data)); gl != ((GList *)((void *)0)); gl = ((gl != 0)?( *((GList *)gl)).next : ((struct _GList *)((void *)0)))) {
        g_string_append_printf(gs," %d. %s\n",i,((gchar *)(gl -> data)));
        ++i;
      }
      string = g_string_free(gs,0);
      break; 
    }
    default:
{
      string = g_strdup_printf("%s(unknown type %d",(((elem -> name) != 0)?(elem -> name) : "(NULL)"),(elem -> type));
      break; 
    }
  }
   *( *items) = string;
  ++( *items);
}
/**
 * Search for and return the node in msg, matching name, or NULL.
 *
 * @param msg Message to search within.
 * @param name Field name to search for.
 *
 * @return The GList * node for the MsimMessageElement with the given name, or NULL if not found or name is NULL.
 *
 * For internal use - users probably want to use msim_msg_get() to
 * access the MsimMessageElement *, instead of the GList * container.
 *
 */

static GList *msim_msg_get_node(const GList *msg,const gchar *name)
{
  GList *node;
  if (!(name != 0) || !(msg != 0)) {
    return 0;
  }
/* Linear search for the given name. O(n) but n is small. */
  for (node = ((GList *)msg); node != ((GList *)((void *)0)); node = ((node != 0)?( *((GList *)node)).next : ((struct _GList *)((void *)0)))) {
    MsimMessageElement *elem;
    elem = ((MsimMessageElement *)(node -> data));
    do {
      if (elem != ((MsimMessageElement *)((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"elem != NULL");
        return 0;
      };
    }while (0);
    do {
      if ((elem -> name) != ((const gchar *)((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"elem->name != NULL");
        return 0;
      };
    }while (0);
    if (strcmp((elem -> name),name) == 0) {
      return node;
    }
  }
  return 0;
}
/**
 * Create a new MsimMessageElement * - must be g_free()'d.
 *
 * For internal use; users probably want msim_msg_append() or msim_msg_insert_before().
 *
 * @param dynamic_name Whether 'name' should be freed when the message is destroyed.
 */

static MsimMessageElement *msim_msg_element_new(const gchar *name,MsimMessageType type,gpointer data,gboolean dynamic_name)
{
  MsimMessageElement *elem;
  elem = ((MsimMessageElement *)(g_malloc0_n(1,(sizeof(MsimMessageElement )))));
  elem -> name = name;
  elem -> dynamic_name = dynamic_name;
  elem -> type = type;
  elem -> data = data;
  return elem;
}
/**
 * Append a new element to a message.
 *
 * @param name Textual name of element (static string, neither copied nor freed).
 * @param type An MSIM_TYPE_* code.
 * @param data Pointer to data, see below.
 *
 * @return The new message - must be assigned to as with GList*. For example:
 *
 *     msg = msim_msg_append(msg, ...)
 *
 * The data parameter depends on the type given:
 *
 * * MSIM_TYPE_INTEGER: Use GUINT_TO_POINTER(x).
 *
 * * MSIM_TYPE_BINARY: Same as integer, non-zero is TRUE and zero is FALSE.
 *
 * * MSIM_TYPE_STRING: gchar *. The data WILL BE FREED - use g_strdup() if needed.
 *
 * * MSIM_TYPE_RAW: gchar *. The data WILL BE FREED - use g_strdup() if needed.
 *
 * * MSIM_TYPE_BINARY: g_string_new_len(data, length). The data AND GString will be freed.
 *
 * * MSIM_TYPE_DICTIONARY: An MsimMessage *. Freed when message is destroyed.
 *
 * * MSIM_TYPE_LIST: GList * of gchar *. Again, everything will be freed.
 *
 * */

GList *msim_msg_append(GList *msg,const gchar *name,MsimMessageType type,gpointer data)
{
  return g_list_append(msg,(msim_msg_element_new(name,type,data,0)));
}
/**
 * Append a new element, but with a dynamically-allocated name.
 * Exactly the same as msim_msg_append(), except 'name' will be freed when
 * the message is destroyed. Normally, it isn't, because a static string is given.
 */

static GList *msim_msg_append_dynamic_name(GList *msg,gchar *name,MsimMessageType type,gpointer data)
{
  return g_list_append(msg,(msim_msg_element_new(name,type,data,(!0))));
}
/**
 * Insert a new element into a message, before the given element name.
 *
 * @param name_before Name of the element to insert the new element before. If
 *                    could not be found or NULL, new element will be inserted at end.
 *
 * See msim_msg_append() for usage of other parameters, and an important note about return value.
 */

GList *msim_msg_insert_before(GList *msg,const gchar *name_before,const gchar *name,MsimMessageType type,gpointer data)
{
  MsimMessageElement *new_elem;
  GList *node_before;
  new_elem = msim_msg_element_new(name,type,data,0);
  node_before = msim_msg_get_node(msg,name_before);
  return g_list_insert_before(msg,node_before,new_elem);
}
/**
 * Perform a deep copy on a GList * of gchar * strings. Free with msim_msg_list_free().
 */

static GList *msim_msg_list_copy(const GList *old)
{
  GList *new_list;
  new_list = ((GList *)((void *)0));
/* Deep copy (g_list_copy is shallow). Copy each string. */
  for (; old != ((const GList *)((void *)0)); old = (((old != 0)?( *((GList *)old)).next : ((struct _GList *)((void *)0))))) {
    new_list = g_list_append(new_list,(g_strdup((old -> data))));
  }
  return new_list;
}
/**
 * Clone an individual element.
 *
 * @param data MsimMessageElement * to clone.
 * @param user_data Pointer to MsimMessage * to add cloned element to.
 */

static void msim_msg_clone_element(gpointer data,gpointer user_data)
{
  MsimMessageElement *elem;
  GList **new;
  gpointer new_data;
  GString *gs;
  GList *dict;
  elem = ((MsimMessageElement *)data);
  new = ((GList **)user_data);
  switch(elem -> type){
    case 'f':
{
    }
    case 'i':
{
      new_data = (elem -> data);
      break; 
    }
    case '-':
{
    }
    case 's':
{
      new_data = (g_strdup(((gchar *)(elem -> data))));
      break; 
    }
    case 108:
{
      new_data = ((gpointer )(msim_msg_list_copy(((GList *)(elem -> data)))));
      break; 
    }
    case 'b':
{
      gs = ((GString *)(elem -> data));
      new_data = (g_string_new_len((gs -> str),(gs -> len)));
      break; 
    }
    case 'd':
{
      dict = ((GList *)(elem -> data));
      new_data = (msim_msg_clone(dict));
      break; 
    }
    default:
{
      purple_debug_info("msim","msim_msg_clone_element: unknown type %d\n",(elem -> type));
      do {
        g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","message.c",584,((const char *)__func__));
        return ;
      }while (0);
    }
  }
/* Append cloned data. Note that the 'name' field is a static string, so it
	 * never needs to be copied nor freed. */
  if ((elem -> dynamic_name) != 0) 
     *new = msim_msg_append_dynamic_name( *new,g_strdup((elem -> name)),(elem -> type),new_data);
  else 
     *new = msim_msg_append( *new,(elem -> name),(elem -> type),new_data);
}
/**
 * Clone an existing MsimMessage.
 *
 * @return Cloned message; caller should free with msim_msg_free().
 */

GList *msim_msg_clone(GList *old)
{
  GList *new;
  if (old == ((GList *)((void *)0))) {
    return 0;
  }
  new = msim_msg_new(0);
  g_list_foreach(old,msim_msg_clone_element,(&new));
  return new;
}
/**
 * Free the data of a message element.
 *
 * @param elem The MsimMessageElement *
 *
 * Note this only frees the element data; you may also want to free the
 * element itself with g_free() (see msim_msg_free_element()).
 */

void msim_msg_free_element_data(MsimMessageElement *elem)
{
  switch(elem -> type){
    case 'f':
{
    }
    case 'i':
{
/* Integer value stored in gpointer - no need to free(). */
      break; 
    }
    case '-':
{
    }
    case 's':
{
/* Always free strings - caller should have g_strdup()'d if
			 * string was static or temporary and not to be freed. */
      g_free((elem -> data));
      break; 
    }
    case 'b':
{
/* Free the GString itself and the binary data. */
      g_string_free(((GString *)(elem -> data)),(!0));
      break; 
    }
    case 'd':
{
      msim_msg_free(((GList *)(elem -> data)));
      break; 
    }
    case 108:
{
      g_list_free(((GList *)(elem -> data)));
      break; 
    }
    default:
{
      purple_debug_info("msim","msim_msg_free_element_data: not freeing unknown type %d\n",(elem -> type));
      break; 
    }
  }
}
/**
 * Free a GList * of MsimMessageElement *'s.
 */

void msim_msg_list_free(GList *l)
{
  for (; l != ((GList *)((void *)0)); l = ((l != 0)?( *((GList *)l)).next : ((struct _GList *)((void *)0)))) {
    MsimMessageElement *elem;
    elem = ((MsimMessageElement *)(l -> data));
/* Note that name is almost never dynamically allocated elsewhere;
		 * it is usually a static string, but not in lists. So cast it. */
    g_free(((gchar *)(elem -> name)));
    g_free((elem -> data));
    g_free(elem);
  }
  g_list_free(l);
}
/**
 * Free an individual message element.
 *
 * @param data MsimMessageElement * to free.
 * @param user_data Not used; required to match g_list_foreach() callback prototype.
 *
 * Frees both the element data and the element itself.
 * Also frees the name if dynamic_name is TRUE.
 */

static void msim_msg_free_element(gpointer data,gpointer user_data)
{
  MsimMessageElement *elem;
  elem = ((MsimMessageElement *)data);
  msim_msg_free_element_data(elem);
  if ((elem -> dynamic_name) != 0) 
/* Need to cast to remove const-ness, because
		 * elem->name is almost always a constant, static
		 * string, but not in this case. */
    g_free(((gchar *)(elem -> name)));
  g_free(elem);
}
/**
 * Free a complete message.
 */

void msim_msg_free(GList *msg)
{
  if (!(msg != 0)) {
/* already free as can be */
    return ;
  }
  g_list_foreach(msg,msim_msg_free_element,0);
  g_list_free(msg);
}
/**
 * Pack an element into its protocol representation.
 *
 * @param data Pointer to an MsimMessageElement.
 * @param user_data Pointer to a gchar ** array of string items.
 *
 * Called by msim_msg_pack(). Will pack the MsimMessageElement into
 * a part of the protocol string and append it to the array. Caller
 * is responsible for creating array to correct dimensions, and
 * freeing each string element of the array added by this function.
 */

static void msim_msg_pack_element(gpointer data,gpointer user_data)
{
  MsimMessageElement *elem;
  gchar *string;
  gchar *data_string;
  gchar ***items;
  elem = ((MsimMessageElement *)data);
  items = ((gchar ***)user_data);
/* Exclude elements beginning with '_' from packed protocol messages. */
  if ((elem -> name)[0] == '_') {
    return ;
  }
  data_string = msim_msg_pack_element_data(elem);
  switch(elem -> type){
/* These types are represented by key name/value pairs (converted above). */
    case '-':
{
    }
    case 'b':
{
    }
    case 'd':
{
    }
    case 'i':
{
    }
    case 108:
{
    }
    case 's':
{
      string = g_strconcat((elem -> name),"\\",data_string,((void *)((void *)0)));
      break; 
    }
/* Boolean is represented by absence or presence of name. */
    case 'f':
{
      if (((guint )((gulong )(elem -> data))) != 0U) {
/* True - leave in, with blank value. */
        string = g_strdup_printf("%s\\",(elem -> name));
      }
      else {
/* False - leave out. */
        string = g_strdup("");
      }
      break; 
    }
    default:
{
      g_free(data_string);
      do {
        g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","message.c",775,((const char *)__func__));
        return ;
      }while (0);
      break; 
    }
  }
  g_free(data_string);
   *( *items) = string;
  ++( *items);
}
/**
 * Pack an element into its protcol representation inside a dictionary.
 *
 * See msim_msg_pack_element().
 */

static void msim_msg_pack_element_dict(gpointer data,gpointer user_data)
{
  MsimMessageElement *elem;
  gchar *string;
  gchar *data_string;
  gchar ***items;
  elem = ((MsimMessageElement *)data);
  items = ((gchar ***)user_data);
/* Exclude elements beginning with '_' from packed protocol messages. */
  if ((elem -> name)[0] == '_') {
    return ;
  }
  data_string = msim_msg_pack_element_data(elem);
  do {
    if (data_string != ((gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"data_string != NULL");
      return ;
    };
  }while (0);
  switch(elem -> type){
/* These types are represented by key name/value pairs (converted above). */
    case '-':
{
    }
    case 'b':
{
    }
    case 'd':
{
    }
/* Boolean is On or Off */
    case 'f':
{
    }
    case 'i':
{
    }
    case 108:
{
    }
    case 's':
{
      string = g_strconcat((elem -> name),"=",data_string,((void *)((void *)0)));
      break; 
    }
    default:
{
      g_free(data_string);
      do {
        if (0) {
        }
        else {
          g_return_if_fail_warning(0,((const char *)__func__),"FALSE");
          return ;
        };
      }while (0);
      break; 
    }
  }
  g_free(data_string);
   *( *items) = string;
  ++( *items);
}
/**
 * Return a packed string of a message suitable for sending over the wire.
 *
 * @return A string. Caller must g_free().
 */

gchar *msim_msg_pack(GList *msg)
{
  do {
    if (msg != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg != NULL");
      return 0;
    };
  }while (0);
  return msim_msg_pack_using(msg,msim_msg_pack_element,"\\","\\","\\final\\");
}
/**
 * Return a packed string of a dictionary, suitable for embedding in MSIM_TYPE_DICTIONARY.
 *
 * @return A string; caller must g_free().
 */

static gchar *msim_msg_pack_dict(GList *msg)
{
  do {
    if (msg != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg != NULL");
      return 0;
    };
  }while (0);
  return msim_msg_pack_using(msg,msim_msg_pack_element_dict,"\034","","");
}
/**
 * Send an existing MsimMessage.
 */

gboolean msim_msg_send(MsimSession *session,GList *msg)
{
  gchar *raw;
  gboolean success;
  raw = msim_msg_pack(msg);
  do {
    if (raw != ((gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"raw != NULL");
      return 0;
    };
  }while (0);
  success = msim_send_raw(session,raw);
  g_free(raw);
  return success;
}
/**
 * Return a message element data as a new string for a raw protocol message,
 * converting from other types (integer, etc.) if necessary.
 *
 * @return const gchar * The data as a string, or NULL. Caller must g_free().
 *
 * Returns a string suitable for inclusion in a raw protocol message, not necessarily
 * optimal for human consumption. For example, strings are escaped. Use
 * msim_msg_get_string() if you want a string, which in some cases is same as this.
 */

gchar *msim_msg_pack_element_data(MsimMessageElement *elem)
{
  GString *gs;
  GList *gl;
  do {
    if (elem != ((MsimMessageElement *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"elem != NULL");
      return 0;
    };
  }while (0);
  switch(elem -> type){
    case 'i':
{
      return g_strdup_printf("%d",((guint )((gulong )(elem -> data))));
    }
    case '-':
{
/* Not un-escaped - this is a raw element, already escaped if necessary. */
      return g_strdup(((gchar *)(elem -> data)));
    }
    case 's':
{
/* Strings get escaped. msim_escape() creates a new string. */
      do {
        if ((elem -> data) != ((void *)((void *)0))) {
        }
        else {
          g_return_if_fail_warning(0,((const char *)__func__),"elem->data != NULL");
          return 0;
        };
      }while (0);
      return ((elem -> data) != 0)?msim_escape(((gchar *)(elem -> data))) : g_strdup("(NULL)");
    }
    case 'b':
{
      gs = ((GString *)(elem -> data));
/* Do not escape! */
      return purple_base64_encode(((guchar *)(gs -> str)),(gs -> len));
    }
    case 'f':
{
/* Not used by messages in the wire protocol * -- see msim_msg_pack_element.
			 * Only used by dictionaries, see msim_msg_pack_element_dict. */
      return ((elem -> data) != 0)?g_strdup("On") : g_strdup("Off");
    }
    case 'd':
{
      return msim_msg_pack_dict(((GList *)(elem -> data)));
    }
    case 108:
{
/* Pack using a|b|c|d|... */
      gs = g_string_new("");
      for (gl = ((GList *)(elem -> data)); gl != ((GList *)((void *)0)); gl = ((gl != 0)?( *((GList *)gl)).next : ((struct _GList *)((void *)0)))) {
        g_string_append_printf(gs,"%s",((gchar *)(gl -> data)));
/* All but last element is separated by a bar. */
        if ((((gl != 0)?( *((GList *)gl)).next : ((struct _GList *)((void *)0)))) != 0) 
          g_string_append(gs,"|");
      }
      return g_string_free(gs,0);
    }
    default:
{
      purple_debug_info("msim","field %s, unknown type %d\n",(((elem -> name) != 0)?(elem -> name) : "(NULL)"),(elem -> type));
      return 0;
    }
  }
}
/**
 * Send a message to the server, whose contents is specified using
 * variable arguments.
 *
 * @param session
 * @param ... A sequence of gchar* key/type/value triplets, terminated with NULL.
 *
 * This function exists for coding convenience: it allows a message to be created
 * and sent in one line of code. Internally it calls msim_msg_send().
 *
 * IMPORTANT: See msim_msg_append() documentation for details on element types.
 *
 */

gboolean msim_send(MsimSession *session,... )
{
  gboolean success;
  GList *msg;
  va_list argp;
  va_start(argp,session);
  msg = msim_msg_new_v(0,argp);
  va_end(argp);
/* Actually send the message. */
  success = msim_msg_send(session,msg);
/* Cleanup. */
  msim_msg_free(msg);
  return success;
}
/**
 * Print a human-readable string of the message to Purple's debug log.
 *
 * @param fmt_string A static string, in which '%s' will be replaced.
 */

void msim_msg_dump(const gchar *fmt_string,GList *msg)
{
  gchar *debug_str;
  do {
    if (fmt_string != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"fmt_string != NULL");
      return ;
    };
  }while (0);
  debug_str = msim_msg_dump_to_str(msg);
  do {
    if (debug_str != ((gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"debug_str != NULL");
      return ;
    };
  }while (0);
  purple_debug_info("msim",fmt_string,debug_str);
  g_free(debug_str);
}
/**
 * Parse a raw protocol message string into a MsimMessage *.
 *
 * @param raw The raw message string to parse, will be g_free()'d.
 *
 * @return MsimMessage *. Caller should msim_msg_free() when done.
 */

GList *msim_parse(const gchar *raw)
{
  GList *msg;
  gchar *token;
  gchar **tokens;
  gchar *key;
  gchar *value;
  int i;
  do {
    if (raw != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"raw != NULL");
      return 0;
    };
  }while (0);
  purple_debug_info("msim","msim_parse: got <%s>\n",raw);
  key = ((gchar *)((void *)0));
/* All messages begin with a \. */
  if ((raw[0] != '\\') || (raw[1] == 0)) {
    purple_debug_info("msim","msim_parse: incomplete/bad string, missing initial backslash: <%s>\n",raw);
/* XXX: Should we try to recover, and read to first backslash? */
    return 0;
  }
  msg = msim_msg_new(0);
  for (((tokens = g_strsplit((raw + 1),"\\",0)) , (i = 0)); (token = tokens[i]) != 0; i++) {
#ifdef MSIM_DEBUG_PARSE
#endif
    if ((i % 2) != 0) {
/* Odd-numbered ordinal is a value. */
      value = token;
/* Incoming protocol messages get tagged as MSIM_TYPE_RAW, which
			 * represents an untyped piece of data. msim_msg_get_* will
			 * convert to appropriate types for caller, and handle unescaping if needed. */
      msg = msim_msg_append_dynamic_name(msg,g_strdup(key),'-',(g_strdup(value)));
#ifdef MSIM_DEBUG_PARSE
#endif
    }
    else {
/* Even numbered indexes are key names. */
      key = token;
    }
  }
  g_strfreev(tokens);
  return msg;
}
/**
 * Return the first MsimMessageElement * with given name in the MsimMessage *.
 *
 * @param name Name to search for.
 *
 * @return MsimMessageElement * matching name, or NULL.
 *
 * Note: useful fields of MsimMessageElement are 'data' and 'type', which
 * you can access directly. But it is often more convenient to use
 * another msim_msg_get_* that converts the data to what type you want.
 */

MsimMessageElement *msim_msg_get(const GList *msg,const gchar *name)
{
  GList *node;
  node = msim_msg_get_node(msg,name);
  if (node != 0) {
    return (MsimMessageElement *)(node -> data);
  }
  else {
    return 0;
  }
}

gchar *msim_msg_get_string_from_element(MsimMessageElement *elem)
{
  do {
    if (elem != ((MsimMessageElement *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"elem != NULL");
      return 0;
    };
  }while (0);
  switch(elem -> type){
    case 'i':
{
      return g_strdup_printf("%d",((guint )((gulong )(elem -> data))));
    }
    case '-':
{
/* Raw element from incoming message - if its a string, it'll
			 * be escaped. */
      return msim_unescape(((gchar *)(elem -> data)));
    }
    case 's':
{
/* Already unescaped. */
      return g_strdup(((gchar *)(elem -> data)));
    }
    default:
{
      purple_debug_info("msim","msim_msg_get_string_element: type %d unknown, name %s\n",(elem -> type),(((elem -> name) != 0)?(elem -> name) : "(NULL)"));
      return 0;
    }
  }
}
/**
 * Return the data of an element of a given name, as a string.
 *
 * @param name Name of element.
 *
 * @return gchar * The data as a string, or NULL if not found.
 *     Caller must g_free().
 *
 * Note that msim_msg_pack_element_data() is similar, but returns a string
 * for inclusion into a raw protocol string (escaped and everything).
 * This function unescapes the string for you, if needed.
 */

gchar *msim_msg_get_string(const GList *msg,const gchar *name)
{
  MsimMessageElement *elem;
  elem = msim_msg_get(msg,name);
  if (!(elem != 0)) {
    return 0;
  }
  return msim_msg_get_string_from_element(elem);
}
/**
 * Parse a |-separated string into a new GList. Free with msim_msg_list_free().
 */

static GList *msim_msg_list_parse(const gchar *raw)
{
  gchar **array;
  GList *list;
  guint i;
  array = g_strsplit(raw,"|",0);
  list = ((GList *)((void *)0));
/* TODO: escape/unescape /3 <-> | within list elements */
  for (i = 0; array[i] != ((gchar *)((void *)0)); ++i) {
    MsimMessageElement *elem;
/* Freed in msim_msg_list_free() */
    elem = ((MsimMessageElement *)(g_malloc0_n(1,(sizeof(MsimMessageElement )))));
/* Give the element a name for debugging purposes.
		 * Not supposed to be looked up by this name; instead,
		 * lookup the elements by indexing the array. */
    elem -> name = (g_strdup_printf("(list item #%d)",i));
    elem -> type = '-';
    elem -> data = (g_strdup(array[i]));
    list = g_list_append(list,elem);
  }
  g_strfreev(array);
  return list;
}

static GList *msim_msg_get_list_from_element(MsimMessageElement *elem)
{
  do {
    if (elem != ((MsimMessageElement *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"elem != NULL");
      return 0;
    };
  }while (0);
  switch(elem -> type){
    case 108:
{
      return msim_msg_list_copy(((GList *)(elem -> data)));
    }
    case '-':
{
      return msim_msg_list_parse(((gchar *)(elem -> data)));
    }
    default:
{
      purple_debug_info("msim_msg_get_list","type %d unknown, name %s\n",(elem -> type),(((elem -> name) != 0)?(elem -> name) : "(NULL)"));
      return 0;
    }
  }
}
/**
 * Return an element as a new list. Caller frees with msim_msg_list_free().
 */

GList *msim_msg_get_list(const GList *msg,const gchar *name)
{
  MsimMessageElement *elem;
  elem = msim_msg_get(msg,name);
  if (!(elem != 0)) {
    return 0;
  }
  return msim_msg_get_list_from_element(elem);
}
/**
 * Parse a \x1c-separated "dictionary" of key=value pairs into a hash table.
 *
 * @param raw The text of the dictionary to parse. Often the
 *                 value for the 'body' field.
 *
 * @return A new MsimMessage *. Must msim_msg_free() when done.
 */

static GList *msim_msg_dictionary_parse(const gchar *raw)
{
  GList *dict;
  gchar *item;
  gchar **items;
  gchar **elements;
  guint i;
  do {
    if (raw != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"raw != NULL");
      return 0;
    };
  }while (0);
  dict = msim_msg_new(0);
{
    for (((items = g_strsplit(raw,"\034",0)) , (i = 0)); (item = items[i]) != 0; i++) {
      gchar *key;
      gchar *value;
      elements = g_strsplit(item,"=",2);
      key = elements[0];
      if (!(key != 0)) {
        purple_debug_info("msim","msim_msg_dictionary_parse(%s): null key\n",raw);
        g_strfreev(elements);
        break; 
      }
      value = elements[1];
      if (!(value != 0)) {
        purple_debug_info("msim","msim_msg_dictionary_prase(%s): null value\n",raw);
        g_strfreev(elements);
        break; 
      }
#ifdef MSIM_DEBUG_PARSE
#endif
/* Append with _dynamic_name since g_strdup(key) is dynamic, and
		 * needs to be freed when the message is destroyed. It isn't static as usual. */
      dict = msim_msg_append_dynamic_name(dict,g_strdup(key),'-',(g_strdup(value)));
      g_strfreev(elements);
    }
  }
  g_strfreev(items);
  return dict;
}

static GList *msim_msg_get_dictionary_from_element(MsimMessageElement *elem)
{
  do {
    if (elem != ((MsimMessageElement *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"elem != NULL");
      return 0;
    };
  }while (0);
  switch(elem -> type){
    case 'd':
{
      return msim_msg_clone(((GList *)(elem -> data)));
    }
    case '-':
{
      return msim_msg_dictionary_parse((elem -> data));
    }
    default:
{
      purple_debug_info("msim_msg_get_dictionary","type %d unknown, name %s\n",(elem -> type),(((elem -> name) != 0)?(elem -> name) : "(NULL)"));
      return 0;
    }
  }
}
/**
 * Return an element as a new dictionary. Caller frees with msim_msg_free().
 */

GList *msim_msg_get_dictionary(const GList *msg,const gchar *name)
{
  MsimMessageElement *elem;
  elem = msim_msg_get(msg,name);
  if (!(elem != 0)) {
    return 0;
  }
  return msim_msg_get_dictionary_from_element(elem);
}

guint msim_msg_get_integer_from_element(MsimMessageElement *elem)
{
  do {
    if (elem != ((MsimMessageElement *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"elem != NULL");
      return 0;
    };
  }while (0);
  switch(elem -> type){
    case 'i':
{
      return (guint )((gulong )(elem -> data));
    }
    case '-':
{
    }
    case 's':
{
/* TODO: find out if we need larger integers */
      return (guint )(atoi(((gchar *)(elem -> data))));
    }
    default:
{
      return 0;
    }
  }
}
/**
 * Return the data of an element of a given name, as an unsigned integer.
 *
 * @param name Name of element.
 *
 * @return guint Numeric representation of data, or 0 if could not be converted / not found.
 *
 * Useful to obtain an element's data if you know it should be an integer,
 * even if it is not stored as an MSIM_TYPE_INTEGER. MSIM_TYPE_STRING will
 * be converted handled correctly, for example.
 */

guint msim_msg_get_integer(const GList *msg,const gchar *name)
{
  MsimMessageElement *elem;
  elem = msim_msg_get(msg,name);
  if (!(elem != 0)) {
    return 0;
  }
  return msim_msg_get_integer_from_element(elem);
}

static gboolean msim_msg_get_binary_from_element(MsimMessageElement *elem,gchar **binary_data,gsize *binary_length)
{
  GString *gs;
  do {
    if (elem != ((MsimMessageElement *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"elem != NULL");
      return 0;
    };
  }while (0);
  switch(elem -> type){
    case '-':
{
/* Incoming messages are tagged with MSIM_TYPE_RAW, and
			 * converted appropriately. They can still be "strings", just they won't
			 * be tagged as MSIM_TYPE_STRING (as MSIM_TYPE_STRING is intended to be used
			 * by msimprpl code for things like instant messages - stuff that should be
			 * escaped if needed). DWIM.
			 */
/* Previously, incoming messages were stored as MSIM_TYPE_STRING.
			 * This was fine for integers and strings, since they can easily be
			 * converted in msim_get_*, as desirable. However, it does not work
			 * well for binary strings. Consider:
			 *
			 * If incoming base64'd elements were tagged as MSIM_TYPE_STRING.
			 * msim_msg_get_binary() sees MSIM_TYPE_STRING, base64 decodes, returns.
			 * everything is fine.
			 * But then, msim_send() is called on the incoming message, which has
			 * a base64'd MSIM_TYPE_STRING that really is encoded binary. The values
			 * will be escaped since strings are escaped, and / becomes /2; no good.
			 *
			 */
       *binary_data = ((gchar *)(purple_base64_decode(((const gchar *)(elem -> data)),binary_length)));
      return  *binary_data != ((gchar *)((void *)0));
    }
    case 'b':
{
      gs = ((GString *)(elem -> data));
/* Duplicate data, so caller can g_free() it. */
       *binary_data = (g_memdup((gs -> str),(gs -> len)));
       *binary_length = (gs -> len);
      return (!0);
    }
/* Rejected because if it isn't already a GString, have to g_new0 it and
			 * then caller has to ALSO free the GString!
			 *
			 * return (GString *)elem->data; */
    default:
{
      purple_debug_info("msim","msim_msg_get_binary: unhandled type %d for key %s\n",(elem -> type),(((elem -> name) != 0)?(elem -> name) : "(NULL)"));
      return 0;
    }
  }
}
/**
 * Return the data of an element of a given name, as a binary GString.
 *
 * @param binary_data A pointer to a new pointer, which will be filled in with the binary data. CALLER MUST g_free().
 *
 * @param binary_length A pointer to an integer, which will be set to the binary data length.
 *
 * @return TRUE if successful, FALSE if not.
 */

gboolean msim_msg_get_binary(const GList *msg,const gchar *name,gchar **binary_data,gsize *binary_length)
{
  MsimMessageElement *elem;
  elem = msim_msg_get(msg,name);
  if (!(elem != 0)) {
    return 0;
  }
  return msim_msg_get_binary_from_element(elem,binary_data,binary_length);
}
