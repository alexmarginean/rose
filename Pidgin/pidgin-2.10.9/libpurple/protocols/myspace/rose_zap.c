/* MySpaceIM Protocol Plugin - zap support
 *
 * Copyright (C) 2007, Jeff Connelly <jeff2@soc.pidgin.im>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "myspace.h"
#include "zap.h"
/** Get zap types. */

GList *msim_attention_types(PurpleAccount *acct)
{
  static GList *types = (GList *)((void *)0);
  PurpleAttentionType *attn;
  if (!(types != 0)) {
#define _MSIM_ADD_NEW_ATTENTION(icn, ulname, nme, incoming, outgoing) \
		attn = purple_attention_type_new(ulname, nme, incoming, outgoing); \
		purple_attention_type_set_icon_name(attn, icn); \
		types = g_list_append(types, attn);
/* TODO: icons for each zap */
/* Lots of comments for translators: */
/* Zap means "to strike suddenly and forcefully as if with a
		 * projectile or weapon."  This term often has an electrical
		 * connotation, for example, "he was zapped by electricity when
		 * he put a fork in the toaster." */
    attn = purple_attention_type_new("Zap",((const char *)(dgettext("pidgin","Zap"))),((const char *)(dgettext("pidgin","%s has zapped you!"))),((const char *)(dgettext("pidgin","Zapping %s..."))));
    purple_attention_type_set_icon_name(attn,0);
    types = g_list_append(types,attn);;
/* Whack means "to hit or strike someone with a sharp blow" */
    attn = purple_attention_type_new("Whack",((const char *)(dgettext("pidgin","Whack"))),((const char *)(dgettext("pidgin","%s has whacked you!"))),((const char *)(dgettext("pidgin","Whacking %s..."))));
    purple_attention_type_set_icon_name(attn,0);
    types = g_list_append(types,attn);;
/* Torch means "to set on fire."  Don't worry, this doesn't
		 * make a whole lot of sense in English, either.  Feel free
		 * to translate it literally. */
    attn = purple_attention_type_new("Torch",((const char *)(dgettext("pidgin","Torch"))),((const char *)(dgettext("pidgin","%s has torched you!"))),((const char *)(dgettext("pidgin","Torching %s..."))));
    purple_attention_type_set_icon_name(attn,0);
    types = g_list_append(types,attn);;
/* Smooch means "to kiss someone, often enthusiastically" */
    attn = purple_attention_type_new("Smooch",((const char *)(dgettext("pidgin","Smooch"))),((const char *)(dgettext("pidgin","%s has smooched you!"))),((const char *)(dgettext("pidgin","Smooching %s..."))));
    purple_attention_type_set_icon_name(attn,0);
    types = g_list_append(types,attn);;
/* A hug is a display of affection; wrapping your arms around someone */
    attn = purple_attention_type_new("Hug",((const char *)(dgettext("pidgin","Hug"))),((const char *)(dgettext("pidgin","%s has hugged you!"))),((const char *)(dgettext("pidgin","Hugging %s..."))));
    purple_attention_type_set_icon_name(attn,0);
    types = g_list_append(types,attn);;
/* Slap means "to hit someone with an open/flat hand" */
    attn = purple_attention_type_new("Slap",((const char *)(dgettext("pidgin","Slap"))),((const char *)(dgettext("pidgin","%s has slapped you!"))),((const char *)(dgettext("pidgin","Slapping %s..."))));
    purple_attention_type_set_icon_name(attn,0);
    types = g_list_append(types,attn);;
/* Goose means "to pinch someone on their butt" */
    attn = purple_attention_type_new("Goose",((const char *)(dgettext("pidgin","Goose"))),((const char *)(dgettext("pidgin","%s has goosed you!"))),((const char *)(dgettext("pidgin","Goosing %s..."))));
    purple_attention_type_set_icon_name(attn,0);
    types = g_list_append(types,attn);;
/* A high-five is when two people's hands slap each other
		 * in the air above their heads.  It is done to celebrate
		 * something, often a victory, or to congratulate someone. */
    attn = purple_attention_type_new("High-five",((const char *)(dgettext("pidgin","High-five"))),((const char *)(dgettext("pidgin","%s has high-fived you!"))),((const char *)(dgettext("pidgin","High-fiving %s..."))));
    purple_attention_type_set_icon_name(attn,0);
    types = g_list_append(types,attn);;
/* We're not entirely sure what the MySpace people mean by
		 * this... but we think it's the equivalent of "prank."  Or, for
		 * someone to perform a mischievous trick or practical joke. */
    attn = purple_attention_type_new("Punk",((const char *)(dgettext("pidgin","Punk"))),((const char *)(dgettext("pidgin","%s has punk\'d you!"))),((const char *)(dgettext("pidgin","Punking %s..."))));
    purple_attention_type_set_icon_name(attn,0);
    types = g_list_append(types,attn);;
/* Raspberry is a slang term for the vibrating sound made
		 * when you stick your tongue out of your mouth with your
		 * lips closed and blow.  It is typically done when
		 * gloating or bragging.  Nowadays it's a pretty silly
		 * gesture, so it does not carry a harsh negative
		 * connotation.  It is generally used in a playful tone
		 * with friends. */
    attn = purple_attention_type_new("Raspberry",((const char *)(dgettext("pidgin","Raspberry"))),((const char *)(dgettext("pidgin","%s has raspberried you!"))),((const char *)(dgettext("pidgin","Raspberrying %s..."))));
    purple_attention_type_set_icon_name(attn,0);
    types = g_list_append(types,attn);;
  }
  return types;
}
/** Send a zap to a user. */

static gboolean msim_send_zap(MsimSession *session,const gchar *username,guint code)
{
  gchar *zap_string;
  gboolean rc;
  do {
    if (session != ((MsimSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return 0;
    };
  }while (0);
  do {
    if (username != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"username != NULL");
      return 0;
    };
  }while (0);
/* Construct and send the actual zap command. */
  zap_string = g_strdup_printf("!!!ZAP_SEND!!!=RTE_BTN_ZAPS_%d",code);
  if (!(msim_send_bm(session,username,zap_string,121) != 0)) {
    purple_debug_info("msim_send_zap","msim_send_bm failed: zapping %s with %s\n",username,zap_string);
    rc = 0;
  }
  else {
    rc = (!0);
  }
  g_free(zap_string);
  return rc;
}
/** Send a zap */

gboolean msim_send_attention(PurpleConnection *gc,const gchar *username,guint code)
{
  GList *types;
  MsimSession *session;
  PurpleAttentionType *attn;
  PurpleBuddy *buddy;
  session = ((MsimSession *)(gc -> proto_data));
/* Look for this attention type, by the code index given. */
  types = msim_attention_types((gc -> account));
  attn = ((PurpleAttentionType *)(g_list_nth_data(types,code)));
  if (!(attn != 0)) {
    purple_debug_info("msim_send_attention","got invalid zap code %d\n",code);
    return 0;
  }
  buddy = purple_find_buddy((session -> account),username);
  if (!(buddy != 0)) {
    return 0;
  }
  msim_send_zap(session,username,code);
  return (!0);
}
/** Zap someone. Callback from msim_blist_node_menu zap menu. */

static void msim_send_zap_from_menu(PurpleBlistNode *node,gpointer zap_num_ptr)
{
  PurpleBuddy *buddy;
  PurpleAccount *account;
  PurpleConnection *gc;
  MsimSession *session;
  guint zap;
  if (!((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)) {
/* Only know about buddies for now. */
    return ;
  }
  do {
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_BLIST_NODE_IS_BUDDY(node)");
      return ;
    };
  }while (0);
  buddy = ((PurpleBuddy *)node);
/* Find the session */
  account = purple_buddy_get_account(buddy);
  gc = purple_account_get_connection(account);
  session = ((MsimSession *)(gc -> proto_data));
  zap = ((gint )((glong )zap_num_ptr));
  purple_prpl_send_attention((session -> gc),purple_buddy_get_name(buddy),zap);
}
/** Return menu, if any, for a buddy list node. */

GList *msim_blist_node_menu(PurpleBlistNode *node)
{
  GList *menu;
  GList *zap_menu;
  GList *types;
  PurpleMenuAction *act;
  guint i;
  if (!((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)) {
/* Only know about buddies for now. */
    return 0;
  }
  zap_menu = ((GList *)((void *)0));
/* TODO: get rid of once is accessible directly in GUI */
  types = msim_attention_types(0);
  i = 0;
  do {
    PurpleAttentionType *attn;
    attn = ((PurpleAttentionType *)(types -> data));
    act = purple_menu_action_new(purple_attention_type_get_name(attn),((PurpleCallback )msim_send_zap_from_menu),((gpointer )((gulong )i)),0);
    zap_menu = g_list_append(zap_menu,act);
    ++i;
  }while ((types = ((types != 0)?( *((GList *)types)).next : ((struct _GList *)((void *)0)))) != 0);
  act = purple_menu_action_new(((const char *)(dgettext("pidgin","Zap"))),0,0,zap_menu);
  menu = g_list_append(0,act);
  return menu;
}
/** Process an incoming zap. */

gboolean msim_incoming_zap(MsimSession *session,GList *msg)
{
  gchar *msg_text;
  gchar *username;
  gint zap;
  msg_text = msim_msg_get_string(msg,"msg");
  username = msim_msg_get_string(msg,"_username");
  do {
    if (msg_text != ((gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg_text != NULL");
      return 0;
    };
  }while (0);
  do {
    if (username != ((gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"username != NULL");
      return 0;
    };
  }while (0);
  do {
    if (sscanf(msg_text,"!!!ZAP_SEND!!!=RTE_BTN_ZAPS_%d",&zap) == 1) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"sscanf(msg_text, \"!!!ZAP_SEND!!!=RTE_BTN_ZAPS_%d\", &zap) == 1");
      return 0;
    };
  }while (0);
  zap = ((zap > 9)?9 : (((zap < 0)?0 : zap)));
  purple_prpl_got_attention((session -> gc),username,zap);
  g_free(msg_text);
  g_free(username);
  return (!0);
}
