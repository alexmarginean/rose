/*
 * nmfield.c
 *
 * Copyright (c) 2004 Novell, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA	02111-1301	USA
 *
 */
#include <string.h>
#include <stdio.h>
#include "nmfield.h"
/* Free a field value and tag */
static void _free_field(NMField *field);
/* Free a field value */
static void _free_field_value(NMField *field);
/* Make a deep copy of the field */
static void _copy_field(NMField *dest,NMField *src);
/* Make a deep copy of the field's value */
static void _copy_field_value(NMField *dest,NMField *src);
/* Create a string from a value -- for debugging */
static char *_value_to_string(NMField *field);

static NMField *_add_blank_field(NMField *fields,guint32 count)
{
  guint32 new_len;
  if (fields == ((NMField *)((void *)0))) {
    fields = ((NMField *)(g_malloc0_n(10,(sizeof(NMField )))));
    fields -> len = 10;
  }
  else {
    if ((fields -> len) < (count + 2)) {
      new_len = (count + 10);
      fields = (g_realloc(fields,(new_len * sizeof(NMField ))));
      fields -> len = new_len;
    }
  }
  return fields;
}

NMField *nm_field_add_number(NMField *fields,const char *tag,guint32 size,guint8 method,guint8 flags,guint32 value,guint8 type)
{
  guint32 count;
  NMField *field;
  count = nm_count_fields(fields);
  fields = _add_blank_field(fields,count);
  field = (fields + count);
  field -> tag = g_strdup(tag);
  field -> size = size;
  field -> method = method;
  field -> flags = flags;
  field -> value = value;
  field -> type = type;
/* Null terminate the field array */
  field = (fields + (count + 1));
  field -> tag = ((char *)((void *)0));
  field -> value = 0;
  field -> ptr_value = ((gpointer )((void *)0));
  return fields;
}

NMField *nm_field_add_pointer(NMField *fields,const char *tag,guint32 size,guint8 method,guint8 flags,gpointer value,guint8 type)
{
  guint32 count;
  NMField *field = (NMField *)((void *)0);
  count = nm_count_fields(fields);
  fields = _add_blank_field(fields,count);
  field = (fields + count);
  field -> tag = g_strdup(tag);
  field -> size = size;
  field -> method = method;
  field -> flags = flags;
  field -> ptr_value = value;
  field -> type = type;
/* Null terminate the field array */
  field = (fields + (count + 1));
  field -> tag = ((char *)((void *)0));
  field -> value = 0;
  field -> ptr_value = ((gpointer )((void *)0));
  return fields;
}

guint32 nm_count_fields(NMField *fields)
{
  guint32 count = 0;
  if (fields != 0) {
    while((fields -> tag) != ((char *)((void *)0))){
      count++;
      fields++;
    }
  }
  return count;
}

void nm_free_fields(NMField **fields)
{
  NMField *field = (NMField *)((void *)0);
  if ((fields == ((NMField **)((void *)0))) || ( *fields == ((NMField *)((void *)0)))) 
    return ;
  field =  *fields;
  while((field -> tag) != ((char *)((void *)0))){
    _free_field(field);
    field++;
  }
  g_free(( *fields));
   *fields = ((NMField *)((void *)0));
}

static void _free_field(NMField *field)
{
  if (field == ((NMField *)((void *)0))) 
    return ;
  _free_field_value(field);
  g_free((field -> tag));
}

static void _free_field_value(NMField *field)
{
  if (field == ((NMField *)((void *)0))) 
    return ;
  switch((field -> type)){
    case 2:
{
    }
    case 10:
{
    }
    case 13:
{
      g_free((field -> ptr_value));
      break; 
    }
    case 9:
{
    }
    case 12:
{
      nm_free_fields(((NMField **)(&field -> ptr_value)));
      break; 
    }
    default:
{
      break; 
    }
  }
  field -> size = 0;
  field -> ptr_value = ((gpointer )((void *)0));
}

NMField *nm_locate_field(char *tag,NMField *fields)
{
  NMField *ret_fields = (NMField *)((void *)0);
  if ((fields == ((NMField *)((void *)0))) || (tag == ((char *)((void *)0)))) {
    return 0;
  }
{
    while((fields -> tag) != ((char *)((void *)0))){
      if (g_ascii_strcasecmp((fields -> tag),tag) == 0) {
        ret_fields = fields;
        break; 
      }
      fields++;
    }
  }
  return ret_fields;
}

NMField *nm_copy_field_array(NMField *src)
{
  NMField *ptr = (NMField *)((void *)0);
  NMField *dest = (NMField *)((void *)0);
  int count;
  if (src != ((NMField *)((void *)0))) {
    count = (nm_count_fields(src) + 1);
    dest = ((NMField *)(g_malloc0_n(count,(sizeof(NMField )))));
    dest -> len = count;
    ptr = dest;
    while((src -> tag) != ((char *)((void *)0))){
      _copy_field(ptr,src);
      ptr++;
      src++;
    }
  }
  return dest;
}

static void _copy_field(NMField *dest,NMField *src)
{
  dest -> type = (src -> type);
  dest -> flags = (src -> flags);
  dest -> method = (src -> method);
  dest -> tag = g_strdup((src -> tag));
  _copy_field_value(dest,src);
}

static void _copy_field_value(NMField *dest,NMField *src)
{
  dest -> type = (src -> type);
  switch((dest -> type)){
    case 10:
{
    }
    case 13:
{
      if (((src -> size) == 0) && ((src -> ptr_value) != ((void *)((void *)0)))) {
        src -> size = (strlen(((char *)(src -> ptr_value))) + 1);
      }
    }
/* fall through */
    case 2:
{
      if (((src -> size) != 0) && ((src -> ptr_value) != ((void *)((void *)0)))) {
        dest -> ptr_value = ((char *)(g_malloc0_n((src -> size),(sizeof(char )))));
        memcpy((dest -> ptr_value),(src -> ptr_value),(src -> size));
      }
      break; 
    }
    case 9:
{
    }
    case 12:
{
      dest -> ptr_value = (nm_copy_field_array(((NMField *)(src -> ptr_value))));
      break; 
    }
    default:
{
/* numeric value */
      dest -> value = (src -> value);
      break; 
    }
  }
  dest -> size = (src -> size);
}

void nm_remove_field(NMField *field)
{
  NMField *tmp;
  guint32 len;
  if ((field != ((NMField *)((void *)0))) && ((field -> tag) != ((char *)((void *)0)))) {
    _free_field(field);
/* Move fields down */
    tmp = (field + 1);
{
      while(1){
/* Don't overwrite the size of the array */
        len = (field -> len);
         *field =  *tmp;
        field -> len = len;
        if ((tmp -> tag) == ((char *)((void *)0))) 
          break; 
        field++;
        tmp++;
      }
    }
  }
}

void nm_print_fields(NMField *fields)
{
  char *str = (char *)((void *)0);
  NMField *field = fields;
  if (fields == ((NMField *)((void *)0))) 
    return ;
  while((field -> tag) != ((char *)((void *)0))){
    if (((field -> type) == 9) || ((field -> type) == 12)) {
      printf("Subarray START: %s Method = %d\n",(field -> tag),(field -> method));
      nm_print_fields(((NMField *)(field -> ptr_value)));
      printf("Subarray END: %s\n",(field -> tag));
    }
    else {
      str = _value_to_string(field);
      printf("Tag=%s;Value=%s\n",(field -> tag),str);
      g_free(str);
      str = ((char *)((void *)0));
    }
    field++;
  }
}

static char *_value_to_string(NMField *field)
{
  char *value = (char *)((void *)0);
  if (field == ((NMField *)((void *)0))) 
    return 0;
/* This is a single value attribute */
  if ((((field -> type) == 10) || ((field -> type) == 13)) && ((field -> ptr_value) != ((void *)((void *)0)))) {
    value = g_strdup(((const char *)(field -> ptr_value)));
  }
  else if (((field -> type) == 2) && ((field -> ptr_value) != ((void *)((void *)0)))) {
    value = ((char *)(g_malloc0_n((field -> size),(sizeof(char )))));
    memcpy(value,((const char *)(field -> ptr_value)),(field -> size));
  }
  else if ((field -> type) == 11) {
    if ((field -> value) != 0U) {
      value = g_strdup("1");
    }
    else {
      value = g_strdup("0");
    }
  }
  else {
/* assume it is a number */
    value = ((char *)(g_malloc0_n(20,(sizeof(char )))));
    switch((field -> type)){
      case 3:
{
      }
      case 5:
{
      }
      case 7:
{
        value = g_strdup_printf("%ld",((long )(field -> value)));
        break; 
      }
      case 4:
{
      }
      case 6:
{
      }
      case 8:
{
        value = g_strdup_printf("%lu",((unsigned long )(field -> value)));
        break; 
      }
    }
  }
  if (value == ((char *)((void *)0))) 
    value = g_strdup("NULL");
  return value;
}
