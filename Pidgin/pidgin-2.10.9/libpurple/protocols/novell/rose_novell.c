/*
 * novell.c
 *
 * Copyright (c) 2004 Novell, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA	02111-1301	USA
 *
 */
#include "internal.h"
#include "accountopt.h"
#include "debug.h"
#include "prpl.h"
#include "server.h"
#include "nmuser.h"
#include "notify.h"
#include "util.h"
#include "sslconn.h"
#include "request.h"
#include "network.h"
#include "privacy.h"
#include "status.h"
#include "version.h"
#define DEFAULT_PORT			8300
#define NOVELL_CONNECT_STEPS	4
#define NM_ROOT_FOLDER_NAME "GroupWise Messenger"
#define NOVELL_STATUS_TYPE_AVAILABLE "available"
#define NOVELL_STATUS_TYPE_AWAY "away"
#define NOVELL_STATUS_TYPE_BUSY "busy"
#define NOVELL_STATUS_TYPE_OFFLINE "offline"
#define NOVELL_STATUS_TYPE_IDLE "idle"
#define NOVELL_STATUS_TYPE_APPEAR_OFFLINE "appearoffline"
static PurplePlugin *my_protocol = (PurplePlugin *)((void *)0);
static gboolean _is_disconnect_error(NMERR_T err);
static gboolean _check_for_disconnect(NMUser *user,NMERR_T err);
static void _send_message(NMUser *user,NMMessage *message);
static void _update_buddy_status(NMUser *user,PurpleBuddy *buddy,int status,int gmt);
static void _remove_purple_buddies(NMUser *user);
static void _add_contacts_to_purple_blist(NMUser *user,NMFolder *folder);
static void _add_purple_buddies(NMUser *user);
static void _sync_contact_list(NMUser *user);
static void _sync_privacy_lists(NMUser *user);
static void _show_info(PurpleConnection *gc,NMUserRecord *user_record,char *name);
const char *_get_conference_name(int id);
/*******************************************************************************
 * Response callbacks
 *******************************************************************************/
/* Handle login response */

static void _login_resp_cb(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  PurpleConnection *gc;
  const char *alias;
  NMERR_T rc;
  if (user == ((NMUser *)((void *)0))) 
    return ;
  gc = purple_account_get_connection((user -> client_data));
  if (gc == ((PurpleConnection *)((void *)0))) 
    return ;
  if (ret_code == 0L) {
/* Set alias for user if not set (use Full Name) */
    alias = purple_account_get_alias((user -> client_data));
    if ((alias == ((const char *)((void *)0))) || (( *alias) == 0)) {
      alias = nm_user_record_get_full_name((user -> user_record));
      if (alias != 0) 
        purple_account_set_alias((user -> client_data),alias);
    }
/* Tell Purple that we are connected */
    purple_connection_set_state(gc,PURPLE_CONNECTED);
    _sync_contact_list(user);
    rc = nm_send_set_status(user,2,0,0,0,0);
    _check_for_disconnect(user,rc);
  }
  else {
    PurpleConnectionError reason;
    char *err = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to login: %s"))),nm_error_to_string(ret_code));
    switch(ret_code){
      case 0xD100L + 12:
{
      }
      case 0xD100L + 0x0046:
{
      }
      case 0xD100L + 0x0049:
{
/* Don't attempt to auto-reconnect if our
				 * password was invalid.
				 */
        if (!(purple_account_get_remember_password((gc -> account)) != 0)) 
          purple_account_set_password((gc -> account),0);
        reason = PURPLE_CONNECTION_ERROR_AUTHENTICATION_FAILED;
        break; 
      }
      default:
{
/* FIXME: There are other reasons login could fail */
        reason = PURPLE_CONNECTION_ERROR_NETWORK_ERROR;
      }
    }
    purple_connection_error_reason(gc,reason,err);
    g_free(err);
  }
}
/* Handle getstatus response*/

static void _get_status_resp_cb(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  PurpleBuddy *buddy;
  GSList *buddies;
  GSList *bnode;
  NMUserRecord *user_record = (NMUserRecord *)resp_data;
  int status;
  if ((user == ((NMUser *)((void *)0))) || (user_record == ((NMUserRecord *)((void *)0)))) 
    return ;
  if (ret_code == 0L) {
/* Find all Purple buddies and update their statuses */
    const char *name = nm_user_record_get_display_id(user_record);
    if (name != 0) {
      buddies = purple_find_buddies(((PurpleAccount *)(user -> client_data)),name);
      for (bnode = buddies; bnode != 0; bnode = (bnode -> next)) {
        buddy = ((PurpleBuddy *)(bnode -> data));
        if (buddy != 0) {
          status = nm_user_record_get_status(user_record);
          _update_buddy_status(user,buddy,status,(time(0)));
        }
      }
      g_slist_free(buddies);
    }
  }
  else {
    purple_debug(PURPLE_DEBUG_INFO,"novell","_get_status_resp_cb(): rc = 0x%X\n",ret_code);
  }
}
/* Show an error if the rename failed */

static void _rename_contact_resp_cb(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  if (ret_code != 0L) {
    purple_debug(PURPLE_DEBUG_INFO,"novell","_rename_contact_resp_cb(): rc = 0x%X\n",ret_code);
  }
}
/* Handle the getdetails response and send the message */

static void _get_details_resp_send_msg(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  PurpleConversation *gconv;
  PurpleConnection *gc;
  NMUserRecord *user_record = (NMUserRecord *)((void *)0);
  NMContact *cntct = (NMContact *)((void *)0);
  NMConference *conf;
  NMMessage *msg = user_data;
  const char *dn = (const char *)((void *)0);
  const char *name;
  if ((user == ((NMUser *)((void *)0))) || (msg == ((NMMessage *)((void *)0)))) 
    return ;
  if (ret_code == 0L) {
    user_record = ((NMUserRecord *)resp_data);
    if (user_record != 0) {
/* Set the title for the conversation */
/* XXX - Should this be PURPLE_CONV_TYPE_IM? */
      gconv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_ANY,nm_user_record_get_display_id(user_record),((PurpleAccount *)(user -> client_data)));
      if (gconv != 0) {
        dn = nm_user_record_get_dn(user_record);
        if (dn != 0) {
          cntct = nm_find_contact(user,dn);
        }
        if (cntct != 0) {
          purple_conversation_set_title(gconv,nm_contact_get_display_name(cntct));
        }
        else {
/* Not in the contact list, try to user full name */
          name = ((char *)(nm_user_record_get_full_name(user_record)));
          if (name != 0) 
            purple_conversation_set_title(gconv,name);
        }
      }
/* Add the user record to particpant list */
      conf = nm_message_get_conference(msg);
      if (conf != 0) {
        nm_conference_add_participant(conf,user_record);
        _send_message(user,msg);
      }
    }
  }
  else {
    gc = purple_account_get_connection((user -> client_data));
    if (gc != ((PurpleConnection *)((void *)0))) {
      char *err = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to send message. Could not get details for user (%s)."))),nm_error_to_string(ret_code));
      purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,err,0,0,0);
      g_free(err);
    }
    if (msg != 0) 
      nm_release_message(msg);
  }
}
/* Set up the new PurpleBuddy based on the response from getdetails */

static void _get_details_resp_setup_buddy(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  NMUserRecord *user_record;
  NMContact *contact;
  PurpleBuddy *buddy;
  const char *alias;
  NMERR_T rc = 0L;
  if (((user == ((NMUser *)((void *)0))) || (resp_data == ((void *)((void *)0)))) || (user_data == ((void *)((void *)0)))) 
    return ;
  contact = user_data;
  if (ret_code == 0L) {
    user_record = resp_data;
    buddy = (nm_contact_get_data(contact));
    nm_contact_set_user_record(contact,user_record);
/* Set the display id */
    purple_blist_rename_buddy(buddy,nm_user_record_get_display_id(user_record));
    alias = purple_buddy_get_alias(buddy);
    if (((alias == ((const char *)((void *)0))) || (( *alias) == 0)) || (strcmp(alias,purple_buddy_get_name(buddy)) == 0)) {
      purple_blist_alias_buddy(buddy,nm_user_record_get_full_name(user_record));
/* Tell the server about the new display name */
      rc = nm_send_rename_contact(user,contact,nm_user_record_get_full_name(user_record),0,0);
      _check_for_disconnect(user,rc);
    }
/* Get initial status for the buddy */
    rc = nm_send_get_status(user,resp_data,_get_status_resp_cb,0);
    _check_for_disconnect(user,rc);
/*		nm_release_contact(contact);*/
  }
  if (contact != 0) 
    nm_release_contact(contact);
}
/* Add the new contact into the PurpleBuddy list */

static void _create_contact_resp_cb(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  NMContact *tmp_contact = (NMContact *)user_data;
  NMContact *new_contact = (NMContact *)((void *)0);
  NMFolder *folder = (NMFolder *)((void *)0);
  PurpleGroup *group;
  PurpleBuddy *buddy;
  const char *folder_name = (const char *)((void *)0);
  NMERR_T rc = 0L;
  if (user == ((NMUser *)((void *)0))) 
    return ;
  if (ret_code == 0L) {
    new_contact = ((NMContact *)resp_data);
    if ((new_contact == ((NMContact *)((void *)0))) || (tmp_contact == ((NMContact *)((void *)0)))) 
      return ;
/* Get the userid and folder name for the new contact */
    folder = nm_find_folder_by_id(user,nm_contact_get_parent_id(new_contact));
    if (folder != 0) {
      folder_name = nm_folder_get_name(folder);
    }
    if ((folder_name == ((const char *)((void *)0))) || (( *folder_name) == 0)) 
      folder_name = "GroupWise Messenger";
/* Re-add the buddy now that we got the okay from the server */
    if ((folder_name != 0) && ((group = purple_find_group(folder_name)) != 0)) {
      const char *alias = nm_contact_get_display_name(tmp_contact);
      const char *display_id = nm_contact_get_display_id(new_contact);
      if (display_id == ((const char *)((void *)0))) 
        display_id = nm_contact_get_dn(new_contact);
      if ((alias != 0) && (strcmp(alias,display_id) != 0)) {
/* The user requested an alias, tell the server about it. */
        rc = nm_send_rename_contact(user,new_contact,alias,_rename_contact_resp_cb,0);
        _check_for_disconnect(user,rc);
      }
      else {
        alias = "";
      }
/* Add it to the purple buddy list if it is not there */
      buddy = purple_find_buddy_in_group((user -> client_data),display_id,group);
      if (buddy == ((PurpleBuddy *)((void *)0))) {
        buddy = purple_buddy_new((user -> client_data),display_id,alias);
        purple_blist_add_buddy(buddy,0,group,0);
      }
/* Save the new buddy as part of the contact object */
      nm_contact_set_data(new_contact,((gpointer )buddy));
/* We need details for the user before we can setup the
			 * new Purple buddy. We always call this because the
			 * 'createcontact' response fields do not always contain
			 * everything that we need.
			 */
      nm_contact_add_ref(new_contact);
      rc = nm_send_get_details(user,nm_contact_get_dn(new_contact),_get_details_resp_setup_buddy,new_contact);
      _check_for_disconnect(user,rc);
    }
  }
  else {
    PurpleConnection *gc = purple_account_get_connection((user -> client_data));
    const char *name = nm_contact_get_dn(tmp_contact);
    char *err;
    err = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to add %s to your buddy list (%s)."))),name,nm_error_to_string(ret_code));
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,err,0,0,0);
    g_free(err);
  }
  if (tmp_contact != 0) 
    nm_release_contact(tmp_contact);
}
/* Show an error if we failed to send the message */

static void _send_message_resp_cb(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  PurpleConnection *gc;
  char *err = (char *)((void *)0);
  if (user == ((NMUser *)((void *)0))) 
    return ;
  if (ret_code != 0L) {
    gc = purple_account_get_connection((user -> client_data));
/* TODO: Improve this! message to who or for what conference? */
    err = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to send message (%s)."))),nm_error_to_string(ret_code));
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,err,0,0,0);
    g_free(err);
  }
}
/* Show an error if the remove failed */

static void _remove_contact_resp_cb(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  if (ret_code != 0L) {
/* TODO: Display an error? */
    purple_debug(PURPLE_DEBUG_INFO,"novell","_remove_contact_resp_cb(): rc = 0x%x\n",ret_code);
  }
}
/* Show an error if the remove failed */

static void _remove_folder_resp_cb(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  if (ret_code != 0L) {
/* TODO: Display an error? */
    purple_debug(PURPLE_DEBUG_INFO,"novell","_remove_folder_resp_cb(): rc = 0x%x\n",ret_code);
  }
}
/* Show an error if the move failed */

static void _move_contact_resp_cb(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  if (ret_code != 0L) {
/* TODO: Display an error? */
    purple_debug(PURPLE_DEBUG_INFO,"novell","_move_contact_resp_cb(): rc = 0x%x\n",ret_code);
  }
}
/* Show an error if the rename failed */

static void _rename_folder_resp_cb(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  if (ret_code != 0L) {
/* TODO: Display an error? */
    purple_debug(PURPLE_DEBUG_INFO,"novell","_rename_folder_resp_cb(): rc = 0x%x\n",ret_code);
  }
}

static void _sendinvite_resp_cb(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  char *err;
  PurpleConnection *gc;
  if (user == ((NMUser *)((void *)0))) 
    return ;
  if (ret_code != 0L) {
    gc = purple_account_get_connection((user -> client_data));
    err = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to invite user (%s)."))),nm_error_to_string(ret_code));
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,err,0,0,0);
    g_free(err);
    purple_debug(PURPLE_DEBUG_INFO,"novell","_sendinvite_resp_cb(): rc = 0x%x\n",ret_code);
  }
}
/* If the createconf was successful attempt to send the message,
 * otherwise display an error message to the user.
 */

static void _createconf_resp_send_msg(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  NMConference *conf;
  NMMessage *msg = user_data;
  if ((user == ((NMUser *)((void *)0))) || (msg == ((NMMessage *)((void *)0)))) 
    return ;
  if (ret_code == 0L) {
    _send_message(user,msg);
  }
  else {
    if ((conf = nm_message_get_conference(msg)) != 0) {
      PurpleConnection *gc = purple_account_get_connection((user -> client_data));
      const char *name = (const char *)((void *)0);
      char *err;
      NMUserRecord *ur;
      ur = nm_conference_get_participant(conf,0);
      if (ur != 0) 
        name = nm_user_record_get_userid(ur);
      if (name != 0) 
        err = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to send message to %s. Could not create the conference (%s)."))),name,nm_error_to_string(ret_code));
      else 
        err = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to send message. Could not create the conference (%s)."))),nm_error_to_string(ret_code));
      purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,err,0,0,0);
      g_free(err);
    }
    if (msg != 0) 
      nm_release_message(msg);
  }
}
/* Move contact to newly created folder */

static void _create_folder_resp_move_contact(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  NMContact *contact = user_data;
  NMFolder *new_folder;
  char *folder_name = resp_data;
  NMERR_T rc = 0L;
  if (((user == ((NMUser *)((void *)0))) || (folder_name == ((char *)((void *)0)))) || (contact == ((NMContact *)((void *)0)))) {
    if (folder_name != 0) 
      g_free(folder_name);
    return ;
  }
  if ((ret_code == 0L) || (ret_code == 0xD100L + 0x0026)) {
    new_folder = nm_find_folder(user,folder_name);
    if (new_folder != 0) {
/* Tell the server to move the contact to the new folder */
/*			rc = nm_send_move_contact(user, contact, new_folder,
			_move_contact_resp_cb, NULL); */
      rc = nm_send_create_contact(user,new_folder,contact,0,0);
      _check_for_disconnect(user,rc);
    }
  }
  else {
    PurpleConnection *gc = purple_account_get_connection((user -> client_data));
    char *err = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to move user %s to folder %s in the server side list. Error while creating folder (%s)."))),nm_contact_get_dn(contact),folder_name,nm_error_to_string(ret_code));
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,err,0,0,0);
    g_free(err);
  }
  if (folder_name != 0) 
    g_free(folder_name);
}
/* Add contact to newly create folder */

static void _create_folder_resp_add_contact(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  NMContact *contact = (NMContact *)user_data;
  NMFolder *folder;
  char *folder_name = (char *)resp_data;
  NMERR_T rc = 0L;
  if (((user == ((NMUser *)((void *)0))) || (folder_name == ((char *)((void *)0)))) || (contact == ((NMContact *)((void *)0)))) {
    if (contact != 0) 
      nm_release_contact(contact);
    if (folder_name != 0) 
      g_free(folder_name);
    return ;
  }
  if ((ret_code == 0L) || (ret_code == 0xD100L + 0x0026)) {
    folder = nm_find_folder(user,folder_name);
    if (folder != 0) {
      rc = nm_send_create_contact(user,folder,contact,_create_contact_resp_cb,contact);
      _check_for_disconnect(user,rc);
    }
  }
  else {
    PurpleConnection *gc = purple_account_get_connection((user -> client_data));
    const char *name = nm_contact_get_dn(contact);
    char *err = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to add %s to your buddy list. Error creating folder in server side list (%s)."))),name,nm_error_to_string(ret_code));
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,err,0,0,0);
    nm_release_contact(contact);
    g_free(err);
  }
  g_free(folder_name);
}

static void _join_conf_resp_cb(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  PurpleConversation *chat;
  PurpleConnection *gc;
  NMUserRecord *ur;
  NMConference *conference = user_data;
  const char *name;
  const char *conf_name;
  int i;
  int count;
  if ((user == ((NMUser *)((void *)0))) || (conference == ((NMConference *)((void *)0)))) 
    return ;
  gc = purple_account_get_connection((user -> client_data));
  if (ret_code == 0L) {
    conf_name = _get_conference_name((++user -> conference_count));
    chat = serv_got_joined_chat(gc,(user -> conference_count),conf_name);
    if (chat != 0) {
      nm_conference_set_data(conference,((gpointer )chat));
      count = nm_conference_get_participant_count(conference);
      for (i = 0; i < count; i++) {
        ur = nm_conference_get_participant(conference,i);
        if (ur != 0) {
          name = nm_user_record_get_display_id(ur);
          purple_conv_chat_add_user(purple_conversation_get_chat_data(chat),name,0,PURPLE_CBFLAGS_NONE,(!0));
        }
      }
    }
  }
}
/* Show info returned by getdetails */

static void _get_details_resp_show_info(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  PurpleConnection *gc;
  NMUserRecord *user_record;
  char *name;
  char *err;
  if (user == ((NMUser *)((void *)0))) 
    return ;
  name = user_data;
  if (ret_code == 0L) {
    user_record = ((NMUserRecord *)resp_data);
    if (user_record != 0) {
      _show_info(purple_account_get_connection((user -> client_data)),user_record,g_strdup(name));
    }
  }
  else {
    gc = purple_account_get_connection((user -> client_data));
    err = g_strdup_printf(((const char *)(dgettext("pidgin","Could not get details for user %s (%s)."))),name,nm_error_to_string(ret_code));
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,err,0,0,0);
    g_free(err);
  }
  if (name != 0) 
    g_free(name);
}
/* Handle get details response add to privacy list */

static void _get_details_resp_add_privacy_item(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  PurpleConnection *gc;
  NMUserRecord *user_record = resp_data;
  char *err;
  gboolean allowed = (gint )((glong )user_data);
  const char *display_id;
  if (user == ((NMUser *)((void *)0))) 
    return ;
  gc = purple_account_get_connection((user -> client_data));
  display_id = nm_user_record_get_display_id(user_record);
  if (ret_code == 0L) {
    if (allowed != 0) {
      if (!(g_slist_find_custom(( *(gc -> account)).permit,display_id,((GCompareFunc )purple_utf8_strcasecmp)) != 0)) {
        purple_privacy_permit_add((gc -> account),display_id,(!0));
      }
    }
    else {
      if (!(g_slist_find_custom(( *(gc -> account)).permit,display_id,((GCompareFunc )purple_utf8_strcasecmp)) != 0)) {
        purple_privacy_deny_add((gc -> account),display_id,(!0));
      }
    }
  }
  else {
    err = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to add user to privacy list (%s)."))),nm_error_to_string(ret_code));
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,err,0,0,0);
    g_free(err);
  }
}
/* Handle response to create privacy item request */

static void _create_privacy_item_deny_resp_cb(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  PurpleConnection *gc;
  NMUserRecord *user_record;
  char *who = user_data;
  char *err;
  NMERR_T rc = 0L;
  const char *display_id = (const char *)((void *)0);
  if (user == ((NMUser *)((void *)0))) 
    return ;
  gc = purple_account_get_connection((user -> client_data));
  if (ret_code == 0L) {
    user_record = nm_find_user_record(user,who);
    if (user_record != 0) 
      display_id = nm_user_record_get_display_id(user_record);
    if (display_id != 0) {
      if (!(g_slist_find_custom(( *(gc -> account)).deny,display_id,((GCompareFunc )purple_utf8_strcasecmp)) != 0)) {
        purple_privacy_deny_add((gc -> account),display_id,(!0));
      }
    }
    else {
      rc = nm_send_get_details(user,who,_get_details_resp_add_privacy_item,0);
      _check_for_disconnect(user,rc);
    }
  }
  else {
    err = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to add %s to deny list (%s)."))),who,nm_error_to_string(ret_code));
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,err,0,0,0);
    g_free(err);
  }
  if (who != 0) 
    g_free(who);
}
/* Handle response to create privacy item request */

static void _create_privacy_item_permit_resp_cb(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  PurpleConnection *gc;
  NMUserRecord *user_record;
  char *who = user_data;
  char *err;
  NMERR_T rc = 0L;
  const char *display_id = (const char *)((void *)0);
  if (user == ((NMUser *)((void *)0))) 
    return ;
  gc = purple_account_get_connection((user -> client_data));
  if (ret_code == 0L) {
    user_record = nm_find_user_record(user,who);
    if (user_record != 0) 
      display_id = nm_user_record_get_display_id(user_record);
    if (display_id != 0) {
      if (!(g_slist_find_custom(( *(gc -> account)).permit,display_id,((GCompareFunc )purple_utf8_strcasecmp)) != 0)) {
        purple_privacy_permit_add((gc -> account),display_id,(!0));
      }
    }
    else {
      rc = nm_send_get_details(user,who,_get_details_resp_add_privacy_item,((gpointer )((gpointer )(!0))));
      _check_for_disconnect(user,rc);
    }
  }
  else {
    err = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to add %s to permit list (%s)."))),who,nm_error_to_string(ret_code));
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,err,0,0,0);
    g_free(err);
  }
  if (who != 0) 
    g_free(who);
}

static void _get_details_send_privacy_create(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  NMERR_T rc = 0L;
  PurpleConnection *gc;
  NMUserRecord *user_record = resp_data;
  char *err;
  gboolean allowed = (gint )((glong )user_data);
  const char *dn;
  const char *display_id;
  if (user == ((NMUser *)((void *)0))) 
    return ;
  gc = purple_account_get_connection((user -> client_data));
  dn = nm_user_record_get_dn(user_record);
  display_id = nm_user_record_get_display_id(user_record);
  if (ret_code == 0L) {
    if (allowed != 0) {
      rc = nm_send_create_privacy_item(user,dn,(!0),_create_privacy_item_permit_resp_cb,(g_strdup(display_id)));
      _check_for_disconnect(user,rc);
    }
    else {
      rc = nm_send_create_privacy_item(user,dn,0,_create_privacy_item_deny_resp_cb,(g_strdup(display_id)));
      _check_for_disconnect(user,rc);
    }
  }
  else {
    err = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to add user to privacy list (%s)."))),nm_error_to_string(ret_code));
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,err,0,0,0);
    g_free(err);
  }
}

static void _remove_privacy_item_resp_cb(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  PurpleConnection *gc;
  char *who = user_data;
  char *err;
  if (user == ((NMUser *)((void *)0))) 
    return ;
  if (ret_code != 0L) {
    gc = purple_account_get_connection((user -> client_data));
    err = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to remove %s from privacy list (%s)."))),who,nm_error_to_string(ret_code));
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,err,0,0,0);
    g_free(err);
  }
  if (who != 0) 
    g_free(who);
}

static void _set_privacy_default_resp_cb(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  PurpleConnection *gc;
  char *err;
  if (user == ((NMUser *)((void *)0))) 
    return ;
  if (ret_code != 0L) {
    gc = purple_account_get_connection((user -> client_data));
    err = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to change server side privacy settings (%s)."))),nm_error_to_string(ret_code));
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,err,0,0,0);
    g_free(err);
  }
}
/* Handle get details response add to privacy list */

static void _get_details_resp_send_invite(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  NMERR_T rc = 0L;
  PurpleConnection *gc;
  NMUserRecord *user_record = resp_data;
  char *err;
  GSList *cnode;
  NMConference *conference;
  gpointer chat;
  long id = (long )user_data;
  if (user == ((NMUser *)((void *)0))) 
    return ;
  gc = purple_account_get_connection((user -> client_data));
  if (ret_code == 0L) {{
      for (cnode = (user -> conferences); cnode != ((GSList *)((void *)0)); cnode = (cnode -> next)) {
        conference = (cnode -> data);
        if ((conference != 0) && ((chat = nm_conference_get_data(conference)) != 0)) {
          if ((purple_conv_chat_get_id((purple_conversation_get_chat_data(chat)))) == id) {
            rc = nm_send_conference_invite(user,conference,user_record,0,_sendinvite_resp_cb,0);
            _check_for_disconnect(user,rc);
            break; 
          }
        }
      }
    }
  }
  else {
    err = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to invite user (%s)."))),nm_error_to_string(ret_code));
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,err,0,0,0);
    g_free(err);
  }
}

static void _createconf_resp_send_invite(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  NMERR_T rc = 0L;
  NMConference *conference = resp_data;
  NMUserRecord *user_record = user_data;
  PurpleConnection *gc;
  char *err;
  if (user == ((NMUser *)((void *)0))) 
    return ;
  if (ret_code == 0L) {
    rc = nm_send_conference_invite(user,conference,user_record,0,_sendinvite_resp_cb,0);
    _check_for_disconnect(user,rc);
  }
  else {
    err = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to create conference (%s)."))),nm_error_to_string(ret_code));
    gc = purple_account_get_connection((user -> client_data));
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,err,0,0,0);
    g_free(err);
  }
}
/*******************************************************************************
 * Helper functions
 ******************************************************************************/

static char *_user_agent_string()
{
#if !defined(_WIN32)
  const char *sysname = "";
  const char *release = "";
  struct utsname u;
  if (uname(&u) == 0) {
    sysname = u.sysname;
    release = u.release;
  }
  else {
    sysname = "Linux";
    release = "Unknown";
  }
  return g_strdup_printf("Purple/%s (%s; %s)","2.10.9",sysname,release);
#else
#endif
}

static gboolean _is_disconnect_error(NMERR_T err)
{
  return ((err == 0x2000L + 2) || (err == 0x2000L + 3)) || (err == 0x2000L + 4);
}

static gboolean _check_for_disconnect(NMUser *user,NMERR_T err)
{
  PurpleConnection *gc = purple_account_get_connection((user -> client_data));
  if (_is_disconnect_error(err) != 0) {
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Error communicating with server. Closing connection."))));
    return (!0);
  }
  return 0;
}
/* Check to see if the conference is instantiated, if so send the message.
 * If not send the create conference -- the response handler for the createconf
 * will call this function again.
 */

static void _send_message(NMUser *user,NMMessage *message)
{
  NMConference *conf;
  NMERR_T rc = 0L;
  conf = nm_message_get_conference(message);
  if (conf != 0) {
/* We have a conference make sure that the
		   server knows about it already. */
    if (nm_conference_is_instantiated(conf) != 0) {
/* We have everything that we need...finally! */
      rc = nm_send_message(user,message,_send_message_resp_cb);
      _check_for_disconnect(user,rc);
      nm_release_message(message);
    }
    else {
      rc = nm_send_create_conference(user,conf,_createconf_resp_send_msg,message);
      _check_for_disconnect(user,rc);
    }
  }
}
/*
 * Update the status of the given buddy in the Purple buddy list
 */

static void _update_buddy_status(NMUser *user,PurpleBuddy *buddy,int novellstatus,int gmt)
{
  PurpleAccount *account;
  const char *status_id;
  const char *text = (const char *)((void *)0);
  const char *dn;
  const char *name;
  int idle = 0;
  account = purple_buddy_get_account(buddy);
  name = purple_buddy_get_name(buddy);
  switch(novellstatus){
    case 2:
{
      status_id = "available";
      break; 
    }
    case 4:
{
      status_id = "away";
      break; 
    }
    case 3:
{
      status_id = "busy";
      break; 
    }
    case 1:
{
      status_id = "offline";
      break; 
    }
    case 5:
{
      status_id = "away";
      idle = gmt;
      break; 
    }
    default:
{
      status_id = "offline";
      break; 
    }
  }
/* Get status text for the user */
  dn = nm_lookup_dn(user,name);
  if (dn != 0) {
    NMUserRecord *user_record = nm_find_user_record(user,dn);
    if (user_record != 0) {
      text = nm_user_record_get_status_text(user_record);
    }
  }
  purple_prpl_got_user_status(account,name,status_id,"message",text,((void *)((void *)0)));
  purple_prpl_got_user_idle(account,name,(novellstatus == 5),idle);
}
/* Iterate through the cached Purple buddy list and remove buddies
 * that are not in the server side list.
 */

static void _remove_purple_buddies(NMUser *user)
{
  PurpleBlistNode *gnode;
  PurpleBlistNode *cnode;
  PurpleBlistNode *bnode;
  PurpleGroup *group;
  PurpleBuddy *buddy;
  GSList *rem_list = (GSList *)((void *)0);
  GSList *l;
  NMFolder *folder = (NMFolder *)((void *)0);
  const char *gname = (const char *)((void *)0);
  for (gnode = purple_blist_get_root(); gnode != 0; gnode = purple_blist_node_get_sibling_next(gnode)) {
    if (!((purple_blist_node_get_type(gnode)) == PURPLE_BLIST_GROUP_NODE)) 
      continue; 
    group = ((PurpleGroup *)gnode);
    gname = purple_group_get_name(group);
    for (cnode = purple_blist_node_get_first_child(gnode); cnode != 0; cnode = purple_blist_node_get_sibling_next(cnode)) {
      if (!((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CONTACT_NODE)) 
        continue; 
      for (bnode = purple_blist_node_get_first_child(cnode); bnode != 0; bnode = purple_blist_node_get_sibling_next(bnode)) {
        if (!((purple_blist_node_get_type(bnode)) == PURPLE_BLIST_BUDDY_NODE)) 
          continue; 
        buddy = ((PurpleBuddy *)bnode);
        if ((purple_buddy_get_account(buddy)) == (user -> client_data)) {
          if (strcmp(gname,"GroupWise Messenger") == 0) 
            gname = "";
          folder = nm_find_folder(user,gname);
          if ((folder == ((NMFolder *)((void *)0))) || !(nm_folder_find_contact_by_display_id(folder,purple_buddy_get_name(buddy)) != 0)) {
            rem_list = g_slist_append(rem_list,buddy);
          }
        }
      }
    }
  }
  if (rem_list != 0) {
    for (l = rem_list; l != 0; l = (l -> next)) {
      purple_blist_remove_buddy((l -> data));
    }
    g_slist_free(rem_list);
  }
}
/* Add all of the contacts in the given folder to the Purple buddy list */

static void _add_contacts_to_purple_blist(NMUser *user,NMFolder *folder)
{
  NMUserRecord *user_record = (NMUserRecord *)((void *)0);
  NMContact *contact = (NMContact *)((void *)0);
  PurpleBuddy *buddy = (PurpleBuddy *)((void *)0);
  PurpleGroup *group;
  NMERR_T cnt = 0;
  NMERR_T i;
  const char *name = (const char *)((void *)0);
  const char *fname = (const char *)((void *)0);
  int status = 0;
/* If this is the root folder give it a name. Purple does not have the concept of
	 * a root folder.
	 */
  fname = nm_folder_get_name(folder);
  if ((fname == ((const char *)((void *)0))) || (( *fname) == 0)) {
    fname = "GroupWise Messenger";
  }
/* Does the Purple group exist already? */
  group = purple_find_group(fname);
  if (group == ((PurpleGroup *)((void *)0))) {
    group = purple_group_new(fname);
    purple_blist_add_group(group,0);
  }
/* Get each contact for this folder */
  cnt = (nm_folder_get_contact_count(folder));
{
    for (i = 0; i < cnt; i++) {
      contact = nm_folder_get_contact(folder,i);
      if (contact != 0) {
        name = nm_contact_get_display_id(contact);
        if (name != 0) {
          buddy = purple_find_buddy_in_group((user -> client_data),name,group);
          if (buddy == ((PurpleBuddy *)((void *)0))) {
/* Add it to the purple buddy list */
            buddy = purple_buddy_new((user -> client_data),name,nm_contact_get_display_name(contact));
            purple_blist_add_buddy(buddy,0,group,0);
          }
/* Set the initial status for the buddy */
          user_record = nm_contact_get_user_record(contact);
          if (user_record != 0) {
            status = nm_user_record_get_status(user_record);
          }
          _update_buddy_status(user,buddy,status,(time(0)));
/* Save the new buddy as part of the contact object */
          nm_contact_set_data(contact,((gpointer )buddy));
        }
      }
      else {
/* NULL contact. This should not happen, but
			 * let's break out of the loop.
			 */
        break; 
      }
    }
  }
}
/* Add all of the server side contacts to the Purple buddy list. */

static void _add_purple_buddies(NMUser *user)
{
  int cnt = 0;
  int i;
  NMFolder *root_folder = (NMFolder *)((void *)0);
  NMFolder *folder = (NMFolder *)((void *)0);
  root_folder = nm_get_root_folder(user);
  if (root_folder != 0) {
/* Add sub-folders and contacts to sub-folders...
		 * iterate throught the sub-folders in reverse order
		 * because Purple adds the folders to the front -- so we
		 * want to add the first folder last
		 */
    cnt = nm_folder_get_subfolder_count(root_folder);
    for (i = (cnt - 1); i >= 0; i--) {
      folder = nm_folder_get_subfolder(root_folder,i);
      if (folder != 0) {
        _add_contacts_to_purple_blist(user,folder);
      }
    }
/* Add contacts for the root folder */
    _add_contacts_to_purple_blist(user,root_folder);
  }
}

static void _sync_contact_list(NMUser *user)
{
/* Remove all buddies from the local list that are
	 * not in the server side list and add all buddies
	 * from the server side list that are not in
	 * the local list
	 */
  _remove_purple_buddies(user);
  _add_purple_buddies(user);
  user -> clist_synched = (!0);
}

static void _sync_privacy_lists(NMUser *user)
{
  GSList *node = (GSList *)((void *)0);
  GSList *rem_list = (GSList *)((void *)0);
  PurpleConnection *gc;
  const char *name;
  const char *dn;
  NMUserRecord *user_record;
  if (user == ((NMUser *)((void *)0))) 
    return ;
  gc = purple_account_get_connection((user -> client_data));
  if (gc == ((PurpleConnection *)((void *)0))) 
    return ;
/* Set the Purple privacy setting */
  if ((user -> default_deny) != 0) {
    if ((user -> allow_list) == ((GSList *)((void *)0))) {
      ( *(gc -> account)).perm_deny = PURPLE_PRIVACY_DENY_ALL;
    }
    else {
      ( *(gc -> account)).perm_deny = PURPLE_PRIVACY_ALLOW_USERS;
    }
  }
  else {
    if ((user -> deny_list) == ((GSList *)((void *)0))) {
      ( *(gc -> account)).perm_deny = PURPLE_PRIVACY_ALLOW_ALL;
    }
    else {
      ( *(gc -> account)).perm_deny = PURPLE_PRIVACY_DENY_USERS;
    }
  }
/* Add stuff */
  for (node = (user -> allow_list); node != 0; node = (node -> next)) {
    user_record = nm_find_user_record(user,((char *)(node -> data)));
    if (user_record != 0) 
      name = nm_user_record_get_display_id(user_record);
    else 
      name = ((char *)(node -> data));
    if (!(g_slist_find_custom(( *(gc -> account)).permit,name,((GCompareFunc )purple_utf8_strcasecmp)) != 0)) {
      purple_privacy_permit_add((gc -> account),name,(!0));
    }
  }
  for (node = (user -> deny_list); node != 0; node = (node -> next)) {
    user_record = nm_find_user_record(user,((char *)(node -> data)));
    if (user_record != 0) 
      name = nm_user_record_get_display_id(user_record);
    else 
      name = ((char *)(node -> data));
    if (!(g_slist_find_custom(( *(gc -> account)).deny,name,((GCompareFunc )purple_utf8_strcasecmp)) != 0)) {
      purple_privacy_deny_add((gc -> account),name,(!0));
    }
  }
/*  Remove stuff */
  for (node = ( *(gc -> account)).permit; node != 0; node = (node -> next)) {
    dn = nm_lookup_dn(user,((char *)(node -> data)));
    if ((dn != ((const char *)((void *)0))) && !(g_slist_find_custom((user -> allow_list),dn,((GCompareFunc )purple_utf8_strcasecmp)) != 0)) {
      rem_list = g_slist_append(rem_list,(node -> data));
    }
  }
  if (rem_list != 0) {
    for (node = rem_list; node != 0; node = (node -> next)) {
      purple_privacy_permit_remove((gc -> account),((char *)(node -> data)),(!0));
    }
    g_slist_free(rem_list);
    rem_list = ((GSList *)((void *)0));
  }
  for (node = ( *(gc -> account)).deny; node != 0; node = (node -> next)) {
    dn = nm_lookup_dn(user,((char *)(node -> data)));
    if ((dn != ((const char *)((void *)0))) && !(g_slist_find_custom((user -> deny_list),dn,((GCompareFunc )purple_utf8_strcasecmp)) != 0)) {
      rem_list = g_slist_append(rem_list,(node -> data));
    }
  }
  if (rem_list != 0) {
    for (node = rem_list; node != 0; node = (node -> next)) {
      purple_privacy_deny_remove((gc -> account),((char *)(node -> data)),(!0));
    }
    g_slist_free(rem_list);
  }
}
/* Map known property tags to user-friendly strings */

static const char *_map_property_tag(const char *tag)
{
  if (tag == ((const char *)((void *)0))) 
    return 0;
  if (strcmp(tag,"telephoneNumber") == 0) 
    return (const char *)(dgettext("pidgin","Telephone Number"));
  else if (strcmp(tag,"L") == 0) 
    return (const char *)(dgettext("pidgin","Location"));
  else if (strcmp(tag,"OU") == 0) 
    return (const char *)(dgettext("pidgin","Department"));
  else if (strcmp(tag,"personalTitle") == 0) 
    return (const char *)(dgettext("pidgin","Personal Title"));
  else if (strcmp(tag,"Title") == 0) 
    return (const char *)(dgettext("pidgin","Job Title"));
  else if (strcmp(tag,"mailstop") == 0) 
    return (const char *)(dgettext("pidgin","Mailstop"));
  else if (strcmp(tag,"Internet EMail Address") == 0) 
    return (const char *)(dgettext("pidgin","Email Address"));
  else 
    return tag;
}
/* Display a dialog box showing the properties for the given user record */

static void _show_info(PurpleConnection *gc,NMUserRecord *user_record,char *name)
{
  PurpleNotifyUserInfo *user_info = purple_notify_user_info_new();
  int count;
  int i;
  NMProperty *property;
  const char *tag;
  const char *value;
  tag = ((const char *)(dgettext("pidgin","User ID")));
  value = nm_user_record_get_userid(user_record);
  if (value != 0) {
    purple_notify_user_info_add_pair(user_info,tag,value);
  }
/*	tag = _("DN");
	value = nm_user_record_get_dn(user_record);
	if (value) {
		purple_notify_user_info_add_pair(user_info, tag, value);
	}
*/
  tag = ((const char *)(dgettext("pidgin","Full name")));
  value = nm_user_record_get_full_name(user_record);
  if (value != 0) {
    purple_notify_user_info_add_pair(user_info,tag,value);
  }
  count = nm_user_record_get_property_count(user_record);
  for (i = 0; i < count; i++) {
    property = nm_user_record_get_property(user_record,i);
    if (property != 0) {
      tag = _map_property_tag(nm_property_get_tag(property));
      value = nm_property_get_value(property);
      if ((tag != 0) && (value != 0)) {
        purple_notify_user_info_add_pair(user_info,tag,value);
      }
      nm_release_property(property);
    }
  }
  purple_notify_userinfo(gc,name,user_info,0,0);
  purple_notify_user_info_destroy(user_info);
  g_free(name);
}
/* Send a join conference, the first item in the parms list is the
 * NMUser object and the second item is the conference to join.
 * This callback is passed to purple_request_action when we ask the
 * user if they want to join the conference.
 */

static void _join_conference_cb(GSList *parms)
{
  NMUser *user;
  NMConference *conference;
  NMERR_T rc = 0L;
  if ((parms == ((GSList *)((void *)0))) || (g_slist_length(parms) != 2)) 
    return ;
  user = (g_slist_nth_data(parms,0));
  conference = (g_slist_nth_data(parms,1));
  if ((user != 0) && (conference != 0)) {
    rc = nm_send_join_conference(user,conference,_join_conf_resp_cb,conference);
    _check_for_disconnect(user,rc);
  }
  g_slist_free(parms);
}
/* Send a reject conference, the first item in the parms list is the
 * NMUser object and the second item is the conference to reject.
 * This callback is passed to purple_request_action when we ask the
 * user if they want to joing the conference.
 */

static void _reject_conference_cb(GSList *parms)
{
  NMUser *user;
  NMConference *conference;
  NMERR_T rc = 0L;
  if ((parms == ((GSList *)((void *)0))) || (g_slist_length(parms) != 2)) 
    return ;
  user = (g_slist_nth_data(parms,0));
  conference = (g_slist_nth_data(parms,1));
  if ((user != 0) && (conference != 0)) {
    rc = nm_send_reject_conference(user,conference,0,0);
    _check_for_disconnect(user,rc);
  }
  g_slist_free(parms);
}

static void _initiate_conference_cb(PurpleBlistNode *node,gpointer ignored)
{
  PurpleBuddy *buddy;
  PurpleConnection *gc;
  NMUser *user;
  const char *conf_name;
  PurpleConversation *chat = (PurpleConversation *)((void *)0);
  NMUserRecord *user_record;
  NMConference *conference;
  do {
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_BLIST_NODE_IS_BUDDY(node)");
      return ;
    };
  }while (0);
  buddy = ((PurpleBuddy *)node);
  gc = purple_account_get_connection((purple_buddy_get_account(buddy)));
  user = (gc -> proto_data);
  if (user == ((NMUser *)((void *)0))) 
    return ;
/* We should already have a userrecord for the buddy */
  user_record = nm_find_user_record(user,purple_buddy_get_name(buddy));
  if (user_record == ((NMUserRecord *)((void *)0))) 
    return ;
  conf_name = _get_conference_name((++user -> conference_count));
  chat = serv_got_joined_chat(gc,(user -> conference_count),conf_name);
  if (chat != 0) {
    conference = nm_create_conference(0);
    nm_conference_set_data(conference,((gpointer )chat));
    nm_send_create_conference(user,conference,_createconf_resp_send_invite,user_record);
    nm_release_conference(conference);
  }
}

const char *_get_conference_name(int id)
{
  static char *name = (char *)((void *)0);
  if (name != 0) 
    g_free(name);
  name = g_strdup_printf(((const char *)(dgettext("pidgin","GroupWise Conference %d"))),id);
  return name;
}

static void _show_privacy_locked_error(PurpleConnection *gc,NMUser *user)
{
  char *err;
  err = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to change server side privacy settings (%s)."))),nm_error_to_string((0xD100L + 28)));
  purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,err,0,0,0);
  g_free(err);
}
/*******************************************************************************
 * Connect and recv callbacks
 ******************************************************************************/

static void novell_ssl_connect_error(PurpleSslConnection *gsc,PurpleSslErrorType error,gpointer data)
{
  PurpleConnection *gc;
  NMUser *user;
  gc = data;
  user = (gc -> proto_data);
  ( *( *(user -> conn)).ssl_conn).data = ((gpointer )((void *)0));
  purple_connection_ssl_error(gc,error);
}

static void novell_ssl_recv_cb(gpointer data,PurpleSslConnection *gsc,PurpleInputCondition condition)
{
  PurpleConnection *gc = data;
  NMUser *user;
  NMERR_T rc;
  if (gc == ((PurpleConnection *)((void *)0))) 
    return ;
  user = (gc -> proto_data);
  if (user == ((NMUser *)((void *)0))) 
    return ;
  rc = nm_process_new_data(user);
  if (rc != 0L) {
    if (_is_disconnect_error(rc) != 0) {
      purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Error communicating with server. Closing connection."))));
    }
    else {
      purple_debug(PURPLE_DEBUG_INFO,"novell","Error processing event or response (%d).\n",rc);
    }
  }
}

static void novell_ssl_connected_cb(gpointer data,PurpleSslConnection *gsc,PurpleInputCondition cond)
{
  PurpleConnection *gc = data;
  NMUser *user;
  NMConn *conn;
  NMERR_T rc = 0;
  const char *pwd = (const char *)((void *)0);
  const char *my_addr = (const char *)((void *)0);
  char *ua = (char *)((void *)0);
  if ((gc == ((PurpleConnection *)((void *)0))) || (gsc == ((PurpleSslConnection *)((void *)0)))) 
    return ;
  user = (gc -> proto_data);
  if ((user == ((NMUser *)((void *)0))) || ((conn = (user -> conn)) == ((NMConn *)((void *)0)))) 
    return ;
  purple_connection_update_progress(gc,((const char *)(dgettext("pidgin","Authenticating..."))),2,4);
  my_addr = purple_network_get_my_ip((gsc -> fd));
  pwd = purple_connection_get_password(gc);
  ua = _user_agent_string();
  rc = nm_send_login(user,pwd,my_addr,ua,_login_resp_cb,0);
  if (rc == 0L) {
    conn -> connected = (!0);
    purple_ssl_input_add(gsc,novell_ssl_recv_cb,gc);
  }
  else {
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Unable to connect"))));
  }
  purple_connection_update_progress(gc,((const char *)(dgettext("pidgin","Waiting for response..."))),3,4);
  g_free(ua);
}
/*******************************************************************************
 * Event callback and event handlers
 ******************************************************************************/

static void _evt_receive_message(NMUser *user,NMEvent *event)
{
  NMUserRecord *user_record = (NMUserRecord *)((void *)0);
  NMContact *contact = (NMContact *)((void *)0);
  PurpleConversation *gconv;
  NMConference *conference;
  PurpleMessageFlags flags;
  char *text = (char *)((void *)0);
  text = g_markup_escape_text(nm_event_get_text(event),(-1));
  conference = nm_event_get_conference(event);
  if (conference != 0) {
    PurpleConversation *chat = (nm_conference_get_data(conference));
/* Is this a single person 'conversation' or a conference? */
    if ((chat == ((PurpleConversation *)((void *)0))) && (nm_conference_get_participant_count(conference) == 1)) {
      user_record = nm_find_user_record(user,nm_event_get_source(event));
      if (user_record != 0) {
        flags = 0;
        if (nm_event_get_type(event) == 121) 
          flags |= PURPLE_MESSAGE_AUTO_RESP;
        serv_got_im(purple_account_get_connection((user -> client_data)),nm_user_record_get_display_id(user_record),text,flags,nm_event_get_gmt(event));
        gconv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,nm_user_record_get_display_id(user_record),((PurpleAccount *)(user -> client_data)));
        if (gconv != 0) {
          contact = nm_find_contact(user,nm_event_get_source(event));
          if (contact != 0) {
            purple_conversation_set_title(gconv,nm_contact_get_display_name(contact));
          }
          else {
            const char *name = nm_user_record_get_full_name(user_record);
            if (name == ((const char *)((void *)0))) 
              name = nm_user_record_get_userid(user_record);
            purple_conversation_set_title(gconv,name);
          }
        }
      }
      else {
/* this should not happen, see the event code.
				 * the event code will get the contact details from
				 * the server if it does not have them before calling
				 * the event callback.
				 */
      }
    }
    else if (chat != 0) {
/* get the contact for send if we have one */
      NMContact *contact = nm_find_contact(user,nm_event_get_source(event));
/* get the user record for the sender */
      user_record = nm_find_user_record(user,nm_event_get_source(event));
      if (user_record != 0) {
        const char *name = nm_contact_get_display_name(contact);
        if (name == ((const char *)((void *)0))) {
          name = nm_user_record_get_full_name(user_record);
          if (name == ((const char *)((void *)0))) 
            name = nm_user_record_get_display_id(user_record);
        }
        serv_got_chat_in(purple_account_get_connection((user -> client_data)),purple_conv_chat_get_id((purple_conversation_get_chat_data(chat))),name,0,text,nm_event_get_gmt(event));
      }
    }
  }
  g_free(text);
}

static void _evt_conference_left(NMUser *user,NMEvent *event)
{
  PurpleConversation *chat;
  NMConference *conference;
  conference = nm_event_get_conference(event);
  if (conference != 0) {
    chat = (nm_conference_get_data(conference));
    if (chat != 0) {
      NMUserRecord *ur = nm_find_user_record(user,nm_event_get_source(event));
      if (ur != 0) 
        purple_conv_chat_remove_user(purple_conversation_get_chat_data(chat),nm_user_record_get_display_id(ur),0);
    }
  }
}

static void _evt_conference_invite_notify(NMUser *user,NMEvent *event)
{
  PurpleConversation *gconv;
  NMConference *conference;
  NMUserRecord *user_record = (NMUserRecord *)((void *)0);
  char *str = (char *)((void *)0);
  user_record = nm_find_user_record(user,nm_event_get_source(event));
  conference = nm_event_get_conference(event);
  if ((user_record != 0) && (conference != 0)) {
    gconv = (nm_conference_get_data(conference));
    str = g_strdup_printf(((const char *)(dgettext("pidgin","%s has been invited to this conversation."))),nm_user_record_get_display_id(user_record));
    purple_conversation_write(gconv,0,str,PURPLE_MESSAGE_SYSTEM,time(0));
    g_free(str);
  }
}

static void _evt_conference_invite(NMUser *user,NMEvent *event)
{
  NMUserRecord *ur;
  PurpleConnection *gc;
  GSList *parms = (GSList *)((void *)0);
  const char *title = (const char *)((void *)0);
  const char *secondary = (const char *)((void *)0);
  const char *name = (const char *)((void *)0);
  char *primary = (char *)((void *)0);
  time_t gmt;
  ur = nm_find_user_record(user,nm_event_get_source(event));
  if (ur != 0) 
    name = nm_user_record_get_full_name(ur);
  if (name == ((const char *)((void *)0))) 
    name = nm_event_get_source(event);
  gmt = nm_event_get_gmt(event);
  title = ((const char *)(dgettext("pidgin","Invitation to Conversation")));
  primary = g_strdup_printf(((const char *)(dgettext("pidgin","Invitation from: %s\n\nSent: %s"))),name,purple_date_format_full((localtime((&gmt)))));
  secondary = ((const char *)(dgettext("pidgin","Would you like to join the conversation\?")));
/* Set up parms list for the callbacks
	 * We need to send the NMUser object and
	 * the NMConference object to the callbacks
	 */
  parms = ((GSList *)((void *)0));
  parms = g_slist_append(parms,user);
  parms = g_slist_append(parms,(nm_event_get_conference(event)));
/* Prompt the user */
/* TODO: Would it be better to use serv_got_chat_invite() here? */
  gc = purple_account_get_connection((user -> client_data));
  purple_request_action(gc,title,primary,secondary,-1,purple_connection_get_account(gc),name,0,parms,2,((const char *)(dgettext("pidgin","Yes"))),((GCallback )_join_conference_cb),((const char *)(dgettext("pidgin","No"))),((GCallback )_reject_conference_cb));
  g_free(primary);
}

static void _evt_conference_joined(NMUser *user,NMEvent *event)
{
  PurpleConversation *chat = (PurpleConversation *)((void *)0);
  PurpleConnection *gc;
  NMConference *conference = (NMConference *)((void *)0);
  NMUserRecord *ur = (NMUserRecord *)((void *)0);
  const char *name;
  const char *conf_name;
  gc = purple_account_get_connection((user -> client_data));
  if (gc == ((PurpleConnection *)((void *)0))) 
    return ;
  conference = nm_event_get_conference(event);
  if (conference != 0) {
    chat = (nm_conference_get_data(conference));
    if ((nm_conference_get_participant_count(conference) == 2) && (chat == ((PurpleConversation *)((void *)0)))) {
      ur = nm_conference_get_participant(conference,0);
      if (ur != 0) {
        conf_name = _get_conference_name((++user -> conference_count));
        chat = serv_got_joined_chat(gc,(user -> conference_count),conf_name);
        if (chat != 0) {
          nm_conference_set_data(conference,((gpointer )chat));
          name = nm_user_record_get_display_id(ur);
          purple_conv_chat_add_user(purple_conversation_get_chat_data(chat),name,0,PURPLE_CBFLAGS_NONE,(!0));
        }
      }
    }
    if (chat != ((PurpleConversation *)((void *)0))) {
      ur = nm_find_user_record(user,nm_event_get_source(event));
      if (ur != 0) {
        name = nm_user_record_get_display_id(ur);
        if (!(purple_conv_chat_find_user(purple_conversation_get_chat_data(chat),name) != 0)) {
          purple_conv_chat_add_user(purple_conversation_get_chat_data(chat),name,0,PURPLE_CBFLAGS_NONE,(!0));
        }
      }
    }
  }
}

static void _evt_status_change(NMUser *user,NMEvent *event)
{
  PurpleBuddy *buddy = (PurpleBuddy *)((void *)0);
  GSList *buddies;
  GSList *bnode;
  NMUserRecord *user_record;
  const char *display_id;
  int status;
  user_record = nm_event_get_user_record(event);
  if (user_record != 0) {
/* Retrieve new status */
    status = nm_user_record_get_status(user_record);
/* Update status for buddy in all folders */
    display_id = nm_user_record_get_display_id(user_record);
    buddies = purple_find_buddies((user -> client_data),display_id);
    for (bnode = buddies; bnode != 0; bnode = (bnode -> next)) {
      buddy = ((PurpleBuddy *)(bnode -> data));
      if (buddy != 0) {
        _update_buddy_status(user,buddy,status,(nm_event_get_gmt(event)));
      }
    }
    g_slist_free(buddies);
  }
}

static void _evt_user_disconnect(NMUser *user,NMEvent *event)
{
  PurpleConnection *gc;
  PurpleAccount *account = (user -> client_data);
  gc = purple_account_get_connection(account);
  if (gc != 0) {
    if (!(purple_account_get_remember_password(account) != 0)) 
      purple_account_set_password(account,0);
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NAME_IN_USE,((const char *)(dgettext("pidgin","You have signed on from another location"))));
  }
}

static void _evt_user_typing(NMUser *user,NMEvent *event)
{
  PurpleConnection *gc;
  NMUserRecord *user_record = (NMUserRecord *)((void *)0);
  gc = purple_account_get_connection(((PurpleAccount *)(user -> client_data)));
  if (gc != 0) {
    user_record = nm_find_user_record(user,nm_event_get_source(event));
    if (user_record != 0) {
      serv_got_typing(gc,nm_user_record_get_display_id(user_record),30,PURPLE_TYPING);
    }
  }
}

static void _evt_user_not_typing(NMUser *user,NMEvent *event)
{
  PurpleConnection *gc;
  NMUserRecord *user_record;
  gc = purple_account_get_connection(((PurpleAccount *)(user -> client_data)));
  if (gc != 0) {
    user_record = nm_find_user_record(user,nm_event_get_source(event));
    if (user_record != 0) {
      serv_got_typing_stopped(gc,nm_user_record_get_display_id(user_record));
    }
  }
}

static void _evt_undeliverable_status(NMUser *user,NMEvent *event)
{
  NMUserRecord *ur;
  PurpleConversation *gconv;
  char *str;
  ur = nm_find_user_record(user,nm_event_get_source(event));
  if (ur != 0) {
/* XXX - Should this be PURPLE_CONV_TYPE_IM? */
    gconv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_ANY,nm_user_record_get_display_id(ur),(user -> client_data));
    if (gconv != 0) {
      const char *name = nm_user_record_get_full_name(ur);
      if (name == ((const char *)((void *)0))) {
        name = nm_user_record_get_display_id(ur);
      }
      str = g_strdup_printf(((const char *)(dgettext("pidgin","%s appears to be offline and did not receive the message that you just sent."))),name);
      purple_conversation_write(gconv,0,str,PURPLE_MESSAGE_SYSTEM,time(0));
      g_free(str);
    }
  }
}

static void _event_callback(NMUser *user,NMEvent *event)
{
  if ((user == ((NMUser *)((void *)0))) || (event == ((NMEvent *)((void *)0)))) 
    return ;
  switch(nm_event_get_type(event)){
    case 103:
{
      _evt_status_change(user,event);
      break; 
    }
    case 108:
{
    }
    case 121:
{
      _evt_receive_message(user,event);
      break; 
    }
    case 114:
{
      _evt_user_disconnect(user,event);
      break; 
    }
    case 112:
{
      _evt_user_typing(user,event);
      break; 
    }
    case 113:
{
      _evt_user_not_typing(user,event);
      break; 
    }
    case 115:
{
/* Nothing to do? */
      break; 
    }
    case 101:
{
      break; 
    }
    case 102:
{
      _evt_undeliverable_status(user,event);
      break; 
    }
    case 118:
{
/* Someone else has been invited to join a
			 * conference that we are currently a part of
			 */
      _evt_conference_invite_notify(user,event);
      break; 
    }
    case 117:
{
/* We have been invited to join a conference */
      _evt_conference_invite(user,event);
      break; 
    }
    case 106:
{
/* Some one has joined a conference that we
			 * are a part of
			 */
      _evt_conference_joined(user,event);
      break; 
    }
    case 107:
{
/* Someone else has left a conference that we
			 * are currently a part of
			 */
      _evt_conference_left(user,event);
      break; 
    }
    default:
{
      purple_debug(PURPLE_DEBUG_INFO,"novell","_event_callback(): unhandled event, %d\n",nm_event_get_type(event));
      break; 
    }
  }
}
/*******************************************************************************
 * Prpl Ops
 ******************************************************************************/

static void novell_login(PurpleAccount *account)
{
  PurpleConnection *gc;
  NMUser *user = (NMUser *)((void *)0);
  const char *server;
  const char *name;
  int port;
  if (account == ((PurpleAccount *)((void *)0))) 
    return ;
  gc = purple_account_get_connection(account);
  if (gc == ((PurpleConnection *)((void *)0))) 
    return ;
  server = purple_account_get_string(account,"server",0);
  if ((server == ((const char *)((void *)0))) || (( *server) == 0)) {
/* TODO: Would be nice to prompt if not set!
		 *  purple_request_fields(gc, _("Server Address"),...);
		 */
/* ...but for now just error out with a nice message. */
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_INVALID_SETTINGS,((const char *)(dgettext("pidgin","Unable to connect to server. Please enter the address of the server to which you wish to connect."))));
    return ;
  }
  port = purple_account_get_int(account,"port",8300);
  name = purple_account_get_username(account);
  user = nm_initialize_user(name,server,port,account,_event_callback);
  if ((user != 0) && ((user -> conn) != 0)) {
/* save user */
    gc -> proto_data = user;
/* connect to the server */
    purple_connection_update_progress(gc,((const char *)(dgettext("pidgin","Connecting"))),1,4);
    ( *(user -> conn)).use_ssl = (!0);
    ( *(user -> conn)).ssl_conn = ((NMSSLConn *)(g_malloc0_n(1,(sizeof(NMSSLConn )))));
    ( *( *(user -> conn)).ssl_conn).read = ((nm_ssl_read_cb )purple_ssl_read);
    ( *( *(user -> conn)).ssl_conn).write = ((nm_ssl_write_cb )purple_ssl_write);
    ( *( *(user -> conn)).ssl_conn).data = (purple_ssl_connect((user -> client_data),( *(user -> conn)).addr,( *(user -> conn)).port,novell_ssl_connected_cb,novell_ssl_connect_error,gc));
    if (( *( *(user -> conn)).ssl_conn).data == ((void *)((void *)0))) {
      purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NO_SSL_SUPPORT,((const char *)(dgettext("pidgin","SSL support unavailable"))));
    }
  }
}

static void novell_close(PurpleConnection *gc)
{
  NMUser *user;
  NMConn *conn;
  if (gc == ((PurpleConnection *)((void *)0))) 
    return ;
  user = (gc -> proto_data);
  if (user != 0) {
    conn = (user -> conn);
    if ((conn != 0) && ((conn -> ssl_conn) != 0)) {
      purple_ssl_close(( *( *(user -> conn)).ssl_conn).data);
    }
    nm_deinitialize_user(user);
  }
  gc -> proto_data = ((void *)((void *)0));
}

static int novell_send_im(PurpleConnection *gc,const char *name,const char *message_body,PurpleMessageFlags flags)
{
  NMUserRecord *user_record = (NMUserRecord *)((void *)0);
  NMConference *conf = (NMConference *)((void *)0);
  NMMessage *message;
  NMUser *user;
  const char *dn = (const char *)((void *)0);
  char *plain;
  gboolean done = (!0);
  gboolean created_conf = 0;
  NMERR_T rc = 0L;
  if ((((gc == ((PurpleConnection *)((void *)0))) || (name == ((const char *)((void *)0)))) || (message_body == ((const char *)((void *)0)))) || (( *message_body) == 0)) 
    return 0;
  user = (gc -> proto_data);
  if (user == ((NMUser *)((void *)0))) 
    return 0;
/* Create a new message */
  plain = purple_unescape_html(message_body);
  message = nm_create_message(plain);
  g_free(plain);
/* Need to get the DN for the buddy so we can look up the convo */
  dn = nm_lookup_dn(user,name);
/* Do we already know about the sender? */
  user_record = nm_find_user_record(user,dn);
  if (user_record != 0) {
/* Do we already have an instantiated conference? */
    conf = nm_find_conversation(user,dn);
    if (conf == ((NMConference *)((void *)0))) {
/* If not, create a blank conference */
      conf = nm_create_conference(0);
      created_conf = (!0);
      nm_conference_add_participant(conf,user_record);
    }
    nm_message_set_conference(message,conf);
/* Make sure conference is instantiated */
    if (!(nm_conference_is_instantiated(conf) != 0)) {
/* It is not, so send the createconf. We will
			 * have to finish sending the message when we
			 * get the response with the new conference guid.
			 */
      rc = nm_send_create_conference(user,conf,_createconf_resp_send_msg,message);
      _check_for_disconnect(user,rc);
      done = 0;
    }
  }
  else {
/* If we don't have details for the user, then we don't have
		 * a conference yet. So create one and send the getdetails
		 * to the server. We will have to finish sending the message
		 * when we get the response from the server.
		 */
    conf = nm_create_conference(0);
    created_conf = (!0);
    nm_message_set_conference(message,conf);
    rc = nm_send_get_details(user,name,_get_details_resp_send_msg,message);
    _check_for_disconnect(user,rc);
    done = 0;
  }
  if (done != 0) {
/* Did we find everything we needed? */
    rc = nm_send_message(user,message,_send_message_resp_cb);
    _check_for_disconnect(user,rc);
    nm_release_message(message);
  }
  if ((created_conf != 0) && (conf != 0)) 
    nm_release_conference(conf);
  return 1;
}

static unsigned int novell_send_typing(PurpleConnection *gc,const char *name,PurpleTypingState state)
{
  NMConference *conf = (NMConference *)((void *)0);
  NMUser *user;
  const char *dn = (const char *)((void *)0);
  NMERR_T rc = 0L;
  if ((gc == ((PurpleConnection *)((void *)0))) || (name == ((const char *)((void *)0)))) 
    return 0;
  user = (gc -> proto_data);
  if (user == ((NMUser *)((void *)0))) 
    return 0;
/* Need to get the DN for the buddy so we can look up the convo */
  dn = nm_lookup_dn(user,name);
  if (dn != 0) {
/* Now find the conference in our list */
    conf = nm_find_conversation(user,dn);
    if (conf != 0) {
      rc = nm_send_typing(user,conf,((state == PURPLE_TYPING)?!0 : 0),0);
      _check_for_disconnect(user,rc);
    }
  }
  return 0;
}

static void novell_convo_closed(PurpleConnection *gc,const char *who)
{
  NMUser *user;
  NMConference *conf;
  const char *dn;
  NMERR_T rc = 0L;
  if ((gc == ((PurpleConnection *)((void *)0))) || (who == ((const char *)((void *)0)))) 
    return ;
  user = (gc -> proto_data);
  if ((user != 0) && ((dn = nm_lookup_dn(user,who)) != 0)) {
    conf = nm_find_conversation(user,dn);
    if (conf != 0) {
      rc = nm_send_leave_conference(user,conf,0,0);
      _check_for_disconnect(user,rc);
    }
  }
}

static void novell_chat_leave(PurpleConnection *gc,int id)
{
  NMConference *conference;
  NMUser *user;
  PurpleConversation *chat;
  GSList *cnode;
  NMERR_T rc = 0L;
  if (gc == ((PurpleConnection *)((void *)0))) 
    return ;
  user = (gc -> proto_data);
  if (user == ((NMUser *)((void *)0))) 
    return ;
{
    for (cnode = (user -> conferences); cnode != ((GSList *)((void *)0)); cnode = (cnode -> next)) {
      conference = (cnode -> data);
      if ((conference != 0) && ((chat = (nm_conference_get_data(conference))) != 0)) {
        if (purple_conv_chat_get_id((purple_conversation_get_chat_data(chat))) == id) {
          rc = nm_send_leave_conference(user,conference,0,0);
          _check_for_disconnect(user,rc);
          break; 
        }
      }
    }
  }
  serv_got_chat_left(gc,id);
}

static void novell_chat_invite(PurpleConnection *gc,int id,const char *message,const char *who)
{
  NMConference *conference;
  NMUser *user;
  PurpleConversation *chat;
  GSList *cnode;
  NMERR_T rc = 0L;
  NMUserRecord *user_record = (NMUserRecord *)((void *)0);
  if (gc == ((PurpleConnection *)((void *)0))) 
    return ;
  user = (gc -> proto_data);
  if (user == ((NMUser *)((void *)0))) 
    return ;
  user_record = nm_find_user_record(user,who);
  if (user_record == ((NMUserRecord *)((void *)0))) {
    rc = nm_send_get_details(user,who,_get_details_resp_send_invite,((gpointer )((glong )id)));
    _check_for_disconnect(user,rc);
    return ;
  }
{
    for (cnode = (user -> conferences); cnode != ((GSList *)((void *)0)); cnode = (cnode -> next)) {
      conference = (cnode -> data);
      if ((conference != 0) && ((chat = (nm_conference_get_data(conference))) != 0)) {
        if (purple_conv_chat_get_id((purple_conversation_get_chat_data(chat))) == id) {
          rc = nm_send_conference_invite(user,conference,user_record,message,_sendinvite_resp_cb,0);
          _check_for_disconnect(user,rc);
          break; 
        }
      }
    }
  }
}

static int novell_chat_send(PurpleConnection *gc,int id,const char *text,PurpleMessageFlags flags)
{
  NMConference *conference;
  PurpleConversation *chat;
  GSList *cnode;
  NMMessage *message;
  NMUser *user;
  NMERR_T rc = 0L;
  const char *name;
  char *str;
  char *plain;
  if ((gc == ((PurpleConnection *)((void *)0))) || (text == ((const char *)((void *)0)))) 
    return -1;
  user = (gc -> proto_data);
  if (user == ((NMUser *)((void *)0))) 
    return -1;
  plain = purple_unescape_html(text);
  message = nm_create_message(plain);
  g_free(plain);
  for (cnode = (user -> conferences); cnode != ((GSList *)((void *)0)); cnode = (cnode -> next)) {
    conference = (cnode -> data);
    if ((conference != 0) && ((chat = (nm_conference_get_data(conference))) != 0)) {
      if (purple_conv_chat_get_id((purple_conversation_get_chat_data(chat))) == id) {
        nm_message_set_conference(message,conference);
/* check to see if the conference is instatiated yet */
        if (!(nm_conference_is_instantiated(conference) != 0)) {
          nm_message_add_ref(message);
          nm_send_create_conference(user,conference,_createconf_resp_send_msg,message);
        }
        else {
          rc = nm_send_message(user,message,_send_message_resp_cb);
        }
        nm_release_message(message);
        if (!(_check_for_disconnect(user,rc) != 0)) {
/* Use the account alias if it is set */
          name = purple_account_get_alias((user -> client_data));
          if ((name == ((const char *)((void *)0))) || (( *name) == 0)) {
/* If there is no account alias, try full name */
            name = nm_user_record_get_full_name((user -> user_record));
            if ((name == ((const char *)((void *)0))) || (( *name) == 0)) {
/* Fall back to the username that we are signed in with */
              name = purple_account_get_username((user -> client_data));
            }
          }
          serv_got_chat_in(gc,id,name,flags,text,time(0));
          return 0;
        }
        else 
          return -1;
      }
    }
  }
/* The conference was not found, must be closed */
  chat = purple_find_chat(gc,id);
  if (chat != 0) {
    str = g_strdup(((const char *)(dgettext("pidgin","This conference has been closed. No more messages can be sent."))));
    purple_conversation_write(chat,0,str,PURPLE_MESSAGE_SYSTEM,time(0));
    g_free(str);
  }
  if (message != 0) 
    nm_release_message(message);
  return -1;
}

static void novell_add_buddy(PurpleConnection *gc,PurpleBuddy *buddy,PurpleGroup *group)
{
  NMFolder *folder = (NMFolder *)((void *)0);
  NMContact *contact;
  NMUser *user;
  NMERR_T rc = 0L;
  const char *alias;
  const char *gname;
  const char *bname;
  if (((gc == ((PurpleConnection *)((void *)0))) || (buddy == ((PurpleBuddy *)((void *)0)))) || (group == ((PurpleGroup *)((void *)0)))) 
    return ;
  user = ((NMUser *)(purple_connection_get_protocol_data(gc)));
  if (user == ((NMUser *)((void *)0))) 
    return ;
/* If we haven't synched the contact list yet, ignore
	 * the add_buddy calls. Server side list is the master.
	 */
  if (!((user -> clist_synched) != 0)) 
    return ;
/* Don't re-add a buddy that is already on our contact list */
  if (nm_find_user_record(user,purple_buddy_get_name(buddy)) != ((NMUserRecord *)((void *)0))) 
    return ;
  contact = nm_create_contact();
  nm_contact_set_dn(contact,purple_buddy_get_name(buddy));
/* Remove the PurpleBuddy (we will add it back after adding it
	 * to the server side list). Save the alias if there is one.
	 */
  alias = purple_buddy_get_alias(buddy);
  bname = purple_buddy_get_name(buddy);
  if ((alias != 0) && (strcmp(alias,bname) != 0)) 
    nm_contact_set_display_name(contact,alias);
  purple_blist_remove_buddy(buddy);
  buddy = ((PurpleBuddy *)((void *)0));
  gname = purple_group_get_name(group);
  if (strcmp(gname,"GroupWise Messenger") == 0) {
    gname = "";
  }
  folder = nm_find_folder(user,gname);
  if (folder != 0) {
/* We have everything that we need, so send the createcontact */
    rc = nm_send_create_contact(user,folder,contact,_create_contact_resp_cb,contact);
  }
  else {
/* Need to create the folder before we can add the contact */
    rc = nm_send_create_folder(user,gname,_create_folder_resp_add_contact,contact);
  }
  _check_for_disconnect(user,rc);
}

static void novell_remove_buddy(PurpleConnection *gc,PurpleBuddy *buddy,PurpleGroup *group)
{
  NMContact *contact;
  NMFolder *folder;
  NMUser *user;
  const char *dn;
  const char *gname;
  NMERR_T rc = 0L;
  if (((gc == ((PurpleConnection *)((void *)0))) || (buddy == ((PurpleBuddy *)((void *)0)))) || (group == ((PurpleGroup *)((void *)0)))) 
    return ;
  user = ((NMUser *)(gc -> proto_data));
  if ((user != 0) && ((dn = nm_lookup_dn(user,purple_buddy_get_name(buddy))) != 0)) {
    gname = purple_group_get_name(group);
    if (strcmp(gname,"GroupWise Messenger") == 0) {
      gname = "";
    }
    folder = nm_find_folder(user,gname);
    if (folder != 0) {
      contact = nm_folder_find_contact(folder,dn);
      if (contact != 0) {
/* Remove the buddy from the contact */
        nm_contact_set_data(contact,0);
/* Tell the server to remove the contact */
        rc = nm_send_remove_contact(user,folder,contact,_remove_contact_resp_cb,0);
        _check_for_disconnect(user,rc);
      }
    }
  }
}

static void novell_remove_group(PurpleConnection *gc,PurpleGroup *group)
{
  NMUser *user;
  NMERR_T rc = 0L;
  if ((gc == ((PurpleConnection *)((void *)0))) || (group == ((PurpleGroup *)((void *)0)))) 
    return ;
  user = ((NMUser *)(gc -> proto_data));
  if (user != 0) {
    NMFolder *folder = nm_find_folder(user,purple_group_get_name(group));
    if (folder != 0) {
      rc = nm_send_remove_folder(user,folder,_remove_folder_resp_cb,0);
      _check_for_disconnect(user,rc);
    }
  }
}

static void novell_alias_buddy(PurpleConnection *gc,const char *name,const char *alias)
{
  NMContact *contact;
  NMUser *user;
  GList *contacts = (GList *)((void *)0);
  GList *cnode = (GList *)((void *)0);
  const char *dn = (const char *)((void *)0);
  const char *fname = (const char *)((void *)0);
  NMERR_T rc = 0L;
  if (((gc == ((PurpleConnection *)((void *)0))) || (name == ((const char *)((void *)0)))) || (alias == ((const char *)((void *)0)))) 
    return ;
  user = ((NMUser *)(gc -> proto_data));
  if ((user != 0) && ((dn = nm_lookup_dn(user,name)) != 0)) {
/* Alias all of instances of the contact */
    contacts = nm_find_contacts(user,dn);
    for (cnode = contacts; cnode != ((GList *)((void *)0)); cnode = (cnode -> next)) {
      contact = ((NMContact *)(cnode -> data));
      if (contact != 0) {
        PurpleGroup *group = (PurpleGroup *)((void *)0);
        PurpleBuddy *buddy;
        NMFolder *folder;
/* Alias the Purple buddy? */
        folder = nm_find_folder_by_id(user,nm_contact_get_parent_id(contact));
        if (folder != 0) {
          fname = nm_folder_get_name(folder);
          if (( *fname) == 0) {
            fname = "GroupWise Messenger";
          }
          group = purple_find_group(fname);
        }
        if (group != 0) {
          const char *balias;
          buddy = purple_find_buddy_in_group((user -> client_data),name,group);
          balias = ((buddy != 0)?purple_buddy_get_local_buddy_alias(buddy) : ((const char *)((void *)0)));
          if ((balias != 0) && (strcmp(balias,alias) != 0)) 
            purple_blist_alias_buddy(buddy,alias);
        }
/* Tell the server to alias the contact */
        rc = nm_send_rename_contact(user,contact,alias,_rename_contact_resp_cb,0);
        _check_for_disconnect(user,rc);
      }
    }
    if (contacts != 0) 
      g_list_free(contacts);
  }
}

static void novell_group_buddy(PurpleConnection *gc,const char *name,const char *old_group_name,const char *new_group_name)
{
  NMFolder *old_folder;
  NMFolder *new_folder;
  NMContact *contact;
  NMUser *user;
  const char *dn;
  NMERR_T rc = 0L;
  if ((((gc == ((PurpleConnection *)((void *)0))) || (name == ((const char *)((void *)0)))) || (old_group_name == ((const char *)((void *)0)))) || (new_group_name == ((const char *)((void *)0)))) 
    return ;
  user = ((NMUser *)(gc -> proto_data));
  if ((user != 0) && ((dn = nm_lookup_dn(user,name)) != 0)) {
/* Find the old folder */
    if (strcmp(old_group_name,"GroupWise Messenger") == 0) {
      old_folder = nm_get_root_folder(user);
      if (nm_folder_find_contact(old_folder,dn) == ((NMContact *)((void *)0))) 
        old_folder = nm_find_folder(user,old_group_name);
    }
    else {
      old_folder = nm_find_folder(user,old_group_name);
    }
    if ((old_folder != 0) && ((contact = nm_folder_find_contact(old_folder,dn)) != 0)) {
/* Find the new folder */
      new_folder = nm_find_folder(user,new_group_name);
      if (new_folder == ((NMFolder *)((void *)0))) {
        if (strcmp(new_group_name,"GroupWise Messenger") == 0) 
          new_folder = nm_get_root_folder(user);
      }
      if (new_folder != 0) {
/* Tell the server to move the contact to the new folder */
        rc = nm_send_move_contact(user,contact,new_folder,_move_contact_resp_cb,0);
      }
      else {
        nm_contact_add_ref(contact);
/* Remove the old contact first */
        nm_send_remove_contact(user,old_folder,contact,_remove_contact_resp_cb,0);
/* New folder does not exist yet, so create it  */
        rc = nm_send_create_folder(user,new_group_name,_create_folder_resp_move_contact,contact);
      }
      _check_for_disconnect(user,rc);
    }
  }
}

static void novell_rename_group(PurpleConnection *gc,const char *old_name,PurpleGroup *group,GList *moved_buddies)
{
  NMERR_T rc = 0L;
  NMFolder *folder;
  NMUser *user;
  if ((((gc == ((PurpleConnection *)((void *)0))) || (old_name == ((const char *)((void *)0)))) || (group == ((PurpleGroup *)((void *)0)))) || (moved_buddies == ((GList *)((void *)0)))) {
    return ;
  }
  user = (gc -> proto_data);
  if (user != 0) {
    const char *gname = purple_group_get_name(group);
/* Does new folder exist already? */
    if (nm_find_folder(user,gname) != 0) {
/* purple_blist_rename_group() adds the buddies
			 * to the new group and removes the old group...
			 * so there is nothing more to do here.
			 */
      return ;
    }
    if (strcmp(old_name,"GroupWise Messenger") == 0) {
/* Can't rename the root folder ... need to revisit this */
      return ;
    }
    folder = nm_find_folder(user,old_name);
    if (folder != 0) {
      rc = nm_send_rename_folder(user,folder,gname,_rename_folder_resp_cb,0);
      _check_for_disconnect(user,rc);
    }
  }
}

static const char *novell_list_icon(PurpleAccount *account,PurpleBuddy *buddy)
{
  return "novell";
}

static void novell_tooltip_text(PurpleBuddy *buddy,PurpleNotifyUserInfo *user_info,gboolean full)
{
  NMUserRecord *user_record = (NMUserRecord *)((void *)0);
  PurpleConnection *gc;
  NMUser *user;
  int status = 0;
  const char *status_str = (const char *)((void *)0);
  const char *text = (const char *)((void *)0);
  if (buddy == ((PurpleBuddy *)((void *)0))) 
    return ;
  gc = purple_account_get_connection((purple_buddy_get_account(buddy)));
  if ((gc == ((PurpleConnection *)((void *)0))) || ((user = (gc -> proto_data)) == ((NMUser *)((void *)0)))) 
    return ;
  if (((buddy != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0)) {
    user_record = nm_find_user_record(user,purple_buddy_get_name(buddy));
    if (user_record != 0) {
      status = nm_user_record_get_status(user_record);
      text = nm_user_record_get_status_text(user_record);
/* No custom text, so default it ... */
      switch(status){
        case 2:
{
          status_str = ((const char *)(dgettext("pidgin","Available")));
          break; 
        }
        case 4:
{
          status_str = ((const char *)(dgettext("pidgin","Away")));
          break; 
        }
        case 3:
{
          status_str = ((const char *)(dgettext("pidgin","Busy")));
          break; 
        }
        case 5:
{
          status_str = ((const char *)(dgettext("pidgin","Idle")));
          break; 
        }
        case 1:
{
          status_str = ((const char *)(dgettext("pidgin","Offline")));
          break; 
        }
        default:
{
          status_str = ((const char *)(dgettext("pidgin","Unknown")));
          break; 
        }
      }
      purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Status"))),status_str);
      if (text != 0) 
        purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Message"))),text);
    }
  }
}

static void novell_set_idle(PurpleConnection *gc,int time)
{
  NMUser *user;
  NMERR_T rc = 0L;
  const char *id = (const char *)((void *)0);
  PurpleStatus *status = (PurpleStatus *)((void *)0);
  if (gc == ((PurpleConnection *)((void *)0))) 
    return ;
  user = (gc -> proto_data);
  if (user == ((NMUser *)((void *)0))) 
    return ;
  status = purple_account_get_active_status((purple_connection_get_account(gc)));
  id = purple_status_get_id(status);
/* Only go idle if active status is available  */
  if (!(strcmp(id,"available") != 0)) {
    if (time > 0) {
      rc = nm_send_set_status(user,5,0,0,0,0);
    }
    else {
      rc = nm_send_set_status(user,2,0,0,0,0);
    }
  }
  _check_for_disconnect(user,rc);
}

static void novell_get_info(PurpleConnection *gc,const char *name)
{
  NMUserRecord *user_record;
  NMUser *user;
  NMERR_T rc;
  if ((gc == ((PurpleConnection *)((void *)0))) || (name == ((const char *)((void *)0)))) 
    return ;
  user = ((NMUser *)(gc -> proto_data));
  if (user != 0) {
    user_record = nm_find_user_record(user,name);
    if (user_record != 0) {
      _show_info(gc,user_record,g_strdup(name));
    }
    else {
      rc = nm_send_get_details(user,name,_get_details_resp_show_info,(g_strdup(name)));
      _check_for_disconnect(user,rc);
    }
  }
}

static char *novell_status_text(PurpleBuddy *buddy)
{
  const char *text = (const char *)((void *)0);
  const char *dn = (const char *)((void *)0);
  PurpleAccount *account;
  account = ((buddy != 0)?purple_buddy_get_account(buddy) : ((struct _PurpleAccount *)((void *)0)));
  if ((buddy != 0) && (account != 0)) {
    PurpleConnection *gc = purple_account_get_connection(account);
    if ((gc != 0) && ((gc -> proto_data) != 0)) {
      NMUser *user = (gc -> proto_data);
      dn = nm_lookup_dn(user,purple_buddy_get_name(buddy));
      if (dn != 0) {
        NMUserRecord *user_record = nm_find_user_record(user,dn);
        if (user_record != 0) {
          text = nm_user_record_get_status_text(user_record);
          if (text != 0) 
            return g_strdup(text);
        }
      }
    }
  }
  return 0;
}

static GList *novell_status_types(PurpleAccount *account)
{
  GList *status_types = (GList *)((void *)0);
  PurpleStatusType *type;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_AVAILABLE,"available",0,(!0),(!0),0,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  status_types = g_list_append(status_types,type);
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_AWAY,"away",0,(!0),(!0),0,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  status_types = g_list_append(status_types,type);
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_UNAVAILABLE,"busy",((const char *)(dgettext("pidgin","Busy"))),(!0),(!0),0,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  status_types = g_list_append(status_types,type);
  type = purple_status_type_new_full(PURPLE_STATUS_INVISIBLE,"appearoffline",0,(!0),(!0),0);
  status_types = g_list_append(status_types,type);
  type = purple_status_type_new_full(PURPLE_STATUS_OFFLINE,0,0,(!0),(!0),0);
  status_types = g_list_append(status_types,type);
  return status_types;
}

static void novell_set_status(PurpleAccount *account,PurpleStatus *status)
{
  PurpleConnection *gc;
  gboolean connected;
  PurplePresence *presence;
  PurpleStatusType *type;
  PurpleStatusPrimitive primitive;
  NMUser *user;
  NMSTATUS_T novellstatus = 2;
  NMERR_T rc = 0L;
  const char *msg = (const char *)((void *)0);
  char *text = (char *)((void *)0);
  connected = purple_account_is_connected(account);
  presence = purple_status_get_presence(status);
  type = purple_status_get_type(status);
  primitive = purple_status_type_get_primitive(type);
/*
	 * We don't have any independent statuses, so we don't need to
	 * do anything when a status is deactivated (because another
	 * status is about to be activated).
	 */
  if (!(purple_status_is_active(status) != 0)) 
    return ;
  if (!(connected != 0)) 
    return ;
  gc = purple_account_get_connection(account);
  user = (gc -> proto_data);
  if (user == ((NMUser *)((void *)0))) 
    return ;
  if (primitive == PURPLE_STATUS_AVAILABLE) {
    novellstatus = 2;
  }
  else if (primitive == PURPLE_STATUS_AWAY) {
    novellstatus = 4;
  }
  else if (primitive == PURPLE_STATUS_UNAVAILABLE) {
    novellstatus = 3;
  }
  else if (primitive == PURPLE_STATUS_INVISIBLE) {
    novellstatus = 1;
  }
  else if (purple_presence_is_idle(presence) != 0) {
    novellstatus = 5;
  }
  else {
    novellstatus = 2;
  }
  if (((primitive == PURPLE_STATUS_AWAY) || (primitive == PURPLE_STATUS_AVAILABLE)) || (primitive == PURPLE_STATUS_UNAVAILABLE)) {
    msg = purple_status_get_attr_string(status,"message");
    text = g_strdup(msg);
    if (primitive == PURPLE_STATUS_AVAILABLE) 
/* no auto replies for online status */
      msg = ((const char *)((void *)0));
/* Don't want newlines in status text */
    purple_util_chrreplace(text,10,32);
  }
  rc = nm_send_set_status(user,novellstatus,text,msg,0,0);
  _check_for_disconnect(user,rc);
  if (text != 0) 
    g_free(text);
}

static void novell_add_permit(PurpleConnection *gc,const char *who)
{
  NMUser *user;
  NMERR_T rc = 0L;
  const char *name = who;
  if ((gc == ((PurpleConnection *)((void *)0))) || (who == ((const char *)((void *)0)))) 
    return ;
  user = (gc -> proto_data);
  if (user == ((NMUser *)((void *)0))) 
    return ;
/* Remove first -- we will add it back in when we get
	 * the okay from the server
	 */
  purple_privacy_permit_remove((gc -> account),who,(!0));
  if (nm_user_is_privacy_locked(user) != 0) {
    _show_privacy_locked_error(gc,user);
    _sync_privacy_lists(user);
    return ;
  }
/* Work around for problem with un-typed, dotted contexts */
  if (strchr(who,'.') != 0) {
    const char *dn = nm_lookup_dn(user,who);
    if (dn == ((const char *)((void *)0))) {
      rc = nm_send_get_details(user,who,_get_details_send_privacy_create,((gpointer )((gpointer )(!0))));
      _check_for_disconnect(user,rc);
      return ;
    }
    else {
      name = dn;
    }
  }
  rc = nm_send_create_privacy_item(user,name,(!0),_create_privacy_item_permit_resp_cb,(g_strdup(who)));
  _check_for_disconnect(user,rc);
}

static void novell_add_deny(PurpleConnection *gc,const char *who)
{
  NMUser *user;
  NMERR_T rc = 0L;
  const char *name = who;
  if ((gc == ((PurpleConnection *)((void *)0))) || (who == ((const char *)((void *)0)))) 
    return ;
  user = (gc -> proto_data);
  if (user == ((NMUser *)((void *)0))) 
    return ;
/* Remove first -- we will add it back in when we get
	 * the okay from the server
	 */
  purple_privacy_deny_remove((gc -> account),who,(!0));
  if (nm_user_is_privacy_locked(user) != 0) {
    _show_privacy_locked_error(gc,user);
    _sync_privacy_lists(user);
    return ;
  }
/* Work around for problem with un-typed, dotted contexts */
  if (strchr(who,'.') != 0) {
    const char *dn = nm_lookup_dn(user,who);
    if (dn == ((const char *)((void *)0))) {
      rc = nm_send_get_details(user,who,_get_details_send_privacy_create,0);
      _check_for_disconnect(user,rc);
      return ;
    }
    else {
      name = dn;
    }
  }
  rc = nm_send_create_privacy_item(user,name,0,_create_privacy_item_deny_resp_cb,(g_strdup(who)));
  _check_for_disconnect(user,rc);
}

static void novell_rem_permit(PurpleConnection *gc,const char *who)
{
  NMUser *user;
  NMERR_T rc = 0L;
  const char *dn = (const char *)((void *)0);
  if ((gc == ((PurpleConnection *)((void *)0))) || (who == ((const char *)((void *)0)))) 
    return ;
  user = (gc -> proto_data);
  if (user == ((NMUser *)((void *)0))) 
    return ;
  if (nm_user_is_privacy_locked(user) != 0) {
    _show_privacy_locked_error(gc,user);
    _sync_privacy_lists(user);
    return ;
  }
  dn = nm_lookup_dn(user,who);
  if (dn == ((const char *)((void *)0))) 
    dn = who;
  rc = nm_send_remove_privacy_item(user,dn,(!0),_remove_privacy_item_resp_cb,(g_strdup(who)));
  _check_for_disconnect(user,rc);
}

static void novell_rem_deny(PurpleConnection *gc,const char *who)
{
  NMUser *user;
  NMERR_T rc = 0L;
  const char *dn = (const char *)((void *)0);
  if ((gc == ((PurpleConnection *)((void *)0))) || (who == ((const char *)((void *)0)))) 
    return ;
  user = (gc -> proto_data);
  if (user == ((NMUser *)((void *)0))) 
    return ;
  if (nm_user_is_privacy_locked(user) != 0) {
    _show_privacy_locked_error(gc,user);
    _sync_privacy_lists(user);
    return ;
  }
  dn = nm_lookup_dn(user,who);
  if (dn == ((const char *)((void *)0))) 
    dn = who;
  rc = nm_send_remove_privacy_item(user,dn,0,_remove_privacy_item_resp_cb,(g_strdup(who)));
  _check_for_disconnect(user,rc);
}

static void novell_set_permit_deny(PurpleConnection *gc)
{
  NMERR_T rc = 0L;
  const char *dn;
  const char *name = (const char *)((void *)0);
  NMUserRecord *user_record = (NMUserRecord *)((void *)0);
  GSList *node = (GSList *)((void *)0);
  GSList *copy = (GSList *)((void *)0);
  NMUser *user;
  int i;
  int j;
  int num_contacts;
  int num_folders;
  NMContact *contact;
  NMFolder *folder = (NMFolder *)((void *)0);
  if (gc == ((PurpleConnection *)((void *)0))) 
    return ;
  user = (gc -> proto_data);
  if (user == ((NMUser *)((void *)0))) 
    return ;
  if ((user -> privacy_synched) == 0) {
    _sync_privacy_lists(user);
    user -> privacy_synched = (!0);
    return ;
  }
  if (nm_user_is_privacy_locked(user) != 0) {
    _show_privacy_locked_error(gc,user);
    _sync_privacy_lists(user);
    return ;
  }
{
    switch(( *(gc -> account)).perm_deny){
      case PURPLE_PRIVACY_ALLOW_ALL:
{
        rc = nm_send_set_privacy_default(user,0,_set_privacy_default_resp_cb,0);
        _check_for_disconnect(user,rc);
/* clear server side deny list */
        if (rc == 0L) {
          copy = g_slist_copy((user -> deny_list));
{
            for (node = copy; (node != 0) && ((node -> data) != 0); node = (node -> next)) {
              rc = nm_send_remove_privacy_item(user,((const char *)(node -> data)),0,0,0);
              if (_check_for_disconnect(user,rc) != 0) 
                break; 
            }
          }
          g_slist_free(copy);
          g_slist_free((user -> deny_list));
          user -> deny_list = ((GSList *)((void *)0));
        }
        break; 
      }
      case PURPLE_PRIVACY_DENY_ALL:
{
        rc = nm_send_set_privacy_default(user,(!0),_set_privacy_default_resp_cb,0);
        _check_for_disconnect(user,rc);
/* clear server side allow list */
        if (rc == 0L) {
          copy = g_slist_copy((user -> allow_list));
{
            for (node = copy; (node != 0) && ((node -> data) != 0); node = (node -> next)) {
              rc = nm_send_remove_privacy_item(user,((const char *)(node -> data)),(!0),0,0);
              if (_check_for_disconnect(user,rc) != 0) 
                break; 
            }
          }
          g_slist_free(copy);
          g_slist_free((user -> allow_list));
          user -> allow_list = ((GSList *)((void *)0));
        }
        break; 
      }
      case PURPLE_PRIVACY_ALLOW_USERS:
{
        rc = nm_send_set_privacy_default(user,(!0),_set_privacy_default_resp_cb,0);
        _check_for_disconnect(user,rc);
/* sync allow lists */
        if (rc == 0L) {
          for (node = (user -> allow_list); node != 0; node = (node -> next)) {
            user_record = nm_find_user_record(user,((char *)(node -> data)));
            if (user_record != 0) {
              name = nm_user_record_get_display_id(user_record);
              if (!(g_slist_find_custom(( *(gc -> account)).permit,name,((GCompareFunc )purple_utf8_strcasecmp)) != 0)) {
                purple_privacy_permit_add((gc -> account),name,(!0));
              }
            }
          }
          for (node = ( *(gc -> account)).permit; node != 0; node = (node -> next)) {
            dn = nm_lookup_dn(user,((char *)(node -> data)));
            if (dn != 0) {
              if (!(g_slist_find_custom((user -> allow_list),dn,((GCompareFunc )purple_utf8_strcasecmp)) != 0)) {
                rc = nm_send_create_privacy_item(user,dn,(!0),_create_privacy_item_deny_resp_cb,(g_strdup(dn)));
              }
            }
            else {
              purple_privacy_permit_remove((gc -> account),((char *)(node -> data)),(!0));
            }
          }
        }
        break; 
      }
      case PURPLE_PRIVACY_DENY_USERS:
{
/* set to default allow */
        rc = nm_send_set_privacy_default(user,0,_set_privacy_default_resp_cb,0);
        _check_for_disconnect(user,rc);
/* sync deny lists */
        if (rc == 0L) {
          for (node = (user -> deny_list); node != 0; node = (node -> next)) {
            user_record = nm_find_user_record(user,((char *)(node -> data)));
            if (user_record != 0) {
              name = nm_user_record_get_display_id(user_record);
              if (!(g_slist_find_custom(( *(gc -> account)).deny,name,((GCompareFunc )purple_utf8_strcasecmp)) != 0)) {
                purple_privacy_deny_add((gc -> account),name,(!0));
              }
            }
          }
          for (node = ( *(gc -> account)).deny; node != 0; node = (node -> next)) {
            name = ((const char *)((void *)0));
            dn = nm_lookup_dn(user,((char *)(node -> data)));
            if (dn != 0) {
              user_record = nm_find_user_record(user,dn);
              name = nm_user_record_get_display_id(user_record);
              if (!(g_slist_find_custom((user -> deny_list),dn,((GCompareFunc )purple_utf8_strcasecmp)) != 0)) {
                rc = nm_send_create_privacy_item(user,dn,0,_create_privacy_item_deny_resp_cb,(g_strdup(name)));
              }
            }
            else {
              purple_privacy_deny_remove((gc -> account),((char *)(node -> data)),(!0));
            }
          }
        }
        break; 
      }
      case PURPLE_PRIVACY_ALLOW_BUDDYLIST:
{
/* remove users from allow list that are not in buddy list */
        copy = g_slist_copy((user -> allow_list));
        for (node = copy; (node != 0) && ((node -> data) != 0); node = (node -> next)) {
          if (!(nm_find_contacts(user,(node -> data)) != 0)) {
            rc = nm_send_remove_privacy_item(user,((const char *)(node -> data)),(!0),0,0);
            if (_check_for_disconnect(user,rc) != 0) 
              return ;
          }
        }
        g_slist_free(copy);
/* add all buddies to allow list */
        num_contacts = nm_folder_get_contact_count((user -> root_folder));
        for (i = 0; i < num_contacts; i++) {
          contact = nm_folder_get_contact((user -> root_folder),i);
          dn = nm_contact_get_dn(contact);
          if ((dn != 0) && !(g_slist_find_custom((user -> allow_list),dn,((GCompareFunc )purple_utf8_strcasecmp)) != 0)) {
            rc = nm_send_create_privacy_item(user,dn,(!0),_create_privacy_item_deny_resp_cb,(g_strdup(dn)));
            if (_check_for_disconnect(user,rc) != 0) 
              return ;
          }
        }
        num_folders = nm_folder_get_subfolder_count((user -> root_folder));
        for (i = 0; i < num_folders; i++) {
          folder = nm_folder_get_subfolder((user -> root_folder),i);
          num_contacts = nm_folder_get_contact_count(folder);
          for (j = 0; j < num_contacts; j++) {
            contact = nm_folder_get_contact(folder,j);
            dn = nm_contact_get_dn(contact);
            if ((dn != 0) && !(g_slist_find_custom((user -> allow_list),dn,((GCompareFunc )purple_utf8_strcasecmp)) != 0)) {
              rc = nm_send_create_privacy_item(user,dn,(!0),_create_privacy_item_deny_resp_cb,(g_strdup(dn)));
              if (_check_for_disconnect(user,rc) != 0) 
                return ;
            }
          }
        }
/* set to default deny */
        rc = nm_send_set_privacy_default(user,(!0),_set_privacy_default_resp_cb,0);
        if (_check_for_disconnect(user,rc) != 0) 
          break; 
        break; 
      }
    }
  }
}

static GList *novell_blist_node_menu(PurpleBlistNode *node)
{
  GList *list = (GList *)((void *)0);
  PurpleMenuAction *act;
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    act = purple_menu_action_new(((const char *)(dgettext("pidgin","Initiate _Chat"))),((PurpleCallback )_initiate_conference_cb),0,0);
    list = g_list_append(list,act);
  }
  return list;
}

static void novell_keepalive(PurpleConnection *gc)
{
  NMUser *user;
  NMERR_T rc = 0L;
  if (gc == ((PurpleConnection *)((void *)0))) 
    return ;
  user = (gc -> proto_data);
  if (user == ((NMUser *)((void *)0))) 
    return ;
  rc = nm_send_keepalive(user,0,0);
  _check_for_disconnect(user,rc);
}
static PurplePluginProtocolInfo prpl_info = {(0), ((GList *)((void *)0)), ((GList *)((void *)0)), 
/* user_splits */
/* protocol_options */
/* icon_spec */
{((char *)((void *)0)), (0), (0), (0), (0), (0), (0)}, (novell_list_icon), ((const char *(*)(PurpleBuddy *))((void *)0)), (novell_status_text), (novell_tooltip_text), (novell_status_types), (novell_blist_node_menu), ((GList *(*)(PurpleConnection *))((void *)0)), ((GHashTable *(*)(PurpleConnection *, const char *))((void *)0)), (novell_login), (novell_close), (novell_send_im), ((void (*)(PurpleConnection *, const char *))((void *)0)), (novell_send_typing), (novell_get_info), (novell_set_status), (novell_set_idle), ((void (*)(PurpleConnection *, const char *, const char *))((void *)0)), (novell_add_buddy), ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)), (novell_remove_buddy), ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)), (novell_add_permit), (novell_add_deny), (novell_rem_permit), (novell_rem_deny), (novell_set_permit_deny), ((void (*)(PurpleConnection *, GHashTable *))((void *)0)), ((void (*)(PurpleConnection *, GHashTable *))((void *)0)), ((char *(*)(GHashTable *))((void *)0)), (novell_chat_invite), (novell_chat_leave), ((void (*)(PurpleConnection *, int , const char *, const char *))((void *)0)), (novell_chat_send), (novell_keepalive), ((void (*)(PurpleAccount *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), (novell_alias_buddy), (novell_group_buddy), (novell_rename_group), ((void (*)(PurpleBuddy *))((void *)0)), (novell_convo_closed), (purple_normalize_nocase), ((void (*)(PurpleConnection *, PurpleStoredImage *))((void *)0)), (novell_remove_group), ((char *(*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((PurpleChat *(*)(PurpleAccount *, const char *))((void *)0)), ((PurpleRoomlist *(*)(PurpleConnection *))((void *)0)), ((void (*)(PurpleRoomlist *))((void *)0)), ((void (*)(PurpleRoomlist *, PurpleRoomlistRoom *))((void *)0)), ((gboolean (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *, const char *))((void *)0)), ((PurpleXfer *(*)(PurpleConnection *, const char *))((void *)0)), ((gboolean (*)(const PurpleBuddy *))((void *)0)), ((PurpleWhiteboardPrplOps *)((void *)0)), ((int (*)(PurpleConnection *, const char *, int ))((void *)0)), ((char *(*)(PurpleRoomlistRoom *))((void *)0)), ((void (*)(PurpleAccount *, PurpleAccountUnregistrationCb , void *))((void *)0)), ((gboolean (*)(PurpleConnection *, const char *, guint ))((void *)0)), ((GList *(*)(PurpleAccount *))((void *)0)), ((sizeof(PurplePluginProtocolInfo ))), ((GHashTable *(*)(PurpleAccount *))((void *)0)), ((gboolean (*)(PurpleAccount *, const char *, PurpleMediaSessionType ))((void *)0)), ((PurpleMediaCaps (*)(PurpleAccount *, const char *))((void *)0)), ((PurpleMood *(*)(PurpleAccount *))((void *)0)), ((void (*)(PurpleConnection *, const char *, PurpleSetPublicAliasSuccessCallback , PurpleSetPublicAliasFailureCallback ))((void *)0)), ((void (*)(PurpleConnection *, PurpleGetPublicAliasSuccessCallback , PurpleGetPublicAliasFailureCallback ))((void *)0)), ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *, const char *))((void *)0)), ((void (*)(PurpleConnection *, GList *, GList *, const char *))((void *)0))
/* list_icon */
/* list_emblems */
/* status_text */
/* tooltip_text */
/* status_types */
/* blist_node_menu */
/* chat_info */
/* chat_info_defaults */
/* login */
/* close */
/* send_im */
/* set_info */
/* send_typing */
/* get_info */
/* set_status */
/* set_idle */
/* change_passwd */
/* add_buddy */
/* add_buddies */
/* remove_buddy */
/* remove_buddies */
/* add_permit */
/* add_deny */
/* rem_permit */
/* rem_deny */
/* set_permit_deny */
/* join_chat */
/* reject_chat */
/* get_chat_name */
/* chat_invite */
/* chat_leave */
/* chat_whisper */
/* chat_send */
/* keepalive */
/* register_user */
/* get_cb_info */
/* get_cb_away */
/* alias_buddy */
/* group_buddy */
/* rename_group */
/* buddy_free */
/* convo_closed */
/* normalize */
/* set_buddy_icon */
/* remove_group */
/* get_cb_real_name */
/* set_chat_topic */
/* find_blist_chat */
/* roomlist_get_list */
/* roomlist_cancel */
/* roomlist_expand_category */
/* can_receive_file */
/* send_file */
/* new_xfer */
/* offline_message */
/* whiteboard_prpl_ops */
/* send_raw */
/* roomlist_room_serialize */
/* unregister_user */
/* send_attention */
/* get_attention_types */
/* struct_size */
/* get_account_text_table */
/* initiate_media */
/* get_media_caps */
/* get_moods */
/* set_public_alias */
/* get_public_alias */
/* add_buddy_with_invite */
/* add_buddies_with_invite */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_PROTOCOL), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("prpl-novell"), ("GroupWise"), ("2.10.9"), ("Novell GroupWise Messenger Protocol Plugin"), ("Novell GroupWise Messenger Protocol Plugin"), ((char *)((void *)0)), ("http://pidgin.im/"), ((gboolean (*)(PurplePlugin *))((void *)0)), ((gboolean (*)(PurplePlugin *))((void *)0)), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((&prpl_info)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
  PurpleAccountOption *option;
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","Server address"))),"server",0);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_int_new(((const char *)(dgettext("pidgin","Server port"))),"port",8300);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  my_protocol = plugin;
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
