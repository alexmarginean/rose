/*
 * nmconn.c
 *
 * Copyright (c) 2004 Novell, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA	02111-1301	USA
 *
 */
#include <glib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "nmconn.h"
#ifdef _WIN32
#include <windows.h>
#endif
#define NO_ESCAPE(ch) ((ch == 0x20) || (ch >= 0x30 && ch <= 0x39) || \
					(ch >= 0x41 && ch <= 0x5a) || (ch >= 0x61 && ch <= 0x7a))
/* Read data from conn until the end of a line */

static NMERR_T read_line(NMConn *conn,char *buff,int len)
{
  NMERR_T rc = 0L;
  int total_bytes = 0;
{
    while((rc == 0L) && (total_bytes < (len - 1))){
      rc = nm_read_all(conn,(buff + total_bytes),1);
      if (rc == 0L) {
        total_bytes += 1;
        if (buff[total_bytes - 1] == 10) {
          break; 
        }
      }
    }
  }
  buff[total_bytes] = 0;
  return rc;
}

static char *url_escape_string(char *src)
{
  guint32 escape = 0;
  char *p;
  char *q;
  char *encoded = (char *)((void *)0);
  int ch;
  static const char hex_table[16UL] = "0123456789abcdef";
  if (src == ((char *)((void *)0))) {
    return 0;
  }
/* Find number of chars to escape */
  for (p = src; ( *p) != 0; p++) {
    ch = ((guchar )( *p));
    if (!((((ch == 32) || ((ch >= 48) && (ch <= 0x39))) || ((ch >= 65) && (ch <= 0x5a))) || ((ch >= 0x61) && (ch <= 0x7a)))) {
      escape++;
    }
  }
  encoded = (g_malloc((((p - src) + (escape * 2)) + 1)));
/* Escape the string */
  for (((p = src) , (q = encoded)); ( *p) != 0; p++) {
    ch = ((guchar )( *p));
    if ((((ch == 32) || ((ch >= 48) && (ch <= 0x39))) || ((ch >= 65) && (ch <= 0x5a))) || ((ch >= 0x61) && (ch <= 0x7a))) {
      if (ch != 32) {
         *q = ch;
        q++;
      }
      else {
         *q = '+';
        q++;
      }
    }
    else {
       *q = '%';
      q++;
       *q = hex_table[ch >> 4];
      q++;
       *q = hex_table[ch & 15];
      q++;
    }
  }
   *q = 0;
  return encoded;
}

static char *encode_method(guint8 method)
{
  char *str;
  switch(method){
    case 4:
{
      str = "G";
      break; 
    }
    case 6:
{
      str = "F";
      break; 
    }
    case 10:
{
      str = "E";
      break; 
    }
    case 12:
{
      str = "D";
      break; 
    }
    case 14:
{
      str = "C";
      break; 
    }
    case 15:
{
      str = "B";
      break; 
    }
    case 16:
{
      str = "A";
      break; 
    }
    case 17:
{
      str = "9";
      break; 
    }
    case 19:
{
      str = "8";
      break; 
    }
    case 20:
{
      str = "7";
      break; 
    }
    case 40:
{
      str = "6";
      break; 
    }
    case 41:
{
      str = "5";
      break; 
    }
    case 42:
{
      str = "4";
      break; 
    }
    case 3:
{
      str = "3";
      break; 
    }
    case 2:
{
      str = "2";
      break; 
    }
    case 5:
{
      str = "1";
      break; 
    }
/* NMFIELD_METHOD_VALID */
    default:
{
      str = "0";
      break; 
    }
  }
  return str;
}

NMConn *nm_create_conn(const char *addr,int port)
{
  NMConn *conn = (NMConn *)(g_malloc0_n(1,(sizeof(NMConn ))));
  conn -> addr = g_strdup(addr);
  conn -> port = port;
  return conn;
}

void nm_release_conn(NMConn *conn)
{
  if (conn != 0) {
    GSList *node;
    for (node = (conn -> requests); node != 0; node = (node -> next)) {
      if ((node -> data) != 0) 
        nm_release_request((node -> data));
    }
    g_slist_free((conn -> requests));
    conn -> requests = ((GSList *)((void *)0));
    if ((conn -> ssl_conn) != 0) {
      g_free((conn -> ssl_conn));
      conn -> ssl_conn = ((NMSSLConn *)((void *)0));
    }
    g_free((conn -> addr));
    conn -> addr = ((char *)((void *)0));
    g_free(conn);
  }
}

int nm_tcp_write(NMConn *conn,const void *buff,int len)
{
  if ((conn == ((NMConn *)((void *)0))) || (buff == ((const void *)((void *)0)))) 
    return -1;
  if (!((conn -> use_ssl) != 0)) 
    return (write((conn -> fd),buff,len));
  else if (((conn -> ssl_conn) != 0) && (( *(conn -> ssl_conn)).write != 0)) 
    return ( *( *(conn -> ssl_conn)).write)(( *(conn -> ssl_conn)).data,buff,len);
  else 
    return -1;
}

int nm_tcp_read(NMConn *conn,void *buff,int len)
{
  if ((conn == ((NMConn *)((void *)0))) || (buff == ((void *)((void *)0)))) 
    return -1;
  if (!((conn -> use_ssl) != 0)) 
    return (read((conn -> fd),buff,len));
  else if (((conn -> ssl_conn) != 0) && (( *(conn -> ssl_conn)).read != 0)) 
    return ( *( *(conn -> ssl_conn)).read)(( *(conn -> ssl_conn)).data,buff,len);
  else 
    return -1;
}

NMERR_T nm_read_all(NMConn *conn,char *buff,int len)
{
  NMERR_T rc = 0L;
  int bytes_left = len;
  int bytes_read;
  int total_bytes = 0;
  int retry = 1000;
  if ((conn == ((NMConn *)((void *)0))) || (buff == ((char *)((void *)0)))) 
    return (0x2000L + 1);
{
/* Keep reading until buffer is full */
    while(bytes_left != 0){
      bytes_read = nm_tcp_read(conn,(buff + total_bytes),bytes_left);
      if (bytes_read > 0) {
        bytes_left -= bytes_read;
        total_bytes += bytes_read;
      }
      else {
        if ( *__errno_location() == 11) {
          if (--retry == 0) {
            rc = (0x2000L + 3);
            break; 
          }
#ifdef _WIN32
#else
          usleep(1000);
#endif
        }
        else {
          rc = (0x2000L + 3);
          break; 
        }
      }
    }
  }
  return rc;
}

NMERR_T nm_read_uint32(NMConn *conn,guint32 *val)
{
  NMERR_T rc = 0L;
  rc = nm_read_all(conn,((char *)val),(sizeof(( *val))));
  if (rc == 0L) {
     *val = ((guint32 )( *val));
  }
  return rc;
}

NMERR_T nm_read_uint16(NMConn *conn,guint16 *val)
{
  NMERR_T rc = 0L;
  rc = nm_read_all(conn,((char *)val),(sizeof(( *val))));
  if (rc == 0L) {
     *val = ((guint16 )( *val));
  }
  return rc;
}

NMERR_T nm_write_fields(NMConn *conn,NMField *fields)
{
  NMERR_T rc = 0L;
  NMField *field;
  char *value = (char *)((void *)0);
  char *method = (char *)((void *)0);
  char buffer[4096UL];
  int ret;
  int bytes_to_send;
  int val = 0;
  if ((conn == ((NMConn *)((void *)0))) || (fields == ((NMField *)((void *)0)))) {
    return (0x2000L + 1);
  }
/* Format each field as valid "post" data and write it out */
  for (field = fields; (rc == 0L) && ((field -> tag) != 0); field++) {
/* We don't currently handle binary types */
    if (((field -> method) == 1) || ((field -> type) == 2)) {
      continue; 
    }
/* Write the field tag */
    bytes_to_send = g_snprintf(buffer,(sizeof(buffer)),"&tag=%s",(field -> tag));
    ret = nm_tcp_write(conn,buffer,bytes_to_send);
    if (ret < 0) {
      rc = (0x2000L + 2);
    }
/* Write the field method */
    if (rc == 0L) {
      method = encode_method((field -> method));
      bytes_to_send = g_snprintf(buffer,(sizeof(buffer)),"&cmd=%s",method);
      ret = nm_tcp_write(conn,buffer,bytes_to_send);
      if (ret < 0) {
        rc = (0x2000L + 2);
      }
    }
/* Write the field value */
    if (rc == 0L) {
      switch((field -> type)){
        case 10:
{
        }
        case 13:
{
          value = url_escape_string(((char *)(field -> ptr_value)));
          bytes_to_send = g_snprintf(buffer,(sizeof(buffer)),"&val=%s",value);
          if (bytes_to_send > ((int )(sizeof(buffer)))) {
            ret = nm_tcp_write(conn,buffer,(sizeof(buffer)));
          }
          else {
            ret = nm_tcp_write(conn,buffer,bytes_to_send);
          }
          if (ret < 0) {
            rc = (0x2000L + 2);
          }
          g_free(value);
          break; 
        }
        case 9:
{
        }
        case 12:
{
          val = (nm_count_fields(((NMField *)(field -> ptr_value))));
          bytes_to_send = g_snprintf(buffer,(sizeof(buffer)),"&val=%u",val);
          ret = nm_tcp_write(conn,buffer,bytes_to_send);
          if (ret < 0) {
            rc = (0x2000L + 2);
          }
          break; 
        }
        default:
{
          bytes_to_send = g_snprintf(buffer,(sizeof(buffer)),"&val=%u",(field -> value));
          ret = nm_tcp_write(conn,buffer,bytes_to_send);
          if (ret < 0) {
            rc = (0x2000L + 2);
          }
          break; 
        }
      }
    }
/* Write the field type */
    if (rc == 0L) {
      bytes_to_send = g_snprintf(buffer,(sizeof(buffer)),"&type=%u",(field -> type));
      ret = nm_tcp_write(conn,buffer,bytes_to_send);
      if (ret < 0) {
        rc = (0x2000L + 2);
      }
    }
/* If the field is a sub array then post its fields */
    if ((rc == 0L) && (val > 0)) {
      if (((field -> type) == 9) || ((field -> type) == 12)) {
        rc = nm_write_fields(conn,((NMField *)(field -> ptr_value)));
      }
    }
  }
  return rc;
}

NMERR_T nm_send_request(NMConn *conn,char *cmd,NMField *fields,nm_response_cb cb,gpointer data,NMRequest **request)
{
  NMERR_T rc = 0L;
  char buffer[512UL];
  int bytes_to_send;
  int ret;
  NMField *request_fields = (NMField *)((void *)0);
  char *str = (char *)((void *)0);
  if ((conn == ((NMConn *)((void *)0))) || (cmd == ((char *)((void *)0)))) 
    return (0x2000L + 1);
/* Write the post */
  bytes_to_send = g_snprintf(buffer,(sizeof(buffer)),"POST /%s HTTP/1.0\r\n",cmd);
  ret = nm_tcp_write(conn,buffer,bytes_to_send);
  if (ret < 0) {
    rc = (0x2000L + 2);
  }
/* Write headers */
  if (rc == 0L) {
    if (strcmp("login",cmd) == 0) {
      bytes_to_send = g_snprintf(buffer,(sizeof(buffer)),"Host: %s:%d\r\n\r\n",(conn -> addr),(conn -> port));
      ret = nm_tcp_write(conn,buffer,bytes_to_send);
      if (ret < 0) {
        rc = (0x2000L + 2);
      }
    }
    else {
      bytes_to_send = g_snprintf(buffer,(sizeof(buffer)),"\r\n");
      ret = nm_tcp_write(conn,buffer,bytes_to_send);
      if (ret < 0) {
        rc = (0x2000L + 2);
      }
    }
  }
/* Add the transaction id to the request fields */
  if (rc == 0L) {
    if (fields != 0) 
      request_fields = nm_copy_field_array(fields);
    str = g_strdup_printf("%d",++conn -> trans_id);
    request_fields = nm_field_add_pointer(request_fields,"NM_A_SZ_TRANSACTION_ID",0,0,0,str,10);
  }
/* Send the request to the server */
  if (rc == 0L) {
    rc = nm_write_fields(conn,request_fields);
  }
/* Write the CRLF to terminate the data */
  if (rc == 0L) {
    ret = nm_tcp_write(conn,"\r\n",(strlen("\r\n")));
    if (ret < 0) {
      rc = (0x2000L + 2);
    }
  }
/* Create a request struct, add it to our queue, and return it */
  if (rc == 0L) {
    NMRequest *new_request = nm_create_request(cmd,(conn -> trans_id),(time(0)),cb,0,data);
    nm_conn_add_request_item(conn,new_request);
/* Set the out param if it was sent in, otherwise release the request */
    if (request != 0) 
       *request = new_request;
    else 
      nm_release_request(new_request);
  }
  if (request_fields != ((NMField *)((void *)0))) 
    nm_free_fields(&request_fields);
  return rc;
}

NMERR_T nm_read_header(NMConn *conn)
{
  NMERR_T rc = 0L;
  char buffer[512UL];
  char *ptr = (char *)((void *)0);
  int i;
  char rtn_buf[8UL];
  int rtn_code = 0;
  if (conn == ((NMConn *)((void *)0))) 
    return (0x2000L + 1);
   *buffer = 0;
  rc = read_line(conn,buffer,(sizeof(buffer)));
  if (rc == 0L) {
/* Find the return code */
    ptr = strchr(buffer,32);
    if (ptr != ((char *)((void *)0))) {
      ptr++;
      i = 0;
      while(((( *__ctype_b_loc())[(int )( *ptr)] & ((unsigned short )_ISdigit)) != 0) && (i < 3)){
        rtn_buf[i] =  *ptr;
        i++;
        ptr++;
      }
      rtn_buf[i] = 0;
      if (i > 0) 
        rtn_code = atoi(rtn_buf);
    }
  }
/* Finish reading header, in the future we might want to do more processing here */
/* TODO: handle more general redirects in the future */
  while((rc == 0L) && (strcmp(buffer,"\r\n") != 0)){
    rc = read_line(conn,buffer,(sizeof(buffer)));
  }
  if ((rc == 0L) && (rtn_code == 301)) 
    rc = (0x2000L + 5);
  return rc;
}

NMERR_T nm_read_fields(NMConn *conn,int count,NMField **fields)
{
  NMERR_T rc = 0L;
  guint8 type;
  guint8 method;
  guint32 val;
  char tag[64UL];
  NMField *sub_fields = (NMField *)((void *)0);
  char *str = (char *)((void *)0);
  if ((conn == ((NMConn *)((void *)0))) || (fields == ((NMField **)((void *)0)))) 
    return (0x2000L + 1);
{
    do {
      if (count > 0) {
        count--;
      }
/* Read the field type, method, and tag */
      rc = nm_read_all(conn,((char *)(&type)),(sizeof(type)));
      if ((rc != 0L) || (type == 0)) 
        break; 
      rc = nm_read_all(conn,((char *)(&method)),(sizeof(method)));
      if (rc != 0L) 
        break; 
      rc = nm_read_uint32(conn,&val);
      if (rc != 0L) 
        break; 
      if (val > sizeof(tag)) {
        rc = (0x2000L + 4);
        break; 
      }
      rc = nm_read_all(conn,tag,val);
      if (rc != 0L) 
        break; 
      if ((type == 12) || (type == 9)) {
/* Read the subarray (first read the number of items in the array) */
        rc = nm_read_uint32(conn,&val);
        if (rc != 0L) 
          break; 
        if (val > 0) {
          rc = nm_read_fields(conn,val,&sub_fields);
          if (rc != 0L) 
            break; 
        }
         *fields = nm_field_add_pointer( *fields,tag,0,method,0,sub_fields,type);
        sub_fields = ((NMField *)((void *)0));
      }
      else if ((type == 10) || (type == 13)) {
/* Read the string (first read the length) */
        rc = nm_read_uint32(conn,&val);
        if (rc != 0L) 
          break; 
        if (val >= 32768) {
          rc = (0x2000L + 4);
          break; 
        }
        if (val > 0) {
          str = ((char *)(g_malloc0_n((val + 1),(sizeof(char )))));
          rc = nm_read_all(conn,str,val);
          if (rc != 0L) 
            break; 
           *fields = nm_field_add_pointer( *fields,tag,0,method,0,str,type);
          str = ((char *)((void *)0));
        }
      }
      else {
/* Read the numerical value */
        rc = nm_read_uint32(conn,&val);
        if (rc != 0L) 
          break; 
         *fields = nm_field_add_number( *fields,tag,0,method,0,val,type);
      }
    }while ((type != 0) && (count != 0));
  }
  if (str != ((char *)((void *)0))) {
    g_free(str);
  }
  if (sub_fields != ((NMField *)((void *)0))) {
    nm_free_fields(&sub_fields);
  }
  return rc;
}

void nm_conn_add_request_item(NMConn *conn,NMRequest *request)
{
  if ((conn == ((NMConn *)((void *)0))) || (request == ((NMRequest *)((void *)0)))) 
    return ;
  nm_request_add_ref(request);
  conn -> requests = g_slist_append((conn -> requests),request);
}

void nm_conn_remove_request_item(NMConn *conn,NMRequest *request)
{
  if ((conn == ((NMConn *)((void *)0))) || (request == ((NMRequest *)((void *)0)))) 
    return ;
  conn -> requests = g_slist_remove((conn -> requests),request);
  nm_release_request(request);
}

NMRequest *nm_conn_find_request(NMConn *conn,int trans_id)
{
  NMRequest *req = (NMRequest *)((void *)0);
  GSList *itr = (GSList *)((void *)0);
  if (conn == ((NMConn *)((void *)0))) 
    return 0;
  itr = (conn -> requests);
  while(itr != 0){
    req = ((NMRequest *)(itr -> data));
    if ((req != ((NMRequest *)((void *)0))) && (nm_request_get_trans_id(req) == trans_id)) {
      return req;
    }
    itr = ((itr != 0)?( *((GSList *)itr)).next : ((struct _GSList *)((void *)0)));
  }
  return 0;
}

const char *nm_conn_get_addr(NMConn *conn)
{
  if (conn == ((NMConn *)((void *)0))) 
    return 0;
  else 
    return (conn -> addr);
}

int nm_conn_get_port(NMConn *conn)
{
  if (conn == ((NMConn *)((void *)0))) 
    return -1;
  else 
    return conn -> port;
}
