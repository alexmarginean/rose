/*
 * nmuser.c
 *
 * Copyright (c) 2004 Novell, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA	02111-1301	USA
 *
 */
#include "internal.h"
#include <string.h>
#include "nmfield.h"
#include "nmuser.h"
#include "nmconn.h"
#include "nmcontact.h"
#include "nmuserrecord.h"
#include "util.h"
/* This is the template that we wrap outgoing messages in, since the other
 * GW Messenger clients expect messages to be in RTF.
 */
#define RTF_TEMPLATE 	"{\\rtf1\\ansi\n"\
                        "{\\fonttbl{\\f0\\fnil Unknown;}}\n"\
						"{\\colortbl ;\\red0\\green0\\blue0;}\n"\
						"\\uc1\\cf1\\f0\\fs24 %s\\par\n}"
#define NM_MAX_MESSAGE_SIZE 2048
static NMERR_T nm_process_response(NMUser *user);
static void _update_contact_list(NMUser *user,NMField *fields);
static void _handle_multiple_get_details_login_cb(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data);
static char *nm_rtfize_text(char *text);
/**
 * See header for comments on on "public" functions
 */

NMUser *nm_initialize_user(const char *name,const char *server_addr,int port,gpointer data,nm_event_cb event_callback)
{
  NMUser *user;
  if (((name == ((const char *)((void *)0))) || (server_addr == ((const char *)((void *)0)))) || (event_callback == ((void (*)(NMUser *, NMEvent *))((void *)0)))) 
    return 0;
  user = ((NMUser *)(g_malloc0_n(1,(sizeof(NMUser )))));
  user -> contacts = g_hash_table_new_full(g_str_hash,nm_utf8_str_equal,g_free,((GDestroyNotify )nm_release_contact));
  user -> user_records = g_hash_table_new_full(g_str_hash,nm_utf8_str_equal,g_free,((GDestroyNotify )nm_release_user_record));
  user -> display_id_to_dn = g_hash_table_new_full(g_str_hash,nm_utf8_str_equal,g_free,g_free);
  user -> name = g_strdup(name);
  user -> conn = nm_create_conn(server_addr,port);
  ( *(user -> conn)).addr = g_strdup(server_addr);
  ( *(user -> conn)).port = port;
  user -> evt_callback = event_callback;
  user -> client_data = data;
  return user;
}

void nm_deinitialize_user(NMUser *user)
{
  nm_release_conn((user -> conn));
  if ((user -> contacts) != 0) {
    g_hash_table_destroy((user -> contacts));
  }
  if ((user -> user_records) != 0) {
    g_hash_table_destroy((user -> user_records));
  }
  if ((user -> display_id_to_dn) != 0) {
    g_hash_table_destroy((user -> display_id_to_dn));
  }
  if ((user -> name) != 0) {
    g_free((user -> name));
  }
  if ((user -> user_record) != 0) {
    nm_release_user_record((user -> user_record));
  }
  nm_conference_list_free(user);
  nm_destroy_contact_list(user);
  g_free(user);
}

NMERR_T nm_send_login(NMUser *user,const char *pwd,const char *my_addr,const char *user_agent,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  NMField *fields = (NMField *)((void *)0);
  if (((user == ((NMUser *)((void *)0))) || (pwd == ((const char *)((void *)0)))) || (user_agent == ((const char *)((void *)0)))) {
    return (0x2000L + 1);
  }
  fields = nm_field_add_pointer(fields,"NM_A_SZ_USERID",0,0,0,(g_strdup((user -> name))),10);
  fields = nm_field_add_pointer(fields,"NM_A_SZ_CREDENTIALS",0,0,0,(g_strdup(pwd)),10);
  fields = nm_field_add_pointer(fields,"NM_A_SZ_USER_AGENT",0,0,0,(g_strdup(user_agent)),10);
  fields = nm_field_add_number(fields,"NM_A_UD_BUILD",0,0,0,2,8);
  if (my_addr != 0) {
    fields = nm_field_add_pointer(fields,"nnmIPAddress",0,0,0,(g_strdup(my_addr)),10);
  }
/* Send the login */
  rc = nm_send_request((user -> conn),"login",fields,callback,data,0);
  nm_free_fields(&fields);
  return rc;
}

NMERR_T nm_send_set_status(NMUser *user,int status,const char *text,const char *auto_resp,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  NMField *fields = (NMField *)((void *)0);
  if (user == ((NMUser *)((void *)0))) 
    return (0x2000L + 1);
/* Add the status */
  fields = nm_field_add_pointer(fields,"NM_A_SZ_STATUS",0,0,0,(g_strdup_printf("%d",status)),10);
/* Add the status text and auto reply text if there is any */
  if (text != 0) {
    fields = nm_field_add_pointer(fields,"NM_A_SZ_STATUS_TEXT",0,0,0,(g_strdup(text)),10);
  }
  if (auto_resp != 0) {
    fields = nm_field_add_pointer(fields,"NM_A_SZ_MESSAGE_BODY",0,0,0,(g_strdup(auto_resp)),10);
  }
  rc = nm_send_request((user -> conn),"setstatus",fields,callback,data,0);
  nm_free_fields(&fields);
  return rc;
}

NMERR_T nm_send_multiple_get_details(NMUser *user,GSList *names,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  NMField *fields = (NMField *)((void *)0);
  GSList *node;
  if ((user == ((NMUser *)((void *)0))) || (names == ((GSList *)((void *)0)))) 
    return (0x2000L + 1);
/* Add in DN or display id */
  for (node = names; node != 0; node = (node -> next)) {
    fields = nm_field_add_pointer(fields,"NM_A_SZ_USERID",0,0,0,(g_strdup((node -> data))),10);
  }
  rc = nm_send_request((user -> conn),"getdetails",fields,callback,data,0);
  nm_free_fields(&fields);
  return rc;
}

NMERR_T nm_send_get_details(NMUser *user,const char *name,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  NMField *fields = (NMField *)((void *)0);
  if ((user == ((NMUser *)((void *)0))) || (name == ((const char *)((void *)0)))) 
    return (0x2000L + 1);
/* Add in DN or display id */
  if (strstr("=",name) != 0) {
    fields = nm_field_add_pointer(fields,"NM_A_SZ_DN",0,0,0,(g_strdup(name)),13);
  }
  else {
    const char *dn = nm_lookup_dn(user,name);
    if (dn != 0) {
      fields = nm_field_add_pointer(fields,"NM_A_SZ_DN",0,0,0,(g_strdup(name)),13);
    }
    else {
      fields = nm_field_add_pointer(fields,"NM_A_SZ_USERID",0,0,0,(g_strdup(name)),10);
    }
  }
  rc = nm_send_request((user -> conn),"getdetails",fields,callback,data,0);
  nm_free_fields(&fields);
  return rc;
}

NMERR_T nm_send_create_conference(NMUser *user,NMConference *conference,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  NMField *fields = (NMField *)((void *)0);
  NMField *tmp = (NMField *)((void *)0);
  NMField *field = (NMField *)((void *)0);
  NMRequest *req = (NMRequest *)((void *)0);
  int count;
  int i;
  if ((user == ((NMUser *)((void *)0))) || (conference == ((NMConference *)((void *)0)))) 
    return (0x2000L + 1);
/* Add in a blank guid */
  tmp = nm_field_add_pointer(tmp,"NM_A_SZ_OBJECT_ID",0,0,0,(g_strdup("[00000000-00000000-00000000-0000-0000]")),10);
  fields = nm_field_add_pointer(fields,"NM_A_FA_CONVERSATION",0,0,0,tmp,9);
  tmp = ((NMField *)((void *)0));
/* Add participants in */
  count = nm_conference_get_participant_count(conference);
  for (i = 0; i < count; i++) {
    NMUserRecord *user_record = nm_conference_get_participant(conference,i);
    if (user_record != 0) {
      fields = nm_field_add_pointer(fields,"NM_A_SZ_DN",0,0,0,(g_strdup(nm_user_record_get_dn(user_record))),13);
    }
  }
/* Add our user in */
  field = nm_locate_field("NM_A_SZ_DN",(user -> fields));
  if (field != 0) {
    fields = nm_field_add_pointer(fields,"NM_A_SZ_DN",0,0,0,(g_strdup(((char *)(field -> ptr_value)))),13);
  }
  rc = nm_send_request((user -> conn),"createconf",fields,callback,data,&req);
  if ((rc == 0L) && (req != 0)) {
    nm_conference_add_ref(conference);
    nm_request_set_data(req,conference);
  }
  if (req != 0) 
    nm_release_request(req);
  nm_free_fields(&fields);
  return rc;
}

NMERR_T nm_send_leave_conference(NMUser *user,NMConference *conference,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  NMField *fields = (NMField *)((void *)0);
  NMField *tmp = (NMField *)((void *)0);
  NMRequest *req = (NMRequest *)((void *)0);
  if ((user == ((NMUser *)((void *)0))) || (conference == ((NMConference *)((void *)0)))) 
    return (0x2000L + 1);
/* Add in the conference guid */
  tmp = nm_field_add_pointer(tmp,"NM_A_SZ_OBJECT_ID",0,0,0,(g_strdup(nm_conference_get_guid(conference))),10);
  fields = nm_field_add_pointer(fields,"NM_A_FA_CONVERSATION",0,0,0,tmp,9);
  tmp = ((NMField *)((void *)0));
/* Send the request to the server */
  rc = nm_send_request((user -> conn),"leaveconf",fields,callback,data,&req);
  if ((rc == 0L) && (req != 0)) 
    nm_request_set_data(req,conference);
  if (req != 0) 
    nm_release_request(req);
  nm_free_fields(&fields);
  return rc;
}

NMERR_T nm_send_join_conference(NMUser *user,NMConference *conference,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  NMField *fields = (NMField *)((void *)0);
  NMField *tmp = (NMField *)((void *)0);
  NMRequest *req = (NMRequest *)((void *)0);
  if ((user == ((NMUser *)((void *)0))) || (conference == ((NMConference *)((void *)0)))) 
    return (0x2000L + 1);
/* Add in the conference guid */
  tmp = nm_field_add_pointer(tmp,"NM_A_SZ_OBJECT_ID",0,0,0,(g_strdup(nm_conference_get_guid(conference))),10);
  fields = nm_field_add_pointer(fields,"NM_A_FA_CONVERSATION",0,0,0,tmp,9);
  tmp = ((NMField *)((void *)0));
/* Send the request to the server */
  rc = nm_send_request((user -> conn),"joinconf",fields,callback,data,&req);
  if ((rc == 0L) && (req != 0)) 
    nm_request_set_data(req,conference);
  if (req != 0) 
    nm_release_request(req);
  nm_free_fields(&fields);
  return rc;
}

NMERR_T nm_send_reject_conference(NMUser *user,NMConference *conference,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  NMField *fields = (NMField *)((void *)0);
  NMField *tmp = (NMField *)((void *)0);
  NMRequest *req = (NMRequest *)((void *)0);
  if ((user == ((NMUser *)((void *)0))) || (conference == ((NMConference *)((void *)0)))) 
    return (0x2000L + 1);
/* Add in the conference guid */
  tmp = nm_field_add_pointer(tmp,"NM_A_SZ_OBJECT_ID",0,0,0,(g_strdup(nm_conference_get_guid(conference))),10);
  fields = nm_field_add_pointer(fields,"NM_A_FA_CONVERSATION",0,0,0,tmp,9);
  tmp = ((NMField *)((void *)0));
/* Send the request to the server */
  rc = nm_send_request((user -> conn),"rejectconf",fields,callback,data,&req);
  if ((rc == 0L) && (req != 0)) 
    nm_request_set_data(req,conference);
  if (req != 0) 
    nm_release_request(req);
  nm_free_fields(&fields);
  return rc;
}

NMERR_T nm_send_conference_invite(NMUser *user,NMConference *conference,NMUserRecord *user_record,const char *message,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  NMField *fields = (NMField *)((void *)0);
  NMField *tmp = (NMField *)((void *)0);
  NMRequest *req = (NMRequest *)((void *)0);
  if (((user == ((NMUser *)((void *)0))) || (conference == ((NMConference *)((void *)0)))) || (user_record == ((NMUserRecord *)((void *)0)))) 
    return (0x2000L + 1);
/* Add in the conference guid */
  tmp = nm_field_add_pointer(tmp,"NM_A_SZ_OBJECT_ID",0,0,0,(g_strdup(nm_conference_get_guid(conference))),10);
  fields = nm_field_add_pointer(fields,"NM_A_FA_CONVERSATION",0,0,0,tmp,9);
  tmp = ((NMField *)((void *)0));
/* Add in DN of user to invite */
  fields = nm_field_add_pointer(fields,"NM_A_SZ_DN",0,0,0,(g_strdup(nm_user_record_get_dn(user_record))),13);
/* Add the invite message if there is one */
  if (message != 0) 
    fields = nm_field_add_pointer(fields,"NM_A_SZ_MESSAGE_BODY",0,0,0,(g_strdup(message)),10);
/* Send the request to the server */
  rc = nm_send_request((user -> conn),"sendinvite",fields,callback,data,&req);
  if ((rc == 0L) && (req != 0)) 
    nm_request_set_data(req,conference);
  if (req != 0) 
    nm_release_request(req);
  nm_free_fields(&fields);
  return rc;
}

NMERR_T nm_send_message(NMUser *user,NMMessage *message,nm_response_cb callback)
{
  NMERR_T rc = 0L;
  char *text;
  char *rtfized;
  NMField *fields = (NMField *)((void *)0);
  NMField *tmp = (NMField *)((void *)0);
  NMConference *conf;
  NMUserRecord *user_record;
  int count;
  int i;
  if ((user == ((NMUser *)((void *)0))) || (message == ((NMMessage *)((void *)0)))) {
    return (0x2000L + 1);
  }
  conf = nm_message_get_conference(message);
  if (!(nm_conference_is_instantiated(conf) != 0)) {
    rc = (0x2000L + 7);
  }
  else {
    tmp = nm_field_add_pointer(tmp,"NM_A_SZ_OBJECT_ID",0,0,0,(g_strdup(nm_conference_get_guid(conf))),10);
    fields = nm_field_add_pointer(fields,"NM_A_FA_CONVERSATION",0,0,0,tmp,9);
    tmp = ((NMField *)((void *)0));
/* Add RTF and plain text versions of the message */
    text = g_strdup(nm_message_get_text(message));
/* Truncate if necessary */
    if (strlen(text) > 2048) 
      text[2048] = 0;
    rtfized = nm_rtfize_text(text);
    purple_debug_info("novell","message text is: %s\n",text);
    purple_debug_info("novell","message rtf is: %s\n",rtfized);
    tmp = nm_field_add_pointer(tmp,"NM_A_SZ_MESSAGE_BODY",0,0,0,rtfized,10);
    tmp = nm_field_add_number(tmp,"NM_A_UD_MESSAGE_TYPE",0,0,0,0,8);
    tmp = nm_field_add_pointer(tmp,"NM_A_SZ_MESSAGE_TEXT",0,0,0,text,10);
    fields = nm_field_add_pointer(fields,"NM_A_FA_MESSAGE",0,0,0,tmp,9);
    tmp = ((NMField *)((void *)0));
/* Add participants */
    count = nm_conference_get_participant_count(conf);
    for (i = 0; i < count; i++) {
      user_record = nm_conference_get_participant(conf,i);
      if (user_record != 0) {
        fields = nm_field_add_pointer(fields,"NM_A_SZ_DN",0,0,0,(g_strdup(nm_user_record_get_dn(user_record))),13);
      }
    }
/* Send the request */
    rc = nm_send_request((user -> conn),"sendmessage",fields,callback,0,0);
  }
  nm_free_fields(&fields);
  return rc;
}

NMERR_T nm_send_typing(NMUser *user,NMConference *conf,gboolean typing,nm_response_cb callback)
{
  NMERR_T rc = 0L;
  char *str = (char *)((void *)0);
  NMField *fields = (NMField *)((void *)0);
  NMField *tmp = (NMField *)((void *)0);
  if ((user == ((NMUser *)((void *)0))) || (conf == ((NMConference *)((void *)0)))) {
    return (0x2000L + 1);
  }
  if (!(nm_conference_is_instantiated(conf) != 0)) {
    rc = (0x2000L + 7);
  }
  else {
/* Add the conference GUID */
    tmp = nm_field_add_pointer(tmp,"NM_A_SZ_OBJECT_ID",0,0,0,(g_strdup(nm_conference_get_guid(conf))),10);
/* Add typing type */
    str = g_strdup_printf("%d",((typing != 0)?112 : 113));
    tmp = nm_field_add_pointer(tmp,"NM_A_SZ_TYPE",0,0,0,str,10);
    fields = nm_field_add_pointer(fields,"NM_A_FA_CONVERSATION",0,0,0,tmp,9);
    tmp = ((NMField *)((void *)0));
    rc = nm_send_request((user -> conn),"sendtyping",fields,callback,0,0);
  }
  nm_free_fields(&fields);
  return rc;
}

NMERR_T nm_send_create_contact(NMUser *user,NMFolder *folder,NMContact *contact,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  NMField *fields = (NMField *)((void *)0);
  NMRequest *req = (NMRequest *)((void *)0);
  const char *name = (const char *)((void *)0);
  const char *display_name = (const char *)((void *)0);
  if (((user == ((NMUser *)((void *)0))) || (folder == ((NMFolder *)((void *)0)))) || (contact == ((NMContact *)((void *)0)))) {
    return (0x2000L + 1);
  }
/* Add parent ID */
  fields = nm_field_add_pointer(fields,"NM_A_SZ_PARENT_ID",0,0,0,(g_strdup_printf("%d",nm_folder_get_id(folder))),10);
/* Check to see if userid is current user and return an error? */
/* Check to see if contact already exists and return an error? */
/* Add userid or dn */
  name = nm_contact_get_dn(contact);
  if (name == ((const char *)((void *)0))) 
    return (0x2000L + 1);
  if (strstr("=",name) != 0) {
    fields = nm_field_add_pointer(fields,"NM_A_SZ_DN",0,0,0,(g_strdup(name)),13);
  }
  else {
    fields = nm_field_add_pointer(fields,"NM_A_SZ_USERID",0,0,0,(g_strdup(name)),10);
  }
/* Add display name */
  display_name = nm_contact_get_display_name(contact);
  if (display_name != 0) 
    fields = nm_field_add_pointer(fields,"NM_A_SZ_DISPLAY_NAME",0,0,0,(g_strdup(display_name)),10);
/* Dispatch the request */
  rc = nm_send_request((user -> conn),"createcontact",fields,callback,data,&req);
  if ((rc == 0L) && (req != 0)) 
    nm_request_set_data(req,contact);
  if (req != 0) 
    nm_release_request(req);
  nm_free_fields(&fields);
  return rc;
}

NMERR_T nm_send_remove_contact(NMUser *user,NMFolder *folder,NMContact *contact,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  NMField *fields = (NMField *)((void *)0);
  NMRequest *req = (NMRequest *)((void *)0);
  if (((user == ((NMUser *)((void *)0))) || (folder == ((NMFolder *)((void *)0)))) || (contact == ((NMContact *)((void *)0)))) {
    return (0x2000L + 1);
  }
/* Add parent id */
  fields = nm_field_add_pointer(fields,"NM_A_SZ_PARENT_ID",0,0,0,(g_strdup_printf("%d",nm_folder_get_id(folder))),10);
/* Add object id */
  fields = nm_field_add_pointer(fields,"NM_A_SZ_OBJECT_ID",0,0,0,(g_strdup_printf("%d",nm_contact_get_id(contact))),10);
/* Dispatch the request */
  rc = nm_send_request((user -> conn),"deletecontact",fields,callback,data,&req);
  if ((rc == 0L) && (req != 0)) 
    nm_request_set_data(req,contact);
  if (req != 0) 
    nm_release_request(req);
  nm_free_fields(&fields);
  return rc;
}

NMERR_T nm_send_create_folder(NMUser *user,const char *name,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  NMField *fields = (NMField *)((void *)0);
  NMRequest *req = (NMRequest *)((void *)0);
  if ((user == ((NMUser *)((void *)0))) || (name == ((const char *)((void *)0)))) {
    return (0x2000L + 1);
  }
/* Add parent ID */
  fields = nm_field_add_pointer(fields,"NM_A_SZ_PARENT_ID",0,0,0,(g_strdup("0")),10);
/* Add name of the folder to add */
  fields = nm_field_add_pointer(fields,"NM_A_SZ_DISPLAY_NAME",0,0,0,(g_strdup(name)),10);
/* Add sequence, for now just put it at the bottom */
  fields = nm_field_add_pointer(fields,"NM_A_SZ_SEQUENCE_NUMBER",0,0,0,(g_strdup("-1")),10);
/* Dispatch the request */
  rc = nm_send_request((user -> conn),"createfolder",fields,callback,data,&req);
  if ((rc == 0L) && (req != 0)) 
    nm_request_set_data(req,(g_strdup(name)));
  if (req != 0) 
    nm_release_request(req);
  nm_free_fields(&fields);
  return rc;
}

NMERR_T nm_send_remove_folder(NMUser *user,NMFolder *folder,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  NMField *fields = (NMField *)((void *)0);
  NMRequest *req = (NMRequest *)((void *)0);
  if ((user == ((NMUser *)((void *)0))) || (folder == ((NMFolder *)((void *)0)))) {
    return (0x2000L + 1);
  }
/* Add the object id */
  fields = nm_field_add_pointer(fields,"NM_A_SZ_OBJECT_ID",0,0,0,(g_strdup_printf("%d",nm_folder_get_id(folder))),10);
/* Dispatch the request */
  rc = nm_send_request((user -> conn),"deletecontact",fields,callback,data,&req);
  if ((rc == 0L) && (req != 0)) 
    nm_request_set_data(req,folder);
  if (req != 0) 
    nm_release_request(req);
  nm_free_fields(&fields);
  return rc;
}

NMERR_T nm_send_get_status(NMUser *user,NMUserRecord *user_record,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  NMField *fields = (NMField *)((void *)0);
  NMRequest *req = (NMRequest *)((void *)0);
  const char *dn;
  if ((user == ((NMUser *)((void *)0))) || (user_record == ((NMUserRecord *)((void *)0)))) 
    return (0x2000L + 1);
/* Add DN to field list */
  dn = nm_user_record_get_dn(user_record);
  if (dn == ((const char *)((void *)0))) 
    return (NMERR_T )(-1);
  fields = nm_field_add_pointer(fields,"NM_A_SZ_DN",0,0,0,(g_strdup(dn)),10);
/* Dispatch the request */
  rc = nm_send_request((user -> conn),"getstatus",fields,callback,data,&req);
  if ((rc == 0L) && (req != 0)) 
    nm_request_set_data(req,user_record);
  if (req != 0) 
    nm_release_request(req);
  nm_free_fields(&fields);
  return rc;
}

NMERR_T nm_send_rename_contact(NMUser *user,NMContact *contact,const char *new_name,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  NMField *field = (NMField *)((void *)0);
  NMField *fields = (NMField *)((void *)0);
  NMField *list = (NMField *)((void *)0);
  NMRequest *req = (NMRequest *)((void *)0);
  if (((user == ((NMUser *)((void *)0))) || (contact == ((NMContact *)((void *)0)))) || (new_name == ((const char *)((void *)0)))) 
    return (0x2000L + 1);
/* Create field list for current contact */
  field = nm_contact_to_fields(contact);
  if (field != 0) {
    fields = nm_field_add_pointer(fields,"NM_A_FA_CONTACT",0,2,0,field,9);
    field = ((NMField *)((void *)0));
/* Update the contacts display name locally */
    nm_contact_set_display_name(contact,new_name);
/* Create field list for updated contact */
    field = nm_contact_to_fields(contact);
    if (field != 0) {
      fields = nm_field_add_pointer(fields,"NM_A_FA_CONTACT",0,5,0,field,9);
      field = ((NMField *)((void *)0));
/* Package it up */
      list = nm_field_add_pointer(list,"NM_A_FA_CONTACT_LIST",0,0,0,fields,9);
      fields = ((NMField *)((void *)0));
      rc = nm_send_request((user -> conn),"updateitem",list,callback,data,&req);
      if ((rc == 0L) && (req != 0)) 
        nm_request_set_data(req,contact);
    }
  }
  if (req != 0) 
    nm_release_request(req);
  if (list != 0) 
    nm_free_fields(&list);
  return rc;
}

NMERR_T nm_send_rename_folder(NMUser *user,NMFolder *folder,const char *new_name,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  NMField *field = (NMField *)((void *)0);
  NMField *fields = (NMField *)((void *)0);
  NMField *list = (NMField *)((void *)0);
  NMRequest *req = (NMRequest *)((void *)0);
  if (((user == ((NMUser *)((void *)0))) || (folder == ((NMFolder *)((void *)0)))) || (new_name == ((const char *)((void *)0)))) 
    return (0x2000L + 1);
/* Make sure folder does not already exist!? */
  if (nm_find_folder(user,new_name) != 0) 
    return (0x2000L + 8);
/* Create field list for current folder */
  field = nm_folder_to_fields(folder);
  if (field != 0) {
    fields = nm_field_add_pointer(fields,"NM_A_FA_FOLDER",0,2,0,field,9);
    field = ((NMField *)((void *)0));
/* Update the folders display name locally */
    nm_folder_set_name(folder,new_name);
/* Create field list for updated folder */
    field = nm_folder_to_fields(folder);
    if (field != 0) {
      fields = nm_field_add_pointer(fields,"NM_A_FA_FOLDER",0,5,0,field,9);
      field = ((NMField *)((void *)0));
/* Package it up */
      list = nm_field_add_pointer(list,"NM_A_FA_CONTACT_LIST",0,0,0,fields,9);
      fields = ((NMField *)((void *)0));
      rc = nm_send_request((user -> conn),"updateitem",list,callback,data,&req);
      if ((rc == 0L) && (req != 0)) 
        nm_request_set_data(req,folder);
    }
  }
  if (req != 0) 
    nm_release_request(req);
  if (list != 0) 
    nm_free_fields(&list);
  return rc;
}

NMERR_T nm_send_move_contact(NMUser *user,NMContact *contact,NMFolder *folder,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  NMField *field = (NMField *)((void *)0);
  NMField *fields = (NMField *)((void *)0);
  NMField *list = (NMField *)((void *)0);
  NMRequest *req = (NMRequest *)((void *)0);
  if (((user == ((NMUser *)((void *)0))) || (contact == ((NMContact *)((void *)0)))) || (folder == ((NMFolder *)((void *)0)))) 
    return (0x2000L + 1);
/* Create field list for the contact */
  field = nm_contact_to_fields(contact);
  if (field != 0) {
    fields = nm_field_add_pointer(fields,"NM_A_FA_CONTACT",0,2,0,field,9);
    field = ((NMField *)((void *)0));
/* Wrap the contact up and add it to the request field list */
    list = nm_field_add_pointer(list,"NM_A_FA_CONTACT_LIST",0,0,0,fields,9);
    fields = ((NMField *)((void *)0));
/* Add sequence number */
    list = nm_field_add_pointer(list,"NM_A_SZ_SEQUENCE_NUMBER",0,0,0,(g_strdup("-1")),10);
/* Add parent ID */
    list = nm_field_add_pointer(list,"NM_A_SZ_PARENT_ID",0,0,0,(g_strdup_printf("%d",nm_folder_get_id(folder))),10);
/* Dispatch the request */
    rc = nm_send_request((user -> conn),"movecontact",list,callback,data,&req);
    if ((rc == 0L) && (req != 0)) 
      nm_request_set_data(req,contact);
  }
  if (req != 0) 
    nm_release_request(req);
  if (list != 0) 
    nm_free_fields(&list);
  return rc;
}

NMERR_T nm_send_create_privacy_item(NMUser *user,const char *who,gboolean allow_list,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  NMField *fields = (NMField *)((void *)0);
  const char *tag;
  if ((user == ((NMUser *)((void *)0))) || (who == ((const char *)((void *)0)))) 
    return (0x2000L + 1);
  if (allow_list != 0) 
    tag = "NM_A_SZ_BLOCKING_ALLOW_ITEM";
  else 
    tag = "NM_A_SZ_BLOCKING_DENY_ITEM";
  fields = nm_field_add_pointer(fields,tag,0,5,0,(g_strdup(who)),10);
  rc = nm_send_request((user -> conn),"createblock",fields,callback,data,0);
  nm_free_fields(&fields);
  return rc;
}

NMERR_T nm_send_remove_privacy_item(NMUser *user,const char *dn,gboolean allow_list,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  NMField *fields = (NMField *)((void *)0);
  const char *tag;
  GSList **list_ptr;
  GSList *node;
  if ((user == ((NMUser *)((void *)0))) || (dn == ((const char *)((void *)0)))) 
    return (0x2000L + 1);
  if (allow_list != 0) {
    tag = "nnmBlockingAllowList";
    list_ptr = &user -> allow_list;
  }
  else {
    tag = "nnmBlockingDenyList";
    list_ptr = &user -> deny_list;
  }
/* Remove item from the cached list */
  if ((node = g_slist_find_custom( *list_ptr,dn,((GCompareFunc )purple_utf8_strcasecmp))) != 0) {
     *list_ptr = g_slist_remove_link( *list_ptr,node);
    g_slist_free_1(node);
  }
  fields = nm_field_add_pointer(fields,tag,0,2,0,(g_strdup(dn)),13);
  rc = nm_send_request((user -> conn),"updateblocks",fields,callback,data,0);
  nm_free_fields(&fields);
  return rc;
}

NMERR_T nm_send_set_privacy_default(NMUser *user,gboolean default_deny,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  NMField *fields = (NMField *)((void *)0);
  if (user == ((NMUser *)((void *)0))) 
    return (0x2000L + 1);
  fields = nm_field_add_pointer(fields,"nnmBlocking",0,6,0,(((default_deny != 0)?g_strdup("1") : g_strdup("0"))),10);
  rc = nm_send_request((user -> conn),"updateblocks",fields,callback,data,0);
  nm_free_fields(&fields);
  return rc;
}

NMERR_T nm_send_keepalive(NMUser *user,nm_response_cb callback,gpointer data)
{
  NMERR_T rc = 0L;
  if (user == ((NMUser *)((void *)0))) 
    return (0x2000L + 1);
  rc = nm_send_request((user -> conn),"ping",0,callback,data,0);
  return rc;
}

NMERR_T nm_process_new_data(NMUser *user)
{
  NMConn *conn;
  NMERR_T rc = 0L;
  guint32 val;
  if (user == ((NMUser *)((void *)0))) 
    return (0x2000L + 1);
  conn = (user -> conn);
/* Check to see if this is an event or a response */
  rc = nm_read_all(conn,((char *)(&val)),(sizeof(val)));
  if (rc == 0L) {
    if (strncmp(((char *)(&val)),"HTTP",strlen("HTTP")) == 0) 
      rc = nm_process_response(user);
    else 
      rc = nm_process_event(user,((guint32 )val));
  }
  else {
    if ( *__errno_location() == 11) 
      rc = 0L;
    else 
      rc = (0x2000L + 4);
  }
  return rc;
}

NMConference *nm_find_conversation(NMUser *user,const char *who)
{
  NMConference *conference = (NMConference *)((void *)0);
  NMConference *tmp;
  GSList *cnode;
  if ((user != 0) && ((user -> conferences) != 0)) {{
      for (cnode = (user -> conferences); cnode != 0; cnode = (cnode -> next)) {
        tmp = (cnode -> data);
        if (nm_conference_get_participant_count(tmp) == 1) {
          NMUserRecord *ur = nm_conference_get_participant(tmp,0);
          if (ur != 0) {
            if (nm_utf8_str_equal((nm_user_record_get_dn(ur)),who) != 0) {
              conference = tmp;
              break; 
            }
          }
        }
      }
    }
  }
  return conference;
}

void nm_conference_list_add(NMUser *user,NMConference *conf)
{
  if ((user == ((NMUser *)((void *)0))) || (conf == ((NMConference *)((void *)0)))) 
    return ;
  nm_conference_add_ref(conf);
  user -> conferences = g_slist_append((user -> conferences),conf);
}

void nm_conference_list_remove(NMUser *user,NMConference *conf)
{
  if ((user == ((NMUser *)((void *)0))) || (conf == ((NMConference *)((void *)0)))) 
    return ;
  if (g_slist_find((user -> conferences),conf) != 0) {
    user -> conferences = g_slist_remove((user -> conferences),conf);
    nm_release_conference(conf);
  }
}

void nm_conference_list_free(NMUser *user)
{
  GSList *cnode;
  NMConference *conference;
  if (user == ((NMUser *)((void *)0))) 
    return ;
  if ((user -> conferences) != 0) {
    for (cnode = (user -> conferences); cnode != 0; cnode = (cnode -> next)) {
      conference = (cnode -> data);
      cnode -> data = ((gpointer )((void *)0));
      nm_release_conference(conference);
    }
    g_slist_free((user -> conferences));
    user -> conferences = ((GSList *)((void *)0));
  }
}

NMConference *nm_conference_list_find(NMUser *user,const char *guid)
{
  GSList *cnode;
  NMConference *conference = (NMConference *)((void *)0);
  NMConference *tmp;
  if ((user == ((NMUser *)((void *)0))) || (guid == ((const char *)((void *)0)))) 
    return 0;
  if ((user -> conferences) != 0) {{
      for (cnode = (user -> conferences); cnode != 0; cnode = (cnode -> next)) {
        tmp = (cnode -> data);
        if (nm_are_guids_equal(nm_conference_get_guid(tmp),guid) != 0) {
          conference = tmp;
          break; 
        }
      }
    }
  }
  return conference;
}

gboolean nm_are_guids_equal(const char *guid1,const char *guid2)
{
  if ((guid1 == ((const char *)((void *)0))) || (guid2 == ((const char *)((void *)0)))) 
    return 0;
  return strncmp(guid1,guid2,27) == 0;
}

void nm_user_add_contact(NMUser *user,NMContact *contact)
{
  if ((user == ((NMUser *)((void *)0))) || (contact == ((NMContact *)((void *)0)))) 
    return ;
  nm_contact_add_ref(contact);
  g_hash_table_insert((user -> contacts),(g_utf8_strdown(nm_contact_get_dn(contact),(-1))),contact);
}

void nm_user_add_user_record(NMUser *user,NMUserRecord *user_record)
{
  const char *display_id;
  const char *dn;
  if (!(user != 0) || !(user_record != 0)) 
    return ;
  display_id = nm_user_record_get_display_id(user_record);
  dn = nm_user_record_get_dn(user_record);
  if (!(dn != 0) || !(display_id != 0)) 
    return ;
  nm_user_record_add_ref(user_record);
  g_hash_table_insert((user -> user_records),(g_utf8_strdown(dn,(-1))),user_record);
  g_hash_table_insert((user -> display_id_to_dn),(g_utf8_strdown(display_id,(-1))),(g_utf8_strdown(dn,(-1))));
}

nm_event_cb nm_user_get_event_callback(NMUser *user)
{
  if (user == ((NMUser *)((void *)0))) 
    return 0;
  return user -> evt_callback;
}

NMConn *nm_user_get_conn(NMUser *user)
{
  if (user == ((NMUser *)((void *)0))) 
    return 0;
  return user -> conn;
}

NMERR_T nm_create_contact_list(NMUser *user)
{
  NMERR_T rc = 0L;
  NMField *locate = (NMField *)((void *)0);
  if ((user == ((NMUser *)((void *)0))) || ((user -> fields) == ((NMField *)((void *)0)))) {
    return (0x2000L + 1);
  }
/* Create the root folder */
  user -> root_folder = nm_create_folder("");
/* Find the contact list in the login fields */
  locate = nm_locate_field("NM_A_FA_CONTACT_LIST",(user -> fields));
  if (locate != ((NMField *)((void *)0))) {
/* Add the folders and then the contacts */
    nm_folder_add_contacts_and_folders(user,(user -> root_folder),((NMField *)(locate -> ptr_value)));
  }
  return rc;
}

gboolean nm_user_is_privacy_locked(NMUser *user)
{
  if (user != 0) {
    return user -> privacy_locked;
  }
  return 0;
}

static gboolean _create_privacy_list(NMUser *user,NMRequest *request)
{
  NMField *locate = (NMField *)((void *)0);
  GSList *need_details = (GSList *)((void *)0);
/* Are the privacy settings locked */
  locate = nm_locate_field("nnmLockedAttrList",(user -> fields));
  if ((locate != 0) && ((locate -> ptr_value) != 0)) {
    if (((locate -> type) == 10) && (purple_utf8_strcasecmp((locate -> ptr_value),"nnmBlocking") == 0)) {
      user -> privacy_locked = (!0);
    }
    else if (((locate -> type) == 12) || ((locate -> type) == 9)) {
      NMField *tmp = (NMField *)(locate -> ptr_value);
{
        while((tmp != 0) && ((tmp -> tag) != 0)){
          if (purple_utf8_strcasecmp((tmp -> ptr_value),"nnmBlocking") == 0) {
            user -> privacy_locked = (!0);
            break; 
          }
          tmp++;
        }
      }
    }
  }
/* Set default deny flag */
  locate = nm_locate_field("nnmBlocking",(user -> fields));
  if ((locate != 0) && ((locate -> ptr_value) != 0)) {
    user -> default_deny = atoi(((char *)(locate -> ptr_value)));
  }
/* Read internal blocking allow list */
  locate = nm_locate_field("nnmBlockingAllowList",(user -> fields));
  if ((locate != 0) && ((locate -> ptr_value) != 0)) {
    if ((locate -> type) == 12) {
      locate = ((NMField *)(locate -> ptr_value));
      for (; (locate -> tag) != ((char *)((void *)0)); locate++) {
        if ((locate -> ptr_value) != 0) {
          user -> allow_list = g_slist_append((user -> allow_list),((char *)(locate -> ptr_value)));
          if (nm_find_user_record(user,((char *)(locate -> ptr_value))) == ((NMUserRecord *)((void *)0))) 
            need_details = g_slist_append(need_details,((char *)(locate -> ptr_value)));
        }
      }
    }
    else {
      user -> allow_list = g_slist_append((user -> allow_list),((char *)(locate -> ptr_value)));
      if (nm_find_user_record(user,((char *)(locate -> ptr_value))) == ((NMUserRecord *)((void *)0))) 
        need_details = g_slist_append(need_details,((char *)(locate -> ptr_value)));
    }
  }
/* Read internal blocking deny list */
  locate = nm_locate_field("nnmBlockingDenyList",(user -> fields));
  if ((locate != 0) && ((locate -> ptr_value) != 0)) {
    if ((locate -> type) == 12) {
      locate = ((NMField *)(locate -> ptr_value));
      for (; (locate -> tag) != ((char *)((void *)0)); locate++) {
        if ((locate -> ptr_value) != 0) {
          user -> deny_list = g_slist_append((user -> deny_list),((char *)(locate -> ptr_value)));
          if (nm_find_user_record(user,((char *)(locate -> ptr_value))) == ((NMUserRecord *)((void *)0))) 
            need_details = g_slist_append(need_details,((char *)(locate -> ptr_value)));
        }
      }
    }
    else {
      user -> deny_list = g_slist_append((user -> deny_list),((char *)(locate -> ptr_value)));
      if (nm_find_user_record(user,((char *)(locate -> ptr_value))) == ((NMUserRecord *)((void *)0))) 
        need_details = g_slist_append(need_details,((char *)(locate -> ptr_value)));
    }
  }
  if (need_details != 0) {
    nm_request_add_ref(request);
    nm_send_multiple_get_details(user,need_details,_handle_multiple_get_details_login_cb,request);
    return 0;
  }
  return (!0);
}

void nm_destroy_contact_list(NMUser *user)
{
  if (user == ((NMUser *)((void *)0))) 
    return ;
  if ((user -> root_folder) != 0) {
    nm_release_folder((user -> root_folder));
    user -> root_folder = ((NMFolder *)((void *)0));
  }
}

NMFolder *nm_get_root_folder(NMUser *user)
{
  if (user == ((NMUser *)((void *)0))) 
    return 0;
  if ((user -> root_folder) == ((NMFolder *)((void *)0))) 
    nm_create_contact_list(user);
  return user -> root_folder;
}

NMContact *nm_find_contact(NMUser *user,const char *name)
{
  char *str;
  const char *dn = (const char *)((void *)0);
  NMContact *contact = (NMContact *)((void *)0);
  if ((user == ((NMUser *)((void *)0))) || (name == ((const char *)((void *)0)))) 
    return 0;
  str = g_utf8_strdown(name,(-1));
  if (strstr(str,"=") != 0) {
    dn = str;
  }
  else {
/* Assume that we have a display id instead of a dn */
    dn = ((const char *)(g_hash_table_lookup((user -> display_id_to_dn),str)));
  }
/* Find contact object in reference table */
  if (dn != 0) {
    contact = ((NMContact *)(g_hash_table_lookup((user -> contacts),dn)));
  }
  g_free(str);
  return contact;
}

GList *nm_find_contacts(NMUser *user,const char *dn)
{
  guint32 i;
  guint32 cnt;
  NMFolder *folder;
  NMContact *contact;
  GList *contacts = (GList *)((void *)0);
  if ((user == ((NMUser *)((void *)0))) || (dn == ((const char *)((void *)0)))) 
    return 0;
/* Check for contact at the root */
  contact = nm_folder_find_contact((user -> root_folder),dn);
  if (contact != 0) {
    contacts = g_list_append(contacts,contact);
    contact = ((NMContact *)((void *)0));
  }
/* Check for contact in each subfolder */
  cnt = (nm_folder_get_subfolder_count((user -> root_folder)));
  for (i = 0; i < cnt; i++) {
    folder = nm_folder_get_subfolder((user -> root_folder),i);
    contact = nm_folder_find_contact(folder,dn);
    if (contact != 0) {
      contacts = g_list_append(contacts,contact);
      contact = ((NMContact *)((void *)0));
    }
  }
  return contacts;
}

NMUserRecord *nm_find_user_record(NMUser *user,const char *name)
{
  char *str = (char *)((void *)0);
  const char *dn = (const char *)((void *)0);
  NMUserRecord *user_record = (NMUserRecord *)((void *)0);
  if ((user == ((NMUser *)((void *)0))) || (name == ((const char *)((void *)0)))) 
    return 0;
  str = g_utf8_strdown(name,(-1));
  if (strstr(str,"=") != 0) {
    dn = str;
  }
  else {
/* Assume that we have a display id instead of a dn */
    dn = ((const char *)(g_hash_table_lookup((user -> display_id_to_dn),str)));
  }
/* Find user record in reference table */
  if (dn != 0) {
    user_record = ((NMUserRecord *)(g_hash_table_lookup((user -> user_records),dn)));
  }
  g_free(str);
  return user_record;
}

const char *nm_lookup_dn(NMUser *user,const char *display_id)
{
  const char *dn;
  char *lower;
  if ((user == ((NMUser *)((void *)0))) || (display_id == ((const char *)((void *)0)))) 
    return 0;
  lower = g_utf8_strdown(display_id,(-1));
  dn = (g_hash_table_lookup((user -> display_id_to_dn),lower));
  g_free(lower);
  return dn;
}

NMFolder *nm_find_folder(NMUser *user,const char *name)
{
  NMFolder *folder = (NMFolder *)((void *)0);
  NMFolder *temp;
  int i;
  int num_folders;
  const char *tname = (const char *)((void *)0);
  if ((user == ((NMUser *)((void *)0))) || (name == ((const char *)((void *)0)))) 
    return 0;
  if (( *name) == 0) 
    return user -> root_folder;
  num_folders = nm_folder_get_subfolder_count((user -> root_folder));
{
    for (i = 0; i < num_folders; i++) {
      temp = nm_folder_get_subfolder((user -> root_folder),i);
      tname = nm_folder_get_name(temp);
      if ((tname != 0) && (strcmp(tname,name) == 0)) {
        folder = temp;
        break; 
      }
    }
  }
  return folder;
}

NMFolder *nm_find_folder_by_id(NMUser *user,int object_id)
{
  NMFolder *folder = (NMFolder *)((void *)0);
  NMFolder *temp;
  int i;
  int num_folders;
  if (user == ((NMUser *)((void *)0))) 
    return 0;
  if (object_id == 0) 
    return user -> root_folder;
  num_folders = nm_folder_get_subfolder_count((user -> root_folder));
{
    for (i = 0; i < num_folders; i++) {
      temp = nm_folder_get_subfolder((user -> root_folder),i);
      if (nm_folder_get_id(temp) == object_id) {
        folder = temp;
        break; 
      }
    }
  }
  return folder;
}

static void _handle_multiple_get_details_login_cb(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  nm_response_cb cb;
  NMRequest *request = user_data;
  if ((user == ((NMUser *)((void *)0))) || (request == ((NMRequest *)((void *)0)))) 
    return ;
  if ((cb = nm_request_get_callback(request)) != 0) {
    ( *cb)(user,ret_code,nm_request_get_data(request),nm_request_get_user_define(request));
    nm_release_request(request);
  }
}

static void _handle_multiple_get_details_joinconf_cb(NMUser *user,NMERR_T ret_code,gpointer resp_data,gpointer user_data)
{
  NMRequest *request = user_data;
  NMUserRecord *user_record = resp_data;
  NMConference *conference;
  GSList *list;
  GSList *node;
  if (((user == ((NMUser *)((void *)0))) || (resp_data == ((void *)((void *)0)))) || (user_data == ((void *)((void *)0)))) 
    return ;
  conference = (nm_request_get_data(request));
  list = (nm_request_get_user_define(request));
  if (((ret_code == 0) && (conference != 0)) && (list != 0)) {
/* Add the user to the conference */
    nm_conference_add_participant(conference,user_record);
{
/* Find the user in the list and remove it */
      for (node = list; node != 0; node = (node -> next)) {
        if (nm_utf8_str_equal((nm_user_record_get_dn(user_record)),((const char *)(node -> data))) != 0) {
          g_free((node -> data));
          list = g_slist_remove(list,(node -> data));
          nm_request_set_user_define(request,list);
          break; 
        }
      }
    }
/* Time to callback? */
    if (list == ((GSList *)((void *)0))) {
      nm_response_cb cb = nm_request_get_callback(request);
      if (cb != 0) {
        ( *cb)(user,0,conference,conference);
      }
      nm_release_request(request);
    }
  }
}

static NMERR_T nm_call_handler(NMUser *user,NMRequest *request,NMField *fields)
{
  NMERR_T rc = 0L;
  NMERR_T ret_code = 0L;
  NMConference *conf = (NMConference *)((void *)0);
  NMUserRecord *user_record = (NMUserRecord *)((void *)0);
  NMField *locate = (NMField *)((void *)0);
  NMField *field = (NMField *)((void *)0);
  const char *cmd;
  nm_response_cb cb;
  gboolean done = (!0);
  if (((user == ((NMUser *)((void *)0))) || (request == ((NMRequest *)((void *)0)))) || (fields == ((NMField *)((void *)0)))) 
    return (0x2000L + 1);
/* Get the return code */
  field = nm_locate_field("NM_A_SZ_RESULT_CODE",fields);
  if (field != 0) {
    ret_code = (atoi(((char *)(field -> ptr_value))));
  }
  else {
    ret_code = (0x2000L + 4);
  }
  cmd = nm_request_get_cmd(request);
  if ((ret_code == 0L) && (cmd != ((const char *)((void *)0)))) {
    if (strcmp("login",cmd) == 0) {
      user -> user_record = nm_create_user_record_from_fields(fields);
/* Save the users fields */
      user -> fields = nm_copy_field_array(fields);
      nm_create_contact_list(user);
      done = _create_privacy_list(user,request);
    }
    else if (strcmp("setstatus",cmd) == 0) {
/* Nothing to do */
    }
    else if (strcmp("createconf",cmd) == 0) {
      conf = ((NMConference *)(nm_request_get_data(request)));
/* get the convo guid */
      locate = nm_locate_field("NM_A_FA_CONVERSATION",fields);
      if (locate != 0) {
        field = nm_locate_field("NM_A_SZ_OBJECT_ID",((NMField *)(fields -> ptr_value)));
        if (field != 0) {
          nm_conference_set_guid(conf,((char *)(field -> ptr_value)));
        }
      }
      nm_conference_list_add(user,conf);
      nm_release_conference(conf);
    }
    else if (strcmp("leaveconf",cmd) == 0) {
      conf = ((NMConference *)(nm_request_get_data(request)));
      nm_conference_list_remove(user,conf);
    }
    else if (strcmp("joinconf",cmd) == 0) {
      GSList *list = (GSList *)((void *)0);
      GSList *node;
      conf = (nm_request_get_data(request));
      locate = nm_locate_field("NM_A_FA_CONTACT_LIST",fields);
      if ((locate != 0) && ((locate -> ptr_value) != 0)) {
        field = ((NMField *)(locate -> ptr_value));
        while((field = nm_locate_field("NM_A_SZ_DN",field)) != 0){
          if ((field != 0) && ((field -> ptr_value) != 0)) {
            if (nm_utf8_str_equal((nm_user_record_get_dn((user -> user_record))),((const char *)(field -> ptr_value))) != 0) {
              field++;
              continue; 
            }
            user_record = nm_find_user_record(user,((const char *)(field -> ptr_value)));
            if (user_record == ((NMUserRecord *)((void *)0))) {
              list = g_slist_append(list,(g_strdup(((char *)(field -> ptr_value)))));
            }
            else {
              nm_conference_add_participant(conf,user_record);
            }
          }
          field++;
        }
        if (list != ((GSList *)((void *)0))) {
          done = 0;
          nm_request_set_user_define(request,list);
          nm_request_add_ref(request);
          for (node = list; node != 0; node = (node -> next)) {
            nm_send_get_details(user,((const char *)(node -> data)),_handle_multiple_get_details_joinconf_cb,request);
          }
        }
      }
    }
    else if (strcmp("getdetails",cmd) == 0) {
      locate = nm_locate_field("NM_A_FA_RESULTS",fields);
      while((locate != 0) && ((locate -> ptr_value) != 0)){
        user_record = nm_create_user_record_from_fields(locate);
        if (user_record != 0) {
          NMUserRecord *tmp;
          tmp = nm_find_user_record(user,nm_user_record_get_dn(user_record));
          if (tmp != 0) {
/* Update the existing user record */
            nm_user_record_copy(tmp,user_record);
            nm_release_user_record(user_record);
            user_record = tmp;
          }
          else {
            nm_user_add_user_record(user,user_record);
            nm_release_user_record(user_record);
          }
/* Response data is new user record */
          nm_request_set_data(request,((gpointer )user_record));
        }
        locate = nm_locate_field("NM_A_FA_RESULTS",(locate + 1));
      }
    }
    else if (strcmp("createfolder",cmd) == 0) {
      _update_contact_list(user,fields);
    }
    else if (strcmp("createcontact",cmd) == 0) {
      _update_contact_list(user,fields);
      locate = nm_locate_field("NM_A_SZ_OBJECT_ID",((NMField *)(fields -> ptr_value)));
      if (locate != 0) {
        NMContact *new_contact = (nm_folder_find_item_by_object_id((user -> root_folder),atoi(((char *)(locate -> ptr_value)))));
        if (new_contact != 0) {
/* Add the contact to our cache */
          nm_user_add_contact(user,new_contact);
/* Set the contact as the response data */
          nm_request_set_data(request,((gpointer )new_contact));
        }
      }
    }
    else if (strcmp("deletecontact",cmd) == 0) {
      _update_contact_list(user,fields);
    }
    else if (strcmp("movecontact",cmd) == 0) {
      _update_contact_list(user,fields);
    }
    else if (strcmp("getstatus",cmd) == 0) {
      locate = nm_locate_field("NM_A_SZ_STATUS",fields);
      if (locate != 0) {
        nm_user_record_set_status(((NMUserRecord *)(nm_request_get_data(request))),atoi(((char *)(locate -> ptr_value))),0);
      }
    }
    else if (strcmp("updateitem",cmd) == 0) {
/* Nothing extra to do here */
    }
    else if (strcmp("createblock",cmd) == 0) {
      if ((locate = nm_locate_field("nnmBlockingDenyList",fields)) != 0) {
        if ((locate -> ptr_value) != 0) {
          user -> deny_list = g_slist_append((user -> deny_list),(g_strdup(((char *)(locate -> ptr_value)))));
        }
      }
      else if ((locate = nm_locate_field("nnmBlockingAllowList",fields)) != 0) {
        if ((locate -> ptr_value) != 0) {
          user -> allow_list = g_slist_append((user -> allow_list),(g_strdup(((char *)(locate -> ptr_value)))));
        }
      }
    }
    else if (strcmp("updateblocks",cmd) == 0) {
/* nothing to do here */
    }
    else {
/* Nothing to do, just print debug message  */
      purple_debug(PURPLE_DEBUG_INFO,"novell","nm_call_handler(): Unknown request command, %s\n",cmd);
    }
  }
  if ((done != 0) && ((cb = nm_request_get_callback(request)) != 0)) {
    ( *cb)(user,ret_code,nm_request_get_data(request),nm_request_get_user_define(request));
  }
  return rc;
}

static NMERR_T nm_process_response(NMUser *user)
{
  NMERR_T rc = 0L;
  NMField *fields = (NMField *)((void *)0);
  NMField *field = (NMField *)((void *)0);
  NMConn *conn = (user -> conn);
  NMRequest *req = (NMRequest *)((void *)0);
  rc = nm_read_header(conn);
  if (rc == 0L) {
    rc = nm_read_fields(conn,-1,&fields);
  }
  if (rc == 0L) {
    field = nm_locate_field("NM_A_SZ_TRANSACTION_ID",fields);
    if ((field != ((NMField *)((void *)0))) && ((field -> ptr_value) != 0)) {
      req = nm_conn_find_request(conn,atoi(((char *)(field -> ptr_value))));
      if (req != ((NMRequest *)((void *)0))) {
        rc = nm_call_handler(user,req,fields);
        nm_conn_remove_request_item(conn,req);
      }
    }
  }
  if (fields != 0) 
    nm_free_fields(&fields);
  return rc;
}
/*
 * Some utility functions...haven't figured out where
 * they belong yet.
 */

gboolean nm_utf8_str_equal(gconstpointer str1,gconstpointer str2)
{
  return purple_utf8_strcasecmp(str1,str2) == 0;
}

char *nm_typed_to_dotted(const char *typed)
{
  unsigned int i = 0;
  unsigned int j = 0;
  char *dotted;
  if (typed == ((const char *)((void *)0))) 
    return 0;
  dotted = ((char *)(g_malloc0_n(strlen(typed),(sizeof(char )))));
{
    do {
/* replace comma with a dot */
      if (j != 0) {
        dotted[j] = '.';
        j++;
      }
/* skip the type */
      while((typed[i] != 0) && (typed[i] != '='))
        i++;
/* verify that we aren't running off the end */
      if (typed[i] == 0) {
        dotted[j] = 0;
        break; 
      }
      i++;
/* copy the object name to context */
      while((typed[i] != 0) && (typed[i] != ',')){
        dotted[j] = typed[i];
        j++;
        i++;
      }
    }while (typed[i] != 0);
  }
  return dotted;
}

const char *nm_error_to_string(NMERR_T err)
{
  static char *unknown_msg = (char *)((void *)0);
  g_free(unknown_msg);
  unknown_msg = ((char *)((void *)0));
  switch(err){
    case 0x2000L + 1:
{
      return (const char *)(dgettext("pidgin","Required parameters not passed in"));
    }
    case 0x2000L + 2:
{
      return (const char *)(dgettext("pidgin","Unable to write to network"));
    }
    case 0x2000L + 3:
{
      return (const char *)(dgettext("pidgin","Unable to read from network"));
    }
    case 0x2000L + 4:
{
      return (const char *)(dgettext("pidgin","Error communicating with server"));
    }
    case 0x2000L + 6:
{
    }
    case 0xD100L + 0x002B:
{
      return (const char *)(dgettext("pidgin","Conference not found"));
    }
    case 0x2000L + 7:
{
      return (const char *)(dgettext("pidgin","Conference does not exist"));
    }
    case 0x2000L + 8:
{
    }
    case 0xD100L + 0x0026:
{
      return (const char *)(dgettext("pidgin","A folder with that name already exists"));
    }
    case 0xD100L + 10:
{
      return (const char *)(dgettext("pidgin","Not supported"));
    }
    case 0xD100L + 11:
{
    }
    case 0xD100L + 0x0042:
{
      return (const char *)(dgettext("pidgin","Password has expired"));
    }
    case 0xD100L + 12:
{
      return (const char *)(dgettext("pidgin","Incorrect password"));
    }
    case 0xD100L + 13:
{
      return (const char *)(dgettext("pidgin","User not found"));
    }
    case 0xD100L + 16:
{
      return (const char *)(dgettext("pidgin","Account has been disabled"));
    }
    case 0xD100L + 0x0011:
{
      return (const char *)(dgettext("pidgin","The server could not access the directory"));
    }
    case 0xD100L + 28:
{
      return (const char *)(dgettext("pidgin","Your system administrator has disabled this operation"));
    }
    case 0xD100L + 0x0023:
{
      return (const char *)(dgettext("pidgin","The server is unavailable; try again later"));
    }
    case 0xD100L + 0x0027:
{
      return (const char *)(dgettext("pidgin","Cannot add a contact to the same folder twice"));
    }
    case 0xD100L + 40:
{
      return (const char *)(dgettext("pidgin","Cannot add yourself"));
    }
    case 0xD100L + 0x003A:
{
      return (const char *)(dgettext("pidgin","Master archive is misconfigured"));
    }
    case 0xD100L + 0x0046:
{
    }
    case 0xD100L + 0x0049:
{
      return (const char *)(dgettext("pidgin","Incorrect username or password"));
    }
    case 0xD100L + 25:
{
      return (const char *)(dgettext("pidgin","Could not recognize the host of the username you entered"));
    }
    case 0xD100L + 6:
{
      return (const char *)(dgettext("pidgin","Your account has been disabled because too many incorrect passwords were entered"));
    }
    case 0xD100L + 31:
{
      return (const char *)(dgettext("pidgin","You cannot add the same person twice to a conversation"));
    }
    case 0xD100L + 0x0029:
{
    }
    case 0xD100L + ',':
{
      return (const char *)(dgettext("pidgin","You have reached your limit for the number of contacts allowed"));
    }
    case 0xD100L + 0x0024:
{
      return (const char *)(dgettext("pidgin","You have entered an incorrect username"));
    }
    case 0xD100L + 0x0025:
{
      return (const char *)(dgettext("pidgin","An error occurred while updating the directory"));
    }
    case 0xD100L + 48:
{
      return (const char *)(dgettext("pidgin","Incompatible protocol version"));
    }
    case 0xD100L + 0x0039:
{
      return (const char *)(dgettext("pidgin","The user has blocked you"));
    }
    case 0xD100L + 0x004A:
{
      return (const char *)(dgettext("pidgin","This evaluation version does not allow more than ten users to log in at one time"));
    }
    case 0xD100L + 0x0035:
{
      return (const char *)(dgettext("pidgin","The user is either offline or you are blocked"));
    }
    default:
{
      unknown_msg = g_strdup_printf(((const char *)(dgettext("pidgin","Unknown error: 0x%X"))),err);
      return unknown_msg;
    }
  }
}

static void _update_contact_list(NMUser *user,NMField *fields)
{
  NMField *list;
  NMField *cursor;
  NMField *locate;
  gint objid1;
  NMContact *contact;
  NMFolder *folder;
  gpointer item;
  if ((user == ((NMUser *)((void *)0))) || (fields == ((NMField *)((void *)0)))) 
    return ;
/* Is it wrapped in a RESULTS array? */
  if (strcmp((fields -> tag),"NM_A_FA_RESULTS") == 0) {
    list = ((NMField *)(fields -> ptr_value));
  }
  else {
    list = fields;
  }
/* Update the cached contact list */
  cursor = ((NMField *)(list -> ptr_value));
  while((cursor -> tag) != ((char *)((void *)0))){
    if ((g_ascii_strcasecmp((cursor -> tag),"NM_A_FA_CONTACT") == 0) || (g_ascii_strcasecmp((cursor -> tag),"NM_A_FA_FOLDER") == 0)) {
      locate = nm_locate_field("NM_A_SZ_OBJECT_ID",((NMField *)(cursor -> ptr_value)));
      if ((locate != ((NMField *)((void *)0))) && ((locate -> ptr_value) != 0)) {
        objid1 = atoi(((char *)(locate -> ptr_value)));
        item = nm_folder_find_item_by_object_id((user -> root_folder),objid1);
        if (item != ((void *)((void *)0))) {
          if ((cursor -> method) == 5) {
            if (g_ascii_strcasecmp((cursor -> tag),"NM_A_FA_CONTACT") == 0) {
              contact = ((NMContact *)item);
              nm_contact_update_list_properties(contact,cursor);
            }
            else if (g_ascii_strcasecmp((cursor -> tag),"NM_A_FA_FOLDER") == 0) {
              folder = ((NMFolder *)item);
              nm_folder_update_list_properties(folder,cursor);
            }
          }
          else if ((cursor -> method) == 2) {
            if (g_ascii_strcasecmp((cursor -> tag),"NM_A_FA_CONTACT") == 0) {
              contact = ((NMContact *)item);
              folder = nm_find_folder_by_id(user,nm_contact_get_parent_id(contact));
              if (folder != 0) {
                nm_folder_remove_contact(folder,contact);
              }
            }
            else if (g_ascii_strcasecmp((cursor -> tag),"NM_A_FA_FOLDER") == 0) {
/* TODO: write nm_folder_remove_folder */
/* ignoring for now, should not be a big deal */
/*								folder = (NMFolder *) item;*/
/*								nm_folder_remove_folder(user->root_folder, folder);*/
            }
          }
        }
        else {
          if ((cursor -> method) == 5) {
/* Not found,  so we need to add it */
            if (g_ascii_strcasecmp((cursor -> tag),"NM_A_FA_CONTACT") == 0) {
              const char *dn = (const char *)((void *)0);
              locate = nm_locate_field("NM_A_SZ_DN",((NMField *)(cursor -> ptr_value)));
              if ((locate != ((NMField *)((void *)0))) && ((locate -> ptr_value) != 0)) {
                dn = ((const char *)(locate -> ptr_value));
                if (dn != ((const char *)((void *)0))) {
                  contact = nm_create_contact_from_fields(cursor);
                  if (contact != 0) {
                    nm_folder_add_contact_to_list((user -> root_folder),contact);
                    nm_release_contact(contact);
                  }
                }
              }
            }
            else if (g_ascii_strcasecmp((cursor -> tag),"NM_A_FA_FOLDER") == 0) {
              folder = nm_create_folder_from_fields(cursor);
              nm_folder_add_folder_to_list((user -> root_folder),folder);
              nm_release_folder(folder);
            }
          }
        }
      }
    }
    cursor++;
  }
}

static char *nm_rtfize_text(char *text)
{
  GString *gstr = (GString *)((void *)0);
  unsigned char *pch;
  char *uni_str = (char *)((void *)0);
  char *rtf = (char *)((void *)0);
  int bytes;
  gunichar uc;
  gstr = g_string_sized_new((strlen(text) * 2));
  pch = ((unsigned char *)text);
  while(( *pch) != 0){
    if (( *pch) <= 0x7F) {
      switch(( *pch)){
        case '\\':
{
        }
        case '{':
{
        }
        case '}':
{
          gstr = g_string_append_c_inline(gstr,'\\');
          gstr = g_string_append_c_inline(gstr,( *pch));
          break; 
        }
        case '\n':
{
          gstr = g_string_append(gstr,"\\par ");
          break; 
        }
        default:
{
          gstr = g_string_append_c_inline(gstr,( *pch));
          break; 
        }
      }
      pch++;
    }
    else {
/* convert the utf-8 character to ucs-4 for rtf encoding */
      if (( *pch) <= 0xDF) {
        uc = (((((gunichar )pch[0]) & 31) << 6) | (((gunichar )pch[1]) & 0x003F));
        bytes = 2;
      }
      else if (( *pch) <= 0xEF) {
        uc = ((((((gunichar )pch[0]) & 15) << 12) | ((((gunichar )pch[1]) & 0x003F) << 6)) | (((gunichar )pch[2]) & 0x003F));
        bytes = 3;
      }
      else if (( *pch) <= 0xF7) {
        uc = (((((((gunichar )pch[0]) & 7) << 18) | ((((gunichar )pch[1]) & 0x003F) << 12)) | ((((gunichar )pch[2]) & 0x003F) << 6)) | (((gunichar )pch[3]) & 0x003F));
        bytes = 4;
      }
      else if (( *pch) <= 0xFB) {
        uc = ((((((((gunichar )pch[0]) & 3) << 24) | ((((gunichar )pch[1]) & 0x003F) << 18)) | ((((gunichar )pch[2]) & 0x003F) << 12)) | ((((gunichar )pch[3]) & 0x003F) << 6)) | (((gunichar )pch[4]) & 0x003F));
        bytes = 5;
      }
      else if (( *pch) <= 0xFD) {
        uc = (((((((((gunichar )pch[0]) & 1) << 30) | ((((gunichar )pch[1]) & 0x003F) << 24)) | ((((gunichar )pch[2]) & 0x003F) << 18)) | ((((gunichar )pch[3]) & 0x003F) << 12)) | ((((gunichar )pch[4]) & 0x003F) << 6)) | (((gunichar )pch[5]) & 0x003F));
        bytes = 6;
      }
      else {
/* should never happen ... bogus utf-8! */
        purple_debug_info("novell","bogus utf-8 lead byte: 0x%X\n",pch[0]);
        uc = 0x003F;
        bytes = 1;
      }
      uni_str = g_strdup_printf("\\u%d\?",uc);
      purple_debug_info("novell","unicode escaped char %s\n",uni_str);
      gstr = g_string_append(gstr,uni_str);
      pch += bytes;
      g_free(uni_str);
    }
  }
  rtf = g_strdup_printf("{\\rtf1\\ansi\n{\\fonttbl{\\f0\\fnil Unknown;}}\n{\\colortbl ;\\red0\\green0\\blue0;}\n\\uc1\\cf1\\f0\\fs24 %s\\par\n}",(gstr -> str));
  g_string_free(gstr,(!0));
  return rtf;
}
