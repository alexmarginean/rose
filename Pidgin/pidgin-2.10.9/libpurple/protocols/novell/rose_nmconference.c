/*
 * nmconference.c
 *
 * Copyright (c) 2004 Novell, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA	02111-1301	USA
 *
 */
#include <string.h>
#include "nmconference.h"
static int conf_count = 0;

struct _NMConference 
{
/* The conference identifier */
  char *guid;
/* The list of participants for the conference */
  GSList *participants;
/* Flags for the conference */
  guint32 flags;
/* User defined data */
  gpointer data;
/* Reference count for this object */
  int ref_count;
}
;
/*******************************************************************************
 * Conference API -- see header file for comments
 ******************************************************************************/

NMConference *nm_create_conference(const char *guid)
{
  NMConference *conf = (NMConference *)(g_malloc0_n(1,(sizeof(NMConference ))));
  if (guid != 0) {
    conf -> guid = g_strdup(guid);
  }
  else {
    conf -> guid = g_strdup("[00000000-00000000-00000000-0000-0000]");
  }
  conf -> ref_count = 1;
  purple_debug(PURPLE_DEBUG_INFO,"novell","Creating a conference %p, total=%d\n",conf,conf_count++);
  return conf;
}

void nm_release_conference(NMConference *conference)
{
  GSList *node;
  do {
    if (conference != ((NMConference *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conference != NULL");
      return ;
    };
  }while (0);
  purple_debug(PURPLE_DEBUG_INFO,"novell","In release conference %p, refs=%d\n",conference,(conference -> ref_count));
  if (--conference -> ref_count == 0) {
    purple_debug(PURPLE_DEBUG_INFO,"novell","Releasing conference %p, total=%d\n",conference,--conf_count);
    if ((conference -> guid) != 0) 
      g_free((conference -> guid));
    if ((conference -> participants) != 0) {
      for (node = (conference -> participants); node != 0; node = (node -> next)) {
        if ((node -> data) != 0) {
          NMUserRecord *user_record = (node -> data);
          nm_release_user_record(user_record);
          node -> data = ((gpointer )((void *)0));
        }
      }
      g_slist_free((conference -> participants));
    }
    g_free(conference);
  }
}

gboolean nm_conference_is_instantiated(NMConference *conference)
{
  if (conference == ((NMConference *)((void *)0))) 
    return 0;
  return strncmp((conference -> guid),"[00000000-00000000-00000000-0000-0000]",27) != 0;
}

int nm_conference_get_participant_count(NMConference *conference)
{
  if (conference == ((NMConference *)((void *)0))) 
    return 0;
  return (g_slist_length((conference -> participants)));
}

NMUserRecord *nm_conference_get_participant(NMConference *conference,int index)
{
  if (conference == ((NMConference *)((void *)0))) 
    return 0;
  return (NMUserRecord *)(g_slist_nth_data((conference -> participants),index));
}

void nm_conference_add_participant(NMConference *conference,NMUserRecord *user_record)
{
  if ((conference == ((NMConference *)((void *)0))) || (user_record == ((NMUserRecord *)((void *)0)))) {
    return ;
  }
  nm_user_record_add_ref(user_record);
  conference -> participants = g_slist_append((conference -> participants),user_record);
}

void nm_conference_remove_participant(NMConference *conference,const char *dn)
{
  GSList *node;
  GSList *element = (GSList *)((void *)0);
  if ((conference == ((NMConference *)((void *)0))) || (dn == ((const char *)((void *)0)))) {
    return ;
  }
{
    for (node = (conference -> participants); node != 0; node = (node -> next)) {
      NMUserRecord *user_record = (node -> data);
      if (user_record != 0) {
        if (nm_utf8_str_equal(dn,(nm_user_record_get_dn(user_record))) != 0) {
          element = node;
          break; 
        }
      }
    }
  }
  if (element != 0) {
    nm_release_user_record(((NMUserRecord *)(element -> data)));
    element -> data = ((gpointer )((void *)0));
    conference -> participants = g_slist_remove_link((conference -> participants),element);
    g_slist_free_1(element);
  }
}

void nm_conference_add_ref(NMConference *conference)
{
  if (conference != 0) 
    conference -> ref_count++;
}

void nm_conference_set_flags(NMConference *conference,guint32 flags)
{
  if (conference != 0) {
    conference -> flags = flags;
  }
}

void nm_conference_set_guid(NMConference *conference,const char *guid)
{
  if (conference != 0) {
/* Release memory for old guid */
    if ((conference -> guid) != 0) {
      g_free((conference -> guid));
    }
/* Set the new guid */
    if (guid != 0) 
      conference -> guid = g_strdup(guid);
    else 
      conference -> guid = g_strdup("[00000000-00000000-00000000-0000-0000]");
  }
}

void nm_conference_set_data(NMConference *conference,gpointer data)
{
  if (conference == ((NMConference *)((void *)0))) 
    return ;
  conference -> data = data;
}

gpointer nm_conference_get_data(NMConference *conference)
{
  if (conference == ((NMConference *)((void *)0))) 
    return 0;
  return conference -> data;
}

const char *nm_conference_get_guid(NMConference *conference)
{
  if (conference == ((NMConference *)((void *)0))) 
    return 0;
  return (conference -> guid);
}
