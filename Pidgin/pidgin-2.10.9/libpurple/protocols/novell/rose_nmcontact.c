/*
 * nmcontact.c
 *
 * Copyright (c) 2004 Novell, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA	02111-1301	USA
 *
 */
#include <glib.h>
#include <string.h>
#include "nmcontact.h"
#include "nmfield.h"
#include "nmuser.h"

struct _NMContact 
{
  int id;
  int parent_id;
  int seq;
  char *dn;
  char *display_name;
  NMUserRecord *user_record;
  gpointer data;
  int ref_count;
}
;

struct _NMFolder 
{
  int id;
  int seq;
  char *name;
  GSList *folders;
  GSList *contacts;
  int ref_count;
}
;
static int count = 0;
static void _release_folder_contacts(NMFolder *folder);
static void _release_folder_folders(NMFolder *folder);
static void _add_contacts(NMUser *user,NMFolder *folder,NMField *fields);
static void _add_folders(NMFolder *root,NMField *fields);
/*********************************************************************
 * Contact API
 *********************************************************************/

NMContact *nm_create_contact()
{
  NMContact *contact = (NMContact *)(g_malloc0_n(1,(sizeof(NMContact ))));
  contact -> ref_count = 1;
  purple_debug(PURPLE_DEBUG_INFO,"novell","Creating contact, total=%d\n",count++);
  return contact;
}
/*
 * This creates a contact for the contact list. The
 * field array that is passed in should be a
 * NM_A_FA_CONTACT array.
 *
 */

NMContact *nm_create_contact_from_fields(NMField *fields)
{
  NMContact *contact;
  NMField *field;
  if ((((fields == ((NMField *)((void *)0))) || ((fields -> tag) == ((char *)((void *)0)))) || ((fields -> ptr_value) == 0)) || (strcmp((fields -> tag),"NM_A_FA_CONTACT") != 0)) {
    return 0;
  }
  contact = nm_create_contact();
  if ((field = nm_locate_field("NM_A_SZ_OBJECT_ID",((NMField *)(fields -> ptr_value)))) != 0) {
    if ((field -> ptr_value) != 0) 
      contact -> id = atoi(((char *)(field -> ptr_value)));
  }
  if ((field = nm_locate_field("NM_A_SZ_PARENT_ID",((NMField *)(fields -> ptr_value)))) != 0) {
    if ((field -> ptr_value) != 0) 
      contact -> parent_id = atoi(((char *)(field -> ptr_value)));
  }
  if ((field = nm_locate_field("NM_A_SZ_SEQUENCE_NUMBER",((NMField *)(fields -> ptr_value)))) != 0) {
    if ((field -> ptr_value) != 0) 
      contact -> seq = atoi(((char *)(field -> ptr_value)));
  }
  if ((field = nm_locate_field("NM_A_SZ_DISPLAY_NAME",((NMField *)(fields -> ptr_value)))) != 0) {
    if ((field -> ptr_value) != 0) 
      contact -> display_name = g_strdup(((char *)(field -> ptr_value)));
  }
  if ((field = nm_locate_field("NM_A_SZ_DN",((NMField *)(fields -> ptr_value)))) != 0) {
    if ((field -> ptr_value) != 0) 
      contact -> dn = g_strdup(((char *)(field -> ptr_value)));
  }
  return contact;
}

void nm_contact_update_list_properties(NMContact *contact,NMField *fields)
{
  NMField *field;
  if (((contact == ((NMContact *)((void *)0))) || (fields == ((NMField *)((void *)0)))) || ((fields -> ptr_value) == 0)) 
    return ;
  if ((field = nm_locate_field("NM_A_SZ_OBJECT_ID",((NMField *)(fields -> ptr_value)))) != 0) {
    if ((field -> ptr_value) != 0) 
      contact -> id = atoi(((char *)(field -> ptr_value)));
  }
  if ((field = nm_locate_field("NM_A_SZ_PARENT_ID",((NMField *)(fields -> ptr_value)))) != 0) {
    if ((field -> ptr_value) != 0) 
      contact -> parent_id = atoi(((char *)(field -> ptr_value)));
  }
  if ((field = nm_locate_field("NM_A_SZ_SEQUENCE_NUMBER",((NMField *)(fields -> ptr_value)))) != 0) {
    if ((field -> ptr_value) != 0) 
      contact -> seq = atoi(((char *)(field -> ptr_value)));
  }
  if ((field = nm_locate_field("NM_A_SZ_DISPLAY_NAME",((NMField *)(fields -> ptr_value)))) != 0) {
    if ((field -> ptr_value) != 0) {
      if ((contact -> display_name) != 0) 
        g_free((contact -> display_name));
      contact -> display_name = g_strdup(((char *)(field -> ptr_value)));
    }
  }
  if ((field = nm_locate_field("NM_A_SZ_DN",((NMField *)(fields -> ptr_value)))) != 0) {
    if ((field -> ptr_value) != 0) {
      if ((contact -> dn) != 0) 
        g_free((contact -> dn));
      contact -> dn = g_strdup(((char *)(field -> ptr_value)));
    }
  }
}

NMField *nm_contact_to_fields(NMContact *contact)
{
  NMField *fields = (NMField *)((void *)0);
  if (contact == ((NMContact *)((void *)0))) 
    return 0;
  fields = nm_field_add_pointer(fields,"NM_A_SZ_OBJECT_ID",0,0,0,(g_strdup_printf("%d",(contact -> id))),10);
  fields = nm_field_add_pointer(fields,"NM_A_SZ_PARENT_ID",0,0,0,(g_strdup_printf("%d",(contact -> parent_id))),10);
  fields = nm_field_add_pointer(fields,"NM_A_SZ_SEQUENCE_NUMBER",0,0,0,(g_strdup_printf("%d",(contact -> seq))),10);
  if ((contact -> display_name) != ((char *)((void *)0))) {
    fields = nm_field_add_pointer(fields,"NM_A_SZ_DISPLAY_NAME",0,0,0,(g_strdup((contact -> display_name))),10);
  }
  if ((contact -> dn) != ((char *)((void *)0))) {
    fields = nm_field_add_pointer(fields,"NM_A_SZ_DN",0,0,0,(g_strdup((contact -> dn))),10);
  }
  return fields;
}

void nm_contact_add_ref(NMContact *contact)
{
  if (contact != 0) 
    contact -> ref_count++;
}

void nm_release_contact(NMContact *contact)
{
  if (contact == ((NMContact *)((void *)0))) 
    return ;
  if (--contact -> ref_count == 0) {
    purple_debug(PURPLE_DEBUG_INFO,"novell","Releasing contact, total=%d\n",--count);
    if ((contact -> display_name) != 0) {
      g_free((contact -> display_name));
    }
    if ((contact -> dn) != 0) {
      g_free((contact -> dn));
    }
    if ((contact -> user_record) != 0) {
      nm_release_user_record((contact -> user_record));
    }
    g_free(contact);
  }
}

const char *nm_contact_get_display_name(NMContact *contact)
{
  if (contact == ((NMContact *)((void *)0))) 
    return 0;
  if (((contact -> user_record) != ((NMUserRecord *)((void *)0))) && ((contact -> display_name) == ((char *)((void *)0)))) {
    const char *full_name;
    const char *lname;
    const char *fname;
    const char *cn;
    const char *display_id;
    full_name = nm_user_record_get_full_name((contact -> user_record));
    fname = nm_user_record_get_first_name((contact -> user_record));
    lname = nm_user_record_get_last_name((contact -> user_record));
    cn = nm_user_record_get_userid((contact -> user_record));
    display_id = nm_user_record_get_display_id((contact -> user_record));
/* Try to build a display name. */
    if (full_name != 0) {
      contact -> display_name = g_strdup(full_name);
    }
    else if ((fname != 0) && (lname != 0)) {
      contact -> display_name = g_strdup_printf("%s %s",fname,lname);
    }
    else {
/* If auth attribute is set use it */
      if ((nm_user_record_get_auth_attr((contact -> user_record)) != 0) && (display_id != ((const char *)((void *)0)))) {
        contact -> display_name = g_strdup(display_id);
      }
      else {
/* Use CN or display id */
        if (cn != 0) {
          contact -> display_name = g_strdup(cn);
        }
        else if (display_id != 0) {
          contact -> display_name = g_strdup(display_id);
        }
      }
    }
  }
  return (contact -> display_name);
}

void nm_contact_set_display_name(NMContact *contact,const char *display_name)
{
  if (contact == ((NMContact *)((void *)0))) 
    return ;
  if ((contact -> display_name) != 0) {
    g_free((contact -> display_name));
    contact -> display_name = ((char *)((void *)0));
  }
  if (display_name != 0) 
    contact -> display_name = g_strdup(display_name);
}

void nm_contact_set_dn(NMContact *contact,const char *dn)
{
  if (contact == ((NMContact *)((void *)0))) 
    return ;
  if ((contact -> dn) != 0) {
    g_free((contact -> dn));
    contact -> dn = ((char *)((void *)0));
  }
  if (dn != 0) 
    contact -> dn = g_strdup(dn);
}

const char *nm_contact_get_dn(NMContact *contact)
{
  if (contact == ((NMContact *)((void *)0))) 
    return 0;
  return (contact -> dn);
}

gpointer nm_contact_get_data(NMContact *contact)
{
  if (contact == ((NMContact *)((void *)0))) 
    return 0;
  return contact -> data;
}

int nm_contact_get_id(NMContact *contact)
{
  if (contact == ((NMContact *)((void *)0))) 
    return -1;
  return contact -> id;
}

int nm_contact_get_parent_id(NMContact *contact)
{
  if (contact == ((NMContact *)((void *)0))) 
    return -1;
  return contact -> parent_id;
}

void nm_contact_set_data(NMContact *contact,gpointer data)
{
  if (contact == ((NMContact *)((void *)0))) 
    return ;
  contact -> data = data;
}

void nm_contact_set_user_record(NMContact *contact,NMUserRecord *user_record)
{
  if (contact == ((NMContact *)((void *)0))) 
    return ;
  if ((contact -> user_record) != 0) {
    nm_release_user_record((contact -> user_record));
  }
  nm_user_record_add_ref(user_record);
  contact -> user_record = user_record;
}

NMUserRecord *nm_contact_get_user_record(NMContact *contact)
{
  if (contact == ((NMContact *)((void *)0))) 
    return 0;
  return contact -> user_record;
}

const char *nm_contact_get_userid(NMContact *contact)
{
  NMUserRecord *user_record;
  const char *userid = (const char *)((void *)0);
  if (contact == ((NMContact *)((void *)0))) 
    return 0;
  user_record = nm_contact_get_user_record(contact);
  if (user_record != 0) {
    userid = nm_user_record_get_userid(user_record);
  }
  return userid;
}

const char *nm_contact_get_display_id(NMContact *contact)
{
  NMUserRecord *user_record;
  const char *id = (const char *)((void *)0);
  if (contact == ((NMContact *)((void *)0))) 
    return 0;
  user_record = nm_contact_get_user_record(contact);
  if (user_record != 0) {
    id = nm_user_record_get_display_id(user_record);
  }
  return id;
}
/*********************************************************************
 * Folder API
 *********************************************************************/

NMFolder *nm_create_folder(const char *name)
{
  NMFolder *folder = (NMFolder *)(g_malloc0_n(1,(sizeof(NMFolder ))));
  if (name != 0) 
    folder -> name = g_strdup(name);
  folder -> ref_count = 1;
  return folder;
}

NMFolder *nm_create_folder_from_fields(NMField *fields)
{
  NMField *field;
  NMFolder *folder;
  if ((fields == ((NMField *)((void *)0))) || ((fields -> ptr_value) == 0)) 
    return 0;
  folder = ((NMFolder *)(g_malloc0_n(1,(sizeof(NMFolder )))));
  if ((field = nm_locate_field("NM_A_SZ_OBJECT_ID",((NMField *)(fields -> ptr_value)))) != 0) {
    if ((field -> ptr_value) != 0) 
      folder -> id = atoi(((char *)(field -> ptr_value)));
  }
  if ((field = nm_locate_field("NM_A_SZ_SEQUENCE_NUMBER",((NMField *)(fields -> ptr_value)))) != 0) {
    if ((field -> ptr_value) != 0) 
      folder -> seq = atoi(((char *)(field -> ptr_value)));
  }
  if ((field = nm_locate_field("NM_A_SZ_DISPLAY_NAME",((NMField *)(fields -> ptr_value)))) != 0) {
    if ((field -> ptr_value) != 0) 
      folder -> name = g_strdup(((char *)(field -> ptr_value)));
  }
  folder -> ref_count = 1;
  return folder;
}

NMField *nm_folder_to_fields(NMFolder *folder)
{
  NMField *fields = (NMField *)((void *)0);
  if (folder == ((NMFolder *)((void *)0))) 
    return 0;
  fields = nm_field_add_pointer(fields,"NM_A_SZ_OBJECT_ID",0,0,0,(g_strdup_printf("%d",(folder -> id))),10);
  fields = nm_field_add_pointer(fields,"NM_A_SZ_PARENT_ID",0,0,0,(g_strdup("0")),10);
  fields = nm_field_add_pointer(fields,"NM_A_SZ_TYPE",0,0,0,(g_strdup("1")),10);
  fields = nm_field_add_pointer(fields,"NM_A_SZ_SEQUENCE_NUMBER",0,0,0,(g_strdup_printf("%d",(folder -> seq))),10);
  if ((folder -> name) != ((char *)((void *)0))) {
    fields = nm_field_add_pointer(fields,"NM_A_SZ_DISPLAY_NAME",0,0,0,(g_strdup((folder -> name))),10);
  }
  return fields;
}

void nm_folder_update_list_properties(NMFolder *folder,NMField *fields)
{
  NMField *field;
  if (((folder == ((NMFolder *)((void *)0))) || (fields == ((NMField *)((void *)0)))) || ((fields -> ptr_value) == 0)) 
    return ;
  if ((field = nm_locate_field("NM_A_SZ_OBJECT_ID",((NMField *)(fields -> ptr_value)))) != 0) {
    if ((field -> ptr_value) != 0) 
      folder -> id = atoi(((char *)(field -> ptr_value)));
  }
  if ((field = nm_locate_field("NM_A_SZ_SEQUENCE_NUMBER",((NMField *)(fields -> ptr_value)))) != 0) {
    if ((field -> ptr_value) != 0) 
      folder -> seq = atoi(((char *)(field -> ptr_value)));
  }
  if ((field = nm_locate_field("NM_A_SZ_DISPLAY_NAME",((NMField *)(fields -> ptr_value)))) != 0) {
    if ((field -> ptr_value) != 0) {
      if ((folder -> name) != 0) 
        g_free((folder -> name));
      folder -> name = g_strdup(((char *)(field -> ptr_value)));
    }
  }
}

void nm_release_folder(NMFolder *folder)
{
  if (folder == ((NMFolder *)((void *)0))) 
    return ;
  if (--folder -> ref_count == 0) {
    if ((folder -> name) != 0) {
      g_free((folder -> name));
    }
    if ((folder -> folders) != 0) {
      _release_folder_folders(folder);
    }
    if ((folder -> contacts) != 0) {
      _release_folder_contacts(folder);
    }
    g_free(folder);
  }
}

void nm_folder_add_ref(NMFolder *folder)
{
  if (folder != 0) 
    folder -> ref_count++;
}

int nm_folder_get_subfolder_count(NMFolder *folder)
{
  if (folder == ((NMFolder *)((void *)0))) 
    return 0;
  if ((folder -> folders) != 0) 
    return (g_slist_length((folder -> folders)));
  else 
    return 0;
}

NMFolder *nm_folder_get_subfolder(NMFolder *folder,int index)
{
  if (folder == ((NMFolder *)((void *)0))) 
    return 0;
  if ((folder -> folders) != 0) 
    return (NMFolder *)(g_slist_nth_data((folder -> folders),index));
  else 
    return 0;
}

int nm_folder_get_contact_count(NMFolder *folder)
{
  if (folder == ((NMFolder *)((void *)0))) 
    return 0;
  if ((folder -> contacts) != ((GSList *)((void *)0))) 
    return (g_slist_length((folder -> contacts)));
  else 
    return 0;
}

NMContact *nm_folder_get_contact(NMFolder *folder,int index)
{
  if (folder == ((NMFolder *)((void *)0))) 
    return 0;
  if ((folder -> contacts) != 0) 
    return (NMContact *)(g_slist_nth_data((folder -> contacts),index));
  else 
    return 0;
}

const char *nm_folder_get_name(NMFolder *folder)
{
  if (folder == ((NMFolder *)((void *)0))) 
    return 0;
  return (folder -> name);
}

void nm_folder_set_name(NMFolder *folder,const char *name)
{
  if ((folder == ((NMFolder *)((void *)0))) || (name == ((const char *)((void *)0)))) 
    return ;
  if ((folder -> name) != 0) 
    g_free((folder -> name));
  folder -> name = g_strdup(name);
}

int nm_folder_get_id(NMFolder *folder)
{
  if (folder == ((NMFolder *)((void *)0))) {
    return -1;
  }
  return folder -> id;
}

void nm_folder_add_folder_to_list(NMFolder *root,NMFolder *folder)
{
  GSList *node;
  if ((root == ((NMFolder *)((void *)0))) || (folder == ((NMFolder *)((void *)0)))) 
    return ;
  node = (root -> folders);
{
    while(node != 0){
      if ((folder -> seq) <= ( *((NMFolder *)(node -> data))).seq) {
        nm_folder_add_ref(folder);
        root -> folders = g_slist_insert_before((root -> folders),node,folder);
        break; 
      }
      node = ((node != 0)?( *((GSList *)node)).next : ((struct _GSList *)((void *)0)));
    }
  }
  if (node == ((GSList *)((void *)0))) {
    nm_folder_add_ref(folder);
    root -> folders = g_slist_append((root -> folders),folder);
  }
}

void nm_folder_remove_contact(NMFolder *folder,NMContact *contact)
{
  GSList *node;
  if ((folder == ((NMFolder *)((void *)0))) || (contact == ((NMContact *)((void *)0)))) 
    return ;
  node = (folder -> contacts);
{
    while(node != 0){
      if ((contact -> id) == ( *((NMContact *)(node -> data))).id) {
        folder -> contacts = g_slist_remove((folder -> contacts),(node -> data));
        nm_release_contact(contact);
        break; 
      }
      node = ((node != 0)?( *((GSList *)node)).next : ((struct _GSList *)((void *)0)));
    }
  }
}

void nm_folder_add_contact_to_list(NMFolder *root_folder,NMContact *contact)
{
  GSList *node = (GSList *)((void *)0);
  NMFolder *folder = root_folder;
  if ((folder == ((NMFolder *)((void *)0))) || (contact == ((NMContact *)((void *)0)))) 
    return ;
/* Find folder to add contact to */
  if ((contact -> parent_id) != 0) {
    node = (folder -> folders);
{
      while(node != 0){
        folder = ((NMFolder *)(node -> data));
        if ((contact -> parent_id) == (folder -> id)) {
          break; 
        }
        folder = ((NMFolder *)((void *)0));
        node = ((node != 0)?( *((GSList *)node)).next : ((struct _GSList *)((void *)0)));
      }
    }
  }
/* Add contact to list */
  if (folder != 0) {
    node = (folder -> contacts);
{
      while(node != 0){
        if ((contact -> seq) <= ( *((NMContact *)(node -> data))).seq) {
          nm_contact_add_ref(contact);
          folder -> contacts = g_slist_insert_before((folder -> contacts),node,contact);
          break; 
        }
        node = ((node != 0)?( *((GSList *)node)).next : ((struct _GSList *)((void *)0)));
      }
    }
    if (node == ((GSList *)((void *)0))) {
      nm_contact_add_ref(contact);
      folder -> contacts = g_slist_append((folder -> contacts),contact);
    }
  }
}

void nm_folder_add_contacts_and_folders(NMUser *user,NMFolder *root,NMField *fields)
{
/* Add the contacts and folders from the field array */
  if (((user != 0) && (root != 0)) && (fields != 0)) {
    _add_folders(root,fields);
    _add_contacts(user,root,fields);
  }
}

gpointer nm_folder_find_item_by_object_id(NMFolder *root_folder,int object_id)
{
  int cnt;
  int cnt2;
  int i;
  int j;
  gpointer item = (gpointer )((void *)0);
  NMFolder *folder;
  NMContact *contact;
  if (root_folder == ((NMFolder *)((void *)0))) 
    return 0;
/* Check all contacts for the top level folder */
  cnt = nm_folder_get_contact_count(root_folder);
{
    for (i = 0; i < cnt; i++) {
      contact = nm_folder_get_contact(root_folder,i);
      if ((contact != 0) && ((contact -> id) == object_id)) {
        item = contact;
        break; 
      }
    }
  }
/* If we haven't found the item yet, check the subfolders */
  if (item == ((void *)((void *)0))) {
    cnt = nm_folder_get_subfolder_count(root_folder);
{
      for (i = 0; (i < cnt) && (item == ((void *)((void *)0))); i++) {
        folder = nm_folder_get_subfolder(root_folder,i);
/* Check the id of this folder */
        if ((folder != 0) && ((folder -> id) == object_id)) {
          item = folder;
          break; 
        }
/* Check all contacts for this folder */
        cnt2 = nm_folder_get_contact_count(folder);
{
          for (j = 0; j < cnt2; j++) {
            contact = nm_folder_get_contact(folder,j);
            if ((contact != 0) && ((contact -> id) == object_id)) {
              item = contact;
              break; 
            }
          }
        }
      }
    }
  }
  return item;
}

NMContact *nm_folder_find_contact_by_userid(NMFolder *folder,const char *userid)
{
  int cnt;
  int i;
  NMContact *tmp;
  NMContact *contact = (NMContact *)((void *)0);
  if ((folder == ((NMFolder *)((void *)0))) || (userid == ((const char *)((void *)0)))) 
    return 0;
  cnt = nm_folder_get_contact_count(folder);
{
    for (i = 0; i < cnt; i++) {
      tmp = nm_folder_get_contact(folder,i);
      if ((tmp != 0) && (nm_utf8_str_equal((nm_contact_get_userid(tmp)),userid) != 0)) {
        contact = tmp;
        break; 
      }
    }
  }
  return contact;
}

NMContact *nm_folder_find_contact_by_display_id(NMFolder *folder,const char *display_id)
{
  int cnt;
  int i;
  NMContact *tmp;
  NMContact *contact = (NMContact *)((void *)0);
  if ((folder == ((NMFolder *)((void *)0))) || (display_id == ((const char *)((void *)0)))) 
    return 0;
  cnt = nm_folder_get_contact_count(folder);
{
    for (i = 0; i < cnt; i++) {
      tmp = nm_folder_get_contact(folder,i);
      if ((tmp != 0) && (nm_utf8_str_equal((nm_contact_get_display_id(tmp)),display_id) != 0)) {
        contact = tmp;
        break; 
      }
    }
  }
  return contact;
}

NMContact *nm_folder_find_contact(NMFolder *folder,const char *dn)
{
  int cnt;
  int i;
  NMContact *tmp;
  NMContact *contact = (NMContact *)((void *)0);
  if ((folder == ((NMFolder *)((void *)0))) || (dn == ((const char *)((void *)0)))) 
    return 0;
  cnt = nm_folder_get_contact_count(folder);
{
    for (i = 0; i < cnt; i++) {
      tmp = nm_folder_get_contact(folder,i);
      if ((tmp != 0) && (nm_utf8_str_equal((nm_contact_get_dn(tmp)),dn) != 0)) {
        contact = tmp;
        break; 
      }
    }
  }
  return contact;
}
/*********************************************************************
 * Utility functions
 *********************************************************************/

static void _release_folder_contacts(NMFolder *folder)
{
  GSList *cnode;
  NMContact *contact;
  for (cnode = (folder -> contacts); cnode != 0; cnode = (cnode -> next)) {
    contact = (cnode -> data);
    cnode -> data = ((gpointer )((void *)0));
    nm_release_contact(contact);
  }
  g_slist_free((folder -> contacts));
  folder -> contacts = ((GSList *)((void *)0));
}

static void _release_folder_folders(NMFolder *folder)
{
  GSList *fnode;
  NMFolder *subfolder;
  if (folder == ((NMFolder *)((void *)0))) 
    return ;
  for (fnode = (folder -> folders); fnode != 0; fnode = (fnode -> next)) {
    subfolder = (fnode -> data);
    fnode -> data = ((gpointer )((void *)0));
    nm_release_folder(subfolder);
  }
  g_slist_free((folder -> folders));
  folder -> folders = ((GSList *)((void *)0));
}

static void _add_folders(NMFolder *root,NMField *fields)
{
  NMFolder *folder = (NMFolder *)((void *)0);
  NMField *locate = (NMField *)((void *)0);
  locate = nm_locate_field("NM_A_FA_FOLDER",fields);
  while(locate != ((NMField *)((void *)0))){
/* Create a new folder */
    folder = nm_create_folder_from_fields(locate);
/* Add subfolder to roots folder list */
    nm_folder_add_folder_to_list(root,folder);
/* Decrement the ref count */
    nm_release_folder(folder);
/* Find the next folder */
    locate = nm_locate_field("NM_A_FA_FOLDER",(locate + 1));
  }
}

static void _add_contacts(NMUser *user,NMFolder *folder,NMField *fields)
{
  NMContact *contact = (NMContact *)((void *)0);
  NMField *locate = (NMField *)((void *)0);
  NMField *details;
  NMUserRecord *user_record = (NMUserRecord *)((void *)0);
  locate = nm_locate_field("NM_A_FA_CONTACT",fields);
  while(locate != ((NMField *)((void *)0))){
/* Create a new contact from the fields */
    contact = nm_create_contact_from_fields(locate);
/* Add it to our contact list */
    nm_folder_add_contact_to_list(folder,contact);
/* Update the contact cache */
    nm_user_add_contact(user,contact);
/* Update the user record cache */
    if ((details = nm_locate_field("NM_A_FA_USER_DETAILS",((NMField *)(locate -> ptr_value)))) != 0) {
      user_record = nm_find_user_record(user,nm_contact_get_dn(contact));
      if (user_record == ((NMUserRecord *)((void *)0))) {
        user_record = nm_create_user_record_from_fields(details);
        nm_user_record_set_dn(user_record,nm_contact_get_dn(contact));
        nm_user_add_user_record(user,user_record);
        nm_release_user_record(user_record);
      }
      nm_contact_set_user_record(contact,user_record);
    }
    nm_release_contact(contact);
    locate = nm_locate_field("NM_A_FA_CONTACT",(locate + 1));
  }
}
