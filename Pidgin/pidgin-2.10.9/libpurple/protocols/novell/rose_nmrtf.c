/*
 * nmrtf.c
 *
 * Copyright (c) 2004 Novell, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA	02111-1301	USA
 *
 */
/* This code was adapted from the sample RTF reader found here:
 * http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dnrtfspec/html/rtfspec.asp
 */
#include <glib.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <ctype.h>
#include <string.h>
#include "nmrtf.h"
#include "debug.h"
/* Internal RTF parser error codes */
#define NMRTF_OK 0                      /* Everything's fine! */
#define NMRTF_STACK_UNDERFLOW    1       /* Unmatched '}' */
#define NMRTF_STACK_OVERFLOW     2       /* Too many '{' -- memory exhausted */
#define NMRTF_UNMATCHED_BRACE    3       /* RTF ended during an open group. */
#define NMRTF_INVALID_HEX        4       /* invalid hex character found in data */
#define NMRTF_BAD_TABLE          5       /* RTF table (sym or prop) invalid */
#define NMRTF_ASSERTION		     6       /* Assertion failure */
#define NMRTF_EOF		         7       /* End of file reached while reading RTF */
#define NMRTF_CONVERT_ERROR		 8		 /* Error converting text  */
#define NMRTF_MAX_DEPTH 256
typedef enum __unnamed_enum___F0_L47_C9_NMRTF_STATE_NORMAL__COMMA__NMRTF_STATE_SKIP__COMMA__NMRTF_STATE_FONTTABLE__COMMA__NMRTF_STATE_BIN__COMMA__NMRTF_STATE_HEX {NMRTF_STATE_NORMAL,NMRTF_STATE_SKIP,NMRTF_STATE_FONTTABLE,NMRTF_STATE_BIN,NMRTF_STATE_HEX
/* Rtf State */
}NMRtfState;
/* Property types that we care about */
typedef enum  {NMRTF_PROP_FONT_IDX,NMRTF_PROP_FONT_CHARSET,NMRTF_PROP_MAX}NMRtfProperty;
typedef enum  {NMRTF_SPECIAL_BIN,NMRTF_SPECIAL_HEX,NMRTF_SPECIAL_UNICODE,NMRTF_SPECIAL_SKIP}NMRtfSpecialKwd;
typedef enum  {NMRTF_DEST_FONTTABLE,NMRTF_DEST_SKIP}NMRtfDestinationType;
typedef enum __unnamed_enum___F0_L78_C9_NMRTF_KWD_CHAR__COMMA__NMRTF_KWD_DEST__COMMA__NMRTF_KWD_PROP__COMMA__NMRTF_KWD_SPEC {NMRTF_KWD_CHAR,NMRTF_KWD_DEST,NMRTF_KWD_PROP,NMRTF_KWD_SPEC}NMRtfKeywordType;
typedef struct _NMRTFCharProp {
/* All we care about for now is the font.
	 * bold, italic, underline, etc. should be
	 * added here
	 */
int font_idx;
int font_charset;}NMRtfCharProp;
typedef struct _NMRtfStateSave {
NMRtfCharProp chp;
NMRtfState rds;
NMRtfState ris;}NMRtfStateSave;
typedef struct _NMRtfSymbol {
/* RTF keyword */
char *keyword;
/* default value to use */
int default_val;
/* true to use default value from this table */
gboolean pass_default;
/* the type of the keyword */
NMRtfKeywordType kwd_type;
/* property type if the keyword represents a property */
int action;
/* destination type if the keyword represents a destination */
/* character to print if the keyword represents a character */
}NMRtfSymbol;
typedef struct _NMRtfFont {
int number;
char *name;
int charset;}NMRtfFont;
/* RTF Context */

struct _NMRtfContext 
{
/* destination state */
  NMRtfState rds;
/* internal state */
  NMRtfState ris;
/* current character properties (ie. font, bold, italic, etc.) */
  NMRtfCharProp chp;
/* the font table */
  GSList *font_table;
/* saved state stack */
  GSList *saved;
/* numeric parameter for the current keyword */
  int param;
/* number of bytes to skip (after encountering \bin) */
  long bytes_to_skip;
/* how many groups deep are we */
  int depth;
/* if true, skip any unknown destinations (this is set after encountering '\*') */
  gboolean skip_unknown;
/* input string */
  char *input;
/* next char in input */
  char nextch;
/* Temporary ansi text, will be convert/flushed to the output string */
  GString *ansi;
/* The plain text UTF8 string */
  GString *output;
}
;
static int rtf_parse(NMRtfContext *ctx);
static int rtf_push_state(NMRtfContext *ctx);
static int rtf_pop_state(NMRtfContext *ctx);
static NMRtfFont *rtf_get_font(NMRtfContext *ctx,int index);
static int rtf_get_char(NMRtfContext *ctx,guchar *ch);
static int rtf_unget_char(NMRtfContext *ctx,guchar ch);
static int rtf_flush_data(NMRtfContext *ctx);
static int rtf_parse_keyword(NMRtfContext *ctx);
static int rtf_dispatch_control(NMRtfContext *ctx,char *keyword,int param,gboolean param_set);
static int rtf_dispatch_char(NMRtfContext *ctx,guchar ch);
static int rtf_dispatch_unicode_char(NMRtfContext *ctx,gunichar ch);
static int rtf_print_char(NMRtfContext *ctx,guchar ch);
static int rtf_print_unicode_char(NMRtfContext *ctx,gunichar ch);
static int rtf_change_destination(NMRtfContext *ctx,NMRtfDestinationType dest);
static int rtf_dispatch_special(NMRtfContext *ctx,NMRtfSpecialKwd special);
static int rtf_apply_property(NMRtfContext *ctx,NMRtfProperty prop,int val);
/* RTF parser tables */
/* Keyword descriptions */
NMRtfSymbol rtf_symbols[] = {
/* keyword, default, pass_default, keyword_type, action */
{("fonttbl"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_FONTTABLE)}, {("f"), (0), (0), (NMRTF_KWD_PROP), (NMRTF_PROP_FONT_IDX)}, {("fcharset"), (0), (0), (NMRTF_KWD_PROP), (NMRTF_PROP_FONT_CHARSET)}, {("par"), (0), (0), (NMRTF_KWD_CHAR), (0x0a)}, {("line"), (0), (0), (NMRTF_KWD_CHAR), (0x0a)}, {("\000x0a"), (0), (0), (NMRTF_KWD_CHAR), (0x0a)}, {("\000x0d"), (0), (0), (NMRTF_KWD_CHAR), (0x0a)}, {("tab"), (0), (0), (NMRTF_KWD_CHAR), (0x09)}, {("\r"), (0), (0), (NMRTF_KWD_CHAR), ('\r')}, {("\n"), (0), (0), (NMRTF_KWD_CHAR), ('\n')}, {("ldblquote"), (0), (0), (NMRTF_KWD_CHAR), ('"')}, {("rdblquote"), (0), (0), (NMRTF_KWD_CHAR), ('"')}, {("{"), (0), (0), (NMRTF_KWD_CHAR), ('{')}, {("}"), (0), (0), (NMRTF_KWD_CHAR), ('}')}, {("\\"), (0), (0), (NMRTF_KWD_CHAR), ('\\')}, {("bin"), (0), (0), (NMRTF_KWD_SPEC), (NMRTF_SPECIAL_BIN)}, {("*"), (0), (0), (NMRTF_KWD_SPEC), (NMRTF_SPECIAL_SKIP)}, {("\'"), (0), (0), (NMRTF_KWD_SPEC), (NMRTF_SPECIAL_HEX)}, {("u"), (0), (0), (NMRTF_KWD_SPEC), (NMRTF_SPECIAL_UNICODE)}, {("colortbl"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("author"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("buptim"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("comment"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("creatim"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("doccomm"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("footer"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("footerf"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("footerl"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("footerr"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("footnote"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("ftncn"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("ftnsep"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("ftnsepc"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("header"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("headerf"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("headerl"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("headerr"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("info"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("keywords"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("operator"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("pict"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("printim"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("private1"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("revtim"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("rxe"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("stylesheet"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("subject"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("tc"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("title"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("txe"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}, {("xe"), (0), (0), (NMRTF_KWD_DEST), (NMRTF_DEST_SKIP)}};
int table_size = (sizeof(rtf_symbols) / sizeof(NMRtfSymbol ));

NMRtfContext *nm_rtf_init()
{
  NMRtfContext *ctx = (NMRtfContext *)(g_malloc0_n(1,(sizeof(NMRtfContext ))));
  ctx -> nextch = (-1);
  ctx -> ansi = g_string_new("");
  ctx -> output = g_string_new("");
  return ctx;
}

char *nm_rtf_strip_formatting(NMRtfContext *ctx,const char *input)
{
  int status;
  ctx -> input = ((char *)input);
  status = rtf_parse(ctx);
  if (status == 0) 
    return g_strdup(( *(ctx -> output)).str);
  purple_debug_info("novell","RTF parser failed with error code %d\n",status);
  return 0;
}

void nm_rtf_deinit(NMRtfContext *ctx)
{
  GSList *node;
  NMRtfFont *font;
  NMRtfStateSave *save;
  if (ctx != 0) {
    for (node = (ctx -> font_table); node != 0; node = (node -> next)) {
      font = (node -> data);
      g_free((font -> name));
      g_free(font);
      node -> data = ((gpointer )((void *)0));
    }
    g_slist_free((ctx -> font_table));
    for (node = (ctx -> saved); node != 0; node = (node -> next)) {
      save = (node -> data);
      g_free(save);
      node -> data = ((gpointer )((void *)0));
    }
    g_slist_free((ctx -> saved));
    g_string_free((ctx -> ansi),(!0));
    g_string_free((ctx -> output),(!0));
    g_free(ctx);
  }
}

static const char *get_current_encoding(NMRtfContext *ctx)
{
  NMRtfFont *font;
  font = rtf_get_font(ctx,ctx -> chp.font_idx);
  switch(font -> charset){
    case 0:
{
      return "CP1252";
    }
    case 77:
{
      return "MACINTOSH";
    }
    case 78:
{
      return "SJIS";
    }
    case 128:
{
      return "CP932";
    }
    case 129:
{
      return "CP949";
    }
    case 130:
{
      return "CP1361";
    }
    case 134:
{
      return "CP936";
    }
    case 136:
{
      return "CP950";
    }
    case 161:
{
      return "CP1253";
    }
    case 162:
{
      return "CP1254";
    }
    case 163:
{
      return "CP1258";
    }
    case 177:
{
    }
    case 181:
{
      return "CP1255";
    }
    case 178:
{
    }
    case 179:
{
    }
    case 180:
{
      return "CP1256";
    }
    case 186:
{
      return "CP1257";
    }
    case 204:
{
      return "CP1251";
    }
    case 222:
{
      return "CP874";
    }
    case 238:
{
      return "CP1250";
    }
    case 254:
{
      return "CP437";
    }
    default:
{
      purple_debug_info("novell","Unhandled font charset %d\n",(font -> charset));
      return "CP1252";
    }
  }
}
/*
 * Add an entry to the font table
 */

static int rtf_add_font_entry(NMRtfContext *ctx,int number,const char *name,int charset)
{
  NMRtfFont *font = (NMRtfFont *)(g_malloc0_n(1,(sizeof(NMRtfFont ))));
  font -> number = number;
  font -> name = g_strdup(name);
  font -> charset = charset;
  purple_debug_info("novell","Adding font to table: #%d\t%s\t%d\n",(font -> number),(font -> name),(font -> charset));
  ctx -> font_table = g_slist_append((ctx -> font_table),font);
  return 0;
}
/*
 * Return the nth entry in the font table
 */

static NMRtfFont *rtf_get_font(NMRtfContext *ctx,int nth)
{
  NMRtfFont *font;
  font = (g_slist_nth_data((ctx -> font_table),nth));
  return font;
}
/*
 * Step 1:
 * Isolate RTF keywords and send them to rtf_parse_keyword;
 * Push and pop state at the start and end of RTF groups;
 * Send text to rtf_dispatch_char for further processing.
 */

static int rtf_parse(NMRtfContext *ctx)
{
  int status;
  guchar ch;
  guchar hex_byte = 0;
  int hex_count = 2;
  int len;
  if ((ctx -> input) == ((char *)((void *)0))) 
    return 0;
  while(rtf_get_char(ctx,&ch) == 0){
    if ((ctx -> depth) < 0) 
      return 1;
/* if we're parsing binary data, handle it directly */
    if ((ctx -> ris) == NMRTF_STATE_BIN) {
      if ((status = rtf_dispatch_char(ctx,ch)) != 0) 
        return status;
    }
    else {
      switch(ch){
        case '{':
{
          if ((ctx -> depth) > 256) 
            return 2;
          rtf_flush_data(ctx);
          if ((status = rtf_push_state(ctx)) != 0) 
            return status;
          break; 
        }
        case '}':
{
          rtf_flush_data(ctx);
/* for some reason there is always an unwanted '\par' at the end */
          if ((ctx -> rds) == NMRTF_STATE_NORMAL) {
            len = ( *(ctx -> output)).len;
            if (( *(ctx -> output)).str[len - 1] == 10) 
              ctx -> output = g_string_truncate((ctx -> output),(len - 1));
          }
          if ((status = rtf_pop_state(ctx)) != 0) 
            return status;
          if ((ctx -> depth) < 0) 
            return 2;
          break; 
        }
        case '\\':
{
          if ((status = rtf_parse_keyword(ctx)) != 0) 
            return status;
          break; 
        }
/*  cr and lf are noise characters... */
        case 0x0a:
{
        }
        case 0x0d:
{
          break; 
        }
        default:
{
          if ((ctx -> ris) == NMRTF_STATE_NORMAL) {
            if ((status = rtf_dispatch_char(ctx,ch)) != 0) 
              return status;
/* parsing a hex encoded character */
          }
          else {
            if ((ctx -> ris) != NMRTF_STATE_HEX) 
              return 6;
            hex_byte = (hex_byte << 4);
            if ((( *__ctype_b_loc())[(int )ch] & ((unsigned short )_ISdigit)) != 0) 
              hex_byte += (((char )ch) - 48);
            else {
              if ((( *__ctype_b_loc())[(int )ch] & ((unsigned short )_ISlower)) != 0) {
                if ((ch < 'a') || (ch > 'f')) 
                  return 4;
                hex_byte += ((((char )ch) - 'a') + 10);
              }
              else {
                if ((ch < 65) || (ch > 'F')) 
                  return 4;
                hex_byte += ((((char )ch) - 65) + 10);
              }
            }
            hex_count--;
            if (hex_count == 0) {
              if ((status = rtf_dispatch_char(ctx,hex_byte)) != 0) 
                return status;
              hex_count = 2;
              hex_byte = 0;
              ctx -> ris = NMRTF_STATE_NORMAL;
            }
          }
          break; 
        }
      }
    }
  }
  if ((ctx -> depth) < 0) 
    return 2;
  if ((ctx -> depth) > 0) 
    return 3;
  return 0;
}
/*
 * Push the current state onto stack
 */

static int rtf_push_state(NMRtfContext *ctx)
{
  NMRtfStateSave *save = (NMRtfStateSave *)(g_malloc0_n(1,(sizeof(NMRtfStateSave ))));
  save -> chp = (ctx -> chp);
  save -> rds = (ctx -> rds);
  save -> ris = (ctx -> ris);
  ctx -> saved = g_slist_prepend((ctx -> saved),save);
  ctx -> ris = NMRTF_STATE_NORMAL;
  ctx -> depth++;
  return 0;
}
/*
 * Restore the state at the top of the stack
 */

static int rtf_pop_state(NMRtfContext *ctx)
{
  NMRtfStateSave *save_old;
  GSList *link_old;
  if ((ctx -> saved) == ((GSList *)((void *)0))) 
    return 1;
  save_old = ( *(ctx -> saved)).data;
  ctx -> chp = (save_old -> chp);
  ctx -> rds = (save_old -> rds);
  ctx -> ris = (save_old -> ris);
  ctx -> depth--;
  g_free(save_old);
  link_old = (ctx -> saved);
  ctx -> saved = g_slist_remove_link((ctx -> saved),link_old);
  g_slist_free_1(link_old);
  return 0;
}
/*
 * Step 2:
 * Get a control word (and its associated value) and
 * dispatch the control.
 */

static int rtf_parse_keyword(NMRtfContext *ctx)
{
  int status = 0;
  guchar ch;
  gboolean param_set = 0;
  gboolean is_neg = 0;
  int param = 0;
  char keyword[30UL];
  char parameter[15 * sizeof(int ) - 4 * sizeof(void *) - sizeof(size_t )];
  int i;
  keyword[0] = 0;
  parameter[0] = 0;
  if ((status = rtf_get_char(ctx,&ch)) != 0) 
    return status;
  if (!((( *__ctype_b_loc())[(int )ch] & ((unsigned short )_ISalpha)) != 0)) {
/* a control symbol; no delimiter. */
    keyword[0] = ((char )ch);
    keyword[1] = 0;
    return rtf_dispatch_control(ctx,keyword,0,param_set);
  }
/* parse keyword */
  for (i = 0; ((( *__ctype_b_loc())[(int )ch] & ((unsigned short )_ISalpha)) != 0) && (i < sizeof(keyword) - 1); rtf_get_char(ctx,&ch)) {
    keyword[i] = ((char )ch);
    i++;
  }
  keyword[i] = 0;
/* check for '-' indicated a negative parameter value  */
  if (ch == '-') {
    is_neg = (!0);
    if ((status = rtf_get_char(ctx,&ch)) != 0) 
      return status;
  }
/* check for numerical param */
  if ((( *__ctype_b_loc())[(int )ch] & ((unsigned short )_ISdigit)) != 0) {
    param_set = (!0);
    for (i = 0; ((( *__ctype_b_loc())[(int )ch] & ((unsigned short )_ISdigit)) != 0) && (i < sizeof(parameter) - 1); rtf_get_char(ctx,&ch)) {
      parameter[i] = ((char )ch);
      i++;
    }
    parameter[i] = 0;
    ctx -> param = (param = atoi(parameter));
    if (is_neg != 0) 
      ctx -> param = (param = -param);
  }
/* space after control is optional, put character back if it is not a space */
  if (ch != 32) 
    rtf_unget_char(ctx,ch);
  return rtf_dispatch_control(ctx,keyword,param,param_set);
}
/*
 * Route the character to the appropriate destination
 */

static int rtf_dispatch_char(NMRtfContext *ctx,guchar ch)
{
  if (((ctx -> ris) == NMRTF_STATE_BIN) && (--ctx -> bytes_to_skip <= 0)) 
    ctx -> ris = NMRTF_STATE_NORMAL;
  switch(ctx -> rds){
    case NMRTF_STATE_SKIP:
{
      return 0;
    }
    case NMRTF_STATE_NORMAL:
{
      return rtf_print_char(ctx,ch);
    }
    case NMRTF_STATE_FONTTABLE:
{
      if (ch == ';') {
        rtf_add_font_entry(ctx,ctx -> chp.font_idx,( *(ctx -> ansi)).str,ctx -> chp.font_charset);
        g_string_truncate((ctx -> ansi),0);
      }
      else {
        return rtf_print_char(ctx,ch);
      }
      return 0;
    }
    default:
{
      return 0;
    }
  }
}
/* Handle a unicode character */

static int rtf_dispatch_unicode_char(NMRtfContext *ctx,gunichar ch)
{
  switch(ctx -> rds){
    case NMRTF_STATE_SKIP:
{
      return 0;
    }
    case NMRTF_STATE_NORMAL:
{
    }
    case NMRTF_STATE_FONTTABLE:
{
      return rtf_print_unicode_char(ctx,ch);
    }
    default:
{
      return 0;
    }
  }
}
/*
 * Output a character
 */

static int rtf_print_char(NMRtfContext *ctx,guchar ch)
{
  ctx -> ansi = g_string_append_c_inline((ctx -> ansi),ch);
  return 0;
}
/*
 * Output a unicode character
 */

static int rtf_print_unicode_char(NMRtfContext *ctx,gunichar ch)
{
  char buf[7UL];
  int num;
/* convert and flush the ansi buffer to the utf8 buffer */
  rtf_flush_data(ctx);
/* convert the unicode character to utf8 and add directly to the output buffer */
  num = g_unichar_to_utf8(((gunichar )ch),buf);
  buf[num] = 0;
  purple_debug_info("novell","converted unichar 0x%X to utf8 char %s\n",ch,buf);
  ctx -> output = g_string_append((ctx -> output),buf);
  return 0;
}
/*
 * Flush the output text
 */

static int rtf_flush_data(NMRtfContext *ctx)
{
  int status = 0;
  char *conv_data = (char *)((void *)0);
  const char *enc = (const char *)((void *)0);
  GError *gerror = (GError *)((void *)0);
  if (((ctx -> rds) == NMRTF_STATE_NORMAL) && (( *(ctx -> ansi)).len > 0)) {
    enc = get_current_encoding(ctx);
    conv_data = g_convert(( *(ctx -> ansi)).str,( *(ctx -> ansi)).len,"UTF-8",enc,0,0,&gerror);
    if (conv_data != 0) {
      ctx -> output = g_string_append((ctx -> output),conv_data);
      g_free(conv_data);
      ctx -> ansi = g_string_truncate((ctx -> ansi),0);
    }
    else {
      status = 8;
      purple_debug_info("novell","failed to convert data! error code = %d msg = %s\n",(gerror -> code),(gerror -> message));
      g_free(gerror);
    }
  }
  return status;
}
/*
 * Handle a property change
 */

static int rtf_apply_property(NMRtfContext *ctx,NMRtfProperty prop,int val)
{
/* If we're skipping text, */
  if ((ctx -> rds) == NMRTF_STATE_SKIP) 
/* don't do anything. */
    return 0;
/* Need to flush any temporary data before a property change*/
  rtf_flush_data(ctx);
  switch(prop){
    case NMRTF_PROP_FONT_IDX:
{
      ctx -> chp.font_idx = val;
      break; 
    }
    case NMRTF_PROP_FONT_CHARSET:
{
      ctx -> chp.font_charset = val;
      break; 
    }
    default:
{
      return 5;
    }
  }
  return 0;
}
/*
 * Step 3.
 * Search the table for keyword and evaluate it appropriately.
 *
 * Inputs:
 * keyword:   The RTF control to evaluate.
 * param:     The parameter of the RTF control.
 * param_set: TRUE if the control had a parameter; (that is, if param is valid)
 *            FALSE if it did not.
 */

static int rtf_dispatch_control(NMRtfContext *ctx,char *keyword,int param,gboolean param_set)
{
  int idx;
{
    for (idx = 0; idx < table_size; idx++) {
      if (strcmp(keyword,rtf_symbols[idx].keyword) == 0) 
        break; 
    }
  }
  if (idx == table_size) {
    if ((ctx -> skip_unknown) != 0) 
      ctx -> rds = NMRTF_STATE_SKIP;
    ctx -> skip_unknown = 0;
    return 0;
  }
/* found it! use kwd_type and action to determine what to do with it. */
  ctx -> skip_unknown = 0;
  switch(rtf_symbols[idx].kwd_type){
    case NMRTF_KWD_PROP:
{
      if ((rtf_symbols[idx].pass_default != 0) || !(param_set != 0)) 
        param = rtf_symbols[idx].default_val;
      return rtf_apply_property(ctx,rtf_symbols[idx].action,param);
    }
    case NMRTF_KWD_CHAR:
{
      return rtf_dispatch_char(ctx,rtf_symbols[idx].action);
    }
    case NMRTF_KWD_DEST:
{
      return rtf_change_destination(ctx,rtf_symbols[idx].action);
    }
    case NMRTF_KWD_SPEC:
{
      return rtf_dispatch_special(ctx,rtf_symbols[idx].action);
    }
    default:
{
      return 5;
    }
  }
  return 5;
}
/*
 * Change to the destination specified.
 */

static int rtf_change_destination(NMRtfContext *ctx,NMRtfDestinationType type)
{
/* if we're skipping text, don't do anything */
  if ((ctx -> rds) == NMRTF_STATE_SKIP) 
    return 0;
  switch(type){
    case NMRTF_DEST_FONTTABLE:
{
      ctx -> rds = NMRTF_STATE_FONTTABLE;
      g_string_truncate((ctx -> ansi),0);
      break; 
    }
    default:
{
/* when in doubt, skip it... */
      ctx -> rds = NMRTF_STATE_SKIP;
      break; 
    }
  }
  return 0;
}
/*
 * Dispatch an RTF control that needs special processing
 */

static int rtf_dispatch_special(NMRtfContext *ctx,NMRtfSpecialKwd type)
{
  int status = 0;
  guchar ch;
/* if we're skipping, and it's not */
  if (((ctx -> rds) == NMRTF_STATE_SKIP) && (type != NMRTF_SPECIAL_BIN)) 
/* the \bin keyword, ignore it. */
    return 0;
  switch(type){
    case NMRTF_SPECIAL_BIN:
{
      ctx -> ris = NMRTF_STATE_BIN;
      ctx -> bytes_to_skip = (ctx -> param);
      break; 
    }
    case NMRTF_SPECIAL_SKIP:
{
      ctx -> skip_unknown = (!0);
      break; 
    }
    case NMRTF_SPECIAL_HEX:
{
      ctx -> ris = NMRTF_STATE_HEX;
      break; 
    }
    case NMRTF_SPECIAL_UNICODE:
{
      purple_debug_info("novell","parsing unichar\n");
      status = rtf_dispatch_unicode_char(ctx,(ctx -> param));
/* Skip next char */
      if (status == 0) 
        status = rtf_get_char(ctx,&ch);
      break; 
    }
    default:
{
      status = 5;
      break; 
    }
  }
  return status;
}
/*
 * Get the next character from the input stream
 */

static int rtf_get_char(NMRtfContext *ctx,guchar *ch)
{
  if ((ctx -> nextch) >= 0) {
     *ch = (ctx -> nextch);
    ctx -> nextch = (-1);
  }
  else {
     *ch = ( *(ctx -> input));
    ctx -> input++;
  }
  if (( *ch) != 0) 
    return 0;
  else 
    return 7;
}
/*
 * Move a character back into the input stream
 */

static int rtf_unget_char(NMRtfContext *ctx,guchar ch)
{
  ctx -> nextch = ch;
  return 0;
}
