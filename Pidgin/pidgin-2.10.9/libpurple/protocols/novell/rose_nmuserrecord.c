/*
 * nmuserrecord.c
 *
 * Copyright (c) 2004 Novell, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA	02111-1301	USA
 *
 */
#include <glib.h>
#include <string.h>
#include "nmuserrecord.h"
#include "nmfield.h"
#include "nmuser.h"

struct _NMUserRecord 
{
  NMSTATUS_T status;
  char *status_text;
  char *dn;
  char *cn;
  char *display_id;
  char *fname;
  char *lname;
  char *full_name;
  NMField *fields;
  gboolean auth_attr;
  gpointer data;
  int ref_count;
}
;

struct _NMProperty 
{
  char *tag;
  char *value;
}
;
static int count = 0;
/* API functions */

NMUserRecord *nm_create_user_record()
{
  NMUserRecord *user_record = (NMUserRecord *)(g_malloc0_n(1,(sizeof(NMUserRecord ))));
  user_record -> ref_count = 1;
  purple_debug(PURPLE_DEBUG_INFO,"novell","Creating user_record, total=%d\n",count++);
  return user_record;
}

static char *_get_attribute_value(NMField *field)
{
  char *value = (char *)((void *)0);
  if ((field -> ptr_value) == ((void *)((void *)0))) 
    return 0;
  if (((field -> type) == 10) || ((field -> type) == 13)) {
    value = ((char *)(field -> ptr_value));
  }
  else if ((field -> type) == 12) {
/* Need to handle multi-valued returns, for now
		 * just pick the first value and return it
		 */
    NMField *tmp = (NMField *)(field -> ptr_value);
    if ((tmp != ((NMField *)((void *)0))) && (((tmp -> type) == 10) || ((tmp -> type) == 13))) {
      value = ((char *)(tmp -> ptr_value));
    }
    else {
      return 0;
    }
  }
  else {
    return 0;
  }
  return g_strdup(value);
}
/*
 * This creates a user_record for the reference list the
 * field array that is passed in should be a
 * NM_A_FA_USER_DETAILS array.
 */

NMUserRecord *nm_create_user_record_from_fields(NMField *details)
{
  NMUserRecord *user_record;
  NMField *field;
  NMField *fields = details;
  if (details == ((NMField *)((void *)0))) {
    return 0;
  }
  if ((details -> type) == 9) {
    if ((details -> ptr_value) == ((void *)((void *)0))) 
      return 0;
    fields = ((NMField *)(details -> ptr_value));
  }
  user_record = nm_create_user_record();
  if ((field = nm_locate_field("NM_A_SZ_AUTH_ATTRIBUTE",fields)) != 0) {
    if ((field -> ptr_value) != 0) {
      user_record -> display_id = _get_attribute_value(field);
      user_record -> auth_attr = (!0);
    }
  }
  if ((field = nm_locate_field("NM_A_SZ_DN",fields)) != 0) {
    if ((field -> ptr_value) != 0) {
      user_record -> dn = _get_attribute_value(field);
    }
  }
  if ((field = nm_locate_field("CN",fields)) != 0) {
    if ((field -> ptr_value) != 0) {
      user_record -> cn = _get_attribute_value(field);
    }
  }
  if ((field = nm_locate_field("Given Name",fields)) != 0) {
    if ((field -> ptr_value) != 0) {
      user_record -> fname = _get_attribute_value(field);
    }
  }
  if ((field = nm_locate_field("Surname",fields)) != 0) {
    if ((field -> ptr_value) != 0) {
      user_record -> lname = _get_attribute_value(field);
    }
  }
  if ((field = nm_locate_field("Full Name",fields)) != 0) {
    if ((field -> ptr_value) != 0) {
      user_record -> full_name = _get_attribute_value(field);
    }
  }
  if ((field = nm_locate_field("NM_A_SZ_STATUS",fields)) != 0) {
    if ((field -> ptr_value) != 0) 
      user_record -> status = atoi(((char *)(field -> ptr_value)));
  }
  if ((field = nm_locate_field("NM_A_SZ_MESSAGE_BODY",fields)) != 0) {
    if ((field -> ptr_value) != 0) 
      user_record -> status_text = g_strdup(((char *)(field -> ptr_value)));
  }
  user_record -> fields = nm_copy_field_array(fields);
  return user_record;
}

void nm_user_record_copy(NMUserRecord *dest,NMUserRecord *src)
{
  if ((dest == ((NMUserRecord *)((void *)0))) || (src == ((NMUserRecord *)((void *)0)))) 
    return ;
  dest -> status = (src -> status);
/* Copy status text */
  if ((dest -> status_text) != 0) {
    g_free((dest -> status_text));
    dest -> status_text = ((char *)((void *)0));
  }
  if ((src -> status_text) != 0) 
    dest -> status_text = g_strdup((src -> status_text));
/* Copy DN */
  if ((dest -> dn) != 0) {
    g_free((dest -> dn));
    dest -> dn = ((char *)((void *)0));
  }
  if ((src -> dn) != 0) 
    dest -> dn = g_strdup((src -> dn));
/* Copy CN */
  if ((dest -> cn) != 0) {
    g_free((dest -> cn));
    dest -> cn = ((char *)((void *)0));
  }
  if ((src -> cn) != 0) 
    dest -> cn = g_strdup((src -> cn));
/* Copy display id */
  if ((dest -> display_id) != 0) {
    g_free((dest -> display_id));
    dest -> display_id = ((char *)((void *)0));
  }
  if ((src -> display_id) != 0) 
    dest -> display_id = g_strdup((src -> display_id));
/* Copy first name */
  if ((dest -> fname) != 0) {
    g_free((dest -> fname));
    dest -> fname = ((char *)((void *)0));
  }
  if ((src -> fname) != 0) 
    dest -> fname = g_strdup((src -> fname));
/* Copy last name */
  if ((dest -> lname) != 0) {
    g_free((dest -> lname));
    dest -> lname = ((char *)((void *)0));
  }
  if ((src -> lname) != 0) 
    dest -> lname = g_strdup((src -> lname));
/* Copy full name */
  if ((dest -> full_name) != 0) {
    g_free((dest -> full_name));
    dest -> full_name = ((char *)((void *)0));
  }
  if ((src -> full_name) != 0) 
    dest -> full_name = g_strdup((src -> full_name));
/* Copy fields */
  if ((src -> fields) != 0) {
    if ((dest -> fields) != 0) {
      nm_free_fields(&dest -> fields);
    }
    dest -> fields = nm_copy_field_array((src -> fields));
  }
/* Copy data */
  dest -> data = (src -> data);
}

void nm_user_record_add_ref(NMUserRecord *user_record)
{
  if (user_record != 0) 
    user_record -> ref_count++;
}

void nm_release_user_record(NMUserRecord *user_record)
{
  if (--user_record -> ref_count == 0) {
    purple_debug(PURPLE_DEBUG_INFO,"novell","Releasing user_record, total=%d\n",--count);
    if ((user_record -> dn) != 0) {
      g_free((user_record -> dn));
    }
    if ((user_record -> cn) != 0) {
      g_free((user_record -> cn));
    }
    if ((user_record -> display_id) != 0) {
      g_free((user_record -> display_id));
    }
    if ((user_record -> fname) != 0) {
      g_free((user_record -> fname));
    }
    if ((user_record -> lname) != 0) {
      g_free((user_record -> lname));
    }
    if ((user_record -> full_name) != 0) {
      g_free((user_record -> full_name));
    }
    if ((user_record -> status_text) != 0) {
      g_free((user_record -> status_text));
    }
    nm_free_fields(&user_record -> fields);
    g_free(user_record);
  }
}
/* UserRecord API */

NMSTATUS_T nm_user_record_get_status(NMUserRecord *user_record)
{
  if (user_record == ((NMUserRecord *)((void *)0))) 
    return (NMSTATUS_T )(-1);
  return user_record -> status;
}

const char *nm_user_record_get_status_text(NMUserRecord *user_record)
{
  if (user_record == ((NMUserRecord *)((void *)0))) 
    return 0;
  return (user_record -> status_text);
}

void nm_user_record_set_dn(NMUserRecord *user_record,const char *dn)
{
  if ((user_record != ((NMUserRecord *)((void *)0))) && (dn != ((const char *)((void *)0)))) {
    if ((user_record -> dn) != 0) 
      g_free((user_record -> dn));
    user_record -> dn = g_strdup(dn);
  }
}

const char *nm_user_record_get_dn(NMUserRecord *user_record)
{
  if (user_record == ((NMUserRecord *)((void *)0))) 
    return 0;
  return (user_record -> dn);
}

void nm_user_record_set_userid(NMUserRecord *user_record,const char *userid)
{
  if ((user_record != ((NMUserRecord *)((void *)0))) && (userid != ((const char *)((void *)0)))) {
    if ((user_record -> cn) != 0) 
      g_free((user_record -> cn));
    user_record -> cn = g_strdup(userid);
  }
}

const char *nm_user_record_get_userid(NMUserRecord *user_record)
{
  if (user_record == ((NMUserRecord *)((void *)0))) 
    return 0;
  return (user_record -> cn);
}

void nm_user_record_set_display_id(NMUserRecord *user_record,const char *display_id)
{
  if ((user_record != ((NMUserRecord *)((void *)0))) && (display_id != ((const char *)((void *)0)))) {
    if ((user_record -> display_id) != 0) 
      g_free((user_record -> display_id));
    user_record -> display_id = g_strdup(display_id);
  }
}

const char *nm_user_record_get_display_id(NMUserRecord *user_record)
{
  if (user_record == ((NMUserRecord *)((void *)0))) 
    return 0;
  if ((user_record -> display_id) == ((char *)((void *)0))) {
    user_record -> display_id = nm_typed_to_dotted((user_record -> dn));
  }
  return (user_record -> display_id);
}

const char *nm_user_record_get_full_name(NMUserRecord *user_record)
{
  if (user_record == ((NMUserRecord *)((void *)0))) 
    return 0;
  if ((user_record -> full_name) == ((char *)((void *)0))) {
    if (((user_record -> fname) != 0) && ((user_record -> lname) != 0)) {
      user_record -> full_name = g_strdup_printf("%s %s",(user_record -> fname),(user_record -> lname));
    }
  }
  return (user_record -> full_name);
}

const char *nm_user_record_get_first_name(NMUserRecord *user_record)
{
  if (user_record == ((NMUserRecord *)((void *)0))) 
    return 0;
  return (user_record -> fname);
}

const char *nm_user_record_get_last_name(NMUserRecord *user_record)
{
  if (user_record == ((NMUserRecord *)((void *)0))) 
    return 0;
  return (user_record -> lname);
}

gpointer nm_user_record_get_data(NMUserRecord *user_record)
{
  if (user_record == ((NMUserRecord *)((void *)0))) 
    return 0;
  return user_record -> data;
}

void nm_user_record_set_data(NMUserRecord *user_record,gpointer data)
{
  if (user_record == ((NMUserRecord *)((void *)0))) 
    return ;
  user_record -> data = data;
}

void nm_user_record_set_status(NMUserRecord *user_record,int status,const char *text)
{
  if (user_record == ((NMUserRecord *)((void *)0))) 
    return ;
  user_record -> status = status;
  if ((user_record -> status_text) != 0) {
    g_free((user_record -> status_text));
    user_record -> status_text = ((char *)((void *)0));
  }
  if (text != 0) 
    user_record -> status_text = g_strdup(text);
}

gboolean nm_user_record_get_auth_attr(NMUserRecord *user_record)
{
  if (user_record == ((NMUserRecord *)((void *)0))) 
    return 0;
  return user_record -> auth_attr;
}

int nm_user_record_get_property_count(NMUserRecord *user_record)
{
  NMField *locate;
  NMField *fields;
  int count = 0;
  if ((user_record != 0) && ((user_record -> fields) != 0)) {
    locate = nm_locate_field("NM_A_FA_INFO_DISPLAY_ARRAY",((NMField *)(user_record -> fields)));
    if ((locate != 0) && ((fields = ((NMField *)(locate -> ptr_value))) != 0)) {
      count = ((int )(nm_count_fields(fields)));
    }
  }
  return count;
}

NMProperty *nm_user_record_get_property(NMUserRecord *user_record,int index)
{
  NMProperty *property = (NMProperty *)((void *)0);
  NMField *field = (NMField *)((void *)0);
  NMField *fields;
  NMField *locate;
  if ((user_record != 0) && ((user_record -> fields) != 0)) {
    locate = nm_locate_field("NM_A_FA_INFO_DISPLAY_ARRAY",((NMField *)(user_record -> fields)));
    if ((locate != 0) && ((fields = ((NMField *)(locate -> ptr_value))) != 0)) {
      int max = (nm_count_fields(fields));
      if (index < max) {
        if (user_record != 0) {
          field = (fields + index);
          if (((field != 0) && ((field -> tag) != 0)) && ((field -> ptr_value) != 0)) {
            property = ((NMProperty *)(g_malloc0_n(1,(sizeof(NMProperty )))));
            property -> tag = g_strdup((field -> tag));
            property -> value = _get_attribute_value(field);
          }
        }
      }
    }
  }
  return property;
}

void nm_release_property(NMProperty *property)
{
  if (property != 0) {
    if ((property -> tag) != 0) 
      g_free((property -> tag));
    if ((property -> value) != 0) 
      g_free((property -> value));
    g_free(property);
  }
}

const char *nm_property_get_tag(NMProperty *property)
{
  if (property != 0) 
    return (property -> tag);
  else 
    return 0;
}

const char *nm_property_get_value(NMProperty *property)
{
  if (property != 0) 
    return (property -> value);
  else 
    return 0;
}
