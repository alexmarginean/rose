/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301, USA.
 */
#include <glib.h>
#include <stdlib.h>
#include "internal.h"
#include "buddy.h"
#include "account.h"
#include "blist.h"
#include "bonjour.h"
#include "mdns_interface.h"
#include "debug.h"
/**
 * Creates a new buddy.
 */

BonjourBuddy *bonjour_buddy_new(const gchar *name,PurpleAccount *account)
{
  BonjourBuddy *buddy = (BonjourBuddy *)(g_malloc0_n(1,(sizeof(BonjourBuddy ))));
  buddy -> account = account;
  buddy -> name = g_strdup(name);
  _mdns_init_buddy(buddy);
  return buddy;
}
#define _B_CLR(x) g_free(x); x = NULL;

void clear_bonjour_buddy_values(BonjourBuddy *buddy)
{
  g_free((buddy -> first));
  buddy -> first = ((gchar *)((void *)0));
  g_free((buddy -> email));
  buddy -> email = ((gchar *)((void *)0));;
  g_free((buddy -> ext));
  buddy -> ext = ((gchar *)((void *)0));;
  g_free((buddy -> jid));
  buddy -> jid = ((gchar *)((void *)0));;
  g_free((buddy -> last));
  buddy -> last = ((gchar *)((void *)0));;
  g_free((buddy -> msg));
  buddy -> msg = ((gchar *)((void *)0));;
  g_free((buddy -> nick));
  buddy -> nick = ((gchar *)((void *)0));;
  g_free((buddy -> node));
  buddy -> node = ((gchar *)((void *)0));;
  g_free((buddy -> phsh));
  buddy -> phsh = ((gchar *)((void *)0));;
  g_free((buddy -> status));
  buddy -> status = ((gchar *)((void *)0));;
  g_free((buddy -> vc));
  buddy -> vc = ((gchar *)((void *)0));;
  g_free((buddy -> ver));
  buddy -> ver = ((gchar *)((void *)0));;
  g_free((buddy -> AIM));
  buddy -> AIM = ((gchar *)((void *)0));;
}

void set_bonjour_buddy_value(BonjourBuddy *buddy,const char *record_key,const char *value,guint32 len)
{
  gchar **fld = (gchar **)((void *)0);
  do {
    if (record_key != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"record_key != NULL");
      return ;
    };
  }while (0);
  if (!(strcmp(record_key,"1st") != 0)) 
    fld = &buddy -> first;
  else if (!(strcmp(record_key,"email") != 0)) 
    fld = &buddy -> email;
  else if (!(strcmp(record_key,"ext") != 0)) 
    fld = &buddy -> ext;
  else if (!(strcmp(record_key,"jid") != 0)) 
    fld = &buddy -> jid;
  else if (!(strcmp(record_key,"last") != 0)) 
    fld = &buddy -> last;
  else if (!(strcmp(record_key,"msg") != 0)) 
    fld = &buddy -> msg;
  else if (!(strcmp(record_key,"nick") != 0)) 
    fld = &buddy -> nick;
  else if (!(strcmp(record_key,"node") != 0)) 
    fld = &buddy -> node;
  else if (!(strcmp(record_key,"phsh") != 0)) 
    fld = &buddy -> phsh;
  else if (!(strcmp(record_key,"status") != 0)) 
    fld = &buddy -> status;
  else if (!(strcmp(record_key,"vc") != 0)) 
    fld = &buddy -> vc;
  else if (!(strcmp(record_key,"ver") != 0)) 
    fld = &buddy -> ver;
  else if (!(strcmp(record_key,"AIM") != 0)) 
    fld = &buddy -> AIM;
  if (fld == ((gchar **)((void *)0))) 
    return ;
  g_free(( *fld));
   *fld = ((gchar *)((void *)0));
   *fld = g_strndup(value,len);
}
/**
 * Check if all the compulsory buddy data is present.
 */

gboolean bonjour_buddy_check(BonjourBuddy *buddy)
{
  if ((buddy -> account) == ((PurpleAccount *)((void *)0))) 
    return 0;
  if ((buddy -> name) == ((gchar *)((void *)0))) 
    return 0;
  return (!0);
}
/**
 * If the buddy does not yet exist, then create it and add it to
 * our buddy list.  In either case we set the correct status for
 * the buddy.
 */

void bonjour_buddy_add_to_purple(BonjourBuddy *bonjour_buddy,PurpleBuddy *buddy)
{
  PurpleGroup *group;
  PurpleAccount *account = (bonjour_buddy -> account);
  const char *status_id;
  const char *old_hash;
  const char *new_hash;
  const char *name;
/* Translate between the Bonjour status and the Purple status */
  if (((bonjour_buddy -> status) != ((gchar *)((void *)0))) && (g_ascii_strcasecmp("dnd",(bonjour_buddy -> status)) == 0)) 
    status_id = "away";
  else 
    status_id = "available";
/*
	 * TODO: Figure out the idle time by getting the "away"
	 * field from the DNS SD.
	 */
/* Make sure the Bonjour group exists in our buddy list */
/* Use the buddy's domain, instead? */
  group = purple_find_group(((const char *)(dgettext("pidgin","Bonjour"))));
  if (group == ((PurpleGroup *)((void *)0))) {
    group = purple_group_new(((const char *)(dgettext("pidgin","Bonjour"))));
    purple_blist_add_group(group,0);
  }
/* Make sure the buddy exists in our buddy list */
  if (buddy == ((PurpleBuddy *)((void *)0))) 
    buddy = purple_find_buddy(account,(bonjour_buddy -> name));
  if (buddy == ((PurpleBuddy *)((void *)0))) {
    buddy = purple_buddy_new(account,(bonjour_buddy -> name),0);
    purple_blist_node_set_flags(((PurpleBlistNode *)buddy),PURPLE_BLIST_NODE_FLAG_NO_SAVE);
    purple_blist_add_buddy(buddy,0,group,0);
  }
  name = purple_buddy_get_name(buddy);
  purple_buddy_set_protocol_data(buddy,bonjour_buddy);
/* Create the alias for the buddy using the first and the last name */
  if (((bonjour_buddy -> nick) != 0) && (( *(bonjour_buddy -> nick)) != 0)) 
    serv_got_alias(purple_account_get_connection(account),name,(bonjour_buddy -> nick));
  else {
    gchar *alias = (gchar *)((void *)0);
    const char *first;
    const char *last;
    first = (bonjour_buddy -> first);
    last = (bonjour_buddy -> last);
    if (((first != 0) && (( *first) != 0)) || ((last != 0) && (( *last) != 0))) 
      alias = g_strdup_printf("%s%s%s",(((first != 0) && (( *first) != 0))?first : ""),(((((first != 0) && (( *first) != 0)) && (last != 0)) && (( *last) != 0))?" " : ""),(((last != 0) && (( *last) != 0))?last : ""));
    serv_got_alias(purple_account_get_connection(account),name,alias);
    g_free(alias);
  }
/* Set the user's status */
  if ((bonjour_buddy -> msg) != ((gchar *)((void *)0))) 
    purple_prpl_got_user_status(account,name,status_id,"message",(bonjour_buddy -> msg),((void *)((void *)0)));
  else 
    purple_prpl_got_user_status(account,name,status_id,((void *)((void *)0)));
  purple_prpl_got_user_idle(account,name,0,0);
/* TODO: Because we don't save Bonjour buddies in blist.xml,
	 * we will always have to look up the buddy icon at login time.
	 * I think we should figure out a way to do something about this. */
/* Deal with the buddy icon */
  old_hash = purple_buddy_icons_get_checksum_for_user(buddy);
  new_hash = (((((bonjour_buddy -> phsh) != 0) && (( *(bonjour_buddy -> phsh)) != 0))?(bonjour_buddy -> phsh) : ((char *)((void *)0))));
  if ((new_hash != 0) && (!(old_hash != 0) || (strcmp(old_hash,new_hash) != 0))) {
/* Look up the new icon data */
/* TODO: Make sure the hash assigned to the retrieved buddy icon is the same
		 * as what we looked up. */
    bonjour_dns_sd_retrieve_buddy_icon(bonjour_buddy);
  }
  else if (!(new_hash != 0)) 
    purple_buddy_icons_set_for_user(account,name,0,0,0);
}
/**
 * The buddy has signed off Bonjour.
 * If the buddy is being saved, mark as offline, otherwise delete
 */

void bonjour_buddy_signed_off(PurpleBuddy *pb)
{
  if (!(((purple_blist_node_get_flags(((PurpleBlistNode *)pb))) & PURPLE_BLIST_NODE_FLAG_NO_SAVE) != 0U)) {
    purple_prpl_got_user_status(purple_buddy_get_account(pb),purple_buddy_get_name(pb),"offline",((void *)((void *)0)));
    bonjour_buddy_delete((purple_buddy_get_protocol_data(pb)));
    purple_buddy_set_protocol_data(pb,0);
  }
  else {
    purple_account_remove_buddy(purple_buddy_get_account(pb),pb,0);
    purple_blist_remove_buddy(pb);
  }
}
/**
 * We got the buddy icon data; deal with it
 */

void bonjour_buddy_got_buddy_icon(BonjourBuddy *buddy,gconstpointer data,gsize len)
{
/* Recalculate the hash instead of using the current phsh to make sure it is accurate for the icon. */
  char *p;
  char *hash;
  if ((data == ((const void *)((void *)0))) || (len == 0)) 
    return ;
/* Take advantage of the fact that we use a SHA-1 hash of the data as the filename. */
  hash = purple_util_get_image_filename(data,len);
/* Get rid of the extension */
  if (!((p = strchr(hash,'.')) != 0)) {
    g_free(hash);
    return ;
  }
   *p = 0;
  purple_debug_info("bonjour","Got buddy icon for %s icon hash=\'%s\' phsh=\'%s\'.\n",(buddy -> name),hash,(((buddy -> phsh) != 0)?(buddy -> phsh) : "(null)"));
  purple_buddy_icons_set_for_user((buddy -> account),(buddy -> name),g_memdup(data,len),len,hash);
  g_free(hash);
}
/**
 * Deletes a buddy from memory.
 */

void bonjour_buddy_delete(BonjourBuddy *buddy)
{
  g_free((buddy -> name));
  while((buddy -> ips) != ((GSList *)((void *)0))){
    g_free(( *(buddy -> ips)).data);
    buddy -> ips = g_slist_delete_link((buddy -> ips),(buddy -> ips));
  }
  g_free((buddy -> first));
  g_free((buddy -> phsh));
  g_free((buddy -> status));
  g_free((buddy -> email));
  g_free((buddy -> last));
  g_free((buddy -> jid));
  g_free((buddy -> AIM));
  g_free((buddy -> vc));
  g_free((buddy -> msg));
  g_free((buddy -> ext));
  g_free((buddy -> nick));
  g_free((buddy -> node));
  g_free((buddy -> ver));
  bonjour_jabber_close_conversation((buddy -> conversation));
  buddy -> conversation = ((BonjourJabberConversation *)((void *)0));
/* Clean up any mdns implementation data */
  _mdns_delete_buddy(buddy);
  g_free(buddy);
}
