/*
 * purple - Bonjour Jabber XML parser stuff
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include <libxml/parser.h>
#include "connection.h"
#include "debug.h"
#include "jabber.h"
#include "parser.h"
#include "util.h"
#include "xmlnode.h"

static gboolean parse_from_attrib_and_find_buddy(BonjourJabberConversation *bconv,int nb_attributes,const xmlChar **attributes)
{
  int i;
/* If the "from" attribute is specified, attach it to the conversation. */
  for (i = 0; i < (nb_attributes * 5); i += 5) {
    if (!(xmlStrcmp(attributes[i],((xmlChar *)"from")) != 0)) {
      int len = (attributes[i + 4] - attributes[i + 3]);
      bconv -> buddy_name = g_strndup(((char *)attributes[i + 3]),len);
      bonjour_jabber_conv_match_by_name(bconv);
      return (bconv -> pb) != ((PurpleBuddy *)((void *)0));
    }
  }
  return 0;
}

static void bonjour_parser_element_start_libxml(void *user_data,const xmlChar *element_name,const xmlChar *prefix,const xmlChar *namespace,int nb_namespaces,const xmlChar **namespaces,int nb_attributes,int nb_defaulted,const xmlChar **attributes)
{
  BonjourJabberConversation *bconv = user_data;
  xmlnode *node;
  int i;
  do {
    if (element_name != ((const xmlChar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"element_name != NULL");
      return ;
    };
  }while (0);
  if (!(xmlStrcmp(element_name,((xmlChar *)"stream")) != 0)) {
    if (!((bconv -> recv_stream_start) != 0)) {
      bconv -> recv_stream_start = (!0);
      if ((bconv -> pb) == ((PurpleBuddy *)((void *)0))) 
        parse_from_attrib_and_find_buddy(bconv,nb_attributes,attributes);
      bonjour_jabber_stream_started(bconv);
    }
  }
  else {
/* If we haven't yet attached a buddy and this isn't "<stream:features />",
		 * try to get a "from" attribute as a last resort to match our buddy. */
    if ((((bconv -> pb) == ((PurpleBuddy *)((void *)0))) && !(((prefix != 0) && !(xmlStrcmp(prefix,((xmlChar *)"stream")) != 0)) && !(xmlStrcmp(element_name,((xmlChar *)"features")) != 0))) && !(parse_from_attrib_and_find_buddy(bconv,nb_attributes,attributes) != 0)) 
/* We've run out of options for finding who the conversation is from
			   using explicitly specified stuff; see if we can make a good match
			   by using the IP */
      bonjour_jabber_conv_match_by_ip(bconv);
    if ((bconv -> current) != 0) 
      node = xmlnode_new_child((bconv -> current),((const char *)element_name));
    else 
      node = xmlnode_new(((const char *)element_name));
    xmlnode_set_namespace(node,((const char *)namespace));
    for (i = 0; i < (nb_attributes * 5); i += 5) {
      const char *name = (const char *)attributes[i];
      const char *prefix = (const char *)attributes[i + 1];
      const char *attrib_ns = (const char *)attributes[i + 2];
      char *txt;
      int attrib_len = (attributes[i + 4] - attributes[i + 3]);
      char *attrib = (g_malloc((attrib_len + 1)));
      memcpy(attrib,attributes[i + 3],attrib_len);
      attrib[attrib_len] = 0;
      txt = attrib;
      attrib = purple_unescape_text(txt);
      g_free(txt);
      xmlnode_set_attrib_full(node,name,attrib_ns,prefix,attrib);
      g_free(attrib);
    }
    bconv -> current = node;
  }
}

static void bonjour_parser_element_end_libxml(void *user_data,const xmlChar *element_name,const xmlChar *prefix,const xmlChar *namespace)
{
  BonjourJabberConversation *bconv = user_data;
  if (!((bconv -> current) != 0)) {
/* We don't keep a reference to the start stream xmlnode,
		 * so we have to check for it here to close the conversation */
    if (!(xmlStrcmp(element_name,((xmlChar *)"stream")) != 0)) 
/* Asynchronously close the conversation to prevent bonjour_parser_setup()
			 * being called from within this context */
      async_bonjour_jabber_close_conversation(bconv);
    return ;
  }
  if (( *(bconv -> current)).parent != 0) {
    if (!(xmlStrcmp(((xmlChar *)( *(bconv -> current)).name),element_name) != 0)) 
      bconv -> current = ( *(bconv -> current)).parent;
  }
  else {
    xmlnode *packet = (bconv -> current);
    bconv -> current = ((xmlnode *)((void *)0));
    bonjour_jabber_process_packet((bconv -> pb),packet);
    xmlnode_free(packet);
  }
}

static void bonjour_parser_element_text_libxml(void *user_data,const xmlChar *text,int text_len)
{
  BonjourJabberConversation *bconv = user_data;
  if (!((bconv -> current) != 0)) 
    return ;
  if (!(text != 0) || !(text_len != 0)) 
    return ;
  xmlnode_insert_data((bconv -> current),((const char *)text),text_len);
}

static void bonjour_parser_structured_error_handler(void *user_data,xmlErrorPtr error)
{
  BonjourJabberConversation *bconv = user_data;
  purple_debug_error("jabber","XML parser error for BonjourJabberConversation %p: Domain %i, code %i, level %i: %s",bconv,(error -> domain),(error -> code),(error -> level),(((error -> message) != 0)?(error -> message) : "(null)\n"));
}
static xmlSAXHandler bonjour_parser_libxml = {((internalSubsetSAXFunc )((void *)0)), ((isStandaloneSAXFunc )((void *)0)), ((hasInternalSubsetSAXFunc )((void *)0)), ((hasExternalSubsetSAXFunc )((void *)0)), ((resolveEntitySAXFunc )((void *)0)), ((getEntitySAXFunc )((void *)0)), ((entityDeclSAXFunc )((void *)0)), ((notationDeclSAXFunc )((void *)0)), ((attributeDeclSAXFunc )((void *)0)), ((elementDeclSAXFunc )((void *)0)), ((unparsedEntityDeclSAXFunc )((void *)0)), ((setDocumentLocatorSAXFunc )((void *)0)), ((startDocumentSAXFunc )((void *)0)), ((endDocumentSAXFunc )((void *)0)), ((startElementSAXFunc )((void *)0)), ((endElementSAXFunc )((void *)0)), ((referenceSAXFunc )((void *)0)), (bonjour_parser_element_text_libxml), ((ignorableWhitespaceSAXFunc )((void *)0)), ((processingInstructionSAXFunc )((void *)0)), ((commentSAXFunc )((void *)0)), ((warningSAXFunc )((void *)0)), ((errorSAXFunc )((void *)0)), ((fatalErrorSAXFunc )((void *)0)), ((getParameterEntitySAXFunc )((void *)0)), ((cdataBlockSAXFunc )((void *)0)), ((externalSubsetSAXFunc )((void *)0)), (0xDEEDBEAF), ((void *)((void *)0)), (bonjour_parser_element_start_libxml), (bonjour_parser_element_end_libxml), (bonjour_parser_structured_error_handler)
/*internalSubset*/
/*isStandalone*/
/*hasInternalSubset*/
/*hasExternalSubset*/
/*resolveEntity*/
/*getEntity*/
/*entityDecl*/
/*notationDecl*/
/*attributeDecl*/
/*elementDecl*/
/*unparsedEntityDecl*/
/*setDocumentLocator*/
/*startDocument*/
/*endDocument*/
/*startElement*/
/*endElement*/
/*reference*/
/*characters*/
/*ignorableWhitespace*/
/*processingInstruction*/
/*comment*/
/*warning*/
/*error*/
/*fatalError*/
/*getParameterEntity*/
/*cdataBlock*/
/*externalSubset*/
/*initialized*/
/*_private*/
/*startElementNs*/
/*endElementNs*/
/*serror*/
};

void bonjour_parser_setup(BonjourJabberConversation *bconv)
{
/* This seems backwards, but it makes sense. The libxml code creates
	 * the parser context when you try to use it (this way, it can figure
	 * out the encoding at creation time. So, setting up the parser is
	 * just a matter of destroying any current parser. */
  if ((bconv -> context) != 0) {
    xmlParseChunk((bconv -> context),0,0,1);
    xmlFreeParserCtxt((bconv -> context));
    bconv -> context = ((xmlParserCtxt *)((void *)0));
  }
}

void bonjour_parser_process(BonjourJabberConversation *bconv,const char *buf,int len)
{
  if ((bconv -> context) == ((xmlParserCtxt *)((void *)0))) {
/* libxml inconsistently starts parsing on creating the
		 * parser, so do a ParseChunk right afterwards to force it. */
    bconv -> context = xmlCreatePushParserCtxt(&bonjour_parser_libxml,bconv,buf,len,0);
    xmlParseChunk((bconv -> context),"",0,0);
  }
  else if (xmlParseChunk((bconv -> context),buf,len,0) < 0) 
/* TODO: What should we do here - I assume we should display an error or something (maybe just print something to the conv?) */
    purple_debug_error("bonjour","Error parsing xml.\n");
}
