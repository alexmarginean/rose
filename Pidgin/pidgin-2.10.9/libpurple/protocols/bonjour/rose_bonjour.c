/*
 * purple - Bonjour Protocol Plugin
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <glib.h>
#ifndef _WIN32
#include <pwd.h>
#else
#define UNICODE
#include <winsock2.h>
#include <windows.h>
#include <lm.h>
#include "dns_sd_proxy.h"
#endif
#include "internal.h"
#include "account.h"
#include "accountopt.h"
#include "debug.h"
#include "util.h"
#include "version.h"
#include "bonjour.h"
#include "mdns_common.h"
#include "jabber.h"
#include "buddy.h"
#include "bonjour_ft.h"
static char *default_firstname;
static char *default_lastname;

const char *bonjour_get_jid(PurpleAccount *account)
{
  PurpleConnection *conn = purple_account_get_connection(account);
  BonjourData *bd = (conn -> proto_data);
  return (bd -> jid);
}

static void bonjour_removeallfromlocal(PurpleConnection *conn,PurpleGroup *bonjour_group)
{
  PurpleAccount *account = purple_connection_get_account(conn);
  PurpleBlistNode *cnode;
  PurpleBlistNode *cnodenext;
  PurpleBlistNode *bnode;
  PurpleBlistNode *bnodenext;
  PurpleBuddy *buddy;
  if (bonjour_group == ((PurpleGroup *)((void *)0))) 
    return ;
/* Go through and remove all buddies that belong to this account */
  for (cnode = purple_blist_node_get_first_child(((PurpleBlistNode *)bonjour_group)); cnode != 0; cnode = cnodenext) {
    cnodenext = purple_blist_node_get_sibling_next(cnode);
    if (!((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CONTACT_NODE)) 
      continue; 
    for (bnode = purple_blist_node_get_first_child(cnode); bnode != 0; bnode = bnodenext) {
      bnodenext = purple_blist_node_get_sibling_next(bnode);
      if (!((purple_blist_node_get_type(bnode)) == PURPLE_BLIST_BUDDY_NODE)) 
        continue; 
      buddy = ((PurpleBuddy *)bnode);
      if (purple_buddy_get_account(buddy) != account) 
        continue; 
      purple_account_remove_buddy(account,buddy,0);
      purple_blist_remove_buddy(buddy);
    }
  }
}

static void bonjour_login(PurpleAccount *account)
{
  PurpleConnection *gc = purple_account_get_connection(account);
  BonjourData *bd;
  PurpleStatus *status;
  PurplePresence *presence;
#ifdef _WIN32
#endif /* _WIN32 */
  gc -> flags |= PURPLE_CONNECTION_HTML;
  gc -> proto_data = (bd = ((BonjourData *)(g_malloc0_n(1,(sizeof(BonjourData ))))));
/* Start waiting for jabber connections (iChat style) */
  bd -> jabber_data = ((BonjourJabber *)(g_malloc0_n(1,(sizeof(BonjourJabber )))));
  ( *(bd -> jabber_data)).socket = (-1);
  ( *(bd -> jabber_data)).socket6 = (-1);
  ( *(bd -> jabber_data)).port = purple_account_get_int(account,"port",5298);
  ( *(bd -> jabber_data)).account = account;
  if (bonjour_jabber_start((bd -> jabber_data)) == -1) {
/* Send a message about the connection error */
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Unable to listen for incoming IM connections"))));
    return ;
  }
/* Connect to the mDNS daemon looking for buddies in the LAN */
  bd -> dns_sd_data = bonjour_dns_sd_new();
  ( *(bd -> dns_sd_data)).first = g_strdup(purple_account_get_string(account,"first",default_firstname));
  ( *(bd -> dns_sd_data)).last = g_strdup(purple_account_get_string(account,"last",default_lastname));
  ( *(bd -> dns_sd_data)).port_p2pj = ( *(bd -> jabber_data)).port;
/* Not engaged in AV conference */
  ( *(bd -> dns_sd_data)).vc = g_strdup("!");
  status = purple_account_get_active_status(account);
  presence = purple_account_get_presence(account);
  if (purple_presence_is_available(presence) != 0) 
    ( *(bd -> dns_sd_data)).status = g_strdup("avail");
  else if (purple_presence_is_idle(presence) != 0) 
    ( *(bd -> dns_sd_data)).status = g_strdup("away");
  else 
    ( *(bd -> dns_sd_data)).status = g_strdup("dnd");
  ( *(bd -> dns_sd_data)).msg = g_strdup(purple_status_get_attr_string(status,"message"));
  ( *(bd -> dns_sd_data)).account = account;
  if (!(bonjour_dns_sd_start((bd -> dns_sd_data)) != 0)) {
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Unable to establish connection with the local mDNS server.  Is it running\?"))));
    return ;
  }
  bonjour_dns_sd_update_buddy_icon((bd -> dns_sd_data));
/* Show the buddy list by telling Purple we have already connected */
  purple_connection_set_state(gc,PURPLE_CONNECTED);
}

static void bonjour_close(PurpleConnection *connection)
{
  PurpleGroup *bonjour_group;
  BonjourData *bd = (connection -> proto_data);
  bonjour_group = purple_find_group(((const char *)(dgettext("pidgin","Bonjour"))));
/* Remove all the bonjour buddies */
  bonjour_removeallfromlocal(connection,bonjour_group);
/* Stop looking for buddies in the LAN */
  if ((bd != ((BonjourData *)((void *)0))) && ((bd -> dns_sd_data) != ((BonjourDnsSd *)((void *)0)))) {
    bonjour_dns_sd_stop((bd -> dns_sd_data));
    bonjour_dns_sd_free((bd -> dns_sd_data));
  }
  if ((bd != ((BonjourData *)((void *)0))) && ((bd -> jabber_data) != ((BonjourJabber *)((void *)0)))) {
/* Stop waiting for conversations */
    bonjour_jabber_stop((bd -> jabber_data));
    g_free((bd -> jabber_data));
  }
/* Delete the bonjour group
	 * (purple_blist_remove_group will bail out if the group isn't empty)
	 */
  if (bonjour_group != ((PurpleGroup *)((void *)0))) 
    purple_blist_remove_group(bonjour_group);
/* Cancel any file transfers */
  while((bd != ((BonjourData *)((void *)0))) && ((bd -> xfer_lists) != 0)){
    purple_xfer_cancel_local(( *(bd -> xfer_lists)).data);
  }
  if (bd != ((BonjourData *)((void *)0))) 
    g_free((bd -> jid));
  g_free(bd);
  connection -> proto_data = ((void *)((void *)0));
}

static const char *bonjour_list_icon(PurpleAccount *account,PurpleBuddy *buddy)
{
  return "bonjour";
}

static int bonjour_send_im(PurpleConnection *connection,const char *to,const char *msg,PurpleMessageFlags flags)
{
  if (!(to != 0) || !(msg != 0)) 
    return 0;
  return bonjour_jabber_send_message(( *((BonjourData *)(connection -> proto_data))).jabber_data,to,msg);
}

static void bonjour_set_status(PurpleAccount *account,PurpleStatus *status)
{
  PurpleConnection *gc;
  BonjourData *bd;
  PurplePresence *presence;
  const char *message;
  const char *bonjour_status;
  gchar *stripped;
  gc = purple_account_get_connection(account);
  bd = (gc -> proto_data);
  presence = purple_account_get_presence(account);
  message = purple_status_get_attr_string(status,"message");
  if (message == ((const char *)((void *)0))) 
    message = "";
  stripped = purple_markup_strip_html(message);
/*
	 * The three possible status for Bonjour are
	 *   -available ("avail")
	 *   -idle ("away")
	 *   -away ("dnd")
	 * Each of them can have an optional message.
	 */
  if (purple_presence_is_available(presence) != 0) 
    bonjour_status = "avail";
  else if (purple_presence_is_idle(presence) != 0) 
    bonjour_status = "away";
  else 
    bonjour_status = "dnd";
  bonjour_dns_sd_send_status((bd -> dns_sd_data),bonjour_status,stripped);
  g_free(stripped);
}
/*
 * The add_buddy callback removes the buddy from the local list.
 * Bonjour manages buddies for you, and adding someone locally by
 * hand is stupid.  Perhaps we should change libpurple not to allow adding
 * if there is no add_buddy callback.
 */

static void bonjour_fake_add_buddy(PurpleConnection *pc,PurpleBuddy *buddy,PurpleGroup *group)
{
  purple_debug_error("bonjour","Buddy \'%s\' manually added; removing.  Bonjour buddies must be discovered and not manually added.\n",purple_buddy_get_name(buddy));
/* I suppose we could alert the user here, but it seems unnecessary. */
/* If this causes problems, it can be moved to an idle callback */
  purple_blist_remove_buddy(buddy);
}

static void bonjour_remove_buddy(PurpleConnection *pc,PurpleBuddy *buddy,PurpleGroup *group)
{
  BonjourBuddy *bb = (purple_buddy_get_protocol_data(buddy));
  if (bb != 0) {
    bonjour_buddy_delete(bb);
    purple_buddy_set_protocol_data(buddy,0);
  }
}

static GList *bonjour_status_types(PurpleAccount *account)
{
  GList *status_types = (GList *)((void *)0);
  PurpleStatusType *type;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_AVAILABLE,"available",0,(!0),(!0),0,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  status_types = g_list_append(status_types,type);
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_AWAY,"away",0,(!0),(!0),0,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  status_types = g_list_append(status_types,type);
  type = purple_status_type_new_full(PURPLE_STATUS_OFFLINE,"offline",0,(!0),(!0),0);
  status_types = g_list_append(status_types,type);
  return status_types;
}

static void bonjour_convo_closed(PurpleConnection *connection,const char *who)
{
  PurpleBuddy *buddy = purple_find_buddy((connection -> account),who);
  BonjourBuddy *bb;
  if ((buddy == ((PurpleBuddy *)((void *)0))) || ((bb = (purple_buddy_get_protocol_data(buddy))) == ((BonjourBuddy *)((void *)0)))) {
/*
		 * This buddy is not in our buddy list, and therefore does not really
		 * exist, so we won't have any data about them.
		 */
    return ;
  }
  bonjour_jabber_close_conversation((bb -> conversation));
  bb -> conversation = ((BonjourJabberConversation *)((void *)0));
}

static void bonjour_set_buddy_icon(PurpleConnection *conn,PurpleStoredImage *img)
{
  BonjourData *bd = (conn -> proto_data);
  bonjour_dns_sd_update_buddy_icon((bd -> dns_sd_data));
}

static char *bonjour_status_text(PurpleBuddy *buddy)
{
  const PurplePresence *presence;
  const PurpleStatus *status;
  const char *message;
  gchar *ret = (gchar *)((void *)0);
  presence = (purple_buddy_get_presence(buddy));
  status = (purple_presence_get_active_status(presence));
  message = purple_status_get_attr_string(status,"message");
  if (message != ((const char *)((void *)0))) {
    ret = g_markup_escape_text(message,(-1));
    purple_util_chrreplace(ret,10,32);
  }
  return ret;
}

static void bonjour_tooltip_text(PurpleBuddy *buddy,PurpleNotifyUserInfo *user_info,gboolean full)
{
  PurplePresence *presence;
  PurpleStatus *status;
  BonjourBuddy *bb = (purple_buddy_get_protocol_data(buddy));
  const char *status_description;
  const char *message;
  presence = purple_buddy_get_presence(buddy);
  status = purple_presence_get_active_status(presence);
  message = purple_status_get_attr_string(status,"message");
  if (purple_presence_is_available(presence) != 0) 
    status_description = purple_status_get_name(status);
  else if (purple_presence_is_idle(presence) != 0) 
    status_description = ((const char *)(dgettext("pidgin","Idle")));
  else 
    status_description = purple_status_get_name(status);
  purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Status"))),status_description);
  if (message != ((const char *)((void *)0))) 
    purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Message"))),message);
  if (bb == ((BonjourBuddy *)((void *)0))) {
    purple_debug_error("bonjour","Got tooltip request for a buddy without protocol data.\n");
    return ;
  }
/* Only show first/last name if there is a nickname set (to avoid duplication) */
  if (((bb -> nick) != ((gchar *)((void *)0))) && (( *(bb -> nick)) != 0)) {
    if (((bb -> first) != ((gchar *)((void *)0))) && (( *(bb -> first)) != 0)) 
      purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","First name"))),(bb -> first));
    if (((bb -> last) != ((gchar *)((void *)0))) && (( *(bb -> last)) != 0)) 
      purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Last name"))),(bb -> last));
  }
  if (((bb -> email) != ((gchar *)((void *)0))) && (( *(bb -> email)) != 0)) 
    purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Email"))),(bb -> email));
  if (((bb -> AIM) != ((gchar *)((void *)0))) && (( *(bb -> AIM)) != 0)) 
    purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","AIM Account"))),(bb -> AIM));
  if (((bb -> jid) != ((gchar *)((void *)0))) && (( *(bb -> jid)) != 0)) 
    purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","XMPP Account"))),(bb -> jid));
}

static void bonjour_do_group_change(PurpleBuddy *buddy,const char *new_group)
{
  PurpleBlistNodeFlags oldflags;
  if (buddy == ((PurpleBuddy *)((void *)0))) 
    return ;
  oldflags = purple_blist_node_get_flags(((PurpleBlistNode *)buddy));
/* If we're moving them out of the bonjour group, make them persistent */
  if (purple_strequal(new_group,((const char *)(dgettext("pidgin","Bonjour")))) != 0) 
    purple_blist_node_set_flags(((PurpleBlistNode *)buddy),(oldflags | PURPLE_BLIST_NODE_FLAG_NO_SAVE));
  else 
    purple_blist_node_set_flags(((PurpleBlistNode *)buddy),(oldflags ^ PURPLE_BLIST_NODE_FLAG_NO_SAVE));
}

static void bonjour_group_buddy(PurpleConnection *connection,const char *who,const char *old_group,const char *new_group)
{
  PurpleBuddy *buddy = purple_find_buddy((connection -> account),who);
  bonjour_do_group_change(buddy,new_group);
}

static void bonjour_rename_group(PurpleConnection *connection,const char *old_name,PurpleGroup *group,GList *moved_buddies)
{
  GList *cur;
  const char *new_group;
  PurpleBuddy *buddy;
  new_group = purple_group_get_name(group);
  for (cur = moved_buddies; cur != 0; cur = (cur -> next)) {
    buddy = (cur -> data);
    bonjour_do_group_change(buddy,new_group);
  }
}

static gboolean bonjour_can_receive_file(PurpleConnection *connection,const char *who)
{
  PurpleBuddy *buddy = purple_find_buddy((connection -> account),who);
  return (buddy != ((PurpleBuddy *)((void *)0))) && (purple_buddy_get_protocol_data(buddy) != ((void *)((void *)0)));
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
/* These shouldn't happen here because they are allocated in _init() */
  g_free(default_firstname);
  g_free(default_lastname);
  return (!0);
}
static PurplePlugin *my_protocol = (PurplePlugin *)((void *)0);
static PurplePluginProtocolInfo prpl_info = {(OPT_PROTO_NO_PASSWORD), ((GList *)((void *)0)), ((GList *)((void *)0)), 
/* user_splits */
/* protocol_options */
/* icon_spec */
{("png,gif,jpeg"), (0), (0), (96), (96), (65535), (PURPLE_ICON_SCALE_DISPLAY)}, (bonjour_list_icon), ((const char *(*)(PurpleBuddy *))((void *)0)), (bonjour_status_text), (bonjour_tooltip_text), (bonjour_status_types), ((GList *(*)(PurpleBlistNode *))((void *)0)), ((GList *(*)(PurpleConnection *))((void *)0)), ((GHashTable *(*)(PurpleConnection *, const char *))((void *)0)), (bonjour_login), (bonjour_close), (bonjour_send_im), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((unsigned int (*)(PurpleConnection *, const char *, PurpleTypingState ))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), (bonjour_set_status), ((void (*)(PurpleConnection *, int ))((void *)0)), ((void (*)(PurpleConnection *, const char *, const char *))((void *)0)), (bonjour_fake_add_buddy), ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)), (bonjour_remove_buddy), ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *))((void *)0)), ((void (*)(PurpleConnection *, GHashTable *))((void *)0)), ((void (*)(PurpleConnection *, GHashTable *))((void *)0)), ((char *(*)(GHashTable *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *, const char *))((void *)0)), ((void (*)(PurpleConnection *, int ))((void *)0)), ((void (*)(PurpleConnection *, int , const char *, const char *))((void *)0)), ((int (*)(PurpleConnection *, int , const char *, PurpleMessageFlags ))((void *)0)), ((void (*)(PurpleConnection *))((void *)0)), ((void (*)(PurpleAccount *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *, const char *))((void *)0)), (bonjour_group_buddy), (bonjour_rename_group), ((void (*)(PurpleBuddy *))((void *)0)), (bonjour_convo_closed), ((const char *(*)(const PurpleAccount *, const char *))((void *)0)), (bonjour_set_buddy_icon), ((void (*)(PurpleConnection *, PurpleGroup *))((void *)0)), ((char *(*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((PurpleChat *(*)(PurpleAccount *, const char *))((void *)0)), ((PurpleRoomlist *(*)(PurpleConnection *))((void *)0)), ((void (*)(PurpleRoomlist *))((void *)0)), ((void (*)(PurpleRoomlist *, PurpleRoomlistRoom *))((void *)0)), (bonjour_can_receive_file), (bonjour_send_file), (bonjour_new_xfer), ((gboolean (*)(const PurpleBuddy *))((void *)0)), ((PurpleWhiteboardPrplOps *)((void *)0)), ((int (*)(PurpleConnection *, const char *, int ))((void *)0)), ((char *(*)(PurpleRoomlistRoom *))((void *)0)), ((void (*)(PurpleAccount *, PurpleAccountUnregistrationCb , void *))((void *)0)), ((gboolean (*)(PurpleConnection *, const char *, guint ))((void *)0)), ((GList *(*)(PurpleAccount *))((void *)0)), ((sizeof(PurplePluginProtocolInfo ))), ((GHashTable *(*)(PurpleAccount *))((void *)0)), ((gboolean (*)(PurpleAccount *, const char *, PurpleMediaSessionType ))((void *)0)), ((PurpleMediaCaps (*)(PurpleAccount *, const char *))((void *)0)), ((PurpleMood *(*)(PurpleAccount *))((void *)0)), ((void (*)(PurpleConnection *, const char *, PurpleSetPublicAliasSuccessCallback , PurpleSetPublicAliasFailureCallback ))((void *)0)), ((void (*)(PurpleConnection *, PurpleGetPublicAliasSuccessCallback , PurpleGetPublicAliasFailureCallback ))((void *)0)), ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *, const char *))((void *)0)), ((void (*)(PurpleConnection *, GList *, GList *, const char *))((void *)0))
/* list_icon */
/* list_emblem */
/* status_text */
/* tooltip_text */
/* status_types */
/* blist_node_menu */
/* chat_info */
/* chat_info_defaults */
/* login */
/* close */
/* send_im */
/* set_info */
/* send_typing */
/* get_info */
/* set_status */
/* set_idle */
/* change_passwd */
/* add_buddy */
/* add_buddies */
/* remove_buddy */
/* remove_buddies */
/* add_permit */
/* add_deny */
/* rem_permit */
/* rem_deny */
/* set_permit_deny */
/* join_chat */
/* reject_chat */
/* get_chat_name */
/* chat_invite */
/* chat_leave */
/* chat_whisper */
/* chat_send */
/* keepalive */
/* register_user */
/* get_cb_info */
/* get_cb_away */
/* alias_buddy */
/* group_buddy */
/* rename_group */
/* buddy_free */
/* convo_closed */
/* normalize */
/* set_buddy_icon */
/* remove_group */
/* get_cb_real_name */
/* set_chat_topic */
/* find_blist_chat */
/* roomlist_get_list */
/* roomlist_cancel */
/* roomlist_expand_category */
/* can_receive_file */
/* send_file */
/* new_xfer */
/* offline_message */
/* whiteboard_prpl_ops */
/* send_raw */
/* roomlist_room_serialize */
/* unregister_user */
/* send_attention */
/* get_attention_types */
/* struct_size */
/* get_account_text_table */
/* initiate_media */
/* get_media_caps */
/* get_moods */
/* set_public_alias */
/* get_public_alias */
/* add_buddy_with_invite */
/* add_buddies_with_invite */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_PROTOCOL), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("prpl-bonjour"), ("Bonjour"), ("2.10.9"), ("Bonjour Protocol Plugin"), ("Bonjour Protocol Plugin"), ((char *)((void *)0)), ("http://pidgin.im/"), ((gboolean (*)(PurplePlugin *))((void *)0)), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((&prpl_info)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/**< prefs_info     */
/* padding */
};
#ifdef WIN32
/* Split the real name into a first and last name */
/* purple_debug_info("bonjour", "Looking up the full name from the %s.\n", (servername ? "domain controller" : "local machine")); */
/* Fall back to the local machine if we didn't get the full name from the domain controller */
/* purple_debug_info("bonjour", "Looking up the full name from the local machine"); */
#endif

static void initialize_default_account_values()
{
#ifndef _WIN32
  struct passwd *info;
#endif
  const char *fullname = (const char *)((void *)0);
  const char *splitpoint;
  const char *tmp;
  gchar *conv = (gchar *)((void *)0);
#ifndef _WIN32
/* Try to figure out the user's real name */
  info = getpwuid(getuid());
  if (((info != ((struct passwd *)((void *)0))) && ((info -> pw_gecos) != ((char *)((void *)0)))) && ((info -> pw_gecos)[0] != 0)) 
    fullname = (info -> pw_gecos);
  else if (((info != ((struct passwd *)((void *)0))) && ((info -> pw_name) != ((char *)((void *)0)))) && ((info -> pw_name)[0] != 0)) 
    fullname = (info -> pw_name);
  else if (((fullname = (getlogin())) != ((const char *)((void *)0))) && (fullname[0] == 0)) 
    fullname = ((const char *)((void *)0));
#else
/* The Win32 username lookup functions are synchronous so we do it in a thread */
#endif
/* Make sure fullname is valid UTF-8.  If not, try to convert it. */
  if ((fullname != ((const char *)((void *)0))) && !(g_utf8_validate(fullname,(-1),0) != 0)) {
    fullname = (conv = g_locale_to_utf8(fullname,(-1),0,0,0));
    if ((conv == ((gchar *)((void *)0))) || (( *conv) == 0)) 
      fullname = ((const char *)((void *)0));
  }
  if (fullname == ((const char *)((void *)0))) 
    fullname = ((const char *)(dgettext("pidgin","Purple Person")));
/* Split the real name into a first and last name */
  splitpoint = (strchr(fullname,32));
  if (splitpoint != ((const char *)((void *)0))) {
    default_firstname = g_strndup(fullname,(splitpoint - fullname));
    tmp = (splitpoint + 1);
/* The last name may be followed by a comma and additional data.
		 * Only use the last name itself.
		 */
    splitpoint = (strchr(tmp,','));
    if (splitpoint != ((const char *)((void *)0))) 
      default_lastname = g_strndup(tmp,(splitpoint - tmp));
    else 
      default_lastname = g_strdup(tmp);
  }
  else {
    default_firstname = g_strdup(fullname);
    default_lastname = g_strdup("");
  }
  g_free(conv);
}

static void init_plugin(PurplePlugin *plugin)
{
  PurpleAccountOption *option;
  initialize_default_account_values();
/* Creating the options for the protocol */
  option = purple_account_option_int_new(((const char *)(dgettext("pidgin","Local Port"))),"port",5298);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","First name"))),"first",default_firstname);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","Last name"))),"last",default_lastname);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","Email"))),"email","");
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","AIM Account"))),"AIM","");
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","XMPP Account"))),"jid","");
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  my_protocol = plugin;
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
