/* This file is part of the Project Athena Zephyr Notification System.
 * It contains the ZCheckIfNotice/select loop used for waiting for
 * a notice, with a timeout.
 *
 *	Copyright (c) 1991 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"
#ifdef WIN32
#include <winsock2.h>
#ifndef ZEPHYR_USES_KERBEROS
/*time since 1 Jan 1601 in 100ns units */
#endif
#else
#include <sys/socket.h>
#endif

Code_t Z_WaitForNotice(notice,pred,arg,timeout)
ZNotice_t *notice;
int (*pred)(ZNotice_t *, void *);
void *arg;
int timeout;
{
  Code_t retval;
  struct timeval tv;
  struct timeval t0;
  fd_set fdmask;
  int i;
  int fd;
  retval = ZCheckIfNotice(notice,0,pred,((char *)arg));
  if (retval == 0) 
    return 0;
  if (retval != -772103672L) 
    return retval;
  fd = __Zephyr_fd;
  do {
    int __d0;
    int __d1;
    asm volatile ("cld; rep; stosq" : "=c" (__d0), "=D" (__d1) : "a" (0), "0" ((sizeof(fd_set ) / sizeof(__fd_mask ))), "1" ((fdmask.__fds_bits + 0)) : "memory");
  }while (0);
  tv.tv_sec = timeout;
  tv.tv_usec = 0;
  gettimeofday(&t0,0);
  t0.tv_sec += timeout;
  while(1){
    fdmask.__fds_bits[fd / (8 * ((int )(sizeof(__fd_mask ))))] |= (((__fd_mask )1) << (fd % (8 * ((int )(sizeof(__fd_mask ))))));
    i = select((fd + 1),&fdmask,0,0,&tv);
    if (i == 0) 
      return 110;
    if ((i < 0) && ( *__errno_location() != 4)) 
      return  *__errno_location();
    if (i > 0) {
      retval = ZCheckIfNotice(notice,0,pred,((char *)arg));
/* includes ZERR_NONE */
      if (retval != -772103672L) 
        return retval;
    }
    gettimeofday(&tv,0);
    tv.tv_usec = (t0.tv_usec - tv.tv_usec);
    if (tv.tv_usec < 0) {
      tv.tv_usec += 1000000;
      tv.tv_sec = ((t0.tv_sec - tv.tv_sec) - 1);
    }
    else 
      tv.tv_sec = (t0.tv_sec - tv.tv_sec);
  }
/*NOTREACHED*/
}
