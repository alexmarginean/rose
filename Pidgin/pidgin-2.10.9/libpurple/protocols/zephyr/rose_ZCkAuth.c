/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for the ZCheckAuthentication function.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987,1991 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"
/* Check authentication of the notice.
   If it looks authentic but fails the Kerberos check, return -1.
   If it looks authentic and passes the Kerberos check, return 1.
   If it doesn't look authentic, return 0
   When not using Kerberos, return true if the notice claims to be authentic.
   Only used by clients; the server uses its own routine.
 */

Code_t ZCheckAuthentication(notice,from)
ZNotice_t *notice;
struct sockaddr_in *from;
{
#ifdef ZEPHYR_USES_KERBEROS
/* If the value is already known, return it. */
#ifdef NOENCRYPTION
#else
#endif
/* if mismatched checksum, then the packet was corrupted */
#else
  return ((notice -> z_auth) != 0)?1 : 0;
#endif
}
