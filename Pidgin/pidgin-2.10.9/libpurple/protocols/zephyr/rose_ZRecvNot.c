/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for ZReceiveNotice function.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987,1988 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"

Code_t ZReceiveNotice(notice,from)
ZNotice_t *notice;
struct sockaddr_in *from;
{
  char *buffer;
  struct _Z_InputQ *nextq;
  int len;
  int auth;
  Code_t retval;
  if ((retval = Z_WaitForComplete()) != 0) 
    return retval;
  nextq = Z_GetFirstComplete();
  if (!(nextq != 0)) 
    return 12;
  len = (nextq -> packet_len);
  if (!((buffer = ((char *)(malloc(((unsigned int )len))))) != 0)) 
    return 12;
  if (from != 0) 
     *from = (nextq -> from);
  memcpy(buffer,(nextq -> packet),len);
  auth = (nextq -> auth);
  Z_RemQueue(nextq);
  if ((retval = ZParseNotice(buffer,len,notice)) != 0) 
    return retval;
  notice -> z_checked_auth = auth;
  return 0;
}
