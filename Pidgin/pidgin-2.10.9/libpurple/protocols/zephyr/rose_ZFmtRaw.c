/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for the ZFormatRawNotice function.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"

Code_t ZFormatRawNotice(notice,buffer,ret_len)
register ZNotice_t *notice;
char **buffer;
int *ret_len;
{
  char header[800UL];
  int hdrlen;
  Code_t retval;
  if ((retval = Z_FormatRawHeader(notice,header,(sizeof(header)),&hdrlen,0,0)) != 0) 
    return retval;
   *ret_len = (hdrlen + (notice -> z_message_len));
/* *ret_len is never 0, don't have to worry about malloc(0) */
  if (!(( *buffer = ((char *)(malloc(((unsigned int )( *ret_len)))))) != 0)) 
    return 12;
  memcpy(( *buffer),header,hdrlen);
  memcpy(( *buffer + hdrlen),(notice -> z_message),(notice -> z_message_len));
  return 0;
}
