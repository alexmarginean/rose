/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for the ZGetSender.c function.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987, 1991 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"
#ifndef WIN32
#include <pwd.h>
#endif

char *ZGetSender()
{
  struct passwd *pw;
#ifdef ZEPHYR_USES_KERBEROS
#else
  static char sender[128UL] = "";
#endif
#ifdef WIN32
#endif
#ifdef ZEPHYR_USES_KERBEROS
#endif
#ifdef WIN32
#else
/* XXX a uid_t is a u_short (now),  but getpwuid
	 * wants an int. AARGH! */
  pw = getpwuid(((int )(getuid())));
  if (!(pw != 0)) 
    return "unknown";
  sprintf(sender,"%s@%s",(pw -> pw_name),__Zephyr_realm);
#endif
  return sender;
}
