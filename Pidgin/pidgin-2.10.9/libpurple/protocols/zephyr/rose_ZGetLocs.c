/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for the ZGetLocations function.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"
#define min(a,b) ((a)<(b)?(a):(b))
/* Prototype for -Wmissing-prototypes */
Code_t ZGetLocations(ZLocations_t *location,int *numlocs);

Code_t ZGetLocations(ZLocations_t *location,int *numlocs)
{
  int i;
  if (!(__locate_list != 0)) 
    return (-772103668L);
  if (__locate_next == __locate_num) 
    return (-772103667L);
  for (i = 0; i < ((( *numlocs < (__locate_num - __locate_next))? *numlocs : (__locate_num - __locate_next))); i++) 
    location[i] = __locate_list[i + __locate_next];
  if ((__locate_num - __locate_next) <  *numlocs) 
     *numlocs = (__locate_num - __locate_next);
  __locate_next +=  *numlocs;
  return 0;
}
