/* This file is part of the Project Athena Zephyr Notification System.
 * It contains the ZhmStat() function.
 *
 *      Created by:     Marc Horowitz
 *
 *      Copyright (c) 1996 by the Massachusetts Institute of Technology.
 *      For copying and distribution information, see the file
 *      "mit-copyright.h".
 */
#include "internal.h"
#ifdef WIN32
#include <winsock2.h>
#else
#include <sys/socket.h>
#endif
#ifndef INADDR_LOOPBACK
#define INADDR_LOOPBACK 0x7f000001
#endif

Code_t ZhmStat(hostaddr,notice)
struct in_addr *hostaddr;
ZNotice_t *notice;
{
  struct servent *sp;
  struct sockaddr_in sin;
  ZNotice_t req;
  Code_t code;
  struct timeval tv;
  fd_set readers;
  memset(((char *)(&sin)),0,(sizeof(struct sockaddr_in )));
  sp = getservbyname("zephyr-hm","udp");
  sin.sin_port = (((sp != 0)?(sp -> s_port) : (htons(((unsigned short )2104)))));
  sin.sin_family = 2;
  if (hostaddr != 0) 
    sin.sin_addr =  *hostaddr;
  else 
    sin.sin_addr.s_addr = htonl(((in_addr_t )0x7f000001));
  memset(((char *)(&req)),0,(sizeof(req)));
  req.z_kind = STAT;
  req.z_port = 0;
  req.z_class = "HM_STAT";
  req.z_class_inst = "HMST_CLIENT";
  req.z_opcode = "GIMMESTATS";
  req.z_sender = "";
  req.z_recipient = "";
  req.z_default_format = "";
  req.z_message_len = 0;
  if ((code = ZSetDestAddr(&sin)) != 0) 
    return code;
  if ((code = ZSendNotice(&req,0)) != 0) 
    return code;
/* Wait up to ten seconds for a response. */
  do {
    int __d0;
    int __d1;
    asm volatile ("cld; rep; stosq" : "=c" (__d0), "=D" (__d1) : "a" (0), "0" ((sizeof(fd_set ) / sizeof(__fd_mask ))), "1" ((readers.__fds_bits + 0)) : "memory");
  }while (0);
  readers.__fds_bits[__Zephyr_fd / (8 * ((int )(sizeof(__fd_mask ))))] |= (((__fd_mask )1) << (__Zephyr_fd % (8 * ((int )(sizeof(__fd_mask ))))));
  tv.tv_sec = 10;
  tv.tv_usec = 0;
  code = select((__Zephyr_fd + 1),&readers,0,0,&tv);
  if ((code < 0) && ( *__errno_location() != 4)) 
    return  *__errno_location();
  if (((code == 0) || ((code < 0) && ( *__errno_location() == 4))) || (ZPending() == 0)) 
    return (-772103670L);
  return ZReceiveNotice(notice,0);
}
