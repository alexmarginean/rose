/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for ZReceivePacket function.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987,1988 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"

Code_t ZReceivePacket(buffer,ret_len,from)
char *buffer;
int *ret_len;
struct sockaddr_in *from;
{
  Code_t retval;
  struct _Z_InputQ *nextq;
  if ((retval = Z_WaitForComplete()) != 0) 
    return retval;
  nextq = Z_GetFirstComplete();
   *ret_len = (nextq -> packet_len);
  if ( *ret_len > 1024) 
    return (-772103680L);
  memcpy(buffer,(nextq -> packet),( *ret_len));
  if (from != 0) 
     *from = (nextq -> from);
  Z_RemQueue(nextq);
  return 0;
}
