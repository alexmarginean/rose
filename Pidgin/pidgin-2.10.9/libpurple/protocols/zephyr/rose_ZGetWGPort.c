/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for the ZGetWGPort function.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"

int ZGetWGPort()
{
  char *envptr;
  char name[128UL];
  FILE *fp;
  int wgport;
#ifdef WIN32
#endif
  envptr = getenv("WGFILE");
  if (!(envptr != 0)) {
#ifdef WIN32
#else
    sprintf(name,"/tmp/wg.%d",getuid());
#endif
    envptr = name;
  }
  if (!((fp = fopen(envptr,"r")) != 0)) 
    return -1;
/* if fscanf fails, return -1 via wgport */
  if (fscanf(fp,"%d",&wgport) != 1) 
    wgport = -1;
  fclose(fp);
  return wgport;
}
