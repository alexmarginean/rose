/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for the ZPending function.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"

int ZPending()
{
  int retval;
  if (__Zephyr_fd < 0) {
     *__errno_location() = (-772103673L);
    return -1;
  }
  if ((retval = Z_ReadEnqueue()) != 0) {
     *__errno_location() = retval;
    return -1;
  }
  return __Q_CompleteLength;
}
