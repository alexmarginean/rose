/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for the ZSendPacket function.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987,1991 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"
#ifdef WIN32
#include <winsock.h>
#else
#include <sys/socket.h>
#endif
static int wait_for_hmack(ZNotice_t *notice,void *uid);

Code_t ZSendPacket(packet,len,waitforack)
char *packet;
int len;
int waitforack;
{
  Code_t retval;
  struct sockaddr_in dest;
  ZNotice_t notice;
  ZNotice_t acknotice;
  if (!(packet != 0) || (len < 0)) 
    return (-772103678L);
  if (len > 1024) 
    return (-772103680L);
  if (__Zephyr_fd < 0) 
    if ((retval = ZOpenPort(0)) != 0) 
      return retval;
  dest = __HM_addr;
  if (sendto(__Zephyr_fd,packet,len,0,((struct sockaddr *)(&dest)),(sizeof(dest))) < 0) 
    return  *__errno_location();
  if (!(waitforack != 0)) 
    return 0;
  if ((retval = ZParseNotice(packet,len,&notice)) != 0) 
    return retval;
  retval = Z_WaitForNotice(&acknotice,wait_for_hmack,(&notice.z_uid),1);
  if (retval == 110) 
    return (-772103670L);
  if (retval == 0) 
    ZFreeNotice(&acknotice);
  return retval;
}

static int wait_for_hmack(ZNotice_t *notice,void *uid)
{
  return ((notice -> z_kind) == HMACK) && (ZCompareUID(&notice -> z_uid,((ZUnique_Id_t *)uid)) != 0);
}
