/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for the ZCheckIfNotice function.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987,1988 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"

Code_t ZCheckIfNotice(notice,from,predicate,args)
ZNotice_t *notice;
struct sockaddr_in *from;
register int (*predicate)(ZNotice_t *, void *);
void *args;
{
  ZNotice_t tmpnotice;
  Code_t retval;
  register char *buffer;
  register struct _Z_InputQ *qptr;
  if ((retval = Z_ReadEnqueue()) != 0) 
    return retval;
  qptr = Z_GetFirstComplete();
  while(qptr != 0){
    if ((retval = ZParseNotice((qptr -> packet),(qptr -> packet_len),&tmpnotice)) != 0) 
      return retval;
    if (( *predicate)(&tmpnotice,args) != 0) {
      if (!((buffer = ((char *)(malloc(((unsigned int )(qptr -> packet_len)))))) != 0)) 
        return 12;
      memcpy(buffer,(qptr -> packet),(qptr -> packet_len));
      if (from != 0) 
         *from = (qptr -> from);
      if ((retval = ZParseNotice(buffer,(qptr -> packet_len),notice)) != 0) {
        free(buffer);
        return retval;
      }
      Z_RemQueue(qptr);
      return 0;
    }
    qptr = Z_GetNextComplete(qptr);
  }
  return (-772103672L);
}
