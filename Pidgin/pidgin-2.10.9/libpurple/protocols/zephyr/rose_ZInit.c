/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for the ZInitialize function.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987, 1991 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#ifdef ZEPHYR_USES_KERBEROS
#ifdef WIN32
#else
#include <krb_err.h>
#endif
#endif
#include "internal.h"
#ifdef WIN32
#include <winsock2.h>
#else
#include <sys/socket.h>
#endif
#ifndef INADDR_NONE
#define INADDR_NONE 0xffffffff
#endif

Code_t ZInitialize()
{
  struct servent *hmserv;
  struct hostent *hostent;
  char addr[4UL];
  char hostname[64UL];
  struct in_addr servaddr;
  struct sockaddr_in sin;
  int s;
  socklen_t sinsize = (sizeof(sin));
  Code_t code;
  ZNotice_t notice;
#ifdef ZEPHYR_USES_KERBEROS
/*    initialize_krb_error_table(); */
#endif
  initialize_zeph_error_table();
  memset(((char *)(&__HM_addr)),0,(sizeof(__HM_addr)));
  __HM_addr.sin_family = 2;
/* Set up local loopback address for HostManager */
  addr[0] = 127;
  addr[1] = 0;
  addr[2] = 0;
  addr[3] = 1;
  hmserv = getservbyname("zephyr-hm","udp");
  __HM_addr.sin_port = (((hmserv != 0)?(hmserv -> s_port) : (htons(((unsigned short )2104)))));
  memcpy(((char *)(&__HM_addr.sin_addr)),addr,4);
  __HM_set = 0;
/* Initialize the input queue */
  __Q_Tail = ((struct _Z_InputQ *)((void *)0));
  __Q_Head = ((struct _Z_InputQ *)((void *)0));
/* if the application is a server, there might not be a zhm.  The
       code will fall back to something which might not be "right",
       but this is is ok, since none of the servers call krb_rd_req. */
  servaddr.s_addr = ((in_addr_t )0xffffffff);
  if (!(__Zephyr_server != 0)) {
    if ((code = ZOpenPort(0)) != 0) 
      return code;
    if ((code = ZhmStat(0,&notice)) != 0) 
      return code;
    ZClosePort();
/* the first field, which is NUL-terminated, is the server name.
	  If this code ever support a multiplexing zhm, this will have to
	  be made smarter, and probably per-message */
#ifdef ZEPHYR_USES_KERBEROS
#endif
    hostent = gethostbyname(notice.z_message);
    if ((hostent != 0) && ((hostent -> h_addrtype) == 2)) 
      memcpy((&servaddr),(hostent -> h_addr_list)[0],(sizeof(servaddr)));
    ZFreeNotice(&notice);
  }
#ifdef ZEPHYR_USES_KERBEROS
#else
  g_strlcpy(__Zephyr_realm,"local-realm",64);
#endif
  __My_addr.s_addr = ((in_addr_t )0xffffffff);
  if (servaddr.s_addr != ((in_addr_t )0xffffffff)) {
/* Try to get the local interface address by connecting a UDP
	 * socket to the server address and getting the local address.
	 * Some broken operating systems (e.g. Solaris 2.0-2.5) yield
	 * INADDR_ANY (zero), so we have to check for that. */
    s = socket(2,SOCK_DGRAM,0);
    if (s != -1) {
      memset((&sin),0,(sizeof(sin)));
      sin.sin_family = 2;
      memcpy((&sin.sin_addr),(&servaddr),(sizeof(servaddr)));
      sin.sin_port = htons(((unsigned short )2105));
      if (((connect(s,((struct sockaddr *)(&sin)),(sizeof(sin))) == 0) && (getsockname(s,((struct sockaddr *)(&sin)),&sinsize) == 0)) && (sin.sin_addr.s_addr != 0)) 
        memcpy((&__My_addr),(&sin.sin_addr),(sizeof(__My_addr)));
      close(s);
    }
  }
  if (__My_addr.s_addr == ((in_addr_t )0xffffffff)) {
/* We couldn't figure out the local interface address by the
	 * above method.  Try by resolving the local hostname.  (This
	 * is a pretty broken thing to do, and unfortunately what we
	 * always do on server machines.) */
    if (gethostname(hostname,(sizeof(hostname))) == 0) {
      hostent = gethostbyname(hostname);
      if ((hostent != 0) && ((hostent -> h_addrtype) == 2)) 
        memcpy((&__My_addr),(hostent -> h_addr_list)[0],(sizeof(__My_addr)));
    }
  }
/* If the above methods failed, zero out __My_addr so things will
     * sort of kind of work. */
  if (__My_addr.s_addr == ((in_addr_t )0xffffffff)) 
    __My_addr.s_addr = 0;
/* Get the sender so we can cache it */
  ZGetSender();
  return 0;
}
