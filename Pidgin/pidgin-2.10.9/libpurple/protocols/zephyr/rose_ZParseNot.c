/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for the ZParseNotice function.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987,1991 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"
/* Assume that strlen is efficient on this machine... */
#define next_field(ptr)	ptr += strlen (ptr) + 1
#if defined (__GNUC__) && defined (__vax__)
#undef next_field
/*
     * This should be faster on VAX models outside the 2 series.  Don't
     * use it if you are using MicroVAX 2 servers.  If you are using a
     * VS2 server, use something like
     *	#define next_field(ptr)		while(*ptr++)
     * instead of this code.
     *
     * This requires use of GCC to get the optimized code, but
     * everybody uses GCC, don't they? :-)
     */
/* Assumes that no field is longer than 64K.... */
#define next_field(ptr) ptr = Istrend (ptr) + 1
#endif
#ifdef mips
#undef next_field
/*
 * The compiler doesn't optimize this macro as well as it does the
 * following function.
 */
#define next_fieldXXX(ptr) do{register unsigned c1,c2;c1= *ptr;	\
		   while((ptr++,c2= *ptr,c1)&&(ptr++,c1= *ptr,c2));}while(0)
/*
     * Calling overhead is still present, but this routine is faster
     * than strlen, and doesn't bother with some of the other math
     * that we'd just have to undo later anyways.
     */
#define next_field(ptr)	ptr=next_field_1(ptr)
#endif

Code_t ZParseNotice(buffer,len,notice)
char *buffer;
int len;
ZNotice_t *notice;
{
  char *ptr;
  char *end;
  unsigned long temp;
  int maj;
  int numfields;
  int i;
#ifdef __LINE__
  int lineno;
/* Note: This definition of BAD eliminates lint and compiler
     * complains about the "while (0)", but require that the macro not
     * be used as the "then" part of an "if" statement that also has
     * an "else" clause.
     */
#define BAD_PACKET	{lineno=__LINE__;goto badpkt;}
/* This one gets lint/compiler complaints.  */
/*#define BAD	do{lineno=__LINE__;goto badpkt;}while(0)*/
#else
#define BAD_PACKET	goto badpkt
#endif
  memset(((char *)notice),0,(sizeof(ZNotice_t )));
  ptr = buffer;
  end = (buffer + len);
  notice -> z_packet = buffer;
  notice -> z_version = ptr;
  if (strncmp(ptr,"ZEPH",(sizeof(( *((char (*)[5UL])"ZEPH"))) - 1)) != 0) 
    return (-772103674L);
  ptr += sizeof(( *((char (*)[5UL])"ZEPH"))) - 1;
  if (!(( *ptr) != 0)) {
#ifdef Z_DEBUG
#endif
    return (-772103675L);
  }
  maj = atoi(ptr);
  if (maj != 0) 
    return (-772103674L);
  ptr += (strlen(ptr) + 1);
  if ((ZReadAscii32(ptr,(end - ptr),&temp)) == -772103665L) {
    lineno = 109;
    goto badpkt;
  };
  numfields = temp;
  ptr += (strlen(ptr) + 1);
/*XXX 3 */
/* numfields, version, and checksum */
  numfields -= 2;
  if (numfields < 0) {
#ifdef __LINE__
    lineno = 117;
    badpkt:
#ifdef Z_DEBUG
#endif
#else
#ifdef Z_DEBUG
#endif
#endif
    return (-772103675L);
  }
  if (numfields != 0) {
    if ((ZReadAscii32(ptr,(end - ptr),&temp)) == -772103665L) {
      lineno = 137;
      goto badpkt;
    };
    notice -> z_kind = temp;
    numfields--;
    ptr += (strlen(ptr) + 1);
  }
  else {
    lineno = 143;
    goto badpkt;
  };
  if (numfields != 0) {
    if ((ZReadAscii(ptr,(end - ptr),((unsigned char *)(&notice -> z_uid)),(sizeof(ZUnique_Id_t )))) == -772103665L) {
      lineno = 148;
      goto badpkt;
    };
    notice -> z_time.tv_sec = (ntohl(((unsigned long )notice -> z_uid.tv.tv_sec)));
    notice -> z_time.tv_usec = (ntohl(((unsigned long )notice -> z_uid.tv.tv_usec)));
    numfields--;
    ptr += (strlen(ptr) + 1);
  }
  else {
    lineno = 155;
    goto badpkt;
  };
  if (numfields != 0) {
    if ((ZReadAscii16(ptr,(end - ptr),&notice -> z_port)) == -772103665L) {
      lineno = 159;
      goto badpkt;
    };
    notice -> z_port = htons((notice -> z_port));
    numfields--;
    ptr += (strlen(ptr) + 1);
  }
  else {
    lineno = 165;
    goto badpkt;
  };
  if (numfields != 0) {
    if ((ZReadAscii32(ptr,(end - ptr),&temp)) == -772103665L) {
      lineno = 169;
      goto badpkt;
    };
    notice -> z_auth = temp;
    numfields--;
    ptr += (strlen(ptr) + 1);
  }
  else {
    lineno = 175;
    goto badpkt;
  };
  notice -> z_checked_auth = -3;
  if (numfields != 0) {
    if ((ZReadAscii32(ptr,(end - ptr),&temp)) == -772103665L) {
      lineno = 180;
      goto badpkt;
    };
    notice -> z_authent_len = temp;
    numfields--;
    ptr += (strlen(ptr) + 1);
  }
  else {
    lineno = 186;
    goto badpkt;
  };
  if (numfields != 0) {
    notice -> z_ascii_authent = ptr;
    numfields--;
    ptr += (strlen(ptr) + 1);
  }
  else {
    lineno = 194;
    goto badpkt;
  };
  if (numfields != 0) {
    notice -> z_class = ptr;
    numfields--;
    ptr += (strlen(ptr) + 1);
  }
  else 
    notice -> z_class = "";
  if (numfields != 0) {
    notice -> z_class_inst = ptr;
    numfields--;
    ptr += (strlen(ptr) + 1);
  }
  else 
    notice -> z_class_inst = "";
  if (numfields != 0) {
    notice -> z_opcode = ptr;
    numfields--;
    ptr += (strlen(ptr) + 1);
  }
  else 
    notice -> z_opcode = "";
  if (numfields != 0) {
    notice -> z_sender = ptr;
    numfields--;
    ptr += (strlen(ptr) + 1);
  }
  else 
    notice -> z_sender = "";
  if (numfields != 0) {
    notice -> z_recipient = ptr;
    numfields--;
    ptr += (strlen(ptr) + 1);
  }
  else 
    notice -> z_recipient = "";
  if (numfields != 0) {
    notice -> z_default_format = ptr;
    numfields--;
    ptr += (strlen(ptr) + 1);
  }
  else 
    notice -> z_default_format = "";
/*XXX*/
  if ((ZReadAscii32(ptr,(end - ptr),&temp)) == -772103665L) {
    lineno = 246;
    goto badpkt;
  };
  notice -> z_checksum = temp;
  numfields--;
  ptr += (strlen(ptr) + 1);
  if (numfields != 0) {
    notice -> z_multinotice = ptr;
    numfields--;
    ptr += (strlen(ptr) + 1);
  }
  else 
    notice -> z_multinotice = "";
  if (numfields != 0) {
    if ((ZReadAscii(ptr,(end - ptr),((unsigned char *)(&notice -> z_multiuid)),(sizeof(ZUnique_Id_t )))) == -772103665L) {
      lineno = 262;
      goto badpkt;
    };
    notice -> z_time.tv_sec = (ntohl(((unsigned long )notice -> z_multiuid.tv.tv_sec)));
    notice -> z_time.tv_usec = (ntohl(((unsigned long )notice -> z_multiuid.tv.tv_usec)));
    numfields--;
    ptr += (strlen(ptr) + 1);
  }
  else 
    notice -> z_multiuid = (notice -> z_uid);
  for (i = 0; (i < 10) && (numfields != 0); (i++ , numfields--)) {
    (notice -> z_other_fields)[i] = ptr;
    ptr += (strlen(ptr) + 1);
  }
  notice -> z_num_other_fields = i;
  for (i = 0; i < numfields; i++) 
    ptr += (strlen(ptr) + 1);
  notice -> z_message = ((void *)ptr);
  notice -> z_message_len = (len - (ptr - buffer));
  return 0;
}
