/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for the ZSubscribeTo, ZUnsubscribeTo, and
 * ZCancelSubscriptions functions.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987,1988 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"
static Code_t Z_Subscriptions(ZSubscription_t *sublist,int nitems,unsigned int port,char *opcode,int authit);
static Code_t subscr_sendoff(ZNotice_t *notice,char **lyst,int num,int authit);

Code_t ZSubscribeTo(sublist,nitems,port)
ZSubscription_t *sublist;
int nitems;
unsigned int port;
{
  return Z_Subscriptions(sublist,nitems,port,"SUBSCRIBE",1);
}

Code_t ZSubscribeToSansDefaults(sublist,nitems,port)
ZSubscription_t *sublist;
int nitems;
unsigned int port;
{
  return Z_Subscriptions(sublist,nitems,port,"SUBSCRIBE_NODEFS",1);
}

Code_t ZUnsubscribeTo(sublist,nitems,port)
ZSubscription_t *sublist;
int nitems;
unsigned int port;
{
  return Z_Subscriptions(sublist,nitems,port,"UNSUBSCRIBE",1);
}

Code_t ZCancelSubscriptions(port)
unsigned int port;
{
  return Z_Subscriptions(0,0,port,"CLEARSUB",0);
}
/*
 * This routine must do its own fragmentation.  Subscriptions must
 * not be broken across packet boundaries, or else the server will
 * mis-interpret them.
 */

static Code_t Z_Subscriptions(sublist,nitems,port,opcode,authit)
register ZSubscription_t *sublist;
int nitems;
unsigned int port;
char *opcode;
int authit;
{
  register int i;
  register int j;
  int retval;
  ZNotice_t notice;
  char header[800UL];
  char **list;
  char *recip;
  int hdrlen;
/* space avail for data,
						 adjusted below */
  int size_avail = 1024 - 13;
  int size;
  int start;
  int numok;
/* nitems = 0 means cancel all subscriptions; still need to allocate a */
/* array for one item so we can cancel, however. */
  list = ((char **)(malloc(((((unsigned int )(((nitems == 0)?1 : nitems))) * 3) * sizeof(char *)))));
  if (!(list != 0)) 
    return 12;
  memset(((char *)(&notice)),0,(sizeof(notice)));
  notice.z_kind = ACKED;
  notice.z_port = port;
  notice.z_class = "ZEPHYR_CTL";
  notice.z_class_inst = "CLIENT";
  notice.z_opcode = opcode;
  notice.z_sender = 0;
  notice.z_recipient = "";
  notice.z_default_format = "";
  notice.z_message_len = 0;
/* format the header to figure out how long it is */
  retval = Z_FormatHeader(&notice,header,(sizeof(header)),&hdrlen,ZMakeAuthentication);
  if ((retval != 0) && !(authit != 0)) 
    retval = Z_FormatHeader(&notice,header,(sizeof(header)),&hdrlen,0);
  if (retval != 0) {
    free(((char *)list));
    return retval;
  }
/* compute amount of room left */
  size_avail -= hdrlen;
  size = size_avail;
/* assemble subs into an array of pointers */
  for (i = 0; i < nitems; i++) {
    list[i * 3] = sublist[i].zsub_class;
    list[(i * 3) + 1] = sublist[i].zsub_classinst;
    recip = sublist[i].zsub_recipient;
    if ((recip != 0) && (( *recip) == '*')) 
      recip++;
    if (!(recip != 0) || ((( *recip) != 0) && (( *recip) != 64))) 
      recip = ZGetSender();
    list[(i * 3) + 2] = recip;
  }
  start = -1;
  i = 0;
  numok = 0;
  if (!(nitems != 0)) {
/* there aren't really any, but we need to xmit anyway */
    retval = subscr_sendoff(&notice,list,0,authit);
    free(((char *)list));
    return retval;
  }
  while(i < nitems){
    if (start == -1) {
      size = size_avail;
      start = i;
      numok = 0;
    }
    if ((j = (((strlen(list[i * 3]) + strlen(list[(i * 3) + 1])) + strlen(list[(i * 3) + 2])) + 3)) <= size) {
/* it will fit in this packet */
      size -= j;
      numok++;
      i++;
      continue; 
    }
/* a single subscription won't
					   fit into one packet */
    if (!(numok != 0)) {
      free(((char *)list));
      return (-772103666L);
    }
    retval = subscr_sendoff(&notice,(list + (start * 3)),numok,authit);
    if (retval != 0) {
      free(((char *)list));
      return retval;
    }
    start = -1;
  }
  if (numok != 0) 
    retval = subscr_sendoff(&notice,(list + (start * 3)),numok,authit);
  free(((char *)list));
  return retval;
}

static Code_t subscr_sendoff(notice,lyst,num,authit)
ZNotice_t *notice;
char **lyst;
int num;
int authit;
{
  register Code_t retval;
  ZNotice_t retnotice;
  retval = ZSendList(notice,lyst,(num * 3),ZMakeAuthentication);
  if ((retval != 0) && !(authit != 0)) 
    retval = ZSendList(notice,lyst,(num * 3),0);
  if (retval != 0) 
    return retval;
  if ((retval = ZIfNotice(&retnotice,0,ZCompareUIDPred,((char *)(&notice -> z_uid)))) != 0) 
    return retval;
  if (retnotice.z_kind == SERVNAK) {
    ZFreeNotice(&retnotice);
    return (-772103664L);
  }
  if (retnotice.z_kind != SERVACK) {
    ZFreeNotice(&retnotice);
    return (-772103669L);
  }
  ZFreeNotice(&retnotice);
  return 0;
}
