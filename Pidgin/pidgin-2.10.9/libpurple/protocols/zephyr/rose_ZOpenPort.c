/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for the ZOpenPort function.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"
#ifdef WIN32
#include <winsock2.h>
#else
#include <sys/socket.h>
#endif

Code_t ZOpenPort(port)
unsigned short *port;
{
  struct sockaddr_in bindin;
  socklen_t len;
  ZClosePort();
  if ((__Zephyr_fd = socket(2,SOCK_DGRAM,0)) < 0) {
    __Zephyr_fd = -1;
    return  *__errno_location();
  }
#ifdef SO_BSDCOMPAT
{
    int on = 1;
    setsockopt(__Zephyr_fd,1,14,((char *)(&on)),(sizeof(on)));
  }
#endif
  bindin.sin_family = 2;
  if ((port != 0) && (( *port) != 0)) 
    bindin.sin_port =  *port;
  else 
    bindin.sin_port = 0;
  bindin.sin_addr.s_addr = ((in_addr_t )0);
  if (bind(__Zephyr_fd,((struct sockaddr *)(&bindin)),(sizeof(bindin))) < 0) {
    if ((( *__errno_location() == 98) && (port != 0)) && (( *port) != 0)) 
      return (-772103676L);
    else 
      return  *__errno_location();
  }
  if (!(bindin.sin_port != 0)) {
    len = (sizeof(bindin));
    if (getsockname(__Zephyr_fd,((struct sockaddr *)(&bindin)),&len) != 0) 
      return  *__errno_location();
  }
  __Zephyr_port = bindin.sin_port;
  __Zephyr_open = 1;
  if (port != 0) 
     *port = bindin.sin_port;
  return 0;
}
