/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for the ZMakeAuthentication function.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"
#ifndef ERROR_TABLE_BASE_krb
#define ERROR_TABLE_BASE_krb (39525376L)
#endif
#ifdef ZEPHYR_USES_KERBEROS
#ifdef WIN32
#else
#include <krb_err.h>
#endif
#endif
#if 0
#ifdef ZEPHYR_USES_KERBEROS
#endif
#endif

Code_t ZMakeAuthentication(notice,buffer,buffer_len,len)
register ZNotice_t *notice;
char *buffer;
int buffer_len;
int *len;
{
#ifdef ZEPHYR_USES_KERBEROS
/* zero length authent is an error, so malloc(0) is not a problem */
/* Compute a checksum over the header and message. */
#else
  notice -> z_checksum = 0;
  notice -> z_auth = 1;
  notice -> z_authent_len = 0;
  notice -> z_ascii_authent = "";
  return Z_FormatRawHeader(notice,buffer,buffer_len,len,0,0);
#endif
}
