/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for asynchronous location functions.
 *
 *	Created by:	Marc Horowitz
 *
 *	Copyright (c) 1990,1991 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"

Code_t ZRequestLocations(user,zald,kind,auth)
const char *user;
ZAsyncLocateData_t *zald;
/* UNSAFE, UNACKED, or ACKED */
ZNotice_Kind_t kind;
Z_AuthProc auth;
{
  int retval;
  ZNotice_t notice;
  size_t userlen;
  size_t versionlen;
  if (__Zephyr_fd < 0) 
    if ((retval = ZOpenPort(0)) != 0) 
      return retval;
  memset(((char *)(&notice)),0,(sizeof(notice)));
  notice.z_kind = kind;
  notice.z_port = __Zephyr_port;
  notice.z_class = "USER_LOCATE";
  notice.z_class_inst = user;
  notice.z_opcode = "LOCATE";
  notice.z_sender = 0;
  notice.z_recipient = "";
  notice.z_default_format = "";
  notice.z_message_len = 0;
  if ((retval = ZSendNotice(&notice,auth)) != 0) 
    return retval;
  userlen = (strlen(user) + 1);
  versionlen = (strlen(notice.z_version) + 1);
  if ((zald -> user = ((char *)(malloc(userlen)))) == ((char *)((void *)0))) {
    return 12;
  }
  if ((zald -> version = ((char *)(malloc(versionlen)))) == ((char *)((void *)0))) {
    free((zald -> user));
    return 12;
  }
  zald -> uid = notice.z_multiuid;
  g_strlcpy((zald -> user),user,userlen);
  g_strlcpy((zald -> version),notice.z_version,versionlen);
  return 0;
}

Code_t ZParseLocations(notice,zald,nlocs,user)
ZNotice_t *notice;
ZAsyncLocateData_t *zald;
int *nlocs;
char **user;
{
  char *ptr;
  char *end;
  int i;
/* This never fails (this function is part of the
			     library, so it is allowed to know this). */
  ZFlushLocations();
/* non-matching protocol version numbers means the
       server is probably an older version--must punt */
  if ((zald != 0) && (strcmp((notice -> z_version),(zald -> version)) != 0)) 
    return (-772103674L);
  if ((notice -> z_kind) == SERVNAK) 
    return (-772103664L);
/* flag ACKs as special */
  if (((notice -> z_kind) == SERVACK) && !(strcmp((notice -> z_opcode),"LOCATE") != 0)) {
     *nlocs = -1;
    return 0;
  }
  if ((notice -> z_kind) != ACKED) 
    return (-772103669L);
  end = ((notice -> z_message) + (notice -> z_message_len));
  __locate_num = 0;
  for (ptr = (notice -> z_message); ptr < end; ptr++) 
    if (!(( *ptr) != 0)) 
      __locate_num++;
  __locate_num /= 3;
  if (__locate_num != 0) {
    __locate_list = ((ZLocations_t *)(malloc((((unsigned int )__locate_num) * sizeof(ZLocations_t )))));
    if (!(__locate_list != 0)) 
      return 12;
  }
  else {
    __locate_list = 0;
  }
  for (((ptr = (notice -> z_message)) , (i = 0)); i < __locate_num; i++) {
    unsigned int len;
    len = (strlen(ptr) + 1);
    __locate_list[i].host = ((char *)(malloc(len)));
    if (!(__locate_list[i].host != 0)) 
      return 12;
    g_strlcpy(__locate_list[i].host,ptr,len);
    ptr += len;
    len = (strlen(ptr) + 1);
    __locate_list[i].time = ((char *)(malloc(len)));
    if (!(__locate_list[i].time != 0)) 
      return 12;
    g_strlcpy(__locate_list[i].time,ptr,len);
    ptr += len;
    len = (strlen(ptr) + 1);
    __locate_list[i].tty = ((char *)(malloc(len)));
    if (!(__locate_list[i].tty != 0)) 
      return 12;
    g_strlcpy(__locate_list[i].tty,ptr,len);
    ptr += len;
  }
  __locate_next = 0;
   *nlocs = __locate_num;
  if (user != 0) {
    size_t len;
    if (zald != 0) {
      len = (strlen((zald -> user)) + 1);
      if (( *user = ((char *)(malloc(len)))) == ((char *)((void *)0))) 
        return 12;
      g_strlcpy( *user,(zald -> user),len);
    }
    else {
      len = (strlen((notice -> z_class_inst)) + 1);
      if (( *user = ((char *)(malloc(len)))) == ((char *)((void *)0))) 
        return 12;
      g_strlcpy( *user,(notice -> z_class_inst),len);
    }
  }
  return 0;
}

int ZCompareALDPred(notice,zald)
ZNotice_t *notice;
void *zald;
{
  return ZCompareUID(&notice -> z_multiuid,&( *((ZAsyncLocateData_t *)zald)).uid);
}

void ZFreeALD(zald)
ZAsyncLocateData_t *zald;
{
  if (!(zald != 0)) 
    return ;
  if ((zald -> user) != 0) 
    free((zald -> user));
  if ((zald -> version) != 0) 
    free((zald -> version));
  memset(zald,0,(sizeof(( *zald))));
}
