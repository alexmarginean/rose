/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for the ZMakeAscii function.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"
static char *itox_chars = "0123456789ABCDEF";

Code_t ZMakeAscii(ptr,len,field,num)
register char *ptr;
int len;
unsigned char *field;
int num;
{
  int i;
  for (i = 0; i < num; i++) {
/* we need to add "0x" if we are between 4 byte pieces */
    if ((i & 3) == 0) {
      if (len < (((i != 0)?4 : 3))) 
        return (-772103666L);
/* except at the beginning, put a space in before the "0x" */
      if (i != 0) {
         *(ptr++) = 32;
        len--;
      }
       *(ptr++) = 48;
       *(ptr++) = 'x';
      len -= 2;
    }
    if (len < 3) 
      return (-772103666L);
     *(ptr++) = itox_chars[field[i] >> 4];
     *(ptr++) = itox_chars[field[i] & 15];
    len -= 2;
  }
   *ptr = 0;
  return 0;
}

Code_t ZMakeAscii32(ptr,len,value)
register char *ptr;
int len;
unsigned long value;
{
  if (len < 11) 
    return (-772103666L);
   *(ptr++) = 48;
   *(ptr++) = 'x';
   *(ptr++) = itox_chars[(value >> 28) & 15];
   *(ptr++) = itox_chars[(value >> 24) & 15];
   *(ptr++) = itox_chars[(value >> 20) & 15];
   *(ptr++) = itox_chars[(value >> 16) & 15];
   *(ptr++) = itox_chars[(value >> 12) & 15];
   *(ptr++) = itox_chars[(value >> 8) & 15];
   *(ptr++) = itox_chars[(value >> 4) & 15];
   *(ptr++) = itox_chars[(value >> 0) & 15];
   *ptr = 0;
  return 0;
}

Code_t ZMakeAscii16(ptr,len,value)
register char *ptr;
int len;
unsigned int value;
{
  if (len < 7) 
    return (-772103666L);
   *(ptr++) = 48;
   *(ptr++) = 'x';
   *(ptr++) = itox_chars[(value >> 12) & 15];
   *(ptr++) = itox_chars[(value >> 8) & 15];
   *(ptr++) = itox_chars[(value >> 4) & 15];
   *(ptr++) = itox_chars[(value >> 0) & 15];
   *ptr = 0;
  return 0;
}
