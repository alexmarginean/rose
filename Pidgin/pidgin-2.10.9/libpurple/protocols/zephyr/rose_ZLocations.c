/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for the ZSetLocation, ZUnsetLocation, and
 * ZFlushMyLocations functions.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987,1988,1991 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"
#ifndef WIN32
#include <pwd.h>
#endif
#include <stdlib.h>
#include <errno.h>

Code_t ZSetLocation(exposure)
char *exposure;
{
  return Z_SendLocation("LOGIN",exposure,ZMakeAuthentication,"$sender logged in to $1 on $3 at $2");
}

Code_t ZUnsetLocation()
{
  return Z_SendLocation("LOGIN","USER_LOGOUT",0,"$sender logged out of $1 on $3 at $2");
}

Code_t ZFlushMyLocations()
{
  return Z_SendLocation("LOGIN","USER_FLUSH",ZMakeAuthentication,"");
}
static char host[64UL];
static char *mytty = (char *)((void *)0);
static int reenter = 0;

Code_t Z_SendLocation(class,opcode,auth,format)
char *class;
char *opcode;
Z_AuthProc auth;
char *format;
{
  int retval;
  time_t ourtime;
  ZNotice_t notice;
  ZNotice_t retnotice;
  char *bptr[3UL];
#ifndef X_DISPLAY_MISSING
  char *display;
#endif
#ifndef WIN32
  char *ttyp;
  char *p;
#endif
  struct hostent *hent;
  short wg_port = (ZGetWGPort());
  memset(((char *)(&notice)),0,(sizeof(notice)));
  notice.z_kind = ACKED;
  notice.z_port = ((unsigned short )(((wg_port == -1)?0 : wg_port)));
  notice.z_class = class;
  notice.z_class_inst = (ZGetSender());
  notice.z_opcode = opcode;
  notice.z_sender = 0;
  notice.z_recipient = "";
  notice.z_num_other_fields = 0;
  notice.z_default_format = format;
/*
      keep track of what we said before so that we can be consistent
      when changing location information.
      This is done mainly for the sake of the WindowGram client.
     */
  if (!(reenter != 0)) {
    if (gethostname(host,64) < 0) 
      return  *__errno_location();
    hent = gethostbyname(host);
    if (hent != 0) {
      strncpy(host,(hent -> h_name),(sizeof(host)));
      host[sizeof(host) - 1] = 0;
    }
#ifndef X_DISPLAY_MISSING
    if (((display = getenv("DISPLAY")) != 0) && (( *display) != 0)) {
      mytty = g_strdup(display);
    }
    else {
#endif
#ifdef WIN32
#else
      ttyp = ttyname(0);
      if ((ttyp != 0) && (( *ttyp) != 0)) {
        p = strchr((ttyp + 1),'/');
        mytty = g_strdup((((p != 0)?(p + 1) : ttyp)));
      }
      else {
        mytty = g_strdup("unknown");
      }
#endif
#ifndef X_DISPLAY_MISSING
    }
#endif
    reenter = 1;
  }
  ourtime = time(0);
  bptr[0] = host;
  bptr[1] = ctime((&ourtime));
  bptr[1][strlen(bptr[1]) - 1] = 0;
  bptr[2] = mytty;
  if ((retval = ZSendList(&notice,bptr,3,auth)) != 0) 
    return retval;
  retval = Z_WaitForNotice(&retnotice,ZCompareUIDPred,(&notice.z_uid),30);
  if (retval != 0) 
    return retval;
  if (retnotice.z_kind == SERVNAK) {
    if (!(retnotice.z_message_len != 0)) {
      ZFreeNotice(&retnotice);
      return (-772103664L);
    }
    if (!(strcmp(retnotice.z_message,"LOST") != 0)) {
      ZFreeNotice(&retnotice);
      return (-772103663L);
    }
    if (!(strcmp(retnotice.z_message,"FAIL") != 0)) {
      ZFreeNotice(&retnotice);
      return (-772103662L);
    }
    ZFreeNotice(&retnotice);
    return (-772103664L);
  }
  if (retnotice.z_kind != SERVACK) {
    ZFreeNotice(&retnotice);
    return (-772103669L);
  }
  if (!(retnotice.z_message_len != 0)) {
    ZFreeNotice(&retnotice);
    return (-772103669L);
  }
  if ((strcmp(retnotice.z_message,"SENT") != 0) && (strcmp(retnotice.z_message,"LOST") != 0)) {
    ZFreeNotice(&retnotice);
    return (-772103669L);
  }
  ZFreeNotice(&retnotice);
  return 0;
}
