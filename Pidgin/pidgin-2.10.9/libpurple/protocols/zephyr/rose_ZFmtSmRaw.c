/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for the ZFormatSmallRawNotice function.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"

Code_t ZFormatSmallRawNotice(notice,buffer,ret_len)
ZNotice_t *notice;
char *buffer;
int *ret_len;
{
  Code_t retval;
  int hdrlen;
  if ((retval = Z_FormatRawHeader(notice,buffer,800,&hdrlen,0,0)) != 0) 
    return retval;
   *ret_len = (hdrlen + (notice -> z_message_len));
  if ( *ret_len > 1024) 
    return (-772103680L);
  memcpy((buffer + hdrlen),(notice -> z_message),(notice -> z_message_len));
  return 0;
}
