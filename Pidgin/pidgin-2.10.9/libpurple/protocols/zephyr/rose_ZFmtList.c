/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for the ZFormatNoticeList function.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987,1991 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"

Code_t ZFormatNoticeList(notice,list,nitems,buffer,ret_len,cert_routine)
ZNotice_t *notice;
register char **list;
int nitems;
char **buffer;
int *ret_len;
Z_AuthProc cert_routine;
{
  char header[800UL];
  register int i;
  int hdrlen;
  int size;
  char *ptr;
  Code_t retval;
  if ((retval = Z_FormatHeader(notice,header,(sizeof(header)),&hdrlen,cert_routine)) != 0) 
    return retval;
  size = 0;
  for (i = 0; i < nitems; i++) 
    size += (strlen(list[i]) + 1);
   *ret_len = (hdrlen + size);
/* *ret_len can never be zero here, no need to worry about malloc(0). */
  if (!(( *buffer = ((char *)(malloc(((unsigned int )( *ret_len)))))) != 0)) 
    return 12;
  memcpy(( *buffer),header,hdrlen);
  ptr = ( *buffer + hdrlen);
  for (; nitems != 0; (nitems-- , list++)) {
    i = (strlen(( *list)) + 1);
    memcpy(ptr,( *list),i);
    ptr += i;
  }
  return 0;
}
