/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for the ZGetVariable, ZSetVariable, and ZUnsetVariable
 * functions.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "libpurple/internal.h"
#include "internal.h"
#include "util.h"
#include <ctype.h>
#ifndef WIN32
#include <pwd.h>
#endif
static char *get_localvarfile();
static char *get_varval(char *fn,char *val);
static int varline(char *bfr,char *var);

char *ZGetVariable(var)
char *var;
{
  char *varfile;
  char *ret;
  if ((varfile = get_localvarfile()) == ((char *)((void *)0))) 
    return 0;
  ret = get_varval(varfile,var);
  g_free(varfile);
  if (ret != 0) 
    return ret;
#ifdef WIN32
#else
  varfile = g_strdup_printf("%s/zephyr.vars","/usr/local/etc");
#endif
  ret = get_varval(varfile,var);
  g_free(varfile);
  return ret;
}

Code_t ZSetVariable(var,value)
char *var;
char *value;
{
  int written;
  FILE *fpin;
  FILE *fpout;
  char *varfile;
  char *varfilebackup;
  char varbfr[512UL];
  written = 0;
  if ((varfile = get_localvarfile()) == ((char *)((void *)0))) 
    return (-772103669L);
  varfilebackup = g_strconcat(varfile,".backup",((void *)((void *)0)));
  if (!((fpout = fopen(varfilebackup,"w")) != 0)) {
    g_free(varfile);
    g_free(varfilebackup);
    return  *__errno_location();
  }
  if ((fpin = fopen(varfile,"r")) != ((FILE *)((void *)0))) {
    while(fgets(varbfr,(sizeof(varbfr)),fpin) != ((char *)((char *)0))){
      if (varbfr[strlen(varbfr) - 1] < 32) 
        varbfr[strlen(varbfr) - 1] = 0;
      if (varline(varbfr,var) != 0) {
        fprintf(fpout,"%s = %s\n",var,value);
        written = 1;
      }
      else 
        fprintf(fpout,"%s\n",varbfr);
    }
/* don't care about errs on input */
    fclose(fpin);
  }
  if (!(written != 0)) 
    fprintf(fpout,"%s = %s\n",var,value);
  if (fclose(fpout) == -1) {
    g_free(varfilebackup);
    g_free(varfile);
/* can't rely on errno */
    return 5;
  }
  if (rename(varfilebackup,varfile) != 0) {
    g_free(varfilebackup);
    g_free(varfile);
    return  *__errno_location();
  }
  g_free(varfilebackup);
  g_free(varfile);
  return 0;
}

Code_t ZUnsetVariable(var)
char *var;
{
  FILE *fpin;
  FILE *fpout;
  char *varfile;
  char *varfilebackup;
  char varbfr[512UL];
  if ((varfile = get_localvarfile()) == ((char *)((void *)0))) 
    return (-772103669L);
  varfilebackup = g_strconcat(varfile,".backup",((void *)((void *)0)));
  if (!((fpout = fopen(varfilebackup,"w")) != 0)) {
    g_free(varfile);
    g_free(varfilebackup);
    return  *__errno_location();
  }
  if ((fpin = fopen(varfile,"r")) != ((FILE *)((void *)0))) {
    while(fgets(varbfr,(sizeof(varbfr)),fpin) != ((char *)((char *)0))){
      if (varbfr[strlen(varbfr) - 1] < 32) 
        varbfr[strlen(varbfr) - 1] = 0;
      if (!(varline(varbfr,var) != 0)) 
        fprintf(fpout,"%s\n",varbfr);
    }
/* don't care about read close errs */
    fclose(fpin);
  }
  if (fclose(fpout) == -1) {
    g_free(varfilebackup);
    g_free(varfile);
/* errno isn't reliable */
    return 5;
  }
  if (rename(varfilebackup,varfile) != 0) {
    g_free(varfilebackup);
    g_free(varfile);
    return  *__errno_location();
  }
  g_free(varfilebackup);
  g_free(varfile);
  return 0;
}

static char *get_localvarfile()
{
  const char *base;
#ifndef WIN32
  struct passwd *pwd;
  base = purple_home_dir();
#else
#endif
  if (!(base != 0)) {
#ifndef WIN32
    if (!((pwd = getpwuid(((int )(getuid())))) != 0)) {
      fprintf(stderr,"Zephyr internal failure: Can\'t find your entry in /etc/passwd\n");
      return 0;
    }
    base = (pwd -> pw_dir);
#endif
  }
  return g_strconcat(base,"/.zephyr.vars",((void *)((void *)0)));
}

static char *get_varval(fn,var)
char *fn;
char *var;
{
  FILE *fp;
  static char varbfr[512UL];
  int i;
  fp = fopen(fn,"r");
  if (!(fp != 0)) 
    return 0;
  while(fgets(varbfr,(sizeof(varbfr)),fp) != ((char *)((char *)0))){
    if (varbfr[strlen(varbfr) - 1] < 32) 
      varbfr[strlen(varbfr) - 1] = 0;
    if (!((i = varline(varbfr,var)) != 0)) 
      continue; 
/* open read-only, don't care */
    fclose(fp);
    return varbfr + i;
  }
/* open read-only, don't care */
  fclose(fp);
  return 0;
}
/* If the variable in the line bfr[] is the same as var, return index to
   the variable value, else return 0. */

static int varline(bfr,var)
char *bfr;
char *var;
{
  register char *cp;
/* comment or null line */
  if (!(bfr[0] != 0) || (bfr[0] == '#')) 
    return 0;
  cp = bfr;
  while(((( *cp) != 0) && !((( *__ctype_b_loc())[(int )( *cp)] & ((unsigned short )_ISspace)) != 0)) && (( *cp) != '='))
    cp++;
#ifndef WIN32
#define max(a,b) ((a > b) ? (a) : (b))
#endif
  if (g_ascii_strncasecmp(bfr,var,((((strlen(var)) > (cp - bfr))?(strlen(var)) : (cp - bfr)))) != 0) 
/* var is not the var in
					   bfr ==> no match */
    return 0;
  cp = strchr(bfr,'=');
  if (!(cp != 0)) 
    return 0;
  cp++;
/* space up to variable value */
  while((( *cp) != 0) && ((( *__ctype_b_loc())[(int )( *cp)] & ((unsigned short )_ISspace)) != 0))
    cp++;
/* return index */
  return (cp - bfr);
}
