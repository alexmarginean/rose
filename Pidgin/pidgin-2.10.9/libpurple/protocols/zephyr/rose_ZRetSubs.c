/* This file is part of the Project Athena Zephyr Notification System.
 * It contains source for the ZRetrieveSubscriptions and
 * ZRetrieveDefaultSubscriptions functions.
 *
 *	Created by:	Robert French
 *
 *	Copyright (c) 1987,1988,1991 by the Massachusetts Institute of Technology.
 *	For copying and distribution information, see the file
 *	"mit-copyright.h".
 */
#include "internal.h"
static Code_t Z_RetSubs(ZNotice_t *notice,int *nsubs,Z_AuthProc auth_routine);
/* Need STDC definition when possible for unsigned short argument. */
#ifdef __STDC__

Code_t ZRetrieveSubscriptions(unsigned short port,int *nsubs)
#else
#endif
{
  int retval;
  ZNotice_t notice;
  char asciiport[50UL];
/* use default port */
  if (!(port != 0)) 
    port = __Zephyr_port;
  retval = ZMakeAscii16(asciiport,(sizeof(asciiport)),(ntohs(port)));
  if (retval != 0) 
    return retval;
  memset(((char *)(&notice)),0,(sizeof(notice)));
  notice.z_message = asciiport;
  notice.z_message_len = (strlen(asciiport) + 1);
  notice.z_opcode = "GIMME";
  return Z_RetSubs(&notice,nsubs,ZMakeAuthentication);
}
#if 0
#endif

static Code_t Z_RetSubs(notice,nsubs,auth_routine)
register ZNotice_t *notice;
int *nsubs;
Z_AuthProc auth_routine;
{
  register int i;
  int retval;
  int nrecv;
  int gimmeack;
  ZNotice_t retnotice;
  char *ptr;
  char *end;
  char *ptr2;
  retval = ZFlushSubscriptions();
  if ((retval != 0) && (retval != -772103661L)) 
    return retval;
  if (__Zephyr_fd < 0) 
    if ((retval = ZOpenPort(0)) != 0) 
      return retval;
  notice -> z_kind = ACKED;
  notice -> z_port = __Zephyr_port;
  notice -> z_class = "ZEPHYR_CTL";
  notice -> z_class_inst = "CLIENT";
  notice -> z_sender = 0;
  notice -> z_recipient = "";
  notice -> z_default_format = "";
  if ((retval = ZSendNotice(notice,auth_routine)) != 0) 
    return retval;
  nrecv = 0;
  gimmeack = 0;
  __subscriptions_list = ((ZSubscription_t *)((ZSubscription_t *)0));
  while(!(nrecv != 0) || !(gimmeack != 0)){
    retval = Z_WaitForNotice(&retnotice,ZCompareMultiUIDPred,(&notice -> z_multiuid),30);
    if (retval == -772103672L) 
      return 110;
    else if (retval != 0) 
      return retval;
    if (retnotice.z_kind == SERVNAK) {
      ZFreeNotice(&retnotice);
      return (-772103664L);
    }
/* non-matching protocol version numbers means the
		   server is probably an older version--must punt */
    if (strcmp((notice -> z_version),retnotice.z_version) != 0) {
      ZFreeNotice(&retnotice);
      return (-772103674L);
    }
    if ((retnotice.z_kind == SERVACK) && !(strcmp(retnotice.z_opcode,(notice -> z_opcode)) != 0)) {
      ZFreeNotice(&retnotice);
      gimmeack = 1;
      continue; 
    }
    if (retnotice.z_kind != ACKED) {
      ZFreeNotice(&retnotice);
      return (-772103669L);
    }
    nrecv++;
    end = (retnotice.z_message + retnotice.z_message_len);
    __subscriptions_num = 0;
    for (ptr = retnotice.z_message; ptr < end; ptr++) 
      if (!(( *ptr) != 0)) 
        __subscriptions_num++;
    __subscriptions_num = (__subscriptions_num / 3);
    __subscriptions_list = ((ZSubscription_t *)(malloc(((unsigned int )(__subscriptions_num * sizeof(ZSubscription_t ))))));
    if ((__subscriptions_num != 0) && !(__subscriptions_list != 0)) {
      ZFreeNotice(&retnotice);
      return 12;
    }
    for (((ptr = retnotice.z_message) , (i = 0)); i < __subscriptions_num; i++) {
      size_t len;
      len = (strlen(ptr) + 1);
      __subscriptions_list[i].zsub_class = ((char *)(malloc(len)));
      if (!(__subscriptions_list[i].zsub_class != 0)) {
        ZFreeNotice(&retnotice);
        return 12;
      }
      g_strlcpy(__subscriptions_list[i].zsub_class,ptr,len);
      ptr += len;
      len = (strlen(ptr) + 1);
      __subscriptions_list[i].zsub_classinst = ((char *)(malloc(len)));
      if (!(__subscriptions_list[i].zsub_classinst != 0)) {
        ZFreeNotice(&retnotice);
        return 12;
      }
      g_strlcpy(__subscriptions_list[i].zsub_classinst,ptr,len);
      ptr += len;
      ptr2 = ptr;
      if (!(( *ptr2) != 0)) 
        ptr2 = "*";
      len = (strlen(ptr2) + 1);
      __subscriptions_list[i].zsub_recipient = ((char *)(malloc(len)));
      if (!(__subscriptions_list[i].zsub_recipient != 0)) {
        ZFreeNotice(&retnotice);
        return 12;
      }
      g_strlcpy(__subscriptions_list[i].zsub_recipient,ptr2,len);
      ptr += (strlen(ptr) + 1);
    }
    ZFreeNotice(&retnotice);
  }
  __subscriptions_next = 0;
   *nsubs = __subscriptions_num;
  return 0;
}
