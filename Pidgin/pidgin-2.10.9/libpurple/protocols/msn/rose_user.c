/**
 * @file user.c User functions
 *
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "debug.h"
#include "util.h"
#include "user.h"
#include "slp.h"

static void free_user_endpoint(MsnUserEndpoint *data)
{
  g_free((data -> id));
  g_free((data -> name));
  g_free(data);
}
/*new a user object*/

MsnUser *msn_user_new(MsnUserList *userlist,const char *passport,const char *friendly_name)
{
  MsnUser *user;
  user = ((MsnUser *)(g_malloc0_n(1,(sizeof(MsnUser )))));
  user -> userlist = userlist;
  msn_user_set_passport(user,passport);
  msn_user_set_friendly_name(user,friendly_name);
  return msn_user_ref(user);
}
/*destroy a user object*/

static void msn_user_destroy(MsnUser *user)
{
  while((user -> endpoints) != ((GSList *)((void *)0))){
    free_user_endpoint(( *(user -> endpoints)).data);
    user -> endpoints = g_slist_delete_link((user -> endpoints),(user -> endpoints));
  }
  if ((user -> clientcaps) != ((GHashTable *)((void *)0))) 
    g_hash_table_destroy((user -> clientcaps));
  if ((user -> group_ids) != ((GList *)((void *)0))) {
    GList *l;
    for (l = (user -> group_ids); l != ((GList *)((void *)0)); l = (l -> next)) {
      g_free((l -> data));
    }
    g_list_free((user -> group_ids));
  }
  if ((user -> msnobj) != ((MsnObject *)((void *)0))) 
    msn_object_destroy((user -> msnobj));
  g_free((user -> passport));
  g_free((user -> friendly_name));
  g_free((user -> uid));
  if ((user -> extinfo) != 0) {
    g_free(( *(user -> extinfo)).media_album);
    g_free(( *(user -> extinfo)).media_artist);
    g_free(( *(user -> extinfo)).media_title);
    g_free(( *(user -> extinfo)).phone_home);
    g_free(( *(user -> extinfo)).phone_mobile);
    g_free(( *(user -> extinfo)).phone_work);
    g_free((user -> extinfo));
  }
  g_free((user -> statusline));
  g_free((user -> invite_message));
  g_free(user);
}

MsnUser *msn_user_ref(MsnUser *user)
{
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return 0;
    };
  }while (0);
  user -> refcount++;
  return user;
}

void msn_user_unref(MsnUser *user)
{
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  user -> refcount--;
  if ((user -> refcount) == 0) 
    msn_user_destroy(user);
}

void msn_user_update(MsnUser *user)
{
  PurpleAccount *account;
  gboolean offline;
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  account = ( *( *(user -> userlist)).session).account;
  offline = ((user -> status) == ((const char *)((void *)0)));
  if (!(offline != 0)) {
    purple_prpl_got_user_status(account,(user -> passport),(user -> status),"message",(user -> statusline),((void *)((void *)0)));
  }
  else {
    if ((user -> mobile) != 0) {
      purple_prpl_got_user_status(account,(user -> passport),"mobile",((void *)((void *)0)));
      purple_prpl_got_user_status(account,(user -> passport),"available",((void *)((void *)0)));
    }
    else {
      purple_prpl_got_user_status(account,(user -> passport),"offline",((void *)((void *)0)));
    }
  }
  if (!(offline != 0) || !((user -> mobile) != 0)) {
    purple_prpl_got_user_status_deactive(account,(user -> passport),"mobile");
  }
  if ((!(offline != 0) && ((user -> extinfo) != 0)) && (( *(user -> extinfo)).media_type != CURRENT_MEDIA_UNKNOWN)) {
    if (( *(user -> extinfo)).media_type == CURRENT_MEDIA_MUSIC) {
      purple_prpl_got_user_status(account,(user -> passport),"tune","tune_artist",( *(user -> extinfo)).media_artist,"tune_album",( *(user -> extinfo)).media_album,"tune_title",( *(user -> extinfo)).media_title,((void *)((void *)0)));
    }
    else if (( *(user -> extinfo)).media_type == CURRENT_MEDIA_GAMES) {
      purple_prpl_got_user_status(account,(user -> passport),"tune","game",( *(user -> extinfo)).media_title,((void *)((void *)0)));
    }
    else if (( *(user -> extinfo)).media_type == CURRENT_MEDIA_OFFICE) {
      purple_prpl_got_user_status(account,(user -> passport),"tune","office",( *(user -> extinfo)).media_title,((void *)((void *)0)));
    }
    else {
      purple_debug_warning("msn","Got CurrentMedia with unknown type %d.\n",( *(user -> extinfo)).media_type);
    }
  }
  else {
    purple_prpl_got_user_status_deactive(account,(user -> passport),"tune");
  }
  if ((user -> idle) != 0) 
    purple_prpl_got_user_idle(account,(user -> passport),(!0),(-1));
  else 
    purple_prpl_got_user_idle(account,(user -> passport),0,0);
}

void msn_user_set_state(MsnUser *user,const char *state)
{
  const char *status;
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  if (state == ((const char *)((void *)0))) {
    user -> status = ((const char *)((void *)0));
    return ;
  }
  if (!(g_ascii_strcasecmp(state,"BSY") != 0)) 
    status = "busy";
  else if (!(g_ascii_strcasecmp(state,"BRB") != 0)) 
    status = "brb";
  else if (!(g_ascii_strcasecmp(state,"AWY") != 0)) 
    status = "away";
  else if (!(g_ascii_strcasecmp(state,"PHN") != 0)) 
    status = "phone";
  else if (!(g_ascii_strcasecmp(state,"LUN") != 0)) 
    status = "lunch";
  else if (!(g_ascii_strcasecmp(state,"HDN") != 0)) 
    status = ((const char *)((void *)0));
  else 
    status = "available";
  if (!(g_ascii_strcasecmp(state,"IDL") != 0)) 
    user -> idle = (!0);
  else 
    user -> idle = 0;
  user -> status = status;
}

void msn_user_set_passport(MsnUser *user,const char *passport)
{
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  g_free((user -> passport));
  user -> passport = g_strdup(passport);
}

gboolean msn_user_set_friendly_name(MsnUser *user,const char *name)
{
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return 0;
    };
  }while (0);
  if (!(name != 0)) 
    return 0;
  if (((user -> friendly_name) != 0) && (!(strcmp((user -> friendly_name),name) != 0) || !(strcmp((user -> passport),name) != 0))) 
    return 0;
  g_free((user -> friendly_name));
  user -> friendly_name = g_strdup(name);
  serv_got_alias(purple_account_get_connection(( *( *(user -> userlist)).session).account),(user -> passport),name);
  return (!0);
}

void msn_user_set_statusline(MsnUser *user,const char *statusline)
{
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  g_free((user -> statusline));
  user -> statusline = g_strdup(statusline);
}

void msn_user_set_uid(MsnUser *user,const char *uid)
{
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  g_free((user -> uid));
  user -> uid = g_strdup(uid);
}

void msn_user_set_endpoint_data(MsnUser *user,const char *input,MsnUserEndpoint *newep)
{
  MsnUserEndpoint *ep;
  char *endpoint;
  GSList *l;
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  do {
    if (input != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"input != NULL");
      return ;
    };
  }while (0);
  endpoint = g_ascii_strdown(input,(-1));
{
    for (l = (user -> endpoints); l != 0; l = (l -> next)) {
      ep = (l -> data);
      if (g_str_equal((ep -> id),endpoint) != 0) {
/* We have info about this endpoint! */
        g_free(endpoint);
        if (newep == ((MsnUserEndpoint *)((void *)0))) {
/* Delete it and exit */
          user -> endpoints = g_slist_delete_link((user -> endpoints),l);
          free_user_endpoint(ep);
          return ;
        }
/* Break out of our loop and update it */
        break; 
      }
    }
  }
  if (l == ((GSList *)((void *)0))) {
/* Need to add a new endpoint */
    ep = ((MsnUserEndpoint *)(g_malloc0_n(1,(sizeof(MsnUserEndpoint )))));
    ep -> id = endpoint;
    user -> endpoints = g_slist_prepend((user -> endpoints),ep);
  }
  ep -> clientid = (newep -> clientid);
  ep -> extcaps = (newep -> extcaps);
}

void msn_user_clear_endpoints(MsnUser *user)
{
  MsnUserEndpoint *ep;
  GSList *l;
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  for (l = (user -> endpoints); l != 0; l = g_slist_delete_link(l,l)) {
    ep = (l -> data);
    free_user_endpoint(ep);
  }
  user -> endpoints = ((GSList *)((void *)0));
}

void msn_user_set_op(MsnUser *user,MsnListOp list_op)
{
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  user -> list_op |= list_op;
}

void msn_user_unset_op(MsnUser *user,MsnListOp list_op)
{
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  user -> list_op &= (~list_op);
}

void msn_user_set_buddy_icon(MsnUser *user,PurpleStoredImage *img)
{
  MsnObject *msnobj;
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  msnobj = msn_object_new_from_image(img,"TFR2C2.tmp",(user -> passport),MSN_OBJECT_USERTILE);
  if (!(msnobj != 0)) 
    purple_debug_error("msn","Unable to open buddy icon from %s!\n",(user -> passport));
  msn_user_set_object(user,msnobj);
}
/*add group id to User object*/

void msn_user_add_group_id(MsnUser *user,const char *group_id)
{
  MsnUserList *userlist;
  PurpleAccount *account;
  PurpleBuddy *b;
  PurpleGroup *g;
  const char *passport;
  const char *group_name;
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  do {
    if (group_id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"group_id != NULL");
      return ;
    };
  }while (0);
  user -> group_ids = g_list_append((user -> group_ids),(g_strdup(group_id)));
  userlist = (user -> userlist);
  account = ( *(userlist -> session)).account;
  passport = msn_user_get_passport(user);
  group_name = msn_userlist_find_group_name(userlist,group_id);
  purple_debug_info("msn","User: group id:%s,name:%s,user:%s\n",group_id,group_name,passport);
  g = purple_find_group(group_name);
  if ((group_id == ((const char *)((void *)0))) && (g == ((PurpleGroup *)((void *)0)))) {
    g = purple_group_new(group_name);
    purple_blist_add_group(g,0);
  }
  b = purple_find_buddy_in_group(account,passport,g);
  if (b == ((PurpleBuddy *)((void *)0))) {
    b = purple_buddy_new(account,passport,0);
    purple_blist_add_buddy(b,0,g,0);
  }
  purple_buddy_set_protocol_data(b,user);
/*Update the blist Node info*/
}
/*check if the msn user is online*/

gboolean msn_user_is_online(PurpleAccount *account,const char *name)
{
  PurpleBuddy *buddy;
  buddy = purple_find_buddy(account,name);
  return ((buddy != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0);
}

gboolean msn_user_is_yahoo(PurpleAccount *account,const char *name)
{
  MsnSession *session = (MsnSession *)((void *)0);
  MsnUser *user;
  PurpleConnection *gc;
  gc = purple_account_get_connection(account);
  if (gc != ((PurpleConnection *)((void *)0))) 
    session = (gc -> proto_data);
  if ((session != ((MsnSession *)((void *)0))) && ((user = msn_userlist_find_user((session -> userlist),name)) != ((MsnUser *)((void *)0)))) {
    return (user -> networkid) == MSN_NETWORK_YAHOO;
  }
  return strstr(name,"@yahoo.") != ((char *)((void *)0));
}

void msn_user_remove_group_id(MsnUser *user,const char *id)
{
  GList *l;
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return ;
    };
  }while (0);
  l = g_list_find_custom((user -> group_ids),id,((GCompareFunc )strcmp));
  if (l == ((GList *)((void *)0))) 
    return ;
  g_free((l -> data));
  user -> group_ids = g_list_delete_link((user -> group_ids),l);
}

void msn_user_set_pending_group(MsnUser *user,const char *group)
{
  user -> pending_group = g_strdup(group);
}

char *msn_user_remove_pending_group(MsnUser *user)
{
  char *group = (user -> pending_group);
  user -> pending_group = ((char *)((void *)0));
  return group;
}

void msn_user_set_home_phone(MsnUser *user,const char *number)
{
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  if (!(number != 0) && !((user -> extinfo) != 0)) 
    return ;
  if ((user -> extinfo) != 0) 
    g_free(( *(user -> extinfo)).phone_home);
  else 
    user -> extinfo = ((MsnUserExtendedInfo *)(g_malloc0_n(1,(sizeof(MsnUserExtendedInfo )))));
  ( *(user -> extinfo)).phone_home = g_strdup(number);
}

void msn_user_set_work_phone(MsnUser *user,const char *number)
{
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  if (!(number != 0) && !((user -> extinfo) != 0)) 
    return ;
  if ((user -> extinfo) != 0) 
    g_free(( *(user -> extinfo)).phone_work);
  else 
    user -> extinfo = ((MsnUserExtendedInfo *)(g_malloc0_n(1,(sizeof(MsnUserExtendedInfo )))));
  ( *(user -> extinfo)).phone_work = g_strdup(number);
}

void msn_user_set_mobile_phone(MsnUser *user,const char *number)
{
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  if (!(number != 0) && !((user -> extinfo) != 0)) 
    return ;
  if ((user -> extinfo) != 0) 
    g_free(( *(user -> extinfo)).phone_mobile);
  else 
    user -> extinfo = ((MsnUserExtendedInfo *)(g_malloc0_n(1,(sizeof(MsnUserExtendedInfo )))));
  ( *(user -> extinfo)).phone_mobile = g_strdup(number);
}

void msn_user_set_clientid(MsnUser *user,guint clientid)
{
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  user -> clientid = clientid;
}

void msn_user_set_extcaps(MsnUser *user,guint extcaps)
{
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  user -> extcaps = extcaps;
}

void msn_user_set_network(MsnUser *user,MsnNetwork network)
{
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  user -> networkid = network;
}

static gboolean buddy_icon_cached(PurpleConnection *gc,MsnObject *obj)
{
  PurpleAccount *account;
  PurpleBuddy *buddy;
  const char *old;
  const char *new;
  do {
    if (obj != ((MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return 0;
    };
  }while (0);
  account = purple_connection_get_account(gc);
  buddy = purple_find_buddy(account,msn_object_get_creator(obj));
  if (buddy == ((PurpleBuddy *)((void *)0))) 
    return 0;
  old = purple_buddy_icons_get_checksum_for_user(buddy);
  new = msn_object_get_sha1(obj);
  if (new == ((const char *)((void *)0))) 
    return 0;
/* If the old and new checksums are the same, and the file actually exists,
	 * then return TRUE */
  if ((old != ((const char *)((void *)0))) && !(strcmp(old,new) != 0)) 
    return (!0);
  return 0;
}

static void queue_buddy_icon_request(MsnUser *user)
{
  PurpleAccount *account;
  MsnObject *obj;
  GQueue *queue;
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  account = ( *( *(user -> userlist)).session).account;
  obj = msn_user_get_object(user);
  if (obj == ((MsnObject *)((void *)0))) {
    purple_buddy_icons_set_for_user(account,(user -> passport),0,0,0);
    return ;
  }
  if (!(buddy_icon_cached((account -> gc),obj) != 0)) {
    MsnUserList *userlist;
    userlist = (user -> userlist);
    queue = (userlist -> buddy_icon_requests);
    if (purple_debug_is_verbose() != 0) 
      purple_debug_info("msn","Queueing buddy icon request for %s (buddy_icon_window = %i)\n",(user -> passport),(userlist -> buddy_icon_window));
    g_queue_push_tail(queue,user);
    if ((userlist -> buddy_icon_window) > 0) 
      msn_release_buddy_icon_request(userlist);
  }
}

void msn_user_set_object(MsnUser *user,MsnObject *obj)
{
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  if (((user -> msnobj) != ((MsnObject *)((void *)0))) && !(msn_object_find_local(msn_object_get_sha1(obj)) != 0)) 
    msn_object_destroy((user -> msnobj));
  user -> msnobj = obj;
  if (((user -> list_op) & MSN_LIST_FL_OP) != 0U) 
    queue_buddy_icon_request(user);
}

void msn_user_set_client_caps(MsnUser *user,GHashTable *info)
{
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  do {
    if (info != ((GHashTable *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"info != NULL");
      return ;
    };
  }while (0);
  if ((user -> clientcaps) != ((GHashTable *)((void *)0))) 
    g_hash_table_destroy((user -> clientcaps));
  user -> clientcaps = info;
}

void msn_user_set_invite_message(MsnUser *user,const char *message)
{
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  g_free((user -> invite_message));
  user -> invite_message = g_strdup(message);
}

const char *msn_user_get_passport(const MsnUser *user)
{
  do {
    if (user != ((const MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return 0;
    };
  }while (0);
  return (user -> passport);
}

const char *msn_user_get_friendly_name(const MsnUser *user)
{
  do {
    if (user != ((const MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return 0;
    };
  }while (0);
  return (user -> friendly_name);
}

const char *msn_user_get_home_phone(const MsnUser *user)
{
  do {
    if (user != ((const MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return 0;
    };
  }while (0);
  return (((user -> extinfo) != 0)?( *(user -> extinfo)).phone_home : ((char *)((void *)0)));
}

const char *msn_user_get_work_phone(const MsnUser *user)
{
  do {
    if (user != ((const MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return 0;
    };
  }while (0);
  return (((user -> extinfo) != 0)?( *(user -> extinfo)).phone_work : ((char *)((void *)0)));
}

const char *msn_user_get_mobile_phone(const MsnUser *user)
{
  do {
    if (user != ((const MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return 0;
    };
  }while (0);
  return (((user -> extinfo) != 0)?( *(user -> extinfo)).phone_mobile : ((char *)((void *)0)));
}

guint msn_user_get_clientid(const MsnUser *user)
{
  do {
    if (user != ((const MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return 0;
    };
  }while (0);
  return user -> clientid;
}

guint msn_user_get_extcaps(const MsnUser *user)
{
  do {
    if (user != ((const MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return 0;
    };
  }while (0);
  return user -> extcaps;
}

MsnUserEndpoint *msn_user_get_endpoint_data(MsnUser *user,const char *input)
{
  char *endpoint;
  GSList *l;
  MsnUserEndpoint *ep;
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return 0;
    };
  }while (0);
  do {
    if (input != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"input != NULL");
      return 0;
    };
  }while (0);
  endpoint = g_ascii_strdown(input,(-1));
  for (l = (user -> endpoints); l != 0; l = (l -> next)) {
    ep = (l -> data);
    if (g_str_equal((ep -> id),endpoint) != 0) {
      g_free(endpoint);
      return ep;
    }
  }
  g_free(endpoint);
  return 0;
}

MsnNetwork msn_user_get_network(const MsnUser *user)
{
  do {
    if (user != ((const MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return MSN_NETWORK_UNKNOWN;
    };
  }while (0);
  return user -> networkid;
}

MsnObject *msn_user_get_object(const MsnUser *user)
{
  do {
    if (user != ((const MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return 0;
    };
  }while (0);
  return user -> msnobj;
}

GHashTable *msn_user_get_client_caps(const MsnUser *user)
{
  do {
    if (user != ((const MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return 0;
    };
  }while (0);
  return user -> clientcaps;
}

const char *msn_user_get_invite_message(const MsnUser *user)
{
  do {
    if (user != ((const MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return 0;
    };
  }while (0);
  return (user -> invite_message);
}

gboolean msn_user_is_capable(MsnUser *user,char *endpoint,guint capability,guint extcap)
{
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return 0;
    };
  }while (0);
  if (endpoint != ((char *)((void *)0))) {
    MsnUserEndpoint *ep = msn_user_get_endpoint_data(user,endpoint);
    if (ep != ((MsnUserEndpoint *)((void *)0))) 
      return (((ep -> clientid) & capability) != 0U) && (((ep -> extcaps) & extcap) != 0U);
    else 
      return 0;
  }
  return (((user -> clientid) & capability) != 0U) && (((user -> extcaps) & extcap) != 0U);
}
/**************************************************************************
 * Utility functions
 **************************************************************************/

int msn_user_passport_cmp(MsnUser *user,const char *passport)
{
  const char *str;
  char *pass;
  int result;
  str = purple_normalize_nocase(0,msn_user_get_passport(user));
  pass = g_strdup(str);
#if GLIB_CHECK_VERSION(2,16,0)
  result = g_strcmp0(pass,purple_normalize_nocase(0,passport));
#else
#endif /* GLIB < 2.16.0 */
  g_free(pass);
  return result;
}

gboolean msn_user_is_in_group(MsnUser *user,const char *group_id)
{
  if (user == ((MsnUser *)((void *)0))) 
    return 0;
  if (group_id == ((const char *)((void *)0))) 
    return 0;
  return g_list_find_custom((user -> group_ids),group_id,((GCompareFunc )strcmp)) != ((GList *)((void *)0));
}

gboolean msn_user_is_in_list(MsnUser *user,MsnListId list_id)
{
  if (user == ((MsnUser *)((void *)0))) 
    return 0;
  return ((user -> list_op) & (1 << list_id));
}
