/**
 * @file nexus.c MSN Nexus functions
 *
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "cipher.h"
#include "debug.h"
#include "msnutils.h"
#include "soap.h"
#include "nexus.h"
#include "notification.h"
/**************************************************************************
 * Valid Ticket Tokens
 **************************************************************************/
#define SSO_VALID_TICKET_DOMAIN 0
#define SSO_VALID_TICKET_POLICY 1
static char *ticket_domains[][2UL] = {
/* http://msnpiki.msnfanatic.com/index.php/MSNP15:SSO */
/* {"Domain", "Policy Ref URI"}, Purpose */
/* Authentication for messenger. */
{("messengerclear.live.com"), ((char *)((void *)0))}, 
/* Authentication for receiving OIMs. */
{("messenger.msn.com"), ("\?id=507")}, 
/* Authentication for the Contact server. */
{("contacts.msn.com"), ("MBI")}, 
/* Authentication for sending OIMs. */
{("messengersecure.live.com"), ("MBI_SSL")}, 
/* Storage REST API */
{("storage.live.com"), ("MBI")}, 
/* What's New service */
{("sup.live.com"), ("MBI")}};
/**************************************************************************
 * Main
 **************************************************************************/

MsnNexus *msn_nexus_new(MsnSession *session)
{
  MsnNexus *nexus;
  int i;
  nexus = ((MsnNexus *)(g_malloc0_n(1,(sizeof(MsnNexus )))));
  nexus -> session = session;
  nexus -> token_len = (sizeof(ticket_domains) / sizeof(char *[2UL]));
  nexus -> tokens = ((MsnTicketToken *)(g_malloc0_n((nexus -> token_len),(sizeof(MsnTicketToken )))));
  for (i = 0; i < (nexus -> token_len); i++) 
    (nexus -> tokens)[i].token = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
  return nexus;
}

void msn_nexus_destroy(MsnNexus *nexus)
{
  int i;
  for (i = 0; i < (nexus -> token_len); i++) {
    g_hash_table_destroy((nexus -> tokens)[i].token);
    g_free((nexus -> tokens)[i].secret);
    g_slist_free((nexus -> tokens)[i].updates);
  }
  g_free((nexus -> tokens));
  g_free((nexus -> policy));
  g_free((nexus -> nonce));
  g_free((nexus -> cipher));
  g_free((nexus -> secret));
  g_free(nexus);
}
/**************************************************************************
 * RPS/SSO Authentication
 **************************************************************************/

static char *rps_create_key(const char *key,int key_len,const char *data,size_t data_len)
{
  const guchar magic[] = "WS-SecureConversation";
  const int magic_len = (sizeof(magic) - 1);
  PurpleCipherContext *hmac;
  guchar hash1[20UL];
  guchar hash2[20UL];
  guchar hash3[20UL];
  guchar hash4[20UL];
  char *result;
  hmac = purple_cipher_context_new_by_name("hmac",0);
  purple_cipher_context_set_option(hmac,"hash","sha1");
  purple_cipher_context_set_key_with_len(hmac,((guchar *)key),key_len);
  purple_cipher_context_append(hmac,magic,magic_len);
  purple_cipher_context_append(hmac,((guchar *)data),data_len);
  purple_cipher_context_digest(hmac,(sizeof(hash1)),hash1,0);
  purple_cipher_context_reset(hmac,0);
  purple_cipher_context_set_option(hmac,"hash","sha1");
  purple_cipher_context_set_key_with_len(hmac,((guchar *)key),key_len);
  purple_cipher_context_append(hmac,hash1,20);
  purple_cipher_context_append(hmac,magic,magic_len);
  purple_cipher_context_append(hmac,((guchar *)data),data_len);
  purple_cipher_context_digest(hmac,(sizeof(hash2)),hash2,0);
  purple_cipher_context_reset(hmac,0);
  purple_cipher_context_set_option(hmac,"hash","sha1");
  purple_cipher_context_set_key_with_len(hmac,((guchar *)key),key_len);
  purple_cipher_context_append(hmac,hash1,20);
  purple_cipher_context_digest(hmac,(sizeof(hash3)),hash3,0);
  purple_cipher_context_reset(hmac,0);
  purple_cipher_context_set_option(hmac,"hash","sha1");
  purple_cipher_context_set_key_with_len(hmac,((guchar *)key),key_len);
  purple_cipher_context_append(hmac,hash3,(sizeof(hash3)));
  purple_cipher_context_append(hmac,magic,magic_len);
  purple_cipher_context_append(hmac,((guchar *)data),data_len);
  purple_cipher_context_digest(hmac,(sizeof(hash4)),hash4,0);
  purple_cipher_context_destroy(hmac);
  result = (g_malloc(24));
  memcpy(result,hash2,(sizeof(hash2)));
  memcpy((result + sizeof(hash2)),hash4,4);
  return result;
}

static char *des3_cbc(const char *key,const char *iv,const char *data,int len,gboolean decrypt)
{
  PurpleCipherContext *des3;
  char *out;
  size_t outlen;
  des3 = purple_cipher_context_new_by_name("des3",0);
  purple_cipher_context_set_key(des3,((guchar *)key));
  purple_cipher_context_set_batch_mode(des3,PURPLE_CIPHER_BATCH_MODE_CBC);
  purple_cipher_context_set_iv(des3,((guchar *)iv),8);
  out = (g_malloc(len));
  if (decrypt != 0) 
    purple_cipher_context_decrypt(des3,((guchar *)data),len,((guchar *)out),&outlen);
  else 
    purple_cipher_context_encrypt(des3,((guchar *)data),len,((guchar *)out),&outlen);
  purple_cipher_context_destroy(des3);
  return out;
}
#define MSN_USER_KEY_SIZE (7*4 + 8 + 20 + 72)
#define CRYPT_MODE_CBC 1
#define CIPHER_TRIPLE_DES 0x6603
#define HASH_SHA1 0x8004

static char *msn_rps_encrypt(MsnNexus *nexus)
{
  char usr_key_base[128UL];
  char *usr_key;
  const char magic1[] = "SESSION KEY HASH";
  const char magic2[] = "SESSION KEY ENCRYPTION";
  PurpleCipherContext *hmac;
  size_t len;
  guchar *hash;
  char *key1;
  char *key2;
  char *key3;
  gsize key1_len;
  const char *iv;
  char *nonce_fixed;
  char *cipher;
  char *response;
  usr_key = (usr_key_base + 0);
/* Header */
/* Header size */
  (msn_write32le(usr_key,28) , (usr_key += 4));
/* Crypt mode */
  (msn_write32le(usr_key,1) , (usr_key += 4));
/* Cipher type */
  (msn_write32le(usr_key,0x6603) , (usr_key += 4));
/* Hash type */
  (msn_write32le(usr_key,0x8004) , (usr_key += 4));
/* IV size */
  (msn_write32le(usr_key,8) , (usr_key += 4));
/* Hash size */
  (msn_write32le(usr_key,20) , (usr_key += 4));
/* Cipher size */
  (msn_write32le(usr_key,72) , (usr_key += 4));
/* Data */
  iv = usr_key;
  (msn_write32le(usr_key,(rand())) , (usr_key += 4));
  (msn_write32le(usr_key,(rand())) , (usr_key += 4));
  hash = ((guchar *)usr_key);
/* Remaining is cipher data */
  usr_key += 20;
  key1 = ((char *)(purple_base64_decode(((const char *)(nexus -> tokens)[MSN_AUTH_MESSENGER].secret),&key1_len)));
  key2 = rps_create_key(key1,key1_len,magic1,(sizeof(magic1) - 1));
  key3 = rps_create_key(key1,key1_len,magic2,(sizeof(magic2) - 1));
  len = strlen((nexus -> nonce));
  hmac = purple_cipher_context_new_by_name("hmac",0);
  purple_cipher_context_set_option(hmac,"hash","sha1");
  purple_cipher_context_set_key_with_len(hmac,((guchar *)key2),24);
  purple_cipher_context_append(hmac,((guchar *)(nexus -> nonce)),len);
  purple_cipher_context_digest(hmac,20,hash,0);
  purple_cipher_context_destroy(hmac);
/* We need to pad this to 72 bytes, apparently */
  nonce_fixed = (g_malloc((len + 8)));
  memcpy(nonce_fixed,(nexus -> nonce),len);
  memset((nonce_fixed + len),8,8);
  cipher = des3_cbc(key3,iv,nonce_fixed,(len + 8),0);
  g_free(nonce_fixed);
  memcpy(usr_key,cipher,72);
  g_free(key1);
  g_free(key2);
  g_free(key3);
  g_free(cipher);
  response = purple_base64_encode(((guchar *)usr_key_base),(7 * 4 + 8 + 20 + 72));
  return response;
}
/**************************************************************************
 * Login
 **************************************************************************/
/* Used to specify which token to update when only doing single updates */
typedef struct _MsnNexusUpdateData MsnNexusUpdateData;

struct _MsnNexusUpdateData 
{
  MsnNexus *nexus;
  int id;
}
;
typedef struct _MsnNexusUpdateCallback MsnNexusUpdateCallback;

struct _MsnNexusUpdateCallback 
{
  GSourceFunc cb;
  gpointer data;
}
;

static gboolean nexus_parse_token(MsnNexus *nexus,int id,xmlnode *node)
{
  char *token_str;
  char *expiry_str;
  const char *id_str;
  char **elems;
  char **cur;
  char **tokens;
  xmlnode *token = xmlnode_get_child(node,"RequestedSecurityToken/BinarySecurityToken");
  xmlnode *secret = xmlnode_get_child(node,"RequestedProofToken/BinarySecret");
  xmlnode *expires = xmlnode_get_child(node,"LifeTime/Expires");
  if (!(token != 0)) 
    return 0;
/* Use the ID that the server sent us */
  if (id == -1) {
    id_str = xmlnode_get_attrib(token,"Id");
    if (id_str == ((const char *)((void *)0))) 
      return 0;
/* 'Compact#' or 'PPToken#' */
    id = (atol((id_str + 7)) - 1);
    if (id >= (nexus -> token_len)) 
/* Where did this come from? */
      return 0;
  }
  token_str = xmlnode_get_data(token);
  if (token_str == ((char *)((void *)0))) 
    return 0;
  g_hash_table_remove_all((nexus -> tokens)[id].token);
  elems = g_strsplit(token_str,"&",0);
  for (cur = elems;  *cur != ((char *)((void *)0)); cur++) {
    tokens = g_strsplit(( *cur),"=",2);
    g_hash_table_insert((nexus -> tokens)[id].token,tokens[0],tokens[1]);
/* Don't free each of the tokens, only the array. */
    g_free(tokens);
  }
  g_strfreev(elems);
  g_free(token_str);
  if (secret != 0) 
    (nexus -> tokens)[id].secret = xmlnode_get_data(secret);
  else 
    (nexus -> tokens)[id].secret = ((char *)((void *)0));
/* Yay for MS using ISO-8601 */
  expiry_str = xmlnode_get_data(expires);
  (nexus -> tokens)[id].expiry = purple_str_to_time(expiry_str,0,0,0,0);
  g_free(expiry_str);
  purple_debug_info("msn","Updated ticket for domain \'%s\', expires at %li.\n",ticket_domains[id][0],((gint64 )(nexus -> tokens)[id].expiry));
  return (!0);
}

static gboolean nexus_parse_collection(MsnNexus *nexus,int id,xmlnode *collection)
{
  xmlnode *node;
  gboolean result;
  node = xmlnode_get_child(collection,"RequestSecurityTokenResponse");
  if (!(node != 0)) 
    return 0;
  result = (!0);
  for (; (node != 0) && (result != 0); node = (node -> next)) {
    xmlnode *endpoint = xmlnode_get_child(node,"AppliesTo/EndpointReference/Address");
    char *address = xmlnode_get_data(endpoint);
    if (g_str_equal(address,"http://Passport.NET/tb") != 0) {
/* This node contains the stuff for updating tokens. */
      char *data;
      xmlnode *cipher = xmlnode_get_child(node,"RequestedSecurityToken/EncryptedData/CipherData/CipherValue");
      xmlnode *secret = xmlnode_get_child(node,"RequestedProofToken/BinarySecret");
      g_free((nexus -> cipher));
      nexus -> cipher = xmlnode_get_data(cipher);
      data = xmlnode_get_data(secret);
      g_free((nexus -> secret));
      nexus -> secret = ((char *)(purple_base64_decode(data,0)));
      g_free(data);
    }
    else {
      result = nexus_parse_token(nexus,id,node);
    }
    g_free(address);
  }
  return result;
}

static void nexus_got_response_cb(MsnSoapMessage *req,MsnSoapMessage *resp,gpointer data)
{
  MsnNexus *nexus = data;
  MsnSession *session = (nexus -> session);
  const char *ticket;
  char *response;
  if (resp == ((MsnSoapMessage *)((void *)0))) {
    msn_session_set_error(session,MSN_ERROR_SERVCONN,((const char *)(dgettext("pidgin","Windows Live ID authentication:Unable to connect"))));
    return ;
  }
  if (!(nexus_parse_collection(nexus,-1,xmlnode_get_child((resp -> xml),"Body/RequestSecurityTokenResponseCollection")) != 0)) {
    msn_session_set_error(session,MSN_ERROR_SERVCONN,((const char *)(dgettext("pidgin","Windows Live ID authentication:Invalid response"))));
    return ;
  }
  ticket = msn_nexus_get_token_str(nexus,MSN_AUTH_MESSENGER);
  response = msn_rps_encrypt(nexus);
  msn_got_login_params(session,ticket,response);
  g_free(response);
}
/*when connect, do the SOAP Style windows Live ID authentication */

void msn_nexus_connect(MsnNexus *nexus)
{
  MsnSession *session = (nexus -> session);
  const char *username;
  const char *password;
  char *password_xml;
  GString *domains;
  char *request;
  int i;
  MsnSoapMessage *soap;
  purple_debug_info("msn","Starting Windows Live ID authentication\n");
  msn_session_set_login_step(session,MSN_LOGIN_STEP_GET_COOKIE);
  username = purple_account_get_username((session -> account));
  password = purple_connection_get_password(( *(session -> account)).gc);
  if (g_utf8_strlen(password,(-1)) > 16) {
/* max byte size for 16 utf8 characters is 64 + 1 for the null */
    gchar truncated[65UL];
    g_utf8_strncpy(truncated,password,16);
    password_xml = g_markup_escape_text(truncated,(-1));
  }
  else {
    password_xml = g_markup_escape_text(password,(-1));
  }
  purple_debug_info("msn","Logging on %s, with policy \'%s\', nonce \'%s\'\n",username,(nexus -> policy),(nexus -> nonce));
  domains = g_string_new(0);
  for (i = 0; i < (nexus -> token_len); i++) {
    g_string_append_printf(domains,"<wst:RequestSecurityToken xmlns=\"http://schemas.xmlsoap.org/ws/2004/04/trust\" Id=\"RST%d\"><wst:RequestType>http://schemas.xmlsoap.org/ws/2004/04/security/trust/Issue</wst:RequestType><wsp:AppliesTo xmlns=\"http://schemas.xmlsoap.org/ws/2002/12/policy\"><wsa:EndpointReference xmlns=\"http://schemas.xmlsoap.org/ws/2004/03/addressing\"><wsa:Address>%s</wsa:Address></wsa:EndpointReference></wsp:AppliesTo><wsse:PolicyReference xmlns=\"http://schemas.xmlsoap.org/ws/2003/06/secext\" URI=\"%s\"></wsse:PolicyReference></wst:RequestSecurityToken>",(i + 1),ticket_domains[i][0],((ticket_domains[i][1] != ((char *)((void *)0)))?ticket_domains[i][1] : (nexus -> policy)));
  }
  request = g_strdup_printf("<\?xml version=\'1.0\' encoding=\'utf-8\'\?><Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsse=\"http://schemas.xmlsoap.org/ws/2003/06/secext\" xmlns:saml=\"urn:oasis:names:tc:SAML:1.0:assertion\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2002/12/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\" xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2004/03/addressing\" xmlns:wssc=\"http://schemas.xmlsoap.org/ws/2004/04/sc\" xmlns:wst=\"http://schemas.xmlsoap.org/ws/2004/04/trust\"><Header><ps:AuthInfo xmlns:ps=\"http://schemas.microsoft.com/Passport/SoapServices/PPCRL\" Id=\"PPAuthInfo\"><ps:HostingApp>{7108E71A-9926-4FCB-BCC9-9A9D3F32E423}</ps:HostingApp><ps:BinaryVersion>4</ps:BinaryVersion><ps:UIVersion>1</ps:UIVersion><ps:Cookies></ps:Cookies><ps:RequestParams>AQAAAAIAAABsYwQAAAAxMDMz</ps:RequestParams></ps:AuthInfo><wsse:Security><wsse:UsernameToken Id=\"user\"><wsse:Username>%s</wsse:Username><wsse:Password>%s</wsse:Password></wsse:UsernameToken></wsse:Security></Header><Body><ps:RequestMultipleSecurityTokens xmlns:ps=\"http://schemas.microsoft.com/Passport/SoapServices/PPCRL\" Id=\"RSTS\"><wst:RequestSecurityToken Id=\"RST0\"><wst:RequestType>http://schemas.xmlsoap.org/ws/2004/04/security/trust/Issue</wst:RequestType><wsp:AppliesTo><wsa:EndpointReference><wsa:Address>http://Passport.NET/tb</wsa:Address></wsa:EndpointReference></wsp:AppliesTo></wst:RequestSecurityToken>%s</ps:RequestMultipleSecurityTokens></Body></Envelope>",username,password_xml,(domains -> str));
  g_free(password_xml);
  g_string_free(domains,(!0));
  soap = msn_soap_message_new(0,xmlnode_from_str(request,(-1)));
  g_free(request);
  msn_soap_message_send(session,soap,"login.live.com","/RST.srf",(!0),nexus_got_response_cb,nexus);
}

static void nexus_got_update_cb(MsnSoapMessage *req,MsnSoapMessage *resp,gpointer data)
{
  MsnNexusUpdateData *ud = data;
  MsnNexus *nexus = (ud -> nexus);
  char iv[8UL] = {(0), (0), (0), (0), (0), (0), (0), (0)};
  xmlnode *enckey;
  char *tmp;
  char *nonce;
  gsize len;
  char *key;
  GSList *updates;
#if 0
#endif
  char *decrypted_data;
  if (resp == ((MsnSoapMessage *)((void *)0))) 
    return ;
  purple_debug_info("msn","Got Update Response for %s.\n",ticket_domains[ud -> id][0]);
  enckey = xmlnode_get_child((resp -> xml),"Header/Security/DerivedKeyToken");
{
    while(enckey != 0){
      if (g_str_equal((xmlnode_get_attrib(enckey,"Id")),"EncKey") != 0) 
        break; 
      enckey = xmlnode_get_next_twin(enckey);
    }
  }
  if (!(enckey != 0)) {
    purple_debug_error("msn","Invalid response in token update.\n");
    return ;
  }
  tmp = xmlnode_get_data((xmlnode_get_child(enckey,"Nonce")));
  nonce = ((char *)(purple_base64_decode(tmp,&len)));
  key = rps_create_key((nexus -> secret),24,nonce,len);
  g_free(tmp);
  g_free(nonce);
#if 0
/* Don't know what this is for yet */
#endif
  tmp = xmlnode_get_data((xmlnode_get_child((resp -> xml),"Body/EncryptedData/CipherData/CipherValue")));
  if (tmp != 0) {
    char *unescaped;
    xmlnode *rstresponse;
    unescaped = ((char *)(purple_base64_decode(tmp,&len)));
    g_free(tmp);
    decrypted_data = des3_cbc(key,iv,unescaped,len,(!0));
    g_free(unescaped);
    purple_debug_info("msn","Got Response Body EncryptedData: %s\n",decrypted_data);
    rstresponse = xmlnode_from_str(decrypted_data,(-1));
    if (g_str_equal((rstresponse -> name),"RequestSecurityTokenResponse") != 0) 
      nexus_parse_token(nexus,(ud -> id),rstresponse);
    else 
      nexus_parse_collection(nexus,(ud -> id),rstresponse);
    g_free(decrypted_data);
  }
  updates = (nexus -> tokens)[ud -> id].updates;
  (nexus -> tokens)[ud -> id].updates = ((GSList *)((void *)0));
  while(updates != ((GSList *)((void *)0))){
    MsnNexusUpdateCallback *update = (updates -> data);
    if ((update -> cb) != 0) 
      purple_timeout_add(0,(update -> cb),(update -> data));
    g_free(update);
    updates = g_slist_delete_link(updates,updates);
  }
  g_free(ud);
  g_free(key);
}

void msn_nexus_update_token(MsnNexus *nexus,int id,GSourceFunc cb,gpointer data)
{
  MsnSession *session = (nexus -> session);
  MsnNexusUpdateData *ud;
  MsnNexusUpdateCallback *update;
  PurpleCipherContext *sha1;
  PurpleCipherContext *hmac;
  char *key;
  guchar digest[20UL];
  struct tm *tm;
  time_t now;
  char *now_str;
  char *timestamp;
  char *timestamp_b64;
  char *domain;
  char *domain_b64;
  char *signedinfo;
  gint32 nonce[6UL];
  int i;
  char *nonce_b64;
  char *signature_b64;
  guchar signature[20UL];
  char *request;
  MsnSoapMessage *soap;
  update = ((MsnNexusUpdateCallback *)(g_malloc0_n(1,(sizeof(MsnNexusUpdateCallback )))));
  update -> cb = cb;
  update -> data = data;
  if ((nexus -> tokens)[id].updates != ((GSList *)((void *)0))) {
/* Update already in progress. Just add to list and return. */
    purple_debug_info("msn","Ticket update for user \'%s\' on domain \'%s\' in progress. Adding request to queue.\n",purple_account_get_username((session -> account)),ticket_domains[id][0]);
    (nexus -> tokens)[id].updates = g_slist_prepend((nexus -> tokens)[id].updates,update);
    return ;
  }
  else {
    purple_debug_info("msn","Updating ticket for user \'%s\' on domain \'%s\'\n",purple_account_get_username((session -> account)),ticket_domains[id][0]);
    (nexus -> tokens)[id].updates = g_slist_prepend((nexus -> tokens)[id].updates,update);
  }
  ud = ((MsnNexusUpdateData *)(g_malloc0_n(1,(sizeof(MsnNexusUpdateData )))));
  ud -> nexus = nexus;
  ud -> id = id;
  sha1 = purple_cipher_context_new_by_name("sha1",0);
  domain = g_strdup_printf("<wst:RequestSecurityToken xmlns=\"http://schemas.xmlsoap.org/ws/2004/04/trust\" Id=\"RST%d\"><wst:RequestType>http://schemas.xmlsoap.org/ws/2004/04/security/trust/Issue</wst:RequestType><wsp:AppliesTo xmlns=\"http://schemas.xmlsoap.org/ws/2002/12/policy\"><wsa:EndpointReference xmlns=\"http://schemas.xmlsoap.org/ws/2004/03/addressing\"><wsa:Address>%s</wsa:Address></wsa:EndpointReference></wsp:AppliesTo><wsse:PolicyReference xmlns=\"http://schemas.xmlsoap.org/ws/2003/06/secext\" URI=\"%s\"></wsse:PolicyReference></wst:RequestSecurityToken>",id,ticket_domains[id][0],((ticket_domains[id][1] != ((char *)((void *)0)))?ticket_domains[id][1] : (nexus -> policy)));
  purple_cipher_context_append(sha1,((guchar *)domain),strlen(domain));
  purple_cipher_context_digest(sha1,20,digest,0);
  domain_b64 = purple_base64_encode(digest,20);
  now = time(0);
  tm = gmtime((&now));
  now_str = g_strdup(purple_utf8_strftime("%Y-%m-%dT%H:%M:%SZ",tm));
  now += (5 * 60);
  tm = gmtime((&now));
  timestamp = g_strdup_printf("<wsu:Timestamp xmlns=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\" Id=\"Timestamp\"><wsu:Created>%s</wsu:Created><wsu:Expires>%s</wsu:Expires></wsu:Timestamp>",now_str,purple_utf8_strftime("%Y-%m-%dT%H:%M:%SZ",tm));
  purple_cipher_context_reset(sha1,0);
  purple_cipher_context_append(sha1,((guchar *)timestamp),strlen(timestamp));
  purple_cipher_context_digest(sha1,20,digest,0);
  timestamp_b64 = purple_base64_encode(digest,20);
  g_free(now_str);
  purple_cipher_context_destroy(sha1);
  signedinfo = g_strdup_printf("<SignedInfo xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><CanonicalizationMethod Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#\"></CanonicalizationMethod><SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#hmac-sha1\"></SignatureMethod><Reference URI=\"#RST%d\"><Transforms><Transform Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#\"></Transform></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"></DigestMethod><DigestValue>%s</DigestValue></Reference><Reference URI=\"#Timestamp\"><Transforms><Transform Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#\"></Transform></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"></DigestMethod><DigestValue>%s</DigestValue></Reference><Reference URI=\"#PPAuthInfo\"><Transforms><Transform Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#\"></Transform></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"></DigestMethod><DigestValue>d2IeTF4DAkPEa/tVETHznsivEpc=</DigestValue></Reference></SignedInfo>",id,domain_b64,timestamp_b64);
  for (i = 0; i < 6; i++) 
    nonce[i] = rand();
  nonce_b64 = purple_base64_encode(((guchar *)(&nonce)),(sizeof(nonce)));
  key = rps_create_key((nexus -> secret),24,((char *)nonce),(sizeof(nonce)));
  hmac = purple_cipher_context_new_by_name("hmac",0);
  purple_cipher_context_set_option(hmac,"hash","sha1");
  purple_cipher_context_set_key_with_len(hmac,((guchar *)key),24);
  purple_cipher_context_append(hmac,((guchar *)signedinfo),strlen(signedinfo));
  purple_cipher_context_digest(hmac,20,signature,0);
  purple_cipher_context_destroy(hmac);
  signature_b64 = purple_base64_encode(signature,20);
  request = g_strdup_printf("<\?xml version=\"1.0\" encoding=\"utf-8\"\?><Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsse=\"http://schemas.xmlsoap.org/ws/2003/06/secext\" xmlns:saml=\"urn:oasis:names:tc:SAML:1.0:assertion\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2002/12/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\" xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2004/03/addressing\" xmlns:wssc=\"http://schemas.xmlsoap.org/ws/2004/04/sc\" xmlns:wst=\"http://schemas.xmlsoap.org/ws/2004/04/trust\"><Header><ps:AuthInfo xmlns:ps=\"http://schemas.microsoft.com/Passport/SoapServices/PPCRL\" Id=\"PPAuthInfo\"><ps:HostingApp>{7108E71A-9926-4FCB-BCC9-9A9D3F32E423}</ps:HostingApp><ps:BinaryVersion>4</ps:BinaryVersion><ps:UIVersion>1</ps:UIVersion><ps:Cookies></ps:Cookies><ps:RequestParams>AQAAAAIAAABsYwQAAAA0MTA1</ps:RequestParams></ps:AuthInfo><wsse:Security><EncryptedData xmlns=\"http://www.w3.org/2001/04/xmlenc#\" Id=\"BinaryDAToken0\" Type=\"http://www.w3.org/2001/04/xmlenc#Element\"><EncryptionMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#tripledes-cbc\"></EncryptionMethod><ds:KeyInfo xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\"><ds:KeyName>http://Passport.NET/STS</ds:KeyName></ds:KeyInfo><CipherData><CipherValue>%s</CipherValue></CipherData></EncryptedData><wssc:DerivedKeyToken Id=\"SignKey\"><wsse:RequestedTokenReference><wsse:KeyIdentifier ValueType=\"http://docs.oasis-open.org/wss/2004/XX/oasis-2004XX-wss-saml-token-profile-1.0#SAMLAssertionID\" /><wsse:Reference URI=\"#BinaryDAToken0\" /></wsse:RequestedTokenReference><wssc:Nonce>%s</wssc:Nonce></wssc:DerivedKeyToken>%s<Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\">%s<SignatureValue>%s</SignatureValue><KeyInfo><wsse:SecurityTokenReference><wsse:Reference URI=\"#SignKey\" /></wsse:SecurityTokenReference></KeyInfo></Signature></wsse:Security></Header><Body>%s</Body></Envelope>",(nexus -> cipher),nonce_b64,timestamp,signedinfo,signature_b64,domain);
  g_free(nonce_b64);
  g_free(domain_b64);
  g_free(timestamp_b64);
  g_free(timestamp);
  g_free(key);
  g_free(signature_b64);
  g_free(signedinfo);
  g_free(domain);
  soap = msn_soap_message_new(0,xmlnode_from_str(request,(-1)));
  g_free(request);
  msn_soap_message_send(session,soap,"login.live.com","/RST.srf",(!0),nexus_got_update_cb,ud);
}

GHashTable *msn_nexus_get_token(MsnNexus *nexus,MsnAuthDomains id)
{
  do {
    if (nexus != ((MsnNexus *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"nexus != NULL");
      return 0;
    };
  }while (0);
  do {
    if (id < (nexus -> token_len)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id < nexus->token_len");
      return 0;
    };
  }while (0);
  return (nexus -> tokens)[id].token;
}

const char *msn_nexus_get_token_str(MsnNexus *nexus,MsnAuthDomains id)
{
  static char buf[1024UL];
  GHashTable *token = msn_nexus_get_token(nexus,id);
  const char *msn_t;
  const char *msn_p;
  gint ret;
  do {
    if (token != ((GHashTable *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"token != NULL");
      return 0;
    };
  }while (0);
  msn_t = (g_hash_table_lookup(token,"t"));
  msn_p = (g_hash_table_lookup(token,"p"));
  do {
    if (msn_t != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msn_t != NULL");
      return 0;
    };
  }while (0);
  do {
    if (msn_p != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msn_p != NULL");
      return 0;
    };
  }while (0);
  ret = g_snprintf(buf,(sizeof(buf) - 1),"t=%s&p=%s",msn_t,msn_p);
  do {
    if (ret != -1) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ret != -1");
      return 0;
    };
  }while (0);
  return buf;
}
