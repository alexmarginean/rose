/**
 * @file soap.c
 * Functions relating to SOAP connections.
 *
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,  USA
 */
#include "internal.h"
#include "soap.h"
#include "session.h"
#include "debug.h"
#include "xmlnode.h"
#include <glib.h>
#if !defined(_WIN32) || !defined(_WINERROR_)
#include <error.h>
#endif
#define SOAP_TIMEOUT (5 * 60)
typedef struct _MsnSoapRequest {
char *path;
MsnSoapMessage *message;
gboolean secure;
MsnSoapCallback cb;
gpointer cb_data;}MsnSoapRequest;
typedef struct _MsnSoapConnection {
MsnSession *session;
char *host;
time_t last_used;
PurpleSslConnection *ssl;
gboolean connected;
guint event_handle;
guint run_timer;
GString *buf;
gsize handled_len;
gsize body_len;
int response_code;
gboolean headers_done;
gboolean close_when_done;
MsnSoapMessage *message;
GQueue *queue;
MsnSoapRequest *current_request;}MsnSoapConnection;
static gboolean msn_soap_connection_run(gpointer data);

static MsnSoapConnection *msn_soap_connection_new(MsnSession *session,const char *host)
{
  MsnSoapConnection *conn = (MsnSoapConnection *)(g_malloc0_n(1,(sizeof(MsnSoapConnection ))));
  conn -> session = session;
  conn -> host = g_strdup(host);
  conn -> queue = g_queue_new();
  return conn;
}

static void msn_soap_message_destroy(MsnSoapMessage *message)
{
  g_slist_foreach((message -> headers),((GFunc )g_free),0);
  g_slist_free((message -> headers));
  g_free((message -> action));
  if ((message -> xml) != 0) 
    xmlnode_free((message -> xml));
  g_free(message);
}

static void msn_soap_request_destroy(MsnSoapRequest *req,gboolean keep_message)
{
  g_free((req -> path));
  if (!(keep_message != 0)) 
    msn_soap_message_destroy((req -> message));
  g_free(req);
}

static void msn_soap_connection_sanitize(MsnSoapConnection *conn,gboolean disconnect)
{
  if ((conn -> event_handle) != 0U) {
    purple_input_remove((conn -> event_handle));
    conn -> event_handle = 0;
  }
  if ((conn -> run_timer) != 0U) {
    purple_timeout_remove((conn -> run_timer));
    conn -> run_timer = 0;
  }
  if ((conn -> message) != 0) {
    msn_soap_message_destroy((conn -> message));
    conn -> message = ((MsnSoapMessage *)((void *)0));
  }
  if ((conn -> buf) != 0) {
    g_string_free((conn -> buf),(!0));
    conn -> buf = ((GString *)((void *)0));
  }
  if (((conn -> ssl) != 0) && ((disconnect != 0) || ((conn -> close_when_done) != 0))) {
    purple_ssl_close((conn -> ssl));
    conn -> ssl = ((PurpleSslConnection *)((void *)0));
  }
  if ((conn -> current_request) != 0) {
    msn_soap_request_destroy((conn -> current_request),0);
    conn -> current_request = ((MsnSoapRequest *)((void *)0));
  }
}

static void msn_soap_connection_destroy_foreach_cb(gpointer item,gpointer data)
{
  MsnSoapRequest *req = item;
  ( *(req -> cb))((req -> message),0,(req -> cb_data));
  msn_soap_request_destroy(req,0);
}

static void msn_soap_connection_destroy(MsnSoapConnection *conn)
{
  if ((conn -> current_request) != 0) {
    MsnSoapRequest *req = (conn -> current_request);
    conn -> current_request = ((MsnSoapRequest *)((void *)0));
    msn_soap_connection_destroy_foreach_cb(req,conn);
  }
  msn_soap_connection_sanitize(conn,(!0));
  g_queue_foreach((conn -> queue),msn_soap_connection_destroy_foreach_cb,conn);
  g_queue_free((conn -> queue));
  g_free((conn -> host));
  g_free(conn);
}

static gboolean msn_soap_cleanup_each(gpointer key,gpointer value,gpointer data)
{
  MsnSoapConnection *conn = value;
  time_t *t = data;
  if (( *t - (conn -> last_used)) > (5 * 60 * 2)) {
    purple_debug_info("soap","cleaning up soap conn %p\n",conn);
    return (!0);
  }
  return 0;
}

static gboolean msn_soap_cleanup_for_session(gpointer data)
{
  MsnSession *sess = data;
  time_t t = time(0);
  purple_debug_info("soap","session cleanup timeout\n");
  if ((sess -> soap_table) != 0) {
    g_hash_table_foreach_remove((sess -> soap_table),msn_soap_cleanup_each,(&t));
    if (g_hash_table_size((sess -> soap_table)) != 0) 
      return (!0);
  }
  sess -> soap_cleanup_handle = 0;
  return 0;
}

static MsnSoapConnection *msn_soap_get_connection(MsnSession *session,const char *host)
{
  MsnSoapConnection *conn = (MsnSoapConnection *)((void *)0);
  if ((session -> soap_table) != 0) {
    conn = (g_hash_table_lookup((session -> soap_table),host));
  }
  else {
    session -> soap_table = g_hash_table_new_full(g_str_hash,g_str_equal,0,((GDestroyNotify )msn_soap_connection_destroy));
  }
  if ((session -> soap_cleanup_handle) == 0) 
    session -> soap_cleanup_handle = purple_timeout_add_seconds((5 * 60),msn_soap_cleanup_for_session,session);
  if (conn == ((MsnSoapConnection *)((void *)0))) {
    conn = msn_soap_connection_new(session,host);
    g_hash_table_insert((session -> soap_table),(conn -> host),conn);
  }
  conn -> last_used = time(0);
  return conn;
}

static void msn_soap_connection_handle_next(MsnSoapConnection *conn)
{
  msn_soap_connection_sanitize(conn,0);
  conn -> run_timer = purple_timeout_add(0,msn_soap_connection_run,conn);
}

static void msn_soap_message_send_internal(MsnSession *session,MsnSoapMessage *message,const char *host,const char *path,gboolean secure,MsnSoapCallback cb,gpointer cb_data,gboolean first)
{
  MsnSoapConnection *conn = msn_soap_get_connection(session,host);
  MsnSoapRequest *req = (MsnSoapRequest *)(g_malloc0_n(1,(sizeof(MsnSoapRequest ))));
  req -> path = g_strdup(path);
  req -> message = message;
  req -> secure = secure;
  req -> cb = cb;
  req -> cb_data = cb_data;
  if (first != 0) {
    g_queue_push_head((conn -> queue),req);
  }
  else {
    g_queue_push_tail((conn -> queue),req);
  }
  if ((conn -> run_timer) == 0) 
    conn -> run_timer = purple_timeout_add(0,msn_soap_connection_run,conn);
}

void msn_soap_message_send(MsnSession *session,MsnSoapMessage *message,const char *host,const char *path,gboolean secure,MsnSoapCallback cb,gpointer cb_data)
{
  do {
    if (message != ((MsnSoapMessage *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"message != NULL");
      return ;
    };
  }while (0);
  do {
    if (cb != ((void (*)(MsnSoapMessage *, MsnSoapMessage *, gpointer ))((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cb != NULL");
      return ;
    };
  }while (0);
  msn_soap_message_send_internal(session,message,host,path,secure,cb,cb_data,0);
}

static gboolean msn_soap_handle_redirect(MsnSoapConnection *conn,const char *url)
{
  char *host;
  char *path;
  if (purple_url_parse(url,&host,0,&path,0,0) != 0) {
    MsnSoapRequest *req = (conn -> current_request);
    conn -> current_request = ((MsnSoapRequest *)((void *)0));
    msn_soap_message_send_internal((conn -> session),(req -> message),host,path,(req -> secure),(req -> cb),(req -> cb_data),(!0));
    msn_soap_request_destroy(req,(!0));
    g_free(host);
    g_free(path);
    return (!0);
  }
  return 0;
}

static gboolean msn_soap_handle_body(MsnSoapConnection *conn,MsnSoapMessage *response)
{
  xmlnode *body = xmlnode_get_child((response -> xml),"Body");
  xmlnode *fault = xmlnode_get_child((response -> xml),"Fault");
  if (fault != 0) {
    xmlnode *faultcode = xmlnode_get_child(fault,"faultcode");
    if (faultcode != ((xmlnode *)((void *)0))) {
      char *faultdata = xmlnode_get_data(faultcode);
      if ((faultdata != 0) && (g_str_equal(faultdata,"psf:Redirect") != 0)) {
        xmlnode *url = xmlnode_get_child(fault,"redirectUrl");
        if (url != 0) {
          char *urldata = xmlnode_get_data(url);
          if (urldata != 0) 
            msn_soap_handle_redirect(conn,urldata);
          g_free(urldata);
        }
        g_free(faultdata);
        msn_soap_message_destroy(response);
        return (!0);
      }
      else if ((faultdata != 0) && (g_str_equal(faultdata,"wsse:FailedAuthentication") != 0)) {
        xmlnode *reason = xmlnode_get_child(fault,"faultstring");
        char *reasondata = (char *)((void *)0);
        if (reason != 0) 
          reasondata = xmlnode_get_data(reason);
        msn_soap_connection_sanitize(conn,(!0));
        msn_session_set_error((conn -> session),MSN_ERROR_AUTH,reasondata);
        g_free(reasondata);
        g_free(faultdata);
        msn_soap_message_destroy(response);
        return 0;
      }
      g_free(faultdata);
    }
  }
  if ((fault != 0) || (body != 0)) {
    if ((conn -> current_request) != 0) {
      MsnSoapRequest *request = (conn -> current_request);
      conn -> current_request = ((MsnSoapRequest *)((void *)0));
      ( *(request -> cb))((request -> message),response,(request -> cb_data));
      msn_soap_request_destroy(request,0);
    }
    msn_soap_message_destroy(response);
  }
  return (!0);
}

static void msn_soap_message_add_header(MsnSoapMessage *message,const char *name,const char *value)
{
  char *header = g_strdup_printf("%s: %s\r\n",name,value);
  message -> headers = g_slist_prepend((message -> headers),header);
}

static void msn_soap_process(MsnSoapConnection *conn)
{
  gboolean handled = 0;
  char *cursor;
  char *linebreak;
  cursor = (( *(conn -> buf)).str + (conn -> handled_len));
  if (!((conn -> headers_done) != 0)) {{
      while((linebreak = strstr(cursor,"\r\n")) != ((char *)((void *)0))){
        conn -> handled_len = ((linebreak - ( *(conn -> buf)).str) + 2);
        if ((conn -> response_code) == 0) {
          if (sscanf(cursor,"HTTP/1.1 %d",&conn -> response_code) != 1) {
/* something horribly wrong */
            purple_ssl_close((conn -> ssl));
            conn -> ssl = ((PurpleSslConnection *)((void *)0));
            handled = (!0);
            break; 
          }
          else if (((conn -> response_code) == 503) && (( *(conn -> session)).login_step < MSN_LOGIN_STEP_END)) {
            msn_soap_connection_sanitize(conn,(!0));
            msn_session_set_error((conn -> session),MSN_ERROR_SERV_UNAVAILABLE,0);
            return ;
          }
        }
        else if (cursor == linebreak) {
/* blank line */
          conn -> headers_done = (!0);
          cursor = (( *(conn -> buf)).str + (conn -> handled_len));
          break; 
        }
        else {
          char *line = g_strndup(cursor,(linebreak - cursor));
          char *sep = strstr(line,": ");
          char *key = line;
          char *value;
          if (sep == ((char *)((void *)0))) {
            purple_debug_info("soap","ignoring malformed line: %s\n",line);
            g_free(line);
            goto loop_end;
          }
          value = (sep + 2);
           *sep = 0;
          msn_soap_message_add_header((conn -> message),key,value);
          if ((((conn -> response_code) == 301) || ((conn -> response_code) == 300)) && (strcmp(key,"Location") == 0)) {
            msn_soap_handle_redirect(conn,value);
            handled = (!0);
            g_free(line);
            break; 
          }
          else if (((conn -> response_code) == 401) && (strcmp(key,"WWW-Authenticate") == 0)) {
            char *error = strstr(value,"cbtxt=");
            if (error != 0) {
              error += strlen("cbtxt=");
            }
            msn_soap_connection_sanitize(conn,(!0));
            msn_session_set_error((conn -> session),MSN_ERROR_AUTH,((error != 0)?purple_url_decode(error) : ((const char *)((void *)0))));
            g_free(line);
            return ;
          }
          else if (strcmp(key,"Content-Length") == 0) {
            if (sscanf(value,"%lu",&conn -> body_len) != 1) 
              purple_debug_error("soap","Unable to parse Content-Length\n");
          }
          else if (strcmp(key,"Connection") == 0) {
            if (strcmp(value,"close") == 0) {
              conn -> close_when_done = (!0);
            }
          }
          g_free(line);
        }
        loop_end:
        cursor = (( *(conn -> buf)).str + (conn -> handled_len));
      }
    }
  }
  if (!(handled != 0) && ((conn -> headers_done) != 0)) {
    if ((( *(conn -> buf)).len - (conn -> handled_len)) >= (conn -> body_len)) {
      xmlnode *node = xmlnode_from_str(cursor,(conn -> body_len));
      if (node == ((xmlnode *)((void *)0))) {
        purple_debug_info("soap","Malformed SOAP response: %s\n",cursor);
      }
      else {
        MsnSoapMessage *message = (conn -> message);
        conn -> message = ((MsnSoapMessage *)((void *)0));
        message -> xml = node;
        if (!(msn_soap_handle_body(conn,message) != 0)) {
          return ;
        }
      }
      msn_soap_connection_handle_next(conn);
    }
    return ;
  }
  if (handled != 0) {
    msn_soap_connection_handle_next(conn);
  }
}

static void msn_soap_read_cb(gpointer data,gint fd,PurpleInputCondition cond)
{
  MsnSoapConnection *conn = data;
  int count = 0;
  int cnt;
  int perrno;
/* This buffer needs to be larger than any packets received from
		login.live.com or Adium will fail to receive the packet
		(something weird with the login.live.com server). With NSS it works
		fine, so I believe it's some bug with OS X */
  char buf[16384UL];
  gsize cursor;
  if ((conn -> message) == ((MsnSoapMessage *)((void *)0))) {
    conn -> message = msn_soap_message_new(0,0);
  }
  if ((conn -> buf) == ((GString *)((void *)0))) {
    conn -> buf = g_string_new_len(buf,0);
  }
  cursor = ( *(conn -> buf)).len;
  while((cnt = (purple_ssl_read((conn -> ssl),buf,(sizeof(buf))))) > 0){
    purple_debug_info("soap","read %d bytes\n",cnt);
    count += cnt;
    g_string_append_len((conn -> buf),buf,cnt);
  }
  perrno =  *__errno_location();
  if ((cnt < 0) && (perrno != 11)) 
    purple_debug_info("soap","read: %s\n",g_strerror(perrno));
  if ((((conn -> current_request) != 0) && (( *(conn -> current_request)).secure != 0)) && !(purple_debug_is_unsafe() != 0)) 
    purple_debug_misc("soap","Received secure request.\n");
  else if (count != 0) 
    purple_debug_misc("soap","current %s\n",(( *(conn -> buf)).str + cursor));
/* && count is necessary for Adium, on OS X the last read always
	   return an error, so we want to proceed anyway. See #5212 for
	   discussion on this and the above buffer size issues */
  if (((cnt < 0) && ( *__errno_location() == 11)) && (count == 0)) 
    return ;
/* msn_soap_process could alter errno */
  msn_soap_process(conn);
  if (((cnt < 0) && (perrno != 11)) || (cnt == 0)) {
/* It's possible msn_soap_process closed the ssl connection */
    if ((conn -> ssl) != 0) {
      purple_ssl_close((conn -> ssl));
      conn -> ssl = ((PurpleSslConnection *)((void *)0));
      msn_soap_connection_handle_next(conn);
    }
  }
}

static gboolean msn_soap_write_cb_internal(gpointer data,gint fd,PurpleInputCondition cond,gboolean initial)
{
  MsnSoapConnection *conn = data;
  int written;
  if (cond != PURPLE_INPUT_WRITE) 
    return (!0);
  written = (purple_ssl_write((conn -> ssl),(( *(conn -> buf)).str + (conn -> handled_len)),(( *(conn -> buf)).len - (conn -> handled_len))));
  if ((written < 0) && ( *__errno_location() == 11)) 
    return (!0);
  else if (written <= 0) {
    purple_ssl_close((conn -> ssl));
    conn -> ssl = ((PurpleSslConnection *)((void *)0));
    if (!(initial != 0)) 
      msn_soap_connection_handle_next(conn);
    return 0;
  }
  conn -> handled_len += written;
  if ((conn -> handled_len) < ( *(conn -> buf)).len) 
    return (!0);
/* we are done! */
  g_string_free((conn -> buf),(!0));
  conn -> buf = ((GString *)((void *)0));
  conn -> handled_len = 0;
  conn -> body_len = 0;
  conn -> response_code = 0;
  conn -> headers_done = 0;
  conn -> close_when_done = 0;
  purple_input_remove((conn -> event_handle));
  conn -> event_handle = purple_input_add(( *(conn -> ssl)).fd,PURPLE_INPUT_READ,msn_soap_read_cb,conn);
  return (!0);
}

static void msn_soap_write_cb(gpointer data,gint fd,PurpleInputCondition cond)
{
  msn_soap_write_cb_internal(data,fd,cond,0);
}

static void msn_soap_error_cb(PurpleSslConnection *ssl,PurpleSslErrorType error,gpointer data)
{
  MsnSoapConnection *conn = data;
/* sslconn already frees the connection in case of error */
  conn -> ssl = ((PurpleSslConnection *)((void *)0));
  g_hash_table_remove(( *(conn -> session)).soap_table,(conn -> host));
}

static void msn_soap_connected_cb(gpointer data,PurpleSslConnection *ssl,PurpleInputCondition cond)
{
  MsnSoapConnection *conn = data;
  conn -> connected = (!0);
  if ((conn -> run_timer) == 0) 
    conn -> run_timer = purple_timeout_add(0,msn_soap_connection_run,conn);
}

MsnSoapMessage *msn_soap_message_new(const char *action,xmlnode *xml)
{
  MsnSoapMessage *message = (MsnSoapMessage *)(g_malloc0_n(1,(sizeof(MsnSoapMessage ))));
  message -> action = g_strdup(action);
  message -> xml = xml;
  return message;
}

static gboolean msn_soap_connection_run(gpointer data)
{
  MsnSoapConnection *conn = data;
  MsnSoapRequest *req = (g_queue_peek_head((conn -> queue)));
  conn -> run_timer = 0;
  if (req != 0) {
    if ((conn -> ssl) == ((PurpleSslConnection *)((void *)0))) {
      conn -> ssl = purple_ssl_connect(( *(conn -> session)).account,(conn -> host),443,msn_soap_connected_cb,msn_soap_error_cb,conn);
    }
    else if ((conn -> connected) != 0) {
      int len = -1;
      char *body = xmlnode_to_str(( *(req -> message)).xml,&len);
      GSList *iter;
      g_queue_pop_head((conn -> queue));
      conn -> buf = g_string_new("");
      g_string_append_printf((conn -> buf),"POST /%s HTTP/1.1\r\nSOAPAction: %s\r\nContent-Type:text/xml; charset=utf-8\r\nUser-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)\r\nAccept: */*\r\nHost: %s\r\nContent-Length: %d\r\nConnection: Keep-Alive\r\nCache-Control: no-cache\r\n",(req -> path),((( *(req -> message)).action != 0)?( *(req -> message)).action : ""),(conn -> host),len);
      for (iter = ( *(req -> message)).headers; iter != 0; iter = (iter -> next)) {
        g_string_append((conn -> buf),((char *)(iter -> data)));
        g_string_append((conn -> buf),"\r\n");
      }
      g_string_append((conn -> buf),"\r\n");
      g_string_append((conn -> buf),body);
      if (((req -> secure) != 0) && !(purple_debug_is_unsafe() != 0)) 
        purple_debug_misc("soap","Sending secure request.\n");
      else 
        purple_debug_misc("soap","%s\n",( *(conn -> buf)).str);
      conn -> handled_len = 0;
      conn -> current_request = req;
      if ((conn -> event_handle) != 0U) 
        purple_input_remove((conn -> event_handle));
      conn -> event_handle = purple_input_add(( *(conn -> ssl)).fd,PURPLE_INPUT_WRITE,msn_soap_write_cb,conn);
      if (!(msn_soap_write_cb_internal(conn,( *(conn -> ssl)).fd,PURPLE_INPUT_WRITE,(!0)) != 0)) {
/* Not connected => reconnect and retry */
        purple_debug_info("soap","not connected, reconnecting\n");
        conn -> connected = 0;
        conn -> current_request = ((MsnSoapRequest *)((void *)0));
        msn_soap_connection_sanitize(conn,0);
        g_queue_push_head((conn -> queue),req);
        conn -> run_timer = purple_timeout_add(0,msn_soap_connection_run,conn);
      }
      g_free(body);
    }
  }
  return 0;
}
