/**
 * @file xfer.c MSN File Transfer functions
 *
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "debug.h"
#include "msnutils.h"
#include "sbconn.h"
#include "xfer.h"
/**************************************************************************
 * Xfer
 **************************************************************************/

void msn_xfer_init(PurpleXfer *xfer)
{
  MsnSlpCall *slpcall;
/* MsnSlpLink *slplink; */
  char *content;
  purple_debug_info("msn","xfer_init\n");
  slpcall = (xfer -> data);
/* Send Ok */
  content = g_strdup_printf("SessionID: %lu\r\n\r\n",(slpcall -> session_id));
  msn_slp_send_ok(slpcall,(slpcall -> branch),"application/x-msnmsgr-sessionreqbody",content);
  g_free(content);
  msn_slplink_send_queued_slpmsgs((slpcall -> slplink));
}

void msn_xfer_cancel(PurpleXfer *xfer)
{
  MsnSlpCall *slpcall;
  char *content;
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return ;
    };
  }while (0);
  do {
    if ((xfer -> data) != ((void *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer->data != NULL");
      return ;
    };
  }while (0);
  slpcall = (xfer -> data);
  if ((purple_xfer_get_status(xfer)) == PURPLE_XFER_STATUS_CANCEL_LOCAL) {
    if ((slpcall -> started) != 0) {
      msn_slpcall_close(slpcall);
    }
    else {
      content = g_strdup_printf("SessionID: %lu\r\n\r\n",(slpcall -> session_id));
      msn_slp_send_decline(slpcall,(slpcall -> branch),"application/x-msnmsgr-sessionreqbody",content);
      g_free(content);
      msn_slplink_send_queued_slpmsgs((slpcall -> slplink));
      if ((purple_xfer_get_type(xfer)) == PURPLE_XFER_SEND) 
        slpcall -> wasted = (!0);
      else 
        msn_slpcall_destroy(slpcall);
    }
  }
}

gssize msn_xfer_write(const guchar *data,gsize len,PurpleXfer *xfer)
{
  MsnSlpCall *slpcall;
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return (-1);
    };
  }while (0);
  do {
    if (data != ((const guchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"data != NULL");
      return (-1);
    };
  }while (0);
  do {
    if (len > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"len > 0");
      return (-1);
    };
  }while (0);
  do {
    if ((purple_xfer_get_type(xfer)) == PURPLE_XFER_SEND) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_xfer_get_type(xfer) == PURPLE_XFER_SEND");
      return (-1);
    };
  }while (0);
  slpcall = (xfer -> data);
/* Not sure I trust it'll be there */
  do {
    if (slpcall != ((MsnSlpCall *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"slpcall != NULL");
      return (-1);
    };
  }while (0);
  do {
    if ((slpcall -> xfer_msg) != ((MsnSlpMessage *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"slpcall->xfer_msg != NULL");
      return (-1);
    };
  }while (0);
  slpcall -> u.outgoing.len = len;
  slpcall -> u.outgoing.data = data;
  msn_slplink_send_msgpart((slpcall -> slplink),(slpcall -> xfer_msg));
  return ((1202 < len)?1202 : len);
}

gssize msn_xfer_read(guchar **data,PurpleXfer *xfer)
{
  MsnSlpCall *slpcall;
  gsize len;
  do {
    if (xfer != ((PurpleXfer *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"xfer != NULL");
      return (-1);
    };
  }while (0);
  do {
    if (data != ((guchar **)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"data != NULL");
      return (-1);
    };
  }while (0);
  do {
    if ((purple_xfer_get_type(xfer)) == PURPLE_XFER_RECEIVE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_xfer_get_type(xfer) == PURPLE_XFER_RECEIVE");
      return (-1);
    };
  }while (0);
  slpcall = (xfer -> data);
/* Not sure I trust it'll be there */
  do {
    if (slpcall != ((MsnSlpCall *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"slpcall != NULL");
      return (-1);
    };
  }while (0);
/* Just pass up the whole GByteArray. We'll make another. */
   *data = ( *slpcall -> u.incoming_data).data;
  len = ( *slpcall -> u.incoming_data).len;
  g_byte_array_free(slpcall -> u.incoming_data,0);
  slpcall -> u.incoming_data = g_byte_array_new();
  return len;
}

void msn_xfer_end_cb(MsnSlpCall *slpcall,MsnSession *session)
{
  if ((((purple_xfer_get_status((slpcall -> xfer))) != PURPLE_XFER_STATUS_DONE) && ((purple_xfer_get_status((slpcall -> xfer))) != PURPLE_XFER_STATUS_CANCEL_REMOTE)) && ((purple_xfer_get_status((slpcall -> xfer))) != PURPLE_XFER_STATUS_CANCEL_LOCAL)) {
    purple_xfer_cancel_remote((slpcall -> xfer));
  }
}

void msn_xfer_completed_cb(MsnSlpCall *slpcall,const guchar *body,gsize size)
{
  PurpleXfer *xfer = (slpcall -> xfer);
  purple_xfer_set_completed(xfer,(!0));
  purple_xfer_end(xfer);
}

gchar *msn_file_context_to_wire(MsnFileContext *context)
{
  gchar *ret;
  gchar *tmp;
  tmp = (ret = ((gchar *)(g_malloc_n((((4 * 4 + 1 * 8 + 2 * 260 + 30) + (context -> preview_len)) + 1),(sizeof(gchar ))))));
  (msn_write32le(tmp,(context -> length)) , (tmp += 4));
  (msn_write32le(tmp,(context -> version)) , (tmp += 4));
  (msn_write64le(tmp,(context -> file_size)) , (tmp += 8));
  (msn_write32le(tmp,(context -> type)) , (tmp += 4));
  memcpy(tmp,(context -> file_name),(260 * 2));
  tmp += 260 * 2;
  memcpy(tmp,(context -> unknown1),(sizeof(context -> unknown1)));
  tmp += sizeof(context -> unknown1);
  (msn_write32le(tmp,(context -> unknown2)) , (tmp += 4));
  if ((context -> preview) != 0) {
    memcpy(tmp,(context -> preview),(context -> preview_len));
  }
  tmp[context -> preview_len] = 0;
  return ret;
}

MsnFileContext *msn_file_context_from_wire(const char *buf,gsize len)
{
  MsnFileContext *context;
  if (!(buf != 0) || (len < (4 * 4 + 1 * 8 + 2 * 260 + 30))) 
    return 0;
  context = ((MsnFileContext *)(g_malloc_n(1,(sizeof(MsnFileContext )))));
  context -> length = msn_read32le(((buf += 4) - 4));
  context -> version = msn_read32le(((buf += 4) - 4));
  if ((context -> version) == 2) {
/* The length field is broken for this version. No check. */
    context -> length = (4 * 4 + 1 * 8 + 2 * 260 + 30);
  }
  else if ((context -> version) == 3) {
    if ((context -> length) != (4 * 4 + 1 * 8 + 2 * 260 + 30 + 63)) {
      g_free(context);
      return 0;
    }
    else if (len < (4 * 4 + 1 * 8 + 2 * 260 + 30 + 63)) {
      g_free(context);
      return 0;
    }
  }
  else {
    purple_debug_warning("msn","Received MsnFileContext with unknown version: %d\n",(context -> version));
    g_free(context);
    return 0;
  }
  context -> file_size = msn_read64le(((buf += 8) - 8));
  context -> type = msn_read32le(((buf += 4) - 4));
  memcpy((context -> file_name),buf,(260 * 2));
  buf += 260 * 2;
  memcpy((context -> unknown1),buf,(sizeof(context -> unknown1)));
  buf += sizeof(context -> unknown1);
  context -> unknown2 = msn_read32le(((buf += 4) - 4));
  if (((context -> type) == 0) && (len > (context -> length))) {
    context -> preview_len = (len - (context -> length));
    context -> preview = (g_memdup(buf,(context -> preview_len)));
  }
  else {
    context -> preview_len = 0;
    context -> preview = ((gchar *)((void *)0));
  }
  return context;
}
