/**
 * @file notification.c Notification server functions
 *
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "cipher.h"
#include "core.h"
#include "debug.h"
#include "notification.h"
#include "contact.h"
#include "error.h"
#include "msnutils.h"
#include "state.h"
#include "userlist.h"
static MsnTable *cbs_table;
/**************************************************************************
 * Main
 **************************************************************************/

static void destroy_cb(MsnServConn *servconn)
{
  MsnNotification *notification;
  notification = ( *(servconn -> cmdproc)).data;
  do {
    if (notification != ((MsnNotification *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"notification != NULL");
      return ;
    };
  }while (0);
  msn_notification_destroy(notification);
}

MsnNotification *msn_notification_new(MsnSession *session)
{
  MsnNotification *notification;
  MsnServConn *servconn;
  do {
    if (session != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return 0;
    };
  }while (0);
  notification = ((MsnNotification *)(g_malloc0_n(1,(sizeof(MsnNotification )))));
  notification -> session = session;
  notification -> servconn = (servconn = msn_servconn_new(session,MSN_SERVCONN_NS));
  msn_servconn_set_destroy_cb(servconn,destroy_cb);
  notification -> cmdproc = (servconn -> cmdproc);
  ( *(notification -> cmdproc)).data = notification;
  ( *(notification -> cmdproc)).cbs_table = cbs_table;
  return notification;
}

void msn_notification_destroy(MsnNotification *notification)
{
  ( *(notification -> cmdproc)).data = ((void *)((void *)0));
  msn_servconn_set_destroy_cb((notification -> servconn),0);
  msn_servconn_destroy((notification -> servconn));
  g_free(notification);
}
/**************************************************************************
 * Connect
 **************************************************************************/

static void connect_cb(MsnServConn *servconn)
{
  MsnCmdProc *cmdproc;
  MsnSession *session;
  MsnTransaction *trans;
  GString *vers;
  const char *ver_str;
  int i;
  do {
    if (servconn != ((MsnServConn *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"servconn != NULL");
      return ;
    };
  }while (0);
  cmdproc = (servconn -> cmdproc);
  session = (servconn -> session);
  vers = g_string_new("");
  for (i = 18; i >= 18; i--) 
    g_string_append_printf(vers," MSNP%d",i);
  g_string_append(vers," CVR0");
  if ((session -> login_step) == MSN_LOGIN_STEP_START) 
    msn_session_set_login_step(session,MSN_LOGIN_STEP_HANDSHAKE);
  else 
    msn_session_set_login_step(session,MSN_LOGIN_STEP_HANDSHAKE2);
/* Skip the initial space */
  ver_str = ((vers -> str) + 1);
  trans = msn_transaction_new(cmdproc,"VER","%s",ver_str);
  msn_cmdproc_send_trans(cmdproc,trans);
  g_string_free(vers,(!0));
}

gboolean msn_notification_connect(MsnNotification *notification,const char *host,int port)
{
  MsnServConn *servconn;
  do {
    if (notification != ((MsnNotification *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"notification != NULL");
      return 0;
    };
  }while (0);
  servconn = (notification -> servconn);
  msn_servconn_set_connect_cb(servconn,connect_cb);
  notification -> in_use = msn_servconn_connect(servconn,host,port,(!0));
  return notification -> in_use;
}

void msn_notification_disconnect(MsnNotification *notification)
{
  do {
    if (notification != ((MsnNotification *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"notification != NULL");
      return ;
    };
  }while (0);
  do {
    if ((notification -> in_use) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"notification->in_use");
      return ;
    };
  }while (0);
  msn_servconn_disconnect((notification -> servconn));
  notification -> in_use = 0;
}
/**************************************************************************
 * Login
 **************************************************************************/

void msn_got_login_params(MsnSession *session,const char *ticket,const char *response)
{
  MsnCmdProc *cmdproc;
  MsnTransaction *trans;
  cmdproc = ( *(session -> notification)).cmdproc;
  msn_session_set_login_step(session,MSN_LOGIN_STEP_AUTH_END);
  trans = msn_transaction_new(cmdproc,"USR","SSO S %s %s %s",ticket,response,(session -> guid));
  msn_cmdproc_send_trans(cmdproc,trans);
}

static void cvr_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  PurpleAccount *account;
  MsnTransaction *trans;
  account = ( *(cmdproc -> session)).account;
  trans = msn_transaction_new(cmdproc,"USR","SSO I %s",purple_account_get_username(account));
  msn_cmdproc_send_trans(cmdproc,trans);
}

static void usr_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  MsnSession *session = (cmdproc -> session);
  if (!(g_ascii_strcasecmp((cmd -> params)[1],"OK") != 0)) {
/* authenticate OK */
    msn_session_set_login_step(session,MSN_LOGIN_STEP_SYN);
  }
  else if (!(g_ascii_strcasecmp((cmd -> params)[1],"SSO") != 0)) {
/* RPS authentication */
    if ((session -> nexus) != 0) 
      msn_nexus_destroy((session -> nexus));
    session -> nexus = msn_nexus_new(session);
    ( *(session -> nexus)).policy = g_strdup((cmd -> params)[3]);
    ( *(session -> nexus)).nonce = g_strdup((cmd -> params)[4]);
    msn_session_set_login_step(session,MSN_LOGIN_STEP_AUTH_START);
    msn_nexus_connect((session -> nexus));
  }
}

static void usr_error(MsnCmdProc *cmdproc,MsnTransaction *trans,int error)
{
  MsnErrorType msnerr = 0;
  switch(error){
    case 500:
{
    }
    case 601:
{
    }
    case 910:
{
    }
    case 921:
{
      msnerr = MSN_ERROR_SERV_UNAVAILABLE;
      break; 
    }
    case 911:
{
      msnerr = MSN_ERROR_AUTH;
      break; 
    }
    default:
{
      return ;
      break; 
    }
  }
  msn_session_set_error((cmdproc -> session),msnerr,0);
}

static void ver_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  MsnSession *session;
  MsnTransaction *trans;
  PurpleAccount *account;
  gboolean protocol_supported = 0;
  int proto_ver;
  size_t i;
  session = (cmdproc -> session);
  account = (session -> account);
  session -> protocol_ver = 0;
  for (i = 1; i < (cmd -> param_count); i++) {
    if (sscanf((cmd -> params)[i],"MSNP%d",&proto_ver) == 1) {
      if (((proto_ver >= 18) && (proto_ver <= 18)) && (proto_ver > (session -> protocol_ver))) {
        protocol_supported = (!0);
        session -> protocol_ver = proto_ver;
      }
    }
  }
  if (!(protocol_supported != 0)) {
    msn_session_set_error(session,MSN_ERROR_UNSUPPORTED_PROTOCOL,0);
    return ;
  }
  purple_debug_info("msn","Negotiated protocol version %d with the server.\n",(session -> protocol_ver));
/*
	 * Windows Live Messenger 8.5
	 * Notice :CVR String discriminate!
	 * reference of http://www.microsoft.com/globaldev/reference/oslocversion.mspx
	 * to see the Local ID
	 */
  trans = msn_transaction_new(cmdproc,"CVR","0x0409 winnt 5.1 i386 MSNMSGR 8.5.1302 BC01 %s",purple_account_get_username(account));
  msn_cmdproc_send_trans(cmdproc,trans);
}
/**************************************************************************
 * Log out
 **************************************************************************/

static void out_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  if ((cmd -> param_count) == 0) 
    msn_session_set_error((cmdproc -> session),(-1),0);
  else if (!(g_ascii_strcasecmp((cmd -> params)[0],"OTH") != 0)) 
    msn_session_set_error((cmdproc -> session),MSN_ERROR_SIGN_OTHER,0);
  else if (!(g_ascii_strcasecmp((cmd -> params)[0],"SSD") != 0)) 
    msn_session_set_error((cmdproc -> session),MSN_ERROR_SERV_DOWN,0);
}

void msn_notification_close(MsnNotification *notification)
{
  MsnTransaction *trans;
  do {
    if (notification != ((MsnNotification *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"notification != NULL");
      return ;
    };
  }while (0);
  if (!((notification -> in_use) != 0)) 
    return ;
  trans = msn_transaction_new((notification -> cmdproc),"OUT",0);
  msn_transaction_set_saveable(trans,0);
  msn_cmdproc_send_trans((notification -> cmdproc),trans);
  msn_notification_disconnect(notification);
}
/**************************************************************************
 * Messages
 **************************************************************************/

static void msg_cmd_post(MsnCmdProc *cmdproc,MsnCommand *cmd,char *payload,size_t len)
{
  MsnMessage *msg;
  msg = msn_message_new_from_cmd((cmdproc -> session),cmd);
  msn_message_parse_payload(msg,payload,len,"\r\n","\r\n\r\n");
  if (purple_debug_is_verbose() != 0) 
    msn_message_show_readable(msg,"Notification",(!0));
  msn_cmdproc_process_msg(cmdproc,msg);
  msn_message_unref(msg);
}

static void msg_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  purple_debug_info("msn","Processing MSG... \n");
/* NOTE: cmd is not always cmdproc->last_cmd, sometimes cmd is a queued
	 * command and we are processing it */
  if ((cmd -> payload) == ((char *)((void *)0))) {
    ( *(cmdproc -> last_cmd)).payload_cb = msg_cmd_post;
    cmd -> payload_len = (atoi((cmd -> params)[2]));
  }
  else {
    do {
      if ((cmd -> payload_cb) != ((void (*)(MsnCmdProc *, MsnCommand *, char *, size_t ))((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"cmd->payload_cb != NULL");
        return ;
      };
    }while (0);
#if 0 /* glib on win32 doesn't correctly support precision modifiers for a string */
#endif
    ( *(cmd -> payload_cb))(cmdproc,cmd,(cmd -> payload),(cmd -> payload_len));
  }
}
/*send Message to Yahoo Messenger*/

void msn_notification_send_uum(MsnSession *session,MsnMessage *msg)
{
  MsnCmdProc *cmdproc;
  MsnTransaction *trans;
  char *payload;
  gsize payload_len;
  int type;
  MsnUser *user;
  int network;
  do {
    if (msg != ((MsnMessage *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg != NULL");
      return ;
    };
  }while (0);
  cmdproc = ( *(session -> notification)).cmdproc;
  payload = msn_message_gen_payload(msg,&payload_len);
  type = (msg -> type);
  user = msn_userlist_find_user((session -> userlist),(msg -> remote_user));
  if (user != 0) 
    network = (msn_user_get_network(user));
  else 
    network = MSN_NETWORK_PASSPORT;
  purple_debug_info("msn","send UUM, payload{%s}, strlen:%lu, len:%lu\n",payload,strlen(payload),payload_len);
  trans = msn_transaction_new(cmdproc,"UUM","%s %d %d %lu",(msg -> remote_user),network,type,payload_len);
  msn_transaction_set_payload(trans,payload,(strlen(payload)));
  msn_cmdproc_send_trans(cmdproc,trans);
}
/*Yahoo msg process*/

static void ubm_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  purple_debug_info("msn","Processing UBM... \n");
/* NOTE: cmd is not always cmdproc->last_cmd, sometimes cmd is a queued
	 * command and we are processing it */
  if ((cmd -> payload) == ((char *)((void *)0))) {
    ( *(cmdproc -> last_cmd)).payload_cb = msg_cmd_post;
    cmd -> payload_len = (atoi((cmd -> params)[5]));
  }
  else {
    do {
      if ((cmd -> payload_cb) != ((void (*)(MsnCmdProc *, MsnCommand *, char *, size_t ))((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"cmd->payload_cb != NULL");
        return ;
      };
    }while (0);
    purple_debug_info("msn","UBM payload:{%.*s}\n",((guint )(cmd -> payload_len)),(cmd -> payload));
    msg_cmd_post(cmdproc,cmd,(cmd -> payload),(cmd -> payload_len));
  }
}
/**************************************************************************
 * Challenges
 *  we use MD5 to caculate the Challenges
 **************************************************************************/

static void chl_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  MsnTransaction *trans;
  char buf[33UL];
  msn_handle_chl((cmd -> params)[1],buf);
  trans = msn_transaction_new(cmdproc,"QRY","%s 32","PROD0119GSJUC$18");
  msn_transaction_set_payload(trans,buf,32);
  msn_cmdproc_send_trans(cmdproc,trans);
}
/**************************************************************************
 * Buddy Lists
 **************************************************************************/
typedef struct MsnFqyCbData {
MsnFqyCb cb;
gpointer data;}MsnFqyCbData;
/* add contact to xmlnode */

static void msn_add_contact_xml(xmlnode *mlNode,const char *passport,MsnListOp list_op,MsnNetwork networkId)
{
  xmlnode *d_node;
  xmlnode *c_node;
  char **tokens;
  const char *email;
  const char *domain;
  char fmt_str[3UL];
  do {
    if (passport != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"passport != NULL");
      return ;
    };
  }while (0);
  purple_debug_info("msn","Passport: %s, type: %d\n",passport,networkId);
  tokens = g_strsplit(passport,"@",2);
  email = tokens[0];
  domain = tokens[1];
  if ((email == ((const char *)((void *)0))) || (domain == ((const char *)((void *)0)))) {
    purple_debug_error("msn","Invalid passport (%s) specified to add to contact xml.\n",passport);
    g_strfreev(tokens);
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","notification.c",456,((const char *)__func__));
      return ;
    }while (0);
  }
{
/*find a domain Node*/
    for (d_node = xmlnode_get_child(mlNode,"d"); d_node != 0; d_node = xmlnode_get_next_twin(d_node)) {{
        const char *attr = xmlnode_get_attrib(d_node,"n");
        if (attr == ((const char *)((void *)0))) 
          continue; 
        if (!(strcmp(attr,domain) != 0)) 
          break; 
      }
    }
  }
  if (d_node == ((xmlnode *)((void *)0))) {
/*domain not found, create a new domain Node*/
    purple_debug_info("msn","Didn\'t find existing domain node, adding one.\n");
    d_node = xmlnode_new("d");
    xmlnode_set_attrib(d_node,"n",domain);
    xmlnode_insert_child(mlNode,d_node);
  }
/*create contact node*/
  c_node = xmlnode_new("c");
  xmlnode_set_attrib(c_node,"n",email);
  if (list_op != 0) {
    purple_debug_info("msn","list_op: %d\n",list_op);
    g_snprintf(fmt_str,(sizeof(fmt_str)),"%d",list_op);
    xmlnode_set_attrib(c_node,"l",fmt_str);
  }
  if (networkId != MSN_NETWORK_UNKNOWN) {
    g_snprintf(fmt_str,(sizeof(fmt_str)),"%d",networkId);
/*mobile*/
/*type_str = g_strdup_printf("4");*/
    xmlnode_set_attrib(c_node,"t",fmt_str);
  }
  xmlnode_insert_child(d_node,c_node);
  g_strfreev(tokens);
}

static void msn_notification_post_adl(MsnCmdProc *cmdproc,const char *payload,int payload_len)
{
  MsnTransaction *trans;
  purple_debug_info("msn","Sending ADL with payload: %s\n",payload);
  trans = msn_transaction_new(cmdproc,"ADL","%i",payload_len);
  msn_transaction_set_payload(trans,payload,payload_len);
  msn_cmdproc_send_trans(cmdproc,trans);
}

static void msn_notification_post_rml(MsnCmdProc *cmdproc,const char *payload,int payload_len)
{
  MsnTransaction *trans;
  purple_debug_info("msn","Sending RML with payload: %s\n",payload);
  trans = msn_transaction_new(cmdproc,"RML","%i",payload_len);
  msn_transaction_set_payload(trans,payload,payload_len);
  msn_cmdproc_send_trans(cmdproc,trans);
}

void msn_notification_send_fqy(MsnSession *session,const char *payload,int payload_len,MsnFqyCb cb,gpointer cb_data)
{
  MsnTransaction *trans;
  MsnCmdProc *cmdproc;
  MsnFqyCbData *data;
  cmdproc = ( *(session -> notification)).cmdproc;
  data = ((MsnFqyCbData *)(g_malloc_n(1,(sizeof(MsnFqyCbData )))));
  data -> cb = cb;
  data -> data = cb_data;
  trans = msn_transaction_new(cmdproc,"FQY","%d",payload_len);
  msn_transaction_set_payload(trans,payload,payload_len);
  msn_transaction_set_data(trans,data);
  msn_transaction_set_data_free(trans,g_free);
  msn_cmdproc_send_trans(cmdproc,trans);
}

static void update_contact_network(MsnSession *session,const char *passport,MsnNetwork network,gpointer unused)
{
  MsnUser *user;
  if (network == MSN_NETWORK_UNKNOWN) {
    purple_debug_warning("msn","Ignoring user %s about which server knows nothing.\n",passport);
/* Decrement the count for unknown results so that we'll continue login.
		   Also, need to finish the login process here as well, because ADL OK
		   will not be called. */
    if (purple_debug_is_verbose() != 0) 
      purple_debug_info("msn","ADL/FQY count is %d\n",(session -> adl_fqy));
    if (--session -> adl_fqy == 0) 
      msn_session_finish_login(session);
    return ;
  }
/* TODO: Also figure out how to update membership lists */
  user = msn_userlist_find_user((session -> userlist),passport);
  if (user != 0) {
    xmlnode *adl_node;
    char *payload;
    int payload_len;
    msn_user_set_network(user,network);
    adl_node = xmlnode_new("ml");
    xmlnode_set_attrib(adl_node,"l","1");
    msn_add_contact_xml(adl_node,passport,((user -> list_op) & 7),network);
    payload = xmlnode_to_str(adl_node,&payload_len);
    msn_notification_post_adl(( *(session -> notification)).cmdproc,payload,payload_len);
    g_free(payload);
    xmlnode_free(adl_node);
  }
  else {
    purple_debug_error("msn","Got FQY update for unknown user %s on network %d.\n",passport,network);
  }
}
/*dump contact info to NS*/

void msn_notification_dump_contact(MsnSession *session)
{
  MsnUser *user;
  GList *l;
  xmlnode *adl_node;
  xmlnode *fqy_node;
  char *payload;
  int payload_len;
  int adl_count = 0;
  int fqy_count = 0;
  PurpleConnection *pc;
  const char *display_name;
  adl_node = xmlnode_new("ml");
  adl_node -> child = ((xmlnode *)((void *)0));
  xmlnode_set_attrib(adl_node,"l","1");
  fqy_node = xmlnode_new("ml");
/*get the userlist*/
  for (l = ( *(session -> userlist)).users; l != ((GList *)((void *)0)); l = (l -> next)) {
    user = (l -> data);
/* skip RL & PL during initial dump */
    if (!(((user -> list_op) & 7) != 0U)) 
      continue; 
    if (((user -> passport) != 0) && !(strcmp((user -> passport),"messenger@microsoft.com") != 0)) 
      continue; 
    if ((((user -> list_op) & 7) & (~MSN_LIST_FL_OP)) == (MSN_LIST_AL_OP | MSN_LIST_BL_OP)) {
/* The server will complain if we send it a user on both the
			   Allow and Block lists. So assume they're on the Block list
			   and remove them from the Allow list in the membership lists to
			   stop this from happening again. */
      purple_debug_warning("msn","User %s is on both Allow and Block list; removing from Allow list.\n",(user -> passport));
      msn_user_unset_op(user,MSN_LIST_AL_OP);
    }
    if ((user -> networkid) != MSN_NETWORK_UNKNOWN) {
      msn_add_contact_xml(adl_node,(user -> passport),((user -> list_op) & 7),(user -> networkid));
/* each ADL command may contain up to 150 contacts */
      if ((++adl_count % 150) == 0) {
        payload = xmlnode_to_str(adl_node,&payload_len);
/* ADL's are returned all-together */
        session -> adl_fqy++;
        if (purple_debug_is_verbose() != 0) 
          purple_debug_info("msn","Posting ADL, count is %d\n",(session -> adl_fqy));
        msn_notification_post_adl(( *(session -> notification)).cmdproc,payload,payload_len);
        g_free(payload);
        xmlnode_free(adl_node);
        adl_node = xmlnode_new("ml");
        adl_node -> child = ((xmlnode *)((void *)0));
        xmlnode_set_attrib(adl_node,"l","1");
      }
    }
    else {
/* FQY's are returned one-at-a-time */
      session -> adl_fqy++;
      if (purple_debug_is_verbose() != 0) 
        purple_debug_info("msn","Adding FQY address, count is %d\n",(session -> adl_fqy));
      msn_add_contact_xml(fqy_node,(user -> passport),0,(user -> networkid));
/* each FQY command may contain up to 150 contacts, probably */
      if ((++fqy_count % 150) == 0) {
        payload = xmlnode_to_str(fqy_node,&payload_len);
        msn_notification_send_fqy(session,payload,payload_len,update_contact_network,0);
        g_free(payload);
        xmlnode_free(fqy_node);
        fqy_node = xmlnode_new("ml");
      }
    }
  }
/* Send the rest, or just an empty one to let the server set us online */
  if ((adl_count == 0) || ((adl_count % 150) != 0)) {
    payload = xmlnode_to_str(adl_node,&payload_len);
/* ADL's are returned all-together */
    session -> adl_fqy++;
    if (purple_debug_is_verbose() != 0) 
      purple_debug_info("msn","Posting ADL, count is %d\n",(session -> adl_fqy));
    msn_notification_post_adl(( *(session -> notification)).cmdproc,payload,payload_len);
    g_free(payload);
  }
  if ((fqy_count % 150) != 0) {
    payload = xmlnode_to_str(fqy_node,&payload_len);
    msn_notification_send_fqy(session,payload,payload_len,update_contact_network,0);
    g_free(payload);
  }
  xmlnode_free(adl_node);
  xmlnode_free(fqy_node);
  msn_session_activate_login_timeout(session);
  pc = purple_account_get_connection((session -> account));
  display_name = purple_connection_get_display_name(pc);
  if ((display_name != 0) && (strcmp(display_name,purple_account_get_username((session -> account))) != 0)) {
    msn_set_public_alias(pc,display_name,0,0);
  }
}

static void blp_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
}

static void adl_cmd_parse(MsnCmdProc *cmdproc,MsnCommand *cmd,char *payload,size_t len)
{
  xmlnode *root;
  xmlnode *domain_node;
  purple_debug_misc("msn","Parsing received ADL XML data\n");
  do {
    if (payload != ((char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"payload != NULL");
      return ;
    };
  }while (0);
  root = xmlnode_from_str(payload,((gssize )len));
  if (root == ((xmlnode *)((void *)0))) {
    purple_debug_info("msn","Invalid XML in ADL!\n");
    return ;
  }
  for (domain_node = xmlnode_get_child(root,"d"); domain_node != 0; domain_node = xmlnode_get_next_twin(domain_node)) {
    xmlnode *contact_node = (xmlnode *)((void *)0);
    for (contact_node = xmlnode_get_child(domain_node,"c"); contact_node != 0; contact_node = xmlnode_get_next_twin(contact_node)) {
      const gchar *list;
      gint list_op = 0;
      list = xmlnode_get_attrib(contact_node,"l");
      if (list != ((const gchar *)((void *)0))) {
        list_op = atoi(list);
      }
      if ((list_op & MSN_LIST_RL_OP) != 0) {
/* someone is adding us */
        msn_get_contact_list((cmdproc -> session),MSN_PS_PENDING_LIST,0);
      }
    }
  }
  xmlnode_free(root);
}

static void adl_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  MsnSession *session;
  do {
    if (cmdproc != ((MsnCmdProc *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cmdproc != NULL");
      return ;
    };
  }while (0);
  do {
    if ((cmdproc -> session) != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cmdproc->session != NULL");
      return ;
    };
  }while (0);
  do {
    if ((cmdproc -> last_cmd) != ((MsnCommand *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cmdproc->last_cmd != NULL");
      return ;
    };
  }while (0);
  do {
    if (cmd != ((MsnCommand *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cmd != NULL");
      return ;
    };
  }while (0);
  session = (cmdproc -> session);
  if (!(strcmp((cmd -> params)[1],"OK") != 0)) {
/* ADL ack */
    if (purple_debug_is_verbose() != 0) 
      purple_debug_info("msn","ADL ACK, count is %d\n",(session -> adl_fqy));
    if (--session -> adl_fqy == 0) 
      msn_session_finish_login(session);
  }
  else {
    ( *(cmdproc -> last_cmd)).payload_cb = adl_cmd_parse;
    cmd -> payload_len = (atoi((cmd -> params)[1]));
  }
}

static void adl_error_parse(MsnCmdProc *cmdproc,MsnCommand *cmd,char *payload,size_t len)
{
  MsnSession *session;
  PurpleAccount *account;
  PurpleConnection *gc;
  int error = (gint )((glong )(cmd -> payload_cbdata));
  session = (cmdproc -> session);
  account = (session -> account);
  gc = purple_account_get_connection(account);
  if (error == 241) {
/* khc: some googling suggests that error 241 means the buddy is somehow
		   in the local list, but not the server list, and that we should add
		   those buddies to the addressbook. For now I will just notify the user
		   about the raw payload, because I am lazy */
    xmlnode *adl = xmlnode_from_str(payload,len);
    GString *emails = g_string_new(0);
    xmlnode *domain = xmlnode_get_child(adl,"d");
    while(domain != 0){
      const char *domain_str = xmlnode_get_attrib(domain,"n");
      xmlnode *contact = xmlnode_get_child(domain,"c");
      while(contact != 0){
        g_string_append_printf(emails,"%s@%s\n",xmlnode_get_attrib(contact,"n"),domain_str);
        contact = xmlnode_get_next_twin(contact);
      }
      domain = xmlnode_get_next_twin(domain);
    }
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","The following users are missing from your addressbook"))),(emails -> str),0,0);
    g_string_free(emails,(!0));
    xmlnode_free(adl);
  }
  else {
    char *adl = g_strndup(payload,len);
    char *reason = g_strdup_printf(((const char *)(dgettext("pidgin","Unknown error (%d): %s"))),error,adl);
    g_free(adl);
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Unable to add user"))),reason,0,0);
    g_free(reason);
  }
}

static void adl_error(MsnCmdProc *cmdproc,MsnTransaction *trans,int error)
{
  MsnSession *session;
  PurpleAccount *account;
  PurpleConnection *gc;
  MsnCommand *cmd = (cmdproc -> last_cmd);
  session = (cmdproc -> session);
  account = (session -> account);
  gc = purple_account_get_connection(account);
  purple_debug_error("msn","ADL error\n");
  if ((cmd -> param_count) > 1) {
    cmd -> payload_cb = adl_error_parse;
    cmd -> payload_len = (atoi((cmd -> params)[1]));
    cmd -> payload_cbdata = ((gpointer )((glong )error));
  }
  else {
    char *reason = g_strdup_printf(((const char *)(dgettext("pidgin","Unknown error (%d)"))),error);
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Unable to add user"))),reason,0,0);
    g_free(reason);
  }
}

static void rml_error_parse(MsnCmdProc *cmdproc,MsnCommand *cmd,char *payload,size_t len)
{
  MsnSession *session;
  PurpleAccount *account;
  PurpleConnection *gc;
  char *adl;
  char *reason;
  int error = (gint )((glong )(cmd -> payload_cbdata));
  session = (cmdproc -> session);
  account = (session -> account);
  gc = purple_account_get_connection(account);
  adl = g_strndup(payload,len);
  reason = g_strdup_printf(((const char *)(dgettext("pidgin","Unknown error (%d): %s"))),error,adl);
  g_free(adl);
  purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Unable to remove user"))),reason,0,0);
  g_free(reason);
}

static void rml_error(MsnCmdProc *cmdproc,MsnTransaction *trans,int error)
{
  MsnSession *session;
  PurpleAccount *account;
  PurpleConnection *gc;
  MsnCommand *cmd = (cmdproc -> last_cmd);
  session = (cmdproc -> session);
  account = (session -> account);
  gc = purple_account_get_connection(account);
  purple_debug_error("msn","RML error\n");
  if ((cmd -> param_count) > 1) {
    cmd -> payload_cb = rml_error_parse;
    cmd -> payload_len = (atoi((cmd -> params)[1]));
    cmd -> payload_cbdata = ((gpointer )((glong )error));
  }
  else {
    char *reason = g_strdup_printf(((const char *)(dgettext("pidgin","Unknown error (%d)"))),error);
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Unable to remove user"))),reason,0,0);
    g_free(reason);
  }
}

static void fqy_cmd_post(MsnCmdProc *cmdproc,MsnCommand *cmd,char *payload,size_t len)
{
  MsnSession *session;
  xmlnode *ml;
  xmlnode *d;
  xmlnode *c;
  const char *domain;
  const char *local;
  const char *type;
  char *passport;
  MsnNetwork network = MSN_NETWORK_PASSPORT;
  session = (cmdproc -> session);
/* FQY response:
	    <ml><d n="domain.com"><c n="local-node" t="network" /></d></ml> */
  ml = xmlnode_from_str(payload,len);
  for (d = xmlnode_get_child(ml,"d"); d != ((xmlnode *)((void *)0)); d = xmlnode_get_next_twin(d)) {
    domain = xmlnode_get_attrib(d,"n");
    for (c = xmlnode_get_child(d,"c"); c != ((xmlnode *)((void *)0)); c = xmlnode_get_next_twin(c)) {
      local = xmlnode_get_attrib(c,"n");
      type = xmlnode_get_attrib(c,"t");
      passport = g_strdup_printf("%s@%s",local,domain);
      if ((g_ascii_table[(guchar )(cmd -> command)[0]] & G_ASCII_DIGIT) != 0) 
        network = MSN_NETWORK_UNKNOWN;
      else if (type != ((const char *)((void *)0))) 
        network = ((MsnNetwork )(strtoul(type,0,10)));
      purple_debug_info("msn","FQY response says %s is from network %d\n",passport,network);
      if (( *(cmd -> trans)).data != 0) {
        MsnFqyCbData *fqy_data = ( *(cmd -> trans)).data;
        ( *(fqy_data -> cb))(session,passport,network,(fqy_data -> data));
/* Don't free fqy_data yet since the server responds to FQY multiple times.
				   It will be freed when cmd->trans is freed. */
      }
      g_free(passport);
    }
  }
  xmlnode_free(ml);
}

static void fqy_error(MsnCmdProc *cmdproc,MsnTransaction *trans,int error)
{
  MsnCommand *cmd = (cmdproc -> last_cmd);
  purple_debug_warning("msn","FQY error %d\n",error);
  if ((cmd -> param_count) > 1) {
    cmd -> payload_cb = fqy_cmd_post;
    cmd -> payload_len = (atoi((cmd -> params)[1]));
    cmd -> payload_cbdata = ((gpointer )((glong )error));
  }
#if 0
/* If the server didn't send us a corresponding email address for this
	   FQY error, it's probably going to disconnect us. So it isn't necessary
	   to tell the handler about it. */
#endif
}

static void fqy_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  purple_debug_info("msn","Process FQY\n");
  ( *(cmdproc -> last_cmd)).payload_cb = fqy_cmd_post;
  cmd -> payload_len = (atoi((cmd -> params)[1]));
}

static void rml_cmd_post(MsnCmdProc *cmdproc,MsnCommand *cmd,char *payload,size_t len)
{
  if (payload != ((char *)((void *)0))) 
    purple_debug_info("msn","Received RML:\n%s\n",payload);
}

static void rml_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  purple_debug_info("msn","Process RML\n");
  cmd -> payload_len = (atoi((cmd -> params)[1]));
  ( *(cmdproc -> last_cmd)).payload_cb = rml_cmd_post;
}

static void qng_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
/* TODO: Call PNG after the timeout specified. */
}

static void fln_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  MsnUser *user;
  char *passport;
  int networkid;
/* Tell libpurple that the user has signed off */
  msn_parse_user((cmd -> params)[0],&passport,&networkid);
  user = msn_userlist_find_user(( *(cmdproc -> session)).userlist,passport);
  msn_user_set_state(user,0);
  msn_user_update(user);
  g_free(passport);
}

static void iln_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  MsnSession *session;
  MsnUser *user;
  MsnObject *msnobj = (MsnObject *)((void *)0);
  unsigned long clientid;
  unsigned long extcaps;
  char *extcap_str;
  int networkid = 0;
  const char *state;
  const char *passport;
  char *friendly;
  session = (cmdproc -> session);
  state = (cmd -> params)[1];
  passport = (cmd -> params)[2];
  user = msn_userlist_find_user((session -> userlist),passport);
  if (user == ((MsnUser *)((void *)0))) 
/* Where'd this come from? */
    return ;
  if ((cmd -> param_count) == 8) {
/* Yahoo! Buddy, looks like */
    networkid = atoi((cmd -> params)[3]);
    friendly = g_strdup(purple_url_decode((cmd -> params)[4]));
    clientid = strtoul((cmd -> params)[5],&extcap_str,10);
    if ((extcap_str != 0) && (( *extcap_str) != 0)) 
      extcaps = strtoul((extcap_str + 1),0,10);
    else 
      extcaps = 0;
/* cmd->params[7] seems to be a URL to a Yahoo! icon:
				https://sec.yimg.com/i/us/nt/b/purpley.1.0.png
		   ... and it's purple, HAH!
		*/
  }
  else if ((cmd -> param_count) == 7) {
/* MSNP14+ with Display Picture object */
    networkid = atoi((cmd -> params)[3]);
    friendly = g_strdup(purple_url_decode((cmd -> params)[4]));
    clientid = strtoul((cmd -> params)[5],&extcap_str,10);
    if ((extcap_str != 0) && (( *extcap_str) != 0)) 
      extcaps = strtoul((extcap_str + 1),0,10);
    else 
      extcaps = 0;
    msnobj = msn_object_new_from_string(purple_url_decode((cmd -> params)[6]));
  }
  else if ((cmd -> param_count) == 6) {
/* Yes, this is 5. The friendly name could start with a number,
		   but the display picture object can't... */
    if ((( *__ctype_b_loc())[(int )(cmd -> params)[5][0]] & ((unsigned short )_ISdigit)) != 0) {
/* MSNP14 without Display Picture object */
      networkid = atoi((cmd -> params)[3]);
      friendly = g_strdup(purple_url_decode((cmd -> params)[4]));
      clientid = strtoul((cmd -> params)[5],&extcap_str,10);
      if ((extcap_str != 0) && (( *extcap_str) != 0)) 
        extcaps = strtoul((extcap_str + 1),0,10);
      else 
        extcaps = 0;
    }
    else {
/* MSNP8+ with Display Picture object */
      friendly = g_strdup(purple_url_decode((cmd -> params)[3]));
      clientid = strtoul((cmd -> params)[4],&extcap_str,10);
      if ((extcap_str != 0) && (( *extcap_str) != 0)) 
        extcaps = strtoul((extcap_str + 1),0,10);
      else 
        extcaps = 0;
      msnobj = msn_object_new_from_string(purple_url_decode((cmd -> params)[5]));
    }
  }
  else if ((cmd -> param_count) == 5) {
/* MSNP8+ without Display Picture object */
    friendly = g_strdup(purple_url_decode((cmd -> params)[3]));
    clientid = strtoul((cmd -> params)[4],&extcap_str,10);
    if ((extcap_str != 0) && (( *extcap_str) != 0)) 
      extcaps = strtoul((extcap_str + 1),0,10);
    else 
      extcaps = 0;
  }
  else {
    purple_debug_warning("msn","Received ILN with unknown number of parameters.\n");
    return ;
  }
  if (msn_user_set_friendly_name(user,friendly) != 0) {
    msn_update_contact(session,passport,MSN_UPDATE_DISPLAY,friendly);
  }
  g_free(friendly);
  msn_user_set_object(user,msnobj);
  user -> mobile = (((clientid & MSN_CAP_MOBILE_ON) != 0UL) || ((((user -> extinfo) != 0) && (( *(user -> extinfo)).phone_mobile != 0)) && (( *(user -> extinfo)).phone_mobile[0] == '+')));
  msn_user_set_clientid(user,clientid);
  msn_user_set_extcaps(user,extcaps);
  msn_user_set_network(user,networkid);
  msn_user_set_state(user,state);
  msn_user_update(user);
}

static void ipg_cmd_post(MsnCmdProc *cmdproc,MsnCommand *cmd,char *payload,size_t len)
{
  PurpleConnection *gc;
  MsnUserList *userlist;
  const char *who = (const char *)((void *)0);
  char *text = (char *)((void *)0);
  const char *id = (const char *)((void *)0);
  xmlnode *payloadNode;
  xmlnode *from;
  xmlnode *msg;
  xmlnode *textNode;
  purple_debug_misc("msn","Incoming Page: {%s}\n",payload);
  userlist = ( *(cmdproc -> session)).userlist;
  gc = purple_account_get_connection(( *(cmdproc -> session)).account);
/* payload looks like this:
	   <?xml version="1.0"?>
	   <NOTIFICATION id="0" siteid="111100400" siteurl="http://mobile.msn.com/">
	     <TO name="passport@example.com">
	       <VIA agent="mobile"/>
	     </TO>
	     <FROM name="tel:+XXXXXXXXXXX"/>
		 <MSG pri="1" id="1">
		   <CAT Id="110110001"/>
		   <ACTION url="2wayIM.asp"/>
		   <SUBSCR url="2wayIM.asp"/>
		   <BODY lcid="1033">
		     <TEXT>Message was here</TEXT>
		   </BODY>
		 </MSG>
	   </NOTIFICATION>
	*/
/* This is the payload if your message was too long:
	   <NOTIFICATION id="TrID" siteid="111100400" siteurl="http://mobile.msn.com/">
	     <TO name="passport@example.com">
	       <VIA agent="mobile"/>
	     </TO>
	     <FROM name="tel:+XXXXXXXXXXX"/>
	     <MSG pri="1" id="407">
	       <CAT Id="110110001"/>
	       <ACTION url="2wayIM.asp"/>
	       <SUBSCR url="2wayIM.asp"/>
	       <BODY lcid="1033">
	         <TEXT></TEXT>
	       </BODY>
	     </MSG>
	   </NOTIFICATION>
	*/
  payloadNode = xmlnode_from_str(payload,len);
  if (!(payloadNode != 0)) 
    return ;
  if ((!((from = xmlnode_get_child(payloadNode,"FROM")) != 0) || !((msg = xmlnode_get_child(payloadNode,"MSG")) != 0)) || !((textNode = xmlnode_get_child(msg,"BODY/TEXT")) != 0)) {
    xmlnode_free(payloadNode);
    return ;
  }
  who = xmlnode_get_attrib(from,"name");
  if (!(who != 0)) 
    return ;
  text = xmlnode_get_data(textNode);
/* Match number to user's mobile number, FROM is a phone number if the
	   other side page you using your phone number */
  if (!(strncmp(who,"tel:+",5) != 0)) {
    MsnUser *user = msn_userlist_find_user_with_mobile_phone(userlist,(who + 4));
    if ((user != 0) && ((user -> passport) != 0)) 
      who = (user -> passport);
  }
  id = xmlnode_get_attrib(msg,"id");
  if ((id != 0) && (strcmp(id,"1") != 0)) {
    PurpleConversation *conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_ANY,who,(gc -> account));
    if (conv != ((PurpleConversation *)((void *)0))) {
      const char *error;
      if (!(strcmp(id,"407") != 0)) 
        error = ((const char *)(dgettext("pidgin","Mobile message was not sent because it was too long.")));
      else 
        error = ((const char *)(dgettext("pidgin","Mobile message was not sent because an unknown error occurred.")));
      purple_conversation_write(conv,0,error,PURPLE_MESSAGE_ERROR,time(0));
      if ((id = xmlnode_get_attrib(payloadNode,"id")) != ((const char *)((void *)0))) {
        unsigned int trId = (atol(id));
        MsnTransaction *trans;
        trans = msn_history_find((cmdproc -> history),trId);
        if (trans != 0) {
          MsnMessage *msg = (MsnMessage *)(trans -> data);
          if (msg != 0) {
            char *body_str = msn_message_to_string(msg);
            char *body_enc = g_markup_escape_text(body_str,(-1));
            purple_conversation_write(conv,0,body_enc,PURPLE_MESSAGE_RAW,time(0));
            g_free(body_str);
            g_free(body_enc);
            msn_message_unref(msg);
            trans -> data = ((void *)((void *)0));
          }
        }
      }
    }
  }
  else {
    serv_got_im(gc,who,text,0,time(0));
  }
  g_free(text);
  xmlnode_free(payloadNode);
}

static void ipg_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  cmd -> payload_len = (atoi((cmd -> params)[0]));
  ( *(cmdproc -> last_cmd)).payload_cb = ipg_cmd_post;
}

static void nln_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  MsnSession *session;
  MsnUser *user;
  MsnObject *msnobj;
  unsigned long clientid;
  unsigned long extcaps;
  char *extcap_str;
  char *passport;
  int networkid;
  const char *state;
  const char *friendly;
  session = (cmdproc -> session);
  state = (cmd -> params)[0];
  msn_parse_user((cmd -> params)[1],&passport,&networkid);
  friendly = purple_url_decode((cmd -> params)[2]);
  user = msn_userlist_find_user((session -> userlist),passport);
  if (user == ((MsnUser *)((void *)0))) 
    return ;
  if ((msn_user_set_friendly_name(user,friendly) != 0) && (user != (session -> user))) {
    msn_update_contact(session,passport,MSN_UPDATE_DISPLAY,friendly);
  }
  if ((cmd -> param_count) == 5) {
    msnobj = msn_object_new_from_string(purple_url_decode((cmd -> params)[4]));
    msn_user_set_object(user,msnobj);
  }
  else {
    msn_user_set_object(user,0);
  }
  clientid = strtoul((cmd -> params)[3],&extcap_str,10);
  if ((extcap_str != 0) && (( *extcap_str) != 0)) 
    extcaps = strtoul((extcap_str + 1),0,10);
  else 
    extcaps = 0;
  user -> mobile = (((clientid & MSN_CAP_MOBILE_ON) != 0UL) || ((((user -> extinfo) != 0) && (( *(user -> extinfo)).phone_mobile != 0)) && (( *(user -> extinfo)).phone_mobile[0] == '+')));
  msn_user_set_clientid(user,clientid);
  msn_user_set_extcaps(user,extcaps);
  msn_user_set_network(user,networkid);
  msn_user_set_state(user,state);
  msn_user_update(user);
  g_free(passport);
}
#if 0
#endif

static void not_cmd_post(MsnCmdProc *cmdproc,MsnCommand *cmd,char *payload,size_t len)
{
#if 0
#endif
}

static void not_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  cmd -> payload_len = (atoi((cmd -> params)[0]));
  ( *(cmdproc -> last_cmd)).payload_cb = not_cmd_post;
}

static void prp_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  MsnSession *session = (cmdproc -> session);
  const char *type;
  const char *value;
  do {
    if ((cmd -> param_count) >= 3) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cmd->param_count >= 3");
      return ;
    };
  }while (0);
  type = (cmd -> params)[2];
  if ((cmd -> param_count) == 4) {
    value = (cmd -> params)[3];
    if (!(strcmp(type,"PHH") != 0)) 
      msn_user_set_home_phone((session -> user),purple_url_decode(value));
    else if (!(strcmp(type,"PHW") != 0)) 
      msn_user_set_work_phone((session -> user),purple_url_decode(value));
    else if (!(strcmp(type,"PHM") != 0)) 
      msn_user_set_mobile_phone((session -> user),purple_url_decode(value));
  }
  else {
    if (!(strcmp(type,"PHH") != 0)) 
      msn_user_set_home_phone((session -> user),0);
    else if (!(strcmp(type,"PHW") != 0)) 
      msn_user_set_work_phone((session -> user),0);
    else if (!(strcmp(type,"PHM") != 0)) 
      msn_user_set_mobile_phone((session -> user),0);
  }
}
/**************************************************************************
 * Misc commands
 **************************************************************************/

static void url_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  MsnSession *session;
  PurpleConnection *gc;
  PurpleAccount *account;
  const char *rru;
  const char *url;
  PurpleCipherContext *cipher;
  gchar creds[33UL];
  char *buf;
  gulong tmp_timestamp;
  session = (cmdproc -> session);
  account = (session -> account);
  gc = (account -> gc);
  rru = (cmd -> params)[1];
  url = (cmd -> params)[2];
  session -> passport_info.mail_timestamp = (time(0));
  tmp_timestamp = (session -> passport_info.mail_timestamp - session -> passport_info.sl);
  buf = g_strdup_printf("%s%lu%s",((session -> passport_info.mspauth != 0)?session -> passport_info.mspauth : "BOGUS"),tmp_timestamp,purple_connection_get_password(gc));
  cipher = purple_cipher_context_new_by_name("md5",0);
  purple_cipher_context_append(cipher,((const guchar *)buf),strlen(buf));
  purple_cipher_context_digest_to_str(cipher,(sizeof(creds)),creds,0);
  purple_cipher_context_destroy(cipher);
  g_free(buf);
  g_free(session -> passport_info.mail_url);
  session -> passport_info.mail_url = g_strdup_printf("%s&auth=%s&creds=%s&sl=%ld&username=%s&mode=ttl&sid=%s&id=2&rru=%s&svc=mail&js=yes",url,((session -> passport_info.mspauth != 0)?purple_url_encode(session -> passport_info.mspauth) : "BOGUS"),creds,tmp_timestamp,msn_user_get_passport((session -> user)),session -> passport_info.sid,rru);
/* The user wants to check his or her email */
  if (((cmd -> trans) != 0) && (( *(cmd -> trans)).data != 0)) 
    purple_notify_uri((purple_account_get_connection(account)),session -> passport_info.mail_url);
}
/**************************************************************************
 * Switchboards
 **************************************************************************/

static void rng_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  MsnSession *session;
  MsnSwitchBoard *swboard;
  const char *session_id;
  char *host;
  int port;
  session = (cmdproc -> session);
  session_id = (cmd -> params)[0];
  msn_parse_socket((cmd -> params)[1],&host,&port);
  if ((session -> http_method) != 0) 
    port = 80;
  swboard = msn_switchboard_new(session);
  msn_switchboard_set_invited(swboard,(!0));
  msn_switchboard_set_session_id(swboard,session_id);
  msn_switchboard_set_auth_key(swboard,(cmd -> params)[3]);
  swboard -> im_user = g_strdup((cmd -> params)[4]);
/* msn_switchboard_add_user(swboard, cmd->params[4]); */
  if (!(msn_switchboard_connect(swboard,host,port) != 0)) 
    msn_switchboard_destroy(swboard);
  g_free(host);
}

static void xfr_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  char *host;
  int port;
  if ((strcmp((cmd -> params)[1],"SB") != 0) && (strcmp((cmd -> params)[1],"NS") != 0)) {
/* Maybe we can have a generic bad command error. */
    purple_debug_error("msn","Bad XFR command (%s)\n",(cmd -> params)[1]);
    return ;
  }
  msn_parse_socket((cmd -> params)[2],&host,&port);
  if (!(strcmp((cmd -> params)[1],"SB") != 0)) {
    purple_debug_error("msn","This shouldn\'t be handled here.\n");
  }
  else if (!(strcmp((cmd -> params)[1],"NS") != 0)) {
    MsnSession *session;
    session = (cmdproc -> session);
    msn_session_set_login_step(session,MSN_LOGIN_STEP_TRANSFER);
    msn_notification_connect((session -> notification),host,port);
  }
  g_free(host);
}

static void gcf_cmd_post(MsnCmdProc *cmdproc,MsnCommand *cmd,char *payload,size_t len)
{
/* QuLogic: Disabled until confirmed correct. */
#if 0
/* We need a get_child with attrib... */
#endif
}

static void gcf_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  purple_debug_info("msn","Processing GCF command\n");
  ( *(cmdproc -> last_cmd)).payload_cb = gcf_cmd_post;
  cmd -> payload_len = (atoi((cmd -> params)[1]));
}

static void sbs_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  purple_debug_info("msn","Processing SBS... \n");
/*get the payload content*/
}

static void parse_user_endpoints(MsnUser *user,xmlnode *payloadNode)
{
  MsnSession *session;
  xmlnode *epNode;
  xmlnode *capsNode;
  MsnUserEndpoint data;
  const char *id;
  char *caps;
  char *tmp;
  gboolean is_me;
  purple_debug_info("msn","Get EndpointData\n");
  session = ( *(user -> userlist)).session;
  is_me = (user == (session -> user));
  msn_user_clear_endpoints(user);
  for (epNode = xmlnode_get_child(payloadNode,"EndpointData"); epNode != 0; epNode = xmlnode_get_next_twin(epNode)) {
    id = xmlnode_get_attrib(epNode,"id");
    capsNode = xmlnode_get_child(epNode,"Capabilities");
/* Disconnect others, if MPOP is disabled */
    if (((is_me != 0) && !((session -> enable_mpop) != 0)) && (strncasecmp((id + 1),(session -> guid),36) != 0)) {
      purple_debug_info("msn","Disconnecting Endpoint %s\n",id);
      tmp = g_strdup_printf("%s;%s",(user -> passport),id);
      msn_notification_send_uun(session,tmp,MSN_UNIFIED_NOTIFICATION_MPOP,"goawyplzthxbye");
      g_free(tmp);
    }
    else {
      if (capsNode != ((xmlnode *)((void *)0))) {
        caps = xmlnode_get_data(capsNode);
        data.clientid = (strtoul(caps,&tmp,10));
        if ((tmp != 0) && (( *tmp) != 0)) 
          data.extcaps = (strtoul((tmp + 1),0,10));
        else 
          data.extcaps = 0;
        g_free(caps);
      }
      else {
        data.clientid = 0;
        data.extcaps = 0;
      }
      msn_user_set_endpoint_data(user,id,&data);
    }
  }
  if ((is_me != 0) && ((session -> enable_mpop) != 0)) {
    for (epNode = xmlnode_get_child(payloadNode,"PrivateEndpointData"); epNode != 0; epNode = xmlnode_get_next_twin(epNode)) {
      MsnUserEndpoint *ep;
      xmlnode *nameNode;
      xmlnode *clientNode;
/*	<PrivateEndpointData id='{GUID}'>
					<EpName>Endpoint Name</EpName>
					<Idle>true/false</Idle>
					<ClientType>1</ClientType>
					<State>NLN</State>
				</PrivateEndpointData>
			*/
      id = xmlnode_get_attrib(epNode,"id");
      ep = msn_user_get_endpoint_data(user,id);
      if (ep != ((MsnUserEndpoint *)((void *)0))) {
        nameNode = xmlnode_get_child(epNode,"EpName");
        if (nameNode != ((xmlnode *)((void *)0))) {
          g_free((ep -> name));
          ep -> name = xmlnode_get_data(nameNode);
        }
        clientNode = xmlnode_get_child(epNode,"ClientType");
        if (clientNode != ((xmlnode *)((void *)0))) {
          tmp = xmlnode_get_data(clientNode);
          ep -> type = (strtoul(tmp,0,10));
          g_free(tmp);
        }
      }
    }
  }
}

static void parse_currentmedia(MsnUser *user,const char *cmedia)
{
  char **cmedia_array;
  int strings = 0;
  if (!(cmedia != 0) || (cmedia[0] == 0)) {
    purple_debug_info("msn","No currentmedia string\n");
    return ;
  }
  purple_debug_info("msn","Parsing currentmedia string: \"%s\"\n",cmedia);
  cmedia_array = g_strsplit(cmedia,"\\0",0);
/*
	 * 0: Application
	 * 1: 'Music'/'Games'/'Office'
	 * 2: '1' if enabled, '0' if not
	 * 3: Format (eg. {0} by {1})
	 * 4: Title
	 * If 'Music':
	 *  5: Artist
	 *  6: Album
	 *  7: ?
	 */
  strings = (g_strv_length(cmedia_array));
  if ((strings >= 4) && !(strcmp(cmedia_array[2],"1") != 0)) {
    if ((user -> extinfo) == ((MsnUserExtendedInfo *)((void *)0))) 
      user -> extinfo = ((MsnUserExtendedInfo *)(g_malloc0_n(1,(sizeof(MsnUserExtendedInfo )))));
    else {
      g_free(( *(user -> extinfo)).media_album);
      g_free(( *(user -> extinfo)).media_artist);
      g_free(( *(user -> extinfo)).media_title);
    }
    if (!(strcmp(cmedia_array[1],"Music") != 0)) 
      ( *(user -> extinfo)).media_type = CURRENT_MEDIA_MUSIC;
    else if (!(strcmp(cmedia_array[1],"Games") != 0)) 
      ( *(user -> extinfo)).media_type = CURRENT_MEDIA_GAMES;
    else if (!(strcmp(cmedia_array[1],"Office") != 0)) 
      ( *(user -> extinfo)).media_type = CURRENT_MEDIA_OFFICE;
    else 
      ( *(user -> extinfo)).media_type = CURRENT_MEDIA_UNKNOWN;
    ( *(user -> extinfo)).media_title = g_strdup(cmedia_array[((strings == 4)?3 : 4)]);
    ( *(user -> extinfo)).media_artist = ((strings > 5)?g_strdup(cmedia_array[5]) : ((char *)((void *)0)));
    ( *(user -> extinfo)).media_album = ((strings > 6)?g_strdup(cmedia_array[6]) : ((char *)((void *)0)));
  }
  g_strfreev(cmedia_array);
}
/*
 * Get the UBX's PSM info
 * Post it to the User status
 * Thanks for Chris <ukdrizzle@yahoo.co.uk>'s code
 */

static void ubx_cmd_post(MsnCmdProc *cmdproc,MsnCommand *cmd,char *payload,size_t len)
{
  MsnSession *session;
  MsnUser *user;
  char *passport;
  int network;
  xmlnode *payloadNode;
  char *psm_str;
  char *str;
  session = (cmdproc -> session);
  msn_parse_user((cmd -> params)[0],&passport,&network);
  user = msn_userlist_find_user((session -> userlist),passport);
  if (user == ((MsnUser *)((void *)0))) {
    str = g_strndup(payload,len);
    purple_debug_info("msn","unknown user %s, payload is %s\n",passport,str);
    g_free(passport);
    g_free(str);
    return ;
  }
  g_free(passport);
/* Free any existing media info for this user */
  if ((user -> extinfo) != 0) {
    g_free(( *(user -> extinfo)).media_album);
    g_free(( *(user -> extinfo)).media_artist);
    g_free(( *(user -> extinfo)).media_title);
    ( *(user -> extinfo)).media_album = ((char *)((void *)0));
    ( *(user -> extinfo)).media_artist = ((char *)((void *)0));
    ( *(user -> extinfo)).media_title = ((char *)((void *)0));
    ( *(user -> extinfo)).media_type = CURRENT_MEDIA_UNKNOWN;
  }
  if (len != 0) {
    payloadNode = xmlnode_from_str(payload,len);
    if (!(payloadNode != 0)) {
      purple_debug_error("msn","UBX XML parse Error!\n");
      msn_user_set_statusline(user,0);
      msn_user_update(user);
      return ;
    }
    psm_str = msn_get_psm(payloadNode);
    msn_user_set_statusline(user,psm_str);
    g_free(psm_str);
    str = msn_get_currentmedia(payloadNode);
    parse_currentmedia(user,str);
    g_free(str);
    parse_user_endpoints(user,payloadNode);
    xmlnode_free(payloadNode);
  }
  else {
    msn_user_set_statusline(user,0);
  }
  msn_user_update(user);
}

static void ubx_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  purple_debug_misc("msn","UBX received.\n");
  ( *(cmdproc -> last_cmd)).payload_cb = ubx_cmd_post;
  cmd -> payload_len = (atoi((cmd -> params)[1]));
}

static void uux_cmd_post(MsnCmdProc *cmdproc,MsnCommand *cmd,char *payload,size_t len)
{
/* Do Nothing, right now. */
  if (payload != ((char *)((void *)0))) 
    purple_debug_info("msn","UUX payload:\n%s\n",payload);
}

static void uux_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  purple_debug_misc("msn","UUX received.\n");
  ( *(cmdproc -> last_cmd)).payload_cb = uux_cmd_post;
  cmd -> payload_len = (atoi((cmd -> params)[1]));
}

void msn_notification_send_uux(MsnSession *session,const char *payload)
{
  MsnTransaction *trans;
  MsnCmdProc *cmdproc;
  size_t len = strlen(payload);
  cmdproc = ( *(session -> notification)).cmdproc;
  purple_debug_misc("msn","Sending UUX command with payload: %s\n",payload);
  trans = msn_transaction_new(cmdproc,"UUX","%lu",len);
  msn_transaction_set_payload(trans,payload,len);
  msn_cmdproc_send_trans(cmdproc,trans);
}

void msn_notification_send_uux_endpointdata(MsnSession *session)
{
  xmlnode *epDataNode;
  xmlnode *capNode;
  char *caps;
  char *payload;
  int length;
  epDataNode = xmlnode_new("EndpointData");
  capNode = xmlnode_new_child(epDataNode,"Capabilities");
  caps = g_strdup_printf("%d:%02d",MSN_CAP_PACKET | MSN_CAP_INK_GIF | MSN_CAP_VOICEIM,0);
  xmlnode_insert_data(capNode,caps,(-1));
  g_free(caps);
  payload = xmlnode_to_str(epDataNode,&length);
  msn_notification_send_uux(session,payload);
  xmlnode_free(epDataNode);
  g_free(payload);
}

void msn_notification_send_uux_private_endpointdata(MsnSession *session)
{
  xmlnode *private;
  const char *name;
  xmlnode *epname;
  xmlnode *idle;
  GHashTable *ui_info;
  const gchar *ui_type;
  xmlnode *client_type;
  xmlnode *state;
  char *payload;
  int length;
  private = xmlnode_new("PrivateEndpointData");
  name = purple_account_get_string((session -> account),"endpoint-name",0);
  epname = xmlnode_new_child(private,"EpName");
  xmlnode_insert_data(epname,name,(-1));
  idle = xmlnode_new_child(private,"Idle");
  xmlnode_insert_data(idle,"false",(-1));
/* ClientType info (from amsn guys):
		0: None
		1: Computer
		2: Website
		3: Mobile / none
		4: Xbox / phone /mobile
		9: MsnGroup
		32: Email member, currently Yahoo!
	*/
  client_type = xmlnode_new_child(private,"ClientType");
  ui_info = purple_core_get_ui_info();
  ui_type = (((ui_info != 0)?g_hash_table_lookup(ui_info,"client_type") : ((void *)((void *)0))));
  if (ui_type != 0) {
    if (strcmp(ui_type,"pc") == 0) 
      xmlnode_insert_data(client_type,"1",(-1));
    else if (strcmp(ui_type,"web") == 0) 
      xmlnode_insert_data(client_type,"2",(-1));
    else if (strcmp(ui_type,"phone") == 0) 
      xmlnode_insert_data(client_type,"3",(-1));
    else if (strcmp(ui_type,"handheld") == 0) 
      xmlnode_insert_data(client_type,"3",(-1));
    else 
      xmlnode_insert_data(client_type,"1",(-1));
  }
  else 
    xmlnode_insert_data(client_type,"1",(-1));
  state = xmlnode_new_child(private,"State");
  xmlnode_insert_data(state,msn_state_get_text(msn_state_from_account((session -> account))),(-1));
  payload = xmlnode_to_str(private,&length);
  msn_notification_send_uux(session,payload);
  xmlnode_free(private);
  g_free(payload);
}

static void ubn_cmd_post(MsnCmdProc *cmdproc,MsnCommand *cmd,char *payload,size_t len)
{
/* Do Nothing, right now. */
  if (payload != ((char *)((void *)0))) 
    purple_debug_info("msn","UBN payload:\n%s\n",payload);
}

static void ubn_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  purple_debug_misc("msn","UBN received from %s.\n",(cmd -> params)[0]);
  ( *(cmdproc -> last_cmd)).payload_cb = ubn_cmd_post;
  cmd -> payload_len = (atoi((cmd -> params)[2]));
}

static void uun_cmd_post(MsnCmdProc *cmdproc,MsnCommand *cmd,char *payload,size_t len)
{
/* Do Nothing, right now. */
  if (payload != ((char *)((void *)0))) 
    purple_debug_info("msn","UUN payload:\n%s\n",payload);
}

static void uun_cmd(MsnCmdProc *cmdproc,MsnCommand *cmd)
{
  if (strcmp((cmd -> params)[1],"OK") != 0) {
    purple_debug_misc("msn","UUN received.\n");
    ( *(cmdproc -> last_cmd)).payload_cb = uun_cmd_post;
    cmd -> payload_len = (atoi((cmd -> params)[1]));
  }
  else 
    purple_debug_misc("msn","UUN OK received.\n");
}

void msn_notification_send_uun(MsnSession *session,const char *user,MsnUnifiedNotificationType type,const char *payload)
{
  MsnTransaction *trans;
  MsnCmdProc *cmdproc;
  size_t len = strlen(payload);
  cmdproc = ( *(session -> notification)).cmdproc;
  purple_debug_misc("msn","Sending UUN command %d to %s with payload: %s\n",type,user,payload);
  trans = msn_transaction_new(cmdproc,"UUN","%s %d %lu",user,type,len);
  msn_transaction_set_payload(trans,payload,len);
  msn_cmdproc_send_trans(cmdproc,trans);
}

void msn_notification_send_circle_auth(MsnSession *session,const char *ticket)
{
  MsnTransaction *trans;
  MsnCmdProc *cmdproc;
  char *encoded;
  cmdproc = ( *(session -> notification)).cmdproc;
  encoded = purple_base64_encode(((guchar *)ticket),strlen(ticket));
  trans = msn_transaction_new(cmdproc,"USR","SHA A %s",encoded);
  msn_cmdproc_send_trans(cmdproc,trans);
  g_free(encoded);
}
/**************************************************************************
 * Message Types
 **************************************************************************/

static void profile_msg(MsnCmdProc *cmdproc,MsnMessage *msg)
{
  MsnSession *session;
  const char *value;
#ifdef MSN_PARTIAL_LISTS
#endif
  session = (cmdproc -> session);
  if (strcmp((msg -> remote_user),"Hotmail") != 0) 
/* This isn't an official message. */
    return ;
  if ((value = msn_message_get_header_value(msg,"sid")) != ((const char *)((void *)0))) {
    g_free(session -> passport_info.sid);
    session -> passport_info.sid = g_strdup(value);
  }
  if ((value = msn_message_get_header_value(msg,"MSPAuth")) != ((const char *)((void *)0))) {
    g_free(session -> passport_info.mspauth);
    session -> passport_info.mspauth = g_strdup(value);
  }
  if ((value = msn_message_get_header_value(msg,"ClientIP")) != ((const char *)((void *)0))) {
    g_free(session -> passport_info.client_ip);
    session -> passport_info.client_ip = g_strdup(value);
  }
  if ((value = msn_message_get_header_value(msg,"ClientPort")) != ((const char *)((void *)0))) {
    session -> passport_info.client_port = (ntohs((atoi(value))));
  }
  if ((value = msn_message_get_header_value(msg,"LoginTime")) != ((const char *)((void *)0))) 
    session -> passport_info.sl = (atol(value));
  if ((value = msn_message_get_header_value(msg,"EmailEnabled")) != ((const char *)((void *)0))) 
    session -> passport_info.email_enabled = ((gboolean )(atol(value)));
#ifdef MSN_PARTIAL_LISTS
/*starting retrieve the contact list*/
/* msn_userlist_load defeats all attempts at trying to detect blist sync issues */
#else
/* always get the full list? */
  msn_get_contact_list(session,MSN_PS_INITIAL,0);
#endif
}

static void initial_email_msg(MsnCmdProc *cmdproc,MsnMessage *msg)
{
  MsnSession *session;
  PurpleConnection *gc;
  GHashTable *table;
  const char *unread;
  session = (cmdproc -> session);
  gc = ( *(session -> account)).gc;
  if (strcmp((msg -> remote_user),"Hotmail") != 0) 
/* This isn't an official message. */
    return ;
  if (session -> passport_info.mail_url == ((char *)((void *)0))) {
    MsnTransaction *trans;
    trans = msn_transaction_new(cmdproc,"URL","%s","INBOX");
    msn_transaction_queue_cmd(trans,(msg -> cmd));
    msn_cmdproc_send_trans(cmdproc,trans);
    return ;
  }
  if (!(purple_account_get_check_mail((session -> account)) != 0)) 
    return ;
  table = msn_message_get_hashtable_from_body(msg);
  unread = (g_hash_table_lookup(table,"Inbox-Unread"));
  if (unread != ((const char *)((void *)0))) {
    int count = atoi(unread);
    if (count > 0) {
      const char *passports[2UL] = {msn_user_get_passport((session -> user))};
      const char *urls[2UL] = {(session -> passport_info.mail_url)};
      purple_notify_emails(gc,count,0,0,0,passports,urls,0,0);
    }
  }
  g_hash_table_destroy(table);
}
/*offline Message notification process*/

static void initial_mdata_msg(MsnCmdProc *cmdproc,MsnMessage *msg)
{
  MsnSession *session;
  PurpleConnection *gc;
  GHashTable *table;
  const char *mdata;
  const char *unread;
  session = (cmdproc -> session);
  gc = ( *(session -> account)).gc;
  if (strcmp((msg -> remote_user),"Hotmail") != 0) 
/* This isn't an official message. */
    return ;
  table = msn_message_get_hashtable_from_body(msg);
  mdata = (g_hash_table_lookup(table,"Mail-Data"));
  if (mdata != ((const char *)((void *)0))) 
    msn_parse_oim_msg((session -> oim),mdata);
  if (g_hash_table_lookup(table,"Inbox-URL") == ((void *)((void *)0))) {
    g_hash_table_destroy(table);
    return ;
  }
  if (session -> passport_info.mail_url == ((char *)((void *)0))) {
    MsnTransaction *trans;
    trans = msn_transaction_new(cmdproc,"URL","%s","INBOX");
    msn_transaction_queue_cmd(trans,(msg -> cmd));
    msn_cmdproc_send_trans(cmdproc,trans);
    g_hash_table_destroy(table);
    return ;
  }
  if (!(purple_account_get_check_mail((session -> account)) != 0)) {
    g_hash_table_destroy(table);
    return ;
  }
  unread = (g_hash_table_lookup(table,"Inbox-Unread"));
  if (unread != ((const char *)((void *)0))) {
    int count = atoi(unread);
    if (count > 0) {
      const char *passports[2UL] = {msn_user_get_passport((session -> user))};
      const char *urls[2UL] = {(session -> passport_info.mail_url)};
      purple_notify_emails(gc,count,0,0,0,passports,urls,0,0);
    }
  }
  g_hash_table_destroy(table);
}
/*offline Message Notification*/

static void delete_oim_msg(MsnCmdProc *cmdproc,MsnMessage *msg)
{
  purple_debug_misc("msn","Delete OIM message.\n");
}

static void email_msg(MsnCmdProc *cmdproc,MsnMessage *msg)
{
  MsnSession *session;
  PurpleConnection *gc;
  GHashTable *table;
  char *from;
  char *subject;
  char *tmp;
  session = (cmdproc -> session);
  gc = ( *(session -> account)).gc;
  if (strcmp((msg -> remote_user),"Hotmail") != 0) 
/* This isn't an official message. */
    return ;
  if (session -> passport_info.mail_url == ((char *)((void *)0))) {
    MsnTransaction *trans;
    trans = msn_transaction_new(cmdproc,"URL","%s","INBOX");
    msn_transaction_queue_cmd(trans,(msg -> cmd));
    msn_cmdproc_send_trans(cmdproc,trans);
    return ;
  }
  if (!(purple_account_get_check_mail((session -> account)) != 0)) 
    return ;
  table = msn_message_get_hashtable_from_body(msg);
  from = (subject = ((char *)((void *)0)));
  tmp = (g_hash_table_lookup(table,"From"));
  if (tmp != ((char *)((void *)0))) 
    from = purple_mime_decode_field(tmp);
  tmp = (g_hash_table_lookup(table,"Subject"));
  if (tmp != ((char *)((void *)0))) 
    subject = purple_mime_decode_field(tmp);
  purple_notify_email(gc,((subject != ((char *)((void *)0)))?subject : ""),((from != ((char *)((void *)0)))?from : ""),msn_user_get_passport((session -> user)),session -> passport_info.mail_url,0,0);
  g_free(from);
  g_free(subject);
  g_hash_table_destroy(table);
}

static void system_msg(MsnCmdProc *cmdproc,MsnMessage *msg)
{
  GHashTable *table;
  const char *type_s;
  if (strcmp((msg -> remote_user),"Hotmail") != 0) 
/* This isn't an official message. */
    return ;
  table = msn_message_get_hashtable_from_body(msg);
  if ((type_s = (g_hash_table_lookup(table,"Type"))) != ((const char *)((void *)0))) {
    int type = atoi(type_s);
    char buf[8192UL] = "";
    int minutes;
    switch(type){
      case 1:
{
        minutes = atoi((g_hash_table_lookup(table,"Arg1")));
        g_snprintf(buf,(sizeof(buf)),(dngettext("pidgin","The MSN server will shut down for maintenance in %d minute. You will automatically be signed out at that time.  Please finish any conversations in progress.\n\nAfter the maintenance has been completed, you will be able to successfully sign in.","The MSN server will shut down for maintenance in %d minutes. You will automatically be signed out at that time.  Please finish any conversations in progress.\n\nAfter the maintenance has been completed, you will be able to successfully sign in.",minutes)),minutes);
      }
      default:
{
        break; 
      }
    }
    if (( *buf) != 0) 
      purple_notify_message(( *( *(cmdproc -> session)).account).gc,PURPLE_NOTIFY_MSG_INFO,0,buf,0,0,0);
  }
  g_hash_table_destroy(table);
}
/**************************************************************************
 * Dispatch server list management
 **************************************************************************/
typedef struct MsnAddRemoveListData {
MsnCmdProc *cmdproc;
MsnUser *user;
MsnListOp list_op;
gboolean add;}MsnAddRemoveListData;

static void modify_unknown_buddy_on_list(MsnSession *session,const char *passport,MsnNetwork network,gpointer data)
{
  MsnAddRemoveListData *addrem = data;
  MsnCmdProc *cmdproc;
  xmlnode *node;
  char *payload;
  int payload_len;
  cmdproc = (addrem -> cmdproc);
/* Update user first */
  msn_user_set_network((addrem -> user),network);
  node = xmlnode_new("ml");
  node -> child = ((xmlnode *)((void *)0));
  msn_add_contact_xml(node,passport,(addrem -> list_op),network);
  payload = xmlnode_to_str(node,&payload_len);
  xmlnode_free(node);
  if ((addrem -> add) != 0) 
    msn_notification_post_adl(cmdproc,payload,payload_len);
  else 
    msn_notification_post_rml(cmdproc,payload,payload_len);
  g_free(payload);
  g_free(addrem);
}

void msn_notification_add_buddy_to_list(MsnNotification *notification,MsnListId list_id,MsnUser *user)
{
  MsnAddRemoveListData *addrem;
  MsnCmdProc *cmdproc;
  MsnListOp list_op = (1 << list_id);
  xmlnode *adl_node;
  char *payload;
  int payload_len;
  cmdproc = ( *(notification -> servconn)).cmdproc;
  adl_node = xmlnode_new("ml");
  adl_node -> child = ((xmlnode *)((void *)0));
  msn_add_contact_xml(adl_node,(user -> passport),list_op,(user -> networkid));
  payload = xmlnode_to_str(adl_node,&payload_len);
  xmlnode_free(adl_node);
  if ((user -> networkid) != MSN_NETWORK_UNKNOWN) {
    msn_notification_post_adl(cmdproc,payload,payload_len);
  }
  else {
    addrem = ((MsnAddRemoveListData *)(g_malloc_n(1,(sizeof(MsnAddRemoveListData )))));
    addrem -> cmdproc = cmdproc;
    addrem -> user = user;
    addrem -> list_op = list_op;
    addrem -> add = (!0);
    msn_notification_send_fqy((notification -> session),payload,payload_len,modify_unknown_buddy_on_list,addrem);
  }
  g_free(payload);
}

void msn_notification_rem_buddy_from_list(MsnNotification *notification,MsnListId list_id,MsnUser *user)
{
  MsnAddRemoveListData *addrem;
  MsnCmdProc *cmdproc;
  MsnListOp list_op = (1 << list_id);
  xmlnode *rml_node;
  char *payload;
  int payload_len;
  cmdproc = ( *(notification -> servconn)).cmdproc;
  rml_node = xmlnode_new("ml");
  rml_node -> child = ((xmlnode *)((void *)0));
  msn_add_contact_xml(rml_node,(user -> passport),list_op,(user -> networkid));
  payload = xmlnode_to_str(rml_node,&payload_len);
  xmlnode_free(rml_node);
  if ((user -> networkid) != MSN_NETWORK_UNKNOWN) {
    msn_notification_post_rml(cmdproc,payload,payload_len);
  }
  else {
    addrem = ((MsnAddRemoveListData *)(g_malloc_n(1,(sizeof(MsnAddRemoveListData )))));
    addrem -> cmdproc = cmdproc;
    addrem -> user = user;
    addrem -> list_op = list_op;
    addrem -> add = 0;
    msn_notification_send_fqy((notification -> session),payload,payload_len,modify_unknown_buddy_on_list,addrem);
  }
  g_free(payload);
}
/**************************************************************************
 * Init
 **************************************************************************/

void msn_notification_init()
{
  cbs_table = msn_table_new();
/* Synchronous */
  msn_table_add_cmd(cbs_table,"CHG","CHG",0);
  msn_table_add_cmd(cbs_table,"CHG","ILN",iln_cmd);
  msn_table_add_cmd(cbs_table,"ADL","ILN",iln_cmd);
  msn_table_add_cmd(cbs_table,"USR","USR",usr_cmd);
  msn_table_add_cmd(cbs_table,"USR","XFR",xfr_cmd);
  msn_table_add_cmd(cbs_table,"USR","GCF",gcf_cmd);
  msn_table_add_cmd(cbs_table,"CVR","CVR",cvr_cmd);
  msn_table_add_cmd(cbs_table,"VER","VER",ver_cmd);
  msn_table_add_cmd(cbs_table,"PRP","PRP",prp_cmd);
  msn_table_add_cmd(cbs_table,"BLP","BLP",blp_cmd);
  msn_table_add_cmd(cbs_table,"XFR","XFR",xfr_cmd);
/* Asynchronous */
  msn_table_add_cmd(cbs_table,0,"IPG",ipg_cmd);
  msn_table_add_cmd(cbs_table,0,"MSG",msg_cmd);
  msn_table_add_cmd(cbs_table,0,"UBM",ubm_cmd);
  msn_table_add_cmd(cbs_table,0,"GCF",gcf_cmd);
  msn_table_add_cmd(cbs_table,0,"SBS",sbs_cmd);
  msn_table_add_cmd(cbs_table,0,"NOT",not_cmd);
  msn_table_add_cmd(cbs_table,0,"CHL",chl_cmd);
  msn_table_add_cmd(cbs_table,0,"RML",rml_cmd);
  msn_table_add_cmd(cbs_table,0,"ADL",adl_cmd);
  msn_table_add_cmd(cbs_table,0,"FQY",fqy_cmd);
  msn_table_add_cmd(cbs_table,0,"QRY",0);
  msn_table_add_cmd(cbs_table,0,"QNG",qng_cmd);
  msn_table_add_cmd(cbs_table,0,"FLN",fln_cmd);
  msn_table_add_cmd(cbs_table,0,"NLN",nln_cmd);
  msn_table_add_cmd(cbs_table,0,"ILN",iln_cmd);
  msn_table_add_cmd(cbs_table,0,"OUT",out_cmd);
  msn_table_add_cmd(cbs_table,0,"RNG",rng_cmd);
  msn_table_add_cmd(cbs_table,0,"UBX",ubx_cmd);
  msn_table_add_cmd(cbs_table,0,"UUX",uux_cmd);
  msn_table_add_cmd(cbs_table,0,"UBN",ubn_cmd);
  msn_table_add_cmd(cbs_table,0,"UUN",uun_cmd);
  msn_table_add_cmd(cbs_table,0,"URL",url_cmd);
  msn_table_add_cmd(cbs_table,"fallback","XFR",xfr_cmd);
  msn_table_add_error(cbs_table,"ADL",adl_error);
  msn_table_add_error(cbs_table,"RML",rml_error);
  msn_table_add_error(cbs_table,"FQY",fqy_error);
  msn_table_add_error(cbs_table,"USR",usr_error);
  msn_table_add_msg_type(cbs_table,"text/x-msmsgsprofile",profile_msg);
/*initial OIM notification*/
  msn_table_add_msg_type(cbs_table,"text/x-msmsgsinitialmdatanotification",initial_mdata_msg);
/*OIM notification when user online*/
  msn_table_add_msg_type(cbs_table,"text/x-msmsgsoimnotification",initial_mdata_msg);
  msn_table_add_msg_type(cbs_table,"text/x-msmsgsinitialemailnotification",initial_email_msg);
  msn_table_add_msg_type(cbs_table,"text/x-msmsgsemailnotification",email_msg);
/*delete an offline Message notification*/
  msn_table_add_msg_type(cbs_table,"text/x-msmsgsactivemailnotification",delete_oim_msg);
  msn_table_add_msg_type(cbs_table,"application/x-msmsgssystemmessage",system_msg);
/* generic message handlers */
  msn_table_add_msg_type(cbs_table,"text/plain",msn_plain_msg);
  msn_table_add_msg_type(cbs_table,"text/x-msmsgscontrol",msn_control_msg);
  msn_table_add_msg_type(cbs_table,"text/x-msnmsgr-datacast",msn_datacast_msg);
}

void msn_notification_end()
{
  msn_table_destroy(cbs_table);
}
