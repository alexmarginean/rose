/**
 * @file userlist.c MSN user list support
 *
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "debug.h"
#include "request.h"
#include "msn.h"
#include "msnutils.h"
#include "userlist.h"
#include "contact.h"
const char *lists[] = {("FL"), ("AL"), ("BL"), ("RL")};
typedef struct __unnamed_class___F0_L37_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L256R__Pe___variable_name_unknown_scope_and_name__scope__gc__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__who__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__friendly {
PurpleConnection *gc;
char *who;
char *friendly;}MsnPermitAdd;
/**************************************************************************
 * Callbacks
 **************************************************************************/

static void msn_accept_add_cb(gpointer data)
{
  MsnPermitAdd *pa = data;
  purple_debug_misc("msn","Accepted the new buddy: %s\n",(pa -> who));
  if (g_list_find(purple_connections_get_all(),(pa -> gc)) != ((GList *)((void *)0))) {
    MsnSession *session = ( *(pa -> gc)).proto_data;
    MsnUserList *userlist = (session -> userlist);
    PurpleAccount *account = purple_connection_get_account((pa -> gc));
    msn_userlist_add_buddy_to_list(userlist,(pa -> who),MSN_LIST_AL);
    purple_privacy_deny_remove(account,(pa -> who),(!0));
    purple_privacy_permit_add(account,(pa -> who),(!0));
    msn_del_contact_from_list(session,0,(pa -> who),MSN_LIST_PL);
  }
  g_free((pa -> who));
  g_free((pa -> friendly));
  g_free(pa);
}

static void msn_cancel_add_cb(gpointer data)
{
  MsnPermitAdd *pa = data;
  purple_debug_misc("msn","Denied the new buddy: %s\n",(pa -> who));
  if (g_list_find(purple_connections_get_all(),(pa -> gc)) != ((GList *)((void *)0))) {
    MsnSession *session = ( *(pa -> gc)).proto_data;
    MsnUserList *userlist = (session -> userlist);
    MsnCallbackState *state = msn_callback_state_new(session);
    msn_callback_state_set_action(state,MSN_DENIED_BUDDY);
    msn_userlist_add_buddy_to_list(userlist,(pa -> who),MSN_LIST_BL);
    msn_del_contact_from_list(session,state,(pa -> who),MSN_LIST_PL);
  }
  g_free((pa -> who));
  g_free((pa -> friendly));
  g_free(pa);
}

static void got_new_entry(PurpleConnection *gc,const char *passport,const char *friendly,const char *message)
{
  PurpleAccount *acct;
  MsnPermitAdd *pa;
  pa = ((MsnPermitAdd *)(g_malloc0_n(1,(sizeof(MsnPermitAdd )))));
  pa -> who = g_strdup(passport);
  pa -> friendly = g_strdup(friendly);
  pa -> gc = gc;
  acct = purple_connection_get_account(gc);
  purple_account_request_authorization(acct,passport,0,friendly,message,(purple_find_buddy(acct,passport) != ((PurpleBuddy *)((void *)0))),msn_accept_add_cb,msn_cancel_add_cb,pa);
}
/**************************************************************************
 * Server functions
 **************************************************************************/

void msn_got_lst_user(MsnSession *session,MsnUser *user,MsnListOp list_op,GSList *group_ids)
{
  PurpleConnection *gc;
  PurpleAccount *account;
  const char *passport;
  const char *store;
  const char *message;
  account = (session -> account);
  gc = purple_account_get_connection(account);
  passport = msn_user_get_passport(user);
  store = msn_user_get_friendly_name(user);
  message = msn_user_get_invite_message(user);
  msn_user_set_op(user,list_op);
  if ((list_op & MSN_LIST_FL_OP) != 0U) {
    GSList *c;
    for (c = group_ids; c != ((GSList *)((void *)0)); c = ((c != 0)?( *((GSList *)c)).next : ((struct _GSList *)((void *)0)))) {
      char *group_id = (c -> data);
      msn_user_add_group_id(user,group_id);
    }
/* FIXME: It might be a real alias */
/* Umm, what? This might fix bug #1385130 */
    serv_got_alias(gc,passport,store);
  }
  if ((list_op & MSN_LIST_AL_OP) != 0U) {
/* These are users who are allowed to see our status. */
    purple_privacy_deny_remove(account,passport,(!0));
    purple_privacy_permit_add(account,passport,(!0));
  }
  if ((list_op & MSN_LIST_BL_OP) != 0U) {
/* These are users who are not allowed to see our status. */
    purple_privacy_permit_remove(account,passport,(!0));
    purple_privacy_deny_add(account,passport,(!0));
  }
  if ((list_op & MSN_LIST_RL_OP) != 0U) {
/* These are users who have us on their buddy list. */
/*
		 * TODO: What is store name set to when this happens?
		 *       For one of my accounts "something@hotmail.com"
		 *       the store name was "something."  Maybe we
		 *       should use the friendly name, instead? --KingAnt
		 */
    if (!((list_op & (MSN_LIST_AL_OP | MSN_LIST_BL_OP)) != 0U)) {
/*			got_new_entry(gc, passport, store, NULL); */
    }
  }
  if ((list_op & MSN_LIST_PL_OP) != 0U) {
    user -> authorized = (!0);
    got_new_entry(gc,passport,store,message);
  }
}
/**************************************************************************
 * UserList functions
 **************************************************************************/

MsnUserList *msn_userlist_new(MsnSession *session)
{
  MsnUserList *userlist;
  userlist = ((MsnUserList *)(g_malloc0_n(1,(sizeof(MsnUserList )))));
  userlist -> session = session;
  userlist -> buddy_icon_requests = g_queue_new();
/* buddy_icon_window is the number of allowed simultaneous buddy icon requests.
	 * XXX With smarter rate limiting code, we could allow more at once... 5 was the limit set when
	 * we weren't retrieiving any more than 5 per MSN session. */
  userlist -> buddy_icon_window = 1;
  return userlist;
}

void msn_userlist_destroy(MsnUserList *userlist)
{
  GList *l;
/*destroy userlist*/
  for (l = (userlist -> users); l != ((GList *)((void *)0)); l = (l -> next)) {
    msn_user_unref((l -> data));
  }
  g_list_free((userlist -> users));
/*destroy group list*/
  for (l = (userlist -> groups); l != ((GList *)((void *)0)); l = (l -> next)) {
    msn_group_destroy((l -> data));
  }
  g_list_free((userlist -> groups));
  g_queue_free((userlist -> buddy_icon_requests));
  if ((userlist -> buddy_icon_request_timer) != 0U) 
    purple_timeout_remove((userlist -> buddy_icon_request_timer));
  g_free(userlist);
}

MsnUser *msn_userlist_find_add_user(MsnUserList *userlist,const char *passport,const char *friendly_name)
{
  MsnUser *user;
  user = msn_userlist_find_user(userlist,passport);
  if (user == ((MsnUser *)((void *)0))) {
    user = msn_user_new(userlist,passport,friendly_name);
    msn_userlist_add_user(userlist,user);
    msn_user_unref(user);
  }
  else {
    msn_user_set_friendly_name(user,friendly_name);
  }
  return user;
}

void msn_userlist_add_user(MsnUserList *userlist,MsnUser *user)
{
  msn_user_ref(user);
  userlist -> users = g_list_prepend((userlist -> users),user);
}

void msn_userlist_remove_user(MsnUserList *userlist,MsnUser *user)
{
  userlist -> users = g_list_remove((userlist -> users),user);
  g_queue_remove((userlist -> buddy_icon_requests),user);
  msn_user_unref(user);
}

MsnUser *msn_userlist_find_user(MsnUserList *userlist,const char *passport)
{
  GList *l;
  do {
    if (passport != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"passport != NULL");
      return 0;
    };
  }while (0);
  for (l = (userlist -> users); l != ((GList *)((void *)0)); l = (l -> next)) {
    MsnUser *user = (MsnUser *)(l -> data);
    do {
      if ((user -> passport) != ((char *)((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"user->passport != NULL");
        return 0;
      };
    }while (0);
    if (!(g_ascii_strcasecmp(passport,(user -> passport)) != 0)) {
      return user;
    }
  }
  return 0;
}

MsnUser *msn_userlist_find_user_with_id(MsnUserList *userlist,const char *uid)
{
  GList *l;
  do {
    if (uid != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"uid != NULL");
      return 0;
    };
  }while (0);
  for (l = (userlist -> users); l != ((GList *)((void *)0)); l = (l -> next)) {{
      MsnUser *user = (MsnUser *)(l -> data);
      if ((user -> uid) == ((char *)((void *)0))) {
        continue; 
      }
      if (!(g_ascii_strcasecmp(uid,(user -> uid)) != 0)) {
        return user;
      }
    }
  }
  return 0;
}

MsnUser *msn_userlist_find_user_with_mobile_phone(MsnUserList *userlist,const char *number)
{
  GList *l;
  do {
    if (number != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"number != NULL");
      return 0;
    };
  }while (0);
  for (l = (userlist -> users); l != ((GList *)((void *)0)); l = (l -> next)) {
    MsnUser *user = (MsnUser *)(l -> data);
    const char *user_number = msn_user_get_mobile_phone(user);
    if ((user_number != 0) && !(g_ascii_strcasecmp(number,user_number) != 0)) 
      return user;
  }
  return 0;
}

void msn_userlist_add_group(MsnUserList *userlist,MsnGroup *group)
{
  userlist -> groups = g_list_append((userlist -> groups),group);
}

void msn_userlist_remove_group(MsnUserList *userlist,MsnGroup *group)
{
  userlist -> groups = g_list_remove((userlist -> groups),group);
}

MsnGroup *msn_userlist_find_group_with_id(MsnUserList *userlist,const char *id)
{
  GList *l;
  do {
    if (userlist != ((MsnUserList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"userlist != NULL");
      return 0;
    };
  }while (0);
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  for (l = (userlist -> groups); l != ((GList *)((void *)0)); l = (l -> next)) {
    MsnGroup *group = (l -> data);
    if (!(g_ascii_strcasecmp((group -> id),id) != 0)) 
      return group;
  }
  return 0;
}

MsnGroup *msn_userlist_find_group_with_name(MsnUserList *userlist,const char *name)
{
  GList *l;
  do {
    if (userlist != ((MsnUserList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"userlist != NULL");
      return 0;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return 0;
    };
  }while (0);
  for (l = (userlist -> groups); l != ((GList *)((void *)0)); l = (l -> next)) {
    MsnGroup *group = (l -> data);
    if (((group -> name) != ((char *)((void *)0))) && !(g_ascii_strcasecmp(name,(group -> name)) != 0)) 
      return group;
  }
  return 0;
}

const char *msn_userlist_find_group_id(MsnUserList *userlist,const char *group_name)
{
  MsnGroup *group;
  group = msn_userlist_find_group_with_name(userlist,group_name);
  if (group != ((MsnGroup *)((void *)0))) 
    return (msn_group_get_id(group));
  else 
    return 0;
}

const char *msn_userlist_find_group_name(MsnUserList *userlist,const char *group_id)
{
  MsnGroup *group;
  group = msn_userlist_find_group_with_id(userlist,group_id);
  if (group != ((MsnGroup *)((void *)0))) 
    return msn_group_get_name(group);
  else 
    return 0;
}

void msn_userlist_rename_group_id(MsnUserList *userlist,const char *group_id,const char *new_name)
{
  MsnGroup *group;
  group = msn_userlist_find_group_with_id(userlist,group_id);
  if (group != ((MsnGroup *)((void *)0))) 
    msn_group_set_name(group,new_name);
}

void msn_userlist_remove_group_id(MsnUserList *userlist,const char *group_id)
{
  MsnGroup *group;
  group = msn_userlist_find_group_with_id(userlist,group_id);
  if (group != ((MsnGroup *)((void *)0))) {
    msn_userlist_remove_group(userlist,group);
    msn_group_destroy(group);
  }
}
typedef struct __unnamed_class___F0_L433_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L371R__Pe___variable_name_unknown_scope_and_name__scope__session__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__uid {
MsnSession *session;
char *uid;}MsnUserlistABData;

static void userlist_ab_delete_cb(void *data,int choice)
{
  MsnUserlistABData *ab = (MsnUserlistABData *)data;
/* msn_delete_contact(ab->session, ab->uid, (gboolean)choice); */
  g_free((ab -> uid));
  g_free(ab);
}

void msn_userlist_rem_buddy(MsnUserList *userlist,const char *who)
{
  MsnUser *user = (MsnUser *)((void *)0);
  do {
    if (userlist != ((MsnUserList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"userlist != NULL");
      return ;
    };
  }while (0);
  do {
    if ((userlist -> session) != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"userlist->session != NULL");
      return ;
    };
  }while (0);
  do {
    if (who != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"who != NULL");
      return ;
    };
  }while (0);
  user = msn_userlist_find_user(userlist,who);
  msn_userlist_rem_buddy_from_list(userlist,who,MSN_LIST_FL);
/* delete the contact from address book via soap action */
  if (user != ((MsnUser *)((void *)0))) {
/*not ready yet*/
    if (0 && (( *(userlist -> session)).passport_info.email_enabled != 0)) {
      MsnUserlistABData *ab = (MsnUserlistABData *)(g_malloc0_n(1,(sizeof(MsnUserlistABData ))));
      ab -> session = (userlist -> session);
/* Not necessary? */
      ab -> uid = g_strdup((user -> uid));
      purple_request_action(( *(userlist -> session)).account,((const char *)(dgettext("pidgin","Delete Buddy from Address Book\?"))),((const char *)(dgettext("pidgin","Do you want to delete this buddy from your address book as well\?"))),(user -> passport),0,( *(userlist -> session)).account,(user -> passport),0,ab,2,((const char *)(dgettext("pidgin","_Yes"))),((GCallback )userlist_ab_delete_cb),((const char *)(dgettext("pidgin","_No"))),((GCallback )userlist_ab_delete_cb));
    }
    else 
      msn_delete_contact((userlist -> session),user);
  }
}

void msn_userlist_rem_buddy_from_list(MsnUserList *userlist,const char *who,MsnListId list_id)
{
  MsnUser *user;
  const gchar *list;
  MsnListOp list_op = (1 << list_id);
  user = msn_userlist_find_user(userlist,who);
  do {
    if (user != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  if (!(msn_user_is_in_list(user,list_id) != 0)) {
    list = lists[list_id];
    purple_debug_info("msn","User %s is not in list %s, not removing.\n",who,list);
    return ;
  }
  msn_user_unset_op(user,list_op);
  msn_notification_rem_buddy_from_list(( *(userlist -> session)).notification,list_id,user);
}
/*add buddy*/

void msn_userlist_add_buddy(MsnUserList *userlist,const char *who,const char *group_name)
{
  MsnUser *user;
  MsnCallbackState *state = (MsnCallbackState *)((void *)0);
  const char *group_id = (const char *)((void *)0);
  const char *new_group_name;
  new_group_name = ((group_name == ((const char *)((void *)0)))?((const char *)(dgettext("pidgin","Other Contacts"))) : group_name);
  do {
    if (userlist != ((MsnUserList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"userlist != NULL");
      return ;
    };
  }while (0);
  do {
    if ((userlist -> session) != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"userlist->session != NULL");
      return ;
    };
  }while (0);
  purple_debug_info("msn","Add user: %s to group: %s\n",who,new_group_name);
  if (!(msn_email_is_valid(who) != 0)) {
/* only notify the user about problems adding to the friends list
		 * maybe we should do something else for other lists, but it probably
		 * won't cause too many problems if we just ignore it */
    char *str = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to add \"%s\"."))),who);
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,str,((const char *)(dgettext("pidgin","The username specified is invalid."))),0,0);
    g_free(str);
    return ;
  }
  state = msn_callback_state_new((userlist -> session));
  msn_callback_state_set_who(state,who);
  msn_callback_state_set_new_group_name(state,new_group_name);
  group_id = msn_userlist_find_group_id(userlist,new_group_name);
  if (group_id == ((const char *)((void *)0))) {
/* Whoa, we must add that group first. */
    purple_debug_info("msn","Adding user %s to a new group, creating group %s first\n",who,new_group_name);
    msn_callback_state_set_action(state,MSN_ADD_BUDDY);
    msn_add_group((userlist -> session),state,new_group_name);
    return ;
  }
  else {
    msn_callback_state_set_guid(state,group_id);
  }
/* XXX: adding user here may not be correct (should add them in the
 	 * ACK to the ADL command), but for now we need to make sure they exist
	 * early enough that the ILN command doesn't screw us up */
  user = msn_userlist_find_add_user(userlist,who,who);
  if (msn_user_is_in_list(user,MSN_LIST_FL) != 0) {
    purple_debug_info("msn","User %s already exists\n",who);
    msn_userlist_rem_buddy_from_list(userlist,who,MSN_LIST_BL);
    if (msn_user_is_in_group(user,group_id) != 0) {
      purple_debug_info("msn","User %s is already in group %s, returning\n",who,new_group_name);
      msn_callback_state_free(state);
      return ;
    }
  }
  purple_debug_info("msn","Adding user: %s to group id: %s\n",who,group_id);
  msn_callback_state_set_action(state,MSN_ADD_BUDDY);
/* Add contact in the Contact server with a SOAP request and if
	   successful, send ADL with MSN_LIST_AL and MSN_LIST_FL and a FQY */
  msn_add_contact_to_group((userlist -> session),state,who,group_id);
}

void msn_userlist_add_buddy_to_list(MsnUserList *userlist,const char *who,MsnListId list_id)
{
  MsnUser *user = (MsnUser *)((void *)0);
  const gchar *list;
  MsnListOp list_op = (1 << list_id);
  do {
    if (userlist != ((MsnUserList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"userlist != NULL");
      return ;
    };
  }while (0);
  user = msn_userlist_find_add_user(userlist,who,who);
/* First we're going to check if it's already there. */
  if (msn_user_is_in_list(user,list_id) != 0) {
    list = lists[list_id];
    purple_debug_info("msn","User \'%s\' is already in list: %s\n",who,list);
    return ;
  }
/* XXX: see XXX above, this should really be done when we get the response from
		the server */
  msn_user_set_op(user,list_op);
  msn_notification_add_buddy_to_list(( *(userlist -> session)).notification,list_id,user);
}

gboolean msn_userlist_add_buddy_to_group(MsnUserList *userlist,const char *who,const char *group_name)
{
  MsnUser *user;
  gchar *group_id;
  do {
    if (userlist != ((MsnUserList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"userlist != NULL");
      return 0;
    };
  }while (0);
  do {
    if (group_name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"group_name != NULL");
      return 0;
    };
  }while (0);
  do {
    if (who != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"who != NULL");
      return 0;
    };
  }while (0);
  purple_debug_info("msn","Adding buddy with passport %s to group %s\n",who,group_name);
  if ((group_id = ((gchar *)(msn_userlist_find_group_id(userlist,group_name)))) == ((gchar *)((void *)0))) {
    purple_debug_error("msn","Group %s has no guid!\n",group_name);
    return 0;
  }
  if ((user = msn_userlist_find_user(userlist,who)) == ((MsnUser *)((void *)0))) {
    purple_debug_error("msn","User %s not found!\n",who);
    return 0;
  }
  msn_user_add_group_id(user,group_id);
  return (!0);
}

gboolean msn_userlist_rem_buddy_from_group(MsnUserList *userlist,const char *who,const char *group_name)
{
  const gchar *group_id;
  MsnUser *user;
  do {
    if (userlist != ((MsnUserList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"userlist != NULL");
      return 0;
    };
  }while (0);
  do {
    if (group_name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"group_name != NULL");
      return 0;
    };
  }while (0);
  do {
    if (who != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"who != NULL");
      return 0;
    };
  }while (0);
  purple_debug_info("msn","Removing buddy with passport %s from group %s\n",who,group_name);
  if ((group_id = msn_userlist_find_group_id(userlist,group_name)) == ((const gchar *)((void *)0))) {
    purple_debug_error("msn","Group %s has no guid!\n",group_name);
    return 0;
  }
  if ((user = msn_userlist_find_user(userlist,who)) == ((MsnUser *)((void *)0))) {
    purple_debug_error("msn","User %s not found!\n",who);
    return 0;
  }
  msn_user_remove_group_id(user,group_id);
  return (!0);
}

void msn_userlist_move_buddy(MsnUserList *userlist,const char *who,const char *old_group_name,const char *new_group_name)
{
  const char *new_group_id;
  MsnCallbackState *state;
  do {
    if (userlist != ((MsnUserList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"userlist != NULL");
      return ;
    };
  }while (0);
  do {
    if ((userlist -> session) != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"userlist->session != NULL");
      return ;
    };
  }while (0);
  state = msn_callback_state_new((userlist -> session));
  msn_callback_state_set_who(state,who);
  msn_callback_state_set_action(state,MSN_MOVE_BUDDY);
  msn_callback_state_set_old_group_name(state,old_group_name);
  msn_callback_state_set_new_group_name(state,new_group_name);
  new_group_id = msn_userlist_find_group_id(userlist,new_group_name);
  if (new_group_id == ((const char *)((void *)0))) {
    msn_add_group((userlist -> session),state,new_group_name);
    return ;
  }
/* add the contact to the new group, and remove it from the old one in
	 * the callback
	*/
  msn_add_contact_to_group((userlist -> session),state,who,new_group_id);
}

void msn_release_buddy_icon_request(MsnUserList *userlist)
{
  MsnUser *user;
  do {
    if (userlist != ((MsnUserList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"userlist != NULL");
      return ;
    };
  }while (0);
  if (purple_debug_is_verbose() != 0) 
    purple_debug_info("msn","Releasing buddy icon request\n");
  if ((userlist -> buddy_icon_window) > 0) {
    GQueue *queue;
    queue = (userlist -> buddy_icon_requests);
    if (g_queue_is_empty((userlist -> buddy_icon_requests)) != 0) 
      return ;
    user = (g_queue_pop_head(queue));
    userlist -> buddy_icon_window--;
    msn_request_user_display(user);
    if (purple_debug_is_verbose() != 0) 
      purple_debug_info("msn","msn_release_buddy_icon_request(): buddy_icon_window-- yields =%d\n",(userlist -> buddy_icon_window));
  }
}
/*load userlist from the Blist file cache*/

void msn_userlist_load(MsnSession *session)
{
  PurpleAccount *account = (session -> account);
  PurpleConnection *gc = purple_account_get_connection(account);
  GSList *l;
  MsnUser *user;
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  for (l = purple_find_buddies(account,0); l != ((GSList *)((void *)0)); l = g_slist_delete_link(l,l)) {
    PurpleBuddy *buddy = (l -> data);
    user = msn_userlist_find_add_user((session -> userlist),purple_buddy_get_name(buddy),0);
    purple_buddy_set_protocol_data(buddy,user);
    msn_user_set_op(user,MSN_LIST_FL_OP);
  }
  for (l = ( *(session -> account)).permit; l != ((GSList *)((void *)0)); l = (l -> next)) {
    user = msn_userlist_find_add_user((session -> userlist),((char *)(l -> data)),0);
    msn_user_set_op(user,MSN_LIST_AL_OP);
  }
  for (l = ( *(session -> account)).deny; l != ((GSList *)((void *)0)); l = (l -> next)) {
    user = msn_userlist_find_add_user((session -> userlist),((char *)(l -> data)),0);
    msn_user_set_op(user,MSN_LIST_BL_OP);
  }
}
