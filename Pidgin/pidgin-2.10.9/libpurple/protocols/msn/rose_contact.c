/**
 * @file contact.c
 * 	get MSN contacts via SOAP request
 *	created by MaYuan<mayuan2006@gmail.com>
 *
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,  USA
 */
#include "internal.h"
#include "debug.h"
#include "contact.h"
#include "xmlnode.h"
#include "group.h"
#include "msnutils.h"
#include "soap.h"
#include "nexus.h"
#include "user.h"
const char *MsnSoapPartnerScenarioText[] = {("Initial"), ("ContactSave"), ("MessengerPendingList"), ("ContactMsgrAPI"), ("BlockUnblock"), ("Timer")};
const char *MsnMemberRole[] = {("Forward"), ("Allow"), ("Block"), ("Reverse"), ("Pending")};
typedef struct __unnamed_class___F0_L57_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L366R__Pe___variable_name_unknown_scope_and_name__scope__session__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L435R_variable_name_unknown_scope_and_name__scope__which {
MsnSession *session;
MsnSoapPartnerScenario which;}GetContactListCbData;

MsnCallbackState *msn_callback_state_new(MsnSession *session)
{
  MsnCallbackState *state = (MsnCallbackState *)(g_malloc0_n(1,(sizeof(MsnCallbackState ))));
  state -> session = session;
  return state;
}

MsnCallbackState *msn_callback_state_dup(MsnCallbackState *state)
{
  MsnCallbackState *new_state = (MsnCallbackState *)(g_malloc0_n(1,(sizeof(MsnCallbackState ))));
  new_state -> session = (state -> session);
  new_state -> who = g_strdup((state -> who));
  new_state -> uid = g_strdup((state -> uid));
  new_state -> old_group_name = g_strdup((state -> old_group_name));
  new_state -> new_group_name = g_strdup((state -> new_group_name));
  new_state -> guid = g_strdup((state -> guid));
/* The rest should be made new */
  return new_state;
}

void msn_callback_state_free(MsnCallbackState *state)
{
  if (state == ((MsnCallbackState *)((void *)0))) 
    return ;
  g_free((state -> who));
  g_free((state -> uid));
  g_free((state -> old_group_name));
  g_free((state -> new_group_name));
  g_free((state -> guid));
  xmlnode_free((state -> body));
  g_free(state);
}

void msn_callback_state_set_who(MsnCallbackState *state,const gchar *who)
{
  do {
    if (state != ((MsnCallbackState *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"state != NULL");
      return ;
    };
  }while (0);
  g_free((state -> who));
  state -> who = g_strdup(who);
}

void msn_callback_state_set_uid(MsnCallbackState *state,const gchar *uid)
{
  do {
    if (state != ((MsnCallbackState *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"state != NULL");
      return ;
    };
  }while (0);
  g_free((state -> uid));
  state -> uid = g_strdup(uid);
}

void msn_callback_state_set_old_group_name(MsnCallbackState *state,const gchar *old_group_name)
{
  do {
    if (state != ((MsnCallbackState *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"state != NULL");
      return ;
    };
  }while (0);
  g_free((state -> old_group_name));
  state -> old_group_name = g_strdup(old_group_name);
}

void msn_callback_state_set_new_group_name(MsnCallbackState *state,const gchar *new_group_name)
{
  do {
    if (state != ((MsnCallbackState *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"state != NULL");
      return ;
    };
  }while (0);
  g_free((state -> new_group_name));
  state -> new_group_name = g_strdup(new_group_name);
}

void msn_callback_state_set_guid(MsnCallbackState *state,const gchar *guid)
{
  do {
    if (state != ((MsnCallbackState *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"state != NULL");
      return ;
    };
  }while (0);
  g_free((state -> guid));
  state -> guid = g_strdup(guid);
}

void msn_callback_state_set_list_id(MsnCallbackState *state,MsnListId list_id)
{
  do {
    if (state != ((MsnCallbackState *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"state != NULL");
      return ;
    };
  }while (0);
  state -> list_id = list_id;
}

void msn_callback_state_set_action(MsnCallbackState *state,MsnCallbackAction action)
{
  do {
    if (state != ((MsnCallbackState *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"state != NULL");
      return ;
    };
  }while (0);
  state -> action |= action;
}
/***************************************************************
 * General SOAP handling
 ***************************************************************/

static const char *msn_contact_operation_str(MsnCallbackAction action)
{
/* Make sure this is large enough when adding more */
  static char buf[2048UL];
  buf[0] = 0;
  if ((action & MSN_ADD_BUDDY) != 0U) 
    strcat(buf,"Adding Buddy,");
  if ((action & MSN_MOVE_BUDDY) != 0U) 
    strcat(buf,"Moving Buddy,");
  if ((action & MSN_ACCEPTED_BUDDY) != 0U) 
    strcat(buf,"Accepted Buddy,");
  if ((action & MSN_DENIED_BUDDY) != 0U) 
    strcat(buf,"Denied Buddy,");
  if ((action & MSN_ADD_GROUP) != 0U) 
    strcat(buf,"Adding Group,");
  if ((action & MSN_DEL_GROUP) != 0U) 
    strcat(buf,"Deleting Group,");
  if ((action & MSN_RENAME_GROUP) != 0U) 
    strcat(buf,"Renaming Group,");
  if ((action & MSN_UPDATE_INFO) != 0U) 
    strcat(buf,"Updating Contact Info,");
  if ((action & MSN_ANNOTATE_USER) != 0U) 
    strcat(buf,"Annotating Contact,");
  return buf;
}
static gboolean msn_contact_request(MsnCallbackState *state);

static void msn_contact_request_cb(MsnSoapMessage *req,MsnSoapMessage *resp,gpointer data)
{
  MsnCallbackState *state = data;
  xmlnode *fault;
  char *faultcode_str;
  xmlnode *cachekey;
  char *changed;
  if (resp == ((MsnSoapMessage *)((void *)0))) {
    purple_debug_error("msn","Operation {%s} failed. No response received from server.\n",msn_contact_operation_str((state -> action)));
    msn_session_set_error((state -> session),MSN_ERROR_BAD_BLIST,0);
    msn_callback_state_free(state);
    return ;
  }
/* Update CacheKey if necessary */
  cachekey = xmlnode_get_child((resp -> xml),"Header/ServiceHeader/CacheKeyChanged");
  if (cachekey != ((xmlnode *)((void *)0))) {
    changed = xmlnode_get_data(cachekey);
    if ((changed != 0) && !(strcmp(changed,"true") != 0)) {
      cachekey = xmlnode_get_child((resp -> xml),"Header/ServiceHeader/CacheKey");
      g_free(( *(state -> session)).abch_cachekey);
      ( *(state -> session)).abch_cachekey = xmlnode_get_data(cachekey);
      purple_debug_info("msn","Updated CacheKey for %s to \'%s\'.\n",purple_account_get_username(( *(state -> session)).account),( *(state -> session)).abch_cachekey);
    }
    g_free(changed);
  }
  fault = xmlnode_get_child((resp -> xml),"Body/Fault");
  if (fault == ((xmlnode *)((void *)0))) {
/* No errors */
    if ((state -> cb) != 0) 
      ( *(state -> cb))(req,resp,data);
    msn_callback_state_free(state);
    return ;
  }
  faultcode_str = xmlnode_get_data((xmlnode_get_child(fault,"faultcode")));
  if ((faultcode_str != 0) && (g_str_equal(faultcode_str,"q0:BadContextToken") != 0)) {
    purple_debug_info("msn","Contact Operation {%s} failed because of bad token. Updating token now and retrying operation.\n",msn_contact_operation_str((state -> action)));
/* Token has expired, so renew it, and try again later */
    msn_nexus_update_token(( *(state -> session)).nexus,MSN_AUTH_CONTACTS,((GSourceFunc )msn_contact_request),data);
  }
  else {
    if ((state -> cb) != 0) {
      ( *(state -> cb))(req,resp,data);
    }
    else {
/* We don't know how to respond to this faultcode, so log it */
      char *str = xmlnode_to_str(fault,0);
      purple_debug_error("msn","Operation {%s} Failed, SOAP Fault was: %s\n",msn_contact_operation_str((state -> action)),str);
      g_free(str);
    }
    msn_callback_state_free(state);
  }
  g_free(faultcode_str);
}

static gboolean msn_contact_request(MsnCallbackState *state)
{
  xmlnode *cachekey = xmlnode_get_child((state -> body),"Header/ABApplicationHeader/CacheKey");
  if (cachekey != ((xmlnode *)((void *)0))) 
    xmlnode_free(cachekey);
  if (( *(state -> session)).abch_cachekey != ((char *)((void *)0))) {
    cachekey = xmlnode_new_child(xmlnode_get_child((state -> body),"Header/ABApplicationHeader"),"CacheKey");
    xmlnode_insert_data(cachekey,( *(state -> session)).abch_cachekey,(-1));
  }
  if ((state -> token) == ((xmlnode *)((void *)0))) 
    state -> token = xmlnode_get_child((state -> body),"Header/ABAuthHeader/TicketToken");
/* delete old & replace with new token */
  xmlnode_free(( *(state -> token)).child);
  xmlnode_insert_data((state -> token),msn_nexus_get_token_str(( *(state -> session)).nexus,MSN_AUTH_CONTACTS),(-1));
  msn_soap_message_send((state -> session),msn_soap_message_new((state -> post_action),xmlnode_copy((state -> body))),"local-bay.contacts.msn.com",(state -> post_url),0,msn_contact_request_cb,state);
  return 0;
}
/***************************************************************
 * Address Book and Membership List Operations
 ***************************************************************/
/*get MSN member role utility*/

static MsnListId msn_get_memberrole(const char *role)
{
  do {
    if (role != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"role != NULL");
      return 0;
    };
  }while (0);
  if (!(strcmp(role,"Allow") != 0)) {
    return MSN_LIST_AL;
  }
  else if (!(strcmp(role,"Block") != 0)) {
    return MSN_LIST_BL;
  }
  else if (!(strcmp(role,"Reverse") != 0)) {
    return MSN_LIST_RL;
  }
  else if (!(strcmp(role,"Pending") != 0)) {
    return MSN_LIST_PL;
  }
  return 0;
}
/* Create the AddressBook in the server, if we don't have one */

static void msn_create_address_cb(MsnSoapMessage *req,MsnSoapMessage *resp,gpointer data)
{
  MsnCallbackState *state = data;
  if ((resp != 0) && (xmlnode_get_child((resp -> xml),"Body/Fault") == ((xmlnode *)((void *)0)))) {
    purple_debug_info("msn","Address Book successfully created!\n");
    msn_get_address_book((state -> session),MSN_PS_INITIAL,0,0);
  }
  else {
    purple_debug_info("msn","Address Book creation failed!\n");
  }
}

static void msn_create_address_book(MsnSession *session)
{
  gchar *body;
  MsnCallbackState *state;
  do {
    if (session != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return ;
    };
  }while (0);
  do {
    if ((session -> user) != ((MsnUser *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session->user != NULL");
      return ;
    };
  }while (0);
  do {
    if (( *(session -> user)).passport != ((char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session->user->passport != NULL");
      return ;
    };
  }while (0);
  purple_debug_info("msn","Creating an Address Book.\n");
  body = g_strdup_printf("<\?xml version=\"1.0\" encoding=\"utf-8\"\?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\"><soap:Header><ABApplicationHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ApplicationId>CFE80F9D-180F-4399-82AB-413F33A1FA11</ApplicationId><IsMigration>false</IsMigration><PartnerScenario>Initial</PartnerScenario></ABApplicationHeader><ABAuthHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ManagedGroupRequest>false</ManagedGroupRequest><TicketToken>EMPTY</TicketToken></ABAuthHeader></soap:Header><soap:Body><ABAdd xmlns=\"http://www.msn.com/webservices/AddressBook\"><abInfo><name/><ownerPuid>0</ownerPuid><ownerEmail>%s</ownerEmail><fDefault>true</fDefault></abInfo></ABAdd></soap:Body></soap:Envelope>",( *(session -> user)).passport);
  state = msn_callback_state_new(session);
  state -> body = xmlnode_from_str(body,(-1));
  state -> post_action = "http://www.msn.com/webservices/AddressBook/ABAdd";
  state -> post_url = "/abservice/abservice.asmx";
  state -> cb = msn_create_address_cb;
  msn_contact_request(state);
  g_free(body);
}

static void msn_parse_each_member(MsnSession *session,xmlnode *member,const char *node,MsnListId list)
{
  char *passport;
  char *type;
  char *member_id;
  MsnUser *user;
  xmlnode *annotation;
  guint nid = MSN_NETWORK_UNKNOWN;
  char *invite = (char *)((void *)0);
  passport = xmlnode_get_data((xmlnode_get_child(member,node)));
  if (!(msn_email_is_valid(passport) != 0)) {
    g_free(passport);
    return ;
  }
  type = xmlnode_get_data((xmlnode_get_child(member,"Type")));
  member_id = xmlnode_get_data((xmlnode_get_child(member,"MembershipId")));
  user = msn_userlist_find_add_user((session -> userlist),passport,0);
  for (annotation = xmlnode_get_child(member,"Annotations/Annotation"); annotation != 0; annotation = xmlnode_get_next_twin(annotation)) {
    char *name = xmlnode_get_data((xmlnode_get_child(annotation,"Name")));
    char *value = xmlnode_get_data((xmlnode_get_child(annotation,"Value")));
    if ((name != 0) && (value != 0)) {
      if (!(strcmp(name,"MSN.IM.BuddyType") != 0)) {
        nid = (strtoul(value,0,10));
      }
      else if (!(strcmp(name,"MSN.IM.InviteMessage") != 0)) {
        invite = value;
        value = ((char *)((void *)0));
      }
    }
    g_free(name);
    g_free(value);
  }
/* For EmailMembers, the network must be found in the annotations, above.
	   Otherwise, PassportMembers are on the Passport network. */
  if (!(strcmp(node,"PassportName") != 0)) 
    nid = MSN_NETWORK_PASSPORT;
  purple_debug_info("msn","CL: %s name: %s, Type: %s, MembershipID: %s, NetworkID: %u\n",node,passport,type,((member_id == ((char *)((void *)0)))?"(null)" : member_id),nid);
  msn_user_set_network(user,nid);
  msn_user_set_invite_message(user,invite);
  if ((list == MSN_LIST_PL) && (member_id != 0)) {
    user -> member_id_on_pending_list = (atoi(member_id));
  }
  msn_got_lst_user(session,user,(1 << list),0);
  g_free(passport);
  g_free(type);
  g_free(member_id);
  g_free(invite);
}

static void msn_parse_each_service(MsnSession *session,xmlnode *service)
{
  xmlnode *type;
  if ((type = xmlnode_get_child(service,"Info/Handle/Type")) != 0) {
    char *type_str = xmlnode_get_data(type);
    if (g_str_equal(type_str,"Profile") != 0) {
/* Process Windows Live 'Messenger Roaming Identity' */
    }
    else if (g_str_equal(type_str,"Messenger") != 0) {
      xmlnode *lastchange = xmlnode_get_child(service,"LastChange");
      char *lastchange_str = xmlnode_get_data(lastchange);
      xmlnode *membership;
      purple_debug_info("msn","CL last change: %s\n",lastchange_str);
      purple_account_set_string((session -> account),"CLLastChange",lastchange_str);
      for (membership = xmlnode_get_child(service,"Memberships/Membership"); membership != 0; membership = xmlnode_get_next_twin(membership)) {
        xmlnode *role = xmlnode_get_child(membership,"MemberRole");
        char *role_str = xmlnode_get_data(role);
        MsnListId list = msn_get_memberrole(role_str);
        xmlnode *member;
        purple_debug_info("msn","CL MemberRole role: %s, list: %d\n",role_str,list);
        for (member = xmlnode_get_child(membership,"Members/Member"); member != 0; member = xmlnode_get_next_twin(member)) {
          const char *member_type = xmlnode_get_attrib(member,"type");
          if (g_str_equal(member_type,"PassportMember") != 0) {
            msn_parse_each_member(session,member,"PassportName",list);
          }
          else if (g_str_equal(member_type,"PhoneMember") != 0) {
          }
          else if (g_str_equal(member_type,"EmailMember") != 0) {
            msn_parse_each_member(session,member,"Email",list);
          }
        }
        g_free(role_str);
      }
      g_free(lastchange_str);
    }
    g_free(type_str);
  }
}
/*parse contact list*/

static gboolean msn_parse_contact_list(MsnSession *session,xmlnode *node)
{
  xmlnode *fault;
  xmlnode *faultnode;
/* we may get a response if our cache data is too old:
	 *
	 * <faultstring>Need to do full sync. Can't sync deltas Client
	 * has too old a copy for us to do a delta sync</faultstring>
	 *
	 * this is not handled yet
	 */
  if ((fault = xmlnode_get_child(node,"Body/Fault")) != 0) {
    if ((faultnode = xmlnode_get_child(fault,"faultstring")) != 0) {
      char *faultstring = xmlnode_get_data(faultnode);
      purple_debug_info("msn","Retrieving contact list failed: %s\n",faultstring);
      g_free(faultstring);
    }
    if ((faultnode = xmlnode_get_child(fault,"detail/errorcode")) != 0) {
      char *errorcode = xmlnode_get_data(faultnode);
      if (g_str_equal(errorcode,"ABDoesNotExist") != 0) {
        msn_create_address_book(session);
        g_free(errorcode);
        return 0;
      }
      g_free(errorcode);
    }
    msn_get_contact_list(session,MSN_PS_INITIAL,0);
    return 0;
  }
  else {
    xmlnode *service;
    for (service = xmlnode_get_child(node,"Body/FindMembershipResponse/FindMembershipResult/Services/Service"); service != 0; service = xmlnode_get_next_twin(service)) {
      msn_parse_each_service(session,service);
    }
    return (!0);
  }
}

static void msn_get_contact_list_cb(MsnSoapMessage *req,MsnSoapMessage *resp,gpointer data)
{
  MsnCallbackState *state = data;
  MsnSession *session = (state -> session);
  do {
    if (session != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return ;
    };
  }while (0);
  if (resp != ((MsnSoapMessage *)((void *)0))) {
#ifdef MSN_PARTIAL_LISTS
#endif
    purple_debug_misc("msn","Got the contact list!\n");
    if (msn_parse_contact_list(session,(resp -> xml)) != 0) {
#ifdef MSN_PARTIAL_LISTS
#endif
      if ((state -> partner_scenario) == MSN_PS_INITIAL) {
#ifdef MSN_PARTIAL_LISTS
/* XXX: this should be enabled when we can correctly do partial
				   syncs with the server. Currently we need to retrieve the whole
				   list to detect sync issues */
#else
        msn_get_address_book(session,MSN_PS_INITIAL,0,0);
#endif
      }
    }
  }
}
/*SOAP  get contact list*/

void msn_get_contact_list(MsnSession *session,const MsnSoapPartnerScenario partner_scenario,const char *update_time)
{
  gchar *body = (gchar *)((void *)0);
  gchar *update_str = (gchar *)((void *)0);
  MsnCallbackState *state;
  const gchar *partner_scenario_str = MsnSoapPartnerScenarioText[partner_scenario];
  purple_debug_misc("msn","Getting Contact List.\n");
  if (update_time != ((const char *)((void *)0))) {
    purple_debug_info("msn","CL Last update time: %s\n",update_time);
    update_str = g_strdup_printf("<View>Full</View><deltasOnly>true</deltasOnly><lastChange>%s</lastChange>",update_time);
  }
  body = g_strdup_printf("<\?xml version=\'1.0\' encoding=\'utf-8\'\?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Header xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><ABApplicationHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ApplicationId xmlns=\"http://www.msn.com/webservices/AddressBook\">CFE80F9D-180F-4399-82AB-413F33A1FA11</ApplicationId><IsMigration xmlns=\"http://www.msn.com/webservices/AddressBook\">false</IsMigration><PartnerScenario xmlns=\"http://www.msn.com/webservices/AddressBook\">%s</PartnerScenario></ABApplicationHeader><ABAuthHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ManagedGroupRequest xmlns=\"http://www.msn.com/webservices/AddressBook\">false</ManagedGroupRequest><TicketToken>EMPTY</TicketToken></ABAuthHeader></soap:Header><soap:Body xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><FindMembership xmlns=\"http://www.msn.com/webservices/AddressBook\"><serviceFilter xmlns=\"http://www.msn.com/webservices/AddressBook\"><Types xmlns=\"http://www.msn.com/webservices/AddressBook\"><ServiceType xmlns=\"http://www.msn.com/webservices/AddressBook\">Messenger</ServiceType><ServiceType xmlns=\"http://www.msn.com/webservices/AddressBook\">Invitation</ServiceType><ServiceType xmlns=\"http://www.msn.com/webservices/AddressBook\">SocialNetwork</ServiceType><ServiceType xmlns=\"http://www.msn.com/webservices/AddressBook\">Space</ServiceType><ServiceType xmlns=\"http://www.msn.com/webservices/AddressBook\">Profile</ServiceType></Types></serviceFilter>%s</FindMembership></soap:Body></soap:Envelope>",partner_scenario_str,((update_str != 0)?update_str : ""));
  state = msn_callback_state_new(session);
  state -> partner_scenario = partner_scenario;
  state -> body = xmlnode_from_str(body,(-1));
  state -> post_action = "http://www.msn.com/webservices/AddressBook/FindMembership";
  state -> post_url = "/abservice/SharingService.asmx";
  state -> cb = msn_get_contact_list_cb;
  msn_contact_request(state);
  g_free(update_str);
  g_free(body);
}

static void msn_parse_addressbook_groups(MsnSession *session,xmlnode *node)
{
  xmlnode *group;
  purple_debug_info("msn","msn_parse_addressbook_groups()\n");
  for (group = xmlnode_get_child(node,"Group"); group != 0; group = xmlnode_get_next_twin(group)) {{
      xmlnode *groupId;
      xmlnode *groupInfo;
      xmlnode *groupname;
      char *group_id = (char *)((void *)0);
      char *group_name = (char *)((void *)0);
      if ((groupId = xmlnode_get_child(group,"groupId")) != 0) 
        group_id = xmlnode_get_data(groupId);
      if (((groupInfo = xmlnode_get_child(group,"groupInfo")) != 0) && ((groupname = xmlnode_get_child(groupInfo,"name")) != 0)) 
        group_name = xmlnode_get_data(groupname);
      if (group_id == ((char *)((void *)0))) {
/* Group of ungroupped buddies */
        g_free(group_name);
        continue; 
      }
      msn_group_new((session -> userlist),group_id,group_name);
      purple_debug_info("msn","AB group_id: %s, name: %s\n",group_id,((group_name != 0)?group_name : "(null)"));
      if (purple_find_group(group_name) == ((PurpleGroup *)((void *)0))) {
        PurpleGroup *g = purple_group_new(group_name);
        purple_blist_add_group(g,0);
      }
      g_free(group_id);
      g_free(group_name);
    }
  }
}

static gboolean msn_parse_addressbook_mobile(xmlnode *contactInfo,char **inout_mobile_number)
{
  xmlnode *phones;
  char *mobile_number = (char *)((void *)0);
  gboolean mobile = 0;
   *inout_mobile_number = ((char *)((void *)0));
  if ((phones = xmlnode_get_child(contactInfo,"phones")) != 0) {
    xmlnode *contact_phone;
    char *phone_type = (char *)((void *)0);
    for (contact_phone = xmlnode_get_child(phones,"ContactPhone"); contact_phone != 0; contact_phone = xmlnode_get_next_twin(contact_phone)) {{
        xmlnode *contact_phone_type;
        if (!((contact_phone_type = xmlnode_get_child(contact_phone,"contactPhoneType")) != 0)) 
          continue; 
        phone_type = xmlnode_get_data(contact_phone_type);
        if ((phone_type != 0) && !(strcmp(phone_type,"ContactPhoneMobile") != 0)) {
          xmlnode *number;
          if ((number = xmlnode_get_child(contact_phone,"number")) != 0) {
            xmlnode *messenger_enabled;
            char *is_messenger_enabled = (char *)((void *)0);
            g_free(mobile_number);
            mobile_number = xmlnode_get_data(number);
            if ((((mobile_number != 0) && ((messenger_enabled = xmlnode_get_child(contact_phone,"isMessengerEnabled")) != 0)) && ((is_messenger_enabled = xmlnode_get_data(messenger_enabled)) != 0)) && !(strcmp(is_messenger_enabled,"true") != 0)) 
              mobile = (!0);
            g_free(is_messenger_enabled);
          }
        }
        g_free(phone_type);
      }
    }
  }
   *inout_mobile_number = mobile_number;
  return mobile;
}

static void msn_parse_addressbook_contacts(MsnSession *session,xmlnode *node)
{
  xmlnode *contactNode;
  char *passport = (char *)((void *)0);
  char *Name = (char *)((void *)0);
  char *uid = (char *)((void *)0);
  char *type = (char *)((void *)0);
  char *mobile_number = (char *)((void *)0);
  char *alias = (char *)((void *)0);
  gboolean mobile = 0;
  PurpleConnection *pc = purple_account_get_connection((session -> account));
  for (contactNode = xmlnode_get_child(node,"Contact"); contactNode != 0; contactNode = xmlnode_get_next_twin(contactNode)) {{
      xmlnode *contactId;
      xmlnode *contactInfo;
      xmlnode *contactType;
      xmlnode *passportName;
      xmlnode *displayName;
      xmlnode *guid;
      xmlnode *groupIds;
      xmlnode *annotation;
      MsnUser *user;
      g_free(passport);
      g_free(Name);
      g_free(uid);
      g_free(type);
      g_free(mobile_number);
      g_free(alias);
      passport = (Name = (uid = (type = (mobile_number = (alias = ((char *)((void *)0)))))));
      mobile = 0;
      if ((!((contactId = xmlnode_get_child(contactNode,"contactId")) != 0) || !((contactInfo = xmlnode_get_child(contactNode,"contactInfo")) != 0)) || !((contactType = xmlnode_get_child(contactInfo,"contactType")) != 0)) 
        continue; 
      uid = xmlnode_get_data(contactId);
      type = xmlnode_get_data(contactType);
/* Find out our settings */
      if ((type != 0) && !(strcmp(type,"Me") != 0)) {
/* setup the Display Name */
        if (purple_connection_get_display_name(pc) == ((const char *)((void *)0))) {
          char *friendly = (char *)((void *)0);
          if ((displayName = xmlnode_get_child(contactInfo,"displayName")) != 0) 
            friendly = xmlnode_get_data(displayName);
          purple_connection_set_display_name(pc,((friendly != 0)?purple_url_decode(friendly) : ((const char *)((void *)0))));
          g_free(friendly);
        }
        for (annotation = xmlnode_get_child(contactInfo,"annotations/Annotation"); annotation != 0; annotation = xmlnode_get_next_twin(annotation)) {
          char *name;
          char *value;
          name = xmlnode_get_data((xmlnode_get_child(annotation,"Name")));
          value = xmlnode_get_data((xmlnode_get_child(annotation,"Value")));
          if ((name != 0) && (g_str_equal(name,"MSN.IM.MPOP") != 0)) {
            if (!(value != 0) || (atoi(value) != 0)) 
              session -> enable_mpop = (!0);
            else 
              session -> enable_mpop = 0;
          }
          g_free(name);
          g_free(value);
        }
/* Not adding own account as buddy to buddylist */
        continue; 
      }
      passportName = xmlnode_get_child(contactInfo,"passportName");
      if (passportName == ((xmlnode *)((void *)0))) {
        xmlnode *emailsNode;
        xmlnode *contactEmailNode;
        xmlnode *emailNode;
        xmlnode *messengerEnabledNode;
        char *msnEnabled;
/*TODO: add it to the non-instant Messenger group and recognize as email Membership*/
/* Yahoo/Federated User? */
        emailsNode = xmlnode_get_child(contactInfo,"emails");
        if (emailsNode == ((xmlnode *)((void *)0))) {
/*TODO:  need to support the Mobile type*/
          continue; 
        }
{
          for (contactEmailNode = xmlnode_get_child(emailsNode,"ContactEmail"); contactEmailNode != 0; contactEmailNode = xmlnode_get_next_twin(contactEmailNode)) {
            if ((messengerEnabledNode = xmlnode_get_child(contactEmailNode,"isMessengerEnabled")) != 0) {
              msnEnabled = xmlnode_get_data(messengerEnabledNode);
              if ((msnEnabled != 0) && !(strcmp(msnEnabled,"true") != 0)) {
                if ((emailNode = xmlnode_get_child(contactEmailNode,"email")) != 0) 
                  passport = xmlnode_get_data(emailNode);
/* Messenger enabled, Get the Passport*/
                purple_debug_info("msn","AB Yahoo/Federated User %s\n",((passport != 0)?passport : "(null)"));
                g_free(msnEnabled);
                break; 
              }
              g_free(msnEnabled);
            }
          }
        }
      }
      else {
        xmlnode *messenger_user;
/* ignore non-messenger contacts */
        if ((messenger_user = xmlnode_get_child(contactInfo,"isMessengerUser")) != 0) {
          char *is_messenger_user = xmlnode_get_data(messenger_user);
          if ((is_messenger_user != 0) && !(strcmp(is_messenger_user,"false") != 0)) {
            g_free(is_messenger_user);
            continue; 
          }
          g_free(is_messenger_user);
        }
        passport = xmlnode_get_data(passportName);
      }
/* Couldn't find anything */
      if (passport == ((char *)((void *)0))) 
        continue; 
      if (!(msn_email_is_valid(passport) != 0)) 
        continue; 
      if ((displayName = xmlnode_get_child(contactInfo,"displayName")) != 0) 
        Name = xmlnode_get_data(displayName);
      else 
        Name = g_strdup(passport);
      for (annotation = xmlnode_get_child(contactInfo,"annotations/Annotation"); annotation != 0; annotation = xmlnode_get_next_twin(annotation)) {{
          char *name;
          name = xmlnode_get_data((xmlnode_get_child(annotation,"Name")));
          if (!(name != 0)) 
            continue; 
          if (!(strcmp(name,"AB.NickName") != 0)) 
            alias = xmlnode_get_data((xmlnode_get_child(annotation,"Value")));
          else if (!(strcmp(name,"MSN.IM.HasSharedFolder") != 0)) 
/* Do nothing yet... */
;
          else if (!(strcmp(name,"AB.Spouse") != 0)) 
/* Do nothing yet... */
;
          else if (!(strcmp(name,"MSN.Mobile.ContactId") != 0)) 
/* Do nothing yet... */
;
          else 
            purple_debug_info("msn","Unknown AB contact annotation: %s\n",name);
          g_free(name);
        }
      }
      mobile = msn_parse_addressbook_mobile(contactInfo,&mobile_number);
      purple_debug_misc("msn","AB passport:{%s} uid:{%s} display:{%s} alias: {%s} mobile:{%s} mobile number:{%s}\n",passport,((uid != 0)?uid : "(null)"),((Name != 0)?Name : "(null)"),((alias != 0)?alias : "(null)"),((mobile != 0)?"true" : "false"),((mobile_number != 0)?mobile_number : "(null)"));
      user = msn_userlist_find_add_user((session -> userlist),passport,Name);
      msn_user_set_uid(user,uid);
      msn_user_set_mobile_phone(user,mobile_number);
      groupIds = xmlnode_get_child(contactInfo,"groupIds");
      if (groupIds != 0) {
        for (guid = xmlnode_get_child(groupIds,"guid"); guid != 0; guid = xmlnode_get_next_twin(guid)) {
          char *group_id = xmlnode_get_data(guid);
          msn_user_add_group_id(user,group_id);
          purple_debug_misc("msn","AB guid:%s\n",((group_id != 0)?group_id : "(null)"));
          g_free(group_id);
        }
      }
      else {
        purple_debug_info("msn","User not in any groups, adding to default group.\n");
/*not in any group,Then set default group*/
        msn_user_add_group_id(user,"1983");
      }
      msn_got_lst_user(session,user,MSN_LIST_FL_OP,0);
      if ((mobile != 0) && (user != 0)) {
        user -> mobile = (!0);
        purple_prpl_got_user_status((session -> account),(user -> passport),"mobile",((void *)((void *)0)));
        purple_prpl_got_user_status((session -> account),(user -> passport),"available",((void *)((void *)0)));
      }
      if (alias != 0) 
        purple_serv_got_private_alias(pc,passport,alias);
    }
  }
  g_free(passport);
  g_free(Name);
  g_free(uid);
  g_free(type);
  g_free(mobile_number);
  g_free(alias);
}

static void msn_parse_addressbook_circles(MsnSession *session,xmlnode *node)
{
  xmlnode *ticket;
/* TODO: Parse groups */
  ticket = xmlnode_get_child(node,"CircleTicket");
  if (ticket != 0) {
    char *data = xmlnode_get_data(ticket);
    msn_notification_send_circle_auth(session,data);
    g_free(data);
  }
}

static gboolean msn_parse_addressbook(MsnSession *session,xmlnode *node)
{
  xmlnode *result;
  xmlnode *groups;
  xmlnode *contacts;
  xmlnode *abNode;
  xmlnode *circleNode;
  xmlnode *fault;
  if ((fault = xmlnode_get_child(node,"Body/Fault")) != 0) {
    xmlnode *faultnode;
    if ((faultnode = xmlnode_get_child(fault,"faultstring")) != 0) {
      gchar *faultstring = xmlnode_get_data(faultnode);
      purple_debug_info("msn","AB Faultstring: %s\n",faultstring);
      g_free(faultstring);
    }
    if ((faultnode = xmlnode_get_child(fault,"detail/errorcode")) != 0) {
      gchar *errorcode = xmlnode_get_data(faultnode);
      purple_debug_info("msn","AB Error Code: %s\n",errorcode);
      if (g_str_equal(errorcode,"ABDoesNotExist") != 0) {
        g_free(errorcode);
        return (!0);
      }
      g_free(errorcode);
    }
    return 0;
  }
  result = xmlnode_get_child(node,"Body/ABFindContactsPagedResponse/ABFindContactsPagedResult");
  if (result == ((xmlnode *)((void *)0))) {
    purple_debug_misc("msn","Received no address book update\n");
    return (!0);
  }
/* I don't see this "groups" tag documented on msnpiki, need to find out
	   if they are really there, and update msnpiki */
/*Process Group List*/
  groups = xmlnode_get_child(result,"Groups");
  if (groups != ((xmlnode *)((void *)0))) {
    msn_parse_addressbook_groups(session,groups);
  }
/* Add an "Other Contacts" group for buddies who aren't in a group */
  msn_group_new((session -> userlist),"1983",((const char *)(dgettext("pidgin","Other Contacts"))));
  purple_debug_misc("msn","AB group_id:%s name:%s\n","1983",((const char *)(dgettext("pidgin","Other Contacts"))));
  if (purple_find_group(((const char *)(dgettext("pidgin","Other Contacts")))) == ((PurpleGroup *)((void *)0))) {
    PurpleGroup *g = purple_group_new(((const char *)(dgettext("pidgin","Other Contacts"))));
    purple_blist_add_group(g,0);
  }
/* Add a "Non-IM Contacts" group */
  msn_group_new((session -> userlist),"email",((const char *)(dgettext("pidgin","Non-IM Contacts"))));
  purple_debug_misc("msn","AB group_id:%s name:%s\n","email",((const char *)(dgettext("pidgin","Non-IM Contacts"))));
  if (purple_find_group(((const char *)(dgettext("pidgin","Non-IM Contacts")))) == ((PurpleGroup *)((void *)0))) {
    PurpleGroup *g = purple_group_new(((const char *)(dgettext("pidgin","Non-IM Contacts"))));
    purple_blist_add_group(g,0);
  }
/*Process contact List*/
  purple_debug_info("msn","Process contact list...\n");
  contacts = xmlnode_get_child(result,"Contacts");
  if (contacts != ((xmlnode *)((void *)0))) {
    msn_parse_addressbook_contacts(session,contacts);
  }
  abNode = xmlnode_get_child(result,"Ab");
  if (abNode != ((xmlnode *)((void *)0))) {
    xmlnode *node2;
    char *tmp = (char *)((void *)0);
    if ((node2 = xmlnode_get_child(abNode,"lastChange")) != 0) 
      tmp = xmlnode_get_data(node2);
    purple_debug_info("msn","AB lastchanged Time:{%s}\n",((tmp != 0)?tmp : "(null)"));
    purple_account_set_string((session -> account),"ablastChange",tmp);
    g_free(tmp);
    tmp = ((char *)((void *)0));
    if ((node2 = xmlnode_get_child(abNode,"DynamicItemLastChanged")) != 0) 
      tmp = xmlnode_get_data(node2);
    purple_debug_info("msn","AB DynamicItemLastChanged :{%s}\n",((tmp != 0)?tmp : "(null)"));
    purple_account_set_string((session -> account),"DynamicItemLastChanged",tmp);
    g_free(tmp);
  }
  circleNode = xmlnode_get_child(result,"CircleResult");
  if (circleNode != ((xmlnode *)((void *)0))) {
    msn_parse_addressbook_circles(session,circleNode);
  }
  return (!0);
}

static void msn_get_address_cb(MsnSoapMessage *req,MsnSoapMessage *resp,gpointer data)
{
  MsnCallbackState *state = data;
  MsnSession *session = (state -> session);
  do {
    if (session != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return ;
    };
  }while (0);
  purple_debug_misc("msn","Got the Address Book!\n");
  if (msn_parse_addressbook(session,(resp -> xml)) != 0) {
    msn_send_privacy(( *(session -> account)).gc);
    msn_notification_dump_contact(session);
  }
  else {
/* This is making us loop infinitely when we fail to parse the
		  address book, disable for now (we should re-enable when we
		  send timestamps)
		*/
/*
		msn_get_address_book(session, NULL, NULL);
		*/
    msn_session_set_error(session,MSN_ERROR_BAD_BLIST,0);
  }
}
/*get the address book*/

void msn_get_address_book(MsnSession *session,MsnSoapPartnerScenario partner_scenario,const char *LastChanged,const char *dynamicItemLastChange)
{
  char *body;
  char *update_str = (char *)((void *)0);
  MsnCallbackState *state;
  purple_debug_misc("msn","Getting Address Book\n");
/*build SOAP and POST it*/
  if (dynamicItemLastChange != ((const char *)((void *)0))) 
    update_str = g_strdup_printf("<filterOptions><deltasOnly>true</deltasOnly><lastChange>%s</lastChange></filterOptions>",dynamicItemLastChange);
  else if (LastChanged != ((const char *)((void *)0))) 
    update_str = g_strdup_printf("<filterOptions><deltasOnly>true</deltasOnly><lastChange>%s</lastChange></filterOptions>",LastChanged);
  body = g_strdup_printf("<\?xml version=\"1.0\" encoding=\"utf-8\"\?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\"><soap:Header><ABApplicationHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ApplicationId>CFE80F9D-180F-4399-82AB-413F33A1FA11</ApplicationId><IsMigration>false</IsMigration><PartnerScenario>%s</PartnerScenario></ABApplicationHeader><ABAuthHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ManagedGroupRequest>false</ManagedGroupRequest><TicketToken>EMPTY</TicketToken></ABAuthHeader></soap:Header><soap:Body><ABFindContactsPaged xmlns=\"http://www.msn.com/webservices/AddressBook\"><abView>Full</abView><extendedContent>AB AllGroups CircleResult</extendedContent>%s</ABFindContactsPaged></soap:Body></soap:Envelope>",MsnSoapPartnerScenarioText[partner_scenario],((update_str != 0)?update_str : ""));
  state = msn_callback_state_new(session);
  state -> body = xmlnode_from_str(body,(-1));
  state -> post_action = "http://www.msn.com/webservices/AddressBook/ABFindContactsPaged";
  state -> post_url = "/abservice/abservice.asmx";
  state -> cb = msn_get_address_cb;
  msn_contact_request(state);
  g_free(update_str);
  g_free(body);
}
/***************************************************************
 * Contact Operations
 ***************************************************************/

static void msn_add_contact_read_cb(MsnSoapMessage *req,MsnSoapMessage *resp,gpointer data)
{
  MsnCallbackState *state = data;
  MsnSession *session = (state -> session);
  MsnUserList *userlist;
  MsnUser *user;
  xmlnode *guid;
  xmlnode *fault;
  do {
    if (session != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return ;
    };
  }while (0);
  userlist = (session -> userlist);
  fault = xmlnode_get_child((resp -> xml),"Body/Fault");
  if (fault != ((xmlnode *)((void *)0))) {
    char *errorcode = xmlnode_get_data((xmlnode_get_child(fault,"detail/errorcode")));
    if ((errorcode != 0) && !(strcmp(errorcode,"EmailDomainIsFederated") != 0)) {
/* Do something special! */
      purple_debug_error("msn","Contact is from a federated domain, but don\'t know what to do yet!\n");
    }
    else if ((errorcode != 0) && !(strcmp(errorcode,"InvalidPassportUser") != 0)) {
      PurpleBuddy *buddy = purple_find_buddy((session -> account),(state -> who));
      char *str = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to add \"%s\"."))),(state -> who));
      purple_notify_message((state -> session),PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Buddy Add error"))),str,((const char *)(dgettext("pidgin","The username specified does not exist."))),0,0);
      g_free(str);
      msn_userlist_rem_buddy(userlist,(state -> who));
      if (buddy != ((PurpleBuddy *)((void *)0))) 
        purple_blist_remove_buddy(buddy);
    }
    else {
/* We don't know how to respond to this faultcode, so log it */
      char *fault_str = xmlnode_to_str(fault,0);
      if (fault_str != ((char *)((void *)0))) {
        purple_debug_error("msn","Operation {%s} Failed, SOAP Fault was: %s\n",msn_contact_operation_str((state -> action)),fault_str);
        g_free(fault_str);
      }
    }
    return ;
  }
  purple_debug_info("msn","Contact added successfully\n");
  msn_userlist_add_buddy_to_list(userlist,(state -> who),MSN_LIST_AL);
  msn_userlist_add_buddy_to_list(userlist,(state -> who),MSN_LIST_FL);
  user = msn_userlist_find_add_user(userlist,(state -> who),(state -> who));
  msn_user_add_group_id(user,(state -> guid));
  guid = xmlnode_get_child((resp -> xml),"Body/ABContactAddResponse/ABContactAddResult/guid");
  if (guid != ((xmlnode *)((void *)0))) {
    char *uid = xmlnode_get_data(guid);
    msn_user_set_uid(user,uid);
    purple_debug_info("msn","Set %s guid to %s.\n",(state -> who),uid);
    g_free(uid);
  }
}
/* add a Contact in MSN_INDIVIDUALS_GROUP */

void msn_add_contact(MsnSession *session,MsnCallbackState *state,const char *passport)
{
  MsnUser *user;
  gchar *body = (gchar *)((void *)0);
  gchar *contact_xml = (gchar *)((void *)0);
  purple_debug_info("msn","Adding contact %s to contact list\n",passport);
  user = msn_userlist_find_user((session -> userlist),passport);
  if (user == ((MsnUser *)((void *)0))) {
    purple_debug_warning("msn","Unable to retrieve user %s from the userlist!\n",passport);
/* guess this never happened! */
    return ;
  }
  if ((user -> networkid) != MSN_NETWORK_PASSPORT) {
    contact_xml = g_strdup_printf("<Contact><contactInfo><emails><ContactEmail><contactEmailType>%s</contactEmailType><email>%s</email><isMessengerEnabled>true</isMessengerEnabled><Capability>%d</Capability><MessengerEnabledExternally>false</MessengerEnabledExternally><propertiesChanged/></ContactEmail></emails></contactInfo></Contact>",(((user -> networkid) == MSN_NETWORK_YAHOO)?"Messenger2" : "Messenger3"),passport,0);
  }
  else {
    contact_xml = g_strdup_printf("<Contact xmlns=\"http://www.msn.com/webservices/AddressBook\"><contactInfo><passportName>%s</passportName><isSmtp>false</isSmtp><isMessengerUser>true</isMessengerUser></contactInfo></Contact>",passport);
  }
  body = g_strdup_printf("<\?xml version=\"1.0\" encoding=\"utf-8\"\?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\"><soap:Header><ABApplicationHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ApplicationId>CFE80F9D-180F-4399-82AB-413F33A1FA11</ApplicationId><IsMigration>false</IsMigration><PartnerScenario>ContactSave</PartnerScenario></ABApplicationHeader><ABAuthHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ManagedGroupRequest>false</ManagedGroupRequest><TicketToken>EMPTY</TicketToken></ABAuthHeader></soap:Header><soap:Body><ABContactAdd xmlns=\"http://www.msn.com/webservices/AddressBook\"><abId>00000000-0000-0000-0000-000000000000</abId><contacts>%s</contacts><options><EnableAllowListManagement>true</EnableAllowListManagement></options></ABContactAdd></soap:Body></soap:Envelope>",contact_xml);
  state -> body = xmlnode_from_str(body,(-1));
  state -> post_action = "http://www.msn.com/webservices/AddressBook/ABContactAdd";
  state -> post_url = "/abservice/abservice.asmx";
  state -> cb = msn_add_contact_read_cb;
  msn_contact_request(state);
  g_free(contact_xml);
  g_free(body);
}

static void msn_add_contact_to_group_read_cb(MsnSoapMessage *req,MsnSoapMessage *resp,gpointer data)
{
  MsnCallbackState *state = data;
  MsnSession *session = (state -> session);
  MsnUserList *userlist;
  xmlnode *fault;
  do {
    if (session != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return ;
    };
  }while (0);
  userlist = (session -> userlist);
  fault = xmlnode_get_child((resp -> xml),"Body/Fault");
  if (fault != ((xmlnode *)((void *)0))) {
    char *errorcode = xmlnode_get_data((xmlnode_get_child(fault,"detail/errorcode")));
    if ((errorcode != 0) && !(strcmp(errorcode,"EmailDomainIsFederated") != 0)) {
/* Do something special! */
      purple_debug_error("msn","Contact is from a federated domain, but don\'t know what to do yet!\n");
    }
    else if ((errorcode != 0) && !(strcmp(errorcode,"InvalidPassportUser") != 0)) {
      PurpleBuddy *buddy = purple_find_buddy((session -> account),(state -> who));
      char *str = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to add \"%s\"."))),(state -> who));
      purple_notify_message(session,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Buddy Add error"))),str,((const char *)(dgettext("pidgin","The username specified does not exist."))),0,0);
      g_free(str);
      msn_userlist_rem_buddy(userlist,(state -> who));
      if (buddy != ((PurpleBuddy *)((void *)0))) 
        purple_blist_remove_buddy(buddy);
    }
    else {
/* We don't know how to respond to this faultcode, so log it */
      char *fault_str = xmlnode_to_str(fault,0);
      if (fault_str != ((char *)((void *)0))) {
        purple_debug_error("msn","Operation {%s} Failed, SOAP Fault was: %s\n",msn_contact_operation_str((state -> action)),fault_str);
        g_free(fault_str);
      }
    }
    return ;
  }
  if (msn_userlist_add_buddy_to_group(userlist,(state -> who),(state -> new_group_name)) != 0) {
    purple_debug_info("msn","Contact %s added to group %s successfully!\n",(state -> who),(state -> new_group_name));
  }
  else {
    purple_debug_info("msn","Contact %s added to group %s successfully on server, but failed in the local list\n",(state -> who),(state -> new_group_name));
  }
  if (((state -> action) & MSN_ADD_BUDDY) != 0U) {
    MsnUser *user = msn_userlist_find_user(userlist,(state -> who));
    xmlnode *guid = xmlnode_get_child((resp -> xml),"Body/ABGroupContactAddResponse/ABGroupContactAddResult/guid");
    if (guid != ((xmlnode *)((void *)0))) {
      char *uid = xmlnode_get_data(guid);
      msn_user_set_uid(user,uid);
      purple_debug_info("msn","Set %s guid to %s.\n",(state -> who),uid);
      g_free(uid);
    }
    msn_userlist_add_buddy_to_list(userlist,(state -> who),MSN_LIST_AL);
    msn_userlist_add_buddy_to_list(userlist,(state -> who),MSN_LIST_FL);
    if (msn_user_is_in_list(user,MSN_LIST_PL) != 0) {
      msn_del_contact_from_list((state -> session),0,(state -> who),MSN_LIST_PL);
      return ;
    }
  }
  if (((state -> action) & MSN_MOVE_BUDDY) != 0U) {
    msn_del_contact_from_group((state -> session),(state -> who),(state -> old_group_name));
  }
}

void msn_add_contact_to_group(MsnSession *session,MsnCallbackState *state,const char *passport,const char *groupId)
{
  MsnUserList *userlist;
  MsnUser *user;
  gchar *body = (gchar *)((void *)0);
  gchar *contact_xml;
  gchar *invite;
  do {
    if (passport != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"passport != NULL");
      return ;
    };
  }while (0);
  do {
    if (groupId != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"groupId != NULL");
      return ;
    };
  }while (0);
  do {
    if (session != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return ;
    };
  }while (0);
  userlist = (session -> userlist);
  if (!(strcmp(groupId,"1983") != 0) || !(strcmp(groupId,"email") != 0)) {
    user = msn_userlist_find_add_user(userlist,passport,passport);
    if (((state -> action) & MSN_ADD_BUDDY) != 0U) {
      msn_add_contact(session,state,passport);
      return ;
    }
    if (((state -> action) & MSN_MOVE_BUDDY) != 0U) {
      msn_user_add_group_id(user,groupId);
      msn_del_contact_from_group(session,passport,(state -> old_group_name));
    }
    return ;
  }
  purple_debug_info("msn","Adding user %s to group %s\n",passport,msn_userlist_find_group_name(userlist,groupId));
  user = msn_userlist_find_user(userlist,passport);
  if (user == ((MsnUser *)((void *)0))) {
    purple_debug_warning("msn","Unable to retrieve user %s from the userlist!\n",passport);
    msn_callback_state_free(state);
/* guess this never happened! */
    return ;
  }
  if ((user -> uid) != ((char *)((void *)0))) {
    contact_xml = g_strdup_printf("<Contact><contactId>%s</contactId></Contact>",(user -> uid));
  }
  else if ((user -> networkid) != MSN_NETWORK_PASSPORT) {
    contact_xml = g_strdup_printf("<Contact><contactInfo><emails><ContactEmail><contactEmailType>%s</contactEmailType><email>%s</email><isMessengerEnabled>true</isMessengerEnabled><Capability>%d</Capability><MessengerEnabledExternally>false</MessengerEnabledExternally><propertiesChanged/></ContactEmail></emails></contactInfo></Contact>",(((user -> networkid) == MSN_NETWORK_YAHOO)?"Messenger2" : "Messenger3"),passport,0);
  }
  else {
    contact_xml = g_strdup_printf("<Contact xmlns=\"http://www.msn.com/webservices/AddressBook\"><contactInfo><passportName>%s</passportName><isSmtp>false</isSmtp><isMessengerUser>true</isMessengerUser></contactInfo></Contact>",passport);
  }
  if ((user -> invite_message) != 0) {
    char *tmp;
    body = g_markup_escape_text((user -> invite_message),(-1));
/* Ignore the cast, we treat it as const anyway. */
    tmp = ((char *)(purple_connection_get_display_name(( *(session -> account)).gc)));
    tmp = ((tmp != 0)?g_markup_escape_text(tmp,(-1)) : g_strdup(""));
    invite = g_strdup_printf("<MessengerMemberInfo><PendingAnnotations><Annotation><Name>MSN.IM.InviteMessage</Name><Value>%s</Value></Annotation></PendingAnnotations><DisplayName>%s</DisplayName></MessengerMemberInfo>",body,tmp);
    g_free(body);
    g_free(tmp);
/* We can free this now */
    g_free((user -> invite_message));
    user -> invite_message = ((char *)((void *)0));
  }
  else {
    invite = g_strdup("");
  }
  body = g_strdup_printf("<\?xml version=\"1.0\" encoding=\"utf-8\"\?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\"><soap:Header><ABApplicationHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ApplicationId>CFE80F9D-180F-4399-82AB-413F33A1FA11</ApplicationId><IsMigration>false</IsMigration><PartnerScenario>ContactSave</PartnerScenario></ABApplicationHeader><ABAuthHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ManagedGroupRequest>false</ManagedGroupRequest><TicketToken>EMPTY</TicketToken></ABAuthHeader></soap:Header><soap:Body><ABGroupContactAdd xmlns=\"http://www.msn.com/webservices/AddressBook\"><abId>00000000-0000-0000-0000-000000000000</abId><groupFilter><groupIds><guid>%s</guid></groupIds></groupFilter><contacts>%s</contacts><groupContactAddOptions><fGenerateMissingQuickName>true</fGenerateMissingQuickName><EnableAllowListManagement>true</EnableAllowListManagement></groupContactAddOptions>%s</ABGroupContactAdd></soap:Body></soap:Envelope>",groupId,contact_xml,invite);
  state -> body = xmlnode_from_str(body,(-1));
  state -> post_action = "http://www.msn.com/webservices/AddressBook/ABGroupContactAdd";
  state -> post_url = "/abservice/abservice.asmx";
  state -> cb = msn_add_contact_to_group_read_cb;
  msn_contact_request(state);
  g_free(invite);
  g_free(contact_xml);
  g_free(body);
}

static void msn_delete_contact_read_cb(MsnSoapMessage *req,MsnSoapMessage *resp,gpointer data)
{
  MsnCallbackState *state = data;
  MsnUserList *userlist = ( *(state -> session)).userlist;
  MsnUser *user = msn_userlist_find_user_with_id(userlist,(state -> uid));
  xmlnode *fault;
/* We don't know how to respond to this faultcode, so log it */
  fault = xmlnode_get_child((resp -> xml),"Body/Fault");
  if (fault != ((xmlnode *)((void *)0))) {
    char *fault_str = xmlnode_to_str(fault,0);
    purple_debug_error("msn","Operation {%s} Failed, SOAP Fault was: %s\n",msn_contact_operation_str((state -> action)),fault_str);
    g_free(fault_str);
    return ;
  }
  purple_debug_info("msn","Delete contact successful\n");
  if (user != ((MsnUser *)((void *)0))) {
    msn_userlist_remove_user(userlist,user);
  }
}
/*delete a Contact*/

void msn_delete_contact(MsnSession *session,MsnUser *user)
{
  gchar *body = (gchar *)((void *)0);
  gchar *contact_id_xml = (gchar *)((void *)0);
  MsnCallbackState *state;
  if ((user -> uid) != ((char *)((void *)0))) {
    contact_id_xml = g_strdup_printf("<Contact><contactId>%s</contactId></Contact>",(user -> uid));
    purple_debug_info("msn","Deleting contact with contactId: %s\n",(user -> uid));
  }
  else {
    purple_debug_info("msn","Unable to delete contact %s without a ContactId\n",(user -> passport));
    return ;
  }
  state = msn_callback_state_new(session);
  msn_callback_state_set_uid(state,(user -> uid));
/* build SOAP request */
  body = g_strdup_printf("<\?xml version=\"1.0\" encoding=\"utf-8\"\?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\"><soap:Header><ABApplicationHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ApplicationId>CFE80F9D-180F-4399-82AB-413F33A1FA11</ApplicationId><IsMigration>false</IsMigration><PartnerScenario>Timer</PartnerScenario></ABApplicationHeader><ABAuthHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ManagedGroupRequest>false</ManagedGroupRequest><TicketToken>EMPTY</TicketToken></ABAuthHeader></soap:Header><soap:Body><ABContactDelete xmlns=\"http://www.msn.com/webservices/AddressBook\"><abId>00000000-0000-0000-0000-000000000000</abId><contacts>%s</contacts></ABContactDelete></soap:Body></soap:Envelope>",contact_id_xml);
  state -> body = xmlnode_from_str(body,(-1));
  state -> post_action = "http://www.msn.com/webservices/AddressBook/ABContactDelete";
  state -> post_url = "/abservice/abservice.asmx";
  state -> cb = msn_delete_contact_read_cb;
  msn_contact_request(state);
  g_free(contact_id_xml);
  g_free(body);
}

static void msn_del_contact_from_group_read_cb(MsnSoapMessage *req,MsnSoapMessage *resp,gpointer data)
{
  MsnCallbackState *state = data;
  xmlnode *fault;
/* We don't know how to respond to this faultcode, so log it */
  fault = xmlnode_get_child((resp -> xml),"Body/Fault");
  if (fault != ((xmlnode *)((void *)0))) {
    char *fault_str = xmlnode_to_str(fault,0);
    purple_debug_error("msn","Operation {%s} Failed, SOAP Fault was: %s\n",msn_contact_operation_str((state -> action)),fault_str);
    g_free(fault_str);
    return ;
  }
  if (msn_userlist_rem_buddy_from_group(( *(state -> session)).userlist,(state -> who),(state -> old_group_name)) != 0) {
    purple_debug_info("msn","Contact %s deleted successfully from group %s\n",(state -> who),(state -> old_group_name));
  }
  else {
    purple_debug_info("msn","Contact %s deleted successfully from group %s in the server, but failed in the local list\n",(state -> who),(state -> old_group_name));
  }
}

void msn_del_contact_from_group(MsnSession *session,const char *passport,const char *group_name)
{
  MsnUserList *userlist;
  MsnUser *user;
  MsnCallbackState *state;
  gchar *body;
  gchar *contact_id_xml;
  const gchar *groupId;
  do {
    if (passport != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"passport != NULL");
      return ;
    };
  }while (0);
  do {
    if (group_name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"group_name != NULL");
      return ;
    };
  }while (0);
  do {
    if (session != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return ;
    };
  }while (0);
  userlist = (session -> userlist);
  groupId = msn_userlist_find_group_id(userlist,group_name);
  if (groupId != ((const gchar *)((void *)0))) {
    purple_debug_info("msn","Deleting user %s from group %s\n",passport,group_name);
  }
  else {
    purple_debug_warning("msn","Unable to retrieve group id from group %s !\n",group_name);
    return ;
  }
  user = msn_userlist_find_user(userlist,passport);
  if (user == ((MsnUser *)((void *)0))) {
    purple_debug_warning("msn","Unable to retrieve user from passport %s!\n",passport);
    return ;
  }
  if (!(strcmp(groupId,"1983") != 0) || !(strcmp(groupId,"email") != 0)) {
    msn_user_remove_group_id(user,groupId);
    return ;
  }
  state = msn_callback_state_new(session);
  msn_callback_state_set_who(state,passport);
  msn_callback_state_set_guid(state,groupId);
  msn_callback_state_set_old_group_name(state,group_name);
  if ((user -> uid) != ((char *)((void *)0))) 
    contact_id_xml = g_strdup_printf("<Contact><contactId>%s</contactId></Contact>",(user -> uid));
  else 
    contact_id_xml = g_strdup_printf("<Contact xmlns=\"http://www.msn.com/webservices/AddressBook\"><contactInfo><passportName>%s</passportName><isSmtp>false</isSmtp><isMessengerUser>true</isMessengerUser></contactInfo></Contact>",passport);
  body = g_strdup_printf("<\?xml version=\"1.0\" encoding=\"utf-8\"\?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\"><soap:Header><ABApplicationHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ApplicationId>CFE80F9D-180F-4399-82AB-413F33A1FA11</ApplicationId><IsMigration>false</IsMigration><PartnerScenario>Timer</PartnerScenario></ABApplicationHeader><ABAuthHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ManagedGroupRequest>false</ManagedGroupRequest><TicketToken>EMPTY</TicketToken></ABAuthHeader></soap:Header><soap:Body><ABGroupContactDelete xmlns=\"http://www.msn.com/webservices/AddressBook\"><abId>00000000-0000-0000-0000-000000000000</abId><contacts>%s</contacts><groupFilter><groupIds><guid>%s</guid></groupIds></groupFilter></ABGroupContactDelete></soap:Body></soap:Envelope>",contact_id_xml,groupId);
  state -> body = xmlnode_from_str(body,(-1));
  state -> post_action = "http://www.msn.com/webservices/AddressBook/ABGroupContactDelete";
  state -> post_url = "/abservice/abservice.asmx";
  state -> cb = msn_del_contact_from_group_read_cb;
  msn_contact_request(state);
  g_free(contact_id_xml);
  g_free(body);
}

static void msn_update_contact_read_cb(MsnSoapMessage *req,MsnSoapMessage *resp,gpointer data)
{
  MsnCallbackState *state = (MsnCallbackState *)data;
  xmlnode *fault;
/* We don't know how to respond to this faultcode, so log it */
  fault = xmlnode_get_child((resp -> xml),"Body/Fault");
  if (fault != ((xmlnode *)((void *)0))) {
    char *fault_str = xmlnode_to_str(fault,0);
    purple_debug_error("msn","Operation {%s} Failed, SOAP Fault was: %s\n",msn_contact_operation_str((state -> action)),fault_str);
    g_free(fault_str);
    return ;
  }
  purple_debug_info("msn","Contact updated successfully\n");
}
/* Update a contact's info */

void msn_update_contact(MsnSession *session,const char *passport,MsnContactUpdateType type,const char *value)
{
  MsnCallbackState *state;
  xmlnode *contact;
  xmlnode *contact_info;
  xmlnode *changes;
  MsnUser *user = (MsnUser *)((void *)0);
  purple_debug_info("msn","Update contact information for %s with new %s: %s\n",((passport != 0)?passport : "(null)"),((type == MSN_UPDATE_DISPLAY)?"display name" : "alias"),((value != 0)?value : "(null)"));
  do {
    if (passport != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"passport != NULL");
      return ;
    };
  }while (0);
  if (strcmp(passport,"Me") != 0) {
    user = msn_userlist_find_user((session -> userlist),passport);
    if (!(user != 0)) 
      return ;
  }
  contact_info = xmlnode_new("contactInfo");
  changes = xmlnode_new("propertiesChanged");
  switch(type){
    xmlnode *annotations;
    xmlnode *display;
    xmlnode *a;
    xmlnode *n;
    xmlnode *v;
    case MSN_UPDATE_DISPLAY:
{
      display = xmlnode_new_child(contact_info,"displayName");
      xmlnode_insert_data(display,value,(-1));
      xmlnode_insert_data(changes,"DisplayName",(-1));
      break; 
    }
    case MSN_UPDATE_ALIAS:
{
      annotations = xmlnode_new_child(contact_info,"annotations");
      xmlnode_insert_data(changes,"Annotation ",(-1));
      a = xmlnode_new_child(annotations,"Annotation");
      n = xmlnode_new_child(a,"Name");
      xmlnode_insert_data(n,"AB.NickName",(-1));
      v = xmlnode_new_child(a,"Value");
      xmlnode_insert_data(v,value,(-1));
      break; 
    }
    default:
{
      do {
        g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","contact.c",1514,((const char *)__func__));
        return ;
      }while (0);
    }
  }
  state = msn_callback_state_new(session);
  state -> body = xmlnode_from_str("<\?xml version=\"1.0\" encoding=\"utf-8\"\?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\"><soap:Header><ABApplicationHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ApplicationId>CFE80F9D-180F-4399-82AB-413F33A1FA11</ApplicationId><IsMigration>false</IsMigration><PartnerScenario></PartnerScenario></ABApplicationHeader><ABAuthHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ManagedGroupRequest>false</ManagedGroupRequest><TicketToken>EMPTY</TicketToken></ABAuthHeader></soap:Header><soap:Body><ABContactUpdate xmlns=\"http://www.msn.com/webservices/AddressBook\"><abId>00000000-0000-0000-0000-000000000000</abId><contacts><Contact xmlns=\"http://www.msn.com/webservices/AddressBook\"></Contact></contacts></ABContactUpdate></soap:Body></soap:Envelope>",(-1));
  state -> action = MSN_UPDATE_INFO;
  state -> post_action = "http://www.msn.com/webservices/AddressBook/ABContactUpdate";
  state -> post_url = "/abservice/abservice.asmx";
  state -> cb = msn_update_contact_read_cb;
  contact = xmlnode_get_child((state -> body),"Body/ABContactUpdate/contacts/Contact");
  xmlnode_insert_child(contact,contact_info);
  xmlnode_insert_child(contact,changes);
  xmlnode_insert_data(xmlnode_get_child((state -> body),"Header/ABApplicationHeader/PartnerScenario"),MsnSoapPartnerScenarioText[MSN_PS_SAVE_CONTACT],(-1));
  if (user != 0) {
    xmlnode *contactId = xmlnode_new_child(contact,"contactId");
    msn_callback_state_set_uid(state,(user -> uid));
    xmlnode_insert_data(contactId,(state -> uid),(-1));
  }
  else {
    xmlnode *contactType = xmlnode_new_child(contact_info,"contactType");
    xmlnode_insert_data(contactType,"Me",(-1));
  }
  msn_contact_request(state);
}

static void msn_annotate_contact_read_cb(MsnSoapMessage *req,MsnSoapMessage *resp,gpointer data)
{
  MsnCallbackState *state = (MsnCallbackState *)data;
  xmlnode *fault;
/* We don't know how to respond to this faultcode, so log it */
  fault = xmlnode_get_child((resp -> xml),"Body/Fault");
  if (fault != ((xmlnode *)((void *)0))) {
    char *fault_str = xmlnode_to_str(fault,0);
    purple_debug_error("msn","Operation {%s} Failed, SOAP Fault was: %s\n",msn_contact_operation_str((state -> action)),fault_str);
    g_free(fault_str);
    return ;
  }
  purple_debug_info("msn","Contact annotated successfully\n");
}
/* Update a contact's annotations */

void msn_annotate_contact(MsnSession *session,const char *passport,... )
{
  va_list params;
  MsnCallbackState *state;
  xmlnode *contact;
  xmlnode *contact_info;
  xmlnode *annotations;
  MsnUser *user = (MsnUser *)((void *)0);
  do {
    if (passport != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"passport != NULL");
      return ;
    };
  }while (0);
  if (strcmp(passport,"Me") != 0) {
    user = msn_userlist_find_user((session -> userlist),passport);
    if (!(user != 0)) 
      return ;
  }
  contact_info = xmlnode_new("contactInfo");
  annotations = xmlnode_new_child(contact_info,"annotations");
  va_start(params,passport);
{
    while(1){
      const char *name;
      const char *value;
      xmlnode *a;
      xmlnode *n;
      xmlnode *v;
      name = (va_arg(params,const char *));
      if (!(name != 0)) 
        break; 
      value = (va_arg(params,const char *));
      if (!(value != 0)) 
        break; 
      a = xmlnode_new_child(annotations,"Annotation");
      n = xmlnode_new_child(a,"Name");
      xmlnode_insert_data(n,name,(-1));
      v = xmlnode_new_child(a,"Value");
      xmlnode_insert_data(v,value,(-1));
    }
  }
  va_end(params);
  state = msn_callback_state_new(session);
  state -> body = xmlnode_from_str("<\?xml version=\"1.0\" encoding=\"utf-8\"\?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\"><soap:Header><ABApplicationHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ApplicationId>CFE80F9D-180F-4399-82AB-413F33A1FA11</ApplicationId><IsMigration>false</IsMigration><PartnerScenario></PartnerScenario></ABApplicationHeader><ABAuthHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ManagedGroupRequest>false</ManagedGroupRequest><TicketToken>EMPTY</TicketToken></ABAuthHeader></soap:Header><soap:Body><ABContactUpdate xmlns=\"http://www.msn.com/webservices/AddressBook\"><abId>00000000-0000-0000-0000-000000000000</abId><contacts><Contact xmlns=\"http://www.msn.com/webservices/AddressBook\"><propertiesChanged>Annotation</propertiesChanged></Contact></contacts></ABContactUpdate></soap:Body></soap:Envelope>",(-1));
  state -> action = MSN_ANNOTATE_USER;
  state -> post_action = "http://www.msn.com/webservices/AddressBook/ABContactUpdate";
  state -> post_url = "/abservice/abservice.asmx";
  state -> cb = msn_annotate_contact_read_cb;
  xmlnode_insert_data(xmlnode_get_child((state -> body),"Header/ABApplicationHeader/PartnerScenario"),MsnSoapPartnerScenarioText[MSN_PS_SAVE_CONTACT],(-1));
  contact = xmlnode_get_child((state -> body),"Body/ABContactUpdate/contacts/Contact");
  xmlnode_insert_child(contact,contact_info);
  if (user != 0) {
    xmlnode *contactId = xmlnode_new_child(contact,"contactId");
    msn_callback_state_set_uid(state,(user -> uid));
    xmlnode_insert_data(contactId,(state -> uid),(-1));
  }
  else {
    xmlnode *contactType = xmlnode_new_child(contact_info,"contactType");
    xmlnode_insert_data(contactType,"Me",(-1));
  }
  msn_contact_request(state);
}

static void msn_del_contact_from_list_read_cb(MsnSoapMessage *req,MsnSoapMessage *resp,gpointer data)
{
  MsnCallbackState *state = data;
  MsnSession *session = (state -> session);
  xmlnode *fault;
/* We don't know how to respond to this faultcode, so log it */
  fault = xmlnode_get_child((resp -> xml),"Body/Fault");
  if (fault != ((xmlnode *)((void *)0))) {
    char *fault_str = xmlnode_to_str(fault,0);
    purple_debug_error("msn","Operation {%s} Failed, SOAP Fault was: %s\n",msn_contact_operation_str((state -> action)),fault_str);
    g_free(fault_str);
    return ;
  }
  purple_debug_info("msn","Contact %s deleted successfully from %s list on server!\n",(state -> who),MsnMemberRole[state -> list_id]);
  if ((state -> list_id) == MSN_LIST_PL) {
    MsnUser *user = msn_userlist_find_user((session -> userlist),(state -> who));
    MsnCallbackState *new_state = msn_callback_state_dup(state);
    if (user != ((MsnUser *)((void *)0))) 
      msn_user_unset_op(user,MSN_LIST_PL_OP);
    msn_add_contact_to_list(session,new_state,(state -> who),MSN_LIST_RL);
    return ;
  }
  else if ((state -> list_id) == MSN_LIST_AL) {
    purple_privacy_permit_remove((session -> account),(state -> who),(!0));
    msn_add_contact_to_list(session,0,(state -> who),MSN_LIST_BL);
  }
  else if ((state -> list_id) == MSN_LIST_BL) {
    purple_privacy_deny_remove((session -> account),(state -> who),(!0));
    msn_add_contact_to_list(session,0,(state -> who),MSN_LIST_AL);
  }
}

void msn_del_contact_from_list(MsnSession *session,MsnCallbackState *state,const gchar *passport,const MsnListId list)
{
  gchar *body = (gchar *)((void *)0);
  gchar *member = (gchar *)((void *)0);
  MsnSoapPartnerScenario partner_scenario;
  MsnUser *user;
  do {
    if (session != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return ;
    };
  }while (0);
  do {
    if ((session -> userlist) != ((MsnUserList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session->userlist != NULL");
      return ;
    };
  }while (0);
  do {
    if (passport != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"passport != NULL");
      return ;
    };
  }while (0);
  do {
    if (list < 5) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"list < 5");
      return ;
    };
  }while (0);
  purple_debug_info("msn","Deleting contact %s from %s list\n",passport,MsnMemberRole[list]);
  if (state == ((MsnCallbackState *)((void *)0))) {
    state = msn_callback_state_new(session);
  }
  msn_callback_state_set_list_id(state,list);
  msn_callback_state_set_who(state,passport);
  user = msn_userlist_find_user((session -> userlist),passport);
  if (list == MSN_LIST_PL) {
    partner_scenario = MSN_PS_CONTACT_API;
    if ((user -> networkid) != MSN_NETWORK_PASSPORT) 
      member = g_strdup_printf("<Member xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"%s\"><Type>%s</Type><MembershipId>%u</MembershipId><State>Accepted</State></Member>","EmailMember","Email",(user -> member_id_on_pending_list));
    else 
      member = g_strdup_printf("<Member xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"%s\"><Type>%s</Type><MembershipId>%u</MembershipId><State>Accepted</State></Member>","PassportMember","Passport",(user -> member_id_on_pending_list));
  }
  else {
/* list == MSN_LIST_AL || list == MSN_LIST_BL */
    partner_scenario = MSN_PS_BLOCK_UNBLOCK;
    if ((user != 0) && ((user -> networkid) != MSN_NETWORK_PASSPORT)) 
      member = g_strdup_printf("<Member xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"%s\"><Type>%s</Type><State>Accepted</State><%s>%s</%s></Member>","EmailMember","Email","Email",passport,"Email");
    else 
      member = g_strdup_printf("<Member xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"%s\"><Type>%s</Type><State>Accepted</State><%s>%s</%s></Member>","PassportMember","Passport","PassportName",passport,"PassportName");
  }
  body = g_strdup_printf("<\?xml version=\"1.0\" encoding=\"utf-8\"\?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\"><soap:Header><ABApplicationHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ApplicationId>CFE80F9D-180F-4399-82AB-413F33A1FA11</ApplicationId><IsMigration>false</IsMigration><PartnerScenario>%s</PartnerScenario></ABApplicationHeader><ABAuthHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ManagedGroupRequest>false</ManagedGroupRequest><TicketToken>EMPTY</TicketToken></ABAuthHeader></soap:Header><soap:Body><DeleteMember xmlns=\"http://www.msn.com/webservices/AddressBook\"><serviceHandle><Id>0</Id><Type>Messenger</Type><ForeignId></ForeignId></serviceHandle><memberships><Membership><MemberRole>%s</MemberRole><Members>%s</Members></Membership></memberships></DeleteMember></soap:Body></soap:Envelope>",MsnSoapPartnerScenarioText[partner_scenario],MsnMemberRole[list],member);
  state -> body = xmlnode_from_str(body,(-1));
  state -> post_action = "http://www.msn.com/webservices/AddressBook/DeleteMember";
  state -> post_url = "/abservice/SharingService.asmx";
  state -> cb = msn_del_contact_from_list_read_cb;
  msn_contact_request(state);
  g_free(member);
  g_free(body);
}

static void msn_add_contact_to_list_read_cb(MsnSoapMessage *req,MsnSoapMessage *resp,gpointer data)
{
  MsnCallbackState *state = data;
  xmlnode *fault;
/* We don't know how to respond to this faultcode, so log it */
  fault = xmlnode_get_child((resp -> xml),"Body/Fault");
  if (fault != ((xmlnode *)((void *)0))) {
    char *fault_str = xmlnode_to_str(fault,0);
    purple_debug_error("msn","Operation {%s} Failed, SOAP Fault was: %s\n",msn_contact_operation_str((state -> action)),fault_str);
    g_free(fault_str);
    return ;
  }
  do {
    if ((state -> session) != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"state->session != NULL");
      return ;
    };
  }while (0);
  purple_debug_info("msn","Contact %s added successfully to %s list on server!\n",(state -> who),MsnMemberRole[state -> list_id]);
  if ((state -> list_id) == MSN_LIST_RL) {
    MsnUser *user = msn_userlist_find_user(( *(state -> session)).userlist,(state -> who));
    if (user != ((MsnUser *)((void *)0))) {
      msn_user_set_op(user,MSN_LIST_RL_OP);
    }
    if (((state -> action) & MSN_DENIED_BUDDY) != 0U) {
      msn_add_contact_to_list((state -> session),0,(state -> who),MSN_LIST_BL);
    }
    else if ((state -> list_id) == MSN_LIST_AL) {
      purple_privacy_permit_add(( *(state -> session)).account,(state -> who),(!0));
    }
    else if ((state -> list_id) == MSN_LIST_BL) {
      purple_privacy_deny_add(( *(state -> session)).account,(state -> who),(!0));
    }
  }
}

void msn_add_contact_to_list(MsnSession *session,MsnCallbackState *state,const gchar *passport,const MsnListId list)
{
  gchar *body = (gchar *)((void *)0);
  gchar *member = (gchar *)((void *)0);
  MsnSoapPartnerScenario partner_scenario;
  MsnUser *user;
  do {
    if (session != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return ;
    };
  }while (0);
  do {
    if (passport != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"passport != NULL");
      return ;
    };
  }while (0);
  do {
    if (list < 5) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"list < 5");
      return ;
    };
  }while (0);
  purple_debug_info("msn","Adding contact %s to %s list\n",passport,MsnMemberRole[list]);
  if (state == ((MsnCallbackState *)((void *)0))) {
    state = msn_callback_state_new(session);
  }
  msn_callback_state_set_list_id(state,list);
  msn_callback_state_set_who(state,passport);
  user = msn_userlist_find_user((session -> userlist),passport);
  partner_scenario = (((list == MSN_LIST_RL)?MSN_PS_CONTACT_API : MSN_PS_BLOCK_UNBLOCK));
  if ((user != 0) && ((user -> networkid) != MSN_NETWORK_PASSPORT)) 
    member = g_strdup_printf("<Member xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"%s\"><Type>%s</Type><State>Accepted</State><%s>%s</%s></Member>","EmailMember","Email","Email",(state -> who),"Email");
  else 
    member = g_strdup_printf("<Member xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"%s\"><Type>%s</Type><State>Accepted</State><%s>%s</%s></Member>","PassportMember","Passport","PassportName",(state -> who),"PassportName");
  body = g_strdup_printf("<\?xml version=\"1.0\" encoding=\"utf-8\"\?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\"><soap:Header><ABApplicationHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ApplicationId>CFE80F9D-180F-4399-82AB-413F33A1FA11</ApplicationId><IsMigration>false</IsMigration><PartnerScenario>%s</PartnerScenario></ABApplicationHeader><ABAuthHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ManagedGroupRequest>false</ManagedGroupRequest><TicketToken>EMPTY</TicketToken></ABAuthHeader></soap:Header><soap:Body><AddMember xmlns=\"http://www.msn.com/webservices/AddressBook\"><serviceHandle><Id>0</Id><Type>Messenger</Type><ForeignId></ForeignId></serviceHandle><memberships><Membership><MemberRole>%s</MemberRole><Members>%s</Members></Membership></memberships></AddMember></soap:Body></soap:Envelope>",MsnSoapPartnerScenarioText[partner_scenario],MsnMemberRole[list],member);
  state -> body = xmlnode_from_str(body,(-1));
  state -> post_action = "http://www.msn.com/webservices/AddressBook/AddMember";
  state -> post_url = "/abservice/SharingService.asmx";
  state -> cb = msn_add_contact_to_list_read_cb;
  msn_contact_request(state);
  g_free(member);
  g_free(body);
}
#if 0
/*get the gleams info*/
#endif
/***************************************************************
 * Group Operations
 ***************************************************************/

static void msn_group_read_cb(MsnSoapMessage *req,MsnSoapMessage *resp,gpointer data)
{
  MsnCallbackState *state = data;
  MsnSession *session;
  MsnUserList *userlist;
  xmlnode *fault;
/* We don't know how to respond to this faultcode, so log it */
  fault = xmlnode_get_child((resp -> xml),"Body/Fault");
  if (fault != ((xmlnode *)((void *)0))) {
    char *fault_str = xmlnode_to_str(fault,0);
    purple_debug_error("msn","Operation {%s} Failed, SOAP Fault was: %s\n",msn_contact_operation_str((state -> action)),fault_str);
    g_free(fault_str);
    return ;
  }
  purple_debug_info("msn","Group request successful.\n");
  do {
    if ((state -> session) != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"state->session != NULL");
      return ;
    };
  }while (0);
  do {
    if (( *(state -> session)).userlist != ((MsnUserList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"state->session->userlist != NULL");
      return ;
    };
  }while (0);
  session = (state -> session);
  userlist = (session -> userlist);
  if (((state -> action) & MSN_RENAME_GROUP) != 0U) {
    msn_userlist_rename_group_id((session -> userlist),(state -> guid),(state -> new_group_name));
  }
  if (((state -> action) & MSN_ADD_GROUP) != 0U) {
/* the response is taken from
		   http://telepathy.freedesktop.org/wiki/Pymsn/MSNP/ContactListActions
		   should copy it to msnpiki some day */
    xmlnode *guid_node = xmlnode_get_child((resp -> xml),"Body/ABGroupAddResponse/ABGroupAddResult/guid");
    if (guid_node != 0) {
      char *guid = xmlnode_get_data(guid_node);
/* create and add the new group to the userlist */
      purple_debug_info("msn","Adding group %s with guid = %s to the userlist\n",(state -> new_group_name),guid);
      msn_group_new((session -> userlist),guid,(state -> new_group_name));
      if (((state -> action) & MSN_ADD_BUDDY) != 0U) {
        msn_userlist_add_buddy((session -> userlist),(state -> who),(state -> new_group_name));
      }
      else if (((state -> action) & MSN_MOVE_BUDDY) != 0U) {
/* This will be freed when the add contact callback fires */
        MsnCallbackState *new_state = msn_callback_state_dup(state);
        msn_add_contact_to_group(session,new_state,(state -> who),guid);
        g_free(guid);
        return ;
      }
      g_free(guid);
    }
    else {
      purple_debug_info("msn","Adding group %s failed\n",(state -> new_group_name));
    }
  }
  if (((state -> action) & MSN_DEL_GROUP) != 0U) {
    GList *l;
    msn_userlist_remove_group_id((session -> userlist),(state -> guid));
    for (l = (userlist -> users); l != ((GList *)((void *)0)); l = (l -> next)) {
      msn_user_remove_group_id(((MsnUser *)(l -> data)),(state -> guid));
    }
  }
}
/* add group */

void msn_add_group(MsnSession *session,MsnCallbackState *state,const char *group_name)
{
  char *body = (char *)((void *)0);
  char *escaped_group_name = (char *)((void *)0);
  do {
    if (session != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return ;
    };
  }while (0);
  do {
    if (group_name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"group_name != NULL");
      return ;
    };
  }while (0);
  purple_debug_info("msn","Adding group %s to contact list.\n",group_name);
  if (state == ((MsnCallbackState *)((void *)0))) {
    state = msn_callback_state_new(session);
  }
  msn_callback_state_set_action(state,MSN_ADD_GROUP);
  msn_callback_state_set_new_group_name(state,group_name);
/* escape group name's html special chars so it can safely be sent
	* in a XML SOAP request
	*/
  escaped_group_name = g_markup_escape_text(group_name,(-1));
  body = g_strdup_printf("<\?xml version=\"1.0\" encoding=\"utf-8\"\?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\"><soap:Header><ABApplicationHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ApplicationId>CFE80F9D-180F-4399-82AB-413F33A1FA11</ApplicationId><IsMigration>false</IsMigration><PartnerScenario>GroupSave</PartnerScenario></ABApplicationHeader><ABAuthHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ManagedGroupRequest>false</ManagedGroupRequest><TicketToken>EMPTY</TicketToken></ABAuthHeader></soap:Header><soap:Body><ABGroupAdd xmlns=\"http://www.msn.com/webservices/AddressBook\"><abId>00000000-0000-0000-0000-000000000000</abId><groupAddOptions><fRenameOnMsgrConflict>false</fRenameOnMsgrConflict></groupAddOptions><groupInfo><GroupInfo><name>%s</name><groupType>C8529CE2-6EAD-434d-881F-341E17DB3FF8</groupType><fMessenger>false</fMessenger><annotations><Annotation><Name>MSN.IM.Display</Name><Value>1</Value></Annotation></annotations></GroupInfo></groupInfo></ABGroupAdd></soap:Body></soap:Envelope>",escaped_group_name);
  state -> body = xmlnode_from_str(body,(-1));
  state -> post_action = "http://www.msn.com/webservices/AddressBook/ABGroupAdd";
  state -> post_url = "/abservice/abservice.asmx";
  state -> cb = msn_group_read_cb;
  msn_contact_request(state);
  g_free(escaped_group_name);
  g_free(body);
}
/* delete group */

void msn_del_group(MsnSession *session,const gchar *group_name)
{
  MsnCallbackState *state;
  char *body = (char *)((void *)0);
  const gchar *guid;
  do {
    if (session != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return ;
    };
  }while (0);
  do {
    if (group_name != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"group_name != NULL");
      return ;
    };
  }while (0);
  purple_debug_info("msn","Deleting group %s from contact list\n",group_name);
  guid = msn_userlist_find_group_id((session -> userlist),group_name);
/* if group uid we need to del is NULL,
	*  we need to delete nothing
	*/
  if (guid == ((const gchar *)((void *)0))) {
    purple_debug_info("msn","Group %s guid not found, returning.\n",group_name);
    return ;
  }
  if (!(strcmp(guid,"1983") != 0) || !(strcmp(guid,"email") != 0)) {
/* XXX add back PurpleGroup since it isn't really removed in the server? */
    return ;
  }
  state = msn_callback_state_new(session);
  msn_callback_state_set_action(state,MSN_DEL_GROUP);
  msn_callback_state_set_guid(state,guid);
  body = g_strdup_printf("<\?xml version=\"1.0\" encoding=\"utf-8\"\?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\"><soap:Header><ABApplicationHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ApplicationId>CFE80F9D-180F-4399-82AB-413F33A1FA11</ApplicationId><IsMigration>false</IsMigration><PartnerScenario>Timer</PartnerScenario></ABApplicationHeader><ABAuthHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ManagedGroupRequest>false</ManagedGroupRequest><TicketToken>EMPTY</TicketToken></ABAuthHeader></soap:Header><soap:Body><ABGroupDelete xmlns=\"http://www.msn.com/webservices/AddressBook\"><abId>00000000-0000-0000-0000-000000000000</abId><groupFilter><groupIds><guid>%s</guid></groupIds></groupFilter></ABGroupDelete></soap:Body></soap:Envelope>",guid);
  state -> body = xmlnode_from_str(body,(-1));
  state -> post_action = "http://www.msn.com/webservices/AddressBook/ABGroupDelete";
  state -> post_url = "/abservice/abservice.asmx";
  state -> cb = msn_group_read_cb;
  msn_contact_request(state);
  g_free(body);
}
/* rename group */

void msn_contact_rename_group(MsnSession *session,const char *old_group_name,const char *new_group_name)
{
  gchar *body = (gchar *)((void *)0);
  const gchar *guid;
  MsnCallbackState *state;
  char *escaped_group_name;
  do {
    if (session != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return ;
    };
  }while (0);
  do {
    if ((session -> userlist) != ((MsnUserList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session->userlist != NULL");
      return ;
    };
  }while (0);
  do {
    if (old_group_name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"old_group_name != NULL");
      return ;
    };
  }while (0);
  do {
    if (new_group_name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"new_group_name != NULL");
      return ;
    };
  }while (0);
  purple_debug_info("msn","Renaming group %s to %s.\n",old_group_name,new_group_name);
  guid = msn_userlist_find_group_id((session -> userlist),old_group_name);
  if (guid == ((const gchar *)((void *)0))) 
    return ;
  state = msn_callback_state_new(session);
  msn_callback_state_set_guid(state,guid);
  msn_callback_state_set_new_group_name(state,new_group_name);
  if (!(strcmp(guid,"1983") != 0) || !(strcmp(guid,"email") != 0)) {
    MsnCallbackState *new_state = msn_callback_state_dup(state);
    msn_add_group(session,new_state,new_group_name);
/* XXX move every buddy there (we probably need to fix concurrent SOAP reqs first) */
  }
  msn_callback_state_set_action(state,MSN_RENAME_GROUP);
  escaped_group_name = g_markup_escape_text(new_group_name,(-1));
  body = g_strdup_printf("<\?xml version=\"1.0\" encoding=\"utf-8\"\?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\"><soap:Header><ABApplicationHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ApplicationId>CFE80F9D-180F-4399-82AB-413F33A1FA11</ApplicationId><IsMigration>false</IsMigration><PartnerScenario>Timer</PartnerScenario></ABApplicationHeader><ABAuthHeader xmlns=\"http://www.msn.com/webservices/AddressBook\"><ManagedGroupRequest>false</ManagedGroupRequest><TicketToken>EMPTY</TicketToken></ABAuthHeader></soap:Header><soap:Body><ABGroupUpdate xmlns=\"http://www.msn.com/webservices/AddressBook\"><abId>00000000-0000-0000-0000-000000000000</abId><groups><Group><groupId>%s</groupId><groupInfo><name>%s</name></groupInfo><propertiesChanged>GroupName </propertiesChanged></Group></groups></ABGroupUpdate></soap:Body></soap:Envelope>",guid,escaped_group_name);
  state -> body = xmlnode_from_str(body,(-1));
  state -> post_action = "http://www.msn.com/webservices/AddressBook/ABGroupUpdate";
  state -> post_url = "/abservice/abservice.asmx";
  state -> cb = msn_group_read_cb;
  msn_contact_request(state);
  g_free(escaped_group_name);
  g_free(body);
}
