/**
 * @file slpcall.c SLP Call Functions
 *
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "debug.h"
#include "smiley.h"
#include "msnutils.h"
#include "slpcall.h"
#include "slp.h"
#include "p2p.h"
#include "xfer.h"
/**************************************************************************
 * Main
 **************************************************************************/

static gboolean msn_slpcall_timeout(gpointer data)
{
  MsnSlpCall *slpcall;
  slpcall = data;
  if (purple_debug_is_verbose() != 0) 
    purple_debug_info("msn","slpcall_timeout: slpcall(%p)\n",slpcall);
  if (!((slpcall -> pending) != 0) && !((slpcall -> progress) != 0)) {
    msn_slpcall_destroy(slpcall);
    return (!0);
  }
  slpcall -> progress = 0;
  return (!0);
}

MsnSlpCall *msn_slpcall_new(MsnSlpLink *slplink)
{
  MsnSlpCall *slpcall;
  do {
    if (slplink != ((MsnSlpLink *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"slplink != NULL");
      return 0;
    };
  }while (0);
  slpcall = ((MsnSlpCall *)(g_malloc0_n(1,(sizeof(MsnSlpCall )))));
  if (purple_debug_is_verbose() != 0) 
    purple_debug_info("msn","slpcall_new: slpcall(%p)\n",slpcall);
  slpcall -> slplink = slplink;
  msn_slplink_add_slpcall(slplink,slpcall);
  slpcall -> timer = purple_timeout_add_seconds(300,msn_slpcall_timeout,slpcall);
  return slpcall;
}

void msn_slpcall_destroy(MsnSlpCall *slpcall)
{
  GList *e;
  if (purple_debug_is_verbose() != 0) 
    purple_debug_info("msn","slpcall_destroy: slpcall(%p)\n",slpcall);
  do {
    if (slpcall != ((MsnSlpCall *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"slpcall != NULL");
      return ;
    };
  }while (0);
  if ((slpcall -> timer) != 0U) 
    purple_timeout_remove((slpcall -> timer));
  for (e = ( *(slpcall -> slplink)).slp_msgs; e != ((GList *)((void *)0)); ) {
    MsnSlpMessage *slpmsg = (e -> data);
    e = (e -> next);
    if (purple_debug_is_verbose() != 0) 
      purple_debug_info("msn","slpcall_destroy: trying slpmsg(%p)\n",slpmsg);
    if ((slpmsg -> slpcall) == slpcall) {
      msn_slpmsg_destroy(slpmsg);
    }
  }
  if ((slpcall -> end_cb) != ((void (*)(MsnSlpCall *, MsnSession *))((void *)0))) 
    ( *(slpcall -> end_cb))(slpcall,( *(slpcall -> slplink)).session);
  if ((slpcall -> xfer) != ((PurpleXfer *)((void *)0))) {
    if ((purple_xfer_get_type((slpcall -> xfer))) == PURPLE_XFER_RECEIVE) 
      g_byte_array_free(slpcall -> u.incoming_data,(!0));
    ( *(slpcall -> xfer)).data = ((void *)((void *)0));
    purple_xfer_unref((slpcall -> xfer));
  }
  msn_slplink_remove_slpcall((slpcall -> slplink),slpcall);
  g_free((slpcall -> id));
  g_free((slpcall -> branch));
  g_free((slpcall -> data_info));
  g_free(slpcall);
}

void msn_slpcall_init(MsnSlpCall *slpcall,MsnSlpCallType type)
{
  slpcall -> session_id = (((rand()) % 0xFFFFFF00) + 4);
  slpcall -> id = rand_guid();
  slpcall -> type = type;
}

void msn_slpcall_session_init(MsnSlpCall *slpcall)
{
  if ((slpcall -> session_init_cb) != 0) 
    ( *(slpcall -> session_init_cb))(slpcall);
  slpcall -> started = (!0);
}

void msn_slpcall_invite(MsnSlpCall *slpcall,const char *euf_guid,MsnP2PAppId app_id,const char *context)
{
  MsnSlpLink *slplink;
  MsnSlpMessage *slpmsg;
  char *header;
  char *content;
  do {
    if (slpcall != ((MsnSlpCall *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"slpcall != NULL");
      return ;
    };
  }while (0);
  do {
    if (context != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context != NULL");
      return ;
    };
  }while (0);
  slplink = (slpcall -> slplink);
  slpcall -> branch = rand_guid();
  content = g_strdup_printf("EUF-GUID: {%s}\r\nSessionID: %lu\r\nAppID: %d\r\nContext: %s\r\n\r\n",euf_guid,(slpcall -> session_id),app_id,context);
  header = g_strdup_printf("INVITE MSNMSGR:%s MSNSLP/1.0",(slplink -> remote_user));
  slpmsg = msn_slpmsg_sip_new(slpcall,0,header,(slpcall -> branch),"application/x-msnmsgr-sessionreqbody",content);
  slpmsg -> info = "SLP INVITE";
  slpmsg -> text_body = (!0);
  msn_slplink_send_slpmsg(slplink,slpmsg);
  g_free(header);
  g_free(content);
}

void msn_slpcall_close(MsnSlpCall *slpcall)
{
  do {
    if (slpcall != ((MsnSlpCall *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"slpcall != NULL");
      return ;
    };
  }while (0);
  do {
    if ((slpcall -> slplink) != ((MsnSlpLink *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"slpcall->slplink != NULL");
      return ;
    };
  }while (0);
  send_bye(slpcall,"application/x-msnmsgr-sessionclosebody");
  msn_slplink_send_queued_slpmsgs((slpcall -> slplink));
  msn_slpcall_destroy(slpcall);
}
/*****************************************************************************
 * Parse received SLP messages
 ****************************************************************************/
/**************************************************************************
 *** Util
 **************************************************************************/

static char *get_token(const char *str,const char *start,const char *end)
{
  const char *c;
  const char *c2;
  if ((c = (strstr(str,start))) == ((const char *)((void *)0))) 
    return 0;
  c += strlen(start);
  if (end != ((const char *)((void *)0))) {
    if ((c2 = (strstr(c,end))) == ((const char *)((void *)0))) 
      return 0;
    return g_strndup(c,(c2 - c));
  }
  else {
/* This has to be changed */
    return g_strdup(c);
  }
}
/* XXX: this could be improved if we tracked custom smileys
 * per-protocol, per-account, per-session or (ideally) per-conversation
 */

static PurpleStoredImage *find_valid_emoticon(PurpleAccount *account,const char *path)
{
  GList *smileys;
  if (!(purple_account_get_bool(account,"custom_smileys",(!0)) != 0)) 
    return 0;
  smileys = purple_smileys_get_all();
  for (; smileys != 0; smileys = g_list_delete_link(smileys,smileys)) {
    PurpleSmiley *smiley;
    PurpleStoredImage *img;
    smiley = (smileys -> data);
    img = purple_smiley_get_stored_image(smiley);
    if (purple_strequal(path,purple_imgstore_get_filename(img)) != 0) {
      g_list_free(smileys);
      return img;
    }
    purple_imgstore_unref(img);
  }
  purple_debug_error("msn","Received illegal request for file %s\n",path);
  return 0;
}

static char *parse_dc_nonce(const char *content,MsnDirectConnNonceType *ntype)
{
  char *nonce;
   *ntype = DC_NONCE_UNKNOWN;
  nonce = get_token(content,"Hashed-Nonce: {","}\r\n");
  if (nonce != 0) {
     *ntype = DC_NONCE_SHA1;
  }
  else {
    guint32 n1;
    guint32 n6;
    guint16 n2;
    guint16 n3;
    guint16 n4;
    guint16 n5;
    nonce = get_token(content,"Nonce: {","}\r\n");
    if ((nonce != 0) && (sscanf(nonce,"%08x-%04hx-%04hx-%04hx-%04hx%08x",&n1,&n2,&n3,&n4,&n5,&n6) == 6)) {
       *ntype = DC_NONCE_PLAIN;
      g_free(nonce);
      nonce = (g_malloc(16));
       *((guint32 *)(nonce + 0)) = ((guint32 )n1);
       *((guint16 *)(nonce + 4)) = ((guint16 )n2);
       *((guint16 *)(nonce + 6)) = ((guint16 )n3);
       *((guint16 *)(nonce + 8)) = ((guint16 )(((guint16 )(((guint16 )n4) >> 8)) | ((guint16 )(((guint16 )n4) << 8))));
       *((guint16 *)(nonce + 10)) = ((guint16 )(((guint16 )(((guint16 )n5) >> 8)) | ((guint16 )(((guint16 )n5) << 8))));
       *((guint32 *)(nonce + 12)) = (((((((guint32 )n6) & ((guint32 )0x000000ffU)) << 24) | ((((guint32 )n6) & ((guint32 )0x0000ff00U)) << 8)) | ((((guint32 )n6) & ((guint32 )0x00ff0000U)) >> 8)) | ((((guint32 )n6) & ((guint32 )0xff000000U)) >> 24));
    }
    else {
/* Invalid nonce, so ignore request */
      g_free(nonce);
      nonce = ((char *)((void *)0));
    }
  }
  return nonce;
}

static void msn_slp_process_transresp(MsnSlpCall *slpcall,const char *content)
{
/* A direct connection negotiation response */
  char *bridge;
  char *nonce;
  char *listening;
  MsnDirectConn *dc = ( *(slpcall -> slplink)).dc;
  MsnDirectConnNonceType ntype;
  purple_debug_info("msn","process_transresp\n");
/* Direct connections are disabled. */
  if (!(purple_account_get_bool(( *( *(slpcall -> slplink)).session).account,"direct_connect",(!0)) != 0)) 
    return ;
  do {
    if (dc != ((MsnDirectConn *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"dc != NULL");
      return ;
    };
  }while (0);
  do {
    if ((dc -> state) == DC_STATE_CLOSED) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"dc->state == DC_STATE_CLOSED");
      return ;
    };
  }while (0);
  bridge = get_token(content,"Bridge: ","\r\n");
  nonce = parse_dc_nonce(content,&ntype);
  listening = get_token(content,"Listening: ","\r\n");
  if (((listening != 0) && (bridge != 0)) && !(strcmp(bridge,"TCPv1") != 0)) {
/* Ok, the client supports direct TCP connection */
/* We always need this. */
    if (ntype == DC_NONCE_SHA1) {
      strncpy((dc -> remote_nonce),nonce,36);
      (dc -> remote_nonce)[36] = 0;
    }
    if (!(strcasecmp(listening,"false") != 0)) {
      if ((dc -> listen_data) != ((PurpleNetworkListenData *)((void *)0))) {
/*
				 * We'll listen for incoming connections but
				 * the listening socket isn't ready yet so we cannot
				 * send the INVITE packet now. Put the slpcall into waiting mode
				 * and let the callback send the invite.
				 */
        slpcall -> wait_for_socket = (!0);
      }
      else if ((dc -> listenfd) != -1) {
/* The listening socket is ready. Send the INVITE here. */
        msn_dc_send_invite(dc);
      }
      else {
/* We weren't able to create a listener either. Use SB. */
        msn_dc_fallback_to_sb(dc);
      }
    }
    else {
/*
			 * We should connect to the client so parse
			 * IP/port from response.
			 */
      char *ip;
      char *port_str;
      int port = 0;
      if (ntype == DC_NONCE_PLAIN) {
/* Only needed for listening side. */
        memcpy((dc -> nonce),nonce,16);
      }
/* Cancel any listen attempts because we don't need them. */
      if ((dc -> listenfd_handle) != 0) {
        purple_input_remove((dc -> listenfd_handle));
        dc -> listenfd_handle = 0;
      }
      if ((dc -> connect_timeout_handle) != 0) {
        purple_timeout_remove((dc -> connect_timeout_handle));
        dc -> connect_timeout_handle = 0;
      }
      if ((dc -> listenfd) != -1) {
        purple_network_remove_port_mapping((dc -> listenfd));
        close((dc -> listenfd));
        dc -> listenfd = -1;
      }
      if ((dc -> listen_data) != ((PurpleNetworkListenData *)((void *)0))) {
        purple_network_listen_cancel((dc -> listen_data));
        dc -> listen_data = ((PurpleNetworkListenData *)((void *)0));
      }
/* Save external IP/port for later use. We'll try local connection first. */
      dc -> ext_ip = get_token(content,"IPv4External-Addrs: ","\r\n");
      port_str = get_token(content,"IPv4External-Port: ","\r\n");
      if (port_str != 0) {
        dc -> ext_port = atoi(port_str);
        g_free(port_str);
      }
      ip = get_token(content,"IPv4Internal-Addrs: ","\r\n");
      port_str = get_token(content,"IPv4Internal-Port: ","\r\n");
      if (port_str != 0) {
        port = atoi(port_str);
        g_free(port_str);
      }
      if ((ip != 0) && (port != 0)) {
/* Try internal address first */
        dc -> connect_data = purple_proxy_connect(0,( *( *(slpcall -> slplink)).session).account,ip,port,msn_dc_connected_to_peer_cb,dc);
        if ((dc -> connect_data) != 0) {
/* Add connect timeout handle */
          dc -> connect_timeout_handle = purple_timeout_add_seconds(5,msn_dc_outgoing_connection_timeout_cb,dc);
        }
        else {
/*
					 * Connection failed
					 * Try external IP/port (if specified)
					 */
          msn_dc_outgoing_connection_timeout_cb(dc);
        }
      }
      else {
/*
				 * Omitted or invalid internal IP address / port
				 * Try external IP/port (if specified)
				 */
        msn_dc_outgoing_connection_timeout_cb(dc);
      }
      g_free(ip);
    }
  }
  else {
/*
		 * Invalid direct connect invitation or
		 * TCP connection is not supported
		 */
  }
  g_free(listening);
  g_free(nonce);
  g_free(bridge);
}

static void got_sessionreq(MsnSlpCall *slpcall,const char *branch,const char *euf_guid,const char *context)
{
  gboolean accepted = 0;
  if (!(strcmp(euf_guid,"A4268EEC-FEC5-49E5-95C3-F126696BDBF6") != 0)) {
/* Emoticon or UserDisplay */
    char *content;
    gsize len;
    MsnSlpLink *slplink;
    MsnSlpMessage *slpmsg;
    MsnObject *obj;
    char *msnobj_data;
    PurpleStoredImage *img = (PurpleStoredImage *)((void *)0);
    int type;
/* Send Ok */
    content = g_strdup_printf("SessionID: %lu\r\n\r\n",(slpcall -> session_id));
    msn_slp_send_ok(slpcall,branch,"application/x-msnmsgr-sessionreqbody",content);
    g_free(content);
    slplink = (slpcall -> slplink);
    msnobj_data = ((char *)(purple_base64_decode(context,&len)));
    obj = msn_object_new_from_string(msnobj_data);
    type = (msn_object_get_type(obj));
    g_free(msnobj_data);
    if (type == MSN_OBJECT_EMOTICON) {
      img = find_valid_emoticon(( *(slplink -> session)).account,(obj -> location));
    }
    else if (type == MSN_OBJECT_USERTILE) {
      img = msn_object_get_image(obj);
      if (img != 0) 
        purple_imgstore_ref(img);
    }
    msn_object_destroy(obj);
    if (img != ((PurpleStoredImage *)((void *)0))) {
/* DATA PREP */
      slpmsg = msn_slpmsg_dataprep_new(slpcall);
      msn_slplink_queue_slpmsg(slplink,slpmsg);
/* DATA */
      slpmsg = msn_slpmsg_obj_new(slpcall,img);
      msn_slplink_queue_slpmsg(slplink,slpmsg);
      purple_imgstore_unref(img);
      accepted = (!0);
    }
    else {
      purple_debug_error("msn","Wrong object.\n");
    }
  }
  else if (!(strcmp(euf_guid,"5D3E02AB-6190-11D3-BBBB-00C04F795683") != 0)) {
/* File Transfer */
    PurpleAccount *account;
    PurpleXfer *xfer;
    MsnFileContext *file_context;
    char *buf;
    gsize bin_len;
    guint32 file_size;
    char *file_name;
    account = ( *( *(slpcall -> slplink)).session).account;
    slpcall -> end_cb = msn_xfer_end_cb;
    slpcall -> branch = g_strdup(branch);
    slpcall -> pending = (!0);
    xfer = purple_xfer_new(account,PURPLE_XFER_RECEIVE,( *(slpcall -> slplink)).remote_user);
    buf = ((char *)(purple_base64_decode(context,&bin_len)));
    file_context = msn_file_context_from_wire(buf,bin_len);
    if (file_context != ((MsnFileContext *)((void *)0))) {
      file_size = (file_context -> file_size);
      file_name = g_convert(((const gchar *)(&file_context -> file_name)),(260 * 2),"UTF-8","UTF-16LE",0,0,0);
      purple_xfer_set_filename(xfer,((file_name != 0)?file_name : ""));
      g_free(file_name);
      purple_xfer_set_size(xfer,file_size);
      purple_xfer_set_init_fnc(xfer,msn_xfer_init);
      purple_xfer_set_request_denied_fnc(xfer,msn_xfer_cancel);
      purple_xfer_set_cancel_recv_fnc(xfer,msn_xfer_cancel);
      purple_xfer_set_read_fnc(xfer,msn_xfer_read);
      purple_xfer_set_write_fnc(xfer,msn_xfer_write);
      slpcall -> u.incoming_data = g_byte_array_new();
      slpcall -> xfer = xfer;
      purple_xfer_ref((slpcall -> xfer));
      xfer -> data = slpcall;
      if ((file_context -> preview) != 0) {
        purple_xfer_set_thumbnail(xfer,(file_context -> preview),(file_context -> preview_len),"image/png");
        g_free((file_context -> preview));
      }
      purple_xfer_request(xfer);
    }
    g_free(file_context);
    g_free(buf);
    accepted = (!0);
  }
  else if (!(strcmp(euf_guid,"1C9AA97E-9C05-4583-A3BD-908A196F1E92") != 0)) {
    purple_debug_info("msn","Cam request.\n");
    if (((slpcall -> slplink) != 0) && (( *(slpcall -> slplink)).session != 0)) {
      PurpleConversation *conv;
      gchar *from = ( *(slpcall -> slplink)).remote_user;
      conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,from,( *( *(slpcall -> slplink)).session).account);
      if (conv != 0) {
        char *buf;
        buf = g_strdup_printf(((const char *)(dgettext("pidgin","%s requests to view your webcam, but this request is not yet supported."))),from);
        purple_conversation_write(conv,0,buf,(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NOTIFY),time(0));
        g_free(buf);
      }
    }
  }
  else if (!(strcmp(euf_guid,"4BD96FC0-AB17-4425-A14A-439185962DC8") != 0)) {
    purple_debug_info("msn","Cam invite.\n");
    if (((slpcall -> slplink) != 0) && (( *(slpcall -> slplink)).session != 0)) {
      PurpleConversation *conv;
      gchar *from = ( *(slpcall -> slplink)).remote_user;
      conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,from,( *( *(slpcall -> slplink)).session).account);
      if (conv != 0) {
        char *buf;
        buf = g_strdup_printf(((const char *)(dgettext("pidgin","%s invited you to view his/her webcam, but this is not yet supported."))),from);
        purple_conversation_write(conv,0,buf,(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NOTIFY),time(0));
        g_free(buf);
      }
    }
  }
  else 
    purple_debug_warning("msn","SLP SessionReq with unknown EUF-GUID: %s\n",euf_guid);
  if (!(accepted != 0)) {
    char *content = g_strdup_printf("SessionID: %lu\r\n\r\n",(slpcall -> session_id));
    msn_slp_send_decline(slpcall,branch,"application/x-msnmsgr-sessionreqbody",content);
    g_free(content);
  }
}

void send_bye(MsnSlpCall *slpcall,const char *type)
{
  MsnSlpLink *slplink;
  PurpleAccount *account;
  MsnSlpMessage *slpmsg;
  char *header;
  slplink = (slpcall -> slplink);
  do {
    if (slplink != ((MsnSlpLink *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"slplink != NULL");
      return ;
    };
  }while (0);
  account = ( *(slplink -> session)).account;
  header = g_strdup_printf("BYE MSNMSGR:%s MSNSLP/1.0",purple_account_get_username(account));
  slpmsg = msn_slpmsg_sip_new(slpcall,0,header,"A0D624A6-6C0C-4283-A9E0-BC97B4B46D32",type,"\r\n");
  g_free(header);
  slpmsg -> info = "SLP BYE";
  slpmsg -> text_body = (!0);
  msn_slplink_queue_slpmsg(slplink,slpmsg);
}

static void got_invite(MsnSlpCall *slpcall,const char *branch,const char *type,const char *content)
{
  MsnSlpLink *slplink;
  slplink = (slpcall -> slplink);
  if (!(strcmp(type,"application/x-msnmsgr-sessionreqbody") != 0)) {
    char *euf_guid;
    char *context;
    char *temp;
    euf_guid = get_token(content,"EUF-GUID: {","}\r\n");
    temp = get_token(content,"SessionID: ","\r\n");
    if (temp != ((char *)((void *)0))) 
      slpcall -> session_id = (atoi(temp));
    g_free(temp);
    temp = get_token(content,"AppID: ","\r\n");
    if (temp != ((char *)((void *)0))) 
      slpcall -> app_id = (atoi(temp));
    g_free(temp);
    context = get_token(content,"Context: ","\r\n");
    if (context != ((char *)((void *)0))) 
      got_sessionreq(slpcall,branch,euf_guid,context);
    g_free(context);
    g_free(euf_guid);
  }
  else if (!(strcmp(type,"application/x-msnmsgr-transreqbody") != 0)) {
/* A direct connection negotiation request */
    char *bridges;
    char *nonce;
    MsnDirectConnNonceType ntype;
    purple_debug_info("msn","got_invite: transreqbody received\n");
/* Direct connections may be disabled. */
    if (!(purple_account_get_bool(( *(slplink -> session)).account,"direct_connect",(!0)) != 0)) {
      msn_slp_send_ok(slpcall,branch,"application/x-msnmsgr-transrespbody","Bridge: TCPv1\r\nListening: false\r\nNonce: {00000000-0000-0000-0000-000000000000}\r\n\r\n");
      msn_slpcall_session_init(slpcall);
      return ;
    }
/* Don't do anything if we already have a direct connection */
    if ((slplink -> dc) != ((MsnDirectConn *)((void *)0))) 
      return ;
    bridges = get_token(content,"Bridges: ","\r\n");
    nonce = parse_dc_nonce(content,&ntype);
    if ((bridges != 0) && (strstr(bridges,"TCPv1") != ((char *)((void *)0)))) {
/*
			 * Ok, the client supports direct TCP connection
			 * Try to create a listening port
			 */
      MsnDirectConn *dc;
      dc = msn_dc_new(slpcall);
      if (ntype == DC_NONCE_PLAIN) {
/* There is only one nonce for plain auth. */
        dc -> nonce_type = ntype;
        memcpy((dc -> nonce),nonce,16);
      }
      else if (ntype == DC_NONCE_SHA1) {
/* Each side has a nonce in SHA1 auth. */
        dc -> nonce_type = ntype;
        strncpy((dc -> remote_nonce),nonce,36);
        (dc -> remote_nonce)[36] = 0;
      }
      dc -> listen_data = purple_network_listen_range(0,0,SOCK_STREAM,msn_dc_listen_socket_created_cb,dc);
      if ((dc -> listen_data) == ((PurpleNetworkListenData *)((void *)0))) {
/* Listen socket creation failed */
        purple_debug_info("msn","got_invite: listening failed\n");
        if ((dc -> nonce_type) != DC_NONCE_PLAIN) 
          msn_slp_send_ok(slpcall,branch,"application/x-msnmsgr-transrespbody","Bridge: TCPv1\r\nListening: false\r\nHashed-Nonce: {00000000-0000-0000-0000-000000000000}\r\n\r\n");
        else 
          msn_slp_send_ok(slpcall,branch,"application/x-msnmsgr-transrespbody","Bridge: TCPv1\r\nListening: false\r\nNonce: {00000000-0000-0000-0000-000000000000}\r\n\r\n");
      }
      else {
/*
				 * Listen socket created successfully.
				 * Don't send anything here because we don't know the parameters
				 * of the created socket yet. msn_dc_send_ok will be called from
				 * the callback function: dc_listen_socket_created_cb
				 */
        purple_debug_info("msn","got_invite: listening socket created\n");
        dc -> send_connection_info_msg_cb = msn_dc_send_ok;
        slpcall -> wait_for_socket = (!0);
      }
    }
    else {
/*
			 * Invalid direct connect invitation or
			 * TCP connection is not supported.
			 */
    }
    g_free(nonce);
    g_free(bridges);
  }
  else if (!(strcmp(type,"application/x-msnmsgr-transrespbody") != 0)) {
/* A direct connection negotiation response */
    msn_slp_process_transresp(slpcall,content);
  }
}

static void got_ok(MsnSlpCall *slpcall,const char *type,const char *content)
{
  do {
    if (slpcall != ((MsnSlpCall *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"slpcall != NULL");
      return ;
    };
  }while (0);
  do {
    if (type != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type != NULL");
      return ;
    };
  }while (0);
  if (!(strcmp(type,"application/x-msnmsgr-sessionreqbody") != 0)) {
    char *content;
    char *header;
    char *nonce = (char *)((void *)0);
    MsnSession *session = ( *(slpcall -> slplink)).session;
    MsnSlpMessage *msg;
    MsnDirectConn *dc;
    MsnUser *user;
    if (!(purple_account_get_bool((session -> account),"direct_connect",(!0)) != 0)) {
/* Don't attempt a direct connection if disabled. */
      msn_slpcall_session_init(slpcall);
      return ;
    }
    if (( *(slpcall -> slplink)).dc != ((MsnDirectConn *)((void *)0))) {
/* If we already have an established direct connection
			 * then just start the transfer.
			 */
      msn_slpcall_session_init(slpcall);
      return ;
    }
    user = msn_userlist_find_user((session -> userlist),( *(slpcall -> slplink)).remote_user);
    if (!(user != 0) || !(((user -> clientid) & 0xF0000000) != 0U)) {
/* Just start a normal SB transfer. */
      msn_slpcall_session_init(slpcall);
      return ;
    }
/* Try direct file transfer by sending a second INVITE */
    dc = msn_dc_new(slpcall);
    g_free((slpcall -> branch));
    slpcall -> branch = rand_guid();
    dc -> listen_data = purple_network_listen_range(0,0,SOCK_STREAM,msn_dc_listen_socket_created_cb,dc);
    header = g_strdup_printf("INVITE MSNMSGR:%s MSNSLP/1.0",( *(slpcall -> slplink)).remote_user);
    if ((dc -> nonce_type) == DC_NONCE_SHA1) 
      nonce = g_strdup_printf("Hashed-Nonce: {%s}\r\n",(dc -> nonce_hash));
    if ((dc -> listen_data) == ((PurpleNetworkListenData *)((void *)0))) {
/* Listen socket creation failed */
      purple_debug_info("msn","got_ok: listening failed\n");
      content = g_strdup_printf("Bridges: TCPv1\r\nNetID: %u\r\nConn-Type: IP-Restrict-NAT\r\nUPnPNat: false\r\nICF: false\r\n%s\r\n",((rand()) % ((guint32 )0xffffffff)),((nonce != 0)?nonce : ""));
    }
    else {
/* Listen socket created successfully. */
      purple_debug_info("msn","got_ok: listening socket created\n");
      content = g_strdup_printf("Bridges: TCPv1\r\nNetID: 0\r\nConn-Type: Direct-Connect\r\nUPnPNat: false\r\nICF: false\r\n%s\r\n",((nonce != 0)?nonce : ""));
    }
    msg = msn_slpmsg_sip_new(slpcall,0,header,(slpcall -> branch),"application/x-msnmsgr-transreqbody",content);
    msg -> info = "DC INVITE";
    msg -> text_body = (!0);
    g_free(nonce);
    g_free(header);
    g_free(content);
    msn_slplink_queue_slpmsg((slpcall -> slplink),msg);
  }
  else if (!(strcmp(type,"application/x-msnmsgr-transreqbody") != 0)) {
/* Do we get this? */
    purple_debug_info("msn","OK with transreqbody\n");
  }
  else if (!(strcmp(type,"application/x-msnmsgr-transrespbody") != 0)) {
    msn_slp_process_transresp(slpcall,content);
  }
}

static void got_error(MsnSlpCall *slpcall,const char *error,const char *type,const char *content)
{
/* It's not valid. Kill this off. */
  purple_debug_error("msn","Received non-OK result: %s\n",((error != 0)?error : "Unknown"));
  if ((type != 0) && !(strcmp(type,"application/x-msnmsgr-transreqbody") != 0)) {
    MsnDirectConn *dc = ( *(slpcall -> slplink)).dc;
    if (dc != 0) {
      msn_dc_fallback_to_sb(dc);
      return ;
    }
  }
  slpcall -> wasted = (!0);
}

static MsnSlpCall *msn_slp_sip_recv(MsnSlpLink *slplink,const char *body)
{
  MsnSlpCall *slpcall;
  if (body == ((const char *)((void *)0))) {
    purple_debug_warning("msn","received bogus message\n");
    return 0;
  }
  if (!(strncmp(body,"INVITE",strlen("INVITE")) != 0)) {
/* This is an INVITE request */
    char *branch;
    char *call_id;
    char *content;
    char *content_type;
/* From: <msnmsgr:buddy@hotmail.com> */
#if 0
#endif
    branch = get_token(body,";branch={","}");
    call_id = get_token(body,"Call-ID: {","}");
#if 0
#endif
    content_type = get_token(body,"Content-Type: ","\r\n");
    content = get_token(body,"\r\n\r\n",0);
    slpcall = ((MsnSlpCall *)((void *)0));
    if ((branch != 0) && (call_id != 0)) {
      slpcall = msn_slplink_find_slp_call(slplink,call_id);
      if (slpcall != 0) {
        g_free((slpcall -> branch));
        slpcall -> branch = g_strdup(branch);
        got_invite(slpcall,branch,content_type,content);
      }
      else if ((content_type != 0) && (content != 0)) {
        slpcall = msn_slpcall_new(slplink);
        slpcall -> id = g_strdup(call_id);
        got_invite(slpcall,branch,content_type,content);
      }
    }
    g_free(call_id);
    g_free(branch);
    g_free(content_type);
    g_free(content);
  }
  else if (!(strncmp(body,"MSNSLP/1.0 ",strlen("MSNSLP/1.0 ")) != 0)) {
/* This is a response */
    char *content;
    char *content_type;
/* Make sure this is "OK" */
    const char *status = (body + strlen("MSNSLP/1.0 "));
    char *call_id;
    call_id = get_token(body,"Call-ID: {","}");
    slpcall = msn_slplink_find_slp_call(slplink,call_id);
    g_free(call_id);
    do {
      if (slpcall != ((MsnSlpCall *)((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"slpcall != NULL");
        return 0;
      };
    }while (0);
    content_type = get_token(body,"Content-Type: ","\r\n");
    content = get_token(body,"\r\n\r\n",0);
    if (strncmp(status,"200 OK",6) != 0) {
      char *error = (char *)((void *)0);
      const char *c;
/* Eww */
      if ((((c = (strchr(status,13))) != 0) || ((c = (strchr(status,10))) != 0)) || ((c = (strchr(status,0))) != 0)) {
        size_t len = (c - status);
        error = g_strndup(status,len);
      }
      got_error(slpcall,error,content_type,content);
      g_free(error);
    }
    else {
/* Everything's just dandy */
      got_ok(slpcall,content_type,content);
    }
    g_free(content_type);
    g_free(content);
  }
  else if (!(strncmp(body,"BYE",strlen("BYE")) != 0)) {
/* This is a BYE request */
    char *call_id;
    call_id = get_token(body,"Call-ID: {","}");
    slpcall = msn_slplink_find_slp_call(slplink,call_id);
    g_free(call_id);
    if (slpcall != ((MsnSlpCall *)((void *)0))) 
      slpcall -> wasted = (!0);
/* msn_slpcall_destroy(slpcall); */
  }
  else 
    slpcall = ((MsnSlpCall *)((void *)0));
  return slpcall;
}

MsnSlpCall *msn_slp_process_msg(MsnSlpLink *slplink,MsnSlpMessage *slpmsg)
{
  MsnSlpCall *slpcall;
  const guchar *body;
  gsize body_len;
  guint32 session_id;
  guint32 flags;
  slpcall = ((MsnSlpCall *)((void *)0));
  body = (slpmsg -> buffer);
  body_len = msn_p2p_info_get_offset((slpmsg -> p2p_info));
  session_id = msn_p2p_info_get_session_id((slpmsg -> p2p_info));
  flags = msn_p2p_info_get_flags((slpmsg -> p2p_info));
  if ((flags == P2P_NO_FLAG) || (flags == P2P_WLM2009_COMP)) {
    char *body_str;
    if (session_id == 64) {
/* This is for handwritten messages (Ink) */
      GError *error = (GError *)((void *)0);
      gsize bytes_read;
      gsize bytes_written;
      body_str = g_convert(((const gchar *)body),(body_len / 2),"UTF-8","UTF-16LE",&bytes_read,&bytes_written,&error);
      body_len -= (bytes_read + 2);
      body += (bytes_read + 2);
      if (((body_str == ((char *)((void *)0))) || (body_len <= 0)) || (strstr(body_str,"image/gif") == ((char *)((void *)0)))) {
        if (error != ((GError *)((void *)0))) {
          purple_debug_error("msn","Unable to convert Ink header from UTF-16 to UTF-8: %s\n",(error -> message));
          g_error_free(error);
        }
        else 
          purple_debug_error("msn","Received Ink in unknown format\n");
        g_free(body_str);
        return 0;
      }
      g_free(body_str);
      body_str = g_convert(((const gchar *)body),(body_len / 2),"UTF-8","UTF-16LE",&bytes_read,&bytes_written,&error);
      if (!(body_str != 0)) {
        if (error != ((GError *)((void *)0))) {
          purple_debug_error("msn","Unable to convert Ink body from UTF-16 to UTF-8: %s\n",(error -> message));
          g_error_free(error);
        }
        else 
          purple_debug_error("msn","Received Ink in unknown format\n");
        return 0;
      }
      msn_switchboard_show_ink(( *(slpmsg -> slplink)).swboard,(slplink -> remote_user),body_str);
    }
    else {
      body_str = g_strndup(((const char *)body),body_len);
      slpcall = msn_slp_sip_recv(slplink,body_str);
    }
    g_free(body_str);
  }
  else if (msn_p2p_msg_is_data((slpmsg -> p2p_info)) != 0) {
    slpcall = msn_slplink_find_slp_call_with_session_id(slplink,session_id);
    if (slpcall != ((MsnSlpCall *)((void *)0))) {
      if ((slpcall -> timer) != 0U) {
        purple_timeout_remove((slpcall -> timer));
        slpcall -> timer = 0;
      }
      if ((slpcall -> cb) != 0) 
        ( *(slpcall -> cb))(slpcall,body,body_len);
      slpcall -> wasted = (!0);
    }
  }
  else if (msn_p2p_info_is_ack((slpmsg -> p2p_info)) != 0) {
/* Acknowledgement of previous message. Don't do anything currently. */
  }
  else 
    purple_debug_warning("msn","Unprocessed SLP message with flags 0x%04x\n",flags);
  return slpcall;
}
