/**
 * @file object.c MSNObject API
 *
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "msn.h"
#include "object.h"
#include "debug.h"
/* Sha1 stuff */
#include "cipher.h"
/* Base64 stuff */
#include "util.h"
#define GET_STRING_TAG(field, id) \
	if ((tag = strstr(str, id "=\"")) != NULL) \
	{ \
		tag += strlen(id "=\""); \
		c = strchr(tag, '"'); \
		if (c != NULL) \
		{ \
			if (obj->field != NULL) \
				g_free(obj->field); \
			obj->field = g_strndup(tag, c - tag); \
		} \
	}
#define GET_INT_TAG(field, id) \
	if ((tag = strstr(str, id "=\"")) != NULL) \
	{ \
		char buf[16]; \
		size_t offset; \
		tag += strlen(id "=\""); \
		c = strchr(tag, '"'); \
		if (c != NULL) \
		{ \
			memset(buf, 0, sizeof(buf)); \
			offset = c - tag; \
			if (offset >= sizeof(buf)) \
				offset = sizeof(buf) - 1; \
			strncpy(buf, tag, offset); \
			obj->field = atoi(buf); \
		} \
	}
static GList *local_objs;

MsnObject *msn_object_new()
{
  MsnObject *obj;
  obj = ((MsnObject *)(g_malloc0_n(1,(sizeof(MsnObject )))));
  msn_object_set_type(obj,MSN_OBJECT_UNKNOWN);
  msn_object_set_friendly(obj,"AAA=");
  return obj;
}

MsnObject *msn_object_new_from_string(const char *str)
{
  MsnObject *obj;
  char *tag;
  char *c;
  do {
    if (str != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"str != NULL");
      return 0;
    };
  }while (0);
  if (strncmp(str,"<msnobj ",8) != 0) 
    return 0;
  obj = msn_object_new();
  if ((tag = strstr(str,"Creator=\"")) != ((char *)((void *)0))) {
    tag += strlen("Creator=\"");
    c = strchr(tag,'"');
    if (c != ((char *)((void *)0))) {
      if ((obj -> creator) != ((char *)((void *)0))) 
        g_free((obj -> creator));
      obj -> creator = g_strndup(tag,(c - tag));
    }
  };
  if ((tag = strstr(str,"Size=\"")) != ((char *)((void *)0))) {
    char buf[16UL];
    size_t offset;
    tag += strlen("Size=\"");
    c = strchr(tag,'"');
    if (c != ((char *)((void *)0))) {
      memset(buf,0,(sizeof(buf)));
      offset = (c - tag);
      if (offset >= sizeof(buf)) 
        offset = (sizeof(buf) - 1);
      strncpy(buf,tag,offset);
      obj -> size = atoi(buf);
    }
  };
  if ((tag = strstr(str,"Type=\"")) != ((char *)((void *)0))) {
    char buf[16UL];
    size_t offset;
    tag += strlen("Type=\"");
    c = strchr(tag,'"');
    if (c != ((char *)((void *)0))) {
      memset(buf,0,(sizeof(buf)));
      offset = (c - tag);
      if (offset >= sizeof(buf)) 
        offset = (sizeof(buf) - 1);
      strncpy(buf,tag,offset);
      obj -> type = (atoi(buf));
    }
  };
  if ((tag = strstr(str,"Location=\"")) != ((char *)((void *)0))) {
    tag += strlen("Location=\"");
    c = strchr(tag,'"');
    if (c != ((char *)((void *)0))) {
      if ((obj -> location) != ((char *)((void *)0))) 
        g_free((obj -> location));
      obj -> location = g_strndup(tag,(c - tag));
    }
  };
  if ((tag = strstr(str,"Friendly=\"")) != ((char *)((void *)0))) {
    tag += strlen("Friendly=\"");
    c = strchr(tag,'"');
    if (c != ((char *)((void *)0))) {
      if ((obj -> friendly) != ((char *)((void *)0))) 
        g_free((obj -> friendly));
      obj -> friendly = g_strndup(tag,(c - tag));
    }
  };
  if ((tag = strstr(str,"SHA1D=\"")) != ((char *)((void *)0))) {
    tag += strlen("SHA1D=\"");
    c = strchr(tag,'"');
    if (c != ((char *)((void *)0))) {
      if ((obj -> sha1d) != ((char *)((void *)0))) 
        g_free((obj -> sha1d));
      obj -> sha1d = g_strndup(tag,(c - tag));
    }
  };
  if ((tag = strstr(str,"SHA1C=\"")) != ((char *)((void *)0))) {
    tag += strlen("SHA1C=\"");
    c = strchr(tag,'"');
    if (c != ((char *)((void *)0))) {
      if ((obj -> sha1c) != ((char *)((void *)0))) 
        g_free((obj -> sha1c));
      obj -> sha1c = g_strndup(tag,(c - tag));
    }
  };
  if ((tag = strstr(str,"Url=\"")) != ((char *)((void *)0))) {
    tag += strlen("Url=\"");
    c = strchr(tag,'"');
    if (c != ((char *)((void *)0))) {
      if ((obj -> url) != ((char *)((void *)0))) 
        g_free((obj -> url));
      obj -> url = g_strndup(tag,(c - tag));
    }
  };
  if ((tag = strstr(str,"Url1=\"")) != ((char *)((void *)0))) {
    tag += strlen("Url1=\"");
    c = strchr(tag,'"');
    if (c != ((char *)((void *)0))) {
      if ((obj -> url1) != ((char *)((void *)0))) 
        g_free((obj -> url1));
      obj -> url1 = g_strndup(tag,(c - tag));
    }
  };
/* If we are missing any of the required elements then discard the object */
  if (((((obj -> creator) == ((char *)((void *)0))) || ((obj -> size) == 0)) || ((obj -> type) == 0)) || ((obj -> sha1d) == ((char *)((void *)0)))) {
    purple_debug_error("msn","Discarding invalid msnobj: \'%s\'\n",str);
    msn_object_destroy(obj);
    return 0;
  }
  if (((obj -> location) == ((char *)((void *)0))) || ((obj -> friendly) == ((char *)((void *)0)))) {
/* Location/friendly are required for non-buddyicon objects */
    if ((obj -> type) != MSN_OBJECT_USERTILE) {
      purple_debug_error("msn","Discarding invalid msnobj: \'%s\'\n",str);
      msn_object_destroy(obj);
      return 0;
/* Buddy icon object can contain Url/Url1 instead */
    }
    else if (((obj -> url) == ((char *)((void *)0))) || ((obj -> url1) == ((char *)((void *)0)))) {
      purple_debug_error("msn","Discarding invalid msnobj: \'%s\'\n",str);
      msn_object_destroy(obj);
      return 0;
    }
  }
  return obj;
}

MsnObject *msn_object_new_from_image(PurpleStoredImage *img,const char *location,const char *creator,MsnObjectType type)
{
  MsnObject *msnobj;
  PurpleCipherContext *ctx;
  char *buf;
  gconstpointer data;
  size_t size;
  char *base64;
  unsigned char digest[20UL];
  msnobj = ((MsnObject *)((void *)0));
  if (img == ((PurpleStoredImage *)((void *)0))) 
    return msnobj;
  size = purple_imgstore_get_size(img);
  data = purple_imgstore_get_data(img);
/* New object */
  msnobj = msn_object_new();
  msn_object_set_local(msnobj);
  msn_object_set_type(msnobj,type);
  msn_object_set_location(msnobj,location);
  msn_object_set_creator(msnobj,creator);
  msn_object_set_image(msnobj,img);
/* Compute the SHA1D field. */
  memset(digest,0,(sizeof(digest)));
  ctx = purple_cipher_context_new_by_name("sha1",0);
  purple_cipher_context_append(ctx,data,size);
  purple_cipher_context_digest(ctx,(sizeof(digest)),digest,0);
  base64 = purple_base64_encode(digest,(sizeof(digest)));
  msn_object_set_sha1d(msnobj,base64);
  g_free(base64);
  msn_object_set_size(msnobj,size);
/* Compute the SHA1C field. */
  buf = g_strdup_printf("Creator%sSize%dType%dLocation%sFriendly%sSHA1D%s",msn_object_get_creator(msnobj),msn_object_get_size(msnobj),msn_object_get_type(msnobj),msn_object_get_location(msnobj),msn_object_get_friendly(msnobj),msn_object_get_sha1d(msnobj));
  memset(digest,0,(sizeof(digest)));
  purple_cipher_context_reset(ctx,0);
  purple_cipher_context_append(ctx,((const guchar *)buf),strlen(buf));
  purple_cipher_context_digest(ctx,(sizeof(digest)),digest,0);
  purple_cipher_context_destroy(ctx);
  g_free(buf);
  base64 = purple_base64_encode(digest,(sizeof(digest)));
  msn_object_set_sha1c(msnobj,base64);
  g_free(base64);
  return msnobj;
}

void msn_object_destroy(MsnObject *obj)
{
  do {
    if (obj != ((MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return ;
    };
  }while (0);
  g_free((obj -> creator));
  g_free((obj -> location));
  g_free((obj -> friendly));
  g_free((obj -> sha1d));
  g_free((obj -> sha1c));
  g_free((obj -> url));
  g_free((obj -> url1));
  purple_imgstore_unref((obj -> img));
  if ((obj -> local) != 0) 
    local_objs = g_list_remove(local_objs,obj);
  g_free(obj);
}

char *msn_object_to_string(const MsnObject *obj)
{
  char *str;
  const char *sha1c;
  do {
    if (obj != ((const MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return 0;
    };
  }while (0);
  sha1c = msn_object_get_sha1c(obj);
  str = g_strdup_printf("<msnobj Creator=\"%s\" Size=\"%d\" Type=\"%d\" Location=\"%s\" Friendly=\"%s\" SHA1D=\"%s\"%s%s%s/>",msn_object_get_creator(obj),msn_object_get_size(obj),msn_object_get_type(obj),msn_object_get_location(obj),msn_object_get_friendly(obj),msn_object_get_sha1d(obj),((sha1c != 0)?" SHA1C=\"" : ""),((sha1c != 0)?sha1c : ""),((sha1c != 0)?"\"" : ""));
  return str;
}

void msn_object_set_creator(MsnObject *obj,const char *creator)
{
  do {
    if (obj != ((MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return ;
    };
  }while (0);
  g_free((obj -> creator));
  obj -> creator = g_strdup(creator);
}

void msn_object_set_size(MsnObject *obj,int size)
{
  do {
    if (obj != ((MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return ;
    };
  }while (0);
  obj -> size = size;
}

void msn_object_set_type(MsnObject *obj,MsnObjectType type)
{
  do {
    if (obj != ((MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return ;
    };
  }while (0);
  obj -> type = type;
}

void msn_object_set_location(MsnObject *obj,const char *location)
{
  do {
    if (obj != ((MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return ;
    };
  }while (0);
  g_free((obj -> location));
  obj -> location = g_strdup(location);
}

void msn_object_set_friendly(MsnObject *obj,const char *friendly)
{
  do {
    if (obj != ((MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return ;
    };
  }while (0);
  g_free((obj -> friendly));
  obj -> friendly = g_strdup(friendly);
}

void msn_object_set_sha1d(MsnObject *obj,const char *sha1d)
{
  do {
    if (obj != ((MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return ;
    };
  }while (0);
  g_free((obj -> sha1d));
  obj -> sha1d = g_strdup(sha1d);
}

void msn_object_set_sha1c(MsnObject *obj,const char *sha1c)
{
  do {
    if (obj != ((MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return ;
    };
  }while (0);
  g_free((obj -> sha1c));
  obj -> sha1c = g_strdup(sha1c);
}

void msn_object_set_url(MsnObject *obj,const char *url)
{
  do {
    if (obj != ((MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return ;
    };
  }while (0);
  g_free((obj -> url));
  obj -> url = g_strdup(url);
}

void msn_object_set_url1(MsnObject *obj,const char *url)
{
  do {
    if (obj != ((MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return ;
    };
  }while (0);
  g_free((obj -> url1));
  obj -> url1 = g_strdup(url);
}

const char *msn_object_get_creator(const MsnObject *obj)
{
  do {
    if (obj != ((const MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return 0;
    };
  }while (0);
  return (obj -> creator);
}

int msn_object_get_size(const MsnObject *obj)
{
  do {
    if (obj != ((const MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return 0;
    };
  }while (0);
  return obj -> size;
}

MsnObjectType msn_object_get_type(const MsnObject *obj)
{
  do {
    if (obj != ((const MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return MSN_OBJECT_UNKNOWN;
    };
  }while (0);
  return obj -> type;
}

const char *msn_object_get_location(const MsnObject *obj)
{
  do {
    if (obj != ((const MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return 0;
    };
  }while (0);
  return (obj -> location);
}

const char *msn_object_get_friendly(const MsnObject *obj)
{
  do {
    if (obj != ((const MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return 0;
    };
  }while (0);
  return (obj -> friendly);
}

const char *msn_object_get_sha1d(const MsnObject *obj)
{
  do {
    if (obj != ((const MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return 0;
    };
  }while (0);
  return (obj -> sha1d);
}

const char *msn_object_get_sha1c(const MsnObject *obj)
{
  do {
    if (obj != ((const MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return 0;
    };
  }while (0);
  return (obj -> sha1c);
}

const char *msn_object_get_sha1(const MsnObject *obj)
{
  do {
    if (obj != ((const MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return 0;
    };
  }while (0);
  if ((obj -> sha1c) != ((char *)((void *)0))) {
    return (obj -> sha1c);
  }
  else {
    return (obj -> sha1d);
  }
}

const char *msn_object_get_url(const MsnObject *obj)
{
  do {
    if (obj != ((const MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return 0;
    };
  }while (0);
  return (obj -> url);
}

const char *msn_object_get_url1(const MsnObject *obj)
{
  do {
    if (obj != ((const MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return 0;
    };
  }while (0);
  return (obj -> url1);
}

MsnObject *msn_object_find_local(const char *sha1)
{
  GList *l;
  do {
    if (sha1 != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"sha1 != NULL");
      return 0;
    };
  }while (0);
  for (l = local_objs; l != ((GList *)((void *)0)); l = (l -> next)) {
    MsnObject *local_obj = (l -> data);
    if (!(strcmp(msn_object_get_sha1(local_obj),sha1) != 0)) 
      return local_obj;
  }
  return 0;
}

void msn_object_set_local(MsnObject *obj)
{
  do {
    if (obj != ((MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return ;
    };
  }while (0);
  obj -> local = (!0);
  local_objs = g_list_append(local_objs,obj);
}

void msn_object_set_image(MsnObject *obj,PurpleStoredImage *img)
{
  do {
    if (obj != ((MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return ;
    };
  }while (0);
  do {
    if (img != ((PurpleStoredImage *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"img != NULL");
      return ;
    };
  }while (0);
/* obj->local = TRUE; */
  purple_imgstore_unref((obj -> img));
  obj -> img = purple_imgstore_ref(img);
}

PurpleStoredImage *msn_object_get_image(const MsnObject *obj)
{
  MsnObject *local_obj;
  do {
    if (obj != ((const MsnObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"obj != NULL");
      return 0;
    };
  }while (0);
  local_obj = msn_object_find_local(msn_object_get_sha1(obj));
  if (local_obj != ((MsnObject *)((void *)0))) 
    return local_obj -> img;
  return 0;
}
