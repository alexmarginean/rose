/**
 * @file state.c State functions and definitions
 *
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "debug.h"
#include "core.h"
#include "notification.h"
#include "state.h"
static const char *away_text[] = {("Available"), ("Available"), ("Busy"), ("Idle"), ("Be Right Back"), ("Away From Computer"), ("On The Phone"), ("Out To Lunch"), ("Available"), ("Available")};
/*
 * WLM media PSM info build prcedure
 *
 * Result can like:
 *	<CurrentMedia>\0Music\01\0{0} - {1}\0Song Title\0Song Artist\0Song Album\0\0</CurrentMedia>\
 *	<CurrentMedia>\0Games\01\0Playing {0}\0Game Name\0</CurrentMedia>\
 *	<CurrentMedia>\0Office\01\0Office Message\0Office App Name\0</CurrentMedia>"
 */

static char *msn_build_psm(const char *psmstr,const char *mediastr,const char *guidstr,guint protocol_ver)
{
  xmlnode *dataNode;
  xmlnode *psmNode;
  xmlnode *mediaNode;
  xmlnode *guidNode;
  char *result;
  int length;
  dataNode = xmlnode_new("Data");
  psmNode = xmlnode_new("PSM");
  if (psmstr != ((const char *)((void *)0))) {
    xmlnode_insert_data(psmNode,psmstr,(-1));
  }
  xmlnode_insert_child(dataNode,psmNode);
  mediaNode = xmlnode_new("CurrentMedia");
  if (mediastr != ((const char *)((void *)0))) {
    xmlnode_insert_data(mediaNode,mediastr,(-1));
  }
  xmlnode_insert_child(dataNode,mediaNode);
  guidNode = xmlnode_new("MachineGuid");
  if (guidstr != ((const char *)((void *)0))) {
    xmlnode_insert_data(guidNode,guidstr,(-1));
  }
  xmlnode_insert_child(dataNode,guidNode);
  if (protocol_ver >= 16) {
/* TODO: What is this for? */
    xmlnode *ddpNode = xmlnode_new("DDP");
    xmlnode_insert_child(dataNode,ddpNode);
  }
  result = xmlnode_to_str(dataNode,&length);
  xmlnode_free(dataNode);
  return result;
}
/* get the CurrentMedia info from the XML node */

char *msn_get_currentmedia(xmlnode *payloadNode)
{
  xmlnode *currentmediaNode;
  char *currentmedia;
  purple_debug_info("msn","Get CurrentMedia\n");
  currentmediaNode = xmlnode_get_child(payloadNode,"CurrentMedia");
  if (currentmediaNode == ((xmlnode *)((void *)0))) {
    purple_debug_info("msn","No CurrentMedia Node\n");
    return 0;
  }
  currentmedia = xmlnode_get_data(currentmediaNode);
  return currentmedia;
}
/* Get the PSM info from the XML node */

char *msn_get_psm(xmlnode *payloadNode)
{
  xmlnode *psmNode;
  char *psm;
  purple_debug_info("msn","msn get PSM\n");
  psmNode = xmlnode_get_child(payloadNode,"PSM");
  if (psmNode == ((xmlnode *)((void *)0))) {
    purple_debug_info("msn","No PSM status Node\n");
    return 0;
  }
  psm = xmlnode_get_data(psmNode);
  return psm;
}

static char *create_media_string(PurplePresence *presence)
{
  const char *title;
  const char *game;
  const char *office;
  char *ret;
  PurpleStatus *status = purple_presence_get_status(presence,"tune");
  if (!(status != 0) || !(purple_status_is_active(status) != 0)) 
    return 0;
  title = purple_status_get_attr_string(status,"tune_title");
  game = purple_status_get_attr_string(status,"game");
  office = purple_status_get_attr_string(status,"office");
  if ((title != 0) && (( *title) != 0)) {
    const char *artist = purple_status_get_attr_string(status,"tune_artist");
    const char *album = purple_status_get_attr_string(status,"tune_album");
    ret = g_strdup_printf("WMP\\0Music\\01\\0{0}%s%s\\0%s\\0%s\\0%s\\0",((artist != 0)?" - {1}" : ""),((album != 0)?" ({2})" : ""),title,((artist != 0)?artist : ""),((album != 0)?album : ""));
  }
  else if ((game != 0) && (( *game) != 0)) 
    ret = g_strdup_printf("\\0Games\\01\\0Playing {0}\\0%s\\0",game);
  else if ((office != 0) && (( *office) != 0)) 
    ret = g_strdup_printf("\\0Office\\01\\0Editing {0}\\0%s\\0",office);
  else 
    ret = ((char *)((void *)0));
  return ret;
}
/* set the MSN's PSM info,Currently Read from the status Line
 * Thanks for Cris Code
 */

static void msn_set_psm(MsnSession *session)
{
  PurpleAccount *account;
  PurplePresence *presence;
  PurpleStatus *status;
  char *payload;
  const char *statusline;
  gchar *statusline_stripped;
  gchar *media = (gchar *)((void *)0);
  do {
    if (session != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return ;
    };
  }while (0);
  do {
    if ((session -> notification) != ((MsnNotification *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session->notification != NULL");
      return ;
    };
  }while (0);
  account = (session -> account);
/* Get the PSM string from Purple's Status Line */
  presence = purple_account_get_presence(account);
  status = purple_presence_get_active_status(presence);
  statusline = purple_status_get_attr_string(status,"message");
/* MSN expects plain text, not HTML */
  statusline_stripped = purple_markup_strip_html(statusline);
  media = create_media_string(presence);
  g_free((session -> psm));
  session -> psm = msn_build_psm(statusline_stripped,media,(session -> guid),(session -> protocol_ver));
  payload = (session -> psm);
  msn_notification_send_uux(session,payload);
  g_free(statusline_stripped);
  g_free(media);
}

void msn_change_status(MsnSession *session)
{
  PurpleAccount *account;
  MsnCmdProc *cmdproc;
  MsnTransaction *trans;
  MsnUser *user;
  MsnObject *msnobj;
  const char *state_text;
  GHashTable *ui_info = purple_core_get_ui_info();
  MsnClientCaps caps = ((MSN_CLIENT_VER_9_0 << 24) | (MSN_CAP_PACKET | MSN_CAP_INK_GIF | MSN_CAP_VOICEIM));
  do {
    if (session != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return ;
    };
  }while (0);
  do {
    if ((session -> notification) != ((MsnNotification *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session->notification != NULL");
      return ;
    };
  }while (0);
/* set client caps based on what the UI tells us it is... */
  if (ui_info != 0) {
    const gchar *client_type = (g_hash_table_lookup(ui_info,"client_type"));
    if (client_type != 0) {
      if ((strcmp(client_type,"phone") == 0) || (strcmp(client_type,"handheld") == 0)) {
        caps |= MSN_CAP_VIA_MOBILE;
      }
      else if (strcmp(client_type,"web") == 0) {
        caps |= MSN_CAP_VIA_WEBIM;
      }
      else if (strcmp(client_type,"bot") == 0) {
        caps |= MSN_CAP_BOT;
      }
/* MSN doesn't a "console" type...
			 What, they have no ncurses UI? :-) */
    }
  }
  account = (session -> account);
  cmdproc = ( *(session -> notification)).cmdproc;
  user = (session -> user);
  state_text = msn_state_get_text(msn_state_from_account(account));
/* If we're not logged in yet, don't send the status to the server,
	 * it will be sent when login completes
	 */
  if (!((session -> logged_in) != 0)) 
    return ;
  msn_set_psm(session);
  msnobj = msn_user_get_object(user);
  if (msnobj == ((MsnObject *)((void *)0))) {
    trans = msn_transaction_new(cmdproc,"CHG","%s %u:%02u 0",state_text,caps,0);
  }
  else {
    char *msnobj_str;
    msnobj_str = msn_object_to_string(msnobj);
    trans = msn_transaction_new(cmdproc,"CHG","%s %u:%02u %s",state_text,caps,0,purple_url_encode(msnobj_str));
    g_free(msnobj_str);
  }
  msn_cmdproc_send_trans(cmdproc,trans);
}

const char *msn_away_get_text(MsnAwayType type)
{
  do {
    if (type <= MSN_HIDDEN) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type <= MSN_HIDDEN");
      return 0;
    };
  }while (0);
  return (const char *)(dgettext("pidgin",away_text[type]));
}

const char *msn_state_get_text(MsnAwayType state)
{
  static char *status_text[] = {("NLN"), ("NLN"), ("BSY"), ("IDL"), ("BRB"), ("AWY"), ("PHN"), ("LUN"), ("HDN"), ("HDN")};
  return status_text[state];
}

MsnAwayType msn_state_from_account(PurpleAccount *account)
{
  MsnAwayType msnstatus;
  PurplePresence *presence;
  PurpleStatus *status;
  const char *status_id;
  presence = purple_account_get_presence(account);
  status = purple_presence_get_active_status(presence);
  status_id = purple_status_get_id(status);
  if (!(strcmp(status_id,"away") != 0)) 
    msnstatus = MSN_AWAY;
  else if (!(strcmp(status_id,"brb") != 0)) 
    msnstatus = MSN_BRB;
  else if (!(strcmp(status_id,"busy") != 0)) 
    msnstatus = MSN_BUSY;
  else if (!(strcmp(status_id,"phone") != 0)) 
    msnstatus = MSN_PHONE;
  else if (!(strcmp(status_id,"lunch") != 0)) 
    msnstatus = MSN_LUNCH;
  else if (!(strcmp(status_id,"invisible") != 0)) 
    msnstatus = MSN_HIDDEN;
  else 
    msnstatus = MSN_ONLINE;
  if ((msnstatus == MSN_ONLINE) && (purple_presence_is_idle(presence) != 0)) 
    msnstatus = MSN_IDLE;
  return msnstatus;
}
