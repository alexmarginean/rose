/**
 * @file servconn.c Server connection functions
 *
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "debug.h"
#include "servconn.h"
#include "error.h"
static void read_cb(gpointer data,gint source,PurpleInputCondition cond);
static void servconn_timeout_renew(MsnServConn *servconn);
/**************************************************************************
 * Main
 **************************************************************************/

MsnServConn *msn_servconn_new(MsnSession *session,MsnServConnType type)
{
  MsnServConn *servconn;
  do {
    if (session != ((MsnSession *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session != NULL");
      return 0;
    };
  }while (0);
  servconn = ((MsnServConn *)(g_malloc0_n(1,(sizeof(MsnServConn )))));
  servconn -> type = type;
  servconn -> session = session;
  servconn -> cmdproc = msn_cmdproc_new(session);
  ( *(servconn -> cmdproc)).servconn = servconn;
  servconn -> httpconn = msn_httpconn_new(servconn);
  servconn -> num = session -> servconns_count++;
  servconn -> tx_buf = purple_circ_buffer_new(8192);
  servconn -> tx_handler = 0;
  servconn -> timeout_sec = 0;
  servconn -> timeout_handle = 0;
  servconn -> fd = -1;
  return servconn;
}

void msn_servconn_destroy(MsnServConn *servconn)
{
  do {
    if (servconn != ((MsnServConn *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"servconn != NULL");
      return ;
    };
  }while (0);
  if ((servconn -> processing) != 0) {
    servconn -> wasted = (!0);
    return ;
  }
  msn_servconn_disconnect(servconn);
  if ((servconn -> destroy_cb) != 0) 
    ( *(servconn -> destroy_cb))(servconn);
  if ((servconn -> httpconn) != ((MsnHttpConn *)((void *)0))) 
    msn_httpconn_destroy((servconn -> httpconn));
  g_free((servconn -> host));
  purple_circ_buffer_destroy((servconn -> tx_buf));
  if ((servconn -> tx_handler) > 0) 
    purple_input_remove((servconn -> tx_handler));
  if ((servconn -> timeout_handle) > 0) 
    purple_timeout_remove((servconn -> timeout_handle));
  msn_cmdproc_destroy((servconn -> cmdproc));
  g_free(servconn);
}

void msn_servconn_set_connect_cb(MsnServConn *servconn,void (*connect_cb)(MsnServConn *))
{
  do {
    if (servconn != ((MsnServConn *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"servconn != NULL");
      return ;
    };
  }while (0);
  servconn -> connect_cb = connect_cb;
}

void msn_servconn_set_disconnect_cb(MsnServConn *servconn,void (*disconnect_cb)(MsnServConn *))
{
  do {
    if (servconn != ((MsnServConn *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"servconn != NULL");
      return ;
    };
  }while (0);
  servconn -> disconnect_cb = disconnect_cb;
}

void msn_servconn_set_destroy_cb(MsnServConn *servconn,void (*destroy_cb)(MsnServConn *))
{
  do {
    if (servconn != ((MsnServConn *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"servconn != NULL");
      return ;
    };
  }while (0);
  servconn -> destroy_cb = destroy_cb;
}
/**************************************************************************
 * Utility
 **************************************************************************/

void msn_servconn_got_error(MsnServConn *servconn,MsnServConnError error,const char *reason)
{
  MsnSession *session = (servconn -> session);
  MsnServConnType type = (servconn -> type);
  const char *names[] = {("Notification"), ("Switchboard")};
  const char *name;
  name = names[type];
  if (reason == ((const char *)((void *)0))) {
    switch(error){
      case MSN_SERVCONN_ERROR_CONNECT:
{
        reason = ((const char *)(dgettext("pidgin","Unable to connect")));
        break; 
      }
      case MSN_SERVCONN_ERROR_WRITE:
{
        reason = ((const char *)(dgettext("pidgin","Writing error")));
        break; 
      }
      case MSN_SERVCONN_ERROR_READ:
{
        reason = ((const char *)(dgettext("pidgin","Reading error")));
        break; 
      }
      default:
{
        reason = ((const char *)(dgettext("pidgin","Unknown error")));
        break; 
      }
    }
  }
  purple_debug_error("msn","Connection error from %s server (%s): %s\n",name,(servconn -> host),reason);
  if (type == MSN_SERVCONN_SB) {
    MsnSwitchBoard *swboard;
    swboard = ( *(servconn -> cmdproc)).data;
    if (swboard != ((MsnSwitchBoard *)((void *)0))) 
      swboard -> error = MSN_SB_ERROR_CONNECTION;
  }
/* servconn->disconnect_cb may destroy servconn, so don't use it again */
  msn_servconn_disconnect(servconn);
  if (type == MSN_SERVCONN_NS) {
    char *tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Connection error from %s server:\n%s"))),name,reason);
    msn_session_set_error(session,MSN_ERROR_SERVCONN,tmp);
    g_free(tmp);
  }
}
/**************************************************************************
 * Connect
 **************************************************************************/

static void connect_cb(gpointer data,gint source,const char *error_message)
{
  MsnServConn *servconn;
  servconn = data;
  servconn -> connect_data = ((PurpleProxyConnectData *)((void *)0));
  servconn -> fd = source;
  if (source >= 0) {
    servconn -> connected = (!0);
/* Someone wants to know we connected. */
    ( *(servconn -> connect_cb))(servconn);
    servconn -> inpa = (purple_input_add((servconn -> fd),PURPLE_INPUT_READ,read_cb,data));
    servconn_timeout_renew(servconn);
  }
  else {
    purple_debug_error("msn","Connection error: %s\n",error_message);
    msn_servconn_got_error(servconn,MSN_SERVCONN_ERROR_CONNECT,error_message);
  }
}

gboolean msn_servconn_connect(MsnServConn *servconn,const char *host,int port,gboolean force)
{
  MsnSession *session;
  do {
    if (servconn != ((MsnServConn *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"servconn != NULL");
      return 0;
    };
  }while (0);
  do {
    if (host != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"host != NULL");
      return 0;
    };
  }while (0);
  do {
    if (port > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"port > 0");
      return 0;
    };
  }while (0);
  session = (servconn -> session);
  if ((servconn -> connected) != 0) 
    msn_servconn_disconnect(servconn);
  g_free((servconn -> host));
  servconn -> host = g_strdup(host);
  if ((session -> http_method) != 0) {
/* HTTP Connection. */
    if (!(( *(servconn -> httpconn)).connected != 0) || (force != 0)) 
      if (!(msn_httpconn_connect((servconn -> httpconn),host,port) != 0)) 
        return 0;
    servconn -> connected = (!0);
    ( *(servconn -> httpconn)).virgin = (!0);
    servconn_timeout_renew(servconn);
/* Someone wants to know we connected. */
    ( *(servconn -> connect_cb))(servconn);
    return (!0);
  }
  servconn -> connect_data = purple_proxy_connect(0,(session -> account),host,port,connect_cb,servconn);
  return (servconn -> connect_data) != ((PurpleProxyConnectData *)((void *)0));
}

void msn_servconn_disconnect(MsnServConn *servconn)
{
  do {
    if (servconn != ((MsnServConn *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"servconn != NULL");
      return ;
    };
  }while (0);
  if ((servconn -> connect_data) != ((PurpleProxyConnectData *)((void *)0))) {
    purple_proxy_connect_cancel((servconn -> connect_data));
    servconn -> connect_data = ((PurpleProxyConnectData *)((void *)0));
  }
  if (!((servconn -> connected) != 0)) {
/* We could not connect. */
    if ((servconn -> disconnect_cb) != ((void (*)(MsnServConn *))((void *)0))) 
      ( *(servconn -> disconnect_cb))(servconn);
    return ;
  }
  if (( *(servconn -> session)).http_method != 0) {
/* Fake disconnection. */
    if ((servconn -> disconnect_cb) != ((void (*)(MsnServConn *))((void *)0))) 
      ( *(servconn -> disconnect_cb))(servconn);
    return ;
  }
  if ((servconn -> inpa) > 0) {
    purple_input_remove((servconn -> inpa));
    servconn -> inpa = 0;
  }
  if ((servconn -> timeout_handle) > 0) {
    purple_timeout_remove((servconn -> timeout_handle));
    servconn -> timeout_handle = 0;
  }
  close((servconn -> fd));
  servconn -> rx_buf = ((char *)((void *)0));
  servconn -> rx_len = 0;
  servconn -> payload_len = 0;
  servconn -> connected = 0;
  if ((servconn -> disconnect_cb) != ((void (*)(MsnServConn *))((void *)0))) 
    ( *(servconn -> disconnect_cb))(servconn);
}

static gboolean servconn_idle_timeout_cb(MsnServConn *servconn)
{
  servconn -> timeout_handle = 0;
  msn_servconn_disconnect(servconn);
  return 0;
}

static void servconn_timeout_renew(MsnServConn *servconn)
{
  if ((servconn -> timeout_handle) != 0U) {
    purple_timeout_remove((servconn -> timeout_handle));
    servconn -> timeout_handle = 0;
  }
  if (((servconn -> connected) != 0) && ((servconn -> timeout_sec) != 0U)) {
    servconn -> timeout_handle = purple_timeout_add_seconds((servconn -> timeout_sec),((GSourceFunc )servconn_idle_timeout_cb),servconn);
  }
}

void msn_servconn_set_idle_timeout(MsnServConn *servconn,guint seconds)
{
  servconn -> timeout_sec = seconds;
  if ((servconn -> connected) != 0) 
    servconn_timeout_renew(servconn);
}

static void servconn_write_cb(gpointer data,gint source,PurpleInputCondition cond)
{
  MsnServConn *servconn = data;
  gssize ret;
  int writelen;
  writelen = (purple_circ_buffer_get_max_read((servconn -> tx_buf)));
  if (writelen == 0) {
    purple_input_remove((servconn -> tx_handler));
    servconn -> tx_handler = 0;
    return ;
  }
  ret = write((servconn -> fd),( *(servconn -> tx_buf)).outptr,writelen);
  if ((ret < 0) && ( *__errno_location() == 11)) 
    return ;
  else if (ret <= 0) {
    msn_servconn_got_error(servconn,MSN_SERVCONN_ERROR_WRITE,0);
    return ;
  }
  purple_circ_buffer_mark_read((servconn -> tx_buf),ret);
  servconn_timeout_renew(servconn);
}

gssize msn_servconn_write(MsnServConn *servconn,const char *buf,size_t len)
{
  gssize ret = 0;
  do {
    if (servconn != ((MsnServConn *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"servconn != NULL");
      return 0;
    };
  }while (0);
  if (!(( *(servconn -> session)).http_method != 0)) {
    if ((servconn -> tx_handler) == 0) {
      switch(servconn -> type){
        case MSN_SERVCONN_NS:
{
        }
        case MSN_SERVCONN_SB:
{
          ret = write((servconn -> fd),buf,len);
          break; 
        }
#if 0
#endif
        default:
{
          ret = write((servconn -> fd),buf,len);
          break; 
        }
      }
    }
    else {
      ret = (-1);
       *__errno_location() = 11;
    }
    if ((ret < 0) && ( *__errno_location() == 11)) 
      ret = 0;
    if ((ret >= 0) && (ret < len)) {
      if ((servconn -> tx_handler) == 0) 
        servconn -> tx_handler = purple_input_add((servconn -> fd),PURPLE_INPUT_WRITE,servconn_write_cb,servconn);
      purple_circ_buffer_append((servconn -> tx_buf),(buf + ret),(len - ret));
    }
  }
  else {
    ret = msn_httpconn_write((servconn -> httpconn),buf,len);
  }
  if (ret == (-1)) {
    msn_servconn_got_error(servconn,MSN_SERVCONN_ERROR_WRITE,0);
  }
  servconn_timeout_renew(servconn);
  return ret;
}

static void read_cb(gpointer data,gint source,PurpleInputCondition cond)
{
  MsnServConn *servconn;
  char buf[8192UL];
  gssize len;
  servconn = data;
  if ((servconn -> type) == MSN_SERVCONN_NS) 
    ( *( *( *(servconn -> session)).account).gc).last_received = time(0);
  len = read((servconn -> fd),buf,(sizeof(buf) - 1));
  if ((len < 0) && ( *__errno_location() == 11)) 
    return ;
  if (len <= 0) {
    purple_debug_error("msn","servconn %03d read error, len: %li, errno: %d, error: %s\n",(servconn -> num),len, *__errno_location(),g_strerror( *__errno_location()));
    msn_servconn_got_error(servconn,MSN_SERVCONN_ERROR_READ,0);
    return ;
  }
  buf[len] = 0;
  servconn -> rx_buf = (g_realloc((servconn -> rx_buf),((len + (servconn -> rx_len)) + 1)));
  memcpy(((servconn -> rx_buf) + (servconn -> rx_len)),buf,(len + 1));
  servconn -> rx_len += len;
  servconn = msn_servconn_process_data(servconn);
  if (servconn != 0) 
    servconn_timeout_renew(servconn);
}

MsnServConn *msn_servconn_process_data(MsnServConn *servconn)
{
  char *cur;
  char *end;
  char *old_rx_buf;
  int cur_len;
  end = (old_rx_buf = (servconn -> rx_buf));
  servconn -> processing = (!0);
{
    do {
      cur = end;
      if ((servconn -> payload_len) != 0UL) {
        if ((servconn -> payload_len) > (servconn -> rx_len)) 
/* The payload is still not complete. */
          break; 
        cur_len = (servconn -> payload_len);
        end += cur_len;
      }
      else {
        end = strstr(cur,"\r\n");
        if (end == ((char *)((void *)0))) 
/* The command is still not complete. */
          break; 
         *end = 0;
        end += 2;
        cur_len = (end - cur);
      }
      servconn -> rx_len -= cur_len;
      if ((servconn -> payload_len) != 0UL) {
        msn_cmdproc_process_payload((servconn -> cmdproc),cur,cur_len);
        servconn -> payload_len = 0;
      }
      else {
        msn_cmdproc_process_cmd_text((servconn -> cmdproc),cur);
        servconn -> payload_len = ( *( *(servconn -> cmdproc)).last_cmd).payload_len;
      }
    }while ((((servconn -> connected) != 0) && !((servconn -> wasted) != 0)) && ((servconn -> rx_len) > 0));
  }
  if (((servconn -> connected) != 0) && !((servconn -> wasted) != 0)) {
    if ((servconn -> rx_len) > 0) 
      servconn -> rx_buf = (g_memdup(cur,(servconn -> rx_len)));
    else 
      servconn -> rx_buf = ((char *)((void *)0));
  }
  servconn -> processing = 0;
  if ((servconn -> wasted) != 0) {
    msn_servconn_destroy(servconn);
    servconn = ((MsnServConn *)((void *)0));
  }
  g_free(old_rx_buf);
  return servconn;
}
#if 0
#if 0
#else
#endif
#ifndef _WIN32
#endif
#endif
