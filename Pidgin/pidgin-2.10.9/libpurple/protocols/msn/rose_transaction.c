/**
 * @file transaction.c MSN transaction functions
 *
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "debug.h"
#include "msn.h"
#include "transaction.h"

MsnTransaction *msn_transaction_new(MsnCmdProc *cmdproc,const char *command,const char *format,... )
{
  MsnTransaction *trans;
  va_list arg;
  do {
    if (command != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"command != NULL");
      return 0;
    };
  }while (0);
  trans = ((MsnTransaction *)(g_malloc0_n(1,(sizeof(MsnTransaction )))));
  trans -> cmdproc = cmdproc;
  trans -> command = g_strdup(command);
  trans -> saveable = (!0);
  if (format != ((const char *)((void *)0))) {
    va_start(arg,format);
    trans -> params = g_strdup_vprintf(format,arg);
    va_end(arg);
  }
/* trans->queue = g_queue_new(); */
  return trans;
}

void msn_transaction_destroy(MsnTransaction *trans)
{
  do {
    if (trans != ((MsnTransaction *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"trans != NULL");
      return ;
    };
  }while (0);
  g_free((trans -> command));
  g_free((trans -> params));
  g_free((trans -> payload));
  if ((trans -> data_free) != 0) 
    ( *(trans -> data_free))((trans -> data));
#if 0
#endif
#if 0
#endif
  if (((trans -> callbacks) != ((GHashTable *)((void *)0))) && ((trans -> has_custom_callbacks) != 0)) 
    g_hash_table_destroy((trans -> callbacks));
  if ((trans -> timer) != 0U) 
    purple_timeout_remove((trans -> timer));
  g_free(trans);
}

char *msn_transaction_to_string(MsnTransaction *trans)
{
  char *str;
  do {
    if (trans != ((MsnTransaction *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"trans != NULL");
      return 0;
    };
  }while (0);
  if ((trans -> params) != ((char *)((void *)0))) 
    str = g_strdup_printf("%s %u %s\r\n",(trans -> command),(trans -> trId),(trans -> params));
  else if ((trans -> saveable) != 0) 
    str = g_strdup_printf("%s %u\r\n",(trans -> command),(trans -> trId));
  else 
    str = g_strdup_printf("%s\r\n",(trans -> command));
  return str;
}

void msn_transaction_queue_cmd(MsnTransaction *trans,MsnCommand *cmd)
{
  purple_debug_info("msn","queueing command.\n");
  trans -> pendent_cmd = cmd;
  msn_command_ref(cmd);
}

void msn_transaction_unqueue_cmd(MsnTransaction *trans,MsnCmdProc *cmdproc)
{
  MsnCommand *cmd;
  if (!(( *(cmdproc -> servconn)).connected != 0)) 
    return ;
  purple_debug_info("msn","unqueueing command.\n");
  cmd = (trans -> pendent_cmd);
  do {
    if (cmd != ((MsnCommand *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cmd != NULL");
      return ;
    };
  }while (0);
  msn_cmdproc_process_cmd(cmdproc,cmd);
  msn_command_unref(cmd);
  trans -> pendent_cmd = ((MsnCommand *)((void *)0));
}
#if 0
#endif

void msn_transaction_set_payload(MsnTransaction *trans,const char *payload,int payload_len)
{
  do {
    if (trans != ((MsnTransaction *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"trans != NULL");
      return ;
    };
  }while (0);
  do {
    if (payload != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"payload != NULL");
      return ;
    };
  }while (0);
  trans -> payload = g_strdup(payload);
  trans -> payload_len = ((payload_len != 0)?payload_len : strlen((trans -> payload)));
}

void msn_transaction_set_data(MsnTransaction *trans,void *data)
{
  do {
    if (trans != ((MsnTransaction *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"trans != NULL");
      return ;
    };
  }while (0);
  trans -> data = data;
}

void msn_transaction_set_data_free(MsnTransaction *trans,GDestroyNotify fn)
{
  do {
    if (trans != ((MsnTransaction *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"trans != NULL");
      return ;
    };
  }while (0);
  trans -> data_free = fn;
}

void msn_transaction_set_saveable(MsnTransaction *trans,gboolean saveable)
{
  do {
    if (trans != ((MsnTransaction *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"trans != NULL");
      return ;
    };
  }while (0);
  trans -> saveable = saveable;
}

void msn_transaction_add_cb(MsnTransaction *trans,char *answer,MsnTransCb cb)
{
  do {
    if (trans != ((MsnTransaction *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"trans != NULL");
      return ;
    };
  }while (0);
  do {
    if (answer != ((char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"answer != NULL");
      return ;
    };
  }while (0);
  if ((trans -> callbacks) == ((GHashTable *)((void *)0))) {
    trans -> has_custom_callbacks = (!0);
    trans -> callbacks = g_hash_table_new_full(g_str_hash,g_str_equal,0,0);
  }
  else if ((trans -> has_custom_callbacks) != !0) 
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","transaction.c",206,((const char *)__func__));
      return ;
    }while (0);
  g_hash_table_insert((trans -> callbacks),answer,cb);
}

static gboolean transaction_timeout(gpointer data)
{
  MsnTransaction *trans;
  trans = data;
  do {
    if (trans != ((MsnTransaction *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"trans != NULL");
      return 0;
    };
  }while (0);
#if 0
#endif
  trans -> timer = 0;
  if ((trans -> timeout_cb) != ((void (*)(MsnCmdProc *, MsnTransaction *))((void *)0))) 
    ( *(trans -> timeout_cb))((trans -> cmdproc),trans);
  return 0;
}

void msn_transaction_set_timeout_cb(MsnTransaction *trans,MsnTimeoutCb cb)
{
  if ((trans -> timer) != 0U) {
    purple_debug_error("msn","This shouldn\'t be happening\n");
    purple_timeout_remove((trans -> timer));
  }
  trans -> timeout_cb = cb;
  trans -> timer = purple_timeout_add_seconds(60,transaction_timeout,trans);
}

void msn_transaction_set_error_cb(MsnTransaction *trans,MsnErrorCb cb)
{
  trans -> error_cb = cb;
}
