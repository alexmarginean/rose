/**
 * @file error.c Error functions
 *
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "debug.h"
/* Masca: can we get rid of the sync issue dialog? */
#include "request.h"
#include "error.h"
typedef struct __unnamed_class___F0_L32_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L371R__Pe___variable_name_unknown_scope_and_name__scope__session__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__who__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__group__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L54R_variable_name_unknown_scope_and_name__scope__add {
MsnSession *session;
char *who;
char *group;
gboolean add;}MsnAddRemData;

const char *msn_error_get_text(unsigned int type,gboolean *debug)
{
  static char msg[256UL];
  const char *result;
   *debug = 0;
  switch(type){
    case 0:
{
      result = ((const char *)(dgettext("pidgin","Unable to parse message")));
       *debug = (!0);
      break; 
    }
    case 200:
{
      result = ((const char *)(dgettext("pidgin","Syntax Error (probably a client bug)")));
       *debug = (!0);
      break; 
    }
    case 201:
{
      result = ((const char *)(dgettext("pidgin","Invalid email address")));
      break; 
    }
    case 205:
{
      result = ((const char *)(dgettext("pidgin","User does not exist")));
      break; 
    }
    case 206:
{
      result = ((const char *)(dgettext("pidgin","Fully qualified domain name missing")));
      break; 
    }
    case 207:
{
      result = ((const char *)(dgettext("pidgin","Already logged in")));
      break; 
    }
    case 208:
{
      result = ((const char *)(dgettext("pidgin","Invalid username")));
      break; 
    }
    case 209:
{
      result = ((const char *)(dgettext("pidgin","Invalid friendly name")));
      break; 
    }
    case 210:
{
      result = ((const char *)(dgettext("pidgin","List full")));
      break; 
    }
    case 215:
{
      result = ((const char *)(dgettext("pidgin","Already there")));
       *debug = (!0);
      break; 
    }
    case 216:
{
      result = ((const char *)(dgettext("pidgin","Not on list")));
      break; 
    }
    case 217:
{
      result = ((const char *)(dgettext("pidgin","User is offline")));
      break; 
    }
    case 218:
{
      result = ((const char *)(dgettext("pidgin","Already in the mode")));
       *debug = (!0);
      break; 
    }
    case 219:
{
      result = ((const char *)(dgettext("pidgin","Already in opposite list")));
       *debug = (!0);
      break; 
    }
    case 223:
{
      result = ((const char *)(dgettext("pidgin","Too many groups")));
      break; 
    }
    case 224:
{
      result = ((const char *)(dgettext("pidgin","Invalid group")));
      break; 
    }
    case 225:
{
      result = ((const char *)(dgettext("pidgin","User not in group")));
      break; 
    }
    case 229:
{
      result = ((const char *)(dgettext("pidgin","Group name too long")));
      break; 
    }
    case 230:
{
      result = ((const char *)(dgettext("pidgin","Cannot remove group zero")));
       *debug = (!0);
      break; 
    }
    case 231:
{
      result = ((const char *)(dgettext("pidgin","Tried to add a user to a group that doesn\'t exist")));
      break; 
    }
    case 280:
{
      result = ((const char *)(dgettext("pidgin","Switchboard failed")));
       *debug = (!0);
      break; 
    }
    case 281:
{
      result = ((const char *)(dgettext("pidgin","Notify transfer failed")));
       *debug = (!0);
      break; 
    }
    case 300:
{
      result = ((const char *)(dgettext("pidgin","Required fields missing")));
       *debug = (!0);
      break; 
    }
    case 301:
{
      result = ((const char *)(dgettext("pidgin","Too many hits to a FND")));
       *debug = (!0);
      break; 
    }
    case 302:
{
      result = ((const char *)(dgettext("pidgin","Not logged in")));
      break; 
    }
    case 500:
{
      result = ((const char *)(dgettext("pidgin","Service temporarily unavailable")));
      break; 
    }
    case 501:
{
      result = ((const char *)(dgettext("pidgin","Database server error")));
       *debug = (!0);
      break; 
    }
    case 502:
{
      result = ((const char *)(dgettext("pidgin","Command disabled")));
       *debug = (!0);
      break; 
    }
    case 510:
{
      result = ((const char *)(dgettext("pidgin","File operation error")));
       *debug = (!0);
      break; 
    }
    case 520:
{
      result = ((const char *)(dgettext("pidgin","Memory allocation error")));
       *debug = (!0);
      break; 
    }
    case 540:
{
      result = ((const char *)(dgettext("pidgin","Wrong CHL value sent to server")));
       *debug = (!0);
      break; 
    }
    case 600:
{
      result = ((const char *)(dgettext("pidgin","Server busy")));
      break; 
    }
    case 601:
{
      result = ((const char *)(dgettext("pidgin","Server unavailable")));
      break; 
    }
    case 602:
{
      result = ((const char *)(dgettext("pidgin","Peer notification server down")));
       *debug = (!0);
      break; 
    }
    case 603:
{
      result = ((const char *)(dgettext("pidgin","Database connect error")));
       *debug = (!0);
      break; 
    }
    case 604:
{
      result = ((const char *)(dgettext("pidgin","Server is going down (abandon ship)")));
      break; 
    }
    case 605:
{
      result = ((const char *)(dgettext("pidgin","Server unavailable")));
      break; 
    }
    case 707:
{
      result = ((const char *)(dgettext("pidgin","Error creating connection")));
       *debug = (!0);
      break; 
    }
    case 710:
{
      result = ((const char *)(dgettext("pidgin","CVR parameters are either unknown or not allowed")));
       *debug = (!0);
      break; 
    }
    case 711:
{
      result = ((const char *)(dgettext("pidgin","Unable to write")));
      break; 
    }
    case 712:
{
      result = ((const char *)(dgettext("pidgin","Session overload")));
       *debug = (!0);
      break; 
    }
    case 713:
{
      result = ((const char *)(dgettext("pidgin","User is too active")));
      break; 
    }
    case 714:
{
      result = ((const char *)(dgettext("pidgin","Too many sessions")));
      break; 
    }
    case 715:
{
      result = ((const char *)(dgettext("pidgin","Passport not verified")));
      break; 
    }
    case 717:
{
      result = ((const char *)(dgettext("pidgin","Bad friend file")));
       *debug = (!0);
      break; 
    }
    case 731:
{
      result = ((const char *)(dgettext("pidgin","Not expected")));
       *debug = (!0);
      break; 
    }
    case 800:
{
      result = ((const char *)(dgettext("pidgin","Friendly name is changing too rapidly")));
      break; 
    }
    case 910:
{
    }
    case 912:
{
    }
    case 918:
{
    }
    case 919:
{
    }
    case 921:
{
    }
    case 922:
{
      result = ((const char *)(dgettext("pidgin","Server too busy")));
      break; 
    }
    case 911:
{
    }
    case 917:
{
      result = ((const char *)(dgettext("pidgin","Authentication failed")));
      break; 
    }
    case 913:
{
      result = ((const char *)(dgettext("pidgin","Not allowed when offline")));
      break; 
    }
    case 914:
{
    }
    case 915:
{
    }
    case 916:
{
      result = ((const char *)(dgettext("pidgin","Server unavailable")));
      break; 
    }
    case 920:
{
      result = ((const char *)(dgettext("pidgin","Not accepting new users")));
      break; 
    }
    case 923:
{
      result = ((const char *)(dgettext("pidgin","Kids Passport without parental consent")));
      break; 
    }
    case 924:
{
      result = ((const char *)(dgettext("pidgin","Passport account not yet verified")));
      break; 
    }
    case 927:
{
      result = ((const char *)(dgettext("pidgin","Passport account suspended")));
      break; 
    }
    case 928:
{
      result = ((const char *)(dgettext("pidgin","Bad ticket")));
       *debug = (!0);
      break; 
    }
    default:
{
      g_snprintf(msg,(sizeof(msg)),((const char *)(dgettext("pidgin","Unknown Error Code %d"))),type);
       *debug = (!0);
      result = msg;
      break; 
    }
  }
  return result;
}

void msn_error_handle(MsnSession *session,unsigned int type)
{
  char *buf;
  gboolean debug;
  buf = g_strdup_printf(((const char *)(dgettext("pidgin","MSN Error: %s\n"))),msn_error_get_text(type,&debug));
  if (debug != 0) 
    purple_debug_warning("msn","error %d: %s\n",type,buf);
  else 
    purple_notify_message(( *(session -> account)).gc,PURPLE_NOTIFY_MSG_ERROR,0,buf,0,0,0);
  g_free(buf);
}
/* Remove the buddy referenced by the MsnAddRemData before the serverside list
 * is changed.  If the buddy will be added, he'll be added back; if he will be
 * removed, he won't be. */
/* Actually with our MSNP14 code that isn't true yet, he won't be added back :(
 * */

static void msn_complete_sync_issue(MsnAddRemData *data)
{
  PurpleBuddy *buddy;
  PurpleGroup *group = (PurpleGroup *)((void *)0);
  if ((data -> group) != ((char *)((void *)0))) 
    group = purple_find_group((data -> group));
  if (group != ((PurpleGroup *)((void *)0))) 
    buddy = purple_find_buddy_in_group(( *(data -> session)).account,(data -> who),group);
  else 
    buddy = purple_find_buddy(( *(data -> session)).account,(data -> who));
  if (buddy != ((PurpleBuddy *)((void *)0))) 
    purple_blist_remove_buddy(buddy);
}

static void msn_add_cb(MsnAddRemData *data)
{
#if 0
/* this *should* be necessary !! */
#endif
  MsnUserList *userlist = ( *(data -> session)).userlist;
  msn_userlist_add_buddy(userlist,(data -> who),(data -> group));
  g_free((data -> group));
  g_free((data -> who));
  g_free(data);
}

static void msn_rem_cb(MsnAddRemData *data)
{
  MsnUserList *userlist = ( *(data -> session)).userlist;
  msn_complete_sync_issue(data);
  if ((data -> group) == ((char *)((void *)0))) {
    msn_userlist_rem_buddy_from_list(userlist,(data -> who),MSN_LIST_FL);
  }
  else {
    g_free((data -> group));
  }
  g_free((data -> who));
  g_free(data);
}

void msn_error_sync_issue(MsnSession *session,const char *passport,const char *group_name)
{
  PurpleConnection *gc;
  PurpleAccount *account;
  MsnAddRemData *data;
  char *msg;
  char *reason;
  account = (session -> account);
  gc = purple_account_get_connection(account);
  data = ((MsnAddRemData *)(g_malloc0_n(1,(sizeof(MsnAddRemData )))));
  data -> who = g_strdup(passport);
  data -> group = g_strdup(group_name);
  data -> session = session;
  msg = g_strdup_printf(((const char *)(dgettext("pidgin","Buddy list synchronization issue in %s (%s)"))),purple_account_get_username(account),purple_account_get_protocol_name(account));
  if (group_name != ((const char *)((void *)0))) {
    reason = g_strdup_printf(((const char *)(dgettext("pidgin","%s on the local list is inside the group \"%s\" but not on the server list. Do you want this buddy to be added\?"))),passport,group_name);
  }
  else {
    reason = g_strdup_printf(((const char *)(dgettext("pidgin","%s is on the local list but not on the server list. Do you want this buddy to be added\?"))),passport);
  }
  purple_request_action(gc,0,msg,reason,-1,account,(data -> who),0,data,2,((const char *)(dgettext("pidgin","Yes"))),((GCallback )msn_add_cb),((const char *)(dgettext("pidgin","No"))),((GCallback )msn_rem_cb));
  g_free(reason);
  g_free(msg);
}
