/**
 * @file history.c MSN history functions
 *
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "msn.h"
#include "history.h"

MsnHistory *msn_history_new()
{
  MsnHistory *history = (MsnHistory *)(g_malloc0_n(1,(sizeof(MsnHistory ))));
  history -> trId = 1;
  history -> queue = g_queue_new();
  return history;
}

void msn_history_destroy(MsnHistory *history)
{
  MsnTransaction *trans;
  while((trans = (g_queue_pop_head((history -> queue)))) != ((MsnTransaction *)((void *)0)))
    msn_transaction_destroy(trans);
  g_queue_free((history -> queue));
  g_free(history);
}

MsnTransaction *msn_history_find(MsnHistory *history,unsigned int trId)
{
  MsnTransaction *trans;
  GList *list;
  for (list = ( *(history -> queue)).head; list != ((GList *)((void *)0)); list = (list -> next)) {
    trans = (list -> data);
    if ((trans -> trId) == trId) 
      return trans;
  }
  return 0;
}

void msn_history_add(MsnHistory *history,MsnTransaction *trans)
{
  GQueue *queue;
  int max_elems;
  do {
    if (history != ((MsnHistory *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"history != NULL");
      return ;
    };
  }while (0);
  do {
    if (trans != ((MsnTransaction *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"trans != NULL");
      return ;
    };
  }while (0);
  queue = (history -> queue);
  trans -> trId = history -> trId++;
  g_queue_push_tail(queue,trans);
  if (( *( *(trans -> cmdproc)).servconn).type == MSN_SERVCONN_NS) 
    max_elems = 0x300;
  else 
    max_elems = 48;
  if ((queue -> length) > max_elems) {
    trans = (g_queue_pop_head(queue));
    msn_transaction_destroy(trans);
  }
}
