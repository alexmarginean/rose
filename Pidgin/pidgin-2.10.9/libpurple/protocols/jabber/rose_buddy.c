/*
 * purple - Jabber Protocol Plugin
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "debug.h"
#include "imgstore.h"
#include "prpl.h"
#include "notify.h"
#include "request.h"
#include "util.h"
#include "xmlnode.h"
#include "buddy.h"
#include "chat.h"
#include "jabber.h"
#include "iq.h"
#include "presence.h"
#include "useravatar.h"
#include "xdata.h"
#include "pep.h"
#include "adhoccommands.h"
#include "google/google.h"
typedef struct __unnamed_class___F0_L43_C9_unknown_scope_and_name_variable_declaration__variable_type_l_variable_name_unknown_scope_and_name__scope__idle_seconds {
long idle_seconds;}JabberBuddyInfoResource;
typedef struct __unnamed_class___F0_L47_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L372R__Pe___variable_name_unknown_scope_and_name__scope__js__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L371R__Pe___variable_name_unknown_scope_and_name__scope__jb__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__jid__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GSList_GSList__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__ids__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L84R__Pe___variable_name_unknown_scope_and_name__scope__resources__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_guintUi__typedef_declaration_variable_name_unknown_scope_and_name__scope__timeout_handle__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__GSList_GSList__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__vcard_imgids__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L323R__Pe___variable_name_unknown_scope_and_name__scope__user_info__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_l_variable_name_unknown_scope_and_name__scope__last_seconds__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__last_message {
JabberStream *js;
JabberBuddy *jb;
char *jid;
GSList *ids;
GHashTable *resources;
guint timeout_handle;
GSList *vcard_imgids;
PurpleNotifyUserInfo *user_info;
long last_seconds;
gchar *last_message;}JabberBuddyInfo;

static void jabber_buddy_resource_free(JabberBuddyResource *jbr)
{
  do {
    if (jbr != ((JabberBuddyResource *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"jbr != NULL");
      return ;
    };
  }while (0);
  ( *(jbr -> jb)).resources = g_list_remove(( *(jbr -> jb)).resources,jbr);
  while((jbr -> commands) != 0){
    JabberAdHocCommands *cmd = ( *(jbr -> commands)).data;
    g_free((cmd -> jid));
    g_free((cmd -> node));
    g_free((cmd -> name));
    g_free(cmd);
    jbr -> commands = g_list_delete_link((jbr -> commands),(jbr -> commands));
  }
  while(jbr -> caps.exts != 0){
    g_free(( *jbr -> caps.exts).data);
    jbr -> caps.exts = g_list_delete_link(jbr -> caps.exts,jbr -> caps.exts);
  }
  g_free((jbr -> name));
  g_free((jbr -> status));
  g_free((jbr -> thread_id));
  g_free(jbr -> client.name);
  g_free(jbr -> client.version);
  g_free(jbr -> client.os);
  g_free(jbr);
}

void jabber_buddy_free(JabberBuddy *jb)
{
  do {
    if (jb != ((JabberBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"jb != NULL");
      return ;
    };
  }while (0);
  g_free((jb -> error_msg));
  while((jb -> resources) != 0)
    jabber_buddy_resource_free(( *(jb -> resources)).data);
  g_free(jb);
}

JabberBuddy *jabber_buddy_find(JabberStream *js,const char *name,gboolean create)
{
  JabberBuddy *jb;
  char *realname;
  if ((js -> buddies) == ((GHashTable *)((void *)0))) 
    return 0;
  if (!((realname = jabber_get_bare_jid(name)) != 0)) 
    return 0;
  jb = (g_hash_table_lookup((js -> buddies),realname));
  if (!(jb != 0) && (create != 0)) {
    jb = ((JabberBuddy *)(g_malloc0_n(1,(sizeof(JabberBuddy )))));
    g_hash_table_insert((js -> buddies),realname,jb);
  }
  else 
    g_free(realname);
  return jb;
}
/* Returns -1 if a is a higher priority resource than b, or is
 * "more available" than b.  0 if they're the same, and 1 if b is
 * higher priority/more available than a.
 */

static gint resource_compare_cb(gconstpointer a,gconstpointer b)
{
  const JabberBuddyResource *jbra = a;
  const JabberBuddyResource *jbrb = b;
  JabberBuddyState state_a;
  JabberBuddyState state_b;
  if ((jbra -> priority) != (jbrb -> priority)) 
    return ((jbra -> priority) > (jbrb -> priority))?-1 : 1;
/* Fold the states for easier comparison */
/* TODO: Differentiate online/chat and away/dnd? */
  switch(jbra -> state){
    case JABBER_BUDDY_STATE_ONLINE:
{
    }
    case JABBER_BUDDY_STATE_CHAT:
{
      state_a = JABBER_BUDDY_STATE_ONLINE;
      break; 
    }
    case JABBER_BUDDY_STATE_AWAY:
{
    }
    case JABBER_BUDDY_STATE_DND:
{
      state_a = JABBER_BUDDY_STATE_AWAY;
      break; 
    }
    case JABBER_BUDDY_STATE_XA:
{
      state_a = JABBER_BUDDY_STATE_XA;
      break; 
    }
    case JABBER_BUDDY_STATE_UNAVAILABLE:
{
      state_a = JABBER_BUDDY_STATE_UNAVAILABLE;
      break; 
    }
    default:
{
      state_a = JABBER_BUDDY_STATE_UNKNOWN;
      break; 
    }
  }
  switch(jbrb -> state){
    case JABBER_BUDDY_STATE_ONLINE:
{
    }
    case JABBER_BUDDY_STATE_CHAT:
{
      state_b = JABBER_BUDDY_STATE_ONLINE;
      break; 
    }
    case JABBER_BUDDY_STATE_AWAY:
{
    }
    case JABBER_BUDDY_STATE_DND:
{
      state_b = JABBER_BUDDY_STATE_AWAY;
      break; 
    }
    case JABBER_BUDDY_STATE_XA:
{
      state_b = JABBER_BUDDY_STATE_XA;
      break; 
    }
    case JABBER_BUDDY_STATE_UNAVAILABLE:
{
      state_b = JABBER_BUDDY_STATE_UNAVAILABLE;
      break; 
    }
    default:
{
      state_b = JABBER_BUDDY_STATE_UNKNOWN;
      break; 
    }
  }
  if (state_a == state_b) {
    if ((jbra -> idle) == (jbrb -> idle)) 
      return 0;
    else if ((((jbra -> idle) != 0L) && !((jbrb -> idle) != 0L)) || ((((jbra -> idle) != 0L) && ((jbrb -> idle) != 0L)) && ((jbra -> idle) < (jbrb -> idle)))) 
      return 1;
    else 
      return (-1);
  }
  if (state_a == JABBER_BUDDY_STATE_ONLINE) 
    return (-1);
  else if ((state_a == JABBER_BUDDY_STATE_AWAY) && (((state_b == JABBER_BUDDY_STATE_XA) || (state_b == JABBER_BUDDY_STATE_UNAVAILABLE)) || (state_b == JABBER_BUDDY_STATE_UNKNOWN))) 
    return (-1);
  else if ((state_a == JABBER_BUDDY_STATE_XA) && ((state_b == JABBER_BUDDY_STATE_UNAVAILABLE) || (state_b == JABBER_BUDDY_STATE_UNKNOWN))) 
    return (-1);
  else if ((state_a == JABBER_BUDDY_STATE_UNAVAILABLE) && (state_b == JABBER_BUDDY_STATE_UNKNOWN)) 
    return (-1);
  return 1;
}

JabberBuddyResource *jabber_buddy_find_resource(JabberBuddy *jb,const char *resource)
{
  GList *l;
  if (!(jb != 0)) 
    return 0;
  if (resource == ((const char *)((void *)0))) 
    return (((jb -> resources) != 0)?( *(jb -> resources)).data : ((void *)((void *)0)));
  for (l = (jb -> resources); l != 0; l = (l -> next)) {
    JabberBuddyResource *jbr = (l -> data);
    if (((jbr -> name) != 0) && (g_str_equal(resource,(jbr -> name)) != 0)) 
      return jbr;
  }
  return 0;
}

JabberBuddyResource *jabber_buddy_track_resource(JabberBuddy *jb,const char *resource,int priority,JabberBuddyState state,const char *status)
{
/* TODO: Optimization: Only reinsert if priority+state changed */
  JabberBuddyResource *jbr = jabber_buddy_find_resource(jb,resource);
  if (jbr != 0) {
    jb -> resources = g_list_remove((jb -> resources),jbr);
  }
  else {
    jbr = ((JabberBuddyResource *)(g_malloc0_n(1,(sizeof(JabberBuddyResource )))));
    jbr -> jb = jb;
    jbr -> name = g_strdup(resource);
    jbr -> capabilities = JABBER_CAP_NONE;
    jbr -> tz_off = (-500000);
  }
  jbr -> priority = priority;
  jbr -> state = state;
  g_free((jbr -> status));
  jbr -> status = g_strdup(status);
  jb -> resources = g_list_insert_sorted((jb -> resources),jbr,resource_compare_cb);
  return jbr;
}

void jabber_buddy_remove_resource(JabberBuddy *jb,const char *resource)
{
  JabberBuddyResource *jbr = jabber_buddy_find_resource(jb,resource);
  if (!(jbr != 0)) 
    return ;
  jabber_buddy_resource_free(jbr);
}
/*******
 * This is the old vCard stuff taken from the old prpl.  vCards, by definition
 * are a temporary thing until jabber can get its act together and come up
 * with a format for user information, hence the namespace of 'vcard-temp'
 *
 * Since I don't feel like putting that much work into something that's
 * _supposed_ to go away, i'm going to just copy the kludgy old code here,
 * and make it purdy when jabber comes up with a standards-track JEP to
 * replace vcard-temp
 *                                 --Nathan
 *******/
/*---------------------------------------*/
/* Jabber "set info" (vCard) support     */
/*---------------------------------------*/
/*
 * V-Card format:
 *
 *  <vCard prodid='' version='' xmlns=''>
 *    <FN></FN>
 *    <N>
 *	<FAMILY/>
 *	<GIVEN/>
 *    </N>
 *    <NICKNAME/>
 *    <URL/>
 *    <ADR>
 *	<STREET/>
 *	<EXTADD/>
 *	<LOCALITY/>
 *	<REGION/>
 *	<PCODE/>
 *	<COUNTRY/>
 *    </ADR>
 *    <TEL/>
 *    <EMAIL/>
 *    <ORG>
 *	<ORGNAME/>
 *	<ORGUNIT/>
 *    </ORG>
 *    <TITLE/>
 *    <ROLE/>
 *    <DESC/>
 *    <BDAY/>
 *  </vCard>
 *
 * See also:
 *
 *	http://docs.jabber.org/proto/html/vcard-temp.html
 *	http://www.vcard-xml.org/dtd/vCard-XML-v2-20010520.dtd
 */
/*
 * Cross-reference user-friendly V-Card entry labels to vCard XML tags
 * and attributes.
 *
 * Order is (or should be) unimportant.  For example: we have no way of
 * knowing in what order real data will arrive.
 *
 * Format: Label, Pre-set text, "visible" flag, "editable" flag, XML tag
 *         name, XML tag's parent tag "path" (relative to vCard node).
 *
 *         List is terminated by a NULL label pointer.
 *
 *	   Entries with no label text, but with XML tag and parent tag
 *	   entries, are used by V-Card XML construction routines to
 *	   "automagically" construct the appropriate XML node tree.
 *
 * Thoughts on future direction/expansion
 *
 *	This is a "simple" vCard.
 *
 *	It is possible for nodes other than the "vCard" node to have
 *      attributes.  Should that prove necessary/desirable, add an
 *      "attributes" pointer to the vcard_template struct, create the
 *      necessary tag_attr structs, and add 'em to the vcard_dflt_data
 *      array.
 *
 *	The above changes will (obviously) require changes to the vCard
 *      construction routines.
 */
const struct vcard_template {
/* label text pointer */
char *label;
/* tag text */
char *tag;
/* parent tag "path" text */
char *ptag;}vcard_template_data[] = {{("Full Name"), ("FN"), ((char *)((void *)0))}, {("Family Name"), ("FAMILY"), ("N")}, {("Given Name"), ("GIVEN"), ("N")}, {("Nickname"), ("NICKNAME"), ((char *)((void *)0))}, {("URL"), ("URL"), ((char *)((void *)0))}, {("Street Address"), ("STREET"), ("ADR")}, {("Extended Address"), ("EXTADD"), ("ADR")}, {("Locality"), ("LOCALITY"), ("ADR")}, {("Region"), ("REGION"), ("ADR")}, {("Postal Code"), ("PCODE"), ("ADR")}, {("Country"), ("CTRY"), ("ADR")}, {("Telephone"), ("NUMBER"), ("TEL")}, {("Email"), ("USERID"), ("EMAIL")}, {("Organization Name"), ("ORGNAME"), ("ORG")}, {("Organization Unit"), ("ORGUNIT"), ("ORG")}, {("Job Title"), ("TITLE"), ((char *)((void *)0))}, {("Role"), ("ROLE"), ((char *)((void *)0))}, {("Birthday"), ("BDAY"), ((char *)((void *)0))}, {("Description"), ("DESC"), ((char *)((void *)0))}, {(""), ("N"), ((char *)((void *)0))}, {(""), ("ADR"), ((char *)((void *)0))}, {(""), ("ORG"), ((char *)((void *)0))}, {((char *)((void *)0)), ((char *)((void *)0)), ((char *)((void *)0))}};
/*
 * The "vCard" tag's attribute list...
 */
const struct tag_attr {
char *attr;
char *value;}vcard_tag_attr_list[] = {{("prodid"), ("-//HandGen//NONSGML vGen v1.0//EN")}, {("version"), ("2.0")}, {("xmlns"), ("vcard-temp")}, {((char *)((void *)0)), ((char *)((void *)0))}};
/*
 * Insert a tag node into an xmlnode tree, recursively inserting parent tag
 * nodes as necessary
 *
 * Returns pointer to inserted node
 *
 * Note to hackers: this code is designed to be re-entrant (it's recursive--it
 * calls itself), so don't put any "static"s in here!
 */

static xmlnode *insert_tag_to_parent_tag(xmlnode *start,const char *parent_tag,const char *new_tag)
{
  xmlnode *x = (xmlnode *)((void *)0);
/*
	 * If the parent tag wasn't specified, see if we can get it
	 * from the vCard template struct.
	 */
  if (parent_tag == ((const char *)((void *)0))) {
    const struct vcard_template *vc_tp = vcard_template_data;
{
      while((vc_tp -> label) != ((char *)((void *)0))){
        if (strcmp((vc_tp -> tag),new_tag) == 0) {
          parent_tag = (vc_tp -> ptag);
          break; 
        }
        ++vc_tp;
      }
    }
  }
/*
	 * If we have a parent tag...
	 */
  if (parent_tag != ((const char *)((void *)0))) {
/*
		 * Try to get the parent node for a tag
		 */
    if ((x = xmlnode_get_child(start,parent_tag)) == ((xmlnode *)((void *)0))) {
/*
			 * Descend?
			 */
      char *grand_parent = g_strdup(parent_tag);
      char *parent;
      if ((parent = strrchr(grand_parent,'/')) != ((char *)((void *)0))) {
         *(parent++) = 0;
        x = insert_tag_to_parent_tag(start,grand_parent,parent);
      }
      else {
        x = xmlnode_new_child(start,grand_parent);
      }
      g_free(grand_parent);
    }
    else {
/*
			 * We found *something* to be the parent node.
			 * Note: may be the "root" node!
			 */
      xmlnode *y;
      if ((y = xmlnode_get_child(x,new_tag)) != ((xmlnode *)((void *)0))) {
        return y;
      }
    }
  }
/*
	 * insert the new tag into its parent node
	 */
  return xmlnode_new_child(((x == ((xmlnode *)((void *)0)))?start : x),new_tag);
}
/*
 * Send vCard info to Jabber server
 */

void jabber_set_info(PurpleConnection *gc,const char *info)
{
  PurpleStoredImage *img;
  JabberIq *iq;
  JabberStream *js = (purple_connection_get_protocol_data(gc));
  xmlnode *vc_node;
  const struct tag_attr *tag_attr;
/* if we have't grabbed the remote vcard yet, we can't
	 * assume that what we have here is correct */
  if (!((js -> vcard_fetched) != 0)) 
    return ;
  if ((js -> vcard_timer) != 0U) {
    purple_timeout_remove((js -> vcard_timer));
    js -> vcard_timer = 0;
  }
  g_free((js -> avatar_hash));
  js -> avatar_hash = ((char *)((void *)0));
/*
	 * Send only if there's actually any *information* to send
	 */
  vc_node = ((info != 0)?xmlnode_from_str(info,(-1)) : ((struct _xmlnode *)((void *)0)));
  if ((vc_node != 0) && (!((vc_node -> name) != 0) || (g_ascii_strncasecmp((vc_node -> name),"vCard",5) != 0))) {
    xmlnode_free(vc_node);
    vc_node = ((xmlnode *)((void *)0));
  }
  if ((img = purple_buddy_icons_find_account_icon((gc -> account))) != 0) {
    gconstpointer avatar_data;
    gsize avatar_len;
    xmlnode *photo;
    xmlnode *binval;
    xmlnode *type;
    gchar *enc;
    if (!(vc_node != 0)) {
      vc_node = xmlnode_new("vCard");
      for (tag_attr = vcard_tag_attr_list; (tag_attr -> attr) != ((char *)((void *)0)); ++tag_attr) 
        xmlnode_set_attrib(vc_node,(tag_attr -> attr),(tag_attr -> value));
    }
    avatar_data = purple_imgstore_get_data(img);
    avatar_len = purple_imgstore_get_size(img);
/* Get rid of an old PHOTO if one exists.
		 * TODO: This may want to be modified to remove all old PHOTO
		 * children, at the moment some people have managed to get
		 * multiple PHOTO entries in their vCard. */
    if ((photo = xmlnode_get_child(vc_node,"PHOTO")) != 0) {
      xmlnode_free(photo);
    }
    photo = xmlnode_new_child(vc_node,"PHOTO");
    type = xmlnode_new_child(photo,"TYPE");
    xmlnode_insert_data(type,"image/png",(-1));
    binval = xmlnode_new_child(photo,"BINVAL");
    enc = purple_base64_encode(avatar_data,avatar_len);
    js -> avatar_hash = jabber_calculate_data_hash(avatar_data,avatar_len,"sha1");
    xmlnode_insert_data(binval,enc,(-1));
    g_free(enc);
    purple_imgstore_unref(img);
  }
  else if (vc_node != 0) {
    xmlnode *photo;
/* TODO: Remove all PHOTO children? (see above note) */
    if ((photo = xmlnode_get_child(vc_node,"PHOTO")) != 0) {
      xmlnode_free(photo);
    }
  }
  if (vc_node != ((xmlnode *)((void *)0))) {
    iq = jabber_iq_new(js,JABBER_IQ_SET);
    xmlnode_insert_child((iq -> node),vc_node);
    jabber_iq_send(iq);
/* Send presence to update vcard-temp:x:update */
    jabber_presence_send(js,0);
  }
}

void jabber_set_buddy_icon(PurpleConnection *gc,PurpleStoredImage *img)
{
  PurpleAccount *account = purple_connection_get_account(gc);
/* Publish the avatar as specified in XEP-0084 */
  jabber_avatar_set((gc -> proto_data),img);
/* Set the image in our vCard */
  jabber_set_info(gc,purple_account_get_user_info(account));
/* TODO: Fake image to ourselves, since a number of servers do not echo
	 * back our presence to us. To do this without uselessly copying the data
	 * of the image, we need purple_buddy_icons_set_for_user_image (i.e. takes
	 * an existing icon/stored image). */
}
/*
 * This is the callback from the "ok clicked" for "set vCard"
 *
 * Sets the vCard with data from PurpleRequestFields.
 */

static void jabber_format_info(PurpleConnection *gc,PurpleRequestFields *fields)
{
  xmlnode *vc_node;
  PurpleRequestField *field;
  const char *text;
  char *p;
  const struct vcard_template *vc_tp;
  const struct tag_attr *tag_attr;
  vc_node = xmlnode_new("vCard");
  for (tag_attr = vcard_tag_attr_list; (tag_attr -> attr) != ((char *)((void *)0)); ++tag_attr) 
    xmlnode_set_attrib(vc_node,(tag_attr -> attr),(tag_attr -> value));
  for (vc_tp = vcard_template_data; (vc_tp -> label) != ((char *)((void *)0)); vc_tp++) {
    if (( *(vc_tp -> label)) == 0) 
      continue; 
    field = purple_request_fields_get_field(fields,(vc_tp -> tag));
    text = purple_request_field_string_get_value(field);
    if ((text != ((const char *)((void *)0))) && (( *text) != 0)) {
      xmlnode *xp;
      purple_debug_info("jabber","Setting %s to \'%s\'\n",(vc_tp -> tag),text);
      if ((xp = insert_tag_to_parent_tag(vc_node,0,(vc_tp -> tag))) != ((xmlnode *)((void *)0))) {
        xmlnode_insert_data(xp,text,(-1));
      }
    }
  }
  p = xmlnode_to_str(vc_node,0);
  xmlnode_free(vc_node);
  purple_account_set_user_info(purple_connection_get_account(gc),p);
  serv_set_info(gc,p);
  g_free(p);
}
/*
 * This gets executed by the proto action
 *
 * Creates a new PurpleRequestFields struct, gets the XML-formatted user_info
 * string (if any) into GSLists for the (multi-entry) edit dialog and
 * calls the set_vcard dialog.
 */

void jabber_setup_set_info(PurplePluginAction *action)
{
  PurpleConnection *gc = (PurpleConnection *)(action -> context);
  PurpleRequestFields *fields;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  const struct vcard_template *vc_tp;
  const char *user_info;
  char *cdata = (char *)((void *)0);
  xmlnode *x_vc_data = (xmlnode *)((void *)0);
  fields = purple_request_fields_new();
  group = purple_request_field_group_new(0);
  purple_request_fields_add_group(fields,group);
/*
	 * Get existing, XML-formatted, user info
	 */
  if ((user_info = purple_account_get_user_info((gc -> account))) != ((const char *)((void *)0))) 
    x_vc_data = xmlnode_from_str(user_info,(-1));
/*
	 * Set up GSLists for edit with labels from "template," data from user info
	 */
  for (vc_tp = vcard_template_data; (vc_tp -> label) != ((char *)((void *)0)); ++vc_tp) {{
      xmlnode *data_node;
      if ((vc_tp -> label)[0] == 0) 
        continue; 
      if (x_vc_data != ((xmlnode *)((void *)0))) {
        if ((vc_tp -> ptag) == ((char *)((void *)0))) {
          data_node = xmlnode_get_child(x_vc_data,(vc_tp -> tag));
        }
        else {
          gchar *tag = g_strdup_printf("%s/%s",(vc_tp -> ptag),(vc_tp -> tag));
          data_node = xmlnode_get_child(x_vc_data,tag);
          g_free(tag);
        }
        if (data_node != 0) 
          cdata = xmlnode_get_data(data_node);
      }
      if (strcmp((vc_tp -> tag),"DESC") == 0) {
        field = purple_request_field_string_new((vc_tp -> tag),((const char *)(dgettext("pidgin",(vc_tp -> label)))),cdata,(!0));
      }
      else {
        field = purple_request_field_string_new((vc_tp -> tag),((const char *)(dgettext("pidgin",(vc_tp -> label)))),cdata,0);
      }
      g_free(cdata);
      cdata = ((char *)((void *)0));
      purple_request_field_group_add_field(group,field);
    }
  }
  if (x_vc_data != ((xmlnode *)((void *)0))) 
    xmlnode_free(x_vc_data);
  purple_request_fields(gc,((const char *)(dgettext("pidgin","Edit XMPP vCard"))),((const char *)(dgettext("pidgin","Edit XMPP vCard"))),((const char *)(dgettext("pidgin","All items below are optional. Enter only the information with which you feel comfortable."))),fields,((const char *)(dgettext("pidgin","Save"))),((GCallback )jabber_format_info),((const char *)(dgettext("pidgin","Cancel"))),0,purple_connection_get_account(gc),0,0,gc);
}
/*---------------------------------------*/
/* End Jabber "set info" (vCard) support */
/*---------------------------------------*/
/******
 * end of that ancient crap that needs to die
 ******/

static void jabber_buddy_info_destroy(JabberBuddyInfo *jbi)
{
/* Remove the timeout, which would otherwise trigger jabber_buddy_get_info_timeout() */
  if ((jbi -> timeout_handle) > 0) 
    purple_timeout_remove((jbi -> timeout_handle));
  g_free((jbi -> jid));
  g_hash_table_destroy((jbi -> resources));
  g_free((jbi -> last_message));
  purple_notify_user_info_destroy((jbi -> user_info));
  g_free(jbi);
}

static void add_jbr_info(JabberBuddyInfo *jbi,const char *resource,JabberBuddyResource *jbr)
{
  JabberBuddyInfoResource *jbir;
  PurpleNotifyUserInfo *user_info;
  jbir = (g_hash_table_lookup((jbi -> resources),resource));
  user_info = (jbi -> user_info);
  if ((jbr != 0) && (jbr -> client.name != 0)) {
    char *tmp = g_strdup_printf("%s%s%s",jbr -> client.name,((jbr -> client.version != 0)?" " : ""),((jbr -> client.version != 0)?jbr -> client.version : ""));
    purple_notify_user_info_prepend_pair(user_info,((const char *)(dgettext("pidgin","Client"))),tmp);
    g_free(tmp);
    if (jbr -> client.os != 0) 
      purple_notify_user_info_prepend_pair(user_info,((const char *)(dgettext("pidgin","Operating System"))),jbr -> client.os);
  }
  if ((jbr != 0) && ((jbr -> tz_off) != (-500000))) {
    time_t now_t;
    struct tm *now;
    char *timestamp;
    time(&now_t);
    now_t += (jbr -> tz_off);
    now = gmtime((&now_t));
    timestamp = g_strdup_printf("%s %c%02d%02d",purple_time_format(now),(((jbr -> tz_off) < 0)?'-' : '+'),abs(((jbr -> tz_off) / (60 * 60))),abs((((jbr -> tz_off) % (60 * 60)) / 60)));
    purple_notify_user_info_prepend_pair(user_info,((const char *)(dgettext("pidgin","Local Time"))),timestamp);
    g_free(timestamp);
  }
  if ((jbir != 0) && ((jbir -> idle_seconds) > 0)) {
    char *idle = purple_str_seconds_to_string((jbir -> idle_seconds));
    purple_notify_user_info_prepend_pair(user_info,((const char *)(dgettext("pidgin","Idle"))),idle);
    g_free(idle);
  }
  if (jbr != 0) {
    char *purdy = (char *)((void *)0);
    char *tmp;
    char priority[12UL];
    const char *status_name = jabber_buddy_state_get_name((jbr -> state));
    if ((jbr -> status) != 0) {
      tmp = purple_markup_escape_text((jbr -> status),(-1));
      purdy = purple_strdup_withhtml(tmp);
      g_free(tmp);
      if (purple_strequal(status_name,purdy) != 0) 
        status_name = ((const char *)((void *)0));
    }
    tmp = g_strdup_printf("%s%s%s",((status_name != 0)?status_name : ""),(((status_name != 0) && (purdy != 0))?": " : ""),((purdy != 0)?purdy : ""));
    purple_notify_user_info_prepend_pair(user_info,((const char *)(dgettext("pidgin","Status"))),tmp);
    g_snprintf(priority,(sizeof(priority)),"%d",(jbr -> priority));
    purple_notify_user_info_prepend_pair(user_info,((const char *)(dgettext("pidgin","Priority"))),priority);
    g_free(tmp);
    g_free(purdy);
  }
  else {
    purple_notify_user_info_prepend_pair(user_info,((const char *)(dgettext("pidgin","Status"))),((const char *)(dgettext("pidgin","Unknown"))));
  }
}

static void jabber_buddy_info_show_if_ready(JabberBuddyInfo *jbi)
{
  char *resource_name;
  JabberBuddyResource *jbr;
  GList *resources;
  PurpleNotifyUserInfo *user_info;
/* not yet */
  if ((jbi -> ids) != 0) 
    return ;
  user_info = (jbi -> user_info);
  resource_name = jabber_get_resource((jbi -> jid));
/* If we have one or more pairs from the vcard, put a section break above it */
  if (purple_notify_user_info_get_entries(user_info) != 0) 
    purple_notify_user_info_prepend_section_break(user_info);
/* Add the information about the user's resource(s) */
  if (resource_name != 0) {
    jbr = jabber_buddy_find_resource((jbi -> jb),resource_name);
    add_jbr_info(jbi,resource_name,jbr);
  }
  else {
/* TODO: This is in priority-ascending order (lowest prio first), because
		 * everything is prepended.  Is that ok? */
    for (resources = ( *(jbi -> jb)).resources; resources != 0; resources = (resources -> next)) {
      jbr = (resources -> data);
/* put a section break between resources, this is not needed if
			 we are at the first, because one was already added for the vcard
			 section */
      if (resources != ( *(jbi -> jb)).resources) 
        purple_notify_user_info_prepend_section_break(user_info);
      add_jbr_info(jbi,(jbr -> name),jbr);
      if ((jbr -> name) != 0) 
        purple_notify_user_info_prepend_pair(user_info,((const char *)(dgettext("pidgin","Resource"))),(jbr -> name));
    }
  }
  if (!(( *(jbi -> jb)).resources != 0)) {
/* the buddy is offline */
    gboolean is_domain = jabber_jid_is_domain((jbi -> jid));
    if ((jbi -> last_seconds) > 0) {
      char *last = purple_str_seconds_to_string((jbi -> last_seconds));
      gchar *message = (gchar *)((void *)0);
      const gchar *title = (const gchar *)((void *)0);
      if (is_domain != 0) {
        title = ((const char *)(dgettext("pidgin","Uptime")));
        message = last;
        last = ((char *)((void *)0));
      }
      else {
        title = ((const char *)(dgettext("pidgin","Logged Off")));
        message = g_strdup_printf(((const char *)(dgettext("pidgin","%s ago"))),last);
      }
      purple_notify_user_info_prepend_pair(user_info,title,message);
      g_free(last);
      g_free(message);
    }
    if (!(is_domain != 0)) {
      gchar *status = g_strdup_printf("%s%s%s",((const char *)(dgettext("pidgin","Offline"))),(((jbi -> last_message) != 0)?": " : ""),(((jbi -> last_message) != 0)?(jbi -> last_message) : ""));
      purple_notify_user_info_prepend_pair(user_info,((const char *)(dgettext("pidgin","Status"))),status);
      g_free(status);
    }
  }
  g_free(resource_name);
  purple_notify_userinfo(( *(jbi -> js)).gc,(jbi -> jid),user_info,0,0);
  while((jbi -> vcard_imgids) != 0){
    purple_imgstore_unref_by_id(((gint )((glong )( *(jbi -> vcard_imgids)).data)));
    jbi -> vcard_imgids = g_slist_delete_link((jbi -> vcard_imgids),(jbi -> vcard_imgids));
  }
  ( *(jbi -> js)).pending_buddy_info_requests = g_slist_remove(( *(jbi -> js)).pending_buddy_info_requests,jbi);
  jabber_buddy_info_destroy(jbi);
}

static void jabber_buddy_info_remove_id(JabberBuddyInfo *jbi,const char *id)
{
  GSList *l = (jbi -> ids);
  char *comp_id;
  if (!(id != 0)) 
    return ;
  while(l != 0){
    comp_id = (l -> data);
    if (!(strcmp(id,comp_id) != 0)) {
      jbi -> ids = g_slist_remove((jbi -> ids),comp_id);
      g_free(comp_id);
      return ;
    }
    l = (l -> next);
  }
}

static gboolean set_own_vcard_cb(gpointer data)
{
  JabberStream *js = data;
  PurpleAccount *account = purple_connection_get_account((js -> gc));
  js -> vcard_timer = 0;
  jabber_set_info((js -> gc),purple_account_get_user_info(account));
  return 0;
}

static void jabber_vcard_save_mine(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  xmlnode *vcard;
  xmlnode *photo;
  xmlnode *binval;
  char *txt;
  char *vcard_hash = (char *)((void *)0);
  PurpleAccount *account;
  if (type == JABBER_IQ_ERROR) {
    xmlnode *error;
    purple_debug_warning("jabber","Server returned error while retrieving vCard\n");
    error = xmlnode_get_child(packet,"error");
    if (!(error != 0) || !(xmlnode_get_child(error,"item-not-found") != 0)) 
      return ;
  }
  account = purple_connection_get_account((js -> gc));
  if (((vcard = xmlnode_get_child(packet,"vCard")) != 0) || ((vcard = xmlnode_get_child_with_namespace(packet,"query","vcard-temp")) != 0)) {
    txt = xmlnode_to_str(vcard,0);
    purple_account_set_user_info(account,txt);
    g_free(txt);
  }
  else {
/* if we have no vCard, then lets not overwrite what we might have locally */
  }
  js -> vcard_fetched = (!0);
  if (((vcard != 0) && ((photo = xmlnode_get_child(vcard,"PHOTO")) != 0)) && ((binval = xmlnode_get_child(photo,"BINVAL")) != 0)) {
    gsize size;
    char *bintext = xmlnode_get_data(binval);
    if (bintext != 0) {
      guchar *data = purple_base64_decode(bintext,&size);
      g_free(bintext);
      if (data != 0) {
        vcard_hash = jabber_calculate_data_hash(data,size,"sha1");
        g_free(data);
      }
    }
  }
/* Republish our vcard if the photo is different than the server's */
  if (((js -> initial_avatar_hash) != 0) && !(purple_strequal(vcard_hash,(js -> initial_avatar_hash)) != 0)) {
/*
		 * Google Talk has developed the behavior that it will not accept
		 * a vcard set in the first 10 seconds (or so) of the connection;
		 * it returns an error (namespaces trimmed):
		 * <error code="500" type="wait"><internal-server-error/></error>.
		 */
    if ((js -> googletalk) != 0) 
      js -> vcard_timer = purple_timeout_add_seconds(10,set_own_vcard_cb,js);
    else 
      jabber_set_info((js -> gc),purple_account_get_user_info(account));
  }
  else if (vcard_hash != 0) {
/* A photo is in the vCard. Advertise its hash */
    js -> avatar_hash = vcard_hash;
    vcard_hash = ((char *)((void *)0));
/* Send presence to update vcard-temp:x:update */
    jabber_presence_send(js,0);
  }
  g_free(vcard_hash);
}

void jabber_vcard_fetch_mine(JabberStream *js)
{
  JabberIq *iq = jabber_iq_new(js,JABBER_IQ_GET);
  xmlnode *vcard = xmlnode_new_child((iq -> node),"vCard");
  xmlnode_set_namespace(vcard,"vcard-temp");
  jabber_iq_set_callback(iq,jabber_vcard_save_mine,0);
  jabber_iq_send(iq);
}

static void jabber_vcard_parse(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  char *bare_jid;
  char *text;
  char *serverside_alias = (char *)((void *)0);
  xmlnode *vcard;
  PurpleAccount *account;
  JabberBuddyInfo *jbi = data;
  PurpleNotifyUserInfo *user_info;
  do {
    if (jbi != ((JabberBuddyInfo *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"jbi != NULL");
      return ;
    };
  }while (0);
  jabber_buddy_info_remove_id(jbi,id);
  if (type == JABBER_IQ_ERROR) {
    purple_debug_info("jabber","Got error response for vCard\n");
    jabber_buddy_info_show_if_ready(jbi);
    return ;
  }
  user_info = (jbi -> user_info);
  account = purple_connection_get_account((js -> gc));
  bare_jid = jabber_get_bare_jid(((from != 0)?from : purple_account_get_username(account)));
/* TODO: Is the query xmlns='vcard-temp' version of this still necessary? */
  if (((vcard = xmlnode_get_child(packet,"vCard")) != 0) || ((vcard = xmlnode_get_child_with_namespace(packet,"query","vcard-temp")) != 0)) {
    xmlnode *child;
    for (child = (vcard -> child); child != 0; child = (child -> next)) {{
        xmlnode *child2;
        if ((child -> type) != XMLNODE_TYPE_TAG) 
          continue; 
        text = xmlnode_get_data(child);
        if ((text != 0) && !(strcmp((child -> name),"FN") != 0)) {
          if (!(serverside_alias != 0)) 
            serverside_alias = g_strdup(text);
          purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Full Name"))),text);
        }
        else if (!(strcmp((child -> name),"N") != 0)) {
          for (child2 = (child -> child); child2 != 0; child2 = (child2 -> next)) {{
              char *text2;
              if ((child2 -> type) != XMLNODE_TYPE_TAG) 
                continue; 
              text2 = xmlnode_get_data(child2);
              if ((text2 != 0) && !(strcmp((child2 -> name),"FAMILY") != 0)) {
                purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Family Name"))),text2);
              }
              else if ((text2 != 0) && !(strcmp((child2 -> name),"GIVEN") != 0)) {
                purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Given Name"))),text2);
              }
              else if ((text2 != 0) && !(strcmp((child2 -> name),"MIDDLE") != 0)) {
                purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Middle Name"))),text2);
              }
              g_free(text2);
            }
          }
        }
        else if ((text != 0) && !(strcmp((child -> name),"NICKNAME") != 0)) {
/* Prefer the Nickcname to the Full Name as the serverside alias if it's not just part of the jid.
				 * Ignore it if it's part of the jid. */
          if (strstr(bare_jid,text) == ((char *)((void *)0))) {
            g_free(serverside_alias);
            serverside_alias = g_strdup(text);
            purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Nickname"))),text);
          }
        }
        else if ((text != 0) && !(strcmp((child -> name),"BDAY") != 0)) {
          purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Birthday"))),text);
        }
        else if (!(strcmp((child -> name),"ADR") != 0)) {
          gboolean address_line_added = 0;
          for (child2 = (child -> child); child2 != 0; child2 = (child2 -> next)) {{
              char *text2;
              if ((child2 -> type) != XMLNODE_TYPE_TAG) 
                continue; 
              text2 = xmlnode_get_data(child2);
              if (text2 == ((char *)((void *)0))) 
                continue; 
/* We do this here so that it's not added if all the child
					 * elements are empty. */
              if (!(address_line_added != 0)) {
                purple_notify_user_info_add_section_header(user_info,((const char *)(dgettext("pidgin","Address"))));
                address_line_added = (!0);
              }
              if (!(strcmp((child2 -> name),"POBOX") != 0)) {
                purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","P.O. Box"))),text2);
              }
              else if ((g_str_equal((child2 -> name),"EXTADD") != 0) || (g_str_equal((child2 -> name),"EXTADR") != 0)) {
/*
						 * EXTADD is correct, EXTADR is generated by other
						 * clients. The next time someone reads this, remove
						 * EXTADR.
						 */
                purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Extended Address"))),text2);
              }
              else if (!(strcmp((child2 -> name),"STREET") != 0)) {
                purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Street Address"))),text2);
              }
              else if (!(strcmp((child2 -> name),"LOCALITY") != 0)) {
                purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Locality"))),text2);
              }
              else if (!(strcmp((child2 -> name),"REGION") != 0)) {
                purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Region"))),text2);
              }
              else if (!(strcmp((child2 -> name),"PCODE") != 0)) {
                purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Postal Code"))),text2);
              }
              else if (!(strcmp((child2 -> name),"CTRY") != 0) || !(strcmp((child2 -> name),"COUNTRY") != 0)) {
                purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Country"))),text2);
              }
              g_free(text2);
            }
          }
          if (address_line_added != 0) 
            purple_notify_user_info_add_section_break(user_info);
        }
        else if (!(strcmp((child -> name),"TEL") != 0)) {
          char *number;
          if ((child2 = xmlnode_get_child(child,"NUMBER")) != 0) {
/* show what kind of number it is */
            number = xmlnode_get_data(child2);
            if (number != 0) {
              purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Telephone"))),number);
              g_free(number);
            }
          }
          else if ((number = xmlnode_get_data(child)) != 0) {
/* lots of clients (including purple) do this, but it's
					 * out of spec */
            purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Telephone"))),number);
            g_free(number);
          }
        }
        else if (!(strcmp((child -> name),"EMAIL") != 0)) {
          char *userid;
          char *escaped;
          if ((child2 = xmlnode_get_child(child,"USERID")) != 0) {
/* show what kind of email it is */
            userid = xmlnode_get_data(child2);
            if (userid != 0) {
              char *mailto;
              escaped = g_markup_escape_text(userid,(-1));
              mailto = g_strdup_printf("<a href=\"mailto:%s\">%s</a>",escaped,escaped);
              purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Email"))),mailto);
              g_free(mailto);
              g_free(escaped);
              g_free(userid);
            }
          }
          else if ((userid = xmlnode_get_data(child)) != 0) {
/* lots of clients (including purple) do this, but it's
					 * out of spec */
            char *mailto;
            escaped = g_markup_escape_text(userid,(-1));
            mailto = g_strdup_printf("<a href=\"mailto:%s\">%s</a>",escaped,escaped);
            purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Email"))),mailto);
            g_free(mailto);
            g_free(escaped);
            g_free(userid);
          }
        }
        else if (!(strcmp((child -> name),"ORG") != 0)) {
          for (child2 = (child -> child); child2 != 0; child2 = (child2 -> next)) {{
              char *text2;
              if ((child2 -> type) != XMLNODE_TYPE_TAG) 
                continue; 
              text2 = xmlnode_get_data(child2);
              if ((text2 != 0) && !(strcmp((child2 -> name),"ORGNAME") != 0)) {
                purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Organization Name"))),text2);
              }
              else if ((text2 != 0) && !(strcmp((child2 -> name),"ORGUNIT") != 0)) {
                purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Organization Unit"))),text2);
              }
              g_free(text2);
            }
          }
        }
        else if ((text != 0) && !(strcmp((child -> name),"TITLE") != 0)) {
          purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Job Title"))),text);
        }
        else if ((text != 0) && !(strcmp((child -> name),"ROLE") != 0)) {
          purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Role"))),text);
        }
        else if ((text != 0) && !(strcmp((child -> name),"DESC") != 0)) {
          purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Description"))),text);
        }
        else if (!(strcmp((child -> name),"PHOTO") != 0) || !(strcmp((child -> name),"LOGO") != 0)) {
          char *bintext = (char *)((void *)0);
          xmlnode *binval;
          if (((binval = xmlnode_get_child(child,"BINVAL")) != 0) && ((bintext = xmlnode_get_data(binval)) != 0)) {
            gsize size;
            guchar *data;
            gboolean photo = (strcmp((child -> name),"PHOTO") == 0);
            data = purple_base64_decode(bintext,&size);
            if (data != 0) {
              char *img_text;
              char *hash;
              jbi -> vcard_imgids = g_slist_prepend((jbi -> vcard_imgids),((gpointer )((glong )(purple_imgstore_add_with_id(g_memdup(data,size),size,"logo.png")))));
              img_text = g_strdup_printf("<img id=\'%d\'>",((gint )((glong )( *(jbi -> vcard_imgids)).data)));
              purple_notify_user_info_add_pair(user_info,((photo != 0)?((const char *)(dgettext("pidgin","Photo"))) : ((const char *)(dgettext("pidgin","Logo")))),img_text);
              hash = jabber_calculate_data_hash(data,size,"sha1");
              purple_buddy_icons_set_for_user(account,bare_jid,data,size,hash);
              g_free(hash);
              g_free(img_text);
            }
            g_free(bintext);
          }
        }
        g_free(text);
      }
    }
  }
  if (serverside_alias != 0) {
    PurpleBuddy *b;
/* If we found a serverside alias, set it and tell the core */
    serv_got_alias((js -> gc),bare_jid,serverside_alias);
    b = purple_find_buddy(account,bare_jid);
    if (b != 0) {
      purple_blist_node_set_string(((PurpleBlistNode *)b),"servernick",serverside_alias);
    }
    g_free(serverside_alias);
  }
  g_free(bare_jid);
  jabber_buddy_info_show_if_ready(jbi);
}

static void jabber_buddy_info_resource_free(gpointer data)
{
  JabberBuddyInfoResource *jbri = data;
  g_free(jbri);
}

static guint jbir_hash(gconstpointer v)
{
  if (v != 0) 
    return g_str_hash(v);
  else 
    return 0;
}

static gboolean jbir_equal(gconstpointer v1,gconstpointer v2)
{
  const gchar *resource_1 = v1;
  const gchar *resource_2 = v2;
  return purple_strequal(resource_1,resource_2);
}

static void jabber_version_parse(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  JabberBuddyInfo *jbi = data;
  xmlnode *query;
  char *resource_name;
  do {
    if (jbi != ((JabberBuddyInfo *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"jbi != NULL");
      return ;
    };
  }while (0);
  jabber_buddy_info_remove_id(jbi,id);
  if (!(from != 0)) 
    return ;
  resource_name = jabber_get_resource(from);
  if (resource_name != 0) {
    if (type == JABBER_IQ_RESULT) {
      if ((query = xmlnode_get_child(packet,"query")) != 0) {
        JabberBuddyResource *jbr = jabber_buddy_find_resource((jbi -> jb),resource_name);
        if (jbr != 0) {
          xmlnode *node;
          if ((node = xmlnode_get_child(query,"name")) != 0) {
            jbr -> client.name = xmlnode_get_data(node);
          }
          if ((node = xmlnode_get_child(query,"version")) != 0) {
            jbr -> client.version = xmlnode_get_data(node);
          }
          if ((node = xmlnode_get_child(query,"os")) != 0) {
            jbr -> client.os = xmlnode_get_data(node);
          }
        }
      }
    }
    g_free(resource_name);
  }
  jabber_buddy_info_show_if_ready(jbi);
}

static void jabber_last_parse(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  JabberBuddyInfo *jbi = data;
  xmlnode *query;
  char *resource_name;
  const char *seconds;
  do {
    if (jbi != ((JabberBuddyInfo *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"jbi != NULL");
      return ;
    };
  }while (0);
  jabber_buddy_info_remove_id(jbi,id);
  if (!(from != 0)) 
    return ;
  resource_name = jabber_get_resource(from);
  if (resource_name != 0) {
    if (type == JABBER_IQ_RESULT) {
      if ((query = xmlnode_get_child(packet,"query")) != 0) {
        seconds = xmlnode_get_attrib(query,"seconds");
        if (seconds != 0) {
          char *end = (char *)((void *)0);
          long sec = strtol(seconds,&end,10);
          JabberBuddy *jb = (JabberBuddy *)((void *)0);
          char *resource = (char *)((void *)0);
          char *buddy_name = (char *)((void *)0);
          JabberBuddyResource *jbr = (JabberBuddyResource *)((void *)0);
          if (end != seconds) {
            JabberBuddyInfoResource *jbir = (g_hash_table_lookup((jbi -> resources),resource_name));
            if (jbir != 0) {
              jbir -> idle_seconds = sec;
            }
          }
/* Update the idle time of the buddy resource, if we got it.
					 This will correct the value when a server doesn't mark
					 delayed presence and we got the presence when signing on */
          jb = jabber_buddy_find(js,from,0);
          if (jb != 0) {
            resource = jabber_get_resource(from);
            buddy_name = jabber_get_bare_jid(from);
/* if the resource already has an idle time set, we
						 must have gotten it originally from a presence. In
						 this case we update it. Otherwise don't update it, to
						 avoid setting an idle and not getting informed about
						 the resource getting unidle */
            if ((resource != 0) && (buddy_name != 0)) {
              jbr = jabber_buddy_find_resource(jb,resource);
              if (jbr != 0) {
                if ((jbr -> idle) != 0L) {
                  if (sec != 0L) {
                    jbr -> idle = (time(0) - sec);
                  }
                  else {
                    jbr -> idle = 0;
                  }
                  if (jbr == jabber_buddy_find_resource(jb,0)) {
                    purple_prpl_got_user_idle(( *(js -> gc)).account,buddy_name,(jbr -> idle),(jbr -> idle));
                  }
                }
              }
            }
            g_free(resource);
            g_free(buddy_name);
          }
        }
      }
    }
    g_free(resource_name);
  }
  jabber_buddy_info_show_if_ready(jbi);
}

static void jabber_last_offline_parse(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  JabberBuddyInfo *jbi = data;
  xmlnode *query;
  const char *seconds;
  do {
    if (jbi != ((JabberBuddyInfo *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"jbi != NULL");
      return ;
    };
  }while (0);
  jabber_buddy_info_remove_id(jbi,id);
  if (type == JABBER_IQ_RESULT) {
    if ((query = xmlnode_get_child(packet,"query")) != 0) {
      seconds = xmlnode_get_attrib(query,"seconds");
      if (seconds != 0) {
        char *end = (char *)((void *)0);
        long sec = strtol(seconds,&end,10);
        if (end != seconds) {
          jbi -> last_seconds = sec;
        }
      }
      jbi -> last_message = xmlnode_get_data(query);
    }
  }
  jabber_buddy_info_show_if_ready(jbi);
}

static void jabber_time_parse(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  JabberBuddyInfo *jbi = data;
  JabberBuddyResource *jbr;
  char *resource_name;
  do {
    if (jbi != ((JabberBuddyInfo *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"jbi != NULL");
      return ;
    };
  }while (0);
  jabber_buddy_info_remove_id(jbi,id);
  if (!(from != 0)) 
    return ;
  resource_name = jabber_get_resource(from);
  jbr = ((resource_name != 0)?jabber_buddy_find_resource((jbi -> jb),resource_name) : ((struct _JabberBuddyResource *)((void *)0)));
  g_free(resource_name);
  if (jbr != 0) {
    if (type == JABBER_IQ_RESULT) {
      xmlnode *time = xmlnode_get_child(packet,"time");
      xmlnode *tzo = (time != 0)?xmlnode_get_child(time,"tzo") : ((struct _xmlnode *)((void *)0));
      char *tzo_data = (tzo != 0)?xmlnode_get_data(tzo) : ((char *)((void *)0));
      if (tzo_data != 0) {
        char *c = tzo_data;
        int hours;
        int minutes;
        if ((tzo_data[0] == 'Z') && (tzo_data[1] == 0)) {
          jbr -> tz_off = 0;
        }
        else {
          gboolean offset_positive = (tzo_data[0] == '+');
/* [+-]HH:MM */
          if ((((( *c) == '+') || (( *c) == '-')) && ((c = (c + 1)) != 0)) && (sscanf(c,"%02d:%02d",&hours,&minutes) == 2)) {
            jbr -> tz_off = ((60 * 60 * hours) + (60 * minutes));
            if (!(offset_positive != 0)) 
              jbr -> tz_off *= (-1);
          }
          else {
            purple_debug_info("jabber","Ignoring malformed timezone %s",tzo_data);
          }
        }
        g_free(tzo_data);
      }
    }
  }
  jabber_buddy_info_show_if_ready(jbi);
}

void jabber_buddy_remove_all_pending_buddy_info_requests(JabberStream *js)
{
  if ((js -> pending_buddy_info_requests) != 0) {
    JabberBuddyInfo *jbi;
    GSList *l = (js -> pending_buddy_info_requests);
    while(l != 0){
      jbi = (l -> data);
      g_slist_free((jbi -> ids));
      jabber_buddy_info_destroy(jbi);
      l = (l -> next);
    }
    g_slist_free((js -> pending_buddy_info_requests));
    js -> pending_buddy_info_requests = ((GSList *)((void *)0));
  }
}

static gboolean jabber_buddy_get_info_timeout(gpointer data)
{
  JabberBuddyInfo *jbi = data;
/* remove the pending callbacks */
  while((jbi -> ids) != 0){
    char *id = ( *(jbi -> ids)).data;
    jabber_iq_remove_callback_by_id((jbi -> js),id);
    jbi -> ids = g_slist_remove((jbi -> ids),id);
    g_free(id);
  }
  ( *(jbi -> js)).pending_buddy_info_requests = g_slist_remove(( *(jbi -> js)).pending_buddy_info_requests,jbi);
  jbi -> timeout_handle = 0;
  jabber_buddy_info_show_if_ready(jbi);
  return 0;
}

static gboolean _client_is_blacklisted(JabberBuddyResource *jbr,const char *ns)
{
/* can't be blacklisted if we don't know what you're running yet */
  if (!(jbr -> client.name != 0)) 
    return 0;
  if (!(strcmp(ns,"jabber:iq:last") != 0)) {
    if (!(strcmp(jbr -> client.name,"Trillian") != 0)) {
/* verified by nwalp 2007/05/09 */
      if (!(strcmp(jbr -> client.version,"3.1.0.121") != 0) || !(strcmp(jbr -> client.version,"3.1.7.0") != 0)) 
/* verified by nwalp 2007/09/19 */
{
        return (!0);
      }
    }
  }
  return 0;
}

static void dispatch_queries_for_resource(JabberStream *js,JabberBuddyInfo *jbi,gboolean is_bare_jid,const char *jid,JabberBuddyResource *jbr)
{
  JabberIq *iq;
  JabberBuddyInfoResource *jbir;
  char *full_jid = (char *)((void *)0);
  const char *to;
  if ((is_bare_jid != 0) && ((jbr -> name) != 0)) {
    full_jid = g_strdup_printf("%s/%s",jid,(jbr -> name));
    to = full_jid;
  }
  else 
    to = jid;
  jbir = ((JabberBuddyInfoResource *)(g_malloc0_n(1,(sizeof(JabberBuddyInfoResource )))));
  g_hash_table_insert((jbi -> resources),(g_strdup((jbr -> name))),jbir);
  if (!(jbr -> client.name != 0)) {
    iq = jabber_iq_new_query(js,JABBER_IQ_GET,"jabber:iq:version");
    xmlnode_set_attrib((iq -> node),"to",to);
    jabber_iq_set_callback(iq,jabber_version_parse,jbi);
    jbi -> ids = g_slist_prepend((jbi -> ids),(g_strdup((iq -> id))));
    jabber_iq_send(iq);
  }
/* this is to fix the feeling of irritation I get when trying
	 * to get info on a friend running Trillian, which doesn't
	 * respond (with an error or otherwise) to jabber:iq:last
	 * requests.  There are a number of Trillian users in my
	 * office. */
  if (!(_client_is_blacklisted(jbr,"jabber:iq:last") != 0)) {
    iq = jabber_iq_new_query(js,JABBER_IQ_GET,"jabber:iq:last");
    xmlnode_set_attrib((iq -> node),"to",to);
    jabber_iq_set_callback(iq,jabber_last_parse,jbi);
    jbi -> ids = g_slist_prepend((jbi -> ids),(g_strdup((iq -> id))));
    jabber_iq_send(iq);
  }
  if (((jbr -> tz_off) == (-500000)) && (!(jbr -> caps.info != 0) || (jabber_resource_has_capability(jbr,"urn:xmpp:time") != 0))) {
    xmlnode *child;
    iq = jabber_iq_new(js,JABBER_IQ_GET);
    xmlnode_set_attrib((iq -> node),"to",to);
    child = xmlnode_new_child((iq -> node),"time");
    xmlnode_set_namespace(child,"urn:xmpp:time");
    jabber_iq_set_callback(iq,jabber_time_parse,jbi);
    jbi -> ids = g_slist_prepend((jbi -> ids),(g_strdup((iq -> id))));
    jabber_iq_send(iq);
  }
  g_free(full_jid);
}

static void jabber_buddy_get_info_for_jid(JabberStream *js,const char *jid)
{
  JabberIq *iq;
  xmlnode *vcard;
  GList *resources;
  JabberBuddy *jb;
  JabberBuddyInfo *jbi;
  const char *slash;
  gboolean is_bare_jid;
  jb = jabber_buddy_find(js,jid,(!0));
/* invalid JID */
  if (!(jb != 0)) 
    return ;
  slash = (strchr(jid,'/'));
  is_bare_jid = (slash == ((const char *)((void *)0)));
  jbi = ((JabberBuddyInfo *)(g_malloc0_n(1,(sizeof(JabberBuddyInfo )))));
  jbi -> jid = g_strdup(jid);
  jbi -> js = js;
  jbi -> jb = jb;
  jbi -> resources = g_hash_table_new_full(jbir_hash,jbir_equal,g_free,jabber_buddy_info_resource_free);
  jbi -> user_info = purple_notify_user_info_new();
  iq = jabber_iq_new(js,JABBER_IQ_GET);
  xmlnode_set_attrib((iq -> node),"to",jid);
  vcard = xmlnode_new_child((iq -> node),"vCard");
  xmlnode_set_namespace(vcard,"vcard-temp");
  jabber_iq_set_callback(iq,jabber_vcard_parse,jbi);
  jbi -> ids = g_slist_prepend((jbi -> ids),(g_strdup((iq -> id))));
  jabber_iq_send(iq);
  if (is_bare_jid != 0) {
    if ((jb -> resources) != 0) {
      for (resources = (jb -> resources); resources != 0; resources = (resources -> next)) {
        JabberBuddyResource *jbr = (resources -> data);
        dispatch_queries_for_resource(js,jbi,is_bare_jid,jid,jbr);
      }
    }
    else {
/* user is offline, send a jabber:iq:last to find out last time online */
      iq = jabber_iq_new_query(js,JABBER_IQ_GET,"jabber:iq:last");
      xmlnode_set_attrib((iq -> node),"to",jid);
      jabber_iq_set_callback(iq,jabber_last_offline_parse,jbi);
      jbi -> ids = g_slist_prepend((jbi -> ids),(g_strdup((iq -> id))));
      jabber_iq_send(iq);
    }
  }
  else {
    JabberBuddyResource *jbr = jabber_buddy_find_resource(jb,(slash + 1));
    if (jbr != 0) 
      dispatch_queries_for_resource(js,jbi,is_bare_jid,jid,jbr);
    else 
      purple_debug_warning("jabber","jabber_buddy_get_info_for_jid() was passed JID %s, but there is no corresponding JabberBuddyResource!\n",jid);
  }
  js -> pending_buddy_info_requests = g_slist_prepend((js -> pending_buddy_info_requests),jbi);
  jbi -> timeout_handle = purple_timeout_add_seconds(30,jabber_buddy_get_info_timeout,jbi);
}

void jabber_buddy_get_info(PurpleConnection *gc,const char *who)
{
  JabberStream *js = (purple_connection_get_protocol_data(gc));
  JabberID *jid = jabber_id_new(who);
  if (!(jid != 0)) 
    return ;
  if (((jid -> node) != 0) && (jabber_chat_find(js,(jid -> node),(jid -> domain)) != 0)) {
/* For a conversation, include the resource (indicates the user). */
    jabber_buddy_get_info_for_jid(js,who);
  }
  else {
    char *bare_jid = jabber_get_bare_jid(who);
    jabber_buddy_get_info_for_jid(js,bare_jid);
    g_free(bare_jid);
  }
  jabber_id_free(jid);
}

static void jabber_buddy_set_invisibility(JabberStream *js,const char *who,gboolean invisible)
{
  PurplePresence *gpresence;
  PurpleAccount *account;
  PurpleStatus *status;
  JabberBuddy *jb = jabber_buddy_find(js,who,(!0));
  xmlnode *presence;
  JabberBuddyState state;
  char *msg;
  int priority;
  account = purple_connection_get_account((js -> gc));
  gpresence = purple_account_get_presence(account);
  status = purple_presence_get_active_status(gpresence);
  purple_status_to_jabber(status,&state,&msg,&priority);
  presence = jabber_presence_create_js(js,state,msg,priority);
  g_free(msg);
  xmlnode_set_attrib(presence,"to",who);
  if (invisible != 0) {
    xmlnode_set_attrib(presence,"type","invisible");
    jb -> invisible |= JABBER_INVIS_BUDDY;
  }
  else {
    jb -> invisible &= (~JABBER_INVIS_BUDDY);
  }
  jabber_send(js,presence);
  xmlnode_free(presence);
}

static void jabber_buddy_make_invisible(PurpleBlistNode *node,gpointer data)
{
  PurpleBuddy *buddy;
  PurpleConnection *gc;
  JabberStream *js;
  do {
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_BLIST_NODE_IS_BUDDY(node)");
      return ;
    };
  }while (0);
  buddy = ((PurpleBuddy *)node);
  gc = purple_account_get_connection((purple_buddy_get_account(buddy)));
  js = (purple_connection_get_protocol_data(gc));
  jabber_buddy_set_invisibility(js,purple_buddy_get_name(buddy),(!0));
}

static void jabber_buddy_make_visible(PurpleBlistNode *node,gpointer data)
{
  PurpleBuddy *buddy;
  PurpleConnection *gc;
  JabberStream *js;
  do {
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_BLIST_NODE_IS_BUDDY(node)");
      return ;
    };
  }while (0);
  buddy = ((PurpleBuddy *)node);
  gc = purple_account_get_connection((purple_buddy_get_account(buddy)));
  js = (purple_connection_get_protocol_data(gc));
  jabber_buddy_set_invisibility(js,purple_buddy_get_name(buddy),0);
}

static void cancel_presence_notification(gpointer data)
{
  PurpleBuddy *buddy;
  PurpleConnection *gc;
  JabberStream *js;
  buddy = data;
  gc = purple_account_get_connection((purple_buddy_get_account(buddy)));
  js = (purple_connection_get_protocol_data(gc));
  jabber_presence_subscription_set(js,purple_buddy_get_name(buddy),"unsubscribed");
}

static void jabber_buddy_cancel_presence_notification(PurpleBlistNode *node,gpointer data)
{
  PurpleBuddy *buddy;
  PurpleAccount *account;
  PurpleConnection *gc;
  const gchar *name;
  char *msg;
  do {
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_BLIST_NODE_IS_BUDDY(node)");
      return ;
    };
  }while (0);
  buddy = ((PurpleBuddy *)node);
  name = purple_buddy_get_name(buddy);
  account = purple_buddy_get_account(buddy);
  gc = purple_account_get_connection(account);
  msg = g_strdup_printf(((const char *)(dgettext("pidgin","%s will no longer be able to see your status updates.  Do you want to continue\?"))),name);
  purple_request_action(gc,0,((const char *)(dgettext("pidgin","Cancel Presence Notification"))),msg,0,account,name,0,buddy,2,((const char *)(dgettext("pidgin","_Yes"))),cancel_presence_notification,((const char *)(dgettext("pidgin","_No"))),((void *)((void *)0)));
/* Yes */
/* Do nothing */
  g_free(msg);
}

static void jabber_buddy_rerequest_auth(PurpleBlistNode *node,gpointer data)
{
  PurpleBuddy *buddy;
  PurpleConnection *gc;
  JabberStream *js;
  do {
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_BLIST_NODE_IS_BUDDY(node)");
      return ;
    };
  }while (0);
  buddy = ((PurpleBuddy *)node);
  gc = purple_account_get_connection((purple_buddy_get_account(buddy)));
  js = (purple_connection_get_protocol_data(gc));
  jabber_presence_subscription_set(js,purple_buddy_get_name(buddy),"subscribe");
}

static void jabber_buddy_unsubscribe(PurpleBlistNode *node,gpointer data)
{
  PurpleBuddy *buddy;
  PurpleConnection *gc;
  JabberStream *js;
  do {
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_BLIST_NODE_IS_BUDDY(node)");
      return ;
    };
  }while (0);
  buddy = ((PurpleBuddy *)node);
  gc = purple_account_get_connection((purple_buddy_get_account(buddy)));
  js = (purple_connection_get_protocol_data(gc));
  jabber_presence_subscription_set(js,purple_buddy_get_name(buddy),"unsubscribe");
}

static void jabber_buddy_login(PurpleBlistNode *node,gpointer data)
{
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
/* simply create a directed presence of the current status */
    PurpleBuddy *buddy = (PurpleBuddy *)node;
    PurpleConnection *gc = purple_account_get_connection((purple_buddy_get_account(buddy)));
    JabberStream *js = (purple_connection_get_protocol_data(gc));
    PurpleAccount *account = purple_connection_get_account(gc);
    PurplePresence *gpresence = purple_account_get_presence(account);
    PurpleStatus *status = purple_presence_get_active_status(gpresence);
    xmlnode *presence;
    JabberBuddyState state;
    char *msg;
    int priority;
    purple_status_to_jabber(status,&state,&msg,&priority);
    presence = jabber_presence_create_js(js,state,msg,priority);
    g_free(msg);
    xmlnode_set_attrib(presence,"to",purple_buddy_get_name(buddy));
    jabber_send(js,presence);
    xmlnode_free(presence);
  }
}

static void jabber_buddy_logout(PurpleBlistNode *node,gpointer data)
{
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
/* simply create a directed unavailable presence */
    PurpleBuddy *buddy = (PurpleBuddy *)node;
    PurpleConnection *gc = purple_account_get_connection((purple_buddy_get_account(buddy)));
    JabberStream *js = (purple_connection_get_protocol_data(gc));
    xmlnode *presence;
    presence = jabber_presence_create_js(js,JABBER_BUDDY_STATE_UNAVAILABLE,0,0);
    xmlnode_set_attrib(presence,"to",purple_buddy_get_name(buddy));
    jabber_send(js,presence);
    xmlnode_free(presence);
  }
}

static GList *jabber_buddy_menu(PurpleBuddy *buddy)
{
  PurpleConnection *gc = purple_account_get_connection((purple_buddy_get_account(buddy)));
  JabberStream *js = (purple_connection_get_protocol_data(gc));
  const char *name = purple_buddy_get_name(buddy);
  JabberBuddy *jb = jabber_buddy_find(js,name,(!0));
  GList *jbrs;
  GList *m = (GList *)((void *)0);
  PurpleMenuAction *act;
  if (!(jb != 0)) 
    return m;
  if (((js -> protocol_version.major == 0) && (js -> protocol_version.minor == 9)) && (jb != (js -> user_jb))) {
    if (((jb -> invisible) & JABBER_INVIS_BUDDY) != 0U) {
      act = purple_menu_action_new(((const char *)(dgettext("pidgin","Un-hide From"))),((PurpleCallback )jabber_buddy_make_visible),0,0);
    }
    else {
      act = purple_menu_action_new(((const char *)(dgettext("pidgin","Temporarily Hide From"))),((PurpleCallback )jabber_buddy_make_invisible),0,0);
    }
    m = g_list_append(m,act);
  }
  if ((((jb -> subscription) & JABBER_SUB_FROM) != 0U) && (jb != (js -> user_jb))) {
    act = purple_menu_action_new(((const char *)(dgettext("pidgin","Cancel Presence Notification"))),((PurpleCallback )jabber_buddy_cancel_presence_notification),0,0);
    m = g_list_append(m,act);
  }
  if (!(((jb -> subscription) & JABBER_SUB_TO) != 0U)) {
    act = purple_menu_action_new(((const char *)(dgettext("pidgin","(Re-)Request authorization"))),((PurpleCallback )jabber_buddy_rerequest_auth),0,0);
    m = g_list_append(m,act);
  }
  else if (jb != (js -> user_jb)) {
/* shouldn't this just happen automatically when the buddy is
		   removed? */
    act = purple_menu_action_new(((const char *)(dgettext("pidgin","Unsubscribe"))),((PurpleCallback )jabber_buddy_unsubscribe),0,0);
    m = g_list_append(m,act);
  }
  if ((js -> googletalk) != 0) {
    act = purple_menu_action_new(((const char *)(dgettext("pidgin","Initiate _Chat"))),((PurpleCallback )google_buddy_node_chat),0,0);
    m = g_list_append(m,act);
  }
/*
	 * This if-condition implements parts of XEP-0100: Gateway Interaction
	 *
	 * According to stpeter, there is no way to know if a jid on the roster is a gateway without sending a disco#info.
	 * However, since the gateway might appear offline to us, we cannot get that information. Therefore, I just assume
	 * that gateways on the roster can be identified by having no '@' in their jid. This is a faily safe assumption, since
	 * people don't tend to have a server or other service there.
	 *
	 * TODO: Use disco#info...
	 */
  if (strchr(name,64) == ((char *)((void *)0))) {
    act = purple_menu_action_new(((const char *)(dgettext("pidgin","Log In"))),((PurpleCallback )jabber_buddy_login),0,0);
    m = g_list_append(m,act);
    act = purple_menu_action_new(((const char *)(dgettext("pidgin","Log Out"))),((PurpleCallback )jabber_buddy_logout),0,0);
    m = g_list_append(m,act);
  }
/* add all ad hoc commands to the action menu */
  for (jbrs = (jb -> resources); jbrs != 0; jbrs = ((jbrs != 0)?( *((GList *)jbrs)).next : ((struct _GList *)((void *)0)))) {{
      JabberBuddyResource *jbr = (jbrs -> data);
      GList *commands;
      if (!((jbr -> commands) != 0)) 
        continue; 
      for (commands = (jbr -> commands); commands != 0; commands = ((commands != 0)?( *((GList *)commands)).next : ((struct _GList *)((void *)0)))) {
        JabberAdHocCommands *cmd = (commands -> data);
        act = purple_menu_action_new((cmd -> name),((PurpleCallback )jabber_adhoc_execute_action),cmd,0);
        m = g_list_append(m,act);
      }
    }
  }
  return m;
}

GList *jabber_blist_node_menu(PurpleBlistNode *node)
{
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    return jabber_buddy_menu(((PurpleBuddy *)node));
  }
  else {
    return 0;
  }
}

static void user_search_result_add_buddy_cb(PurpleConnection *gc,GList *row,void *user_data)
{
/* XXX find out the jid */
  purple_blist_request_add_buddy(purple_connection_get_account(gc),(g_list_nth_data(row,0)),0,0);
}

static void user_search_result_cb(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  PurpleNotifySearchResults *results;
  PurpleNotifySearchColumn *column;
  xmlnode *x;
  xmlnode *query;
  xmlnode *item;
  xmlnode *field;
/* XXX error checking? */
  if (!((query = xmlnode_get_child(packet,"query")) != 0)) 
    return ;
  results = purple_notify_searchresults_new();
  if ((x = xmlnode_get_child_with_namespace(query,"x","jabber:x:data")) != 0) {
    xmlnode *reported;
    GSList *column_vars = (GSList *)((void *)0);
    purple_debug_info("jabber","new-skool\n");
    if ((reported = xmlnode_get_child(x,"reported")) != 0) {
      xmlnode *field = xmlnode_get_child(reported,"field");
      while(field != 0){
        const char *var = xmlnode_get_attrib(field,"var");
        const char *label = xmlnode_get_attrib(field,"label");
        if (var != 0) {
          column = purple_notify_searchresults_column_new(((label != 0)?label : var));
          purple_notify_searchresults_column_add(results,column);
          column_vars = g_slist_append(column_vars,((char *)var));
        }
        field = xmlnode_get_next_twin(field);
      }
    }
    item = xmlnode_get_child(x,"item");
    while(item != 0){
      GList *row = (GList *)((void *)0);
      GSList *l;
      xmlnode *valuenode;
      const char *var;
      for (l = column_vars; l != ((GSList *)((void *)0)); l = (l -> next)) {{
/*
				 * Build a row containing the strings that correspond
				 * to each column of the search results.
				 */
          for (field = xmlnode_get_child(item,"field"); field != ((xmlnode *)((void *)0)); field = xmlnode_get_next_twin(field)) {
            if ((((var = xmlnode_get_attrib(field,"var")) != 0) && !(strcmp(var,(l -> data)) != 0)) && ((valuenode = xmlnode_get_child(field,"value")) != 0)) {
              char *value = xmlnode_get_data(valuenode);
              row = g_list_append(row,value);
              break; 
            }
          }
        }
        if (field == ((xmlnode *)((void *)0))) 
/* No data for this column */
          row = g_list_append(row,0);
      }
      purple_notify_searchresults_row_add(results,row);
      item = xmlnode_get_next_twin(item);
    }
    g_slist_free(column_vars);
  }
  else {
/* old skool */
    purple_debug_info("jabber","old-skool\n");
    column = purple_notify_searchresults_column_new(((const char *)(dgettext("pidgin","JID"))));
    purple_notify_searchresults_column_add(results,column);
    column = purple_notify_searchresults_column_new(((const char *)(dgettext("pidgin","First Name"))));
    purple_notify_searchresults_column_add(results,column);
    column = purple_notify_searchresults_column_new(((const char *)(dgettext("pidgin","Last Name"))));
    purple_notify_searchresults_column_add(results,column);
    column = purple_notify_searchresults_column_new(((const char *)(dgettext("pidgin","Nickname"))));
    purple_notify_searchresults_column_add(results,column);
    column = purple_notify_searchresults_column_new(((const char *)(dgettext("pidgin","Email"))));
    purple_notify_searchresults_column_add(results,column);
    for (item = xmlnode_get_child(query,"item"); item != 0; item = xmlnode_get_next_twin(item)) {{
        const char *jid;
        xmlnode *node;
        GList *row = (GList *)((void *)0);
        if (!((jid = xmlnode_get_attrib(item,"jid")) != 0)) 
          continue; 
        row = g_list_append(row,(g_strdup(jid)));
        node = xmlnode_get_child(item,"first");
        row = g_list_append(row,(((node != 0)?xmlnode_get_data(node) : ((char *)((void *)0)))));
        node = xmlnode_get_child(item,"last");
        row = g_list_append(row,(((node != 0)?xmlnode_get_data(node) : ((char *)((void *)0)))));
        node = xmlnode_get_child(item,"nick");
        row = g_list_append(row,(((node != 0)?xmlnode_get_data(node) : ((char *)((void *)0)))));
        node = xmlnode_get_child(item,"email");
        row = g_list_append(row,(((node != 0)?xmlnode_get_data(node) : ((char *)((void *)0)))));
        purple_debug_info("jabber","row=%p\n",row);
        purple_notify_searchresults_row_add(results,row);
      }
    }
  }
  purple_notify_searchresults_button_add(results,PURPLE_NOTIFY_BUTTON_ADD,user_search_result_add_buddy_cb);
  purple_notify_searchresults((js -> gc),0,0,((const char *)(dgettext("pidgin","The following are the results of your search"))),results,0,0);
}

static void user_search_x_data_cb(JabberStream *js,xmlnode *result,gpointer data)
{
  xmlnode *query;
  JabberIq *iq;
  char *dir_server = data;
  const char *type;
/* if they've cancelled the search, we're
	 * just going to get an error if we send
	 * a cancel, so skip it */
  type = xmlnode_get_attrib(result,"type");
  if ((type != 0) && !(strcmp(type,"cancel") != 0)) {
    g_free(dir_server);
    return ;
  }
  iq = jabber_iq_new_query(js,JABBER_IQ_SET,"jabber:iq:search");
  query = xmlnode_get_child((iq -> node),"query");
  xmlnode_insert_child(query,result);
  jabber_iq_set_callback(iq,user_search_result_cb,0);
  xmlnode_set_attrib((iq -> node),"to",dir_server);
  jabber_iq_send(iq);
  g_free(dir_server);
}

struct user_search_info 
{
  JabberStream *js;
  char *directory_server;
}
;

static void user_search_cancel_cb(struct user_search_info *usi,PurpleRequestFields *fields)
{
  g_free((usi -> directory_server));
  g_free(usi);
}

static void user_search_cb(struct user_search_info *usi,PurpleRequestFields *fields)
{
  JabberStream *js = (usi -> js);
  JabberIq *iq;
  xmlnode *query;
  GList *groups;
  GList *flds;
  iq = jabber_iq_new_query(js,JABBER_IQ_SET,"jabber:iq:search");
  query = xmlnode_get_child((iq -> node),"query");
  for (groups = purple_request_fields_get_groups(fields); groups != 0; groups = (groups -> next)) {
    for (flds = purple_request_field_group_get_fields((groups -> data)); flds != 0; flds = (flds -> next)) {
      PurpleRequestField *field = (flds -> data);
      const char *id = purple_request_field_get_id(field);
      const char *value = purple_request_field_string_get_value(field);
      if ((value != 0) && (((!(strcmp(id,"first") != 0) || !(strcmp(id,"last") != 0)) || !(strcmp(id,"nick") != 0)) || !(strcmp(id,"email") != 0))) {
        xmlnode *y = xmlnode_new_child(query,id);
        xmlnode_insert_data(y,value,(-1));
      }
    }
  }
  jabber_iq_set_callback(iq,user_search_result_cb,0);
  xmlnode_set_attrib((iq -> node),"to",(usi -> directory_server));
  jabber_iq_send(iq);
  g_free((usi -> directory_server));
  g_free(usi);
}
#if 0
/* This is for gettext only -- it will see this even though there's an #if 0. */
/*
 * An incomplete list of server generated original language search
 * comments for Jabber User Directories
 *
 * See discussion thread "Search comment for Jabber is not translatable"
 * in purple-i18n@lists.sourceforge.net (March 2006)
 */
/* current comment from Jabber User Directory users.jabber.org */
#endif

static void user_search_fields_result_cb(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  xmlnode *query;
  xmlnode *x;
  if (!(from != 0)) 
    return ;
  if (type == JABBER_IQ_ERROR) {
    char *msg = jabber_parse_error(js,packet,0);
    if (!(msg != 0)) 
      msg = g_strdup(((const char *)(dgettext("pidgin","Unknown error"))));
    purple_notify_message((js -> gc),PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Directory Query Failed"))),((const char *)(dgettext("pidgin","Could not query the directory server."))),msg,0,0);
    g_free(msg);
    return ;
  }
  if (!((query = xmlnode_get_child(packet,"query")) != 0)) 
    return ;
  if ((x = xmlnode_get_child_with_namespace(query,"x","jabber:x:data")) != 0) {
    jabber_x_data_request(js,x,user_search_x_data_cb,(g_strdup(from)));
    return ;
  }
  else {
    struct user_search_info *usi;
    xmlnode *instnode;
    char *instructions = (char *)((void *)0);
    PurpleRequestFields *fields;
    PurpleRequestFieldGroup *group;
    PurpleRequestField *field;
/* old skool */
    fields = purple_request_fields_new();
    group = purple_request_field_group_new(0);
    purple_request_fields_add_group(fields,group);
    if ((instnode = xmlnode_get_child(query,"instructions")) != 0) {
      char *tmp = xmlnode_get_data(instnode);
      if (tmp != 0) {
/* Try to translate the message (see static message
				   list in jabber_user_dir_comments[]) */
        instructions = g_strdup_printf(((const char *)(dgettext("pidgin","Server Instructions: %s"))),((const char *)(dgettext("pidgin",tmp))));
        g_free(tmp);
      }
    }
    if (!(instructions != 0)) {
      instructions = g_strdup(((const char *)(dgettext("pidgin","Fill in one or more fields to search for any matching XMPP users."))));
    }
    if (xmlnode_get_child(query,"first") != 0) {
      field = purple_request_field_string_new("first",((const char *)(dgettext("pidgin","First Name"))),0,0);
      purple_request_field_group_add_field(group,field);
    }
    if (xmlnode_get_child(query,"last") != 0) {
      field = purple_request_field_string_new("last",((const char *)(dgettext("pidgin","Last Name"))),0,0);
      purple_request_field_group_add_field(group,field);
    }
    if (xmlnode_get_child(query,"nick") != 0) {
      field = purple_request_field_string_new("nick",((const char *)(dgettext("pidgin","Nickname"))),0,0);
      purple_request_field_group_add_field(group,field);
    }
    if (xmlnode_get_child(query,"email") != 0) {
      field = purple_request_field_string_new("email",((const char *)(dgettext("pidgin","Email Address"))),0,0);
      purple_request_field_group_add_field(group,field);
    }
    usi = ((struct user_search_info *)(g_malloc0_n(1,(sizeof(struct user_search_info )))));
    usi -> js = js;
    usi -> directory_server = g_strdup(from);
    purple_request_fields((js -> gc),((const char *)(dgettext("pidgin","Search for XMPP users"))),((const char *)(dgettext("pidgin","Search for XMPP users"))),instructions,fields,((const char *)(dgettext("pidgin","Search"))),((GCallback )user_search_cb),((const char *)(dgettext("pidgin","Cancel"))),((GCallback )user_search_cancel_cb),purple_connection_get_account((js -> gc)),0,0,usi);
    g_free(instructions);
  }
}

void jabber_user_search(JabberStream *js,const char *directory)
{
  JabberIq *iq;
/* XXX: should probably better validate the directory we're given */
  if (!(directory != 0) || !(( *directory) != 0)) {
    purple_notify_message((js -> gc),PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Invalid Directory"))),((const char *)(dgettext("pidgin","Invalid Directory"))),0,0,0);
    return ;
  }
/* If the value provided isn't the disco#info default, persist it.  Otherwise,
	   make sure we aren't persisting an old value */
  if ((((js -> user_directories) != 0) && (( *(js -> user_directories)).data != 0)) && !(strcmp(directory,( *(js -> user_directories)).data) != 0)) {
    purple_account_set_string(( *(js -> gc)).account,"user_directory","");
  }
  else {
    purple_account_set_string(( *(js -> gc)).account,"user_directory",directory);
  }
  iq = jabber_iq_new_query(js,JABBER_IQ_GET,"jabber:iq:search");
  xmlnode_set_attrib((iq -> node),"to",directory);
  jabber_iq_set_callback(iq,user_search_fields_result_cb,0);
  jabber_iq_send(iq);
}

void jabber_user_search_begin(PurplePluginAction *action)
{
  PurpleConnection *gc = (PurpleConnection *)(action -> context);
  JabberStream *js = (purple_connection_get_protocol_data(gc));
  const char *def_val = purple_account_get_string(( *(js -> gc)).account,"user_directory","");
  if (!(( *def_val) != 0) && ((js -> user_directories) != 0)) 
    def_val = ( *(js -> user_directories)).data;
  purple_request_input(gc,((const char *)(dgettext("pidgin","Enter a User Directory"))),((const char *)(dgettext("pidgin","Enter a User Directory"))),((const char *)(dgettext("pidgin","Select a user directory to search"))),def_val,0,0,0,((const char *)(dgettext("pidgin","Search Directory"))),((PurpleCallback )jabber_user_search),((const char *)(dgettext("pidgin","Cancel"))),0,0,0,0,js);
}

gboolean jabber_resource_know_capabilities(const JabberBuddyResource *jbr)
{
  return jbr -> caps.info != ((JabberCapsClientInfo *)((void *)0));
}

gboolean jabber_resource_has_capability(const JabberBuddyResource *jbr,const gchar *cap)
{
  const GList *node = (const GList *)((void *)0);
  const JabberCapsNodeExts *exts;
  if (!(jbr -> caps.info != 0)) {
    purple_debug_info("jabber","Unable to find caps: nothing known about buddy\n");
    return 0;
  }
  node = (g_list_find_custom(( *jbr -> caps.info).features,cap,((GCompareFunc )strcmp)));
  if ((!(node != 0) && (jbr -> caps.exts != 0)) && (( *jbr -> caps.info).exts != 0)) {
    const GList *ext;
    exts = ( *jbr -> caps.info).exts;
/* Walk through all the enabled caps, checking each list for the cap.
		 * Don't check it twice, though. */
    for (ext = jbr -> caps.exts; (ext != 0) && !(node != 0); ext = (ext -> next)) {
      GList *features = (g_hash_table_lookup((exts -> exts),(ext -> data)));
      if (features != 0) 
        node = (g_list_find_custom(features,cap,((GCompareFunc )strcmp)));
    }
  }
  return node != ((const GList *)((void *)0));
}

gboolean jabber_buddy_has_capability(const JabberBuddy *jb,const gchar *cap)
{
  JabberBuddyResource *jbr = jabber_buddy_find_resource(((JabberBuddy *)jb),0);
  if (!(jbr != 0)) {
    purple_debug_info("jabber","Unable to find caps: buddy might be offline\n");
    return 0;
  }
  return jabber_resource_has_capability(jbr,cap);
}

const gchar *jabber_resource_get_identity_category_type(const JabberBuddyResource *jbr,const gchar *category)
{
  const GList *iter = (const GList *)((void *)0);
  if (jbr -> caps.info != 0) {
    for (iter = ( *jbr -> caps.info).identities; iter != 0; iter = (((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0))))) {
      const JabberIdentity *identity = ((JabberIdentity *)(iter -> data));
      if (strcmp((identity -> category),category) == 0) {
        return (identity -> type);
      }
    }
  }
  return 0;
}
