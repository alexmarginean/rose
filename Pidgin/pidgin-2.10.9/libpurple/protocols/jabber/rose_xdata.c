/*
 * purple - Jabber Protocol Plugin
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "request.h"
#include "server.h"
#include "xdata.h"
typedef enum __unnamed_enum___F0_L29_C9_JABBER_X_DATA_IGNORE__COMMA__JABBER_X_DATA_TEXT_SINGLE__COMMA__JABBER_X_DATA_TEXT_MULTI__COMMA__JABBER_X_DATA_LIST_SINGLE__COMMA__JABBER_X_DATA_LIST_MULTI__COMMA__JABBER_X_DATA_BOOLEAN__COMMA__JABBER_X_DATA_JID_SINGLE {JABBER_X_DATA_IGNORE,JABBER_X_DATA_TEXT_SINGLE,JABBER_X_DATA_TEXT_MULTI,JABBER_X_DATA_LIST_SINGLE,JABBER_X_DATA_LIST_MULTI,JABBER_X_DATA_BOOLEAN,JABBER_X_DATA_JID_SINGLE}jabber_x_data_field_type;

struct jabber_x_data_data 
{
  GHashTable *fields;
  GSList *values;
  jabber_x_data_action_cb cb;
  gpointer user_data;
  JabberStream *js;
  GList *actions;
  PurpleRequestFieldGroup *actiongroup;
}
;

static void jabber_x_data_ok_cb(struct jabber_x_data_data *data,PurpleRequestFields *fields)
{
  xmlnode *result = xmlnode_new("x");
  jabber_x_data_action_cb cb = (data -> cb);
  gpointer user_data = (data -> user_data);
  JabberStream *js = (data -> js);
  GList *groups;
  GList *flds;
  char *actionhandle = (char *)((void *)0);
  gboolean hasActions = ((data -> actions) != ((GList *)((void *)0)));
  xmlnode_set_namespace(result,"jabber:x:data");
  xmlnode_set_attrib(result,"type","submit");
  for (groups = purple_request_fields_get_groups(fields); groups != 0; groups = (groups -> next)) {
    if ((groups -> data) == (data -> actiongroup)) {{
        for (flds = purple_request_field_group_get_fields((groups -> data)); flds != 0; flds = (flds -> next)) {{
            PurpleRequestField *field = (flds -> data);
            const char *id = purple_request_field_get_id(field);
            int handleindex;
            if (strcmp(id,"libpurple:jabber:xdata:actions") != 0) 
              continue; 
            handleindex = purple_request_field_choice_get_value(field);
            actionhandle = g_strdup((g_list_nth_data((data -> actions),handleindex)));
            break; 
          }
        }
      }
      continue; 
    }
    for (flds = purple_request_field_group_get_fields((groups -> data)); flds != 0; flds = (flds -> next)) {
      xmlnode *fieldnode;
      xmlnode *valuenode;
      PurpleRequestField *field = (flds -> data);
      const char *id = purple_request_field_get_id(field);
      jabber_x_data_field_type type = ((gint )((glong )(g_hash_table_lookup((data -> fields),id))));
{
        switch(type){
          case JABBER_X_DATA_TEXT_SINGLE:
{
          }
          case JABBER_X_DATA_JID_SINGLE:
{
{
              const char *value = purple_request_field_string_get_value(field);
              if (value == ((const char *)((void *)0))) 
                break; 
              fieldnode = xmlnode_new_child(result,"field");
              xmlnode_set_attrib(fieldnode,"var",id);
              valuenode = xmlnode_new_child(fieldnode,"value");
              if (value != 0) 
                xmlnode_insert_data(valuenode,value,(-1));
              break; 
            }
          }
          case JABBER_X_DATA_TEXT_MULTI:
{
{
              char **pieces;
              char **p;
              const char *value = purple_request_field_string_get_value(field);
              if (value == ((const char *)((void *)0))) 
                break; 
              fieldnode = xmlnode_new_child(result,"field");
              xmlnode_set_attrib(fieldnode,"var",id);
              pieces = g_strsplit(value,"\n",(-1));
              for (p = pieces;  *p != ((char *)((void *)0)); p++) {
                valuenode = xmlnode_new_child(fieldnode,"value");
                xmlnode_insert_data(valuenode,( *p),(-1));
              }
              g_strfreev(pieces);
            }
            break; 
          }
          case JABBER_X_DATA_LIST_SINGLE:
{
          }
          case JABBER_X_DATA_LIST_MULTI:
{
{
              GList *selected = purple_request_field_list_get_selected(field);
              char *value;
              fieldnode = xmlnode_new_child(result,"field");
              xmlnode_set_attrib(fieldnode,"var",id);
              while(selected != 0){
                value = (purple_request_field_list_get_data(field,(selected -> data)));
                valuenode = xmlnode_new_child(fieldnode,"value");
                if (value != 0) 
                  xmlnode_insert_data(valuenode,value,(-1));
                selected = (selected -> next);
              }
            }
            break; 
          }
          case JABBER_X_DATA_BOOLEAN:
{
            fieldnode = xmlnode_new_child(result,"field");
            xmlnode_set_attrib(fieldnode,"var",id);
            valuenode = xmlnode_new_child(fieldnode,"value");
            if (purple_request_field_bool_get_value(field) != 0) 
              xmlnode_insert_data(valuenode,"1",(-1));
            else 
              xmlnode_insert_data(valuenode,"0",(-1));
            break; 
          }
          case JABBER_X_DATA_IGNORE:
{
            break; 
          }
        }
      }
    }
  }
  g_hash_table_destroy((data -> fields));
  while((data -> values) != 0){
    g_free(( *(data -> values)).data);
    data -> values = g_slist_delete_link((data -> values),(data -> values));
  }
  if ((data -> actions) != 0) {
    GList *action;
    for (action = (data -> actions); action != 0; action = ((action != 0)?( *((GList *)action)).next : ((struct _GList *)((void *)0)))) {
      g_free((action -> data));
    }
    g_list_free((data -> actions));
  }
  g_free(data);
  if (hasActions != 0) 
    ( *cb)(js,result,actionhandle,user_data);
  else 
    ( *((jabber_x_data_cb )cb))(js,result,user_data);
  g_free(actionhandle);
}

static void jabber_x_data_cancel_cb(struct jabber_x_data_data *data,PurpleRequestFields *fields)
{
  xmlnode *result = xmlnode_new("x");
  jabber_x_data_action_cb cb = (data -> cb);
  gpointer user_data = (data -> user_data);
  JabberStream *js = (data -> js);
  gboolean hasActions = 0;
  g_hash_table_destroy((data -> fields));
  while((data -> values) != 0){
    g_free(( *(data -> values)).data);
    data -> values = g_slist_delete_link((data -> values),(data -> values));
  }
  if ((data -> actions) != 0) {
    GList *action;
    hasActions = (!0);
    for (action = (data -> actions); action != 0; action = ((action != 0)?( *((GList *)action)).next : ((struct _GList *)((void *)0)))) {
      g_free((action -> data));
    }
    g_list_free((data -> actions));
  }
  g_free(data);
  xmlnode_set_namespace(result,"jabber:x:data");
  xmlnode_set_attrib(result,"type","cancel");
  if (hasActions != 0) 
    ( *cb)(js,result,0,user_data);
  else 
    ( *((jabber_x_data_cb )cb))(js,result,user_data);
}

void *jabber_x_data_request(JabberStream *js,xmlnode *packet,jabber_x_data_cb cb,gpointer user_data)
{
  return jabber_x_data_request_with_actions(js,packet,0,0,((jabber_x_data_action_cb )cb),user_data);
}

void *jabber_x_data_request_with_actions(JabberStream *js,xmlnode *packet,GList *actions,int defaultaction,jabber_x_data_action_cb cb,gpointer user_data)
{
  void *handle;
  xmlnode *fn;
  xmlnode *x;
  PurpleRequestFields *fields;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field = (PurpleRequestField *)((void *)0);
  char *title = (char *)((void *)0);
  char *instructions = (char *)((void *)0);
  struct jabber_x_data_data *data = (struct jabber_x_data_data *)(g_malloc0_n(1,(sizeof(struct jabber_x_data_data ))));
  data -> fields = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,0);
  data -> user_data = user_data;
  data -> cb = cb;
  data -> js = js;
  fields = purple_request_fields_new();
  group = purple_request_field_group_new(0);
  purple_request_fields_add_group(fields,group);
  for (fn = xmlnode_get_child(packet,"field"); fn != 0; fn = xmlnode_get_next_twin(fn)) {{
      xmlnode *valuenode;
      const char *type = xmlnode_get_attrib(fn,"type");
      const char *label = xmlnode_get_attrib(fn,"label");
      const char *var = xmlnode_get_attrib(fn,"var");
      char *value = (char *)((void *)0);
      if (!(type != 0)) 
        type = "text-single";
      if (!(var != 0) && (strcmp(type,"fixed") != 0)) 
        continue; 
      if (!(label != 0)) 
        label = var;
      if (!(strcmp(type,"text-private") != 0)) {
        if ((valuenode = xmlnode_get_child(fn,"value")) != 0) 
          value = xmlnode_get_data(valuenode);
        field = purple_request_field_string_new(var,label,(((value != 0)?value : "")),0);
        purple_request_field_string_set_masked(field,(!0));
        purple_request_field_group_add_field(group,field);
        g_hash_table_replace((data -> fields),(g_strdup(var)),((gpointer )((gpointer )((glong )JABBER_X_DATA_TEXT_SINGLE))));
        g_free(value);
      }
      else if (!(strcmp(type,"text-multi") != 0) || !(strcmp(type,"jid-multi") != 0)) {
        GString *str = g_string_new("");
        for (valuenode = xmlnode_get_child(fn,"value"); valuenode != 0; valuenode = xmlnode_get_next_twin(valuenode)) {
          if (!((value = xmlnode_get_data(valuenode)) != 0)) 
            continue; 
          g_string_append_printf(str,"%s\n",value);
          g_free(value);
        }
        field = purple_request_field_string_new(var,label,(str -> str),(!0));
        purple_request_field_group_add_field(group,field);
        g_hash_table_replace((data -> fields),(g_strdup(var)),((gpointer )((gpointer )((glong )JABBER_X_DATA_TEXT_MULTI))));
        g_string_free(str,(!0));
      }
      else if (!(strcmp(type,"list-single") != 0) || !(strcmp(type,"list-multi") != 0)) {
        xmlnode *optnode;
        GList *selected = (GList *)((void *)0);
        field = purple_request_field_list_new(var,label);
        if (!(strcmp(type,"list-multi") != 0)) {
          purple_request_field_list_set_multi_select(field,(!0));
          g_hash_table_replace((data -> fields),(g_strdup(var)),((gpointer )((gpointer )((glong )JABBER_X_DATA_LIST_MULTI))));
        }
        else {
          g_hash_table_replace((data -> fields),(g_strdup(var)),((gpointer )((gpointer )((glong )JABBER_X_DATA_LIST_SINGLE))));
        }
        for (valuenode = xmlnode_get_child(fn,"value"); valuenode != 0; valuenode = xmlnode_get_next_twin(valuenode)) {
          char *data = xmlnode_get_data(valuenode);
          if (data != ((char *)((void *)0))) {
            selected = g_list_prepend(selected,data);
          }
        }
        for (optnode = xmlnode_get_child(fn,"option"); optnode != 0; optnode = xmlnode_get_next_twin(optnode)) {{
            const char *lbl;
            if (!((valuenode = xmlnode_get_child(optnode,"value")) != 0)) 
              continue; 
            if (!((value = xmlnode_get_data(valuenode)) != 0)) 
              continue; 
            if (!((lbl = xmlnode_get_attrib(optnode,"label")) != 0)) 
              lbl = value;
            data -> values = g_slist_prepend((data -> values),value);
            purple_request_field_list_add_icon(field,lbl,0,value);
            if (g_list_find_custom(selected,value,((GCompareFunc )strcmp)) != 0) 
              purple_request_field_list_add_selected(field,lbl);
          }
        }
        purple_request_field_group_add_field(group,field);
        while(selected != 0){
          g_free((selected -> data));
          selected = g_list_delete_link(selected,selected);
        }
      }
      else if (!(strcmp(type,"boolean") != 0)) {
        gboolean def = 0;
        if ((valuenode = xmlnode_get_child(fn,"value")) != 0) 
          value = xmlnode_get_data(valuenode);
        if ((value != 0) && ((!(g_ascii_strcasecmp(value,"yes") != 0) || !(g_ascii_strcasecmp(value,"true") != 0)) || !(g_ascii_strcasecmp(value,"1") != 0))) 
          def = (!0);
        field = purple_request_field_bool_new(var,label,def);
        purple_request_field_group_add_field(group,field);
        g_hash_table_replace((data -> fields),(g_strdup(var)),((gpointer )((gpointer )((glong )JABBER_X_DATA_BOOLEAN))));
        g_free(value);
      }
      else if (!(strcmp(type,"fixed") != 0)) {
        if ((valuenode = xmlnode_get_child(fn,"value")) != 0) 
          value = xmlnode_get_data(valuenode);
        if (value != ((char *)((void *)0))) {
          field = purple_request_field_label_new("",value);
          purple_request_field_group_add_field(group,field);
          g_free(value);
        }
      }
      else if (!(strcmp(type,"hidden") != 0)) {
        if ((valuenode = xmlnode_get_child(fn,"value")) != 0) 
          value = xmlnode_get_data(valuenode);
        field = purple_request_field_string_new(var,"",(((value != 0)?value : "")),0);
        purple_request_field_set_visible(field,0);
        purple_request_field_group_add_field(group,field);
        g_hash_table_replace((data -> fields),(g_strdup(var)),((gpointer )((gpointer )((glong )JABBER_X_DATA_TEXT_SINGLE))));
        g_free(value);
/* text-single, jid-single, and the default */
      }
      else {
        if ((valuenode = xmlnode_get_child(fn,"value")) != 0) 
          value = xmlnode_get_data(valuenode);
        field = purple_request_field_string_new(var,label,(((value != 0)?value : "")),0);
        purple_request_field_group_add_field(group,field);
        if (!(strcmp(type,"jid-single") != 0)) {
          purple_request_field_set_type_hint(field,"screenname");
          g_hash_table_replace((data -> fields),(g_strdup(var)),((gpointer )((gpointer )((glong )JABBER_X_DATA_JID_SINGLE))));
        }
        else {
          g_hash_table_replace((data -> fields),(g_strdup(var)),((gpointer )((gpointer )((glong )JABBER_X_DATA_TEXT_SINGLE))));
        }
        g_free(value);
      }
      if ((field != 0) && (xmlnode_get_child(fn,"required") != 0)) 
        purple_request_field_set_required(field,(!0));
    }
  }
  if (actions != ((GList *)((void *)0))) {
    PurpleRequestField *actionfield;
    GList *action;
    data -> actiongroup = (group = purple_request_field_group_new(((const char *)(dgettext("pidgin","Actions")))));
    purple_request_fields_add_group(fields,group);
    actionfield = purple_request_field_choice_new("libpurple:jabber:xdata:actions",((const char *)(dgettext("pidgin","Select an action"))),defaultaction);
    for (action = actions; action != 0; action = ((action != 0)?( *((GList *)action)).next : ((struct _GList *)((void *)0)))) {
      JabberXDataAction *a = (action -> data);
      purple_request_field_choice_add(actionfield,(a -> name));
      data -> actions = g_list_append((data -> actions),(g_strdup((a -> handle))));
    }
    purple_request_field_set_required(actionfield,(!0));
    purple_request_field_group_add_field(group,actionfield);
  }
  if ((x = xmlnode_get_child(packet,"title")) != 0) 
    title = xmlnode_get_data(x);
  if ((x = xmlnode_get_child(packet,"instructions")) != 0) 
    instructions = xmlnode_get_data(x);
  handle = purple_request_fields((js -> gc),title,title,instructions,fields,((const char *)(dgettext("pidgin","OK"))),((GCallback )jabber_x_data_ok_cb),((const char *)(dgettext("pidgin","Cancel"))),((GCallback )jabber_x_data_cancel_cb),purple_connection_get_account((js -> gc)),0,0,data);
/* XXX Do we have a who here? */
  g_free(title);
  g_free(instructions);
  return handle;
}

gchar *jabber_x_data_get_formtype(const xmlnode *form)
{
  xmlnode *field;
  do {
    if (form != ((const xmlnode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"form != NULL");
      return 0;
    };
  }while (0);
  for (field = xmlnode_get_child(((xmlnode *)form),"field"); field != 0; field = xmlnode_get_next_twin(field)) {
    const char *var = xmlnode_get_attrib(field,"var");
    if (purple_strequal(var,"FORM_TYPE") != 0) {
      xmlnode *value = xmlnode_get_child(field,"value");
      if (value != 0) 
        return xmlnode_get_data(value);
      else 
/* An interesting corner case... Looking for a second
				 * FORM_TYPE would be more considerate, but I'm in favor
				 * of not helping broken clients.
				 */
        return 0;
    }
  }
/* Erm, none found :( */
  return 0;
}
