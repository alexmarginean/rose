/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
/* libxmpp is the XMPP protocol plugin. It is linked against libjabbercommon,
 * which may be used to support other protocols (Bonjour) which may need to
 * share code.
 */
#include "internal.h"
#include "accountopt.h"
#include "core.h"
#include "debug.h"
#include "version.h"
#include "iq.h"
#include "jabber.h"
#include "chat.h"
#include "disco.h"
#include "message.h"
#include "roster.h"
#include "si.h"
#include "message.h"
#include "presence.h"
#include "google/google.h"
#include "pep.h"
#include "usermood.h"
#include "usertune.h"
#include "caps.h"
#include "data.h"
#include "ibb.h"
static PurplePlugin *my_protocol = (PurplePlugin *)((void *)0);
static PurplePluginProtocolInfo prpl_info = {((OPT_PROTO_CHAT_TOPIC | OPT_PROTO_UNIQUE_CHATNAME | OPT_PROTO_MAIL_CHECK | OPT_PROTO_SLASH_COMMANDS_NATIVE)), ((GList *)((void *)0)), ((GList *)((void *)0)), 
#ifdef HAVE_CYRUS_SASL
#endif
/* user_splits */
/* protocol_options */
/* icon_spec */
{("png"), (32), (32), (96), (96), (0), ((PURPLE_ICON_SCALE_SEND | PURPLE_ICON_SCALE_DISPLAY))}, (jabber_list_icon), (jabber_list_emblem), (jabber_status_text), (jabber_tooltip_text), (jabber_status_types), (jabber_blist_node_menu), (jabber_chat_info), (jabber_chat_info_defaults), (jabber_login), (jabber_close), (jabber_message_send_im), (jabber_set_info), (jabber_send_typing), (jabber_buddy_get_info), (jabber_set_status), (jabber_idle_set), ((void (*)(PurpleConnection *, const char *, const char *))((void *)0)), (jabber_roster_add_buddy), ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)), (jabber_roster_remove_buddy), ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), (jabber_add_deny), ((void (*)(PurpleConnection *, const char *))((void *)0)), (jabber_rem_deny), ((void (*)(PurpleConnection *))((void *)0)), (jabber_chat_join), ((void (*)(PurpleConnection *, GHashTable *))((void *)0)), (jabber_get_chat_name), (jabber_chat_invite), (jabber_chat_leave), ((void (*)(PurpleConnection *, int , const char *, const char *))((void *)0)), (jabber_message_send_chat), (jabber_keepalive), (jabber_register_account), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), (jabber_roster_alias_change), (jabber_roster_group_change), (jabber_roster_group_rename), ((void (*)(PurpleBuddy *))((void *)0)), (jabber_convo_closed), (jabber_normalize), (jabber_set_buddy_icon), ((void (*)(PurpleConnection *, PurpleGroup *))((void *)0)), (jabber_chat_buddy_real_name), (jabber_chat_set_topic), (jabber_find_blist_chat), (jabber_roomlist_get_list), (jabber_roomlist_cancel), ((void (*)(PurpleRoomlist *, PurpleRoomlistRoom *))((void *)0)), (jabber_can_receive_file), (jabber_si_xfer_send), (jabber_si_new_xfer), (jabber_offline_message), ((PurpleWhiteboardPrplOps *)((void *)0)), (jabber_prpl_send_raw), (jabber_roomlist_room_serialize), (jabber_unregister_account), (jabber_send_attention), (jabber_attention_types), ((sizeof(PurplePluginProtocolInfo ))), ((GHashTable *(*)(PurpleAccount *))((void *)0)), (jabber_initiate_media), (jabber_get_media_caps), (jabber_get_moods), ((void (*)(PurpleConnection *, const char *, PurpleSetPublicAliasSuccessCallback , PurpleSetPublicAliasFailureCallback ))((void *)0)), ((void (*)(PurpleConnection *, PurpleGetPublicAliasSuccessCallback , PurpleGetPublicAliasFailureCallback ))((void *)0)), ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *, const char *))((void *)0)), ((void (*)(PurpleConnection *, GList *, GList *, const char *))((void *)0))
/* list_icon */
/* list_emblems */
/* status_text */
/* tooltip_text */
/* status_types */
/* blist_node_menu */
/* chat_info */
/* chat_info_defaults */
/* login */
/* close */
/* send_im */
/* set_info */
/* send_typing */
/* get_info */
/* set_status */
/* set_idle */
/* change_passwd */
/* add_buddy */
/* add_buddies */
/* remove_buddy */
/* remove_buddies */
/* add_permit */
/* add_deny */
/* rem_permit */
/* rem_deny */
/* set_permit_deny */
/* join_chat */
/* reject_chat */
/* get_chat_name */
/* chat_invite */
/* chat_leave */
/* chat_whisper */
/* chat_send */
/* keepalive */
/* register_user */
/* get_cb_info */
/* get_cb_away */
/* alias_buddy */
/* group_buddy */
/* rename_group */
/* buddy_free */
/* convo_closed */
/* normalize */
/* set_buddy_icon */
/* remove_group */
/* get_cb_real_name */
/* set_chat_topic */
/* find_blist_chat */
/* roomlist_get_list */
/* roomlist_cancel */
/* roomlist_expand_category */
/* can_receive_file */
/* send_file */
/* new_xfer */
/* offline_message */
/* whiteboard_prpl_ops */
/* send_raw */
/* roomlist_room_serialize */
/* unregister_user */
/* send_attention */
/* attention_types */
/* struct_size */
/* get_account_text_table */
/* initiate_media */
/* get_media_caps */
/* get_moods */
/* set_public_alias */
/* get_public_alias */
/* add_buddy_with_invite */
/* add_buddies_with_invite */
};

static gboolean load_plugin(PurplePlugin *plugin)
{
  jabber_plugin_init(plugin);
  return (!0);
}

static gboolean unload_plugin(PurplePlugin *plugin)
{
  jabber_plugin_uninit(plugin);
  return (!0);
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_PROTOCOL), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("prpl-jabber"), ("XMPP"), ("2.10.9"), ("XMPP Protocol Plugin"), ("XMPP Protocol Plugin"), ((char *)((void *)0)), ("http://pidgin.im/"), (load_plugin), (unload_plugin), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((&prpl_info)), ((PurplePluginUiInfo *)((void *)0)), (jabber_actions), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/**< prefs_info     */
/* padding */
};

static PurpleAccount *find_acct(const char *prpl,const char *acct_id)
{
  PurpleAccount *acct = (PurpleAccount *)((void *)0);
/* If we have a specific acct, use it */
  if (acct_id != 0) {
    acct = purple_accounts_find(acct_id,prpl);
    if ((acct != 0) && !(purple_account_is_connected(acct) != 0)) 
      acct = ((PurpleAccount *)((void *)0));
/* Otherwise find an active account for the protocol */
  }
  else {
    GList *l = purple_accounts_get_all();
{
      while(l != 0){
        if (!(strcmp(prpl,purple_account_get_protocol_id((l -> data))) != 0) && (purple_account_is_connected((l -> data)) != 0)) {
          acct = (l -> data);
          break; 
        }
        l = (l -> next);
      }
    }
  }
  return acct;
}

static gboolean xmpp_uri_handler(const char *proto,const char *user,GHashTable *params)
{
  char *acct_id = ((params != 0)?g_hash_table_lookup(params,"account") : ((void *)((void *)0)));
  PurpleAccount *acct;
  if (g_ascii_strcasecmp(proto,"xmpp") != 0) 
    return 0;
  acct = find_acct(purple_plugin_get_id(my_protocol),acct_id);
  if (!(acct != 0)) 
    return 0;
/* xmpp:romeo@montague.net?message;subject=Test%20Message;body=Here%27s%20a%20test%20message */
/* params is NULL if the URI has no '?' (or anything after it) */
  if (!(params != 0) || (g_hash_table_lookup_extended(params,"message",0,0) != 0)) {
    char *body = (g_hash_table_lookup(params,"body"));
    if ((user != 0) && (( *user) != 0)) {
      PurpleConversation *conv = purple_conversation_new(PURPLE_CONV_TYPE_IM,acct,user);
      purple_conversation_present(conv);
      if ((body != 0) && (( *body) != 0)) 
        purple_conv_send_confirm(conv,body);
    }
  }
  else if (g_hash_table_lookup_extended(params,"roster",0,0) != 0) {
    char *name = (g_hash_table_lookup(params,"name"));
    if ((user != 0) && (( *user) != 0)) 
      purple_blist_request_add_buddy(acct,user,0,name);
  }
  else if (g_hash_table_lookup_extended(params,"join",0,0) != 0) {
    PurpleConnection *gc = purple_account_get_connection(acct);
    if ((user != 0) && (( *user) != 0)) {
      GHashTable *params = jabber_chat_info_defaults(gc,user);
      jabber_chat_join(gc,params);
    }
    return (!0);
  }
  return 0;
}

static void init_plugin(PurplePlugin *plugin)
{
  PurpleAccountUserSplit *split;
  PurpleAccountOption *option;
  GList *encryption_values = (GList *)((void *)0);
/* Translators: 'domain' is used here in the context of Internet domains, e.g. pidgin.im */
  split = purple_account_user_split_new(((const char *)(dgettext("pidgin","Domain"))),0,64);
  purple_account_user_split_set_reverse(split,0);
  prpl_info.user_splits = g_list_append(prpl_info.user_splits,split);
  split = purple_account_user_split_new(((const char *)(dgettext("pidgin","Resource"))),"",'/');
  purple_account_user_split_set_reverse(split,0);
  prpl_info.user_splits = g_list_append(prpl_info.user_splits,split);
#define ADD_VALUE(list, desc, v) { \
	PurpleKeyValuePair *kvp = g_new0(PurpleKeyValuePair, 1); \
	kvp->key = g_strdup((desc)); \
	kvp->value = g_strdup((v)); \
	list = g_list_prepend(list, kvp); \
}
{
    PurpleKeyValuePair *kvp = (PurpleKeyValuePair *)(g_malloc0_n(1,(sizeof(PurpleKeyValuePair ))));
    kvp -> key = g_strdup(((const char *)(dgettext("pidgin","Require encryption"))));
    kvp -> value = (g_strdup("require_tls"));
    encryption_values = g_list_prepend(encryption_values,kvp);
  };
{
    PurpleKeyValuePair *kvp = (PurpleKeyValuePair *)(g_malloc0_n(1,(sizeof(PurpleKeyValuePair ))));
    kvp -> key = g_strdup(((const char *)(dgettext("pidgin","Use encryption if available"))));
    kvp -> value = (g_strdup("opportunistic_tls"));
    encryption_values = g_list_prepend(encryption_values,kvp);
  };
{
    PurpleKeyValuePair *kvp = (PurpleKeyValuePair *)(g_malloc0_n(1,(sizeof(PurpleKeyValuePair ))));
    kvp -> key = g_strdup(((const char *)(dgettext("pidgin","Use old-style SSL"))));
    kvp -> value = (g_strdup("old_ssl"));
    encryption_values = g_list_prepend(encryption_values,kvp);
  };
#if 0
#endif
  encryption_values = g_list_reverse(encryption_values);
#undef ADD_VALUE
  option = purple_account_option_list_new(((const char *)(dgettext("pidgin","Connection security"))),"connection_security",encryption_values);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_bool_new(((const char *)(dgettext("pidgin","Allow plaintext auth over unencrypted streams"))),"auth_plain_in_clear",0);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_int_new(((const char *)(dgettext("pidgin","Connect port"))),"port",5222);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","Connect server"))),"connect_server",0);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","File transfer proxies"))),"ft_proxies","proxy.eu.jabber.org");
/* TODO: Is this an acceptable default?
						 * Also, keep this in sync as they add more servers */
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","BOSH URL"))),"bosh_url",0);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
/* this should probably be part of global smiley theme settings later on,
	  shared with MSN */
  option = purple_account_option_bool_new(((const char *)(dgettext("pidgin","Show Custom Smileys"))),"custom_smileys",(!0));
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  my_protocol = plugin;
  purple_prefs_remove("/plugins/prpl/jabber");
  purple_signal_connect((purple_get_core()),"uri-handler",plugin,((PurpleCallback )xmpp_uri_handler),0);
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
