/**
 * @file transport.c
 *
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "transport.h"
#include "jingle.h"
#include "debug.h"
#include <string.h>

struct _JingleTransportPrivate 
{
  void *dummy;
}
;
#define JINGLE_TRANSPORT_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE((obj), JINGLE_TYPE_TRANSPORT, JingleTransportPrivate))
static void jingle_transport_class_init(JingleTransportClass *klass);
static void jingle_transport_init(JingleTransport *transport);
static void jingle_transport_finalize(GObject *object);
static void jingle_transport_get_property(GObject *object,guint prop_id,GValue *value,GParamSpec *pspec);
static void jingle_transport_set_property(GObject *object,guint prop_id,const GValue *value,GParamSpec *pspec);
JingleTransport *jingle_transport_parse_internal(xmlnode *transport);
xmlnode *jingle_transport_to_xml_internal(JingleTransport *transport,xmlnode *content,JingleActionType action);
static GObjectClass *parent_class = (GObjectClass *)((void *)0);
enum __unnamed_enum___F0_L50_C1_PROP_0 {PROP_0};

GType jingle_transport_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(JingleTransportClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )jingle_transport_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(JingleTransport ))), (0), ((GInstanceInitFunc )jingle_transport_init), ((const GTypeValueTable *)((void *)0))};
    type = g_type_register_static(((GType )(20 << 2)),"JingleTransport",&info,0);
  }
  return type;
}

static void jingle_transport_class_init(JingleTransportClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *)klass;
  parent_class = (g_type_class_peek_parent(klass));
  gobject_class -> finalize = jingle_transport_finalize;
  gobject_class -> set_property = jingle_transport_set_property;
  gobject_class -> get_property = jingle_transport_get_property;
  klass -> to_xml = jingle_transport_to_xml_internal;
  klass -> parse = jingle_transport_parse_internal;
  g_type_class_add_private(klass,(sizeof(JingleTransportPrivate )));
}

static void jingle_transport_init(JingleTransport *transport)
{
  transport -> priv = ((JingleTransportPrivate *)(g_type_instance_get_private(((GTypeInstance *)transport),jingle_transport_get_type())));
  ( *(transport -> priv)).dummy = ((void *)((void *)0));
}

static void jingle_transport_finalize(GObject *transport)
{
/* JingleTransportPrivate *priv = JINGLE_TRANSPORT_GET_PRIVATE(transport); */
  purple_debug_info("jingle","jingle_transport_finalize\n");
  ( *(parent_class -> finalize))(transport);
}

static void jingle_transport_set_property(GObject *object,guint prop_id,const GValue *value,GParamSpec *pspec)
{
  do {
    if (object != ((GObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"object != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = jingle_transport_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"JINGLE_IS_TRANSPORT(object)");
      return ;
    };
  }while (0);
  switch(prop_id){
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = prop_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","transport.c:116","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void jingle_transport_get_property(GObject *object,guint prop_id,GValue *value,GParamSpec *pspec)
{
  do {
    if (object != ((GObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"object != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = jingle_transport_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"JINGLE_IS_TRANSPORT(object)");
      return ;
    };
  }while (0);
  switch(prop_id){
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = prop_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","transport.c:129","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

JingleTransport *jingle_transport_create(const gchar *type)
{
  return (g_object_new(jingle_get_type(type),0));
}

const gchar *jingle_transport_get_transport_type(JingleTransport *transport)
{
  return ( *((JingleTransportClass *)( *((GTypeInstance *)transport)).g_class)).transport_type;
}

JingleTransport *jingle_transport_parse_internal(xmlnode *transport)
{
  const gchar *type = xmlnode_get_namespace(transport);
  return jingle_transport_create(type);
}

xmlnode *jingle_transport_to_xml_internal(JingleTransport *transport,xmlnode *content,JingleActionType action)
{
  xmlnode *node = xmlnode_new_child(content,"transport");
  xmlnode_set_namespace(node,jingle_transport_get_transport_type(transport));
  return node;
}

JingleTransport *jingle_transport_parse(xmlnode *transport)
{
  const gchar *type_name = xmlnode_get_namespace(transport);
  GType type = jingle_get_type(type_name);
  if (type == ((GType )(1 << 2))) 
    return 0;
  return ( *( *((JingleTransportClass *)(g_type_check_class_cast(((GTypeClass *)(g_type_class_ref(type))),jingle_transport_get_type())))).parse)(transport);
}

xmlnode *jingle_transport_to_xml(JingleTransport *transport,xmlnode *content,JingleActionType action)
{
  do {
    if (transport != ((JingleTransport *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"transport != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)transport;
      GType __t = jingle_transport_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"JINGLE_IS_TRANSPORT(transport)");
      return 0;
    };
  }while (0);
  return ( *( *((JingleTransportClass *)( *((GTypeInstance *)transport)).g_class)).to_xml)(transport,content,action);
}
