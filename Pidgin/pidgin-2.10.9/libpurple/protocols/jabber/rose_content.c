/**
 * @file content.c
 *
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "debug.h"
#include "content.h"
#include "jingle.h"
#include <string.h>

struct _JingleContentPrivate 
{
  JingleSession *session;
  gchar *description_type;
  gchar *creator;
  gchar *disposition;
  gchar *name;
  gchar *senders;
  JingleTransport *transport;
  JingleTransport *pending_transport;
}
;
#define JINGLE_CONTENT_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE((obj), JINGLE_TYPE_CONTENT, JingleContentPrivate))
static void jingle_content_class_init(JingleContentClass *klass);
static void jingle_content_init(JingleContent *content);
static void jingle_content_finalize(GObject *object);
static void jingle_content_get_property(GObject *object,guint prop_id,GValue *value,GParamSpec *pspec);
static void jingle_content_set_property(GObject *object,guint prop_id,const GValue *value,GParamSpec *pspec);
static xmlnode *jingle_content_to_xml_internal(JingleContent *content,xmlnode *jingle,JingleActionType action);
static JingleContent *jingle_content_parse_internal(xmlnode *content);
static GObjectClass *parent_class = (GObjectClass *)((void *)0);
enum __unnamed_enum___F0_L57_C1_PROP_0__COMMA__PROP_SESSION__COMMA__PROP_CREATOR__COMMA__PROP_DISPOSITION__COMMA__PROP_NAME__COMMA__PROP_SENDERS__COMMA__PROP_TRANSPORT__COMMA__PROP_PENDING_TRANSPORT {PROP_0,PROP_SESSION,PROP_CREATOR,PROP_DISPOSITION,PROP_NAME,PROP_SENDERS,PROP_TRANSPORT,PROP_PENDING_TRANSPORT};

GType jingle_content_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(JingleContentClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )jingle_content_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(JingleContent ))), (0), ((GInstanceInitFunc )jingle_content_init), ((const GTypeValueTable *)((void *)0))};
    type = g_type_register_static(((GType )(20 << 2)),"JingleContent",&info,0);
  }
  return type;
}

static void jingle_content_class_init(JingleContentClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *)klass;
  parent_class = (g_type_class_peek_parent(klass));
  gobject_class -> finalize = jingle_content_finalize;
  gobject_class -> set_property = jingle_content_set_property;
  gobject_class -> get_property = jingle_content_get_property;
  klass -> to_xml = jingle_content_to_xml_internal;
  klass -> parse = jingle_content_parse_internal;
  g_object_class_install_property(gobject_class,PROP_SESSION,g_param_spec_object("session","Jingle Session","The jingle session parent of this content.",jingle_session_get_type(),(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(gobject_class,PROP_CREATOR,g_param_spec_string("creator","Creator","The participant that created this content.",0,(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_class_install_property(gobject_class,PROP_DISPOSITION,g_param_spec_string("disposition","Disposition","The disposition of the content.",0,(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_class_install_property(gobject_class,PROP_NAME,g_param_spec_string("name","Name","The name of this content.",0,(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_class_install_property(gobject_class,PROP_SENDERS,g_param_spec_string("senders","Senders","The sender of this content.",0,(G_PARAM_CONSTRUCT | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_class_install_property(gobject_class,PROP_TRANSPORT,g_param_spec_object("transport","transport","The transport of this content.",jingle_transport_get_type(),(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_class_install_property(gobject_class,PROP_PENDING_TRANSPORT,g_param_spec_object("pending-transport","Pending transport","The pending transport contained within this content",jingle_transport_get_type(),(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_type_class_add_private(klass,(sizeof(JingleContentPrivate )));
}

static void jingle_content_init(JingleContent *content)
{
  content -> priv = ((JingleContentPrivate *)(g_type_instance_get_private(((GTypeInstance *)content),jingle_content_get_type())));
  memset((content -> priv),0,(sizeof(( *(content -> priv)))));
}

static void jingle_content_finalize(GObject *content)
{
  JingleContentPrivate *priv = (JingleContentPrivate *)(g_type_instance_get_private(((GTypeInstance *)content),jingle_content_get_type()));
  purple_debug_info("jingle","jingle_content_finalize\n");
  g_free((priv -> description_type));
  g_free((priv -> creator));
  g_free((priv -> disposition));
  g_free((priv -> name));
  g_free((priv -> senders));
  g_object_unref((priv -> transport));
  if ((priv -> pending_transport) != 0) 
    g_object_unref((priv -> pending_transport));
  ( *(parent_class -> finalize))(content);
}

static void jingle_content_set_property(GObject *object,guint prop_id,const GValue *value,GParamSpec *pspec)
{
  JingleContent *content;
  do {
    if (object != ((GObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"object != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = jingle_content_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"JINGLE_IS_CONTENT(object)");
      return ;
    };
  }while (0);
  content = ((JingleContent *)(g_type_check_instance_cast(((GTypeInstance *)object),jingle_content_get_type())));
  switch(prop_id){
    case PROP_SESSION:
{
      ( *(content -> priv)).session = (g_value_get_object(value));
      break; 
    }
    case PROP_CREATOR:
{
      g_free(( *(content -> priv)).creator);
      ( *(content -> priv)).creator = g_value_dup_string(value);
      break; 
    }
    case PROP_DISPOSITION:
{
      g_free(( *(content -> priv)).disposition);
      ( *(content -> priv)).disposition = g_value_dup_string(value);
      break; 
    }
    case PROP_NAME:
{
      g_free(( *(content -> priv)).name);
      ( *(content -> priv)).name = g_value_dup_string(value);
      break; 
    }
    case PROP_SENDERS:
{
      g_free(( *(content -> priv)).senders);
      ( *(content -> priv)).senders = g_value_dup_string(value);
      break; 
    }
    case PROP_TRANSPORT:
{
      if (( *(content -> priv)).transport != 0) 
        g_object_unref(( *(content -> priv)).transport);
      ( *(content -> priv)).transport = (g_value_get_object(value));
      break; 
    }
    case PROP_PENDING_TRANSPORT:
{
      if (( *(content -> priv)).pending_transport != 0) 
        g_object_unref(( *(content -> priv)).pending_transport);
      ( *(content -> priv)).pending_transport = (g_value_get_object(value));
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = prop_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","content.c:221","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void jingle_content_get_property(GObject *object,guint prop_id,GValue *value,GParamSpec *pspec)
{
  JingleContent *content;
  do {
    if (object != ((GObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"object != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = jingle_content_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"JINGLE_IS_CONTENT(object)");
      return ;
    };
  }while (0);
  content = ((JingleContent *)(g_type_check_instance_cast(((GTypeInstance *)object),jingle_content_get_type())));
  switch(prop_id){
    case PROP_SESSION:
{
      g_value_set_object(value,( *(content -> priv)).session);
      break; 
    }
    case PROP_CREATOR:
{
      g_value_set_string(value,( *(content -> priv)).creator);
      break; 
    }
    case PROP_DISPOSITION:
{
      g_value_set_string(value,( *(content -> priv)).disposition);
      break; 
    }
    case PROP_NAME:
{
      g_value_set_string(value,( *(content -> priv)).name);
      break; 
    }
    case PROP_SENDERS:
{
      g_value_set_string(value,( *(content -> priv)).senders);
      break; 
    }
    case PROP_TRANSPORT:
{
      g_value_set_object(value,( *(content -> priv)).transport);
      break; 
    }
    case PROP_PENDING_TRANSPORT:
{
      g_value_set_object(value,( *(content -> priv)).pending_transport);
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = prop_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","content.c:259","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

JingleContent *jingle_content_create(const gchar *type,const gchar *creator,const gchar *disposition,const gchar *name,const gchar *senders,JingleTransport *transport)
{
  JingleContent *content = (g_object_new(jingle_get_type(type),"creator",creator,"disposition",((disposition != ((const gchar *)((void *)0)))?disposition : "session"),"name",name,"senders",((senders != ((const gchar *)((void *)0)))?senders : "both"),"transport",transport,((void *)((void *)0))));
  return content;
}

JingleSession *jingle_content_get_session(JingleContent *content)
{
  JingleSession *session;
  g_object_get(content,"session",&session,((void *)((void *)0)));
  return session;
}

const gchar *jingle_content_get_description_type(JingleContent *content)
{
  return ( *((JingleContentClass *)( *((GTypeInstance *)content)).g_class)).description_type;
}

gchar *jingle_content_get_creator(JingleContent *content)
{
  gchar *creator;
  g_object_get(content,"creator",&creator,((void *)((void *)0)));
  return creator;
}

gchar *jingle_content_get_disposition(JingleContent *content)
{
  gchar *disposition;
  g_object_get(content,"disposition",&disposition,((void *)((void *)0)));
  return disposition;
}

gchar *jingle_content_get_name(JingleContent *content)
{
  gchar *name;
  g_object_get(content,"name",&name,((void *)((void *)0)));
  return name;
}

gchar *jingle_content_get_senders(JingleContent *content)
{
  gchar *senders;
  g_object_get(content,"senders",&senders,((void *)((void *)0)));
  return senders;
}

JingleTransport *jingle_content_get_transport(JingleContent *content)
{
  JingleTransport *transport;
  g_object_get(content,"transport",&transport,((void *)((void *)0)));
  return transport;
}

void jingle_content_set_session(JingleContent *content,JingleSession *session)
{
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)content;
      GType __t = jingle_content_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"JINGLE_IS_CONTENT(content)");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)session;
      GType __t = jingle_session_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"JINGLE_IS_SESSION(session)");
      return ;
    };
  }while (0);
  g_object_set(content,"session",session,((void *)((void *)0)));
}

JingleTransport *jingle_content_get_pending_transport(JingleContent *content)
{
  JingleTransport *pending_transport;
  g_object_get(content,"pending_transport",&pending_transport,((void *)((void *)0)));
  return pending_transport;
}

void jingle_content_set_pending_transport(JingleContent *content,JingleTransport *transport)
{
  g_object_set(content,"pending-transport",transport,((void *)((void *)0)));
}

void jingle_content_accept_transport(JingleContent *content)
{
  if (( *(content -> priv)).transport != 0) 
    g_object_unref(( *(content -> priv)).transport);
  ( *(content -> priv)).transport = ( *(content -> priv)).pending_transport;
  ( *(content -> priv)).pending_transport = ((JingleTransport *)((void *)0));
}

void jingle_content_remove_pending_transport(JingleContent *content)
{
  if (( *(content -> priv)).pending_transport != 0) {
    g_object_unref(( *(content -> priv)).pending_transport);
    ( *(content -> priv)).pending_transport = ((JingleTransport *)((void *)0));
  }
}

void jingle_content_modify(JingleContent *content,const gchar *senders)
{
  g_object_set(content,"senders",senders,((void *)((void *)0)));
}

static JingleContent *jingle_content_parse_internal(xmlnode *content)
{
  xmlnode *description = xmlnode_get_child(content,"description");
  const gchar *type = xmlnode_get_namespace(description);
  const gchar *creator = xmlnode_get_attrib(content,"creator");
  const gchar *disposition = xmlnode_get_attrib(content,"disposition");
  const gchar *senders = xmlnode_get_attrib(content,"senders");
  const gchar *name = xmlnode_get_attrib(content,"name");
  JingleTransport *transport = jingle_transport_parse(xmlnode_get_child(content,"transport"));
  if (transport == ((JingleTransport *)((void *)0))) 
    return 0;
  if (senders == ((const gchar *)((void *)0))) 
    senders = "both";
  return jingle_content_create(type,creator,disposition,name,senders,transport);
}

JingleContent *jingle_content_parse(xmlnode *content)
{
  const gchar *type = xmlnode_get_namespace(xmlnode_get_child(content,"description"));
  GType jingle_type = jingle_get_type(type);
  if (jingle_type != ((GType )(1 << 2))) {
    return ( *( *((JingleContentClass *)(g_type_check_class_cast(((GTypeClass *)(g_type_class_ref(jingle_type))),jingle_content_get_type())))).parse)(content);
  }
  else {
    return 0;
  }
}

static xmlnode *jingle_content_to_xml_internal(JingleContent *content,xmlnode *jingle,JingleActionType action)
{
  xmlnode *node = xmlnode_new_child(jingle,"content");
  gchar *creator = jingle_content_get_creator(content);
  gchar *name = jingle_content_get_name(content);
  gchar *senders = jingle_content_get_senders(content);
  gchar *disposition = jingle_content_get_disposition(content);
  xmlnode_set_attrib(node,"creator",creator);
  xmlnode_set_attrib(node,"name",name);
  xmlnode_set_attrib(node,"senders",senders);
  if (strcmp("session",disposition) != 0) 
    xmlnode_set_attrib(node,"disposition",disposition);
  g_free(disposition);
  g_free(senders);
  g_free(name);
  g_free(creator);
  if (action != JINGLE_CONTENT_REMOVE) {
    JingleTransport *transport;
    if ((((action != JINGLE_TRANSPORT_ACCEPT) && (action != JINGLE_TRANSPORT_INFO)) && (action != JINGLE_TRANSPORT_REJECT)) && (action != JINGLE_TRANSPORT_REPLACE)) {
      xmlnode *description = xmlnode_new_child(node,"description");
      xmlnode_set_namespace(description,jingle_content_get_description_type(content));
    }
    if ((action != JINGLE_TRANSPORT_REJECT) && (action == JINGLE_TRANSPORT_REPLACE)) 
      transport = jingle_content_get_pending_transport(content);
    else 
      transport = jingle_content_get_transport(content);
    jingle_transport_to_xml(transport,node,action);
    g_object_unref(transport);
  }
  return node;
}

xmlnode *jingle_content_to_xml(JingleContent *content,xmlnode *jingle,JingleActionType action)
{
  do {
    if (content != ((JingleContent *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"content != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)content;
      GType __t = jingle_content_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"JINGLE_IS_CONTENT(content)");
      return 0;
    };
  }while (0);
  return ( *( *((JingleContentClass *)( *((GTypeInstance *)content)).g_class)).to_xml)(content,jingle,action);
}

void jingle_content_handle_action(JingleContent *content,xmlnode *xmlcontent,JingleActionType action)
{
  do {
    if (content != ((JingleContent *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"content != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)content;
      GType __t = jingle_content_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"JINGLE_IS_CONTENT(content)");
      return ;
    };
  }while (0);
  ( *( *((JingleContentClass *)( *((GTypeInstance *)content)).g_class)).handle_action)(content,xmlcontent,action);
}
