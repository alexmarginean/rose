/*
 * purple - Jabber Protocol Plugin
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "debug.h"
#include "server.h"
#include "util.h"
#include "buddy.h"
#include "chat.h"
#include "google/google.h"
#include "google/google_roster.h"
#include "presence.h"
#include "roster.h"
#include "iq.h"
#include <string.h>
/* Take a list of strings and join them with a ", " separator */

static gchar *roster_groups_join(GSList *list)
{
  GString *out = g_string_new(0);
  for (; list != 0; list = (list -> next)) {
    out = g_string_append(out,((const char *)(list -> data)));
    if ((list -> next) != 0) 
      out = g_string_append(out,", ");
  }
  return g_string_free(out,0);
}

static void roster_request_cb(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  xmlnode *query;
  if (type == JABBER_IQ_ERROR) {
/*
		 * This shouldn't happen in any real circumstances and
		 * likely constitutes a server-side misconfiguration (i.e.
		 * explicitly not loading mod_roster...)
		 */
    purple_debug_error("jabber","Error retrieving roster!\?\n");
    jabber_stream_set_state(js,JABBER_STREAM_CONNECTED);
    return ;
  }
  query = xmlnode_get_child(packet,"query");
  if (query == ((xmlnode *)((void *)0))) {
    jabber_stream_set_state(js,JABBER_STREAM_CONNECTED);
    return ;
  }
  jabber_roster_parse(js,from,type,id,query);
  jabber_stream_set_state(js,JABBER_STREAM_CONNECTED);
}

void jabber_roster_request(JabberStream *js)
{
  JabberIq *iq;
  xmlnode *query;
  iq = jabber_iq_new_query(js,JABBER_IQ_GET,"jabber:iq:roster");
  query = xmlnode_get_child((iq -> node),"query");
  if (((js -> server_caps) & JABBER_CAP_GOOGLE_ROSTER) != 0) {
    xmlnode_set_attrib(query,"xmlns:gr","google:roster");
    xmlnode_set_attrib(query,"gr:ext","2");
  }
  jabber_iq_set_callback(iq,roster_request_cb,0);
  jabber_iq_send(iq);
}

static void remove_purple_buddies(JabberStream *js,const char *jid)
{
  GSList *buddies;
  GSList *l;
  buddies = purple_find_buddies(( *(js -> gc)).account,jid);
  for (l = buddies; l != 0; l = (l -> next)) 
    purple_blist_remove_buddy((l -> data));
  g_slist_free(buddies);
}

static void add_purple_buddy_to_groups(JabberStream *js,const char *jid,const char *alias,GSList *groups)
{
  GSList *buddies;
  GSList *l;
  PurpleAccount *account = purple_connection_get_account((js -> gc));
  buddies = purple_find_buddies(( *(js -> gc)).account,jid);
  if (!(groups != 0)) {
    if (!(buddies != 0)) 
      groups = g_slist_append(groups,(g_strdup(((const char *)(dgettext("pidgin","Buddies"))))));
    else {
/* TODO: What should we do here? Removing the local buddies
			 * is wrong, but so is letting the group state get out of sync with
			 * the server.
			 */
      g_slist_free(buddies);
      return ;
    }
  }
  while(buddies != 0){
    PurpleBuddy *b = (buddies -> data);
    PurpleGroup *g = purple_buddy_get_group(b);
    buddies = g_slist_delete_link(buddies,buddies);
/* XMPP groups are case-sensitive, but libpurple groups are
		 * case-insensitive. We treat a buddy in both "Friends" and "friends"
		 * as only being in one group, but if we push changes about the buddy
		 * to the server, the buddy will be dropped from one of the groups.
		 * Not optimal, but better than the alternative, I think.
		 */
    if ((l = g_slist_find_custom(groups,(purple_group_get_name(g)),((GCompareFunc )purple_utf8_strcasecmp))) != 0) {
/* The buddy is already on the local list. Update info. */
      const char *servernick;
      const char *balias;
/* Previously stored serverside / buddy-supplied alias */
      if ((servernick = purple_blist_node_get_string(((PurpleBlistNode *)b),"servernick")) != 0) 
        serv_got_alias((js -> gc),jid,servernick);
/* Alias from our roster retrieval */
      balias = purple_buddy_get_local_buddy_alias(b);
      if ((alias != 0) && !(purple_strequal(alias,balias) != 0)) 
        purple_serv_got_private_alias((js -> gc),jid,alias);
      g_free((l -> data));
      groups = g_slist_delete_link(groups,l);
    }
    else {
/* This buddy isn't in the group on the server anymore */
      purple_debug_info("jabber","jabber_roster_parse(): Removing %s from group \'%s\' on the local list\n",purple_buddy_get_name(b),purple_group_get_name(g));
      purple_blist_remove_buddy(b);
    }
  }
  if (groups != 0) {
    char *tmp = roster_groups_join(groups);
    purple_debug_info("jabber","jabber_roster_parse(): Adding %s to groups: %s\n",jid,tmp);
    g_free(tmp);
  }
  while(groups != 0){
    PurpleGroup *g = purple_find_group((groups -> data));
    PurpleBuddy *b = purple_buddy_new(account,jid,alias);
    if (!(g != 0)) {
      g = purple_group_new((groups -> data));
      purple_blist_add_group(g,0);
    }
    purple_blist_add_buddy(b,0,g,0);
    purple_blist_alias_buddy(b,alias);
    g_free((groups -> data));
    groups = g_slist_delete_link(groups,groups);
  }
  g_slist_free(buddies);
}

void jabber_roster_parse(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *query)
{
  xmlnode *item;
  xmlnode *group;
#if 0
#endif
  if (!(jabber_is_own_account(js,from) != 0)) {
    purple_debug_warning("jabber","Received bogon roster push from %s\n",from);
    return ;
  }
  js -> currently_parsing_roster_push = (!0);
  for (item = xmlnode_get_child(query,"item"); item != 0; item = xmlnode_get_next_twin(item)) {{
      const char *jid;
      const char *name;
      const char *subscription;
      const char *ask;
      JabberBuddy *jb;
      subscription = xmlnode_get_attrib(item,"subscription");
      jid = xmlnode_get_attrib(item,"jid");
      name = xmlnode_get_attrib(item,"name");
      ask = xmlnode_get_attrib(item,"ask");
      if (!(jid != 0)) 
        continue; 
      if (!((jb = jabber_buddy_find(js,jid,(!0))) != 0)) 
        continue; 
      if (subscription != 0) {
        if (g_str_equal(subscription,"remove") != 0) 
          jb -> subscription = JABBER_SUB_REMOVE;
        else if (jb == (js -> user_jb)) 
          jb -> subscription = JABBER_SUB_BOTH;
        else if (g_str_equal(subscription,"none") != 0) 
          jb -> subscription = JABBER_SUB_NONE;
        else if (g_str_equal(subscription,"to") != 0) 
          jb -> subscription = JABBER_SUB_TO;
        else if (g_str_equal(subscription,"from") != 0) 
          jb -> subscription = JABBER_SUB_FROM;
        else if (g_str_equal(subscription,"both") != 0) 
          jb -> subscription = JABBER_SUB_BOTH;
      }
      if (purple_strequal(ask,"subscribe") != 0) 
        jb -> subscription |= JABBER_SUB_PENDING;
      else 
        jb -> subscription &= (~JABBER_SUB_PENDING);
      if (((jb -> subscription) & JABBER_SUB_REMOVE) != 0U) {
        remove_purple_buddies(js,jid);
      }
      else {
        GSList *groups = (GSList *)((void *)0);
        if (((js -> server_caps) & JABBER_CAP_GOOGLE_ROSTER) != 0) 
          if (!(jabber_google_roster_incoming(js,item) != 0)) 
            continue; 
        for (group = xmlnode_get_child(item,"group"); group != 0; group = xmlnode_get_next_twin(group)) {
          char *group_name = xmlnode_get_data(group);
          if ((group_name == ((char *)((void *)0))) || (( *group_name) == 0)) 
/* Changing this string?  Look in add_purple_buddy_to_groups */
            group_name = g_strdup(((const char *)(dgettext("pidgin","Buddies"))));
/*
				 * See the note in add_purple_buddy_to_groups; the core handles
				 * names case-insensitively and this is required to not
				 * end up with duplicates if a buddy is in, e.g.,
				 * 'XMPP' and 'xmpp'
				 */
          if (g_slist_find_custom(groups,group_name,((GCompareFunc )purple_utf8_strcasecmp)) != 0) 
            g_free(group_name);
          else 
            groups = g_slist_prepend(groups,group_name);
        }
        add_purple_buddy_to_groups(js,jid,name,groups);
        if (jb == (js -> user_jb)) 
          jabber_presence_fake_to_self(js,0);
      }
    }
  }
#if 0
#endif
  if (type == JABBER_IQ_SET) {
    JabberIq *ack = jabber_iq_new(js,JABBER_IQ_RESULT);
    jabber_iq_set_id(ack,id);
    jabber_iq_send(ack);
  }
  js -> currently_parsing_roster_push = 0;
}
/* jabber_roster_update frees the GSList* passed in */

static void jabber_roster_update(JabberStream *js,const char *name,GSList *groups)
{
  PurpleBuddy *b;
  PurpleGroup *g;
  GSList *l;
  JabberIq *iq;
  xmlnode *query;
  xmlnode *item;
  xmlnode *group;
  const char *balias;
  if ((js -> currently_parsing_roster_push) != 0) 
    return ;
  if (!((b = purple_find_buddy(( *(js -> gc)).account,name)) != 0)) 
    return ;
  if (groups != 0) {
    char *tmp = roster_groups_join(groups);
    purple_debug_info("jabber","jabber_roster_update(%s): [Source: groups]: groups: %s\n",name,tmp);
    g_free(tmp);
  }
  else {
    GSList *buddies = purple_find_buddies(( *(js -> gc)).account,name);
    char *tmp;
    if (!(buddies != 0)) 
      return ;
    while(buddies != 0){
      b = (buddies -> data);
      g = purple_buddy_get_group(b);
      groups = g_slist_append(groups,((char *)(purple_group_get_name(g))));
      buddies = g_slist_remove(buddies,b);
    }
    tmp = roster_groups_join(groups);
    purple_debug_info("jabber","jabber_roster_update(%s): [Source: local blist]: groups: %s\n",name,tmp);
    g_free(tmp);
  }
  iq = jabber_iq_new_query(js,JABBER_IQ_SET,"jabber:iq:roster");
  query = xmlnode_get_child((iq -> node),"query");
  item = xmlnode_new_child(query,"item");
  xmlnode_set_attrib(item,"jid",name);
  balias = purple_buddy_get_local_buddy_alias(b);
  xmlnode_set_attrib(item,"name",((balias != 0)?balias : ""));
  for (l = groups; l != 0; l = (l -> next)) {
    group = xmlnode_new_child(item,"group");
    xmlnode_insert_data(group,(l -> data),(-1));
  }
  g_slist_free(groups);
  if (((js -> server_caps) & JABBER_CAP_GOOGLE_ROSTER) != 0) {
    jabber_google_roster_outgoing(js,query,item);
    xmlnode_set_attrib(query,"xmlns:gr","google:roster");
    xmlnode_set_attrib(query,"gr:ext","2");
  }
  jabber_iq_send(iq);
}

void jabber_roster_add_buddy(PurpleConnection *gc,PurpleBuddy *buddy,PurpleGroup *group)
{
  JabberStream *js = (gc -> proto_data);
  char *who;
  JabberID *jid;
  JabberBuddy *jb;
  JabberBuddyResource *jbr;
  const char *name;
/* If we haven't received the roster yet, ignore any adds */
  if ((js -> state) != JABBER_STREAM_CONNECTED) 
    return ;
  name = purple_buddy_get_name(buddy);
  jid = jabber_id_new(name);
  if (jid == ((JabberID *)((void *)0))) {
/* TODO: Remove the buddy from the list? */
    return ;
  }
/* Adding a chat room or a chat buddy to the roster is *not* supported. */
  if (((jid -> node) != 0) && (jabber_chat_find(js,(jid -> node),(jid -> domain)) != ((JabberChat *)((void *)0)))) {
/*
		 * This is the same thing Bonjour does. If it causes problems, move
		 * it to an idle callback.
		 */
    purple_debug_warning("jabber","Cowardly refusing to add a MUC user to your buddy list and removing the buddy. Buddies can only be added by real (non-MUC) JID\n");
    purple_blist_remove_buddy(buddy);
    jabber_id_free(jid);
    return ;
  }
  who = jabber_id_get_bare_jid(jid);
  if ((jid -> resource) != ((char *)((void *)0))) {
/*
		 * If the buddy name added contains a resource, strip that off and
		 * rename the buddy.
		 */
    purple_blist_rename_buddy(buddy,who);
  }
  jb = jabber_buddy_find(js,who,0);
  purple_debug_info("jabber","jabber_roster_add_buddy(): Adding %s\n",who);
  jabber_roster_update(js,who,0);
  if (jb == (js -> user_jb)) {
    jabber_presence_fake_to_self(js,0);
  }
  else if (!(jb != 0) || !(((jb -> subscription) & JABBER_SUB_TO) != 0U)) {
    jabber_presence_subscription_set(js,who,"subscribe");
  }
  else if ((jbr = jabber_buddy_find_resource(jb,0)) != 0) {
    purple_prpl_got_user_status((gc -> account),who,jabber_buddy_state_get_status_id((jbr -> state)),"priority",(jbr -> priority),(((jbr -> status) != 0)?"message" : ((char *)((void *)0))),(jbr -> status),((void *)((void *)0)));
  }
  g_free(who);
}

void jabber_roster_alias_change(PurpleConnection *gc,const char *name,const char *alias)
{
  PurpleBuddy *b = purple_find_buddy((gc -> account),name);
  if (b != ((PurpleBuddy *)((void *)0))) {
    purple_blist_alias_buddy(b,alias);
    purple_debug_info("jabber","jabber_roster_alias_change(): Aliased %s to %s\n",name,((alias != 0)?alias : "(null)"));
    jabber_roster_update((gc -> proto_data),name,0);
  }
}

void jabber_roster_group_change(PurpleConnection *gc,const char *name,const char *old_group,const char *new_group)
{
  GSList *buddies;
  GSList *groups = (GSList *)((void *)0);
  PurpleBuddy *b;
  PurpleGroup *g;
  const char *gname;
  if ((!(old_group != 0) || !(new_group != 0)) || !(strcmp(old_group,new_group) != 0)) 
    return ;
  buddies = purple_find_buddies((gc -> account),name);
  while(buddies != 0){
    b = (buddies -> data);
    g = purple_buddy_get_group(b);
    gname = purple_group_get_name(g);
    if (!(strcmp(gname,old_group) != 0)) 
/* ick */
      groups = g_slist_append(groups,((char *)new_group));
    else 
      groups = g_slist_append(groups,((char *)gname));
    buddies = g_slist_remove(buddies,b);
  }
  purple_debug_info("jabber","jabber_roster_group_change(): Moving %s from %s to %s\n",name,old_group,new_group);
  jabber_roster_update((gc -> proto_data),name,groups);
}

void jabber_roster_group_rename(PurpleConnection *gc,const char *old_name,PurpleGroup *group,GList *moved_buddies)
{
  GList *l;
  const char *gname = purple_group_get_name(group);
  for (l = moved_buddies; l != 0; l = (l -> next)) {
    PurpleBuddy *buddy = (l -> data);
    jabber_roster_group_change(gc,purple_buddy_get_name(buddy),old_name,gname);
  }
}

void jabber_roster_remove_buddy(PurpleConnection *gc,PurpleBuddy *buddy,PurpleGroup *group)
{
  const char *name = purple_buddy_get_name(buddy);
  GSList *buddies = purple_find_buddies(purple_connection_get_account(gc),name);
  buddies = g_slist_remove(buddies,buddy);
  if (buddies != ((GSList *)((void *)0))) {
    PurpleBuddy *tmpbuddy;
    PurpleGroup *tmpgroup;
    GSList *groups = (GSList *)((void *)0);
    while(buddies != 0){
      tmpbuddy = (buddies -> data);
      tmpgroup = purple_buddy_get_group(tmpbuddy);
      groups = g_slist_append(groups,((char *)(purple_group_get_name(tmpgroup))));
      buddies = g_slist_remove(buddies,tmpbuddy);
    }
    purple_debug_info("jabber","jabber_roster_remove_buddy(): Removing %s from %s\n",purple_buddy_get_name(buddy),purple_group_get_name(group));
    jabber_roster_update((gc -> proto_data),name,groups);
  }
  else {
    JabberIq *iq = jabber_iq_new_query((gc -> proto_data),JABBER_IQ_SET,"jabber:iq:roster");
    xmlnode *query = xmlnode_get_child((iq -> node),"query");
    xmlnode *item = xmlnode_new_child(query,"item");
    xmlnode_set_attrib(item,"jid",name);
    xmlnode_set_attrib(item,"subscription","remove");
    purple_debug_info("jabber","jabber_roster_remove_buddy(): Removing %s\n",purple_buddy_get_name(buddy));
    jabber_iq_send(iq);
  }
}
