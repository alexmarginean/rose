/**
 * @file session.c
 *
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "content.h"
#include "debug.h"
#include "session.h"
#include "jingle.h"
#include <string.h>

struct _JingleSessionPrivate 
{
  gchar *sid;
  JabberStream *js;
  gchar *remote_jid;
  gchar *local_jid;
  gboolean is_initiator;
  gboolean state;
  GList *contents;
  GList *pending_contents;
}
;
#define JINGLE_SESSION_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE((obj), JINGLE_TYPE_SESSION, JingleSessionPrivate))
static void jingle_session_class_init(JingleSessionClass *klass);
static void jingle_session_init(JingleSession *session);
static void jingle_session_finalize(GObject *object);
static void jingle_session_get_property(GObject *object,guint prop_id,GValue *value,GParamSpec *pspec);
static void jingle_session_set_property(GObject *object,guint prop_id,const GValue *value,GParamSpec *pspec);
static GObjectClass *parent_class = (GObjectClass *)((void *)0);
enum __unnamed_enum___F0_L56_C1_PROP_0__COMMA__PROP_SID__COMMA__PROP_JS__COMMA__PROP_REMOTE_JID__COMMA__PROP_LOCAL_JID__COMMA__PROP_IS_INITIATOR__COMMA__PROP_STATE__COMMA__PROP_CONTENTS__COMMA__PROP_PENDING_CONTENTS {PROP_0,PROP_SID,PROP_JS,PROP_REMOTE_JID,PROP_LOCAL_JID,PROP_IS_INITIATOR,PROP_STATE,PROP_CONTENTS,PROP_PENDING_CONTENTS};

GType jingle_session_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(JingleSessionClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )jingle_session_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(JingleSession ))), (0), ((GInstanceInitFunc )jingle_session_init), ((const GTypeValueTable *)((void *)0))};
    type = g_type_register_static(((GType )(20 << 2)),"JingleSession",&info,0);
  }
  return type;
}

static void jingle_session_class_init(JingleSessionClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *)klass;
  parent_class = (g_type_class_peek_parent(klass));
  gobject_class -> finalize = jingle_session_finalize;
  gobject_class -> set_property = jingle_session_set_property;
  gobject_class -> get_property = jingle_session_get_property;
  g_object_class_install_property(gobject_class,PROP_SID,g_param_spec_string("sid","Session ID","The unique session ID of the Jingle Session.",0,(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_class_install_property(gobject_class,PROP_JS,g_param_spec_pointer("js","JabberStream","The Jabber stream associated with this session.",(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_class_install_property(gobject_class,PROP_REMOTE_JID,g_param_spec_string("remote-jid","Remote JID","The JID of the remote participant.",0,(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_class_install_property(gobject_class,PROP_LOCAL_JID,g_param_spec_string("local-jid","Local JID","The JID of the local participant.",0,(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_class_install_property(gobject_class,PROP_IS_INITIATOR,g_param_spec_boolean("is-initiator","Is Initiator","Whether or not the local JID is the initiator of the session.",0,(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_class_install_property(gobject_class,PROP_STATE,g_param_spec_boolean("state","State","The state of the session (PENDING=FALSE, ACTIVE=TRUE).",0,G_PARAM_READABLE));
  g_object_class_install_property(gobject_class,PROP_CONTENTS,g_param_spec_pointer("contents","Contents","The active contents contained within this session",G_PARAM_READABLE));
  g_object_class_install_property(gobject_class,PROP_PENDING_CONTENTS,g_param_spec_pointer("pending-contents","Pending contents","The pending contents contained within this session",G_PARAM_READABLE));
  g_type_class_add_private(klass,(sizeof(JingleSessionPrivate )));
}

static void jingle_session_init(JingleSession *session)
{
  session -> priv = ((JingleSessionPrivate *)(g_type_instance_get_private(((GTypeInstance *)session),jingle_session_get_type())));
  memset((session -> priv),0,(sizeof(( *(session -> priv)))));
}

static void jingle_session_finalize(GObject *session)
{
  JingleSessionPrivate *priv = (JingleSessionPrivate *)(g_type_instance_get_private(((GTypeInstance *)session),jingle_session_get_type()));
  purple_debug_info("jingle","jingle_session_finalize\n");
  g_hash_table_remove(( *(priv -> js)).sessions,(priv -> sid));
  g_free((priv -> sid));
  g_free((priv -> remote_jid));
  g_free((priv -> local_jid));
  for (; (priv -> contents) != 0; priv -> contents = g_list_delete_link((priv -> contents),(priv -> contents))) {
    g_object_unref(( *(priv -> contents)).data);
  }
  for (; (priv -> pending_contents) != 0; priv -> pending_contents = g_list_delete_link((priv -> pending_contents),(priv -> pending_contents))) {
    g_object_unref(( *(priv -> pending_contents)).data);
  }
  ( *(parent_class -> finalize))(session);
}

static void jingle_session_set_property(GObject *object,guint prop_id,const GValue *value,GParamSpec *pspec)
{
  JingleSession *session;
  do {
    if (object != ((GObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"object != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = jingle_session_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"JINGLE_IS_SESSION(object)");
      return ;
    };
  }while (0);
  session = ((JingleSession *)(g_type_check_instance_cast(((GTypeInstance *)object),jingle_session_get_type())));
  switch(prop_id){
    case PROP_SID:
{
      g_free(( *(session -> priv)).sid);
      ( *(session -> priv)).sid = g_value_dup_string(value);
      break; 
    }
    case PROP_JS:
{
      ( *(session -> priv)).js = (g_value_get_pointer(value));
      break; 
    }
    case PROP_REMOTE_JID:
{
      g_free(( *(session -> priv)).remote_jid);
      ( *(session -> priv)).remote_jid = g_value_dup_string(value);
      break; 
    }
    case PROP_LOCAL_JID:
{
      g_free(( *(session -> priv)).local_jid);
      ( *(session -> priv)).local_jid = g_value_dup_string(value);
      break; 
    }
    case PROP_IS_INITIATOR:
{
      ( *(session -> priv)).is_initiator = g_value_get_boolean(value);
      break; 
    }
    case PROP_STATE:
{
      ( *(session -> priv)).state = g_value_get_boolean(value);
      break; 
    }
    case PROP_CONTENTS:
{
      ( *(session -> priv)).contents = (g_value_get_pointer(value));
      break; 
    }
    case PROP_PENDING_CONTENTS:
{
      ( *(session -> priv)).pending_contents = (g_value_get_pointer(value));
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = prop_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","session.c:227","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void jingle_session_get_property(GObject *object,guint prop_id,GValue *value,GParamSpec *pspec)
{
  JingleSession *session;
  do {
    if (object != ((GObject *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"object != NULL");
      return ;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = jingle_session_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"JINGLE_IS_SESSION(object)");
      return ;
    };
  }while (0);
  session = ((JingleSession *)(g_type_check_instance_cast(((GTypeInstance *)object),jingle_session_get_type())));
  switch(prop_id){
    case PROP_SID:
{
      g_value_set_string(value,( *(session -> priv)).sid);
      break; 
    }
    case PROP_JS:
{
      g_value_set_pointer(value,( *(session -> priv)).js);
      break; 
    }
    case PROP_REMOTE_JID:
{
      g_value_set_string(value,( *(session -> priv)).remote_jid);
      break; 
    }
    case PROP_LOCAL_JID:
{
      g_value_set_string(value,( *(session -> priv)).local_jid);
      break; 
    }
    case PROP_IS_INITIATOR:
{
      g_value_set_boolean(value,( *(session -> priv)).is_initiator);
      break; 
    }
    case PROP_STATE:
{
      g_value_set_boolean(value,( *(session -> priv)).state);
      break; 
    }
    case PROP_CONTENTS:
{
      g_value_set_pointer(value,( *(session -> priv)).contents);
      break; 
    }
    case PROP_PENDING_CONTENTS:
{
      g_value_set_pointer(value,( *(session -> priv)).pending_contents);
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = prop_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","session.c:268","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

JingleSession *jingle_session_create(JabberStream *js,const gchar *sid,const gchar *local_jid,const gchar *remote_jid,gboolean is_initiator)
{
  JingleSession *session = (g_object_new(jingle_session_get_type(),"js",js,"sid",sid,"local-jid",local_jid,"remote-jid",remote_jid,"is_initiator",is_initiator,((void *)((void *)0))));
/* insert it into the hash table */
  if (!((js -> sessions) != 0)) {
    purple_debug_info("jingle","Creating hash table for sessions\n");
    js -> sessions = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,0);
  }
  purple_debug_info("jingle","inserting session with key: %s into table\n",sid);
  g_hash_table_insert((js -> sessions),(g_strdup(sid)),session);
  return session;
}

JabberStream *jingle_session_get_js(JingleSession *session)
{
  JabberStream *js;
  g_object_get(session,"js",&js,((void *)((void *)0)));
  return js;
}

gchar *jingle_session_get_sid(JingleSession *session)
{
  gchar *sid;
  g_object_get(session,"sid",&sid,((void *)((void *)0)));
  return sid;
}

gchar *jingle_session_get_local_jid(JingleSession *session)
{
  gchar *local_jid;
  g_object_get(session,"local-jid",&local_jid,((void *)((void *)0)));
  return local_jid;
}

gchar *jingle_session_get_remote_jid(JingleSession *session)
{
  gchar *remote_jid;
  g_object_get(session,"remote-jid",&remote_jid,((void *)((void *)0)));
  return remote_jid;
}

gboolean jingle_session_is_initiator(JingleSession *session)
{
  gboolean is_initiator;
  g_object_get(session,"is-initiator",&is_initiator,((void *)((void *)0)));
  return is_initiator;
}

gboolean jingle_session_get_state(JingleSession *session)
{
  gboolean state;
  g_object_get(session,"state",&state,((void *)((void *)0)));
  return state;
}

GList *jingle_session_get_contents(JingleSession *session)
{
  GList *contents;
  g_object_get(session,"contents",&contents,((void *)((void *)0)));
  return contents;
}

GList *jingle_session_get_pending_contents(JingleSession *session)
{
  GList *pending_contents;
  g_object_get(session,"pending-contents",&pending_contents,((void *)((void *)0)));
  return pending_contents;
}

JingleSession *jingle_session_find_by_sid(JabberStream *js,const gchar *sid)
{
  JingleSession *session = (JingleSession *)((void *)0);
  if ((js -> sessions) != 0) 
    session = (g_hash_table_lookup((js -> sessions),sid));
  purple_debug_info("jingle","find_by_id %s\n",sid);
  purple_debug_info("jingle","lookup: %p\n",session);
  return session;
}

static gboolean find_by_jid_ghr(gpointer key,gpointer value,gpointer user_data)
{
  JingleSession *session = (JingleSession *)value;
  const gchar *jid = user_data;
  gboolean use_bare = (strchr(jid,'/') == ((char *)((void *)0)));
  gchar *remote_jid = jingle_session_get_remote_jid(session);
  gchar *cmp_jid = (use_bare != 0)?jabber_get_bare_jid(remote_jid) : g_strdup(remote_jid);
  g_free(remote_jid);
  if (g_str_equal(jid,cmp_jid) != 0) {
    g_free(cmp_jid);
    return (!0);
  }
  g_free(cmp_jid);
  return 0;
}

JingleSession *jingle_session_find_by_jid(JabberStream *js,const gchar *jid)
{
  return (((js -> sessions) != ((GHashTable *)((void *)0)))?g_hash_table_find((js -> sessions),find_by_jid_ghr,((gpointer )jid)) : ((void *)((void *)0)));
}

static xmlnode *jingle_add_jingle_packet(JingleSession *session,JabberIq *iq,JingleActionType action)
{
  xmlnode *jingle = (iq != 0)?xmlnode_new_child((iq -> node),"jingle") : xmlnode_new("jingle");
  gchar *local_jid = jingle_session_get_local_jid(session);
  gchar *remote_jid = jingle_session_get_remote_jid(session);
  gchar *sid = jingle_session_get_sid(session);
  xmlnode_set_namespace(jingle,"urn:xmpp:jingle:1");
  xmlnode_set_attrib(jingle,"action",jingle_get_action_name(action));
  if (jingle_session_is_initiator(session) != 0) {
    xmlnode_set_attrib(jingle,"initiator",local_jid);
    xmlnode_set_attrib(jingle,"responder",remote_jid);
  }
  else {
    xmlnode_set_attrib(jingle,"initiator",remote_jid);
    xmlnode_set_attrib(jingle,"responder",local_jid);
  }
  xmlnode_set_attrib(jingle,"sid",sid);
  g_free(local_jid);
  g_free(remote_jid);
  g_free(sid);
  return jingle;
}

JabberIq *jingle_session_create_ack(JingleSession *session,const xmlnode *jingle)
{
  JabberIq *result = jabber_iq_new(jingle_session_get_js(session),JABBER_IQ_RESULT);
  xmlnode *packet = xmlnode_get_parent(jingle);
  jabber_iq_set_id(result,xmlnode_get_attrib(packet,"id"));
  xmlnode_set_attrib((result -> node),"from",xmlnode_get_attrib(packet,"to"));
  xmlnode_set_attrib((result -> node),"to",xmlnode_get_attrib(packet,"from"));
  return result;
}

static JabberIq *jingle_create_iq(JingleSession *session)
{
  JabberStream *js = jingle_session_get_js(session);
  JabberIq *result = jabber_iq_new(js,JABBER_IQ_SET);
  gchar *from = jingle_session_get_local_jid(session);
  gchar *to = jingle_session_get_remote_jid(session);
  xmlnode_set_attrib((result -> node),"from",from);
  xmlnode_set_attrib((result -> node),"to",to);
  g_free(from);
  g_free(to);
  return result;
}

xmlnode *jingle_session_to_xml(JingleSession *session,xmlnode *jingle,JingleActionType action)
{
  if ((action != JINGLE_SESSION_INFO) && (action != JINGLE_SESSION_TERMINATE)) {
    GList *iter;
    if (((action == JINGLE_CONTENT_ACCEPT) || (action == JINGLE_CONTENT_ADD)) || (action == JINGLE_CONTENT_REMOVE)) 
      iter = jingle_session_get_pending_contents(session);
    else 
      iter = jingle_session_get_contents(session);
    for (; iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
      jingle_content_to_xml((iter -> data),jingle,action);
    }
  }
  return jingle;
}

JabberIq *jingle_session_to_packet(JingleSession *session,JingleActionType action)
{
  JabberIq *iq = jingle_create_iq(session);
  xmlnode *jingle = jingle_add_jingle_packet(session,iq,action);
  jingle_session_to_xml(session,jingle,action);
  return iq;
}

void jingle_session_handle_action(JingleSession *session,xmlnode *jingle,JingleActionType action)
{
  GList *iter;
  if ((action == JINGLE_CONTENT_ADD) || (action == JINGLE_CONTENT_REMOVE)) 
    iter = jingle_session_get_pending_contents(session);
  else 
    iter = jingle_session_get_contents(session);
  for (; iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
    jingle_content_handle_action((iter -> data),jingle,action);
  }
}

struct _JingleContent *jingle_session_find_content(JingleSession *session,const gchar *name,const gchar *creator)
{
  GList *iter;
  if (name == ((const gchar *)((void *)0))) 
    return 0;
  iter = ( *(session -> priv)).contents;
  for (; iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
    JingleContent *content = (iter -> data);
    gchar *cname = jingle_content_get_name(content);
    gboolean result = g_str_equal(name,cname);
    g_free(cname);
    if (creator != ((const gchar *)((void *)0))) {
      gchar *ccreator = jingle_content_get_creator(content);
      result = ((result != 0) && !(strcmp(creator,ccreator) != 0));
      g_free(ccreator);
    }
    if (result == !0) 
      return content;
  }
  return 0;
}

struct _JingleContent *jingle_session_find_pending_content(JingleSession *session,const gchar *name,const gchar *creator)
{
  GList *iter;
  if (name == ((const gchar *)((void *)0))) 
    return 0;
  iter = ( *(session -> priv)).pending_contents;
  for (; iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
    JingleContent *content = (iter -> data);
    gchar *cname = jingle_content_get_name(content);
    gboolean result = g_str_equal(name,cname);
    g_free(cname);
    if (creator != ((const gchar *)((void *)0))) {
      gchar *ccreator = jingle_content_get_creator(content);
      result = ((result != 0) && !(strcmp(creator,ccreator) != 0));
      g_free(ccreator);
    }
    if (result == !0) 
      return content;
  }
  return 0;
}

void jingle_session_add_content(JingleSession *session,JingleContent *content)
{
  ( *(session -> priv)).contents = g_list_append(( *(session -> priv)).contents,content);
  jingle_content_set_session(content,session);
}

void jingle_session_remove_content(JingleSession *session,const gchar *name,const gchar *creator)
{
  JingleContent *content = jingle_session_find_content(session,name,creator);
  if (content != 0) {
    ( *(session -> priv)).contents = g_list_remove(( *(session -> priv)).contents,content);
    g_object_unref(content);
  }
}

void jingle_session_add_pending_content(JingleSession *session,JingleContent *content)
{
  ( *(session -> priv)).pending_contents = g_list_append(( *(session -> priv)).pending_contents,content);
  jingle_content_set_session(content,session);
}

void jingle_session_remove_pending_content(JingleSession *session,const gchar *name,const gchar *creator)
{
  JingleContent *content = jingle_session_find_pending_content(session,name,creator);
  if (content != 0) {
    ( *(session -> priv)).pending_contents = g_list_remove(( *(session -> priv)).pending_contents,content);
    g_object_unref(content);
  }
}

void jingle_session_accept_content(JingleSession *session,const gchar *name,const gchar *creator)
{
  JingleContent *content = jingle_session_find_pending_content(session,name,creator);
  if (content != 0) {
    g_object_ref(content);
    jingle_session_remove_pending_content(session,name,creator);
    jingle_session_add_content(session,content);
  }
}

void jingle_session_accept_session(JingleSession *session)
{
  ( *(session -> priv)).state = (!0);
}

JabberIq *jingle_session_terminate_packet(JingleSession *session,const gchar *reason)
{
  JabberIq *iq = jingle_session_to_packet(session,JINGLE_SESSION_TERMINATE);
  xmlnode *jingle = xmlnode_get_child((iq -> node),"jingle");
  if (reason != ((const gchar *)((void *)0))) {
    xmlnode *reason_node;
    reason_node = xmlnode_new_child(jingle,"reason");
    xmlnode_new_child(reason_node,reason);
  }
  return iq;
}

JabberIq *jingle_session_redirect_packet(JingleSession *session,const gchar *sid)
{
  JabberIq *iq = jingle_session_terminate_packet(session,"alternative-session");
  xmlnode *alt_session;
  if (sid == ((const gchar *)((void *)0))) 
    return iq;
  alt_session = xmlnode_get_child((iq -> node),"jingle/reason/alternative-session");
  if (alt_session != ((xmlnode *)((void *)0))) {
    xmlnode *sid_node = xmlnode_new_child(alt_session,"sid");
    xmlnode_insert_data(sid_node,sid,(-1));
  }
  return iq;
}
