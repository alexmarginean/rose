/*
 * purple - Jabber Protocol Plugin
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "account.h"
#include "accountopt.h"
#include "blist.h"
#include "core.h"
#include "cmds.h"
#include "connection.h"
#include "conversation.h"
#include "debug.h"
#include "dnssrv.h"
#include "imgstore.h"
#include "message.h"
#include "notify.h"
#include "pluginpref.h"
#include "privacy.h"
#include "proxy.h"
#include "prpl.h"
#include "request.h"
#include "server.h"
#include "status.h"
#include "util.h"
#include "version.h"
#include "xmlnode.h"
#include "auth.h"
#include "buddy.h"
#include "caps.h"
#include "chat.h"
#include "data.h"
#include "disco.h"
#include "google/google.h"
#include "google/google_roster.h"
#include "google/google_session.h"
#include "ibb.h"
#include "iq.h"
#include "jutil.h"
#include "message.h"
#include "parser.h"
#include "presence.h"
#include "jabber.h"
#include "roster.h"
#include "ping.h"
#include "si.h"
#include "usermood.h"
#include "xdata.h"
#include "pep.h"
#include "adhoccommands.h"
#include "jingle/jingle.h"
#include "jingle/rtp.h"
#define PING_TIMEOUT 60
/* Send a whitespace keepalive to the server if we haven't sent
 * anything in the last 120 seconds
 */
#define DEFAULT_INACTIVITY_TIME 120
GList *jabber_features = (GList *)((void *)0);
GList *jabber_identities = (GList *)((void *)0);
/* PurplePlugin * => GSList of ids */
static GHashTable *jabber_cmds = (GHashTable *)((void *)0);
static gint plugin_ref = 0;
static void jabber_unregister_account_cb(JabberStream *js);
static void try_srv_connect(JabberStream *js);

static void jabber_stream_init(JabberStream *js)
{
  char *open_stream;
  if ((js -> stream_id) != 0) {
    g_free((js -> stream_id));
    js -> stream_id = ((char *)((void *)0));
  }
  open_stream = g_strdup_printf("<stream:stream to=\'%s\' xmlns=\'jabber:client\' xmlns:stream=\'http://etherx.jabber.org/streams\' version=\'1.0\'>",( *(js -> user)).domain);
/* setup the parser fresh for each stream */
  jabber_parser_setup(js);
  jabber_send_raw(js,open_stream,-1);
  js -> reinit = 0;
  g_free(open_stream);
}

static void jabber_session_initialized_cb(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  if (type == JABBER_IQ_RESULT) {
    jabber_disco_items_server(js);
    if ((js -> unregistration) != 0) 
      jabber_unregister_account_cb(js);
  }
  else {
    purple_connection_error_reason((js -> gc),PURPLE_CONNECTION_ERROR_NETWORK_ERROR,"Error initializing session");
  }
}

static void jabber_session_init(JabberStream *js)
{
  JabberIq *iq = jabber_iq_new(js,JABBER_IQ_SET);
  xmlnode *session;
  jabber_iq_set_callback(iq,jabber_session_initialized_cb,0);
  session = xmlnode_new_child((iq -> node),"session");
  xmlnode_set_namespace(session,"urn:ietf:params:xml:ns:xmpp-session");
  jabber_iq_send(iq);
}

static void jabber_bind_result_cb(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  xmlnode *bind;
  if ((type == JABBER_IQ_RESULT) && ((bind = xmlnode_get_child_with_namespace(packet,"bind","urn:ietf:params:xml:ns:xmpp-bind")) != 0)) {
    xmlnode *jid;
    char *full_jid;
    if (((jid = xmlnode_get_child(bind,"jid")) != 0) && ((full_jid = xmlnode_get_data(jid)) != 0)) {
      jabber_id_free((js -> user));
      js -> user = jabber_id_new(full_jid);
      if ((js -> user) == ((JabberID *)((void *)0))) {
        purple_connection_error_reason((js -> gc),PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Invalid response from server"))));
        g_free(full_jid);
        return ;
      }
      js -> user_jb = jabber_buddy_find(js,full_jid,(!0));
      ( *(js -> user_jb)).subscription |= JABBER_SUB_BOTH;
      purple_connection_set_display_name((js -> gc),full_jid);
      g_free(full_jid);
    }
  }
  else {
    PurpleConnectionError reason = PURPLE_CONNECTION_ERROR_NETWORK_ERROR;
    char *msg = jabber_parse_error(js,packet,&reason);
    purple_connection_error_reason((js -> gc),reason,msg);
    g_free(msg);
    return ;
  }
  jabber_session_init(js);
}

static char *jabber_prep_resource(char *input)
{
/* current hostname */
  char hostname[256UL];
  char *dot = (char *)((void *)0);
/* Empty resource == don't send any */
  if ((input == ((char *)((void *)0))) || (( *input) == 0)) 
    return 0;
  if (strstr(input,"__HOSTNAME__") == ((char *)((void *)0))) 
    return g_strdup(input);
/* Replace __HOSTNAME__ with hostname */
  if (gethostname(hostname,(sizeof(hostname) - 1)) != 0) {
    purple_debug_warning("jabber","gethostname: %s\n",g_strerror( *__errno_location()));
/* according to glibc doc, the only time an error is returned
		   is if the hostname is longer than the buffer, in which case
		   glibc 2.2+ would still fill the buffer with partial
		   hostname, so maybe we want to detect that and use it
		   instead
		*/
    g_strlcpy(hostname,"localhost",(sizeof(hostname)));
  }
  hostname[sizeof(hostname) - 1] = 0;
/* We want only the short hostname, not the FQDN - this will prevent the
	 * resource string from being unreasonably long on systems which stuff the
	 * whole FQDN in the hostname */
  if ((dot = strchr(hostname,'.')) != 0) 
     *dot = 0;
  return purple_strreplace(input,"__HOSTNAME__",hostname);
}

static gboolean jabber_process_starttls(JabberStream *js,xmlnode *packet)
{
  PurpleAccount *account;
  xmlnode *starttls;
  account = purple_connection_get_account((js -> gc));
#if 0
/*
	 * This code DOES NOT EXIST, will never be enabled by default, and
	 * will never ever be supported (by me).
	 * It's literally *only* for developer testing.
	 */
#else
  if (purple_ssl_is_supported() != 0) {
    jabber_send_raw(js,"<starttls xmlns=\'urn:ietf:params:xml:ns:xmpp-tls\'/>",-1);
    return (!0);
  }
  else {
    purple_debug_warning("jabber","No libpurple TLS/SSL support found.");
  }
#endif
  starttls = xmlnode_get_child(packet,"starttls");
  if (xmlnode_get_child(starttls,"required") != 0) {
    purple_connection_error_reason((js -> gc),PURPLE_CONNECTION_ERROR_NO_SSL_SUPPORT,((const char *)(dgettext("pidgin","Server requires TLS/SSL, but no TLS/SSL support was found."))));
    return (!0);
  }
  if (g_str_equal("require_tls",(purple_account_get_string(account,"connection_security","require_starttls"))) != 0) {
    purple_connection_error_reason((js -> gc),PURPLE_CONNECTION_ERROR_NO_SSL_SUPPORT,((const char *)(dgettext("pidgin","You require encryption, but no TLS/SSL support was found."))));
    return (!0);
  }
  return 0;
}

void jabber_stream_features_parse(JabberStream *js,xmlnode *packet)
{
  PurpleAccount *account = purple_connection_get_account((js -> gc));
  const char *connection_security = purple_account_get_string(account,"connection_security","require_starttls");
  if (xmlnode_get_child(packet,"starttls") != 0) {
    if (jabber_process_starttls(js,packet) != 0) {
      jabber_stream_set_state(js,JABBER_STREAM_INITIALIZING_ENCRYPTION);
      return ;
    }
  }
  else if ((g_str_equal(connection_security,"require_tls") != 0) && !(jabber_stream_is_ssl(js) != 0)) {
    purple_connection_error_reason((js -> gc),PURPLE_CONNECTION_ERROR_ENCRYPTION_ERROR,((const char *)(dgettext("pidgin","You require encryption, but it is not available on this server."))));
    return ;
  }
  if ((js -> registration) != 0) {
    jabber_register_start(js);
  }
  else if (xmlnode_get_child(packet,"mechanisms") != 0) {
    jabber_stream_set_state(js,JABBER_STREAM_AUTHENTICATING);
    jabber_auth_start(js,packet);
  }
  else if (xmlnode_get_child(packet,"bind") != 0) {
    xmlnode *bind;
    xmlnode *resource;
    char *requested_resource;
    JabberIq *iq = jabber_iq_new(js,JABBER_IQ_SET);
    bind = xmlnode_new_child((iq -> node),"bind");
    xmlnode_set_namespace(bind,"urn:ietf:params:xml:ns:xmpp-bind");
    requested_resource = jabber_prep_resource(( *(js -> user)).resource);
    if (requested_resource != ((char *)((void *)0))) {
      resource = xmlnode_new_child(bind,"resource");
      xmlnode_insert_data(resource,requested_resource,(-1));
      g_free(requested_resource);
    }
    jabber_iq_set_callback(iq,jabber_bind_result_cb,0);
    jabber_iq_send(iq);
  }
  else if (xmlnode_get_child_with_namespace(packet,"ver","urn:xmpp:features:rosterver") != 0) {
    js -> server_caps |= JABBER_CAP_ROSTER_VERSIONING;
/* if(xmlnode_get_child_with_namespace(packet, "auth")) */
  }
  else {
/* If we get an empty stream:features packet, or we explicitly get
		 * an auth feature with namespace http://jabber.org/features/iq-auth
		 * we should revert back to iq:auth authentication, even though we're
		 * connecting to an XMPP server.  */
    jabber_stream_set_state(js,JABBER_STREAM_AUTHENTICATING);
    jabber_auth_start_old(js);
  }
}

static void jabber_stream_handle_error(JabberStream *js,xmlnode *packet)
{
  PurpleConnectionError reason = PURPLE_CONNECTION_ERROR_NETWORK_ERROR;
  char *msg = jabber_parse_error(js,packet,&reason);
  purple_connection_error_reason((js -> gc),reason,msg);
  g_free(msg);
}
static void tls_init(JabberStream *js);

void jabber_process_packet(JabberStream *js,xmlnode **packet)
{
  const char *name;
  const char *xmlns;
  purple_signal_emit((purple_connection_get_prpl((js -> gc))),"jabber-receiving-xmlnode",(js -> gc),packet);
/* if the signal leaves us with a null packet, we're done */
  if (((xmlnode *)((void *)0)) ==  *packet) 
    return ;
  name = ( *( *packet)).name;
  xmlns = xmlnode_get_namespace( *packet);
  if (!(strcmp(( *( *packet)).name,"iq") != 0)) {
    jabber_iq_parse(js, *packet);
  }
  else if (!(strcmp(( *( *packet)).name,"presence") != 0)) {
    jabber_presence_parse(js, *packet);
  }
  else if (!(strcmp(( *( *packet)).name,"message") != 0)) {
    jabber_message_parse(js, *packet);
  }
  else if (purple_strequal(xmlns,"http://etherx.jabber.org/streams") != 0) {
    if (g_str_equal(name,"features") != 0) 
      jabber_stream_features_parse(js, *packet);
    else if (g_str_equal(name,"error") != 0) 
      jabber_stream_handle_error(js, *packet);
  }
  else if (purple_strequal(xmlns,"urn:ietf:params:xml:ns:xmpp-sasl") != 0) {
    if ((js -> state) != JABBER_STREAM_AUTHENTICATING) 
      purple_debug_warning("jabber","Ignoring spurious SASL stanza %s\n",name);
    else {
      if (g_str_equal(name,"challenge") != 0) 
        jabber_auth_handle_challenge(js, *packet);
      else if (g_str_equal(name,"success") != 0) 
        jabber_auth_handle_success(js, *packet);
      else if (g_str_equal(name,"failure") != 0) 
        jabber_auth_handle_failure(js, *packet);
    }
  }
  else if (purple_strequal(xmlns,"urn:ietf:params:xml:ns:xmpp-tls") != 0) {
    if (((js -> state) != JABBER_STREAM_INITIALIZING_ENCRYPTION) || ((js -> gsc) != 0)) 
      purple_debug_warning("jabber","Ignoring spurious %s\n",name);
    else {
      if (g_str_equal(name,"proceed") != 0) 
        tls_init(js);
/* TODO: Handle <failure/>, I guess? */
    }
  }
  else {
    purple_debug_warning("jabber","Unknown packet: %s\n",( *( *packet)).name);
  }
}

static int jabber_do_send(JabberStream *js,const char *data,int len)
{
  int ret;
  if ((js -> gsc) != 0) 
    ret = (purple_ssl_write((js -> gsc),data,len));
  else 
    ret = (write((js -> fd),data,len));
  return ret;
}

static void jabber_send_cb(gpointer data,gint source,PurpleInputCondition cond)
{
  JabberStream *js = data;
  int ret;
  int writelen;
  writelen = (purple_circ_buffer_get_max_read((js -> write_buffer)));
  if (writelen == 0) {
    purple_input_remove((js -> writeh));
    js -> writeh = 0;
    return ;
  }
  ret = jabber_do_send(js,( *(js -> write_buffer)).outptr,writelen);
  if ((ret < 0) && ( *__errno_location() == 11)) 
    return ;
  else if (ret <= 0) {
    gchar *tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Lost connection with server: %s"))),g_strerror( *__errno_location()));
    purple_connection_error_reason((js -> gc),PURPLE_CONNECTION_ERROR_NETWORK_ERROR,tmp);
    g_free(tmp);
    return ;
  }
  purple_circ_buffer_mark_read((js -> write_buffer),ret);
}

static gboolean do_jabber_send_raw(JabberStream *js,const char *data,int len)
{
  int ret;
  gboolean success = (!0);
  do {
    if (len > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"len > 0");
      return 0;
    };
  }while (0);
  if ((js -> state) == JABBER_STREAM_CONNECTED) 
    jabber_stream_restart_inactivity_timer(js);
  if ((js -> writeh) == 0) 
    ret = jabber_do_send(js,data,len);
  else {
    ret = -1;
     *__errno_location() = 11;
  }
  if ((ret < 0) && ( *__errno_location() != 11)) {
    PurpleAccount *account = purple_connection_get_account((js -> gc));
/*
		 * The server may have closed the socket (on a stream error), so if
		 * we're disconnecting, don't generate (possibly another) error that
		 * (for some UIs) would mask the first.
		 */
    if (!((account -> disconnecting) != 0)) {
      gchar *tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Lost connection with server: %s"))),g_strerror( *__errno_location()));
      purple_connection_error_reason((js -> gc),PURPLE_CONNECTION_ERROR_NETWORK_ERROR,tmp);
      g_free(tmp);
    }
    success = 0;
  }
  else if (ret < len) {
    if (ret < 0) 
      ret = 0;
    if ((js -> writeh) == 0) 
      js -> writeh = purple_input_add((((js -> gsc) != 0)?( *(js -> gsc)).fd : (js -> fd)),PURPLE_INPUT_WRITE,jabber_send_cb,js);
    purple_circ_buffer_append((js -> write_buffer),(data + ret),(len - ret));
  }
  return success;
}

void jabber_send_raw(JabberStream *js,const char *data,int len)
{
  PurpleConnection *gc;
  PurpleAccount *account;
  gc = (js -> gc);
  account = purple_connection_get_account(gc);
/* because printing a tab to debug every minute gets old */
  if (strcmp(data,"\t") != 0) {
    const char *username;
    char *text = (char *)((void *)0);
    char *last_part = (char *)((void *)0);
    char *tag_start = (char *)((void *)0);
/* Because debug logs with plaintext passwords make me sad */
    if ((!(purple_debug_is_unsafe() != 0) && ((js -> state) != JABBER_STREAM_CONNECTED)) && ((((tag_start = strstr(data,"<auth ")) != 0) && (strstr(data,"xmlns=\'urn:ietf:params:xml:ns:xmpp-sasl\'") != 0)) || ((((tag_start = strstr(data,"<query ")) != 0) && (strstr(data,"xmlns=\'jabber:iq:auth\'>") != 0)) && ((tag_start = strstr(tag_start,"<password>")) != 0)))) 
/* Either <auth> or <query><password>... */
{
      char *data_start;
      char *tag_end = strchr(tag_start,'>');
      text = g_strdup(data);
/* Better to print out some wacky debugging than crash
			 * due to a plugin sending bad xml */
      if (tag_end == ((char *)((void *)0))) 
        tag_end = tag_start;
      data_start = ((text + (tag_end - data)) + 1);
      last_part = strchr(data_start,60);
       *data_start = 0;
    }
    username = purple_connection_get_display_name(gc);
    if (!(username != 0)) 
      username = purple_account_get_username(account);
    purple_debug_misc("jabber","Sending%s (%s): %s%s%s\n",((jabber_stream_is_ssl(js) != 0)?" (ssl)" : ""),username,((text != 0)?text : data),((last_part != 0)?"password removed" : ""),((last_part != 0)?last_part : ""));
    g_free(text);
  }
  purple_signal_emit((purple_connection_get_prpl(gc)),"jabber-sending-text",gc,&data);
  if (data == ((const char *)((void *)0))) 
    return ;
  if (len == -1) 
    len = (strlen(data));
/* If we've got a security layer, we need to encode the data,
	 * splitting it on the maximum buffer length negotiated */
#ifdef HAVE_CYRUS_SASL
/* do_jabber_send_raw returns FALSE when it throws a
			 * connection error.
			 */
#endif
  if ((js -> bosh) != 0) 
    jabber_bosh_connection_send_raw((js -> bosh),data);
  else 
    do_jabber_send_raw(js,data,len);
}

int jabber_prpl_send_raw(PurpleConnection *gc,const char *buf,int len)
{
  JabberStream *js = (purple_connection_get_protocol_data(gc));
  do {
    if (js != ((JabberStream *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"js != NULL");
      return -1;
    };
  }while (0);
/* TODO: It's probably worthwhile to restrict this to when the account
	 * state is CONNECTED, but I can /almost/ envision reasons for wanting
	 * to do things during the connection process.
	 */
  jabber_send_raw(js,buf,len);
  return ((len < 0)?strlen(buf) : len);
}

void jabber_send_signal_cb(PurpleConnection *pc,xmlnode **packet,gpointer unused)
{
  JabberStream *js;
  char *txt;
  int len;
  if (((xmlnode **)((void *)0)) == packet) 
    return ;
  do {
    if (g_list_find(purple_connections_get_all(),pc) != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_CONNECTION_IS_VALID(pc)");
      return ;
    };
  }while (0);
  js = (purple_connection_get_protocol_data(pc));
  if (((JabberStream *)((void *)0)) == js) 
    return ;
  if ((js -> bosh) != 0) 
    if (((g_str_equal(( *( *packet)).name,"message") != 0) || (g_str_equal(( *( *packet)).name,"iq") != 0)) || (g_str_equal(( *( *packet)).name,"presence") != 0)) 
      xmlnode_set_namespace( *packet,"jabber:client");
  txt = xmlnode_to_str(( *packet),&len);
  jabber_send_raw(js,txt,len);
  g_free(txt);
}

void jabber_send(JabberStream *js,xmlnode *packet)
{
  purple_signal_emit((purple_connection_get_prpl((js -> gc))),"jabber-sending-xmlnode",(js -> gc),&packet);
}

static gboolean jabber_keepalive_timeout(PurpleConnection *gc)
{
  JabberStream *js = (gc -> proto_data);
  purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Ping timed out"))));
  js -> keepalive_timeout = 0;
  return 0;
}

void jabber_keepalive(PurpleConnection *gc)
{
  JabberStream *js = (purple_connection_get_protocol_data(gc));
  time_t now = time(0);
  if (((js -> keepalive_timeout) == 0) && ((now - (js -> last_ping)) >= 60)) {
    js -> last_ping = now;
    jabber_keepalive_ping(js);
    js -> keepalive_timeout = purple_timeout_add_seconds(120,((GSourceFunc )jabber_keepalive_timeout),gc);
  }
}

static void jabber_recv_cb_ssl(gpointer data,PurpleSslConnection *gsc,PurpleInputCondition cond)
{
  PurpleConnection *gc = data;
  JabberStream *js = (gc -> proto_data);
  int len;
  static char buf[4096UL];
/* TODO: It should be possible to make this check unnecessary */
  if (!(g_list_find(purple_connections_get_all(),gc) != ((GList *)((void *)0)))) {
    purple_ssl_close(gsc);
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","jabber.c",652,((const char *)__func__));
      return ;
    }while (0);
  }
  while((len = (purple_ssl_read(gsc,buf,(sizeof(buf) - 1)))) > 0){
    gc -> last_received = time(0);
    buf[len] = 0;
    purple_debug_info("jabber","Recv (ssl)(%d): %s\n",len,buf);
    jabber_parser_process(js,buf,len);
    if ((js -> reinit) != 0) 
      jabber_stream_init(js);
  }
  if ((len < 0) && ( *__errno_location() == 11)) 
    return ;
  else {
    gchar *tmp;
    if (len == 0) 
      tmp = g_strdup(((const char *)(dgettext("pidgin","Server closed the connection"))));
    else 
      tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Lost connection with server: %s"))),g_strerror( *__errno_location()));
    purple_connection_error_reason((js -> gc),PURPLE_CONNECTION_ERROR_NETWORK_ERROR,tmp);
    g_free(tmp);
  }
}

static void jabber_recv_cb(gpointer data,gint source,PurpleInputCondition condition)
{
  PurpleConnection *gc = data;
  JabberStream *js = (purple_connection_get_protocol_data(gc));
  int len;
  static char buf[4096UL];
  do {
    if (g_list_find(purple_connections_get_all(),gc) != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_CONNECTION_IS_VALID(gc)");
      return ;
    };
  }while (0);
  if ((len = (read((js -> fd),buf,(sizeof(buf) - 1)))) > 0) {
    gc -> last_received = time(0);
#ifdef HAVE_CYRUS_SASL
#endif
    buf[len] = 0;
    purple_debug_info("jabber","Recv (%d): %s\n",len,buf);
    jabber_parser_process(js,buf,len);
    if ((js -> reinit) != 0) 
      jabber_stream_init(js);
  }
  else if ((len < 0) && ( *__errno_location() == 11)) {
    return ;
  }
  else {
    gchar *tmp;
    if (len == 0) 
      tmp = g_strdup(((const char *)(dgettext("pidgin","Server closed the connection"))));
    else 
      tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Lost connection with server: %s"))),g_strerror( *__errno_location()));
    purple_connection_error_reason((js -> gc),PURPLE_CONNECTION_ERROR_NETWORK_ERROR,tmp);
    g_free(tmp);
  }
}

static void jabber_login_callback_ssl(gpointer data,PurpleSslConnection *gsc,PurpleInputCondition cond)
{
  PurpleConnection *gc = data;
  JabberStream *js;
/* TODO: It should be possible to make this check unnecessary */
  if (!(g_list_find(purple_connections_get_all(),gc) != ((GList *)((void *)0)))) {
    purple_ssl_close(gsc);
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","jabber.c",747,((const char *)__func__));
      return ;
    }while (0);
  }
  js = (gc -> proto_data);
  if ((js -> state) == JABBER_STREAM_CONNECTING) 
    jabber_send_raw(js,"<\?xml version=\'1.0\' \?>",-1);
  jabber_stream_set_state(js,JABBER_STREAM_INITIALIZING);
  purple_ssl_input_add(gsc,jabber_recv_cb_ssl,gc);
/* Tell the app that we're doing encryption */
  jabber_stream_set_state(js,JABBER_STREAM_INITIALIZING_ENCRYPTION);
}

static void txt_resolved_cb(GList *responses,gpointer data)
{
  JabberStream *js = data;
  gboolean found = 0;
  js -> srv_query_data = ((PurpleSrvTxtQueryData *)((void *)0));
{
    while(responses != 0){
      PurpleTxtResponse *resp = (responses -> data);
      gchar **token;
      token = g_strsplit(purple_txt_response_get_content(resp),"=",2);
      if (!(strcmp(token[0],"_xmpp-client-xbosh") != 0)) {
        purple_debug_info("jabber","Found alternative connection method using %s at %s.\n",token[0],token[1]);
        js -> bosh = jabber_bosh_connection_init(js,token[1]);
        g_strfreev(token);
        break; 
      }
      g_strfreev(token);
      purple_txt_response_destroy(resp);
      responses = g_list_delete_link(responses,responses);
    }
  }
  if ((js -> bosh) != 0) {
    found = (!0);
    jabber_bosh_connection_connect((js -> bosh));
  }
  if (!(found != 0)) {
    purple_debug_warning("jabber","Unable to find alternative XMPP connection methods after failing to connect directly.\n");
    purple_connection_error_reason((js -> gc),PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Unable to connect"))));
    return ;
  }
  if (responses != 0) {
    g_list_foreach(responses,((GFunc )purple_txt_response_destroy),0);
    g_list_free(responses);
  }
}

static void jabber_login_callback(gpointer data,gint source,const gchar *error)
{
  PurpleConnection *gc = data;
  JabberStream *js = (purple_connection_get_protocol_data(gc));
  if (source < 0) {
    if ((js -> srv_rec) != ((PurpleSrvResponse *)((void *)0))) {
      purple_debug_error("jabber","Unable to connect to server: %s.  Trying next SRV record or connecting directly.\n",error);
      try_srv_connect(js);
    }
    else {
      purple_debug_info("jabber","Couldn\'t connect directly to %s.  Trying to find alternative connection methods, like BOSH.\n",( *(js -> user)).domain);
      js -> srv_query_data = purple_txt_resolve_account(purple_connection_get_account(gc),"_xmppconnect",( *(js -> user)).domain,txt_resolved_cb,js);
    }
    return ;
  }
  g_free((js -> srv_rec));
  js -> srv_rec = ((PurpleSrvResponse *)((void *)0));
  js -> fd = source;
  if ((js -> state) == JABBER_STREAM_CONNECTING) 
    jabber_send_raw(js,"<\?xml version=\'1.0\' \?>",-1);
  jabber_stream_set_state(js,JABBER_STREAM_INITIALIZING);
  gc -> inpa = (purple_input_add((js -> fd),PURPLE_INPUT_READ,jabber_recv_cb,gc));
}

static void jabber_ssl_connect_failure(PurpleSslConnection *gsc,PurpleSslErrorType error,gpointer data)
{
  PurpleConnection *gc = data;
  JabberStream *js;
/* If the connection is already disconnected, we don't need to do anything else */
  do {
    if (g_list_find(purple_connections_get_all(),gc) != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_CONNECTION_IS_VALID(gc)");
      return ;
    };
  }while (0);
  js = (gc -> proto_data);
  js -> gsc = ((PurpleSslConnection *)((void *)0));
  purple_connection_ssl_error(gc,error);
}

static void tls_init(JabberStream *js)
{
  purple_input_remove(( *(js -> gc)).inpa);
  ( *(js -> gc)).inpa = 0;
  js -> gsc = purple_ssl_connect_with_host_fd(( *(js -> gc)).account,(js -> fd),jabber_login_callback_ssl,jabber_ssl_connect_failure,(js -> certificate_CN),(js -> gc));
/* The fd is no longer our concern */
  js -> fd = -1;
}

static gboolean jabber_login_connect(JabberStream *js,const char *domain,const char *host,int port,gboolean fatal_failure)
{
/* host should be used in preference to domain to
	 * allow SASL authentication to work with FQDN of the server,
	 * but we use domain as fallback for when users enter IP address
	 * in connect server */
  g_free((js -> serverFQDN));
  if (purple_ip_address_is_valid(host) != 0) 
    js -> serverFQDN = g_strdup(domain);
  else 
    js -> serverFQDN = g_strdup(host);
  if (purple_proxy_connect((js -> gc),purple_connection_get_account((js -> gc)),host,port,jabber_login_callback,(js -> gc)) == ((PurpleProxyConnectData *)((void *)0))) {
    if (fatal_failure != 0) {
      purple_connection_error_reason((js -> gc),PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Unable to connect"))));
    }
    return 0;
  }
  return (!0);
}

static void try_srv_connect(JabberStream *js)
{
  while(((js -> srv_rec) != ((PurpleSrvResponse *)((void *)0))) && ((js -> srv_rec_idx) < (js -> max_srv_rec_idx))){
    PurpleSrvResponse *tmp_resp = ((js -> srv_rec) + js -> srv_rec_idx++);
    if (jabber_login_connect(js,(tmp_resp -> hostname),(tmp_resp -> hostname),(tmp_resp -> port),0) != 0) 
      return ;
  }
  g_free((js -> srv_rec));
  js -> srv_rec = ((PurpleSrvResponse *)((void *)0));
/* Fall back to the defaults (I'm not sure if we should actually do this) */
  jabber_login_connect(js,( *(js -> user)).domain,( *(js -> user)).domain,purple_account_get_int((purple_connection_get_account((js -> gc))),"port",5222),(!0));
}

static void srv_resolved_cb(PurpleSrvResponse *resp,int results,gpointer data)
{
  JabberStream *js = data;
  js -> srv_query_data = ((PurpleSrvTxtQueryData *)((void *)0));
  if (results != 0) {
    js -> srv_rec = resp;
    js -> srv_rec_idx = 0;
    js -> max_srv_rec_idx = results;
    try_srv_connect(js);
  }
  else {
    jabber_login_connect(js,( *(js -> user)).domain,( *(js -> user)).domain,purple_account_get_int((purple_connection_get_account((js -> gc))),"port",5222),(!0));
  }
}

static JabberStream *jabber_stream_new(PurpleAccount *account)
{
  PurpleConnection *gc = purple_account_get_connection(account);
  JabberStream *js;
  PurplePresence *presence;
  gchar *user;
  gchar *slash;
  js = (gc -> proto_data = ((JabberStream *)(g_malloc0_n(1,(sizeof(JabberStream ))))));
  js -> gc = gc;
  js -> fd = -1;
  user = g_strdup(purple_account_get_username(account));
/* jabber_id_new doesn't accept "user@domain/" as valid */
  slash = strchr(user,'/');
  if ((slash != 0) && (slash[1] == 0)) 
     *slash = 0;
  js -> user = jabber_id_new(user);
  if (!((js -> user) != 0)) {
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_INVALID_SETTINGS,((const char *)(dgettext("pidgin","Invalid XMPP ID"))));
    g_free(user);
/* Destroying the connection will free the JabberStream */
    return 0;
  }
  if (!(( *(js -> user)).node != 0) || (( *( *(js -> user)).node) == 0)) {
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_INVALID_SETTINGS,((const char *)(dgettext("pidgin","Invalid XMPP ID. Username portion must be set."))));
    g_free(user);
/* Destroying the connection will free the JabberStream */
    return 0;
  }
  if (!(( *(js -> user)).domain != 0) || (( *( *(js -> user)).domain) == 0)) {
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_INVALID_SETTINGS,((const char *)(dgettext("pidgin","Invalid XMPP ID. Domain must be set."))));
    g_free(user);
/* Destroying the connection will free the JabberStream */
    return 0;
  }
  js -> buddies = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,((GDestroyNotify )jabber_buddy_free));
/* This is overridden during binding, but we need it here
	 * in case the server only does legacy non-sasl auth!.
	 */
  purple_connection_set_display_name(gc,user);
  js -> user_jb = jabber_buddy_find(js,user,(!0));
  g_free(user);
  if (!((js -> user_jb) != 0)) {
/* This basically *can't* fail, but for good measure... */
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_INVALID_SETTINGS,((const char *)(dgettext("pidgin","Invalid XMPP ID"))));
/* Destroying the connection will free the JabberStream */
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","jabber.c",985,((const char *)__func__));
      return 0;
    }while (0);
  }
  ( *(js -> user_jb)).subscription |= JABBER_SUB_BOTH;
  js -> iq_callbacks = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,((GDestroyNotify )jabber_iq_callbackdata_free));
  js -> chats = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,((GDestroyNotify )jabber_chat_free));
  js -> next_id = (g_random_int());
  js -> write_buffer = purple_circ_buffer_new(512);
  js -> old_length = 0;
  js -> keepalive_timeout = 0;
  js -> max_inactivity = 120;
/* Set the default protocol version to 1.0. Overridden in parser.c. */
  js -> protocol_version.major = 1;
  js -> protocol_version.minor = 0;
  js -> sessions = ((GHashTable *)((void *)0));
  js -> stun_ip = ((gchar *)((void *)0));
  js -> stun_port = 0;
  js -> stun_query = ((PurpleDnsQueryData *)((void *)0));
  js -> google_relay_token = ((gchar *)((void *)0));
  js -> google_relay_host = ((gchar *)((void *)0));
  js -> google_relay_requests = ((GList *)((void *)0));
/* if we are idle, set idle-ness on the stream (this could happen if we get
		disconnected and the reconnects while being idle. I don't think it makes
		sense to do this when registering a new account... */
  presence = purple_account_get_presence(account);
  if (purple_presence_is_idle(presence) != 0) 
    js -> idle = purple_presence_get_idle_time(presence);
  return js;
}

static void jabber_stream_connect(JabberStream *js)
{
  PurpleConnection *gc = (js -> gc);
  PurpleAccount *account = purple_connection_get_account(gc);
  const char *connect_server = purple_account_get_string(account,"connect_server","");
  const char *bosh_url = purple_account_get_string(account,"bosh_url","");
  jabber_stream_set_state(js,JABBER_STREAM_CONNECTING);
/* If both BOSH and a Connect Server are specified, we prefer BOSH. I'm not
	 * attached to that choice, though.
	 */
  if (( *bosh_url) != 0) {
    js -> bosh = jabber_bosh_connection_init(js,bosh_url);
    if ((js -> bosh) != 0) 
      jabber_bosh_connection_connect((js -> bosh));
    else {
      purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_INVALID_SETTINGS,((const char *)(dgettext("pidgin","Malformed BOSH URL"))));
    }
    return ;
  }
  js -> certificate_CN = g_strdup(((connect_server[0] != 0)?connect_server : ( *(js -> user)).domain));
/* if they've got old-ssl mode going, we probably want to ignore SRV lookups */
  if (g_str_equal("old_ssl",(purple_account_get_string(account,"connection_security","require_starttls"))) != 0) {
    if (purple_ssl_is_supported() != 0) {
      js -> gsc = purple_ssl_connect(account,(js -> certificate_CN),purple_account_get_int(account,"port",5223),jabber_login_callback_ssl,jabber_ssl_connect_failure,gc);
      if (!((js -> gsc) != 0)) {
        purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NO_SSL_SUPPORT,((const char *)(dgettext("pidgin","Unable to establish SSL connection"))));
      }
    }
    else {
      purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NO_SSL_SUPPORT,((const char *)(dgettext("pidgin","SSL support unavailable"))));
    }
    return ;
  }
/* no old-ssl, so if they've specified a connect server, we'll use that, otherwise we'll
	 * invoke the magic of SRV lookups, to figure out host and port */
  if (connect_server[0] != 0) {
    jabber_login_connect(js,( *(js -> user)).domain,connect_server,purple_account_get_int(account,"port",5222),(!0));
  }
  else {
    js -> srv_query_data = purple_srv_resolve_account(account,"xmpp-client","tcp",( *(js -> user)).domain,srv_resolved_cb,js);
  }
}

void jabber_login(PurpleAccount *account)
{
  PurpleConnection *gc = purple_account_get_connection(account);
  JabberStream *js;
  PurpleStoredImage *image;
  gc -> flags |= (PURPLE_CONNECTION_HTML | PURPLE_CONNECTION_ALLOW_CUSTOM_SMILEY);
  js = jabber_stream_new(account);
  if (js == ((JabberStream *)((void *)0))) 
    return ;
/* TODO: Remove this at some point.  Added 2010-02-14 (v2.6.6) */
  if (g_str_equal("proxy.jabber.org",(purple_account_get_string(account,"ft_proxies",""))) != 0) 
    purple_account_set_string(account,"ft_proxies","proxy.eu.jabber.org");
/*
	 * Calculate the avatar hash for our current image so we know (when we
	 * fetch our vCard and PEP avatar) if we should send our avatar to the
	 * server.
	 */
  image = purple_buddy_icons_find_account_icon(account);
  if (image != ((PurpleStoredImage *)((void *)0))) {
    js -> initial_avatar_hash = jabber_calculate_data_hash(purple_imgstore_get_data(image),purple_imgstore_get_size(image),"sha1");
    purple_imgstore_unref(image);
  }
  jabber_stream_connect(js);
}

static gboolean conn_close_cb(gpointer data)
{
  JabberStream *js = data;
  PurpleAccount *account = purple_connection_get_account((js -> gc));
  jabber_parser_free(js);
  purple_account_disconnect(account);
  return 0;
}

static void jabber_connection_schedule_close(JabberStream *js)
{
  purple_timeout_add(0,conn_close_cb,js);
}

static void jabber_registration_result_cb(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  PurpleAccount *account = purple_connection_get_account((js -> gc));
  char *buf;
  char *to = data;
  if (type == JABBER_IQ_RESULT) {
    if ((js -> registration) != 0) {
      buf = g_strdup_printf(((const char *)(dgettext("pidgin","Registration of %s@%s successful"))),( *(js -> user)).node,( *(js -> user)).domain);
      if ((account -> registration_cb) != 0) 
        ( *(account -> registration_cb))(account,(!0),(account -> registration_cb_user_data));
    }
    else {
      do {
        if (to != ((char *)((void *)0))) {
        }
        else {
          g_return_if_fail_warning(0,((const char *)__func__),"to != NULL");
          return ;
        };
      }while (0);
      buf = g_strdup_printf(((const char *)(dgettext("pidgin","Registration to %s successful"))),to);
    }
    purple_notify_message(0,PURPLE_NOTIFY_MSG_INFO,((const char *)(dgettext("pidgin","Registration Successful"))),((const char *)(dgettext("pidgin","Registration Successful"))),buf,0,0);
    g_free(buf);
  }
  else {
    char *msg = jabber_parse_error(js,packet,0);
    if (!(msg != 0)) 
      msg = g_strdup(((const char *)(dgettext("pidgin","Unknown Error"))));
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Registration Failed"))),((const char *)(dgettext("pidgin","Registration Failed"))),msg,0,0);
    g_free(msg);
    if ((account -> registration_cb) != 0) 
      ( *(account -> registration_cb))(account,0,(account -> registration_cb_user_data));
  }
  g_free(to);
  if ((js -> registration) != 0) 
    jabber_connection_schedule_close(js);
}

static void jabber_unregistration_result_cb(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  char *buf;
  char *to = data;
/* This function is never called for unregistering our XMPP account from
	 * the server, so there should always be a 'to' address. */
  do {
    if (to != ((char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"to != NULL");
      return ;
    };
  }while (0);
  if (type == JABBER_IQ_RESULT) {
    buf = g_strdup_printf(((const char *)(dgettext("pidgin","Registration from %s successfully removed"))),to);
    purple_notify_message(0,PURPLE_NOTIFY_MSG_INFO,((const char *)(dgettext("pidgin","Unregistration Successful"))),((const char *)(dgettext("pidgin","Unregistration Successful"))),buf,0,0);
    g_free(buf);
  }
  else {
    char *msg = jabber_parse_error(js,packet,0);
    if (!(msg != 0)) 
      msg = g_strdup(((const char *)(dgettext("pidgin","Unknown Error"))));
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Unregistration Failed"))),((const char *)(dgettext("pidgin","Unregistration Failed"))),msg,0,0);
    g_free(msg);
  }
  g_free(to);
}
typedef struct _JabberRegisterCBData {
JabberStream *js;
char *who;}JabberRegisterCBData;

static void jabber_register_cb(JabberRegisterCBData *cbdata,PurpleRequestFields *fields)
{
  GList *groups;
  GList *flds;
  xmlnode *query;
  xmlnode *y;
  JabberIq *iq;
  char *username;
  iq = jabber_iq_new_query((cbdata -> js),JABBER_IQ_SET,"jabber:iq:register");
  query = xmlnode_get_child((iq -> node),"query");
  if ((cbdata -> who) != 0) 
    xmlnode_set_attrib((iq -> node),"to",(cbdata -> who));
  for (groups = purple_request_fields_get_groups(fields); groups != 0; groups = (groups -> next)) {
    for (flds = purple_request_field_group_get_fields((groups -> data)); flds != 0; flds = (flds -> next)) {{
        PurpleRequestField *field = (flds -> data);
        const char *id = purple_request_field_get_id(field);
        if (!(strcmp(id,"unregister") != 0)) {
          gboolean value = purple_request_field_bool_get_value(field);
          if (value != 0) {
/* unregister from service. this doesn't include any of the fields, so remove them from the stanza by recreating it
					   (there's no "remove child" function for xmlnode) */
            jabber_iq_free(iq);
            iq = jabber_iq_new_query((cbdata -> js),JABBER_IQ_SET,"jabber:iq:register");
            query = xmlnode_get_child((iq -> node),"query");
            if ((cbdata -> who) != 0) 
              xmlnode_set_attrib((iq -> node),"to",(cbdata -> who));
            xmlnode_new_child(query,"remove");
            jabber_iq_set_callback(iq,jabber_unregistration_result_cb,(cbdata -> who));
            jabber_iq_send(iq);
            g_free(cbdata);
            return ;
          }
        }
        else {
          const char *ids[] = {("username"), ("password"), ("name"), ("email"), ("nick"), ("first"), ("last"), ("address"), ("city"), ("state"), ("zip"), ("phone"), ("url"), ("date"), ((const char *)((void *)0))};
          const char *value = purple_request_field_string_get_value(field);
          int i;
{
            for (i = 0; ids[i] != 0; i++) {
              if (!(strcmp(id,ids[i]) != 0)) 
                break; 
            }
          }
          if (!(ids[i] != 0)) 
            continue; 
          y = xmlnode_new_child(query,ids[i]);
          xmlnode_insert_data(y,value,(-1));
          if ((( *(cbdata -> js)).registration != 0) && !(strcmp(id,"username") != 0)) {
            g_free(( *( *(cbdata -> js)).user).node);
            ( *( *(cbdata -> js)).user).node = g_strdup(value);
          }
          if ((( *(cbdata -> js)).registration != 0) && !(strcmp(id,"password") != 0)) 
            purple_account_set_password(( *( *(cbdata -> js)).gc).account,value);
        }
      }
    }
  }
  if (( *(cbdata -> js)).registration != 0) {
    username = g_strdup_printf("%s@%s%s%s",( *( *(cbdata -> js)).user).node,( *( *(cbdata -> js)).user).domain,((( *( *(cbdata -> js)).user).resource != 0)?"/" : ""),((( *( *(cbdata -> js)).user).resource != 0)?( *( *(cbdata -> js)).user).resource : ""));
    purple_account_set_username(( *( *(cbdata -> js)).gc).account,username);
    g_free(username);
  }
  jabber_iq_set_callback(iq,jabber_registration_result_cb,(cbdata -> who));
  jabber_iq_send(iq);
  g_free(cbdata);
}

static void jabber_register_cancel_cb(JabberRegisterCBData *cbdata,PurpleRequestFields *fields)
{
  PurpleAccount *account = purple_connection_get_account(( *(cbdata -> js)).gc);
  if ((account != 0) && (( *(cbdata -> js)).registration != 0)) {
    if ((account -> registration_cb) != 0) 
      ( *(account -> registration_cb))(account,0,(account -> registration_cb_user_data));
    jabber_connection_schedule_close((cbdata -> js));
  }
  g_free((cbdata -> who));
  g_free(cbdata);
}

static void jabber_register_x_data_cb(JabberStream *js,xmlnode *result,gpointer data)
{
  xmlnode *query;
  JabberIq *iq;
  char *to = data;
  iq = jabber_iq_new_query(js,JABBER_IQ_SET,"jabber:iq:register");
  query = xmlnode_get_child((iq -> node),"query");
  if (to != 0) 
    xmlnode_set_attrib((iq -> node),"to",to);
  xmlnode_insert_child(query,result);
  jabber_iq_set_callback(iq,jabber_registration_result_cb,to);
  jabber_iq_send(iq);
}
static const struct __unnamed_class___F0_L1316_C14_unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__name__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__label {
const char *name;
const char *label;}registration_fields[] = {{("email"), ("Email")}, {("nick"), ("Nickname")}, {("first"), ("First name")}, {("last"), ("Last name")}, {("address"), ("Address")}, {("city"), ("City")}, {("state"), ("State")}, {("zip"), ("Postal code")}, {("phone"), ("Phone")}, {("url"), ("URL")}, {("date"), ("Date")}, {((const char *)((void *)0)), ((const char *)((void *)0))}};

void jabber_register_parse(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *query)
{
  PurpleAccount *account = purple_connection_get_account((js -> gc));
  PurpleRequestFields *fields;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  xmlnode *x;
  xmlnode *y;
  xmlnode *node;
  char *instructions;
  JabberRegisterCBData *cbdata;
  gboolean registered = 0;
  int i;
  if (type != JABBER_IQ_RESULT) 
    return ;
  if ((js -> registration) != 0) {
/* get rid of the login thingy */
    purple_connection_set_state((js -> gc),PURPLE_CONNECTED);
  }
  if (xmlnode_get_child(query,"registered") != 0) {
    registered = (!0);
    if ((js -> registration) != 0) {
      purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Already Registered"))),((const char *)(dgettext("pidgin","Already Registered"))),0,0,0);
      if ((account -> registration_cb) != 0) 
        ( *(account -> registration_cb))(account,0,(account -> registration_cb_user_data));
      jabber_connection_schedule_close(js);
      return ;
    }
  }
  if ((x = xmlnode_get_child_with_namespace(query,"x","jabber:x:data")) != 0) {
    jabber_x_data_request(js,x,jabber_register_x_data_cb,(g_strdup(from)));
    return ;
  }
  else if ((x = xmlnode_get_child_with_namespace(query,"x","jabber:x:oob")) != 0) {
    xmlnode *url;
    if ((url = xmlnode_get_child(x,"url")) != 0) {
      char *href;
      if ((href = xmlnode_get_data(url)) != 0) {
        purple_notify_uri(0,href);
        g_free(href);
        if ((js -> registration) != 0) {
          ( *(js -> gc)).wants_to_die = (!0);
/* succeeded, but we have no login info */
          if ((account -> registration_cb) != 0) 
            ( *(account -> registration_cb))(account,(!0),(account -> registration_cb_user_data));
          jabber_connection_schedule_close(js);
        }
        return ;
      }
    }
  }
/* as a last resort, use the old jabber:iq:register syntax */
  fields = purple_request_fields_new();
  group = purple_request_field_group_new(0);
  purple_request_fields_add_group(fields,group);
  if ((node = xmlnode_get_child(query,"username")) != 0) {
    char *data = xmlnode_get_data(node);
    if ((js -> registration) != 0) 
      field = purple_request_field_string_new("username",((const char *)(dgettext("pidgin","Username"))),(((data != 0)?data : ( *(js -> user)).node)),0);
    else 
      field = purple_request_field_string_new("username",((const char *)(dgettext("pidgin","Username"))),data,0);
    purple_request_field_group_add_field(group,field);
    g_free(data);
  }
  if ((node = xmlnode_get_child(query,"password")) != 0) {
    if ((js -> registration) != 0) 
      field = purple_request_field_string_new("password",((const char *)(dgettext("pidgin","Password"))),purple_connection_get_password((js -> gc)),0);
    else {
      char *data = xmlnode_get_data(node);
      field = purple_request_field_string_new("password",((const char *)(dgettext("pidgin","Password"))),data,0);
      g_free(data);
    }
    purple_request_field_string_set_masked(field,(!0));
    purple_request_field_group_add_field(group,field);
  }
  if ((node = xmlnode_get_child(query,"name")) != 0) {
    if ((js -> registration) != 0) 
      field = purple_request_field_string_new("name",((const char *)(dgettext("pidgin","Name"))),purple_account_get_alias(( *(js -> gc)).account),0);
    else {
      char *data = xmlnode_get_data(node);
      field = purple_request_field_string_new("name",((const char *)(dgettext("pidgin","Name"))),data,0);
      g_free(data);
    }
    purple_request_field_group_add_field(group,field);
  }
  for (i = 0; registration_fields[i].name != ((const char *)((void *)0)); ++i) {
    if ((node = xmlnode_get_child(query,registration_fields[i].name)) != 0) {
      char *data = xmlnode_get_data(node);
      field = purple_request_field_string_new(registration_fields[i].name,((const char *)(dgettext("pidgin",registration_fields[i].label))),data,0);
      purple_request_field_group_add_field(group,field);
      g_free(data);
    }
  }
  if (registered != 0) {
    field = purple_request_field_bool_new("unregister",((const char *)(dgettext("pidgin","Unregister"))),0);
    purple_request_field_group_add_field(group,field);
  }
  if ((y = xmlnode_get_child(query,"instructions")) != 0) 
    instructions = xmlnode_get_data(y);
  else if (registered != 0) 
    instructions = g_strdup(((const char *)(dgettext("pidgin","Please fill out the information below to change your account registration."))));
  else 
    instructions = g_strdup(((const char *)(dgettext("pidgin","Please fill out the information below to register your new account."))));
  cbdata = ((JabberRegisterCBData *)(g_malloc0_n(1,(sizeof(JabberRegisterCBData )))));
  cbdata -> js = js;
  cbdata -> who = g_strdup(from);
  if ((js -> registration) != 0) 
    purple_request_fields((js -> gc),((const char *)(dgettext("pidgin","Register New XMPP Account"))),((const char *)(dgettext("pidgin","Register New XMPP Account"))),instructions,fields,((const char *)(dgettext("pidgin","Register"))),((GCallback )jabber_register_cb),((const char *)(dgettext("pidgin","Cancel"))),((GCallback )jabber_register_cancel_cb),purple_connection_get_account((js -> gc)),0,0,cbdata);
  else {
    char *title;
    do {
      if (from != ((const char *)((void *)0))) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"from != NULL");
        return ;
      };
    }while (0);
    title = ((registered != 0)?g_strdup_printf(((const char *)(dgettext("pidgin","Change Account Registration at %s"))),from) : g_strdup_printf(((const char *)(dgettext("pidgin","Register New Account at %s"))),from));
    purple_request_fields((js -> gc),title,title,instructions,fields,((registered != 0)?((const char *)(dgettext("pidgin","Change Registration"))) : ((const char *)(dgettext("pidgin","Register")))),((GCallback )jabber_register_cb),((const char *)(dgettext("pidgin","Cancel"))),((GCallback )jabber_register_cancel_cb),purple_connection_get_account((js -> gc)),0,0,cbdata);
    g_free(title);
  }
  g_free(instructions);
}

void jabber_register_start(JabberStream *js)
{
  JabberIq *iq;
  iq = jabber_iq_new_query(js,JABBER_IQ_GET,"jabber:iq:register");
  jabber_iq_send(iq);
}

void jabber_register_gateway(JabberStream *js,const char *gateway)
{
  JabberIq *iq;
  iq = jabber_iq_new_query(js,JABBER_IQ_GET,"jabber:iq:register");
  xmlnode_set_attrib((iq -> node),"to",gateway);
  jabber_iq_send(iq);
}

void jabber_register_account(PurpleAccount *account)
{
  JabberStream *js;
  js = jabber_stream_new(account);
  if (js == ((JabberStream *)((void *)0))) 
    return ;
  js -> registration = (!0);
  jabber_stream_connect(js);
}

static void jabber_unregister_account_iq_cb(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  PurpleAccount *account = purple_connection_get_account((js -> gc));
  if (type == JABBER_IQ_ERROR) {
    char *msg = jabber_parse_error(js,packet,0);
    purple_notify_message((js -> gc),PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Error unregistering account"))),((const char *)(dgettext("pidgin","Error unregistering account"))),msg,0,0);
    g_free(msg);
    if ((js -> unregistration_cb) != 0) 
      ( *(js -> unregistration_cb))(account,0,(js -> unregistration_user_data));
  }
  else {
    purple_notify_message((js -> gc),PURPLE_NOTIFY_MSG_INFO,((const char *)(dgettext("pidgin","Account successfully unregistered"))),((const char *)(dgettext("pidgin","Account successfully unregistered"))),0,0,0);
    if ((js -> unregistration_cb) != 0) 
      ( *(js -> unregistration_cb))(account,(!0),(js -> unregistration_user_data));
  }
}

static void jabber_unregister_account_cb(JabberStream *js)
{
  JabberIq *iq;
  xmlnode *query;
  do {
    if ((js -> unregistration) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"js->unregistration");
      return ;
    };
  }while (0);
  iq = jabber_iq_new_query(js,JABBER_IQ_SET,"jabber:iq:register");
  query = xmlnode_get_child_with_namespace((iq -> node),"query","jabber:iq:register");
  xmlnode_new_child(query,"remove");
  xmlnode_set_attrib((iq -> node),"to",( *(js -> user)).domain);
  jabber_iq_set_callback(iq,jabber_unregister_account_iq_cb,0);
  jabber_iq_send(iq);
}

void jabber_unregister_account(PurpleAccount *account,PurpleAccountUnregistrationCb cb,void *user_data)
{
  PurpleConnection *gc = purple_account_get_connection(account);
  JabberStream *js;
  if ((gc -> state) != PURPLE_CONNECTED) {
    if ((gc -> state) != PURPLE_CONNECTING) 
      jabber_login(account);
    js = (gc -> proto_data);
    js -> unregistration = (!0);
    js -> unregistration_cb = cb;
    js -> unregistration_user_data = user_data;
    return ;
  }
  js = (gc -> proto_data);
  if ((js -> unregistration) != 0) {
    purple_debug_error("jabber","Unregistration in process; ignoring duplicate request.\n");
    return ;
  }
  js -> unregistration = (!0);
  js -> unregistration_cb = cb;
  js -> unregistration_user_data = user_data;
  jabber_unregister_account_cb(js);
}
/* TODO: As Will pointed out in IRC, after being notified by the core to
 * shutdown, we should async. wait for the server to send us the stream
 * termination before destorying everything. That seems like it would require
 * changing the semantics of prpl->close(), so it's a good idea for 3.0.0.
 */

void jabber_close(PurpleConnection *gc)
{
  JabberStream *js = (purple_connection_get_protocol_data(gc));
/* Close all of the open Jingle sessions on this stream */
  jingle_terminate_sessions(js);
  if ((js -> bosh) != 0) 
    jabber_bosh_connection_close((js -> bosh));
  else if ((((js -> gsc) != 0) && (( *(js -> gsc)).fd > 0)) || ((js -> fd) > 0)) 
    jabber_send_raw(js,"</stream:stream>",-1);
  if ((js -> srv_query_data) != 0) 
    purple_srv_cancel((js -> srv_query_data));
  if ((js -> gsc) != 0) {
    purple_ssl_close((js -> gsc));
  }
  else if ((js -> fd) > 0) {
    if (( *(js -> gc)).inpa != 0) 
      purple_input_remove(( *(js -> gc)).inpa);
    close((js -> fd));
  }
  if ((js -> bosh) != 0) 
    jabber_bosh_connection_destroy((js -> bosh));
  jabber_buddy_remove_all_pending_buddy_info_requests(js);
  jabber_parser_free(js);
  if ((js -> iq_callbacks) != 0) 
    g_hash_table_destroy((js -> iq_callbacks));
  if ((js -> buddies) != 0) 
    g_hash_table_destroy((js -> buddies));
  if ((js -> chats) != 0) 
    g_hash_table_destroy((js -> chats));
  while((js -> chat_servers) != 0){
    g_free(( *(js -> chat_servers)).data);
    js -> chat_servers = g_list_delete_link((js -> chat_servers),(js -> chat_servers));
  }
  while((js -> user_directories) != 0){
    g_free(( *(js -> user_directories)).data);
    js -> user_directories = g_list_delete_link((js -> user_directories),(js -> user_directories));
  }
  while((js -> bs_proxies) != 0){
    JabberBytestreamsStreamhost *sh = ( *(js -> bs_proxies)).data;
    g_free((sh -> jid));
    g_free((sh -> host));
    g_free((sh -> zeroconf));
    g_free(sh);
    js -> bs_proxies = g_list_delete_link((js -> bs_proxies),(js -> bs_proxies));
  }
  while((js -> url_datas) != 0){
    purple_util_fetch_url_cancel(( *(js -> url_datas)).data);
    js -> url_datas = g_slist_delete_link((js -> url_datas),(js -> url_datas));
  }
  g_free((js -> stream_id));
  if ((js -> user) != 0) 
    jabber_id_free((js -> user));
  g_free((js -> initial_avatar_hash));
  g_free((js -> avatar_hash));
  g_free((js -> caps_hash));
  if ((js -> write_buffer) != 0) 
    purple_circ_buffer_destroy((js -> write_buffer));
  if ((js -> writeh) != 0U) 
    purple_input_remove((js -> writeh));
  if (((js -> auth_mech) != 0) && (( *(js -> auth_mech)).dispose != 0)) 
    ( *( *(js -> auth_mech)).dispose)(js);
#ifdef HAVE_CYRUS_SASL
/* Note: _not_ g_free.  See auth_cyrus.c:jabber_sasl_cb_secret */
#endif
  g_free((js -> serverFQDN));
  while((js -> commands) != 0){
    JabberAdHocCommands *cmd = ( *(js -> commands)).data;
    g_free((cmd -> jid));
    g_free((cmd -> node));
    g_free((cmd -> name));
    g_free(cmd);
    js -> commands = g_list_delete_link((js -> commands),(js -> commands));
  }
  g_free((js -> server_name));
  g_free((js -> certificate_CN));
  g_free((js -> gmail_last_time));
  g_free((js -> gmail_last_tid));
  g_free((js -> old_msg));
  g_free((js -> old_avatarhash));
  g_free((js -> old_artist));
  g_free((js -> old_title));
  g_free((js -> old_source));
  g_free((js -> old_uri));
  g_free((js -> old_track));
  if ((js -> vcard_timer) != 0) 
    purple_timeout_remove((js -> vcard_timer));
  if ((js -> keepalive_timeout) != 0) 
    purple_timeout_remove((js -> keepalive_timeout));
  if ((js -> inactivity_timer) != 0) 
    purple_timeout_remove((js -> inactivity_timer));
  g_free((js -> srv_rec));
  js -> srv_rec = ((PurpleSrvResponse *)((void *)0));
  g_free((js -> stun_ip));
  js -> stun_ip = ((gchar *)((void *)0));
/* cancel DNS query for STUN, if one is ongoing */
  if ((js -> stun_query) != 0) {
    purple_dnsquery_destroy((js -> stun_query));
    js -> stun_query = ((PurpleDnsQueryData *)((void *)0));
  }
/* remove Google relay-related stuff */
  g_free((js -> google_relay_token));
  g_free((js -> google_relay_host));
  if ((js -> google_relay_requests) != 0) {
    while((js -> google_relay_requests) != 0){
      PurpleUtilFetchUrlData *url_data = (PurpleUtilFetchUrlData *)( *(js -> google_relay_requests)).data;
      purple_util_fetch_url_cancel(url_data);
      g_free(url_data);
      js -> google_relay_requests = g_list_delete_link((js -> google_relay_requests),(js -> google_relay_requests));
    }
  }
  g_free(js);
  gc -> proto_data = ((void *)((void *)0));
}

void jabber_stream_set_state(JabberStream *js,JabberStreamState state)
{
#define JABBER_CONNECT_STEPS ((js->gsc || js->state == JABBER_STREAM_INITIALIZING_ENCRYPTION) ? 9 : 5)
  js -> state = state;
  switch(state){
    case JABBER_STREAM_OFFLINE:
{
      break; 
    }
    case JABBER_STREAM_CONNECTING:
{
      purple_connection_update_progress((js -> gc),((const char *)(dgettext("pidgin","Connecting"))),1,((((js -> gsc) != 0) || ((js -> state) == JABBER_STREAM_INITIALIZING_ENCRYPTION))?9 : 5));
      break; 
    }
    case JABBER_STREAM_INITIALIZING:
{
      purple_connection_update_progress((js -> gc),((const char *)(dgettext("pidgin","Initializing Stream"))),(((js -> gsc) != 0)?5 : 2),((((js -> gsc) != 0) || ((js -> state) == JABBER_STREAM_INITIALIZING_ENCRYPTION))?9 : 5));
      jabber_stream_init(js);
      break; 
    }
    case JABBER_STREAM_INITIALIZING_ENCRYPTION:
{
      purple_connection_update_progress((js -> gc),((const char *)(dgettext("pidgin","Initializing SSL/TLS"))),6,((((js -> gsc) != 0) || ((js -> state) == JABBER_STREAM_INITIALIZING_ENCRYPTION))?9 : 5));
      break; 
    }
    case JABBER_STREAM_AUTHENTICATING:
{
      purple_connection_update_progress((js -> gc),((const char *)(dgettext("pidgin","Authenticating"))),(((js -> gsc) != 0)?7 : 3),((((js -> gsc) != 0) || ((js -> state) == JABBER_STREAM_INITIALIZING_ENCRYPTION))?9 : 5));
      break; 
    }
    case JABBER_STREAM_POST_AUTH:
{
      purple_connection_update_progress((js -> gc),((const char *)(dgettext("pidgin","Re-initializing Stream"))),(((js -> gsc) != 0)?8 : 4),((((js -> gsc) != 0) || ((js -> state) == JABBER_STREAM_INITIALIZING_ENCRYPTION))?9 : 5));
      break; 
    }
    case JABBER_STREAM_CONNECTED:
{
/* Send initial presence */
      jabber_presence_send(js,(!0));
/* Start up the inactivity timer */
      jabber_stream_restart_inactivity_timer(js);
      purple_connection_set_state((js -> gc),PURPLE_CONNECTED);
      break; 
    }
  }
#undef JABBER_CONNECT_STEPS
}

char *jabber_get_next_id(JabberStream *js)
{
  return g_strdup_printf("purple%x",js -> next_id++);
}

void jabber_idle_set(PurpleConnection *gc,int idle)
{
  JabberStream *js = (gc -> proto_data);
  js -> idle = ((idle != 0)?(time(0) - idle) : idle);
/* send out an updated prescence */
  purple_debug_info("jabber","sending updated presence for idle\n");
  jabber_presence_send(js,0);
}

void jabber_blocklist_parse_push(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *child)
{
  JabberIq *result;
  xmlnode *item;
  PurpleAccount *account;
  gboolean is_block;
  if (!(jabber_is_own_account(js,from) != 0)) {
    xmlnode *error;
    xmlnode *x;
    result = jabber_iq_new(js,JABBER_IQ_ERROR);
    xmlnode_set_attrib((result -> node),"id",id);
    if (from != 0) 
      xmlnode_set_attrib((result -> node),"to",from);
    error = xmlnode_new_child((result -> node),"error");
    xmlnode_set_attrib(error,"type","cancel");
    x = xmlnode_new_child(error,"not-allowed");
    xmlnode_set_namespace(x,"urn:ietf:params:xml:ns:xmpp-stanzas");
    jabber_iq_send(result);
    return ;
  }
  account = purple_connection_get_account((js -> gc));
  is_block = g_str_equal((child -> name),"block");
  item = xmlnode_get_child(child,"item");
  if (!(is_block != 0) && (item == ((xmlnode *)((void *)0)))) {
/* Unblock everyone */
    purple_debug_info("jabber","Received unblock push. Unblocking everyone.\n");
    while((account -> deny) != ((GSList *)((void *)0))){
      purple_privacy_deny_remove(account,( *(account -> deny)).data,(!0));
    }
  }
  else if (item == ((xmlnode *)((void *)0))) {
/* An empty <block/> is bogus */
    xmlnode *error;
    xmlnode *x;
    result = jabber_iq_new(js,JABBER_IQ_ERROR);
    xmlnode_set_attrib((result -> node),"id",id);
    error = xmlnode_new_child((result -> node),"error");
    xmlnode_set_attrib(error,"type","modify");
    x = xmlnode_new_child(error,"bad-request");
    xmlnode_set_namespace(x,"urn:ietf:params:xml:ns:xmpp-stanzas");
    jabber_iq_send(result);
    return ;
  }
  else {
    for (; item != 0; item = xmlnode_get_next_twin(item)) {{
        const char *jid = xmlnode_get_attrib(item,"jid");
        if ((jid == ((const char *)((void *)0))) || (( *jid) == 0)) 
          continue; 
        if (is_block != 0) 
          purple_privacy_deny_add(account,jid,(!0));
        else 
          purple_privacy_deny_remove(account,jid,(!0));
      }
    }
  }
  result = jabber_iq_new(js,JABBER_IQ_RESULT);
  xmlnode_set_attrib((result -> node),"id",id);
  jabber_iq_send(result);
}

static void jabber_blocklist_parse(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  xmlnode *blocklist;
  xmlnode *item;
  PurpleAccount *account;
  blocklist = xmlnode_get_child_with_namespace(packet,"blocklist","urn:xmpp:blocking");
  account = purple_connection_get_account((js -> gc));
  if ((type == JABBER_IQ_ERROR) || (blocklist == ((xmlnode *)((void *)0)))) 
    return ;
/* This is the only privacy method supported by XEP-0191 */
  if ((account -> perm_deny) != PURPLE_PRIVACY_DENY_USERS) 
    account -> perm_deny = PURPLE_PRIVACY_DENY_USERS;
/*
	 * TODO: When account->deny is something more than a hash table, this can
	 * be re-written to find the set intersection and difference.
	 */
  while((account -> deny) != 0)
    purple_privacy_deny_remove(account,( *(account -> deny)).data,(!0));
  item = xmlnode_get_child(blocklist,"item");
  while(item != ((xmlnode *)((void *)0))){
    const char *jid = xmlnode_get_attrib(item,"jid");
    purple_privacy_deny_add(account,jid,(!0));
    item = xmlnode_get_next_twin(item);
  }
}

void jabber_request_block_list(JabberStream *js)
{
  JabberIq *iq;
  xmlnode *blocklist;
  iq = jabber_iq_new(js,JABBER_IQ_GET);
  blocklist = xmlnode_new_child((iq -> node),"blocklist");
  xmlnode_set_namespace(blocklist,"urn:xmpp:blocking");
  jabber_iq_set_callback(iq,jabber_blocklist_parse,0);
  jabber_iq_send(iq);
}

void jabber_add_deny(PurpleConnection *gc,const char *who)
{
  JabberStream *js;
  JabberIq *iq;
  xmlnode *block;
  xmlnode *item;
  do {
    if ((who != ((const char *)((void *)0))) && (( *who) != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"who != NULL && *who != \'\\0\'");
      return ;
    };
  }while (0);
  js = (purple_connection_get_protocol_data(gc));
  if (js == ((JabberStream *)((void *)0))) 
    return ;
  if (((js -> server_caps) & JABBER_CAP_GOOGLE_ROSTER) != 0) {
    jabber_google_roster_add_deny(js,who);
    return ;
  }
  if (!(((js -> server_caps) & JABBER_CAP_BLOCKING) != 0)) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Server doesn\'t support blocking"))),((const char *)(dgettext("pidgin","Server doesn\'t support blocking"))),0,0,0);
    return ;
  }
  iq = jabber_iq_new(js,JABBER_IQ_SET);
  block = xmlnode_new_child((iq -> node),"block");
  xmlnode_set_namespace(block,"urn:xmpp:blocking");
  item = xmlnode_new_child(block,"item");
  xmlnode_set_attrib(item,"jid",who);
  jabber_iq_send(iq);
}

void jabber_rem_deny(PurpleConnection *gc,const char *who)
{
  JabberStream *js;
  JabberIq *iq;
  xmlnode *unblock;
  xmlnode *item;
  do {
    if ((who != ((const char *)((void *)0))) && (( *who) != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"who != NULL && *who != \'\\0\'");
      return ;
    };
  }while (0);
  js = (purple_connection_get_protocol_data(gc));
  if (js == ((JabberStream *)((void *)0))) 
    return ;
  if (((js -> server_caps) & JABBER_CAP_GOOGLE_ROSTER) != 0) {
    jabber_google_roster_rem_deny(js,who);
    return ;
  }
  if (!(((js -> server_caps) & JABBER_CAP_BLOCKING) != 0)) 
    return ;
  iq = jabber_iq_new(js,JABBER_IQ_SET);
  unblock = xmlnode_new_child((iq -> node),"unblock");
  xmlnode_set_namespace(unblock,"urn:xmpp:blocking");
  item = xmlnode_new_child(unblock,"item");
  xmlnode_set_attrib(item,"jid",who);
  jabber_iq_send(iq);
}

void jabber_add_feature(const char *namespace,JabberFeatureEnabled cb)
{
  JabberFeature *feat;
  do {
    if (namespace != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"namespace != NULL");
      return ;
    };
  }while (0);
  feat = ((JabberFeature *)(g_malloc0_n(1,(sizeof(JabberFeature )))));
  feat -> namespace = g_strdup(namespace);
  feat -> is_enabled = cb;
/* try to remove just in case it already exists in the list */
  jabber_remove_feature(namespace);
  jabber_features = g_list_append(jabber_features,feat);
}

void jabber_remove_feature(const char *namespace)
{
  GList *feature;
{
    for (feature = jabber_features; feature != 0; feature = (feature -> next)) {
      JabberFeature *feat = (JabberFeature *)(feature -> data);
      if (!(strcmp((feat -> namespace),namespace) != 0)) {
        g_free((feat -> namespace));
        g_free((feature -> data));
        jabber_features = g_list_delete_link(jabber_features,feature);
        break; 
      }
    }
  }
}

static void jabber_features_destroy()
{
  while(jabber_features != 0){
    JabberFeature *feature = (jabber_features -> data);
    g_free((feature -> namespace));
    g_free(feature);
    jabber_features = g_list_delete_link(jabber_features,jabber_features);
  }
}

gint jabber_identity_compare(gconstpointer a,gconstpointer b)
{
  const JabberIdentity *ac;
  const JabberIdentity *bc;
  gint cat_cmp;
  gint typ_cmp;
  ac = a;
  bc = b;
  if ((cat_cmp = strcmp((ac -> category),(bc -> category))) == 0) {
    if ((typ_cmp = strcmp((ac -> type),(bc -> type))) == 0) {
      if (!((ac -> lang) != 0) && !((bc -> lang) != 0)) {
        return 0;
      }
      else if (((ac -> lang) != 0) && !((bc -> lang) != 0)) {
        return 1;
      }
      else if (!((ac -> lang) != 0) && ((bc -> lang) != 0)) {
        return (-1);
      }
      else {
        return strcmp((ac -> lang),(bc -> lang));
      }
    }
    else {
      return typ_cmp;
    }
  }
  else {
    return cat_cmp;
  }
}

void jabber_add_identity(const gchar *category,const gchar *type,const gchar *lang,const gchar *name)
{
  GList *identity;
  JabberIdentity *ident;
/* both required according to XEP-0030 */
  do {
    if (category != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"category != NULL");
      return ;
    };
  }while (0);
  do {
    if (type != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type != NULL");
      return ;
    };
  }while (0);
/* Check if this identity is already there... */
  for (identity = jabber_identities; identity != 0; identity = (identity -> next)) {
    JabberIdentity *id = (identity -> data);
    if (((g_str_equal((id -> category),category) != 0) && (g_str_equal((id -> type),type) != 0)) && (purple_strequal((id -> lang),lang) != 0)) 
      return ;
  }
  ident = ((JabberIdentity *)(g_malloc0_n(1,(sizeof(JabberIdentity )))));
  ident -> category = g_strdup(category);
  ident -> type = g_strdup(type);
  ident -> lang = g_strdup(lang);
  ident -> name = g_strdup(name);
  jabber_identities = g_list_insert_sorted(jabber_identities,ident,jabber_identity_compare);
}

static void jabber_identities_destroy()
{
  while(jabber_identities != 0){
    JabberIdentity *id = (jabber_identities -> data);
    g_free((id -> category));
    g_free((id -> type));
    g_free((id -> lang));
    g_free((id -> name));
    g_free(id);
    jabber_identities = g_list_delete_link(jabber_identities,jabber_identities);
  }
}

gboolean jabber_stream_is_ssl(JabberStream *js)
{
  return (((js -> bosh) != 0) && (jabber_bosh_connection_is_ssl((js -> bosh)) != 0)) || (!((js -> bosh) != 0) && ((js -> gsc) != 0));
}

static gboolean inactivity_cb(gpointer data)
{
  JabberStream *js = data;
/* We want whatever is sent to set this.  It's okay because
	 * the eventloop unsets it via the return FALSE.
	 */
  js -> inactivity_timer = 0;
  if ((js -> bosh) != 0) 
    jabber_bosh_connection_send_keepalive((js -> bosh));
  else 
    jabber_send_raw(js,"\t",1);
  return 0;
}

void jabber_stream_restart_inactivity_timer(JabberStream *js)
{
  if ((js -> inactivity_timer) != 0) {
    purple_timeout_remove((js -> inactivity_timer));
    js -> inactivity_timer = 0;
  }
  do {
    if ((js -> max_inactivity) > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"js->max_inactivity > 0");
      return ;
    };
  }while (0);
  js -> inactivity_timer = purple_timeout_add_seconds((js -> max_inactivity),inactivity_cb,js);
}

const char *jabber_list_icon(PurpleAccount *a,PurpleBuddy *b)
{
  return "jabber";
}

const char *jabber_list_emblem(PurpleBuddy *b)
{
  JabberStream *js;
  JabberBuddy *jb = (JabberBuddy *)((void *)0);
  PurpleConnection *gc = purple_account_get_connection((purple_buddy_get_account(b)));
  if (!(gc != 0)) 
    return 0;
  js = (gc -> proto_data);
  if (js != 0) 
    jb = jabber_buddy_find(js,purple_buddy_get_name(b),0);
  if (!(((b != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(b))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(b))) != 0))) {
    if ((jb != 0) && ((((jb -> subscription) & JABBER_SUB_PENDING) != 0U) || !(((jb -> subscription) & JABBER_SUB_TO) != 0U))) 
      return "not-authorized";
  }
  if (jb != 0) {
    JabberBuddyResource *jbr = jabber_buddy_find_resource(jb,0);
    if (jbr != 0) {
      const gchar *client_type = jabber_resource_get_identity_category_type(jbr,"client");
      if (client_type != 0) {
        if (strcmp(client_type,"phone") == 0) {
          return "mobile";
        }
        else if (strcmp(client_type,"web") == 0) {
          return "external";
        }
        else if (strcmp(client_type,"handheld") == 0) {
          return "hiptop";
        }
        else if (strcmp(client_type,"bot") == 0) {
          return "bot";
        }
/* the default value "pc" falls through and has no emblem */
      }
    }
  }
  return 0;
}

char *jabber_status_text(PurpleBuddy *b)
{
  char *ret = (char *)((void *)0);
  JabberBuddy *jb = (JabberBuddy *)((void *)0);
  PurpleAccount *account = purple_buddy_get_account(b);
  PurpleConnection *gc = purple_account_get_connection(account);
  if ((gc != 0) && ((gc -> proto_data) != 0)) 
    jb = jabber_buddy_find((gc -> proto_data),purple_buddy_get_name(b),0);
  if (((jb != 0) && !(((b != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(b))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(b))) != 0))) && ((((jb -> subscription) & JABBER_SUB_PENDING) != 0U) || !(((jb -> subscription) & JABBER_SUB_TO) != 0U))) {
    ret = g_strdup(((const char *)(dgettext("pidgin","Not Authorized"))));
  }
  else if (((jb != 0) && !(((b != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(b))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(b))) != 0))) && ((jb -> error_msg) != 0)) {
    ret = g_strdup((jb -> error_msg));
  }
  else {
    PurplePresence *presence = purple_buddy_get_presence(b);
    PurpleStatus *status = purple_presence_get_active_status(presence);
    const char *message;
    if ((message = purple_status_get_attr_string(status,"message")) != 0) {
      ret = g_markup_escape_text(message,(-1));
    }
    else if (purple_presence_is_status_primitive_active(presence,PURPLE_STATUS_TUNE) != 0) {
      PurpleStatus *status = purple_presence_get_status(presence,"tune");
      const char *title = purple_status_get_attr_string(status,"tune_title");
      const char *artist = purple_status_get_attr_string(status,"tune_artist");
      const char *album = purple_status_get_attr_string(status,"tune_album");
      ret = purple_util_format_song_info(title,artist,album,0);
    }
  }
  return ret;
}

static void jabber_tooltip_add_resource_text(JabberBuddyResource *jbr,PurpleNotifyUserInfo *user_info,gboolean multiple_resources)
{
  char *text = (char *)((void *)0);
  char *res = (char *)((void *)0);
  char *label;
  char *value;
  const char *state;
  if ((jbr -> status) != 0) {
    text = g_markup_escape_text((jbr -> status),(-1));
  }
  if ((jbr -> name) != 0) 
    res = g_strdup_printf(" (%s)",(jbr -> name));
  state = jabber_buddy_state_get_name((jbr -> state));
  if ((text != ((char *)((void *)0))) && !(purple_utf8_strcasecmp(state,text) != 0)) {
    g_free(text);
    text = ((char *)((void *)0));
  }
  label = g_strdup_printf("%s%s",((const char *)(dgettext("pidgin","Status"))),((res != 0)?res : ""));
  value = g_strdup_printf("%s%s%s",state,((text != 0)?": " : ""),((text != 0)?text : ""));
  purple_notify_user_info_add_pair(user_info,label,value);
  g_free(label);
  g_free(value);
  g_free(text);
/* if the resource is idle, show that */
/* only show it if there is more than one resource available for
	the buddy, since the "general" idleness will be shown anyway,
	this way we can see see the idleness of lower-priority resources */
  if (((jbr -> idle) != 0L) && (multiple_resources != 0)) {
    gchar *idle_str = purple_str_seconds_to_string((time(0) - (jbr -> idle)));
    label = g_strdup_printf("%s%s",((const char *)(dgettext("pidgin","Idle"))),((res != 0)?res : ""));
    purple_notify_user_info_add_pair(user_info,label,idle_str);
    g_free(idle_str);
    g_free(label);
  }
  g_free(res);
}

void jabber_tooltip_text(PurpleBuddy *b,PurpleNotifyUserInfo *user_info,gboolean full)
{
  JabberBuddy *jb;
  PurpleAccount *account;
  PurpleConnection *gc;
  do {
    if (b != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"b != NULL");
      return ;
    };
  }while (0);
  account = purple_buddy_get_account(b);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  gc = purple_account_get_connection(account);
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  do {
    if ((gc -> proto_data) != ((void *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc->proto_data != NULL");
      return ;
    };
  }while (0);
  jb = jabber_buddy_find((gc -> proto_data),purple_buddy_get_name(b),0);
  if (jb != 0) {
    JabberBuddyResource *jbr = (JabberBuddyResource *)((void *)0);
    PurplePresence *presence = purple_buddy_get_presence(b);
    const char *sub;
    GList *l;
    const char *mood;
    gboolean multiple_resources = (((jb -> resources) != 0) && (((((jb -> resources) != 0)?( *((GList *)(jb -> resources))).next : ((struct _GList *)((void *)0)))) != 0));
    JabberBuddyResource *top_jbr = jabber_buddy_find_resource(jb,0);
/* resource-specific info for the top resource */
    if (top_jbr != 0) {
      jabber_tooltip_add_resource_text(top_jbr,user_info,multiple_resources);
    }
    for (l = (jb -> resources); l != 0; l = (l -> next)) {
      jbr = (l -> data);
/* the remaining resources */
      if (jbr != top_jbr) {
        jabber_tooltip_add_resource_text(jbr,user_info,multiple_resources);
      }
    }
    if (full != 0) {
      PurpleStatus *status;
      status = purple_presence_get_status(presence,"mood");
      mood = purple_status_get_attr_string(status,"mood");
      if ((mood != 0) && (( *mood) != 0)) {
        const char *moodtext;
/* find the mood */
        PurpleMood *moods = jabber_get_moods(account);
        const char *description = (const char *)((void *)0);
{
          for (; (moods -> mood) != 0; moods++) {
            if (purple_strequal((moods -> mood),mood) != 0) {
              description = (moods -> description);
              break; 
            }
          }
        }
        moodtext = purple_status_get_attr_string(status,"moodtext");
        if ((moodtext != 0) && (( *moodtext) != 0)) {
          char *moodplustext = g_strdup_printf("%s (%s)",((description != 0)?((const char *)(dgettext("pidgin",description))) : mood),moodtext);
          purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Mood"))),moodplustext);
          g_free(moodplustext);
        }
        else 
          purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Mood"))),((description != 0)?((const char *)(dgettext("pidgin",description))) : mood));
      }
      if (purple_presence_is_status_primitive_active(presence,PURPLE_STATUS_TUNE) != 0) {
        PurpleStatus *tune = purple_presence_get_status(presence,"tune");
        const char *title = purple_status_get_attr_string(tune,"tune_title");
        const char *artist = purple_status_get_attr_string(tune,"tune_artist");
        const char *album = purple_status_get_attr_string(tune,"tune_album");
        char *playing = purple_util_format_song_info(title,artist,album,0);
        if (playing != 0) {
          purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Now Listening"))),playing);
          g_free(playing);
        }
      }
      if (((jb -> subscription) & JABBER_SUB_FROM) != 0U) {
        if (((jb -> subscription) & JABBER_SUB_TO) != 0U) 
          sub = ((const char *)(dgettext("pidgin","Both")));
        else if (((jb -> subscription) & JABBER_SUB_PENDING) != 0U) 
          sub = ((const char *)(dgettext("pidgin","From (To pending)")));
        else 
          sub = ((const char *)(dgettext("pidgin","From")));
      }
      else {
        if (((jb -> subscription) & JABBER_SUB_TO) != 0U) 
          sub = ((const char *)(dgettext("pidgin","To")));
        else if (((jb -> subscription) & JABBER_SUB_PENDING) != 0U) 
          sub = ((const char *)(dgettext("pidgin","None (To pending)")));
        else 
          sub = ((const char *)(dgettext("pidgin","None")));
      }
      purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Subscription"))),sub);
    }
    if (!(((b != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(b))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(b))) != 0)) && ((jb -> error_msg) != 0)) {
      purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Error"))),(jb -> error_msg));
    }
  }
}

GList *jabber_status_types(PurpleAccount *account)
{
  PurpleStatusType *type;
  GList *types = (GList *)((void *)0);
  PurpleValue *priority_value;
  PurpleValue *buzz_enabled;
  priority_value = purple_value_new(PURPLE_TYPE_INT);
  purple_value_set_int(priority_value,1);
  buzz_enabled = purple_value_new(PURPLE_TYPE_BOOLEAN);
  purple_value_set_boolean(buzz_enabled,(!0));
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_AVAILABLE,jabber_buddy_state_get_status_id(JABBER_BUDDY_STATE_ONLINE),0,(!0),(!0),0,"priority",((const char *)(dgettext("pidgin","Priority"))),priority_value,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),"mood",((const char *)(dgettext("pidgin","Mood"))),purple_value_new(PURPLE_TYPE_STRING),"moodtext",((const char *)(dgettext("pidgin","Mood Text"))),purple_value_new(PURPLE_TYPE_STRING),"nick",((const char *)(dgettext("pidgin","Nickname"))),purple_value_new(PURPLE_TYPE_STRING),"buzz",((const char *)(dgettext("pidgin","Allow Buzz"))),buzz_enabled,((void *)((void *)0)));
  types = g_list_prepend(types,type);
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_MOOD,"mood",0,(!0),(!0),(!0),"mood",((const char *)(dgettext("pidgin","Mood Name"))),purple_value_new(PURPLE_TYPE_STRING),"moodtext",((const char *)(dgettext("pidgin","Mood Comment"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  types = g_list_prepend(types,type);
  priority_value = purple_value_new(PURPLE_TYPE_INT);
  purple_value_set_int(priority_value,1);
  buzz_enabled = purple_value_new(PURPLE_TYPE_BOOLEAN);
  purple_value_set_boolean(buzz_enabled,(!0));
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_AVAILABLE,jabber_buddy_state_get_status_id(JABBER_BUDDY_STATE_CHAT),((const char *)(dgettext("pidgin","Chatty"))),(!0),(!0),0,"priority",((const char *)(dgettext("pidgin","Priority"))),priority_value,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),"mood",((const char *)(dgettext("pidgin","Mood"))),purple_value_new(PURPLE_TYPE_STRING),"moodtext",((const char *)(dgettext("pidgin","Mood Text"))),purple_value_new(PURPLE_TYPE_STRING),"nick",((const char *)(dgettext("pidgin","Nickname"))),purple_value_new(PURPLE_TYPE_STRING),"buzz",((const char *)(dgettext("pidgin","Allow Buzz"))),buzz_enabled,((void *)((void *)0)));
  types = g_list_prepend(types,type);
  priority_value = purple_value_new(PURPLE_TYPE_INT);
  purple_value_set_int(priority_value,0);
  buzz_enabled = purple_value_new(PURPLE_TYPE_BOOLEAN);
  purple_value_set_boolean(buzz_enabled,(!0));
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_AWAY,jabber_buddy_state_get_status_id(JABBER_BUDDY_STATE_AWAY),0,(!0),(!0),0,"priority",((const char *)(dgettext("pidgin","Priority"))),priority_value,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),"mood",((const char *)(dgettext("pidgin","Mood"))),purple_value_new(PURPLE_TYPE_STRING),"moodtext",((const char *)(dgettext("pidgin","Mood Text"))),purple_value_new(PURPLE_TYPE_STRING),"nick",((const char *)(dgettext("pidgin","Nickname"))),purple_value_new(PURPLE_TYPE_STRING),"buzz",((const char *)(dgettext("pidgin","Allow Buzz"))),buzz_enabled,((void *)((void *)0)));
  types = g_list_prepend(types,type);
  priority_value = purple_value_new(PURPLE_TYPE_INT);
  purple_value_set_int(priority_value,0);
  buzz_enabled = purple_value_new(PURPLE_TYPE_BOOLEAN);
  purple_value_set_boolean(buzz_enabled,(!0));
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_EXTENDED_AWAY,jabber_buddy_state_get_status_id(JABBER_BUDDY_STATE_XA),0,(!0),(!0),0,"priority",((const char *)(dgettext("pidgin","Priority"))),priority_value,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),"mood",((const char *)(dgettext("pidgin","Mood"))),purple_value_new(PURPLE_TYPE_STRING),"moodtext",((const char *)(dgettext("pidgin","Mood Text"))),purple_value_new(PURPLE_TYPE_STRING),"nick",((const char *)(dgettext("pidgin","Nickname"))),purple_value_new(PURPLE_TYPE_STRING),"buzz",((const char *)(dgettext("pidgin","Allow Buzz"))),buzz_enabled,((void *)((void *)0)));
  types = g_list_prepend(types,type);
  priority_value = purple_value_new(PURPLE_TYPE_INT);
  purple_value_set_int(priority_value,0);
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_UNAVAILABLE,jabber_buddy_state_get_status_id(JABBER_BUDDY_STATE_DND),((const char *)(dgettext("pidgin","Do Not Disturb"))),(!0),(!0),0,"priority",((const char *)(dgettext("pidgin","Priority"))),priority_value,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),"mood",((const char *)(dgettext("pidgin","Mood"))),purple_value_new(PURPLE_TYPE_STRING),"moodtext",((const char *)(dgettext("pidgin","Mood Text"))),purple_value_new(PURPLE_TYPE_STRING),"nick",((const char *)(dgettext("pidgin","Nickname"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  types = g_list_prepend(types,type);
/*
	if(js->protocol_version == JABBER_PROTO_0_9)
		"Invisible"
	*/
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_OFFLINE,jabber_buddy_state_get_status_id(JABBER_BUDDY_STATE_UNAVAILABLE),0,(!0),(!0),0,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  types = g_list_prepend(types,type);
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_TUNE,"tune",0,0,(!0),(!0),"tune_artist",((const char *)(dgettext("pidgin","Tune Artist"))),purple_value_new(PURPLE_TYPE_STRING),"tune_title",((const char *)(dgettext("pidgin","Tune Title"))),purple_value_new(PURPLE_TYPE_STRING),"tune_album",((const char *)(dgettext("pidgin","Tune Album"))),purple_value_new(PURPLE_TYPE_STRING),"tune_genre",((const char *)(dgettext("pidgin","Tune Genre"))),purple_value_new(PURPLE_TYPE_STRING),"tune_comment",((const char *)(dgettext("pidgin","Tune Comment"))),purple_value_new(PURPLE_TYPE_STRING),"tune_track",((const char *)(dgettext("pidgin","Tune Track"))),purple_value_new(PURPLE_TYPE_STRING),"tune_time",((const char *)(dgettext("pidgin","Tune Time"))),purple_value_new(PURPLE_TYPE_INT),"tune_year",((const char *)(dgettext("pidgin","Tune Year"))),purple_value_new(PURPLE_TYPE_INT),"tune_url",((const char *)(dgettext("pidgin","Tune URL"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  types = g_list_prepend(types,type);
  return g_list_reverse(types);
}

static void jabber_password_change_result_cb(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  if (type == JABBER_IQ_RESULT) {
    purple_notify_message((js -> gc),PURPLE_NOTIFY_MSG_INFO,((const char *)(dgettext("pidgin","Password Changed"))),((const char *)(dgettext("pidgin","Password Changed"))),((const char *)(dgettext("pidgin","Your password has been changed."))),0,0);
    purple_account_set_password(( *(js -> gc)).account,((char *)data));
  }
  else {
    char *msg = jabber_parse_error(js,packet,0);
    purple_notify_message((js -> gc),PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Error changing password"))),((const char *)(dgettext("pidgin","Error changing password"))),msg,0,0);
    g_free(msg);
  }
  g_free(data);
}

static void jabber_password_change_cb(JabberStream *js,PurpleRequestFields *fields)
{
  const char *p1;
  const char *p2;
  JabberIq *iq;
  xmlnode *query;
  xmlnode *y;
  p1 = purple_request_fields_get_string(fields,"password1");
  p2 = purple_request_fields_get_string(fields,"password2");
  if (strcmp(p1,p2) != 0) {
    purple_notify_message((js -> gc),PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","New passwords do not match."))),0,0,0);
    return ;
  }
  iq = jabber_iq_new_query(js,JABBER_IQ_SET,"jabber:iq:register");
  xmlnode_set_attrib((iq -> node),"to",( *(js -> user)).domain);
  query = xmlnode_get_child((iq -> node),"query");
  y = xmlnode_new_child(query,"username");
  xmlnode_insert_data(y,( *(js -> user)).node,(-1));
  y = xmlnode_new_child(query,"password");
  xmlnode_insert_data(y,p1,(-1));
  jabber_iq_set_callback(iq,jabber_password_change_result_cb,(g_strdup(p1)));
  jabber_iq_send(iq);
}

static void jabber_password_change(PurplePluginAction *action)
{
  PurpleConnection *gc = (PurpleConnection *)(action -> context);
  JabberStream *js = (gc -> proto_data);
  PurpleRequestFields *fields;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  fields = purple_request_fields_new();
  group = purple_request_field_group_new(0);
  purple_request_fields_add_group(fields,group);
  field = purple_request_field_string_new("password1",((const char *)(dgettext("pidgin","Password"))),"",0);
  purple_request_field_string_set_masked(field,(!0));
  purple_request_field_set_required(field,(!0));
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_string_new("password2",((const char *)(dgettext("pidgin","Password (again)"))),"",0);
  purple_request_field_string_set_masked(field,(!0));
  purple_request_field_set_required(field,(!0));
  purple_request_field_group_add_field(group,field);
  purple_request_fields((js -> gc),((const char *)(dgettext("pidgin","Change XMPP Password"))),((const char *)(dgettext("pidgin","Change XMPP Password"))),((const char *)(dgettext("pidgin","Please enter your new password"))),fields,((const char *)(dgettext("pidgin","OK"))),((GCallback )jabber_password_change_cb),((const char *)(dgettext("pidgin","Cancel"))),0,purple_connection_get_account(gc),0,0,js);
}

GList *jabber_actions(PurplePlugin *plugin,gpointer context)
{
  PurpleConnection *gc = (PurpleConnection *)context;
  JabberStream *js = (gc -> proto_data);
  GList *m = (GList *)((void *)0);
  PurplePluginAction *act;
  act = purple_plugin_action_new(((const char *)(dgettext("pidgin","Set User Info..."))),jabber_setup_set_info);
  m = g_list_append(m,act);
/* if (js->protocol_options & CHANGE_PASSWORD) { */
  act = purple_plugin_action_new(((const char *)(dgettext("pidgin","Change Password..."))),jabber_password_change);
  m = g_list_append(m,act);
/* } */
  act = purple_plugin_action_new(((const char *)(dgettext("pidgin","Search for Users..."))),jabber_user_search_begin);
  m = g_list_append(m,act);
  purple_debug_info("jabber","jabber_actions: have pep: %s\n",(((js -> pep) != 0)?"YES" : "NO"));
  if ((js -> pep) != 0) 
    jabber_pep_init_actions(&m);
  if ((js -> commands) != 0) 
    jabber_adhoc_init_server_commands(js,&m);
  return m;
}

PurpleChat *jabber_find_blist_chat(PurpleAccount *account,const char *name)
{
  PurpleBlistNode *gnode;
  PurpleBlistNode *cnode;
  JabberID *jid;
  if (!((jid = jabber_id_new(name)) != 0)) 
    return 0;
  for (gnode = purple_blist_get_root(); gnode != 0; gnode = purple_blist_node_get_sibling_next(gnode)) {
    for (cnode = purple_blist_node_get_first_child(gnode); cnode != 0; cnode = purple_blist_node_get_sibling_next(cnode)) {{
        PurpleChat *chat = (PurpleChat *)cnode;
        const char *room;
        const char *server;
        GHashTable *components;
        if (!((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CHAT_NODE)) 
          continue; 
        if (purple_chat_get_account(chat) != account) 
          continue; 
        components = purple_chat_get_components(chat);
        if (!((room = (g_hash_table_lookup(components,"room"))) != 0)) 
          continue; 
        if (!((server = (g_hash_table_lookup(components,"server"))) != 0)) 
          continue; 
/* FIXME: Collate is wrong in a few cases here; this should be prepped */
        if (((((jid -> node) != 0) && ((jid -> domain) != 0)) && !(g_utf8_collate(room,(jid -> node)) != 0)) && !(g_utf8_collate(server,(jid -> domain)) != 0)) {
          jabber_id_free(jid);
          return chat;
        }
      }
    }
  }
  jabber_id_free(jid);
  return 0;
}

void jabber_convo_closed(PurpleConnection *gc,const char *who)
{
  JabberStream *js = (gc -> proto_data);
  JabberID *jid;
  JabberBuddy *jb;
  JabberBuddyResource *jbr;
  if (!((jid = jabber_id_new(who)) != 0)) 
    return ;
  if (((jb = jabber_buddy_find(js,who,(!0))) != 0) && ((jbr = jabber_buddy_find_resource(jb,(jid -> resource))) != 0)) {
    if ((jbr -> thread_id) != 0) {
      g_free((jbr -> thread_id));
      jbr -> thread_id = ((char *)((void *)0));
    }
  }
  jabber_id_free(jid);
}

char *jabber_parse_error(JabberStream *js,xmlnode *packet,PurpleConnectionError *reason)
{
  xmlnode *error;
  const char *code = (const char *)((void *)0);
  const char *text = (const char *)((void *)0);
  const char *xmlns = xmlnode_get_namespace(packet);
  char *cdata = (char *)((void *)0);
#define SET_REASON(x) \
	if(reason != NULL) { *reason = x; }
  if ((error = xmlnode_get_child(packet,"error")) != 0) {
    xmlnode *t = xmlnode_get_child_with_namespace(error,"text","urn:ietf:params:xml:ns:xmpp-stanzas");
    if (t != 0) 
      cdata = xmlnode_get_data(t);
#if 0
#endif
    code = xmlnode_get_attrib(error,"code");
/* Stanza errors */
    if (xmlnode_get_child(error,"bad-request") != 0) {
      text = ((const char *)(dgettext("pidgin","Bad Request")));
    }
    else if (xmlnode_get_child(error,"conflict") != 0) {
      if (reason != ((PurpleConnectionError *)((void *)0))) {
         *reason = PURPLE_CONNECTION_ERROR_NAME_IN_USE;
      };
      text = ((const char *)(dgettext("pidgin","Conflict")));
    }
    else if (xmlnode_get_child(error,"feature-not-implemented") != 0) {
      text = ((const char *)(dgettext("pidgin","Feature Not Implemented")));
    }
    else if (xmlnode_get_child(error,"forbidden") != 0) {
      text = ((const char *)(dgettext("pidgin","Forbidden")));
    }
    else if (xmlnode_get_child(error,"gone") != 0) {
      text = ((const char *)(dgettext("pidgin","Gone")));
    }
    else if (xmlnode_get_child(error,"internal-server-error") != 0) {
      text = ((const char *)(dgettext("pidgin","Internal Server Error")));
    }
    else if (xmlnode_get_child(error,"item-not-found") != 0) {
      text = ((const char *)(dgettext("pidgin","Item Not Found")));
    }
    else if (xmlnode_get_child(error,"jid-malformed") != 0) {
      text = ((const char *)(dgettext("pidgin","Malformed XMPP ID")));
    }
    else if (xmlnode_get_child(error,"not-acceptable") != 0) {
      text = ((const char *)(dgettext("pidgin","Not Acceptable")));
    }
    else if (xmlnode_get_child(error,"not-allowed") != 0) {
      text = ((const char *)(dgettext("pidgin","Not Allowed")));
    }
    else if (xmlnode_get_child(error,"not-authorized") != 0) {
      text = ((const char *)(dgettext("pidgin","Not Authorized")));
    }
    else if (xmlnode_get_child(error,"payment-required") != 0) {
      text = ((const char *)(dgettext("pidgin","Payment Required")));
    }
    else if (xmlnode_get_child(error,"recipient-unavailable") != 0) {
      text = ((const char *)(dgettext("pidgin","Recipient Unavailable")));
    }
    else if (xmlnode_get_child(error,"redirect") != 0) {
/* XXX */
    }
    else if (xmlnode_get_child(error,"registration-required") != 0) {
      text = ((const char *)(dgettext("pidgin","Registration Required")));
    }
    else if (xmlnode_get_child(error,"remote-server-not-found") != 0) {
      text = ((const char *)(dgettext("pidgin","Remote Server Not Found")));
    }
    else if (xmlnode_get_child(error,"remote-server-timeout") != 0) {
      text = ((const char *)(dgettext("pidgin","Remote Server Timeout")));
    }
    else if (xmlnode_get_child(error,"resource-constraint") != 0) {
      text = ((const char *)(dgettext("pidgin","Server Overloaded")));
    }
    else if (xmlnode_get_child(error,"service-unavailable") != 0) {
      text = ((const char *)(dgettext("pidgin","Service Unavailable")));
    }
    else if (xmlnode_get_child(error,"subscription-required") != 0) {
      text = ((const char *)(dgettext("pidgin","Subscription Required")));
    }
    else if (xmlnode_get_child(error,"unexpected-request") != 0) {
      text = ((const char *)(dgettext("pidgin","Unexpected Request")));
    }
    else if (xmlnode_get_child(error,"undefined-condition") != 0) {
      text = ((const char *)(dgettext("pidgin","Unknown Error")));
    }
  }
  else if ((xmlns != 0) && !(strcmp(xmlns,"urn:ietf:params:xml:ns:xmpp-sasl") != 0)) {
/* Most common reason can be the default */
    if (reason != ((PurpleConnectionError *)((void *)0))) {
       *reason = PURPLE_CONNECTION_ERROR_NETWORK_ERROR;
    };
    if (xmlnode_get_child(packet,"aborted") != 0) {
      text = ((const char *)(dgettext("pidgin","Authorization Aborted")));
    }
    else if (xmlnode_get_child(packet,"incorrect-encoding") != 0) {
      text = ((const char *)(dgettext("pidgin","Incorrect encoding in authorization")));
    }
    else if (xmlnode_get_child(packet,"invalid-authzid") != 0) {
      text = ((const char *)(dgettext("pidgin","Invalid authzid")));
    }
    else if (xmlnode_get_child(packet,"invalid-mechanism") != 0) {
      text = ((const char *)(dgettext("pidgin","Invalid Authorization Mechanism")));
    }
    else if (xmlnode_get_child(packet,"mechanism-too-weak") != 0) {
      if (reason != ((PurpleConnectionError *)((void *)0))) {
         *reason = PURPLE_CONNECTION_ERROR_AUTHENTICATION_IMPOSSIBLE;
      };
      text = ((const char *)(dgettext("pidgin","Authorization mechanism too weak")));
    }
    else if (xmlnode_get_child(packet,"not-authorized") != 0) {
      if (reason != ((PurpleConnectionError *)((void *)0))) {
         *reason = PURPLE_CONNECTION_ERROR_AUTHENTICATION_FAILED;
      };
/* Clear the pasword if it isn't being saved */
      if (!(purple_account_get_remember_password(( *(js -> gc)).account) != 0)) 
        purple_account_set_password(( *(js -> gc)).account,0);
      text = ((const char *)(dgettext("pidgin","Not Authorized")));
    }
    else if (xmlnode_get_child(packet,"temporary-auth-failure") != 0) {
      text = ((const char *)(dgettext("pidgin","Temporary Authentication Failure")));
    }
    else {
      if (reason != ((PurpleConnectionError *)((void *)0))) {
         *reason = PURPLE_CONNECTION_ERROR_AUTHENTICATION_FAILED;
      };
      text = ((const char *)(dgettext("pidgin","Authentication Failure")));
    }
  }
  else if (!(strcmp((packet -> name),"stream:error") != 0) || ((!(strcmp((packet -> name),"error") != 0) && (xmlns != 0)) && !(strcmp(xmlns,"http://etherx.jabber.org/streams") != 0))) {
/* Most common reason as default: */
    if (reason != ((PurpleConnectionError *)((void *)0))) {
       *reason = PURPLE_CONNECTION_ERROR_NETWORK_ERROR;
    };
    if (xmlnode_get_child(packet,"bad-format") != 0) {
      text = ((const char *)(dgettext("pidgin","Bad Format")));
    }
    else if (xmlnode_get_child(packet,"bad-namespace-prefix") != 0) {
      text = ((const char *)(dgettext("pidgin","Bad Namespace Prefix")));
    }
    else if (xmlnode_get_child(packet,"conflict") != 0) {
      if (reason != ((PurpleConnectionError *)((void *)0))) {
         *reason = PURPLE_CONNECTION_ERROR_NAME_IN_USE;
      };
      text = ((const char *)(dgettext("pidgin","Resource Conflict")));
    }
    else if (xmlnode_get_child(packet,"connection-timeout") != 0) {
      text = ((const char *)(dgettext("pidgin","Connection Timeout")));
    }
    else if (xmlnode_get_child(packet,"host-gone") != 0) {
      text = ((const char *)(dgettext("pidgin","Host Gone")));
    }
    else if (xmlnode_get_child(packet,"host-unknown") != 0) {
      text = ((const char *)(dgettext("pidgin","Host Unknown")));
    }
    else if (xmlnode_get_child(packet,"improper-addressing") != 0) {
      text = ((const char *)(dgettext("pidgin","Improper Addressing")));
    }
    else if (xmlnode_get_child(packet,"internal-server-error") != 0) {
      text = ((const char *)(dgettext("pidgin","Internal Server Error")));
    }
    else if (xmlnode_get_child(packet,"invalid-id") != 0) {
      text = ((const char *)(dgettext("pidgin","Invalid ID")));
    }
    else if (xmlnode_get_child(packet,"invalid-namespace") != 0) {
      text = ((const char *)(dgettext("pidgin","Invalid Namespace")));
    }
    else if (xmlnode_get_child(packet,"invalid-xml") != 0) {
      text = ((const char *)(dgettext("pidgin","Invalid XML")));
    }
    else if (xmlnode_get_child(packet,"nonmatching-hosts") != 0) {
      text = ((const char *)(dgettext("pidgin","Non-matching Hosts")));
    }
    else if (xmlnode_get_child(packet,"not-authorized") != 0) {
      text = ((const char *)(dgettext("pidgin","Not Authorized")));
    }
    else if (xmlnode_get_child(packet,"policy-violation") != 0) {
      text = ((const char *)(dgettext("pidgin","Policy Violation")));
    }
    else if (xmlnode_get_child(packet,"remote-connection-failed") != 0) {
      text = ((const char *)(dgettext("pidgin","Remote Connection Failed")));
    }
    else if (xmlnode_get_child(packet,"resource-constraint") != 0) {
      text = ((const char *)(dgettext("pidgin","Resource Constraint")));
    }
    else if (xmlnode_get_child(packet,"restricted-xml") != 0) {
      text = ((const char *)(dgettext("pidgin","Restricted XML")));
    }
    else if (xmlnode_get_child(packet,"see-other-host") != 0) {
      text = ((const char *)(dgettext("pidgin","See Other Host")));
    }
    else if (xmlnode_get_child(packet,"system-shutdown") != 0) {
      text = ((const char *)(dgettext("pidgin","System Shutdown")));
    }
    else if (xmlnode_get_child(packet,"undefined-condition") != 0) {
      text = ((const char *)(dgettext("pidgin","Undefined Condition")));
    }
    else if (xmlnode_get_child(packet,"unsupported-encoding") != 0) {
      text = ((const char *)(dgettext("pidgin","Unsupported Encoding")));
    }
    else if (xmlnode_get_child(packet,"unsupported-stanza-type") != 0) {
      text = ((const char *)(dgettext("pidgin","Unsupported Stanza Type")));
    }
    else if (xmlnode_get_child(packet,"unsupported-version") != 0) {
      text = ((const char *)(dgettext("pidgin","Unsupported Version")));
    }
    else if (xmlnode_get_child(packet,"xml-not-well-formed") != 0) {
      text = ((const char *)(dgettext("pidgin","XML Not Well Formed")));
    }
    else {
      text = ((const char *)(dgettext("pidgin","Stream Error")));
    }
  }
#undef SET_REASON
  if ((text != 0) || (cdata != 0)) {
    char *ret = g_strdup_printf("%s%s%s",((code != 0)?code : ""),((code != 0)?": " : ""),((text != 0)?text : cdata));
    g_free(cdata);
    return ret;
  }
  else {
    return 0;
  }
}

static PurpleCmdRet jabber_cmd_chat_config(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  JabberChat *chat = jabber_chat_find_by_conv(conv);
  if (!(chat != 0)) 
    return PURPLE_CMD_RET_FAILED;
  jabber_chat_request_room_configure(chat);
  return PURPLE_CMD_RET_OK;
}

static PurpleCmdRet jabber_cmd_chat_register(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  JabberChat *chat = jabber_chat_find_by_conv(conv);
  if (!(chat != 0)) 
    return PURPLE_CMD_RET_FAILED;
  jabber_chat_register(chat);
  return PURPLE_CMD_RET_OK;
}

static PurpleCmdRet jabber_cmd_chat_topic(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  JabberChat *chat = jabber_chat_find_by_conv(conv);
  if (!(chat != 0)) 
    return PURPLE_CMD_RET_FAILED;
  if (((args != 0) && (args[0] != 0)) && (( *args[0]) != 0)) 
    jabber_chat_change_topic(chat,args[0]);
  else {
    const char *cur = purple_conv_chat_get_topic((purple_conversation_get_chat_data(conv)));
    char *buf;
    char *tmp;
    char *tmp2;
    if (cur != 0) {
      tmp = g_markup_escape_text(cur,(-1));
      tmp2 = purple_markup_linkify(tmp);
      buf = g_strdup_printf(((const char *)(dgettext("pidgin","current topic is: %s"))),tmp2);
      g_free(tmp);
      g_free(tmp2);
    }
    else 
      buf = g_strdup(((const char *)(dgettext("pidgin","No topic is set"))));
    purple_conv_chat_write(purple_conversation_get_chat_data(conv),"",buf,(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LOG),time(0));
    g_free(buf);
  }
  return PURPLE_CMD_RET_OK;
}

static PurpleCmdRet jabber_cmd_chat_nick(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  JabberChat *chat = jabber_chat_find_by_conv(conv);
  if ((!(chat != 0) || !(args != 0)) || !(args[0] != 0)) 
    return PURPLE_CMD_RET_FAILED;
  if (!(jabber_resourceprep_validate(args[0]) != 0)) {
     *error = g_strdup(((const char *)(dgettext("pidgin","Invalid nickname"))));
    return PURPLE_CMD_RET_FAILED;
  }
  if (jabber_chat_change_nick(chat,args[0]) != 0) 
    return PURPLE_CMD_RET_OK;
  else 
    return PURPLE_CMD_RET_FAILED;
}

static PurpleCmdRet jabber_cmd_chat_part(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  JabberChat *chat = jabber_chat_find_by_conv(conv);
  if (!(chat != 0)) 
    return PURPLE_CMD_RET_FAILED;
  jabber_chat_part(chat,((args != 0)?args[0] : ((char *)((void *)0))));
  return PURPLE_CMD_RET_OK;
}

static PurpleCmdRet jabber_cmd_chat_ban(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  JabberChat *chat = jabber_chat_find_by_conv(conv);
  if ((!(chat != 0) || !(args != 0)) || !(args[0] != 0)) 
    return PURPLE_CMD_RET_FAILED;
  if (!(jabber_chat_ban_user(chat,args[0],args[1]) != 0)) {
     *error = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to ban user %s"))),args[0]);
    return PURPLE_CMD_RET_FAILED;
  }
  return PURPLE_CMD_RET_OK;
}

static PurpleCmdRet jabber_cmd_chat_affiliate(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  JabberChat *chat = jabber_chat_find_by_conv(conv);
  if ((!(chat != 0) || !(args != 0)) || !(args[0] != 0)) 
    return PURPLE_CMD_RET_FAILED;
  if (((((strcmp(args[0],"owner") != 0) && (strcmp(args[0],"admin") != 0)) && (strcmp(args[0],"member") != 0)) && (strcmp(args[0],"outcast") != 0)) && (strcmp(args[0],"none") != 0)) {
     *error = g_strdup_printf(((const char *)(dgettext("pidgin","Unknown affiliation: \"%s\""))),args[0]);
    return PURPLE_CMD_RET_FAILED;
  }
  if (args[1] != 0) {
    int i;
    char **nicks = g_strsplit(args[1]," ",(-1));
    for (i = 0; nicks[i] != 0; ++i) 
      if (!(jabber_chat_affiliate_user(chat,nicks[i],args[0]) != 0)) {
         *error = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to affiliate user %s as \"%s\""))),nicks[i],args[0]);
        g_strfreev(nicks);
        return PURPLE_CMD_RET_FAILED;
      }
    g_strfreev(nicks);
  }
  else {
    jabber_chat_affiliation_list(chat,args[0]);
  }
  return PURPLE_CMD_RET_OK;
}

static PurpleCmdRet jabber_cmd_chat_role(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  JabberChat *chat = jabber_chat_find_by_conv(conv);
  if ((!(chat != 0) || !(args != 0)) || !(args[0] != 0)) 
    return PURPLE_CMD_RET_FAILED;
  if ((((strcmp(args[0],"moderator") != 0) && (strcmp(args[0],"participant") != 0)) && (strcmp(args[0],"visitor") != 0)) && (strcmp(args[0],"none") != 0)) {
     *error = g_strdup_printf(((const char *)(dgettext("pidgin","Unknown role: \"%s\""))),args[0]);
    return PURPLE_CMD_RET_FAILED;
  }
  if (args[1] != 0) {
    int i;
    char **nicks = g_strsplit(args[1]," ",(-1));
    for (i = 0; nicks[i] != 0; i++) 
      if (!(jabber_chat_role_user(chat,nicks[i],args[0],0) != 0)) {
         *error = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to set role \"%s\" for user: %s"))),args[0],nicks[i]);
        g_strfreev(nicks);
        return PURPLE_CMD_RET_FAILED;
      }
    g_strfreev(nicks);
  }
  else {
    jabber_chat_role_list(chat,args[0]);
  }
  return PURPLE_CMD_RET_OK;
}

static PurpleCmdRet jabber_cmd_chat_invite(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  if (!(args != 0) || !(args[0] != 0)) 
    return PURPLE_CMD_RET_FAILED;
  jabber_chat_invite(purple_conversation_get_gc(conv),purple_conv_chat_get_id((purple_conversation_get_chat_data(conv))),((args[1] != 0)?args[1] : ""),args[0]);
  return PURPLE_CMD_RET_OK;
}

static PurpleCmdRet jabber_cmd_chat_join(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  JabberChat *chat = jabber_chat_find_by_conv(conv);
  GHashTable *components;
  JabberID *jid = (JabberID *)((void *)0);
  const char *room = (const char *)((void *)0);
  const char *server = (const char *)((void *)0);
  const char *handle = (const char *)((void *)0);
  if ((!(chat != 0) || !(args != 0)) || !(args[0] != 0)) 
    return PURPLE_CMD_RET_FAILED;
  components = g_hash_table_new_full(g_str_hash,g_str_equal,0,0);
  if (strchr(args[0],64) != 0) 
    jid = jabber_id_new(args[0]);
  if (jid != 0) {
    room = (jid -> node);
    server = (jid -> domain);
    handle = ((((jid -> resource) != 0)?(jid -> resource) : (chat -> handle)));
  }
  else {
/* If jabber_id_new failed, the user may have just passed in
		 * a room name.  For backward compatibility, handle that here.
		 */
    if (strchr(args[0],64) != 0) {
       *error = g_strdup(((const char *)(dgettext("pidgin","Invalid XMPP ID"))));
      return PURPLE_CMD_RET_FAILED;
    }
    room = args[0];
    server = (chat -> server);
    handle = (chat -> handle);
  }
  g_hash_table_insert(components,"room",((gpointer )room));
  g_hash_table_insert(components,"server",((gpointer )server));
  g_hash_table_insert(components,"handle",((gpointer )handle));
  if (args[1] != 0) 
    g_hash_table_insert(components,"password",args[1]);
  jabber_chat_join(purple_conversation_get_gc(conv),components);
  g_hash_table_destroy(components);
  jabber_id_free(jid);
  return PURPLE_CMD_RET_OK;
}

static PurpleCmdRet jabber_cmd_chat_kick(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  JabberChat *chat = jabber_chat_find_by_conv(conv);
  if ((!(chat != 0) || !(args != 0)) || !(args[0] != 0)) 
    return PURPLE_CMD_RET_FAILED;
  if (!(jabber_chat_role_user(chat,args[0],"none",args[1]) != 0)) {
     *error = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to kick user %s"))),args[0]);
    return PURPLE_CMD_RET_FAILED;
  }
  return PURPLE_CMD_RET_OK;
}

static PurpleCmdRet jabber_cmd_chat_msg(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  JabberChat *chat = jabber_chat_find_by_conv(conv);
  char *who;
  if (!(chat != 0)) 
    return PURPLE_CMD_RET_FAILED;
  who = g_strdup_printf("%s@%s/%s",(chat -> room),(chat -> server),args[0]);
  jabber_message_send_im(purple_conversation_get_gc(conv),who,args[1],0);
  g_free(who);
  return PURPLE_CMD_RET_OK;
}

static PurpleCmdRet jabber_cmd_ping(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  PurpleAccount *account;
  PurpleConnection *pc;
  if (!(args != 0) || !(args[0] != 0)) 
    return PURPLE_CMD_RET_FAILED;
  account = purple_conversation_get_account(conv);
  pc = purple_account_get_connection(account);
  if (!(jabber_ping_jid((purple_connection_get_protocol_data(pc)),args[0]) != 0)) {
     *error = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to ping user %s"))),args[0]);
    return PURPLE_CMD_RET_FAILED;
  }
  return PURPLE_CMD_RET_OK;
}

static gboolean _jabber_send_buzz(JabberStream *js,const char *username,char **error)
{
  JabberBuddy *jb;
  JabberBuddyResource *jbr;
  PurpleConnection *gc = (js -> gc);
  PurpleBuddy *buddy = purple_find_buddy(purple_connection_get_account(gc),username);
  const gchar *alias = (buddy != 0)?purple_buddy_get_contact_alias(buddy) : username;
  if (!(username != 0)) 
    return 0;
  jb = jabber_buddy_find(js,username,0);
  if (!(jb != 0)) {
     *error = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to buzz, because there is nothing known about %s."))),alias);
    return 0;
  }
  jbr = jabber_buddy_find_resource(jb,0);
  if (!(jbr != 0)) {
     *error = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to buzz, because %s might be offline."))),alias);
    return 0;
  }
  if (jabber_resource_has_capability(jbr,"urn:xmpp:attention:0") != 0) {
    xmlnode *buzz;
    xmlnode *msg = xmlnode_new("message");
    gchar *to;
    to = g_strdup_printf("%s/%s",username,(jbr -> name));
    xmlnode_set_attrib(msg,"to",to);
    g_free(to);
/* avoid offline storage */
    xmlnode_set_attrib(msg,"type","headline");
    buzz = xmlnode_new_child(msg,"attention");
    xmlnode_set_namespace(buzz,"urn:xmpp:attention:0");
    jabber_send(js,msg);
    xmlnode_free(msg);
    return (!0);
  }
  else {
     *error = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to buzz, because %s does not support it or does not wish to receive buzzes now."))),alias);
    return 0;
  }
}

static PurpleCmdRet jabber_cmd_buzz(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  JabberStream *js = ( *( *(conv -> account)).gc).proto_data;
  const gchar *who;
  gchar *description;
  PurpleBuddy *buddy;
  const char *alias;
  PurpleAttentionType *attn = purple_get_attention_type_from_code((conv -> account),0);
  if (!(args != 0) || !(args[0] != 0)) {
/* use the buddy from conversation, if it's a one-to-one conversation */
    if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
      who = purple_conversation_get_name(conv);
    }
    else {
      return PURPLE_CMD_RET_FAILED;
    }
  }
  else {
    who = args[0];
  }
  buddy = purple_find_buddy((conv -> account),who);
  if (buddy != ((PurpleBuddy *)((void *)0))) 
    alias = purple_buddy_get_contact_alias(buddy);
  else 
    alias = who;
  description = g_strdup_printf(purple_attention_type_get_outgoing_desc(attn),alias);
  purple_conversation_write(conv,0,description,(PURPLE_MESSAGE_NOTIFY | PURPLE_MESSAGE_SYSTEM),time(0));
  g_free(description);
  return ((_jabber_send_buzz(js,who,error) != 0)?PURPLE_CMD_RET_OK : PURPLE_CMD_RET_FAILED);
}

GList *jabber_attention_types(PurpleAccount *account)
{
  static GList *types = (GList *)((void *)0);
  if (!(types != 0)) {
    types = g_list_append(types,(purple_attention_type_new("Buzz",((const char *)(dgettext("pidgin","Buzz"))),((const char *)(dgettext("pidgin","%s has buzzed you!"))),((const char *)(dgettext("pidgin","Buzzing %s..."))))));
  }
  return types;
}

gboolean jabber_send_attention(PurpleConnection *gc,const char *username,guint code)
{
  JabberStream *js = (gc -> proto_data);
  gchar *error = (gchar *)((void *)0);
  if (!(_jabber_send_buzz(js,username,&error) != 0)) {
    PurpleAccount *account = purple_connection_get_account(gc);
    PurpleConversation *conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_ANY,username,account);
    purple_debug_error("jabber","jabber_send_attention: jabber_cmd_buzz failed with error: %s\n",((error != 0)?error : "(NULL)"));
    if (conv != 0) {
      purple_conversation_write(conv,username,error,PURPLE_MESSAGE_ERROR,time(0));
    }
    g_free(error);
    return 0;
  }
  return (!0);
}

gboolean jabber_offline_message(const PurpleBuddy *buddy)
{
  return (!0);
}
#ifdef USE_VV

gboolean jabber_audio_enabled(JabberStream *js,const char *namespace)
{
  PurpleMediaManager *manager = purple_media_manager_get();
  PurpleMediaCaps caps = purple_media_manager_get_ui_caps(manager);
  return (caps & (PURPLE_MEDIA_CAPS_AUDIO | PURPLE_MEDIA_CAPS_AUDIO_SINGLE_DIRECTION));
}

gboolean jabber_video_enabled(JabberStream *js,const char *namespace)
{
  PurpleMediaManager *manager = purple_media_manager_get();
  PurpleMediaCaps caps = purple_media_manager_get_ui_caps(manager);
  return (caps & (PURPLE_MEDIA_CAPS_VIDEO | PURPLE_MEDIA_CAPS_VIDEO_SINGLE_DIRECTION));
}
typedef struct __unnamed_class___F0_L3255_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L247R__Pe___variable_name_unknown_scope_and_name__scope__account__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__who__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L634R_variable_name_unknown_scope_and_name__scope__type {
PurpleAccount *account;
gchar *who;
PurpleMediaSessionType type;}JabberMediaRequest;

static void jabber_media_cancel_cb(JabberMediaRequest *request,PurpleRequestFields *fields)
{
  g_free((request -> who));
  g_free(request);
}

static void jabber_media_ok_cb(JabberMediaRequest *request,PurpleRequestFields *fields)
{
  PurpleRequestField *field = purple_request_fields_get_field(fields,"resource");
  int selected_id = purple_request_field_choice_get_value(field);
  GList *labels = purple_request_field_choice_get_labels(field);
  gchar *who = g_strdup_printf("%s/%s",(request -> who),((gchar *)(g_list_nth_data(labels,selected_id))));
  jabber_initiate_media((request -> account),who,(request -> type));
  g_free(who);
  g_free((request -> who));
  g_free(request);
}
#endif

gboolean jabber_initiate_media(PurpleAccount *account,const char *who,PurpleMediaSessionType type)
{
#ifdef USE_VV
  JabberStream *js = (JabberStream *)( *purple_account_get_connection(account)).proto_data;
  JabberBuddy *jb;
  JabberBuddyResource *jbr = (JabberBuddyResource *)((void *)0);
  char *resource = (char *)((void *)0);
  if (!(js != 0)) {
    purple_debug_error("jabber","jabber_initiate_media: NULL stream\n");
    return 0;
  }
  jb = jabber_buddy_find(js,who,0);
  if ((!(jb != 0) || !((jb -> resources) != 0)) || (((resource = jabber_get_resource(who)) != ((char *)((void *)0))) && ((jbr = jabber_buddy_find_resource(jb,resource)) == ((JabberBuddyResource *)((void *)0))))) {
/* no resources online, we're trying to initiate with someone
		 * whose presence we're not subscribed to, or
		 * someone who is offline.  Let's inform the user */
    char *msg;
    if (!(jb != 0)) {
      msg = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to initiate media with %s: invalid JID"))),who);
    }
    else if ((((jb -> subscription) & JABBER_SUB_TO) != 0U) && !((jb -> resources) != 0)) {
      msg = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to initiate media with %s: user is not online"))),who);
    }
    else if (resource != 0) {
      msg = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to initiate media with %s: resource is not online"))),who);
    }
    else {
      msg = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to initiate media with %s: not subscribed to user presence"))),who);
    }
    purple_notify_message(account,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Media Initiation Failed"))),((const char *)(dgettext("pidgin","Media Initiation Failed"))),msg,0,0);
    g_free(msg);
    g_free(resource);
    return 0;
  }
  else if (jbr != ((JabberBuddyResource *)((void *)0))) {
/* they've specified a resource, no need to ask or
		 * default or anything, just do it */
    g_free(resource);
    if ((((type & PURPLE_MEDIA_AUDIO) != 0U) && !(jabber_resource_has_capability(jbr,"urn:xmpp:jingle:apps:rtp:audio") != 0)) && (jabber_resource_has_capability(jbr,"http://www.google.com/xmpp/protocol/voice/v1") != 0)) 
      return jabber_google_session_initiate(js,who,type);
    else 
      return jingle_rtp_initiate_media(js,who,type);
  }
  else if (!(( *(jb -> resources)).next != 0)) {
/* only 1 resource online (probably our most common case)
		 * so no need to ask who to initiate with */
    gchar *name;
    gboolean result;
    jbr = ( *(jb -> resources)).data;
    name = g_strdup_printf("%s/%s",who,(jbr -> name));
    result = jabber_initiate_media(account,name,type);
    g_free(name);
    return result;
  }
  else {
/* we've got multiple resources,
		 * we need to pick one to initiate with */
    GList *l;
    char *msg;
    PurpleRequestFields *fields;
    PurpleRequestField *field = purple_request_field_choice_new("resource",((const char *)(dgettext("pidgin","Resource"))),0);
    PurpleRequestFieldGroup *group;
    JabberMediaRequest *request;
    for (l = (jb -> resources); l != 0; l = (l -> next)) {
      JabberBuddyResource *ljbr = (l -> data);
      PurpleMediaCaps caps;
      gchar *name;
      name = g_strdup_printf("%s/%s",who,(ljbr -> name));
      caps = jabber_get_media_caps(account,name);
      g_free(name);
      if (((type & PURPLE_MEDIA_AUDIO) != 0U) && ((type & PURPLE_MEDIA_VIDEO) != 0U)) {
        if ((caps & PURPLE_MEDIA_CAPS_AUDIO_VIDEO) != 0U) {
          jbr = ljbr;
          purple_request_field_choice_add(field,(jbr -> name));
        }
      }
      else if (((type & PURPLE_MEDIA_AUDIO) != 0U) && ((caps & PURPLE_MEDIA_CAPS_AUDIO) != 0U)) {
        jbr = ljbr;
        purple_request_field_choice_add(field,(jbr -> name));
      }
      else if (((type & PURPLE_MEDIA_VIDEO) != 0U) && ((caps & PURPLE_MEDIA_CAPS_VIDEO) != 0U)) {
        jbr = ljbr;
        purple_request_field_choice_add(field,(jbr -> name));
      }
    }
    if (jbr == ((JabberBuddyResource *)((void *)0))) {
      purple_debug_error("jabber","No resources available\n");
      return 0;
    }
    if (g_list_length(purple_request_field_choice_get_labels(field)) <= 1) {
      gchar *name;
      gboolean result;
      purple_request_field_destroy(field);
      name = g_strdup_printf("%s/%s",who,(jbr -> name));
      result = jabber_initiate_media(account,name,type);
      g_free(name);
      return result;
    }
    msg = g_strdup_printf(((const char *)(dgettext("pidgin","Please select the resource of %s with which you would like to start a media session."))),who);
    fields = purple_request_fields_new();
    group = purple_request_field_group_new(0);
    request = ((JabberMediaRequest *)(g_malloc0_n(1,(sizeof(JabberMediaRequest )))));
    request -> account = account;
    request -> who = g_strdup(who);
    request -> type = type;
    purple_request_field_group_add_field(group,field);
    purple_request_fields_add_group(fields,group);
    purple_request_fields(account,((const char *)(dgettext("pidgin","Select a Resource"))),msg,0,fields,((const char *)(dgettext("pidgin","Initiate Media"))),((GCallback )jabber_media_ok_cb),((const char *)(dgettext("pidgin","Cancel"))),((GCallback )jabber_media_cancel_cb),account,who,0,request);
    g_free(msg);
    return (!0);
  }
#endif
  return 0;
}

PurpleMediaCaps jabber_get_media_caps(PurpleAccount *account,const char *who)
{
#ifdef USE_VV
  JabberStream *js = (JabberStream *)( *purple_account_get_connection(account)).proto_data;
  JabberBuddy *jb;
  JabberBuddyResource *jbr;
  PurpleMediaCaps total = PURPLE_MEDIA_CAPS_NONE;
  gchar *resource;
  GList *specific = (GList *)((void *)0);
  GList *l;
  if (!(js != 0)) {
    purple_debug_info("jabber","jabber_can_do_media: NULL stream\n");
    return 0;
  }
  jb = jabber_buddy_find(js,who,0);
  if (!(jb != 0) || !((jb -> resources) != 0)) {
/* no resources online, we're trying to get caps for someone
		 * whose presence we're not subscribed to, or
		 * someone who is offline. */
    return total;
  }
  else if ((resource = jabber_get_resource(who)) != ((gchar *)((void *)0))) {
/* they've specified a resource, no need to ask or
		 * default or anything, just do it */
    jbr = jabber_buddy_find_resource(jb,resource);
    g_free(resource);
    if (!(jbr != 0)) {
      purple_debug_error("jabber","jabber_get_media_caps: Can\'t find resource %s\n",who);
      return total;
    }
    l = (specific = g_list_prepend(specific,jbr));
  }
  else {
/* we've got multiple resources, combine their caps */
    l = (jb -> resources);
  }
  for (; l != 0; l = (l -> next)) {
    PurpleMediaCaps caps = PURPLE_MEDIA_CAPS_NONE;
    jbr = (l -> data);
    if (jabber_resource_has_capability(jbr,"urn:xmpp:jingle:apps:rtp:audio") != 0) 
      caps |= (PURPLE_MEDIA_CAPS_AUDIO_SINGLE_DIRECTION | PURPLE_MEDIA_CAPS_AUDIO);
    if (jabber_resource_has_capability(jbr,"urn:xmpp:jingle:apps:rtp:video") != 0) 
      caps |= (PURPLE_MEDIA_CAPS_VIDEO_SINGLE_DIRECTION | PURPLE_MEDIA_CAPS_VIDEO);
    if (((caps & PURPLE_MEDIA_CAPS_AUDIO) != 0U) && ((caps & PURPLE_MEDIA_CAPS_VIDEO) != 0U)) 
      caps |= PURPLE_MEDIA_CAPS_AUDIO_VIDEO;
    if (caps != PURPLE_MEDIA_CAPS_NONE) {
      if (!(jabber_resource_has_capability(jbr,"urn:xmpp:jingle:transports:ice-udp:1") != 0) && !(jabber_resource_has_capability(jbr,"urn:xmpp:jingle:transports:raw-udp:1") != 0)) {
        purple_debug_info("jingle-rtp","Buddy doesn\'t support the same transport types\n");
        caps = PURPLE_MEDIA_CAPS_NONE;
      }
      else 
        caps |= (PURPLE_MEDIA_CAPS_MODIFY_SESSION | PURPLE_MEDIA_CAPS_CHANGE_DIRECTION);
    }
    if (jabber_resource_has_capability(jbr,"http://www.google.com/xmpp/protocol/voice/v1") != 0) {
      caps |= PURPLE_MEDIA_CAPS_AUDIO;
      if (jabber_resource_has_capability(jbr,"http://www.google.com/xmpp/protocol/video/v1") != 0) 
        caps |= PURPLE_MEDIA_CAPS_AUDIO_VIDEO;
    }
    total |= caps;
  }
  if (specific != 0) {
    g_list_free(specific);
  }
  return total;
#else
#endif
}

gboolean jabber_can_receive_file(PurpleConnection *gc,const char *who)
{
  JabberStream *js = (gc -> proto_data);
  if (js != 0) {
    JabberBuddy *jb = jabber_buddy_find(js,who,0);
    GList *iter;
    gboolean has_resources_without_caps = 0;
/* if we didn't find a JabberBuddy, we don't have presence for this
		 buddy, let's assume they can receive files, disco should tell us
		 when actually trying */
    if (jb == ((JabberBuddy *)((void *)0))) 
      return (!0);
/* find out if there is any resources without caps */
    for (iter = (jb -> resources); iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
      JabberBuddyResource *jbr = (JabberBuddyResource *)(iter -> data);
      if (!(jabber_resource_know_capabilities(jbr) != 0)) {
        has_resources_without_caps = (!0);
      }
    }
    if (has_resources_without_caps != 0) {
/* there is at least one resource which we don't have caps for,
			 let's assume they can receive files... */
      return (!0);
    }
    else {
/* we have caps for all the resources, see if at least one has
			 right caps */
      for (iter = (jb -> resources); iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
        JabberBuddyResource *jbr = (JabberBuddyResource *)(iter -> data);
        if ((jabber_resource_has_capability(jbr,"http://jabber.org/protocol/si/profile/file-transfer") != 0) && ((jabber_resource_has_capability(jbr,"http://jabber.org/protocol/bytestreams") != 0) || (jabber_resource_has_capability(jbr,"http://jabber.org/protocol/ibb") != 0))) {
          return (!0);
        }
      }
      return 0;
    }
  }
  else {
    return (!0);
  }
}

static PurpleCmdRet jabber_cmd_mood(PurpleConversation *conv,const char *cmd,char **args,char **error,void *data)
{
  JabberStream *js = ( *( *(conv -> account)).gc).proto_data;
  if ((js -> pep) != 0) {
/* if no argument was given, unset mood */
    if (!(args != 0) || !(args[0] != 0)) {
      jabber_mood_set(js,0,0);
    }
    else if (!(args[1] != 0)) {
      jabber_mood_set(js,args[0],0);
    }
    else {
      jabber_mood_set(js,args[0],args[1]);
    }
    return PURPLE_CMD_RET_OK;
  }
  else {
/* account does not support PEP, can't set a mood */
    purple_conversation_write(conv,0,((const char *)(dgettext("pidgin","Account does not support PEP, can\'t set mood"))),PURPLE_MESSAGE_ERROR,time(0));
    return PURPLE_CMD_RET_FAILED;
  }
}

static void jabber_register_commands(PurplePlugin *plugin)
{
  GSList *commands = (GSList *)((void *)0);
  PurpleCmdId id;
  id = purple_cmd_register("config","",PURPLE_CMD_P_PRPL,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_PRPL_ONLY),"prpl-jabber",jabber_cmd_chat_config,((const char *)(dgettext("pidgin","config:  Configure a chat room."))),0);
  commands = g_slist_prepend(commands,((gpointer )((gulong )id)));
  id = purple_cmd_register("configure","",PURPLE_CMD_P_PRPL,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_PRPL_ONLY),"prpl-jabber",jabber_cmd_chat_config,((const char *)(dgettext("pidgin","configure:  Configure a chat room."))),0);
  commands = g_slist_prepend(commands,((gpointer )((gulong )id)));
  id = purple_cmd_register("nick","s",PURPLE_CMD_P_PRPL,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_PRPL_ONLY),"prpl-jabber",jabber_cmd_chat_nick,((const char *)(dgettext("pidgin","nick &lt;new nickname&gt;:  Change your nickname."))),0);
  commands = g_slist_prepend(commands,((gpointer )((gulong )id)));
  id = purple_cmd_register("part","s",PURPLE_CMD_P_PRPL,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_PRPL_ONLY | PURPLE_CMD_FLAG_ALLOW_WRONG_ARGS),"prpl-jabber",jabber_cmd_chat_part,((const char *)(dgettext("pidgin","part [message]:  Leave the room."))),0);
  commands = g_slist_prepend(commands,((gpointer )((gulong )id)));
  id = purple_cmd_register("register","",PURPLE_CMD_P_PRPL,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_PRPL_ONLY),"prpl-jabber",jabber_cmd_chat_register,((const char *)(dgettext("pidgin","register:  Register with a chat room."))),0);
  commands = g_slist_prepend(commands,((gpointer )((gulong )id)));
/* XXX: there needs to be a core /topic cmd, methinks */
  id = purple_cmd_register("topic","s",PURPLE_CMD_P_PRPL,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_PRPL_ONLY | PURPLE_CMD_FLAG_ALLOW_WRONG_ARGS),"prpl-jabber",jabber_cmd_chat_topic,((const char *)(dgettext("pidgin","topic [new topic]:  View or change the topic."))),0);
  commands = g_slist_prepend(commands,((gpointer )((gulong )id)));
  id = purple_cmd_register("ban","ws",PURPLE_CMD_P_PRPL,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_PRPL_ONLY | PURPLE_CMD_FLAG_ALLOW_WRONG_ARGS),"prpl-jabber",jabber_cmd_chat_ban,((const char *)(dgettext("pidgin","ban &lt;user&gt; [reason]:  Ban a user from the room."))),0);
  commands = g_slist_prepend(commands,((gpointer )((gulong )id)));
  id = purple_cmd_register("affiliate","ws",PURPLE_CMD_P_PRPL,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_PRPL_ONLY | PURPLE_CMD_FLAG_ALLOW_WRONG_ARGS),"prpl-jabber",jabber_cmd_chat_affiliate,((const char *)(dgettext("pidgin","affiliate &lt;owner|admin|member|outcast|none&gt; [nick1] [nick2] ...: Get the users with an affiliation or set users\' affiliation with the room."))),0);
  commands = g_slist_prepend(commands,((gpointer )((gulong )id)));
  id = purple_cmd_register("role","ws",PURPLE_CMD_P_PRPL,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_PRPL_ONLY | PURPLE_CMD_FLAG_ALLOW_WRONG_ARGS),"prpl-jabber",jabber_cmd_chat_role,((const char *)(dgettext("pidgin","role &lt;moderator|participant|visitor|none&gt; [nick1] [nick2] ...: Get the users with a role or set users\' role with the room."))),0);
  commands = g_slist_prepend(commands,((gpointer )((gulong )id)));
  id = purple_cmd_register("invite","ws",PURPLE_CMD_P_PRPL,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_PRPL_ONLY | PURPLE_CMD_FLAG_ALLOW_WRONG_ARGS),"prpl-jabber",jabber_cmd_chat_invite,((const char *)(dgettext("pidgin","invite &lt;user&gt; [message]:  Invite a user to the room."))),0);
  commands = g_slist_prepend(commands,((gpointer )((gulong )id)));
  id = purple_cmd_register("join","ws",PURPLE_CMD_P_PRPL,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_PRPL_ONLY | PURPLE_CMD_FLAG_ALLOW_WRONG_ARGS),"prpl-jabber",jabber_cmd_chat_join,((const char *)(dgettext("pidgin","join: &lt;room[@server]&gt; [password]:  Join a chat."))),0);
  commands = g_slist_prepend(commands,((gpointer )((gulong )id)));
  id = purple_cmd_register("kick","ws",PURPLE_CMD_P_PRPL,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_PRPL_ONLY | PURPLE_CMD_FLAG_ALLOW_WRONG_ARGS),"prpl-jabber",jabber_cmd_chat_kick,((const char *)(dgettext("pidgin","kick &lt;user&gt; [reason]:  Kick a user from the room."))),0);
  commands = g_slist_prepend(commands,((gpointer )((gulong )id)));
  id = purple_cmd_register("msg","ws",PURPLE_CMD_P_PRPL,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_PRPL_ONLY),"prpl-jabber",jabber_cmd_chat_msg,((const char *)(dgettext("pidgin","msg &lt;user&gt; &lt;message&gt;:  Send a private message to another user."))),0);
  commands = g_slist_prepend(commands,((gpointer )((gulong )id)));
  id = purple_cmd_register("ping","w",PURPLE_CMD_P_PRPL,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM | PURPLE_CMD_FLAG_PRPL_ONLY),"prpl-jabber",jabber_cmd_ping,((const char *)(dgettext("pidgin","ping &lt;jid&gt;:\tPing a user/component/server."))),0);
  commands = g_slist_prepend(commands,((gpointer )((gulong )id)));
  id = purple_cmd_register("buzz","w",PURPLE_CMD_P_PRPL,(PURPLE_CMD_FLAG_IM | PURPLE_CMD_FLAG_PRPL_ONLY | PURPLE_CMD_FLAG_ALLOW_WRONG_ARGS),"prpl-jabber",jabber_cmd_buzz,((const char *)(dgettext("pidgin","buzz: Buzz a user to get their attention"))),0);
  commands = g_slist_prepend(commands,((gpointer )((gulong )id)));
  id = purple_cmd_register("mood","ws",PURPLE_CMD_P_PRPL,(PURPLE_CMD_FLAG_CHAT | PURPLE_CMD_FLAG_IM | PURPLE_CMD_FLAG_PRPL_ONLY | PURPLE_CMD_FLAG_ALLOW_WRONG_ARGS),"prpl-jabber",jabber_cmd_mood,((const char *)(dgettext("pidgin","mood: Set current user mood"))),0);
  commands = g_slist_prepend(commands,((gpointer )((gulong )id)));
  g_hash_table_insert(jabber_cmds,plugin,commands);
}

static void cmds_free_func(gpointer value)
{
  GSList *commands = value;
  while(commands != 0){
    purple_cmd_unregister(((guint )((gulong )(commands -> data))));
    commands = g_slist_delete_link(commands,commands);
  }
}

static void jabber_unregister_commands(PurplePlugin *plugin)
{
  g_hash_table_remove(jabber_cmds,plugin);
}
/* IPC functions */
/**
 * IPC function for determining if a contact supports a certain feature.
 *
 * @param account   The PurpleAccount
 * @param jid       The full JID of the contact.
 * @param feature   The feature's namespace.
 *
 * @return TRUE if supports feature; else FALSE.
 */

static gboolean jabber_ipc_contact_has_feature(PurpleAccount *account,const gchar *jid,const gchar *feature)
{
  PurpleConnection *gc = purple_account_get_connection(account);
  JabberStream *js;
  JabberBuddy *jb;
  JabberBuddyResource *jbr;
  gchar *resource;
  if (!(purple_account_is_connected(account) != 0)) 
    return 0;
  js = (gc -> proto_data);
  if ((!((resource = jabber_get_resource(jid)) != 0) || !((jb = jabber_buddy_find(js,jid,0)) != 0)) || !((jbr = jabber_buddy_find_resource(jb,resource)) != 0)) {
    g_free(resource);
    return 0;
  }
  g_free(resource);
  return jabber_resource_has_capability(jbr,feature);
}

static void jabber_ipc_add_feature(const gchar *feature)
{
  if (!(feature != 0)) 
    return ;
  jabber_add_feature(feature,0);
/* send presence with new caps info for all connected accounts */
  jabber_caps_broadcast_change();
}

static void jabber_do_init()
{
  GHashTable *ui_info = purple_core_get_ui_info();
  const gchar *ui_type;
/* default client type, if unknown or
								unspecified */
  const gchar *type = "pc";
  const gchar *ui_name = (const gchar *)((void *)0);
#ifdef HAVE_CYRUS_SASL
/* We really really only want to do this once per process */
#ifdef _WIN32
#endif
#endif
/* XXX - If any other plugin wants SASL this won't be good ... */
#ifdef HAVE_CYRUS_SASL
#ifdef _WIN32
/* Suppress error popups for failing to load sasl plugins */
#endif
#ifdef _WIN32
/* Restore the original error mode */
#endif
#endif
  jabber_cmds = g_hash_table_new_full(g_direct_hash,g_direct_equal,0,cmds_free_func);
  ui_type = (((ui_info != 0)?g_hash_table_lookup(ui_info,"client_type") : ((void *)((void *)0))));
  if (ui_type != 0) {
    if ((((((strcmp(ui_type,"pc") == 0) || (strcmp(ui_type,"console") == 0)) || (strcmp(ui_type,"phone") == 0)) || (strcmp(ui_type,"handheld") == 0)) || (strcmp(ui_type,"web") == 0)) || (strcmp(ui_type,"bot") == 0)) {
      type = ui_type;
    }
  }
  if (ui_info != 0) 
    ui_name = (g_hash_table_lookup(ui_info,"name"));
  if (ui_name == ((const gchar *)((void *)0))) 
    ui_name = "pidgin";
  jabber_add_identity("client",type,0,ui_name);
/* initialize jabber_features list */
  jabber_add_feature("jabber:iq:last",0);
  jabber_add_feature("jabber:iq:oob",0);
  jabber_add_feature("urn:xmpp:time",0);
  jabber_add_feature("jabber:iq:version",0);
  jabber_add_feature("jabber:x:conference",0);
  jabber_add_feature("http://jabber.org/protocol/bytestreams",0);
  jabber_add_feature("http://jabber.org/protocol/caps",0);
  jabber_add_feature("http://jabber.org/protocol/chatstates",0);
  jabber_add_feature("http://jabber.org/protocol/disco#info",0);
  jabber_add_feature("http://jabber.org/protocol/disco#items",0);
  jabber_add_feature("http://jabber.org/protocol/ibb",0);
  jabber_add_feature("http://jabber.org/protocol/muc",0);
  jabber_add_feature("http://jabber.org/protocol/muc#user",0);
  jabber_add_feature("http://jabber.org/protocol/si",0);
  jabber_add_feature("http://jabber.org/protocol/si/profile/file-transfer",0);
  jabber_add_feature("http://jabber.org/protocol/xhtml-im",0);
  jabber_add_feature("urn:xmpp:ping",0);
/* Buzz/Attention */
  jabber_add_feature("urn:xmpp:attention:0",jabber_buzz_isenabled);
/* Bits Of Binary */
  jabber_add_feature("urn:xmpp:bob",0);
/* Jingle features! */
  jabber_add_feature("urn:xmpp:jingle:1",0);
#ifdef USE_VV
  jabber_add_feature("http://www.google.com/xmpp/protocol/session",jabber_audio_enabled);
  jabber_add_feature("http://www.google.com/xmpp/protocol/voice/v1",jabber_audio_enabled);
  jabber_add_feature("http://www.google.com/xmpp/protocol/video/v1",jabber_video_enabled);
  jabber_add_feature("http://www.google.com/xmpp/protocol/camera/v1",jabber_video_enabled);
  jabber_add_feature("urn:xmpp:jingle:apps:rtp:1",0);
  jabber_add_feature("urn:xmpp:jingle:apps:rtp:audio",jabber_audio_enabled);
  jabber_add_feature("urn:xmpp:jingle:apps:rtp:video",jabber_video_enabled);
  jabber_add_feature("urn:xmpp:jingle:transports:raw-udp:1",0);
  jabber_add_feature("urn:xmpp:jingle:transports:ice-udp:1",0);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(purple_media_manager_get())),((GType )(20 << 2))))),"ui-caps-changed",((GCallback )jabber_caps_broadcast_change),0,0,((GConnectFlags )0));
#endif
/* reverse order of unload_plugin */
  jabber_iq_init();
  jabber_presence_init();
  jabber_caps_init();
/* PEP things should be init via jabber_pep_init, not here */
  jabber_pep_init();
  jabber_data_init();
  jabber_bosh_init();
/* TODO: Implement adding and retrieving own features via IPC API */
  jabber_ibb_init();
  jabber_si_init();
  jabber_auth_init();
}

static void jabber_do_uninit()
{
/* reverse order of jabber_do_init */
  jabber_bosh_uninit();
  jabber_data_uninit();
  jabber_si_uninit();
  jabber_ibb_uninit();
/* PEP things should be uninit via jabber_pep_uninit, not here */
  jabber_pep_uninit();
  jabber_caps_uninit();
  jabber_presence_uninit();
  jabber_iq_uninit();
#ifdef USE_VV
  g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(purple_media_manager_get())),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )jabber_caps_broadcast_change),0);
#endif
  jabber_auth_uninit();
  jabber_features_destroy();
  jabber_identities_destroy();
  g_hash_table_destroy(jabber_cmds);
  jabber_cmds = ((GHashTable *)((void *)0));
}

void jabber_plugin_init(PurplePlugin *plugin)
{
  ++plugin_ref;
  if (plugin_ref == 1) 
    jabber_do_init();
  jabber_register_commands(plugin);
/* IPC functions */
  purple_plugin_ipc_register(plugin,"contact_has_feature",((PurpleCallback )jabber_ipc_contact_has_feature),purple_marshal_BOOLEAN__POINTER_POINTER_POINTER,purple_value_new(PURPLE_TYPE_BOOLEAN),3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING));
  purple_plugin_ipc_register(plugin,"add_feature",((PurpleCallback )jabber_ipc_add_feature),purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_STRING));
  purple_plugin_ipc_register(plugin,"register_namespace_watcher",((PurpleCallback )jabber_iq_signal_register),purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING));
/* node */
/* namespace */
  purple_plugin_ipc_register(plugin,"unregister_namespace_watcher",((PurpleCallback )jabber_iq_signal_unregister),purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING));
/* node */
/* namespace */
  purple_signal_register(plugin,"jabber-register-namespace-watcher",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING));
/* node */
/* namespace */
  purple_signal_register(plugin,"jabber-unregister-namespace-watcher",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING));
/* node */
/* namespace */
  purple_signal_connect(plugin,"jabber-register-namespace-watcher",plugin,((PurpleCallback )jabber_iq_signal_register),0);
  purple_signal_connect(plugin,"jabber-unregister-namespace-watcher",plugin,((PurpleCallback )jabber_iq_signal_unregister),0);
  purple_signal_register(plugin,"jabber-receiving-xmlnode",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONNECTION),purple_value_new_outgoing(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_XMLNODE));
  purple_signal_register(plugin,"jabber-sending-xmlnode",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONNECTION),purple_value_new_outgoing(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_XMLNODE));
/*
	 * Do not remove this or the plugin will fail. Completely. You have been
	 * warned!
	 */
  purple_signal_connect_priority(plugin,"jabber-sending-xmlnode",plugin,((PurpleCallback )jabber_send_signal_cb),0,9999);
  purple_signal_register(plugin,"jabber-sending-text",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONNECTION),purple_value_new_outgoing(PURPLE_TYPE_STRING));
  purple_signal_register(plugin,"jabber-receiving-message",purple_marshal_BOOLEAN__POINTER_POINTER_POINTER_POINTER_POINTER_POINTER,purple_value_new(PURPLE_TYPE_BOOLEAN),6,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONNECTION),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_XMLNODE));
/* type */
/* id */
/* from */
/* to */
  purple_signal_register(plugin,"jabber-receiving-iq",purple_marshal_BOOLEAN__POINTER_POINTER_POINTER_POINTER_POINTER,purple_value_new(PURPLE_TYPE_BOOLEAN),5,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONNECTION),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_XMLNODE));
/* type */
/* id */
/* from */
  purple_signal_register(plugin,"jabber-watched-iq",purple_marshal_BOOLEAN__POINTER_POINTER_POINTER_POINTER_POINTER,purple_value_new(PURPLE_TYPE_BOOLEAN),5,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONNECTION),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_XMLNODE));
/* type */
/* id */
/* from */
/* child */
  purple_signal_register(plugin,"jabber-receiving-presence",purple_marshal_BOOLEAN__POINTER_POINTER_POINTER_POINTER,purple_value_new(PURPLE_TYPE_BOOLEAN),4,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONNECTION),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_XMLNODE));
/* type */
/* from */
}

void jabber_plugin_uninit(PurplePlugin *plugin)
{
  do {
    if (plugin_ref > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin_ref > 0");
      return ;
    };
  }while (0);
  purple_signals_unregister_by_instance(plugin);
  purple_plugin_ipc_unregister_all(plugin);
  jabber_unregister_commands(plugin);
  --plugin_ref;
  if (plugin_ref == 0) 
    jabber_do_uninit();
}
