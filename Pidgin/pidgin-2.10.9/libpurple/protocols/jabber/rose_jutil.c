/*
 * purple - Jabber Protocol Plugin
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "account.h"
#include "cipher.h"
#include "conversation.h"
#include "debug.h"
#include "server.h"
#include "util.h"
#include "xmlnode.h"
#include "chat.h"
#include "presence.h"
#include "jutil.h"
#ifdef USE_IDN
#include <idna.h>
#include <stringprep.h>
static char idn_buffer[1024UL];
#endif
#ifdef USE_IDN

static gboolean jabber_nodeprep(char *str,size_t buflen)
{
  return stringprep(str,buflen,0,stringprep_xmpp_nodeprep) == STRINGPREP_OK;
}

static gboolean jabber_resourceprep(char *str,size_t buflen)
{
  return stringprep(str,buflen,0,stringprep_xmpp_resourceprep) == STRINGPREP_OK;
}

static JabberID *jabber_idn_validate(const char *str,const char *at,const char *slash,const char *null)
{
  const char *node = (const char *)((void *)0);
  const char *domain = (const char *)((void *)0);
  const char *resource = (const char *)((void *)0);
  int node_len = 0;
  int domain_len = 0;
  int resource_len = 0;
  char *out;
  JabberID *jid;
/* Ensure no parts are > 1023 bytes */
  if (at != 0) {
    node = str;
    node_len = (at - str);
    domain = (at + 1);
    if (slash != 0) {
      domain_len = (slash - (at + 1));
      resource = (slash + 1);
      resource_len = (null - (slash + 1));
    }
    else {
      domain_len = (null - (at + 1));
    }
  }
  else {
    domain = str;
    if (slash != 0) {
      domain_len = (slash - str);
      resource = slash;
      resource_len = (null - (slash + 1));
    }
    else {
      domain_len = (null - (str + 1));
    }
  }
  if ((node != 0) && (node_len > 1023)) 
    return 0;
  if (domain_len > 1023) 
    return 0;
  if ((resource != 0) && (resource_len > 1023)) 
    return 0;
  jid = ((JabberID *)(g_malloc0_n(1,(sizeof(JabberID )))));
  if (node != 0) {
    strncpy(idn_buffer,node,node_len);
    idn_buffer[node_len] = 0;
    if (!(jabber_nodeprep(idn_buffer,(sizeof(idn_buffer))) != 0)) {
      jabber_id_free(jid);
      jid = ((JabberID *)((void *)0));
      goto out;
    }
    jid -> node = g_strdup(idn_buffer);
  }
/* domain *must* be here */
  strncpy(idn_buffer,domain,domain_len);
  idn_buffer[domain_len] = 0;
/* IPv6 address */
  if (domain[0] == '[') {
    gboolean valid = 0;
    if (idn_buffer[domain_len - 1] == ']') {
      idn_buffer[domain_len - 1] = 0;
      valid = purple_ipv6_address_is_valid((idn_buffer + 1));
    }
    if (!(valid != 0)) {
      jabber_id_free(jid);
      jid = ((JabberID *)((void *)0));
      goto out;
    }
  }
  else {
/* Apply nameprep */
    if (stringprep(idn_buffer,(sizeof(idn_buffer)),0,stringprep_nameprep) != STRINGPREP_OK) {
      jabber_id_free(jid);
      jid = ((JabberID *)((void *)0));
      goto out;
    }
/* And now ToASCII */
    if (idna_to_ascii_8z(idn_buffer,&out,IDNA_USE_STD3_ASCII_RULES) != IDNA_SUCCESS) {
      jabber_id_free(jid);
      jid = ((JabberID *)((void *)0));
      goto out;
    }
/* This *MUST* be freed using 'free', not 'g_free' */
    free(out);
    jid -> domain = g_strdup(idn_buffer);
  }
  if (resource != 0) {
    strncpy(idn_buffer,resource,resource_len);
    idn_buffer[resource_len] = 0;
    if (!(jabber_resourceprep(idn_buffer,(sizeof(idn_buffer))) != 0)) {
      jabber_id_free(jid);
      jid = ((JabberID *)((void *)0));
      goto out;
    }
    else 
      jid -> resource = g_strdup(idn_buffer);
  }
  out:
  return jid;
}
#endif /* USE_IDN */

gboolean jabber_nodeprep_validate(const char *str)
{
#ifdef USE_IDN
  gboolean result;
#else
#endif
  if (!(str != 0)) 
    return (!0);
  if (strlen(str) > 1023) 
    return 0;
#ifdef USE_IDN
  strncpy(idn_buffer,str,(sizeof(idn_buffer) - 1));
  idn_buffer[sizeof(idn_buffer) - 1] = 0;
  result = jabber_nodeprep(idn_buffer,(sizeof(idn_buffer)));
  return result;
#else /* USE_IDN */
#endif /* USE_IDN */
}

gboolean jabber_domain_validate(const char *str)
{
  const char *c;
  size_t len;
  if (!(str != 0)) 
    return (!0);
  len = strlen(str);
  if (len > 1023) 
    return 0;
  c = str;
  if (( *c) == '[') {
/* Check if str is a valid IPv6 identifier */
    gboolean valid = 0;
    if (( *((c + len) - 1)) != ']') 
      return 0;
/* Ugly, but in-place */
     *((gchar *)((c + len) - 1)) = 0;
    valid = purple_ipv6_address_is_valid((c + 1));
     *((gchar *)((c + len) - 1)) = ']';
    return valid;
  }
  while((c != 0) && (( *c) != 0)){
    gunichar ch = g_utf8_get_char(c);
/* The list of characters allowed in domain names is pretty small */
    if (((ch <= 0x7F) && !((((((ch >= 'a') && (ch <= 'z')) || ((ch >= 48) && (ch <= '9'))) || ((ch >= 65) && (ch <= 'Z'))) || (ch == '.')) || (ch == '-'))) || ((ch >= 128) && !(g_unichar_isgraph(ch) != 0))) 
      return 0;
    c = ((char *)(c + g_utf8_skip[ *((const guchar *)c)]));
  }
  return (!0);
}

gboolean jabber_resourceprep_validate(const char *str)
{
#ifdef USE_IDN
  gboolean result;
#else
#endif
  if (!(str != 0)) 
    return (!0);
  if (strlen(str) > 1023) 
    return 0;
#ifdef USE_IDN
  strncpy(idn_buffer,str,(sizeof(idn_buffer) - 1));
  idn_buffer[sizeof(idn_buffer) - 1] = 0;
  result = jabber_resourceprep(idn_buffer,(sizeof(idn_buffer)));
  return result;
#else /* USE_IDN */
#endif /* USE_IDN */
}

char *jabber_saslprep(const char *in)
{
#ifdef USE_IDN
  char *out;
  do {
    if (in != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"in != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((strlen(in)) <= sizeof(idn_buffer) - 1) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"strlen(in) <= sizeof(idn_buffer) - 1");
      return 0;
    };
  }while (0);
  strncpy(idn_buffer,in,(sizeof(idn_buffer) - 1));
  idn_buffer[sizeof(idn_buffer) - 1] = 0;
  if (STRINGPREP_OK != stringprep(idn_buffer,(sizeof(idn_buffer)),0,stringprep_saslprep)) {
    memset(idn_buffer,0,(sizeof(idn_buffer)));
    return 0;
  }
  out = g_strdup(idn_buffer);
  memset(idn_buffer,0,(sizeof(idn_buffer)));
  return out;
#else /* USE_IDN */
/* TODO: Something better than disallowing all non-ASCII characters */
/* TODO: Is this even correct? */
/* Non-ASCII characters */
/* ASCII Delete character */
/* ASCII control characters */
#endif /* USE_IDN */
}

static JabberID *jabber_id_new_internal(const char *str,gboolean allow_terminating_slash)
{
  const char *at = (const char *)((void *)0);
  const char *slash = (const char *)((void *)0);
  const char *c;
  gboolean needs_validation = 0;
#if 0
#endif
#ifndef USE_IDN
#endif
  JabberID *jid;
  if (!(str != 0)) 
    return 0;
  for (c = str; ( *c) != 0; c++) {{
      switch(( *c)){
        case '@':
{
          if (!(slash != 0)) {
            if (at != 0) {
/* Multiple @'s in the node/domain portion, not a valid JID! */
              return 0;
            }
            if (c == str) {
/* JIDs cannot start with @ */
              return 0;
            }
            if (c[1] == 0) {
/* JIDs cannot end with @ */
              return 0;
            }
            at = c;
          }
          break; 
        }
        case '/':
{
          if (!(slash != 0)) {
            if (c == str) {
/* JIDs cannot start with / */
              return 0;
            }
            if ((c[1] == 0) && !(allow_terminating_slash != 0)) {
/* JIDs cannot end with / */
              return 0;
            }
            slash = c;
          }
          break; 
        }
        default:
{
/* characters allowed everywhere */
          if ((((((( *c) >= 'a') && (( *c) <= 'z')) || ((( *c) >= 48) && (( *c) <= '9'))) || ((( *c) >= 65) && (( *c) <= 'Z'))) || (( *c) == '.')) || (( *c) == '-')) 
/* We're good */
            break; 
#if 0
/* characters allowed only in the resource */
/* We're good */
/* characters allowed only in the node */
/*
					 * Ok, this character is valid, but only if it's a part
					 * of the node and not the domain.  But we don't know
					 * if "c" is a part of the node or the domain until after
					 * we've found the @.  So set a flag for now and check
					 * that we found an @ later.
					 */
#endif
/*
				 * Hmm, this character is a bit more exotic.  Better fall
				 * back to using the more expensive UTF-8 compliant
				 * stringprep functions.
				 */
          needs_validation = (!0);
          break; 
        }
      }
    }
  }
#if 0
/* Found invalid characters in the domain */
#endif
  if (!(needs_validation != 0)) {
/* JID is made of only ASCII characters--just lowercase and return */
    jid = ((JabberID *)(g_malloc0_n(1,(sizeof(JabberID )))));
    if (at != 0) {
      jid -> node = g_ascii_strdown(str,(at - str));
      if (slash != 0) {
        jid -> domain = g_ascii_strdown((at + 1),(slash - (at + 1)));
        if (slash[1] != 0) 
          jid -> resource = g_strdup((slash + 1));
      }
      else {
        jid -> domain = g_ascii_strdown((at + 1),(-1));
      }
    }
    else {
      if (slash != 0) {
        jid -> domain = g_ascii_strdown(str,(slash - str));
        if (slash[1] != 0) 
          jid -> resource = g_strdup((slash + 1));
      }
      else {
        jid -> domain = g_ascii_strdown(str,(-1));
      }
    }
    return jid;
  }
/*
	 * If we get here, there are some non-ASCII chars in the string, so
	 * we'll need to validate it, normalize, and finally do a full jabber
	 * nodeprep on the jid.
	 */
  if (!(g_utf8_validate(str,(-1),0) != 0)) 
    return 0;
#ifdef USE_IDN
/* points to the null */
  return jabber_idn_validate(str,at,slash,c);
#else /* USE_IDN */
/* normalization */
/* and finally the jabber nodeprep */
#endif /* USE_IDN */
}

void jabber_id_free(JabberID *jid)
{
  if (jid != 0) {
    g_free((jid -> node));
    g_free((jid -> domain));
    g_free((jid -> resource));
    g_free(jid);
  }
}

gboolean jabber_id_equal(const JabberID *jid1,const JabberID *jid2)
{
  if (!(jid1 != 0) && !(jid2 != 0)) {
/* Both are null therefore equal */
    return (!0);
  }
  if (!(jid1 != 0) || !(jid2 != 0)) {
/* One is null, other is non-null, therefore not equal */
    return 0;
  }
  return ((purple_strequal((jid1 -> node),(jid2 -> node)) != 0) && (purple_strequal((jid1 -> domain),(jid2 -> domain)) != 0)) && (purple_strequal((jid1 -> resource),(jid2 -> resource)) != 0);
}

char *jabber_get_domain(const char *in)
{
  JabberID *jid = jabber_id_new(in);
  char *out;
  if (!(jid != 0)) 
    return 0;
  out = g_strdup((jid -> domain));
  jabber_id_free(jid);
  return out;
}

char *jabber_get_resource(const char *in)
{
  JabberID *jid = jabber_id_new(in);
  char *out;
  if (!(jid != 0)) 
    return 0;
  out = g_strdup((jid -> resource));
  jabber_id_free(jid);
  return out;
}

JabberID *jabber_id_to_bare_jid(const JabberID *jid)
{
  JabberID *result = (JabberID *)(g_malloc0_n(1,(sizeof(JabberID ))));
  result -> node = g_strdup((jid -> node));
  result -> domain = g_strdup((jid -> domain));
  return result;
}

char *jabber_get_bare_jid(const char *in)
{
  JabberID *jid = jabber_id_new(in);
  char *out;
  if (!(jid != 0)) 
    return 0;
  out = jabber_id_get_bare_jid(jid);
  jabber_id_free(jid);
  return out;
}

char *jabber_id_get_bare_jid(const JabberID *jid)
{
  do {
    if (jid != ((const JabberID *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"jid != NULL");
      return 0;
    };
  }while (0);
  return g_strconcat((((jid -> node) != 0)?(jid -> node) : ""),(((jid -> node) != 0)?"@" : ""),(jid -> domain),((void *)((void *)0)));
}

char *jabber_id_get_full_jid(const JabberID *jid)
{
  do {
    if (jid != ((const JabberID *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"jid != NULL");
      return 0;
    };
  }while (0);
  return g_strconcat((((jid -> node) != 0)?(jid -> node) : ""),(((jid -> node) != 0)?"@" : ""),(jid -> domain),(((jid -> resource) != 0)?"/" : ""),(((jid -> resource) != 0)?(jid -> resource) : ""),((void *)((void *)0)));
}

gboolean jabber_jid_is_domain(const char *jid)
{
  const char *c;
  for (c = jid; ( *c) != 0; ++c) {
    if ((( *c) == 64) || (( *c) == '/')) 
      return 0;
  }
  return (!0);
}

JabberID *jabber_id_new(const char *str)
{
  return jabber_id_new_internal(str,0);
}

const char *jabber_normalize(const PurpleAccount *account,const char *in)
{
  PurpleConnection *gc = (account != 0)?(account -> gc) : ((struct _PurpleConnection *)((void *)0));
  JabberStream *js = ((gc != 0)?(gc -> proto_data) : ((void *)((void *)0)));
/* maximum legal length of a jabber jid */
  static char buf[3072UL];
  JabberID *jid;
  jid = jabber_id_new_internal(in,(!0));
  if (!(jid != 0)) 
    return 0;
  if ((((js != 0) && ((jid -> node) != 0)) && ((jid -> resource) != 0)) && (jabber_chat_find(js,(jid -> node),(jid -> domain)) != 0)) 
    g_snprintf(buf,(sizeof(buf)),"%s@%s/%s",(jid -> node),(jid -> domain),(jid -> resource));
  else 
    g_snprintf(buf,(sizeof(buf)),"%s%s%s",(((jid -> node) != 0)?(jid -> node) : ""),(((jid -> node) != 0)?"@" : ""),(jid -> domain));
  jabber_id_free(jid);
  return buf;
}

gboolean jabber_is_own_server(JabberStream *js,const char *str)
{
  JabberID *jid;
  gboolean equal;
  if (str == ((const char *)((void *)0))) 
    return 0;
  do {
    if (( *str) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"*str != \'\\0\'");
      return 0;
    };
  }while (0);
  jid = jabber_id_new(str);
  if (!(jid != 0)) 
    return 0;
  equal = ((((jid -> node) == ((char *)((void *)0))) && (g_str_equal((jid -> domain),( *(js -> user)).domain) != 0)) && ((jid -> resource) == ((char *)((void *)0))));
  jabber_id_free(jid);
  return equal;
}

gboolean jabber_is_own_account(JabberStream *js,const char *str)
{
  JabberID *jid;
  gboolean equal;
  if (str == ((const char *)((void *)0))) 
    return (!0);
  do {
    if (( *str) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"*str != \'\\0\'");
      return 0;
    };
  }while (0);
  jid = jabber_id_new(str);
  if (!(jid != 0)) 
    return 0;
  equal = (((purple_strequal((jid -> node),( *(js -> user)).node) != 0) && (g_str_equal((jid -> domain),( *(js -> user)).domain) != 0)) && (((jid -> resource) == ((char *)((void *)0))) || (g_str_equal((jid -> resource),( *(js -> user)).resource) != 0)));
  jabber_id_free(jid);
  return equal;
}
static const struct __unnamed_class___F0_L696_C14_unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__status_id__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__show__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__readable__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L602R_variable_name_unknown_scope_and_name__scope__state {
/* link to core */
const char *status_id;
/* The show child's cdata in a presence stanza */
const char *show;
/* readable representation */
const char *readable;
JabberBuddyState state;}jabber_statuses[] = {{("offline"), ((const char *)((void *)0)), ("Offline"), (JABBER_BUDDY_STATE_UNAVAILABLE)}, {("available"), ((const char *)((void *)0)), ("Available"), (JABBER_BUDDY_STATE_ONLINE)}, {("freeforchat"), ("chat"), ("Chatty"), (JABBER_BUDDY_STATE_CHAT)}, {("away"), ("away"), ("Away"), (JABBER_BUDDY_STATE_AWAY)}, {("extended_away"), ("xa"), ("Extended Away"), (JABBER_BUDDY_STATE_XA)}, {("dnd"), ("dnd"), ("Do Not Disturb"), (JABBER_BUDDY_STATE_DND)}, {("error"), ((const char *)((void *)0)), ("Error"), (JABBER_BUDDY_STATE_ERROR)}};

const char *jabber_buddy_state_get_name(const JabberBuddyState state)
{
  int i;
  for (i = 0; i < sizeof(jabber_statuses) / sizeof(jabber_statuses[0]); ++i) 
    if (jabber_statuses[i].state == state) 
      return (const char *)(dgettext("pidgin",jabber_statuses[i].readable));
  return (const char *)(dgettext("pidgin","Unknown"));
}

JabberBuddyState jabber_buddy_status_id_get_state(const char *id)
{
  int i;
  if (!(id != 0)) 
    return JABBER_BUDDY_STATE_UNKNOWN;
  for (i = 0; i < sizeof(jabber_statuses) / sizeof(jabber_statuses[0]); ++i) 
    if (g_str_equal(id,jabber_statuses[i].status_id) != 0) 
      return jabber_statuses[i].state;
  return JABBER_BUDDY_STATE_UNKNOWN;
}

JabberBuddyState jabber_buddy_show_get_state(const char *id)
{
  int i;
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return JABBER_BUDDY_STATE_UNKNOWN;
    };
  }while (0);
  for (i = 0; i < sizeof(jabber_statuses) / sizeof(jabber_statuses[0]); ++i) 
    if ((jabber_statuses[i].show != 0) && (g_str_equal(id,jabber_statuses[i].show) != 0)) 
      return jabber_statuses[i].state;
  purple_debug_warning("jabber","Invalid value of presence <show/> attribute: %s\n",id);
  return JABBER_BUDDY_STATE_UNKNOWN;
}

const char *jabber_buddy_state_get_show(JabberBuddyState state)
{
  int i;
  for (i = 0; i < sizeof(jabber_statuses) / sizeof(jabber_statuses[0]); ++i) 
    if (state == jabber_statuses[i].state) 
      return jabber_statuses[i].show;
  return 0;
}

const char *jabber_buddy_state_get_status_id(JabberBuddyState state)
{
  int i;
  for (i = 0; i < sizeof(jabber_statuses) / sizeof(jabber_statuses[0]); ++i) 
    if (state == jabber_statuses[i].state) 
      return jabber_statuses[i].status_id;
  return 0;
}

char *jabber_calculate_data_hash(gconstpointer data,size_t len,const gchar *hash_algo)
{
  PurpleCipherContext *context;
/* 512 bits hex + \0 */
  static gchar digest[129UL];
  context = purple_cipher_context_new_by_name(hash_algo,0);
  if (context == ((PurpleCipherContext *)((void *)0))) {
    purple_debug_error("jabber","Could not find %s cipher\n",hash_algo);
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","jutil.c",784,((const char *)__func__));
      return 0;
    }while (0);
  }
/* Hash the data */
  purple_cipher_context_append(context,data,len);
  if (!(purple_cipher_context_digest_to_str(context,(sizeof(digest)),digest,0) != 0)) {
    purple_debug_error("jabber","Failed to get digest for %s cipher.\n",hash_algo);
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","jutil.c",793,((const char *)__func__));
      return 0;
    }while (0);
  }
  purple_cipher_context_destroy(context);
  return g_strdup(digest);
}
