/*
 * purple - Jabber Protocol Plugin
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "debug.h"
#include "caps.h"
#include "cipher.h"
#include "iq.h"
#include "presence.h"
#include "util.h"
#include "xdata.h"
#define JABBER_CAPS_FILENAME "xmpp-caps.xml"
typedef struct _JabberDataFormField {
gchar *var;
GList *values;}JabberDataFormField;
/* JabberCapsTuple -> JabberCapsClientInfo */
static GHashTable *capstable = (GHashTable *)((void *)0);
/* char *node -> JabberCapsNodeExts */
static GHashTable *nodetable = (GHashTable *)((void *)0);
static guint save_timer = 0;
/* Free a GList of allocated char* */

static void free_string_glist(GList *list)
{
  while(list != 0){
    g_free((list -> data));
    list = g_list_delete_link(list,list);
  }
}

static JabberCapsNodeExts *jabber_caps_node_exts_ref(JabberCapsNodeExts *exts)
{
  do {
    if (exts != ((JabberCapsNodeExts *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"exts != NULL");
      return 0;
    };
  }while (0);
  ++exts -> ref;
  return exts;
}

static void jabber_caps_node_exts_unref(JabberCapsNodeExts *exts)
{
  if (exts == ((JabberCapsNodeExts *)((void *)0))) 
    return ;
  do {
    if ((exts -> ref) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"exts->ref != 0");
      return ;
    };
  }while (0);
  if (--exts -> ref != 0) 
    return ;
  g_hash_table_destroy((exts -> exts));
  g_free(exts);
}

static guint jabber_caps_hash(gconstpointer data)
{
  const JabberCapsTuple *key = data;
  guint nodehash = g_str_hash((key -> node));
  guint verhash = g_str_hash((key -> ver));
/*
	 * 'hash' was optional in XEP-0115 v1.4 and g_str_hash crashes on NULL >:O.
	 * Okay, maybe I've played too much Zelda, but that looks like
	 * a Deku Shrub...
	 */
  guint hashhash = ((key -> hash) != 0)?g_str_hash((key -> hash)) : 0;
  return (nodehash ^ verhash) ^ hashhash;
}

static gboolean jabber_caps_compare(gconstpointer v1,gconstpointer v2)
{
  const JabberCapsTuple *name1 = v1;
  const JabberCapsTuple *name2 = v2;
  return ((g_str_equal((name1 -> node),(name2 -> node)) != 0) && (g_str_equal((name1 -> ver),(name2 -> ver)) != 0)) && (purple_strequal((name1 -> hash),(name2 -> hash)) != 0);
}

static void jabber_caps_client_info_destroy(JabberCapsClientInfo *info)
{
  if (info == ((JabberCapsClientInfo *)((void *)0))) 
    return ;
  while((info -> identities) != 0){
    JabberIdentity *id = ( *(info -> identities)).data;
    g_free((id -> category));
    g_free((id -> type));
    g_free((id -> name));
    g_free((id -> lang));
    g_free(id);
    info -> identities = g_list_delete_link((info -> identities),(info -> identities));
  }
  free_string_glist((info -> features));
  while((info -> forms) != 0){
    xmlnode_free(( *(info -> forms)).data);
    info -> forms = g_list_delete_link((info -> forms),(info -> forms));
  }
  jabber_caps_node_exts_unref((info -> exts));
  g_free(((char *)info -> tuple.node));
  g_free(((char *)info -> tuple.ver));
  g_free(((char *)info -> tuple.hash));
  g_free(info);
}
/* NOTE: Takes a reference to the exts, unref it if you don't really want to
 * keep it around. */

static JabberCapsNodeExts *jabber_caps_find_exts_by_node(const char *node)
{
  JabberCapsNodeExts *exts;
  if (((JabberCapsNodeExts *)((void *)0)) == (exts = (g_hash_table_lookup(nodetable,node)))) {
    exts = ((JabberCapsNodeExts *)(g_malloc0_n(1,(sizeof(JabberCapsNodeExts )))));
    exts -> exts = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,((GDestroyNotify )free_string_glist));
    g_hash_table_insert(nodetable,(g_strdup(node)),(jabber_caps_node_exts_ref(exts)));
  }
  return jabber_caps_node_exts_ref(exts);
}

static void exts_to_xmlnode(gconstpointer key,gconstpointer value,gpointer user_data)
{
  const char *identifier = key;
  const GList *features = value;
  const GList *node;
  xmlnode *client = user_data;
  xmlnode *ext;
  xmlnode *feature;
  ext = xmlnode_new_child(client,"ext");
  xmlnode_set_attrib(ext,"identifier",identifier);
  for (node = features; node != 0; node = (node -> next)) {
    feature = xmlnode_new_child(ext,"feature");
    xmlnode_set_attrib(feature,"var",((const gchar *)(node -> data)));
  }
}

static void jabber_caps_store_client(gpointer key,gpointer value,gpointer user_data)
{
  const JabberCapsTuple *tuple = key;
  const JabberCapsClientInfo *props = value;
  xmlnode *root = user_data;
  xmlnode *client = xmlnode_new_child(root,"client");
  GList *iter;
  xmlnode_set_attrib(client,"node",(tuple -> node));
  xmlnode_set_attrib(client,"ver",(tuple -> ver));
  if ((tuple -> hash) != 0) 
    xmlnode_set_attrib(client,"hash",(tuple -> hash));
  for (iter = (props -> identities); iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
    JabberIdentity *id = (iter -> data);
    xmlnode *identity = xmlnode_new_child(client,"identity");
    xmlnode_set_attrib(identity,"category",(id -> category));
    xmlnode_set_attrib(identity,"type",(id -> type));
    if ((id -> name) != 0) 
      xmlnode_set_attrib(identity,"name",(id -> name));
    if ((id -> lang) != 0) 
      xmlnode_set_attrib(identity,"lang",(id -> lang));
  }
  for (iter = (props -> features); iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
    const char *feat = (iter -> data);
    xmlnode *feature = xmlnode_new_child(client,"feature");
    xmlnode_set_attrib(feature,"var",feat);
  }
  for (iter = (props -> forms); iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
/* FIXME: See #7814 */
    xmlnode *xdata = (iter -> data);
    xmlnode_insert_child(client,xmlnode_copy(xdata));
  }
/* TODO: Ideally, only save this once-per-node... */
  if ((props -> exts) != 0) 
    g_hash_table_foreach(( *(props -> exts)).exts,((GHFunc )exts_to_xmlnode),client);
}

static gboolean do_jabber_caps_store(gpointer data)
{
  char *str;
  int length = 0;
  xmlnode *root = xmlnode_new("capabilities");
  g_hash_table_foreach(capstable,jabber_caps_store_client,root);
  str = xmlnode_to_formatted_str(root,&length);
  xmlnode_free(root);
  purple_util_write_data_to_file("xmpp-caps.xml",str,length);
  g_free(str);
  save_timer = 0;
  return 0;
}

static void schedule_caps_save()
{
  if (save_timer == 0) 
    save_timer = purple_timeout_add_seconds(5,do_jabber_caps_store,0);
}

static void jabber_caps_load()
{
  xmlnode *capsdata = purple_util_read_xml_from_file("xmpp-caps.xml","XMPP capabilities cache");
  xmlnode *client;
  if (!(capsdata != 0)) 
    return ;
  if (!(g_str_equal((capsdata -> name),"capabilities") != 0)) {
    xmlnode_free(capsdata);
    return ;
  }
  for (client = (capsdata -> child); client != 0; client = (client -> next)) {
    if ((client -> type) != XMLNODE_TYPE_TAG) 
      continue; 
    if (g_str_equal((client -> name),"client") != 0) {
      JabberCapsClientInfo *value = (JabberCapsClientInfo *)(g_malloc0_n(1,(sizeof(JabberCapsClientInfo ))));
      JabberCapsTuple *key = (JabberCapsTuple *)(&value -> tuple);
      xmlnode *child;
      JabberCapsNodeExts *exts = (JabberCapsNodeExts *)((void *)0);
      key -> node = (g_strdup(xmlnode_get_attrib(client,"node")));
      key -> ver = (g_strdup(xmlnode_get_attrib(client,"ver")));
      key -> hash = (g_strdup(xmlnode_get_attrib(client,"hash")));
/* v1.3 capabilities */
      if ((key -> hash) == ((const char *)((void *)0))) 
        exts = jabber_caps_find_exts_by_node((key -> node));
      for (child = (client -> child); child != 0; child = (child -> next)) {
        if ((child -> type) != XMLNODE_TYPE_TAG) 
          continue; 
        if (g_str_equal((child -> name),"feature") != 0) {
          const char *var = xmlnode_get_attrib(child,"var");
          if (!(var != 0)) 
            continue; 
          value -> features = g_list_append((value -> features),(g_strdup(var)));
        }
        else if (g_str_equal((child -> name),"identity") != 0) {
          const char *category = xmlnode_get_attrib(child,"category");
          const char *type = xmlnode_get_attrib(child,"type");
          const char *name = xmlnode_get_attrib(child,"name");
          const char *lang = xmlnode_get_attrib(child,"lang");
          JabberIdentity *id;
          if (!(category != 0) || !(type != 0)) 
            continue; 
          id = ((JabberIdentity *)(g_malloc0_n(1,(sizeof(JabberIdentity )))));
          id -> category = g_strdup(category);
          id -> type = g_strdup(type);
          id -> name = g_strdup(name);
          id -> lang = g_strdup(lang);
          value -> identities = g_list_append((value -> identities),id);
        }
        else if (g_str_equal((child -> name),"x") != 0) {
/* TODO: See #7814 -- this might cause problems if anyone
					 * ever actually specifies forms. In fact, for this to
					 * work properly, that bug needs to be fixed in
					 * xmlnode_from_str, not the output version... */
          value -> forms = g_list_append((value -> forms),(xmlnode_copy(child)));
        }
        else if (g_str_equal((child -> name),"ext") != 0) {
          if ((key -> hash) != ((const char *)((void *)0))) 
            purple_debug_warning("jabber","Ignoring exts when reading new-style caps\n");
          else {
/* TODO: Do we care about reading in the identities listed here? */
            const char *identifier = xmlnode_get_attrib(child,"identifier");
            xmlnode *node;
            GList *features = (GList *)((void *)0);
            if (!(identifier != 0)) 
              continue; 
            for (node = (child -> child); node != 0; node = (node -> next)) {
              if ((node -> type) != XMLNODE_TYPE_TAG) 
                continue; 
              if (g_str_equal((node -> name),"feature") != 0) {
                const char *var = xmlnode_get_attrib(node,"var");
                if (!(var != 0)) 
                  continue; 
                features = g_list_prepend(features,(g_strdup(var)));
              }
            }
            if (features != 0) {
              g_hash_table_insert((exts -> exts),(g_strdup(identifier)),features);
            }
            else 
              purple_debug_warning("jabber","Caps ext %s had no features.\n",identifier);
          }
        }
      }
      value -> exts = exts;
      g_hash_table_replace(capstable,key,value);
    }
  }
  xmlnode_free(capsdata);
}

void jabber_caps_init()
{
  nodetable = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,((GDestroyNotify )jabber_caps_node_exts_unref));
  capstable = g_hash_table_new_full(jabber_caps_hash,jabber_caps_compare,0,((GDestroyNotify )jabber_caps_client_info_destroy));
  jabber_caps_load();
}

void jabber_caps_uninit()
{
  if (save_timer != 0) {
    purple_timeout_remove(save_timer);
    save_timer = 0;
    do_jabber_caps_store(0);
  }
  g_hash_table_destroy(capstable);
  g_hash_table_destroy(nodetable);
  capstable = (nodetable = ((GHashTable *)((void *)0)));
}

gboolean jabber_caps_exts_known(const JabberCapsClientInfo *info,char **exts)
{
  int i;
  do {
    if (info != ((const JabberCapsClientInfo *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"info != NULL");
      return 0;
    };
  }while (0);
  if (!(exts != 0)) 
    return (!0);
  for (i = 0; exts[i] != 0; ++i) {
/* Hack since we advertise the ext along with v1.5 caps but don't
		 * store any exts */
    if ((g_str_equal(exts[i],"voice-v1") != 0) && !((info -> exts) != 0)) 
      continue; 
    if (!((info -> exts) != 0) || !(g_hash_table_lookup(( *(info -> exts)).exts,exts[i]) != 0)) 
      return 0;
  }
  return (!0);
}
typedef struct _jabber_caps_cbplususerdata {
guint ref;
jabber_caps_get_info_cb cb;
gpointer cb_data;
char *who;
char *node;
char *ver;
char *hash;
JabberCapsClientInfo *info;
GList *exts;
guint extOutstanding;
JabberCapsNodeExts *node_exts;}jabber_caps_cbplususerdata;

static jabber_caps_cbplususerdata *cbplususerdata_ref(jabber_caps_cbplususerdata *data)
{
  do {
    if (data != ((jabber_caps_cbplususerdata *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"data != NULL");
      return 0;
    };
  }while (0);
  ++data -> ref;
  return data;
}

static void cbplususerdata_unref(jabber_caps_cbplususerdata *data)
{
  if (data == ((jabber_caps_cbplususerdata *)((void *)0))) 
    return ;
  do {
    if ((data -> ref) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"data->ref != 0");
      return ;
    };
  }while (0);
  if (--data -> ref > 0) 
    return ;
  g_free((data -> who));
  g_free((data -> node));
  g_free((data -> ver));
  g_free((data -> hash));
/* If we have info here, it's already in the capstable, so don't free it */
  if ((data -> exts) != 0) 
    free_string_glist((data -> exts));
  if ((data -> node_exts) != 0) 
    jabber_caps_node_exts_unref((data -> node_exts));
  g_free(data);
}

static void jabber_caps_get_info_complete(jabber_caps_cbplususerdata *userdata)
{
  if ((userdata -> cb) != 0) {
    ( *(userdata -> cb))((userdata -> info),(userdata -> exts),(userdata -> cb_data));
    userdata -> info = ((JabberCapsClientInfo *)((void *)0));
    userdata -> exts = ((GList *)((void *)0));
  }
  if ((userdata -> ref) != 1) 
    purple_debug_warning("jabber","Lost a reference to caps cbdata: %d\n",(userdata -> ref));
}

static void jabber_caps_client_iqcb(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  xmlnode *query = xmlnode_get_child_with_namespace(packet,"query","http://jabber.org/protocol/disco#info");
  jabber_caps_cbplususerdata *userdata = data;
  JabberCapsClientInfo *info = (JabberCapsClientInfo *)((void *)0);
  JabberCapsClientInfo *value;
  JabberCapsTuple key;
  if (!(query != 0) || (type == JABBER_IQ_ERROR)) {
/* Any outstanding exts will be dealt with via ref-counting */
    ( *(userdata -> cb))(0,0,(userdata -> cb_data));
    cbplususerdata_unref(userdata);
    return ;
  }
/* check hash */
  info = jabber_caps_parse_client_info(query);
/* Only validate if these are v1.5 capabilities */
  if ((userdata -> hash) != 0) {
    gchar *hash = (gchar *)((void *)0);
/*
		 * TODO: If you add *any* hash here, make sure the checksum buffer
		 * size in jabber_caps_calculate_hash is large enough. The cipher API
		 * doesn't seem to offer a "Get the hash size" function(?).
		 */
    if (g_str_equal((userdata -> hash),"sha-1") != 0) {
      hash = jabber_caps_calculate_hash(info,"sha1");
    }
    else if (g_str_equal((userdata -> hash),"md5") != 0) {
      hash = jabber_caps_calculate_hash(info,"md5");
    }
    if (!(hash != 0) || !(g_str_equal(hash,(userdata -> ver)) != 0)) {
      purple_debug_warning("jabber","Could not validate caps info from %s. Expected %s, got %s\n",xmlnode_get_attrib(packet,"from"),(userdata -> ver),((hash != 0)?hash : "(null)"));
      ( *(userdata -> cb))(0,0,(userdata -> cb_data));
      jabber_caps_client_info_destroy(info);
      cbplususerdata_unref(userdata);
      g_free(hash);
      return ;
    }
    g_free(hash);
  }
  if (!((userdata -> hash) != 0) && ((userdata -> node_exts) != 0)) {
/* If the ClientInfo doesn't have information about the exts, give them
		 * ours (along with our ref) */
    info -> exts = (userdata -> node_exts);
    userdata -> node_exts = ((JabberCapsNodeExts *)((void *)0));
  }
  key.node = (userdata -> node);
  key.ver = (userdata -> ver);
  key.hash = (userdata -> hash);
/* Use the copy of this data already in the table if it exists or insert
	 * a new one if we need to */
  if ((value = (g_hash_table_lookup(capstable,(&key)))) != 0) {
    jabber_caps_client_info_destroy(info);
    info = value;
  }
  else {
    JabberCapsTuple *n_key = (JabberCapsTuple *)(&info -> tuple);
    n_key -> node = (userdata -> node);
    n_key -> ver = (userdata -> ver);
    n_key -> hash = (userdata -> hash);
    userdata -> node = (userdata -> ver = (userdata -> hash = ((char *)((void *)0))));
/* The capstable gets a reference */
    g_hash_table_insert(capstable,n_key,info);
    schedule_caps_save();
  }
  userdata -> info = info;
  if ((userdata -> extOutstanding) == 0) 
    jabber_caps_get_info_complete(userdata);
  cbplususerdata_unref(userdata);
}
typedef struct __unnamed_class___F0_L521_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__name__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L574R__Pe___variable_name_unknown_scope_and_name__scope__data {
const char *name;
jabber_caps_cbplususerdata *data;}ext_iq_data;

static void jabber_caps_ext_iqcb(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  xmlnode *query = xmlnode_get_child_with_namespace(packet,"query","http://jabber.org/protocol/disco#info");
  xmlnode *child;
  ext_iq_data *userdata = data;
  GList *features = (GList *)((void *)0);
  JabberCapsNodeExts *node_exts;
  if (!(query != 0) || (type == JABBER_IQ_ERROR)) {
    cbplususerdata_unref((userdata -> data));
    g_free(userdata);
    return ;
  }
  node_exts = ((( *(userdata -> data)).info != 0)?( *( *(userdata -> data)).info).exts : ( *(userdata -> data)).node_exts);
/* TODO: I don't see how this can actually happen, but it crashed khc. */
  if (!(node_exts != 0)) {
    purple_debug_error("jabber","Couldn\'t find JabberCapsNodeExts. If you see this, please tell darkrain42 and save your debug log.\nJabberCapsClientInfo = %p\n",( *(userdata -> data)).info);
/* Try once more to find the exts and then fail */
    node_exts = jabber_caps_find_exts_by_node(( *(userdata -> data)).node);
    if (node_exts != 0) {
      purple_debug_info("jabber","Found the exts on the second try.\n");
      if (( *(userdata -> data)).info != 0) 
        ( *( *(userdata -> data)).info).exts = node_exts;
      else 
        ( *(userdata -> data)).node_exts = node_exts;
    }
    else {
      cbplususerdata_unref((userdata -> data));
      g_free(userdata);
      do {
        g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","caps.c",564,((const char *)__func__));
        return ;
      }while (0);
    }
  }
/* So, we decrement this after checking for an error, which means that
	 * if there *is* an error, we'll never call the callback passed to
	 * jabber_caps_get_info. We will still free all of our data, though.
	 */
  --( *(userdata -> data)).extOutstanding;
  for (child = xmlnode_get_child(query,"feature"); child != 0; child = xmlnode_get_next_twin(child)) {
    const char *var = xmlnode_get_attrib(child,"var");
    if (var != 0) 
      features = g_list_prepend(features,(g_strdup(var)));
  }
  g_hash_table_insert((node_exts -> exts),(g_strdup((userdata -> name))),features);
  schedule_caps_save();
/* Are we done? */
  if ((( *(userdata -> data)).info != 0) && (( *(userdata -> data)).extOutstanding == 0)) 
    jabber_caps_get_info_complete((userdata -> data));
  cbplususerdata_unref((userdata -> data));
  g_free(userdata);
}

void jabber_caps_get_info(JabberStream *js,const char *who,const char *node,const char *ver,const char *hash,char **exts,jabber_caps_get_info_cb cb,gpointer user_data)
{
  JabberCapsClientInfo *info;
  JabberCapsTuple key;
  jabber_caps_cbplususerdata *userdata;
  if ((exts != 0) && (hash != 0)) {
    purple_debug_misc("jabber","Ignoring exts in new-style caps from %s\n",who);
    g_strfreev(exts);
    exts = ((char **)((void *)0));
  }
/* Using this in a read-only fashion, so the cast is OK */
  key.node = ((char *)node);
  key.ver = ((char *)ver);
  key.hash = ((char *)hash);
  info = (g_hash_table_lookup(capstable,(&key)));
  if ((info != 0) && (hash != 0)) {
/* v1.5 - We already have all the information we care about */
    if (cb != 0) 
      ( *cb)(info,0,user_data);
    return ;
  }
  userdata = ((jabber_caps_cbplususerdata *)(g_malloc0_n(1,(sizeof(jabber_caps_cbplususerdata )))));
/* We start out with 0 references. Every query takes one */
  userdata -> cb = cb;
  userdata -> cb_data = user_data;
  userdata -> who = g_strdup(who);
  userdata -> node = g_strdup(node);
  userdata -> ver = g_strdup(ver);
  userdata -> hash = g_strdup(hash);
  if (info != 0) {
    userdata -> info = info;
  }
  else {
/* If we don't have the basic information about the client, we need
		 * to fetch it. */
    JabberIq *iq;
    xmlnode *query;
    char *nodever;
    iq = jabber_iq_new_query(js,JABBER_IQ_GET,"http://jabber.org/protocol/disco#info");
    query = xmlnode_get_child_with_namespace((iq -> node),"query","http://jabber.org/protocol/disco#info");
    nodever = g_strdup_printf("%s#%s",node,ver);
    xmlnode_set_attrib(query,"node",nodever);
    g_free(nodever);
    xmlnode_set_attrib((iq -> node),"to",who);
    cbplususerdata_ref(userdata);
    jabber_iq_set_callback(iq,jabber_caps_client_iqcb,userdata);
    jabber_iq_send(iq);
  }
/* Are there any exts that we don't recognize? */
  if (exts != 0) {
    JabberCapsNodeExts *node_exts;
    int i;
    if (info != 0) {
      if ((info -> exts) != 0) 
        node_exts = (info -> exts);
      else 
        node_exts = (info -> exts = jabber_caps_find_exts_by_node(node));
    }
    else 
/* We'll put it in later once we have the client info */
      node_exts = (userdata -> node_exts = jabber_caps_find_exts_by_node(node));
    for (i = 0; exts[i] != 0; ++i) {
      userdata -> exts = g_list_prepend((userdata -> exts),exts[i]);
/* Look it up if we don't already know what it means */
      if (!(g_hash_table_lookup((node_exts -> exts),exts[i]) != 0)) {
        JabberIq *iq;
        xmlnode *query;
        char *nodeext;
        ext_iq_data *cbdata = (ext_iq_data *)(g_malloc_n(1,(sizeof(ext_iq_data ))));
        cbdata -> name = exts[i];
        cbdata -> data = cbplususerdata_ref(userdata);
        iq = jabber_iq_new_query(js,JABBER_IQ_GET,"http://jabber.org/protocol/disco#info");
        query = xmlnode_get_child_with_namespace((iq -> node),"query","http://jabber.org/protocol/disco#info");
        nodeext = g_strdup_printf("%s#%s",node,exts[i]);
        xmlnode_set_attrib(query,"node",nodeext);
        g_free(nodeext);
        xmlnode_set_attrib((iq -> node),"to",who);
        jabber_iq_set_callback(iq,jabber_caps_ext_iqcb,cbdata);
        jabber_iq_send(iq);
        ++userdata -> extOutstanding;
      }
      exts[i] = ((char *)((void *)0));
    }
/* All the strings are now part of the GList, so don't need
		 * g_strfreev. */
    g_free(exts);
  }
  if (((userdata -> info) != 0) && ((userdata -> extOutstanding) == 0)) {
/* Start with 1 ref so the below functions are happy */
    userdata -> ref = 1;
/* We have everything we need right now */
    jabber_caps_get_info_complete(userdata);
    cbplususerdata_unref(userdata);
  }
}

static gint jabber_xdata_compare(gconstpointer a,gconstpointer b)
{
  const xmlnode *aformtypefield = a;
  const xmlnode *bformtypefield = b;
  char *aformtype;
  char *bformtype;
  int result;
  aformtype = jabber_x_data_get_formtype(aformtypefield);
  bformtype = jabber_x_data_get_formtype(bformtypefield);
  result = strcmp(aformtype,bformtype);
  g_free(aformtype);
  g_free(bformtype);
  return result;
}

JabberCapsClientInfo *jabber_caps_parse_client_info(xmlnode *query)
{
  xmlnode *child;
  JabberCapsClientInfo *info;
  if ((!(query != 0) || !(g_str_equal((query -> name),"query") != 0)) || !(purple_strequal((query -> xmlns),"http://jabber.org/protocol/disco#info") != 0)) 
    return 0;
  info = ((JabberCapsClientInfo *)(g_malloc0_n(1,(sizeof(JabberCapsClientInfo )))));
  for (child = (query -> child); child != 0; child = (child -> next)) {
    if ((child -> type) != XMLNODE_TYPE_TAG) 
      continue; 
    if (g_str_equal((child -> name),"identity") != 0) {
/* parse identity */
      const char *category = xmlnode_get_attrib(child,"category");
      const char *type = xmlnode_get_attrib(child,"type");
      const char *name = xmlnode_get_attrib(child,"name");
      const char *lang = xmlnode_get_attrib(child,"lang");
      JabberIdentity *id;
      if (!(category != 0) || !(type != 0)) 
        continue; 
      id = ((JabberIdentity *)(g_malloc0_n(1,(sizeof(JabberIdentity )))));
      id -> category = g_strdup(category);
      id -> type = g_strdup(type);
      id -> name = g_strdup(name);
      id -> lang = g_strdup(lang);
      info -> identities = g_list_append((info -> identities),id);
    }
    else if (g_str_equal((child -> name),"feature") != 0) {
/* parse feature */
      const char *var = xmlnode_get_attrib(child,"var");
      if (var != 0) 
        info -> features = g_list_prepend((info -> features),(g_strdup(var)));
    }
    else if (g_str_equal((child -> name),"x") != 0) {
      if (purple_strequal((child -> xmlns),"jabber:x:data") != 0) {
/* x-data form */
        xmlnode *dataform = xmlnode_copy(child);
        info -> forms = g_list_append((info -> forms),dataform);
      }
    }
  }
  return info;
}

static gint jabber_caps_xdata_field_compare(gconstpointer a,gconstpointer b)
{
  const JabberDataFormField *ac = a;
  const JabberDataFormField *bc = b;
  return strcmp((ac -> var),(bc -> var));
}

static GList *jabber_caps_xdata_get_fields(const xmlnode *x)
{
  GList *fields = (GList *)((void *)0);
  xmlnode *field;
  if (!(x != 0)) 
    return 0;
  for (field = xmlnode_get_child(x,"field"); field != 0; field = xmlnode_get_next_twin(field)) {
    xmlnode *value;
    JabberDataFormField *xdatafield = (JabberDataFormField *)(g_malloc0_n(1,(sizeof(JabberDataFormField ))));
    xdatafield -> var = g_strdup(xmlnode_get_attrib(field,"var"));
    for (value = xmlnode_get_child(field,"value"); value != 0; value = xmlnode_get_next_twin(value)) {
      gchar *val = xmlnode_get_data(value);
      xdatafield -> values = g_list_append((xdatafield -> values),val);
    }
    xdatafield -> values = g_list_sort((xdatafield -> values),((GCompareFunc )strcmp));
    fields = g_list_append(fields,xdatafield);
  }
  fields = g_list_sort(fields,jabber_caps_xdata_field_compare);
  return fields;
}

static void append_escaped_string(PurpleCipherContext *context,const gchar *str)
{
  if ((str != 0) && (( *str) != 0)) {
    char *tmp = g_markup_escape_text(str,(-1));
    purple_cipher_context_append(context,((const guchar *)tmp),strlen(tmp));
    g_free(tmp);
  }
  purple_cipher_context_append(context,((const guchar *)"<"),1);
}

gchar *jabber_caps_calculate_hash(JabberCapsClientInfo *info,const char *hash)
{
  GList *node;
  PurpleCipherContext *context;
  guint8 checksum[20UL];
  gsize checksum_size = 20;
  gboolean success;
  if (!(info != 0) || !((context = purple_cipher_context_new_by_name(hash,0)) != 0)) 
    return 0;
/* sort identities, features and x-data forms */
  info -> identities = g_list_sort((info -> identities),jabber_identity_compare);
  info -> features = g_list_sort((info -> features),((GCompareFunc )strcmp));
  info -> forms = g_list_sort((info -> forms),jabber_xdata_compare);
/* Add identities to the hash data */
  for (node = (info -> identities); node != 0; node = (node -> next)) {
    JabberIdentity *id = (JabberIdentity *)(node -> data);
    char *category = g_markup_escape_text((id -> category),(-1));
    char *type = g_markup_escape_text((id -> type),(-1));
    char *lang = (char *)((void *)0);
    char *name = (char *)((void *)0);
    char *tmp;
    if ((id -> lang) != 0) 
      lang = g_markup_escape_text((id -> lang),(-1));
    if ((id -> name) != 0) 
      name = g_markup_escape_text((id -> name),(-1));
    tmp = g_strconcat(category,"/",type,"/",((lang != 0)?lang : ""),"/",((name != 0)?name : ""),"<",((void *)((void *)0)));
    purple_cipher_context_append(context,((const guchar *)tmp),strlen(tmp));
    g_free(tmp);
    g_free(category);
    g_free(type);
    g_free(lang);
    g_free(name);
  }
/* concat features to the verification string */
  for (node = (info -> features); node != 0; node = (node -> next)) {
    append_escaped_string(context,(node -> data));
  }
/* concat x-data forms to the verification string */
  for (node = (info -> forms); node != 0; node = (node -> next)) {
    xmlnode *data = (xmlnode *)(node -> data);
    gchar *formtype = jabber_x_data_get_formtype(data);
    GList *fields = jabber_caps_xdata_get_fields(data);
/* append FORM_TYPE's field value to the verification string */
    append_escaped_string(context,formtype);
    g_free(formtype);
    while(fields != 0){
      GList *value;
      JabberDataFormField *field = (JabberDataFormField *)(fields -> data);
      if (!(g_str_equal((field -> var),"FORM_TYPE") != 0)) {
/* Append the "var" attribute */
        append_escaped_string(context,(field -> var));
/* Append <value/> elements' cdata */
        for (value = (field -> values); value != 0; value = (value -> next)) {
          append_escaped_string(context,(value -> data));
          g_free((value -> data));
        }
      }
      g_free((field -> var));
      g_list_free((field -> values));
      fields = g_list_delete_link(fields,fields);
    }
  }
/* generate hash */
  success = purple_cipher_context_digest(context,checksum_size,checksum,&checksum_size);
  purple_cipher_context_destroy(context);
  return (success != 0)?purple_base64_encode(checksum,checksum_size) : ((char *)((void *)0));
}

void jabber_caps_calculate_own_hash(JabberStream *js)
{
  JabberCapsClientInfo info;
  GList *iter = 0;
  GList *features = 0;
  if (!(jabber_identities != 0) && !(jabber_features != 0)) {
/* This really shouldn't ever happen */
    purple_debug_warning("jabber","No features or identities, cannot calculate own caps hash.\n");
    g_free((js -> caps_hash));
    js -> caps_hash = ((char *)((void *)0));
    return ;
  }
/* build the currently-supported list of features */
  if (jabber_features != 0) {
    for (iter = jabber_features; iter != 0; iter = (iter -> next)) {
      JabberFeature *feat = (iter -> data);
      if (!((feat -> is_enabled) != 0) || (( *(feat -> is_enabled))(js,(feat -> namespace)) != 0)) {
        features = g_list_append(features,(feat -> namespace));
      }
    }
  }
  info.features = features;
/* TODO: This copy can go away, I think, since jabber_identities
	 * is pre-sorted, so the sort in calculate_hash should be idempotent.
	 * However, I want to test that. --darkrain
	 */
  info.identities = g_list_copy(jabber_identities);
  info.forms = ((GList *)((void *)0));
  g_free((js -> caps_hash));
  js -> caps_hash = jabber_caps_calculate_hash(&info,"sha1");
  g_list_free(info.identities);
  g_list_free(info.features);
}

const gchar *jabber_caps_get_own_hash(JabberStream *js)
{
  if (!((js -> caps_hash) != 0)) 
    jabber_caps_calculate_own_hash(js);
  return (js -> caps_hash);
}

void jabber_caps_broadcast_change()
{
  GList *node;
  GList *accounts = purple_accounts_get_all_active();
  for (node = accounts; node != 0; node = (node -> next)) {
    PurpleAccount *account = (node -> data);
    const char *prpl_id = purple_account_get_protocol_id(account);
    if ((g_str_equal("prpl-jabber",prpl_id) != 0) && (purple_account_is_connected(account) != 0)) {
      PurpleConnection *gc = purple_account_get_connection(account);
      jabber_presence_send((gc -> proto_data),(!0));
    }
  }
  g_list_free(accounts);
}
