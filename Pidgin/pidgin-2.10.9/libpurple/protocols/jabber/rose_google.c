/**
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "debug.h"
#include "google.h"
#include "jabber.h"
#include "chat.h"
/* This does two passes on the string. The first pass goes through
 * and determine if all the structured text is properly balanced, and
 * how many instances of each there is. The second pass goes and converts
 * everything to HTML, depending on what's figured out by the first pass.
 * It will short circuit once it knows it has no more replacements to make
 */

char *jabber_google_format_to_html(const char *text)
{
  const char *p;
/* The start of the screen may be consdiered a space for this purpose */
  gboolean preceding_space = (!0);
  gboolean in_bold = 0;
  gboolean in_italic = 0;
  gboolean in_tag = 0;
  gint bold_count = 0;
  gint italic_count = 0;
  GString *str;
  for (p = text; ( *p) != 0; p = ((char *)(p + g_utf8_skip[ *((const guchar *)p)]))) {
    gunichar c = g_utf8_get_char(p);
    if ((c == '*') && !(in_tag != 0)) {
      if ((in_bold != 0) && (((g_unichar_isspace(p[1]) != 0) || (p[1] == 0)) || (p[1] == 60))) {
        bold_count++;
        in_bold = 0;
      }
      else if (((preceding_space != 0) && !(in_bold != 0)) && !(g_unichar_isspace(p[1]) != 0)) {
        bold_count++;
        in_bold = (!0);
      }
      preceding_space = (!0);
    }
    else if ((c == '_') && !(in_tag != 0)) {
      if ((in_italic != 0) && (((g_unichar_isspace(p[1]) != 0) || (p[1] == 0)) || (p[1] == 60))) {
        italic_count++;
        in_italic = 0;
      }
      else if (((preceding_space != 0) && !(in_italic != 0)) && !(g_unichar_isspace(p[1]) != 0)) {
        italic_count++;
        in_italic = (!0);
      }
      preceding_space = (!0);
    }
    else if ((c == 60) && !(in_tag != 0)) {
      in_tag = (!0);
    }
    else if ((c == '>') && (in_tag != 0)) {
      in_tag = 0;
    }
    else if (!(in_tag != 0)) {
      if (g_unichar_isspace(c) != 0) 
        preceding_space = (!0);
      else 
        preceding_space = 0;
    }
  }
  str = g_string_new(0);
  in_bold = (in_italic = (in_tag = 0));
  preceding_space = (!0);
  for (p = text; ( *p) != 0; p = ((char *)(p + g_utf8_skip[ *((const guchar *)p)]))) {
    gunichar c = g_utf8_get_char(p);
    if ((((bold_count < 2) && (italic_count < 2)) && !(in_bold != 0)) && !(in_italic != 0)) {
      g_string_append(str,p);
      return g_string_free(str,0);
    }
    if ((c == '*') && !(in_tag != 0)) {
      if ((in_bold != 0) && ((g_unichar_isspace(p[1]) != 0) || (p[1] == 60))) 
/* This is safe in UTF-8 */
{
        str = g_string_append(str,"</b>");
        in_bold = 0;
        bold_count--;
      }
      else if (((preceding_space != 0) && (bold_count > 1)) && !(g_unichar_isspace(p[1]) != 0)) {
        str = g_string_append(str,"<b>");
        bold_count--;
        in_bold = (!0);
      }
      else {
        str = g_string_append_unichar(str,c);
      }
      preceding_space = (!0);
    }
    else if ((c == '_') && !(in_tag != 0)) {
      if ((in_italic != 0) && ((g_unichar_isspace(p[1]) != 0) || (p[1] == 60))) {
        str = g_string_append(str,"</i>");
        italic_count--;
        in_italic = 0;
      }
      else if (((preceding_space != 0) && (italic_count > 1)) && !(g_unichar_isspace(p[1]) != 0)) {
        str = g_string_append(str,"<i>");
        italic_count--;
        in_italic = (!0);
      }
      else {
        str = g_string_append_unichar(str,c);
      }
      preceding_space = (!0);
    }
    else if ((c == 60) && !(in_tag != 0)) {
      str = g_string_append_unichar(str,c);
      in_tag = (!0);
    }
    else if ((c == '>') && (in_tag != 0)) {
      str = g_string_append_unichar(str,c);
      in_tag = 0;
    }
    else if (!(in_tag != 0)) {
      str = g_string_append_unichar(str,c);
      if (g_unichar_isspace(c) != 0) 
        preceding_space = (!0);
      else 
        preceding_space = 0;
    }
    else {
      str = g_string_append_unichar(str,c);
    }
  }
  return g_string_free(str,0);
}

void google_buddy_node_chat(PurpleBlistNode *node,gpointer data)
{
  PurpleBuddy *buddy;
  PurpleConnection *gc;
  JabberStream *js;
  JabberChat *chat;
  gchar *room;
  gchar *uuid = purple_uuid_random();
  do {
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_BLIST_NODE_IS_BUDDY(node)");
      return ;
    };
  }while (0);
  buddy = ((PurpleBuddy *)node);
  gc = purple_account_get_connection((purple_buddy_get_account(buddy)));
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  js = (purple_connection_get_protocol_data(gc));
  room = g_strdup_printf("private-chat-%s",uuid);
  chat = jabber_join_chat(js,room,"groupchat.google.com",( *(js -> user)).node,0,0);
  if (chat != 0) {
    chat -> muc = (!0);
    jabber_chat_invite(gc,(chat -> id),"",purple_buddy_get_name(buddy));
  }
  g_free(room);
  g_free(uuid);
}
