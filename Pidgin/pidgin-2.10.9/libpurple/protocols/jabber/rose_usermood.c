/*
 * purple - Jabber Protocol Plugin
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "usermood.h"
#include "pep.h"
#include <string.h>
#include "internal.h"
#include "request.h"
#include "debug.h"
static PurpleMood moods[] = {{("afraid"), ("Afraid"), ((gpointer *)((void *)0))}, {("amazed"), ("Amazed"), ((gpointer *)((void *)0))}, {("amorous"), ("Amorous"), ((gpointer *)((void *)0))}, {("angry"), ("Angry"), ((gpointer *)((void *)0))}, {("annoyed"), ("Annoyed"), ((gpointer *)((void *)0))}, {("anxious"), ("Anxious"), ((gpointer *)((void *)0))}, {("aroused"), ("Aroused"), ((gpointer *)((void *)0))}, {("ashamed"), ("Ashamed"), ((gpointer *)((void *)0))}, {("bored"), ("Bored"), ((gpointer *)((void *)0))}, {("brave"), ("Brave"), ((gpointer *)((void *)0))}, {("calm"), ("Calm"), ((gpointer *)((void *)0))}, {("cautious"), ("Cautious"), ((gpointer *)((void *)0))}, {("cold"), ("Cold"), ((gpointer *)((void *)0))}, {("confident"), ("Confident"), ((gpointer *)((void *)0))}, {("confused"), ("Confused"), ((gpointer *)((void *)0))}, {("contemplative"), ("Contemplative"), ((gpointer *)((void *)0))}, {("contented"), ("Contented"), ((gpointer *)((void *)0))}, {("cranky"), ("Cranky"), ((gpointer *)((void *)0))}, {("crazy"), ("Crazy"), ((gpointer *)((void *)0))}, {("creative"), ("Creative"), ((gpointer *)((void *)0))}, {("curious"), ("Curious"), ((gpointer *)((void *)0))}, {("dejected"), ("Dejected"), ((gpointer *)((void *)0))}, {("depressed"), ("Depressed"), ((gpointer *)((void *)0))}, {("disappointed"), ("Disappointed"), ((gpointer *)((void *)0))}, {("disgusted"), ("Disgusted"), ((gpointer *)((void *)0))}, {("dismayed"), ("Dismayed"), ((gpointer *)((void *)0))}, {("distracted"), ("Distracted"), ((gpointer *)((void *)0))}, {("embarrassed"), ("Embarrassed"), ((gpointer *)((void *)0))}, {("envious"), ("Envious"), ((gpointer *)((void *)0))}, {("excited"), ("Excited"), ((gpointer *)((void *)0))}, {("flirtatious"), ("Flirtatious"), ((gpointer *)((void *)0))}, {("frustrated"), ("Frustrated"), ((gpointer *)((void *)0))}, {("grateful"), ("Grateful"), ((gpointer *)((void *)0))}, {("grieving"), ("Grieving"), ((gpointer *)((void *)0))}, {("grumpy"), ("Grumpy"), ((gpointer *)((void *)0))}, {("guilty"), ("Guilty"), ((gpointer *)((void *)0))}, {("happy"), ("Happy"), ((gpointer *)((void *)0))}, {("hopeful"), ("Hopeful"), ((gpointer *)((void *)0))}, {("hot"), ("Hot"), ((gpointer *)((void *)0))}, {("humbled"), ("Humbled"), ((gpointer *)((void *)0))}, {("humiliated"), ("Humiliated"), ((gpointer *)((void *)0))}, {("hungry"), ("Hungry"), ((gpointer *)((void *)0))}, {("hurt"), ("Hurt"), ((gpointer *)((void *)0))}, {("impressed"), ("Impressed"), ((gpointer *)((void *)0))}, {("in_awe"), ("In awe"), ((gpointer *)((void *)0))}, {("in_love"), ("In love"), ((gpointer *)((void *)0))}, {("indignant"), ("Indignant"), ((gpointer *)((void *)0))}, {("interested"), ("Interested"), ((gpointer *)((void *)0))}, {("intoxicated"), ("Intoxicated"), ((gpointer *)((void *)0))}, {("invincible"), ("Invincible"), ((gpointer *)((void *)0))}, {("jealous"), ("Jealous"), ((gpointer *)((void *)0))}, {("lonely"), ("Lonely"), ((gpointer *)((void *)0))}, {("lost"), ("Lost"), ((gpointer *)((void *)0))}, {("lucky"), ("Lucky"), ((gpointer *)((void *)0))}, {("mean"), ("Mean"), ((gpointer *)((void *)0))}, {("moody"), ("Moody"), ((gpointer *)((void *)0))}, {("nervous"), ("Nervous"), ((gpointer *)((void *)0))}, {("neutral"), ("Neutral"), ((gpointer *)((void *)0))}, {("offended"), ("Offended"), ((gpointer *)((void *)0))}, {("outraged"), ("Outraged"), ((gpointer *)((void *)0))}, {("playful"), ("Playful"), ((gpointer *)((void *)0))}, {("proud"), ("Proud"), ((gpointer *)((void *)0))}, {("relaxed"), ("Relaxed"), ((gpointer *)((void *)0))}, {("relieved"), ("Relieved"), ((gpointer *)((void *)0))}, {("remorseful"), ("Remorseful"), ((gpointer *)((void *)0))}, {("restless"), ("Restless"), ((gpointer *)((void *)0))}, {("sad"), ("Sad"), ((gpointer *)((void *)0))}, {("sarcastic"), ("Sarcastic"), ((gpointer *)((void *)0))}, {("satisfied"), ("Satisfied"), ((gpointer *)((void *)0))}, {("serious"), ("Serious"), ((gpointer *)((void *)0))}, {("shocked"), ("Shocked"), ((gpointer *)((void *)0))}, {("shy"), ("Shy"), ((gpointer *)((void *)0))}, {("sick"), ("Sick"), ((gpointer *)((void *)0))}, {("sleepy"), ("Sleepy"), ((gpointer *)((void *)0))}, {("spontaneous"), ("Spontaneous"), ((gpointer *)((void *)0))}, {("stressed"), ("Stressed"), ((gpointer *)((void *)0))}, {("strong"), ("Strong"), ((gpointer *)((void *)0))}, {("surprised"), ("Surprised"), ((gpointer *)((void *)0))}, {("thankful"), ("Thankful"), ((gpointer *)((void *)0))}, {("thirsty"), ("Thirsty"), ((gpointer *)((void *)0))}, {("tired"), ("Tired"), ((gpointer *)((void *)0))}, {("undefined"), ("Undefined"), ((gpointer *)((void *)0))}, {("weak"), ("Weak"), ((gpointer *)((void *)0))}, {("worried"), ("Worried"), ((gpointer *)((void *)0))}, 
/* Mark the last record. */
{((const char *)((void *)0)), ((const char *)((void *)0)), ((gpointer *)((void *)0))}};

static void jabber_mood_cb(JabberStream *js,const char *from,xmlnode *items)
{
/* it doesn't make sense to have more than one item here, so let's just pick the first one */
  xmlnode *item = xmlnode_get_child(items,"item");
  const char *newmood = (const char *)((void *)0);
  char *moodtext = (char *)((void *)0);
  JabberBuddy *buddy = jabber_buddy_find(js,from,0);
  xmlnode *moodinfo;
  xmlnode *mood;
/* ignore the mood of people not on our buddy list */
  if (!(buddy != 0) || !(item != 0)) 
    return ;
  mood = xmlnode_get_child_with_namespace(item,"mood","http://jabber.org/protocol/mood");
  if (!(mood != 0)) 
    return ;
{
    for (moodinfo = (mood -> child); moodinfo != 0; moodinfo = (moodinfo -> next)) {
      if ((moodinfo -> type) == XMLNODE_TYPE_TAG) {
        if (!(strcmp((moodinfo -> name),"text") != 0)) {
/* only pick the first one */
          if (!(moodtext != 0)) 
            moodtext = xmlnode_get_data(moodinfo);
        }
        else {
          int i;
{
            for (i = 0; moods[i].mood != 0; ++i) {
/* verify that the mood is known (valid) */
              if (!(strcmp((moodinfo -> name),moods[i].mood) != 0)) {
                newmood = moods[i].mood;
                break; 
              }
            }
          }
        }
      }
      if ((newmood != ((const char *)((void *)0))) && (moodtext != ((char *)((void *)0)))) 
        break; 
    }
  }
  if (newmood != ((const char *)((void *)0))) {
    purple_prpl_got_user_status(( *(js -> gc)).account,from,"mood","mood",newmood,"moodtext",moodtext,((void *)((void *)0)));
  }
  else {
    purple_prpl_got_user_status_deactive(( *(js -> gc)).account,from,"mood");
  }
  g_free(moodtext);
}

void jabber_mood_init()
{
  jabber_add_feature("http://jabber.org/protocol/mood",jabber_pep_namespace_only_when_pep_enabled_cb);
  jabber_pep_register_handler("http://jabber.org/protocol/mood",jabber_mood_cb);
}

void jabber_mood_set(JabberStream *js,const char *mood,const char *text)
{
  xmlnode *publish;
  xmlnode *moodnode;
  publish = xmlnode_new("publish");
  xmlnode_set_attrib(publish,"node","http://jabber.org/protocol/mood");
  moodnode = xmlnode_new_child(xmlnode_new_child(publish,"item"),"mood");
  xmlnode_set_namespace(moodnode,"http://jabber.org/protocol/mood");
  if ((mood != 0) && (( *mood) != 0)) {
/* if mood is NULL, set an empty mood node, meaning: unset mood */
    xmlnode_new_child(moodnode,mood);
  }
  if ((text != 0) && (( *text) != 0)) {
    xmlnode *textnode = xmlnode_new_child(moodnode,"text");
    xmlnode_insert_data(textnode,text,(-1));
  }
  jabber_pep_publish(js,publish);
/* publish is freed by jabber_pep_publish -> jabber_iq_send -> jabber_iq_free
	   (yay for well-defined memory management rules) */
}

PurpleMood *jabber_get_moods(PurpleAccount *account)
{
  return moods;
}
