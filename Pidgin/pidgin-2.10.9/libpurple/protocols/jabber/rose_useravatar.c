/*
 * purple - Jabber Protocol Plugin
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "useravatar.h"
#include "pep.h"
#include "debug.h"
#define MAX_HTTP_BUDDYICON_BYTES (200 * 1024)
static void update_buddy_metadata(JabberStream *js,const char *from,xmlnode *items);

void jabber_avatar_init()
{
  jabber_add_feature("urn:xmpp:avatar:metadata",jabber_pep_namespace_only_when_pep_enabled_cb);
  jabber_add_feature("urn:xmpp:avatar:data",jabber_pep_namespace_only_when_pep_enabled_cb);
  jabber_pep_register_handler("urn:xmpp:avatar:metadata",update_buddy_metadata);
}

static void remove_avatar_0_12_nodes(JabberStream *js)
{
#if 0
/* See note below for why this is #if 0'd */
/* Publish an empty avatar according to the XEP-0084 v0.12 semantics */
/* publish the metadata */
/* publish */
#endif
/*
	 * This causes ejabberd 2.0.0 to kill the connection unceremoniously.
	 * See https://support.process-one.net/browse/EJAB-623. When adiumx.com
	 * was upgraded, the issue went away.
	 *
	 * I think it makes a lot of sense to not have an avatar at the old
	 * node instead of having something interpreted as "no avatar". When
	 * a contact with an older client logs in, in the latter situation,
	 * there's a race between interpreting the <presence/> vcard-temp:x:update
	 * avatar (non-empty) and the XEP-0084 v0.12 avatar (empty, so show no
	 * avatar for the buddy) which leads to unhappy and confused users.
	 *
	 * A deluge of frustrating "Read error" bug reports may change my mind
	 * about this.
	 * --darkrain42
	 */
  jabber_pep_delete_node(js,"http://www.xmpp.org/extensions/xep-0084.html#ns-metadata");
  jabber_pep_delete_node(js,"http://www.xmpp.org/extensions/xep-0084.html#ns-data");
}

void jabber_avatar_set(JabberStream *js,PurpleStoredImage *img)
{
  xmlnode *publish;
  xmlnode *metadata;
  xmlnode *item;
  if (!((js -> pep) != 0)) 
    return ;
/* Hmmm, not sure if this is worth the traffic, but meh */
  remove_avatar_0_12_nodes(js);
  if (!(img != 0)) {
    publish = xmlnode_new("publish");
    xmlnode_set_attrib(publish,"node","urn:xmpp:avatar:metadata");
    item = xmlnode_new_child(publish,"item");
    metadata = xmlnode_new_child(item,"metadata");
    xmlnode_set_namespace(metadata,"urn:xmpp:avatar:metadata");
/* publish */
    jabber_pep_publish(js,publish);
  }
  else {
/*
		 * TODO: This is pretty gross.  The Jabber PRPL really shouldn't
		 *       do voodoo to try to determine the image type, height
		 *       and width.
		 */
/* A PNG header, including the IHDR, but nothing else */
/* ATTN: this is in network byte order! */
    const struct __unnamed_class___F0_L117_C9_L579R_variable_declaration__variable_type__Ab_gucharUc__typedef_declaration_index_8_Ae__variable_name_L579R__scope__signature__DELIMITER__L579R_variable_declaration__variable_type__variable_name_L579R__scope__ihdr {
/* must be hex 89 50 4E 47 0D 0A 1A 0A */
    guchar (signature)[8UL];
    struct __unnamed_class___F0_L119_C4_L580R_variable_declaration__variable_type_guint32Ui__typedef_declaration_variable_name_L580R__scope__length__DELIMITER__L580R_variable_declaration__variable_type__Ab_gucharUc__typedef_declaration_index_4_Ae__variable_name_L580R__scope__type__DELIMITER__L580R_variable_declaration__variable_type_guint32Ui__typedef_declaration_variable_name_L580R__scope__width__DELIMITER__L580R_variable_declaration__variable_type_guint32Ui__typedef_declaration_variable_name_L580R__scope__height__DELIMITER__L580R_variable_declaration__variable_type_gucharUc__typedef_declaration_variable_name_L580R__scope__bitdepth__DELIMITER__L580R_variable_declaration__variable_type_gucharUc__typedef_declaration_variable_name_L580R__scope__colortype__DELIMITER__L580R_variable_declaration__variable_type_gucharUc__typedef_declaration_variable_name_L580R__scope__compression__DELIMITER__L580R_variable_declaration__variable_type_gucharUc__typedef_declaration_variable_name_L580R__scope__filter__DELIMITER__L580R_variable_declaration__variable_type_gucharUc__typedef_declaration_variable_name_L580R__scope__interlace {
/* must be 0x0d */
    guint32 length;
/* must be 'I' 'H' 'D' 'R' */
    guchar (type)[4UL];
    guint32 width;
    guint32 height;
    guchar bitdepth;
    guchar colortype;
    guchar compression;
    guchar filter;
    guchar interlace;}ihdr;}*png = (const struct __unnamed_class___F0_L117_C9_L579R_variable_declaration__variable_type__Ab_gucharUc__typedef_declaration_index_8_Ae__variable_name_L579R__scope__signature__DELIMITER__L579R_variable_declaration__variable_type__variable_name_L579R__scope__ihdr *)((void *)0);
    if ((purple_imgstore_get_size(img)) > sizeof(( *png))) 
      png = (purple_imgstore_get_data(img));
/* check if the data is a valid png file (well, at least to some extent) */
    if ((((((((((((((png != 0) && ((png -> signature)[0] == 0x89)) && ((png -> signature)[1] == 0x50)) && ((png -> signature)[2] == 0x4e)) && ((png -> signature)[3] == 0x47)) && ((png -> signature)[4] == 13)) && ((png -> signature)[5] == 10)) && ((png -> signature)[6] == 0x1a)) && ((png -> signature)[7] == 10)) && (ntohl(png -> ihdr.length) == 13)) && (png -> ihdr.type[0] == 'I')) && (png -> ihdr.type[1] == 'H')) && (png -> ihdr.type[2] == 'D')) && (png -> ihdr.type[3] == 'R')) {
/* parse PNG header to get the size of the image (yes, this is required) */
      guint32 width = ntohl(png -> ihdr.width);
      guint32 height = ntohl(png -> ihdr.height);
      xmlnode *data;
      xmlnode *info;
      char *lengthstring;
      char *widthstring;
      char *heightstring;
/* compute the sha1 hash */
      char *hash = jabber_calculate_data_hash(purple_imgstore_get_data(img),purple_imgstore_get_size(img),"sha1");
      char *base64avatar = purple_base64_encode((purple_imgstore_get_data(img)),purple_imgstore_get_size(img));
      publish = xmlnode_new("publish");
      xmlnode_set_attrib(publish,"node","urn:xmpp:avatar:data");
      item = xmlnode_new_child(publish,"item");
      xmlnode_set_attrib(item,"id",hash);
      data = xmlnode_new_child(item,"data");
      xmlnode_set_namespace(data,"urn:xmpp:avatar:data");
      xmlnode_insert_data(data,base64avatar,(-1));
/* publish the avatar itself */
      jabber_pep_publish(js,publish);
      g_free(base64avatar);
      lengthstring = g_strdup_printf("%lu",purple_imgstore_get_size(img));
      widthstring = g_strdup_printf("%u",width);
      heightstring = g_strdup_printf("%u",height);
/* publish the metadata */
      publish = xmlnode_new("publish");
      xmlnode_set_attrib(publish,"node","urn:xmpp:avatar:metadata");
      item = xmlnode_new_child(publish,"item");
      xmlnode_set_attrib(item,"id",hash);
      metadata = xmlnode_new_child(item,"metadata");
      xmlnode_set_namespace(metadata,"urn:xmpp:avatar:metadata");
      info = xmlnode_new_child(metadata,"info");
      xmlnode_set_attrib(info,"id",hash);
      xmlnode_set_attrib(info,"type","image/png");
      xmlnode_set_attrib(info,"bytes",lengthstring);
      xmlnode_set_attrib(info,"width",widthstring);
      xmlnode_set_attrib(info,"height",heightstring);
      jabber_pep_publish(js,publish);
      g_free(lengthstring);
      g_free(widthstring);
      g_free(heightstring);
      g_free(hash);
    }
    else {
      purple_debug_error("jabber","Cannot set PEP avatar to non-PNG data\n");
    }
  }
}

static void do_got_own_avatar_0_12_cb(JabberStream *js,const char *from,xmlnode *items)
{
  if (items != 0) 
/* It wasn't an error (i.e. 'item-not-found') */
    remove_avatar_0_12_nodes(js);
}

static void do_got_own_avatar_cb(JabberStream *js,const char *from,xmlnode *items)
{
  xmlnode *item = (xmlnode *)((void *)0);
  xmlnode *metadata = (xmlnode *)((void *)0);
  xmlnode *info = (xmlnode *)((void *)0);
  PurpleAccount *account = purple_connection_get_account((js -> gc));
  const char *server_hash = (const char *)((void *)0);
  if ((((items != 0) && ((item = xmlnode_get_child(items,"item")) != 0)) && ((metadata = xmlnode_get_child(item,"metadata")) != 0)) && ((info = xmlnode_get_child(metadata,"info")) != 0)) {
    server_hash = xmlnode_get_attrib(info,"id");
  }
/*
	 * If we have an avatar and the server returned an error/malformed data,
	 * push our avatar. If the server avatar doesn't match the local one, push
	 * our avatar.
	 */
  if ((!(items != 0) || !(metadata != 0)) || !(purple_strequal(server_hash,(js -> initial_avatar_hash)) != 0)) {
    PurpleStoredImage *img = purple_buddy_icons_find_account_icon(account);
    jabber_avatar_set(js,img);
    purple_imgstore_unref(img);
  }
}

void jabber_avatar_fetch_mine(JabberStream *js)
{
  if ((js -> initial_avatar_hash) != 0) {
    jabber_pep_request_item(js,0,"http://www.xmpp.org/extensions/xep-0084.html#ns-metadata",0,do_got_own_avatar_0_12_cb);
    jabber_pep_request_item(js,0,"urn:xmpp:avatar:metadata",0,do_got_own_avatar_cb);
  }
}
typedef struct _JabberBuddyAvatarUpdateURLInfo {
JabberStream *js;
char *from;
char *id;}JabberBuddyAvatarUpdateURLInfo;

static void do_buddy_avatar_update_fromurl(PurpleUtilFetchUrlData *url_data,gpointer user_data,const gchar *url_text,gsize len,const gchar *error_message)
{
  JabberBuddyAvatarUpdateURLInfo *info = user_data;
  gpointer icon_data;
  if (!(url_text != 0)) {
    purple_debug_error("jabber","do_buddy_avatar_update_fromurl got error \"%s\"",error_message);
    goto out;
  }
  icon_data = g_memdup(url_text,len);
  purple_buddy_icons_set_for_user(purple_connection_get_account(( *(info -> js)).gc),(info -> from),icon_data,len,(info -> id));
  out:
  g_free((info -> from));
  g_free((info -> id));
  g_free(info);
}

static void do_buddy_avatar_update_data(JabberStream *js,const char *from,xmlnode *items)
{
  xmlnode *item;
  xmlnode *data;
  const char *checksum;
  char *b64data;
  void *img;
  size_t size;
  if (!(items != 0)) 
    return ;
  item = xmlnode_get_child(items,"item");
  if (!(item != 0)) 
    return ;
  data = xmlnode_get_child(item,"data");
  if (!(data != 0)) 
    return ;
  checksum = xmlnode_get_attrib(item,"id");
  if (!(checksum != 0)) 
    return ;
  b64data = xmlnode_get_data(data);
  if (!(b64data != 0)) 
    return ;
  img = (purple_base64_decode(b64data,&size));
  if (!(img != 0)) {
    g_free(b64data);
    return ;
  }
  purple_buddy_icons_set_for_user(purple_connection_get_account((js -> gc)),from,img,size,checksum);
  g_free(b64data);
}

static void update_buddy_metadata(JabberStream *js,const char *from,xmlnode *items)
{
  PurpleBuddy *buddy = purple_find_buddy(purple_connection_get_account((js -> gc)),from);
  const char *checksum;
  xmlnode *item;
  xmlnode *metadata;
  if (!(buddy != 0)) 
    return ;
  if (!(items != 0)) 
    return ;
  item = xmlnode_get_child(items,"item");
  if (!(item != 0)) 
    return ;
  metadata = xmlnode_get_child(item,"metadata");
  if (!(metadata != 0)) 
    return ;
  checksum = purple_buddy_icons_get_checksum_for_user(buddy);
/* <stop/> was the pre-v1.1 method of publishing an empty avatar */
  if (xmlnode_get_child(metadata,"stop") != 0) {
    purple_buddy_icons_set_for_user(purple_connection_get_account((js -> gc)),from,0,0,0);
  }
  else {
    xmlnode *info;
    xmlnode *goodinfo = (xmlnode *)((void *)0);
    gboolean has_children = 0;
{
/* iterate over all info nodes to get one we can use */
      for (info = (metadata -> child); info != 0; info = (info -> next)) {
        if ((info -> type) == XMLNODE_TYPE_TAG) 
          has_children = (!0);
        if (((info -> type) == XMLNODE_TYPE_TAG) && !(strcmp((info -> name),"info") != 0)) {
          const char *type = xmlnode_get_attrib(info,"type");
          const char *id = xmlnode_get_attrib(info,"id");
          if (((checksum != 0) && (id != 0)) && !(strcmp(id,checksum) != 0)) {
/* we already have that avatar, so we don't have to do anything */
            goodinfo = ((xmlnode *)((void *)0));
            break; 
          }
/* We'll only pick the png one for now. It's a very nice image format anyways. */
          if ((((type != 0) && (id != 0)) && !(goodinfo != 0)) && !(strcmp(type,"image/png") != 0)) 
            goodinfo = info;
        }
      }
    }
    if (has_children == 0) {
      purple_buddy_icons_set_for_user(purple_connection_get_account((js -> gc)),from,0,0,0);
    }
    else if (goodinfo != 0) {
      const char *url = xmlnode_get_attrib(goodinfo,"url");
      const char *id = xmlnode_get_attrib(goodinfo,"id");
/* the avatar might either be stored in a pep node, or on a HTTP(S) URL */
      if (!(url != 0)) {
        jabber_pep_request_item(js,from,"urn:xmpp:avatar:data",id,do_buddy_avatar_update_data);
      }
      else {
        PurpleUtilFetchUrlData *url_data;
        JabberBuddyAvatarUpdateURLInfo *info = (JabberBuddyAvatarUpdateURLInfo *)(g_malloc0_n(1,(sizeof(JabberBuddyAvatarUpdateURLInfo ))));
        info -> js = js;
        url_data = purple_util_fetch_url_request_len(url,(!0),0,(!0),0,0,(200 * 1024),do_buddy_avatar_update_fromurl,info);;
        if (url_data != 0) {
          info -> from = g_strdup(from);
          info -> id = g_strdup(id);
          js -> url_datas = g_slist_prepend((js -> url_datas),url_data);
        }
        else 
          g_free(info);
      }
    }
  }
}
