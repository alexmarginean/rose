/*
 * purple - Jabber Protocol Plugin
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "debug.h"
#include "cipher.h"
#include "util.h"
#include "xmlnode.h"
#include "auth_digest_md5.h"
#include "auth.h"
#include "jabber.h"

static JabberSaslState digest_md5_start(JabberStream *js,xmlnode *packet,xmlnode **response,char **error)
{
  xmlnode *auth = xmlnode_new("auth");
  xmlnode_set_namespace(auth,"urn:ietf:params:xml:ns:xmpp-sasl");
  xmlnode_set_attrib(auth,"mechanism","DIGEST-MD5");
   *response = auth;
  return JABBER_SASL_STATE_CONTINUE;
}
/* Parts of this algorithm are inspired by stuff in libgsasl */

GHashTable *jabber_auth_digest_md5_parse(const char *challenge)
{
  const char *token_start;
  const char *val_start;
  const char *val_end;
  const char *cur;
  GHashTable *ret = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
  cur = challenge;
  while(( *cur) != 0){
/* Find the end of the token */
    gboolean in_quotes = 0;
    char *name;
    char *value = (char *)((void *)0);
    token_start = cur;
    while((( *cur) != 0) && ((in_quotes != 0) || (!(in_quotes != 0) && (( *cur) != ',')))){
      if (( *cur) == '"') 
        in_quotes = !(in_quotes != 0);
      cur++;
    }
/* Find start of value.  */
    val_start = (strchr(token_start,'='));
    if ((val_start == ((const char *)((void *)0))) || (val_start > cur)) 
      val_start = cur;
    if (token_start != val_start) {
      name = g_strndup(token_start,(val_start - token_start));
      if (val_start != cur) {
        val_start++;
        while((val_start != cur) && (((((( *val_start) == 32) || (( *val_start) == 9)) || (( *val_start) == 13)) || (( *val_start) == 10)) || (( *val_start) == '"')))
          val_start++;
        val_end = cur;
        while((val_end >= val_start) && (((((((( *val_end) == 32) || (( *val_end) == ',')) || (( *val_end) == 9)) || (( *val_end) == 13)) || (( *val_end) == 10)) || (( *val_end) == '"')) || (( *val_end) == 0)))
          val_end--;
        if (((val_end - val_start) + 1) >= 0) 
          value = g_strndup(val_start,((val_end - val_start) + 1));
      }
      g_hash_table_replace(ret,name,value);
    }
/* Find the start of the next token, if there is one */
    if (( *cur) != 0) {
      cur++;
      while(((((( *cur) == 32) || (( *cur) == ',')) || (( *cur) == 9)) || (( *cur) == 13)) || (( *cur) == 10))
        cur++;
    }
  }
  return ret;
}

static char *generate_response_value(JabberID *jid,const char *passwd,const char *nonce,const char *cnonce,const char *a2,const char *realm)
{
  PurpleCipher *cipher;
  PurpleCipherContext *context;
  guchar result[16UL];
  size_t a1len;
  gchar *a1;
  gchar *convnode = (gchar *)((void *)0);
  gchar *convpasswd = (gchar *)((void *)0);
  gchar *ha1;
  gchar *ha2;
  gchar *kd;
  gchar *x;
  gchar *z;
  if ((convnode = g_convert((jid -> node),(-1),"iso-8859-1","utf-8",0,0,0)) == ((gchar *)((void *)0))) {
    convnode = g_strdup((jid -> node));
  }
  if ((passwd != 0) && ((convpasswd = g_convert(passwd,(-1),"iso-8859-1","utf-8",0,0,0)) == ((gchar *)((void *)0)))) {
    convpasswd = g_strdup(passwd);
  }
  cipher = purple_ciphers_find_cipher("md5");
  context = purple_cipher_context_new(cipher,0);
  x = g_strdup_printf("%s:%s:%s",convnode,realm,((convpasswd != 0)?convpasswd : ""));
  purple_cipher_context_append(context,((const guchar *)x),strlen(x));
  purple_cipher_context_digest(context,(sizeof(result)),result,0);
  a1 = g_strdup_printf("xxxxxxxxxxxxxxxx:%s:%s",nonce,cnonce);
  a1len = strlen(a1);
  do {
    memmove(a1,result,16);
  }while (0);
  purple_cipher_context_reset(context,0);
  purple_cipher_context_append(context,((const guchar *)a1),a1len);
  purple_cipher_context_digest(context,(sizeof(result)),result,0);
  ha1 = purple_base16_encode(result,16);
  purple_cipher_context_reset(context,0);
  purple_cipher_context_append(context,((const guchar *)a2),strlen(a2));
  purple_cipher_context_digest(context,(sizeof(result)),result,0);
  ha2 = purple_base16_encode(result,16);
  kd = g_strdup_printf("%s:%s:00000001:%s:auth:%s",ha1,nonce,cnonce,ha2);
  purple_cipher_context_reset(context,0);
  purple_cipher_context_append(context,((const guchar *)kd),strlen(kd));
  purple_cipher_context_digest(context,(sizeof(result)),result,0);
  purple_cipher_context_destroy(context);
  z = purple_base16_encode(result,16);
  g_free(convnode);
  g_free(convpasswd);
  g_free(x);
  g_free(a1);
  g_free(ha1);
  g_free(ha2);
  g_free(kd);
  return z;
}

static JabberSaslState digest_md5_handle_challenge(JabberStream *js,xmlnode *packet,xmlnode **response,char **msg)
{
  xmlnode *reply = (xmlnode *)((void *)0);
  char *enc_in = xmlnode_get_data(packet);
  char *dec_in;
  char *enc_out;
  GHashTable *parts;
  JabberSaslState state = JABBER_SASL_STATE_CONTINUE;
  if (!(enc_in != 0)) {
     *msg = g_strdup(((const char *)(dgettext("pidgin","Invalid response from server"))));
    return JABBER_SASL_STATE_FAIL;
  }
  dec_in = ((char *)(purple_base64_decode(enc_in,0)));
  purple_debug_misc("jabber","decoded challenge (%lu): %s\n",strlen(dec_in),dec_in);
  parts = jabber_auth_digest_md5_parse(dec_in);
  if (g_hash_table_lookup(parts,"rspauth") != 0) {
    char *rspauth = (g_hash_table_lookup(parts,"rspauth"));
    char *expected_rspauth = (js -> auth_mech_data);
    if ((rspauth != 0) && (purple_strequal(rspauth,expected_rspauth) != 0)) {
      reply = xmlnode_new("response");
      xmlnode_set_namespace(reply,"urn:ietf:params:xml:ns:xmpp-sasl");
    }
    else {
       *msg = g_strdup(((const char *)(dgettext("pidgin","Invalid challenge from server"))));
      state = JABBER_SASL_STATE_FAIL;
    }
    g_free((js -> auth_mech_data));
    js -> auth_mech_data = ((gpointer )((void *)0));
  }
  else {
/* assemble a response, and send it */
/* see RFC 2831 */
    char *realm;
    char *nonce;
/* Make sure the auth string contains everything that should be there.
		   This isn't everything in RFC2831, but it is what we need. */
    nonce = (g_hash_table_lookup(parts,"nonce"));
/* we're actually supposed to prompt the user for a realm if
		 * the server doesn't send one, but that really complicates things,
		 * so i'm not gonna worry about it until is poses a problem to
		 * someone, or I get really bored */
    realm = (g_hash_table_lookup(parts,"realm"));
    if (!(realm != 0)) 
      realm = ( *(js -> user)).domain;
    if ((nonce == ((char *)((void *)0))) || (realm == ((char *)((void *)0)))) {
       *msg = g_strdup(((const char *)(dgettext("pidgin","Invalid challenge from server"))));
      state = JABBER_SASL_STATE_FAIL;
    }
    else {
      GString *response = g_string_new("");
      char *a2;
      char *auth_resp;
      char *cnonce;
      cnonce = g_strdup_printf("%x%u%x",g_random_int(),((int )(time(0))),g_random_int());
      a2 = g_strdup_printf("AUTHENTICATE:xmpp/%s",realm);
      auth_resp = generate_response_value((js -> user),purple_connection_get_password((js -> gc)),nonce,cnonce,a2,realm);
      g_free(a2);
      a2 = g_strdup_printf(":xmpp/%s",realm);
      js -> auth_mech_data = (generate_response_value((js -> user),purple_connection_get_password((js -> gc)),nonce,cnonce,a2,realm));
      g_free(a2);
      g_string_append_printf(response,"username=\"%s\"",( *(js -> user)).node);
      g_string_append_printf(response,",realm=\"%s\"",realm);
      g_string_append_printf(response,",nonce=\"%s\"",nonce);
      g_string_append_printf(response,",cnonce=\"%s\"",cnonce);
      g_string_append_printf(response,",nc=00000001");
      g_string_append_printf(response,",qop=auth");
      g_string_append_printf(response,",digest-uri=\"xmpp/%s\"",realm);
      g_string_append_printf(response,",response=%s",auth_resp);
      g_string_append_printf(response,",charset=utf-8");
      g_free(auth_resp);
      g_free(cnonce);
      enc_out = purple_base64_encode(((guchar *)(response -> str)),(response -> len));
      purple_debug_misc("jabber","decoded response (%lu): %s\n",(response -> len),(response -> str));
      reply = xmlnode_new("response");
      xmlnode_set_namespace(reply,"urn:ietf:params:xml:ns:xmpp-sasl");
      xmlnode_insert_data(reply,enc_out,(-1));
      g_free(enc_out);
      g_string_free(response,(!0));
    }
  }
  g_free(enc_in);
  g_free(dec_in);
  g_hash_table_destroy(parts);
   *response = reply;
  return state;
}

static void digest_md5_dispose(JabberStream *js)
{
  g_free((js -> auth_mech_data));
  js -> auth_mech_data = ((gpointer )((void *)0));
}
static JabberSaslMech digest_md5_mech = {(10), ("DIGEST-MD5"), (digest_md5_start), (digest_md5_handle_challenge), ((JabberSaslState (*)(JabberStream *, xmlnode *, char **))((void *)0)), ((JabberSaslState (*)(JabberStream *, xmlnode *, xmlnode **, char **))((void *)0)), (digest_md5_dispose)
/* priority */
/* name */
/* handle_success */
/* handle_failure */
};

JabberSaslMech *jabber_auth_get_digest_md5_mech()
{
  return &digest_md5_mech;
}
