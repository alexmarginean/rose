/*
 * purple - Jabber Protocol Plugin
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#include "internal.h"
#include "adhoccommands.h"
#include <string.h>
#include "internal.h"
#include "xdata.h"
#include "iq.h"
#include "request.h"

static void do_adhoc_ignoreme(JabberStream *js,... )
{
/* we don't have to do anything */
}
typedef struct _JabberAdHocActionInfo {
char *sessionid;
char *who;
char *node;
GList *actionslist;}JabberAdHocActionInfo;

static void jabber_adhoc_got_buddy_list(JabberStream *js,const char *from,xmlnode *query)
{
  JabberID *jid;
  JabberBuddy *jb;
  JabberBuddyResource *jbr = (JabberBuddyResource *)((void *)0);
  xmlnode *item;
  if ((jid = jabber_id_new(from)) != 0) {
    if (((jid -> resource) != 0) && ((jb = jabber_buddy_find(js,from,(!0))) != 0)) 
      jbr = jabber_buddy_find_resource(jb,(jid -> resource));
    jabber_id_free(jid);
  }
  if (!(jbr != 0)) 
    return ;
  if ((jbr -> commands) != 0) {
/* since the list we just received is complete, wipe the old one */
    while((jbr -> commands) != 0){
      JabberAdHocCommands *cmd = ( *(jbr -> commands)).data;
      g_free((cmd -> jid));
      g_free((cmd -> node));
      g_free((cmd -> name));
      g_free(cmd);
      jbr -> commands = g_list_delete_link((jbr -> commands),(jbr -> commands));
    }
  }
  for (item = (query -> child); item != 0; item = (item -> next)) {{
      JabberAdHocCommands *cmd;
      if ((item -> type) != XMLNODE_TYPE_TAG) 
        continue; 
      if (strcmp((item -> name),"item") != 0) 
        continue; 
      cmd = ((JabberAdHocCommands *)(g_malloc0_n(1,(sizeof(JabberAdHocCommands )))));
      cmd -> jid = g_strdup(xmlnode_get_attrib(item,"jid"));
      cmd -> node = g_strdup(xmlnode_get_attrib(item,"node"));
      cmd -> name = g_strdup(xmlnode_get_attrib(item,"name"));
      jbr -> commands = g_list_append((jbr -> commands),cmd);
    }
  }
}

void jabber_adhoc_disco_result_cb(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  xmlnode *query;
  const char *node;
  if (type == JABBER_IQ_ERROR) 
    return ;
  query = xmlnode_get_child_with_namespace(packet,"query","http://jabber.org/protocol/disco#items");
  if (!(query != 0)) 
    return ;
  node = xmlnode_get_attrib(query,"node");
  if (!(purple_strequal(node,"http://jabber.org/protocol/commands") != 0)) 
    return ;
  jabber_adhoc_got_buddy_list(js,from,query);
}
static void jabber_adhoc_parse(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data);

static void do_adhoc_action_cb(JabberStream *js,xmlnode *result,const char *actionhandle,gpointer user_data)
{
  xmlnode *command;
  GList *action;
  JabberAdHocActionInfo *actionInfo = user_data;
  JabberIq *iq = jabber_iq_new(js,JABBER_IQ_SET);
  jabber_iq_set_callback(iq,jabber_adhoc_parse,0);
  xmlnode_set_attrib((iq -> node),"to",(actionInfo -> who));
  command = xmlnode_new_child((iq -> node),"command");
  xmlnode_set_namespace(command,"http://jabber.org/protocol/commands");
  xmlnode_set_attrib(command,"sessionid",(actionInfo -> sessionid));
  xmlnode_set_attrib(command,"node",(actionInfo -> node));
/* cancel is handled differently on ad-hoc commands than regular forms */
  if ((purple_strequal(xmlnode_get_namespace(result),"jabber:x:data") != 0) && (purple_strequal(xmlnode_get_attrib(result,"type"),"cancel") != 0)) {
    xmlnode_set_attrib(command,"action","cancel");
  }
  else {
    if (actionhandle != 0) 
      xmlnode_set_attrib(command,"action",actionhandle);
    xmlnode_insert_child(command,result);
  }
  for (action = (actionInfo -> actionslist); action != 0; action = ((action != 0)?( *((GList *)action)).next : ((struct _GList *)((void *)0)))) {
    char *handle = (action -> data);
    g_free(handle);
  }
  g_list_free((actionInfo -> actionslist));
  g_free((actionInfo -> sessionid));
  g_free((actionInfo -> who));
  g_free((actionInfo -> node));
  jabber_iq_send(iq);
}

static void jabber_adhoc_parse(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  xmlnode *command = xmlnode_get_child_with_namespace(packet,"command","http://jabber.org/protocol/commands");
  const char *status = xmlnode_get_attrib(command,"status");
  xmlnode *xdata = xmlnode_get_child_with_namespace(command,"x","jabber:x:data");
  if (type == JABBER_IQ_ERROR) {
    char *msg = jabber_parse_error(js,packet,0);
    if (!(msg != 0)) 
      msg = g_strdup(((const char *)(dgettext("pidgin","Unknown Error"))));
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Ad-Hoc Command Failed"))),((const char *)(dgettext("pidgin","Ad-Hoc Command Failed"))),msg,0,0);
    g_free(msg);
    return ;
  }
  if (!(status != 0)) 
    return ;
  if (!(strcmp(status,"completed") != 0)) {
/* display result */
    xmlnode *note = xmlnode_get_child(command,"note");
    if (note != 0) {
      char *data = xmlnode_get_data(note);
      purple_notify_message(0,PURPLE_NOTIFY_MSG_INFO,from,data,0,0,0);
      g_free(data);
    }
    if (xdata != 0) 
      jabber_x_data_request(js,xdata,((jabber_x_data_cb )do_adhoc_ignoreme),0);
    return ;
  }
  if (!(strcmp(status,"executing") != 0)) {
/* this command needs more steps */
    xmlnode *actions;
    xmlnode *action;
    int actionindex = 0;
    GList *actionslist = (GList *)((void *)0);
    JabberAdHocActionInfo *actionInfo;
    if (!(xdata != 0)) 
/* shouldn't happen */
      return ;
    actions = xmlnode_get_child(command,"actions");
    if (!(actions != 0)) {
      JabberXDataAction *defaultaction = (JabberXDataAction *)(g_malloc0_n(1,(sizeof(JabberXDataAction ))));
      defaultaction -> name = g_strdup(((const char *)(dgettext("pidgin","execute"))));
      defaultaction -> handle = g_strdup("execute");
      actionslist = g_list_append(actionslist,defaultaction);
    }
    else {
      const char *defaultactionhandle = xmlnode_get_attrib(actions,"execute");
      int index = 0;
      for (action = (actions -> child); action != 0; ((action = (action -> next)) , ++index)) {
        if ((action -> type) == XMLNODE_TYPE_TAG) {
          JabberXDataAction *newaction = (JabberXDataAction *)(g_malloc0_n(1,(sizeof(JabberXDataAction ))));
          newaction -> name = g_strdup(((const char *)(dgettext("pidgin",(action -> name)))));
          newaction -> handle = g_strdup((action -> name));
          actionslist = g_list_append(actionslist,newaction);
          if ((defaultactionhandle != 0) && !(strcmp(defaultactionhandle,(action -> name)) != 0)) 
            actionindex = index;
        }
      }
    }
    actionInfo = ((JabberAdHocActionInfo *)(g_malloc0_n(1,(sizeof(JabberAdHocActionInfo )))));
    actionInfo -> sessionid = g_strdup(xmlnode_get_attrib(command,"sessionid"));
    actionInfo -> who = g_strdup(from);
    actionInfo -> node = g_strdup(xmlnode_get_attrib(command,"node"));
    actionInfo -> actionslist = actionslist;
    jabber_x_data_request_with_actions(js,xdata,actionslist,actionindex,do_adhoc_action_cb,actionInfo);
  }
}

void jabber_adhoc_execute_action(PurpleBlistNode *node,gpointer data)
{
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    JabberAdHocCommands *cmd = data;
    PurpleBuddy *buddy = (PurpleBuddy *)node;
    PurpleAccount *account = purple_buddy_get_account(buddy);
    JabberStream *js = ( *purple_account_get_connection(account)).proto_data;
    jabber_adhoc_execute(js,cmd);
  }
}

static void jabber_adhoc_got_server_list(JabberStream *js,const char *from,xmlnode *query)
{
  xmlnode *item;
  if (!(query != 0)) 
    return ;
/* clean current list (just in case there is one) */
  while((js -> commands) != 0){
    JabberAdHocCommands *cmd = ( *(js -> commands)).data;
    g_free((cmd -> jid));
    g_free((cmd -> node));
    g_free((cmd -> name));
    g_free(cmd);
    js -> commands = g_list_delete_link((js -> commands),(js -> commands));
  }
/* re-fill list */
  for (item = (query -> child); item != 0; item = (item -> next)) {{
      JabberAdHocCommands *cmd;
      if ((item -> type) != XMLNODE_TYPE_TAG) 
        continue; 
      if (strcmp((item -> name),"item") != 0) 
        continue; 
      cmd = ((JabberAdHocCommands *)(g_malloc0_n(1,(sizeof(JabberAdHocCommands )))));
      cmd -> jid = g_strdup(xmlnode_get_attrib(item,"jid"));
      cmd -> node = g_strdup(xmlnode_get_attrib(item,"node"));
      cmd -> name = g_strdup(xmlnode_get_attrib(item,"name"));
      js -> commands = g_list_append((js -> commands),cmd);
    }
  }
  if ((js -> state) == JABBER_STREAM_CONNECTED) 
    purple_prpl_got_account_actions(purple_connection_get_account((js -> gc)));
}

static void jabber_adhoc_server_got_list_cb(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  xmlnode *query = xmlnode_get_child_with_namespace(packet,"query","http://jabber.org/protocol/disco#items");
  jabber_adhoc_got_server_list(js,from,query);
}

void jabber_adhoc_got_list(JabberStream *js,const char *from,xmlnode *query)
{
  if (purple_strequal(from,( *(js -> user)).domain) != 0) {
    jabber_adhoc_got_server_list(js,from,query);
  }
  else {
    jabber_adhoc_got_buddy_list(js,from,query);
  }
}

void jabber_adhoc_server_get_list(JabberStream *js)
{
  JabberIq *iq = jabber_iq_new_query(js,JABBER_IQ_GET,"http://jabber.org/protocol/disco#items");
  xmlnode *query = xmlnode_get_child_with_namespace((iq -> node),"query","http://jabber.org/protocol/disco#items");
  xmlnode_set_attrib((iq -> node),"to",( *(js -> user)).domain);
  xmlnode_set_attrib(query,"node","http://jabber.org/protocol/commands");
  jabber_iq_set_callback(iq,jabber_adhoc_server_got_list_cb,0);
  jabber_iq_send(iq);
}

void jabber_adhoc_execute(JabberStream *js,JabberAdHocCommands *cmd)
{
  JabberIq *iq = jabber_iq_new(js,JABBER_IQ_SET);
  xmlnode *command = xmlnode_new_child((iq -> node),"command");
  xmlnode_set_attrib((iq -> node),"to",(cmd -> jid));
  xmlnode_set_namespace(command,"http://jabber.org/protocol/commands");
  xmlnode_set_attrib(command,"node",(cmd -> node));
  xmlnode_set_attrib(command,"action","execute");
  jabber_iq_set_callback(iq,jabber_adhoc_parse,0);
  jabber_iq_send(iq);
}

static void jabber_adhoc_server_execute(PurplePluginAction *action)
{
  JabberAdHocCommands *cmd = (action -> user_data);
  if (cmd != 0) {
    PurpleConnection *gc = (PurpleConnection *)(action -> context);
    JabberStream *js = (gc -> proto_data);
    jabber_adhoc_execute(js,cmd);
  }
}

void jabber_adhoc_init_server_commands(JabberStream *js,GList **m)
{
  GList *cmdlst;
  JabberBuddy *jb;
/* also add commands for other clients connected to the same account on another resource */
  char *accountname = g_strdup_printf("%s@%s",( *(js -> user)).node,( *(js -> user)).domain);
  if ((jb = jabber_buddy_find(js,accountname,(!0))) != 0) {
    GList *iter;
    for (iter = (jb -> resources); iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
      JabberBuddyResource *jbr = (iter -> data);
      GList *riter;
      for (riter = (jbr -> commands); riter != 0; riter = ((riter != 0)?( *((GList *)riter)).next : ((struct _GList *)((void *)0)))) {
        JabberAdHocCommands *cmd = (riter -> data);
        char *cmdname = g_strdup_printf("%s (%s)",(cmd -> name),(jbr -> name));
        PurplePluginAction *act = purple_plugin_action_new(cmdname,jabber_adhoc_server_execute);
        act -> user_data = cmd;
         *m = g_list_append( *m,act);
        g_free(cmdname);
      }
    }
  }
  g_free(accountname);
/* now add server commands */
  for (cmdlst = (js -> commands); cmdlst != 0; cmdlst = ((cmdlst != 0)?( *((GList *)cmdlst)).next : ((struct _GList *)((void *)0)))) {
    JabberAdHocCommands *cmd = (cmdlst -> data);
    PurplePluginAction *act = purple_plugin_action_new((cmd -> name),jabber_adhoc_server_execute);
    act -> user_data = cmd;
     *m = g_list_append( *m,act);
  }
}
