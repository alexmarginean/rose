/**
 * @file rtp.c
 *
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "config.h"
#ifdef USE_VV
#include "jabber.h"
#include "jingle.h"
#include "media.h"
#include "mediamanager.h"
#include "iceudp.h"
#include "rawudp.h"
#include "rtp.h"
#include "session.h"
#include "debug.h"
#include <string.h>

struct _JingleRtpPrivate 
{
  gchar *media_type;
  gchar *ssrc;
}
;
#define JINGLE_RTP_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE((obj), JINGLE_TYPE_RTP, JingleRtpPrivate))
static void jingle_rtp_class_init(JingleRtpClass *klass);
static void jingle_rtp_init(JingleRtp *rtp);
static void jingle_rtp_finalize(GObject *object);
static void jingle_rtp_get_property(GObject *object,guint prop_id,GValue *value,GParamSpec *pspec);
static void jingle_rtp_set_property(GObject *object,guint prop_id,const GValue *value,GParamSpec *pspec);
static JingleContent *jingle_rtp_parse_internal(xmlnode *rtp);
static xmlnode *jingle_rtp_to_xml_internal(JingleContent *rtp,xmlnode *content,JingleActionType action);
static void jingle_rtp_handle_action_internal(JingleContent *content,xmlnode *jingle,JingleActionType action);
static PurpleMedia *jingle_rtp_get_media(JingleSession *session);
static JingleContentClass *parent_class = (JingleContentClass *)((void *)0);
#if 0
#endif
enum __unnamed_enum___F0_L68_C1_PROP_0__COMMA__PROP_MEDIA_TYPE__COMMA__PROP_SSRC {PROP_0,PROP_MEDIA_TYPE,PROP_SSRC};

GType jingle_rtp_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(JingleRtpClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )jingle_rtp_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(JingleRtp ))), (0), ((GInstanceInitFunc )jingle_rtp_init), ((const GTypeValueTable *)((void *)0))};
    type = g_type_register_static(jingle_content_get_type(),"JingleRtp",&info,0);
  }
  return type;
}

static void jingle_rtp_class_init(JingleRtpClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *)klass;
  parent_class = (g_type_class_peek_parent(klass));
  gobject_class -> finalize = jingle_rtp_finalize;
  gobject_class -> set_property = jingle_rtp_set_property;
  gobject_class -> get_property = jingle_rtp_get_property;
  klass -> parent_class.to_xml = jingle_rtp_to_xml_internal;
  klass -> parent_class.parse = jingle_rtp_parse_internal;
  klass -> parent_class.description_type = "urn:xmpp:jingle:apps:rtp:1";
  klass -> parent_class.handle_action = jingle_rtp_handle_action_internal;
  g_object_class_install_property(gobject_class,PROP_MEDIA_TYPE,g_param_spec_string("media-type","Media Type","The media type (\"audio\" or \"video\") for this rtp session.",0,(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(gobject_class,PROP_SSRC,g_param_spec_string("ssrc","ssrc","The ssrc for this rtp session.",0,(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_type_class_add_private(klass,(sizeof(JingleRtpPrivate )));
}

static void jingle_rtp_init(JingleRtp *rtp)
{
  rtp -> priv = ((JingleRtpPrivate *)(g_type_instance_get_private(((GTypeInstance *)rtp),jingle_rtp_get_type())));
  memset((rtp -> priv),0,(sizeof(( *(rtp -> priv)))));
}

static void jingle_rtp_finalize(GObject *rtp)
{
  JingleRtpPrivate *priv = (JingleRtpPrivate *)(g_type_instance_get_private(((GTypeInstance *)rtp),jingle_rtp_get_type()));
  purple_debug_info("jingle-rtp","jingle_rtp_finalize\n");
  g_free((priv -> media_type));
  g_free((priv -> ssrc));
  ( *( *((GObjectClass *)(g_type_check_class_cast(((GTypeClass *)parent_class),((GType )(20 << 2)))))).finalize)(rtp);
}

static void jingle_rtp_set_property(GObject *object,guint prop_id,const GValue *value,GParamSpec *pspec)
{
  JingleRtp *rtp;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = jingle_rtp_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"JINGLE_IS_RTP(object)");
      return ;
    };
  }while (0);
  rtp = ((JingleRtp *)(g_type_check_instance_cast(((GTypeInstance *)object),jingle_rtp_get_type())));
  switch(prop_id){
    case PROP_MEDIA_TYPE:
{
      g_free(( *(rtp -> priv)).media_type);
      ( *(rtp -> priv)).media_type = g_value_dup_string(value);
      break; 
    }
    case PROP_SSRC:
{
      g_free(( *(rtp -> priv)).ssrc);
      ( *(rtp -> priv)).ssrc = g_value_dup_string(value);
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = prop_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","rtp.c:164","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void jingle_rtp_get_property(GObject *object,guint prop_id,GValue *value,GParamSpec *pspec)
{
  JingleRtp *rtp;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = jingle_rtp_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"JINGLE_IS_RTP(object)");
      return ;
    };
  }while (0);
  rtp = ((JingleRtp *)(g_type_check_instance_cast(((GTypeInstance *)object),jingle_rtp_get_type())));
  switch(prop_id){
    case PROP_MEDIA_TYPE:
{
      g_value_set_string(value,( *(rtp -> priv)).media_type);
      break; 
    }
    case PROP_SSRC:
{
      g_value_set_string(value,( *(rtp -> priv)).ssrc);
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = prop_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","rtp.c:185","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

gchar *jingle_rtp_get_media_type(JingleContent *content)
{
  gchar *media_type;
  g_object_get(content,"media-type",&media_type,((void *)((void *)0)));
  return media_type;
}

gchar *jingle_rtp_get_ssrc(JingleContent *content)
{
  gchar *ssrc;
  g_object_get(content,"ssrc",&ssrc,((void *)((void *)0)));
  return ssrc;
}

static PurpleMedia *jingle_rtp_get_media(JingleSession *session)
{
  JabberStream *js = jingle_session_get_js(session);
  PurpleMedia *media = (PurpleMedia *)((void *)0);
  GList *iter = purple_media_manager_get_media_by_account(purple_media_manager_get(),purple_connection_get_account((js -> gc)));
{
    for (; iter != 0; iter = g_list_delete_link(iter,iter)) {
      JingleSession *media_session = (purple_media_get_prpl_data((iter -> data)));
      if (media_session == session) {
        media = (iter -> data);
        break; 
      }
    }
  }
  if (iter != ((GList *)((void *)0))) 
    g_list_free(iter);
  return media;
}

static JingleRawUdpCandidate *jingle_rtp_candidate_to_rawudp(JingleSession *session,guint generation,PurpleMediaCandidate *candidate)
{
  gchar *id = jabber_get_next_id(jingle_session_get_js(session));
  gchar *ip = purple_media_candidate_get_ip(candidate);
  JingleRawUdpCandidate *rawudp_candidate = jingle_rawudp_candidate_new(id,generation,purple_media_candidate_get_component_id(candidate),ip,(purple_media_candidate_get_port(candidate)));
  g_free(ip);
  g_free(id);
  return rawudp_candidate;
}

static JingleIceUdpCandidate *jingle_rtp_candidate_to_iceudp(JingleSession *session,guint generation,PurpleMediaCandidate *candidate)
{
  gchar *id = jabber_get_next_id(jingle_session_get_js(session));
  gchar *ip = purple_media_candidate_get_ip(candidate);
  gchar *username = purple_media_candidate_get_username(candidate);
  gchar *password = purple_media_candidate_get_password(candidate);
  PurpleMediaCandidateType type = purple_media_candidate_get_candidate_type(candidate);
  JingleIceUdpCandidate *iceudp_candidate = jingle_iceudp_candidate_new(purple_media_candidate_get_component_id(candidate),(purple_media_candidate_get_foundation(candidate)),generation,id,ip,0,(purple_media_candidate_get_port(candidate)),purple_media_candidate_get_priority(candidate),"udp",((type == PURPLE_MEDIA_CANDIDATE_TYPE_HOST)?"host" : (((type == PURPLE_MEDIA_CANDIDATE_TYPE_SRFLX)?"srflx" : (((type == PURPLE_MEDIA_CANDIDATE_TYPE_PRFLX)?"prflx" : (((type == PURPLE_MEDIA_CANDIDATE_TYPE_RELAY)?"relay" : ""))))))),username,password);
  iceudp_candidate -> reladdr = purple_media_candidate_get_base_ip(candidate);
  iceudp_candidate -> relport = (purple_media_candidate_get_base_port(candidate));
  g_free(password);
  g_free(username);
  g_free(ip);
  g_free(id);
  return iceudp_candidate;
}

static JingleTransport *jingle_rtp_candidates_to_transport(JingleSession *session,GType type,guint generation,GList *candidates)
{
  if (type == jingle_rawudp_get_type()) {
    JingleTransport *transport = jingle_transport_create("urn:xmpp:jingle:transports:raw-udp:1");
    JingleRawUdpCandidate *rawudp_candidate;
    for (; candidates != 0; candidates = ((candidates != 0)?( *((GList *)candidates)).next : ((struct _GList *)((void *)0)))) {
      PurpleMediaCandidate *candidate = (candidates -> data);
      rawudp_candidate = jingle_rtp_candidate_to_rawudp(session,generation,candidate);
      jingle_rawudp_add_local_candidate(((JingleRawUdp *)(g_type_check_instance_cast(((GTypeInstance *)transport),jingle_rawudp_get_type()))),rawudp_candidate);
    }
    return transport;
  }
  else if (type == jingle_iceudp_get_type()) {
    JingleTransport *transport = jingle_transport_create("urn:xmpp:jingle:transports:ice-udp:1");
    JingleIceUdpCandidate *iceudp_candidate;
    for (; candidates != 0; candidates = ((candidates != 0)?( *((GList *)candidates)).next : ((struct _GList *)((void *)0)))) {
      PurpleMediaCandidate *candidate = (candidates -> data);
      iceudp_candidate = jingle_rtp_candidate_to_iceudp(session,generation,candidate);
      jingle_iceudp_add_local_candidate(((JingleIceUdp *)(g_type_check_instance_cast(((GTypeInstance *)transport),jingle_iceudp_get_type()))),iceudp_candidate);
    }
    return transport;
  }
  else {
    return 0;
  }
}

static GList *jingle_rtp_transport_to_candidates(JingleTransport *transport)
{
  const gchar *type = jingle_transport_get_transport_type(transport);
  GList *ret = (GList *)((void *)0);
  if (!(strcmp(type,"urn:xmpp:jingle:transports:raw-udp:1") != 0)) {
    GList *candidates = jingle_rawudp_get_remote_candidates(((JingleRawUdp *)(g_type_check_instance_cast(((GTypeInstance *)transport),jingle_rawudp_get_type()))));
    for (; candidates != 0; candidates = g_list_delete_link(candidates,candidates)) {
      JingleRawUdpCandidate *candidate = (candidates -> data);
      ret = g_list_append(ret,(purple_media_candidate_new("",(candidate -> component),PURPLE_MEDIA_CANDIDATE_TYPE_SRFLX,PURPLE_MEDIA_NETWORK_PROTOCOL_UDP,(candidate -> ip),(candidate -> port))));
    }
    return ret;
  }
  else if (!(strcmp(type,"urn:xmpp:jingle:transports:ice-udp:1") != 0)) {
    GList *candidates = jingle_iceudp_get_remote_candidates(((JingleIceUdp *)(g_type_check_instance_cast(((GTypeInstance *)transport),jingle_iceudp_get_type()))));
    for (; candidates != 0; candidates = g_list_delete_link(candidates,candidates)) {
      JingleIceUdpCandidate *candidate = (candidates -> data);
      PurpleMediaCandidate *new_candidate = purple_media_candidate_new((candidate -> foundation),(candidate -> component),(!(strcmp((candidate -> type),"host") != 0)?PURPLE_MEDIA_CANDIDATE_TYPE_HOST : ((!(strcmp((candidate -> type),"srflx") != 0)?PURPLE_MEDIA_CANDIDATE_TYPE_SRFLX : ((!(strcmp((candidate -> type),"prflx") != 0)?PURPLE_MEDIA_CANDIDATE_TYPE_PRFLX : ((!(strcmp((candidate -> type),"relay") != 0)?PURPLE_MEDIA_CANDIDATE_TYPE_RELAY : 0))))))),PURPLE_MEDIA_NETWORK_PROTOCOL_UDP,(candidate -> ip),(candidate -> port));
      g_object_set(new_candidate,"base-ip",(candidate -> reladdr),"base-port",(candidate -> relport),"username",(candidate -> username),"password",(candidate -> password),"priority",(candidate -> priority),((void *)((void *)0)));
      ret = g_list_append(ret,new_candidate);
    }
    return ret;
  }
  else {
    return 0;
  }
}
static void jingle_rtp_ready(JingleSession *session);

static void jingle_rtp_candidates_prepared_cb(PurpleMedia *media,gchar *sid,gchar *name,JingleSession *session)
{
  JingleContent *content = jingle_session_find_content(session,sid,0);
  JingleTransport *oldtransport;
  JingleTransport *transport;
  GList *candidates;
  purple_debug_info("jingle-rtp","jingle_rtp_candidates_prepared_cb\n");
  if (content == ((JingleContent *)((void *)0))) {
    purple_debug_error("jingle-rtp","jingle_rtp_candidates_prepared_cb: Can\'t find session %s\n",sid);
    return ;
  }
  oldtransport = jingle_content_get_transport(content);
  candidates = purple_media_get_local_candidates(media,sid,name);
  transport = ((JingleTransport *)(g_type_check_instance_cast(((GTypeInstance *)(jingle_rtp_candidates_to_transport(session,((((
{
    GTypeInstance *__inst = (GTypeInstance *)oldtransport;
    GType __t = jingle_rawudp_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)?jingle_rawudp_get_type() : jingle_iceudp_get_type()),0,candidates))),jingle_transport_get_type())));
  g_list_free(candidates);
  g_object_unref(oldtransport);
  jingle_content_set_pending_transport(content,transport);
  jingle_content_accept_transport(content);
  jingle_rtp_ready(session);
}

static void jingle_rtp_codecs_changed_cb(PurpleMedia *media,gchar *sid,JingleSession *session)
{
  purple_debug_info("jingle-rtp","jingle_rtp_codecs_changed_cb: session_id: %s jingle_session: %p\n",sid,session);
  jingle_rtp_ready(session);
}

static void jingle_rtp_new_candidate_cb(PurpleMedia *media,gchar *sid,gchar *name,PurpleMediaCandidate *candidate,JingleSession *session)
{
  JingleContent *content = jingle_session_find_content(session,sid,0);
  JingleTransport *transport;
  purple_debug_info("jingle-rtp","jingle_rtp_new_candidate_cb\n");
  if (content == ((JingleContent *)((void *)0))) {
    purple_debug_error("jingle-rtp","jingle_rtp_new_candidate_cb: Can\'t find session %s\n",sid);
    return ;
  }
  transport = jingle_content_get_transport(content);
  if ((({
    GTypeInstance *__inst = (GTypeInstance *)transport;
    GType __t = jingle_iceudp_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
    jingle_iceudp_add_local_candidate(((JingleIceUdp *)(g_type_check_instance_cast(((GTypeInstance *)transport),jingle_iceudp_get_type()))),jingle_rtp_candidate_to_iceudp(session,1,candidate));
  else if ((({
    GTypeInstance *__inst = (GTypeInstance *)transport;
    GType __t = jingle_rawudp_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
    jingle_rawudp_add_local_candidate(((JingleRawUdp *)(g_type_check_instance_cast(((GTypeInstance *)transport),jingle_rawudp_get_type()))),jingle_rtp_candidate_to_rawudp(session,1,candidate));
  g_object_unref(transport);
  jabber_iq_send(jingle_session_to_packet(session,JINGLE_TRANSPORT_INFO));
}

static void jingle_rtp_initiate_ack_cb(JabberStream *js,const char *from,JabberIqType type,const char *id,xmlnode *packet,gpointer data)
{
  JingleSession *session = data;
  if ((type == JABBER_IQ_ERROR) || (xmlnode_get_child(packet,"error") != 0)) {
    purple_media_end(jingle_rtp_get_media(session),0,0);
    g_object_unref(session);
    return ;
  }
}

static void jingle_rtp_state_changed_cb(PurpleMedia *media,PurpleMediaState state,gchar *sid,gchar *name,JingleSession *session)
{
  purple_debug_info("jingle-rtp","state-changed: state %d id: %s name: %s\n",state,((sid != 0)?sid : "(null)"),((name != 0)?name : "(null)"));
}

static void jingle_rtp_stream_info_cb(PurpleMedia *media,PurpleMediaInfoType type,gchar *sid,gchar *name,gboolean local,JingleSession *session)
{
  purple_debug_info("jingle-rtp","stream-info: type %d id: %s name: %s\n",type,((sid != 0)?sid : "(null)"),((name != 0)?name : "(null)"));
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)session;
      GType __t = jingle_session_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"JINGLE_IS_SESSION(session)");
      return ;
    };
  }while (0);
  if ((type == PURPLE_MEDIA_INFO_HANGUP) || (type == PURPLE_MEDIA_INFO_REJECT)) {
    jabber_iq_send(jingle_session_terminate_packet(session,((type == PURPLE_MEDIA_INFO_HANGUP)?"success" : "decline")));
    g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)media),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )jingle_rtp_state_changed_cb),session);
    g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)media),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )jingle_rtp_stream_info_cb),session);
    g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)media),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )jingle_rtp_new_candidate_cb),session);
    g_object_unref(session);
  }
  else if ((type == PURPLE_MEDIA_INFO_ACCEPT) && (jingle_session_is_initiator(session) == 0)) {
    jingle_rtp_ready(session);
  }
}

static void jingle_rtp_ready(JingleSession *session)
{
  PurpleMedia *media = jingle_rtp_get_media(session);
  if (((purple_media_candidates_prepared(media,0,0) != 0) && (purple_media_codecs_ready(media,0) != 0)) && ((jingle_session_is_initiator(session) == !0) || (purple_media_accepted(media,0,0) != 0))) {
    if (jingle_session_is_initiator(session) != 0) {
      JabberIq *iq = jingle_session_to_packet(session,JINGLE_SESSION_INITIATE);
      jabber_iq_set_callback(iq,jingle_rtp_initiate_ack_cb,session);
      jabber_iq_send(iq);
    }
    else {
      jabber_iq_send(jingle_session_to_packet(session,JINGLE_SESSION_ACCEPT));
    }
    g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)media),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )jingle_rtp_candidates_prepared_cb),session);
    g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)media),((GType )(20 << 2))))),((GSignalMatchType )(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA)),0,0,0,((GCallback )jingle_rtp_codecs_changed_cb),session);
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)media),((GType )(20 << 2))))),"new-candidate",((GCallback )jingle_rtp_new_candidate_cb),session,0,((GConnectFlags )0));
  }
}

static PurpleMedia *jingle_rtp_create_media(JingleContent *content)
{
  JingleSession *session = jingle_content_get_session(content);
  JabberStream *js = jingle_session_get_js(session);
  gchar *remote_jid = jingle_session_get_remote_jid(session);
  PurpleMedia *media = purple_media_manager_create_media(purple_media_manager_get(),purple_connection_get_account((js -> gc)),"fsrtpconference",remote_jid,jingle_session_is_initiator(session));
  g_free(remote_jid);
  if (!(media != 0)) {
    purple_debug_error("jingle-rtp","Couldn\'t create media session\n");
    return 0;
  }
  purple_media_set_prpl_data(media,session);
/* connect callbacks */
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)media),((GType )(20 << 2))))),"candidates-prepared",((GCallback )jingle_rtp_candidates_prepared_cb),session,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)media),((GType )(20 << 2))))),"codecs-changed",((GCallback )jingle_rtp_codecs_changed_cb),session,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)media),((GType )(20 << 2))))),"state-changed",((GCallback )jingle_rtp_state_changed_cb),session,0,((GConnectFlags )0));
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)media),((GType )(20 << 2))))),"stream-info",((GCallback )jingle_rtp_stream_info_cb),session,0,((GConnectFlags )0));
  g_object_unref(session);
  return media;
}

static gboolean jingle_rtp_init_media(JingleContent *content)
{
  JingleSession *session = jingle_content_get_session(content);
  PurpleMedia *media = jingle_rtp_get_media(session);
  gchar *creator;
  gchar *media_type;
  gchar *remote_jid;
  gchar *senders;
  gchar *name;
  const gchar *transmitter;
  gboolean is_audio;
  gboolean is_creator;
  PurpleMediaSessionType type;
  JingleTransport *transport;
  GParameter *params = (GParameter *)((void *)0);
  guint num_params;
/* maybe this create ought to just be in initiate and handle initiate */
  if (media == ((PurpleMedia *)((void *)0))) {
    media = jingle_rtp_create_media(content);
    if (media == ((PurpleMedia *)((void *)0))) 
      return 0;
  }
  name = jingle_content_get_name(content);
  media_type = jingle_rtp_get_media_type(content);
  remote_jid = jingle_session_get_remote_jid(session);
  senders = jingle_content_get_senders(content);
  transport = jingle_content_get_transport(content);
  if (media_type == ((gchar *)((void *)0))) {
    g_free(name);
    g_free(remote_jid);
    g_free(senders);
    g_free(params);
    g_object_unref(transport);
    g_object_unref(session);
    return 0;
  }
  if ((({
    GTypeInstance *__inst = (GTypeInstance *)transport;
    GType __t = jingle_rawudp_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
    transmitter = "rawudp";
  else if ((({
    GTypeInstance *__inst = (GTypeInstance *)transport;
    GType __t = jingle_iceudp_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
    transmitter = "nice";
  else 
    transmitter = "notransmitter";
  g_object_unref(transport);
  is_audio = g_str_equal(media_type,"audio");
  if (purple_strequal(senders,"both") != 0) 
    type = (((is_audio != 0)?PURPLE_MEDIA_AUDIO : PURPLE_MEDIA_VIDEO));
  else if (purple_strequal(senders,"initiator") == jingle_session_is_initiator(session)) 
    type = (((is_audio != 0)?PURPLE_MEDIA_SEND_AUDIO : PURPLE_MEDIA_SEND_VIDEO));
  else 
    type = (((is_audio != 0)?PURPLE_MEDIA_RECV_AUDIO : PURPLE_MEDIA_RECV_VIDEO));
  params = jingle_get_params(jingle_session_get_js(session),0,0,0,0,0,0,&num_params);
  creator = jingle_content_get_creator(content);
  if (creator == ((gchar *)((void *)0))) {
    g_free(name);
    g_free(media_type);
    g_free(remote_jid);
    g_free(senders);
    g_free(params);
    g_object_unref(session);
    return 0;
  }
  if (g_str_equal(creator,"initiator") != 0) 
    is_creator = jingle_session_is_initiator(session);
  else 
    is_creator = !(jingle_session_is_initiator(session) != 0);
  g_free(creator);
  if (!(purple_media_add_stream(media,name,remote_jid,type,is_creator,transmitter,num_params,params) != 0)) {
    purple_media_end(media,0,0);
/* TODO: How much clean-up is necessary here? (does calling
		         purple_media_end lead to cleaning up Jingle structs?) */
    return 0;
  }
  g_free(name);
  g_free(media_type);
  g_free(remote_jid);
  g_free(senders);
  g_free(params);
  g_object_unref(session);
  return (!0);
}

static GList *jingle_rtp_parse_codecs(xmlnode *description)
{
  GList *codecs = (GList *)((void *)0);
  xmlnode *codec_element = (xmlnode *)((void *)0);
  const char *encoding_name;
  const char *id;
  const char *clock_rate;
  PurpleMediaCodec *codec;
  const gchar *media = xmlnode_get_attrib(description,"media");
  PurpleMediaSessionType type;
  if (media == ((const gchar *)((void *)0))) {
    purple_debug_warning("jingle-rtp","missing media type\n");
    return 0;
  }
  if (g_str_equal(media,"video") != 0) {
    type = PURPLE_MEDIA_VIDEO;
  }
  else if (g_str_equal(media,"audio") != 0) {
    type = PURPLE_MEDIA_AUDIO;
  }
  else {
    purple_debug_warning("jingle-rtp","unknown media type: %s\n",media);
    return 0;
  }
  for (codec_element = xmlnode_get_child(description,"payload-type"); codec_element != 0; codec_element = xmlnode_get_next_twin(codec_element)) {
    xmlnode *param;
    gchar *codec_str;
    encoding_name = xmlnode_get_attrib(codec_element,"name");
    id = xmlnode_get_attrib(codec_element,"id");
    clock_rate = xmlnode_get_attrib(codec_element,"clockrate");
    codec = purple_media_codec_new(atoi(id),encoding_name,type,(((clock_rate != 0)?atoi(clock_rate) : 0)));
    for (param = xmlnode_get_child(codec_element,"parameter"); param != 0; param = xmlnode_get_next_twin(param)) {
      purple_media_codec_add_optional_parameter(codec,xmlnode_get_attrib(param,"name"),xmlnode_get_attrib(param,"value"));
    }
    codec_str = purple_media_codec_to_string(codec);
    purple_debug_info("jingle-rtp","received codec: %s\n",codec_str);
    g_free(codec_str);
    codecs = g_list_append(codecs,codec);
  }
  return codecs;
}

static JingleContent *jingle_rtp_parse_internal(xmlnode *rtp)
{
  JingleContent *content = ( *(parent_class -> parse))(rtp);
  xmlnode *description = xmlnode_get_child(rtp,"description");
  const gchar *media_type = xmlnode_get_attrib(description,"media");
  const gchar *ssrc = xmlnode_get_attrib(description,"ssrc");
  purple_debug_info("jingle-rtp","rtp parse\n");
  g_object_set(content,"media-type",media_type,((void *)((void *)0)));
  if (ssrc != ((const gchar *)((void *)0))) 
    g_object_set(content,"ssrc",ssrc,((void *)((void *)0)));
  return content;
}

static void jingle_rtp_add_payloads(xmlnode *description,GList *codecs)
{
  for (; codecs != 0; codecs = (codecs -> next)) {
    PurpleMediaCodec *codec = (PurpleMediaCodec *)(codecs -> data);
    GList *iter = purple_media_codec_get_optional_parameters(codec);
    gchar *id;
    gchar *name;
    gchar *clockrate;
    gchar *channels;
    gchar *codec_str;
    xmlnode *payload = xmlnode_new_child(description,"payload-type");
    id = g_strdup_printf("%d",purple_media_codec_get_id(codec));
    name = purple_media_codec_get_encoding_name(codec);
    clockrate = g_strdup_printf("%d",purple_media_codec_get_clock_rate(codec));
    channels = g_strdup_printf("%d",purple_media_codec_get_channels(codec));
    xmlnode_set_attrib(payload,"name",name);
    xmlnode_set_attrib(payload,"id",id);
    xmlnode_set_attrib(payload,"clockrate",clockrate);
    xmlnode_set_attrib(payload,"channels",channels);
    g_free(channels);
    g_free(clockrate);
    g_free(name);
    g_free(id);
    for (; iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
      PurpleKeyValuePair *mparam = (iter -> data);
      xmlnode *param = xmlnode_new_child(payload,"parameter");
      xmlnode_set_attrib(param,"name",(mparam -> key));
      xmlnode_set_attrib(param,"value",(mparam -> value));
    }
    codec_str = purple_media_codec_to_string(codec);
    purple_debug_info("jingle","adding codec: %s\n",codec_str);
    g_free(codec_str);
  }
}

static xmlnode *jingle_rtp_to_xml_internal(JingleContent *rtp,xmlnode *content,JingleActionType action)
{
  xmlnode *node = ( *(parent_class -> to_xml))(rtp,content,action);
  xmlnode *description = xmlnode_get_child(node,"description");
  if (description != ((xmlnode *)((void *)0))) {
    JingleSession *session = jingle_content_get_session(rtp);
    PurpleMedia *media = jingle_rtp_get_media(session);
    gchar *media_type = jingle_rtp_get_media_type(rtp);
    gchar *ssrc = jingle_rtp_get_ssrc(rtp);
    gchar *name = jingle_content_get_name(rtp);
    GList *codecs = purple_media_get_codecs(media,name);
    xmlnode_set_attrib(description,"media",media_type);
    if (ssrc != ((gchar *)((void *)0))) 
      xmlnode_set_attrib(description,"ssrc",ssrc);
    g_free(media_type);
    g_free(name);
    g_object_unref(session);
    jingle_rtp_add_payloads(description,codecs);
    purple_media_codec_list_free(codecs);
  }
  return node;
}

static void jingle_rtp_handle_action_internal(JingleContent *content,xmlnode *xmlcontent,JingleActionType action)
{
{
    switch(action){
      case JINGLE_SESSION_ACCEPT:
{
      }
      case JINGLE_SESSION_INITIATE:
{
{
          JingleSession *session;
          JingleTransport *transport;
          xmlnode *description;
          GList *candidates;
          GList *codecs;
          gchar *name;
          gchar *remote_jid;
          PurpleMedia *media;
          session = jingle_content_get_session(content);
          if ((action == JINGLE_SESSION_INITIATE) && !(jingle_rtp_init_media(content) != 0)) {
/* XXX: send error */
            jabber_iq_send(jingle_session_terminate_packet(session,"general-error"));
            g_object_unref(session);
            break; 
          }
          transport = jingle_transport_parse(xmlnode_get_child(xmlcontent,"transport"));
          description = xmlnode_get_child(xmlcontent,"description");
          candidates = jingle_rtp_transport_to_candidates(transport);
          codecs = jingle_rtp_parse_codecs(description);
          name = jingle_content_get_name(content);
          remote_jid = jingle_session_get_remote_jid(session);
          media = jingle_rtp_get_media(session);
          purple_media_set_remote_codecs(media,name,remote_jid,codecs);
          purple_media_add_remote_candidates(media,name,remote_jid,candidates);
          if (action == JINGLE_SESSION_ACCEPT) 
            purple_media_stream_info(media,PURPLE_MEDIA_INFO_ACCEPT,name,remote_jid,0);
          g_free(remote_jid);
          g_free(name);
          g_object_unref(session);
          break; 
        }
      }
      case JINGLE_SESSION_TERMINATE:
{
{
          JingleSession *session = jingle_content_get_session(content);
          PurpleMedia *media = jingle_rtp_get_media(session);
          if (media != ((PurpleMedia *)((void *)0))) {
            purple_media_end(media,0,0);
          }
          g_object_unref(session);
          break; 
        }
      }
      case JINGLE_TRANSPORT_INFO:
{
{
          JingleSession *session = jingle_content_get_session(content);
          JingleTransport *transport = jingle_transport_parse(xmlnode_get_child(xmlcontent,"transport"));
          GList *candidates = jingle_rtp_transport_to_candidates(transport);
          gchar *name = jingle_content_get_name(content);
          gchar *remote_jid = jingle_session_get_remote_jid(session);
          purple_media_add_remote_candidates(jingle_rtp_get_media(session),name,remote_jid,candidates);
          g_free(remote_jid);
          g_free(name);
          g_object_unref(session);
          break; 
        }
      }
      case JINGLE_DESCRIPTION_INFO:
{
{
          JingleSession *session = jingle_content_get_session(content);
          xmlnode *description = xmlnode_get_child(xmlcontent,"description");
          GList *codecs;
          GList *iter;
          GList *iter2;
          GList *remote_codecs = jingle_rtp_parse_codecs(description);
          gchar *name = jingle_content_get_name(content);
          gchar *remote_jid = jingle_session_get_remote_jid(session);
          PurpleMedia *media;
          media = jingle_rtp_get_media(session);
/*
			 * This may have problems if description-info is
			 * received without the optional parameters for a
			 * codec with configuration info (such as THEORA
			 * or H264). The local configuration info may be
			 * set for the remote codec.
			 *
			 * As of 2.6.3 there's no API to support getting
			 * the remote codecs specifically, just the
			 * intersection. Another option may be to cache
			 * the remote codecs received in initiate/accept.
			 */
          codecs = purple_media_get_codecs(media,name);
          for (iter = codecs; iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
            guint id;
            id = purple_media_codec_get_id((iter -> data));
            iter2 = remote_codecs;
{
              for (; iter2 != 0; iter2 = ((iter2 != 0)?( *((GList *)iter2)).next : ((struct _GList *)((void *)0)))) {
                if (purple_media_codec_get_id((iter2 -> data)) != id) 
                  continue; 
                g_object_unref((iter -> data));
                iter -> data = (iter2 -> data);
                remote_codecs = g_list_delete_link(remote_codecs,iter2);
                break; 
              }
            }
          }
          codecs = g_list_concat(codecs,remote_codecs);
          purple_media_set_remote_codecs(media,name,remote_jid,codecs);
          purple_media_codec_list_free(codecs);
          g_free(remote_jid);
          g_free(name);
          g_object_unref(session);
          break; 
        }
      }
      default:
{
        break; 
      }
    }
  }
}

gboolean jingle_rtp_initiate_media(JabberStream *js,const gchar *who,PurpleMediaSessionType type)
{
/* create content negotiation */
  JingleSession *session;
  JingleContent *content;
  JingleTransport *transport;
  JabberBuddy *jb;
  JabberBuddyResource *jbr;
  const gchar *transport_type;
  gchar *resource = (gchar *)((void *)0);
  gchar *me = (gchar *)((void *)0);
  gchar *sid = (gchar *)((void *)0);
/* construct JID to send to */
  jb = jabber_buddy_find(js,who,0);
  if (!(jb != 0)) {
    purple_debug_error("jingle-rtp","Could not find Jabber buddy\n");
    return 0;
  }
  resource = jabber_get_resource(who);
  jbr = jabber_buddy_find_resource(jb,resource);
  g_free(resource);
  if (!(jbr != 0)) {
    purple_debug_error("jingle-rtp","Could not find buddy\'s resource - %s\n",resource);
    return 0;
  }
  if (jabber_resource_has_capability(jbr,"urn:xmpp:jingle:transports:ice-udp:1") != 0) {
    transport_type = "urn:xmpp:jingle:transports:ice-udp:1";
  }
  else if (jabber_resource_has_capability(jbr,"urn:xmpp:jingle:transports:raw-udp:1") != 0) {
    transport_type = "urn:xmpp:jingle:transports:raw-udp:1";
  }
  else {
    purple_debug_error("jingle-rtp","Resource doesn\'t support the same transport types\n");
    return 0;
  }
/* set ourselves as initiator */
  me = g_strdup_printf("%s@%s/%s",( *(js -> user)).node,( *(js -> user)).domain,( *(js -> user)).resource);
  sid = jabber_get_next_id(js);
  session = jingle_session_create(js,sid,me,who,(!0));
  g_free(sid);
  if ((type & PURPLE_MEDIA_AUDIO) != 0U) {
    transport = jingle_transport_create(transport_type);
    content = jingle_content_create("urn:xmpp:jingle:apps:rtp:1","initiator","session","audio-session","both",transport);
    jingle_session_add_content(session,content);
    ( *( *((JingleRtp *)(g_type_check_instance_cast(((GTypeInstance *)content),jingle_rtp_get_type())))).priv).media_type = g_strdup("audio");
    jingle_rtp_init_media(content);
  }
  if ((type & PURPLE_MEDIA_VIDEO) != 0U) {
    transport = jingle_transport_create(transport_type);
    content = jingle_content_create("urn:xmpp:jingle:apps:rtp:1","initiator","session","video-session","both",transport);
    jingle_session_add_content(session,content);
    ( *( *((JingleRtp *)(g_type_check_instance_cast(((GTypeInstance *)content),jingle_rtp_get_type())))).priv).media_type = g_strdup("video");
    jingle_rtp_init_media(content);
  }
  g_free(me);
  if (jingle_rtp_get_media(session) == ((PurpleMedia *)((void *)0))) {
    return 0;
  }
  return (!0);
}

void jingle_rtp_terminate_session(JabberStream *js,const gchar *who)
{
  JingleSession *session;
/* XXX: This may cause file transfers and xml sessions to stop as well */
  session = jingle_session_find_by_jid(js,who);
  if (session != 0) {
    PurpleMedia *media = jingle_rtp_get_media(session);
    if (media != 0) {
      purple_debug_info("jingle-rtp","hanging up media\n");
      purple_media_stream_info(media,PURPLE_MEDIA_INFO_HANGUP,0,0,(!0));
    }
  }
}
#endif /* USE_VV */
