/**
 * @file msgs.c
 *
 * purple
 *
 * Copyright (C) 2003, 2012 Ethan Blanton <elb@pidgin.im>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
/*
 * Note: If you change any of these functions to use additional args you
 * MUST ensure the arg count is correct in parse.c. Otherwise it may be
 * possible for a malicious server or man-in-the-middle to trigger a crash.
 */
#include "internal.h"
#include "conversation.h"
#include "blist.h"
#include "notify.h"
#include "util.h"
#include "debug.h"
#include "irc.h"
#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_CYRUS_SASL
#include <sasl/sasl.h>
#endif
static char *irc_mask_nick(const char *mask);
static char *irc_mask_userhost(const char *mask);
static void irc_chat_remove_buddy(PurpleConversation *convo,char *data[2UL]);
static void irc_buddy_status(char *name,struct irc_buddy *ib,struct irc_conn *irc);
static void irc_connected(struct irc_conn *irc,const char *nick);
static void irc_msg_handle_privmsg(struct irc_conn *irc,const char *name,const char *from,const char *to,const char *rawmsg,gboolean notice);
#ifdef HAVE_CYRUS_SASL
#endif

static char *irc_mask_nick(const char *mask)
{
  char *end;
  char *buf;
  end = strchr(mask,'!');
  if (!(end != 0)) 
    buf = g_strdup(mask);
  else 
    buf = g_strndup(mask,(end - mask));
  return buf;
}

static char *irc_mask_userhost(const char *mask)
{
  return g_strdup((strchr(mask,'!') + 1));
}

static void irc_chat_remove_buddy(PurpleConversation *convo,char *data[2UL])
{
  char *message;
  char *stripped;
  stripped = ((data[1] != 0)?irc_mirc2txt(data[1]) : ((char *)((void *)0)));
  message = g_strdup_printf("quit: %s",stripped);
  g_free(stripped);
  if (purple_conv_chat_find_user(purple_conversation_get_chat_data(convo),data[0]) != 0) 
    purple_conv_chat_remove_user(purple_conversation_get_chat_data(convo),data[0],message);
  g_free(message);
}

static void irc_connected(struct irc_conn *irc,const char *nick)
{
  PurpleConnection *gc;
  PurpleStatus *status;
  GSList *buddies;
  PurpleAccount *account;
  if (((gc = purple_account_get_connection((irc -> account))) == ((PurpleConnection *)((void *)0))) || ((purple_connection_get_state(gc)) == PURPLE_CONNECTED)) 
    return ;
  purple_connection_set_display_name(gc,nick);
  purple_connection_set_state(gc,PURPLE_CONNECTED);
  account = purple_connection_get_account(gc);
/* If we're away then set our away message */
  status = purple_account_get_active_status((irc -> account));
  if (purple_status_get_type(status) != PURPLE_STATUS_AVAILABLE) {
    PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)( *( *(gc -> prpl)).info).extra_info;
    ( *(prpl_info -> set_status))((irc -> account),status);
  }
/* this used to be in the core, but it's not now */
  for (buddies = purple_find_buddies(account,0); buddies != 0; buddies = g_slist_delete_link(buddies,buddies)) {
    PurpleBuddy *b = (buddies -> data);
    struct irc_buddy *ib = (struct irc_buddy *)(g_malloc0_n(1,(sizeof(struct irc_buddy ))));
    ib -> name = g_strdup(purple_buddy_get_name(b));
    ib -> ref = 1;
    g_hash_table_replace((irc -> buddies),(ib -> name),ib);
  }
  irc_blist_timeout(irc);
  if (!((irc -> timer) != 0U)) 
    irc -> timer = purple_timeout_add_seconds(45,((GSourceFunc )irc_blist_timeout),((gpointer )irc));
}
/* This function is ugly, but it's really an error handler. */

void irc_msg_default(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  int i;
  const char *end;
  const char *cur;
  const char *numeric = (const char *)((void *)0);
  char *clean;
  char *tmp;
  char *convname;
  PurpleConversation *convo;
  for (((cur = args[0]) , (i = 0)); i < 4; i++) {
    end = (strchr(cur,32));
    if (end == ((const char *)((void *)0))) {
      goto undirected;
    }
/* Check for 3-digit numeric in second position */
    if (i == 1) {
      if (((((end - cur) != 3) || !((( *__ctype_b_loc())[(int )cur[0]] & ((unsigned short )_ISdigit)) != 0)) || !((( *__ctype_b_loc())[(int )cur[1]] & ((unsigned short )_ISdigit)) != 0)) || !((( *__ctype_b_loc())[(int )cur[2]] & ((unsigned short )_ISdigit)) != 0)) {
        goto undirected;
      }
/* Save the numeric for printing to the channel */
      numeric = cur;
    }
/* Don't advance cur if we're on the final iteration. */
    if (i != 3) {
      cur = (end + 1);
    }
  }
/* At this point, cur is the beginning of the fourth position,
	 * end is the following space, and there are remaining
	 * arguments.  We'll check to see if this argument is a
	 * currently active conversation (private message or channel,
	 * either one), and print the numeric to that conversation if it
	 * is. */
  tmp = g_strndup(cur,(end - cur));
  convname = purple_utf8_salvage(tmp);
  g_free(tmp);
/* Check for an existing conversation */
  convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_ANY,convname,(irc -> account));
  g_free(convname);
  if (convo == ((PurpleConversation *)((void *)0))) {
    goto undirected;
  }
/* end + 1 is the first argument past the target.  The initial
	 * arguments we've skipped are routing info, numeric, recipient
	 * (this account's nick, most likely), and target (this
	 * channel).  If end + 1 is an ASCII :, skip it, because it's
	 * meaningless in this context.  This won't catch all
	 * :-arguments, but it'll catch the easy case. */
  if (( *(++end)) == ':') {
    end++;
  }
/* We then print "numeric: remainder". */
  clean = purple_utf8_salvage(end);
  tmp = g_strdup_printf("%.3s: %s",numeric,clean);
  g_free(clean);
  purple_conversation_write(convo,"",tmp,(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LOG | PURPLE_MESSAGE_RAW | PURPLE_MESSAGE_NO_LINKIFY),time(0));
  g_free(tmp);
  return ;
  undirected:
/* This, too, should be escaped somehow (smarter) */
  clean = purple_utf8_salvage(args[0]);
  purple_debug(PURPLE_DEBUG_INFO,"irc","Unrecognized message: %s\n",clean);
  g_free(clean);
}

void irc_msg_features(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  gchar **features;
  int i;
  features = g_strsplit(args[1]," ",(-1));
  for (i = 0; features[i] != 0; i++) {
    char *val;
    if (!(strncmp(features[i],"PREFIX=",7) != 0)) {
      if ((val = strchr((features[i] + 7),')')) != ((char *)((void *)0))) 
        irc -> mode_chars = g_strdup((val + 1));
    }
  }
  g_strfreev(features);
}

void irc_msg_luser(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  if (!(strcmp(name,"251") != 0)) {
/* 251 is required, so we pluck our nick from here and
		 * finalize connection */
    irc_connected(irc,args[0]);
/* Some IRC servers seem to not send a 255 numeric, so
		 * I guess we can't require it; 251 will do. */
/* } else if (!strcmp(name, "255")) { */
  }
}

void irc_msg_away(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc;
  char *msg;
  if ((irc -> whois.nick != 0) && !(purple_utf8_strcasecmp(irc -> whois.nick,args[1]) != 0)) {
/* We're doing a whois, show this in the whois dialog */
    irc_msg_whois(irc,name,from,args);
    return ;
  }
  gc = purple_account_get_connection((irc -> account));
  if (gc != 0) {
    msg = g_markup_escape_text(args[2],(-1));
    serv_got_im(gc,args[1],msg,PURPLE_MESSAGE_AUTO_RESP,time(0));
    g_free(msg);
  }
}

void irc_msg_badmode(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  do {
    if (gc != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc");
      return ;
    };
  }while (0);
  purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Bad mode"))),args[1],0,0);
}

void irc_msg_ban(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConversation *convo;
  convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,args[1],(irc -> account));
  if (!(strcmp(name,"367") != 0)) {
    char *msg = (char *)((void *)0);
/* Ban list entry */
    if ((args[3] != 0) && (args[4] != 0)) {
/* This is an extended syntax, not in RFC 1459 */
      int t1 = atoi(args[4]);
      time_t t2 = time(0);
      char *time = purple_str_seconds_to_string((t2 - t1));
      msg = g_strdup_printf(((const char *)(dgettext("pidgin","Ban on %s by %s, set %s ago"))),args[2],args[3],time);
      g_free(time);
    }
    else {
      msg = g_strdup_printf(((const char *)(dgettext("pidgin","Ban on %s"))),args[2]);
    }
    if (convo != 0) {
      purple_conv_chat_write(purple_conversation_get_chat_data(convo),"",msg,(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LOG),time(0));
    }
    else {
      purple_debug_info("irc","%s\n",msg);
    }
    g_free(msg);
  }
  else if (!(strcmp(name,"368") != 0)) {
    if (!(convo != 0)) 
      return ;
/* End of ban list */
    purple_conv_chat_write(purple_conversation_get_chat_data(convo),"",((const char *)(dgettext("pidgin","End of ban list"))),(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LOG),time(0));
  }
}

void irc_msg_banned(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  char *buf;
  do {
    if (gc != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc");
      return ;
    };
  }while (0);
  buf = g_strdup_printf(((const char *)(dgettext("pidgin","You are banned from %s."))),args[1]);
  purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Banned"))),((const char *)(dgettext("pidgin","Banned"))),buf,0,0);
  g_free(buf);
}

void irc_msg_banfull(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConversation *convo;
  char *buf;
  char *nick;
  convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,args[1],(irc -> account));
  if (!(convo != 0)) 
    return ;
  nick = g_markup_escape_text(args[2],(-1));
  buf = g_strdup_printf(((const char *)(dgettext("pidgin","Cannot ban %s: banlist is full"))),nick);
  g_free(nick);
  purple_conv_chat_write(purple_conversation_get_chat_data(convo),"",buf,(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LOG),time(0));
  g_free(buf);
}

void irc_msg_chanmode(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConversation *convo;
  char *buf;
  char *escaped;
  convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,args[1],(irc -> account));
/* XXX punt on channels we are not in for now */
  if (!(convo != 0)) 
    return ;
  escaped = ((args[3] != ((char *)((void *)0)))?g_markup_escape_text(args[3],(-1)) : ((char *)((void *)0)));
  buf = g_strdup_printf("mode for %s: %s %s",args[1],args[2],((escaped != 0)?escaped : ""));
  purple_conv_chat_write(purple_conversation_get_chat_data(convo),"",buf,PURPLE_MESSAGE_SYSTEM,time(0));
  g_free(escaped);
  g_free(buf);
}

void irc_msg_whois(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  if (!(irc -> whois.nick != 0)) {
    purple_debug(PURPLE_DEBUG_WARNING,"irc","Unexpected %s reply for %s\n",(!(strcmp(name,"314") != 0)?"WHOWAS" : "WHOIS"),args[1]);
    return ;
  }
  if (purple_utf8_strcasecmp(irc -> whois.nick,args[1]) != 0) {
    purple_debug(PURPLE_DEBUG_WARNING,"irc","Got %s reply for %s while waiting for %s\n",(!(strcmp(name,"314") != 0)?"WHOWAS" : "WHOIS"),args[1],irc -> whois.nick);
    return ;
  }
  if (!(strcmp(name,"301") != 0)) {
    irc -> whois.away = g_strdup(args[2]);
  }
  else if (!(strcmp(name,"311") != 0) || !(strcmp(name,"314") != 0)) {
    irc -> whois.ident = g_strdup(args[2]);
    irc -> whois.host = g_strdup(args[3]);
    irc -> whois.real = g_strdup(args[5]);
  }
  else if (!(strcmp(name,"312") != 0)) {
    irc -> whois.server = g_strdup(args[2]);
    irc -> whois.serverinfo = g_strdup(args[3]);
  }
  else if (!(strcmp(name,"313") != 0)) {
    irc -> whois.ircop = 1;
  }
  else if (!(strcmp(name,"317") != 0)) {
    irc -> whois.idle = atoi(args[2]);
    if (args[3] != 0) 
      irc -> whois.signon = ((time_t )(atoi(args[3])));
  }
  else if (!(strcmp(name,"319") != 0)) {
    if (irc -> whois.channels == ((GString *)((void *)0))) {
      irc -> whois.channels = g_string_new(args[2]);
    }
    else {
      irc -> whois.channels = g_string_append(irc -> whois.channels,args[2]);
    }
  }
  else if (!(strcmp(name,"320") != 0)) {
    irc -> whois.identified = 1;
  }
  else if (!(strcmp(name,"330") != 0)) {
    purple_debug(PURPLE_DEBUG_INFO,"irc","330 %s: 1=[%s] 2=[%s] 3=[%s]",name,args[1],args[2],args[3]);
    if (!(strcmp(args[3],"is logged in as") != 0)) 
      irc -> whois.login = g_strdup(args[2]);
  }
}

void irc_msg_endwhois(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc;
  char *tmp;
  char *tmp2;
  PurpleNotifyUserInfo *user_info;
  if (!(irc -> whois.nick != 0)) {
    purple_debug(PURPLE_DEBUG_WARNING,"irc","Unexpected End of %s for %s\n",(!(strcmp(name,"369") != 0)?"WHOWAS" : "WHOIS"),args[1]);
    return ;
  }
  if (purple_utf8_strcasecmp(irc -> whois.nick,args[1]) != 0) {
    purple_debug(PURPLE_DEBUG_WARNING,"irc","Received end of %s for %s, expecting %s\n",(!(strcmp(name,"369") != 0)?"WHOWAS" : "WHOIS"),args[1],irc -> whois.nick);
    return ;
  }
  user_info = purple_notify_user_info_new();
  tmp2 = g_markup_escape_text(args[1],(-1));
  tmp = g_strdup_printf("%s%s%s",tmp2,((irc -> whois.ircop != 0)?((const char *)(dgettext("pidgin"," <i>(ircop)</i>"))) : ""),((irc -> whois.identified != 0)?((const char *)(dgettext("pidgin"," <i>(identified)</i>"))) : ""));
  purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Nick"))),tmp);
  g_free(tmp2);
  g_free(tmp);
  if (irc -> whois.away != 0) {
    tmp = g_markup_escape_text(irc -> whois.away,(strlen(irc -> whois.away)));
    g_free(irc -> whois.away);
    purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Away"))),tmp);
    g_free(tmp);
  }
  if (irc -> whois.real != 0) {
    purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Real name"))),irc -> whois.real);
    g_free(irc -> whois.real);
  }
  if (irc -> whois.login != 0) {
    purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Login name"))),irc -> whois.login);
    g_free(irc -> whois.login);
  }
  if (irc -> whois.ident != 0) {
    purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Ident name"))),irc -> whois.ident);
    g_free(irc -> whois.ident);
  }
  if (irc -> whois.host != 0) {
    purple_notify_user_info_add_pair_plaintext(user_info,((const char *)(dgettext("pidgin","Host name"))),irc -> whois.host);
    g_free(irc -> whois.host);
  }
  if (irc -> whois.server != 0) {
    tmp = g_strdup_printf("%s (%s)",irc -> whois.server,irc -> whois.serverinfo);
    purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Server"))),tmp);
    g_free(tmp);
    g_free(irc -> whois.server);
    g_free(irc -> whois.serverinfo);
  }
  if (irc -> whois.channels != 0) {
    purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Currently on"))),( *irc -> whois.channels).str);
    g_string_free(irc -> whois.channels,(!0));
  }
  if (irc -> whois.idle != 0) {
    gchar *timex = purple_str_seconds_to_string(irc -> whois.idle);
    purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Idle for"))),timex);
    g_free(timex);
    purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Online since"))),purple_date_format_full((localtime((&irc -> whois.signon)))));
  }
  if (!(strcmp(irc -> whois.nick,"Paco-Paco") != 0)) {
    purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","<b>Defining adjective:</b>"))),((const char *)(dgettext("pidgin","Glorious"))));
  }
  gc = purple_account_get_connection((irc -> account));
  purple_notify_userinfo(gc,irc -> whois.nick,user_info,0,0);
  purple_notify_user_info_destroy(user_info);
  g_free(irc -> whois.nick);
  memset((&irc -> whois),0,(sizeof(irc -> whois)));
}

void irc_msg_who(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  if (!(strcmp(name,"352") != 0)) {
    PurpleConversation *conv;
    PurpleConvChat *chat;
    PurpleConvChatBuddy *cb;
    char *cur;
    char *userhost;
    char *realname;
    PurpleConvChatBuddyFlags flags;
    GList *keys = (GList *)((void *)0);
    GList *values = (GList *)((void *)0);
    conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,args[1],(irc -> account));
    if (!(conv != 0)) {
      purple_debug(PURPLE_DEBUG_ERROR,"irc","Got a WHO response for %s, which doesn\'t exist\n",args[1]);
      return ;
    }
    cb = purple_conv_chat_cb_find(purple_conversation_get_chat_data(conv),args[5]);
    if (!(cb != 0)) {
      purple_debug(PURPLE_DEBUG_ERROR,"irc","Got a WHO response for %s who isn\'t a buddy.\n",args[5]);
      return ;
    }
    chat = purple_conversation_get_chat_data(conv);
    userhost = g_strdup_printf("%s@%s",args[2],args[3]);
{
/* The final argument is a :-argument, but annoyingly
		 * contains two "words", the hop count and real name. */
      for (cur = args[7]; ( *cur) != 0; cur++) {
        if (( *cur) == 32) {
          cur++;
          break; 
        }
      }
    }
    realname = g_strdup(cur);
    keys = g_list_prepend(keys,"userhost");
    values = g_list_prepend(values,userhost);
    keys = g_list_prepend(keys,"realname");
    values = g_list_prepend(values,realname);
    purple_conv_chat_cb_set_attributes(chat,cb,keys,values);
    g_list_free(keys);
    g_list_free(values);
    g_free(userhost);
    g_free(realname);
    flags = (cb -> flags);
/* FIXME: I'm not sure this is really a good idea, now
		 * that we no longer do periodic WHO.  It seems to me
		 * like it's more likely to be confusing than not.
		 * Comments? */
    if ((args[6][0] == 'G') && !((flags & PURPLE_CBFLAGS_AWAY) != 0U)) {
      purple_conv_chat_user_set_flags(chat,(cb -> name),(flags | PURPLE_CBFLAGS_AWAY));
    }
    else if ((args[6][0] == 'H') && ((flags & PURPLE_CBFLAGS_AWAY) != 0U)) {
      purple_conv_chat_user_set_flags(chat,(cb -> name),(flags & (~PURPLE_CBFLAGS_AWAY)));
    }
  }
}

void irc_msg_list(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  if (!((irc -> roomlist) != 0)) 
    return ;
  if (!(strcmp(name,"321") != 0)) {
    purple_roomlist_set_in_progress((irc -> roomlist),(!0));
    return ;
  }
  if (!(strcmp(name,"323") != 0)) {
    purple_roomlist_set_in_progress((irc -> roomlist),0);
    purple_roomlist_unref((irc -> roomlist));
    irc -> roomlist = ((PurpleRoomlist *)((void *)0));
    return ;
  }
  if (!(strcmp(name,"322") != 0)) {
    PurpleRoomlistRoom *room;
    char *topic;
    if (!(purple_roomlist_get_in_progress((irc -> roomlist)) != 0)) {
      purple_debug_warning("irc","Buggy server didn\'t send RPL_LISTSTART.\n");
      purple_roomlist_set_in_progress((irc -> roomlist),(!0));
    }
    room = purple_roomlist_room_new(PURPLE_ROOMLIST_ROOMTYPE_ROOM,args[1],0);
    purple_roomlist_room_add_field((irc -> roomlist),room,args[1]);
    purple_roomlist_room_add_field((irc -> roomlist),room,((gpointer )(strtol(args[2],0,10))));
    topic = irc_mirc2txt(args[3]);
    purple_roomlist_room_add_field((irc -> roomlist),room,topic);
    g_free(topic);
    purple_roomlist_room_add((irc -> roomlist),room);
  }
}

void irc_msg_topic(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  char *chan;
  char *topic;
  char *msg;
  char *nick;
  char *tmp;
  char *tmp2;
  PurpleConversation *convo;
  if (!(strcmp(name,"topic") != 0)) {
    chan = args[0];
    topic = irc_mirc2txt(args[1]);
  }
  else {
    chan = args[1];
    topic = irc_mirc2txt(args[2]);
  }
  convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,chan,(irc -> account));
  if (!(convo != 0)) {
    purple_debug(PURPLE_DEBUG_ERROR,"irc","Got a topic for %s, which doesn\'t exist\n",chan);
    g_free(topic);
    return ;
  }
/* If this is an interactive update, print it out */
  tmp = g_markup_escape_text(topic,(-1));
  tmp2 = purple_markup_linkify(tmp);
  g_free(tmp);
  if (!(strcmp(name,"topic") != 0)) {
    const char *current_topic = purple_conv_chat_get_topic((purple_conversation_get_chat_data(convo)));
    if (!((current_topic != ((const char *)((void *)0))) && (strcmp(tmp2,current_topic) == 0))) {
      char *nick_esc;
      nick = irc_mask_nick(from);
      nick_esc = g_markup_escape_text(nick,(-1));
      purple_conv_chat_set_topic(purple_conversation_get_chat_data(convo),nick,topic);
      if (( *tmp2) != 0) 
        msg = g_strdup_printf(((const char *)(dgettext("pidgin","%s has changed the topic to: %s"))),nick_esc,tmp2);
      else 
        msg = g_strdup_printf(((const char *)(dgettext("pidgin","%s has cleared the topic."))),nick_esc);
      g_free(nick_esc);
      g_free(nick);
      purple_conv_chat_write(purple_conversation_get_chat_data(convo),from,msg,PURPLE_MESSAGE_SYSTEM,time(0));
      g_free(msg);
    }
  }
  else {
    char *chan_esc = g_markup_escape_text(chan,(-1));
    msg = g_strdup_printf(((const char *)(dgettext("pidgin","The topic for %s is: %s"))),chan_esc,tmp2);
    g_free(chan_esc);
    purple_conv_chat_set_topic(purple_conversation_get_chat_data(convo),0,topic);
    purple_conv_chat_write(purple_conversation_get_chat_data(convo),"",msg,PURPLE_MESSAGE_SYSTEM,time(0));
    g_free(msg);
  }
  g_free(tmp2);
  g_free(topic);
}

void irc_msg_topicinfo(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConversation *convo;
  struct tm *tm;
  time_t t;
  char *msg;
  char *timestamp;
  char *datestamp;
  convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,args[1],(irc -> account));
  if (!(convo != 0)) {
    purple_debug(PURPLE_DEBUG_ERROR,"irc","Got topic info for %s, which doesn\'t exist\n",args[1]);
    return ;
  }
  t = atol(args[3]);
  if (t == 0) {
    purple_debug(PURPLE_DEBUG_ERROR,"irc","Got apparently nonsensical topic timestamp %s\n",args[3]);
    return ;
  }
  tm = localtime((&t));
  timestamp = g_strdup(purple_time_format(tm));
  datestamp = g_strdup(purple_date_format_short(tm));
  msg = g_strdup_printf(((const char *)(dgettext("pidgin","Topic for %s set by %s at %s on %s"))),args[1],args[2],timestamp,datestamp);
  purple_conv_chat_write(purple_conversation_get_chat_data(convo),"",msg,(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LINKIFY),time(0));
  g_free(timestamp);
  g_free(datestamp);
  g_free(msg);
}

void irc_msg_unknown(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  char *buf;
  do {
    if (gc != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc");
      return ;
    };
  }while (0);
  buf = g_strdup_printf(((const char *)(dgettext("pidgin","Unknown message \'%s\'"))),args[1]);
  purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Unknown message"))),buf,((const char *)(dgettext("pidgin","The IRC server received a message it did not understand."))),0,0);
  g_free(buf);
}

void irc_msg_names(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  char *names;
  char *cur;
  char *end;
  char *tmp;
  char *msg;
  PurpleConversation *convo;
  if (!(strcmp(name,"366") != 0)) {
    convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_ANY,args[1],(irc -> account));
    if (!(convo != 0)) {
      purple_debug(PURPLE_DEBUG_ERROR,"irc","Got a NAMES list for %s, which doesn\'t exist\n",args[1]);
      g_string_free((irc -> names),(!0));
      irc -> names = ((GString *)((void *)0));
      return ;
    }
    names = (cur = g_string_free((irc -> names),0));
    irc -> names = ((GString *)((void *)0));
    if (purple_conversation_get_data(convo,"irc-namelist") != 0) {
      msg = g_strdup_printf(((const char *)(dgettext("pidgin","Users on %s: %s"))),args[1],((names != 0)?names : ""));
      if ((purple_conversation_get_type(convo)) == PURPLE_CONV_TYPE_CHAT) 
        purple_conv_chat_write(purple_conversation_get_chat_data(convo),"",msg,(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LOG),time(0));
      else 
        purple_conv_im_write(purple_conversation_get_im_data(convo),"",msg,(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LOG),time(0));
      g_free(msg);
    }
    else if (cur != ((char *)((void *)0))) {
      GList *users = (GList *)((void *)0);
      GList *flags = (GList *)((void *)0);
      while(( *cur) != 0){
        PurpleConvChatBuddyFlags f = PURPLE_CBFLAGS_NONE;
        end = strchr(cur,32);
        if (!(end != 0)) 
          end = (cur + strlen(cur));
        if (( *cur) == 64) {
          f = PURPLE_CBFLAGS_OP;
          cur++;
        }
        else if (( *cur) == '%') {
          f = PURPLE_CBFLAGS_HALFOP;
          cur++;
        }
        else if (( *cur) == '+') {
          f = PURPLE_CBFLAGS_VOICE;
          cur++;
        }
        else if (((irc -> mode_chars) != 0) && (strchr((irc -> mode_chars),( *cur)) != 0)) {
          if (( *cur) == '~') 
            f = PURPLE_CBFLAGS_FOUNDER;
          cur++;
        }
        tmp = g_strndup(cur,(end - cur));
        users = g_list_prepend(users,tmp);
        flags = g_list_prepend(flags,((gpointer )((glong )f)));
        cur = end;
        if (( *cur) != 0) 
          cur++;
      }
      if (users != ((GList *)((void *)0))) {
        GList *l;
        purple_conv_chat_add_users(purple_conversation_get_chat_data(convo),users,0,flags,0);
        for (l = users; l != ((GList *)((void *)0)); l = (l -> next)) 
          g_free((l -> data));
        g_list_free(users);
        g_list_free(flags);
      }
      purple_conversation_set_data(convo,"irc-namelist",((gpointer )((gpointer )((glong )(!0)))));
    }
    g_free(names);
  }
  else {
    if (!((irc -> names) != 0)) 
      irc -> names = g_string_new("");
    if ((( *(irc -> names)).len != 0UL) && (( *(irc -> names)).str[( *(irc -> names)).len - 1] != 32)) 
      irc -> names = g_string_append_c_inline((irc -> names),32);
    irc -> names = g_string_append((irc -> names),args[3]);
  }
}

void irc_msg_motd(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  char *escaped;
  if (!(strcmp(name,"375") != 0)) {
    if ((irc -> motd) != 0) 
      g_string_free((irc -> motd),(!0));
    irc -> motd = g_string_new("");
    return ;
  }
  else if (!(strcmp(name,"376") != 0)) {
/* dircproxy 1.0.5 does not send 251 on reconnection, so
		 * finalize the connection here if it is not already done. */
    irc_connected(irc,args[0]);
    return ;
  }
  else if (!(strcmp(name,"422") != 0)) {
/* in case there is no 251, and no MOTD set, finalize the connection.
		 * (and clear the motd for good measure). */
    if ((irc -> motd) != 0) 
      g_string_free((irc -> motd),(!0));
    irc_connected(irc,args[0]);
    return ;
  }
  if (!((irc -> motd) != 0)) {
    purple_debug_error("irc","IRC server sent MOTD without STARTMOTD\n");
    return ;
  }
  if (!(args[1] != 0)) 
    return ;
  escaped = g_markup_escape_text(args[1],(-1));
  g_string_append_printf((irc -> motd),"%s<br>",escaped);
  g_free(escaped);
}

void irc_msg_time(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  do {
    if (gc != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc");
      return ;
    };
  }while (0);
  purple_notify_message(gc,PURPLE_NOTIFY_MSG_INFO,((const char *)(dgettext("pidgin","Time Response"))),((const char *)(dgettext("pidgin","The IRC server\'s local time is:"))),args[2],0,0);
}

void irc_msg_nochan(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  do {
    if (gc != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc");
      return ;
    };
  }while (0);
  purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","No such channel"))),args[1],0,0);
}

void irc_msg_nonick(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc;
  PurpleConversation *convo;
  convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_ANY,args[1],(irc -> account));
  if (convo != 0) {
/* does this happen? */
    if ((purple_conversation_get_type(convo)) == PURPLE_CONV_TYPE_CHAT) 
      purple_conv_chat_write(purple_conversation_get_chat_data(convo),args[1],((const char *)(dgettext("pidgin","no such channel"))),(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LOG),time(0));
    else 
      purple_conv_im_write(purple_conversation_get_im_data(convo),args[1],((const char *)(dgettext("pidgin","User is not logged in"))),(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LOG),time(0));
  }
  else {
    if ((gc = purple_account_get_connection((irc -> account))) == ((PurpleConnection *)((void *)0))) 
      return ;
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","No such nick or channel"))),args[1],0,0);
  }
  if ((irc -> whois.nick != 0) && !(purple_utf8_strcasecmp(irc -> whois.nick,args[1]) != 0)) {
    g_free(irc -> whois.nick);
    irc -> whois.nick = ((char *)((void *)0));
  }
}

void irc_msg_nosend(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc;
  PurpleConversation *convo;
  convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,args[1],(irc -> account));
  if (convo != 0) {
    purple_conv_chat_write(purple_conversation_get_chat_data(convo),args[1],args[2],(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LOG),time(0));
  }
  else {
    if ((gc = purple_account_get_connection((irc -> account))) == ((PurpleConnection *)((void *)0))) 
      return ;
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Could not send"))),args[2],0,0);
  }
}

void irc_msg_notinchan(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConversation *convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,args[1],(irc -> account));
  purple_debug(PURPLE_DEBUG_INFO,"irc","We\'re apparently not in %s, but tried to use it\n",args[1]);
  if (convo != 0) {
/*g_slist_remove(irc->gc->buddy_chats, convo);
		  purple_conversation_set_account(convo, NULL);*/
    purple_conv_chat_write(purple_conversation_get_chat_data(convo),args[1],args[2],(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LOG),time(0));
  }
}

void irc_msg_notop(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConversation *convo;
  convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,args[1],(irc -> account));
  if (!(convo != 0)) 
    return ;
  purple_conv_chat_write(purple_conversation_get_chat_data(convo),"",args[2],PURPLE_MESSAGE_SYSTEM,time(0));
}

void irc_msg_invite(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  GHashTable *components;
  gchar *nick;
  do {
    if (gc != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc");
      return ;
    };
  }while (0);
  components = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
  nick = irc_mask_nick(from);
  g_hash_table_insert(components,(g_strdup("channel")),(g_strdup(args[1])));
  serv_got_chat_invite(gc,args[1],nick,0,components);
  g_free(nick);
}

void irc_msg_inviteonly(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  char *buf;
  do {
    if (gc != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc");
      return ;
    };
  }while (0);
  buf = g_strdup_printf(((const char *)(dgettext("pidgin","Joining %s requires an invitation."))),args[1]);
  purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Invitation only"))),((const char *)(dgettext("pidgin","Invitation only"))),buf,0,0);
  g_free(buf);
}

void irc_msg_ison(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  char **nicks;
  struct irc_buddy *ib;
  int i;
  nicks = g_strsplit(args[1]," ",(-1));
  for (i = 0; nicks[i] != 0; i++) {
    if ((ib = (g_hash_table_lookup((irc -> buddies),((gconstpointer )nicks[i])))) == ((struct irc_buddy *)((void *)0))) {
      continue; 
    }
    ib -> new_online_status = (!0);
  }
  g_strfreev(nicks);
  if ((irc -> ison_outstanding) != 0) 
    irc_buddy_query(irc);
  if (!((irc -> ison_outstanding) != 0)) 
    g_hash_table_foreach((irc -> buddies),((GHFunc )irc_buddy_status),((gpointer )irc));
}

static void irc_buddy_status(char *name,struct irc_buddy *ib,struct irc_conn *irc)
{
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  PurpleBuddy *buddy = purple_find_buddy((irc -> account),name);
  if (!(gc != 0) || !(buddy != 0)) 
    return ;
  if (((ib -> online) != 0) && !((ib -> new_online_status) != 0)) {
    purple_prpl_got_user_status((irc -> account),name,"offline",((void *)((void *)0)));
    ib -> online = 0;
  }
  else if (!((ib -> online) != 0) && ((ib -> new_online_status) != 0)) {
    purple_prpl_got_user_status((irc -> account),name,"available",((void *)((void *)0)));
    ib -> online = (!0);
  }
}

void irc_msg_join(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  PurpleConversation *convo;
  PurpleConvChat *chat;
  PurpleConvChatBuddy *cb;
  char *nick;
  char *userhost;
  char *buf;
  struct irc_buddy *ib;
  static int id = 1;
  do {
    if (gc != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc");
      return ;
    };
  }while (0);
  nick = irc_mask_nick(from);
  if (!(purple_utf8_strcasecmp(nick,purple_connection_get_display_name(gc)) != 0)) {
/* We are joining a channel for the first time */
    serv_got_joined_chat(gc,id++,args[0]);
    g_free(nick);
    convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,args[0],(irc -> account));
    if (convo == ((PurpleConversation *)((void *)0))) {
      purple_debug_error("irc","tried to join %s but couldn\'t\n",args[0]);
      return ;
    }
    purple_conversation_set_data(convo,"irc-namelist",0);
// Get the real name and user host for all participants.
    buf = irc_format(irc,"vc","WHO",args[0]);
    irc_send(irc,buf);
    g_free(buf);
/* Until purple_conversation_present does something that
		 * one would expect in Pidgin, this call produces buggy
		 * behavior both for the /join and auto-join cases. */
/* purple_conversation_present(convo); */
    return ;
  }
  convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,args[0],(irc -> account));
  if (convo == ((PurpleConversation *)((void *)0))) {
    purple_debug(PURPLE_DEBUG_ERROR,"irc","JOIN for %s failed\n",args[0]);
    g_free(nick);
    return ;
  }
  userhost = irc_mask_userhost(from);
  chat = purple_conversation_get_chat_data(convo);
  purple_conv_chat_add_user(chat,nick,userhost,PURPLE_CBFLAGS_NONE,(!0));
  cb = purple_conv_chat_cb_find(chat,nick);
  if (cb != 0) {
    purple_conv_chat_cb_set_attribute(chat,cb,"userhost",userhost);
  }
  if ((ib = (g_hash_table_lookup((irc -> buddies),nick))) != ((struct irc_buddy *)((void *)0))) {
    ib -> new_online_status = (!0);
    irc_buddy_status(nick,ib,irc);
  }
  g_free(userhost);
  g_free(nick);
}

void irc_msg_kick(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  PurpleConversation *convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,args[0],(irc -> account));
  char *nick;
  char *buf;
  do {
    if (gc != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc");
      return ;
    };
  }while (0);
  nick = irc_mask_nick(from);
  if (!(convo != 0)) {
    purple_debug(PURPLE_DEBUG_ERROR,"irc","Received a KICK for unknown channel %s\n",args[0]);
    g_free(nick);
    return ;
  }
  if (!(purple_utf8_strcasecmp(purple_connection_get_display_name(gc),args[1]) != 0)) {
    buf = g_strdup_printf(((const char *)(dgettext("pidgin","You have been kicked by %s: (%s)"))),nick,args[2]);
    purple_conv_chat_write(purple_conversation_get_chat_data(convo),args[0],buf,PURPLE_MESSAGE_SYSTEM,time(0));
    g_free(buf);
    serv_got_chat_left(gc,purple_conv_chat_get_id((purple_conversation_get_chat_data(convo))));
  }
  else {
    buf = g_strdup_printf(((const char *)(dgettext("pidgin","Kicked by %s (%s)"))),nick,args[2]);
    purple_conv_chat_remove_user(purple_conversation_get_chat_data(convo),args[1],buf);
    g_free(buf);
  }
  g_free(nick);
}

void irc_msg_mode(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConversation *convo;
  char *nick = irc_mask_nick(from);
  char *buf;
/* Channel	*/
  if ((( *args[0]) == '#') || (( *args[0]) == '&')) {
    char *escaped;
    convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,args[0],(irc -> account));
    if (!(convo != 0)) {
      purple_debug(PURPLE_DEBUG_ERROR,"irc","MODE received for %s, which we are not in\n",args[0]);
      g_free(nick);
      return ;
    }
    escaped = ((args[2] != ((char *)((void *)0)))?g_markup_escape_text(args[2],(-1)) : ((char *)((void *)0)));
    buf = g_strdup_printf(((const char *)(dgettext("pidgin","mode (%s %s) by %s"))),args[1],((escaped != 0)?escaped : ""),nick);
    purple_conv_chat_write(purple_conversation_get_chat_data(convo),args[0],buf,PURPLE_MESSAGE_SYSTEM,time(0));
    g_free(escaped);
    g_free(buf);
    if (args[2] != 0) {
      PurpleConvChatBuddyFlags newflag;
      PurpleConvChatBuddyFlags flags;
      char *mcur;
      char *cur;
      char *end;
      char *user;
      gboolean add = 0;
      mcur = args[1];
      cur = args[2];
      while((( *cur) != 0) && (( *mcur) != 0)){
        if ((( *mcur) == '+') || (( *mcur) == 45)) {
          add = ((( *mcur) == '+')?!0 : 0);
          mcur++;
          continue; 
        }
        end = strchr(cur,32);
        if (!(end != 0)) 
          end = (cur + strlen(cur));
        user = g_strndup(cur,(end - cur));
        flags = purple_conv_chat_user_get_flags(purple_conversation_get_chat_data(convo),user);
        newflag = PURPLE_CBFLAGS_NONE;
        if (( *mcur) == 'o') 
          newflag = PURPLE_CBFLAGS_OP;
        else if (( *mcur) == 'h') 
          newflag = PURPLE_CBFLAGS_HALFOP;
        else if (( *mcur) == 'v') 
          newflag = PURPLE_CBFLAGS_VOICE;
        else if ((((irc -> mode_chars) != 0) && (strchr((irc -> mode_chars),'~') != 0)) && (( *mcur) == 'q')) 
          newflag = PURPLE_CBFLAGS_FOUNDER;
        if (newflag != 0U) {
          if (add != 0) 
            flags |= newflag;
          else 
            flags &= (~newflag);
          purple_conv_chat_user_set_flags(purple_conversation_get_chat_data(convo),user,flags);
        }
        g_free(user);
        cur = end;
        if (( *cur) != 0) 
          cur++;
        if (( *mcur) != 0) 
          mcur++;
      }
    }
/* User		*/
  }
  else {
  }
  g_free(nick);
}

void irc_msg_nick(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  PurpleConversation *conv;
  GSList *chats;
  char *nick = irc_mask_nick(from);
  irc -> nickused = 0;
  if (!(gc != 0)) {
    g_free(nick);
    return ;
  }
  chats = (gc -> buddy_chats);
  if (!(purple_utf8_strcasecmp(nick,purple_connection_get_display_name(gc)) != 0)) {
    purple_connection_set_display_name(gc,args[0]);
  }
  while(chats != 0){
    PurpleConvChat *chat = purple_conversation_get_chat_data((chats -> data));
/* This is ugly ... */
    if (purple_conv_chat_find_user(chat,nick) != 0) 
      purple_conv_chat_rename_user(chat,nick,args[0]);
    chats = (chats -> next);
  }
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,nick,(irc -> account));
  if (conv != ((PurpleConversation *)((void *)0))) 
    purple_conversation_set_name(conv,args[0]);
  g_free(nick);
}

void irc_msg_badnick(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  if ((purple_connection_get_state(gc)) == PURPLE_CONNECTED) {
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Invalid nickname"))),((const char *)(dgettext("pidgin","Invalid nickname"))),((const char *)(dgettext("pidgin","Your selected nickname was rejected by the server.  It probably contains invalid characters."))),0,0);
  }
  else {
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_INVALID_SETTINGS,((const char *)(dgettext("pidgin","Your selected account name was rejected by the server.  It probably contains invalid characters."))));
  }
}

void irc_msg_nickused(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  char *newnick;
  char *buf;
  char *end;
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  if ((gc != 0) && ((purple_connection_get_state(gc)) == PURPLE_CONNECTED)) {
/* We only want to do the following dance if the connection
		   has not been successfully completed.  If it has, just
		   notify the user that their /nick command didn't go. */
    buf = g_strdup_printf(((const char *)(dgettext("pidgin","The nickname \"%s\" is already being used."))),(irc -> reqnick));
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Nickname in use"))),((const char *)(dgettext("pidgin","Nickname in use"))),buf,0,0);
    g_free(buf);
    g_free((irc -> reqnick));
    irc -> reqnick = ((char *)((void *)0));
    return ;
  }
  if ((strlen(args[1]) < strlen((irc -> reqnick))) || ((irc -> nickused) != 0)) 
    newnick = g_strdup(args[1]);
  else 
    newnick = g_strdup_printf("%s0",args[1]);
  end = ((newnick + strlen(newnick)) - 1);
/* try fallbacks */
  if ((( *end) < '9') && (( *end) >= '1')) {
     *end = (( *end) + 1);
  }
  else 
     *end = '1';
  g_free((irc -> reqnick));
  irc -> reqnick = newnick;
  irc -> nickused = (!0);
  purple_connection_set_display_name(purple_account_get_connection((irc -> account)),newnick);
  buf = irc_format(irc,"vn","NICK",newnick);
  irc_send(irc,buf);
  g_free(buf);
}

void irc_msg_notice(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  irc_msg_handle_privmsg(irc,name,from,args[0],args[1],(!0));
}

void irc_msg_nochangenick(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  do {
    if (gc != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc");
      return ;
    };
  }while (0);
  purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Cannot change nick"))),((const char *)(dgettext("pidgin","Could not change nick"))),args[2],0,0);
}

void irc_msg_part(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  PurpleConversation *convo;
  char *nick;
  char *msg;
  char *channel;
  do {
    if (gc != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc");
      return ;
    };
  }while (0);
/* Undernet likes to :-quote the channel name, for no good reason
	 * that I can see.  This catches that. */
  channel = ((args[0][0] == ':')?(args[0] + 1) : args[0]);
  convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,channel,(irc -> account));
  if (!(convo != 0)) {
    purple_debug(PURPLE_DEBUG_INFO,"irc","Got a PART on %s, which doesn\'t exist -- probably closed\n",channel);
    return ;
  }
  nick = irc_mask_nick(from);
  if (!(purple_utf8_strcasecmp(nick,purple_connection_get_display_name(gc)) != 0)) {
    char *escaped = (args[1] != 0)?g_markup_escape_text(args[1],(-1)) : ((char *)((void *)0));
    msg = g_strdup_printf(((const char *)(dgettext("pidgin","You have parted the channel%s%s"))),(((args[1] != 0) && (( *args[1]) != 0))?": " : ""),(((escaped != 0) && (( *escaped) != 0))?escaped : ""));
    g_free(escaped);
    purple_conv_chat_write(purple_conversation_get_chat_data(convo),channel,msg,PURPLE_MESSAGE_SYSTEM,time(0));
    g_free(msg);
    serv_got_chat_left(gc,purple_conv_chat_get_id((purple_conversation_get_chat_data(convo))));
  }
  else {
    msg = ((args[1] != 0)?irc_mirc2txt(args[1]) : ((char *)((void *)0)));
    purple_conv_chat_remove_user(purple_conversation_get_chat_data(convo),nick,msg);
    g_free(msg);
  }
  g_free(nick);
}

void irc_msg_ping(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  char *buf;
  buf = irc_format(irc,"v:","PONG",args[0]);
  irc_send(irc,buf);
  g_free(buf);
}

void irc_msg_pong(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConversation *convo;
  PurpleConnection *gc;
  char **parts;
  char *msg;
  time_t oldstamp;
  parts = g_strsplit(args[1]," ",2);
  if (!(parts[0] != 0) || !(parts[1] != 0)) {
    g_strfreev(parts);
    return ;
  }
  if (sscanf(parts[1],"%lu",&oldstamp) != 1) {
    msg = g_strdup(((const char *)(dgettext("pidgin","Error: invalid PONG from server"))));
  }
  else {
    msg = g_strdup_printf(((const char *)(dgettext("pidgin","PING reply -- Lag: %lu seconds"))),(time(0) - oldstamp));
  }
  convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_ANY,parts[0],(irc -> account));
  g_strfreev(parts);
  if (convo != 0) {
    if ((purple_conversation_get_type(convo)) == PURPLE_CONV_TYPE_CHAT) 
      purple_conv_chat_write(purple_conversation_get_chat_data(convo),"PONG",msg,(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LOG),time(0));
    else 
      purple_conv_im_write(purple_conversation_get_im_data(convo),"PONG",msg,(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LOG),time(0));
  }
  else {
    gc = purple_account_get_connection((irc -> account));
    if (!(gc != 0)) {
      g_free(msg);
      return ;
    }
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_INFO,0,"PONG",msg,0,0);
  }
  g_free(msg);
}

void irc_msg_privmsg(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  irc_msg_handle_privmsg(irc,name,from,args[0],args[1],0);
}

static void irc_msg_handle_privmsg(struct irc_conn *irc,const char *name,const char *from,const char *to,const char *rawmsg,gboolean notice)
{
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  PurpleConversation *convo;
  char *tmp;
  char *msg;
  char *nick;
  if (!(gc != 0)) 
    return ;
  nick = irc_mask_nick(from);
  tmp = irc_parse_ctcp(irc,nick,to,rawmsg,notice);
  if (!(tmp != 0)) {
    g_free(nick);
    return ;
  }
  msg = irc_escape_privmsg(tmp,(-1));
  g_free(tmp);
  tmp = irc_mirc2html(msg);
  g_free(msg);
  msg = tmp;
  if (notice != 0) {
    tmp = g_strdup_printf("(notice) %s",msg);
    g_free(msg);
    msg = tmp;
  }
  if (!(purple_utf8_strcasecmp(to,purple_connection_get_display_name(gc)) != 0)) {
    serv_got_im(gc,nick,msg,0,time(0));
  }
  else {
    convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,irc_nick_skip_mode(irc,to),(irc -> account));
    if (convo != 0) 
      serv_got_chat_in(gc,purple_conv_chat_get_id((purple_conversation_get_chat_data(convo))),nick,0,msg,time(0));
    else 
      purple_debug_error("irc","Got a %s on %s, which does not exist\n",((notice != 0)?"NOTICE" : "PRIVMSG"),to);
  }
  g_free(msg);
  g_free(nick);
}

void irc_msg_regonly(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  PurpleConversation *convo;
  char *msg;
  do {
    if (gc != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc");
      return ;
    };
  }while (0);
  convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,args[1],(irc -> account));
  if (convo != 0) {
/* This is a channel we're already in; for some reason,
		 * freenode feels the need to notify us that in some
		 * hypothetical other situation this might not have
		 * succeeded.  Suppress that. */
    return ;
  }
  msg = g_strdup_printf(((const char *)(dgettext("pidgin","Cannot join %s: Registration is required."))),args[1]);
  purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Cannot join channel"))),msg,args[2],0,0);
  g_free(msg);
}

void irc_msg_quit(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  struct irc_buddy *ib;
  char *data[2UL];
  do {
    if (gc != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc");
      return ;
    };
  }while (0);
  data[0] = irc_mask_nick(from);
  data[1] = args[0];
/* XXX this should have an API, I shouldn't grab this directly */
  g_slist_foreach((gc -> buddy_chats),((GFunc )irc_chat_remove_buddy),data);
  if ((ib = (g_hash_table_lookup((irc -> buddies),data[0]))) != ((struct irc_buddy *)((void *)0))) {
    ib -> new_online_status = 0;
    irc_buddy_status(data[0],ib,irc);
  }
  g_free(data[0]);
}

void irc_msg_unavailable(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Nick or channel is temporarily unavailable."))),args[1],0,0);
}

void irc_msg_wallops(struct irc_conn *irc,const char *name,const char *from,char **args)
{
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  char *nick;
  char *msg;
  do {
    if (gc != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc");
      return ;
    };
  }while (0);
  nick = irc_mask_nick(from);
  msg = g_strdup_printf(((const char *)(dgettext("pidgin","Wallops from %s"))),nick);
  g_free(nick);
  purple_notify_message(gc,PURPLE_NOTIFY_MSG_INFO,0,msg,args[0],0,0);
  g_free(msg);
}
#ifdef HAVE_CYRUS_SASL
/* Not an off-by-one because sasl_secret_t defines char data[1] */
/* TODO: This can probably be moved to glib's allocator */
/* Set up security properties and options */
/* Remove space which separated this mech from the next */
/* SASL authentication */
/* We need to do this to be able to list the mechanisms. */
/* Finish auth session */
/* We already received at least one AUTHENTICATE reply from the
	 * server. This suggests it supports this mechanism, but the
	 * password was incorrect. It would be better to abort and inform
	 * the user than to try again with a different mechanism, so they
	 * aren't told the server supports no worthy mechanisms.
	 */
/* Remove space which separated this mech from the next */
/* Only show an error if we did not abort ourselves. */
/* Auth failed, abort */
#endif

void irc_msg_ignore(struct irc_conn *irc,const char *name,const char *from,char **args)
{
}
