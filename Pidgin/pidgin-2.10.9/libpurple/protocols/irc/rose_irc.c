/**
 * @file irc.c
 *
 * purple
 *
 * Copyright (C) 2003, Robbert Haarman <purple@inglorion.net>
 * Copyright (C) 2003, 2012 Ethan Blanton <elb@pidgin.im>
 * Copyright (C) 2000-2003, Rob Flynn <rob@tgflinux.com>
 * Copyright (C) 1998-1999, Mark Spencer <markster@marko.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "accountopt.h"
#include "blist.h"
#include "conversation.h"
#include "debug.h"
#include "notify.h"
#include "prpl.h"
#include "plugin.h"
#include "util.h"
#include "version.h"
#include "irc.h"
#define PING_TIMEOUT 60
static void irc_ison_buddy_init(char *name,struct irc_buddy *ib,GList **list);
static const char *irc_blist_icon(PurpleAccount *a,PurpleBuddy *b);
static GList *irc_status_types(PurpleAccount *account);
static GList *irc_actions(PurplePlugin *plugin,gpointer context);
/* static GList *irc_chat_info(PurpleConnection *gc); */
static void irc_login(PurpleAccount *account);
static void irc_login_cb_ssl(gpointer data,PurpleSslConnection *gsc,PurpleInputCondition cond);
static void irc_login_cb(gpointer data,gint source,const gchar *error_message);
static void irc_ssl_connect_failure(PurpleSslConnection *gsc,PurpleSslErrorType error,gpointer data);
static void irc_close(PurpleConnection *gc);
static int irc_im_send(PurpleConnection *gc,const char *who,const char *what,PurpleMessageFlags flags);
static int irc_chat_send(PurpleConnection *gc,int id,const char *what,PurpleMessageFlags flags);
static void irc_chat_join(PurpleConnection *gc,GHashTable *data);
static void irc_input_cb(gpointer data,gint source,PurpleInputCondition cond);
static void irc_input_cb_ssl(gpointer data,PurpleSslConnection *gsc,PurpleInputCondition cond);
static guint irc_nick_hash(const char *nick);
static gboolean irc_nick_equal(const char *nick1,const char *nick2);
static void irc_buddy_free(struct irc_buddy *ib);
PurplePlugin *_irc_plugin = (PurplePlugin *)((void *)0);

static void irc_view_motd(PurplePluginAction *action)
{
  PurpleConnection *gc = (PurpleConnection *)(action -> context);
  struct irc_conn *irc;
  char *title;
  if ((gc == ((PurpleConnection *)((void *)0))) || ((gc -> proto_data) == ((void *)((void *)0)))) {
    purple_debug(PURPLE_DEBUG_ERROR,"irc","got MOTD request for NULL gc\n");
    return ;
  }
  irc = (gc -> proto_data);
  if ((irc -> motd) == ((GString *)((void *)0))) {
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Error displaying MOTD"))),((const char *)(dgettext("pidgin","No MOTD available"))),((const char *)(dgettext("pidgin","There is no MOTD associated with this connection."))),0,0);
    return ;
  }
  title = g_strdup_printf(((const char *)(dgettext("pidgin","MOTD for %s"))),(irc -> server));
  purple_notify_formatted(gc,title,title,0,( *(irc -> motd)).str,0,0);
  g_free(title);
}

static int do_send(struct irc_conn *irc,const char *buf,gsize len)
{
  int ret;
  if ((irc -> gsc) != 0) {
    ret = (purple_ssl_write((irc -> gsc),buf,len));
  }
  else {
    ret = (write((irc -> fd),buf,len));
  }
  return ret;
}

static int irc_send_raw(PurpleConnection *gc,const char *buf,int len)
{
  struct irc_conn *irc = (struct irc_conn *)(gc -> proto_data);
  if (len == -1) {
    len = (strlen(buf));
  }
  irc_send_len(irc,buf,len);
  return len;
}

static void irc_send_cb(gpointer data,gint source,PurpleInputCondition cond)
{
  struct irc_conn *irc = data;
  int ret;
  int writelen;
  writelen = (purple_circ_buffer_get_max_read((irc -> outbuf)));
  if (writelen == 0) {
    purple_input_remove((irc -> writeh));
    irc -> writeh = 0;
    return ;
  }
  ret = do_send(irc,( *(irc -> outbuf)).outptr,writelen);
  if ((ret < 0) && ( *__errno_location() == 11)) 
    return ;
  else if (ret <= 0) {
    PurpleConnection *gc = purple_account_get_connection((irc -> account));
    gchar *tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Lost connection with server: %s"))),g_strerror( *__errno_location()));
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,tmp);
    g_free(tmp);
    return ;
  }
  purple_circ_buffer_mark_read((irc -> outbuf),ret);
#if 0
/* We *could* try to write more if we wrote it all */
#endif
}

int irc_send(struct irc_conn *irc,const char *buf)
{
  return irc_send_len(irc,buf,(strlen(buf)));
}

int irc_send_len(struct irc_conn *irc,const char *buf,int buflen)
{
  int ret;
  char *tosend = g_strdup(buf);
  purple_signal_emit(_irc_plugin,"irc-sending-text",purple_account_get_connection((irc -> account)),&tosend);
  if (tosend == ((char *)((void *)0))) 
    return 0;
/* If we're not buffering writes, try to send immediately */
  if (!((irc -> writeh) != 0U)) 
    ret = do_send(irc,tosend,buflen);
  else {
    ret = -1;
     *__errno_location() = 11;
  }
/* purple_debug(PURPLE_DEBUG_MISC, "irc", "sent%s: %s",
		irc->gsc ? " (ssl)" : "", tosend); */
  if ((ret <= 0) && ( *__errno_location() != 11)) {
    PurpleConnection *gc = purple_account_get_connection((irc -> account));
    gchar *tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Lost connection with server: %s"))),g_strerror( *__errno_location()));
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,tmp);
    g_free(tmp);
  }
  else if (ret < buflen) {
    if (ret < 0) 
      ret = 0;
    if (!((irc -> writeh) != 0U)) 
      irc -> writeh = purple_input_add((((irc -> gsc) != 0)?( *(irc -> gsc)).fd : (irc -> fd)),PURPLE_INPUT_WRITE,irc_send_cb,irc);
    purple_circ_buffer_append((irc -> outbuf),(tosend + ret),(buflen - ret));
  }
  g_free(tosend);
  return ret;
}
/* XXX I don't like messing directly with these buddies */

gboolean irc_blist_timeout(struct irc_conn *irc)
{
  if ((irc -> ison_outstanding) != 0) {
    return (!0);
  }
  g_hash_table_foreach((irc -> buddies),((GHFunc )irc_ison_buddy_init),((gpointer *)(&irc -> buddies_outstanding)));
  irc_buddy_query(irc);
  return (!0);
}

void irc_buddy_query(struct irc_conn *irc)
{
  GList *lp;
  GString *string;
  struct irc_buddy *ib;
  char *buf;
  string = g_string_sized_new(512);
{
    while((lp = g_list_first((irc -> buddies_outstanding))) != 0){
      ib = ((struct irc_buddy *)(lp -> data));
      if ((((string -> len) + strlen((ib -> name))) + 1) > 450) 
        break; 
      g_string_append_printf(string,"%s ",(ib -> name));
      ib -> new_online_status = 0;
      irc -> buddies_outstanding = g_list_remove_link((irc -> buddies_outstanding),lp);
    }
  }
  if ((string -> len) != 0UL) {
    buf = irc_format(irc,"vn","ISON",(string -> str));
    irc_send(irc,buf);
    g_free(buf);
    irc -> ison_outstanding = (!0);
  }
  else 
    irc -> ison_outstanding = 0;
  g_string_free(string,(!0));
}

static void irc_ison_buddy_init(char *name,struct irc_buddy *ib,GList **list)
{
   *list = g_list_append( *list,ib);
}

static void irc_ison_one(struct irc_conn *irc,struct irc_buddy *ib)
{
  char *buf;
  if ((irc -> buddies_outstanding) != ((GList *)((void *)0))) {
    irc -> buddies_outstanding = g_list_append((irc -> buddies_outstanding),ib);
    return ;
  }
  ib -> new_online_status = 0;
  buf = irc_format(irc,"vn","ISON",(ib -> name));
  irc_send(irc,buf);
  g_free(buf);
}

static const char *irc_blist_icon(PurpleAccount *a,PurpleBuddy *b)
{
  return "irc";
}

static GList *irc_status_types(PurpleAccount *account)
{
  PurpleStatusType *type;
  GList *types = (GList *)((void *)0);
  type = purple_status_type_new(PURPLE_STATUS_AVAILABLE,0,0,(!0));
  types = g_list_append(types,type);
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_AWAY,0,0,(!0),(!0),0,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  types = g_list_append(types,type);
  type = purple_status_type_new(PURPLE_STATUS_OFFLINE,0,0,(!0));
  types = g_list_append(types,type);
  return types;
}

static GList *irc_actions(PurplePlugin *plugin,gpointer context)
{
  GList *list = (GList *)((void *)0);
  PurplePluginAction *act = (PurplePluginAction *)((void *)0);
  act = purple_plugin_action_new(((const char *)(dgettext("pidgin","View MOTD"))),irc_view_motd);
  list = g_list_append(list,act);
  return list;
}

static GList *irc_chat_join_info(PurpleConnection *gc)
{
  GList *m = (GList *)((void *)0);
  struct proto_chat_entry *pce;
  pce = ((struct proto_chat_entry *)(g_malloc0_n(1,(sizeof(struct proto_chat_entry )))));
  pce -> label = ((const char *)(dgettext("pidgin","_Channel:")));
  pce -> identifier = "channel";
  pce -> required = (!0);
  m = g_list_append(m,pce);
  pce = ((struct proto_chat_entry *)(g_malloc0_n(1,(sizeof(struct proto_chat_entry )))));
  pce -> label = ((const char *)(dgettext("pidgin","_Password:")));
  pce -> identifier = "password";
  pce -> secret = (!0);
  m = g_list_append(m,pce);
  return m;
}

static GHashTable *irc_chat_info_defaults(PurpleConnection *gc,const char *chat_name)
{
  GHashTable *defaults;
  defaults = g_hash_table_new_full(g_str_hash,g_str_equal,0,g_free);
  if (chat_name != ((const char *)((void *)0))) 
    g_hash_table_insert(defaults,"channel",(g_strdup(chat_name)));
  return defaults;
}

static void irc_login(PurpleAccount *account)
{
  PurpleConnection *gc;
  struct irc_conn *irc;
  char **userparts;
  const char *username = purple_account_get_username(account);
  gc = purple_account_get_connection(account);
  gc -> flags |= PURPLE_CONNECTION_NO_NEWLINES;
  if (strpbrk(username," \t\v\r\n") != ((char *)((void *)0))) {
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_INVALID_SETTINGS,((const char *)(dgettext("pidgin","IRC nick and server may not contain whitespace"))));
    return ;
  }
  gc -> proto_data = (irc = ((struct irc_conn *)(g_malloc0_n(1,(sizeof(struct irc_conn ))))));
  irc -> fd = -1;
  irc -> account = account;
  irc -> outbuf = purple_circ_buffer_new(512);
  userparts = g_strsplit(username,"@",2);
  purple_connection_set_display_name(gc,userparts[0]);
  irc -> server = g_strdup(userparts[1]);
  g_strfreev(userparts);
  irc -> buddies = g_hash_table_new_full(((GHashFunc )irc_nick_hash),((GEqualFunc )irc_nick_equal),0,((GDestroyNotify )irc_buddy_free));
  irc -> cmds = g_hash_table_new(g_str_hash,g_str_equal);
  irc_cmd_table_build(irc);
  irc -> msgs = g_hash_table_new(g_str_hash,g_str_equal);
  irc_msg_table_build(irc);
  purple_connection_update_progress(gc,((const char *)(dgettext("pidgin","Connecting"))),1,2);
  if (purple_account_get_bool(account,"ssl",0) != 0) {
    if (purple_ssl_is_supported() != 0) {
      irc -> gsc = purple_ssl_connect(account,(irc -> server),purple_account_get_int(account,"port",994),irc_login_cb_ssl,irc_ssl_connect_failure,gc);
    }
    else {
      purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NO_SSL_SUPPORT,((const char *)(dgettext("pidgin","SSL support unavailable"))));
      return ;
    }
  }
  if (!((irc -> gsc) != 0)) {
    if (purple_proxy_connect(gc,account,(irc -> server),purple_account_get_int(account,"port",6667),irc_login_cb,gc) == ((PurpleProxyConnectData *)((void *)0))) {
      purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Unable to connect"))));
      return ;
    }
  }
}

static gboolean do_login(PurpleConnection *gc)
{
  char *buf;
  char *tmp = (char *)((void *)0);
  char *server;
  const char *nickname;
  const char *identname;
  const char *realname;
  struct irc_conn *irc = (gc -> proto_data);
  const char *pass = purple_connection_get_password(gc);
#ifdef HAVE_CYRUS_SASL
#endif
  if ((pass != 0) && (( *pass) != 0)) {
#ifdef HAVE_CYRUS_SASL
/* intended to fall through */
#endif
    buf = irc_format(irc,"v:","PASS",pass);
    if (irc_send(irc,buf) < 0) {
      g_free(buf);
      return 0;
    }
    g_free(buf);
  }
  realname = purple_account_get_string((irc -> account),"realname","");
  identname = purple_account_get_string((irc -> account),"username","");
  if ((identname == ((const char *)((void *)0))) || (( *identname) == 0)) {
    identname = g_get_user_name();
  }
  if ((identname != ((const char *)((void *)0))) && (strchr(identname,32) != ((char *)((void *)0)))) {
    tmp = g_strdup(identname);
    while((buf = strchr(tmp,32)) != ((char *)((void *)0))){
       *buf = '_';
    }
  }
  if (( *(irc -> server)) == ':') {
/* Same as hostname, above. */
    server = g_strdup_printf("0%s",(irc -> server));
  }
  else {
    server = g_strdup((irc -> server));
  }
  buf = irc_format(irc,"vvvv:","USER",((tmp != 0)?tmp : identname),"*",server,((strlen(realname) != 0UL)?realname : "purple"));
  g_free(tmp);
  g_free(server);
  if (irc_send(irc,buf) < 0) {
    g_free(buf);
    return 0;
  }
  g_free(buf);
  nickname = purple_connection_get_display_name(gc);
  buf = irc_format(irc,"vn","NICK",nickname);
  irc -> reqnick = g_strdup(nickname);
  irc -> nickused = 0;
  if (irc_send(irc,buf) < 0) {
    g_free(buf);
    return 0;
  }
  g_free(buf);
  irc -> recv_time = time(0);
  return (!0);
}

static void irc_login_cb_ssl(gpointer data,PurpleSslConnection *gsc,PurpleInputCondition cond)
{
  PurpleConnection *gc = data;
  if (do_login(gc) != 0) {
    purple_ssl_input_add(gsc,irc_input_cb_ssl,gc);
  }
}

static void irc_login_cb(gpointer data,gint source,const gchar *error_message)
{
  PurpleConnection *gc = data;
  struct irc_conn *irc = (gc -> proto_data);
  if (source < 0) {
    gchar *tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to connect: %s"))),error_message);
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,tmp);
    g_free(tmp);
    return ;
  }
  irc -> fd = source;
  if (do_login(gc) != 0) {
    gc -> inpa = (purple_input_add((irc -> fd),PURPLE_INPUT_READ,irc_input_cb,gc));
  }
}

static void irc_ssl_connect_failure(PurpleSslConnection *gsc,PurpleSslErrorType error,gpointer data)
{
  PurpleConnection *gc = data;
  struct irc_conn *irc = (gc -> proto_data);
  irc -> gsc = ((PurpleSslConnection *)((void *)0));
  purple_connection_ssl_error(gc,error);
}

static void irc_close(PurpleConnection *gc)
{
  struct irc_conn *irc = (gc -> proto_data);
  if (irc == ((struct irc_conn *)((void *)0))) 
    return ;
  if (((irc -> gsc) != 0) || ((irc -> fd) >= 0)) 
    irc_cmd_quit(irc,"quit",0,0);
  if ((gc -> inpa) != 0) 
    purple_input_remove((gc -> inpa));
  g_free((irc -> inbuf));
  if ((irc -> gsc) != 0) {
    purple_ssl_close((irc -> gsc));
  }
  else if ((irc -> fd) >= 0) {
    close((irc -> fd));
  }
  if ((irc -> timer) != 0U) 
    purple_timeout_remove((irc -> timer));
  g_hash_table_destroy((irc -> cmds));
  g_hash_table_destroy((irc -> msgs));
  g_hash_table_destroy((irc -> buddies));
  if ((irc -> motd) != 0) 
    g_string_free((irc -> motd),(!0));
  g_free((irc -> server));
  if ((irc -> writeh) != 0U) 
    purple_input_remove((irc -> writeh));
  purple_circ_buffer_destroy((irc -> outbuf));
  g_free((irc -> mode_chars));
  g_free((irc -> reqnick));
#ifdef HAVE_CYRUS_SASL
#endif
  g_free(irc);
}

static int irc_im_send(PurpleConnection *gc,const char *who,const char *what,PurpleMessageFlags flags)
{
  struct irc_conn *irc = (gc -> proto_data);
  char *plain;
  const char *args[2UL];
  args[0] = irc_nick_skip_mode(irc,who);
  purple_markup_html_to_xhtml(what,0,&plain);
  args[1] = plain;
  irc_cmd_privmsg(irc,"msg",0,args);
  g_free(plain);
  return 1;
}

static void irc_get_info(PurpleConnection *gc,const char *who)
{
  struct irc_conn *irc = (gc -> proto_data);
  const char *args[2UL];
  args[0] = who;
  args[1] = ((const char *)((void *)0));
  irc_cmd_whois(irc,"whois",0,args);
}

static void irc_set_status(PurpleAccount *account,PurpleStatus *status)
{
  PurpleConnection *gc = purple_account_get_connection(account);
  struct irc_conn *irc;
  const char *args[1UL];
  const char *status_id = purple_status_get_id(status);
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  irc = (gc -> proto_data);
  if (!(purple_status_is_active(status) != 0)) 
    return ;
  args[0] = ((const char *)((void *)0));
  if (!(strcmp(status_id,"away") != 0)) {
    args[0] = purple_status_get_attr_string(status,"message");
    if ((args[0] == ((const char *)((void *)0))) || (( *args[0]) == 0)) 
      args[0] = ((const char *)(dgettext("pidgin","Away")));
    irc_cmd_away(irc,"away",0,args);
  }
  else if (!(strcmp(status_id,"available") != 0)) {
    irc_cmd_away(irc,"back",0,args);
  }
}

static void irc_add_buddy(PurpleConnection *gc,PurpleBuddy *buddy,PurpleGroup *group)
{
  struct irc_conn *irc = (struct irc_conn *)(gc -> proto_data);
  struct irc_buddy *ib;
  const char *bname = purple_buddy_get_name(buddy);
  ib = (g_hash_table_lookup((irc -> buddies),bname));
  if (ib != ((struct irc_buddy *)((void *)0))) {
    ib -> ref++;
    purple_prpl_got_user_status((irc -> account),bname,(((ib -> online) != 0)?"available" : "offline"),((void *)((void *)0)));
  }
  else {
    ib = ((struct irc_buddy *)(g_malloc0_n(1,(sizeof(struct irc_buddy )))));
    ib -> name = g_strdup(bname);
    ib -> ref = 1;
    g_hash_table_replace((irc -> buddies),(ib -> name),ib);
  }
/* if the timer isn't set, this is during signon, so we don't want to flood
	 * ourself off with ISON's, so we don't, but after that we want to know when
	 * someone's online asap */
  if ((irc -> timer) != 0U) 
    irc_ison_one(irc,ib);
}

static void irc_remove_buddy(PurpleConnection *gc,PurpleBuddy *buddy,PurpleGroup *group)
{
  struct irc_conn *irc = (struct irc_conn *)(gc -> proto_data);
  struct irc_buddy *ib;
  ib = (g_hash_table_lookup((irc -> buddies),(purple_buddy_get_name(buddy))));
  if ((ib != 0) && (--ib -> ref == 0)) {
    g_hash_table_remove((irc -> buddies),(purple_buddy_get_name(buddy)));
  }
}

static void read_input(struct irc_conn *irc,int len)
{
  char *cur;
  char *end;
  ( *( *(irc -> account)).gc).last_received = time(0);
  irc -> inbufused += len;
  (irc -> inbuf)[irc -> inbufused] = 0;
  cur = (irc -> inbuf);
/* This is a hack to work around the fact that marv gets messages
	 * with null bytes in them while using some weird irc server at work
	 */
  while((cur < ((irc -> inbuf) + (irc -> inbufused))) && !(( *cur) != 0))
    cur++;
  while((cur < ((irc -> inbuf) + (irc -> inbufused))) && (((end = strstr(cur,"\r\n")) != 0) || ((end = strstr(cur,"\n")) != 0))){
    int step = (( *end) == 13)?2 : 1;
     *end = 0;
    irc_parse_msg(irc,cur);
    cur = (end + step);
  }
/* leftover */
  if (cur != ((irc -> inbuf) + (irc -> inbufused))) {
    irc -> inbufused -= (cur - (irc -> inbuf));
    memmove((irc -> inbuf),cur,(irc -> inbufused));
  }
  else {
    irc -> inbufused = 0;
  }
}

static void irc_input_cb_ssl(gpointer data,PurpleSslConnection *gsc,PurpleInputCondition cond)
{
  PurpleConnection *gc = data;
  struct irc_conn *irc = (gc -> proto_data);
  int len;
  if (!(g_list_find(purple_connections_get_all(),gc) != 0)) {
    purple_ssl_close(gsc);
    return ;
  }
  if ((irc -> inbuflen) < ((irc -> inbufused) + 1024)) {
    irc -> inbuflen += 1024;
    irc -> inbuf = (g_realloc((irc -> inbuf),(irc -> inbuflen)));
  }
  len = (purple_ssl_read(gsc,((irc -> inbuf) + (irc -> inbufused)),(1024 - 1)));
  if ((len < 0) && ( *__errno_location() == 11)) {
/* Try again later */
    return ;
  }
  else if (len < 0) {
    gchar *tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Lost connection with server: %s"))),g_strerror( *__errno_location()));
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,tmp);
    g_free(tmp);
    return ;
  }
  else if (len == 0) {
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Server closed the connection"))));
    return ;
  }
  read_input(irc,len);
}

static void irc_input_cb(gpointer data,gint source,PurpleInputCondition cond)
{
  PurpleConnection *gc = data;
  struct irc_conn *irc = (gc -> proto_data);
  int len;
  if ((irc -> inbuflen) < ((irc -> inbufused) + 1024)) {
    irc -> inbuflen += 1024;
    irc -> inbuf = (g_realloc((irc -> inbuf),(irc -> inbuflen)));
  }
  len = (read((irc -> fd),((irc -> inbuf) + (irc -> inbufused)),(1024 - 1)));
  if ((len < 0) && ( *__errno_location() == 11)) {
    return ;
  }
  else if (len < 0) {
    gchar *tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Lost connection with server: %s"))),g_strerror( *__errno_location()));
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,tmp);
    g_free(tmp);
    return ;
  }
  else if (len == 0) {
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Server closed the connection"))));
    return ;
  }
  read_input(irc,len);
}

static void irc_chat_join(PurpleConnection *gc,GHashTable *data)
{
  struct irc_conn *irc = (gc -> proto_data);
  const char *args[2UL];
  args[0] = (g_hash_table_lookup(data,"channel"));
  args[1] = (g_hash_table_lookup(data,"password"));
  irc_cmd_join(irc,"join",0,args);
}

static char *irc_get_chat_name(GHashTable *data)
{
  return g_strdup((g_hash_table_lookup(data,"channel")));
}

static void irc_chat_invite(PurpleConnection *gc,int id,const char *message,const char *name)
{
  struct irc_conn *irc = (gc -> proto_data);
  PurpleConversation *convo = purple_find_chat(gc,id);
  const char *args[2UL];
  if (!(convo != 0)) {
    purple_debug(PURPLE_DEBUG_ERROR,"irc","Got chat invite request for bogus chat\n");
    return ;
  }
  args[0] = name;
  args[1] = purple_conversation_get_name(convo);
  irc_cmd_invite(irc,"invite",purple_conversation_get_name(convo),args);
}

static void irc_chat_leave(PurpleConnection *gc,int id)
{
  struct irc_conn *irc = (gc -> proto_data);
  PurpleConversation *convo = purple_find_chat(gc,id);
  const char *args[2UL];
  if (!(convo != 0)) 
    return ;
  args[0] = purple_conversation_get_name(convo);
  args[1] = ((const char *)((void *)0));
  irc_cmd_part(irc,"part",purple_conversation_get_name(convo),args);
  serv_got_chat_left(gc,id);
}

static int irc_chat_send(PurpleConnection *gc,int id,const char *what,PurpleMessageFlags flags)
{
  struct irc_conn *irc = (gc -> proto_data);
  PurpleConversation *convo = purple_find_chat(gc,id);
  const char *args[2UL];
  char *tmp;
  if (!(convo != 0)) {
    purple_debug(PURPLE_DEBUG_ERROR,"irc","chat send on nonexistent chat\n");
    return -22;
  }
#if 0
#endif
  purple_markup_html_to_xhtml(what,0,&tmp);
  args[0] = (convo -> name);
  args[1] = tmp;
  irc_cmd_privmsg(irc,"msg",0,args);
  serv_got_chat_in(gc,id,purple_connection_get_display_name(gc),flags,what,time(0));
  g_free(tmp);
  return 0;
}

static guint irc_nick_hash(const char *nick)
{
  char *lc;
  guint bucket;
  lc = g_utf8_strdown(nick,(-1));
  bucket = g_str_hash(lc);
  g_free(lc);
  return bucket;
}

static gboolean irc_nick_equal(const char *nick1,const char *nick2)
{
  return purple_utf8_strcasecmp(nick1,nick2) == 0;
}

static void irc_buddy_free(struct irc_buddy *ib)
{
  g_free((ib -> name));
  g_free(ib);
}

static void irc_chat_set_topic(PurpleConnection *gc,int id,const char *topic)
{
  char *buf;
  const char *name = (const char *)((void *)0);
  struct irc_conn *irc;
  irc = (gc -> proto_data);
  name = purple_conversation_get_name((purple_find_chat(gc,id)));
  if (name == ((const char *)((void *)0))) 
    return ;
  buf = irc_format(irc,"vt:","TOPIC",name,topic);
  irc_send(irc,buf);
  g_free(buf);
}

static PurpleRoomlist *irc_roomlist_get_list(PurpleConnection *gc)
{
  struct irc_conn *irc;
  GList *fields = (GList *)((void *)0);
  PurpleRoomlistField *f;
  char *buf;
  irc = (gc -> proto_data);
  if ((irc -> roomlist) != 0) 
    purple_roomlist_unref((irc -> roomlist));
  irc -> roomlist = purple_roomlist_new(purple_connection_get_account(gc));
  f = purple_roomlist_field_new(PURPLE_ROOMLIST_FIELD_STRING,"","channel",(!0));
  fields = g_list_append(fields,f);
  f = purple_roomlist_field_new(PURPLE_ROOMLIST_FIELD_INT,((const char *)(dgettext("pidgin","Users"))),"users",0);
  fields = g_list_append(fields,f);
  f = purple_roomlist_field_new(PURPLE_ROOMLIST_FIELD_STRING,((const char *)(dgettext("pidgin","Topic"))),"topic",0);
  fields = g_list_append(fields,f);
  purple_roomlist_set_fields((irc -> roomlist),fields);
  buf = irc_format(irc,"v","LIST");
  irc_send(irc,buf);
  g_free(buf);
  return irc -> roomlist;
}

static void irc_roomlist_cancel(PurpleRoomlist *list)
{
  PurpleConnection *gc = purple_account_get_connection((list -> account));
  struct irc_conn *irc;
  if (gc == ((PurpleConnection *)((void *)0))) 
    return ;
  irc = (gc -> proto_data);
  purple_roomlist_set_in_progress(list,0);
  if ((irc -> roomlist) == list) {
    irc -> roomlist = ((PurpleRoomlist *)((void *)0));
    purple_roomlist_unref(list);
  }
}

static void irc_keepalive(PurpleConnection *gc)
{
  struct irc_conn *irc = (gc -> proto_data);
  if ((time(0) - (irc -> recv_time)) > 60) 
    irc_cmd_ping(irc,0,0,0);
}
static PurplePluginProtocolInfo prpl_info = {((OPT_PROTO_CHAT_TOPIC | OPT_PROTO_PASSWORD_OPTIONAL | OPT_PROTO_SLASH_COMMANDS_NATIVE)), ((GList *)((void *)0)), ((GList *)((void *)0)), 
/* user_splits */
/* protocol_options */
/* icon_spec */
{((char *)((void *)0)), (0), (0), (0), (0), (0), (0)}, (irc_blist_icon), ((const char *(*)(PurpleBuddy *))((void *)0)), ((char *(*)(PurpleBuddy *))((void *)0)), ((void (*)(PurpleBuddy *, PurpleNotifyUserInfo *, gboolean ))((void *)0)), (irc_status_types), ((GList *(*)(PurpleBlistNode *))((void *)0)), (irc_chat_join_info), (irc_chat_info_defaults), (irc_login), (irc_close), (irc_im_send), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((unsigned int (*)(PurpleConnection *, const char *, PurpleTypingState ))((void *)0)), (irc_get_info), (irc_set_status), ((void (*)(PurpleConnection *, int ))((void *)0)), ((void (*)(PurpleConnection *, const char *, const char *))((void *)0)), (irc_add_buddy), ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)), (irc_remove_buddy), ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *))((void *)0)), (irc_chat_join), ((void (*)(PurpleConnection *, GHashTable *))((void *)0)), (irc_get_chat_name), (irc_chat_invite), (irc_chat_leave), ((void (*)(PurpleConnection *, int , const char *, const char *))((void *)0)), (irc_chat_send), (irc_keepalive), ((void (*)(PurpleAccount *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *, const char *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *, PurpleGroup *, GList *))((void *)0)), ((void (*)(PurpleBuddy *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), (purple_normalize_nocase), ((void (*)(PurpleConnection *, PurpleStoredImage *))((void *)0)), ((void (*)(PurpleConnection *, PurpleGroup *))((void *)0)), ((char *(*)(PurpleConnection *, int , const char *))((void *)0)), (irc_chat_set_topic), ((PurpleChat *(*)(PurpleAccount *, const char *))((void *)0)), (irc_roomlist_get_list), (irc_roomlist_cancel), ((void (*)(PurpleRoomlist *, PurpleRoomlistRoom *))((void *)0)), ((gboolean (*)(PurpleConnection *, const char *))((void *)0)), (irc_dccsend_send_file), (irc_dccsend_new_xfer), ((gboolean (*)(const PurpleBuddy *))((void *)0)), ((PurpleWhiteboardPrplOps *)((void *)0)), (irc_send_raw), ((char *(*)(PurpleRoomlistRoom *))((void *)0)), ((void (*)(PurpleAccount *, PurpleAccountUnregistrationCb , void *))((void *)0)), ((gboolean (*)(PurpleConnection *, const char *, guint ))((void *)0)), ((GList *(*)(PurpleAccount *))((void *)0)), ((sizeof(PurplePluginProtocolInfo ))), ((GHashTable *(*)(PurpleAccount *))((void *)0)), ((gboolean (*)(PurpleAccount *, const char *, PurpleMediaSessionType ))((void *)0)), ((PurpleMediaCaps (*)(PurpleAccount *, const char *))((void *)0)), ((PurpleMood *(*)(PurpleAccount *))((void *)0)), ((void (*)(PurpleConnection *, const char *, PurpleSetPublicAliasSuccessCallback , PurpleSetPublicAliasFailureCallback ))((void *)0)), ((void (*)(PurpleConnection *, PurpleGetPublicAliasSuccessCallback , PurpleGetPublicAliasFailureCallback ))((void *)0)), ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *, const char *))((void *)0)), ((void (*)(PurpleConnection *, GList *, GList *, const char *))((void *)0))
/* list_icon */
/* list_emblems */
/* status_text */
/* tooltip_text */
/* away_states */
/* blist_node_menu */
/* chat_info */
/* chat_info_defaults */
/* login */
/* close */
/* send_im */
/* set_info */
/* send_typing */
/* get_info */
/* set_status */
/* set_idle */
/* change_passwd */
/* add_buddy */
/* add_buddies */
/* remove_buddy */
/* remove_buddies */
/* add_permit */
/* add_deny */
/* rem_permit */
/* rem_deny */
/* set_permit_deny */
/* join_chat */
/* reject_chat */
/* get_chat_name */
/* chat_invite */
/* chat_leave */
/* chat_whisper */
/* chat_send */
/* keepalive */
/* register_user */
/* get_cb_info */
/* get_cb_away */
/* alias_buddy */
/* group_buddy */
/* rename_group */
/* buddy_free */
/* convo_closed */
/* normalize */
/* set_buddy_icon */
/* remove_group */
/* get_cb_real_name */
/* set_chat_topic */
/* find_blist_chat */
/* roomlist_get_list */
/* roomlist_cancel */
/* roomlist_expand_category */
/* can_receive_file */
/* send_file */
/* new_xfer */
/* offline_message */
/* whiteboard_prpl_ops */
/* send_raw */
/* roomlist_room_serialize */
/* unregister_user */
/* send_attention */
/* get_attention_types */
/* struct_size */
/* get_account_text_table */
/* initiate_media */
/* get_media_caps */
/* get_moods */
/* set_public_alias */
/* get_public_alias */
/* add_buddy_with_invite */
/* add_buddies_with_invite */
};

static gboolean load_plugin(PurplePlugin *plugin)
{
  purple_signal_register(plugin,"irc-sending-text",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONNECTION),purple_value_new_outgoing(PURPLE_TYPE_STRING));
  purple_signal_register(plugin,"irc-receiving-text",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONNECTION),purple_value_new_outgoing(PURPLE_TYPE_STRING));
  return (!0);
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_PROTOCOL), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("prpl-irc"), ("IRC"), ("2.10.9"), ("IRC Protocol Plugin"), ("The IRC Protocol Plugin that Sucks Less"), ((char *)((void *)0)), ("http://pidgin.im/"), (load_plugin), ((gboolean (*)(PurplePlugin *))((void *)0)), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((&prpl_info)), ((PurplePluginUiInfo *)((void *)0)), (irc_actions), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/**< prefs_info     */
/* padding */
};

static void _init_plugin(PurplePlugin *plugin)
{
  PurpleAccountUserSplit *split;
  PurpleAccountOption *option;
  split = purple_account_user_split_new(((const char *)(dgettext("pidgin","Server"))),"irc.freenode.net",64);
  prpl_info.user_splits = g_list_append(prpl_info.user_splits,split);
  option = purple_account_option_int_new(((const char *)(dgettext("pidgin","Port"))),"port",6667);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","Encodings"))),"encoding","UTF-8");
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_bool_new(((const char *)(dgettext("pidgin","Auto-detect incoming UTF-8"))),"autodetect_utf8",0);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","Ident name"))),"username","");
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","Real name"))),"realname","");
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
/*
	option = purple_account_option_string_new(_("Quit message"), "quitmsg", IRC_DEFAULT_QUIT);
	prpl_info.protocol_options = g_list_append(prpl_info.protocol_options, option);
	*/
  option = purple_account_option_bool_new(((const char *)(dgettext("pidgin","Use SSL"))),"ssl",0);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
#ifdef HAVE_CYRUS_SASL
#endif
  _irc_plugin = plugin;
  purple_prefs_remove("/plugins/prpl/irc/quitmsg");
  purple_prefs_remove("/plugins/prpl/irc");
  irc_register_commands();
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  _init_plugin(plugin);
  return purple_plugin_register(plugin);
}
