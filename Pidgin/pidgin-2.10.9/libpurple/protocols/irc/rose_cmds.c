/**
 * @file cmds.c
 *
 * purple
 *
 * Copyright (C) 2003, Ethan Blanton <eblanton@cs.purdue.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "conversation.h"
#include "debug.h"
#include "notify.h"
#include "util.h"
#include "irc.h"
static void irc_do_mode(struct irc_conn *irc,const char *target,const char *sign,char **ops);

int irc_cmd_default(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  PurpleConversation *convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_ANY,target,(irc -> account));
  char *buf;
  if (!(convo != 0)) 
    return 1;
  buf = g_strdup_printf(((const char *)(dgettext("pidgin","Unknown command: %s"))),cmd);
  if ((purple_conversation_get_type(convo)) == PURPLE_CONV_TYPE_IM) 
    purple_conv_im_write(purple_conversation_get_im_data(convo),"",buf,(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LOG),time(0));
  else 
    purple_conv_chat_write(purple_conversation_get_chat_data(convo),"",buf,(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LOG),time(0));
  g_free(buf);
  return 1;
}

int irc_cmd_away(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  char *buf;
  char *message;
  if ((args[0] != 0) && (strcmp(cmd,"back") != 0)) {
    message = purple_markup_strip_html(args[0]);
    purple_util_chrreplace(message,10,32);
    buf = irc_format(irc,"v:","AWAY",message);
    g_free(message);
  }
  else {
    buf = irc_format(irc,"v","AWAY");
  }
  irc_send(irc,buf);
  g_free(buf);
  return 0;
}

int irc_cmd_ctcp(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
/* we have defined args as args[0] is target and args[1] is ctcp command */
  char *buf;
  GString *string;
/* check if we have args */
  if ((!(args != 0) || !(args[0] != 0)) || !(args[1] != 0)) 
    return 0;
/* TODO:strip newlines or send each line as separate ctcp or something
	 * actually, this shouldn't be done here but somewhere else since irc should support escaping newlines */
  string = g_string_new(args[1]);
  g_string_prepend_c(string,1);
  g_string_append_c_inline(string,1);
  buf = irc_format(irc,"vn:","PRIVMSG",args[0],(string -> str));
  g_string_free(string,(!0));
  irc_send(irc,buf);
  g_free(buf);
  return 1;
}

int irc_cmd_ctcp_action(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  PurpleConnection *gc = purple_account_get_connection((irc -> account));
  char *action;
  char *escaped;
  char *dst;
  char **newargs;
  const char *src;
  PurpleConversation *convo;
  if ((!(args != 0) || !(args[0] != 0)) || !(gc != 0)) 
    return 0;
  action = (g_malloc((strlen(args[0]) + 10)));
  sprintf(action,"\001ACTION ");
  src = args[0];
  dst = (action + 8);
{
    while(( *src) != 0){
      if (( *src) == 10) {
        if (src[1] == 0) {
          break; 
        }
        else {
           *(dst++) = 32;
          src++;
          continue; 
        }
      }
       *(dst++) =  *(src++);
    }
  }
   *(dst++) = 1;
   *dst = 0;
  newargs = ((char **)(g_malloc0_n(2,(sizeof(char *)))));
  newargs[0] = g_strdup(target);
  newargs[1] = action;
  irc_cmd_privmsg(irc,cmd,target,((const char **)newargs));
  g_free(newargs[0]);
  g_free(newargs[1]);
  g_free(newargs);
  convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_ANY,target,(irc -> account));
  if (convo != 0) {
    escaped = g_markup_escape_text(args[0],(-1));
    action = g_strdup_printf("/me %s",escaped);
    g_free(escaped);
    if (action[strlen(action) - 1] == 10) 
      action[strlen(action) - 1] = 0;
    if ((purple_conversation_get_type(convo)) == PURPLE_CONV_TYPE_CHAT) 
      serv_got_chat_in(gc,purple_conv_chat_get_id((purple_conversation_get_chat_data(convo))),purple_connection_get_display_name(gc),PURPLE_MESSAGE_SEND,action,time(0));
    else 
      purple_conv_im_write(purple_conversation_get_im_data(convo),purple_connection_get_display_name(gc),action,PURPLE_MESSAGE_SEND,time(0));
    g_free(action);
  }
  return 1;
}

int irc_cmd_ctcp_version(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  char *buf;
  if (!(args != 0) || !(args[0] != 0)) 
    return 0;
  buf = irc_format(irc,"vn:","PRIVMSG",args[0],"\001VERSION\001");
  irc_send(irc,buf);
  g_free(buf);
  return 0;
}

int irc_cmd_invite(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  char *buf;
  if ((!(args != 0) || !(args[0] != 0)) || !((args[1] != 0) || (target != 0))) 
    return 0;
  buf = irc_format(irc,"vnc","INVITE",args[0],((args[1] != 0)?args[1] : target));
  irc_send(irc,buf);
  g_free(buf);
  return 0;
}

int irc_cmd_join(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  char *buf;
  if (!(args != 0) || !(args[0] != 0)) 
    return 0;
  if (args[1] != 0) 
    buf = irc_format(irc,"vcv","JOIN",args[0],args[1]);
  else 
    buf = irc_format(irc,"vc","JOIN",args[0]);
  irc_send(irc,buf);
  g_free(buf);
  return 0;
}

int irc_cmd_kick(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  char *buf;
  PurpleConversation *convo;
  if (!(args != 0) || !(args[0] != 0)) 
    return 0;
  convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,target,(irc -> account));
  if (!(convo != 0)) 
    return 0;
  if (args[1] != 0) 
    buf = irc_format(irc,"vcn:","KICK",target,args[0],args[1]);
  else 
    buf = irc_format(irc,"vcn","KICK",target,args[0]);
  irc_send(irc,buf);
  g_free(buf);
  return 0;
}

int irc_cmd_list(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  purple_roomlist_show_with_account((irc -> account));
  return 0;
}

int irc_cmd_mode(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  PurpleConnection *gc;
  char *buf;
  if (!(args != 0)) 
    return 0;
  if (!(strcmp(cmd,"mode") != 0)) {
    if (!(args[0] != 0) && (irc_ischannel(target) != 0)) 
      buf = irc_format(irc,"vc","MODE",target);
    else if ((args[0] != 0) && ((( *args[0]) == '+') || (( *args[0]) == '-'))) 
      buf = irc_format(irc,"vcn","MODE",target,args[0]);
    else if (args[0] != 0) 
      buf = irc_format(irc,"vn","MODE",args[0]);
    else 
      return 0;
  }
  else if (!(strcmp(cmd,"umode") != 0)) {
    if (!(args[0] != 0)) 
      return 0;
    gc = purple_account_get_connection((irc -> account));
    buf = irc_format(irc,"vnc","MODE",purple_connection_get_display_name(gc),args[0]);
  }
  else {
    return 0;
  }
  irc_send(irc,buf);
  g_free(buf);
  return 0;
}

int irc_cmd_names(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  char *buf;
  if (!(args != 0) || (!(args[0] != 0) && !(irc_ischannel(target) != 0))) 
    return 0;
  buf = irc_format(irc,"vc","NAMES",((args[0] != 0)?args[0] : target));
  irc_send(irc,buf);
  g_free(buf);
  return 0;
}

int irc_cmd_nick(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  char *buf;
  if (!(args != 0) || !(args[0] != 0)) 
    return 0;
  buf = irc_format(irc,"v:","NICK",args[0]);
  g_free((irc -> reqnick));
  irc -> reqnick = g_strdup(args[0]);
  irc -> nickused = 0;
  irc_send(irc,buf);
  g_free(buf);
  return 0;
}

int irc_cmd_op(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  char **nicks;
  char **ops;
  char *sign;
  char *mode;
  int i = 0;
  int used = 0;
  if ((!(args != 0) || !(args[0] != 0)) || !(( *args[0]) != 0)) 
    return 0;
  if (!(strcmp(cmd,"op") != 0)) {
    sign = "+";
    mode = "o";
  }
  else if (!(strcmp(cmd,"deop") != 0)) {
    sign = "-";
    mode = "o";
  }
  else if (!(strcmp(cmd,"voice") != 0)) {
    sign = "+";
    mode = "v";
  }
  else if (!(strcmp(cmd,"devoice") != 0)) {
    sign = "-";
    mode = "v";
  }
  else {
    purple_debug(PURPLE_DEBUG_ERROR,"irc","invalid \'op\' command \'%s\'\n",cmd);
    return 0;
  }
  nicks = g_strsplit(args[0]," ",(-1));
  for (i = 0; nicks[i] != 0; i++) 
/* nothing */
;
  ops = ((char **)(g_malloc0_n(((i * 2) + 1),(sizeof(char *)))));
  for (i = 0; nicks[i] != 0; i++) {
    if (( *nicks[i]) != 0) {
      ops[used++] = mode;
      ops[used++] = nicks[i];
    }
  }
  irc_do_mode(irc,target,sign,ops);
  g_free(ops);
  g_strfreev(nicks);
  return 0;
}

int irc_cmd_part(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  char *buf;
  if (!(args != 0)) 
    return 0;
  if (args[1] != 0) 
    buf = irc_format(irc,"vc:","PART",((args[0] != 0)?args[0] : target),args[1]);
  else 
    buf = irc_format(irc,"vc","PART",((args[0] != 0)?args[0] : target));
  irc_send(irc,buf);
  g_free(buf);
  return 0;
}

int irc_cmd_ping(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  char *stamp;
  char *buf;
  if ((args != 0) && (args[0] != 0)) {
    if (irc_ischannel(args[0]) != 0) 
      return 0;
    stamp = g_strdup_printf("\001PING %lu\001",time(0));
    buf = irc_format(irc,"vn:","PRIVMSG",args[0],stamp);
    g_free(stamp);
  }
  else if (target != 0) {
    stamp = g_strdup_printf("%s %lu",target,time(0));
    buf = irc_format(irc,"v:","PING",stamp);
    g_free(stamp);
  }
  else {
    stamp = g_strdup_printf("%lu",time(0));
    buf = irc_format(irc,"vv","PING",stamp);
    g_free(stamp);
  }
  irc_send(irc,buf);
  g_free(buf);
  return 0;
}

int irc_cmd_privmsg(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  const char *cur;
  const char *end;
  char *msg;
  char *buf;
  if ((!(args != 0) || !(args[0] != 0)) || !(args[1] != 0)) 
    return 0;
  cur = args[1];
  end = args[1];
  while((( *end) != 0) && (( *cur) != 0)){
    end = (strchr(cur,10));
    if (!(end != 0)) 
      end = (cur + strlen(cur));
    msg = g_strndup(cur,(end - cur));
    if (!(strcmp(cmd,"notice") != 0)) 
      buf = irc_format(irc,"vt:","NOTICE",args[0],msg);
    else 
      buf = irc_format(irc,"vt:","PRIVMSG",args[0],msg);
    irc_send(irc,buf);
    g_free(msg);
    g_free(buf);
    cur = (end + 1);
  }
  return 0;
}

int irc_cmd_quit(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  char *buf;
  if (!((irc -> quitting) != 0)) {
/*
		 * Use purple_account_get_string(irc->account, "quitmsg", IRC_DEFAULT_QUIT)
		 * and uncomment the appropriate account preference in irc.c if we
		 * decide we want custom quit messages.
		 */
    buf = irc_format(irc,"v:","QUIT",(((args != 0) && (args[0] != 0))?args[0] : "Leaving."));
    irc_send(irc,buf);
    g_free(buf);
    irc -> quitting = (!0);
    if (!(( *(irc -> account)).disconnecting != 0)) 
      purple_account_set_status((irc -> account),"offline",(!0),((void *)((void *)0)));
  }
  return 0;
}

int irc_cmd_quote(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  char *buf;
  if (!(args != 0) || !(args[0] != 0)) 
    return 0;
  buf = irc_format(irc,"n",args[0]);
  irc_send(irc,buf);
  g_free(buf);
  return 0;
}

int irc_cmd_query(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  PurpleConversation *convo;
  PurpleConnection *gc;
  if (!(args != 0) || !(args[0] != 0)) 
    return 0;
  convo = purple_conversation_new(PURPLE_CONV_TYPE_IM,(irc -> account),args[0]);
  purple_conversation_present(convo);
  if (args[1] != 0) {
    gc = purple_account_get_connection((irc -> account));
    irc_cmd_privmsg(irc,cmd,target,args);
    purple_conv_im_write(purple_conversation_get_im_data(convo),purple_connection_get_display_name(gc),args[1],PURPLE_MESSAGE_SEND,time(0));
  }
  return 0;
}

int irc_cmd_remove(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  char *buf;
  if (!(args != 0) || !(args[0] != 0)) 
    return 0;
/* not a channel, punt */
  if (!(irc_ischannel(target) != 0)) 
    return 0;
  if (args[1] != 0) 
    buf = irc_format(irc,"vcn:","REMOVE",target,args[0],args[1]);
  else 
    buf = irc_format(irc,"vcn","REMOVE",target,args[0]);
  irc_send(irc,buf);
  g_free(buf);
  return 0;
}

int irc_cmd_service(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  char *capital_cmd;
  char *buf;
  if (!(args != 0) || !(args[0] != 0)) 
    return 0;
/* cmd will be one of nickserv, chanserv, memoserv or operserv */
  capital_cmd = g_ascii_strup(cmd,(-1));
  buf = irc_format(irc,"v:",capital_cmd,args[0]);
  irc_send(irc,buf);
  g_free(capital_cmd);
  g_free(buf);
  return 0;
}

int irc_cmd_time(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  char *buf;
  buf = irc_format(irc,"v","TIME");
  irc_send(irc,buf);
  g_free(buf);
  return 0;
}

int irc_cmd_topic(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  char *buf;
  const char *topic;
  PurpleConversation *convo;
  if (!(args != 0)) 
    return 0;
  convo = purple_find_conversation_with_account(PURPLE_CONV_TYPE_CHAT,target,(irc -> account));
  if (!(convo != 0)) 
    return 0;
  if (!(args[0] != 0)) {
    topic = purple_conv_chat_get_topic((purple_conversation_get_chat_data(convo)));
    if (topic != 0) {
      char *tmp;
      char *tmp2;
      tmp = g_markup_escape_text(topic,(-1));
      tmp2 = purple_markup_linkify(tmp);
      buf = g_strdup_printf(((const char *)(dgettext("pidgin","current topic is: %s"))),tmp2);
      g_free(tmp);
      g_free(tmp2);
    }
    else 
      buf = g_strdup(((const char *)(dgettext("pidgin","No topic is set"))));
    purple_conv_chat_write(purple_conversation_get_chat_data(convo),target,buf,(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LOG),time(0));
    g_free(buf);
    return 0;
  }
  buf = irc_format(irc,"vt:","TOPIC",target,args[0]);
  irc_send(irc,buf);
  g_free(buf);
  return 0;
}

int irc_cmd_wallops(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  char *buf;
  if (!(args != 0) || !(args[0] != 0)) 
    return 0;
  if (!(strcmp(cmd,"wallops") != 0)) 
    buf = irc_format(irc,"v:","WALLOPS",args[0]);
  else if (!(strcmp(cmd,"operwall") != 0)) 
    buf = irc_format(irc,"v:","OPERWALL",args[0]);
  else 
    return 0;
  irc_send(irc,buf);
  g_free(buf);
  return 0;
}

int irc_cmd_whois(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  char *buf;
  if (!(args != 0) || !(args[0] != 0)) 
    return 0;
  if (args[1] != 0) {
    buf = irc_format(irc,"vvn","WHOIS",args[0],args[1]);
    irc -> whois.nick = g_strdup(args[1]);
  }
  else {
    buf = irc_format(irc,"vn","WHOIS",args[0]);
    irc -> whois.nick = g_strdup(args[0]);
  }
  irc_send(irc,buf);
  g_free(buf);
  return 0;
}

int irc_cmd_whowas(struct irc_conn *irc,const char *cmd,const char *target,const char **args)
{
  char *buf;
  if (!(args != 0) || !(args[0] != 0)) 
    return 0;
  buf = irc_format(irc,"vn","WHOWAS",args[0]);
  irc -> whois.nick = g_strdup(args[0]);
  irc_send(irc,buf);
  g_free(buf);
  return 0;
}

static void irc_do_mode(struct irc_conn *irc,const char *target,const char *sign,char **ops)
{
  char *buf;
  char mode[5UL];
  int i = 0;
  if (!(sign != 0)) 
    return ;
  while(ops[i] != 0){
    if ((ops[i + 2] != 0) && (ops[i + 4] != 0)) {
      g_snprintf(mode,(sizeof(mode)),"%s%s%s%s",sign,ops[i],ops[i + 2],ops[i + 4]);
      buf = irc_format(irc,"vcvnnn","MODE",target,mode,ops[i + 1],ops[i + 3],ops[i + 5]);
      i += 6;
    }
    else if (ops[i + 2] != 0) {
      g_snprintf(mode,(sizeof(mode)),"%s%s%s",sign,ops[i],ops[i + 2]);
      buf = irc_format(irc,"vcvnn","MODE",target,mode,ops[i + 1],ops[i + 3]);
      i += 4;
    }
    else {
      g_snprintf(mode,(sizeof(mode)),"%s%s",sign,ops[i]);
      buf = irc_format(irc,"vcvn","MODE",target,mode,ops[i + 1]);
      i += 2;
    }
    irc_send(irc,buf);
    g_free(buf);
  }
}
