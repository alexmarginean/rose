/*
 *					MXit Protocol libPurple Plugin
 *
 *					-- user profile's --
 *
 *				Andrew Victor	<libpurple@mxit.com>
 *
 *			(C) Copyright 2009	MXit Lifestyle (Pty) Ltd.
 *				<http://www.mxitlifestyle.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#define		_XOPEN_SOURCE
#include	<time.h>
#include	"internal.h"
#include	"mxit.h"
#include	"profile.h"
#include	"roster.h"
/*------------------------------------------------------------------------
 * Return the MXit Relationship status as a string.
 *
 * @param id		The Relationship status value (see profile.h)
 * @return			The relationship status as a text string.
 */

const char *mxit_relationship_to_name(short id)
{
  switch(id){
    case 0:
{
      return (const char *)(dgettext("pidgin","Unknown"));
    }
    case 1:
{
      return (const char *)(dgettext("pidgin","Don\'t want to say"));
    }
    case 2:
{
      return (const char *)(dgettext("pidgin","Single"));
    }
    case 3:
{
      return (const char *)(dgettext("pidgin","In a relationship"));
    }
    case 4:
{
      return (const char *)(dgettext("pidgin","Engaged"));
    }
    case 5:
{
      return (const char *)(dgettext("pidgin","Married"));
    }
    case 6:
{
      return (const char *)(dgettext("pidgin","It\'s complicated"));
    }
    case 7:
{
      return (const char *)(dgettext("pidgin","Widowed"));
    }
    case 8:
{
      return (const char *)(dgettext("pidgin","Separated"));
    }
    case 9:
{
      return (const char *)(dgettext("pidgin","Divorced"));
    }
    default:
{
      return "";
    }
  }
}
/*------------------------------------------------------------------------
 * Returns true if it is a valid date.
 *
 * @param bday		Date-of-Birth string (YYYY-MM-DD)
 * @return			TRUE if valid, else FALSE
 */

gboolean validateDate(const char *bday)
{
  struct tm *tm;
  time_t t;
  int cur_year;
  int max_days[13UL] = {(0), (31), (29), (31), (30), (31), (30), (31), (31), (30), (31), (30), (31)};
  char date[16UL];
  int year;
  int month;
  int day;
/* validate length */
  if (strlen(bday) != 10) {
    return 0;
  }
/* validate the format */
/* year */
  if (((((((((!((( *__ctype_b_loc())[(int )bday[0]] & ((unsigned short )_ISdigit)) != 0) || !((( *__ctype_b_loc())[(int )bday[1]] & ((unsigned short )_ISdigit)) != 0)) || !((( *__ctype_b_loc())[(int )bday[2]] & ((unsigned short )_ISdigit)) != 0)) || !((( *__ctype_b_loc())[(int )bday[3]] & ((unsigned short )_ISdigit)) != 0)) || (bday[4] != '-')) || !((( *__ctype_b_loc())[(int )bday[5]] & ((unsigned short )_ISdigit)) != 0)) || !((( *__ctype_b_loc())[(int )bday[6]] & ((unsigned short )_ISdigit)) != 0)) || (bday[7] != '-')) || !((( *__ctype_b_loc())[(int )bday[8]] & ((unsigned short )_ISdigit)) != 0)) || !((( *__ctype_b_loc())[(int )bday[9]] & ((unsigned short )_ISdigit)) != 0)) 
/* month */
/* day */
{
    return 0;
  }
/* convert */
  t = time(0);
  tm = gmtime((&t));
  cur_year = ((tm -> tm_year) + 1900);
  memcpy(date,bday,10);
  date[4] = 0;
  date[7] = 0;
  date[10] = 0;
  year = atoi((date + 0));
  month = atoi((date + 5));
  day = atoi((date + 8));
/* validate month */
  if ((month < 1) || (month > 12)) {
    return 0;
  }
/* validate day */
  if ((day < 1) || (day > max_days[month])) {
    return 0;
  }
/* validate year */
  if ((year < (cur_year - 100)) || (year >= cur_year)) {
/* you are either tooo old or tooo young to join mxit... sorry */
    return 0;
  }
/* special case leap-year */
  if ((((year % 4) != 0) && (month == 2)) && (day == 29)) {
/* cannot have 29 days in February in non leap-years! */
    return 0;
  }
  return (!0);
}
/*------------------------------------------------------------------------
 * Calculate an Age from the date-of-birth.
 *
 * @param date		Date-of-Birth string (YYYY-MM-DD)
 * @return			The age
 */

static int calculateAge(const char *date)
{
  time_t t;
  struct tm now;
  struct tm bdate;
  int age;
  if (!(date != 0) || (strlen(date) == 0)) 
    return 0;
/* current time */
  t = time(0);
  localtime_r((&t),&now);
/* decode hdate */
  memset((&bdate),0,(sizeof(struct tm )));
  purple_str_to_time(date,0,&bdate,0,0);
/* calculate difference */
  age = (now.tm_year - bdate.tm_year);
/* is before month of birth */
  if (now.tm_mon < bdate.tm_mon) 
    age--;
  else 
/* before birthday in current month */
if ((now.tm_mon == bdate.tm_mon) && (now.tm_mday < bdate.tm_mday)) 
    age--;
  return age;
}
/*------------------------------------------------------------------------
 * Returns timestamp field in date & time format (DD-MM-YYYY HH:MM:SS)
 *
 * @param msecs		The timestamps (milliseconds since epoch)
 * @return			Date & Time in a display'able format.
 */

static const char *datetime(gint64 msecs)
{
  time_t secs = (msecs / 1000);
  struct tm t;
  localtime_r((&secs),&t);
  return purple_utf8_strftime("%d-%m-%Y %H:%M:%S",(&t));
}
/*------------------------------------------------------------------------
 * Display the profile information.
 *
 * @param session		The MXit session object
 * @param username		The username who's profile information this is
 * @param profile		The profile
 */

void mxit_show_profile(struct MXitSession *session,const char *username,struct MXitProfile *profile)
{
  PurpleNotifyUserInfo *info = purple_notify_user_info_new();
  struct contact *contact = (struct contact *)((void *)0);
  PurpleBuddy *buddy;
  gchar *tmp = (gchar *)((void *)0);
  buddy = purple_find_buddy((session -> acc),username);
  if (buddy != 0) {
    purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Alias"))),purple_buddy_get_alias(buddy));
    purple_notify_user_info_add_section_break(info);
    contact = (purple_buddy_get_protocol_data(buddy));
  }
  purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Display Name"))),(profile -> nickname));
  tmp = g_strdup_printf("%s (%i)",(profile -> birthday),calculateAge((profile -> birthday)));
  purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Birthday"))),tmp);
  g_free(tmp);
  purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Gender"))),(((profile -> male) != 0)?((const char *)(dgettext("pidgin","Male"))) : ((const char *)(dgettext("pidgin","Female")))));
/* optional information */
  purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","First Name"))),(profile -> firstname));
  purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Last Name"))),(profile -> lastname));
  purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Country"))),(profile -> regcountry));
  if (strlen((profile -> aboutme)) > 0) 
    purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","About Me"))),(profile -> aboutme));
  if (strlen((profile -> whereami)) > 0) 
    purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Where I Live"))),(profile -> whereami));
  purple_notify_user_info_add_pair_plaintext(info,((const char *)(dgettext("pidgin","Relationship Status"))),mxit_relationship_to_name((profile -> relationship)));
  purple_notify_user_info_add_section_break(info);
  if (contact != 0) {
/* presence */
    purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Status"))),mxit_convert_presence_to_name((contact -> presence)));
/* last online */
    if ((contact -> presence) == 0) 
      purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Last Online"))),(((profile -> lastonline) == 0)?((const char *)(dgettext("pidgin","Unknown"))) : datetime((profile -> lastonline))));
/* mood */
    if ((contact -> mood) != 0) 
      purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Mood"))),mxit_convert_mood_to_name((contact -> mood)));
    else 
      purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Mood"))),((const char *)(dgettext("pidgin","None"))));
/* status message */
    if ((contact -> statusMsg) != 0) 
      purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Status Message"))),(contact -> statusMsg));
/* subscription type */
    purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Subscription"))),mxit_convert_subtype_to_name((contact -> subtype)));
  }
  else {
/* this is an invite */
    contact = get_mxit_invite_contact(session,username);
    if (contact != 0) {
/* invite found */
      if ((contact -> msg) != 0) 
        purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Invite Message"))),(contact -> msg));
      if ((contact -> imgid) != 0) {
/* this invite has a avatar */
        char *img_text;
        img_text = g_strdup_printf("<img id=\'%d\'>",(contact -> imgid));
        purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Photo"))),img_text);
        g_free(img_text);
      }
      if ((contact -> statusMsg) != 0) 
        purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Status Message"))),(contact -> statusMsg));
    }
  }
  purple_notify_userinfo((session -> con),username,info,0,0);
  purple_notify_user_info_destroy(info);
}
/*------------------------------------------------------------------------
 * Display the profiles of search results.
 *
 * @param gc			The connection object
 * @param row			The selected row from search-results
 * @param user_data		NULL (unused)
 */

static void mxit_search_results_add_cb(PurpleConnection *gc,GList *row,gpointer user_data)
{
/* display add buddy dialog */
  purple_blist_request_add_buddy(purple_connection_get_account(gc),(g_list_nth_data(row,0)),0,(g_list_nth_data(row,1)));
}
/*------------------------------------------------------------------------
 * Display the profiles of search results.
 *
 * @param session		The MXit session object
 * @param searchType	The type of search (CP_SUGGEST_*)
 * @param maxResults	The maximum number of results
 * @param entries		The list of profile entries
 */

void mxit_show_search_results(struct MXitSession *session,int searchType,int maxResults,GList *entries)
{
  PurpleNotifySearchResults *results;
  PurpleNotifySearchColumn *column;
  gchar *text;
  if (!(entries != 0)) {
    mxit_popup(PURPLE_NOTIFY_MSG_INFO,((const char *)(dgettext("pidgin","No results"))),((const char *)(dgettext("pidgin","No contacts found."))));
    return ;
  }
  results = purple_notify_searchresults_new();
  if (!(results != 0)) 
    return ;
/* define columns */
  column = purple_notify_searchresults_column_new(((const char *)(dgettext("pidgin","UserId"))));
  purple_notify_searchresults_column_add(results,column);
  column = purple_notify_searchresults_column_new(((const char *)(dgettext("pidgin","Display Name"))));
  purple_notify_searchresults_column_add(results,column);
  column = purple_notify_searchresults_column_new(((const char *)(dgettext("pidgin","First Name"))));
  purple_notify_searchresults_column_add(results,column);
  column = purple_notify_searchresults_column_new(((const char *)(dgettext("pidgin","Last Name"))));
  purple_notify_searchresults_column_add(results,column);
  column = purple_notify_searchresults_column_new(((const char *)(dgettext("pidgin","Gender"))));
  purple_notify_searchresults_column_add(results,column);
  column = purple_notify_searchresults_column_new(((const char *)(dgettext("pidgin","Age"))));
  purple_notify_searchresults_column_add(results,column);
  column = purple_notify_searchresults_column_new(((const char *)(dgettext("pidgin","Where I live"))));
  purple_notify_searchresults_column_add(results,column);
  while(entries != ((GList *)((void *)0))){
    struct MXitProfile *profile = (struct MXitProfile *)(entries -> data);
    GList *row;
    gchar *tmp = purple_base64_encode(((unsigned char *)(profile -> userid)),strlen((profile -> userid)));
/* column values */
    row = g_list_append(0,(g_strdup_printf("#%s",tmp)));
    row = g_list_append(row,(g_strdup((profile -> nickname))));
    row = g_list_append(row,(g_strdup((profile -> firstname))));
    row = g_list_append(row,(g_strdup((profile -> lastname))));
    row = g_list_append(row,(g_strdup(((((profile -> male) != 0)?"Male" : "Female")))));
    row = g_list_append(row,(g_strdup_printf("%i",calculateAge((profile -> birthday)))));
    row = g_list_append(row,(g_strdup((profile -> whereami))));
    purple_notify_searchresults_row_add(results,row);
    entries = ((entries != 0)?( *((GList *)entries)).next : ((struct _GList *)((void *)0)));
    g_free(tmp);
  }
/* button */
  purple_notify_searchresults_button_add(results,PURPLE_NOTIFY_BUTTON_INVITE,mxit_search_results_add_cb);
  if (searchType == 1) 
    text = g_strdup_printf((dngettext("pidgin","You have %i suggested friend.","You have %i suggested friends.",maxResults)),maxResults);
  else 
    text = g_strdup_printf((dngettext("pidgin","We found %i contact that matches your search.","We found %i contacts that match your search.",maxResults)),maxResults);
  purple_notify_searchresults((session -> con),0,text,0,results,0,0);
  g_free(text);
}
