/*
 *					MXit Protocol libPurple Plugin
 *
 *					-- MXit Forms & Commands --
 *
 *				Andrew Victor	<libpurple@mxit.com>
 *
 *			(C) Copyright 2009	MXit Lifestyle (Pty) Ltd.
 *				<http://www.mxitlifestyle.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "debug.h"
#include "protocol.h"
#include "mxit.h"
#include "markup.h"
#include "formcmds.h"
#undef MXIT_DEBUG_COMMANDS
/*
 * the MXit Command identifiers
 */
typedef enum __unnamed_enum___F0_L40_C9_MXIT_CMD_UNKNOWN__COMMA__MXIT_CMD_CLEAR__COMMA__MXIT_CMD_SENDSMS__COMMA__MXIT_CMD_REPLY__COMMA__MXIT_CMD_PLATREQ__COMMA__MXIT_CMD_SELECTCONTACT__COMMA__MXIT_CMD_IMAGE__COMMA__MXIT_CMD_SCREENCONFIG__COMMA__MXIT_CMD_SCREENINFO__COMMA__MXIT_CMD_IMAGESTRIP__COMMA__MXIT_CMD_TABLE {
/* Unknown command */
MXIT_CMD_UNKNOWN,
/* Clear (clear) */
MXIT_CMD_CLEAR,
/* Send SMS (sendsms) */
MXIT_CMD_SENDSMS,
/* Reply (reply) */
MXIT_CMD_REPLY,
/* Platform Request (platreq) */
MXIT_CMD_PLATREQ,
/* Select Contact (selc) */
MXIT_CMD_SELECTCONTACT,
/* Inline image (img) */
MXIT_CMD_IMAGE,
/* Chat-screen config (csc) */
MXIT_CMD_SCREENCONFIG,
/* Chat-screen info (csi) */
MXIT_CMD_SCREENINFO,
/* Image Strip (is) */
MXIT_CMD_IMAGESTRIP,
/* Table (tbl) */
MXIT_CMD_TABLE}MXitCommandType;
/* Chat-screen behaviours (bhvr) */
#define SCREEN_NO_HEADINGS		0x01
#define SCREEN_FULLSCREEN		0x02
#define SCREEN_AUTOCLEAR		0x04
#define SCREEN_NO_AUDIO			0x08
#define SCREEN_NO_MSGPREFIX		0x10
#define SCREEN_NOTIFY			0x20
#define SCREEN_PROGRESSBAR		0x40
/*
 * object for an inline image request with an URL
 */

struct ii_url_request 
{
  struct RXMsgData *mx;
  char *url;
}
;
/*------------------------------------------------------------------------
 * Callback function invoked when an inline image request to a web site completes.
 *
 *  @param url_data
 *  @param user_data		The Markup message object
 *  @param url_text			The data returned from the WAP site
 *  @param len				The length of the data returned
 *  @param error_message	Descriptive error message
 */

static void mxit_cb_ii_returned(PurpleUtilFetchUrlData *url_data,gpointer user_data,const gchar *url_text,gsize len,const gchar *error_message)
{
  struct ii_url_request *iireq = (struct ii_url_request *)user_data;
  int *intptr = (int *)((void *)0);
  int id;
#ifdef	MXIT_DEBUG_COMMANDS
#endif
  if (!(url_text != 0)) {
/* no reply from the WAP site */
    purple_debug_error("prpl-loubserp-mxit","Error downloading Inline Image from %s.\n",(iireq -> url));
    goto done;
  }
/* lets first see if we don't have the inline image already in cache */
  if (g_hash_table_lookup(( *( *(iireq -> mx)).session).iimages,(iireq -> url)) != 0) {
/* inline image found in the cache, so we just ignore this reply */
    goto done;
  }
/* we now have the inline image, store a copy in the imagestore */
  id = purple_imgstore_add_with_id(g_memdup(url_text,len),len,0);
/* map the inline image id to purple image id */
  intptr = (g_malloc((sizeof(int ))));
   *intptr = id;
  g_hash_table_insert(( *( *(iireq -> mx)).session).iimages,(iireq -> url),intptr);
  ( *(iireq -> mx)).flags |= PURPLE_MESSAGE_IMAGES;
  done:
  ( *(iireq -> mx)).img_count--;
  if ((( *(iireq -> mx)).img_count == 0) && (( *(iireq -> mx)).converted != 0)) {
/*
		 * this was the last outstanding emoticon for this message,
		 * so we can now display it to the user.
		 */
    mxit_show_message((iireq -> mx));
  }
  g_free(iireq);
}
/*------------------------------------------------------------------------
 * Return the command identifier of this MXit Command.
 *
 *  @param cmd			The MXit command <key,value> map
 *  @return				The MXit command identifier
 */

static MXitCommandType command_type(GHashTable *hash)
{
  char *op;
  char *type;
  op = (g_hash_table_lookup(hash,"op"));
  if (op != 0) {
    if (strcmp(op,"cmd") == 0) {
      type = (g_hash_table_lookup(hash,"type"));
/* no command provided */
      if (type == ((char *)((void *)0))) 
        return MXIT_CMD_UNKNOWN;
      else 
/* clear */
if (strcmp(type,"clear") == 0) 
        return MXIT_CMD_CLEAR;
      else 
/* send an SMS */
if (strcmp(type,"sendsms") == 0) 
        return MXIT_CMD_SENDSMS;
      else 
/* list of options */
if (strcmp(type,"reply") == 0) 
        return MXIT_CMD_REPLY;
      else 
/* platform request */
if (strcmp(type,"platreq") == 0) 
        return MXIT_CMD_PLATREQ;
      else 
/* select contact */
if (strcmp(type,"selc") == 0) 
        return MXIT_CMD_SELECTCONTACT;
    }
    else 
/* inline image */
if (strcmp(op,"img") == 0) 
      return MXIT_CMD_IMAGE;
    else 
/* chat-screen config */
if (strcmp(op,"csc") == 0) 
      return MXIT_CMD_SCREENCONFIG;
    else 
/* chat-screen info */
if (strcmp(op,"csi") == 0) 
      return MXIT_CMD_SCREENINFO;
    else 
/* image-strip */
if (strcmp(op,"is") == 0) 
      return MXIT_CMD_IMAGESTRIP;
    else 
/* table */
if (strcmp(op,"tbl") == 0) 
      return MXIT_CMD_TABLE;
  }
  return MXIT_CMD_UNKNOWN;
}
/*------------------------------------------------------------------------
 * Tokenize a MXit Command string into a <key,value> map.
 *
 *  @param cmd			The MXit command string
 *  @return				The <key,value> hash-map, or NULL on error.
 */

static GHashTable *command_tokenize(char *cmd)
{
  GHashTable *hash = (GHashTable *)((void *)0);
  gchar **parts;
  int i = 0;
#ifdef MXIT_DEBUG_COMMANDS
#endif
/* explode the command into parts */
  parts = g_strsplit(cmd,"|",0);
  hash = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
/* now break part into a key & value */
  while(parts[i] != ((gchar *)((void *)0))){
    char *value;
/* find start of value */
    value = strchr(parts[i],'=');
    if (value != ((char *)((void *)0))) {
       *value = 0;
      value++;
    }
#ifdef MXIT_DEBUG_COMMANDS
#endif
    g_hash_table_insert(hash,(g_strdup(parts[i])),(g_strdup(value)));
    i++;
  }
  g_strfreev(parts);
  return hash;
}
/*------------------------------------------------------------------------
 * Process a Clear MXit command.
 *  [::op=cmd|type=clear|clearmsgscreen=true|auto=true|id=12345:]
 *
 *  @param session		The MXit session object
 *  @param from			The sender of the message.
 *  @param hash			The MXit command <key,value> map
 */

static void command_clear(struct MXitSession *session,const char *from,GHashTable *hash)
{
  PurpleConversation *conv;
  char *clearmsgscreen;
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,from,(session -> acc));
  if (conv == ((PurpleConversation *)((void *)0))) {
    purple_debug_error("prpl-loubserp-mxit",((const char *)(dgettext("pidgin","Conversation with \'%s\' not found\n"))),from);
    return ;
  }
  clearmsgscreen = (g_hash_table_lookup(hash,"clearmsgscreen"));
  if ((clearmsgscreen != 0) && (strcmp(clearmsgscreen,"true") == 0)) {
/* this is a command to clear the chat screen */
    purple_conversation_clear_message_history(conv);
  }
}
/*------------------------------------------------------------------------
 * Process a Reply MXit command.
 *  [::op=cmd|type=reply|replymsg=back|selmsg=b) Back|displaymsg=Processing|id=12345:]
 *  [::op=cmd|nm=rep|type=reply|replymsg=back|selmsg=b) Back|displaymsg=Processing|id=12345:]
 *
 *  @param mx			The received message data object
 *  @param hash			The MXit command <key,value> map
 */

static void command_reply(struct RXMsgData *mx,GHashTable *hash)
{
  char *replymsg;
  char *selmsg;
  char *nm;
/* selection message */
  selmsg = (g_hash_table_lookup(hash,"selmsg"));
/* reply message */
  replymsg = (g_hash_table_lookup(hash,"replymsg"));
/* name parameter */
  nm = (g_hash_table_lookup(hash,"nm"));
  if ((selmsg == ((char *)((void *)0))) || (replymsg == ((char *)((void *)0)))) 
/* these parameters are required */
    return ;
/* indicates response must be a structured response */
  if (nm != 0) {
    gchar *seltext = g_markup_escape_text(purple_url_decode(selmsg),(-1));
    gchar *replycmd = g_strdup_printf("type=reply|nm=%s|res=%s|err=0",nm,purple_url_decode(replymsg));
    mxit_add_html_link(mx,replycmd,(!0),seltext);
    g_free(seltext);
    g_free(replycmd);
  }
  else {
    gchar *seltext = g_markup_escape_text(purple_url_decode(selmsg),(-1));
    mxit_add_html_link(mx,purple_url_decode(replymsg),0,seltext);
    g_free(seltext);
  }
}
/*------------------------------------------------------------------------
 * Process a PlatformRequest MXit command.
 *  [::op=cmd|type=platreq|selmsg=Upgrade MXit|dest=http%3a//m.mxit.com|id=12345:]
 *
 *  @param hash			The MXit command <key,value> map
 *  @param msg			The message to display (as generated so far)
 */

static void command_platformreq(GHashTable *hash,GString *msg)
{
  gchar *text = (gchar *)((void *)0);
  char *selmsg;
  char *dest;
/* find the selection message */
  selmsg = (g_hash_table_lookup(hash,"selmsg"));
  if ((selmsg != 0) && (strlen(selmsg) > 0)) {
    text = g_markup_escape_text(purple_url_decode(selmsg),(-1));
  }
/* find the destination */
  dest = (g_hash_table_lookup(hash,"dest"));
  if (dest != 0) {
/* add link to display message */
    g_string_append_printf(msg,"<a href=\"%s\">%s</a>",purple_url_decode(dest),((text != 0)?text : ((const char *)(dgettext("pidgin","Download")))));
  }
  if (text != 0) 
    g_free(text);
}
/*------------------------------------------------------------------------
 * Process an inline image MXit command.
 *  [::op=img|dat=ASDF23408asdflkj2309flkjsadf%3d%3d|algn=1|w=120|h=12|t=100|replymsg=text:]
 *
 *  @param mx			The received message data object
 *  @param hash			The MXit command <key,value> map
 *  @param msg			The message to display (as generated so far)
 */

static void command_image(struct RXMsgData *mx,GHashTable *hash,GString *msg)
{
  const char *img;
  const char *reply;
  guchar *rawimg;
  gsize rawimglen;
  int imgid;
  img = (g_hash_table_lookup(hash,"dat"));
  if (img != 0) {
    rawimg = purple_base64_decode(img,&rawimglen);
//purple_util_write_data_to_file_absolute("/tmp/mxitinline.png", (char*) rawimg, rawimglen);
    imgid = purple_imgstore_add_with_id(rawimg,rawimglen,0);
    g_string_append_printf(msg,"<img id=\"%i\">",imgid);
    mx -> flags |= PURPLE_MESSAGE_IMAGES;
  }
  else {
    img = (g_hash_table_lookup(hash,"src"));
    if (img != 0) {
      struct ii_url_request *iireq;
      iireq = ((struct ii_url_request *)(g_malloc0_n(1,(sizeof(struct ii_url_request )))));
      iireq -> url = g_strdup(purple_url_decode(img));
      iireq -> mx = mx;
      g_string_append_printf(msg,"%s%s>","<MXII=",(iireq -> url));
      mx -> got_img = (!0);
/* lets first see if we don't have the inline image already in cache */
      if (g_hash_table_lookup(( *(mx -> session)).iimages,(iireq -> url)) != 0) {
/* inline image found in the cache, so we do not have to request it from the web */
        g_free(iireq);
      }
      else {
/* send the request for the inline image */
        purple_debug_info("prpl-loubserp-mxit","sending request for inline image \'%s\'\n",(iireq -> url));
/* request the image (reference: "libpurple/util.h") */
        purple_util_fetch_url_request((iireq -> url),(!0),0,(!0),0,0,mxit_cb_ii_returned,iireq);
        mx -> img_count++;
      }
    }
  }
/* if this is a clickable image, show a click link */
  reply = (g_hash_table_lookup(hash,"replymsg"));
  if (reply != 0) {
    g_string_append_printf(msg,"\n");
    mxit_add_html_link(mx,purple_url_decode(reply),0,((const char *)(dgettext("pidgin","click here"))));
  }
}
/*------------------------------------------------------------------------
 * Process an Imagestrip MXit command.
 *  [::op=is|nm=status|dat=iVBORw0KGgoAAAA%3d%3d|v=63398792426788|fw=8|fh=8|layer=0:]
 *
 *  @param from			The sender of the message.
 *  @param hash			The MXit command <key,value> map
 */

static void command_imagestrip(struct MXitSession *session,const char *from,GHashTable *hash)
{
  const char *name;
  const char *validator;
  const char *tmp;
  int width;
  int height;
  int layer;
  purple_debug_info("prpl-loubserp-mxit","ImageStrip received from %s\n",from);
/* image strip name */
  name = (g_hash_table_lookup(hash,"nm"));
/* validator */
  validator = (g_hash_table_lookup(hash,"v"));
/* image data */
  tmp = (g_hash_table_lookup(hash,"dat"));
  if (tmp != 0) {
    guchar *rawimg;
    gsize rawimglen;
    char *dir;
    char *escfrom;
    char *escname;
    char *escvalidator;
    char *filename;
/* base64 decode the image data */
    rawimg = purple_base64_decode(tmp,&rawimglen);
    if (!(rawimg != 0)) 
      return ;
/* save it to a file */
    dir = g_build_filename(purple_user_dir(),"mxit","imagestrips",((void *)((void *)0)));
/* ensure directory exists */
    purple_build_dir(dir,256 | 128 | 64);
    escfrom = g_strdup(purple_escape_filename(from));
    escname = g_strdup(purple_escape_filename(name));
    escvalidator = g_strdup(purple_escape_filename(validator));
    filename = g_strdup_printf("%s/%s-%s-%s.png",dir,escfrom,escname,escvalidator);
    purple_util_write_data_to_file_absolute(filename,((char *)rawimg),rawimglen);
    g_free(dir);
    g_free(escfrom);
    g_free(escname);
    g_free(escvalidator);
    g_free(filename);
  }
  tmp = (g_hash_table_lookup(hash,"fw"));
  width = atoi(tmp);
  tmp = (g_hash_table_lookup(hash,"fh"));
  height = atoi(tmp);
  tmp = (g_hash_table_lookup(hash,"layer"));
  layer = atoi(tmp);
  purple_debug_info("prpl-loubserp-mxit","ImageStrip %s from %s: [w=%i h=%i l=%i validator=%s]\n",name,from,width,height,layer,validator);
}
/*------------------------------------------------------------------------
 * Process a Chat-Screen-Info MXit command.
 *  [::op=csi:]
 *
 *  @param session		The MXit session object
 *  @param from			The sender of the message.
 */

static void command_screeninfo(struct MXitSession *session,const char *from)
{
  char *response;
  purple_debug_info("prpl-loubserp-mxit","Chat Screen Info received from %s\n",from);
// TODO: Determine width, height, colors of chat-screen.
  response = g_strdup_printf("::type=csi|res=bhvr,0;w,%i;h,%i;col,0.ffffffff,29.ff000000:",300,400);
/* send response back to MXit */
  mxit_send_message(session,from,response,0,(!0));
  g_free(response);
}
/*------------------------------------------------------------------------
 * Process a Chat-Screen-Configure MXit command.
 *  [::op=csc|bhvr=|menu=<menu>|col=<colors>:]
 *  where:
 *   menu ::= <menuitem> { ";" <menuitem> }
 *     menuitem ::= { type "," <text> "," <name> "," <meta> }
 *   colors ::= <color> { ";" <color> }
 *     color ::= <colorid> "," <ARGB hex color>
 *
 *  @param session		The MXit session object
 *  @param from			The sender of the message.
 *  @param hash			The MXit command <key,value> map
 */

static void command_screenconfig(struct MXitSession *session,const char *from,GHashTable *hash)
{
  const char *tmp;
  purple_debug_info("prpl-loubserp-mxit","Chat Screen Configure received from %s\n",from);
/* Behaviour */
  tmp = (g_hash_table_lookup(hash,"bhvr"));
  if (tmp != 0) {
    purple_debug_info("prpl-loubserp-mxit","  behaviour = %s\n",tmp);
// TODO: Re-configure conversation screen.
  }
/* Menu */
  tmp = (g_hash_table_lookup(hash,"menu"));
  if (tmp != 0) {
    purple_debug_info("prpl-loubserp-mxit","  menu = %s\n",tmp);
// TODO: Implement conversation-specific sub-menu.
  }
/* Colours */
  tmp = (g_hash_table_lookup(hash,"col"));
  if (tmp != 0) {
    purple_debug_info("prpl-loubserp-mxit","  colours = %s\n",tmp);
// TODO: Re-configuration conversation colors.
  }
}
/*------------------------------------------------------------------------
 * Process a Table Markup MXit command.
 *
 *  @param mx			The received message data object
 *  @param hash			The MXit command <key,value> map
 */

static void command_table(struct RXMsgData *mx,GHashTable *hash)
{
  const char *tmp;
  const char *name;
  int mode;
  int nr_columns = 0;
  int nr_rows = 0;
  gchar **coldata;
  int i;
  int j;
/* table name */
  name = (g_hash_table_lookup(hash,"nm"));
/* number of columns */
  tmp = (g_hash_table_lookup(hash,"col"));
  nr_columns = atoi(tmp);
/* number of rows */
  tmp = (g_hash_table_lookup(hash,"row"));
  nr_rows = atoi(tmp);
/* mode */
  tmp = (g_hash_table_lookup(hash,"mode"));
  mode = atoi(tmp);
/* table data */
  tmp = (g_hash_table_lookup(hash,"d"));
/* split into entries for each row & column */
  coldata = g_strsplit(tmp,"~",0);
  purple_debug_info("prpl-loubserp-mxit","Table %s from %s: [cols=%i rows=%i mode=%i]\n",name,(mx -> from),nr_columns,nr_rows,mode);
  for (i = 0; i < nr_rows; i++) {
    for (j = 0; j < nr_columns; j++) {
      purple_debug_info("prpl-loubserp-mxit"," Row %i Column %i = %s\n",i,j,coldata[(i * nr_columns) + j]);
    }
  }
}
/*------------------------------------------------------------------------
 * Process a received MXit Command message.
 *
 *  @param mx				The received message data object
 *  @param message			The message text
 *  @return					The length of the command
 */

int mxit_parse_command(struct RXMsgData *mx,char *message)
{
  GHashTable *hash = (GHashTable *)((void *)0);
  char *start;
  char *end;
/* ensure that this is really a command */
  if ((message[0] != ':') || (message[1] != ':')) {
/* this is not a command */
    return 0;
  }
  start = (message + 2);
  end = strstr(start,":");
  if (end != 0) {
/* end of a command found */
/* terminate command string */
     *end = 0;
/* break into <key,value> pairs */
    hash = command_tokenize(start);
    if (hash != 0) {
      MXitCommandType type = command_type(hash);
      switch(type){
        case MXIT_CMD_CLEAR:
{
          command_clear((mx -> session),(mx -> from),hash);
          break; 
        }
        case MXIT_CMD_REPLY:
{
          command_reply(mx,hash);
          break; 
        }
        case MXIT_CMD_PLATREQ:
{
          command_platformreq(hash,(mx -> msg));
          break; 
        }
        case MXIT_CMD_IMAGE:
{
          command_image(mx,hash,(mx -> msg));
          break; 
        }
        case MXIT_CMD_SCREENCONFIG:
{
          command_screenconfig((mx -> session),(mx -> from),hash);
          break; 
        }
        case MXIT_CMD_SCREENINFO:
{
          command_screeninfo((mx -> session),(mx -> from));
          break; 
        }
        case MXIT_CMD_IMAGESTRIP:
{
          command_imagestrip((mx -> session),(mx -> from),hash);
          break; 
        }
        case MXIT_CMD_TABLE:
{
          command_table(mx,hash);
          break; 
        }
        default:
{
/* command unknown, or not currently supported */
          break; 
        }
      }
      g_hash_table_destroy(hash);
    }
     *end = ':';
    return (end - message);
  }
  else {
    return 0;
  }
}
