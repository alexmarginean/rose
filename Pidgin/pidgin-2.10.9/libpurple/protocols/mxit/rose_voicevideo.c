/*
 *					MXit Protocol libPurple Plugin
 *
 *						-- voice & video --
 *
 *				Andrew Victor	<libpurple@mxit.com>
 *
 *			(C) Copyright 2010	MXit Lifestyle (Pty) Ltd.
 *				<http://www.mxitlifestyle.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "mxit.h"
#include "roster.h"
#include "voicevideo.h"
#if defined(USE_VV) && defined(MXIT_DEV_VV)
#warning "MXit VV support enabled."
/*------------------------------------------------------------------------
 * Does this client support Voice?
 */
/*------------------------------------------------------------------------
 * Does this client support Voice and Video?
 */
/*------------------------------------------------------------------------
 * Return the list of media capabilities this contact supports.
 *
 *  @param account		The MXit account object
 *  @param who			The username of the contact.
 *  @return				The media capabilities supported
 */
/* We need to have a voice/video server */
/* find the buddy information for this contact (reference: "libpurple/blist.h") */
/* can only communicate with MXit users */
/* and only with contacts in the 'Both' subscription state */
/* and only when they're online */
/* they support voice-only */
/* they support voice-and-video */
// TODO: Send INVITE via SIP.
// TODO: ??
/*------------------------------------------------------------------------
 * Initiate a voice/video session with a contact.
 *
 *  @param account		The MXit account object
 *  @param who			The username of the contact.
 *  @param type			The type of media session to initiate
 *  @return				TRUE if session was initiated
 */
/* attach callbacks */
/* initiate audio session */
/* initiate video session */
#else
/*
 * Voice and Video not supported.
 */

gboolean mxit_audio_enabled()
{
  return 0;
}

gboolean mxit_video_enabled()
{
  return 0;
}

PurpleMediaCaps mxit_media_caps(PurpleAccount *account,const char *who)
{
  return PURPLE_MEDIA_CAPS_NONE;
}

gboolean mxit_media_initiate(PurpleAccount *account,const char *who,PurpleMediaSessionType type)
{
  return 0;
}
#endif
