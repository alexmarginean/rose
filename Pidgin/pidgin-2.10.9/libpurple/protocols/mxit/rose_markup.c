/*
 *					MXit Protocol libPurple Plugin
 *
 *			-- convert between MXit and libPurple markup --
 *
 *				Pieter Loubser	<libpurple@mxit.com>
 *
 *			(C) Copyright 2009	MXit Lifestyle (Pty) Ltd.
 *				<http://www.mxitlifestyle.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include	"internal.h"
#include	"debug.h"
#include	"protocol.h"
#include	"mxit.h"
#include	"markup.h"
#include	"chunk.h"
#include	"formcmds.h"
#include	"roster.h"
/* define this to enable emoticon (markup) debugging */
#undef		MXIT_DEBUG_EMO
/* define this to enable markup conversion debugging */
#undef		MXIT_DEBUG_MARKUP
#define		MXIT_FRAME_MAGIC		"MXF\x01"			/* mxit emoticon magic number */
#define		MXIT_MAX_EMO_ID			16					/* maximum emoticon ID length */
#define		COLORCODE_LEN			6					/* colour code ID length */
/* HTML tag types */
#define		MXIT_TAG_COLOR			0x01				/* font color tag */
#define		MXIT_TAG_SIZE			0x02				/* font size tag */
#define		MXIT_MAX_MSG_TAGS		90					/* maximum tags per message (pigdin hack work around) */
/*
 * a HTML tag object
 */

struct tag 
{
  char type;
  char *value;
}
;
#define		MXIT_VIBE_MSG_COLOR			"#9933FF"
#define		MXIT_FAREWELL_MSG_COLOR		"#949494"
/* vibes */
static const char *vibes[] = {("Cool Vibrations"), ("Purple Rain"), ("Polite"), ("Rock n Roll"), ("Summer Slumber"), ("Electric Razor"), ("S.O.S"), ("Jack Hammer"), ("Bumble Bee"), ("Ripple")
/* 0 */
/* 1 */
/* 2 */
/* 3 */
/* 4 */
/* 5 */
/* 6 */
/* 7 */
/* 8 */
/* 9 */
};
#ifdef	MXIT_DEBUG_EMO
/*------------------------------------------------------------------------
 * Dump a byte buffer as hexadecimal to the console for debugging purposes.
 *
 *  @param buf				The data to dump
 *  @param len				The length of the data
 */
#endif
/*------------------------------------------------------------------------
 * Adds a link to a message
 *
 *  @param mx				The Markup message object
 *	@param replydata		This is the what will be returned when the link gets clicked
 *	@param isStructured		Indicates that the reply is a structured reply
 *	@param displaytext		This is the text for the link which will be displayed in the UI
 */

void mxit_add_html_link(struct RXMsgData *mx,const char *replydata,gboolean isStructured,const char *displaytext)
{
#ifdef	MXIT_LINK_CLICK
  gchar *link = (gchar *)((void *)0);
  gchar *link64 = (gchar *)((void *)0);
/*
	 * The link content is encoded as follows:
	 *  MXIT_LINK_KEY | ACCOUNT_USER | ACCOUNT_PROTO | REPLY_TO | REPLY_FORMAT | REPLY_DATA
	 */
  link = g_strdup_printf("%s|%s|%s|%s|%i|%s","MXIT",purple_account_get_username(( *(mx -> session)).acc),purple_account_get_protocol_id(( *(mx -> session)).acc),(mx -> from),((isStructured != 0)?1 : 0),replydata);
  link64 = purple_base64_encode(((const unsigned char *)link),strlen(link));
  g_string_append_printf((mx -> msg),"<a href=\"%s%s\">%s</a>","gopher://",link64,displaytext);
  g_free(link64);
  g_free(link);
#else
#endif
}
/*------------------------------------------------------------------------
 * Extract an ASN.1 formatted length field from the data.
 *
 *  @param data				The source data
 *  @param size				The extracted length
 *  @return					The number of bytes extracted
 */

static unsigned int asn_getlength(const gchar *data,int *size)
{
  unsigned int len = 0;
  unsigned char bytes;
  unsigned char byte;
  int i;
/* first byte specifies the number of bytes in the length */
  bytes = (data[0] & ~128);
  if (bytes > sizeof(unsigned int )) {
/* file too big! */
    return (-1);
  }
  data++;
/* parse out the actual length */
  for (i = 0; i < bytes; i++) {
    byte = data[i];
    len <<= 8;
    len += byte;
  }
   *size = len;
  return (bytes + 1);
}
/*------------------------------------------------------------------------
 * Extract an ASN.1 formatted UTF-8 string field from the data.
 *
 *  @param data				The source data
 *  @param type				Expected type of string
 *  @param utf8				The extracted string.  Must be deallocated by caller.
 *  @return					The number of bytes extracted
 */

static int asn_getUtf8(const gchar *data,gchar type,char **utf8)
{
  unsigned int len;
  gchar *out_str;
/* validate the field type [1 byte] */
  if (data[0] != type) {
/* this is not a utf-8 string! */
    purple_debug_error("prpl-loubserp-mxit","Invalid UTF-8 encoded string in ASN data (got 0x%02X, expected 0x%02X)\n",data[0],type);
    return -1;
  }
/* length field [1 byte] */
  len = ((uint8_t )data[1]);
  out_str = (g_malloc((len + 1)));
/* data field */
  memcpy(out_str,(data + 2),len);
  out_str[len] = 0;
   *utf8 = out_str;
  return (len + 2);
}
/*------------------------------------------------------------------------
 * Free data associated with a Markup message object.
 *
 *  @param mx				The Markup message object
 */

static void free_markupdata(struct RXMsgData *mx)
{
  if (mx != 0) {
    if ((mx -> msg) != 0) 
      g_string_free((mx -> msg),(!0));
    if ((mx -> from) != 0) 
      g_free((mx -> from));
    g_free(mx);
  }
}
/*------------------------------------------------------------------------
 * Split the message into smaller messages and send them one at a time
 * to pidgin to be displayed on the UI
 *
 *  @param mx				The received message object
 */

static void mxit_show_split_message(struct RXMsgData *mx)
{
  GString *msg = (GString *)((void *)0);
  char *ch = (char *)((void *)0);
  unsigned int pos = 0;
  unsigned int start = 0;
  unsigned int l_nl = 0;
  unsigned int l_sp = 0;
  unsigned int l_gt = 0;
  unsigned int stop = 0;
  int tags = 0;
  gboolean intag = 0;
/*
	 * awful hack to work around the awful hack in pidgin to work around GtkIMHtml's
	 * inefficient rendering of messages with lots of formatting changes.
	 * (reference: see the function pidgin_conv_write_conv() in gtkconv.c) the issue
	 * is that when you have more than 100 '<' characters in the message passed to
	 * pidgin, none of the markup (including links) are rendered and thus just dump
	 * all the text as is to the conversation window. this message dump is very
	 * confusing and makes it totally unusable. to work around this we will count
	 * the amount of tags and if its more than the pidgin threshold, we will just
	 * break the message up into smaller parts and send them separately to pidgin.
	 * to the user it will look like multiple messages, but at least he will be able
	 * to use and understand it.
	 */
  ch = ( *(mx -> msg)).str;
  pos = start;
  while(ch[pos] != 0){
    if (ch[pos] == 60) {
      tags++;
      intag = (!0);
    }
    else if (ch[pos] == 10) {
      l_nl = pos;
    }
    else if (ch[pos] == '>') {
      l_gt = pos;
      intag = 0;
    }
    else if (ch[pos] == 32) {
/* ignore spaces inside tags */
      if (!(intag != 0)) 
        l_sp = pos;
    }
    else if (((ch[pos] == 'w') && ((pos + 4) < ( *(mx -> msg)).len)) && (memcmp((ch + pos),"www.",4) == 0)) {
      tags += 2;
    }
    else if (((ch[pos] == 'h') && ((pos + 8) < ( *(mx -> msg)).len)) && (memcmp((ch + pos),"http://",7) == 0)) {
      tags += 2;
    }
    if (tags > 90) {
/* we have reached the maximum amount of tags pidgin (gtk) can handle per message.
			   so its time to send what we have and then start building a new message */
/* now find the right place to break the message */
      if (l_nl > start) {
/* break at last '\n' char */
        stop = l_nl;
        ch[stop] = 0;
        msg = g_string_new((ch + start));
        ch[stop] = 10;
      }
      else if (l_sp > start) {
/* break at last ' ' char */
        stop = l_sp;
        ch[stop] = 0;
        msg = g_string_new((ch + start));
        ch[stop] = 32;
      }
      else {
/* break at the last '>' char */
        char t;
        stop = (l_gt + 1);
        t = ch[stop];
        ch[stop] = 0;
        msg = g_string_new((ch + start));
        ch[stop] = t;
        stop--;
      }
/* push message to pidgin */
      serv_got_im(( *(mx -> session)).con,(mx -> from),(msg -> str),(mx -> flags),(mx -> timestamp));
      g_string_free(msg,(!0));
      msg = ((GString *)((void *)0));
/* next part need this flag set */
      mx -> flags |= PURPLE_MESSAGE_RAW;
      tags = 0;
      start = (stop + 1);
      pos = start;
    }
    else 
      pos++;
  }
  if (start != pos) {
/* send the last part of the message */
/* build the string */
    ch[pos] = 0;
    msg = g_string_new((ch + start));
    ch[pos] = 10;
/* push message to pidgin */
    serv_got_im(( *(mx -> session)).con,(mx -> from),(msg -> str),(mx -> flags),(mx -> timestamp));
    g_string_free(msg,(!0));
    msg = ((GString *)((void *)0));
  }
}
/*------------------------------------------------------------------------
 * Insert custom emoticons and inline images into the message (if there
 * are any), then give the message to the UI to display to the user.
 *
 *  @param mx				The received message object
 */

void mxit_show_message(struct RXMsgData *mx)
{
  char *pos;
  int start;
  unsigned int end;
  int emo_ofs;
  char *ii;
  char tag[64UL];
  int *img_id;
  if ((mx -> got_img) != 0) {{
/* search and replace all emoticon tags with proper image tags */
      while((pos = strstr(( *(mx -> msg)).str,"<MXII=")) != ((char *)((void *)0))){
/* offset at which MXIT_II_TAG starts */
        start = (pos - ( *(mx -> msg)).str);
/* offset at which EMO's ID starts */
        emo_ofs = (start + strlen("<MXII="));
/* offset at which MXIT_II_TAG ends */
        end = (emo_ofs + 1);
        while((end < ( *(mx -> msg)).len) && (( *(mx -> msg)).str[end] != '>'))
          end++;
/* end of emoticon tag not found */
        if (end == ( *(mx -> msg)).len) 
          break; 
        ii = g_strndup((( *(mx -> msg)).str + emo_ofs),(end - emo_ofs));
/* remove inline image tag */
        g_string_erase((mx -> msg),start,((end - start) + 1));
/* find the image entry */
        img_id = ((int *)(g_hash_table_lookup(( *(mx -> session)).iimages,ii)));
        if (!(img_id != 0)) {
/* inline image not found, so we will just skip it */
          purple_debug_error("prpl-loubserp-mxit","inline image NOT found (%s)\n",ii);
        }
        else {
/* insert img tag */
          g_snprintf(tag,(sizeof(tag)),"<img id=\"%i\">", *img_id);
          g_string_insert((mx -> msg),start,tag);
        }
        g_free(ii);
      }
    }
  }
#ifdef MXIT_DEBUG_MARKUP
#endif
  if ((mx -> processed) != 0) {
/* this message has already been taken care of, so just ignore it here */
  }
  else if ((mx -> chatid) < 0) {
/* normal chat message */
    mxit_show_split_message(mx);
  }
  else {
/* this is a multimx message */
    serv_got_chat_in(( *(mx -> session)).con,(mx -> chatid),(mx -> from),(mx -> flags),( *(mx -> msg)).str,(mx -> timestamp));
  }
/* freeup resource */
  free_markupdata(mx);
}
/*------------------------------------------------------------------------
 * Extract the custom emoticon ID from the message.
 *
 *  @param message			The input data
 *  @param emid				The extracted emoticon ID
 */

static void parse_emoticon_str(const char *message,char *emid)
{
  int i;
  for (i = 0; ((message[i] != 0) && (message[i] != '}')) && (i < 16); i++) {
    emid[i] = message[i];
  }
  if (message[i] == 0) {
/* end of message reached, ignore the tag */
    emid[0] = 0;
  }
  else if (i == 16) {
/* invalid tag length, ignore the tag */
    emid[0] = 0;
  }
  else 
    emid[i] = 0;
}
/*------------------------------------------------------------------------
 * Callback function invoked when a custom emoticon request to the WAP site completes.
 *
 *  @param url_data
 *  @param user_data		The Markup message object
 *  @param url_text			The data returned from the WAP site
 *  @param len				The length of the data returned
 *  @param error_message	Descriptive error message
 */

static void emoticon_returned(PurpleUtilFetchUrlData *url_data,gpointer user_data,const gchar *url_text,gsize len,const gchar *error_message)
{
  struct RXMsgData *mx = (struct RXMsgData *)user_data;
  const gchar *data = url_text;
  unsigned int pos = 0;
  int id;
  char *str;
  int em_size = 0;
  char *em_data = (char *)((void *)0);
  char *em_id = (char *)((void *)0);
  int *intptr = (int *)((void *)0);
  int res;
  purple_debug_info("prpl-loubserp-mxit","emoticon_returned\n");
/* remove request from the async outstanding calls list */
  ( *(mx -> session)).async_calls = g_slist_remove(( *(mx -> session)).async_calls,url_data);
  if (!(url_text != 0)) {
/* no reply from the WAP site */
    purple_debug_error("prpl-loubserp-mxit","Error contacting the MXit WAP site. Please try again later (emoticon).\n");
    goto done;
  }
#ifdef	MXIT_DEBUG_EMO
#endif
/* validate that the returned data starts with the magic constant that indicates it is a custom emoticon */
  if (memcmp("MXF\001",(data + pos),strlen("MXF\001")) != 0) {
    purple_debug_error("prpl-loubserp-mxit","Invalid emoticon received from wapsite (bad magic)\n");
    goto done;
  }
  pos += strlen("MXF\001");
/* validate the image frame desc byte */
  if (data[pos] != '\x6F') {
    purple_debug_error("prpl-loubserp-mxit","Invalid emoticon received from wapsite (bad frame desc)\n");
    goto done;
  }
  pos++;
/* get the frame image data length */
  res = (asn_getlength((data + pos),&em_size));
  if (res <= 0) {
    purple_debug_error("prpl-loubserp-mxit","Invalid emoticon received from wapsite (bad frame length)\n");
    goto done;
  }
  pos += res;
#ifdef	MXIT_DEBUG_EMO
#endif
/* utf-8 (emoticon name) */
  res = asn_getUtf8((data + pos),12,&str);
  if (res <= 0) {
    purple_debug_error("prpl-loubserp-mxit","Invalid emoticon received from wapsite (bad name string)\n");
    goto done;
  }
  pos += res;
#ifdef	MXIT_DEBUG_EMO
#endif
  g_free(str);
  str = ((char *)((void *)0));
/* utf-8 (emoticon shortcut) */
  res = asn_getUtf8((data + pos),0x81,&str);
  if (res <= 0) {
    purple_debug_error("prpl-loubserp-mxit","Invalid emoticon received from wapsite (bad shortcut string)\n");
    goto done;
  }
  pos += res;
#ifdef	MXIT_DEBUG_EMO
#endif
  em_id = str;
/* validate the image data type */
  if (data[pos] != '\x82') {
    purple_debug_error("prpl-loubserp-mxit","Invalid emoticon received from wapsite (bad data type)\n");
    g_free(em_id);
    goto done;
  }
  pos++;
/* get the data length */
  res = (asn_getlength((data + pos),&em_size));
  if (res <= 0) {
/* bad frame length */
    purple_debug_error("prpl-loubserp-mxit","Invalid emoticon received from wapsite (bad data length)\n");
    g_free(em_id);
    goto done;
  }
  pos += res;
#ifdef	MXIT_DEBUG_EMO
#endif
/* strip the mxit markup tags from the emoticon id (eg, .{XY} -> XY) */
  if ((em_id[0] == '.') && (em_id[1] == '{')) {
    char emo[17UL];
    parse_emoticon_str((em_id + 2),emo);
    strcpy(em_id,emo);
  }
  if (g_hash_table_lookup(( *(mx -> session)).iimages,em_id) != 0) {
/* emoticon found in the table, so ignore this one */
    g_free(em_id);
    goto done;
  }
/* make a copy of the data */
  em_data = (g_malloc(em_size));
  memcpy(em_data,(data + pos),em_size);
/* we now have the emoticon, store it in the imagestore */
  id = purple_imgstore_add_with_id(em_data,em_size,0);
/* map the mxit emoticon id to purple image id */
  intptr = (g_malloc((sizeof(int ))));
   *intptr = id;
  g_hash_table_insert(( *(mx -> session)).iimages,em_id,intptr);
  mx -> flags |= PURPLE_MESSAGE_IMAGES;
  done:
  mx -> img_count--;
  if (((mx -> img_count) == 0) && ((mx -> converted) != 0)) {
/*
		 * this was the last outstanding emoticon for this message,
		 * so we can now display it to the user.
		 */
    mxit_show_message(mx);
  }
}
/*------------------------------------------------------------------------
 * Send a request to the MXit WAP site to download the specified emoticon.
 *
 *  @param mx				The Markup message object
 *  @param id				The ID for the emoticon
 */

static void emoticon_request(struct RXMsgData *mx,const char *id)
{
  PurpleUtilFetchUrlData *url_data;
  const char *wapserver;
  char *url;
  purple_debug_info("prpl-loubserp-mxit","sending request for emoticon \'%s\'\n",id);
  wapserver = purple_account_get_string(( *(mx -> session)).acc,"wap_server","http://www.mxit.com");
/* reference: "libpurple/util.h" */
  url = g_strdup_printf("%s/res/\?type=emo&mlh=%i&sc=%s&ts=%li",wapserver,18,id,time(0));
  url_data = purple_util_fetch_url_request(url,(!0),0,(!0),0,0,emoticon_returned,mx);
  if (url_data != 0) 
    ( *(mx -> session)).async_calls = g_slist_prepend(( *(mx -> session)).async_calls,url_data);
  g_free(url);
}
/*------------------------------------------------------------------------
 * Parse a Vibe command.
 *
 *  @param mx				The Markup message object
 *  @param message			The message text (which contains the vibe)
 *  @return id				The length of the message to skip
 */

static int mxit_parse_vibe(struct RXMsgData *mx,const char *message)
{
  unsigned int vibeid;
  vibeid = (message[2] - 48);
  purple_debug_info("prpl-loubserp-mxit","Vibe received (%i)\n",vibeid);
  if (vibeid > sizeof(vibes) / sizeof(vibes[0]) - 1) {
    purple_debug_warning("prpl-loubserp-mxit","Unsupported vibe received (%i)\n",vibeid);
/* unsupported vibe */
    return 0;
  }
  g_string_append_printf((mx -> msg),"<font color=\"%s\"><i>%s Vibe...</i></font>","#9933FF",((const char *)(dgettext("pidgin",vibes[vibeid]))));
  return 2;
}
/*------------------------------------------------------------------------
 * Extract the nickname from a chatroom message and display it nicely in
 * libPurple-style (HTML) markup.
 *
 *  @param mx				The received message data object
 *  @param message			The message text
 *  @return					The length of the message to skip
 */

static int mxit_extract_chatroom_nick(struct RXMsgData *mx,char *message,int len,int msgflags)
{
  int i;
  if (message[0] == 60) {
/*
		 * The message MIGHT contains an embedded nickname.  But we can't
		 * be sure unless we find the end-of-nickname sequence: (>\n)
		 * Search for it....
		 */
    gboolean found = 0;
{
      for (i = 1; i < len; i++) {
        if ((message[i] == 10) && (message[i - 1] == '>')) {
          found = (!0);
/* loose the '>' */
          message[i - 1] = 0;
/* and skip the new-line */
          i++;
          break; 
        }
      }
    }
    if (found != 0) {
      gchar *nickname;
/*
			 * The message definitely had an embedded nickname - generate a marked-up
			 * message to be displayed.
			 */
      nickname = g_markup_escape_text((message + 1),(-1));
/* Remove any MXit escaping from nickname ("\X" --> "X") */
      if ((msgflags & 0x0200) != 0) {
        int nicklen = (strlen(nickname));
        int j;
        int k;
        for (((j = 0) , (k = 0)); j < nicklen; j++) {
          if (nickname[j] == '\\') 
            j++;
          nickname[k] = nickname[j];
          k++;
        }
/* terminate string */
        nickname[k] = 0;
      }
/* add nickname within some BOLD markup to the new converted message */
      g_string_append_printf((mx -> msg),"<b>%s:</b> ",nickname);
/* free up the resources */
      g_free(nickname);
      return i;
    }
  }
  return 0;
}
/*------------------------------------------------------------------------
 * Convert a message containing MXit protocol markup to libPurple-style (HTML) markup.
 *
 *  @param mx				The received message data object
 *  @param message			The message text
 *  @param len				The length of the message
 */

void mxit_parse_markup(struct RXMsgData *mx,char *message,int len,short msgtype,int msgflags)
{
  char tmpstr1[128UL];
  char *ch;
  int i = 0;
/* tags */
  gboolean tag_bold = 0;
  gboolean tag_under = 0;
  gboolean tag_italic = 0;
  int font_size = 0;
#ifdef MXIT_DEBUG_MARKUP
#endif
/*
	 * supported MXit markup:
	 * '*'			bold
	 * '_'			underline
	 * '/'			italics
	 * '$'			highlight text
	 * '.+' 		inc font size
	 * '.-'			dec font size
	 * '#XXXXXX'	foreground color
	 * '.{XX}'		custom emoticon
	 * '\'			escape the following character
	 * '::'			MXit commands
	 */
  if (is_mxit_chatroom_contact((mx -> session),(mx -> from)) != 0) {
/* chatroom message, so we need to extract and skip the sender's nickname
		 * which is embedded inside the message */
    i = mxit_extract_chatroom_nick(mx,message,len,msgflags);
  }
/* run through the message and check for custom emoticons and markup */
  for (; i < len; i++) {{
      switch(message[i]){
/* mxit markup parsing */
        case '*':
{
          if (!((msgflags & 0x0200) != 0)) {
            g_string_append_c_inline((mx -> msg),message[i]);
            break; 
          }
/* bold markup */
          if (!(tag_bold != 0)) 
            g_string_append((mx -> msg),"<b>");
          else 
            g_string_append((mx -> msg),"</b>");
          tag_bold = !(tag_bold != 0);
          break; 
        }
        case '_':
{
          if (!((msgflags & 0x0200) != 0)) {
            g_string_append_c_inline((mx -> msg),message[i]);
            break; 
          }
/* underscore markup */
          if (!(tag_under != 0)) 
            g_string_append((mx -> msg),"<u>");
          else 
            g_string_append((mx -> msg),"</u>");
          tag_under = !(tag_under != 0);
          break; 
        }
        case '/':
{
          if (!((msgflags & 0x0200) != 0)) {
            g_string_append_c_inline((mx -> msg),message[i]);
            break; 
          }
/* italics markup */
          if (!(tag_italic != 0)) 
            g_string_append((mx -> msg),"<i>");
          else 
            g_string_append((mx -> msg),"</i>");
          tag_italic = !(tag_italic != 0);
          break; 
        }
        case '$':
{
          if (!((msgflags & 0x0200) != 0)) {
            g_string_append_c_inline((mx -> msg),message[i]);
            break; 
          }
          else if ((i + 1) >= len) {
/* message too short for complete link */
            g_string_append_c_inline((mx -> msg),'$');
            break; 
          }
/* find the end tag */
          ch = strstr((message + (i + 1)),"$");
          if (ch != 0) {
/* end found */
             *ch = 0;
            mxit_add_html_link(mx,(message + (i + 1)),0,(message + (i + 1)));
             *ch = '$';
            i += ((ch - (message + (i + 1))) + 1);
          }
          else {
            g_string_append_c_inline((mx -> msg),message[i]);
          }
/* highlight text */
          break; 
        }
        case '#':
{
          if (!((msgflags & 0x0200) != 0)) {
            g_string_append_c_inline((mx -> msg),message[i]);
            break; 
          }
          else if ((i + 6) >= len) {
/* message too short for complete colour code */
            g_string_append_c_inline((mx -> msg),'#');
            break; 
          }
/* foreground (text) color */
          memcpy(tmpstr1,(message + (i + 1)),6);
/* terminate string */
          tmpstr1[6] = 0;
          if (strcmp(tmpstr1,"\?\?\?\?\?\?") == 0) {
/* need to reset the font */
            g_string_append((mx -> msg),"</font>");
            i += 6;
          }
          else if (strspn(tmpstr1,"0123456789abcdefABCDEF") == 6) {
/* definitely a numeric colour code */
            g_string_append_printf((mx -> msg),"<font color=\"#%s\">",tmpstr1);
            i += 6;
          }
          else {
/* not valid colour markup */
            g_string_append_c_inline((mx -> msg),'#');
          }
          break; 
        }
        case '.':
{
          if ((i + 1) >= len) {
/* message too short */
            g_string_append_c_inline((mx -> msg),'.');
            break; 
          }
          if (((msgflags & 1024) != 0) && (message[i + 1] == '{')) {
/* custom emoticon */
            if ((i + 2) >= len) {
/* message too short */
              g_string_append_c_inline((mx -> msg),'.');
              break; 
            }
            parse_emoticon_str((message + (i + 2)),tmpstr1);
            if (tmpstr1[0] != 0) {
              mx -> got_img = (!0);
              if (g_hash_table_lookup(( *(mx -> session)).iimages,tmpstr1) != 0) {
/* emoticon found in the cache, so we do not have to request it from the WAPsite */
              }
              else {
/* request emoticon from the WAPsite */
                mx -> img_count++;
                emoticon_request(mx,tmpstr1);
              }
              g_string_append_printf((mx -> msg),"<MXII=%s>",tmpstr1);
              i += (strlen(tmpstr1) + 2);
            }
            else 
              g_string_append_c_inline((mx -> msg),'.');
          }
          else if (((msgflags & 0x0200) != 0) && (message[i + 1] == '+')) {
/* increment text size */
            font_size++;
            g_string_append_printf((mx -> msg),"<font size=\"%+i\">",font_size);
            i++;
          }
          else if (((msgflags & 0x0200) != 0) && (message[i + 1] == '-')) {
/* decrement text size */
            font_size--;
            g_string_append_printf((mx -> msg),"<font size=\"%+i\">",font_size);
            i++;
          }
          else 
            g_string_append_c_inline((mx -> msg),'.');
          break; 
        }
        case '\\':
{
          if ((i + 1) >= len) {
/* message too short for an escaped character */
            g_string_append_c_inline((mx -> msg),'\\');
          }
          else {
/* ignore the next character, because its been escaped */
            g_string_append_c_inline((mx -> msg),message[i + 1]);
            i++;
          }
          break; 
        }
/* command parsing */
        case ':':
{
          if ((i + 1) >= len) {
/* message too short */
            g_string_append_c_inline((mx -> msg),':');
            break; 
          }
          if (message[i + 1] == 64) {
/* this is a vibe! */
            int size;
            if ((i + 2) >= len) {
/* message too short */
              g_string_append_c_inline((mx -> msg),message[i]);
              break; 
            }
            size = mxit_parse_vibe(mx,(message + i));
            if (size == 0) 
              g_string_append_c_inline((mx -> msg),message[i]);
            else 
              i += size;
          }
          else if (msgtype != 7) {
/* this is not a command message */
            g_string_append_c_inline((mx -> msg),message[i]);
          }
          else if (message[i + 1] == ':') {
/* parse out the command */
            int size;
            size = mxit_parse_command(mx,(message + i));
            if (size == 0) 
              g_string_append_c_inline((mx -> msg),':');
            else 
              i += size;
          }
          else {
            g_string_append_c_inline((mx -> msg),':');
          }
          break; 
        }
/* these aren't MXit markup, but are interpreted by libPurple */
        case '<':
{
          g_string_append((mx -> msg),"&lt;");
          break; 
        }
        case '>':
{
          g_string_append((mx -> msg),"&gt;");
          break; 
        }
        case '&':
{
          g_string_append((mx -> msg),"&amp;");
          break; 
        }
        case '"':
{
          g_string_append((mx -> msg),"&quot;");
          break; 
        }
        default:
{
/* text */
          g_string_append_c_inline((mx -> msg),message[i]);
          break; 
        }
      }
    }
  }
  if ((msgflags & 0x0800) != 0) {
/* this is a farewell message */
    g_string_prepend((mx -> msg),"<font color=\"#949494\"><i>");
    g_string_append((mx -> msg),"</i></font>");
  }
}
/*------------------------------------------------------------------------
 * Insert an inline image command.
 *
 *  @param mx				The message text as processed so far.
 *  @oaram id				The imgstore ID of the inline image.
 */

static void inline_image_add(GString *mx,int id)
{
  PurpleStoredImage *image;
  gconstpointer img_data;
  gsize img_size;
  gchar *enc;
  image = purple_imgstore_find_by_id(id);
  if (image == ((PurpleStoredImage *)((void *)0))) 
    return ;
  img_data = purple_imgstore_get_data(image);
  img_size = purple_imgstore_get_size(image);
  enc = purple_base64_encode(img_data,img_size);
  g_string_append(mx,"::op=img|dat=");
  g_string_append(mx,enc);
  g_string_append_c_inline(mx,':');
  g_free(enc);
}
/*------------------------------------------------------------------------
 * Convert libpurple (HTML) markup to MXit protocol markup (for sending to MXit).
 * Any MXit markup codes in the original message also need to be escaped.
 *
 *  @param message			The message text containing libPurple (HTML) markup
 *  @return					The message text containing MXit markup
 */

char *mxit_convert_markup_tx(const char *message,int *msgtype)
{
  GString *mx;
  struct tag *tag;
  GList *entry;
  GList *tagstack = (GList *)((void *)0);
  char *reply;
  char color[8UL];
  int len = (strlen(message));
  int i;
#ifdef MXIT_DEBUG_MARKUP
#endif
/*
	 * libPurple uses the following HTML markup codes:
	 *   Bold:			<b>...</b>
	 *   Italics:		<i>...</i>
	 *   Underline:		<u>...</u>
	 *   Strikethrough:	<s>...</s>					(NO MXIT SUPPORT)
	 *   Font size:		<font size="">...</font>
	 *   Font type:		<font face="">...</font>	(NO MXIT SUPPORT)
	 *   Font colour:	<font color=#">...</font>
	 *   Links:			<a href="">...</a>
	 *   Newline:		<br>
	 *   Inline image:  <IMG ID="">
	 * The following characters are also encoded:
	 *   &amp;  &quot;  &lt;  &gt;
	 */
/* new message data */
  mx = g_string_sized_new(len);
/* run through the message and check for HTML markup */
  for (i = 0; i < len; i++) {
    switch(message[i]){
      case '<':
{
        if ((purple_str_has_prefix((message + i),"<b>") != 0) || (purple_str_has_prefix((message + i),"</b>") != 0)) {
/* bold */
          g_string_append_c_inline(mx,'*');
        }
        else if ((purple_str_has_prefix((message + i),"<i>") != 0) || (purple_str_has_prefix((message + i),"</i>") != 0)) {
/* italics */
          g_string_append_c_inline(mx,'/');
        }
        else if ((purple_str_has_prefix((message + i),"<u>") != 0) || (purple_str_has_prefix((message + i),"</u>") != 0)) {
/* underline */
          g_string_append_c_inline(mx,'_');
        }
        else if (purple_str_has_prefix((message + i),"<br>") != 0) {
/* newline */
          g_string_append_c_inline(mx,10);
        }
        else if (purple_str_has_prefix((message + i),"<font size=") != 0) {
/* font size */
          int fontsize;
          tag = ((struct tag *)(g_malloc0_n(1,(sizeof(struct tag )))));
          tag -> type = 2;
          tagstack = g_list_prepend(tagstack,tag);
// TODO: implement size control
          if (sscanf((message + (i + 12)),"%i",&fontsize) != 0) {
            purple_debug_info("prpl-loubserp-mxit","Font size set to %i\n",fontsize);
          }
        }
        else if (purple_str_has_prefix((message + i),"<font color=") != 0) {
/* font colour */
          tag = ((struct tag *)(g_malloc0_n(1,(sizeof(struct tag )))));
          tag -> type = 1;
          tagstack = g_list_append(tagstack,tag);
          memset(color,0,(sizeof(color)));
          memcpy(color,(message + (i + 13)),7);
          g_string_append(mx,color);
        }
        else if (purple_str_has_prefix((message + i),"</font>") != 0) {
/* end of font tag */
          entry = g_list_last(tagstack);
          if (entry != 0) {
            tag = (entry -> data);
            if ((tag -> type) == 1) {
/* font color reset */
              g_string_append(mx,"#\?\?\?\?\?\?");
            }
            else if ((tag -> type) == 2) {
/* font size */
// TODO: implement size control
            }
            tagstack = g_list_remove(tagstack,tag);
            g_free(tag);
          }
        }
        else if (purple_str_has_prefix((message + i),"<IMG ID=") != 0) {
/* inline image */
          int imgid;
          if (sscanf((message + (i + 9)),"%i",&imgid) != 0) {
            inline_image_add(mx,imgid);
/* inline image must be sent as a MXit command */
             *msgtype = 7;
          }
        }
/* skip to end of tag ('>') */
        for (i++; (i < len) && (message[i] != '>'); i++) ;
        break; 
      }
/* MXit bold */
/* MXit underline */
/* MXit italic */
/* MXit font color */
      case '#':
{
      }
/* MXit highlight text */
      case '$':
{
      }
      case '*':
{
      }
      case '/':
{
      }
/* MXit escape backslash */
      case '\\':
{
      }
      case '_':
{
/* escape character */
        g_string_append(mx,"\\");
/* character to escape */
        g_string_append_c_inline(mx,message[i]);
        break; 
      }
/* might be a MXit font size change, or custom emoticon */
      case '.':
{
        if ((i + 1) < len) {
          if ((message[i + 1] == '+') || (message[i + 1] == '-')) 
/* escape "." */
            g_string_append(mx,"\\.");
          else 
            g_string_append_c_inline(mx,'.');
        }
        else 
          g_string_append_c_inline(mx,'.');
        break; 
      }
      default:
{
        g_string_append_c_inline(mx,message[i]);
        break; 
      }
    }
  }
/* unescape HTML entities to their literal characters (reference: "libpurple/utils.h") */
  reply = purple_unescape_html((mx -> str));
  g_string_free(mx,(!0));
#ifdef MXIT_DEBUG_MARKUP
#endif
  return reply;
}
/*------------------------------------------------------------------------
 * Free an emoticon entry.
 *
 *  @param key				MXit emoticon ID
 *  @param value			Imagestore ID for emoticon
 *  @param user_data		NULL (unused)
 *  @return					TRUE
 */

static gboolean emoticon_entry_free(gpointer key,gpointer value,gpointer user_data)
{
  int *imgid = value;
/* key is a string */
  g_free(key);
/* value is 'id' in imagestore */
  purple_imgstore_unref_by_id( *imgid);
  g_free(value);
  return (!0);
}
/*------------------------------------------------------------------------
 * Free all entries in the emoticon cache.
 *
 *  @param session			The MXit session object
 */

void mxit_free_emoticon_cache(struct MXitSession *session)
{
  g_hash_table_foreach_remove((session -> iimages),emoticon_entry_free,0);
  g_hash_table_destroy((session -> iimages));
}
