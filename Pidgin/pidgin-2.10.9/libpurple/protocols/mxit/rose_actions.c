/*
 *					MXit Protocol libPurple Plugin
 *
 *					-- handle MXit plugin actions --
 *
 *				Pieter Loubser	<libpurple@mxit.com>
 *
 *			(C) Copyright 2009	MXit Lifestyle (Pty) Ltd.
 *				<http://www.mxitlifestyle.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include	"internal.h"
#include	"debug.h"
#include	"request.h"
#include	"protocol.h"
#include	"mxit.h"
#include	"roster.h"
#include	"actions.h"
#include	"splashscreen.h"
#include	"cipher.h"
#include	"profile.h"
/*------------------------------------------------------------------------
 * The user has selected to change their profile.
 *
 *  @param gc		The connection object
 *  @param fields	The fields from the request pop-up
 */

static void mxit_profile_cb(PurpleConnection *gc,PurpleRequestFields *fields)
{
  struct MXitSession *session = (purple_connection_get_protocol_data(gc));
  PurpleRequestField *field = (PurpleRequestField *)((void *)0);
  const char *name = (const char *)((void *)0);
  const char *bday = (const char *)((void *)0);
  const char *err = (const char *)((void *)0);
  GList *entry = (GList *)((void *)0);
  purple_debug_info("prpl-loubserp-mxit","mxit_profile_cb\n");
  if (!(g_list_find(purple_connections_get_all(),gc) != ((GList *)((void *)0)))) {
    purple_debug_error("prpl-loubserp-mxit","Unable to update profile; account offline.\n");
    return ;
  }
/* validate name */
  name = purple_request_fields_get_string(fields,"name");
  if (!(name != 0) || (strlen(name) < 3)) {
    err = ((const char *)(dgettext("pidgin","The Display Name you entered is invalid.")));
    goto out;
  }
/* validate birthdate */
  bday = purple_request_fields_get_string(fields,"bday");
  if ((!(bday != 0) || (strlen(bday) < 10)) || !(validateDate(bday) != 0)) {
    err = ((const char *)(dgettext("pidgin","The birthday you entered is invalid. The correct format is: \'YYYY-MM-DD\'.")));
    goto out;
  }
  out:
  if (!(err != 0)) {
    struct MXitProfile *profile = (session -> profile);
    GString *attributes = g_string_sized_new(128);
    char attrib[512UL];
    unsigned int acount = 0;
/* update name */
    g_strlcpy((profile -> nickname),name,(sizeof(profile -> nickname)));
    g_snprintf(attrib,(sizeof(attrib)),"\001%s\001%i\001%s","fullname",10,(profile -> nickname));
    g_string_append(attributes,attrib);
    acount++;
/* update birthday */
    g_strlcpy((profile -> birthday),bday,(sizeof(profile -> birthday)));
    g_snprintf(attrib,(sizeof(attrib)),"\001%s\001%i\001%s","birthdate",10,(profile -> birthday));
    g_string_append(attributes,attrib);
    acount++;
/* update gender */
    profile -> male = (purple_request_fields_get_choice(fields,"male") != 0);
    g_snprintf(attrib,(sizeof(attrib)),"\001%s\001%i\001%s","gender",2,(((profile -> male) != 0)?"1" : "0"));
    g_string_append(attributes,attrib);
    acount++;
/* update title */
    name = purple_request_fields_get_string(fields,"title");
    if (!(name != 0)) 
      (profile -> title)[0] = 0;
    else 
      g_strlcpy((profile -> title),name,(sizeof(profile -> title)));
    g_snprintf(attrib,(sizeof(attrib)),"\001%s\001%i\001%s","title",10,(profile -> title));
    g_string_append(attributes,attrib);
    acount++;
/* update firstname */
    name = purple_request_fields_get_string(fields,"firstname");
    if (!(name != 0)) 
      (profile -> firstname)[0] = 0;
    else 
      g_strlcpy((profile -> firstname),name,(sizeof(profile -> firstname)));
    g_snprintf(attrib,(sizeof(attrib)),"\001%s\001%i\001%s","firstname",10,(profile -> firstname));
    g_string_append(attributes,attrib);
    acount++;
/* update lastname */
    name = purple_request_fields_get_string(fields,"lastname");
    if (!(name != 0)) 
      (profile -> lastname)[0] = 0;
    else 
      g_strlcpy((profile -> lastname),name,(sizeof(profile -> lastname)));
    g_snprintf(attrib,(sizeof(attrib)),"\001%s\001%i\001%s","lastname",10,(profile -> lastname));
    g_string_append(attributes,attrib);
    acount++;
/* update email address */
    name = purple_request_fields_get_string(fields,"email");
    if (!(name != 0)) 
      (profile -> email)[0] = 0;
    else 
      g_strlcpy((profile -> email),name,(sizeof(profile -> email)));
    g_snprintf(attrib,(sizeof(attrib)),"\001%s\001%i\001%s","email",10,(profile -> email));
    g_string_append(attributes,attrib);
    acount++;
/* update mobile number */
    name = purple_request_fields_get_string(fields,"mobilenumber");
    if (!(name != 0)) 
      (profile -> mobilenr)[0] = 0;
    else 
      g_strlcpy((profile -> mobilenr),name,(sizeof(profile -> mobilenr)));
    g_snprintf(attrib,(sizeof(attrib)),"\001%s\001%i\001%s","mobilenumber",10,(profile -> mobilenr));
    g_string_append(attributes,attrib);
    acount++;
/* update about me */
    name = purple_request_fields_get_string(fields,"aboutme");
    if (!(name != 0)) 
      (profile -> aboutme)[0] = 0;
    else 
      g_strlcpy((profile -> aboutme),name,(sizeof(profile -> aboutme)));
    g_snprintf(attrib,(sizeof(attrib)),"\001%s\001%i\001%s","aboutme",10,(profile -> aboutme));
    g_string_append(attributes,attrib);
    acount++;
/* update where am i */
    name = purple_request_fields_get_string(fields,"whereami");
    if (!(name != 0)) 
      (profile -> whereami)[0] = 0;
    else 
      g_strlcpy((profile -> whereami),name,(sizeof(profile -> whereami)));
    g_snprintf(attrib,(sizeof(attrib)),"\001%s\001%i\001%s","whereami",10,(profile -> whereami));
    g_string_append(attributes,attrib);
    acount++;
/* relationship status */
    field = purple_request_fields_get_field(fields,"relationship");
    entry = g_list_first(purple_request_field_list_get_selected(field));
    profile -> relationship = atoi((purple_request_field_list_get_data(field,(entry -> data))));
    g_snprintf(attrib,(sizeof(attrib)),"\001%s\001%i\001%i","relationship",4,(profile -> relationship));
    g_string_append(attributes,attrib);
    acount++;
/* update flags */
    field = purple_request_fields_get_field(fields,"searchable");
/* is searchable -> clear not-searchable flag */
    if (purple_request_field_bool_get_value(field) != 0) 
      profile -> flags &= (~2);
    else 
      profile -> flags |= 2;
    field = purple_request_fields_get_field(fields,"suggestable");
/* is suggestable -> clear not-suggestable flag */
    if (purple_request_field_bool_get_value(field) != 0) 
      profile -> flags &= (~8);
    else 
      profile -> flags |= 8;
    g_snprintf(attrib,(sizeof(attrib)),"\001%s\001%i\001%li","flags",6,(profile -> flags));
    g_string_append(attributes,attrib);
    acount++;
/* send the profile update to MXit */
    mxit_send_extprofile_update(session,0,acount,(attributes -> str));
    g_string_free(attributes,(!0));
  }
  else {
/* show error to user */
    mxit_popup(PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Profile Update Error"))),err);
  }
}
/*------------------------------------------------------------------------
 * Display and update the user's profile.
 *
 *  @param action	The action object
 */

static void mxit_profile_action(PurplePluginAction *action)
{
  PurpleConnection *gc = (PurpleConnection *)(action -> context);
  struct MXitSession *session = (purple_connection_get_protocol_data(gc));
  struct MXitProfile *profile = (session -> profile);
  PurpleRequestFields *fields = (PurpleRequestFields *)((void *)0);
  PurpleRequestField *field = (PurpleRequestField *)((void *)0);
  purple_debug_info("prpl-loubserp-mxit","mxit_profile_action\n");
/* ensure that we actually have the user's profile information */
  if (!(profile != 0)) {
/* no profile information yet, so we cannot update */
    mxit_popup(PURPLE_NOTIFY_MSG_WARNING,((const char *)(dgettext("pidgin","Profile"))),((const char *)(dgettext("pidgin","Your profile information is not yet retrieved. Please try again later."))));
    return ;
  }
  fields = purple_request_fields_new();
/* Public information - what other users can see */
{
    PurpleRequestFieldGroup *public_group = purple_request_field_group_new("Public information");
/* display name */
    field = purple_request_field_string_new("name",((const char *)(dgettext("pidgin","Display Name"))),(profile -> nickname),0);
    purple_request_field_group_add_field(public_group,field);
/* birthday */
    field = purple_request_field_string_new("bday",((const char *)(dgettext("pidgin","Birthday"))),(profile -> birthday),0);
    purple_request_field_group_add_field(public_group,field);
    if (((profile -> flags) & 64) != 0L) 
      purple_request_field_string_set_editable(field,0);
/* gender */
    field = purple_request_field_choice_new("male",((const char *)(dgettext("pidgin","Gender"))),(((profile -> male) != 0)?1 : 0));
/* 0 */
    purple_request_field_choice_add(field,((const char *)(dgettext("pidgin","Female"))));
/* 1 */
    purple_request_field_choice_add(field,((const char *)(dgettext("pidgin","Male"))));
    purple_request_field_group_add_field(public_group,field);
/* first name */
    field = purple_request_field_string_new("firstname",((const char *)(dgettext("pidgin","First Name"))),(profile -> firstname),0);
    purple_request_field_group_add_field(public_group,field);
/* last name */
    field = purple_request_field_string_new("lastname",((const char *)(dgettext("pidgin","Last Name"))),(profile -> lastname),0);
    purple_request_field_group_add_field(public_group,field);
/* about me */
    field = purple_request_field_string_new("aboutme",((const char *)(dgettext("pidgin","About Me"))),(profile -> aboutme),0);
    purple_request_field_group_add_field(public_group,field);
/* where I live */
    field = purple_request_field_string_new("whereami",((const char *)(dgettext("pidgin","Where I Live"))),(profile -> whereami),0);
    purple_request_field_group_add_field(public_group,field);
/* relationship status */
    field = purple_request_field_list_new("relationship",((const char *)(dgettext("pidgin","Relationship Status"))));
    purple_request_field_list_set_multi_select(field,0);
    purple_request_field_list_add_icon(field,mxit_relationship_to_name(0),0,(g_strdup_printf("%i",0)));
    purple_request_field_list_add_icon(field,mxit_relationship_to_name(1),0,(g_strdup_printf("%i",1)));
    purple_request_field_list_add_icon(field,mxit_relationship_to_name(2),0,(g_strdup_printf("%i",2)));
    purple_request_field_list_add_icon(field,mxit_relationship_to_name(3),0,(g_strdup_printf("%i",3)));
    purple_request_field_list_add_icon(field,mxit_relationship_to_name(4),0,(g_strdup_printf("%i",4)));
    purple_request_field_list_add_icon(field,mxit_relationship_to_name(5),0,(g_strdup_printf("%i",5)));
    purple_request_field_list_add_icon(field,mxit_relationship_to_name(6),0,(g_strdup_printf("%i",6)));
    purple_request_field_list_add_icon(field,mxit_relationship_to_name(7),0,(g_strdup_printf("%i",7)));
    purple_request_field_list_add_icon(field,mxit_relationship_to_name(8),0,(g_strdup_printf("%i",8)));
    purple_request_field_list_add_icon(field,mxit_relationship_to_name(9),0,(g_strdup_printf("%i",9)));
    purple_request_field_list_add_selected(field,mxit_relationship_to_name((profile -> relationship)));
    purple_request_field_group_add_field(public_group,field);
    purple_request_fields_add_group(fields,public_group);
  }
/* Private information - what only MXit can see */
{
    PurpleRequestFieldGroup *private_group = purple_request_field_group_new("Private information");
/* title */
    field = purple_request_field_string_new("title",((const char *)(dgettext("pidgin","Title"))),(profile -> title),0);
    purple_request_field_group_add_field(private_group,field);
/* email */
    field = purple_request_field_string_new("email",((const char *)(dgettext("pidgin","Email"))),(profile -> email),0);
    purple_request_field_group_add_field(private_group,field);
/* mobile number */
    field = purple_request_field_string_new("mobilenumber",((const char *)(dgettext("pidgin","Mobile Number"))),(profile -> mobilenr),0);
    purple_request_field_group_add_field(private_group,field);
/* is searchable */
    field = purple_request_field_bool_new("searchable",((const char *)(dgettext("pidgin","Can be searched"))),(((profile -> flags) & 2) == 0));
    purple_request_field_group_add_field(private_group,field);
/* is suggestable */
    field = purple_request_field_bool_new("suggestable",((const char *)(dgettext("pidgin","Can be suggested"))),(((profile -> flags) & 8) == 0));
    purple_request_field_group_add_field(private_group,field);
    purple_request_fields_add_group(fields,private_group);
  }
/* (reference: "libpurple/request.h") */
  purple_request_fields(gc,((const char *)(dgettext("pidgin","Profile"))),((const char *)(dgettext("pidgin","Update your MXit Profile"))),0,fields,((const char *)(dgettext("pidgin","Set"))),((GCallback )mxit_profile_cb),((const char *)(dgettext("pidgin","Cancel"))),0,purple_connection_get_account(gc),0,0,gc);
}
/*------------------------------------------------------------------------
 * The user has selected to change their PIN.
 *
 *  @param gc		The connection object
 *  @param fields	The fields from the request pop-up
 */

static void mxit_change_pin_cb(PurpleConnection *gc,PurpleRequestFields *fields)
{
  struct MXitSession *session = (purple_connection_get_protocol_data(gc));
  const char *pin = (const char *)((void *)0);
  const char *pin2 = (const char *)((void *)0);
  const char *err = (const char *)((void *)0);
  int len;
  int i;
  if (!(g_list_find(purple_connections_get_all(),gc) != ((GList *)((void *)0)))) {
    purple_debug_error("prpl-loubserp-mxit","Unable to update PIN; account offline.\n");
    return ;
  }
/* validate pin */
  pin = purple_request_fields_get_string(fields,"pin");
  if (!(pin != 0)) {
    err = ((const char *)(dgettext("pidgin","The PIN you entered is invalid.")));
    goto out;
  }
  len = (strlen(pin));
  if ((len < 4) || (len > 10)) {
    err = ((const char *)(dgettext("pidgin","The PIN you entered has an invalid length [4-10].")));
    goto out;
  }
  for (i = 0; i < len; i++) {
    if (!((g_ascii_table[(guchar )pin[i]] & G_ASCII_DIGIT) != 0)) {
      err = ((const char *)(dgettext("pidgin","The PIN is invalid. It should only consist of digits [0-9].")));
      goto out;
    }
  }
  pin2 = purple_request_fields_get_string(fields,"pin2");
  if (!(pin2 != 0) || (strcmp(pin,pin2) != 0)) {
    err = ((const char *)(dgettext("pidgin","The two PINs you entered do not match.")));
    goto out;
  }
  out:
  if (!(err != 0)) {
/* update PIN in account */
    purple_account_set_password((session -> acc),pin);
/* update session object */
    g_free((session -> encpwd));
    session -> encpwd = mxit_encrypt_password(session);
/* send the update request to MXit */
    mxit_send_extprofile_update(session,(session -> encpwd),0,0);
  }
  else {
/* show error to user */
    mxit_popup(PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","PIN Update Error"))),err);
  }
}
/*------------------------------------------------------------------------
 * Enable the user to change their PIN.
 *
 *  @param action	The action object
 */

static void mxit_change_pin_action(PurplePluginAction *action)
{
  PurpleConnection *gc = (PurpleConnection *)(action -> context);
  struct MXitSession *session = (purple_connection_get_protocol_data(gc));
  PurpleRequestFields *fields = (PurpleRequestFields *)((void *)0);
  PurpleRequestFieldGroup *group = (PurpleRequestFieldGroup *)((void *)0);
  PurpleRequestField *field = (PurpleRequestField *)((void *)0);
  purple_debug_info("prpl-loubserp-mxit","mxit_change_pin_action\n");
  fields = purple_request_fields_new();
  group = purple_request_field_group_new(0);
  purple_request_fields_add_group(fields,group);
/* pin */
  field = purple_request_field_string_new("pin",((const char *)(dgettext("pidgin","PIN"))),purple_account_get_password((session -> acc)),0);
  purple_request_field_string_set_masked(field,(!0));
  purple_request_field_group_add_field(group,field);
/* verify pin */
  field = purple_request_field_string_new("pin2",((const char *)(dgettext("pidgin","Verify PIN"))),purple_account_get_password((session -> acc)),0);
  purple_request_field_string_set_masked(field,(!0));
  purple_request_field_group_add_field(group,field);
/* (reference: "libpurple/request.h") */
  purple_request_fields(gc,((const char *)(dgettext("pidgin","Change PIN"))),((const char *)(dgettext("pidgin","Change MXit PIN"))),0,fields,((const char *)(dgettext("pidgin","Set"))),((GCallback )mxit_change_pin_cb),((const char *)(dgettext("pidgin","Cancel"))),0,purple_connection_get_account(gc),0,0,gc);
}
/*------------------------------------------------------------------------
 * Display the current splash-screen, or a notification pop-up if one is not available.
 *
 *  @param action	The action object
 */

static void mxit_splash_action(PurplePluginAction *action)
{
  PurpleConnection *gc = (PurpleConnection *)(action -> context);
  struct MXitSession *session = (purple_connection_get_protocol_data(gc));
  if (splash_current(session) != ((const char *)((void *)0))) 
    splash_display(session);
  else 
    mxit_popup(PURPLE_NOTIFY_MSG_INFO,((const char *)(dgettext("pidgin","View Splash"))),((const char *)(dgettext("pidgin","There is no splash-screen currently available"))));
}
/*------------------------------------------------------------------------
 * Display info about the plugin.
 *
 *  @param action	The action object
 */

static void mxit_about_action(PurplePluginAction *action)
{
  char version[256UL];
  g_snprintf(version,(sizeof(version)),"MXit Client Protocol v%i.%i\n\nAuthor:\nPieter Loubser\n\nContributors:\nAndrew Victor\n\nTesters:\nBraeme Le Roux\n\n",63 / 10,63 % 10);
  mxit_popup(PURPLE_NOTIFY_MSG_INFO,((const char *)(dgettext("pidgin","About"))),version);
}
/*------------------------------------------------------------------------
 * Request list of suggested friends.
 *
 *  @param action	The action object
 */

static void mxit_suggested_friends_action(PurplePluginAction *action)
{
  PurpleConnection *gc = (PurpleConnection *)(action -> context);
  struct MXitSession *session = (purple_connection_get_protocol_data(gc));
  const char *profilelist[] = {("birthdate"), ("gender"), ("fullname"), ("firstname"), ("lastname"), ("registeredcountry"), ("statusmsg"), ("avatarid"), ("whereami"), ("aboutme")};
  mxit_send_suggest_friends(session,30,(sizeof(profilelist) / sizeof(profilelist[0])),profilelist);
}
/*------------------------------------------------------------------------
 * Perform contact search.
 *
 *  @param action	The action object
 */

static void mxit_user_search_cb(PurpleConnection *gc,const char *input)
{
  struct MXitSession *session = (purple_connection_get_protocol_data(gc));
  const char *profilelist[] = {("birthdate"), ("gender"), ("fullname"), ("firstname"), ("lastname"), ("registeredcountry"), ("statusmsg"), ("avatarid"), ("whereami"), ("aboutme")};
  mxit_send_suggest_search(session,30,input,(sizeof(profilelist) / sizeof(profilelist[0])),profilelist);
}
/*------------------------------------------------------------------------
 * Display the search input form.
 *
 *  @param action	The action object
 */

static void mxit_user_search_action(PurplePluginAction *action)
{
  PurpleConnection *gc = (PurpleConnection *)(action -> context);
  purple_request_input(gc,((const char *)(dgettext("pidgin","Search for user"))),((const char *)(dgettext("pidgin","Search for a MXit contact"))),((const char *)(dgettext("pidgin","Type search information"))),0,0,0,0,((const char *)(dgettext("pidgin","_Search"))),((GCallback )mxit_user_search_cb),((const char *)(dgettext("pidgin","_Cancel"))),0,purple_connection_get_account(gc),0,0,gc);
}
/*------------------------------------------------------------------------
 * Associate actions with the MXit plugin.
 *
 *  @param plugin	The MXit protocol plugin
 *  @param context	The connection context (if available)
 *  @return			The list of plugin actions
 */

GList *mxit_actions(PurplePlugin *plugin,gpointer context)
{
  PurplePluginAction *action = (PurplePluginAction *)((void *)0);
  GList *m = (GList *)((void *)0);
/* display / change profile */
  action = purple_plugin_action_new(((const char *)(dgettext("pidgin","Change Profile..."))),mxit_profile_action);
  m = g_list_append(m,action);
/* change PIN */
  action = purple_plugin_action_new(((const char *)(dgettext("pidgin","Change PIN..."))),mxit_change_pin_action);
  m = g_list_append(m,action);
/* suggested friends */
  action = purple_plugin_action_new(((const char *)(dgettext("pidgin","Suggested friends..."))),mxit_suggested_friends_action);
  m = g_list_append(m,action);
/* search for contacts */
  action = purple_plugin_action_new(((const char *)(dgettext("pidgin","Search for contacts..."))),mxit_user_search_action);
  m = g_list_append(m,action);
/* display splash-screen */
  action = purple_plugin_action_new(((const char *)(dgettext("pidgin","View Splash..."))),mxit_splash_action);
  m = g_list_append(m,action);
/* display plugin version */
  action = purple_plugin_action_new(((const char *)(dgettext("pidgin","About..."))),mxit_about_action);
  m = g_list_append(m,action);
  return m;
}
