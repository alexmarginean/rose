/*
 *					MXit Protocol libPurple Plugin
 *
 *			-- MXit client protocol implementation --
 *
 *				Pieter Loubser	<libpurple@mxit.com>
 *
 *			(C) Copyright 2009	MXit Lifestyle (Pty) Ltd.
 *				<http://www.mxitlifestyle.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include	"internal.h"
#include	"debug.h"
#include	"version.h"
#include	"protocol.h"
#include	"mxit.h"
#include	"roster.h"
#include	"chunk.h"
#include	"filexfer.h"
#include	"markup.h"
#include	"multimx.h"
#include	"splashscreen.h"
#include	"login.h"
#include	"formcmds.h"
#include	"http.h"
#include	"cipher.h"
#include	"voicevideo.h"
#define		MXIT_MS_OFFSET		3
/* configure the right record terminator char to use */
#define		CP_REC_TERM			( ( session->http ) ? CP_HTTP_REC_TERM : CP_SOCK_REC_TERM )
/*------------------------------------------------------------------------
 * return the current timestamp in milliseconds
 */

gint64 mxit_now_milli()
{
  GTimeVal now;
  g_get_current_time(&now);
  return (now.tv_sec * 1000) + (now.tv_usec / 1000);
}
/*------------------------------------------------------------------------
 * Display a notification popup message to the user.
 *
 *  @param type			The type of notification:
 *		- info:		PURPLE_NOTIFY_MSG_INFO
 *		- warning:	PURPLE_NOTIFY_MSG_WARNING
 *		- error:	PURPLE_NOTIFY_MSG_ERROR
 *  @param heading		Heading text
 *  @param message		Message text
 */

void mxit_popup(int type,const char *heading,const char *message)
{
/* (reference: "libpurple/notify.h") */
  purple_notify_message(0,type,((const char *)(dgettext("pidgin","MXit Notification"))),heading,message,0,0);
}
/*------------------------------------------------------------------------
 * For compatibility with legacy clients, all usernames are sent from MXit with a domain
 *  appended.  For MXit contacts, this domain is set to "@m".  This function strips
 *  those fake domains.
 *
 *  @param username		The username of the contact
 */

void mxit_strip_domain(char *username)
{
  if (g_str_has_suffix(username,"@m") != 0) 
    username[strlen(username) - 2] = 0;
}
/*------------------------------------------------------------------------
 * Dump a byte buffer to the console for debugging purposes.
 *
 *  @param buf			The data
 *  @param len			The data length
 */

void dump_bytes(struct MXitSession *session,const char *buf,int len)
{
  char *msg = (g_malloc0((len + 1)));
  int i;
  for (i = 0; i < len; i++) {
    char ch = buf[i];
/* record terminator */
    if (ch == ((((session -> http) != 0)?'\x26' : 0))) 
      msg[i] = '!';
    else 
/* field terminator */
if (ch == 1) 
      msg[i] = '^';
    else 
/* packet terminator */
if (ch == 2) 
      msg[i] = 64;
    else 
/* non-printable character */
if ((ch < 32) || (ch > 0x7E)) 
      msg[i] = '_';
    else 
      msg[i] = ch;
  }
  purple_debug_info("prpl-loubserp-mxit","DUMP: \'%s\'\n",msg);
  g_free(msg);
}
/*------------------------------------------------------------------------
 * Determine if we have an active chat with a specific contact
 *
 *  @param session		The MXit session object
 *  @param who			The contact name
 *  @return				Return true if we have an active chat with the contact
 */

gboolean find_active_chat(const GList *chats,const char *who)
{
  const GList *list = chats;
  const char *chat = (const char *)((void *)0);
  while(list != 0){
    chat = ((const char *)(list -> data));
    if (strcmp(chat,who) == 0) 
      return (!0);
    list = (((list != 0)?( *((GList *)list)).next : ((struct _GList *)((void *)0))));
  }
  return 0;
}
/*========================================================================================================================
 * Low-level Packet transmission
 */
/*------------------------------------------------------------------------
 * Remove next packet from transmission queue.
 *
 *  @param session		The MXit session object
 *  @return				The next packet for transmission (or NULL)
 */

static struct tx_packet *pop_tx_packet(struct MXitSession *session)
{
  struct tx_packet *packet = (struct tx_packet *)((void *)0);
  if (session -> queue.count > 0) {
/* dequeue the next packet */
    packet = session -> queue.packets[session -> queue.rd_i];
    session -> queue.packets[session -> queue.rd_i] = ((struct tx_packet *)((void *)0));
    session -> queue.rd_i = ((session -> queue.rd_i + 1) % (1 << 5));
    session -> queue.count--;
  }
  return packet;
}
/*------------------------------------------------------------------------
 * Add packet to transmission queue.
 *
 *  @param session		The MXit session object
 *  @param packet		The packet to transmit
 *  @return				Return TRUE if packet was enqueue, or FALSE if queue is full.
 */

static gboolean push_tx_packet(struct MXitSession *session,struct tx_packet *packet)
{
  if (session -> queue.count < 1 << 5) {
/* enqueue packet */
    session -> queue.packets[session -> queue.wr_i] = packet;
    session -> queue.wr_i = ((session -> queue.wr_i + 1) % (1 << 5));
    session -> queue.count++;
    return (!0);
  }
  else 
/* queue is full */
    return 0;
}
/*------------------------------------------------------------------------
 * Deallocate transmission packet.
 *
 *  @param packet		The packet to deallocate.
 */

static void free_tx_packet(struct tx_packet *packet)
{
  g_free((packet -> data));
  g_free(packet);
  packet = ((struct tx_packet *)((void *)0));
}
/*------------------------------------------------------------------------
 * Flush all the packets from the tx queue and release the resources.
 *
 *  @param session		The MXit session object
 */

static void flush_queue(struct MXitSession *session)
{
  struct tx_packet *packet;
  purple_debug_info("prpl-loubserp-mxit","flushing the tx queue\n");
  while((packet = pop_tx_packet(session)) != ((struct tx_packet *)((void *)0)))
    free_tx_packet(packet);
}
/*------------------------------------------------------------------------
 * TX Step 3: Write the packet data to the TCP connection.
 *
 *  @param fd			The file descriptor
 *  @param pktdata		The packet data
 *  @param pktlen		The length of the packet data
 *  @return				Return -1 on error, otherwise 0
 */

static int mxit_write_sock_packet(int fd,const char *pktdata,int pktlen)
{
  int written;
  int res;
  written = 0;
  while(written < pktlen){
    res = (write(fd,(pktdata + written),(pktlen - written)));
    if (res <= 0) {
/* error on socket */
      if ( *__errno_location() == 11) 
        continue; 
      purple_debug_error("prpl-loubserp-mxit","Error while writing packet to MXit server (%i)\n",res);
      return -1;
    }
    written += res;
  }
  return 0;
}
/*------------------------------------------------------------------------
 * Callback called for handling a HTTP GET response
 *
 *  @param url_data			libPurple internal object (see purple_util_fetch_url_request)
 *  @param user_data		The MXit session object
 *  @param url_text			The data returned (could be NULL if error)
 *  @param len				The length of the data returned (0 if error)
 *  @param error_message	Descriptive error message
 */

static void mxit_cb_http_rx(PurpleUtilFetchUrlData *url_data,gpointer user_data,const gchar *url_text,gsize len,const gchar *error_message)
{
  struct MXitSession *session = (struct MXitSession *)user_data;
/* clear outstanding request */
  session -> async_calls = g_slist_remove((session -> async_calls),url_data);
  if (!(url_text != 0) || (len == 0)) {
/* error with request */
    purple_debug_error("prpl-loubserp-mxit","HTTP response error (%s)\n",error_message);
    return ;
  }
/* convert the HTTP result */
  memcpy((session -> rx_dbuf),url_text,len);
  session -> rx_i = len;
  mxit_parse_packet(session);
}
/*------------------------------------------------------------------------
 * TX Step 3: Write the packet data to the HTTP connection (GET style).
 *
 *  @param session		The MXit session object
 *  @param pktdata		The packet data
 *  @param pktlen		The length of the packet data
 *  @return				Return -1 on error, otherwise 0
 */

static void mxit_write_http_get(struct MXitSession *session,struct tx_packet *packet)
{
  PurpleUtilFetchUrlData *url_data;
  char *part = (char *)((void *)0);
  char *url = (char *)((void *)0);
  if ((packet -> datalen) > 0) {
    char *tmp = (char *)((void *)0);
    tmp = g_strndup((packet -> data),(packet -> datalen));
    part = g_strdup(purple_url_encode(tmp));
    g_free(tmp);
  }
  url = g_strdup_printf("%s\?%s%s",(session -> http_server),purple_url_encode((packet -> header)),(!(part != 0)?"" : part));
#ifdef	DEBUG_PROTOCOL
  purple_debug_info("prpl-loubserp-mxit","HTTP GET: \'%s\'\n",url);
#endif
/* send the HTTP request */
  url_data = purple_util_fetch_url_request(url,(!0),"libpurple-2.10.9",(!0),0,0,mxit_cb_http_rx,session);
  if (url_data != 0) 
    session -> async_calls = g_slist_prepend((session -> async_calls),url_data);
  g_free(url);
  if (part != 0) 
    g_free(part);
}
/*------------------------------------------------------------------------
 * TX Step 3: Write the packet data to the HTTP connection (POST style).
 *
 *  @param session		The MXit session object
 *  @param pktdata		The packet data
 *  @param pktlen		The length of the packet data
 *  @return				Return -1 on error, otherwise 0
 */

static void mxit_write_http_post(struct MXitSession *session,struct tx_packet *packet)
{
  char request[256 + (packet -> datalen)];
  int reqlen;
  char *host_name;
  int host_port;
  gboolean ok;
/* extract the HTTP host name and host port number to connect to */
  ok = purple_url_parse((session -> http_server),&host_name,&host_port,0,0,0);
  if (!(ok != 0)) {
    purple_debug_error("prpl-loubserp-mxit","HTTP POST error: (host name \'%s\' not valid)\n",(session -> http_server));
  }
/* strip off the last '&' from the header */
  (packet -> header)[(packet -> headerlen) - 1] = 0;
  packet -> headerlen--;
/* build the HTTP request packet */
  reqlen = g_snprintf(request,256,"POST %s\?%s HTTP/1.1\r\nUser-Agent: libpurple-2.10.9\r\nContent-Type: application/octet-stream\r\nHost: %s\r\nContent-Length: %d\r\n\r\n",(session -> http_server),purple_url_encode((packet -> header)),host_name,((packet -> datalen) - 3));
/* copy over the packet body data (could be binary) */
  memcpy((request + reqlen),((packet -> data) + 3),((packet -> datalen) - 3));
  reqlen += (packet -> datalen);
#ifdef	DEBUG_PROTOCOL
  purple_debug_info("prpl-loubserp-mxit","HTTP POST:\n");
  dump_bytes(session,request,reqlen);
#endif
/* send the request to the HTTP server */
  mxit_http_send_request(session,host_name,host_port,request,reqlen);
}
/*------------------------------------------------------------------------
 * TX Step 2: Handle the transmission of the packet to the MXit server.
 *
 *  @param session		The MXit session object
 *  @param packet		The packet to transmit
 */

static void mxit_send_packet(struct MXitSession *session,struct tx_packet *packet)
{
  int res;
  if (!(((session -> flags) & 1) != 0)) {
/* we are not connected so ignore all packets to be send */
    purple_debug_error("prpl-loubserp-mxit","Dropping TX packet (we are not connected)\n");
    return ;
  }
  purple_debug_info("prpl-loubserp-mxit","Packet send CMD:%i (%i)\n",(packet -> cmd),((packet -> headerlen) + (packet -> datalen)));
#ifdef	DEBUG_PROTOCOL
  dump_bytes(session,(packet -> header),(packet -> headerlen));
  dump_bytes(session,(packet -> data),(packet -> datalen));
#endif
  if (!((session -> http) != 0)) {
/* socket connection */
    char data[(packet -> datalen) + (packet -> headerlen)];
    int datalen;
/* create raw data buffer */
    memcpy(data,(packet -> header),(packet -> headerlen));
    memcpy((data + (packet -> headerlen)),(packet -> data),(packet -> datalen));
    datalen = ((packet -> headerlen) + (packet -> datalen));
    res = mxit_write_sock_packet((session -> fd),data,datalen);
    if (res < 0) {
/* we must have lost the connection, so terminate it so that we can reconnect */
      purple_connection_error((session -> con),((const char *)(dgettext("pidgin","We have lost the connection to MXit. Please reconnect."))));
    }
  }
  else {
/* http connection */
    if ((packet -> cmd) == 27) {
/* multimedia packets must be send with a HTTP POST */
      mxit_write_http_post(session,packet);
    }
    else {
      mxit_write_http_get(session,packet);
    }
  }
/* update the timestamp of the last-transmitted packet */
  session -> last_tx = mxit_now_milli();
/*
	 * we need to remember that we are still waiting for the ACK from
	 * the server on this request
	 */
  session -> outack = (packet -> cmd);
/* free up the packet resources */
  free_tx_packet(packet);
}
/*------------------------------------------------------------------------
 * TX Step 1: Create a new Tx packet and queue it for sending.
 *
 *  @param session		The MXit session object
 *  @param data			The packet data (payload)
 *  @param datalen		The length of the packet data
 *  @param cmd			The MXit command for this packet
 */

static void mxit_queue_packet(struct MXitSession *session,const char *data,int datalen,int cmd)
{
  struct tx_packet *packet;
  char header[256UL];
  int hlen;
/* create a packet for sending */
  packet = ((struct tx_packet *)(g_malloc0_n(1,(sizeof(struct tx_packet )))));
  packet -> data = (g_malloc0(datalen));
  packet -> cmd = cmd;
  packet -> headerlen = 0;
/* create generic packet header */
/* client mxitid */
  hlen = g_snprintf(header,(sizeof(header)),"id=%s%c",purple_account_get_username((session -> acc)),(((session -> http) != 0)?'\x26' : 0));
  if ((session -> http) != 0) {
/* http connection only */
    hlen += g_snprintf((header + hlen),(sizeof(header) - hlen),"s=");
    if ((session -> http_sesid) > 0) {
/* http session id */
      hlen += g_snprintf((header + hlen),(sizeof(header) - hlen),"%u%c",(session -> http_sesid),1);
    }
    session -> http_seqno++;
/* http request sequence id */
    hlen += g_snprintf((header + hlen),(sizeof(header) - hlen),"%u%c",(session -> http_seqno),(((session -> http) != 0)?'\x26' : 0));
  }
/* packet command */
  hlen += g_snprintf((header + hlen),(sizeof(header) - hlen),"cm=%i%c",cmd,(((session -> http) != 0)?'\x26' : 0));
  if (!((session -> http) != 0)) {
/* socket connection only */
/* packet length */
    packet -> headerlen = g_snprintf((packet -> header),(sizeof(packet -> header)),"ln=%i%c",(datalen + hlen),(((session -> http) != 0)?'\x26' : 0));
  }
/* copy the header to packet */
  memcpy(((packet -> header) + (packet -> headerlen)),header,hlen);
  packet -> headerlen += hlen;
/* copy payload to packet */
  if (datalen > 0) 
    memcpy((packet -> data),data,datalen);
  packet -> datalen = datalen;
/* shortcut */
  if ((session -> queue.count == 0) && ((session -> outack) == 0)) {
/* the queue is empty and there are no outstanding acks so we can write it directly */
    mxit_send_packet(session,packet);
  }
  else {
/* we need to queue this packet */
    if (((packet -> cmd) == 1000) || ((packet -> cmd) == 0x0011)) {
/* we do NOT queue HTTP poll nor socket ping packets */
      free_tx_packet(packet);
      return ;
    }
    purple_debug_info("prpl-loubserp-mxit","queueing packet for later sending cmd=%i\n",cmd);
    if (!(push_tx_packet(session,packet) != 0)) {
/* packet could not be queued for transmission */
      mxit_popup(PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Message Send Error"))),((const char *)(dgettext("pidgin","Unable to process your request at this time"))));
      free_tx_packet(packet);
    }
  }
}
/*------------------------------------------------------------------------
 * Manage the packet send queue (send next packet, timeout's, etc).
 *
 *  @param session		The MXit session object
 */

static void mxit_manage_queue(struct MXitSession *session)
{
  struct tx_packet *packet = (struct tx_packet *)((void *)0);
  gint64 now = mxit_now_milli();
  if (!(((session -> flags) & 1) != 0)) {
/* we are not connected, so ignore the queue */
    return ;
  }
  else if ((session -> outack) > 0) {
/* we are still waiting for an outstanding ACK from the MXit server */
    if ((session -> last_tx) <= (mxit_now_milli() - (30 * 1000))) {
/* ack timeout! so we close the connection here */
      purple_debug_info("prpl-loubserp-mxit","mxit_manage_queue: Timeout awaiting ACK for command \'%i\'\n",(session -> outack));
      purple_connection_error((session -> con),((const char *)(dgettext("pidgin","Timeout while waiting for a response from the MXit server."))));
    }
    return ;
  }
/*
	 * the mxit server has flood detection and it prevents you from sending messages to fast.
	 * this is a self defense mechanism, a very annoying feature. so the client must ensure that
	 * it does not send messages too fast otherwise mxit will ignore the user for 30 seconds.
	 * this is what we are trying to avoid here..
	 */
  if ((session -> q_fast_timer_id) == 0) {
/* the fast timer has not been set yet */
    if ((session -> last_tx) > (now - 100)) {
/* we need to wait a little before sending the next packet, so schedule a wakeup call */
      gint64 tdiff = (now - (session -> last_tx));
      guint delay = ((100 - tdiff) + 9);
      if (delay <= 0) 
        delay = 100;
      session -> q_fast_timer_id = purple_timeout_add(delay,mxit_manage_queue_fast,session);
    }
    else {
/* get the next packet from the queue to send */
      packet = pop_tx_packet(session);
      if (packet != ((struct tx_packet *)((void *)0))) {
/* there was a packet waiting to be sent to the server, now is the time to do something about it */
/* send the packet to MXit server */
        mxit_send_packet(session,packet);
      }
    }
  }
}
/*------------------------------------------------------------------------
 * Slow callback to manage the packet send queue.
 *
 *  @param session		The MXit session object
 */

gboolean mxit_manage_queue_slow(gpointer user_data)
{
  struct MXitSession *session = (struct MXitSession *)user_data;
  mxit_manage_queue(session);
/* continue running */
  return (!0);
}
/*------------------------------------------------------------------------
 * Fast callback to manage the packet send queue.
 *
 *  @param session		The MXit session object
 */

gboolean mxit_manage_queue_fast(gpointer user_data)
{
  struct MXitSession *session = (struct MXitSession *)user_data;
  session -> q_fast_timer_id = 0;
  mxit_manage_queue(session);
/* stop running */
  return 0;
}
/*------------------------------------------------------------------------
 * Callback to manage HTTP server polling (HTTP connections ONLY)
 *
 *  @param session		The MXit session object
 */

gboolean mxit_manage_polling(gpointer user_data)
{
  struct MXitSession *session = (struct MXitSession *)user_data;
  gboolean poll = 0;
  gint64 now = mxit_now_milli();
  gint64 rxdiff;
  if (!(((session -> flags) & 2) != 0)) {
/* we only poll if we are actually logged in */
    return (!0);
  }
/* calculate the time differences */
  rxdiff = (now - (session -> last_rx));
  if (rxdiff < 7) {
/* we received some reply a few moments ago, so reset the poll interval */
    session -> http_interval = 7;
  }
  else if ((session -> http_last_poll) < (now - (session -> http_interval))) {
/* time to poll again */
    poll = (!0);
/* back-off some more with the polling */
    session -> http_interval = ((session -> http_interval) + ((session -> http_interval) / 2));
    if ((session -> http_interval) > 10 * 60) 
      session -> http_interval = 10 * 60;
  }
/* debugging */
//purple_debug_info( MXIT_PLUGIN_ID, "POLL TIMER: %i (%i)\n", session->http_interval, rxdiff );
  if (poll != 0) {
/* send poll request */
    session -> http_last_poll = mxit_now_milli();
    mxit_send_poll(session);
  }
  return (!0);
}
/*========================================================================================================================
 * Send MXit operations.
 */
/*------------------------------------------------------------------------
 * Send a ping/keepalive packet to MXit server.
 *
 *  @param session		The MXit session object
 */

void mxit_send_ping(struct MXitSession *session)
{
/* queue packet for transmission */
  mxit_queue_packet(session,0,0,1000);
}
/*------------------------------------------------------------------------
 * Send a poll request to the HTTP server (HTTP connections ONLY).
 *
 *  @param session		The MXit session object
 */

void mxit_send_poll(struct MXitSession *session)
{
/* queue packet for transmission */
  mxit_queue_packet(session,0,0,0x0011);
}
/*------------------------------------------------------------------------
 * Send a logout packet to the MXit server.
 *
 *  @param session		The MXit session object
 */

void mxit_send_logout(struct MXitSession *session)
{
/* queue packet for transmission */
  mxit_queue_packet(session,0,0,2);
}
/*------------------------------------------------------------------------
 * Send a register packet to the MXit server.
 *
 *  @param session		The MXit session object
 */

void mxit_send_register(struct MXitSession *session)
{
  struct MXitProfile *profile = (session -> profile);
  const char *locale;
  char data[1 * 1000 * 1000];
  int datalen;
  char *clientVersion;
  unsigned int features = (2 | 32 | 256 | 0x002000 | 0x040000 | 0x100000 | 4096 | 8 | 0x004000 | 64 | 0x20000000);
  locale = purple_account_get_string((session -> acc),"locale","en");
/* Voice and Video supported */
  if ((mxit_audio_enabled() != 0) && (mxit_video_enabled() != 0)) 
    features |= (0x1000000 | 0x2000000);
  else if (mxit_audio_enabled() != 0) 
    features |= 0x1000000;
/* generate client version string (eg, P-2.7.10-Y-PURPLE) */
  clientVersion = g_strdup_printf("%c-%i.%i.%i-%s-%s",'P',2,10,9,"Y","PURPLE");
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=%s%c%s%c%i%c%s%c%s%c%i%c%s%c%s%c%s%c%i%c%s%c%s%c%i%c%i",(session -> encpwd),1,clientVersion,1,1 * 1000 * 1000 - 1000,1,(profile -> nickname),1,(profile -> birthday),1,(((profile -> male) != 0)?1 : 0),1,"planetpurple",1,"utf8=true;cid=LP",1,(session -> distcode),1,features,1,(session -> dialcode),1,locale,1,63,1,0);
/* "ms"=password\1version\1maxreplyLen\1name\1 */
/* dateOfBirth\1gender\1location\1capabilities\1 */
/* dc\1features\1dialingcode\1locale */
/* \1protocolVer\1lastRosterUpdate */
/* queue packet for transmission */
  mxit_queue_packet(session,data,datalen,11);
  g_free(clientVersion);
}
/*------------------------------------------------------------------------
 * Send a login packet to the MXit server.
 *
 *  @param session		The MXit session object
 */

void mxit_send_login(struct MXitSession *session)
{
  const char *splashId;
  const char *locale;
  char data[1 * 1000 * 1000];
  int datalen;
  char *clientVersion;
  unsigned int features = (2 | 32 | 256 | 0x002000 | 0x040000 | 0x100000 | 4096 | 8 | 0x004000 | 64 | 0x20000000);
  locale = purple_account_get_string((session -> acc),"locale","en");
/* Voice and Video supported */
  if ((mxit_audio_enabled() != 0) && (mxit_video_enabled() != 0)) 
    features |= (0x1000000 | 0x2000000);
  else if (mxit_audio_enabled() != 0) 
    features |= 0x1000000;
/* generate client version string (eg, P-2.7.10-Y-PURPLE) */
  clientVersion = g_strdup_printf("%c-%i.%i.%i-%s-%s",'P',2,10,9,"Y","PURPLE");
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=%s%c%s%c%i%c%s%c%s%c%i%c%s%c%s%c%i%c%i%c%i",(session -> encpwd),1,clientVersion,1,1,1,"utf8=true;cid=LP",1,(session -> distcode),1,features,1,(session -> dialcode),1,locale,1,1 * 1000 * 1000 - 1000,1,63,1,0);
/* "ms"=password\1version\1getContacts\1 */
/* capabilities\1dc\1features\1 */
/* dialingcode\1locale\1 */
/* maxReplyLen\1protocolVer\1lastRosterUpdate */
/* include "custom resource" information */
  splashId = splash_current(session);
  if (splashId != ((const char *)((void *)0))) 
    datalen += g_snprintf((data + datalen),(sizeof(data) - datalen),"%ccr=%s",(((session -> http) != 0)?'\x26' : 0),splashId);
/* queue packet for transmission */
  mxit_queue_packet(session,data,datalen,1);
  g_free(clientVersion);
}
/*------------------------------------------------------------------------
 * Send a chat message packet to the MXit server.
 *
 *  @param session		The MXit session object
 *  @param to			The username of the recipient
 *  @param msg			The message text
 */

void mxit_send_message(struct MXitSession *session,const char *to,const char *msg,gboolean parse_markup,gboolean is_command)
{
  char data[1 * 1000 * 1000];
  char *markuped_msg;
  int datalen;
  int msgtype = (is_command != 0)?7 : 1;
/* first we need to convert the markup from libPurple to MXit format */
  if (parse_markup != 0) 
    markuped_msg = mxit_convert_markup_tx(msg,&msgtype);
  else 
    markuped_msg = g_strdup(msg);
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=%s%c%s%c%i%c%i",to,1,markuped_msg,1,msgtype,1,0x0200 | 1024);
/* "ms"=jid\1msg\1type\1flags */
/* free the resources */
  g_free(markuped_msg);
/* queue packet for transmission */
  mxit_queue_packet(session,data,datalen,10);
}
/*------------------------------------------------------------------------
 * Send a extended profile request packet to the MXit server.
 *
 *  @param session		The MXit session object
 *  @param username		Username who's profile is being requested (NULL = our own)
 *  @param nr_attribs	Number of attributes being requested
 *  @param attribute	The names of the attributes
 */

void mxit_send_extprofile_request(struct MXitSession *session,const char *username,unsigned int nr_attrib,const char *(attribute)[])
{
  char data[1 * 1000 * 1000];
  int datalen;
  unsigned int i;
  datalen = g_snprintf(data,(sizeof(data)),"ms=%s%c%i",((username != 0)?username : ""),1,nr_attrib);
/* "ms="mxitid\1nr_attributes */
/* add attributes */
  for (i = 0; i < nr_attrib; i++) 
    datalen += g_snprintf((data + datalen),(sizeof(data) - datalen),"%c%s",1,attribute[i]);
/* queue packet for transmission */
  mxit_queue_packet(session,data,datalen,0x0039);
}
/*------------------------------------------------------------------------
 * Send an update profile packet to the MXit server.
 *
 *  @param session		The MXit session object
 *  @param password		The new password to be used for logging in (optional)
 *	@param nr_attrib	The number of attributes
 *	@param attributes	String containing the attribute-name, attribute-type and value (seperated by '\01')
 */

void mxit_send_extprofile_update(struct MXitSession *session,const char *password,unsigned int nr_attrib,const char *attributes)
{
  char data[1 * 1000 * 1000];
  gchar **parts = (gchar **)((void *)0);
  int datalen;
  unsigned int i;
  if (attributes != 0) 
    parts = g_strsplit(attributes,"\001",(1 + (nr_attrib * 3)));
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=%s%c%i",((password != 0)?password : ""),1,nr_attrib);
/* "ms"=password\1nr_attibutes  */
/* add attributes */
  for (i = 1; i < (nr_attrib * 3); i += 3) {
    if ((((parts == ((gchar **)((void *)0))) || (parts[i] == ((gchar *)((void *)0)))) || (parts[i + 1] == ((gchar *)((void *)0)))) || (parts[i + 2] == ((gchar *)((void *)0)))) {
      purple_debug_error("prpl-loubserp-mxit","Invalid profile update attributes = \'%s\' - nbr=%u\n",attributes,nr_attrib);
      g_strfreev(parts);
      return ;
    }
    datalen += g_snprintf((data + datalen),(sizeof(data) - datalen),"%c%s%c%s%c%s",1,parts[i],1,parts[i + 1],1,parts[i + 2]);
/* \1name\1type\1value  */
  }
/* queue packet for transmission */
  mxit_queue_packet(session,data,datalen,0x003A);
/* freeup the memory */
  g_strfreev(parts);
}
/*------------------------------------------------------------------------
 * Send packet to request list of suggested friends.
 *
 *  @param session		The MXit session object
 *  @param max			Maximum number of results to return
 *  @param nr_attribs	Number of attributes being requested
 *  @param attribute	The names of the attributes
 */

void mxit_send_suggest_friends(struct MXitSession *session,int max,unsigned int nr_attrib,const char *attribute[])
{
  char data[1 * 1000 * 1000];
  int datalen;
  unsigned int i;
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=%i%c%s%c%i%c%i%c%i",1,1,"",1,max,1,0,1,nr_attrib);
/* inputType \1 input \1 maxSuggestions \1 startIndex \1 numAttributes \1 name0 \1 name1 ... \1 nameN */
/* add attributes */
  for (i = 0; i < nr_attrib; i++) 
    datalen += g_snprintf((data + datalen),(sizeof(data) - datalen),"%c%s",1,attribute[i]);
/* queue packet for transmission */
  mxit_queue_packet(session,data,datalen,13);
}
/*------------------------------------------------------------------------
 * Send packet to perform a search for users.
 *
 *  @param session		The MXit session object
 *  @param max			Maximum number of results to return
 *  @param text			The search text
 *  @param nr_attribs	Number of attributes being requested
 *  @param attribute	The names of the attributes
 */

void mxit_send_suggest_search(struct MXitSession *session,int max,const char *text,unsigned int nr_attrib,const char *(attribute)[])
{
  char data[1 * 1000 * 1000];
  int datalen;
  unsigned int i;
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=%i%c%s%c%i%c%i%c%i",2,1,text,1,max,1,0,1,nr_attrib);
/* inputType \1 input \1 maxSuggestions \1 startIndex \1 numAttributes \1 name0 \1 name1 ... \1 nameN */
/* add attributes */
  for (i = 0; i < nr_attrib; i++) 
    datalen += g_snprintf((data + datalen),(sizeof(data) - datalen),"%c%s",1,attribute[i]);
/* queue packet for transmission */
  mxit_queue_packet(session,data,datalen,13);
}
/*------------------------------------------------------------------------
 * Send a presence update packet to the MXit server.
 *
 *  @param session		The MXit session object
 *  @param presence		The presence (as per MXit types)
 *  @param statusmsg	The status message (can be NULL)
 */

void mxit_send_presence(struct MXitSession *session,int presence,const char *statusmsg)
{
  char data[1 * 1000 * 1000];
  int datalen;
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=%i%c",presence,1);
/* "ms"=show\1status */
/* append status message (if one is set) */
  if (statusmsg != 0) 
    datalen += g_snprintf((data + datalen),(sizeof(data) - datalen),"%s",statusmsg);
/* queue packet for transmission */
  mxit_queue_packet(session,data,datalen,32);
}
/*------------------------------------------------------------------------
 * Send a mood update packet to the MXit server.
 *
 *  @param session		The MXit session object
 *  @param mood			The mood (as per MXit types)
 */

void mxit_send_mood(struct MXitSession *session,int mood)
{
  char data[1 * 1000 * 1000];
  int datalen;
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=%i",mood);
/* "ms"=mood */
/* queue packet for transmission */
  mxit_queue_packet(session,data,datalen,0x0029);
}
/*------------------------------------------------------------------------
 * Send an invite contact packet to the MXit server.
 *
 *  @param session		The MXit session object
 *  @param username		The username of the contact being invited
 *  @param mxitid		Indicates the username is a MXitId.
 *  @param alias		Our alias for the contact
 *  @param groupname	Group in which contact should be stored.
 *  @param message		Invite message
 */

void mxit_send_invite(struct MXitSession *session,const char *username,gboolean mxitid,const char *alias,const char *groupname,const char *message)
{
  char data[1 * 1000 * 1000];
  int datalen;
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=%s%c%s%c%s%c%i%c%s%c%i",groupname,1,username,1,alias,1,0,1,((message != 0)?message : ""),1,((mxitid != 0)?0 : 1));
/* "ms"=group \1 username \1 alias \1 type \1 msg \1 isuserid */
/* queue packet for transmission */
  mxit_queue_packet(session,data,datalen,6);
}
/*------------------------------------------------------------------------
 * Send a remove contact packet to the MXit server.
 *
 *  @param session		The MXit session object
 *  @param username		The username of the contact being removed
 */

void mxit_send_remove(struct MXitSession *session,const char *username)
{
  char data[1 * 1000 * 1000];
  int datalen;
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=%s",username);
/* "ms"=username */
/* queue packet for transmission */
  mxit_queue_packet(session,data,datalen,8);
}
/*------------------------------------------------------------------------
 * Send an accept subscription (invite) packet to the MXit server.
 *
 *  @param session		The MXit session object
 *  @param username		The username of the contact being accepted
 *  @param alias		Our alias for the contact
 */

void mxit_send_allow_sub(struct MXitSession *session,const char *username,const char *alias)
{
  char data[1 * 1000 * 1000];
  int datalen;
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=%s%c%s%c%s",username,1,"",1,alias);
/* "ms"=username\1group\1alias */
/* queue packet for transmission */
  mxit_queue_packet(session,data,datalen,0x0034);
}
/*------------------------------------------------------------------------
 * Send an deny subscription (invite) packet to the MXit server.
 *
 *  @param session		The MXit session object
 *  @param username		The username of the contact being denied
 *  @param reason		The message describing the reason for the rejection (can be NULL).
 */

void mxit_send_deny_sub(struct MXitSession *session,const char *username,const char *reason)
{
  char data[1 * 1000 * 1000];
  int datalen;
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=%s",username);
/* "ms"=username */
/* append reason (if one is set) */
  if (reason != 0) 
    datalen += g_snprintf((data + datalen),(sizeof(data) - datalen),"%c%s",1,reason);
/* queue packet for transmission */
  mxit_queue_packet(session,data,datalen,0x0037);
}
/*------------------------------------------------------------------------
 * Send an update contact packet to the MXit server.
 *
 *  @param session		The MXit session object
 *  @param username		The username of the contact being denied
 *  @param alias		Our alias for the contact
 *  @param groupname	Group in which contact should be stored.
 */

void mxit_send_update_contact(struct MXitSession *session,const char *username,const char *alias,const char *groupname)
{
  char data[1 * 1000 * 1000];
  int datalen;
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=%s%c%s%c%s",groupname,1,username,1,alias);
/* "ms"=groupname\1username\1alias */
/* queue packet for transmission */
  mxit_queue_packet(session,data,datalen,5);
}
/*------------------------------------------------------------------------
 * Send a splash-screen click event packet.
 *
 *  @param session		The MXit session object
 *  @param splashid		The identifier of the splash-screen
 */

void mxit_send_splashclick(struct MXitSession *session,const char *splashid)
{
  char data[1 * 1000 * 1000];
  int datalen;
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=%s",splashid);
/* "ms"=splashId */
/* queue packet for transmission */
  mxit_queue_packet(session,data,datalen,31);
}
/*------------------------------------------------------------------------
 * Send a message event packet.
 *
 *  @param session		The MXit session object
 *  @param to           The username of the original sender (ie, recipient of the event)
 *  @param id			The identifier of the event (received in message)
 *  @param event		Identified the type of event
 */

void mxit_send_msgevent(struct MXitSession *session,const char *to,const char *id,int event)
{
  char data[1 * 1000 * 1000];
  int datalen;
  purple_debug_info("prpl-loubserp-mxit","mxit_send_msgevent: to=%s id=%s event=%i\n",to,id,event);
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=%s%c%s%c%i",to,1,id,1,event);
/* "ms"=contactAddress \1 id \1 event */
/* queue packet for transmission */
  mxit_queue_packet(session,data,datalen,0x0023);
}
/*------------------------------------------------------------------------
 * Send packet to create a MultiMX room.
 *
 *  @param session		The MXit session object
 *  @param groupname	Name of the room to create
 *  @param nr_usernames	Number of users in initial invite
 *  @param usernames	The usernames of the users in the initial invite
 */

void mxit_send_groupchat_create(struct MXitSession *session,const char *groupname,int nr_usernames,const char *(usernames)[])
{
  char data[1 * 1000 * 1000];
  int datalen;
  int i;
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=%s%c%i",groupname,1,nr_usernames);
/* "ms"=roomname\1nr_jids\1jid0\1..\1jidN */
/* add usernames */
  for (i = 0; i < nr_usernames; i++) 
    datalen += g_snprintf((data + datalen),(sizeof(data) - datalen),"%c%s",1,usernames[i]);
/* queue packet for transmission */
  mxit_queue_packet(session,data,datalen,0x002C);
}
/*------------------------------------------------------------------------
 * Send packet to invite users to existing MultiMX room.
 *
 *  @param session		The MXit session object
 *  @param roomid		The unique RoomID for the MultiMx room.
 *  @param nr_usernames	Number of users being invited
 *  @param usernames	The usernames of the users being invited
 */

void mxit_send_groupchat_invite(struct MXitSession *session,const char *roomid,int nr_usernames,const char *(usernames)[])
{
  char data[1 * 1000 * 1000];
  int datalen;
  int i;
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=%s%c%i",roomid,1,nr_usernames);
/* "ms"=roomid\1nr_jids\1jid0\1..\1jidN */
/* add usernames */
  for (i = 0; i < nr_usernames; i++) 
    datalen += g_snprintf((data + datalen),(sizeof(data) - datalen),"%c%s",1,usernames[i]);
/* queue packet for transmission */
  mxit_queue_packet(session,data,datalen,0x002D);
}
/*------------------------------------------------------------------------
 * Send a "send file direct" multimedia packet.
 *
 *  @param session		The MXit session object
 *  @param username		The username of the recipient
 *  @param filename		The name of the file being sent
 *  @param buf			The content of the file
 *  @param buflen		The length of the file contents
 */

void mxit_send_file(struct MXitSession *session,const char *username,const char *filename,const unsigned char *buf,int buflen)
{
  char data[1 * 1000 * 1000];
  int datalen = 0;
  gchar *chunk;
  int size;
  purple_debug_info("prpl-loubserp-mxit","SENDING FILE \'%s\' of %i bytes to user \'%s\'\n",filename,buflen,username);
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=");
/* map chunk header over data buffer */
  chunk = (data + datalen);
  size = mxit_chunk_create_senddirect(chunk_data(chunk),username,filename,buf,buflen);
  if (size < 0) {
    purple_debug_error("prpl-loubserp-mxit","Error creating senddirect chunk (%i)\n",size);
    return ;
  }
  set_chunk_type(chunk,10);
  set_chunk_length(chunk,size);
  datalen += (5 + size);
/* send the byte stream to the mxit server */
  mxit_queue_packet(session,data,datalen,27);
}
/*------------------------------------------------------------------------
 * Send a "reject file" multimedia packet.
 *
 *  @param session		The MXit session object
 *  @param fileid		A unique ID that identifies this file
 */

void mxit_send_file_reject(struct MXitSession *session,const char *fileid)
{
  char data[1 * 1000 * 1000];
  int datalen = 0;
  gchar *chunk;
  int size;
  purple_debug_info("prpl-loubserp-mxit","mxit_send_file_reject\n");
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=");
/* map chunk header over data buffer */
  chunk = (data + datalen);
  size = mxit_chunk_create_reject(chunk_data(chunk),fileid);
  if (size < 0) {
    purple_debug_error("prpl-loubserp-mxit","Error creating reject chunk (%i)\n",size);
    return ;
  }
  set_chunk_type(chunk,7);
  set_chunk_length(chunk,size);
  datalen += (5 + size);
/* send the byte stream to the mxit server */
  mxit_queue_packet(session,data,datalen,27);
}
/*------------------------------------------------------------------------
 * Send a "get file" multimedia packet.
 *
 *  @param session		The MXit session object
 *  @param fileid		A unique ID that identifies this file
 *  @param filesize		The number of bytes to retrieve
 *  @param offset		Offset in file at which to start retrieving
 */

void mxit_send_file_accept(struct MXitSession *session,const char *fileid,int filesize,int offset)
{
  char data[1 * 1000 * 1000];
  int datalen = 0;
  gchar *chunk;
  int size;
  purple_debug_info("prpl-loubserp-mxit","mxit_send_file_accept\n");
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=");
/* map chunk header over data buffer */
  chunk = (data + datalen);
  size = mxit_chunk_create_get(chunk_data(chunk),fileid,filesize,offset);
  if (size < 0) {
    purple_debug_error("prpl-loubserp-mxit","Error creating getfile chunk (%i)\n",size);
    return ;
  }
  set_chunk_type(chunk,8);
  set_chunk_length(chunk,size);
  datalen += (5 + size);
/* send the byte stream to the mxit server */
  mxit_queue_packet(session,data,datalen,27);
}
/*------------------------------------------------------------------------
 * Send a "received file" multimedia packet.
 *
 *  @param session		The MXit session object
 *  @param status		The status of the file-transfer
 */

void mxit_send_file_received(struct MXitSession *session,const char *fileid,short status)
{
  char data[1 * 1000 * 1000];
  int datalen = 0;
  gchar *chunk;
  int size;
  purple_debug_info("prpl-loubserp-mxit","mxit_send_file_received\n");
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=");
/* map chunk header over data buffer */
  chunk = (data + datalen);
  size = mxit_chunk_create_received(chunk_data(chunk),fileid,status);
  if (size < 0) {
    purple_debug_error("prpl-loubserp-mxit","Error creating received chunk (%i)\n",size);
    return ;
  }
  set_chunk_type(chunk,9);
  set_chunk_length(chunk,size);
  datalen += (5 + size);
/* send the byte stream to the mxit server */
  mxit_queue_packet(session,data,datalen,27);
}
/*------------------------------------------------------------------------
 * Send a "set avatar" multimedia packet.
 *
 *  @param session		The MXit session object
 *  @param data			The avatar data
 *  @param buflen		The length of the avatar data
 */

void mxit_set_avatar(struct MXitSession *session,const unsigned char *avatar,int avatarlen)
{
  char data[1 * 1000 * 1000];
  int datalen = 0;
  gchar *chunk;
  int size;
  purple_debug_info("prpl-loubserp-mxit","mxit_set_avatar: %i bytes\n",avatarlen);
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=");
/* map chunk header over data buffer */
  chunk = (data + datalen);
  size = mxit_chunk_create_set_avatar(chunk_data(chunk),avatar,avatarlen);
  if (size < 0) {
    purple_debug_error("prpl-loubserp-mxit","Error creating set avatar chunk (%i)\n",size);
    return ;
  }
  set_chunk_type(chunk,13);
  set_chunk_length(chunk,size);
  datalen += (5 + size);
/* send the byte stream to the mxit server */
  mxit_queue_packet(session,data,datalen,27);
}
/*------------------------------------------------------------------------
 * Send a "get avatar" multimedia packet.
 *
 *  @param session		The MXit session object
 *  @param mxitId		The username who's avatar to request
 *  @param avatarId		The id of the avatar image (as string)
 *  @param data			The avatar data
 *  @param buflen		The length of the avatar data
 */

void mxit_get_avatar(struct MXitSession *session,const char *mxitId,const char *avatarId)
{
  char data[1 * 1000 * 1000];
  int datalen = 0;
  gchar *chunk;
  int size;
  purple_debug_info("prpl-loubserp-mxit","mxit_get_avatar: %s\n",mxitId);
/* convert the packet to a byte stream */
  datalen = g_snprintf(data,(sizeof(data)),"ms=");
/* map chunk header over data buffer */
  chunk = (data + datalen);
  size = mxit_chunk_create_get_avatar(chunk_data(chunk),mxitId,avatarId);
  if (size < 0) {
    purple_debug_error("prpl-loubserp-mxit","Error creating get avatar chunk (%i)\n",size);
    return ;
  }
  set_chunk_type(chunk,14);
  set_chunk_length(chunk,size);
  datalen += (5 + size);
/* send the byte stream to the mxit server */
  mxit_queue_packet(session,data,datalen,27);
}
/*------------------------------------------------------------------------
 * Process a login message packet.
 *
 *  @param session		The MXit session object
 *  @param records		The packet's data records
 *  @param rcount		The number of data records
 */

static void mxit_parse_cmd_login(struct MXitSession *session,struct record **records,int rcount)
{
  PurpleStatus *status;
  int presence;
  const char *statusmsg;
  const char *profilelist[] = {("birthdate"), ("gender"), ("fullname"), ("title"), ("firstname"), ("lastname"), ("email"), ("mobilenumber"), ("whereami"), ("aboutme"), ("relationship"), ("flags")};
  purple_account_set_int((session -> acc),"state",0);
/* we were not yet logged in so we need to complete the login sequence here */
  session -> flags |= 2;
  purple_connection_update_progress((session -> con),((const char *)(dgettext("pidgin","Successfully Logged In..."))),3,4);
  purple_connection_set_state((session -> con),PURPLE_CONNECTED);
/* save extra info if this is a HTTP connection */
  if ((session -> http) != 0) {
/* save the http server to use for this session */
    g_strlcpy((session -> http_server),( *( *records[1]).fields[3]).data,(sizeof(session -> http_server)));
/* save the session id */
    session -> http_sesid = (atoi(( *( *records[0]).fields[0]).data));
  }
/* extract UserId (from protocol 5.9) */
  if (( *records[1]).fcount >= 9) 
    session -> uid = g_strdup(( *( *records[1]).fields[8]).data);
/* extract VoIP server (from protocol 6.2) */
  if (( *records[1]).fcount >= 11) 
    g_strlcpy((session -> voip_server),( *( *records[1]).fields[10]).data,(sizeof(session -> voip_server)));
/* display the current splash-screen */
  if (splash_popup_enabled(session) != 0) 
    splash_display(session);
/* update presence status */
  status = purple_account_get_active_status((session -> acc));
  presence = mxit_convert_presence(purple_status_get_id(status));
  statusmsg = purple_status_get_attr_string(status,"message");
  if ((presence != 1) || (statusmsg != 0)) {
/* when logging into MXit, your default presence is online. but with the UI, one can change
		 * the presence to whatever. in the case where its changed to a different presence setting
		 * we need to send an update to the server, otherwise the user's presence will be out of
		 * sync between the UI and MXit.
		 */
    char *statusmsg1 = purple_markup_strip_html(statusmsg);
    char *statusmsg2 = g_strndup(statusmsg1,250);
    mxit_send_presence(session,presence,statusmsg2);
    g_free(statusmsg1);
    g_free(statusmsg2);
  }
/* retrieve our MXit profile */
  mxit_send_extprofile_request(session,0,(sizeof(profilelist) / sizeof(profilelist[0])),profilelist);
}
/*------------------------------------------------------------------------
 * Process a received message packet.
 *
 *  @param session		The MXit session object
 *  @param records		The packet's data records
 *  @param rcount		The number of data records
 */

static void mxit_parse_cmd_message(struct MXitSession *session,struct record **records,int rcount)
{
  struct RXMsgData *mx = (struct RXMsgData *)((void *)0);
  char *message = (char *)((void *)0);
  char *sender = (char *)((void *)0);
  int msglen = 0;
  int msgflags = 0;
  int msgtype = 0;
  if ((((rcount == 1) || (( *records[0]).fcount < 2)) || (( *records[1]).fcount == 0)) || (( *( *records[1]).fields[0]).len == 0)) {
/* packet contains no message or an empty message */
    return ;
  }
  message = ( *( *records[1]).fields[0]).data;
  msglen = (strlen(message));
/* strip off dummy domain */
  sender = ( *( *records[0]).fields[0]).data;
  mxit_strip_domain(sender);
#ifdef	DEBUG_PROTOCOL
  purple_debug_info("prpl-loubserp-mxit","Message received from \'%s\'\n",sender);
#endif
/* decode message flags (if any) */
  if (( *records[0]).fcount >= 5) 
    msgflags = atoi(( *( *records[0]).fields[4]).data);
  msgtype = atoi(( *( *records[0]).fields[2]).data);
  if ((msgflags & 16) != 0) {
/* this is a password encrypted message. we do not currently support those so ignore it */
    PurpleBuddy *buddy;
    const char *name;
    char msg[128UL];
    buddy = purple_find_buddy((session -> acc),sender);
    if (buddy != 0) 
      name = purple_buddy_get_alias(buddy);
    else 
      name = sender;
    g_snprintf(msg,(sizeof(msg)),((const char *)(dgettext("pidgin","%s sent you an encrypted message, but it is not supported on this client."))),name);
    mxit_popup(PURPLE_NOTIFY_MSG_WARNING,((const char *)(dgettext("pidgin","Message Error"))),msg);
    return ;
  }
  else if ((msgflags & 32) != 0) {
/* this is a transport-layer encrypted message. */
    message = mxit_decrypt_message(session,message);
    if (!(message != 0)) {
/* could not be decrypted */
      serv_got_im((session -> con),sender,((const char *)(dgettext("pidgin","An encrypted message was received which could not be decrypted."))),PURPLE_MESSAGE_ERROR,time(0));
      return ;
    }
  }
  if ((msgflags & 2) != 0) {
/* delivery notification is requested */
    if (( *records[0]).fcount >= 4) 
      mxit_send_msgevent(session,sender,( *( *records[0]).fields[3]).data,2);
  }
/* create and initialise new markup struct */
  mx = ((struct RXMsgData *)(g_malloc0_n(1,(sizeof(struct RXMsgData )))));
  mx -> msg = g_string_sized_new(msglen);
  mx -> session = session;
  mx -> from = g_strdup(sender);
  mx -> timestamp = (atoi(( *( *records[0]).fields[1]).data));
  mx -> got_img = 0;
  mx -> chatid = -1;
  mx -> img_count = 0;
/* update list of active chats */
  if (!(find_active_chat((session -> active_chats),(mx -> from)) != 0)) {
    session -> active_chats = g_list_append((session -> active_chats),(g_strdup((mx -> from))));
  }
  if (is_multimx_contact(session,(mx -> from)) != 0) {
/* this is a MultiMx chatroom message */
    multimx_message_received(mx,message,msglen,msgtype,msgflags);
  }
  else {
    mxit_parse_markup(mx,message,msglen,msgtype,msgflags);
  }
/* we are now done parsing the message */
  mx -> converted = (!0);
  if ((mx -> img_count) == 0) {
/* we have all the data we need for this message to be displayed now. */
    mxit_show_message(mx);
  }
  else {
/* this means there are still images outstanding for this message and
		 * still need to wait for them before we can display the message.
		 * so the image received callback function will eventually display
		 * the message. */
  }
/* cleanup */
  if ((msgflags & 32) != 0) 
    g_free(message);
}
/*------------------------------------------------------------------------
 * Process a received subscription request packet.
 *
 *  @param session		The MXit session object
 *  @param records		The packet's data records
 *  @param rcount		The number of data records
 */

static void mxit_parse_cmd_new_sub(struct MXitSession *session,struct record **records,int rcount)
{
  struct contact *contact;
  struct record *rec;
  int i;
  purple_debug_info("prpl-loubserp-mxit","mxit_parse_cmd_new_sub (%i recs)\n",rcount);
{
    for (i = 0; i < rcount; i++) {
      rec = records[i];
      if ((rec -> fcount) < 4) {
        purple_debug_error("prpl-loubserp-mxit","BAD SUBSCRIPTION RECORD! %i fields\n",(rec -> fcount));
        break; 
      }
/* build up a new contact info struct */
      contact = ((struct contact *)(g_malloc0_n(1,(sizeof(struct contact )))));
      g_strlcpy((contact -> username),( *(rec -> fields)[0]).data,(sizeof(contact -> username)));
/* remove dummy domain */
      mxit_strip_domain((contact -> username));
      g_strlcpy((contact -> alias),( *(rec -> fields)[1]).data,(sizeof(contact -> alias)));
      contact -> type = (atoi(( *(rec -> fields)[2]).data));
      if ((rec -> fcount) >= 5) {
/* there is a personal invite message attached */
        if ((( *(rec -> fields)[4]).data != 0) && (strlen(( *(rec -> fields)[4]).data) > 0)) 
          contact -> msg = strdup(( *(rec -> fields)[4]).data);
      }
/* handle the subscription */
/* subscription to a MultiMX room */
      if ((contact -> type) == 14) {
        char *creator = (char *)((void *)0);
        if ((rec -> fcount) >= 6) 
          creator = ( *(rec -> fields)[5]).data;
        multimx_invite(session,contact,creator);
      }
      else 
        mxit_new_subscription(session,contact);
    }
  }
}
/*------------------------------------------------------------------------
 * Parse the received presence value, and ensure that it is supported.
 *
 *  @param value		The received presence value.
 *  @return				A valid presence value.
 */

static short mxit_parse_presence(const char *value)
{
  short presence = (atoi(value));
/* ensure that the presence value is valid */
  switch(presence){
    case 0x00:
{
    }
    case 0x01:
{
    }
    case 0x02:
{
    }
    case 0x04:
{
      return presence;
    }
    default:
{
      return 1;
    }
  }
}
/*------------------------------------------------------------------------
 * Process a received contact update packet.
 *
 *  @param session		The MXit session object
 *  @param records		The packet's data records
 *  @param rcount		The number of data records
 */

static void mxit_parse_cmd_contact(struct MXitSession *session,struct record **records,int rcount)
{
  struct contact *contact = (struct contact *)((void *)0);
  struct record *rec;
  int i;
  purple_debug_info("prpl-loubserp-mxit","mxit_parse_cmd_contact (%i recs)\n",rcount);
{
    for (i = 0; i < rcount; i++) {
      rec = records[i];
      if ((rec -> fcount) < 6) {
        purple_debug_error("prpl-loubserp-mxit","BAD CONTACT RECORD! %i fields\n",(rec -> fcount));
        break; 
      }
/* build up a new contact info struct */
      contact = ((struct contact *)(g_malloc0_n(1,(sizeof(struct contact )))));
      g_strlcpy((contact -> groupname),( *(rec -> fields)[0]).data,(sizeof(contact -> groupname)));
      g_strlcpy((contact -> username),( *(rec -> fields)[1]).data,(sizeof(contact -> username)));
/* remove dummy domain */
      mxit_strip_domain((contact -> username));
      g_strlcpy((contact -> alias),( *(rec -> fields)[2]).data,(sizeof(contact -> alias)));
      contact -> presence = mxit_parse_presence(( *(rec -> fields)[3]).data);
      contact -> type = (atoi(( *(rec -> fields)[4]).data));
      contact -> mood = (atoi(( *(rec -> fields)[5]).data));
      if ((rec -> fcount) > 6) {
/* added in protocol 5.9 - flags & subtype */
        contact -> flags = atoi(( *(rec -> fields)[6]).data);
        contact -> subtype = ( *(rec -> fields)[7]).data[0];
      }
      if ((rec -> fcount) > 8) {
/* added in protocol 6.0 - reject message */
        contact -> msg = g_strdup(( *(rec -> fields)[8]).data);
      }
/* add the contact to the buddy list */
/* contact is a MultiMX room */
      if ((contact -> type) == 14) 
        multimx_created(session,contact);
      else 
        mxit_update_contact(session,contact);
    }
  }
  if (!(((session -> flags) & 4) != 0)) {
    session -> flags |= 4;
    mxit_update_blist(session);
  }
}
/*------------------------------------------------------------------------
 * Process a received presence update packet.
 *
 *  @param session		The MXit session object
 *  @param records		The packet's data records
 *  @param rcount		The number of data records
 */

static void mxit_parse_cmd_presence(struct MXitSession *session,struct record **records,int rcount)
{
  int i;
  purple_debug_info("prpl-loubserp-mxit","mxit_parse_cmd_presence (%i recs)\n",rcount);
{
    for (i = 0; i < rcount; i++) {
      struct record *rec = records[i];
      int flags = 0;
      if ((rec -> fcount) < 6) {
        purple_debug_error("prpl-loubserp-mxit","BAD PRESENCE RECORD! %i fields\n",(rec -> fcount));
        break; 
      }
/*
		 * The format of the record is:
		 * contactAddressN \1 presenceN \1 moodN \1 customMoodN \1 statusMsgN \1 avatarIdN [ \1 flagsN ]
		 */
/* contactAddress */
      mxit_strip_domain(( *(rec -> fields)[0]).data);
/* flags field is included */
      if ((rec -> fcount) >= 7) 
        flags = atoi(( *(rec -> fields)[6]).data);
      mxit_update_buddy_presence(session,( *(rec -> fields)[0]).data,mxit_parse_presence(( *(rec -> fields)[1]).data),(atoi(( *(rec -> fields)[2]).data)),( *(rec -> fields)[3]).data,( *(rec -> fields)[4]).data,flags);
      mxit_update_buddy_avatar(session,( *(rec -> fields)[0]).data,( *(rec -> fields)[5]).data);
    }
  }
}
/*------------------------------------------------------------------------
 * Process a received extended profile packet.
 *
 *  @param session		The MXit session object
 *  @param records		The packet's data records
 *  @param rcount		The number of data records
 */

static void mxit_parse_cmd_extprofile(struct MXitSession *session,struct record **records,int rcount)
{
  const char *mxitId = ( *( *records[0]).fields[0]).data;
  struct MXitProfile *profile = (struct MXitProfile *)((void *)0);
  int count;
  int i;
  const char *avatarId = (const char *)((void *)0);
  char *statusMsg = (char *)((void *)0);
  purple_debug_info("prpl-loubserp-mxit","mxit_parse_cmd_extprofile: profile for \'%s\'\n",mxitId);
  if ((( *( *records[0]).fields[0]).len == 0) || (((session -> uid) != 0) && (strcmp((session -> uid),( *( *records[0]).fields[0]).data) == 0))) {
/* No UserId or Our UserId provided, so this must be our own profile information */
    if ((session -> profile) == ((struct MXitProfile *)((void *)0))) 
      session -> profile = ((struct MXitProfile *)(g_malloc0_n(1,(sizeof(struct MXitProfile )))));
    profile = (session -> profile);
  }
  else {
/* is a buddy's profile */
    profile = ((struct MXitProfile *)(g_malloc0_n(1,(sizeof(struct MXitProfile )))));
  }
/* set the count for attributes */
  count = atoi(( *( *records[0]).fields[1]).data);
  for (i = 0; i < count; i++) {{
      char *fname;
      char *fvalue;
      char *fstatus;
      int f = ((i * 3) + 2);
/* field name */
      fname = ( *( *records[0]).fields[f]).data;
/* field value */
      fvalue = ( *( *records[0]).fields[f + 1]).data;
/* field status */
      fstatus = ( *( *records[0]).fields[f + 2]).data;
/* first check the status on the returned attribute */
      if (fstatus[0] != 48) {
/* error: attribute requested was NOT found */
        purple_debug_error("prpl-loubserp-mxit","Bad profile status on attribute \'%s\' \n",fname);
        continue; 
      }
      if (strcmp("birthdate",fname) == 0) {
/* birthdate */
        if (( *( *records[0]).fields[f + 1]).len > 10) {
          fvalue[10] = 0;
          ( *( *records[0]).fields[f + 1]).len = 10;
        }
        memcpy((profile -> birthday),fvalue,( *( *records[0]).fields[f + 1]).len);
      }
      else if (strcmp("gender",fname) == 0) {
/* gender */
        profile -> male = (fvalue[0] == '1');
      }
      else if (strcmp("fullname",fname) == 0) {
/* nickname */
        g_strlcpy((profile -> nickname),fvalue,(sizeof(profile -> nickname)));
      }
      else if (strcmp("statusmsg",fname) == 0) {
/* status message - just keep a reference to the value */
        statusMsg = g_markup_escape_text(fvalue,(-1));
      }
      else if (strcmp("avatarid",fname) == 0) {
/* avatar id - just keep a reference to the value */
        avatarId = fvalue;
      }
      else if (strcmp("title",fname) == 0) {
/* title */
        g_strlcpy((profile -> title),fvalue,(sizeof(profile -> title)));
      }
      else if (strcmp("firstname",fname) == 0) {
/* first name */
        g_strlcpy((profile -> firstname),fvalue,(sizeof(profile -> firstname)));
      }
      else if (strcmp("lastname",fname) == 0) {
/* last name */
        g_strlcpy((profile -> lastname),fvalue,(sizeof(profile -> lastname)));
      }
      else if (strcmp("email",fname) == 0) {
/* email address */
        g_strlcpy((profile -> email),fvalue,(sizeof(profile -> email)));
      }
      else if (strcmp("mobilenumber",fname) == 0) {
/* mobile number */
        g_strlcpy((profile -> mobilenr),fvalue,(sizeof(profile -> mobilenr)));
      }
      else if (strcmp("registeredcountry",fname) == 0) {
/* registered country */
        g_strlcpy((profile -> regcountry),fvalue,(sizeof(profile -> regcountry)));
      }
      else if (strcmp("flags",fname) == 0) {
/* profile flags */
        profile -> flags = g_ascii_strtoll(fvalue,0,10);
      }
      else if (strcmp("lastseen",fname) == 0) {
/* last seen online */
        profile -> lastonline = g_ascii_strtoll(fvalue,0,10);
      }
      else if (strcmp("whereami",fname) == 0) {
/* where am I */
        g_strlcpy((profile -> whereami),fvalue,(sizeof(profile -> whereami)));
      }
      else if (strcmp("aboutme",fname) == 0) {
/* about me */
        g_strlcpy((profile -> aboutme),fvalue,(sizeof(profile -> aboutme)));
      }
      else if (strcmp("relationship",fname) == 0) {
/* relatinship status */
        profile -> relationship = (strtol(fvalue,0,10));
      }
      else {
/* invalid profile attribute */
        purple_debug_error("prpl-loubserp-mxit","Invalid profile attribute received \'%s\' \n",fname);
      }
    }
  }
  if (profile != (session -> profile)) {
/* not our own profile */
    struct contact *contact = (struct contact *)((void *)0);
    contact = get_mxit_invite_contact(session,mxitId);
    if (contact != 0) {
/* this is an invite, so update its profile info */
      if ((statusMsg != 0) && (strlen(statusMsg) > 0)) {
/* update the status message */
        if ((contact -> statusMsg) != 0) 
          g_free((contact -> statusMsg));
        contact -> statusMsg = strdup(statusMsg);
      }
      else 
        contact -> statusMsg = ((char *)((void *)0));
      if ((contact -> profile) != 0) 
        g_free((contact -> profile));
      contact -> profile = profile;
      if ((avatarId != 0) && (strlen(avatarId) > 0)) {
/* avatar must be requested for this invite before we can display it */
        mxit_get_avatar(session,mxitId,avatarId);
        if ((contact -> avatarId) != 0) 
          g_free((contact -> avatarId));
        contact -> avatarId = strdup(avatarId);
      }
      else {
/* display what we have */
        contact -> avatarId = ((char *)((void *)0));
        mxit_show_profile(session,mxitId,profile);
      }
    }
    else {
/* this is a contact */
      if (avatarId != 0) 
        mxit_update_buddy_avatar(session,mxitId,avatarId);
      if ((statusMsg != 0) && (strlen(statusMsg) > 0)) {
/* update the status message */
        PurpleBuddy *buddy = (PurpleBuddy *)((void *)0);
        buddy = purple_find_buddy((session -> acc),mxitId);
        if (buddy != 0) {
          contact = (purple_buddy_get_protocol_data(buddy));
          if (contact != 0) {
            if ((contact -> statusMsg) != 0) 
              g_free((contact -> statusMsg));
            contact -> statusMsg = strdup(statusMsg);
          }
        }
      }
/* show the profile */
      mxit_show_profile(session,mxitId,profile);
      g_free(profile);
    }
  }
  g_free(statusMsg);
}
/*------------------------------------------------------------------------
 * Process a received suggest-contacts packet.
 *
 *  @param session		The MXit session object
 *  @param records		The packet's data records
 *  @param rcount		The number of data records
 */

static void mxit_parse_cmd_suggestcontacts(struct MXitSession *session,struct record **records,int rcount)
{
  GList *entries = (GList *)((void *)0);
  int searchType;
  int maxResults;
  int count;
  int i;
/*
	 * searchType \1 numSuggestions \1 total \1 numAttributes \1 name0 \1 name1 \1 ... \1 nameN \0
	 * userid \1 contactType \1 value0 \1 value1 ... valueN \0
	 * ...
	 * userid \1 contactType \1 value0 \1 value1 ... valueN
	 */
/* the type of results */
  searchType = atoi(( *( *records[0]).fields[0]).data);
/* the maximum number of results */
  maxResults = atoi(( *( *records[0]).fields[2]).data);
/* set the count for attributes */
  count = atoi(( *( *records[0]).fields[3]).data);
  for (i = 1; i < rcount; i++) {
    struct record *rec = records[i];
    struct MXitProfile *profile = (struct MXitProfile *)(g_malloc0_n(1,(sizeof(struct MXitProfile ))));
    int j;
    g_strlcpy((profile -> userid),( *(rec -> fields)[0]).data,(sizeof(profile -> userid)));
// TODO: ContactType - User or Service
    for (j = 0; j < count; j++) {
      char *fname;
      char *fvalue = "";
/* field name */
      fname = ( *( *records[0]).fields[4 + j]).data;
      if (( *records[i]).fcount > (2 + j)) 
/* field value */
        fvalue = ( *( *records[i]).fields[2 + j]).data;
      purple_debug_info("prpl-loubserp-mxit"," %s: field=\'%s\' value=\'%s\'\n",(profile -> userid),fname,fvalue);
      if (strcmp("birthdate",fname) == 0) {
/* birthdate */
        g_strlcpy((profile -> birthday),fvalue,(sizeof(profile -> birthday)));
      }
      else if (strcmp("firstname",fname) == 0) {
/* first name */
        g_strlcpy((profile -> firstname),fvalue,(sizeof(profile -> firstname)));
      }
      else if (strcmp("lastname",fname) == 0) {
/* last name */
        g_strlcpy((profile -> lastname),fvalue,(sizeof(profile -> lastname)));
      }
      else if (strcmp("gender",fname) == 0) {
/* gender */
        profile -> male = (fvalue[0] == '1');
      }
      else if (strcmp("fullname",fname) == 0) {
/* nickname */
        g_strlcpy((profile -> nickname),fvalue,(sizeof(profile -> nickname)));
      }
      else if (strcmp("whereami",fname) == 0) {
/* where am I */
        g_strlcpy((profile -> whereami),fvalue,(sizeof(profile -> whereami)));
      }
/* ignore other attibutes */
    }
    entries = g_list_append(entries,profile);
  }
/* display */
  mxit_show_search_results(session,searchType,maxResults,entries);
/* cleanup */
  g_list_foreach(entries,((GFunc )g_free),0);
}
/*------------------------------------------------------------------------
 * Process a received message event packet.
 *
 *  @param session		The MXit session object
 *  @param records		The packet's data records
 *  @param rcount		The number of data records
 */

static void mxit_parse_cmd_msgevent(struct MXitSession *session,struct record **records,int rcount)
{
  int event;
/*
	 * contactAddress \1 dateTime \1 id \1 event
	 */
/* strip off dummy domain */
  mxit_strip_domain(( *( *records[0]).fields[0]).data);
  event = atoi(( *( *records[0]).fields[3]).data);
  switch(event){
/* user is typing */
    case 0x10:
{
    }
/* user is typing angrily */
    case 0x40:
{
      serv_got_typing((session -> con),( *( *records[0]).fields[0]).data,0,PURPLE_TYPING);
      break; 
    }
/* user has stopped typing */
    case 0x20:
{
      serv_got_typing_stopped((session -> con),( *( *records[0]).fields[0]).data);
      break; 
    }
/* user is erasing text */
/* message was delivered */
    case 0x02:
{
    }
/* message was viewed */
    case 0x04:
{
    }
    case 0x80:
{
/* these are currently not supported by libPurple */
      break; 
    }
    default:
{
      purple_debug_error("prpl-loubserp-mxit","Unknown message event received (%i)\n",event);
    }
  }
}
/*------------------------------------------------------------------------
 * Return the length of a multimedia chunk
 *
 * @return		The actual chunk data length in bytes
 */

static int get_chunk_len(const char *chunkdata)
{
  int *sizeptr;
/* we skip the first byte (type field) */
  sizeptr = ((int *)(chunkdata + 1));
  return (ntohl(( *sizeptr)));
}
/*------------------------------------------------------------------------
 * Process a received multimedia packet.
 *
 *  @param session		The MXit session object
 *  @param records		The packet's data records
 *  @param rcount		The number of data records
 */

static void mxit_parse_cmd_media(struct MXitSession *session,struct record **records,int rcount)
{
  char type;
  int size;
  type = ( *( *records[0]).fields[0]).data[0];
  size = get_chunk_len(( *( *records[0]).fields[0]).data);
  purple_debug_info("prpl-loubserp-mxit","mxit_parse_cmd_media (%i records) (%i bytes)\n",rcount,size);
/* supported chunked data types */
  switch(type){
/* custom resource */
    case 0x01:
{
{
        struct cr_chunk chunk;
/* decode the chunked data */
        memset((&chunk),0,(sizeof(struct cr_chunk )));
        mxit_chunk_parse_cr((( *( *records[0]).fields[0]).data + (sizeof(char ) + sizeof(int ))),( *( *records[0]).fields[0]).len,&chunk);
        purple_debug_info("prpl-loubserp-mxit","chunk info id=%s handle=%s op=%i\n",chunk.id,chunk.handle,chunk.operation);
/* this is a splash-screen operation */
        if (strcmp(chunk.handle,"plas2.png") == 0) {
/* update the splash-screen */
          if (chunk.operation == 0) {
// TODO: Fix - assuming 1st resource is splash
            struct splash_chunk *splash = ( *chunk.resources).data;
// TODO: Fix - if 2 resources, then is clickable
            gboolean clickable = (g_list_length(chunk.resources) > 1);
            if (splash != ((struct splash_chunk *)((void *)0))) 
              splash_update(session,chunk.id,(splash -> data),(splash -> datalen),clickable);
          }
          else 
/* remove the splash-screen */
if (chunk.operation == 1) 
            splash_remove(session);
        }
/* cleanup custom resources */
        g_list_foreach(chunk.resources,((GFunc )g_free),0);
      }
      break; 
    }
/* file offer */
    case 0x06:
{
{
        struct offerfile_chunk chunk;
/* decode the chunked data */
        memset((&chunk),0,(sizeof(struct offerfile_chunk )));
        mxit_chunk_parse_offer((( *( *records[0]).fields[0]).data + (sizeof(char ) + sizeof(int ))),( *( *records[0]).fields[0]).len,&chunk);
/* process the offer */
        mxit_xfer_rx_offer(session,chunk.username,chunk.filename,chunk.filesize,chunk.fileid);
      }
      break; 
    }
/* get file response */
    case 0x08:
{
{
        struct getfile_chunk chunk;
/* decode the chunked data */
        memset((&chunk),0,(sizeof(struct getfile_chunk )));
        mxit_chunk_parse_get((( *( *records[0]).fields[0]).data + (sizeof(char ) + sizeof(int ))),( *( *records[0]).fields[0]).len,&chunk);
/* process the getfile */
        mxit_xfer_rx_file(session,chunk.fileid,chunk.data,chunk.length);
      }
      break; 
    }
/* get avatars */
    case 0x0E:
{
{
        struct getavatar_chunk chunk;
        struct contact *contact = (struct contact *)((void *)0);
/* decode the chunked data */
        memset((&chunk),0,(sizeof(struct getavatar_chunk )));
        mxit_chunk_parse_get_avatar((( *( *records[0]).fields[0]).data + (sizeof(char ) + sizeof(int ))),( *( *records[0]).fields[0]).len,&chunk);
/* update avatar image */
        if (chunk.data != 0) {
          purple_debug_info("prpl-loubserp-mxit","updating avatar for contact \'%s\'\n",chunk.mxitid);
          contact = get_mxit_invite_contact(session,chunk.mxitid);
          if (contact != 0) {
/* this is an invite (add image to the internal image store) */
            contact -> imgid = purple_imgstore_add_with_id(g_memdup(chunk.data,chunk.length),chunk.length,0);
/* show the profile */
            mxit_show_profile(session,chunk.mxitid,(contact -> profile));
          }
          else {
/* this is a contact's avatar, so update it */
            purple_buddy_icons_set_for_user((session -> acc),chunk.mxitid,g_memdup(chunk.data,chunk.length),chunk.length,chunk.avatarid);
          }
        }
      }
      break; 
    }
    case 0x0D:
{
/* this is a reply packet to a set avatar request. no action is required */
      break; 
    }
    case 0x0A:
{
/* this is a ack for a file send. */
{
        struct sendfile_chunk chunk;
        memset((&chunk),0,(sizeof(struct sendfile_chunk )));
        mxit_chunk_parse_sendfile((( *( *records[0]).fields[0]).data + (sizeof(char ) + sizeof(int ))),( *( *records[0]).fields[0]).len,&chunk);
        purple_debug_info("prpl-loubserp-mxit","file-send send to \'%s\' [status=%i message=\'%s\']\n",chunk.username,chunk.status,chunk.statusmsg);
/* not success */
        if (chunk.status != 0) 
          mxit_popup(PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","File Send Failed"))),chunk.statusmsg);
      }
      break; 
    }
    case 0x09:
{
/* this is a ack for a file received. no action is required */
      break; 
    }
    default:
{
      purple_debug_error("prpl-loubserp-mxit","Unsupported chunked data packet type received (%i)\n",type);
      break; 
    }
  }
}
/*------------------------------------------------------------------------
 * Handle a redirect sent from the MXit server.
 *
 *  @param session		The MXit session object
 *  @param url			The redirect information
 */

static void mxit_perform_redirect(struct MXitSession *session,const char *url)
{
  gchar **parts;
  gchar **host;
  int type;
  purple_debug_info("prpl-loubserp-mxit","mxit_perform_redirect: %s\n",url);
/* tokenize the URL string */
  parts = g_strsplit(url,";",0);
/* Part 1: protocol://host:port */
  host = g_strsplit(parts[0],":",4);
  if (strcmp(host[0],"socket") == 0) {
/* redirect to a MXit socket proxy */
    g_strlcpy((session -> server),(host[1] + 2),(sizeof(session -> server)));
    session -> port = atoi(host[2]);
  }
  else {
    purple_connection_error((session -> con),((const char *)(dgettext("pidgin","Cannot perform redirect using the specified protocol"))));
    goto redirect_fail;
  }
/* Part 2: type of redirect */
  type = atoi(parts[1]);
  if (type == 1) {
/* permanent redirect, so save new MXit server and port */
    purple_account_set_string((session -> acc),"server",(session -> server));
    purple_account_set_int((session -> acc),"port",(session -> port));
  }
/* Part 3: message (optional) */
  if (parts[2] != ((gchar *)((void *)0))) 
    purple_connection_notice((session -> con),parts[2]);
  purple_debug_info("prpl-loubserp-mxit","mxit_perform_redirect: %s redirect to %s:%i\n",((type == 1)?"Permanent" : "Temporary"),(session -> server),(session -> port));
/* perform the re-connect to the new MXit server */
  mxit_reconnect(session);
  redirect_fail:
  g_strfreev(parts);
  g_strfreev(host);
}
/*------------------------------------------------------------------------
 * Process a success response received from the MXit server.
 *
 *  @param session		The MXit session object
 *  @param packet		The received packet
 */

static int process_success_response(struct MXitSession *session,struct rx_packet *packet)
{
/* ignore ping/poll packets */
  if (((packet -> cmd) != 1000) && ((packet -> cmd) != 0x0011)) 
    session -> last_rx = mxit_now_milli();
/*
	 * when we pass the packet records to the next level for parsing
	 * we minus 3 records because 1) the first record is the packet
	 * type 2) packet reply status 3) the last record is bogus
	 */
/* packet command */
  switch(packet -> cmd){
/* fall through, when registeration successful, MXit will auto login */
    case 0x0001:
{
    }
    case 0x000B:
{
/* login response */
      if (!(((session -> flags) & 2) != 0)) {
        mxit_parse_cmd_login(session,((packet -> records) + 2),((packet -> rcount) - 3));
      }
      break; 
    }
    case 0x0002:
{
/* logout response */
      session -> flags &= ~2;
      purple_account_disconnect((session -> acc));
/* note:
				 * we do not prompt the user here for a reconnect, because this could be the user
				 * logging in with his phone. so we just disconnect the account otherwise
				 * mxit will start to bounce between the phone and pidgin. also could be a valid
				 * disconnect selected by the user.
				 */
      return -1;
    }
    case 0x0003:
{
/* contact update */
      mxit_parse_cmd_contact(session,((packet -> records) + 2),((packet -> rcount) - 3));
      break; 
    }
    case 0x0007:
{
/* presence update */
      mxit_parse_cmd_presence(session,((packet -> records) + 2),((packet -> rcount) - 3));
      break; 
    }
    case 0x0009:
{
/* incoming message (no bogus record) */
      mxit_parse_cmd_message(session,((packet -> records) + 2),((packet -> rcount) - 2));
      break; 
    }
    case 0x0033:
{
/* new subscription request */
      mxit_parse_cmd_new_sub(session,((packet -> records) + 2),((packet -> rcount) - 3));
      break; 
    }
    case 0x001B:
{
/* multi-media message */
      mxit_parse_cmd_media(session,((packet -> records) + 2),((packet -> rcount) - 2));
      break; 
    }
    case 0x0039:
{
/* profile update */
      mxit_parse_cmd_extprofile(session,((packet -> records) + 2),((packet -> rcount) - 2));
      break; 
    }
    case 0x000D:
{
/* suggest contacts */
      mxit_parse_cmd_suggestcontacts(session,((packet -> records) + 2),((packet -> rcount) - 2));
      break; 
    }
    case 0x0024:
{
/* received message event */
      mxit_parse_cmd_msgevent(session,((packet -> records) + 2),((packet -> rcount) - 2));
      break; 
    }
/* mood update */
    case 0x0005:
{
    }
/* update contact information */
/* allow subscription ack */
/* deny subscription ack */
    case 0x0006:
{
    }
/* invite contact ack */
    case 0x0008:
{
    }
/* remove contact ack */
    case 0x000A:
{
    }
/* outgoing message ack */
/* presence update ack */
/* create groupchat */
/* groupchat invite */
/* ping reply */
    case 0x0011:
{
    }
/* HTTP poll reply */
/* profile update */
// TODO: Protocol 6.2 indicates status for each attribute, and current value.
    case 0x001F:
{
    }
    case 0x0020:
{
    }
/* splash-screen clickthrough */
    case 0x0023:
{
    }
    case 0x0029:
{
    }
    case 0x002C:
{
    }
    case 0x002D:
{
    }
    case 0x0034:
{
    }
    case 0x0037:
{
    }
    case 0x003A:
{
    }
    case 0x03E8:
{
/* event message */
      break; 
    }
    default:
{
/* unknown packet */
      purple_debug_error("prpl-loubserp-mxit","Received unknown client packet (cmd = %i)\n",(packet -> cmd));
    }
  }
  return 0;
}
/*------------------------------------------------------------------------
 * Process an error response received from the MXit server.
 *
 *  @param session		The MXit session object
 *  @param packet		The received packet
 */

static int process_error_response(struct MXitSession *session,struct rx_packet *packet)
{
  char errmsg[256UL];
  const char *errdesc;
/* set the error description to be shown to the user */
  if ((packet -> errmsg) != 0) 
    errdesc = (packet -> errmsg);
  else 
    errdesc = ((const char *)(dgettext("pidgin","An internal MXit server error occurred.")));
  purple_debug_info("prpl-loubserp-mxit","Error Reply %i:%s\n",(packet -> errcode),errdesc);
  if ((packet -> errcode) == 42) {
/* we are not currently logged in, so we need to reconnect */
    purple_connection_error((session -> con),((const char *)(dgettext("pidgin",errdesc))));
  }
/* packet command */
  switch(packet -> cmd){
    case 0x0001:
{
    }
    case 0x000B:
{
      if ((packet -> errcode) == 16) {
        mxit_perform_redirect(session,(packet -> errmsg));
        return 0;
      }
      else {
        g_snprintf(errmsg,(sizeof(errmsg)),((const char *)(dgettext("pidgin","Login error: %s (%i)"))),errdesc,(packet -> errcode));
        purple_connection_error((session -> con),errmsg);
        return -1;
      }
    }
    case 0x0002:
{
      g_snprintf(errmsg,(sizeof(errmsg)),((const char *)(dgettext("pidgin","Logout error: %s (%i)"))),errdesc,(packet -> errcode));
      purple_connection_error_reason((session -> con),PURPLE_CONNECTION_ERROR_NAME_IN_USE,((const char *)(dgettext("pidgin",errmsg))));
      return -1;
    }
    case 0x0003:
{
      mxit_popup(PURPLE_NOTIFY_MSG_WARNING,((const char *)(dgettext("pidgin","Contact Error"))),((const char *)(dgettext("pidgin",errdesc))));
      break; 
    }
    case 0x0009:
{
      mxit_popup(PURPLE_NOTIFY_MSG_WARNING,((const char *)(dgettext("pidgin","Message Error"))),((const char *)(dgettext("pidgin",errdesc))));
      break; 
    }
    case 0x000A:
{
      mxit_popup(PURPLE_NOTIFY_MSG_WARNING,((const char *)(dgettext("pidgin","Message Sending Error"))),((const char *)(dgettext("pidgin",errdesc))));
      break; 
    }
    case 0x0020:
{
      mxit_popup(PURPLE_NOTIFY_MSG_WARNING,((const char *)(dgettext("pidgin","Status Error"))),((const char *)(dgettext("pidgin",errdesc))));
      break; 
    }
    case 0x0029:
{
      mxit_popup(PURPLE_NOTIFY_MSG_WARNING,((const char *)(dgettext("pidgin","Mood Error"))),((const char *)(dgettext("pidgin",errdesc))));
      break; 
    }
    case 0x002B:
{
/*
				 * the MXit server sends this packet if we were idle for too long.
				 * to stop the server from closing this connection we need to resend
				 * the login packet.
				 */
      mxit_send_login(session);
      break; 
    }
    case 0x0006:
{
      mxit_popup(PURPLE_NOTIFY_MSG_WARNING,((const char *)(dgettext("pidgin","Invitation Error"))),((const char *)(dgettext("pidgin",errdesc))));
      break; 
    }
    case 0x0008:
{
      mxit_popup(PURPLE_NOTIFY_MSG_WARNING,((const char *)(dgettext("pidgin","Contact Removal Error"))),((const char *)(dgettext("pidgin",errdesc))));
      break; 
    }
    case 0x0034:
{
    }
    case 0x0037:
{
      mxit_popup(PURPLE_NOTIFY_MSG_WARNING,((const char *)(dgettext("pidgin","Subscription Error"))),((const char *)(dgettext("pidgin",errdesc))));
      break; 
    }
    case 0x0005:
{
      mxit_popup(PURPLE_NOTIFY_MSG_WARNING,((const char *)(dgettext("pidgin","Contact Update Error"))),((const char *)(dgettext("pidgin",errdesc))));
      break; 
    }
    case 0x001B:
{
      mxit_popup(PURPLE_NOTIFY_MSG_WARNING,((const char *)(dgettext("pidgin","File Transfer Error"))),((const char *)(dgettext("pidgin",errdesc))));
      break; 
    }
    case 0x002C:
{
      mxit_popup(PURPLE_NOTIFY_MSG_WARNING,((const char *)(dgettext("pidgin","Cannot create MultiMx room"))),((const char *)(dgettext("pidgin",errdesc))));
      break; 
    }
    case 0x002D:
{
      mxit_popup(PURPLE_NOTIFY_MSG_WARNING,((const char *)(dgettext("pidgin","MultiMx Invitation Error"))),((const char *)(dgettext("pidgin",errdesc))));
      break; 
    }
    case 0x0039:
{
    }
    case 0x003A:
{
      mxit_popup(PURPLE_NOTIFY_MSG_WARNING,((const char *)(dgettext("pidgin","Profile Error"))),((const char *)(dgettext("pidgin",errdesc))));
      break; 
    }
    case 0x001F:
{
    }
    case 0x0023:
{
/* ignore error */
      break; 
    }
    case 0x0011:
{
    }
    case 0x03E8:
{
      break; 
    }
    default:
{
      mxit_popup(PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Error"))),((const char *)(dgettext("pidgin",errdesc))));
      break; 
    }
  }
  return 0;
}
/*========================================================================================================================
 * Low-level Packet receive
 */
#ifdef	DEBUG_PROTOCOL
/*------------------------------------------------------------------------
 * Dump a received packet structure.
 *
 *  @param p			The received packet
 */

static void dump_packet(struct rx_packet *p)
{
  struct record *r = (struct record *)((void *)0);
  struct field *f = (struct field *)((void *)0);
  int i;
  int j;
  purple_debug_info("prpl-loubserp-mxit","PACKET DUMP: (%i records)\n",(p -> rcount));
  for (i = 0; i < (p -> rcount); i++) {
    r = (p -> records)[i];
    purple_debug_info("prpl-loubserp-mxit","RECORD: (%i fields)\n",(r -> fcount));
    for (j = 0; j < (r -> fcount); j++) {
      f = (r -> fields)[j];
      purple_debug_info("prpl-loubserp-mxit","\tFIELD: (len=%i) \'%s\' \n",(f -> len),(f -> data));
    }
  }
}
#endif
/*------------------------------------------------------------------------
 * Free up memory used by a packet structure.
 *
 *  @param p			The received packet
 */

static void free_rx_packet(struct rx_packet *p)
{
  struct record *r = (struct record *)((void *)0);
  struct field *f = (struct field *)((void *)0);
  int i;
  int j;
  for (i = 0; i < (p -> rcount); i++) {
    r = (p -> records)[i];
    for (j = 0; j < (r -> fcount); j++) {
      g_free(f);
    }
    g_free((r -> fields));
    g_free(r);
  }
  g_free((p -> records));
}
/*------------------------------------------------------------------------
 * Add a new field to a record.
 *
 *  @param r			Parent record object
 *  @return				The newly created field
 */

static struct field *add_field(struct record *r)
{
  struct field *field;
  field = ((struct field *)(g_malloc0_n(1,(sizeof(struct field )))));
  r -> fields = (g_realloc((r -> fields),(sizeof(struct field *) * ((r -> fcount) + 1))));
  (r -> fields)[r -> fcount] = field;
  r -> fcount++;
  return field;
}
/*------------------------------------------------------------------------
 * Add a new record to a packet.
 *
 *  @param p			The packet object
 *  @return				The newly created record
 */

static struct record *add_record(struct rx_packet *p)
{
  struct record *rec;
  rec = ((struct record *)(g_malloc0_n(1,(sizeof(struct record )))));
  p -> records = (g_realloc((p -> records),(sizeof(struct record *) * ((p -> rcount) + 1))));
  (p -> records)[p -> rcount] = rec;
  p -> rcount++;
  return rec;
}
/*------------------------------------------------------------------------
 * Parse the received byte stream into a proper client protocol packet.
 *
 *  @param session		The MXit session object
 *  @return				Success (0) or Failure (!0)
 */

int mxit_parse_packet(struct MXitSession *session)
{
  struct rx_packet packet;
  struct record *rec;
  struct field *field;
  gboolean pbreak;
  unsigned int i;
  int res = 0;
#ifdef	DEBUG_PROTOCOL
  purple_debug_info("prpl-loubserp-mxit","Received packet (%i bytes)\n",(session -> rx_i));
  dump_bytes(session,(session -> rx_dbuf),(session -> rx_i));
#endif
  i = 0;
  while(i < (session -> rx_i)){
/* create first record and field */
    rec = ((struct record *)((void *)0));
    field = ((struct field *)((void *)0));
    memset((&packet),0,(sizeof(struct rx_packet )));
    rec = add_record(&packet);
    pbreak = 0;
/* break up the received packet into fields and records for easy parsing */
    while((i < (session -> rx_i)) && !(pbreak != 0)){
      switch((session -> rx_dbuf)[i]){
        case '\x00':
{
/* new record */
          if (packet.rcount == 1) {
/* packet command */
            packet.cmd = atoi(( *( *packet.records[0]).fields[0]).data);
          }
          else if (packet.rcount == 2) {
/* special case: binary multimedia packets should not be parsed here */
            if (packet.cmd == 27) {
/* add the chunked to new record */
              rec = add_record(&packet);
              field = add_field(rec);
              field -> data = ((session -> rx_dbuf) + (i + 1));
              field -> len = ((session -> rx_i) - i);
/* now skip the binary data */
              res = get_chunk_len((field -> data));
/* determine if we have more packets */
              if (((res + 6) + i) < (session -> rx_i)) {
/* we have more than one packet in this stream */
                i += (res + 6);
                pbreak = (!0);
              }
              else {
                i = (session -> rx_i);
              }
            }
          }
          else if (!(field != 0)) {
            field = add_field(rec);
            field -> data = ((session -> rx_dbuf) + i);
          }
          (session -> rx_dbuf)[i] = 0;
          rec = add_record(&packet);
          field = ((struct field *)((void *)0));
          break; 
        }
        case '\x01':
{
/* new field */
          (session -> rx_dbuf)[i] = 0;
          if (!(field != 0)) {
            field = add_field(rec);
            field -> data = ((session -> rx_dbuf) + i);
          }
          field = ((struct field *)((void *)0));
          break; 
        }
        case '\x02':
{
/* packet is done! */
          (session -> rx_dbuf)[i] = 0;
          pbreak = (!0);
          break; 
        }
        default:
{
/* skip non special characters */
          if (!(field != 0)) {
            field = add_field(rec);
            field -> data = ((session -> rx_dbuf) + i);
          }
          field -> len++;
          break; 
        }
      }
      i++;
    }
    if (packet.rcount < 2) {
/* bad packet */
      purple_connection_error((session -> con),((const char *)(dgettext("pidgin","Invalid packet received from MXit."))));
      free_rx_packet(&packet);
      continue; 
    }
    (session -> rx_dbuf)[session -> rx_i] = 0;
    packet.errcode = atoi(( *( *packet.records[1]).fields[0]).data);
    purple_debug_info("prpl-loubserp-mxit","Packet received CMD:%i (%i)\n",packet.cmd,packet.errcode);
#ifdef	DEBUG_PROTOCOL
/* debug */
    dump_packet(&packet);
#endif
/* reset the out ack */
    if ((session -> outack) == packet.cmd) {
/* outstanding ack received from mxit server */
      session -> outack = 0;
    }
/* check packet status */
    if (packet.errcode != 0) {
/* error reply! */
      if ((( *packet.records[1]).fcount > 1) && (( *( *packet.records[1]).fields[1]).data != 0)) 
        packet.errmsg = ( *( *packet.records[1]).fields[1]).data;
      else 
        packet.errmsg = ((char *)((void *)0));
      res = process_error_response(session,&packet);
    }
    else {
/* success reply! */
      res = process_success_response(session,&packet);
    }
/* free up the packet resources */
    free_rx_packet(&packet);
  }
  if ((session -> outack) == 0) 
    mxit_manage_queue(session);
  return res;
}
/*------------------------------------------------------------------------
 * Callback when data is received from the MXit server.
 *
 *  @param user_data		The MXit session object
 *  @param source			The file-descriptor on which data was received
 *  @param cond				Condition which caused the callback (PURPLE_INPUT_READ)
 */

void mxit_cb_rx(gpointer user_data,gint source,PurpleInputCondition cond)
{
  struct MXitSession *session = (struct MXitSession *)user_data;
  char ch;
  int res;
  int len;
  if ((session -> rx_state) == 1) {
/* we are reading in the packet length */
    len = (read((session -> fd),(&ch),1));
    if (len < 0) {
/* connection error */
      purple_connection_error((session -> con),((const char *)(dgettext("pidgin","A connection error occurred to MXit. (read stage 0x01)"))));
      return ;
    }
    else if (len == 0) {
/* connection closed */
      purple_connection_error((session -> con),((const char *)(dgettext("pidgin","A connection error occurred to MXit. (read stage 0x02)"))));
      return ;
    }
    else {
/* byte read */
      if (ch == ((((session -> http) != 0)?'\x26' : 0))) {
/* the end of the length record found */
        (session -> rx_lbuf)[session -> rx_i] = 0;
        session -> rx_res = atoi(((session -> rx_lbuf) + 3));
        if ((session -> rx_res) > 1 * 1000 * 1000) {
          purple_connection_error((session -> con),((const char *)(dgettext("pidgin","A connection error occurred to MXit. (read stage 0x03)"))));
        }
        session -> rx_state = 2;
        session -> rx_i = 0;
      }
      else {
/* still part of the packet length record */
        (session -> rx_lbuf)[session -> rx_i] = ch;
        session -> rx_i++;
        if ((session -> rx_i) >= sizeof(session -> rx_lbuf)) {
/* malformed packet length record (too long) */
          purple_connection_error((session -> con),((const char *)(dgettext("pidgin","A connection error occurred to MXit. (read stage 0x04)"))));
          return ;
        }
      }
    }
  }
  else if ((session -> rx_state) == 2) {
/* we are reading in the packet data */
    len = (read((session -> fd),((session -> rx_dbuf) + (session -> rx_i)),(session -> rx_res)));
    if (len < 0) {
/* connection error */
      purple_connection_error((session -> con),((const char *)(dgettext("pidgin","A connection error occurred to MXit. (read stage 0x05)"))));
      return ;
    }
    else if (len == 0) {
/* connection closed */
      purple_connection_error((session -> con),((const char *)(dgettext("pidgin","A connection error occurred to MXit. (read stage 0x06)"))));
      return ;
    }
    else {
/* data read */
      session -> rx_i += len;
      session -> rx_res -= len;
      if ((session -> rx_res) == 0) {
/* ok, so now we have read in the whole packet */
        session -> rx_state = 3;
      }
    }
  }
  if ((session -> rx_state) == 3) {
/* we have a full packet, which we now need to process */
    res = mxit_parse_packet(session);
    if (res == 0) {
/* we are still logged in */
      session -> rx_state = 1;
      session -> rx_res = 0;
      session -> rx_i = 0;
    }
  }
}
/*------------------------------------------------------------------------
 * Log the user off MXit and close the connection
 *
 *  @param session		The MXit session object
 */

void mxit_close_connection(struct MXitSession *session)
{
  purple_debug_info("prpl-loubserp-mxit","mxit_close_connection\n");
  if (!(((session -> flags) & 1) != 0)) {
/* we are already closed */
    return ;
  }
  else if (((session -> flags) & 2) != 0) {
/* we are currently logged in so we need to send a logout packet */
    if (!((session -> http) != 0)) {
      mxit_send_logout(session);
    }
    session -> flags &= ~2;
  }
  session -> flags &= ~1;
/* cancel all outstanding async calls */
  while((session -> async_calls) != 0){
    purple_util_fetch_url_cancel(( *(session -> async_calls)).data);
    session -> async_calls = g_slist_delete_link((session -> async_calls),(session -> async_calls));
  }
/* remove the input cb function */
  if (( *(session -> con)).inpa != 0) {
    purple_input_remove(( *(session -> con)).inpa);
    ( *(session -> con)).inpa = 0;
  }
/* remove HTTP poll timer */
  if ((session -> http_timer_id) > 0) 
    purple_timeout_remove((session -> http_timer_id));
/* remove slow queue manager timer */
  if ((session -> q_slow_timer_id) > 0) 
    purple_timeout_remove((session -> q_slow_timer_id));
/* remove fast queue manager timer */
  if ((session -> q_fast_timer_id) > 0) 
    purple_timeout_remove((session -> q_fast_timer_id));
/* remove all groupchat rooms */
  while((session -> rooms) != ((GList *)((void *)0))){
    struct multimx *multimx = (struct multimx *)( *(session -> rooms)).data;
    session -> rooms = g_list_remove((session -> rooms),multimx);
    free(multimx);
  }
  g_list_free((session -> rooms));
  session -> rooms = ((GList *)((void *)0));
/* remove all rx chats names */
  while((session -> active_chats) != ((GList *)((void *)0))){
    char *chat = (char *)( *(session -> active_chats)).data;
    session -> active_chats = g_list_remove((session -> active_chats),chat);
    g_free(chat);
  }
  g_list_free((session -> active_chats));
  session -> active_chats = ((GList *)((void *)0));
/* clear the internal invites */
  while((session -> invites) != ((GList *)((void *)0))){
    struct contact *contact = (struct contact *)( *(session -> invites)).data;
    session -> invites = g_list_remove((session -> invites),contact);
    if ((contact -> msg) != 0) 
      g_free((contact -> msg));
    if ((contact -> statusMsg) != 0) 
      g_free((contact -> statusMsg));
    if ((contact -> profile) != 0) 
      g_free((contact -> profile));
    g_free(contact);
  }
  g_list_free((session -> invites));
  session -> invites = ((GList *)((void *)0));
/* free profile information */
  if ((session -> profile) != 0) 
    free((session -> profile));
/* free custom emoticons */
  mxit_free_emoticon_cache(session);
/* free allocated memory */
  if ((session -> uid) != 0) 
    g_free((session -> uid));
  g_free((session -> encpwd));
  session -> encpwd = ((char *)((void *)0));
/* flush all the commands still in the queue */
  flush_queue(session);
}
