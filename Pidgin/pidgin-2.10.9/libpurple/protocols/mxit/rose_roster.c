/*
 *					MXit Protocol libPurple Plugin
 *
 *			-- user roster management (mxit contacts) --
 *
 *				Pieter Loubser	<libpurple@mxit.com>
 *
 *			(C) Copyright 2009	MXit Lifestyle (Pty) Ltd.
 *				<http://www.mxitlifestyle.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include	"internal.h"
#include	"debug.h"
#include	"protocol.h"
#include	"mxit.h"
#include	"roster.h"

struct contact_invite 
{
/* MXit session object */
  struct MXitSession *session;
/* The contact performing the invite */
  struct contact *contact;
}
;
/*========================================================================================================================
 * Presence / Status
 */
/* statuses (reference: libpurple/status.h) */
static const struct status {
PurpleStatusPrimitive primitive;
int mxit;
const char *id;
const char *name;}mxit_statuses[] = {
/*	primitive,						no,							id,			name					*/
/* 0 */
{(PURPLE_STATUS_OFFLINE), (0x00), ("offline"), ("Offline")}, 
/* 1 */
{(PURPLE_STATUS_AVAILABLE), (0x01), ("online"), ("Available")}, 
/* 2 */
{(PURPLE_STATUS_AWAY), (0x02), ("away"), ("Away")}, 
/* 3 */
{(PURPLE_STATUS_AVAILABLE), (0x03), ("chat"), ("Chatty")}, 
/* 4 */
{(PURPLE_STATUS_UNAVAILABLE), (0x04), ("dnd"), ("Do Not Disturb")}};
/*------------------------------------------------------------------------
 * Return list of supported statuses. (see status.h)
 *
 *  @param account	The MXit account object
 *  @return			List of PurpleStatusType
 */

GList *mxit_status_types(PurpleAccount *account)
{
  GList *statuslist = (GList *)((void *)0);
  PurpleStatusType *type;
  unsigned int i;
  for (i = 0; i < sizeof(mxit_statuses) / sizeof(mxit_statuses[0]); i++) {
    const struct status *status = (mxit_statuses + i);
/* add mxit status (reference: "libpurple/status.h") */
    type = purple_status_type_new_with_attrs((status -> primitive),(status -> id),((const char *)(dgettext("pidgin",(status -> name)))),(!0),(!0),0,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
    statuslist = g_list_append(statuslist,type);
  }
/* add Mood option */
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_MOOD,"mood",0,0,(!0),(!0),"mood",((const char *)(dgettext("pidgin","Mood Name"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  statuslist = g_list_append(statuslist,type);
  return statuslist;
}
/*------------------------------------------------------------------------
 * Returns the MXit presence code, given the unique status ID.
 *
 *  @param id		The status ID
 *  @return			The MXit presence code
 */

int mxit_convert_presence(const char *id)
{
  unsigned int i;
  for (i = 0; i < sizeof(mxit_statuses) / sizeof(mxit_statuses[0]); i++) {
/* status found! */
    if (strcmp(mxit_statuses[i].id,id) == 0) 
      return mxit_statuses[i].mxit;
  }
  return -1;
}
/*------------------------------------------------------------------------
 * Returns the MXit presence as a string, given the MXit presence ID.
 *
 *  @param no		The MXit presence I (see above)
 *  @return			The presence as a text string
 */

const char *mxit_convert_presence_to_name(short no)
{
  unsigned int i;
  for (i = 0; i < sizeof(mxit_statuses) / sizeof(mxit_statuses[0]); i++) {
/* status found! */
    if (mxit_statuses[i].mxit == no) 
      return (const char *)(dgettext("pidgin",mxit_statuses[i].name));
  }
  return "";
}
/*========================================================================================================================
 * Moods
 */
/* moods (reference: libpurple/status.h) */
static PurpleMood mxit_moods[] = {{("angry"), ("Angry"), ((gpointer *)((void *)0))}, {("excited"), ("Excited"), ((gpointer *)((void *)0))}, {("grumpy"), ("Grumpy"), ((gpointer *)((void *)0))}, {("happy"), ("Happy"), ((gpointer *)((void *)0))}, {("in_love"), ("In love"), ((gpointer *)((void *)0))}, {("invincible"), ("Invincible"), ((gpointer *)((void *)0))}, {("sad"), ("Sad"), ((gpointer *)((void *)0))}, {("hot"), ("Hot"), ((gpointer *)((void *)0))}, {("sick"), ("Sick"), ((gpointer *)((void *)0))}, {("sleepy"), ("Sleepy"), ((gpointer *)((void *)0))}, {("bored"), ("Bored"), ((gpointer *)((void *)0))}, {("cold"), ("Cold"), ((gpointer *)((void *)0))}, {("confused"), ("Confused"), ((gpointer *)((void *)0))}, {("hungry"), ("Hungry"), ((gpointer *)((void *)0))}, {("stressed"), ("Stressed"), ((gpointer *)((void *)0))}, 
/* Mark the last record. */
{((const char *)((void *)0)), ((const char *)((void *)0)), ((gpointer *)((void *)0))}};
/*------------------------------------------------------------------------
 * Returns the MXit mood code, given the unique mood ID.
 *
 *  @param id		The mood ID
 *  @return			The MXit mood code
 */

int mxit_convert_mood(const char *id)
{
  unsigned int i;
/* Mood is being unset */
  if (id == ((const char *)((void *)0))) 
    return 0;
  for (i = 0; i < sizeof(mxit_moods) / sizeof(mxit_moods[0]) - 1; i++) {
/* mood found! */
    if (strcmp(mxit_moods[i].mood,id) == 0) 
/* because MXIT_MOOD_NONE is 0 */
      return (i + 1);
  }
  return -1;
}
/*------------------------------------------------------------------------
 * Return the list of MXit-supported moods.
 *
 *  @param account	The MXit account object
 */

PurpleMood *mxit_get_moods(PurpleAccount *account)
{
  return mxit_moods;
}
/*------------------------------------------------------------------------
 * Returns the MXit mood as a string, given the MXit mood's ID.
 *
 *  @param id		The MXit mood ID (see roster.h)
 *  @return			The mood as a text string
 */

const char *mxit_convert_mood_to_name(short id)
{
  switch(id){
    case 0x01:
{
      return (const char *)(dgettext("pidgin","Angry"));
    }
    case 0x02:
{
      return (const char *)(dgettext("pidgin","Excited"));
    }
    case 0x03:
{
      return (const char *)(dgettext("pidgin","Grumpy"));
    }
    case 0x04:
{
      return (const char *)(dgettext("pidgin","Happy"));
    }
    case 0x05:
{
      return (const char *)(dgettext("pidgin","In Love"));
    }
    case 0x06:
{
      return (const char *)(dgettext("pidgin","Invincible"));
    }
    case 0x07:
{
      return (const char *)(dgettext("pidgin","Sad"));
    }
    case 0x08:
{
      return (const char *)(dgettext("pidgin","Hot"));
    }
    case 0x09:
{
      return (const char *)(dgettext("pidgin","Sick"));
    }
    case 0x0A:
{
      return (const char *)(dgettext("pidgin","Sleepy"));
    }
    case 0x0B:
{
      return (const char *)(dgettext("pidgin","Bored"));
    }
    case 0x0C:
{
      return (const char *)(dgettext("pidgin","Cold"));
    }
    case 0x0D:
{
      return (const char *)(dgettext("pidgin","Confused"));
    }
    case 0x0E:
{
      return (const char *)(dgettext("pidgin","Hungry"));
    }
    case 0x0F:
{
      return (const char *)(dgettext("pidgin","Stressed"));
    }
    default:
{
      return "";
    }
  }
}
/*========================================================================================================================
 * Subscription Types
 */
/*------------------------------------------------------------------------
 * Returns a Contact subscription type as a string.
 *
 *  @param subtype	The subscription type
 *  @return			The subscription type as a text string
 */

const char *mxit_convert_subtype_to_name(short subtype)
{
  switch(subtype){
    case 'B':
{
      return (const char *)(dgettext("pidgin","Both"));
    }
    case 'P':
{
      return (const char *)(dgettext("pidgin","Pending"));
    }
    case 'A':
{
      return (const char *)(dgettext("pidgin","Invited"));
    }
    case 'R':
{
      return (const char *)(dgettext("pidgin","Rejected"));
    }
    case 'D':
{
      return (const char *)(dgettext("pidgin","Deleted"));
    }
    case 'N':
{
      return (const char *)(dgettext("pidgin","None"));
    }
    default:
{
      return "";
    }
  }
}
/*========================================================================================================================
 * Calls from the MXit Protocol layer
 */
#if	0
/*------------------------------------------------------------------------
 * Dump a contact's info the the debug console.
 *
 *  @param contact		The contact
 */
#endif
#if	0
/*------------------------------------------------------------------------
 * Move a buddy from one group to another
 *
 * @param buddy		the buddy to move between groups
 * @param group		the new group to move the buddy to
 */
/* make sure the groups actually differs */
/* groupnames does not match, so we need to make the update */
/*
		 * XXX: libPurple does not currently provide an API to change or rename the group name
		 * for a specific buddy. One option is to remove the buddy from the list and re-adding
		 * him in the new group, but by doing that makes the buddy go offline and then online
		 * again. This is really not ideal and very irritating, but how else then?
		 */
/* create new buddy, and transfer 'contact' data */
/* remove the buddy */
/* add buddy */
/* now re-instate his presence again */
/* update the buddy's status (reference: "libpurple/prpl.h") */
/* update the buddy's mood */
/* update avatar */
#endif
/*------------------------------------------------------------------------
 * A contact update packet was received from the MXit server, so update the buddy's
 * information.
 *
 *  @param session		The MXit session object
 *  @param contact		The contact
 */

void mxit_update_contact(struct MXitSession *session,struct contact *contact)
{
  PurpleBuddy *buddy = (PurpleBuddy *)((void *)0);
  PurpleGroup *group = (PurpleGroup *)((void *)0);
  const char *id = (const char *)((void *)0);
  purple_debug_info("prpl-loubserp-mxit","mxit_update_contact: user=\'%s\' alias=\'%s\' group=\'%s\'\n",(contact -> username),(contact -> alias),(contact -> groupname));
/*
	 * libPurple requires all contacts to be in a group.
	 * So if this MXit contact isn't in a group, pretend it is.
	 */
  if (( *contact -> groupname) == 0) {
    g_strlcpy((contact -> groupname),"MXit",(sizeof(contact -> groupname)));
  }
/* find or create a group for this contact */
  group = purple_find_group((contact -> groupname));
  if (!(group != 0)) 
    group = purple_group_new((contact -> groupname));
/* see if the buddy is not in the group already */
  buddy = purple_find_buddy_in_group((session -> acc),(contact -> username),group);
  if (!(buddy != 0)) {
/* buddy not found in the group */
/* lets try finding him in all groups */
    buddy = purple_find_buddy((session -> acc),(contact -> username));
    if (buddy != 0) {
/* ok, so we found him in another group. to switch him between groups we must delete him and add him again. */
      purple_blist_remove_buddy(buddy);
      buddy = ((PurpleBuddy *)((void *)0));
    }
/* create new buddy */
    buddy = purple_buddy_new((session -> acc),(contact -> username),(contact -> alias));
    purple_buddy_set_protocol_data(buddy,contact);
/* add new buddy to list */
    purple_blist_add_buddy(buddy,0,group,0);
  }
  else {
/* buddy was found in the group */
    gpointer data = (gpointer )((void *)0);
/* now update the buddy's alias */
    purple_blist_alias_buddy(buddy,(contact -> alias));
/* replace the buddy's contact struct */
    if ((data = purple_buddy_get_protocol_data(buddy)) != 0) 
      free(data);
    purple_buddy_set_protocol_data(buddy,contact);
  }
/* load buddy's avatar id */
  id = purple_buddy_icons_get_checksum_for_user(buddy);
  if (id != 0) 
    contact -> avatarId = g_strdup(id);
  else 
    contact -> avatarId = ((char *)((void *)0));
/* update the buddy's status (reference: "libpurple/prpl.h") */
  purple_prpl_got_user_status((session -> acc),(contact -> username),mxit_statuses[contact -> presence].id,((void *)((void *)0)));
/* update the buddy's mood */
  if ((contact -> mood) == 0) 
    purple_prpl_got_user_status_deactive((session -> acc),(contact -> username),"mood");
  else 
    purple_prpl_got_user_status((session -> acc),(contact -> username),"mood","mood",mxit_moods[(contact -> mood) - 1].mood,((void *)((void *)0)));
}
/*------------------------------------------------------------------------
 * A presence update packet was received from the MXit server, so update the buddy's
 * information.
 *
 *  @param session		The MXit session object
 *  @param username		The contact which presence to update
 *  @param presence		The new presence state for the contact
 *  @param mood			The new mood for the contact
 *  @param customMood	The custom mood identifier
 *  @param statusMsg	This is the contact's status message
 *  @param flags		The contact's presence flags.
 */

void mxit_update_buddy_presence(struct MXitSession *session,const char *username,short presence,short mood,const char *customMood,const char *statusMsg,int flags)
{
  PurpleBuddy *buddy = (PurpleBuddy *)((void *)0);
  struct contact *contact = (struct contact *)((void *)0);
  purple_debug_info("prpl-loubserp-mxit","mxit_update_buddy_presence: user=\'%s\' presence=%i mood=%i customMood=\'%s\' statusMsg=\'%s\'\n",username,presence,mood,customMood,statusMsg);
  if ((presence < 0) || (presence > 4)) {
    purple_debug_info("prpl-loubserp-mxit","mxit_update_buddy_presence: invalid presence state %i\n",presence);
/* ignore packet */
    return ;
  }
/* find the buddy information for this contact (reference: "libpurple/blist.h") */
  buddy = purple_find_buddy((session -> acc),username);
  if (!(buddy != 0)) {
    purple_debug_warning("prpl-loubserp-mxit","mxit_update_buddy_presence: unable to find the buddy \'%s\'\n",username);
    return ;
  }
  contact = (purple_buddy_get_protocol_data(buddy));
  if (!(contact != 0)) 
    return ;
  contact -> presence = presence;
  contact -> mood = mood;
  contact -> capabilities = flags;
/* validate mood */
  if (((contact -> mood) < 0) || ((contact -> mood) > 15)) 
    contact -> mood = 0;
  g_strlcpy((contact -> customMood),customMood,(sizeof(contact -> customMood)));
// TODO: Download custom mood frame.
/* update status message */
  if ((contact -> statusMsg) != 0) {
    g_free((contact -> statusMsg));
    contact -> statusMsg = ((char *)((void *)0));
  }
  if ((statusMsg != 0) && (statusMsg[0] != 0)) 
    contact -> statusMsg = g_markup_escape_text(statusMsg,(-1));
/* update the buddy's status (reference: "libpurple/prpl.h") */
  if ((contact -> statusMsg) != 0) 
    purple_prpl_got_user_status((session -> acc),username,mxit_statuses[contact -> presence].id,"message",(contact -> statusMsg),((void *)((void *)0)));
  else 
    purple_prpl_got_user_status((session -> acc),username,mxit_statuses[contact -> presence].id,((void *)((void *)0)));
/* update the buddy's mood */
  if ((contact -> mood) == 0) 
    purple_prpl_got_user_status_deactive((session -> acc),username,"mood");
  else 
    purple_prpl_got_user_status((session -> acc),username,"mood","mood",mxit_moods[(contact -> mood) - 1].mood,((void *)((void *)0)));
}
/*------------------------------------------------------------------------
 * Update the buddy's avatar.
 * Either a presence update packet was received from the MXit server, or a profile response.
 *
 *  @param session		The MXit session object
 *  @param username		The contact which presence to update
 *  @param avatarId		This is the contact's avatar id
 */

void mxit_update_buddy_avatar(struct MXitSession *session,const char *username,const char *avatarId)
{
  PurpleBuddy *buddy = (PurpleBuddy *)((void *)0);
  struct contact *contact = (struct contact *)((void *)0);
  purple_debug_info("prpl-loubserp-mxit","mxit_update_buddy_avatar: user=\'%s\' avatar=\'%s\'\n",username,avatarId);
/* find the buddy information for this contact (reference: "libpurple/blist.h") */
  buddy = purple_find_buddy((session -> acc),username);
  if (!(buddy != 0)) {
    purple_debug_warning("prpl-loubserp-mxit","mxit_update_buddy_presence: unable to find the buddy \'%s\'\n",username);
    return ;
  }
  contact = (purple_buddy_get_protocol_data(buddy));
  if (!(contact != 0)) 
    return ;
  if (((contact -> avatarId) != 0) && (g_ascii_strcasecmp((contact -> avatarId),avatarId) == 0)) {
/*  avatar has not changed - do nothing */
  }
  else 
/* avatar has changed */
if (avatarId[0] != 0) {
    if ((contact -> avatarId) != 0) 
      g_free((contact -> avatarId));
    contact -> avatarId = g_strdup(avatarId);
/* Send request to download new avatar image */
    mxit_get_avatar(session,username,avatarId);
  }
  else 
/* clear current avatar */
    purple_buddy_icons_set_for_user((session -> acc),username,0,0,0);
}
/*------------------------------------------------------------------------
 * update the blist cached by libPurple. We need to do this to keep
 * libPurple and MXit's rosters in sync with each other.
 *
 * @param session		The MXit session object
 */

void mxit_update_blist(struct MXitSession *session)
{
  PurpleBuddy *buddy = (PurpleBuddy *)((void *)0);
  GSList *list = (GSList *)((void *)0);
  unsigned int i;
/* remove all buddies we did not receive a roster update for.
	 * these contacts must have been removed from another client */
  list = purple_find_buddies((session -> acc),0);
  for (i = 0; i < g_slist_length(list); i++) {
    buddy = (g_slist_nth_data(list,i));
    if (!(purple_buddy_get_protocol_data(buddy) != 0)) {
      const gchar *alias = purple_buddy_get_alias(buddy);
      const gchar *name = purple_buddy_get_name(buddy);
/* this buddy should be removed, because we did not receive him in our roster update from MXit */
      purple_debug_info("prpl-loubserp-mxit","Removed \'old\' buddy from the blist \'%s\' (%s)\n",alias,name);
      purple_blist_remove_buddy(buddy);
    }
  }
/* tell the UI to update the blist */
  purple_blist_add_account((session -> acc));
}
/*------------------------------------------------------------------------
 * The user authorized an invite (subscription request).
 *
 *  @param user_data	Object associated with the invite
 */

static void mxit_cb_buddy_auth(gpointer user_data)
{
  struct contact_invite *invite = (struct contact_invite *)user_data;
  purple_debug_info("prpl-loubserp-mxit","mxit_cb_buddy_auth \'%s\'\n",( *(invite -> contact)).username);
/* send a allow subscription packet to MXit */
  mxit_send_allow_sub((invite -> session),( *(invite -> contact)).username,( *(invite -> contact)).alias);
/* remove the invite from our internal invites list */
  ( *(invite -> session)).invites = g_list_remove(( *(invite -> session)).invites,(invite -> contact));
/* freeup invite object */
  if (( *(invite -> contact)).msg != 0) 
    g_free(( *(invite -> contact)).msg);
  if (( *(invite -> contact)).statusMsg != 0) 
    g_free(( *(invite -> contact)).statusMsg);
  if (( *(invite -> contact)).profile != 0) 
    g_free(( *(invite -> contact)).profile);
  g_free((invite -> contact));
  g_free(invite);
}
/*------------------------------------------------------------------------
 * The user rejected an invite (subscription request).
 *
 *  @param user_data	Object associated with the invite
 */

static void mxit_cb_buddy_deny(gpointer user_data)
{
  struct contact_invite *invite = (struct contact_invite *)user_data;
  purple_debug_info("prpl-loubserp-mxit","mxit_cb_buddy_deny \'%s\'\n",( *(invite -> contact)).username);
/* send a deny subscription packet to MXit */
  mxit_send_deny_sub((invite -> session),( *(invite -> contact)).username,0);
/* remove the invite from our internal invites list */
  ( *(invite -> session)).invites = g_list_remove(( *(invite -> session)).invites,(invite -> contact));
/* freeup invite object */
  if (( *(invite -> contact)).msg != 0) 
    g_free(( *(invite -> contact)).msg);
  if (( *(invite -> contact)).statusMsg != 0) 
    g_free(( *(invite -> contact)).statusMsg);
  if (( *(invite -> contact)).profile != 0) 
    g_free(( *(invite -> contact)).profile);
  g_free((invite -> contact));
  g_free(invite);
}
/*------------------------------------------------------------------------
 * A new subscription request packet was received from the MXit server.
 * Prompt user to accept or reject it.
 *
 *  @param session		The MXit session object
 *  @param contact		The contact performing the invite
 */

void mxit_new_subscription(struct MXitSession *session,struct contact *contact)
{
  struct contact_invite *invite;
  purple_debug_info("prpl-loubserp-mxit","mxit_new_subscription from \'%s\' (%s)\n",(contact -> username),(contact -> alias));
  invite = ((struct contact_invite *)(g_malloc0_n(1,(sizeof(struct contact_invite )))));
  invite -> session = session;
  invite -> contact = contact;
/* add the invite to our internal invites list */
  ( *(invite -> session)).invites = g_list_append(( *(invite -> session)).invites,(invite -> contact));
/* (reference: "libpurple/account.h") */
  purple_account_request_authorization((session -> acc),(contact -> username),0,(contact -> alias),(contact -> msg),0,mxit_cb_buddy_auth,mxit_cb_buddy_deny,invite);
}
/*------------------------------------------------------------------------
 * Return the contact object for a mxit invite
 *
 *  @param session		The MXit session object
 *  @param username		The username of the contact
 *  @return				The contact object for the inviting user
 */

struct contact *get_mxit_invite_contact(struct MXitSession *session,const char *username)
{
  struct contact *con = (struct contact *)((void *)0);
  struct contact *match = (struct contact *)((void *)0);
  unsigned int i;
{
/* run through all the invites and try and find the match */
    for (i = 0; i < g_list_length((session -> invites)); i++) {
      con = (g_list_nth_data((session -> invites),i));
      if (strcmp((con -> username),username) == 0) {
/* invite found */
        match = con;
        break; 
      }
    }
  }
  return match;
}
/*------------------------------------------------------------------------
 * Return TRUE if this is a MXit Chatroom contact.
 *
 *  @param session		The MXit session object
 *  @param username		The username of the contact
 */

gboolean is_mxit_chatroom_contact(struct MXitSession *session,const char *username)
{
  PurpleBuddy *buddy;
  struct contact *contact = (struct contact *)((void *)0);
/* find the buddy */
  buddy = purple_find_buddy((session -> acc),username);
  if (!(buddy != 0)) {
    purple_debug_warning("prpl-loubserp-mxit","is_mxit_chatroom_contact: unable to find the buddy \'%s\'\n",username);
    return 0;
  }
  contact = (purple_buddy_get_protocol_data(buddy));
  if (!(contact != 0)) 
    return 0;
  return (contact -> type) == 9;
}
/*========================================================================================================================
 * Callbacks from libpurple
 */
/*------------------------------------------------------------------------
 * The user has added a buddy to the list, so send an invite request.
 *
 *  @param gc		The connection object
 *  @param buddy	The new buddy
 *  @param group	The group of the new buddy
 *  @param message	The invite message
 */

void mxit_add_buddy(PurpleConnection *gc,PurpleBuddy *buddy,PurpleGroup *group,const char *message)
{
  struct MXitSession *session = (purple_connection_get_protocol_data(gc));
  GSList *list = (GSList *)((void *)0);
  PurpleBuddy *mxbuddy = (PurpleBuddy *)((void *)0);
  unsigned int i;
  const gchar *buddy_name = purple_buddy_get_name(buddy);
  const gchar *buddy_alias = purple_buddy_get_alias(buddy);
  const gchar *group_name = purple_group_get_name(group);
  purple_debug_info("prpl-loubserp-mxit","mxit_add_buddy \'%s\' (group=\'%s\')\n",buddy_name,group_name);
  list = purple_find_buddies((session -> acc),buddy_name);
  if (g_slist_length(list) == 1) {
    purple_debug_info("prpl-loubserp-mxit","mxit_add_buddy (scenario 1) (list:%i)\n",g_slist_length(list));
/*
		 * we only send an invite to MXit when the user is not already inside our
		 * blist.  this is done because purple does an add_buddy() call when
		 * you accept an invite.  so in that case the user is already
		 * in our blist and ready to be chatted to.
		 */
    if (buddy_name[0] == '#') {
      gchar *tmp = (gchar *)(purple_base64_decode((buddy_name + 1),0));
      if (tmp != 0) {
        mxit_send_invite(session,tmp,0,buddy_alias,group_name,message);
        g_free(tmp);
      }
    }
    else 
      mxit_send_invite(session,buddy_name,(!0),buddy_alias,group_name,message);
  }
  else {
    purple_debug_info("prpl-loubserp-mxit","mxit_add_buddy (scenario 2) (list:%i)\n",g_slist_length(list));
/*
		 * we already have the buddy in our list, so we will only update
		 * his information here and not send another invite message
		 */
/* find the correct buddy */
    for (i = 0; i < g_slist_length(list); i++) {
      mxbuddy = (g_slist_nth_data(list,i));
      if (purple_buddy_get_protocol_data(mxbuddy) != ((void *)((void *)0))) {
/* this is our REAL MXit buddy! */
/* now update the buddy's alias */
        purple_blist_alias_buddy(mxbuddy,buddy_alias);
/* now update the buddy's group */
//				mxbuddy = mxit_update_buddy_group( session, mxbuddy, group );
/* send the update to the MXit server */
        mxit_send_update_contact(session,purple_buddy_get_name(mxbuddy),purple_buddy_get_alias(mxbuddy),group_name);
      }
    }
  }
/*
	 * we remove the buddy here from the buddy list because the MXit server
	 * will send us a proper contact update packet if this succeeds.  now
	 * we do not have to worry about error handling in case of adding an
	 * invalid contact.  so the user will still see the contact as offline
	 * until he eventually accepts the invite.
	 */
  purple_blist_remove_buddy(buddy);
  g_slist_free(list);
}
/*------------------------------------------------------------------------
 * The user has removed a buddy from the list.
 *
 *  @param gc		The connection object
 *  @param buddy	The buddy being removed
 *  @param group	The group the buddy was in
 */

void mxit_remove_buddy(PurpleConnection *gc,PurpleBuddy *buddy,PurpleGroup *group)
{
  struct MXitSession *session = (purple_connection_get_protocol_data(gc));
  const gchar *buddy_name = purple_buddy_get_name(buddy);
  purple_debug_info("prpl-loubserp-mxit","mxit_remove_buddy \'%s\'\n",buddy_name);
  mxit_send_remove(session,buddy_name);
}
/*------------------------------------------------------------------------
 * The user changed the buddy's alias.
 *
 *  @param gc		The connection object
 *  @param who		The username of the buddy
 *  @param alias	The new alias
 */

void mxit_buddy_alias(PurpleConnection *gc,const char *who,const char *alias)
{
  struct MXitSession *session = (purple_connection_get_protocol_data(gc));
  PurpleBuddy *buddy = (PurpleBuddy *)((void *)0);
  PurpleGroup *group = (PurpleGroup *)((void *)0);
  purple_debug_info("prpl-loubserp-mxit","mxit_buddy_alias \'%s\' to \'%s\n",who,alias);
/* find the buddy */
  buddy = purple_find_buddy((session -> acc),who);
  if (!(buddy != 0)) {
    purple_debug_warning("prpl-loubserp-mxit","mxit_buddy_alias: unable to find the buddy \'%s\'\n",who);
    return ;
  }
/* find buddy group */
  group = purple_buddy_get_group(buddy);
  if (!(group != 0)) {
    purple_debug_warning("prpl-loubserp-mxit","mxit_buddy_alias: unable to find the group for buddy \'%s\'\n",who);
    return ;
  }
  mxit_send_update_contact(session,who,alias,purple_group_get_name(group));
}
/*------------------------------------------------------------------------
 * The user changed the group for a single buddy.
 *
 *  @param gc			The connection object
 *  @param who			The username of the buddy
 *  @param old_group	The old group's name
 *  @param new_group	The new group's name
 */

void mxit_buddy_group(PurpleConnection *gc,const char *who,const char *old_group,const char *new_group)
{
  struct MXitSession *session = (purple_connection_get_protocol_data(gc));
  PurpleBuddy *buddy = (PurpleBuddy *)((void *)0);
  purple_debug_info("prpl-loubserp-mxit","mxit_buddy_group from \'%s\' to \'%s\'\n",old_group,new_group);
/* find the buddy */
  buddy = purple_find_buddy((session -> acc),who);
  if (!(buddy != 0)) {
    purple_debug_warning("prpl-loubserp-mxit","mxit_buddy_group: unable to find the buddy \'%s\'\n",who);
    return ;
  }
  mxit_send_update_contact(session,who,purple_buddy_get_alias(buddy),new_group);
}
/*------------------------------------------------------------------------
 * The user has selected to rename a group, so update all contacts in that
 * group.
 *
 *  @param gc				The connection object
 *  @param old_name			The old group name
 *  @param group			The updated group object
 *  @param moved_buddies	The buddies affected by the rename
 */

void mxit_rename_group(PurpleConnection *gc,const char *old_name,PurpleGroup *group,GList *moved_buddies)
{
  struct MXitSession *session = (purple_connection_get_protocol_data(gc));
  PurpleBuddy *buddy = (PurpleBuddy *)((void *)0);
  GList *item = (GList *)((void *)0);
  purple_debug_info("prpl-loubserp-mxit","mxit_rename_group from \'%s\' to \'%s\n",old_name,purple_group_get_name(group));
//  TODO: Might be more efficient to use the "rename group" command (cmd=29).
/* loop through all the contacts in the group and send updates */
  item = moved_buddies;
  while(item != 0){
    buddy = (item -> data);
    mxit_send_update_contact(session,purple_buddy_get_name(buddy),purple_buddy_get_alias(buddy),purple_group_get_name(group));
    item = ((item != 0)?( *((GList *)item)).next : ((struct _GList *)((void *)0)));
  }
}
