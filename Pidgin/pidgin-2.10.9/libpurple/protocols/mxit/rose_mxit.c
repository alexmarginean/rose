/*
 *					MXit Protocol libPurple Plugin
 *
 *					--  MXit libPurple plugin API --
 *
 *				Pieter Loubser	<libpurple@mxit.com>
 *
 *			(C) Copyright 2009	MXit Lifestyle (Pty) Ltd.
 *				<http://www.mxitlifestyle.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include	"internal.h"
#include	"debug.h"
#include	"accountopt.h"
#include	"version.h"
#include	"mxit.h"
#include	"protocol.h"
#include	"login.h"
#include	"roster.h"
#include	"chunk.h"
#include	"filexfer.h"
#include	"actions.h"
#include	"multimx.h"
#include	"voicevideo.h"
#ifdef	MXIT_LINK_CLICK
/* pidgin callback function pointers for URI click interception */
static void *(*mxit_pidgin_uri_cb)(const char *);
static PurpleNotifyUiOps *mxit_nots_override_original;
static PurpleNotifyUiOps mxit_nots_override;
static int not_link_ref_count = 0;
/*------------------------------------------------------------------------
 * Handle an URI clicked on the UI
 *
 * @param link	the link name which has been clicked
 */

static void *mxit_link_click(const char *link64)
{
  PurpleAccount *account;
  PurpleConnection *gc;
  gchar **parts = (gchar **)((void *)0);
  gchar *link = (gchar *)((void *)0);
  gsize len;
  gboolean is_command = 0;
  purple_debug_info("prpl-loubserp-mxit","mxit_link_click (%s)\n",link64);
  if (g_ascii_strncasecmp(link64,"gopher://",strlen("gopher://")) != 0) {
/* this is not for us */
    goto skip;
  }
/* decode the base64 payload */
  link = ((gchar *)(purple_base64_decode((link64 + strlen("gopher://")),&len)));
  purple_debug_info("prpl-loubserp-mxit","Clicked Link: \'%s\'\n",link);
  parts = g_strsplit(link,"|",6);
/* check if this is a valid mxit link */
  if ((((((!(parts != 0) || !(parts[0] != 0)) || !(parts[1] != 0)) || !(parts[2] != 0)) || !(parts[3] != 0)) || !(parts[4] != 0)) || !(parts[5] != 0)) {
/* this is not for us */
    goto skip;
  }
  else if (g_ascii_strcasecmp(parts[0],"MXIT") != 0) {
/* this is not for us */
    goto skip;
  }
/* find the account */
  account = purple_accounts_find(parts[1],parts[2]);
  if (!(account != 0)) 
    goto skip;
  gc = purple_account_get_connection(account);
  if (!(gc != 0)) 
    goto skip;
/* determine if it's a command-response to send */
  is_command = (atoi(parts[4]) == 1);
/* send click message back to MXit */
  mxit_send_message((purple_connection_get_protocol_data(gc)),parts[3],parts[5],0,is_command);
  g_free(link);
  link = ((gchar *)((void *)0));
  g_strfreev(parts);
  parts = ((gchar **)((void *)0));
  return (void *)link64;
  skip:
/* this is not an internal mxit link */
  if (link != 0) 
    g_free(link);
  link = ((gchar *)((void *)0));
  if (parts != 0) 
    g_strfreev(parts);
  parts = ((gchar **)((void *)0));
  if (mxit_pidgin_uri_cb != 0) 
    return ( *mxit_pidgin_uri_cb)(link64);
  else 
    return (void *)link64;
}
/*------------------------------------------------------------------------
 * Register MXit to receive URI click notifications from the UI
 */

void mxit_register_uri_handler()
{
  not_link_ref_count++;
  if (not_link_ref_count == 1) {
/* make copy of notifications */
    mxit_nots_override_original = purple_notify_get_ui_ops();
    memcpy((&mxit_nots_override),mxit_nots_override_original,(sizeof(PurpleNotifyUiOps )));
/* save previously configured callback function pointer */
    mxit_pidgin_uri_cb = mxit_nots_override.notify_uri;
/* override the URI function call with MXit's own one */
    mxit_nots_override.notify_uri = mxit_link_click;
    purple_notify_set_ui_ops(&mxit_nots_override);
  }
}
/*------------------------------------------------------------------------
 * Unregister MXit from receiving URI click notifications from the UI
 */

static void mxit_unregister_uri_handler()
{
  not_link_ref_count--;
  if (not_link_ref_count == 0) {
/* restore the notifications to its original state */
    purple_notify_set_ui_ops(mxit_nots_override_original);
  }
}
#endif
/*------------------------------------------------------------------------
 * This gets called when a new chat conversation is opened by the user
 *
 *  @param conv				The conversation object
 *  @param session			The MXit session object
 */

static void mxit_cb_chat_created(PurpleConversation *conv,struct MXitSession *session)
{
  PurpleConnection *gc;
  struct contact *contact;
  PurpleBuddy *buddy;
  const char *who;
  char *tmp;
  gc = purple_conversation_get_gc(conv);
  if ((session -> con) != gc) {
/* not our conversation */
    return ;
  }
  else if ((purple_conversation_get_type(conv)) != PURPLE_CONV_TYPE_IM) {
/* wrong type of conversation */
    return ;
  }
/* get the contact name */
  who = purple_conversation_get_name(conv);
  if (!(who != 0)) 
    return ;
  purple_debug_info("prpl-loubserp-mxit","Conversation started with \'%s\'\n",who);
/* find the buddy object */
  buddy = purple_find_buddy((session -> acc),who);
  if (!(buddy != 0)) 
    return ;
  contact = (purple_buddy_get_protocol_data(buddy));
  if (!(contact != 0)) 
    return ;
/* we ignore all conversations with which we have chatted with in this session */
  if (find_active_chat((session -> active_chats),who) != 0) 
    return ;
/* determine if this buddy is a MXit service */
  switch((contact -> type)){
    case 0x08:
{
    }
    case 0x09:
{
    }
    case 0x0C:
{
    }
    case 0x0D:
{
      tmp = g_strdup_printf("<font color=\"#999999\">%s</font>\n",((const char *)(dgettext("pidgin","Loading menu..."))));
      serv_got_im((session -> con),who,tmp,PURPLE_MESSAGE_NOTIFY,time(0));
      g_free(tmp);
      mxit_send_message(session,who," ",0,0);
    }
    default:
{
      break; 
    }
  }
}
/*------------------------------------------------------------------------
 * Enable some signals to handled by our plugin
 *
 *  @param session			The MXit session object
 */

void mxit_enable_signals(struct MXitSession *session)
{
/* enable the signal when a new conversation is opened by the user */
  purple_signal_connect_priority(purple_conversations_get_handle(),"conversation-created",session,((PurpleCallback )mxit_cb_chat_created),session,9999);
}
/*------------------------------------------------------------------------
 * Disable some signals handled by our plugin
 *
 *  @param session			The MXit session object
 */

static void mxit_disable_signals(struct MXitSession *session)
{
/* disable the signal when a new conversation is opened by the user */
  purple_signal_disconnect(purple_conversations_get_handle(),"conversation-created",session,((PurpleCallback )mxit_cb_chat_created));
}
/*------------------------------------------------------------------------
 * Return the base icon name.
 *
 *  @param account	The MXit account object
 *  @param buddy	The buddy
 *  @return			The icon name (excluding extension)
 */

static const char *mxit_list_icon(PurpleAccount *account,PurpleBuddy *buddy)
{
  return "mxit";
}
/*------------------------------------------------------------------------
 * Return the emblem icon name.
 *
 *  @param buddy	The buddy
 *  @return			The icon name (excluding extension)
 */

static const char *mxit_list_emblem(PurpleBuddy *buddy)
{
  struct contact *contact = (purple_buddy_get_protocol_data(buddy));
  if (!(contact != 0)) 
    return 0;
/* subscription state is Pending, Rejected or Deleted */
  if ((contact -> subtype) != 'B') 
    return "not-authorized";
  switch((contact -> type)){
/* external contacts via MXit */
    case 0x01:
{
    }
    case 0x02:
{
    }
    case 0x03:
{
    }
    case 0x04:
{
    }
    case 0x05:
{
    }
    case 0x06:
{
    }
    case 0x07:
{
      return "external";
    }
/* MXit services */
    case 0x08:
{
    }
    case 0x0C:
{
    }
    case 0x0D:
{
      return "bot";
    }
/* MXit group chat services */
    default:
{
      return 0;
    }
  }
}
/*------------------------------------------------------------------------
 * Return short string representing buddy's status for display on buddy list.
 * Returns status message (if one is set), or otherwise the mood.
 *
 *  @param buddy	The buddy.
 *  @return			The status text
 */

char *mxit_status_text(PurpleBuddy *buddy)
{
  char *text = (char *)((void *)0);
  struct contact *contact = (purple_buddy_get_protocol_data(buddy));
  if (!(contact != 0)) 
    return 0;
/* status message */
  if ((contact -> statusMsg) != 0) 
    text = g_strdup((contact -> statusMsg));
  else 
/* mood */
if ((contact -> mood) != 0) 
    text = g_strdup(mxit_convert_mood_to_name((contact -> mood)));
  return text;
}
/*------------------------------------------------------------------------
 * Return UI tooltip information for a buddy when hovering in buddy list.
 *
 *  @param buddy	The buddy
 *  @param info		The tooltip info being returned
 *  @param full		Return full or summarized information
 */

static void mxit_tooltip(PurpleBuddy *buddy,PurpleNotifyUserInfo *info,gboolean full)
{
  struct contact *contact = (purple_buddy_get_protocol_data(buddy));
  if (!(contact != 0)) 
    return ;
/* status (reference: "libpurple/notify.h") */
  if ((contact -> presence) != 0) 
    purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Status"))),mxit_convert_presence_to_name((contact -> presence)));
/* status message */
  if ((contact -> statusMsg) != 0) 
    purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Status Message"))),(contact -> statusMsg));
/* mood */
  if ((contact -> mood) != 0) 
    purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Mood"))),mxit_convert_mood_to_name((contact -> mood)));
/* subscription type */
  if ((contact -> subtype) != 0) 
    purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Subscription"))),mxit_convert_subtype_to_name((contact -> subtype)));
/* rejection message */
  if (((contact -> subtype) == 'R') && ((contact -> msg) != ((char *)((void *)0)))) 
    purple_notify_user_info_add_pair(info,((const char *)(dgettext("pidgin","Rejection Message"))),(contact -> msg));
}
/*------------------------------------------------------------------------
 * Initiate the logout sequence, close the connection and clear the session data.
 *
 *  @param gc	The connection object
 */

static void mxit_close(PurpleConnection *gc)
{
  struct MXitSession *session = (purple_connection_get_protocol_data(gc));
/* disable signals */
  mxit_disable_signals(session);
/* close the connection */
  mxit_close_connection(session);
#ifdef		MXIT_LINK_CLICK
/* unregister for uri click notification */
  mxit_unregister_uri_handler();
#endif
  purple_debug_info("prpl-loubserp-mxit","Releasing the session object..\n");
/* free the session memory */
  g_free(session);
  session = ((struct MXitSession *)((void *)0));
}
/*------------------------------------------------------------------------
 * Send a message to a contact
 *
 *  @param gc		The connection object
 *  @param who		The username of the recipient
 *  @param message	The message text
 *  @param flags	Message flags (defined in conversation.h)
 *  @return			Positive value (success, and echo to conversation window)
					Zero (success, no echo)
					Negative value (error)
 */

static int mxit_send_im(PurpleConnection *gc,const char *who,const char *message,PurpleMessageFlags flags)
{
  purple_debug_info("prpl-loubserp-mxit","Sending message \'%s\' to buddy \'%s\'\n",message,who);
  mxit_send_message((purple_connection_get_protocol_data(gc)),who,message,(!0),0);
/* echo to conversation window */
  return 1;
}
/*------------------------------------------------------------------------
 * The user changed their current presence state.
 *
 *  @param account	The MXit account object
 *  @param status	The new status (libPurple status type)
 */

static void mxit_set_status(PurpleAccount *account,PurpleStatus *status)
{
  struct MXitSession *session = (purple_connection_get_protocol_data((purple_account_get_connection(account))));
  const char *statusid;
  int presence;
  char *statusmsg1;
  char *statusmsg2;
/* Handle mood changes */
  if ((purple_status_type_get_primitive((purple_status_get_type(status)))) == PURPLE_STATUS_MOOD) {
    const char *moodid = purple_status_get_attr_string(status,"mood");
    int mood;
/* convert the purple mood to a mxit mood */
    mood = mxit_convert_mood(moodid);
    if (mood < 0) {
/* error, mood not found */
      purple_debug_info("prpl-loubserp-mxit","Mood status NOT found! (id = %s)\n",moodid);
      return ;
    }
/* update mood state */
    mxit_send_mood(session,mood);
    return ;
  }
/* get the status id (reference: "libpurple/status.h") */
  statusid = purple_status_get_id(status);
/* convert the purple status to a mxit status */
  presence = mxit_convert_presence(statusid);
  if (presence < 0) {
/* error, status not found */
    purple_debug_info("prpl-loubserp-mxit","Presence status NOT found! (id = %s)\n",statusid);
    return ;
  }
  statusmsg1 = purple_markup_strip_html(purple_status_get_attr_string(status,"message"));
  statusmsg2 = g_strndup(statusmsg1,250);
  purple_debug_info("prpl-loubserp-mxit","mxit_set_status: \'%s\'\n",statusmsg2);
/* update presence state */
  mxit_send_presence(session,presence,statusmsg2);
  g_free(statusmsg1);
  g_free(statusmsg2);
}
/*------------------------------------------------------------------------
 * MXit supports messages to offline contacts.
 *
 *  @param buddy	The buddy
 */

static gboolean mxit_offline_message(const PurpleBuddy *buddy)
{
  return (!0);
}
/*------------------------------------------------------------------------
 * Free the resources used to store a buddy.
 *
 *  @param buddy	The buddy
 */

static void mxit_free_buddy(PurpleBuddy *buddy)
{
  struct contact *contact;
  purple_debug_info("prpl-loubserp-mxit","mxit_free_buddy\n");
  contact = (purple_buddy_get_protocol_data(buddy));
  if (contact != 0) {
    if ((contact -> statusMsg) != 0) 
      g_free((contact -> statusMsg));
    if ((contact -> avatarId) != 0) 
      g_free((contact -> avatarId));
    if ((contact -> msg) != 0) 
      g_free((contact -> msg));
    g_free(contact);
  }
  purple_buddy_set_protocol_data(buddy,0);
}
/*------------------------------------------------------------------------
 * Periodic task called every KEEPALIVE_INTERVAL (30 sec) to to maintain
 * idle connections, timeouts and the transmission queue to the MXit server.
 *
 *  @param gc		The connection object
 */

static void mxit_keepalive(PurpleConnection *gc)
{
  struct MXitSession *session = (purple_connection_get_protocol_data(gc));
/* if not logged in, there is nothing to do */
  if (!(((session -> flags) & 2) != 0)) 
    return ;
/* pinging is only for socket connections (HTTP does polling) */
  if ((session -> http) != 0) 
    return ;
  if ((session -> last_tx) <= (mxit_now_milli() - (5 * 60 * 1000))) {
/*
		 * this connection has been idle for too long, better ping
		 * the server before it kills our connection.
		 */
    mxit_send_ping(session);
  }
}
/*------------------------------------------------------------------------
 * Set or clear our Buddy icon.
 *
 *  @param gc		The connection object
 *  @param img		The buddy icon data
 */

static void mxit_set_buddy_icon(PurpleConnection *gc,PurpleStoredImage *img)
{
  struct MXitSession *session = (purple_connection_get_protocol_data(gc));
  if (img == ((PurpleStoredImage *)((void *)0))) 
    mxit_set_avatar(session,0,0);
  else 
    mxit_set_avatar(session,(purple_imgstore_get_data(img)),(purple_imgstore_get_size(img)));
}
/*------------------------------------------------------------------------
 * Request profile information for another MXit contact.
 *
 *  @param gc		The connection object
 *  @param who		The username of the contact.
 */

static void mxit_get_info(PurpleConnection *gc,const char *who)
{
  PurpleBuddy *buddy;
  struct contact *contact;
  struct MXitSession *session = (purple_connection_get_protocol_data(gc));
  const char *profilelist[] = {("birthdate"), ("gender"), ("fullname"), ("firstname"), ("lastname"), ("registeredcountry"), ("lastseen"), ("statusmsg"), ("avatarid"), ("whereami"), ("aboutme"), ("relationship")};
  purple_debug_info("prpl-loubserp-mxit","mxit_get_info: \'%s\'\n",who);
/* find the buddy information for this contact (reference: "libpurple/blist.h") */
  buddy = purple_find_buddy((session -> acc),who);
  if (buddy != 0) {
/* user is in our contact-list, so it's not an invite */
    contact = (purple_buddy_get_protocol_data(buddy));
    if (!(contact != 0)) 
      return ;
/* only MXit users have profiles */
    if ((contact -> type) != 0) {
      mxit_popup(PURPLE_NOTIFY_MSG_WARNING,((const char *)(dgettext("pidgin","No profile available"))),((const char *)(dgettext("pidgin","This contact does not have a profile."))));
      return ;
    }
  }
/* send profile request */
  mxit_send_extprofile_request(session,who,(sizeof(profilelist) / sizeof(profilelist[0])),profilelist);
}
/*------------------------------------------------------------------------
 * Return a list of labels to be used by Pidgin for assisting the user.
 */

static GHashTable *mxit_get_text_table(PurpleAccount *acc)
{
  GHashTable *table;
  table = g_hash_table_new(g_str_hash,g_str_equal);
  g_hash_table_insert(table,"login_label",((gpointer )((const char *)(dgettext("pidgin","Your MXit ID...")))));
  return table;
}
/*------------------------------------------------------------------------
 * Re-Invite was selected from the buddy-list menu.
 *
 *  @param node		The entry in the buddy list.
 *  @param ignored	(not used)
 */

static void mxit_reinvite(PurpleBlistNode *node,gpointer ignored)
{
  PurpleBuddy *buddy = (PurpleBuddy *)node;
  PurpleConnection *gc = purple_account_get_connection((purple_buddy_get_account(buddy)));
  struct MXitSession *session = (purple_connection_get_protocol_data(gc));
  struct contact *contact;
  contact = (purple_buddy_get_protocol_data(((PurpleBuddy *)node)));
  if (!(contact != 0)) 
    return ;
/* send a new invite */
  mxit_send_invite(session,(contact -> username),(!0),(contact -> alias),(contact -> groupname),0);
}
/*------------------------------------------------------------------------
 * Buddy-list menu.
 *
 *  @param node		The entry in the buddy list.
 */

static GList *mxit_blist_menu(PurpleBlistNode *node)
{
  PurpleBuddy *buddy;
  struct contact *contact;
  GList *m = (GList *)((void *)0);
  PurpleMenuAction *act;
  if (!((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)) 
    return 0;
  buddy = ((PurpleBuddy *)node);
  contact = (purple_buddy_get_protocol_data(buddy));
  if (!(contact != 0)) 
    return 0;
  if ((((contact -> subtype) == 'D') || ((contact -> subtype) == 'R')) || ((contact -> subtype) == 'N')) {
/* contact is in Deleted, Rejected or None state */
    act = purple_menu_action_new(((const char *)(dgettext("pidgin","Re-Invite"))),((PurpleCallback )mxit_reinvite),0,0);
    m = g_list_append(m,act);
  }
  return m;
}
/*------------------------------------------------------------------------
 * Return Chat-room default settings.
 *
 *  @return		Chat defaults list
 */

static GHashTable *mxit_chat_info_defaults(PurpleConnection *gc,const char *chat_name)
{
  return g_hash_table_new_full(g_str_hash,g_str_equal,0,g_free);
}
/*------------------------------------------------------------------------
 * Send a typing indicator event.
 *
 *  @param gc		The connection object
 *  @param name		The username of the contact
 *  @param state	The typing state to be reported.
 */

static unsigned int mxit_send_typing(PurpleConnection *gc,const char *name,PurpleTypingState state)
{
  PurpleAccount *account = purple_connection_get_account(gc);
  struct MXitSession *session = (purple_connection_get_protocol_data(gc));
  PurpleBuddy *buddy;
  struct contact *contact;
  gchar *messageId = (gchar *)((void *)0);
/* find the buddy information for this contact (reference: "libpurple/blist.h") */
  buddy = purple_find_buddy(account,name);
  if (!(buddy != 0)) {
    purple_debug_warning("prpl-loubserp-mxit","mxit_send_typing: unable to find the buddy \'%s\'\n",name);
    return 0;
  }
  contact = (purple_buddy_get_protocol_data(buddy));
  if (!(contact != 0)) 
    return 0;
/* does this contact support and want typing notification? */
  if (!(((contact -> capabilities) & 4) != 0)) 
    return 0;
/* generate a unique message id */
  messageId = purple_uuid_random();
  switch(state){
/* currently typing */
    case PURPLE_TYPING:
{
      mxit_send_msgevent(session,name,messageId,16);
      break; 
    }
/* stopped typing */
/* not typing / erased all text */
    case PURPLE_NOT_TYPING:
{
    }
    case PURPLE_TYPED:
{
      mxit_send_msgevent(session,name,messageId,32);
      break; 
    }
    default:
{
      break; 
    }
  }
  g_free(messageId);
  return 0;
}
/*========================================================================================================================*/
static PurplePluginProtocolInfo proto_info = {((OPT_PROTO_REGISTER_NOSCREENNAME | OPT_PROTO_UNIQUE_CHATNAME | OPT_PROTO_IM_IMAGE | OPT_PROTO_INVITE_MESSAGE)), ((GList *)((void *)0)), ((GList *)((void *)0)), 
/* options */
/* user_splits */
/* protocol_options */
/* icon_spec */
/* supported formats */
/* min width & height */
/* max width & height */
/* max filesize */
/* scaling rules */
{("png,jpeg,bmp"), (32), (32), (800), (800), ((1 * 1000 * 1000 - 1000)), ((PURPLE_ICON_SCALE_SEND | PURPLE_ICON_SCALE_DISPLAY))}, (mxit_list_icon), (mxit_list_emblem), (mxit_status_text), (mxit_tooltip), (mxit_status_types), (mxit_blist_menu), (mxit_chat_info), (mxit_chat_info_defaults), (mxit_login), (mxit_close), (mxit_send_im), ((void (*)(PurpleConnection *, const char *))((void *)0)), (mxit_send_typing), (mxit_get_info), (mxit_set_status), ((void (*)(PurpleConnection *, int ))((void *)0)), ((void (*)(PurpleConnection *, const char *, const char *))((void *)0)), ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *))((void *)0)), ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)), (mxit_remove_buddy), ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *))((void *)0)), (mxit_chat_join), (mxit_chat_reject), (mxit_chat_name), (mxit_chat_invite), (mxit_chat_leave), ((void (*)(PurpleConnection *, int , const char *, const char *))((void *)0)), (mxit_chat_send), (mxit_keepalive), (mxit_register), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), (mxit_buddy_alias), (mxit_buddy_group), (mxit_rename_group), (mxit_free_buddy), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((const char *(*)(const PurpleAccount *, const char *))((void *)0)), (mxit_set_buddy_icon), ((void (*)(PurpleConnection *, PurpleGroup *))((void *)0)), ((char *(*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((PurpleChat *(*)(PurpleAccount *, const char *))((void *)0)), ((PurpleRoomlist *(*)(PurpleConnection *))((void *)0)), ((void (*)(PurpleRoomlist *))((void *)0)), ((void (*)(PurpleRoomlist *, PurpleRoomlistRoom *))((void *)0)), (mxit_xfer_enabled), (mxit_xfer_tx), (mxit_xfer_new), (mxit_offline_message), ((PurpleWhiteboardPrplOps *)((void *)0)), ((int (*)(PurpleConnection *, const char *, int ))((void *)0)), ((char *(*)(PurpleRoomlistRoom *))((void *)0)), ((void (*)(PurpleAccount *, PurpleAccountUnregistrationCb , void *))((void *)0)), ((gboolean (*)(PurpleConnection *, const char *, guint ))((void *)0)), ((GList *(*)(PurpleAccount *))((void *)0)), ((sizeof(PurplePluginProtocolInfo ))), (mxit_get_text_table), (mxit_media_initiate), (mxit_media_caps), (mxit_get_moods), ((void (*)(PurpleConnection *, const char *, PurpleSetPublicAliasSuccessCallback , PurpleSetPublicAliasFailureCallback ))((void *)0)), ((void (*)(PurpleConnection *, PurpleGetPublicAliasSuccessCallback , PurpleGetPublicAliasFailureCallback ))((void *)0)), (mxit_add_buddy), ((void (*)(PurpleConnection *, GList *, GList *, const char *))((void *)0))
/* list_icon */
/* list_emblem */
/* status_text */
/* tooltip_text */
/* status types				[roster.c] */
/* blist_node_menu */
/* chat_info				[multimx.c] */
/* chat_info_defaults */
/* login					[login.c] */
/* close */
/* send_im */
/* set_info */
/* send_typing */
/* get_info */
/* set_status */
/* set_idle */
/* change_passwd */
/* add_buddy				[roster.c] */
/* add_buddies */
/* remove_buddy				[roster.c] */
/* remove_buddies */
/* add_permit */
/* add_deny */
/* rem_permit */
/* rem_deny */
/* set_permit_deny */
/* join_chat				[multimx.c] */
/* reject chat invite		[multimx.c] */
/* get_chat_name			[multimx.c] */
/* chat_invite				[multimx.c] */
/* chat_leave				[multimx.c] */
/* chat_whisper */
/* chat_send				[multimx.c] */
/* keepalive */
/* register_user */
/* get_cb_info */
/* get_cb_away */
/* alias_buddy				[roster.c] */
/* group_buddy				[roster.c] */
/* rename_group				[roster.c] */
/* buddy_free */
/* convo_closed */
/* normalize */
/* set_buddy_icon */
/* remove_group */
// TODO: Add function to move all contacts out of this group (cmd=30 - remove group)?
/* get_cb_real_name */
/* set_chat_topic */
/* find_blist_chat */
/* roomlist_get_list */
/* roomlist_cancel */
/* roomlist_expand_category */
/* can_receive_file			[filexfer.c] */
/* send_file				[filexfer.c */
/* new_xfer					[filexfer.c] */
/* offline_message */
/* whiteboard_prpl_ops */
/* send_raw */
/* roomlist_room_serialize */
/* unregister_user */
/* send_attention */
/* attention_types */
/* struct_size */
/* get_account_text_table */
/* initiate_media */
/* get_media_caps */
/* get_moods */
/* set_public_alias */
/* get_public_alias */
/* add_buddy_with_invite */
/* add_buddies_with_invite */
};
static PurplePluginInfo plugin_info = {(5), (2), (10), (PURPLE_PLUGIN_PROTOCOL), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("prpl-loubserp-mxit"), ("MXit"), ("2.10.9"), ("MXit Protocol Plugin"), ("MXit"), ("Pieter Loubser <libpurple@mxit.com>"), ("http://www.mxit.com"), ((gboolean (*)(PurplePlugin *))((void *)0)), ((gboolean (*)(PurplePlugin *))((void *)0)), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((&proto_info)), ((PurplePluginUiInfo *)((void *)0)), (mxit_actions), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* purple magic, this must always be PURPLE_PLUGIN_MAGIC */
/* libpurple version */
/* libpurple version */
/* plugin type (connecting to another network) */
/* UI requirement (NULL for core plugin) */
/* plugin flags (zero is default) */
/* plugin dependencies (set this value to NULL no matter what) */
/* libpurple priority */
/* plugin id (must be unique) */
/* plugin name (this will be displayed in the UI) */
/* version of the plugin */
/* short summary of the plugin */
/* description of the plugin (can be long) */
/* plugin author name and email address */
/* plugin website (to find new versions and reporting of bugs) */
/* function pointer for loading the plugin */
/* function pointer for unloading the plugin */
/* function pointer for destroying the plugin */
/* pointer to an UI-specific struct */
/* pointer to either a PurplePluginLoaderInfo or PurplePluginProtocolInfo struct */
/* pointer to a PurplePluginUiInfo struct */
/* function pointer where you can define plugin-actions */
/* padding */
/* pointer reserved for future use */
/* pointer reserved for future use */
/* pointer reserved for future use */
/* pointer reserved for future use */
};
/*------------------------------------------------------------------------
 * Initialising the MXit plugin.
 *
 *  @param plugin	The plugin object
 */

static void init_plugin(PurplePlugin *plugin)
{
  PurpleAccountOption *option;
  purple_debug_info("prpl-loubserp-mxit","Loading MXit libPurple plugin...\n");
/* Configuration options */
/* WAP server (reference: "libpurple/accountopt.h") */
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","WAP Server"))),"wap_server","http://www.mxit.com");
  proto_info.protocol_options = g_list_append(proto_info.protocol_options,option);
  option = purple_account_option_bool_new(((const char *)(dgettext("pidgin","Connect via HTTP"))),"use_http",0);
  proto_info.protocol_options = g_list_append(proto_info.protocol_options,option);
  option = purple_account_option_bool_new(((const char *)(dgettext("pidgin","Enable splash-screen popup"))),"splashpopup",0);
  proto_info.protocol_options = g_list_append(proto_info.protocol_options,option);
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &plugin_info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
