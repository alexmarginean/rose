/**
 * @file gg.c Gadu-Gadu protocol plugin
 *
 * purple
 *
 * Copyright (C) 2005  Bartosz Oler <bartosz@bzimage.us>
 *
 * Some parts of the code are adapted or taken from the previous implementation
 * of this plugin written by Arkadiusz Miskiewicz <misiek@pld.org.pl>
 * Some parts Copyright (C) 2009  Krzysztof Klinikowski <grommasher@gmail.com>
 *
 * Thanks to Google's Summer of Code Program.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "plugin.h"
#include "version.h"
#include "notify.h"
#include "status.h"
#include "blist.h"
#include "accountopt.h"
#include "debug.h"
#include "util.h"
#include "request.h"
#include "xmlnode.h"
#include <libgadu.h>
#include "gg.h"
#include "confer.h"
#include "search.h"
#include "buddylist.h"
#include "gg-utils.h"
#define DISABLE_AVATARS 1
static PurplePlugin *my_protocol = (PurplePlugin *)((void *)0);
/* Prototypes */
static void ggp_set_status(PurpleAccount *account,PurpleStatus *status);
static int ggp_to_gg_status(PurpleStatus *status,char **msg);
/* ---------------------------------------------------------------------- */
/* ----- EXTERNAL CALLBACKS --------------------------------------------- */
/* ---------------------------------------------------------------------- */
/* ----- HELPERS -------------------------------------------------------- */

static PurpleInputCondition ggp_tcpsocket_inputcond_gg_to_purple(enum gg_check_t check)
{
  PurpleInputCondition cond = 0;
  if ((check & GG_CHECK_READ) != 0U) 
    cond |= PURPLE_INPUT_READ;
  if ((check & GG_CHECK_WRITE) != 0U) 
    cond |= PURPLE_INPUT_WRITE;
  return cond;
}
/**
 * Set up libgadu's proxy.
 *
 * @param account Account for which to set up the proxy.
 *
 * @return Zero if proxy setup is valid, otherwise -1.
 */

static int ggp_setup_proxy(PurpleAccount *account)
{
  PurpleProxyInfo *gpi;
  gpi = purple_proxy_get_setup(account);
  if (((purple_proxy_info_get_type(gpi)) != PURPLE_PROXY_NONE) && ((purple_proxy_info_get_host(gpi) == ((const char *)((void *)0))) || (purple_proxy_info_get_port(gpi) <= 0))) {
    gg_proxy_enabled = 0;
    purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Invalid proxy settings"))),((const char *)(dgettext("pidgin","Either the host name or port number specified for your given proxy type is invalid."))),0,0);
    return -1;
  }
  else if ((purple_proxy_info_get_type(gpi)) != PURPLE_PROXY_NONE) {
    gg_proxy_enabled = 1;
    gg_proxy_host = g_strdup(purple_proxy_info_get_host(gpi));
    gg_proxy_port = purple_proxy_info_get_port(gpi);
    gg_proxy_username = g_strdup(purple_proxy_info_get_username(gpi));
    gg_proxy_password = g_strdup(purple_proxy_info_get_password(gpi));
  }
  else {
    gg_proxy_enabled = 0;
  }
  return 0;
}
/* }}} */
/* ---------------------------------------------------------------------- */

static void ggp_callback_buddylist_save_ok(PurpleConnection *gc,const char *filename)
{
  PurpleAccount *account = purple_connection_get_account(gc);
  char *buddylist = ggp_buddylist_dump(account);
  purple_debug_info("gg","Saving...\n");
  purple_debug_info("gg","file = %s\n",filename);
  if (buddylist == ((char *)((void *)0))) {
    purple_notify_message(account,PURPLE_NOTIFY_MSG_INFO,((const char *)(dgettext("pidgin","Save Buddylist..."))),((const char *)(dgettext("pidgin","Your buddylist is empty, nothing was written to the file."))),0,0,0);
    return ;
  }
  if (purple_util_write_data_to_file_absolute(filename,buddylist,(-1)) != 0) {
    purple_notify_message(account,PURPLE_NOTIFY_MSG_INFO,((const char *)(dgettext("pidgin","Save Buddylist..."))),((const char *)(dgettext("pidgin","Buddylist saved successfully!"))),0,0,0);
  }
  else {
    gchar *primary = g_strdup_printf(((const char *)(dgettext("pidgin","Couldn\'t write buddy list for %s to %s"))),purple_account_get_username(account),filename);
    purple_notify_message(account,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Save Buddylist..."))),primary,0,0,0);
    g_free(primary);
  }
  g_free(buddylist);
}

static void ggp_callback_buddylist_load_ok(PurpleConnection *gc,gchar *file)
{
  PurpleAccount *account = purple_connection_get_account(gc);
  GError *error = (GError *)((void *)0);
  char *buddylist = (char *)((void *)0);
  gsize length;
  purple_debug_info("gg","file_name = %s\n",file);
  if (!(g_file_get_contents(file,&buddylist,&length,&error) != 0)) {
    purple_notify_message(account,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Couldn\'t load buddylist"))),((const char *)(dgettext("pidgin","Couldn\'t load buddylist"))),(error -> message),0,0);
    purple_debug_error("gg","Couldn\'t load buddylist. file = %s; error = %s\n",file,(error -> message));
    g_error_free(error);
    return ;
  }
  ggp_buddylist_load(gc,buddylist);
  g_free(buddylist);
  purple_notify_message(account,PURPLE_NOTIFY_MSG_INFO,((const char *)(dgettext("pidgin","Load Buddylist..."))),((const char *)(dgettext("pidgin","Buddylist loaded successfully!"))),0,0,0);
}
/* }}} */
/*
 */
/* static void ggp_action_buddylist_save(PurplePluginAction *action) {{{ */

static void ggp_action_buddylist_save(PurplePluginAction *action)
{
  PurpleConnection *gc = (PurpleConnection *)(action -> context);
  purple_request_file(action,((const char *)(dgettext("pidgin","Save buddylist..."))),0,(!0),((GCallback )ggp_callback_buddylist_save_ok),0,purple_connection_get_account(gc),0,0,gc);
}

static void ggp_action_buddylist_load(PurplePluginAction *action)
{
  PurpleConnection *gc = (PurpleConnection *)(action -> context);
  purple_request_file(action,((const char *)(dgettext("pidgin","Load buddylist from file..."))),0,0,((GCallback )ggp_callback_buddylist_load_ok),0,purple_connection_get_account(gc),0,0,gc);
}
/* ----- PUBLIC DIRECTORY SEARCH ---------------------------------------- */

static void ggp_callback_show_next(PurpleConnection *gc,GList *row,gpointer user_data)
{
  GGPInfo *info = (gc -> proto_data);
  GGPSearchForm *form = user_data;
  guint32 seq;
  form -> page_number++;
  ggp_search_remove((info -> searches),(form -> seq));
  purple_debug_info("gg","ggp_callback_show_next(): Removed seq %u\n",(form -> seq));
  seq = ggp_search_start(gc,form);
  ggp_search_add((info -> searches),seq,form);
  purple_debug_info("gg","ggp_callback_show_next(): Added seq %u\n",seq);
}

static void ggp_callback_add_buddy(PurpleConnection *gc,GList *row,gpointer user_data)
{
  purple_blist_request_add_buddy(purple_connection_get_account(gc),(g_list_nth_data(row,0)),0,0);
}

static void ggp_callback_im(PurpleConnection *gc,GList *row,gpointer user_data)
{
  PurpleAccount *account;
  PurpleConversation *conv;
  char *name;
  account = purple_connection_get_account(gc);
  name = (g_list_nth_data(row,0));
  conv = purple_conversation_new(PURPLE_CONV_TYPE_IM,account,name);
  purple_conversation_present(conv);
}

static void ggp_callback_find_buddies(PurpleConnection *gc,PurpleRequestFields *fields)
{
  GGPInfo *info = (gc -> proto_data);
  GGPSearchForm *form;
  guint32 seq;
  form = ggp_search_form_new(GGP_SEARCH_TYPE_FULL);
  form -> user_data = info;
  form -> lastname = g_strdup(purple_request_fields_get_string(fields,"lastname"));
  form -> firstname = g_strdup(purple_request_fields_get_string(fields,"firstname"));
  form -> nickname = g_strdup(purple_request_fields_get_string(fields,"nickname"));
  form -> city = g_strdup(purple_request_fields_get_string(fields,"city"));
  form -> birthyear = g_strdup(purple_request_fields_get_string(fields,"year"));
  switch(purple_request_fields_get_choice(fields,"gender")){
    case 1:
{
      form -> gender = g_strdup("2");
      break; 
    }
    case 2:
{
      form -> gender = g_strdup("1");
      break; 
    }
    default:
{
      form -> gender = ((char *)((void *)0));
      break; 
    }
  }
  form -> active = ((purple_request_fields_get_bool(fields,"active") != 0)?g_strdup("1") : ((char *)((void *)0)));
  seq = ggp_search_start(gc,form);
  ggp_search_add((info -> searches),seq,form);
  purple_debug_info("gg","ggp_callback_find_buddies(): Added seq %u\n",seq);
}

static void ggp_find_buddies(PurplePluginAction *action)
{
  PurpleConnection *gc = (PurpleConnection *)(action -> context);
  PurpleRequestFields *fields;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  fields = purple_request_fields_new();
  group = purple_request_field_group_new(0);
  purple_request_fields_add_group(fields,group);
  field = purple_request_field_string_new("lastname",((const char *)(dgettext("pidgin","Last name"))),0,0);
  purple_request_field_string_set_masked(field,0);
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_string_new("firstname",((const char *)(dgettext("pidgin","First name"))),0,0);
  purple_request_field_string_set_masked(field,0);
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_string_new("nickname",((const char *)(dgettext("pidgin","Nickname"))),0,0);
  purple_request_field_string_set_masked(field,0);
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_string_new("city",((const char *)(dgettext("pidgin","City"))),0,0);
  purple_request_field_string_set_masked(field,0);
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_string_new("year",((const char *)(dgettext("pidgin","Year of birth"))),0,0);
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_choice_new("gender",((const char *)(dgettext("pidgin","Gender"))),0);
  purple_request_field_choice_add(field,((const char *)(dgettext("pidgin","Male or female"))));
  purple_request_field_choice_add(field,((const char *)(dgettext("pidgin","Male"))));
  purple_request_field_choice_add(field,((const char *)(dgettext("pidgin","Female"))));
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_bool_new("active",((const char *)(dgettext("pidgin","Only online"))),0);
  purple_request_field_group_add_field(group,field);
  purple_request_fields(gc,((const char *)(dgettext("pidgin","Find buddies"))),((const char *)(dgettext("pidgin","Find buddies"))),((const char *)(dgettext("pidgin","Please, enter your search criteria below"))),fields,((const char *)(dgettext("pidgin","OK"))),((GCallback )ggp_callback_find_buddies),((const char *)(dgettext("pidgin","Cancel"))),0,purple_connection_get_account(gc),0,0,gc);
}
/* ----- CHANGE STATUS BROADCASTING ------------------------------------------------ */

static void ggp_action_change_status_broadcasting_ok(PurpleConnection *gc,PurpleRequestFields *fields)
{
  GGPInfo *info = (gc -> proto_data);
  int selected_field;
  PurpleAccount *account = purple_connection_get_account(gc);
  PurpleStatus *status;
  selected_field = purple_request_fields_get_choice(fields,"status_broadcasting");
  if (selected_field == 0) 
    info -> status_broadcasting = (!0);
  else 
    info -> status_broadcasting = 0;
  status = purple_account_get_active_status(account);
  ggp_set_status(account,status);
}

static void ggp_action_change_status_broadcasting(PurplePluginAction *action)
{
  PurpleConnection *gc = (PurpleConnection *)(action -> context);
  GGPInfo *info = (gc -> proto_data);
  PurpleRequestFields *fields;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  fields = purple_request_fields_new();
  group = purple_request_field_group_new(0);
  purple_request_fields_add_group(fields,group);
  field = purple_request_field_choice_new("status_broadcasting",((const char *)(dgettext("pidgin","Show status to:"))),0);
  purple_request_field_choice_add(field,((const char *)(dgettext("pidgin","All people"))));
  purple_request_field_choice_add(field,((const char *)(dgettext("pidgin","Only buddies"))));
  purple_request_field_group_add_field(group,field);
  if ((info -> status_broadcasting) != 0) 
    purple_request_field_choice_set_default_value(field,0);
  else 
    purple_request_field_choice_set_default_value(field,1);
  purple_request_fields(gc,((const char *)(dgettext("pidgin","Change status broadcasting"))),((const char *)(dgettext("pidgin","Change status broadcasting"))),((const char *)(dgettext("pidgin","Please, select who can see your status"))),fields,((const char *)(dgettext("pidgin","OK"))),((GCallback )ggp_action_change_status_broadcasting_ok),((const char *)(dgettext("pidgin","Cancel"))),0,purple_connection_get_account(gc),0,0,gc);
}
/* ----- CONFERENCES ---------------------------------------------------- */

static void ggp_callback_add_to_chat_ok(PurpleBuddy *buddy,PurpleRequestFields *fields)
{
  PurpleConnection *conn;
  PurpleRequestField *field;
  GList *sel;
  conn = purple_account_get_connection((purple_buddy_get_account(buddy)));
  do {
    if (conn != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conn != NULL");
      return ;
    };
  }while (0);
  field = purple_request_fields_get_field(fields,"name");
  sel = purple_request_field_list_get_selected(field);
  if (sel == ((GList *)((void *)0))) {
    purple_debug_error("gg","No chat selected\n");
    return ;
  }
  ggp_confer_participants_add_uin(conn,(sel -> data),ggp_str_to_uin(purple_buddy_get_name(buddy)));
}

static void ggp_bmenu_add_to_chat(PurpleBlistNode *node,gpointer ignored)
{
  PurpleBuddy *buddy;
  PurpleConnection *gc;
  GGPInfo *info;
  PurpleRequestFields *fields;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  GList *l;
  gchar *msg;
  buddy = ((PurpleBuddy *)node);
  gc = purple_account_get_connection((purple_buddy_get_account(buddy)));
  info = (gc -> proto_data);
  fields = purple_request_fields_new();
  group = purple_request_field_group_new(0);
  purple_request_fields_add_group(fields,group);
  field = purple_request_field_list_new("name","Chat name");
  for (l = (info -> chats); l != ((GList *)((void *)0)); l = (l -> next)) {
    GGPChat *chat = (l -> data);
    purple_request_field_list_add(field,(chat -> name),(chat -> name));
  }
  purple_request_field_group_add_field(group,field);
  msg = g_strdup_printf(((const char *)(dgettext("pidgin","Select a chat for buddy: %s"))),purple_buddy_get_alias(buddy));
  purple_request_fields(gc,((const char *)(dgettext("pidgin","Add to chat..."))),((const char *)(dgettext("pidgin","Add to chat..."))),msg,fields,((const char *)(dgettext("pidgin","Add"))),((GCallback )ggp_callback_add_to_chat_ok),((const char *)(dgettext("pidgin","Cancel"))),0,purple_connection_get_account(gc),0,0,buddy);
  g_free(msg);
}
/* ----- BLOCK BUDDIES -------------------------------------------------- */

static void ggp_add_deny(PurpleConnection *gc,const char *who)
{
  GGPInfo *info = (gc -> proto_data);
  uin_t uin = ggp_str_to_uin(who);
  purple_debug_info("gg","ggp_add_deny: %u\n",uin);
  gg_remove_notify_ex((info -> session),uin,3);
  gg_add_notify_ex((info -> session),uin,4);
}

static void ggp_rem_deny(PurpleConnection *gc,const char *who)
{
  GGPInfo *info = (gc -> proto_data);
  uin_t uin = ggp_str_to_uin(who);
  purple_debug_info("gg","ggp_rem_deny: %u\n",uin);
  gg_remove_notify_ex((info -> session),uin,4);
  gg_add_notify_ex((info -> session),uin,3);
}
/* ---------------------------------------------------------------------- */
/* ----- INTERNAL CALLBACKS --------------------------------------------- */
/* ---------------------------------------------------------------------- */
#if !DISABLE_AVATARS
/* FIXME: This shouldn't be necessary */
#endif
/**
 * Try to update avatar of the buddy.
 *
 * @param gc     PurpleConnection
 * @param uin    UIN of the buddy.
 */

static void ggp_update_buddy_avatar(PurpleConnection *gc,uin_t uin)
{
#if DISABLE_AVATARS
  purple_debug_warning("gg","ggp_update_buddy_avatar: disabled, please update to 3.0.0, when available\n");
#else
#endif
}
/**
 * Handle change of the status of the buddy.
 *
 * @param gc     PurpleConnection
 * @param uin    UIN of the buddy.
 * @param status ID of the status.
 * @param descr  Description.
 */

static void ggp_generic_status_handler(PurpleConnection *gc,uin_t uin,int status,const char *descr)
{
  gchar *from;
  const char *st;
  char *status_msg = (char *)((void *)0);
  ggp_update_buddy_avatar(gc,uin);
  from = g_strdup_printf("%u",uin);
  switch(status){
    case 0x0001:
{
    }
    case 0x0015:
{
      st = purple_primitive_get_id_from_type(PURPLE_STATUS_OFFLINE);
      break; 
    }
    case 0x0017:
{
    }
    case 0x0018:
{
      st = purple_primitive_get_id_from_type(PURPLE_STATUS_AVAILABLE);
      break; 
    }
    case 0x0002:
{
    }
    case 0x0004:
{
      st = purple_primitive_get_id_from_type(PURPLE_STATUS_AVAILABLE);
      break; 
    }
    case 0x0003:
{
    }
    case 0x0005:
{
      st = purple_primitive_get_id_from_type(PURPLE_STATUS_AWAY);
      break; 
    }
    case 0x0021:
{
    }
    case 0x0022:
{
      st = purple_primitive_get_id_from_type(PURPLE_STATUS_UNAVAILABLE);
      break; 
    }
    case 0x0006:
{
/* user is blocking us.... */
      st = "blocked";
      break; 
    }
    default:
{
      st = purple_primitive_get_id_from_type(PURPLE_STATUS_AVAILABLE);
      purple_debug_info("gg","GG_EVENT_NOTIFY: Unknown status: %d\n",status);
      break; 
    }
  }
  if (descr != ((const char *)((void *)0))) {
    status_msg = g_strdup(descr);
    g_strchomp(g_strchug(status_msg));
    if (status_msg[0] == 0) {
      g_free(status_msg);
      status_msg = ((char *)((void *)0));
    }
  }
  purple_debug_info("gg","status of %u is %s [%s]\n",uin,st,((status_msg != 0)?status_msg : ""));
  if (status_msg == ((char *)((void *)0))) {
    purple_prpl_got_user_status(purple_connection_get_account(gc),from,st,((void *)((void *)0)));
  }
  else {
    purple_prpl_got_user_status(purple_connection_get_account(gc),from,st,"message",status_msg,((void *)((void *)0)));
    g_free(status_msg);
  }
  g_free(from);
}

static void ggp_sr_close_cb(gpointer user_data)
{
  GGPSearchForm *form = user_data;
  GGPInfo *info = (form -> user_data);
  ggp_search_remove((info -> searches),(form -> seq));
  purple_debug_info("gg","ggp_sr_close_cb(): Removed seq %u\n",(form -> seq));
  ggp_search_form_destroy(form);
}
/**
 * Translate a status' ID to a more user-friendly name.
 *
 * @param id The ID of the status.
 *
 * @return The user-friendly name of the status.
 */

static const char *ggp_status_by_id(unsigned int id)
{
  const char *st;
  purple_debug_info("gg","ggp_status_by_id: %d\n",id);
  switch(id){
    case 1:
{
    }
    case 21:
{
      st = ((const char *)(dgettext("pidgin","Offline")));
      break; 
    }
    case 2:
{
    }
    case 4:
{
      st = ((const char *)(dgettext("pidgin","Available")));
      break; 
    }
    case 23:
{
    }
    case 24:
{
      return (const char *)(dgettext("pidgin","Chatty"));
    }
    case 0x0021:
{
    }
    case 0x0022:
{
      return (const char *)(dgettext("pidgin","Do Not Disturb"));
    }
    case 3:
{
    }
    case 5:
{
      st = ((const char *)(dgettext("pidgin","Away")));
      break; 
    }
    default:
{
      st = ((const char *)(dgettext("pidgin","Unknown")));
      break; 
    }
  }
  return st;
}

static void ggp_pubdir_handle_info(PurpleConnection *gc,gg_pubdir50_t req,GGPSearchForm *form)
{
  PurpleNotifyUserInfo *user_info;
  PurpleBuddy *buddy;
  char *val;
  char *who;
  user_info = purple_notify_user_info_new();
  val = ggp_search_get_result(req,0,"FmStatus");
/* XXX: Use of ggp_str_to_uin() is an ugly hack! */
  purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Status"))),ggp_status_by_id(ggp_str_to_uin(val)));
  g_free(val);
  who = ggp_search_get_result(req,0,"FmNumber");
  purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","UIN"))),who);
  val = ggp_search_get_result(req,0,"firstname");
  purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","First Name"))),val);
  g_free(val);
  val = ggp_search_get_result(req,0,"nickname");
  purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Nickname"))),val);
  g_free(val);
  val = ggp_search_get_result(req,0,"city");
  purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","City"))),val);
  g_free(val);
  val = ggp_search_get_result(req,0,"birthyear");
  if (strncmp(val,"0",1) != 0) {
    purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Birth Year"))),val);
  }
  g_free(val);
/*
	 * Include a status message, if exists and buddy is in the blist.
	 */
  buddy = purple_find_buddy(purple_connection_get_account(gc),who);
  if (((PurpleBuddy *)((void *)0)) != buddy) {
    PurpleStatus *status;
    const char *msg;
    char *text;
    status = purple_presence_get_active_status((purple_buddy_get_presence(buddy)));
    msg = purple_status_get_attr_string(status,"message");
    if (msg != ((const char *)((void *)0))) {
      text = g_markup_escape_text(msg,(-1));
      purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Message"))),text);
      g_free(text);
    }
  }
  purple_notify_userinfo(gc,who,user_info,ggp_sr_close_cb,form);
  g_free(who);
  purple_notify_user_info_destroy(user_info);
}

static void ggp_pubdir_handle_full(PurpleConnection *gc,gg_pubdir50_t req,GGPSearchForm *form)
{
  PurpleNotifySearchResults *results;
  PurpleNotifySearchColumn *column;
  int res_count;
  int start;
  int i;
  do {
    if (form != ((GGPSearchForm *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"form != NULL");
      return ;
    };
  }while (0);
  res_count = gg_pubdir50_count(req);
  res_count = ((res_count > 20)?20 : res_count);
  if ((form -> page_size) == 0) 
    form -> page_size = res_count;
  results = purple_notify_searchresults_new();
  if (results == ((PurpleNotifySearchResults *)((void *)0))) {
    purple_debug_error("gg","ggp_pubdir_reply_handler: Unable to display the search results.\n");
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Unable to display the search results."))),0,0,0);
    if ((form -> window) == ((void *)((void *)0))) 
      ggp_sr_close_cb(form);
    return ;
  }
  column = purple_notify_searchresults_column_new(((const char *)(dgettext("pidgin","UIN"))));
  purple_notify_searchresults_column_add(results,column);
  column = purple_notify_searchresults_column_new(((const char *)(dgettext("pidgin","First Name"))));
  purple_notify_searchresults_column_add(results,column);
  column = purple_notify_searchresults_column_new(((const char *)(dgettext("pidgin","Nickname"))));
  purple_notify_searchresults_column_add(results,column);
  column = purple_notify_searchresults_column_new(((const char *)(dgettext("pidgin","City"))));
  purple_notify_searchresults_column_add(results,column);
  column = purple_notify_searchresults_column_new(((const char *)(dgettext("pidgin","Birth Year"))));
  purple_notify_searchresults_column_add(results,column);
  purple_debug_info("gg","Going with %d entries\n",res_count);
  start = ((int )(ggp_str_to_uin(gg_pubdir50_get(req,0,"fmstart"))));
  purple_debug_info("gg","start = %d\n",start);
  for (i = 0; i < res_count; i++) {
    GList *row = (GList *)((void *)0);
    char *birth = ggp_search_get_result(req,i,"birthyear");
/* TODO: Status will be displayed as an icon. */
/* row = g_list_append(row, ggp_search_get_result(req, i, GG_PUBDIR50_STATUS)); */
    row = g_list_append(row,(ggp_search_get_result(req,i,"FmNumber")));
    row = g_list_append(row,(ggp_search_get_result(req,i,"firstname")));
    row = g_list_append(row,(ggp_search_get_result(req,i,"nickname")));
    row = g_list_append(row,(ggp_search_get_result(req,i,"city")));
    row = g_list_append(row,((((birth != 0) && (strncmp(birth,"0",1) != 0))?birth : g_strdup("-"))));
    purple_notify_searchresults_row_add(results,row);
  }
  purple_notify_searchresults_button_add(results,PURPLE_NOTIFY_BUTTON_CONTINUE,ggp_callback_show_next);
  purple_notify_searchresults_button_add(results,PURPLE_NOTIFY_BUTTON_ADD,ggp_callback_add_buddy);
  purple_notify_searchresults_button_add(results,PURPLE_NOTIFY_BUTTON_IM,ggp_callback_im);
  if ((form -> window) == ((void *)((void *)0))) {
    void *h = purple_notify_searchresults(gc,((const char *)(dgettext("pidgin","Gadu-Gadu Public Directory"))),((const char *)(dgettext("pidgin","Search results"))),0,results,((PurpleNotifyCloseCallback )ggp_sr_close_cb),form);
    if (h == ((void *)((void *)0))) {
      purple_debug_error("gg","ggp_pubdir_reply_handler: Unable to display the search results.\n");
      purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Unable to display the search results."))),0,0,0);
      return ;
    }
    form -> window = h;
  }
  else {
    purple_notify_searchresults_new_rows(gc,results,(form -> window));
  }
}

static void ggp_pubdir_reply_handler(PurpleConnection *gc,gg_pubdir50_t req)
{
  GGPInfo *info = (gc -> proto_data);
  GGPSearchForm *form;
  int res_count;
  guint32 seq;
  seq = gg_pubdir50_seq(req);
  form = ggp_search_get((info -> searches),seq);
  purple_debug_info("gg","ggp_pubdir_reply_handler(): seq %u --> form %p\n",seq,form);
/*
	 * this can happen when user will request more results
	 * and close the results window before they arrive.
	 */
  do {
    if (form != ((GGPSearchForm *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"form != NULL");
      return ;
    };
  }while (0);
  res_count = gg_pubdir50_count(req);
  if (res_count < 1) {
    purple_debug_info("gg","GG_EVENT_PUBDIR50_SEARCH_REPLY: Nothing found\n");
    purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","No matching users found"))),((const char *)(dgettext("pidgin","There are no users matching your search criteria."))),0,0);
    if ((form -> window) == ((void *)((void *)0))) 
      ggp_sr_close_cb(form);
    return ;
  }
  switch(form -> search_type){
    case GGP_SEARCH_TYPE_INFO:
{
      ggp_pubdir_handle_info(gc,req,form);
      break; 
    }
    case GGP_SEARCH_TYPE_FULL:
{
      ggp_pubdir_handle_full(gc,req,form);
      break; 
    }
    default:
{
      purple_debug_warning("gg","Unknown search_type!\n");
      break; 
    }
  }
}

static void ggp_recv_image_handler(PurpleConnection *gc,const struct gg_event *ev)
{
  gint imgid = 0;
  GGPInfo *info = (gc -> proto_data);
  GList *entry = g_list_first((info -> pending_richtext_messages));
  gchar *handlerid = g_strdup_printf("IMGID_HANDLER-%i",ev -> event.image_reply.crc32);
  imgid = purple_imgstore_add_with_id(g_memdup(ev -> event.image_reply.image,ev -> event.image_reply.size),ev -> event.image_reply.size,ev -> event.image_reply.filename);
  purple_debug_info("gg","ggp_recv_image_handler: got image with crc32: %u\n",ev -> event.image_reply.crc32);
{
    while(entry != 0){
      if (strstr(((gchar *)(entry -> data)),handlerid) != ((char *)((void *)0))) {
        gchar **split = g_strsplit(((gchar *)(entry -> data)),handlerid,3);
        gchar *text = g_strdup_printf("%s%i%s",split[0],imgid,split[1]);
        purple_debug_info("gg","ggp_recv_image_handler: found message matching crc32: %s\n",((gchar *)(entry -> data)));
        g_strfreev(split);
        info -> pending_richtext_messages = g_list_remove((info -> pending_richtext_messages),(entry -> data));
/* We don't have any more images to download */
        if (strstr(text,"<IMG ID=\"IMGID_HANDLER") == ((char *)((void *)0))) {
          gchar *buf = g_strdup_printf("%lu",((unsigned long )ev -> event.image_reply.sender));
          serv_got_im(gc,buf,text,PURPLE_MESSAGE_IMAGES,time(0));
          g_free(buf);
          purple_debug_info("gg","ggp_recv_image_handler: richtext message: %s\n",text);
          g_free(text);
          break; 
        }
        info -> pending_richtext_messages = g_list_append((info -> pending_richtext_messages),text);
        break; 
      }
      entry = ((entry != 0)?( *((GList *)entry)).next : ((struct _GList *)((void *)0)));
    }
  }
  g_free(handlerid);
}
/**
 * Dispatch a message received from a buddy.
 *
 * @param gc PurpleConnection.
 * @param ev Gadu-Gadu event structure.
 *
 * Image receiving, some code borrowed from Kadu http://www.kadu.net
 */

static void ggp_recv_message_handler(PurpleConnection *gc,const struct gg_event *ev)
{
  GGPInfo *info = (gc -> proto_data);
  PurpleConversation *conv;
  gchar *from;
  gchar *msg;
  gchar *tmp;
  if (ev -> event.msg.message == ((unsigned char *)((void *)0))) {
    purple_debug_warning("gg","ggp_recv_message_handler: NULL as message pointer\n");
    return ;
  }
  from = g_strdup_printf("%lu",((unsigned long )ev -> event.msg.sender));
/*
	tmp = charset_convert((const char *)ev->event.msg.message,
			      "CP1250", "UTF-8");
	*/
  tmp = g_strdup_printf("%s",ev -> event.msg.message);
  purple_str_strip_char(tmp,13);
  msg = g_markup_escape_text(tmp,(-1));
  g_free(tmp);
/* We got richtext message */
  if (ev -> event.msg.formats_length != 0) {
    gboolean got_image = 0;
    gboolean bold = 0;
    gboolean italic = 0;
    gboolean under = 0;
    char *cformats = (char *)ev -> event.msg.formats;
    char *cformats_end = (cformats + ev -> event.msg.formats_length);
    gint increased_len = 0;
    struct gg_msg_richtext_format *actformat;
    struct gg_msg_richtext_image *actimage;
    GString *message = g_string_new(msg);
    gchar *handlerid;
    purple_debug_info("gg","ggp_recv_message_handler: richtext msg from (%s): %s %i formats\n",from,msg,ev -> event.msg.formats_length);
    while(cformats < cformats_end){{
        gint byteoffset;
        actformat = ((struct gg_msg_richtext_format *)cformats);
        cformats += sizeof(struct gg_msg_richtext_format );
        byteoffset = (g_utf8_offset_to_pointer((message -> str),((actformat -> position) + increased_len)) - (message -> str));
        if (((actformat -> position) == 0) && ((actformat -> font) == 0)) {
          purple_debug_warning("gg","ggp_recv_message_handler: bogus formatting (inc: %i)\n",increased_len);
          continue; 
        }
        purple_debug_info("gg","ggp_recv_message_handler: format at pos: %i, image:%i, bold:%i, italic: %i, under:%i (inc: %i)\n",(actformat -> position),(((actformat -> font) & 128) != 0),(((actformat -> font) & 1) != 0),(((actformat -> font) & 2) != 0),(((actformat -> font) & 4) != 0),increased_len);
        if (((actformat -> font) & 128) != 0) {
          got_image = (!0);
          actimage = ((struct gg_msg_richtext_image *)cformats);
          cformats += sizeof(struct gg_msg_richtext_image );
          purple_debug_info("gg","ggp_recv_message_handler: image received, size: %d, crc32: %i\n",(actimage -> size),(actimage -> crc32));
/* Checking for errors, image size shouldn't be
				 * larger than 255.000 bytes */
          if ((actimage -> size) > 255000) {
            purple_debug_warning("gg","ggp_recv_message_handler: received image large than 255 kb\n");
            continue; 
          }
          gg_image_request((info -> session),ev -> event.msg.sender,(actimage -> size),(actimage -> crc32));
          handlerid = g_strdup_printf("<IMG ID=\"IMGID_HANDLER-%i\">",(actimage -> crc32));
          g_string_insert(message,byteoffset,handlerid);
          increased_len += strlen(handlerid);
          g_free(handlerid);
          continue; 
        }
        if (((actformat -> font) & 1) != 0) {
          if (bold == 0) {
            g_string_insert(message,byteoffset,"<b>");
            increased_len += 3;
            bold = (!0);
          }
        }
        else if (bold != 0) {
          g_string_insert(message,byteoffset,"</b>");
          increased_len += 4;
          bold = 0;
        }
        if (((actformat -> font) & 2) != 0) {
          if (italic == 0) {
            g_string_insert(message,byteoffset,"<i>");
            increased_len += 3;
            italic = (!0);
          }
        }
        else if (italic != 0) {
          g_string_insert(message,byteoffset,"</i>");
          increased_len += 4;
          italic = 0;
        }
        if (((actformat -> font) & 4) != 0) {
          if (under == 0) {
            g_string_insert(message,byteoffset,"<u>");
            increased_len += 3;
            under = (!0);
          }
        }
        else if (under != 0) {
          g_string_insert(message,byteoffset,"</u>");
          increased_len += 4;
          under = 0;
        }
        if (((actformat -> font) & 8) != 0) {
          cformats += sizeof(struct gg_msg_richtext_color );
        }
      }
    }
    msg = (message -> str);
    g_string_free(message,0);
    if (got_image != 0) {
      info -> pending_richtext_messages = g_list_append((info -> pending_richtext_messages),msg);
      return ;
    }
  }
  purple_debug_info("gg","ggp_recv_message_handler: msg from (%s): %s (class = %d; rcpt_count = %d)\n",from,msg,ev -> event.msg.msgclass,ev -> event.msg.recipients_count);
  if (ev -> event.msg.recipients_count == 0) {
    serv_got_im(gc,from,msg,0,ev -> event.msg.time);
  }
  else {
    const char *chat_name;
    int chat_id;
    char *buddy_name;
    chat_name = ggp_confer_find_by_participants(gc,ev -> event.msg.recipients,ev -> event.msg.recipients_count);
    if (chat_name == ((const char *)((void *)0))) {
      chat_name = ggp_confer_add_new(gc,0);
      serv_got_joined_chat(gc,(info -> chats_count),chat_name);
      ggp_confer_participants_add_uin(gc,chat_name,ev -> event.msg.sender);
      ggp_confer_participants_add(gc,chat_name,ev -> event.msg.recipients,ev -> event.msg.recipients_count);
    }
    conv = ggp_confer_find_by_name(gc,chat_name);
    chat_id = purple_conv_chat_get_id((purple_conversation_get_chat_data(conv)));
    buddy_name = ggp_buddy_get_name(gc,ev -> event.msg.sender);
    serv_got_chat_in(gc,chat_id,buddy_name,PURPLE_MESSAGE_RECV,msg,ev -> event.msg.time);
    g_free(buddy_name);
  }
  g_free(msg);
  g_free(from);
}

static void ggp_send_image_handler(PurpleConnection *gc,const struct gg_event *ev)
{
  GGPInfo *info = (gc -> proto_data);
  PurpleStoredImage *image;
  gint imgid = (gint )((glong )(g_hash_table_lookup((info -> pending_images),((gpointer )((glong )ev -> event.image_request.crc32)))));
  purple_debug_info("gg","ggp_send_image_handler: image request received, crc32: %u, imgid: %d\n",ev -> event.image_request.crc32,imgid);
  if (imgid != 0) {
    if ((image = purple_imgstore_find_by_id(imgid)) != 0) {
      gint image_size = (purple_imgstore_get_size(image));
      gconstpointer image_bin = purple_imgstore_get_data(image);
      const char *image_filename = purple_imgstore_get_filename(image);
      purple_debug_info("gg","ggp_send_image_handler: sending image imgid: %i, crc: %u\n",imgid,ev -> event.image_request.crc32);
      gg_image_reply((info -> session),((unsigned long )ev -> event.image_request.sender),image_filename,image_bin,image_size);
      purple_imgstore_unref(image);
    }
    else {
      purple_debug_error("gg","ggp_send_image_handler: image imgid: %i, crc: %u in hash but not found in imgstore!\n",imgid,ev -> event.image_request.crc32);
    }
    g_hash_table_remove((info -> pending_images),((gpointer )((glong )ev -> event.image_request.crc32)));
  }
}

static void ggp_typing_notification_handler(PurpleConnection *gc,uin_t uin,int length)
{
  gchar *from;
  from = g_strdup_printf("%u",uin);
  if (length != 0) 
    serv_got_typing(gc,from,0,PURPLE_TYPING);
  else 
    serv_got_typing_stopped(gc,from);
  g_free(from);
}
/**
 * Handling of XML events.
 *
 * @param gc PurpleConnection.
 * @param data Raw XML contents.
 *
 * @see http://toxygen.net/libgadu/protocol/#ch1.13
 */

static void ggp_xml_event_handler(PurpleConnection *gc,char *data)
{
  xmlnode *xml = (xmlnode *)((void *)0);
  xmlnode *xmlnode_next_event;
  xml = xmlnode_from_str(data,(-1));
  if (xml == ((xmlnode *)((void *)0))) 
    goto out;
  xmlnode_next_event = xmlnode_get_child(xml,"event");
  while(xmlnode_next_event != ((xmlnode *)((void *)0))){{
      xmlnode *xmlnode_current_event = xmlnode_next_event;
      xmlnode *xmlnode_type;
      char *event_type_raw;
      int event_type = 0;
      xmlnode *xmlnode_sender;
      char *event_sender_raw;
      uin_t event_sender = 0;
      xmlnode_next_event = xmlnode_get_next_twin(xmlnode_next_event);
      xmlnode_type = xmlnode_get_child(xmlnode_current_event,"type");
      if (xmlnode_type == ((xmlnode *)((void *)0))) 
        continue; 
      event_type_raw = xmlnode_get_data(xmlnode_type);
      if (event_type_raw != ((char *)((void *)0))) 
        event_type = atoi(event_type_raw);
      g_free(event_type_raw);
      xmlnode_sender = xmlnode_get_child(xmlnode_current_event,"sender");
      if (xmlnode_sender != ((xmlnode *)((void *)0))) {
        event_sender_raw = xmlnode_get_data(xmlnode_sender);
        if (event_sender_raw != ((char *)((void *)0))) 
          event_sender = ggp_str_to_uin(event_sender_raw);
        g_free(event_sender_raw);
      }
      switch(event_type){
/* avatar update */
        case 28:
{
          purple_debug_info("gg","ggp_xml_event_handler: avatar updated (uid: %u)\n",event_sender);
          ggp_update_buddy_avatar(gc,event_sender);
          break; 
        }
        default:
{
          purple_debug_error("gg","ggp_xml_event_handler: unsupported event type=%d from=%u\n",event_type,event_sender);
        }
      }
    }
  }
  out:
  if (xml != 0) 
    xmlnode_free(xml);
}

static void ggp_callback_recv(gpointer _gc,gint fd,PurpleInputCondition cond)
{
  PurpleConnection *gc = _gc;
  GGPInfo *info = (gc -> proto_data);
  struct gg_event *ev;
  int i;
  if (!((ev = gg_watch_fd((info -> session))) != 0)) {
    purple_debug_error("gg","ggp_callback_recv: gg_watch_fd failed -- CRITICAL!\n");
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Unable to read from socket"))));
    return ;
  }
  purple_input_remove((gc -> inpa));
  gc -> inpa = (purple_input_add(( *(info -> session)).fd,ggp_tcpsocket_inputcond_gg_to_purple(( *(info -> session)).check),ggp_callback_recv,gc));
  switch(ev -> type){
    case 0:
{
/* Nothing happened. */
      break; 
    }
    case 1:
{
      ggp_recv_message_handler(gc,ev);
      break; 
    }
    case 5:
{
/* Changing %u to %i fixes compiler warning */
      purple_debug_info("gg","ggp_callback_recv: message sent to: %i, delivery status=%d, seq=%d\n",ev -> event.ack.recipient,ev -> event.ack.status,ev -> event.ack.seq);
      break; 
    }
    case 26:
{
      ggp_recv_image_handler(gc,ev);
      break; 
    }
    case 25:
{
      ggp_send_image_handler(gc,ev);
      break; 
    }
    case 2:
{
    }
    case 3:
{
{
        struct gg_notify_reply *n;
        char *descr;
        purple_debug_info("gg","notify_pre: (%d) status: %d\n",( *ev -> event.notify).uin,(( *ev -> event.notify).status & 255));
        n = (((ev -> type) == GG_EVENT_NOTIFY)?ev -> event.notify : ev -> event.notify_descr.notify);
        for (; (n -> uin) != 0U; n++) {
          descr = (((ev -> type) == GG_EVENT_NOTIFY)?((char *)((void *)0)) : ev -> event.notify_descr.descr);
          purple_debug_info("gg","notify: (%d) status: %d; descr: %s\n",(n -> uin),((n -> status) & 255),((descr != 0)?descr : "(null)"));
          ggp_generic_status_handler(gc,(n -> uin),((n -> status) & 255),descr);
        }
      }
      break; 
    }
    case 23:
{
      for (i = 0; ev -> event.notify60[i].uin != 0U; i++) {
        purple_debug_info("gg","notify60: (%d) status=%d; version=%d; descr=%s\n",ev -> event.notify60[i].uin,(ev -> event.notify60[i].status & 255),ev -> event.notify60[i].version,((ev -> event.notify60[i].descr != 0)?ev -> event.notify60[i].descr : "(null)"));
        ggp_generic_status_handler(gc,ev -> event.notify60[i].uin,(ev -> event.notify60[i].status & 255),ev -> event.notify60[i].descr);
      }
      break; 
    }
    case 4:
{
      purple_debug_info("gg","status: (%d) status=%d; descr=%s\n",ev -> event.status.uin,(ev -> event.status.status & 255),((ev -> event.status.descr != 0)?ev -> event.status.descr : "(null)"));
      ggp_generic_status_handler(gc,ev -> event.status.uin,(ev -> event.status.status & 255),ev -> event.status.descr);
      break; 
    }
    case 22:
{
      purple_debug_info("gg","status60: (%d) status=%d; version=%d; descr=%s\n",ev -> event.status60.uin,(ev -> event.status60.status & 255),ev -> event.status60.version,((ev -> event.status60.descr != 0)?ev -> event.status60.descr : "(null)"));
      ggp_generic_status_handler(gc,ev -> event.status60.uin,(ev -> event.status60.status & 255),ev -> event.status60.descr);
      break; 
    }
    case 19:
{
      ggp_pubdir_reply_handler(gc,ev -> event.pubdir50);
      break; 
    }
    case 37:
{
      ggp_typing_notification_handler(gc,ev -> event.typing_notification.uin,ev -> event.typing_notification.length);
      break; 
    }
    case 35:
{
      purple_debug_info("gg","GG_EVENT_XML_EVENT\n");
      ggp_xml_event_handler(gc,ev -> event.xml_event.data);
      break; 
    }
    default:
{
      purple_debug_error("gg","unsupported event type=%d\n",(ev -> type));
      break; 
    }
  }
  gg_event_free(ev);
}

static void ggp_async_login_handler(gpointer _gc,gint fd,PurpleInputCondition cond)
{
  PurpleConnection *gc = _gc;
  GGPInfo *info;
  struct gg_event *ev;
  do {
    if (g_list_find(purple_connections_get_all(),gc) != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_CONNECTION_IS_VALID(gc)");
      return ;
    };
  }while (0);
  info = (gc -> proto_data);
  purple_debug_info("gg","login_handler: session: check = %d; state = %d;\n",( *(info -> session)).check,( *(info -> session)).state);
  switch(( *(info -> session)).state){
    case 1:
{
      purple_debug_info("gg","GG_STATE_RESOLVING\n");
      break; 
    }
    case 43:
{
      purple_debug_info("gg","GG_STATE_RESOLVING_GG\n");
      break; 
    }
    case 5:
{
      purple_debug_info("gg","GG_STATE_CONNECTING_HUB\n");
      break; 
    }
    case 3:
{
      purple_debug_info("gg","GG_STATE_READING_DATA\n");
      break; 
    }
    case 6:
{
      purple_debug_info("gg","GG_STATE_CONNECTING_GG\n");
      break; 
    }
    case 7:
{
      purple_debug_info("gg","GG_STATE_READING_KEY\n");
      break; 
    }
    case 8:
{
      purple_debug_info("gg","GG_STATE_READING_REPLY\n");
      break; 
    }
    case 37:
{
      purple_debug_info("gg","GG_STATE_TLS_NEGOTIATION\n");
      break; 
    }
    default:
{
      purple_debug_error("gg","unknown state = %d\n",( *(info -> session)).state);
      break; 
    }
  }
  if (!((ev = gg_watch_fd((info -> session))) != 0)) {
    purple_debug_error("gg","login_handler: gg_watch_fd failed!\n");
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Unable to read from socket"))));
    return ;
  }
  purple_debug_info("gg","login_handler: session->fd = %d\n",( *(info -> session)).fd);
  purple_debug_info("gg","login_handler: session: check = %d; state = %d;\n",( *(info -> session)).check,( *(info -> session)).state);
  purple_input_remove((gc -> inpa));
/** XXX I think that this shouldn't be done if ev->type is GG_EVENT_CONN_FAILED or GG_EVENT_CONN_SUCCESS -datallah */
  if (( *(info -> session)).fd >= 0) 
    gc -> inpa = (purple_input_add(( *(info -> session)).fd,ggp_tcpsocket_inputcond_gg_to_purple(( *(info -> session)).check),ggp_async_login_handler,gc));
  switch(ev -> type){
    case 0:
{
/* Nothing happened. */
      purple_debug_info("gg","GG_EVENT_NONE\n");
      break; 
    }
    case 8:
{
{
        purple_debug_info("gg","GG_EVENT_CONN_SUCCESS\n");
        purple_input_remove((gc -> inpa));
        gc -> inpa = (purple_input_add(( *(info -> session)).fd,ggp_tcpsocket_inputcond_gg_to_purple(( *(info -> session)).check),ggp_callback_recv,gc));
        ggp_buddylist_send(gc);
        purple_connection_update_progress(gc,((const char *)(dgettext("pidgin","Connected"))),1,2);
        purple_connection_set_state(gc,PURPLE_CONNECTED);
      }
      break; 
    }
    case 7:
{
      purple_input_remove((gc -> inpa));
      gc -> inpa = 0;
      purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Connection failed"))));
      break; 
    }
    case 1:
{
      if (ev -> event.msg.sender == 0) 
/* system messages are mostly ads */
        purple_debug_info("gg","System message:\n%s\n",ev -> event.msg.message);
      else 
        purple_debug_warning("gg","GG_EVENT_MSG: message from user %u unexpected while connecting:\n%s\n",ev -> event.msg.sender,ev -> event.msg.message);
      break; 
    }
    default:
{
      purple_debug_error("gg","strange event: %d\n",(ev -> type));
      break; 
    }
  }
  gg_event_free(ev);
}
/* ---------------------------------------------------------------------- */
/* ----- PurplePluginProtocolInfo ----------------------------------------- */
/* ---------------------------------------------------------------------- */

static const char *ggp_list_icon(PurpleAccount *account,PurpleBuddy *buddy)
{
  return "gadu-gadu";
}

static char *ggp_status_text(PurpleBuddy *b)
{
  PurpleStatus *status;
  const char *msg;
  char *text;
  char *tmp;
  status = purple_presence_get_active_status((purple_buddy_get_presence(b)));
  msg = purple_status_get_attr_string(status,"message");
  if (msg == ((const char *)((void *)0))) 
    return 0;
  tmp = purple_markup_strip_html(msg);
  text = g_markup_escape_text(tmp,(-1));
  g_free(tmp);
  return text;
}

static void ggp_tooltip_text(PurpleBuddy *b,PurpleNotifyUserInfo *user_info,gboolean full)
{
  PurpleStatus *status;
  char *text;
  char *tmp;
  const char *msg;
  const char *name;
  const char *alias;
  do {
    if (b != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"b != NULL");
      return ;
    };
  }while (0);
  status = purple_presence_get_active_status((purple_buddy_get_presence(b)));
  msg = purple_status_get_attr_string(status,"message");
  name = purple_status_get_name(status);
  alias = purple_buddy_get_alias(b);
  purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Alias"))),alias);
  if (msg != ((const char *)((void *)0))) {
    text = g_markup_escape_text(msg,(-1));
    if (((b != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(b))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(b))) != 0)) {
      tmp = g_strdup_printf("%s: %s",name,text);
      purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Status"))),tmp);
      g_free(tmp);
    }
    else {
      purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Message"))),text);
    }
    g_free(text);
/* We don't want to duplicate 'Status: Offline'. */
  }
  else if (((b != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(b))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(b))) != 0)) {
    purple_notify_user_info_add_pair(user_info,((const char *)(dgettext("pidgin","Status"))),name);
  }
}

static GList *ggp_status_types(PurpleAccount *account)
{
  PurpleStatusType *type;
  GList *types = (GList *)((void *)0);
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_AVAILABLE,0,0,(!0),(!0),0,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  types = g_list_append(types,type);
/*
	 * Without this selecting Invisible as own status doesn't
	 * work. It's not used and not needed to show status of buddies.
	 */
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_INVISIBLE,0,0,(!0),(!0),0,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  types = g_list_append(types,type);
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_AWAY,0,0,(!0),(!0),0,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  types = g_list_append(types,type);
/*
	 * New statuses for GG 8.0 like PoGGadaj ze mna (not yet because
	 * libpurple can't support Chatty status) and Nie przeszkadzac
	 */
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_UNAVAILABLE,0,0,(!0),(!0),0,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  types = g_list_append(types,type);
/*
	 * This status is necessary to display guys who are blocking *us*.
	 */
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_INVISIBLE,"blocked",((const char *)(dgettext("pidgin","Blocked"))),(!0),0,0,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  types = g_list_append(types,type);
  type = purple_status_type_new_with_attrs(PURPLE_STATUS_OFFLINE,0,0,(!0),(!0),0,"message",((const char *)(dgettext("pidgin","Message"))),purple_value_new(PURPLE_TYPE_STRING),((void *)((void *)0)));
  types = g_list_append(types,type);
  return types;
}

static GList *ggp_blist_node_menu(PurpleBlistNode *node)
{
  PurpleMenuAction *act;
  GList *m = (GList *)((void *)0);
  PurpleAccount *account;
  GGPInfo *info;
  if (!((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)) 
    return 0;
  account = purple_buddy_get_account(((PurpleBuddy *)node));
  info = ( *purple_account_get_connection(account)).proto_data;
  if ((info -> chats) != 0) {
    act = purple_menu_action_new(((const char *)(dgettext("pidgin","Add to chat"))),((PurpleCallback )ggp_bmenu_add_to_chat),0,0);
    m = g_list_append(m,act);
  }
  return m;
}

static GList *ggp_chat_info(PurpleConnection *gc)
{
  GList *m = (GList *)((void *)0);
  struct proto_chat_entry *pce;
  pce = ((struct proto_chat_entry *)(g_malloc0_n(1,(sizeof(struct proto_chat_entry )))));
  pce -> label = ((const char *)(dgettext("pidgin","Chat _name:")));
  pce -> identifier = "name";
  pce -> required = (!0);
  m = g_list_append(m,pce);
  return m;
}

static void ggp_login(PurpleAccount *account)
{
  PurpleConnection *gc;
  PurplePresence *presence;
  PurpleStatus *status;
  struct gg_login_params *glp;
  GGPInfo *info;
  const char *address;
  const gchar *encryption_type;
  if (ggp_setup_proxy(account) == -1) 
    return ;
  gc = purple_account_get_connection(account);
  glp = ((struct gg_login_params *)(g_malloc0_n(1,(sizeof(struct gg_login_params )))));
  info = ((GGPInfo *)(g_malloc0_n(1,(sizeof(GGPInfo )))));
/* Probably this should be moved to *_new() function. */
  info -> session = ((struct gg_session *)((void *)0));
  info -> chats = ((GList *)((void *)0));
  info -> chats_count = 0;
  info -> token = ((GGPToken *)((void *)0));
  info -> searches = ggp_search_new();
  info -> pending_richtext_messages = ((GList *)((void *)0));
  info -> pending_images = g_hash_table_new(g_direct_hash,g_direct_equal);
  info -> status_broadcasting = purple_account_get_bool(account,"status_broadcasting",(!0));
  gc -> proto_data = info;
  glp -> uin = ggp_get_uin(account);
  glp -> password = ((char *)(purple_account_get_password(account)));
  glp -> image_size = 255;
  presence = purple_account_get_presence(account);
  status = purple_presence_get_active_status(presence);
  glp -> encoding = GG_ENCODING_UTF8;
  glp -> protocol_features = 0 | 16 | 0x2000;
  glp -> async = 1;
  glp -> status = ggp_to_gg_status(status,&glp -> status_descr);
  encryption_type = purple_account_get_string(account,"encryption","none");
  purple_debug_info("gg","Requested encryption type: %s\n",encryption_type);
  if (strcmp(encryption_type,"opportunistic_tls") == 0) 
    glp -> tls = 1;
  else 
    glp -> tls = 0;
  purple_debug_info("gg","TLS enabled: %d\n",(glp -> tls));
  if (!((info -> status_broadcasting) != 0)) 
    glp -> status = ((glp -> status) | 0x8000);
  address = purple_account_get_string(account,"gg_server","");
  if ((address != 0) && (( *address) != 0)) {
/* TODO: Make this non-blocking */
    struct in_addr *addr = gg_gethostbyname(address);
    purple_debug_info("gg","Using gg server given by user (%s)\n",address);
    if (addr == ((struct in_addr *)((void *)0))) {
      gchar *tmp = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to resolve hostname \'%s\': %s"))),address,g_strerror( *__errno_location()));
      purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,tmp);
/* should this be a settings error? */
      g_free(tmp);
      return ;
    }
    glp -> server_addr = inet_addr((inet_ntoa( *addr)));
    glp -> server_port = 8074;
    free(addr);
  }
  else 
    purple_debug_info("gg","Trying to retrieve address from gg appmsg service\n");
  info -> session = gg_login(glp);
  purple_connection_update_progress(gc,((const char *)(dgettext("pidgin","Connecting"))),0,2);
  if ((info -> session) == ((struct gg_session *)((void *)0))) {
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Connection failed"))));
    g_free(glp);
    return ;
  }
  gc -> inpa = (purple_input_add(( *(info -> session)).fd,ggp_tcpsocket_inputcond_gg_to_purple(( *(info -> session)).check),ggp_async_login_handler,gc));
}

static void ggp_close(PurpleConnection *gc)
{
  if (gc == ((PurpleConnection *)((void *)0))) {
    purple_debug_info("gg","gc == NULL\n");
    return ;
  }
  if ((gc -> proto_data) != 0) {
    PurpleAccount *account = purple_connection_get_account(gc);
    PurpleStatus *status;
    GGPInfo *info = (gc -> proto_data);
    status = purple_account_get_active_status(account);
    if ((info -> session) != ((struct gg_session *)((void *)0))) {
      ggp_set_status(account,status);
      gg_logoff((info -> session));
      gg_free_session((info -> session));
    }
    purple_account_set_bool(account,"status_broadcasting",(info -> status_broadcasting));
/* Immediately close any notifications on this handle since that process depends
		 * upon the contents of info->searches, which we are about to destroy.
		 */
    purple_notify_close_with_handle(gc);
    ggp_search_destroy((info -> searches));
    g_list_free((info -> pending_richtext_messages));
    g_hash_table_destroy((info -> pending_images));
    g_free(info);
    gc -> proto_data = ((void *)((void *)0));
  }
  if ((gc -> inpa) > 0) 
    purple_input_remove((gc -> inpa));
  purple_debug_info("gg","Connection closed.\n");
}

static int ggp_send_im(PurpleConnection *gc,const char *who,const char *msg,PurpleMessageFlags flags)
{
  GGPInfo *info = (gc -> proto_data);
  char *tmp;
  char *plain;
  int ret = 1;
  unsigned char format[1024UL];
  unsigned int format_length = (sizeof(struct gg_msg_richtext ));
  gint pos = 0;
  GData *attribs;
  const char *start;
  const char *end = (const char *)((void *)0);
  const char *last;
  if ((msg == ((const char *)((void *)0))) || (( *msg) == 0)) {
    return 0;
  }
  last = msg;
/* Check if the message is richtext */
/* TODO: Check formatting, too */
  if (purple_markup_find_tag("img",last,&start,&end,&attribs) != 0) {
    GString *string_buffer = g_string_new(0);
    struct gg_msg_richtext fmt;
    do {
      PurpleStoredImage *image;
      const char *id;
/* Add text before the image */
      if ((start - last) != 0LL) {
        pos = (pos + g_utf8_strlen(last,(start - last)));
        g_string_append_len(string_buffer,last,(start - last));
      }
      if (((id = (g_datalist_get_data(&attribs,"id"))) != 0) && ((image = purple_imgstore_find_by_id(atoi(id))) != 0)) {
        struct gg_msg_richtext_format actformat;
        struct gg_msg_richtext_image actimage;
        gint image_size = (purple_imgstore_get_size(image));
        gconstpointer image_bin = purple_imgstore_get_data(image);
        const char *image_filename = purple_imgstore_get_filename(image);
        uint32_t crc32 = gg_crc32(0,image_bin,image_size);
        g_hash_table_insert((info -> pending_images),((gpointer )((glong )crc32)),((gpointer )((glong )(atoi(id)))));
        purple_imgstore_ref(image);
        purple_debug_info("gg","ggp_send_im_richtext: got crc: %u for imgid: %i\n",crc32,atoi(id));
        actformat.font = 128;
        actformat.position = pos;
        actimage.unknown1 = 0x0109;
        actimage.size = gg_fix32(image_size);
        actimage.crc32 = gg_fix32(crc32);
        if (actimage.size > 255000) {
          purple_debug_warning("gg","ggp_send_im_richtext: image over 255kb!\n");
        }
        else {
          purple_debug_info("gg","ggp_send_im_richtext: adding images to richtext, size: %i, crc32: %u, name: %s\n",actimage.size,actimage.crc32,image_filename);
          memcpy((format + format_length),(&actformat),(sizeof(actformat)));
          format_length += sizeof(actformat);
          memcpy((format + format_length),(&actimage),(sizeof(actimage)));
          format_length += sizeof(actimage);
        }
      }
      else {
        purple_debug_error("gg","ggp_send_im_richtext: image not found in the image store!");
      }
      last = (end + 1);
      g_datalist_clear(&attribs);
    }while (purple_markup_find_tag("img",last,&start,&end,&attribs) != 0);
/* Add text after the images */
    if ((last != 0) && (( *last) != 0)) {
      pos = (pos + g_utf8_strlen(last,(-1)));
      g_string_append(string_buffer,last);
    }
    fmt.flag = 2;
    fmt.length = (format_length - sizeof(fmt));
    memcpy(format,(&fmt),(sizeof(fmt)));
    purple_debug_info("gg","ggp_send_im: richtext msg = %s\n",(string_buffer -> str));
    plain = purple_unescape_html((string_buffer -> str));
    g_string_free(string_buffer,(!0));
  }
  else {
    purple_debug_info("gg","ggp_send_im: msg = %s\n",msg);
    plain = purple_unescape_html(msg);
  }
/*
	tmp = charset_convert(plain, "UTF-8", "CP1250");
	*/
  tmp = g_strdup_printf("%s",plain);
  if ((tmp != 0) && ((format_length - sizeof(struct gg_msg_richtext )) != 0ULL)) {
    if (gg_send_message_richtext((info -> session),8,ggp_str_to_uin(who),((unsigned char *)tmp),format,format_length) < 0) {
      ret = -1;
    }
    else {
      ret = 1;
    }
  }
  else if ((((char *)((void *)0)) == tmp) || (( *tmp) == 0)) {
    ret = 0;
  }
  else if (strlen(tmp) > 1989) {
    ret = -7;
  }
  else if (gg_send_message((info -> session),8,ggp_str_to_uin(who),((unsigned char *)tmp)) < 0) {
    ret = -1;
  }
  else {
    ret = 1;
  }
  g_free(plain);
  g_free(tmp);
  return ret;
}

static unsigned int ggp_send_typing(PurpleConnection *gc,const char *name,PurpleTypingState state)
{
// we don't send real length of typed message
  int dummy_length;
// not supported
  if (state == PURPLE_TYPED) 
    return 1;
  if (state == PURPLE_TYPING) 
    dummy_length = ((int )(g_random_int()));
  else 
// PURPLE_NOT_TYPING
    dummy_length = 0;
  gg_typing_notification(( *((GGPInfo *)(gc -> proto_data))).session,ggp_str_to_uin(name),dummy_length);
// wait 1 second before another notification
  return 1;
}

static void ggp_get_info(PurpleConnection *gc,const char *name)
{
  GGPInfo *info = (gc -> proto_data);
  GGPSearchForm *form;
  guint32 seq;
  form = ggp_search_form_new(GGP_SEARCH_TYPE_INFO);
  form -> user_data = info;
  form -> uin = g_strdup(name);
  seq = ggp_search_start(gc,form);
  ggp_search_add((info -> searches),seq,form);
  purple_debug_info("gg","ggp_get_info(): Added seq %u",seq);
}

static int ggp_to_gg_status(PurpleStatus *status,char **msg)
{
  const char *status_id = purple_status_get_id(status);
  int new_status;
  int new_status_descr;
  const char *new_msg;
  do {
    if (msg != ((char **)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg != NULL");
      return 0;
    };
  }while (0);
  purple_debug_info("gg","ggp_to_gg_status: Requested status = %s\n",status_id);
  if (strcmp(status_id,"available") == 0) {
    new_status = 2;
    new_status_descr = 4;
  }
  else if (strcmp(status_id,"away") == 0) {
    new_status = 3;
    new_status_descr = 5;
  }
  else if (strcmp(status_id,"unavailable") == 0) {
    new_status = 0x0021;
    new_status_descr = 0x0022;
  }
  else if (strcmp(status_id,"invisible") == 0) {
    new_status = 20;
    new_status_descr = 22;
  }
  else if (strcmp(status_id,"offline") == 0) {
    new_status = 1;
    new_status_descr = 21;
  }
  else {
    new_status = 2;
    new_status_descr = 4;
    purple_debug_info("gg","ggp_set_status: unknown status requested (status_id=%s)\n",status_id);
  }
  new_msg = purple_status_get_attr_string(status,"message");
  if (new_msg != 0) {
/*
		char *tmp = purple_markup_strip_html(new_msg);
		*msg = charset_convert(tmp, "UTF-8", "CP1250");
		g_free(tmp);
		*/
     *msg = purple_markup_strip_html(new_msg);
    return new_status_descr;
  }
  else {
     *msg = ((char *)((void *)0));
    return new_status;
  }
}

static void ggp_set_status(PurpleAccount *account,PurpleStatus *status)
{
  PurpleConnection *gc;
  GGPInfo *info;
  int new_status;
  char *new_msg = (char *)((void *)0);
  if (!(purple_status_is_active(status) != 0)) 
    return ;
  gc = purple_account_get_connection(account);
  info = (gc -> proto_data);
  new_status = ggp_to_gg_status(status,&new_msg);
  if (!((info -> status_broadcasting) != 0)) 
    new_status = (new_status | 0x8000);
  if (new_msg == ((char *)((void *)0))) {
    gg_change_status((info -> session),new_status);
  }
  else {
    gg_change_status_descr((info -> session),new_status,new_msg);
    g_free(new_msg);
  }
  ggp_status_fake_to_self(account);
}

static void ggp_add_buddy(PurpleConnection *gc,PurpleBuddy *buddy,PurpleGroup *group)
{
  PurpleAccount *account;
  GGPInfo *info = (gc -> proto_data);
  const gchar *name = purple_buddy_get_name(buddy);
  gg_add_notify((info -> session),ggp_str_to_uin(name));
  account = purple_connection_get_account(gc);
  if (strcmp(purple_account_get_username(account),name) == 0) {
    ggp_status_fake_to_self(account);
  }
}

static void ggp_remove_buddy(PurpleConnection *gc,PurpleBuddy *buddy,PurpleGroup *group)
{
  GGPInfo *info = (gc -> proto_data);
  gg_remove_notify((info -> session),ggp_str_to_uin(purple_buddy_get_name(buddy)));
}

static void ggp_join_chat(PurpleConnection *gc,GHashTable *data)
{
  GGPInfo *info = (gc -> proto_data);
  GGPChat *chat;
  char *chat_name;
  GList *l;
  PurpleConversation *conv;
  PurpleAccount *account = purple_connection_get_account(gc);
  chat_name = (g_hash_table_lookup(data,"name"));
  if (chat_name == ((char *)((void *)0))) 
    return ;
  purple_debug_info("gg","joined %s chat\n",chat_name);
  for (l = (info -> chats); l != ((GList *)((void *)0)); l = (l -> next)) {
    chat = (l -> data);
    if ((chat != ((GGPChat *)((void *)0))) && (g_utf8_collate((chat -> name),chat_name) == 0)) {
      purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Chat error"))),((const char *)(dgettext("pidgin","This chat name is already in use"))),0,0,0);
      return ;
    }
  }
  ggp_confer_add_new(gc,chat_name);
  conv = serv_got_joined_chat(gc,(info -> chats_count),chat_name);
  purple_conv_chat_add_user(purple_conversation_get_chat_data(conv),purple_account_get_username(account),0,PURPLE_CBFLAGS_NONE,(!0));
}

static char *ggp_get_chat_name(GHashTable *data)
{
  return g_strdup((g_hash_table_lookup(data,"name")));
}

static int ggp_chat_send(PurpleConnection *gc,int id,const char *message,PurpleMessageFlags flags)
{
  PurpleConversation *conv;
  GGPInfo *info = (gc -> proto_data);
  GGPChat *chat = (GGPChat *)((void *)0);
  GList *l;
/* char *msg, *plain; */
  gchar *msg;
  uin_t *uins;
  int count = 0;
  if ((conv = purple_find_chat(gc,id)) == ((PurpleConversation *)((void *)0))) 
    return -22;
{
    for (l = (info -> chats); l != ((GList *)((void *)0)); l = (l -> next)) {
      chat = (l -> data);
      if (g_utf8_collate((chat -> name),(conv -> name)) == 0) {
        break; 
      }
      chat = ((GGPChat *)((void *)0));
    }
  }
  if (chat == ((GGPChat *)((void *)0))) {
    purple_debug_error("gg","ggp_chat_send: Hm... that\'s strange. No such chat\?\n");
    return -22;
  }
  uins = ((uin_t *)(g_malloc0_n((g_list_length((chat -> participants))),(sizeof(uin_t )))));
  for (l = (chat -> participants); l != ((GList *)((void *)0)); l = (l -> next)) {
    uin_t uin = ((gint )((glong )(l -> data)));
    uins[count++] = uin;
  }
/*
	plain = purple_unescape_html(message);
	msg = charset_convert(plain, "UTF-8", "CP1250");
	g_free(plain);
	*/
  msg = purple_unescape_html(message);
  gg_send_message_confer((info -> session),8,count,uins,((unsigned char *)msg));
  g_free(msg);
  g_free(uins);
  serv_got_chat_in(gc,id,purple_account_get_username((purple_connection_get_account(gc))),flags,message,time(0));
  return 0;
}

static void ggp_keepalive(PurpleConnection *gc)
{
  GGPInfo *info = (gc -> proto_data);
/* purple_debug_info("gg", "Keeping connection alive....\n"); */
  if (gg_ping((info -> session)) < 0) {
    purple_debug_info("gg","Not connected to the server or gg_session is not correct\n");
    purple_connection_error_reason(gc,PURPLE_CONNECTION_ERROR_NETWORK_ERROR,((const char *)(dgettext("pidgin","Not connected to the server"))));
  }
}

static GList *ggp_actions(PurplePlugin *plugin,gpointer context)
{
  GList *m = (GList *)((void *)0);
  PurplePluginAction *act;
  act = purple_plugin_action_new(((const char *)(dgettext("pidgin","Find buddies..."))),ggp_find_buddies);
  m = g_list_append(m,act);
  act = purple_plugin_action_new(((const char *)(dgettext("pidgin","Change status broadcasting"))),ggp_action_change_status_broadcasting);
  m = g_list_append(m,act);
  m = g_list_append(m,0);
  act = purple_plugin_action_new(((const char *)(dgettext("pidgin","Save buddylist to file..."))),ggp_action_buddylist_save);
  m = g_list_append(m,act);
  act = purple_plugin_action_new(((const char *)(dgettext("pidgin","Load buddylist from file..."))),ggp_action_buddylist_load);
  m = g_list_append(m,act);
  return m;
}

static gboolean ggp_offline_message(const PurpleBuddy *buddy)
{
  return (!0);
}
static PurplePluginProtocolInfo prpl_info = {(OPT_PROTO_IM_IMAGE), ((GList *)((void *)0)), ((GList *)((void *)0)), 
/* user_splits */
/* protocol_options */
/* icon_spec */
{("png"), (32), (32), (96), (96), (0), (PURPLE_ICON_SCALE_DISPLAY)}, (ggp_list_icon), ((const char *(*)(PurpleBuddy *))((void *)0)), (ggp_status_text), (ggp_tooltip_text), (ggp_status_types), (ggp_blist_node_menu), (ggp_chat_info), ((GHashTable *(*)(PurpleConnection *, const char *))((void *)0)), (ggp_login), (ggp_close), (ggp_send_im), ((void (*)(PurpleConnection *, const char *))((void *)0)), (ggp_send_typing), (ggp_get_info), (ggp_set_status), ((void (*)(PurpleConnection *, int ))((void *)0)), ((void (*)(PurpleConnection *, const char *, const char *))((void *)0)), (ggp_add_buddy), ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)), (ggp_remove_buddy), ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), (ggp_add_deny), ((void (*)(PurpleConnection *, const char *))((void *)0)), (ggp_rem_deny), ((void (*)(PurpleConnection *))((void *)0)), (ggp_join_chat), ((void (*)(PurpleConnection *, GHashTable *))((void *)0)), (ggp_get_chat_name), ((void (*)(PurpleConnection *, int , const char *, const char *))((void *)0)), ((void (*)(PurpleConnection *, int ))((void *)0)), ((void (*)(PurpleConnection *, int , const char *, const char *))((void *)0)), (ggp_chat_send), (ggp_keepalive), ((void (*)(PurpleAccount *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *, const char *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *, PurpleGroup *, GList *))((void *)0)), ((void (*)(PurpleBuddy *))((void *)0)), ((void (*)(PurpleConnection *, const char *))((void *)0)), ((const char *(*)(const PurpleAccount *, const char *))((void *)0)), ((void (*)(PurpleConnection *, PurpleStoredImage *))((void *)0)), ((void (*)(PurpleConnection *, PurpleGroup *))((void *)0)), ((char *(*)(PurpleConnection *, int , const char *))((void *)0)), ((void (*)(PurpleConnection *, int , const char *))((void *)0)), ((PurpleChat *(*)(PurpleAccount *, const char *))((void *)0)), ((PurpleRoomlist *(*)(PurpleConnection *))((void *)0)), ((void (*)(PurpleRoomlist *))((void *)0)), ((void (*)(PurpleRoomlist *, PurpleRoomlistRoom *))((void *)0)), ((gboolean (*)(PurpleConnection *, const char *))((void *)0)), ((void (*)(PurpleConnection *, const char *, const char *))((void *)0)), ((PurpleXfer *(*)(PurpleConnection *, const char *))((void *)0)), (ggp_offline_message), ((PurpleWhiteboardPrplOps *)((void *)0)), ((int (*)(PurpleConnection *, const char *, int ))((void *)0)), ((char *(*)(PurpleRoomlistRoom *))((void *)0)), ((void (*)(PurpleAccount *, PurpleAccountUnregistrationCb , void *))((void *)0)), ((gboolean (*)(PurpleConnection *, const char *, guint ))((void *)0)), ((GList *(*)(PurpleAccount *))((void *)0)), ((sizeof(PurplePluginProtocolInfo ))), ((GHashTable *(*)(PurpleAccount *))((void *)0)), ((gboolean (*)(PurpleAccount *, const char *, PurpleMediaSessionType ))((void *)0)), ((PurpleMediaCaps (*)(PurpleAccount *, const char *))((void *)0)), ((PurpleMood *(*)(PurpleAccount *))((void *)0)), ((void (*)(PurpleConnection *, const char *, PurpleSetPublicAliasSuccessCallback , PurpleSetPublicAliasFailureCallback ))((void *)0)), ((void (*)(PurpleConnection *, PurpleGetPublicAliasSuccessCallback , PurpleGetPublicAliasFailureCallback ))((void *)0)), ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *, const char *))((void *)0)), ((void (*)(PurpleConnection *, GList *, GList *, const char *))((void *)0))
/* list_icon */
/* list_emblem */
/* status_text */
/* tooltip_text */
/* status_types */
/* blist_node_menu */
/* chat_info */
/* chat_info_defaults */
/* login */
/* close */
/* send_im */
/* set_info */
/* send_typing */
/* get_info */
/* set_away */
/* set_idle */
/* change_passwd */
/* add_buddy */
/* add_buddies */
/* remove_buddy */
/* remove_buddies */
/* add_permit */
/* add_deny */
/* rem_permit */
/* rem_deny */
/* set_permit_deny */
/* join_chat */
/* reject_chat */
/* get_chat_name */
/* chat_invite */
/* chat_leave */
/* chat_whisper */
/* chat_send */
/* keepalive */
/* register_user */
/* get_cb_info */
/* get_cb_away */
/* alias_buddy */
/* group_buddy */
/* rename_group */
/* buddy_free */
/* convo_closed */
/* normalize */
/* set_buddy_icon */
/* remove_group */
/* get_cb_real_name */
/* set_chat_topic */
/* find_blist_chat */
/* roomlist_get_list */
/* roomlist_cancel */
/* roomlist_expand_category */
/* can_receive_file */
/* send_file */
/* new_xfer */
/* offline_message */
/* whiteboard_prpl_ops */
/* send_raw */
/* roomlist_room_serialize */
/* unregister_user */
/* send_attention */
/* get_attention_types */
/* struct_size */
/* get_account_text_table */
/* initiate_media */
/* can_do_media */
/* get_moods */
/* set_public_alias */
/* get_public_alias */
/* add_buddy_with_invite */
/* add_buddies_with_invite */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_PROTOCOL), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("prpl-gg"), ("Gadu-Gadu"), ("2.10.9"), ("Gadu-Gadu Protocol Plugin"), ("Polish popular IM"), ("boler@sourceforge.net"), ("http://pidgin.im/"), ((gboolean (*)(PurplePlugin *))((void *)0)), ((gboolean (*)(PurplePlugin *))((void *)0)), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((&prpl_info)), ((PurplePluginUiInfo *)((void *)0)), (ggp_actions), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* magic */
/* major_version */
/* minor_version */
/* plugin type */
/* ui_requirement */
/* flags */
/* dependencies */
/* priority */
/* id */
/* name */
/* version */
/* summary */
/* description */
/* author */
/* homepage */
/* load */
/* unload */
/* destroy */
/* ui_info */
/* extra_info */
/* prefs_info */
/* actions */
/* padding */
};

static void purple_gg_debug_handler(int level,const char *format,va_list args)
{
  PurpleDebugLevel purple_level;
  char *msg = g_strdup_vprintf(format,args);
/* This is pretty pointless since the GG_DEBUG levels don't correspond to
	 * the purple ones */
  switch(level){
    case 8:
{
      purple_level = PURPLE_DEBUG_INFO;
      break; 
    }
    default:
{
      purple_level = PURPLE_DEBUG_MISC;
      break; 
    }
  }
  purple_debug(purple_level,"gg","%s",msg);
  g_free(msg);
}

static void init_plugin(PurplePlugin *plugin)
{
  PurpleAccountOption *option;
  GList *encryption_options = (GList *)((void *)0);
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","Nickname"))),"nick",((const char *)(dgettext("pidgin","Gadu-Gadu User"))));
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  option = purple_account_option_string_new(((const char *)(dgettext("pidgin","GG server"))),"gg_server","");
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
#define ADD_VALUE(list, desc, v) { \
	PurpleKeyValuePair *kvp = g_new0(PurpleKeyValuePair, 1); \
	kvp->key = g_strdup((desc)); \
	kvp->value = g_strdup((v)); \
	list = g_list_append(list, kvp); \
}
{
    PurpleKeyValuePair *kvp = (PurpleKeyValuePair *)(g_malloc0_n(1,(sizeof(PurpleKeyValuePair ))));
    kvp -> key = g_strdup(((const char *)(dgettext("pidgin","Don\'t use encryption"))));
    kvp -> value = (g_strdup("none"));
    encryption_options = g_list_append(encryption_options,kvp);
  };
{
    PurpleKeyValuePair *kvp = (PurpleKeyValuePair *)(g_malloc0_n(1,(sizeof(PurpleKeyValuePair ))));
    kvp -> key = g_strdup(((const char *)(dgettext("pidgin","Use encryption if available"))));
    kvp -> value = (g_strdup("opportunistic_tls"));
    encryption_options = g_list_append(encryption_options,kvp);
  };
#if 0
/* TODO */
#endif
  option = purple_account_option_list_new(((const char *)(dgettext("pidgin","Connection security"))),"encryption",encryption_options);
  prpl_info.protocol_options = g_list_append(prpl_info.protocol_options,option);
  my_protocol = plugin;
  gg_debug_handler = purple_gg_debug_handler;
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
/* vim: set ts=8 sts=0 sw=8 noet: */
