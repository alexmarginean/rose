/**
 * @file search.c
 *
 * purple
 *
 * Copyright (C) 2005  Bartosz Oler <bartosz@bzimage.us>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <libgadu.h>
#include "gg-utils.h"
#include "search.h"
/* GGPSearchForm *ggp_search_form_new() {{{ */

GGPSearchForm *ggp_search_form_new(GGPSearchType st)
{
  GGPSearchForm *form;
  form = ((GGPSearchForm *)(g_malloc0_n(1,(sizeof(GGPSearchForm )))));
  form -> search_type = st;
  form -> window = ((void *)((void *)0));
  form -> user_data = ((void *)((void *)0));
  form -> seq = 0;
  form -> page_number = 0;
  form -> page_size = 0;
  form -> uin = ((char *)((void *)0));
  form -> lastname = ((char *)((void *)0));
  form -> firstname = ((char *)((void *)0));
  form -> nickname = ((char *)((void *)0));
  form -> city = ((char *)((void *)0));
  form -> birthyear = ((char *)((void *)0));
  form -> gender = ((char *)((void *)0));
  form -> active = ((char *)((void *)0));
  return form;
}
/* }}} */
/* void ggp_search_form_destroy(GGPSearchForm *form) {{{ */

void ggp_search_form_destroy(GGPSearchForm *form)
{
  do {
    if (form != ((GGPSearchForm *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"form != NULL");
      return ;
    };
  }while (0);
  form -> window = ((void *)((void *)0));
  form -> user_data = ((void *)((void *)0));
  form -> seq = 0;
  form -> page_number = 0;
  form -> page_size = 0;
  g_free((form -> uin));
  g_free((form -> lastname));
  g_free((form -> firstname));
  g_free((form -> nickname));
  g_free((form -> city));
  g_free((form -> birthyear));
  g_free((form -> gender));
  g_free((form -> active));
  g_free(form);
}
/* }}} */
/* void ggp_search_add(GGPSearches *searches, guint32 seq, GGPSearchForm *form) {{{ */

void ggp_search_add(GGPSearches *searches,guint32 seq,GGPSearchForm *form)
{
  guint32 *tmp;
  do {
    if (searches != ((GGPSearches *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"searches != NULL");
      return ;
    };
  }while (0);
  do {
    if (form != ((GGPSearchForm *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"form != NULL");
      return ;
    };
  }while (0);
  tmp = ((guint32 *)(g_malloc0_n(1,(sizeof(guint32 )))));
   *tmp = seq;
  form -> seq = seq;
  g_hash_table_insert(searches,tmp,form);
}
/* }}} */
/* void ggp_search_remove(GGPSearches *searches, guint32 seq) {{{ */

void ggp_search_remove(GGPSearches *searches,guint32 seq)
{
  do {
    if (searches != ((GGPSearches *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"searches != NULL");
      return ;
    };
  }while (0);
  g_hash_table_remove(searches,(&seq));
}
/* }}} */
/* GGPSearchForm *ggp_search_get(GGPSearches *searches, seq) {{{ */

GGPSearchForm *ggp_search_get(GGPSearches *searches,guint32 seq)
{
  do {
    if (searches != ((GGPSearches *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"searches != NULL");
      return 0;
    };
  }while (0);
  return (g_hash_table_lookup(searches,(&seq)));
}
/* }}} */
/* GGPSearches *ggp_search_new() {{{ */

GGPSearches *ggp_search_new()
{
  GGPSearches *searches;
  searches = g_hash_table_new_full(g_int_hash,g_int_equal,g_free,0);
  return searches;
}
/* }}} */
/* void ggp_search_destroy(GGPSearches *searches) {{{ */

void ggp_search_destroy(GGPSearches *searches)
{
  do {
    if (searches != ((GGPSearches *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"searches != NULL");
      return ;
    };
  }while (0);
  g_hash_table_destroy(searches);
}
/* }}} */
/* guint32 ggp_search_start(PurpleConnection *gc, GGPSearchForm *form) {{{ */

guint32 ggp_search_start(PurpleConnection *gc,GGPSearchForm *form)
{
  GGPInfo *info = (gc -> proto_data);
  gg_pubdir50_t req;
  guint seq;
  guint offset;
  gchar *tmp;
  purple_debug_info("gg","It\'s time to perform a search...\n");
  if ((req = gg_pubdir50_new(3)) == ((struct gg_pubdir50_s *)((void *)0))) {
    purple_debug_error("gg","ggp_bmenu_show_details: Unable to create req variable.\n");
    return 0;
  }
  if ((form -> uin) != ((char *)((void *)0))) {
    purple_debug_info("gg","    uin: %s\n",(form -> uin));
    gg_pubdir50_add(req,"FmNumber",(form -> uin));
  }
  else {
    if ((form -> lastname) != ((char *)((void *)0))) {
      purple_debug_info("gg","    lastname: %s\n",(form -> lastname));
      gg_pubdir50_add(req,"lastname",(form -> lastname));
    }
    if ((form -> firstname) != ((char *)((void *)0))) {
      purple_debug_info("gg","    firstname: %s\n",(form -> firstname));
      gg_pubdir50_add(req,"firstname",(form -> firstname));
    }
    if ((form -> nickname) != ((char *)((void *)0))) {
      purple_debug_info("gg","    nickname: %s\n",(form -> nickname));
      gg_pubdir50_add(req,"nickname",(form -> nickname));
    }
    if ((form -> city) != ((char *)((void *)0))) {
      purple_debug_info("gg","    city: %s\n",(form -> city));
      gg_pubdir50_add(req,"city",(form -> city));
    }
    if ((form -> birthyear) != ((char *)((void *)0))) {
      purple_debug_info("gg","    birthyear: %s\n",(form -> birthyear));
      gg_pubdir50_add(req,"birthyear",(form -> birthyear));
    }
    if ((form -> gender) != ((char *)((void *)0))) {
      purple_debug_info("gg","    gender: %s\n",(form -> gender));
      gg_pubdir50_add(req,"gender",(form -> gender));
    }
    if ((form -> active) != ((char *)((void *)0))) {
      purple_debug_info("gg","    active: %s\n",(form -> active));
      gg_pubdir50_add(req,"ActiveOnly",(form -> active));
    }
  }
  offset = ((form -> page_size) * (form -> page_number));
  purple_debug_info("gg","page number: %u, page size: %u, offset: %u\n",(form -> page_number),(form -> page_size),offset);
  tmp = g_strdup_printf("%u",offset);
  gg_pubdir50_add(req,"fmstart",tmp);
  g_free(tmp);
  if ((seq = gg_pubdir50((info -> session),req)) == 0) {
    purple_debug_warning("gg","ggp_bmenu_show_details: Search failed.\n");
    gg_pubdir50_free(req);
    return 0;
  }
  purple_debug_info("gg","search sequence number: %d\n",seq);
  gg_pubdir50_free(req);
  return seq;
}
/* }}} */
/* char *ggp_search_get_result(gg_pubdir50_t res, int num, const char *field) {{{ */

char *ggp_search_get_result(gg_pubdir50_t res,int num,const char *field)
{
  char *tmp;
  tmp = g_strdup(gg_pubdir50_get(res,num,field));
  return (tmp == ((char *)((void *)0)))?g_strdup("") : tmp;
}
/* }}} */
/* vim: set ts=8 sts=0 sw=8 noet: */
