/**
 * @file account.c Account API
 * @ingroup core
 */
/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "account.h"
#include "core.h"
#include "dbus-maybe.h"
#include "debug.h"
#include "network.h"
#include "notify.h"
#include "pounce.h"
#include "prefs.h"
#include "privacy.h"
#include "prpl.h"
#include "request.h"
#include "server.h"
#include "signals.h"
#include "status.h"
#include "util.h"
#include "xmlnode.h"
typedef struct __unnamed_class___F0_L44_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L486R__Pe___variable_name_unknown_scope_and_name__scope__current_error {
PurpleConnectionErrorInfo *current_error;}PurpleAccountPrivate;
#define PURPLE_ACCOUNT_GET_PRIVATE(account) \
	((PurpleAccountPrivate *) (account->priv))
/* TODO: Should use PurpleValue instead of this?  What about "ui"? */
typedef struct __unnamed_class___F0_L53_C9_unknown_scope_and_name_variable_declaration__variable_type_L273R_variable_name_unknown_scope_and_name__scope__type__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__ui__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__variable_name_unknown_scope_and_name__scope__value {
PurplePrefType type;
char *ui;
union __unnamed_class___F0_L59_C2_L487R_variable_declaration__variable_type_i_variable_name_L487R__scope__integer__DELIMITER__L487R_variable_declaration__variable_type___Pb__c__Pe___variable_name_L487R__scope__string__DELIMITER__L487R_variable_declaration__variable_type_L54R_variable_name_L487R__scope__boolean {
int integer;
char *string;
gboolean boolean;}value;}PurpleAccountSetting;
typedef struct __unnamed_class___F0_L69_C9_unknown_scope_and_name_variable_declaration__variable_type_L489R_variable_name_unknown_scope_and_name__scope__type__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L247R__Pe___variable_name_unknown_scope_and_name__scope__account__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__v__Pe___variable_name_unknown_scope_and_name__scope__ui_handle__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__user__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L55R_variable_name_unknown_scope_and_name__scope__userdata__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L249R_variable_name_unknown_scope_and_name__scope__auth_cb__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L249R_variable_name_unknown_scope_and_name__scope__deny_cb__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_guintUi__typedef_declaration_variable_name_unknown_scope_and_name__scope__ref {
PurpleAccountRequestType type;
PurpleAccount *account;
void *ui_handle;
char *user;
gpointer userdata;
PurpleAccountRequestAuthorizationCb auth_cb;
PurpleAccountRequestAuthorizationCb deny_cb;
guint ref;}PurpleAccountRequestInfo;
static PurpleAccountUiOps *account_ui_ops = (PurpleAccountUiOps *)((void *)0);
static GList *accounts = (GList *)((void *)0);
static guint save_timer = 0;
static gboolean accounts_loaded = 0;
static GList *handles = (GList *)((void *)0);
static void set_current_error(PurpleAccount *account,PurpleConnectionErrorInfo *new_err);
/*********************************************************************
 * Writing to disk                                                   *
 *********************************************************************/

static void setting_to_xmlnode(gpointer key,gpointer value,gpointer user_data)
{
  const char *name;
  PurpleAccountSetting *setting;
  xmlnode *node;
  xmlnode *child;
  char buf[21UL];
  name = ((const char *)key);
  setting = ((PurpleAccountSetting *)value);
  node = ((xmlnode *)user_data);
  child = xmlnode_new_child(node,"setting");
  xmlnode_set_attrib(child,"name",name);
  if ((setting -> type) == PURPLE_PREF_INT) {
    xmlnode_set_attrib(child,"type","int");
    g_snprintf(buf,(sizeof(buf)),"%d",setting -> value.integer);
    xmlnode_insert_data(child,buf,(-1));
  }
  else if (((setting -> type) == PURPLE_PREF_STRING) && (setting -> value.string != ((char *)((void *)0)))) {
    xmlnode_set_attrib(child,"type","string");
    xmlnode_insert_data(child,setting -> value.string,(-1));
  }
  else if ((setting -> type) == PURPLE_PREF_BOOLEAN) {
    xmlnode_set_attrib(child,"type","bool");
    g_snprintf(buf,(sizeof(buf)),"%d",setting -> value.boolean);
    xmlnode_insert_data(child,buf,(-1));
  }
}

static void ui_setting_to_xmlnode(gpointer key,gpointer value,gpointer user_data)
{
  const char *ui;
  GHashTable *table;
  xmlnode *node;
  xmlnode *child;
  ui = ((const char *)key);
  table = ((GHashTable *)value);
  node = ((xmlnode *)user_data);
  if (g_hash_table_size(table) > 0) {
    child = xmlnode_new_child(node,"settings");
    xmlnode_set_attrib(child,"ui",ui);
    g_hash_table_foreach(table,setting_to_xmlnode,child);
  }
}

static xmlnode *status_attr_to_xmlnode(const PurpleStatus *status,const PurpleStatusType *type,const PurpleStatusAttr *attr)
{
  xmlnode *node;
  const char *id;
  char *value = (char *)((void *)0);
  PurpleStatusAttr *default_attr;
  PurpleValue *default_value;
  PurpleType attr_type;
  PurpleValue *attr_value;
  id = purple_status_attr_get_id(attr);
  do {
    if (id != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id");
      return 0;
    };
  }while (0);
  attr_value = purple_status_get_attr_value(status,id);
  do {
    if (attr_value != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"attr_value");
      return 0;
    };
  }while (0);
  attr_type = purple_value_get_type(attr_value);
/*
	 * If attr_value is a different type than it should be
	 * then don't write it to the file.
	 */
  default_attr = purple_status_type_get_attr(type,id);
  default_value = purple_status_attr_get_value(default_attr);
  if (attr_type != (purple_value_get_type(default_value))) 
    return 0;
/*
	 * If attr_value is the same as the default for this status
	 * then there is no need to write it to the file.
	 */
  if (attr_type == PURPLE_TYPE_STRING) {
    const char *string_value = purple_value_get_string(attr_value);
    const char *default_string_value = purple_value_get_string(default_value);
    if (purple_strequal(string_value,default_string_value) != 0) 
      return 0;
    value = g_strdup(purple_value_get_string(attr_value));
  }
  else if (attr_type == PURPLE_TYPE_INT) {
    int int_value = purple_value_get_int(attr_value);
    if (int_value == purple_value_get_int(default_value)) 
      return 0;
    value = g_strdup_printf("%d",int_value);
  }
  else if (attr_type == PURPLE_TYPE_BOOLEAN) {
    gboolean boolean_value = purple_value_get_boolean(attr_value);
    if (boolean_value == purple_value_get_boolean(default_value)) 
      return 0;
    value = g_strdup((((boolean_value != 0)?"true" : "false")));
  }
  else {
    return 0;
  }
  do {
    if (value != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"value");
      return 0;
    };
  }while (0);
  node = xmlnode_new("attribute");
  xmlnode_set_attrib(node,"id",id);
  xmlnode_set_attrib(node,"value",value);
  g_free(value);
  return node;
}

static xmlnode *status_attrs_to_xmlnode(const PurpleStatus *status)
{
  PurpleStatusType *type = purple_status_get_type(status);
  xmlnode *node;
  xmlnode *child;
  GList *attrs;
  GList *attr;
  node = xmlnode_new("attributes");
  attrs = purple_status_type_get_attrs(type);
  for (attr = attrs; attr != ((GList *)((void *)0)); attr = (attr -> next)) {
    child = status_attr_to_xmlnode(status,type,((const PurpleStatusAttr *)(attr -> data)));
    if (child != 0) 
      xmlnode_insert_child(node,child);
  }
  return node;
}

static xmlnode *status_to_xmlnode(const PurpleStatus *status)
{
  xmlnode *node;
  xmlnode *child;
  node = xmlnode_new("status");
  xmlnode_set_attrib(node,"type",purple_status_get_id(status));
  if (purple_status_get_name(status) != ((const char *)((void *)0))) 
    xmlnode_set_attrib(node,"name",purple_status_get_name(status));
  xmlnode_set_attrib(node,"active",((purple_status_is_active(status) != 0)?"true" : "false"));
  child = status_attrs_to_xmlnode(status);
  xmlnode_insert_child(node,child);
  return node;
}

static xmlnode *statuses_to_xmlnode(const PurplePresence *presence)
{
  xmlnode *node;
  xmlnode *child;
  GList *statuses;
  PurpleStatus *status;
  node = xmlnode_new("statuses");
  statuses = purple_presence_get_statuses(presence);
  for (; statuses != ((GList *)((void *)0)); statuses = (statuses -> next)) {
    status = (statuses -> data);
    if (purple_status_type_is_saveable((purple_status_get_type(status))) != 0) {
      child = status_to_xmlnode(status);
      xmlnode_insert_child(node,child);
    }
  }
  return node;
}

static xmlnode *proxy_settings_to_xmlnode(PurpleProxyInfo *proxy_info)
{
  xmlnode *node;
  xmlnode *child;
  PurpleProxyType proxy_type;
  const char *value;
  int int_value;
  char buf[21UL];
  proxy_type = purple_proxy_info_get_type(proxy_info);
  node = xmlnode_new("proxy");
  child = xmlnode_new_child(node,"type");
  xmlnode_insert_data(child,((proxy_type == PURPLE_PROXY_USE_GLOBAL)?"global" : (((proxy_type == PURPLE_PROXY_NONE)?"none" : (((proxy_type == PURPLE_PROXY_HTTP)?"http" : (((proxy_type == PURPLE_PROXY_SOCKS4)?"socks4" : (((proxy_type == PURPLE_PROXY_SOCKS5)?"socks5" : (((proxy_type == PURPLE_PROXY_TOR)?"tor" : (((proxy_type == PURPLE_PROXY_USE_ENVVAR)?"envvar" : "unknown"))))))))))))),(-1));
  if ((value = purple_proxy_info_get_host(proxy_info)) != ((const char *)((void *)0))) {
    child = xmlnode_new_child(node,"host");
    xmlnode_insert_data(child,value,(-1));
  }
  if ((int_value = purple_proxy_info_get_port(proxy_info)) != 0) {
    g_snprintf(buf,(sizeof(buf)),"%d",int_value);
    child = xmlnode_new_child(node,"port");
    xmlnode_insert_data(child,buf,(-1));
  }
  if ((value = purple_proxy_info_get_username(proxy_info)) != ((const char *)((void *)0))) {
    child = xmlnode_new_child(node,"username");
    xmlnode_insert_data(child,value,(-1));
  }
  if ((value = purple_proxy_info_get_password(proxy_info)) != ((const char *)((void *)0))) {
    child = xmlnode_new_child(node,"password");
    xmlnode_insert_data(child,value,(-1));
  }
  return node;
}

static xmlnode *current_error_to_xmlnode(PurpleConnectionErrorInfo *err)
{
  xmlnode *node;
  xmlnode *child;
  char type_str[3UL];
  node = xmlnode_new("current_error");
  if (err == ((PurpleConnectionErrorInfo *)((void *)0))) 
    return node;
/* It doesn't make sense to have transient errors persist across a
	 * restart.
	 */
  if (!(purple_connection_error_is_fatal((err -> type)) != 0)) 
    return node;
  child = xmlnode_new_child(node,"type");
  g_snprintf(type_str,(sizeof(type_str)),"%u",(err -> type));
  xmlnode_insert_data(child,type_str,(-1));
  child = xmlnode_new_child(node,"description");
  if ((err -> description) != 0) {
    char *utf8ized = purple_utf8_try_convert((err -> description));
    if (utf8ized == ((char *)((void *)0))) 
      utf8ized = purple_utf8_salvage((err -> description));
    xmlnode_insert_data(child,utf8ized,(-1));
    g_free(utf8ized);
  }
  return node;
}

static xmlnode *account_to_xmlnode(PurpleAccount *account)
{
  PurpleAccountPrivate *priv = (PurpleAccountPrivate *)(account -> priv);
  xmlnode *node;
  xmlnode *child;
  const char *tmp;
  PurplePresence *presence;
  PurpleProxyInfo *proxy_info;
  node = xmlnode_new("account");
  child = xmlnode_new_child(node,"protocol");
  xmlnode_insert_data(child,purple_account_get_protocol_id(account),(-1));
  child = xmlnode_new_child(node,"name");
  xmlnode_insert_data(child,purple_account_get_username(account),(-1));
  if ((purple_account_get_remember_password(account) != 0) && ((tmp = purple_account_get_password(account)) != ((const char *)((void *)0)))) {
    child = xmlnode_new_child(node,"password");
    xmlnode_insert_data(child,tmp,(-1));
  }
  if ((tmp = purple_account_get_alias(account)) != ((const char *)((void *)0))) {
    child = xmlnode_new_child(node,"alias");
    xmlnode_insert_data(child,tmp,(-1));
  }
  if ((presence = purple_account_get_presence(account)) != ((PurplePresence *)((void *)0))) {
    child = statuses_to_xmlnode(presence);
    xmlnode_insert_child(node,child);
  }
  if ((tmp = purple_account_get_user_info(account)) != ((const char *)((void *)0))) {
/* TODO: Do we need to call purple_str_strip_char(tmp, '\r') here? */
    child = xmlnode_new_child(node,"userinfo");
    xmlnode_insert_data(child,tmp,(-1));
  }
  if (g_hash_table_size((account -> settings)) > 0) {
    child = xmlnode_new_child(node,"settings");
    g_hash_table_foreach((account -> settings),setting_to_xmlnode,child);
  }
  if (g_hash_table_size((account -> ui_settings)) > 0) {
    g_hash_table_foreach((account -> ui_settings),ui_setting_to_xmlnode,node);
  }
  if ((proxy_info = purple_account_get_proxy_info(account)) != ((PurpleProxyInfo *)((void *)0))) {
    child = proxy_settings_to_xmlnode(proxy_info);
    xmlnode_insert_child(node,child);
  }
  child = current_error_to_xmlnode((priv -> current_error));
  xmlnode_insert_child(node,child);
  return node;
}

static xmlnode *accounts_to_xmlnode()
{
  xmlnode *node;
  xmlnode *child;
  GList *cur;
  node = xmlnode_new("account");
  xmlnode_set_attrib(node,"version","1.0");
  for (cur = purple_accounts_get_all(); cur != ((GList *)((void *)0)); cur = (cur -> next)) {
    child = account_to_xmlnode((cur -> data));
    xmlnode_insert_child(node,child);
  }
  return node;
}

static void sync_accounts()
{
  xmlnode *node;
  char *data;
  if (!(accounts_loaded != 0)) {
    purple_debug_error("account","Attempted to save accounts before they were read!\n");
    return ;
  }
  node = accounts_to_xmlnode();
  data = xmlnode_to_formatted_str(node,0);
  purple_util_write_data_to_file("accounts.xml",data,(-1));
  g_free(data);
  xmlnode_free(node);
}

static gboolean save_cb(gpointer data)
{
  sync_accounts();
  save_timer = 0;
  return 0;
}

static void schedule_accounts_save()
{
  if (save_timer == 0) 
    save_timer = purple_timeout_add_seconds(5,save_cb,0);
}
/*********************************************************************
 * Reading from disk                                                 *
 *********************************************************************/

static void migrate_yahoo_japan(PurpleAccount *account)
{
/* detect a Yahoo! JAPAN account that existed prior to 2.6.0 and convert it
	 * to use the new prpl-yahoojp.  Also remove the account-specific settings
	 * we no longer need */
  if (purple_strequal(purple_account_get_protocol_id(account),"prpl-yahoo") != 0) {
    if (purple_account_get_bool(account,"yahoojp",0) != 0) {
      const char *serverjp = purple_account_get_string(account,"serverjp",0);
      const char *xferjp_host = purple_account_get_string(account,"xferjp_host",0);
      do {
        if (serverjp != ((const char *)((void *)0))) {
        }
        else {
          g_return_if_fail_warning(0,((const char *)__func__),"serverjp != NULL");
          return ;
        };
      }while (0);
      do {
        if (xferjp_host != ((const char *)((void *)0))) {
        }
        else {
          g_return_if_fail_warning(0,((const char *)__func__),"xferjp_host != NULL");
          return ;
        };
      }while (0);
      purple_account_set_string(account,"server",serverjp);
      purple_account_set_string(account,"xfer_host",xferjp_host);
      purple_account_set_protocol_id(account,"prpl-yahoojp");
    }
/* these should always be nuked */
    purple_account_remove_setting(account,"yahoojp");
    purple_account_remove_setting(account,"serverjp");
    purple_account_remove_setting(account,"xferjp_host");
  }
}

static void migrate_icq_server(PurpleAccount *account)
{
/* Migrate the login server setting for ICQ accounts.  See
	 * 'mtn log --last 1 --no-graph --from b6d7712e90b68610df3bd2d8cbaf46d94c8b3794'
	 * for details on the change. */
  if (purple_strequal(purple_account_get_protocol_id(account),"prpl-icq") != 0) {
    const char *tmp = purple_account_get_string(account,"server",0);
/* Non-secure server */
    if ((purple_strequal(tmp,"login.messaging.aol.com") != 0) || (purple_strequal(tmp,"login.oscar.aol.com") != 0)) 
      purple_account_set_string(account,"server","login.icq.com");
/* Secure server */
    if (purple_strequal(tmp,"slogin.oscar.aol.com") != 0) 
      purple_account_set_string(account,"server","slogin.icq.com");
  }
}

static void migrate_xmpp_encryption(PurpleAccount *account)
{
/* When this is removed, nuke the "old_ssl" and "require_tls" settings */
  if (g_str_equal((purple_account_get_protocol_id(account)),"prpl-jabber") != 0) {
    const char *sec = purple_account_get_string(account,"connection_security","");
    if (g_str_equal("",sec) != 0) {
      const char *val = "require_tls";
      if (purple_account_get_bool(account,"old_ssl",0) != 0) 
        val = "old_ssl";
      else if (!(purple_account_get_bool(account,"require_tls",(!0)) != 0)) 
        val = "opportunistic_tls";
      purple_account_set_string(account,"connection_security",val);
    }
  }
}

static void parse_settings(xmlnode *node,PurpleAccount *account)
{
  const char *ui;
  xmlnode *child;
/* Get the UI string, if these are UI settings */
  ui = xmlnode_get_attrib(node,"ui");
/* Read settings, one by one */
  for (child = xmlnode_get_child(node,"setting"); child != ((xmlnode *)((void *)0)); child = xmlnode_get_next_twin(child)) {{
      const char *name;
      const char *str_type;
      PurplePrefType type;
      char *data;
      name = xmlnode_get_attrib(child,"name");
      if (name == ((const char *)((void *)0))) 
/* Ignore this setting */
        continue; 
      str_type = xmlnode_get_attrib(child,"type");
      if (str_type == ((const char *)((void *)0))) 
/* Ignore this setting */
        continue; 
      if (purple_strequal(str_type,"string") != 0) 
        type = PURPLE_PREF_STRING;
      else if (purple_strequal(str_type,"int") != 0) 
        type = PURPLE_PREF_INT;
      else if (purple_strequal(str_type,"bool") != 0) 
        type = PURPLE_PREF_BOOLEAN;
      else 
/* Ignore this setting */
        continue; 
      data = xmlnode_get_data(child);
      if (data == ((char *)((void *)0))) 
/* Ignore this setting */
        continue; 
      if (ui == ((const char *)((void *)0))) {
        if (type == PURPLE_PREF_STRING) 
          purple_account_set_string(account,name,data);
        else if (type == PURPLE_PREF_INT) 
          purple_account_set_int(account,name,atoi(data));
        else if (type == PURPLE_PREF_BOOLEAN) 
          purple_account_set_bool(account,name,((( *data) == 48)?0 : !0));
      }
      else {
        if (type == PURPLE_PREF_STRING) 
          purple_account_set_ui_string(account,ui,name,data);
        else if (type == PURPLE_PREF_INT) 
          purple_account_set_ui_int(account,ui,name,atoi(data));
        else if (type == PURPLE_PREF_BOOLEAN) 
          purple_account_set_ui_bool(account,ui,name,((( *data) == 48)?0 : !0));
      }
      g_free(data);
    }
  }
/* we do this here because we need access to account settings to determine
	 * if we can/should migrate an old Yahoo! JAPAN account */
  migrate_yahoo_japan(account);
/* we do this here because we need access to account settings to determine
	 * if we can/should migrate an ICQ account's server setting */
  migrate_icq_server(account);
/* we do this here because we need to do it before the user views the
	 * Edit Account dialog. */
  migrate_xmpp_encryption(account);
}

static GList *parse_status_attrs(xmlnode *node,PurpleStatus *status)
{
  GList *list = (GList *)((void *)0);
  xmlnode *child;
  PurpleValue *attr_value;
  for (child = xmlnode_get_child(node,"attribute"); child != ((xmlnode *)((void *)0)); child = xmlnode_get_next_twin(child)) {{
      const char *id = xmlnode_get_attrib(child,"id");
      const char *value = xmlnode_get_attrib(child,"value");
      if (((!(id != 0) || !(( *id) != 0)) || !(value != 0)) || !(( *value) != 0)) 
        continue; 
      attr_value = purple_status_get_attr_value(status,id);
      if (!(attr_value != 0)) 
        continue; 
      list = g_list_append(list,((char *)id));
{
        switch(purple_value_get_type(attr_value)){
          case PURPLE_TYPE_STRING:
{
            list = g_list_append(list,((char *)value));
            break; 
          }
          case PURPLE_TYPE_BOOLEAN:
{
          }
          case PURPLE_TYPE_INT:
{
{
              int v;
              if (sscanf(value,"%d",&v) == 1) 
                list = g_list_append(list,((gpointer )((glong )v)));
              else 
                list = g_list_remove(list,id);
              break; 
            }
          }
          default:
{
            break; 
          }
        }
      }
    }
  }
  return list;
}

static void parse_status(xmlnode *node,PurpleAccount *account)
{
  gboolean active = 0;
  const char *data;
  const char *type;
  xmlnode *child;
  GList *attrs = (GList *)((void *)0);
/* Get the active/inactive state */
  data = xmlnode_get_attrib(node,"active");
  if (data == ((const char *)((void *)0))) 
    return ;
  if (g_ascii_strcasecmp(data,"true") == 0) 
    active = (!0);
  else if (g_ascii_strcasecmp(data,"false") == 0) 
    active = 0;
  else 
    return ;
/* Get the type of the status */
  type = xmlnode_get_attrib(node,"type");
  if (type == ((const char *)((void *)0))) 
    return ;
/* Read attributes into a GList */
  child = xmlnode_get_child(node,"attributes");
  if (child != ((xmlnode *)((void *)0))) {
    attrs = parse_status_attrs(child,purple_account_get_status(account,type));
  }
  purple_account_set_status_list(account,type,active,attrs);
  g_list_free(attrs);
}

static void parse_statuses(xmlnode *node,PurpleAccount *account)
{
  xmlnode *child;
  for (child = xmlnode_get_child(node,"status"); child != ((xmlnode *)((void *)0)); child = xmlnode_get_next_twin(child)) {
    parse_status(child,account);
  }
}

static void parse_proxy_info(xmlnode *node,PurpleAccount *account)
{
  PurpleProxyInfo *proxy_info;
  xmlnode *child;
  char *data;
  proxy_info = purple_proxy_info_new();
/* Use the global proxy settings, by default */
  purple_proxy_info_set_type(proxy_info,PURPLE_PROXY_USE_GLOBAL);
/* Read proxy type */
  child = xmlnode_get_child(node,"type");
  if ((child != ((xmlnode *)((void *)0))) && ((data = xmlnode_get_data(child)) != ((char *)((void *)0)))) {
    if (purple_strequal(data,"global") != 0) 
      purple_proxy_info_set_type(proxy_info,PURPLE_PROXY_USE_GLOBAL);
    else if (purple_strequal(data,"none") != 0) 
      purple_proxy_info_set_type(proxy_info,PURPLE_PROXY_NONE);
    else if (purple_strequal(data,"http") != 0) 
      purple_proxy_info_set_type(proxy_info,PURPLE_PROXY_HTTP);
    else if (purple_strequal(data,"socks4") != 0) 
      purple_proxy_info_set_type(proxy_info,PURPLE_PROXY_SOCKS4);
    else if (purple_strequal(data,"socks5") != 0) 
      purple_proxy_info_set_type(proxy_info,PURPLE_PROXY_SOCKS5);
    else if (purple_strequal(data,"tor") != 0) 
      purple_proxy_info_set_type(proxy_info,PURPLE_PROXY_TOR);
    else if (purple_strequal(data,"envvar") != 0) 
      purple_proxy_info_set_type(proxy_info,PURPLE_PROXY_USE_ENVVAR);
    else {
      purple_debug_error("account","Invalid proxy type found when loading account information for %s\n",purple_account_get_username(account));
    }
    g_free(data);
  }
/* Read proxy host */
  child = xmlnode_get_child(node,"host");
  if ((child != ((xmlnode *)((void *)0))) && ((data = xmlnode_get_data(child)) != ((char *)((void *)0)))) {
    purple_proxy_info_set_host(proxy_info,data);
    g_free(data);
  }
/* Read proxy port */
  child = xmlnode_get_child(node,"port");
  if ((child != ((xmlnode *)((void *)0))) && ((data = xmlnode_get_data(child)) != ((char *)((void *)0)))) {
    purple_proxy_info_set_port(proxy_info,atoi(data));
    g_free(data);
  }
/* Read proxy username */
  child = xmlnode_get_child(node,"username");
  if ((child != ((xmlnode *)((void *)0))) && ((data = xmlnode_get_data(child)) != ((char *)((void *)0)))) {
    purple_proxy_info_set_username(proxy_info,data);
    g_free(data);
  }
/* Read proxy password */
  child = xmlnode_get_child(node,"password");
  if ((child != ((xmlnode *)((void *)0))) && ((data = xmlnode_get_data(child)) != ((char *)((void *)0)))) {
    purple_proxy_info_set_password(proxy_info,data);
    g_free(data);
  }
/* If there are no values set then proxy_info NULL */
  if ((((((purple_proxy_info_get_type(proxy_info)) == PURPLE_PROXY_USE_GLOBAL) && (purple_proxy_info_get_host(proxy_info) == ((const char *)((void *)0)))) && (purple_proxy_info_get_port(proxy_info) == 0)) && (purple_proxy_info_get_username(proxy_info) == ((const char *)((void *)0)))) && (purple_proxy_info_get_password(proxy_info) == ((const char *)((void *)0)))) {
    purple_proxy_info_destroy(proxy_info);
    return ;
  }
  purple_account_set_proxy_info(account,proxy_info);
}

static void parse_current_error(xmlnode *node,PurpleAccount *account)
{
  guint type;
  char *type_str = (char *)((void *)0);
  char *description = (char *)((void *)0);
  xmlnode *child;
  PurpleConnectionErrorInfo *current_error = (PurpleConnectionErrorInfo *)((void *)0);
  child = xmlnode_get_child(node,"type");
  if ((child == ((xmlnode *)((void *)0))) || ((type_str = xmlnode_get_data(child)) == ((char *)((void *)0)))) 
    return ;
  type = (atoi(type_str));
  g_free(type_str);
  if (type > PURPLE_CONNECTION_ERROR_OTHER_ERROR) {
    purple_debug_error("account","Invalid PurpleConnectionError value %d found when loading account information for %s\n",type,purple_account_get_username(account));
    type = PURPLE_CONNECTION_ERROR_OTHER_ERROR;
  }
  child = xmlnode_get_child(node,"description");
  if (child != 0) 
    description = xmlnode_get_data(child);
  if (description == ((char *)((void *)0))) 
    description = g_strdup("");
  current_error = ((PurpleConnectionErrorInfo *)(g_malloc0_n(1,(sizeof(PurpleConnectionErrorInfo )))));
{
    PurpleConnectionErrorInfo *typed_ptr = current_error;
    purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurpleConnectionErrorInfo);
  };
  current_error -> type = type;
  current_error -> description = description;
  set_current_error(account,current_error);
}

static PurpleAccount *parse_account(xmlnode *node)
{
  PurpleAccount *ret;
  xmlnode *child;
  char *protocol_id = (char *)((void *)0);
  char *name = (char *)((void *)0);
  char *data;
  child = xmlnode_get_child(node,"protocol");
  if (child != ((xmlnode *)((void *)0))) 
    protocol_id = xmlnode_get_data(child);
  child = xmlnode_get_child(node,"name");
  if (child != ((xmlnode *)((void *)0))) 
    name = xmlnode_get_data(child);
  if (name == ((char *)((void *)0))) {
/* Do we really need to do this? */
    child = xmlnode_get_child(node,"username");
    if (child != ((xmlnode *)((void *)0))) 
      name = xmlnode_get_data(child);
  }
  if ((protocol_id == ((char *)((void *)0))) || (name == ((char *)((void *)0)))) {
    g_free(protocol_id);
    g_free(name);
    return 0;
  }
/* XXX: */
  ret = purple_account_new(name,_purple_oscar_convert(name,protocol_id));
  g_free(name);
  g_free(protocol_id);
/* Read the password */
  child = xmlnode_get_child(node,"password");
  if ((child != ((xmlnode *)((void *)0))) && ((data = xmlnode_get_data(child)) != ((char *)((void *)0)))) {
    purple_account_set_remember_password(ret,(!0));
    purple_account_set_password(ret,data);
    g_free(data);
  }
/* Read the alias */
  child = xmlnode_get_child(node,"alias");
  if ((child != ((xmlnode *)((void *)0))) && ((data = xmlnode_get_data(child)) != ((char *)((void *)0)))) {
    if (( *data) != 0) 
      purple_account_set_alias(ret,data);
    g_free(data);
  }
/* Read the statuses */
  child = xmlnode_get_child(node,"statuses");
  if (child != ((xmlnode *)((void *)0))) {
    parse_statuses(child,ret);
  }
/* Read the userinfo */
  child = xmlnode_get_child(node,"userinfo");
  if ((child != ((xmlnode *)((void *)0))) && ((data = xmlnode_get_data(child)) != ((char *)((void *)0)))) {
    purple_account_set_user_info(ret,data);
    g_free(data);
  }
/* Read an old buddyicon */
  child = xmlnode_get_child(node,"buddyicon");
  if ((child != ((xmlnode *)((void *)0))) && ((data = xmlnode_get_data(child)) != ((char *)((void *)0)))) {
    const char *dirname = purple_buddy_icons_get_cache_dir();
    char *filename = g_build_filename(dirname,data,((void *)((void *)0)));
    gchar *contents;
    gsize len;
    if (g_file_get_contents(filename,&contents,&len,0) != 0) {
      purple_buddy_icons_set_account_icon(ret,((guchar *)contents),len);
    }
    else {
/* Try to see if the icon got left behind in the old cache. */
      g_free(filename);
      filename = g_build_filename(g_get_home_dir(),".gaim","icons",data,((void *)((void *)0)));
      if (g_file_get_contents(filename,&contents,&len,0) != 0) {
        purple_buddy_icons_set_account_icon(ret,((guchar *)contents),len);
      }
    }
    g_free(filename);
    g_free(data);
  }
/* Read settings (both core and UI) */
  for (child = xmlnode_get_child(node,"settings"); child != ((xmlnode *)((void *)0)); child = xmlnode_get_next_twin(child)) {
    parse_settings(child,ret);
  }
/* Read proxy */
  child = xmlnode_get_child(node,"proxy");
  if (child != ((xmlnode *)((void *)0))) {
    parse_proxy_info(child,ret);
  }
/* Read current error */
  child = xmlnode_get_child(node,"current_error");
  if (child != ((xmlnode *)((void *)0))) {
    parse_current_error(child,ret);
  }
  return ret;
}

static void load_accounts()
{
  xmlnode *node;
  xmlnode *child;
  accounts_loaded = (!0);
  node = purple_util_read_xml_from_file("accounts.xml",((const char *)(dgettext("pidgin","accounts"))));
  if (node == ((xmlnode *)((void *)0))) 
    return ;
  for (child = xmlnode_get_child(node,"account"); child != ((xmlnode *)((void *)0)); child = xmlnode_get_next_twin(child)) {
    PurpleAccount *new_acct;
    new_acct = parse_account(child);
    purple_accounts_add(new_acct);
  }
  xmlnode_free(node);
  _purple_buddy_icons_account_loaded_cb();
}

static void delete_setting(void *data)
{
  PurpleAccountSetting *setting = (PurpleAccountSetting *)data;
  g_free((setting -> ui));
  if ((setting -> type) == PURPLE_PREF_STRING) 
    g_free(setting -> value.string);
  g_free(setting);
}

PurpleAccount *purple_account_new(const char *username,const char *protocol_id)
{
  PurpleAccount *account = (PurpleAccount *)((void *)0);
  PurpleAccountPrivate *priv = (PurpleAccountPrivate *)((void *)0);
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurpleStatusType *status_type;
  do {
    if (username != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"username != NULL");
      return 0;
    };
  }while (0);
  do {
    if (protocol_id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"protocol_id != NULL");
      return 0;
    };
  }while (0);
  account = purple_accounts_find(username,protocol_id);
  if (account != ((PurpleAccount *)((void *)0))) 
    return account;
  account = ((PurpleAccount *)(g_malloc0_n(1,(sizeof(PurpleAccount )))));
{
    PurpleAccount *typed_ptr = account;
    purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurpleAccount);
  };
  priv = ((PurpleAccountPrivate *)(g_malloc0_n(1,(sizeof(PurpleAccountPrivate )))));
  account -> priv = priv;
  purple_account_set_username(account,username);
  purple_account_set_protocol_id(account,protocol_id);
  account -> settings = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,delete_setting);
  account -> ui_settings = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,((GDestroyNotify )g_hash_table_destroy));
  account -> system_log = ((PurpleLog *)((void *)0));
/* 0 is not a valid privacy setting */
  account -> perm_deny = PURPLE_PRIVACY_ALLOW_ALL;
  purple_signal_emit(purple_accounts_get_handle(),"account-created",account);
  prpl = purple_find_prpl(protocol_id);
  if (prpl == ((PurplePlugin *)((void *)0))) 
    return account;
  prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if ((prpl_info != ((PurplePluginProtocolInfo *)((void *)0))) && ((prpl_info -> status_types) != ((GList *(*)(PurpleAccount *))((void *)0)))) 
    purple_account_set_status_types(account,( *(prpl_info -> status_types))(account));
  account -> presence = purple_presence_new_for_account(account);
  status_type = purple_account_get_status_type_with_primitive(account,PURPLE_STATUS_AVAILABLE);
  if (status_type != ((PurpleStatusType *)((void *)0))) 
    purple_presence_set_status_active((account -> presence),purple_status_type_get_id(status_type),(!0));
  else 
    purple_presence_set_status_active((account -> presence),"offline",(!0));
  return account;
}

void purple_account_destroy(PurpleAccount *account)
{
  PurpleAccountPrivate *priv = (PurpleAccountPrivate *)((void *)0);
  GList *l;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  purple_debug_info("account","Destroying account %p\n",account);
  purple_signal_emit(purple_accounts_get_handle(),"account-destroying",account);
  for (l = purple_get_conversations(); l != ((GList *)((void *)0)); l = (l -> next)) {
    PurpleConversation *conv = (PurpleConversation *)(l -> data);
    if (purple_conversation_get_account(conv) == account) 
      purple_conversation_set_account(conv,0);
  }
  g_free((account -> username));
  g_free((account -> alias));
  g_free((account -> password));
  g_free((account -> user_info));
  g_free((account -> buddy_icon_path));
  g_free((account -> protocol_id));
  g_hash_table_destroy((account -> settings));
  g_hash_table_destroy((account -> ui_settings));
  if ((account -> proxy_info) != 0) 
    purple_proxy_info_destroy((account -> proxy_info));
  purple_account_set_status_types(account,0);
  purple_presence_destroy((account -> presence));
  if ((account -> system_log) != 0) 
    purple_log_free((account -> system_log));
  while((account -> deny) != 0){
    g_free(( *(account -> deny)).data);
    account -> deny = g_slist_delete_link((account -> deny),(account -> deny));
  }
  while((account -> permit) != 0){
    g_free(( *(account -> permit)).data);
    account -> permit = g_slist_delete_link((account -> permit),(account -> permit));
  }
  priv = ((PurpleAccountPrivate *)(account -> priv));
  purple_dbus_unregister_pointer((priv -> current_error));
  if ((priv -> current_error) != 0) {
    g_free(( *(priv -> current_error)).description);
    g_free((priv -> current_error));
  }
  g_free(priv);
  purple_dbus_unregister_pointer(account);
  g_free(account);
}

void purple_account_set_register_callback(PurpleAccount *account,PurpleAccountRegistrationCb cb,void *user_data)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  account -> registration_cb = cb;
  account -> registration_cb_user_data = user_data;
}

void purple_account_register(PurpleAccount *account)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  purple_debug_info("account","Registering account %s\n",purple_account_get_username(account));
  _purple_connection_new(account,(!0),purple_account_get_password(account));
}

void purple_account_unregister(PurpleAccount *account,PurpleAccountUnregistrationCb cb,void *user_data)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  purple_debug_info("account","Unregistering account %s\n",purple_account_get_username(account));
  _purple_connection_new_unregister(account,purple_account_get_password(account),cb,user_data);
}

static void request_password_ok_cb(PurpleAccount *account,PurpleRequestFields *fields)
{
  const char *entry;
  gboolean remember;
  entry = purple_request_fields_get_string(fields,"password");
  remember = purple_request_fields_get_bool(fields,"remember");
  if (!(entry != 0) || !(( *entry) != 0)) {
    purple_notify_message(account,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Password is required to sign on."))),0,0,0);
    return ;
  }
  if (remember != 0) 
    purple_account_set_remember_password(account,(!0));
  purple_account_set_password(account,entry);
  _purple_connection_new(account,0,entry);
}

static void request_password_cancel_cb(PurpleAccount *account,PurpleRequestFields *fields)
{
/* Disable the account as the user has cancelled connecting */
  purple_account_set_enabled(account,purple_core_get_ui(),0);
}

void purple_account_request_password(PurpleAccount *account,GCallback ok_cb,GCallback cancel_cb,void *user_data)
{
  gchar *primary;
  const gchar *username;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  PurpleRequestFields *fields;
/* Close any previous password request windows */
  purple_request_close_with_handle(account);
  username = purple_account_get_username(account);
  primary = g_strdup_printf(((const char *)(dgettext("pidgin","Enter password for %s (%s)"))),username,purple_account_get_protocol_name(account));
  fields = purple_request_fields_new();
  group = purple_request_field_group_new(0);
  purple_request_fields_add_group(fields,group);
  field = purple_request_field_string_new("password",((const char *)(dgettext("pidgin","Enter Password"))),0,0);
  purple_request_field_string_set_masked(field,(!0));
  purple_request_field_set_required(field,(!0));
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_bool_new("remember",((const char *)(dgettext("pidgin","Save password"))),0);
  purple_request_field_group_add_field(group,field);
  purple_request_fields(account,0,primary,0,fields,((const char *)(dgettext("pidgin","OK"))),ok_cb,((const char *)(dgettext("pidgin","Cancel"))),cancel_cb,account,0,0,user_data);
  g_free(primary);
}

void purple_account_connect(PurpleAccount *account)
{
  PurplePlugin *prpl;
  const char *password;
  const char *username;
  PurplePluginProtocolInfo *prpl_info;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  username = purple_account_get_username(account);
  if (!(purple_account_get_enabled(account,purple_core_get_ui()) != 0)) {
    purple_debug_info("account","Account %s not enabled, not connecting.\n",username);
    return ;
  }
  prpl = purple_find_prpl(purple_account_get_protocol_id(account));
  if (prpl == ((PurplePlugin *)((void *)0))) {
    gchar *message;
    message = g_strdup_printf(((const char *)(dgettext("pidgin","Missing protocol plugin for %s"))),username);
    purple_notify_message(account,PURPLE_NOTIFY_MSG_ERROR,((const char *)(dgettext("pidgin","Connection Error"))),message,0,0,0);
    g_free(message);
    return ;
  }
  purple_debug_info("account","Connecting to account %s.\n",username);
  prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  password = purple_account_get_password(account);
  if (((password == ((const char *)((void *)0))) && !(((prpl_info -> options) & OPT_PROTO_NO_PASSWORD) != 0U)) && !(((prpl_info -> options) & OPT_PROTO_PASSWORD_OPTIONAL) != 0U)) 
    purple_account_request_password(account,((GCallback )request_password_ok_cb),((GCallback )request_password_cancel_cb),account);
  else 
    _purple_connection_new(account,0,password);
}

void purple_account_disconnect(PurpleAccount *account)
{
  PurpleConnection *gc;
  const char *username;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (!(purple_account_is_disconnected(account) != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"!purple_account_is_disconnected(account)");
      return ;
    };
  }while (0);
  username = purple_account_get_username(account);
  purple_debug_info("account","Disconnecting account %s (%p)\n",((username != 0)?username : "(null)"),account);
  account -> disconnecting = (!0);
  gc = purple_account_get_connection(account);
  _purple_connection_destroy(gc);
  if (!(purple_account_get_remember_password(account) != 0)) 
    purple_account_set_password(account,0);
  purple_account_set_connection(account,0);
  account -> disconnecting = 0;
}

void purple_account_notify_added(PurpleAccount *account,const char *remote_user,const char *id,const char *alias,const char *message)
{
  PurpleAccountUiOps *ui_ops;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (remote_user != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"remote_user != NULL");
      return ;
    };
  }while (0);
  ui_ops = purple_accounts_get_ui_ops();
  if ((ui_ops != ((PurpleAccountUiOps *)((void *)0))) && ((ui_ops -> notify_added) != ((void (*)(PurpleAccount *, const char *, const char *, const char *, const char *))((void *)0)))) 
    ( *(ui_ops -> notify_added))(account,remote_user,id,alias,message);
}

void purple_account_request_add(PurpleAccount *account,const char *remote_user,const char *id,const char *alias,const char *message)
{
  PurpleAccountUiOps *ui_ops;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (remote_user != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"remote_user != NULL");
      return ;
    };
  }while (0);
  ui_ops = purple_accounts_get_ui_ops();
  if ((ui_ops != ((PurpleAccountUiOps *)((void *)0))) && ((ui_ops -> request_add) != ((void (*)(PurpleAccount *, const char *, const char *, const char *, const char *))((void *)0)))) 
    ( *(ui_ops -> request_add))(account,remote_user,id,alias,message);
}

static PurpleAccountRequestInfo *purple_account_request_info_unref(PurpleAccountRequestInfo *info)
{
  if (--info -> ref != 0U) 
    return info;
/* TODO: This will leak info->user_data, but there is no callback to just clean that up */
  g_free((info -> user));
  g_free(info);
  return 0;
}

static void purple_account_request_close_info(PurpleAccountRequestInfo *info)
{
  PurpleAccountUiOps *ops;
  ops = purple_accounts_get_ui_ops();
  if ((ops != ((PurpleAccountUiOps *)((void *)0))) && ((ops -> close_account_request) != ((void (*)(void *))((void *)0)))) 
    ( *(ops -> close_account_request))((info -> ui_handle));
  purple_account_request_info_unref(info);
}

void purple_account_request_close_with_account(PurpleAccount *account)
{
  GList *l;
  GList *l_next;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  for (l = handles; l != ((GList *)((void *)0)); l = l_next) {
    PurpleAccountRequestInfo *info = (l -> data);
    l_next = (l -> next);
    if ((info -> account) == account) {
      handles = g_list_remove(handles,info);
      purple_account_request_close_info(info);
    }
  }
}

void purple_account_request_close(void *ui_handle)
{
  GList *l;
  GList *l_next;
  do {
    if (ui_handle != ((void *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ui_handle != NULL");
      return ;
    };
  }while (0);
  for (l = handles; l != ((GList *)((void *)0)); l = l_next) {
    PurpleAccountRequestInfo *info = (l -> data);
    l_next = (l -> next);
    if ((info -> ui_handle) == ui_handle) {
      handles = g_list_remove(handles,info);
      purple_account_request_close_info(info);
    }
  }
}

static void request_auth_cb(void *data)
{
  PurpleAccountRequestInfo *info = data;
  handles = g_list_remove(handles,info);
  if ((info -> auth_cb) != ((void (*)(void *))((void *)0))) 
    ( *(info -> auth_cb))((info -> userdata));
  purple_signal_emit(purple_accounts_get_handle(),"account-authorization-granted",(info -> account),(info -> user));
  purple_account_request_info_unref(info);
}

static void request_deny_cb(void *data)
{
  PurpleAccountRequestInfo *info = data;
  handles = g_list_remove(handles,info);
  if ((info -> deny_cb) != ((void (*)(void *))((void *)0))) 
    ( *(info -> deny_cb))((info -> userdata));
  purple_signal_emit(purple_accounts_get_handle(),"account-authorization-denied",(info -> account),(info -> user));
  purple_account_request_info_unref(info);
}

void *purple_account_request_authorization(PurpleAccount *account,const char *remote_user,const char *id,const char *alias,const char *message,gboolean on_list,PurpleAccountRequestAuthorizationCb auth_cb,PurpleAccountRequestAuthorizationCb deny_cb,void *user_data)
{
  PurpleAccountUiOps *ui_ops;
  PurpleAccountRequestInfo *info;
  int plugin_return;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  do {
    if (remote_user != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"remote_user != NULL");
      return 0;
    };
  }while (0);
  ui_ops = purple_accounts_get_ui_ops();
  plugin_return = ((gint )((glong )(purple_signal_emit_return_1(purple_accounts_get_handle(),"account-authorization-requested",account,remote_user))));
  if (plugin_return > 0) {
    if (auth_cb != ((void (*)(void *))((void *)0))) 
      ( *auth_cb)(user_data);
    return 0;
  }
  else if (plugin_return < 0) {
    if (deny_cb != ((void (*)(void *))((void *)0))) 
      ( *deny_cb)(user_data);
    return 0;
  }
  plugin_return = ((gint )((glong )(purple_signal_emit_return_1(purple_accounts_get_handle(),"account-authorization-requested-with-message",account,remote_user,message))));
  switch(plugin_return){
    case -2:
{
      return 0;
    }
    case 1:
{
      if (auth_cb != ((void (*)(void *))((void *)0))) 
        ( *auth_cb)(user_data);
      return 0;
    }
    case -1:
{
      if (deny_cb != ((void (*)(void *))((void *)0))) 
        ( *deny_cb)(user_data);
      return 0;
    }
  }
  if ((ui_ops != ((PurpleAccountUiOps *)((void *)0))) && ((ui_ops -> request_authorize) != ((void *(*)(PurpleAccount *, const char *, const char *, const char *, const char *, gboolean , PurpleAccountRequestAuthorizationCb , PurpleAccountRequestAuthorizationCb , void *))((void *)0)))) {
    info = ((PurpleAccountRequestInfo *)(g_malloc0_n(1,(sizeof(PurpleAccountRequestInfo )))));
    info -> type = PURPLE_ACCOUNT_REQUEST_AUTHORIZATION;
    info -> account = account;
    info -> auth_cb = auth_cb;
    info -> deny_cb = deny_cb;
    info -> userdata = user_data;
    info -> user = g_strdup(remote_user);
/* We hold an extra ref to make sure info remains valid
		                         if any of the callbacks are called synchronously. We
		                         unref it after the function call */
    info -> ref = 2;
    info -> ui_handle = ( *(ui_ops -> request_authorize))(account,remote_user,id,alias,message,on_list,request_auth_cb,request_deny_cb,info);
    info = purple_account_request_info_unref(info);
    if (info != 0) {
      handles = g_list_append(handles,info);
      return info -> ui_handle;
    }
  }
  return 0;
}

static void change_password_cb(PurpleAccount *account,PurpleRequestFields *fields)
{
  const char *orig_pass;
  const char *new_pass_1;
  const char *new_pass_2;
  orig_pass = purple_request_fields_get_string(fields,"password");
  new_pass_1 = purple_request_fields_get_string(fields,"new_password_1");
  new_pass_2 = purple_request_fields_get_string(fields,"new_password_2");
  if (g_utf8_collate(new_pass_1,new_pass_2) != 0) {
    purple_notify_message(account,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","New passwords do not match."))),0,0,0);
    return ;
  }
  if ((((purple_request_fields_is_field_required(fields,"password") != 0) && ((orig_pass == ((const char *)((void *)0))) || (( *orig_pass) == 0))) || ((purple_request_fields_is_field_required(fields,"new_password_1") != 0) && ((new_pass_1 == ((const char *)((void *)0))) || (( *new_pass_1) == 0)))) || ((purple_request_fields_is_field_required(fields,"new_password_2") != 0) && ((new_pass_2 == ((const char *)((void *)0))) || (( *new_pass_2) == 0)))) {
    purple_notify_message(account,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Fill out all fields completely."))),0,0,0);
    return ;
  }
  purple_account_change_password(account,orig_pass,new_pass_1);
}

void purple_account_request_change_password(PurpleAccount *account)
{
  PurpleRequestFields *fields;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  PurpleConnection *gc;
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  char primary[256UL];
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (purple_account_is_connected(account) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_account_is_connected(account)");
      return ;
    };
  }while (0);
  gc = purple_account_get_connection(account);
  if (gc != ((PurpleConnection *)((void *)0))) 
    prpl = purple_connection_get_prpl(gc);
  if (prpl != ((PurplePlugin *)((void *)0))) 
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  fields = purple_request_fields_new();
  group = purple_request_field_group_new(0);
  purple_request_fields_add_group(fields,group);
  field = purple_request_field_string_new("password",((const char *)(dgettext("pidgin","Original password"))),0,0);
  purple_request_field_string_set_masked(field,(!0));
  if (!(prpl_info != 0) || !(((prpl_info -> options) & OPT_PROTO_PASSWORD_OPTIONAL) != 0U)) 
    purple_request_field_set_required(field,(!0));
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_string_new("new_password_1",((const char *)(dgettext("pidgin","New password"))),0,0);
  purple_request_field_string_set_masked(field,(!0));
  if (!(prpl_info != 0) || !(((prpl_info -> options) & OPT_PROTO_PASSWORD_OPTIONAL) != 0U)) 
    purple_request_field_set_required(field,(!0));
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_string_new("new_password_2",((const char *)(dgettext("pidgin","New password (again)"))),0,0);
  purple_request_field_string_set_masked(field,(!0));
  if (!(prpl_info != 0) || !(((prpl_info -> options) & OPT_PROTO_PASSWORD_OPTIONAL) != 0U)) 
    purple_request_field_set_required(field,(!0));
  purple_request_field_group_add_field(group,field);
  g_snprintf(primary,(sizeof(primary)),((const char *)(dgettext("pidgin","Change password for %s"))),purple_account_get_username(account));
/* I'm sticking this somewhere in the code: bologna */
  purple_request_fields((purple_account_get_connection(account)),0,primary,((const char *)(dgettext("pidgin","Please enter your current password and your new password."))),fields,((const char *)(dgettext("pidgin","OK"))),((GCallback )change_password_cb),((const char *)(dgettext("pidgin","Cancel"))),0,account,0,0,account);
}

static void set_user_info_cb(PurpleAccount *account,const char *user_info)
{
  PurpleConnection *gc;
  purple_account_set_user_info(account,user_info);
  gc = purple_account_get_connection(account);
  serv_set_info(gc,user_info);
}

void purple_account_request_change_user_info(PurpleAccount *account)
{
  PurpleConnection *gc;
  char primary[256UL];
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (purple_account_is_connected(account) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_account_is_connected(account)");
      return ;
    };
  }while (0);
  gc = purple_account_get_connection(account);
  g_snprintf(primary,(sizeof(primary)),((const char *)(dgettext("pidgin","Change user information for %s"))),purple_account_get_username(account));
  purple_request_input(gc,((const char *)(dgettext("pidgin","Set User Info"))),primary,0,purple_account_get_user_info(account),(!0),0,(((gc != ((PurpleConnection *)((void *)0))) && (((gc -> flags) & PURPLE_CONNECTION_HTML) != 0U))?"html" : ((char *)((void *)0))),((const char *)(dgettext("pidgin","Save"))),((GCallback )set_user_info_cb),((const char *)(dgettext("pidgin","Cancel"))),0,account,0,0,account);
}

void purple_account_set_username(PurpleAccount *account,const char *username)
{
  PurpleBlistUiOps *blist_ops;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  g_free((account -> username));
  account -> username = g_strdup(username);
  schedule_accounts_save();
/* if the name changes, we should re-write the buddy list
	 * to disk with the new name */
  blist_ops = purple_blist_get_ui_ops();
  if ((blist_ops != ((PurpleBlistUiOps *)((void *)0))) && ((blist_ops -> save_account) != ((void (*)(PurpleAccount *))((void *)0)))) 
    ( *(blist_ops -> save_account))(account);
}

void purple_account_set_password(PurpleAccount *account,const char *password)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  g_free((account -> password));
  account -> password = g_strdup(password);
  schedule_accounts_save();
}

void purple_account_set_alias(PurpleAccount *account,const char *alias)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
/*
	 * Do nothing if alias and account->alias are both NULL.  Or if
	 * they're the exact same string.
	 */
  if (alias == (account -> alias)) 
    return ;
  if (((!(alias != 0) && ((account -> alias) != 0)) || ((alias != 0) && !((account -> alias) != 0))) || (g_utf8_collate((account -> alias),alias) != 0)) {
    char *old = (account -> alias);
    account -> alias = g_strdup(alias);
    purple_signal_emit(purple_accounts_get_handle(),"account-alias-changed",account,old);
    g_free(old);
    schedule_accounts_save();
  }
}

void purple_account_set_user_info(PurpleAccount *account,const char *user_info)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  g_free((account -> user_info));
  account -> user_info = g_strdup(user_info);
  schedule_accounts_save();
}

void purple_account_set_buddy_icon_path(PurpleAccount *account,const char *path)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  g_free((account -> buddy_icon_path));
  account -> buddy_icon_path = g_strdup(path);
  schedule_accounts_save();
}

void purple_account_set_protocol_id(PurpleAccount *account,const char *protocol_id)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (protocol_id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"protocol_id != NULL");
      return ;
    };
  }while (0);
  g_free((account -> protocol_id));
  account -> protocol_id = g_strdup(protocol_id);
  schedule_accounts_save();
}

void purple_account_set_connection(PurpleAccount *account,PurpleConnection *gc)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  account -> gc = gc;
}

void purple_account_set_remember_password(PurpleAccount *account,gboolean value)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  account -> remember_pass = value;
  schedule_accounts_save();
}

void purple_account_set_check_mail(PurpleAccount *account,gboolean value)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  purple_account_set_bool(account,"check-mail",value);
}

void purple_account_set_enabled(PurpleAccount *account,const char *ui,gboolean value)
{
  PurpleConnection *gc;
  gboolean was_enabled = 0;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (ui != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ui != NULL");
      return ;
    };
  }while (0);
  was_enabled = purple_account_get_enabled(account,ui);
  purple_account_set_ui_bool(account,ui,"auto-login",value);
  gc = purple_account_get_connection(account);
  if ((was_enabled != 0) && !(value != 0)) 
    purple_signal_emit(purple_accounts_get_handle(),"account-disabled",account);
  else if (!(was_enabled != 0) && (value != 0)) 
    purple_signal_emit(purple_accounts_get_handle(),"account-enabled",account);
  if ((gc != ((PurpleConnection *)((void *)0))) && ((gc -> wants_to_die) == !0)) 
    return ;
  if ((value != 0) && (purple_presence_is_online((account -> presence)) != 0)) 
    purple_account_connect(account);
  else if (!(value != 0) && !(purple_account_is_disconnected(account) != 0)) 
    purple_account_disconnect(account);
}

void purple_account_set_proxy_info(PurpleAccount *account,PurpleProxyInfo *info)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  if ((account -> proxy_info) != ((PurpleProxyInfo *)((void *)0))) 
    purple_proxy_info_destroy((account -> proxy_info));
  account -> proxy_info = info;
  schedule_accounts_save();
}

void purple_account_set_privacy_type(PurpleAccount *account,PurplePrivacyType privacy_type)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  account -> perm_deny = privacy_type;
}

void purple_account_set_status_types(PurpleAccount *account,GList *status_types)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
/* Out with the old... */
  if ((account -> status_types) != ((GList *)((void *)0))) {
    g_list_foreach((account -> status_types),((GFunc )purple_status_type_destroy),0);
    g_list_free((account -> status_types));
  }
/* In with the new... */
  account -> status_types = status_types;
}

void purple_account_set_status(PurpleAccount *account,const char *status_id,gboolean active,... )
{
  GList *attrs = (GList *)((void *)0);
  const gchar *id;
  gpointer data;
  va_list args;
  va_start(args,active);
  while((id = (va_arg(args,const char *))) != ((const gchar *)((void *)0))){
    attrs = g_list_append(attrs,((char *)id));
    data = (va_arg(args,void *));
    attrs = g_list_append(attrs,data);
  }
  purple_account_set_status_list(account,status_id,active,attrs);
  g_list_free(attrs);
  va_end(args);
}

void purple_account_set_status_list(PurpleAccount *account,const char *status_id,gboolean active,GList *attrs)
{
  PurpleStatus *status;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (status_id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"status_id != NULL");
      return ;
    };
  }while (0);
  status = purple_account_get_status(account,status_id);
  if (status == ((PurpleStatus *)((void *)0))) {
    purple_debug_error("account","Invalid status ID \'%s\' for account %s (%s)\n",status_id,purple_account_get_username(account),purple_account_get_protocol_id(account));
    return ;
  }
  if ((active != 0) || (purple_status_is_independent(status) != 0)) 
    purple_status_set_active_with_attrs_list(status,active,attrs);
/*
	 * Our current statuses are saved to accounts.xml (so that when we
	 * reconnect, we go back to the previous status).
	 */
  schedule_accounts_save();
}

struct public_alias_closure 
{
  PurpleAccount *account;
  gpointer failure_cb;
}
;

static gboolean set_public_alias_unsupported(gpointer data)
{
  struct public_alias_closure *closure = data;
  PurpleSetPublicAliasFailureCallback failure_cb = (closure -> failure_cb);
  ( *failure_cb)((closure -> account),((const char *)(dgettext("pidgin","This protocol does not support setting a public alias."))));
  g_free(closure);
  return 0;
}

void purple_account_set_public_alias(PurpleAccount *account,const char *alias,PurpleSetPublicAliasSuccessCallback success_cb,PurpleSetPublicAliasFailureCallback failure_cb)
{
  PurpleConnection *gc;
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (purple_account_is_connected(account) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_account_is_connected(account)");
      return ;
    };
  }while (0);
  gc = purple_account_get_connection(account);
  prpl = purple_connection_get_prpl(gc);
  prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).set_public_alias))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).set_public_alias))) < (prpl_info -> struct_size))) && ((prpl_info -> set_public_alias) != ((void (*)(PurpleConnection *, const char *, PurpleSetPublicAliasSuccessCallback , PurpleSetPublicAliasFailureCallback ))((void *)0)))) 
    ( *(prpl_info -> set_public_alias))(gc,alias,success_cb,failure_cb);
  else if (failure_cb != 0) {
    struct public_alias_closure *closure = (struct public_alias_closure *)(g_malloc0_n(1,(sizeof(struct public_alias_closure ))));
    closure -> account = account;
    closure -> failure_cb = failure_cb;
    purple_timeout_add(0,set_public_alias_unsupported,closure);
  }
}

static gboolean get_public_alias_unsupported(gpointer data)
{
  struct public_alias_closure *closure = data;
  PurpleGetPublicAliasFailureCallback failure_cb = (closure -> failure_cb);
  ( *failure_cb)((closure -> account),((const char *)(dgettext("pidgin","This protocol does not support fetching the public alias."))));
  g_free(closure);
  return 0;
}

void purple_account_get_public_alias(PurpleAccount *account,PurpleGetPublicAliasSuccessCallback success_cb,PurpleGetPublicAliasFailureCallback failure_cb)
{
  PurpleConnection *gc;
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (purple_account_is_connected(account) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_account_is_connected(account)");
      return ;
    };
  }while (0);
  gc = purple_account_get_connection(account);
  prpl = purple_connection_get_prpl(gc);
  prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).get_public_alias))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).get_public_alias))) < (prpl_info -> struct_size))) && ((prpl_info -> get_public_alias) != ((void (*)(PurpleConnection *, PurpleGetPublicAliasSuccessCallback , PurpleGetPublicAliasFailureCallback ))((void *)0)))) 
    ( *(prpl_info -> get_public_alias))(gc,success_cb,failure_cb);
  else if (failure_cb != 0) {
    struct public_alias_closure *closure = (struct public_alias_closure *)(g_malloc0_n(1,(sizeof(struct public_alias_closure ))));
    closure -> account = account;
    closure -> failure_cb = failure_cb;
    purple_timeout_add(0,get_public_alias_unsupported,closure);
  }
}

gboolean purple_account_get_silence_suppression(const PurpleAccount *account)
{
  return purple_account_get_bool(account,"silence-suppression",0);
}

void purple_account_set_silence_suppression(PurpleAccount *account,gboolean value)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  purple_account_set_bool(account,"silence-suppression",value);
}

void purple_account_clear_settings(PurpleAccount *account)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  g_hash_table_destroy((account -> settings));
  account -> settings = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,delete_setting);
}

void purple_account_remove_setting(PurpleAccount *account,const char *setting)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (setting != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"setting != NULL");
      return ;
    };
  }while (0);
  g_hash_table_remove((account -> settings),setting);
}

void purple_account_set_int(PurpleAccount *account,const char *name,int value)
{
  PurpleAccountSetting *setting;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return ;
    };
  }while (0);
  setting = ((PurpleAccountSetting *)(g_malloc0_n(1,(sizeof(PurpleAccountSetting )))));
  setting -> type = PURPLE_PREF_INT;
  setting -> value.integer = value;
  g_hash_table_insert((account -> settings),(g_strdup(name)),setting);
  schedule_accounts_save();
}

void purple_account_set_string(PurpleAccount *account,const char *name,const char *value)
{
  PurpleAccountSetting *setting;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return ;
    };
  }while (0);
  setting = ((PurpleAccountSetting *)(g_malloc0_n(1,(sizeof(PurpleAccountSetting )))));
  setting -> type = PURPLE_PREF_STRING;
  setting -> value.string = g_strdup(value);
  g_hash_table_insert((account -> settings),(g_strdup(name)),setting);
  schedule_accounts_save();
}

void purple_account_set_bool(PurpleAccount *account,const char *name,gboolean value)
{
  PurpleAccountSetting *setting;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return ;
    };
  }while (0);
  setting = ((PurpleAccountSetting *)(g_malloc0_n(1,(sizeof(PurpleAccountSetting )))));
  setting -> type = PURPLE_PREF_BOOLEAN;
  setting -> value.boolean = value;
  g_hash_table_insert((account -> settings),(g_strdup(name)),setting);
  schedule_accounts_save();
}

static GHashTable *get_ui_settings_table(PurpleAccount *account,const char *ui)
{
  GHashTable *table;
  table = (g_hash_table_lookup((account -> ui_settings),ui));
  if (table == ((GHashTable *)((void *)0))) {
    table = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,delete_setting);
    g_hash_table_insert((account -> ui_settings),(g_strdup(ui)),table);
  }
  return table;
}

void purple_account_set_ui_int(PurpleAccount *account,const char *ui,const char *name,int value)
{
  PurpleAccountSetting *setting;
  GHashTable *table;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (ui != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ui != NULL");
      return ;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return ;
    };
  }while (0);
  setting = ((PurpleAccountSetting *)(g_malloc0_n(1,(sizeof(PurpleAccountSetting )))));
  setting -> type = PURPLE_PREF_INT;
  setting -> ui = g_strdup(ui);
  setting -> value.integer = value;
  table = get_ui_settings_table(account,ui);
  g_hash_table_insert(table,(g_strdup(name)),setting);
  schedule_accounts_save();
}

void purple_account_set_ui_string(PurpleAccount *account,const char *ui,const char *name,const char *value)
{
  PurpleAccountSetting *setting;
  GHashTable *table;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (ui != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ui != NULL");
      return ;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return ;
    };
  }while (0);
  setting = ((PurpleAccountSetting *)(g_malloc0_n(1,(sizeof(PurpleAccountSetting )))));
  setting -> type = PURPLE_PREF_STRING;
  setting -> ui = g_strdup(ui);
  setting -> value.string = g_strdup(value);
  table = get_ui_settings_table(account,ui);
  g_hash_table_insert(table,(g_strdup(name)),setting);
  schedule_accounts_save();
}

void purple_account_set_ui_bool(PurpleAccount *account,const char *ui,const char *name,gboolean value)
{
  PurpleAccountSetting *setting;
  GHashTable *table;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (ui != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ui != NULL");
      return ;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return ;
    };
  }while (0);
  setting = ((PurpleAccountSetting *)(g_malloc0_n(1,(sizeof(PurpleAccountSetting )))));
  setting -> type = PURPLE_PREF_BOOLEAN;
  setting -> ui = g_strdup(ui);
  setting -> value.boolean = value;
  table = get_ui_settings_table(account,ui);
  g_hash_table_insert(table,(g_strdup(name)),setting);
  schedule_accounts_save();
}

static PurpleConnectionState purple_account_get_state(const PurpleAccount *account)
{
  PurpleConnection *gc;
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return PURPLE_DISCONNECTED;
    };
  }while (0);
  gc = purple_account_get_connection(account);
  if (!(gc != 0)) 
    return PURPLE_DISCONNECTED;
  return purple_connection_get_state(gc);
}

gboolean purple_account_is_connected(const PurpleAccount *account)
{
  return (purple_account_get_state(account)) == PURPLE_CONNECTED;
}

gboolean purple_account_is_connecting(const PurpleAccount *account)
{
  return (purple_account_get_state(account)) == PURPLE_CONNECTING;
}

gboolean purple_account_is_disconnected(const PurpleAccount *account)
{
  return (purple_account_get_state(account)) == PURPLE_DISCONNECTED;
}

const char *purple_account_get_username(const PurpleAccount *account)
{
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  return (account -> username);
}

const char *purple_account_get_password(const PurpleAccount *account)
{
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  return (account -> password);
}

const char *purple_account_get_alias(const PurpleAccount *account)
{
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  return (account -> alias);
}

const char *purple_account_get_user_info(const PurpleAccount *account)
{
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  return (account -> user_info);
}

const char *purple_account_get_buddy_icon_path(const PurpleAccount *account)
{
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  return (account -> buddy_icon_path);
}

const char *purple_account_get_protocol_id(const PurpleAccount *account)
{
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  return (account -> protocol_id);
}

const char *purple_account_get_protocol_name(const PurpleAccount *account)
{
  PurplePlugin *p;
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  p = purple_find_prpl(purple_account_get_protocol_id(account));
  return ((p != 0) && (( *(p -> info)).name != 0))?((const char *)(dgettext("pidgin",( *(p -> info)).name))) : ((const char *)(dgettext("pidgin","Unknown")));
}

PurpleConnection *purple_account_get_connection(const PurpleAccount *account)
{
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  return account -> gc;
}

const gchar *purple_account_get_name_for_display(const PurpleAccount *account)
{
  PurpleBuddy *self = (PurpleBuddy *)((void *)0);
  PurpleConnection *gc = (PurpleConnection *)((void *)0);
  const gchar *name = (const gchar *)((void *)0);
  const gchar *username = (const gchar *)((void *)0);
  const gchar *displayname = (const gchar *)((void *)0);
  name = purple_account_get_alias(account);
  if (name != 0) {
    return name;
  }
  username = purple_account_get_username(account);
  self = purple_find_buddy(((PurpleAccount *)account),username);
  if (self != 0) {
    const gchar *calias = purple_buddy_get_contact_alias(self);
/* We don't want to return the buddy name if the buddy/contact
		 * doesn't have an alias set. */
    if (!(purple_strequal(username,calias) != 0)) {
      return calias;
    }
  }
  gc = purple_account_get_connection(account);
  displayname = purple_connection_get_display_name(gc);
  if (displayname != 0) {
    return displayname;
  }
  return username;
}

gboolean purple_account_get_remember_password(const PurpleAccount *account)
{
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  return account -> remember_pass;
}

gboolean purple_account_get_check_mail(const PurpleAccount *account)
{
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  return purple_account_get_bool(account,"check-mail",0);
}

gboolean purple_account_get_enabled(const PurpleAccount *account,const char *ui)
{
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  do {
    if (ui != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ui != NULL");
      return 0;
    };
  }while (0);
  return purple_account_get_ui_bool(account,ui,"auto-login",0);
}

PurpleProxyInfo *purple_account_get_proxy_info(const PurpleAccount *account)
{
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  return account -> proxy_info;
}

PurplePrivacyType purple_account_get_privacy_type(const PurpleAccount *account)
{
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return PURPLE_PRIVACY_ALLOW_ALL;
    };
  }while (0);
  return account -> perm_deny;
}

PurpleStatus *purple_account_get_active_status(const PurpleAccount *account)
{
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  return purple_presence_get_active_status((account -> presence));
}

PurpleStatus *purple_account_get_status(const PurpleAccount *account,const char *status_id)
{
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  do {
    if (status_id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"status_id != NULL");
      return 0;
    };
  }while (0);
  return purple_presence_get_status((account -> presence),status_id);
}

PurpleStatusType *purple_account_get_status_type(const PurpleAccount *account,const char *id)
{
  GList *l;
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  for (l = purple_account_get_status_types(account); l != ((GList *)((void *)0)); l = (l -> next)) {
    PurpleStatusType *status_type = (PurpleStatusType *)(l -> data);
    if (purple_strequal(purple_status_type_get_id(status_type),id) != 0) 
      return status_type;
  }
  return 0;
}

PurpleStatusType *purple_account_get_status_type_with_primitive(const PurpleAccount *account,PurpleStatusPrimitive primitive)
{
  GList *l;
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  for (l = purple_account_get_status_types(account); l != ((GList *)((void *)0)); l = (l -> next)) {
    PurpleStatusType *status_type = (PurpleStatusType *)(l -> data);
    if ((purple_status_type_get_primitive(status_type)) == primitive) 
      return status_type;
  }
  return 0;
}

PurplePresence *purple_account_get_presence(const PurpleAccount *account)
{
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  return account -> presence;
}

gboolean purple_account_is_status_active(const PurpleAccount *account,const char *status_id)
{
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  do {
    if (status_id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"status_id != NULL");
      return 0;
    };
  }while (0);
  return purple_presence_is_status_active((account -> presence),status_id);
}

GList *purple_account_get_status_types(const PurpleAccount *account)
{
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  return account -> status_types;
}

int purple_account_get_int(const PurpleAccount *account,const char *name,int default_value)
{
  PurpleAccountSetting *setting;
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return default_value;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return default_value;
    };
  }while (0);
  setting = (g_hash_table_lookup((account -> settings),name));
  if (setting == ((PurpleAccountSetting *)((void *)0))) 
    return default_value;
  do {
    if ((setting -> type) == PURPLE_PREF_INT) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"setting->type == PURPLE_PREF_INT");
      return default_value;
    };
  }while (0);
  return setting -> value.integer;
}

const char *purple_account_get_string(const PurpleAccount *account,const char *name,const char *default_value)
{
  PurpleAccountSetting *setting;
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return default_value;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return default_value;
    };
  }while (0);
  setting = (g_hash_table_lookup((account -> settings),name));
  if (setting == ((PurpleAccountSetting *)((void *)0))) 
    return default_value;
  do {
    if ((setting -> type) == PURPLE_PREF_STRING) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"setting->type == PURPLE_PREF_STRING");
      return default_value;
    };
  }while (0);
  return setting -> value.string;
}

gboolean purple_account_get_bool(const PurpleAccount *account,const char *name,gboolean default_value)
{
  PurpleAccountSetting *setting;
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return default_value;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return default_value;
    };
  }while (0);
  setting = (g_hash_table_lookup((account -> settings),name));
  if (setting == ((PurpleAccountSetting *)((void *)0))) 
    return default_value;
  do {
    if ((setting -> type) == PURPLE_PREF_BOOLEAN) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"setting->type == PURPLE_PREF_BOOLEAN");
      return default_value;
    };
  }while (0);
  return setting -> value.boolean;
}

int purple_account_get_ui_int(const PurpleAccount *account,const char *ui,const char *name,int default_value)
{
  PurpleAccountSetting *setting;
  GHashTable *table;
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return default_value;
    };
  }while (0);
  do {
    if (ui != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ui != NULL");
      return default_value;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return default_value;
    };
  }while (0);
  if ((table = (g_hash_table_lookup((account -> ui_settings),ui))) == ((GHashTable *)((void *)0))) 
    return default_value;
  if ((setting = (g_hash_table_lookup(table,name))) == ((PurpleAccountSetting *)((void *)0))) 
    return default_value;
  do {
    if ((setting -> type) == PURPLE_PREF_INT) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"setting->type == PURPLE_PREF_INT");
      return default_value;
    };
  }while (0);
  return setting -> value.integer;
}

const char *purple_account_get_ui_string(const PurpleAccount *account,const char *ui,const char *name,const char *default_value)
{
  PurpleAccountSetting *setting;
  GHashTable *table;
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return default_value;
    };
  }while (0);
  do {
    if (ui != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ui != NULL");
      return default_value;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return default_value;
    };
  }while (0);
  if ((table = (g_hash_table_lookup((account -> ui_settings),ui))) == ((GHashTable *)((void *)0))) 
    return default_value;
  if ((setting = (g_hash_table_lookup(table,name))) == ((PurpleAccountSetting *)((void *)0))) 
    return default_value;
  do {
    if ((setting -> type) == PURPLE_PREF_STRING) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"setting->type == PURPLE_PREF_STRING");
      return default_value;
    };
  }while (0);
  return setting -> value.string;
}

gboolean purple_account_get_ui_bool(const PurpleAccount *account,const char *ui,const char *name,gboolean default_value)
{
  PurpleAccountSetting *setting;
  GHashTable *table;
  do {
    if (account != ((const PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return default_value;
    };
  }while (0);
  do {
    if (ui != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ui != NULL");
      return default_value;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return default_value;
    };
  }while (0);
  if ((table = (g_hash_table_lookup((account -> ui_settings),ui))) == ((GHashTable *)((void *)0))) 
    return default_value;
  if ((setting = (g_hash_table_lookup(table,name))) == ((PurpleAccountSetting *)((void *)0))) 
    return default_value;
  do {
    if ((setting -> type) == PURPLE_PREF_BOOLEAN) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"setting->type == PURPLE_PREF_BOOLEAN");
      return default_value;
    };
  }while (0);
  return setting -> value.boolean;
}

PurpleLog *purple_account_get_log(PurpleAccount *account,gboolean create)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  if (!((account -> system_log) != 0) && (create != 0)) {
    PurplePresence *presence;
    int login_time;
    presence = purple_account_get_presence(account);
    login_time = (purple_presence_get_login_time(presence));
    account -> system_log = purple_log_new(PURPLE_LOG_SYSTEM,purple_account_get_username(account),account,0,((login_time != 0)?login_time : time(0)),0);
  }
  return account -> system_log;
}

void purple_account_destroy_log(PurpleAccount *account)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  if ((account -> system_log) != 0) {
    purple_log_free((account -> system_log));
    account -> system_log = ((PurpleLog *)((void *)0));
  }
}

void purple_account_add_buddy(PurpleAccount *account,PurpleBuddy *buddy)
{
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurpleConnection *gc;
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return ;
    };
  }while (0);
  gc = purple_account_get_connection(account);
  if (gc != ((PurpleConnection *)((void *)0))) 
    prpl = purple_connection_get_prpl(gc);
  if (prpl != ((PurplePlugin *)((void *)0))) 
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if (prpl_info != ((PurplePluginProtocolInfo *)((void *)0))) {
    if (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddy_with_invite))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddy_with_invite))) < (prpl_info -> struct_size))) && ((prpl_info -> add_buddy_with_invite) != ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *, const char *))((void *)0)))) 
      ( *(prpl_info -> add_buddy_with_invite))(gc,buddy,purple_buddy_get_group(buddy),0);
    else if (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddy))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddy))) < (prpl_info -> struct_size))) && ((prpl_info -> add_buddy) != ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *))((void *)0)))) 
      ( *(prpl_info -> add_buddy))(gc,buddy,purple_buddy_get_group(buddy));
  }
}

void purple_account_add_buddy_with_invite(PurpleAccount *account,PurpleBuddy *buddy,const char *message)
{
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurpleConnection *gc;
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return ;
    };
  }while (0);
  gc = purple_account_get_connection(account);
  if (gc != ((PurpleConnection *)((void *)0))) 
    prpl = purple_connection_get_prpl(gc);
  if (prpl != ((PurplePlugin *)((void *)0))) 
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if (prpl_info != ((PurplePluginProtocolInfo *)((void *)0))) {
    if (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddy_with_invite))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddy_with_invite))) < (prpl_info -> struct_size))) && ((prpl_info -> add_buddy_with_invite) != ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *, const char *))((void *)0)))) 
      ( *(prpl_info -> add_buddy_with_invite))(gc,buddy,purple_buddy_get_group(buddy),message);
    else if (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddy))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddy))) < (prpl_info -> struct_size))) && ((prpl_info -> add_buddy) != ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *))((void *)0)))) 
      ( *(prpl_info -> add_buddy))(gc,buddy,purple_buddy_get_group(buddy));
  }
}

void purple_account_add_buddies(PurpleAccount *account,GList *buddies)
{
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurpleConnection *gc = purple_account_get_connection(account);
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  if (gc != ((PurpleConnection *)((void *)0))) 
    prpl = purple_connection_get_prpl(gc);
  if (prpl != ((PurplePlugin *)((void *)0))) 
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if (prpl_info != 0) {
    GList *cur;
    GList *groups = (GList *)((void *)0);
/* Make a list of what group each buddy is in */
    for (cur = buddies; cur != ((GList *)((void *)0)); cur = (cur -> next)) {
      PurpleBuddy *buddy = (cur -> data);
      groups = g_list_append(groups,(purple_buddy_get_group(buddy)));
    }
    if (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddies_with_invite))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddies_with_invite))) < (prpl_info -> struct_size))) && ((prpl_info -> add_buddies_with_invite) != ((void (*)(PurpleConnection *, GList *, GList *, const char *))((void *)0)))) 
      ( *(prpl_info -> add_buddies_with_invite))(gc,buddies,groups,0);
    else if (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddies))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddies))) < (prpl_info -> struct_size))) && ((prpl_info -> add_buddies) != ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)))) 
      ( *(prpl_info -> add_buddies))(gc,buddies,groups);
    else if (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddy_with_invite))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddy_with_invite))) < (prpl_info -> struct_size))) && ((prpl_info -> add_buddy_with_invite) != ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *, const char *))((void *)0)))) {
      GList *curb = buddies;
      GList *curg = groups;
      while((curb != ((GList *)((void *)0))) && (curg != ((GList *)((void *)0)))){
        ( *(prpl_info -> add_buddy_with_invite))(gc,(curb -> data),(curg -> data),0);
        curb = (curb -> next);
        curg = (curg -> next);
      }
    }
    else if (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddy))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddy))) < (prpl_info -> struct_size))) && ((prpl_info -> add_buddy) != ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *))((void *)0)))) {
      GList *curb = buddies;
      GList *curg = groups;
      while((curb != ((GList *)((void *)0))) && (curg != ((GList *)((void *)0)))){
        ( *(prpl_info -> add_buddy))(gc,(curb -> data),(curg -> data));
        curb = (curb -> next);
        curg = (curg -> next);
      }
    }
    g_list_free(groups);
  }
}

void purple_account_add_buddies_with_invite(PurpleAccount *account,GList *buddies,const char *message)
{
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurpleConnection *gc = purple_account_get_connection(account);
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  if (gc != ((PurpleConnection *)((void *)0))) 
    prpl = purple_connection_get_prpl(gc);
  if (prpl != ((PurplePlugin *)((void *)0))) 
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if (prpl_info != 0) {
    GList *cur;
    GList *groups = (GList *)((void *)0);
/* Make a list of what group each buddy is in */
    for (cur = buddies; cur != ((GList *)((void *)0)); cur = (cur -> next)) {
      PurpleBuddy *buddy = (cur -> data);
      groups = g_list_append(groups,(purple_buddy_get_group(buddy)));
    }
    if (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddies_with_invite))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddies_with_invite))) < (prpl_info -> struct_size))) && ((prpl_info -> add_buddies_with_invite) != ((void (*)(PurpleConnection *, GList *, GList *, const char *))((void *)0)))) 
      ( *(prpl_info -> add_buddies_with_invite))(gc,buddies,groups,message);
    else if (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddy_with_invite))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddy_with_invite))) < (prpl_info -> struct_size))) && ((prpl_info -> add_buddy_with_invite) != ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *, const char *))((void *)0)))) {
      GList *curb = buddies;
      GList *curg = groups;
      while((curb != ((GList *)((void *)0))) && (curg != ((GList *)((void *)0)))){
        ( *(prpl_info -> add_buddy_with_invite))(gc,(curb -> data),(curg -> data),message);
        curb = (curb -> next);
        curg = (curg -> next);
      }
    }
    else if (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddies))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddies))) < (prpl_info -> struct_size))) && ((prpl_info -> add_buddies) != ((void (*)(PurpleConnection *, GList *, GList *))((void *)0)))) 
      ( *(prpl_info -> add_buddies))(gc,buddies,groups);
    else if (((((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddy))) < ((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).struct_size)))) || (((glong )((size_t )(&( *((PurplePluginProtocolInfo *)((PurplePluginProtocolInfo *)0))).add_buddy))) < (prpl_info -> struct_size))) && ((prpl_info -> add_buddy) != ((void (*)(PurpleConnection *, PurpleBuddy *, PurpleGroup *))((void *)0)))) {
      GList *curb = buddies;
      GList *curg = groups;
      while((curb != ((GList *)((void *)0))) && (curg != ((GList *)((void *)0)))){
        ( *(prpl_info -> add_buddy))(gc,(curb -> data),(curg -> data));
        curb = (curb -> next);
        curg = (curg -> next);
      }
    }
    g_list_free(groups);
  }
}

void purple_account_remove_buddy(PurpleAccount *account,PurpleBuddy *buddy,PurpleGroup *group)
{
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurpleConnection *gc = purple_account_get_connection(account);
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  if (gc != ((PurpleConnection *)((void *)0))) 
    prpl = purple_connection_get_prpl(gc);
  if (prpl != ((PurplePlugin *)((void *)0))) 
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if ((prpl_info != 0) && ((prpl_info -> remove_buddy) != 0)) 
    ( *(prpl_info -> remove_buddy))(gc,buddy,group);
}

void purple_account_remove_buddies(PurpleAccount *account,GList *buddies,GList *groups)
{
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurpleConnection *gc = purple_account_get_connection(account);
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  if (gc != ((PurpleConnection *)((void *)0))) 
    prpl = purple_connection_get_prpl(gc);
  if (prpl != ((PurplePlugin *)((void *)0))) 
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if (prpl_info != 0) {
    if ((prpl_info -> remove_buddies) != 0) 
      ( *(prpl_info -> remove_buddies))(gc,buddies,groups);
    else {
      GList *curb = buddies;
      GList *curg = groups;
      while((curb != ((GList *)((void *)0))) && (curg != ((GList *)((void *)0)))){
        purple_account_remove_buddy(account,(curb -> data),(curg -> data));
        curb = (curb -> next);
        curg = (curg -> next);
      }
    }
  }
}

void purple_account_remove_group(PurpleAccount *account,PurpleGroup *group)
{
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurpleConnection *gc = purple_account_get_connection(account);
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  if (gc != ((PurpleConnection *)((void *)0))) 
    prpl = purple_connection_get_prpl(gc);
  if (prpl != ((PurplePlugin *)((void *)0))) 
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if ((prpl_info != 0) && ((prpl_info -> remove_group) != 0)) 
    ( *(prpl_info -> remove_group))(gc,group);
}

void purple_account_change_password(PurpleAccount *account,const char *orig_pw,const char *new_pw)
{
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurpleConnection *gc = purple_account_get_connection(account);
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  purple_account_set_password(account,new_pw);
  if (gc != ((PurpleConnection *)((void *)0))) 
    prpl = purple_connection_get_prpl(gc);
  if (prpl != ((PurplePlugin *)((void *)0))) 
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if ((prpl_info != 0) && ((prpl_info -> change_passwd) != 0)) 
    ( *(prpl_info -> change_passwd))(gc,orig_pw,new_pw);
}

gboolean purple_account_supports_offline_message(PurpleAccount *account,PurpleBuddy *buddy)
{
  PurpleConnection *gc;
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurplePlugin *prpl = (PurplePlugin *)((void *)0);
  do {
    if (account != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account");
      return 0;
    };
  }while (0);
  do {
    if (buddy != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy");
      return 0;
    };
  }while (0);
  gc = purple_account_get_connection(account);
  if (gc == ((PurpleConnection *)((void *)0))) 
    return 0;
  prpl = purple_connection_get_prpl(gc);
  if (prpl != ((PurplePlugin *)((void *)0))) 
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if (!(prpl_info != 0) || !((prpl_info -> offline_message) != 0)) 
    return 0;
  return ( *(prpl_info -> offline_message))(buddy);
}

static void signed_on_cb(PurpleConnection *gc,gpointer unused)
{
  PurpleAccount *account = purple_connection_get_account(gc);
  purple_account_clear_current_error(account);
  purple_signal_emit(purple_accounts_get_handle(),"account-signed-on",account);
}

static void signed_off_cb(PurpleConnection *gc,gpointer unused)
{
  PurpleAccount *account = purple_connection_get_account(gc);
  purple_signal_emit(purple_accounts_get_handle(),"account-signed-off",account);
}

static void set_current_error(PurpleAccount *account,PurpleConnectionErrorInfo *new_err)
{
  PurpleAccountPrivate *priv;
  PurpleConnectionErrorInfo *old_err;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  priv = ((PurpleAccountPrivate *)(account -> priv));
  old_err = (priv -> current_error);
  if (new_err == old_err) 
    return ;
  priv -> current_error = new_err;
  purple_signal_emit(purple_accounts_get_handle(),"account-error-changed",account,old_err,new_err);
  schedule_accounts_save();
  if (old_err != 0) 
    g_free((old_err -> description));
  purple_dbus_unregister_pointer(old_err);
  g_free(old_err);
}

static void connection_error_cb(PurpleConnection *gc,PurpleConnectionError type,const gchar *description,gpointer unused)
{
  PurpleAccount *account;
  PurpleConnectionErrorInfo *err;
  account = purple_connection_get_account(gc);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  err = ((PurpleConnectionErrorInfo *)(g_malloc0_n(1,(sizeof(PurpleConnectionErrorInfo )))));
{
    PurpleConnectionErrorInfo *typed_ptr = err;
    purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurpleConnectionErrorInfo);
  };
  err -> type = type;
  err -> description = g_strdup(description);
  set_current_error(account,err);
  purple_signal_emit(purple_accounts_get_handle(),"account-connection-error",account,type,description);
}

const PurpleConnectionErrorInfo *purple_account_get_current_error(PurpleAccount *account)
{
  PurpleAccountPrivate *priv = (PurpleAccountPrivate *)(account -> priv);
  return (priv -> current_error);
}

void purple_account_clear_current_error(PurpleAccount *account)
{
  set_current_error(account,0);
}

void purple_accounts_add(PurpleAccount *account)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  if (g_list_find(accounts,account) != ((GList *)((void *)0))) 
    return ;
  accounts = g_list_append(accounts,account);
  schedule_accounts_save();
  purple_signal_emit(purple_accounts_get_handle(),"account-added",account);
}

void purple_accounts_remove(PurpleAccount *account)
{
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  accounts = g_list_remove(accounts,account);
  schedule_accounts_save();
/* Clearing the error ensures that account-error-changed is emitted,
	 * which is the end of the guarantee that the the error's pointer is
	 * valid.
	 */
  purple_account_clear_current_error(account);
  purple_signal_emit(purple_accounts_get_handle(),"account-removed",account);
}

void purple_accounts_delete(PurpleAccount *account)
{
  PurpleBlistNode *gnode;
  PurpleBlistNode *cnode;
  PurpleBlistNode *bnode;
  GList *iter;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
/*
	 * Disable the account before blowing it out of the water.
	 * Conceptually it probably makes more sense to disable the
	 * account for all UIs rather than the just the current UI,
	 * but it doesn't really matter.
	 */
  purple_account_set_enabled(account,purple_core_get_ui(),0);
  purple_notify_close_with_handle(account);
  purple_request_close_with_handle(account);
  purple_accounts_remove(account);
/* Remove this account's buddies */
  for (gnode = purple_blist_get_root(); gnode != ((PurpleBlistNode *)((void *)0)); gnode = purple_blist_node_get_sibling_next(gnode)) {
    if (!((purple_blist_node_get_type(gnode)) == PURPLE_BLIST_GROUP_NODE)) 
      continue; 
    cnode = purple_blist_node_get_first_child(gnode);
    while(cnode != 0){
      PurpleBlistNode *cnode_next = purple_blist_node_get_sibling_next(cnode);
      if ((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CONTACT_NODE) {
        bnode = purple_blist_node_get_first_child(cnode);
        while(bnode != 0){
          PurpleBlistNode *bnode_next = purple_blist_node_get_sibling_next(bnode);
          if ((purple_blist_node_get_type(bnode)) == PURPLE_BLIST_BUDDY_NODE) {
            PurpleBuddy *b = (PurpleBuddy *)bnode;
            if (purple_buddy_get_account(b) == account) 
              purple_blist_remove_buddy(b);
          }
          bnode = bnode_next;
        }
      }
      else if ((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CHAT_NODE) {
        PurpleChat *c = (PurpleChat *)cnode;
        if (purple_chat_get_account(c) == account) 
          purple_blist_remove_chat(c);
      }
      cnode = cnode_next;
    }
  }
/* Remove any open conversation for this account */
  for (iter = purple_get_conversations(); iter != 0; ) {
    PurpleConversation *conv = (iter -> data);
    iter = (iter -> next);
    if (purple_conversation_get_account(conv) == account) 
      purple_conversation_destroy(conv);
  }
/* Remove this account's pounces */
  purple_pounce_destroy_all_by_account(account);
/* This will cause the deletion of an old buddy icon. */
  purple_buddy_icons_set_account_icon(account,0,0);
  purple_account_destroy(account);
}

void purple_accounts_reorder(PurpleAccount *account,gint new_index)
{
  gint index;
  GList *l;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (new_index <= g_list_length(accounts)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"new_index <= g_list_length(accounts)");
      return ;
    };
  }while (0);
  index = g_list_index(accounts,account);
  if (index == -1) {
    purple_debug_error("account","Unregistered account (%s) discovered during reorder!\n",purple_account_get_username(account));
    return ;
  }
  l = g_list_nth(accounts,index);
  if (new_index > index) 
    new_index--;
/* Remove the old one. */
  accounts = g_list_delete_link(accounts,l);
/* Insert it where it should go. */
  accounts = g_list_insert(accounts,account,new_index);
  schedule_accounts_save();
}

GList *purple_accounts_get_all()
{
  return accounts;
}

GList *purple_accounts_get_all_active()
{
  GList *list = (GList *)((void *)0);
  GList *all = purple_accounts_get_all();
  while(all != ((GList *)((void *)0))){
    PurpleAccount *account = (all -> data);
    if (purple_account_get_enabled(account,purple_core_get_ui()) != 0) 
      list = g_list_append(list,account);
    all = (all -> next);
  }
  return list;
}

PurpleAccount *purple_accounts_find(const char *name,const char *protocol_id)
{
  PurpleAccount *account = (PurpleAccount *)((void *)0);
  GList *l;
  char *who;
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return 0;
    };
  }while (0);
  do {
    if (protocol_id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"protocol_id != NULL");
      return 0;
    };
  }while (0);
  for (l = purple_accounts_get_all(); l != ((GList *)((void *)0)); l = (l -> next)) {
    account = ((PurpleAccount *)(l -> data));
    if (!(purple_strequal((account -> protocol_id),protocol_id) != 0)) 
      continue; 
    who = g_strdup(purple_normalize(account,name));
    if (purple_strequal(purple_normalize(account,purple_account_get_username(account)),who) != 0) {
      g_free(who);
      return account;
    }
    g_free(who);
  }
  return 0;
}

void purple_accounts_restore_current_statuses()
{
  GList *l;
  PurpleAccount *account;
/* If we're not connected to the Internet right now, we bail on this */
  if (!(purple_network_is_available() != 0)) {
    purple_debug_warning("account","Network not connected; skipping reconnect\n");
    return ;
  }
  for (l = purple_accounts_get_all(); l != ((GList *)((void *)0)); l = (l -> next)) {
    account = ((PurpleAccount *)(l -> data));
    if ((purple_account_get_enabled(account,purple_core_get_ui()) != 0) && (purple_presence_is_online((account -> presence)) != 0)) {
      purple_account_connect(account);
    }
  }
}

void purple_accounts_set_ui_ops(PurpleAccountUiOps *ops)
{
  account_ui_ops = ops;
}

PurpleAccountUiOps *purple_accounts_get_ui_ops()
{
  return account_ui_ops;
}

void *purple_accounts_get_handle()
{
  static int handle;
  return (&handle);
}

void purple_accounts_init()
{
  void *handle = purple_accounts_get_handle();
  void *conn_handle = purple_connections_get_handle();
  purple_signal_register(handle,"account-connecting",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT));
  purple_signal_register(handle,"account-disabled",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT));
  purple_signal_register(handle,"account-enabled",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT));
  purple_signal_register(handle,"account-setting-info",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING));
  purple_signal_register(handle,"account-set-info",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING));
  purple_signal_register(handle,"account-created",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT));
  purple_signal_register(handle,"account-destroying",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT));
  purple_signal_register(handle,"account-added",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT));
  purple_signal_register(handle,"account-removed",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT));
  purple_signal_register(handle,"account-status-changed",purple_marshal_VOID__POINTER_POINTER_POINTER,0,3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_STATUS),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_STATUS));
  purple_signal_register(handle,"account-actions-changed",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT));
  purple_signal_register(handle,"account-alias-changed",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING));
  purple_signal_register(handle,"account-authorization-requested",purple_marshal_INT__POINTER_POINTER,purple_value_new(PURPLE_TYPE_INT),2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING));
  purple_signal_register(handle,"account-authorization-requested-with-message",purple_marshal_INT__POINTER_POINTER_POINTER,purple_value_new(PURPLE_TYPE_INT),3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING));
  purple_signal_register(handle,"account-authorization-denied",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING));
  purple_signal_register(handle,"account-authorization-granted",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING));
  purple_signal_register(handle,"account-error-changed",purple_marshal_VOID__POINTER_POINTER_POINTER,0,3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_POINTER),purple_value_new(PURPLE_TYPE_POINTER));
  purple_signal_register(handle,"account-signed-on",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT));
  purple_signal_register(handle,"account-signed-off",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT));
  purple_signal_register(handle,"account-connection-error",purple_marshal_VOID__POINTER_INT_POINTER,0,3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_ENUM),purple_value_new(PURPLE_TYPE_STRING));
  purple_signal_connect(conn_handle,"signed-on",handle,((PurpleCallback )signed_on_cb),0);
  purple_signal_connect(conn_handle,"signed-off",handle,((PurpleCallback )signed_off_cb),0);
  purple_signal_connect(conn_handle,"connection-error",handle,((PurpleCallback )connection_error_cb),0);
  load_accounts();
}

void purple_accounts_uninit()
{
  gpointer handle = purple_accounts_get_handle();
  if (save_timer != 0) {
    purple_timeout_remove(save_timer);
    save_timer = 0;
    sync_accounts();
  }
  for (; accounts != 0; accounts = g_list_delete_link(accounts,accounts)) 
    purple_account_destroy((accounts -> data));
  purple_signals_disconnect_by_handle(handle);
  purple_signals_unregister_by_instance(handle);
}
