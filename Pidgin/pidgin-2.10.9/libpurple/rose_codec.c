/**
 * @file codec.c Codec for Media API
 * @ingroup core
 */
/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "codec.h"
/** @copydoc _PurpleMediaCodecClass */
typedef struct _PurpleMediaCodecClass PurpleMediaCodecClass;
/** @copydoc _PurpleMediaCodecPrivate */
typedef struct _PurpleMediaCodecPrivate PurpleMediaCodecPrivate;
#define PURPLE_MEDIA_CODEC_GET_PRIVATE(obj) \
		(G_TYPE_INSTANCE_GET_PRIVATE((obj), \
		PURPLE_TYPE_MEDIA_CODEC, PurpleMediaCodecPrivate))

struct _PurpleMediaCodecClass 
{
  GObjectClass parent_class;
}
;

struct _PurpleMediaCodec 
{
  GObject parent;
}
;
static void purple_media_codec_init(PurpleMediaCodec *self);
static void purple_media_codec_class_init(PurpleMediaCodecClass *klass);
static gpointer purple_media_codec_parent_class = (gpointer )((void *)0);
static gint PurpleMediaCodec_private_offset;

static void purple_media_codec_class_intern_init(gpointer klass)
{
  purple_media_codec_parent_class = g_type_class_peek_parent(klass);
  if (PurpleMediaCodec_private_offset != 0) 
    g_type_class_adjust_private_offset(klass,&PurpleMediaCodec_private_offset);
  purple_media_codec_class_init(((PurpleMediaCodecClass *)klass));
}

inline static gpointer purple_media_codec_get_instance_private(PurpleMediaCodec *self)
{
  return (gpointer )(((guint8 *)self) + ((glong )PurpleMediaCodec_private_offset));
}

GType purple_media_codec_get_type()
{
  static volatile gsize g_define_type_id__volatile = 0;
  if ((({
    typedef char _GStaticAssertCompileTimeAssertion_48[1UL];
    (0?((gpointer )g_define_type_id__volatile) : 0);
    !(((
{
      typedef char _GStaticAssertCompileTimeAssertion_48[1UL];
      __sync_synchronize();
      (gpointer )g_define_type_id__volatile;
    })) != 0) && (g_once_init_enter((&g_define_type_id__volatile)) != 0);
  })) != 0) 
{
    GType g_define_type_id = g_type_register_static_simple(((GType )(20 << 2)),g_intern_static_string("PurpleMediaCodec"),(sizeof(PurpleMediaCodecClass )),((GClassInitFunc )purple_media_codec_class_intern_init),(sizeof(PurpleMediaCodec )),((GInstanceInitFunc )purple_media_codec_init),((GTypeFlags )0));
{
{
{
        };
      }
    }
    (
{
      typedef char _GStaticAssertCompileTimeAssertion_48[1UL];
      0?(g_define_type_id__volatile = g_define_type_id) : 0;
      g_once_init_leave((&g_define_type_id__volatile),((gsize )g_define_type_id));
    });
  }
  return g_define_type_id__volatile;
}

struct _PurpleMediaCodecPrivate 
{
  gint id;
  char *encoding_name;
  PurpleMediaSessionType media_type;
  guint clock_rate;
  guint channels;
  GList *optional_params;
}
;
enum __unnamed_enum___F0_L60_C1_PROP_CODEC_0__COMMA__PROP_ID__COMMA__PROP_ENCODING_NAME__COMMA__PROP_MEDIA_TYPE__COMMA__PROP_CLOCK_RATE__COMMA__PROP_CHANNELS__COMMA__PROP_OPTIONAL_PARAMS {PROP_CODEC_0,PROP_ID,PROP_ENCODING_NAME,PROP_MEDIA_TYPE,PROP_CLOCK_RATE,PROP_CHANNELS,PROP_OPTIONAL_PARAMS};

static void purple_media_codec_init(PurpleMediaCodec *info)
{
  PurpleMediaCodecPrivate *priv = (PurpleMediaCodecPrivate *)(g_type_instance_get_private(((GTypeInstance *)info),purple_media_codec_get_type()));
  priv -> encoding_name = ((char *)((void *)0));
  priv -> optional_params = ((GList *)((void *)0));
}

static void purple_media_codec_finalize(GObject *info)
{
  PurpleMediaCodecPrivate *priv = (PurpleMediaCodecPrivate *)(g_type_instance_get_private(((GTypeInstance *)info),purple_media_codec_get_type()));
  g_free((priv -> encoding_name));
  for (; (priv -> optional_params) != 0; priv -> optional_params = g_list_delete_link((priv -> optional_params),(priv -> optional_params))) {
    PurpleKeyValuePair *param = ( *(priv -> optional_params)).data;
    g_free((param -> key));
    g_free((param -> value));
    g_free(param);
  }
}

static void purple_media_codec_set_property(GObject *object,guint prop_id,const GValue *value,GParamSpec *pspec)
{
  PurpleMediaCodecPrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = purple_media_codec_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CODEC(object)");
      return ;
    };
  }while (0);
  priv = ((PurpleMediaCodecPrivate *)(g_type_instance_get_private(((GTypeInstance *)object),purple_media_codec_get_type())));
  switch(prop_id){
    case PROP_ID:
{
      priv -> id = (g_value_get_uint(value));
      break; 
    }
    case PROP_ENCODING_NAME:
{
      g_free((priv -> encoding_name));
      priv -> encoding_name = g_value_dup_string(value);
      break; 
    }
    case PROP_MEDIA_TYPE:
{
      priv -> media_type = (g_value_get_flags(value));
      break; 
    }
    case PROP_CLOCK_RATE:
{
      priv -> clock_rate = g_value_get_uint(value);
      break; 
    }
    case PROP_CHANNELS:
{
      priv -> channels = g_value_get_uint(value);
      break; 
    }
    case PROP_OPTIONAL_PARAMS:
{
      priv -> optional_params = (g_value_get_pointer(value));
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = prop_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","codec.c:124","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void purple_media_codec_get_property(GObject *object,guint prop_id,GValue *value,GParamSpec *pspec)
{
  PurpleMediaCodecPrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = purple_media_codec_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CODEC(object)");
      return ;
    };
  }while (0);
  priv = ((PurpleMediaCodecPrivate *)(g_type_instance_get_private(((GTypeInstance *)object),purple_media_codec_get_type())));
  switch(prop_id){
    case PROP_ID:
{
      g_value_set_uint(value,(priv -> id));
      break; 
    }
    case PROP_ENCODING_NAME:
{
      g_value_set_string(value,(priv -> encoding_name));
      break; 
    }
    case PROP_MEDIA_TYPE:
{
      g_value_set_flags(value,(priv -> media_type));
      break; 
    }
    case PROP_CLOCK_RATE:
{
      g_value_set_uint(value,(priv -> clock_rate));
      break; 
    }
    case PROP_CHANNELS:
{
      g_value_set_uint(value,(priv -> channels));
      break; 
    }
    case PROP_OPTIONAL_PARAMS:
{
      g_value_set_pointer(value,(priv -> optional_params));
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = prop_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","codec.c:159","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void purple_media_codec_class_init(PurpleMediaCodecClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *)klass;
  gobject_class -> finalize = purple_media_codec_finalize;
  gobject_class -> set_property = purple_media_codec_set_property;
  gobject_class -> get_property = purple_media_codec_get_property;
  g_object_class_install_property(gobject_class,PROP_ID,g_param_spec_uint("id","ID","The numeric identifier of the codec.",0,(2147483647 * 2U + 1U),0,(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_class_install_property(gobject_class,PROP_ENCODING_NAME,g_param_spec_string("encoding-name","Encoding Name","The name of the codec.",0,(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_class_install_property(gobject_class,PROP_MEDIA_TYPE,g_param_spec_flags("media-type","Media Type","Whether this is an audio of video codec.",purple_media_session_type_get_type(),PURPLE_MEDIA_NONE,(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_class_install_property(gobject_class,PROP_CLOCK_RATE,g_param_spec_uint("clock-rate","Create Callback","The function called to create this element.",0,(2147483647 * 2U + 1U),0,(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(gobject_class,PROP_CHANNELS,g_param_spec_uint("channels","Channels","The number of channels in this codec.",0,(2147483647 * 2U + 1U),0,(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_object_class_install_property(gobject_class,PROP_OPTIONAL_PARAMS,g_param_spec_pointer("optional-params","Optional Params","A list of optional parameters for the codec.",(G_PARAM_READABLE | G_PARAM_WRITABLE)));
  g_type_class_add_private(klass,(sizeof(PurpleMediaCodecPrivate )));
}

PurpleMediaCodec *purple_media_codec_new(int id,const char *encoding_name,PurpleMediaSessionType media_type,guint clock_rate)
{
  PurpleMediaCodec *codec = (g_object_new(purple_media_codec_get_type(),"id",id,"encoding_name",encoding_name,"media_type",media_type,"clock-rate",clock_rate,((void *)((void *)0))));
  return codec;
}

guint purple_media_codec_get_id(PurpleMediaCodec *codec)
{
  guint id;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)codec;
      GType __t = purple_media_codec_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CODEC(codec)");
      return 0;
    };
  }while (0);
  g_object_get(codec,"id",&id,((void *)((void *)0)));
  return id;
}

gchar *purple_media_codec_get_encoding_name(PurpleMediaCodec *codec)
{
  gchar *name;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)codec;
      GType __t = purple_media_codec_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CODEC(codec)");
      return 0;
    };
  }while (0);
  g_object_get(codec,"encoding-name",&name,((void *)((void *)0)));
  return name;
}

guint purple_media_codec_get_clock_rate(PurpleMediaCodec *codec)
{
  guint clock_rate;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)codec;
      GType __t = purple_media_codec_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CODEC(codec)");
      return 0;
    };
  }while (0);
  g_object_get(codec,"clock-rate",&clock_rate,((void *)((void *)0)));
  return clock_rate;
}

guint purple_media_codec_get_channels(PurpleMediaCodec *codec)
{
  guint channels;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)codec;
      GType __t = purple_media_codec_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CODEC(codec)");
      return 0;
    };
  }while (0);
  g_object_get(codec,"channels",&channels,((void *)((void *)0)));
  return channels;
}

GList *purple_media_codec_get_optional_parameters(PurpleMediaCodec *codec)
{
  GList *optional_params;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)codec;
      GType __t = purple_media_codec_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_CODEC(codec)");
      return 0;
    };
  }while (0);
  g_object_get(codec,"optional-params",&optional_params,((void *)((void *)0)));
  return optional_params;
}

void purple_media_codec_add_optional_parameter(PurpleMediaCodec *codec,const gchar *name,const gchar *value)
{
  PurpleMediaCodecPrivate *priv;
  PurpleKeyValuePair *new_param;
  do {
    if (codec != ((PurpleMediaCodec *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"codec != NULL");
      return ;
    };
  }while (0);
  do {
    if ((name != ((const gchar *)((void *)0))) && (value != ((const gchar *)((void *)0)))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL && value != NULL");
      return ;
    };
  }while (0);
  priv = ((PurpleMediaCodecPrivate *)(g_type_instance_get_private(((GTypeInstance *)codec),purple_media_codec_get_type())));
  new_param = ((PurpleKeyValuePair *)(g_malloc0_n(1,(sizeof(PurpleKeyValuePair )))));
  new_param -> key = g_strdup(name);
  new_param -> value = (g_strdup(value));
  priv -> optional_params = g_list_append((priv -> optional_params),new_param);
}

void purple_media_codec_remove_optional_parameter(PurpleMediaCodec *codec,PurpleKeyValuePair *param)
{
  PurpleMediaCodecPrivate *priv;
  do {
    if ((codec != ((PurpleMediaCodec *)((void *)0))) && (param != ((PurpleKeyValuePair *)((void *)0)))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"codec != NULL && param != NULL");
      return ;
    };
  }while (0);
  priv = ((PurpleMediaCodecPrivate *)(g_type_instance_get_private(((GTypeInstance *)codec),purple_media_codec_get_type())));
  g_free((param -> key));
  g_free((param -> value));
  priv -> optional_params = g_list_remove((priv -> optional_params),param);
  g_free(param);
}

PurpleKeyValuePair *purple_media_codec_get_optional_parameter(PurpleMediaCodec *codec,const gchar *name,const gchar *value)
{
  PurpleMediaCodecPrivate *priv;
  GList *iter;
  do {
    if (codec != ((PurpleMediaCodec *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"codec != NULL");
      return 0;
    };
  }while (0);
  do {
    if (name != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return 0;
    };
  }while (0);
  priv = ((PurpleMediaCodecPrivate *)(g_type_instance_get_private(((GTypeInstance *)codec),purple_media_codec_get_type())));
  for (iter = (priv -> optional_params); iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
    PurpleKeyValuePair *param = (iter -> data);
    if (!(g_ascii_strcasecmp((param -> key),name) != 0) && ((value == ((const gchar *)((void *)0))) || !(g_ascii_strcasecmp((param -> value),value) != 0))) 
      return param;
  }
  return 0;
}

PurpleMediaCodec *purple_media_codec_copy(PurpleMediaCodec *codec)
{
  PurpleMediaCodecPrivate *priv;
  PurpleMediaCodec *new_codec;
  GList *iter;
  if (codec == ((PurpleMediaCodec *)((void *)0))) 
    return 0;
  priv = ((PurpleMediaCodecPrivate *)(g_type_instance_get_private(((GTypeInstance *)codec),purple_media_codec_get_type())));
  new_codec = purple_media_codec_new((priv -> id),(priv -> encoding_name),(priv -> media_type),(priv -> clock_rate));
  g_object_set(codec,"channels",(priv -> channels),((void *)((void *)0)));
  for (iter = (priv -> optional_params); iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
    PurpleKeyValuePair *param = (PurpleKeyValuePair *)(iter -> data);
    purple_media_codec_add_optional_parameter(new_codec,(param -> key),(param -> value));
  }
  return new_codec;
}

GList *purple_media_codec_list_copy(GList *codecs)
{
  GList *new_list = (GList *)((void *)0);
  for (; codecs != 0; codecs = ((codecs != 0)?( *((GList *)codecs)).next : ((struct _GList *)((void *)0)))) {
    new_list = g_list_prepend(new_list,(purple_media_codec_copy((codecs -> data))));
  }
  new_list = g_list_reverse(new_list);
  return new_list;
}

void purple_media_codec_list_free(GList *codecs)
{
  for (; codecs != 0; codecs = g_list_delete_link(codecs,codecs)) {
    g_object_unref((codecs -> data));
  }
}

gchar *purple_media_codec_to_string(const PurpleMediaCodec *codec)
{
  PurpleMediaCodecPrivate *priv;
  GString *string = (GString *)((void *)0);
  GList *item;
  gchar *charstring;
  const gchar *media_type_str = (const gchar *)((void *)0);
  if (codec == ((const PurpleMediaCodec *)((void *)0))) 
    return g_strdup("(NULL)");
  priv = ((PurpleMediaCodecPrivate *)(g_type_instance_get_private(((GTypeInstance *)codec),purple_media_codec_get_type())));
  string = g_string_new("");
  if (((priv -> media_type) & PURPLE_MEDIA_AUDIO) != 0U) 
    media_type_str = "audio";
  else if (((priv -> media_type) & PURPLE_MEDIA_VIDEO) != 0U) 
    media_type_str = "video";
  g_string_printf(string,"%d: %s %s clock:%d channels:%d",(priv -> id),media_type_str,(priv -> encoding_name),(priv -> clock_rate),(priv -> channels));
  for (item = (priv -> optional_params); item != 0; item = ((item != 0)?( *((GList *)item)).next : ((struct _GList *)((void *)0)))) {
    PurpleKeyValuePair *param = (item -> data);
    g_string_append_printf(string," %s=%s",(param -> key),((gchar *)(param -> value)));
  }
  charstring = (string -> str);
  g_string_free(string,0);
  return charstring;
}
