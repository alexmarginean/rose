/**
 * @file ssl.c Main SSL plugin
 *
 * purple
 *
 * Copyright (C) 2003 Christian Hammond <chipx86@gnupdate.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "debug.h"
#include "plugin.h"
#include "sslconn.h"
#include "version.h"
#define SSL_PLUGIN_ID "core-ssl"
static PurplePlugin *ssl_plugin = (PurplePlugin *)((void *)0);

static gboolean probe_ssl_plugins(PurplePlugin *my_plugin)
{
  PurplePlugin *plugin;
  GList *l;
  ssl_plugin = ((PurplePlugin *)((void *)0));
{
    for (l = purple_plugins_get_all(); l != ((GList *)((void *)0)); l = (l -> next)) {
      plugin = ((PurplePlugin *)(l -> data));
      if (plugin == my_plugin) 
        continue; 
      if ((((plugin -> info) != ((PurplePluginInfo *)((void *)0))) && (( *(plugin -> info)).id != ((char *)((void *)0)))) && (strncmp(( *(plugin -> info)).id,"ssl-",4) == 0)) {
        if ((purple_plugin_is_loaded(plugin) != 0) || (purple_plugin_load(plugin) != 0)) {
          ssl_plugin = plugin;
          break; 
        }
      }
    }
  }
  return ssl_plugin != ((PurplePlugin *)((void *)0));
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  return probe_ssl_plugins(plugin);
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  if ((ssl_plugin != ((PurplePlugin *)((void *)0))) && (g_list_find(purple_plugins_get_loaded(),ssl_plugin) != ((GList *)((void *)0)))) {
    purple_plugin_unload(ssl_plugin);
  }
  ssl_plugin = ((PurplePlugin *)((void *)0));
  return (!0);
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ((char *)((void *)0)), (1), ((GList *)((void *)0)), (0), ("core-ssl"), ("SSL"), ("2.10.9"), ("Provides a wrapper around SSL support libraries."), ("Provides a wrapper around SSL support libraries."), ("Christian Hammond <chipx86@gnupdate.org>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
