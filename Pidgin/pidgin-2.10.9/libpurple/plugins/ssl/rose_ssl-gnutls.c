/**
 * @file ssl-gnutls.c GNUTLS SSL plugin.
 *
 * purple
 *
 * Copyright (C) 2003 Christian Hammond <chipx86@gnupdate.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "debug.h"
#include "certificate.h"
#include "plugin.h"
#include "sslconn.h"
#include "version.h"
#include "util.h"
#define SSL_GNUTLS_PLUGIN_ID "ssl-gnutls"
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
typedef struct __unnamed_class___F0_L35_C9_unknown_scope_and_name_variable_declaration__variable_type_L465R_variable_name_unknown_scope_and_name__scope__session__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_guintUi__typedef_declaration_variable_name_unknown_scope_and_name__scope__handshake_handler__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_guintUi__typedef_declaration_variable_name_unknown_scope_and_name__scope__handshake_timer {
gnutls_session session;
guint handshake_handler;
guint handshake_timer;}PurpleSslGnutlsData;
#define PURPLE_SSL_GNUTLS_DATA(gsc) ((PurpleSslGnutlsData *)gsc->private_data)
static gnutls_certificate_client_credentials xcred = (gnutls_certificate_client_credentials )((void *)0);
#ifdef HAVE_GNUTLS_PRIORITY_FUNCS
/* Priority strings.  The default one is, well, the default (and is always
 * set).  The hash table is of the form hostname => priority (both
 * char *).
 *
 * We only use a gnutls_priority_t for the default on the assumption that
 * that's the more common case.  Improvement patches (like matching on
 * subdomains) welcome.
 */
static gnutls_priority_t default_priority = (gnutls_priority_t )((void *)0);
static GHashTable *host_priorities = (GHashTable *)((void *)0);
#endif

static void ssl_gnutls_log(int level,const char *str)
{
/* GnuTLS log messages include the '\n' */
  purple_debug_misc("gnutls","lvl %d: %s",level,str);
}

static void ssl_gnutls_init_gnutls()
{
  const char *debug_level;
  const char *host_priorities_str;
/* Configure GnuTLS to use glib memory management */
/* I expect that this isn't really necessary, but it may prevent
	   some bugs */
/* TODO: It may be necessary to wrap this allocators for GnuTLS.
	   If there are strange bugs, perhaps look here (yes, I am a
	   hypocrite) */
  gnutls_global_set_mem_functions(((gnutls_alloc_function )g_malloc),((gnutls_alloc_function )g_malloc),0,((gnutls_realloc_function )g_realloc),((gnutls_free_function )g_free));
/* malloc */
/* secure malloc */
/* mem_is_secure */
/* realloc */
/* free */
  debug_level = g_getenv("PURPLE_GNUTLS_DEBUG");
  if (debug_level != 0) {
    int level = atoi(debug_level);
    if (level < 0) {
      purple_debug_warning("gnutls","Assuming log level 0 instead of %d\n",level);
      level = 0;
    }
/* "The level is an integer between 0 and 9. Higher values mean more verbosity." */
    gnutls_global_set_log_level(level);
    gnutls_global_set_log_function(ssl_gnutls_log);
  }
/* Expected format: host=priority;host2=priority;*=priority
	 * where "*" is used to override the default priority string for
	 * libpurple.
	 */
  host_priorities_str = g_getenv("PURPLE_GNUTLS_PRIORITIES");
  if (host_priorities_str != 0) {
#ifndef HAVE_GNUTLS_PRIORITY_FUNCS
#else /* HAVE_GNUTLS_PRIORITY_FUNCS */
    char **entries = g_strsplit(host_priorities_str,";",(-1));
    char *default_priority_str = (char *)((void *)0);
    guint i;
    host_priorities = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
    for (i = 0; entries[i] != 0; ++i) {
      char *host = entries[i];
      char *equals = strchr(host,'=');
      char *prio_str;
      if (equals != 0) {
         *equals = 0;
        prio_str = (equals + 1);
/* Empty? */
        if (( *prio_str) == 0) {
          purple_debug_warning("gnutls","Ignoring empty priority string for %s\n",host);
        }
        else {
/* TODO: Validate each of these and complain */
          if (g_str_equal(host,"*") != 0) {
/* Override the default priority */
            g_free(default_priority_str);
            default_priority_str = g_strdup(prio_str);
          }
          else 
            g_hash_table_insert(host_priorities,(g_strdup(host)),(g_strdup(prio_str)));
        }
      }
    }
    if (default_priority_str != 0) {
      if (gnutls_priority_init(&default_priority,default_priority_str,0) != 0) {
        purple_debug_warning("gnutls","Unable to set default priority to %s\n",default_priority_str);
/* Versions of GnuTLS as of 2.8.6 (2010-03-31) don't free/NULL
				 * this on error.
				 */
        ( *gnutls_free)(default_priority);
        default_priority = ((gnutls_priority_t )((void *)0));
      }
      g_free(default_priority_str);
    }
    g_strfreev(entries);
#endif /* HAVE_GNUTLS_PRIORITY_FUNCS */
  }
#ifdef HAVE_GNUTLS_PRIORITY_FUNCS
/* Make sure we set have a default priority! */
  if (!(default_priority != 0)) {
    if (gnutls_priority_init(&default_priority,"NORMAL:%SSL3_RECORD_VERSION",0) != 0) {
/* See comment above about memory leak */
      ( *gnutls_free)(default_priority);
      gnutls_priority_init(&default_priority,"NORMAL",0);
    }
  }
#endif /* HAVE_GNUTLS_PRIORITY_FUNCS */
  gnutls_global_init();
  gnutls_certificate_allocate_credentials((&xcred));
/* TODO: I can likely remove this */
  gnutls_certificate_set_x509_trust_file(xcred,"ca.pem",GNUTLS_X509_FMT_PEM);
}

static gboolean ssl_gnutls_init()
{
  return (!0);
}

static void ssl_gnutls_uninit()
{
  gnutls_global_deinit();
  gnutls_certificate_free_credentials(xcred);
  xcred = ((gnutls_certificate_client_credentials )((void *)0));
#ifdef HAVE_GNUTLS_PRIORITY_FUNCS
  if (host_priorities != 0) {
    g_hash_table_destroy(host_priorities);
    host_priorities = ((GHashTable *)((void *)0));
  }
  gnutls_priority_deinit(default_priority);
  default_priority = ((gnutls_priority_t )((void *)0));
#endif
}

static void ssl_gnutls_verified_cb(PurpleCertificateVerificationStatus st,gpointer userdata)
{
  PurpleSslConnection *gsc = (PurpleSslConnection *)userdata;
  if (st == PURPLE_CERTIFICATE_VALID) {
/* Certificate valid? Good! Do the connection! */
    ( *(gsc -> connect_cb))((gsc -> connect_cb_data),gsc,PURPLE_INPUT_READ);
  }
  else {
/* Otherwise, signal an error */
    if ((gsc -> error_cb) != ((void (*)(PurpleSslConnection *, PurpleSslErrorType , gpointer ))((void *)0))) 
      ( *(gsc -> error_cb))(gsc,PURPLE_SSL_CERTIFICATE_INVALID,(gsc -> connect_cb_data));
    purple_ssl_close(gsc);
  }
}

static void ssl_gnutls_handshake_cb(gpointer data,gint source,PurpleInputCondition cond)
{
  PurpleSslConnection *gsc = data;
  PurpleSslGnutlsData *gnutls_data = (PurpleSslGnutlsData *)(gsc -> private_data);
  ssize_t ret;
/*purple_debug_info("gnutls", "Handshaking with %s\n", gsc->host);*/
  ret = (gnutls_handshake((gnutls_data -> session)));
  if ((ret == (-28)) || (ret == (-52))) 
    return ;
  purple_input_remove((gnutls_data -> handshake_handler));
  gnutls_data -> handshake_handler = 0;
  if (ret != 0) {
    purple_debug_error("gnutls","Handshake failed. Error %s\n",gnutls_strerror(ret));
    if ((gsc -> error_cb) != ((void (*)(PurpleSslConnection *, PurpleSslErrorType , gpointer ))((void *)0))) 
      ( *(gsc -> error_cb))(gsc,PURPLE_SSL_HANDSHAKE_FAILED,(gsc -> connect_cb_data));
    purple_ssl_close(gsc);
  }
  else {
/* Now we are cooking with gas! */
    PurpleSslOps *ops = purple_ssl_get_ops();
    GList *peers = ( *(ops -> get_peer_certificates))(gsc);
    PurpleCertificateScheme *x509 = purple_certificate_find_scheme("x509");
    GList *l;
/* TODO: Remove all this debugging babble */
    purple_debug_info("gnutls","Handshake complete\n");
    for (l = peers; l != 0; l = (l -> next)) {
      PurpleCertificate *crt = (l -> data);
      GByteArray *z = ( *(x509 -> get_fingerprint_sha1))(crt);
      gchar *fpr = purple_base16_encode_chunked((z -> data),(z -> len));
      purple_debug_info("gnutls/x509","Key print: %s\n",fpr);
/* Kill the cert! */
      ( *(x509 -> destroy_certificate))(crt);
      g_free(fpr);
      g_byte_array_free(z,(!0));
    }
    g_list_free(peers);
{
      const gnutls_datum *cert_list;
      unsigned int cert_list_size = 0;
      gnutls_session session = (gnutls_data -> session);
      int i;
      cert_list = gnutls_certificate_get_peers(session,&cert_list_size);
      purple_debug_info("gnutls","Peer provided %d certs\n",cert_list_size);
      for (i = 0; i < cert_list_size; i++) {
        gchar fpr_bin[256UL];
        gsize fpr_bin_sz = (sizeof(fpr_bin));
        gchar *fpr_asc = (gchar *)((void *)0);
        gchar tbuf[256UL];
        gsize tsz = (sizeof(tbuf));
        gchar *tasc = (gchar *)((void *)0);
        gnutls_x509_crt cert;
        gnutls_x509_crt_init(&cert);
        gnutls_x509_crt_import(cert,(cert_list + i),GNUTLS_X509_FMT_DER);
        gnutls_x509_crt_get_fingerprint(cert,GNUTLS_MAC_SHA1,fpr_bin,&fpr_bin_sz);
        fpr_asc = purple_base16_encode_chunked(((const guchar *)fpr_bin),fpr_bin_sz);
        purple_debug_info("gnutls","Lvl %d SHA1 fingerprint: %s\n",i,fpr_asc);
        tsz = (sizeof(tbuf));
        gnutls_x509_crt_get_serial(cert,tbuf,&tsz);
        tasc = purple_base16_encode_chunked(((const guchar *)tbuf),tsz);
        purple_debug_info("gnutls","Serial: %s\n",tasc);
        g_free(tasc);
        tsz = (sizeof(tbuf));
        gnutls_x509_crt_get_dn(cert,tbuf,&tsz);
        purple_debug_info("gnutls","Cert DN: %s\n",tbuf);
        tsz = (sizeof(tbuf));
        gnutls_x509_crt_get_issuer_dn(cert,tbuf,&tsz);
        purple_debug_info("gnutls","Cert Issuer DN: %s\n",tbuf);
        g_free(fpr_asc);
        fpr_asc = ((gchar *)((void *)0));
        gnutls_x509_crt_deinit(cert);
      }
    }
/* TODO: The following logic should really be in libpurple */
/* If a Verifier was given, hand control over to it */
    if ((gsc -> verifier) != 0) {
      GList *peers;
/* First, get the peer cert chain */
      peers = purple_ssl_get_peer_certificates(gsc);
/* Now kick off the verification process */
      purple_certificate_verify((gsc -> verifier),(gsc -> host),peers,ssl_gnutls_verified_cb,gsc);
      purple_certificate_destroy_list(peers);
    }
    else {
/* Otherwise, just call the "connection complete"
			   callback */
      ( *(gsc -> connect_cb))((gsc -> connect_cb_data),gsc,cond);
    }
  }
}

static gboolean start_handshake_cb(gpointer data)
{
  PurpleSslConnection *gsc = data;
  PurpleSslGnutlsData *gnutls_data = (PurpleSslGnutlsData *)(gsc -> private_data);
  purple_debug_info("gnutls","Starting handshake with %s\n",(gsc -> host));
  gnutls_data -> handshake_timer = 0;
  ssl_gnutls_handshake_cb(gsc,(gsc -> fd),PURPLE_INPUT_READ);
  return 0;
}

static void ssl_gnutls_connect(PurpleSslConnection *gsc)
{
  PurpleSslGnutlsData *gnutls_data;
  static const int cert_type_priority[2UL] = {(GNUTLS_CRT_X509), (0)};
  gnutls_data = ((PurpleSslGnutlsData *)(g_malloc0_n(1,(sizeof(PurpleSslGnutlsData )))));
  gsc -> private_data = gnutls_data;
  gnutls_init(&gnutls_data -> session,(1 << 1));
#ifdef HAVE_GNUTLS_PRIORITY_FUNCS
{
    const char *prio_str = (const char *)((void *)0);
    gboolean set = 0;
/* Let's see if someone has specified a specific priority */
    if (((gsc -> host) != 0) && (host_priorities != 0)) 
      prio_str = (g_hash_table_lookup(host_priorities,(gsc -> host)));
    if (prio_str != 0) 
      set = (0 == gnutls_priority_set_direct((gnutls_data -> session),prio_str,0));
    if (!(set != 0)) 
      gnutls_priority_set((gnutls_data -> session),default_priority);
  }
#else
#endif
  gnutls_certificate_type_set_priority((gnutls_data -> session),cert_type_priority);
  gnutls_credentials_set((gnutls_data -> session),GNUTLS_CRD_CERTIFICATE,xcred);
  gnutls_transport_set_ptr((gnutls_data -> session),((gpointer )((glong )(gsc -> fd))));
  gnutls_data -> handshake_handler = purple_input_add((gsc -> fd),PURPLE_INPUT_READ,ssl_gnutls_handshake_cb,gsc);
/* Orborde asks: Why are we configuring a callback, then
	   (almost) immediately calling it?
	   Answer: gnutls_handshake (up in handshake_cb) needs to be called
	   once in order to get the ball rolling on the SSL connection.
	   Once it has done so, only then will the server reply, triggering
	   the callback.
	   Since the logic driving gnutls_handshake is the same with the first
	   and subsequent calls, we'll just fire the callback immediately to
	   accomplish this.
	*/
  gnutls_data -> handshake_timer = purple_timeout_add(0,start_handshake_cb,gsc);
}

static void ssl_gnutls_close(PurpleSslConnection *gsc)
{
  PurpleSslGnutlsData *gnutls_data = (PurpleSslGnutlsData *)(gsc -> private_data);
  if (!(gnutls_data != 0)) 
    return ;
  if ((gnutls_data -> handshake_handler) != 0U) 
    purple_input_remove((gnutls_data -> handshake_handler));
  if ((gnutls_data -> handshake_timer) != 0U) 
    purple_timeout_remove((gnutls_data -> handshake_timer));
  gnutls_bye((gnutls_data -> session),GNUTLS_SHUT_RDWR);
  gnutls_deinit((gnutls_data -> session));
  g_free(gnutls_data);
  gsc -> private_data = ((void *)((void *)0));
}

static size_t ssl_gnutls_read(PurpleSslConnection *gsc,void *data,size_t len)
{
  PurpleSslGnutlsData *gnutls_data = (PurpleSslGnutlsData *)(gsc -> private_data);
  ssize_t s;
  s = gnutls_record_recv((gnutls_data -> session),data,len);
  if ((s == (-28)) || (s == (-52))) {
    s = (-1);
     *__errno_location() = 11;
  }
  else if (s < 0) {
    purple_debug_error("gnutls","receive failed: %s\n",gnutls_strerror(s));
    s = (-1);
/*
		 * TODO: Set errno to something more appropriate.  Or even
		 *       better: allow ssl plugins to keep track of their
		 *       own error message, then add a new ssl_ops function
		 *       that returns the error message.
		 */
     *__errno_location() = 5;
  }
  return s;
}

static size_t ssl_gnutls_write(PurpleSslConnection *gsc,const void *data,size_t len)
{
  PurpleSslGnutlsData *gnutls_data = (PurpleSslGnutlsData *)(gsc -> private_data);
  ssize_t s = 0;
/* XXX: when will gnutls_data be NULL? */
  if (gnutls_data != 0) 
    s = gnutls_record_send((gnutls_data -> session),data,len);
  if ((s == (-28)) || (s == (-52))) {
    s = (-1);
     *__errno_location() = 11;
  }
  else if (s < 0) {
    purple_debug_error("gnutls","send failed: %s\n",gnutls_strerror(s));
    s = (-1);
/*
		 * TODO: Set errno to something more appropriate.  Or even
		 *       better: allow ssl plugins to keep track of their
		 *       own error message, then add a new ssl_ops function
		 *       that returns the error message.
		 */
     *__errno_location() = 5;
  }
  return s;
}
/* Forward declarations are fun! */
static PurpleCertificate *x509_import_from_datum(const gnutls_datum dt,gnutls_x509_crt_fmt mode);
/* indeed! */
static gboolean x509_certificate_signed_by(PurpleCertificate *crt,PurpleCertificate *issuer);
static void x509_destroy_certificate(PurpleCertificate *crt);

static GList *ssl_gnutls_get_peer_certificates(PurpleSslConnection *gsc)
{
  PurpleSslGnutlsData *gnutls_data = (PurpleSslGnutlsData *)(gsc -> private_data);
  PurpleCertificate *prvcrt = (PurpleCertificate *)((void *)0);
/* List of Certificate instances to return */
  GList *peer_certs = (GList *)((void *)0);
/* List of raw certificates as given by GnuTLS */
  const gnutls_datum *cert_list;
  unsigned int cert_list_size = 0;
  unsigned int i;
/* This should never, ever happen. */
  do {
    if ((gnutls_certificate_type_get((gnutls_data -> session))) == GNUTLS_CRT_X509) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gnutls_certificate_type_get (gnutls_data->session) == GNUTLS_CRT_X509");
      return 0;
    };
  }while (0);
/* Get the certificate list from GnuTLS */
/* TODO: I am _pretty sure_ this doesn't block or do other exciting things */
  cert_list = gnutls_certificate_get_peers((gnutls_data -> session),&cert_list_size);
{
/* Convert each certificate to a Certificate and append it to the list */
    for (i = 0; i < cert_list_size; i++) {
      PurpleCertificate *newcrt = x509_import_from_datum(cert_list[i],GNUTLS_X509_FMT_DER);
/* Append is somewhat inefficient on linked lists, but is easy
		   to read. If someone complains, I'll change it.
		   TODO: Is anyone complaining? (Maybe elb?) */
/* only append if previous cert was actually signed by this one.
		 * Thanks Microsoft. */
      if ((prvcrt == ((PurpleCertificate *)((void *)0))) || (x509_certificate_signed_by(prvcrt,newcrt) != 0)) {
        peer_certs = g_list_append(peer_certs,newcrt);
        prvcrt = newcrt;
      }
      else {
        x509_destroy_certificate(newcrt);
        purple_debug_error("gnutls","Dropping further peer certificates because the chain is broken!\n");
        break; 
      }
    }
  }
/* cert_list doesn't need free()-ing */
  return peer_certs;
}
/************************************************************************/
/* X.509 functionality                                                  */
/************************************************************************/
const gchar *SCHEME_NAME = "x509";
extern PurpleCertificateScheme x509_gnutls;
/** Refcounted GnuTLS certificate data instance */
typedef struct __unnamed_class___F0_L586_C9_unknown_scope_and_name_variable_declaration__variable_type_ginti__typedef_declaration_variable_name_unknown_scope_and_name__scope__refcount__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L471R_variable_name_unknown_scope_and_name__scope__crt {
gint refcount;
gnutls_x509_crt crt;}x509_crtdata_t;
/** Helper functions for reference counting */

static x509_crtdata_t *x509_crtdata_addref(x509_crtdata_t *cd)
{
  cd -> refcount++;
  return cd;
}

static void x509_crtdata_delref(x509_crtdata_t *cd)
{
  cd -> refcount--;
  if ((cd -> refcount) < 0) 
    g_log(0,G_LOG_LEVEL_CRITICAL,"Refcount of x509_crtdata_t is %d, which is less than zero!\n",(cd -> refcount));
/* If the refcount reaches zero, kill the structure */
  if ((cd -> refcount) <= 0) {
/* Kill the internal data */
    gnutls_x509_crt_deinit((cd -> crt));
/* And kill the struct */
    g_free(cd);
  }
}
/** Helper macro to retrieve the GnuTLS crt_t from a PurpleCertificate */
#define X509_GET_GNUTLS_DATA(pcrt) ( ((x509_crtdata_t *) (pcrt->data))->crt)
/** Transforms a gnutls_datum containing an X.509 certificate into a Certificate instance under the x509_gnutls scheme
 *
 * @param dt   Datum to transform
 * @param mode GnuTLS certificate format specifier (GNUTLS_X509_FMT_PEM for
 *             reading from files, and GNUTLS_X509_FMT_DER for converting
 *             "over the wire" certs for SSL)
 *
 * @return A newly allocated Certificate structure of the x509_gnutls scheme
 */

static PurpleCertificate *x509_import_from_datum(const gnutls_datum dt,gnutls_x509_crt_fmt mode)
{
/* Internal certificate data structure */
  x509_crtdata_t *certdat;
/* New certificate to return */
  PurpleCertificate *crt;
/* Allocate and prepare the internal certificate data */
  certdat = ((x509_crtdata_t *)(g_malloc0_n(1,(sizeof(x509_crtdata_t )))));
  gnutls_x509_crt_init(&certdat -> crt);
  certdat -> refcount = 0;
/* Perform the actual certificate parse */
/* Yes, certdat->crt should be passed as-is */
  gnutls_x509_crt_import((certdat -> crt),&dt,mode);
/* Allocate the certificate and load it with data */
  crt = ((PurpleCertificate *)(g_malloc0_n(1,(sizeof(PurpleCertificate )))));
  crt -> scheme = &x509_gnutls;
  crt -> data = (x509_crtdata_addref(certdat));
  return crt;
}
/** Imports a PEM-formatted X.509 certificate from the specified file.
 * @param filename Filename to import from. Format is PEM
 *
 * @return A newly allocated Certificate structure of the x509_gnutls scheme
 */

static PurpleCertificate *x509_import_from_file(const gchar *filename)
{
/* Certificate being constructed */
  PurpleCertificate *crt;
/* Used to load the raw file data */
  gchar *buf;
/* Size of the above */
  gsize buf_sz;
/* Struct to pass down to GnuTLS */
  gnutls_datum dt;
  purple_debug_info("gnutls","Attempting to load X.509 certificate from %s\n",filename);
/* Next, we'll simply yank the entire contents of the file
	   into memory */
/* TODO: Should I worry about very large files here? */
  do {
    if (g_file_get_contents(filename,&buf,&buf_sz,0) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"g_file_get_contents(filename, &buf, &buf_sz, NULL )");
      return 0;
    };
  }while (0);
/* No error checking for now */
/* Load the datum struct */
  dt.data = ((unsigned char *)buf);
  dt.size = buf_sz;
/* Perform the conversion; files should be in PEM format */
  crt = x509_import_from_datum(dt,GNUTLS_X509_FMT_PEM);
/* Cleanup */
  g_free(buf);
  return crt;
}
/** Imports a number of PEM-formatted X.509 certificates from the specified file.
 * @param filename Filename to import from. Format is PEM
 *
 * @return A newly allocated GSList of Certificate structures of the x509_gnutls scheme
 */

static GSList *x509_importcerts_from_file(const gchar *filename)
{
/* Certificate being constructed */
  PurpleCertificate *crt;
/* Used to load the raw file data */
  gchar *buf;
  gchar *begin;
  gchar *end;
  GSList *crts = (GSList *)((void *)0);
/* Size of the above */
  gsize buf_sz;
/* Struct to pass down to GnuTLS */
  gnutls_datum dt;
  purple_debug_info("gnutls","Attempting to load X.509 certificates from %s\n",filename);
/* Next, we'll simply yank the entire contents of the file
	   into memory */
/* TODO: Should I worry about very large files here? */
  do {
    if (g_file_get_contents(filename,&buf,&buf_sz,0) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"g_file_get_contents(filename, &buf, &buf_sz, NULL )");
      return 0;
    };
  }while (0);
/* No error checking for now */
  begin = buf;
  while((end = strstr(begin,"-----END CERTIFICATE-----")) != ((gchar *)((void *)0))){
    end += sizeof(( *((char (*)[26UL])"-----END CERTIFICATE-----"))) - 1;
/* Load the datum struct */
    dt.data = ((unsigned char *)begin);
    dt.size = (end - begin);
/* Perform the conversion; files should be in PEM format */
    crt = x509_import_from_datum(dt,GNUTLS_X509_FMT_PEM);
    crts = g_slist_prepend(crts,crt);
    begin = end;
  }
/* Cleanup */
  g_free(buf);
  return crts;
}
/**
 * Exports a PEM-formatted X.509 certificate to the specified file.
 * @param filename Filename to export to. Format will be PEM
 * @param crt      Certificate to export
 *
 * @return TRUE if success, otherwise FALSE
 */

static gboolean x509_export_certificate(const gchar *filename,PurpleCertificate *crt)
{
/* GnuTLS cert struct */
  gnutls_x509_crt crt_dat;
  int ret;
/* Data to output */
  gchar *out_buf;
/* Output size */
  size_t out_size;
  gboolean success = 0;
/* Paranoia paranoia paranoia! */
  do {
    if (filename != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"filename");
      return 0;
    };
  }while (0);
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return 0;
    };
  }while (0);
  do {
    if ((crt -> scheme) == &x509_gnutls) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme == &x509_gnutls");
      return 0;
    };
  }while (0);
  do {
    if ((crt -> data) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->data");
      return 0;
    };
  }while (0);
  crt_dat = ( *((x509_crtdata_t *)(crt -> data))).crt;
/* Obtain the output size required */
  out_size = 0;
  ret = gnutls_x509_crt_export(crt_dat,GNUTLS_X509_FMT_PEM,0,&out_size);
/* Provide no buffer yet */
/* Put size here */
  do {
    if (ret == -51) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ret == GNUTLS_E_SHORT_MEMORY_BUFFER");
      return 0;
    };
  }while (0);
/* Now allocate a buffer and *really* export it */
  out_buf = ((gchar *)(g_malloc0_n(out_size,(sizeof(gchar )))));
  ret = gnutls_x509_crt_export(crt_dat,GNUTLS_X509_FMT_PEM,out_buf,&out_size);
/* Export to our new buffer */
/* Put size here */
  if (ret != 0) {
    purple_debug_error("gnutls/x509","Failed to export cert to buffer with code %d\n",ret);
    g_free(out_buf);
    return 0;
  }
/* Write it out to an actual file */
  success = purple_util_write_data_to_file_absolute(filename,out_buf,out_size);
  g_free(out_buf);
  return success;
}

static PurpleCertificate *x509_copy_certificate(PurpleCertificate *crt)
{
  x509_crtdata_t *crtdat;
  PurpleCertificate *newcrt;
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return 0;
    };
  }while (0);
  do {
    if ((crt -> scheme) == &x509_gnutls) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme == &x509_gnutls");
      return 0;
    };
  }while (0);
  crtdat = ((x509_crtdata_t *)(crt -> data));
  newcrt = ((PurpleCertificate *)(g_malloc0_n(1,(sizeof(PurpleCertificate )))));
  newcrt -> scheme = &x509_gnutls;
  newcrt -> data = (x509_crtdata_addref(crtdat));
  return newcrt;
}
/** Frees a Certificate
 *
 * Destroys a Certificate's internal data structures and frees the pointer
 * given.
 * @param crt Certificate instance to be destroyed. It WILL NOT be destroyed
 *            if it is not of the correct CertificateScheme. Can be NULL
 *
 */

static void x509_destroy_certificate(PurpleCertificate *crt)
{
  if (((PurpleCertificate *)((void *)0)) == crt) 
    return ;
/* Check that the scheme is x509_gnutls */
  if ((crt -> scheme) != &x509_gnutls) {
    purple_debug_error("gnutls","destroy_certificate attempted on certificate of wrong scheme (scheme was %s, expected %s)\n",( *(crt -> scheme)).name,SCHEME_NAME);
    return ;
  }
  do {
    if ((crt -> data) != ((void *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->data != NULL");
      return ;
    };
  }while (0);
  do {
    if ((crt -> scheme) != ((PurpleCertificateScheme *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme != NULL");
      return ;
    };
  }while (0);
/* Use the reference counting system to free (or not) the
	   underlying data */
  x509_crtdata_delref(((x509_crtdata_t *)(crt -> data)));
/* Kill the structure itself */
  g_free(crt);
}
/** Determines whether one certificate has been issued and signed by another
 *
 * @param crt       Certificate to check the signature of
 * @param issuer    Issuer's certificate
 *
 * @return TRUE if crt was signed and issued by issuer, otherwise FALSE
 * @TODO  Modify this function to return a reason for invalidity?
 */

static gboolean x509_certificate_signed_by(PurpleCertificate *crt,PurpleCertificate *issuer)
{
  gnutls_x509_crt crt_dat;
  gnutls_x509_crt issuer_dat;
/* used to store result from GnuTLS verifier */
  unsigned int verify;
  int ret;
  gchar *crt_id = (gchar *)((void *)0);
  gchar *issuer_id = (gchar *)((void *)0);
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return 0;
    };
  }while (0);
  do {
    if (issuer != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"issuer");
      return 0;
    };
  }while (0);
/* Verify that both certs are the correct scheme */
  do {
    if ((crt -> scheme) == &x509_gnutls) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme == &x509_gnutls");
      return 0;
    };
  }while (0);
  do {
    if ((issuer -> scheme) == &x509_gnutls) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"issuer->scheme == &x509_gnutls");
      return 0;
    };
  }while (0);
/* TODO: check for more nullness? */
  crt_dat = ( *((x509_crtdata_t *)(crt -> data))).crt;
  issuer_dat = ( *((x509_crtdata_t *)(issuer -> data))).crt;
/* First, let's check that crt.issuer is actually issuer */
  ret = gnutls_x509_crt_check_issuer(crt_dat,issuer_dat);
  if (ret <= 0) {
    if (ret < 0) {
      purple_debug_error("gnutls/x509","GnuTLS error %d while checking certificate issuer match.",ret);
    }
    else {
      gchar *crt_id;
      gchar *issuer_id;
      gchar *crt_issuer_id;
      crt_id = purple_certificate_get_unique_id(crt);
      issuer_id = purple_certificate_get_unique_id(issuer);
      crt_issuer_id = purple_certificate_get_issuer_unique_id(crt);
      purple_debug_info("gnutls/x509","Certificate %s is issued by %s, which does not match %s.\n",((crt_id != 0)?crt_id : "(null)"),((crt_issuer_id != 0)?crt_issuer_id : "(null)"),((issuer_id != 0)?issuer_id : "(null)"));
      g_free(crt_id);
      g_free(issuer_id);
      g_free(crt_issuer_id);
    }
/* The issuer is not correct, or there were errors */
    return 0;
  }
/* Now, check the signature */
/* The second argument is a ptr to an array of "trusted" issuer certs,
	   but we're only using one trusted one */
  ret = gnutls_x509_crt_verify(crt_dat,(&issuer_dat),1,GNUTLS_VERIFY_ALLOW_X509_V1_CA_CRT,&verify);
/* Permit signings by X.509v1 certs
					(Verisign and possibly others have
					root certificates that predate the
					current standard) */
  if (ret != 0) {
    purple_debug_error("gnutls/x509","Attempted certificate verification caused a GnuTLS error code %d. I will just say the signature is bad, but you should look into this.\n",ret);
    return 0;
  }
#ifdef HAVE_GNUTLS_CERT_INSECURE_ALGORITHM
  if ((verify & GNUTLS_CERT_INSECURE_ALGORITHM) != 0U) {
/*
		 * A certificate in the chain is signed with an insecure
		 * algorithm. Put a warning into the log to make this error
		 * perfectly clear as soon as someone looks at the debug log is
		 * generated.
		 */
    crt_id = purple_certificate_get_unique_id(crt);
    issuer_id = purple_certificate_get_issuer_unique_id(crt);
    purple_debug_warning("gnutls/x509","Insecure hash algorithm used by %s to sign %s\n",issuer_id,crt_id);
  }
#endif
  if ((verify & GNUTLS_CERT_INVALID) != 0U) {
/* Signature didn't check out, but at least
		   there were no errors*/
    if (!(crt_id != 0)) 
      crt_id = purple_certificate_get_unique_id(crt);
    if (!(issuer_id != 0)) 
      issuer_id = purple_certificate_get_issuer_unique_id(crt);
    purple_debug_error("gnutls/x509","Bad signature from %s on %s\n",issuer_id,crt_id);
    g_free(crt_id);
    g_free(issuer_id);
    return 0;
/* if (ret, etc.) */
  }
/* If we got here, the signature is good */
  return (!0);
}

static GByteArray *x509_sha1sum(PurpleCertificate *crt)
{
/* SHA1 hashes are 20 bytes */
  size_t hashlen = 20;
/* Throw-away variable for GnuTLS to stomp on*/
  size_t tmpsz = hashlen;
  gnutls_x509_crt crt_dat;
/**< Final hash container */
  GByteArray *hash;
/**< Temporary buffer to contain hash */
  guchar hashbuf[hashlen];
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return 0;
    };
  }while (0);
  crt_dat = ( *((x509_crtdata_t *)(crt -> data))).crt;
/* Extract the fingerprint */
  do {
    if (0 == gnutls_x509_crt_get_fingerprint(crt_dat,GNUTLS_MAC_SHA1,hashbuf,&tmpsz)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"0 == gnutls_x509_crt_get_fingerprint(crt_dat, GNUTLS_MAC_SHA, hashbuf, &tmpsz)");
      return 0;
    };
  }while (0);
/* This shouldn't happen */
  do {
    if (tmpsz == hashlen) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"tmpsz == hashlen");
      return 0;
    };
  }while (0);
/* Okay, now create and fill hash array */
  hash = g_byte_array_new();
  g_byte_array_append(hash,hashbuf,hashlen);
  return hash;
}

static gchar *x509_cert_dn(PurpleCertificate *crt)
{
  gnutls_x509_crt cert_dat;
  gchar *dn = (gchar *)((void *)0);
  size_t dn_size;
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return 0;
    };
  }while (0);
  do {
    if ((crt -> scheme) == &x509_gnutls) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme == &x509_gnutls");
      return 0;
    };
  }while (0);
  cert_dat = ( *((x509_crtdata_t *)(crt -> data))).crt;
/* Figure out the length of the Distinguished Name */
/* Claim that the buffer is size 0 so GnuTLS just tells us how much
	   space it needs */
  dn_size = 0;
  gnutls_x509_crt_get_dn(cert_dat,dn,&dn_size);
/* Now allocate and get the Distinguished Name */
/* Old versions of GnuTLS have an off-by-one error in reporting
	   the size of the needed buffer in some functions, so allocate
	   an extra byte */
  dn = ((gchar *)(g_malloc0_n(++dn_size,(sizeof(gchar )))));
  if (0 != gnutls_x509_crt_get_dn(cert_dat,dn,&dn_size)) {
    purple_debug_error("gnutls/x509","Failed to get Distinguished Name\n");
    g_free(dn);
    return 0;
  }
  return dn;
}

static gchar *x509_issuer_dn(PurpleCertificate *crt)
{
  gnutls_x509_crt cert_dat;
  gchar *dn = (gchar *)((void *)0);
  size_t dn_size;
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return 0;
    };
  }while (0);
  do {
    if ((crt -> scheme) == &x509_gnutls) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme == &x509_gnutls");
      return 0;
    };
  }while (0);
  cert_dat = ( *((x509_crtdata_t *)(crt -> data))).crt;
/* Figure out the length of the Distinguished Name */
/* Claim that the buffer is size 0 so GnuTLS just tells us how much
	   space it needs */
  dn_size = 0;
  gnutls_x509_crt_get_issuer_dn(cert_dat,dn,&dn_size);
/* Now allocate and get the Distinguished Name */
/* Old versions of GnuTLS have an off-by-one error in reporting
	   the size of the needed buffer in some functions, so allocate
	   an extra byte */
  dn = ((gchar *)(g_malloc0_n(++dn_size,(sizeof(gchar )))));
  if (0 != gnutls_x509_crt_get_issuer_dn(cert_dat,dn,&dn_size)) {
    purple_debug_error("gnutls/x509","Failed to get issuer\'s Distinguished Name\n");
    g_free(dn);
    return 0;
  }
  return dn;
}

static gchar *x509_common_name(PurpleCertificate *crt)
{
  gnutls_x509_crt cert_dat;
  gchar *cn = (gchar *)((void *)0);
  size_t cn_size;
  int ret;
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return 0;
    };
  }while (0);
  do {
    if ((crt -> scheme) == &x509_gnutls) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme == &x509_gnutls");
      return 0;
    };
  }while (0);
  cert_dat = ( *((x509_crtdata_t *)(crt -> data))).crt;
/* Figure out the length of the Common Name */
/* Claim that the buffer is size 0 so GnuTLS just tells us how much
	   space it needs */
  cn_size = 0;
  gnutls_x509_crt_get_dn_by_oid(cert_dat,"2.5.4.3",0,0,cn,&cn_size);
/* First CN found, please */
/* Not in raw mode */
/* Now allocate and get the Common Name */
/* Old versions of GnuTLS have an off-by-one error in reporting
	   the size of the needed buffer in some functions, so allocate
	   an extra byte */
  cn = ((gchar *)(g_malloc0_n(++cn_size,(sizeof(gchar )))));
  ret = gnutls_x509_crt_get_dn_by_oid(cert_dat,"2.5.4.3",0,0,cn,&cn_size);
/* First CN found, please */
/* Not in raw mode */
  if (ret != 0) {
    purple_debug_error("gnutls/x509","Failed to get Common Name\n");
    g_free(cn);
    return 0;
  }
  return cn;
}

static gboolean x509_check_name(PurpleCertificate *crt,const gchar *name)
{
  gnutls_x509_crt crt_dat;
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return 0;
    };
  }while (0);
  do {
    if ((crt -> scheme) == &x509_gnutls) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme == &x509_gnutls");
      return 0;
    };
  }while (0);
  do {
    if (name != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name");
      return 0;
    };
  }while (0);
  crt_dat = ( *((x509_crtdata_t *)(crt -> data))).crt;
  if (gnutls_x509_crt_check_hostname(crt_dat,name) != 0) {
    return (!0);
  }
  else {
    return 0;
  }
}

static gboolean x509_times(PurpleCertificate *crt,time_t *activation,time_t *expiration)
{
  gnutls_x509_crt crt_dat;
/* GnuTLS time functions return this on error */
  const time_t errval = (time_t )(-1);
  gboolean success = (!0);
  do {
    if (crt != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt");
      return 0;
    };
  }while (0);
  do {
    if ((crt -> scheme) == &x509_gnutls) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"crt->scheme == &x509_gnutls");
      return 0;
    };
  }while (0);
  crt_dat = ( *((x509_crtdata_t *)(crt -> data))).crt;
  if (activation != 0) {
     *activation = gnutls_x509_crt_get_activation_time(crt_dat);
    if ( *activation == errval) 
      success = 0;
  }
  if (expiration != 0) {
     *expiration = gnutls_x509_crt_get_expiration_time(crt_dat);
    if ( *expiration == errval) 
      success = 0;
  }
  return success;
}
/* X.509 certificate operations provided by this plugin */
static PurpleCertificateScheme x509_gnutls = {("x509"), ("X.509 Certificates"), (x509_import_from_file), (x509_export_certificate), (x509_copy_certificate), (x509_destroy_certificate), (x509_certificate_signed_by), (x509_sha1sum), (x509_cert_dn), (x509_issuer_dn), (x509_common_name), (x509_check_name), (x509_times), (x509_importcerts_from_file), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* Scheme name */
/* User-visible scheme name */
/* Certificate import function */
/* Certificate export function */
/* Copy */
/* Destroy cert */
/* Signature checker */
/* SHA1 fingerprint */
/* Unique ID */
/* Issuer Unique ID */
/* Subject name */
/* Check subject name */
/* Activation/Expiration time */
/* Multiple certificates import function */
};
static PurpleSslOps ssl_ops = {(ssl_gnutls_init), (ssl_gnutls_uninit), (ssl_gnutls_connect), (ssl_gnutls_close), (ssl_gnutls_read), (ssl_gnutls_write), (ssl_gnutls_get_peer_certificates), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* padding */
};

static gboolean plugin_load(PurplePlugin *plugin)
{
  if (!(purple_ssl_get_ops() != 0)) {
    purple_ssl_set_ops(&ssl_ops);
  }
/* Init GNUTLS now so others can use it even if sslconn never does */
  ssl_gnutls_init_gnutls();
/* Register that we're providing an X.509 CertScheme */
  purple_certificate_register_scheme(&x509_gnutls);
  return (!0);
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  if (purple_ssl_get_ops() == &ssl_ops) {
    purple_ssl_set_ops(0);
  }
  purple_certificate_unregister_scheme(&x509_gnutls);
  return (!0);
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ((char *)((void *)0)), (1), ((GList *)((void *)0)), (0), ("ssl-gnutls"), ("GNUTLS"), ("2.10.9"), ("Provides SSL support through GNUTLS."), ("Provides SSL support through GNUTLS."), ("Christian Hammond <chipx86@gnupdate.org>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/**< prefs_info     */
/**< actions        */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
