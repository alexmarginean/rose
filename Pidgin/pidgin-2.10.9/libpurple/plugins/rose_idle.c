/*
 * idle.c - I'dle Mak'er plugin for Purple
 *
 * This file is part of Purple.
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "connection.h"
#include "debug.h"
#include "notify.h"
#include "plugin.h"
#include "request.h"
#include "server.h"
#include "status.h"
#include "version.h"
/* This plugin no longer depends on gtk */
#define IDLE_PLUGIN_ID "core-idle"
static GList *idled_accts = (GList *)((void *)0);

static gboolean unidle_filter(PurpleAccount *acct)
{
  if (g_list_find(idled_accts,acct) != 0) 
    return (!0);
  return 0;
}

static gboolean idleable_filter(PurpleAccount *account)
{
  PurplePlugin *prpl;
  prpl = purple_find_prpl(purple_account_get_protocol_id(account));
  do {
    if (prpl != ((PurplePlugin *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"prpl != NULL");
      return 0;
    };
  }while (0);
  return ( *((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info)).set_idle != ((void (*)(PurpleConnection *, int ))((void *)0));
}

static void set_idle_time(PurpleAccount *acct,int mins_idle)
{
  time_t t;
  PurpleConnection *gc = purple_account_get_connection(acct);
  PurplePresence *presence = purple_account_get_presence(acct);
  if (!(gc != 0)) 
    return ;
  purple_debug_info("idle","setting idle time for %s to %d\n",purple_account_get_username(acct),mins_idle);
  if (mins_idle != 0) 
/* subtract seconds idle from current time */
    t = (time(0) - (60 * mins_idle));
  else 
/* time idle is irrelevant */
    t = 0;
  purple_presence_set_idle(presence,((mins_idle != 0)?!0 : 0),t);
}

static void idle_action_ok(void *ignored,PurpleRequestFields *fields)
{
  int tm = purple_request_fields_get_integer(fields,"mins");
  PurpleAccount *acct = purple_request_fields_get_account(fields,"acct");
/* only add the account to the GList if it's not already been idled */
  if (!(unidle_filter(acct) != 0)) {
    purple_debug_misc("idle","%s hasn\'t been idled yet; adding to list.\n",purple_account_get_username(acct));
    idled_accts = g_list_append(idled_accts,acct);
  }
  set_idle_time(acct,tm);
}

static void idle_all_action_ok(void *ignored,PurpleRequestFields *fields)
{
  PurpleAccount *acct = (PurpleAccount *)((void *)0);
  GList *list;
  GList *iter;
  int tm = purple_request_fields_get_integer(fields,"mins");
  list = purple_accounts_get_all_active();
  for (iter = list; iter != 0; iter = (iter -> next)) {
    acct = ((PurpleAccount *)(iter -> data));
    if ((acct != 0) && (idleable_filter(acct) != 0)) {
      purple_debug_misc("idle","Idling %s.\n",purple_account_get_username(acct));
      set_idle_time(acct,tm);
      if (!(g_list_find(idled_accts,acct) != 0)) 
        idled_accts = g_list_append(idled_accts,acct);
    }
  }
  g_list_free(list);
}

static void unidle_action_ok(void *ignored,PurpleRequestFields *fields)
{
  PurpleAccount *acct = purple_request_fields_get_account(fields,"acct");
/* unidle the account */
  set_idle_time(acct,0);
/* once the account has been unidled it shouldn't be in the list */
  idled_accts = g_list_remove(idled_accts,acct);
}

static void idle_action(PurplePluginAction *action)
{
/* Use the super fancy request API */
  PurpleRequestFields *request;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  group = purple_request_field_group_new(0);
  field = purple_request_field_account_new("acct",((const char *)(dgettext("pidgin","Account"))),0);
  purple_request_field_account_set_filter(field,idleable_filter);
  purple_request_field_account_set_show_all(field,0);
  purple_request_field_group_add_field(group,field);
  field = purple_request_field_int_new("mins",((const char *)(dgettext("pidgin","Minutes"))),10);
  purple_request_field_group_add_field(group,field);
  request = purple_request_fields_new();
  purple_request_fields_add_group(request,group);
  purple_request_fields((action -> plugin),"I\'dle Mak\'er",((const char *)(dgettext("pidgin","Set Account Idle Time"))),0,request,((const char *)(dgettext("pidgin","_Set"))),((GCallback )idle_action_ok),((const char *)(dgettext("pidgin","_Cancel"))),0,0,0,0,0);
}

static void unidle_action(PurplePluginAction *action)
{
  PurpleRequestFields *request;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  if (idled_accts == ((GList *)((void *)0))) {
    purple_notify_message(0,PURPLE_NOTIFY_MSG_INFO,0,((const char *)(dgettext("pidgin","None of your accounts are idle."))),0,0,0);
    return ;
  }
  group = purple_request_field_group_new(0);
  field = purple_request_field_account_new("acct",((const char *)(dgettext("pidgin","Account"))),0);
  purple_request_field_account_set_filter(field,unidle_filter);
  purple_request_field_account_set_show_all(field,0);
  purple_request_field_group_add_field(group,field);
  request = purple_request_fields_new();
  purple_request_fields_add_group(request,group);
  purple_request_fields((action -> plugin),"I\'dle Mak\'er",((const char *)(dgettext("pidgin","Unset Account Idle Time"))),0,request,((const char *)(dgettext("pidgin","_Unset"))),((GCallback )unidle_action_ok),((const char *)(dgettext("pidgin","_Cancel"))),0,0,0,0,0);
}

static void idle_all_action(PurplePluginAction *action)
{
  PurpleRequestFields *request;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  group = purple_request_field_group_new(0);
  field = purple_request_field_int_new("mins",((const char *)(dgettext("pidgin","Minutes"))),10);
  purple_request_field_group_add_field(group,field);
  request = purple_request_fields_new();
  purple_request_fields_add_group(request,group);
  purple_request_fields((action -> plugin),"I\'dle Mak\'er",((const char *)(dgettext("pidgin","Set Idle Time for All Accounts"))),0,request,((const char *)(dgettext("pidgin","_Set"))),((GCallback )idle_all_action_ok),((const char *)(dgettext("pidgin","_Cancel"))),0,0,0,0,0);
}

static void unidle_all_action(PurplePluginAction *action)
{
  GList *l;
/* freeing the list here will cause segfaults if the user idles an account
	 * after the list is freed */
  for (l = idled_accts; l != 0; l = (l -> next)) {
    PurpleAccount *account = (l -> data);
    set_idle_time(account,0);
  }
  g_list_free(idled_accts);
  idled_accts = ((GList *)((void *)0));
}

static GList *actions(PurplePlugin *plugin,gpointer context)
{
  GList *l = (GList *)((void *)0);
  PurplePluginAction *act = (PurplePluginAction *)((void *)0);
  act = purple_plugin_action_new(((const char *)(dgettext("pidgin","Set Account Idle Time"))),idle_action);
  l = g_list_append(l,act);
  act = purple_plugin_action_new(((const char *)(dgettext("pidgin","Unset Account Idle Time"))),unidle_action);
  l = g_list_append(l,act);
  act = purple_plugin_action_new(((const char *)(dgettext("pidgin","Set Idle Time for All Accounts"))),idle_all_action);
  l = g_list_append(l,act);
  act = purple_plugin_action_new(((const char *)(dgettext("pidgin","Unset Idle Time for All Idled Accounts"))),unidle_all_action);
  l = g_list_append(l,act);
  return l;
}

static void signing_off_cb(PurpleConnection *gc,void *data)
{
  PurpleAccount *account;
  account = purple_connection_get_account(gc);
  idled_accts = g_list_remove(idled_accts,account);
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  purple_signal_connect(purple_connections_get_handle(),"signing-off",plugin,((PurpleCallback )signing_off_cb),0);
  return (!0);
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  unidle_all_action(0);
  return (!0);
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("core-idle"), ("I\'dle Mak\'er"), ("2.10.9"), ("Allows you to hand-configure how long you\'ve been idle"), ("Allows you to hand-configure how long you\'ve been idle"), ("Eric Warmenhoven <eric@warmenhoven.org>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), (actions), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* This is a cultural reference.  Dy'er Mak'er is a song by Led Zeppelin.
	   If that doesn't translate well into your language, drop the 's before translating. */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
