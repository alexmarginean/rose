/*
 * One Time Password support plugin for libpurple
 *
 * Copyright (C) 2009, Daniel Atallah <datallah@pidgin.im>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02111-1301, USA.
 */
#include "internal.h"
#include "debug.h"
#include "plugin.h"
#include "version.h"
#include "account.h"
#include "accountopt.h"
#define PLUGIN_ID "core-one_time_password"
#define PREF_NAME PLUGIN_ID "_enabled"

static void signed_on_cb(PurpleConnection *conn,void *data)
{
  PurpleAccount *account = purple_connection_get_account(conn);
  if (purple_account_get_bool(account,"core-one_time_password_enabled",0) != 0) {
    if (purple_account_get_remember_password(account) != 0) 
      purple_debug_error("One Time Password","Unable to enforce one time password for account %s (%s).\nAccount is set to remember the password.\n",purple_account_get_username(account),purple_account_get_protocol_name(account));
    else {
      purple_debug_info("One Time Password","Clearing password for account %s (%s).\n",purple_account_get_username(account),purple_account_get_protocol_name(account));
      purple_account_set_password(account,0);
/* TODO: Do we need to somehow clear conn->password ? */
    }
  }
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  PurpleAccountOption *option;
  GList *l;
/* Register protocol preference. */
  for (l = purple_plugins_get_protocols(); l != ((GList *)((void *)0)); l = (l -> next)) {
    prpl = ((PurplePlugin *)(l -> data));
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
    if ((prpl_info != ((PurplePluginProtocolInfo *)((void *)0))) && !(((prpl_info -> options) & OPT_PROTO_NO_PASSWORD) != 0U)) {
      option = purple_account_option_bool_new(((const char *)(dgettext("pidgin","One Time Password"))),"core-one_time_password_enabled",0);
      prpl_info -> protocol_options = g_list_append((prpl_info -> protocol_options),option);
    }
  }
/* Register callback. */
  purple_signal_connect(purple_connections_get_handle(),"signed-on",plugin,((PurpleCallback )signed_on_cb),0);
  return (!0);
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
  PurpleAccountOption *option;
  GList *l;
  GList *options;
/* Remove protocol preference. */
  for (l = purple_plugins_get_protocols(); l != ((GList *)((void *)0)); l = (l -> next)) {
    prpl = ((PurplePlugin *)(l -> data));
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
    if ((prpl_info != ((PurplePluginProtocolInfo *)((void *)0))) && !(((prpl_info -> options) & OPT_PROTO_NO_PASSWORD) != 0U)) {
      options = (prpl_info -> protocol_options);
{
        while(options != ((GList *)((void *)0))){
          option = ((PurpleAccountOption *)(options -> data));
          if (strcmp("core-one_time_password_enabled",purple_account_option_get_setting(option)) == 0) {
            prpl_info -> protocol_options = g_list_delete_link((prpl_info -> protocol_options),options);
            purple_account_option_destroy(option);
            break; 
          }
          options = (options -> next);
        }
      }
    }
  }
/* Callback will be automagically unregistered */
  return (!0);
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("core-one_time_password"), ("One Time Password Support"), ("2.10.9"), ("Enforce that passwords are used only once."), ("Allows you to enforce on a per-account basis that passwords not being saved are only used in a single successful connection.\nNote: The account password must not be saved for this to work."), ("Daniel Atallah <datallah@pidgin.im>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/**< prefs_info     */
/**< actions        */
/**< reserved 1     */
/**< reserved 2     */
/**< reserved 3     */
/**< reserved 4     */
};

static void init_plugin(PurplePlugin *plugin)
{
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
