/**
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "conversation.h"
#include "debug.h"
#include "plugin.h"
#include "version.h"
#define JOINPART_PLUGIN_ID "core-rlaager-joinpart"
/* Preferences */
/* The number of minutes before a person is considered
 * to have stopped being part of active conversation. */
#define DELAY_PREF "/plugins/core/joinpart/delay"
#define DELAY_DEFAULT 10
/* The number of people that must be in a room for this
 * plugin to have any effect */
#define THRESHOLD_PREF "/plugins/core/joinpart/threshold"
#define THRESHOLD_DEFAULT 20
/* Hide buddies */
#define HIDE_BUDDIES_PREF "/plugins/core/joinpart/hide_buddies"
#define HIDE_BUDDIES_DEFAULT FALSE

struct joinpart_key 
{
  PurpleConversation *conv;
  char *user;
}
;

static guint joinpart_key_hash(const struct joinpart_key *key)
{
  do {
    if (key != ((const struct joinpart_key *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"key != NULL");
      return 0;
    };
  }while (0);
  return g_direct_hash((key -> conv)) + g_str_hash((key -> user));
}

static gboolean joinpart_key_equal(const struct joinpart_key *a,const struct joinpart_key *b)
{
  if (a == ((const struct joinpart_key *)((void *)0))) 
    return b == ((const struct joinpart_key *)((void *)0));
  else if (b == ((const struct joinpart_key *)((void *)0))) 
    return 0;
  return ((a -> conv) == (b -> conv)) && !(strcmp((a -> user),(b -> user)) != 0);
}

static void joinpart_key_destroy(struct joinpart_key *key)
{
  do {
    if (key != ((struct joinpart_key *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"key != NULL");
      return ;
    };
  }while (0);
  g_free((key -> user));
  g_free(key);
}

static gboolean should_hide_notice(PurpleConversation *conv,const char *name,GHashTable *users)
{
  PurpleConvChat *chat;
  int threshold;
  struct joinpart_key key;
  time_t *last_said;
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_conversation_get_type(conv) == PURPLE_CONV_TYPE_CHAT");
      return 0;
    };
  }while (0);
/* If the room is small, don't bother. */
  chat = purple_conversation_get_chat_data(conv);
  threshold = purple_prefs_get_int("/plugins/core/joinpart/threshold");
  if (g_list_length(purple_conv_chat_get_users(chat)) < threshold) 
    return 0;
  if (!(purple_prefs_get_bool("/plugins/core/joinpart/hide_buddies") != 0) && (purple_find_buddy(purple_conversation_get_account(conv),name) != 0)) 
    return 0;
/* Only show the notice if the user has spoken recently. */
  key.conv = conv;
  key.user = ((gchar *)name);
  last_said = (g_hash_table_lookup(users,(&key)));
  if (last_said != ((time_t *)((void *)0))) {
    int delay = purple_prefs_get_int("/plugins/core/joinpart/delay");
    if ((delay > 0) && (( *last_said + (delay * 60)) >= time(0))) 
      return 0;
  }
  return (!0);
}

static gboolean chat_buddy_leaving_cb(PurpleConversation *conv,const char *name,const char *reason,GHashTable *users)
{
  return should_hide_notice(conv,name,users);
}

static gboolean chat_buddy_joining_cb(PurpleConversation *conv,const char *name,PurpleConvChatBuddyFlags flags,GHashTable *users)
{
  return should_hide_notice(conv,name,users);
}

static void received_chat_msg_cb(PurpleAccount *account,char *sender,char *message,PurpleConversation *conv,PurpleMessageFlags flags,GHashTable *users)
{
  struct joinpart_key key;
  time_t *last_said;
/* Most of the time, we'll already have tracked the user,
	 * so we avoid memory allocation here. */
  key.conv = conv;
  key.user = sender;
  last_said = (g_hash_table_lookup(users,(&key)));
  if (last_said != ((time_t *)((void *)0))) {
/* They just said something, so update the time. */
    time(last_said);
  }
  else {
    struct joinpart_key *key2;
    key2 = ((struct joinpart_key *)(g_malloc_n(1,(sizeof(struct joinpart_key )))));
    key2 -> conv = conv;
    key2 -> user = g_strdup(sender);
    last_said = ((time_t *)(g_malloc_n(1,(sizeof(time_t )))));
    time(last_said);
    g_hash_table_insert(users,key2,last_said);
  }
}

static gboolean check_expire_time(struct joinpart_key *key,time_t *last_said,time_t *limit)
{
  purple_debug_info("joinpart","Removing key for %s\n",(key -> user));
  return  *last_said <  *limit;
}

static gboolean clean_users_hash(GHashTable *users)
{
  int delay = purple_prefs_get_int("/plugins/core/joinpart/delay");
  time_t limit = (time(0) - (60 * delay));
  g_hash_table_foreach_remove(users,((GHRFunc )check_expire_time),(&limit));
  return (!0);
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  void *conv_handle;
  GHashTable *users;
  guint id;
  gpointer *data;
  users = g_hash_table_new_full(((GHashFunc )joinpart_key_hash),((GEqualFunc )joinpart_key_equal),((GDestroyNotify )joinpart_key_destroy),g_free);
  conv_handle = purple_conversations_get_handle();
  purple_signal_connect(conv_handle,"chat-buddy-joining",plugin,((PurpleCallback )chat_buddy_joining_cb),users);
  purple_signal_connect(conv_handle,"chat-buddy-leaving",plugin,((PurpleCallback )chat_buddy_leaving_cb),users);
  purple_signal_connect(conv_handle,"received-chat-msg",plugin,((PurpleCallback )received_chat_msg_cb),users);
/* Cleanup every 5 minutes */
  id = purple_timeout_add_seconds((60 * 5),((GSourceFunc )clean_users_hash),users);
  data = ((gpointer *)(g_malloc_n(2,(sizeof(gpointer )))));
  data[0] = users;
  data[1] = ((gpointer )((gulong )id));
  plugin -> extra = data;
  return (!0);
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  gpointer *data = (plugin -> extra);
/* Destroy the hash table. The core plugin code will
	 * disconnect the signals, and since Purple is single-threaded,
	 * we don't have to worry one will be called after this. */
  g_hash_table_destroy(((GHashTable *)data[0]));
  purple_timeout_remove(((guint )((gulong )data[1])));
  g_free(data);
  return (!0);
}

static PurplePluginPrefFrame *get_plugin_pref_frame(PurplePlugin *plugin)
{
  PurplePluginPrefFrame *frame;
  PurplePluginPref *ppref;
  do {
    if (plugin != ((PurplePlugin *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin != NULL");
      return 0;
    };
  }while (0);
  frame = purple_plugin_pref_frame_new();
  ppref = purple_plugin_pref_new_with_label(((const char *)(dgettext("pidgin","Hide Joins/Parts"))));
  purple_plugin_pref_frame_add(frame,ppref);
  ppref = purple_plugin_pref_new_with_name_and_label("/plugins/core/joinpart/threshold",((const char *)(dgettext("pidgin","For rooms with more than this many people"))));
/* Translators: Followed by an input request a number of people */
  purple_plugin_pref_set_bounds(ppref,0,1000);
  purple_plugin_pref_frame_add(frame,ppref);
  ppref = purple_plugin_pref_new_with_name_and_label("/plugins/core/joinpart/delay",((const char *)(dgettext("pidgin","If user has not spoken in this many minutes"))));
/* 8 Hours */
  purple_plugin_pref_set_bounds(ppref,0,8 * 60);
  purple_plugin_pref_frame_add(frame,ppref);
  ppref = purple_plugin_pref_new_with_name_and_label("/plugins/core/joinpart/hide_buddies",((const char *)(dgettext("pidgin","Apply hiding rules to buddies"))));
  purple_plugin_pref_frame_add(frame,ppref);
  return frame;
}
static PurplePluginUiInfo prefs_info = {(get_plugin_pref_frame), (0), ((PurplePluginPrefFrame *)((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* page_num (reserved) */
/* frame (reserved) */
/* padding */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("core-rlaager-joinpart"), ("Join/Part Hiding"), ("2.10.9"), ("Hides extraneous join/part messages."), ("This plugin hides join/part messages in large rooms, except for those users actively taking part in a conversation."), ("Richard Laager <rlaager@pidgin.im>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), (&prefs_info), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/**< prefs_info     */
/**< actions        */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
  purple_prefs_add_none("/plugins/core/joinpart");
  purple_prefs_add_int("/plugins/core/joinpart/delay",10);
  purple_prefs_add_int("/plugins/core/joinpart/threshold",20);
  purple_prefs_add_bool("/plugins/core/joinpart/hide_buddies",0);
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
