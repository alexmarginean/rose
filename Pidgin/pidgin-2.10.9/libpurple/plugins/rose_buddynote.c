/*
 * BuddyNote - Store notes on particular buddies
 * Copyright (C) 2004 Stu Tomlinson <stu@nosnilmot.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301, USA.
 */
#include "internal.h"
#include <debug.h>
#include <notify.h>
#include <request.h>
#include <signals.h>
#include <util.h>
#include <version.h>

static void dont_do_it_cb(PurpleBlistNode *node,const char *note)
{
}

static void do_it_cb(PurpleBlistNode *node,const char *note)
{
  purple_blist_node_set_string(node,"notes",note);
}

static void buddynote_edit_cb(PurpleBlistNode *node,gpointer data)
{
  const char *note;
  note = purple_blist_node_get_string(node,"notes");
  purple_request_input(node,((const char *)(dgettext("pidgin","Notes"))),((const char *)(dgettext("pidgin","Enter your notes below..."))),0,note,(!0),0,"html",((const char *)(dgettext("pidgin","Save"))),((GCallback )do_it_cb),((const char *)(dgettext("pidgin","Cancel"))),((GCallback )dont_do_it_cb),0,0,0,node);
}

static void buddynote_extended_menu_cb(PurpleBlistNode *node,GList **m)
{
  PurpleMenuAction *bna = (PurpleMenuAction *)((void *)0);
  if (((purple_blist_node_get_flags(node)) & PURPLE_BLIST_NODE_FLAG_NO_SAVE) != 0U) 
    return ;
   *m = g_list_append( *m,bna);
  bna = purple_menu_action_new(((const char *)(dgettext("pidgin","Edit Notes..."))),((PurpleCallback )buddynote_edit_cb),0,0);
   *m = g_list_append( *m,bna);
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  purple_signal_connect(purple_blist_get_handle(),"blist-node-extended-menu",plugin,((PurpleCallback )buddynote_extended_menu_cb),0);
  return (!0);
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("core-plugin_pack-buddynote"), ("Buddy Notes"), ("2.10.9"), ("Store notes on particular buddies."), ("Adds the option to store notes for buddies on your buddy list."), ("Stu Tomlinson <stu@nosnilmot.com>"), ("http://pidgin.im/"), (plugin_load), ((gboolean (*)(PurplePlugin *))((void *)0)), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< major version	*/
/**< minor version	*/
/**< type			*/
/**< ui_requirement	*/
/**< flags			*/
/**< dependencies	*/
/**< priority		*/
/**< id				*/
/**< name			*/
/**< version		*/
/**< summary		*/
/**< description	*/
/**< author			*/
/**< homepage		*/
/**< load			*/
/**< unload			*/
/**< destroy		*/
/**< ui_info		*/
/**< extra_info		*/
/**< prefs_info		*/
/**< actions		*/
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
