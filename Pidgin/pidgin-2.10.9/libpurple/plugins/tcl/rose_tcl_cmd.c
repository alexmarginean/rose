/**
 * @file tcl_cmd.c Purple Tcl cmd API
 *
 * purple
 *
 * Copyright (C) 2006 Etan Reisner <deryni@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include <tcl.h>
#include "tcl_purple.h"
#include "internal.h"
#include "cmds.h"
#include "debug.h"
static GList *tcl_cmd_callbacks;
static PurpleCmdRet tcl_cmd_callback(PurpleConversation *conv,const gchar *cmd,gchar **args,gchar **errors,struct tcl_cmd_handler *handler);
static Tcl_Obj *new_cmd_cb_namespace();

void tcl_cmd_init()
{
  tcl_cmd_callbacks = ((GList *)((void *)0));
}

void tcl_cmd_handler_free(struct tcl_cmd_handler *handler)
{
  if (handler == ((struct tcl_cmd_handler *)((void *)0))) 
    return ;
  do {
    Tcl_Obj *_objPtr = (handler -> namespace);
    if (--_objPtr -> refCount <= 0) {
      TclFreeObj(_objPtr);
    }
  }while (0);
  g_free(handler);
}

void tcl_cmd_cleanup(Tcl_Interp *interp)
{
  GList *cur;
  struct tcl_cmd_handler *handler;
  for (cur = tcl_cmd_callbacks; cur != ((GList *)((void *)0)); cur = ((cur != 0)?( *((GList *)cur)).next : ((struct _GList *)((void *)0)))) {
    handler = (cur -> data);
    if ((handler -> interp) == interp) {
      purple_cmd_unregister((handler -> id));
      tcl_cmd_handler_free(handler);
      cur -> data = ((gpointer )((void *)0));
    }
  }
  tcl_cmd_callbacks = g_list_remove_all(tcl_cmd_callbacks,0);
}

PurpleCmdId tcl_cmd_register(struct tcl_cmd_handler *handler)
{
  int id;
  GString *proc;
  if ((id = (purple_cmd_register((Tcl_GetString((handler -> cmd))),(handler -> args),(handler -> priority),(handler -> flags),(handler -> prpl_id),((PurpleCmdFunc )tcl_cmd_callback),(handler -> helpstr),((void *)handler)))) == 0) 
    return 0;
  handler -> namespace = new_cmd_cb_namespace();
  ++( *(handler -> namespace)).refCount;
  proc = g_string_new("");
  g_string_append_printf(proc,"namespace eval %s { proc cb { conv cmd arglist } { %s } }",Tcl_GetString((handler -> namespace)),Tcl_GetString((handler -> proc)));
  if (Tcl_Eval((handler -> interp),(proc -> str)) != 0) {
    do {
      Tcl_Obj *_objPtr = (handler -> namespace);
      if (--_objPtr -> refCount <= 0) {
        TclFreeObj(_objPtr);
      }
    }while (0);
    g_string_free(proc,(!0));
    return 0;
  }
  g_string_free(proc,(!0));
  tcl_cmd_callbacks = g_list_append(tcl_cmd_callbacks,((gpointer )handler));
  return id;
}

void tcl_cmd_unregister(PurpleCmdId id,Tcl_Interp *interp)
{
  GList *cur;
  GString *cmd;
  gboolean found = 0;
  struct tcl_cmd_handler *handler;
{
    for (cur = tcl_cmd_callbacks; cur != ((GList *)((void *)0)); cur = ((cur != 0)?( *((GList *)cur)).next : ((struct _GList *)((void *)0)))) {
      handler = (cur -> data);
      if (((handler -> interp) == interp) && ((handler -> id) == id)) {
        purple_cmd_unregister(id);
        cmd = g_string_sized_new(64);
        g_string_printf(cmd,"namespace delete %s",Tcl_GetString((handler -> namespace)));
        Tcl_EvalEx(interp,(cmd -> str),-1,0x020000);
        tcl_cmd_handler_free(handler);
        g_string_free(cmd,(!0));
        cur -> data = ((gpointer )((void *)0));
        found = (!0);
        break; 
      }
    }
  }
  if (found != 0) 
    tcl_cmd_callbacks = g_list_remove_all(tcl_cmd_callbacks,0);
}

static PurpleCmdRet tcl_cmd_callback(PurpleConversation *conv,const gchar *cmd,gchar **args,gchar **errors,struct tcl_cmd_handler *handler)
{
  int retval;
  int i;
  Tcl_Obj *command;
  Tcl_Obj *arg;
  Tcl_Obj *tclargs;
  Tcl_Obj *result;
  command = Tcl_NewListObj(0,0);
  ++command -> refCount;
/* The callback */
  arg = Tcl_DuplicateObj((handler -> namespace));
  Tcl_AppendStringsToObj(arg,"::cb",((void *)((void *)0)));
  Tcl_ListObjAppendElement((handler -> interp),command,arg);
/* The conversation */
  arg = purple_tcl_ref_new(PurpleTclRefConversation,conv);
  Tcl_ListObjAppendElement((handler -> interp),command,arg);
/* The command */
  arg = Tcl_NewStringObj(cmd,-1);
  Tcl_ListObjAppendElement((handler -> interp),command,arg);
/* The args list */
  tclargs = Tcl_NewListObj(0,0);
  for (i = 0; i < (handler -> nargs); i++) {
    arg = Tcl_NewStringObj(args[i],-1);
    Tcl_ListObjAppendElement((handler -> interp),tclargs,arg);
  }
  Tcl_ListObjAppendElement((handler -> interp),command,tclargs);
  if (Tcl_EvalObjEx((handler -> interp),command,0x020000) != 0) {
    gchar *errorstr;
    errorstr = g_strdup_printf("error evaluating callback: %s\n",Tcl_GetString(Tcl_GetObjResult((handler -> interp))));
    purple_debug(PURPLE_DEBUG_ERROR,"tcl","%s",errorstr);
     *errors = errorstr;
    retval = PURPLE_CMD_RET_FAILED;
  }
  else {
    result = Tcl_GetObjResult((handler -> interp));
    if (Tcl_GetIntFromObj((handler -> interp),result,&retval) != 0) {
      gchar *errorstr;
      errorstr = g_strdup_printf("Error retreiving procedure result: %s\n",Tcl_GetString(Tcl_GetObjResult((handler -> interp))));
      purple_debug(PURPLE_DEBUG_ERROR,"tcl","%s",errorstr);
       *errors = errorstr;
      retval = PURPLE_CMD_RET_FAILED;
    }
  }
  return retval;
}

static Tcl_Obj *new_cmd_cb_namespace()
{
  char name[32UL];
  static int cbnum;
  g_snprintf(name,(sizeof(name)),"::purple::_cmd_callback::cb_%d",cbnum++);
  return Tcl_NewStringObj(name,-1);
}
