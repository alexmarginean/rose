#include "internal.h"
#include "debug.h"
#include "plugin.h"
#include "version.h"
/** Plugin id : type-author-name (to guarantee uniqueness) */
#define SIMPLE_PLUGIN_ID "core-ewarmenhoven-simple"

static gboolean plugin_load(PurplePlugin *plugin)
{
  purple_debug(PURPLE_DEBUG_INFO,"simple","simple plugin loaded.\n");
  return (!0);
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  purple_debug(PURPLE_DEBUG_INFO,"simple","simple plugin unloaded.\n");
  return (!0);
}
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("core-ewarmenhoven-simple"), ("Simple Plugin"), ("2.10.9"), ("Tests to see that most things are working."), ("Tests to see that most things are working."), ("Eric Warmenhoven <eric@warmenhoven.org>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), ((PurplePluginUiInfo *)((void *)0)), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/* Padding */
};

static void init_plugin(PurplePlugin *plugin)
{
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
