#include "internal.h"
#include "blist.h"
#include "conversation.h"
#include "debug.h"
#include "signals.h"
#include "version.h"
#include "plugin.h"
#include "pluginpref.h"
#include "prefs.h"
#define STATENOTIFY_PLUGIN_ID "core-statenotify"

static void write_status(PurpleBuddy *buddy,const char *message)
{
  PurpleAccount *account = (PurpleAccount *)((void *)0);
  PurpleConversation *conv;
  const char *who;
  char buf[256UL];
  char *escaped;
  const gchar *buddy_name = (const gchar *)((void *)0);
  account = purple_buddy_get_account(buddy);
  buddy_name = purple_buddy_get_name(buddy);
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,buddy_name,account);
  if (conv == ((PurpleConversation *)((void *)0))) 
    return ;
  do {
    if ((conv -> type) == PURPLE_CONV_TYPE_IM) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv->type == PURPLE_CONV_TYPE_IM");
      return ;
    };
  }while (0);
/* Prevent duplicate notifications for buddies in multiple groups */
  if (buddy != purple_find_buddy(account,buddy_name)) 
    return ;
  who = purple_buddy_get_alias(buddy);
  escaped = g_markup_escape_text(who,(-1));
  g_snprintf(buf,(sizeof(buf)),message,escaped);
  g_free(escaped);
  purple_conv_im_write(conv -> u.im,0,buf,(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_ACTIVE_ONLY | PURPLE_MESSAGE_NO_LINKIFY),time(0));
}

static void buddy_status_changed_cb(PurpleBuddy *buddy,PurpleStatus *old_status,PurpleStatus *status,void *data)
{
  gboolean available;
  gboolean old_available;
  if (!(purple_status_is_exclusive(status) != 0) || !(purple_status_is_exclusive(old_status) != 0)) 
    return ;
  available = purple_status_is_available(status);
  old_available = purple_status_is_available(old_status);
  if (purple_prefs_get_bool("/plugins/core/statenotify/notify_away") != 0) {
    if ((available != 0) && !(old_available != 0)) 
      write_status(buddy,((const char *)(dgettext("pidgin","%s is no longer away."))));
    else if (!(available != 0) && (old_available != 0)) 
      write_status(buddy,((const char *)(dgettext("pidgin","%s has gone away."))));
  }
}

static void buddy_idle_changed_cb(PurpleBuddy *buddy,gboolean old_idle,gboolean idle,void *data)
{
  if (purple_prefs_get_bool("/plugins/core/statenotify/notify_idle") != 0) {
    if ((idle != 0) && !(old_idle != 0)) {
      write_status(buddy,((const char *)(dgettext("pidgin","%s has become idle."))));
    }
    else if (!(idle != 0) && (old_idle != 0)) {
      write_status(buddy,((const char *)(dgettext("pidgin","%s is no longer idle."))));
    }
  }
}

static void buddy_signon_cb(PurpleBuddy *buddy,void *data)
{
  if (purple_prefs_get_bool("/plugins/core/statenotify/notify_signon") != 0) 
    write_status(buddy,((const char *)(dgettext("pidgin","%s has signed on."))));
}

static void buddy_signoff_cb(PurpleBuddy *buddy,void *data)
{
  if (purple_prefs_get_bool("/plugins/core/statenotify/notify_signon") != 0) 
    write_status(buddy,((const char *)(dgettext("pidgin","%s has signed off."))));
}

static PurplePluginPrefFrame *get_plugin_pref_frame(PurplePlugin *plugin)
{
  PurplePluginPrefFrame *frame;
  PurplePluginPref *ppref;
  frame = purple_plugin_pref_frame_new();
  ppref = purple_plugin_pref_new_with_label(((const char *)(dgettext("pidgin","Notify When"))));
  purple_plugin_pref_frame_add(frame,ppref);
  ppref = purple_plugin_pref_new_with_name_and_label("/plugins/core/statenotify/notify_away",((const char *)(dgettext("pidgin","Buddy Goes _Away"))));
  purple_plugin_pref_frame_add(frame,ppref);
  ppref = purple_plugin_pref_new_with_name_and_label("/plugins/core/statenotify/notify_idle",((const char *)(dgettext("pidgin","Buddy Goes _Idle"))));
  purple_plugin_pref_frame_add(frame,ppref);
  ppref = purple_plugin_pref_new_with_name_and_label("/plugins/core/statenotify/notify_signon",((const char *)(dgettext("pidgin","Buddy _Signs On/Off"))));
  purple_plugin_pref_frame_add(frame,ppref);
  return frame;
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  void *blist_handle = purple_blist_get_handle();
  purple_signal_connect(blist_handle,"buddy-status-changed",plugin,((PurpleCallback )buddy_status_changed_cb),0);
  purple_signal_connect(blist_handle,"buddy-idle-changed",plugin,((PurpleCallback )buddy_idle_changed_cb),0);
  purple_signal_connect(blist_handle,"buddy-signed-on",plugin,((PurpleCallback )buddy_signon_cb),0);
  purple_signal_connect(blist_handle,"buddy-signed-off",plugin,((PurpleCallback )buddy_signoff_cb),0);
  return (!0);
}
static PurplePluginUiInfo prefs_info = {(get_plugin_pref_frame), (0), ((PurplePluginPrefFrame *)((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* page_num (Reserved) */
/* frame (Reserved) */
/* padding */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("core-statenotify"), ("Buddy State Notification"), ("2.10.9"), ("Notifies in a conversation window when a buddy goes or returns from away or idle."), ("Notifies in a conversation window when a buddy goes or returns from away or idle."), ("Christian Hammond <chipx86@gnupdate.org>"), ("http://pidgin.im/"), (plugin_load), ((gboolean (*)(PurplePlugin *))((void *)0)), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), (&prefs_info), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type           */
/**< ui_requirement */
/**< flags          */
/**< dependencies   */
/**< priority       */
/**< id             */
/**< name           */
/**< version        */
/**  summary        */
/**  description    */
/**< author         */
/**< homepage       */
/**< load           */
/**< unload         */
/**< destroy        */
/**< ui_info        */
/**< extra_info     */
/**< prefs_info     */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
  purple_prefs_add_none("/plugins/core/statenotify");
  purple_prefs_add_bool("/plugins/core/statenotify/notify_away",(!0));
  purple_prefs_add_bool("/plugins/core/statenotify/notify_idle",(!0));
  purple_prefs_add_bool("/plugins/core/statenotify/notify_signon",(!0));
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
