/*
 * Displays messages on a new line, below the nick
 * Copyright (C) 2004 Stu Tomlinson <stu@nosnilmot.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301, USA.
 */
#include "internal.h"
#include <string.h>
#include <conversation.h>
#include <debug.h>
#include <plugin.h>
#include <signals.h>
#include <util.h>
#include <version.h>

static gboolean addnewline_msg_cb(PurpleAccount *account,char *sender,char **message,PurpleConversation *conv,int *flags,void *data)
{
  if ((((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) && !(purple_prefs_get_bool("/plugins/core/newline/im") != 0)) || (((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) && !(purple_prefs_get_bool("/plugins/core/newline/chat") != 0))) 
    return 0;
  if (g_ascii_strncasecmp(( *message),"/me ",strlen("/me ")) != 0) {
    char *tmp = g_strdup_printf("<br/>%s", *message);
    g_free(( *message));
     *message = tmp;
  }
  return 0;
}

static PurplePluginPrefFrame *get_plugin_pref_frame(PurplePlugin *plugin)
{
  PurplePluginPrefFrame *frame;
  PurplePluginPref *ppref;
  frame = purple_plugin_pref_frame_new();
  ppref = purple_plugin_pref_new_with_name_and_label("/plugins/core/newline/im",((const char *)(dgettext("pidgin","Add new line in IMs"))));
  purple_plugin_pref_frame_add(frame,ppref);
  ppref = purple_plugin_pref_new_with_name_and_label("/plugins/core/newline/chat",((const char *)(dgettext("pidgin","Add new line in Chats"))));
  purple_plugin_pref_frame_add(frame,ppref);
  return frame;
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  void *conversation = purple_conversations_get_handle();
  purple_signal_connect(conversation,"writing-im-msg",plugin,((PurpleCallback )addnewline_msg_cb),0);
  purple_signal_connect(conversation,"writing-chat-msg",plugin,((PurpleCallback )addnewline_msg_cb),0);
  return (!0);
}
static PurplePluginUiInfo prefs_info = {(get_plugin_pref_frame), (0), ((PurplePluginPrefFrame *)((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* page_num (Reserved) */
/* frame (Reserved) */
/* Padding */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("core-plugin_pack-newline"), ("New Line"), ("2.10.9"), ("Prepends a newline to displayed message."), ("Prepends a newline to messages so that the rest of the message appears below the username in the conversation window."), ("Stu Tomlinson <stu@nosnilmot.com>"), ("http://pidgin.im/"), (plugin_load), ((gboolean (*)(PurplePlugin *))((void *)0)), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), (&prefs_info), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< magic			*/
/**< major version	*/
/**< minor version	*/
/**< type			*/
/**< ui_requirement	*/
/**< flags			*/
/**< dependencies	*/
/**< priority		*/
/**< id				*/
/**< name			*/
/**< version		*/
/**< summary		*/
/**< description	*/
/**< author			*/
/**< homepage		*/
/**< load			*/
/**< unload			*/
/**< destroy		*/
/**< ui_info		*/
/**< extra_info		*/
/**< prefs_info		*/
/**< actions		*/
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
  purple_prefs_add_none("/plugins/core/newline");
  purple_prefs_add_bool("/plugins/core/newline/im",(!0));
  purple_prefs_add_bool("/plugins/core/newline/chat",(!0));
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
