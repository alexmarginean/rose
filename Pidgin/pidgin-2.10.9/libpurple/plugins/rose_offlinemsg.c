/*
 * Offline Message Emulation - Save messages sent to an offline user as pounce
 * Copyright (C) 2004
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02111-1301, USA.
 */
#include "internal.h"
#define PLUGIN_ID			"core-plugin_pack-offlinemsg"
#define PLUGIN_NAME			N_("Offline Message Emulation")
#define PLUGIN_STATIC_NAME	offlinemsg
#define PLUGIN_SUMMARY		N_("Save messages sent to an offline user as pounce.")
#define PLUGIN_DESCRIPTION	N_("Save messages sent to an offline user as pounce.")
#define PLUGIN_AUTHOR		"Sadrul H Chowdhury <sadrul@users.sourceforge.net>"
/* Purple headers */
#include <version.h>
#include <blist.h>
#include <conversation.h>
#include <core.h>
#include <debug.h>
#include <pounce.h>
#include <request.h>
#define	PREF_PREFIX		"/plugins/core/" PLUGIN_ID
#define	PREF_ALWAYS		PREF_PREFIX "/always"
typedef struct _OfflineMsg OfflineMsg;
typedef enum __unnamed_enum___F0_L44_C9_OFFLINE_MSG_NONE__COMMA__OFFLINE_MSG_YES__COMMA__OFFLINE_MSG_NO {OFFLINE_MSG_NONE,OFFLINE_MSG_YES,OFFLINE_MSG_NO}OfflineMessageSetting;

struct _OfflineMsg 
{
  PurpleAccount *account;
  PurpleConversation *conv;
  char *who;
  char *message;
}
;

static void discard_data(OfflineMsg *offline)
{
  g_free((offline -> who));
  g_free((offline -> message));
  g_free(offline);
}

static void cancel_poune(OfflineMsg *offline)
{
  purple_conversation_set_data((offline -> conv),"plugin_pack:offlinemsg",((gpointer )((gpointer )((glong )OFFLINE_MSG_NO))));
  purple_conv_im_send_with_flags(purple_conversation_get_im_data((offline -> conv)),(offline -> message),0);
  discard_data(offline);
}

static void record_pounce(OfflineMsg *offline)
{
  PurplePounce *pounce;
  PurplePounceEvent event;
  PurplePounceOption option;
  PurpleConversation *conv;
  event = PURPLE_POUNCE_SIGNON;
  option = PURPLE_POUNCE_OPTION_NONE;
  pounce = purple_pounce_new(purple_core_get_ui(),(offline -> account),(offline -> who),event,option);
  purple_pounce_action_set_enabled(pounce,"send-message",(!0));
  purple_pounce_action_set_attribute(pounce,"send-message","message",(offline -> message));
  conv = (offline -> conv);
  if (!(purple_conversation_get_data(conv,"plugin_pack:offlinemsg") != 0)) 
    purple_conversation_write(conv,0,((const char *)(dgettext("pidgin","The rest of the messages will be saved as pounces. You can edit/delete the pounce from the `Buddy Pounce\' dialog."))),PURPLE_MESSAGE_SYSTEM,time(0));
  purple_conversation_set_data(conv,"plugin_pack:offlinemsg",((gpointer )((gpointer )((glong )OFFLINE_MSG_YES))));
  purple_conv_im_write(purple_conversation_get_im_data(conv),(offline -> who),(offline -> message),PURPLE_MESSAGE_SEND,time(0));
  discard_data(offline);
}

static void sending_msg_cb(PurpleAccount *account,const char *who,char **message,gpointer handle)
{
  PurpleBuddy *buddy;
  OfflineMsg *offline;
  PurpleConversation *conv;
  OfflineMessageSetting setting;
  if (((message == ((char **)((void *)0))) || ( *message == ((char *)((void *)0)))) || (( *( *message)) == 0)) 
    return ;
  buddy = purple_find_buddy(account,who);
  if (!(buddy != 0)) 
    return ;
  if (purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0) 
    return ;
  if (purple_account_supports_offline_message(account,buddy) != 0) {
    purple_debug_info("offlinemsg","Account \"%s\" supports offline messages.\n",purple_account_get_username(account));
    return ;
  }
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,who,account);
  if (!(conv != 0)) 
    return ;
  setting = ((gint )((glong )(purple_conversation_get_data(conv,"plugin_pack:offlinemsg"))));
  if (setting == OFFLINE_MSG_NO) 
    return ;
  offline = ((OfflineMsg *)(g_malloc0_n(1,(sizeof(OfflineMsg )))));
  offline -> conv = conv;
  offline -> account = account;
  offline -> who = g_strdup(who);
  offline -> message =  *message;
   *message = ((char *)((void *)0));
  if ((purple_prefs_get_bool("/plugins/core/core-plugin_pack-offlinemsg/always") != 0) || (setting == OFFLINE_MSG_YES)) 
    record_pounce(offline);
  else if (setting == OFFLINE_MSG_NONE) {
    char *ask;
    ask = g_strdup_printf(((const char *)(dgettext("pidgin","\"%s\" is currently offline. Do you want to save the rest of the messages in a pounce and automatically send them when \"%s\" logs back in\?"))),who,who);
    purple_request_action(handle,((const char *)(dgettext("pidgin","Offline Message"))),ask,((const char *)(dgettext("pidgin","You can edit/delete the pounce from the `Buddy Pounces\' dialog"))),0,(offline -> account),(offline -> who),(offline -> conv),offline,2,((const char *)(dgettext("pidgin","Yes"))),record_pounce,((const char *)(dgettext("pidgin","No"))),cancel_poune);
    g_free(ask);
  }
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  purple_signal_connect_priority(purple_conversations_get_handle(),"sending-im-msg",plugin,((PurpleCallback )sending_msg_cb),plugin,9999);
  return (!0);
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  return (!0);
}

static PurplePluginPrefFrame *get_plugin_pref_frame(PurplePlugin *plugin)
{
  PurplePluginPrefFrame *frame;
  PurplePluginPref *pref;
  frame = purple_plugin_pref_frame_new();
  pref = purple_plugin_pref_new_with_label(((const char *)(dgettext("pidgin","Save offline messages in pounce"))));
  purple_plugin_pref_frame_add(frame,pref);
  pref = purple_plugin_pref_new_with_name_and_label("/plugins/core/core-plugin_pack-offlinemsg/always",((const char *)(dgettext("pidgin","Do not ask. Always save in pounce."))));
  purple_plugin_pref_frame_add(frame,pref);
  return frame;
}
static PurplePluginUiInfo prefs_info = {(get_plugin_pref_frame), (0), ((PurplePluginPrefFrame *)((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* padding */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("core-plugin_pack-offlinemsg"), ("Offline Message Emulation"), ("2.10.9"), ("Save messages sent to an offline user as pounce."), ("Save messages sent to an offline user as pounce."), ("Sadrul H Chowdhury <sadrul@users.sourceforge.net>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), (&prefs_info), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* Magic				*/
/* Purple Major Version	*/
/* Purple Minor Version	*/
/* plugin type			*/
/* ui requirement		*/
/* flags				*/
/* dependencies			*/
/* priority				*/
/* plugin id			*/
/* name					*/
/* version				*/
/* summary				*/
/* description			*/
/* author				*/
/* website				*/
/* load					*/
/* unload				*/
/* destroy				*/
/* ui_info				*/
/* extra_info			*/
/* prefs_info			*/
/* actions				*/
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
  purple_prefs_add_none("/plugins/core/core-plugin_pack-offlinemsg");
  purple_prefs_add_bool("/plugins/core/core-plugin_pack-offlinemsg/always",0);
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
