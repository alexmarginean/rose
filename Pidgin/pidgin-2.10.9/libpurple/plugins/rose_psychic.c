#include "internal.h"
#include "account.h"
#include "blist.h"
#include "conversation.h"
#include "debug.h"
#include "signals.h"
#include "status.h"
#include "version.h"
#include "privacy.h"
#include "plugin.h"
#include "pluginpref.h"
#include "prefs.h"
#define PLUGIN_ID       "core-psychic"
#define PLUGIN_NAME     N_("Psychic Mode")
#define PLUGIN_SUMMARY  N_("Psychic mode for incoming conversation")
#define PLUGIN_DESC     N_("Causes conversation windows to appear as other" \
			   " users begin to message you.  This works for" \
			   " AIM, ICQ, XMPP, Sametime, and Yahoo!")
#define PLUGIN_AUTHOR   "Christopher O'Brien <siege@preoccupied.net>"
#define PREFS_BASE    "/plugins/core/psychic"
#define PREF_BUDDIES  PREFS_BASE "/buddies_only"
#define PREF_NOTICE   PREFS_BASE "/show_notice"
#define PREF_STATUS   PREFS_BASE "/activate_online"
#define PREF_RAISE    PREFS_BASE "/raise_conv"

static void buddy_typing_cb(PurpleAccount *acct,const char *name,void *data)
{
  PurpleConversation *gconv;
  if ((purple_prefs_get_bool("/plugins/core/psychic/activate_online") != 0) && !(purple_status_is_available((purple_account_get_active_status(acct))) != 0)) {
    purple_debug_info("psychic","not available, doing nothing\n");
    return ;
  }
  if ((purple_prefs_get_bool("/plugins/core/psychic/buddies_only") != 0) && !(purple_find_buddy(acct,name) != 0)) {
    purple_debug_info("psychic","not in blist, doing nothing\n");
    return ;
  }
  if (0 == purple_privacy_check(acct,name)) {
    purple_debug_info("psychic","user %s is blocked\n",name);
    return ;
  }
  gconv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,name,acct);
  if (!(gconv != 0)) {
    purple_debug_info("psychic","no previous conversation exists\n");
    gconv = purple_conversation_new(PURPLE_CONV_TYPE_IM,acct,name);
    if (purple_prefs_get_bool("/plugins/core/psychic/raise_conv") != 0) {
      purple_conversation_present(gconv);
    }
    if (purple_prefs_get_bool("/plugins/core/psychic/show_notice") != 0) {
/* This is a quote from Star Wars.  You should probably not
	 translate it literally.  If you can't find a fitting cultural
	 reference in your language, consider translating something
	 like this instead: "You feel a new message coming." */
      purple_conversation_write(gconv,0,((const char *)(dgettext("pidgin","You feel a disturbance in the force..."))),(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LOG | PURPLE_MESSAGE_ACTIVE_ONLY),time(0));
    }
/* Necessary because we may be creating a new conversation window. */
    purple_conv_im_set_typing_state(purple_conversation_get_im_data(gconv),PURPLE_TYPING);
  }
}

static PurplePluginPrefFrame *get_plugin_pref_frame(PurplePlugin *plugin)
{
  PurplePluginPrefFrame *frame;
  PurplePluginPref *pref;
  frame = purple_plugin_pref_frame_new();
  pref = purple_plugin_pref_new_with_name("/plugins/core/psychic/buddies_only");
  purple_plugin_pref_set_label(pref,((const char *)(dgettext("pidgin","Only enable for users on the buddy list"))));
  purple_plugin_pref_frame_add(frame,pref);
  pref = purple_plugin_pref_new_with_name("/plugins/core/psychic/activate_online");
  purple_plugin_pref_set_label(pref,((const char *)(dgettext("pidgin","Disable when away"))));
  purple_plugin_pref_frame_add(frame,pref);
  pref = purple_plugin_pref_new_with_name("/plugins/core/psychic/show_notice");
  purple_plugin_pref_set_label(pref,((const char *)(dgettext("pidgin","Display notification message in conversations"))));
  purple_plugin_pref_frame_add(frame,pref);
  pref = purple_plugin_pref_new_with_name("/plugins/core/psychic/raise_conv");
  purple_plugin_pref_set_label(pref,((const char *)(dgettext("pidgin","Raise psychic conversations"))));
  purple_plugin_pref_frame_add(frame,pref);
  return frame;
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  void *convs_handle;
  convs_handle = purple_conversations_get_handle();
  purple_signal_connect(convs_handle,"buddy-typing",plugin,((PurpleCallback )buddy_typing_cb),0);
  return (!0);
}
static PurplePluginUiInfo prefs_info = {(get_plugin_pref_frame), (0), ((PurplePluginPrefFrame *)((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* page_num (Reserved) */
/* frame (Reserved) */
/* padding */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("core-psychic"), ("Psychic Mode"), ("2.10.9"), ("Psychic mode for incoming conversation"), ("Causes conversation windows to appear as other users begin to message you.  This works for AIM, ICQ, XMPP, Sametime, and Yahoo!"), ("Christopher O\'Brien <siege@preoccupied.net>"), ("http://pidgin.im/"), (plugin_load), ((gboolean (*)(PurplePlugin *))((void *)0)), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), (&prefs_info), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/**< type */
/**< ui_requirement */
/**< flags */
/**< dependencies */
/**< priority */
/**< id */
/**< name */
/**< version */
/**< summary */
/**< description */
/**< author */
/**< homepage */
/**< load */
/**< unload */
/**< destroy */
/**< ui_info */
/**< extra_info */
/**< prefs_info */
/**< actions */
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
  purple_prefs_add_none("/plugins/core/psychic");
  purple_prefs_add_bool("/plugins/core/psychic/buddies_only",0);
  purple_prefs_add_bool("/plugins/core/psychic/show_notice",(!0));
  purple_prefs_add_bool("/plugins/core/psychic/activate_online",(!0));
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
