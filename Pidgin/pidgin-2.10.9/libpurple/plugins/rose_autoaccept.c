/*
 * Autoaccept - Auto-accept file transfers from selected users
 * Copyright (C) 2006
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02111-1301, USA.
 */
#include "internal.h"
#define PLUGIN_ID			"core-plugin_pack-autoaccept"
#define PLUGIN_NAME			N_("Autoaccept")
#define PLUGIN_STATIC_NAME	Autoaccept
#define PLUGIN_SUMMARY		N_("Auto-accept file transfer requests from selected users.")
#define PLUGIN_DESCRIPTION	N_("Auto-accept file transfer requests from selected users.")
#define PLUGIN_AUTHOR		"Sadrul H Chowdhury <sadrul@users.sourceforge.net>"
/* System headers */
#include <glib.h>
#include <glib/gstdio.h>
/* Purple headers */
#include <plugin.h>
#include <version.h>
#include <blist.h>
#include <conversation.h>
#include <ft.h>
#include <request.h>
#include <notify.h>
#include <util.h>
#define PREF_PREFIX		"/plugins/core/" PLUGIN_ID
#define PREF_PATH		PREF_PREFIX "/path"
#define PREF_STRANGER	PREF_PREFIX "/stranger"
#define PREF_NOTIFY		PREF_PREFIX "/notify"
#define PREF_NEWDIR     PREF_PREFIX "/newdir"
#define PREF_ESCAPE     PREF_PREFIX "/escape"
#define PREF_STRANGER_OLD PREF_PREFIX "/reject_stranger"
typedef enum  {FT_ASK,FT_ACCEPT,FT_REJECT}AutoAcceptSetting;

static gboolean ensure_path_exists(const char *dir)
{
  if (!(g_file_test(dir,G_FILE_TEST_IS_DIR) != 0)) {
    if (purple_build_dir(dir,256 | 128 | 64) != 0) 
      return 0;
  }
  return (!0);
}

static void auto_accept_complete_cb(PurpleXfer *xfer,PurpleXfer *my)
{
  if (((xfer == my) && (purple_prefs_get_bool("/plugins/core/core-plugin_pack-autoaccept/notify") != 0)) && !(purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,(xfer -> who),(xfer -> account)) != 0)) {
    char *message = g_strdup_printf(((const char *)(dgettext("pidgin","Autoaccepted file transfer of \"%s\" from \"%s\" completed."))),(xfer -> filename),(xfer -> who));
    purple_notify_message(0,PURPLE_NOTIFY_MSG_INFO,((const char *)(dgettext("pidgin","Autoaccept complete"))),message,0,0,0);
    g_free(message);
  }
}

static void file_recv_request_cb(PurpleXfer *xfer,gpointer handle)
{
  PurpleAccount *account;
  PurpleBlistNode *node;
  const char *pref;
  char *filename;
  char *dirname;
  int accept_setting;
  account = (xfer -> account);
  node = ((PurpleBlistNode *)(purple_find_buddy(account,(xfer -> who))));
/* If person is on buddy list, use the buddy setting; otherwise, use the
	   stranger setting. */
  if (node != 0) {
    node = purple_blist_node_get_parent(node);
    do {
      if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
      }
      else {
        g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_BLIST_NODE_IS_CONTACT(node)");
        return ;
      };
    }while (0);
    accept_setting = purple_blist_node_get_int(node,"autoaccept");
  }
  else {
    accept_setting = purple_prefs_get_int("/plugins/core/core-plugin_pack-autoaccept/stranger");
  }
{
    switch(accept_setting){
      case 0:
{
        break; 
      }
      case 1:
{
        pref = purple_prefs_get_string("/plugins/core/core-plugin_pack-autoaccept/path");
        if (ensure_path_exists(pref) != 0) {
          int count = 1;
          const char *escape;
          gchar **name_and_ext;
          const gchar *name;
          gchar *ext;
          if (purple_prefs_get_bool("/plugins/core/core-plugin_pack-autoaccept/newdir") != 0) 
            dirname = g_build_filename(pref,purple_normalize(account,(xfer -> who)),((void *)((void *)0)));
          else 
            dirname = g_build_filename(pref,((void *)((void *)0)));
          if (!(ensure_path_exists(dirname) != 0)) {
            g_free(dirname);
            break; 
          }
/* Escape filename (if escaping is turned on) */
          if (purple_prefs_get_bool("/plugins/core/core-plugin_pack-autoaccept/escape") != 0) {
            escape = purple_escape_filename((xfer -> filename));
          }
          else {
            escape = (xfer -> filename);
          }
          filename = g_build_filename(dirname,escape,((void *)((void *)0)));
/* Split at the first dot, to avoid uniquifying "foo.tar.gz" to "foo.tar-2.gz" */
          name_and_ext = g_strsplit(escape,".",2);
          name = name_and_ext[0];
          do {
            if (name != ((const gchar *)((void *)0))) {
            }
            else {
              g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
              return ;
            };
          }while (0);
          if (name_and_ext[1] != ((gchar *)((void *)0))) {
/* g_strsplit does not include the separator in each chunk. */
            ext = g_strdup_printf(".%s",name_and_ext[1]);
          }
          else {
            ext = g_strdup("");
          }
/* Make sure the file doesn't exist. Do we want some better checking than this? */
/* FIXME: There is a race here: if the newly uniquified file name gets created between
				 *        this g_file_test and the transfer starting, the file created in the meantime
				 *        will be clobbered. But it's not at all straightforward to fix.
				 */
          while(g_file_test(filename,G_FILE_TEST_EXISTS) != 0){
            char *file = g_strdup_printf("%s-%d%s",name,count++,ext);
            g_free(filename);
            filename = g_build_filename(dirname,file,((void *)((void *)0)));
            g_free(file);
          }
          purple_xfer_request_accepted(xfer,filename);
          g_strfreev(name_and_ext);
          g_free(ext);
          g_free(dirname);
          g_free(filename);
        }
        purple_signal_connect(purple_xfers_get_handle(),"file-recv-complete",handle,((PurpleCallback )auto_accept_complete_cb),xfer);
        break; 
      }
      case 2:
{
        xfer -> status = PURPLE_XFER_STATUS_CANCEL_LOCAL;
        break; 
      }
    }
  }
}

static void save_cb(PurpleBlistNode *node,int choice)
{
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) 
    node = purple_blist_node_get_parent(node);
  do {
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_BLIST_NODE_IS_CONTACT(node)");
      return ;
    };
  }while (0);
  purple_blist_node_set_int(node,"autoaccept",choice);
}

static void set_auto_accept_settings(PurpleBlistNode *node,gpointer plugin)
{
  char *message;
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) 
    node = purple_blist_node_get_parent(node);
  do {
    if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_BLIST_NODE_IS_CONTACT(node)");
      return ;
    };
  }while (0);
  message = g_strdup_printf(((const char *)(dgettext("pidgin","When a file-transfer request arrives from %s"))),purple_contact_get_alias(((PurpleContact *)node)));
  purple_request_choice(plugin,((const char *)(dgettext("pidgin","Set Autoaccept Setting"))),message,0,purple_blist_node_get_int(node,"autoaccept"),((const char *)(dgettext("pidgin","_Save"))),((GCallback )save_cb),((const char *)(dgettext("pidgin","_Cancel"))),0,0,0,0,node,((const char *)(dgettext("pidgin","Ask"))),FT_ASK,((const char *)(dgettext("pidgin","Auto Accept"))),FT_ACCEPT,((const char *)(dgettext("pidgin","Auto Reject"))),FT_REJECT,((void *)((void *)0)),purple_contact_get_alias(((PurpleContact *)node)),((void *)((void *)0)),((void *)((void *)0)));
  g_free(message);
}

static void context_menu(PurpleBlistNode *node,GList **menu,gpointer plugin)
{
  PurpleMenuAction *action;
  if ((!((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) && !((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE)) && !(((purple_blist_node_get_flags(node)) & PURPLE_BLIST_NODE_FLAG_NO_SAVE) != 0U)) 
    return ;
  action = purple_menu_action_new(((const char *)(dgettext("pidgin","Autoaccept File Transfers..."))),((PurpleCallback )set_auto_accept_settings),plugin,0);
   *menu = g_list_prepend( *menu,action);
}

static gboolean plugin_load(PurplePlugin *plugin)
{
/* migrate the old pref (we should only care if the plugin is actually *used*) */
/*
	 * TODO: We should eventually call purple_prefs_remove(PREFS_STRANGER_OLD)
	 *       to clean up after ourselves, but we don't want to do it yet
	 *       so that we don't break users who share a .purple directory
	 *       between old libpurple clients and new libpurple clients.
	 *                                             --Mark Doliner, 2011-01-03
	 */
  if (!(purple_prefs_exists("/plugins/core/core-plugin_pack-autoaccept/stranger") != 0)) {
    if (purple_prefs_get_bool("/plugins/core/core-plugin_pack-autoaccept/reject_stranger") != 0) 
      purple_prefs_add_int("/plugins/core/core-plugin_pack-autoaccept/stranger",FT_REJECT);
    else 
      purple_prefs_set_int("/plugins/core/core-plugin_pack-autoaccept/stranger",FT_ASK);
  }
  purple_signal_connect(purple_xfers_get_handle(),"file-recv-request",plugin,((PurpleCallback )file_recv_request_cb),plugin);
  purple_signal_connect(purple_blist_get_handle(),"blist-node-extended-menu",plugin,((PurpleCallback )context_menu),plugin);
  return (!0);
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  return (!0);
}

static PurplePluginPrefFrame *get_plugin_pref_frame(PurplePlugin *plugin)
{
  PurplePluginPrefFrame *frame;
  PurplePluginPref *pref;
  frame = purple_plugin_pref_frame_new();
/* XXX: Is there a better way than this? There really should be. */
  pref = purple_plugin_pref_new_with_name_and_label("/plugins/core/core-plugin_pack-autoaccept/path",((const char *)(dgettext("pidgin","Path to save the files in\n(Please provide the full path)"))));
  purple_plugin_pref_frame_add(frame,pref);
  pref = purple_plugin_pref_new_with_name_and_label("/plugins/core/core-plugin_pack-autoaccept/stranger",((const char *)(dgettext("pidgin","When a file-transfer request arrives from a user who is\n*not* on your buddy list:"))));
  purple_plugin_pref_set_type(pref,PURPLE_PLUGIN_PREF_CHOICE);
  purple_plugin_pref_add_choice(pref,((const char *)(dgettext("pidgin","Ask"))),0);
  purple_plugin_pref_add_choice(pref,((const char *)(dgettext("pidgin","Auto Accept"))),((gpointer )((gpointer )((glong )FT_ACCEPT))));
  purple_plugin_pref_add_choice(pref,((const char *)(dgettext("pidgin","Auto Reject"))),((gpointer )((gpointer )((glong )FT_REJECT))));
  purple_plugin_pref_frame_add(frame,pref);
  pref = purple_plugin_pref_new_with_name_and_label("/plugins/core/core-plugin_pack-autoaccept/notify",((const char *)(dgettext("pidgin","Notify with a popup when an autoaccepted file transfer is complete\n(only when there\'s no conversation with the sender)"))));
  purple_plugin_pref_frame_add(frame,pref);
  pref = purple_plugin_pref_new_with_name_and_label("/plugins/core/core-plugin_pack-autoaccept/newdir",((const char *)(dgettext("pidgin","Create a new directory for each user"))));
  purple_plugin_pref_frame_add(frame,pref);
  pref = purple_plugin_pref_new_with_name_and_label("/plugins/core/core-plugin_pack-autoaccept/escape",((const char *)(dgettext("pidgin","Escape the filenames"))));
  purple_plugin_pref_frame_add(frame,pref);
  return frame;
}
static PurplePluginUiInfo prefs_info = {(get_plugin_pref_frame), (0), ((PurplePluginPrefFrame *)((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* padding */
};
static PurplePluginInfo info = {(5), (2), (10), (PURPLE_PLUGIN_STANDARD), ((char *)((void *)0)), (0), ((GList *)((void *)0)), (0), ("core-plugin_pack-autoaccept"), ("Autoaccept"), ("2.10.9"), ("Auto-accept file transfer requests from selected users."), ("Auto-accept file transfer requests from selected users."), ("Sadrul H Chowdhury <sadrul@users.sourceforge.net>"), ("http://pidgin.im/"), (plugin_load), (plugin_unload), ((void (*)(PurplePlugin *))((void *)0)), ((void *)((void *)0)), ((void *)((void *)0)), (&prefs_info), ((GList *(*)(PurplePlugin *, gpointer ))((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0)), ((void (*)())((void *)0))
/* Magic				*/
/* Purple Major Version	*/
/* Purple Minor Version	*/
/* plugin type			*/
/* ui requirement		*/
/* flags				*/
/* dependencies			*/
/* priority				*/
/* plugin id			*/
/* name					*/
/* version				*/
/* summary				*/
/* description			*/
/* author				*/
/* website				*/
/* load					*/
/* unload				*/
/* destroy				*/
/* ui_info				*/
/* extra_info			*/
/* prefs_info			*/
/* actions				*/
/* padding */
};

static void init_plugin(PurplePlugin *plugin)
{
  char *dirname;
  dirname = g_build_filename(purple_user_dir(),"autoaccept",((void *)((void *)0)));
  purple_prefs_add_none("/plugins/core/core-plugin_pack-autoaccept");
  purple_prefs_add_string("/plugins/core/core-plugin_pack-autoaccept/path",dirname);
  purple_prefs_add_bool("/plugins/core/core-plugin_pack-autoaccept/notify",(!0));
  purple_prefs_add_bool("/plugins/core/core-plugin_pack-autoaccept/newdir",(!0));
  purple_prefs_add_bool("/plugins/core/core-plugin_pack-autoaccept/escape",(!0));
  g_free(dirname);
}
gboolean purple_init_plugin(PurplePlugin *plugin);

gboolean purple_init_plugin(PurplePlugin *plugin)
{
  plugin -> info = &info;
  init_plugin(plugin);
  return purple_plugin_register(plugin);
}
