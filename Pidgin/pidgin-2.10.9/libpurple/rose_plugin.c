/*
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#define _PURPLE_PLUGIN_C_
#include "internal.h"
#include "accountopt.h"
#include "core.h"
#include "dbus-maybe.h"
#include "debug.h"
#include "notify.h"
#include "prefs.h"
#include "prpl.h"
#include "request.h"
#include "signals.h"
#include "util.h"
#include "valgrind.h"
#include "version.h"
typedef struct __unnamed_class___F0_L39_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L84R__Pe___variable_name_unknown_scope_and_name__scope__commands__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__command_count {
GHashTable *commands;
size_t command_count;}PurplePluginIpcInfo;
typedef struct __unnamed_class___F0_L46_C9_unknown_scope_and_name_variable_declaration__variable_type_L261R_variable_name_unknown_scope_and_name__scope__func__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L262R_variable_name_unknown_scope_and_name__scope__marshal__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__num_params__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb____Pb__PurpleValueL462R__typedef_declaration__Pe____Pe___variable_name_unknown_scope_and_name__scope__params__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__PurpleValueL462R__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__ret_value {
PurpleCallback func;
PurpleSignalMarshalFunc marshal;
int num_params;
PurpleValue **params;
PurpleValue *ret_value;}PurplePluginIpcCommand;
static GList *search_paths = (GList *)((void *)0);
static GList *plugins = (GList *)((void *)0);
static GList *loaded_plugins = (GList *)((void *)0);
static GList *protocol_plugins = (GList *)((void *)0);
#ifdef PURPLE_PLUGINS
static GList *load_queue = (GList *)((void *)0);
static GList *plugin_loaders = (GList *)((void *)0);
static GList *plugins_to_disable = (GList *)((void *)0);
#endif
static void (*probe_cb)(void *) = (void (*)(void *))((void *)0);
static void *probe_cb_data = (void *)((void *)0);
static void (*load_cb)(PurplePlugin *, void *) = (void (*)(PurplePlugin *, void *))((void *)0);
static void *load_cb_data = (void *)((void *)0);
static void (*unload_cb)(PurplePlugin *, void *) = (void (*)(PurplePlugin *, void *))((void *)0);
static void *unload_cb_data = (void *)((void *)0);
#ifdef PURPLE_PLUGINS

static gboolean has_file_extension(const char *filename,const char *ext)
{
  int len;
  int extlen;
  if (((filename == ((const char *)((void *)0))) || (( *filename) == 0)) || (ext == ((const char *)((void *)0)))) 
    return 0;
  extlen = (strlen(ext));
  len = (strlen(filename) - extlen);
  if (len < 0) 
    return 0;
  return strncmp((filename + len),ext,extlen) == 0;
}

static gboolean is_native(const char *filename)
{
  const char *last_period;
  last_period = (strrchr(filename,'.'));
  if (last_period == ((const char *)((void *)0))) 
    return 0;
  return !(((strcmp(last_period,".dll") & strcmp(last_period,".sl")) & strcmp(last_period,".so")) != 0);
}

static char *purple_plugin_get_basename(const char *filename)
{
  const char *basename;
  const char *last_period;
  basename = (strrchr(filename,'/'));
  if (basename != ((const char *)((void *)0))) 
    basename++;
  else 
    basename = filename;
  if ((is_native(basename) != 0) && ((last_period = (strrchr(basename,'.'))) != ((const char *)((void *)0)))) 
    return g_strndup(basename,(last_period - basename));
  return g_strdup(basename);
}

static gboolean loader_supports_file(PurplePlugin *loader,const char *filename)
{
  GList *exts;
  for (exts = ( *((PurplePluginLoaderInfo *)( *(loader -> info)).extra_info)).exts; exts != ((GList *)((void *)0)); exts = (exts -> next)) {
    if (has_file_extension(filename,((char *)(exts -> data))) != 0) {
      return (!0);
    }
  }
  return 0;
}

static PurplePlugin *find_loader_for_plugin(const PurplePlugin *plugin)
{
  PurplePlugin *loader;
  GList *l;
  if ((plugin -> path) == ((char *)((void *)0))) 
    return 0;
  for (l = purple_plugins_get_loaded(); l != ((GList *)((void *)0)); l = (l -> next)) {
    loader = (l -> data);
    if ((( *(loader -> info)).type == PURPLE_PLUGIN_LOADER) && (loader_supports_file(loader,(plugin -> path)) != 0)) {
      return loader;
    }
    loader = ((PurplePlugin *)((void *)0));
  }
  return 0;
}
#endif /* PURPLE_PLUGINS */
/**
 * Negative if a before b, 0 if equal, positive if a after b.
 */

static gint compare_prpl(PurplePlugin *a,PurplePlugin *b)
{
  if (( *(a -> info)).type == PURPLE_PLUGIN_PROTOCOL) {
    if (( *(b -> info)).type == PURPLE_PLUGIN_PROTOCOL) 
      return strcmp(( *(a -> info)).name,( *(b -> info)).name);
    else 
      return (-1);
  }
  else {
    if (( *(b -> info)).type == PURPLE_PLUGIN_PROTOCOL) 
      return 1;
    else 
      return 0;
  }
}

PurplePlugin *purple_plugin_new(gboolean native,const char *path)
{
  PurplePlugin *plugin;
  plugin = ((PurplePlugin *)(g_malloc0_n(1,(sizeof(PurplePlugin )))));
  plugin -> native_plugin = native;
  plugin -> path = g_strdup(path);
{
    PurplePlugin *typed_ptr = plugin;
    purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurplePlugin);
  };
  return plugin;
}

PurplePlugin *purple_plugin_probe(const char *filename)
{
#ifdef PURPLE_PLUGINS
  PurplePlugin *plugin = (PurplePlugin *)((void *)0);
  PurplePlugin *loader;
  gpointer unpunned;
  gchar *basename = (gchar *)((void *)0);
  gboolean (*purple_init_plugin)(PurplePlugin *);
  purple_debug_misc("plugins","probing %s\n",filename);
  do {
    if (filename != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"filename != NULL");
      return 0;
    };
  }while (0);
  if (!(g_file_test(filename,G_FILE_TEST_EXISTS) != 0)) 
    return 0;
/* If this plugin has already been probed then exit */
  basename = purple_plugin_get_basename(filename);
  plugin = purple_plugins_find_with_basename(basename);
  g_free(basename);
  if (plugin != ((PurplePlugin *)((void *)0))) {
    if (purple_strequal(filename,(plugin -> path)) != 0) 
      return plugin;
    else if (!(purple_plugin_is_unloadable(plugin) != 0)) {
      purple_debug_warning("plugins","Not loading %s. Another plugin with the same name (%s) has already been loaded.\n",filename,(plugin -> path));
      return plugin;
    }
    else {
/* The old plugin was a different file and it was unloadable.
			 * There's no guarantee that this new file with the same name
			 * will be loadable, but unless it fails in one of the silent
			 * ways and the first one didn't, it's not any worse.  The user
			 * will still see a greyed-out plugin, which is what we want. */
      purple_plugin_destroy(plugin);
    }
  }
  plugin = purple_plugin_new(has_file_extension(filename,"so"),filename);
  if ((plugin -> native_plugin) != 0) {
    const char *error;
#ifdef _WIN32
/* Suppress error popups for failing to load plugins */
#endif
/*
		 * We pass G_MODULE_BIND_LOCAL here to prevent symbols from
		 * plugins being added to the global name space.
		 *
		 * G_MODULE_BIND_LOCAL was added in glib 2.3.3.
		 */
    plugin -> handle = (g_module_open(filename,G_MODULE_BIND_LOCAL));
    if ((plugin -> handle) == ((void *)((void *)0))) {
      const char *error = g_module_error();
      if ((error != ((const char *)((void *)0))) && (purple_str_has_prefix(error,filename) != 0)) {
        error = (error + strlen(filename));
/* These are just so we don't crash.  If we
				 * got this far, they should always be true. */
        if (( *error) == ':') 
          error++;
        if (( *error) == 32) 
          error++;
      }
      if ((error == ((const char *)((void *)0))) || !(( *error) != 0)) {
        plugin -> error = g_strdup(((const char *)(dgettext("pidgin","Unknown error"))));
        purple_debug_error("plugins","%s is not loadable: Unknown error\n",(plugin -> path));
      }
      else {
        plugin -> error = g_strdup(error);
        purple_debug_error("plugins","%s is not loadable: %s\n",(plugin -> path),(plugin -> error));
      }
      plugin -> handle = (g_module_open(filename,(G_MODULE_BIND_LAZY | G_MODULE_BIND_LOCAL)));
      if ((plugin -> handle) == ((void *)((void *)0))) {
#ifdef _WIN32
/* Restore the original error mode */
#endif
        purple_plugin_destroy(plugin);
        return 0;
      }
      else {
/* We were able to load the plugin with lazy symbol binding.
				 * This means we're missing some symbol.  Mark it as
				 * unloadable and keep going so we get the info to display
				 * to the user so they know to rebuild this plugin. */
        plugin -> unloadable = (!0);
      }
    }
    if (!(g_module_symbol((plugin -> handle),"purple_init_plugin",&unpunned) != 0)) {
      purple_debug_error("plugins","%s is not usable because the \'purple_init_plugin\' symbol could not be found.  Does the plugin call the PURPLE_INIT_PLUGIN() macro\?\n",(plugin -> path));
      g_module_close((plugin -> handle));
      error = g_module_error();
      if (error != ((const char *)((void *)0))) 
        purple_debug_error("plugins","Error closing module %s: %s\n",(plugin -> path),error);
      plugin -> handle = ((void *)((void *)0));
#ifdef _WIN32
/* Restore the original error mode */
#endif
      purple_plugin_destroy(plugin);
      return 0;
    }
    purple_init_plugin = unpunned;
#ifdef _WIN32
/* Restore the original error mode */
#endif
  }
  else {
    loader = find_loader_for_plugin(plugin);
    if (loader == ((PurplePlugin *)((void *)0))) {
      purple_plugin_destroy(plugin);
      return 0;
    }
    purple_init_plugin = ( *((PurplePluginLoaderInfo *)( *(loader -> info)).extra_info)).probe;
  }
  if (!(( *purple_init_plugin)(plugin) != 0) || ((plugin -> info) == ((PurplePluginInfo *)((void *)0)))) {
    purple_plugin_destroy(plugin);
    return 0;
  }
  else if ((( *(plugin -> info)).ui_requirement != 0) && !(purple_strequal(( *(plugin -> info)).ui_requirement,purple_core_get_ui()) != 0)) {
    plugin -> error = g_strdup_printf(((const char *)(dgettext("pidgin","You are using %s, but this plugin requires %s."))),purple_core_get_ui(),( *(plugin -> info)).ui_requirement);
    purple_debug_error("plugins","%s is not loadable: The UI requirement is not met. (%s)\n",(plugin -> path),(plugin -> error));
    plugin -> unloadable = (!0);
    return plugin;
  }
/*
	 * Check to make sure a plugin has defined an id.
	 * Not having this check caused purple_plugin_unload to
	 * enter an infinite loop in certain situations by passing
	 * purple_find_plugin_by_id a NULL value. -- ecoffey
	 */
  if ((( *(plugin -> info)).id == ((char *)((void *)0))) || (( *( *(plugin -> info)).id) == 0)) {
    plugin -> error = g_strdup(((const char *)(dgettext("pidgin","This plugin has not defined an ID."))));
    purple_debug_error("plugins","%s is not loadable: info->id is not defined.\n",(plugin -> path));
    plugin -> unloadable = (!0);
    return plugin;
  }
/* Really old plugins. */
  if (( *(plugin -> info)).magic != 5) {
    if ((( *(plugin -> info)).magic >= 2) && (( *(plugin -> info)).magic <= 4)) {
      struct _PurplePluginInfo2 {
      unsigned int api_version;
      PurplePluginType type;
      char *ui_requirement;
      unsigned long flags;
      GList *dependencies;
      PurplePluginPriority priority;
      char *id;
      char *name;
      char *version;
      char *summary;
      char *description;
      char *author;
      char *homepage;
      gboolean (*load)(PurplePlugin *);
      gboolean (*unload)(PurplePlugin *);
      void (*destroy)(PurplePlugin *);
      void *ui_info;
      void *extra_info;
      PurplePluginUiInfo *prefs_info;
      GList *(*actions)(PurplePlugin *, gpointer );}*info2 = (struct _PurplePluginInfo2 *)(plugin -> info);
/* This leaks... but only for ancient plugins, so deal with it. */
      plugin -> info = ((PurplePluginInfo *)(g_malloc0_n(1,(sizeof(PurplePluginInfo )))));
/* We don't really need all these to display the plugin info, but
			 * I'm copying them all for good measure. */
      ( *(plugin -> info)).magic = (info2 -> api_version);
      ( *(plugin -> info)).type = (info2 -> type);
      ( *(plugin -> info)).ui_requirement = (info2 -> ui_requirement);
      ( *(plugin -> info)).flags = (info2 -> flags);
      ( *(plugin -> info)).dependencies = (info2 -> dependencies);
      ( *(plugin -> info)).id = (info2 -> id);
      ( *(plugin -> info)).name = (info2 -> name);
      ( *(plugin -> info)).version = (info2 -> version);
      ( *(plugin -> info)).summary = (info2 -> summary);
      ( *(plugin -> info)).description = (info2 -> description);
      ( *(plugin -> info)).author = (info2 -> author);
      ( *(plugin -> info)).homepage = (info2 -> homepage);
      ( *(plugin -> info)).load = (info2 -> load);
      ( *(plugin -> info)).unload = (info2 -> unload);
      ( *(plugin -> info)).destroy = (info2 -> destroy);
      ( *(plugin -> info)).ui_info = (info2 -> ui_info);
      ( *(plugin -> info)).extra_info = (info2 -> extra_info);
      if ((info2 -> api_version) >= 3) 
        ( *(plugin -> info)).prefs_info = (info2 -> prefs_info);
      if ((info2 -> api_version) >= 4) 
        ( *(plugin -> info)).actions = (info2 -> actions);
      plugin -> error = g_strdup_printf(((const char *)(dgettext("pidgin","Plugin magic mismatch %d (need %d)"))),( *(plugin -> info)).magic,5);
      purple_debug_error("plugins","%s is not loadable: Plugin magic mismatch %d (need %d)\n",(plugin -> path),( *(plugin -> info)).magic,5);
      plugin -> unloadable = (!0);
      return plugin;
    }
    purple_debug_error("plugins","%s is not loadable: Plugin magic mismatch %d (need %d)\n",(plugin -> path),( *(plugin -> info)).magic,5);
    purple_plugin_destroy(plugin);
    return 0;
  }
  if ((( *(plugin -> info)).major_version != 2) || (( *(plugin -> info)).minor_version > 10)) {
    plugin -> error = g_strdup_printf(((const char *)(dgettext("pidgin","ABI version mismatch %d.%d.x (need %d.%d.x)"))),( *(plugin -> info)).major_version,( *(plugin -> info)).minor_version,2,10);
    purple_debug_error("plugins","%s is not loadable: ABI version mismatch %d.%d.x (need %d.%d.x)\n",(plugin -> path),( *(plugin -> info)).major_version,( *(plugin -> info)).minor_version,2,10);
    plugin -> unloadable = (!0);
    return plugin;
  }
  if (( *(plugin -> info)).type == PURPLE_PLUGIN_PROTOCOL) {
/* If plugin is a PRPL, make sure it implements the required functions */
    if (((( *((PurplePluginProtocolInfo *)( *(plugin -> info)).extra_info)).list_icon == ((const char *(*)(PurpleAccount *, PurpleBuddy *))((void *)0))) || (( *((PurplePluginProtocolInfo *)( *(plugin -> info)).extra_info)).login == ((void (*)(PurpleAccount *))((void *)0)))) || (( *((PurplePluginProtocolInfo *)( *(plugin -> info)).extra_info)).close == ((void (*)(PurpleConnection *))((void *)0)))) {
      plugin -> error = g_strdup(((const char *)(dgettext("pidgin","Plugin does not implement all required functions (list_icon, login and close)"))));
      purple_debug_error("plugins","%s is not loadable: %s\n",(plugin -> path),(plugin -> error));
      plugin -> unloadable = (!0);
      return plugin;
    }
/* For debugging, let's warn about prpl prefs. */
    if (( *(plugin -> info)).prefs_info != ((PurplePluginUiInfo *)((void *)0))) {
      purple_debug_error("plugins","%s has a prefs_info, but is a prpl. This is no longer supported.\n",(plugin -> path));
    }
  }
  return plugin;
#else
#endif /* !PURPLE_PLUGINS */
}
#ifdef PURPLE_PLUGINS

static gint compare_plugins(gconstpointer a,gconstpointer b)
{
  const PurplePlugin *plugina = a;
  const PurplePlugin *pluginb = b;
  return strcmp(( *(plugina -> info)).name,( *(pluginb -> info)).name);
}
#endif /* PURPLE_PLUGINS */

gboolean purple_plugin_load(PurplePlugin *plugin)
{
#ifdef PURPLE_PLUGINS
  GList *dep_list = (GList *)((void *)0);
  GList *l;
  do {
    if (plugin != ((PurplePlugin *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin != NULL");
      return 0;
    };
  }while (0);
  if (purple_plugin_is_loaded(plugin) != 0) 
    return (!0);
  if (purple_plugin_is_unloadable(plugin) != 0) 
    return 0;
  do {
    if ((plugin -> error) == ((char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin->error == NULL");
      return 0;
    };
  }while (0);
/*
	 * Go through the list of the plugin's dependencies.
	 *
	 * First pass: Make sure all the plugins needed are probed.
	 */
  for (l = ( *(plugin -> info)).dependencies; l != ((GList *)((void *)0)); l = (l -> next)) {
    const char *dep_name = (const char *)(l -> data);
    PurplePlugin *dep_plugin;
    dep_plugin = purple_plugins_find_with_id(dep_name);
    if (dep_plugin == ((PurplePlugin *)((void *)0))) {
      char *tmp;
      tmp = g_strdup_printf(((const char *)(dgettext("pidgin","The required plugin %s was not found. Please install this plugin and try again."))),dep_name);
      purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Unable to load the plugin"))),tmp,0,0);
      g_free(tmp);
      g_list_free(dep_list);
      return 0;
    }
    dep_list = g_list_append(dep_list,dep_plugin);
  }
/* Second pass: load all the required plugins. */
  for (l = dep_list; l != ((GList *)((void *)0)); l = (l -> next)) {
    PurplePlugin *dep_plugin = (PurplePlugin *)(l -> data);
    if (!(purple_plugin_is_loaded(dep_plugin) != 0)) {
      if (!(purple_plugin_load(dep_plugin) != 0)) {
        char *tmp;
        tmp = g_strdup_printf(((const char *)(dgettext("pidgin","The required plugin %s was unable to load."))),( *(plugin -> info)).name);
        purple_notify_message(0,PURPLE_NOTIFY_MSG_ERROR,0,((const char *)(dgettext("pidgin","Unable to load your plugin."))),tmp,0,0);
        g_free(tmp);
        g_list_free(dep_list);
        return 0;
      }
    }
  }
/* Third pass: note that other plugins are dependencies of this plugin.
	 * This is done separately in case we had to bail out earlier. */
  for (l = dep_list; l != ((GList *)((void *)0)); l = (l -> next)) {
    PurplePlugin *dep_plugin = (PurplePlugin *)(l -> data);
    dep_plugin -> dependent_plugins = g_list_prepend((dep_plugin -> dependent_plugins),( *(plugin -> info)).id);
  }
  g_list_free(dep_list);
  if ((plugin -> native_plugin) != 0) {
    if ((( *(plugin -> info)).load != ((gboolean (*)(PurplePlugin *))((void *)0))) && !(( *( *(plugin -> info)).load)(plugin) != 0)) 
      return 0;
  }
  else {
    PurplePlugin *loader;
    PurplePluginLoaderInfo *loader_info;
    loader = find_loader_for_plugin(plugin);
    if (loader == ((PurplePlugin *)((void *)0))) 
      return 0;
    loader_info = ((PurplePluginLoaderInfo *)( *(loader -> info)).extra_info);
    if ((loader_info -> load) != ((gboolean (*)(PurplePlugin *))((void *)0))) {
      if (!(( *(loader_info -> load))(plugin) != 0)) 
        return 0;
    }
  }
  loaded_plugins = g_list_insert_sorted(loaded_plugins,plugin,compare_plugins);
  plugin -> loaded = (!0);
  if (load_cb != ((void (*)(PurplePlugin *, void *))((void *)0))) 
    ( *load_cb)(plugin,load_cb_data);
  purple_signal_emit(purple_plugins_get_handle(),"plugin-load",plugin);
  return (!0);
#else
#endif /* !PURPLE_PLUGINS */
}

gboolean purple_plugin_unload(PurplePlugin *plugin)
{
#ifdef PURPLE_PLUGINS
  GList *l;
  GList *ll;
  do {
    if (plugin != ((PurplePlugin *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin != NULL");
      return 0;
    };
  }while (0);
  do {
    if (purple_plugin_is_loaded(plugin) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_plugin_is_loaded(plugin)");
      return 0;
    };
  }while (0);
  purple_debug_info("plugins","Unloading plugin %s\n",( *(plugin -> info)).name);
/* Unload all plugins that depend on this plugin. */
  for (l = (plugin -> dependent_plugins); l != ((GList *)((void *)0)); l = ll) {
    const char *dep_name = (const char *)(l -> data);
    PurplePlugin *dep_plugin;
/* Store a pointer to the next element in the list.
		 * This is because we'll be modifying this list in the loop. */
    ll = (l -> next);
    dep_plugin = purple_plugins_find_with_id(dep_name);
    if ((dep_plugin != ((PurplePlugin *)((void *)0))) && (purple_plugin_is_loaded(dep_plugin) != 0)) {
      if (!(purple_plugin_unload(dep_plugin) != 0)) {
        g_free((plugin -> error));
        plugin -> error = g_strdup_printf(((const char *)(dgettext("pidgin","%s requires %s, but it failed to unload."))),((const char *)(dgettext("pidgin",( *(plugin -> info)).name))),((const char *)(dgettext("pidgin",( *(dep_plugin -> info)).name))));
        return 0;
      }
      else {
#if 0
/* This isn't necessary. This has already been done when unloading dep_plugin. */
#endif
      }
    }
  }
/* Remove this plugin from each dependency's dependent_plugins list. */
  for (l = ( *(plugin -> info)).dependencies; l != ((GList *)((void *)0)); l = (l -> next)) {
    const char *dep_name = (const char *)(l -> data);
    PurplePlugin *dependency;
    dependency = purple_plugins_find_with_id(dep_name);
    if (dependency != ((PurplePlugin *)((void *)0))) 
      dependency -> dependent_plugins = g_list_remove((dependency -> dependent_plugins),( *(plugin -> info)).id);
    else 
      purple_debug_error("plugins","Unable to remove from dependency list for %s\n",dep_name);
  }
  if ((plugin -> native_plugin) != 0) {
    if ((( *(plugin -> info)).unload != 0) && !(( *( *(plugin -> info)).unload)(plugin) != 0)) 
      return 0;
    if (( *(plugin -> info)).type == PURPLE_PLUGIN_PROTOCOL) {
      PurplePluginProtocolInfo *prpl_info;
      GList *l;
      prpl_info = ((PurplePluginProtocolInfo *)( *(plugin -> info)).extra_info);
      for (l = (prpl_info -> user_splits); l != ((GList *)((void *)0)); l = (l -> next)) 
        purple_account_user_split_destroy((l -> data));
      for (l = (prpl_info -> protocol_options); l != ((GList *)((void *)0)); l = (l -> next)) 
        purple_account_option_destroy((l -> data));
      if ((prpl_info -> user_splits) != ((GList *)((void *)0))) {
        g_list_free((prpl_info -> user_splits));
        prpl_info -> user_splits = ((GList *)((void *)0));
      }
      if ((prpl_info -> protocol_options) != ((GList *)((void *)0))) {
        g_list_free((prpl_info -> protocol_options));
        prpl_info -> protocol_options = ((GList *)((void *)0));
      }
    }
  }
  else {
    PurplePlugin *loader;
    PurplePluginLoaderInfo *loader_info;
    loader = find_loader_for_plugin(plugin);
    if (loader == ((PurplePlugin *)((void *)0))) 
      return 0;
    loader_info = ((PurplePluginLoaderInfo *)( *(loader -> info)).extra_info);
    if (((loader_info -> unload) != 0) && !(( *(loader_info -> unload))(plugin) != 0)) 
      return 0;
  }
/* cancel any pending dialogs the plugin has */
  purple_request_close_with_handle(plugin);
  purple_notify_close_with_handle(plugin);
  purple_signals_disconnect_by_handle(plugin);
  purple_plugin_ipc_unregister_all(plugin);
  loaded_plugins = g_list_remove(loaded_plugins,plugin);
  if (((plugin -> info) != ((PurplePluginInfo *)((void *)0))) && (( *(plugin -> info)).type == PURPLE_PLUGIN_PROTOCOL)) 
    protocol_plugins = g_list_remove(protocol_plugins,plugin);
  plugins_to_disable = g_list_remove(plugins_to_disable,plugin);
  plugin -> loaded = 0;
/* We wouldn't be anywhere near here if the plugin wasn't loaded, so
	 * if plugin->error is set at all, it had to be from a previous
	 * unload failure.  It's obviously okay now.
	 */
  g_free((plugin -> error));
  plugin -> error = ((char *)((void *)0));
  if (unload_cb != ((void (*)(PurplePlugin *, void *))((void *)0))) 
    ( *unload_cb)(plugin,unload_cb_data);
  purple_signal_emit(purple_plugins_get_handle(),"plugin-unload",plugin);
  purple_prefs_disconnect_by_handle(plugin);
  return (!0);
#else
#endif /* PURPLE_PLUGINS */
}

void purple_plugin_disable(PurplePlugin *plugin)
{
#ifdef PURPLE_PLUGINS
  do {
    if (plugin != ((PurplePlugin *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin != NULL");
      return ;
    };
  }while (0);
  if (!(g_list_find(plugins_to_disable,plugin) != 0)) 
    plugins_to_disable = g_list_prepend(plugins_to_disable,plugin);
#endif
}

gboolean purple_plugin_reload(PurplePlugin *plugin)
{
#ifdef PURPLE_PLUGINS
  do {
    if (plugin != ((PurplePlugin *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin != NULL");
      return 0;
    };
  }while (0);
  do {
    if (purple_plugin_is_loaded(plugin) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_plugin_is_loaded(plugin)");
      return 0;
    };
  }while (0);
  if (!(purple_plugin_unload(plugin) != 0)) 
    return 0;
  if (!(purple_plugin_load(plugin) != 0)) 
    return 0;
  return (!0);
#else
#endif /* !PURPLE_PLUGINS */
}

void purple_plugin_destroy(PurplePlugin *plugin)
{
#ifdef PURPLE_PLUGINS
  do {
    if (plugin != ((PurplePlugin *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin != NULL");
      return ;
    };
  }while (0);
  if (purple_plugin_is_loaded(plugin) != 0) 
    purple_plugin_unload(plugin);
  plugins = g_list_remove(plugins,plugin);
  if (load_queue != ((GList *)((void *)0))) 
    load_queue = g_list_remove(load_queue,plugin);
/* true, this may leak a little memory if there is a major version
	 * mismatch, but it's a lot better than trying to free something
	 * we shouldn't, and crashing while trying to load an old plugin */
  if ((((plugin -> info) == ((PurplePluginInfo *)((void *)0))) || (( *(plugin -> info)).magic != 5)) || (( *(plugin -> info)).major_version != 2)) {
    if ((plugin -> handle) != 0) 
      g_module_close((plugin -> handle));
    g_free((plugin -> path));
    g_free((plugin -> error));
    purple_dbus_unregister_pointer(plugin);
    g_free(plugin);
    return ;
  }
  if ((plugin -> info) != ((PurplePluginInfo *)((void *)0))) 
    g_list_free(( *(plugin -> info)).dependencies);
  if ((plugin -> native_plugin) != 0) {
    if (((plugin -> info) != ((PurplePluginInfo *)((void *)0))) && (( *(plugin -> info)).type == PURPLE_PLUGIN_LOADER)) {
      PurplePluginLoaderInfo *loader_info;
      GList *exts;
      GList *l;
      GList *next_l;
      PurplePlugin *p2;
      loader_info = ((PurplePluginLoaderInfo *)( *(plugin -> info)).extra_info);
      if ((loader_info != ((PurplePluginLoaderInfo *)((void *)0))) && ((loader_info -> exts) != ((GList *)((void *)0)))) {
        for (exts = ( *((PurplePluginLoaderInfo *)( *(plugin -> info)).extra_info)).exts; exts != ((GList *)((void *)0)); exts = (exts -> next)) {
          for (l = purple_plugins_get_all(); l != ((GList *)((void *)0)); l = next_l) {
            next_l = (l -> next);
            p2 = (l -> data);
            if (((p2 -> path) != ((char *)((void *)0))) && (has_file_extension((p2 -> path),(exts -> data)) != 0)) {
              purple_plugin_destroy(p2);
            }
          }
        }
        g_list_free((loader_info -> exts));
        loader_info -> exts = ((GList *)((void *)0));
      }
      plugin_loaders = g_list_remove(plugin_loaders,plugin);
    }
    if (((plugin -> info) != ((PurplePluginInfo *)((void *)0))) && (( *(plugin -> info)).destroy != ((void (*)(PurplePlugin *))((void *)0)))) 
      ( *( *(plugin -> info)).destroy)(plugin);
/*
		 * I find it extremely useful to do this when using valgrind, as
		 * it keeps all the plugins open, meaning that valgrind is able to
		 * resolve symbol names in leak traces from plugins.
		 */
    if (!(g_getenv("PURPLE_LEAKCHECK_HELP") != 0) && !((({
      unsigned int _qzz_res;
{
        volatile unsigned long long _zzq_args[6UL];
        volatile unsigned long long _zzq_result;
        _zzq_args[0] = ((unsigned long long )VG_USERREQ__RUNNING_ON_VALGRIND);
        _zzq_args[1] = ((unsigned long long )0);
        _zzq_args[2] = ((unsigned long long )0);
        _zzq_args[3] = ((unsigned long long )0);
        _zzq_args[4] = ((unsigned long long )0);
        _zzq_args[5] = ((unsigned long long )0);
        asm volatile ("rolq $3,  %%rdi ; rolq $13, %%rdi\\n\\trolq $61, %%rdi ; rolq $51, %%rdi\\n\\txchgq %%rbx,%%rbx" : "=d" (_zzq_result) : "a" ((_zzq_args + 0)), "0" (0) : "memory");
        _qzz_res = _zzq_result;
      };
      _qzz_res;
    })) != 0U)) 
{
      if ((plugin -> handle) != ((void *)((void *)0))) 
        g_module_close((plugin -> handle));
    }
  }
  else {
    PurplePlugin *loader;
    PurplePluginLoaderInfo *loader_info;
    loader = find_loader_for_plugin(plugin);
    if (loader != ((PurplePlugin *)((void *)0))) {
      loader_info = ((PurplePluginLoaderInfo *)( *(loader -> info)).extra_info);
      if ((loader_info -> destroy) != ((void (*)(PurplePlugin *))((void *)0))) 
        ( *(loader_info -> destroy))(plugin);
    }
  }
  g_free((plugin -> path));
  g_free((plugin -> error));
  purple_dbus_unregister_pointer(plugin);
  g_free(plugin);
#endif /* !PURPLE_PLUGINS */
}

gboolean purple_plugin_is_loaded(const PurplePlugin *plugin)
{
  do {
    if (plugin != ((const PurplePlugin *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin != NULL");
      return 0;
    };
  }while (0);
  return plugin -> loaded;
}

gboolean purple_plugin_is_unloadable(const PurplePlugin *plugin)
{
  do {
    if (plugin != ((const PurplePlugin *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin != NULL");
      return 0;
    };
  }while (0);
  return plugin -> unloadable;
}

const gchar *purple_plugin_get_id(const PurplePlugin *plugin)
{
  do {
    if (plugin != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin");
      return 0;
    };
  }while (0);
  do {
    if ((plugin -> info) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin->info");
      return 0;
    };
  }while (0);
  return ( *(plugin -> info)).id;
}

const gchar *purple_plugin_get_name(const PurplePlugin *plugin)
{
  do {
    if (plugin != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin");
      return 0;
    };
  }while (0);
  do {
    if ((plugin -> info) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin->info");
      return 0;
    };
  }while (0);
  return (const char *)(dgettext("pidgin",( *(plugin -> info)).name));
}

const gchar *purple_plugin_get_version(const PurplePlugin *plugin)
{
  do {
    if (plugin != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin");
      return 0;
    };
  }while (0);
  do {
    if ((plugin -> info) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin->info");
      return 0;
    };
  }while (0);
  return ( *(plugin -> info)).version;
}

const gchar *purple_plugin_get_summary(const PurplePlugin *plugin)
{
  do {
    if (plugin != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin");
      return 0;
    };
  }while (0);
  do {
    if ((plugin -> info) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin->info");
      return 0;
    };
  }while (0);
  return (const char *)(dgettext("pidgin",( *(plugin -> info)).summary));
}

const gchar *purple_plugin_get_description(const PurplePlugin *plugin)
{
  do {
    if (plugin != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin");
      return 0;
    };
  }while (0);
  do {
    if ((plugin -> info) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin->info");
      return 0;
    };
  }while (0);
  return (const char *)(dgettext("pidgin",( *(plugin -> info)).description));
}

const gchar *purple_plugin_get_author(const PurplePlugin *plugin)
{
  do {
    if (plugin != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin");
      return 0;
    };
  }while (0);
  do {
    if ((plugin -> info) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin->info");
      return 0;
    };
  }while (0);
  return (const char *)(dgettext("pidgin",( *(plugin -> info)).author));
}

const gchar *purple_plugin_get_homepage(const PurplePlugin *plugin)
{
  do {
    if (plugin != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin");
      return 0;
    };
  }while (0);
  do {
    if ((plugin -> info) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin->info");
      return 0;
    };
  }while (0);
  return ( *(plugin -> info)).homepage;
}
/**************************************************************************
 * Plugin IPC
 **************************************************************************/

static void destroy_ipc_info(void *data)
{
  PurplePluginIpcCommand *ipc_command = (PurplePluginIpcCommand *)data;
  int i;
  if ((ipc_command -> params) != ((PurpleValue **)((void *)0))) {
    for (i = 0; i < (ipc_command -> num_params); i++) 
      purple_value_destroy((ipc_command -> params)[i]);
    g_free((ipc_command -> params));
  }
  if ((ipc_command -> ret_value) != ((PurpleValue *)((void *)0))) 
    purple_value_destroy((ipc_command -> ret_value));
  g_free(ipc_command);
}

gboolean purple_plugin_ipc_register(PurplePlugin *plugin,const char *command,PurpleCallback func,PurpleSignalMarshalFunc marshal,PurpleValue *ret_value,int num_params,... )
{
  PurplePluginIpcInfo *ipc_info;
  PurplePluginIpcCommand *ipc_command;
  do {
    if (plugin != ((PurplePlugin *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin != NULL");
      return 0;
    };
  }while (0);
  do {
    if (command != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"command != NULL");
      return 0;
    };
  }while (0);
  do {
    if (func != ((void (*)())((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"func != NULL");
      return 0;
    };
  }while (0);
  do {
    if (marshal != ((void (*)(PurpleCallback , va_list , void *, void **))((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"marshal != NULL");
      return 0;
    };
  }while (0);
  if ((plugin -> ipc_data) == ((void *)((void *)0))) {
    ipc_info = (plugin -> ipc_data = ((PurplePluginIpcInfo *)(g_malloc0_n(1,(sizeof(PurplePluginIpcInfo ))))));
    ipc_info -> commands = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,destroy_ipc_info);
  }
  else 
    ipc_info = ((PurplePluginIpcInfo *)(plugin -> ipc_data));
  ipc_command = ((PurplePluginIpcCommand *)(g_malloc0_n(1,(sizeof(PurplePluginIpcCommand )))));
  ipc_command -> func = func;
  ipc_command -> marshal = marshal;
  ipc_command -> num_params = num_params;
  ipc_command -> ret_value = ret_value;
  if (num_params > 0) {
    va_list args;
    int i;
    ipc_command -> params = ((PurpleValue **)(g_malloc0_n(num_params,(sizeof(PurpleValue *)))));
    va_start(args,num_params);
    for (i = 0; i < num_params; i++) 
      (ipc_command -> params)[i] = (va_arg(args,PurpleValue *));
    va_end(args);
  }
  g_hash_table_replace((ipc_info -> commands),(g_strdup(command)),ipc_command);
  ipc_info -> command_count++;
  return (!0);
}

void purple_plugin_ipc_unregister(PurplePlugin *plugin,const char *command)
{
  PurplePluginIpcInfo *ipc_info;
  do {
    if (plugin != ((PurplePlugin *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin != NULL");
      return ;
    };
  }while (0);
  do {
    if (command != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"command != NULL");
      return ;
    };
  }while (0);
  ipc_info = ((PurplePluginIpcInfo *)(plugin -> ipc_data));
  if ((ipc_info == ((PurplePluginIpcInfo *)((void *)0))) || (g_hash_table_lookup((ipc_info -> commands),command) == ((void *)((void *)0)))) {
    purple_debug_error("plugins","IPC command \'%s\' was not registered for plugin %s\n",command,( *(plugin -> info)).name);
    return ;
  }
  g_hash_table_remove((ipc_info -> commands),command);
  ipc_info -> command_count--;
  if ((ipc_info -> command_count) == 0) {
    g_hash_table_destroy((ipc_info -> commands));
    g_free(ipc_info);
    plugin -> ipc_data = ((void *)((void *)0));
  }
}

void purple_plugin_ipc_unregister_all(PurplePlugin *plugin)
{
  PurplePluginIpcInfo *ipc_info;
  do {
    if (plugin != ((PurplePlugin *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin != NULL");
      return ;
    };
  }while (0);
  if ((plugin -> ipc_data) == ((void *)((void *)0))) 
/* Silently ignore it. */
    return ;
  ipc_info = ((PurplePluginIpcInfo *)(plugin -> ipc_data));
  g_hash_table_destroy((ipc_info -> commands));
  g_free(ipc_info);
  plugin -> ipc_data = ((void *)((void *)0));
}

gboolean purple_plugin_ipc_get_params(PurplePlugin *plugin,const char *command,PurpleValue **ret_value,int *num_params,PurpleValue ***params)
{
  PurplePluginIpcInfo *ipc_info;
  PurplePluginIpcCommand *ipc_command;
  do {
    if (plugin != ((PurplePlugin *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin != NULL");
      return 0;
    };
  }while (0);
  do {
    if (command != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"command != NULL");
      return 0;
    };
  }while (0);
  ipc_info = ((PurplePluginIpcInfo *)(plugin -> ipc_data));
  if ((ipc_info == ((PurplePluginIpcInfo *)((void *)0))) || ((ipc_command = (g_hash_table_lookup((ipc_info -> commands),command))) == ((PurplePluginIpcCommand *)((void *)0)))) {
    purple_debug_error("plugins","IPC command \'%s\' was not registered for plugin %s\n",command,( *(plugin -> info)).name);
    return 0;
  }
  if (num_params != ((int *)((void *)0))) 
     *num_params = (ipc_command -> num_params);
  if (params != ((PurpleValue ***)((void *)0))) 
     *params = (ipc_command -> params);
  if (ret_value != ((PurpleValue **)((void *)0))) 
     *ret_value = (ipc_command -> ret_value);
  return (!0);
}

void *purple_plugin_ipc_call(PurplePlugin *plugin,const char *command,gboolean *ok,... )
{
  PurplePluginIpcInfo *ipc_info;
  PurplePluginIpcCommand *ipc_command;
  va_list args;
  void *ret_value;
  if (ok != ((gboolean *)((void *)0))) 
     *ok = 0;
  do {
    if (plugin != ((PurplePlugin *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin != NULL");
      return 0;
    };
  }while (0);
  do {
    if (command != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"command != NULL");
      return 0;
    };
  }while (0);
  ipc_info = ((PurplePluginIpcInfo *)(plugin -> ipc_data));
  if ((ipc_info == ((PurplePluginIpcInfo *)((void *)0))) || ((ipc_command = (g_hash_table_lookup((ipc_info -> commands),command))) == ((PurplePluginIpcCommand *)((void *)0)))) {
    purple_debug_error("plugins","IPC command \'%s\' was not registered for plugin %s\n",command,( *(plugin -> info)).name);
    return 0;
  }
  va_start(args,ok);
  ( *(ipc_command -> marshal))((ipc_command -> func),args,0,&ret_value);
  va_end(args);
  if (ok != ((gboolean *)((void *)0))) 
     *ok = (!0);
  return ret_value;
}
/**************************************************************************
 * Plugins subsystem
 **************************************************************************/

void *purple_plugins_get_handle()
{
  static int handle;
  return (&handle);
}

void purple_plugins_init()
{
  void *handle = purple_plugins_get_handle();
  purple_plugins_add_search_path("/usr/local/lib/purple-2/");
  purple_signal_register(handle,"plugin-load",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_PLUGIN));
  purple_signal_register(handle,"plugin-unload",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_PLUGIN));
}

void purple_plugins_uninit()
{
  void *handle = purple_plugins_get_handle();
  purple_signals_disconnect_by_handle(handle);
  purple_signals_unregister_by_instance(handle);
  while(search_paths != 0){
    g_free((search_paths -> data));
    search_paths = g_list_delete_link(search_paths,search_paths);
  }
}
/**************************************************************************
 * Plugins API
 **************************************************************************/

void purple_plugins_add_search_path(const char *path)
{
  do {
    if (path != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"path != NULL");
      return ;
    };
  }while (0);
  if (g_list_find_custom(search_paths,path,((GCompareFunc )strcmp)) != 0) 
    return ;
  search_paths = g_list_append(search_paths,(g_strdup(path)));
}

GList *purple_plugins_get_search_paths()
{
  return search_paths;
}

void purple_plugins_unload_all()
{
#ifdef PURPLE_PLUGINS
  while(loaded_plugins != ((GList *)((void *)0)))
    purple_plugin_unload((loaded_plugins -> data));
#endif /* PURPLE_PLUGINS */
}

void purple_plugins_unload(PurplePluginType type)
{
#ifdef PURPLE_PLUGINS
  GList *l;
  for (l = plugins; l != 0; l = (l -> next)) {
    PurplePlugin *plugin = (l -> data);
    if ((( *(plugin -> info)).type == type) && (purple_plugin_is_loaded(plugin) != 0)) 
      purple_plugin_unload(plugin);
  }
#endif /* PURPLE_PLUGINS */
}

void purple_plugins_destroy_all()
{
#ifdef PURPLE_PLUGINS
  while(plugins != ((GList *)((void *)0)))
    purple_plugin_destroy((plugins -> data));
#endif /* PURPLE_PLUGINS */
}

void purple_plugins_save_loaded(const char *key)
{
#ifdef PURPLE_PLUGINS
  GList *pl;
  GList *files = (GList *)((void *)0);
  for (pl = purple_plugins_get_loaded(); pl != ((GList *)((void *)0)); pl = (pl -> next)) {
    PurplePlugin *plugin = (pl -> data);
    if (((( *(plugin -> info)).type != PURPLE_PLUGIN_PROTOCOL) && (( *(plugin -> info)).type != PURPLE_PLUGIN_LOADER)) && !(g_list_find(plugins_to_disable,plugin) != 0)) {
      files = g_list_append(files,(plugin -> path));
    }
  }
  purple_prefs_set_path_list(key,files);
  g_list_free(files);
#endif
}

void purple_plugins_load_saved(const char *key)
{
#ifdef PURPLE_PLUGINS
  GList *f;
  GList *files;
  do {
    if (key != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"key != NULL");
      return ;
    };
  }while (0);
  files = purple_prefs_get_path_list(key);
  for (f = files; f != 0; f = (f -> next)) {{
      char *filename;
      char *basename;
      PurplePlugin *plugin;
      if ((f -> data) == ((void *)((void *)0))) 
        continue; 
      filename = (f -> data);
/*
		 * We don't know if the filename uses Windows or Unix path
		 * separators (because people might be sharing a prefs.xml
		 * file across systems), so we find the last occurrence
		 * of either.
		 */
      basename = strrchr(filename,'/');
      if ((basename == ((char *)((void *)0))) || (basename < strrchr(filename,'\\'))) 
        basename = strrchr(filename,'\\');
      if (basename != ((char *)((void *)0))) 
        basename++;
/* Strip the extension */
      if (basename != 0) 
        basename = purple_plugin_get_basename(basename);
      if ((((plugin = purple_plugins_find_with_filename(filename)) != ((PurplePlugin *)((void *)0))) || ((basename != 0) && ((plugin = purple_plugins_find_with_basename(basename)) != ((PurplePlugin *)((void *)0))))) || ((plugin = purple_plugin_probe(filename)) != ((PurplePlugin *)((void *)0)))) {
        purple_debug_info("plugins","Loading saved plugin %s\n",(plugin -> path));
        purple_plugin_load(plugin);
      }
      else {
        purple_debug_error("plugins","Unable to find saved plugin %s\n",filename);
      }
      g_free(basename);
      g_free((f -> data));
    }
  }
  g_list_free(files);
#endif /* PURPLE_PLUGINS */
}

void purple_plugins_probe(const char *ext)
{
#ifdef PURPLE_PLUGINS
  GDir *dir;
  const gchar *file;
  gchar *path;
  PurplePlugin *plugin;
  GList *cur;
  const char *search_path;
  if (!(g_module_supported() != 0)) 
    return ;
/* Probe plugins */
  for (cur = search_paths; cur != ((GList *)((void *)0)); cur = (cur -> next)) {
    search_path = (cur -> data);
    dir = g_dir_open(search_path,0,0);
    if (dir != ((GDir *)((void *)0))) {
      while((file = g_dir_read_name(dir)) != ((const gchar *)((void *)0))){
        path = g_build_filename(search_path,file,((void *)((void *)0)));
        if ((ext == ((const char *)((void *)0))) || (has_file_extension(file,ext) != 0)) 
          purple_plugin_probe(path);
        g_free(path);
      }
      g_dir_close(dir);
    }
  }
/* See if we have any plugins waiting to load */
  while(load_queue != ((GList *)((void *)0))){
    plugin = ((PurplePlugin *)(load_queue -> data));
    load_queue = g_list_remove(load_queue,plugin);
    if ((plugin == ((PurplePlugin *)((void *)0))) || ((plugin -> info) == ((PurplePluginInfo *)((void *)0)))) 
      continue; 
    if (( *(plugin -> info)).type == PURPLE_PLUGIN_LOADER) {
/* We'll just load this right now. */
      if (!(purple_plugin_load(plugin) != 0)) {
        purple_plugin_destroy(plugin);
        continue; 
      }
      plugin_loaders = g_list_append(plugin_loaders,plugin);
      for (cur = ( *((PurplePluginLoaderInfo *)( *(plugin -> info)).extra_info)).exts; cur != ((GList *)((void *)0)); cur = (cur -> next)) {
        purple_plugins_probe((cur -> data));
      }
    }
    else if (( *(plugin -> info)).type == PURPLE_PLUGIN_PROTOCOL) {
/* We'll just load this right now. */
      if (!(purple_plugin_load(plugin) != 0)) {
        purple_plugin_destroy(plugin);
        continue; 
      }
/* Make sure we don't load two PRPLs with the same name? */
      if (purple_find_prpl(( *(plugin -> info)).id) != 0) {
/* Nothing to see here--move along, move along */
        purple_plugin_destroy(plugin);
        continue; 
      }
      protocol_plugins = g_list_insert_sorted(protocol_plugins,plugin,((GCompareFunc )compare_prpl));
    }
  }
  if (probe_cb != ((void (*)(void *))((void *)0))) 
    ( *probe_cb)(probe_cb_data);
#endif /* PURPLE_PLUGINS */
}

gboolean purple_plugin_register(PurplePlugin *plugin)
{
  do {
    if (plugin != ((PurplePlugin *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"plugin != NULL");
      return 0;
    };
  }while (0);
/* If this plugin has been registered already then exit */
  if (g_list_find(plugins,plugin) != 0) 
    return (!0);
/* Ensure the plugin has the requisite information */
  if (( *(plugin -> info)).type == PURPLE_PLUGIN_LOADER) {
    PurplePluginLoaderInfo *loader_info;
    loader_info = ((PurplePluginLoaderInfo *)( *(plugin -> info)).extra_info);
    if (loader_info == ((PurplePluginLoaderInfo *)((void *)0))) {
      purple_debug_error("plugins","%s is not loadable, loader plugin missing loader_info\n",(plugin -> path));
      return 0;
    }
  }
  else if (( *(plugin -> info)).type == PURPLE_PLUGIN_PROTOCOL) {
    PurplePluginProtocolInfo *prpl_info;
    prpl_info = ((PurplePluginProtocolInfo *)( *(plugin -> info)).extra_info);
    if (prpl_info == ((PurplePluginProtocolInfo *)((void *)0))) {
      purple_debug_error("plugins","%s is not loadable, protocol plugin missing prpl_info\n",(plugin -> path));
      return 0;
    }
  }
#ifdef PURPLE_PLUGINS
/* This plugin should be probed and maybe loaded--add it to the queue */
  load_queue = g_list_append(load_queue,plugin);
#else
#endif
  plugins = g_list_append(plugins,plugin);
  return (!0);
}

gboolean purple_plugins_enabled()
{
#ifdef PURPLE_PLUGINS
  return (!0);
#else
#endif
}

void purple_plugins_register_probe_notify_cb(void (*func)(void *),void *data)
{
  probe_cb = func;
  probe_cb_data = data;
}

void purple_plugins_unregister_probe_notify_cb(void (*func)(void *))
{
  probe_cb = ((void (*)(void *))((void *)0));
  probe_cb_data = ((void *)((void *)0));
}

void purple_plugins_register_load_notify_cb(void (*func)(PurplePlugin *, void *),void *data)
{
  load_cb = func;
  load_cb_data = data;
}

void purple_plugins_unregister_load_notify_cb(void (*func)(PurplePlugin *, void *))
{
  load_cb = ((void (*)(PurplePlugin *, void *))((void *)0));
  load_cb_data = ((void *)((void *)0));
}

void purple_plugins_register_unload_notify_cb(void (*func)(PurplePlugin *, void *),void *data)
{
  unload_cb = func;
  unload_cb_data = data;
}

void purple_plugins_unregister_unload_notify_cb(void (*func)(PurplePlugin *, void *))
{
  unload_cb = ((void (*)(PurplePlugin *, void *))((void *)0));
  unload_cb_data = ((void *)((void *)0));
}

PurplePlugin *purple_plugins_find_with_name(const char *name)
{
  PurplePlugin *plugin;
  GList *l;
  for (l = plugins; l != ((GList *)((void *)0)); l = (l -> next)) {
    plugin = (l -> data);
    if (purple_strequal(( *(plugin -> info)).name,name) != 0) 
      return plugin;
  }
  return 0;
}

PurplePlugin *purple_plugins_find_with_filename(const char *filename)
{
  PurplePlugin *plugin;
  GList *l;
  for (l = plugins; l != ((GList *)((void *)0)); l = (l -> next)) {
    plugin = (l -> data);
    if (purple_strequal((plugin -> path),filename) != 0) 
      return plugin;
  }
  return 0;
}

PurplePlugin *purple_plugins_find_with_basename(const char *basename)
{
#ifdef PURPLE_PLUGINS
  PurplePlugin *plugin;
  GList *l;
  char *tmp;
  do {
    if (basename != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"basename != NULL");
      return 0;
    };
  }while (0);
  for (l = plugins; l != ((GList *)((void *)0)); l = (l -> next)) {
    plugin = ((PurplePlugin *)(l -> data));
    if ((plugin -> path) != ((char *)((void *)0))) {
      tmp = purple_plugin_get_basename((plugin -> path));
      if (purple_strequal(tmp,basename) != 0) {
        g_free(tmp);
        return plugin;
      }
      g_free(tmp);
    }
  }
#endif /* PURPLE_PLUGINS */
  return 0;
}

PurplePlugin *purple_plugins_find_with_id(const char *id)
{
  PurplePlugin *plugin;
  GList *l;
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  for (l = plugins; l != ((GList *)((void *)0)); l = (l -> next)) {
    plugin = (l -> data);
    if (purple_strequal(( *(plugin -> info)).id,id) != 0) 
      return plugin;
  }
  return 0;
}

GList *purple_plugins_get_loaded()
{
  return loaded_plugins;
}

GList *purple_plugins_get_protocols()
{
  return protocol_plugins;
}

GList *purple_plugins_get_all()
{
  return plugins;
}

PurplePluginAction *purple_plugin_action_new(const char *label,void (*callback)(PurplePluginAction *))
{
  PurplePluginAction *act = (PurplePluginAction *)(g_malloc0_n(1,(sizeof(PurplePluginAction ))));
  act -> label = g_strdup(label);
  act -> callback = callback;
  return act;
}

void purple_plugin_action_free(PurplePluginAction *action)
{
  do {
    if (action != ((PurplePluginAction *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"action != NULL");
      return ;
    };
  }while (0);
  g_free((action -> label));
  g_free(action);
}
