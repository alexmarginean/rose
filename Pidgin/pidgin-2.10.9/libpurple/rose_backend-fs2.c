/**
 * @file backend-fs2.c Farstream backend for media API
 * @ingroup core
 */
/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "backend-fs2.h"
#ifdef USE_VV
#include "backend-iface.h"
#include "debug.h"
#include "network.h"
#include "media-gst.h"
#ifdef HAVE_FARSIGHT
#include <gst/farsight/fs-conference-iface.h>
#include <gst/farsight/fs-element-added-notifier.h>
#else
#include <farstream/fs-conference.h>
#include <farstream/fs-element-added-notifier.h>
#include <farstream/fs-utils.h>
#endif
/** @copydoc _PurpleMediaBackendFs2Class */
typedef struct _PurpleMediaBackendFs2Class PurpleMediaBackendFs2Class;
/** @copydoc _PurpleMediaBackendFs2Private */
typedef struct _PurpleMediaBackendFs2Private PurpleMediaBackendFs2Private;
/** @copydoc _PurpleMediaBackendFs2Session */
typedef struct _PurpleMediaBackendFs2Session PurpleMediaBackendFs2Session;
/** @copydoc _PurpleMediaBackendFs2Stream */
typedef struct _PurpleMediaBackendFs2Stream PurpleMediaBackendFs2Stream;
#define PURPLE_MEDIA_BACKEND_FS2_GET_PRIVATE(obj) \
		(G_TYPE_INSTANCE_GET_PRIVATE((obj), \
		PURPLE_TYPE_MEDIA_BACKEND_FS2, PurpleMediaBackendFs2Private))
static void purple_media_backend_iface_init(PurpleMediaBackendIface *iface);
static gboolean gst_bus_cb(GstBus *bus,GstMessage *msg,PurpleMediaBackendFs2 *self);
static void state_changed_cb(PurpleMedia *media,PurpleMediaState state,gchar *sid,gchar *name,PurpleMediaBackendFs2 *self);
static void stream_info_cb(PurpleMedia *media,PurpleMediaInfoType type,gchar *sid,gchar *name,gboolean local,PurpleMediaBackendFs2 *self);
static gboolean purple_media_backend_fs2_add_stream(PurpleMediaBackend *self,const gchar *sess_id,const gchar *who,PurpleMediaSessionType type,gboolean initiator,const gchar *transmitter,guint num_params,GParameter *params);
static void purple_media_backend_fs2_add_remote_candidates(PurpleMediaBackend *self,const gchar *sess_id,const gchar *participant,GList *remote_candidates);
static gboolean purple_media_backend_fs2_codecs_ready(PurpleMediaBackend *self,const gchar *sess_id);
static GList *purple_media_backend_fs2_get_codecs(PurpleMediaBackend *self,const gchar *sess_id);
static GList *purple_media_backend_fs2_get_local_candidates(PurpleMediaBackend *self,const gchar *sess_id,const gchar *participant);
static gboolean purple_media_backend_fs2_set_remote_codecs(PurpleMediaBackend *self,const gchar *sess_id,const gchar *participant,GList *codecs);
static gboolean purple_media_backend_fs2_set_send_codec(PurpleMediaBackend *self,const gchar *sess_id,PurpleMediaCodec *codec);
static void purple_media_backend_fs2_set_params(PurpleMediaBackend *self,guint num_params,GParameter *params);
static const gchar **purple_media_backend_fs2_get_available_params();
static void free_stream(PurpleMediaBackendFs2Stream *stream);
static void free_session(PurpleMediaBackendFs2Session *session);

struct _PurpleMediaBackendFs2Class 
{
  GObjectClass parent_class;
}
;

struct _PurpleMediaBackendFs2 
{
  GObject parent;
}
;
static void purple_media_backend_fs2_init(PurpleMediaBackendFs2 *self);
static void purple_media_backend_fs2_class_init(PurpleMediaBackendFs2Class *klass);
static gpointer purple_media_backend_fs2_parent_class = (gpointer )((void *)0);
static gint PurpleMediaBackendFs2_private_offset;

static void purple_media_backend_fs2_class_intern_init(gpointer klass)
{
  purple_media_backend_fs2_parent_class = g_type_class_peek_parent(klass);
  if (PurpleMediaBackendFs2_private_offset != 0) 
    g_type_class_adjust_private_offset(klass,&PurpleMediaBackendFs2_private_offset);
  purple_media_backend_fs2_class_init(((PurpleMediaBackendFs2Class *)klass));
}

inline static gpointer purple_media_backend_fs2_get_instance_private(PurpleMediaBackendFs2 *self)
{
  return (gpointer )(((guint8 *)self) + ((glong )PurpleMediaBackendFs2_private_offset));
}

GType purple_media_backend_fs2_get_type()
{
  static volatile gsize g_define_type_id__volatile = 0;
  if ((({
    typedef char _GStaticAssertCompileTimeAssertion_111[1UL];
    (0?((gpointer )g_define_type_id__volatile) : 0);
    !(((
{
      typedef char _GStaticAssertCompileTimeAssertion_111[1UL];
      __sync_synchronize();
      (gpointer )g_define_type_id__volatile;
    })) != 0) && (g_once_init_enter((&g_define_type_id__volatile)) != 0);
  })) != 0) 
{
    GType g_define_type_id = g_type_register_static_simple(((GType )(20 << 2)),g_intern_static_string("PurpleMediaBackendFs2"),(sizeof(PurpleMediaBackendFs2Class )),((GClassInitFunc )purple_media_backend_fs2_class_intern_init),(sizeof(PurpleMediaBackendFs2 )),((GInstanceInitFunc )purple_media_backend_fs2_init),((GTypeFlags )0));
{
{
{
          const GInterfaceInfo g_implement_interface_info = {((GInterfaceInitFunc )purple_media_backend_iface_init), ((GInterfaceFinalizeFunc )((void *)0)), ((gpointer )((void *)0))};
          g_type_add_interface_static(g_define_type_id,purple_media_backend_get_type(),&g_implement_interface_info);
        };
      }
    }
    (
{
      typedef char _GStaticAssertCompileTimeAssertion_111[1UL];
      0?(g_define_type_id__volatile = g_define_type_id) : 0;
      g_once_init_leave((&g_define_type_id__volatile),((gsize )g_define_type_id));
    });
  }
  return g_define_type_id__volatile;
}

struct _PurpleMediaBackendFs2Stream 
{
  PurpleMediaBackendFs2Session *session;
  gchar *participant;
  FsStream *stream;
#ifndef HAVE_FARSIGHT
  gboolean supports_add;
#endif
  GstElement *src;
  GstElement *tee;
  GstElement *volume;
  GstElement *level;
  GstElement *fakesink;
  GstElement *queue;
  GList *local_candidates;
  GList *remote_candidates;
  guint connected_cb_id;
}
;

struct _PurpleMediaBackendFs2Session 
{
  PurpleMediaBackendFs2 *backend;
  gchar *id;
  FsSession *session;
  GstElement *src;
  GstElement *tee;
  GstElement *srcvalve;
  GstPad *srcpad;
  PurpleMediaSessionType type;
}
;

struct _PurpleMediaBackendFs2Private 
{
  PurpleMedia *media;
  GstElement *confbin;
  FsConference *conference;
  gchar *conference_type;
#ifndef HAVE_FARSIGHT
  FsElementAddedNotifier *notifier;
#endif
  GHashTable *sessions;
  GHashTable *participants;
  GList *streams;
  gdouble silence_threshold;
}
;
enum __unnamed_enum___F0_L172_C1_PROP_0__COMMA__PROP_CONFERENCE_TYPE__COMMA__PROP_MEDIA {PROP_0,PROP_CONFERENCE_TYPE,PROP_MEDIA};

static void purple_media_backend_fs2_init(PurpleMediaBackendFs2 *self)
{
}

static gboolean event_probe_cb(GstPad *srcpad,GstEvent *event,gboolean release_pad)
{
  if ((( *((GstEvent *)event)).type == GST_EVENT_CUSTOM_DOWNSTREAM) && (gst_event_has_name(event,"purple-unlink-tee") != 0)) {
    const GstStructure *s = gst_event_get_structure(event);
    gst_pad_unlink(srcpad,gst_pad_get_peer(srcpad));
    gst_pad_remove_event_probe(srcpad,g_value_get_uint(gst_structure_get_value(s,"handler-id")));
    if (g_value_get_boolean(gst_structure_get_value(s,"release-pad")) != 0) 
      gst_element_release_request_pad(((GstElement *)( *((GstObject *)srcpad)).parent),srcpad);
    return 0;
  }
  return (!0);
}

static void unlink_teepad_dynamic(GstPad *srcpad,gboolean release_pad)
{
  guint id = (gst_pad_add_event_probe(srcpad,((GCallback )event_probe_cb),0));
  if ((({
    GTypeInstance *__inst = (GTypeInstance *)srcpad;
    GType __t = gst_ghost_pad_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) 
    srcpad = gst_ghost_pad_get_target(((GstGhostPad *)(g_type_check_instance_cast(((GTypeInstance *)srcpad),gst_ghost_pad_get_type()))));
  gst_element_send_event(gst_pad_get_parent_element(srcpad),gst_event_new_custom(GST_EVENT_CUSTOM_DOWNSTREAM,gst_structure_new("purple-unlink-tee","release-pad",((GType )(5 << 2)),release_pad,"handler-id",((GType )(7 << 2)),id,((void *)((void *)0)))));
}

static void purple_media_backend_fs2_dispose(GObject *obj)
{
  PurpleMediaBackendFs2Private *priv = (PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)obj),purple_media_backend_fs2_get_type()));
  GList *iter = (GList *)((void *)0);
  purple_debug_info("backend-fs2","purple_media_backend_fs2_dispose\n");
#ifndef HAVE_FARSIGHT
  if ((priv -> notifier) != 0) {
    g_object_unref((priv -> notifier));
    priv -> notifier = ((FsElementAddedNotifier *)((void *)0));
  }
#endif
  if ((priv -> confbin) != 0) {
    GstElement *pipeline;
    pipeline = purple_media_manager_get_pipeline(purple_media_get_manager((priv -> media)));
/* All connections to media sources should be blocked before confbin is
		 * removed, to prevent freezing of any other simultaneously running
		 * media calls. */
    if ((priv -> sessions) != 0) {
      GList *sessions = g_hash_table_get_values((priv -> sessions));
      for (; sessions != 0; sessions = g_list_delete_link(sessions,sessions)) {
        PurpleMediaBackendFs2Session *session = (sessions -> data);
        if ((session -> srcpad) != 0) {
          unlink_teepad_dynamic((session -> srcpad),0);
          gst_object_unref((session -> srcpad));
          session -> srcpad = ((GstPad *)((void *)0));
        }
      }
    }
    gst_element_set_locked_state((priv -> confbin),(!0));
    gst_element_set_state(((GstElement *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> confbin)),gst_element_get_type()))),GST_STATE_NULL);
    if (pipeline != 0) {
      GstBus *bus;
      gst_bin_remove(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)pipeline),gst_bin_get_type()))),(priv -> confbin));
      bus = gst_pipeline_get_bus(((GstPipeline *)(g_type_check_instance_cast(((GTypeInstance *)pipeline),gst_pipeline_get_type()))));
      g_signal_handlers_disconnect_matched(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)bus),((GType )(20 << 2))))),(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA),0,0,0,gst_bus_cb,obj);
      gst_object_unref(bus);
    }
    else {
      purple_debug_warning("backend-fs2","Unable to properly dispose the conference. Couldn\'t get the pipeline.\n");
    }
    priv -> confbin = ((GstElement *)((void *)0));
    priv -> conference = ((FsConference *)((void *)0));
  }
  if ((priv -> sessions) != 0) {
    GList *sessions = g_hash_table_get_values((priv -> sessions));
    for (; sessions != 0; sessions = g_list_delete_link(sessions,sessions)) {
      PurpleMediaBackendFs2Session *session = (sessions -> data);
      if ((session -> session) != 0) {
        g_object_unref((session -> session));
        session -> session = ((FsSession *)((void *)0));
      }
    }
  }
  if ((priv -> participants) != 0) {
    g_hash_table_destroy((priv -> participants));
    priv -> participants = ((GHashTable *)((void *)0));
  }
  for (iter = (priv -> streams); iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
    PurpleMediaBackendFs2Stream *stream = (iter -> data);
    if ((stream -> stream) != 0) {
      g_object_unref((stream -> stream));
      stream -> stream = ((FsStream *)((void *)0));
    }
  }
  if ((priv -> media) != 0) {
    g_object_remove_weak_pointer(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> media)),((GType )(20 << 2))))),((gpointer *)(&priv -> media)));
    priv -> media = ((PurpleMedia *)((void *)0));
  }
  ( *( *((GObjectClass *)(g_type_check_class_cast(((GTypeClass *)purple_media_backend_fs2_parent_class),((GType )(20 << 2)))))).dispose)(obj);
}

static void purple_media_backend_fs2_finalize(GObject *obj)
{
  PurpleMediaBackendFs2Private *priv = (PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)obj),purple_media_backend_fs2_get_type()));
  purple_debug_info("backend-fs2","purple_media_backend_fs2_finalize\n");
  g_free((priv -> conference_type));
  for (; (priv -> streams) != 0; priv -> streams = g_list_delete_link((priv -> streams),(priv -> streams))) {
    PurpleMediaBackendFs2Stream *stream = ( *(priv -> streams)).data;
    free_stream(stream);
  }
  if ((priv -> sessions) != 0) {
    GList *sessions = g_hash_table_get_values((priv -> sessions));
    for (; sessions != 0; sessions = g_list_delete_link(sessions,sessions)) {
      PurpleMediaBackendFs2Session *session = (sessions -> data);
      free_session(session);
    }
    g_hash_table_destroy((priv -> sessions));
  }
  ( *( *((GObjectClass *)(g_type_check_class_cast(((GTypeClass *)purple_media_backend_fs2_parent_class),((GType )(20 << 2)))))).finalize)(obj);
}

static void purple_media_backend_fs2_set_property(GObject *object,guint prop_id,const GValue *value,GParamSpec *pspec)
{
  PurpleMediaBackendFs2Private *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = purple_media_backend_fs2_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND_FS2(object)");
      return ;
    };
  }while (0);
  priv = ((PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)object),purple_media_backend_fs2_get_type())));
{
    switch(prop_id){
      case PROP_CONFERENCE_TYPE:
{
        priv -> conference_type = g_value_dup_string(value);
        break; 
      }
      case PROP_MEDIA:
{
        priv -> media = (g_value_get_object(value));
        if ((priv -> media) == ((PurpleMedia *)((void *)0))) 
          break; 
        g_object_add_weak_pointer(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> media)),((GType )(20 << 2))))),((gpointer *)(&priv -> media)));
        g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> media)),((GType )(20 << 2))))),"state-changed",((GCallback )state_changed_cb),((PurpleMediaBackendFs2 *)(g_type_check_instance_cast(((GTypeInstance *)object),purple_media_backend_fs2_get_type()))),0,((GConnectFlags )0));
        g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> media)),((GType )(20 << 2))))),"stream-info",((GCallback )stream_info_cb),((PurpleMediaBackendFs2 *)(g_type_check_instance_cast(((GTypeInstance *)object),purple_media_backend_fs2_get_type()))),0,((GConnectFlags )0));
        break; 
      }
      default:
{
        do {
          GObject *_glib__object = (GObject *)object;
          GParamSpec *_glib__pspec = (GParamSpec *)pspec;
          guint _glib__property_id = prop_id;
          g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","backend-fs2.c:382","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
        }while (0);
        break; 
      }
    }
  }
}

static void purple_media_backend_fs2_get_property(GObject *object,guint prop_id,GValue *value,GParamSpec *pspec)
{
  PurpleMediaBackendFs2Private *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = purple_media_backend_fs2_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND_FS2(object)");
      return ;
    };
  }while (0);
  priv = ((PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)object),purple_media_backend_fs2_get_type())));
  switch(prop_id){
    case PROP_CONFERENCE_TYPE:
{
      g_value_set_string(value,(priv -> conference_type));
      break; 
    }
    case PROP_MEDIA:
{
      g_value_set_object(value,(priv -> media));
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = prop_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","backend-fs2.c:405","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void purple_media_backend_fs2_class_init(PurpleMediaBackendFs2Class *klass)
{
  GObjectClass *gobject_class = (GObjectClass *)klass;
  gobject_class -> dispose = purple_media_backend_fs2_dispose;
  gobject_class -> finalize = purple_media_backend_fs2_finalize;
  gobject_class -> set_property = purple_media_backend_fs2_set_property;
  gobject_class -> get_property = purple_media_backend_fs2_get_property;
  g_object_class_override_property(gobject_class,PROP_CONFERENCE_TYPE,"conference-type");
  g_object_class_override_property(gobject_class,PROP_MEDIA,"media");
  g_type_class_add_private(klass,(sizeof(PurpleMediaBackendFs2Private )));
}

static void purple_media_backend_iface_init(PurpleMediaBackendIface *iface)
{
  iface -> add_stream = purple_media_backend_fs2_add_stream;
  iface -> add_remote_candidates = purple_media_backend_fs2_add_remote_candidates;
  iface -> codecs_ready = purple_media_backend_fs2_codecs_ready;
  iface -> get_codecs = purple_media_backend_fs2_get_codecs;
  iface -> get_local_candidates = purple_media_backend_fs2_get_local_candidates;
  iface -> set_remote_codecs = purple_media_backend_fs2_set_remote_codecs;
  iface -> set_send_codec = purple_media_backend_fs2_set_send_codec;
  iface -> set_params = purple_media_backend_fs2_set_params;
  iface -> get_available_params = purple_media_backend_fs2_get_available_params;
}

static FsMediaType session_type_to_fs_media_type(PurpleMediaSessionType type)
{
  if ((type & PURPLE_MEDIA_AUDIO) != 0U) 
    return FS_MEDIA_TYPE_AUDIO;
  else if ((type & PURPLE_MEDIA_VIDEO) != 0U) 
    return FS_MEDIA_TYPE_VIDEO;
  else 
    return 0;
}

static FsStreamDirection session_type_to_fs_stream_direction(PurpleMediaSessionType type)
{
  if (((type & PURPLE_MEDIA_AUDIO) == PURPLE_MEDIA_AUDIO) || ((type & PURPLE_MEDIA_VIDEO) == PURPLE_MEDIA_VIDEO)) 
    return FS_DIRECTION_BOTH;
  else if (((type & PURPLE_MEDIA_SEND_AUDIO) != 0U) || ((type & PURPLE_MEDIA_SEND_VIDEO) != 0U)) 
    return FS_DIRECTION_SEND;
  else if (((type & PURPLE_MEDIA_RECV_AUDIO) != 0U) || ((type & PURPLE_MEDIA_RECV_VIDEO) != 0U)) 
    return FS_DIRECTION_RECV;
  else 
    return FS_DIRECTION_NONE;
}

static PurpleMediaSessionType session_type_from_fs(FsMediaType type,FsStreamDirection direction)
{
  PurpleMediaSessionType result = PURPLE_MEDIA_NONE;
  if (type == FS_MEDIA_TYPE_AUDIO) {
    if ((direction & FS_DIRECTION_SEND) != 0U) 
      result |= PURPLE_MEDIA_SEND_AUDIO;
    if ((direction & FS_DIRECTION_RECV) != 0U) 
      result |= PURPLE_MEDIA_RECV_AUDIO;
  }
  else if (type == FS_MEDIA_TYPE_VIDEO) {
    if ((direction & FS_DIRECTION_SEND) != 0U) 
      result |= PURPLE_MEDIA_SEND_VIDEO;
    if ((direction & FS_DIRECTION_RECV) != 0U) 
      result |= PURPLE_MEDIA_RECV_VIDEO;
  }
  return result;
}

static FsCandidate *candidate_to_fs(PurpleMediaCandidate *candidate)
{
  FsCandidate *fscandidate;
  gchar *foundation;
  guint component_id;
  gchar *ip;
  guint port;
  gchar *base_ip;
  guint base_port;
  PurpleMediaNetworkProtocol proto;
  guint32 priority;
  PurpleMediaCandidateType type;
  gchar *username;
  gchar *password;
  guint ttl;
  if (candidate == ((PurpleMediaCandidate *)((void *)0))) 
    return 0;
  g_object_get(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)candidate),((GType )(20 << 2))))),"foundation",&foundation,"component-id",&component_id,"ip",&ip,"port",&port,"base-ip",&base_ip,"base-port",&base_port,"protocol",&proto,"priority",&priority,"type",&type,"username",&username,"password",&password,"ttl",&ttl,((void *)((void *)0)));
  fscandidate = fs_candidate_new(foundation,component_id,type,proto,ip,port);
  fscandidate -> base_ip = base_ip;
  fscandidate -> base_port = base_port;
  fscandidate -> priority = priority;
  fscandidate -> username = username;
  fscandidate -> password = password;
  fscandidate -> ttl = ttl;
  g_free(foundation);
  g_free(ip);
  return fscandidate;
}

static GList *candidate_list_to_fs(GList *candidates)
{
  GList *new_list = (GList *)((void *)0);
  for (; candidates != 0; candidates = ((candidates != 0)?( *((GList *)candidates)).next : ((struct _GList *)((void *)0)))) {
    new_list = g_list_prepend(new_list,(candidate_to_fs((candidates -> data))));
  }
  new_list = g_list_reverse(new_list);
  return new_list;
}

static PurpleMediaCandidate *candidate_from_fs(FsCandidate *fscandidate)
{
  PurpleMediaCandidate *candidate;
  if (fscandidate == ((FsCandidate *)((void *)0))) 
    return 0;
  candidate = purple_media_candidate_new((fscandidate -> foundation),(fscandidate -> component_id),(fscandidate -> type),(fscandidate -> proto),(fscandidate -> ip),(fscandidate -> port));
  g_object_set(candidate,"base-ip",(fscandidate -> base_ip),"base-port",(fscandidate -> base_port),"priority",(fscandidate -> priority),"username",(fscandidate -> username),"password",(fscandidate -> password),"ttl",(fscandidate -> ttl),((void *)((void *)0)));
  return candidate;
}

static GList *candidate_list_from_fs(GList *candidates)
{
  GList *new_list = (GList *)((void *)0);
  for (; candidates != 0; candidates = ((candidates != 0)?( *((GList *)candidates)).next : ((struct _GList *)((void *)0)))) {
    new_list = g_list_prepend(new_list,(candidate_from_fs((candidates -> data))));
  }
  new_list = g_list_reverse(new_list);
  return new_list;
}

static FsCodec *codec_to_fs(const PurpleMediaCodec *codec)
{
  FsCodec *new_codec;
  gint id;
  char *encoding_name;
  PurpleMediaSessionType media_type;
  guint clock_rate;
  guint channels;
  GList *iter;
  if (codec == ((const PurpleMediaCodec *)((void *)0))) 
    return 0;
  g_object_get(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)codec),((GType )(20 << 2))))),"id",&id,"encoding-name",&encoding_name,"media-type",&media_type,"clock-rate",&clock_rate,"channels",&channels,"optional-params",&iter,((void *)((void *)0)));
  new_codec = fs_codec_new(id,encoding_name,session_type_to_fs_media_type(media_type),clock_rate);
  new_codec -> channels = channels;
  for (; iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
    PurpleKeyValuePair *param = (PurpleKeyValuePair *)(iter -> data);
    fs_codec_add_optional_parameter(new_codec,(param -> key),(param -> value));
  }
  g_free(encoding_name);
  return new_codec;
}

static PurpleMediaCodec *codec_from_fs(const FsCodec *codec)
{
  PurpleMediaCodec *new_codec;
  GList *iter;
  if (codec == ((const FsCodec *)((void *)0))) 
    return 0;
  new_codec = purple_media_codec_new((codec -> id),(codec -> encoding_name),session_type_from_fs((codec -> media_type),FS_DIRECTION_BOTH),(codec -> clock_rate));
  g_object_set(new_codec,"channels",(codec -> channels),((void *)((void *)0)));
  for (iter = (codec -> optional_params); iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
    FsCodecParameter *param = (FsCodecParameter *)(iter -> data);
    purple_media_codec_add_optional_parameter(new_codec,(param -> name),(param -> value));
  }
  return new_codec;
}

static GList *codec_list_from_fs(GList *codecs)
{
  GList *new_list = (GList *)((void *)0);
  for (; codecs != 0; codecs = ((codecs != 0)?( *((GList *)codecs)).next : ((struct _GList *)((void *)0)))) {
    new_list = g_list_prepend(new_list,(codec_from_fs((codecs -> data))));
  }
  new_list = g_list_reverse(new_list);
  return new_list;
}

static GList *codec_list_to_fs(GList *codecs)
{
  GList *new_list = (GList *)((void *)0);
  for (; codecs != 0; codecs = ((codecs != 0)?( *((GList *)codecs)).next : ((struct _GList *)((void *)0)))) {
    new_list = g_list_prepend(new_list,(codec_to_fs((codecs -> data))));
  }
  new_list = g_list_reverse(new_list);
  return new_list;
}

static PurpleMediaBackendFs2Session *get_session(PurpleMediaBackendFs2 *self,const gchar *sess_id)
{
  PurpleMediaBackendFs2Private *priv;
  PurpleMediaBackendFs2Session *session = (PurpleMediaBackendFs2Session *)((void *)0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_fs2_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND_FS2(self)");
      return 0;
    };
  }while (0);
  priv = ((PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)self),purple_media_backend_fs2_get_type())));
  if ((priv -> sessions) != ((GHashTable *)((void *)0))) 
    session = (g_hash_table_lookup((priv -> sessions),sess_id));
  return session;
}

static FsParticipant *get_participant(PurpleMediaBackendFs2 *self,const gchar *name)
{
  PurpleMediaBackendFs2Private *priv;
  FsParticipant *participant = (FsParticipant *)((void *)0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_fs2_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND_FS2(self)");
      return 0;
    };
  }while (0);
  priv = ((PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)self),purple_media_backend_fs2_get_type())));
  if ((priv -> participants) != ((GHashTable *)((void *)0))) 
    participant = (g_hash_table_lookup((priv -> participants),name));
  return participant;
}

static PurpleMediaBackendFs2Stream *get_stream(PurpleMediaBackendFs2 *self,const gchar *sess_id,const gchar *name)
{
  PurpleMediaBackendFs2Private *priv;
  GList *streams;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_fs2_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND_FS2(self)");
      return 0;
    };
  }while (0);
  priv = ((PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)self),purple_media_backend_fs2_get_type())));
  streams = (priv -> streams);
  for (; streams != 0; streams = ((streams != 0)?( *((GList *)streams)).next : ((struct _GList *)((void *)0)))) {
    PurpleMediaBackendFs2Stream *stream = (streams -> data);
    if (!(strcmp(( *(stream -> session)).id,sess_id) != 0) && !(strcmp((stream -> participant),name) != 0)) 
      return stream;
  }
  return 0;
}

static GList *get_streams(PurpleMediaBackendFs2 *self,const gchar *sess_id,const gchar *name)
{
  PurpleMediaBackendFs2Private *priv;
  GList *streams;
  GList *ret = (GList *)((void *)0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_fs2_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND_FS2(self)");
      return 0;
    };
  }while (0);
  priv = ((PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)self),purple_media_backend_fs2_get_type())));
  streams = (priv -> streams);
  for (; streams != 0; streams = ((streams != 0)?( *((GList *)streams)).next : ((struct _GList *)((void *)0)))) {{
      PurpleMediaBackendFs2Stream *stream = (streams -> data);
      if ((sess_id != ((const gchar *)((void *)0))) && (strcmp(( *(stream -> session)).id,sess_id) != 0)) 
        continue; 
      else if ((name != ((const gchar *)((void *)0))) && (strcmp((stream -> participant),name) != 0)) 
        continue; 
      else 
        ret = g_list_prepend(ret,stream);
    }
  }
  ret = g_list_reverse(ret);
  return ret;
}

static PurpleMediaBackendFs2Session *get_session_from_fs_stream(PurpleMediaBackendFs2 *self,FsStream *stream)
{
  PurpleMediaBackendFs2Private *priv = (PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)self),purple_media_backend_fs2_get_type()));
  FsSession *fssession;
  GList *values;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_fs2_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND_FS2(self)");
      return 0;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)stream;
      GType __t = fs_stream_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"FS_IS_STREAM(stream)");
      return 0;
    };
  }while (0);
  g_object_get(stream,"session",&fssession,((void *)((void *)0)));
  values = g_hash_table_get_values((priv -> sessions));
  for (; values != 0; values = g_list_delete_link(values,values)) {
    PurpleMediaBackendFs2Session *session = (values -> data);
    if ((session -> session) == fssession) {
      g_list_free(values);
      g_object_unref(fssession);
      return session;
    }
  }
  g_object_unref(fssession);
  return 0;
}

static gdouble gst_msg_db_to_percent(GstMessage *msg,gchar *value_name)
{
  const GValue *list;
  const GValue *value;
  gdouble value_db;
  gdouble percent;
  list = gst_structure_get_value(gst_message_get_structure(msg),value_name);
  value = gst_value_list_get_value(list,0);
  value_db = g_value_get_double(value);
  percent = pow(10,(value_db / 20));
  return (percent > 1.0)?1.0 : percent;
}

static void gst_handle_message_element(GstBus *bus,GstMessage *msg,PurpleMediaBackendFs2 *self)
{
  PurpleMediaBackendFs2Private *priv = (PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)self),purple_media_backend_fs2_get_type()));
  GstElement *src = (GstElement *)(g_type_check_instance_cast(((GTypeInstance *)( *((GstMessage *)msg)).src),gst_element_get_type()));
  static guint level_id = 0;
  if (level_id == 0) 
    level_id = g_signal_lookup("level",purple_media_get_type());
  if (gst_structure_has_name((msg -> structure),"level") != 0) {
    GstElement *src = (GstElement *)(g_type_check_instance_cast(((GTypeInstance *)( *((GstMessage *)msg)).src),gst_element_get_type()));
    gchar *name;
    gchar *participant = (gchar *)((void *)0);
    PurpleMediaBackendFs2Session *session = (PurpleMediaBackendFs2Session *)((void *)0);
    gdouble percent;
    if (!((({
      GTypeInstance *__inst = (GTypeInstance *)(priv -> media);
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) || (((GstElement *)( *((GstObject *)src)).parent) != (priv -> confbin))) 
      return ;
    name = gst_object_get_name(((GstObject *)src));
    if (!(strncmp(name,"sendlevel_",10) != 0)) {
      session = get_session(self,(name + 10));
      if ((priv -> silence_threshold) > 0) {
        percent = gst_msg_db_to_percent(msg,"decay");
        g_object_set((session -> srcvalve),"drop",(percent < (priv -> silence_threshold)),((void *)((void *)0)));
      }
    }
    g_free(name);
    if (!(g_signal_has_handler_pending((priv -> media),level_id,0,0) != 0)) 
      return ;
    if (!(session != 0)) {
      GList *iter = (priv -> streams);
      PurpleMediaBackendFs2Stream *stream;
{
        for (; iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
          stream = (iter -> data);
          if ((stream -> level) == src) {
            session = (stream -> session);
            participant = (stream -> participant);
            break; 
          }
        }
      }
    }
    if (!(session != 0)) 
      return ;
    percent = gst_msg_db_to_percent(msg,"rms");
    g_signal_emit((priv -> media),level_id,0,(session -> id),participant,percent);
    return ;
  }
  if ((!((({
    GTypeInstance *__inst = (GTypeInstance *)src;
    GType __t = fs_conference_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0) || !(((
{
    GTypeInstance *__inst = (GTypeInstance *)self;
    GType __t = purple_media_backend_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) || ((priv -> conference) != ((FsConference *)(g_type_check_instance_cast(((GTypeInstance *)src),fs_conference_get_type()))))) 
    return ;
#ifdef HAVE_FARSIGHT
#else
  if (gst_structure_has_name((msg -> structure),"farstream-error") != 0) {
#endif
    FsError error_no;
    gst_structure_get_enum((msg -> structure),"error-no",fs_error_get_type(),((gint *)(&error_no)));
    switch(error_no){
      case FS_ERROR_NO_CODECS:
{
        purple_media_error((priv -> media),((const char *)(dgettext("pidgin","No codecs found. Install some GStreamer codecs found in GStreamer plugins packages."))));
        purple_media_end((priv -> media),0,0);
        break; 
      }
#ifdef HAVE_FARSIGHT
/*
				 * Unknown CName is only a problem for the
				 * multicast transmitter which isn't used.
				 * It is also deprecated.
				 */
#endif
      default:
{
        purple_debug_error("backend-fs2","farstream-error: %i: %s\n",error_no,gst_structure_get_string((msg -> structure),"error-msg"));
#ifdef HAVE_FARSIGHT
#else
#endif
        break; 
      }
    }
    if (error_no < 100) {
#ifdef HAVE_FARSIGHT
#else
      purple_media_error((priv -> media),((const char *)(dgettext("pidgin","A non-recoverable Farstream error has occurred."))));
#endif
      purple_media_end((priv -> media),0,0);
    }
  }
  else if (gst_structure_has_name((msg -> structure),"farstream-new-local-candidate") != 0) 
#ifdef HAVE_FARSIGHT
#else
{
#endif
    const GValue *value;
    FsStream *stream;
    FsCandidate *local_candidate;
    PurpleMediaCandidate *candidate;
    FsParticipant *participant;
    PurpleMediaBackendFs2Session *session;
    PurpleMediaBackendFs2Stream *media_stream;
    gchar *name;
    value = gst_structure_get_value((msg -> structure),"stream");
    stream = (g_value_get_object(value));
    value = gst_structure_get_value((msg -> structure),"candidate");
    local_candidate = (g_value_get_boxed(value));
    session = get_session_from_fs_stream(self,stream);
    purple_debug_info("backend-fs2","got new local candidate: %s\n",(local_candidate -> foundation));
    g_object_get(stream,"participant",&participant,((void *)((void *)0)));
    g_object_get(participant,"cname",&name,((void *)((void *)0)));
    g_object_unref(participant);
    media_stream = get_stream(self,(session -> id),name);
    media_stream -> local_candidates = g_list_append((media_stream -> local_candidates),(fs_candidate_copy(local_candidate)));
    candidate = candidate_from_fs(local_candidate);
    g_signal_emit_by_name(self,"new-candidate",(session -> id),name,candidate);
    g_object_unref(candidate);
  }
  else if (gst_structure_has_name((msg -> structure),"farstream-local-candidates-prepared") != 0) 
#ifdef HAVE_FARSIGHT
#else
{
#endif
    const GValue *value;
    FsStream *stream;
    FsParticipant *participant;
    PurpleMediaBackendFs2Session *session;
    gchar *name;
    value = gst_structure_get_value((msg -> structure),"stream");
    stream = (g_value_get_object(value));
    session = get_session_from_fs_stream(self,stream);
    g_object_get(stream,"participant",&participant,((void *)((void *)0)));
    g_object_get(participant,"cname",&name,((void *)((void *)0)));
    g_object_unref(participant);
    g_signal_emit_by_name(self,"candidates-prepared",(session -> id),name);
  }
  else if (gst_structure_has_name((msg -> structure),"farstream-new-active-candidate-pair") != 0) 
#ifdef HAVE_FARSIGHT
#else
{
#endif
    const GValue *value;
    FsStream *stream;
    FsCandidate *local_candidate;
    FsCandidate *remote_candidate;
    FsParticipant *participant;
    PurpleMediaBackendFs2Session *session;
    PurpleMediaCandidate *lcandidate;
    PurpleMediaCandidate *rcandidate;
    gchar *name;
    value = gst_structure_get_value((msg -> structure),"stream");
    stream = (g_value_get_object(value));
    value = gst_structure_get_value((msg -> structure),"local-candidate");
    local_candidate = (g_value_get_boxed(value));
    value = gst_structure_get_value((msg -> structure),"remote-candidate");
    remote_candidate = (g_value_get_boxed(value));
    g_object_get(stream,"participant",&participant,((void *)((void *)0)));
    g_object_get(participant,"cname",&name,((void *)((void *)0)));
    g_object_unref(participant);
    session = get_session_from_fs_stream(self,stream);
    lcandidate = candidate_from_fs(local_candidate);
    rcandidate = candidate_from_fs(remote_candidate);
    g_signal_emit_by_name(self,"active-candidate-pair",(session -> id),name,lcandidate,rcandidate);
    g_object_unref(lcandidate);
    g_object_unref(rcandidate);
  }
  else if (gst_structure_has_name((msg -> structure),"farstream-recv-codecs-changed") != 0) 
#ifdef HAVE_FARSIGHT
#else
{
#endif
    const GValue *value;
    GList *codecs;
    FsCodec *codec;
    value = gst_structure_get_value((msg -> structure),"codecs");
    codecs = (g_value_get_boxed(value));
    codec = (codecs -> data);
    purple_debug_info("backend-fs2","farstream-recv-codecs-changed: %s\n",(codec -> encoding_name));
#ifdef HAVE_FARSIGHT
#else
#endif
  }
  else if (gst_structure_has_name((msg -> structure),"farstream-component-state-changed") != 0) 
#ifdef HAVE_FARSIGHT
#else
{
#endif
    const GValue *value;
    FsStreamState fsstate;
    guint component;
    const gchar *state;
    value = gst_structure_get_value((msg -> structure),"state");
    fsstate = (g_value_get_enum(value));
    value = gst_structure_get_value((msg -> structure),"component");
    component = g_value_get_uint(value);
    switch(fsstate){
      case FS_STREAM_STATE_FAILED:
{
        state = "FAILED";
        break; 
      }
      case FS_STREAM_STATE_DISCONNECTED:
{
        state = "DISCONNECTED";
        break; 
      }
      case FS_STREAM_STATE_GATHERING:
{
        state = "GATHERING";
        break; 
      }
      case FS_STREAM_STATE_CONNECTING:
{
        state = "CONNECTING";
        break; 
      }
      case FS_STREAM_STATE_CONNECTED:
{
        state = "CONNECTED";
        break; 
      }
      case FS_STREAM_STATE_READY:
{
        state = "READY";
        break; 
      }
      default:
{
        state = "UNKNOWN";
        break; 
      }
    }
    purple_debug_info("backend-fs2","farstream-component-state-changed: component: %u state: %s\n",component,state);
#ifdef HAVE_FARSIGHT
#else
#endif
  }
  else if (gst_structure_has_name((msg -> structure),"farstream-send-codec-changed") != 0) 
#ifdef HAVE_FARSIGHT
#else
{
#endif
    const GValue *value;
    FsCodec *codec;
    gchar *codec_str;
    value = gst_structure_get_value((msg -> structure),"codec");
    codec = (g_value_get_boxed(value));
    codec_str = fs_codec_to_string(codec);
    purple_debug_info("backend-fs2","farstream-send-codec-changed: codec: %s\n",codec_str);
#ifdef HAVE_FARSIGHT
#else
#endif
    g_free(codec_str);
  }
  else if (gst_structure_has_name((msg -> structure),"farstream-codecs-changed") != 0) 
#ifdef HAVE_FARSIGHT
#else
{
#endif
    const GValue *value;
    FsSession *fssession;
    GList *sessions;
    value = gst_structure_get_value((msg -> structure),"session");
    fssession = (g_value_get_object(value));
    sessions = g_hash_table_get_values((priv -> sessions));
{
      for (; sessions != 0; sessions = g_list_delete_link(sessions,sessions)) {{
          PurpleMediaBackendFs2Session *session = (sessions -> data);
          gchar *session_id;
          if ((session -> session) != fssession) 
            continue; 
          session_id = g_strdup((session -> id));
          g_signal_emit_by_name(self,"codecs-changed",session_id);
          g_free(session_id);
          g_list_free(sessions);
          break; 
        }
      }
    }
  }
}

static void gst_handle_message_error(GstBus *bus,GstMessage *msg,PurpleMediaBackendFs2 *self)
{
  PurpleMediaBackendFs2Private *priv = (PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)self),purple_media_backend_fs2_get_type()));
  GstElement *element = (GstElement *)(g_type_check_instance_cast(((GTypeInstance *)( *((GstMessage *)msg)).src),gst_element_get_type()));
  GstElement *lastElement = (GstElement *)((void *)0);
  GList *sessions;
  GError *error = (GError *)((void *)0);
  gchar *debug_msg = (gchar *)((void *)0);
  gst_message_parse_error(msg,&error,&debug_msg);
  purple_debug_error("backend-fs2","gst error %s\ndebugging: %s\n",(error -> message),debug_msg);
  g_error_free(error);
  g_free(debug_msg);
{
    while((element != 0) && !((({
      GTypeInstance *__inst = (GTypeInstance *)element;
      GType __t = gst_pipeline_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0))
{
      if (element == (priv -> confbin)) 
        break; 
      lastElement = element;
      element = ((GstElement *)( *((GstObject *)element)).parent);
    }
  }
  if (!(element != 0) || !((({
    GTypeInstance *__inst = (GTypeInstance *)element;
    GType __t = gst_pipeline_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) 
    return ;
  sessions = purple_media_get_session_ids((priv -> media));
{
    for (; sessions != 0; sessions = g_list_delete_link(sessions,sessions)) {
      if (purple_media_get_src((priv -> media),(sessions -> data)) != lastElement) 
        continue; 
      if (((purple_media_get_session_type((priv -> media),(sessions -> data))) & PURPLE_MEDIA_AUDIO) != 0U) 
        purple_media_error((priv -> media),((const char *)(dgettext("pidgin","Error with your microphone"))));
      else 
        purple_media_error((priv -> media),((const char *)(dgettext("pidgin","Error with your webcam"))));
      break; 
    }
  }
  g_list_free(sessions);
  purple_media_error((priv -> media),((const char *)(dgettext("pidgin","Conference error"))));
  purple_media_end((priv -> media),0,0);
}

static gboolean gst_bus_cb(GstBus *bus,GstMessage *msg,PurpleMediaBackendFs2 *self)
{
  switch(( *((GstMessage *)msg)).type){
    case GST_MESSAGE_ELEMENT:
{
      gst_handle_message_element(bus,msg,self);
      break; 
    }
    case GST_MESSAGE_ERROR:
{
      gst_handle_message_error(bus,msg,self);
      break; 
    }
    default:
{
      break; 
    }
  }
  return (!0);
}

static void remove_element(GstElement *element)
{
  if (element != 0) {
    gst_element_set_locked_state(element,(!0));
    gst_element_set_state(element,GST_STATE_NULL);
    gst_bin_remove(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)((GstElement *)( *((GstObject *)element)).parent)),gst_bin_get_type()))),element);
  }
}

static void state_changed_cb(PurpleMedia *media,PurpleMediaState state,gchar *sid,gchar *name,PurpleMediaBackendFs2 *self)
{
  if (state == PURPLE_MEDIA_STATE_END) {
    PurpleMediaBackendFs2Private *priv = (PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)self),purple_media_backend_fs2_get_type()));
    if ((sid != 0) && (name != 0)) {
      PurpleMediaBackendFs2Stream *stream = get_stream(self,sid,name);
      gst_object_unref((stream -> stream));
      priv -> streams = g_list_remove((priv -> streams),stream);
      remove_element((stream -> src));
      remove_element((stream -> tee));
      remove_element((stream -> volume));
      remove_element((stream -> level));
      remove_element((stream -> fakesink));
      remove_element((stream -> queue));
      free_stream(stream);
    }
    else if ((sid != 0) && !(name != 0)) {
      PurpleMediaBackendFs2Session *session = get_session(self,sid);
      GstPad *pad;
      g_object_get((session -> session),"sink-pad",&pad,((void *)((void *)0)));
      gst_pad_unlink(( *((GstPad *)pad)).peer,pad);
      gst_object_unref(pad);
      gst_object_unref((session -> session));
      g_hash_table_remove((priv -> sessions),(session -> id));
      pad = gst_pad_get_peer((session -> srcpad));
      gst_element_remove_pad(((GstElement *)( *((GstObject *)pad)).parent),pad);
      gst_object_unref(pad);
      gst_object_unref((session -> srcpad));
      remove_element((session -> srcvalve));
      remove_element((session -> tee));
      free_session(session);
    }
    purple_media_manager_remove_output_windows(purple_media_get_manager(media),media,sid,name);
  }
}

static void stream_info_cb(PurpleMedia *media,PurpleMediaInfoType type,gchar *sid,gchar *name,gboolean local,PurpleMediaBackendFs2 *self)
{
  if (((type == PURPLE_MEDIA_INFO_ACCEPT) && (sid != ((gchar *)((void *)0)))) && (name != ((gchar *)((void *)0)))) {
    PurpleMediaBackendFs2Stream *stream = get_stream(self,sid,name);
    GError *err = (GError *)((void *)0);
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(stream -> stream)),((GType )(20 << 2))))),"direction",session_type_to_fs_stream_direction(( *(stream -> session)).type),((void *)((void *)0)));
    if (((stream -> remote_candidates) == ((GList *)((void *)0))) || (purple_media_is_initiator(media,sid,name) != 0)) 
      return ;
#ifdef HAVE_FARSIGHT
#else
    if ((stream -> supports_add) != 0) 
      fs_stream_add_remote_candidates((stream -> stream),(stream -> remote_candidates),&err);
    else 
      fs_stream_force_remote_candidates((stream -> stream),(stream -> remote_candidates),&err);
#endif
    if (err == ((GError *)((void *)0))) 
      return ;
    purple_debug_error("backend-fs2","Error adding remote candidates: %s\n",(err -> message));
    g_error_free(err);
  }
  else if ((local == !0) && ((type == PURPLE_MEDIA_INFO_MUTE) || (type == PURPLE_MEDIA_INFO_UNMUTE))) {
    PurpleMediaBackendFs2Private *priv = (PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)self),purple_media_backend_fs2_get_type()));
    gboolean active = (type == PURPLE_MEDIA_INFO_MUTE);
    GList *sessions;
    if (sid == ((gchar *)((void *)0))) 
      sessions = g_hash_table_get_values((priv -> sessions));
    else 
      sessions = g_list_prepend(0,(get_session(self,sid)));
    purple_debug_info("media","Turning mute %s\n",((active != 0)?"on" : "off"));
    for (; sessions != 0; sessions = g_list_delete_link(sessions,sessions)) {
      PurpleMediaBackendFs2Session *session = (sessions -> data);
      if (((session -> type) & PURPLE_MEDIA_SEND_AUDIO) != 0U) {
        gchar *name = g_strdup_printf("volume_%s",(session -> id));
        GstElement *volume = gst_bin_get_by_name(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> confbin)),gst_bin_get_type()))),name);
        g_free(name);
        g_object_set(volume,"mute",active,((void *)((void *)0)));
      }
    }
  }
  else if ((local == !0) && ((type == PURPLE_MEDIA_INFO_HOLD) || (type == PURPLE_MEDIA_INFO_UNHOLD))) {
    gboolean active = (type == PURPLE_MEDIA_INFO_HOLD);
    GList *streams = get_streams(self,sid,name);
    for (; streams != 0; streams = g_list_delete_link(streams,streams)) {
      PurpleMediaBackendFs2Stream *stream = (streams -> data);
      if ((( *(stream -> session)).type & PURPLE_MEDIA_SEND_AUDIO) != 0U) {
        g_object_set((stream -> stream),"direction",session_type_to_fs_stream_direction((( *(stream -> session)).type & (((active != 0)?~PURPLE_MEDIA_SEND_AUDIO : PURPLE_MEDIA_AUDIO)))),((void *)((void *)0)));
      }
    }
  }
  else if ((local == !0) && ((type == PURPLE_MEDIA_INFO_PAUSE) || (type == PURPLE_MEDIA_INFO_UNPAUSE))) {
    gboolean active = (type == PURPLE_MEDIA_INFO_PAUSE);
    GList *streams = get_streams(self,sid,name);
    for (; streams != 0; streams = g_list_delete_link(streams,streams)) {
      PurpleMediaBackendFs2Stream *stream = (streams -> data);
      if ((( *(stream -> session)).type & PURPLE_MEDIA_SEND_VIDEO) != 0U) {
        g_object_set((stream -> stream),"direction",session_type_to_fs_stream_direction((( *(stream -> session)).type & (((active != 0)?~PURPLE_MEDIA_SEND_VIDEO : PURPLE_MEDIA_VIDEO)))),((void *)((void *)0)));
      }
    }
  }
}

static gboolean init_conference(PurpleMediaBackendFs2 *self)
{
  PurpleMediaBackendFs2Private *priv = (PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)self),purple_media_backend_fs2_get_type()));
  GstElement *pipeline;
  GstBus *bus;
  gchar *name;
#ifndef HAVE_FARSIGHT
  GKeyFile *default_props;
#endif
  priv -> conference = ((FsConference *)(g_type_check_instance_cast(((GTypeInstance *)(gst_element_factory_make((priv -> conference_type),0))),fs_conference_get_type())));
  if ((priv -> conference) == ((FsConference *)((void *)0))) {
    purple_debug_error("backend-fs2","Conference == NULL\n");
    return 0;
  }
  if (purple_account_get_silence_suppression((purple_media_get_account((priv -> media)))) != 0) 
    priv -> silence_threshold = ((purple_prefs_get_int("/purple/media/audio/silence_threshold")) / 100.0);
  else 
    priv -> silence_threshold = 0;
  pipeline = purple_media_manager_get_pipeline(purple_media_get_manager((priv -> media)));
  if (pipeline == ((GstElement *)((void *)0))) {
    purple_debug_error("backend-fs2","Couldn\'t retrieve pipeline.\n");
    return 0;
  }
  name = g_strdup_printf("conf_%p",(priv -> conference));
  priv -> confbin = gst_bin_new(name);
  if ((priv -> confbin) == ((GstElement *)((void *)0))) {
    purple_debug_error("backend-fs2","Couldn\'t create confbin.\n");
    return 0;
  }
  g_free(name);
  bus = gst_pipeline_get_bus(((GstPipeline *)(g_type_check_instance_cast(((GTypeInstance *)pipeline),gst_pipeline_get_type()))));
  if (bus == ((GstBus *)((void *)0))) {
    purple_debug_error("backend-fs2","Couldn\'t get the pipeline\'s bus.\n");
    return 0;
  }
#ifndef HAVE_FARSIGHT
  default_props = fs_utils_get_default_element_properties(((GstElement *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> conference)),gst_element_get_type()))));
  if (default_props != ((GKeyFile *)((void *)0))) {
    priv -> notifier = fs_element_added_notifier_new();
    fs_element_added_notifier_add((priv -> notifier),((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> confbin)),gst_bin_get_type()))));
    fs_element_added_notifier_set_properties_from_keyfile((priv -> notifier),default_props);
  }
#endif
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)bus),((GType )(20 << 2))))),"message",((GCallback )gst_bus_cb),self,0,((GConnectFlags )0));
  gst_object_unref(bus);
  if (!(gst_bin_add(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)pipeline),gst_bin_get_type()))),((GstElement *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> confbin)),gst_element_get_type())))) != 0)) {
    purple_debug_error("backend-fs2","Couldn\'t add confbin element to the pipeline\n");
    return 0;
  }
  if (!(gst_bin_add(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> confbin)),gst_bin_get_type()))),((GstElement *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> conference)),gst_element_get_type())))) != 0)) {
    purple_debug_error("backend-fs2","Couldn\'t add conference element to the confbin\n");
    return 0;
  }
  if ((gst_element_set_state(((GstElement *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> confbin)),gst_element_get_type()))),GST_STATE_PLAYING)) == GST_STATE_CHANGE_FAILURE) {
    purple_debug_error("backend-fs2","Failed to start conference.\n");
    return 0;
  }
  return (!0);
}

static void gst_element_added_cb(FsElementAddedNotifier *self,GstBin *bin,GstElement *element,gpointer user_data)
{
/*
	 * Hack to make H264 work with Gmail video.
	 */
  if (!(strncmp(( *((GstObject *)element)).name,"x264",4) != 0)) {
    g_object_set(((GstObject *)(g_type_check_instance_cast(((GTypeInstance *)element),gst_object_get_type()))),"cabac",0,((void *)((void *)0)));
  }
}

static gboolean create_src(PurpleMediaBackendFs2 *self,const gchar *sess_id,PurpleMediaSessionType type)
{
  PurpleMediaBackendFs2Private *priv = (PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)self),purple_media_backend_fs2_get_type()));
  PurpleMediaBackendFs2Session *session;
  PurpleMediaSessionType session_type;
  FsMediaType media_type = session_type_to_fs_media_type(type);
  FsStreamDirection type_direction = session_type_to_fs_stream_direction(type);
  GstElement *src;
  GstPad *sinkpad;
  GstPad *srcpad;
  GstPad *ghost = (GstPad *)((void *)0);
  if ((type_direction & FS_DIRECTION_SEND) == 0) 
    return (!0);
  session_type = session_type_from_fs(media_type,FS_DIRECTION_SEND);
  src = purple_media_manager_get_element(purple_media_get_manager((priv -> media)),session_type,(priv -> media),sess_id,0);
  if (!((({
    GTypeInstance *__inst = (GTypeInstance *)src;
    GType __t = gst_element_get_type();
    gboolean __r;
    if (!(__inst != 0)) 
      __r = 0;
    else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
      __r = (!0);
    else 
      __r = g_type_check_instance_is_a(__inst,__t);
    __r;
  })) != 0)) 
{
    purple_debug_error("backend-fs2","Error creating src for session %s\n",sess_id);
    return 0;
  }
  session = get_session(self,sess_id);
  if (session == ((PurpleMediaBackendFs2Session *)((void *)0))) {
    purple_debug_warning("backend-fs2","purple_media_set_src: trying to set src on non-existent session\n");
    return 0;
  }
  if ((session -> src) != 0) 
    gst_object_unref((session -> src));
  session -> src = src;
  gst_element_set_locked_state((session -> src),(!0));
  session -> tee = gst_element_factory_make("tee",0);
  gst_bin_add(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> confbin)),gst_bin_get_type()))),(session -> tee));
/* This supposedly isn't necessary, but it silences some warnings */
  if (((GstElement *)( *((GstObject *)(priv -> confbin))).parent) == ((GstElement *)( *((GstObject *)(session -> src))).parent)) {
    GstPad *pad = gst_element_get_static_pad((session -> tee),"sink");
    ghost = gst_ghost_pad_new(0,pad);
    gst_object_unref(pad);
    gst_pad_set_active(ghost,(!0));
    gst_element_add_pad((priv -> confbin),ghost);
  }
  gst_element_set_state((session -> tee),GST_STATE_PLAYING);
  gst_element_link((session -> src),(priv -> confbin));
  if (ghost != 0) 
    session -> srcpad = gst_pad_get_peer(ghost);
  g_object_get((session -> session),"sink-pad",&sinkpad,((void *)((void *)0)));
  if (((session -> type) & PURPLE_MEDIA_SEND_AUDIO) != 0U) {
    gchar *name = g_strdup_printf("volume_%s",(session -> id));
    GstElement *level;
    GstElement *volume = gst_element_factory_make("volume",name);
    double input_volume = ((purple_prefs_get_int("/purple/media/audio/volume/input")) / 10.0);
    g_free(name);
    name = g_strdup_printf("sendlevel_%s",(session -> id));
    level = gst_element_factory_make("level",name);
    g_free(name);
    session -> srcvalve = gst_element_factory_make("valve",0);
    gst_bin_add(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> confbin)),gst_bin_get_type()))),volume);
    gst_bin_add(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> confbin)),gst_bin_get_type()))),level);
    gst_bin_add(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> confbin)),gst_bin_get_type()))),(session -> srcvalve));
    gst_element_set_state(level,GST_STATE_PLAYING);
    gst_element_set_state(volume,GST_STATE_PLAYING);
    gst_element_set_state((session -> srcvalve),GST_STATE_PLAYING);
    gst_element_link(level,(session -> srcvalve));
    gst_element_link(volume,level);
    gst_element_link((session -> tee),volume);
    srcpad = gst_element_get_static_pad((session -> srcvalve),"src");
    g_object_set(volume,"volume",input_volume,((void *)((void *)0)));
  }
  else {
    srcpad = gst_element_get_request_pad((session -> tee),"src%d");
  }
  purple_debug_info("backend-fs2","connecting pad: %s\n",(((gst_pad_link(srcpad,sinkpad)) == GST_PAD_LINK_OK)?"success" : "failure"));
  gst_element_set_locked_state((session -> src),0);
  gst_object_unref((session -> src));
  gst_object_unref(sinkpad);
  gst_element_set_state((session -> src),GST_STATE_PLAYING);
  purple_media_manager_create_output_window(purple_media_get_manager((priv -> media)),(priv -> media),sess_id,0);
  return (!0);
}

static gboolean create_session(PurpleMediaBackendFs2 *self,const gchar *sess_id,PurpleMediaSessionType type,gboolean initiator,const gchar *transmitter)
{
  PurpleMediaBackendFs2Private *priv = (PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)self),purple_media_backend_fs2_get_type()));
  PurpleMediaBackendFs2Session *session;
  GError *err = (GError *)((void *)0);
  GList *codec_conf = (GList *)((void *)0);
  GList *iter = (GList *)((void *)0);
  gchar *filename = (gchar *)((void *)0);
  gboolean is_nice = !(strcmp(transmitter,"nice") != 0);
  session = ((PurpleMediaBackendFs2Session *)(g_malloc0_n(1,(sizeof(PurpleMediaBackendFs2Session )))));
  session -> session = fs_conference_new_session((priv -> conference),session_type_to_fs_media_type(type),&err);
  if (err != ((GError *)((void *)0))) {
    purple_media_error((priv -> media),((const char *)(dgettext("pidgin","Error creating session: %s"))),(err -> message));
    g_error_free(err);
    g_free(session);
    return 0;
  }
  filename = g_build_filename(purple_user_dir(),"fs-codec.conf",((void *)((void *)0)));
  codec_conf = fs_codec_list_from_keyfile(filename,&err);
  g_free(filename);
  if (err != ((GError *)((void *)0))) {
    if ((err -> code) == 4) 
      purple_debug_info("backend-fs2","Couldn\'t read fs-codec.conf: %s\n",(err -> message));
    else 
      purple_debug_error("backend-fs2","Error reading fs-codec.conf: %s\n",(err -> message));
    g_error_free(err);
  }
{
/*
	 * Add SPEEX if the configuration file doesn't exist or
	 * there isn't a speex entry.
	 */
    for (iter = codec_conf; iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
      FsCodec *codec = (iter -> data);
      if (!(g_ascii_strcasecmp((codec -> encoding_name),"speex") != 0)) 
        break; 
    }
  }
  if (iter == ((GList *)((void *)0))) {
    codec_conf = g_list_prepend(codec_conf,(fs_codec_new(-1,"SPEEX",FS_MEDIA_TYPE_AUDIO,8000)));
    codec_conf = g_list_prepend(codec_conf,(fs_codec_new(-1,"SPEEX",FS_MEDIA_TYPE_AUDIO,16000)));
  }
  fs_session_set_codec_preferences((session -> session),codec_conf,0);
  fs_codec_list_destroy(codec_conf);
/*
	 * Removes a 5-7 second delay before
	 * receiving the src-pad-added signal.
	 * Only works for non-multicast FsRtpSessions.
	 */
  if (!(!(strcmp(transmitter,"multicast") != 0))) 
    g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(session -> session)),((GType )(20 << 2))))),"no-rtcp-timeout",0,((void *)((void *)0)));
/*
	 * Hack to make x264 work with Gmail video.
	 */
  if ((is_nice != 0) && !(strcmp(sess_id,"google-video") != 0)) {
    FsElementAddedNotifier *notifier = fs_element_added_notifier_new();
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)notifier),((GType )(20 << 2))))),"element-added",((GCallback )gst_element_added_cb),0,0,((GConnectFlags )0));
    fs_element_added_notifier_add(notifier,((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> conference)),gst_bin_get_type()))));
  }
  session -> id = g_strdup(sess_id);
  session -> backend = self;
  session -> type = type;
  if (!((priv -> sessions) != 0)) {
    purple_debug_info("backend-fs2","Creating hash table for sessions\n");
    priv -> sessions = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,0);
  }
  g_hash_table_insert((priv -> sessions),(g_strdup((session -> id))),session);
  if (!(create_src(self,sess_id,type) != 0)) {
    purple_debug_info("backend-fs2","Error creating the src\n");
    return 0;
  }
  return (!0);
}

static void free_session(PurpleMediaBackendFs2Session *session)
{
  g_free((session -> id));
  g_free(session);
}

static gboolean create_participant(PurpleMediaBackendFs2 *self,const gchar *name)
{
  PurpleMediaBackendFs2Private *priv = (PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)self),purple_media_backend_fs2_get_type()));
  FsParticipant *participant;
  GError *err = (GError *)((void *)0);
  participant = fs_conference_new_participant((priv -> conference),&err);
#ifdef HAVE_FARSIGHT
#else
#endif
  if (err != 0) {
    purple_debug_error("backend-fs2","Error creating participant: %s\n",(err -> message));
    g_error_free(err);
    return 0;
  }
#ifndef HAVE_FARSIGHT
  if (g_object_class_find_property(((GObjectClass *)( *((GTypeInstance *)participant)).g_class),"cname") != 0) {
    g_object_set(participant,"cname",name,((void *)((void *)0)));
  }
#endif
  if (!((priv -> participants) != 0)) {
    purple_debug_info("backend-fs2","Creating hash table for participants\n");
    priv -> participants = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_object_unref);
  }
  g_hash_table_insert((priv -> participants),(g_strdup(name)),participant);
  return (!0);
}

static gboolean src_pad_added_cb_cb(PurpleMediaBackendFs2Stream *stream)
{
  PurpleMediaBackendFs2Private *priv;
  do {
    if (stream != ((PurpleMediaBackendFs2Stream *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"stream != NULL");
      return 0;
    };
  }while (0);
  priv = ((PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)( *(stream -> session)).backend),purple_media_backend_fs2_get_type())));
  stream -> connected_cb_id = 0;
  purple_media_manager_create_output_window(purple_media_get_manager((priv -> media)),(priv -> media),( *(stream -> session)).id,(stream -> participant));
  g_signal_emit_by_name((priv -> media),"state-changed",PURPLE_MEDIA_STATE_CONNECTED,( *(stream -> session)).id,(stream -> participant));
  return 0;
}

static void src_pad_added_cb(FsStream *fsstream,GstPad *srcpad,FsCodec *codec,PurpleMediaBackendFs2Stream *stream)
{
  PurpleMediaBackendFs2Private *priv;
  GstPad *sinkpad;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)fsstream;
      GType __t = fs_stream_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"FS_IS_STREAM(fsstream)");
      return ;
    };
  }while (0);
  do {
    if (stream != ((PurpleMediaBackendFs2Stream *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"stream != NULL");
      return ;
    };
  }while (0);
  priv = ((PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)( *(stream -> session)).backend),purple_media_backend_fs2_get_type())));
  if ((stream -> src) == ((GstElement *)((void *)0))) {
    GstElement *sink = (GstElement *)((void *)0);
    if ((codec -> media_type) == FS_MEDIA_TYPE_AUDIO) {
      double output_volume = ((purple_prefs_get_int("/purple/media/audio/volume/output")) / 10.0);
/*
			 * Should this instead be:
			 *  audioconvert ! audioresample ! liveadder !
			 *   audioresample ! audioconvert ! realsink
			 */
      stream -> queue = gst_element_factory_make("queue",0);
      stream -> volume = gst_element_factory_make("volume",0);
      g_object_set((stream -> volume),"volume",output_volume,((void *)((void *)0)));
      stream -> level = gst_element_factory_make("level",0);
      stream -> src = gst_element_factory_make("liveadder",0);
      sink = purple_media_manager_get_element(purple_media_get_manager((priv -> media)),PURPLE_MEDIA_RECV_AUDIO,(priv -> media),( *(stream -> session)).id,(stream -> participant));
      gst_bin_add(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> confbin)),gst_bin_get_type()))),(stream -> queue));
      gst_bin_add(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> confbin)),gst_bin_get_type()))),(stream -> volume));
      gst_bin_add(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> confbin)),gst_bin_get_type()))),(stream -> level));
      gst_bin_add(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> confbin)),gst_bin_get_type()))),sink);
      gst_element_set_state(sink,GST_STATE_PLAYING);
      gst_element_set_state((stream -> level),GST_STATE_PLAYING);
      gst_element_set_state((stream -> volume),GST_STATE_PLAYING);
      gst_element_set_state((stream -> queue),GST_STATE_PLAYING);
      gst_element_link((stream -> level),sink);
      gst_element_link((stream -> volume),(stream -> level));
      gst_element_link((stream -> queue),(stream -> volume));
      sink = (stream -> queue);
    }
    else if ((codec -> media_type) == FS_MEDIA_TYPE_VIDEO) {
      stream -> src = gst_element_factory_make("fsfunnel",0);
      sink = gst_element_factory_make("fakesink",0);
      g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)sink),((GType )(20 << 2))))),"async",0,((void *)((void *)0)));
      gst_bin_add(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> confbin)),gst_bin_get_type()))),sink);
      gst_element_set_state(sink,GST_STATE_PLAYING);
      stream -> fakesink = sink;
    }
    stream -> tee = gst_element_factory_make("tee",0);
    gst_bin_add_many(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> confbin)),gst_bin_get_type()))),(stream -> src),(stream -> tee),((void *)((void *)0)));
    gst_element_set_state((stream -> tee),GST_STATE_PLAYING);
    gst_element_set_state((stream -> src),GST_STATE_PLAYING);
    gst_element_link_many((stream -> src),(stream -> tee),sink,((void *)((void *)0)));
  }
  sinkpad = gst_element_get_request_pad((stream -> src),"sink%d");
  gst_pad_link(srcpad,sinkpad);
  gst_object_unref(sinkpad);
  stream -> connected_cb_id = purple_timeout_add(0,((GSourceFunc )src_pad_added_cb_cb),stream);
}

static GValueArray *append_relay_info(GValueArray *relay_info,const gchar *ip,gint port,const gchar *username,const gchar *password,const gchar *type)
{
  GValue value;
  GstStructure *turn_setup = gst_structure_new("relay-info","ip",((GType )(16 << 2)),ip,"port",((GType )(7 << 2)),port,"username",((GType )(16 << 2)),username,"password",((GType )(16 << 2)),password,"relay-type",((GType )(16 << 2)),type,((void *)((void *)0)));
  if (turn_setup != 0) {
    memset((&value),0,(sizeof(GValue )));
    g_value_init(&value,gst_structure_get_type());
    gst_value_set_structure(&value,turn_setup);
    relay_info = g_value_array_append(relay_info,(&value));
    gst_structure_free(turn_setup);
  }
  return relay_info;
}

static gboolean create_stream(PurpleMediaBackendFs2 *self,const gchar *sess_id,const gchar *who,PurpleMediaSessionType type,gboolean initiator,const gchar *transmitter,guint num_params,GParameter *params)
{
  PurpleMediaBackendFs2Private *priv = (PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)self),purple_media_backend_fs2_get_type()));
  GError *err = (GError *)((void *)0);
  FsStream *fsstream = (FsStream *)((void *)0);
  const gchar *stun_ip = purple_network_get_stun_ip();
  const gchar *turn_ip = purple_network_get_turn_ip();
  guint _num_params = num_params;
  GParameter *_params;
  FsStreamDirection type_direction = session_type_to_fs_stream_direction(type);
  PurpleMediaBackendFs2Session *session;
  PurpleMediaBackendFs2Stream *stream;
  FsParticipant *participant;
/* check if the prpl has already specified a relay-info
	  we need to do this to allow them to override when using non-standard
	  TURN modes, like Google f.ex. */
  gboolean got_turn_from_prpl = 0;
  int i;
  session = get_session(self,sess_id);
  if (session == ((PurpleMediaBackendFs2Session *)((void *)0))) {
    purple_debug_error("backend-fs2","Couldn\'t find session to create stream.\n");
    return 0;
  }
  participant = get_participant(self,who);
  if (participant == ((FsParticipant *)((void *)0))) {
    purple_debug_error("backend-fs2","Couldn\'t find participant to create stream.\n");
    return 0;
  }
#ifndef HAVE_FARSIGHT
  fsstream = fs_session_new_stream((session -> session),participant,(((initiator == !0)?type_direction : (type_direction & FS_DIRECTION_RECV))),&err);
  if (fsstream == ((FsStream *)((void *)0))) {
    if (err != 0) {
      purple_debug_error("backend-fs2","Error creating stream: %s\n",(((err != 0) && ((err -> message) != 0))?(err -> message) : "NULL"));
      g_error_free(err);
    }
    else 
      purple_debug_error("backend-fs2","Error creating stream\n");
    return 0;
  }
{
#endif
    for (i = 0; i < num_params; i++) {
      if (purple_strequal(params[i].name,"relay-info") != 0) {
        got_turn_from_prpl = (!0);
        break; 
      }
    }
  }
  _params = ((GParameter *)(g_malloc0_n((num_params + 3),(sizeof(GParameter )))));
  memcpy(_params,params,(sizeof(GParameter ) * num_params));
/* set the controlling mode parameter */
  _params[_num_params].name = "controlling-mode";
  g_value_init(&_params[_num_params].value,((GType )(5 << 2)));
  g_value_set_boolean(&_params[_num_params].value,initiator);
  ++_num_params;
  if (stun_ip != 0) {
    purple_debug_info("backend-fs2","Setting stun-ip on new stream: %s\n",stun_ip);
    _params[_num_params].name = "stun-ip";
    g_value_init(&_params[_num_params].value,((GType )(16 << 2)));
    g_value_set_string(&_params[_num_params].value,stun_ip);
    ++_num_params;
  }
  if (((turn_ip != 0) && !(strcmp("nice",transmitter) != 0)) && !(got_turn_from_prpl != 0)) {
    GValueArray *relay_info = g_value_array_new(0);
    gint port;
    const gchar *username = purple_prefs_get_string("/purple/network/turn_username");
    const gchar *password = purple_prefs_get_string("/purple/network/turn_password");
/* UDP */
    port = purple_prefs_get_int("/purple/network/turn_port");
    if (port > 0) {
      relay_info = append_relay_info(relay_info,turn_ip,port,username,password,"udp");
    }
/* TCP */
    port = purple_prefs_get_int("/purple/network/turn_port_tcp");
    if (port > 0) {
      relay_info = append_relay_info(relay_info,turn_ip,port,username,password,"tcp");
    }
/* TURN over SSL is only supported by libnice for Google's "psuedo" SSL mode
			at this time */
    purple_debug_info("backend-fs2","Setting relay-info on new stream\n");
    _params[_num_params].name = "relay-info";
    g_value_init(&_params[_num_params].value,g_value_array_get_type());
    g_value_set_boxed(&_params[_num_params].value,relay_info);
    g_value_array_free(relay_info);
    _num_params++;
  }
#ifdef HAVE_FARSIGHT
#else
  if (!(fs_stream_set_transmitter(fsstream,transmitter,_params,_num_params,&err) != 0)) {
    purple_debug_error("backend-fs2","Could not set transmitter %s: %s.\n",transmitter,(err -> message));
    g_clear_error(&err);
    g_free(_params);
    return 0;
  }
  g_free(_params);
#endif
  stream = ((PurpleMediaBackendFs2Stream *)(g_malloc0_n(1,(sizeof(PurpleMediaBackendFs2Stream )))));
  stream -> participant = g_strdup(who);
  stream -> session = session;
  stream -> stream = fsstream;
#ifndef HAVE_FARSIGHT
  stream -> supports_add = !(strcmp(transmitter,"nice") != 0);
#endif
  priv -> streams = g_list_append((priv -> streams),stream);
  g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)fsstream),((GType )(20 << 2))))),"src-pad-added",((GCallback )src_pad_added_cb),stream,0,((GConnectFlags )0));
  return (!0);
}

static void free_stream(PurpleMediaBackendFs2Stream *stream)
{
/* Remove the connected_cb timeout */
  if ((stream -> connected_cb_id) != 0) 
    purple_timeout_remove((stream -> connected_cb_id));
  g_free((stream -> participant));
  if ((stream -> local_candidates) != 0) 
    fs_candidate_list_destroy((stream -> local_candidates));
  if ((stream -> remote_candidates) != 0) 
    fs_candidate_list_destroy((stream -> remote_candidates));
  g_free(stream);
}

static gboolean purple_media_backend_fs2_add_stream(PurpleMediaBackend *self,const gchar *sess_id,const gchar *who,PurpleMediaSessionType type,gboolean initiator,const gchar *transmitter,guint num_params,GParameter *params)
{
  PurpleMediaBackendFs2 *backend = (PurpleMediaBackendFs2 *)(g_type_check_instance_cast(((GTypeInstance *)self),purple_media_backend_fs2_get_type()));
  PurpleMediaBackendFs2Private *priv = (PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)backend),purple_media_backend_fs2_get_type()));
  PurpleMediaBackendFs2Stream *stream;
  if (((priv -> conference) == ((FsConference *)((void *)0))) && !(init_conference(backend) != 0)) {
    purple_debug_error("backend-fs2","Error initializing the conference.\n");
    return 0;
  }
  if ((get_session(backend,sess_id) == ((PurpleMediaBackendFs2Session *)((void *)0))) && !(create_session(backend,sess_id,type,initiator,transmitter) != 0)) {
    purple_debug_error("backend-fs2","Error creating the session.\n");
    return 0;
  }
  if ((get_participant(backend,who) == ((FsParticipant *)((void *)0))) && !(create_participant(backend,who) != 0)) {
    purple_debug_error("backend-fs2","Error creating the participant.\n");
    return 0;
  }
  stream = get_stream(backend,sess_id,who);
  if (stream != ((PurpleMediaBackendFs2Stream *)((void *)0))) {
    FsStreamDirection type_direction = session_type_to_fs_stream_direction(type);
    if ((session_type_to_fs_stream_direction(( *(stream -> session)).type)) != type_direction) {
/* change direction */
      g_object_set((stream -> stream),"direction",type_direction,((void *)((void *)0)));
    }
  }
  else if (!(create_stream(backend,sess_id,who,type,initiator,transmitter,num_params,params) != 0)) {
    purple_debug_error("backend-fs2","Error creating the stream.\n");
    return 0;
  }
  return (!0);
}

static void purple_media_backend_fs2_add_remote_candidates(PurpleMediaBackend *self,const gchar *sess_id,const gchar *participant,GList *remote_candidates)
{
  PurpleMediaBackendFs2Private *priv;
  PurpleMediaBackendFs2Stream *stream;
  GError *err = (GError *)((void *)0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_fs2_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND_FS2(self)");
      return ;
    };
  }while (0);
  priv = ((PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)self),purple_media_backend_fs2_get_type())));
  stream = get_stream(((PurpleMediaBackendFs2 *)(g_type_check_instance_cast(((GTypeInstance *)self),purple_media_backend_fs2_get_type()))),sess_id,participant);
  if (stream == ((PurpleMediaBackendFs2Stream *)((void *)0))) {
    purple_debug_error("backend-fs2","purple_media_add_remote_candidates: couldn\'t find stream %s %s.\n",((sess_id != 0)?sess_id : "(null)"),((participant != 0)?participant : "(null)"));
    return ;
  }
  stream -> remote_candidates = g_list_concat((stream -> remote_candidates),candidate_list_to_fs(remote_candidates));
  if ((purple_media_is_initiator((priv -> media),sess_id,participant) != 0) || (purple_media_accepted((priv -> media),sess_id,participant) != 0)) {
#ifdef HAVE_FARSIGHT
#else
    if ((stream -> supports_add) != 0) 
      fs_stream_add_remote_candidates((stream -> stream),(stream -> remote_candidates),&err);
    else 
      fs_stream_force_remote_candidates((stream -> stream),(stream -> remote_candidates),&err);
#endif
    if (err != 0) {
      purple_debug_error("backend-fs2","Error adding remote candidates: %s\n",(err -> message));
      g_error_free(err);
    }
  }
}

static gboolean purple_media_backend_fs2_codecs_ready(PurpleMediaBackend *self,const gchar *sess_id)
{
  PurpleMediaBackendFs2Private *priv;
  gboolean ret = 0;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_fs2_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND_FS2(self)");
      return 0;
    };
  }while (0);
  priv = ((PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)self),purple_media_backend_fs2_get_type())));
  if (sess_id != ((const gchar *)((void *)0))) {
    PurpleMediaBackendFs2Session *session = get_session(((PurpleMediaBackendFs2 *)(g_type_check_instance_cast(((GTypeInstance *)self),purple_media_backend_fs2_get_type()))),sess_id);
    if (session == ((PurpleMediaBackendFs2Session *)((void *)0))) 
      return 0;
    if (((session -> type) & (PURPLE_MEDIA_SEND_AUDIO | PURPLE_MEDIA_SEND_VIDEO)) != 0U) {
#ifdef HAVE_FARSIGHT
#else
      GList *codecs = (GList *)((void *)0);
      g_object_get((session -> session),"codecs",&codecs,((void *)((void *)0)));
      if (codecs != 0) {
        fs_codec_list_destroy(codecs);
        ret = (!0);
      }
#endif
    }
    else 
      ret = (!0);
  }
  else {
    GList *values = g_hash_table_get_values((priv -> sessions));
{
      for (; values != 0; values = g_list_delete_link(values,values)) {
        PurpleMediaBackendFs2Session *session = (values -> data);
        if (((session -> type) & (PURPLE_MEDIA_SEND_AUDIO | PURPLE_MEDIA_SEND_VIDEO)) != 0U) {
#ifdef HAVE_FARSIGHT
#else
          GList *codecs = (GList *)((void *)0);
          g_object_get((session -> session),"codecs",&codecs,((void *)((void *)0)));
          if (codecs != 0) {
            fs_codec_list_destroy(codecs);
            ret = (!0);
          }
          else {
            ret = 0;
            break; 
          }
#endif
        }
        else 
          ret = (!0);
      }
    }
    if (values != ((GList *)((void *)0))) 
      g_list_free(values);
  }
  return ret;
}

static GList *purple_media_backend_fs2_get_codecs(PurpleMediaBackend *self,const gchar *sess_id)
{
  PurpleMediaBackendFs2Session *session;
  GList *fscodecs;
  GList *codecs;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_fs2_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND_FS2(self)");
      return 0;
    };
  }while (0);
  session = get_session(((PurpleMediaBackendFs2 *)(g_type_check_instance_cast(((GTypeInstance *)self),purple_media_backend_fs2_get_type()))),sess_id);
  if (session == ((PurpleMediaBackendFs2Session *)((void *)0))) 
    return 0;
  g_object_get(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(session -> session)),((GType )(20 << 2))))),"codecs",&fscodecs,((void *)((void *)0)));
  codecs = codec_list_from_fs(fscodecs);
  fs_codec_list_destroy(fscodecs);
  return codecs;
}

static GList *purple_media_backend_fs2_get_local_candidates(PurpleMediaBackend *self,const gchar *sess_id,const gchar *participant)
{
  PurpleMediaBackendFs2Stream *stream;
  GList *candidates = (GList *)((void *)0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_fs2_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND_FS2(self)");
      return 0;
    };
  }while (0);
  stream = get_stream(((PurpleMediaBackendFs2 *)(g_type_check_instance_cast(((GTypeInstance *)self),purple_media_backend_fs2_get_type()))),sess_id,participant);
  if (stream != ((PurpleMediaBackendFs2Stream *)((void *)0))) 
    candidates = candidate_list_from_fs((stream -> local_candidates));
  return candidates;
}

static gboolean purple_media_backend_fs2_set_remote_codecs(PurpleMediaBackend *self,const gchar *sess_id,const gchar *participant,GList *codecs)
{
  PurpleMediaBackendFs2Stream *stream;
  GList *fscodecs;
  GError *err = (GError *)((void *)0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_fs2_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND_FS2(self)");
      return 0;
    };
  }while (0);
  stream = get_stream(((PurpleMediaBackendFs2 *)(g_type_check_instance_cast(((GTypeInstance *)self),purple_media_backend_fs2_get_type()))),sess_id,participant);
  if (stream == ((PurpleMediaBackendFs2Stream *)((void *)0))) 
    return 0;
  fscodecs = codec_list_to_fs(codecs);
  fs_stream_set_remote_codecs((stream -> stream),fscodecs,&err);
  fs_codec_list_destroy(fscodecs);
  if (err != 0) {
    purple_debug_error("backend-fs2","Error setting remote codecs: %s\n",(err -> message));
    g_error_free(err);
    return 0;
  }
  return (!0);
}

static gboolean purple_media_backend_fs2_set_send_codec(PurpleMediaBackend *self,const gchar *sess_id,PurpleMediaCodec *codec)
{
  PurpleMediaBackendFs2Session *session;
  FsCodec *fscodec;
  GError *err = (GError *)((void *)0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_fs2_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND_FS2(self)");
      return 0;
    };
  }while (0);
  session = get_session(((PurpleMediaBackendFs2 *)(g_type_check_instance_cast(((GTypeInstance *)self),purple_media_backend_fs2_get_type()))),sess_id);
  if (session == ((PurpleMediaBackendFs2Session *)((void *)0))) 
    return 0;
  fscodec = codec_to_fs(codec);
  fs_session_set_send_codec((session -> session),fscodec,&err);
  fs_codec_destroy(fscodec);
  if (err != 0) {
    purple_debug_error("media","Error setting send codec\n");
    g_error_free(err);
    return 0;
  }
  return (!0);
}

static void purple_media_backend_fs2_set_params(PurpleMediaBackend *self,guint num_params,GParameter *params)
{
  PurpleMediaBackendFs2Private *priv;
  const gchar **supported = purple_media_backend_fs2_get_available_params();
  const gchar **p;
  guint i;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_fs2_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND_FS2(self)");
      return ;
    };
  }while (0);
  priv = ((PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)self),purple_media_backend_fs2_get_type())));
  if (((priv -> conference) == ((FsConference *)((void *)0))) && !(init_conference(((PurpleMediaBackendFs2 *)(g_type_check_instance_cast(((GTypeInstance *)self),purple_media_backend_fs2_get_type())))) != 0)) {
    purple_debug_error("backend-fs2","Error initializing the conference.\n");
    return ;
  }
  for (i = 0; i != num_params; ++i) {{
      for (p = supported;  *p != ((const gchar *)((void *)0)); ++p) {
        if (!(strcmp(params[i].name, *p) != 0)) {
          g_object_set((priv -> conference),params[i].name,g_value_get_string((&params[i].value)),((void *)((void *)0)));
          break; 
        }
      }
    }
  }
}

static const gchar **purple_media_backend_fs2_get_available_params()
{
  static const gchar *supported_params[] = {("sdes-cname"), ("sdes-email"), ("sdes-location"), ("sdes-name"), ("sdes-note"), ("sdes-phone"), ("sdes-tool"), ((const gchar *)((void *)0))};
  return supported_params;
}
#else
#endif /* USE_VV */
#ifdef USE_GSTREAMER

GstElement *purple_media_backend_fs2_get_src(PurpleMediaBackendFs2 *self,const gchar *sess_id)
{
#ifdef USE_VV
  PurpleMediaBackendFs2Session *session = get_session(self,sess_id);
  return (session != ((PurpleMediaBackendFs2Session *)((void *)0)))?(session -> src) : ((struct _GstElement *)((void *)0));
#else
#endif
}

GstElement *purple_media_backend_fs2_get_tee(PurpleMediaBackendFs2 *self,const gchar *sess_id,const gchar *who)
{
#ifdef USE_VV
  if ((sess_id != ((const gchar *)((void *)0))) && (who == ((const gchar *)((void *)0)))) {
    PurpleMediaBackendFs2Session *session = get_session(self,sess_id);
    return (session != ((PurpleMediaBackendFs2Session *)((void *)0)))?(session -> tee) : ((struct _GstElement *)((void *)0));
  }
  else if ((sess_id != ((const gchar *)((void *)0))) && (who != ((const gchar *)((void *)0)))) {
    PurpleMediaBackendFs2Stream *stream = get_stream(self,sess_id,who);
    return (stream != ((PurpleMediaBackendFs2Stream *)((void *)0)))?(stream -> tee) : ((struct _GstElement *)((void *)0));
  }
#endif /* USE_VV */
  do {
    g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","backend-fs2.c",2410,((const char *)__func__));
    return 0;
  }while (0);
}

void purple_media_backend_fs2_set_input_volume(PurpleMediaBackendFs2 *self,const gchar *sess_id,double level)
{
#ifdef USE_VV
  PurpleMediaBackendFs2Private *priv;
  GList *sessions;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_fs2_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND_FS2(self)");
      return ;
    };
  }while (0);
  priv = ((PurpleMediaBackendFs2Private *)(g_type_instance_get_private(((GTypeInstance *)self),purple_media_backend_fs2_get_type())));
  purple_prefs_set_int("/purple/media/audio/volume/input",level);
  if (sess_id == ((const gchar *)((void *)0))) 
    sessions = g_hash_table_get_values((priv -> sessions));
  else 
    sessions = g_list_append(0,(get_session(self,sess_id)));
  for (; sessions != 0; sessions = g_list_delete_link(sessions,sessions)) {
    PurpleMediaBackendFs2Session *session = (sessions -> data);
    if (((session -> type) & PURPLE_MEDIA_SEND_AUDIO) != 0U) {
      gchar *name = g_strdup_printf("volume_%s",(session -> id));
      GstElement *volume = gst_bin_get_by_name(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)(priv -> confbin)),gst_bin_get_type()))),name);
      g_free(name);
      g_object_set(volume,"volume",(level / 10.0),((void *)((void *)0)));
    }
  }
#endif /* USE_VV */
}

void purple_media_backend_fs2_set_output_volume(PurpleMediaBackendFs2 *self,const gchar *sess_id,const gchar *who,double level)
{
#ifdef USE_VV
  GList *streams;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)self;
      GType __t = purple_media_backend_fs2_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_BACKEND_FS2(self)");
      return ;
    };
  }while (0);
  purple_prefs_set_int("/purple/media/audio/volume/output",level);
  streams = get_streams(self,sess_id,who);
  for (; streams != 0; streams = g_list_delete_link(streams,streams)) {
    PurpleMediaBackendFs2Stream *stream = (streams -> data);
    if (((( *(stream -> session)).type & PURPLE_MEDIA_RECV_AUDIO) != 0U) && ((({
      GTypeInstance *__inst = (GTypeInstance *)(stream -> volume);
      GType __t = gst_element_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0)) 
{
      g_object_set((stream -> volume),"volume",(level / 10.0),((void *)((void *)0)));
    }
  }
#endif /* USE_VV */
}
#endif /* USE_GSTREAMER */
