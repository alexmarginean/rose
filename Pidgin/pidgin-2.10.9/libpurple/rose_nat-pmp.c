/**
 * @file nat-pmp.c NAT-PMP Implementation
 * @ingroup core
 */
/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * Most code in nat-pmp.c copyright (C) 2007, R. Tyler Ballance, bleep, LLC.
 * This file is distributed under the 3-clause (modified) BSD license:
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list of conditions and
 * the following disclaimer.
 * Neither the name of the bleep. LLC nor the names of its contributors may be used to endorse or promote
 * products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 */
#include "internal.h"
#include "nat-pmp.h"
#include "debug.h"
#include "signals.h"
#include "network.h"
#ifdef HAVE_SYS_PARAM_H
#include <sys/param.h>
#endif
#ifdef HAVE_SYS_SYSCTL_H
#include <sys/sysctl.h>
#endif
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif
/* We will need sysctl() and NET_RT_DUMP, both of which are not present
 * on all platforms, to continue. */
#if defined(HAVE_SYS_SYSCTL_H) && defined(NET_RT_DUMP)
#include <sys/types.h>
#include <net/route.h>
#define PMP_DEBUG	1
/* 128 + n */
/*
 *	Thanks to R. Matthew Emerson for the fixes on this
 */
#define PMP_MAP_OPCODE_UDP	1
#define PMP_MAP_OPCODE_TCP	2
#define PMP_VERSION			0
#define PMP_PORT			5351
#define PMP_TIMEOUT			250000	/* 250000 useconds */
/* alignment constraint for routing socket */
#define ROUNDUP(a)			((a) > 0 ? (1 + (((a) - 1) | (sizeof(long) - 1))) : sizeof(long))
#define ADVANCE(x, n)		(x += ROUNDUP((n)->sa_len))
#ifdef HAVE_STRUCT_SOCKADDR_SA_LEN
#else
#ifdef AF_INET6
#endif
#endif
#ifdef HAVE_STRUCT_SOCKADDR_SA_LEN
#else
#endif
/*!
 * The return sockaddr_in must be g_free()'d when no longer needed
 */
/* entire routing table or a subset of it */
/* protocol number - always 0 */
/* address family - 0 for all addres families */
/* Determine the buffer side needed to get the full routing table */
/* Read the routing table into buf */
/* We found the default route. Now get the destination address and netmask. */
/*!
 *	purple_pmp_get_public_ip() will return the publicly facing IP address of the
 *	default NAT gateway. The function will return NULL if:
 *		- The gateway doesn't support NAT-PMP
 *		- The gateway errors in some other spectacular fashion
 */
#ifdef PMP_DEBUG
#endif
/* If we get a NULL gateway, don't try again next time */
/* Default port for NAT-PMP is 5351 */
/* Clean out both req and resp structures */
/* The NAT-PMP spec says we should attempt to contact the gateway 9 times, doubling the time we wait each time.
	 * Even starting with a timeout of 0.1 seconds, that means that we have a total waiting of 204.6 seconds.
	 * With the recommended timeout of 0.25 seconds, we're talking 511.5 seconds (8.5 minutes).
	 *
	 * This seems really silly... if this were nonblocking, a couple retries might be in order, but it's not at present.
	 */
#ifdef PMP_DEBUG
#endif
/* TODO: Non-blocking! */
/* TODO: Non-blocking! */
#ifdef PMP_DEBUG
#endif
/* Default port for NAT-PMP is 5351 */
/* Set up the req */
/* What a difference byte ordering makes...d'oh! */
/* The NAT-PMP spec says we should attempt to contact the gateway 9 times, doubling the time we wait each time.
	 * Even starting with a timeout of 0.1 seconds, that means that we have a total waiting of 204.6 seconds.
	 * With the recommended timeout of 0.25 seconds, we're talking 511.5 seconds (8.5 minutes).
	 *
	 * This seems really silly... if this were nonblocking, a couple retries might be in order, but it's not at present.
	 * XXX Make this nonblocking.
	 * XXX This code looks like the pmp_get_public_ip() code. Can it be consolidated?
	 */
#ifdef PMP_DEBUG
#endif
/* TODO: Non-blocking! */
/* The original code treats EAGAIN as a reason to iterate.. but I've removed iteration. This may be a problem */
/* TODO: Non-blocking! */
#ifdef PMP_DEBUG
#endif
/* XXX The private port may actually differ from the one we requested, according to the spec.
	 * We don't handle that situation at present.
	 *
	 * TODO: Look at the result and verify it matches what we wanted; either return a failure if it doesn't,
	 * or change network.c to know what to do if the desired private port shifts as a result of the nat-pmp operation.
	 */
#else /* #ifdef NET_RT_DUMP */

char *purple_pmp_get_public_ip()
{
  return 0;
}

gboolean purple_pmp_create_map(PurplePmpType type,unsigned short privateport,unsigned short publicport,int lifetime)
{
  return 0;
}

gboolean purple_pmp_destroy_map(PurplePmpType type,unsigned short privateport)
{
  return 0;
}

void purple_pmp_init()
{
}
#endif /* #if !(defined(HAVE_SYS_SYCTL_H) && defined(NET_RT_DUMP)) */
