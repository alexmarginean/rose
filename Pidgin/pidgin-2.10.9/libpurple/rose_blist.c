/*
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 *
 */
#define _PURPLE_BLIST_C_
#include "internal.h"
#include "blist.h"
#include "conversation.h"
#include "dbus-maybe.h"
#include "debug.h"
#include "notify.h"
#include "pounce.h"
#include "prefs.h"
#include "privacy.h"
#include "prpl.h"
#include "server.h"
#include "signals.h"
#include "util.h"
#include "value.h"
#include "xmlnode.h"
static PurpleBlistUiOps *blist_ui_ops = (PurpleBlistUiOps *)((void *)0);
static PurpleBuddyList *purplebuddylist = (PurpleBuddyList *)((void *)0);
/**
 * A hash table used for efficient lookups of buddies by name.
 * PurpleAccount* => GHashTable*, with the inner hash table being
 * struct _purple_hbuddy => PurpleBuddy*
 */
static GHashTable *buddies_cache = (GHashTable *)((void *)0);
/**
 * A hash table used for efficient lookups of groups by name.
 * UTF-8 collate-key => PurpleGroup*.
 */
static GHashTable *groups_cache = (GHashTable *)((void *)0);
static guint save_timer = 0;
static gboolean blist_loaded = 0;
/*********************************************************************
 * Private utility functions                                         *
 *********************************************************************/

static PurpleBlistNode *purple_blist_get_last_sibling(PurpleBlistNode *node)
{
  PurpleBlistNode *n = node;
  if (!(n != 0)) 
    return 0;
  while((n -> next) != 0)
    n = (n -> next);
  return n;
}

static PurpleBlistNode *purple_blist_get_last_child(PurpleBlistNode *node)
{
  if (!(node != 0)) 
    return 0;
  return purple_blist_get_last_sibling((node -> child));
}

struct _list_account_buddies 
{
  GSList *list;
  PurpleAccount *account;
}
;

struct _purple_hbuddy 
{
  char *name;
  PurpleAccount *account;
  PurpleBlistNode *group;
}
;
/* This function must not use purple_normalize */

static guint _purple_blist_hbuddy_hash(struct _purple_hbuddy *hb)
{
  return (g_str_hash((hb -> name)) ^ g_direct_hash((hb -> group))) ^ g_direct_hash((hb -> account));
}
/* This function must not use purple_normalize */

static guint _purple_blist_hbuddy_equal(struct _purple_hbuddy *hb1,struct _purple_hbuddy *hb2)
{
  return ((((hb1 -> group) == (hb2 -> group)) && ((hb1 -> account) == (hb2 -> account))) && (g_str_equal((hb1 -> name),(hb2 -> name)) != 0));
}

static void _purple_blist_hbuddy_free_key(struct _purple_hbuddy *hb)
{
  g_free((hb -> name));
  g_free(hb);
}

static void purple_blist_buddies_cache_add_account(PurpleAccount *account)
{
  GHashTable *account_buddies = g_hash_table_new_full(((GHashFunc )_purple_blist_hbuddy_hash),((GEqualFunc )_purple_blist_hbuddy_equal),((GDestroyNotify )_purple_blist_hbuddy_free_key),0);
  g_hash_table_insert(buddies_cache,account,account_buddies);
}

static void purple_blist_buddies_cache_remove_account(const PurpleAccount *account)
{
  g_hash_table_remove(buddies_cache,account);
}
/*********************************************************************
 * Writing to disk                                                   *
 *********************************************************************/

static void value_to_xmlnode(gpointer key,gpointer hvalue,gpointer user_data)
{
  const char *name;
  PurpleValue *value;
  xmlnode *node;
  xmlnode *child;
  char buf[21UL];
  name = ((const char *)key);
  value = ((PurpleValue *)hvalue);
  node = ((xmlnode *)user_data);
  do {
    if (value != ((PurpleValue *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"value != NULL");
      return ;
    };
  }while (0);
  child = xmlnode_new_child(node,"setting");
  xmlnode_set_attrib(child,"name",name);
  if ((purple_value_get_type(value)) == PURPLE_TYPE_INT) {
    xmlnode_set_attrib(child,"type","int");
    g_snprintf(buf,(sizeof(buf)),"%d",purple_value_get_int(value));
    xmlnode_insert_data(child,buf,(-1));
  }
  else if ((purple_value_get_type(value)) == PURPLE_TYPE_STRING) {
    xmlnode_set_attrib(child,"type","string");
    xmlnode_insert_data(child,purple_value_get_string(value),(-1));
  }
  else if ((purple_value_get_type(value)) == PURPLE_TYPE_BOOLEAN) {
    xmlnode_set_attrib(child,"type","bool");
    g_snprintf(buf,(sizeof(buf)),"%d",purple_value_get_boolean(value));
    xmlnode_insert_data(child,buf,(-1));
  }
}

static void chat_component_to_xmlnode(gpointer key,gpointer value,gpointer user_data)
{
  const char *name;
  const char *data;
  xmlnode *node;
  xmlnode *child;
  name = ((const char *)key);
  data = ((const char *)value);
  node = ((xmlnode *)user_data);
  do {
    if (data != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"data != NULL");
      return ;
    };
  }while (0);
  child = xmlnode_new_child(node,"component");
  xmlnode_set_attrib(child,"name",name);
  xmlnode_insert_data(child,data,(-1));
}

static xmlnode *buddy_to_xmlnode(PurpleBlistNode *bnode)
{
  xmlnode *node;
  xmlnode *child;
  PurpleBuddy *buddy;
  buddy = ((PurpleBuddy *)bnode);
  node = xmlnode_new("buddy");
  xmlnode_set_attrib(node,"account",purple_account_get_username((buddy -> account)));
  xmlnode_set_attrib(node,"proto",purple_account_get_protocol_id((buddy -> account)));
  child = xmlnode_new_child(node,"name");
  xmlnode_insert_data(child,(buddy -> name),(-1));
  if ((buddy -> alias) != ((char *)((void *)0))) {
    child = xmlnode_new_child(node,"alias");
    xmlnode_insert_data(child,(buddy -> alias),(-1));
  }
/* Write buddy settings */
  g_hash_table_foreach(buddy -> node.settings,value_to_xmlnode,node);
  return node;
}

static xmlnode *contact_to_xmlnode(PurpleBlistNode *cnode)
{
  xmlnode *node;
  xmlnode *child;
  PurpleContact *contact;
  PurpleBlistNode *bnode;
  contact = ((PurpleContact *)cnode);
  node = xmlnode_new("contact");
  if ((contact -> alias) != ((char *)((void *)0))) {
    xmlnode_set_attrib(node,"alias",(contact -> alias));
  }
/* Write buddies */
  for (bnode = (cnode -> child); bnode != ((PurpleBlistNode *)((void *)0)); bnode = (bnode -> next)) {
    if (!(!(((purple_blist_node_get_flags(((PurpleBlistNode *)bnode))) & PURPLE_BLIST_NODE_FLAG_NO_SAVE) != 0U))) 
      continue; 
    if ((purple_blist_node_get_type(bnode)) == PURPLE_BLIST_BUDDY_NODE) {
      child = buddy_to_xmlnode(bnode);
      xmlnode_insert_child(node,child);
    }
  }
/* Write contact settings */
  g_hash_table_foreach((cnode -> settings),value_to_xmlnode,node);
  return node;
}

static xmlnode *chat_to_xmlnode(PurpleBlistNode *cnode)
{
  xmlnode *node;
  xmlnode *child;
  PurpleChat *chat;
  chat = ((PurpleChat *)cnode);
  node = xmlnode_new("chat");
  xmlnode_set_attrib(node,"proto",purple_account_get_protocol_id((chat -> account)));
  xmlnode_set_attrib(node,"account",purple_account_get_username((chat -> account)));
  if ((chat -> alias) != ((char *)((void *)0))) {
    child = xmlnode_new_child(node,"alias");
    xmlnode_insert_data(child,(chat -> alias),(-1));
  }
/* Write chat components */
  g_hash_table_foreach((chat -> components),chat_component_to_xmlnode,node);
/* Write chat settings */
  g_hash_table_foreach(chat -> node.settings,value_to_xmlnode,node);
  return node;
}

static xmlnode *group_to_xmlnode(PurpleBlistNode *gnode)
{
  xmlnode *node;
  xmlnode *child;
  PurpleGroup *group;
  PurpleBlistNode *cnode;
  group = ((PurpleGroup *)gnode);
  node = xmlnode_new("group");
  xmlnode_set_attrib(node,"name",(group -> name));
/* Write settings */
  g_hash_table_foreach(group -> node.settings,value_to_xmlnode,node);
/* Write contacts and chats */
  for (cnode = (gnode -> child); cnode != ((PurpleBlistNode *)((void *)0)); cnode = (cnode -> next)) {
    if (!(!(((purple_blist_node_get_flags(((PurpleBlistNode *)cnode))) & PURPLE_BLIST_NODE_FLAG_NO_SAVE) != 0U))) 
      continue; 
    if ((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CONTACT_NODE) {
      child = contact_to_xmlnode(cnode);
      xmlnode_insert_child(node,child);
    }
    else if ((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CHAT_NODE) {
      child = chat_to_xmlnode(cnode);
      xmlnode_insert_child(node,child);
    }
  }
  return node;
}

static xmlnode *accountprivacy_to_xmlnode(PurpleAccount *account)
{
  xmlnode *node;
  xmlnode *child;
  GSList *cur;
  char buf[10UL];
  node = xmlnode_new("account");
  xmlnode_set_attrib(node,"proto",purple_account_get_protocol_id(account));
  xmlnode_set_attrib(node,"name",purple_account_get_username(account));
  g_snprintf(buf,(sizeof(buf)),"%d",(account -> perm_deny));
  xmlnode_set_attrib(node,"mode",buf);
  for (cur = (account -> permit); cur != 0; cur = (cur -> next)) {
    child = xmlnode_new_child(node,"permit");
    xmlnode_insert_data(child,(cur -> data),(-1));
  }
  for (cur = (account -> deny); cur != 0; cur = (cur -> next)) {
    child = xmlnode_new_child(node,"block");
    xmlnode_insert_data(child,(cur -> data),(-1));
  }
  return node;
}

static xmlnode *blist_to_xmlnode()
{
  xmlnode *node;
  xmlnode *child;
  xmlnode *grandchild;
  PurpleBlistNode *gnode;
  GList *cur;
  node = xmlnode_new("purple");
  xmlnode_set_attrib(node,"version","1.0");
/* Write groups */
  child = xmlnode_new_child(node,"blist");
  for (gnode = (purplebuddylist -> root); gnode != ((PurpleBlistNode *)((void *)0)); gnode = (gnode -> next)) {
    if (!(!(((purple_blist_node_get_flags(((PurpleBlistNode *)gnode))) & PURPLE_BLIST_NODE_FLAG_NO_SAVE) != 0U))) 
      continue; 
    if ((purple_blist_node_get_type(gnode)) == PURPLE_BLIST_GROUP_NODE) {
      grandchild = group_to_xmlnode(gnode);
      xmlnode_insert_child(child,grandchild);
    }
  }
/* Write privacy settings */
  child = xmlnode_new_child(node,"privacy");
  for (cur = purple_accounts_get_all(); cur != ((GList *)((void *)0)); cur = (cur -> next)) {
    grandchild = accountprivacy_to_xmlnode((cur -> data));
    xmlnode_insert_child(child,grandchild);
  }
  return node;
}

static void purple_blist_sync()
{
  xmlnode *node;
  char *data;
  if (!(blist_loaded != 0)) {
    purple_debug_error("blist","Attempted to save buddy list before it was read!\n");
    return ;
  }
  node = blist_to_xmlnode();
  data = xmlnode_to_formatted_str(node,0);
  purple_util_write_data_to_file("blist.xml",data,(-1));
  g_free(data);
  xmlnode_free(node);
}

static gboolean save_cb(gpointer data)
{
  purple_blist_sync();
  save_timer = 0;
  return 0;
}

static void _purple_blist_schedule_save()
{
  if (save_timer == 0) 
    save_timer = purple_timeout_add_seconds(5,save_cb,0);
}

static void purple_blist_save_account(PurpleAccount *account)
{
#if 1
  _purple_blist_schedule_save();
#else
/* Save the buddies and privacy data for this account */
/* Save all buddies and privacy data */
#endif
}

static void purple_blist_save_node(PurpleBlistNode *node)
{
  _purple_blist_schedule_save();
}

void purple_blist_schedule_save()
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
/* Save everything */
  if ((ops != 0) && ((ops -> save_account) != 0)) 
    ( *(ops -> save_account))(0);
}
/*********************************************************************
 * Reading from disk                                                 *
 *********************************************************************/

static void parse_setting(PurpleBlistNode *node,xmlnode *setting)
{
  const char *name = xmlnode_get_attrib(setting,"name");
  const char *type = xmlnode_get_attrib(setting,"type");
  char *value = xmlnode_get_data(setting);
  if (!(value != 0)) 
    return ;
  if (!(type != 0) || (purple_strequal(type,"string") != 0)) 
    purple_blist_node_set_string(node,name,value);
  else if (purple_strequal(type,"bool") != 0) 
    purple_blist_node_set_bool(node,name,atoi(value));
  else if (purple_strequal(type,"int") != 0) 
    purple_blist_node_set_int(node,name,atoi(value));
  g_free(value);
}

static void parse_buddy(PurpleGroup *group,PurpleContact *contact,xmlnode *bnode)
{
  PurpleAccount *account;
  PurpleBuddy *buddy;
  char *name = (char *)((void *)0);
  char *alias = (char *)((void *)0);
  const char *acct_name;
  const char *proto;
  const char *protocol;
  xmlnode *x;
  acct_name = xmlnode_get_attrib(bnode,"account");
  protocol = xmlnode_get_attrib(bnode,"protocol");
/* XXX: Remove */
  protocol = _purple_oscar_convert(acct_name,protocol);
  proto = xmlnode_get_attrib(bnode,"proto");
/* XXX: Remove */
  proto = _purple_oscar_convert(acct_name,proto);
  if (!(acct_name != 0) || (!(proto != 0) && !(protocol != 0))) 
    return ;
  account = purple_accounts_find(acct_name,((proto != 0)?proto : protocol));
  if (!(account != 0)) 
    return ;
  if ((x = xmlnode_get_child(bnode,"name")) != 0) 
    name = xmlnode_get_data(x);
  if (!(name != 0)) 
    return ;
  if ((x = xmlnode_get_child(bnode,"alias")) != 0) 
    alias = xmlnode_get_data(x);
  buddy = purple_buddy_new(account,name,alias);
  purple_blist_add_buddy(buddy,contact,group,purple_blist_get_last_child(((PurpleBlistNode *)contact)));
  for (x = xmlnode_get_child(bnode,"setting"); x != 0; x = xmlnode_get_next_twin(x)) {
    parse_setting(((PurpleBlistNode *)buddy),x);
  }
  g_free(name);
  g_free(alias);
}

static void parse_contact(PurpleGroup *group,xmlnode *cnode)
{
  PurpleContact *contact = purple_contact_new();
  xmlnode *x;
  const char *alias;
  purple_blist_add_contact(contact,group,purple_blist_get_last_child(((PurpleBlistNode *)group)));
  if ((alias = xmlnode_get_attrib(cnode,"alias")) != 0) {
    purple_blist_alias_contact(contact,alias);
  }
  for (x = (cnode -> child); x != 0; x = (x -> next)) {
    if ((x -> type) != XMLNODE_TYPE_TAG) 
      continue; 
    if (purple_strequal((x -> name),"buddy") != 0) 
      parse_buddy(group,contact,x);
    else if (purple_strequal((x -> name),"setting") != 0) 
      parse_setting(((PurpleBlistNode *)contact),x);
  }
/* if the contact is empty, don't keep it around.  it causes problems */
  if (!(( *((PurpleBlistNode *)contact)).child != 0)) 
    purple_blist_remove_contact(contact);
}

static void parse_chat(PurpleGroup *group,xmlnode *cnode)
{
  PurpleChat *chat;
  PurpleAccount *account;
  const char *acct_name;
  const char *proto;
  const char *protocol;
  xmlnode *x;
  char *alias = (char *)((void *)0);
  GHashTable *components;
  acct_name = xmlnode_get_attrib(cnode,"account");
  protocol = xmlnode_get_attrib(cnode,"protocol");
  proto = xmlnode_get_attrib(cnode,"proto");
  if (!(acct_name != 0) || (!(proto != 0) && !(protocol != 0))) 
    return ;
  account = purple_accounts_find(acct_name,((proto != 0)?proto : protocol));
  if (!(account != 0)) 
    return ;
  if ((x = xmlnode_get_child(cnode,"alias")) != 0) 
    alias = xmlnode_get_data(x);
  components = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
  for (x = xmlnode_get_child(cnode,"component"); x != 0; x = xmlnode_get_next_twin(x)) {
    const char *name;
    char *value;
    name = xmlnode_get_attrib(x,"name");
    value = xmlnode_get_data(x);
    g_hash_table_replace(components,(g_strdup(name)),value);
  }
  chat = purple_chat_new(account,alias,components);
  purple_blist_add_chat(chat,group,purple_blist_get_last_child(((PurpleBlistNode *)group)));
  for (x = xmlnode_get_child(cnode,"setting"); x != 0; x = xmlnode_get_next_twin(x)) {
    parse_setting(((PurpleBlistNode *)chat),x);
  }
  g_free(alias);
}

static void parse_group(xmlnode *groupnode)
{
  const char *name = xmlnode_get_attrib(groupnode,"name");
  PurpleGroup *group;
  xmlnode *cnode;
  if (!(name != 0)) 
    name = ((const char *)(dgettext("pidgin","Buddies")));
  group = purple_group_new(name);
  purple_blist_add_group(group,purple_blist_get_last_sibling((purplebuddylist -> root)));
  for (cnode = (groupnode -> child); cnode != 0; cnode = (cnode -> next)) {
    if ((cnode -> type) != XMLNODE_TYPE_TAG) 
      continue; 
    if (purple_strequal((cnode -> name),"setting") != 0) 
      parse_setting(((PurpleBlistNode *)group),cnode);
    else if ((purple_strequal((cnode -> name),"contact") != 0) || (purple_strequal((cnode -> name),"person") != 0)) 
      parse_contact(group,cnode);
    else if (purple_strequal((cnode -> name),"chat") != 0) 
      parse_chat(group,cnode);
  }
}
/* TODO: Make static and rename to load_blist */

void purple_blist_load()
{
  xmlnode *purple;
  xmlnode *blist;
  xmlnode *privacy;
  blist_loaded = (!0);
  purple = purple_util_read_xml_from_file("blist.xml",((const char *)(dgettext("pidgin","buddy list"))));
  if (purple == ((xmlnode *)((void *)0))) 
    return ;
  blist = xmlnode_get_child(purple,"blist");
  if (blist != 0) {
    xmlnode *groupnode;
    for (groupnode = xmlnode_get_child(blist,"group"); groupnode != ((xmlnode *)((void *)0)); groupnode = xmlnode_get_next_twin(groupnode)) {
      parse_group(groupnode);
    }
  }
  privacy = xmlnode_get_child(purple,"privacy");
  if (privacy != 0) {
    xmlnode *anode;
    for (anode = (privacy -> child); anode != 0; anode = (anode -> next)) {{
        xmlnode *x;
        PurpleAccount *account;
        int imode;
        const char *acct_name;
        const char *proto;
        const char *mode;
        const char *protocol;
        acct_name = xmlnode_get_attrib(anode,"name");
        protocol = xmlnode_get_attrib(anode,"protocol");
        proto = xmlnode_get_attrib(anode,"proto");
        mode = xmlnode_get_attrib(anode,"mode");
        if ((!(acct_name != 0) || (!(proto != 0) && !(protocol != 0))) || !(mode != 0)) 
          continue; 
        account = purple_accounts_find(acct_name,((proto != 0)?proto : protocol));
        if (!(account != 0)) 
          continue; 
        imode = atoi(mode);
        account -> perm_deny = (((imode != 0)?imode : PURPLE_PRIVACY_ALLOW_ALL));
        for (x = (anode -> child); x != 0; x = (x -> next)) {{
            char *name;
            if ((x -> type) != XMLNODE_TYPE_TAG) 
              continue; 
            if (purple_strequal((x -> name),"permit") != 0) {
              name = xmlnode_get_data(x);
              purple_privacy_permit_add(account,name,(!0));
              g_free(name);
            }
            else if (purple_strequal((x -> name),"block") != 0) {
              name = xmlnode_get_data(x);
              purple_privacy_deny_add(account,name,(!0));
              g_free(name);
            }
          }
        }
      }
    }
  }
  xmlnode_free(purple);
/* This tells the buddy icon code to do its thing. */
  _purple_buddy_icons_blist_loaded_cb();
}
/*********************************************************************
 * Stuff                                                             *
 *********************************************************************/

static void purple_contact_compute_priority_buddy(PurpleContact *contact)
{
  PurpleBlistNode *bnode;
  PurpleBuddy *new_priority = (PurpleBuddy *)((void *)0);
  do {
    if (contact != ((PurpleContact *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"contact != NULL");
      return ;
    };
  }while (0);
  contact -> priority = ((PurpleBuddy *)((void *)0));
  for (bnode = ( *((PurpleBlistNode *)contact)).child; bnode != ((PurpleBlistNode *)((void *)0)); bnode = (bnode -> next)) {{
      PurpleBuddy *buddy;
      if (!((purple_blist_node_get_type(bnode)) == PURPLE_BLIST_BUDDY_NODE)) 
        continue; 
      buddy = ((PurpleBuddy *)bnode);
      if (new_priority == ((PurpleBuddy *)((void *)0))) {
        new_priority = buddy;
        continue; 
      }
      if (purple_account_is_connected((buddy -> account)) != 0) {
        int cmp = 1;
        if (purple_account_is_connected((new_priority -> account)) != 0) 
          cmp = purple_presence_compare((purple_buddy_get_presence(new_priority)),(purple_buddy_get_presence(buddy)));
        if ((cmp > 0) || ((cmp == 0) && (purple_prefs_get_bool("/purple/contact/last_match") != 0))) {
          new_priority = buddy;
        }
      }
    }
  }
  contact -> priority = new_priority;
  contact -> priority_valid = (!0);
}
/*****************************************************************************
 * Public API functions                                                      *
 *****************************************************************************/

PurpleBuddyList *purple_blist_new()
{
  PurpleBlistUiOps *ui_ops;
  GList *account;
  PurpleBuddyList *gbl = (PurpleBuddyList *)(g_malloc0_n(1,(sizeof(PurpleBuddyList ))));
{
    PurpleBuddyList *typed_ptr = gbl;
    purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurpleBuddyList);
  };
  ui_ops = purple_blist_get_ui_ops();
  gbl -> buddies = g_hash_table_new_full(((GHashFunc )_purple_blist_hbuddy_hash),((GEqualFunc )_purple_blist_hbuddy_equal),((GDestroyNotify )_purple_blist_hbuddy_free_key),0);
  buddies_cache = g_hash_table_new_full(g_direct_hash,g_direct_equal,0,((GDestroyNotify )g_hash_table_destroy));
  groups_cache = g_hash_table_new_full(((GHashFunc )g_str_hash),((GEqualFunc )g_str_equal),((GDestroyNotify )g_free),0);
  for (account = purple_accounts_get_all(); account != ((GList *)((void *)0)); account = (account -> next)) {
    purple_blist_buddies_cache_add_account((account -> data));
  }
  if ((ui_ops != ((PurpleBlistUiOps *)((void *)0))) && ((ui_ops -> new_list) != ((void (*)(PurpleBuddyList *))((void *)0)))) 
    ( *(ui_ops -> new_list))(gbl);
  return gbl;
}

void purple_set_blist(PurpleBuddyList *list)
{
  purplebuddylist = list;
}

PurpleBuddyList *purple_get_blist()
{
  return purplebuddylist;
}

PurpleBlistNode *purple_blist_get_root()
{
  return (purplebuddylist != 0)?(purplebuddylist -> root) : ((struct _PurpleBlistNode *)((void *)0));
}

static void append_buddy(gpointer key,gpointer value,gpointer user_data)
{
  GSList **list = user_data;
   *list = g_slist_prepend( *list,value);
}

GSList *purple_blist_get_buddies()
{
  GSList *buddies = (GSList *)((void *)0);
  if (!(purplebuddylist != 0)) 
    return 0;
  g_hash_table_foreach((purplebuddylist -> buddies),append_buddy,(&buddies));
  return buddies;
}

gpointer purple_blist_get_ui_data()
{
  return purplebuddylist -> ui_data;
}

void purple_blist_set_ui_data(void *ui_data)
{
  purplebuddylist -> ui_data = ui_data;
}

void purple_blist_show()
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  if ((ops != 0) && ((ops -> show) != 0)) 
    ( *(ops -> show))(purplebuddylist);
}

void purple_blist_destroy()
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  purple_debug(PURPLE_DEBUG_INFO,"blist","Destroying\n");
  if ((ops != 0) && ((ops -> destroy) != 0)) 
    ( *(ops -> destroy))(purplebuddylist);
}

void purple_blist_set_visible(gboolean show)
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  if ((ops != 0) && ((ops -> set_visible) != 0)) 
    ( *(ops -> set_visible))(purplebuddylist,show);
}

static PurpleBlistNode *get_next_node(PurpleBlistNode *node,gboolean godeep)
{
  if (node == ((PurpleBlistNode *)((void *)0))) 
    return 0;
  if ((godeep != 0) && ((node -> child) != 0)) 
    return node -> child;
  if ((node -> next) != 0) 
    return node -> next;
  return get_next_node((node -> parent),0);
}

PurpleBlistNode *purple_blist_node_next(PurpleBlistNode *node,gboolean offline)
{
  PurpleBlistNode *ret = node;
  if (offline != 0) 
    return get_next_node(ret,(!0));
  do {
    ret = get_next_node(ret,(!0));
  }while (((ret != 0) && ((purple_blist_node_get_type(ret)) == PURPLE_BLIST_BUDDY_NODE)) && !(purple_account_is_connected((purple_buddy_get_account(((PurpleBuddy *)ret)))) != 0));
  return ret;
}

PurpleBlistNode *purple_blist_node_get_parent(PurpleBlistNode *node)
{
  return (node != 0)?(node -> parent) : ((struct _PurpleBlistNode *)((void *)0));
}

PurpleBlistNode *purple_blist_node_get_first_child(PurpleBlistNode *node)
{
  return (node != 0)?(node -> child) : ((struct _PurpleBlistNode *)((void *)0));
}

PurpleBlistNode *purple_blist_node_get_sibling_next(PurpleBlistNode *node)
{
  return (node != 0)?(node -> next) : ((struct _PurpleBlistNode *)((void *)0));
}

PurpleBlistNode *purple_blist_node_get_sibling_prev(PurpleBlistNode *node)
{
  return (node != 0)?(node -> prev) : ((struct _PurpleBlistNode *)((void *)0));
}

gpointer purple_blist_node_get_ui_data(const PurpleBlistNode *node)
{
  do {
    if (node != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node");
      return 0;
    };
  }while (0);
  return node -> ui_data;
}

void purple_blist_node_set_ui_data(PurpleBlistNode *node,void *ui_data)
{
  do {
    if (node != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node");
      return ;
    };
  }while (0);
  node -> ui_data = ui_data;
}

void purple_blist_update_buddy_status(PurpleBuddy *buddy,PurpleStatus *old_status)
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  PurplePresence *presence;
  PurpleStatus *status;
  PurpleBlistNode *cnode;
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return ;
    };
  }while (0);
  presence = purple_buddy_get_presence(buddy);
  status = purple_presence_get_active_status(presence);
  purple_debug_info("blist","Updating buddy status for %s (%s)\n",(buddy -> name),purple_account_get_protocol_name((buddy -> account)));
  if ((purple_status_is_online(status) != 0) && !(purple_status_is_online(old_status) != 0)) {
    purple_signal_emit(purple_blist_get_handle(),"buddy-signed-on",buddy);
    cnode = buddy -> node.parent;
    if (++( *((PurpleContact *)cnode)).online == 1) 
      ( *((PurpleGroup *)(cnode -> parent))).online++;
  }
  else if (!(purple_status_is_online(status) != 0) && (purple_status_is_online(old_status) != 0)) {
    purple_blist_node_set_int(&buddy -> node,"last_seen",(time(0)));
    purple_signal_emit(purple_blist_get_handle(),"buddy-signed-off",buddy);
    cnode = buddy -> node.parent;
    if (--( *((PurpleContact *)cnode)).online == 0) 
      ( *((PurpleGroup *)(cnode -> parent))).online--;
  }
  else {
    purple_signal_emit(purple_blist_get_handle(),"buddy-status-changed",buddy,old_status,status);
  }
/*
	 * This function used to only call the following two functions if one of
	 * the above signals had been triggered, but that's not good, because
	 * if someone's away message changes and they don't go from away to back
	 * to away then no signal is triggered.
	 *
	 * It's a safe assumption that SOMETHING called this function.  PROBABLY
	 * because something, somewhere changed.  Calling the stuff below
	 * certainly won't hurt anything.  Unless you're on a K6-2 300.
	 */
  purple_contact_invalidate_priority_buddy(purple_buddy_get_contact(buddy));
  if ((ops != 0) && ((ops -> update) != 0)) 
    ( *(ops -> update))(purplebuddylist,((PurpleBlistNode *)buddy));
}

void purple_blist_update_node_icon(PurpleBlistNode *node)
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  do {
    if (node != ((PurpleBlistNode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return ;
    };
  }while (0);
  if ((ops != 0) && ((ops -> update) != 0)) 
    ( *(ops -> update))(purplebuddylist,node);
}

void purple_blist_update_buddy_icon(PurpleBuddy *buddy)
{
  purple_blist_update_node_icon(((PurpleBlistNode *)buddy));
}
/*
 * TODO: Maybe remove the call to this from server.c and call it
 * from oscar.c and toc.c instead?
 */

void purple_blist_rename_buddy(PurpleBuddy *buddy,const char *name)
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  struct _purple_hbuddy *hb;
  struct _purple_hbuddy *hb2;
  GHashTable *account_buddies;
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return ;
    };
  }while (0);
  hb = ((struct _purple_hbuddy *)(g_malloc_n(1,(sizeof(struct _purple_hbuddy )))));
  hb -> name = ((gchar *)(purple_normalize((buddy -> account),(buddy -> name))));
  hb -> account = (buddy -> account);
  hb -> group = ( *( *((PurpleBlistNode *)buddy)).parent).parent;
  g_hash_table_remove((purplebuddylist -> buddies),hb);
  account_buddies = (g_hash_table_lookup(buddies_cache,(buddy -> account)));
  g_hash_table_remove(account_buddies,hb);
  hb -> name = g_strdup(purple_normalize((buddy -> account),name));
  g_hash_table_replace((purplebuddylist -> buddies),hb,buddy);
  hb2 = ((struct _purple_hbuddy *)(g_malloc_n(1,(sizeof(struct _purple_hbuddy )))));
  hb2 -> name = g_strdup((hb -> name));
  hb2 -> account = (buddy -> account);
  hb2 -> group = ( *( *((PurpleBlistNode *)buddy)).parent).parent;
  g_hash_table_replace(account_buddies,hb2,buddy);
  g_free((buddy -> name));
  buddy -> name = g_strdup(name);
  if ((ops != 0) && ((ops -> save_node) != 0)) 
    ( *(ops -> save_node))(((PurpleBlistNode *)buddy));
  if ((ops != 0) && ((ops -> update) != 0)) 
    ( *(ops -> update))(purplebuddylist,((PurpleBlistNode *)buddy));
}

static gboolean purple_strings_are_different(const char *one,const char *two)
{
  return !((((one != 0) && (two != 0)) && (g_utf8_collate(one,two) == 0)) || (((one == ((const char *)((void *)0))) || (( *one) == 0)) && ((two == ((const char *)((void *)0))) || (( *two) == 0))));
}

void purple_blist_alias_contact(PurpleContact *contact,const char *alias)
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  PurpleConversation *conv;
  PurpleBlistNode *bnode;
  char *old_alias;
  char *new_alias = (char *)((void *)0);
  do {
    if (contact != ((PurpleContact *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"contact != NULL");
      return ;
    };
  }while (0);
  if ((alias != ((const char *)((void *)0))) && (( *alias) != 0)) 
    new_alias = purple_utf8_strip_unprintables(alias);
  if (!(purple_strings_are_different((contact -> alias),new_alias) != 0)) {
    g_free(new_alias);
    return ;
  }
  old_alias = (contact -> alias);
  if ((new_alias != ((char *)((void *)0))) && (( *new_alias) != 0)) 
    contact -> alias = new_alias;
  else {
    contact -> alias = ((char *)((void *)0));
/* could be "\0" */
    g_free(new_alias);
  }
  if ((ops != 0) && ((ops -> save_node) != 0)) 
    ( *(ops -> save_node))(((PurpleBlistNode *)contact));
  if ((ops != 0) && ((ops -> update) != 0)) 
    ( *(ops -> update))(purplebuddylist,((PurpleBlistNode *)contact));
  for (bnode = ( *((PurpleBlistNode *)contact)).child; bnode != ((PurpleBlistNode *)((void *)0)); bnode = (bnode -> next)) {
    PurpleBuddy *buddy = (PurpleBuddy *)bnode;
    conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,(buddy -> name),(buddy -> account));
    if (conv != 0) 
      purple_conversation_autoset_title(conv);
  }
  purple_signal_emit(purple_blist_get_handle(),"blist-node-aliased",contact,old_alias);
  g_free(old_alias);
}

void purple_blist_alias_chat(PurpleChat *chat,const char *alias)
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  char *old_alias;
  char *new_alias = (char *)((void *)0);
  do {
    if (chat != ((PurpleChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return ;
    };
  }while (0);
  if ((alias != ((const char *)((void *)0))) && (( *alias) != 0)) 
    new_alias = purple_utf8_strip_unprintables(alias);
  if (!(purple_strings_are_different((chat -> alias),new_alias) != 0)) {
    g_free(new_alias);
    return ;
  }
  old_alias = (chat -> alias);
  if ((new_alias != ((char *)((void *)0))) && (( *new_alias) != 0)) 
    chat -> alias = new_alias;
  else {
    chat -> alias = ((char *)((void *)0));
/* could be "\0" */
    g_free(new_alias);
  }
  if ((ops != 0) && ((ops -> save_node) != 0)) 
    ( *(ops -> save_node))(((PurpleBlistNode *)chat));
  if ((ops != 0) && ((ops -> update) != 0)) 
    ( *(ops -> update))(purplebuddylist,((PurpleBlistNode *)chat));
  purple_signal_emit(purple_blist_get_handle(),"blist-node-aliased",chat,old_alias);
  g_free(old_alias);
}

void purple_blist_alias_buddy(PurpleBuddy *buddy,const char *alias)
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  PurpleConversation *conv;
  char *old_alias;
  char *new_alias = (char *)((void *)0);
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return ;
    };
  }while (0);
  if ((alias != ((const char *)((void *)0))) && (( *alias) != 0)) 
    new_alias = purple_utf8_strip_unprintables(alias);
  if (!(purple_strings_are_different((buddy -> alias),new_alias) != 0)) {
    g_free(new_alias);
    return ;
  }
  old_alias = (buddy -> alias);
  if ((new_alias != ((char *)((void *)0))) && (( *new_alias) != 0)) 
    buddy -> alias = new_alias;
  else {
    buddy -> alias = ((char *)((void *)0));
/* could be "\0" */
    g_free(new_alias);
  }
  if ((ops != 0) && ((ops -> save_node) != 0)) 
    ( *(ops -> save_node))(((PurpleBlistNode *)buddy));
  if ((ops != 0) && ((ops -> update) != 0)) 
    ( *(ops -> update))(purplebuddylist,((PurpleBlistNode *)buddy));
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,(buddy -> name),(buddy -> account));
  if (conv != 0) 
    purple_conversation_autoset_title(conv);
  purple_signal_emit(purple_blist_get_handle(),"blist-node-aliased",buddy,old_alias);
  g_free(old_alias);
}

void purple_blist_server_alias_buddy(PurpleBuddy *buddy,const char *alias)
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  PurpleConversation *conv;
  char *old_alias;
  char *new_alias = (char *)((void *)0);
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return ;
    };
  }while (0);
  if (((alias != ((const char *)((void *)0))) && (( *alias) != 0)) && (g_utf8_validate(alias,(-1),0) != 0)) 
    new_alias = purple_utf8_strip_unprintables(alias);
  if (!(purple_strings_are_different((buddy -> server_alias),new_alias) != 0)) {
    g_free(new_alias);
    return ;
  }
  old_alias = (buddy -> server_alias);
  if ((new_alias != ((char *)((void *)0))) && (( *new_alias) != 0)) 
    buddy -> server_alias = new_alias;
  else {
    buddy -> server_alias = ((char *)((void *)0));
/* could be "\0"; */
    g_free(new_alias);
  }
  if ((ops != 0) && ((ops -> save_node) != 0)) 
    ( *(ops -> save_node))(((PurpleBlistNode *)buddy));
  if ((ops != 0) && ((ops -> update) != 0)) 
    ( *(ops -> update))(purplebuddylist,((PurpleBlistNode *)buddy));
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM,(buddy -> name),(buddy -> account));
  if (conv != 0) 
    purple_conversation_autoset_title(conv);
  purple_signal_emit(purple_blist_get_handle(),"blist-node-aliased",buddy,old_alias);
  g_free(old_alias);
}
/*
 * TODO: If merging, prompt the user if they want to merge.
 */

void purple_blist_rename_group(PurpleGroup *source,const char *name)
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  PurpleGroup *dest;
  gchar *old_name;
  gchar *new_name;
  GList *moved_buddies = (GList *)((void *)0);
  GSList *accts;
  do {
    if (source != ((PurpleGroup *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"source != NULL");
      return ;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return ;
    };
  }while (0);
  new_name = purple_utf8_strip_unprintables(name);
  if ((( *new_name) == 0) || (purple_strequal(new_name,(source -> name)) != 0)) {
    g_free(new_name);
    return ;
  }
  dest = purple_find_group(new_name);
  if ((dest != ((PurpleGroup *)((void *)0))) && (purple_utf8_strcasecmp((source -> name),(dest -> name)) != 0)) {
/* We're merging two groups */
    PurpleBlistNode *prev;
    PurpleBlistNode *child;
    PurpleBlistNode *next;
    prev = purple_blist_get_last_child(((PurpleBlistNode *)dest));
    child = ( *((PurpleBlistNode *)source)).child;
/*
		 * TODO: This seems like a dumb way to do this... why not just
		 * append all children from the old group to the end of the new
		 * one?  PRPLs might be expecting to receive an add_buddy() for
		 * each moved buddy...
		 */
    while(child != 0){
      next = (child -> next);
      if ((purple_blist_node_get_type(child)) == PURPLE_BLIST_CONTACT_NODE) {
        PurpleBlistNode *bnode;
        purple_blist_add_contact(((PurpleContact *)child),dest,prev);
        for (bnode = (child -> child); bnode != ((PurpleBlistNode *)((void *)0)); bnode = (bnode -> next)) {
          purple_blist_add_buddy(((PurpleBuddy *)bnode),((PurpleContact *)child),0,(bnode -> prev));
          moved_buddies = g_list_append(moved_buddies,bnode);
        }
        prev = child;
      }
      else if ((purple_blist_node_get_type(child)) == PURPLE_BLIST_CHAT_NODE) {
        purple_blist_add_chat(((PurpleChat *)child),dest,prev);
        prev = child;
      }
      else {
        purple_debug(PURPLE_DEBUG_ERROR,"blist","Unknown child type in group %s\n",(source -> name));
      }
      child = next;
    }
/* Make a copy of the old group name and then delete the old group */
    old_name = g_strdup((source -> name));
    purple_blist_remove_group(source);
    source = dest;
    g_free(new_name);
  }
  else {
/* A simple rename */
    PurpleBlistNode *cnode;
    PurpleBlistNode *bnode;
    gchar *key;
/* Build a GList of all buddies in this group */
    for (cnode = ( *((PurpleBlistNode *)source)).child; cnode != ((PurpleBlistNode *)((void *)0)); cnode = (cnode -> next)) {
      if ((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CONTACT_NODE) 
        for (bnode = (cnode -> child); bnode != ((PurpleBlistNode *)((void *)0)); bnode = (bnode -> next)) 
          moved_buddies = g_list_append(moved_buddies,bnode);
    }
    old_name = (source -> name);
    source -> name = new_name;
    key = g_utf8_collate_key(old_name,(-1));
    g_hash_table_remove(groups_cache,key);
    g_free(key);
    key = g_utf8_collate_key(new_name,(-1));
    g_hash_table_insert(groups_cache,key,source);
  }
/* Save our changes */
  if ((ops != 0) && ((ops -> save_node) != 0)) 
    ( *(ops -> save_node))(((PurpleBlistNode *)source));
/* Update the UI */
  if ((ops != 0) && ((ops -> update) != 0)) 
    ( *(ops -> update))(purplebuddylist,((PurpleBlistNode *)source));
/* Notify all PRPLs */
/* TODO: Is this condition needed?  Seems like it would always be TRUE */
  if ((old_name != 0) && !(purple_strequal((source -> name),old_name) != 0)) {
    for (accts = purple_group_get_accounts(source); accts != 0; accts = g_slist_remove(accts,(accts -> data))) {{
        PurpleAccount *account = (accts -> data);
        PurpleConnection *gc = (PurpleConnection *)((void *)0);
        PurplePlugin *prpl = (PurplePlugin *)((void *)0);
        PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
        GList *l = (GList *)((void *)0);
        GList *buddies = (GList *)((void *)0);
        gc = purple_account_get_connection(account);
        if (gc != 0) 
          prpl = purple_connection_get_prpl(gc);
        if ((gc != 0) && (prpl != 0)) 
          prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
        if (!(prpl_info != 0)) 
          continue; 
        for (l = moved_buddies; l != 0; l = (l -> next)) {
          PurpleBuddy *buddy = (PurpleBuddy *)(l -> data);
          if ((buddy != 0) && ((buddy -> account) == account)) 
            buddies = g_list_append(buddies,((PurpleBlistNode *)buddy));
        }
        if ((prpl_info -> rename_group) != 0) {
          ( *(prpl_info -> rename_group))(gc,old_name,source,buddies);
        }
        else {
          GList *cur;
          GList *groups = (GList *)((void *)0);
/* Make a list of what the groups each buddy is in */
          for (cur = buddies; cur != 0; cur = (cur -> next)) {
            PurpleBlistNode *node = (PurpleBlistNode *)(cur -> data);
            groups = g_list_prepend(groups,( *(node -> parent)).parent);
          }
          purple_account_remove_buddies(account,buddies,groups);
          g_list_free(groups);
          purple_account_add_buddies(account,buddies);
        }
        g_list_free(buddies);
      }
    }
  }
  g_list_free(moved_buddies);
  g_free(old_name);
}
static void purple_blist_node_initialize_settings(PurpleBlistNode *node);

PurpleChat *purple_chat_new(PurpleAccount *account,const char *alias,GHashTable *components)
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  PurpleChat *chat;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  do {
    if (components != ((GHashTable *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"components != NULL");
      return 0;
    };
  }while (0);
  chat = ((PurpleChat *)(g_malloc0_n(1,(sizeof(PurpleChat )))));
  chat -> account = account;
  if ((alias != ((const char *)((void *)0))) && (( *alias) != 0)) 
    chat -> alias = purple_utf8_strip_unprintables(alias);
  chat -> components = components;
  purple_blist_node_initialize_settings(((PurpleBlistNode *)chat));
  ( *((PurpleBlistNode *)chat)).type = PURPLE_BLIST_CHAT_NODE;
  if ((ops != ((PurpleBlistUiOps *)((void *)0))) && ((ops -> new_node) != ((void (*)(PurpleBlistNode *))((void *)0)))) 
    ( *(ops -> new_node))(((PurpleBlistNode *)chat));
{
    PurpleChat *typed_ptr = chat;
    purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurpleChat);
  };
  return chat;
}

void purple_chat_destroy(PurpleChat *chat)
{
  g_hash_table_destroy((chat -> components));
  g_hash_table_destroy(chat -> node.settings);
  g_free((chat -> alias));
  purple_dbus_unregister_pointer(chat);
  g_free(chat);
}

PurpleBuddy *purple_buddy_new(PurpleAccount *account,const char *name,const char *alias)
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  PurpleBuddy *buddy;
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return 0;
    };
  }while (0);
  buddy = ((PurpleBuddy *)(g_malloc0_n(1,(sizeof(PurpleBuddy )))));
  buddy -> account = account;
  buddy -> name = purple_utf8_strip_unprintables(name);
  buddy -> alias = purple_utf8_strip_unprintables(alias);
  buddy -> presence = purple_presence_new_for_buddy(buddy);
  ( *((PurpleBlistNode *)buddy)).type = PURPLE_BLIST_BUDDY_NODE;
  purple_presence_set_status_active((buddy -> presence),"offline",(!0));
  purple_blist_node_initialize_settings(((PurpleBlistNode *)buddy));
  if ((ops != 0) && ((ops -> new_node) != 0)) 
    ( *(ops -> new_node))(((PurpleBlistNode *)buddy));
{
    PurpleBuddy *typed_ptr = buddy;
    purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurpleBuddy);
  };
  return buddy;
}

void purple_buddy_destroy(PurpleBuddy *buddy)
{
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info;
/*
	 * Tell the owner PRPL that we're about to free the buddy so it
	 * can free proto_data
	 */
  prpl = purple_find_prpl(purple_account_get_protocol_id((buddy -> account)));
  if (prpl != 0) {
    prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
    if ((prpl_info != 0) && ((prpl_info -> buddy_free) != 0)) 
      ( *(prpl_info -> buddy_free))(buddy);
  }
/* Delete the node */
  purple_buddy_icon_unref((buddy -> icon));
  g_hash_table_destroy(buddy -> node.settings);
  purple_presence_destroy((buddy -> presence));
  g_free((buddy -> name));
  g_free((buddy -> alias));
  g_free((buddy -> server_alias));
  purple_dbus_unregister_pointer(buddy);
  g_free(buddy);
/* FIXME: Once PurpleBuddy is a GObject, timeout callbacks can
	 * g_object_ref() it when connecting the callback and
	 * g_object_unref() it in the handler.  That way, it won't
	 * get freed while the timeout is pending and this line can
	 * be removed. */
  while(g_source_remove_by_user_data(((gpointer *)buddy)) != 0);
}

void purple_buddy_set_icon(PurpleBuddy *buddy,PurpleBuddyIcon *icon)
{
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return ;
    };
  }while (0);
  if ((buddy -> icon) != icon) {
    purple_buddy_icon_unref((buddy -> icon));
    buddy -> icon = ((icon != ((PurpleBuddyIcon *)((void *)0)))?purple_buddy_icon_ref(icon) : ((struct _PurpleBuddyIcon *)((void *)0)));
  }
  purple_signal_emit(purple_blist_get_handle(),"buddy-icon-changed",buddy);
  purple_blist_update_node_icon(((PurpleBlistNode *)buddy));
}

PurpleAccount *purple_buddy_get_account(const PurpleBuddy *buddy)
{
  do {
    if (buddy != ((const PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return 0;
    };
  }while (0);
  return buddy -> account;
}

const char *purple_buddy_get_name(const PurpleBuddy *buddy)
{
  do {
    if (buddy != ((const PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return 0;
    };
  }while (0);
  return (buddy -> name);
}

PurpleBuddyIcon *purple_buddy_get_icon(const PurpleBuddy *buddy)
{
  do {
    if (buddy != ((const PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return 0;
    };
  }while (0);
  return buddy -> icon;
}

gpointer purple_buddy_get_protocol_data(const PurpleBuddy *buddy)
{
  do {
    if (buddy != ((const PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return 0;
    };
  }while (0);
  return buddy -> proto_data;
}

void purple_buddy_set_protocol_data(PurpleBuddy *buddy,gpointer data)
{
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return ;
    };
  }while (0);
  buddy -> proto_data = data;
}

void purple_blist_add_chat(PurpleChat *chat,PurpleGroup *group,PurpleBlistNode *node)
{
  PurpleBlistNode *cnode = (PurpleBlistNode *)chat;
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  do {
    if (chat != ((PurpleChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return ;
    };
  }while (0);
  do {
    if ((purple_blist_node_get_type(((PurpleBlistNode *)chat))) == PURPLE_BLIST_CHAT_NODE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_BLIST_NODE_IS_CHAT((PurpleBlistNode *)chat)");
      return ;
    };
  }while (0);
  if (node == ((PurpleBlistNode *)((void *)0))) {
    if (group == ((PurpleGroup *)((void *)0))) 
      group = purple_group_new(((const char *)(dgettext("pidgin","Chats"))));
/* Add group to blist if isn't already on it. Fixes #2752. */
    if (!(purple_find_group((group -> name)) != 0)) {
      purple_blist_add_group(group,purple_blist_get_last_sibling((purplebuddylist -> root)));
    }
  }
  else {
    group = ((PurpleGroup *)(node -> parent));
  }
/* if we're moving to overtop of ourselves, do nothing */
  if (cnode == node) 
    return ;
  if ((cnode -> parent) != 0) {
/* This chat was already in the list and is
		 * being moved.
		 */
    ( *((PurpleGroup *)(cnode -> parent))).totalsize--;
    if (purple_account_is_connected((chat -> account)) != 0) {
      ( *((PurpleGroup *)(cnode -> parent))).online--;
      ( *((PurpleGroup *)(cnode -> parent))).currentsize--;
    }
    if ((cnode -> next) != 0) 
      ( *(cnode -> next)).prev = (cnode -> prev);
    if ((cnode -> prev) != 0) 
      ( *(cnode -> prev)).next = (cnode -> next);
    if (( *(cnode -> parent)).child == cnode) 
      ( *(cnode -> parent)).child = (cnode -> next);
    if ((ops != 0) && ((ops -> remove) != 0)) 
      ( *(ops -> remove))(purplebuddylist,cnode);
/* ops->remove() cleaned up the cnode's ui_data, so we need to
		 * reinitialize it */
    if ((ops != 0) && ((ops -> new_node) != 0)) 
      ( *(ops -> new_node))(cnode);
  }
  if (node != ((PurpleBlistNode *)((void *)0))) {
    if ((node -> next) != 0) 
      ( *(node -> next)).prev = cnode;
    cnode -> next = (node -> next);
    cnode -> prev = node;
    cnode -> parent = (node -> parent);
    node -> next = cnode;
    ( *((PurpleGroup *)(node -> parent))).totalsize++;
    if (purple_account_is_connected((chat -> account)) != 0) {
      ( *((PurpleGroup *)(node -> parent))).online++;
      ( *((PurpleGroup *)(node -> parent))).currentsize++;
    }
  }
  else {
    if (( *((PurpleBlistNode *)group)).child != 0) 
      ( *( *((PurpleBlistNode *)group)).child).prev = cnode;
    cnode -> next = ( *((PurpleBlistNode *)group)).child;
    cnode -> prev = ((PurpleBlistNode *)((void *)0));
    ( *((PurpleBlistNode *)group)).child = cnode;
    cnode -> parent = ((PurpleBlistNode *)group);
    group -> totalsize++;
    if (purple_account_is_connected((chat -> account)) != 0) {
      group -> online++;
      group -> currentsize++;
    }
  }
  if ((ops != 0) && ((ops -> save_node) != 0)) 
    ( *(ops -> save_node))(cnode);
  if ((ops != 0) && ((ops -> update) != 0)) 
    ( *(ops -> update))(purplebuddylist,((PurpleBlistNode *)cnode));
  purple_signal_emit(purple_blist_get_handle(),"blist-node-added",cnode);
}

void purple_blist_add_buddy(PurpleBuddy *buddy,PurpleContact *contact,PurpleGroup *group,PurpleBlistNode *node)
{
  PurpleBlistNode *cnode;
  PurpleBlistNode *bnode;
  PurpleGroup *g;
  PurpleContact *c;
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  struct _purple_hbuddy *hb;
  struct _purple_hbuddy *hb2;
  GHashTable *account_buddies;
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return ;
    };
  }while (0);
  do {
    if ((purple_blist_node_get_type(((PurpleBlistNode *)buddy))) == PURPLE_BLIST_BUDDY_NODE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_BLIST_NODE_IS_BUDDY((PurpleBlistNode*)buddy)");
      return ;
    };
  }while (0);
  bnode = ((PurpleBlistNode *)buddy);
/* if we're moving to overtop of ourselves, do nothing */
  if ((bnode == node) || ((((!(node != 0) && ((bnode -> parent) != 0)) && (contact != 0)) && ((bnode -> parent) == ((PurpleBlistNode *)contact))) && (bnode == ( *(bnode -> parent)).child))) 
    return ;
  if ((node != 0) && ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)) {
    c = ((PurpleContact *)(node -> parent));
    g = ((PurpleGroup *)( *(node -> parent)).parent);
  }
  else if (contact != 0) {
    c = contact;
    g = ((PurpleGroup *)( *((PurpleBlistNode *)c)).parent);
  }
  else {
    g = group;
    if (g == ((PurpleGroup *)((void *)0))) 
      g = purple_group_new(((const char *)(dgettext("pidgin","Buddies"))));
/* Add group to blist if isn't already on it. Fixes #2752. */
    if (!(purple_find_group((g -> name)) != 0)) {
      purple_blist_add_group(g,purple_blist_get_last_sibling((purplebuddylist -> root)));
    }
    c = purple_contact_new();
    purple_blist_add_contact(c,g,purple_blist_get_last_child(((PurpleBlistNode *)g)));
  }
  cnode = ((PurpleBlistNode *)c);
  if ((bnode -> parent) != 0) {
    if (((buddy != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0)) {
      ( *((PurpleContact *)(bnode -> parent))).online--;
      if (( *((PurpleContact *)(bnode -> parent))).online == 0) 
        ( *((PurpleGroup *)( *(bnode -> parent)).parent)).online--;
    }
    if (purple_account_is_connected((buddy -> account)) != 0) {
      ( *((PurpleContact *)(bnode -> parent))).currentsize--;
      if (( *((PurpleContact *)(bnode -> parent))).currentsize == 0) 
        ( *((PurpleGroup *)( *(bnode -> parent)).parent)).currentsize--;
    }
    ( *((PurpleContact *)(bnode -> parent))).totalsize--;
/* the group totalsize will be taken care of by remove_contact below */
    if (( *(bnode -> parent)).parent != ((PurpleBlistNode *)g)) 
      serv_move_buddy(buddy,((PurpleGroup *)( *(bnode -> parent)).parent),g);
    if ((bnode -> next) != 0) 
      ( *(bnode -> next)).prev = (bnode -> prev);
    if ((bnode -> prev) != 0) 
      ( *(bnode -> prev)).next = (bnode -> next);
    if (( *(bnode -> parent)).child == bnode) 
      ( *(bnode -> parent)).child = (bnode -> next);
    if ((ops != 0) && ((ops -> remove) != 0)) 
      ( *(ops -> remove))(purplebuddylist,bnode);
    if (( *(bnode -> parent)).parent != ((PurpleBlistNode *)g)) {
      struct _purple_hbuddy hb;
      hb.name = ((gchar *)(purple_normalize((buddy -> account),(buddy -> name))));
      hb.account = (buddy -> account);
      hb.group = ( *(bnode -> parent)).parent;
      g_hash_table_remove((purplebuddylist -> buddies),(&hb));
      account_buddies = (g_hash_table_lookup(buddies_cache,(buddy -> account)));
      g_hash_table_remove(account_buddies,(&hb));
    }
    if (!(( *(bnode -> parent)).child != 0)) {
      purple_blist_remove_contact(((PurpleContact *)(bnode -> parent)));
    }
    else {
      purple_contact_invalidate_priority_buddy(((PurpleContact *)(bnode -> parent)));
      if ((ops != 0) && ((ops -> update) != 0)) 
        ( *(ops -> update))(purplebuddylist,(bnode -> parent));
    }
  }
  if ((node != 0) && ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE)) {
    if ((node -> next) != 0) 
      ( *(node -> next)).prev = bnode;
    bnode -> next = (node -> next);
    bnode -> prev = node;
    bnode -> parent = (node -> parent);
    node -> next = bnode;
  }
  else {
    if ((cnode -> child) != 0) 
      ( *(cnode -> child)).prev = bnode;
    bnode -> prev = ((PurpleBlistNode *)((void *)0));
    bnode -> next = (cnode -> child);
    cnode -> child = bnode;
    bnode -> parent = cnode;
  }
  if (((buddy != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0)) {
    if (++( *((PurpleContact *)(bnode -> parent))).online == 1) 
      ( *((PurpleGroup *)( *(bnode -> parent)).parent)).online++;
  }
  if (purple_account_is_connected((buddy -> account)) != 0) {
    if (++( *((PurpleContact *)(bnode -> parent))).currentsize == 1) 
      ( *((PurpleGroup *)( *(bnode -> parent)).parent)).currentsize++;
  }
  ( *((PurpleContact *)(bnode -> parent))).totalsize++;
  hb = ((struct _purple_hbuddy *)(g_malloc_n(1,(sizeof(struct _purple_hbuddy )))));
  hb -> name = g_strdup(purple_normalize((buddy -> account),(buddy -> name)));
  hb -> account = (buddy -> account);
  hb -> group = ( *( *((PurpleBlistNode *)buddy)).parent).parent;
  g_hash_table_replace((purplebuddylist -> buddies),hb,buddy);
  account_buddies = (g_hash_table_lookup(buddies_cache,(buddy -> account)));
  hb2 = ((struct _purple_hbuddy *)(g_malloc_n(1,(sizeof(struct _purple_hbuddy )))));
  hb2 -> name = g_strdup((hb -> name));
  hb2 -> account = (buddy -> account);
  hb2 -> group = ( *( *((PurpleBlistNode *)buddy)).parent).parent;
  g_hash_table_replace(account_buddies,hb2,buddy);
  purple_contact_invalidate_priority_buddy(purple_buddy_get_contact(buddy));
  if ((ops != 0) && ((ops -> save_node) != 0)) 
    ( *(ops -> save_node))(((PurpleBlistNode *)buddy));
  if ((ops != 0) && ((ops -> update) != 0)) 
    ( *(ops -> update))(purplebuddylist,((PurpleBlistNode *)buddy));
/* Signal that the buddy has been added */
  purple_signal_emit(purple_blist_get_handle(),"buddy-added",buddy);
  purple_signal_emit(purple_blist_get_handle(),"blist-node-added",((PurpleBlistNode *)buddy));
}

PurpleContact *purple_contact_new()
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  PurpleContact *contact = (PurpleContact *)(g_malloc0_n(1,(sizeof(PurpleContact ))));
  contact -> totalsize = 0;
  contact -> currentsize = 0;
  contact -> online = 0;
  purple_blist_node_initialize_settings(((PurpleBlistNode *)contact));
  ( *((PurpleBlistNode *)contact)).type = PURPLE_BLIST_CONTACT_NODE;
  if ((ops != 0) && ((ops -> new_node) != 0)) 
    ( *(ops -> new_node))(((PurpleBlistNode *)contact));
{
    PurpleContact *typed_ptr = contact;
    purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurpleContact);
  };
  return contact;
}

void purple_contact_destroy(PurpleContact *contact)
{
  g_hash_table_destroy(contact -> node.settings);
  g_free((contact -> alias));
  purple_dbus_unregister_pointer(contact);
  g_free(contact);
}

PurpleGroup *purple_contact_get_group(const PurpleContact *contact)
{
  do {
    if (contact != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"contact");
      return 0;
    };
  }while (0);
  return (PurpleGroup *)( *((PurpleBlistNode *)contact)).parent;
}

void purple_contact_set_alias(PurpleContact *contact,const char *alias)
{
  purple_blist_alias_contact(contact,alias);
}

const char *purple_contact_get_alias(PurpleContact *contact)
{
  do {
    if (contact != ((PurpleContact *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"contact != NULL");
      return 0;
    };
  }while (0);
  if ((contact -> alias) != 0) 
    return (contact -> alias);
  return purple_buddy_get_alias(purple_contact_get_priority_buddy(contact));
}

gboolean purple_contact_on_account(PurpleContact *c,PurpleAccount *account)
{
  PurpleBlistNode *bnode;
  PurpleBlistNode *cnode = (PurpleBlistNode *)c;
  do {
    if (c != ((PurpleContact *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"c != NULL");
      return 0;
    };
  }while (0);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  for (bnode = (cnode -> child); bnode != 0; bnode = (bnode -> next)) {{
      PurpleBuddy *buddy;
      if (!((purple_blist_node_get_type(bnode)) == PURPLE_BLIST_BUDDY_NODE)) 
        continue; 
      buddy = ((PurpleBuddy *)bnode);
      if ((buddy -> account) == account) 
        return (!0);
    }
  }
  return 0;
}

void purple_contact_invalidate_priority_buddy(PurpleContact *contact)
{
  do {
    if (contact != ((PurpleContact *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"contact != NULL");
      return ;
    };
  }while (0);
  contact -> priority_valid = 0;
}

PurpleGroup *purple_group_new(const char *name)
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  PurpleGroup *group;
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return 0;
    };
  }while (0);
  do {
    if (( *name) != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"*name != \'\\0\'");
      return 0;
    };
  }while (0);
  group = purple_find_group(name);
  if (group != ((PurpleGroup *)((void *)0))) 
    return group;
  group = ((PurpleGroup *)(g_malloc0_n(1,(sizeof(PurpleGroup )))));
  group -> name = purple_utf8_strip_unprintables(name);
  group -> totalsize = 0;
  group -> currentsize = 0;
  group -> online = 0;
  purple_blist_node_initialize_settings(((PurpleBlistNode *)group));
  ( *((PurpleBlistNode *)group)).type = PURPLE_BLIST_GROUP_NODE;
  if ((ops != 0) && ((ops -> new_node) != 0)) 
    ( *(ops -> new_node))(((PurpleBlistNode *)group));
{
    PurpleGroup *typed_ptr = group;
    purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurpleGroup);
  };
  return group;
}

void purple_group_destroy(PurpleGroup *group)
{
  g_hash_table_destroy(group -> node.settings);
  g_free((group -> name));
  purple_dbus_unregister_pointer(group);
  g_free(group);
}

void purple_blist_add_contact(PurpleContact *contact,PurpleGroup *group,PurpleBlistNode *node)
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  PurpleGroup *g;
  PurpleBlistNode *gnode;
  PurpleBlistNode *cnode;
  PurpleBlistNode *bnode;
  do {
    if (contact != ((PurpleContact *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"contact != NULL");
      return ;
    };
  }while (0);
  do {
    if ((purple_blist_node_get_type(((PurpleBlistNode *)contact))) == PURPLE_BLIST_CONTACT_NODE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_BLIST_NODE_IS_CONTACT((PurpleBlistNode*)contact)");
      return ;
    };
  }while (0);
  if (((PurpleBlistNode *)contact) == node) 
    return ;
  if ((node != 0) && (((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE))) 
    g = ((PurpleGroup *)(node -> parent));
  else if (group != 0) 
    g = group;
  else {
    g = purple_find_group(((const char *)(dgettext("pidgin","Buddies"))));
    if (g == ((PurpleGroup *)((void *)0))) {
      g = purple_group_new(((const char *)(dgettext("pidgin","Buddies"))));
      purple_blist_add_group(g,purple_blist_get_last_sibling((purplebuddylist -> root)));
    }
  }
  gnode = ((PurpleBlistNode *)g);
  cnode = ((PurpleBlistNode *)contact);
  if ((cnode -> parent) != 0) {
    if (( *(cnode -> parent)).child == cnode) 
      ( *(cnode -> parent)).child = (cnode -> next);
    if ((cnode -> prev) != 0) 
      ( *(cnode -> prev)).next = (cnode -> next);
    if ((cnode -> next) != 0) 
      ( *(cnode -> next)).prev = (cnode -> prev);
    if ((cnode -> parent) != gnode) {
      bnode = (cnode -> child);
      while(bnode != 0){
        PurpleBlistNode *next_bnode = (bnode -> next);
        PurpleBuddy *b = (PurpleBuddy *)bnode;
        GHashTable *account_buddies;
        struct _purple_hbuddy *hb;
        struct _purple_hbuddy *hb2;
        hb = ((struct _purple_hbuddy *)(g_malloc_n(1,(sizeof(struct _purple_hbuddy )))));
        hb -> name = g_strdup(purple_normalize((b -> account),(b -> name)));
        hb -> account = (b -> account);
        hb -> group = (cnode -> parent);
        g_hash_table_remove((purplebuddylist -> buddies),hb);
        account_buddies = (g_hash_table_lookup(buddies_cache,(b -> account)));
        g_hash_table_remove(account_buddies,hb);
        if (!(purple_find_buddy_in_group((b -> account),(b -> name),g) != 0)) {
          hb -> group = gnode;
          g_hash_table_replace((purplebuddylist -> buddies),hb,b);
          hb2 = ((struct _purple_hbuddy *)(g_malloc_n(1,(sizeof(struct _purple_hbuddy )))));
          hb2 -> name = g_strdup((hb -> name));
          hb2 -> account = (b -> account);
          hb2 -> group = gnode;
          g_hash_table_replace(account_buddies,hb2,b);
          if (purple_account_get_connection((b -> account)) != 0) 
            serv_move_buddy(b,((PurpleGroup *)(cnode -> parent)),g);
        }
        else {
          gboolean empty_contact = 0;
/* this buddy already exists in the group, so we're
					 * gonna delete it instead */
          g_free((hb -> name));
          g_free(hb);
          if (purple_account_get_connection((b -> account)) != 0) 
            purple_account_remove_buddy((b -> account),b,((PurpleGroup *)(cnode -> parent)));
          if (!(( *(cnode -> child)).next != 0)) 
            empty_contact = (!0);
          purple_blist_remove_buddy(b);
/** in purple_blist_remove_buddy(), if the last buddy in a
					 * contact is removed, the contact is cleaned up and
					 * g_free'd, so we mustn't try to reference bnode->next */
          if (empty_contact != 0) 
            return ;
        }
        bnode = next_bnode;
      }
    }
    if ((contact -> online) > 0) 
      ( *((PurpleGroup *)(cnode -> parent))).online--;
    if ((contact -> currentsize) > 0) 
      ( *((PurpleGroup *)(cnode -> parent))).currentsize--;
    ( *((PurpleGroup *)(cnode -> parent))).totalsize--;
    if ((ops != 0) && ((ops -> remove) != 0)) 
      ( *(ops -> remove))(purplebuddylist,cnode);
    if ((ops != 0) && ((ops -> remove_node) != 0)) 
      ( *(ops -> remove_node))(cnode);
  }
  if ((node != 0) && (((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) || ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE))) {
    if ((node -> next) != 0) 
      ( *(node -> next)).prev = cnode;
    cnode -> next = (node -> next);
    cnode -> prev = node;
    cnode -> parent = (node -> parent);
    node -> next = cnode;
  }
  else {
    if ((gnode -> child) != 0) 
      ( *(gnode -> child)).prev = cnode;
    cnode -> prev = ((PurpleBlistNode *)((void *)0));
    cnode -> next = (gnode -> child);
    gnode -> child = cnode;
    cnode -> parent = gnode;
  }
  if ((contact -> online) > 0) 
    g -> online++;
  if ((contact -> currentsize) > 0) 
    g -> currentsize++;
  g -> totalsize++;
  if ((ops != 0) && ((ops -> save_node) != 0)) {
    if ((cnode -> child) != 0) 
      ( *(ops -> save_node))(cnode);
    for (bnode = (cnode -> child); bnode != 0; bnode = (bnode -> next)) 
      ( *(ops -> save_node))(bnode);
  }
  if ((ops != 0) && ((ops -> update) != 0)) {
    if ((cnode -> child) != 0) 
      ( *(ops -> update))(purplebuddylist,cnode);
    for (bnode = (cnode -> child); bnode != 0; bnode = (bnode -> next)) 
      ( *(ops -> update))(purplebuddylist,bnode);
  }
}

void purple_blist_merge_contact(PurpleContact *source,PurpleBlistNode *node)
{
  PurpleBlistNode *sourcenode = (PurpleBlistNode *)source;
  PurpleBlistNode *prev;
  PurpleBlistNode *cur;
  PurpleBlistNode *next;
  PurpleContact *target;
  do {
    if (source != ((PurpleContact *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"source != NULL");
      return ;
    };
  }while (0);
  do {
    if (node != ((PurpleBlistNode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return ;
    };
  }while (0);
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) {
    target = ((PurpleContact *)node);
    prev = purple_blist_get_last_child(node);
  }
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) {
    target = ((PurpleContact *)(node -> parent));
    prev = node;
  }
  else {
    return ;
  }
  if ((source == target) || !(target != 0)) 
    return ;
  next = (sourcenode -> child);
  while(next != 0){
    cur = next;
    next = (cur -> next);
    if ((purple_blist_node_get_type(cur)) == PURPLE_BLIST_BUDDY_NODE) {
      purple_blist_add_buddy(((PurpleBuddy *)cur),target,0,prev);
      prev = cur;
    }
  }
}

void purple_blist_add_group(PurpleGroup *group,PurpleBlistNode *node)
{
  PurpleBlistUiOps *ops;
  PurpleBlistNode *gnode = (PurpleBlistNode *)group;
  gchar *key;
  do {
    if (group != ((PurpleGroup *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"group != NULL");
      return ;
    };
  }while (0);
  do {
    if ((purple_blist_node_get_type(((PurpleBlistNode *)group))) == PURPLE_BLIST_GROUP_NODE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_BLIST_NODE_IS_GROUP((PurpleBlistNode *)group)");
      return ;
    };
  }while (0);
  ops = purple_blist_get_ui_ops();
/* if we're moving to overtop of ourselves, do nothing */
  if (gnode == node) {
    if (!((purplebuddylist -> root) != 0)) 
      node = ((PurpleBlistNode *)((void *)0));
    else 
      return ;
  }
  if (purple_find_group((group -> name)) != 0) {
/* This is just being moved */
    if ((ops != 0) && ((ops -> remove) != 0)) 
      ( *(ops -> remove))(purplebuddylist,((PurpleBlistNode *)group));
    if (gnode == (purplebuddylist -> root)) 
      purplebuddylist -> root = (gnode -> next);
    if ((gnode -> prev) != 0) 
      ( *(gnode -> prev)).next = (gnode -> next);
    if ((gnode -> next) != 0) 
      ( *(gnode -> next)).prev = (gnode -> prev);
  }
  else {
    key = g_utf8_collate_key((group -> name),(-1));
    g_hash_table_insert(groups_cache,key,group);
  }
  if ((node != 0) && ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE)) {
    gnode -> next = (node -> next);
    gnode -> prev = node;
    if ((node -> next) != 0) 
      ( *(node -> next)).prev = gnode;
    node -> next = gnode;
  }
  else {
    if ((purplebuddylist -> root) != 0) 
      ( *(purplebuddylist -> root)).prev = gnode;
    gnode -> next = (purplebuddylist -> root);
    gnode -> prev = ((PurpleBlistNode *)((void *)0));
    purplebuddylist -> root = gnode;
  }
  if ((ops != 0) && ((ops -> save_node) != 0)) {
    ( *(ops -> save_node))(gnode);
    for (node = (gnode -> child); node != 0; node = (node -> next)) 
      ( *(ops -> save_node))(node);
  }
  if ((ops != 0) && ((ops -> update) != 0)) {
    ( *(ops -> update))(purplebuddylist,gnode);
    for (node = (gnode -> child); node != 0; node = (node -> next)) 
      ( *(ops -> update))(purplebuddylist,node);
  }
  purple_signal_emit(purple_blist_get_handle(),"blist-node-added",gnode);
}

void purple_blist_remove_contact(PurpleContact *contact)
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  PurpleBlistNode *node;
  PurpleBlistNode *gnode;
  do {
    if (contact != ((PurpleContact *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"contact != NULL");
      return ;
    };
  }while (0);
  node = ((PurpleBlistNode *)contact);
  gnode = (node -> parent);
  if ((node -> child) != 0) {
/*
		 * If this contact has children then remove them.  When the last
		 * buddy is removed from the contact, the contact is automatically
		 * deleted.
		 */
    while(( *(node -> child)).next != 0){
      purple_blist_remove_buddy(((PurpleBuddy *)(node -> child)));
    }
/*
		 * Remove the last buddy and trigger the deletion of the contact.
		 * It would probably be cleaner if contact-deletion was done after
		 * a timeout?  Or if it had to be done manually, like below?
		 */
    purple_blist_remove_buddy(((PurpleBuddy *)(node -> child)));
  }
  else {
/* Remove the node from its parent */
    if ((gnode -> child) == node) 
      gnode -> child = (node -> next);
    if ((node -> prev) != 0) 
      ( *(node -> prev)).next = (node -> next);
    if ((node -> next) != 0) 
      ( *(node -> next)).prev = (node -> prev);
/* Update the UI */
    if ((ops != 0) && ((ops -> remove) != 0)) 
      ( *(ops -> remove))(purplebuddylist,node);
    if ((ops != 0) && ((ops -> remove_node) != 0)) 
      ( *(ops -> remove_node))(node);
    purple_signal_emit(purple_blist_get_handle(),"blist-node-removed",((PurpleBlistNode *)contact));
/* Delete the node */
    purple_contact_destroy(contact);
  }
}

void purple_blist_remove_buddy(PurpleBuddy *buddy)
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  PurpleBlistNode *node;
  PurpleBlistNode *cnode;
  PurpleBlistNode *gnode;
  PurpleContact *contact;
  PurpleGroup *group;
  struct _purple_hbuddy hb;
  GHashTable *account_buddies;
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return ;
    };
  }while (0);
  node = ((PurpleBlistNode *)buddy);
  cnode = (node -> parent);
  gnode = ((cnode != ((PurpleBlistNode *)((void *)0)))?(cnode -> parent) : ((struct _PurpleBlistNode *)((void *)0)));
  contact = ((PurpleContact *)cnode);
  group = ((PurpleGroup *)gnode);
/* Remove the node from its parent */
  if ((node -> prev) != 0) 
    ( *(node -> prev)).next = (node -> next);
  if ((node -> next) != 0) 
    ( *(node -> next)).prev = (node -> prev);
  if ((cnode != ((PurpleBlistNode *)((void *)0))) && ((cnode -> child) == node)) 
    cnode -> child = (node -> next);
/* Adjust size counts */
  if (contact != ((PurpleContact *)((void *)0))) {
    if (((buddy != ((PurpleBuddy *)((void *)0))) && (purple_account_is_connected((purple_buddy_get_account(buddy))) != 0)) && (purple_presence_is_online((purple_buddy_get_presence(buddy))) != 0)) {
      contact -> online--;
      if ((contact -> online) == 0) 
        group -> online--;
    }
    if (purple_account_is_connected((buddy -> account)) != 0) {
      contact -> currentsize--;
      if ((contact -> currentsize) == 0) 
        group -> currentsize--;
    }
    contact -> totalsize--;
/* Re-sort the contact */
    if (((cnode -> child) != 0) && ((contact -> priority) == buddy)) {
      purple_contact_invalidate_priority_buddy(contact);
      if ((ops != 0) && ((ops -> update) != 0)) 
        ( *(ops -> update))(purplebuddylist,cnode);
    }
  }
/* Remove this buddy from the buddies hash table */
  hb.name = ((gchar *)(purple_normalize((buddy -> account),(buddy -> name))));
  hb.account = (buddy -> account);
  hb.group = gnode;
  g_hash_table_remove((purplebuddylist -> buddies),(&hb));
  account_buddies = (g_hash_table_lookup(buddies_cache,(buddy -> account)));
  g_hash_table_remove(account_buddies,(&hb));
/* Update the UI */
  if ((ops != 0) && ((ops -> remove) != 0)) 
    ( *(ops -> remove))(purplebuddylist,node);
  if ((ops != 0) && ((ops -> remove_node) != 0)) 
    ( *(ops -> remove_node))(node);
/* Remove this buddy's pounces */
  purple_pounce_destroy_all_by_buddy(buddy);
/* Signal that the buddy has been removed before freeing the memory for it */
  purple_signal_emit(purple_blist_get_handle(),"buddy-removed",buddy);
  purple_signal_emit(purple_blist_get_handle(),"blist-node-removed",((PurpleBlistNode *)buddy));
  purple_buddy_destroy(buddy);
/* If the contact is empty then remove it */
  if ((contact != ((PurpleContact *)((void *)0))) && !((cnode -> child) != 0)) 
    purple_blist_remove_contact(contact);
}

void purple_blist_remove_chat(PurpleChat *chat)
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  PurpleBlistNode *node;
  PurpleBlistNode *gnode;
  PurpleGroup *group;
  do {
    if (chat != ((PurpleChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return ;
    };
  }while (0);
  node = ((PurpleBlistNode *)chat);
  gnode = (node -> parent);
  group = ((PurpleGroup *)gnode);
  if (gnode != ((PurpleBlistNode *)((void *)0))) {
/* Remove the node from its parent */
    if ((gnode -> child) == node) 
      gnode -> child = (node -> next);
    if ((node -> prev) != 0) 
      ( *(node -> prev)).next = (node -> next);
    if ((node -> next) != 0) 
      ( *(node -> next)).prev = (node -> prev);
/* Adjust size counts */
    if (purple_account_is_connected((chat -> account)) != 0) {
      group -> online--;
      group -> currentsize--;
    }
    group -> totalsize--;
  }
/* Update the UI */
  if ((ops != 0) && ((ops -> remove) != 0)) 
    ( *(ops -> remove))(purplebuddylist,node);
  if ((ops != 0) && ((ops -> remove_node) != 0)) 
    ( *(ops -> remove_node))(node);
  purple_signal_emit(purple_blist_get_handle(),"blist-node-removed",((PurpleBlistNode *)chat));
/* Delete the node */
  purple_chat_destroy(chat);
}

void purple_blist_remove_group(PurpleGroup *group)
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  PurpleBlistNode *node;
  GList *l;
  gchar *key;
  do {
    if (group != ((PurpleGroup *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"group != NULL");
      return ;
    };
  }while (0);
  node = ((PurpleBlistNode *)group);
/* Make sure the group is empty */
  if ((node -> child) != 0) 
    return ;
/* Remove the node from its parent */
  if ((purplebuddylist -> root) == node) 
    purplebuddylist -> root = (node -> next);
  if ((node -> prev) != 0) 
    ( *(node -> prev)).next = (node -> next);
  if ((node -> next) != 0) 
    ( *(node -> next)).prev = (node -> prev);
  key = g_utf8_collate_key((group -> name),(-1));
  g_hash_table_remove(groups_cache,key);
  g_free(key);
/* Update the UI */
  if ((ops != 0) && ((ops -> remove) != 0)) 
    ( *(ops -> remove))(purplebuddylist,node);
  if ((ops != 0) && ((ops -> remove_node) != 0)) 
    ( *(ops -> remove_node))(node);
  purple_signal_emit(purple_blist_get_handle(),"blist-node-removed",((PurpleBlistNode *)group));
/* Remove the group from all accounts that are online */
  for (l = purple_connections_get_all(); l != ((GList *)((void *)0)); l = (l -> next)) {
    PurpleConnection *gc = (PurpleConnection *)(l -> data);
    if ((purple_connection_get_state(gc)) == PURPLE_CONNECTED) 
      purple_account_remove_group(purple_connection_get_account(gc),group);
  }
/* Delete the node */
  purple_group_destroy(group);
}

PurpleBuddy *purple_contact_get_priority_buddy(PurpleContact *contact)
{
  do {
    if (contact != ((PurpleContact *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"contact != NULL");
      return 0;
    };
  }while (0);
  if (!((contact -> priority_valid) != 0)) 
    purple_contact_compute_priority_buddy(contact);
  return contact -> priority;
}

const char *purple_buddy_get_alias_only(PurpleBuddy *buddy)
{
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return 0;
    };
  }while (0);
  if (((buddy -> alias) != ((char *)((void *)0))) && (( *(buddy -> alias)) != 0)) {
    return (buddy -> alias);
  }
  else if (((buddy -> server_alias) != ((char *)((void *)0))) && (( *(buddy -> server_alias)) != 0)) {
    return (buddy -> server_alias);
  }
  return 0;
}

const char *purple_buddy_get_contact_alias(PurpleBuddy *buddy)
{
  PurpleContact *c;
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return 0;
    };
  }while (0);
/* Search for an alias for the buddy. In order of precedence: */
/* The buddy alias */
  if ((buddy -> alias) != ((char *)((void *)0))) 
    return (buddy -> alias);
/* The contact alias */
  c = purple_buddy_get_contact(buddy);
  if ((c != ((PurpleContact *)((void *)0))) && ((c -> alias) != ((char *)((void *)0)))) 
    return (c -> alias);
/* The server alias */
  if (((buddy -> server_alias) != 0) && (( *(buddy -> server_alias)) != 0)) 
    return (buddy -> server_alias);
/* The buddy's user name (i.e. no alias) */
  return (buddy -> name);
}

const char *purple_buddy_get_alias(PurpleBuddy *buddy)
{
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return 0;
    };
  }while (0);
/* Search for an alias for the buddy. In order of precedence: */
/* The buddy alias */
  if ((buddy -> alias) != ((char *)((void *)0))) 
    return (buddy -> alias);
/* The server alias */
  if (((buddy -> server_alias) != 0) && (( *(buddy -> server_alias)) != 0)) 
    return (buddy -> server_alias);
/* The buddy's user name (i.e. no alias) */
  return (buddy -> name);
}

const char *purple_buddy_get_local_buddy_alias(PurpleBuddy *buddy)
{
  do {
    if (buddy != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy");
      return 0;
    };
  }while (0);
  return (buddy -> alias);
}

const char *purple_buddy_get_server_alias(PurpleBuddy *buddy)
{
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return 0;
    };
  }while (0);
  if (((buddy -> server_alias) != 0) && (( *(buddy -> server_alias)) != 0)) 
    return (buddy -> server_alias);
  return 0;
}

const char *purple_buddy_get_local_alias(PurpleBuddy *buddy)
{
  PurpleContact *c;
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return 0;
    };
  }while (0);
/* Search for an alias for the buddy. In order of precedence: */
/* The buddy alias */
  if ((buddy -> alias) != ((char *)((void *)0))) 
    return (buddy -> alias);
/* The contact alias */
  c = purple_buddy_get_contact(buddy);
  if ((c != ((PurpleContact *)((void *)0))) && ((c -> alias) != ((char *)((void *)0)))) 
    return (c -> alias);
/* The buddy's user name (i.e. no alias) */
  return (buddy -> name);
}

const char *purple_chat_get_name(PurpleChat *chat)
{
  char *ret = (char *)((void *)0);
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  do {
    if (chat != ((PurpleChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return 0;
    };
  }while (0);
  if (((chat -> alias) != ((char *)((void *)0))) && (( *(chat -> alias)) != 0)) 
    return (chat -> alias);
  prpl = purple_find_prpl(purple_account_get_protocol_id((chat -> account)));
  prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if ((prpl_info -> chat_info) != 0) {
    struct proto_chat_entry *pce;
    GList *parts = ( *(prpl_info -> chat_info))(purple_account_get_connection((chat -> account)));
    pce = (parts -> data);
    ret = (g_hash_table_lookup((chat -> components),(pce -> identifier)));
    g_list_foreach(parts,((GFunc )g_free),0);
    g_list_free(parts);
  }
  return ret;
}

PurpleBuddy *purple_find_buddy(PurpleAccount *account,const char *name)
{
  PurpleBuddy *buddy;
  struct _purple_hbuddy hb;
  PurpleBlistNode *group;
  do {
    if (purplebuddylist != ((PurpleBuddyList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purplebuddylist != NULL");
      return 0;
    };
  }while (0);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((name != ((const char *)((void *)0))) && (( *name) != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"(name != NULL) && (*name != \'\\0\')");
      return 0;
    };
  }while (0);
  hb.account = account;
  hb.name = ((gchar *)(purple_normalize(account,name)));
  for (group = (purplebuddylist -> root); group != 0; group = (group -> next)) {
    if (!((group -> child) != 0)) 
      continue; 
    hb.group = group;
    if ((buddy = (g_hash_table_lookup((purplebuddylist -> buddies),(&hb)))) != 0) {
      return buddy;
    }
  }
  return 0;
}

PurpleBuddy *purple_find_buddy_in_group(PurpleAccount *account,const char *name,PurpleGroup *group)
{
  struct _purple_hbuddy hb;
  do {
    if (purplebuddylist != ((PurpleBuddyList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purplebuddylist != NULL");
      return 0;
    };
  }while (0);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((name != ((const char *)((void *)0))) && (( *name) != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"(name != NULL) && (*name != \'\\0\')");
      return 0;
    };
  }while (0);
  hb.name = ((gchar *)(purple_normalize(account,name)));
  hb.account = account;
  hb.group = ((PurpleBlistNode *)group);
  return (g_hash_table_lookup((purplebuddylist -> buddies),(&hb)));
}

static void find_acct_buddies(gpointer key,gpointer value,gpointer data)
{
  PurpleBuddy *buddy = value;
  GSList **list = data;
   *list = g_slist_prepend( *list,buddy);
}

GSList *purple_find_buddies(PurpleAccount *account,const char *name)
{
  PurpleBuddy *buddy;
  PurpleBlistNode *node;
  GSList *ret = (GSList *)((void *)0);
  do {
    if (purplebuddylist != ((PurpleBuddyList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purplebuddylist != NULL");
      return 0;
    };
  }while (0);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  if ((name != ((const char *)((void *)0))) && (( *name) != 0)) {
    struct _purple_hbuddy hb;
    hb.name = ((gchar *)(purple_normalize(account,name)));
    hb.account = account;
    for (node = (purplebuddylist -> root); node != ((PurpleBlistNode *)((void *)0)); node = (node -> next)) {
      if (!((node -> child) != 0)) 
        continue; 
      hb.group = node;
      if ((buddy = (g_hash_table_lookup((purplebuddylist -> buddies),(&hb)))) != ((PurpleBuddy *)((void *)0))) 
        ret = g_slist_prepend(ret,buddy);
    }
  }
  else {
    GSList *list = (GSList *)((void *)0);
    GHashTable *buddies = (g_hash_table_lookup(buddies_cache,account));
    g_hash_table_foreach(buddies,find_acct_buddies,(&list));
    ret = list;
  }
  return ret;
}

PurpleGroup *purple_find_group(const char *name)
{
  gchar *key;
  PurpleGroup *group;
  do {
    if (purplebuddylist != ((PurpleBuddyList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purplebuddylist != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((name != ((const char *)((void *)0))) && (( *name) != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"(name != NULL) && (*name != \'\\0\')");
      return 0;
    };
  }while (0);
  key = g_utf8_collate_key(name,(-1));
  group = (g_hash_table_lookup(groups_cache,key));
  g_free(key);
  return group;
}

PurpleChat *purple_blist_find_chat(PurpleAccount *account,const char *name)
{
  char *chat_name;
  PurpleChat *chat;
  PurplePlugin *prpl;
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  struct proto_chat_entry *pce;
  PurpleBlistNode *node;
  PurpleBlistNode *group;
  GList *parts;
  char *normname;
  do {
    if (purplebuddylist != ((PurpleBuddyList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purplebuddylist != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((name != ((const char *)((void *)0))) && (( *name) != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"(name != NULL) && (*name != \'\\0\')");
      return 0;
    };
  }while (0);
  if (!(purple_account_is_connected(account) != 0)) 
    return 0;
  prpl = purple_find_prpl(purple_account_get_protocol_id(account));
  prpl_info = ((PurplePluginProtocolInfo *)( *(prpl -> info)).extra_info);
  if ((prpl_info -> find_blist_chat) != ((PurpleChat *(*)(PurpleAccount *, const char *))((void *)0))) 
    return ( *(prpl_info -> find_blist_chat))(account,name);
  normname = g_strdup(purple_normalize(account,name));
  for (group = (purplebuddylist -> root); group != ((PurpleBlistNode *)((void *)0)); group = (group -> next)) {
    for (node = (group -> child); node != ((PurpleBlistNode *)((void *)0)); node = (node -> next)) {
      if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) {
        chat = ((PurpleChat *)node);
        if (account != (chat -> account)) 
          continue; 
        parts = ( *(prpl_info -> chat_info))(purple_account_get_connection((chat -> account)));
        pce = (parts -> data);
        chat_name = (g_hash_table_lookup((chat -> components),(pce -> identifier)));
        g_list_foreach(parts,((GFunc )g_free),0);
        g_list_free(parts);
        if (((((chat -> account) == account) && (chat_name != ((char *)((void *)0)))) && (normname != ((char *)((void *)0)))) && !(strcmp(purple_normalize(account,chat_name),normname) != 0)) {
          g_free(normname);
          return chat;
        }
      }
    }
  }
  g_free(normname);
  return 0;
}

PurpleGroup *purple_chat_get_group(PurpleChat *chat)
{
  do {
    if (chat != ((PurpleChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return 0;
    };
  }while (0);
  return (PurpleGroup *)( *((PurpleBlistNode *)chat)).parent;
}

PurpleAccount *purple_chat_get_account(PurpleChat *chat)
{
  do {
    if (chat != ((PurpleChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return 0;
    };
  }while (0);
  return chat -> account;
}

GHashTable *purple_chat_get_components(PurpleChat *chat)
{
  do {
    if (chat != ((PurpleChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return 0;
    };
  }while (0);
  return chat -> components;
}

PurpleContact *purple_buddy_get_contact(PurpleBuddy *buddy)
{
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return 0;
    };
  }while (0);
  return (PurpleContact *)( *((PurpleBlistNode *)buddy)).parent;
}

PurplePresence *purple_buddy_get_presence(const PurpleBuddy *buddy)
{
  do {
    if (buddy != ((const PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return 0;
    };
  }while (0);
  return buddy -> presence;
}

PurpleMediaCaps purple_buddy_get_media_caps(const PurpleBuddy *buddy)
{
  do {
    if (buddy != ((const PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return 0;
    };
  }while (0);
  return buddy -> media_caps;
}

void purple_buddy_set_media_caps(PurpleBuddy *buddy,PurpleMediaCaps media_caps)
{
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return ;
    };
  }while (0);
  buddy -> media_caps = media_caps;
}

PurpleGroup *purple_buddy_get_group(PurpleBuddy *buddy)
{
  do {
    if (buddy != ((PurpleBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buddy != NULL");
      return 0;
    };
  }while (0);
  if (( *((PurpleBlistNode *)buddy)).parent == ((PurpleBlistNode *)((void *)0))) 
    return 0;
  return (PurpleGroup *)( *( *((PurpleBlistNode *)buddy)).parent).parent;
}

GSList *purple_group_get_accounts(PurpleGroup *group)
{
  GSList *l = (GSList *)((void *)0);
  PurpleBlistNode *gnode;
  PurpleBlistNode *cnode;
  PurpleBlistNode *bnode;
  gnode = ((PurpleBlistNode *)group);
  for (cnode = (gnode -> child); cnode != 0; cnode = (cnode -> next)) {
    if ((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CHAT_NODE) {
      if (!(g_slist_find(l,( *((PurpleChat *)cnode)).account) != 0)) 
        l = g_slist_append(l,( *((PurpleChat *)cnode)).account);
    }
    else if ((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CONTACT_NODE) {
      for (bnode = (cnode -> child); bnode != 0; bnode = (bnode -> next)) {
        if ((purple_blist_node_get_type(bnode)) == PURPLE_BLIST_BUDDY_NODE) {
          if (!(g_slist_find(l,( *((PurpleBuddy *)bnode)).account) != 0)) 
            l = g_slist_append(l,( *((PurpleBuddy *)bnode)).account);
        }
      }
    }
  }
  return l;
}

void purple_blist_add_account(PurpleAccount *account)
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  PurpleBlistNode *gnode;
  PurpleBlistNode *cnode;
  PurpleBlistNode *bnode;
  do {
    if (purplebuddylist != ((PurpleBuddyList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purplebuddylist != NULL");
      return ;
    };
  }while (0);
  if (!(ops != 0) || !((ops -> update) != 0)) 
    return ;
  for (gnode = (purplebuddylist -> root); gnode != 0; gnode = (gnode -> next)) {
    if (!((purple_blist_node_get_type(gnode)) == PURPLE_BLIST_GROUP_NODE)) 
      continue; 
    for (cnode = (gnode -> child); cnode != 0; cnode = (cnode -> next)) {
      if ((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CONTACT_NODE) {
        gboolean recompute = 0;
        for (bnode = (cnode -> child); bnode != 0; bnode = (bnode -> next)) {
          if (((purple_blist_node_get_type(bnode)) == PURPLE_BLIST_BUDDY_NODE) && (( *((PurpleBuddy *)bnode)).account == account)) {
            recompute = (!0);
            ( *((PurpleContact *)cnode)).currentsize++;
            if (( *((PurpleContact *)cnode)).currentsize == 1) 
              ( *((PurpleGroup *)gnode)).currentsize++;
            ( *(ops -> update))(purplebuddylist,bnode);
          }
        }
        if ((recompute != 0) || (purple_blist_node_get_bool(cnode,"show_offline") != 0)) {
          purple_contact_invalidate_priority_buddy(((PurpleContact *)cnode));
          ( *(ops -> update))(purplebuddylist,cnode);
        }
      }
      else if (((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CHAT_NODE) && (( *((PurpleChat *)cnode)).account == account)) {
        ( *((PurpleGroup *)gnode)).online++;
        ( *((PurpleGroup *)gnode)).currentsize++;
        ( *(ops -> update))(purplebuddylist,cnode);
      }
    }
    ( *(ops -> update))(purplebuddylist,gnode);
  }
}

void purple_blist_remove_account(PurpleAccount *account)
{
  PurpleBlistUiOps *ops = purple_blist_get_ui_ops();
  PurpleBlistNode *gnode;
  PurpleBlistNode *cnode;
  PurpleBlistNode *bnode;
  PurpleBuddy *buddy;
  PurpleChat *chat;
  PurpleContact *contact;
  PurpleGroup *group;
  GList *list = (GList *)((void *)0);
  GList *iter = (GList *)((void *)0);
  do {
    if (purplebuddylist != ((PurpleBuddyList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purplebuddylist != NULL");
      return ;
    };
  }while (0);
  for (gnode = (purplebuddylist -> root); gnode != 0; gnode = (gnode -> next)) {
    if (!((purple_blist_node_get_type(gnode)) == PURPLE_BLIST_GROUP_NODE)) 
      continue; 
    group = ((PurpleGroup *)gnode);
    for (cnode = (gnode -> child); cnode != 0; cnode = (cnode -> next)) {
      if ((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CONTACT_NODE) {
        gboolean recompute = 0;
        contact = ((PurpleContact *)cnode);
        for (bnode = (cnode -> child); bnode != 0; bnode = (bnode -> next)) {
          if (!((purple_blist_node_get_type(bnode)) == PURPLE_BLIST_BUDDY_NODE)) 
            continue; 
          buddy = ((PurpleBuddy *)bnode);
          if (account == (buddy -> account)) {
            PurplePresence *presence;
            presence = purple_buddy_get_presence(buddy);
            if (purple_presence_is_online(presence) != 0) {
              contact -> online--;
              if ((contact -> online) == 0) 
                group -> online--;
              purple_blist_node_set_int(&buddy -> node,"last_seen",(time(0)));
            }
            contact -> currentsize--;
            if ((contact -> currentsize) == 0) 
              group -> currentsize--;
            if (!(g_list_find(list,presence) != 0)) 
              list = g_list_prepend(list,presence);
            if ((contact -> priority) == buddy) 
              purple_contact_invalidate_priority_buddy(contact);
            else 
              recompute = (!0);
            if ((ops != 0) && ((ops -> remove) != 0)) {
              ( *(ops -> remove))(purplebuddylist,bnode);
            }
          }
        }
        if (recompute != 0) {
          purple_contact_invalidate_priority_buddy(contact);
          if ((ops != 0) && ((ops -> update) != 0)) 
            ( *(ops -> update))(purplebuddylist,cnode);
        }
      }
      else if ((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CHAT_NODE) {
        chat = ((PurpleChat *)cnode);
        if ((chat -> account) == account) {
          group -> currentsize--;
          group -> online--;
          if ((ops != 0) && ((ops -> remove) != 0)) 
            ( *(ops -> remove))(purplebuddylist,cnode);
        }
      }
    }
  }
  for (iter = list; iter != 0; iter = (iter -> next)) {
    purple_presence_set_status_active((iter -> data),"offline",(!0));
  }
  g_list_free(list);
}

gboolean purple_group_on_account(PurpleGroup *g,PurpleAccount *account)
{
  PurpleBlistNode *cnode;
  for (cnode = ( *((PurpleBlistNode *)g)).child; cnode != 0; cnode = (cnode -> next)) {
    if ((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CONTACT_NODE) {
      if (purple_contact_on_account(((PurpleContact *)cnode),account) != 0) 
        return (!0);
    }
    else if ((purple_blist_node_get_type(cnode)) == PURPLE_BLIST_CHAT_NODE) {
      PurpleChat *chat = (PurpleChat *)cnode;
      if ((!(account != 0) && (purple_account_is_connected((chat -> account)) != 0)) || ((chat -> account) == account)) 
        return (!0);
    }
  }
  return 0;
}

const char *purple_group_get_name(PurpleGroup *group)
{
  do {
    if (group != ((PurpleGroup *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"group != NULL");
      return 0;
    };
  }while (0);
  return (group -> name);
}

void purple_blist_request_add_buddy(PurpleAccount *account,const char *username,const char *group,const char *alias)
{
  PurpleBlistUiOps *ui_ops;
  ui_ops = purple_blist_get_ui_ops();
  if ((ui_ops != ((PurpleBlistUiOps *)((void *)0))) && ((ui_ops -> request_add_buddy) != ((void (*)(PurpleAccount *, const char *, const char *, const char *))((void *)0)))) 
    ( *(ui_ops -> request_add_buddy))(account,username,group,alias);
}

void purple_blist_request_add_chat(PurpleAccount *account,PurpleGroup *group,const char *alias,const char *name)
{
  PurpleBlistUiOps *ui_ops;
  ui_ops = purple_blist_get_ui_ops();
  if ((ui_ops != ((PurpleBlistUiOps *)((void *)0))) && ((ui_ops -> request_add_chat) != ((void (*)(PurpleAccount *, PurpleGroup *, const char *, const char *))((void *)0)))) 
    ( *(ui_ops -> request_add_chat))(account,group,alias,name);
}

void purple_blist_request_add_group()
{
  PurpleBlistUiOps *ui_ops;
  ui_ops = purple_blist_get_ui_ops();
  if ((ui_ops != ((PurpleBlistUiOps *)((void *)0))) && ((ui_ops -> request_add_group) != ((void (*)())((void *)0)))) 
    ( *(ui_ops -> request_add_group))();
}

static void purple_blist_node_destroy(PurpleBlistNode *node)
{
  PurpleBlistUiOps *ui_ops;
  PurpleBlistNode *child;
  PurpleBlistNode *next_child;
  ui_ops = purple_blist_get_ui_ops();
  child = (node -> child);
  while(child != 0){
    next_child = (child -> next);
    purple_blist_node_destroy(child);
    child = next_child;
  }
/* Allow the UI to free data */
  node -> parent = ((PurpleBlistNode *)((void *)0));
  node -> child = ((PurpleBlistNode *)((void *)0));
  node -> next = ((PurpleBlistNode *)((void *)0));
  node -> prev = ((PurpleBlistNode *)((void *)0));
  if ((ui_ops != 0) && ((ui_ops -> remove) != 0)) 
    ( *(ui_ops -> remove))(purplebuddylist,node);
  if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_BUDDY_NODE) 
    purple_buddy_destroy(((PurpleBuddy *)node));
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CHAT_NODE) 
    purple_chat_destroy(((PurpleChat *)node));
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_CONTACT_NODE) 
    purple_contact_destroy(((PurpleContact *)node));
  else if ((purple_blist_node_get_type(node)) == PURPLE_BLIST_GROUP_NODE) 
    purple_group_destroy(((PurpleGroup *)node));
}

static void purple_blist_node_setting_free(gpointer data)
{
  PurpleValue *value;
  value = ((PurpleValue *)data);
  purple_value_destroy(value);
}

static void purple_blist_node_initialize_settings(PurpleBlistNode *node)
{
  if ((node -> settings) != 0) 
    return ;
  node -> settings = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,((GDestroyNotify )purple_blist_node_setting_free));
}

void purple_blist_node_remove_setting(PurpleBlistNode *node,const char *key)
{
  PurpleBlistUiOps *ops;
  do {
    if (node != ((PurpleBlistNode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return ;
    };
  }while (0);
  do {
    if ((node -> settings) != ((GHashTable *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node->settings != NULL");
      return ;
    };
  }while (0);
  do {
    if (key != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"key != NULL");
      return ;
    };
  }while (0);
  g_hash_table_remove((node -> settings),key);
  ops = purple_blist_get_ui_ops();
  if ((ops != 0) && ((ops -> save_node) != 0)) 
    ( *(ops -> save_node))(node);
}

void purple_blist_node_set_flags(PurpleBlistNode *node,PurpleBlistNodeFlags flags)
{
  do {
    if (node != ((PurpleBlistNode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return ;
    };
  }while (0);
  node -> flags = flags;
}

PurpleBlistNodeFlags purple_blist_node_get_flags(PurpleBlistNode *node)
{
  do {
    if (node != ((PurpleBlistNode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return 0;
    };
  }while (0);
  return node -> flags;
}

PurpleBlistNodeType purple_blist_node_get_type(PurpleBlistNode *node)
{
  do {
    if (node != ((PurpleBlistNode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return PURPLE_BLIST_OTHER_NODE;
    };
  }while (0);
  return node -> type;
}

void purple_blist_node_set_bool(PurpleBlistNode *node,const char *key,gboolean data)
{
  PurpleValue *value;
  PurpleBlistUiOps *ops;
  do {
    if (node != ((PurpleBlistNode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return ;
    };
  }while (0);
  do {
    if ((node -> settings) != ((GHashTable *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node->settings != NULL");
      return ;
    };
  }while (0);
  do {
    if (key != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"key != NULL");
      return ;
    };
  }while (0);
  value = purple_value_new(PURPLE_TYPE_BOOLEAN);
  purple_value_set_boolean(value,data);
  g_hash_table_replace((node -> settings),(g_strdup(key)),value);
  ops = purple_blist_get_ui_ops();
  if ((ops != 0) && ((ops -> save_node) != 0)) 
    ( *(ops -> save_node))(node);
}

gboolean purple_blist_node_get_bool(PurpleBlistNode *node,const char *key)
{
  PurpleValue *value;
  do {
    if (node != ((PurpleBlistNode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((node -> settings) != ((GHashTable *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node->settings != NULL");
      return 0;
    };
  }while (0);
  do {
    if (key != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"key != NULL");
      return 0;
    };
  }while (0);
  value = (g_hash_table_lookup((node -> settings),key));
  if (value == ((PurpleValue *)((void *)0))) 
    return 0;
  do {
    if ((purple_value_get_type(value)) == PURPLE_TYPE_BOOLEAN) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_value_get_type(value) == PURPLE_TYPE_BOOLEAN");
      return 0;
    };
  }while (0);
  return purple_value_get_boolean(value);
}

void purple_blist_node_set_int(PurpleBlistNode *node,const char *key,int data)
{
  PurpleValue *value;
  PurpleBlistUiOps *ops;
  do {
    if (node != ((PurpleBlistNode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return ;
    };
  }while (0);
  do {
    if ((node -> settings) != ((GHashTable *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node->settings != NULL");
      return ;
    };
  }while (0);
  do {
    if (key != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"key != NULL");
      return ;
    };
  }while (0);
  value = purple_value_new(PURPLE_TYPE_INT);
  purple_value_set_int(value,data);
  g_hash_table_replace((node -> settings),(g_strdup(key)),value);
  ops = purple_blist_get_ui_ops();
  if ((ops != 0) && ((ops -> save_node) != 0)) 
    ( *(ops -> save_node))(node);
}

int purple_blist_node_get_int(PurpleBlistNode *node,const char *key)
{
  PurpleValue *value;
  do {
    if (node != ((PurpleBlistNode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((node -> settings) != ((GHashTable *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node->settings != NULL");
      return 0;
    };
  }while (0);
  do {
    if (key != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"key != NULL");
      return 0;
    };
  }while (0);
  value = (g_hash_table_lookup((node -> settings),key));
  if (value == ((PurpleValue *)((void *)0))) 
    return 0;
  do {
    if ((purple_value_get_type(value)) == PURPLE_TYPE_INT) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_value_get_type(value) == PURPLE_TYPE_INT");
      return 0;
    };
  }while (0);
  return purple_value_get_int(value);
}

void purple_blist_node_set_string(PurpleBlistNode *node,const char *key,const char *data)
{
  PurpleValue *value;
  PurpleBlistUiOps *ops;
  do {
    if (node != ((PurpleBlistNode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return ;
    };
  }while (0);
  do {
    if ((node -> settings) != ((GHashTable *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node->settings != NULL");
      return ;
    };
  }while (0);
  do {
    if (key != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"key != NULL");
      return ;
    };
  }while (0);
  value = purple_value_new(PURPLE_TYPE_STRING);
  purple_value_set_string(value,data);
  g_hash_table_replace((node -> settings),(g_strdup(key)),value);
  ops = purple_blist_get_ui_ops();
  if ((ops != 0) && ((ops -> save_node) != 0)) 
    ( *(ops -> save_node))(node);
}

const char *purple_blist_node_get_string(PurpleBlistNode *node,const char *key)
{
  PurpleValue *value;
  do {
    if (node != ((PurpleBlistNode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((node -> settings) != ((GHashTable *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"node->settings != NULL");
      return 0;
    };
  }while (0);
  do {
    if (key != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"key != NULL");
      return 0;
    };
  }while (0);
  value = (g_hash_table_lookup((node -> settings),key));
  if (value == ((PurpleValue *)((void *)0))) 
    return 0;
  do {
    if ((purple_value_get_type(value)) == PURPLE_TYPE_STRING) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"purple_value_get_type(value) == PURPLE_TYPE_STRING");
      return 0;
    };
  }while (0);
  return purple_value_get_string(value);
}

GList *purple_blist_node_get_extended_menu(PurpleBlistNode *n)
{
  GList *menu = (GList *)((void *)0);
  do {
    if (n != ((PurpleBlistNode *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"n != NULL");
      return 0;
    };
  }while (0);
  purple_signal_emit(purple_blist_get_handle(),"blist-node-extended-menu",n,&menu);
  return menu;
}

int purple_blist_get_group_size(PurpleGroup *group,gboolean offline)
{
  if (!(group != 0)) 
    return 0;
  return (offline != 0)?(group -> totalsize) : (group -> currentsize);
}

int purple_blist_get_group_online_count(PurpleGroup *group)
{
  if (!(group != 0)) 
    return 0;
  return group -> online;
}

void purple_blist_set_ui_ops(PurpleBlistUiOps *ops)
{
  gboolean overrode = 0;
  blist_ui_ops = ops;
  if (!(ops != 0)) 
    return ;
  if (!((ops -> save_node) != 0)) {
    ops -> save_node = purple_blist_save_node;
    overrode = (!0);
  }
  if (!((ops -> remove_node) != 0)) {
    ops -> remove_node = purple_blist_save_node;
    overrode = (!0);
  }
  if (!((ops -> save_account) != 0)) {
    ops -> save_account = purple_blist_save_account;
    overrode = (!0);
  }
  if ((overrode != 0) && ((((ops -> save_node) != purple_blist_save_node) || ((ops -> remove_node) != purple_blist_save_node)) || ((ops -> save_account) != purple_blist_save_account))) {
    purple_debug_warning("blist","Only some of the blist saving UI ops were overridden. This probably is not what you want!\n");
  }
}

PurpleBlistUiOps *purple_blist_get_ui_ops()
{
  return blist_ui_ops;
}

void *purple_blist_get_handle()
{
  static int handle;
  return (&handle);
}

void purple_blist_init()
{
  void *handle = purple_blist_get_handle();
  purple_signal_register(handle,"buddy-status-changed",purple_marshal_VOID__POINTER_POINTER_POINTER,0,3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_BLIST_BUDDY),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_STATUS),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_STATUS));
  purple_signal_register(handle,"buddy-privacy-changed",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_BLIST_BUDDY));
  purple_signal_register(handle,"buddy-idle-changed",purple_marshal_VOID__POINTER_INT_INT,0,3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_BLIST_BUDDY),purple_value_new(PURPLE_TYPE_INT),purple_value_new(PURPLE_TYPE_INT));
  purple_signal_register(handle,"buddy-signed-on",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_BLIST_BUDDY));
  purple_signal_register(handle,"buddy-signed-off",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_BLIST_BUDDY));
  purple_signal_register(handle,"buddy-got-login-time",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_BLIST_BUDDY));
  purple_signal_register(handle,"blist-node-added",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_BLIST_NODE));
  purple_signal_register(handle,"blist-node-removed",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_BLIST_NODE));
  purple_signal_register(handle,"buddy-added",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_BLIST_BUDDY));
  purple_signal_register(handle,"buddy-removed",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_BLIST_BUDDY));
  purple_signal_register(handle,"buddy-icon-changed",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_BLIST_BUDDY));
  purple_signal_register(handle,"update-idle",purple_marshal_VOID,0,0);
  purple_signal_register(handle,"blist-node-extended-menu",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_BLIST_NODE),purple_value_new(PURPLE_TYPE_BOXED,"GList **"));
  purple_signal_register(handle,"blist-node-aliased",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_BLIST_NODE),purple_value_new(PURPLE_TYPE_STRING));
  purple_signal_register(handle,"buddy-caps-changed",purple_marshal_VOID__POINTER_INT_INT,0,3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_BLIST_BUDDY),purple_value_new(PURPLE_TYPE_INT),purple_value_new(PURPLE_TYPE_INT));
  purple_signal_connect(purple_accounts_get_handle(),"account-created",handle,((PurpleCallback )purple_blist_buddies_cache_add_account),0);
  purple_signal_connect(purple_accounts_get_handle(),"account-destroying",handle,((PurpleCallback )purple_blist_buddies_cache_remove_account),0);
}

void purple_blist_uninit()
{
  PurpleBlistNode *node;
  PurpleBlistNode *next_node;
/* This happens if we quit before purple_set_blist is called. */
  if (purplebuddylist == ((PurpleBuddyList *)((void *)0))) 
    return ;
  if (save_timer != 0) {
    purple_timeout_remove(save_timer);
    save_timer = 0;
    purple_blist_sync();
  }
  purple_blist_destroy();
  node = purple_blist_get_root();
  while(node != 0){
    next_node = (node -> next);
    purple_blist_node_destroy(node);
    node = next_node;
  }
  purplebuddylist -> root = ((PurpleBlistNode *)((void *)0));
  g_hash_table_destroy((purplebuddylist -> buddies));
  g_hash_table_destroy(buddies_cache);
  g_hash_table_destroy(groups_cache);
  buddies_cache = ((GHashTable *)((void *)0));
  groups_cache = ((GHashTable *)((void *)0));
  purple_dbus_unregister_pointer(purplebuddylist);
  g_free(purplebuddylist);
  purplebuddylist = ((PurpleBuddyList *)((void *)0));
  purple_signals_disconnect_by_handle(purple_blist_get_handle());
  purple_signals_unregister_by_instance(purple_blist_get_handle());
}
