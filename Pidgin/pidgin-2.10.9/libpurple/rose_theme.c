/*
 * Themes for libpurple
 *
 * Pidgin is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "theme.h"
#include "util.h"
#define PURPLE_THEME_GET_PRIVATE(PurpleTheme) \
	((PurpleThemePrivate *) ((PurpleTheme)->priv))
void purple_theme_set_type_string(PurpleTheme *theme,const gchar *type);
/******************************************************************************
 * Structs
 *****************************************************************************/
typedef struct __unnamed_class___F0_L36_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__name__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__description__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__author__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__type__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__dir__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__gcharc__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__img {
gchar *name;
gchar *description;
gchar *author;
gchar *type;
gchar *dir;
gchar *img;}PurpleThemePrivate;
/******************************************************************************
 * Globals
 *****************************************************************************/
static GObjectClass *parent_class = (GObjectClass *)((void *)0);
/******************************************************************************
 * Enums
 *****************************************************************************/
enum __unnamed_enum___F0_L55_C1_PROP_ZERO__COMMA__PROP_NAME__COMMA__PROP_DESCRIPTION__COMMA__PROP_AUTHOR__COMMA__PROP_TYPE__COMMA__PROP_DIR__COMMA__PROP_IMAGE {PROP_ZERO,PROP_NAME,PROP_DESCRIPTION,PROP_AUTHOR,PROP_TYPE,PROP_DIR,PROP_IMAGE};
/******************************************************************************
 * GObject Stuff
 *****************************************************************************/

static void purple_theme_get_property(GObject *obj,guint param_id,GValue *value,GParamSpec *psec)
{
  PurpleTheme *theme = (PurpleTheme *)(g_type_check_instance_cast(((GTypeInstance *)obj),purple_theme_get_type()));
  switch(param_id){
    case PROP_NAME:
{
      g_value_set_string(value,purple_theme_get_name(theme));
      break; 
    }
    case PROP_DESCRIPTION:
{
      g_value_set_string(value,purple_theme_get_description(theme));
      break; 
    }
    case PROP_AUTHOR:
{
      g_value_set_string(value,purple_theme_get_author(theme));
      break; 
    }
    case PROP_TYPE:
{
      g_value_set_string(value,purple_theme_get_type_string(theme));
      break; 
    }
    case PROP_DIR:
{
      g_value_set_string(value,purple_theme_get_dir(theme));
      break; 
    }
    case PROP_IMAGE:
{
      g_value_set_string(value,purple_theme_get_image(theme));
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)obj;
        GParamSpec *_glib__pspec = (GParamSpec *)psec;
        guint _glib__property_id = param_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","theme.c:95","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void purple_theme_set_property(GObject *obj,guint param_id,const GValue *value,GParamSpec *psec)
{
  PurpleTheme *theme = (PurpleTheme *)(g_type_check_instance_cast(((GTypeInstance *)obj),purple_theme_get_type()));
  switch(param_id){
    case PROP_NAME:
{
      purple_theme_set_name(theme,g_value_get_string(value));
      break; 
    }
    case PROP_DESCRIPTION:
{
      purple_theme_set_description(theme,g_value_get_string(value));
      break; 
    }
    case PROP_AUTHOR:
{
      purple_theme_set_author(theme,g_value_get_string(value));
      break; 
    }
    case PROP_TYPE:
{
      purple_theme_set_type_string(theme,g_value_get_string(value));
      break; 
    }
    case PROP_DIR:
{
      purple_theme_set_dir(theme,g_value_get_string(value));
      break; 
    }
    case PROP_IMAGE:
{
      purple_theme_set_image(theme,g_value_get_string(value));
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)obj;
        GParamSpec *_glib__pspec = (GParamSpec *)psec;
        guint _glib__property_id = param_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","theme.c:126","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void purple_theme_init(GTypeInstance *instance,gpointer klass)
{
  PurpleTheme *theme = (PurpleTheme *)(g_type_check_instance_cast(((GTypeInstance *)instance),purple_theme_get_type()));
  theme -> priv = ((PurpleThemePrivate *)(g_malloc0_n(1,(sizeof(PurpleThemePrivate )))));
}

static void purple_theme_finalize(GObject *obj)
{
  PurpleTheme *theme = (PurpleTheme *)(g_type_check_instance_cast(((GTypeInstance *)obj),purple_theme_get_type()));
  PurpleThemePrivate *priv = (PurpleThemePrivate *)(theme -> priv);
  g_free((priv -> name));
  g_free((priv -> description));
  g_free((priv -> author));
  g_free((priv -> type));
  g_free((priv -> dir));
  g_free((priv -> img));
  ( *( *((GObjectClass *)(g_type_check_class_cast(((GTypeClass *)parent_class),((GType )(20 << 2)))))).finalize)(obj);
}

static void purple_theme_class_init(PurpleThemeClass *klass)
{
  GObjectClass *obj_class = (GObjectClass *)(g_type_check_class_cast(((GTypeClass *)klass),((GType )(20 << 2))));
  GParamSpec *pspec;
  parent_class = (g_type_class_peek_parent(klass));
  obj_class -> get_property = purple_theme_get_property;
  obj_class -> set_property = purple_theme_set_property;
  obj_class -> finalize = purple_theme_finalize;
/* NAME */
  pspec = g_param_spec_string("name","Name","The name of the theme",0,(G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT));
  g_object_class_install_property(obj_class,PROP_NAME,pspec);
/* DESCRIPTION */
  pspec = g_param_spec_string("description","Description","The description of the theme",0,(G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT));
  g_object_class_install_property(obj_class,PROP_DESCRIPTION,pspec);
/* AUTHOR */
  pspec = g_param_spec_string("author","Author","The author of the theme",0,(G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT));
  g_object_class_install_property(obj_class,PROP_AUTHOR,pspec);
/* TYPE STRING (read only) */
  pspec = g_param_spec_string("type","Type","The string representing the type of the theme",0,(G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));
  g_object_class_install_property(obj_class,PROP_TYPE,pspec);
/* DIRECTORY */
  pspec = g_param_spec_string("directory","Directory","The directory that contains the theme and all its files",0,(G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT));
  g_object_class_install_property(obj_class,PROP_DIR,pspec);
/* PREVIEW IMAGE */
  pspec = g_param_spec_string("image","Image","A preview image of the theme",0,(G_PARAM_READABLE | G_PARAM_WRITABLE));
  g_object_class_install_property(obj_class,PROP_IMAGE,pspec);
}

GType purple_theme_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(PurpleThemeClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )purple_theme_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(PurpleTheme ))), (0), (purple_theme_init), ((const GTypeValueTable *)((void *)0))
/* base_init */
/* base_finalize */
/* class_init */
/* class_finalize */
/* class_data */
/* n_preallocs */
/* instance_init */
/* value table */
};
    type = g_type_register_static(((GType )(20 << 2)),"PurpleTheme",&info,G_TYPE_FLAG_ABSTRACT);
  }
  return type;
}
/******************************************************************************
 * Helper Functions
 *****************************************************************************/

static gchar *theme_clean_text(const gchar *text)
{
  gchar *clean_text = (gchar *)((void *)0);
  if (text != ((const gchar *)((void *)0))) {
    clean_text = g_markup_escape_text(text,(-1));
    g_strdelimit(clean_text,"\n",32);
    purple_str_strip_char(clean_text,13);
  }
  return clean_text;
}
/*****************************************************************************
 * Public API function
 *****************************************************************************/

const gchar *purple_theme_get_name(PurpleTheme *theme)
{
  PurpleThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = purple_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PurpleThemePrivate *)(theme -> priv));
  return (priv -> name);
}

void purple_theme_set_name(PurpleTheme *theme,const gchar *name)
{
  PurpleThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = purple_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PurpleThemePrivate *)(theme -> priv));
  g_free((priv -> name));
  priv -> name = theme_clean_text(name);
}

const gchar *purple_theme_get_description(PurpleTheme *theme)
{
  PurpleThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = purple_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PurpleThemePrivate *)(theme -> priv));
  return (priv -> description);
}

void purple_theme_set_description(PurpleTheme *theme,const gchar *description)
{
  PurpleThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = purple_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PurpleThemePrivate *)(theme -> priv));
  g_free((priv -> description));
  priv -> description = theme_clean_text(description);
}

const gchar *purple_theme_get_author(PurpleTheme *theme)
{
  PurpleThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = purple_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PurpleThemePrivate *)(theme -> priv));
  return (priv -> author);
}

void purple_theme_set_author(PurpleTheme *theme,const gchar *author)
{
  PurpleThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = purple_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PurpleThemePrivate *)(theme -> priv));
  g_free((priv -> author));
  priv -> author = theme_clean_text(author);
}

const gchar *purple_theme_get_type_string(PurpleTheme *theme)
{
  PurpleThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = purple_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PurpleThemePrivate *)(theme -> priv));
  return (priv -> type);
}
/* < private > */

void purple_theme_set_type_string(PurpleTheme *theme,const gchar *type)
{
  PurpleThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = purple_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PurpleThemePrivate *)(theme -> priv));
  g_free((priv -> type));
  priv -> type = g_strdup(type);
}

const gchar *purple_theme_get_dir(PurpleTheme *theme)
{
  PurpleThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = purple_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PurpleThemePrivate *)(theme -> priv));
  return (priv -> dir);
}

void purple_theme_set_dir(PurpleTheme *theme,const gchar *dir)
{
  PurpleThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = purple_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PurpleThemePrivate *)(theme -> priv));
  g_free((priv -> dir));
  priv -> dir = g_strdup(dir);
}

const gchar *purple_theme_get_image(PurpleTheme *theme)
{
  PurpleThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = purple_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_THEME(theme)");
      return 0;
    };
  }while (0);
  priv = ((PurpleThemePrivate *)(theme -> priv));
  return (priv -> img);
}

gchar *purple_theme_get_image_full(PurpleTheme *theme)
{
  const gchar *filename = purple_theme_get_image(theme);
  if (filename != 0) 
    return g_build_filename(purple_theme_get_dir(((PurpleTheme *)(g_type_check_instance_cast(((GTypeInstance *)theme),purple_theme_get_type())))),filename,((void *)((void *)0)));
  else 
    return 0;
}

void purple_theme_set_image(PurpleTheme *theme,const gchar *img)
{
  PurpleThemePrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)theme;
      GType __t = purple_theme_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_THEME(theme)");
      return ;
    };
  }while (0);
  priv = ((PurpleThemePrivate *)(theme -> priv));
  g_free((priv -> img));
  priv -> img = g_strdup(img);
}
