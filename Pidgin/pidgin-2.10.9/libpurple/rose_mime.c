/*
 * Purple
 *
 * Purple is the legal property of its developers, whose names are too
 * numerous to list here. Please refer to the COPYRIGHT file distributed
 * with this source distribution
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301,
 * USA.
 */
#include "internal.h"
/* this should become "util.h" if we ever get this into purple proper */
#include "debug.h"
#include "mime.h"
#include "util.h"
/**
 * @struct mime_fields
 *
 * Utility structure used in both MIME document and parts, which maps
 * field names to their values, and keeps an easily accessible list of
 * keys.
 */

struct mime_fields 
{
  GHashTable *map;
  GList *keys;
}
;

struct _PurpleMimeDocument 
{
  struct mime_fields fields;
  GList *parts;
}
;

struct _PurpleMimePart 
{
  struct mime_fields fields;
  struct _PurpleMimeDocument *doc;
  GString *data;
}
;

static void fields_set(struct mime_fields *mf,const char *key,const char *val)
{
  char *k;
  char *v;
  do {
    if (mf != ((struct mime_fields *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"mf != NULL");
      return ;
    };
  }while (0);
  do {
    if ((mf -> map) != ((GHashTable *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"mf->map != NULL");
      return ;
    };
  }while (0);
  k = g_ascii_strdown(key,(-1));
  v = g_strdup(val);
/* append to the keys list only if it's not already there */
  if (!(g_hash_table_lookup((mf -> map),k) != 0)) {
    mf -> keys = g_list_append((mf -> keys),k);
  }
/* important to use insert. If the key is already in the table, then
		 it's already in the keys list. Insert will free the new instance
		 of the key rather than the old instance. */
  g_hash_table_insert((mf -> map),k,v);
}

static const char *fields_get(struct mime_fields *mf,const char *key)
{
  char *kdown;
  const char *ret;
  do {
    if (mf != ((struct mime_fields *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"mf != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((mf -> map) != ((GHashTable *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"mf->map != NULL");
      return 0;
    };
  }while (0);
  kdown = g_ascii_strdown(key,(-1));
  ret = (g_hash_table_lookup((mf -> map),kdown));
  g_free(kdown);
  return ret;
}

static void fields_init(struct mime_fields *mf)
{
  do {
    if (mf != ((struct mime_fields *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"mf != NULL");
      return ;
    };
  }while (0);
  mf -> map = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
}

static void fields_loadline(struct mime_fields *mf,const char *line,gsize len)
{
/* split the line into key: value */
  char *key;
  char *newkey;
  char *val;
  char **tokens;
/* feh, need it to be NUL terminated */
  key = g_strndup(line,len);
/* split */
  val = strchr(key,':');
  if (!(val != 0)) {
    g_free(key);
    return ;
  }
   *(val++) = 0;
/* normalize whitespace (sorta) and trim on key and value */
  tokens = g_strsplit(key,"\t\r\n",0);
  newkey = g_strjoinv("",tokens);
  g_strchomp(g_strchug(newkey));
  g_strfreev(tokens);
  tokens = g_strsplit(val,"\t\r\n",0);
  val = g_strjoinv("",tokens);
  g_strchomp(g_strchug(val));
  g_strfreev(tokens);
  fields_set(mf,newkey,val);
  g_free(newkey);
  g_free(key);
  g_free(val);
}

static void fields_load(struct mime_fields *mf,char **buf,gsize *len)
{
  char *tail;
  while((tail = g_strstr_len(( *buf),( *len),"\r\n")) != 0){
    char *line;
    gsize ln;
/* determine the current line */
    line =  *buf;
    ln = (tail - line);
/* advance our search space past the CRLF */
     *buf = (tail + 2);
     *len -= (ln + 2);
/* empty line, end of headers */
    if (!(ln != 0UL)) 
      return ;
/* look out for line continuations */
    if (line[ln - 1] == ';') {
      tail = g_strstr_len(( *buf),( *len),"\r\n");
      if (tail != 0) {
        gsize cln;
        cln = (tail -  *buf);
        ln = (tail - line);
/* advance our search space past the CRLF (again) */
         *buf = (tail + 2);
         *len -= (cln + 2);
      }
    }
/* process our super-cool line */
    fields_loadline(mf,line,ln);
  }
}

static void field_write(const char *key,const char *val,GString *str)
{
  g_string_append_printf(str,"%s: %s\r\n",key,val);
}

static void fields_write(struct mime_fields *mf,GString *str)
{
  do {
    if (mf != ((struct mime_fields *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"mf != NULL");
      return ;
    };
  }while (0);
  g_hash_table_foreach((mf -> map),((GHFunc )field_write),str);
  g_string_append(str,"\r\n");
}

static void fields_destroy(struct mime_fields *mf)
{
  do {
    if (mf != ((struct mime_fields *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"mf != NULL");
      return ;
    };
  }while (0);
  g_hash_table_destroy((mf -> map));
  g_list_free((mf -> keys));
  mf -> map = ((GHashTable *)((void *)0));
  mf -> keys = ((GList *)((void *)0));
}

static PurpleMimePart *part_new(PurpleMimeDocument *doc)
{
  PurpleMimePart *part;
  part = ((PurpleMimePart *)(g_malloc0_n(1,(sizeof(PurpleMimePart )))));
  fields_init(&part -> fields);
  part -> doc = doc;
  part -> data = g_string_new(0);
  doc -> parts = g_list_prepend((doc -> parts),part);
  return part;
}

static void part_load(PurpleMimePart *part,const char *buf,gsize len)
{
  char *b = (char *)buf;
  gsize n = len;
  fields_load(&part -> fields,&b,&n);
/* the remainder will have a blank line, if there's anything at all,
		 so check if there's anything then trim off the trailing four
		 bytes, \r\n\r\n */
  if (n > 4) 
    n -= 4;
  g_string_append_len((part -> data),b,n);
}

static void part_write(PurpleMimePart *part,GString *str)
{
  fields_write(&part -> fields,str);
  g_string_append_printf(str,"%s\r\n\r\n",( *(part -> data)).str);
}

static void part_free(PurpleMimePart *part)
{
  fields_destroy(&part -> fields);
  g_string_free((part -> data),(!0));
  part -> data = ((GString *)((void *)0));
  g_free(part);
}

PurpleMimePart *purple_mime_part_new(PurpleMimeDocument *doc)
{
  do {
    if (doc != ((PurpleMimeDocument *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"doc != NULL");
      return 0;
    };
  }while (0);
  return part_new(doc);
}

GList *purple_mime_part_get_fields(PurpleMimePart *part)
{
  do {
    if (part != ((PurpleMimePart *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"part != NULL");
      return 0;
    };
  }while (0);
  return part -> fields.keys;
}

const char *purple_mime_part_get_field(PurpleMimePart *part,const char *field)
{
  do {
    if (part != ((PurpleMimePart *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"part != NULL");
      return 0;
    };
  }while (0);
  return fields_get(&part -> fields,field);
}

char *purple_mime_part_get_field_decoded(PurpleMimePart *part,const char *field)
{
  const char *f;
  do {
    if (part != ((PurpleMimePart *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"part != NULL");
      return 0;
    };
  }while (0);
  f = fields_get(&part -> fields,field);
  return purple_mime_decode_field(f);
}

void purple_mime_part_set_field(PurpleMimePart *part,const char *field,const char *value)
{
  do {
    if (part != ((PurpleMimePart *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"part != NULL");
      return ;
    };
  }while (0);
  fields_set(&part -> fields,field,value);
}

const char *purple_mime_part_get_data(PurpleMimePart *part)
{
  do {
    if (part != ((PurpleMimePart *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"part != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((part -> data) != ((GString *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"part->data != NULL");
      return 0;
    };
  }while (0);
  return ( *(part -> data)).str;
}

void purple_mime_part_get_data_decoded(PurpleMimePart *part,guchar **data,gsize *len)
{
  const char *enc;
  do {
    if (part != ((PurpleMimePart *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"part != NULL");
      return ;
    };
  }while (0);
  do {
    if (data != ((guchar **)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"data != NULL");
      return ;
    };
  }while (0);
  do {
    if (len != ((gsize *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"len != NULL");
      return ;
    };
  }while (0);
  do {
    if ((part -> data) != ((GString *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"part->data != NULL");
      return ;
    };
  }while (0);
  enc = purple_mime_part_get_field(part,"content-transfer-encoding");
  if (!(enc != 0)) {
     *data = ((guchar *)(g_strdup(( *(part -> data)).str)));
     *len = ( *(part -> data)).len;
  }
  else if (!(g_ascii_strcasecmp(enc,"7bit") != 0)) {
     *data = ((guchar *)(g_strdup(( *(part -> data)).str)));
     *len = ( *(part -> data)).len;
  }
  else if (!(g_ascii_strcasecmp(enc,"8bit") != 0)) {
     *data = ((guchar *)(g_strdup(( *(part -> data)).str)));
     *len = ( *(part -> data)).len;
  }
  else if (!(g_ascii_strcasecmp(enc,"base16") != 0)) {
     *data = purple_base16_decode(( *(part -> data)).str,len);
  }
  else if (!(g_ascii_strcasecmp(enc,"base64") != 0)) {
     *data = purple_base64_decode(( *(part -> data)).str,len);
  }
  else if (!(g_ascii_strcasecmp(enc,"quoted-printable") != 0)) {
     *data = purple_quotedp_decode(( *(part -> data)).str,len);
  }
  else {
    purple_debug_warning("mime","purple_mime_part_get_data_decoded: unknown encoding \'%s\'\n",enc);
     *data = ((guchar *)((void *)0));
     *len = 0;
  }
}

gsize purple_mime_part_get_length(PurpleMimePart *part)
{
  do {
    if (part != ((PurpleMimePart *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"part != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((part -> data) != ((GString *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"part->data != NULL");
      return 0;
    };
  }while (0);
  return ( *(part -> data)).len;
}

void purple_mime_part_set_data(PurpleMimePart *part,const char *data)
{
  do {
    if (part != ((PurpleMimePart *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"part != NULL");
      return ;
    };
  }while (0);
  g_string_free((part -> data),(!0));
  part -> data = g_string_new(data);
}

PurpleMimeDocument *purple_mime_document_new()
{
  PurpleMimeDocument *doc;
  doc = ((PurpleMimeDocument *)(g_malloc0_n(1,(sizeof(PurpleMimeDocument )))));
  fields_init(&doc -> fields);
  return doc;
}

static void doc_parts_load(PurpleMimeDocument *doc,const char *boundary,const char *buf,gsize len)
{
  char *b = (char *)buf;
  gsize n = len;
  char *bnd;
  gsize bl;
  bnd = g_strdup_printf("--%s",boundary);
  bl = strlen(bnd);
  for (b = g_strstr_len(b,n,bnd); b != 0; ) {
    char *tail;
/* skip the boundary */
    b += bl;
    n -= bl;
/* skip the trailing \r\n or -- as well */
    if (n >= 2) {
      b += 2;
      n -= 2;
    }
/* find the next boundary */
    tail = g_strstr_len(b,n,bnd);
    if (tail != 0) {
      gsize sl;
      sl = (tail - b);
      if (sl != 0UL) {
        PurpleMimePart *part = part_new(doc);
        part_load(part,b,sl);
      }
    }
    b = tail;
  }
  g_free(bnd);
}
#define BOUNDARY "boundary="

static char *parse_boundary(const char *ct)
{
  char *boundary_begin = g_strstr_len(ct,(-1),"boundary=");
  char *boundary_end;
  if (!(boundary_begin != 0)) 
    return 0;
  boundary_begin += sizeof(( *((char (*)[10UL])"boundary="))) - 1;
  if (( *boundary_begin) == '"') {
    boundary_end = strchr((++boundary_begin),'"');
    if (!(boundary_end != 0)) 
      return 0;
  }
  else {
    boundary_end = strchr(boundary_begin,32);
    if (!(boundary_end != 0)) {
      boundary_end = strchr(boundary_begin,';');
      if (!(boundary_end != 0)) 
        boundary_end = (boundary_begin + strlen(boundary_begin));
    }
  }
  return g_strndup(boundary_begin,(boundary_end - boundary_begin));
}
#undef BOUNDARY

PurpleMimeDocument *purple_mime_document_parsen(const char *buf,gsize len)
{
  PurpleMimeDocument *doc;
  char *b = (char *)buf;
  gsize n = len;
  do {
    if (buf != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buf != NULL");
      return 0;
    };
  }while (0);
  doc = purple_mime_document_new();
  if (!(len != 0UL)) 
    return doc;
  fields_load(&doc -> fields,&b,&n);
{
    const char *ct = fields_get(&doc -> fields,"content-type");
    if ((ct != 0) && (purple_str_has_prefix(ct,"multipart") != 0)) {
      char *bd = parse_boundary(ct);
      if (bd != 0) {
        doc_parts_load(doc,bd,b,n);
        g_free(bd);
      }
    }
  }
  return doc;
}

PurpleMimeDocument *purple_mime_document_parse(const char *buf)
{
  do {
    if (buf != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buf != NULL");
      return 0;
    };
  }while (0);
  return purple_mime_document_parsen(buf,strlen(buf));
}

void purple_mime_document_write(PurpleMimeDocument *doc,GString *str)
{
  const char *bd = (const char *)((void *)0);
  do {
    if (doc != ((PurpleMimeDocument *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"doc != NULL");
      return ;
    };
  }while (0);
  do {
    if (str != ((GString *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"str != NULL");
      return ;
    };
  }while (0);
{
    const char *ct = fields_get(&doc -> fields,"content-type");
    if ((ct != 0) && (purple_str_has_prefix(ct,"multipart") != 0)) {
      char *b = strrchr(ct,'=');
      if (b++ != 0) 
        bd = b;
    }
  }
  fields_write(&doc -> fields,str);
  if (bd != 0) {
    GList *l;
    for (l = (doc -> parts); l != 0; l = (l -> next)) {
      g_string_append_printf(str,"--%s\r\n",bd);
      part_write((l -> data),str);
      if (!((l -> next) != 0)) {
        g_string_append_printf(str,"--%s--\r\n",bd);
      }
    }
  }
}

GList *purple_mime_document_get_fields(PurpleMimeDocument *doc)
{
  do {
    if (doc != ((PurpleMimeDocument *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"doc != NULL");
      return 0;
    };
  }while (0);
  return doc -> fields.keys;
}

const char *purple_mime_document_get_field(PurpleMimeDocument *doc,const char *field)
{
  do {
    if (doc != ((PurpleMimeDocument *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"doc != NULL");
      return 0;
    };
  }while (0);
  return fields_get(&doc -> fields,field);
}

void purple_mime_document_set_field(PurpleMimeDocument *doc,const char *field,const char *value)
{
  do {
    if (doc != ((PurpleMimeDocument *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"doc != NULL");
      return ;
    };
  }while (0);
  fields_set(&doc -> fields,field,value);
}

GList *purple_mime_document_get_parts(PurpleMimeDocument *doc)
{
  do {
    if (doc != ((PurpleMimeDocument *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"doc != NULL");
      return 0;
    };
  }while (0);
  return doc -> parts;
}

void purple_mime_document_free(PurpleMimeDocument *doc)
{
  if (!(doc != 0)) 
    return ;
  fields_destroy(&doc -> fields);
  while((doc -> parts) != 0){
    part_free(( *(doc -> parts)).data);
    doc -> parts = g_list_delete_link((doc -> parts),(doc -> parts));
  }
  g_free(doc);
}
