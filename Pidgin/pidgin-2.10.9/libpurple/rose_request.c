/**
 * @file request.c Request API
 * @ingroup core
 */
/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#define _PURPLE_REQUEST_C_
#include "internal.h"
#include "notify.h"
#include "request.h"
#include "debug.h"
static PurpleRequestUiOps *request_ui_ops = (PurpleRequestUiOps *)((void *)0);
static GList *handles = (GList *)((void *)0);
typedef struct __unnamed_class___F0_L37_C9_unknown_scope_and_name_variable_declaration__variable_type_L397R_variable_name_unknown_scope_and_name__scope__type__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__v__Pe___variable_name_unknown_scope_and_name__scope__handle__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__v__Pe___variable_name_unknown_scope_and_name__scope__ui_handle {
PurpleRequestType type;
void *handle;
void *ui_handle;}PurpleRequestInfo;

PurpleRequestFields *purple_request_fields_new()
{
  PurpleRequestFields *fields;
  fields = ((PurpleRequestFields *)(g_malloc0_n(1,(sizeof(PurpleRequestFields )))));
  fields -> fields = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,0);
  return fields;
}

void purple_request_fields_destroy(PurpleRequestFields *fields)
{
  do {
    if (fields != ((PurpleRequestFields *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"fields != NULL");
      return ;
    };
  }while (0);
  g_list_foreach((fields -> groups),((GFunc )purple_request_field_group_destroy),0);
  g_list_free((fields -> groups));
  g_list_free((fields -> required_fields));
  g_hash_table_destroy((fields -> fields));
  g_free(fields);
}

void purple_request_fields_add_group(PurpleRequestFields *fields,PurpleRequestFieldGroup *group)
{
  GList *l;
  PurpleRequestField *field;
  do {
    if (fields != ((PurpleRequestFields *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"fields != NULL");
      return ;
    };
  }while (0);
  do {
    if (group != ((PurpleRequestFieldGroup *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"group != NULL");
      return ;
    };
  }while (0);
  fields -> groups = g_list_append((fields -> groups),group);
  group -> fields_list = fields;
  for (l = purple_request_field_group_get_fields(group); l != ((GList *)((void *)0)); l = (l -> next)) {
    field = (l -> data);
    g_hash_table_insert((fields -> fields),(g_strdup(purple_request_field_get_id(field))),field);
    if (purple_request_field_is_required(field) != 0) {
      fields -> required_fields = g_list_append((fields -> required_fields),field);
    }
  }
}

GList *purple_request_fields_get_groups(const PurpleRequestFields *fields)
{
  do {
    if (fields != ((const PurpleRequestFields *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"fields != NULL");
      return 0;
    };
  }while (0);
  return fields -> groups;
}

gboolean purple_request_fields_exists(const PurpleRequestFields *fields,const char *id)
{
  do {
    if (fields != ((const PurpleRequestFields *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"fields != NULL");
      return 0;
    };
  }while (0);
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  return g_hash_table_lookup((fields -> fields),id) != ((void *)((void *)0));
}

GList *purple_request_fields_get_required(const PurpleRequestFields *fields)
{
  do {
    if (fields != ((const PurpleRequestFields *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"fields != NULL");
      return 0;
    };
  }while (0);
  return fields -> required_fields;
}

gboolean purple_request_fields_is_field_required(const PurpleRequestFields *fields,const char *id)
{
  PurpleRequestField *field;
  do {
    if (fields != ((const PurpleRequestFields *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"fields != NULL");
      return 0;
    };
  }while (0);
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  if ((field = purple_request_fields_get_field(fields,id)) == ((PurpleRequestField *)((void *)0))) 
    return 0;
  return purple_request_field_is_required(field);
}

gpointer purple_request_field_get_ui_data(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  return field -> ui_data;
}

void purple_request_field_set_ui_data(PurpleRequestField *field,gpointer ui_data)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  field -> ui_data = ui_data;
}

gboolean purple_request_fields_all_required_filled(const PurpleRequestFields *fields)
{
  GList *l;
  do {
    if (fields != ((const PurpleRequestFields *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"fields != NULL");
      return 0;
    };
  }while (0);
  for (l = (fields -> required_fields); l != ((GList *)((void *)0)); l = (l -> next)) {
    PurpleRequestField *field = (PurpleRequestField *)(l -> data);
    switch(purple_request_field_get_type(field)){
      case PURPLE_REQUEST_FIELD_STRING:
{
        if ((purple_request_field_string_get_value(field) == ((const char *)((void *)0))) || (( *purple_request_field_string_get_value(field)) == 0)) 
          return 0;
        break; 
      }
      default:
{
        break; 
      }
    }
  }
  return (!0);
}

PurpleRequestField *purple_request_fields_get_field(const PurpleRequestFields *fields,const char *id)
{
  PurpleRequestField *field;
  do {
    if (fields != ((const PurpleRequestFields *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"fields != NULL");
      return 0;
    };
  }while (0);
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  field = (g_hash_table_lookup((fields -> fields),id));
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  return field;
}

const char *purple_request_fields_get_string(const PurpleRequestFields *fields,const char *id)
{
  PurpleRequestField *field;
  do {
    if (fields != ((const PurpleRequestFields *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"fields != NULL");
      return 0;
    };
  }while (0);
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  if ((field = purple_request_fields_get_field(fields,id)) == ((PurpleRequestField *)((void *)0))) 
    return 0;
  return purple_request_field_string_get_value(field);
}

int purple_request_fields_get_integer(const PurpleRequestFields *fields,const char *id)
{
  PurpleRequestField *field;
  do {
    if (fields != ((const PurpleRequestFields *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"fields != NULL");
      return 0;
    };
  }while (0);
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  if ((field = purple_request_fields_get_field(fields,id)) == ((PurpleRequestField *)((void *)0))) 
    return 0;
  return purple_request_field_int_get_value(field);
}

gboolean purple_request_fields_get_bool(const PurpleRequestFields *fields,const char *id)
{
  PurpleRequestField *field;
  do {
    if (fields != ((const PurpleRequestFields *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"fields != NULL");
      return 0;
    };
  }while (0);
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  if ((field = purple_request_fields_get_field(fields,id)) == ((PurpleRequestField *)((void *)0))) 
    return 0;
  return purple_request_field_bool_get_value(field);
}

int purple_request_fields_get_choice(const PurpleRequestFields *fields,const char *id)
{
  PurpleRequestField *field;
  do {
    if (fields != ((const PurpleRequestFields *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"fields != NULL");
      return -1;
    };
  }while (0);
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return -1;
    };
  }while (0);
  if ((field = purple_request_fields_get_field(fields,id)) == ((PurpleRequestField *)((void *)0))) 
    return -1;
  return purple_request_field_choice_get_value(field);
}

PurpleAccount *purple_request_fields_get_account(const PurpleRequestFields *fields,const char *id)
{
  PurpleRequestField *field;
  do {
    if (fields != ((const PurpleRequestFields *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"fields != NULL");
      return 0;
    };
  }while (0);
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  if ((field = purple_request_fields_get_field(fields,id)) == ((PurpleRequestField *)((void *)0))) 
    return 0;
  return purple_request_field_account_get_value(field);
}

PurpleRequestFieldGroup *purple_request_field_group_new(const char *title)
{
  PurpleRequestFieldGroup *group;
  group = ((PurpleRequestFieldGroup *)(g_malloc0_n(1,(sizeof(PurpleRequestFieldGroup )))));
  group -> title = g_strdup(title);
  return group;
}

void purple_request_field_group_destroy(PurpleRequestFieldGroup *group)
{
  do {
    if (group != ((PurpleRequestFieldGroup *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"group != NULL");
      return ;
    };
  }while (0);
  g_free((group -> title));
  g_list_foreach((group -> fields),((GFunc )purple_request_field_destroy),0);
  g_list_free((group -> fields));
  g_free(group);
}

void purple_request_field_group_add_field(PurpleRequestFieldGroup *group,PurpleRequestField *field)
{
  do {
    if (group != ((PurpleRequestFieldGroup *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"group != NULL");
      return ;
    };
  }while (0);
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  group -> fields = g_list_append((group -> fields),field);
  if ((group -> fields_list) != ((PurpleRequestFields *)((void *)0))) {
    g_hash_table_insert(( *(group -> fields_list)).fields,(g_strdup(purple_request_field_get_id(field))),field);
    if (purple_request_field_is_required(field) != 0) {
      ( *(group -> fields_list)).required_fields = g_list_append(( *(group -> fields_list)).required_fields,field);
    }
  }
  field -> group = group;
}

const char *purple_request_field_group_get_title(const PurpleRequestFieldGroup *group)
{
  do {
    if (group != ((const PurpleRequestFieldGroup *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"group != NULL");
      return 0;
    };
  }while (0);
  return (group -> title);
}

GList *purple_request_field_group_get_fields(const PurpleRequestFieldGroup *group)
{
  do {
    if (group != ((const PurpleRequestFieldGroup *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"group != NULL");
      return 0;
    };
  }while (0);
  return group -> fields;
}

PurpleRequestField *purple_request_field_new(const char *id,const char *text,PurpleRequestFieldType type)
{
  PurpleRequestField *field;
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  do {
    if (type != PURPLE_REQUEST_FIELD_NONE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type != PURPLE_REQUEST_FIELD_NONE");
      return 0;
    };
  }while (0);
  field = ((PurpleRequestField *)(g_malloc0_n(1,(sizeof(PurpleRequestField )))));
  field -> id = g_strdup(id);
  field -> type = type;
  purple_request_field_set_label(field,text);
  purple_request_field_set_visible(field,(!0));
  return field;
}

void purple_request_field_destroy(PurpleRequestField *field)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  g_free((field -> id));
  g_free((field -> label));
  g_free((field -> type_hint));
  if ((field -> type) == PURPLE_REQUEST_FIELD_STRING) {
    g_free(field -> u.string.default_value);
    g_free(field -> u.string.value);
  }
  else if ((field -> type) == PURPLE_REQUEST_FIELD_CHOICE) {
    if (field -> u.choice.labels != ((GList *)((void *)0))) {
      g_list_foreach(field -> u.choice.labels,((GFunc )g_free),0);
      g_list_free(field -> u.choice.labels);
    }
  }
  else if ((field -> type) == PURPLE_REQUEST_FIELD_LIST) {
    if (field -> u.list.items != ((GList *)((void *)0))) {
      g_list_foreach(field -> u.list.items,((GFunc )g_free),0);
      g_list_free(field -> u.list.items);
    }
    if (field -> u.list.selected != ((GList *)((void *)0))) {
      g_list_foreach(field -> u.list.selected,((GFunc )g_free),0);
      g_list_free(field -> u.list.selected);
    }
    g_hash_table_destroy(field -> u.list.item_data);
    g_hash_table_destroy(field -> u.list.selected_table);
  }
  g_free(field);
}

void purple_request_field_set_label(PurpleRequestField *field,const char *label)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  g_free((field -> label));
  field -> label = g_strdup(label);
}

void purple_request_field_set_visible(PurpleRequestField *field,gboolean visible)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  field -> visible = visible;
}

void purple_request_field_set_type_hint(PurpleRequestField *field,const char *type_hint)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  g_free((field -> type_hint));
  field -> type_hint = g_strdup(type_hint);
}

void purple_request_field_set_required(PurpleRequestField *field,gboolean required)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  if ((field -> required) == required) 
    return ;
  field -> required = required;
  if ((field -> group) != ((PurpleRequestFieldGroup *)((void *)0))) {
    if (required != 0) {
      ( *( *(field -> group)).fields_list).required_fields = g_list_append(( *( *(field -> group)).fields_list).required_fields,field);
    }
    else {
      ( *( *(field -> group)).fields_list).required_fields = g_list_remove(( *( *(field -> group)).fields_list).required_fields,field);
    }
  }
}

PurpleRequestFieldType purple_request_field_get_type(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return PURPLE_REQUEST_FIELD_NONE;
    };
  }while (0);
  return field -> type;
}

PurpleRequestFieldGroup *purple_request_field_get_group(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  return field -> group;
}

const char *purple_request_field_get_id(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  return (field -> id);
}

const char *purple_request_field_get_label(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  return (field -> label);
}

gboolean purple_request_field_is_visible(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  return field -> visible;
}

const char *purple_request_field_get_type_hint(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  return (field -> type_hint);
}

gboolean purple_request_field_is_required(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  return field -> required;
}

PurpleRequestField *purple_request_field_string_new(const char *id,const char *text,const char *default_value,gboolean multiline)
{
  PurpleRequestField *field;
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  do {
    if (text != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"text != NULL");
      return 0;
    };
  }while (0);
  field = purple_request_field_new(id,text,PURPLE_REQUEST_FIELD_STRING);
  field -> u.string.multiline = multiline;
  field -> u.string.editable = (!0);
  purple_request_field_string_set_default_value(field,default_value);
  purple_request_field_string_set_value(field,default_value);
  return field;
}

void purple_request_field_string_set_default_value(PurpleRequestField *field,const char *default_value)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_STRING) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_STRING");
      return ;
    };
  }while (0);
  g_free(field -> u.string.default_value);
  field -> u.string.default_value = g_strdup(default_value);
}

void purple_request_field_string_set_value(PurpleRequestField *field,const char *value)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_STRING) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_STRING");
      return ;
    };
  }while (0);
  g_free(field -> u.string.value);
  field -> u.string.value = g_strdup(value);
}

void purple_request_field_string_set_masked(PurpleRequestField *field,gboolean masked)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_STRING) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_STRING");
      return ;
    };
  }while (0);
  field -> u.string.masked = masked;
}

void purple_request_field_string_set_editable(PurpleRequestField *field,gboolean editable)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_STRING) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_STRING");
      return ;
    };
  }while (0);
  field -> u.string.editable = editable;
}

const char *purple_request_field_string_get_default_value(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_STRING) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_STRING");
      return 0;
    };
  }while (0);
  return field -> u.string.default_value;
}

const char *purple_request_field_string_get_value(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_STRING) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_STRING");
      return 0;
    };
  }while (0);
  return field -> u.string.value;
}

gboolean purple_request_field_string_is_multiline(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_STRING) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_STRING");
      return 0;
    };
  }while (0);
  return field -> u.string.multiline;
}

gboolean purple_request_field_string_is_masked(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_STRING) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_STRING");
      return 0;
    };
  }while (0);
  return field -> u.string.masked;
}

gboolean purple_request_field_string_is_editable(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_STRING) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_STRING");
      return 0;
    };
  }while (0);
  return field -> u.string.editable;
}

PurpleRequestField *purple_request_field_int_new(const char *id,const char *text,int default_value)
{
  PurpleRequestField *field;
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  do {
    if (text != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"text != NULL");
      return 0;
    };
  }while (0);
  field = purple_request_field_new(id,text,PURPLE_REQUEST_FIELD_INTEGER);
  purple_request_field_int_set_default_value(field,default_value);
  purple_request_field_int_set_value(field,default_value);
  return field;
}

void purple_request_field_int_set_default_value(PurpleRequestField *field,int default_value)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_INTEGER) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_INTEGER");
      return ;
    };
  }while (0);
  field -> u.integer.default_value = default_value;
}

void purple_request_field_int_set_value(PurpleRequestField *field,int value)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_INTEGER) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_INTEGER");
      return ;
    };
  }while (0);
  field -> u.integer.value = value;
}

int purple_request_field_int_get_default_value(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_INTEGER) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_INTEGER");
      return 0;
    };
  }while (0);
  return field -> u.integer.default_value;
}

int purple_request_field_int_get_value(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_INTEGER) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_INTEGER");
      return 0;
    };
  }while (0);
  return field -> u.integer.value;
}

PurpleRequestField *purple_request_field_bool_new(const char *id,const char *text,gboolean default_value)
{
  PurpleRequestField *field;
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  do {
    if (text != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"text != NULL");
      return 0;
    };
  }while (0);
  field = purple_request_field_new(id,text,PURPLE_REQUEST_FIELD_BOOLEAN);
  purple_request_field_bool_set_default_value(field,default_value);
  purple_request_field_bool_set_value(field,default_value);
  return field;
}

void purple_request_field_bool_set_default_value(PurpleRequestField *field,gboolean default_value)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_BOOLEAN) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_BOOLEAN");
      return ;
    };
  }while (0);
  field -> u.boolean.default_value = default_value;
}

void purple_request_field_bool_set_value(PurpleRequestField *field,gboolean value)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_BOOLEAN) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_BOOLEAN");
      return ;
    };
  }while (0);
  field -> u.boolean.value = value;
}

gboolean purple_request_field_bool_get_default_value(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_BOOLEAN) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_BOOLEAN");
      return 0;
    };
  }while (0);
  return field -> u.boolean.default_value;
}

gboolean purple_request_field_bool_get_value(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_BOOLEAN) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_BOOLEAN");
      return 0;
    };
  }while (0);
  return field -> u.boolean.value;
}

PurpleRequestField *purple_request_field_choice_new(const char *id,const char *text,int default_value)
{
  PurpleRequestField *field;
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  do {
    if (text != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"text != NULL");
      return 0;
    };
  }while (0);
  field = purple_request_field_new(id,text,PURPLE_REQUEST_FIELD_CHOICE);
  purple_request_field_choice_set_default_value(field,default_value);
  purple_request_field_choice_set_value(field,default_value);
  return field;
}

void purple_request_field_choice_add(PurpleRequestField *field,const char *label)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if (label != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"label != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_CHOICE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_CHOICE");
      return ;
    };
  }while (0);
  field -> u.choice.labels = g_list_append(field -> u.choice.labels,(g_strdup(label)));
}

void purple_request_field_choice_set_default_value(PurpleRequestField *field,int default_value)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_CHOICE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_CHOICE");
      return ;
    };
  }while (0);
  field -> u.choice.default_value = default_value;
}

void purple_request_field_choice_set_value(PurpleRequestField *field,int value)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_CHOICE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_CHOICE");
      return ;
    };
  }while (0);
  field -> u.choice.value = value;
}

int purple_request_field_choice_get_default_value(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return -1;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_CHOICE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_CHOICE");
      return -1;
    };
  }while (0);
  return field -> u.choice.default_value;
}

int purple_request_field_choice_get_value(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return -1;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_CHOICE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_CHOICE");
      return -1;
    };
  }while (0);
  return field -> u.choice.value;
}

GList *purple_request_field_choice_get_labels(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_CHOICE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_CHOICE");
      return 0;
    };
  }while (0);
  return field -> u.choice.labels;
}

PurpleRequestField *purple_request_field_list_new(const char *id,const char *text)
{
  PurpleRequestField *field;
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  field = purple_request_field_new(id,text,PURPLE_REQUEST_FIELD_LIST);
  field -> u.list.item_data = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,0);
  field -> u.list.selected_table = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,0);
  return field;
}

void purple_request_field_list_set_multi_select(PurpleRequestField *field,gboolean multi_select)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_LIST) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_LIST");
      return ;
    };
  }while (0);
  field -> u.list.multiple_selection = multi_select;
}

gboolean purple_request_field_list_get_multi_select(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_LIST) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_LIST");
      return 0;
    };
  }while (0);
  return field -> u.list.multiple_selection;
}

void *purple_request_field_list_get_data(const PurpleRequestField *field,const char *text)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if (text != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"text != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_LIST) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_LIST");
      return 0;
    };
  }while (0);
  return g_hash_table_lookup(field -> u.list.item_data,text);
}

void purple_request_field_list_add(PurpleRequestField *field,const char *item,void *data)
{
  purple_request_field_list_add_icon(field,item,0,data);
}

void purple_request_field_list_add_icon(PurpleRequestField *field,const char *item,const char *icon_path,void *data)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if (item != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"item != NULL");
      return ;
    };
  }while (0);
  do {
    if (data != ((void *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"data != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_LIST) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_LIST");
      return ;
    };
  }while (0);
  if (icon_path != 0) {
    if (field -> u.list.icons == ((GList *)((void *)0))) {
      GList *l;
      for (l = field -> u.list.items; l != ((GList *)((void *)0)); l = (l -> next)) {
/* Order doesn't matter, because we're just
				 * filing in blank items.  So, we use
				 * g_list_prepend() because it's faster. */
        field -> u.list.icons = g_list_prepend(field -> u.list.icons,0);
      }
    }
    field -> u.list.icons = g_list_append(field -> u.list.icons,(g_strdup(icon_path)));
  }
  else if (field -> u.list.icons != 0) {
/* Keep this even with the items list. */
    field -> u.list.icons = g_list_append(field -> u.list.icons,0);
  }
  field -> u.list.items = g_list_append(field -> u.list.items,(g_strdup(item)));
  g_hash_table_insert(field -> u.list.item_data,(g_strdup(item)),data);
}

void purple_request_field_list_add_selected(PurpleRequestField *field,const char *item)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if (item != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"item != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_LIST) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_LIST");
      return ;
    };
  }while (0);
  if (!(purple_request_field_list_get_multi_select(field) != 0) && (field -> u.list.selected != ((GList *)((void *)0)))) {
    purple_debug_warning("request","More than one item added to non-multi-select field %s\n",purple_request_field_get_id(field));
    return ;
  }
  field -> u.list.selected = g_list_append(field -> u.list.selected,(g_strdup(item)));
  g_hash_table_insert(field -> u.list.selected_table,(g_strdup(item)),0);
}

void purple_request_field_list_clear_selected(PurpleRequestField *field)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_LIST) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_LIST");
      return ;
    };
  }while (0);
  if (field -> u.list.selected != ((GList *)((void *)0))) {
    g_list_foreach(field -> u.list.selected,((GFunc )g_free),0);
    g_list_free(field -> u.list.selected);
    field -> u.list.selected = ((GList *)((void *)0));
  }
  g_hash_table_destroy(field -> u.list.selected_table);
  field -> u.list.selected_table = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,0);
}

void purple_request_field_list_set_selected(PurpleRequestField *field,GList *items)
{
  GList *l;
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if (items != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"items != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_LIST) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_LIST");
      return ;
    };
  }while (0);
  purple_request_field_list_clear_selected(field);
  if ((!(purple_request_field_list_get_multi_select(field) != 0) && (items != 0)) && ((items -> next) != 0)) {
    purple_debug_warning("request","More than one item added to non-multi-select field %s\n",purple_request_field_get_id(field));
    return ;
  }
  for (l = items; l != ((GList *)((void *)0)); l = (l -> next)) {
    field -> u.list.selected = g_list_append(field -> u.list.selected,(g_strdup((l -> data))));
    g_hash_table_insert(field -> u.list.selected_table,(g_strdup(((char *)(l -> data)))),0);
  }
}

gboolean purple_request_field_list_is_selected(const PurpleRequestField *field,const char *item)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if (item != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"item != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_LIST) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_LIST");
      return 0;
    };
  }while (0);
  return g_hash_table_lookup_extended(field -> u.list.selected_table,item,0,0);
}

GList *purple_request_field_list_get_selected(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_LIST) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_LIST");
      return 0;
    };
  }while (0);
  return field -> u.list.selected;
}

GList *purple_request_field_list_get_items(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_LIST) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_LIST");
      return 0;
    };
  }while (0);
  return field -> u.list.items;
}

GList *purple_request_field_list_get_icons(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_LIST) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_LIST");
      return 0;
    };
  }while (0);
  return field -> u.list.icons;
}

PurpleRequestField *purple_request_field_label_new(const char *id,const char *text)
{
  PurpleRequestField *field;
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  do {
    if (text != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"text != NULL");
      return 0;
    };
  }while (0);
  field = purple_request_field_new(id,text,PURPLE_REQUEST_FIELD_LABEL);
  return field;
}

PurpleRequestField *purple_request_field_image_new(const char *id,const char *text,const char *buf,gsize size)
{
  PurpleRequestField *field;
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  do {
    if (text != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"text != NULL");
      return 0;
    };
  }while (0);
  do {
    if (buf != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"buf != NULL");
      return 0;
    };
  }while (0);
  do {
    if (size > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"size > 0");
      return 0;
    };
  }while (0);
  field = purple_request_field_new(id,text,PURPLE_REQUEST_FIELD_IMAGE);
  field -> u.image.buffer = (g_memdup(buf,size));
  field -> u.image.size = size;
  field -> u.image.scale_x = 1;
  field -> u.image.scale_y = 1;
  return field;
}

void purple_request_field_image_set_scale(PurpleRequestField *field,unsigned int x,unsigned int y)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_IMAGE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_IMAGE");
      return ;
    };
  }while (0);
  field -> u.image.scale_x = x;
  field -> u.image.scale_y = y;
}

const char *purple_request_field_image_get_buffer(PurpleRequestField *field)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_IMAGE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_IMAGE");
      return 0;
    };
  }while (0);
  return field -> u.image.buffer;
}

gsize purple_request_field_image_get_size(PurpleRequestField *field)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_IMAGE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_IMAGE");
      return 0;
    };
  }while (0);
  return field -> u.image.size;
}

unsigned int purple_request_field_image_get_scale_x(PurpleRequestField *field)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_IMAGE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_IMAGE");
      return 0;
    };
  }while (0);
  return field -> u.image.scale_x;
}

unsigned int purple_request_field_image_get_scale_y(PurpleRequestField *field)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_IMAGE) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_IMAGE");
      return 0;
    };
  }while (0);
  return field -> u.image.scale_y;
}

PurpleRequestField *purple_request_field_account_new(const char *id,const char *text,PurpleAccount *account)
{
  PurpleRequestField *field;
  do {
    if (id != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"id != NULL");
      return 0;
    };
  }while (0);
  do {
    if (text != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"text != NULL");
      return 0;
    };
  }while (0);
  field = purple_request_field_new(id,text,PURPLE_REQUEST_FIELD_ACCOUNT);
  if ((account == ((PurpleAccount *)((void *)0))) && (purple_connections_get_all() != ((GList *)((void *)0)))) {
    account = purple_connection_get_account(((PurpleConnection *)( *purple_connections_get_all()).data));
  }
  purple_request_field_account_set_default_value(field,account);
  purple_request_field_account_set_value(field,account);
  return field;
}

void purple_request_field_account_set_default_value(PurpleRequestField *field,PurpleAccount *default_value)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_ACCOUNT) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_ACCOUNT");
      return ;
    };
  }while (0);
  field -> u.account.default_account = default_value;
}

void purple_request_field_account_set_value(PurpleRequestField *field,PurpleAccount *value)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_ACCOUNT) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_ACCOUNT");
      return ;
    };
  }while (0);
  field -> u.account.account = value;
}

void purple_request_field_account_set_show_all(PurpleRequestField *field,gboolean show_all)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_ACCOUNT) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_ACCOUNT");
      return ;
    };
  }while (0);
  if (field -> u.account.show_all == show_all) 
    return ;
  field -> u.account.show_all = show_all;
  if (!(show_all != 0)) {
    if (purple_account_is_connected(field -> u.account.default_account) != 0) {
      purple_request_field_account_set_default_value(field,((PurpleAccount *)( *purple_connections_get_all()).data));
    }
    if (purple_account_is_connected(field -> u.account.account) != 0) {
      purple_request_field_account_set_value(field,((PurpleAccount *)( *purple_connections_get_all()).data));
    }
  }
}

void purple_request_field_account_set_filter(PurpleRequestField *field,PurpleFilterAccountFunc filter_func)
{
  do {
    if (field != ((PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return ;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_ACCOUNT) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_ACCOUNT");
      return ;
    };
  }while (0);
  field -> u.account.filter_func = filter_func;
}

PurpleAccount *purple_request_field_account_get_default_value(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_ACCOUNT) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_ACCOUNT");
      return 0;
    };
  }while (0);
  return field -> u.account.default_account;
}

PurpleAccount *purple_request_field_account_get_value(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_ACCOUNT) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_ACCOUNT");
      return 0;
    };
  }while (0);
  return field -> u.account.account;
}

gboolean purple_request_field_account_get_show_all(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_ACCOUNT) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_ACCOUNT");
      return 0;
    };
  }while (0);
  return field -> u.account.show_all;
}

PurpleFilterAccountFunc purple_request_field_account_get_filter(const PurpleRequestField *field)
{
  do {
    if (field != ((const PurpleRequestField *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field != NULL");
      return 0;
    };
  }while (0);
  do {
    if ((field -> type) == PURPLE_REQUEST_FIELD_ACCOUNT) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"field->type == PURPLE_REQUEST_FIELD_ACCOUNT");
      return 0;
    };
  }while (0);
  return field -> u.account.filter_func;
}
/* -- */

void *purple_request_input(void *handle,const char *title,const char *primary,const char *secondary,const char *default_value,gboolean multiline,gboolean masked,gchar *hint,const char *ok_text,GCallback ok_cb,const char *cancel_text,GCallback cancel_cb,PurpleAccount *account,const char *who,PurpleConversation *conv,void *user_data)
{
  PurpleRequestUiOps *ops;
  do {
    if (ok_text != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ok_text != NULL");
      return 0;
    };
  }while (0);
  do {
    if (ok_cb != ((void (*)())((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ok_cb != NULL");
      return 0;
    };
  }while (0);
  ops = purple_request_get_ui_ops();
  if ((ops != ((PurpleRequestUiOps *)((void *)0))) && ((ops -> request_input) != ((void *(*)(const char *, const char *, const char *, const char *, gboolean , gboolean , gchar *, const char *, GCallback , const char *, GCallback , PurpleAccount *, const char *, PurpleConversation *, void *))((void *)0)))) {
    PurpleRequestInfo *info;
    info = ((PurpleRequestInfo *)(g_malloc0_n(1,(sizeof(PurpleRequestInfo )))));
    info -> type = PURPLE_REQUEST_INPUT;
    info -> handle = handle;
    info -> ui_handle = ( *(ops -> request_input))(title,primary,secondary,default_value,multiline,masked,hint,ok_text,ok_cb,cancel_text,cancel_cb,account,who,conv,user_data);
    handles = g_list_append(handles,info);
    return info -> ui_handle;
  }
  return 0;
}

void *purple_request_choice(void *handle,const char *title,const char *primary,const char *secondary,int default_value,const char *ok_text,GCallback ok_cb,const char *cancel_text,GCallback cancel_cb,PurpleAccount *account,const char *who,PurpleConversation *conv,void *user_data,... )
{
  void *ui_handle;
  va_list args;
  do {
    if (ok_text != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ok_text != NULL");
      return 0;
    };
  }while (0);
  do {
    if (ok_cb != ((void (*)())((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ok_cb != NULL");
      return 0;
    };
  }while (0);
  va_start(args,user_data);
  ui_handle = purple_request_choice_varg(handle,title,primary,secondary,default_value,ok_text,ok_cb,cancel_text,cancel_cb,account,who,conv,user_data,args);
  va_end(args);
  return ui_handle;
}

void *purple_request_choice_varg(void *handle,const char *title,const char *primary,const char *secondary,int default_value,const char *ok_text,GCallback ok_cb,const char *cancel_text,GCallback cancel_cb,PurpleAccount *account,const char *who,PurpleConversation *conv,void *user_data,va_list choices)
{
  PurpleRequestUiOps *ops;
  do {
    if (ok_text != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ok_text != NULL");
      return 0;
    };
  }while (0);
  do {
    if (ok_cb != ((void (*)())((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ok_cb != NULL");
      return 0;
    };
  }while (0);
  do {
    if (cancel_text != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cancel_text != NULL");
      return 0;
    };
  }while (0);
  ops = purple_request_get_ui_ops();
  if ((ops != ((PurpleRequestUiOps *)((void *)0))) && ((ops -> request_choice) != ((void *(*)(const char *, const char *, const char *, int , const char *, GCallback , const char *, GCallback , PurpleAccount *, const char *, PurpleConversation *, void *, va_list ))((void *)0)))) {
    PurpleRequestInfo *info;
    info = ((PurpleRequestInfo *)(g_malloc0_n(1,(sizeof(PurpleRequestInfo )))));
    info -> type = PURPLE_REQUEST_CHOICE;
    info -> handle = handle;
    info -> ui_handle = ( *(ops -> request_choice))(title,primary,secondary,default_value,ok_text,ok_cb,cancel_text,cancel_cb,account,who,conv,user_data,choices);
    handles = g_list_append(handles,info);
    return info -> ui_handle;
  }
  return 0;
}

void *purple_request_action(void *handle,const char *title,const char *primary,const char *secondary,int default_action,PurpleAccount *account,const char *who,PurpleConversation *conv,void *user_data,size_t action_count,... )
{
  void *ui_handle;
  va_list args;
  do {
    if (action_count > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"action_count > 0");
      return 0;
    };
  }while (0);
  va_start(args,action_count);
  ui_handle = purple_request_action_varg(handle,title,primary,secondary,default_action,account,who,conv,user_data,action_count,args);
  va_end(args);
  return ui_handle;
}

void *purple_request_action_with_icon(void *handle,const char *title,const char *primary,const char *secondary,int default_action,PurpleAccount *account,const char *who,PurpleConversation *conv,gconstpointer icon_data,gsize icon_size,void *user_data,size_t action_count,... )
{
  void *ui_handle;
  va_list args;
  do {
    if (action_count > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"action_count > 0");
      return 0;
    };
  }while (0);
  va_start(args,action_count);
  ui_handle = purple_request_action_with_icon_varg(handle,title,primary,secondary,default_action,account,who,conv,icon_data,icon_size,user_data,action_count,args);
  va_end(args);
  return ui_handle;
}

void *purple_request_action_varg(void *handle,const char *title,const char *primary,const char *secondary,int default_action,PurpleAccount *account,const char *who,PurpleConversation *conv,void *user_data,size_t action_count,va_list actions)
{
  PurpleRequestUiOps *ops;
  do {
    if (action_count > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"action_count > 0");
      return 0;
    };
  }while (0);
  ops = purple_request_get_ui_ops();
  if ((ops != ((PurpleRequestUiOps *)((void *)0))) && ((ops -> request_action) != ((void *(*)(const char *, const char *, const char *, int , PurpleAccount *, const char *, PurpleConversation *, void *, size_t , va_list ))((void *)0)))) {
    PurpleRequestInfo *info;
    info = ((PurpleRequestInfo *)(g_malloc0_n(1,(sizeof(PurpleRequestInfo )))));
    info -> type = PURPLE_REQUEST_ACTION;
    info -> handle = handle;
    info -> ui_handle = ( *(ops -> request_action))(title,primary,secondary,default_action,account,who,conv,user_data,action_count,actions);
    handles = g_list_append(handles,info);
    return info -> ui_handle;
  }
  return 0;
}

void *purple_request_action_with_icon_varg(void *handle,const char *title,const char *primary,const char *secondary,int default_action,PurpleAccount *account,const char *who,PurpleConversation *conv,gconstpointer icon_data,gsize icon_size,void *user_data,size_t action_count,va_list actions)
{
  PurpleRequestUiOps *ops;
  do {
    if (action_count > 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"action_count > 0");
      return 0;
    };
  }while (0);
  ops = purple_request_get_ui_ops();
  if ((ops != ((PurpleRequestUiOps *)((void *)0))) && ((ops -> request_action_with_icon) != ((void *(*)(const char *, const char *, const char *, int , PurpleAccount *, const char *, PurpleConversation *, gconstpointer , gsize , void *, size_t , va_list ))((void *)0)))) {
    PurpleRequestInfo *info;
    info = ((PurpleRequestInfo *)(g_malloc0_n(1,(sizeof(PurpleRequestInfo )))));
    info -> type = PURPLE_REQUEST_ACTION;
    info -> handle = handle;
    info -> ui_handle = ( *(ops -> request_action_with_icon))(title,primary,secondary,default_action,account,who,conv,icon_data,icon_size,user_data,action_count,actions);
    handles = g_list_append(handles,info);
    return info -> ui_handle;
  }
  else {
/* Fall back on the non-icon request if the UI doesn't support icon
		 requests */
    return purple_request_action_varg(handle,title,primary,secondary,default_action,account,who,conv,user_data,action_count,actions);
  }
  return 0;
}

void *purple_request_fields(void *handle,const char *title,const char *primary,const char *secondary,PurpleRequestFields *fields,const char *ok_text,GCallback ok_cb,const char *cancel_text,GCallback cancel_cb,PurpleAccount *account,const char *who,PurpleConversation *conv,void *user_data)
{
  PurpleRequestUiOps *ops;
  do {
    if (fields != ((PurpleRequestFields *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"fields != NULL");
      return 0;
    };
  }while (0);
  do {
    if (ok_text != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ok_text != NULL");
      return 0;
    };
  }while (0);
  do {
    if (ok_cb != ((void (*)())((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ok_cb != NULL");
      return 0;
    };
  }while (0);
  do {
    if (cancel_text != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cancel_text != NULL");
      return 0;
    };
  }while (0);
  ops = purple_request_get_ui_ops();
  if ((ops != ((PurpleRequestUiOps *)((void *)0))) && ((ops -> request_fields) != ((void *(*)(const char *, const char *, const char *, PurpleRequestFields *, const char *, GCallback , const char *, GCallback , PurpleAccount *, const char *, PurpleConversation *, void *))((void *)0)))) {
    PurpleRequestInfo *info;
    info = ((PurpleRequestInfo *)(g_malloc0_n(1,(sizeof(PurpleRequestInfo )))));
    info -> type = PURPLE_REQUEST_FIELDS;
    info -> handle = handle;
    info -> ui_handle = ( *(ops -> request_fields))(title,primary,secondary,fields,ok_text,ok_cb,cancel_text,cancel_cb,account,who,conv,user_data);
    handles = g_list_append(handles,info);
    return info -> ui_handle;
  }
  return 0;
}

void *purple_request_file(void *handle,const char *title,const char *filename,gboolean savedialog,GCallback ok_cb,GCallback cancel_cb,PurpleAccount *account,const char *who,PurpleConversation *conv,void *user_data)
{
  PurpleRequestUiOps *ops;
  ops = purple_request_get_ui_ops();
  if ((ops != ((PurpleRequestUiOps *)((void *)0))) && ((ops -> request_file) != ((void *(*)(const char *, const char *, gboolean , GCallback , GCallback , PurpleAccount *, const char *, PurpleConversation *, void *))((void *)0)))) {
    PurpleRequestInfo *info;
    info = ((PurpleRequestInfo *)(g_malloc0_n(1,(sizeof(PurpleRequestInfo )))));
    info -> type = PURPLE_REQUEST_FILE;
    info -> handle = handle;
    info -> ui_handle = ( *(ops -> request_file))(title,filename,savedialog,ok_cb,cancel_cb,account,who,conv,user_data);
    handles = g_list_append(handles,info);
    return info -> ui_handle;
  }
  return 0;
}

void *purple_request_folder(void *handle,const char *title,const char *dirname,GCallback ok_cb,GCallback cancel_cb,PurpleAccount *account,const char *who,PurpleConversation *conv,void *user_data)
{
  PurpleRequestUiOps *ops;
  ops = purple_request_get_ui_ops();
  if ((ops != ((PurpleRequestUiOps *)((void *)0))) && ((ops -> request_file) != ((void *(*)(const char *, const char *, gboolean , GCallback , GCallback , PurpleAccount *, const char *, PurpleConversation *, void *))((void *)0)))) {
    PurpleRequestInfo *info;
    info = ((PurpleRequestInfo *)(g_malloc0_n(1,(sizeof(PurpleRequestInfo )))));
    info -> type = PURPLE_REQUEST_FOLDER;
    info -> handle = handle;
    info -> ui_handle = ( *(ops -> request_folder))(title,dirname,ok_cb,cancel_cb,account,who,conv,user_data);
    handles = g_list_append(handles,info);
    return info -> ui_handle;
  }
  return 0;
}

static void purple_request_close_info(PurpleRequestInfo *info)
{
  PurpleRequestUiOps *ops;
  ops = purple_request_get_ui_ops();
  purple_notify_close_with_handle((info -> ui_handle));
  purple_request_close_with_handle((info -> ui_handle));
  if ((ops != ((PurpleRequestUiOps *)((void *)0))) && ((ops -> close_request) != ((void (*)(PurpleRequestType , void *))((void *)0)))) 
    ( *(ops -> close_request))((info -> type),(info -> ui_handle));
  g_free(info);
}

void purple_request_close(PurpleRequestType type,void *ui_handle)
{
  GList *l;
  do {
    if (ui_handle != ((void *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ui_handle != NULL");
      return ;
    };
  }while (0);
{
    for (l = handles; l != ((GList *)((void *)0)); l = (l -> next)) {
      PurpleRequestInfo *info = (l -> data);
      if ((info -> ui_handle) == ui_handle) {
        handles = g_list_remove(handles,info);
        purple_request_close_info(info);
        break; 
      }
    }
  }
}

void purple_request_close_with_handle(void *handle)
{
  GList *l;
  GList *l_next;
  do {
    if (handle != ((void *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"handle != NULL");
      return ;
    };
  }while (0);
  for (l = handles; l != ((GList *)((void *)0)); l = l_next) {
    PurpleRequestInfo *info = (l -> data);
    l_next = (l -> next);
    if ((info -> handle) == handle) {
      handles = g_list_remove(handles,info);
      purple_request_close_info(info);
    }
  }
}

void purple_request_set_ui_ops(PurpleRequestUiOps *ops)
{
  request_ui_ops = ops;
}

PurpleRequestUiOps *purple_request_get_ui_ops()
{
  return request_ui_ops;
}
