/*
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "blist.h"
#include "cmds.h"
#include "conversation.h"
#include "dbus-maybe.h"
#include "debug.h"
#include "imgstore.h"
#include "notify.h"
#include "prefs.h"
#include "prpl.h"
#include "request.h"
#include "signals.h"
#include "util.h"
#define SEND_TYPED_TIMEOUT_SECONDS 5
static GList *conversations = (GList *)((void *)0);
static GList *ims = (GList *)((void *)0);
static GList *chats = (GList *)((void *)0);
static PurpleConversationUiOps *default_ops = (PurpleConversationUiOps *)((void *)0);
/**
 * A hash table used for efficient lookups of conversations by name.
 * struct _purple_hconv => PurpleConversation*
 */
static GHashTable *conversation_cache = (GHashTable *)((void *)0);

struct _purple_hconv 
{
  PurpleConversationType type;
  char *name;
  const PurpleAccount *account;
}
;

static guint _purple_conversations_hconv_hash(struct _purple_hconv *hc)
{
  return (g_str_hash((hc -> name)) ^ (hc -> type)) ^ g_direct_hash((hc -> account));
}

static guint _purple_conversations_hconv_equal(struct _purple_hconv *hc1,struct _purple_hconv *hc2)
{
  return ((((hc1 -> type) == (hc2 -> type)) && ((hc1 -> account) == (hc2 -> account))) && (g_str_equal((hc1 -> name),(hc2 -> name)) != 0));
}

static void _purple_conversations_hconv_free_key(struct _purple_hconv *hc)
{
  g_free((hc -> name));
  g_free(hc);
}

static guint _purple_conversation_user_hash(gconstpointer data)
{
  const gchar *name = data;
  gchar *collated;
  guint hash;
  collated = g_utf8_collate_key(name,(-1));
  hash = g_str_hash(collated);
  g_free(collated);
  return hash;
}

static gboolean _purple_conversation_user_equal(gconstpointer a,gconstpointer b)
{
  return !(g_utf8_collate(a,b) != 0);
}

void purple_conversations_set_ui_ops(PurpleConversationUiOps *ops)
{
  default_ops = ops;
}

static gboolean reset_typing_cb(gpointer data)
{
  PurpleConversation *c = (PurpleConversation *)data;
  PurpleConvIm *im;
  im = purple_conversation_get_im_data(c);
  purple_conv_im_set_typing_state(im,PURPLE_NOT_TYPING);
  purple_conv_im_stop_typing_timeout(im);
  return 0;
}

static gboolean send_typed_cb(gpointer data)
{
  PurpleConversation *conv = (PurpleConversation *)data;
  PurpleConnection *gc;
  const char *name;
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return 0;
    };
  }while (0);
  gc = purple_conversation_get_gc(conv);
  name = purple_conversation_get_name(conv);
  if ((gc != ((PurpleConnection *)((void *)0))) && (name != ((const char *)((void *)0)))) {
/* We set this to 1 so that PURPLE_TYPING will be sent
		 * if the Purple user types anything else.
		 */
    purple_conv_im_set_type_again(purple_conversation_get_im_data(conv),1);
    serv_send_typing(gc,name,PURPLE_TYPED);
    purple_debug(PURPLE_DEBUG_MISC,"conversation","typed...\n");
  }
  return 0;
}

static void common_send(PurpleConversation *conv,const char *message,PurpleMessageFlags msgflags)
{
  PurpleConversationType type;
  PurpleAccount *account;
  PurpleConnection *gc;
  char *displayed = (char *)((void *)0);
  char *sent = (char *)((void *)0);
  int err = 0;
  if (( *message) == 0) 
    return ;
  account = purple_conversation_get_account(conv);
  gc = purple_conversation_get_gc(conv);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return ;
    };
  }while (0);
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  type = purple_conversation_get_type(conv);
/* Always linkfy the text for display, unless we're
	 * explicitly asked to do otheriwse*/
  if (!((msgflags & PURPLE_MESSAGE_INVISIBLE) != 0U)) {
    if ((msgflags & PURPLE_MESSAGE_NO_LINKIFY) != 0U) 
      displayed = g_strdup(message);
    else 
      displayed = purple_markup_linkify(message);
  }
  if (((displayed != 0) && (((conv -> features) & PURPLE_CONNECTION_HTML) != 0U)) && !((msgflags & PURPLE_MESSAGE_RAW) != 0U)) {
    sent = g_strdup(displayed);
  }
  else 
    sent = g_strdup(message);
  msgflags |= PURPLE_MESSAGE_SEND;
  if (type == PURPLE_CONV_TYPE_IM) {
    PurpleConvIm *im = purple_conversation_get_im_data(conv);
    purple_signal_emit(purple_conversations_get_handle(),"sending-im-msg",account,purple_conversation_get_name(conv),&sent);
    if ((sent != ((char *)((void *)0))) && (sent[0] != 0)) {
      err = serv_send_im(gc,purple_conversation_get_name(conv),sent,msgflags);
      if ((err > 0) && (displayed != ((char *)((void *)0)))) 
        purple_conv_im_write(im,0,displayed,msgflags,time(0));
      purple_signal_emit(purple_conversations_get_handle(),"sent-im-msg",account,purple_conversation_get_name(conv),sent);
    }
  }
  else {
    purple_signal_emit(purple_conversations_get_handle(),"sending-chat-msg",account,&sent,purple_conv_chat_get_id((purple_conversation_get_chat_data(conv))));
    if ((sent != ((char *)((void *)0))) && (sent[0] != 0)) {
      err = serv_chat_send(gc,purple_conv_chat_get_id((purple_conversation_get_chat_data(conv))),sent,msgflags);
      purple_signal_emit(purple_conversations_get_handle(),"sent-chat-msg",account,sent,purple_conv_chat_get_id((purple_conversation_get_chat_data(conv))));
    }
  }
  if (err < 0) {
    const char *who;
    const char *msg;
    who = purple_conversation_get_name(conv);
    if (err == -7) {
      msg = ((const char *)(dgettext("pidgin","Unable to send message: The message is too large.")));
      if (!(purple_conv_present_error(who,account,msg) != 0)) {
        char *msg2 = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to send message to %s."))),who);
        purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,msg2,((const char *)(dgettext("pidgin","The message is too large."))),0,0);
        g_free(msg2);
      }
    }
    else if (err == -107) {
      purple_debug(PURPLE_DEBUG_ERROR,"conversation","Not yet connected.\n");
    }
    else {
      msg = ((const char *)(dgettext("pidgin","Unable to send message.")));
      if (!(purple_conv_present_error(who,account,msg) != 0)) {
        char *msg2 = g_strdup_printf(((const char *)(dgettext("pidgin","Unable to send message to %s."))),who);
        purple_notify_message(gc,PURPLE_NOTIFY_MSG_ERROR,0,msg2,0,0,0);
        g_free(msg2);
      }
    }
  }
  g_free(displayed);
  g_free(sent);
}

static void open_log(PurpleConversation *conv)
{
  conv -> logs = g_list_append(0,(purple_log_new(((((conv -> type) == PURPLE_CONV_TYPE_CHAT)?PURPLE_LOG_CHAT : PURPLE_LOG_IM)),(conv -> name),(conv -> account),conv,time(0),0)));
}
/* Functions that deal with PurpleConvMessage */

static void add_message_to_history(PurpleConversation *conv,const char *who,const char *alias,const char *message,PurpleMessageFlags flags,time_t when)
{
  PurpleConvMessage *msg;
  PurpleConnection *gc;
  gc = purple_account_get_connection((conv -> account));
  if ((flags & PURPLE_MESSAGE_SEND) != 0U) {
    const char *me = (const char *)((void *)0);
    if (gc != 0) 
      me = purple_connection_get_display_name(gc);
    if (!(me != 0)) 
      me = ( *(conv -> account)).username;
    who = me;
  }
  msg = ((PurpleConvMessage *)(g_malloc0_n(1,(sizeof(PurpleConvMessage )))));
{
    PurpleConvMessage *typed_ptr = msg;
    purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurpleConvMessage);
  };
  msg -> who = g_strdup(who);
  msg -> alias = g_strdup(alias);
  msg -> flags = flags;
  msg -> what = g_strdup(message);
  msg -> when = when;
  msg -> conv = conv;
  conv -> message_history = g_list_prepend((conv -> message_history),msg);
}

static void free_conv_message(PurpleConvMessage *msg)
{
  g_free((msg -> who));
  g_free((msg -> alias));
  g_free((msg -> what));
  purple_dbus_unregister_pointer(msg);
  g_free(msg);
}

static void message_history_free(GList *list)
{
  g_list_foreach(list,((GFunc )free_conv_message),0);
  g_list_free(list);
}
/**************************************************************************
 * Conversation API
 **************************************************************************/

static void purple_conversation_chat_cleanup_for_rejoin(PurpleConversation *conv)
{
  const char *disp;
  PurpleAccount *account;
  PurpleConnection *gc;
  account = purple_conversation_get_account(conv);
  purple_conversation_close_logs(conv);
  open_log(conv);
  gc = purple_account_get_connection(account);
  if ((disp = purple_connection_get_display_name(gc)) != ((const char *)((void *)0))) 
    purple_conv_chat_set_nick(purple_conversation_get_chat_data(conv),disp);
  else {
    purple_conv_chat_set_nick(purple_conversation_get_chat_data(conv),purple_account_get_username(account));
  }
  purple_conv_chat_clear_users(purple_conversation_get_chat_data(conv));
  purple_conv_chat_set_topic(purple_conversation_get_chat_data(conv),0,0);
  ( *purple_conversation_get_chat_data(conv)).left = 0;
  purple_conversation_update(conv,PURPLE_CONV_UPDATE_CHATLEFT);
}

PurpleConversation *purple_conversation_new(PurpleConversationType type,PurpleAccount *account,const char *name)
{
  PurpleConversation *conv;
  PurpleConnection *gc;
  PurpleConversationUiOps *ops;
  struct _purple_hconv *hc;
  do {
    if (type != PURPLE_CONV_TYPE_UNKNOWN) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"type != PURPLE_CONV_TYPE_UNKNOWN");
      return 0;
    };
  }while (0);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account != NULL");
      return 0;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return 0;
    };
  }while (0);
/* Check if this conversation already exists. */
  if ((conv = purple_find_conversation_with_account(type,name,account)) != ((PurpleConversation *)((void *)0))) {
    if (((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) && !(purple_conv_chat_has_left(purple_conversation_get_chat_data(conv)) != 0)) {
      purple_debug_warning("conversation","Trying to create multiple chats (%s) with the same name is deprecated and will be removed in libpurple 3.0.0",name);
    }
/*
		 * This hack is necessary because some prpls (MSN) have unnamed chats
		 * that all use the same name.  A PurpleConversation for one of those
		 * is only ever re-used if the user has left, so calls to
		 * purple_conversation_new need to fall-through to creating a new
		 * chat.
		 * TODO 3.0.0: Remove this workaround and mandate unique names.
		 */
    if (((purple_conversation_get_type(conv)) != PURPLE_CONV_TYPE_CHAT) || (purple_conv_chat_has_left(purple_conversation_get_chat_data(conv)) != 0)) {
      if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) 
        purple_conversation_chat_cleanup_for_rejoin(conv);
      return conv;
    }
  }
  gc = purple_account_get_connection(account);
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return 0;
    };
  }while (0);
  conv = ((PurpleConversation *)(g_malloc0_n(1,(sizeof(PurpleConversation )))));
{
    PurpleConversation *typed_ptr = conv;
    purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurpleConversation);
  };
  conv -> type = type;
  conv -> account = account;
  conv -> name = g_strdup(name);
  conv -> title = g_strdup(name);
  conv -> data = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,0);
/* copy features from the connection. */
  conv -> features = (gc -> flags);
  if (type == PURPLE_CONV_TYPE_IM) {
    PurpleBuddyIcon *icon;
    conv -> u.im = ((PurpleConvIm *)(g_malloc0_n(1,(sizeof(PurpleConvIm )))));
    ( *conv -> u.im).conv = conv;
{
      PurpleConvIm *typed_ptr = conv -> u.im;
      purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurpleConvIm);
    };
    ims = g_list_prepend(ims,conv);
    if ((icon = purple_buddy_icons_find(account,name)) != 0) {
      purple_conv_im_set_icon(conv -> u.im,icon);
/* purple_conv_im_set_icon refs the icon. */
      purple_buddy_icon_unref(icon);
    }
    if (purple_prefs_get_bool("/purple/logging/log_ims") != 0) {
      purple_conversation_set_logging(conv,(!0));
      open_log(conv);
    }
  }
  else if (type == PURPLE_CONV_TYPE_CHAT) {
    const char *disp;
    conv -> u.chat = ((PurpleConvChat *)(g_malloc0_n(1,(sizeof(PurpleConvChat )))));
    ( *conv -> u.chat).conv = conv;
    ( *conv -> u.chat).users = g_hash_table_new_full(_purple_conversation_user_hash,_purple_conversation_user_equal,g_free,0);
{
      PurpleConvChat *typed_ptr = conv -> u.chat;
      purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurpleConvChat);
    };
    chats = g_list_prepend(chats,conv);
    if ((disp = purple_connection_get_display_name((account -> gc))) != 0) 
      purple_conv_chat_set_nick(conv -> u.chat,disp);
    else 
      purple_conv_chat_set_nick(conv -> u.chat,purple_account_get_username(account));
    if (purple_prefs_get_bool("/purple/logging/log_chats") != 0) {
      purple_conversation_set_logging(conv,(!0));
      open_log(conv);
    }
  }
  conversations = g_list_prepend(conversations,conv);
  hc = ((struct _purple_hconv *)(g_malloc_n(1,(sizeof(struct _purple_hconv )))));
  hc -> name = g_strdup(purple_normalize(account,(conv -> name)));
  hc -> account = account;
  hc -> type = type;
  g_hash_table_insert(conversation_cache,hc,conv);
/* Auto-set the title. */
  purple_conversation_autoset_title(conv);
/* Don't move this.. it needs to be one of the last things done otherwise
	 * it causes mysterious crashes on my system.
	 *  -- Gary
	 */
  ops = (conv -> ui_ops = default_ops);
  if ((ops != ((PurpleConversationUiOps *)((void *)0))) && ((ops -> create_conversation) != ((void (*)(PurpleConversation *))((void *)0)))) 
    ( *(ops -> create_conversation))(conv);
  purple_signal_emit(purple_conversations_get_handle(),"conversation-created",conv);
  return conv;
}

void purple_conversation_destroy(PurpleConversation *conv)
{
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurpleConversationUiOps *ops;
  PurpleConnection *gc;
  const char *name;
  struct _purple_hconv hc;
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  purple_request_close_with_handle(conv);
  ops = purple_conversation_get_ui_ops(conv);
  gc = purple_conversation_get_gc(conv);
  name = purple_conversation_get_name(conv);
  if (gc != ((PurpleConnection *)((void *)0))) {
/* Still connected */
    prpl_info = ((PurplePluginProtocolInfo *)( *( *purple_connection_get_prpl(gc)).info).extra_info);
    if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
      if (purple_prefs_get_bool("/purple/conversations/im/send_typing") != 0) 
        serv_send_typing(gc,name,PURPLE_NOT_TYPING);
      if ((gc != 0) && ((prpl_info -> convo_closed) != ((void (*)(PurpleConnection *, const char *))((void *)0)))) 
        ( *(prpl_info -> convo_closed))(gc,name);
    }
    else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) {
      int chat_id = purple_conv_chat_get_id((purple_conversation_get_chat_data(conv)));
#if 0
/*
			 * This is unfortunately necessary, because calling
			 * serv_chat_leave() calls this purple_conversation_destroy(),
			 * which leads to two calls here.. We can't just return after
			 * this, because then it'll return on the next pass. So, since
			 * serv_got_chat_left(), which is eventually called from the
			 * prpl that serv_chat_leave() calls, removes this conversation
			 * from the gc's buddy_chats list, we're going to check to see
			 * if this exists in the list. If so, we want to return after
			 * calling this, because it'll be called again. If not, fall
			 * through, because it'll have already been removed, and we'd
			 * be on the 2nd pass.
			 *
			 * Long paragraph. <-- Short sentence.
			 *
			 *   -- ChipX86
			 */
#endif
/*
			 * Instead of all of that, lets just close the window when
			 * the user tells us to, and let the prpl deal with the
			 * internals on it's own time. Don't do this if the prpl already
			 * knows it left the chat.
			 */
      if (!(purple_conv_chat_has_left(purple_conversation_get_chat_data(conv)) != 0)) 
        serv_chat_leave(gc,chat_id);
/*
			 * If they didn't call serv_got_chat_left by now, it's too late.
			 * So we better do it for them before we destroy the thing.
			 */
      if (!(purple_conv_chat_has_left(purple_conversation_get_chat_data(conv)) != 0)) 
        serv_got_chat_left(gc,chat_id);
    }
  }
/* remove from conversations and im/chats lists prior to emit */
  conversations = g_list_remove(conversations,conv);
  if ((conv -> type) == PURPLE_CONV_TYPE_IM) 
    ims = g_list_remove(ims,conv);
  else if ((conv -> type) == PURPLE_CONV_TYPE_CHAT) 
    chats = g_list_remove(chats,conv);
  hc.name = ((gchar *)(purple_normalize((conv -> account),(conv -> name))));
  hc.account = (conv -> account);
  hc.type = (conv -> type);
  g_hash_table_remove(conversation_cache,(&hc));
  purple_signal_emit(purple_conversations_get_handle(),"deleting-conversation",conv);
  g_free((conv -> name));
  g_free((conv -> title));
  conv -> name = ((char *)((void *)0));
  conv -> title = ((char *)((void *)0));
  if ((conv -> type) == PURPLE_CONV_TYPE_IM) {
    purple_conv_im_stop_typing_timeout(conv -> u.im);
    purple_conv_im_stop_send_typed_timeout(conv -> u.im);
    purple_buddy_icon_unref(( *conv -> u.im).icon);
    ( *conv -> u.im).icon = ((PurpleBuddyIcon *)((void *)0));
    purple_dbus_unregister_pointer(conv -> u.im);
    g_free(conv -> u.im);
    conv -> u.im = ((PurpleConvIm *)((void *)0));
  }
  else if ((conv -> type) == PURPLE_CONV_TYPE_CHAT) {
    g_hash_table_destroy(( *conv -> u.chat).users);
    ( *conv -> u.chat).users = ((GHashTable *)((void *)0));
    g_list_foreach(( *conv -> u.chat).in_room,((GFunc )purple_conv_chat_cb_destroy),0);
    g_list_free(( *conv -> u.chat).in_room);
    g_list_foreach(( *conv -> u.chat).ignored,((GFunc )g_free),0);
    g_list_free(( *conv -> u.chat).ignored);
    ( *conv -> u.chat).in_room = ((GList *)((void *)0));
    ( *conv -> u.chat).ignored = ((GList *)((void *)0));
    g_free(( *conv -> u.chat).who);
    ( *conv -> u.chat).who = ((char *)((void *)0));
    g_free(( *conv -> u.chat).topic);
    ( *conv -> u.chat).topic = ((char *)((void *)0));
    g_free(( *conv -> u.chat).nick);
    purple_dbus_unregister_pointer(conv -> u.chat);
    g_free(conv -> u.chat);
    conv -> u.chat = ((PurpleConvChat *)((void *)0));
  }
  g_hash_table_destroy((conv -> data));
  conv -> data = ((GHashTable *)((void *)0));
  if ((ops != ((PurpleConversationUiOps *)((void *)0))) && ((ops -> destroy_conversation) != ((void (*)(PurpleConversation *))((void *)0)))) 
    ( *(ops -> destroy_conversation))(conv);
  conv -> ui_data = ((void *)((void *)0));
  purple_conversation_close_logs(conv);
  purple_conversation_clear_message_history(conv);
  purple_dbus_unregister_pointer(conv);
  g_free(conv);
  conv = ((PurpleConversation *)((void *)0));
}

void purple_conversation_present(PurpleConversation *conv)
{
  PurpleConversationUiOps *ops;
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  ops = purple_conversation_get_ui_ops(conv);
  if ((ops != 0) && ((ops -> present) != 0)) 
    ( *(ops -> present))(conv);
}

void purple_conversation_set_features(PurpleConversation *conv,PurpleConnectionFlags features)
{
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  conv -> features = features;
  purple_conversation_update(conv,PURPLE_CONV_UPDATE_FEATURES);
}

PurpleConnectionFlags purple_conversation_get_features(PurpleConversation *conv)
{
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return 0;
    };
  }while (0);
  return conv -> features;
}

PurpleConversationType purple_conversation_get_type(const PurpleConversation *conv)
{
  do {
    if (conv != ((const PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return PURPLE_CONV_TYPE_UNKNOWN;
    };
  }while (0);
  return conv -> type;
}

void purple_conversation_set_ui_ops(PurpleConversation *conv,PurpleConversationUiOps *ops)
{
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  if ((conv -> ui_ops) == ops) 
    return ;
  if (((conv -> ui_ops) != ((PurpleConversationUiOps *)((void *)0))) && (( *(conv -> ui_ops)).destroy_conversation != ((void (*)(PurpleConversation *))((void *)0)))) 
    ( *( *(conv -> ui_ops)).destroy_conversation)(conv);
  conv -> ui_data = ((void *)((void *)0));
  conv -> ui_ops = ops;
}

PurpleConversationUiOps *purple_conversation_get_ui_ops(const PurpleConversation *conv)
{
  do {
    if (conv != ((const PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return 0;
    };
  }while (0);
  return conv -> ui_ops;
}

void purple_conversation_set_account(PurpleConversation *conv,PurpleAccount *account)
{
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  if (account == purple_conversation_get_account(conv)) 
    return ;
  conv -> account = account;
  purple_conversation_update(conv,PURPLE_CONV_UPDATE_ACCOUNT);
}

PurpleAccount *purple_conversation_get_account(const PurpleConversation *conv)
{
  do {
    if (conv != ((const PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return 0;
    };
  }while (0);
  return conv -> account;
}

PurpleConnection *purple_conversation_get_gc(const PurpleConversation *conv)
{
  PurpleAccount *account;
  do {
    if (conv != ((const PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return 0;
    };
  }while (0);
  account = purple_conversation_get_account(conv);
  if (account == ((PurpleAccount *)((void *)0))) 
    return 0;
  return account -> gc;
}

void purple_conversation_set_title(PurpleConversation *conv,const char *title)
{
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  do {
    if (title != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"title != NULL");
      return ;
    };
  }while (0);
  g_free((conv -> title));
  conv -> title = g_strdup(title);
  purple_conversation_update(conv,PURPLE_CONV_UPDATE_TITLE);
}

const char *purple_conversation_get_title(const PurpleConversation *conv)
{
  do {
    if (conv != ((const PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return 0;
    };
  }while (0);
  return (conv -> title);
}

void purple_conversation_autoset_title(PurpleConversation *conv)
{
  PurpleAccount *account;
  PurpleBuddy *b;
  PurpleChat *chat;
  const char *text = (const char *)((void *)0);
  const char *name;
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  account = purple_conversation_get_account(conv);
  name = purple_conversation_get_name(conv);
  if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) {
    if ((account != 0) && ((b = purple_find_buddy(account,name)) != ((PurpleBuddy *)((void *)0)))) 
      text = purple_buddy_get_contact_alias(b);
  }
  else if ((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) {
    if ((account != 0) && ((chat = purple_blist_find_chat(account,name)) != ((PurpleChat *)((void *)0)))) 
      text = purple_chat_get_name(chat);
  }
  if (text == ((const char *)((void *)0))) 
    text = name;
  purple_conversation_set_title(conv,text);
}

void purple_conversation_foreach(void (*func)(PurpleConversation *))
{
  PurpleConversation *conv;
  GList *l;
  do {
    if (func != ((void (*)(PurpleConversation *))((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"func != NULL");
      return ;
    };
  }while (0);
  for (l = purple_get_conversations(); l != ((GList *)((void *)0)); l = (l -> next)) {
    conv = ((PurpleConversation *)(l -> data));
    ( *func)(conv);
  }
}

void purple_conversation_set_name(PurpleConversation *conv,const char *name)
{
  struct _purple_hconv *hc;
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  hc = ((struct _purple_hconv *)(g_malloc_n(1,(sizeof(struct _purple_hconv )))));
  hc -> type = (conv -> type);
  hc -> account = (conv -> account);
  hc -> name = ((gchar *)(purple_normalize((conv -> account),(conv -> name))));
  g_hash_table_remove(conversation_cache,hc);
  g_free((conv -> name));
  conv -> name = g_strdup(name);
  hc -> name = g_strdup(purple_normalize((conv -> account),(conv -> name)));
  g_hash_table_insert(conversation_cache,hc,conv);
  purple_conversation_autoset_title(conv);
}

const char *purple_conversation_get_name(const PurpleConversation *conv)
{
  do {
    if (conv != ((const PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return 0;
    };
  }while (0);
  return (conv -> name);
}

void purple_conversation_set_logging(PurpleConversation *conv,gboolean log)
{
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  if ((conv -> logging) != log) {
    conv -> logging = log;
    purple_conversation_update(conv,PURPLE_CONV_UPDATE_LOGGING);
  }
}

gboolean purple_conversation_is_logging(const PurpleConversation *conv)
{
  do {
    if (conv != ((const PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return 0;
    };
  }while (0);
  return conv -> logging;
}

void purple_conversation_close_logs(PurpleConversation *conv)
{
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  g_list_foreach((conv -> logs),((GFunc )purple_log_free),0);
  g_list_free((conv -> logs));
  conv -> logs = ((GList *)((void *)0));
}

PurpleConvIm *purple_conversation_get_im_data(const PurpleConversation *conv)
{
  do {
    if (conv != ((const PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return 0;
    };
  }while (0);
  if ((purple_conversation_get_type(conv)) != PURPLE_CONV_TYPE_IM) 
    return 0;
  return conv -> u.im;
}

PurpleConvChat *purple_conversation_get_chat_data(const PurpleConversation *conv)
{
  do {
    if (conv != ((const PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return 0;
    };
  }while (0);
  if ((purple_conversation_get_type(conv)) != PURPLE_CONV_TYPE_CHAT) 
    return 0;
  return conv -> u.chat;
}

void purple_conversation_set_data(PurpleConversation *conv,const char *key,gpointer data)
{
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  do {
    if (key != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"key != NULL");
      return ;
    };
  }while (0);
  g_hash_table_replace((conv -> data),(g_strdup(key)),data);
}

gpointer purple_conversation_get_data(PurpleConversation *conv,const char *key)
{
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return 0;
    };
  }while (0);
  do {
    if (key != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"key != NULL");
      return 0;
    };
  }while (0);
  return g_hash_table_lookup((conv -> data),key);
}

GList *purple_get_conversations()
{
  return conversations;
}

GList *purple_get_ims()
{
  return ims;
}

GList *purple_get_chats()
{
  return chats;
}

PurpleConversation *purple_find_conversation_with_account(PurpleConversationType type,const char *name,const PurpleAccount *account)
{
  PurpleConversation *c = (PurpleConversation *)((void *)0);
  struct _purple_hconv hc;
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return 0;
    };
  }while (0);
  hc.name = ((gchar *)(purple_normalize(account,name)));
  hc.account = account;
  hc.type = type;
  switch(type){
    case PURPLE_CONV_TYPE_IM:
{
    }
    case PURPLE_CONV_TYPE_CHAT:
{
      c = (g_hash_table_lookup(conversation_cache,(&hc)));
      break; 
    }
    case PURPLE_CONV_TYPE_ANY:
{
      hc.type = PURPLE_CONV_TYPE_IM;
      c = (g_hash_table_lookup(conversation_cache,(&hc)));
      if (!(c != 0)) {
        hc.type = PURPLE_CONV_TYPE_CHAT;
        c = (g_hash_table_lookup(conversation_cache,(&hc)));
      }
      break; 
    }
    default:
{
      do {
        g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","conversation.c",921,((const char *)__func__));
        return 0;
      }while (0);
    }
  }
  return c;
}

void purple_conversation_write(PurpleConversation *conv,const char *who,const char *message,PurpleMessageFlags flags,time_t mtime)
{
  PurplePluginProtocolInfo *prpl_info = (PurplePluginProtocolInfo *)((void *)0);
  PurpleConnection *gc = (PurpleConnection *)((void *)0);
  PurpleAccount *account;
  PurpleConversationUiOps *ops;
  const char *alias;
  char *displayed = (char *)((void *)0);
  PurpleBuddy *b;
  int plugin_return;
  PurpleConversationType type;
/* int logging_font_options = 0; */
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  do {
    if (message != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"message != NULL");
      return ;
    };
  }while (0);
  ops = purple_conversation_get_ui_ops(conv);
  account = purple_conversation_get_account(conv);
  type = purple_conversation_get_type(conv);
  if (account != ((PurpleAccount *)((void *)0))) 
    gc = purple_account_get_connection(account);
  if (((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_CHAT) && ((gc != ((PurpleConnection *)((void *)0))) && !(g_slist_find((gc -> buddy_chats),conv) != 0))) 
    return ;
  if (((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) && !(g_list_find(purple_get_conversations(),conv) != 0)) 
    return ;
  displayed = g_strdup(message);
  if ((who == ((const char *)((void *)0))) || (( *who) == 0)) 
    who = purple_conversation_get_name(conv);
  alias = who;
  plugin_return = ((gint )((glong )(purple_signal_emit_return_1(purple_conversations_get_handle(),(((type == PURPLE_CONV_TYPE_IM)?"writing-im-msg" : "writing-chat-msg")),account,who,&displayed,conv,flags))));
  if (displayed == ((char *)((void *)0))) 
    return ;
  if (plugin_return != 0) {
    g_free(displayed);
    return ;
  }
  if (account != ((PurpleAccount *)((void *)0))) {
    prpl_info = ((PurplePluginProtocolInfo *)( *( *purple_find_prpl(purple_account_get_protocol_id(account))).info).extra_info);
    if (((purple_conversation_get_type(conv)) == PURPLE_CONV_TYPE_IM) || !(((prpl_info -> options) & OPT_PROTO_UNIQUE_CHATNAME) != 0U)) {
      if ((flags & PURPLE_MESSAGE_SEND) != 0U) {
        b = purple_find_buddy(account,purple_account_get_username(account));
        if (purple_account_get_alias(account) != ((const char *)((void *)0))) 
          alias = (account -> alias);
        else if ((b != ((PurpleBuddy *)((void *)0))) && !(purple_strequal(purple_buddy_get_name(b),purple_buddy_get_contact_alias(b)) != 0)) 
          alias = purple_buddy_get_contact_alias(b);
        else if (purple_connection_get_display_name(gc) != ((const char *)((void *)0))) 
          alias = purple_connection_get_display_name(gc);
        else 
          alias = purple_account_get_username(account);
      }
      else {
        b = purple_find_buddy(account,who);
        if (b != ((PurpleBuddy *)((void *)0))) 
          alias = purple_buddy_get_contact_alias(b);
      }
    }
  }
  if (!((flags & PURPLE_MESSAGE_NO_LOG) != 0U) && (purple_conversation_is_logging(conv) != 0)) {
    GList *log;
    if ((conv -> logs) == ((GList *)((void *)0))) 
      open_log(conv);
    log = (conv -> logs);
    while(log != ((GList *)((void *)0))){
      purple_log_write(((PurpleLog *)(log -> data)),flags,alias,mtime,displayed);
      log = (log -> next);
    }
  }
  if ((ops != 0) && ((ops -> write_conv) != 0)) 
    ( *(ops -> write_conv))(conv,who,alias,displayed,flags,mtime);
  add_message_to_history(conv,who,alias,message,flags,mtime);
  purple_signal_emit(purple_conversations_get_handle(),((type == PURPLE_CONV_TYPE_IM)?"wrote-im-msg" : "wrote-chat-msg"),account,who,displayed,conv,flags);
  g_free(displayed);
}

gboolean purple_conversation_has_focus(PurpleConversation *conv)
{
  gboolean ret = 0;
  PurpleConversationUiOps *ops;
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return 0;
    };
  }while (0);
  ops = purple_conversation_get_ui_ops(conv);
  if ((ops != ((PurpleConversationUiOps *)((void *)0))) && ((ops -> has_focus) != ((gboolean (*)(PurpleConversation *))((void *)0)))) 
    ret = ( *(ops -> has_focus))(conv);
  return ret;
}
/*
 * TODO: Need to make sure calls to this function happen in the core
 * instead of the UI.  That way UIs have less work to do, and the
 * core/UI split is cleaner.  Also need to make sure this is called
 * when chats are added/removed from the blist.
 */

void purple_conversation_update(PurpleConversation *conv,PurpleConvUpdateType type)
{
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  purple_signal_emit(purple_conversations_get_handle(),"conversation-updated",conv,type);
}
/**************************************************************************
 * IM Conversation API
 **************************************************************************/

PurpleConversation *purple_conv_im_get_conversation(const PurpleConvIm *im)
{
  do {
    if (im != ((const PurpleConvIm *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"im != NULL");
      return 0;
    };
  }while (0);
  return im -> conv;
}

void purple_conv_im_set_icon(PurpleConvIm *im,PurpleBuddyIcon *icon)
{
  do {
    if (im != ((PurpleConvIm *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"im != NULL");
      return ;
    };
  }while (0);
  if ((im -> icon) != icon) {
    purple_buddy_icon_unref((im -> icon));
    im -> icon = ((icon == ((PurpleBuddyIcon *)((void *)0)))?((struct _PurpleBuddyIcon *)((void *)0)) : purple_buddy_icon_ref(icon));
  }
  purple_conversation_update(purple_conv_im_get_conversation(im),PURPLE_CONV_UPDATE_ICON);
}

PurpleBuddyIcon *purple_conv_im_get_icon(const PurpleConvIm *im)
{
  do {
    if (im != ((const PurpleConvIm *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"im != NULL");
      return 0;
    };
  }while (0);
  return im -> icon;
}

void purple_conv_im_set_typing_state(PurpleConvIm *im,PurpleTypingState state)
{
  do {
    if (im != ((PurpleConvIm *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"im != NULL");
      return ;
    };
  }while (0);
  if ((im -> typing_state) != state) {
    im -> typing_state = state;
    switch(state){
      case PURPLE_TYPING:
{
        purple_signal_emit(purple_conversations_get_handle(),"buddy-typing",( *(im -> conv)).account,( *(im -> conv)).name);
        break; 
      }
      case PURPLE_TYPED:
{
        purple_signal_emit(purple_conversations_get_handle(),"buddy-typed",( *(im -> conv)).account,( *(im -> conv)).name);
        break; 
      }
      case PURPLE_NOT_TYPING:
{
        purple_signal_emit(purple_conversations_get_handle(),"buddy-typing-stopped",( *(im -> conv)).account,( *(im -> conv)).name);
        break; 
      }
    }
    purple_conv_im_update_typing(im);
  }
}

PurpleTypingState purple_conv_im_get_typing_state(const PurpleConvIm *im)
{
  do {
    if (im != ((const PurpleConvIm *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"im != NULL");
      return 0;
    };
  }while (0);
  return im -> typing_state;
}

void purple_conv_im_start_typing_timeout(PurpleConvIm *im,int timeout)
{
  PurpleConversation *conv;
  do {
    if (im != ((PurpleConvIm *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"im != NULL");
      return ;
    };
  }while (0);
  if ((im -> typing_timeout) > 0) 
    purple_conv_im_stop_typing_timeout(im);
  conv = purple_conv_im_get_conversation(im);
  im -> typing_timeout = purple_timeout_add_seconds(timeout,reset_typing_cb,conv);
}

void purple_conv_im_stop_typing_timeout(PurpleConvIm *im)
{
  do {
    if (im != ((PurpleConvIm *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"im != NULL");
      return ;
    };
  }while (0);
  if ((im -> typing_timeout) == 0) 
    return ;
  purple_timeout_remove((im -> typing_timeout));
  im -> typing_timeout = 0;
}

guint purple_conv_im_get_typing_timeout(const PurpleConvIm *im)
{
  do {
    if (im != ((const PurpleConvIm *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"im != NULL");
      return 0;
    };
  }while (0);
  return im -> typing_timeout;
}

void purple_conv_im_set_type_again(PurpleConvIm *im,unsigned int val)
{
  do {
    if (im != ((PurpleConvIm *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"im != NULL");
      return ;
    };
  }while (0);
  if (val == 0) 
    im -> type_again = 0;
  else 
    im -> type_again = (time(0) + val);
}

time_t purple_conv_im_get_type_again(const PurpleConvIm *im)
{
  do {
    if (im != ((const PurpleConvIm *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"im != NULL");
      return 0;
    };
  }while (0);
  return im -> type_again;
}

void purple_conv_im_start_send_typed_timeout(PurpleConvIm *im)
{
  do {
    if (im != ((PurpleConvIm *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"im != NULL");
      return ;
    };
  }while (0);
  im -> send_typed_timeout = purple_timeout_add_seconds(5,send_typed_cb,(purple_conv_im_get_conversation(im)));
}

void purple_conv_im_stop_send_typed_timeout(PurpleConvIm *im)
{
  do {
    if (im != ((PurpleConvIm *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"im != NULL");
      return ;
    };
  }while (0);
  if ((im -> send_typed_timeout) == 0) 
    return ;
  purple_timeout_remove((im -> send_typed_timeout));
  im -> send_typed_timeout = 0;
}

guint purple_conv_im_get_send_typed_timeout(const PurpleConvIm *im)
{
  do {
    if (im != ((const PurpleConvIm *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"im != NULL");
      return 0;
    };
  }while (0);
  return im -> send_typed_timeout;
}

void purple_conv_im_update_typing(PurpleConvIm *im)
{
  do {
    if (im != ((PurpleConvIm *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"im != NULL");
      return ;
    };
  }while (0);
  purple_conversation_update(purple_conv_im_get_conversation(im),PURPLE_CONV_UPDATE_TYPING);
}

void purple_conv_im_write(PurpleConvIm *im,const char *who,const char *message,PurpleMessageFlags flags,time_t mtime)
{
  PurpleConversation *c;
  do {
    if (im != ((PurpleConvIm *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"im != NULL");
      return ;
    };
  }while (0);
  do {
    if (message != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"message != NULL");
      return ;
    };
  }while (0);
  c = purple_conv_im_get_conversation(im);
  if ((flags & PURPLE_MESSAGE_RECV) == PURPLE_MESSAGE_RECV) {
    purple_conv_im_set_typing_state(im,PURPLE_NOT_TYPING);
  }
/* Pass this on to either the ops structure or the default write func. */
  if (((c -> ui_ops) != ((PurpleConversationUiOps *)((void *)0))) && (( *(c -> ui_ops)).write_im != ((void (*)(PurpleConversation *, const char *, const char *, PurpleMessageFlags , time_t ))((void *)0)))) 
    ( *( *(c -> ui_ops)).write_im)(c,who,message,flags,mtime);
  else 
    purple_conversation_write(c,who,message,flags,mtime);
}

gboolean purple_conv_present_error(const char *who,PurpleAccount *account,const char *what)
{
  PurpleConversation *conv;
  do {
    if (who != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"who != NULL");
      return 0;
    };
  }while (0);
  do {
    if (account != ((PurpleAccount *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"account !=NULL");
      return 0;
    };
  }while (0);
  do {
    if (what != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"what != NULL");
      return 0;
    };
  }while (0);
  conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_ANY,who,account);
  if (conv != ((PurpleConversation *)((void *)0))) 
    purple_conversation_write(conv,0,what,PURPLE_MESSAGE_ERROR,time(0));
  else 
    return 0;
  return (!0);
}

void purple_conv_im_send(PurpleConvIm *im,const char *message)
{
  purple_conv_im_send_with_flags(im,message,0);
}

static void purple_conv_send_confirm_cb(gpointer *data)
{
  PurpleConversation *conv = data[0];
  char *message = data[1];
  g_free(data);
  common_send(conv,message,0);
}

void purple_conv_send_confirm(PurpleConversation *conv,const char *message)
{
  char *text;
  gpointer *data;
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  do {
    if (message != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"message != NULL");
      return ;
    };
  }while (0);
  if (((conv -> ui_ops) != ((PurpleConversationUiOps *)((void *)0))) && (( *(conv -> ui_ops)).send_confirm != ((void (*)(PurpleConversation *, const char *))((void *)0)))) {
    ( *( *(conv -> ui_ops)).send_confirm)(conv,message);
    return ;
  }
  text = g_strdup_printf("You are about to send the following message:\n%s",message);
  data = ((gpointer *)(g_malloc0_n(2,(sizeof(gpointer )))));
  data[0] = conv;
  data[1] = ((gpointer )message);
  purple_request_action(conv,0,((const char *)(dgettext("pidgin","Send Message"))),text,0,purple_conversation_get_account(conv),0,conv,data,2,((const char *)(dgettext("pidgin","_Send Message"))),((GCallback )purple_conv_send_confirm_cb),((const char *)(dgettext("pidgin","Cancel"))),((void *)((void *)0)));
}

void purple_conv_im_send_with_flags(PurpleConvIm *im,const char *message,PurpleMessageFlags flags)
{
  do {
    if (im != ((PurpleConvIm *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"im != NULL");
      return ;
    };
  }while (0);
  do {
    if (message != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"message != NULL");
      return ;
    };
  }while (0);
  common_send(purple_conv_im_get_conversation(im),message,flags);
}

gboolean purple_conv_custom_smiley_add(PurpleConversation *conv,const char *smile,const char *cksum_type,const char *chksum,gboolean remote)
{
  if (((conv == ((PurpleConversation *)((void *)0))) || (smile == ((const char *)((void *)0)))) || !(( *smile) != 0)) {
    return 0;
  }
/* TODO: check if the icon is in the cache and return false if so */
/* TODO: add an icon cache (that doesn't suck) */
  if (((conv -> ui_ops) != ((PurpleConversationUiOps *)((void *)0))) && (( *(conv -> ui_ops)).custom_smiley_add != ((gboolean (*)(PurpleConversation *, const char *, gboolean ))((void *)0)))) {
    return ( *( *(conv -> ui_ops)).custom_smiley_add)(conv,smile,remote);
  }
  else {
    purple_debug_info("conversation","Could not find add custom smiley function");
    return 0;
  }
}

void purple_conv_custom_smiley_write(PurpleConversation *conv,const char *smile,const guchar *data,gsize size)
{
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  do {
    if ((smile != ((const char *)((void *)0))) && (( *smile) != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"smile != NULL && *smile");
      return ;
    };
  }while (0);
  if (((conv -> ui_ops) != ((PurpleConversationUiOps *)((void *)0))) && (( *(conv -> ui_ops)).custom_smiley_write != ((void (*)(PurpleConversation *, const char *, const guchar *, gsize ))((void *)0)))) 
    ( *( *(conv -> ui_ops)).custom_smiley_write)(conv,smile,data,size);
  else 
    purple_debug_info("conversation","Could not find the smiley write function");
}

void purple_conv_custom_smiley_close(PurpleConversation *conv,const char *smile)
{
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return ;
    };
  }while (0);
  do {
    if ((smile != ((const char *)((void *)0))) && (( *smile) != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"smile != NULL && *smile");
      return ;
    };
  }while (0);
  if (((conv -> ui_ops) != ((PurpleConversationUiOps *)((void *)0))) && (( *(conv -> ui_ops)).custom_smiley_close != ((void (*)(PurpleConversation *, const char *))((void *)0)))) 
    ( *( *(conv -> ui_ops)).custom_smiley_close)(conv,smile);
  else 
    purple_debug_info("conversation","Could not find custom smiley close function");
}
/**************************************************************************
 * Chat Conversation API
 **************************************************************************/

PurpleConversation *purple_conv_chat_get_conversation(const PurpleConvChat *chat)
{
  do {
    if (chat != ((const PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return 0;
    };
  }while (0);
  return chat -> conv;
}

GList *purple_conv_chat_set_users(PurpleConvChat *chat,GList *users)
{
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return 0;
    };
  }while (0);
  chat -> in_room = users;
  return users;
}

GList *purple_conv_chat_get_users(const PurpleConvChat *chat)
{
  do {
    if (chat != ((const PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return 0;
    };
  }while (0);
  return chat -> in_room;
}

void purple_conv_chat_ignore(PurpleConvChat *chat,const char *name)
{
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return ;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return ;
    };
  }while (0);
/* Make sure the user isn't already ignored. */
  if (purple_conv_chat_is_user_ignored(chat,name) != 0) 
    return ;
  purple_conv_chat_set_ignored(chat,g_list_append((chat -> ignored),(g_strdup(name))));
}

void purple_conv_chat_unignore(PurpleConvChat *chat,const char *name)
{
  GList *item;
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return ;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return ;
    };
  }while (0);
/* Make sure the user is actually ignored. */
  if (!(purple_conv_chat_is_user_ignored(chat,name) != 0)) 
    return ;
  item = g_list_find(purple_conv_chat_get_ignored(chat),(purple_conv_chat_get_ignored_user(chat,name)));
  purple_conv_chat_set_ignored(chat,g_list_remove_link((chat -> ignored),item));
  g_free((item -> data));
  g_list_free_1(item);
}

GList *purple_conv_chat_set_ignored(PurpleConvChat *chat,GList *ignored)
{
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return 0;
    };
  }while (0);
  chat -> ignored = ignored;
  return ignored;
}

GList *purple_conv_chat_get_ignored(const PurpleConvChat *chat)
{
  do {
    if (chat != ((const PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return 0;
    };
  }while (0);
  return chat -> ignored;
}

const char *purple_conv_chat_get_ignored_user(const PurpleConvChat *chat,const char *user)
{
  GList *ignored;
  do {
    if (chat != ((const PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return 0;
    };
  }while (0);
  do {
    if (user != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return 0;
    };
  }while (0);
  for (ignored = purple_conv_chat_get_ignored(chat); ignored != ((GList *)((void *)0)); ignored = (ignored -> next)) {
    const char *ign = (const char *)(ignored -> data);
    if (!(purple_utf8_strcasecmp(user,ign) != 0) || (((( *ign) == '+') || (( *ign) == '%')) && !(purple_utf8_strcasecmp(user,(ign + 1)) != 0))) 
      return ign;
    if (( *ign) == 64) {
      ign++;
      if (((( *ign) == '+') && !(purple_utf8_strcasecmp(user,(ign + 1)) != 0)) || ((( *ign) != '+') && !(purple_utf8_strcasecmp(user,ign) != 0))) 
        return ign;
    }
  }
  return 0;
}

gboolean purple_conv_chat_is_user_ignored(const PurpleConvChat *chat,const char *user)
{
  do {
    if (chat != ((const PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return 0;
    };
  }while (0);
  do {
    if (user != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return 0;
    };
  }while (0);
  return purple_conv_chat_get_ignored_user(chat,user) != ((const char *)((void *)0));
}

void purple_conv_chat_set_topic(PurpleConvChat *chat,const char *who,const char *topic)
{
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return ;
    };
  }while (0);
  g_free((chat -> who));
  g_free((chat -> topic));
  chat -> who = g_strdup(who);
  chat -> topic = g_strdup(topic);
  purple_conversation_update(purple_conv_chat_get_conversation(chat),PURPLE_CONV_UPDATE_TOPIC);
  purple_signal_emit(purple_conversations_get_handle(),"chat-topic-changed",(chat -> conv),(chat -> who),(chat -> topic));
}

const char *purple_conv_chat_get_topic(const PurpleConvChat *chat)
{
  do {
    if (chat != ((const PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return 0;
    };
  }while (0);
  return (chat -> topic);
}

void purple_conv_chat_set_id(PurpleConvChat *chat,int id)
{
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return ;
    };
  }while (0);
  chat -> id = id;
}

int purple_conv_chat_get_id(const PurpleConvChat *chat)
{
  do {
    if (chat != ((const PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return -1;
    };
  }while (0);
  return chat -> id;
}

void purple_conv_chat_write(PurpleConvChat *chat,const char *who,const char *message,PurpleMessageFlags flags,time_t mtime)
{
  PurpleAccount *account;
  PurpleConversation *conv;
  PurpleConnection *gc;
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return ;
    };
  }while (0);
  do {
    if (who != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"who != NULL");
      return ;
    };
  }while (0);
  do {
    if (message != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"message != NULL");
      return ;
    };
  }while (0);
  conv = purple_conv_chat_get_conversation(chat);
  gc = purple_conversation_get_gc(conv);
  account = purple_connection_get_account(gc);
/* Don't display this if the person who wrote it is ignored. */
  if (purple_conv_chat_is_user_ignored(chat,who) != 0) 
    return ;
  if (mtime < 0) {
    purple_debug_error("conversation","purple_conv_chat_write ignoring negative timestamp\n");
/* TODO: Would be more appropriate to use a value that indicates
		   that the timestamp is unknown, and surface that in the UI. */
    mtime = time(0);
  }
  if (!((flags & PURPLE_MESSAGE_WHISPER) != 0U)) {
    const char *str;
    str = purple_normalize(account,who);
    if (purple_strequal(str,(chat -> nick)) != 0) {
      flags |= PURPLE_MESSAGE_SEND;
    }
    else {
      flags |= PURPLE_MESSAGE_RECV;
      if (purple_utf8_has_word(message,(chat -> nick)) != 0) 
        flags |= PURPLE_MESSAGE_NICK;
    }
  }
/* Pass this on to either the ops structure or the default write func. */
  if (((conv -> ui_ops) != ((PurpleConversationUiOps *)((void *)0))) && (( *(conv -> ui_ops)).write_chat != ((void (*)(PurpleConversation *, const char *, const char *, PurpleMessageFlags , time_t ))((void *)0)))) 
    ( *( *(conv -> ui_ops)).write_chat)(conv,who,message,flags,mtime);
  else 
    purple_conversation_write(conv,who,message,flags,mtime);
}

void purple_conv_chat_send(PurpleConvChat *chat,const char *message)
{
  purple_conv_chat_send_with_flags(chat,message,0);
}

void purple_conv_chat_send_with_flags(PurpleConvChat *chat,const char *message,PurpleMessageFlags flags)
{
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return ;
    };
  }while (0);
  do {
    if (message != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"message != NULL");
      return ;
    };
  }while (0);
  common_send(purple_conv_chat_get_conversation(chat),message,flags);
}

void purple_conv_chat_add_user(PurpleConvChat *chat,const char *user,const char *extra_msg,PurpleConvChatBuddyFlags flags,gboolean new_arrival)
{
  GList *users = g_list_append(0,((char *)user));
  GList *extra_msgs = g_list_append(0,((char *)extra_msg));
  GList *flags2 = g_list_append(0,((gpointer )((glong )flags)));
  purple_conv_chat_add_users(chat,users,extra_msgs,flags2,new_arrival);
  g_list_free(users);
  g_list_free(extra_msgs);
  g_list_free(flags2);
}

static int purple_conv_chat_cb_compare(PurpleConvChatBuddy *a,PurpleConvChatBuddy *b)
{
  PurpleConvChatBuddyFlags f1 = 0;
  PurpleConvChatBuddyFlags f2 = 0;
  char *user1 = (char *)((void *)0);
  char *user2 = (char *)((void *)0);
  gint ret = 0;
  if (a != 0) {
    f1 = (a -> flags);
    if ((a -> alias_key) != 0) 
      user1 = (a -> alias_key);
    else if ((a -> name) != 0) 
      user1 = (a -> name);
  }
  if (b != 0) {
    f2 = (b -> flags);
    if ((b -> alias_key) != 0) 
      user2 = (b -> alias_key);
    else if ((b -> name) != 0) 
      user2 = (b -> name);
  }
  if ((user1 == ((char *)((void *)0))) || (user2 == ((char *)((void *)0)))) {
    if (!((user1 == ((char *)((void *)0))) && (user2 == ((char *)((void *)0))))) 
      ret = ((user1 == ((char *)((void *)0)))?-1 : 1);
  }
  else if (f1 != f2) {
/* sort more important users first */
    ret = ((f1 > f2)?-1 : 1);
  }
  else if ((a -> buddy) != (b -> buddy)) {
    ret = (((a -> buddy) != 0)?-1 : 1);
  }
  else {
    ret = purple_utf8_strcasecmp(user1,user2);
  }
  return ret;
}

void purple_conv_chat_add_users(PurpleConvChat *chat,GList *users,GList *extra_msgs,GList *flags,gboolean new_arrivals)
{
  PurpleConversation *conv;
  PurpleConversationUiOps *ops;
  PurpleConvChatBuddy *cbuddy;
  PurpleConnection *gc;
  PurplePluginProtocolInfo *prpl_info;
  GList *ul;
  GList *fl;
  GList *cbuddies = (GList *)((void *)0);
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return ;
    };
  }while (0);
  do {
    if (users != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"users != NULL");
      return ;
    };
  }while (0);
  conv = purple_conv_chat_get_conversation(chat);
  ops = purple_conversation_get_ui_ops(conv);
  gc = purple_conversation_get_gc(conv);
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  prpl_info = ((PurplePluginProtocolInfo *)( *( *purple_connection_get_prpl(gc)).info).extra_info);
  do {
    if (prpl_info != ((PurplePluginProtocolInfo *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"prpl_info != NULL");
      return ;
    };
  }while (0);
  ul = users;
  fl = flags;
  while((ul != ((GList *)((void *)0))) && (fl != ((GList *)((void *)0)))){
    const char *user = (const char *)(ul -> data);
    const char *alias = user;
    gboolean quiet;
    PurpleConvChatBuddyFlags flag = ((gint )((glong )(fl -> data)));
    const char *extra_msg = ((extra_msgs != 0)?(extra_msgs -> data) : ((void *)((void *)0)));
    if (!(((prpl_info -> options) & OPT_PROTO_UNIQUE_CHATNAME) != 0U)) {
      if (purple_strequal((chat -> nick),purple_normalize((conv -> account),user)) != 0) {
        const char *alias2 = purple_account_get_alias((conv -> account));
        if (alias2 != ((const char *)((void *)0))) 
          alias = alias2;
        else {
          const char *display_name = purple_connection_get_display_name(gc);
          if (display_name != ((const char *)((void *)0))) 
            alias = display_name;
        }
      }
      else {
        PurpleBuddy *buddy;
        if ((buddy = purple_find_buddy((gc -> account),user)) != ((PurpleBuddy *)((void *)0))) 
          alias = purple_buddy_get_contact_alias(buddy);
      }
    }
    quiet = ((((gint )((glong )(purple_signal_emit_return_1(purple_conversations_get_handle(),"chat-buddy-joining",conv,user,flag)))) != 0) || (purple_conv_chat_is_user_ignored(chat,user) != 0));
    cbuddy = purple_conv_chat_cb_new(user,alias,flag);
    cbuddy -> buddy = (purple_find_buddy((conv -> account),user) != ((PurpleBuddy *)((void *)0)));
    chat -> in_room = g_list_prepend((chat -> in_room),cbuddy);
    g_hash_table_replace((chat -> users),(g_strdup((cbuddy -> name))),cbuddy);
    cbuddies = g_list_prepend(cbuddies,cbuddy);
    if (!(quiet != 0) && (new_arrivals != 0)) {
      char *alias_esc = g_markup_escape_text(alias,(-1));
      char *tmp;
      if (extra_msg == ((const char *)((void *)0))) 
        tmp = g_strdup_printf(((const char *)(dgettext("pidgin","%s entered the room."))),alias_esc);
      else {
        char *extra_msg_esc = g_markup_escape_text(extra_msg,(-1));
        tmp = g_strdup_printf(((const char *)(dgettext("pidgin","%s [<I>%s</I>] entered the room."))),alias_esc,extra_msg_esc);
        g_free(extra_msg_esc);
      }
      g_free(alias_esc);
      purple_conversation_write(conv,0,tmp,(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LINKIFY),time(0));
      g_free(tmp);
    }
    purple_signal_emit(purple_conversations_get_handle(),"chat-buddy-joined",conv,user,flag,new_arrivals);
    ul = (ul -> next);
    fl = (fl -> next);
    if (extra_msgs != ((GList *)((void *)0))) 
      extra_msgs = (extra_msgs -> next);
  }
  cbuddies = g_list_sort(cbuddies,((GCompareFunc )purple_conv_chat_cb_compare));
  if ((ops != ((PurpleConversationUiOps *)((void *)0))) && ((ops -> chat_add_users) != ((void (*)(PurpleConversation *, GList *, gboolean ))((void *)0)))) 
    ( *(ops -> chat_add_users))(conv,cbuddies,new_arrivals);
  g_list_free(cbuddies);
}

void purple_conv_chat_rename_user(PurpleConvChat *chat,const char *old_user,const char *new_user)
{
  PurpleConversation *conv;
  PurpleConversationUiOps *ops;
  PurpleConnection *gc;
  PurplePluginProtocolInfo *prpl_info;
  PurpleConvChatBuddy *cb;
  PurpleConvChatBuddyFlags flags;
  const char *new_alias = new_user;
  char tmp[4096UL];
  gboolean is_me = 0;
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return ;
    };
  }while (0);
  do {
    if (old_user != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"old_user != NULL");
      return ;
    };
  }while (0);
  do {
    if (new_user != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"new_user != NULL");
      return ;
    };
  }while (0);
  conv = purple_conv_chat_get_conversation(chat);
  ops = purple_conversation_get_ui_ops(conv);
  gc = purple_conversation_get_gc(conv);
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  prpl_info = ((PurplePluginProtocolInfo *)( *( *purple_connection_get_prpl(gc)).info).extra_info);
  do {
    if (prpl_info != ((PurplePluginProtocolInfo *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"prpl_info != NULL");
      return ;
    };
  }while (0);
  if (purple_strequal((chat -> nick),purple_normalize((conv -> account),old_user)) != 0) {
    const char *alias;
/* Note this for later. */
    is_me = (!0);
    if (!(((prpl_info -> options) & OPT_PROTO_UNIQUE_CHATNAME) != 0U)) {
      alias = purple_account_get_alias((conv -> account));
      if (alias != ((const char *)((void *)0))) 
        new_alias = alias;
      else {
        const char *display_name = purple_connection_get_display_name(gc);
        if (display_name != ((const char *)((void *)0))) 
          alias = display_name;
      }
    }
  }
  else if (!(((prpl_info -> options) & OPT_PROTO_UNIQUE_CHATNAME) != 0U)) {
    PurpleBuddy *buddy;
    if ((buddy = purple_find_buddy((gc -> account),new_user)) != ((PurpleBuddy *)((void *)0))) 
      new_alias = purple_buddy_get_contact_alias(buddy);
  }
  flags = purple_conv_chat_user_get_flags(chat,old_user);
  cb = purple_conv_chat_cb_new(new_user,new_alias,flags);
  cb -> buddy = (purple_find_buddy((conv -> account),new_user) != ((PurpleBuddy *)((void *)0)));
  chat -> in_room = g_list_prepend((chat -> in_room),cb);
  g_hash_table_replace((chat -> users),(g_strdup((cb -> name))),cb);
  if ((ops != ((PurpleConversationUiOps *)((void *)0))) && ((ops -> chat_rename_user) != ((void (*)(PurpleConversation *, const char *, const char *, const char *))((void *)0)))) 
    ( *(ops -> chat_rename_user))(conv,old_user,new_user,new_alias);
  cb = purple_conv_chat_cb_find(chat,old_user);
  if (cb != 0) {
    chat -> in_room = g_list_remove((chat -> in_room),cb);
    g_hash_table_remove((chat -> users),(cb -> name));
    purple_conv_chat_cb_destroy(cb);
  }
  if (purple_conv_chat_is_user_ignored(chat,old_user) != 0) {
    purple_conv_chat_unignore(chat,old_user);
    purple_conv_chat_ignore(chat,new_user);
  }
  else if (purple_conv_chat_is_user_ignored(chat,new_user) != 0) 
    purple_conv_chat_unignore(chat,new_user);
  if (is_me != 0) 
    purple_conv_chat_set_nick(chat,new_user);
  if ((purple_prefs_get_bool("/purple/conversations/chat/show_nick_change") != 0) && !(purple_conv_chat_is_user_ignored(chat,new_user) != 0)) {
    if (is_me != 0) {
      char *escaped = g_markup_escape_text(new_user,(-1));
      g_snprintf(tmp,(sizeof(tmp)),((const char *)(dgettext("pidgin","You are now known as %s"))),escaped);
      g_free(escaped);
    }
    else {
      const char *old_alias = old_user;
      const char *new_alias = new_user;
      char *escaped;
      char *escaped2;
      if (!(((prpl_info -> options) & OPT_PROTO_UNIQUE_CHATNAME) != 0U)) {
        PurpleBuddy *buddy;
        if ((buddy = purple_find_buddy((gc -> account),old_user)) != ((PurpleBuddy *)((void *)0))) 
          old_alias = purple_buddy_get_contact_alias(buddy);
        if ((buddy = purple_find_buddy((gc -> account),new_user)) != ((PurpleBuddy *)((void *)0))) 
          new_alias = purple_buddy_get_contact_alias(buddy);
      }
      escaped = g_markup_escape_text(old_alias,(-1));
      escaped2 = g_markup_escape_text(new_alias,(-1));
      g_snprintf(tmp,(sizeof(tmp)),((const char *)(dgettext("pidgin","%s is now known as %s"))),escaped,escaped2);
      g_free(escaped);
      g_free(escaped2);
    }
    purple_conversation_write(conv,0,tmp,(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LINKIFY),time(0));
  }
}

void purple_conv_chat_remove_user(PurpleConvChat *chat,const char *user,const char *reason)
{
  GList *users = g_list_append(0,((char *)user));
  purple_conv_chat_remove_users(chat,users,reason);
  g_list_free(users);
}

void purple_conv_chat_remove_users(PurpleConvChat *chat,GList *users,const char *reason)
{
  PurpleConversation *conv;
  PurpleConnection *gc;
  PurplePluginProtocolInfo *prpl_info;
  PurpleConversationUiOps *ops;
  PurpleConvChatBuddy *cb;
  GList *l;
  gboolean quiet;
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return ;
    };
  }while (0);
  do {
    if (users != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"users != NULL");
      return ;
    };
  }while (0);
  conv = purple_conv_chat_get_conversation(chat);
  gc = purple_conversation_get_gc(conv);
  do {
    if (gc != ((PurpleConnection *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"gc != NULL");
      return ;
    };
  }while (0);
  prpl_info = ((PurplePluginProtocolInfo *)( *( *purple_connection_get_prpl(gc)).info).extra_info);
  do {
    if (prpl_info != ((PurplePluginProtocolInfo *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"prpl_info != NULL");
      return ;
    };
  }while (0);
  ops = purple_conversation_get_ui_ops(conv);
  for (l = users; l != ((GList *)((void *)0)); l = (l -> next)) {
    const char *user = (const char *)(l -> data);
    quiet = (((gint )((glong )(purple_signal_emit_return_1(purple_conversations_get_handle(),"chat-buddy-leaving",conv,user,reason)))) | purple_conv_chat_is_user_ignored(chat,user));
    cb = purple_conv_chat_cb_find(chat,user);
    if (cb != 0) {
      chat -> in_room = g_list_remove((chat -> in_room),cb);
      g_hash_table_remove((chat -> users),(cb -> name));
      purple_conv_chat_cb_destroy(cb);
    }
/* NOTE: Don't remove them from ignored in case they re-enter. */
    if (!(quiet != 0)) {
      const char *alias = user;
      char *alias_esc;
      char *tmp;
      if (!(((prpl_info -> options) & OPT_PROTO_UNIQUE_CHATNAME) != 0U)) {
        PurpleBuddy *buddy;
        if ((buddy = purple_find_buddy((gc -> account),user)) != ((PurpleBuddy *)((void *)0))) 
          alias = purple_buddy_get_contact_alias(buddy);
      }
      alias_esc = g_markup_escape_text(alias,(-1));
      if ((reason == ((const char *)((void *)0))) || !(( *reason) != 0)) 
        tmp = g_strdup_printf(((const char *)(dgettext("pidgin","%s left the room."))),alias_esc);
      else {
        char *reason_esc = g_markup_escape_text(reason,(-1));
        tmp = g_strdup_printf(((const char *)(dgettext("pidgin","%s left the room (%s)."))),alias_esc,reason_esc);
        g_free(reason_esc);
      }
      g_free(alias_esc);
      purple_conversation_write(conv,0,tmp,(PURPLE_MESSAGE_SYSTEM | PURPLE_MESSAGE_NO_LINKIFY),time(0));
      g_free(tmp);
    }
    purple_signal_emit(purple_conversations_get_handle(),"chat-buddy-left",conv,user,reason);
  }
  if ((ops != ((PurpleConversationUiOps *)((void *)0))) && ((ops -> chat_remove_users) != ((void (*)(PurpleConversation *, GList *))((void *)0)))) 
    ( *(ops -> chat_remove_users))(conv,users);
}

void purple_conv_chat_clear_users(PurpleConvChat *chat)
{
  PurpleConversation *conv;
  PurpleConversationUiOps *ops;
  GList *users;
  GList *l;
  GList *names = (GList *)((void *)0);
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return ;
    };
  }while (0);
  conv = purple_conv_chat_get_conversation(chat);
  ops = purple_conversation_get_ui_ops(conv);
  users = (chat -> in_room);
  if ((ops != ((PurpleConversationUiOps *)((void *)0))) && ((ops -> chat_remove_users) != ((void (*)(PurpleConversation *, GList *))((void *)0)))) {
    for (l = users; l != 0; l = (l -> next)) {
      PurpleConvChatBuddy *cb = (l -> data);
      names = g_list_prepend(names,(cb -> name));
    }
    ( *(ops -> chat_remove_users))(conv,names);
    g_list_free(names);
  }
  for (l = users; l != 0; l = (l -> next)) {
    PurpleConvChatBuddy *cb = (l -> data);
    purple_signal_emit(purple_conversations_get_handle(),"chat-buddy-leaving",conv,(cb -> name),((void *)((void *)0)));
    purple_signal_emit(purple_conversations_get_handle(),"chat-buddy-left",conv,(cb -> name),((void *)((void *)0)));
    purple_conv_chat_cb_destroy(cb);
  }
  g_hash_table_remove_all((chat -> users));
  g_list_free(users);
  chat -> in_room = ((GList *)((void *)0));
}

gboolean purple_conv_chat_find_user(PurpleConvChat *chat,const char *user)
{
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return 0;
    };
  }while (0);
  do {
    if (user != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return 0;
    };
  }while (0);
  return purple_conv_chat_cb_find(chat,user) != ((PurpleConvChatBuddy *)((void *)0));
}

void purple_conv_chat_user_set_flags(PurpleConvChat *chat,const char *user,PurpleConvChatBuddyFlags flags)
{
  PurpleConversation *conv;
  PurpleConversationUiOps *ops;
  PurpleConvChatBuddy *cb;
  PurpleConvChatBuddyFlags oldflags;
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return ;
    };
  }while (0);
  do {
    if (user != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return ;
    };
  }while (0);
  cb = purple_conv_chat_cb_find(chat,user);
  if (!(cb != 0)) 
    return ;
  if (flags == (cb -> flags)) 
    return ;
  oldflags = (cb -> flags);
  cb -> flags = flags;
  conv = purple_conv_chat_get_conversation(chat);
  ops = purple_conversation_get_ui_ops(conv);
  if ((ops != ((PurpleConversationUiOps *)((void *)0))) && ((ops -> chat_update_user) != ((void (*)(PurpleConversation *, const char *))((void *)0)))) 
    ( *(ops -> chat_update_user))(conv,user);
  purple_signal_emit(purple_conversations_get_handle(),"chat-buddy-flags",conv,user,oldflags,flags);
}

PurpleConvChatBuddyFlags purple_conv_chat_user_get_flags(PurpleConvChat *chat,const char *user)
{
  PurpleConvChatBuddy *cb;
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return 0;
    };
  }while (0);
  do {
    if (user != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"user != NULL");
      return 0;
    };
  }while (0);
  cb = purple_conv_chat_cb_find(chat,user);
  if (!(cb != 0)) 
    return PURPLE_CBFLAGS_NONE;
  return cb -> flags;
}

void purple_conv_chat_set_nick(PurpleConvChat *chat,const char *nick)
{
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return ;
    };
  }while (0);
  g_free((chat -> nick));
  chat -> nick = g_strdup(purple_normalize(( *(chat -> conv)).account,nick));
}

const char *purple_conv_chat_get_nick(PurpleConvChat *chat)
{
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return 0;
    };
  }while (0);
  return (chat -> nick);
}

PurpleConversation *purple_find_chat(const PurpleConnection *gc,int id)
{
  GList *l;
  PurpleConversation *conv;
  for (l = purple_get_chats(); l != ((GList *)((void *)0)); l = (l -> next)) {
    conv = ((PurpleConversation *)(l -> data));
    if ((purple_conv_chat_get_id((purple_conversation_get_chat_data(conv))) == id) && ((purple_conversation_get_gc(conv)) == gc)) 
      return conv;
  }
  return 0;
}

void purple_conv_chat_left(PurpleConvChat *chat)
{
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return ;
    };
  }while (0);
  chat -> left = (!0);
  purple_conversation_update((chat -> conv),PURPLE_CONV_UPDATE_CHATLEFT);
}

static void invite_user_to_chat(gpointer data,PurpleRequestFields *fields)
{
  PurpleConversation *conv;
  PurpleConvChat *chat;
  const char *user;
  const char *message;
  conv = data;
  chat = purple_conversation_get_chat_data(conv);
  user = purple_request_fields_get_string(fields,"screenname");
  message = purple_request_fields_get_string(fields,"message");
  serv_chat_invite(purple_conversation_get_gc(conv),(chat -> id),message,user);
}

void purple_conv_chat_invite_user(PurpleConvChat *chat,const char *user,const char *message,gboolean confirm)
{
  PurpleAccount *account;
  PurpleConversation *conv;
  PurpleRequestFields *fields;
  PurpleRequestFieldGroup *group;
  PurpleRequestField *field;
  do {
    if (chat != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat");
      return ;
    };
  }while (0);
  if (((!(user != 0) || !(( *user) != 0)) || !(message != 0)) || !(( *message) != 0)) 
    confirm = (!0);
  conv = (chat -> conv);
  account = (conv -> account);
  if (!(confirm != 0)) {
    serv_chat_invite(purple_account_get_connection(account),purple_conv_chat_get_id(chat),message,user);
    return ;
  }
  fields = purple_request_fields_new();
  group = purple_request_field_group_new(((const char *)(dgettext("pidgin","Invite to chat"))));
  purple_request_fields_add_group(fields,group);
  field = purple_request_field_string_new("screenname",((const char *)(dgettext("pidgin","Buddy"))),user,0);
  purple_request_field_group_add_field(group,field);
  purple_request_field_set_required(field,(!0));
  purple_request_field_set_type_hint(field,"screenname");
  field = purple_request_field_string_new("message",((const char *)(dgettext("pidgin","Message"))),message,0);
  purple_request_field_group_add_field(group,field);
  purple_request_fields(conv,((const char *)(dgettext("pidgin","Invite to chat"))),0,((const char *)(dgettext("pidgin","Please enter the name of the user you wish to invite, along with an optional invite message."))),fields,((const char *)(dgettext("pidgin","Invite"))),((GCallback )invite_user_to_chat),((const char *)(dgettext("pidgin","Cancel"))),0,account,user,conv,conv);
}

gboolean purple_conv_chat_has_left(PurpleConvChat *chat)
{
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return (!0);
    };
  }while (0);
  return chat -> left;
}

PurpleConvChatBuddy *purple_conv_chat_cb_new(const char *name,const char *alias,PurpleConvChatBuddyFlags flags)
{
  PurpleConvChatBuddy *cb;
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return 0;
    };
  }while (0);
  cb = ((PurpleConvChatBuddy *)(g_malloc0_n(1,(sizeof(PurpleConvChatBuddy )))));
  cb -> name = g_strdup(name);
  cb -> flags = flags;
  cb -> alias = g_strdup(alias);
  cb -> attributes = g_hash_table_new_full(g_str_hash,g_str_equal,g_free,g_free);
{
    PurpleConvChatBuddy *typed_ptr = cb;
    purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurpleConvChatBuddy);
  };
  return cb;
}

PurpleConvChatBuddy *purple_conv_chat_cb_find(PurpleConvChat *chat,const char *name)
{
  do {
    if (chat != ((PurpleConvChat *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"chat != NULL");
      return 0;
    };
  }while (0);
  do {
    if (name != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name != NULL");
      return 0;
    };
  }while (0);
  return (g_hash_table_lookup((chat -> users),name));
}

void purple_conv_chat_cb_destroy(PurpleConvChatBuddy *cb)
{
  if (cb == ((PurpleConvChatBuddy *)((void *)0))) 
    return ;
  purple_signal_emit(purple_conversations_get_handle(),"deleting-chat-buddy",cb);
  g_free((cb -> alias));
  g_free((cb -> alias_key));
  g_free((cb -> name));
  g_hash_table_destroy((cb -> attributes));
  purple_dbus_unregister_pointer(cb);
  g_free(cb);
}

const char *purple_conv_chat_cb_get_name(PurpleConvChatBuddy *cb)
{
  do {
    if (cb != ((PurpleConvChatBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cb != NULL");
      return 0;
    };
  }while (0);
  return (cb -> name);
}

const char *purple_conv_chat_cb_get_attribute(PurpleConvChatBuddy *cb,const char *key)
{
  do {
    if (cb != ((PurpleConvChatBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cb != NULL");
      return 0;
    };
  }while (0);
  do {
    if (key != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"key != NULL");
      return 0;
    };
  }while (0);
  return (g_hash_table_lookup((cb -> attributes),key));
}

static void append_attribute_key(gpointer key,gpointer value,gpointer user_data)
{
  GList **list = user_data;
   *list = g_list_prepend( *list,key);
}

GList *purple_conv_chat_cb_get_attribute_keys(PurpleConvChatBuddy *cb)
{
  GList *keys = (GList *)((void *)0);
  do {
    if (cb != ((PurpleConvChatBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cb != NULL");
      return 0;
    };
  }while (0);
  g_hash_table_foreach((cb -> attributes),((GHFunc )append_attribute_key),(&keys));
  return keys;
}

void purple_conv_chat_cb_set_attribute(PurpleConvChat *chat,PurpleConvChatBuddy *cb,const char *key,const char *value)
{
  PurpleConversation *conv;
  PurpleConversationUiOps *ops;
  do {
    if (cb != ((PurpleConvChatBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cb != NULL");
      return ;
    };
  }while (0);
  do {
    if (key != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"key != NULL");
      return ;
    };
  }while (0);
  do {
    if (value != ((const char *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"value != NULL");
      return ;
    };
  }while (0);
  g_hash_table_replace((cb -> attributes),(g_strdup(key)),(g_strdup(value)));
  conv = purple_conv_chat_get_conversation(chat);
  ops = purple_conversation_get_ui_ops(conv);
  if ((ops != ((PurpleConversationUiOps *)((void *)0))) && ((ops -> chat_update_user) != ((void (*)(PurpleConversation *, const char *))((void *)0)))) 
    ( *(ops -> chat_update_user))(conv,(cb -> name));
}

void purple_conv_chat_cb_set_attributes(PurpleConvChat *chat,PurpleConvChatBuddy *cb,GList *keys,GList *values)
{
  PurpleConversation *conv;
  PurpleConversationUiOps *ops;
  do {
    if (cb != ((PurpleConvChatBuddy *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cb != NULL");
      return ;
    };
  }while (0);
  do {
    if (keys != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"keys != NULL");
      return ;
    };
  }while (0);
  do {
    if (values != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"values != NULL");
      return ;
    };
  }while (0);
  while((keys != ((GList *)((void *)0))) && (values != ((GList *)((void *)0)))){
    g_hash_table_replace((cb -> attributes),(g_strdup((keys -> data))),(g_strdup((values -> data))));
    keys = ((keys != 0)?( *((GList *)keys)).next : ((struct _GList *)((void *)0)));
    values = ((values != 0)?( *((GList *)values)).next : ((struct _GList *)((void *)0)));
  }
  conv = purple_conv_chat_get_conversation(chat);
  ops = purple_conversation_get_ui_ops(conv);
  if ((ops != ((PurpleConversationUiOps *)((void *)0))) && ((ops -> chat_update_user) != ((void (*)(PurpleConversation *, const char *))((void *)0)))) 
    ( *(ops -> chat_update_user))(conv,(cb -> name));
}

GList *purple_conversation_get_extended_menu(PurpleConversation *conv)
{
  GList *menu = (GList *)((void *)0);
  do {
    if (conv != ((PurpleConversation *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"conv != NULL");
      return 0;
    };
  }while (0);
  purple_signal_emit(purple_conversations_get_handle(),"conversation-extended-menu",conv,&menu);
  return menu;
}

void purple_conversation_clear_message_history(PurpleConversation *conv)
{
  GList *list = (conv -> message_history);
  message_history_free(list);
  conv -> message_history = ((GList *)((void *)0));
  purple_signal_emit(purple_conversations_get_handle(),"cleared-message-history",conv);
}

GList *purple_conversation_get_message_history(PurpleConversation *conv)
{
  return conv -> message_history;
}

const char *purple_conversation_message_get_sender(PurpleConvMessage *msg)
{
  do {
    if (msg != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg");
      return 0;
    };
  }while (0);
  return (msg -> who);
}

const char *purple_conversation_message_get_message(PurpleConvMessage *msg)
{
  do {
    if (msg != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg");
      return 0;
    };
  }while (0);
  return (msg -> what);
}

PurpleMessageFlags purple_conversation_message_get_flags(PurpleConvMessage *msg)
{
  do {
    if (msg != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg");
      return 0;
    };
  }while (0);
  return msg -> flags;
}

time_t purple_conversation_message_get_timestamp(PurpleConvMessage *msg)
{
  do {
    if (msg != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"msg");
      return 0;
    };
  }while (0);
  return msg -> when;
}

gboolean purple_conversation_do_command(PurpleConversation *conv,const gchar *cmdline,const gchar *markup,gchar **error)
{
  char *mark = ((markup != 0) && (( *markup) != 0))?((char *)((void *)0)) : g_markup_escape_text(cmdline,(-1));
  char *err = (char *)((void *)0);
  PurpleCmdStatus status = purple_cmd_do_command(conv,cmdline,((mark != 0)?mark : markup),((error != 0)?error : &err));
  g_free(mark);
  g_free(err);
  return status == PURPLE_CMD_STATUS_OK;
}

void *purple_conversations_get_handle()
{
  static int handle;
  return (&handle);
}

void purple_conversations_init()
{
  void *handle = purple_conversations_get_handle();
  conversation_cache = g_hash_table_new_full(((GHashFunc )_purple_conversations_hconv_hash),((GEqualFunc )_purple_conversations_hconv_equal),((GDestroyNotify )_purple_conversations_hconv_free_key),0);
/**********************************************************************
	 * Register preferences
	 **********************************************************************/
/* Conversations */
  purple_prefs_add_none("/purple/conversations");
/* Conversations -> Chat */
  purple_prefs_add_none("/purple/conversations/chat");
  purple_prefs_add_bool("/purple/conversations/chat/show_nick_change",(!0));
/* Conversations -> IM */
  purple_prefs_add_none("/purple/conversations/im");
  purple_prefs_add_bool("/purple/conversations/im/send_typing",(!0));
/**********************************************************************
	 * Register signals
	 **********************************************************************/
  purple_signal_register(handle,"writing-im-msg",purple_marshal_BOOLEAN__POINTER_POINTER_POINTER_POINTER_UINT,purple_value_new(PURPLE_TYPE_BOOLEAN),5,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING),purple_value_new_outgoing(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_UINT));
  purple_signal_register(handle,"wrote-im-msg",purple_marshal_VOID__POINTER_POINTER_POINTER_POINTER_UINT,0,5,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_UINT));
  purple_signal_register(handle,"sent-attention",purple_marshal_VOID__POINTER_POINTER_POINTER_UINT,0,4,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_UINT));
  purple_signal_register(handle,"got-attention",purple_marshal_VOID__POINTER_POINTER_POINTER_UINT,0,4,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_UINT));
  purple_signal_register(handle,"sending-im-msg",purple_marshal_VOID__POINTER_POINTER_POINTER,0,3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING),purple_value_new_outgoing(PURPLE_TYPE_STRING));
  purple_signal_register(handle,"sent-im-msg",purple_marshal_VOID__POINTER_POINTER_POINTER,0,3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING));
  purple_signal_register(handle,"receiving-im-msg",purple_marshal_BOOLEAN__POINTER_POINTER_POINTER_POINTER_POINTER,purple_value_new(PURPLE_TYPE_BOOLEAN),5,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new_outgoing(PURPLE_TYPE_STRING),purple_value_new_outgoing(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new_outgoing(PURPLE_TYPE_UINT));
  purple_signal_register(handle,"received-im-msg",purple_marshal_VOID__POINTER_POINTER_POINTER_POINTER_UINT,0,5,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_UINT));
  purple_signal_register(handle,"blocked-im-msg",purple_marshal_VOID__POINTER_POINTER_POINTER_UINT_UINT,0,5,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_UINT),purple_value_new(PURPLE_TYPE_UINT));
  purple_signal_register(handle,"writing-chat-msg",purple_marshal_BOOLEAN__POINTER_POINTER_POINTER_POINTER_UINT,purple_value_new(PURPLE_TYPE_BOOLEAN),5,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING),purple_value_new_outgoing(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_UINT));
  purple_signal_register(handle,"wrote-chat-msg",purple_marshal_VOID__POINTER_POINTER_POINTER_POINTER_UINT,0,5,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_UINT));
  purple_signal_register(handle,"sending-chat-msg",purple_marshal_VOID__POINTER_POINTER_UINT,0,3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new_outgoing(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_UINT));
  purple_signal_register(handle,"sent-chat-msg",purple_marshal_VOID__POINTER_POINTER_UINT,0,3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_UINT));
  purple_signal_register(handle,"receiving-chat-msg",purple_marshal_BOOLEAN__POINTER_POINTER_POINTER_POINTER_POINTER,purple_value_new(PURPLE_TYPE_BOOLEAN),5,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new_outgoing(PURPLE_TYPE_STRING),purple_value_new_outgoing(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new_outgoing(PURPLE_TYPE_UINT));
  purple_signal_register(handle,"received-chat-msg",purple_marshal_VOID__POINTER_POINTER_POINTER_POINTER_UINT,0,5,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_UINT));
  purple_signal_register(handle,"conversation-created",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION));
  purple_signal_register(handle,"conversation-updated",purple_marshal_VOID__POINTER_UINT,0,2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_UINT));
  purple_signal_register(handle,"deleting-conversation",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION));
  purple_signal_register(handle,"buddy-typing",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING));
  purple_signal_register(handle,"buddy-typed",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING));
  purple_signal_register(handle,"buddy-typing-stopped",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING));
  purple_signal_register(handle,"chat-buddy-joining",purple_marshal_BOOLEAN__POINTER_POINTER_UINT,purple_value_new(PURPLE_TYPE_BOOLEAN),3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_UINT));
  purple_signal_register(handle,"chat-buddy-joined",purple_marshal_VOID__POINTER_POINTER_UINT_UINT,0,4,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_UINT),purple_value_new(PURPLE_TYPE_BOOLEAN));
  purple_signal_register(handle,"chat-buddy-flags",purple_marshal_VOID__POINTER_POINTER_UINT_UINT,0,4,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_UINT),purple_value_new(PURPLE_TYPE_UINT));
  purple_signal_register(handle,"chat-buddy-leaving",purple_marshal_BOOLEAN__POINTER_POINTER_POINTER,purple_value_new(PURPLE_TYPE_BOOLEAN),3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING));
  purple_signal_register(handle,"chat-buddy-left",purple_marshal_VOID__POINTER_POINTER_POINTER,0,3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING));
  purple_signal_register(handle,"deleting-chat-buddy",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CHATBUDDY));
  purple_signal_register(handle,"chat-inviting-user",purple_marshal_VOID__POINTER_POINTER_POINTER,0,3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_STRING),purple_value_new_outgoing(PURPLE_TYPE_STRING));
  purple_signal_register(handle,"chat-invited-user",purple_marshal_VOID__POINTER_POINTER_POINTER,0,3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING));
  purple_signal_register(handle,"chat-invited",purple_marshal_INT__POINTER_POINTER_POINTER_POINTER_POINTER,purple_value_new(PURPLE_TYPE_INT),5,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_POINTER));
  purple_signal_register(handle,"chat-invite-blocked",purple_marshal_VOID__POINTER_POINTER_POINTER_POINTER_POINTER,0,5,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_ACCOUNT),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_BOXED,"GHashTable *"));
  purple_signal_register(handle,"chat-joined",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION));
  purple_signal_register(handle,"chat-join-failed",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONNECTION),purple_value_new(PURPLE_TYPE_POINTER));
  purple_signal_register(handle,"chat-left",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION));
  purple_signal_register(handle,"chat-topic-changed",purple_marshal_VOID__POINTER_POINTER_POINTER,0,3,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_STRING),purple_value_new(PURPLE_TYPE_STRING));
  purple_signal_register(handle,"cleared-message-history",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION));
  purple_signal_register(handle,"conversation-extended-menu",purple_marshal_VOID__POINTER_POINTER,0,2,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CONVERSATION),purple_value_new(PURPLE_TYPE_BOXED,"GList **"));
}

void purple_conversations_uninit()
{
  while(conversations != 0)
    purple_conversation_destroy(((PurpleConversation *)(conversations -> data)));
  g_hash_table_destroy(conversation_cache);
  purple_signals_unregister_by_instance(purple_conversations_get_handle());
}
