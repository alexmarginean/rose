/**
 * @file dnssrv.c
 */
/* purple
 *
 * Copyright (C) 2005 Thomas Butter <butter@uni-mannheim.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#define _PURPLE_DNSSRV_C_
#include "internal.h"
#include "util.h"
#ifndef _WIN32
#include <arpa/nameser.h>
#include <resolv.h>
#ifdef HAVE_ARPA_NAMESER_COMPAT_H
#include <arpa/nameser_compat.h>
#endif
#else /* WIN32 */
#include <windns.h>
/* Missing from the mingw headers */
#ifndef DNS_TYPE_SRV
# define DNS_TYPE_SRV PurpleDnsTypeSrv
#endif
#ifndef DNS_TYPE_TXT
# define DNS_TYPE_TXT PurpleDnsTypeTxt
#endif
#endif
#ifndef T_SRV
#define T_SRV	PurpleDnsTypeSrv
#endif
#ifndef T_TXT
#define T_TXT	PurpleDnsTypeTxt
#endif
#include "debug.h"
#include "dnssrv.h"
#include "eventloop.h"
#include "network.h"
static PurpleSrvTxtQueryUiOps *srv_txt_query_ui_ops = (PurpleSrvTxtQueryUiOps *)((void *)0);
#ifndef _WIN32
typedef union __unnamed_class___F0_L60_C9_unknown_scope_and_name_variable_declaration__variable_type_HEADERL408R__typedef_declaration_variable_name_unknown_scope_and_name__scope__hdr__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_L13R_index_1024_Ae__variable_name_unknown_scope_and_name__scope__buf {
HEADER hdr;
u_char buf[1024UL];}queryans;
#endif

struct _PurpleSrvTxtQueryData 
{
  union __unnamed_class___F0_L67_C2__PurpleSrvTxtQueryData_variable_declaration__variable_type_L379R_variable_name__PurpleSrvTxtQueryData__scope__srv__DELIMITER___PurpleSrvTxtQueryData_variable_declaration__variable_type_L380R_variable_name__PurpleSrvTxtQueryData__scope__txt {
  PurpleSrvCallback srv;
  PurpleTxtCallback txt;}cb;
  gpointer extradata;
  guint handle;
  int type;
  char *query;
#ifdef _WIN32
#else
  int fd_in;
  int fd_out;
  pid_t pid;
#endif
}
;
typedef struct _PurpleSrvInternalQuery {
int type;
char query[256UL];}PurpleSrvInternalQuery;
typedef struct _PurpleSrvResponseContainer {
PurpleSrvResponse *response;
int sum;}PurpleSrvResponseContainer;
static gboolean purple_srv_txt_query_ui_resolve(PurpleSrvTxtQueryData *query_data);
/**
 * Sort by priority, then by weight.  Strictly numerically--no
 * randomness.  Technically we only need to sort by pref and then
 * make sure any records with weight 0 are at the beginning of
 * their group, but it's just as easy to sort by weight.
 */

static gint responsecompare(gconstpointer ar,gconstpointer br)
{
  PurpleSrvResponse *a = (PurpleSrvResponse *)ar;
  PurpleSrvResponse *b = (PurpleSrvResponse *)br;
  if ((a -> pref) == (b -> pref)) {
    if ((a -> weight) == (b -> weight)) 
      return 0;
    if ((a -> weight) < (b -> weight)) 
      return (-1);
    return 1;
  }
  if ((a -> pref) < (b -> pref)) 
    return (-1);
  return 1;
}
/**
 * Iterate over a list of PurpleSrvResponseContainer making the sum
 * the running total of the sums.  Select a random integer in the range
 * (1, sum+1), then find the first element greater than or equal to the
 * number selected.  From RFC 2782.
 *
 * @param list The list of PurpleSrvResponseContainer.  This function
 *        removes a node from this list and returns the new list.
 * @param container_ptr The PurpleSrvResponseContainer that was chosen
 *        will be returned here.
 */

static GList *select_random_response(GList *list,PurpleSrvResponseContainer **container_ptr)
{
  GList *cur;
  size_t runningtotal;
  int r;
  runningtotal = 0;
  cur = list;
  while(cur != 0){
    PurpleSrvResponseContainer *container = (cur -> data);
    runningtotal += ( *(container -> response)).weight;
    container -> sum = runningtotal;
    cur = (cur -> next);
  }
/*
	 * If the running total is greater than 0, pick a number between
	 * 1 and the runningtotal inclusive. (This is not precisely what
	 * the RFC algorithm describes, but we wish to deal with integers
	 * and avoid floats.  This is functionally equivalent.)
	 * If running total is 0, then choose r = 0.
	 */
  r = ((runningtotal != 0UL)?g_random_int_range(1,(runningtotal + 1)) : 0);
  cur = list;
  while(r > ( *((PurpleSrvResponseContainer *)(cur -> data))).sum){
    cur = (cur -> next);
  }
/* Set the return parameter and remove cur from the list */
   *container_ptr = (cur -> data);
  return g_list_delete_link(list,cur);
}
/**
 * Reorder a GList of PurpleSrvResponses that have the same priority
 * (aka "pref").
 */

static void srv_reorder(GList *list,int num)
{
  int i;
  GList *cur;
  GList *container_list = (GList *)((void *)0);
  PurpleSrvResponseContainer *container;
  if (num < 2) 
/* Nothing to sort */
    return ;
/* First build a list of container structs */
  for (((i = 0) , (cur = list)); i < num; (i++ , (cur = (cur -> next)))) {
    container = ((PurpleSrvResponseContainer *)(g_malloc_n(1,(sizeof(PurpleSrvResponseContainer )))));
    container -> response = (cur -> data);
    container_list = g_list_prepend(container_list,container);
  }
  container_list = g_list_reverse(container_list);
/*
	 * Re-order the list that was passed in as a parameter.  We leave
	 * the list nodes in place, but replace their data pointers.
	 */
  cur = list;
  while(container_list != 0){
    container_list = select_random_response(container_list,&container);
    cur -> data = (container -> response);
    g_free(container);
    cur = (cur -> next);
  }
}
/**
 * Sorts a GList of PurpleSrvResponses according to the
 * algorithm described in RFC 2782.
 *
 * @param response GList of PurpleSrvResponse's
 * @param The original list, resorted
 */

static GList *purple_srv_sort(GList *list)
{
  int pref;
  int count;
  GList *cur;
  GList *start;
  if (!(list != 0) || !((list -> next) != 0)) {
/* Nothing to sort */
    return list;
  }
  list = g_list_sort(list,responsecompare);
  start = (cur = list);
  count = 1;
  while(cur != 0){
    PurpleSrvResponse *next_response;
    pref = ( *((PurpleSrvResponse *)(cur -> data))).pref;
    next_response = ((((cur -> next) != 0)?( *(cur -> next)).data : ((void *)((void *)0))));
    if (!(next_response != 0) || ((next_response -> pref) != pref)) {
/*
			 * The 'count' records starting at 'start' all have the same
			 * priority.  Sort them by weight.
			 */
      srv_reorder(start,count);
      start = (cur -> next);
      count = 0;
    }
    count++;
    cur = (cur -> next);
  }
  return list;
}

static PurpleSrvTxtQueryData *query_data_new(int type,gchar *query,gpointer extradata)
{
  PurpleSrvTxtQueryData *query_data = (PurpleSrvTxtQueryData *)(g_malloc0_n(1,(sizeof(PurpleSrvTxtQueryData ))));
  query_data -> type = type;
  query_data -> extradata = extradata;
  query_data -> query = query;
#ifndef _WIN32
  query_data -> fd_in = -1;
  query_data -> fd_out = -1;
#endif
  return query_data;
}

void purple_srv_txt_query_destroy(PurpleSrvTxtQueryData *query_data)
{
  PurpleSrvTxtQueryUiOps *ops = purple_srv_txt_query_get_ui_ops();
  if ((ops != 0) && ((ops -> destroy) != 0)) 
    ( *(ops -> destroy))(query_data);
  if ((query_data -> handle) > 0) 
    purple_input_remove((query_data -> handle));
#ifdef _WIN32
/*
		 * It's not really possible to kill a thread.  So instead we
		 * just set the callback to NULL and let the DNS lookup
		 * finish.
		 */
#else
  if ((query_data -> fd_out) != -1) 
    close((query_data -> fd_out));
  if ((query_data -> fd_in) != -1) 
    close((query_data -> fd_in));
#endif
  g_free((query_data -> query));
  g_free(query_data);
}
#ifdef USE_IDN

static gboolean dns_str_is_ascii(const char *name)
{
  guchar *c;
  for (c = ((guchar *)name); (c != 0) && (( *c) != 0); ++c) {
    if (( *c) > 0x7f) 
      return 0;
  }
  return (!0);
}
#endif
#ifndef _WIN32

static void write_to_parent(int in,int out,gconstpointer data,gsize size)
{
  const guchar *buf = data;
  gssize w;
  do {
    w = write(out,buf,size);
    if (w > 0) {
      buf += w;
      size -= w;
    }
    else if ((w < 0) && ( *__errno_location() == 4)) {
/* Let's try some more; */
      w = 1;
    }
  }while ((size > 0) && (w > 0));
  if (size != 0) {
/* An error occurred */
    close(out);
    close(in);
    _exit(0);
  }
}
/* Read size bytes to data. Dies if an error occurs. */

static void read_from_parent(int in,int out,gpointer data,gsize size)
{
  guchar *buf = data;
  gssize r;
  do {
    r = read(in,data,size);
    if (r > 0) {
      buf += r;
      size -= r;
    }
    else if ((r < 0) && ( *__errno_location() == 4)) {
/* Let's try some more; */
      r = 1;
    }
  }while ((size > 0) && (r > 0));
  if (size != 0) {
/* An error occurred */
    close(out);
    close(in);
    _exit(0);
  }
}

static void resolve(int in,int out)
{
  GList *ret = (GList *)((void *)0);
  PurpleSrvResponse *srvres;
  PurpleTxtResponse *txtres;
  queryans answer;
  int size;
  int qdcount;
  int ancount;
  guchar *end;
  guchar *cp;
  gchar name[256UL];
  guint16 type;
  guint16 dlen;
  guint16 pref;
  guint16 weight;
  guint16 port;
  PurpleSrvInternalQuery query;
#ifdef HAVE_SIGNAL_H
  purple_restore_default_signal_handlers();
#endif
  read_from_parent(in,out,(&query),(sizeof(query)));
  size = __res_query(query.query,ns_c_in,query.type,((u_char *)(&answer)),(sizeof(answer)));
  if (size == -1) {
    write_to_parent(in,out,(&query.type),(sizeof(query.type)));
    write_to_parent(in,out,(&size),(sizeof(size)));
    close(out);
    close(in);
    _exit(0);
  }
  qdcount = (ntohs(answer.hdr.qdcount));
  ancount = (ntohs(answer.hdr.ancount));
  cp = (((guchar *)(&answer)) + sizeof(HEADER ));
  end = (((guchar *)(&answer)) + size);
/* skip over unwanted stuff */
  while((qdcount-- > 0) && (cp < end)){
    size = __dn_expand(((unsigned char *)(&answer)),end,cp,name,256);
    if (size < 0) 
      goto end;
    cp += (size + 4);
  }
  while((ancount-- > 0) && (cp < end)){
    size = __dn_expand(((unsigned char *)(&answer)),end,cp,name,256);
    if (size < 0) 
      goto end;
    cp += size;
    do {
      const u_char *t_cp = (const u_char *)cp;
      type = ((((u_int16_t )t_cp[0]) << 8) | ((u_int16_t )t_cp[1]));
      cp += 2;
    }while (0);
/* skip ttl and class since we already know it */
    cp += 6;
    do {
      const u_char *t_cp = (const u_char *)cp;
      dlen = ((((u_int16_t )t_cp[0]) << 8) | ((u_int16_t )t_cp[1]));
      cp += 2;
    }while (0);
    if (type == ns_t_srv) {
      do {
        const u_char *t_cp = (const u_char *)cp;
        pref = ((((u_int16_t )t_cp[0]) << 8) | ((u_int16_t )t_cp[1]));
        cp += 2;
      }while (0);
      do {
        const u_char *t_cp = (const u_char *)cp;
        weight = ((((u_int16_t )t_cp[0]) << 8) | ((u_int16_t )t_cp[1]));
        cp += 2;
      }while (0);
      do {
        const u_char *t_cp = (const u_char *)cp;
        port = ((((u_int16_t )t_cp[0]) << 8) | ((u_int16_t )t_cp[1]));
        cp += 2;
      }while (0);
      size = __dn_expand(((unsigned char *)(&answer)),end,cp,name,256);
      if (size < 0) 
        goto end;
      cp += size;
      srvres = ((PurpleSrvResponse *)(g_malloc0_n(1,(sizeof(PurpleSrvResponse )))));
      if ((strlen(name)) > sizeof(srvres -> hostname) - 1) {
        purple_debug_error("dnssrv","hostname is longer than available buffer (\'%s\', %zd bytes)!",name,strlen(name));
      }
      g_strlcpy((srvres -> hostname),name,(sizeof(srvres -> hostname)));
      srvres -> pref = pref;
      srvres -> port = port;
      srvres -> weight = weight;
      ret = g_list_prepend(ret,srvres);
    }
    else if (type == ns_t_txt) {
      txtres = ((PurpleTxtResponse *)(g_malloc0_n(1,(sizeof(PurpleTxtResponse )))));
      txtres -> content = g_strndup(((gchar *)(++cp)),(dlen - 1));
      ret = g_list_append(ret,txtres);
      cp += (dlen - 1);
    }
    else {
      cp += dlen;
    }
  }
  end:
  size = (g_list_length(ret));
  if (query.type == ns_t_srv) 
    ret = purple_srv_sort(ret);
  write_to_parent(in,out,(&query.type),(sizeof(query.type)));
  write_to_parent(in,out,(&size),(sizeof(size)));
  while(ret != ((GList *)((void *)0))){
    if (query.type == ns_t_srv) 
      write_to_parent(in,out,(ret -> data),(sizeof(PurpleSrvResponse )));
    if (query.type == ns_t_txt) {
      PurpleTxtResponse *response = (ret -> data);
/* null byte */
      gsize l = (strlen((response -> content)) + 1);
      write_to_parent(in,out,(&l),(sizeof(l)));
      write_to_parent(in,out,(response -> content),l);
    }
    g_free((ret -> data));
    ret = g_list_remove(ret,(ret -> data));
  }
  close(out);
  close(in);
  _exit(0);
}

static void resolved(gpointer data,gint source,PurpleInputCondition cond)
{
  int size;
  int type;
  PurpleSrvTxtQueryData *query_data = (PurpleSrvTxtQueryData *)data;
  int i;
  int status;
  if ((read(source,(&type),(sizeof(type)))) == sizeof(type)) {
    if ((read(source,(&size),(sizeof(size)))) == sizeof(size)) {
      if ((size == -1) || (size == 0)) {
        if (size == -1) {
          purple_debug_warning("dnssrv","res_query returned an error\n");
/* Re-read resolv.conf and friends in case DNS servers have changed */
          __res_init();
        }
        else 
          purple_debug_info("dnssrv","Found 0 entries, errno is %i\n", *__errno_location());
        if (type == ns_t_srv) {
          PurpleSrvCallback cb = query_data -> cb.srv;
          ( *cb)(0,0,(query_data -> extradata));
        }
        else if (type == ns_t_txt) {
          PurpleTxtCallback cb = query_data -> cb.txt;
          ( *cb)(0,(query_data -> extradata));
        }
        else {
          purple_debug_error("dnssrv","type unknown of DNS result entry; errno is %i\n", *__errno_location());
        }
      }
      else if (size != 0) {
        if (type == ns_t_srv) {
          PurpleSrvResponse *res;
          PurpleSrvResponse *tmp;
          PurpleSrvCallback cb = query_data -> cb.srv;
          ssize_t red;
          purple_debug_info("dnssrv","found %d SRV entries\n",size);
          tmp = (res = ((PurpleSrvResponse *)(g_malloc0_n(size,(sizeof(PurpleSrvResponse ))))));
          for (i = 0; i < size; i++) {
            red = read(source,(tmp++),(sizeof(PurpleSrvResponse )));
            if (red != sizeof(PurpleSrvResponse )) {
              purple_debug_error("dnssrv","unable to read srv response: %s\n",g_strerror( *__errno_location()));
              size = 0;
              g_free(res);
              res = ((PurpleSrvResponse *)((void *)0));
            }
          }
          ( *cb)(res,size,(query_data -> extradata));
        }
        else if (type == ns_t_txt) {
          GList *responses = (GList *)((void *)0);
          PurpleTxtResponse *res;
          PurpleTxtCallback cb = query_data -> cb.txt;
          ssize_t red;
          purple_debug_info("dnssrv","found %d TXT entries\n",size);
{
            for (i = 0; i < size; i++) {
              gsize len;
              red = read(source,(&len),(sizeof(len)));
              if (red != sizeof(len)) {
                purple_debug_error("dnssrv","unable to read txt response length: %s\n",g_strerror( *__errno_location()));
                size = 0;
                g_list_foreach(responses,((GFunc )purple_txt_response_destroy),0);
                g_list_free(responses);
                responses = ((GList *)((void *)0));
                break; 
              }
              res = ((PurpleTxtResponse *)(g_malloc0_n(1,(sizeof(PurpleTxtResponse )))));
              res -> content = ((gchar *)(g_malloc0_n(len,(sizeof(gchar )))));
              red = read(source,(res -> content),len);
              if (red != len) {
                purple_debug_error("dnssrv","unable to read txt response: %s\n",g_strerror( *__errno_location()));
                size = 0;
                purple_txt_response_destroy(res);
                g_list_foreach(responses,((GFunc )purple_txt_response_destroy),0);
                g_list_free(responses);
                responses = ((GList *)((void *)0));
                break; 
              }
              responses = g_list_prepend(responses,res);
            }
          }
          responses = g_list_reverse(responses);
          ( *cb)(responses,(query_data -> extradata));
        }
        else {
          purple_debug_error("dnssrv","type unknown of DNS result entry; errno is %i\n", *__errno_location());
        }
      }
    }
  }
  waitpid((query_data -> pid),&status,0);
  purple_srv_txt_query_destroy(query_data);
}
#else /* _WIN32 */
/** The Jabber Server code was inspiration for parts of this. */
/* Discard any incorrect entries. I'm not sure if this is necessary */
/* Discard any incorrect entries. I'm not sure if this is necessary */
/* back to main thread */
/* Note: this should *not* be attached to query_data->handle - it will cause leakage */
#endif

PurpleSrvTxtQueryData *purple_srv_resolve(const char *protocol,const char *transport,const char *domain,PurpleSrvCallback cb,gpointer extradata)
{
  return purple_srv_resolve_account(0,protocol,transport,domain,cb,extradata);
}

PurpleSrvTxtQueryData *purple_srv_resolve_account(PurpleAccount *account,const char *protocol,const char *transport,const char *domain,PurpleSrvCallback cb,gpointer extradata)
{
  char *query;
  char *hostname;
  PurpleSrvTxtQueryData *query_data;
  PurpleProxyType proxy_type;
#ifndef _WIN32
  PurpleSrvInternalQuery internal_query;
  int in[2UL];
  int out[2UL];
  int pid;
#else
#endif
  if (((((!(protocol != 0) || !(( *protocol) != 0)) || !(transport != 0)) || !(( *transport) != 0)) || !(domain != 0)) || !(( *domain) != 0)) {
    purple_debug_error("dnssrv","Wrong arguments\n");
    ( *cb)(0,0,extradata);
    do {
      g_log(0,G_LOG_LEVEL_CRITICAL,"file %s: line %d (%s): should not be reached","dnssrv.c",748,((const char *)__func__));
      return 0;
    }while (0);
  }
  proxy_type = purple_proxy_info_get_type((purple_proxy_get_setup(account)));
  if (proxy_type == PURPLE_PROXY_TOR) {
    purple_debug_info("dnssrv","Aborting SRV lookup in Tor Proxy mode.");
    ( *cb)(0,0,extradata);
    return 0;
  }
#ifdef USE_IDN
  if (!(dns_str_is_ascii(domain) != 0)) {
    int ret = purple_network_convert_idn_to_ascii(domain,&hostname);
    if (ret != 0) {
      purple_debug_error("dnssrv","IDNA ToASCII failed\n");
      ( *cb)(0,0,extradata);
      return 0;
    }
/* Fallthru is intentional */
  }
  else 
#endif
    hostname = g_strdup(domain);
  query = g_strdup_printf("_%s._%s.%s",protocol,transport,hostname);
  purple_debug_info("dnssrv","querying SRV record for %s: %s\n",domain,query);
  g_free(hostname);
  query_data = query_data_new(PurpleDnsTypeSrv,query,extradata);
  query_data -> cb.srv = cb;
  if (purple_srv_txt_query_ui_resolve(query_data) != 0) {
    return query_data;
  }
#ifndef _WIN32
  if ((pipe(in) != 0) || (pipe(out) != 0)) {
    purple_debug_error("dnssrv","Could not create pipe\n");
    g_free(query);
    g_free(query_data);
    ( *cb)(0,0,extradata);
    return 0;
  }
  pid = fork();
  if (pid == -1) {
    purple_debug_error("dnssrv","Could not create process!\n");
    g_free(query);
    g_free(query_data);
    ( *cb)(0,0,extradata);
    return 0;
  }
/* Child */
  if (pid == 0) {
    g_free(query);
    g_free(query_data);
    close(out[0]);
    close(in[1]);
    resolve(in[0],out[1]);
/* resolve() does not return */
  }
  close(out[1]);
  close(in[0]);
  internal_query.type = ns_t_srv;
  strncpy(internal_query.query,query,255);
  internal_query.query[255] = 0;
  if (write(in[1],(&internal_query),(sizeof(internal_query))) < 0) 
    purple_debug_error("dnssrv","Could not write to SRV resolver\n");
  query_data -> pid = pid;
  query_data -> fd_out = out[0];
  query_data -> fd_in = in[1];
  query_data -> handle = purple_input_add(out[0],PURPLE_INPUT_READ,resolved,query_data);
  return query_data;
#else
/* The query isn't going to happen, so finish the SRV lookup now.
	 * Asynchronously call the callback since stuff may not expect
	 * the callback to be called before this returns */
#endif
}

PurpleSrvTxtQueryData *purple_txt_resolve(const char *owner,const char *domain,PurpleTxtCallback cb,gpointer extradata)
{
  return purple_txt_resolve_account(0,owner,domain,cb,extradata);
}

PurpleSrvTxtQueryData *purple_txt_resolve_account(PurpleAccount *account,const char *owner,const char *domain,PurpleTxtCallback cb,gpointer extradata)
{
  char *query;
  char *hostname;
  PurpleSrvTxtQueryData *query_data;
  PurpleProxyType proxy_type;
#ifndef _WIN32
  PurpleSrvInternalQuery internal_query;
  int in[2UL];
  int out[2UL];
  int pid;
#else
#endif
  proxy_type = purple_proxy_info_get_type((purple_proxy_get_setup(account)));
  if (proxy_type == PURPLE_PROXY_TOR) {
    purple_debug_info("dnssrv","Aborting TXT lookup in Tor Proxy mode.");
    ( *cb)(0,extradata);
    return 0;
  }
#ifdef USE_IDN
  if (!(dns_str_is_ascii(domain) != 0)) {
    int ret = purple_network_convert_idn_to_ascii(domain,&hostname);
    if (ret != 0) {
      purple_debug_error("dnssrv","IDNA ToASCII failed\n");
      ( *cb)(0,extradata);
      return 0;
    }
/* fallthru is intentional */
  }
  else 
#endif
    hostname = g_strdup(domain);
  query = g_strdup_printf("%s.%s",owner,hostname);
  purple_debug_info("dnssrv","querying TXT record for %s: %s\n",domain,query);
  g_free(hostname);
  query_data = query_data_new(PurpleDnsTypeTxt,query,extradata);
  query_data -> cb.txt = cb;
  if (purple_srv_txt_query_ui_resolve(query_data) != 0) {
/* query intentionally not freed
		 */
    return query_data;
  }
#ifndef _WIN32
  if ((pipe(in) != 0) || (pipe(out) != 0)) {
    purple_debug_error("dnssrv","Could not create pipe\n");
    g_free(query);
    g_free(query_data);
    ( *cb)(0,extradata);
    return 0;
  }
  pid = fork();
  if (pid == -1) {
    purple_debug_error("dnssrv","Could not create process!\n");
    g_free(query);
    g_free(query_data);
    ( *cb)(0,extradata);
    return 0;
  }
/* Child */
  if (pid == 0) {
    g_free(query);
    g_free(query_data);
    close(out[0]);
    close(in[1]);
    resolve(in[0],out[1]);
/* resolve() does not return */
  }
  close(out[1]);
  close(in[0]);
  internal_query.type = ns_t_txt;
  strncpy(internal_query.query,query,255);
  internal_query.query[255] = 0;
  if (write(in[1],(&internal_query),(sizeof(internal_query))) < 0) 
    purple_debug_error("dnssrv","Could not write to TXT resolver\n");
  query_data -> pid = pid;
  query_data -> fd_out = out[0];
  query_data -> fd_in = in[1];
  query_data -> handle = purple_input_add(out[0],PURPLE_INPUT_READ,resolved,query_data);
  return query_data;
#else
/* The query isn't going to happen, so finish the TXT lookup now.
	 * Asynchronously call the callback since stuff may not expect
	 * the callback to be called before this returns */
#endif
}

void purple_txt_cancel(PurpleSrvTxtQueryData *query_data)
{
  purple_srv_txt_query_destroy(query_data);
}

void purple_srv_cancel(PurpleSrvTxtQueryData *query_data)
{
  purple_srv_txt_query_destroy(query_data);
}

const gchar *purple_txt_response_get_content(PurpleTxtResponse *resp)
{
  do {
    if (resp != ((PurpleTxtResponse *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"resp != NULL");
      return 0;
    };
  }while (0);
  return (resp -> content);
}

void purple_txt_response_destroy(PurpleTxtResponse *resp)
{
  do {
    if (resp != ((PurpleTxtResponse *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"resp != NULL");
      return ;
    };
  }while (0);
  g_free((resp -> content));
  g_free(resp);
}
/*
 * Only used as the callback for the ui ops.
 */

static void purple_srv_query_resolved(PurpleSrvTxtQueryData *query_data,GList *records)
{
  GList *l;
  PurpleSrvResponse *records_array;
  int i = 0;
  int length;
  do {
    if (records != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"records != NULL");
      return ;
    };
  }while (0);
  if (query_data -> cb.srv == ((void (*)(PurpleSrvResponse *, int , gpointer ))((void *)0))) {
    purple_srv_txt_query_destroy(query_data);
    while(records != 0){
      g_free((records -> data));
      records = g_list_delete_link(records,records);
    }
    return ;
  }
  records = purple_srv_sort(records);
  length = (g_list_length(records));
  purple_debug_info("dnssrv","SRV records resolved for %s, count: %d\n",(query_data -> query),length);
  records_array = ((PurpleSrvResponse *)(g_malloc_n(length,(sizeof(PurpleSrvResponse )))));
  for (l = records; l != 0; ((l = (l -> next)) , i++)) {
    records_array[i] =  *((PurpleSrvResponse *)(l -> data));
  }
  ( *query_data -> cb.srv)(records_array,length,(query_data -> extradata));
  purple_srv_txt_query_destroy(query_data);
  while(records != 0){
    g_free((records -> data));
    records = g_list_delete_link(records,records);
  }
}
/*
 * Only used as the callback for the ui ops.
 */

static void purple_txt_query_resolved(PurpleSrvTxtQueryData *query_data,GList *entries)
{
  do {
    if (entries != ((GList *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"entries != NULL");
      return ;
    };
  }while (0);
  purple_debug_info("dnssrv","TXT entries resolved for %s, count: %d\n",(query_data -> query),g_list_length(entries));
/* the callback should g_free the entries.
	 */
  if (query_data -> cb.txt != ((void (*)(GList *, gpointer ))((void *)0))) 
    ( *query_data -> cb.txt)(entries,(query_data -> extradata));
  else {
    while(entries != 0){
      g_free((entries -> data));
      entries = g_list_delete_link(entries,entries);
    }
  }
  purple_srv_txt_query_destroy(query_data);
}

static void purple_srv_query_failed(PurpleSrvTxtQueryData *query_data,const gchar *error_message)
{
  purple_debug_error("dnssrv","%s\n",error_message);
  if (query_data -> cb.srv != ((void (*)(PurpleSrvResponse *, int , gpointer ))((void *)0))) 
    ( *query_data -> cb.srv)(0,0,(query_data -> extradata));
  purple_srv_txt_query_destroy(query_data);
}

static gboolean purple_srv_txt_query_ui_resolve(PurpleSrvTxtQueryData *query_data)
{
  PurpleSrvTxtQueryUiOps *ops = purple_srv_txt_query_get_ui_ops();
  if ((ops != 0) && ((ops -> resolve) != 0)) 
    return ( *(ops -> resolve))(query_data,(((query_data -> type) == ns_t_srv)?purple_srv_query_resolved : purple_txt_query_resolved),purple_srv_query_failed);
  return 0;
}

void purple_srv_txt_query_set_ui_ops(PurpleSrvTxtQueryUiOps *ops)
{
  srv_txt_query_ui_ops = ops;
}

PurpleSrvTxtQueryUiOps *purple_srv_txt_query_get_ui_ops()
{
/* It is perfectly acceptable for srv_txt_query_ui_ops to be NULL; this just
	 * means that the default platform-specific implementation will be used.
	 */
  return srv_txt_query_ui_ops;
}

char *purple_srv_txt_query_get_query(PurpleSrvTxtQueryData *query_data)
{
  do {
    if (query_data != ((PurpleSrvTxtQueryData *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"query_data != NULL");
      return 0;
    };
  }while (0);
  return query_data -> query;
}

int purple_srv_txt_query_get_type(PurpleSrvTxtQueryData *query_data)
{
  do {
    if (query_data != ((PurpleSrvTxtQueryData *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"query_data != NULL");
      return 0;
    };
  }while (0);
  return query_data -> type;
}
