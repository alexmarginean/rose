/*
 * purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * Original des taken from gpg
 *
 * des.c - DES and Triple-DES encryption/decryption Algorithm
 *	Copyright (C) 1998 Free Software Foundation, Inc.
 *
 *	Please see below for more legal information!
 *
 *	 According to the definition of DES in FIPS PUB 46-2 from December 1993.
 *	 For a description of triple encryption, see:
 *	   Bruce Schneier: Applied Cryptography. Second Edition.
 *	   John Wiley & Sons, 1996. ISBN 0-471-12845-7. Pages 358 ff.
 *
 *	 This file is part of GnuPG.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "cipher.h"
#include "dbus-maybe.h"
#include "debug.h"
#include "signals.h"
#include "value.h"
/*******************************************************************************
 * Structs
 ******************************************************************************/

struct _PurpleCipher 
{
/**< Internal name - used for searching */
  gchar *name;
/**< Operations supported by this cipher */
  PurpleCipherOps *ops;
/**< Reference count */
  guint ref;
}
;

struct _PurpleCipherContext 
{
/**< Cipher this context is under */
  PurpleCipher *cipher;
/**< Internal cipher state data */
  gpointer data;
}
;
/******************************************************************************
 * Globals
 *****************************************************************************/
static GList *ciphers = (GList *)((void *)0);
/******************************************************************************
 * PurpleCipher API
 *****************************************************************************/

const gchar *purple_cipher_get_name(PurpleCipher *cipher)
{
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return 0;
    };
  }while (0);
  return (cipher -> name);
}

guint purple_cipher_get_capabilities(PurpleCipher *cipher)
{
  PurpleCipherOps *ops = (PurpleCipherOps *)((void *)0);
  guint caps = 0;
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return 0;
    };
  }while (0);
  ops = (cipher -> ops);
  do {
    if (ops != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ops");
      return 0;
    };
  }while (0);
  if ((ops -> set_option) != 0) 
    caps |= PURPLE_CIPHER_CAPS_SET_OPT;
  if ((ops -> get_option) != 0) 
    caps |= PURPLE_CIPHER_CAPS_GET_OPT;
  if ((ops -> init) != 0) 
    caps |= PURPLE_CIPHER_CAPS_INIT;
  if ((ops -> reset) != 0) 
    caps |= PURPLE_CIPHER_CAPS_RESET;
  if ((ops -> uninit) != 0) 
    caps |= PURPLE_CIPHER_CAPS_UNINIT;
  if ((ops -> set_iv) != 0) 
    caps |= PURPLE_CIPHER_CAPS_SET_IV;
  if ((ops -> append) != 0) 
    caps |= PURPLE_CIPHER_CAPS_APPEND;
  if ((ops -> digest) != 0) 
    caps |= PURPLE_CIPHER_CAPS_DIGEST;
  if ((ops -> encrypt) != 0) 
    caps |= PURPLE_CIPHER_CAPS_ENCRYPT;
  if ((ops -> decrypt) != 0) 
    caps |= PURPLE_CIPHER_CAPS_DECRYPT;
  if ((ops -> set_salt) != 0) 
    caps |= PURPLE_CIPHER_CAPS_SET_SALT;
  if ((ops -> get_salt_size) != 0) 
    caps |= PURPLE_CIPHER_CAPS_GET_SALT_SIZE;
  if ((ops -> set_key) != 0) 
    caps |= PURPLE_CIPHER_CAPS_SET_KEY;
  if ((ops -> get_key_size) != 0) 
    caps |= PURPLE_CIPHER_CAPS_GET_KEY_SIZE;
  if ((ops -> set_batch_mode) != 0) 
    caps |= PURPLE_CIPHER_CAPS_SET_BATCH_MODE;
  if ((ops -> get_batch_mode) != 0) 
    caps |= PURPLE_CIPHER_CAPS_GET_BATCH_MODE;
  if ((ops -> get_block_size) != 0) 
    caps |= PURPLE_CIPHER_CAPS_GET_BLOCK_SIZE;
  if ((ops -> set_key_with_len) != 0) 
    caps |= PURPLE_CIPHER_CAPS_SET_KEY_WITH_LEN;
  return caps;
}

gboolean purple_cipher_digest_region(const gchar *name,const guchar *data,size_t data_len,size_t in_len,guchar (digest)[],size_t *out_len)
{
  PurpleCipher *cipher;
  PurpleCipherContext *context;
  gboolean ret = 0;
  do {
    if (name != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name");
      return 0;
    };
  }while (0);
  do {
    if (data != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"data");
      return 0;
    };
  }while (0);
  cipher = purple_ciphers_find_cipher(name);
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return 0;
    };
  }while (0);
  if (!(( *(cipher -> ops)).append != 0) || !(( *(cipher -> ops)).digest != 0)) {
    purple_debug_warning("cipher","purple_cipher_region failed: the %s cipher does not support appending and or digesting.",(cipher -> name));
    return 0;
  }
  context = purple_cipher_context_new(cipher,0);
  purple_cipher_context_append(context,data,data_len);
  ret = purple_cipher_context_digest(context,in_len,digest,out_len);
  purple_cipher_context_destroy(context);
  return ret;
}
/******************************************************************************
 * PurpleCiphers API
 *****************************************************************************/

PurpleCipher *purple_ciphers_find_cipher(const gchar *name)
{
  PurpleCipher *cipher;
  GList *l;
  do {
    if (name != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name");
      return 0;
    };
  }while (0);
  for (l = ciphers; l != 0; l = (l -> next)) {
    cipher = ((PurpleCipher *)(l -> data));
    if (!(g_ascii_strcasecmp((cipher -> name),name) != 0)) 
      return cipher;
  }
  return 0;
}

PurpleCipher *purple_ciphers_register_cipher(const gchar *name,PurpleCipherOps *ops)
{
  PurpleCipher *cipher = (PurpleCipher *)((void *)0);
  do {
    if (name != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name");
      return 0;
    };
  }while (0);
  do {
    if (ops != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"ops");
      return 0;
    };
  }while (0);
  do {
    if (!(purple_ciphers_find_cipher(name) != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"!purple_ciphers_find_cipher(name)");
      return 0;
    };
  }while (0);
  cipher = ((PurpleCipher *)(g_malloc0_n(1,(sizeof(PurpleCipher )))));
{
    PurpleCipher *typed_ptr = cipher;
    purple_dbus_register_pointer(typed_ptr,&PURPLE_DBUS_TYPE_PurpleCipher);
  };
  cipher -> name = g_strdup(name);
  cipher -> ops = ops;
  ciphers = g_list_append(ciphers,cipher);
  purple_signal_emit(purple_ciphers_get_handle(),"cipher-added",cipher);
  return cipher;
}

gboolean purple_ciphers_unregister_cipher(PurpleCipher *cipher)
{
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return 0;
    };
  }while (0);
  do {
    if ((cipher -> ref) == 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher->ref == 0");
      return 0;
    };
  }while (0);
  purple_signal_emit(purple_ciphers_get_handle(),"cipher-removed",cipher);
  ciphers = g_list_remove(ciphers,cipher);
  g_free((cipher -> name));
  purple_dbus_unregister_pointer(cipher);
  g_free(cipher);
  return (!0);
}

GList *purple_ciphers_get_ciphers()
{
  return ciphers;
}
/******************************************************************************
 * PurpleCipher Subsystem API
 *****************************************************************************/

gpointer purple_ciphers_get_handle()
{
  static gint handle;
  return (&handle);
}
/* These are implemented in the purple-ciphers sublibrary built in the ciphers
 * directory.  We could put a header file in there, but it's less hassle for
 * the developer to just add it here since they have to register it here as
 * well.
 */
PurpleCipherOps *purple_des_cipher_get_ops();
PurpleCipherOps *purple_des3_cipher_get_ops();
PurpleCipherOps *purple_hmac_cipher_get_ops();
PurpleCipherOps *purple_md4_cipher_get_ops();
PurpleCipherOps *purple_md5_cipher_get_ops();
PurpleCipherOps *purple_rc4_cipher_get_ops();
PurpleCipherOps *purple_sha1_cipher_get_ops();
PurpleCipherOps *purple_sha256_cipher_get_ops();

void purple_ciphers_init()
{
  gpointer handle;
  handle = purple_ciphers_get_handle();
  purple_signal_register(handle,"cipher-added",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CIPHER));
  purple_signal_register(handle,"cipher-removed",purple_marshal_VOID__POINTER,0,1,purple_value_new(PURPLE_TYPE_SUBTYPE,PURPLE_SUBTYPE_CIPHER));
  purple_ciphers_register_cipher("md5",purple_md5_cipher_get_ops());
  purple_ciphers_register_cipher("sha1",purple_sha1_cipher_get_ops());
  purple_ciphers_register_cipher("sha256",purple_sha256_cipher_get_ops());
  purple_ciphers_register_cipher("md4",purple_md4_cipher_get_ops());
  purple_ciphers_register_cipher("hmac",purple_hmac_cipher_get_ops());
  purple_ciphers_register_cipher("des",purple_des_cipher_get_ops());
  purple_ciphers_register_cipher("des3",purple_des3_cipher_get_ops());
  purple_ciphers_register_cipher("rc4",purple_rc4_cipher_get_ops());
}

void purple_ciphers_uninit()
{
  PurpleCipher *cipher;
  GList *l;
  GList *ll;
  for (l = ciphers; l != 0; l = ll) {
    ll = (l -> next);
    cipher = ((PurpleCipher *)(l -> data));
    purple_ciphers_unregister_cipher(cipher);
  }
  g_list_free(ciphers);
  purple_signals_unregister_by_instance(purple_ciphers_get_handle());
}
/******************************************************************************
 * PurpleCipherContext API
 *****************************************************************************/

void purple_cipher_context_set_option(PurpleCipherContext *context,const gchar *name,gpointer value)
{
  PurpleCipher *cipher = (PurpleCipher *)((void *)0);
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return ;
    };
  }while (0);
  do {
    if (name != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name");
      return ;
    };
  }while (0);
  cipher = (context -> cipher);
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return ;
    };
  }while (0);
  if (((cipher -> ops) != 0) && (( *(cipher -> ops)).set_option != 0)) 
    ( *( *(cipher -> ops)).set_option)(context,name,value);
  else 
    purple_debug_warning("cipher","the %s cipher does not support the set_option operation\n",(cipher -> name));
}

gpointer purple_cipher_context_get_option(PurpleCipherContext *context,const gchar *name)
{
  PurpleCipher *cipher = (PurpleCipher *)((void *)0);
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return 0;
    };
  }while (0);
  do {
    if (name != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name");
      return 0;
    };
  }while (0);
  cipher = (context -> cipher);
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return 0;
    };
  }while (0);
  if (((cipher -> ops) != 0) && (( *(cipher -> ops)).get_option != 0)) 
    return ( *( *(cipher -> ops)).get_option)(context,name);
  else {
    purple_debug_warning("cipher","the %s cipher does not support the get_option operation\n",(cipher -> name));
    return 0;
  }
}

PurpleCipherContext *purple_cipher_context_new(PurpleCipher *cipher,void *extra)
{
  PurpleCipherContext *context = (PurpleCipherContext *)((void *)0);
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return 0;
    };
  }while (0);
  cipher -> ref++;
  context = ((PurpleCipherContext *)(g_malloc0_n(1,(sizeof(PurpleCipherContext )))));
  context -> cipher = cipher;
  if (( *(cipher -> ops)).init != 0) 
    ( *( *(cipher -> ops)).init)(context,extra);
  return context;
}

PurpleCipherContext *purple_cipher_context_new_by_name(const gchar *name,void *extra)
{
  PurpleCipher *cipher;
  do {
    if (name != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"name");
      return 0;
    };
  }while (0);
  cipher = purple_ciphers_find_cipher(name);
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return 0;
    };
  }while (0);
  return purple_cipher_context_new(cipher,extra);
}

void purple_cipher_context_reset(PurpleCipherContext *context,void *extra)
{
  PurpleCipher *cipher = (PurpleCipher *)((void *)0);
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return ;
    };
  }while (0);
  cipher = (context -> cipher);
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return ;
    };
  }while (0);
  if (((cipher -> ops) != 0) && (( *(cipher -> ops)).reset != 0)) 
    ( *( *( *(context -> cipher)).ops).reset)(context,extra);
}

void purple_cipher_context_destroy(PurpleCipherContext *context)
{
  PurpleCipher *cipher = (PurpleCipher *)((void *)0);
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return ;
    };
  }while (0);
  cipher = (context -> cipher);
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return ;
    };
  }while (0);
  cipher -> ref--;
  if (((cipher -> ops) != 0) && (( *(cipher -> ops)).uninit != 0)) 
    ( *( *(cipher -> ops)).uninit)(context);
  memset(context,0,(sizeof(( *context))));
  g_free(context);
  context = ((PurpleCipherContext *)((void *)0));
}

void purple_cipher_context_set_iv(PurpleCipherContext *context,guchar *iv,size_t len)
{
  PurpleCipher *cipher = (PurpleCipher *)((void *)0);
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return ;
    };
  }while (0);
  do {
    if (iv != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"iv");
      return ;
    };
  }while (0);
  cipher = (context -> cipher);
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return ;
    };
  }while (0);
  if (((cipher -> ops) != 0) && (( *(cipher -> ops)).set_iv != 0)) 
    ( *( *(cipher -> ops)).set_iv)(context,iv,len);
  else 
    purple_debug_warning("cipher","the %s cipher does not support the setinitialization vector operation\n",(cipher -> name));
}

void purple_cipher_context_append(PurpleCipherContext *context,const guchar *data,size_t len)
{
  PurpleCipher *cipher = (PurpleCipher *)((void *)0);
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return ;
    };
  }while (0);
  cipher = (context -> cipher);
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return ;
    };
  }while (0);
  if (((cipher -> ops) != 0) && (( *(cipher -> ops)).append != 0)) 
    ( *( *(cipher -> ops)).append)(context,data,len);
  else 
    purple_debug_warning("cipher","the %s cipher does not support the append operation\n",(cipher -> name));
}

gboolean purple_cipher_context_digest(PurpleCipherContext *context,size_t in_len,guchar digest[],size_t *out_len)
{
  PurpleCipher *cipher = (PurpleCipher *)((void *)0);
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return 0;
    };
  }while (0);
  cipher = (context -> cipher);
  if (((cipher -> ops) != 0) && (( *(cipher -> ops)).digest != 0)) 
    return ( *( *(cipher -> ops)).digest)(context,in_len,digest,out_len);
  else {
    purple_debug_warning("cipher","the %s cipher does not support the digest operation\n",(cipher -> name));
    return 0;
  }
}

gboolean purple_cipher_context_digest_to_str(PurpleCipherContext *context,size_t in_len,gchar digest_s[],size_t *out_len)
{
/* 8k is a bit excessive, will tweak later. */
  guchar digest[8192UL];
  gint n = 0;
  size_t dlen = 0;
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return 0;
    };
  }while (0);
  do {
    if (digest_s != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"digest_s");
      return 0;
    };
  }while (0);
  if (!(purple_cipher_context_digest(context,(sizeof(digest)),digest,&dlen) != 0)) 
    return 0;
/* in_len must be greater than dlen * 2 so we have room for the NUL. */
  if (in_len <= (dlen * 2)) 
    return 0;
  for (n = 0; n < dlen; n++) 
    sprintf((digest_s + (n * 2)),"%02x",digest[n]);
  digest_s[n * 2] = 0;
  if (out_len != 0) 
     *out_len = (dlen * 2);
  return (!0);
}

gint purple_cipher_context_encrypt(PurpleCipherContext *context,const guchar data[],size_t len,guchar output[],size_t *outlen)
{
  PurpleCipher *cipher = (PurpleCipher *)((void *)0);
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return (-1);
    };
  }while (0);
  cipher = (context -> cipher);
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return (-1);
    };
  }while (0);
  if (((cipher -> ops) != 0) && (( *(cipher -> ops)).encrypt != 0)) 
    return ( *( *(cipher -> ops)).encrypt)(context,data,len,output,outlen);
  else {
    purple_debug_warning("cipher","the %s cipher does not support the encryptoperation\n",(cipher -> name));
    if (outlen != 0) 
       *outlen = (-1);
    return (-1);
  }
}

gint purple_cipher_context_decrypt(PurpleCipherContext *context,const guchar data[],size_t len,guchar output[],size_t *outlen)
{
  PurpleCipher *cipher = (PurpleCipher *)((void *)0);
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return (-1);
    };
  }while (0);
  cipher = (context -> cipher);
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return (-1);
    };
  }while (0);
  if (((cipher -> ops) != 0) && (( *(cipher -> ops)).decrypt != 0)) 
    return ( *( *(cipher -> ops)).decrypt)(context,data,len,output,outlen);
  else {
    purple_debug_warning("cipher","the %s cipher does not support the decryptoperation\n",(cipher -> name));
    if (outlen != 0) 
       *outlen = (-1);
    return (-1);
  }
}

void purple_cipher_context_set_salt(PurpleCipherContext *context,guchar *salt)
{
  PurpleCipher *cipher = (PurpleCipher *)((void *)0);
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return ;
    };
  }while (0);
  cipher = (context -> cipher);
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return ;
    };
  }while (0);
  if (((cipher -> ops) != 0) && (( *(cipher -> ops)).set_salt != 0)) 
    ( *( *(cipher -> ops)).set_salt)(context,salt);
  else 
    purple_debug_warning("cipher","the %s cipher does not support the set_salt operation\n",(cipher -> name));
}

size_t purple_cipher_context_get_salt_size(PurpleCipherContext *context)
{
  PurpleCipher *cipher = (PurpleCipher *)((void *)0);
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return (-1);
    };
  }while (0);
  cipher = (context -> cipher);
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return (-1);
    };
  }while (0);
  if (((cipher -> ops) != 0) && (( *(cipher -> ops)).get_salt_size != 0)) 
    return ( *( *(cipher -> ops)).get_salt_size)(context);
  else {
    purple_debug_warning("cipher","the %s cipher does not support the get_salt_size operation\n",(cipher -> name));
    return (-1);
  }
}

void purple_cipher_context_set_key(PurpleCipherContext *context,const guchar *key)
{
  PurpleCipher *cipher = (PurpleCipher *)((void *)0);
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return ;
    };
  }while (0);
  cipher = (context -> cipher);
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return ;
    };
  }while (0);
  if (((cipher -> ops) != 0) && (( *(cipher -> ops)).set_key != 0)) 
    ( *( *(cipher -> ops)).set_key)(context,key);
  else 
    purple_debug_warning("cipher","the %s cipher does not support the set_key operation\n",(cipher -> name));
}

size_t purple_cipher_context_get_key_size(PurpleCipherContext *context)
{
  PurpleCipher *cipher = (PurpleCipher *)((void *)0);
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return (-1);
    };
  }while (0);
  cipher = (context -> cipher);
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return (-1);
    };
  }while (0);
  if (((cipher -> ops) != 0) && (( *(cipher -> ops)).get_key_size != 0)) 
    return ( *( *(cipher -> ops)).get_key_size)(context);
  else {
    purple_debug_warning("cipher","the %s cipher does not support the get_key_size operation\n",(cipher -> name));
    return (-1);
  }
}

void purple_cipher_context_set_batch_mode(PurpleCipherContext *context,PurpleCipherBatchMode mode)
{
  PurpleCipher *cipher = (PurpleCipher *)((void *)0);
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return ;
    };
  }while (0);
  cipher = (context -> cipher);
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return ;
    };
  }while (0);
  if (((cipher -> ops) != 0) && (( *(cipher -> ops)).set_batch_mode != 0)) 
    ( *( *(cipher -> ops)).set_batch_mode)(context,mode);
  else 
    purple_debug_warning("cipher","The %s cipher does not support the set_batch_mode operation\n",(cipher -> name));
}

PurpleCipherBatchMode purple_cipher_context_get_batch_mode(PurpleCipherContext *context)
{
  PurpleCipher *cipher = (PurpleCipher *)((void *)0);
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return (-1);
    };
  }while (0);
  cipher = (context -> cipher);
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return (-1);
    };
  }while (0);
  if (((cipher -> ops) != 0) && (( *(cipher -> ops)).get_batch_mode != 0)) 
    return ( *( *(cipher -> ops)).get_batch_mode)(context);
  else {
    purple_debug_warning("cipher","The %s cipher does not support the get_batch_mode operation\n",(cipher -> name));
    return (-1);
  }
}

size_t purple_cipher_context_get_block_size(PurpleCipherContext *context)
{
  PurpleCipher *cipher = (PurpleCipher *)((void *)0);
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return (-1);
    };
  }while (0);
  cipher = (context -> cipher);
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return (-1);
    };
  }while (0);
  if (((cipher -> ops) != 0) && (( *(cipher -> ops)).get_block_size != 0)) 
    return ( *( *(cipher -> ops)).get_block_size)(context);
  else {
    purple_debug_warning("cipher","The %s cipher does not support the get_block_size operation\n",(cipher -> name));
    return (-1);
  }
}

void purple_cipher_context_set_key_with_len(PurpleCipherContext *context,const guchar *key,size_t len)
{
  PurpleCipher *cipher = (PurpleCipher *)((void *)0);
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return ;
    };
  }while (0);
  cipher = (context -> cipher);
  do {
    if (cipher != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher");
      return ;
    };
  }while (0);
  if (((cipher -> ops) != 0) && (( *(cipher -> ops)).set_key_with_len != 0)) 
    ( *( *(cipher -> ops)).set_key_with_len)(context,key,len);
  else 
    purple_debug_warning("cipher","The %s cipher does not support the set_key_with_len operation\n",(cipher -> name));
}

void purple_cipher_context_set_data(PurpleCipherContext *context,gpointer data)
{
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return ;
    };
  }while (0);
  context -> data = data;
}

gpointer purple_cipher_context_get_data(PurpleCipherContext *context)
{
  do {
    if (context != 0) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"context");
      return 0;
    };
  }while (0);
  return context -> data;
}

gchar *purple_cipher_http_digest_calculate_session_key(const gchar *algorithm,const gchar *username,const gchar *realm,const gchar *password,const gchar *nonce,const gchar *client_nonce)
{
  PurpleCipher *cipher;
  PurpleCipherContext *context;
/* We only support MD5. */
  gchar hash[33UL];
  do {
    if (username != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"username != NULL");
      return 0;
    };
  }while (0);
  do {
    if (realm != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"realm != NULL");
      return 0;
    };
  }while (0);
  do {
    if (password != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"password != NULL");
      return 0;
    };
  }while (0);
  do {
    if (nonce != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"nonce != NULL");
      return 0;
    };
  }while (0);
/* Check for a supported algorithm. */
  do {
    if ((((algorithm == ((const gchar *)((void *)0))) || (( *algorithm) == 0)) || (g_ascii_strcasecmp(algorithm,"MD5") != 0)) || (g_ascii_strcasecmp(algorithm,"MD5-sess") != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"algorithm == NULL || *algorithm == \'\\0\' || g_ascii_strcasecmp(algorithm, \"MD5\") || g_ascii_strcasecmp(algorithm, \"MD5-sess\")");
      return 0;
    };
  }while (0);
  cipher = purple_ciphers_find_cipher("md5");
  do {
    if (cipher != ((PurpleCipher *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher != NULL");
      return 0;
    };
  }while (0);
  context = purple_cipher_context_new(cipher,0);
  purple_cipher_context_append(context,((guchar *)username),strlen(username));
  purple_cipher_context_append(context,((guchar *)":"),1);
  purple_cipher_context_append(context,((guchar *)realm),strlen(realm));
  purple_cipher_context_append(context,((guchar *)":"),1);
  purple_cipher_context_append(context,((guchar *)password),strlen(password));
  if ((algorithm != ((const gchar *)((void *)0))) && !(g_ascii_strcasecmp(algorithm,"MD5-sess") != 0)) {
    guchar digest[16UL];
    if (client_nonce == ((const gchar *)((void *)0))) {
      purple_cipher_context_destroy(context);
      purple_debug_error("cipher","Required client_nonce missing for MD5-sess digest calculation.\n");
      return 0;
    }
    purple_cipher_context_digest(context,(sizeof(digest)),digest,0);
    purple_cipher_context_destroy(context);
    context = purple_cipher_context_new(cipher,0);
    purple_cipher_context_append(context,digest,(sizeof(digest)));
    purple_cipher_context_append(context,((guchar *)":"),1);
    purple_cipher_context_append(context,((guchar *)nonce),strlen(nonce));
    purple_cipher_context_append(context,((guchar *)":"),1);
    purple_cipher_context_append(context,((guchar *)client_nonce),strlen(client_nonce));
  }
  purple_cipher_context_digest_to_str(context,(sizeof(hash)),hash,0);
  purple_cipher_context_destroy(context);
  return g_strdup(hash);
}

gchar *purple_cipher_http_digest_calculate_response(const gchar *algorithm,const gchar *method,const gchar *digest_uri,const gchar *qop,const gchar *entity,const gchar *nonce,const gchar *nonce_count,const gchar *client_nonce,const gchar *session_key)
{
  PurpleCipher *cipher;
  PurpleCipherContext *context;
/* We only support MD5. */
  static gchar hash2[33UL];
  do {
    if (method != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"method != NULL");
      return 0;
    };
  }while (0);
  do {
    if (digest_uri != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"digest_uri != NULL");
      return 0;
    };
  }while (0);
  do {
    if (nonce != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"nonce != NULL");
      return 0;
    };
  }while (0);
  do {
    if (session_key != ((const gchar *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"session_key != NULL");
      return 0;
    };
  }while (0);
/* Check for a supported algorithm. */
  do {
    if ((((algorithm == ((const gchar *)((void *)0))) || (( *algorithm) == 0)) || (g_ascii_strcasecmp(algorithm,"MD5") != 0)) || (g_ascii_strcasecmp(algorithm,"MD5-sess") != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"algorithm == NULL || *algorithm == \'\\0\' || g_ascii_strcasecmp(algorithm, \"MD5\") || g_ascii_strcasecmp(algorithm, \"MD5-sess\")");
      return 0;
    };
  }while (0);
/* Check for a supported "quality of protection". */
  do {
    if ((((qop == ((const gchar *)((void *)0))) || (( *qop) == 0)) || (g_ascii_strcasecmp(qop,"auth") != 0)) || (g_ascii_strcasecmp(qop,"auth-int") != 0)) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"qop == NULL || *qop == \'\\0\' || g_ascii_strcasecmp(qop, \"auth\") || g_ascii_strcasecmp(qop, \"auth-int\")");
      return 0;
    };
  }while (0);
  cipher = purple_ciphers_find_cipher("md5");
  do {
    if (cipher != ((PurpleCipher *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"cipher != NULL");
      return 0;
    };
  }while (0);
  context = purple_cipher_context_new(cipher,0);
  purple_cipher_context_append(context,((guchar *)method),strlen(method));
  purple_cipher_context_append(context,((guchar *)":"),1);
  purple_cipher_context_append(context,((guchar *)digest_uri),strlen(digest_uri));
  if ((qop != ((const gchar *)((void *)0))) && !(g_ascii_strcasecmp(qop,"auth-int") != 0)) {
    PurpleCipherContext *context2;
    gchar entity_hash[33UL];
    if (entity == ((const gchar *)((void *)0))) {
      purple_cipher_context_destroy(context);
      purple_debug_error("cipher","Required entity missing for auth-int digest calculation.\n");
      return 0;
    }
    context2 = purple_cipher_context_new(cipher,0);
    purple_cipher_context_append(context2,((guchar *)entity),strlen(entity));
    purple_cipher_context_digest_to_str(context2,(sizeof(entity_hash)),entity_hash,0);
    purple_cipher_context_destroy(context2);
    purple_cipher_context_append(context,((guchar *)":"),1);
    purple_cipher_context_append(context,((guchar *)entity_hash),strlen(entity_hash));
  }
  purple_cipher_context_digest_to_str(context,(sizeof(hash2)),hash2,0);
  purple_cipher_context_destroy(context);
  context = purple_cipher_context_new(cipher,0);
  purple_cipher_context_append(context,((guchar *)session_key),strlen(session_key));
  purple_cipher_context_append(context,((guchar *)":"),1);
  purple_cipher_context_append(context,((guchar *)nonce),strlen(nonce));
  purple_cipher_context_append(context,((guchar *)":"),1);
  if ((qop != ((const gchar *)((void *)0))) && (( *qop) != 0)) {
    if (nonce_count == ((const gchar *)((void *)0))) {
      purple_cipher_context_destroy(context);
      purple_debug_error("cipher","Required nonce_count missing for digest calculation.\n");
      return 0;
    }
    if (client_nonce == ((const gchar *)((void *)0))) {
      purple_cipher_context_destroy(context);
      purple_debug_error("cipher","Required client_nonce missing for digest calculation.\n");
      return 0;
    }
    purple_cipher_context_append(context,((guchar *)nonce_count),strlen(nonce_count));
    purple_cipher_context_append(context,((guchar *)":"),1);
    purple_cipher_context_append(context,((guchar *)client_nonce),strlen(client_nonce));
    purple_cipher_context_append(context,((guchar *)":"),1);
    purple_cipher_context_append(context,((guchar *)qop),strlen(qop));
    purple_cipher_context_append(context,((guchar *)":"),1);
  }
  purple_cipher_context_append(context,((guchar *)hash2),strlen(hash2));
  purple_cipher_context_digest_to_str(context,(sizeof(hash2)),hash2,0);
  purple_cipher_context_destroy(context);
  return g_strdup(hash2);
}
