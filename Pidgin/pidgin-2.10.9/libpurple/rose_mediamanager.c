/**
 * @file mediamanager.c Media Manager API
 * @ingroup core
 */
/* purple
 *
 * Purple is the legal property of its developers, whose names are too numerous
 * to list here.  Please refer to the COPYRIGHT file distributed with this
 * source distribution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1301  USA
 */
#include "internal.h"
#include "account.h"
#include "debug.h"
#include "media.h"
#include "mediamanager.h"
#ifdef USE_GSTREAMER
#include "marshallers.h"
#include "media-gst.h"
#endif
#ifdef USE_VV
#include <media/backend-fs2.h>
#ifdef HAVE_FARSIGHT
#include <gst/farsight/fs-element-added-notifier.h>
#else
#include <farstream/fs-element-added-notifier.h>
#endif
#include <gst/interfaces/xoverlay.h>
/** @copydoc _PurpleMediaManagerPrivate */
typedef struct _PurpleMediaManagerPrivate PurpleMediaManagerPrivate;
/** @copydoc _PurpleMediaOutputWindow */
typedef struct _PurpleMediaOutputWindow PurpleMediaOutputWindow;
/** @copydoc _PurpleMediaManagerPrivate */
typedef struct _PurpleMediaElementInfoPrivate PurpleMediaElementInfoPrivate;
/** The media manager class. */

struct _PurpleMediaManagerClass 
{
/**< The parent class. */
  GObjectClass parent_class;
}
;
/** The media manager's data. */

struct _PurpleMediaManager 
{
/**< The parent of this manager. */
  GObject parent;
/**< Private data for the manager. */
  PurpleMediaManagerPrivate *priv;
}
;

struct _PurpleMediaOutputWindow 
{
  gulong id;
  PurpleMedia *media;
  gchar *session_id;
  gchar *participant;
  gulong window_id;
  GstElement *sink;
}
;

struct _PurpleMediaManagerPrivate 
{
  GstElement *pipeline;
  PurpleMediaCaps ui_caps;
  GList *medias;
  GList *elements;
  GList *output_windows;
  gulong next_output_window_id;
  GType backend_type;
  GstCaps *video_caps;
  PurpleMediaElementInfo *video_src;
  PurpleMediaElementInfo *video_sink;
  PurpleMediaElementInfo *audio_src;
  PurpleMediaElementInfo *audio_sink;
}
;
#define PURPLE_MEDIA_MANAGER_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE((obj), PURPLE_TYPE_MEDIA_MANAGER, PurpleMediaManagerPrivate))
#define PURPLE_MEDIA_ELEMENT_INFO_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE((obj), PURPLE_TYPE_MEDIA_ELEMENT_INFO, PurpleMediaElementInfoPrivate))
static void purple_media_manager_class_init(PurpleMediaManagerClass *klass);
static void purple_media_manager_init(PurpleMediaManager *media);
static void purple_media_manager_finalize(GObject *object);
static GObjectClass *parent_class = (GObjectClass *)((void *)0);
enum __unnamed_enum___F0_L107_C1_INIT_MEDIA__COMMA__UI_CAPS_CHANGED__COMMA__LAST_SIGNAL {INIT_MEDIA,UI_CAPS_CHANGED,LAST_SIGNAL};
static guint purple_media_manager_signals[2UL] = {(0)};
#endif

GType purple_media_manager_get_type()
{
#ifdef USE_VV
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {((sizeof(PurpleMediaManagerClass ))), ((GBaseInitFunc )((void *)0)), ((GBaseFinalizeFunc )((void *)0)), ((GClassInitFunc )purple_media_manager_class_init), ((GClassFinalizeFunc )((void *)0)), ((gconstpointer )((void *)0)), ((sizeof(PurpleMediaManager ))), (0), ((GInstanceInitFunc )purple_media_manager_init), ((const GTypeValueTable *)((void *)0))};
    type = g_type_register_static(((GType )(20 << 2)),"PurpleMediaManager",&info,0);
  }
  return type;
#else
#endif
}
#ifdef USE_VV

static void purple_media_manager_class_init(PurpleMediaManagerClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *)klass;
  parent_class = (g_type_class_peek_parent(klass));
  gobject_class -> finalize = purple_media_manager_finalize;
  purple_media_manager_signals[INIT_MEDIA] = g_signal_new("init-media",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,0,0,0,purple_smarshal_BOOLEAN__OBJECT_POINTER_STRING,((GType )(5 << 2)),3,purple_media_get_type(),((GType )(17 << 2)),((GType )(16 << 2)));
  purple_media_manager_signals[UI_CAPS_CHANGED] = g_signal_new("ui-caps-changed",( *((GTypeClass *)klass)).g_type,G_SIGNAL_RUN_LAST,0,0,0,purple_smarshal_VOID__FLAGS_FLAGS,((GType )(1 << 2)),2,purple_media_caps_get_type(),purple_media_caps_get_type());
  g_type_class_add_private(klass,(sizeof(PurpleMediaManagerPrivate )));
}

static void purple_media_manager_init(PurpleMediaManager *media)
{
  media -> priv = ((PurpleMediaManagerPrivate *)(g_type_instance_get_private(((GTypeInstance *)media),purple_media_manager_get_type())));
  ( *(media -> priv)).medias = ((GList *)((void *)0));
  ( *(media -> priv)).next_output_window_id = 1;
#ifdef USE_VV
  ( *(media -> priv)).backend_type = purple_media_backend_fs2_get_type();
#endif
  purple_prefs_add_none("/purple/media");
  purple_prefs_add_none("/purple/media/audio");
  purple_prefs_add_int("/purple/media/audio/silence_threshold",5);
  purple_prefs_add_none("/purple/media/audio/volume");
  purple_prefs_add_int("/purple/media/audio/volume/input",10);
  purple_prefs_add_int("/purple/media/audio/volume/output",10);
}

static void purple_media_manager_finalize(GObject *media)
{
  PurpleMediaManagerPrivate *priv = (PurpleMediaManagerPrivate *)(g_type_instance_get_private(((GTypeInstance *)media),purple_media_manager_get_type()));
  for (; (priv -> medias) != 0; priv -> medias = g_list_delete_link((priv -> medias),(priv -> medias))) {
    g_object_unref(( *(priv -> medias)).data);
  }
  for (; (priv -> elements) != 0; priv -> elements = g_list_delete_link((priv -> elements),(priv -> elements))) {
    g_object_unref(( *(priv -> elements)).data);
  }
  if ((priv -> video_caps) != 0) 
    gst_caps_unref((priv -> video_caps));
  ( *(parent_class -> finalize))(media);
}
#endif

PurpleMediaManager *purple_media_manager_get()
{
#ifdef USE_VV
  static PurpleMediaManager *manager = (PurpleMediaManager *)((void *)0);
  if (manager == ((PurpleMediaManager *)((void *)0))) 
    manager = ((PurpleMediaManager *)(g_type_check_instance_cast(((GTypeInstance *)(g_object_new(purple_media_manager_get_type(),0))),purple_media_manager_get_type())));
  return manager;
#else
#endif
}
#ifdef USE_VV

static gboolean pipeline_bus_call(GstBus *bus,GstMessage *msg,PurpleMediaManager *manager)
{
{
    switch(( *((GstMessage *)msg)).type){
      case GST_MESSAGE_EOS:
{
        purple_debug_info("mediamanager","End of Stream\n");
        break; 
      }
      case GST_MESSAGE_ERROR:
{
{
          gchar *debug = (gchar *)((void *)0);
          GError *err = (GError *)((void *)0);
          gst_message_parse_error(msg,&err,&debug);
          purple_debug_error("mediamanager","gst pipeline error: %s\n",(err -> message));
          g_error_free(err);
          if (debug != 0) {
            purple_debug_error("mediamanager","Debug details: %s\n",debug);
            g_free(debug);
          }
          break; 
        }
      }
      default:
{
        break; 
      }
    }
  }
  return (!0);
}
#endif
#ifdef USE_GSTREAMER

GstElement *purple_media_manager_get_pipeline(PurpleMediaManager *manager)
{
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)manager;
      GType __t = purple_media_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_MANAGER(manager)");
      return 0;
    };
  }while (0);
  if (( *(manager -> priv)).pipeline == ((GstElement *)((void *)0))) {
    FsElementAddedNotifier *notifier;
    gchar *filename;
    GError *err = (GError *)((void *)0);
    GKeyFile *keyfile;
    GstBus *bus;
    ( *(manager -> priv)).pipeline = gst_pipeline_new(0);
    bus = gst_pipeline_get_bus(((GstPipeline *)(g_type_check_instance_cast(((GTypeInstance *)( *(manager -> priv)).pipeline),gst_pipeline_get_type()))));
    gst_bus_add_signal_watch(((GstBus *)(g_type_check_instance_cast(((GTypeInstance *)bus),gst_bus_get_type()))));
    g_signal_connect_data(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)bus),((GType )(20 << 2))))),"message",((GCallback )pipeline_bus_call),manager,0,((GConnectFlags )0));
    gst_bus_set_sync_handler(bus,gst_bus_sync_signal_handler,0);
    gst_object_unref(bus);
    filename = g_build_filename(purple_user_dir(),"fs-element.conf",((void *)((void *)0)));
    keyfile = g_key_file_new();
    if (!(g_key_file_load_from_file(keyfile,filename,G_KEY_FILE_NONE,&err) != 0)) {
      if ((err -> code) == 4) 
        purple_debug_info("mediamanager","Couldn\'t read fs-element.conf: %s\n",(err -> message));
      else 
        purple_debug_error("mediamanager","Error reading fs-element.conf: %s\n",(err -> message));
      g_error_free(err);
    }
    g_free(filename);
/* Hack to make alsasrc stop messing up audio timestamps */
    if (!(g_key_file_has_key(keyfile,"alsasrc","slave-method",0) != 0)) {
      g_key_file_set_integer(keyfile,"alsasrc","slave-method",2);
    }
    notifier = fs_element_added_notifier_new();
    fs_element_added_notifier_add(notifier,((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)( *(manager -> priv)).pipeline),gst_bin_get_type()))));
    fs_element_added_notifier_set_properties_from_keyfile(notifier,keyfile);
    gst_element_set_state(( *(manager -> priv)).pipeline,GST_STATE_PLAYING);
  }
  return ( *(manager -> priv)).pipeline;
#else
#endif
}
#endif /* USE_GSTREAMER */

PurpleMedia *purple_media_manager_create_media(PurpleMediaManager *manager,PurpleAccount *account,const char *conference_type,const char *remote_user,gboolean initiator)
{
#ifdef USE_VV
  PurpleMedia *media;
  gboolean signal_ret;
  media = ((PurpleMedia *)(g_type_check_instance_cast(((GTypeInstance *)(g_object_new(purple_media_get_type(),"manager",manager,"account",account,"conference-type",conference_type,"initiator",initiator,((void *)((void *)0))))),purple_media_get_type())));
  g_signal_emit(manager,purple_media_manager_signals[INIT_MEDIA],0,media,account,remote_user,&signal_ret);
  if (signal_ret == 0) {
    g_object_unref(media);
    return 0;
  }
  ( *(manager -> priv)).medias = g_list_append(( *(manager -> priv)).medias,media);
  return media;
#else
#endif
}

GList *purple_media_manager_get_media(PurpleMediaManager *manager)
{
#ifdef USE_VV
  return ( *(manager -> priv)).medias;
#else
#endif
}

GList *purple_media_manager_get_media_by_account(PurpleMediaManager *manager,PurpleAccount *account)
{
#ifdef USE_VV
  GList *media = (GList *)((void *)0);
  GList *iter;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)manager;
      GType __t = purple_media_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_MANAGER(manager)");
      return 0;
    };
  }while (0);
  iter = ( *(manager -> priv)).medias;
  for (; iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
    if (purple_media_get_account((iter -> data)) == account) {
      media = g_list_prepend(media,(iter -> data));
    }
  }
  return media;
#else
#endif
}

void purple_media_manager_remove_media(PurpleMediaManager *manager,PurpleMedia *media)
{
#ifdef USE_VV
  GList *list = g_list_find(( *(manager -> priv)).medias,media);
  if (list != 0) 
    ( *(manager -> priv)).medias = g_list_delete_link(( *(manager -> priv)).medias,list);
#endif
}
#ifdef USE_VV

static void request_pad_unlinked_cb(GstPad *pad,GstPad *peer,gpointer user_data)
{
  GstElement *parent = (GstElement *)( *((GstObject *)pad)).parent;
  GstIterator *iter;
  GstPad *remaining_pad;
  GstIteratorResult result;
  gst_element_release_request_pad(((GstElement *)( *((GstObject *)pad)).parent),pad);
  iter = gst_element_iterate_src_pads(parent);
  result = gst_iterator_next(iter,((gpointer )(&remaining_pad)));
  if (result == GST_ITERATOR_DONE) {
    gst_element_set_locked_state(parent,(!0));
    gst_element_set_state(parent,GST_STATE_NULL);
    gst_bin_remove(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)((GstElement *)( *((GstObject *)parent)).parent)),gst_bin_get_type()))),parent);
  }
  else if (result == GST_ITERATOR_OK) {
    gst_object_unref(remaining_pad);
  }
  gst_iterator_free(iter);
}
#endif
#ifdef USE_GSTREAMER

void purple_media_manager_set_video_caps(PurpleMediaManager *manager,GstCaps *caps)
{
#ifdef USE_VV
  if (( *(manager -> priv)).video_caps != 0) 
    gst_caps_unref(( *(manager -> priv)).video_caps);
  ( *(manager -> priv)).video_caps = caps;
  if ((( *(manager -> priv)).pipeline != 0) && (( *(manager -> priv)).video_src != 0)) {
    gchar *id = purple_media_element_info_get_id(( *(manager -> priv)).video_src);
    GstElement *src = gst_bin_get_by_name(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)( *(manager -> priv)).pipeline),gst_bin_get_type()))),id);
    if (src != 0) {
      GstElement *capsfilter = gst_bin_get_by_name(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)src),gst_bin_get_type()))),"prpl_video_caps");
      g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)capsfilter),((GType )(20 << 2))))),"caps",caps,((void *)((void *)0)));
    }
    g_free(id);
  }
#endif
}

GstCaps *purple_media_manager_get_video_caps(PurpleMediaManager *manager)
{
#ifdef USE_VV
  if (( *(manager -> priv)).video_caps == ((GstCaps *)((void *)0))) 
    ( *(manager -> priv)).video_caps = gst_caps_from_string("video/x-raw-yuv,width=[250,352], height=[200,288], framerate=[1/1,20/1]");
  return ( *(manager -> priv)).video_caps;
#else
#endif
}

GstElement *purple_media_manager_get_element(PurpleMediaManager *manager,PurpleMediaSessionType type,PurpleMedia *media,const gchar *session_id,const gchar *participant)
{
#ifdef USE_VV
  GstElement *ret = (GstElement *)((void *)0);
  PurpleMediaElementInfo *info = (PurpleMediaElementInfo *)((void *)0);
  PurpleMediaElementType element_type;
  if ((type & PURPLE_MEDIA_SEND_AUDIO) != 0U) 
    info = ( *(manager -> priv)).audio_src;
  else if ((type & PURPLE_MEDIA_RECV_AUDIO) != 0U) 
    info = ( *(manager -> priv)).audio_sink;
  else if ((type & PURPLE_MEDIA_SEND_VIDEO) != 0U) 
    info = ( *(manager -> priv)).video_src;
  else if ((type & PURPLE_MEDIA_RECV_VIDEO) != 0U) 
    info = ( *(manager -> priv)).video_sink;
  if (info == ((PurpleMediaElementInfo *)((void *)0))) 
    return 0;
  element_type = purple_media_element_info_get_element_type(info);
  if (((element_type & PURPLE_MEDIA_ELEMENT_UNIQUE) != 0U) && ((element_type & PURPLE_MEDIA_ELEMENT_SRC) != 0U)) {
    GstElement *tee;
    GstPad *pad;
    GstPad *ghost;
    gchar *id = purple_media_element_info_get_id(info);
    ret = gst_bin_get_by_name(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)(purple_media_manager_get_pipeline(manager))),gst_bin_get_type()))),id);
    if (ret == ((GstElement *)((void *)0))) {
      GstElement *bin;
      GstElement *fakesink;
      ret = purple_media_element_info_call_create(info,media,session_id,participant);
      bin = gst_bin_new(id);
      tee = gst_element_factory_make("tee","tee");
      gst_bin_add_many(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)bin),gst_bin_get_type()))),ret,tee,((void *)((void *)0)));
      if ((type & PURPLE_MEDIA_SEND_VIDEO) != 0U) {
        GstElement *videoscale;
        GstElement *capsfilter;
        videoscale = gst_element_factory_make("videoscale",0);
        capsfilter = gst_element_factory_make("capsfilter","prpl_video_caps");
        g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)capsfilter),((GType )(20 << 2))))),"caps",purple_media_manager_get_video_caps(manager),((void *)((void *)0)));
        gst_bin_add_many(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)bin),gst_bin_get_type()))),videoscale,capsfilter,((void *)((void *)0)));
        gst_element_link_many(ret,videoscale,capsfilter,tee,((void *)((void *)0)));
      }
      else 
        gst_element_link(ret,tee);
/*
			 * This shouldn't be necessary, but it stops it from
			 * giving a not-linked error upon destruction
			 */
      fakesink = gst_element_factory_make("fakesink",0);
      g_object_set(fakesink,"sync",0,((void *)((void *)0)));
      gst_bin_add(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)bin),gst_bin_get_type()))),fakesink);
      gst_element_link(tee,fakesink);
      ret = bin;
      gst_object_ref(ret);
      gst_bin_add(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)(purple_media_manager_get_pipeline(manager))),gst_bin_get_type()))),ret);
    }
    g_free(id);
    tee = gst_bin_get_by_name(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)ret),gst_bin_get_type()))),"tee");
    pad = gst_element_get_request_pad(tee,"src%d");
    gst_object_unref(tee);
    ghost = gst_ghost_pad_new(0,pad);
    gst_object_unref(pad);
    g_signal_connect_data(((GstPad *)(g_type_check_instance_cast(((GTypeInstance *)ghost),gst_pad_get_type()))),"unlinked",((GCallback )request_pad_unlinked_cb),0,0,((GConnectFlags )0));
    gst_pad_set_active(ghost,(!0));
    gst_element_add_pad(ret,ghost);
  }
  else {
    ret = purple_media_element_info_call_create(info,media,session_id,participant);
  }
  if (ret == ((GstElement *)((void *)0))) 
    purple_debug_error("media","Error creating source or sink\n");
  return ret;
#else
#endif
}

PurpleMediaElementInfo *purple_media_manager_get_element_info(PurpleMediaManager *manager,const gchar *id)
{
#ifdef USE_VV
  GList *iter;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)manager;
      GType __t = purple_media_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_MANAGER(manager)");
      return 0;
    };
  }while (0);
  iter = ( *(manager -> priv)).elements;
  for (; iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
    gchar *element_id = purple_media_element_info_get_id((iter -> data));
    if (!(strcmp(element_id,id) != 0)) {
      g_free(element_id);
      g_object_ref((iter -> data));
      return (iter -> data);
    }
    g_free(element_id);
  }
#endif
  return 0;
}

gboolean purple_media_manager_register_element(PurpleMediaManager *manager,PurpleMediaElementInfo *info)
{
#ifdef USE_VV
  PurpleMediaElementInfo *info2;
  gchar *id;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)manager;
      GType __t = purple_media_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_MANAGER(manager)");
      return 0;
    };
  }while (0);
  do {
    if (info != ((PurpleMediaElementInfo *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"info != NULL");
      return 0;
    };
  }while (0);
  id = purple_media_element_info_get_id(info);
  info2 = purple_media_manager_get_element_info(manager,id);
  g_free(id);
  if (info2 != ((PurpleMediaElementInfo *)((void *)0))) {
    g_object_unref(info2);
    return 0;
  }
  ( *(manager -> priv)).elements = g_list_prepend(( *(manager -> priv)).elements,info);
  return (!0);
#else
#endif
}

gboolean purple_media_manager_unregister_element(PurpleMediaManager *manager,const gchar *id)
{
#ifdef USE_VV
  PurpleMediaElementInfo *info;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)manager;
      GType __t = purple_media_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_MANAGER(manager)");
      return 0;
    };
  }while (0);
  info = purple_media_manager_get_element_info(manager,id);
  if (info == ((PurpleMediaElementInfo *)((void *)0))) {
    g_object_unref(info);
    return 0;
  }
  if (( *(manager -> priv)).audio_src == info) 
    ( *(manager -> priv)).audio_src = ((PurpleMediaElementInfo *)((void *)0));
  if (( *(manager -> priv)).audio_sink == info) 
    ( *(manager -> priv)).audio_sink = ((PurpleMediaElementInfo *)((void *)0));
  if (( *(manager -> priv)).video_src == info) 
    ( *(manager -> priv)).video_src = ((PurpleMediaElementInfo *)((void *)0));
  if (( *(manager -> priv)).video_sink == info) 
    ( *(manager -> priv)).video_sink = ((PurpleMediaElementInfo *)((void *)0));
  ( *(manager -> priv)).elements = g_list_remove(( *(manager -> priv)).elements,info);
  g_object_unref(info);
  return (!0);
#else
#endif
}

gboolean purple_media_manager_set_active_element(PurpleMediaManager *manager,PurpleMediaElementInfo *info)
{
#ifdef USE_VV
  PurpleMediaElementInfo *info2;
  PurpleMediaElementType type;
  gboolean ret = 0;
  gchar *id;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)manager;
      GType __t = purple_media_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_MANAGER(manager)");
      return 0;
    };
  }while (0);
  do {
    if (info != ((PurpleMediaElementInfo *)((void *)0))) {
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"info != NULL");
      return 0;
    };
  }while (0);
  id = purple_media_element_info_get_id(info);
  info2 = purple_media_manager_get_element_info(manager,id);
  g_free(id);
  if (info2 == ((PurpleMediaElementInfo *)((void *)0))) 
    purple_media_manager_register_element(manager,info);
  else 
    g_object_unref(info2);
  type = purple_media_element_info_get_element_type(info);
  if ((type & PURPLE_MEDIA_ELEMENT_SRC) != 0U) {
    if ((type & PURPLE_MEDIA_ELEMENT_AUDIO) != 0U) {
      ( *(manager -> priv)).audio_src = info;
      ret = (!0);
    }
    if ((type & PURPLE_MEDIA_ELEMENT_VIDEO) != 0U) {
      ( *(manager -> priv)).video_src = info;
      ret = (!0);
    }
  }
  if ((type & PURPLE_MEDIA_ELEMENT_SINK) != 0U) {
    if ((type & PURPLE_MEDIA_ELEMENT_AUDIO) != 0U) {
      ( *(manager -> priv)).audio_sink = info;
      ret = (!0);
    }
    if ((type & PURPLE_MEDIA_ELEMENT_VIDEO) != 0U) {
      ( *(manager -> priv)).video_sink = info;
      ret = (!0);
    }
  }
  return ret;
#else
#endif
}

PurpleMediaElementInfo *purple_media_manager_get_active_element(PurpleMediaManager *manager,PurpleMediaElementType type)
{
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)manager;
      GType __t = purple_media_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_MANAGER(manager)");
      return 0;
    };
  }while (0);
  if ((type & PURPLE_MEDIA_ELEMENT_SRC) != 0U) {
    if ((type & PURPLE_MEDIA_ELEMENT_AUDIO) != 0U) 
      return ( *(manager -> priv)).audio_src;
    else if ((type & PURPLE_MEDIA_ELEMENT_VIDEO) != 0U) 
      return ( *(manager -> priv)).video_src;
  }
  else if ((type & PURPLE_MEDIA_ELEMENT_SINK) != 0U) {
    if ((type & PURPLE_MEDIA_ELEMENT_AUDIO) != 0U) 
      return ( *(manager -> priv)).audio_sink;
    else if ((type & PURPLE_MEDIA_ELEMENT_VIDEO) != 0U) 
      return ( *(manager -> priv)).video_sink;
  }
#endif
  return 0;
}
#endif /* USE_GSTREAMER */
#ifdef USE_VV

static void window_id_cb(GstBus *bus,GstMessage *msg,PurpleMediaOutputWindow *ow)
{
  GstElement *sink;
  if ((( *((GstMessage *)msg)).type != GST_MESSAGE_ELEMENT) || !(gst_structure_has_name((msg -> structure),"prepare-xwindow-id") != 0)) 
    return ;
  sink = ((GstElement *)(g_type_check_instance_cast(((GTypeInstance *)( *((GstMessage *)msg)).src),gst_element_get_type())));
  while(sink != (ow -> sink)){
    if (sink == ((GstElement *)((void *)0))) 
      return ;
    sink = ((GstElement *)( *((GstObject *)sink)).parent);
  }
  g_signal_handlers_disconnect_matched(bus,(G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA),0,0,0,window_id_cb,ow);
  gst_x_overlay_set_xwindow_id(((GstXOverlay *)(gst_implements_interface_cast(( *((GstMessage *)msg)).src,gst_x_overlay_get_type()))),(ow -> window_id));
}
#endif

gboolean purple_media_manager_create_output_window(PurpleMediaManager *manager,PurpleMedia *media,const gchar *session_id,const gchar *participant)
{
#ifdef USE_VV
  GList *iter;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  iter = ( *(manager -> priv)).output_windows;
  for (; iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {{
      PurpleMediaOutputWindow *ow = (iter -> data);
      if (((((ow -> sink) == ((GstElement *)((void *)0))) && ((ow -> media) == media)) && ((((participant != ((const gchar *)((void *)0))) && ((ow -> participant) != ((gchar *)((void *)0)))) && !(strcmp(participant,(ow -> participant)) != 0)) || (participant == (ow -> participant)))) && !(strcmp(session_id,(ow -> session_id)) != 0)) {
        GstBus *bus;
        GstElement *queue;
        GstElement *colorspace;
        GstElement *tee = purple_media_get_tee(media,session_id,participant);
        if (tee == ((GstElement *)((void *)0))) 
          continue; 
        queue = gst_element_factory_make("queue",0);
        colorspace = gst_element_factory_make("ffmpegcolorspace",0);
        ow -> sink = purple_media_manager_get_element(manager,PURPLE_MEDIA_RECV_VIDEO,(ow -> media),(ow -> session_id),(ow -> participant));
        if (participant == ((const gchar *)((void *)0))) {
/* aka this is a preview sink */
          GObjectClass *klass = (GObjectClass *)( *((GTypeInstance *)(ow -> sink))).g_class;
          if (g_object_class_find_property(klass,"sync") != 0) 
            g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ow -> sink)),((GType )(20 << 2))))),"sync","FALSE",((void *)((void *)0)));
          if (g_object_class_find_property(klass,"async") != 0) 
            g_object_set(((GObject *)(g_type_check_instance_cast(((GTypeInstance *)(ow -> sink)),((GType )(20 << 2))))),"async",0,((void *)((void *)0)));
        }
        gst_bin_add_many(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)((GstElement *)( *((GstObject *)tee)).parent)),gst_bin_get_type()))),queue,colorspace,(ow -> sink),((void *)((void *)0)));
        bus = gst_pipeline_get_bus(((GstPipeline *)(g_type_check_instance_cast(((GTypeInstance *)( *(manager -> priv)).pipeline),gst_pipeline_get_type()))));
        g_signal_connect_data(bus,"sync-message::element",((GCallback )window_id_cb),ow,0,((GConnectFlags )0));
        gst_object_unref(bus);
        gst_element_set_state((ow -> sink),GST_STATE_PLAYING);
        gst_element_set_state(colorspace,GST_STATE_PLAYING);
        gst_element_set_state(queue,GST_STATE_PLAYING);
        gst_element_link(colorspace,(ow -> sink));
        gst_element_link(queue,colorspace);
        gst_element_link(tee,queue);
      }
    }
  }
  return (!0);
#else
#endif
}

gulong purple_media_manager_set_output_window(PurpleMediaManager *manager,PurpleMedia *media,const gchar *session_id,const gchar *participant,gulong window_id)
{
#ifdef USE_VV
  PurpleMediaOutputWindow *output_window;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)manager;
      GType __t = purple_media_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_MANAGER(manager)");
      return 0;
    };
  }while (0);
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return 0;
    };
  }while (0);
  output_window = ((PurpleMediaOutputWindow *)(g_malloc0_n(1,(sizeof(PurpleMediaOutputWindow )))));
  output_window -> id = ( *(manager -> priv)).next_output_window_id++;
  output_window -> media = media;
  output_window -> session_id = g_strdup(session_id);
  output_window -> participant = g_strdup(participant);
  output_window -> window_id = window_id;
  ( *(manager -> priv)).output_windows = g_list_prepend(( *(manager -> priv)).output_windows,output_window);
  if (purple_media_get_tee(media,session_id,participant) != ((GstElement *)((void *)0))) 
    purple_media_manager_create_output_window(manager,media,session_id,participant);
  return output_window -> id;
#else
#endif
}

gboolean purple_media_manager_remove_output_window(PurpleMediaManager *manager,gulong output_window_id)
{
#ifdef USE_VV
  PurpleMediaOutputWindow *output_window = (PurpleMediaOutputWindow *)((void *)0);
  GList *iter;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)manager;
      GType __t = purple_media_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_MANAGER(manager)");
      return 0;
    };
  }while (0);
  iter = ( *(manager -> priv)).output_windows;
{
    for (; iter != 0; iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)))) {
      PurpleMediaOutputWindow *ow = (iter -> data);
      if ((ow -> id) == output_window_id) {
        ( *(manager -> priv)).output_windows = g_list_delete_link(( *(manager -> priv)).output_windows,iter);
        output_window = ow;
        break; 
      }
    }
  }
  if (output_window == ((PurpleMediaOutputWindow *)((void *)0))) 
    return 0;
  if ((output_window -> sink) != ((GstElement *)((void *)0))) {
    GstPad *pad = gst_element_get_static_pad((output_window -> sink),"sink");
    GstPad *peer = gst_pad_get_peer(pad);
    GstElement *colorspace = (GstElement *)( *((GstObject *)peer)).parent;
    GstElement *queue;
    gst_object_unref(pad);
    gst_object_unref(peer);
    pad = gst_element_get_static_pad(colorspace,"sink");
    peer = gst_pad_get_peer(pad);
    queue = ((GstElement *)( *((GstObject *)peer)).parent);
    gst_object_unref(pad);
    gst_object_unref(peer);
    pad = gst_element_get_static_pad(queue,"sink");
    peer = gst_pad_get_peer(pad);
    gst_object_unref(pad);
    if (peer != ((GstPad *)((void *)0))) 
      gst_element_release_request_pad(((GstElement *)( *((GstObject *)peer)).parent),peer);
    gst_element_set_locked_state(queue,(!0));
    gst_element_set_state(queue,GST_STATE_NULL);
    gst_bin_remove(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)((GstElement *)( *((GstObject *)queue)).parent)),gst_bin_get_type()))),queue);
    gst_element_set_locked_state(colorspace,(!0));
    gst_element_set_state(colorspace,GST_STATE_NULL);
    gst_bin_remove(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)((GstElement *)( *((GstObject *)colorspace)).parent)),gst_bin_get_type()))),colorspace);
    gst_element_set_locked_state((output_window -> sink),(!0));
    gst_element_set_state((output_window -> sink),GST_STATE_NULL);
    gst_bin_remove(((GstBin *)(g_type_check_instance_cast(((GTypeInstance *)((GstElement *)( *((GstObject *)(output_window -> sink))).parent)),gst_bin_get_type()))),(output_window -> sink));
  }
  g_free((output_window -> session_id));
  g_free((output_window -> participant));
  g_free(output_window);
  return (!0);
#else
#endif
}

void purple_media_manager_remove_output_windows(PurpleMediaManager *manager,PurpleMedia *media,const gchar *session_id,const gchar *participant)
{
#ifdef USE_VV
  GList *iter;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)media;
      GType __t = purple_media_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA(media)");
      return ;
    };
  }while (0);
  iter = ( *(manager -> priv)).output_windows;
  for (; iter != 0; ) {
    PurpleMediaOutputWindow *ow = (iter -> data);
    iter = ((iter != 0)?( *((GList *)iter)).next : ((struct _GList *)((void *)0)));
    if (((media == (ow -> media)) && ((((session_id != ((const gchar *)((void *)0))) && ((ow -> session_id) != ((gchar *)((void *)0)))) && !(strcmp(session_id,(ow -> session_id)) != 0)) || (session_id == (ow -> session_id)))) && ((((participant != ((const gchar *)((void *)0))) && ((ow -> participant) != ((gchar *)((void *)0)))) && !(strcmp(participant,(ow -> participant)) != 0)) || (participant == (ow -> participant)))) 
      purple_media_manager_remove_output_window(manager,(ow -> id));
  }
#endif
}

void purple_media_manager_set_ui_caps(PurpleMediaManager *manager,PurpleMediaCaps caps)
{
#ifdef USE_VV
  PurpleMediaCaps oldcaps;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)manager;
      GType __t = purple_media_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_MANAGER(manager)");
      return ;
    };
  }while (0);
  oldcaps = ( *(manager -> priv)).ui_caps;
  ( *(manager -> priv)).ui_caps = caps;
  if (caps != oldcaps) 
    g_signal_emit(manager,purple_media_manager_signals[UI_CAPS_CHANGED],0,caps,oldcaps);
#endif
}

PurpleMediaCaps purple_media_manager_get_ui_caps(PurpleMediaManager *manager)
{
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)manager;
      GType __t = purple_media_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_MANAGER(manager)");
      return PURPLE_MEDIA_CAPS_NONE;
    };
  }while (0);
  return ( *(manager -> priv)).ui_caps;
#else
#endif
}

void purple_media_manager_set_backend_type(PurpleMediaManager *manager,GType backend_type)
{
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)manager;
      GType __t = purple_media_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_MANAGER(manager)");
      return ;
    };
  }while (0);
  ( *(manager -> priv)).backend_type = backend_type;
#endif
}

GType purple_media_manager_get_backend_type(PurpleMediaManager *manager)
{
#ifdef USE_VV
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)manager;
      GType __t = purple_media_manager_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_MANAGER(manager)");
      return PURPLE_MEDIA_CAPS_NONE;
    };
  }while (0);
  return ( *(manager -> priv)).backend_type;
#else
#endif
}
#ifdef USE_GSTREAMER
/*
 * PurpleMediaElementType
 */

GType purple_media_element_type_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GFlagsValue values[] = {{(PURPLE_MEDIA_ELEMENT_NONE), ("PURPLE_MEDIA_ELEMENT_NONE"), ("none")}, {(PURPLE_MEDIA_ELEMENT_AUDIO), ("PURPLE_MEDIA_ELEMENT_AUDIO"), ("audio")}, {(PURPLE_MEDIA_ELEMENT_VIDEO), ("PURPLE_MEDIA_ELEMENT_VIDEO"), ("video")}, {(PURPLE_MEDIA_ELEMENT_AUDIO_VIDEO), ("PURPLE_MEDIA_ELEMENT_AUDIO_VIDEO"), ("audio-video")}, {(PURPLE_MEDIA_ELEMENT_NO_SRCS), ("PURPLE_MEDIA_ELEMENT_NO_SRCS"), ("no-srcs")}, {(PURPLE_MEDIA_ELEMENT_ONE_SRC), ("PURPLE_MEDIA_ELEMENT_ONE_SRC"), ("one-src")}, {(PURPLE_MEDIA_ELEMENT_MULTI_SRC), ("PURPLE_MEDIA_ELEMENT_MULTI_SRC"), ("multi-src")}, {(PURPLE_MEDIA_ELEMENT_REQUEST_SRC), ("PURPLE_MEDIA_ELEMENT_REQUEST_SRC"), ("request-src")}, {(PURPLE_MEDIA_ELEMENT_NO_SINKS), ("PURPLE_MEDIA_ELEMENT_NO_SINKS"), ("no-sinks")}, {(PURPLE_MEDIA_ELEMENT_ONE_SINK), ("PURPLE_MEDIA_ELEMENT_ONE_SINK"), ("one-sink")}, {(PURPLE_MEDIA_ELEMENT_MULTI_SINK), ("PURPLE_MEDIA_ELEMENT_MULTI_SINK"), ("multi-sink")}, {(PURPLE_MEDIA_ELEMENT_REQUEST_SINK), ("PURPLE_MEDIA_ELEMENT_REQUEST_SINK"), ("request-sink")}, {(PURPLE_MEDIA_ELEMENT_UNIQUE), ("PURPLE_MEDIA_ELEMENT_UNIQUE"), ("unique")}, {(PURPLE_MEDIA_ELEMENT_SRC), ("PURPLE_MEDIA_ELEMENT_SRC"), ("src")}, {(PURPLE_MEDIA_ELEMENT_SINK), ("PURPLE_MEDIA_ELEMENT_SINK"), ("sink")}, {(0), ((const gchar *)((void *)0)), ((const gchar *)((void *)0))}};
    type = g_flags_register_static("PurpleMediaElementType",values);
  }
  return type;
}
/*
 * PurpleMediaElementInfo
 */

struct _PurpleMediaElementInfoClass 
{
  GObjectClass parent_class;
}
;

struct _PurpleMediaElementInfo 
{
  GObject parent;
}
;
#ifdef USE_VV

struct _PurpleMediaElementInfoPrivate 
{
  gchar *id;
  gchar *name;
  PurpleMediaElementType type;
  PurpleMediaElementCreateCallback create;
}
;
enum __unnamed_enum___F0_L1080_C1_PROP_0__COMMA__PROP_ID__COMMA__PROP_NAME__COMMA__PROP_TYPE__COMMA__PROP_CREATE_CB {PROP_0,PROP_ID,PROP_NAME,PROP_TYPE,PROP_CREATE_CB};

static void purple_media_element_info_init(PurpleMediaElementInfo *info)
{
  PurpleMediaElementInfoPrivate *priv = (PurpleMediaElementInfoPrivate *)(g_type_instance_get_private(((GTypeInstance *)info),purple_media_element_info_get_type()));
  priv -> id = ((gchar *)((void *)0));
  priv -> name = ((gchar *)((void *)0));
  priv -> type = PURPLE_MEDIA_ELEMENT_NONE;
  priv -> create = ((PurpleMediaElementCreateCallback )((void *)0));
}

static void purple_media_element_info_finalize(GObject *info)
{
  PurpleMediaElementInfoPrivate *priv = (PurpleMediaElementInfoPrivate *)(g_type_instance_get_private(((GTypeInstance *)info),purple_media_element_info_get_type()));
  g_free((priv -> id));
  g_free((priv -> name));
}

static void purple_media_element_info_set_property(GObject *object,guint prop_id,const GValue *value,GParamSpec *pspec)
{
  PurpleMediaElementInfoPrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = purple_media_element_info_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_ELEMENT_INFO(object)");
      return ;
    };
  }while (0);
  priv = ((PurpleMediaElementInfoPrivate *)(g_type_instance_get_private(((GTypeInstance *)object),purple_media_element_info_get_type())));
{
    switch(prop_id){
      case PROP_ID:
{
        g_free((priv -> id));
        priv -> id = g_value_dup_string(value);
        break; 
      }
      case PROP_NAME:
{
        g_free((priv -> name));
        priv -> name = g_value_dup_string(value);
        break; 
      }
      case PROP_TYPE:
{
{
          priv -> type = (g_value_get_flags(value));
          break; 
        }
      }
      case PROP_CREATE_CB:
{
        priv -> create = (g_value_get_pointer(value));
        break; 
      }
      default:
{
        do {
          GObject *_glib__object = (GObject *)object;
          GParamSpec *_glib__pspec = (GParamSpec *)pspec;
          guint _glib__property_id = prop_id;
          g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","mediamanager.c:1134","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
        }while (0);
        break; 
      }
    }
  }
}

static void purple_media_element_info_get_property(GObject *object,guint prop_id,GValue *value,GParamSpec *pspec)
{
  PurpleMediaElementInfoPrivate *priv;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)object;
      GType __t = purple_media_element_info_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_ELEMENT_INFO(object)");
      return ;
    };
  }while (0);
  priv = ((PurpleMediaElementInfoPrivate *)(g_type_instance_get_private(((GTypeInstance *)object),purple_media_element_info_get_type())));
  switch(prop_id){
    case PROP_ID:
{
      g_value_set_string(value,(priv -> id));
      break; 
    }
    case PROP_NAME:
{
      g_value_set_string(value,(priv -> name));
      break; 
    }
    case PROP_TYPE:
{
      g_value_set_flags(value,(priv -> type));
      break; 
    }
    case PROP_CREATE_CB:
{
      g_value_set_pointer(value,(priv -> create));
      break; 
    }
    default:
{
      do {
        GObject *_glib__object = (GObject *)object;
        GParamSpec *_glib__pspec = (GParamSpec *)pspec;
        guint _glib__property_id = prop_id;
        g_log(0,G_LOG_LEVEL_WARNING,"%s: invalid %s id %u for \"%s\" of type \'%s\' in \'%s\'","mediamanager.c:1163","property",_glib__property_id,(_glib__pspec -> name),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__pspec)).g_class)).g_type),g_type_name(( *((GTypeClass *)( *((GTypeInstance *)_glib__object)).g_class)).g_type));
      }while (0);
      break; 
    }
  }
}

static void purple_media_element_info_class_init(PurpleMediaElementInfoClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *)klass;
  gobject_class -> finalize = purple_media_element_info_finalize;
  gobject_class -> set_property = purple_media_element_info_set_property;
  gobject_class -> get_property = purple_media_element_info_get_property;
  g_object_class_install_property(gobject_class,PROP_ID,g_param_spec_string("id","ID","The unique identifier of the element.",0,(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_class_install_property(gobject_class,PROP_NAME,g_param_spec_string("name","Name","The friendly/display name of this element.",0,(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_class_install_property(gobject_class,PROP_TYPE,g_param_spec_flags("type","Element Type","The type of element this is.",purple_media_element_type_get_type(),PURPLE_MEDIA_ELEMENT_NONE,(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_object_class_install_property(gobject_class,PROP_CREATE_CB,g_param_spec_pointer("create-cb","Create Callback","The function called to create this element.",(G_PARAM_CONSTRUCT_ONLY | (G_PARAM_READABLE | G_PARAM_WRITABLE))));
  g_type_class_add_private(klass,(sizeof(PurpleMediaElementInfoPrivate )));
}
static void purple_media_element_info_init(PurpleMediaElementInfo *self);
static void purple_media_element_info_class_init(PurpleMediaElementInfoClass *klass);
static gpointer purple_media_element_info_parent_class = (gpointer )((void *)0);
static gint PurpleMediaElementInfo_private_offset;

static void purple_media_element_info_class_intern_init(gpointer klass)
{
  purple_media_element_info_parent_class = g_type_class_peek_parent(klass);
  if (PurpleMediaElementInfo_private_offset != 0) 
    g_type_class_adjust_private_offset(klass,&PurpleMediaElementInfo_private_offset);
  purple_media_element_info_class_init(((PurpleMediaElementInfoClass *)klass));
}

inline static gpointer purple_media_element_info_get_instance_private(PurpleMediaElementInfo *self)
{
  return (gpointer )(((guint8 *)self) + ((glong )PurpleMediaElementInfo_private_offset));
}

GType purple_media_element_info_get_type()
{
  static volatile gsize g_define_type_id__volatile = 0;
  if ((({
    typedef char _GStaticAssertCompileTimeAssertion_1209[1UL];
    (0?((gpointer )g_define_type_id__volatile) : 0);
    !(((
{
      typedef char _GStaticAssertCompileTimeAssertion_1209[1UL];
      __sync_synchronize();
      (gpointer )g_define_type_id__volatile;
    })) != 0) && (g_once_init_enter((&g_define_type_id__volatile)) != 0);
  })) != 0) 
{
    GType g_define_type_id = g_type_register_static_simple(((GType )(20 << 2)),g_intern_static_string("PurpleMediaElementInfo"),(sizeof(PurpleMediaElementInfoClass )),((GClassInitFunc )purple_media_element_info_class_intern_init),(sizeof(PurpleMediaElementInfo )),((GInstanceInitFunc )purple_media_element_info_init),((GTypeFlags )0));
{
{
{
        };
      }
    }
    (
{
      typedef char _GStaticAssertCompileTimeAssertion_1209[1UL];
      0?(g_define_type_id__volatile = g_define_type_id) : 0;
      g_once_init_leave((&g_define_type_id__volatile),((gsize )g_define_type_id));
    });
  }
  return g_define_type_id__volatile;
}
#else
#endif

gchar *purple_media_element_info_get_id(PurpleMediaElementInfo *info)
{
#ifdef USE_VV
  gchar *id;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)info;
      GType __t = purple_media_element_info_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_ELEMENT_INFO(info)");
      return 0;
    };
  }while (0);
  g_object_get(info,"id",&id,((void *)((void *)0)));
  return id;
#else
#endif
}

gchar *purple_media_element_info_get_name(PurpleMediaElementInfo *info)
{
#ifdef USE_VV
  gchar *name;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)info;
      GType __t = purple_media_element_info_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_ELEMENT_INFO(info)");
      return 0;
    };
  }while (0);
  g_object_get(info,"name",&name,((void *)((void *)0)));
  return name;
#else
#endif
}

PurpleMediaElementType purple_media_element_info_get_element_type(PurpleMediaElementInfo *info)
{
#ifdef USE_VV
  PurpleMediaElementType type;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)info;
      GType __t = purple_media_element_info_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_ELEMENT_INFO(info)");
      return PURPLE_MEDIA_ELEMENT_NONE;
    };
  }while (0);
  g_object_get(info,"type",&type,((void *)((void *)0)));
  return type;
#else
#endif
}

GstElement *purple_media_element_info_call_create(PurpleMediaElementInfo *info,PurpleMedia *media,const gchar *session_id,const gchar *participant)
{
#ifdef USE_VV
  PurpleMediaElementCreateCallback create;
  do {
    if ((({
      GTypeInstance *__inst = (GTypeInstance *)info;
      GType __t = purple_media_element_info_get_type();
      gboolean __r;
      if (!(__inst != 0)) 
        __r = 0;
      else if (((__inst -> g_class) != 0) && (( *(__inst -> g_class)).g_type == __t)) 
        __r = (!0);
      else 
        __r = g_type_check_instance_is_a(__inst,__t);
      __r;
    })) != 0) 
{
    }
    else {
      g_return_if_fail_warning(0,((const char *)__func__),"PURPLE_IS_MEDIA_ELEMENT_INFO(info)");
      return 0;
    };
  }while (0);
  g_object_get(info,"create-cb",&create,((void *)((void *)0)));
  if (create != 0) 
    return ( *create)(media,session_id,participant);
#endif
  return 0;
}
#endif /* USE_GSTREAMER */
