/* confdefs.h */
#define PACKAGE_NAME "pidgin"
#define PACKAGE_TARNAME "pidgin"
#define PACKAGE_VERSION "2.10.9"
#define PACKAGE_STRING "pidgin 2.10.9"
#define PACKAGE_BUGREPORT "devel@pidgin.im"
#define PACKAGE_URL ""
#define PACKAGE "pidgin"
#define VERSION "2.10.9"
#define CONFIG_ARGS " 'CC=exampleTranslator' '--disable-perl'"
#define STDC_HEADERS 1
#define HAVE_SYS_TYPES_H 1
#define HAVE_SYS_STAT_H 1
#define HAVE_STDLIB_H 1
#define HAVE_STRING_H 1
#define HAVE_MEMORY_H 1
#define HAVE_STRINGS_H 1
#define HAVE_INTTYPES_H 1
#define HAVE_STDINT_H 1
#define HAVE_UNISTD_H 1
#define HAVE_DLFCN_H 1
#define LT_OBJDIR ".libs/"
#define HAVE_ALLOCA_H 1
#define HAVE_ALLOCA 1
#define STDC_HEADERS 1
#define HAVE_ARPA_NAMESER_COMPAT_H 1
#define HAVE_FCNTL_H 1
#define HAVE_SYS_TIME_H 1
#define HAVE_UNISTD_H 1
#define HAVE_LOCALE_H 1
#define HAVE_SIGNAL_H 1
#define HAVE_STDINT_H 1
#define HAVE_REGEX_H 1
#define SIZEOF_TIME_T 8
#define RETSIGTYPE void
#define HAVE_STRFTIME 1
#define HAVE_STRDUP 1
#define HAVE_STRSTR 1
#define HAVE_ATEXIT 1
#define HAVE_SETLOCALE 1
#define HAVE_GETOPT_LONG 1
#define HAVE_LIBRESOLV 1
#define HAVE_LIBNSL 1
#define HAVE_GETADDRINFO 1
#define HAVE_INET_NTOP 1
#define HAVE_GETIFADDRS 1
#define HAVE_IPV6_V6ONLY 1
#define HAVE_FILENO 1
#define HAVE_STRFTIME_Z_FORMAT 1
#define HAVE_LOCALE_H 1
#define HAVE_LC_MESSAGES 1
#define HAVE_BIND_TEXTDOMAIN_CODESET 1
#define HAVE_GETTEXT 1
#define HAVE_DCGETTEXT 1
#define ENABLE_NLS 1
#define DISPLAY_VERSION "2.10.9"
#define HAVE_PANGO14 1
#define HAVE_X11 1
#define USE_SCREENSAVER 1
#define HAVE_X11_SM_SMLIB_H 1
#define USE_SM 1
#define USE_GTKSPELL 1
#define USE_GSTREAMER 1
#define GST_CAN_DISABLE_FORKING /**/
#define USE_GSTINTERFACES 1
#define USE_VV 1
#define USE_IDN 1
#define HAVE_LIBGADU 1
#define STATIC_PROTO_INIT  static void static_proto_init(void) {  }
#define HAVE_SYS_UTSNAME_H 1
#define HAVE_NETWORKMANAGER 1
#define HAVE_DBUS 1
#define HAVE_LIBPTHREAD 1
#define HAVE_LIBUTIL 1
#define USE_PYTHON 1
#define HAVE_GNUTLS_GNUTLS_H 1
#define HAVE_GNUTLS 1
#define HAVE_SSL 1
#define HAVE_GNUTLS_PRIORITY_FUNCS 1
#define HAVE_GNUTLS_CERT_INSECURE_ALGORITHM 1
#define HAVE_NSS 1
#define HAVE_SSL 1
#define HAVE_TCL 1
#define HAVE_TK 1
#define PURPLE_PLUGINS 1
#define HAVE_SNPRINTF 1
#define HAVE_CONNECT 1
#define ZEPHYR_INT32 long
#define HAVE_GETHOSTID 1
#define HAVE_LRAND48 1
#define HAVE_TIMEGM 1
#define HAVE_MEMCPY 1
#define HAVE_MEMMOVE 1
#define HAVE_RANDOM 1
#define HAVE_STRCHR 1
#define HAVE_STRERROR 1
#define HAVE_VPRINTF 1
#define HAVE_MALLOC_H 1
#define HAVE_PATHS_H 1
#define HAVE_SGTTY_H 1
#define HAVE_STDARG_H 1
#define HAVE_SYS_CDEFS_H 1
#define HAVE_SYS_FILE_H 1
#define HAVE_SYS_IOCTL_H 1
#define HAVE_SYS_SELECT_H 1
#define HAVE_SYS_UIO_H 1
#define HAVE_SYS_UTSNAME_H 1
#define HAVE_TERMIOS_H 1
#define HAVE_SYS_PARAM_H 1
#define HAVE_SYS_SYSCTL_H 1
#define HAVE_SYS_SOCKET_H 1
#define HAVE_STRUCT_TM_TM_ZONE 1
#define HAVE_TM_ZONE 1
#define HAVE_TIMEZONE 1
#define HAVE_DAYLIGHT 1
#define HAVE_TM_GMTOFF 1
/* end confdefs.h.  */
#include <stdarg.h>
#include <stdlib.h>

void f(int i,... )
{
  va_list args1;
  va_list args2;
  va_start(args1,i);
  args2 = args1;
  if (((va_arg(args2,int )) != 42) || ((va_arg(args1,int )) != 42)) 
    exit(1);
  va_end(args1);
  va_end(args2);
}

int main()
{
  f(0,42);
  return 0;
}
