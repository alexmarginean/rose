/* confdefs.h */
#define PACKAGE_NAME "SoX"
#define PACKAGE_TARNAME "sox"
#define PACKAGE_VERSION "14.4.1"
#define PACKAGE_STRING "SoX 14.4.1"
#define PACKAGE_BUGREPORT "sox-devel@lists.sourceforge.net"
#define PACKAGE_URL ""
#define PACKAGE "sox"
#define VERSION "14.4.1"
#define STDC_HEADERS 1
#define HAVE_SYS_TYPES_H 1
#define HAVE_SYS_STAT_H 1
#define HAVE_STDLIB_H 1
#define HAVE_STRING_H 1
#define HAVE_MEMORY_H 1
#define HAVE_STRINGS_H 1
#define HAVE_INTTYPES_H 1
#define HAVE_STDINT_H 1
#define HAVE_UNISTD_H 1
#define HAVE_DLFCN_H 1
#define LT_OBJDIR ".libs/"
#define STDC_HEADERS 1
#define HAVE_FCNTL_H 1
#define HAVE_UNISTD_H 1
#define HAVE_BYTESWAP_H 1
#define HAVE_SYS_STAT_H 1
#define HAVE_SYS_TIME_H 1
#define HAVE_SYS_TIMEB_H 1
#define HAVE_SYS_TYPES_H 1
#define HAVE_SYS_UTSNAME_H 1
#define HAVE_TERMIOS_H 1
#define HAVE_GLOB_H 1
#define HAVE_STRCASECMP 1
#define HAVE_STRDUP 1
#define HAVE_POPEN 1
#define HAVE_VSNPRINTF 1
#define HAVE_GETTIMEOFDAY 1
#define HAVE_MKSTEMP 1
#define HAVE_FMEMOPEN 1
#define HAVE_LIBM 1
#define HAVE_FSEEKO 1
#define HAVE_LTDL_H 1
#define HAVE_LIBLTDL 1
#define HAVE_OMP_H 1
#define HAVE_OPENMP 1
#define HAVE_PNG_H 1
#define HAVE_LIBPNG_PNG_H 1
#define HAVE_PNG 1
#define HAVE_LADSPA_H 1
#define HAVE_MAD_H 1
#define HAVE_ALSA 1
#define STATIC_ALSA 1
#define HAVE_PULSEAUDIO 1
#define STATIC_PULSEAUDIO 1
#define HAVE_LIBAVFORMAT_AVFORMAT_H 1
/* end confdefs.h.  */
#include <stdio.h>
#ifdef HAVE_SYS_TYPES_H
# include <sys/types.h>
#endif
#ifdef HAVE_SYS_STAT_H
# include <sys/stat.h>
#endif
#ifdef STDC_HEADERS
# include <stdlib.h>
# include <stddef.h>
#else
# ifdef HAVE_STDLIB_H
#  include <stdlib.h>
# endif
#endif
#ifdef HAVE_STRING_H
# if !defined STDC_HEADERS && defined HAVE_MEMORY_H
#  include <memory.h>
# endif
# include <string.h>
#endif
#ifdef HAVE_STRINGS_H
# include <strings.h>
#endif
#ifdef HAVE_INTTYPES_H
# include <inttypes.h>
#endif
#ifdef HAVE_STDINT_H
# include <stdint.h>
#endif
#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif
#include <sys/soundcard.h>
