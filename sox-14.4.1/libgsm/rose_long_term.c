/*
 * Copyright 1992 by Jutta Degener and Carsten Bormann, Technische
 * Universitaet Berlin.  See the accompanying file "COPYRIGHT" for
 * details.  THERE IS ABSOLUTELY NO WARRANTY FOR THIS SOFTWARE.
 */
/* $Header: /cvsroot/sox/sox/libgsm/long_term.c,v 1.2 2007/11/04 16:32:36 robs Exp $ */
#include <stdio.h>
#include <assert.h>
#include "private.h"
#include "gsm.h"
/*
 *  4.2.11 .. 4.2.12 LONG TERM PREDICTOR (LTP) SECTION
 */
/*
 * This module computes the LTP gain (bc) and the LTP lag (Nc)
 * for the long term analysis filter.   This is done by calculating a
 * maximum of the cross-correlation function between the current
 * sub-segment short term residual signal d[0..39] (output of
 * the short term analysis filter; for simplification the index
 * of this array begins at 0 and ends at 39 for each sub-segment of the
 * RPE-LTP analysis) and the previous reconstructed short term
 * residual signal dp[ -120 .. -1 ].  A dynamic scaling must be
 * performed to avoid overflow.
 */
/* The next procedure exists in six versions.  First two integer
  * version (if USE_FLOAT_MUL is not defined); then four floating
  * point versions, twice with proper scaling (USE_FLOAT_MUL defined),
  * once without (USE_FLOAT_MUL and FAST defined, and fast run-time
  * option used).  Every pair has first a Cut version (see the -C
  * option to toast or the LTP_CUT option to gsm_option()), then the
  * uncut one.  (For a detailed explanation of why this is altogether
  * a bad idea, see Henry Spencer and Geoff Collyer, ``#ifdef Considered
  * Harmful''.)
  */
#ifndef  USE_FLOAT_MUL
#ifdef	LTP_CUT
/* [0..39]	IN	*/
/* [-120..-1]	IN	*/
/* 		OUT	*/
/* 		OUT	*/
/*  Search of the optimum scaling of d[0..39].
	 */
/* Search for the maximum cross-correlation and coding of the LTP lag
	 */
/* index for the maximum cross-correlation */
/*  Rescaling of L_max
	 */
/* sub(6, scal) */
/*   Compute the power of the reconstructed short term residual
	 *   signal dp[..]
	 */
/* from L_MULT */
/*  Normalization of L_max and L_power
	 */
/*  Coding of the LTP gain
	 */
/*  Table 4.3a must be used to obtain the level DLB[i] for the
	 *  quantization of the LTP gain b to get the coded version bc.
	 */
#endif 	/* LTP_CUT */

static void Calculation_of_the_LTP_parameters(
/* [0..39]	IN	*/
register word *d,
/* [-120..-1]	IN	*/
register word *dp,
/* 		OUT	*/
word *bc_out,
/* 		OUT	*/
word *Nc_out)
{
  register int k;
  register int lambda;
  word Nc;
  word bc;
  word wt[40UL];
  longword L_max;
  longword L_power;
  word R;
  word S;
  word dmax;
  word scal;
  register word temp;
/*  Search of the optimum scaling of d[0..39].
	 */
  dmax = 0;
  for (k = 0; k <= 39; k++) {
    temp = d[k];
    temp = (((temp < 0)?(((temp == -32767 - 1)?32767 : -temp)) : temp));
    if (temp > dmax) 
      dmax = temp;
  }
  temp = 0;
  if (dmax == 0) 
    scal = 0;
  else {
    (dmax > 0)?((void )0) : __assert_fail("dmax > 0","long_term.c",183,"void Calculation_of_the_LTP_parameters(short *, short *, short *, short *)");
    temp = lsx_gsm_norm((((longword )dmax) << 16));
  }
  if (temp > 6) 
    scal = 0;
  else 
    scal = (6 - temp);
  (scal >= 0)?((void )0) : __assert_fail("scal >= 0","long_term.c",190,"void Calculation_of_the_LTP_parameters(short *, short *, short *, short *)");
/*  Initialization of a working array wt
	 */
  for (k = 0; k <= 39; k++) 
    wt[k] = (d[k] >> scal);
/* Search for the maximum cross-correlation and coding of the LTP lag
	 */
  L_max = 0;
/* index for the maximum cross-correlation */
  Nc = 40;
  for (lambda = 40; lambda <= 120; lambda++) {
# undef STEP
#		define STEP(k) 	(longword)wt[k] * dp[k - lambda]
    register longword L_result;
    L_result = (((longword )wt[0]) * dp[0 - lambda]);
    L_result += (((longword )wt[1]) * dp[1 - lambda]);
    L_result += (((longword )wt[2]) * dp[2 - lambda]);
    L_result += (((longword )wt[3]) * dp[3 - lambda]);
    L_result += (((longword )wt[4]) * dp[4 - lambda]);
    L_result += (((longword )wt[5]) * dp[5 - lambda]);
    L_result += (((longword )wt[6]) * dp[6 - lambda]);
    L_result += (((longword )wt[7]) * dp[7 - lambda]);
    L_result += (((longword )wt[8]) * dp[8 - lambda]);
    L_result += (((longword )wt[9]) * dp[9 - lambda]);
    L_result += (((longword )wt[10]) * dp[10 - lambda]);
    L_result += (((longword )wt[11]) * dp[11 - lambda]);
    L_result += (((longword )wt[12]) * dp[12 - lambda]);
    L_result += (((longword )wt[13]) * dp[13 - lambda]);
    L_result += (((longword )wt[14]) * dp[14 - lambda]);
    L_result += (((longword )wt[15]) * dp[15 - lambda]);
    L_result += (((longword )wt[16]) * dp[16 - lambda]);
    L_result += (((longword )wt[17]) * dp[17 - lambda]);
    L_result += (((longword )wt[18]) * dp[18 - lambda]);
    L_result += (((longword )wt[19]) * dp[19 - lambda]);
    L_result += (((longword )wt[20]) * dp[20 - lambda]);
    L_result += (((longword )wt[21]) * dp[21 - lambda]);
    L_result += (((longword )wt[22]) * dp[22 - lambda]);
    L_result += (((longword )wt[23]) * dp[23 - lambda]);
    L_result += (((longword )wt[24]) * dp[24 - lambda]);
    L_result += (((longword )wt[25]) * dp[25 - lambda]);
    L_result += (((longword )wt[26]) * dp[26 - lambda]);
    L_result += (((longword )wt[27]) * dp[27 - lambda]);
    L_result += (((longword )wt[28]) * dp[28 - lambda]);
    L_result += (((longword )wt[29]) * dp[29 - lambda]);
    L_result += (((longword )wt[30]) * dp[30 - lambda]);
    L_result += (((longword )wt[31]) * dp[31 - lambda]);
    L_result += (((longword )wt[32]) * dp[32 - lambda]);
    L_result += (((longword )wt[33]) * dp[33 - lambda]);
    L_result += (((longword )wt[34]) * dp[34 - lambda]);
    L_result += (((longword )wt[35]) * dp[35 - lambda]);
    L_result += (((longword )wt[36]) * dp[36 - lambda]);
    L_result += (((longword )wt[37]) * dp[37 - lambda]);
    L_result += (((longword )wt[38]) * dp[38 - lambda]);
    L_result += (((longword )wt[39]) * dp[39 - lambda]);
    if (L_result > L_max) {
      Nc = lambda;
      L_max = L_result;
    }
  }
   *Nc_out = Nc;
  L_max <<= 1;
/*  Rescaling of L_max
	 */
  ((scal <= 100) && (scal >= -100))?((void )0) : __assert_fail("scal <= 100 && scal >= -100","long_term.c",243,"void Calculation_of_the_LTP_parameters(short *, short *, short *, short *)");
/* sub(6, scal) */
  L_max = (L_max >> (6 - scal));
  ((Nc <= 120) && (Nc >= 40))?((void )0) : __assert_fail("Nc <= 120 && Nc >= 40","long_term.c",246,"void Calculation_of_the_LTP_parameters(short *, short *, short *, short *)");
/*   Compute the power of the reconstructed short term residual
	 *   signal dp[..]
	 */
  L_power = 0;
  for (k = 0; k <= 39; k++) {
    register longword L_temp;
    L_temp = (dp[k - Nc] >> 3);
    L_power += (L_temp * L_temp);
  }
/* from L_MULT */
  L_power <<= 1;
/*  Normalization of L_max and L_power
	 */
  if (L_max <= 0) {
     *bc_out = 0;
    return ;
  }
  if (L_max >= L_power) {
     *bc_out = 3;
    return ;
  }
  temp = lsx_gsm_norm(L_power);
  R = ((L_max << temp) >> 16);
  S = ((L_power << temp) >> 16);
{
/*  Coding of the LTP gain
	 */
/*  Table 4.3a must be used to obtain the level DLB[i] for the
	 *  quantization of the LTP gain b to get the coded version bc.
	 */
    for (bc = 0; bc <= 2; bc++) 
      if (R <= (lsx_gsm_mult(S,lsx_gsm_DLB[bc]))) 
        break; 
  }
   *bc_out = bc;
}
#else	/* USE_FLOAT_MUL */
#ifdef	LTP_CUT
/*              IN 	*/
/* [0..39]	IN	*/
/* [-120..-1]	IN	*/
/* 		OUT	*/
/* 		OUT	*/
/*  Search of the optimum scaling of d[0..39].
	 */
/*  Initialization of a working array wt
	 */
/* Search for the maximum cross-correlation and coding of the LTP lag
	 */
/* index for the maximum cross-correlation */
/*  Calculate L_result for l = lambda .. lambda + 9.
		 */
#		undef STEP
#		define	STEP(K, a, b, c, d, e, f, g, h) \
			if ((W = wt_float[K]) != 0.0) {	\
			E = W * a; S8 += E;		\
			E = W * b; S7 += E;		\
			E = W * c; S6 += E;		\
			E = W * d; S5 += E;		\
			E = W * e; S4 += E;		\
			E = W * f; S3 += E;		\
			E = W * g; S2 += E;		\
			E = W * h; S1 += E;		\
			a  = lp[K];			\
			E = W * a; S0 += E; } else (a = lp[K])
#		define	STEP_A(K)	STEP(K, a, b, c, d, e, f, g, h)
#		define	STEP_B(K)	STEP(K, b, c, d, e, f, g, h, a)
#		define	STEP_C(K)	STEP(K, c, d, e, f, g, h, a, b)
#		define	STEP_D(K)	STEP(K, d, e, f, g, h, a, b, c)
#		define	STEP_E(K)	STEP(K, e, f, g, h, a, b, c, d)
#		define	STEP_F(K)	STEP(K, f, g, h, a, b, c, d, e)
#		define	STEP_G(K)	STEP(K, g, h, a, b, c, d, e, f)
#		define	STEP_H(K)	STEP(K, h, a, b, c, d, e, f, g)
/*  Rescaling of L_max
	 */
/* sub(6, scal) */
/*   Compute the power of the reconstructed short term residual
	 *   signal dp[..]
	 */
/* from L_MULT */
/*  Normalization of L_max and L_power
	 */
/*  Coding of the LTP gain
	 */
/*  Table 4.3a must be used to obtain the level DLB[i] for the
	 *  quantization of the LTP gain b to get the coded version bc.
	 */
#endif /* LTP_CUT */
/* [0..39]	IN	*/
/* [-120..-1]	IN	*/
/* 		OUT	*/
/* 		OUT	*/
/*  Search of the optimum scaling of d[0..39].
	 */
/*  Initialization of a working array wt
	 */
/* Search for the maximum cross-correlation and coding of the LTP lag
	 */
/* index for the maximum cross-correlation */
/*  Calculate L_result for l = lambda .. lambda + 9.
		 */
#		undef STEP
#		define	STEP(K, a, b, c, d, e, f, g, h) \
			W = wt_float[K];		\
			E = W * a; S8 += E;		\
			E = W * b; S7 += E;		\
			E = W * c; S6 += E;		\
			E = W * d; S5 += E;		\
			E = W * e; S4 += E;		\
			E = W * f; S3 += E;		\
			E = W * g; S2 += E;		\
			E = W * h; S1 += E;		\
			a  = lp[K];			\
			E = W * a; S0 += E
#		define	STEP_A(K)	STEP(K, a, b, c, d, e, f, g, h)
#		define	STEP_B(K)	STEP(K, b, c, d, e, f, g, h, a)
#		define	STEP_C(K)	STEP(K, c, d, e, f, g, h, a, b)
#		define	STEP_D(K)	STEP(K, d, e, f, g, h, a, b, c)
#		define	STEP_E(K)	STEP(K, e, f, g, h, a, b, c, d)
#		define	STEP_F(K)	STEP(K, f, g, h, a, b, c, d, e)
#		define	STEP_G(K)	STEP(K, g, h, a, b, c, d, e, f)
#		define	STEP_H(K)	STEP(K, h, a, b, c, d, e, f, g)
/*  Rescaling of L_max
	 */
/* sub(6, scal) */
/*   Compute the power of the reconstructed short term residual
	 *   signal dp[..]
	 */
/* from L_MULT */
/*  Normalization of L_max and L_power
	 */
/*  Coding of the LTP gain
	 */
/*  Table 4.3a must be used to obtain the level DLB[i] for the
	 *  quantization of the LTP gain b to get the coded version bc.
	 */
#ifdef	FAST
#ifdef	LTP_CUT
/*              IN	*/
/* [0..39]	IN	*/
/* [-120..-1]	IN	*/
/* 		OUT	*/
/* 		OUT	*/
/* Search for the maximum cross-correlation and coding of the LTP lag
	 */
/* index for the maximum cross-correlation */
/*  Compute the power of the reconstructed short term residual
	 *  signal dp[..]
	 */
/*  Coding of the LTP gain
	 *  Table 4.3a must be used to obtain the level DLB[i] for the
	 *  quantization of the LTP gain b to get the coded version bc.
	 */
#endif /* LTP_CUT */
/* [0..39]	IN	*/
/* [-120..-1]	IN	*/
/* 		OUT	*/
/* 		OUT	*/
/* Search for the maximum cross-correlation and coding of the LTP lag
	 */
/* index for the maximum cross-correlation */
/*  Calculate L_result for l = lambda .. lambda + 9.
		 */
#		undef STEP
#		define	STEP(K, a, b, c, d, e, f, g, h) \
			W = wt_float[K];		\
			E = W * a; S8 += E;		\
			E = W * b; S7 += E;		\
			E = W * c; S6 += E;		\
			E = W * d; S5 += E;		\
			E = W * e; S4 += E;		\
			E = W * f; S3 += E;		\
			E = W * g; S2 += E;		\
			E = W * h; S1 += E;		\
			a  = lp[K];			\
			E = W * a; S0 += E
#		define	STEP_A(K)	STEP(K, a, b, c, d, e, f, g, h)
#		define	STEP_B(K)	STEP(K, b, c, d, e, f, g, h, a)
#		define	STEP_C(K)	STEP(K, c, d, e, f, g, h, a, b)
#		define	STEP_D(K)	STEP(K, d, e, f, g, h, a, b, c)
#		define	STEP_E(K)	STEP(K, e, f, g, h, a, b, c, d)
#		define	STEP_F(K)	STEP(K, f, g, h, a, b, c, d, e)
#		define	STEP_G(K)	STEP(K, g, h, a, b, c, d, e, f)
#		define	STEP_H(K)	STEP(K, h, a, b, c, d, e, f, g)
/*  Compute the power of the reconstructed short term residual
	 *  signal dp[..]
	 */
/*  Coding of the LTP gain
	 *  Table 4.3a must be used to obtain the level DLB[i] for the
	 *  quantization of the LTP gain b to get the coded version bc.
	 */
#endif	/* FAST 	 */
#endif	/* USE_FLOAT_MUL */
/* 4.2.12 */

static void Long_term_analysis_filtering(
/* 					IN  */
word bc,
/* 					IN  */
word Nc,
/* previous d	[-120..-1]		IN  */
register word *dp,
/* d		[0..39]			IN  */
register word *d,
/* estimate	[0..39]			OUT */
register word *dpp,
/* long term res. signal [0..39]	OUT */
register word *e)
/*
 *  In this part, we have to decode the bc parameter to compute
 *  the samples of the estimate dpp[0..39].  The decoding of bc needs the
 *  use of table 4.3b.  The long term residual signal e[0..39]
 *  is then calculated to be fed to the RPE encoding section.
 */
{
  register int k;
  register longword ltmp;
#	undef STEP
#	define STEP(BP)					\
	for (k = 0; k <= 39; k++) {			\
		dpp[k]  = GSM_MULT_R( BP, dp[k - Nc]);	\
		e[k]	= GSM_SUB( d[k], dpp[k] );	\
	}
  switch(bc){
    case 0:
{
      for (k = 0; k <= 39; k++) {
        dpp[k] = (((((longword )3277) * ((longword )dp[k - Nc])) + 16384) >> 15);
        e[k] = ((((ltmp = (((longword )d[k]) - ((longword )dpp[k]))) >= 32767)?32767 : (((ltmp <= (-32767 - 1))?(-32767 - 1) : ltmp))));
      };
      break; 
    }
    case 1:
{
      for (k = 0; k <= 39; k++) {
        dpp[k] = (((((longword )11469) * ((longword )dp[k - Nc])) + 16384) >> 15);
        e[k] = ((((ltmp = (((longword )d[k]) - ((longword )dpp[k]))) >= 32767)?32767 : (((ltmp <= (-32767 - 1))?(-32767 - 1) : ltmp))));
      };
      break; 
    }
    case 2:
{
      for (k = 0; k <= 39; k++) {
        dpp[k] = (((((longword )21299) * ((longword )dp[k - Nc])) + 16384) >> 15);
        e[k] = ((((ltmp = (((longword )d[k]) - ((longword )dpp[k]))) >= 32767)?32767 : (((ltmp <= (-32767 - 1))?(-32767 - 1) : ltmp))));
      };
      break; 
    }
    case 3:
{
      for (k = 0; k <= 39; k++) {
        dpp[k] = (((((longword )32767) * ((longword )dp[k - Nc])) + 16384) >> 15);
        e[k] = ((((ltmp = (((longword )d[k]) - ((longword )dpp[k]))) >= 32767)?32767 : (((ltmp <= (-32767 - 1))?(-32767 - 1) : ltmp))));
      };
      break; 
    }
  }
}
/* 4x for 160 samples */

void lsx_Gsm_Long_Term_Predictor(struct gsm_state *S,
/* [0..39]   residual signal	IN	*/
word *d,
/* [-120..-1] d'		IN	*/
word *dp,
/* [0..39] 			OUT	*/
word *e,
/* [0..39] 			OUT	*/
word *dpp,
/* correlation lag		OUT	*/
word *Nc,
/* gain factor			OUT	*/
word *bc)
{
/* Denotes intentionally unused */
  S;
  (d != 0)?((void )0) : __assert_fail("d","long_term.c",880,"void lsx_Gsm_Long_Term_Predictor(struct gsm_state *, short *, short *, short *, short *, short *, short *)");
  (dp != 0)?((void )0) : __assert_fail("dp","long_term.c",880,"void lsx_Gsm_Long_Term_Predictor(struct gsm_state *, short *, short *, short *, short *, short *, short *)");
  (e != 0)?((void )0) : __assert_fail("e","long_term.c",880,"void lsx_Gsm_Long_Term_Predictor(struct gsm_state *, short *, short *, short *, short *, short *, short *)");
  (dpp != 0)?((void )0) : __assert_fail("dpp","long_term.c",881,"void lsx_Gsm_Long_Term_Predictor(struct gsm_state *, short *, short *, short *, short *, short *, short *)");
  (Nc != 0)?((void )0) : __assert_fail("Nc","long_term.c",881,"void lsx_Gsm_Long_Term_Predictor(struct gsm_state *, short *, short *, short *, short *, short *, short *)");
  (bc != 0)?((void )0) : __assert_fail("bc","long_term.c",881,"void lsx_Gsm_Long_Term_Predictor(struct gsm_state *, short *, short *, short *, short *, short *, short *)");
#if defined(FAST) && defined(USE_FLOAT_MUL)
#if   defined (LTP_CUT)
#endif /* LTP_CUT */
#endif /* FAST & USE_FLOAT_MUL */
#ifdef LTP_CUT
#endif
  Calculation_of_the_LTP_parameters(d,dp,bc,Nc);
  Long_term_analysis_filtering( *bc, *Nc,dp,d,dpp,e);
}
/* 4.3.2 */

void lsx_Gsm_Long_Term_Synthesis_Filtering(struct gsm_state *S,word Ncr,word bcr,
/* [0..39]		  	 IN */
register word *erp,
/* [-120..-1] IN, [-120..40] OUT */
register word *drp)
/*
 *  This procedure uses the bcr and Ncr parameter to realize the
 *  long term synthesis filtering.  The decoding of bcr needs
 *  table 4.3b.
 */
{
/* for ADD */
  register longword ltmp;
  register int k;
  word brp;
  word drpp;
  word Nr;
/*  Check the limits of Nr.
	 */
  Nr = ((((Ncr < 40) || (Ncr > 120))?(S -> nrp) : Ncr));
  S -> nrp = Nr;
  ((Nr >= 40) && (Nr <= 120))?((void )0) : __assert_fail("Nr >= 40 && Nr <= 120","long_term.c",927,"void lsx_Gsm_Long_Term_Synthesis_Filtering(struct gsm_state *, short, short, short *, short *)");
/*  Decoding of the LTP gain bcr
	 */
  brp = lsx_gsm_QLB[bcr];
/*  Computation of the reconstructed short term residual 
	 *  signal drp[0..39]
	 */
  (brp != -32767 - 1)?((void )0) : __assert_fail("brp != (-32767 - 1)","long_term.c",936,"void lsx_Gsm_Long_Term_Synthesis_Filtering(struct gsm_state *, short, short, short *, short *)");
  for (k = 0; k <= 39; k++) {
    drpp = (((((longword )brp) * ((longword )drp[k - Nr])) + 16384) >> 15);
    drp[k] = (((((ulongword )((ltmp = (((longword )erp[k]) + ((longword )drpp))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  }
/*
	 *  Update of the reconstructed short term residual signal
	 *  drp[ -1..-120 ]
	 */
  for (k = 0; k <= 119; k++) 
    drp[-120 + k] = drp[-80 + k];
}
