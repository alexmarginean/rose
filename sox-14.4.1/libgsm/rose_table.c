/*
 * Copyright 1992 by Jutta Degener and Carsten Bormann, Technische
 * Universitaet Berlin.  See the accompanying file "COPYRIGHT" for
 * details.  THERE IS ABSOLUTELY NO WARRANTY FOR THIS SOFTWARE.
 */
/* $Header: /cvsroot/sox/sox/libgsm/table.c,v 1.1 2007/09/06 16:50:56 cbagwell Exp $ */
/*  Most of these tables are inlined at their point of use.
 */
/*  4.4 TABLES USED IN THE FIXED POINT IMPLEMENTATION OF THE RPE-LTP
 *      CODER AND DECODER
 *
 *	(Most of them inlined, so watch out.)
 */
#define	GSM_TABLE_C
#include "private.h"
#include	"gsm.h"
/*  Table 4.1  Quantization of the Log.-Area Ratios
 */
/* i 		     1      2      3        4      5      6        7       8 */
word lsx_gsm_A[8UL] = {(20480), (20480), (20480), (20480), (13964), (15360), (8534), (9036)};
word lsx_gsm_B[8UL] = {(0), (0), (2048), ((-2560)), (94), ((-1792)), ((-341)), ((-1144))};
word lsx_gsm_MIC[8UL] = {((-32)), ((-32)), ((-16)), ((-16)), ((-8)), ((-8)), ((-4)), ((-4))};
word lsx_gsm_MAC[8UL] = {(31), (31), (15), (15), (7), (7), (3), (3)};
/*  Table 4.2  Tabulation  of 1/A[1..8]
 */
word lsx_gsm_INVA[8UL] = {(13107), (13107), (13107), (13107), (19223), (17476), (31454), (29708)};
/*   Table 4.3a  Decision level of the LTP gain quantizer
 */
/*  bc		      0	        1	  2	     3			*/
word lsx_gsm_DLB[4UL] = {(6554), (16384), (26214), (32767)};
/*   Table 4.3b   Quantization levels of the LTP gain quantizer
 */
/* bc		      0          1        2          3			*/
word lsx_gsm_QLB[4UL] = {(3277), (11469), (21299), (32767)};
/*   Table 4.4	 Coefficients of the weighting filter
 */
/* i		    0      1   2    3   4      5      6     7   8   9    10  */
word lsx_gsm_H[11UL] = {((-134)), ((-374)), (0), (2054), (5741), (8192), (5741), (2054), (0), ((-374)), ((-134))};
/*   Table 4.5 	 Normalized inverse mantissa used to compute xM/xmax 
 */
/* i		 	0        1    2      3      4      5     6      7   */
word lsx_gsm_NRFAC[8UL] = {(29128), (26215), (23832), (21846), (20165), (18725), (17476), (16384)};
/*   Table 4.6	 Normalized direct mantissa used to compute xM/xmax
 */
/* i                  0      1       2      3      4      5      6      7   */
word lsx_gsm_FAC[8UL] = {(18431), (20479), (22527), (24575), (26623), (28671), (30719), (32767)};
