/*
 * Copyright 1992 by Jutta Degener and Carsten Bormann, Technische
 * Universitaet Berlin.  See the accompanying file "COPYRIGHT" for
 * details.  THERE IS ABSOLUTELY NO WARRANTY FOR THIS SOFTWARE.
 */
/* $Header: /cvsroot/sox/sox/libgsm/decode.c,v 1.1 2007/09/06 16:50:55 cbagwell Exp $ */
#include <stdio.h>
#include	"private.h"
#include	"gsm.h"
/*
 *  4.3 FIXED POINT IMPLEMENTATION OF THE RPE-LTP DECODER
 */

static void Postprocessing(struct gsm_state *S,register word *s)
{
  register int k;
  register word msr = (S -> msr);
/* for GSM_ADD */
  register longword ltmp;
  register word tmp;
  for (k = 160; k-- != 0; s++) {
    tmp = (((((longword )msr) * ((longword )28180)) + 16384) >> 15);
/* Deemphasis 	     */
    msr = (((((ulongword )((ltmp = (((longword )( *s)) + ((longword )tmp))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
/* Truncation & Upscaling */
     *s = ((((((ulongword )((ltmp = (((longword )msr) + ((longword )msr))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp)) & 0xFFF8);
  }
  S -> msr = msr;
}

void lsx_Gsm_Decoder(struct gsm_state *S,
/* [0..7]		IN	*/
word *LARcr,
/* [0..3] 		IN 	*/
word *Ncr,
/* [0..3]		IN	*/
word *bcr,
/* [0..3] 		IN 	*/
word *Mcr,
/* [0..3]		IN 	*/
word *xmaxcr,
/* [0..13*4]		IN	*/
word *xMcr,
/* [0..159]		OUT 	*/
word *s)
{
  int j;
  int k;
  word erp[40UL];
  word wt[160UL];
  word *drp = ((S -> dp0) + 120);
  for (j = 0; j <= 3; (((((((((j++ , xmaxcr++)) , bcr++)) , Ncr++)) , Mcr++)) , (xMcr += 13))) {
    lsx_Gsm_RPE_Decoding(S, *xmaxcr, *Mcr,xMcr,erp);
    lsx_Gsm_Long_Term_Synthesis_Filtering(S, *Ncr, *bcr,erp,drp);
    for (k = 0; k <= 39; k++) 
      wt[(j * 40) + k] = drp[k];
  }
  lsx_Gsm_Short_Term_Synthesis_Filter(S,LARcr,wt,s);
  Postprocessing(S,s);
}
