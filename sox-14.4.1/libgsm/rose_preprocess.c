/*
 * Copyright 1992 by Jutta Degener and Carsten Bormann, Technische
 * Universitaet Berlin.  See the accompanying file "COPYRIGHT" for
 * details.  THERE IS ABSOLUTELY NO WARRANTY FOR THIS SOFTWARE.
 */
/* $Header: /cvsroot/sox/sox/libgsm/preprocess.c,v 1.1 2007/09/06 16:50:55 cbagwell Exp $ */
#include	<stdio.h>
#include	<assert.h>
#include "private.h"
#include	"gsm.h"
/*	4.2.0 .. 4.2.3	PREPROCESSING SECTION
 *  
 *  	After A-law to linear conversion (or directly from the
 *   	Ato D converter) the following scaling is assumed for
 * 	input to the RPE-LTP algorithm:
 *
 *      in:  0.1.....................12
 *	     S.v.v.v.v.v.v.v.v.v.v.v.v.*.*.*
 *
 *	Where S is the sign bit, v a valid bit, and * a "don't care" bit.
 * 	The original signal is called sop[..]
 *
 *      out:   0.1................... 12 
 *	     S.S.v.v.v.v.v.v.v.v.v.v.v.v.0.0
 */

void lsx_Gsm_Preprocess(struct gsm_state *S,word *s,
/* [0..159] 	IN/OUT	*/
word *so)
{
  word z1 = (S -> z1);
  longword L_z2 = (S -> L_z2);
  word mp = (S -> mp);
  word s1;
  longword L_s2;
  longword L_temp;
  word msp;
  word lsp;
  word SO;
/* for   ADD */
  longword ltmp;
/* for L_ADD */
  ulongword utmp;
  register int k = 160;
  while(k-- != 0){
/*  4.2.1   Downscaling of the input signal
	 */
    SO = ((( *s) >> 3) << 2);
    s++;
/* downscaled by     */
    (SO >= -0x4000)?((void )0) : __assert_fail("SO >= -0x4000","preprocess.c",63,"void lsx_Gsm_Preprocess(struct gsm_state *, short *, short *)");
/* previous routine. */
    (SO <= 0x3FFC)?((void )0) : __assert_fail("SO <= 0x3FFC","preprocess.c",64,"void lsx_Gsm_Preprocess(struct gsm_state *, short *, short *)");
/*  4.2.2   Offset compensation
	 * 
	 *  This part implements a high-pass filter and requires extended
	 *  arithmetic precision for the recursive part of this filter.
	 *  The input of this procedure is the array so[0...159] and the
	 *  output the array sof[ 0...159 ].
	 */
/*   Compute the non-recursive part
		 */
/* s1 = gsm_sub( *so, z1 ); */
    s1 = (SO - z1);
    z1 = SO;
    (s1 != -32767 - 1)?((void )0) : __assert_fail("s1 != (-32767 - 1)","preprocess.c",80,"void lsx_Gsm_Preprocess(struct gsm_state *, short *, short *)");
/*   Compute the recursive part
		 */
    L_s2 = s1;
    L_s2 <<= 15;
/*   Execution of a 31 bv 16 bits multiplication
		 */
    msp = (L_z2 >> 15);
/* gsm_L_sub(L_z2,(msp<<15)); */
    lsp = (L_z2 - (((longword )msp) << 15));
    L_s2 += (((((longword )lsp) * ((longword )32735)) + 0x4000) >> 15);
/* GSM_L_MULT(msp,32735) >> 1;*/
    L_temp = (((longword )msp) * 32735);
    L_z2 = ((L_temp < 0)?(((L_s2 >= 0)?(L_temp + L_s2) : ((((utmp = (((ulongword )(-(L_temp + 1))) + ((ulongword )(-(L_s2 + 1))))) >= 2147483647)?(-2147483647 - 1) : (-((longword )utmp) - 2))))) : (((L_s2 <= 0)?(L_temp + L_s2) : ((((utmp = (((ulongword )L_temp) + ((ulongword )L_s2))) >= 2147483647)?2147483647 : ((longword )utmp))))));
/*    Compute sof[k] with rounding
		 */
    L_temp = ((L_z2 < 0)?((1?(L_z2 + 0x4000) : ((((utmp = (((ulongword )(-(L_z2 + 1))) + ((ulongword )(-(0x4000 + 1))))) >= 2147483647)?(-2147483647 - 1) : (-((longword )utmp) - 2))))) : ((0?(L_z2 + 0x4000) : ((((utmp = (((ulongword )L_z2) + ((ulongword )0x4000))) >= 2147483647)?2147483647 : ((longword )utmp))))));
/*   4.2.3  Preemphasis
	 */
    msp = (((((longword )mp) * ((longword )(-28180))) + 0x4000) >> 15);
    mp = (L_temp >> 15);
     *(so++) = (((((ulongword )((ltmp = (((longword )mp) + ((longword )msp))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  }
  S -> z1 = z1;
  S -> L_z2 = L_z2;
  S -> mp = mp;
}
