/*
 * Copyright 1992 by Jutta Degener and Carsten Bormann, Technische
 * Universitaet Berlin.  See the accompanying file "COPYRIGHT" for
 * details.  THERE IS ABSOLUTELY NO WARRANTY FOR THIS SOFTWARE.
 */
/* $Header: /cvsroot/sox/sox/libgsm/short_term.c,v 1.1 2007/09/06 16:50:56 cbagwell Exp $ */
#include <stdio.h>
#include <assert.h>
#include "private.h"
#include "gsm.h"
/*
 *  SHORT TERM ANALYSIS FILTERING SECTION
 */
/* 4.2.8 */

static void Decoding_of_the_coded_Log_Area_Ratios(
/* coded log area ratio	[0..7] 	IN	*/
word *LARc,
/* out: decoded ..			*/
word *LARpp)
{
/* , temp2 */
  register word temp1;
/* for GSM_ADD */
  register long ltmp;
/*  This procedure requires for efficient implementation
	 *  two tables.
 	 *
	 *  INVA[1..8] = integer( (32768 * 8) / real_A[1..8])
	 *  MIC[1..8]  = minimum value of the LARc[1..8]
	 */
/*  Compute the LARpp[1..8]
	 */
/* 	for (i = 1; i <= 8; i++, B++, MIC++, INVA++, LARc++, LARpp++) {
	 *
	 *		temp1  = GSM_ADD( *LARc, *MIC ) << 10;
	 *		temp2  = *B << 1;
	 *		temp1  = GSM_SUB( temp1, temp2 );
	 *
	 *		assert(*INVA != MIN_WORD);
	 *
	 *		temp1  = GSM_MULT_R( *INVA, temp1 );
	 *		*LARpp = GSM_ADD( temp1, temp1 );
	 *	}
	 */
#undef	STEP
#define	STEP( B, MIC, INVA )	\
		temp1    = GSM_ADD( *LARc++, MIC ) << 10;	\
		temp1    = GSM_SUB( temp1, B << 1 );		\
		temp1    = GSM_MULT_R( INVA, temp1 );		\
		*LARpp++ = GSM_ADD( temp1, temp1 );
  temp1 = ((((((ulongword )((ltmp = (((longword )( *(LARc++))) + ((longword )(-32)))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp)) << 10);
  temp1 = ((((ltmp = (((longword )temp1) - ((longword )(0 << 1)))) >= 32767)?32767 : (((ltmp <= (-32767 - 1))?(-32767 - 1) : ltmp))));
  temp1 = (((((longword )13107) * ((longword )temp1)) + 16384) >> 15);
   *(LARpp++) = (((((ulongword )((ltmp = (((longword )temp1) + ((longword )temp1))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));;
  temp1 = ((((((ulongword )((ltmp = (((longword )( *(LARc++))) + ((longword )(-32)))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp)) << 10);
  temp1 = ((((ltmp = (((longword )temp1) - ((longword )(0 << 1)))) >= 32767)?32767 : (((ltmp <= (-32767 - 1))?(-32767 - 1) : ltmp))));
  temp1 = (((((longword )13107) * ((longword )temp1)) + 16384) >> 15);
   *(LARpp++) = (((((ulongword )((ltmp = (((longword )temp1) + ((longword )temp1))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));;
  temp1 = ((((((ulongword )((ltmp = (((longword )( *(LARc++))) + ((longword )(-16)))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp)) << 10);
  temp1 = ((((ltmp = (((longword )temp1) - ((longword )(2048 << 1)))) >= 32767)?32767 : (((ltmp <= (-32767 - 1))?(-32767 - 1) : ltmp))));
  temp1 = (((((longword )13107) * ((longword )temp1)) + 16384) >> 15);
   *(LARpp++) = (((((ulongword )((ltmp = (((longword )temp1) + ((longword )temp1))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));;
  temp1 = ((((((ulongword )((ltmp = (((longword )( *(LARc++))) + ((longword )(-16)))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp)) << 10);
  temp1 = ((((ltmp = (((longword )temp1) - ((longword )(-2560 << 1)))) >= 32767)?32767 : (((ltmp <= (-32767 - 1))?(-32767 - 1) : ltmp))));
  temp1 = (((((longword )13107) * ((longword )temp1)) + 16384) >> 15);
   *(LARpp++) = (((((ulongword )((ltmp = (((longword )temp1) + ((longword )temp1))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));;
  temp1 = ((((((ulongword )((ltmp = (((longword )( *(LARc++))) + ((longword )(-8)))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp)) << 10);
  temp1 = ((((ltmp = (((longword )temp1) - ((longword )(94 << 1)))) >= 32767)?32767 : (((ltmp <= (-32767 - 1))?(-32767 - 1) : ltmp))));
  temp1 = (((((longword )19223) * ((longword )temp1)) + 16384) >> 15);
   *(LARpp++) = (((((ulongword )((ltmp = (((longword )temp1) + ((longword )temp1))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));;
  temp1 = ((((((ulongword )((ltmp = (((longword )( *(LARc++))) + ((longword )(-8)))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp)) << 10);
  temp1 = ((((ltmp = (((longword )temp1) - ((longword )(-1792 << 1)))) >= 32767)?32767 : (((ltmp <= (-32767 - 1))?(-32767 - 1) : ltmp))));
  temp1 = (((((longword )17476) * ((longword )temp1)) + 16384) >> 15);
   *(LARpp++) = (((((ulongword )((ltmp = (((longword )temp1) + ((longword )temp1))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));;
  temp1 = ((((((ulongword )((ltmp = (((longword )( *(LARc++))) + ((longword )(-4)))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp)) << 10);
  temp1 = ((((ltmp = (((longword )temp1) - ((longword )(-341 << 1)))) >= 32767)?32767 : (((ltmp <= (-32767 - 1))?(-32767 - 1) : ltmp))));
  temp1 = (((((longword )31454) * ((longword )temp1)) + 16384) >> 15);
   *(LARpp++) = (((((ulongword )((ltmp = (((longword )temp1) + ((longword )temp1))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));;
  temp1 = ((((((ulongword )((ltmp = (((longword )( *(LARc++))) + ((longword )(-4)))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp)) << 10);
  temp1 = ((((ltmp = (((longword )temp1) - ((longword )(-1144 << 1)))) >= 32767)?32767 : (((ltmp <= (-32767 - 1))?(-32767 - 1) : ltmp))));
  temp1 = (((((longword )29708) * ((longword )temp1)) + 16384) >> 15);
   *(LARpp++) = (((((ulongword )((ltmp = (((longword )temp1) + ((longword )temp1))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));;
/* NOTE: the addition of *MIC is used to restore
	 * 	 the sign of *LARc.
	 */
}
/* 4.2.9 */
/* Computation of the quantized reflection coefficients 
 */
/* 4.2.9.1  Interpolation of the LARpp[1..8] to get the LARp[1..8]
 */
/*
 *  Within each frame of 160 analyzed speech samples the short term
 *  analysis and synthesis filters operate with four different sets of
 *  coefficients, derived from the previous set of decoded LARs(LARpp(j-1))
 *  and the actual set of decoded LARs (LARpp(j))
 *
 * (Initial value: LARpp(j-1)[1..8] = 0.)
 */

static void Coefficients_0_12(register word *LARpp_j_1,register word *LARpp_j,register word *LARp)
{
  register int i;
  register longword ltmp;
  for (i = 1; i <= 8; (((((i++ , LARp++)) , LARpp_j_1++)) , LARpp_j++)) {
     *LARp = (((((ulongword )((ltmp = (((longword )(( *LARpp_j_1) >> 2)) + ((longword )(( *LARpp_j) >> 2)))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
     *LARp = (((((ulongword )((ltmp = (((longword )( *LARp)) + ((longword )(( *LARpp_j_1) >> 1)))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  }
}

static void Coefficients_13_26(register word *LARpp_j_1,register word *LARpp_j,register word *LARp)
{
  register int i;
  register longword ltmp;
  for (i = 1; i <= 8; (((((i++ , LARpp_j_1++)) , LARpp_j++)) , LARp++)) {
     *LARp = (((((ulongword )((ltmp = (((longword )(( *LARpp_j_1) >> 1)) + ((longword )(( *LARpp_j) >> 1)))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  }
}

static void Coefficients_27_39(register word *LARpp_j_1,register word *LARpp_j,register word *LARp)
{
  register int i;
  register longword ltmp;
  for (i = 1; i <= 8; (((((i++ , LARpp_j_1++)) , LARpp_j++)) , LARp++)) {
     *LARp = (((((ulongword )((ltmp = (((longword )(( *LARpp_j_1) >> 2)) + ((longword )(( *LARpp_j) >> 2)))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
     *LARp = (((((ulongword )((ltmp = (((longword )( *LARp)) + ((longword )(( *LARpp_j) >> 1)))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  }
}

static void Coefficients_40_159(register word *LARpp_j,register word *LARp)
{
  register int i;
  for (i = 1; i <= 8; (((i++ , LARp++)) , LARpp_j++)) 
     *LARp =  *LARpp_j;
}
/* 4.2.9.2 */

static void LARp_to_rp(
/* [0..7] IN/OUT  */
register word *LARp)
/*
 *  The input of this procedure is the interpolated LARp[0..7] array.
 *  The reflection coefficients, rp[i], are used in the analysis
 *  filter and in the synthesis filter.
 */
{
  register int i;
  register word temp;
  register longword ltmp;
  for (i = 1; i <= 8; (i++ , LARp++)) {
/* temp = GSM_ABS( *LARp );
	         *
		 * if (temp < 11059) temp <<= 1;
		 * else if (temp < 20070) temp += 11059;
		 * else temp = GSM_ADD( temp >> 2, 26112 );
		 *
		 * *LARp = *LARp < 0 ? -temp : temp;
		 */
    if (( *LARp) < 0) {
      temp = (((( *LARp) == -32767 - 1)?32767 : -( *LARp)));
       *LARp = (-(((temp < 11059)?(temp << 1) : (((temp < 20070)?(temp + 11059) : (((((ulongword )((ltmp = (((longword )(temp >> 2)) + ((longword )26112))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp)))))));
    }
    else {
      temp =  *LARp;
       *LARp = (((temp < 11059)?(temp << 1) : (((temp < 20070)?(temp + 11059) : (((((ulongword )((ltmp = (((longword )(temp >> 2)) + ((longword )26112))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp))))));
    }
  }
}
/* 4.2.10 */

static void Short_term_analysis_filtering(struct gsm_state *S,
/* [0..7]	IN	*/
register word *rp,
/*   k_end - k_start	*/
register int k_n,
/* [0..n-1]	IN/OUT	*/
register word *s)
/*
 *  This procedure computes the short term residual signal d[..] to be fed
 *  to the RPE-LTP loop from the s[..] signal and from the local rp[..]
 *  array (quantized reflection coefficients).  As the call of this
 *  procedure can be done in many ways (see the interpolation of the LAR
 *  coefficient), it is assumed that the computation begins with index
 *  k_start (for arrays d[..] and s[..]) and stops with index k_end
 *  (k_start and k_end are defined in 4.2.9.1).  This procedure also
 *  needs to keep the array u[0..7] in memory for each call.
 */
{
  register word *u = (S -> u);
  register int i;
  register word di;
  register word zzz;
  register word ui;
  register word sav;
  register word rpi;
  register longword ltmp;
  for (; k_n-- != 0; s++) {
    di = (sav =  *s);
/* YYY */
    for (i = 0; i < 8; i++) {
      ui = u[i];
      rpi = rp[i];
      u[i] = sav;
      zzz = (((((longword )rpi) * ((longword )di)) + 16384) >> 15);
      sav = (((((ulongword )((ltmp = (((longword )ui) + ((longword )zzz))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
      zzz = (((((longword )rpi) * ((longword )ui)) + 16384) >> 15);
      di = (((((ulongword )((ltmp = (((longword )di) + ((longword )zzz))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
    }
     *s = di;
  }
}
#if defined(USE_FLOAT_MUL) && defined(FAST)
/* [0..7]	IN	*/
/*   k_end - k_start	*/
/* [0..n-1]	IN/OUT	*/
#endif /* ! (defined (USE_FLOAT_MUL) && defined (FAST)) */

static void Short_term_synthesis_filtering(struct gsm_state *S,
/* [0..7]	IN	*/
register word *rrp,
/* k_end - k_start	*/
register int k,
/* [0..k-1]	IN	*/
register word *wt,
/* [0..k-1]	OUT	*/
register word *sr)
{
  register word *v = (S -> v);
  register int i;
  register word sri;
  register word tmp1;
  register word tmp2;
/* for GSM_ADD  & GSM_SUB */
  register longword ltmp;
  while(k-- != 0){
    sri =  *(wt++);
    for (i = 8; i-- != 0; ) {
/* sri = GSM_SUB( sri, gsm_mult_r( rrp[i], v[i] ) );
			 */
      tmp1 = rrp[i];
      tmp2 = v[i];
      tmp2 = ((((tmp1 == -32767 - 1) && (tmp2 == -32767 - 1))?32767 : (0x0FFFF & (((((longword )tmp1) * ((longword )tmp2)) + 16384) >> 15))));
      sri = ((((ltmp = (((longword )sri) - ((longword )tmp2))) >= 32767)?32767 : (((ltmp <= (-32767 - 1))?(-32767 - 1) : ltmp))));
/* v[i+1] = GSM_ADD( v[i], gsm_mult_r( rrp[i], sri ) );
			 */
      tmp1 = ((((tmp1 == -32767 - 1) && (sri == -32767 - 1))?32767 : (0x0FFFF & (((((longword )tmp1) * ((longword )sri)) + 16384) >> 15))));
      v[i + 1] = (((((ulongword )((ltmp = (((longword )v[i]) + ((longword )tmp1))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
    }
     *(sr++) = (v[0] = sri);
  }
}
#if defined(FAST) && defined(USE_FLOAT_MUL)
/* [0..7]	IN	*/
/* k_end - k_start	*/
/* [0..k-1]	IN	*/
/* [0..k-1]	OUT	*/
#endif /* defined(FAST) && defined(USE_FLOAT_MUL) */

void lsx_Gsm_Short_Term_Analysis_Filter(struct gsm_state *S,
/* coded log area ratio [0..7]  IN	*/
word *LARc,
/* signal [0..159]		IN/OUT	*/
word *s)
{
  word *LARpp_j = (S -> LARpp)[S -> j];
  word *LARpp_j_1 = (S -> LARpp)[S -> j ^= 1];
  word LARp[8UL];
#undef	FILTER
#if 	defined(FAST) && defined(USE_FLOAT_MUL)
# 	define	FILTER 	(* (S->fast			\
			   ? Fast_Short_term_analysis_filtering	\
		    	   : Short_term_analysis_filtering	))
#else
# 	define	FILTER	Short_term_analysis_filtering
#endif
  Decoding_of_the_coded_Log_Area_Ratios(LARc,LARpp_j);
  Coefficients_0_12(LARpp_j_1,LARpp_j,LARp);
  LARp_to_rp(LARp);
  Short_term_analysis_filtering(S,LARp,13,s);
  Coefficients_13_26(LARpp_j_1,LARpp_j,LARp);
  LARp_to_rp(LARp);
  Short_term_analysis_filtering(S,LARp,14,(s + 13));
  Coefficients_27_39(LARpp_j_1,LARpp_j,LARp);
  LARp_to_rp(LARp);
  Short_term_analysis_filtering(S,LARp,13,(s + 27));
  Coefficients_40_159(LARpp_j,LARp);
  LARp_to_rp(LARp);
  Short_term_analysis_filtering(S,LARp,120,(s + 40));
}

void lsx_Gsm_Short_Term_Synthesis_Filter(struct gsm_state *S,
/* received log area ratios [0..7] IN  */
word *LARcr,
/* received d [0..159]		   IN  */
word *wt,
/* signal   s [0..159]		  OUT  */
word *s)
{
  word *LARpp_j = (S -> LARpp)[S -> j];
  word *LARpp_j_1 = (S -> LARpp)[S -> j ^= 1];
  word LARp[8UL];
#undef	FILTER
#if 	defined(FAST) && defined(USE_FLOAT_MUL)
# 	define	FILTER 	(* (S->fast			\
			   ? Fast_Short_term_synthesis_filtering	\
		    	   : Short_term_synthesis_filtering	))
#else
#	define	FILTER	Short_term_synthesis_filtering
#endif
  Decoding_of_the_coded_Log_Area_Ratios(LARcr,LARpp_j);
  Coefficients_0_12(LARpp_j_1,LARpp_j,LARp);
  LARp_to_rp(LARp);
  Short_term_synthesis_filtering(S,LARp,13,wt,s);
  Coefficients_13_26(LARpp_j_1,LARpp_j,LARp);
  LARp_to_rp(LARp);
  Short_term_synthesis_filtering(S,LARp,14,(wt + 13),(s + 13));
  Coefficients_27_39(LARpp_j_1,LARpp_j,LARp);
  LARp_to_rp(LARp);
  Short_term_synthesis_filtering(S,LARp,13,(wt + 27),(s + 27));
  Coefficients_40_159(LARpp_j,LARp);
  LARp_to_rp(LARp);
  Short_term_synthesis_filtering(S,LARp,120,(wt + 40),(s + 40));
}
