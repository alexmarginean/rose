/*
 * Copyright 1992 by Jutta Degener and Carsten Bormann, Technische
 * Universitaet Berlin.  See the accompanying file "COPYRIGHT" for
 * details.  THERE IS ABSOLUTELY NO WARRANTY FOR THIS SOFTWARE.
 */
/* $Header: /cvsroot/sox/sox/libgsm/code.c,v 1.1 2007/09/06 16:50:55 cbagwell Exp $ */
#include	<stdlib.h>
#include	<string.h>
#include	"private.h"
#include	"gsm.h"
/* 
 *  4.2 FIXED POINT IMPLEMENTATION OF THE RPE-LTP CODER 
 */

void lsx_Gsm_Coder(struct gsm_state *S,
/* [0..159] samples		  	IN	*/
word *s,
/*
 * The RPE-LTD coder works on a frame by frame basis.  The length of
 * the frame is equal to 160 samples.  Some computations are done
 * once per frame to produce at the output of the coder the
 * LARc[1..8] parameters which are the coded LAR coefficients and 
 * also to realize the inverse filtering operation for the entire
 * frame (160 samples of signal d[0..159]).  These parts produce at
 * the output of the coder:
 */
/* [0..7] LAR coefficients		OUT	*/
word *LARc,
/*
 * Procedure 4.2.11 to 4.2.18 are to be executed four times per
 * frame.  That means once for each sub-segment RPE-LTP analysis of
 * 40 samples.  These parts produce at the output of the coder:
 */
/* [0..3] LTP lag			OUT 	*/
word *Nc,
/* [0..3] coded LTP gain		OUT 	*/
word *bc,
/* [0..3] RPE grid selection		OUT     */
word *Mc,
/* [0..3] Coded maximum amplitude	OUT	*/
word *xmaxc,
/* [13*4] normalized RPE samples	OUT	*/
word *xMc)
{
  int k;
/* [ -120...-1 ] */
  word *dp = ((S -> dp0) + 120);
/* [ 0...39 ]	 */
  word *dpp = dp;
  static word e[50UL];
  word so[160UL];
  lsx_Gsm_Preprocess(S,s,so);
  lsx_Gsm_LPC_Analysis(S,so,LARc);
  lsx_Gsm_Short_Term_Analysis_Filter(S,LARc,so);
  for (k = 0; k <= 3; (k++ , (xMc += 13))) {
    lsx_Gsm_Long_Term_Predictor(S,(so + (k * 40)),dp,(e + 5),dpp,Nc++,bc++);
/* d      [0..39] IN	*/
/* dp  [-120..-1] IN	*/
/* e      [0..39] OUT	*/
/* dpp    [0..39] OUT */
    lsx_Gsm_RPE_Encoding(S,(e + 5),xmaxc++,Mc++,xMc);
/* e	  ][0..39][ IN/OUT */
/*
		 * Gsm_Update_of_reconstructed_short_time_residual_signal
		 *			( dpp, e + 5, dp );
		 */
{
      register int i;
      register longword ltmp;
      for (i = 0; i <= 39; i++) 
        dp[i] = (((((ulongword )((ltmp = (((longword )e[5 + i]) + ((longword )dpp[i]))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
    }
    dp += 40;
    dpp += 40;
  }
  memcpy(((char *)(S -> dp0)),((char *)((S -> dp0) + 160)),(120 * sizeof(( *S -> dp0))));
}
