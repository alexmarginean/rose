/*
 * Copyright 1992 by Jutta Degener and Carsten Bormann, Technische
 * Universitaet Berlin.  See the accompanying file "COPYRIGHT" for
 * details.  THERE IS ABSOLUTELY NO WARRANTY FOR THIS SOFTWARE.
 */
/* $Header: /cvsroot/sox/sox/libgsm/gsm_option.c,v 1.2 2008/02/16 18:30:03 robs Exp $ */
#include "private.h"
#include "gsm.h"

int lsx_gsm_option(gsm r,int opt,int *val)
{
  int result = -1;
  switch(opt){
    case 3:
{
#ifdef 	LTP_CUT
#endif
      break; 
    }
    case 1:
{
#ifndef	NDEBUG
      result = (r -> verbose);
      if (val != 0) 
        r -> verbose = ( *val);
#endif
      break; 
    }
    case 2:
{
#if	defined(FAST) && defined(USE_FLOAT_MUL)
#endif
      break; 
    }
    case 6:
{
#ifdef WAV49
      result = (r -> frame_chain);
      if (val != 0) 
        r -> frame_chain = ( *val);
#endif
      break; 
    }
    case 5:
{
#ifdef WAV49
      result = (r -> frame_index);
      if (val != 0) 
        r -> frame_index = ( *val);
#endif
      break; 
    }
    case 4:
{
#ifdef WAV49 
      result = (r -> wav_fmt);
      if (val != 0) 
        r -> wav_fmt = (!(!( *val != 0)));
#endif
      break; 
    }
    default:
{
      (r , ((void )val));
      break; 
    }
  }
  return result;
}
