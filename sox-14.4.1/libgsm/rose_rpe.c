/*
 * Copyright 1992 by Jutta Degener and Carsten Bormann, Technische
 * Universitaet Berlin.  See the accompanying file "COPYRIGHT" for
 * details.  THERE IS ABSOLUTELY NO WARRANTY FOR THIS SOFTWARE.
 */
/* $Header: /cvsroot/sox/sox/libgsm/rpe.c,v 1.2 2007/11/04 16:32:36 robs Exp $ */
#include <stdio.h>
#include <assert.h>
#include "private.h"
#include "gsm.h"
/*  4.2.13 .. 4.2.17  RPE ENCODING SECTION
 */
/* 4.2.13 */

static void Weighting_filter(
/* signal [-5..0.39.44]	IN  */
register word *e,
/* signal [0..39]	OUT */
word *x)
/*
 *  The coefficients of the weighting filter are stored in a table
 *  (see table 4.4).  The following scaling is used:
 *
 *	H[0..10] = integer( real_H[ 0..10] * 8192 ); 
 */
{
/* word			wt[ 50 ]; */
  register longword L_result;
/* , i */
  register int k;
/*  Initialization of a temporary working array wt[0...49]
	 */
/* for (k =  0; k <=  4; k++) wt[k] = 0;
	 * for (k =  5; k <= 44; k++) wt[k] = *e++;
	 * for (k = 45; k <= 49; k++) wt[k] = 0;
	 *
	 *  (e[-5..-1] and e[40..44] are allocated by the caller,
	 *  are initially zero and are not written anywhere.)
	 */
  e -= 5;
/*  Compute the signal x[0..39]
	 */
  for (k = 0; k <= 39; k++) {
    L_result = (8192 >> 1);
/* for (i = 0; i <= 10; i++) {
		 *	L_temp   = GSM_L_MULT( wt[k+i], gsm_H[i] );
		 *	L_result = GSM_L_ADD( L_result, L_temp );
		 * }
		 */
#undef	STEP
#define	STEP( i, H )	(e[ k + i ] * (longword)H)
/*  Every one of these multiplications is done twice --
		 *  but I don't see an elegant way to optimize this. 
		 *  Do you?
		 */
#ifdef	STUPID_COMPILER
/* + STEP(	2, 	0    )  */
/* + STEP(	8, 	0    )  */
#else
    L_result += (((((((((e[k + 0] * ((longword )(-134))) + (e[k + 1] * ((longword )(-374)))) + (e[k + 3] * ((longword )2054))) + (e[k + 4] * ((longword )5741))) + (e[k + 5] * ((longword )8192))) + (e[k + 6] * ((longword )5741))) + (e[k + 7] * ((longword )2054))) + (e[k + 9] * ((longword )(-374)))) + (e[k + 10] * ((longword )(-134))));
/* + STEP(	2, 	0    )  */
/* + STEP(	8, 	0    )  */
#endif
/* L_result = GSM_L_ADD( L_result, L_result ); (* scaling(x2) *)
		 * L_result = GSM_L_ADD( L_result, L_result ); (* scaling(x4) *)
		 *
		 * x[k] = SASR( L_result, 16 );
		 */
/* 2 adds vs. >>16 => 14, minus one shift to compensate for
		 * those we lost when replacing L_MULT by '*'.
		 */
    L_result = (L_result >> 13);
    x[k] = (((L_result < (-32767 - 1))?(-32767 - 1) : (((L_result > 32767)?32767 : L_result))));
  }
}
/* 4.2.14 */

static void RPE_grid_selection(
/* [0..39]		IN  */
word *x,
/* [0..12]		OUT */
word *xM,
/*			OUT */
word *Mc_out)
/*
 *  The signal x[0..39] is used to select the RPE grid which is
 *  represented by Mc.
 */
{
/* register word	temp1;	*/
/* m, */
  register int i;
  register longword L_result;
  register longword L_temp;
/* xxx should be L_EM? */
  longword EM;
  word Mc;
  longword L_common_0_3;
  EM = 0;
  Mc = 0;
/* for (m = 0; m <= 3; m++) {
	 *	L_result = 0;
	 *
	 *
	 *	for (i = 0; i <= 12; i++) {
	 *
	 *		temp1    = SASR( x[m + 3*i], 2 );
	 *
	 *		assert(temp1 != MIN_WORD);
	 *
	 *		L_temp   = GSM_L_MULT( temp1, temp1 );
	 *		L_result = GSM_L_ADD( L_temp, L_result );
	 *	}
	 * 
	 *	if (L_result > EM) {
	 *		Mc = m;
	 *		EM = L_result;
	 *	}
	 * }
	 */
#undef	STEP
#define	STEP( m, i )		L_temp = SASR( x[m + 3 * i], 2 );	\
				L_result += L_temp * L_temp;
/* common part of 0 and 3 */
  L_result = 0;
  L_temp = (x[0 + 3 * 1] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[0 + 3 * 2] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[0 + 3 * 3] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[0 + 3 * 4] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[0 + 3 * 5] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[0 + 3 * 6] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[0 + 3 * 7] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[0 + 3 * 8] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[0 + 3 * 9] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[0 + 3 * 10] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[0 + 3 * 11] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[0 + 3 * 12] >> 2);
  L_result += (L_temp * L_temp);;
  L_common_0_3 = L_result;
/* i = 0 */
  L_temp = (x[0 + 3 * 0] >> 2);
  L_result += (L_temp * L_temp);;
/* implicit in L_MULT */
  L_result <<= 1;
  EM = L_result;
/* i = 1 */
  L_result = 0;
  L_temp = (x[1 + 3 * 0] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[1 + 3 * 1] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[1 + 3 * 2] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[1 + 3 * 3] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[1 + 3 * 4] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[1 + 3 * 5] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[1 + 3 * 6] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[1 + 3 * 7] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[1 + 3 * 8] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[1 + 3 * 9] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[1 + 3 * 10] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[1 + 3 * 11] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[1 + 3 * 12] >> 2);
  L_result += (L_temp * L_temp);;
  L_result <<= 1;
  if (L_result > EM) {
    Mc = 1;
    EM = L_result;
  }
/* i = 2 */
  L_result = 0;
  L_temp = (x[2 + 3 * 0] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[2 + 3 * 1] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[2 + 3 * 2] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[2 + 3 * 3] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[2 + 3 * 4] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[2 + 3 * 5] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[2 + 3 * 6] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[2 + 3 * 7] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[2 + 3 * 8] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[2 + 3 * 9] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[2 + 3 * 10] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[2 + 3 * 11] >> 2);
  L_result += (L_temp * L_temp);;
  L_temp = (x[2 + 3 * 12] >> 2);
  L_result += (L_temp * L_temp);;
  L_result <<= 1;
  if (L_result > EM) {
    Mc = 2;
    EM = L_result;
  }
/* i = 3 */
  L_result = L_common_0_3;
  L_temp = (x[3 + 3 * 12] >> 2);
  L_result += (L_temp * L_temp);;
  L_result <<= 1;
  if (L_result > EM) {
    Mc = 3;
    EM = L_result;
  }
/**/
/*  Down-sampling by a factor 3 to get the selected xM[0..12]
	 *  RPE sequence.
	 */
  for (i = 0; i <= 12; i++) 
    xM[i] = x[Mc + (3 * i)];
   *Mc_out = Mc;
}
/* 4.12.15 */

static void APCM_quantization_xmaxc_to_exp_mant(
/* IN 	*/
word xmaxc,
/* OUT	*/
word *exp_out,
/* OUT  */
word *mant_out)
{
  word exp;
  word mant;
/* Compute exponent and mantissa of the decoded version of xmaxc
	 */
  exp = 0;
  if (xmaxc > 15) 
    exp = ((xmaxc >> 3) - 1);
  mant = (xmaxc - (exp << 3));
  if (mant == 0) {
    exp = (-4);
    mant = 7;
  }
  else {
    while(mant <= 7){
      mant = ((mant << 1) | 1);
      exp--;
    }
    mant -= 8;
  }
  ((exp >= -4) && (exp <= 6))?((void )0) : __assert_fail("exp >= -4 && exp <= 6","rpe.c",248,"void APCM_quantization_xmaxc_to_exp_mant(short, short *, short *)");
  ((mant >= 0) && (mant <= 7))?((void )0) : __assert_fail("mant >= 0 && mant <= 7","rpe.c",249,"void APCM_quantization_xmaxc_to_exp_mant(short, short *, short *)");
   *exp_out = exp;
   *mant_out = mant;
}

static void APCM_quantization(
/* [0..12]		IN	*/
word *xM,
/* [0..12]		OUT	*/
word *xMc,
/* 			OUT	*/
word *mant_out,
/*			OUT	*/
word *exp_out,
/*			OUT	*/
word *xmaxc_out)
{
  int i;
  int itest;
  word xmax;
  word xmaxc;
  word temp;
  word temp1;
  word temp2;
  word exp;
  word mant;
/*  Find the maximum absolute value xmax of xM[0..12].
	 */
  xmax = 0;
  for (i = 0; i <= 12; i++) {
    temp = xM[i];
    temp = (((temp < 0)?(((temp == -32767 - 1)?32767 : -temp)) : temp));
    if (temp > xmax) 
      xmax = temp;
  }
/*  Qantizing and coding of xmax to get xmaxc.
	 */
  exp = 0;
  temp = (xmax >> 9);
  itest = 0;
  for (i = 0; i <= 5; i++) {
    itest |= (temp <= 0);
    temp = (temp >> 1);
    (exp <= 5)?((void )0) : __assert_fail("exp <= 5","rpe.c",292,"void APCM_quantization(short *, short *, short *, short *, short *)");
/* exp = add (exp, 1) */
    if (itest == 0) 
      exp++;
  }
  ((exp <= 6) && (exp >= 0))?((void )0) : __assert_fail("exp <= 6 && exp >= 0","rpe.c",296,"void APCM_quantization(short *, short *, short *, short *, short *)");
  temp = (exp + 5);
  ((temp <= 11) && (temp >= 0))?((void )0) : __assert_fail("temp <= 11 && temp >= 0","rpe.c",299,"void APCM_quantization(short *, short *, short *, short *, short *)");
  xmaxc = lsx_gsm_add((xmax >> temp),(exp << 3));
/*   Quantizing and coding of the xM[0..12] RPE sequence
	 *   to get the xMc[0..12]
	 */
  APCM_quantization_xmaxc_to_exp_mant(xmaxc,&exp,&mant);
/*  This computation uses the fact that the decoded version of xmaxc
	 *  can be calculated by using the exponent and the mantissa part of
	 *  xmaxc (logarithmic table).
	 *  So, this method avoids any division and uses only a scaling
	 *  of the RPE samples by a function of the exponent.  A direct 
	 *  multiplication by the inverse of the mantissa (NRFAC[0..7]
	 *  found in table 4.5) gives the 3 bit coded version xMc[0..12]
	 *  of the RPE samples.
	 */
/* Direct computation of xMc[0..12] using table 4.5
	 */
  ((exp <= 4096) && (exp >= -4096))?((void )0) : __assert_fail("exp <= 4096 && exp >= -4096","rpe.c",322,"void APCM_quantization(short *, short *, short *, short *, short *)");
  ((mant >= 0) && (mant <= 7))?((void )0) : __assert_fail("mant >= 0 && mant <= 7","rpe.c",323,"void APCM_quantization(short *, short *, short *, short *, short *)");
/* normalization by the exponent */
  temp1 = (6 - exp);
/* inverse mantissa 		 */
  temp2 = lsx_gsm_NRFAC[mant];
  for (i = 0; i <= 12; i++) {
    ((temp1 >= 0) && (temp1 < 16))?((void )0) : __assert_fail("temp1 >= 0 && temp1 < 16","rpe.c",330,"void APCM_quantization(short *, short *, short *, short *, short *)");
    temp = (xM[i] << temp1);
    temp = ((((longword )temp) * ((longword )temp2)) >> 15);
    temp = (temp >> 12);
/* see note below */
    xMc[i] = (temp + 4);
  }
/*  NOTE: This equation is used to make all the xMc[i] positive.
	 */
   *mant_out = mant;
   *exp_out = exp;
   *xmaxc_out = xmaxc;
}
/* 4.2.16 */

static void APCM_inverse_quantization(
/* [0..12]			IN 	*/
register word *xMc,word mant,word exp,
/* [0..12]			OUT 	*/
register word *xMp)
/* 
 *  This part is for decoding the RPE sequence of coded xMc[0..12]
 *  samples to obtain the xMp[0..12] array.  Table 4.6 is used to get
 *  the mantissa of xmaxc (FAC[0..7]).
 */
{
  int i;
  word temp;
  word temp1;
  word temp2;
  word temp3;
  longword ltmp;
  ((mant >= 0) && (mant <= 7))?((void )0) : __assert_fail("mant >= 0 && mant <= 7","rpe.c",363,"void APCM_inverse_quantization(short *, short, short, short *)");
/* see 4.2-15 for mant */
  temp1 = lsx_gsm_FAC[mant];
/* see 4.2-15 for exp  */
  temp2 = lsx_gsm_sub(6,exp);
  temp3 = lsx_gsm_asl(1,(lsx_gsm_sub(temp2,1)));
  for (i = 13; i-- != 0; ) {
/* 3 bit unsigned */
    ((( *xMc) <= 7) && (( *xMc) >= 0))?((void )0) : __assert_fail("*xMc <= 7 && *xMc >= 0","rpe.c",371,"void APCM_inverse_quantization(short *, short, short, short *)");
/* temp = gsm_sub( *xMc++ << 1, 7 ); */
/* restore sign   */
    temp = ((( *(xMc++)) << 1) - 7);
/* 4 bit signed   */
    ((temp <= 7) && (temp >= -7))?((void )0) : __assert_fail("temp <= 7 && temp >= -7","rpe.c",375,"void APCM_inverse_quantization(short *, short, short, short *)");
/* 16 bit signed  */
    temp <<= 12;
    temp = (((((longword )temp1) * ((longword )temp)) + 16384) >> 15);
    temp = (((((ulongword )((ltmp = (((longword )temp) + ((longword )temp3))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
     *(xMp++) = lsx_gsm_asr(temp,temp2);
  }
}
/* 4.2.17 */

static void RPE_grid_positioning(
/* grid position	IN	*/
word Mc,
/* [0..12]		IN	*/
register word *xMp,
/* [0..39]		OUT	*/
register word *ep)
/*
 *  This procedure computes the reconstructed long term residual signal
 *  ep[0..39] for the LTP analysis filter.  The inputs are the Mc
 *  which is the grid position selection and the xMp[0..12] decoded
 *  RPE samples which are upsampled by a factor of 3 by inserting zero
 *  values.
 */
{
  int i = 13;
  ((0 <= Mc) && (Mc <= 3))?((void )0) : __assert_fail("0 <= Mc && Mc <= 3","rpe.c",401,"void RPE_grid_positioning(short, short *, short *)");
  switch(Mc){
    case 3:
{
       *(ep++) = 0;
    }
    case 2:
{
      do {
         *(ep++) = 0;
        case 1:
{
        }
         *(ep++) = 0;
        case 0:
{
        }
         *(ep++) =  *(xMp++);
      }while (--i != 0);
    }
  }
  while((++Mc) < 4)
     *(ep++) = 0;
/*
	int i, k;
	for (k = 0; k <= 39; k++) ep[k] = 0;
	for (i = 0; i <= 12; i++) {
		ep[ Mc + (3*i) ] = xMp[i];
	}
	*/
}
/* 4.2.18 */
/*  This procedure adds the reconstructed long term residual signal
 *  ep[0..39] to the estimated signal dpp[0..39] from the long term
 *  analysis filter to compute the reconstructed short term residual
 *  signal dp[-40..-1]; also the reconstructed short term residual
 *  array dp[-120..-41] is updated.
 */
#if 0	/* Has been inlined in code.c */
/* [0...39]	IN	*/
/* [0...39]	IN	*/
/* [-120...-1]  IN/OUT 	*/
#endif	/* Has been inlined in code.c */

void lsx_Gsm_RPE_Encoding(struct gsm_state *S,
/* -5..-1][0..39][40..44	IN/OUT  */
word *e,
/* 				OUT */
word *xmaxc,
/* 			  	OUT */
word *Mc,
/* [0..12]			OUT */
word *xMc)
{
  word x[40UL];
  word xM[13UL];
  word xMp[13UL];
  word mant;
  word exp;
/* Denotes intentionally unused */
  S;
  Weighting_filter(e,x);
  RPE_grid_selection(x,xM,Mc);
  APCM_quantization(xM,xMc,&mant,&exp,xmaxc);
  APCM_inverse_quantization(xMc,mant,exp,xMp);
  RPE_grid_positioning( *Mc,xMp,e);
}

void lsx_Gsm_RPE_Decoding(struct gsm_state *S,word xmaxcr,word Mcr,
/* [0..12], 3 bits 		IN	*/
word *xMcr,
/* [0..39]			OUT 	*/
word *erp)
{
  word exp;
  word mant;
  word xMp[13UL];
/* Denotes intentionally unused */
  S;
  APCM_quantization_xmaxc_to_exp_mant(xmaxcr,&exp,&mant);
  APCM_inverse_quantization(xMcr,mant,exp,xMp);
  RPE_grid_positioning(Mcr,xMp,erp);
}
