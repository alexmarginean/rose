/*
 * Copyright 1992 by Jutta Degener and Carsten Bormann, Technische
 * Universitaet Berlin.  See the accompanying file "COPYRIGHT" for
 * details.  THERE IS ABSOLUTELY NO WARRANTY FOR THIS SOFTWARE.
 */
/* $Header: /cvsroot/sox/sox/libgsm/add.c,v 1.1 2007/09/06 16:50:55 cbagwell Exp $ */
/*
 *  See private.h for the more commonly used macro versions.
 */
#include	<stdio.h>
#include	<assert.h>
#include	"private.h"
#include	"gsm.h"
#define	saturate(x) 	\
	((x) < MIN_WORD ? MIN_WORD : (x) > MAX_WORD ? MAX_WORD: (x))

word lsx_gsm_add(word a,word b)
{
  longword sum = (((longword )a) + ((longword )b));
  return ((sum < (-32767 - 1))?(-32767 - 1) : (((sum > 32767)?32767 : sum)));
}

word lsx_gsm_sub(word a,word b)
{
  longword diff = (((longword )a) - ((longword )b));
  return ((diff < (-32767 - 1))?(-32767 - 1) : (((diff > 32767)?32767 : diff)));
}

word lsx_gsm_mult(word a,word b)
{
  if ((a == -32767 - 1) && (b == -32767 - 1)) 
    return 32767;
  else 
    return ((((longword )a) * ((longword )b)) >> 15);
}

word lsx_gsm_mult_r(word a,word b)
{
  if ((b == -32767 - 1) && (a == -32767 - 1)) 
    return 32767;
  else {
    longword prod = ((((longword )a) * ((longword )b)) + 16384);
    prod >>= 15;
    return (prod & 0xFFFF);
  }
}

word lsx_gsm_abs(word a)
{
  return ((a < 0)?(((a == -32767 - 1)?32767 : -a)) : a);
}

longword lsx_gsm_L_mult(word a,word b)
{
  ((a != -32767 - 1) || (b != -32767 - 1))?((void )0) : __assert_fail("a != (-32767 - 1) || b != (-32767 - 1)","add.c",57,"long lsx_gsm_L_mult(short, short)");
  return (((longword )a) * ((longword )b)) << 1;
}

longword lsx_gsm_L_add(longword a,longword b)
{
  if (a < 0) {
    if (b >= 0) 
      return a + b;
    else {
      ulongword A = (((ulongword )(-(a + 1))) + ((ulongword )(-(b + 1))));
      return (A >= 2147483647)?(-2147483647 - 1) : (-((longword )A) - 2);
    }
  }
  else if (b <= 0) 
    return a + b;
  else {
    ulongword A = (((ulongword )a) + ((ulongword )b));
    return ((A > 2147483647)?2147483647 : A);
  }
}

longword lsx_gsm_L_sub(longword a,longword b)
{
  if (a >= 0) {
    if (b >= 0) 
      return a - b;
    else {
/* a>=0, b<0 */
      ulongword A = (((ulongword )a) + (-(b + 1)));
      return ((A >= 2147483647)?2147483647 : (A + 1));
    }
  }
  else if (b <= 0) 
    return a - b;
  else {
/* a<0, b>0 */
    ulongword A = (((ulongword )(-(a + 1))) + b);
    return (A >= 2147483647)?(-2147483647 - 1) : (-((longword )A) - 1);
  }
}
static const unsigned char bitoff[256UL] = {(8), (7), (6), (6), (5), (5), (5), (5), (4), (4), (4), (4), (4), (4), (4), (4), (3), (3), (3), (3), (3), (3), (3), (3), (3), (3), (3), (3), (3), (3), (3), (3), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (2), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (1), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0), (0)};

word lsx_gsm_norm(longword a)
/*
 * the number of left shifts needed to normalize the 32 bit
 * variable L_var1 for positive values on the interval
 *
 * with minimum of
 * minimum of 1073741824  (01000000000000000000000000000000) and 
 * maximum of 2147483647  (01111111111111111111111111111111)
 *
 *
 * and for negative values on the interval with
 * minimum of -2147483648 (-10000000000000000000000000000000) and
 * maximum of -1073741824 ( -1000000000000000000000000000000).
 *
 * in order to normalize the result, the following
 * operation must be done: L_norm_var1 = L_var1 << norm( L_var1 );
 *
 * (That's 'ffs', only from the left, not the right..)
 */
{
  (a != 0)?((void )0) : __assert_fail("a != 0","add.c",136,"short lsx_gsm_norm(long)");
  if (a < 0) {
    if (a <= (-1073741824)) 
      return 0;
    a = ~a;
  }
  return (((a & 0xffff0000) != 0L)?((((a & 0xff000000) != 0L)?(-1 + bitoff[0xFF & (a >> 24)]) : (7 + bitoff[0xFF & (a >> 16)]))) : ((((a & 0xff00) != 0L)?(15 + bitoff[0xFF & (a >> 8)]) : (23 + bitoff[0xFF & a]))));
}

longword lsx_gsm_L_asl(longword a,int n)
{
  if (n >= 32) 
    return 0;
  if (n <= -32) 
    return (-(a < 0));
  if (n < 0) 
    return lsx_gsm_L_asr(a,-n);
  return a << n;
}

word lsx_gsm_asl(word a,int n)
{
  if (n >= 16) 
    return 0;
  if (n <= -16) 
    return (-(a < 0));
  if (n < 0) 
    return lsx_gsm_asr(a,-n);
  return (a << n);
}

longword lsx_gsm_L_asr(longword a,int n)
{
  if (n >= 32) 
    return (-(a < 0));
  if (n <= -32) 
    return 0;
  if (n < 0) 
    return a << -n;
#	ifdef	SASR
  return a >> n;
#	else
#	endif
}

word lsx_gsm_asr(word a,int n)
{
  if (n >= 16) 
    return (-(a < 0));
  if (n <= -16) 
    return 0;
  if (n < 0) 
    return (a << -n);
#	ifdef	SASR
  return (a >> n);
#	else
#	endif
}
/* 
 *  (From p. 46, end of section 4.2.5)
 *
 *  NOTE: The following lines gives [sic] one correct implementation
 *  	  of the div(num, denum) arithmetic operation.  Compute div
 *        which is the integer division of num by denum: with denum
 *	  >= num > 0
 */

word lsx_gsm_div(word num,word denum)
{
  longword L_num = num;
  longword L_denum = denum;
  word div = 0;
  int k = 15;
/* The parameter num sometimes becomes zero.
	 * Although this is explicitly guarded against in 4.2.5,
	 * we assume that the result should then be zero as well.
	 */
/* assert(num != 0); */
  ((num >= 0) && (denum >= num))?((void )0) : __assert_fail("num >= 0 && denum >= num","add.c",219,"short lsx_gsm_div(short, short)");
  if (num == 0) 
    return 0;
  while(k-- != 0){
    div <<= 1;
    L_num <<= 1;
    if (L_num >= L_denum) {
      L_num -= L_denum;
      div++;
    }
  }
  return div;
}
