/*
 * Copyright 1992 by Jutta Degener and Carsten Bormann, Technische
 * Universitaet Berlin.  See the accompanying file "COPYRIGHT" for
 * details.  THERE IS ABSOLUTELY NO WARRANTY FOR THIS SOFTWARE.
 */
/* $Header: /cvsroot/sox/sox/libgsm/lpc.c,v 1.2 2007/11/04 16:32:36 robs Exp $ */
#include <stdio.h>
#include <assert.h>
#include "private.h"
#include "gsm.h"
/*
 *  4.2.4 .. 4.2.7 LPC ANALYSIS SECTION
 */
/* 4.2.4 */

static void Autocorrelation(
/* [0..159]	IN/OUT  */
word *s,
/* [0..8]	OUT     */
longword *L_ACF)
/*
 *  The goal is to compute the array L_ACF[k].  The signal s[i] must
 *  be scaled in order to avoid an overflow situation.
 */
{
  register int k;
  register int i;
  word temp;
  word smax;
  word scalauto;
#ifdef	USE_FLOAT_MUL
#endif
/*  Dynamic scaling of the array  s[0..159]
	 */
/*  Search for the maximum.
	 */
  smax = 0;
  for (k = 0; k <= 159; k++) {
    temp = (((s[k] < 0)?(((s[k] == -32767 - 1)?32767 : -s[k])) : s[k]));
    if (temp > smax) 
      smax = temp;
  }
/*  Computation of the scaling factor.
	 */
  if (smax == 0) 
    scalauto = 0;
  else {
    (smax > 0)?((void )0) : __assert_fail("smax > 0","lpc.c",54,"void Autocorrelation(short *, long *)");
/* sub(4,..) */
    scalauto = (4 - (lsx_gsm_norm((((longword )smax) << 16))));
  }
/*  Scaling of the array s[0...159]
	 */
  if (scalauto > 0) {
# ifdef USE_FLOAT_MUL
#   define SCALE(n)	\
	case n: for (k = 0; k <= 159; k++) \
			float_s[k] = (float)	\
				(s[k] = GSM_MULT_R(s[k], 16384 >> (n-1)));\
		break;
# else 
#   define SCALE(n)	\
	case n: for (k = 0; k <= 159; k++) \
			s[k] = GSM_MULT_R( s[k], 16384 >> (n-1) );\
		break;
# endif /* USE_FLOAT_MUL */
    switch(scalauto){
      case 1:
{
        for (k = 0; k <= 159; k++) 
          s[k] = (((((longword )s[k]) * ((longword )(16384 >> 1 - 1))) + 16384) >> 15);
        break; 
      }
      case 2:
{
        for (k = 0; k <= 159; k++) 
          s[k] = (((((longword )s[k]) * ((longword )(16384 >> 2 - 1))) + 16384) >> 15);
        break; 
      }
      case 3:
{
        for (k = 0; k <= 159; k++) 
          s[k] = (((((longword )s[k]) * ((longword )(16384 >> 3 - 1))) + 16384) >> 15);
        break; 
      }
      case 4:
{
        for (k = 0; k <= 159; k++) 
          s[k] = (((((longword )s[k]) * ((longword )(16384 >> 4 - 1))) + 16384) >> 15);
        break; 
      }
    }
# undef	SCALE
  }
# ifdef	USE_FLOAT_MUL
# endif
/*  Compute the L_ACF[..].
	 */
{
# ifdef	USE_FLOAT_MUL
#		define STEP(k)	 L_ACF[k] += (longword)(sl * sp[ -(k) ]);
# else
    word *sp = s;
    word sl =  *sp;
#		define STEP(k)	 L_ACF[k] += ((longword)sl * sp[ -(k) ]);
# endif
#	define NEXTI	 sl = *++sp
    for (k = 9; k-- != 0; L_ACF[k] = 0) ;
    L_ACF[0] += (((longword )sl) * sp[-0]);;
    sl =  *(++sp);
    L_ACF[0] += (((longword )sl) * sp[-0]);;
    L_ACF[1] += (((longword )sl) * sp[-1]);;
    sl =  *(++sp);
    L_ACF[0] += (((longword )sl) * sp[-0]);;
    L_ACF[1] += (((longword )sl) * sp[-1]);;
    L_ACF[2] += (((longword )sl) * sp[-2]);;
    sl =  *(++sp);
    L_ACF[0] += (((longword )sl) * sp[-0]);;
    L_ACF[1] += (((longword )sl) * sp[-1]);;
    L_ACF[2] += (((longword )sl) * sp[-2]);;
    L_ACF[3] += (((longword )sl) * sp[-3]);;
    sl =  *(++sp);
    L_ACF[0] += (((longword )sl) * sp[-0]);;
    L_ACF[1] += (((longword )sl) * sp[-1]);;
    L_ACF[2] += (((longword )sl) * sp[-2]);;
    L_ACF[3] += (((longword )sl) * sp[-3]);;
    L_ACF[4] += (((longword )sl) * sp[-4]);;
    sl =  *(++sp);
    L_ACF[0] += (((longword )sl) * sp[-0]);;
    L_ACF[1] += (((longword )sl) * sp[-1]);;
    L_ACF[2] += (((longword )sl) * sp[-2]);;
    L_ACF[3] += (((longword )sl) * sp[-3]);;
    L_ACF[4] += (((longword )sl) * sp[-4]);;
    L_ACF[5] += (((longword )sl) * sp[-5]);;
    sl =  *(++sp);
    L_ACF[0] += (((longword )sl) * sp[-0]);;
    L_ACF[1] += (((longword )sl) * sp[-1]);;
    L_ACF[2] += (((longword )sl) * sp[-2]);;
    L_ACF[3] += (((longword )sl) * sp[-3]);;
    L_ACF[4] += (((longword )sl) * sp[-4]);;
    L_ACF[5] += (((longword )sl) * sp[-5]);;
    L_ACF[6] += (((longword )sl) * sp[-6]);;
    sl =  *(++sp);
    L_ACF[0] += (((longword )sl) * sp[-0]);;
    L_ACF[1] += (((longword )sl) * sp[-1]);;
    L_ACF[2] += (((longword )sl) * sp[-2]);;
    L_ACF[3] += (((longword )sl) * sp[-3]);;
    L_ACF[4] += (((longword )sl) * sp[-4]);;
    L_ACF[5] += (((longword )sl) * sp[-5]);;
    L_ACF[6] += (((longword )sl) * sp[-6]);;
    L_ACF[7] += (((longword )sl) * sp[-7]);;
    for (i = 8; i <= 159; i++) {
      sl =  *(++sp);
      L_ACF[0] += (((longword )sl) * sp[-0]);;
      L_ACF[1] += (((longword )sl) * sp[-1]);;
      L_ACF[2] += (((longword )sl) * sp[-2]);;
      L_ACF[3] += (((longword )sl) * sp[-3]);;
      L_ACF[4] += (((longword )sl) * sp[-4]);;
      L_ACF[5] += (((longword )sl) * sp[-5]);;
      L_ACF[6] += (((longword )sl) * sp[-6]);;
      L_ACF[7] += (((longword )sl) * sp[-7]);;
      L_ACF[8] += (((longword )sl) * sp[-8]);;
    }
    for (k = 9; k-- != 0; L_ACF[k] <<= 1) ;
  }
/*   Rescaling of the array s[0..159]
	 */
  if (scalauto > 0) {
    (scalauto <= 4)?((void )0) : __assert_fail("scalauto <= 4","lpc.c",139,"void Autocorrelation(short *, long *)");
    for (k = 160; k-- != 0;  *(s++) <<= scalauto) ;
  }
}
#if defined(USE_FLOAT_MUL) && defined(FAST)
/* [0..159]	IN/OUT  */
/* [0..8]	OUT     */
#endif	/* defined (USE_FLOAT_MUL) && defined (FAST) */
/* 4.2.5 */

static void Reflection_coefficients(
/* 0...8	IN	*/
longword *L_ACF,
/* 0...7	OUT 	*/
register word *r)
{
  register int i;
  register int m;
  register int n;
  register word temp;
  register longword ltmp;
/* 0..8 */
  word ACF[9UL];
/* 0..8 */
  word P[9UL];
/* 2..8 */
  word K[9UL];
/*  Schur recursion with 16 bits arithmetic.
	 */
  if (L_ACF[0] == 0) {
    for (i = 8; i-- != 0;  *(r++) = 0) ;
    return ;
  }
  (L_ACF[0] != 0)?((void )0) : __assert_fail("L_ACF[0] != 0","lpc.c",194,"void Reflection_coefficients(long *, short *)");
  temp = lsx_gsm_norm(L_ACF[0]);
  ((temp >= 0) && (temp < 32))?((void )0) : __assert_fail("temp >= 0 && temp < 32","lpc.c",197,"void Reflection_coefficients(long *, short *)");
/* ? overflow ? */
  for (i = 0; i <= 8; i++) 
    ACF[i] = ((L_ACF[i] << temp) >> 16);
/*   Initialize array P[..] and K[..] for the recursion.
	 */
  for (i = 1; i <= 7; i++) 
    K[i] = ACF[i];
  for (i = 0; i <= 8; i++) 
    P[i] = ACF[i];
/*   Compute reflection coefficients
	 */
  for (n = 1; n <= 8; (n++ , r++)) {
    temp = P[1];
    temp = (((temp < 0)?(((temp == -32767 - 1)?32767 : -temp)) : temp));
    if (P[0] < temp) {
      for (i = n; i <= 8; i++) 
         *(r++) = 0;
      return ;
    }
     *r = lsx_gsm_div(temp,P[0]);
    (( *r) >= 0)?((void )0) : __assert_fail("*r >= 0","lpc.c",221,"void Reflection_coefficients(long *, short *)");
/* r[n] = sub(0, r[n]) */
    if (P[1] > 0) 
       *r = (-( *r));
    (( *r) != -32767 - 1)?((void )0) : __assert_fail("*r != (-32767 - 1)","lpc.c",223,"void Reflection_coefficients(long *, short *)");
    if (n == 8) 
      return ;
/*  Schur recursion
		 */
    temp = (((((longword )P[1]) * ((longword )( *r))) + 16384) >> 15);
    P[0] = (((((ulongword )((ltmp = (((longword )P[0]) + ((longword )temp))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
    for (m = 1; m <= (8 - n); m++) {
      temp = (((((longword )K[m]) * ((longword )( *r))) + 16384) >> 15);
      P[m] = (((((ulongword )((ltmp = (((longword )P[m + 1]) + ((longword )temp))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
      temp = (((((longword )P[m + 1]) * ((longword )( *r))) + 16384) >> 15);
      K[m] = (((((ulongword )((ltmp = (((longword )K[m]) + ((longword )temp))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
    }
  }
}
/* 4.2.6 */

static void Transformation_to_Log_Area_Ratios(
/* 0..7	   IN/OUT */
register word *r)
/*
 *  The following scaling for r[..] and LAR[..] has been used:
 *
 *  r[..]   = integer( real_r[..]*32768. ); -1 <= real_r < 1.
 *  LAR[..] = integer( real_LAR[..] * 16384 );
 *  with -1.625 <= real_LAR <= 1.625
 */
{
  register word temp;
  register int i;
/* Computation of the LAR[0..7] from the r[0..7]
	 */
  for (i = 1; i <= 8; (i++ , r++)) {
    temp =  *r;
    temp = (((temp < 0)?(((temp == -32767 - 1)?32767 : -temp)) : temp));
    (temp >= 0)?((void )0) : __assert_fail("temp >= 0","lpc.c",264,"void Transformation_to_Log_Area_Ratios(short *)");
    if (temp < 22118) {
      temp >>= 1;
    }
    else if (temp < 31130) {
      (temp >= 11059)?((void )0) : __assert_fail("temp >= 11059","lpc.c",269,"void Transformation_to_Log_Area_Ratios(short *)");
      temp -= 11059;
    }
    else {
      (temp >= 26112)?((void )0) : __assert_fail("temp >= 26112","lpc.c",272,"void Transformation_to_Log_Area_Ratios(short *)");
      temp -= 26112;
      temp <<= 2;
    }
     *r = (((( *r) < 0)?-temp : temp));
    (( *r) != -32767 - 1)?((void )0) : __assert_fail("*r != (-32767 - 1)","lpc.c",278,"void Transformation_to_Log_Area_Ratios(short *)");
  }
}
/* 4.2.7 */

static void Quantization_and_coding(
/* [0..7]	IN/OUT	*/
register word *LAR)
{
  register word temp;
  longword ltmp;
/*  This procedure needs four tables; the following equations
	 *  give the optimum scaling for the constants:
	 *  
	 *  A[0..7] = integer( real_A[0..7] * 1024 )
	 *  B[0..7] = integer( real_B[0..7] *  512 )
	 *  MAC[0..7] = maximum of the LARc[0..7]
	 *  MIC[0..7] = minimum of the LARc[0..7]
	 */
#	undef STEP
#	define	STEP( A, B, MAC, MIC )		\
		temp = GSM_MULT( A,   *LAR );	\
		temp = GSM_ADD(  temp,   B );	\
		temp = GSM_ADD(  temp, 256 );	\
		temp = SASR(     temp,   9 );	\
		*LAR  =  temp>MAC ? MAC - MIC : (temp<MIC ? 0 : temp - MIC); \
		LAR++;
  temp = ((((longword )20480) * ((longword )( *LAR))) >> 15);
  temp = (((((ulongword )((ltmp = (((longword )temp) + ((longword )0))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  temp = (((((ulongword )((ltmp = (((longword )temp) + ((longword )256))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  temp = (temp >> 9);
   *LAR = (((temp > 31)?31 - -32 : (((temp < -32)?0 : (temp - -32)))));
  LAR++;;
  temp = ((((longword )20480) * ((longword )( *LAR))) >> 15);
  temp = (((((ulongword )((ltmp = (((longword )temp) + ((longword )0))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  temp = (((((ulongword )((ltmp = (((longword )temp) + ((longword )256))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  temp = (temp >> 9);
   *LAR = (((temp > 31)?31 - -32 : (((temp < -32)?0 : (temp - -32)))));
  LAR++;;
  temp = ((((longword )20480) * ((longword )( *LAR))) >> 15);
  temp = (((((ulongword )((ltmp = (((longword )temp) + ((longword )2048))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  temp = (((((ulongword )((ltmp = (((longword )temp) + ((longword )256))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  temp = (temp >> 9);
   *LAR = (((temp > 15)?15 - -16 : (((temp < -16)?0 : (temp - -16)))));
  LAR++;;
  temp = ((((longword )20480) * ((longword )( *LAR))) >> 15);
  temp = (((((ulongword )((ltmp = (((longword )temp) + ((longword )(-2560)))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  temp = (((((ulongword )((ltmp = (((longword )temp) + ((longword )256))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  temp = (temp >> 9);
   *LAR = (((temp > 15)?15 - -16 : (((temp < -16)?0 : (temp - -16)))));
  LAR++;;
  temp = ((((longword )13964) * ((longword )( *LAR))) >> 15);
  temp = (((((ulongword )((ltmp = (((longword )temp) + ((longword )94))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  temp = (((((ulongword )((ltmp = (((longword )temp) + ((longword )256))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  temp = (temp >> 9);
   *LAR = (((temp > 7)?7 - -8 : (((temp < -8)?0 : (temp - -8)))));
  LAR++;;
  temp = ((((longword )15360) * ((longword )( *LAR))) >> 15);
  temp = (((((ulongword )((ltmp = (((longword )temp) + ((longword )(-1792)))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  temp = (((((ulongword )((ltmp = (((longword )temp) + ((longword )256))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  temp = (temp >> 9);
   *LAR = (((temp > 7)?7 - -8 : (((temp < -8)?0 : (temp - -8)))));
  LAR++;;
  temp = ((((longword )8534) * ((longword )( *LAR))) >> 15);
  temp = (((((ulongword )((ltmp = (((longword )temp) + ((longword )(-341)))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  temp = (((((ulongword )((ltmp = (((longword )temp) + ((longword )256))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  temp = (temp >> 9);
   *LAR = (((temp > 3)?3 - -4 : (((temp < -4)?0 : (temp - -4)))));
  LAR++;;
  temp = ((((longword )9036) * ((longword )( *LAR))) >> 15);
  temp = (((((ulongword )((ltmp = (((longword )temp) + ((longword )(-1144)))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  temp = (((((ulongword )((ltmp = (((longword )temp) + ((longword )256))) - (-32767 - 1))) > (32767 - (-32767 - 1)))?(((ltmp > 0)?32767 : -32767 - 1)) : ltmp));
  temp = (temp >> 9);
   *LAR = (((temp > 3)?3 - -4 : (((temp < -4)?0 : (temp - -4)))));
  LAR++;;
#	undef	STEP
}

void lsx_Gsm_LPC_Analysis(struct gsm_state *S,
/* 0..159 signals	IN/OUT	*/
word *s,
/* 0..7   LARc's	OUT	*/
word *LARc)
{
  longword L_ACF[9UL];
/* Denotes intentionally unused */
  S;
#if defined(USE_FLOAT_MUL) && defined(FAST)
#endif
  Autocorrelation(s,L_ACF);
  Reflection_coefficients(L_ACF,LARc);
  Transformation_to_Log_Area_Ratios(LARc);
  Quantization_and_coding(LARc);
}
