/* libSoX file format: DVMS (see cvsd.c)        (c) 2007-8 SoX contributors
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "cvsd.h"
const sox_format_handler_t *lsx_dvms_format_fn();

const sox_format_handler_t *lsx_dvms_format_fn()
{
  static const char *const names[] = {("dvms"), ("vms"), ((const char *)((void *)0))};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_CVSD), (1), (0), (0)};
  static const sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("MIL Std 188 113 Continuously Variable Slope Delta modulation with header"), (names), (256), (lsx_dvmsstartread), (lsx_cvsdread), (lsx_cvsdstopread), (lsx_dvmsstartwrite), (lsx_cvsdwrite), (lsx_dvmsstopwrite), ((sox_format_handler_seek )((void *)0)), (write_encodings), ((const sox_rate_t *)((void *)0)), ((sizeof(cvsd_priv_t )))};
  return &handler;
}
