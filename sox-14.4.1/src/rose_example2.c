/* Simple example of using SoX libraries
 *
 * Copyright (c) 2008 robs@users.sourceforge.net
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#ifdef NDEBUG /* N.B. assert used with active statements so enable always. */
#undef NDEBUG /* Must undef above assert.h or other that might include it. */
#endif
#include "sox.h"
#include "util.h"
#include <stdio.h>
#include <math.h>
#include <assert.h>
/*
 * Reads input file and displays a few seconds of wave-form, starting from
 * a given time through the audio.   E.g. example2 song2.au 30.75 1
 */

int main(int argc,char *argv[])
{
  sox_format_t *in;
  sox_sample_t *buf;
  size_t blocks;
  size_t block_size;
/* Period of audio over which we will measure its volume in order to
   * display the wave-form: */
/* seconds */
  static const double block_period = 0.025;
  double start_secs = 0;
  double period = 2;
  char dummy;
  uint64_t seek;
/* All libSoX applications must start by initialising the SoX library */
  (sox_init() == SOX_SUCCESS)?((void )0) : __assert_fail("sox_init() == SOX_SUCCESS","example2.c",47,"int main(int, char **)");
  (argc > 1)?((void )0) : __assert_fail("argc > 1","example2.c",49,"int main(int, char **)");
/* Move to 1st parameter */
  (++argv , --argc);
/* Open the input file (with default parameters) */
  ((in = sox_open_read(( *argv),0,0,0)) != 0)?((void )0) : __assert_fail("in = sox_open_read(*argv, ((void *)0), ((void *)0), ((void *)0))","example2.c",53,"int main(int, char **)");
/* Move past this parameter */
  (++argv , --argc);
/* If given, read the start time: */
  if (argc != 0) {
    (sscanf(( *argv),"%lf%c",&start_secs,&dummy) == 1)?((void )0) : __assert_fail("sscanf(*argv, \"%lf%c\", &start_secs, &dummy) == 1","example2.c",57,"int main(int, char **)");
/* Move past this parameter */
    (++argv , --argc);
  }
/* If given, read the period of time to display: */
  if (argc != 0) {
    (sscanf(( *argv),"%lf%c",&period,&dummy) == 1)?((void )0) : __assert_fail("sscanf(*argv, \"%lf%c\", &period, &dummy) == 1","example2.c",62,"int main(int, char **)");
/* Move past this parameter */
    (++argv , --argc);
  }
/* Calculate the start position in number of samples: */
  seek = (((start_secs * in -> signal.rate) * in -> signal.channels) + .5);
/* Make sure that this is at a `wide sample' boundary: */
  seek -= (seek % in -> signal.channels);
/* Move the file pointer to the desired starting position */
  (sox_seek(in,seek,0) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_seek(in, seek, 0) == SOX_SUCCESS","example2.c",71,"int main(int, char **)");
/* Convert block size (in seconds) to a number of samples: */
  block_size = (((block_period * in -> signal.rate) * in -> signal.channels) + .5);
/* Make sure that this is at a `wide sample' boundary: */
  block_size -= (block_size % in -> signal.channels);
/* Allocate a block of memory to store the block of audio samples: */
  ((buf = (malloc((sizeof(sox_sample_t ) * block_size)))) != 0)?((void )0) : __assert_fail("buf = malloc(sizeof(sox_sample_t) * block_size)","example2.c",78,"int main(int, char **)");
/* This example program requires that the audio has precisely 2 channels: */
  (in -> signal.channels == 2)?((void )0) : __assert_fail("in->signal.channels == 2","example2.c",81,"int main(int, char **)");
/* Read and process blocks of audio for the selected period or until EOF: */
  for (blocks = 0; (sox_read(in,buf,block_size) == block_size) && ((blocks * block_period) < period); ++blocks) {
    double left = 0;
    double right = 0;
    size_t i;
    static const char line[] = "===================================";
    int l;
    int r;
    for (i = 0; i < block_size; ++i) {
      sox_sample_t sox_macro_temp_sample;
      double sox_macro_temp_double;
/* convert the sample from SoX's internal format to a `double' for
       * processing in this application: */
      double sample = (buf[i] * (1. / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)));
/* The samples for each channel are interleaved; in this example
       * we allow only stereo audio, so the left channel audio can be found in
       * even-numbered samples, and the right channel audio in odd-numbered
       * samples: */
      if ((i & 1) != 0UL) 
/* Find the peak volume in the block */
        right = ((right >= fabs(sample))?right : fabs(sample));
      else 
/* Find the peak volume in the block */
        left = ((left >= fabs(sample))?left : fabs(sample));
    }
/* Build up the wave form by displaying the left & right channel
     * volume as a line length: */
    l = (((1 - left) * 35) + .5);
    r = (((1 - right) * 35) + .5);
    printf("%8.3f%36s|%s\n",(start_secs + (blocks * block_period)),(line + l),(line + r));
  }
/* All done; tidy up: */
  free(buf);
  sox_close(in);
  sox_quit();
  return 0;
}
