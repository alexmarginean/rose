/* libSoX file formats: raw         (c) 2007-8 SoX contributors
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include "raw.h"
static const char *names[] = {("la"), ((const char *)((void *)0))};

static int la_start(sox_format_t *ft)
{
  return lsx_rawstart(ft,sox_true,sox_true,sox_true,SOX_ENCODING_ALAW,8);
}
const sox_format_handler_t *lsx_la_format_fn();

const sox_format_handler_t *lsx_la_format_fn()
{
  static const unsigned int write_encodings[] = {(SOX_ENCODING_ALAW), (8), (0), (0)};
  static sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("Raw audio"), (names), (16), (la_start), (lsx_rawread), ((sox_format_handler_stopread )((void *)0)), (la_start), (lsx_rawwrite), ((sox_format_handler_stopwrite )((void *)0)), ((sox_format_handler_seek )((void *)0)), (write_encodings), ((const sox_rate_t *)((void *)0)), (0)};
  return (&handler);
}
