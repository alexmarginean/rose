/* libSoX ima_rw.c -- codex utilities for WAV_FORMAT_IMA_ADPCM
 * Copyright (C) 1999 Stanley J. Brooks <stabro@megsinet.net>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/
#include "sox_i.h"
#include "ima_rw.h"
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
/*
 *
 * Lookup tables for IMA ADPCM format
 *
 */
#define ISSTMAX 88
static const int imaStepSizeTable[88 + 1] = {(7), (8), (9), (10), (11), (12), (13), (14), (16), (17), (19), (21), (23), (25), (28), (31), (34), (37), (41), (45), (50), (55), (60), (66), (73), (80), (88), (97), (107), (118), (130), (143), (157), (173), (190), (209), (230), (253), (279), (307), (337), (371), (408), (449), (494), (544), (598), (658), (724), (796), (876), (963), (1060), (1166), (1282), (1411), (1552), (1707), (1878), (2066), (2272), (2499), (2749), (3024), (3327), (3660), (4026), (4428), (4871), (5358), (5894), (6484), (7132), (7845), (8630), (9493), (10442), (11487), (12635), (13899), (15289), (16818), (18500), (20350), (22385), (24623), (27086), (29794), (32767)};
#define imaStateAdjust(c) (((c)<4)? -1:(2*(c)-6))
/* +0 - +3, decrease step size */
/* +4 - +7, increase step size */
/* -0 - -3, decrease step size */
/* -4 - -7, increase step size */
static unsigned char imaStateAdjustTable[88 + 1][8UL];

void lsx_ima_init_table()
{
  int i;
  int j;
  int k;
  for (i = 0; i <= 88; i++) {
    for (j = 0; j < 8; j++) {
      k = (i + (((j < 4)?-1 : ((2 * j) - 6))));
      if (k < 0) 
        k = 0;
      else if (k > 88) 
        k = 88;
      imaStateAdjustTable[i][j] = k;
    }
  }
}

static void ImaExpandS(
/* channel number to decode, REQUIRE 0 <= ch < chans  */
unsigned int ch,
/* total channels             */
unsigned int chans,
/* input buffer[blockAlign]   */
const unsigned char *ibuff,
/* obuff[n] will be output samples */
short *obuff,
/* samples to decode PER channel, REQUIRE n % 8 == 1  */
int n,
/* index difference between successive output samples */
unsigned int o_inc)
{
  const unsigned char *ip;
  int i_inc;
  short *op;
  int i;
  int val;
  int state;
/* input pointer to 4-byte block state-initializer   */
  ip = (ibuff + (4 * ch));
/* amount by which to incr ip after each 4-byte read */
  i_inc = (4 * (chans - 1));
/* need cast for sign-extend */
  val = ((short )(ip[0] + (ip[1] << 8)));
  state = ip[2];
  if (state > 88) {
    ((( *sox_get_globals()).subsystem = "ima_rw.c") , lsx_warn_impl("IMA_ADPCM block ch%d initial-state (%d) out of range",ch,state));
    state = 0;
  }
/* specs say to ignore ip[3] , but write it as 0 */
  ip += (4 + i_inc);
  op = obuff;
/* 1st output sample for this channel */
   *op = val;
  op += o_inc;
  for (i = 1; i < n; i++) {
    int step;
    int dp;
    int c;
    int cm;
/* 1st of pair */
    if ((i & 1) != 0) {
      cm = (( *ip) & 15);
    }
    else {
      cm = (( *(ip++)) >> 4);
/* ends the 8-sample input block for this channel */
      if ((i & 7) == 0) 
/* skip ip for next group */
        ip += i_inc;
    }
    step = imaStepSizeTable[state];
/* Update the state for the next sample */
    c = (cm & 7);
    state = imaStateAdjustTable[state][c];
    dp = 0;
    if ((c & 4) != 0) 
      dp += step;
    step = (step >> 1);
    if ((c & 2) != 0) 
      dp += step;
    step = (step >> 1);
    if ((c & 1) != 0) 
      dp += step;
    step = (step >> 1);
    dp += step;
    if (c != cm) {
      val -= dp;
      if (val < -0x8000) 
        val = -0x8000;
    }
    else {
      val += dp;
      if (val > 0x7fff) 
        val = 0x7fff;
    }
     *op = val;
    op += o_inc;
  }
}
/* lsx_ima_block_expand_i() outputs interleaved samples into one output buffer */

void lsx_ima_block_expand_i(
/* total channels             */
unsigned int chans,
/* input buffer[blockAlign]   */
const unsigned char *ibuff,
/* output samples, n*chans    */
short *obuff,
/* samples to decode PER channel, REQUIRE n % 8 == 1  */
int n)
{
  unsigned int ch;
  for (ch = 0; ch < chans; ch++) 
    ImaExpandS(ch,chans,ibuff,(obuff + ch),n,chans);
}
/* lsx_ima_block_expand_m() outputs non-interleaved samples into chan separate output buffers */

void lsx_ima_block_expand_m(
/* total channels             */
unsigned int chans,
/* input buffer[blockAlign]   */
const unsigned char *ibuff,
/* chan output sample buffers, each takes n samples */
short **obuffs,
/* samples to decode PER channel, REQUIRE n % 8 == 1  */
int n)
{
  unsigned int ch;
  for (ch = 0; ch < chans; ch++) 
    ImaExpandS(ch,chans,ibuff,obuffs[ch],n,1);
}

static int ImaMashS(
/* channel number to encode, REQUIRE 0 <= ch < chans  */
unsigned int ch,
/* total channels */
unsigned int chans,
/* value to use as starting prediction0 */
int v0,
/* ibuff[] is interleaved input samples */
const short *ibuff,
/* samples to encode PER channel, REQUIRE n % 8 == 1 */
int n,
/* input/output state, REQUIRE 0 <= *st <= ISSTMAX */
int *st,
/* output buffer[blockAlign], or NULL for no output  */
unsigned char *obuff)
{
  const short *ip;
  const short *itop;
  unsigned char *op;
/* set 0 only to shut up gcc's 'might be uninitialized' */
  int o_inc = 0;
  int i;
  int val;
  int state;
/* long long is okay also, speed abt the same */
  double d2;
/* point ip to 1st input sample for this channel */
  ip = (ibuff + ch);
  itop = (ibuff + (n * chans));
/* 1st input sample for this channel */
  val = (( *ip) - v0);
  ip += chans;
/* d2 will be sum of squares of errors, given input v0 and *st */
  d2 = (val * val);
  val = v0;
/* output pointer (or NULL) */
  op = obuff;
/* NULL means don't output, just compute the rms error */
  if (op != 0) {
/* where to put this channel's 4-byte block state-initializer */
    op += (4 * ch);
/* amount by which to incr op after each 4-byte written */
    o_inc = (4 * (chans - 1));
     *(op++) = val;
     *(op++) = (val >> 8);
/* they could have put a mid-block state-correction here  */
     *(op++) = ( *st);
     *(op++) = 0;
/* _sigh_   NEVER waste a byte.      It's a rule!         */
    op += o_inc;
  }
  state =  *st;
  for (i = 0; ip < itop; ip += chans) {
    int step;
    int d;
    int dp;
    int c;
/* difference between last prediction and current sample */
    d = (( *ip) - val);
    step = imaStepSizeTable[state];
    c = ((abs(d) << 2) / step);
    if (c > 7) 
      c = 7;
/* Update the state for the next sample */
    state = imaStateAdjustTable[state][c];
/* if we want output, put it in proper place */
    if (op != 0) {
      int cm = c;
      if (d < 0) 
        cm |= 8;
/* odd numbered output */
      if ((i & 1) != 0) {
         *(op++) |= (cm << 4);
/* ends the 8-sample output block for this channel */
        if (i == 7) 
/* skip op for next group */
          op += o_inc;
      }
      else {
         *op = cm;
      }
      i = ((i + 1) & 7);
    }
    dp = 0;
    if ((c & 4) != 0) 
      dp += step;
    step = (step >> 1);
    if ((c & 2) != 0) 
      dp += step;
    step = (step >> 1);
    if ((c & 1) != 0) 
      dp += step;
    step = (step >> 1);
    dp += step;
    if (d < 0) {
      val -= dp;
      if (val < -0x8000) 
        val = -0x8000;
    }
    else {
      val += dp;
      if (val > 0x7fff) 
        val = 0x7fff;
    }
{
      int x = (( *ip) - val);
      d2 += (x * x);
    }
  }
/* be sure it's non-negative */
  d2 /= n;
   *st = state;
  return (int )(sqrt(d2));
}
/* mash one channel... if you want to use opt>0, 9 is a reasonable value */

inline static void ImaMashChannel(
/* channel number to encode, REQUIRE 0 <= ch < chans  */
unsigned int ch,
/* total channels */
unsigned int chans,
/* ip[] is interleaved input samples */
const short *ip,
/* samples to encode PER channel, REQUIRE n % 8 == 1 */
int n,
/* input/output state, REQUIRE 0 <= *st <= ISSTMAX */
int *st,
/* output buffer[blockAlign] */
unsigned char *obuff,
/* non-zero allows some cpu-intensive code to improve output */
int opt)
{
  int snext;
  int s0;
  int d0;
  s0 =  *st;
  if (opt > 0) {
    int low;
    int hi;
    int w;
    int low0;
    int hi0;
    snext = s0;
    d0 = ImaMashS(ch,chans,ip[0],ip,n,&snext,0);
    w = 0;
    low = (hi = s0);
    low0 = (low - opt);
    if (low0 < 0) 
      low0 = 0;
    hi0 = (hi + opt);
    if (hi0 > 88) 
      hi0 = 88;
    while((low > low0) || (hi < hi0)){
      if (!(w != 0) && (low > low0)) {
        int d2;
        snext = --low;
        d2 = ImaMashS(ch,chans,ip[0],ip,n,&snext,0);
        if (d2 < d0) {
          d0 = d2;
          s0 = low;
          low0 = (low - opt);
          if (low0 < 0) 
            low0 = 0;
          hi0 = (low + opt);
          if (hi0 > 88) 
            hi0 = 88;
        }
      }
      if ((w != 0) && (hi < hi0)) {
        int d2;
        snext = ++hi;
        d2 = ImaMashS(ch,chans,ip[0],ip,n,&snext,0);
        if (d2 < d0) {
          d0 = d2;
          s0 = hi;
          low0 = (hi - opt);
          if (low0 < 0) 
            low0 = 0;
          hi0 = (hi + opt);
          if (hi0 > 88) 
            hi0 = 88;
        }
      }
      w = (1 - w);
    }
     *st = s0;
  }
  ImaMashS(ch,chans,ip[0],ip,n,st,obuff);
}
/* mash one block.  if you want to use opt>0, 9 is a reasonable value */

void lsx_ima_block_mash_i(
/* total channels */
unsigned int chans,
/* ip[] is interleaved input samples */
const short *ip,
/* samples to encode PER channel, REQUIRE n % 8 == 1 */
int n,
/* input/output state, REQUIRE 0 <= *st <= ISSTMAX */
int *st,
/* output buffer[blockAlign] */
unsigned char *obuff,
/* non-zero allows some cpu-intensive code to improve output */
int opt)
{
  unsigned int ch;
  for (ch = 0; ch < chans; ch++) 
    ImaMashChannel(ch,chans,ip,n,(st + ch),obuff,opt);
}
/*
 * lsx_ima_samples_in(dataLen, chans, blockAlign, samplesPerBlock)
 *  returns the number of samples/channel which would go
 *  in the dataLen, given the other parameters ...
 *  if input samplesPerBlock is 0, then returns the max
 *  samplesPerBlock which would go into a block of size blockAlign
 *  Yes, it is confusing.
 */

size_t lsx_ima_samples_in(size_t dataLen,size_t chans,size_t blockAlign,size_t samplesPerBlock)
{
  size_t m;
  size_t n;
  if (samplesPerBlock != 0UL) {
    n = ((dataLen / blockAlign) * samplesPerBlock);
    m = (dataLen % blockAlign);
  }
  else {
    n = 0;
    m = blockAlign;
  }
  if (m >= (((size_t )4) * chans)) {
/* number of bytes beyond block-header */
    m -= (4 * chans);
/* number of 4-byte blocks/channel beyond header */
    m /= (4 * chans);
/* samples/chan beyond header + 1 in header */
    m = ((8 * m) + 1);
    if ((samplesPerBlock != 0UL) && (m > samplesPerBlock)) 
      m = samplesPerBlock;
    n += m;
  }
  return n;
/*wSamplesPerBlock = ((wBlockAlign - 4*wChannels)/(4*wChannels))*8 + 1;*/
}
/*
 * size_t lsx_ima_bytes_per_block(chans, samplesPerBlock)
 *   return minimum blocksize which would be required
 *   to encode number of chans with given samplesPerBlock
 */

size_t lsx_ima_bytes_per_block(size_t chans,size_t samplesPerBlock)
{
  size_t n;
/* per channel, ima has blocks of len 4, the 1st has 1st sample, the others
   * up to 8 samples per block,
   * so number of later blocks is (nsamp-1 + 7)/8, total blocks/chan is
   * (nsamp-1+7)/8 + 1 = (nsamp+14)/8
   */
  n = ((((((size_t )samplesPerBlock) + 14) / 8) * 4) * chans);
  return n;
}
