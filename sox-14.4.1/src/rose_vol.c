/* Copyright (c) 20/03/2000 Fabien COELHO <fabien@coelho.net>
 * Copyright (c) 2000-2007 SoX contributors
 *
 * SoX vol effect; change volume with basic linear amplitude formula.
 * Beware of saturations!  Clipping is checked and reported.
 *
 * FIXME: deprecate or remove the limiter in favour of compand.
 */
#define vol_usage \
  "GAIN [TYPE [LIMITERGAIN]]\n" \
  "\t(default TYPE=amplitude: 1 is constant, < 0 change phase;\n" \
  "\tTYPE=power 1 is constant; TYPE=dB: 0 is constant, +6 doubles ampl.)\n" \
  "\tThe peak limiter has a gain much less than 1 (e.g. 0.05 or 0.02) and\n" \
  "\tis only used on peaks (to prevent clipping); default is no limiter."
#include "sox_i.h"
typedef struct __unnamed_class___F0_L18_C9_unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__gain__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__uselimiter__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__limiterthreshhold__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__limitergain__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__limited__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__totalprocessed {
/* amplitude gain. */
double gain;
sox_bool uselimiter;
double limiterthreshhold;
double limitergain;
/* number of limited values to report. */
uint64_t limited;
uint64_t totalprocessed;}priv_t;
enum __unnamed_enum___F0_L27_C1_vol_amplitude__COMMA__vol_dB__COMMA__vol_power {vol_amplitude,vol_dB,vol_power};
static const lsx_enum_item vol_types[] = {{("amplitude"), (vol_amplitude)}, {("dB"), (vol_dB)}, {("power"), (vol_power)}, {(0), (0)}};
/*
 * Process options: gain (float) type (amplitude, power, dB)
 */

static int getopts(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *vol = (priv_t *)(effp -> priv);
  char type_string[11UL];
  char *type_ptr = type_string;
/* To check for extraneous chars. */
  char dummy;
  sox_bool have_type;
  (--argc , ++argv);
/* Default is no change. */
  vol -> gain = 1;
/* Default is no limiter. */
  vol -> uselimiter = sox_false;
/* Get the vol, and the type if it's in the same arg. */
  if (!(argc != 0) || ((have_type = (sscanf(argv[0],"%lf %10s %c",&vol -> gain,type_string,&dummy) - 1)) > 1)) 
    return lsx_usage(effp);
  (++argv , --argc);
/* No type yet? Get it from the next arg: */
  if (!(have_type != 0U) && (argc != 0)) {
    have_type = sox_true;
    type_ptr =  *argv;
    (++argv , --argc);
  }
  if (have_type != 0U) {
    const lsx_enum_item *p = lsx_find_enum_text(type_ptr,vol_types,0);
    if (!(p != 0)) 
      return lsx_usage(effp);
    switch(p -> value){
      case vol_dB:
{
        vol -> gain = exp((((vol -> gain) * 2.30258509299404568402) * 0.05));
        break; 
      }
/* power to amplitude, keep phase change */
      case vol_power:
{
        vol -> gain = (((vol -> gain) > 0)?sqrt((vol -> gain)) : -sqrt(-(vol -> gain)));
        break; 
      }
    }
  }
  if (argc != 0) {
    if ((((fabs((vol -> gain)) < 1) || (sscanf(( *argv),"%lf %c",&vol -> limitergain,&dummy) != 1)) || ((vol -> limitergain) <= 0)) || ((vol -> limitergain) >= 1)) 
      return lsx_usage(effp);
    vol -> uselimiter = sox_true;
/* The following equation is derived so that there is no
     * discontinuity in output amplitudes */
/* and a SOX_SAMPLE_MAX input always maps to a SOX_SAMPLE_MAX output
     * when the limiter is activated. */
/* (NOTE: There **WILL** be a discontinuity in the slope
     * of the output amplitudes when using the limiter.) */
    vol -> limiterthreshhold = ((((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) * (1.0 - (vol -> limitergain))) / (fabs((vol -> gain)) - (vol -> limitergain)));
  }
  ((( *sox_get_globals()).subsystem = "vol.c") , lsx_debug_impl("mult=%g limit=%g",(vol -> gain),(vol -> limitergain)));
  return SOX_SUCCESS;
}
/*
 * Start processing
 */

static int start(sox_effect_t *effp)
{
  priv_t *vol = (priv_t *)(effp -> priv);
  if ((vol -> gain) == 1) 
    return 32;
  vol -> limited = 0;
  vol -> totalprocessed = 0;
  return SOX_SUCCESS;
}
/*
 * Process data.
 */

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *vol = (priv_t *)(effp -> priv);
  register double gain = (vol -> gain);
  register double limiterthreshhold = (vol -> limiterthreshhold);
  register double sample;
  register size_t len;
  len = (( *osamp <=  *isamp)? *osamp :  *isamp);
/* report back dealt with amount. */
   *isamp = len;
   *osamp = len;
  if ((vol -> uselimiter) != 0U) {
    vol -> totalprocessed += len;
    for (; len > 0; len--) {
      sample = ( *(ibuf++));
      if (sample > limiterthreshhold) {
        sample = (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - ((vol -> limitergain) * (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - sample)));
        vol -> limited++;
      }
      else if (sample < -limiterthreshhold) {
        sample = -(((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - ((vol -> limitergain) * (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + sample)));
/* FIXME: MIN is (-MAX)-1 so need to make sure we
                         * don't go over that.  Probably could do this
                         * check inside the above equation but I didn't
                         * think it thru.
                         */
        if (sample < ((sox_sample_t )(1 << 32 - 1))) 
          sample = ((sox_sample_t )(1 << 32 - 1));
        vol -> limited++;
      }
      else 
        sample = (gain * sample);
      do {
        if (sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32))) {
          sample = ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32));
          effp -> clips++;
        }
        else if (sample < ((sox_sample_t )(1 << 32 - 1))) {
          sample = ((sox_sample_t )(1 << 32 - 1));
          effp -> clips++;
        }
      }while (0);
       *(obuf++) = sample;
    }
  }
  else {
/* quite basic, with clipping */
    for (; len > 0; len--) {
      sample = (gain * ( *(ibuf++)));
      do {
        if (sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32))) {
          sample = ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32));
          effp -> clips++;
        }
        else if (sample < ((sox_sample_t )(1 << 32 - 1))) {
          sample = ((sox_sample_t )(1 << 32 - 1));
          effp -> clips++;
        }
      }while (0);
       *(obuf++) = sample;
    }
  }
  return SOX_SUCCESS;
}

static int stop(sox_effect_t *effp)
{
  priv_t *vol = (priv_t *)(effp -> priv);
  if ((vol -> limited) != 0UL) {
    ((( *sox_get_globals()).subsystem = "vol.c") , lsx_warn_impl("limited %lu values (%d percent).",(vol -> limited),((int )(((vol -> limited) * 100.0) / (vol -> totalprocessed)))));
  }
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_vol_effect_fn()
{
  static sox_effect_handler_t handler = {("vol"), ("GAIN [TYPE [LIMITERGAIN]]\n\t(default TYPE=amplitude: 1 is constant, < 0 change phase;\n\tTYPE=power 1 is constant; TYPE=dB: 0 is constant, +6 doubles ampl.)\n\tThe peak limiter has a gain much less than 1 (e.g. 0.05 or 0.02) and\n\tis only used on peaks (to prevent clipping); default is no limiter."), ((16 | 128)), (getopts), (start), (flow), (0), (stop), (0), ((sizeof(priv_t )))};
  return (&handler);
}
