/* Abstract effect: dft filter     Copyright (c) 2008 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include "fft4g.h"
#include "dft_filter.h"
#include <string.h>
typedef dft_filter_t filter_t;
typedef dft_filter_priv_t priv_t;

void lsx_set_dft_filter(dft_filter_t *f,double *h,int n,int post_peak)
{
  int i;
  f -> num_taps = n;
  f -> post_peak = post_peak;
  f -> dft_length = lsx_set_dft_length((f -> num_taps));
  f -> coefs = (((((f -> dft_length) * sizeof(( *(f -> coefs)))) != 0ULL)?memset(lsx_realloc(0,((f -> dft_length) * sizeof(( *(f -> coefs))))),0,((f -> dft_length) * sizeof(( *(f -> coefs))))) : ((void *)((void *)0))));
  for (i = 0; i < (f -> num_taps); ++i) 
    (f -> coefs)[(((i + (f -> dft_length)) - (f -> num_taps)) + 1) & ((f -> dft_length) - 1)] = ((h[i] / (f -> dft_length)) * 2);
  lsx_safe_rdft((f -> dft_length),1,(f -> coefs));
  free(h);
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  fifo_create(&p -> input_fifo,((int )(sizeof(double ))));
  memset(fifo_reserve(&p -> input_fifo,( *(p -> filter_ptr)).post_peak),0,(sizeof(double ) * ( *(p -> filter_ptr)).post_peak));
  fifo_create(&p -> output_fifo,((int )(sizeof(double ))));
  return SOX_SUCCESS;
}

static void filter(priv_t *p)
{
  int i;
  int num_in = (0 >= fifo_occupancy(&p -> input_fifo))?0 : fifo_occupancy(&p -> input_fifo);
  const filter_t *f = (p -> filter_ptr);
  const int overlap = ((f -> num_taps) - 1);
  double *output;
  while(num_in >= (f -> dft_length)){
    const double *input = (fifo_read(&p -> input_fifo,((int )0),0));
    fifo_read(&p -> input_fifo,((f -> dft_length) - overlap),0);
    num_in -= ((f -> dft_length) - overlap);
    output = (fifo_reserve(&p -> output_fifo,(f -> dft_length)));
    fifo_trim_by(&p -> output_fifo,overlap);
    memcpy(output,input,((f -> dft_length) * sizeof(( *output))));
    lsx_safe_rdft((f -> dft_length),1,output);
    output[0] *= (f -> coefs)[0];
    output[1] *= (f -> coefs)[1];
    for (i = 2; i < (f -> dft_length); i += 2) {
      double tmp = output[i];
      output[i] = (((f -> coefs)[i] * tmp) - ((f -> coefs)[i + 1] * output[i + 1]));
      output[i + 1] = (((f -> coefs)[i + 1] * tmp) + ((f -> coefs)[i] * output[i + 1]));
    }
    lsx_safe_rdft((f -> dft_length),-1,output);
  }
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t i;
  size_t odone = ( *osamp <= ((size_t )(fifo_occupancy(&p -> output_fifo))))? *osamp : ((size_t )(fifo_occupancy(&p -> output_fifo)));
  const double *s = (fifo_read(&p -> output_fifo,((int )odone),0));
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  for (i = 0; i < odone; ++i) 
     *(obuf++) = ((sox_sample_t )((((sox_macro_temp_sample , (sox_macro_temp_double = ( *(s++) * (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.))))) , ((sox_macro_temp_double < 0)?(((sox_macro_temp_double <= ((sox_sample_t )(1 << 32 - 1)) - .5)?((++effp -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (sox_macro_temp_double - .5))) : (((sox_macro_temp_double >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + .5)?(((sox_macro_temp_double > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)?((++effp -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : (sox_macro_temp_double + .5)))))));
  p -> samples_out += odone;
  if (( *isamp != 0UL) && (odone <  *osamp)) {
    double *t = (fifo_write(&p -> input_fifo,((int )( *isamp)),0));
    p -> samples_in +=  *isamp;
    for (i =  *isamp; i != 0UL; --i) 
       *(t++) = (( *(ibuf++)) * (1. / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)));
    filter(p);
  }
  else 
     *isamp = 0;
   *osamp = odone;
  return SOX_SUCCESS;
}

static int drain(sox_effect_t *effp,sox_sample_t *obuf,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  static size_t isamp = 0;
  size_t remaining = ((p -> samples_in) > (p -> samples_out))?((p -> samples_in) - (p -> samples_out)) : 0;
  double *buff = (1?memset(lsx_realloc(0,(1024 * sizeof(( *buff)))),0,(1024 * sizeof(( *buff)))) : ((void *)((void *)0)));
  if (remaining > 0) {
    while(((size_t )(fifo_occupancy(&p -> output_fifo))) < remaining){
      fifo_write(&p -> input_fifo,1024,buff);
      p -> samples_in += 1024;
      filter(p);
    }
    fifo_trim_to(&p -> output_fifo,((int )remaining));
    p -> samples_in = 0;
  }
  free(buff);
  return flow(effp,0,obuf,&isamp,osamp);
}

static int stop(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  fifo_delete(&p -> input_fifo);
  fifo_delete(&p -> output_fifo);
  free(( *(p -> filter_ptr)).coefs);
  memset((p -> filter_ptr),0,(sizeof(( *(p -> filter_ptr)))));
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_dft_filter_effect_fn()
{
  static sox_effect_handler_t handler = {((const char *)((void *)0)), ((const char *)((void *)0)), (128), ((sox_effect_handler_getopts )((void *)0)), (start), (flow), (drain), (stop), ((sox_effect_handler_kill )((void *)0)), (0)};
  return (&handler);
}
