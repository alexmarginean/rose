/* libSoX file format: IRCAM SoundFile   (c) 2008 robs@users.sourceforge.net
 *
 * See http://www-mmsp.ece.mcgill.ca/Documents/AudioFormats/IRCAM/IRCAM.html
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include <string.h>
/* Magic numbers used in IRCAM audio files */
static struct __unnamed_class___F0_L24_C8_unknown_scope_and_name_variable_declaration__variable_type__Ab_c_index_4_Ae__variable_name_unknown_scope_and_name__scope__str__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__reverse_bytes__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__desc {
char str[4UL];
sox_bool reverse_bytes;
const char *desc;}id[] = {{("d\243\001"), (0), ("little-endian VAX (native)")}, {("\000\001\243d"), (1), ("big-endian VAX")}, {("d\243\002"), (1), ("big-endian Sun (native)")}, {("\000\002\243d"), (0), ("little-endian Sun")}, {("d\243\003"), (0), ("little-endian MIPS (DEC)")}, {("\000\003\243d"), (1), ("big-endian MIPS (SGI)")}, {("d\243\004"), (1), ("big-endian NeXT")}, {("    "), (0), ((const char *)((void *)0))}};
#define FIXED_HDR     1024
#define SF_COMMENT    2        /* code for "comment line" */
typedef enum  {Unspecified,Linear_8,Alaw_8=0x10001,Mulaw_8=0x20001,Linear_16=0x00002,Linear_24,Linear_32=0x40004,Float=0x00004,Double=0x00008}ft_encoding_t;

static ft_encoding_t ft_enc(unsigned int size,sox_encoding_t encoding)
{
  if ((encoding == SOX_ENCODING_ULAW) && (size == 8)) 
    return Mulaw_8;
  if ((encoding == SOX_ENCODING_ALAW) && (size == 8)) 
    return Alaw_8;
  if ((encoding == SOX_ENCODING_SIGN2) && (size == 8)) 
    return Linear_8;
  if ((encoding == SOX_ENCODING_SIGN2) && (size == 16)) 
    return Linear_16;
  if ((encoding == SOX_ENCODING_SIGN2) && (size == 24)) 
    return Linear_24;
  if ((encoding == SOX_ENCODING_SIGN2) && (size == 32)) 
    return Linear_32;
  if ((encoding == SOX_ENCODING_FLOAT) && (size == 32)) 
    return Float;
  if ((encoding == SOX_ENCODING_FLOAT) && (size == 64)) 
    return Double;
  return Unspecified;
}

static sox_encoding_t sox_enc(uint32_t ft_encoding,unsigned int *size)
{
  switch(ft_encoding){
    case Mulaw_8:
{
       *size = 8;
      return SOX_ENCODING_ULAW;
    }
    case Alaw_8:
{
       *size = 8;
      return SOX_ENCODING_ALAW;
    }
    case Linear_8:
{
       *size = 8;
      return SOX_ENCODING_SIGN2;
    }
    case Linear_16:
{
       *size = 16;
      return SOX_ENCODING_SIGN2;
    }
    case Linear_24:
{
       *size = 24;
      return SOX_ENCODING_SIGN2;
    }
    case Linear_32:
{
       *size = 32;
      return SOX_ENCODING_SIGN2;
    }
    case Float:
{
       *size = 32;
      return SOX_ENCODING_FLOAT;
    }
    case Double:
{
       *size = 64;
      return SOX_ENCODING_FLOAT;
    }
    default:
{
      return SOX_ENCODING_UNKNOWN;
    }
  }
}

static int startread(sox_format_t *ft)
{
  char magic[4UL];
  float rate;
  uint32_t channels;
  uint32_t ft_encoding;
  unsigned int i;
  unsigned int bits_per_sample;
  sox_encoding_t encoding;
  uint16_t code;
  uint16_t size;
  if (lsx_readchars(ft,magic,(sizeof(magic))) != 0) 
    return SOX_EOF;
  for (i = 0; (id[i].desc != 0) && (memcmp(magic,id[i].str,(sizeof(magic))) != 0); ++i) ;
  if (!(id[i].desc != 0)) {
    lsx_fail_errno(ft,SOX_EHDR,"sf: can\'t find IRCAM identifier");
    return SOX_EOF;
  }
  ((( *sox_get_globals()).subsystem = "sf.c") , lsx_report_impl("found %s identifier",id[i].desc));
  ft -> encoding.reverse_bytes = id[i].reverse_bytes;
  if (((lsx_readf(ft,&rate) != 0) || (lsx_readdw(ft,&channels) != 0)) || (lsx_readdw(ft,&ft_encoding) != 0)) 
    return SOX_EOF;
  if (!((encoding = sox_enc(ft_encoding,&bits_per_sample)) != 0U)) {
    lsx_fail_errno(ft,SOX_EFMT,"sf: unsupported encoding %#x)",ft_encoding);
    return SOX_EOF;
  }
  do {
    if ((lsx_readw(ft,&code) != 0) || (lsx_readw(ft,&size) != 0)) 
      return SOX_EOF;
    if (code == 2) {
/* +1 ensures null-terminated */
      char *buf = (((1 * (((size_t )size) + 1)) != 0UL)?memset(lsx_realloc(0,(1 * (((size_t )size) + 1))),0,(1 * (((size_t )size) + 1))) : ((void *)((void *)0)));
      if (lsx_readchars(ft,buf,((size_t )size)) != SOX_SUCCESS) {
        free(buf);
        return SOX_EOF;
      }
      sox_append_comments(&ft -> oob.comments,buf);
      free(buf);
    }
    else if (lsx_skipbytes(ft,((size_t )size)) != 0) 
      return SOX_EOF;
  }while (code != 0);
  if (lsx_skipbytes(ft,(1024 - ((size_t )(lsx_tell(ft))))) != 0) 
    return SOX_EOF;
  return lsx_check_read_params(ft,channels,rate,encoding,bits_per_sample,((uint64_t )0),sox_true);
}

static int write_header(sox_format_t *ft)
{
  char *comment = lsx_cat_comments(ft -> oob.comments);
/* null-terminated */
  size_t len = (((((1024 - 26) <= strlen(comment))?(1024 - 26) : strlen(comment))) + 1);
/* Minimum & multiple of 4 bytes */
  size_t info_len = (4 >= ((len + 3) & (~3u)))?4 : ((len + 3) & (~3u));
  int i = (ft -> encoding.reverse_bytes == 0)?0 : 2;
  sox_bool error = ((((((((0 || (((((lsx_writebuf(ft,id[i].str,(sizeof(id[i].str)))) == sizeof(id[i].str))?SOX_SUCCESS : SOX_EOF)) != 0)) || (lsx_writef(ft,ft -> signal.rate) != 0)) || (lsx_writedw(ft,ft -> signal.channels) != 0)) || (lsx_writedw(ft,(ft_enc(ft -> encoding.bits_per_sample,ft -> encoding.encoding))) != 0)) || (lsx_writew(ft,2) != 0)) || (lsx_writew(ft,((unsigned int )info_len)) != 0)) || ((((lsx_writebuf(ft,comment,len) == len)?SOX_SUCCESS : SOX_EOF)) != 0)) || (lsx_padbytes(ft,((1024 - 20) - len)) != 0));
  free(comment);
  return (error != 0U)?SOX_EOF : SOX_SUCCESS;
}
const sox_format_handler_t *lsx_sf_format_fn();

const sox_format_handler_t *lsx_sf_format_fn()
{
  static const char *const names[] = {("sf"), ("ircam"), ((const char *)((void *)0))};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_ULAW), (8), (0), (SOX_ENCODING_ALAW), (8), (0), (SOX_ENCODING_SIGN2), (8), (16), (24), (32), (0), (SOX_ENCODING_FLOAT), (32), (64), (0), (0)};
  static const sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("Institut de Recherche et Coordination Acoustique/Musique"), (names), ((64 | 0)), (startread), (lsx_rawread), ((sox_format_handler_stopread )((void *)0)), (write_header), (lsx_rawwrite), ((sox_format_handler_stopwrite )((void *)0)), (lsx_rawseek), (write_encodings), ((const sox_rate_t *)((void *)0)), (0)};
  return &handler;
}
