/* libSoX SoundTool format handler          (c) 2008 robs@users.sourceforge.net
 * See description in sndtl26.zip on the net.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include <string.h>
static const char ID1[6UL] = "SOUND\032";
#define text_field_len (size_t)96  /* Includes null-terminator */

static int start_read(sox_format_t *ft)
{
  char id1[6UL];
  char comments[97UL];
  uint32_t nsamples;
  uint16_t rate;
  if ((((((lsx_readchars(ft,id1,(sizeof(ID1))) != 0) || (lsx_skipbytes(ft,((size_t )10)) != 0)) || (lsx_readdw(ft,&nsamples) != 0)) || (lsx_readw(ft,&rate) != 0)) || (lsx_skipbytes(ft,((size_t )6)) != 0)) || (lsx_readchars(ft,comments,((size_t )96)) != 0)) 
    return SOX_EOF;
  if (memcmp(ID1,id1,(sizeof(id1))) != 0) {
    lsx_fail_errno(ft,SOX_EHDR,"soundtool: can\'t find SoundTool identifier");
    return SOX_EOF;
  }
/* Be defensive against incorrect files */
  comments[(size_t )96] = 0;
  sox_append_comments(&ft -> oob.comments,comments);
  return lsx_check_read_params(ft,1,((sox_rate_t )rate),SOX_ENCODING_UNSIGNED,8,((uint64_t )nsamples),sox_true);
}

static int write_header(sox_format_t *ft)
{
  char *comment = lsx_cat_comments(ft -> oob.comments);
  char text_buf[96UL];
  uint64_t length = ((ft -> olength) != 0UL)?(ft -> olength) : ft -> signal.length;
  memset(text_buf,0,(sizeof(text_buf)));
  strncpy(text_buf,comment,(((size_t )96) - 1));
  free(comment);
  return ((((((((((((((lsx_writebuf(ft,ID1,(sizeof(ID1)))) == sizeof(ID1))?SOX_SUCCESS : SOX_EOF)) != 0) || (lsx_writew(ft,0) != 0)) || (lsx_writedw(ft,((unsigned int )length)) != 0)) || (lsx_writedw(ft,0) != 0)) || (lsx_writedw(ft,((unsigned int )length)) != 0)) || (lsx_writew(ft,((65535 <= ((unsigned int )(ft -> signal.rate + .5)))?65535 : ((unsigned int )(ft -> signal.rate + .5)))) != 0)) || (lsx_writew(ft,0) != 0)) || (lsx_writew(ft,10) != 0)) || (lsx_writew(ft,4) != 0)) || (((((lsx_writebuf(ft,text_buf,(sizeof(text_buf)))) == sizeof(text_buf))?SOX_SUCCESS : SOX_EOF)) != 0))?SOX_EOF : SOX_SUCCESS;
/* GSound: not used */
/* length of complete sample */
/* first byte to play from sample */
/* first byte NOT to play from sample */
/* sample size/type */
/* speaker driver volume */
/* speaker driver DC shift */
}
const sox_format_handler_t *lsx_soundtool_format_fn();

const sox_format_handler_t *lsx_soundtool_format_fn()
{
  static const char *const names[] = {("sndt"), ((const char *)((void *)0))};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_UNSIGNED), (8), (0), (0)};
  static const sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("8-bit linear audio as used by Martin Hepperle\'s `SoundTool\' of 1991/2"), (names), ((0x0040 | 0 | 256 | 8)), (start_read), (lsx_rawread), ((sox_format_handler_stopread )((void *)0)), (write_header), (lsx_rawwrite), ((sox_format_handler_stopwrite )((void *)0)), (lsx_rawseek), (write_encodings), ((const sox_rate_t *)((void *)0)), (0)};
  return &handler;
}
