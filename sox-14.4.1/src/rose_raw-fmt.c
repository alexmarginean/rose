/* libSoX file formats: raw         (c) 2007-11 SoX contributors
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"

static int raw_start(sox_format_t *ft)
{
  return lsx_rawstart(ft,sox_false,sox_false,sox_true,SOX_ENCODING_UNKNOWN,0);
}
const sox_format_handler_t *lsx_raw_format_fn();

const sox_format_handler_t *lsx_raw_format_fn()
{
  static const char *const names[] = {("raw"), ((const char *)((void *)0))};
  static const unsigned int encodings[] = {(SOX_ENCODING_SIGN2), (32), (24), (16), (8), (0), (SOX_ENCODING_UNSIGNED), (32), (24), (16), (8), (0), (SOX_ENCODING_ULAW), (8), (0), (SOX_ENCODING_ALAW), (8), (0), (SOX_ENCODING_FLOAT), (64), (32), (0), (0)};
  static const sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("Raw PCM, mu-law, or A-law"), (names), (0), (raw_start), (lsx_rawread), ((sox_format_handler_stopread )((void *)0)), (raw_start), (lsx_rawwrite), ((sox_format_handler_stopwrite )((void *)0)), (lsx_rawseek), (encodings), ((const sox_rate_t *)((void *)0)), (0)};
  return &handler;
}

static int sln_start(sox_format_t *ft)
{
  return lsx_check_read_params(ft,1,8000.,SOX_ENCODING_SIGN2,16,((uint64_t )0),sox_false);
}
const sox_format_handler_t *lsx_sln_format_fn();

const sox_format_handler_t *lsx_sln_format_fn()
{
  static const char *const names[] = {("sln"), ((const char *)((void *)0))};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_SIGN2), (16), (0), (0)};
  static const sox_rate_t write_rates[] = {(8000), (0)};
  static sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("Asterisk PBX headerless format"), (names), ((64 | 0 | 256)), (sln_start), (lsx_rawread), ((sox_format_handler_stopread )((void *)0)), ((sox_format_handler_startwrite )((void *)0)), (lsx_rawwrite), ((sox_format_handler_stopwrite )((void *)0)), (lsx_rawseek), (write_encodings), (write_rates), (0)};
  return (&handler);
}
