/* adpcm.c  codex functions for MS_ADPCM data
 *          (hopefully) provides interoperability with
 *          Microsoft's ADPCM format, but, as usual,
 *          see LACK-OF-WARRANTY information below.
 *
 *      Copyright (C) 1999 Stanley J. Brooks <stabro@megsinet.net>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
/*
 * November 22, 1999
 *  specs I've seen are unclear about ADPCM supporting more than 2 channels,
 *  but these routines support more channels in a manner which looks (IMHO)
 *  like the most natural extension.
 *
 *  Remark: code still turbulent, encoding very new.
 *
 */
#include "sox_i.h"
#include "adpcm.h"
#include <sys/types.h>
#include <stdio.h>
typedef struct __unnamed_class___F0_L40_C9_unknown_scope_and_name_variable_declaration__variable_type_L5R_variable_name_unknown_scope_and_name__scope__step__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_s_index_2_Ae__variable_name_unknown_scope_and_name__scope__coef {
/* step size */
sox_sample_t step;
short coef[2UL];}MsState_t;
#define lsbshortldi(x,p) { (x)=((short)((int)(p)[0] + ((int)(p)[1]<<8))); (p) += 2; }
/*
 * Lookup tables for MS ADPCM format
 */
/* these are step-size adjust factors, where
 * 1.0 is scaled to 0x100
 */
static const sox_sample_t stepAdjustTable[] = {(230), (230), (230), (230), (307), (409), (512), (614), (768), (614), (512), (409), (307), (230), (230), (230)};
/* TODO : The first 7 lsx_ms_adpcm_i_coef sets are always hardcoded and must
   appear in the actual WAVE file.  They should be read in
   in case a sound program added extras to the list. */
const short lsx_ms_adpcm_i_coef[7UL][2UL] = {{(256), (0)}, {(512), ((-256))}, {(0), (0)}, {(192), (64)}, {(240), (0)}, {(460), ((-208))}, {(392), ((-232))}};

inline static sox_sample_t AdpcmDecode(sox_sample_t c,MsState_t *state,sox_sample_t sample1,sox_sample_t sample2)
{
  sox_sample_t vlin;
  sox_sample_t sample;
  sox_sample_t step;
/** Compute next step value **/
  step = (state -> step);
{
    sox_sample_t nstep;
    nstep = ((stepAdjustTable[c] * step) >> 8);
    state -> step = ((nstep < 16)?16 : nstep);
  }
/** make linear prediction for next sample **/
  vlin = (((sample1 * (state -> coef)[0]) + (sample2 * (state -> coef)[1])) >> 8);
/** then add the code*step adjustment **/
  c -= ((c & 8) << 1);
  sample = ((c * step) + vlin);
  if (sample > 0x7fff) 
    sample = 0x7fff;
  else if (sample < -0x8000) 
    sample = (-0x8000);
  return sample;
}
/* lsx_ms_adpcm_block_expand_i() outputs interleaved samples into one output buffer */

const char *lsx_ms_adpcm_block_expand_i(
/* total channels             */
unsigned int chans,int nCoef,const short *coef,
/* input buffer[blockAlign]   */
const unsigned char *ibuff,
/* output samples, n*chans    */
short *obuff,
/* samples to decode PER channel */
int n)
{
  const unsigned char *ip;
  unsigned int ch;
  const char *errmsg = (const char *)((void *)0);
/* One decompressor state for each channel */
  MsState_t state[4UL];
/* Read the four-byte header for each channel */
  ip = ibuff;
  for (ch = 0; ch < chans; ch++) {
    unsigned char bpred =  *(ip++);
    if (bpred >= nCoef) {
      errmsg = "MSADPCM bpred >= nCoef, arbitrarily using 0\n";
      bpred = 0;
    }
    state[ch].coef[0] = coef[(((int )bpred) * 2) + 0];
    state[ch].coef[1] = coef[(((int )bpred) * 2) + 1];
  }
  for (ch = 0; ch < chans; ch++) {
    state[ch].step = ((short )(((int )ip[0]) + (((int )ip[1]) << 8)));
    ip += 2;
  };
/* sample1's directly into obuff */
  for (ch = 0; ch < chans; ch++) {
    obuff[chans + ch] = ((short )(((int )ip[0]) + (((int )ip[1]) << 8)));
    ip += 2;
  };
/* sample2's directly into obuff */
  for (ch = 0; ch < chans; ch++) {
    obuff[ch] = ((short )(((int )ip[0]) + (((int )ip[1]) << 8)));
    ip += 2;
  };
{
    unsigned int ch2;
    unsigned char b;
    short *op;
    short *top;
    short *tmp;
/* already have 1st 2 samples from block-header */
    op = (obuff + (2 * chans));
    top = (obuff + (n * chans));
    ch2 = 0;
/*** N.B. Without int casts, crashes on 64-bit arch ***/
    while(op < top){
      b =  *(ip++);
      tmp = op;
       *(op++) = (AdpcmDecode((b >> 4),(state + ch2),tmp[-((int )chans)],tmp[-((int )(2 * chans))]));
      if (++ch2 == chans) 
        ch2 = 0;
      tmp = op;
       *(op++) = (AdpcmDecode((b & 15),(state + ch2),tmp[-((int )chans)],tmp[-((int )(2 * chans))]));
      if (++ch2 == chans) 
        ch2 = 0;
    }
  }
  return errmsg;
}

static int AdpcmMashS(
/* channel number to encode, REQUIRE 0 <= ch < chans  */
unsigned int ch,
/* total channels */
unsigned int chans,
/* values to use as starting 2 */
short v[2UL],
/* lin predictor coeffs */
const short coef[2UL],
/* ibuff[] is interleaved input samples */
const short *ibuff,
/* samples to encode PER channel */
int n,
/* input/output step, REQUIRE 16 <= *st <= 0x7fff */
int *iostep,
/* output buffer[blockAlign], or NULL for no output  */
unsigned char *obuff)
{
  const short *ip;
  const short *itop;
  unsigned char *op;
/*  */
  int ox = 0;
  int d;
  int v0;
  int v1;
  int step;
/* long long is okay also, speed abt the same */
  double d2;
/* point ip to 1st input sample for this channel */
  ip = (ibuff + ch);
  itop = (ibuff + (n * chans));
  v0 = v[0];
  v1 = v[1];
/* 1st input sample for this channel */
  d = (( *ip) - v1);
  ip += chans;
/* d2 will be sum of squares of errors, given input v0 and *st */
  d2 = (d * d);
/* 2nd input sample for this channel */
  d = (( *ip) - v0);
  ip += chans;
  d2 += (d * d);
  step =  *iostep;
/* output pointer (or NULL) */
  op = obuff;
/* NULL means don't output, just compute the rms error */
  if (op != 0) {
/* skip bpred indices */
    op += chans;
/* channel's stepsize */
    op += (2 * ch);
    op[0] = step;
    op[1] = (step >> 8);
/* skip to v0 */
    op += (2 * chans);
    op[0] = v0;
    op[1] = (v0 >> 8);
/* skip to v1 */
    op += (2 * chans);
    op[0] = v1;
    op[1] = (v1 >> 8);
/* point to base of output nibbles */
    op = (obuff + (7 * chans));
    ox = (4 * ch);
  }
  for (; ip < itop; ip += chans) {
    int vlin;
    int d3;
    int dp;
    int c;
/* make linear prediction for next sample */
    vlin = (((v0 * coef[0]) + (v1 * coef[1])) >> 8);
/* difference between linear prediction and current sample */
    d3 = (( *ip) - vlin);
    dp = ((d3 + (step << 3)) + (step >> 1));
    c = 0;
    if (dp > 0) {
      c = (dp / step);
      if (c > 15) 
        c = 15;
    }
    c -= 8;
/* quantized estimate of samp - vlin */
    dp = (c * step);
/* mask to 4 bits */
    c &= 15;
/* shift history */
    v1 = v0;
    v0 = (vlin + dp);
    if (v0 < -0x8000) 
      v0 = -0x8000;
    else if (v0 > 0x7fff) 
      v0 = 0x7fff;
    d3 = (( *ip) - v0);
/* update square-error */
    d2 += (d3 * d3);
/* if we want output, put it in proper place */
    if (op != 0) {
      op[ox >> 3] |= (((ox & 4) != 0)?c : (c << 4));
      ox += (4 * chans);
      ((( *sox_get_globals()).subsystem = "adpcm.c") , lsx_debug_more_impl("%.1x",c));
    }
/* Update the step for the next sample */
    step = ((stepAdjustTable[c] * step) >> 8);
    if (step < 16) 
      step = 16;
  }
  if (op != 0) 
    ((( *sox_get_globals()).subsystem = "adpcm.c") , lsx_debug_more_impl("\n"));
/* be sure it's non-negative */
  d2 /= n;
  ((( *sox_get_globals()).subsystem = "adpcm.c") , lsx_debug_more_impl("ch%d: st %d->%d, d %.1f\n",ch, *iostep,step,sqrt(d2)));
   *iostep = step;
  return (int )(sqrt(d2));
}

inline static void AdpcmMashChannel(
/* channel number to encode, REQUIRE 0 <= ch < chans  */
unsigned int ch,
/* total channels */
unsigned int chans,
/* ip[] is interleaved input samples */
const short *ip,
/* samples to encode PER channel, REQUIRE */
int n,
/* input/output steps, 16<=st[i] */
int *st,
/* output buffer[blockAlign] */
unsigned char *obuff)
{
  short v[2UL];
  int n0;
  int s0;
  int s1;
  int ss;
  int smin;
  int dmin;
  int k;
  int kmin;
  n0 = (n / 2);
  if (n0 > 32) 
    n0 = 32;
  if ( *st < 16) 
     *st = 16;
  v[1] = ip[ch];
  v[0] = ip[ch + chans];
  dmin = 0;
  kmin = 0;
  smin = 0;
/* for each of 7 standard coeff sets, we try compression
         * beginning with last step-value, and with slightly
         * forward-adjusted step-value, taking best of the 14
         */
  for (k = 0; k < 7; k++) {
    int d0;
    int d1;
    ss = (s0 =  *st);
/* with step s0 */
    d0 = AdpcmMashS(ch,chans,v,lsx_ms_adpcm_i_coef[k],ip,n,&ss,0);
    s1 = s0;
    AdpcmMashS(ch,chans,v,lsx_ms_adpcm_i_coef[k],ip,n0,&s1,0);
    ((( *sox_get_globals()).subsystem = "adpcm.c") , lsx_debug_more_impl(" s32 %d\n",s1));
    ss = (s1 = (((3 * s0) + s1) / 4));
/* with step s1 */
    d1 = AdpcmMashS(ch,chans,v,lsx_ms_adpcm_i_coef[k],ip,n,&ss,0);
    if ((!(k != 0) || (d0 < dmin)) || (d1 < dmin)) {
      kmin = k;
      if (d0 <= d1) {
        dmin = d0;
        smin = s0;
      }
      else {
        dmin = d1;
        smin = s1;
      }
    }
  }
   *st = smin;
  ((( *sox_get_globals()).subsystem = "adpcm.c") , lsx_debug_more_impl("kmin %d, smin %5d, ",kmin,smin));
  AdpcmMashS(ch,chans,v,lsx_ms_adpcm_i_coef[kmin],ip,n,st,obuff);
  obuff[ch] = kmin;
}

void lsx_ms_adpcm_block_mash_i(
/* total channels */
unsigned int chans,
/* ip[n*chans] is interleaved input samples */
const short *ip,
/* samples to encode PER channel */
int n,
/* input/output steps, 16<=st[i] */
int *st,
/* output buffer[blockAlign]     */
unsigned char *obuff,
/* >= 7*chans + chans*(n-2)/2.0    */
int blockAlign)
{
  unsigned int ch;
  unsigned char *p;
  ((( *sox_get_globals()).subsystem = "adpcm.c") , lsx_debug_more_impl("AdpcmMashI(chans %d, ip %p, n %d, st %p, obuff %p, bA %d)\n",chans,((void *)ip),n,((void *)st),obuff,blockAlign));
  for (p = (obuff + (7 * chans)); p < (obuff + blockAlign); p++) 
     *p = 0;
  for (ch = 0; ch < chans; ch++) 
    AdpcmMashChannel(ch,chans,ip,n,(st + ch),obuff);
}
/*
 * lsx_ms_adpcm_samples_in(dataLen, chans, blockAlign, samplesPerBlock)
 *  returns the number of samples/channel which would be
 *  in the dataLen, given the other parameters ...
 *  if input samplesPerBlock is 0, then returns the max
 *  samplesPerBlock which would go into a block of size blockAlign
 *  Yes, it is confusing usage.
 */

size_t lsx_ms_adpcm_samples_in(size_t dataLen,size_t chans,size_t blockAlign,size_t samplesPerBlock)
{
  size_t m;
  size_t n;
  if (samplesPerBlock != 0UL) {
    n = ((dataLen / blockAlign) * samplesPerBlock);
    m = (dataLen % blockAlign);
  }
  else {
    n = 0;
    m = blockAlign;
  }
  if (m >= (7 * chans)) {
/* bytes beyond block-header */
    m -= (7 * chans);
/* nibbles/chans + 2 in header */
    m = (((2 * m) / chans) + 2);
    if ((samplesPerBlock != 0UL) && (m > samplesPerBlock)) 
      m = samplesPerBlock;
    n += m;
  }
  return n;
}

size_t lsx_ms_adpcm_bytes_per_block(size_t chans,size_t samplesPerBlock)
{
  size_t n;
/* header */
  n = (7 * chans);
  if (samplesPerBlock > 2) 
    n += ((((((size_t )samplesPerBlock) - 2) * chans) + 1) / 2);
  return n;
}
