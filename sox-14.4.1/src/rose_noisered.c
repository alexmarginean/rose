/* noisered - Noise Reduction Effect.
 *
 * Written by Ian Turner (vectro@vectro.org)
 *
 * Copyright 1999 Ian Turner
 * This source code is freely redistributable and may be used for
 * any purpose.  This copyright notice must be maintained.
 * Authors are not responsible for the consequences of using this software.
 */
#include "noisered.h"
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <assert.h>
typedef struct __unnamed_class___F0_L18_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__f__Pe___variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__f__Pe___variable_name_unknown_scope_and_name__scope__lastwindow__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__f__Pe___variable_name_unknown_scope_and_name__scope__noisegate__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__f__Pe___variable_name_unknown_scope_and_name__scope__smoothing {
float *window;
float *lastwindow;
float *noisegate;
float *smoothing;}chandata_t;
/* Holds profile information */
typedef struct __unnamed_class___F0_L26_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__profile_filename__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_f_variable_name_unknown_scope_and_name__scope__threshold__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__chandata_tL126R__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__chandata__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__bufdata {
char *profile_filename;
float threshold;
chandata_t *chandata;
size_t bufdata;}priv_t;

static void FFT(unsigned int NumSamples,int InverseTransform,const float *RealIn,float *ImagIn,float *RealOut,float *ImagOut)
{
  unsigned int i;
  double *work = (malloc(((2 * NumSamples) * sizeof(( *work)))));
  for (i = 0; i < (2 * NumSamples); i += 2) {
    work[i] = RealIn[i >> 1];
    work[i + 1] = (((ImagIn != 0)?ImagIn[i >> 1] : 0));
  }
  lsx_safe_cdft((2 * ((int )NumSamples)),((InverseTransform != 0)?-1 : 1),work);
  if (InverseTransform != 0) 
    for (i = 0; i < (2 * NumSamples); i += 2) {
      RealOut[i >> 1] = (work[i] / NumSamples);
      ImagOut[i >> 1] = (work[i + 1] / NumSamples);
    }
  else 
    for (i = 0; i < (2 * NumSamples); i += 2) {
      RealOut[i >> 1] = work[i];
      ImagOut[i >> 1] = work[i + 1];
    }
  free(work);
}
/*
 * Get the options. Default file is stdin (if the audio
 * input file isn't coming from there, of course!)
 */

static int sox_noisered_getopts(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  (--argc , ++argv);
  if (argc > 0) {
    p -> profile_filename = argv[0];
    ++argv;
    --argc;
  }
  p -> threshold = 0.5;
{
/* break-able block */
    do {{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 0) || (d > 1)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "noisered.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","threshold",((double )0),((double )1)));
            return lsx_usage(effp);
          }
          p -> threshold = d;
          (--argc , ++argv);
        }
      };
    }while (0);
  }
  return (argc != 0)?lsx_usage(effp) : SOX_SUCCESS;
}
/*
 * Prepare processing.
 * Do all initializations.
 */

static int sox_noisered_start(sox_effect_t *effp)
{
  priv_t *data = (priv_t *)(effp -> priv);
  size_t fchannels = 0;
  size_t channels = effp -> in_signal.channels;
  size_t i;
  FILE *ifp = lsx_open_input_file(effp,(data -> profile_filename));
  if (!(ifp != 0)) 
    return SOX_EOF;
  data -> chandata = ((((channels * sizeof(( *(data -> chandata)))) != 0ULL)?memset(lsx_realloc(0,(channels * sizeof(( *(data -> chandata))))),0,(channels * sizeof(( *(data -> chandata))))) : ((void *)((void *)0))));
  data -> bufdata = 0;
  for (i = 0; i < channels; i++) {
    (data -> chandata)[i].noisegate = ((1?memset(lsx_realloc(0,((2048 / 2 + 1) * sizeof(float ))),0,((2048 / 2 + 1) * sizeof(float ))) : ((void *)((void *)0))));
    (data -> chandata)[i].smoothing = ((1?memset(lsx_realloc(0,((2048 / 2 + 1) * sizeof(float ))),0,((2048 / 2 + 1) * sizeof(float ))) : ((void *)((void *)0))));
    (data -> chandata)[i].lastwindow = ((float *)((void *)0));
  }
{
    while(1){
      unsigned long i1_ul;
      size_t i1;
      float f1;
      if (2 != fscanf(ifp," Channel %lu: %f",&i1_ul,&f1)) 
        break; 
      i1 = i1_ul;
      if (i1 != fchannels) {
        ((( *sox_get_globals()).subsystem = "noisered.c") , lsx_fail_impl("noisered: Got channel %lu, expected channel %lu.",((unsigned long )i1),((unsigned long )fchannels)));
        return SOX_EOF;
      }
      (data -> chandata)[fchannels].noisegate[0] = f1;
      for (i = 1; i < (2048 / 2 + 1); i++) {
        if (1 != fscanf(ifp,", %f",&f1)) {
          ((( *sox_get_globals()).subsystem = "noisered.c") , lsx_fail_impl("noisered: Not enough data for channel %lu (expected %d, got %lu)",((unsigned long )fchannels),2048 / 2 + 1,((unsigned long )i)));
          return SOX_EOF;
        }
        (data -> chandata)[fchannels].noisegate[i] = f1;
      }
      fchannels++;
    }
  }
  if (fchannels != channels) {
    ((( *sox_get_globals()).subsystem = "noisered.c") , lsx_fail_impl("noisered: channel mismatch: %lu in input, %lu in profile.",((unsigned long )channels),((unsigned long )fchannels)));
    return SOX_EOF;
  }
  if (ifp != stdin) 
    fclose(ifp);
/* TODO: calculate actual length */
  effp -> out_signal.length = ((sox_uint64_t )(-1));
  return SOX_SUCCESS;
}
/* Mangle a single window. Each output sample (except the first and last
 * half-window) is the result of two distinct calls to this function,
 * due to overlapping windows. */

static void reduce_noise(chandata_t *chan,float *window,double level)
{
  float *inr;
  float *ini;
  float *outr;
  float *outi;
  float *power;
  float *smoothing = (chan -> smoothing);
  int i;
  inr = ((1?memset(lsx_realloc(0,((2048 * 5) * sizeof(float ))),0,((2048 * 5) * sizeof(float ))) : ((void *)((void *)0))));
  ini = (inr + 2048);
  outr = (ini + 2048);
  outi = (outr + 2048);
  power = (outi + 2048);
  for (i = 0; i < 2048 / 2 + 1; i++) 
    ((smoothing[i] >= 0) && (smoothing[i] <= 1))?((void )0) : __assert_fail("smoothing[i] >= 0 && smoothing[i] <= 1","noisered.c",154,"void reduce_noise(struct <unnamed> *, float *, double)");
  memcpy(inr,window,(2048 * sizeof(float )));
  FFT(2048,0,inr,0,outr,outi);
  memcpy(inr,window,(2048 * sizeof(float )));
  lsx_apply_hann_f(inr,2048);
  lsx_power_spectrum_f(2048,inr,power);
  for (i = 0; i < 2048 / 2 + 1; i++) {
    float smooth;
    float plog;
    plog = (log(power[i]));
    if ((power[i] != 0) && (plog < ((chan -> noisegate)[i] + (level * 8.0)))) 
      smooth = 0.0;
    else 
      smooth = 1.0;
    smoothing[i] = ((smooth * 0.5) + (smoothing[i] * 0.5));
  }
/* Audacity says this code will eliminate tinkle bells.
     * I have no idea what that means. */
  for (i = 2; i < 2048 / 2 + 1 - 2; i++) {
    if ((((((smoothing[i] >= 0.5) && (smoothing[i] <= 0.55)) && (smoothing[i - 1] < 0.1)) && (smoothing[i - 2] < 0.1)) && (smoothing[i + 1] < 0.1)) && (smoothing[i + 2] < 0.1)) 
      smoothing[i] = 0.0;
  }
  outr[0] *= smoothing[0];
  outi[0] *= smoothing[0];
  outr[2048 / 2 + 1 - 1] *= smoothing[2048 / 2 + 1 - 1];
  outi[2048 / 2 + 1 - 1] *= smoothing[2048 / 2 + 1 - 1];
  for (i = 1; i < 2048 / 2 + 1 - 1; i++) {
    int j = (2048 - i);
    float smooth = smoothing[i];
    outr[i] *= smooth;
    outi[i] *= smooth;
    outr[j] *= smooth;
    outi[j] *= smooth;
  }
  FFT(2048,1,outr,outi,inr,ini);
  lsx_apply_hann_f(inr,2048);
  memcpy(window,inr,(2048 * sizeof(float )));
  for (i = 0; i < 2048 / 2 + 1; i++) 
    ((smoothing[i] >= 0) && (smoothing[i] <= 1))?((void )0) : __assert_fail("smoothing[i] >= 0 && smoothing[i] <= 1","noisered.c",209,"void reduce_noise(struct <unnamed> *, float *, double)");
  free(inr);
}
/* Do window management once we have a complete window, including mangling
 * the current window. */

static int process_window(sox_effect_t *effp,priv_t *data,unsigned int chan_num,unsigned int num_chans,sox_sample_t *obuf,unsigned int len)
{
  int j;
  float *nextwindow;
  int use = ((((len <= 2048)?len : 2048)) - (((len <= (2048 / 2))?len : (2048 / 2))));
  chandata_t *chan = ((data -> chandata) + chan_num);
  int first = ((chan -> lastwindow) == ((float *)((void *)0)));
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  if ((nextwindow = ((1?memset(lsx_realloc(0,(2048 * sizeof(float ))),0,(2048 * sizeof(float ))) : ((void *)((void *)0))))) == ((float *)((void *)0))) 
    return SOX_EOF;
  memcpy(nextwindow,((chan -> window) + 2048 / 2),(sizeof(float ) * (2048 / 2)));
  reduce_noise(chan,(chan -> window),(data -> threshold));
  if (!(first != 0)) {
    for (j = 0; j < use; j++) {
      float s = ((chan -> window)[j] + (chan -> lastwindow)[2048 / 2 + j]);
      obuf[chan_num + (num_chans * j)] = ((sox_sample_t )((((sox_macro_temp_sample , (sox_macro_temp_double = (s * (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.0))))) , ((sox_macro_temp_double < ((sox_sample_t )(1 << 32 - 1)))?((++effp -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (((sox_macro_temp_double >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.0)?(((sox_macro_temp_double > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.0)?((++effp -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : sox_macro_temp_double))))));
    }
    free((chan -> lastwindow));
  }
  else {
    for (j = 0; j < use; j++) {
      (((chan -> window)[j] >= (-1)) && ((chan -> window)[j] <= 1))?((void )0) : __assert_fail("chan->window[j] >= -1 && chan->window[j] <= 1","noisered.c",241,"int process_window(struct sox_effect_t *, struct <unnamed> *, unsigned int, unsigned int, int *, unsigned int)");
      obuf[chan_num + (num_chans * j)] = ((sox_sample_t )((((sox_macro_temp_sample , (sox_macro_temp_double = ((chan -> window)[j] * (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.0))))) , ((sox_macro_temp_double < ((sox_sample_t )(1 << 32 - 1)))?((++effp -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (((sox_macro_temp_double >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.0)?(((sox_macro_temp_double > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.0)?((++effp -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : sox_macro_temp_double))))));
    }
  }
  chan -> lastwindow = (chan -> window);
  chan -> window = nextwindow;
  return use;
}
/*
 * Read in windows, and call process_window once we get a whole one.
 */

static int sox_noisered_flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *data = (priv_t *)(effp -> priv);
  size_t samp = ( *isamp <=  *osamp)? *isamp :  *osamp;
  size_t tracks = effp -> in_signal.channels;
  size_t track_samples = (samp / tracks);
  size_t ncopy = (track_samples <= (2048 - (data -> bufdata)))?track_samples : (2048 - (data -> bufdata));
  size_t whole_window = ((ncopy + (data -> bufdata)) == 2048);
  int oldbuf = (data -> bufdata);
  size_t i;
/* FIXME: Make this automatic for all effects */
  (effp -> in_signal.channels == effp -> out_signal.channels)?((void )0) : __assert_fail("effp->in_signal.channels == effp->out_signal.channels","noisered.c",268,"int sox_noisered_flow(struct sox_effect_t *, const int *, int *, unsigned long *, unsigned long *)");
  if (whole_window != 0UL) 
    data -> bufdata = (2048 / 2);
  else 
    data -> bufdata += ncopy;
/* Reduce noise on every channel. */
  for (i = 0; i < tracks; i++) {{
      sox_sample_t sox_macro_temp_sample;
      double sox_macro_temp_double;
      chandata_t *chan = ((data -> chandata) + i);
      size_t j;
      if ((chan -> window) == ((float *)((void *)0))) 
        chan -> window = ((1?memset(lsx_realloc(0,(2048 * sizeof(float ))),0,(2048 * sizeof(float ))) : ((void *)((void *)0))));
      for (j = 0; j < ncopy; j++) 
        (chan -> window)[oldbuf + j] = ((((sox_macro_temp_double , (sox_macro_temp_sample = ibuf[i + (tracks * j)]))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - 128)?((++effp -> clips , 1)) : (((sox_macro_temp_sample + 128) & ~255) * (1.0 / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.0))))));
      if (!(whole_window != 0UL)) 
        continue; 
      else 
        process_window(effp,data,((unsigned int )i),((unsigned int )tracks),obuf,((unsigned int )(oldbuf + ncopy)));
    }
  }
   *isamp = (tracks * ncopy);
  if (whole_window != 0UL) 
     *osamp = (tracks * (2048 / 2));
  else 
     *osamp = 0;
  return SOX_SUCCESS;
}
/*
 * We have up to half a window left to dump.
 */

static int sox_noisered_drain(sox_effect_t *effp,sox_sample_t *obuf,size_t *osamp)
{
  priv_t *data = (priv_t *)(effp -> priv);
  unsigned int i;
  unsigned int tracks = effp -> in_signal.channels;
  for (i = 0; i < tracks; i++) 
     *osamp = (process_window(effp,data,i,tracks,obuf,((unsigned int )(data -> bufdata))));
/* FIXME: This is very picky.  osamp needs to be big enough to get all
     * remaining data or it will be discarded.
     */
  return SOX_EOF;
}
/*
 * Clean up.
 */

static int sox_noisered_stop(sox_effect_t *effp)
{
  priv_t *data = (priv_t *)(effp -> priv);
  size_t i;
  for (i = 0; i < effp -> in_signal.channels; i++) {
    chandata_t *chan = ((data -> chandata) + i);
    free((chan -> lastwindow));
    free((chan -> window));
    free((chan -> smoothing));
    free((chan -> noisegate));
  }
  free((data -> chandata));
  return SOX_SUCCESS;
}
static sox_effect_handler_t sox_noisered_effect = {("noisered"), ("[profile-file [amount]]"), ((16 | 8)), (sox_noisered_getopts), (sox_noisered_start), (sox_noisered_flow), (sox_noisered_drain), (sox_noisered_stop), ((sox_effect_handler_kill )((void *)0)), ((sizeof(priv_t )))};

const sox_effect_handler_t *lsx_noisered_effect_fn()
{
  return (&sox_noisered_effect);
}
