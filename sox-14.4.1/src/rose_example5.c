/* Simple example of using SoX libraries
 *
 * Copyright (c) 2009 robs@users.sourceforge.net
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#ifdef NDEBUG /* N.B. assert used with active statements so enable always. */
#undef NDEBUG /* Must undef above assert.h or other that might include it. */
#endif
#include "sox.h"
#include "util.h"
#include <stdio.h>
#include <assert.h>
/* Example of reading and writing audio files stored in memory buffers
 * rather than actual files.
 *
 * Usage: example5 input output
 */
/* Uncomment following line for fixed instead of malloc'd buffer: */
/*#define FIXED_BUFFER */
#if defined FIXED_BUFFER
#define buffer_size 123456
#endif

int main(int argc,char *argv[])
{
/* input and output files */
  static sox_format_t *in;
  static sox_format_t *out;
  #define MAX_SAMPLES (size_t)2048
/* Temporary store whilst copying. */
  sox_sample_t samples[2048UL];
#if !defined FIXED_BUFFER
  char *buffer;
  size_t buffer_size;
#endif
  size_t number_read;
  (argc == 3)?((void )0) : __assert_fail("argc == 3","example5.c",54,"int main(int, char **)");
/* All libSoX applications must start by initialising the SoX library */
  (sox_init() == SOX_SUCCESS)?((void )0) : __assert_fail("sox_init() == SOX_SUCCESS","example5.c",57,"int main(int, char **)");
/* Open the input file (with default parameters) */
  ((in = sox_open_read(argv[1],0,0,0)) != 0)?((void )0) : __assert_fail("in = sox_open_read(argv[1], ((void *)0), ((void *)0), ((void *)0))","example5.c",60,"int main(int, char **)");
#if defined FIXED_BUFFER
#else
  ((out = sox_open_memstream_write(&buffer,&buffer_size,(&in -> signal),0,"sox",0)) != 0)?((void )0) : __assert_fail("out = sox_open_memstream_write(&buffer, &buffer_size, &in->signal, ((void *)0), \"sox\", ((void *)0))","example5.c",64,"int main(int, char **)");
#endif
  while((number_read = sox_read(in,samples,((size_t )2048))) != 0UL)
    (sox_write(out,samples,number_read) == number_read)?((void )0) : __assert_fail("sox_write(out, samples, number_read) == number_read","example5.c",67,"int main(int, char **)");
  sox_close(out);
  sox_close(in);
  ((in = sox_open_mem_read(buffer,buffer_size,0,0,0)) != 0)?((void )0) : __assert_fail("in = sox_open_mem_read(buffer, buffer_size, ((void *)0), ((void *)0), ((void *)0))","example5.c",71,"int main(int, char **)");
  ((out = sox_open_write(argv[2],(&in -> signal),0,0,0,0)) != 0)?((void )0) : __assert_fail("out = sox_open_write(argv[2], &in->signal, ((void *)0), ((void *)0), ((void *)0), ((void *)0))","example5.c",72,"int main(int, char **)");
  while((number_read = sox_read(in,samples,((size_t )2048))) != 0UL)
    (sox_write(out,samples,number_read) == number_read)?((void )0) : __assert_fail("sox_write(out, samples, number_read) == number_read","example5.c",74,"int main(int, char **)");
  sox_close(out);
  sox_close(in);
#if !defined FIXED_BUFFER
  free(buffer);
#endif
  sox_quit();
  return 0;
}
