/* libSoX file format: cdda   (c) 2006-8 SoX contributors
 * Based on an original idea by David Elliott
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"

static int start(sox_format_t *ft)
{
  return lsx_check_read_params(ft,2,44100.,SOX_ENCODING_SIGN2,16,((uint64_t )0),sox_true);
}

static int stopwrite(sox_format_t *ft)
{
  const unsigned int sector_num_samples = (588 * ft -> signal.channels);
  unsigned int i = ((ft -> olength) % sector_num_samples);
/* Pad with silence to multiple */
  if (i != 0U) 
    while(i++ < sector_num_samples)
/* of 1/75th of a second. */
      lsx_writew(ft,0);
  return SOX_SUCCESS;
}
const sox_format_handler_t *lsx_cdr_format_fn();

const sox_format_handler_t *lsx_cdr_format_fn()
{
  static const char *const names[] = {("cdda"), ("cdr"), ((const char *)((void *)0))};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_SIGN2), (16), (0), (0)};
  static const sox_rate_t write_rates[] = {(44100), (0)};
  static sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("Red Book Compact Disc Digital Audio"), (names), ((0x0040 | 0x0080 | 0x0200)), (start), (lsx_rawread), ((sox_format_handler_stopread )((void *)0)), ((sox_format_handler_startwrite )((void *)0)), (lsx_rawwrite), (stopwrite), (lsx_rawseek), (write_encodings), (write_rates), (0)};
  return (&handler);
}
