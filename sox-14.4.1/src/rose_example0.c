/* Simple example of using SoX libraries
 *
 * Copyright (c) 2007-8 robs@users.sourceforge.net
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#ifdef NDEBUG /* N.B. assert used with active statements so enable always. */
#undef NDEBUG /* Must undef above assert.h or other that might include it. */
#endif
#include "sox.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
/*
 * Reads input file, applies vol & flanger effects, stores in output file.
 * E.g. example1 monkey.au monkey.aiff
 */

int main(int argc,char *argv[])
{
/* input and output files */
  static sox_format_t *in;
  static sox_format_t *out;
  sox_effects_chain_t *chain;
  sox_effect_t *e;
  char *args[10UL];
  (argc == 3)?((void )0) : __assert_fail("argc == 3","example0.c",40,"int main(int, char **)");
/* All libSoX applications must start by initialising the SoX library */
  (sox_init() == SOX_SUCCESS)?((void )0) : __assert_fail("sox_init() == SOX_SUCCESS","example0.c",43,"int main(int, char **)");
/* Open the input file (with default parameters) */
  ((in = sox_open_read(argv[1],0,0,0)) != 0)?((void )0) : __assert_fail("in = sox_open_read(argv[1], ((void *)0), ((void *)0), ((void *)0))","example0.c",46,"int main(int, char **)");
/* Open the output file; we must specify the output signal characteristics.
   * Since we are using only simple effects, they are the same as the input
   * file characteristics */
  ((out = sox_open_write(argv[2],(&in -> signal),0,0,0,0)) != 0)?((void )0) : __assert_fail("out = sox_open_write(argv[2], &in->signal, ((void *)0), ((void *)0), ((void *)0), ((void *)0))","example0.c",51,"int main(int, char **)");
/* Create an effects chain; some effects need to know about the input
   * or output file encoding so we provide that information here */
  chain = sox_create_effects_chain((&in -> encoding),(&out -> encoding));
/* The first effect in the effect chain must be something that can source
   * samples; in this case, we use the built-in handler that inputs
   * data from an audio file */
  e = sox_create_effect(sox_find_effect("input"));
  ((args[0] = ((char *)in)) , ((sox_effect_options(e,1,args) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_effect_options(e, 1, args) == SOX_SUCCESS","example0.c",61,"int main(int, char **)")));
/* This becomes the first `effect' in the chain */
  (sox_add_effect(chain,e,&in -> signal,(&in -> signal)) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_add_effect(chain, e, &in->signal, &in->signal) == SOX_SUCCESS","example0.c",63,"int main(int, char **)");
  free(e);
/* Create the `vol' effect, and initialise it with the desired parameters: */
  e = sox_create_effect(sox_find_effect("vol"));
  ((args[0] = "3dB") , ((sox_effect_options(e,1,args) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_effect_options(e, 1, args) == SOX_SUCCESS","example0.c",68,"int main(int, char **)")));
/* Add the effect to the end of the effects processing chain: */
  (sox_add_effect(chain,e,&in -> signal,(&in -> signal)) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_add_effect(chain, e, &in->signal, &in->signal) == SOX_SUCCESS","example0.c",70,"int main(int, char **)");
  free(e);
/* Create the `flanger' effect, and initialise it with default parameters: */
  e = sox_create_effect(sox_find_effect("flanger"));
  (sox_effect_options(e,0,0) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_effect_options(e, 0, ((void *)0)) == SOX_SUCCESS","example0.c",75,"int main(int, char **)");
/* Add the effect to the end of the effects processing chain: */
  (sox_add_effect(chain,e,&in -> signal,(&in -> signal)) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_add_effect(chain, e, &in->signal, &in->signal) == SOX_SUCCESS","example0.c",77,"int main(int, char **)");
  free(e);
/* The last effect in the effect chain must be something that only consumes
   * samples; in this case, we use the built-in handler that outputs
   * data to an audio file */
  e = sox_create_effect(sox_find_effect("output"));
  ((args[0] = ((char *)out)) , ((sox_effect_options(e,1,args) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_effect_options(e, 1, args) == SOX_SUCCESS","example0.c",84,"int main(int, char **)")));
  (sox_add_effect(chain,e,&in -> signal,(&in -> signal)) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_add_effect(chain, e, &in->signal, &in->signal) == SOX_SUCCESS","example0.c",85,"int main(int, char **)");
  free(e);
/* Flow samples through the effects processing chain until EOF is reached */
  sox_flow_effects(chain,0,0);
/* All done; tidy up: */
  sox_delete_effects_chain(chain);
  sox_close(out);
  sox_close(in);
  sox_quit();
  return 0;
}
