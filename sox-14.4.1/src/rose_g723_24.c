/* This source code is a product of Sun Microsystems, Inc. and is provided
 * for unrestricted use.  Users may copy or modify this source code without
 * charge.
 *
 * SUN SOURCE CODE IS PROVIDED AS IS WITH NO WARRANTIES OF ANY KIND INCLUDING
 * THE WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, OR ARISING FROM A COURSE OF DEALING, USAGE OR TRADE PRACTICE.
 *
 * Sun source code is provided with no support and without any obligation on
 * the part of Sun Microsystems, Inc. to assist in its use, correction,
 * modification or enhancement.
 *
 * SUN MICROSYSTEMS, INC. SHALL HAVE NO LIABILITY WITH RESPECT TO THE
 * INFRINGEMENT OF COPYRIGHTS, TRADE SECRETS OR ANY PATENTS BY THIS SOFTWARE
 * OR ANY PART THEREOF.
 *
 * In no event will Sun Microsystems, Inc. be liable for any lost revenue
 * or profits or other special, indirect and consequential damages, even if
 * Sun has been advised of the possibility of such damages.
 *
 * Sun Microsystems, Inc.
 * 2550 Garcia Avenue
 * Mountain View, California  94043
 */
/*
 * g723_24.c
 *
 * Description:
 *
 * g723_24_encoder(), g723_24_decoder()
 *
 * These routines comprise an implementation of the CCITT G.723 24 Kbps
 * ADPCM coding algorithm.  Essentially, this implementation is identical to
 * the bit level description except for a few deviations which take advantage
 * of workstation attributes, such as hardware 2's complement arithmetic.
 *
 */
#include "sox_i.h"
#include "g711.h"
#include "g72x.h"
/*
 * Maps G.723_24 code word to reconstructed scale factor normalized log
 * magnitude values.
 */
static const short _dqlntab[8UL] = {((-2048)), (135), (273), (373), (373), (273), (135), ((-2048))};
/* Maps G.723_24 code word to log of scale factor multiplier. */
static const short _witab[8UL] = {((-128)), (960), (4384), (18624), (18624), (4384), (960), ((-128))};
/*
 * Maps G.723_24 code words to a set of values whose long and short
 * term averages are computed and then compared to give an indication
 * how stationary (steady state) the signal is.
 */
static const short _fitab[8UL] = {(0), (0x200), (1024), (0xE00), (0xE00), (1024), (0x200), (0)};
static const short qtab_723_24[3UL] = {(8), (218), (331)};
/*
 * g723_24_encoder()
 *
 * Encodes a linear PCM, A-law or u-law input sample and returns its 3-bit code.
 * Returns -1 if invalid input coding value.
 */

int lsx_g723_24_encoder(int sl,int in_coding,struct g72x_state *state_ptr)
{
/* ACCUM */
  short sei;
  short sezi;
  short se;
  short sez;
/* SUBTA */
  short d;
/* MIX */
  short y;
/* ADDB */
  short sr;
/* ADDC */
  short dqsez;
  short dq;
  short i;
/* linearize input sample to 14-bit PCM */
  switch(in_coding){
    case 2:
{
      sl = (lsx_alaw2linear16[sl] >> 2);
      break; 
    }
    case 1:
{
      sl = (lsx_ulaw2linear16[sl] >> 2);
      break; 
    }
    case 3:
{
/* sl of 14-bit dynamic range */
      sl >>= 2;
      break; 
    }
    default:
{
      return -1;
    }
  }
  sezi = (lsx_g72x_predictor_zero(state_ptr));
  sez = (sezi >> 1);
  sei = (sezi + lsx_g72x_predictor_pole(state_ptr));
/* se = estimated signal */
  se = (sei >> 1);
/* d = estimation diff. */
  d = (sl - se);
/* quantize prediction difference d */
/* quantizer step size */
  y = (lsx_g72x_step_size(state_ptr));
/* i = ADPCM code */
  i = (lsx_g72x_quantize(d,y,qtab_723_24,3));
/* quantized diff. */
  dq = (lsx_g72x_reconstruct((i & 4),_dqlntab[i],y));
/* reconstructed signal */
  sr = (((dq < 0)?(se - (dq & 0x3FFF)) : (se + dq)));
/* pole prediction diff. */
  dqsez = ((sr + sez) - se);
  lsx_g72x_update(3,y,_witab[i],_fitab[i],dq,sr,dqsez,state_ptr);
  return i;
}
/*
 * g723_24_decoder()
 *
 * Decodes a 3-bit CCITT G.723_24 ADPCM code and returns
 * the resulting 16-bit linear PCM, A-law or u-law sample value.
 * -1 is returned if the output coding is unknown.
 */

int lsx_g723_24_decoder(int i,int out_coding,struct g72x_state *state_ptr)
{
/* ACCUM */
  short sezi;
  short sei;
  short sez;
  short se;
/* MIX */
  short y;
/* ADDB */
  short sr;
  short dq;
  short dqsez;
/* mask to get proper bits */
  i &= 7;
  sezi = (lsx_g72x_predictor_zero(state_ptr));
  sez = (sezi >> 1);
  sei = (sezi + lsx_g72x_predictor_pole(state_ptr));
/* se = estimated signal */
  se = (sei >> 1);
/* adaptive quantizer step size */
  y = (lsx_g72x_step_size(state_ptr));
/* unquantize pred diff */
  dq = (lsx_g72x_reconstruct((i & 4),_dqlntab[i],y));
/* reconst. signal */
  sr = (((dq < 0)?(se - (dq & 0x3FFF)) : (se + dq)));
/* pole prediction diff. */
  dqsez = ((sr - se) + sez);
  lsx_g72x_update(3,y,_witab[i],_fitab[i],dq,sr,dqsez,state_ptr);
  switch(out_coding){
    case 2:
{
      return lsx_g72x_tandem_adjust_alaw(sr,se,y,i,4,qtab_723_24);
    }
    case 1:
{
      return lsx_g72x_tandem_adjust_ulaw(sr,se,y,i,4,qtab_723_24);
    }
    case 3:
{
/* sr was of 14-bit dynamic range */
      return sr << 2;
    }
    default:
{
      return -1;
    }
  }
}
