/* libSoX device driver: ALSA   (c) 2006-2012 SoX contributors
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include <alsa/asoundlib.h>
typedef struct __unnamed_class___F0_L21_C9_unknown_scope_and_name_variable_declaration__variable_type_snd_pcm_uframes_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__buf_len__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_snd_pcm_uframes_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__period__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__snd_pcm_t_snd_pcm__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__pcm__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__buf__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__format {
snd_pcm_uframes_t buf_len;
snd_pcm_uframes_t period;
snd_pcm_t *pcm;
char *buf;
unsigned int format;}priv_t;
static const struct __unnamed_class___F0_L29_C3_unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__bits__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__snd_pcm_format_variable_name_unknown_scope_and_name__scope__alsa_fmt__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__bytes__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L9R_variable_name_unknown_scope_and_name__scope__enc {
unsigned int bits;
enum _snd_pcm_format alsa_fmt;
/* occupied in the buffer per sample */
unsigned int bytes;
sox_encoding_t enc;}formats[] = {
/* order by # of bits; within that, preferred first */
{(8), (SND_PCM_FORMAT_S8), (1), (SOX_ENCODING_SIGN2)}, {(8), (SND_PCM_FORMAT_U8), (1), (SOX_ENCODING_UNSIGNED)}, {(16), (SND_PCM_FORMAT_S16), (2), (SOX_ENCODING_SIGN2)}, {(16), (SND_PCM_FORMAT_U16), (2), (SOX_ENCODING_UNSIGNED)}, {(24), (SND_PCM_FORMAT_S24), (4), (SOX_ENCODING_SIGN2)}, {(24), (SND_PCM_FORMAT_U24), (4), (SOX_ENCODING_UNSIGNED)}, {(24), (SND_PCM_FORMAT_S24_3LE), (3), (SOX_ENCODING_SIGN2)}, {(32), (SND_PCM_FORMAT_S32), (4), (SOX_ENCODING_SIGN2)}, {(32), (SND_PCM_FORMAT_U32), (4), (SOX_ENCODING_UNSIGNED)}, 
/* end of list */
{(0), (0), (0), (SOX_ENCODING_UNKNOWN)}};

static int select_format(sox_encoding_t *encoding_,unsigned int *nbits_,const snd_pcm_format_mask_t *mask,unsigned int *format)
{
/* NB: "to" actually points one after the last */
  unsigned int from = 0;
  unsigned int to;
  int cand = -1;
  while((formats[from].bits <  *nbits_) && (formats[from].bits != 0))
/* find the first entry with at least *nbits_ bits */
    from++;
/* find end of list */
  for (to = from; formats[to].bits != 0; to++) ;{
    while(to > 0){
      unsigned int i;
      unsigned int bits_next = 0;
{
        for (i = from; i < to; i++) {
          ((( *sox_get_globals()).subsystem = "alsa.c") , lsx_debug_most_impl("select_format: trying #%u",i));
          if (snd_pcm_format_mask_test(mask,formats[i].alsa_fmt) != 0) {
            if (formats[i].enc == ( *encoding_)) {
              cand = i;
/* found a match */
              break; 
/* don't overwrite a candidate that
                                       was earlier in the list */
            }
            else if (cand == -1) 
/* will work, but encoding differs */
              cand = i;
          }
        }
      }
      if (cand != -1) 
        break; 
/* no candidate found yet; now try formats with less bits: */
      to = from;
      if (from > 0) 
        bits_next = formats[from - 1].bits;
      while((from != 0U) && (formats[from - 1].bits == bits_next))
/* go back to the first entry with bits_next bits */
        from--;
    }
  }
  if (cand == -1) {
    ((( *sox_get_globals()).subsystem = "alsa.c") , lsx_debug_impl("select_format: no suitable ALSA format found"));
    return -1;
  }
  if (( *nbits_ != formats[cand].bits) || (( *encoding_) != formats[cand].enc)) {
    ((( *sox_get_globals()).subsystem = "alsa.c") , lsx_warn_impl("can\'t encode %u-bit %s", *nbits_,sox_get_encodings_info()[ *encoding_].desc));
     *nbits_ = formats[cand].bits;
     *encoding_ = formats[cand].enc;
  }
  ((( *sox_get_globals()).subsystem = "alsa.c") , lsx_debug_impl("selecting format %d: %s (%s)",cand,snd_pcm_format_name(formats[cand].alsa_fmt),snd_pcm_format_description(formats[cand].alsa_fmt)));
   *format = cand;
  return 0;
}
#define _(x,y) do {if ((err = x y) < 0) {lsx_fail_errno(ft, SOX_EPERM, #x " error: %s", snd_strerror(err)); goto error;} } while (0)

static int setup(sox_format_t *ft)
{
  priv_t *p = (priv_t *)(ft -> priv);
  snd_pcm_hw_params_t *params = (snd_pcm_hw_params_t *)((void *)0);
  snd_pcm_format_mask_t *mask = (snd_pcm_format_mask_t *)((void *)0);
  snd_pcm_uframes_t min;
  snd_pcm_uframes_t max;
  unsigned int n;
  int err;
  do {
    if ((err = snd_pcm_open(&p -> pcm,(ft -> filename),((((ft -> mode) == 'r')?SND_PCM_STREAM_CAPTURE : SND_PCM_STREAM_PLAYBACK)),0)) < 0) {
      lsx_fail_errno(ft,SOX_EPERM,"snd_pcm_open error: %s",snd_strerror(err));
      goto error;
    }
  }while (0);
  do {
    if ((err = snd_pcm_hw_params_malloc(&params)) < 0) {
      lsx_fail_errno(ft,SOX_EPERM,"snd_pcm_hw_params_malloc error: %s",snd_strerror(err));
      goto error;
    }
  }while (0);
  do {
    if ((err = snd_pcm_hw_params_any((p -> pcm),params)) < 0) {
      lsx_fail_errno(ft,SOX_EPERM,"snd_pcm_hw_params_any error: %s",snd_strerror(err));
      goto error;
    }
  }while (0);
#if SND_LIB_VERSION >= 0x010009               /* Disable alsa-lib resampling: */
  do {
    if ((err = snd_pcm_hw_params_set_rate_resample((p -> pcm),params,0)) < 0) {
      lsx_fail_errno(ft,SOX_EPERM,"snd_pcm_hw_params_set_rate_resample error: %s",snd_strerror(err));
      goto error;
    }
  }while (0);
#endif
  do {
    if ((err = snd_pcm_hw_params_set_access((p -> pcm),params,SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
      lsx_fail_errno(ft,SOX_EPERM,"snd_pcm_hw_params_set_access error: %s",snd_strerror(err));
      goto error;
    }
  }while (0);
/* Set format: */
  do {
    if ((err = snd_pcm_format_mask_malloc(&mask)) < 0) {
      lsx_fail_errno(ft,SOX_EPERM,"snd_pcm_format_mask_malloc error: %s",snd_strerror(err));
      goto error;
    }
  }while (0);
  snd_pcm_hw_params_get_format_mask(params,mask);
  do {
    if ((err = select_format(&ft -> encoding.encoding,&ft -> encoding.bits_per_sample,mask,&p -> format)) < 0) {
      lsx_fail_errno(ft,SOX_EPERM,"select_format error: %s",snd_strerror(err));
      goto error;
    }
  }while (0);
  do {
    if ((err = snd_pcm_hw_params_set_format((p -> pcm),params,formats[p -> format].alsa_fmt)) < 0) {
      lsx_fail_errno(ft,SOX_EPERM,"snd_pcm_hw_params_set_format error: %s",snd_strerror(err));
      goto error;
    }
  }while (0);
  (snd_pcm_format_mask_free(mask) , (mask = ((snd_pcm_format_mask_t *)((void *)0))));
/* Set rate: */
  n = ft -> signal.rate;
  do {
    if ((err = snd_pcm_hw_params_set_rate_near((p -> pcm),params,&n,0)) < 0) {
      lsx_fail_errno(ft,SOX_EPERM,"snd_pcm_hw_params_set_rate_near error: %s",snd_strerror(err));
      goto error;
    }
  }while (0);
  ft -> signal.rate = n;
/* Set channels: */
  n = ft -> signal.channels;
  do {
    if ((err = snd_pcm_hw_params_set_channels_near((p -> pcm),params,&n)) < 0) {
      lsx_fail_errno(ft,SOX_EPERM,"snd_pcm_hw_params_set_channels_near error: %s",snd_strerror(err));
      goto error;
    }
  }while (0);
  ft -> signal.channels = n;
/* Get number of significant bits: */
  if ((err = snd_pcm_hw_params_get_sbits(params)) > 0) 
    ft -> signal.precision = (((err <= 32)?err : 32));
  else 
    ((( *sox_get_globals()).subsystem = "alsa.c") , lsx_debug_impl("snd_pcm_hw_params_get_sbits can\'t tell precision: %s",snd_strerror(err)));
/* Set buf_len > > sox_globals.bufsiz for no underrun: */
  p -> buf_len = (((( *sox_get_globals()).bufsiz * 8) / formats[p -> format].bytes) / ft -> signal.channels);
  do {
    if ((err = snd_pcm_hw_params_get_buffer_size_min(params,&min)) < 0) {
      lsx_fail_errno(ft,SOX_EPERM,"snd_pcm_hw_params_get_buffer_size_min error: %s",snd_strerror(err));
      goto error;
    }
  }while (0);
  do {
    if ((err = snd_pcm_hw_params_get_buffer_size_max(params,&max)) < 0) {
      lsx_fail_errno(ft,SOX_EPERM,"snd_pcm_hw_params_get_buffer_size_max error: %s",snd_strerror(err));
      goto error;
    }
  }while (0);
  p -> period = ((((((((p -> buf_len) >= min)?(p -> buf_len) : min)) <= max)?((((p -> buf_len) >= min)?(p -> buf_len) : min)) : max)) / 8);
  p -> buf_len = ((p -> period) * 8);
  do {
    if ((err = snd_pcm_hw_params_set_period_size_near((p -> pcm),params,&p -> period,0)) < 0) {
      lsx_fail_errno(ft,SOX_EPERM,"snd_pcm_hw_params_set_period_size_near error: %s",snd_strerror(err));
      goto error;
    }
  }while (0);
  do {
    if ((err = snd_pcm_hw_params_set_buffer_size_near((p -> pcm),params,&p -> buf_len)) < 0) {
      lsx_fail_errno(ft,SOX_EPERM,"snd_pcm_hw_params_set_buffer_size_near error: %s",snd_strerror(err));
      goto error;
    }
  }while (0);
  if (((p -> period) * 2) > (p -> buf_len)) {
    lsx_fail_errno(ft,SOX_EPERM,"buffer too small");
    goto error;
  }
/* Configure ALSA */
  do {
    if ((err = snd_pcm_hw_params((p -> pcm),params)) < 0) {
      lsx_fail_errno(ft,SOX_EPERM,"snd_pcm_hw_params error: %s",snd_strerror(err));
      goto error;
    }
  }while (0);
  (snd_pcm_hw_params_free(params) , (params = ((snd_pcm_hw_params_t *)((void *)0))));
  do {
    if ((err = snd_pcm_prepare((p -> pcm))) < 0) {
      lsx_fail_errno(ft,SOX_EPERM,"snd_pcm_prepare error: %s",snd_strerror(err));
      goto error;
    }
  }while (0);
/* No longer in `frames' */
  p -> buf_len *= ft -> signal.channels;
  p -> buf = (lsx_realloc(0,((p -> buf_len) * formats[p -> format].bytes)));
  return SOX_SUCCESS;
  error:
  if (mask != 0) 
    snd_pcm_format_mask_free(mask);
  if (params != 0) 
    snd_pcm_hw_params_free(params);
  return SOX_EOF;
}

static int recover(sox_format_t *ft,snd_pcm_t *pcm,int err)
{
  if (err == -32) 
    ((( *sox_get_globals()).subsystem = "alsa.c") , lsx_warn_impl("%s-run",(((ft -> mode) == 'r')?"over" : "under")));
  else if (err != -86) 
    ((( *sox_get_globals()).subsystem = "alsa.c") , lsx_warn_impl("%s",snd_strerror(err)));
  else 
    while((err = snd_pcm_resume(pcm)) == -11){
      ((( *sox_get_globals()).subsystem = "alsa.c") , lsx_report_impl("suspended"));
/* Wait until the suspend flag is released */
      sleep(1);
    }
  if ((err < 0) && ((err = snd_pcm_prepare(pcm)) < 0)) 
    lsx_fail_errno(ft,SOX_EPERM,"%s",snd_strerror(err));
  return err;
}

static size_t read_(sox_format_t *ft,sox_sample_t *buf,size_t len)
{
  priv_t *p = (priv_t *)(ft -> priv);
  snd_pcm_sframes_t i;
  snd_pcm_sframes_t n;
  size_t done;
  len = ((len <= (p -> buf_len))?len : (p -> buf_len));
  for (done = 0; done < len; done += n) {
    do {
      n = snd_pcm_readi((p -> pcm),(p -> buf),((len - done) / ft -> signal.channels));
      if ((n < 0) && (recover(ft,(p -> pcm),((int )n)) < 0)) 
        return 0;
    }while (n <= 0);
    i = (n *= ft -> signal.channels);
{
      switch(formats[p -> format].alsa_fmt){
        case SND_PCM_FORMAT_S8:
{
{
            int8_t *buf1 = (int8_t *)(p -> buf);
            while(i-- != 0L)
               *(buf++) = (((sox_sample_t )( *(buf1++))) << 32 - 8);
            break; 
          }
        }
        case SND_PCM_FORMAT_U8:
{
{
            uint8_t *buf1 = (uint8_t *)(p -> buf);
            while(i-- != 0L)
               *(buf++) = ((((sox_sample_t )( *(buf1++))) << 32 - 8) ^ 1 << 32 - 1);
            break; 
          }
        }
        case SND_PCM_FORMAT_S16:
{
{
            int16_t *buf1 = (int16_t *)(p -> buf);
            if (ft -> encoding.reverse_bytes != 0U) 
              while(i-- != 0L)
                 *(buf++) = (((sox_sample_t )((
{
                  unsigned short __v;
                  unsigned short __x = (unsigned short )( *(buf1++));
                  if (0) 
                    __v = ((unsigned short )(((__x >> 8) & 0xff) | ((__x & 0xff) << 8)));
                  else 
                    asm ("rorw $8, %w0" : "=r" (__v) : "0" (__x));
                  __v;
                }))) << 32 - 16);
            else 
              while(i-- != 0L)
                 *(buf++) = (((sox_sample_t )( *(buf1++))) << 32 - 16);
            break; 
          }
        }
        case SND_PCM_FORMAT_U16:
{
{
            uint16_t *buf1 = (uint16_t *)(p -> buf);
            if (ft -> encoding.reverse_bytes != 0U) 
              while(i-- != 0L)
                 *(buf++) = ((((sox_sample_t )((
{
                  unsigned short __v;
                  unsigned short __x = (unsigned short )( *(buf1++));
                  if (0) 
                    __v = ((unsigned short )(((__x >> 8) & 0xff) | ((__x & 0xff) << 8)));
                  else 
                    asm ("rorw $8, %w0" : "=r" (__v) : "0" (__x));
                  __v;
                }))) << 32 - 16) ^ 1 << 32 - 1);
            else 
              while(i-- != 0L)
                 *(buf++) = ((((sox_sample_t )( *(buf1++))) << 32 - 16) ^ 1 << 32 - 1);
            break; 
          }
        }
        case SND_PCM_FORMAT_S24:
{
{
            sox_int24_t *buf1 = (sox_int24_t *)(p -> buf);
            while(i-- != 0L)
               *(buf++) = (((sox_sample_t )( *(buf1++))) << 32 - 24);
            break; 
          }
        }
        case SND_PCM_FORMAT_S24_3LE:
{
{
            unsigned char *buf1 = (unsigned char *)(p -> buf);
            while(i-- != 0L){
              uint32_t temp;
              temp = ( *(buf1++));
              temp |= (( *(buf1++)) << 8);
              temp |= (( *(buf1++)) << 16);
               *(buf++) = (((sox_sample_t )((sox_int24_t )temp)) << 32 - 24);
            }
            break; 
          }
        }
        case SND_PCM_FORMAT_U24:
{
{
            sox_uint24_t *buf1 = (sox_uint24_t *)(p -> buf);
            while(i-- != 0L)
               *(buf++) = ((((sox_sample_t )( *(buf1++))) << 32 - 24) ^ 1 << 32 - 1);
            break; 
          }
        }
        case SND_PCM_FORMAT_S32:
{
{
            int32_t *buf1 = (int32_t *)(p -> buf);
            while(i-- != 0L)
               *(buf++) = ((sox_sample_t )( *(buf1++)));
            break; 
          }
        }
        case SND_PCM_FORMAT_U32:
{
{
            uint32_t *buf1 = (uint32_t *)(p -> buf);
            while(i-- != 0L)
               *(buf++) = (((sox_sample_t )( *(buf1++))) ^ 1 << 32 - 1);
            break; 
          }
        }
        default:
{
          lsx_fail_errno(ft,SOX_EFMT,"invalid format");
          return 0;
        }
      }
    }
  }
  return len;
}

static size_t write_(sox_format_t *ft,const sox_sample_t *buf,size_t len)
{
  priv_t *p = (priv_t *)(ft -> priv);
  size_t done;
  size_t i;
  size_t n;
  snd_pcm_sframes_t actual;
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  for (done = 0; done < len; done += n) {
    i = (n = (((len - done) <= (p -> buf_len))?(len - done) : (p -> buf_len)));
{
      switch(formats[p -> format].alsa_fmt){
        case SND_PCM_FORMAT_S8:
{
{
            int8_t *buf1 = (int8_t *)(p -> buf);
            while(i-- != 0UL)
               *(buf1++) = ((sox_int8_t )((((sox_macro_temp_double , (sox_macro_temp_sample =  *(buf++)))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - (1 << 31 - 8))?((++ft -> clips , ((unsigned int )(-1)) >> 33 - 8)) : (((sox_uint32_t )(sox_macro_temp_sample + (1 << 31 - 8))) >> 32 - 8)))));
            break; 
          }
        }
        case SND_PCM_FORMAT_U8:
{
{
            uint8_t *buf1 = (uint8_t *)(p -> buf);
            while(i-- != 0UL)
               *(buf1++) = ((sox_uint8_t )(((sox_int8_t )((((sox_macro_temp_double , (sox_macro_temp_sample =  *(buf++)))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - (1 << 31 - 8))?((++ft -> clips , ((unsigned int )(-1)) >> 33 - 8)) : (((sox_uint32_t )(sox_macro_temp_sample + (1 << 31 - 8))) >> 32 - 8))))) ^ 1 << 8 - 1));
            break; 
          }
        }
        case SND_PCM_FORMAT_S16:
{
{
            int16_t *buf1 = (int16_t *)(p -> buf);
            if (ft -> encoding.reverse_bytes != 0U) 
              while(i-- != 0UL)
                 *(buf1++) = ((
{
                  unsigned short __v;
                  unsigned short __x = (unsigned short )((sox_int16_t )((((sox_macro_temp_double , (sox_macro_temp_sample =  *(buf++)))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - (1 << 31 - 16))?((++ft -> clips , ((unsigned int )(-1)) >> 33 - 16)) : (((sox_uint32_t )(sox_macro_temp_sample + (1 << 31 - 16))) >> 32 - 16)))));
                  if (0) 
                    __v = ((unsigned short )(((__x >> 8) & 0xff) | ((__x & 0xff) << 8)));
                  else 
                    asm ("rorw $8, %w0" : "=r" (__v) : "0" (__x));
                  __v;
                }));
            else 
              while(i-- != 0UL)
                 *(buf1++) = ((sox_int16_t )((((sox_macro_temp_double , (sox_macro_temp_sample =  *(buf++)))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - (1 << 31 - 16))?((++ft -> clips , ((unsigned int )(-1)) >> 33 - 16)) : (((sox_uint32_t )(sox_macro_temp_sample + (1 << 31 - 16))) >> 32 - 16)))));
            break; 
          }
        }
        case SND_PCM_FORMAT_U16:
{
{
            uint16_t *buf1 = (uint16_t *)(p -> buf);
            if (ft -> encoding.reverse_bytes != 0U) 
              while(i-- != 0UL)
                 *(buf1++) = ((
{
                  unsigned short __v;
                  unsigned short __x = (unsigned short )(((sox_int16_t )((((sox_macro_temp_double , (sox_macro_temp_sample =  *(buf++)))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - (1 << 31 - 16))?((++ft -> clips , ((unsigned int )(-1)) >> 33 - 16)) : (((sox_uint32_t )(sox_macro_temp_sample + (1 << 31 - 16))) >> 32 - 16))))) ^ 1 << 16 - 1);
                  if (0) 
                    __v = ((unsigned short )(((__x >> 8) & 0xff) | ((__x & 0xff) << 8)));
                  else 
                    asm ("rorw $8, %w0" : "=r" (__v) : "0" (__x));
                  __v;
                }));
            else 
              while(i-- != 0UL)
                 *(buf1++) = ((sox_uint16_t )(((sox_int16_t )((((sox_macro_temp_double , (sox_macro_temp_sample =  *(buf++)))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - (1 << 31 - 16))?((++ft -> clips , ((unsigned int )(-1)) >> 33 - 16)) : (((sox_uint32_t )(sox_macro_temp_sample + (1 << 31 - 16))) >> 32 - 16))))) ^ 1 << 16 - 1));
            break; 
          }
        }
        case SND_PCM_FORMAT_S24:
{
{
            sox_int24_t *buf1 = (sox_int24_t *)(p -> buf);
            while(i-- != 0UL)
               *(buf1++) = ((sox_int24_t )((((sox_macro_temp_double , (sox_macro_temp_sample =  *(buf++)))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - (1 << 31 - 24))?((++ft -> clips , ((unsigned int )(-1)) >> 33 - 24)) : (((sox_uint32_t )(sox_macro_temp_sample + (1 << 31 - 24))) >> 32 - 24)))));
            break; 
          }
        }
        case SND_PCM_FORMAT_S24_3LE:
{
{
            unsigned char *buf1 = (unsigned char *)(p -> buf);
            while(i-- != 0UL){
              uint32_t temp = (uint32_t )((sox_int24_t )((((sox_macro_temp_double , (sox_macro_temp_sample =  *(buf++)))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - (1 << 31 - 24))?((++ft -> clips , ((unsigned int )(-1)) >> 33 - 24)) : (((sox_uint32_t )(sox_macro_temp_sample + (1 << 31 - 24))) >> 32 - 24)))));
               *(buf1++) = (temp & 0xff);
               *(buf1++) = ((temp & 0x0000FF00) >> 8);
               *(buf1++) = ((temp & 0x00FF0000) >> 16);
            }
            break; 
          }
        }
        case SND_PCM_FORMAT_U24:
{
{
            sox_uint24_t *buf1 = (sox_uint24_t *)(p -> buf);
            while(i-- != 0UL)
               *(buf1++) = ((sox_uint24_t )(((sox_int24_t )((((sox_macro_temp_double , (sox_macro_temp_sample =  *(buf++)))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - (1 << 31 - 24))?((++ft -> clips , ((unsigned int )(-1)) >> 33 - 24)) : (((sox_uint32_t )(sox_macro_temp_sample + (1 << 31 - 24))) >> 32 - 24))))) ^ 1 << 24 - 1));
            break; 
          }
        }
        case SND_PCM_FORMAT_S32:
{
{
            int32_t *buf1 = (int32_t *)(p -> buf);
            while(i-- != 0UL)
               *(buf1++) = ((sox_int32_t )( *(buf++)));
            break; 
          }
        }
        case SND_PCM_FORMAT_U32:
{
{
            uint32_t *buf1 = (uint32_t *)(p -> buf);
            while(i-- != 0UL)
               *(buf1++) = ((sox_uint32_t )( *(buf++) ^ 1 << 32 - 1));
            break; 
          }
        }
        default:
{
          lsx_fail_errno(ft,SOX_EFMT,"invalid format");
          return 0;
        }
      }
    }
    for (i = 0; i < n; i += (actual * ft -> signal.channels)) 
      do {
        actual = snd_pcm_writei((p -> pcm),((p -> buf) + (i * formats[p -> format].bytes)),((n - i) / ft -> signal.channels));
/* Happens naturally; don't report it: */
        if ( *__errno_location() == 11) 
           *__errno_location() = 0;
        if ((actual < 0) && (recover(ft,(p -> pcm),((int )actual)) < 0)) 
          return 0;
      }while (actual < 0);
  }
  return len;
}

static int stop(sox_format_t *ft)
{
  priv_t *p = (priv_t *)(ft -> priv);
  snd_pcm_close((p -> pcm));
  free((p -> buf));
  return SOX_SUCCESS;
}

static int stop_write(sox_format_t *ft)
{
  priv_t *p = (priv_t *)(ft -> priv);
  size_t n = (ft -> signal.channels * (p -> period));
  size_t npad = (n - ((ft -> olength) % n));
/* silent samples */
  sox_sample_t *buf = (((npad * sizeof(( *buf))) != 0ULL)?memset(lsx_realloc(0,(npad * sizeof(( *buf)))),0,(npad * sizeof(( *buf)))) : ((void *)((void *)0)));
/* pad to hardware period: */
  if (npad != n) 
    write_(ft,buf,npad);
  free(buf);
  snd_pcm_drain((p -> pcm));
  return stop(ft);
}
const sox_format_handler_t *lsx_alsa_format_fn();

const sox_format_handler_t *lsx_alsa_format_fn()
{
  static const char *const names[] = {("alsa"), ((const char *)((void *)0))};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_SIGN2), (32), (24), (16), (8), (0), (SOX_ENCODING_UNSIGNED), (32), (24), (16), (8), (0), (0)};
  static const sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("Advanced Linux Sound Architecture device driver"), (names), ((2 | 1)), (setup), (read_), (stop), (setup), (write_), (stop_write), ((sox_format_handler_seek )((void *)0)), (write_encodings), ((const sox_rate_t *)((void *)0)), ((sizeof(priv_t )))};
  return &handler;
}
