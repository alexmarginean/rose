/* General purpose, i.e. non SoX specific, utility functions.
 * Copyright (c) 2007-8 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include <ctype.h>
#include <stdio.h>

int lsx_strcasecmp(const char *s1,const char *s2)
{
#if defined(HAVE_STRCASECMP)
  return strcasecmp(s1,s2);
#elif defined(_MSC_VER)
#else
#endif
}

int lsx_strncasecmp(const char *s1,const char *s2,size_t n)
{
#if defined(HAVE_STRCASECMP)
  return strncasecmp(s1,s2,n);
#elif defined(_MSC_VER)
#else
#endif
}

sox_bool lsx_strends(const char *str,const char *end)
{
  size_t str_len = strlen(str);
  size_t end_len = strlen(end);
  return ((str_len >= end_len) && !(strcmp(((str + str_len) - end_len),end) != 0));
}

const char *lsx_find_file_extension(const char *pathname)
{
/* First, chop off any path portions of filename.  This
   * prevents the next search from considering that part. */
  const char *result = (strrchr(pathname,'/'));
  if (!(result != 0)) 
    result = pathname;
/* Now look for an filename extension */
  result = (strrchr(result,'.'));
  if (result != 0) 
    ++result;
  return result;
}

const lsx_enum_item *lsx_find_enum_text(const char *text,const lsx_enum_item *enum_items,int flags)
{
/* Assume not found */
  const lsx_enum_item *result = (const lsx_enum_item *)((void *)0);
  sox_bool sensitive = (!(!((flags & lsx_find_enum_item_case_sensitive) != 0)));
  while((enum_items -> text) != 0){
    if ((!(sensitive != 0U) && !(strcasecmp(text,(enum_items -> text)) != 0)) || ((sensitive != 0U) && !(strcmp(text,(enum_items -> text)) != 0))) 
/* Found exact match */
      return enum_items;
    if ((!(sensitive != 0U) && !(strncasecmp(text,(enum_items -> text),strlen(text)) != 0)) || ((sensitive != 0U) && !(strncmp(text,(enum_items -> text),strlen(text)) != 0))) {
      if ((result != ((const lsx_enum_item *)((void *)0))) && ((result -> value) != (enum_items -> value))) 
/* Found ambiguity */
        return 0;
/* Found sub-string match */
      result = enum_items;
    }
    ++enum_items;
  }
  return result;
}

const lsx_enum_item *lsx_find_enum_value(unsigned int value,const lsx_enum_item *enum_items)
{
  for (; (enum_items -> text) != 0; ++enum_items) 
    if (value == (enum_items -> value)) 
      return enum_items;
  return 0;
}

int lsx_enum_option(int c,const char *arg,const lsx_enum_item *items)
{
  const lsx_enum_item *p = lsx_find_enum_text(arg,items,sox_false);
  if (p == ((const lsx_enum_item *)((void *)0))) {
    size_t len = 1;
    char *set = (lsx_realloc(0,len));
     *set = 0;
    for (p = items; (p -> text) != 0; ++p) {
      set = (lsx_realloc(set,(len += (2 + strlen((p -> text))))));
      strcat(set,", ");
      strcat(set,(p -> text));
    }
    ((( *sox_get_globals()).subsystem = "util.c") , lsx_fail_impl("-%c: `%s\' is not one of: %s.",c,arg,(set + 2)));
    free(set);
    return 2147483647;
  }
  return (p -> value);
}

const char *lsx_sigfigs3(double number)
{
  static const char symbols[] = "\000kMGTPEZY";
/* FIXME: not thread-safe */
  static char string[16UL][10UL];
/* ditto */
  static unsigned int n;
  unsigned int a;
  unsigned int b;
  unsigned int c;
  sprintf(string[n = ((n + 1) & 15)],"%#.3g",number);
  switch(sscanf(string[n],"%u.%ue%u",&a,&b,&c)){
/* Can fall through */
    case 2:
{
      if (b != 0U) 
        return string[n];
    }
    case 1:
{
      c = 2;
      break; 
    }
    case 3:
{
      a = ((100 * a) + b);
      break; 
    }
  }
  if (c < sizeof(symbols) / sizeof(symbols[0]) * 3 - 3) 
    switch(c % 3){
      case 0:
{
        sprintf(string[n],"%u.%02u%c",(a / 100),(a % 100),symbols[c / 3]);
        break; 
      }
      case 1:
{
        sprintf(string[n],"%u.%u%c",(a / 10),(a % 10),symbols[c / 3]);
        break; 
      }
      case 2:
{
        sprintf(string[n],"%u%c",a,symbols[c / 3]);
        break; 
      }
    }
  return string[n];
}

const char *lsx_sigfigs3p(double percentage)
{
  static char string[16UL][10UL];
  static unsigned int n;
  sprintf(string[n = ((n + 1) & 15)],"%.1f%%",percentage);
  if (strlen(string[n]) < 5) 
    sprintf(string[n],"%.2f%%",percentage);
  else if (strlen(string[n]) > 5) 
    sprintf(string[n],"%.0f%%",percentage);
  return string[n];
}

int lsx_open_dllibrary(int show_error_on_failure,const char *library_description,const char *const (library_names)[],const lsx_dlfunction_info (func_infos)[],lsx_dlptr selected_funcs[],lsx_dlhandle *pdl)
{
  int failed = 0;
  lsx_dlhandle dl = (lsx_dlhandle )((void *)0);
/* Track enough information to give a good error message about one failure.
   * Let failed symbol load override failed library open, and let failed
   * library open override missing static symbols.
   */
  const char *failed_libname = (const char *)((void *)0);
  const char *failed_funcname = (const char *)((void *)0);
#ifdef HAVE_LIBLTDL
  if ((library_names != 0) && (library_names[0] != 0)) {
    const char *const *libname;
    if (lt_dlinit() != 0) {
      ((( *sox_get_globals()).subsystem = "util.c") , lsx_fail_impl("Unable to load %s - failed to initialize ltdl.",library_description));
      return 1;
    }
{
      for (libname = library_names;  *libname != 0; libname++) {
        ((( *sox_get_globals()).subsystem = "util.c") , lsx_debug_impl("Attempting to open %s (%s).",library_description, *libname));
        dl = lt_dlopenext( *libname);
        if (dl != 0) {
          size_t i;
          ((( *sox_get_globals()).subsystem = "util.c") , lsx_debug_impl("Opened %s (%s).",library_description, *libname));
{
            for (i = 0; func_infos[i].name != 0; i++) {
              union __unnamed_class___F0_L188_C11_L128R_variable_declaration__variable_type_L118R_variable_name_L128R__scope__fn__DELIMITER__L128R_variable_declaration__variable_type___Pb__v__Pe___variable_name_L128R__scope__ptr {
              lsx_dlptr fn;
              void *ptr;}func;
              func.ptr = lt_dlsym(dl,func_infos[i].name);
              selected_funcs[i] = ((func.fn != 0)?func.fn : func_infos[i].stub_func);
              if (!(selected_funcs[i] != 0)) {
                lt_dlclose(dl);
                dl = ((lsx_dlhandle )((void *)0));
                failed_libname =  *libname;
                failed_funcname = func_infos[i].name;
                ((( *sox_get_globals()).subsystem = "util.c") , lsx_debug_impl("Cannot use %s (%s) - missing function \"%s\".",library_description,failed_libname,failed_funcname));
                break; 
              }
            }
          }
          if (dl != 0) 
            break; 
        }
        else if (!(failed_libname != 0)) {
          failed_libname =  *libname;
        }
      }
    }
    if (!(dl != 0)) 
      lt_dlexit();
  }
#endif /* HAVE_LIBLTDL */
  if (!(dl != 0)) {
    size_t i;
{
      for (i = 0; func_infos[i].name != 0; i++) {
        selected_funcs[i] = ((func_infos[i].static_func != 0)?func_infos[i].static_func : func_infos[i].stub_func);
        if (!(selected_funcs[i] != 0)) {
          if (!(failed_libname != 0)) {
            failed_libname = "static";
            failed_funcname = func_infos[i].name;
          }
          failed = 1;
          break; 
        }
      }
    }
  }
  if (failed != 0) {
    size_t i;
    for (i = 0; func_infos[i].name != 0; i++) 
      selected_funcs[i] = ((lsx_dlptr )((void *)0));
#ifdef HAVE_LIBLTDL
#define LTDL_MISSING ""
#else
#define LTDL_MISSING " (Dynamic library support not configured.)"
#endif /* HAVE_LIBLTDL */
    if (failed_funcname != 0) {
      if (show_error_on_failure != 0) 
        ((( *sox_get_globals()).subsystem = "util.c") , lsx_fail_impl("Unable to load %s (%s) function \"%s\".",library_description,failed_libname,failed_funcname));
      else 
        ((( *sox_get_globals()).subsystem = "util.c") , lsx_report_impl("Unable to load %s (%s) function \"%s\".",library_description,failed_libname,failed_funcname));
    }
    else if (failed_libname != 0) {
      if (show_error_on_failure != 0) 
        ((( *sox_get_globals()).subsystem = "util.c") , lsx_fail_impl("Unable to load %s (%s).",library_description,failed_libname));
      else 
        ((( *sox_get_globals()).subsystem = "util.c") , lsx_report_impl("Unable to load %s (%s).",library_description,failed_libname));
    }
    else {
      if (show_error_on_failure != 0) 
        ((( *sox_get_globals()).subsystem = "util.c") , lsx_fail_impl("Unable to load %s - no dynamic library names selected.",library_description));
      else 
        ((( *sox_get_globals()).subsystem = "util.c") , lsx_report_impl("Unable to load %s - no dynamic library names selected.",library_description));
    }
  }
   *pdl = dl;
  return failed;
}

void lsx_close_dllibrary(lsx_dlhandle dl)
{
#ifdef HAVE_LIBLTDL
  if (dl != 0) {
    lt_dlclose(dl);
    lt_dlexit();
  }
#endif /* HAVE_LIBLTDL */
}
