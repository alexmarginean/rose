/* Simple example of using SoX libraries
 *
 * Copyright (c) 2009 robs@users.sourceforge.net
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#include "sox.h"
#include <stdio.h>
/* Concatenate audio files.  Note that the files must have the same number
 * of channels and the same sample rate.
 *
 * Usage: example4 input-1 input-2 [... input-n] output
 */
#define check(x) do if (!(x)) { \
  fprintf(stderr, "check failed: %s\n", #x); goto error; } while (0)

int main(int argc,char *argv[])
{
  sox_format_t *output = (sox_format_t *)((void *)0);
  int i;
/* Need at least 2 input files + 1 output file. */
  do 
    if (!(argc >= 1 + 2 + 1)) {
      fprintf(stderr,"check failed: %s\n","argc >= 1 + 2 + 1");
      goto error;
    }while (0);
  do 
    if (!(sox_init() == SOX_SUCCESS)) {
      fprintf(stderr,"check failed: %s\n","sox_init() == SOX_SUCCESS");
      goto error;
    }while (0);
/* Defer openning the output file as we want to set its characteristics
   * based on those of the input files. */
/* For each input file... */
  for (i = 1; i < (argc - 1); ++i) {
    sox_format_t *input;
/* static quashes `uninitialised' warning.*/
    static sox_signalinfo_t signal;
/* The (maximum) number of samples that we shall read/write at a time;
     * chosen as a rough match to typical operating system I/O buffer size: */
    #define MAX_SAMPLES (size_t)2048
/* Temporary store whilst copying. */
    sox_sample_t samples[2048UL];
    size_t number_read;
/* Open this input file: */
    do 
      if (!((input = sox_open_read(argv[i],0,0,0)) != 0)) {
        fprintf(stderr,"check failed: %s\n","input = sox_open_read(argv[i], NULL, NULL, NULL)");
        goto error;
      }while (0);
/* If this is the first input file... */
    if (i == 1) {
/* Open the output file using the same signal and encoding character-
       * istics as the first input file.  Note that here, input->signal.length
       * will not be equal to the output file length so we are relying on
       * libSoX to set the output length correctly (i.e. non-seekable output
       * is not catered for); an alternative would be to first calculate the
       * output length by summing the lengths of the input files and modifying
       * the second parameter to sox_open_write accordingly. */
      do 
        if (!((output = sox_open_write(argv[argc - 1],(&input -> signal),(&input -> encoding),0,0,0)) != 0)) {
          fprintf(stderr,"check failed: %s\n","output = sox_open_write(argv[argc - 1], &input->signal, &input->encoding, NULL, NULL, NULL)");
          goto error;
        }while (0);
/* Also, we'll store the signal characteristics of the first file
       * so that we can check that these match those of the other inputs: */
      signal = (input -> signal);
    }
    else 
/* Second or subsequent input file... */
{
/* Check that this input file's signal matches that of the first file: */
      do 
        if (!(input -> signal.channels == signal.channels)) {
          fprintf(stderr,"check failed: %s\n","input->signal.channels == signal.channels");
          goto error;
        }while (0);
      do 
        if (!(input -> signal.rate == signal.rate)) {
          fprintf(stderr,"check failed: %s\n","input->signal.rate == signal.rate");
          goto error;
        }while (0);
    }
/* Copy all of the audio from this input file to the output file: */
    while((number_read = sox_read(input,samples,((size_t )2048))) != 0UL)
      do 
        if (!(sox_write(output,samples,number_read) == number_read)) {
          fprintf(stderr,"check failed: %s\n","sox_write(output, samples, number_read) == number_read");
          goto error;
        }while (0);
/* Finished with this input file.*/
    do 
      if (!(sox_close(input) == SOX_SUCCESS)) {
        fprintf(stderr,"check failed: %s\n","sox_close(input) == SOX_SUCCESS");
        goto error;
      }while (0);
  }
/* Finished with the output file. */
  do 
    if (!(sox_close(output) == SOX_SUCCESS)) {
      fprintf(stderr,"check failed: %s\n","sox_close(output) == SOX_SUCCESS");
      goto error;
    }while (0);
  output = ((sox_format_t *)((void *)0));
  do 
    if (!(sox_quit() == SOX_SUCCESS)) {
      fprintf(stderr,"check failed: %s\n","sox_quit() == SOX_SUCCESS");
      goto error;
    }while (0);
  return 0;
/* Truncate output file on error: */
  error:
  if (output != 0) {
    FILE *f;
    sox_close(output);
    if ((f = fopen(argv[argc - 1],"w")) != 0) 
      fclose(f);
  }
  return 1;
}
