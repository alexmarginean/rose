/* libSoX effect: stats   (c) 2009 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include <ctype.h>
#include <string.h>
typedef struct __unnamed_class___F0_L22_C9_unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__scale_bits__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__hex_bits__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__time_constant__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__scale__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__last__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__sigma_x__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__sigma_x2__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__avg_sigma_x2__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__min_sigma_x2__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__max_sigma_x2__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__min__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__max__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__mult__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__min_run__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__min_runs__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__max_run__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__max_runs__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L65R_variable_name_unknown_scope_and_name__scope__num_samples__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L65R_variable_name_unknown_scope_and_name__scope__tc_samples__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L65R_variable_name_unknown_scope_and_name__scope__min_count__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L65R_variable_name_unknown_scope_and_name__scope__max_count__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint32_tUi__typedef_declaration_variable_name_unknown_scope_and_name__scope__mask {
int scale_bits;
int hex_bits;
double time_constant;
double scale;
double last;
double sigma_x;
double sigma_x2;
double avg_sigma_x2;
double min_sigma_x2;
double max_sigma_x2;
double min;
double max;
double mult;
double min_run;
double min_runs;
double max_run;
double max_runs;
off_t num_samples;
off_t tc_samples;
off_t min_count;
off_t max_count;
uint32_t mask;}priv_t;

static int getopts(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  int c;
  lsx_getopt_t optstate;
  lsx_getopt_init(argc,argv,"+x:b:w:s:",0,lsx_getopt_flag_none,1,&optstate);
  p -> time_constant = .05;
  p -> scale = 1;
  while((c = lsx_getopt(&optstate)) != -1){
    switch(c){
      case 'x':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < 2)) || (d > 32)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "stats.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","hex_bits",((double )2),((double )32)));
            return lsx_usage(effp);
          }
          p -> hex_bits = d;
          break; 
        }
      }
      case 'b':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < 2)) || (d > 32)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "stats.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","scale_bits",((double )2),((double )32)));
            return lsx_usage(effp);
          }
          p -> scale_bits = d;
          break; 
        }
      }
      case 'w':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < .01)) || (d > 10)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "stats.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","time_constant",((double ).01),((double )10)));
            return lsx_usage(effp);
          }
          p -> time_constant = d;
          break; 
        }
      }
      case 's':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < (-99))) || (d > 99)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "stats.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","scale",((double )(-99)),((double )99)));
            return lsx_usage(effp);
          }
          p -> scale = d;
          break; 
        }
      }
      default:
{
        ((( *sox_get_globals()).subsystem = "stats.c") , lsx_fail_impl("invalid option `-%c\'",optstate.opt));
        return lsx_usage(effp);
      }
    }
  }
  if ((p -> hex_bits) != 0) 
    p -> scale_bits = (p -> hex_bits);
  return (optstate.ind != argc)?lsx_usage(effp) : SOX_SUCCESS;
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  p -> last = 0;
  p -> mult = exp((((-1) / (p -> time_constant)) / effp -> in_signal.rate));
  p -> tc_samples = (((5 * (p -> time_constant)) * effp -> in_signal.rate) + .5);
  p -> sigma_x = (p -> sigma_x2 = (p -> avg_sigma_x2 = (p -> max_sigma_x2 = 0)));
  p -> min = (p -> min_sigma_x2 = 2);
  p -> max = -(p -> min);
  p -> num_samples = 0;
  p -> mask = 0;
  return SOX_SUCCESS;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *ilen,size_t *olen)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t len = ( *ilen = ( *olen = (( *ilen <=  *olen)? *ilen :  *olen)));
  memcpy(obuf,ibuf,(len * sizeof(( *obuf))));
  for (; len-- != 0UL; (++ibuf , ++p -> num_samples)) {
    double d = (( *ibuf) * (1. / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)));
    if (d < (p -> min)) 
      ((((((p -> min = d) , (p -> min_count = 1))) , (p -> min_run = 1))) , (p -> min_runs = 0));
    else if (d == (p -> min)) {
      ++p -> min_count;
      p -> min_run = ((d == (p -> last))?((p -> min_run) + 1) : 1);
    }
    else if ((p -> last) == (p -> min)) 
      p -> min_runs += ((p -> min_run) * (p -> min_run));
    if (d > (p -> max)) 
      ((((((p -> max = d) , (p -> max_count = 1))) , (p -> max_run = 1))) , (p -> max_runs = 0));
    else if (d == (p -> max)) {
      ++p -> max_count;
      p -> max_run = ((d == (p -> last))?((p -> max_run) + 1) : 1);
    }
    else if ((p -> last) == (p -> max)) 
      p -> max_runs += ((p -> max_run) * (p -> max_run));
    p -> sigma_x += d;
    p -> sigma_x2 += (d * d);
    p -> avg_sigma_x2 = (((p -> avg_sigma_x2) * (p -> mult)) + ((1 - (p -> mult)) * (d * d)));
    if ((p -> num_samples) >= (p -> tc_samples)) {
      if ((p -> avg_sigma_x2) > (p -> max_sigma_x2)) 
        p -> max_sigma_x2 = (p -> avg_sigma_x2);
      if ((p -> avg_sigma_x2) < (p -> min_sigma_x2)) 
        p -> min_sigma_x2 = (p -> avg_sigma_x2);
    }
    p -> last = d;
    p -> mask |= ( *ibuf);
  }
  return SOX_SUCCESS;
}

static int drain(sox_effect_t *effp,sox_sample_t *obuf,size_t *olen)
{
  priv_t *p = (priv_t *)(effp -> priv);
  if ((p -> last) == (p -> min)) 
    p -> min_runs += ((p -> min_run) * (p -> min_run));
  if ((p -> last) == (p -> max)) 
    p -> max_runs += ((p -> max_run) * (p -> max_run));
  (obuf , ( *olen = 0));
  return SOX_SUCCESS;
}

static unsigned int bit_depth(uint32_t mask,double min,double max,unsigned int *x)
{
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  unsigned int result = 32;
  unsigned int dummy = 0;
  for (; (result != 0U) && !((mask & 1) != 0U); (--result , (mask >>= 1))) ;
  if (x != 0) 
     *x = result;
  mask = ((sox_sample_t )((((sox_macro_temp_sample , (sox_macro_temp_double = (max * (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.))))) , ((sox_macro_temp_double < 0)?(((sox_macro_temp_double <= ((sox_sample_t )(1 << 32 - 1)) - .5)?((++dummy , ((sox_sample_t )(1 << 32 - 1)))) : (sox_macro_temp_double - .5))) : (((sox_macro_temp_double >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + .5)?(((sox_macro_temp_double > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)?((++dummy , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : (sox_macro_temp_double + .5)))))));
  if (min < 0) 
    mask |= (~(((sox_sample_t )((((sox_macro_temp_sample , (sox_macro_temp_double = (min * (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.))))) , ((sox_macro_temp_double < 0)?(((sox_macro_temp_double <= ((sox_sample_t )(1 << 32 - 1)) - .5)?((++dummy , ((sox_sample_t )(1 << 32 - 1)))) : (sox_macro_temp_double - .5))) : (((sox_macro_temp_double >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + .5)?(((sox_macro_temp_double > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)?((++dummy , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : (sox_macro_temp_double + .5))))))) << 1));
  for (; (result != 0U) && !((mask & ((sox_sample_t )(1 << 32 - 1))) != 0U); (--result , (mask <<= 1))) ;
  return result;
}

static void output(const priv_t *p,double x)
{
  if ((p -> scale_bits) != 0) {
    unsigned int mult = (1 << ((p -> scale_bits) - 1));
    int i;
    x = floor(((x * mult) + .5));
    i = (((x <= (mult - 1.))?x : (mult - 1.)));
    if ((p -> hex_bits) != 0) 
      if (x < 0) {
        char buf[30UL];
        sprintf(buf,"%x",-i);
        fprintf(stderr," %*c%s",(9 - ((int )(strlen(buf)))),'-',buf);
      }
      else 
        fprintf(stderr," %9x",i);
    else 
      fprintf(stderr," %9i",i);
  }
  else 
    fprintf(stderr," %9.*f",((fabs((p -> scale)) < 10)?6 : 5),((p -> scale) * x));
}

static int stop(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  if (!((effp -> flow) != 0UL)) {
    double min_runs = 0;
    double max_count = 0;
    double min = 2;
    double max = (-2);
    double max_sigma_x = 0;
    double sigma_x = 0;
    double sigma_x2 = 0;
    double min_sigma_x2 = 2;
    double max_sigma_x2 = 0;
    double avg_peak = 0;
    off_t num_samples = 0;
    off_t min_count = 0;
    off_t max_runs = 0;
    uint32_t mask = 0;
    unsigned int b1;
    unsigned int b2;
    unsigned int i;
    unsigned int n = (((effp -> flows) > 1)?(effp -> flows) : 0);
    for (i = 0; i < (effp -> flows); ++i) {
      priv_t *q = (priv_t *)( *((effp - (effp -> flow)) + i)).priv;
      min = ((min <= (q -> min))?min : (q -> min));
      max = ((max >= (q -> max))?max : (q -> max));
      if ((q -> num_samples) < (q -> tc_samples)) 
        q -> min_sigma_x2 = (q -> max_sigma_x2 = ((q -> sigma_x2) / (q -> num_samples)));
      min_sigma_x2 = ((min_sigma_x2 <= (q -> min_sigma_x2))?min_sigma_x2 : (q -> min_sigma_x2));
      max_sigma_x2 = ((max_sigma_x2 >= (q -> max_sigma_x2))?max_sigma_x2 : (q -> max_sigma_x2));
      sigma_x += (q -> sigma_x);
      sigma_x2 += (q -> sigma_x2);
      num_samples += (q -> num_samples);
      mask |= (q -> mask);
      if (fabs((q -> sigma_x)) > fabs(max_sigma_x)) 
        max_sigma_x = (q -> sigma_x);
      min_count += (q -> min_count);
      min_runs += (q -> min_runs);
      max_count += (q -> max_count);
      max_runs += (q -> max_runs);
      avg_peak += ((-(q -> min) >= (q -> max))?-(q -> min) : (q -> max));
    }
    avg_peak /= (effp -> flows);
    if (!(num_samples != 0L)) {
      ((( *sox_get_globals()).subsystem = "stats.c") , lsx_warn_impl("no audio"));
      return SOX_SUCCESS;
    }
    if (n == 2) 
      fprintf(stderr,"             Overall     Left      Right\n");
    else if (n != 0U) {
      fprintf(stderr,"             Overall");
      for (i = 0; i < n; ++i) 
        fprintf(stderr,"     Ch%-3i",(i + 1));
      fprintf(stderr,"\n");
    }
    fprintf(stderr,"DC offset ");
    output(p,(max_sigma_x / (p -> num_samples)));
    for (i = 0; i < n; ++i) {
      priv_t *q = (priv_t *)( *((effp - (effp -> flow)) + i)).priv;
      output(p,((q -> sigma_x) / (q -> num_samples)));
    }
    fprintf(stderr,"\nMin level ");
    output(p,min);
    for (i = 0; i < n; ++i) {
      priv_t *q = (priv_t *)( *((effp - (effp -> flow)) + i)).priv;
      output(p,(q -> min));
    }
    fprintf(stderr,"\nMax level ");
    output(p,max);
    for (i = 0; i < n; ++i) {
      priv_t *q = (priv_t *)( *((effp - (effp -> flow)) + i)).priv;
      output(p,(q -> max));
    }
    fprintf(stderr,"\nPk lev dB %10.2f",(log10(((-min >= max)?-min : max)) * 20));
    for (i = 0; i < n; ++i) {
      priv_t *q = (priv_t *)( *((effp - (effp -> flow)) + i)).priv;
      fprintf(stderr,"%10.2f",(log10(((-(q -> min) >= (q -> max))?-(q -> min) : (q -> max))) * 20));
    }
    fprintf(stderr,"\nRMS lev dB%10.2f",(log10(sqrt((sigma_x2 / num_samples))) * 20));
    for (i = 0; i < n; ++i) {
      priv_t *q = (priv_t *)( *((effp - (effp -> flow)) + i)).priv;
      fprintf(stderr,"%10.2f",(log10(sqrt(((q -> sigma_x2) / (q -> num_samples)))) * 20));
    }
    fprintf(stderr,"\nRMS Pk dB %10.2f",(log10(sqrt(max_sigma_x2)) * 20));
    for (i = 0; i < n; ++i) {
      priv_t *q = (priv_t *)( *((effp - (effp -> flow)) + i)).priv;
      fprintf(stderr,"%10.2f",(log10(sqrt((q -> max_sigma_x2))) * 20));
    }
    fprintf(stderr,"\nRMS Tr dB ");
    if (min_sigma_x2 != 1) 
      fprintf(stderr,"%10.2f",(log10(sqrt(min_sigma_x2)) * 20));
    else 
      fprintf(stderr,"         -");
    for (i = 0; i < n; ++i) {
      priv_t *q = (priv_t *)( *((effp - (effp -> flow)) + i)).priv;
      if ((q -> min_sigma_x2) != 1) 
        fprintf(stderr,"%10.2f",(log10(sqrt((q -> min_sigma_x2))) * 20));
      else 
        fprintf(stderr,"         -");
    }
    if ((effp -> flows) > 1) 
      fprintf(stderr,"\nCrest factor       -");
    else 
      fprintf(stderr,"\nCrest factor %7.2f",((sigma_x2 != 0.00000)?(avg_peak / sqrt((sigma_x2 / num_samples))) : 1));
    for (i = 0; i < n; ++i) {
      priv_t *q = (priv_t *)( *((effp - (effp -> flow)) + i)).priv;
      fprintf(stderr,"%10.2f",(((q -> sigma_x2) != 0.00000)?((((-(q -> min) >= (q -> max))?-(q -> min) : (q -> max))) / sqrt(((q -> sigma_x2) / (q -> num_samples)))) : 1));
    }
    fprintf(stderr,"\nFlat factor%9.2f",(log10(((min_runs + max_runs) / (min_count + max_count))) * 20));
    for (i = 0; i < n; ++i) {
      priv_t *q = (priv_t *)( *((effp - (effp -> flow)) + i)).priv;
      fprintf(stderr," %9.2f",(log10((((q -> min_runs) + (q -> max_runs)) / ((q -> min_count) + (q -> max_count)))) * 20));
    }
    fprintf(stderr,"\nPk count   %9s",lsx_sigfigs3(((min_count + max_count) / (effp -> flows))));
    for (i = 0; i < n; ++i) {
      priv_t *q = (priv_t *)( *((effp - (effp -> flow)) + i)).priv;
      fprintf(stderr," %9s",lsx_sigfigs3(((double )((q -> min_count) + (q -> max_count)))));
    }
    b1 = bit_depth(mask,min,max,&b2);
    fprintf(stderr,"\nBit-depth      %2u/%-2u",b1,b2);
    for (i = 0; i < n; ++i) {
      priv_t *q = (priv_t *)( *((effp - (effp -> flow)) + i)).priv;
      b1 = bit_depth((q -> mask),(q -> min),(q -> max),&b2);
      fprintf(stderr,"     %2u/%-2u",b1,b2);
    }
    fprintf(stderr,"\nNum samples%9s",lsx_sigfigs3(((double )(p -> num_samples))));
    fprintf(stderr,"\nLength s   %9.3f",((p -> num_samples) / effp -> in_signal.rate));
    fprintf(stderr,"\nScale max ");
    output(p,1.);
    fprintf(stderr,"\nWindow s   %9.3f",(p -> time_constant));
    fprintf(stderr,"\n");
  }
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_stats_effect_fn()
{
  static sox_effect_handler_t handler = {("stats"), ("[-b bits|-x bits|-s scale] [-w window-time]"), (256), (getopts), (start), (flow), (drain), (stop), ((sox_effect_handler_kill )((void *)0)), ((sizeof(priv_t )))};
  return (&handler);
}
