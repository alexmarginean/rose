/* Effect: phaser     Copyright (C) 1998 Juergen Mueller And Sundry Contributors
 *
 * This source code is freely redistributable and may be used for
 * any purpose.  This copyright notice must be maintained.
 * Juergen Mueller And Sundry Contributors are not responsible for
 * the consequences of using this software.
 *
 * Flow diagram scheme:                                          August 24, 1998
 *
 *        * gain-in  +---+                     * gain-out
 * ibuff ----------->|   |----------------------------------> obuff
 *                   | + |  * decay
 *                   |   |<------------+
 *                   +---+  _______    |
 *                     |   |       |   |
 *                     +---| delay |---+
 *                         |_______|
 *                            /|\
 *                             |
 *                     +---------------+      +------------------+
 *                     | Delay control |<-----| modulation speed |
 *                     +---------------+      +------------------+
 *
 * The delay is controled by a sine or triangle modulation.
 *
 * Usage:
 *   phaser gain-in gain-out delay decay speed [ -s | -t ]
 *
 * Where:
 *   gain-in, decay : 0.0 .. 1.0             volume
 *   gain-out       : 0.0 ..                 volume
 *   delay          : 0.0 .. 5.0 msec
 *   speed          : 0.1 .. 2.0 Hz          modulation speed
 *   -s             : modulation by sine     (default)
 *   -t             : modulation by triangle
 *
 * Note:
 *   When decay is close to 1.0, the samples may begin clipping or the output
 *   can saturate!  Hint:
 *     in-gain < (1 - decay * decay)
 *     1 / out-gain > gain-in / (1 - decay)
 */
#include "sox_i.h"
#include <string.h>
typedef struct __unnamed_class___F0_L47_C9_unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__in_gain__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__out_gain__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__delay_ms__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__decay__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__mod_speed__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_lsx_wave_tL126R__typedef_declaration_variable_name_unknown_scope_and_name__scope__mod_type__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__i__Pe___variable_name_unknown_scope_and_name__scope__mod_buf__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__mod_buf_len__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__mod_pos__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__d__Pe___variable_name_unknown_scope_and_name__scope__delay_buf__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__delay_buf_len__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__delay_pos {
double in_gain;
double out_gain;
double delay_ms;
double decay;
double mod_speed;
lsx_wave_t mod_type;
int *mod_buf;
size_t mod_buf_len;
int mod_pos;
double *delay_buf;
size_t delay_buf_len;
int delay_pos;}priv_t;

static int getopts(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  char chars[2UL];
/* Set non-zero defaults: */
  p -> in_gain = .4;
  p -> out_gain = .74;
  p -> delay_ms = 3.;
  p -> decay = .4;
  p -> mod_speed = .5;
  (--argc , ++argv);
{
/* break-able block */
    do {{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < .0) || (d > 1)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "phaser.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","in_gain",((double ).0),((double )1)));
            return lsx_usage(effp);
          }
          p -> in_gain = d;
          (--argc , ++argv);
        }
      }
{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < .0) || (d > 1e9)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "phaser.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","out_gain",((double ).0),((double )1e9)));
            return lsx_usage(effp);
          }
          p -> out_gain = d;
          (--argc , ++argv);
        }
      }
{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < .0) || (d > 5)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "phaser.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","delay_ms",((double ).0),((double )5)));
            return lsx_usage(effp);
          }
          p -> delay_ms = d;
          (--argc , ++argv);
        }
      }
{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < .0) || (d > .99)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "phaser.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","decay",((double ).0),((double ).99)));
            return lsx_usage(effp);
          }
          p -> decay = d;
          (--argc , ++argv);
        }
      }
{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < .1) || (d > 2)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "phaser.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","mod_speed",((double ).1),((double )2)));
            return lsx_usage(effp);
          }
          p -> mod_speed = d;
          (--argc , ++argv);
        }
      }
    }while (0);
  }
  if ((argc != 0) && (sscanf(( *argv),"-%1[st]%c",chars,(chars + 1)) == 1)) {
    p -> mod_type = (((( *chars) == 's')?SOX_WAVE_SINE : SOX_WAVE_TRIANGLE));
    (--argc , ++argv);
  }
  if ((p -> in_gain) > (1 - ((p -> decay) * (p -> decay)))) 
    ((( *sox_get_globals()).subsystem = "phaser.c") , lsx_warn_impl("warning: gain-in might cause clipping"));
  if (((p -> in_gain) / (1 - (p -> decay))) > (1 / (p -> out_gain))) 
    ((( *sox_get_globals()).subsystem = "phaser.c") , lsx_warn_impl("warning: gain-out might cause clipping"));
  return (argc != 0)?lsx_usage(effp) : SOX_SUCCESS;
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  p -> delay_buf_len = ((((p -> delay_ms) * .001) * effp -> in_signal.rate) + .5);
  p -> delay_buf = (((((p -> delay_buf_len) * sizeof(( *(p -> delay_buf)))) != 0ULL)?memset(lsx_realloc(0,((p -> delay_buf_len) * sizeof(( *(p -> delay_buf))))),0,((p -> delay_buf_len) * sizeof(( *(p -> delay_buf))))) : ((void *)((void *)0))));
  p -> mod_buf_len = ((effp -> in_signal.rate / (p -> mod_speed)) + .5);
  p -> mod_buf = (lsx_realloc(0,((p -> mod_buf_len) * sizeof(( *(p -> mod_buf))))));
  lsx_generate_wave_table((p -> mod_type),SOX_INT,(p -> mod_buf),(p -> mod_buf_len),1.,((double )(p -> delay_buf_len)),1.57079632679489661923);
  p -> delay_pos = (p -> mod_pos = 0);
/* TODO: calculate actual length */
  effp -> out_signal.length = ((sox_uint64_t )(-1));
  return SOX_SUCCESS;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t len = ( *isamp = ( *osamp = (( *isamp <=  *osamp)? *isamp :  *osamp)));
  while(len-- != 0UL){
    double d = ((( *(ibuf++)) * (p -> in_gain)) + ((p -> delay_buf)[((p -> delay_pos) + (p -> mod_buf)[p -> mod_pos]) % (p -> delay_buf_len)] * (p -> decay)));
    p -> mod_pos = (((p -> mod_pos) + 1) % (p -> mod_buf_len));
    p -> delay_pos = (((p -> delay_pos) + 1) % (p -> delay_buf_len));
    (p -> delay_buf)[p -> delay_pos] = d;
     *(obuf++) = ((((d * (p -> out_gain)) < 0)?((((d * (p -> out_gain)) <= ((sox_sample_t )(1 << 32 - 1)) - .5)?((++effp -> clips , ((sox_sample_t )(1 << 32 - 1)))) : ((d * (p -> out_gain)) - .5))) : ((((d * (p -> out_gain)) >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + .5)?((++effp -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : ((d * (p -> out_gain)) + .5)))));
  }
  return SOX_SUCCESS;
}

static int stop(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  free((p -> delay_buf));
  free((p -> mod_buf));
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_phaser_effect_fn()
{
  static sox_effect_handler_t handler = {("phaser"), ("gain-in gain-out delay decay speed [ -s | -t ]"), ((8 | 128)), (getopts), (start), (flow), ((sox_effect_handler_drain )((void *)0)), (stop), ((sox_effect_handler_kill )((void *)0)), ((sizeof(priv_t )))};
  return (&handler);
}
