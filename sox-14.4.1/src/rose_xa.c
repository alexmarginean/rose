/* libSoX xa.c  Support for Maxis .XA file format
 *
 *      Copyright (C) 2006 Dwayne C. Litzenberger <dlitz@dlitz.net>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Thanks to Valery V. Anisimovsky <samael@avn.mccme.ru> for the
 * "Maxis XA Audio File Format Description", dated 5-01-2002. */
#include "sox_i.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#define HNIBBLE(byte) (((byte) >> 4) & 0xf)
#define LNIBBLE(byte) ((byte) & 0xf)
/* .xa file header */
typedef struct __unnamed_class___F0_L33_C9_unknown_scope_and_name_variable_declaration__variable_type__Ab_c_index_4_Ae__variable_name_unknown_scope_and_name__scope__magic__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint32_tUi__typedef_declaration_variable_name_unknown_scope_and_name__scope__outSize__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint16_tUs__typedef_declaration_variable_name_unknown_scope_and_name__scope__tag__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint16_tUs__typedef_declaration_variable_name_unknown_scope_and_name__scope__channels__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint32_tUi__typedef_declaration_variable_name_unknown_scope_and_name__scope__sampleRate__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint32_tUi__typedef_declaration_variable_name_unknown_scope_and_name__scope__avgByteRate__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint16_tUs__typedef_declaration_variable_name_unknown_scope_and_name__scope__align__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint16_tUs__typedef_declaration_variable_name_unknown_scope_and_name__scope__bits {
/* "XA\0\0", "XAI\0" (sound/speech), or "XAJ\0" (music) */
char magic[4UL];
/* decompressed size of the stream (in bytes) */
uint32_t outSize;
/* WAVEFORMATEX structure for the decompressed data */
/* 0x0001 - PCM data */
uint16_t tag;
/* number of channels */
uint16_t channels;
/* sample rate (samples/sec) */
uint32_t sampleRate;
/* sampleRate * align */
uint32_t avgByteRate;
/* bits / 8 * channels */
uint16_t align;
/* 8 or 16 */
uint16_t bits;}xa_header_t;
typedef struct __unnamed_class___F0_L46_C9_unknown_scope_and_name_variable_declaration__variable_type_int32_ti__typedef_declaration_variable_name_unknown_scope_and_name__scope__curSample__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_int32_ti__typedef_declaration_variable_name_unknown_scope_and_name__scope__prevSample__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_int32_ti__typedef_declaration_variable_name_unknown_scope_and_name__scope__c1__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_int32_ti__typedef_declaration_variable_name_unknown_scope_and_name__scope__c2__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__shift {
/* current sample */
int32_t curSample;
/* previous sample */
int32_t prevSample;
int32_t c1;
int32_t c2;
unsigned int shift;}xa_state_t;
/* Private data for .xa file */
typedef struct __unnamed_class___F0_L55_C9_unknown_scope_and_name_variable_declaration__variable_type_xa_header_tL126R__typedef_declaration_variable_name_unknown_scope_and_name__scope__header__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__xa_state_tL127R__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__state__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__blockSize__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__bufPos__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Uc__Pe___variable_name_unknown_scope_and_name__scope__buf__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__bytesDecoded {
xa_header_t header;
xa_state_t *state;
unsigned int blockSize;
/* position within the current block */
unsigned int bufPos;
/* buffer for the current block */
unsigned char *buf;
/* number of decompressed bytes read */
unsigned int bytesDecoded;}priv_t;
/* coefficients for EA ADPCM */
static const int32_t EA_ADPCM_Table[] = {(0), (240), (460), (392), (0), (0), ((-208)), ((-220)), (0), (1), (3), (4), (7), (8), (10), (11), (0), ((-1)), ((-3)), ((-4))};
/* Clip sample to 16 bits */

inline static int32_t clip16(int32_t sample)
{
  if (sample > 32767) {
    return 32767;
  }
  else if (sample < -32768) {
    return (-32768);
  }
  else {
    return sample;
  }
}

static int startread(sox_format_t *ft)
{
  priv_t *xa = (priv_t *)(ft -> priv);
  char *magic = xa -> header.magic;
/* Check for the magic value */
  if ((lsx_readbuf(ft,xa -> header.magic,((size_t )4)) != 4) || (((memcmp("XA\000\000",xa -> header.magic,((size_t )4)) != 0) && (memcmp("XAI\000",xa -> header.magic,((size_t )4)) != 0)) && (memcmp("XAJ\000",xa -> header.magic,((size_t )4)) != 0))) {
    lsx_fail_errno(ft,SOX_EHDR,"XA: Header not found");
    return SOX_EOF;
  }
/* Read the rest of the header */
  if (lsx_readdw(ft,&xa -> header.outSize) != SOX_SUCCESS) 
    return SOX_EOF;
  if (lsx_readw(ft,&xa -> header.tag) != SOX_SUCCESS) 
    return SOX_EOF;
  if (lsx_readw(ft,&xa -> header.channels) != SOX_SUCCESS) 
    return SOX_EOF;
  if (lsx_readdw(ft,&xa -> header.sampleRate) != SOX_SUCCESS) 
    return SOX_EOF;
  if (lsx_readdw(ft,&xa -> header.avgByteRate) != SOX_SUCCESS) 
    return SOX_EOF;
  if (lsx_readw(ft,&xa -> header.align) != SOX_SUCCESS) 
    return SOX_EOF;
  if (lsx_readw(ft,&xa -> header.bits) != SOX_SUCCESS) 
    return SOX_EOF;
/* Output the data from the header */
  ((( *sox_get_globals()).subsystem = "xa.c") , lsx_debug_impl("XA Header:"));
  ((( *sox_get_globals()).subsystem = "xa.c") , lsx_debug_impl(" szID:          %02x %02x %02x %02x  |%c%c%c%c|",magic[0],magic[1],magic[2],magic[3],(((magic[0] >= 32) && (magic[0] <= 0x7e))?magic[0] : '.'),(((magic[1] >= 32) && (magic[1] <= 0x7e))?magic[1] : '.'),(((magic[2] >= 32) && (magic[2] <= 0x7e))?magic[2] : '.'),(((magic[3] >= 32) && (magic[3] <= 0x7e))?magic[3] : '.')));
  ((( *sox_get_globals()).subsystem = "xa.c") , lsx_debug_impl(" dwOutSize:     %u",xa -> header.outSize));
  ((( *sox_get_globals()).subsystem = "xa.c") , lsx_debug_impl(" wTag:          0x%04x",xa -> header.tag));
  ((( *sox_get_globals()).subsystem = "xa.c") , lsx_debug_impl(" wChannels:     %u",xa -> header.channels));
  ((( *sox_get_globals()).subsystem = "xa.c") , lsx_debug_impl(" dwSampleRate:  %u",xa -> header.sampleRate));
  ((( *sox_get_globals()).subsystem = "xa.c") , lsx_debug_impl(" dwAvgByteRate: %u",xa -> header.avgByteRate));
  ((( *sox_get_globals()).subsystem = "xa.c") , lsx_debug_impl(" wAlign:        %u",xa -> header.align));
  ((( *sox_get_globals()).subsystem = "xa.c") , lsx_debug_impl(" wBits:         %u",xa -> header.bits));
/* Populate the sox_soundstream structure */
  ft -> encoding.encoding = SOX_ENCODING_SIGN2;
  if (!(ft -> encoding.bits_per_sample != 0U) || (ft -> encoding.bits_per_sample == xa -> header.bits)) {
    ft -> encoding.bits_per_sample = xa -> header.bits;
  }
  else {
    ((( *sox_get_globals()).subsystem = "xa.c") , lsx_report_impl("User options overriding size read in .xa header"));
  }
  if ((ft -> signal.channels == 0) || (ft -> signal.channels == xa -> header.channels)) {
    ft -> signal.channels = xa -> header.channels;
  }
  else {
    ((( *sox_get_globals()).subsystem = "xa.c") , lsx_report_impl("User options overriding channels read in .xa header"));
  }
  if ((ft -> signal.rate == 0) || (ft -> signal.rate == xa -> header.sampleRate)) {
    ft -> signal.rate = xa -> header.sampleRate;
  }
  else {
    ((( *sox_get_globals()).subsystem = "xa.c") , lsx_report_impl("User options overriding rate read in .xa header"));
  }
/* Check for supported formats */
  if (ft -> encoding.bits_per_sample != 16) {
    lsx_fail_errno(ft,SOX_EFMT,"%d-bit sample resolution not supported.",ft -> encoding.bits_per_sample);
    return SOX_EOF;
  }
/* Validate the header */
  if (xa -> header.bits != ft -> encoding.bits_per_sample) {
    ((( *sox_get_globals()).subsystem = "xa.c") , lsx_report_impl("Invalid sample resolution %d bits.  Assuming %d bits.",xa -> header.bits,ft -> encoding.bits_per_sample));
    xa -> header.bits = ft -> encoding.bits_per_sample;
  }
  if (xa -> header.align != ((ft -> encoding.bits_per_sample >> 3) * xa -> header.channels)) {
    ((( *sox_get_globals()).subsystem = "xa.c") , lsx_report_impl("Invalid sample alignment value %d.  Assuming %d.",xa -> header.align,((ft -> encoding.bits_per_sample >> 3) * xa -> header.channels)));
    xa -> header.align = ((ft -> encoding.bits_per_sample >> 3) * xa -> header.channels);
  }
  if (xa -> header.avgByteRate != (xa -> header.align * xa -> header.sampleRate)) {
    ((( *sox_get_globals()).subsystem = "xa.c") , lsx_report_impl("Invalid dwAvgByteRate value %d.  Assuming %d.",xa -> header.avgByteRate,(xa -> header.align * xa -> header.sampleRate)));
    xa -> header.avgByteRate = (xa -> header.align * xa -> header.sampleRate);
  }
/* Set up the block buffer */
  xa -> blockSize = (ft -> signal.channels * 15);
  xa -> bufPos = (xa -> blockSize);
/* Allocate memory for the block buffer */
  xa -> buf = ((((1 * ((size_t )(xa -> blockSize))) != 0UL)?memset(lsx_realloc(0,(1 * ((size_t )(xa -> blockSize)))),0,(1 * ((size_t )(xa -> blockSize)))) : ((void *)((void *)0))));
/* Allocate memory for the state */
  xa -> state = ((((sizeof(xa_state_t ) * ft -> signal.channels) != 0ULL)?memset(lsx_realloc(0,(sizeof(xa_state_t ) * ft -> signal.channels)),0,(sizeof(xa_state_t ) * ft -> signal.channels)) : ((void *)((void *)0))));
/* Final initialization */
  xa -> bytesDecoded = 0;
  return SOX_SUCCESS;
}
/*
 * Read up to len samples from a file, converted to signed longs.
 * Return the number of samples read.
 */

static size_t read_samples(sox_format_t *ft,sox_sample_t *buf,size_t len)
{
  priv_t *xa = (priv_t *)(ft -> priv);
  int32_t sample;
  unsigned char inByte;
  size_t i;
  size_t done;
  size_t bytes;
  ft -> sox_errno = SOX_SUCCESS;
  done = 0;
  while(done < len){
    if ((xa -> bufPos) >= (xa -> blockSize)) {
/* Read the next block */
      bytes = lsx_readbuf(ft,(xa -> buf),((size_t )(xa -> blockSize)));
      if (bytes < (xa -> blockSize)) {
        if (lsx_eof(ft) != 0) {
          if (done > 0) {
            return done;
          }
          lsx_fail_errno(ft,SOX_EOF,"Premature EOF on .xa input file");
          return 0;
        }
        else {
/* error */
          lsx_fail_errno(ft,SOX_EOF,"read error on input stream");
          return 0;
        }
      }
      xa -> bufPos = 0;
      for (i = 0; i < ft -> signal.channels; i++) {
        inByte = (xa -> buf)[i];
        (xa -> state)[i].c1 = EA_ADPCM_Table[(inByte >> 4) & 15];
        (xa -> state)[i].c2 = EA_ADPCM_Table[((inByte >> 4) & 15) + 4];
        (xa -> state)[i].shift = ((inByte & 15) + 8);
      }
      xa -> bufPos += ft -> signal.channels;
    }
    else {
/* Process the block */
      for (i = 0; (i < ft -> signal.channels) && (done < len); i++) {
/* high nibble */
        sample = (((xa -> buf)[(xa -> bufPos) + i] >> 4) & 15);
        sample = ((sample << 28) >> (xa -> state)[i].shift);
        sample = ((((sample + ((xa -> state)[i].curSample * (xa -> state)[i].c1)) + ((xa -> state)[i].prevSample * (xa -> state)[i].c2)) + 0x80) >> 8);
        sample = clip16(sample);
        (xa -> state)[i].prevSample = (xa -> state)[i].curSample;
        (xa -> state)[i].curSample = sample;
        buf[done++] = (((sox_sample_t )sample) << 32 - 16);
        xa -> bytesDecoded += (ft -> encoding.bits_per_sample >> 3);
      }
      for (i = 0; (i < ft -> signal.channels) && (done < len); i++) {
/* low nibble */
        sample = ((xa -> buf)[(xa -> bufPos) + i] & 15);
        sample = ((sample << 28) >> (xa -> state)[i].shift);
        sample = ((((sample + ((xa -> state)[i].curSample * (xa -> state)[i].c1)) + ((xa -> state)[i].prevSample * (xa -> state)[i].c2)) + 0x80) >> 8);
        sample = clip16(sample);
        (xa -> state)[i].prevSample = (xa -> state)[i].curSample;
        (xa -> state)[i].curSample = sample;
        buf[done++] = (((sox_sample_t )sample) << 32 - 16);
        xa -> bytesDecoded += (ft -> encoding.bits_per_sample >> 3);
      }
      xa -> bufPos += ft -> signal.channels;
    }
  }
  if (done == 0) {
    return 0;
  }
  return done;
}

static int stopread(sox_format_t *ft)
{
  priv_t *xa = (priv_t *)(ft -> priv);
  ft -> sox_errno = SOX_SUCCESS;
/* Free memory */
  free((xa -> buf));
  xa -> buf = ((unsigned char *)((void *)0));
  free((xa -> state));
  xa -> state = ((xa_state_t *)((void *)0));
  return SOX_SUCCESS;
}
const sox_format_handler_t *lsx_xa_format_fn();

const sox_format_handler_t *lsx_xa_format_fn()
{
  static const char *const names[] = {("xa"), ((const char *)((void *)0))};
  static const sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("16-bit ADPCM audio files used by Maxis games"), (names), ((0x0040 | 0)), (startread), (read_samples), (stopread), ((sox_format_handler_startwrite )((void *)0)), ((sox_format_handler_write )((void *)0)), ((sox_format_handler_stopwrite )((void *)0)), ((sox_format_handler_seek )((void *)0)), ((const unsigned int *)((void *)0)), ((const sox_rate_t *)((void *)0)), ((sizeof(priv_t )))};
  return &handler;
}
