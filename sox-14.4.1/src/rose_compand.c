/* libSoX compander effect
 *
 * Written by Nick Bailey (nick@bailey-family.org.uk or
 *                         n.bailey@elec.gla.ac.uk)
 *
 * Copyright 1999 Chris Bagwell And Nick Bailey
 * This source code is freely redistributable and may be used for
 * any purpose.  This copyright notice must be maintained.
 * Chris Bagwell And Nick Bailey are not responsible for
 * the consequences of using this software.
 */
#include "sox_i.h"
#include <string.h>
#include <stdlib.h>
#include "compandt.h"
/*
 * Compressor/expander effect for libSoX.
 *
 * Flow diagram for one channel:
 *
 *               ------------      ---------------
 *              |            |    |               |     ---
 * ibuff ---+---| integrator |--->| transfer func |--->|   |
 *          |   |            |    |               |    |   |
 *          |    ------------      ---------------     |   |  * gain
 *          |                                          | * |----------->obuff
 *          |       -------                            |   |
 *          |      |       |                           |   |
 *          +----->| delay |-------------------------->|   |
 *                 |       |                            ---
 *                  -------
 */
#define compand_usage \
  "attack1,decay1{,attack2,decay2} [soft-knee-dB:]in-dB1[,out-dB1]{,in-dB2,out-dB2} [gain [initial-volume-dB [delay]]]\n" \
  "\twhere {} means optional and repeatable and [] means optional.\n" \
  "\tdB values are floating point or -inf'; times are in seconds."
/*
 * Note: clipping can occur if the transfer function pushes things too
 * close to 0 dB.  In that case, use a negative gain, or reduce the
 * output level of the transfer function.
 */
typedef struct __unnamed_class___F0_L46_C9_unknown_scope_and_name_variable_declaration__variable_type_sox_compandt_tL127R__typedef_declaration_variable_name_unknown_scope_and_name__scope__transfer_fn__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__un_named_base_type__Pe___variable_name_unknown_scope_and_name__scope__channels__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__expectedChannels__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__delay__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L5R__Pe___variable_name_unknown_scope_and_name__scope__delay_buf__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_ptrdiff_tl__typedef_declaration_variable_name_unknown_scope_and_name__scope__delay_buf_size__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_ptrdiff_tl__typedef_declaration_variable_name_unknown_scope_and_name__scope__delay_buf_index__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_ptrdiff_tl__typedef_declaration_variable_name_unknown_scope_and_name__scope__delay_buf_cnt__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__delay_buf_full__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__arg0__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__arg1__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__arg2 {
sox_compandt_t transfer_fn;
struct __unnamed_class___F0_L49_C3_L128R_variable_declaration__variable_type__Ab_d_index_2_Ae__variable_name_L128R__scope__attack_times__DELIMITER__L128R_variable_declaration__variable_type_d_variable_name_L128R__scope__volume {
/* 0:attack_time, 1:decay_time */
double attack_times[2UL];
/* Current "volume" of each channel */
double volume;}*channels;
/* Also flags that channels aren't to be treated
                               individually when = 1 and input not mono */
unsigned int expectedChannels;
/* Delay to apply before companding */
double delay;
/* Old samples, used for delay processing */
sox_sample_t *delay_buf;
/* Size of delay_buf in samples */
ptrdiff_t delay_buf_size;
/* Index into delay_buf */
ptrdiff_t delay_buf_index;
/* No. of active entries in delay_buf */
ptrdiff_t delay_buf_cnt;
/* Shows buffer situation (important for drain) */
int delay_buf_full;
/* copies of arguments, so that they may be modified */
char *arg0;
char *arg1;
char *arg2;}priv_t;

static int getopts(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *l = (priv_t *)(effp -> priv);
  char *s;
/* To check for extraneous chars. */
  char dummy;
  unsigned int pairs;
  unsigned int i;
  unsigned int j;
  unsigned int commas;
  (--argc , ++argv);
  if ((argc < 2) || (argc > 5)) 
    return lsx_usage(effp);
  l -> arg0 = ((argv[0] != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(argv[0]) + 1)))),argv[0]) : ((char *)((void *)0)));
  l -> arg1 = ((argv[1] != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(argv[1]) + 1)))),argv[1]) : ((char *)((void *)0)));
  l -> arg2 = ((argc > 2)?(((argv[2] != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(argv[2]) + 1)))),argv[2]) : ((char *)((void *)0)))) : ((char *)((void *)0)));
/* Start by checking the attack and decay rates */
  for (((s = (l -> arg0)) , (commas = 0)); ( *s) != 0; ++s) 
    if (( *s) == ',') 
      ++commas;
  if ((commas % 2) == 0) {
    ((( *sox_get_globals()).subsystem = "compand.c") , lsx_fail_impl("there must be an even number of attack/decay parameters"));
    return SOX_EOF;
  }
  pairs = (1 + (commas / 2));
  l -> channels = ((((pairs * sizeof(( *(l -> channels)))) != 0ULL)?memset(lsx_realloc(0,(pairs * sizeof(( *(l -> channels))))),0,(pairs * sizeof(( *(l -> channels))))) : ((void *)((void *)0))));
  l -> expectedChannels = pairs;
/* Now tokenise the rates string and set up these arrays.  Keep
     them in seconds at the moment: we don't know the sample rate yet. */
  for (((i = 0) , (s = strtok((l -> arg0),","))); s != ((char *)((void *)0)); ++i) {
    for (j = 0; j < 2; ++j) {
      if (sscanf(s,"%lf %c",((l -> channels)[i].attack_times + j),&dummy) != 1) {
        ((( *sox_get_globals()).subsystem = "compand.c") , lsx_fail_impl("syntax error trying to read attack/decay time"));
        return SOX_EOF;
      }
      else if ((l -> channels)[i].attack_times[j] < 0) {
        ((( *sox_get_globals()).subsystem = "compand.c") , lsx_fail_impl("attack & decay times can\'t be less than 0 seconds"));
        return SOX_EOF;
      }
      s = strtok(0,",");
    }
  }
  if (!(lsx_compandt_parse(&l -> transfer_fn,(l -> arg1),(l -> arg2)) != 0U)) 
    return SOX_EOF;
/* Set the initial "volume" to be attibuted to the input channels.
     Unless specified, choose 0dB otherwise clipping will
     result if the user has seleced a long attack time */
  for (i = 0; i < (l -> expectedChannels); ++i) {
    double init_vol_dB = 0;
    if ((argc > 3) && (sscanf(argv[3],"%lf %c",&init_vol_dB,&dummy) != 1)) {
      ((( *sox_get_globals()).subsystem = "compand.c") , lsx_fail_impl("syntax error trying to read initial volume"));
      return SOX_EOF;
    }
    else if (init_vol_dB > 0) {
      ((( *sox_get_globals()).subsystem = "compand.c") , lsx_fail_impl("initial volume is relative to maximum volume so can\'t exceed 0dB"));
      return SOX_EOF;
    }
    (l -> channels)[i].volume = pow(10.,(init_vol_dB / 20));
  }
/* If there is a delay, store it. */
  if ((argc > 4) && (sscanf(argv[4],"%lf %c",&l -> delay,&dummy) != 1)) {
    ((( *sox_get_globals()).subsystem = "compand.c") , lsx_fail_impl("syntax error trying to read delay value"));
    return SOX_EOF;
  }
  else if ((l -> delay) < 0) {
    ((( *sox_get_globals()).subsystem = "compand.c") , lsx_fail_impl("delay can\'t be less than 0 seconds"));
    return SOX_EOF;
  }
  return SOX_SUCCESS;
}

static int start(sox_effect_t *effp)
{
  priv_t *l = (priv_t *)(effp -> priv);
  unsigned int i;
  unsigned int j;
  ((( *sox_get_globals()).subsystem = "compand.c") , lsx_debug_impl("%i input channel(s) expected: actually %i",(l -> expectedChannels),effp -> out_signal.channels));
  for (i = 0; i < (l -> expectedChannels); ++i) 
    ((( *sox_get_globals()).subsystem = "compand.c") , lsx_debug_impl("Channel %i: attack = %g decay = %g",i,(l -> channels)[i].attack_times[0],(l -> channels)[i].attack_times[1]));
  if (!(lsx_compandt_show(&l -> transfer_fn,( *(effp -> global_info)).plot) != 0U)) 
    return SOX_EOF;
/* Convert attack and decay rates using number of samples */
  for (i = 0; i < (l -> expectedChannels); ++i) 
    for (j = 0; j < 2; ++j) 
      if ((l -> channels)[i].attack_times[j] > (1.0 / effp -> out_signal.rate)) 
        (l -> channels)[i].attack_times[j] = (1.0 - exp((-1.0 / (effp -> out_signal.rate * (l -> channels)[i].attack_times[j]))));
      else 
        (l -> channels)[i].attack_times[j] = 1.0;
/* Allocate the delay buffer */
  l -> delay_buf_size = (((l -> delay) * effp -> out_signal.rate) * effp -> out_signal.channels);
  if ((l -> delay_buf_size) > 0) 
    l -> delay_buf = ((((((size_t )(l -> delay_buf_size)) * sizeof(( *(l -> delay_buf)))) != 0ULL)?memset(lsx_realloc(0,(((size_t )(l -> delay_buf_size)) * sizeof(( *(l -> delay_buf))))),0,(((size_t )(l -> delay_buf_size)) * sizeof(( *(l -> delay_buf))))) : ((void *)((void *)0))));
  l -> delay_buf_index = 0;
  l -> delay_buf_cnt = 0;
  l -> delay_buf_full = 0;
  return SOX_SUCCESS;
}
/*
 * Update a volume value using the given sample
 * value, the attack rate and decay rate
 */

static void doVolume(double *v,double samp,priv_t *l,int chan)
{
  double s = (-samp / ((sox_sample_t )(1 << 32 - 1)));
  double delta = (s -  *v);
/* increase volume according to attack rate */
  if (delta > 0.0) 
     *v += (delta * (l -> channels)[chan].attack_times[0]);
  else 
/* reduce volume according to decay rate */
     *v += (delta * (l -> channels)[chan].attack_times[1]);
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *l = (priv_t *)(effp -> priv);
  int len = (( *isamp >  *osamp)? *osamp :  *isamp);
  int filechans = effp -> out_signal.channels;
  int idone;
  int odone;
  for (((idone = 0) , (odone = 0)); idone < len; ibuf += filechans) {
    int chan;
{
/* Maintain the volume fields by simulating a leaky pump circuit */
      for (chan = 0; chan < filechans; ++chan) {
        if (((l -> expectedChannels) == 1) && (filechans > 1)) {
/* User is expecting same compander for all channels */
          int i;
          double maxsamp = 0.0;
          for (i = 0; i < filechans; ++i) {
            double rect = fabs(((double )ibuf[i]));
            if (rect > maxsamp) 
              maxsamp = rect;
          }
          doVolume(&(l -> channels)[0].volume,maxsamp,l,0);
          break; 
        }
        else 
          doVolume(&(l -> channels)[chan].volume,fabs(((double )ibuf[chan])),l,chan);
      }
    }
/* Volume memory is updated: perform compand */
    for (chan = 0; chan < filechans; ++chan) {
      int ch = ((l -> expectedChannels) > 1)?chan : 0;
      double level_in_lin = (l -> channels)[ch].volume;
      double level_out_lin = lsx_compandt(&l -> transfer_fn,level_in_lin);
      double checkbuf;
      if ((l -> delay_buf_size) <= 0) {
        checkbuf = (ibuf[chan] * level_out_lin);
        do {
          if (checkbuf > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32))) {
            checkbuf = ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32));
            effp -> clips++;
          }
          else if (checkbuf < ((sox_sample_t )(1 << 32 - 1))) {
            checkbuf = ((sox_sample_t )(1 << 32 - 1));
            effp -> clips++;
          }
        }while (0);
        obuf[odone++] = checkbuf;
        idone++;
      }
      else {
        if ((l -> delay_buf_cnt) >= (l -> delay_buf_size)) {
/* delay buffer is now definitely full */
          l -> delay_buf_full = 1;
          checkbuf = ((l -> delay_buf)[l -> delay_buf_index] * level_out_lin);
          do {
            if (checkbuf > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32))) {
              checkbuf = ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32));
              effp -> clips++;
            }
            else if (checkbuf < ((sox_sample_t )(1 << 32 - 1))) {
              checkbuf = ((sox_sample_t )(1 << 32 - 1));
              effp -> clips++;
            }
          }while (0);
          obuf[odone] = checkbuf;
          odone++;
          idone++;
        }
        else {
          l -> delay_buf_cnt++;
/* no "odone++" because we did not fill obuf[...] */
          idone++;
        }
        (l -> delay_buf)[l -> delay_buf_index++] = ibuf[chan];
        l -> delay_buf_index %= (l -> delay_buf_size);
      }
    }
  }
   *isamp = idone;
   *osamp = odone;
  return SOX_SUCCESS;
}

static int drain(sox_effect_t *effp,sox_sample_t *obuf,size_t *osamp)
{
  priv_t *l = (priv_t *)(effp -> priv);
  size_t chan;
  size_t done = 0;
  if ((l -> delay_buf_full) == 0) 
    l -> delay_buf_index = 0;
  while(((done + effp -> out_signal.channels) <=  *osamp) && ((l -> delay_buf_cnt) > 0))
    for (chan = 0; chan < effp -> out_signal.channels; ++chan) {
      int c = (((l -> expectedChannels) > 1)?chan : 0);
      double level_in_lin = (l -> channels)[c].volume;
      double level_out_lin = lsx_compandt(&l -> transfer_fn,level_in_lin);
      obuf[done++] = ((l -> delay_buf)[l -> delay_buf_index++] * level_out_lin);
      l -> delay_buf_index %= (l -> delay_buf_size);
      l -> delay_buf_cnt--;
    }
   *osamp = done;
  return ((l -> delay_buf_cnt) > 0)?SOX_SUCCESS : SOX_EOF;
}

static int stop(sox_effect_t *effp)
{
  priv_t *l = (priv_t *)(effp -> priv);
  free((l -> delay_buf));
  return SOX_SUCCESS;
}

static int lsx_kill(sox_effect_t *effp)
{
  priv_t *l = (priv_t *)(effp -> priv);
  lsx_compandt_kill(&l -> transfer_fn);
  free((l -> channels));
  free((l -> arg0));
  free((l -> arg1));
  free((l -> arg2));
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_compand_effect_fn()
{
  static sox_effect_handler_t handler = {("compand"), ("attack1,decay1{,attack2,decay2} [soft-knee-dB:]in-dB1[,out-dB1]{,in-dB2,out-dB2} [gain [initial-volume-dB [delay]]]\n\twhere {} means optional and repeatable and [] means optional.\n\tdB values are floating point or -inf\'; times are in seconds."), ((16 | 128)), (getopts), (start), (flow), (drain), (stop), (lsx_kill), ((sizeof(priv_t )))};
  return (&handler);
}
