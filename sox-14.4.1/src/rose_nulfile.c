/* libSoX file format: null   (c) 2006-8 SoX contributors
 * Based on an original idea by Carsten Borchardt
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include <string.h>

static int startread(sox_format_t *ft)
{
  if (!(ft -> signal.rate != 0.00000)) {
    ft -> signal.rate = 48000;
    ((( *sox_get_globals()).subsystem = "nulfile.c") , lsx_report_impl("sample rate not specified; using %g",ft -> signal.rate));
  }
  ft -> signal.precision = ((ft -> encoding.bits_per_sample != 0U)?ft -> encoding.bits_per_sample : 32);
/* Default number of channels is application-dependent */
  return SOX_SUCCESS;
}

static size_t read_samples(sox_format_t *ft,sox_sample_t *buf,size_t len)
{
/* Reading from null generates silence i.e. (sox_sample_t)0. */
  ft;
  memset(buf,0,(sizeof(sox_sample_t ) * len));
/* Return number of samples "read". */
  return len;
}

static size_t write_samples(sox_format_t *ft,const sox_sample_t *buf,size_t len)
{
/* Writing to null just discards the samples */
  (ft , ((void )buf));
/* Return number of samples "written". */
  return len;
}
const sox_format_handler_t *lsx_nul_format_fn();

const sox_format_handler_t *lsx_nul_format_fn()
{
  static const char *const names[] = {("null"), ((const char *)((void *)0))};
  static const sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ((const char *)((void *)0)), (names), ((2 | 4 | 1)), (startread), (read_samples), ((sox_format_handler_stopread )((void *)0)), ((sox_format_handler_startwrite )((void *)0)), (write_samples), ((sox_format_handler_stopwrite )((void *)0)), ((sox_format_handler_seek )((void *)0)), ((const unsigned int *)((void *)0)), ((const sox_rate_t *)((void *)0)), (0)};
  return &handler;
}
