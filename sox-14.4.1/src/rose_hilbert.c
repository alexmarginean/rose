/* libSoX effect: Hilbert transform filter
 *
 * First version of this effect written 11/2011 by Ulrich Klauer, using maths
 * from "Understanding digital signal processing" by Richard G. Lyons.
 *
 * Copyright 2011 Chris Bagwell and SoX Contributors
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include "dft_filter.h"
typedef struct __unnamed_class___F0_L26_C9_unknown_scope_and_name_variable_declaration__variable_type_L129R_variable_name_unknown_scope_and_name__scope__base__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__d__Pe___variable_name_unknown_scope_and_name__scope__h__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__taps {
dft_filter_priv_t base;
double *h;
int taps;}priv_t;

static int getopts(sox_effect_t *effp,int argc,char **argv)
{
  lsx_getopt_t optstate;
  int c;
  priv_t *p = (priv_t *)(effp -> priv);
  dft_filter_priv_t *b = &p -> base;
  b -> filter_ptr = &b -> filter;
  lsx_getopt_init(argc,argv,"+n:",0,lsx_getopt_flag_none,1,&optstate);
  while((c = lsx_getopt(&optstate)) != -1){
    switch(c){
      case 'n':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < 3)) || (d > 32767)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "hilbert.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","taps",((double )3),((double )32767)));
            return lsx_usage(effp);
          }
          p -> taps = d;
          break; 
        }
      }
      default:
{
        ((( *sox_get_globals()).subsystem = "hilbert.c") , lsx_fail_impl("invalid option `-%c\'",optstate.opt));
        return lsx_usage(effp);
      }
    }
  }
  if (((p -> taps) != 0) && (((p -> taps) % 2) == 0)) {
    ((( *sox_get_globals()).subsystem = "hilbert.c") , lsx_fail_impl("only filters with an odd number of taps are supported"));
    return SOX_EOF;
  }
  return (optstate.ind != argc)?lsx_usage(effp) : SOX_SUCCESS;
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  dft_filter_t *f = p -> base.filter_ptr;
  if (!((f -> num_taps) != 0)) {
    int i;
    if (!((p -> taps) != 0)) {
      p -> taps = ((effp -> in_signal.rate / 76.5) + 2);
      p -> taps += (1 - ((p -> taps) % 2));
/* results in a cutoff frequency of about 75 Hz with a Blackman window */
      ((( *sox_get_globals()).subsystem = "hilbert.c") , lsx_debug_impl("choosing number of taps = %d (override with -n)",(p -> taps)));
    }
    p -> h = (lsx_realloc(0,((p -> taps) * sizeof(( *(p -> h))))));
    for (i = 0; i < (p -> taps); i++) {
      int k = (-((p -> taps) / 2) + i);
      if ((k % 2) == 0) {
        (p -> h)[i] = 0.0;
      }
      else {
        double pk = (3.14159265358979323846 * k);
        (p -> h)[i] = ((1 - cos(pk)) / pk);
      }
    }
    lsx_apply_blackman((p -> h),(p -> taps),.16);
    if (( *(effp -> global_info)).plot != sox_plot_off) {
      char title[100UL];
      sprintf(title,"SoX effect: hilbert (%d taps)",(p -> taps));
      lsx_plot_fir((p -> h),(p -> taps),effp -> in_signal.rate,( *(effp -> global_info)).plot,title,-20.,5.);
      free((p -> h));
      return SOX_EOF;
    }
    lsx_set_dft_filter(f,(p -> h),(p -> taps),((p -> taps) / 2));
  }
  return ( *( *lsx_dft_filter_effect_fn()).start)(effp);
}

const sox_effect_handler_t *lsx_hilbert_effect_fn()
{
  static sox_effect_handler_t handler;
  handler =  *lsx_dft_filter_effect_fn();
  handler.name = "hilbert";
  handler.usage = "[-n taps]";
  handler.getopts = getopts;
  handler.start = start;
  handler.priv_size = (sizeof(priv_t ));
  return (&handler);
}
