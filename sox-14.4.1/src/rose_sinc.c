/* Effect: sinc filters     Copyright (c) 2008-9 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include "dft_filter.h"
#include <string.h>
typedef struct __unnamed_class___F0_L22_C9_unknown_scope_and_name_variable_declaration__variable_type_L129R_variable_name_unknown_scope_and_name__scope__base__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__att__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__beta__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__phase__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__Fc0__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__Fc1__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__tbw0__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__tbw1__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_i_index_2_Ae__variable_name_unknown_scope_and_name__scope__num_taps__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__round {
dft_filter_priv_t base;
double att;
double beta;
double phase;
double Fc0;
double Fc1;
double tbw0;
double tbw1;
int num_taps[2UL];
sox_bool round;}priv_t;

static int create(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  dft_filter_priv_t *b = &p -> base;
  char *parse_ptr = argv[0];
  int i = 0;
  lsx_getopt_t optstate;
  lsx_getopt_init(argc,argv,"+ra:b:p:MILt:n:",0,lsx_getopt_flag_none,1,&optstate);
  b -> filter_ptr = &b -> filter;
  p -> phase = 50;
  p -> beta = (-1);
  while(i < 2){
    int c = 1;
    while((c != 0) && ((c = lsx_getopt(&optstate)) != -1)){
      switch(c){
        char *parse_ptr2;
        case 'r':
{
          p -> round = sox_true;
          break; 
        }
        case 'a':
{
{
            char *end_ptr;
            double d = strtod(optstate.arg,&end_ptr);
            if ((((end_ptr == optstate.arg) || (d < 40)) || (d > 180)) || (( *end_ptr) != 0)) {
              ((( *sox_get_globals()).subsystem = "sinc.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","att",((double )40),((double )180)));
              return lsx_usage(effp);
            }
            p -> att = d;
            break; 
          }
        }
        case 'b':
{
{
            char *end_ptr;
            double d = strtod(optstate.arg,&end_ptr);
            if ((((end_ptr == optstate.arg) || (d < 0)) || (d > 256)) || (( *end_ptr) != 0)) {
              ((( *sox_get_globals()).subsystem = "sinc.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","beta",((double )0),((double )256)));
              return lsx_usage(effp);
            }
            p -> beta = d;
            break; 
          }
        }
        case 'p':
{
{
            char *end_ptr;
            double d = strtod(optstate.arg,&end_ptr);
            if ((((end_ptr == optstate.arg) || (d < 0)) || (d > 100)) || (( *end_ptr) != 0)) {
              ((( *sox_get_globals()).subsystem = "sinc.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","phase",((double )0),((double )100)));
              return lsx_usage(effp);
            }
            p -> phase = d;
            break; 
          }
        }
        case 'M':
{
          p -> phase = 0;
          break; 
        }
        case 'I':
{
          p -> phase = 25;
          break; 
        }
        case 'L':
{
          p -> phase = 50;
          break; 
        }
        case 'n':
{
{
            char *end_ptr;
            double d = strtod(optstate.arg,&end_ptr);
            if ((((end_ptr == optstate.arg) || (d < 11)) || (d > 32767)) || (( *end_ptr) != 0)) {
              ((( *sox_get_globals()).subsystem = "sinc.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","num_taps[1]",((double )11),((double )32767)));
              return lsx_usage(effp);
            }
            (p -> num_taps)[1] = d;
            break; 
          }
        }
        case 't':
{
          p -> tbw1 = lsx_parse_frequency_k(optstate.arg,&parse_ptr2,2147483647);
          if (((p -> tbw1) < 1) || (( *parse_ptr2) != 0)) 
            return lsx_usage(effp);
          break; 
        }
        default:
{
          c = 0;
        }
      }
    }
    if ((((p -> att) != 0.00000) && ((p -> beta) >= 0)) || (((p -> tbw1) != 0.00000) && ((p -> num_taps)[1] != 0))) 
      return lsx_usage(effp);
    if (!(i != 0) || !((p -> Fc1) != 0.00000)) 
      ((p -> tbw0 = (p -> tbw1)) , ((p -> num_taps)[0] = (p -> num_taps)[1]));
    if (!(i++ != 0) && (optstate.ind < argc)) {
      if (( *(parse_ptr = argv[optstate.ind++])) != '-') 
        p -> Fc0 = lsx_parse_frequency_k(parse_ptr,&parse_ptr,2147483647);
      if (( *parse_ptr) == '-') 
        p -> Fc1 = lsx_parse_frequency_k((parse_ptr + 1),&parse_ptr,2147483647);
    }
  }
  return ((((optstate.ind != argc) || ((p -> Fc0) < 0)) || ((p -> Fc1) < 0)) || (( *parse_ptr) != 0))?lsx_usage(effp) : SOX_SUCCESS;
}

static void invert(double *h,int n)
{
  int i;
  for (i = 0; i < n; ++i) 
    h[i] = -h[i];
  h[(n - 1) / 2] += 1;
}

static double *lpf(double Fn,double Fc,double tbw,int *num_taps,double att,double *beta,sox_bool round)
{
  if (((Fc /= Fn) <= 0) || (Fc >= 1)) {
     *num_taps = 0;
    return 0;
  }
  att = ((att != 0.00000)?att : 120);
   *beta = (( *beta < 0)?lsx_kaiser_beta(att) :  *beta);
  if (!( *num_taps != 0)) {
    int n = lsx_lpf_num_taps(att,((((tbw != 0.00000)?(tbw / Fn) : .05)) * .5),0);
     *num_taps = (((((n >= 11)?n : 11)) <= 32767)?(((n >= 11)?n : 11)) : 32767);
    if (round != 0U) 
       *num_taps = (1 + (2 * ((int )((((int )((( *num_taps / 2) * Fc) + .5)) / Fc) + .5))));
    ((( *sox_get_globals()).subsystem = "sinc.c") , lsx_report_impl("num taps = %i (from %i)", *num_taps,n));
  }
  return lsx_make_lpf(( *num_taps |= 1),Fc, *beta,1.,sox_false);
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  dft_filter_t *f = p -> base.filter_ptr;
  if (!((f -> num_taps) != 0)) {
    double Fn = (effp -> in_signal.rate * .5);
    double *h[2UL];
    int i;
    int n;
    int post_peak;
    int longer;
    if (((p -> Fc0) >= Fn) || ((p -> Fc1) >= Fn)) {
      ((( *sox_get_globals()).subsystem = "sinc.c") , lsx_fail_impl("filter frequency must be less than sample-rate / 2"));
      return SOX_EOF;
    }
    h[0] = lpf(Fn,(p -> Fc0),(p -> tbw0),((p -> num_taps) + 0),(p -> att),&p -> beta,(p -> round));
    h[1] = lpf(Fn,(p -> Fc1),(p -> tbw1),((p -> num_taps) + 1),(p -> att),&p -> beta,(p -> round));
    if (h[0] != 0) 
      invert(h[0],(p -> num_taps)[0]);
    longer = ((p -> num_taps)[1] > (p -> num_taps)[0]);
    n = (p -> num_taps)[longer];
    if ((h[0] != 0) && (h[1] != 0)) {
      for (i = 0; i < (p -> num_taps)[!(longer != 0)]; ++i) 
        h[longer][i + ((n - (p -> num_taps)[!(longer != 0)]) / 2)] += h[!(longer != 0)][i];
      if ((p -> Fc0) < (p -> Fc1)) 
        invert(h[longer],n);
      free(h[!(longer != 0)]);
    }
    if ((p -> phase) != 50) 
      lsx_fir_to_phase((h + longer),&n,&post_peak,(p -> phase));
    else 
      post_peak = (n >> 1);
    if (( *(effp -> global_info)).plot != sox_plot_off) {
      char title[100UL];
      sprintf(title,"SoX effect: sinc filter freq=%g-%g",(p -> Fc0),(((p -> Fc1) != 0.00000)?(p -> Fc1) : Fn));
      lsx_plot_fir(h[longer],n,effp -> in_signal.rate,( *(effp -> global_info)).plot,title,((-(p -> beta) * 10) - 25),5.);
      return SOX_EOF;
    }
    lsx_set_dft_filter(f,h[longer],n,post_peak);
  }
  return ( *( *lsx_dft_filter_effect_fn()).start)(effp);
}

const sox_effect_handler_t *lsx_sinc_effect_fn()
{
  static sox_effect_handler_t handler;
  handler =  *lsx_dft_filter_effect_fn();
  handler.name = "sinc";
  handler.usage = "[-a att|-b beta] [-p phase|-M|-I|-L] [-t tbw|-n taps] [freqHP][-freqLP [-t tbw|-n taps]]";
  handler.getopts = create;
  handler.start = start;
  handler.priv_size = (sizeof(priv_t ));
  return (&handler);
}
