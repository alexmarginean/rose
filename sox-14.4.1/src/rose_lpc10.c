/* libSoX lpc-10 format.
 *
 * Copyright 2007 Reuben Thomas <rrt@sc3d.org>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#ifdef EXTERNAL_LPC10
#include <lpc10.h>
#else
#include "../lpc10/lpc10.h"
#endif
/* Private data */
typedef struct __unnamed_class___F0_L29_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__lpc10_encoder_state__Pe___variable_name_unknown_scope_and_name__scope__encst__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_f_index_180_Ae__variable_name_unknown_scope_and_name__scope__speech__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__samples__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__lpc10_decoder_state__Pe___variable_name_unknown_scope_and_name__scope__decst {
struct lpc10_encoder_state *encst;
float speech[180UL];
unsigned int samples;
struct lpc10_decoder_state *decst;}priv_t;
/*
  Write the bits in bits[0] through bits[len-1] to file f, in "packed"
  format.
  bits is expected to be an array of len integer values, where each
  integer is 0 to represent a 0 bit, and any other value represents a
  1 bit. This bit string is written to the file f in the form of
  several 8 bit characters. If len is not a multiple of 8, then the
  last character is padded with 0 bits -- the padding is in the least
  significant bits of the last byte. The 8 bit characters are "filled"
  in order from most significant bit to least significant.
*/

static void write_bits(sox_format_t *ft,INT32 *bits,int len)
{
  int i;
/* The next bit position within the variable "data" to
                   place the next bit. */
  uint8_t mask;
/* The contents of the next byte to place in the
                   output. */
  uint8_t data;
/* Fill in the array bits.
   * The first compressed output bit will be the most significant
   * bit of the byte, so initialize mask to 0x80.  The next byte of
   * compressed data is initially 0, and the desired bits will be
   * turned on below.
   */
  mask = 0x80;
  data = 0;
  for (i = 0; i < len; i++) {
/* Turn on the next bit of output data, if necessary. */
    if (bits[i] != 0) {
      data |= mask;
    }
/*
     * If the byte data is full, determined by mask becoming 0,
     * then write the byte to the output file, and reinitialize
     * data and mask for the next output byte.  Also add the byte
     * if (i == len-1), because if len is not a multiple of 8,
     * then mask won't yet be 0.  */
    mask >>= 1;
    if ((mask == 0) || (i == (len - 1))) {
      lsx_writeb(ft,data);
      data = 0;
      mask = 0x80;
    }
  }
}
/*
  Read bits from file f into bits[0] through bits[len-1], in "packed"
  format.
  Read ceiling(len/8) characters from file f, if that many are
  available to read, otherwise read to the end of the file. The first
  character's 8 bits, in order from MSB to LSB, are used to fill
  bits[0] through bits[7]. The second character's bits are used to
  fill bits[8] through bits[15], and so on. If ceiling(len/8)
  characters are available to read, and len is not a multiple of 8,
  then some of the least significant bits of the last character read
  are completely ignored. Every entry of bits[] that is modified is
  changed to either a 0 or a 1.
  The number of bits successfully read is returned, and is always in
  the range 0 to len, inclusive. If it is less than len, it will
  always be a multiple of 8.
*/

static int read_bits(sox_format_t *ft,INT32 *bits,int len)
{
  int i;
  uint8_t c = 0;
/* Unpack the array bits into coded_frame. */
  for (i = 0; i < len; i++) {
    if ((i % 8) == 0) {
      lsx_read_b_buf(ft,&c,((size_t )1));
      if (lsx_eof(ft) != 0) {
        return i;
      }
    }
    if ((c & (0x80 >> (i & 7))) != 0) {
      bits[i] = 1;
    }
    else {
      bits[i] = 0;
    }
  }
  return len;
}

static int startread(sox_format_t *ft)
{
  priv_t *lpc = (priv_t *)(ft -> priv);
  if ((lpc -> decst = lsx_lpc10_create_decoder_state()) == ((struct lpc10_decoder_state *)((void *)0))) {
    fprintf(stderr,"lpc10 could not allocate decoder state");
    return SOX_EOF;
  }
  lpc -> samples = 180;
  return lsx_check_read_params(ft,1,8000.,SOX_ENCODING_LPC10,0,((uint64_t )0),sox_false);
}

static int startwrite(sox_format_t *ft)
{
  priv_t *lpc = (priv_t *)(ft -> priv);
  if ((lpc -> encst = lsx_lpc10_create_encoder_state()) == ((struct lpc10_encoder_state *)((void *)0))) {
    fprintf(stderr,"lpc10 could not allocate encoder state");
    return SOX_EOF;
  }
  lpc -> samples = 0;
  return SOX_SUCCESS;
}

static size_t read_samples(sox_format_t *ft,sox_sample_t *buf,size_t len)
{
  priv_t *lpc = (priv_t *)(ft -> priv);
  size_t nread = 0;
{
    while(nread < len){
      sox_sample_t sox_macro_temp_sample;
      double sox_macro_temp_double;
/* Read more data if buffer is empty */
      if ((lpc -> samples) == 180) {
        INT32 bits[54UL];
        if (read_bits(ft,bits,54) != 54) 
          break; 
        lsx_lpc10_decode(bits,(lpc -> speech),(lpc -> decst));
        lpc -> samples = 0;
      }
      while((nread < len) && ((lpc -> samples) < 180))
        buf[nread++] = ((sox_sample_t )((((sox_macro_temp_sample , (sox_macro_temp_double = ((lpc -> speech)[lpc -> samples++] * (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.))))) , ((sox_macro_temp_double < ((sox_sample_t )(1 << 32 - 1)))?((++ft -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (((sox_macro_temp_double >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)?(((sox_macro_temp_double > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)?((++ft -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : sox_macro_temp_double))))));
    }
  }
  return nread;
}

static size_t write_samples(sox_format_t *ft,const sox_sample_t *buf,size_t len)
{
  priv_t *lpc = (priv_t *)(ft -> priv);
  size_t nwritten = 0;
  while(len > 0){
    while((len > 0) && ((lpc -> samples) < 180)){
      sox_sample_t sox_macro_temp_sample;
      double sox_macro_temp_double;
      (lpc -> speech)[lpc -> samples++] = ((((sox_macro_temp_double , (sox_macro_temp_sample = buf[nwritten++]))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - 0x80)?((++ft -> clips , 1)) : (((sox_macro_temp_sample + 0x80) & ~255) * (1. / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.))))));
      len--;
    }
    if ((lpc -> samples) == 180) {
      INT32 bits[54UL];
      lsx_lpc10_encode((lpc -> speech),bits,(lpc -> encst));
      write_bits(ft,bits,54);
      lpc -> samples = 0;
    }
  }
  return nwritten;
}

static int stopread(sox_format_t *ft)
{
  priv_t *lpc = (priv_t *)(ft -> priv);
  free((lpc -> decst));
  return SOX_SUCCESS;
}

static int stopwrite(sox_format_t *ft)
{
  priv_t *lpc = (priv_t *)(ft -> priv);
  free((lpc -> encst));
  return SOX_SUCCESS;
}
const sox_format_handler_t *lsx_lpc10_format_fn();

const sox_format_handler_t *lsx_lpc10_format_fn()
{
  static const char *const names[] = {("lpc10"), ("lpc"), ((const char *)((void *)0))};
  static const sox_rate_t write_rates[] = {(8000), (0)};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_LPC10), (0), (0)};
  static sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("Low bandwidth, robotic sounding speech compression"), (names), (256), (startread), (read_samples), (stopread), (startwrite), (write_samples), (stopwrite), ((sox_format_handler_seek )((void *)0)), (write_encodings), (write_rates), ((sizeof(priv_t )))};
  return (&handler);
}
