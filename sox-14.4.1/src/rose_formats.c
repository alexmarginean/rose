/* Implements the public API for using libSoX file formats.
 * All public functions & data are prefixed with sox_ .
 *
 * (c) 2005-8 Chris Bagwell and SoX contributors
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#ifdef HAVE_IO_H
  #include <io.h>
#endif
#if HAVE_MAGIC
  #include <magic.h>
#endif
#define PIPE_AUTO_DETECT_SIZE 256 /* Only as much as we can rewind a pipe */
#define AUTO_DETECT_SIZE 4096     /* For seekable file, so no restriction */

static const char *auto_detect_format(sox_format_t *ft,const char *ext)
{
  char data[4096UL];
  size_t len = lsx_readbuf(ft,data,(((ft -> seekable) != 0U)?sizeof(data) : 256));
  #define CHECK(type, p2, l2, d2, p1, l1, d1) if (len >= p1 + l1 && \
      !memcmp(data + p1, d1, (size_t)l1) && !memcmp(data + p2, d2, (size_t)l2)) return #type;
  if (((len >= (0 + 20)) && !(memcmp((data + 0),"Creative Voice File\032",((size_t )20)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "voc";
  if (((len >= (0 + 17)) && !(memcmp((data + 0),"SOUND SAMPLE DATA",((size_t )17)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "smp";
  if (((len >= (0 + 15)) && !(memcmp((data + 0),"ALawSoundFile**",((size_t )15)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "wve";
  if (((len >= (16 + 9)) && !(memcmp((data + 16),"ring.bin",((size_t )9)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "gsrt";
  if (((len >= (0 + 9)) && !(memcmp((data + 0),"#!AMR-WB\n",((size_t )9)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "amr-wb";
  if (((len >= (0 + 8)) && !(memcmp((data + 0),"7\000\000\020m\000\000\020",((size_t )8)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "prc";
  if (((len >= (0 + 7)) && !(memcmp((data + 0),"NIST_1A",((size_t )7)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "sph";
  if (((len >= (0 + 6)) && !(memcmp((data + 0),"#!AMR\n",((size_t )6)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "amr-nb";
  if (((len >= (0 + 6)) && !(memcmp((data + 0),"LM8953",((size_t )6)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "txw";
  if (((len >= (0 + 6)) && !(memcmp((data + 0),"SOUND\032",((size_t )6)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "sndt";
  if (((len >= (29 + 6)) && !(memcmp((data + 29),"vorbis",((size_t )6)) != 0)) && !(memcmp((data + 0),"OggS",((size_t )4)) != 0)) 
    return "vorbis";
  if (((len >= (28 + 6)) && !(memcmp((data + 28),"Speex",((size_t )6)) != 0)) && !(memcmp((data + 0),"OggS",((size_t )4)) != 0)) 
    return "speex";
  if (((len >= (128 + 4)) && !(memcmp((data + 128),"HCOM",((size_t )4)) != 0)) && !(memcmp((data + 65),"FSSD",((size_t )4)) != 0)) 
    return "hcom";
  if (((len >= (8 + 4)) && !(memcmp((data + 8),"WAVE",((size_t )4)) != 0)) && !(memcmp((data + 0),"RIFF",((size_t )4)) != 0)) 
    return "wav";
  if (((len >= (8 + 4)) && !(memcmp((data + 8),"WAVE",((size_t )4)) != 0)) && !(memcmp((data + 0),"RIFX",((size_t )4)) != 0)) 
    return "wav";
  if (((len >= (8 + 4)) && !(memcmp((data + 8),"AIFF",((size_t )4)) != 0)) && !(memcmp((data + 0),"FORM",((size_t )4)) != 0)) 
    return "aiff";
  if (((len >= (8 + 4)) && !(memcmp((data + 8),"AIFC",((size_t )4)) != 0)) && !(memcmp((data + 0),"FORM",((size_t )4)) != 0)) 
    return "aifc";
  if (((len >= (8 + 4)) && !(memcmp((data + 8),"8SVX",((size_t )4)) != 0)) && !(memcmp((data + 0),"FORM",((size_t )4)) != 0)) 
    return "8svx";
  if (((len >= (8 + 4)) && !(memcmp((data + 8),"MAUD",((size_t )4)) != 0)) && !(memcmp((data + 0),"FORM",((size_t )4)) != 0)) 
    return "maud";
  if (((len >= (0 + 4)) && !(memcmp((data + 0),"XA\000\000",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "xa";
  if (((len >= (0 + 4)) && !(memcmp((data + 0),"XAI\000",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "xa";
  if (((len >= (0 + 4)) && !(memcmp((data + 0),"XAJ\000",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "xa";
  if (((len >= (0 + 4)) && !(memcmp((data + 0),".snd",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "au";
  if (((len >= (0 + 4)) && !(memcmp((data + 0),"dns.",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "au";
  if (((len >= (0 + 4)) && !(memcmp((data + 0),"\000ds.",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "au";
  if (((len >= (0 + 4)) && !(memcmp((data + 0),".sd\000",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "au";
  if (((len >= (0 + 4)) && !(memcmp((data + 0),"fLaC",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "flac";
  if (((len >= (0 + 4)) && !(memcmp((data + 0),"2BIT",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "avr";
  if (((len >= (0 + 4)) && !(memcmp((data + 0),"caff",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "caf";
  if (((len >= (0 + 4)) && !(memcmp((data + 0),"wvpk",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "wv";
  if (((len >= (0 + 4)) && !(memcmp((data + 0)," paf",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "paf";
  if (((len >= (0 + 4)) && !(memcmp((data + 0),"d\243\001\000",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "sf";
  if (((len >= (0 + 4)) && !(memcmp((data + 0),"\000\001\243d",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "sf";
  if (((len >= (0 + 4)) && !(memcmp((data + 0),"d\243\002\000",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "sf";
  if (((len >= (0 + 4)) && !(memcmp((data + 0),"\000\002\243d",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "sf";
  if (((len >= (0 + 4)) && !(memcmp((data + 0),"d\243\003\000",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "sf";
  if (((len >= (0 + 4)) && !(memcmp((data + 0),"\000\003\243d",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "sf";
  if (((len >= (0 + 4)) && !(memcmp((data + 0),"d\243\004\000",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "sf";
  if (((len >= (0 + 4)) && !(memcmp((data + 0),".SoX",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "sox";
  if (((len >= (0 + 4)) && !(memcmp((data + 0),"XoS.",((size_t )4)) != 0)) && !(memcmp((data + 0),"",((size_t )0)) != 0)) 
    return "sox";
  if ((ext != 0) && !(strcasecmp(ext,"snd") != 0)) 
    if (((len >= (0 + 2)) && !(memcmp((data + 0),"\000",((size_t )2)) != 0)) && !(memcmp((data + 7),"",((size_t )1)) != 0)) 
      return "sndr";
  #undef CHECK
#if HAVE_MAGIC
#endif
  return 0;
}
static const sox_encodings_info_t s_sox_encodings_info[] = {{(sox_encodings_none), ("n/a"), ("Unknown or not applicable")}, {(sox_encodings_none), ("Signed PCM"), ("Signed Integer PCM")}, {(sox_encodings_none), ("Unsigned PCM"), ("Unsigned Integer PCM")}, {(sox_encodings_none), ("F.P. PCM"), ("Floating Point PCM")}, {(sox_encodings_none), ("F.P. PCM"), ("Floating Point (text) PCM")}, {(sox_encodings_none), ("FLAC"), ("FLAC")}, {(sox_encodings_none), ("HCOM"), ("HCOM")}, {(sox_encodings_none), ("WavPack"), ("WavPack")}, {(sox_encodings_none), ("F.P. WavPack"), ("Floating Point WavPack")}, {(sox_encodings_lossy1), ("u-law"), ("u-law")}, {(sox_encodings_lossy1), ("A-law"), ("A-law")}, {(sox_encodings_lossy1), ("G.721 ADPCM"), ("G.721 ADPCM")}, {(sox_encodings_lossy1), ("G.723 ADPCM"), ("G.723 ADPCM")}, {(sox_encodings_lossy1), ("CL ADPCM (8)"), ("CL ADPCM (from 8-bit)")}, {(sox_encodings_lossy1), ("CL ADPCM (16)"), ("CL ADPCM (from 16-bit)")}, {(sox_encodings_lossy1), ("MS ADPCM"), ("MS ADPCM")}, {(sox_encodings_lossy1), ("IMA ADPCM"), ("IMA ADPCM")}, {(sox_encodings_lossy1), ("OKI ADPCM"), ("OKI ADPCM")}, {(sox_encodings_lossy1), ("DPCM"), ("DPCM")}, {(sox_encodings_none), ("DWVW"), ("DWVW")}, {(sox_encodings_none), ("DWVWN"), ("DWVWN")}, {(sox_encodings_lossy2), ("GSM"), ("GSM")}, {(sox_encodings_lossy2), ("MPEG audio"), ("MPEG audio (layer I, II or III)")}, {(sox_encodings_lossy2), ("Vorbis"), ("Vorbis")}, {(sox_encodings_lossy2), ("AMR-WB"), ("AMR-WB")}, {(sox_encodings_lossy2), ("AMR-NB"), ("AMR-NB")}, {(sox_encodings_lossy2), ("CVSD"), ("CVSD")}, {(sox_encodings_lossy2), ("LPC10"), ("LPC10")}};
enum __unnamed_enum___F0_L147_C1_assert_static__SIZE_MISMATCH_BETWEEN_sox_encoding_t_AND_sox_encodings_info {assert_static__SIZE_MISMATCH_BETWEEN_sox_encoding_t_AND_sox_encodings_info=1};

const sox_encodings_info_t *sox_get_encodings_info()
{
  return s_sox_encodings_info;
}

unsigned int sox_precision(sox_encoding_t encoding,unsigned int bits_per_sample)
{
  switch(encoding){
    case SOX_ENCODING_DWVW:
{
      return bits_per_sample;
    }
/* ? */
    case SOX_ENCODING_DWVWN:
{
      return (!(bits_per_sample != 0U)?16 : 0);
    }
    case SOX_ENCODING_HCOM:
{
      return (!((bits_per_sample & 7) != 0U) && (((bits_per_sample >> 3) - 1) < 1))?bits_per_sample : 0;
    }
    case SOX_ENCODING_FLAC:
{
    }
    case SOX_ENCODING_WAVPACK:
{
      return (!((bits_per_sample & 7) != 0U) && (((bits_per_sample >> 3) - 1) < 4))?bits_per_sample : 0;
    }
    case SOX_ENCODING_SIGN2:
{
      return (bits_per_sample <= 32)?bits_per_sample : 0;
    }
    case SOX_ENCODING_UNSIGNED:
{
      return (!((bits_per_sample & 7) != 0U) && (((bits_per_sample >> 3) - 1) < 4))?bits_per_sample : 0;
    }
    case SOX_ENCODING_ALAW:
{
      return ((bits_per_sample == 8)?13 : 0);
    }
    case SOX_ENCODING_ULAW:
{
      return ((bits_per_sample == 8)?14 : 0);
    }
    case SOX_ENCODING_CL_ADPCM:
{
      return ((bits_per_sample != 0U)?8 : 0);
    }
    case SOX_ENCODING_CL_ADPCM16:
{
      return ((bits_per_sample == 4)?13 : 0);
    }
    case SOX_ENCODING_MS_ADPCM:
{
      return ((bits_per_sample == 4)?14 : 0);
    }
    case SOX_ENCODING_IMA_ADPCM:
{
      return ((bits_per_sample == 4)?13 : 0);
    }
    case SOX_ENCODING_OKI_ADPCM:
{
      return ((bits_per_sample == 4)?12 : 0);
    }
    case SOX_ENCODING_G721:
{
      return ((bits_per_sample == 4)?12 : 0);
    }
    case SOX_ENCODING_G723:
{
      return ((bits_per_sample == 3)?8 : (((bits_per_sample == 5)?14 : 0)));
    }
    case SOX_ENCODING_CVSD:
{
      return ((bits_per_sample == 1)?16 : 0);
    }
/* ? */
    case SOX_ENCODING_DPCM:
{
      return bits_per_sample;
    }
/* Accept the precision returned by the format. */
    case SOX_ENCODING_MP3:
{
      return 0;
    }
    case SOX_ENCODING_GSM:
{
    }
    case SOX_ENCODING_VORBIS:
{
    }
    case SOX_ENCODING_AMR_WB:
{
    }
    case SOX_ENCODING_AMR_NB:
{
    }
    case SOX_ENCODING_LPC10:
{
      return (!(bits_per_sample != 0U)?16 : 0);
    }
    case SOX_ENCODING_FLOAT:
{
    }
    case SOX_ENCODING_WAVPACKF:
{
      return ((bits_per_sample == 32)?24 : (((bits_per_sample == 64)?53 : 0)));
    }
    case SOX_ENCODING_FLOAT_TEXT:
{
      return (!(bits_per_sample != 0U)?53 : 0);
    }
    case SOX_ENCODING_UNKNOWN:
{
    }
    case SOX_ENCODINGS:
{
      break; 
    }
  }
  return 0;
}

void sox_init_encodinginfo(sox_encodinginfo_t *e)
{
  e -> reverse_bytes = sox_option_default;
  e -> reverse_nibbles = sox_option_default;
  e -> reverse_bits = sox_option_default;
  e -> compression = __builtin_huge_val();
}
/*--------------------------------- Comments ---------------------------------*/

size_t sox_num_comments(sox_comments_t comments)
{
  size_t result = 0;
  if (!(comments != 0)) 
    return 0;
  while( *(comments++) != 0)
    ++result;
  return result;
}

void sox_append_comment(sox_comments_t *comments,const char *comment)
{
  size_t n = sox_num_comments( *comments);
   *comments = (lsx_realloc(( *comments),((n + 2) * sizeof(( *( *comments))))));
  (comment != 0)?((void )0) : __assert_fail("comment","formats.c",223,"void sox_append_comment(char ***, const char *)");
  ( *comments)[n++] = ((comment != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(comment) + 1)))),comment) : ((char *)((void *)0)));
  ( *comments)[n] = 0;
}

void sox_append_comments(sox_comments_t *comments,const char *comment)
{
  char *end;
  if (comment != 0) {
    while((end = strchr(comment,10)) != 0){
      size_t len = (end - comment);
      char *c = (lsx_realloc(0,((len + 1) * sizeof(( *c)))));
      strncpy(c,comment,len);
      c[len] = 0;
      sox_append_comment(comments,c);
      comment += (len + 1);
      free(c);
    }
    if (( *comment) != 0) 
      sox_append_comment(comments,comment);
  }
}

sox_comments_t sox_copy_comments(sox_comments_t comments)
{
  sox_comments_t result = 0;
  if (comments != 0) 
    while( *comments != 0)
      sox_append_comment(&result,( *(comments++)));
  return result;
}

void sox_delete_comments(sox_comments_t *comments)
{
  sox_comments_t p =  *comments;
  if (p != 0) 
    while( *p != 0)
      free(( *(p++)));
  free(( *comments));
   *comments = 0;
}

char *lsx_cat_comments(sox_comments_t comments)
{
  sox_comments_t p = comments;
  size_t len = 0;
  char *result;
  if (p != 0) 
    while( *p != 0)
      len += (strlen(( *(p++))) + 1);
  result = (((((((len != 0UL)?len : 1)) * sizeof(( *result))) != 0ULL)?memset(lsx_realloc(0,((((len != 0UL)?len : 1)) * sizeof(( *result)))),0,((((len != 0UL)?len : 1)) * sizeof(( *result)))) : ((void *)((void *)0))));
  if (((p = comments) != 0) && ( *p != 0)) {
    strcpy(result,( *p));
    while( *(++p) != 0)
      strcat(strcat(result,"\n"),( *p));
  }
  return result;
}

const char *sox_find_comment(sox_comments_t comments,const char *id)
{
  size_t len = strlen(id);
  if (comments != 0) 
    for (;  *comments != 0; ++comments) 
      if (!(strncasecmp(( *comments),id,len) != 0) && (( *comments)[len] == '=')) 
        return (( *comments + len) + 1);
  return 0;
}

static void set_endiannesses(sox_format_t *ft)
{
  if (ft -> encoding.opposite_endian != 0U) 
    ft -> encoding.reverse_bytes = ((((ft -> handler.flags & 64) != 0U)?(!((ft -> handler.flags & 128) != 0U) != 0) : sox_true));
  else if (ft -> encoding.reverse_bytes == sox_option_default) 
    ft -> encoding.reverse_bytes = ((((ft -> handler.flags & 64) != 0U)?(!((ft -> handler.flags & 128) != 0U) == 0) : sox_false));
/* FIXME: Change reports to suitable warnings if trying
   * to override something that can't be overridden. */
  if ((ft -> handler.flags & 64) != 0U) {
    if (ft -> encoding.reverse_bytes == ((sox_option_t )(!((ft -> handler.flags & 128) != 0U) != 0))) 
      ((( *sox_get_globals()).subsystem = "formats.c") , lsx_report_impl("`%s\': overriding file-type byte-order",(ft -> filename)));
  }
  else if (ft -> encoding.reverse_bytes == sox_option_yes) 
    ((( *sox_get_globals()).subsystem = "formats.c") , lsx_report_impl("`%s\': overriding machine byte-order",(ft -> filename)));
  if (ft -> encoding.reverse_bits == sox_option_default) 
    ft -> encoding.reverse_bits = (!(!((ft -> handler.flags & 16) != 0U)));
  else if (ft -> encoding.reverse_bits == (!((ft -> handler.flags & 16) != 0U))) 
    ((( *sox_get_globals()).subsystem = "formats.c") , lsx_report_impl("`%s\': overriding file-type bit-order",(ft -> filename)));
  if (ft -> encoding.reverse_nibbles == sox_option_default) 
    ft -> encoding.reverse_nibbles = (!(!((ft -> handler.flags & 32) != 0U)));
  else if (ft -> encoding.reverse_nibbles == (!((ft -> handler.flags & 32) != 0U))) 
    ((( *sox_get_globals()).subsystem = "formats.c") , lsx_report_impl("`%s\': overriding file-type nibble-order",(ft -> filename)));
}

static sox_bool is_seekable(const sox_format_t *ft)
{
  struct stat st;
  (ft != 0)?((void )0) : __assert_fail("ft","formats.c",329,"enum sox_bool is_seekable(const struct sox_format_t *)");
  if (!((ft -> fp) != 0)) 
    return sox_false;
  fstat(fileno(((FILE *)(ft -> fp))),&st);
  return ((st.st_mode & 0170000) == 0100000);
}
/* check that all settings have been given */

static int sox_checkformat(sox_format_t *ft)
{
  ft -> sox_errno = SOX_SUCCESS;
  if (!(ft -> signal.rate != 0.00000)) {
    lsx_fail_errno(ft,SOX_EFMT,"sampling rate was not specified");
    return SOX_EOF;
  }
  if (!(ft -> signal.precision != 0U)) {
    lsx_fail_errno(ft,SOX_EFMT,"data encoding or sample size was not specified");
    return SOX_EOF;
  }
  return SOX_SUCCESS;
}
/* detects only wget-supported URLs */

static sox_bool is_url(const char *text)
{
  return (!(((strncasecmp(text,"http:",((size_t )5)) != 0) && (strncasecmp(text,"https:",((size_t )6)) != 0)) && (strncasecmp(text,"ftp:",((size_t )4)) != 0)));
}

static int xfclose(FILE *file,lsx_io_type io_type)
{
  return (io_type != lsx_io_file)?pclose(file) : fclose(file);
#ifdef HAVE_POPEN
#endif
}

static FILE *xfopen(const char *identifier,const char *mode,lsx_io_type *io_type)
{
   *io_type = lsx_io_file;
  if (( *identifier) == '|') {
    FILE *f = (FILE *)((void *)0);
#ifdef HAVE_POPEN
#ifndef POPEN_MODE
#define POPEN_MODE "r"
#endif
    f = popen((identifier + 1),"r");
     *io_type = lsx_io_pipe;
#else
#endif
    return f;
  }
  else if (is_url(identifier) != 0U) {
    FILE *f = (FILE *)((void *)0);
#ifdef HAVE_POPEN
    const char *const command_format = "wget --no-check-certificate -q -O- \"%s\"";
    char *command = (lsx_realloc(0,(strlen(command_format) + strlen(identifier))));
    sprintf(command,command_format,identifier);
    f = popen(command,"r");
    free(command);
     *io_type = lsx_io_url;
#else
#endif
    return f;
  }
  return fopen(identifier,mode);
}
/* Hack to rewind pipes (a small amount).
 * Works by resetting the FILE buffer pointer */

static void rewind_pipe(FILE *fp)
{
/* _FSTDIO is for Torek stdio (i.e. most BSD-derived libc's)
 * In theory, we no longer need to check _NEWLIB_VERSION or __APPLE__ */
#if defined _FSTDIO || defined _NEWLIB_VERSION || defined __APPLE__
#elif defined __GLIBC__
  fp -> _IO_read_ptr = (fp -> _IO_read_base);
#elif defined _MSC_VER || defined _WIN32 || defined _WIN64 || defined _ISO_STDIO_ISO_H
#else
/* To fix this #error, either simply remove the #error line and live without
   * file-type detection with pipes, or add support for your compiler in the
   * lines above.  Test with cat monkey.wav | ./sox --info - */
  #error FIX NEEDED HERE
  #define NO_REWIND_PIPE
#endif
}

static sox_format_t *open_read(const char *path,void *buffer,size_t buffer_size,const sox_signalinfo_t *signal,const sox_encodinginfo_t *encoding,const char *filetype)
{
  sox_format_t *ft = (1?memset(lsx_realloc(0,(1 * sizeof(( *ft)))),0,(1 * sizeof(( *ft)))) : ((void *)((void *)0)));
  const sox_format_handler_t *handler;
  const char *const io_types[] = {("file"), ("pipe"), ("file URL")};
  const char *type = "";
  size_t input_bufsiz = (( *sox_get_globals()).input_bufsiz != 0UL)?( *sox_get_globals()).input_bufsiz : ( *sox_get_globals()).bufsiz;
  if (filetype != 0) {
    if (!((handler = sox_find_format(filetype,sox_false)) != 0)) {
      ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("no handler for given file type `%s\'",filetype));
      goto error;
    }
    ft -> handler =  *handler;
  }
  if (!((ft -> handler.flags & 1) != 0U)) {
/* Use stdin if the filename is "-" */
    if (!(strcmp(path,"-") != 0)) {
      if (( *sox_get_globals()).stdin_in_use_by != 0) {
        ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("`-\' (stdin) already in use by `%s\'",( *sox_get_globals()).stdin_in_use_by));
        goto error;
      }
      ( *sox_get_globals()).stdin_in_use_by = "audio input";;
      ft -> fp = stdin;
    }
    else {
      ft -> fp = (((buffer != 0)?fmemopen(buffer,buffer_size,"rb") : xfopen(path,"rb",&ft -> io_type)));
#ifdef HAVE_FMEMOPEN
#endif
      type = io_types[ft -> io_type];
      if ((ft -> fp) == ((void *)((void *)0))) {
        ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("can\'t open input %s `%s\': %s",type,path,strerror( *__errno_location())));
        goto error;
      }
    }
    if (setvbuf((ft -> fp),0,0,(sizeof(char ) * input_bufsiz)) != 0) {
      ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("Can\'t set read buffer"));
      goto error;
    }
    ft -> seekable = is_seekable(ft);
  }
  if (!(filetype != 0)) {
    if ((ft -> seekable) != 0U) {
      filetype = auto_detect_format(ft,lsx_find_file_extension(path));
      lsx_rewind(ft);
    }
    else 
#ifndef NO_REWIND_PIPE
if (!((ft -> handler.flags & 1) != 0U) && (input_bufsiz >= 256)) {
      filetype = auto_detect_format(ft,lsx_find_file_extension(path));
      rewind_pipe((ft -> fp));
      ft -> tell_off = 0;
    }
#endif
    if (filetype != 0) {
      ((( *sox_get_globals()).subsystem = "formats.c") , lsx_report_impl("detected file format type `%s\'",filetype));
      if (!((handler = sox_find_format(filetype,sox_false)) != 0)) {
        ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("no handler for detected file type `%s\'",filetype));
        goto error;
      }
    }
    else {
      if ((ft -> io_type) == lsx_io_pipe) {
/* With successful pipe rewind, this isn't useful */
        filetype = "sox";
        ((( *sox_get_globals()).subsystem = "formats.c") , lsx_report_impl("assuming input pipe `%s\' has file-type `sox\'",path));
      }
      else if (!((filetype = lsx_find_file_extension(path)) != 0)) {
        ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("can\'t determine type of %s `%s\'",type,path));
        goto error;
      }
      if (!((handler = sox_find_format(filetype,sox_true)) != 0)) {
        ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("no handler for file extension `%s\'",filetype));
        goto error;
      }
    }
    ft -> handler =  *handler;
    if ((ft -> handler.flags & 1) != 0U) {
      xfclose((ft -> fp),(ft -> io_type));
      ft -> fp = ((void *)((void *)0));
    }
  }
  if (!(ft -> handler.startread != 0) && !(ft -> handler.read != 0)) {
    ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("file type `%s\' isn\'t readable",filetype));
    goto error;
  }
  ft -> mode = 'r';
  ft -> filetype = ((filetype != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(filetype) + 1)))),filetype) : ((char *)((void *)0)));
  ft -> filename = ((path != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(path) + 1)))),path) : ((char *)((void *)0)));
  if (signal != 0) 
    ft -> signal =  *signal;
  if (encoding != 0) 
    ft -> encoding =  *encoding;
  else 
    sox_init_encodinginfo(&ft -> encoding);
  set_endiannesses(ft);
  if (((ft -> handler.flags & 2) != 0U) && !((ft -> handler.flags & 4) != 0U)) 
    lsx_set_signal_defaults(ft);
  ft -> priv = (((1 * ft -> handler.priv_size) != 0UL)?memset(lsx_realloc(0,(1 * ft -> handler.priv_size)),0,(1 * ft -> handler.priv_size)) : ((void *)((void *)0)));
/* Read and write starters can change their formats. */
  if ((ft -> handler.startread != 0) && (( *ft -> handler.startread)(ft) != SOX_SUCCESS)) {
    ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("can\'t open input %s `%s\': %s",type,(ft -> filename),(ft -> sox_errstr)));
    goto error;
  }
/* Fill in some defaults: */
  if (sox_precision(ft -> encoding.encoding,ft -> encoding.bits_per_sample) != 0U) 
    ft -> signal.precision = sox_precision(ft -> encoding.encoding,ft -> encoding.bits_per_sample);
  if (!((ft -> handler.flags & 4) != 0U) && !(ft -> signal.channels != 0U)) 
    ft -> signal.channels = 1;
  if (sox_checkformat(ft) != SOX_SUCCESS) {
    ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("bad input format for %s `%s\': %s",type,(ft -> filename),(ft -> sox_errstr)));
    goto error;
  }
  if (signal != 0) {
    if (((signal -> rate) != 0.00000) && ((signal -> rate) != ft -> signal.rate)) 
      ((( *sox_get_globals()).subsystem = "formats.c") , lsx_warn_impl("can\'t set sample rate %g; using %g",(signal -> rate),ft -> signal.rate));
    if (((signal -> channels) != 0U) && ((signal -> channels) != ft -> signal.channels)) 
      ((( *sox_get_globals()).subsystem = "formats.c") , lsx_warn_impl("can\'t set %u channels; using %u",(signal -> channels),ft -> signal.channels));
  }
  return ft;
  error:
  if (((ft -> fp) != 0) && ((ft -> fp) != stdin)) 
    xfclose((ft -> fp),(ft -> io_type));
  free((ft -> priv));
  free((ft -> filename));
  free((ft -> filetype));
  free(ft);
  return 0;
}

sox_format_t *sox_open_read(const char *path,const sox_signalinfo_t *signal,const sox_encodinginfo_t *encoding,const char *filetype)
{
  return open_read(path,0,((size_t )0),signal,encoding,filetype);
}

sox_format_t *sox_open_mem_read(void *buffer,size_t buffer_size,const sox_signalinfo_t *signal,const sox_encodinginfo_t *encoding,const char *filetype)
{
  return open_read("",buffer,buffer_size,signal,encoding,filetype);
}

sox_bool sox_format_supports_encoding(const char *path,const char *filetype,const sox_encodinginfo_t *encoding)
{
  #define enc_arg(T) (T)handler->write_formats[i++]
  sox_bool is_file_extension = (filetype == ((const char *)((void *)0)));
  const sox_format_handler_t *handler;
  unsigned int i = 0;
  unsigned int s;
  sox_encoding_t e;
  ((path != 0) || (filetype != 0))?((void )0) : __assert_fail("path || filetype","formats.c",604,"enum sox_bool sox_format_supports_encoding(const char *, const char *, const struct sox_encodinginfo_t *)");
  (encoding != 0)?((void )0) : __assert_fail("encoding","formats.c",605,"enum sox_bool sox_format_supports_encoding(const char *, const char *, const struct sox_encodinginfo_t *)");
  if (!(filetype != 0)) 
    filetype = lsx_find_file_extension(path);
  if ((!(filetype != 0) || !((handler = sox_find_format(filetype,is_file_extension)) != 0)) || !((handler -> write_formats) != 0)) 
    return sox_false;
{
    while((e = ((sox_encoding_t )(handler -> write_formats)[i++])) != 0U){
      if (e == (encoding -> encoding)) {
        sox_bool has_bits;
        for (has_bits = sox_false; (s = ((unsigned int )(handler -> write_formats)[i++])) != 0U; has_bits = sox_true) 
          if (s == (encoding -> bits_per_sample)) 
            return sox_true;
        if (!(has_bits != 0U) && !((encoding -> bits_per_sample) != 0U)) 
          return sox_true;
        break; 
      }
      while(((unsigned int )(handler -> write_formats)[i++]) != 0U);
    }
  }
  return sox_false;
  #undef enc_arg
}

static void set_output_format(sox_format_t *ft)
{
  sox_encoding_t e = SOX_ENCODING_UNKNOWN;
  unsigned int i;
  unsigned int s;
  const unsigned int *encodings = ft -> handler.write_formats;
#define enc_arg(T) (T)encodings[i++]
  if (ft -> handler.write_rates != 0) {
    if (!(ft -> signal.rate != 0.00000)) 
      ft -> signal.rate = ft -> handler.write_rates[0];
    else {
      sox_rate_t r;
      i = 0;
{
        while((r = ft -> handler.write_rates[i++]) != 0.00000){
          if (r == ft -> signal.rate) 
            break; 
        }
      }
      if (r != ft -> signal.rate) {
        sox_rate_t given = ft -> signal.rate;
        sox_rate_t max = 0;
        ft -> signal.rate = __builtin_huge_val();
        i = 0;
        while((r = ft -> handler.write_rates[i++]) != 0.00000){
          if ((r > given) && (r < ft -> signal.rate)) 
            ft -> signal.rate = r;
          else 
            max = ((r >= max)?r : max);
        }
        if (ft -> signal.rate == __builtin_huge_val()) 
          ft -> signal.rate = max;
        ((( *sox_get_globals()).subsystem = "formats.c") , lsx_warn_impl("%s can\'t encode at %gHz; using %gHz",ft -> handler.names[0],given,ft -> signal.rate));
      }
    }
  }
  else if (!(ft -> signal.rate != 0.00000)) 
    ft -> signal.rate = 48000;
  if ((ft -> handler.flags & (256 | 0x0200 | 1024)) != 0U) {
    if ((ft -> signal.channels == 1) && !((ft -> handler.flags & 256) != 0U)) {
      ft -> signal.channels = ((((ft -> handler.flags & 0x0200) != 0U)?2 : 4));
      ((( *sox_get_globals()).subsystem = "formats.c") , lsx_warn_impl("%s can\'t encode mono; setting channels to %u",ft -> handler.names[0],ft -> signal.channels));
    }
    else if ((ft -> signal.channels == 2) && !((ft -> handler.flags & 0x0200) != 0U)) {
      ft -> signal.channels = ((((ft -> handler.flags & 1024) != 0U)?4 : 1));
      ((( *sox_get_globals()).subsystem = "formats.c") , lsx_warn_impl("%s can\'t encode stereo; setting channels to %u",ft -> handler.names[0],ft -> signal.channels));
    }
    else if ((ft -> signal.channels == 4) && !((ft -> handler.flags & 1024) != 0U)) {
      ft -> signal.channels = ((((ft -> handler.flags & 0x0200) != 0U)?2 : 1));
      ((( *sox_get_globals()).subsystem = "formats.c") , lsx_warn_impl("%s can\'t encode quad; setting channels to %u",ft -> handler.names[0],ft -> signal.channels));
    }
  }
  else 
    ft -> signal.channels = ((ft -> signal.channels >= 1)?ft -> signal.channels : 1);
  if (!(encodings != 0)) 
    return ;
/* If an encoding has been given, check if it supported by this handler */
  if (ft -> encoding.encoding != 0U) {
    i = 0;
{
      while((e = ((sox_encoding_t )encodings[i++])) != 0U){
        if (e == ft -> encoding.encoding) 
          break; 
        while(((unsigned int )encodings[i++]) != 0U);
      }
    }
    if (e != ft -> encoding.encoding) {
      ((( *sox_get_globals()).subsystem = "formats.c") , lsx_warn_impl("%s can\'t encode %s",ft -> handler.names[0],sox_get_encodings_info()[ft -> encoding.encoding].desc));
      ft -> encoding.encoding = 0;
    }
    else {
      unsigned int max_p = 0;
      unsigned int max_p_s = 0;
      unsigned int given_size = 0;
      sox_bool found = sox_false;
      if (ft -> encoding.bits_per_sample != 0U) 
        given_size = ft -> encoding.bits_per_sample;
      ft -> encoding.bits_per_sample = 65;
      while((s = ((unsigned int )encodings[i++])) != 0U){
        if (s == given_size) 
          found = sox_true;
        if (sox_precision(e,s) >= ft -> signal.precision) {
          if (s < ft -> encoding.bits_per_sample) 
            ft -> encoding.bits_per_sample = s;
        }
        else if (sox_precision(e,s) > max_p) {
          max_p = sox_precision(e,s);
          max_p_s = s;
        }
      }
      if (ft -> encoding.bits_per_sample == 65) 
        ft -> encoding.bits_per_sample = max_p_s;
      if (given_size != 0U) {
        if (found != 0U) 
          ft -> encoding.bits_per_sample = given_size;
        else 
          ((( *sox_get_globals()).subsystem = "formats.c") , lsx_warn_impl("%s can\'t encode %s to %u-bit",ft -> handler.names[0],sox_get_encodings_info()[ft -> encoding.encoding].desc,given_size));
      }
    }
  }
/* If a size has been given, check if it supported by this handler */
  if (!(ft -> encoding.encoding != 0U) && (ft -> encoding.bits_per_sample != 0U)) {
    i = 0;
    s = 0;
    while((s != ft -> encoding.bits_per_sample) && ((e = ((sox_encoding_t )encodings[i++])) != 0U))
      while(((s = ((unsigned int )encodings[i++])) != 0U) && (s != ft -> encoding.bits_per_sample));
    if (s != ft -> encoding.bits_per_sample) {
      ((( *sox_get_globals()).subsystem = "formats.c") , lsx_warn_impl("%s can\'t encode to %u-bit",ft -> handler.names[0],ft -> encoding.bits_per_sample));
      ft -> encoding.bits_per_sample = 0;
    }
    else 
      ft -> encoding.encoding = e;
  }
/* Find the smallest lossless encoding with precision >= signal.precision */
  if (!(ft -> encoding.encoding != 0U)) {
    ft -> encoding.bits_per_sample = 65;
    i = 0;
    while((e = ((sox_encoding_t )encodings[i++])) != 0U)
      while((s = ((unsigned int )encodings[i++])) != 0U)
        if ((!((sox_get_encodings_info()[e].flags & (sox_encodings_lossy1 | sox_encodings_lossy2)) != 0U) && (sox_precision(e,s) >= ft -> signal.precision)) && (s < ft -> encoding.bits_per_sample)) {
          ft -> encoding.encoding = e;
          ft -> encoding.bits_per_sample = s;
        }
  }
/* Find the smallest lossy encoding with precision >= signal precision,
   * or, if none such, the highest precision encoding */
  if (!(ft -> encoding.encoding != 0U)) {
    unsigned int max_p = 0;
    sox_encoding_t max_p_e = 0;
    unsigned int max_p_s = 0;
    i = 0;
    while((e = ((sox_encoding_t )encodings[i++])) != 0U)
      do {
        s = ((unsigned int )encodings[i++]);
        if (sox_precision(e,s) >= ft -> signal.precision) {
          if (s < ft -> encoding.bits_per_sample) {
            ft -> encoding.encoding = e;
            ft -> encoding.bits_per_sample = s;
          }
        }
        else if (sox_precision(e,s) > max_p) {
          max_p = sox_precision(e,s);
          max_p_e = e;
          max_p_s = s;
        }
      }while (s != 0U);
    if (!(ft -> encoding.encoding != 0U)) {
      ft -> encoding.encoding = max_p_e;
      ft -> encoding.bits_per_sample = max_p_s;
    }
  }
  ft -> signal.precision = sox_precision(ft -> encoding.encoding,ft -> encoding.bits_per_sample);
  #undef enc_arg
}

const sox_format_handler_t *sox_write_handler(const char *path,const char *filetype,const char **filetype1)
{
  const sox_format_handler_t *handler;
  if (filetype != 0) {
    if (!((handler = sox_find_format(filetype,sox_false)) != 0)) {
      if (filetype1 != 0) 
        ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("no handler for given file type `%s\'",filetype));
      return 0;
    }
  }
  else if (path != 0) {
    if (!((filetype = lsx_find_file_extension(path)) != 0)) {
      if (filetype1 != 0) 
        ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("can\'t determine type of `%s\'",path));
      return 0;
    }
    if (!((handler = sox_find_format(filetype,sox_true)) != 0)) {
      if (filetype1 != 0) 
        ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("no handler for file extension `%s\'",filetype));
      return 0;
    }
  }
  else 
    return 0;
  if (!((handler -> startwrite) != 0) && !((handler -> write) != 0)) {
    if (filetype1 != 0) 
      ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("file type `%s\' isn\'t writeable",filetype));
    return 0;
  }
  if (filetype1 != 0) 
     *filetype1 = filetype;
  return handler;
}

static sox_format_t *open_write(const char *path,void *buffer,size_t buffer_size,char **buffer_ptr,size_t *buffer_size_ptr,const sox_signalinfo_t *signal,const sox_encodinginfo_t *encoding,const char *filetype,const sox_oob_t *oob,sox_bool (*overwrite_permitted)(const char *))
{
  sox_format_t *ft = (1?memset(lsx_realloc(0,(sizeof(( *ft)) * 1)),0,(sizeof(( *ft)) * 1)) : ((void *)((void *)0)));
  const sox_format_handler_t *handler;
  if (!(path != 0) || !(signal != 0)) {
    ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("must specify file name and signal parameters to write file"));
    goto error;
  }
  if (!((handler = sox_write_handler(path,filetype,&filetype)) != 0)) 
    goto error;
  ft -> handler =  *handler;
  if (!((ft -> handler.flags & 1) != 0U)) {
/* Use stdout if the filename is "-" */
    if (!(strcmp(path,"-") != 0)) {
      if (( *sox_get_globals()).stdout_in_use_by != 0) {
        ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("`-\' (stdout) already in use by `%s\'",( *sox_get_globals()).stdout_in_use_by));
        goto error;
      }
      ( *sox_get_globals()).stdout_in_use_by = "audio output";;
      ft -> fp = stdout;
    }
    else {
      struct stat st;
      if ((!(stat(path,&st) != 0) && ((st.st_mode & 0170000) == 0100000)) && ((overwrite_permitted != 0) && !(( *overwrite_permitted)(path) != 0U))) {
        ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("permission to overwrite `%s\' denied",path));
        goto error;
      }
      ft -> fp = (((buffer != 0)?fmemopen(buffer,buffer_size,"w+b") : (((buffer_ptr != 0)?open_memstream(buffer_ptr,buffer_size_ptr) : fopen(path,"w+b")))));
#ifdef HAVE_FMEMOPEN
#endif
      if ((ft -> fp) == ((void *)((void *)0))) {
        ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("can\'t open output file `%s\': %s",path,strerror( *__errno_location())));
        goto error;
      }
    }
/* stdout tends to be line-buffered.  Override this */
/* to be Full Buffering. */
    if (setvbuf((ft -> fp),0,0,(sizeof(char ) * ( *sox_get_globals()).bufsiz)) != 0) {
      ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("Can\'t set write buffer"));
      goto error;
    }
    ft -> seekable = is_seekable(ft);
  }
  ft -> filetype = ((filetype != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(filetype) + 1)))),filetype) : ((char *)((void *)0)));
  ft -> filename = ((path != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(path) + 1)))),path) : ((char *)((void *)0)));
  ft -> mode = 'w';
  ft -> signal =  *signal;
  if (encoding != 0) 
    ft -> encoding =  *encoding;
  else 
    sox_init_encodinginfo(&ft -> encoding);
  set_endiannesses(ft);
  if (oob != 0) {
    ft -> oob =  *oob;
/* deep copy: */
    ft -> oob.comments = sox_copy_comments((oob -> comments));
  }
  set_output_format(ft);
/* FIXME: doesn't cover the situation where
   * codec changes audio length due to block alignment (e.g. 8svx, gsm): */
  if (((signal -> rate) != 0.00000) && ((signal -> channels) != 0U)) 
    ft -> signal.length = (((((ft -> signal.length * ft -> signal.rate) / (signal -> rate)) * ft -> signal.channels) / (signal -> channels)) + .5);
  if (((((ft -> handler.flags & 8) != 0U) && (strcmp((ft -> filetype),"sox") != 0)) && !(ft -> signal.length != 0UL)) && !((ft -> seekable) != 0U)) 
    ((( *sox_get_globals()).subsystem = "formats.c") , lsx_warn_impl("can\'t seek in output file `%s\'; length in file header will be unspecified",(ft -> filename)));
  ft -> priv = (((1 * ft -> handler.priv_size) != 0UL)?memset(lsx_realloc(0,(1 * ft -> handler.priv_size)),0,(1 * ft -> handler.priv_size)) : ((void *)((void *)0)));
/* Read and write starters can change their formats. */
  if ((ft -> handler.startwrite != 0) && (( *ft -> handler.startwrite)(ft) != SOX_SUCCESS)) {
    ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("can\'t open output file `%s\': %s",(ft -> filename),(ft -> sox_errstr)));
    goto error;
  }
  if (sox_checkformat(ft) != SOX_SUCCESS) {
    ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("bad format for output file `%s\': %s",(ft -> filename),(ft -> sox_errstr)));
    goto error;
  }
  if (((ft -> handler.flags & 2) != 0U) && (signal != 0)) {
    if (((signal -> rate) != 0.00000) && ((signal -> rate) != ft -> signal.rate)) 
      ((( *sox_get_globals()).subsystem = "formats.c") , lsx_report_impl("can\'t set sample rate %g; using %g",(signal -> rate),ft -> signal.rate));
    if (((signal -> channels) != 0U) && ((signal -> channels) != ft -> signal.channels)) 
      ((( *sox_get_globals()).subsystem = "formats.c") , lsx_report_impl("can\'t set %u channels; using %u",(signal -> channels),ft -> signal.channels));
  }
  return ft;
  error:
  if (((ft -> fp) != 0) && ((ft -> fp) != stdout)) 
    xfclose((ft -> fp),(ft -> io_type));
  free((ft -> priv));
  free((ft -> filename));
  free((ft -> filetype));
  free(ft);
  return 0;
}

sox_format_t *sox_open_write(const char *path,const sox_signalinfo_t *signal,const sox_encodinginfo_t *encoding,const char *filetype,const sox_oob_t *oob,sox_bool (*overwrite_permitted)(const char *))
{
  return open_write(path,0,((size_t )0),0,0,signal,encoding,filetype,oob,overwrite_permitted);
}

sox_format_t *sox_open_mem_write(void *buffer,size_t buffer_size,const sox_signalinfo_t *signal,const sox_encodinginfo_t *encoding,const char *filetype,const sox_oob_t *oob)
{
  return open_write("",buffer,buffer_size,0,0,signal,encoding,filetype,oob,0);
}

sox_format_t *sox_open_memstream_write(char **buffer_ptr,size_t *buffer_size_ptr,const sox_signalinfo_t *signal,const sox_encodinginfo_t *encoding,const char *filetype,const sox_oob_t *oob)
{
  return open_write("",0,((size_t )0),buffer_ptr,buffer_size_ptr,signal,encoding,filetype,oob,0);
}

size_t sox_read(sox_format_t *ft,sox_sample_t *buf,size_t len)
{
  size_t actual;
  if (ft -> signal.length != 0) 
    len = ((len <= (ft -> signal.length - (ft -> olength)))?len : (ft -> signal.length - (ft -> olength)));
  actual = ((ft -> handler.read != 0)?( *ft -> handler.read)(ft,buf,len) : 0);
  actual = ((actual > len)?0 : actual);
  ft -> olength += actual;
  return actual;
}

size_t sox_write(sox_format_t *ft,const sox_sample_t *buf,size_t len)
{
  size_t actual = (ft -> handler.write != 0)?( *ft -> handler.write)(ft,buf,len) : 0;
  ft -> olength += actual;
  return actual;
}

int sox_close(sox_format_t *ft)
{
  int result = SOX_SUCCESS;
  if ((ft -> mode) == 'r') 
    result = ((ft -> handler.stopread != 0)?( *ft -> handler.stopread)(ft) : SOX_SUCCESS);
  else {
    if ((ft -> handler.flags & 8) != 0U) {
      if (((ft -> olength) != ft -> signal.length) && ((ft -> seekable) != 0U)) {
        result = lsx_seeki(ft,((off_t )0),0);
        if (result == SOX_SUCCESS) 
          result = ((ft -> handler.stopwrite != 0)?( *ft -> handler.stopwrite)(ft) : (((ft -> handler.startwrite != 0)?( *ft -> handler.startwrite)(ft) : SOX_SUCCESS)));
      }
    }
    else 
      result = ((ft -> handler.stopwrite != 0)?( *ft -> handler.stopwrite)(ft) : SOX_SUCCESS);
  }
  if ((((ft -> fp) != 0) && ((ft -> fp) != stdin)) && ((ft -> fp) != stdout)) 
    xfclose((ft -> fp),(ft -> io_type));
  free((ft -> priv));
  free((ft -> filename));
  free((ft -> filetype));
  sox_delete_comments(&ft -> oob.comments);
  free(ft);
  return result;
}

int sox_seek(sox_format_t *ft,sox_uint64_t offset,int whence)
{
/* FIXME: Implement SOX_SEEK_CUR and SOX_SEEK_END. */
  if (whence != 0) 
/* FIXME: return SOX_EINVAL */
    return SOX_EOF;
/* If file is a seekable file and this handler supports seeking,
     * then invoke handler's function.
     */
  if (((ft -> seekable) != 0U) && (ft -> handler.seek != 0)) 
    return ( *ft -> handler.seek)(ft,offset);
/* FIXME: return SOX_EBADF */
  return SOX_EOF;
}

static int strcaseends(const char *str,const char *end)
{
  size_t str_len = strlen(str);
  size_t end_len = strlen(end);
  return (str_len >= end_len) && !(strcasecmp(((str + str_len) - end_len),end) != 0);
}
typedef enum __unnamed_enum___F0_L1035_C9_None__COMMA__M3u__COMMA__Pls {None,M3u,Pls}playlist_t;

static playlist_t playlist_type(const char *filename)
{
  char *x;
  char *p;
  playlist_t result = None;
  if (( *filename) == '|') 
    return result;
  if (strcaseends(filename,".m3u") != 0) 
    return M3u;
  if (strcaseends(filename,".pls") != 0) 
    return Pls;
  x = ((filename != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(filename) + 1)))),filename) : ((char *)((void *)0)));
  p = strrchr(x,'?');
  if (p != 0) {
     *p = 0;
    result = playlist_type(x);
  }
  free(x);
  return result;
}

sox_bool sox_is_playlist(const char *filename)
{
  return ((playlist_type(filename)) != None);
}

int sox_parse_playlist(sox_playlist_callback_t callback,void *p,const char *const listname)
{
  const sox_bool is_pls = ((playlist_type(listname)) == Pls);
  const int comment_char = "#;"[is_pls];
  size_t text_length = 100;
  char *text = (lsx_realloc(0,(text_length + 1)));
  char *dirname = (listname != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(listname) + 1)))),listname) : ((char *)((void *)0));
  char *slash_pos = strrchr(dirname,'/');
  lsx_io_type io_type;
  FILE *file = xfopen(listname,"r",&io_type);
  char *filename;
  int c;
  int result = SOX_SUCCESS;
  if (!(slash_pos != 0)) 
     *dirname = 0;
  else 
     *slash_pos = 0;
  if (file == ((FILE *)((void *)0))) {
    ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("Can\'t open playlist file `%s\': %s",listname,strerror( *__errno_location())));
    result = SOX_EOF;
  }
  else {{
      do {
        size_t i = 0;
        size_t begin = 0;
        size_t end = 0;
        while((( *__ctype_b_loc())[c = _IO_getc(file)] & ((unsigned short )_ISspace)) != 0);
        if (c == -1) 
          break; 
        while(((c != -1) && !(strchr("\r\n",c) != 0)) && (c != comment_char)){
          if (i == text_length) 
            text = (lsx_realloc(text,((text_length <<= 1) + 1)));
          text[i++] = c;
          if (!(strchr(" \t\f",c) != 0)) 
            end = i;
          c = _IO_getc(file);
        }
        if (ferror(file) != 0) 
          break; 
        if (c == comment_char) {
          do 
            c = _IO_getc(file);while ((c != -1) && !(strchr("\r\n",c) != 0));
          if (ferror(file) != 0) 
            break; 
        }
        text[end] = 0;
        if (is_pls != 0U) {
          char dummy;
          if (!(strncasecmp(text,"file",((size_t )4)) != 0) && (sscanf((text + 4),"%*u=%c",&dummy) == 1)) 
            begin = ((strchr((text + 5),'=') - text) + 1);
          else 
            end = 0;
        }
        if (begin != end) {
          const char *id = (text + begin);
          if ((!(dirname[0] != 0) || (is_url(id) != 0U)) || (id[0] == '/')) 
            filename = ((id != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(id) + 1)))),id) : ((char *)((void *)0)));
          else {
            filename = (lsx_realloc(0,((strlen(dirname) + strlen(id)) + 2)));
            sprintf(filename,"%s/%s",dirname,id);
          }
          if (sox_is_playlist(filename) != 0U) 
            sox_parse_playlist(callback,p,filename);
          else if (( *callback)(p,filename) != 0) 
            c = -1;
          free(filename);
        }
      }while (c != -1);
    }
    if (ferror(file) != 0) {
      ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("error reading playlist file `%s\': %s",listname,strerror( *__errno_location())));
      result = SOX_EOF;
    }
    if ((xfclose(file,io_type) != 0) && (io_type == lsx_io_url)) {
      ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("error reading playlist file URL `%s\'",listname));
      result = SOX_EOF;
    }
  }
  free(text);
  free(dirname);
  return result;
}
/*----------------------------- Formats library ------------------------------*/
enum __unnamed_enum___F0_L1149_C1_aifc__COMMA__aiff__COMMA__al__COMMA__au__COMMA__avr__COMMA__cdr__COMMA__cvsd__COMMA__cvu__COMMA__dat__COMMA__dvms__COMMA__f4__COMMA__f8__COMMA__gsrt__COMMA__hcom__COMMA__htk__COMMA__ima__COMMA__la__COMMA__lu__COMMA__maud__COMMA__nul__COMMA__prc__COMMA__raw__COMMA__s1__COMMA__s2__COMMA__s3__COMMA__s4__COMMA__sf__COMMA__sln__COMMA__smp__COMMA__sounder__COMMA__soundtool__COMMA__sox__COMMA__sphere__COMMA__svx__COMMA__txw__COMMA__u1__COMMA__u2__COMMA__u3__COMMA__u4__COMMA__ul__COMMA__voc__COMMA__vox__COMMA__wav__COMMA__wve__COMMA__xa__COMMA__alsa__COMMA__gsm__COMMA__lpc10__COMMA__mp3__COMMA__oss__COMMA__pulseaudio__COMMA__NSTATIC_FORMATS {
  #define FORMAT(f) f,
  #include "formats.h"
  #undef FORMAT
NSTATIC_FORMATS};
static sox_bool plugins_initted = sox_false;
#ifdef HAVE_LIBLTDL /* Plugin format handlers */
  #define MAX_DYNAMIC_FORMATS 42
  #define MAX_FORMATS (NSTATIC_FORMATS + MAX_DYNAMIC_FORMATS)
  #define MAX_FORMATS_1 (MAX_FORMATS + 1)
  #define MAX_NAME_LEN (size_t)1024 /* FIXME: Use vasprintf */
#else
  #define MAX_FORMATS_1
#endif
#define FORMAT(f) extern sox_format_handler_t const * lsx_##f##_format_fn(void);
#include "formats.h"
#undef FORMAT
static sox_format_tab_t s_sox_format_fns[NSTATIC_FORMATS + 42 + 1] = {
  #define FORMAT(f) {NULL, lsx_##f##_format_fn},
  #include "formats.h"
  #undef FORMAT
{((char *)((void *)0)), ((sox_format_fn_t )((void *)0))}};

const sox_format_tab_t *sox_get_format_fns()
{
  return s_sox_format_fns;
}
#ifdef HAVE_LIBLTDL /* Plugin format handlers */
static unsigned int nformats = NSTATIC_FORMATS;

static int init_format(const char *file,void *data)
{
  lt_dlhandle lth = lt_dlopenext(file);
  const char *end = (file + strlen(file));
  const char prefix[] = "sox_fmt_";
  char fnname[1024UL];
  char *start = strstr(file,prefix);
  data;
  if ((start != 0) && ((start += sizeof(prefix) - 1) < end)) {
    int ret = snprintf(fnname,((size_t )1024),"lsx_%.*s_format_fn",((int )(end - start)),start);
    if ((ret > 0) && (ret < ((int )((size_t )1024)))) {
      union __unnamed_class___F0_L1200_C9_L128R_variable_declaration__variable_type_L18R_variable_name_L128R__scope__fn__DELIMITER__L128R_variable_declaration__variable_type___Pb__v__Pe___variable_name_L128R__scope__ptr {
      sox_format_fn_t fn;
      void *ptr;}ltptr;
      ltptr.ptr = lt_dlsym(lth,fnname);
      ((( *sox_get_globals()).subsystem = "formats.c") , lsx_debug_impl("opening format plugin `%s\': library %p, entry point %p\n",fnname,((void *)lth),ltptr.ptr));
      if ((ltptr.fn != 0) && ((( *( *ltptr.fn)()).sox_lib_version_code & (~255)) == ((14 << 16) + (4 << 8) + 1 & ~255))) 
/* compatible version check */
{
        if (nformats == (NSTATIC_FORMATS + 42)) {
          ((( *sox_get_globals()).subsystem = "formats.c") , lsx_warn_impl("too many plugin formats"));
          return -1;
        }
        s_sox_format_fns[nformats++].fn = ltptr.fn;
      }
    }
  }
  return 0;
}
#endif
/* Find & load format handlers.  */

int sox_format_init()
{
  if (plugins_initted != 0U) 
    return SOX_EOF;
  plugins_initted = sox_true;
#ifdef HAVE_LIBLTDL
{
    int error = lt_dlinit();
    if (error != 0) {
      ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("lt_dlinit failed with %d error(s): %s",error,lt_dlerror()));
      return SOX_EOF;
    }
    lt_dlforeachfile("/usr/local/lib/sox",init_format,0);
  }
#endif
  return SOX_SUCCESS;
}
/* Cleanup things.  */

void sox_format_quit()
{
#ifdef HAVE_LIBLTDL
  int ret;
  if ((plugins_initted != 0U) && ((ret = lt_dlexit()) != 0)) 
    ((( *sox_get_globals()).subsystem = "formats.c") , lsx_fail_impl("lt_dlexit failed with %d error(s): %s",ret,lt_dlerror()));
  plugins_initted = sox_false;
  nformats = NSTATIC_FORMATS;
#endif
}
/* Find a named format in the formats library.
 *
 * (c) 2005-9 Chris Bagwell and SoX contributors.
 * Copyright 1991 Lance Norskog And Sundry Contributors.
 *
 * This source code is freely redistributable and may be used for any
 * purpose.  This copyright notice must be maintained.
 *
 * Lance Norskog, Sundry Contributors, Chris Bagwell and SoX contributors
 * are not responsible for the consequences of using this software.
 */

const sox_format_handler_t *sox_find_format(const char *name0,sox_bool no_dev)
{
  size_t f;
  size_t n;
  if (name0 != 0) {
    char *name = (name0 != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(name0) + 1)))),name0) : ((char *)((void *)0));
    char *pos = strchr(name,';');
/* Use only the 1st clause of a mime string */
    if (pos != 0) 
       *pos = 0;
    for (f = 0; s_sox_format_fns[f].fn != 0; ++f) {
      const sox_format_handler_t *handler = ( *s_sox_format_fns[f].fn)();
      if (!((no_dev != 0U) && (((handler -> flags) & 2) != 0U))) 
        for (n = 0; (handler -> names)[n] != 0; ++n) 
          if (!(strcasecmp((handler -> names)[n],name) != 0)) {
            free(name);
/* Found it. */
            return handler;
          }
    }
    free(name);
  }
/* Try again with plugins */
  if (sox_format_init() == SOX_SUCCESS) 
    return sox_find_format(name0,no_dev);
  return 0;
}
