/* libSoX Sounder format handler          (c) 2008 robs@users.sourceforge.net
 * See description in soundr3b.zip on the net.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"

static int start_read(sox_format_t *ft)
{
  uint16_t type;
  uint16_t rate;
  if (((lsx_readw(ft,&type) != 0) || (lsx_readw(ft,&rate) != 0)) || (lsx_skipbytes(ft,((size_t )4)) != 0)) 
    return SOX_EOF;
  if (type != 0) {
    lsx_fail_errno(ft,SOX_EHDR,"invalid Sounder header");
    return SOX_EOF;
  }
  return lsx_check_read_params(ft,1,((sox_rate_t )rate),SOX_ENCODING_UNSIGNED,8,((uint64_t )0),sox_true);
}

static int write_header(sox_format_t *ft)
{
/* sample type */
  return ((((lsx_writew(ft,0) != 0) || (lsx_writew(ft,((65535 <= ((unsigned int )(ft -> signal.rate + .5)))?65535 : ((unsigned int )(ft -> signal.rate + .5)))) != 0)) || (lsx_writew(ft,10) != 0)) || (lsx_writew(ft,4) != 0))?SOX_EOF : SOX_SUCCESS;
/* speaker driver volume */
/* speaker driver DC shift */
}
const sox_format_handler_t *lsx_sounder_format_fn();

const sox_format_handler_t *lsx_sounder_format_fn()
{
  static const char *const names[] = {("sndr"), ((const char *)((void *)0))};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_UNSIGNED), (8), (0), (0)};
  static const sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("8-bit linear audio as used by Aaron Wallace\'s `Sounder\' of 1991"), (names), ((0x0040 | 0 | 256)), (start_read), (lsx_rawread), ((sox_format_handler_stopread )((void *)0)), (write_header), (lsx_rawwrite), ((sox_format_handler_stopwrite )((void *)0)), (lsx_rawseek), (write_encodings), ((const sox_rate_t *)((void *)0)), (0)};
  return &handler;
}
