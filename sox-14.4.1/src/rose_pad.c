/* libSoX effect: Pad With Silence   (c) 2006 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
typedef struct __unnamed_class___F0_L20_C9_unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__npads__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__un_named_base_type__Pe___variable_name_unknown_scope_and_name__scope__pads__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__in_pos__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__pads_pos__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__pad_pos {
/* Number of pads requested */
unsigned int npads;
struct __unnamed_class___F0_L22_C3_L126R_variable_declaration__variable_type___Pb__c__Pe___variable_name_L126R__scope__str__DELIMITER__L126R_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_L126R__scope__start__DELIMITER__L126R_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_L126R__scope__pad {
/* Command-line argument to parse for this pad */
char *str;
/* Start padding when in_pos equals this */
uint64_t start;
/* Number of samples to pad */
uint64_t pad;}*pads;
/* Number of samples read from the input stream */
uint64_t in_pos;
/* Number of pads completed so far */
unsigned int pads_pos;
/* Number of samples through the current pad */
uint64_t pad_pos;}priv_t;

static int parse(sox_effect_t *effp,char **argv,sox_rate_t rate)
{
  priv_t *p = (priv_t *)(effp -> priv);
  const char *next;
  unsigned int i;
{
    for (i = 0; i < (p -> npads); ++i) {
/* 1st parse only */
      if (argv != 0) 
        (p -> pads)[i].str = ((argv[i] != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(argv[i]) + 1)))),argv[i]) : ((char *)((void *)0)));
      next = lsx_parsesamples(rate,(p -> pads)[i].str,&(p -> pads)[i].pad,'t');
      if (next == ((const char *)((void *)0))) 
        break; 
      if (( *next) == 0) 
        (p -> pads)[i].start = ((i != 0U)?18446744073709551615UL : 0);
      else {
        if (( *next) != '@') 
          break; 
        next = lsx_parsesamples(rate,(next + 1),&(p -> pads)[i].start,'t');
        if ((next == ((const char *)((void *)0))) || (( *next) != 0)) 
          break; 
      }
      if (!(argv != 0)) {
/* Do this check only during the second pass when the actual
         sample rate is known, otherwise it might fail on legal
         commands like
           pad 1@5 1@30000s
         if the rate is, e.g., 48k. */
        if ((i > 0) && ((p -> pads)[i].start <= (p -> pads)[i - 1].start)) 
          break; 
      }
    }
  }
  if (i < (p -> npads)) 
    return lsx_usage(effp);
  return SOX_SUCCESS;
}

static int create(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  (--argc , ++argv);
  p -> npads = argc;
  p -> pads = (((((p -> npads) * sizeof(( *(p -> pads)))) != 0ULL)?memset(lsx_realloc(0,((p -> npads) * sizeof(( *(p -> pads))))),0,((p -> npads) * sizeof(( *(p -> pads))))) : ((void *)((void *)0))));
/* No rate yet; parse with dummy */
  return parse(effp,argv,1e5);
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  unsigned int i;
/* Re-parse now rate is known */
  if (parse(effp,0,effp -> in_signal.rate) != SOX_SUCCESS) 
    return SOX_EOF;
  if ((effp -> out_signal.length = effp -> in_signal.length) != ((sox_uint64_t )(-1))) {
    for (i = 0; i < (p -> npads); ++i) 
      effp -> out_signal.length += ((p -> pads)[i].pad * effp -> in_signal.channels);
/* Check that the last pad position (except for "at the end")
       is within bounds. */
    i = (p -> npads);
    if ((i > 0) && ((p -> pads)[i - 1].start == 18446744073709551615UL)) 
      i--;
    if ((i > 0) && (((p -> pads)[i - 1].start * effp -> in_signal.channels) > effp -> in_signal.length)) {
      ((( *sox_get_globals()).subsystem = "pad.c") , lsx_fail_impl("pad position after end of audio"));
      return SOX_EOF;
    }
  }
  p -> in_pos = (p -> pad_pos = (p -> pads_pos = 0));
  for (i = 0; i < (p -> npads); ++i) 
    if ((p -> pads)[i].pad != 0UL) 
      return SOX_SUCCESS;
  return 32;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t c;
  size_t idone = 0;
  size_t odone = 0;
   *isamp /= effp -> in_signal.channels;
   *osamp /= effp -> in_signal.channels;
  do {
/* Copying: */
    for (; ((idone <  *isamp) && (odone <  *osamp)) && !(((p -> pads_pos) != (p -> npads)) && ((p -> in_pos) == (p -> pads)[p -> pads_pos].start)); (((++idone , ++odone)) , ++p -> in_pos)) 
      for (c = 0; c < effp -> in_signal.channels; ++c) 
         *(obuf++) =  *(ibuf++);
/* Padding: */
    if (((p -> pads_pos) != (p -> npads)) && ((p -> in_pos) == (p -> pads)[p -> pads_pos].start)) {
      for (; (odone <  *osamp) && ((p -> pad_pos) < (p -> pads)[p -> pads_pos].pad); (++odone , ++p -> pad_pos)) 
        for (c = 0; c < effp -> in_signal.channels; ++c) 
           *(obuf++) = 0;
/* Move to next pad? */
      if ((p -> pad_pos) == (p -> pads)[p -> pads_pos].pad) {
        ++p -> pads_pos;
        p -> pad_pos = 0;
      }
    }
  }while ((idone <  *isamp) && (odone <  *osamp));
   *isamp = (idone * effp -> in_signal.channels);
   *osamp = (odone * effp -> in_signal.channels);
  return SOX_SUCCESS;
}

static int drain(sox_effect_t *effp,sox_sample_t *obuf,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  static size_t isamp = 0;
  if (((p -> pads_pos) != (p -> npads)) && ((p -> in_pos) != (p -> pads)[p -> pads_pos].start)) 
/* Invoke the final pad (with no given start) */
    p -> in_pos = 18446744073709551615UL;
  return flow(effp,0,obuf,&isamp,osamp);
}

static int stop(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  if ((p -> pads_pos) != (p -> npads)) 
    ((( *sox_get_globals()).subsystem = "pad.c") , lsx_warn_impl("Input audio too short; pads not applied: %u",((p -> npads) - (p -> pads_pos))));
  return SOX_SUCCESS;
}

static int lsx_kill(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  unsigned int i;
  for (i = 0; i < (p -> npads); ++i) 
    free((p -> pads)[i].str);
  free((p -> pads));
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_pad_effect_fn()
{
  static sox_effect_handler_t handler = {("pad"), ("{length[@position]}"), ((16 | 8 | 256)), (create), (start), (flow), (drain), (stop), (lsx_kill), ((sizeof(priv_t )))};
  return (&handler);
}
