/* Common routines for G.721 and G.723 conversions.
 *
 * (c) SoX Contributors
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *
 * This code is based on code from Sun, which came with the following
 * copyright notice:
 * -----------------------------------------------------------------------
 * This source code is a product of Sun Microsystems, Inc. and is provided
 * for unrestricted use.  Users may copy or modify this source code without
 * charge.
 *
 * SUN SOURCE CODE IS PROVIDED AS IS WITH NO WARRANTIES OF ANY KIND INCLUDING
 * THE WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, OR ARISING FROM A COURSE OF DEALING, USAGE OR TRADE PRACTICE.
 *
 * Sun source code is provided with no support and without any obligation on
 * the part of Sun Microsystems, Inc. to assist in its use, correction,
 * modification or enhancement.
 *
 * SUN MICROSYSTEMS, INC. SHALL HAVE NO LIABILITY WITH RESPECT TO THE
 * INFRINGEMENT OF COPYRIGHTS, TRADE SECRETS OR ANY PATENTS BY THIS SOFTWARE
 * OR ANY PART THEREOF.
 *
 * In no event will Sun Microsystems, Inc. be liable for any lost revenue
 * or profits or other special, indirect and consequential damages, even if
 * Sun has been advised of the possibility of such damages.
 *
 * Sun Microsystems, Inc.
 * 2550 Garcia Avenue
 * Mountain View, California  94043
 * -----------------------------------------------------------------------
 */
#include "sox_i.h"
#include "g711.h"
#include "g72x.h"
static const char LogTable256[] = {(0), (0), (1), (1), (2), (2), (2), (2), (3), (3), (3), (3), (3), (3), (3), (3), (4), (4), (4), (4), (4), (4), (4), (4), (4), (4), (4), (4), (4), (4), (4), (4), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (5), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (6), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7), (7)};

inline static int log2plus1(int val)
{
/* From http://graphics.stanford.edu/~seander/bithacks.html#IntegerLogLookup */
/* 32-bit word to find the log of */
  unsigned int v = (unsigned int )val;
/* r will be lg(v) */
  unsigned int r;
/* temporaries */
  register unsigned int t;
  register unsigned int tt;
  if ((tt = (v >> 16)) != 0U) {
    r = ((((t = (tt >> 8)) != 0U)?(24 + LogTable256[t]) : (16 + LogTable256[tt])));
  }
  else {
    r = ((((t = (v >> 8)) != 0U)?(8 + LogTable256[t]) : LogTable256[v]));
  }
  return (r + 1);
}
/*
 * quan()
 *
 * quantizes the input val against the table of size short integers.
 * It returns i if table[i - 1] <= val < table[i].
 *
 * Using linear search for simple coding.
 */

static int quan(int val,const short *table,int size)
{
  int i;
{
    for (i = 0; i < size; i++) 
      if (val < ( *(table++))) 
        break; 
  }
  return i;
}
/*
 * fmult()
 *
 * returns the integer product of the 14-bit integer "an" and
 * "floating point" representation (4-bit exponent, 6-bit mantessa) "srn".
 */

static int fmult(int an,int srn)
{
  short anmag;
  short anexp;
  short anmant;
  short wanexp;
  short wanmant;
  short retval;
  anmag = (((an > 0)?an : (-an & 0x1FFF)));
  anexp = (log2plus1(anmag) - 6);
  anmant = (((anmag == 0)?32 : (((anexp >= 0)?(anmag >> anexp) : (anmag << -anexp)))));
  wanexp = ((anexp + ((srn >> 6) & 15)) - 13);
  wanmant = (((anmant * (srn & 077)) + 48) >> 4);
  retval = (((wanexp >= 0)?((wanmant << wanexp) & 0x7FFF) : (wanmant >> -wanexp)));
  return ((an ^ srn) < 0)?-retval : retval;
}
/*
 * g72x_init_state()
 *
 * This routine initializes and/or resets the g72x_state structure
 * pointed to by 'state_ptr'.
 * All the initial state values are specified in the CCITT G.721 document.
 */

void lsx_g72x_init_state(struct g72x_state *state_ptr)
{
  int cnta;
  state_ptr -> yl = 34816;
  state_ptr -> yu = 544;
  state_ptr -> dms = 0;
  state_ptr -> dml = 0;
  state_ptr -> ap = 0;
  for (cnta = 0; cnta < 2; cnta++) {
    (state_ptr -> a)[cnta] = 0;
    (state_ptr -> pk)[cnta] = 0;
    (state_ptr -> sr)[cnta] = 32;
  }
  for (cnta = 0; cnta < 6; cnta++) {
    (state_ptr -> b)[cnta] = 0;
    (state_ptr -> dq)[cnta] = 32;
  }
  state_ptr -> td = 0;
}
/*
 * predictor_zero()
 *
 * computes the estimated signal from 6-zero predictor.
 *
 */

int lsx_g72x_predictor_zero(struct g72x_state *state_ptr)
{
  int i;
  int sezi;
  sezi = fmult(((state_ptr -> b)[0] >> 2),(state_ptr -> dq)[0]);
/* ACCUM */
  for (i = 1; i < 6; i++) 
    sezi += fmult(((state_ptr -> b)[i] >> 2),(state_ptr -> dq)[i]);
  return sezi;
}
/*
 * predictor_pole()
 *
 * computes the estimated signal from 2-pole predictor.
 *
 */

int lsx_g72x_predictor_pole(struct g72x_state *state_ptr)
{
  return fmult(((state_ptr -> a)[1] >> 2),(state_ptr -> sr)[1]) + fmult(((state_ptr -> a)[0] >> 2),(state_ptr -> sr)[0]);
}
/*
 * step_size()
 *
 * computes the quantization step size of the adaptive quantizer.
 *
 */

int lsx_g72x_step_size(struct g72x_state *state_ptr)
{
  int y;
  int dif;
  int al;
  if ((state_ptr -> ap) >= 256) 
    return (state_ptr -> yu);
  else {
    y = ((state_ptr -> yl) >> 6);
    dif = ((state_ptr -> yu) - y);
    al = ((state_ptr -> ap) >> 2);
    if (dif > 0) 
      y += ((dif * al) >> 6);
    else if (dif < 0) 
      y += (((dif * al) + 077) >> 6);
    return y;
  }
}
/*
 * quantize()
 *
 * Given a raw sample, 'd', of the difference signal and a
 * quantization step size scale factor, 'y', this routine returns the
 * ADPCM codeword to which that sample gets quantized.  The step
 * size scale factor division operation is done in the log base 2 domain
 * as a subtraction.
 */

int lsx_g72x_quantize(int d,int y,const short *table,int size)
{
/* Magnitude of 'd' */
  short dqm;
/* Integer part of base 2 log of 'd' */
  short exp;
/* Fractional part of base 2 log */
  short mant;
/* Log of magnitude of 'd' */
  short dl;
/* Step size scale factor normalized log */
  short dln;
  int i;
/*
         * LOG
         *
         * Compute base 2 log of 'd', and store in 'dl'.
         */
  dqm = (abs(d));
  exp = (log2plus1((dqm >> 1)));
/* Fractional portion. */
  mant = (((dqm << 7) >> exp) & 0x7F);
  dl = ((exp << 7) + mant);
/*
         * SUBTB
         *
         * "Divide" by step size multiplier.
         */
  dln = (dl - (y >> 2));
/*
         * QUAN
         *
         * Obtain codword i for 'd'.
         */
  i = quan(dln,table,size);
/* take 1's complement of i */
  if (d < 0) 
    return ((size << 1) + 1) - i;
  else 
/* take 1's complement of 0 */
if (i == 0) 
/* new in 1988 */
    return (size << 1) + 1;
  else 
    return i;
}
/*
 * reconstruct()
 *
 * Returns reconstructed difference signal 'dq' obtained from
 * codeword 'i' and quantization step size scale factor 'y'.
 * Multiplication is performed in log base 2 domain as addition.
 */

int lsx_g72x_reconstruct(int sign,int dqln,int y)
{
/* Log of 'dq' magnitude */
  short dql;
/* Integer part of log */
  short dex;
  short dqt;
/* Reconstructed difference signal sample */
  short dq;
/* ADDA */
  dql = (dqln + (y >> 2));
  if (dql < 0) {
    return (sign != 0)?-0x8000 : 0;
/* ANTILOG */
  }
  else {
    dex = ((dql >> 7) & 15);
    dqt = (128 + (dql & 0x7F));
    dq = ((dqt << 7) >> (14 - dex));
    return (sign != 0)?(dq - 0x8000) : dq;
  }
}
/*
 * update()
 *
 * updates the state variables for each output code
 */

void lsx_g72x_update(int code_size,int y,int wi,int fi,int dq,int sr,int dqsez,struct g72x_state *state_ptr)
{
  int cnt;
/* Adaptive predictor, FLOAT A */
  short mag;
  short exp;
/* LIMC */
  short a2p = 0;
/* UPA1 */
  short a1ul;
/* UPA2 */
  short pks1;
  short fa1;
/* tone/transition detector */
  char tr;
  short ylint;
  short thr2;
  short dqthr;
  short ylfrac;
  short thr1;
  short pk0;
/* needed in updating predictor poles */
  pk0 = (((dqsez < 0)?1 : 0));
/* prediction difference magnitude */
  mag = (dq & 0x7FFF);
/* TRANS */
/* exponent part of yl */
  ylint = ((state_ptr -> yl) >> 15);
/* fractional part of yl */
  ylfrac = (((state_ptr -> yl) >> 10) & 0x1F);
/* threshold */
  thr1 = ((32 + ylfrac) << ylint);
/* limit thr2 to 31 << 10 */
  thr2 = (((ylint > 9)?0x1F << 10 : thr1));
/* dqthr = 0.75 * thr2 */
  dqthr = ((thr2 + (thr2 >> 1)) >> 1);
/* signal supposed voice */
  if ((state_ptr -> td) == 0) 
    tr = 0;
  else 
/* supposed data, but small mag */
if (mag <= dqthr) 
/* treated as voice */
    tr = 0;
  else 
/* signal is data (modem) */
    tr = 1;
/*
         * Quantizer scale factor adaptation.
         */
/* FUNCTW & FILTD & DELAY */
/* update non-steady state step size multiplier */
  state_ptr -> yu = (y + ((wi - y) >> 5));
/* LIMB */
/* 544 <= yu <= 5120 */
  if ((state_ptr -> yu) < 544) 
    state_ptr -> yu = 544;
  else if ((state_ptr -> yu) > 5120) 
    state_ptr -> yu = 5120;
/* FILTE & DELAY */
/* update steady state step size multiplier */
  state_ptr -> yl += ((state_ptr -> yu) + (-(state_ptr -> yl) >> 6));
/*
         * Adaptive predictor coefficients.
         */
/* reset a's and b's for modem signal */
  if (tr == 1) {
    (state_ptr -> a)[0] = 0;
    (state_ptr -> a)[1] = 0;
    (state_ptr -> b)[0] = 0;
    (state_ptr -> b)[1] = 0;
    (state_ptr -> b)[2] = 0;
    (state_ptr -> b)[3] = 0;
    (state_ptr -> b)[4] = 0;
    (state_ptr -> b)[5] = 0;
/* update a's and b's */
  }
  else {
/* UPA2 */
    pks1 = (pk0 ^ (state_ptr -> pk)[0]);
/* update predictor pole a[1] */
    a2p = ((state_ptr -> a)[1] - ((state_ptr -> a)[1] >> 7));
    if (dqsez != 0) {
      fa1 = (((pks1 != 0)?(state_ptr -> a)[0] : -(state_ptr -> a)[0]));
/* a2p = function of fa1 */
      if (fa1 < -0x1FFF) 
        a2p -= 256;
      else if (fa1 > 0x1FFF) 
        a2p += 0xFF;
      else 
        a2p += (fa1 >> 5);
      if ((pk0 ^ (state_ptr -> pk)[1]) != 0) {
/* LIMC */
        if (a2p <= -12160) 
          a2p = (-12288);
        else if (a2p >= 12416) 
          a2p = 12288;
        else 
          a2p -= 128;
      }
      else if (a2p <= -12416) 
        a2p = (-12288);
      else if (a2p >= 12160) 
        a2p = 12288;
      else 
        a2p += 128;
    }
/* Possible bug: a2p not initialized if dqsez == 0) */
/* TRIGB & DELAY */
    (state_ptr -> a)[1] = a2p;
/* UPA1 */
/* update predictor pole a[0] */
    (state_ptr -> a)[0] -= ((state_ptr -> a)[0] >> 8);
    if (dqsez != 0) {
      if (pks1 == 0) 
        (state_ptr -> a)[0] += 192;
      else 
        (state_ptr -> a)[0] -= 192;
    }
/* LIMD */
    a1ul = (15360 - a2p);
    if ((state_ptr -> a)[0] < -a1ul) 
      (state_ptr -> a)[0] = (-a1ul);
    else if ((state_ptr -> a)[0] > a1ul) 
      (state_ptr -> a)[0] = a1ul;
/* UPB : update predictor zeros b[6] */
    for (cnt = 0; cnt < 6; cnt++) {
/* for 40Kbps G.723 */
      if (code_size == 5) 
        (state_ptr -> b)[cnt] -= ((state_ptr -> b)[cnt] >> 9);
      else 
/* for G.721 and 24Kbps G.723 */
        (state_ptr -> b)[cnt] -= ((state_ptr -> b)[cnt] >> 8);
/* XOR */
      if ((dq & 0x7FFF) != 0) {
        if ((dq ^ (state_ptr -> dq)[cnt]) >= 0) 
          (state_ptr -> b)[cnt] += 128;
        else 
          (state_ptr -> b)[cnt] -= 128;
      }
    }
  }
  for (cnt = 5; cnt > 0; cnt--) 
    (state_ptr -> dq)[cnt] = (state_ptr -> dq)[cnt - 1];
/* FLOAT A : convert dq[0] to 4-bit exp, 6-bit mantissa f.p. */
  if (mag == 0) {
    (state_ptr -> dq)[0] = (((dq >= 0)?32 : ((short )((unsigned short )0xFC20))));
  }
  else {
    exp = (log2plus1(mag));
    (state_ptr -> dq)[0] = (((dq >= 0)?((exp << 6) + ((mag << 6) >> exp)) : (((exp << 6) + ((mag << 6) >> exp)) - 1024)));
  }
  (state_ptr -> sr)[1] = (state_ptr -> sr)[0];
/* FLOAT B : convert sr to 4-bit exp., 6-bit mantissa f.p. */
  if (sr == 0) {
    (state_ptr -> sr)[0] = 32;
  }
  else if (sr > 0) {
    exp = (log2plus1(sr));
    (state_ptr -> sr)[0] = ((exp << 6) + ((sr << 6) >> exp));
  }
  else if (sr > -0x8000) {
    mag = (-sr);
    exp = (log2plus1(mag));
    (state_ptr -> sr)[0] = (((exp << 6) + ((mag << 6) >> exp)) - 1024);
  }
  else 
    (state_ptr -> sr)[0] = ((short )((unsigned short )0xFC20));
/* DELAY A */
  (state_ptr -> pk)[1] = (state_ptr -> pk)[0];
  (state_ptr -> pk)[0] = pk0;
/* TONE */
/* this sample has been treated as data */
  if (tr == 1) 
/* next one will be treated as voice */
    state_ptr -> td = 0;
  else 
/* small sample-to-sample correlation */
if (a2p < -11776) 
/* signal may be data */
    state_ptr -> td = 1;
  else 
/* signal is voice */
    state_ptr -> td = 0;
/*
         * Adaptation speed control.
         */
/* FILTA */
  state_ptr -> dms += ((fi - (state_ptr -> dms)) >> 5);
/* FILTB */
  state_ptr -> dml += (((fi << 2) - (state_ptr -> dml)) >> 7);
  if (tr == 1) 
    state_ptr -> ap = 256;
  else 
/* SUBTC */
if (y < 1536) 
    state_ptr -> ap += ((0x200 - (state_ptr -> ap)) >> 4);
  else if ((state_ptr -> td) == 1) 
    state_ptr -> ap += ((0x200 - (state_ptr -> ap)) >> 4);
  else if (abs((((state_ptr -> dms) << 2) - (state_ptr -> dml))) >= ((state_ptr -> dml) >> 3)) 
    state_ptr -> ap += ((0x200 - (state_ptr -> ap)) >> 4);
  else 
    state_ptr -> ap += (-(state_ptr -> ap) >> 4);
}
/*
 * tandem_adjust(sr, se, y, i, sign)
 *
 * At the end of ADPCM decoding, it simulates an encoder which may be receiving
 * the output of this decoder as a tandem process. If the output of the
 * simulated encoder differs from the input to this decoder, the decoder output
 * is adjusted by one level of A-law or u-law codes.
 *
 * Input:
 *      sr      decoder output linear PCM sample,
 *      se      predictor estimate sample,
 *      y       quantizer step size,
 *      i       decoder input code,
 *      sign    sign bit of code i
 *
 * Return:
 *      adjusted A-law or u-law compressed sample.
 */

int lsx_g72x_tandem_adjust_alaw(int sr,int se,int y,int i,int sign,const short *qtab)
{
/* A-law compressed 8-bit code */
  unsigned char sp;
/* prediction error */
  short dx;
/* quantized prediction error */
  char id;
/* adjusted A-law decoded sample value */
  int sd;
/* biased magnitude of i */
  int im;
/* biased magnitude of id */
  int imx;
  if (sr <= -0x8000) 
    sr = -1;
/* short to A-law compression */
  sp = lsx_13linear2alaw[((sr >> 1) << 3) + 0x1000];
/* 16-bit prediction error */
  dx = ((lsx_alaw2linear16[sp] >> 2) - se);
  id = (lsx_g72x_quantize(dx,y,qtab,(sign - 1)));
/* no adjustment on sp */
  if (id == i) {
    return sp;
/* sp adjustment needed */
  }
  else {
/* ADPCM codes : 8, 9, ... F, 0, 1, ... , 6, 7 */
/* 2's complement to biased unsigned */
    im = (i ^ sign);
    imx = (id ^ sign);
/* sp adjusted to next lower value */
    if (imx > im) {
      if ((sp & 128) != 0) {
        sd = ((sp == 0xD5)?0x55 : (((sp ^ 0x55) - 1) ^ 0x55));
      }
      else {
        sd = ((sp == 0x2A)?0x2A : (((sp ^ 0x55) + 1) ^ 0x55));
      }
/* sp adjusted to next higher value */
    }
    else {
      if ((sp & 128) != 0) 
        sd = ((sp == 0xAA)?0xAA : (((sp ^ 0x55) + 1) ^ 0x55));
      else 
        sd = ((sp == 0x55)?0xD5 : (((sp ^ 0x55) - 1) ^ 0x55));
    }
    return sd;
  }
}

int lsx_g72x_tandem_adjust_ulaw(int sr,int se,int y,int i,int sign,const short *qtab)
{
/* u-law compressed 8-bit code */
  unsigned char sp;
/* prediction error */
  short dx;
/* quantized prediction error */
  char id;
/* adjusted u-law decoded sample value */
  int sd;
/* biased magnitude of i */
  int im;
/* biased magnitude of id */
  int imx;
  if (sr <= -0x8000) 
    sr = 0;
/* short to u-law compression */
  sp = lsx_14linear2ulaw[(sr << 2) + 0x2000];
/* 16-bit prediction error */
  dx = ((lsx_ulaw2linear16[sp] >> 2) - se);
  id = (lsx_g72x_quantize(dx,y,qtab,(sign - 1)));
  if (id == i) {
    return sp;
  }
  else {
/* ADPCM codes : 8, 9, ... F, 0, 1, ... , 6, 7 */
/* 2's complement to biased unsigned */
    im = (i ^ sign);
    imx = (id ^ sign);
/* sp adjusted to next lower value */
    if (imx > im) {
      if ((sp & 128) != 0) 
        sd = ((sp == 0xFF)?0x7E : (sp + 1));
      else 
        sd = ((sp == 0)?0 : (sp - 1));
/* sp adjusted to next higher value */
    }
    else {
      if ((sp & 128) != 0) 
        sd = ((sp == 128)?128 : (sp - 1));
      else 
        sd = ((sp == 0x7F)?0xFE : (sp + 1));
    }
    return sd;
  }
}
