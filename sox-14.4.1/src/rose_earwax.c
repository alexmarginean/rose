/* libSoX earwax - makes listening to headphones easier     November 9, 2000
 *
 * Copyright (c) 2000 Edward Beingessner And Sundry Contributors.
 * This source code is freely redistributable and may be used for any purpose.
 * This copyright notice must be maintained.  Edward Beingessner And Sundry
 * Contributors are not responsible for the consequences of using this
 * software.
 *
 * This effect takes a 44.1kHz stereo (CD format) signal that is meant to be
 * listened to on headphones, and adds audio cues to move the soundstage from
 * inside your head (standard for headphones) to outside and in front of the
 * listener (standard for speakers). This makes the sound much easier to listen
 * to on headphones.
 */
#include "sox_i.h"
#include <string.h>
static const sox_sample_t filt[32 * 2] = {(4), ((-6)), (4), ((-11)), ((-1)), ((-5)), (3), (3), ((-2)), (5), ((-5)), (0), (9), (1), (6), (3), ((-4)), ((-1)), ((-5)), ((-3)), ((-2)), ((-5)), ((-7)), (1), (6), ((-7)), (30), ((-29)), (12), ((-3)), ((-11)), (4), ((-3)), (7), ((-20)), (23), (2), (0), (1), ((-6)), ((-14)), ((-5)), (15), ((-18)), (6), (7), (15), ((-10)), ((-14)), (22), ((-7)), ((-2)), ((-4)), (9), (6), ((-12)), (6), ((-6)), (0), ((-11)), (0), ((-5)), (4), (0)
/* 30°  330° */
/* 32 tap stereo FIR filter. */
/* One side filters as if the */
/* signal was from 30 degrees */
/* from the ear, the other as */
/* if 330 degrees. */
/*                         Input                         */
/*                   Left         Right                  */
/*                __________   __________                */
/*               |          | |          |               */
/*           .---|  Hh,0(f) | |  Hh,0(f) |---.           */
/*          /    |__________| |__________|    \          */
/*         /                \ /                \         */
/*        /                  X                  \        */
/*       /                  / \                  \       */
/*  ____V_____   __________V   V__________   _____V____  */
/* |          | |          |   |          | |          | */
/* | Hh,30(f) | | Hh,330(f)|   | Hh,330(f)| | Hh,30(f) | */
/* |__________| |__________|   |__________| |__________| */
/*      \     ___      /           \      ___     /      */
/*       \   /   \    /    _____    \    /   \   /       */
/*        `->| + |<--'    /     \    `-->| + |<-'        */
/*           \___/      _/       \_      \___/           */
/*               \     / \       / \     /               */
/*                `--->| |       | |<---'                */
/*                     \_/       \_/                     */
/*                                                       */
/*                       Headphones                      */
};
#define NUMTAPS array_length(filt)
/* FIR filter z^-1 delays */
typedef struct __unnamed_class___F0_L55_C9_unknown_scope_and_name_variable_declaration__variable_type__Ab_L5R_index_64_Ae__variable_name_unknown_scope_and_name__scope__tap {
sox_sample_t tap[sizeof(filt) / sizeof(filt[0])];}priv_t;

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  if ((effp -> in_signal.rate != 44100) || (effp -> in_signal.channels != 2)) {
    ((( *sox_get_globals()).subsystem = "earwax.c") , lsx_fail_impl("works only with stereo audio sampled at 44100Hz (i.e. CDDA)"));
    return SOX_EOF;
  }
/* zero tap memory */
  memset((p -> tap),0,(sizeof(filt) / sizeof(filt[0]) * sizeof(( *p -> tap))));
  if (effp -> in_signal.mult != 0) 
     *effp -> in_signal.mult *= exp(-4.4 * 2.30258509299404568402 * 0.05);
  return SOX_SUCCESS;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t i;
  size_t len = ( *isamp = ( *osamp = (( *isamp <=  *osamp)? *isamp :  *osamp)));
/* update taps and calculate output */
  while(len-- != 0UL){
    double output = 0;
    for (i = (sizeof(filt) / sizeof(filt[0]) - 1); i != 0UL; --i) {
      (p -> tap)[i] = (p -> tap)[i - 1];
      output += ((p -> tap)[i] * filt[i]);
    }
/* scale output */
    (p -> tap)[0] = ( *(ibuf++) / 64);
    output += ((p -> tap)[0] * filt[0]);
     *(obuf++) = (((output < 0)?(((output <= ((sox_sample_t )(1 << 32 - 1)) - 0.5)?((++effp -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (output - 0.5))) : (((output >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 0.5)?((++effp -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : (output + 0.5)))));
  }
  return SOX_SUCCESS;
}
/* No drain: preserve audio file length; it's only 32 samples anyway. */

const sox_effect_handler_t *lsx_earwax_effect_fn()
{
  static sox_effect_handler_t handler = {("earwax"), ((const char *)((void *)0)), (16), ((sox_effect_handler_getopts )((void *)0)), (start), (flow), ((sox_effect_handler_drain )((void *)0)), ((sox_effect_handler_stop )((void *)0)), ((sox_effect_handler_kill )((void *)0)), ((sizeof(priv_t )))};
  return (&handler);
}
