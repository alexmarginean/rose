/* Simple example of using SoX libraries
 *
 * Copyright (c) 2007-8 robs@users.sourceforge.net
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#ifdef NDEBUG /* N.B. assert used with active statements so enable always. */
#undef NDEBUG /* Must undef above assert.h or other that might include it. */
#endif
#include "sox.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
/* input and output files */
static sox_format_t *in;
static sox_format_t *out;
/* The function that will be called to input samples into the effects chain.
 * In this example, we get samples to process from a SoX-openned audio file.
 * In a different application, they might be generated or come from a different
 * part of the application. */

static int input_drain(sox_effect_t *effp,sox_sample_t *obuf,size_t *osamp)
{
/* This parameter is not needed in this example */
  effp;
/* ensure that *osamp is a multiple of the number of channels. */
   *osamp -= ( *osamp % effp -> out_signal.channels);
/* Read up to *osamp samples into obuf; store the actual number read
   * back to *osamp */
   *osamp = sox_read(in,obuf, *osamp);
/* sox_read may return a number that is less than was requested; only if
   * 0 samples is returned does it indicate that end-of-file has been reached
   * or an error has occurred */
  if (!( *osamp != 0UL) && ((in -> sox_errno) != 0)) 
    fprintf(stderr,"%s: %s\n",(in -> filename),(in -> sox_errstr));
  return ( *osamp != 0UL)?SOX_SUCCESS : SOX_EOF;
}
/* The function that will be called to output samples from the effects chain.
 * In this example, we store the samples in a SoX-opened audio file.
 * In a different application, they might perhaps be analysed in some way,
 * or displayed as a wave-form */

static int output_flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
/* Write out *isamp samples */
  size_t len = sox_write(out,ibuf, *isamp);
/* len is the number of samples that were actually written out; if this is
   * different to *isamp, then something has gone wrong--most often, it's
   * out of disc space */
  if (len !=  *isamp) {
    fprintf(stderr,"%s: %s\n",(out -> filename),(out -> sox_errstr));
    return SOX_EOF;
  }
/* Outputting is the last `effect' in the effect chain so always passes
   * 0 samples on to the next effect (as there isn't one!) */
   *osamp = 0;
/* This parameter is not needed in this example */
  effp;
/* All samples output successfully */
  return SOX_SUCCESS;
}
/* A `stub' effect handler to handle inputting samples to the effects
 * chain; the only function needed for this example is `drain' */

static const sox_effect_handler_t *input_handler()
{
  static sox_effect_handler_t handler = {("input"), ((const char *)((void *)0)), (16), ((sox_effect_handler_getopts )((void *)0)), ((sox_effect_handler_start )((void *)0)), ((sox_effect_handler_flow )((void *)0)), (input_drain), ((sox_effect_handler_stop )((void *)0)), ((sox_effect_handler_kill )((void *)0)), (0)};
  return (&handler);
}
/* A `stub' effect handler to handle outputting samples from the effects
 * chain; the only function needed for this example is `flow' */

static const sox_effect_handler_t *output_handler()
{
  static sox_effect_handler_t handler = {("output"), ((const char *)((void *)0)), (16), ((sox_effect_handler_getopts )((void *)0)), ((sox_effect_handler_start )((void *)0)), (output_flow), ((sox_effect_handler_drain )((void *)0)), ((sox_effect_handler_stop )((void *)0)), ((sox_effect_handler_kill )((void *)0)), (0)};
  return (&handler);
}
/*
 * Reads input file, applies vol & flanger effects, stores in output file.
 * E.g. example1 monkey.au monkey.aiff
 */

int main(int argc,char *argv[])
{
  sox_effects_chain_t *chain;
  sox_effect_t *e;
  char *vol[] = {("3dB")};
  (argc == 3)?((void )0) : __assert_fail("argc == 3","example1.c",112,"int main(int, char **)");
/* All libSoX applications must start by initialising the SoX library */
  (sox_init() == SOX_SUCCESS)?((void )0) : __assert_fail("sox_init() == SOX_SUCCESS","example1.c",115,"int main(int, char **)");
/* Open the input file (with default parameters) */
  ((in = sox_open_read(argv[1],0,0,0)) != 0)?((void )0) : __assert_fail("in = sox_open_read(argv[1], ((void *)0), ((void *)0), ((void *)0))","example1.c",118,"int main(int, char **)");
/* Open the output file; we must specify the output signal characteristics.
   * Since we are using only simple effects, they are the same as the input
   * file characteristics */
  ((out = sox_open_write(argv[2],(&in -> signal),0,0,0,0)) != 0)?((void )0) : __assert_fail("out = sox_open_write(argv[2], &in->signal, ((void *)0), ((void *)0), ((void *)0), ((void *)0))","example1.c",123,"int main(int, char **)");
/* Create an effects chain; some effects need to know about the input
   * or output file encoding so we provide that information here */
  chain = sox_create_effects_chain((&in -> encoding),(&out -> encoding));
/* The first effect in the effect chain must be something that can source
   * samples; in this case, we have defined an input handler that inputs
   * data from an audio file */
  e = sox_create_effect(input_handler());
/* This becomes the first `effect' in the chain */
  (sox_add_effect(chain,e,&in -> signal,(&in -> signal)) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_add_effect(chain, e, &in->signal, &in->signal) == SOX_SUCCESS","example1.c",134,"int main(int, char **)");
  free(e);
/* Create the `vol' effect, and initialise it with the desired parameters: */
  e = sox_create_effect(sox_find_effect("vol"));
  (sox_effect_options(e,1,vol) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_effect_options(e, 1, vol) == SOX_SUCCESS","example1.c",139,"int main(int, char **)");
/* Add the effect to the end of the effects processing chain: */
  (sox_add_effect(chain,e,&in -> signal,(&in -> signal)) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_add_effect(chain, e, &in->signal, &in->signal) == SOX_SUCCESS","example1.c",141,"int main(int, char **)");
  free(e);
/* Create the `flanger' effect, and initialise it with default parameters: */
  e = sox_create_effect(sox_find_effect("flanger"));
  (sox_effect_options(e,0,0) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_effect_options(e, 0, ((void *)0)) == SOX_SUCCESS","example1.c",146,"int main(int, char **)");
/* Add the effect to the end of the effects processing chain: */
  (sox_add_effect(chain,e,&in -> signal,(&in -> signal)) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_add_effect(chain, e, &in->signal, &in->signal) == SOX_SUCCESS","example1.c",148,"int main(int, char **)");
  free(e);
/* The last effect in the effect chain must be something that only consumes
   * samples; in this case, we have defined an output handler that outputs
   * data to an audio file */
  e = sox_create_effect(output_handler());
  (sox_add_effect(chain,e,&in -> signal,(&in -> signal)) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_add_effect(chain, e, &in->signal, &in->signal) == SOX_SUCCESS","example1.c",155,"int main(int, char **)");
  free(e);
/* Flow samples through the effects processing chain until EOF is reached */
  sox_flow_effects(chain,0,0);
/* All done; tidy up: */
  sox_delete_effects_chain(chain);
  sox_close(out);
  sox_close(in);
  sox_quit();
  return 0;
}
