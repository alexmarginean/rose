/* Copyright 1991, 1992, 1993 Guido van Rossum And Sundry Contributors.
 * This source code is freely redistributable and may be used for
 * any purpose.  This copyright notice must be maintained.
 * Guido van Rossum And Sundry Contributors are not responsible for
 * the consequences of using this software.
 */
/*
 * GSM 06.10 courtesy Communications and Operating Systems Research Group,
 * Technische Universitaet Berlin
 *
 * More information on this format can be obtained from
 * http://www.cs.tu-berlin.de/~jutta/toasox.html
 *
 * Source is available from ftp://ftp.cs.tu-berlin.de/pub/local/kbs/tubmik/gsm
 *
 * Written 26 Jan 1995 by Andrew Pam
 * Portions Copyright (c) 1995 Serious Cybernetics
 *
 * July 19, 1998 - Chris Bagwell (cbagwell@sprynet.com)
 *   Added GSM support to SOX from patches floating around with the help
 *   of Dima Barsky (ess2db@ee.surrey.ac.uk).
 *
 * Nov. 26, 1999 - Stan Brooks (stabro@megsinet.com)
 *   Rewritten to support multiple channels
 */
#include "sox_i.h"
#ifdef EXTERNAL_GSM
#ifdef HAVE_GSM_GSM_H
#include <gsm/gsm.h>
#else
#include <gsm.h>
#endif
#else
#include "../libgsm/gsm.h"
#endif
#include <errno.h>
#define MAXCHANS 16
/* sizeof(gsm_frame) */
#define FRAMESIZE (size_t)33
/* samples per gsm_frame */
#define BLOCKSIZE 160
/* Private data */
typedef struct __unnamed_class___F0_L52_C9_unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__channels__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__gsm_signals__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__samples__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__gsm_signals__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__samplePtr__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__gsm_signals__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__sampleTop__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__gsm_byteUc__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__frames__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_L120R_index_16_Ae__variable_name_unknown_scope_and_name__scope__handle {
unsigned int channels;
gsm_signal *samples;
gsm_signal *samplePtr;
gsm_signal *sampleTop;
gsm_byte *frames;
gsm handle[16UL];}priv_t;

static int gsmstart_rw(sox_format_t *ft,int w)
{
  priv_t *p = (priv_t *)(ft -> priv);
  unsigned int ch;
  ft -> encoding.encoding = SOX_ENCODING_GSM;
  if (!(ft -> signal.rate != 0.00000)) 
    ft -> signal.rate = 8000;
  if (ft -> signal.channels == 0) 
    ft -> signal.channels = 1;
  p -> channels = ft -> signal.channels;
  if (((p -> channels) > 16) || ((p -> channels) <= 0)) {
    lsx_fail_errno(ft,SOX_EFMT,"gsm: channels(%d) must be in 1-16",ft -> signal.channels);
    return SOX_EOF;
  }
  for (ch = 0; ch < (p -> channels); ch++) {
    (p -> handle)[ch] = lsx_gsm_create();
    if (!((p -> handle)[ch] != 0)) {
      lsx_fail_errno(ft, *__errno_location(),"unable to create GSM stream");
      return SOX_EOF;
    }
  }
  p -> frames = (lsx_realloc(0,((p -> channels) * ((size_t )33))));
  p -> samples = (lsx_realloc(0,((160 * ((p -> channels) + 1)) * sizeof(gsm_signal ))));
  p -> sampleTop = ((p -> samples) + (160 * (p -> channels)));
  p -> samplePtr = ((w != 0)?(p -> samples) : (p -> sampleTop));
  return SOX_SUCCESS;
}

static int sox_gsmstartread(sox_format_t *ft)
{
  return gsmstart_rw(ft,0);
}

static int sox_gsmstartwrite(sox_format_t *ft)
{
  return gsmstart_rw(ft,1);
}
/*
 * Read up to len samples from file.
 * Convert to signed longs.
 * Place in buf[].
 * Return number of samples read.
 */

static size_t sox_gsmread(sox_format_t *ft,sox_sample_t *buf,size_t samp)
{
  size_t done = 0;
  size_t r;
  int ch;
  int chans;
  gsm_signal *gbuff;
  priv_t *p = (priv_t *)(ft -> priv);
  chans = (p -> channels);
{
    while(done < samp){
      while(((p -> samplePtr) < (p -> sampleTop)) && (done < samp))
        buf[done++] = (((sox_sample_t )( *(p -> samplePtr++))) << 32 - 16);
      if (done >= samp) 
        break; 
      r = lsx_readbuf(ft,(p -> frames),((p -> channels) * ((size_t )33)));
      if (r != ((p -> channels) * ((size_t )33))) 
        break; 
      p -> samplePtr = (p -> samples);
      for (ch = 0; ch < chans; ch++) {
        int i;
        gsm_signal *gsp;
        gbuff = (p -> sampleTop);
        if (lsx_gsm_decode((p -> handle)[ch],((p -> frames) + (ch * ((size_t )33))),gbuff) < 0) {
          lsx_fail_errno(ft, *__errno_location(),"error during GSM decode");
          return 0;
        }
        gsp = ((p -> samples) + ch);
        for (i = 0; i < 160; i++) {
           *gsp =  *(gbuff++);
          gsp += chans;
        }
      }
    }
  }
  return done;
}

static int gsmflush(sox_format_t *ft)
{
  int r;
  int ch;
  int chans;
  gsm_signal *gbuff;
  priv_t *p = (priv_t *)(ft -> priv);
  chans = (p -> channels);
/* zero-fill samples as needed */
  while((p -> samplePtr) < (p -> sampleTop))
     *(p -> samplePtr++) = 0;
  gbuff = (p -> sampleTop);
  for (ch = 0; ch < chans; ch++) {
    int i;
    gsm_signal *gsp;
    gsp = ((p -> samples) + ch);
    for (i = 0; i < 160; i++) {
      gbuff[i] =  *gsp;
      gsp += chans;
    }
    lsx_gsm_encode((p -> handle)[ch],gbuff,(p -> frames));
    r = (lsx_writebuf(ft,(p -> frames),((size_t )33)));
    if (r != ((size_t )33)) {
      lsx_fail_errno(ft, *__errno_location(),"write error");
      return SOX_EOF;
    }
  }
  p -> samplePtr = (p -> samples);
  return SOX_SUCCESS;
}

static size_t sox_gsmwrite(sox_format_t *ft,const sox_sample_t *buf,size_t samp)
{
  size_t done = 0;
  priv_t *p = (priv_t *)(ft -> priv);
  while(done < samp){
    sox_sample_t sox_macro_temp_sample;
    double sox_macro_temp_double;
    while(((p -> samplePtr) < (p -> sampleTop)) && (done < samp))
       *(p -> samplePtr++) = ((sox_int16_t )((((sox_macro_temp_double , (sox_macro_temp_sample = buf[done++]))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - (1 << 31 - 16))?((++ft -> clips , ((unsigned int )(-1)) >> 33 - 16)) : (((sox_uint32_t )(sox_macro_temp_sample + (1 << 31 - 16))) >> 32 - 16)))));
    if ((p -> samplePtr) == (p -> sampleTop)) {
      if (gsmflush(ft) != 0) {
        return 0;
      }
    }
  }
  return done;
}

static int sox_gsmstopread(sox_format_t *ft)
{
  priv_t *p = (priv_t *)(ft -> priv);
  unsigned int ch;
  for (ch = 0; ch < (p -> channels); ch++) 
    lsx_gsm_destroy((p -> handle)[ch]);
  free((p -> samples));
  free((p -> frames));
  return SOX_SUCCESS;
}

static int sox_gsmstopwrite(sox_format_t *ft)
{
  int rc;
  priv_t *p = (priv_t *)(ft -> priv);
  if ((p -> samplePtr) > (p -> samples)) {
    rc = gsmflush(ft);
    if (rc != 0) 
      return rc;
  }
/* destroy handles and free buffers */
  return sox_gsmstopread(ft);
}
const sox_format_handler_t *lsx_gsm_format_fn();

const sox_format_handler_t *lsx_gsm_format_fn()
{
  static const char *const names[] = {("gsm"), ((const char *)((void *)0))};
  static const sox_rate_t write_rates[] = {(8000), (0)};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_GSM), (0), (0)};
  static sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("GSM 06.10 (full-rate) lossy speech compression"), (names), (0), (sox_gsmstartread), (sox_gsmread), (sox_gsmstopread), (sox_gsmstartwrite), (sox_gsmwrite), (sox_gsmstopwrite), ((sox_format_handler_seek )((void *)0)), (write_encodings), (write_rates), ((sizeof(priv_t )))};
  return (&handler);
}
