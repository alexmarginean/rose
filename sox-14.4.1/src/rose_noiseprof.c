/* noiseprof - SoX Noise Profiling Effect.
 *
 * Written by Ian Turner (vectro@vectro.org)
 * Copyright 1999 Ian Turner and others
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "noisered.h"
#include <assert.h>
#include <string.h>
#include <errno.h>
typedef struct __unnamed_class___F0_L27_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__f__Pe___variable_name_unknown_scope_and_name__scope__sum__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__i__Pe___variable_name_unknown_scope_and_name__scope__profilecount__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__f__Pe___variable_name_unknown_scope_and_name__scope__window {
float *sum;
int *profilecount;
float *window;}chandata_t;
typedef struct __unnamed_class___F0_L34_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__output_filename__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__FILE_IO_FILE__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__output_file__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__chandata_tL126R__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__chandata__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__bufdata {
char *output_filename;
FILE *output_file;
chandata_t *chandata;
size_t bufdata;}priv_t;
/*
 * Get the filename, if any. We don't open it until sox_noiseprof_start.
 */

static int sox_noiseprof_getopts(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *data = (priv_t *)(effp -> priv);
  (--argc , ++argv);
  if (argc == 1) {
    data -> output_filename = argv[0];
  }
  else if (argc > 1) 
    return lsx_usage(effp);
  return SOX_SUCCESS;
}
/*
 * Prepare processing.
 * Do all initializations.
 */

static int sox_noiseprof_start(sox_effect_t *effp)
{
  priv_t *data = (priv_t *)(effp -> priv);
  unsigned int channels = effp -> in_signal.channels;
  unsigned int i;
/* Note: don't fall back to stderr if stdout is unavailable
   * since we already use stderr for diagnostics. */
  if (!((data -> output_filename) != 0) || !(strcmp((data -> output_filename),"-") != 0)) {
    if (( *( *(effp -> global_info)).global_info).stdout_in_use_by != 0) {
      ((( *sox_get_globals()).subsystem = "noiseprof.c") , lsx_fail_impl("stdout already in use by `%s\'",( *( *(effp -> global_info)).global_info).stdout_in_use_by));
      return SOX_EOF;
    }
    ( *( *(effp -> global_info)).global_info).stdout_in_use_by = effp -> handler.name;
    data -> output_file = stdout;
  }
  else if ((data -> output_file = fopen((data -> output_filename),"w")) == ((FILE *)((void *)0))) {
    ((( *sox_get_globals()).subsystem = "noiseprof.c") , lsx_fail_impl("Couldn\'t open profile file %s: %s",(data -> output_filename),strerror( *__errno_location())));
    return SOX_EOF;
  }
  data -> chandata = ((((channels * sizeof(( *(data -> chandata)))) != 0ULL)?memset(lsx_realloc(0,(channels * sizeof(( *(data -> chandata))))),0,(channels * sizeof(( *(data -> chandata))))) : ((void *)((void *)0))));
  data -> bufdata = 0;
  for (i = 0; i < channels; i++) {
    (data -> chandata)[i].sum = ((1?memset(lsx_realloc(0,((2048 / 2 + 1) * sizeof(float ))),0,((2048 / 2 + 1) * sizeof(float ))) : ((void *)((void *)0))));
    (data -> chandata)[i].profilecount = ((1?memset(lsx_realloc(0,((2048 / 2 + 1) * sizeof(int ))),0,((2048 / 2 + 1) * sizeof(int ))) : ((void *)((void *)0))));
    (data -> chandata)[i].window = ((1?memset(lsx_realloc(0,(2048 * sizeof(float ))),0,(2048 * sizeof(float ))) : ((void *)((void *)0))));
  }
  return SOX_SUCCESS;
}
/* Collect statistics from the complete window on channel chan. */

static void collect_data(chandata_t *chan)
{
  float *out = (1?memset(lsx_realloc(0,((2048 / 2 + 1) * sizeof(float ))),0,((2048 / 2 + 1) * sizeof(float ))) : ((void *)((void *)0)));
  int i;
  lsx_power_spectrum_f(2048,(chan -> window),out);
  for (i = 0; i < 2048 / 2 + 1; i++) {
    if (out[i] > 0) {
      float value = (log(out[i]));
      (chan -> sum)[i] += value;
      (chan -> profilecount)[i]++;
    }
  }
  free(out);
}
/*
 * Grab what we can from ibuf, and process if we have a whole window.
 */

static int sox_noiseprof_flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
/* No need to clip count */
  size_t samp = ( *isamp <=  *osamp)? *isamp :  *osamp;
  size_t dummy = 0;
  size_t chans = effp -> in_signal.channels;
  size_t i;
  size_t j;
  size_t n = ((samp / chans) <= (2048 - (p -> bufdata)))?(samp / chans) : (2048 - (p -> bufdata));
/* Pass on audio unaffected */
  memcpy(obuf,ibuf,((n * chans) * sizeof(( *obuf))));
   *isamp = ( *osamp = (n * chans));
/* Collect data for every channel. */
  for (i = 0; i < chans; i++) {
    sox_sample_t sox_macro_temp_sample;
    double sox_macro_temp_double;
    chandata_t *chan = ((p -> chandata) + i);
    for (j = 0; j < n; j++) 
      (chan -> window)[j + (p -> bufdata)] = ((((sox_macro_temp_double , (sox_macro_temp_sample = ibuf[i + (j * chans)]))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - 128)?((++dummy , 1)) : (((sox_macro_temp_sample + 128) & ~255) * (1. / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.))))));
    if ((n + (p -> bufdata)) == 2048) 
      collect_data(chan);
  }
  p -> bufdata += n;
  ((p -> bufdata) <= 2048)?((void )0) : __assert_fail("p->bufdata <= 2048","noiseprof.c",138,"int sox_noiseprof_flow(struct sox_effect_t *, const int *, int *, unsigned long *, unsigned long *)");
  if ((p -> bufdata) == 2048) 
    p -> bufdata = 0;
  return SOX_SUCCESS;
}
/*
 * Finish off the last window.
 */

static int sox_noiseprof_drain(sox_effect_t *effp,sox_sample_t *obuf,size_t *osamp)
{
  priv_t *data = (priv_t *)(effp -> priv);
  int tracks = effp -> in_signal.channels;
  int i;
   *osamp = 0;
  if ((data -> bufdata) == 0) {
    return SOX_EOF;
  }
  for (i = 0; i < tracks; i++) {
    int j;
    for (j = ((data -> bufdata) + 1); j < 2048; j++) {
      (data -> chandata)[i].window[j] = 0;
    }
    collect_data(((data -> chandata) + i));
  }
  if (((data -> bufdata) == 2048) || ((data -> bufdata) == 0)) 
    return SOX_EOF;
  else 
    return SOX_SUCCESS;
}
/*
 * Print profile and clean up.
 */

static int sox_noiseprof_stop(sox_effect_t *effp)
{
  priv_t *data = (priv_t *)(effp -> priv);
  size_t i;
  for (i = 0; i < effp -> in_signal.channels; i++) {
    int j;
    chandata_t *chan = ((data -> chandata) + i);
    fprintf((data -> output_file),"Channel %lu: ",((unsigned long )i));
    for (j = 0; j < 2048 / 2 + 1; j++) {
      double r = (((chan -> profilecount)[j] != 0)?((chan -> sum)[j] / (chan -> profilecount)[j]) : 0);
      fprintf((data -> output_file),"%s%f",((j == 0)?"" : ", "),r);
    }
    fprintf((data -> output_file),"\n");
    free((chan -> sum));
    free((chan -> profilecount));
  }
  free((data -> chandata));
  if ((data -> output_file) != stdout) 
    fclose((data -> output_file));
  return SOX_SUCCESS;
}
static sox_effect_handler_t sox_noiseprof_effect = {("noiseprof"), ("[profile-file]"), ((16 | 256)), (sox_noiseprof_getopts), (sox_noiseprof_start), (sox_noiseprof_flow), (sox_noiseprof_drain), (sox_noiseprof_stop), ((sox_effect_handler_kill )((void *)0)), ((sizeof(priv_t )))};

const sox_effect_handler_t *lsx_noiseprof_effect_fn()
{
  return (&sox_noiseprof_effect);
}
