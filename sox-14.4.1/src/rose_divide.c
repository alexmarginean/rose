/* libSoX effect: divide   Copyright (c) 2009 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* This is W.I.P. hence marked SOX_EFF_ALPHA for now.
 * Needs better handling of when the divisor approaches or is zero; some
 * sort of interpolation of the output values perhaps.
 */
#include "sox_i.h"
#include <string.h>
typedef struct __unnamed_class___F0_L26_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L5R__Pe___variable_name_unknown_scope_and_name__scope__last {
sox_sample_t *last;}priv_t;

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  p -> last = ((((effp -> in_signal.channels * sizeof(( *(p -> last)))) != 0ULL)?memset(lsx_realloc(0,(effp -> in_signal.channels * sizeof(( *(p -> last))))),0,(effp -> in_signal.channels * sizeof(( *(p -> last))))) : ((void *)((void *)0))));
  return SOX_SUCCESS;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t i;
  size_t len = (((( *isamp <=  *osamp)? *isamp :  *osamp)) / effp -> in_signal.channels);
   *osamp = ( *isamp = (len * effp -> in_signal.channels));
  while(len-- != 0UL){
    double divisor = ( *(obuf++) =  *(ibuf++));
    if (divisor != 0.00000) {
      double out;
      double mult = (1. / (divisor * (1. / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.))));
      for (i = 1; i < effp -> in_signal.channels; ++i) {
        out = (( *(ibuf++)) * mult);
        (p -> last)[i] = ( *(obuf++) = (((out < 0)?(((out <= ((sox_sample_t )(1 << 32 - 1)) - 0.5)?((++effp -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (out - 0.5))) : (((out >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 0.5)?((++effp -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : (out + 0.5))))));
      }
    }
    else 
      for (i = 1; i < effp -> in_signal.channels; (++i , ++ibuf)) 
         *(obuf++) = (p -> last)[i];
  }
  return SOX_SUCCESS;
}

static int stop(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  free((p -> last));
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_divide_effect_fn()
{
  static sox_effect_handler_t handler = {("divide"), ((const char *)((void *)0)), ((16 | 128 | 512)), ((sox_effect_handler_getopts )((void *)0)), (start), (flow), ((sox_effect_handler_drain )((void *)0)), (stop), ((sox_effect_handler_kill )((void *)0)), ((sizeof(priv_t )))};
  return (&handler);
}
