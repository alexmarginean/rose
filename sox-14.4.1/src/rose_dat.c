/* libSoX text format file.  Tom Littlejohn, March 93.
 *
 * Reads/writes sound files as text.
 *
 * Copyright 1998-2006 Chris Bagwell and SoX Contributors
 * This source code is freely redistributable and may be used for
 * any purpose.  This copyright notice must be maintained.
 * Lance Norskog And Sundry Contributors are not responsible for
 * the consequences of using this software.
 */
#include "sox_i.h"
#include <string.h>
#define LINEWIDTH (size_t)256
/* Private data for dat file */
typedef struct __unnamed_class___F0_L18_C9_unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__timevalue__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__deltat__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__buffered__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_c_index_256_Ae__variable_name_unknown_scope_and_name__scope__prevline {
double timevalue;
double deltat;
int buffered;
char prevline[256UL];}priv_t;

static int sox_datstartread(sox_format_t *ft)
{
  char inpstr[256UL];
  long rate;
  int chan;
  int status;
  char sc;
{
/* Read lines until EOF or first non-comment line */
    while((status = lsx_reads(ft,inpstr,(((size_t )256) - 1))) != SOX_EOF){
      inpstr[((size_t )256) - 1] = 0;
      if ((sscanf(inpstr," %c",&sc) != 0) && (sc != ';')) 
        break; 
      if (sscanf(inpstr," ; Sample Rate %ld",&rate) != 0) {
        ft -> signal.rate = rate;
      }
      else if (sscanf(inpstr," ; Channels %d",&chan) != 0) {
        ft -> signal.channels = chan;
      }
    }
  }
/* Hold a copy of the last line we read (first non-comment) */
  if (status != SOX_EOF) {
    strncpy(( *((priv_t *)(ft -> priv))).prevline,inpstr,((size_t )((size_t )256)));
    ( *((priv_t *)(ft -> priv))).buffered = 1;
  }
  else {
    ( *((priv_t *)(ft -> priv))).buffered = 0;
  }
/* Default channels to 1 if not found */
  if (ft -> signal.channels == 0) 
    ft -> signal.channels = 1;
  ft -> encoding.encoding = SOX_ENCODING_FLOAT_TEXT;
  return SOX_SUCCESS;
}

static int sox_datstartwrite(sox_format_t *ft)
{
  priv_t *dat = (priv_t *)(ft -> priv);
  char s[256UL];
  dat -> timevalue = 0.0;
  dat -> deltat = (1.0 / ((double )ft -> signal.rate));
/* Write format comments to start of file */
  sprintf(s,"; Sample Rate %ld\r\n",((long )ft -> signal.rate));
  lsx_writes(ft,s);
  sprintf(s,"; Channels %d\r\n",((int )ft -> signal.channels));
  lsx_writes(ft,s);
  return SOX_SUCCESS;
}

static size_t sox_datread(sox_format_t *ft,sox_sample_t *buf,size_t nsamp)
{
  char inpstr[256UL];
  int inpPtr = 0;
  int inpPtrInc = 0;
  double sampval = 0.0;
  int retc = 0;
  char sc = 0;
  size_t done = 0;
  size_t i = 0;
/* Always read a complete set of channels */
  nsamp -= (nsamp % ft -> signal.channels);
  while(done < nsamp){
/* Read a line or grab the buffered first line */
    if (( *((priv_t *)(ft -> priv))).buffered != 0) {
      strncpy(inpstr,( *((priv_t *)(ft -> priv))).prevline,((size_t )((size_t )256)));
      inpstr[((size_t )256) - 1] = 0;
      ( *((priv_t *)(ft -> priv))).buffered = 0;
    }
    else {
      lsx_reads(ft,inpstr,(((size_t )256) - 1));
      inpstr[((size_t )256) - 1] = 0;
      if (lsx_eof(ft) != 0) 
        return done;
    }
/* Skip over comments - ie. 0 or more whitespace, then ';' */
    if ((sscanf(inpstr," %c",&sc) != 0) && (sc == ';')) 
      continue; 
/* Read a complete set of channels */
    sscanf(inpstr," %*s%n",&inpPtr);
    for (i = 0; i < ft -> signal.channels; i++) {
      sox_sample_t sox_macro_temp_sample;
      double sox_macro_temp_double;
      retc = sscanf((inpstr + inpPtr)," %lg%n",&sampval,&inpPtrInc);
      inpPtr += inpPtrInc;
      if (retc != 1) {
        lsx_fail_errno(ft,SOX_EOF,"Unable to read sample.");
        return 0;
      }
       *(buf++) = ((sox_sample_t )((((sox_macro_temp_sample , (sox_macro_temp_double = (sampval * (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.0))))) , ((sox_macro_temp_double < 0)?(((sox_macro_temp_double <= ((sox_sample_t )(1 << 32 - 1)) - .5)?((++ft -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (sox_macro_temp_double - .5))) : (((sox_macro_temp_double >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + .5)?(((sox_macro_temp_double > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.0)?((++ft -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : (sox_macro_temp_double + .5)))))));
      done++;
    }
  }
  return done;
}

static size_t sox_datwrite(sox_format_t *ft,const sox_sample_t *buf,size_t nsamp)
{
  priv_t *dat = (priv_t *)(ft -> priv);
  size_t done = 0;
  double sampval = 0.0;
  char s[256UL];
  size_t i = 0;
/* Always write a complete set of channels */
  nsamp -= (nsamp % ft -> signal.channels);
/* Write time, then sample values, then CRLF newline */
  while(done < nsamp){
    sprintf(s," %15.8g ",(dat -> timevalue));
    lsx_writes(ft,s);
    for (i = 0; i < ft -> signal.channels; i++) {
      sampval = (( *(buf++)) * (1.0 / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.0)));
      sprintf(s," %15.8g",sampval);
      lsx_writes(ft,s);
      done++;
    }
    sprintf(s," \r\n");
    lsx_writes(ft,s);
    dat -> timevalue += (dat -> deltat);
  }
  return done;
}
const sox_format_handler_t *lsx_dat_format_fn();

const sox_format_handler_t *lsx_dat_format_fn()
{
  static const char *const names[] = {("dat"), ((const char *)((void *)0))};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_FLOAT_TEXT), (0), (0)};
  static const sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("Textual representation of the sampled audio"), (names), (0), (sox_datstartread), (sox_datread), ((sox_format_handler_stopread )((void *)0)), (sox_datstartwrite), (sox_datwrite), ((sox_format_handler_stopwrite )((void *)0)), ((sox_format_handler_seek )((void *)0)), (write_encodings), ((const sox_rate_t *)((void *)0)), ((sizeof(priv_t )))};
  return &handler;
}
