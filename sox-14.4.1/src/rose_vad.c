/* libSoX effect: Voice Activity Detector  (c) 2009 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include <string.h>
typedef struct __unnamed_class___F0_L21_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__d__Pe___variable_name_unknown_scope_and_name__scope__dftBuf__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__d__Pe___variable_name_unknown_scope_and_name__scope__noiseSpectrum__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__d__Pe___variable_name_unknown_scope_and_name__scope__spectrum__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__d__Pe___variable_name_unknown_scope_and_name__scope__measures__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__meanMeas {
double *dftBuf;
double *noiseSpectrum;
double *spectrum;
double *measures;
double meanMeas;}chan_t;
/* Configuration parameters: */
typedef struct __unnamed_class___F0_L25_C9_unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__bootTime__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__noiseTcUp__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__noiseTcDown__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__noiseReductionAmount__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__measureFreq__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__measureDuration__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__measureTc__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__preTriggerTime__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__hpFilterFreq__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__lpFilterFreq__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__hpLifterFreq__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__lpLifterFreq__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__triggerTc__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__triggerLevel__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__searchTime__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__gapTime__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L5R__Pe___variable_name_unknown_scope_and_name__scope__samples__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__dftLen_ws__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__samplesLen_ns__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__samplesIndex_ns__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__flushedLen_ns__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__gapLen__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__measurePeriod_ns__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__measuresLen__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__measuresIndex__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__measureTimer_ns__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__measureLen_ws__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__measureLen_ns__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__spectrumStart__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__spectrumEnd__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__cepstrumStart__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__cepstrumEnd__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__bootCountMax__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__bootCount__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__noiseTcUpMult__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__noiseTcDownMult__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__measureTcMult__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__triggerMeasTcMult__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__d__Pe___variable_name_unknown_scope_and_name__scope__spectrumWindow__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__d__Pe___variable_name_unknown_scope_and_name__scope__cepstrumWindow__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__chan_tL126R__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__channels {
double bootTime;
double noiseTcUp;
double noiseTcDown;
double noiseReductionAmount;
double measureFreq;
double measureDuration;
double measureTc;
double preTriggerTime;
double hpFilterFreq;
double lpFilterFreq;
double hpLifterFreq;
double lpLifterFreq;
double triggerTc;
double triggerLevel;
double searchTime;
double gapTime;
/* Working variables: */
sox_sample_t *samples;
unsigned int dftLen_ws;
unsigned int samplesLen_ns;
unsigned int samplesIndex_ns;
unsigned int flushedLen_ns;
unsigned int gapLen;
unsigned int measurePeriod_ns;
unsigned int measuresLen;
unsigned int measuresIndex;
unsigned int measureTimer_ns;
unsigned int measureLen_ws;
unsigned int measureLen_ns;
/* bins */
unsigned int spectrumStart;
unsigned int spectrumEnd;
unsigned int cepstrumStart;
unsigned int cepstrumEnd;
int bootCountMax;
int bootCount;
double noiseTcUpMult;
double noiseTcDownMult;
double measureTcMult;
double triggerMeasTcMult;
double *spectrumWindow;
double *cepstrumWindow;
chan_t *channels;}priv_t;
#define GETOPT_FREQ(optstate, c, name, min) \
    case c: p->name = lsx_parse_frequency(optstate.arg, &parseIndex); \
      if (p->name < min || *parseIndex) return lsx_usage(effp); \
      break;

static int create(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  #define opt_str "+b:N:n:r:f:m:M:h:l:H:L:T:t:s:g:p:"
  int c;
  lsx_getopt_t optstate;
  lsx_getopt_init(argc,argv,"+b:N:n:r:f:m:M:h:l:H:L:T:t:s:g:p:",0,lsx_getopt_flag_none,1,&optstate);
  p -> bootTime = .35;
  p -> noiseTcUp = .1;
  p -> noiseTcDown = .01;
  p -> noiseReductionAmount = 1.35;
  p -> measureFreq = 20;
/* 50% overlap */
  p -> measureDuration = (2 / (p -> measureFreq));
  p -> measureTc = .4;
  p -> hpFilterFreq = 50;
  p -> lpFilterFreq = 6000;
  p -> hpLifterFreq = 150;
  p -> lpLifterFreq = 2000;
  p -> triggerTc = .25;
  p -> triggerLevel = 7;
  p -> searchTime = 1;
  p -> gapTime = .25;
  while((c = lsx_getopt(&optstate)) != -1){
    switch(c){
      char *parseIndex;
      case 'b':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < .1)) || (d > 10)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "vad.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","bootTime",((double ).1),((double )10)));
            return lsx_usage(effp);
          }
          p -> bootTime = d;
          break; 
        }
      }
      case 'N':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < .1)) || (d > 10)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "vad.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","noiseTcUp",((double ).1),((double )10)));
            return lsx_usage(effp);
          }
          p -> noiseTcUp = d;
          break; 
        }
      }
      case 'n':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < .001)) || (d > .1)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "vad.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","noiseTcDown",((double ).001),((double ).1)));
            return lsx_usage(effp);
          }
          p -> noiseTcDown = d;
          break; 
        }
      }
      case 'r':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < 0)) || (d > 2)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "vad.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","noiseReductionAmount",((double )0),((double )2)));
            return lsx_usage(effp);
          }
          p -> noiseReductionAmount = d;
          break; 
        }
      }
      case 'f':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < 5)) || (d > 50)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "vad.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","measureFreq",((double )5),((double )50)));
            return lsx_usage(effp);
          }
          p -> measureFreq = d;
          break; 
        }
      }
      case 'm':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < .01)) || (d > 1)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "vad.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","measureDuration",((double ).01),((double )1)));
            return lsx_usage(effp);
          }
          p -> measureDuration = d;
          break; 
        }
      }
      case 'M':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < .1)) || (d > 1)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "vad.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","measureTc",((double ).1),((double )1)));
            return lsx_usage(effp);
          }
          p -> measureTc = d;
          break; 
        }
      }
      case 'h':
{
        p -> hpFilterFreq = lsx_parse_frequency_k(optstate.arg,&parseIndex,2147483647);
        if (((p -> hpFilterFreq) < 10) || (( *parseIndex) != 0)) 
          return lsx_usage(effp);
        break; 
      }
      case 'l':
{
        p -> lpFilterFreq = lsx_parse_frequency_k(optstate.arg,&parseIndex,2147483647);
        if (((p -> lpFilterFreq) < 1000) || (( *parseIndex) != 0)) 
          return lsx_usage(effp);
        break; 
      }
      case 'H':
{
        p -> hpLifterFreq = lsx_parse_frequency_k(optstate.arg,&parseIndex,2147483647);
        if (((p -> hpLifterFreq) < 10) || (( *parseIndex) != 0)) 
          return lsx_usage(effp);
        break; 
      }
      case 'L':
{
        p -> lpLifterFreq = lsx_parse_frequency_k(optstate.arg,&parseIndex,2147483647);
        if (((p -> lpLifterFreq) < 1000) || (( *parseIndex) != 0)) 
          return lsx_usage(effp);
        break; 
      }
      case 'T':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < .01)) || (d > 1)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "vad.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","triggerTc",((double ).01),((double )1)));
            return lsx_usage(effp);
          }
          p -> triggerTc = d;
          break; 
        }
      }
      case 't':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < 0)) || (d > 20)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "vad.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","triggerLevel",((double )0),((double )20)));
            return lsx_usage(effp);
          }
          p -> triggerLevel = d;
          break; 
        }
      }
      case 's':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < .1)) || (d > 4)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "vad.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","searchTime",((double ).1),((double )4)));
            return lsx_usage(effp);
          }
          p -> searchTime = d;
          break; 
        }
      }
      case 'g':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < .1)) || (d > 1)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "vad.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","gapTime",((double ).1),((double )1)));
            return lsx_usage(effp);
          }
          p -> gapTime = d;
          break; 
        }
      }
      case 'p':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < 0)) || (d > 4)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "vad.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","preTriggerTime",((double )0),((double )4)));
            return lsx_usage(effp);
          }
          p -> preTriggerTime = d;
          break; 
        }
      }
      default:
{
        ((( *sox_get_globals()).subsystem = "vad.c") , lsx_fail_impl("invalid option `-%c\'",optstate.opt));
        return lsx_usage(effp);
      }
    }
  }
  return (optstate.ind != argc)?lsx_usage(effp) : SOX_SUCCESS;
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  unsigned int i;
  unsigned int fixedPreTriggerLen_ns;
  unsigned int searchPreTriggerLen_ns;
  fixedPreTriggerLen_ns = (((p -> preTriggerTime) * effp -> in_signal.rate) + .5);
  fixedPreTriggerLen_ns *= effp -> in_signal.channels;
  p -> measureLen_ws = ((effp -> in_signal.rate * (p -> measureDuration)) + .5);
  p -> measureLen_ns = ((p -> measureLen_ws) * effp -> in_signal.channels);
  for (p -> dftLen_ws = 16; (p -> dftLen_ws) < (p -> measureLen_ws); p -> dftLen_ws <<= 1) ;
  ((( *sox_get_globals()).subsystem = "vad.c") , lsx_debug_impl("dftLen_ws=%u measureLen_ws=%u",(p -> dftLen_ws),(p -> measureLen_ws)));
  p -> measurePeriod_ns = ((effp -> in_signal.rate / (p -> measureFreq)) + .5);
  p -> measurePeriod_ns *= effp -> in_signal.channels;
  p -> measuresLen = (ceil(((p -> searchTime) * (p -> measureFreq))));
  searchPreTriggerLen_ns = ((p -> measuresLen) * (p -> measurePeriod_ns));
  p -> gapLen = (((p -> gapTime) * (p -> measureFreq)) + .5);
  p -> samplesLen_ns = ((fixedPreTriggerLen_ns + searchPreTriggerLen_ns) + (p -> measureLen_ns));
  p -> samples = (((((p -> samplesLen_ns) * sizeof(( *(p -> samples)))) != 0ULL)?memset(lsx_realloc(0,((p -> samplesLen_ns) * sizeof(( *(p -> samples))))),0,((p -> samplesLen_ns) * sizeof(( *(p -> samples))))) : ((void *)((void *)0))));
  p -> channels = ((((effp -> in_signal.channels * sizeof(( *(p -> channels)))) != 0ULL)?memset(lsx_realloc(0,(effp -> in_signal.channels * sizeof(( *(p -> channels))))),0,(effp -> in_signal.channels * sizeof(( *(p -> channels))))) : ((void *)((void *)0))));
  for (i = 0; i < effp -> in_signal.channels; ++i) {
    chan_t *c = ((p -> channels) + i);
    c -> dftBuf = (((((p -> dftLen_ws) * sizeof(( *(c -> dftBuf)))) != 0ULL)?memset(lsx_realloc(0,((p -> dftLen_ws) * sizeof(( *(c -> dftBuf))))),0,((p -> dftLen_ws) * sizeof(( *(c -> dftBuf))))) : ((void *)((void *)0))));
    c -> spectrum = (((((p -> dftLen_ws) * sizeof(( *(c -> spectrum)))) != 0ULL)?memset(lsx_realloc(0,((p -> dftLen_ws) * sizeof(( *(c -> spectrum))))),0,((p -> dftLen_ws) * sizeof(( *(c -> spectrum))))) : ((void *)((void *)0))));
    c -> noiseSpectrum = (((((p -> dftLen_ws) * sizeof(( *(c -> noiseSpectrum)))) != 0ULL)?memset(lsx_realloc(0,((p -> dftLen_ws) * sizeof(( *(c -> noiseSpectrum))))),0,((p -> dftLen_ws) * sizeof(( *(c -> noiseSpectrum))))) : ((void *)((void *)0))));
    c -> measures = (((((p -> measuresLen) * sizeof(( *(c -> measures)))) != 0ULL)?memset(lsx_realloc(0,((p -> measuresLen) * sizeof(( *(c -> measures))))),0,((p -> measuresLen) * sizeof(( *(c -> measures))))) : ((void *)((void *)0))));
  }
  p -> spectrumWindow = (((((p -> measureLen_ws) * sizeof(( *(p -> spectrumWindow)))) != 0ULL)?memset(lsx_realloc(0,((p -> measureLen_ws) * sizeof(( *(p -> spectrumWindow))))),0,((p -> measureLen_ws) * sizeof(( *(p -> spectrumWindow))))) : ((void *)((void *)0))));
  for (i = 0; i < (p -> measureLen_ws); ++i) 
    (p -> spectrumWindow)[i] = (-2. / ((sox_sample_t )(1 << 32 - 1)) / sqrt(((double )(p -> measureLen_ws))));
  lsx_apply_hann((p -> spectrumWindow),((int )(p -> measureLen_ws)));
  p -> spectrumStart = ((((p -> hpFilterFreq) / effp -> in_signal.rate) * (p -> dftLen_ws)) + .5);
  p -> spectrumStart = (((p -> spectrumStart) >= 1)?(p -> spectrumStart) : 1);
  p -> spectrumEnd = ((((p -> lpFilterFreq) / effp -> in_signal.rate) * (p -> dftLen_ws)) + .5);
  p -> spectrumEnd = (((p -> spectrumEnd) <= ((p -> dftLen_ws) / 2))?(p -> spectrumEnd) : ((p -> dftLen_ws) / 2));
  p -> cepstrumWindow = ((((((p -> spectrumEnd) - (p -> spectrumStart)) * sizeof(( *(p -> cepstrumWindow)))) != 0ULL)?memset(lsx_realloc(0,(((p -> spectrumEnd) - (p -> spectrumStart)) * sizeof(( *(p -> cepstrumWindow))))),0,(((p -> spectrumEnd) - (p -> spectrumStart)) * sizeof(( *(p -> cepstrumWindow))))) : ((void *)((void *)0))));
  for (i = 0; i < ((p -> spectrumEnd) - (p -> spectrumStart)); ++i) 
    (p -> cepstrumWindow)[i] = (2 / sqrt((((double )(p -> spectrumEnd)) - (p -> spectrumStart))));
  lsx_apply_hann((p -> cepstrumWindow),((int )((p -> spectrumEnd) - (p -> spectrumStart))));
  p -> cepstrumStart = (ceil(((effp -> in_signal.rate * .5) / (p -> lpLifterFreq))));
  p -> cepstrumEnd = (floor(((effp -> in_signal.rate * .5) / (p -> hpLifterFreq))));
  p -> cepstrumEnd = (((p -> cepstrumEnd) <= ((p -> dftLen_ws) / 4))?(p -> cepstrumEnd) : ((p -> dftLen_ws) / 4));
  if ((p -> cepstrumEnd) <= (p -> cepstrumStart)) 
    return SOX_EOF;
  p -> noiseTcUpMult = exp(((-1) / ((p -> noiseTcUp) * (p -> measureFreq))));
  p -> noiseTcDownMult = exp(((-1) / ((p -> noiseTcDown) * (p -> measureFreq))));
  p -> measureTcMult = exp(((-1) / ((p -> measureTc) * (p -> measureFreq))));
  p -> triggerMeasTcMult = exp(((-1) / ((p -> triggerTc) * (p -> measureFreq))));
  p -> bootCountMax = (((p -> bootTime) * (p -> measureFreq)) - .5);
  p -> measureTimer_ns = (p -> measureLen_ns);
  p -> bootCount = (p -> measuresIndex = (p -> flushedLen_ns = (p -> samplesIndex_ns = 0)));
/* depends on input data */
  effp -> out_signal.length = ((sox_uint64_t )(-1));
  return SOX_SUCCESS;
}

static int flowFlush(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *ilen,size_t *olen)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t odone = (((p -> samplesLen_ns) - (p -> flushedLen_ns)) <=  *olen)?((p -> samplesLen_ns) - (p -> flushedLen_ns)) :  *olen;
  size_t odone1 = (odone <= ((p -> samplesLen_ns) - (p -> samplesIndex_ns)))?odone : ((p -> samplesLen_ns) - (p -> samplesIndex_ns));
  memcpy(obuf,((p -> samples) + (p -> samplesIndex_ns)),(odone1 * sizeof(( *obuf))));
  if ((p -> samplesIndex_ns += odone1) == (p -> samplesLen_ns)) {
    memcpy((obuf + odone1),(p -> samples),((odone - odone1) * sizeof(( *obuf))));
    p -> samplesIndex_ns = (odone - odone1);
  }
  if ((p -> flushedLen_ns += odone) == (p -> samplesLen_ns)) {
    size_t olen1 = ( *olen - odone);
    ( *(effp -> handler.flow = lsx_flow_copy))(effp,ibuf,(obuf + odone),ilen,&olen1);
    odone += olen1;
  }
  else 
     *ilen = 0;
   *olen = odone;
  return SOX_SUCCESS;
}

static double measure(priv_t *p,chan_t *c,size_t index_ns,unsigned int step_ns,int bootCount)
{
  double mult;
  double result = 0;
  size_t i;
  for (i = 0; i < (p -> measureLen_ws); (++i , (index_ns = ((index_ns + step_ns) % (p -> samplesLen_ns))))) 
    (c -> dftBuf)[i] = ((p -> samples)[index_ns] * (p -> spectrumWindow)[i]);
  memset(((c -> dftBuf) + i),0,(((p -> dftLen_ws) - i) * sizeof(( *(c -> dftBuf)))));
  lsx_safe_rdft(((int )(p -> dftLen_ws)),1,(c -> dftBuf));
  memset((c -> dftBuf),0,((p -> spectrumStart) * sizeof(( *(c -> dftBuf)))));
  for (i = (p -> spectrumStart); i < (p -> spectrumEnd); ++i) {
    double d = sqrt((((c -> dftBuf)[2 * i] * (c -> dftBuf)[2 * i]) + ((c -> dftBuf)[(2 * i) + 1] * (c -> dftBuf)[(2 * i) + 1])));
    mult = ((bootCount >= 0)?(bootCount / (1. + bootCount)) : (p -> measureTcMult));
    (c -> spectrum)[i] = (((c -> spectrum)[i] * mult) + (d * (1 - mult)));
    d = ((c -> spectrum)[i] * (c -> spectrum)[i]);
    mult = ((bootCount >= 0)?0 : (((d > (c -> noiseSpectrum)[i])?(p -> noiseTcUpMult) : (p -> noiseTcDownMult))));
    (c -> noiseSpectrum)[i] = (((c -> noiseSpectrum)[i] * mult) + (d * (1 - mult)));
    d = sqrt(((0 >= (d - ((p -> noiseReductionAmount) * (c -> noiseSpectrum)[i])))?0 : (d - ((p -> noiseReductionAmount) * (c -> noiseSpectrum)[i]))));
    (c -> dftBuf)[i] = (d * (p -> cepstrumWindow)[i - (p -> spectrumStart)]);
  }
  memset(((c -> dftBuf) + i),0,((((p -> dftLen_ws) >> 1) - i) * sizeof(( *(c -> dftBuf)))));
  lsx_safe_rdft((((int )(p -> dftLen_ws)) >> 1),1,(c -> dftBuf));
  for (i = (p -> cepstrumStart); i < (p -> cepstrumEnd); ++i) 
    result += (((c -> dftBuf)[2 * i] * (c -> dftBuf)[2 * i]) + ((c -> dftBuf)[(2 * i) + 1] * (c -> dftBuf)[(2 * i) + 1]));
  result = log((result / ((p -> cepstrumEnd) - (p -> cepstrumStart))));
  return (0 >= (21 + result))?0 : (21 + result);
}

static int flowTrigger(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *ilen,size_t *olen)
{
  priv_t *p = (priv_t *)(effp -> priv);
  sox_bool hasTriggered = sox_false;
  size_t i;
  size_t idone = 0;
  size_t numMeasuresToFlush = 0;
  while((idone <  *ilen) && !(hasTriggered != 0U)){
    p -> measureTimer_ns -= effp -> in_signal.channels;
    for (i = 0; i < effp -> in_signal.channels; (++i , ++idone)) {
      chan_t *c = ((p -> channels) + i);
      (p -> samples)[p -> samplesIndex_ns++] =  *(ibuf++);
      if (!((p -> measureTimer_ns) != 0U)) {
        size_t x = ((((p -> samplesIndex_ns) + (p -> samplesLen_ns)) - (p -> measureLen_ns)) % (p -> samplesLen_ns));
        double meas = measure(p,c,x,effp -> in_signal.channels,(p -> bootCount));
        (c -> measures)[p -> measuresIndex] = meas;
        c -> meanMeas = (((c -> meanMeas) * (p -> triggerMeasTcMult)) + (meas * (1 - (p -> triggerMeasTcMult))));
        if ((hasTriggered |= ((c -> meanMeas) >= (p -> triggerLevel))) != 0U) {
          unsigned int n = (p -> measuresLen);
          unsigned int k = (p -> measuresIndex);
          unsigned int j;
          unsigned int jTrigger = n;
          unsigned int jZero = n;
          for (j = 0; j < n; (++j , (k = (((k + n) - 1) % n)))) 
            if (((c -> measures)[k] >= (p -> triggerLevel)) && (j <= (jTrigger + (p -> gapLen)))) 
              jZero = (jTrigger = j);
            else if (!((c -> measures)[k] != 0.00000) && (jTrigger >= jZero)) 
              jZero = j;
          j = ((j <= jZero)?j : jZero);
          numMeasuresToFlush = (((((j >= numMeasuresToFlush)?j : numMeasuresToFlush)) <= n)?(((j >= numMeasuresToFlush)?j : numMeasuresToFlush)) : n);
        }
        ((( *sox_get_globals()).subsystem = "vad.c") , lsx_debug_more_impl("%12g %12g %u",meas,(c -> meanMeas),((unsigned int )numMeasuresToFlush)));
      }
    }
    if ((p -> samplesIndex_ns) == (p -> samplesLen_ns)) 
      p -> samplesIndex_ns = 0;
    if (!((p -> measureTimer_ns) != 0U)) {
      p -> measureTimer_ns = (p -> measurePeriod_ns);
      ++p -> measuresIndex;
      p -> measuresIndex %= (p -> measuresLen);
      if ((p -> bootCount) >= 0) 
        p -> bootCount = (((p -> bootCount) == (p -> bootCountMax))?-1 : ((p -> bootCount) + 1));
    }
  }
  if (hasTriggered != 0U) {
    size_t ilen1 = ( *ilen - idone);
    p -> flushedLen_ns = (((p -> measuresLen) - numMeasuresToFlush) * (p -> measurePeriod_ns));
    p -> samplesIndex_ns = (((p -> samplesIndex_ns) + (p -> flushedLen_ns)) % (p -> samplesLen_ns));
    ( *(effp -> handler.flow = flowFlush))(effp,ibuf,obuf,&ilen1,olen);
    idone += ilen1;
  }
  else 
     *olen = 0;
   *ilen = idone;
  return SOX_SUCCESS;
}

static int drain(sox_effect_t *effp,sox_sample_t *obuf,size_t *olen)
{
  size_t ilen = 0;
  return ( *effp -> handler.flow)(effp,0,obuf,&ilen,olen);
}

static int stop(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  unsigned int i;
  for (i = 0; i < effp -> in_signal.channels; ++i) {
    chan_t *c = ((p -> channels) + i);
    free((c -> measures));
    free((c -> noiseSpectrum));
    free((c -> spectrum));
    free((c -> dftBuf));
  }
  free((p -> channels));
  free((p -> cepstrumWindow));
  free((p -> spectrumWindow));
  free((p -> samples));
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_vad_effect_fn()
{
  static sox_effect_handler_t handler = {("vad"), ((const char *)((void *)0)), ((16 | 8 | 256)), (create), (start), (flowTrigger), (drain), (stop), ((sox_effect_handler_kill )((void *)0)), ((sizeof(priv_t )))};
  static const char *lines[] = {("[options]"), ("\t-t trigger-level                (7)"), ("\t-T trigger-time-constant        (0.25 s)"), ("\t-s search-time                  (1 s)"), ("\t-g allowed-gap                  (0.25 s)"), ("\t-p pre-trigger-time             (0 s)"), ("Advanced options:"), ("\t-b noise-est-boot-time          (0.35 s)"), ("\t-N noise-est-time-constant-up   (0.1 s)"), ("\t-n noise-est-time-constant-down (0.01 s)"), ("\t-r noise-reduction-amount       (1.35)"), ("\t-f measurement-frequency        (20 Hz)"), ("\t-m measurement-duration         (0.1 s)"), ("\t-M measurement-time-constant    (0.4 s)"), ("\t-h high-pass-filter             (50 Hz)"), ("\t-l low-pass-filter              (6000 Hz)"), ("\t-H high-pass-lifter             (150 Hz)"), ("\t-L low-pass-lifter              (2000 Hz)")};
  static char *usage;
  handler.usage = (lsx_usage_lines(&usage,lines,(sizeof(lines) / sizeof(lines[0]))));
  return (&handler);
}
