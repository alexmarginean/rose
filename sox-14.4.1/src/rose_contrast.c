/* libSoX effect: Contrast Enhancement    (c) 2008 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
typedef struct __unnamed_class___F0_L20_C9_unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__contrast {
double contrast;}priv_t;

static int create(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  p -> contrast = 75;
  (--argc , ++argv);
{
    do {{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 0) || (d > 100)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "contrast.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","contrast",((double )0),((double )100)));
            return lsx_usage(effp);
          }
          p -> contrast = d;
          (--argc , ++argv);
        }
      }
    }while (0);
  }
/* shift range to 0 to 0.1333, default 0.1 */
  p -> contrast /= 750;
  return (argc != 0)?lsx_usage(effp) : SOX_SUCCESS;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t len = ( *isamp = ( *osamp = (( *isamp <=  *osamp)? *isamp :  *osamp)));
  while(len-- != 0UL){
    double d = (( *(ibuf++)) * (-1.57079632679489661923 / ((sox_sample_t )(1 << 32 - 1))));
     *(obuf++) = (sin((d + ((p -> contrast) * sin((d * 4))))) * ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)));
  }
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_contrast_effect_fn()
{
  static sox_effect_handler_t handler = {("contrast"), ("[enhancement (75)]"), (0), (create), ((sox_effect_handler_start )((void *)0)), (flow), ((sox_effect_handler_drain )((void *)0)), ((sox_effect_handler_stop )((void *)0)), ((sox_effect_handler_kill )((void *)0)), ((sizeof(priv_t )))};
  return (&handler);
}
