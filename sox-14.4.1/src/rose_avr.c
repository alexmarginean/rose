/* AVR file format handler for SoX
 * Copyright (C) 1999 Jan Paul Schmidt <jps@fundament.org>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include <stdio.h>
#include <string.h>
#define AVR_MAGIC "2BIT"
/* Taken from the Audio File Formats FAQ */
typedef struct __unnamed_class___F0_L28_C9_unknown_scope_and_name_variable_declaration__variable_type__Ab_c_index_5_Ae__variable_name_unknown_scope_and_name__scope__magic__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_c_index_8_Ae__variable_name_unknown_scope_and_name__scope__name__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Us_variable_name_unknown_scope_and_name__scope__mono__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Us_variable_name_unknown_scope_and_name__scope__rez__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Us_variable_name_unknown_scope_and_name__scope__sign__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Us_variable_name_unknown_scope_and_name__scope__loop__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Us_variable_name_unknown_scope_and_name__scope__midi__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint32_tUi__typedef_declaration_variable_name_unknown_scope_and_name__scope__rate__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint32_tUi__typedef_declaration_variable_name_unknown_scope_and_name__scope__size__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint32_tUi__typedef_declaration_variable_name_unknown_scope_and_name__scope__lbeg__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint32_tUi__typedef_declaration_variable_name_unknown_scope_and_name__scope__lend__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Us_variable_name_unknown_scope_and_name__scope__res1__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Us_variable_name_unknown_scope_and_name__scope__res2__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Us_variable_name_unknown_scope_and_name__scope__res3__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_c_index_20_Ae__variable_name_unknown_scope_and_name__scope__ext__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_c_index_64_Ae__variable_name_unknown_scope_and_name__scope__user {
/* 2BIT */
char magic[5UL];
/* null-padded sample name */
char name[8UL];
/* 0 = mono, 0xffff = stereo */
unsigned short mono;
/* 8 = 8 bit, 16 = 16 bit */
unsigned short rez;
/* 0 = unsigned, 0xffff = signed */
unsigned short sign;
/* 0 = no loop, 0xffff = looping sample */
unsigned short loop;
/* 0xffff = no MIDI note assigned,
                          0xffXX = single key note assignment
                          0xLLHH = key split, low/hi note */
unsigned short midi;
/* sample frequency in hertz */
uint32_t rate;
/* sample length in bytes or words (see rez) */
uint32_t size;
/* offset to start of loop in bytes or words.
                          set to zero if unused. */
uint32_t lbeg;
/* offset to end of loop in bytes or words.
                          set to sample length if unused. */
uint32_t lend;
/* Reserved, MIDI keyboard split */
unsigned short res1;
/* Reserved, sample compression */
unsigned short res2;
/* Reserved */
unsigned short res3;
/* Additional filename space, used
                          if (name[7] != 0) */
char ext[15 * sizeof(int ) - 4 * sizeof(void *) - sizeof(size_t )];
/* User defined. Typically ASCII message. */
char user[64UL];}priv_t;
/*
 * Do anything required before you start reading samples.
 * Read file header.
 *      Find out sampling rate,
 *      size and encoding of samples,
 *      mono/stereo/quad.
 */

static int startread(sox_format_t *ft)
{
  priv_t *avr = (priv_t *)(ft -> priv);
  int rc;
  lsx_reads(ft,(avr -> magic),((size_t )4));
  if (strncmp((avr -> magic),"2BIT",((size_t )4)) != 0) {
    lsx_fail_errno(ft,SOX_EHDR,"AVR: unknown header");
    return SOX_EOF;
  }
  lsx_readbuf(ft,(avr -> name),(sizeof(avr -> name)));
  lsx_readw(ft,&avr -> mono);
  if ((avr -> mono) != 0) {
    ft -> signal.channels = 2;
  }
  else {
    ft -> signal.channels = 1;
  }
  lsx_readw(ft,&avr -> rez);
  if ((avr -> rez) == 8) {
    ft -> encoding.bits_per_sample = 8;
  }
  else if ((avr -> rez) == 16) {
    ft -> encoding.bits_per_sample = 16;
  }
  else {
    lsx_fail_errno(ft,SOX_EFMT,"AVR: unsupported sample resolution");
    return SOX_EOF;
  }
  lsx_readw(ft,&avr -> sign);
  if ((avr -> sign) != 0) {
    ft -> encoding.encoding = SOX_ENCODING_SIGN2;
  }
  else {
    ft -> encoding.encoding = SOX_ENCODING_UNSIGNED;
  }
  lsx_readw(ft,&avr -> loop);
  lsx_readw(ft,&avr -> midi);
  lsx_readdw(ft,&avr -> rate);
/*
   * No support for AVRs created by ST-Replay,
   * Replay Proffesional and PRO-Series 12.
   *
   * Just masking the upper byte out.
   */
  ft -> signal.rate = ((avr -> rate) & 0x00ffffff);
  lsx_readdw(ft,&avr -> size);
  lsx_readdw(ft,&avr -> lbeg);
  lsx_readdw(ft,&avr -> lend);
  lsx_readw(ft,&avr -> res1);
  lsx_readw(ft,&avr -> res2);
  lsx_readw(ft,&avr -> res3);
  lsx_readbuf(ft,(avr -> ext),(sizeof(avr -> ext)));
  lsx_readbuf(ft,(avr -> user),(sizeof(avr -> user)));
  rc = lsx_rawstart(ft,sox_false,sox_false,sox_false,SOX_ENCODING_UNKNOWN,0);
  if (rc != 0) 
    return rc;
  return SOX_SUCCESS;
}

static int startwrite(sox_format_t *ft)
{
  priv_t *avr = (priv_t *)(ft -> priv);
  int rc;
  if (!((ft -> seekable) != 0U)) {
    lsx_fail_errno(ft,SOX_EOF,"AVR: file is not seekable");
    return SOX_EOF;
  }
  rc = lsx_rawstart(ft,sox_false,sox_false,sox_false,SOX_ENCODING_UNKNOWN,0);
  if (rc != 0) 
    return rc;
/* magic */
  lsx_writes(ft,"2BIT");
/* name */
  lsx_writeb(ft,0);
  lsx_writeb(ft,0);
  lsx_writeb(ft,0);
  lsx_writeb(ft,0);
  lsx_writeb(ft,0);
  lsx_writeb(ft,0);
  lsx_writeb(ft,0);
  lsx_writeb(ft,0);
/* mono */
  if (ft -> signal.channels == 1) {
    lsx_writew(ft,0);
  }
  else if (ft -> signal.channels == 2) {
    lsx_writew(ft,0xffff);
  }
  else {
    lsx_fail_errno(ft,SOX_EFMT,"AVR: number of channels not supported");
    return 0;
  }
/* rez */
  if (ft -> encoding.bits_per_sample == 8) {
    lsx_writew(ft,8);
  }
  else if (ft -> encoding.bits_per_sample == 16) {
    lsx_writew(ft,16);
  }
  else {
    lsx_fail_errno(ft,SOX_EFMT,"AVR: unsupported sample resolution");
    return SOX_EOF;
  }
/* sign */
  if (ft -> encoding.encoding == SOX_ENCODING_SIGN2) {
    lsx_writew(ft,0xffff);
  }
  else if (ft -> encoding.encoding == SOX_ENCODING_UNSIGNED) {
    lsx_writew(ft,0);
  }
  else {
    lsx_fail_errno(ft,SOX_EFMT,"AVR: unsupported encoding");
    return SOX_EOF;
  }
/* loop */
  lsx_writew(ft,0xffff);
/* midi */
  lsx_writew(ft,0xffff);
/* rate */
  lsx_writedw(ft,((unsigned int )(ft -> signal.rate + .5)));
/* size */
/* Don't know the size yet. */
  lsx_writedw(ft,0);
/* lbeg */
  lsx_writedw(ft,0);
/* lend */
/* Don't know the size yet, so we can't set lend, either. */
  lsx_writedw(ft,0);
/* res1 */
  lsx_writew(ft,0);
/* res2 */
  lsx_writew(ft,0);
/* res3 */
  lsx_writew(ft,0);
/* ext */
  lsx_writebuf(ft,"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000",(sizeof(avr -> ext)));
/* user */
  lsx_writebuf(ft,"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000",(sizeof(avr -> user)));
  return SOX_SUCCESS;
}

static size_t write_samples(sox_format_t *ft,const sox_sample_t *buf,size_t nsamp)
{
  priv_t *avr = (priv_t *)(ft -> priv);
  avr -> size += nsamp;
  return lsx_rawwrite(ft,buf,nsamp);
}

static int stopwrite(sox_format_t *ft)
{
  priv_t *avr = (priv_t *)(ft -> priv);
  unsigned int size = ((avr -> size) / ft -> signal.channels);
/* Fix size */
  lsx_seeki(ft,((off_t )26),0);
  lsx_writedw(ft,size);
/* Fix lend */
  lsx_seeki(ft,((off_t )34),0);
  lsx_writedw(ft,size);
  return SOX_SUCCESS;
}
const sox_format_handler_t *lsx_avr_format_fn();

const sox_format_handler_t *lsx_avr_format_fn()
{
  static const char *const names[] = {("avr"), ((const char *)((void *)0))};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_SIGN2), (16), (8), (0), (SOX_ENCODING_UNSIGNED), (16), (8), (0), (0)};
  static sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("Audio Visual Research format; used on the Mac"), (names), ((64 | 0x0080 | 256 | 0x0200)), (startread), (lsx_rawread), ((sox_format_handler_stopread )((void *)0)), (startwrite), (write_samples), (stopwrite), ((sox_format_handler_seek )((void *)0)), (write_encodings), ((const sox_rate_t *)((void *)0)), ((sizeof(priv_t )))};
  return (&handler);
}
