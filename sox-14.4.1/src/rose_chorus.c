/* August 24, 1998
 * Copyright (C) 1998 Juergen Mueller And Sundry Contributors
 * This source code is freely redistributable and may be used for
 * any purpose.  This copyright notice must be maintained.
 * Juergen Mueller And Sundry Contributors are not responsible for
 * the consequences of using this software.
 */
/*
 *      Chorus effect.
 *
 * Flow diagram scheme for n delays ( 1 <= n <= MAX_CHORUS ):
 *
 *        * gain-in                                           ___
 * ibuff -----+--------------------------------------------->|   |
 *            |      _________                               |   |
 *            |     |         |                   * decay 1  |   |
 *            +---->| delay 1 |----------------------------->|   |
 *            |     |_________|                              |   |
 *            |        /|\                                   |   |
 *            :         |                                    |   |
 *            : +-----------------+   +--------------+       | + |
 *            : | Delay control 1 |<--| mod. speed 1 |       |   |
 *            : +-----------------+   +--------------+       |   |
 *            |      _________                               |   |
 *            |     |         |                   * decay n  |   |
 *            +---->| delay n |----------------------------->|   |
 *                  |_________|                              |   |
 *                     /|\                                   |___|
 *                      |                                      |
 *              +-----------------+   +--------------+         | * gain-out
 *              | Delay control n |<--| mod. speed n |         |
 *              +-----------------+   +--------------+         +----->obuff
 *
 *
 * The delay i is controled by a sine or triangle modulation i ( 1 <= i <= n).
 *
 * Usage:
 *   chorus gain-in gain-out delay-1 decay-1 speed-1 depth-1 -s1|t1 [
 *       delay-2 decay-2 speed-2 depth-2 -s2|-t2 ... ]
 *
 * Where:
 *   gain-in, decay-1 ... decay-n :  0.0 ... 1.0      volume
 *   gain-out :  0.0 ...      volume
 *   delay-1 ... delay-n :  20.0 ... 100.0 msec
 *   speed-1 ... speed-n :  0.1 ... 5.0 Hz       modulation 1 ... n
 *   depth-1 ... depth-n :  0.0 ... 10.0 msec    modulated delay 1 ... n
 *   -s1 ... -sn : modulation by sine 1 ... n
 *   -t1 ... -tn : modulation by triangle 1 ... n
 *
 * Note:
 *   when decay is close to 1.0, the samples can begin clipping and the output
 *   can saturate!
 *
 * Hint:
 *   1 / out-gain < gain-in ( 1 + decay-1 + ... + decay-n )
 *
*/
/*
 * libSoX chorus effect file.
 */
#include "sox_i.h"
#include <stdlib.h> /* Harmless, and prototypes atof() etc. --dgc */
#include <string.h>
#define MOD_SINE        0
#define MOD_TRIANGLE    1
#define MAX_CHORUS      7
typedef struct __unnamed_class___F0_L73_C9_unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__num_chorus__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_i_index_7_Ae__variable_name_unknown_scope_and_name__scope__modulation__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__counter__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_l_index_7_Ae__variable_name_unknown_scope_and_name__scope__phase__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__f__Pe___variable_name_unknown_scope_and_name__scope__chorusbuf__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_f_variable_name_unknown_scope_and_name__scope__in_gain__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_f_variable_name_unknown_scope_and_name__scope__out_gain__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_f_index_7_Ae__variable_name_unknown_scope_and_name__scope__delay__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_f_index_7_Ae__variable_name_unknown_scope_and_name__scope__decay__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_f_index_7_Ae__variable_name_unknown_scope_and_name__scope__speed__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_f_index_7_Ae__variable_name_unknown_scope_and_name__scope__depth__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_l_index_7_Ae__variable_name_unknown_scope_and_name__scope__length__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab___Pb__i__Pe___index_7_Ae__variable_name_unknown_scope_and_name__scope__lookup_tab__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_i_index_7_Ae__variable_name_unknown_scope_and_name__scope__depth_samples__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_i_index_7_Ae__variable_name_unknown_scope_and_name__scope__samples__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__maxsamples__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__fade_out {
int num_chorus;
int modulation[7UL];
int counter;
long phase[7UL];
float *chorusbuf;
float in_gain;
float out_gain;
float delay[7UL];
float decay[7UL];
float speed[7UL];
float depth[7UL];
long length[7UL];
int *lookup_tab[7UL];
int depth_samples[7UL];
int samples[7UL];
int maxsamples;
unsigned int fade_out;}priv_t;
/*
 * Process options
 */

static int sox_chorus_getopts(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *chorus = (priv_t *)(effp -> priv);
  int i;
  (--argc , ++argv);
  chorus -> num_chorus = 0;
  i = 0;
  if ((argc < 7) || (((argc - 2) % 5) != 0)) 
    return lsx_usage(effp);
  sscanf(argv[i++],"%f",&chorus -> in_gain);
  sscanf(argv[i++],"%f",&chorus -> out_gain);
  while(i < argc){
    if ((chorus -> num_chorus) > 7) {
      ((( *sox_get_globals()).subsystem = "chorus.c") , lsx_fail_impl("chorus: to many delays, use less than %i delays",7));
      return SOX_EOF;
    }
    sscanf(argv[i++],"%f",((chorus -> delay) + (chorus -> num_chorus)));
    sscanf(argv[i++],"%f",((chorus -> decay) + (chorus -> num_chorus)));
    sscanf(argv[i++],"%f",((chorus -> speed) + (chorus -> num_chorus)));
    sscanf(argv[i++],"%f",((chorus -> depth) + (chorus -> num_chorus)));
    if (!(strcmp(argv[i],"-s") != 0)) 
      (chorus -> modulation)[chorus -> num_chorus] = 0;
    else if (!(strcmp(argv[i],"-t") != 0)) 
      (chorus -> modulation)[chorus -> num_chorus] = 1;
    else 
      return lsx_usage(effp);
    i++;
    chorus -> num_chorus++;
  }
  return SOX_SUCCESS;
}
/*
 * Prepare for processing.
 */

static int sox_chorus_start(sox_effect_t *effp)
{
  priv_t *chorus = (priv_t *)(effp -> priv);
  int i;
  float sum_in_volume;
  chorus -> maxsamples = 0;
  if ((chorus -> in_gain) < 0.0) {
    ((( *sox_get_globals()).subsystem = "chorus.c") , lsx_fail_impl("chorus: gain-in must be positive!"));
    return SOX_EOF;
  }
  if ((chorus -> in_gain) > 1.0) {
    ((( *sox_get_globals()).subsystem = "chorus.c") , lsx_fail_impl("chorus: gain-in must be less than 1.0!"));
    return SOX_EOF;
  }
  if ((chorus -> out_gain) < 0.0) {
    ((( *sox_get_globals()).subsystem = "chorus.c") , lsx_fail_impl("chorus: gain-out must be positive!"));
    return SOX_EOF;
  }
  for (i = 0; i < (chorus -> num_chorus); i++) {
    (chorus -> samples)[i] = ((int )((((chorus -> delay)[i] + (chorus -> depth)[i]) * effp -> in_signal.rate) / 1000.0));
    (chorus -> depth_samples)[i] = ((int )(((chorus -> depth)[i] * effp -> in_signal.rate) / 1000.0));
    if ((chorus -> delay)[i] < 20.0) {
      ((( *sox_get_globals()).subsystem = "chorus.c") , lsx_fail_impl("chorus: delay must be more than 20.0 msec!"));
      return SOX_EOF;
    }
    if ((chorus -> delay)[i] > 100.0) {
      ((( *sox_get_globals()).subsystem = "chorus.c") , lsx_fail_impl("chorus: delay must be less than 100.0 msec!"));
      return SOX_EOF;
    }
    if ((chorus -> speed)[i] < 0.1) {
      ((( *sox_get_globals()).subsystem = "chorus.c") , lsx_fail_impl("chorus: speed must be more than 0.1 Hz!"));
      return SOX_EOF;
    }
    if ((chorus -> speed)[i] > 5.0) {
      ((( *sox_get_globals()).subsystem = "chorus.c") , lsx_fail_impl("chorus: speed must be less than 5.0 Hz!"));
      return SOX_EOF;
    }
    if ((chorus -> depth)[i] < 0.0) {
      ((( *sox_get_globals()).subsystem = "chorus.c") , lsx_fail_impl("chorus: delay must be more positive!"));
      return SOX_EOF;
    }
    if ((chorus -> depth)[i] > 10.0) {
      ((( *sox_get_globals()).subsystem = "chorus.c") , lsx_fail_impl("chorus: delay must be less than 10.0 msec!"));
      return SOX_EOF;
    }
    if ((chorus -> decay)[i] < 0.0) {
      ((( *sox_get_globals()).subsystem = "chorus.c") , lsx_fail_impl("chorus: decay must be positive!"));
      return SOX_EOF;
    }
    if ((chorus -> decay)[i] > 1.0) {
      ((( *sox_get_globals()).subsystem = "chorus.c") , lsx_fail_impl("chorus: decay must be less that 1.0!"));
      return SOX_EOF;
    }
    (chorus -> length)[i] = (effp -> in_signal.rate / (chorus -> speed)[i]);
    (chorus -> lookup_tab)[i] = (lsx_realloc(0,(sizeof(int ) * (chorus -> length)[i])));
    if ((chorus -> modulation)[i] == 0) 
      lsx_generate_wave_table(SOX_WAVE_SINE,SOX_INT,(chorus -> lookup_tab)[i],((size_t )(chorus -> length)[i]),0.0,((double )(chorus -> depth_samples)[i]),0.0);
    else 
      lsx_generate_wave_table(SOX_WAVE_TRIANGLE,SOX_INT,(chorus -> lookup_tab)[i],((size_t )(chorus -> length)[i]),((double )(((chorus -> samples)[i] - 1) - (2 * (chorus -> depth_samples)[i]))),((double )((chorus -> samples)[i] - 1)),3 * 1.57079632679489661923);
    (chorus -> phase)[i] = 0;
    if ((chorus -> samples)[i] > (chorus -> maxsamples)) 
      chorus -> maxsamples = (chorus -> samples)[i];
  }
/* Be nice and check the hint with warning, if... */
  sum_in_volume = 1.0;
  for (i = 0; i < (chorus -> num_chorus); i++) 
    sum_in_volume += (chorus -> decay)[i];
  if (((chorus -> in_gain) * sum_in_volume) > (1.0 / (chorus -> out_gain))) 
    ((( *sox_get_globals()).subsystem = "chorus.c") , lsx_warn_impl("chorus: warning >>> gain-out can cause saturation or clipping of output <<<"));
  chorus -> chorusbuf = (lsx_realloc(0,(sizeof(float ) * (chorus -> maxsamples))));
  for (i = 0; i < (chorus -> maxsamples); i++) 
    (chorus -> chorusbuf)[i] = 0.0;
  chorus -> counter = 0;
  chorus -> fade_out = (chorus -> maxsamples);
/* TODO: calculate actual length */
  effp -> out_signal.length = ((sox_uint64_t )(-1));
  return SOX_SUCCESS;
}
/*
 * Processed signed long samples from ibuf to obuf.
 * Return number of samples processed.
 */

static int sox_chorus_flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *chorus = (priv_t *)(effp -> priv);
  int i;
  float d_in;
  float d_out;
  sox_sample_t out;
  size_t len = ( *isamp <=  *osamp)? *isamp :  *osamp;
   *isamp = ( *osamp = len);
  while(len-- != 0UL){
/* Store delays as 24-bit signed longs */
    d_in = (((float )( *(ibuf++))) / 256);
/* Compute output first */
    d_out = (d_in * (chorus -> in_gain));
    for (i = 0; i < (chorus -> num_chorus); i++) 
      d_out += ((chorus -> chorusbuf)[(((chorus -> maxsamples) + (chorus -> counter)) - (chorus -> lookup_tab)[i][(chorus -> phase)[i]]) % (chorus -> maxsamples)] * (chorus -> decay)[i]);
/* Adjust the output volume and size to 24 bit */
    d_out = (d_out * (chorus -> out_gain));
    out = ((((sox_sample_t )d_out) > (1 << 24 - 1) - 1)?((++effp -> clips , (1 << 24 - 1) - 1)) : (((((sox_sample_t )d_out) < -1 << 24 - 1)?((++effp -> clips , -1 << 24 - 1)) : ((sox_sample_t )d_out))));
     *(obuf++) = (out * 256);
/* Mix decay of delay and input */
    (chorus -> chorusbuf)[chorus -> counter] = d_in;
    chorus -> counter = (((chorus -> counter) + 1) % (chorus -> maxsamples));
    for (i = 0; i < (chorus -> num_chorus); i++) 
      (chorus -> phase)[i] = (((chorus -> phase)[i] + 1) % (chorus -> length)[i]);
  }
/* processed all samples */
  return SOX_SUCCESS;
}
/*
 * Drain out reverb lines.
 */

static int sox_chorus_drain(sox_effect_t *effp,sox_sample_t *obuf,size_t *osamp)
{
  priv_t *chorus = (priv_t *)(effp -> priv);
  size_t done;
  int i;
  float d_in;
  float d_out;
  sox_sample_t out;
  done = 0;
  while((done <  *osamp) && (done < (chorus -> fade_out))){
    d_in = 0;
    d_out = 0;
/* Compute output first */
    for (i = 0; i < (chorus -> num_chorus); i++) 
      d_out += ((chorus -> chorusbuf)[(((chorus -> maxsamples) + (chorus -> counter)) - (chorus -> lookup_tab)[i][(chorus -> phase)[i]]) % (chorus -> maxsamples)] * (chorus -> decay)[i]);
/* Adjust the output volume and size to 24 bit */
    d_out = (d_out * (chorus -> out_gain));
    out = ((((sox_sample_t )d_out) > (1 << 24 - 1) - 1)?((++effp -> clips , (1 << 24 - 1) - 1)) : (((((sox_sample_t )d_out) < -1 << 24 - 1)?((++effp -> clips , -1 << 24 - 1)) : ((sox_sample_t )d_out))));
     *(obuf++) = (out * 256);
/* Mix decay of delay and input */
    (chorus -> chorusbuf)[chorus -> counter] = d_in;
    chorus -> counter = (((chorus -> counter) + 1) % (chorus -> maxsamples));
    for (i = 0; i < (chorus -> num_chorus); i++) 
      (chorus -> phase)[i] = (((chorus -> phase)[i] + 1) % (chorus -> length)[i]);
    done++;
    chorus -> fade_out--;
  }
/* samples played, it remains */
   *osamp = done;
  if ((chorus -> fade_out) == 0) 
    return SOX_EOF;
  else 
    return SOX_SUCCESS;
}
/*
 * Clean up chorus effect.
 */

static int sox_chorus_stop(sox_effect_t *effp)
{
  priv_t *chorus = (priv_t *)(effp -> priv);
  int i;
  free((chorus -> chorusbuf));
  chorus -> chorusbuf = ((float *)((void *)0));
  for (i = 0; i < (chorus -> num_chorus); i++) {
    free((chorus -> lookup_tab)[i]);
    (chorus -> lookup_tab)[i] = ((int *)((void *)0));
  }
  return SOX_SUCCESS;
}
static sox_effect_handler_t sox_chorus_effect = {("chorus"), ("gain-in gain-out delay decay speed depth [ -s | -t ]"), ((8 | 128)), (sox_chorus_getopts), (sox_chorus_start), (sox_chorus_flow), (sox_chorus_drain), (sox_chorus_stop), ((sox_effect_handler_kill )((void *)0)), ((sizeof(priv_t )))};

const sox_effect_handler_t *lsx_chorus_effect_fn()
{
  return (&sox_chorus_effect);
}
