/* Effect: firfit filter     Copyright (c) 2009 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* This is W.I.P. hence marked SOX_EFF_ALPHA for now.
 * Need to add other interpolation types e.g. linear, bspline, window types,
 * and filter length, maybe phase response type too.
 */
#include "sox_i.h"
#include "dft_filter.h"
typedef struct __unnamed_class___F0_L26_C9_unknown_scope_and_name_variable_declaration__variable_type_L129R_variable_name_unknown_scope_and_name__scope__base__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__filename__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__un_named_base_type__Pe___variable_name_unknown_scope_and_name__scope__knots__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__num_knots__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__n {
dft_filter_priv_t base;
const char *filename;
struct __unnamed_class___F0_L29_C3_L130R_variable_declaration__variable_type_d_variable_name_L130R__scope__f__DELIMITER__L130R_variable_declaration__variable_type_d_variable_name_L130R__scope__gain {
double f;
double gain;}*knots;
int num_knots;
int n;}priv_t;

static int create(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  dft_filter_priv_t *b = &p -> base;
  b -> filter_ptr = &b -> filter;
  (--argc , ++argv);
  if (argc == 1) 
    ((p -> filename = argv[0]) , --argc);
  p -> n = 2047;
  return (argc != 0)?lsx_usage(effp) : SOX_SUCCESS;
}

static double *make_filter(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  double *log_freqs;
  double *gains;
  double *d;
  double *work;
  double *h;
  sox_rate_t rate = effp -> in_signal.rate;
  int i;
  int work_len;
  log_freqs = (lsx_realloc(0,((p -> num_knots) * sizeof(( *log_freqs)))));
  gains = (lsx_realloc(0,((p -> num_knots) * sizeof(( *gains)))));
  d = (lsx_realloc(0,((p -> num_knots) * sizeof(( *d)))));
  for (i = 0; i < (p -> num_knots); ++i) {
    log_freqs[i] = log((((p -> knots)[i].f >= 1)?(p -> knots)[i].f : 1));
    gains[i] = (p -> knots)[i].gain;
  }
  lsx_prepare_spline3(log_freqs,gains,(p -> num_knots),__builtin_huge_val(),__builtin_huge_val(),d);
  for (work_len = 8192; work_len < (rate / 2); work_len <<= 1) ;
  work = (((((work_len + 2) * sizeof(( *work))) != 0ULL)?memset(lsx_realloc(0,((work_len + 2) * sizeof(( *work)))),0,((work_len + 2) * sizeof(( *work)))) : ((void *)((void *)0))));
  h = (lsx_realloc(0,((p -> n) * sizeof(( *h)))));
  for (i = 0; i <= work_len; i += 2) {
    double f = (((rate * 0.5) * i) / work_len);
    double spl1 = (f < ((((p -> knots)[0].f >= 1)?(p -> knots)[0].f : 1)))?gains[0] : (((f > (p -> knots)[(p -> num_knots) - 1].f)?gains[(p -> num_knots) - 1] : lsx_spline3(log_freqs,gains,d,(p -> num_knots),log(f))));
    work[i] = exp(((spl1 * 2.30258509299404568402) * 0.05));
  }
  work[1] = work[work_len];
  lsx_safe_rdft(work_len,-1,work);
  for (i = 0; i < (p -> n); ++i) 
    h[i] = ((work[((work_len - ((p -> n) / 2)) + i) % work_len] * 2.) / work_len);
  lsx_apply_blackman_nutall(h,(p -> n));
  free(work);
  return h;
}

static sox_bool read_knots(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  FILE *file = lsx_open_input_file(effp,(p -> filename));
  sox_bool result = sox_false;
  int num_converted = 1;
  char c;
  if (file != 0) {
    p -> knots = (lsx_realloc(0,(1 * sizeof(( *(p -> knots))))));
{
      while(fscanf(file," #%*[^\n]%c",&c) >= 0){
        num_converted = fscanf(file,"%lf %lf",&(p -> knots)[p -> num_knots].f,&(p -> knots)[p -> num_knots].gain);
        if (num_converted == 2) 
          p -> knots = (lsx_realloc((p -> knots),((++p -> num_knots + 1) * sizeof(( *(p -> knots))))));
        else if (num_converted != 0) 
          break; 
      }
    }
    ((( *sox_get_globals()).subsystem = "firfit.c") , lsx_report_impl("%i knots",(p -> num_knots)));
    if ((feof(file) != 0) && (num_converted != 1)) 
      result = sox_true;
    else 
      ((( *sox_get_globals()).subsystem = "firfit.c") , lsx_fail_impl("error reading knot file"));
    if (file != stdin) 
      fclose(file);
  }
  return result;
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  dft_filter_t *f = p -> base.filter_ptr;
  if (!((f -> num_taps) != 0)) {
    if (!((p -> num_knots) != 0) && !(read_knots(effp) != 0U)) 
      return SOX_EOF;
    lsx_set_dft_filter(f,make_filter(effp),(p -> n),((p -> n) >> 1));
  }
  return ( *( *lsx_dft_filter_effect_fn()).start)(effp);
}

const sox_effect_handler_t *lsx_firfit_effect_fn()
{
  static sox_effect_handler_t handler;
  handler =  *lsx_dft_filter_effect_fn();
  handler.name = "firfit";
  handler.usage = "[knots-file]";
  handler.flags |= 512;
  handler.getopts = create;
  handler.start = start;
  handler.priv_size = (sizeof(priv_t ));
  return (&handler);
}
