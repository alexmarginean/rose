/* libSoX file format: SoX native   (c) 2008 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include <string.h>
static const char magic[2UL][4UL] = {(".SoX"), ("XoS.")};
#define FIXED_HDR     (4 + 8 + 8 + 4 + 4) /* Without magic */

static int startread(sox_format_t *ft)
{
  char magic_[4UL];
  uint32_t headers_bytes;
  uint32_t num_channels;
  uint32_t comments_bytes;
  uint64_t num_samples;
  double rate;
  if (lsx_readdw(ft,((uint32_t *)(&magic_))) != 0) 
    return SOX_EOF;
  if (memcmp(magic[0],magic_,(sizeof(magic_))) != 0) {
    if (memcmp(magic[1],magic_,(sizeof(magic_))) != 0) {
      lsx_fail_errno(ft,SOX_EHDR,"can\'t find sox file format identifier");
      return SOX_EOF;
    }
    ft -> encoding.reverse_bytes = (!(ft -> encoding.reverse_bytes != 0U));
    ((( *sox_get_globals()).subsystem = "sox-fmt.c") , lsx_report_impl("file is opposite endian"));
  }
  if (((((lsx_readdw(ft,&headers_bytes) != 0) || (lsx_readqw(ft,&num_samples) != 0)) || (lsx_readdf(ft,&rate) != 0)) || (lsx_readdw(ft,&num_channels) != 0)) || (lsx_readdw(ft,&comments_bytes) != 0)) 
    return SOX_EOF;
  if (((((headers_bytes + 4) & 7) != 0U) || (headers_bytes < ((4 + 8 + 8 + 4 + 4) + comments_bytes))) || (num_channels > 65535)) 
/* Reserve top 16 bits */
{
    lsx_fail_errno(ft,SOX_EHDR,"invalid sox file format header");
    return SOX_EOF;
  }
  if (comments_bytes != 0U) {
/* ensure nul-terminated */
    char *buf = (((1 * (((size_t )comments_bytes) + 1)) != 0UL)?memset(lsx_realloc(0,(1 * (((size_t )comments_bytes) + 1))),0,(1 * (((size_t )comments_bytes) + 1))) : ((void *)((void *)0)));
    if (lsx_readchars(ft,buf,((size_t )comments_bytes)) != SOX_SUCCESS) {
      free(buf);
      return SOX_EOF;
    }
    sox_append_comments(&ft -> oob.comments,buf);
    free(buf);
  }
/* Consume any bytes after the comments and before the start of the audio
   * block.  These may include comment padding up to a multiple of 8 bytes,
   * and further header information that might be defined in future. */
  lsx_seeki(ft,((off_t )((headers_bytes - (4 + 8 + 8 + 4 + 4)) - comments_bytes)),1);
  return lsx_check_read_params(ft,num_channels,rate,SOX_ENCODING_SIGN2,32,num_samples,sox_true);
}

static int write_header(sox_format_t *ft)
{
  char *comments = lsx_cat_comments(ft -> oob.comments);
  size_t comments_len = strlen(comments);
/* Multiple of 8 bytes */
  size_t comments_bytes = ((comments_len + 7) & (~7u));
  uint64_t size = ((ft -> olength) != 0UL)?(ft -> olength) : ft -> signal.length;
  int error;
  uint32_t header;
  memcpy((&header),magic[0],(sizeof(header)));
  error = ((((((((0 || (lsx_writedw(ft,header) != 0)) || (lsx_writedw(ft,((4 + 8 + 8 + 4 + 4) + ((unsigned int )comments_bytes))) != 0)) || (lsx_writeqw(ft,size) != 0)) || (lsx_writedf(ft,ft -> signal.rate) != 0)) || (lsx_writedw(ft,ft -> signal.channels) != 0)) || (lsx_writedw(ft,((unsigned int )comments_len)) != 0)) || ((((lsx_writebuf(ft,comments,comments_len) == comments_len)?SOX_SUCCESS : SOX_EOF)) != 0)) || (lsx_padbytes(ft,(comments_bytes - comments_len)) != 0));
  free(comments);
  return (error != 0)?SOX_EOF : SOX_SUCCESS;
}
const sox_format_handler_t *lsx_sox_format_fn();

const sox_format_handler_t *lsx_sox_format_fn()
{
  static const char *const names[] = {("sox"), ((const char *)((void *)0))};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_SIGN2), (32), (0), (0)};
  static const sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("SoX native intermediate format"), (names), (8), (startread), (lsx_rawread), ((sox_format_handler_stopread )((void *)0)), (write_header), (lsx_rawwrite), ((sox_format_handler_stopwrite )((void *)0)), (lsx_rawseek), (write_encodings), ((const sox_rate_t *)((void *)0)), (0)};
  return &handler;
}
