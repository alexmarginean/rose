/* libSoX repeat effect  Copyright (c) 2004 Jan Paul Schmidt <jps@fundament.org>
 * Re-write (c) 2008 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
typedef struct __unnamed_class___F0_L21_C9_unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__num_repeats__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__remaining_repeats__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__num_samples__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__remaining_samples__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__FILE_IO_FILE__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__tmp_file {
unsigned int num_repeats;
unsigned int remaining_repeats;
uint64_t num_samples;
uint64_t remaining_samples;
FILE *tmp_file;}priv_t;

static int create(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  p -> num_repeats = 1;
  (--argc , ++argv);
{
    do {{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 0) || (d > (2147483647 * 2U + 1U - 1))) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "repeat.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","num_repeats",((double )0),((double )(2147483647 * 2U + 1U)) - 1));
            return lsx_usage(effp);
          }
          p -> num_repeats = d;
          (--argc , ++argv);
        }
      }
    }while (0);
  }
  return (argc != 0)?lsx_usage(effp) : SOX_SUCCESS;
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  if (!((p -> num_repeats) != 0U)) 
    return 32;
  if (!((p -> tmp_file = lsx_tmpfile()) != 0)) {
    ((( *sox_get_globals()).subsystem = "repeat.c") , lsx_fail_impl("can\'t create temporary file: %s",strerror( *__errno_location())));
    return SOX_EOF;
  }
  p -> num_samples = (p -> remaining_samples = 0);
  p -> remaining_repeats = (p -> num_repeats);
  effp -> out_signal.length = ((effp -> in_signal.length != ((sox_uint64_t )(-1)))?(effp -> in_signal.length * ((p -> num_repeats) + 1)) : ((sox_uint64_t )(-1)));
  return SOX_SUCCESS;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t len = ( *isamp <=  *osamp)? *isamp :  *osamp;
  memcpy(obuf,ibuf,(len * sizeof(( *obuf))));
  if (fwrite(ibuf,(sizeof(( *ibuf))),len,(p -> tmp_file)) != len) {
    ((( *sox_get_globals()).subsystem = "repeat.c") , lsx_fail_impl("error writing temporary file: %s",strerror( *__errno_location())));
    return SOX_EOF;
  }
  p -> num_samples += len;
   *isamp = ( *osamp = len);
  return SOX_SUCCESS;
}

static int drain(sox_effect_t *effp,sox_sample_t *obuf,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t odone = 0;
  size_t n;
   *osamp -= ( *osamp % effp -> in_signal.channels);
  while((((p -> remaining_samples) != 0UL) || ((p -> remaining_repeats) != 0U)) && (odone <  *osamp)){
    if (!((p -> remaining_samples) != 0UL)) {
      p -> remaining_samples = (p -> num_samples);
      --p -> remaining_repeats;
      rewind((p -> tmp_file));
    }
    n = (((p -> remaining_samples) <= ( *osamp - odone))?(p -> remaining_samples) : ( *osamp - odone));
    if (fread((obuf + odone),(sizeof(( *obuf))),n,(p -> tmp_file)) != n) {
      ((( *sox_get_globals()).subsystem = "repeat.c") , lsx_fail_impl("error reading temporary file: %s",strerror( *__errno_location())));
      return SOX_EOF;
    }
    p -> remaining_samples -= n;
    odone += n;
  }
   *osamp = odone;
  return (((p -> remaining_samples) != 0UL) || ((p -> remaining_repeats) != 0U))?SOX_SUCCESS : SOX_EOF;
}

static int stop(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
/* auto-deleted by lsx_tmpfile */
  fclose((p -> tmp_file));
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_repeat_effect_fn()
{
  static sox_effect_handler_t effect = {("repeat"), ("[count (1)]"), ((16 | 8 | 256)), (create), (start), (flow), (drain), (stop), ((sox_effect_handler_kill )((void *)0)), ((sizeof(priv_t )))};
  return (&effect);
}
