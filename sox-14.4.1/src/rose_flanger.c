/* libSoX effect: Stereo Flanger   (c) 2006 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* TODO: Slide in the delay at the start? */
#include "sox_i.h"
#include <string.h>
typedef enum __unnamed_enum___F0_L23_C9_INTERP_LINEAR__COMMA__INTERP_QUADRATIC {INTERP_LINEAR,INTERP_QUADRATIC}interp_t;
#define MAX_CHANNELS 4
typedef struct __unnamed_class___F0_L27_C9_unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__delay_min__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__delay_depth__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__feedback_gain__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__delay_gain__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__speed__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_lsx_wave_tL126R__typedef_declaration_variable_name_unknown_scope_and_name__scope__wave_shape__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__channel_phase__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_interp_tL127R__typedef_declaration_variable_name_unknown_scope_and_name__scope__interpolation__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab___Pb__d__Pe___index_4_Ae__variable_name_unknown_scope_and_name__scope__delay_bufs__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__delay_buf_length__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__delay_buf_pos__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_d_index_4_Ae__variable_name_unknown_scope_and_name__scope__delay_last__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__f__Pe___variable_name_unknown_scope_and_name__scope__lfo__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__lfo_length__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__lfo_pos__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__in_gain {
/* Parameters */
double delay_min;
double delay_depth;
double feedback_gain;
double delay_gain;
double speed;
lsx_wave_t wave_shape;
double channel_phase;
interp_t interpolation;
/* Delay buffers */
double *delay_bufs[4UL];
size_t delay_buf_length;
size_t delay_buf_pos;
double delay_last[4UL];
/* Low Frequency Oscillator */
float *lfo;
size_t lfo_length;
size_t lfo_pos;
/* Balancing */
double in_gain;}priv_t;
static const lsx_enum_item interp_enum[] = {{("LINEAR"), (INTERP_LINEAR)}, {("QUADRATIC"), (INTERP_QUADRATIC)}, {(0), (0)}};

static int getopts(sox_effect_t *effp,int argc,char *argv[])
{
  priv_t *p = (priv_t *)(effp -> priv);
  (--argc , ++argv);
/* Set non-zero defaults: */
  p -> delay_depth = 2;
  p -> delay_gain = 71;
  p -> speed = 0.5;
  p -> channel_phase = 25;
{
/* break-able block */
    do {{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 0) || (d > 30)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "flanger.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","delay_min",((double )0),((double )30)));
            return lsx_usage(effp);
          }
          p -> delay_min = d;
          (--argc , ++argv);
        }
      }
{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 0) || (d > 10)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "flanger.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","delay_depth",((double )0),((double )10)));
            return lsx_usage(effp);
          }
          p -> delay_depth = d;
          (--argc , ++argv);
        }
      }
{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < (-95)) || (d > 95)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "flanger.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","feedback_gain",((double )(-95)),((double )95)));
            return lsx_usage(effp);
          }
          p -> feedback_gain = d;
          (--argc , ++argv);
        }
      }
{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 0) || (d > 100)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "flanger.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","delay_gain",((double )0),((double )100)));
            return lsx_usage(effp);
          }
          p -> delay_gain = d;
          (--argc , ++argv);
        }
      }
{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 0.1) || (d > 10)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "flanger.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","speed",((double )0.1),((double )10)));
            return lsx_usage(effp);
          }
          p -> speed = d;
          (--argc , ++argv);
        }
      }
{
        const lsx_enum_item *e;
        if (argc == 0) 
          break; 
        e = lsx_find_enum_text(( *argv),lsx_get_wave_enum(),0);
        if (e != ((const lsx_enum_item *)((void *)0))) {
          p -> wave_shape = (e -> value);
          (--argc , ++argv);
        }
      }
{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 0) || (d > 100)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "flanger.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","channel_phase",((double )0),((double )100)));
            return lsx_usage(effp);
          }
          p -> channel_phase = d;
          (--argc , ++argv);
        }
      }
{
        const lsx_enum_item *e;
        if (argc == 0) 
          break; 
        e = lsx_find_enum_text(( *argv),interp_enum,0);
        if (e != ((const lsx_enum_item *)((void *)0))) {
          p -> interpolation = (e -> value);
          (--argc , ++argv);
        }
      }
    }while (0);
  }
  if (argc != 0) 
    return lsx_usage(effp);
  ((( *sox_get_globals()).subsystem = "flanger.c") , lsx_report_impl("parameters:\ndelay = %gms\ndepth = %gms\nregen = %g%%\nwidth = %g%%\nspeed = %gHz\nshape = %s\nphase = %g%%\ninterp= %s",(p -> delay_min),(p -> delay_depth),(p -> feedback_gain),(p -> delay_gain),(p -> speed),lsx_get_wave_enum()[p -> wave_shape].text,(p -> channel_phase),interp_enum[p -> interpolation].text));
/* Scale to unity: */
  p -> feedback_gain /= 100;
  p -> delay_gain /= 100;
  p -> channel_phase /= 100;
  p -> delay_min /= 1000;
  p -> delay_depth /= 1000;
  return SOX_SUCCESS;
}

static int start(sox_effect_t *effp)
{
  priv_t *f = (priv_t *)(effp -> priv);
  int c;
  int channels = effp -> in_signal.channels;
  if (channels > 4) {
    ((( *sox_get_globals()).subsystem = "flanger.c") , lsx_fail_impl("Can not operate with more than %i channels",4));
    return SOX_EOF;
  }
/* Balance output: */
  f -> in_gain = (1 / (1 + (f -> delay_gain)));
  f -> delay_gain /= (1 + (f -> delay_gain));
/* Balance feedback loop: */
  f -> delay_gain *= (1 - fabs((f -> feedback_gain)));
  ((( *sox_get_globals()).subsystem = "flanger.c") , lsx_debug_impl("in_gain=%g feedback_gain=%g delay_gain=%g\n",(f -> in_gain),(f -> feedback_gain),(f -> delay_gain)));
/* Create the delay buffers, one for each channel: */
  f -> delay_buf_length = ((((f -> delay_min) + (f -> delay_depth)) * effp -> in_signal.rate) + 0.5);
/* Need 0 to n, i.e. n + 1. */
  ++f -> delay_buf_length;
/* Quadratic interpolator needs one more. */
  ++f -> delay_buf_length;
  for (c = 0; c < channels; ++c) 
    (f -> delay_bufs)[c] = (((((f -> delay_buf_length) * sizeof(( *(f -> delay_bufs)[0]))) != 0ULL)?memset(lsx_realloc(0,((f -> delay_buf_length) * sizeof(( *(f -> delay_bufs)[0])))),0,((f -> delay_buf_length) * sizeof(( *(f -> delay_bufs)[0])))) : ((void *)((void *)0))));
/* Create the LFO lookup table: */
  f -> lfo_length = (effp -> in_signal.rate / (f -> speed));
  f -> lfo = (((((f -> lfo_length) * sizeof(( *(f -> lfo)))) != 0ULL)?memset(lsx_realloc(0,((f -> lfo_length) * sizeof(( *(f -> lfo))))),0,((f -> lfo_length) * sizeof(( *(f -> lfo))))) : ((void *)((void *)0))));
  lsx_generate_wave_table((f -> wave_shape),SOX_FLOAT,(f -> lfo),(f -> lfo_length),floor((((f -> delay_min) * effp -> in_signal.rate) + 0.5)),((f -> delay_buf_length) - 2.),3 * 1.57079632679489661923);
/* Start the sweep at minimum delay (for mono at least) */
  ((( *sox_get_globals()).subsystem = "flanger.c") , lsx_debug_impl("delay_buf_length=%lu lfo_length=%lu\n",(f -> delay_buf_length),(f -> lfo_length)));
  return SOX_SUCCESS;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *f = (priv_t *)(effp -> priv);
  int c;
  int channels = effp -> in_signal.channels;
  size_t len = (((( *isamp >  *osamp)? *osamp :  *isamp)) / channels);
   *isamp = ( *osamp = (len * channels));
  while(len-- != 0UL){
    f -> delay_buf_pos = ((((f -> delay_buf_pos) + (f -> delay_buf_length)) - 1) % (f -> delay_buf_length));
    for (c = 0; c < channels; ++c) {
      double delayed_0;
      double delayed_1;
      double delayed;
      double in;
      double out;
      size_t channel_phase = (((c * (f -> lfo_length)) * (f -> channel_phase)) + 0.5);
      double delay = (f -> lfo)[((f -> lfo_pos) + channel_phase) % (f -> lfo_length)];
      double frac_delay = modf(delay,&delay);
      size_t int_delay = (size_t )delay;
      in = ( *(ibuf++));
      (f -> delay_bufs)[c][f -> delay_buf_pos] = (in + ((f -> delay_last)[c] * (f -> feedback_gain)));
      delayed_0 = (f -> delay_bufs)[c][((f -> delay_buf_pos) + int_delay++) % (f -> delay_buf_length)];
      delayed_1 = (f -> delay_bufs)[c][((f -> delay_buf_pos) + int_delay++) % (f -> delay_buf_length)];
      if ((f -> interpolation) == INTERP_LINEAR) 
        delayed = (delayed_0 + ((delayed_1 - delayed_0) * frac_delay));
      else 
/* if (f->interpolation == INTERP_QUADRATIC) */
{
        double a;
        double b;
        double delayed_2 = (f -> delay_bufs)[c][((f -> delay_buf_pos) + int_delay++) % (f -> delay_buf_length)];
        delayed_2 -= delayed_0;
        delayed_1 -= delayed_0;
        a = ((delayed_2 * 0.5) - delayed_1);
        b = ((delayed_1 * 2) - (delayed_2 * 0.5));
        delayed = (delayed_0 + (((a * frac_delay) + b) * frac_delay));
      }
      (f -> delay_last)[c] = delayed;
      out = ((in * (f -> in_gain)) + (delayed * (f -> delay_gain)));
       *(obuf++) = (((out < 0)?(((out <= ((sox_sample_t )(1 << 32 - 1)) - 0.5)?((++effp -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (out - 0.5))) : (((out >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 0.5)?((++effp -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : (out + 0.5)))));
    }
    f -> lfo_pos = (((f -> lfo_pos) + 1) % (f -> lfo_length));
  }
  return SOX_SUCCESS;
}

static int stop(sox_effect_t *effp)
{
  priv_t *f = (priv_t *)(effp -> priv);
  int c;
  int channels = effp -> in_signal.channels;
  for (c = 0; c < channels; ++c) 
    free((f -> delay_bufs)[c]);
  free((f -> lfo));
  memset(f,0,(sizeof(( *f))));
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_flanger_effect_fn()
{
  static sox_effect_handler_t handler = {("flanger"), ((const char *)((void *)0)), (16), (getopts), (start), (flow), ((sox_effect_handler_drain )((void *)0)), (stop), ((sox_effect_handler_kill )((void *)0)), ((sizeof(priv_t )))};
  static const char *lines[] = {("[delay depth regen width speed shape phase interp]"), ("                  ."), ("                 /|regen"), ("                / |"), ("            +--(  |------------+"), ("            |   \\ |            |   ."), ("           _V_   \\|  _______   |   |\\ width   ___"), ("          |   |   \' |       |  |   | \\       |   |"), ("      +-->| + |---->| DELAY |--+-->|  )----->|   |"), ("      |   |___|     |_______|      | /       |   |"), ("      |           delay : depth    |/        |   |"), ("  In  |                 : interp   \'         |   | Out"), ("  --->+               __:__                  | + |--->"), ("      |              |     |speed            |   |"), ("      |              |  ~  |shape            |   |"), ("      |              |_____|phase            |   |"), ("      +------------------------------------->|   |"), ("                                             |___|"), ("       RANGE DEFAULT DESCRIPTION"), ("delay   0 30    0    base delay in milliseconds"), ("depth   0 10    2    added swept delay in milliseconds"), ("regen -95 +95   0    percentage regeneration (delayed signal feedback)"), ("width   0 100   71   percentage of delayed signal mixed with original"), ("speed  0.1 10  0.5   sweeps per second (Hz) "), ("shape    --    sin   swept wave shape: sine|triangle"), ("phase   0 100   25   swept wave percentage phase-shift for multi-channel"), ("                     (e.g. stereo) flange; 0 = 100 = same phase on each channel"), ("interp   --    lin   delay-line interpolation: linear|quadratic")};
  static char *usage;
  handler.usage = (lsx_usage_lines(&usage,lines,(sizeof(lines) / sizeof(lines[0]))));
  return (&handler);
}
