/* MP3 support for SoX
 *
 * Uses libmad for MP3 decoding
 * libmp3lame for MP3 encoding
 * and libtwolame for MP2 encoding
 *
 * Written by Fabrizio Gennari <fabrizio.ge@tiscali.it>
 *
 * The decoding part is based on the decoder-tutorial program madlld
 * written by Bertrand Petit <madlld@phoe.fmug.org>,
 */
#include "sox_i.h"
#include <string.h>
#if defined(HAVE_LAME_LAME_H) || defined(HAVE_LAME_H) || defined(DL_LAME)
#define HAVE_LAME 1
#endif
#if defined(HAVE_TWOLAME_H) || defined(DL_TWOLAME)
  #define HAVE_TWOLAME 1
#endif
#if defined(HAVE_MAD_H) || defined(HAVE_LAME) || defined(HAVE_TWOLAME)
#ifdef HAVE_MAD_H
#include <mad.h>
#endif
#if defined(HAVE_LAME_LAME_H)
#include <lame/lame.h>
#elif defined(HAVE_LAME_H)
#include <lame.h>
#elif defined(DL_LAME)
#endif
#if defined(HAVE_ID3TAG) && (defined(HAVE_IO_H) || defined(HAVE_UNISTD_H))
#define USING_ID3TAG 1
#endif
#ifdef USING_ID3TAG
  #include <id3tag.h>
#if defined(HAVE_UNISTD_H)
  #include <unistd.h>
#elif defined(HAVE_IO_H)
  #include <io.h>
#endif
#else
  #define ID3_TAG_FLAG_FOOTERPRESENT 0x10
#endif
#ifdef HAVE_TWOLAME_H
  #include <twolame.h>
#endif
#ifndef HAVE_LIBLTDL
  #undef DL_LAME
  #undef DL_MAD
#endif
/* Under Windows, importing data from DLLs is a dicey proposition. This is true
 * when using dlopen, but also true if linking directly against the DLL if the
 * header does not mark the data as __declspec(dllexport), which mad.h does not.
 * Sidestep the issue by defining our own mad_timer_zero. This is needed because
 * mad_timer_zero is used in some of the mad.h macros.
 */
#ifdef HAVE_MAD_H
#define mad_timer_zero mad_timer_zero_stub
static const mad_timer_t mad_timer_zero_stub = {(0), (0)};
#endif
#define MAXFRAMESIZE 2880
#define ID3PADDING 128
/* LAME takes float values as input. */
#define MP3_LAME_PRECISION 24
/* MAD returns values with MAD_F_FRACBITS (28) bits of precision, though it's
   not certain that all of them are meaningful. Default to 16 bits to
   align with most users expectation of output file should be 16 bits. */
#define MP3_MAD_PRECISION  16
static const char *const mad_library_names[] = {((const char *)((void *)0))
#ifdef DL_MAD
#endif
};
#ifdef DL_MAD
  #define MAD_FUNC LSX_DLENTRY_DYNAMIC
#else
  #define MAD_FUNC LSX_DLENTRY_STATIC
#endif
#define MAD_FUNC_ENTRIES(f,x) \
  MAD_FUNC(f,x, void, mad_stream_buffer, (struct mad_stream *, unsigned char const *, unsigned long)) \
  MAD_FUNC(f,x, void, mad_stream_skip, (struct mad_stream *, unsigned long)) \
  MAD_FUNC(f,x, int, mad_stream_sync, (struct mad_stream *)) \
  MAD_FUNC(f,x, void, mad_stream_init, (struct mad_stream *)) \
  MAD_FUNC(f,x, void, mad_frame_init, (struct mad_frame *)) \
  MAD_FUNC(f,x, void, mad_synth_init, (struct mad_synth *)) \
  MAD_FUNC(f,x, int, mad_frame_decode, (struct mad_frame *, struct mad_stream *)) \
  MAD_FUNC(f,x, void, mad_timer_add, (mad_timer_t *, mad_timer_t)) \
  MAD_FUNC(f,x, void, mad_synth_frame, (struct mad_synth *, struct mad_frame const *)) \
  MAD_FUNC(f,x, char const *, mad_stream_errorstr, (struct mad_stream const *)) \
  MAD_FUNC(f,x, void, mad_frame_finish, (struct mad_frame *)) \
  MAD_FUNC(f,x, void, mad_stream_finish, (struct mad_stream *)) \
  MAD_FUNC(f,x, unsigned long, mad_bit_read, (struct mad_bitptr *, unsigned int)) \
  MAD_FUNC(f,x, int, mad_header_decode, (struct mad_header *, struct mad_stream *)) \
  MAD_FUNC(f,x, void, mad_header_init, (struct mad_header *)) \
  MAD_FUNC(f,x, signed long, mad_timer_count, (mad_timer_t, enum mad_units)) \
  MAD_FUNC(f,x, void, mad_timer_multiply, (mad_timer_t *, signed long))
static const char *const lame_library_names[] = {((const char *)((void *)0))
#ifdef DL_LAME
#endif
};
#ifdef DL_LAME
/* Expected to be present in all builds of LAME. */
  #define LAME_FUNC           LSX_DLENTRY_DYNAMIC
/* id3tag support is an optional component of LAME. Use if available. */
  #define LAME_FUNC_ID3       LSX_DLENTRY_STUB
#else /* DL_LAME */
/* Expected to be present in all builds of LAME. */
  #define LAME_FUNC           LSX_DLENTRY_STATIC
/* id3tag support is an optional component of LAME. Use if available. */
  #ifdef HAVE_LAME_ID3TAG
    #define LAME_FUNC_ID3     LSX_DLENTRY_STATIC
  #else
    #define LAME_FUNC_ID3     LSX_DLENTRY_STUB
  #endif
#endif /* DL_LAME */
#define LAME_FUNC_ENTRIES(f,x) \
  LAME_FUNC(f,x, lame_global_flags*, lame_init, (void)) \
  LAME_FUNC(f,x, int, lame_set_errorf, (lame_global_flags *, void (*)(const char *, va_list))) \
  LAME_FUNC(f,x, int, lame_set_debugf, (lame_global_flags *, void (*)(const char *, va_list))) \
  LAME_FUNC(f,x, int, lame_set_msgf, (lame_global_flags *, void (*)(const char *, va_list))) \
  LAME_FUNC(f,x, int, lame_set_num_samples, (lame_global_flags *, unsigned long)) \
  LAME_FUNC(f,x, int, lame_get_num_channels, (const lame_global_flags *)) \
  LAME_FUNC(f,x, int, lame_set_num_channels, (lame_global_flags *, int)) \
  LAME_FUNC(f,x, int, lame_set_in_samplerate, (lame_global_flags *, int)) \
  LAME_FUNC(f,x, int, lame_set_out_samplerate, (lame_global_flags *, int)) \
  LAME_FUNC(f,x, int, lame_set_bWriteVbrTag, (lame_global_flags *, int)) \
  LAME_FUNC(f,x, int, lame_set_brate, (lame_global_flags *, int)) \
  LAME_FUNC(f,x, int, lame_set_quality, (lame_global_flags *, int)) \
  LAME_FUNC(f,x, vbr_mode, lame_get_VBR, (const lame_global_flags *)) \
  LAME_FUNC(f,x, int, lame_set_VBR, (lame_global_flags *, vbr_mode)) \
  LAME_FUNC(f,x, int, lame_set_VBR_q, (lame_global_flags *, int)) \
  LAME_FUNC(f,x, int, lame_init_params, (lame_global_flags *)) \
  LAME_FUNC(f,x, int, lame_encode_buffer_float, (lame_global_flags *, const float[], const float[], const int, unsigned char *, const int)) \
  LAME_FUNC(f,x, int, lame_encode_flush, (lame_global_flags *, unsigned char *, int)) \
  LAME_FUNC(f,x, int, lame_close, (lame_global_flags *)) \
  LAME_FUNC(f,x, size_t, lame_get_lametag_frame, (const lame_global_flags *, unsigned char*, size_t)) \
  LAME_FUNC_ID3(f,x, void, id3tag_init, (lame_global_flags *)) \
  LAME_FUNC_ID3(f,x, void, id3tag_set_title, (lame_global_flags *, const char* title)) \
  LAME_FUNC_ID3(f,x, void, id3tag_set_artist, (lame_global_flags *, const char* artist)) \
  LAME_FUNC_ID3(f,x, void, id3tag_set_album, (lame_global_flags *, const char* album)) \
  LAME_FUNC_ID3(f,x, void, id3tag_set_year, (lame_global_flags *, const char* year)) \
  LAME_FUNC_ID3(f,x, void, id3tag_set_comment, (lame_global_flags *, const char* comment)) \
  LAME_FUNC_ID3(f,x, int, id3tag_set_track, (lame_global_flags *, const char* track)) \
  LAME_FUNC_ID3(f,x, int, id3tag_set_genre, (lame_global_flags *, const char* genre)) \
  LAME_FUNC_ID3(f,x, size_t, id3tag_set_pad, (lame_global_flags *, size_t)) \
  LAME_FUNC_ID3(f,x, size_t, lame_get_id3v2_tag, (lame_global_flags *, unsigned char*, size_t)) \
  LAME_FUNC_ID3(f,x, int, id3tag_set_fieldvalue, (lame_global_flags *, const char *))
static const char *const twolame_library_names[] = {((const char *)((void *)0))
#ifdef DL_TWOLAME
#endif
};
#ifdef DL_TWOLAME
  #define TWOLAME_FUNC LSX_DLENTRY_DYNAMIC
#else
  #define TWOLAME_FUNC LSX_DLENTRY_STATIC
#endif
#define TWOLAME_FUNC_ENTRIES(f,x) \
  TWOLAME_FUNC(f,x, twolame_options*, twolame_init, (void)) \
  TWOLAME_FUNC(f,x, int, twolame_get_num_channels, (twolame_options*)) \
  TWOLAME_FUNC(f,x, int, twolame_set_num_channels, (twolame_options*, int)) \
  TWOLAME_FUNC(f,x, int, twolame_set_in_samplerate, (twolame_options *, int)) \
  TWOLAME_FUNC(f,x, int, twolame_set_out_samplerate, (twolame_options *, int)) \
  TWOLAME_FUNC(f,x, int, twolame_set_brate, (twolame_options *, int)) \
  TWOLAME_FUNC(f,x, int, twolame_init_params, (twolame_options *)) \
  TWOLAME_FUNC(f,x, int, twolame_encode_buffer_float32_interleaved, (twolame_options *, const float [], int, unsigned char *, int)) \
  TWOLAME_FUNC(f,x, int, twolame_encode_flush, (twolame_options *, unsigned char *, int)) \
  TWOLAME_FUNC(f,x, void, twolame_close, (twolame_options **))
/* Private data */
typedef struct mp3_priv_t {
unsigned char *mp3_buffer;
size_t mp3_buffer_size;
#ifdef HAVE_MAD_H
struct mad_stream Stream;
struct mad_frame Frame;
struct mad_synth Synth;
mad_timer_t Timer;
ptrdiff_t cursamp;
size_t FrameCount;
void (*mad_stream_buffer)(struct mad_stream *, const unsigned char *, unsigned long );
void (*mad_stream_skip)(struct mad_stream *, unsigned long );
int (*mad_stream_sync)(struct mad_stream *);
void (*mad_stream_init)(struct mad_stream *);
void (*mad_frame_init)(struct mad_frame *);
void (*mad_synth_init)(struct mad_synth *);
int (*mad_frame_decode)(struct mad_frame *, struct mad_stream *);
void (*mad_timer_add)(mad_timer_t *, mad_timer_t );
void (*mad_synth_frame)(struct mad_synth *, const struct mad_frame *);
const char *(*mad_stream_errorstr)(const struct mad_stream *);
void (*mad_frame_finish)(struct mad_frame *);
void (*mad_stream_finish)(struct mad_stream *);
unsigned long (*mad_bit_read)(struct mad_bitptr *, unsigned int );
int (*mad_header_decode)(struct mad_header *, struct mad_stream *);
void (*mad_header_init)(struct mad_header *);
long (*mad_timer_count)(mad_timer_t , mad_units );
void (*mad_timer_multiply)(mad_timer_t *, long );
lsx_dlhandle mad_dl;
#endif /*HAVE_MAD_H*/
#if defined(HAVE_LAME) || defined(HAVE_TWOLAME)
#endif
#ifdef HAVE_LAME
#endif
#ifdef HAVE_TWOLAME
#endif
}priv_t;
#ifdef HAVE_MAD_H
/* This function merges the functions tagtype() and id3_tag_query()
   from MAD's libid3tag, so we don't have to link to it
   Returns 0 if the frame is not an ID3 tag, tag length if it is */

static int tagtype(const unsigned char *data,size_t length)
{
  if ((((length >= 3) && (data[0] == 'T')) && (data[1] == 'A')) && (data[2] == 'G')) {
/* ID3V1 */
    return 128;
  }
  if ((((((((length >= 10) && (((data[0] == 'I') && (data[1] == 'D')) && (data[2] == '3'))) && (data[3] < 0xff)) && (data[4] < 0xff)) && (data[6] < 128)) && (data[7] < 128)) && (data[8] < 128)) && (data[9] < 128)) 
/* ID3V2 */
{
    unsigned char flags;
    unsigned int size;
    flags = data[5];
    size = ((((10 + (data[6] << 21)) + (data[7] << 14)) + (data[8] << 7)) + data[9]);
    if ((flags & 16) != 0) 
      size += 10;
/* Consume padding */
    for (; (size < length) && !(data[size] != 0); ++size) ;
    return size;
  }
  return 0;
}
#endif /*HAVE_MAD_H*/
#include "mp3-util.h"
#ifdef HAVE_MAD_H
/*
 * (Re)fill the stream buffer that is to be decoded.  If any data
 * still exists in the buffer then they are first shifted to be
 * front of the stream buffer.
 */

static int sox_mp3_input(sox_format_t *ft)
{
  priv_t *p = (priv_t *)(ft -> priv);
  size_t bytes_read;
  size_t remaining;
  remaining = (p -> Stream.bufend - p -> Stream.next_frame);
/* libmad does not consume all the buffer it's given. Some
     * data, part of a truncated frame, is left unused at the
     * end of the buffer. That data must be put back at the
     * beginning of the buffer and taken in account for
     * refilling the buffer. This means that the input buffer
     * must be large enough to hold a complete frame at the
     * highest observable bit-rate (currently 448 kb/s).
     * TODO: Is 2016 bytes the size of the largest frame?
     * (448000*(1152/32000))/8
     */
  memmove((p -> mp3_buffer),p -> Stream.next_frame,remaining);
  bytes_read = lsx_readbuf(ft,((p -> mp3_buffer) + remaining),((p -> mp3_buffer_size) - remaining));
  if (bytes_read == 0) {
    return SOX_EOF;
  }
  ( *(p -> mad_stream_buffer))(&p -> Stream,(p -> mp3_buffer),(bytes_read + remaining));
  p -> Stream.error = 0;
  return SOX_SUCCESS;
}
/* Attempts to read an ID3 tag at the current location in stream and
 * consume it all.  Returns SOX_EOF if no tag is found.  Its up to
 * caller to recover.
 * */

static int sox_mp3_inputtag(sox_format_t *ft)
{
  priv_t *p = (priv_t *)(ft -> priv);
  int rc = SOX_EOF;
  size_t remaining;
  size_t tagsize;
/* FIXME: This needs some more work if we are to ever
     * look at the ID3 frame.  This is because the Stream
     * may not be able to hold the complete ID3 frame.
     * We should consume the whole frame inside tagtype()
     * instead of outside of tagframe().  That would support
     * recovering when Stream contains less then 8-bytes (header)
     * and also when ID3v2 is bigger then Stream buffer size.
     * Need to pass in stream so that buffer can be
     * consumed as well as letting additional data to be
     * read in.
     */
  remaining = (p -> Stream.bufend - p -> Stream.next_frame);
  if ((tagsize = (tagtype(p -> Stream.this_frame,remaining))) != 0UL) {
    ( *(p -> mad_stream_skip))(&p -> Stream,tagsize);
    rc = SOX_SUCCESS;
  }
/* We know that a valid frame hasn't been found yet
     * so help libmad out and go back into frame seek mode.
     * This is true whether an ID3 tag was found or not.
     */
  ( *(p -> mad_stream_sync))(&p -> Stream);
  return rc;
}

static int startread(sox_format_t *ft)
{
  priv_t *p = (priv_t *)(ft -> priv);
  size_t ReadSize;
  sox_bool ignore_length = (ft -> signal.length == ((sox_uint64_t )(-2)));
  int open_library_result;
  do {
    lsx_dlfunction_info lsx_dlfunction_open_library_infos[] = {{("mad_stream_buffer"), ((lsx_dlptr )mad_stream_buffer), ((lsx_dlptr )((lsx_dlptr )((void *)0)))}, {("mad_stream_skip"), ((lsx_dlptr )mad_stream_skip), ((lsx_dlptr )((lsx_dlptr )((void *)0)))}, {("mad_stream_sync"), ((lsx_dlptr )mad_stream_sync), ((lsx_dlptr )((lsx_dlptr )((void *)0)))}, {("mad_stream_init"), ((lsx_dlptr )mad_stream_init), ((lsx_dlptr )((lsx_dlptr )((void *)0)))}, {("mad_frame_init"), ((lsx_dlptr )mad_frame_init), ((lsx_dlptr )((lsx_dlptr )((void *)0)))}, {("mad_synth_init"), ((lsx_dlptr )mad_synth_init), ((lsx_dlptr )((lsx_dlptr )((void *)0)))}, {("mad_frame_decode"), ((lsx_dlptr )mad_frame_decode), ((lsx_dlptr )((lsx_dlptr )((void *)0)))}, {("mad_timer_add"), ((lsx_dlptr )mad_timer_add), ((lsx_dlptr )((lsx_dlptr )((void *)0)))}, {("mad_synth_frame"), ((lsx_dlptr )mad_synth_frame), ((lsx_dlptr )((lsx_dlptr )((void *)0)))}, {("mad_stream_errorstr"), ((lsx_dlptr )mad_stream_errorstr), ((lsx_dlptr )((lsx_dlptr )((void *)0)))}, {("mad_frame_finish"), ((lsx_dlptr )mad_frame_finish), ((lsx_dlptr )((lsx_dlptr )((void *)0)))}, {("mad_stream_finish"), ((lsx_dlptr )mad_stream_finish), ((lsx_dlptr )((lsx_dlptr )((void *)0)))}, {("mad_bit_read"), ((lsx_dlptr )mad_bit_read), ((lsx_dlptr )((lsx_dlptr )((void *)0)))}, {("mad_header_decode"), ((lsx_dlptr )mad_header_decode), ((lsx_dlptr )((lsx_dlptr )((void *)0)))}, {("mad_header_init"), ((lsx_dlptr )mad_header_init), ((lsx_dlptr )((lsx_dlptr )((void *)0)))}, {("mad_timer_count"), ((lsx_dlptr )mad_timer_count), ((lsx_dlptr )((lsx_dlptr )((void *)0)))}, {("mad_timer_multiply"), ((lsx_dlptr )mad_timer_multiply), ((lsx_dlptr )((lsx_dlptr )((void *)0)))}, {((const char *)((void *)0)), ((lsx_dlptr )((void *)0)), ((lsx_dlptr )((void *)0))}};
    int lsx_dlfunction_open_library_index = 0;
    lsx_dlptr lsx_dlfunction_open_library_funcs[18UL];
    open_library_result = lsx_open_dllibrary(1,"MAD decoder library",mad_library_names,lsx_dlfunction_open_library_infos,lsx_dlfunction_open_library_funcs,&p -> mad_dl);
    p -> mad_stream_buffer = ((void (*)(struct mad_stream *, const unsigned char *, unsigned long ))lsx_dlfunction_open_library_funcs[lsx_dlfunction_open_library_index++]);
    p -> mad_stream_skip = ((void (*)(struct mad_stream *, unsigned long ))lsx_dlfunction_open_library_funcs[lsx_dlfunction_open_library_index++]);
    p -> mad_stream_sync = ((int (*)(struct mad_stream *))lsx_dlfunction_open_library_funcs[lsx_dlfunction_open_library_index++]);
    p -> mad_stream_init = ((void (*)(struct mad_stream *))lsx_dlfunction_open_library_funcs[lsx_dlfunction_open_library_index++]);
    p -> mad_frame_init = ((void (*)(struct mad_frame *))lsx_dlfunction_open_library_funcs[lsx_dlfunction_open_library_index++]);
    p -> mad_synth_init = ((void (*)(struct mad_synth *))lsx_dlfunction_open_library_funcs[lsx_dlfunction_open_library_index++]);
    p -> mad_frame_decode = ((int (*)(struct mad_frame *, struct mad_stream *))lsx_dlfunction_open_library_funcs[lsx_dlfunction_open_library_index++]);
    p -> mad_timer_add = ((void (*)(mad_timer_t *, mad_timer_t ))lsx_dlfunction_open_library_funcs[lsx_dlfunction_open_library_index++]);
    p -> mad_synth_frame = ((void (*)(struct mad_synth *, const struct mad_frame *))lsx_dlfunction_open_library_funcs[lsx_dlfunction_open_library_index++]);
    p -> mad_stream_errorstr = ((const char *(*)(const struct mad_stream *))lsx_dlfunction_open_library_funcs[lsx_dlfunction_open_library_index++]);
    p -> mad_frame_finish = ((void (*)(struct mad_frame *))lsx_dlfunction_open_library_funcs[lsx_dlfunction_open_library_index++]);
    p -> mad_stream_finish = ((void (*)(struct mad_stream *))lsx_dlfunction_open_library_funcs[lsx_dlfunction_open_library_index++]);
    p -> mad_bit_read = ((unsigned long (*)(struct mad_bitptr *, unsigned int ))lsx_dlfunction_open_library_funcs[lsx_dlfunction_open_library_index++]);
    p -> mad_header_decode = ((int (*)(struct mad_header *, struct mad_stream *))lsx_dlfunction_open_library_funcs[lsx_dlfunction_open_library_index++]);
    p -> mad_header_init = ((void (*)(struct mad_header *))lsx_dlfunction_open_library_funcs[lsx_dlfunction_open_library_index++]);
    p -> mad_timer_count = ((long (*)(mad_timer_t , mad_units ))lsx_dlfunction_open_library_funcs[lsx_dlfunction_open_library_index++]);
    p -> mad_timer_multiply = ((void (*)(mad_timer_t *, long ))lsx_dlfunction_open_library_funcs[lsx_dlfunction_open_library_index++]);
  }while (0);
  if (open_library_result != 0) 
    return SOX_EOF;
  p -> mp3_buffer_size = ( *sox_get_globals()).bufsiz;
  p -> mp3_buffer = (lsx_realloc(0,(p -> mp3_buffer_size)));
  ft -> signal.length = 0;
  if ((ft -> seekable) != 0U) {
#ifdef USING_ID3TAG
#endif
    if (!(ignore_length != 0U)) 
      ft -> signal.length = mp3_duration_ms(ft);
  }
  ( *(p -> mad_stream_init))(&p -> Stream);
  ( *(p -> mad_frame_init))(&p -> Frame);
  ( *(p -> mad_synth_init))(&p -> Synth);
  p -> Timer = mad_timer_zero_stub;
  ft -> encoding.encoding = SOX_ENCODING_MP3;
/* Decode at least one valid frame to find out the input
   * format.  The decoded frame will be saved off so that it
   * can be processed later.
   */
  ReadSize = lsx_readbuf(ft,(p -> mp3_buffer),(p -> mp3_buffer_size));
  if ((ReadSize != (p -> mp3_buffer_size)) && (ferror(((FILE *)(ft -> fp))) != 0)) 
    return SOX_EOF;
  ( *(p -> mad_stream_buffer))(&p -> Stream,(p -> mp3_buffer),ReadSize);
/* Find a valid frame before starting up.  This makes sure
   * that we have a valid MP3 and also skips past ID3v2 tags
   * at the beginning of the audio file.
   */
  p -> Stream.error = 0;
  while(( *(p -> mad_frame_decode))(&p -> Frame,&p -> Stream) != 0){
/* check whether input buffer needs a refill */
    if (p -> Stream.error == MAD_ERROR_BUFLEN) {
      if (sox_mp3_input(ft) == SOX_EOF) 
        return SOX_EOF;
      continue; 
    }
/* Consume any ID3 tags */
    sox_mp3_inputtag(ft);
/* FIXME: We should probably detect when we've read
       * a bunch of non-ID3 data and still haven't found a
       * frame.  In that case we can abort early without
       * scanning the whole file.
       */
    p -> Stream.error = 0;
  }
  if (p -> Stream.error != 0U) {
    lsx_fail_errno(ft,SOX_EOF,"No valid MP3 frame found");
    return SOX_EOF;
  }
  switch(p -> Frame.header.mode){
    case MAD_MODE_SINGLE_CHANNEL:
{
    }
    case MAD_MODE_DUAL_CHANNEL:
{
    }
    case MAD_MODE_JOINT_STEREO:
{
    }
    case MAD_MODE_STEREO:
{
      ft -> signal.channels = (((p -> Frame.header.mode != 0U)?2 : 1));
      break; 
    }
    default:
{
      lsx_fail_errno(ft,SOX_EFMT,"Cannot determine number of channels");
      return SOX_EOF;
    }
  }
  p -> FrameCount = 1;
  ( *(p -> mad_timer_add))(&p -> Timer,p -> Frame.header.duration);
  ( *(p -> mad_synth_frame))(&p -> Synth,(&p -> Frame));
  ft -> signal.precision = 16;
  ft -> signal.rate = p -> Synth.pcm.samplerate;
  if (ignore_length != 0U) 
    ft -> signal.length = 0;
  else {
    ft -> signal.length = ((uint64_t )(((ft -> signal.length * .001) * ft -> signal.rate) + .5));
/* Keep separate from line above! */
    ft -> signal.length *= ft -> signal.channels;
  }
  p -> cursamp = 0;
  return SOX_SUCCESS;
}
/*
 * Read up to len samples from p->Synth
 * If needed, read some more MP3 data, decode them and synth them
 * Place in buf[].
 * Return number of samples read.
 */

static size_t sox_mp3read(sox_format_t *ft,sox_sample_t *buf,size_t len)
{
  priv_t *p = (priv_t *)(ft -> priv);
  size_t donow;
  size_t i;
  size_t done = 0;
  mad_fixed_t sample;
  size_t chan;
{
    do {{
        size_t x = ((p -> Synth.pcm.length - (p -> cursamp)) * ft -> signal.channels);
        donow = ((len <= x)?len : x);
        i = 0;
        while(i < donow){
          for (chan = 0; chan < ft -> signal.channels; chan++) {
            sample = p -> Synth.pcm.samples[chan][p -> cursamp];
            if (sample < (-((mad_fixed_t )0x10000000L))) 
              sample = -((mad_fixed_t )0x10000000L);
            else if (sample >= ((mad_fixed_t )0x10000000L)) 
              sample = (((mad_fixed_t )0x10000000L) - 1);
             *(buf++) = (sample << 32 - 1 - 28);
            i++;
          }
          p -> cursamp++;
        };
        len -= donow;
        done += donow;
        if (len == 0) 
          break; 
/* check whether input buffer needs a refill */
        if (p -> Stream.error == MAD_ERROR_BUFLEN) {
          if (sox_mp3_input(ft) == SOX_EOF) {
            ((( *sox_get_globals()).subsystem = "mp3.c") , lsx_debug_impl("sox_mp3_input EOF"));
            break; 
          }
        }
        if (( *(p -> mad_frame_decode))(&p -> Frame,&p -> Stream) != 0) {
          if ((p -> Stream.error & 0xff00) != 0U) {
            sox_mp3_inputtag(ft);
            continue; 
          }
          else {
            if (p -> Stream.error == MAD_ERROR_BUFLEN) 
              continue; 
            else {
              ((( *sox_get_globals()).subsystem = "mp3.c") , lsx_report_impl("unrecoverable frame level error (%s).",( *(p -> mad_stream_errorstr))((&p -> Stream))));
              break; 
            }
          }
        }
        p -> FrameCount++;
        ( *(p -> mad_timer_add))(&p -> Timer,p -> Frame.header.duration);
        ( *(p -> mad_synth_frame))(&p -> Synth,(&p -> Frame));
        p -> cursamp = 0;
      }
    }while (1);
  }
  return done;
}

static int stopread(sox_format_t *ft)
{
  priv_t *p = (priv_t *)(ft -> priv);;
  ( *(p -> mad_frame_finish))(&p -> Frame);
  ( *(p -> mad_stream_finish))(&p -> Stream);
  free((p -> mp3_buffer));
  lsx_close_dllibrary((p -> mad_dl));
  return SOX_SUCCESS;
}

static int sox_mp3seek(sox_format_t *ft,uint64_t offset)
{
  priv_t *p = (priv_t *)(ft -> priv);
  size_t initial_bitrate = p -> Frame.header.bitrate;
  size_t tagsize = 0;
  size_t consumed = 0;
/* Variable Bit Rate */
  sox_bool vbr = sox_false;
  sox_bool depadded = sox_false;
  uint64_t to_skip_samples = 0;
/* Reset all */
  rewind(((FILE *)(ft -> fp)));
  p -> Timer = mad_timer_zero_stub;
  p -> FrameCount = 0;
/* They where opened in startread */
;
  ( *(p -> mad_frame_finish))(&p -> Frame);
  ( *(p -> mad_stream_finish))(&p -> Stream);
  ( *(p -> mad_stream_init))(&p -> Stream);
  ( *(p -> mad_frame_init))(&p -> Frame);
  ( *(p -> mad_synth_init))(&p -> Synth);
  offset /= ft -> signal.channels;
  to_skip_samples = offset;
{
/* Read data from the MP3 file */
    while(1){
      int read;
      int padding = 0;
      size_t leftover = (p -> Stream.bufend - p -> Stream.next_frame);
      memcpy((p -> mp3_buffer),p -> Stream.this_frame,leftover);
      read = (fread(((p -> mp3_buffer) + leftover),((size_t )1),((p -> mp3_buffer_size) - leftover),((FILE *)(ft -> fp))));
      if (read <= 0) {
        ((( *sox_get_globals()).subsystem = "mp3.c") , lsx_debug_impl("seek failure. unexpected EOF (frames=%lu leftover=%lu)",(p -> FrameCount),leftover));
        break; 
      }
      for (; (!(depadded != 0U) && (padding < read)) && !((p -> mp3_buffer)[padding] != 0); ++padding) ;
      depadded = sox_true;
      ( *(p -> mad_stream_buffer))(&p -> Stream,((p -> mp3_buffer) + padding),((leftover + read) - padding));
{
/* Decode frame headers */
        while(1){{
            static unsigned short samples;
            p -> Stream.error = MAD_ERROR_NONE;
/* Not an audio frame */
            if (( *(p -> mad_header_decode))(&p -> Frame.header,&p -> Stream) == -1) {
              if (p -> Stream.error == MAD_ERROR_BUFLEN) 
/* Normal behaviour; get some more data from the file */
                break; 
              if (!((p -> Stream.error & 0xff00) != 0U)) {
                ((( *sox_get_globals()).subsystem = "mp3.c") , lsx_warn_impl("unrecoverable MAD error"));
                break; 
              }
              if (p -> Stream.error == MAD_ERROR_LOSTSYNC) {
                unsigned int available = (p -> Stream.bufend - p -> Stream.this_frame);
                tagsize = (tagtype(p -> Stream.this_frame,((size_t )available)));
/* It's some ID3 tags, so just skip */
                if (tagsize != 0UL) {
                  if (tagsize >= available) {
                    fseeko(((FILE *)(ft -> fp)),((off_t )(tagsize - available)),1);
                    depadded = sox_false;
                  }
                  ( *(p -> mad_stream_skip))(&p -> Stream,((tagsize <= available)?tagsize : available));
                }
                else 
                  ((( *sox_get_globals()).subsystem = "mp3.c") , lsx_warn_impl("MAD lost sync"));
              }
              else 
                ((( *sox_get_globals()).subsystem = "mp3.c") , lsx_warn_impl("recoverable MAD error"));
              continue; 
            }
            consumed += (p -> Stream.next_frame - p -> Stream.this_frame);
            vbr |= (p -> Frame.header.bitrate != initial_bitrate);
            samples = (32 * (((p -> Frame.header.layer == MAD_LAYER_I)?12 : ((((p -> Frame.header.layer == MAD_LAYER_III) && ((p -> Frame.header.flags & MAD_FLAG_LSF_EXT) != 0))?18 : 36)))));
            p -> FrameCount++;
            ( *(p -> mad_timer_add))(&p -> Timer,p -> Frame.header.duration);
            if (to_skip_samples <= samples) {
              ( *(p -> mad_frame_decode))(&p -> Frame,&p -> Stream);
              ( *(p -> mad_synth_frame))(&p -> Synth,(&p -> Frame));
              p -> cursamp = to_skip_samples;
              return SOX_SUCCESS;
            }
            else 
              to_skip_samples -= samples;
/* If not VBR, we can extrapolate frame size */
            if (((p -> FrameCount) == 64) && !(vbr != 0U)) {
              p -> FrameCount = (offset / samples);
              to_skip_samples = (offset % samples);
              if (SOX_SUCCESS != lsx_seeki(ft,((off_t )((((p -> FrameCount) * consumed) / 64) + tagsize)),0)) 
                return SOX_EOF;
/* Reset Stream for refilling buffer */
              ( *(p -> mad_stream_finish))(&p -> Stream);
              ( *(p -> mad_stream_init))(&p -> Stream);
              break; 
            }
          }
        }
      }
    }
  };
  return SOX_EOF;
}
#else /* !HAVE_MAD_H */
#define sox_mp3read NULL
#define stopread NULL
#define sox_mp3seek NULL
#endif /*HAVE_MAD_H*/
#ifdef HAVE_LAME
/* Adapters for lame message callbacks: */
/* These functions are considered optional. If they aren't present in the
   library, the stub versions defined here will be used instead. */
/* read 10 bytes in case there's an ID3 version 2 header here */
/* not readable, maybe opened Write-Only */
/* does the stream begin with the ID3 version 2 file identifier? */
/* the tag size (minus the 10-byte header) is encoded into four
     * bytes where the most significant bit is clear in each byte */
/* no ID3 version 2 tag in this stream */
/* Overwrite the Id3v2 tag (this time TLEN should be accurate) */
/* Get file size */
#endif /* HAVE_LAME */
#if defined(HAVE_LAME) || defined(HAVE_TWOLAME)
#define LAME_BUFFER_SIZE(num_samples) (((num_samples) + 3) / 4 * 5 + 7200)
#ifdef HAVE_TWOLAME
#else
#endif
#ifdef HAVE_LAME
#else
#endif
#ifdef HAVE_TWOLAME
#endif
#ifdef HAVE_LAME
/* First set message callbacks so we don't miss any messages: */
#endif
#ifdef HAVE_TWOLAME
#endif
#ifdef HAVE_LAME
#endif
#ifdef HAVE_TWOLAME
/* Twolame default */
#endif
#ifdef HAVE_LAME
/* LAME default */
#endif
#ifdef HAVE_TWOLAME
#endif
#ifdef HAVE_LAME
#endif
#ifdef HAVE_LAME
#endif
/* The primary parameter to the LAME encoder is the bit rate. If the
   * value of encoding.compression is a positive integer, it's taken as
   * the bitrate in kbps (that is if you specify 128, it use 128 kbps).
   *
   * The second most important parameter is probably "quality" (really
   * performance), which allows balancing encoding speed vs. quality.
   * In LAME, 0 specifies highest quality but is very slow, while
   * 9 selects poor quality, but is fast. (5 is the default and 2 is
   * recommended as a good trade-off for high quality encodes.)
   *
   * Because encoding.compression is a float, the fractional part is used
   * to select quality. 128.2 selects 128 kbps encoding with a quality
   * of 2. There is one problem with this approach. We need 128 to specify
   * 128 kbps encoding with default quality, so .0 means use default. Instead
   * of .0 you have to use .01 to specify the highest quality (128.01).
   *
   * LAME uses bitrate to specify a constant bitrate, but higher quality
   * can be achieved using Variable Bit Rate (VBR). VBR quality (really
   * size) is selected using a number from 0 to 9. Use a value of 0 for high
   * quality, larger files, and 9 for smaller files of lower quality. 4 is
   * the default.
   *
   * In order to squeeze the selection of VBR into the encoding.compression
   * float we use negative numbers to select VRR. -4.2 would select default
   * VBR encoding (size) with high quality (speed). One special case is 0,
   * which is a valid VBR encoding parameter but not a valid bitrate.
   * Compression value of 0 is always treated as a high quality vbr, as a
   * result both -0.2 and 0.2 are treated as highest quality VBR (size) and
   * high quality (speed).
   *
   * Note: It would have been nice to simply use low values, 0-9, to trigger
   * VBR mode, but 8 kbps is a valid bit rate, so negative values were
   * used instead.
  */
/* Do nothing, use defaults: */
#ifdef HAVE_LAME
#endif
#ifdef HAVE_TWOLAME
#endif
#ifdef HAVE_LAME
#endif
/* Set Quality */
/* use default quality value */
#ifdef HAVE_LAME
#endif
#ifdef HAVE_LAME
#endif
#ifdef HAVE_TWOLAME
#endif
#ifdef HAVE_LAME
#endif
#define MP3_SAMPLE_TO_FLOAT(d,clips) ((float)(32768*SOX_SAMPLE_TO_FLOAT_32BIT(d,clips)))
/* lame doesn't support interleaved samples for floats so we must break
             * them out into seperate buffers.
             */
#ifdef HAVE_TWOLAME
#endif
#ifdef HAVE_LAME
#endif
#ifdef HAVE_TWOLAME
#endif
#ifdef HAVE_LAME
#endif
#ifdef HAVE_LAME
#endif
#ifdef HAVE_TWOLAME
#endif
#ifdef HAVE_LAME
#endif
#else /* !(HAVE_LAME || HAVE_TWOLAME) */

static int startwrite(sox_format_t *ft)
{
  lsx_fail_errno(ft,SOX_EOF,"SoX was compiled with neither MP2 nor MP3 encoding support");
  return SOX_EOF;
}
#define sox_mp3write NULL
#define stopwrite NULL
#endif /* HAVE_LAME || HAVE_TWOLAME */
const sox_format_handler_t *lsx_mp3_format_fn();

const sox_format_handler_t *lsx_mp3_format_fn()
{
  static const char *const names[] = {("mp3"), ("mp2"), ("audio/mpeg"), ((const char *)((void *)0))};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_MP3), (0), (0)};
  static const sox_rate_t write_rates[] = {(8000), (11025), (12000), (16000), (22050), (24000), (32000), (44100), (48000), (0)};
  static const sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("MPEG Layer 2/3 lossy audio compression"), (names), (0), (startread), (sox_mp3read), (stopread), (startwrite), ((sox_format_handler_write )((void *)0)), ((sox_format_handler_stopwrite )((void *)0)), (sox_mp3seek), (write_encodings), (write_rates), ((sizeof(priv_t )))};
  return &handler;
}
#endif /* defined(HAVE_MAD_H) || defined(HAVE_LAME) || defined(HAVE_TWOLAME) */
