/* libSoX Compander Transfer Function: (c) 2007 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include "compandt.h"
#include <string.h>
#define LOG_TO_LOG10(x) ((x) * 20 / M_LN10)

sox_bool lsx_compandt_show(sox_compandt_t *t,sox_plot_t plot)
{
  int i;
  for (i = 1; (t -> segments)[i - 1].x != 0.00000; ++i) 
    ((( *sox_get_globals()).subsystem = "compandt.c") , lsx_debug_impl("TF: %g %g %g %g",(((t -> segments)[i].x * 20) / 2.30258509299404568402),(((t -> segments)[i].y * 20) / 2.30258509299404568402),(((t -> segments)[i].a * 20) / 2.30258509299404568402),(((t -> segments)[i].b * 20) / 2.30258509299404568402)));
  if (plot == sox_plot_octave) {
    printf("%% GNU Octave file (may also work with MATLAB(R) )\nin=linspace(-99.5,0,200);\nout=[");
    for (i = -199; i <= 0; ++i) {
      double in = (i / 2.);
      double in_lin = pow(10.,(in / 20));
      printf("%g ",(in + (20 * log10(lsx_compandt(t,in_lin)))));
    }
    printf("];\nplot(in,out)\ntitle(\'SoX effect: compand\')\nxlabel(\'Input level (dB)\')\nylabel(\'Output level (dB)\')\ngrid on\ndisp(\'Hit return to continue\')\npause\n");
    return sox_false;
  }
  if (plot == sox_plot_gnuplot) {
    printf("# gnuplot file\nset title \'SoX effect: compand\'\nset xlabel \'Input level (dB)\'\nset ylabel \'Output level (dB)\'\nset grid xtics ytics\nset key off\nplot \'-\' with lines\n");
    for (i = -199; i <= 0; ++i) {
      double in = (i / 2.);
      double in_lin = pow(10.,(in / 20));
      printf("%g %g\n",in,(in + (20 * log10(lsx_compandt(t,in_lin)))));
    }
    printf("e\npause -1 \'Hit return to continue\'\n");
    return sox_false;
  }
  return sox_true;
}

static void prepare_transfer_fn(sox_compandt_t *t)
{
  int i;
  double radius = (((t -> curve_dB) * 2.30258509299404568402) / 20);
  for (i = 0; !(i != 0) || ((t -> segments)[i - 2].x != 0.00000); i += 2) {
    (t -> segments)[i].y += (t -> outgain_dB);
/* Convert to natural logs */
    (t -> segments)[i].x *= 2.30258509299404568402 / 20;
    (t -> segments)[i].y *= 2.30258509299404568402 / 20;
  }
#define line1 t->segments[i - 4]
#define curve t->segments[i - 3]
#define line2 t->segments[i - 2]
#define line3 t->segments[i - 0]
  for (i = 4; (t -> segments)[i - 2].x != 0.00000; i += 2) {
    double x;
    double y;
    double cx;
    double cy;
    double in1;
    double in2;
    double out1;
    double out2;
    double theta;
    double len;
    double r;
    (t -> segments)[i - 4].a = 0;
    (t -> segments)[i - 4].b = (((t -> segments)[i - 2].y - (t -> segments)[i - 4].y) / ((t -> segments)[i - 2].x - (t -> segments)[i - 4].x));
    (t -> segments)[i - 2].a = 0;
    (t -> segments)[i - 2].b = (((t -> segments)[i - 0].y - (t -> segments)[i - 2].y) / ((t -> segments)[i - 0].x - (t -> segments)[i - 2].x));
    theta = atan2(((t -> segments)[i - 2].y - (t -> segments)[i - 4].y),((t -> segments)[i - 2].x - (t -> segments)[i - 4].x));
    len = sqrt((pow(((t -> segments)[i - 2].x - (t -> segments)[i - 4].x),2.) + pow(((t -> segments)[i - 2].y - (t -> segments)[i - 4].y),2.)));
    r = ((radius <= len)?radius : len);
    (t -> segments)[i - 3].x = ((t -> segments)[i - 2].x - (r * cos(theta)));
    (t -> segments)[i - 3].y = ((t -> segments)[i - 2].y - (r * sin(theta)));
    theta = atan2(((t -> segments)[i - 0].y - (t -> segments)[i - 2].y),((t -> segments)[i - 0].x - (t -> segments)[i - 2].x));
    len = sqrt((pow(((t -> segments)[i - 0].x - (t -> segments)[i - 2].x),2.) + pow(((t -> segments)[i - 0].y - (t -> segments)[i - 2].y),2.)));
    r = ((radius <= (len / 2))?radius : (len / 2));
    x = ((t -> segments)[i - 2].x + (r * cos(theta)));
    y = ((t -> segments)[i - 2].y + (r * sin(theta)));
    cx = ((((t -> segments)[i - 3].x + (t -> segments)[i - 2].x) + x) / 3);
    cy = ((((t -> segments)[i - 3].y + (t -> segments)[i - 2].y) + y) / 3);
    (t -> segments)[i - 2].x = x;
    (t -> segments)[i - 2].y = y;
    in1 = (cx - (t -> segments)[i - 3].x);
    out1 = (cy - (t -> segments)[i - 3].y);
    in2 = ((t -> segments)[i - 2].x - (t -> segments)[i - 3].x);
    out2 = ((t -> segments)[i - 2].y - (t -> segments)[i - 3].y);
    (t -> segments)[i - 3].a = (((out2 / in2) - (out1 / in1)) / (in2 - in1));
    (t -> segments)[i - 3].b = ((out1 / in1) - ((t -> segments)[i - 3].a * in1));
  }
#undef line1
#undef curve
#undef line2
#undef line3
  (t -> segments)[i - 3].x = 0;
  (t -> segments)[i - 3].y = (t -> segments)[i - 2].y;
  t -> in_min_lin = exp((t -> segments)[1].x);
  t -> out_min_lin = exp((t -> segments)[1].y);
}

static sox_bool parse_transfer_value(const char *text,double *value)
{
/* To check for extraneous chars. */
  char dummy;
  if (!(text != 0)) {
    ((( *sox_get_globals()).subsystem = "compandt.c") , lsx_fail_impl("syntax error trying to read transfer function value"));
    return sox_false;
  }
  if (!(strcmp(text,"-inf") != 0)) 
     *value = ((-20) * log10(-((double )((sox_sample_t )(1 << 32 - 1)))));
  else if (sscanf(text,"%lf %c",value,&dummy) != 1) {
    ((( *sox_get_globals()).subsystem = "compandt.c") , lsx_fail_impl("syntax error trying to read transfer function value"));
    return sox_false;
  }
  else if ( *value > 0) {
    ((( *sox_get_globals()).subsystem = "compandt.c") , lsx_fail_impl("transfer function values are relative to maximum volume so can\'t exceed 0dB"));
    return sox_false;
  }
  return sox_true;
}

sox_bool lsx_compandt_parse(sox_compandt_t *t,char *points,char *gain)
{
  const char *text = points;
  unsigned int i;
  unsigned int j;
  unsigned int num;
  unsigned int pairs;
  unsigned int commas = 0;
/* To check for extraneous chars. */
  char dummy;
  if ((sscanf(points,"%lf %c",&t -> curve_dB,&dummy) == 2) && (dummy == ':')) 
    points = (strchr(points,':') + 1);
  else 
    t -> curve_dB = 0;
  t -> curve_dB = (((t -> curve_dB) >= .01)?(t -> curve_dB) : .01);
  while(( *text) != 0)
    commas += (( *(text++)) == ',');
  pairs = (1 + (commas / 2));
/* allow room for extra pair at the beginning */
  ++pairs;
/* allow room for the auto-curves */
  pairs *= 2;
/* allow room for 0,0 at end */
  ++pairs;
  t -> segments = ((((pairs * sizeof(( *(t -> segments)))) != 0ULL)?memset(lsx_realloc(0,(pairs * sizeof(( *(t -> segments))))),0,(pairs * sizeof(( *(t -> segments))))) : ((void *)((void *)0))));
#define s(n) t->segments[2*((n)+1)]
  for (((i = 0) , (text = (strtok(points,",")))); text != ((const char *)((void *)0)); ++i) {
    if (!(parse_transfer_value(text,&(t -> segments)[2 * (i + 1)].x) != 0U)) 
      return sox_false;
    if ((i != 0U) && ((t -> segments)[2 * ((i - 1) + 1)].x > (t -> segments)[2 * (i + 1)].x)) {
      ((( *sox_get_globals()).subsystem = "compandt.c") , lsx_fail_impl("transfer function input values must be strictly increasing"));
      return sox_false;
    }
    if ((i != 0U) || ((commas & 1) != 0U)) {
      text = (strtok(0,","));
      if (!(parse_transfer_value(text,&(t -> segments)[2 * (i + 1)].y) != 0U)) 
        return sox_false;
      (t -> segments)[2 * (i + 1)].y -= (t -> segments)[2 * (i + 1)].x;
    }
    text = (strtok(0,","));
  }
  num = i;
/* Add 0,0 if necessary */
  if ((num == 0) || ((t -> segments)[2 * ((num - 1) + 1)].x != 0.00000)) 
    ++num;
#undef s
  if ((gain != 0) && (sscanf(gain,"%lf %c",&t -> outgain_dB,&dummy) != 1)) {
    ((( *sox_get_globals()).subsystem = "compandt.c") , lsx_fail_impl("syntax error trying to read post-processing gain value"));
    return sox_false;
  }
#define s(n) t->segments[2*(n)]
/* Add a tail off segment at the start */
  (t -> segments)[2 * 0].x = ((t -> segments)[2 * 1].x - (2 * (t -> curve_dB)));
  (t -> segments)[2 * 0].y = (t -> segments)[2 * 1].y;
  ++num;
/* Join adjacent colinear segments */
  for (i = 2; i < num; ++i) {{
      double g1 = (((t -> segments)[2 * (i - 1)].y - (t -> segments)[2 * (i - 2)].y) * ((t -> segments)[2 * (i - 0)].x - (t -> segments)[2 * (i - 1)].x));
      double g2 = (((t -> segments)[2 * (i - 0)].y - (t -> segments)[2 * (i - 1)].y) * ((t -> segments)[2 * (i - 1)].x - (t -> segments)[2 * (i - 2)].x));
/* fabs stops epsilon problems */
      if (fabs((g1 - g2)) != 0.00000) 
        continue; 
      --num;
      for (j = --i; j < num; ++j) 
        (t -> segments)[2 * j] = (t -> segments)[2 * (j + 1)];
    }
  }
#undef s
  prepare_transfer_fn(t);
  return sox_true;
}

void lsx_compandt_kill(sox_compandt_t *p)
{
  free((p -> segments));
}
