/* libSoX file format: Psion wve   (c) 2008 robs@users.sourceforge.net
 *
 * See http://filext.com/file-extension/WVE
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include <string.h>
static const char ID1[18UL] = "ALawSoundFile**\000\017\020";
/* pad & repeat info: ignore */
static const char ID2[] = {(0), (0), (0), (1), (0), (0), (0), (0), (0), (0)};

static int start_read(sox_format_t *ft)
{
  char buf[18UL];
  uint32_t num_samples;
  if (((lsx_readchars(ft,buf,(sizeof(buf))) != 0) || (lsx_readdw(ft,&num_samples) != 0)) || (lsx_skipbytes(ft,(sizeof(ID2))) != 0)) 
    return SOX_EOF;
  if (memcmp(ID1,buf,(sizeof(buf))) != 0) {
    lsx_fail_errno(ft,SOX_EHDR,"wve: can\'t find Psion identifier");
    return SOX_EOF;
  }
  return lsx_check_read_params(ft,1,8000.,SOX_ENCODING_ALAW,8,((uint64_t )num_samples),sox_true);
}

static int write_header(sox_format_t *ft)
{
  uint64_t size64 = ((ft -> olength) != 0UL)?(ft -> olength) : ft -> signal.length;
  unsigned int size = (size64 > (2147483647 * 2U + 1U))?0 : ((unsigned int )size64);
  return (((((((lsx_writebuf(ft,ID1,(sizeof(ID1)))) == sizeof(ID1))?SOX_SUCCESS : SOX_EOF)) != 0) || (lsx_writedw(ft,size) != 0)) || (((((lsx_writebuf(ft,ID2,(sizeof(ID2)))) == sizeof(ID2))?SOX_SUCCESS : SOX_EOF)) != 0))?SOX_EOF : SOX_SUCCESS;
}
const sox_format_handler_t *lsx_wve_format_fn();

const sox_format_handler_t *lsx_wve_format_fn()
{
  static const char *const names[] = {("wve"), ((const char *)((void *)0))};
  static const sox_rate_t write_rates[] = {(8000), (0)};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_ALAW), (8), (0), (0)};
  static const sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("Psion 3 audio format"), (names), ((0x0040 | 0x0080 | 256 | 8)), (start_read), (lsx_rawread), ((sox_format_handler_stopread )((void *)0)), (write_header), (lsx_rawwrite), ((sox_format_handler_stopwrite )((void *)0)), (lsx_rawseek), (write_encodings), (write_rates), (0)};
  return &handler;
}
