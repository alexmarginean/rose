/* Amiga 8SVX format handler: W V Neisius, February 1992 */
#include "sox_i.h"
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
/* Private data used by writer */
typedef struct __unnamed_class___F0_L11_C9_unknown_scope_and_name_variable_declaration__variable_type_uint32_tUi__typedef_declaration_variable_name_unknown_scope_and_name__scope__nsamples__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab___Pb__FILE_IO_FILE__typedef_declaration__Pe___index_4_Ae__variable_name_unknown_scope_and_name__scope__ch {
uint32_t nsamples;
FILE *ch[4UL];}priv_t;
static void svxwriteheader(sox_format_t *,size_t );
/*======================================================================*/
/*                         8SVXSTARTREAD                                */
/*======================================================================*/

static int startread(sox_format_t *ft)
{
  priv_t *p = (priv_t *)(ft -> priv);
  char buf[12UL];
  char *chunk_buf;
  uint32_t totalsize;
  uint32_t chunksize;
  uint32_t channels;
  uint32_t i;
  unsigned short rate;
  off_t chan1_pos;
  if (!((ft -> seekable) != 0U)) {
    lsx_fail_errno(ft,SOX_EINVAL,"8svx input file must be a file, not a pipe");
    return SOX_EOF;
  }
  rate = 0;
  channels = 1;
/* read FORM chunk */
  if ((lsx_reads(ft,buf,((size_t )4)) == SOX_EOF) || (strncmp(buf,"FORM",((size_t )4)) != 0)) {
    lsx_fail_errno(ft,SOX_EHDR,"Header did not begin with magic word `FORM\'");
    return SOX_EOF;
  }
  lsx_readdw(ft,&totalsize);
  if ((lsx_reads(ft,buf,((size_t )4)) == SOX_EOF) || (strncmp(buf,"8SVX",((size_t )4)) != 0)) {
    lsx_fail_errno(ft,SOX_EHDR,"\'FORM\' chunk does not specify `8SVX\' as type");
    return SOX_EOF;
  }
/* read chunks until 'BODY' (or end) */
  while((lsx_reads(ft,buf,((size_t )4)) == SOX_SUCCESS) && (strncmp(buf,"BODY",((size_t )4)) != 0)){
    if (strncmp(buf,"VHDR",((size_t )4)) == 0) {
      lsx_readdw(ft,&chunksize);
      if (chunksize != 20) {
        lsx_fail_errno(ft,SOX_EHDR,"VHDR chunk has bad size");
        return SOX_EOF;
      }
      lsx_seeki(ft,((off_t )12),1);
      lsx_readw(ft,&rate);
      lsx_seeki(ft,((off_t )1),1);
      lsx_readbuf(ft,buf,((size_t )1));
      if (buf[0] != 0) {
        lsx_fail_errno(ft,SOX_EFMT,"Unsupported data compression");
        return SOX_EOF;
      }
      lsx_seeki(ft,((off_t )4),1);
      continue; 
    }
    if (strncmp(buf,"ANNO",((size_t )4)) == 0) {
      lsx_readdw(ft,&chunksize);
      if ((chunksize & 1) != 0U) 
        chunksize++;
      chunk_buf = (lsx_realloc(0,(chunksize + ((size_t )2))));
      if (lsx_readbuf(ft,chunk_buf,((size_t )chunksize)) != chunksize) {
        lsx_fail_errno(ft,SOX_EHDR,"Couldn\'t read all of header");
        return SOX_EOF;
      }
      chunk_buf[chunksize] = 0;
      ((( *sox_get_globals()).subsystem = "8svx.c") , lsx_debug_impl("%s",chunk_buf));
      free(chunk_buf);
      continue; 
    }
    if (strncmp(buf,"NAME",((size_t )4)) == 0) {
      lsx_readdw(ft,&chunksize);
      if ((chunksize & 1) != 0U) 
        chunksize++;
      chunk_buf = (lsx_realloc(0,(chunksize + ((size_t )1))));
      if (lsx_readbuf(ft,chunk_buf,((size_t )chunksize)) != chunksize) {
        lsx_fail_errno(ft,SOX_EHDR,"Couldn\'t read all of header");
        return SOX_EOF;
      }
      chunk_buf[chunksize] = 0;
      ((( *sox_get_globals()).subsystem = "8svx.c") , lsx_debug_impl("%s",chunk_buf));
      free(chunk_buf);
      continue; 
    }
    if (strncmp(buf,"CHAN",((size_t )4)) == 0) {
      lsx_readdw(ft,&chunksize);
      if (chunksize != 4) {
        lsx_fail_errno(ft,SOX_EHDR,"Couldn\'t read all of header");
        return SOX_EOF;
      }
      lsx_readdw(ft,&channels);
      channels = ((((channels & 1) + ((channels & 2) >> 1)) + ((channels & 4) >> 2)) + ((channels & 8) >> 3));
      continue; 
    }
/* some other kind of chunk */
    lsx_readdw(ft,&chunksize);
    if ((chunksize & 1) != 0U) 
      chunksize++;
    lsx_seeki(ft,((off_t )chunksize),1);
    continue; 
  }
  if (rate == 0) {
    lsx_fail_errno(ft,SOX_EHDR,"Invalid sample rate");
    return SOX_EOF;
  }
  if (strncmp(buf,"BODY",((size_t )4)) != 0) {
    lsx_fail_errno(ft,SOX_EHDR,"BODY chunk not found");
    return SOX_EOF;
  }
  lsx_readdw(ft,&p -> nsamples);
  ft -> signal.length = (p -> nsamples);
  ft -> signal.channels = channels;
  ft -> signal.rate = rate;
  ft -> encoding.encoding = SOX_ENCODING_SIGN2;
  ft -> encoding.bits_per_sample = 8;
/* open files to channels */
  (p -> ch)[0] = (ft -> fp);
  chan1_pos = lsx_tell(ft);
  for (i = 1; i < channels; i++) {
    if (((p -> ch)[i] = fopen((ft -> filename),"rb")) == ((FILE *)((void *)0))) {
      lsx_fail_errno(ft, *__errno_location(),"Can\'t open channel file \'%s\'",(ft -> filename));
      return SOX_EOF;
    }
/* position channel files */
    if (fseeko((p -> ch)[i],chan1_pos,0) != 0) {
      lsx_fail_errno(ft, *__errno_location(),"Can\'t position channel %d",i);
      return SOX_EOF;
    }
    if (fseeko((p -> ch)[i],((off_t )(((p -> nsamples) / channels) * i)),1) != 0) {
      lsx_fail_errno(ft, *__errno_location(),"Can\'t seek channel %d",i);
      return SOX_EOF;
    }
  }
  return SOX_SUCCESS;
}
/*======================================================================*/
/*                         8SVXREAD                                     */
/*======================================================================*/

static size_t read_samples(sox_format_t *ft,sox_sample_t *buf,size_t nsamp)
{
  unsigned char datum;
  size_t done = 0;
  size_t i;
  priv_t *p = (priv_t *)(ft -> priv);
  while(done < nsamp){
    for (i = 0; i < ft -> signal.channels; i++) {
/* FIXME: don't pass FILE pointers! */
      datum = (_IO_getc((p -> ch)[i]));
      if (feof((p -> ch)[i]) != 0) 
        return done;
/* scale signed up to long's range */
       *(buf++) = (((sox_sample_t )datum) << 32 - 8);
    }
    done += ft -> signal.channels;
  }
  return done;
}
/*======================================================================*/
/*                         8SVXSTOPREAD                                 */
/*======================================================================*/

static int stopread(sox_format_t *ft)
{
  size_t i;
  priv_t *p = (priv_t *)(ft -> priv);
/* close channel files */
  for (i = 1; i < ft -> signal.channels; i++) {
    fclose((p -> ch)[i]);
  }
  return SOX_SUCCESS;
}
/*======================================================================*/
/*                         8SVXSTARTWRITE                               */
/*======================================================================*/

static int startwrite(sox_format_t *ft)
{
  priv_t *p = (priv_t *)(ft -> priv);
  size_t i;
/* open channel output files */
  (p -> ch)[0] = (ft -> fp);
  for (i = 1; i < ft -> signal.channels; i++) {
    if (((p -> ch)[i] = lsx_tmpfile()) == ((FILE *)((void *)0))) {
      lsx_fail_errno(ft, *__errno_location(),"Can\'t open channel output file");
      return SOX_EOF;
    }
  }
/* write header (channel 0) */
  p -> nsamples = 0;
  svxwriteheader(ft,((size_t )(p -> nsamples)));
  return SOX_SUCCESS;
}
/*======================================================================*/
/*                         8SVXWRITE                                    */
/*======================================================================*/

static size_t write_samples(sox_format_t *ft,const sox_sample_t *buf,size_t len)
{
  priv_t *p = (priv_t *)(ft -> priv);
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  unsigned char datum;
  size_t done = 0;
  size_t i;
  p -> nsamples += len;
  while(done < len){
    for (i = 0; i < ft -> signal.channels; i++) {
      datum = ((sox_int8_t )((((sox_macro_temp_double , (sox_macro_temp_sample =  *(buf++)))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - (1 << 31 - 8))?((++ft -> clips , ((unsigned int )(-1)) >> 33 - 8)) : (((sox_uint32_t )(sox_macro_temp_sample + (1 << 31 - 8))) >> 32 - 8)))));
/* FIXME: Needs to pass ft struct and not FILE */
      _IO_putc(datum,(p -> ch)[i]);
    }
    done += ft -> signal.channels;
  }
  return done;
}
/*======================================================================*/
/*                         8SVXSTOPWRITE                                */
/*======================================================================*/

static int stopwrite(sox_format_t *ft)
{
  priv_t *p = (priv_t *)(ft -> priv);
  size_t i;
  size_t len;
  char svxbuf[512UL];
/* append all channel pieces to channel 0 */
/* close temp files */
  for (i = 1; i < ft -> signal.channels; i++) {
    if (fseeko((p -> ch)[i],((off_t )0),0) != 0) {
      lsx_fail_errno(ft, *__errno_location(),"Can\'t rewind channel output file %lu",((unsigned long )i));
      return SOX_EOF;
    }
    while(!(feof((p -> ch)[i]) != 0)){
      len = fread(svxbuf,((size_t )1),((size_t )512),(p -> ch)[i]);
      if (fwrite(svxbuf,((size_t )1),len,(p -> ch)[0]) != len) {
        lsx_fail_errno(ft, *__errno_location(),"Can\'t write channel output file %lu",((unsigned long )i));
        return SOX_EOF;
      }
    }
    fclose((p -> ch)[i]);
  }
/* add a pad byte if BODY size is odd */
  if (((p -> nsamples) % 2) != 0) 
    lsx_writeb(ft,0);
/* fixup file sizes in header */
  if (lsx_seeki(ft,((off_t )0),0) != 0) {
    lsx_fail_errno(ft, *__errno_location(),"can\'t rewind output file to rewrite 8SVX header");
    return SOX_EOF;
  }
  svxwriteheader(ft,((size_t )(p -> nsamples)));
  return SOX_SUCCESS;
}
/*======================================================================*/
/*                         8SVXWRITEHEADER                              */
/*======================================================================*/
#define SVXHEADERSIZE 100

static void svxwriteheader(sox_format_t *ft,size_t nsamples)
{
  size_t formsize = ((nsamples + 100) - 8);
/* FORM size must be even */
  if ((formsize % 2) != 0) 
    formsize++;
  lsx_writes(ft,"FORM");
/* size of file */
  lsx_writedw(ft,((unsigned int )formsize));
/* File type */
  lsx_writes(ft,"8SVX");
  lsx_writes(ft,"VHDR");
/* number of bytes to follow */
  lsx_writedw(ft,20);
/* samples, 1-shot */
  lsx_writedw(ft,(((unsigned int )nsamples) / ft -> signal.channels));
/* samples, repeat */
  lsx_writedw(ft,0);
/* samples per repeat cycle */
  lsx_writedw(ft,0);
  lsx_writew(ft,((65535 <= ((unsigned int )(ft -> signal.rate + .5)))?65535 : ((unsigned int )(ft -> signal.rate + .5))));
/* number of octabes */
  lsx_writeb(ft,1);
/* data compression (none) */
  lsx_writeb(ft,0);
/* volume */
  lsx_writew(ft,1);
  lsx_writew(ft,0);
  lsx_writes(ft,"ANNO");
/* length of block */
  lsx_writedw(ft,32);
  lsx_writes(ft,"File created by Sound Exchange  ");
  lsx_writes(ft,"CHAN");
  lsx_writedw(ft,4);
  lsx_writedw(ft,((ft -> signal.channels == 2)?6u : (((ft -> signal.channels == 4)?15u : 2u))));
  lsx_writes(ft,"BODY");
/* samples in file */
  lsx_writedw(ft,((unsigned int )nsamples));
}
const sox_format_handler_t *lsx_svx_format_fn();

const sox_format_handler_t *lsx_svx_format_fn()
{
  static const char *const names[] = {("8svx"), ((const char *)((void *)0))};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_SIGN2), (8), (0), (0)};
  static const sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("Amiga audio format (a subformat of the Interchange File Format)"), (names), ((0x0040 | 0x0080 | 256 | 512 | 1024)), (startread), (read_samples), (stopread), (startwrite), (write_samples), (stopwrite), ((sox_format_handler_seek )((void *)0)), (write_encodings), ((const sox_rate_t *)((void *)0)), ((sizeof(priv_t )))};
  return &handler;
}
