/* libSoX effect: splice audio   Copyright (c) 2008-9 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"

static double difference(const sox_sample_t *a,const sox_sample_t *b,size_t length)
{
  double diff = 0;
  size_t i = 0;
  #define _ diff += sqr((double)a[i] - b[i]), ++i; /* Loop optimisation */
/* N.B. length ≡ 0 (mod 8) */
  do {
    ((diff += ((((double )a[i]) - b[i]) * (((double )a[i]) - b[i]))) , ++i);
    ((diff += ((((double )a[i]) - b[i]) * (((double )a[i]) - b[i]))) , ++i);
    ((diff += ((((double )a[i]) - b[i]) * (((double )a[i]) - b[i]))) , ++i);
    ((diff += ((((double )a[i]) - b[i]) * (((double )a[i]) - b[i]))) , ++i);
    ((diff += ((((double )a[i]) - b[i]) * (((double )a[i]) - b[i]))) , ++i);
    ((diff += ((((double )a[i]) - b[i]) * (((double )a[i]) - b[i]))) , ++i);
    ((diff += ((((double )a[i]) - b[i]) * (((double )a[i]) - b[i]))) , ++i);
    ((diff += ((((double )a[i]) - b[i]) * (((double )a[i]) - b[i]))) , ++i);
  }while (i < length);
  #undef _
  return diff;
}
/* Find where the two segments are most alike over the overlap period. */

static size_t best_overlap_position(const sox_sample_t *f1,const sox_sample_t *f2,uint64_t overlap,uint64_t search,size_t channels)
{
  size_t i;
  size_t best_pos = 0;
  double diff;
  double least_diff = difference(f2,f1,(channels * overlap));
/* linear search */
  for (i = 1; i < search; ++i) {
    diff = difference((f2 + (channels * i)),f1,(channels * overlap));
    if (diff < least_diff) 
      ((least_diff = diff) , (best_pos = i));
  }
  return best_pos;
}
typedef struct __unnamed_class___F0_L48_C9_unknown_scope_and_name_variable_declaration__variable_type_L126R_variable_name_unknown_scope_and_name__scope__fade_type__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__nsplices__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__un_named_base_type__Pe___variable_name_unknown_scope_and_name__scope__splices__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__in_pos__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__splices_pos__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__buffer_pos__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__max_buffer_size__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L5R__Pe___variable_name_unknown_scope_and_name__scope__buffer__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__state {
enum __unnamed_enum___F0_L49_C3_Cosine_2__COMMA__Cosine_4__COMMA__Triangular {Cosine_2,Cosine_4,Triangular}fade_type;
/* Number of splices requested */
unsigned int nsplices;
struct __unnamed_class___F0_L51_C3_L127R_variable_declaration__variable_type___Pb__c__Pe___variable_name_L127R__scope__str__DELIMITER__L127R_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_L127R__scope__overlap__DELIMITER__L127R_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_L127R__scope__search__DELIMITER__L127R_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_L127R__scope__start {
/* Command-line argument to parse for this splice */
char *str;
/* Number of samples to overlap */
uint64_t overlap;
/* Number of samples to search */
uint64_t search;
/* Start splicing when in_pos equals this */
uint64_t start;}*splices;
/* Number of samples read from the input stream */
uint64_t in_pos;
/* Number of splices completed so far */
unsigned int splices_pos;
/* Number of samples through the current splice */
size_t buffer_pos;
size_t max_buffer_size;
sox_sample_t *buffer;
unsigned int state;}priv_t;

static void splice(sox_effect_t *effp,const sox_sample_t *in1,const sox_sample_t *in2,sox_sample_t *output,uint64_t overlap,size_t channels)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t i;
  size_t j;
  size_t k = 0;
  if ((p -> fade_type) == Cosine_4) {
    double fade_step = (1.57079632679489661923 / overlap);
    for (i = 0; i < overlap; ++i) {
      double fade_in = sin((i * fade_step));
/* constant RMS level (`power') */
      double fade_out = cos((i * fade_step));
      for (j = 0; j < channels; (++j , ++k)) {
        double d = ((in1[k] * fade_out) + (in2[k] * fade_in));
/* Might clip */
        output[k] = (((d < 0)?(((d <= ((sox_sample_t )(1 << 32 - 1)) - 0.5)?((++effp -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (d - 0.5))) : (((d >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 0.5)?((++effp -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : (d + 0.5)))));
      }
    }
  }
  else if ((p -> fade_type) == Cosine_2) {
    double fade_step = (3.14159265358979323846 / overlap);
    for (i = 0; i < overlap; ++i) {
      double fade_in = (0.5 - (0.5 * cos((i * fade_step))));
/* constant peak level (`gain') */
      double fade_out = (1 - fade_in);
      for (j = 0; j < channels; (++j , ++k)) {
        double d = ((in1[k] * fade_out) + (in2[k] * fade_in));
/* Should not clip */
        output[k] = (((d < 0)?(((d <= ((sox_sample_t )(1 << 32 - 1)) - 0.5)?((++effp -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (d - 0.5))) : (((d >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 0.5)?((++effp -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : (d + 0.5)))));
      }
    }
  }
  else 
/* Triangular */
{
    double fade_step = (1. / overlap);
    for (i = 0; i < overlap; ++i) {
      double fade_in = (fade_step * i);
/* constant peak level (`gain') */
      double fade_out = (1 - fade_in);
      for (j = 0; j < channels; (++j , ++k)) {
        double d = ((in1[k] * fade_out) + (in2[k] * fade_in));
/* Should not clip */
        output[k] = (((d < 0)?(((d <= ((sox_sample_t )(1 << 32 - 1)) - 0.5)?((++effp -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (d - 0.5))) : (((d >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 0.5)?((++effp -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : (d + 0.5)))));
      }
    }
  }
}

static uint64_t do_splice(sox_effect_t *effp,sox_sample_t *f,uint64_t overlap,uint64_t search,size_t channels)
{
  uint64_t offset = (search != 0UL)?best_overlap_position(f,(f + (overlap * channels)),overlap,search,channels) : 0;
  splice(effp,f,(f + ((overlap + offset) * channels)),(f + ((overlap + offset) * channels)),overlap,channels);
  return overlap + offset;
}

static int parse(sox_effect_t *effp,char **argv,sox_rate_t rate)
{
  priv_t *p = (priv_t *)(effp -> priv);
  const char *next;
  size_t i;
  size_t buffer_size;
  p -> max_buffer_size = 0;
{
    for (i = 0; i < (p -> nsplices); ++i) {
/* 1st parse only */
      if (argv != 0) 
        (p -> splices)[i].str = ((argv[i] != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(argv[i]) + 1)))),argv[i]) : ((char *)((void *)0)));
      (p -> splices)[i].overlap = ((rate * 0.01) + 0.5);
      (p -> splices)[i].search = (((p -> fade_type) == Cosine_4)?0 : (p -> splices)[i].overlap);
      next = lsx_parsesamples(rate,(p -> splices)[i].str,&(p -> splices)[i].start,'t');
      if (next == ((const char *)((void *)0))) 
        break; 
      if (( *next) == ',') {
        next = lsx_parsesamples(rate,(next + 1),&(p -> splices)[i].overlap,'t');
        if (next == ((const char *)((void *)0))) 
          break; 
        (p -> splices)[i].overlap *= 2;
        if (( *next) == ',') {
          next = lsx_parsesamples(rate,(next + 1),&(p -> splices)[i].search,'t');
          if (next == ((const char *)((void *)0))) 
            break; 
          (p -> splices)[i].search *= 2;
        }
      }
      if (( *next) != 0) 
        break; 
      (p -> splices)[i].overlap = ((((p -> splices)[i].overlap + 4) >= 16)?((p -> splices)[i].overlap + 4) : 16);
/* Make divisible by 8 for loop optimisation */
      (p -> splices)[i].overlap &= (~7);
      if ((i > 0) && ((p -> splices)[i].start <= (p -> splices)[i - 1].start)) 
        break; 
      if ((p -> splices)[i].start < (p -> splices)[i].overlap) 
        break; 
      (p -> splices)[i].start -= (p -> splices)[i].overlap;
      buffer_size = ((2 * (p -> splices)[i].overlap) + (p -> splices)[i].search);
      p -> max_buffer_size = (((p -> max_buffer_size) >= buffer_size)?(p -> max_buffer_size) : buffer_size);
    }
  }
  if (i < (p -> nsplices)) 
    return lsx_usage(effp);
  return SOX_SUCCESS;
}

static int create(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  (--argc , ++argv);
  if (argc != 0) {
    if (!(strcmp(( *argv),"-t") != 0)) 
      ((((p -> fade_type = Triangular) , --argc)) , ++argv);
    else if (!(strcmp(( *argv),"-q") != 0)) 
      ((((p -> fade_type = Cosine_4) , --argc)) , ++argv);
    else if (!(strcmp(( *argv),"-h") != 0)) 
      ((((p -> fade_type = Cosine_2) , --argc)) , ++argv);
  }
  p -> nsplices = argc;
  p -> splices = (((((p -> nsplices) * sizeof(( *(p -> splices)))) != 0ULL)?memset(lsx_realloc(0,((p -> nsplices) * sizeof(( *(p -> splices))))),0,((p -> nsplices) * sizeof(( *(p -> splices))))) : ((void *)((void *)0))));
/* No rate yet; parse with dummy */
  return parse(effp,argv,1e5);
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  unsigned int i;
/* Re-parse now rate is known */
  parse(effp,0,effp -> in_signal.rate);
  p -> buffer = ((((((p -> max_buffer_size) * effp -> in_signal.channels) * sizeof(( *(p -> buffer)))) != 0ULL)?memset(lsx_realloc(0,(((p -> max_buffer_size) * effp -> in_signal.channels) * sizeof(( *(p -> buffer))))),0,(((p -> max_buffer_size) * effp -> in_signal.channels) * sizeof(( *(p -> buffer))))) : ((void *)((void *)0))));
  p -> in_pos = (p -> buffer_pos = (p -> splices_pos = 0));
  p -> state = (((p -> splices_pos) != (p -> nsplices)) && ((p -> in_pos) == (p -> splices)[p -> splices_pos].start));
/* depends on input data */
  effp -> out_signal.length = ((sox_uint64_t )(-1));
  for (i = 0; i < (p -> nsplices); ++i) 
    if ((p -> splices)[i].overlap != 0UL) {
      if (((p -> fade_type) == Cosine_4) && (effp -> in_signal.mult != 0)) 
         *effp -> in_signal.mult *= pow(0.5,0.5);
      return SOX_SUCCESS;
    }
  return 32;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t c;
  size_t idone = 0;
  size_t odone = 0;
   *isamp /= effp -> in_signal.channels;
   *osamp /= effp -> in_signal.channels;
{
    while(1){
      copying:
      if ((p -> state) == 0) {
        for (; (idone <  *isamp) && (odone <  *osamp); (((++idone , ++odone)) , ++p -> in_pos)) {
          if (((p -> splices_pos) != (p -> nsplices)) && ((p -> in_pos) == (p -> splices)[p -> splices_pos].start)) {
            p -> state = 1;
            goto buffering;
          }
          for (c = 0; c < effp -> in_signal.channels; ++c) 
             *(obuf++) =  *(ibuf++);
        }
        break; 
      }
      buffering:
      if ((p -> state) == 1) {
        size_t buffer_size = (((2 * (p -> splices)[p -> splices_pos].overlap) + (p -> splices)[p -> splices_pos].search) * effp -> in_signal.channels);
{
          for (; idone <  *isamp; (++idone , ++p -> in_pos)) {
            if ((p -> buffer_pos) == buffer_size) {
              p -> buffer_pos = (do_splice(effp,(p -> buffer),(p -> splices)[p -> splices_pos].overlap,(p -> splices)[p -> splices_pos].search,((size_t )effp -> in_signal.channels)) * effp -> in_signal.channels);
              p -> state = 2;
              goto flushing;
              break; 
            }
            for (c = 0; c < effp -> in_signal.channels; ++c) 
              (p -> buffer)[p -> buffer_pos++] =  *(ibuf++);
          }
        }
        break; 
      }
      flushing:
      if ((p -> state) == 2) {
        size_t buffer_size = (((2 * (p -> splices)[p -> splices_pos].overlap) + (p -> splices)[p -> splices_pos].search) * effp -> in_signal.channels);
        for (; odone <  *osamp; ++odone) {
          if ((p -> buffer_pos) == buffer_size) {
            p -> buffer_pos = 0;
            ++p -> splices_pos;
            p -> state = (((p -> splices_pos) != (p -> nsplices)) && ((p -> in_pos) == (p -> splices)[p -> splices_pos].start));
            goto copying;
          }
          for (c = 0; c < effp -> in_signal.channels; ++c) 
             *(obuf++) = (p -> buffer)[p -> buffer_pos++];
        }
        break; 
      }
    }
  }
   *isamp = (idone * effp -> in_signal.channels);
   *osamp = (odone * effp -> in_signal.channels);
  return SOX_SUCCESS;
}

static int drain(sox_effect_t *effp,sox_sample_t *obuf,size_t *osamp)
{
  size_t isamp = 0;
  return flow(effp,0,obuf,&isamp,osamp);
}

static int stop(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  if ((p -> splices_pos) != (p -> nsplices)) 
    ((( *sox_get_globals()).subsystem = "splice.c") , lsx_warn_impl("Input audio too short; splices not made: %u",((p -> nsplices) - (p -> splices_pos))));
  free((p -> buffer));
  return SOX_SUCCESS;
}

static int lsx_kill(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  unsigned int i;
  for (i = 0; i < (p -> nsplices); ++i) 
    free((p -> splices)[i].str);
  free((p -> splices));
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_splice_effect_fn()
{
  static sox_effect_handler_t handler = {("splice"), ("[-h|-t|-q] {position[,excess[,leeway]]}\n  -h        Half sine fade (default); constant gain (for correlated audio)\n  -t        Triangular (linear) fade; constant gain (for correlated audio)\n  -q        Quarter sine fade; constant power (for correlated audio e.g. x-fade)\n  position  The length of part 1 (including the excess)\n  excess    At the end of part 1 & the start of part2 (default 0.005)\n  leeway    Before part2 (default 0.005; set to 0 for cross-fade)"), ((16 | 8)), (create), (start), (flow), (drain), (stop), (lsx_kill), ((sizeof(priv_t )))};
  return (&handler);
}
