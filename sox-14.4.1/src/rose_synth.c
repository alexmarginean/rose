/* libSoX synth - Synthesizer Effect.
 *
 * Copyright (c) 2001-2009 SoX contributors
 * Copyright (c) Jan 2001  Carsten Borchardt
 *
 * This source code is freely redistributable and may be used for any purpose.
 * This copyright notice must be maintained.  The authors are not responsible
 * for the consequences of using this software.
 *
 * Except for synth types: pluck, tpdf, & brownnoise, and sweep types: linear
 *   square & exp, which are:
 *
 * Copyright (c) 2006-2009 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include <string.h>
#include <ctype.h>
typedef enum __unnamed_enum___F0_L35_C9_synth_sine__COMMA__synth_square__COMMA__synth_sawtooth__COMMA__synth_triangle__COMMA__synth_trapezium__COMMA__synth_trapetz__COMMA__synth_exp__COMMA__synth_whitenoise__COMMA__synth_noise__COMMA__synth_tpdfnoise__COMMA__synth_pinknoise__COMMA__synth_brownnoise__COMMA__synth_pluck {synth_sine,synth_square,synth_sawtooth,synth_triangle,synth_trapezium,
/* Deprecated name for trapezium */
synth_trapetz=4,synth_exp,
/* Tones above, noises below */
synth_whitenoise,
/* Just a handy alias */
synth_noise=6,synth_tpdfnoise,synth_pinknoise,synth_brownnoise,synth_pluck}type_t;
static const lsx_enum_item synth_type[] = {{("sine"), (synth_sine)}, {("square"), (synth_square)}, {("sawtooth"), (synth_sawtooth)}, {("triangle"), (synth_triangle)}, {("trapezium"), (synth_trapezium)}, {("trapetz"), (synth_trapetz)}, {("exp"), (synth_exp)}, {("whitenoise"), (synth_whitenoise)}, {("noise"), (synth_noise)}, {("tpdfnoise"), (synth_tpdfnoise)}, {("pinknoise"), (synth_pinknoise)}, {("brownnoise"), (synth_brownnoise)}, {("pluck"), (synth_pluck)}, {(0), (0)}};
typedef enum __unnamed_enum___F0_L69_C9_synth_create__COMMA__synth_mix__COMMA__synth_amod__COMMA__synth_fmod {synth_create,synth_mix,synth_amod,synth_fmod}combine_t;
static const lsx_enum_item combine_type[] = {{("create"), (synth_create)}, {("mix"), (synth_mix)}, {("amod"), (synth_amod)}, {("fmod"), (synth_fmod)}, {(0), (0)}};
/******************************************************************************
 * start of pink noise generator stuff
 * algorithm stolen from:
 * Author: Phil Burk, http://www.softsynth.com
 */
#define PINK_MAX_RANDOM_ROWS   (30)
#define PINK_RANDOM_BITS       (24)
#define PINK_RANDOM_SHIFT      ((sizeof(int32_t)*8)-PINK_RANDOM_BITS)
typedef struct __unnamed_class___F0_L91_C9_unknown_scope_and_name_variable_declaration__variable_type__Ab_l_index_30_Ae__variable_name_unknown_scope_and_name__scope__pink_Rows__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_l_variable_name_unknown_scope_and_name__scope__pink_RunningSum__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__pink_Index__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__pink_IndexMask__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_f_variable_name_unknown_scope_and_name__scope__pink_Scalar {
long pink_Rows[30];
/* Used to optimize summing of generators. */
long pink_RunningSum;
/* Incremented each sample. */
int pink_Index;
/* Index wrapped by ANDing with this mask. */
int pink_IndexMask;
/* Used to scale within range of -1 to +1 */
float pink_Scalar;}PinkNoise;
/* Setup PinkNoise structure for N rows of generators. */

static void InitializePinkNoise(PinkNoise *pink,size_t numRows)
{
  size_t i;
  long pmax;
  pink -> pink_Index = 0;
  pink -> pink_IndexMask = ((1 << numRows) - 1);
/* Calculate maximum possible signed random value. Extra 1 for white noise always added. */
  pmax = ((numRows + 1) * (1 << 24 - 1));
  pink -> pink_Scalar = (1.0f / pmax);
/* Initialize rows. */
  for (i = 0; i < numRows; i++) 
    (pink -> pink_Rows)[i] = 0;
  pink -> pink_RunningSum = 0;
}
/* Generate Pink noise values between -1 and +1 */

static float GeneratePinkNoise(PinkNoise *pink)
{
  long newRandom;
  long sum;
  float output;
/* Increment and mask index. */
  pink -> pink_Index = (((pink -> pink_Index) + 1) & (pink -> pink_IndexMask));
/* If index is zero, don't update any random values. */
  if ((pink -> pink_Index) != 0) {
/* Determine how many trailing zeros in PinkIndex. */
/* This algorithm will hang if n==0 so test first. */
    int numZeros = 0;
    int n = (pink -> pink_Index);
    while((n & 1) == 0){
      n = (n >> 1);
      numZeros++;
    }
/* Replace the indexed ROWS random value.
     * Subtract and add back to RunningSum instead of adding all the random
     * values together. Only one changes each time.
     */
    pink -> pink_RunningSum -= (pink -> pink_Rows)[numZeros];
    newRandom = ((( *sox_get_globals()).ranqd1 = ((1664525L * ( *sox_get_globals()).ranqd1) + 1013904223L)) >> sizeof(int32_t ) * 8 - 24);
    pink -> pink_RunningSum += newRandom;
    (pink -> pink_Rows)[numZeros] = newRandom;
  }
/* Add extra white noise value. */
  newRandom = ((( *sox_get_globals()).ranqd1 = ((1664525L * ( *sox_get_globals()).ranqd1) + 1013904223L)) >> sizeof(int32_t ) * 8 - 24);
  sum = ((pink -> pink_RunningSum) + newRandom);
/* Scale to range of -1 to 0.9999. */
  output = ((pink -> pink_Scalar) * sum);
  return output;
}
/**************** end of pink noise stuff */
typedef enum __unnamed_enum___F0_L162_C9_Linear__COMMA__Square__COMMA__Exp__COMMA__Exp_cycle {Linear,Square,Exp,Exp_cycle}sweep_t;
typedef struct __unnamed_class___F0_L164_C9_unknown_scope_and_name_variable_declaration__variable_type_type_tL126R__typedef_declaration_variable_name_unknown_scope_and_name__scope__type__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_combine_tL127R__typedef_declaration_variable_name_unknown_scope_and_name__scope__combine__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__freq__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__freq2__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__mult__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sweep_tL128R__typedef_declaration_variable_name_unknown_scope_and_name__scope__sweep__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__offset__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__phase__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__p1__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__p2__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__p3__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__lp_last_out__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__hp_last_out__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__hp_last_in__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__ap_last_out__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__ap_last_in__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__cycle_start_time_s__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__c0__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__c1__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__c2__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__c3__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__c4__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_PinkNoiseL129R__typedef_declaration_variable_name_unknown_scope_and_name__scope__pink_noise__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__d__Pe___variable_name_unknown_scope_and_name__scope__buffer__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__buffer_len__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__pos {
/* options */
type_t type;
combine_t combine;
double freq;
double freq2;
double mult;
sweep_t sweep;
double offset;
double phase;
/* Use depends on synth type */
double p1;
double p2;
double p3;
/* internal stuff */
double lp_last_out;
double hp_last_out;
double hp_last_in;
double ap_last_out;
double ap_last_in;
double cycle_start_time_s;
double c0;
double c1;
double c2;
double c3;
double c4;
PinkNoise pink_noise;
double *buffer;
size_t buffer_len;
size_t pos;}channel_t;
/* Private data for the synthesizer */
typedef struct __unnamed_class___F0_L185_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__length_str__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__channel_tL130R__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__getopts_channels__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__getopts_nchannels__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__samples_done__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__samples_to_do__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__channel_tL130R__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__channels__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__number_of_channels__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__no_headroom__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__gain {
char *length_str;
channel_t *getopts_channels;
size_t getopts_nchannels;
uint64_t samples_done;
uint64_t samples_to_do;
channel_t *channels;
size_t number_of_channels;
sox_bool no_headroom;
double gain;}priv_t;

static void create_channel(channel_t *chan)
{
  memset(chan,0,(sizeof(( *chan))));
  chan -> freq2 = (chan -> freq = 440);
  chan -> p3 = (chan -> p2 = (chan -> p1 = (-1)));
}

static void set_default_parameters(channel_t *chan,size_t c)
{
  switch(chan -> type){
/* p1 is pulse width */
    case synth_square:
{
      if ((chan -> p1) < 0) 
/* default to 50% duty cycle */
        chan -> p1 = 0.5;
      break; 
    }
/* p1 is position of maximum */
    case synth_triangle:
{
      if ((chan -> p1) < 0) 
        chan -> p1 = 0.5;
      break; 
    }
    case synth_trapezium:
{
/* p1 is length of rising slope,
       * p2 position where falling slope begins
       * p3 position of end of falling slope
       */
      if ((chan -> p1) < 0) {
        chan -> p1 = 0.1;
        chan -> p2 = 0.5;
        chan -> p3 = 0.6;
/* try a symmetric waveform */
      }
      else if ((chan -> p2) < 0) {
        if ((chan -> p1) <= 0.5) {
          chan -> p2 = ((1 - (2 * (chan -> p1))) / 2);
          chan -> p3 = ((chan -> p2) + (chan -> p1));
        }
        else {
/* symetric is not possible, fall back to asymmetrical triangle */
          chan -> p2 = (chan -> p1);
          chan -> p3 = 1;
        }
      }
      else if ((chan -> p3) < 0) 
/* simple falling slope to the end */
        chan -> p3 = 1;
      break; 
    }
    case synth_pinknoise:
{
/* Initialize pink noise signals with different numbers of rows. */
      InitializePinkNoise(&chan -> pink_noise,(10 + (2 * c)));
      break; 
    }
    case synth_exp:
{
/* p1 is position of maximum */
      if ((chan -> p1) < 0) 
        chan -> p1 = 0.5;
/* p2 is amplitude */
      if ((chan -> p2) < 0) 
        chan -> p2 = 0.5;
      break; 
    }
    case synth_pluck:
{
      if ((chan -> p1) < 0) 
        chan -> p1 = .4;
      if ((chan -> p2) < 0) 
        ((chan -> p2 = .2) , (chan -> p3 = .9));
    }
    default:
{
      break; 
    }
  }
}
#undef NUMERIC_PARAMETER
#define NUMERIC_PARAMETER(p, min, max) { \
char * end_ptr_np; \
double d_np = strtod(argv[argn], &end_ptr_np); \
if (end_ptr_np == argv[argn]) \
  break; \
if (d_np < min || d_np > max || *end_ptr_np != '\0') { \
  lsx_fail("parameter error"); \
  return SOX_EOF; \
} \
chan->p = d_np / 100; /* adjust so abs(parameter) <= 1 */\
if (++argn == argc) \
  break; \
}

static int getopts(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  channel_t master;
  channel_t *chan = &master;
  int key = 2147483647;
  int argn = 0;
  char dummy;
  char *end_ptr;
  const char *n;
  (--argc , ++argv);
  if ((argc != 0) && !(strcmp(( *argv),"-n") != 0)) 
    ((((p -> no_headroom = sox_true) , ++argv)) , --argc);
  if (((argc > 1) && !(strcmp(( *argv),"-j") != 0)) && ((sscanf(argv[1],"%i %c",&key,&dummy) == 1) || (((key = lsx_parse_note(argv[1],&end_ptr)) != 2147483647) && !(( *end_ptr) != 0)))) {
    argc -= 2;
    argv += 2;
  }
/* Get duration if given (if first arg starts with digit) */
  if ((argc != 0) && (((( *__ctype_b_loc())[(int )argv[argn][0]] & ((unsigned short )_ISdigit)) != 0) || (argv[argn][0] == '.'))) {
    p -> length_str = ((argv[argn] != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(argv[argn]) + 1)))),argv[argn]) : ((char *)((void *)0)));
/* Do a dummy parse of to see if it will fail */
    n = lsx_parsesamples(0.,(p -> length_str),&p -> samples_to_do,'t');
    if (!(n != 0) || (( *n) != 0)) 
      return lsx_usage(effp);
    argn++;
  }
  create_channel(chan);
/* [off [ph [p1 [p2 [p3]]]]]] */
  if (argn < argc) {{
/* break-able block */
      do {{
          char *end_ptr_np;
          double d_np = strtod(argv[argn],&end_ptr_np);
          if (end_ptr_np == argv[argn]) 
            break; 
          if (((d_np < (-100)) || (d_np > 100)) || (( *end_ptr_np) != 0)) {
            ((( *sox_get_globals()).subsystem = "synth.c") , lsx_fail_impl("parameter error"));
            return SOX_EOF;
          }
          chan -> offset = (d_np / 100);
          if (++argn == argc) 
            break; 
        }
{
          char *end_ptr_np;
          double d_np = strtod(argv[argn],&end_ptr_np);
          if (end_ptr_np == argv[argn]) 
            break; 
          if (((d_np < 0) || (d_np > 100)) || (( *end_ptr_np) != 0)) {
            ((( *sox_get_globals()).subsystem = "synth.c") , lsx_fail_impl("parameter error"));
            return SOX_EOF;
          }
          chan -> phase = (d_np / 100);
          if (++argn == argc) 
            break; 
        }
{
          char *end_ptr_np;
          double d_np = strtod(argv[argn],&end_ptr_np);
          if (end_ptr_np == argv[argn]) 
            break; 
          if (((d_np < 0) || (d_np > 100)) || (( *end_ptr_np) != 0)) {
            ((( *sox_get_globals()).subsystem = "synth.c") , lsx_fail_impl("parameter error"));
            return SOX_EOF;
          }
          chan -> p1 = (d_np / 100);
          if (++argn == argc) 
            break; 
        }
{
          char *end_ptr_np;
          double d_np = strtod(argv[argn],&end_ptr_np);
          if (end_ptr_np == argv[argn]) 
            break; 
          if (((d_np < 0) || (d_np > 100)) || (( *end_ptr_np) != 0)) {
            ((( *sox_get_globals()).subsystem = "synth.c") , lsx_fail_impl("parameter error"));
            return SOX_EOF;
          }
          chan -> p2 = (d_np / 100);
          if (++argn == argc) 
            break; 
        }
{
          char *end_ptr_np;
          double d_np = strtod(argv[argn],&end_ptr_np);
          if (end_ptr_np == argv[argn]) 
            break; 
          if (((d_np < 0) || (d_np > 100)) || (( *end_ptr_np) != 0)) {
            ((( *sox_get_globals()).subsystem = "synth.c") , lsx_fail_impl("parameter error"));
            return SOX_EOF;
          }
          chan -> p3 = (d_np / 100);
          if (++argn == argc) 
            break; 
        }
      }while (0);
    }
  }
{
/* type [combine] [f1[-f2] [off [ph [p1 [p2 [p3]]]]]] */
    while(argn < argc){
      const lsx_enum_item *enum_p = lsx_find_enum_text(argv[argn],synth_type,lsx_find_enum_item_case_sensitive);
      if (enum_p == ((const lsx_enum_item *)((void *)0))) {
        ((( *sox_get_globals()).subsystem = "synth.c") , lsx_fail_impl("no type given"));
        return SOX_EOF;
      }
      p -> getopts_channels = (lsx_realloc((p -> getopts_channels),(sizeof(( *(p -> getopts_channels))) * ((p -> getopts_nchannels) + 1))));
      chan = ((p -> getopts_channels) + p -> getopts_nchannels++);
      memcpy(chan,(&master),(sizeof(( *chan))));
      chan -> type = (enum_p -> value);
      if (++argn == argc) 
        break; 
/* maybe there is a combine-type in next arg */
      enum_p = lsx_find_enum_text(argv[argn],combine_type,lsx_find_enum_item_case_sensitive);
      if (enum_p != ((const lsx_enum_item *)((void *)0))) {
        chan -> combine = (enum_p -> value);
        if (++argn == argc) 
          break; 
      }
/* read frequencies if given */
      if (!(lsx_find_enum_text(argv[argn],synth_type,lsx_find_enum_item_case_sensitive) != 0) && (argv[argn][0] != '-')) {
        static const char sweeps[] = ":+/-";
        chan -> freq2 = (chan -> freq = lsx_parse_frequency_k(argv[argn],&end_ptr,key));
        if (((chan -> freq) < ((((chan -> type) == synth_pluck)?27.5 : 0))) || (((chan -> type) == synth_pluck) && ((chan -> freq) > 4220))) {
          ((( *sox_get_globals()).subsystem = "synth.c") , lsx_fail_impl("invalid freq"));
          return SOX_EOF;
        }
/* freq2 given? */
        if ((( *end_ptr) != 0) && (strchr(sweeps,( *end_ptr)) != 0)) {
          if ((chan -> type) >= synth_noise) {
            ((( *sox_get_globals()).subsystem = "synth.c") , lsx_fail_impl("can\'t sweep this type"));
            return SOX_EOF;
          }
          chan -> sweep = (strchr(sweeps,( *end_ptr)) - sweeps);
          chan -> freq2 = lsx_parse_frequency_k((end_ptr + 1),&end_ptr,key);
          if ((chan -> freq2) < 0) {
            ((( *sox_get_globals()).subsystem = "synth.c") , lsx_fail_impl("invalid freq2"));
            return SOX_EOF;
          }
          if ((p -> length_str) == ((char *)((void *)0))) {
            ((( *sox_get_globals()).subsystem = "synth.c") , lsx_fail_impl("duration must be given when using freq2"));
            return SOX_EOF;
          }
        }
        if (( *end_ptr) != 0) {
          ((( *sox_get_globals()).subsystem = "synth.c") , lsx_fail_impl("frequency: invalid trailing character"));
          return SOX_EOF;
        }
        if (((chan -> sweep) >= Exp) && (((chan -> freq) * (chan -> freq2)) == 0)) {
          ((( *sox_get_globals()).subsystem = "synth.c") , lsx_fail_impl("invalid frequency for exponential sweep"));
          return SOX_EOF;
        }
        if (++argn == argc) 
          break; 
      }
{
/* read rest of parameters */
/* break-able block */
        do {{
            char *end_ptr_np;
            double d_np = strtod(argv[argn],&end_ptr_np);
            if (end_ptr_np == argv[argn]) 
              break; 
            if (((d_np < (-100)) || (d_np > 100)) || (( *end_ptr_np) != 0)) {
              ((( *sox_get_globals()).subsystem = "synth.c") , lsx_fail_impl("parameter error"));
              return SOX_EOF;
            }
            chan -> offset = (d_np / 100);
            if (++argn == argc) 
              break; 
          }
{
            char *end_ptr_np;
            double d_np = strtod(argv[argn],&end_ptr_np);
            if (end_ptr_np == argv[argn]) 
              break; 
            if (((d_np < 0) || (d_np > 100)) || (( *end_ptr_np) != 0)) {
              ((( *sox_get_globals()).subsystem = "synth.c") , lsx_fail_impl("parameter error"));
              return SOX_EOF;
            }
            chan -> phase = (d_np / 100);
            if (++argn == argc) 
              break; 
          }
{
            char *end_ptr_np;
            double d_np = strtod(argv[argn],&end_ptr_np);
            if (end_ptr_np == argv[argn]) 
              break; 
            if (((d_np < 0) || (d_np > 100)) || (( *end_ptr_np) != 0)) {
              ((( *sox_get_globals()).subsystem = "synth.c") , lsx_fail_impl("parameter error"));
              return SOX_EOF;
            }
            chan -> p1 = (d_np / 100);
            if (++argn == argc) 
              break; 
          }
{
            char *end_ptr_np;
            double d_np = strtod(argv[argn],&end_ptr_np);
            if (end_ptr_np == argv[argn]) 
              break; 
            if (((d_np < 0) || (d_np > 100)) || (( *end_ptr_np) != 0)) {
              ((( *sox_get_globals()).subsystem = "synth.c") , lsx_fail_impl("parameter error"));
              return SOX_EOF;
            }
            chan -> p2 = (d_np / 100);
            if (++argn == argc) 
              break; 
          }
{
            char *end_ptr_np;
            double d_np = strtod(argv[argn],&end_ptr_np);
            if (end_ptr_np == argv[argn]) 
              break; 
            if (((d_np < 0) || (d_np > 100)) || (( *end_ptr_np) != 0)) {
              ((( *sox_get_globals()).subsystem = "synth.c") , lsx_fail_impl("parameter error"));
              return SOX_EOF;
            }
            chan -> p3 = (d_np / 100);
            if (++argn == argc) 
              break; 
          }
        }while (0);
      }
    }
  }
/* If no channel parameters were given, create one default channel: */
  if (!((p -> getopts_nchannels) != 0UL)) {
    p -> getopts_channels = (lsx_realloc(0,(sizeof(( *(p -> getopts_channels))))));
    memcpy(((p -> getopts_channels) + 0),(&master),(sizeof(channel_t )));
    ++p -> getopts_nchannels;
  }
  if (!(effp -> in_signal.channels != 0U)) 
    effp -> in_signal.channels = (p -> getopts_nchannels);
  return SOX_SUCCESS;
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t i;
  size_t j;
  size_t k;
  p -> samples_done = 0;
  if ((p -> length_str) != 0) {
    if (lsx_parsesamples(effp -> in_signal.rate,(p -> length_str),&p -> samples_to_do,'t') == ((const char *)((void *)0))) 
      return lsx_usage(effp);
  }
  else 
    p -> samples_to_do = ((effp -> in_signal.length != ((sox_uint64_t )(-1)))?(effp -> in_signal.length / effp -> in_signal.channels) : 0);
  p -> number_of_channels = effp -> in_signal.channels;
  p -> channels = (((((p -> number_of_channels) * sizeof(( *(p -> channels)))) != 0ULL)?memset(lsx_realloc(0,((p -> number_of_channels) * sizeof(( *(p -> channels))))),0,((p -> number_of_channels) * sizeof(( *(p -> channels))))) : ((void *)((void *)0))));
  for (i = 0; i < (p -> number_of_channels); ++i) {
    channel_t *chan = ((p -> channels) + i);
     *chan = (p -> getopts_channels)[i % (p -> getopts_nchannels)];
    set_default_parameters(chan,i);
    if ((chan -> type) == synth_pluck) {
      double min;
      double max;
      double frac;
      double p2;
/* Low pass: */
/* dB / s */
      const double decay_rate = (-2);
      const double decay_f = (912 <= (266 + (106 * log((chan -> freq)))))?912 : (266 + (106 * log((chan -> freq))));
      double d = (exp((((decay_rate / (chan -> freq)) * 2.30258509299404568402) * 0.05)) * exp((((decay_rate / (chan -> freq)) * 2.30258509299404568402) * 0.05)));
      d = (((d * cos(((2 * 3.14159265358979323846 * decay_f) / effp -> in_signal.rate))) - 1) / (d - 1));
      chan -> c0 = (d - sqrt(((d * d) - 1)));
      chan -> c1 = (1 - (chan -> c0));
/* Single-pole low pass is very rate-dependent: */
      if ((effp -> in_signal.rate < 44100) || (effp -> in_signal.rate > 48000)) {
        ((( *sox_get_globals()).subsystem = "synth.c") , lsx_fail_impl("sample rate for pluck must be 44100-48000; use `rate\' to resample"));
        return SOX_EOF;
      }
/* Decay: */
      chan -> c1 *= exp((((-2e4 / (0.05 + (chan -> p1))) / (chan -> freq)) / effp -> in_signal.rate));
/* High pass (DC-block): */
      chan -> c2 = exp(((-2) * 3.14159265358979323846 * 10 / effp -> in_signal.rate));
      chan -> c3 = ((1 + (chan -> c2)) * 0.5);
/* All pass (for fractional delay): */
      d = ((chan -> c0) / ((chan -> c0) + (chan -> c1)));
      chan -> buffer_len = ((effp -> in_signal.rate / (chan -> freq)) - d);
      frac = (((effp -> in_signal.rate / (chan -> freq)) - d) - (chan -> buffer_len));
      chan -> c4 = ((1 - frac) / (1 + frac));
      chan -> pos = 0;
/* Exitation: */
      chan -> buffer = (((((chan -> buffer_len) * sizeof(( *(chan -> buffer)))) != 0ULL)?memset(lsx_realloc(0,((chan -> buffer_len) * sizeof(( *(chan -> buffer))))),0,((chan -> buffer_len) * sizeof(( *(chan -> buffer))))) : ((void *)((void *)0))));
      for (((k = 0) , (p2 = (chan -> p2))); (k < 2) && (p2 >= 0); (++k , (p2 = (chan -> p3)))) {
        double d1 = 0;
        double d2;
        double colour = pow(2.,(4 * (p2 - 1)));
        int32_t r = ((p2 * 100) + 0.5);
        for (j = 0; j < (chan -> buffer_len); ++j) {
          do 
            d2 = (d1 + (((((chan -> phase) != 0.)?((( *sox_get_globals()).ranqd1 = ((1664525L * ( *sox_get_globals()).ranqd1) + 1013904223L)) * (1. / (65536. * 32768.))) : ((r = ((1664525L * r) + 1013904223L)) * (1. / (65536. * 32768.))))) * colour));while (fabs(d2) > 1);
          (chan -> buffer)[j] += (d2 * (1 - (.3 * k)));
          d1 = (d2 * (colour != 1));
#ifdef TEST_PLUCK
#endif
        }
      }
/* In-delay filter graduation: */
      for (((j = 0) , (min = (max = 0))); j < (chan -> buffer_len); ++j) {
        double d2;
        double t = (((double )j) / (chan -> buffer_len));
        chan -> lp_last_out = (d2 = (((chan -> buffer)[j] * (chan -> c1)) + ((chan -> lp_last_out) * (chan -> c0))));
        chan -> ap_last_out = (((d2 * (chan -> c4)) + (chan -> ap_last_in)) - ((chan -> ap_last_out) * (chan -> c4)));
        chan -> ap_last_in = d2;
        (chan -> buffer)[j] = (((chan -> buffer)[j] * (1 - t)) + ((chan -> ap_last_out) * t));
        min = ((min <= (chan -> buffer)[j])?min : (chan -> buffer)[j]);
        max = ((max >= (chan -> buffer)[j])?max : (chan -> buffer)[j]);
      }
/* Normalise: */
      for (((j = 0) , (d = 0)); j < (chan -> buffer_len); ++j) {
        (chan -> buffer)[j] = ((((2 * (chan -> buffer)[j]) - max) - min) / (max - min));
        d += ((chan -> buffer)[j] * (chan -> buffer)[j]);
      }
      ((( *sox_get_globals()).subsystem = "synth.c") , lsx_debug_impl("rms=%f c0=%f c1=%f df=%f d3f=%f c2=%f c3=%f c4=%f frac=%f",(10 * log((d / (chan -> buffer_len)))),(chan -> c0),(chan -> c1),decay_f,(((log((chan -> c0)) / (-2)) / 3.14159265358979323846) * effp -> in_signal.rate),(chan -> c2),(chan -> c3),(chan -> c4),frac));
    }
    switch(chan -> sweep){
      case Linear:
{
        chan -> mult = (((p -> samples_to_do) != 0UL)?((((chan -> freq2) - (chan -> freq)) / (p -> samples_to_do)) / 2) : 0);
        break; 
      }
      case Square:
{
        chan -> mult = (((p -> samples_to_do) != 0UL)?((sqrt(fabs(((chan -> freq2) - (chan -> freq)))) / (p -> samples_to_do)) / sqrt(3.)) : 0);
        if ((chan -> freq) > (chan -> freq2)) 
          chan -> mult = -(chan -> mult);
        break; 
      }
      case Exp:
{
        chan -> mult = (((p -> samples_to_do) != 0UL)?((log(((chan -> freq2) / (chan -> freq))) / (p -> samples_to_do)) * effp -> in_signal.rate) : 1);
        chan -> freq /= (chan -> mult);
        break; 
      }
      case Exp_cycle:
{
        chan -> mult = (((p -> samples_to_do) != 0UL)?((log((chan -> freq2)) - log((chan -> freq))) / (p -> samples_to_do)) : 1);
        break; 
      }
    }
    ((( *sox_get_globals()).subsystem = "synth.c") , lsx_debug_impl("type=%s, combine=%s, samples_to_do=%lu, f1=%g, f2=%g, offset=%g, phase=%g, p1=%g, p2=%g, p3=%g mult=%g",( *lsx_find_enum_value((chan -> type),synth_type)).text,( *lsx_find_enum_value((chan -> combine),combine_type)).text,(p -> samples_to_do),(chan -> freq),(chan -> freq2),(chan -> offset),(chan -> phase),(chan -> p1),(chan -> p2),(chan -> p3),(chan -> mult)));
  }
  p -> gain = 1;
  effp -> out_signal.mult = (((p -> no_headroom) != 0U)?((double *)((void *)0)) : &p -> gain);
  effp -> out_signal.length = (((p -> samples_to_do) != 0UL)?((p -> samples_to_do) * effp -> out_signal.channels) : ((sox_uint64_t )(-1)));
  return SOX_SUCCESS;
}
#define elapsed_time_s p->samples_done / effp->in_signal.rate

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  unsigned int len = (((( *isamp <=  *osamp)? *isamp :  *osamp)) / effp -> in_signal.channels);
  unsigned int c;
  unsigned int done;
  int result = SOX_SUCCESS;
  for (done = 0; (done < len) && (result == SOX_SUCCESS); ++done) {
    for (c = 0; c < effp -> in_signal.channels; c++) {
      sox_sample_t synth_input =  *(ibuf++);
      channel_t *chan = ((p -> channels) + c);
/* [-1, 1] */
      double synth_out;
/* Need to calculate phase: */
      if ((chan -> type) < synth_noise) {
/* [0, 1) */
        double phase;
{
          switch(chan -> sweep){
            case Linear:
{
              phase = ((((chan -> freq) + ((p -> samples_done) * (chan -> mult))) * (p -> samples_done)) / effp -> in_signal.rate);
              break; 
            }
            case Square:
{
              phase = ((((chan -> freq) + (((((chan -> mult) < 0)?-1 : 1)) * (((p -> samples_done) * (chan -> mult)) * ((p -> samples_done) * (chan -> mult))))) * (p -> samples_done)) / effp -> in_signal.rate);
              break; 
            }
            case Exp:
{
              phase = ((chan -> freq) * exp((((chan -> mult) * (p -> samples_done)) / effp -> in_signal.rate)));
              break; 
            }
            default:
{
{
                double f = ((chan -> freq) * exp(((p -> samples_done) * (chan -> mult))));
                double cycle_elapsed_time_s = (((p -> samples_done) / effp -> in_signal.rate) - (chan -> cycle_start_time_s));
/* move to next cycle */
                if ((f * cycle_elapsed_time_s) >= 1) {
                  chan -> cycle_start_time_s += (1 / f);
                  cycle_elapsed_time_s = (((p -> samples_done) / effp -> in_signal.rate) - (chan -> cycle_start_time_s));
                }
                phase = (f * cycle_elapsed_time_s);
                break; 
              }
            }
          }
        }
        phase = fmod((phase + (chan -> phase)),1.);
        switch(chan -> type){
          case synth_sine:
{
            synth_out = sin((2 * 3.14159265358979323846 * phase));
            break; 
          }
          case synth_square:
{
/* |_______           | +1
             * |       |          |
             * |_______|__________|  0
             * |       |          |
             * |       |__________| -1
             * |                  |
             * 0       p1          1
             */
            synth_out = (-1 + (2 * (phase < (chan -> p1))));
            break; 
          }
          case synth_sawtooth:
{
/* |           __| +1
             * |        __/  |
             * |_______/_____|  0
             * |  __/        |
             * |_/           | -1
             * |             |
             * 0             1
             */
            synth_out = ((-1) + (2 * phase));
            break; 
          }
          case synth_triangle:
{
/* |    .    | +1
             * |   / \   |
             * |__/___\__|  0
             * | /     \ |
             * |/       \| -1
             * |         |
             * 0   p1    1
             */
            if (phase < (chan -> p1)) 
/* In rising part of period */
              synth_out = ((-1) + ((2 * phase) / (chan -> p1)));
            else 
/* In falling part */
              synth_out = (1 - ((2 * (phase - (chan -> p1))) / (1 - (chan -> p1))));
            break; 
          }
          case synth_trapezium:
{
/* |    ______             |+1
             * |   /      \            |
             * |__/________\___________| 0
             * | /          \          |
             * |/            \_________|-1
             * |                       |
             * 0   p1    p2   p3       1
             */
/* In rising part of period */
            if (phase < (chan -> p1)) 
              synth_out = ((-1) + ((2 * phase) / (chan -> p1)));
            else 
/* In high part of period */
if (phase < (chan -> p2)) 
              synth_out = 1;
            else 
/* In falling part */
if (phase < (chan -> p3)) 
              synth_out = (1 - ((2 * (phase - (chan -> p2))) / ((chan -> p3) - (chan -> p2))));
            else 
/* In low part of period */
              synth_out = (-1);
            break; 
          }
          case synth_exp:
{
/* |             |              | +1
             * |            | |             |
             * |          _|   |_           | 0
             * |       __-       -__        |
             * |____---             ---____ | f(p2)
             * |                            |
             * 0             p1             1
             */
/* 0 ..  1 */
            synth_out = exp(((((chan -> p2) * (-200)) * 2.30258509299404568402) * 0.05));
            if (phase < (chan -> p1)) 
              synth_out = (synth_out * exp(((phase * log((1 / synth_out))) / (chan -> p1))));
            else 
              synth_out = (synth_out * exp((((1 - phase) * log((1 / synth_out))) / (1 - (chan -> p1)))));
/* map 0 .. 1 to -1 .. +1 */
            synth_out = ((synth_out * 2) - 1);
            break; 
          }
          default:
{
            synth_out = 0;
          }
        }
      }
      else {
        switch(chan -> type){
          case synth_whitenoise:
{
            synth_out = ((( *sox_get_globals()).ranqd1 = ((1664525L * ( *sox_get_globals()).ranqd1) + 1013904223L)) * (1. / (65536. * 32768.)));
            break; 
          }
          case synth_tpdfnoise:
{
            synth_out = (0.5 * (((( *sox_get_globals()).ranqd1 = ((1664525L * ( *sox_get_globals()).ranqd1) + 1013904223L)) * (1. / (65536. * 32768.))) + ((( *sox_get_globals()).ranqd1 = ((1664525L * ( *sox_get_globals()).ranqd1) + 1013904223L)) * (1. / (65536. * 32768.)))));
            break; 
          }
          case synth_pinknoise:
{
            synth_out = (GeneratePinkNoise(&chan -> pink_noise));
            break; 
          }
          case synth_brownnoise:
{
            do 
              synth_out = ((chan -> lp_last_out) + (((( *sox_get_globals()).ranqd1 = ((1664525L * ( *sox_get_globals()).ranqd1) + 1013904223L)) * (1. / (65536. * 32768.))) * (1. / 16)));while (fabs(synth_out) > 1);
            chan -> lp_last_out = synth_out;
            break; 
          }
          case synth_pluck:
{
{
              double d = (chan -> buffer)[chan -> pos];
              chan -> hp_last_out = (((d - (chan -> hp_last_in)) * (chan -> c3)) + ((chan -> hp_last_out) * (chan -> c2)));
              chan -> hp_last_in = d;
              synth_out = ((((((chan -> hp_last_out) >= (-1))?(chan -> hp_last_out) : (-1))) <= 1)?((((chan -> hp_last_out) >= (-1))?(chan -> hp_last_out) : (-1))) : 1);
              chan -> lp_last_out = (d = ((d * (chan -> c1)) + ((chan -> lp_last_out) * (chan -> c0))));
              chan -> ap_last_out = ((chan -> buffer)[chan -> pos] = (((d - (chan -> ap_last_out)) * (chan -> c4)) + (chan -> ap_last_in)));
              chan -> ap_last_in = d;
              chan -> pos = ((((chan -> pos) + 1) == (chan -> buffer_len))?0 : ((chan -> pos) + 1));
              break; 
            }
          }
          default:
{
            synth_out = 0;
          }
        }
      }
/* Add offset, but prevent clipping: */
      synth_out = ((synth_out * (1 - fabs((chan -> offset)))) + (chan -> offset));
      switch(chan -> combine){
        case synth_create:
{
          synth_out *= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32));
          break; 
        }
        case synth_mix:
{
          synth_out = (((synth_out * ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32))) + synth_input) * 0.5);
          break; 
        }
        case synth_amod:
{
          synth_out = (((synth_out + 1) * synth_input) * 0.5);
          break; 
        }
        case synth_fmod:
{
          synth_out *= synth_input;
          break; 
        }
      }
       *(obuf++) = (((synth_out < 0)?((synth_out * (p -> gain)) - 0.5) : ((synth_out * (p -> gain)) + 0.5)));
    }
    if (++p -> samples_done == (p -> samples_to_do)) 
      result = SOX_EOF;
  }
   *isamp = ( *osamp = (done * effp -> in_signal.channels));
  return result;
}

static int stop(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t i;
  for (i = 0; i < (p -> number_of_channels); ++i) 
    free((p -> channels)[i].buffer);
  free((p -> channels));
  return SOX_SUCCESS;
}

static int lsx_kill(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  free((p -> getopts_channels));
  free((p -> length_str));
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_synth_effect_fn()
{
  static sox_effect_handler_t handler = {("synth"), ("[-j KEY] [-n] [length [offset [phase [p1 [p2 [p3]]]]]]] {type [combine] [[%]freq[k][:|+|/|-[%]freq2[k]] [offset [phase [p1 [p2 [p3]]]]]]}"), ((16 | 8 | 128)), (getopts), (start), (flow), (0), (stop), (lsx_kill), ((sizeof(priv_t )))};
  return (&handler);
}
