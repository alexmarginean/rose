/* libSoX effect: Output audio to a file   (c) 2008 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
typedef struct __unnamed_class___F0_L20_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__L13R__Pe___variable_name_unknown_scope_and_name__scope__file {
sox_format_t *file;}priv_t;

static int getopts(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  if (((argc != 2) || !((p -> file = ((sox_format_t *)argv[1])) != 0)) || (( *(p -> file)).mode != 'w')) 
    return SOX_EOF;
  return SOX_SUCCESS;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
/* Write out *isamp samples */
  size_t len = sox_write((p -> file),ibuf, *isamp);
/* len is the number of samples that were actually written out; if this is
   * different to *isamp, then something has gone wrong--most often, it's
   * out of disc space */
  if (len !=  *isamp) {
    ((( *sox_get_globals()).subsystem = "output.c") , lsx_fail_impl("%s: %s",( *(p -> file)).filename,( *(p -> file)).sox_errstr));
    return SOX_EOF;
  }
/* Outputting is the last `effect' in the effect chain so always passes
   * 0 samples on to the next effect (as there isn't one!) */
  (obuf , ( *osamp = 0));
/* All samples output successfully */
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_output_effect_fn()
{
  static sox_effect_handler_t handler = {("output"), ((const char *)((void *)0)), ((16 | 1024)), (getopts), ((sox_effect_handler_start )((void *)0)), (flow), ((sox_effect_handler_drain )((void *)0)), ((sox_effect_handler_stop )((void *)0)), ((sox_effect_handler_kill )((void *)0)), ((sizeof(priv_t )))};
  return (&handler);
}
