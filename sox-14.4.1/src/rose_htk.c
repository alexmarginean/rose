/* libSoX file format: HTK   (c) 2008 robs@users.sourceforge.net
 *
 * See http://labrosa.ee.columbia.edu/doc/HTKBook21/HTKBook.html
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
typedef enum  {Waveform,Lpc,Lprefc,Lpcepstra,Lpdelcep,Irefc,Mfcc,Fbank,Melspec,User,Discrete,Unknown}kind_t;
static const char *const str[] = {("Sampled waveform"), ("Linear prediction filter"), ("Linear prediction"), ("LPC cepstral"), ("LPC cepstra plus delta"), ("LPC reflection coef in"), ("Mel-frequency cepstral"), ("Log mel-filter bank"), ("Linear mel-filter bank"), ("User defined sample"), ("Vector quantised data"), ("Unknown")};

static int start_read(sox_format_t *ft)
{
  uint32_t period_100ns;
  uint32_t num_samples;
  uint16_t bytes_per_sample;
  uint16_t parmKind;
  if ((((lsx_readdw(ft,&num_samples) != 0) || (lsx_readdw(ft,&period_100ns) != 0)) || (lsx_readw(ft,&bytes_per_sample) != 0)) || (lsx_readw(ft,&parmKind) != 0)) 
    return SOX_EOF;
  if (parmKind != Waveform) {
    int n = ((parmKind & 077) <= Unknown)?(parmKind & 077) : Unknown;
    lsx_fail_errno(ft,SOX_EFMT,"unsupported HTK type `%s\' (0%o)",str[n],parmKind);
    return SOX_EOF;
  }
  return lsx_check_read_params(ft,1,(1e7 / period_100ns),SOX_ENCODING_SIGN2,(((unsigned int )bytes_per_sample) << 3),((uint64_t )num_samples),sox_true);
}

static int write_header(sox_format_t *ft)
{
  double period_100ns = (1e7 / ft -> signal.rate);
  uint64_t len = ((ft -> olength) != 0UL)?(ft -> olength) : ft -> signal.length;
  if (len > (2147483647 * 2U + 1U)) {
    ((( *sox_get_globals()).subsystem = "htk.c") , lsx_warn_impl("length greater than 32 bits - cannot fit actual length in header"));
    len = (2147483647 * 2U + 1U);
  }
  if (!((ft -> olength) != 0UL) && (floor(period_100ns) != period_100ns)) 
    ((( *sox_get_globals()).subsystem = "htk.c") , lsx_warn_impl("rounding sample period %f (x 100ns) to nearest integer",period_100ns));
  return ((((lsx_writedw(ft,((unsigned int )len)) != 0) || (lsx_writedw(ft,((unsigned int )(period_100ns + .5))) != 0)) || (lsx_writew(ft,(ft -> encoding.bits_per_sample >> 3)) != 0)) || (lsx_writew(ft,Waveform) != 0))?SOX_EOF : SOX_SUCCESS;
}
const sox_format_handler_t *lsx_htk_format_fn();

const sox_format_handler_t *lsx_htk_format_fn()
{
  static const char *const names[] = {("htk"), ((const char *)((void *)0))};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_SIGN2), (16), (0), (0)};
  static sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("PCM format used for Hidden Markov Model speech processing"), (names), ((0x0040 | 0x0080 | 256 | 8)), (start_read), (lsx_rawread), ((sox_format_handler_stopread )((void *)0)), (write_header), (lsx_rawwrite), ((sox_format_handler_stopwrite )((void *)0)), (lsx_rawseek), (write_encodings), ((const sox_rate_t *)((void *)0)), (0)};
  return (&handler);
}
