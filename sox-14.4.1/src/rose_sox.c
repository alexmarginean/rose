/* SoX - The Swiss Army Knife of Audio Manipulation.
 *
 * This is the main function for the SoX command line programs:
 *   sox, play, rec, soxi.
 *
 * Copyright 1998-2009 Chris Bagwell and SoX contributors
 * Copyright 1991 Lance Norskog And Sundry Contributors
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#include "soxconfig.h"
#include "sox.h"
#include "util.h"
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#if defined(HAVE_WIN32_GLOB_H)
#include "win32-glob.h"
#define HAVE_GLOB_H 1
#elif defined(HAVE_GLOB_H)
#include <glob.h>
#endif
#ifdef HAVE_IO_H
#include <io.h>
#endif
#ifdef HAVE_SUN_AUDIOIO_H
#include <sun/audioio.h>
#define HAVE_AUDIOIO_H 1
#else
#ifdef HAVE_SYS_AUDIOIO_H
#include <sys/audioio.h>
#define HAVE_AUDIOIO_H 1
#endif
#endif
#ifdef HAVE_SYS_SOUNDCARD_H
#include <sys/soundcard.h>
#define HAVE_SOUNDCARD_H 1
#else
#ifdef HAVE_MACHINE_SOUNDCARD_H
#include <machine/soundcard.h>
#define HAVE_SOUNDCARD_H 1
#endif
#endif
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#ifdef HAVE_SYS_TIMEB_H
#include <sys/timeb.h>
#endif
#ifdef HAVE_SYS_UTSNAME_H
#include <sys/utsname.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_GETTIMEOFDAY
#define TIME_FRAC 1e6
#else
#define timeval timeb
#define gettimeofday(a,b) ftime(a)
#define tv_sec time
#define tv_usec millitm
#define TIME_FRAC 1e3
#endif
#if !defined(HAVE_CONIO_H) && !defined(HAVE_TERMIOS_H) && (defined(_MSC_VER) || defined(__MINGW32__))
#define HAVE_CONIO_H 1
#endif
#ifdef HAVE_CONIO_H
/* _kbhit and _getch */
#include <conio.h>
#undef HAVE_TERMIOS_H
#endif
/*#define MORE_INTERACTIVE 1*/
#define SOX_OPTS "SOX_OPTS"
static lsx_getopt_t optstate;
/* argv[0] options */
static const char *myname = (const char *)((void *)0);
static enum __unnamed_enum___F0_L116_C8_sox_sox__COMMA__sox_play__COMMA__sox_rec__COMMA__sox_soxi {sox_sox,sox_play,sox_rec,sox_soxi}sox_mode;
/* gopts */
static enum __unnamed_enum___F0_L122_C8_sox_sequence__COMMA__sox_concatenate__COMMA__sox_mix__COMMA__sox_mix_power__COMMA__sox_merge__COMMA__sox_multiply__COMMA__sox_default {sox_sequence,sox_concatenate,sox_mix,sox_mix_power,sox_merge,sox_multiply,sox_default}combine_method = sox_default;
static enum __unnamed_enum___F0_L127_C8_sox_single__COMMA__sox_multiple {sox_single,sox_multiple}output_method = sox_single;
#define is_serial(m) ((m) <= sox_concatenate)
#define is_parallel(m) (!is_serial(m))
static sox_bool no_clobber = sox_false;
static sox_bool interactive = sox_false;
static sox_bool uservolume = sox_false;
typedef enum __unnamed_enum___F0_L135_C9_RG_off__COMMA__RG_track__COMMA__RG_album__COMMA__RG_default {RG_off,RG_track,RG_album,RG_default}rg_mode;
static const lsx_enum_item rg_modes[] = {{("off"), (RG_off)}, {("track"), (RG_track)}, {("album"), (RG_album)}, {(0), (0)}};
static rg_mode replay_gain_mode = RG_default;
static sox_option_t show_progress = sox_option_default;
/* Input & output files */
typedef struct __unnamed_class___F0_L150_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__filename__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__filetype__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L36R_variable_name_unknown_scope_and_name__scope__signal__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L38R_variable_name_unknown_scope_and_name__scope__encoding__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__volume__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__replay_gain__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_oob_tsox_oob_t__typedef_declaration_variable_name_unknown_scope_and_name__scope__oob__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__no_glob__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L12R__Pe___variable_name_unknown_scope_and_name__scope__ft__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__volume_clips__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_rg_modeL123R__typedef_declaration_variable_name_unknown_scope_and_name__scope__replay_gain_mode {
char *filename;
/* fopts */
const char *filetype;
sox_signalinfo_t signal;
sox_encodinginfo_t encoding;
double volume;
double replay_gain;
sox_oob_t oob;
sox_bool no_glob;
/* libSoX file descriptor */
sox_format_t *ft;
uint64_t volume_clips;
rg_mode replay_gain_mode;}file_t;
/* Array tracking input and output files */
static file_t **files = (file_t **)((void *)0);
#define ofile files[file_count - 1]
static size_t file_count = 0;
static size_t input_count = 0;
static size_t output_count = 0;
/* Effects */
/* We parse effects into a temporary effects table and then place into
 * the real effects chain.  This allows scanning all effects to give
 * hints to what input effect options should be as well as determining
 * when mixer or resample effects need to be auto-inserted as well.
 */
static sox_effect_t **user_efftab = (sox_effect_t **)((void *)0);
static size_t user_efftab_size = 0;
static sox_effects_chain_t *effects_chain = (sox_effects_chain_t *)((void *)0);
static sox_effect_t *save_output_eff = (sox_effect_t *)((void *)0);
static struct __unnamed_class___F0_L185_C8_unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__name__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__argc__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb____Pb__c__Pe____Pe___variable_name_unknown_scope_and_name__scope__argv__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__argv_size {
char *name;
int argc;
char **argv;
size_t argv_size;}**user_effargs = (struct __unnamed_class___F0_L185_C8_unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__name__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__argc__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb____Pb__c__Pe____Pe___variable_name_unknown_scope_and_name__scope__argv__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__argv_size **)((void *)0);
/* array: size of user_effargs for each chain */
static size_t *user_effargs_size = (size_t *)((void *)0);
/* Size of memory structures related to effects arguments (user_effargs[i],
 * user_effargs[i][j].argv) to be extended in steps of EFFARGS_STEP */
#define EFFARGS_STEP 8
/* array: number of effects in each chain */
static unsigned int *nuser_effects = (unsigned int *)((void *)0);
static int current_eff_chain = 0;
static int eff_chain_count = 0;
static sox_bool very_first_effchain = sox_true;
/* Indicates that not only the first effects chain is in effect (hrm), but
   also that it has never been restarted. Only then we may use the
   optimize_trim() hack. */
static char *effects_filename = (char *)((void *)0);
static char *play_rate_arg = (char *)((void *)0);
static char *norm_level = (char *)((void *)0);
/* Flowing */
static sox_signalinfo_t combiner_signal;
static sox_signalinfo_t ofile_signal_options;
static sox_encodinginfo_t combiner_encoding;
static sox_encodinginfo_t ofile_encoding_options;
static uint64_t mixing_clips = 0;
static size_t current_input = 0;
static uint64_t input_wide_samples = 0;
static uint64_t read_wide_samples = 0;
static uint64_t output_samples = 0;
static sox_bool input_eof = sox_false;
static sox_bool output_eof = sox_false;
static sox_bool user_abort = sox_false;
static sox_bool user_skip = sox_false;
static sox_bool user_restart_eff = sox_false;
static int success = 0;
static int cleanup_called = 0;
static sox_sample_t omax[2UL];
static sox_sample_t omin[2UL];
#ifdef HAVE_TERMIOS_H
#include <termios.h>
static struct termios original_termios;
static sox_bool original_termios_saved = sox_false;
#endif
static sox_bool stdin_is_a_tty;
static sox_bool is_player;
static sox_bool is_guarded;
static sox_bool do_guarded_norm;
static sox_bool no_dither;
static sox_bool reported_sox_opts;

static void cleanup()
{
  size_t i;
  if (!(success != 0) && !(reported_sox_opts != 0U)) {
    const char *env_opts = (getenv("SOX_OPTS"));
    if ((env_opts != 0) && (( *env_opts) != 0)) 
      ((( *sox_get_globals()).subsystem = "sox.c") , lsx_report_impl("used SOX_OPTS=%s",env_opts));
  }
/* Close the input and output files before exiting. */
  for (i = 0; i < input_count; i++) {
    if (( *files[i]).ft != 0) {
      sox_close(( *files[i]).ft);
    }
    free(( *files[i]).filename);
    free(files[i]);
  }
  if (file_count != 0UL) {
    if (( *files[file_count - 1]).ft != 0) {
/* If we failed part way through */
      if (!(success != 0) && (( *( *files[file_count - 1]).ft).fp != 0)) {
/* writing a normal file, remove it. */
        struct stat st;
        if (!(stat(( *( *files[file_count - 1]).ft).filename,&st) != 0) && ((st.st_mode & 0170000) == 0100000)) 
          unlink(( *( *files[file_count - 1]).ft).filename);
      }
/* Assume we can unlink a file before closing it. */
      sox_close(( *files[file_count - 1]).ft);
    }
    free(( *files[file_count - 1]).filename);
    free(files[file_count - 1]);
  }
  free(files);
#ifdef HAVE_TERMIOS_H
  if (original_termios_saved != 0U) 
    tcsetattr(fileno(stdin),0,(&original_termios));
#endif
  free(user_efftab);
  free(( *sox_get_globals()).tmp_path);
  ( *sox_get_globals()).tmp_path = ((char *)((void *)0));
  free(play_rate_arg);
  free(effects_filename);
  free(norm_level);
  sox_quit();
  cleanup_called = 1;
}
/* Cleanup atexit() function, hence always called. */

static void atexit_cleanup()
{
/* Do not call cleanup using atexit() if possible.  pthread's can
     * act unpredictable if called outside of main().
     */
  if (!(cleanup_called != 0)) 
    cleanup();
}

static const char *str_time(double seconds)
{
  static char string[16UL][50UL];
  static int i;
  int hours;
  int mins = (seconds / 60);
  seconds -= (mins * 60);
  hours = (mins / 60);
  mins -= (hours * 60);
  i = ((i + 1) & 15);
  sprintf(string[i],"%02i:%02i:%05.2f",hours,mins,seconds);
  return string[i];
}

static const char *size_and_bitrate(sox_format_t *ft,const char **text)
{
/* ft->fp may validly be NULL, so stat not fstat */
  struct stat st;
  if ((stat((ft -> filename),&st) != 0) || ((st.st_mode & 0170000) != 0100000)) 
    return 0;
  if ((((ft -> signal.length != 0UL) && (ft -> signal.channels != 0U)) && (ft -> signal.rate != 0.00000)) && (text != 0)) {
    double secs = ((ft -> signal.length / ft -> signal.channels) / ft -> signal.rate);
     *text = lsx_sigfigs3(((8. * st.st_size) / secs));
  }
  return lsx_sigfigs3(((double )st.st_size));
}

static void play_file_info(sox_format_t *ft,file_t *f,sox_bool full)
{
  FILE *const output = (sox_mode == sox_soxi)?stdout : stderr;
  const char *text;
  const char *text2 = (const char *)((void *)0);
  char buffer[30UL];
  uint64_t ws = (ft -> signal.length / ft -> signal.channels);
  full;
  fprintf(output,"\n");
  if ((ft -> filename)[0] != 0) {
    fprintf(output,"%s:",(ft -> filename));
    if ((strcmp((ft -> filename),"-") == 0) || ((ft -> handler.flags & 2) != 0U)) 
      fprintf(output," (%s)",ft -> handler.names[0]);
    fprintf(output,"\n\n");
  }
  if ((text = size_and_bitrate(ft,&text2)) != 0) {
    fprintf(output," File Size: %-10s",text);
    if (text2 != 0) 
      fprintf(output,"Bit Rate: %s",text2);
    fprintf(output,"\n");
  }
  fprintf(output,"  Encoding: %-14s",sox_get_encodings_info()[ft -> encoding.encoding].name);
  text = sox_find_comment(( *(f -> ft)).oob.comments,"Comment");
  if (!(text != 0)) 
    text = sox_find_comment(( *(f -> ft)).oob.comments,"Description");
  if (!(text != 0)) 
    text = sox_find_comment(( *(f -> ft)).oob.comments,"Year");
  if (text != 0) 
    fprintf(output,"Info: %s",text);
  fprintf(output,"\n");
  sprintf(buffer,"  Channels: %u @ %u-bit",ft -> signal.channels,ft -> signal.precision);
  fprintf(output,"%-25s",buffer);
  text = sox_find_comment(( *(f -> ft)).oob.comments,"Tracknumber");
  if (text != 0) {
    fprintf(output,"Track: %s",text);
    text = sox_find_comment(( *(f -> ft)).oob.comments,"Tracktotal");
    if (text != 0) 
      fprintf(output," of %s",text);
  }
  fprintf(output,"\n");
  sprintf(buffer,"Samplerate: %gHz",ft -> signal.rate);
  fprintf(output,"%-25s",buffer);
  text = sox_find_comment(( *(f -> ft)).oob.comments,"Album");
  if (text != 0) 
    fprintf(output,"Album: %s",text);
  fprintf(output,"\n");
  if ((f != 0) && ((f -> replay_gain) != __builtin_huge_val())) {
    sprintf(buffer,"%s gain: %+.1fdB",( *lsx_find_enum_value((f -> replay_gain_mode),rg_modes)).text,(f -> replay_gain));
    buffer[0] += 65 - 'a';
    fprintf(output,"%-24s",buffer);
  }
  else 
    fprintf(output,"%-24s","Replaygain: off");
  text = sox_find_comment(( *(f -> ft)).oob.comments,"Artist");
  if (text != 0) 
    fprintf(output,"Artist: %s",text);
  fprintf(output,"\n");
  fprintf(output,"  Duration: %-13s",((ft -> signal.length != 0UL)?str_time((((double )ws) / ft -> signal.rate)) : "unknown"));
  text = sox_find_comment(( *(f -> ft)).oob.comments,"Title");
  if (text != 0) 
    fprintf(output,"Title: %s",text);
  fprintf(output,"\n\n");
}

static void display_file_info(sox_format_t *ft,file_t *f,sox_bool full)
{
  static const char *const no_yes[] = {("no"), ("yes")};
  FILE *const output = (sox_mode == sox_soxi)?stdout : stderr;
  const char *filetype = lsx_find_file_extension((ft -> filename));
  sox_bool show_type = sox_true;
  size_t i;
  if ((is_player != 0U) && (( *sox_get_globals()).verbosity < 3)) {
    play_file_info(ft,f,full);
    return ;
  }
  fprintf(output,"\n%s: \'%s\'",(((ft -> mode) == 'r')?"Input File     " : "Output File    "),(ft -> filename));
  if (filetype != 0) 
    for (i = 0; (ft -> handler.names[i] != 0) && (show_type != 0U); ++i) 
      if (!(strcasecmp(filetype,ft -> handler.names[i]) != 0)) 
        show_type = sox_false;
  if (show_type != 0U) 
    fprintf(output," (%s)",ft -> handler.names[0]);
  fprintf(output,"\n");
  fprintf(output,"Channels       : %u\nSample Rate    : %g\nPrecision      : %u-bit\n",ft -> signal.channels,ft -> signal.rate,ft -> signal.precision);
  if (((ft -> signal.length != 0UL) && (ft -> signal.channels != 0U)) && (ft -> signal.rate != 0.00000)) {
    uint64_t ws = (ft -> signal.length / ft -> signal.channels);
    const char *text;
    const char *text2 = (const char *)((void *)0);
    fprintf(output,"Duration       : %s = %lu samples %c %g CDDA sectors\n",str_time((((double )ws) / ft -> signal.rate)),ws,"~="[ft -> signal.rate == 44100],(((((double )ws) / ft -> signal.rate) * 44100) / 588));
    if (((ft -> mode) == 'r') && ((text = size_and_bitrate(ft,&text2)) != 0)) {
      fprintf(output,"File Size      : %s\n",text);
      if (text2 != 0) 
        fprintf(output,"Bit Rate       : %s\n",text2);
    }
  }
  if (ft -> encoding.encoding != 0U) {
    char buffer[15 * sizeof(int ) - 4 * sizeof(void *) - sizeof(size_t )] = {(0)};
    if (ft -> encoding.bits_per_sample != 0U) 
      sprintf(buffer,"%u-bit ",ft -> encoding.bits_per_sample);
    fprintf(output,"Sample Encoding: %s%s\n",buffer,sox_get_encodings_info()[ft -> encoding.encoding].desc);
  }
  if (full != 0U) {
    if ((ft -> encoding.bits_per_sample > 8) || ((ft -> handler.flags & 64) != 0U)) 
      fprintf(output,"Endian Type    : %s\n",((ft -> encoding.reverse_bytes != 0)?"big" : "little"));
    if (ft -> encoding.bits_per_sample != 0U) 
      fprintf(output,"Reverse Nibbles: %s\nReverse Bits   : %s\n",no_yes[ft -> encoding.reverse_nibbles],no_yes[ft -> encoding.reverse_bits]);
  }
  if ((f != 0) && ((f -> replay_gain) != __builtin_huge_val())) 
    fprintf(output,"Replay gain    : %+g dB (%s)\n",(f -> replay_gain),( *lsx_find_enum_value((f -> replay_gain_mode),rg_modes)).text);
  if ((f != 0) && ((f -> volume) != __builtin_huge_val())) 
    fprintf(output,"Level adjust   : %g (linear gain)\n",(f -> volume));
  if (!((ft -> handler.flags & 2) != 0U) && (ft -> oob.comments != 0)) {
    if (sox_num_comments(ft -> oob.comments) > 1) {
      sox_comments_t p = ft -> oob.comments;
      fprintf(output,"Comments       : \n");
      do 
        fprintf(output,"%s\n", *p);while ( *(++p) != 0);
    }
    else 
      fprintf(output,"Comment        : \'%s\'\n",ft -> oob.comments[0]);
  }
  fprintf(output,"\n");
}

static void report_file_info(file_t *f)
{
  if (( *sox_get_globals()).verbosity > 2) 
    display_file_info((f -> ft),f,sox_true);
}

static void progress_to_next_input_file(file_t *f,sox_effect_t *effp)
{
  if (user_skip != 0U) {
    user_skip = sox_false;
    fprintf(stderr,"\nSkipped (Ctrl-C twice to quit).\n");
  }
  read_wide_samples = 0;
  input_wide_samples = (( *(f -> ft)).signal.length / ( *(f -> ft)).signal.channels);
  if ((show_progress != 0U) && ((( *sox_get_globals()).verbosity < 3) || ((combine_method <= sox_concatenate) && (input_count > 1)))) 
    display_file_info((f -> ft),f,sox_false);
  if ((f -> volume) == __builtin_huge_val()) 
    f -> volume = 1;
  if ((f -> replay_gain) != __builtin_huge_val()) 
    f -> volume *= pow(10.0,((f -> replay_gain) / 20));
  if ((effp != 0) && ((f -> volume) != floor((f -> volume)))) 
    effp -> out_signal.precision = 32;
  ( *(f -> ft)).sox_errno = ( *__errno_location() = 0);
}
/* Read up to max `wide' samples.  A wide sample contains one sample per channel
 * from the input audio. */

static size_t sox_read_wide(sox_format_t *ft,sox_sample_t *buf,size_t max)
{
  size_t len = (max / combiner_signal.channels);
  len = (sox_read(ft,buf,(len * ft -> signal.channels)) / ft -> signal.channels);
  if (!(len != 0UL) && ((ft -> sox_errno) != 0)) 
    ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("`%s\' %s: %s",(ft -> filename),(ft -> sox_errstr),sox_strerror((ft -> sox_errno))));
  return len;
}

static void balance_input(sox_sample_t *buf,size_t ws,file_t *f)
{
  size_t s = (ws * ( *(f -> ft)).signal.channels);
  if ((f -> volume) != 1) 
    while(s-- != 0UL){
      double d = ((f -> volume) * ( *buf));
       *(buf++) = (((d < 0)?(((d <= ((sox_sample_t )(1 << 32 - 1)) - 0.5)?((++f -> volume_clips , ((sox_sample_t )(1 << 32 - 1)))) : (d - 0.5))) : (((d >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 0.5)?((++f -> volume_clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : (d + 0.5)))));
    }
}
/* The input combiner: contains one sample buffer per input file, but only
 * needed if is_parallel(combine_method) */
typedef struct __unnamed_class___F0_L511_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb____Pb__L4R__Pe____Pe___variable_name_unknown_scope_and_name__scope__ibuf__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__size_tUl__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__ilen {
sox_sample_t **ibuf;
size_t *ilen;}input_combiner_t;

static int combiner_start(sox_effect_t *effp)
{
  input_combiner_t *z = (input_combiner_t *)(effp -> priv);
  uint64_t ws;
  size_t i;
  if (combine_method <= sox_concatenate) 
    progress_to_next_input_file(files[current_input],effp);
  else {
    ws = 0;
    z -> ibuf = (lsx_realloc(0,(input_count * sizeof(( *(z -> ibuf))))));
    for (i = 0; i < input_count; i++) {
      (z -> ibuf)[i] = (lsx_realloc(0,(( *sox_get_globals()).bufsiz * sizeof(sox_sample_t ))));
      progress_to_next_input_file(files[i],effp);
      ws = ((ws >= input_wide_samples)?ws : input_wide_samples);
    }
/* Output length is that of longest input file. */
    input_wide_samples = ws;
  }
  z -> ilen = (lsx_realloc(0,(input_count * sizeof(( *(z -> ilen))))));
  return SOX_SUCCESS;
}

static sox_bool can_segue(size_t i)
{
  return ((( *( *files[i]).ft).signal.channels == ( *( *files[i - 1]).ft).signal.channels) && (( *( *files[i]).ft).signal.rate == ( *( *files[i - 1]).ft).signal.rate));
}

static int combiner_drain(sox_effect_t *effp,sox_sample_t *obuf,size_t *osamp)
{
  input_combiner_t *z = (input_combiner_t *)(effp -> priv);
  size_t ws;
  size_t s;
  size_t i;
  size_t olen = 0;
  if (combine_method <= sox_concatenate) {{
      while(1){
        if (!(user_skip != 0U)) 
          olen = sox_read_wide(( *files[current_input]).ft,obuf, *osamp);
/* If EOF, go to the next input file. */
        if (olen == 0) {
          if (++current_input < input_count) {
            if ((combine_method == sox_sequence) && !(can_segue(current_input) != 0U)) 
              break; 
            progress_to_next_input_file(files[current_input],0);
            continue; 
          }
        }
        balance_input(obuf,olen,files[current_input]);
        break; 
/* while */
      }
    }
/* is_serial */
/* else is_parallel() */
  }
  else {
    sox_sample_t *p = obuf;
    for (i = 0; i < input_count; ++i) {
      (z -> ilen)[i] = sox_read_wide(( *files[i]).ft,(z -> ibuf)[i], *osamp);
      balance_input((z -> ibuf)[i],(z -> ilen)[i],files[i]);
      olen = ((olen >= (z -> ilen)[i])?olen : (z -> ilen)[i]);
    }
/* wide samples */
    for (ws = 0; ws < olen; ++ws) {
      if ((combine_method == sox_mix) || (combine_method == sox_mix_power)) {
/* sum samples */
        for (s = 0; s < effp -> in_signal.channels; (++s , ++p)) {
           *p = 0;
          for (i = 0; i < input_count; ++i) 
            if ((ws < (z -> ilen)[i]) && (s < ( *( *files[i]).ft).signal.channels)) {
/* Cast to double prevents integer overflow */
              double sample = (( *p) + ((double )(z -> ibuf)[i][(ws * ( *( *files[i]).ft).signal.channels) + s]));
               *p = (((sample < 0)?(((sample <= ((sox_sample_t )(1 << 32 - 1)) - 0.5)?((++mixing_clips , ((sox_sample_t )(1 << 32 - 1)))) : (sample - 0.5))) : (((sample >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 0.5)?((++mixing_clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : (sample + 0.5)))));
            }
        }
/* sox_mix */
      }
      else if (combine_method == sox_multiply) {
/* multiply samples */
        for (s = 0; s < effp -> in_signal.channels; (++s , ++p)) {
          i = 0;
           *p = (((ws < (z -> ilen)[i]) && (s < ( *( *files[i]).ft).signal.channels))?(z -> ibuf)[i][(ws * ( *( *files[i]).ft).signal.channels) + s] : 0);
          for (++i; i < input_count; ++i) {
            double sample = ((( *p) * (-1. / ((sox_sample_t )(1 << 32 - 1)))) * ((((ws < (z -> ilen)[i]) && (s < ( *( *files[i]).ft).signal.channels))?(z -> ibuf)[i][(ws * ( *( *files[i]).ft).signal.channels) + s] : 0)));
             *p = (((sample < 0)?(((sample <= ((sox_sample_t )(1 << 32 - 1)) - 0.5)?((++mixing_clips , ((sox_sample_t )(1 << 32 - 1)))) : (sample - 0.5))) : (((sample >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 0.5)?((++mixing_clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : (sample + 0.5)))));
          }
        }
/* sox_multiply */
/* sox_merge: like a multi-track recorder */
      }
      else {
        for (i = 0; i < input_count; ++i) 
          for (s = 0; s < ( *( *files[i]).ft).signal.channels; ++s) 
             *(p++) = ((ws < (z -> ilen)[i]) * (z -> ibuf)[i][(ws * ( *( *files[i]).ft).signal.channels) + s]);
/* sox_merge */
      }
/* wide samples */
    }
/* is_parallel */
  }
  read_wide_samples += olen;
  olen *= effp -> in_signal.channels;
   *osamp = olen;
  input_eof = (((olen != 0UL)?sox_false : sox_true));
  if ((input_eof != 0U) && !(combine_method <= sox_concatenate)) 
    current_input += input_count;
  return (olen != 0UL)?SOX_SUCCESS : SOX_EOF;
}

static int combiner_stop(sox_effect_t *effp)
{
  input_combiner_t *z = (input_combiner_t *)(effp -> priv);
  size_t i;
  if (!(combine_method <= sox_concatenate)) {
/* Free input buffers now that they are not used */
    for (i = 0; i < input_count; i++) 
      free((z -> ibuf)[i]);
    free((z -> ibuf));
  }
  free((z -> ilen));
  return SOX_SUCCESS;
}

static const sox_effect_handler_t *input_combiner_effect_fn()
{
  static sox_effect_handler_t handler = {("input"), (0), ((16 | 256)), (0), (combiner_start), (0), (combiner_drain), (combiner_stop), (0), ((sizeof(input_combiner_t )))};
  return (&handler);
}

static int ostart(sox_effect_t *effp)
{
  unsigned int prec = effp -> out_signal.precision;
  if ((effp -> in_signal.mult != 0) && (effp -> in_signal.precision > prec)) 
     *effp -> in_signal.mult *= (1 - ((1 << (31 - prec)) * (1. / ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))));
  return SOX_SUCCESS;
}

static int output_flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  size_t len;
  (effp , ((void )obuf));
  if (show_progress != 0U) 
    for (len = 0; len <  *isamp; len += effp -> in_signal.channels) {
      omax[0] = ((omax[0] >= ibuf[len])?omax[0] : ibuf[len]);
      omin[0] = ((omin[0] <= ibuf[len])?omin[0] : ibuf[len]);
      if (effp -> in_signal.channels > 1) {
        omax[1] = ((omax[1] >= ibuf[len + 1])?omax[1] : ibuf[len + 1]);
        omin[1] = ((omin[1] <= ibuf[len + 1])?omin[1] : ibuf[len + 1]);
      }
      else {
        omax[1] = omax[0];
        omin[1] = omin[0];
      }
    }
   *osamp = 0;
  len = (( *isamp != 0UL)?sox_write(( *files[file_count - 1]).ft,ibuf, *isamp) : 0);
  output_samples += (len / ( *( *files[file_count - 1]).ft).signal.channels);
  output_eof = (((len !=  *isamp)?sox_true : sox_false));
  if (len !=  *isamp) {
    if (( *( *files[file_count - 1]).ft).sox_errno != 0) 
      ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("`%s\' %s: %s",( *( *files[file_count - 1]).ft).filename,( *( *files[file_count - 1]).ft).sox_errstr,sox_strerror(( *( *files[file_count - 1]).ft).sox_errno)));
    return SOX_EOF;
  }
  return SOX_SUCCESS;
}

static const sox_effect_handler_t *output_effect_fn()
{
  static sox_effect_handler_t handler = {("output"), (0), ((16 | 256 | 4)), ((sox_effect_handler_getopts )((void *)0)), (ostart), (output_flow), ((sox_effect_handler_drain )((void *)0)), ((sox_effect_handler_stop )((void *)0)), ((sox_effect_handler_kill )((void *)0)), (0)};
  return (&handler);
}
static void auto_effect(sox_effects_chain_t *,const char *,int ,char **,sox_signalinfo_t *,int *);

static int add_effect(sox_effects_chain_t *chain,sox_effect_t *effp,sox_signalinfo_t *in,const sox_signalinfo_t *out,int *guard)
{
  int no_guard = -1;
  switch( *guard){
    case 0:
{
      if (!((effp -> handler.flags & 128) != 0U)) {
        char *arg = "-h";
        auto_effect(chain,"gain",1,&arg,in,&no_guard);
        ++( *guard);
      }
      break; 
    }
    case 1:
{
      if ((effp -> handler.flags & 128) != 0U) {
        char *arg = "-r";
        auto_effect(chain,"gain",1,&arg,in,&no_guard);
        --( *guard);
      }
      break; 
    }
    case 2:
{
      if (!((effp -> handler.flags & 256) != 0U)) {
        ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("%s: effects that modify audio should not follow dither",effp -> handler.name));
      }
      break; 
    }
  }
  return sox_add_effect(chain,effp,in,out);
}

static void auto_effect(sox_effects_chain_t *chain,const char *name,int argc,char *(argv)[],sox_signalinfo_t *signal,int *guard)
{
  sox_effect_t *effp;
/* Should always succeed. */
  effp = sox_create_effect(sox_find_effect(name));
  if (sox_effect_options(effp,argc,argv) == SOX_EOF) 
/* The failing effect should have displayed an error message */
    exit(1);
  if (add_effect(chain,effp,signal,(&( *( *files[file_count - 1]).ft).signal),guard) != SOX_SUCCESS) 
/* The effects chain should have displayed an error message */
    exit(2);
  free(effp);
}
/* add_eff_chain() - NOTE: this only adds memory for one
 * additional effects chain beyond value of eff_chain_count.  It
 * does not unconditionally increase size of effects chain.
 */

static void add_eff_chain()
{
  user_effargs = (lsx_realloc(user_effargs,((eff_chain_count + 1) * sizeof(( *user_effargs)))));
  user_effargs[eff_chain_count] = (lsx_realloc(0,(sizeof(( *( *user_effargs))))));
  user_effargs_size = (lsx_realloc(user_effargs_size,((eff_chain_count + 1) * sizeof(( *user_effargs_size)))));
  user_effargs_size[eff_chain_count] = 0;
  nuser_effects = (lsx_realloc(nuser_effects,((eff_chain_count + 1) * sizeof(( *nuser_effects)))));
  nuser_effects[eff_chain_count] = 0;
/* add_eff_chain */
}
/* free_eff_chain() - the inverse of add_eff_chain().  Frees
 * one effects chain (with index eff_chain_count) such that
 * there are eff_chain_count left, the last having index
 * eff_chain_count-1.
 */

static void free_eff_chain()
{
  unsigned int j;
  int k;
  for (j = 0; j < nuser_effects[eff_chain_count]; j++) {
    free(user_effargs[eff_chain_count][j].name);
    user_effargs[eff_chain_count][j].name = ((char *)((void *)0));
    for (k = 0; k < user_effargs[eff_chain_count][j].argc; k++) {
      free(user_effargs[eff_chain_count][j].argv[k]);
      user_effargs[eff_chain_count][j].argv[k] = ((char *)((void *)0));
    }
    user_effargs[eff_chain_count][j].argc = 0;
    free(user_effargs[eff_chain_count][j].argv);
    user_effargs[eff_chain_count][j].argv = ((char **)((void *)0));
    user_effargs[eff_chain_count][j].argv_size = 0;
  }
  nuser_effects[eff_chain_count] = 0;
  free(user_effargs[eff_chain_count]);
/* free_eff_chain */
}

static void delete_eff_chains()
{
  while(eff_chain_count > 0){
    eff_chain_count--;
    free_eff_chain();
  }
  free(user_effargs);
  free(user_effargs_size);
  free(nuser_effects);
  user_effargs = ((struct __unnamed_class___F0_L185_C8_unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__name__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__argc__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb____Pb__c__Pe____Pe___variable_name_unknown_scope_and_name__scope__argv__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__argv_size **)((void *)0));
  user_effargs_size = ((size_t *)((void *)0));
  nuser_effects = ((unsigned int *)((void *)0));
/* delete_eff_chains */
}

static sox_bool is_pseudo_effect(const char *s)
{
  if (s != 0) 
    if (((strcmp("newfile",s) == 0) || (strcmp("restart",s) == 0)) || (strcmp(":",s) == 0)) 
      return sox_true;
  return sox_false;
/* is_pseudo_effect */
}

static void parse_effects(int argc,char **argv)
{
  while(optstate.ind < argc){{
      unsigned int eff_offset;
      size_t j;
      int newline_mode = 0;
      eff_offset = nuser_effects[eff_chain_count];
      if (eff_offset == user_effargs_size[eff_chain_count]) {
        size_t i = user_effargs_size[eff_chain_count];
        user_effargs_size[eff_chain_count] += 8;
        user_effargs[eff_chain_count] = (lsx_realloc(user_effargs[eff_chain_count],(user_effargs_size[eff_chain_count] * sizeof(( *user_effargs[eff_chain_count])))));
        for (; i < user_effargs_size[eff_chain_count]; i++) {
          user_effargs[eff_chain_count][i].argv = ((char **)((void *)0));
          user_effargs[eff_chain_count][i].argv_size = 0;
        }
      }
/* pseudo-effect ":" is used to create a new effects chain */
      if (strcmp(argv[optstate.ind],":") == 0) {
/* Only create a new chain if current one has effects.
             * Error checking will be done when loop is restarted.
             */
        if (nuser_effects[eff_chain_count] != 0) {
          eff_chain_count++;
          add_eff_chain();
        }
        optstate.ind++;
        continue; 
      }
      if (strcmp(argv[optstate.ind],"newfile") == 0) {
/* Start a new effect chain for newfile if user doesn't
             * manually do it.  Restart loop without advancing
             * optstate.ind to do error checking.
             */
        if (nuser_effects[eff_chain_count] != 0) {
          eff_chain_count++;
          add_eff_chain();
          continue; 
        }
        newline_mode = 1;
        output_method = sox_multiple;
      }
      else if (strcmp(argv[optstate.ind],"restart") == 0) {
/* Start a new effect chain for restart if user doesn't
             * manually do it.  Restart loop without advancing
             * optstate.ind to do error checking.
             */
        if (nuser_effects[eff_chain_count] != 0) {
          eff_chain_count++;
          add_eff_chain();
          continue; 
        }
        newline_mode = 1;
      }
/* Name should always be correct! */
      user_effargs[eff_chain_count][eff_offset].name = ((argv[optstate.ind] != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(argv[optstate.ind]) + 1)))),argv[optstate.ind]) : ((char *)((void *)0)));
      optstate.ind++;
      for (j = 0; ((j < ((size_t )(argc - optstate.ind))) && !(sox_find_effect(argv[optstate.ind + j]) != 0)) && !(is_pseudo_effect(argv[optstate.ind + j]) != 0U); ++j) {
        if (j >= user_effargs[eff_chain_count][eff_offset].argv_size) {
          user_effargs[eff_chain_count][eff_offset].argv_size += 8;
          user_effargs[eff_chain_count][eff_offset].argv = (lsx_realloc(user_effargs[eff_chain_count][eff_offset].argv,(user_effargs[eff_chain_count][eff_offset].argv_size * sizeof(( *user_effargs[eff_chain_count][eff_offset].argv)))));
        }
        user_effargs[eff_chain_count][eff_offset].argv[j] = ((argv[optstate.ind + j] != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(argv[optstate.ind + j]) + 1)))),argv[optstate.ind + j]) : ((char *)((void *)0)));
      }
      user_effargs[eff_chain_count][eff_offset].argc = j;
/* Skip past the effect arguments */
      optstate.ind += j;
      nuser_effects[eff_chain_count]++;
      if (newline_mode != 0) {
        eff_chain_count++;
        add_eff_chain();
      }
    }
  }
/* parse_effects */
}

static char **strtoargv(char *s,int *argc)
{
/* Single quote mode (') is in effect. */
  sox_bool squote = sox_false;
/* Double quote mode (") is in effect. */
  sox_bool dquote = sox_false;
/* Escape mode (\) is in effect. */
  sox_bool esc = sox_false;
  char *t;
  char **argv = (char **)((void *)0);
  for ( *argc = 0; ( *s) != 0; ) {
/* Skip past any (more) white space. */
    for (; (( *__ctype_b_loc())[(int )( *s)] & ((unsigned short )_ISspace)) != 0; ++s) ;
/* Found an arg. */
    if (( *s) != 0) {
      argv = (lsx_realloc(argv,(( *argc + 1) * sizeof(( *argv)))));
/* Store pointer to start of arg. */
      argv[( *argc)++] = s;
/* Find the end of the arg: */
      for (t = s; (( *s) != 0) && ((((esc != 0U) || (squote != 0U)) || (dquote != 0U)) || !((( *__ctype_b_loc())[(int )( *s)] & ((unsigned short )_ISspace)) != 0)); ++s) 
        if ((!(esc != 0U) && !(squote != 0U)) && (( *s) == '"')) 
/* Toggle double quote mode. */
          dquote = (!(dquote != 0U));
        else if ((!(esc != 0U) && !(dquote != 0U)) && (( *s) == '\'')) 
/* Toggle single quote mode. */
          squote = (!(squote != 0U));
        else if (!((esc = (((!(esc != 0U) && (( *s) == '\\')) && (s[1] != 0)) && (!(squote != 0U) && ((s[1] == '"') || !(dquote != 0U))))) != 0U)) 
/* Only copy if not an active ', ", or \ */
           *(t++) =  *s;
/* Skip the 1st white space char. */
      s = ((( *s) != 0)?(s + 1) : s);
/* Terminate the arg. */
       *t = 0;
    }
  }
  return argv;
/* strtoargv */
}

static void read_user_effects(const char *filename)
{
  FILE *file = fopen(filename,"rt");
  const size_t buffer_size_step = 1024;
  size_t buffer_size = buffer_size_step;
/* buffer for one input line */
  char *s = (lsx_realloc(0,buffer_size));
  int pos = 0;
  int argc;
  char **argv;
/* last line read consisted of ":" only */
  sox_bool last_was_colon = sox_false;
/* Free any command line options and then re-initialize to
     * starter user_effargs.
     */
  delete_eff_chains();
  current_eff_chain = 0;
  add_eff_chain();
  if (!(file != 0)) {
    ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("Cannot open effects file `%s\': %s",filename,strerror( *__errno_location())));
    exit(1);
  }
  ((( *sox_get_globals()).subsystem = "sox.c") , lsx_report_impl("Reading effects from file `%s\'",filename));
{
    while(fgets((s + pos),((int )(buffer_size - pos)),file) != 0){{
        int len = (strlen((s + pos)));
        if ((len != 0) && (s[(pos + len) - 1] == 10)) 
/* we've read a complete line */
          ((s[(pos + len) - 1] = 0) , (pos = 0));
        else if (len == ((int )((buffer_size - pos) - 1))) {
/* line was longer than buffer size */
          buffer_size += buffer_size_step;
          s = (lsx_realloc(s,buffer_size));
          pos += len;
/* read next part */
          continue; 
        }
        else {
/* something strange happened; the file might have ended
               without a '\n', might contain '\0', or a read error
               occurred */
          if (ferror(file) != 0) 
/* use error reporting after loop */
            break; 
          ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("Error reading effects file `%s\' (not a text file\?)",filename));
          exit(1);
        }
        last_was_colon = sox_false;
        argv = strtoargv(s,&argc);
        if (((argv != 0) && (argc == 1)) && (strcmp(argv[0],":") == 0)) 
          last_was_colon = sox_true;
        if (argv != 0) {
/* Make sure first option is an effect name. */
          if (!(sox_find_effect(argv[0]) != 0) && !(is_pseudo_effect(argv[0]) != 0U)) {
            ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("Cannot find an effect called `%s\'.",argv[0]));
            exit(1);
          }
/* parse_effects normally parses options from command line.
             * Reset opt index so it thinks its back at beginning of
             * main()'s argv[].
             */
          optstate.ind = 0;
          parse_effects(argc,argv);
/* Advance to next effect but only if current chain has been
             * filled in.  This recovers from side affects of pseudo-effects.
             */
          if (nuser_effects[eff_chain_count] > 0) {
            eff_chain_count++;
            add_eff_chain();
          }
          free(argv);
        }
      }
    }
  }
  if (ferror(file) != 0) {
    ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("Error reading effects file `%s\': %s",filename,strerror( *__errno_location())));
    exit(1);
  }
  fclose(file);
  free(s);
  if ((last_was_colon != 0U) || (eff_chain_count == 0)) {
/* user explicitly wanted an empty last effects chain,
           or didn't specify any chains at all */
    eff_chain_count++;
  }
  else {
/* there's one unneeded effects chain */
    free_eff_chain();
  }
/* read_user_effects */
}
/* Creates users effects and passes in user specified options.
 * This is done without putting anything into the effects chain
 * because an effect may set the effp->in_format and we may want
 * to copy that back into the input/combiner before opening and
 * inserting it.
 * Similarly, we may want to use effp->out_format to override the
 * default values of output file before we open it.
 * To keep things simple, we create all user effects.  Later, when
 * we add them, some may already be in the chain and we will need to free
 * them.
 */

static void create_user_effects()
{
  unsigned int i;
  sox_effect_t *effp;
  size_t num_effects = nuser_effects[current_eff_chain];
/* extend user_efftab, if needed */
  if (user_efftab_size < num_effects) {
    user_efftab_size = num_effects;
    user_efftab = (lsx_realloc(user_efftab,(num_effects * sizeof(( *user_efftab)))));
  }
  for (i = 0; i < num_effects; i++) {
    effp = sox_create_effect(sox_find_effect(user_effargs[current_eff_chain][i].name));
    if ((effp -> handler.flags & 64) != 0U) 
      ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("effect `%s\' is deprecated; see sox(1) for an alternative",effp -> handler.name));
    else if ((effp -> handler.flags & 512) != 0U) 
      ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("effect `%s\' is experimental/incomplete",effp -> handler.name));
    else if ((effp -> handler.flags & 1024) != 0U) {
      ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("`%s\' is a libSoX-only effect",effp -> handler.name));
      exit(1);
    }
/* The failing effect should have displayed an error message */
    if (sox_effect_options(effp,user_effargs[current_eff_chain][i].argc,user_effargs[current_eff_chain][i].argv) == SOX_EOF) 
      exit(1);
    user_efftab[i] = effp;
  }
}
/* Add all user effects to the chain.  If the output effect's rate or
 * channel count do not match the end of the effects chain then
 * insert effects to correct this.
 *
 * This can be called with the input effect already in the effects
 * chain from a previous run.  Also, it use a pre-existing
 * output effect if its been saved into save_output_eff.
 */

static void add_effects(sox_effects_chain_t *chain)
{
  sox_signalinfo_t signal = combiner_signal;
  int guard = (is_guarded - 1);
  unsigned int i;
  sox_effect_t *effp;
  char *rate_arg = (is_player != 0U)?(((play_rate_arg != 0)?play_rate_arg : "-l")) : ((char *)((void *)0));
/* 1st `effect' in the chain is the input combiner_signal.
     * add it only if its not there from a previous run.  */
  if ((chain -> length) == 0) {
    effp = sox_create_effect(input_combiner_effect_fn());
    sox_add_effect(chain,effp,&signal,(&( *( *files[file_count - 1]).ft).signal));
    free(effp);
  }
/* Add user specified effects; stop before `dither' */
  for (i = 0; (i < nuser_effects[current_eff_chain]) && (strcmp(( *user_efftab[i]).handler.name,"dither") != 0); i++) {
    if (add_effect(chain,user_efftab[i],&signal,(&( *( *files[file_count - 1]).ft).signal),&guard) != SOX_SUCCESS) 
/* Effects chain should have displayed an error message */
      exit(2);
    free(user_efftab[i]);
  }
/* Add auto effects if still needed at this point */
  if ((signal.channels < ( *( *files[file_count - 1]).ft).signal.channels) && (signal.rate != ( *( *files[file_count - 1]).ft).signal.rate)) 
    auto_effect(chain,"rate",(rate_arg != ((char *)((void *)0))),&rate_arg,&signal,&guard);
  if (signal.channels != ( *( *files[file_count - 1]).ft).signal.channels) 
    auto_effect(chain,"channels",0,0,&signal,&guard);
  if (signal.rate != ( *( *files[file_count - 1]).ft).signal.rate) 
    auto_effect(chain,"rate",(rate_arg != ((char *)((void *)0))),&rate_arg,&signal,&guard);
  if ((is_guarded != 0U) && ((do_guarded_norm != 0U) || !((signal.mult != 0) && ( *signal.mult == 1)))) {
    char *args[2UL];
    int no_guard = -1;
    args[0] = ((do_guarded_norm != 0U)?"-nh" : (((guard != 0)?"-rh" : "-h")));
    args[1] = norm_level;
    auto_effect(chain,"gain",((norm_level != 0)?2 : 1),args,&signal,&no_guard);
    guard = 1;
  }
  if ((((i == nuser_effects[current_eff_chain]) && !(no_dither != 0U)) && (signal.precision > ( *( *files[file_count - 1]).ft).signal.precision)) && (( *( *files[file_count - 1]).ft).signal.precision < 24)) 
    auto_effect(chain,"dither",0,0,&signal,&guard);
/* Add user specified effects from `dither' onwards */
  for (; i < nuser_effects[current_eff_chain]; (i++ , (guard = 2))) {
    if (add_effect(chain,user_efftab[i],&signal,(&( *( *files[file_count - 1]).ft).signal),&guard) != SOX_SUCCESS) 
/* Effects chain should have displayed an error message */
      exit(2);
    free(user_efftab[i]);
  }
  if (!(save_output_eff != 0)) {
/* Last `effect' in the chain is the output file */
    effp = sox_create_effect(output_effect_fn());
    if (sox_add_effect(chain,effp,&signal,(&( *( *files[file_count - 1]).ft).signal)) != SOX_SUCCESS) 
      exit(2);
    free(effp);
  }
  else {
    sox_push_effect_last(chain,save_output_eff);
    save_output_eff = ((sox_effect_t *)((void *)0));
  }
  for (i = 0; i < (chain -> length); ++i) {
    const char *format = ((( *sox_get_globals()).verbosity > 3)?"effects chain: %-10s %7gHz %2u channels %7s %2u bits %s" : "effects chain: %-10s %7gHz %2u channels");
    const sox_effect_t *effp = ((chain -> effects)[i] + 0);
    ((( *sox_get_globals()).subsystem = "sox.c") , lsx_report_impl(format,effp -> handler.name,effp -> out_signal.rate,effp -> out_signal.channels,(((effp -> handler.flags & 16) != 0U)?"(multi)" : ""),effp -> out_signal.precision,((effp -> out_signal.length != ((sox_uint64_t )(-1)))?str_time(((effp -> out_signal.length / effp -> out_signal.channels) / effp -> out_signal.rate)) : "unknown length")));
  }
}

static int advance_eff_chain()
{
  sox_bool reuse_output = sox_true;
  very_first_effchain = sox_false;
/* If input file reached EOF then delete all effects in current
     * chain and restart the current chain.
     *
     * This is only used with sox_sequence combine mode even though
     * we do not specifically check for that method.
     */
  if (input_eof != 0U) 
    sox_delete_effects(effects_chain);
  else {
/* If user requested to restart this effect chain then
         * do not advance to next.  Usually, this is because
         * an option to current effect was changed.
         */
    if (user_restart_eff != 0U) 
      user_restart_eff = sox_false;
    else 
/* Effect chain stopped so advance to next effect chain but
             * quite if no more chains exist.
             */
if (++current_eff_chain >= eff_chain_count) 
      return SOX_EOF;
    while((nuser_effects[current_eff_chain] == 1) && (is_pseudo_effect(user_effargs[current_eff_chain][0].name) != 0U)){
      if (strcmp("newfile",user_effargs[current_eff_chain][0].name) == 0) {
        if (++current_eff_chain >= eff_chain_count) 
          return SOX_EOF;
        reuse_output = sox_false;
      }
      else if (strcmp("restart",user_effargs[current_eff_chain][0].name) == 0) 
        current_eff_chain = 0;
    }
    if (reuse_output != 0U) 
      save_output_eff = sox_pop_effect_last(effects_chain);
    while((effects_chain -> length) > 1)
      sox_delete_effect_last(effects_chain);
  }
  return SOX_SUCCESS;
/* advance_eff_chain */
}

static uint64_t total_clips()
{
  unsigned int i;
  uint64_t clips = 0;
  for (i = 0; i < file_count; ++i) 
    clips += (( *( *files[i]).ft).clips + ( *files[i]).volume_clips);
  return (clips + mixing_clips) + sox_effects_clips(effects_chain);
}

static sox_bool since(struct timeval *then,double secs,sox_bool always_reset)
{
  sox_bool ret;
  struct timeval now;
  time_t d;
  gettimeofday(&now,0);
  d = (now.tv_sec - (then -> tv_sec));
  ret = ((d > ceil(secs)) || (((now.tv_usec - (then -> tv_usec)) + (d * 1e6)) >= (secs * 1e6)));
  if ((ret != 0U) || (always_reset != 0U)) 
     *then = now;
  return ret;
}
#define MIN_HEADROOM 6.
static double min_headroom = 6.;

static const char *vu(unsigned int channel)
{
  static struct timeval then;
  static const char *const text[][2UL] = {
/* White: 2dB steps */
{(""), ("")}, {("-"), ("-")}, {("="), ("=")}, {("-="), ("=-")}, {("=="), ("==")}, {("-=="), ("==-")}, {("==="), ("===")}, {("-==="), ("===-")}, {("===="), ("====")}, {("-===="), ("====-")}, {("====="), ("=====")}, {("-====="), ("=====-")}, {("======"), ("======")}, 
/* Red: 1dB steps */
{("!====="), ("=====!")}};
  const int red = 1;
  const int white = (sizeof(text) / sizeof(text[0]) - red);
  const double MAX = ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32));
  const double MIN = ((sox_sample_t )(1 << 32 - 1));
  double linear = ((omax[channel] / MAX) >= (omin[channel] / MIN))?(omax[channel] / MAX) : (omin[channel] / MIN);
  double dB = (log10(linear) * 20);
  int vu_dB = ((linear != 0.00000)?floor((((2 * white) + red) + dB)) : 0);
  int index = (vu_dB < (2 * white))?((((vu_dB / 2) >= 0)?(vu_dB / 2) : 0)) : ((((vu_dB - white) <= ((red + white) - 1))?(vu_dB - white) : ((red + white) - 1)));
  omax[channel] = (omin[channel] = 0);
  if (-dB < min_headroom) {
    gettimeofday(&then,0);
    min_headroom = -dB;
  }
  else if (since(&then,3.,sox_false) != 0U) 
    min_headroom = -dB;
  return text[index][channel];
}

static char *headroom()
{
  static char buff[10UL];
  unsigned int h = (unsigned int )(min_headroom * 10);
  if (min_headroom >= 6.) 
    return "      ";
  sprintf(buff,"Hd:%u.%u",(h / 10),(h % 10));
  return buff;
}

static void display_status(sox_bool all_done)
{
  static struct timeval then;
  if (!(show_progress != 0U)) 
    return ;
  if ((all_done != 0U) || (since(&then,.1,sox_false) != 0U)) {
    double read_time = (((double )read_wide_samples) / combiner_signal.rate);
    double left_time = 0;
    double in_time = 0;
    double percentage = 0;
    if (input_wide_samples != 0UL) {
      in_time = (((double )input_wide_samples) / combiner_signal.rate);
      left_time = (((in_time - read_time) >= 0)?(in_time - read_time) : 0);
      percentage = ((((100. * read_wide_samples) / input_wide_samples) >= 0)?((100. * read_wide_samples) / input_wide_samples) : 0);
    }
    fprintf(stderr,"\rIn:%-5s %s [%s] Out:%-5s [%6s|%-6s] %s Clip:%-5s",lsx_sigfigs3p(percentage),str_time(read_time),str_time(left_time),lsx_sigfigs3(((double )output_samples)),vu(0),vu(1),headroom(),lsx_sigfigs3(((double )(total_clips()))));
  }
  if (all_done != 0U) 
    fputc(10,stderr);
}
#ifdef HAVE_TERMIOS_H

static int kbhit()
{
  struct timeval time_val = {(0), (0)};
  fd_set fdset;
  do {
    int __d0;
    int __d1;
    asm volatile ("cld; rep; stosq" : "=c" (__d0), "=D" (__d1) : "a" (0), "0" ((sizeof(fd_set ) / sizeof(__fd_mask ))), "1" ((fdset.__fds_bits + 0)) : "memory");
  }while (0);
  fdset.__fds_bits[fileno(stdin) / (8 * ((int )(sizeof(__fd_mask ))))] |= (((__fd_mask )1) << (fileno(stdin) % (8 * ((int )(sizeof(__fd_mask ))))));
  select((fileno(stdin) + 1),&fdset,0,0,&time_val);
  return (fdset.__fds_bits[fileno(stdin) / (8 * ((int )(sizeof(__fd_mask ))))] & (((__fd_mask )1) << (fileno(stdin) % (8 * ((int )(sizeof(__fd_mask ))))))) != 0;
}
#elif !defined(HAVE_CONIO_H)
#define kbhit() 0
#endif
#ifdef HAVE_SOUNDCARD_H
#include <sys/ioctl.h>

static void adjust_volume(int delta)
{
  char *from_env = getenv("MIXERDEV");
  int vol1 = 0;
  int vol2 = 0;
  int fd = open(((from_env != 0)?from_env : "/dev/mixer"),2);
  if (fd >= 0) {
    if (ioctl(fd,((2U << 0 + 8 + 8 + 14 | ('M' << 0 + 8) | (4 << 0)) | sizeof(int ) << 0 + 8 + 8),&vol1) != -1) {
      int side1 = (vol1 & 0xff);
      int side2 = ((vol1 >> 8) & 0xff);
      delta = ((delta < 0)?(((delta >= -(((side1 <= side2)?side1 : side2)))?delta : -(((side1 <= side2)?side1 : side2)))) : (((delta <= (100 - (((side1 >= side2)?side1 : side2))))?delta : (100 - (((side1 >= side2)?side1 : side2))))));
      vol2 = ((((side2 + delta) << 8) + side1) + delta);
      ((( *sox_get_globals()).subsystem = "sox.c") , lsx_debug_impl("%04x %04x",vol1,vol2));
      if ((vol1 != vol2) && (ioctl(fd,(((2U | 1U) << 0 + 8 + 8 + 14 | ('M' << 0 + 8) | (4 << 0)) | sizeof(int ) << 0 + 8 + 8),&vol2) < 0)) 
        vol2 = vol1;
    }
    close(fd);
  }
  if (vol1 == vol2) 
    _IO_putc(7,stderr);
}
#elif defined(HAVE_AUDIOIO_H)
#else
#endif

static int update_status(sox_bool all_done,void *client_data)
{
  client_data;
  if (interactive != 0U) 
    while(kbhit() != 0){
#ifdef HAVE_CONIO_H
#else
      int ch = getchar();
#endif
#ifdef MORE_INTERACTIVE
/* 30 sec. */
/* FIXME: Do something if seek fails. */
/* 30 sec. */
/* FIXME: Do something if seek fails. */
/* Not very useful, eh!  Sample though of the place you
                 * could change the value to effects options
                 * like vol or speed or mixer.
                 * Modify values in user_effargs[current_eff_chain][xxx]
                 * and then chain will be drain()ed and restarted whence
                 * this function is existed.
                 */
#endif
      switch(ch){
        case 'V':
{
          adjust_volume(+7);
          break; 
        }
        case 'v':
{
          adjust_volume(-7);
          break; 
        }
      }
    }
  display_status(((all_done != 0U) || (user_abort != 0U)));
  return ((user_abort != 0U) || (user_restart_eff != 0U))?SOX_EOF : SOX_SUCCESS;
}

static void optimize_trim()
{
/* Speed hack.  If the "trim" effect is the first effect then peek inside its
     * "effect descriptor" and see what the start location is.  This has to be
     * done after its start() is called to have the correct location.  Also, only
     * do this when only working with one input file.  This is because the logic
     * to do it for multiple files is complex and probably never used.  The same
     * is true for a restarted or additional effects chain (relative positioning
     * within the file and possible samples still buffered in the input effect
     * would have to be taken into account).  This hack is a huge time savings
     * when trimming gigs of audio data into managable chunks.  */
  if ((((input_count == 1) && (very_first_effchain != 0U)) && ((effects_chain -> length) > 1)) && (strcmp((effects_chain -> effects)[1][0].handler.name,"trim") == 0)) {
    if ((( *( *files[0]).ft).handler.seek != 0) && (( *( *files[0]).ft).seekable != 0U)) {
      uint64_t offset = sox_trim_get_start(((effects_chain -> effects)[1] + 0));
      if ((offset != 0UL) && (sox_seek(( *files[0]).ft,offset,0) == SOX_SUCCESS)) {
        read_wide_samples = (offset / ( *( *files[0]).ft).signal.channels);
/* Assuming a failed seek stayed where it was.  If the seek worked then
                 * reset the start location of trim so that it thinks user didn't
                 * request a skip.  */
        sox_trim_clear_start(((effects_chain -> effects)[1] + 0));
        ((( *sox_get_globals()).subsystem = "sox.c") , lsx_debug_impl("optimize_trim successful"));
      }
    }
  }
}

static sox_bool overwrite_permitted(const char *filename)
{
  char c;
  if (!(no_clobber != 0U)) {
    ((( *sox_get_globals()).subsystem = "sox.c") , lsx_report_impl("Overwriting `%s\'",filename));
    return sox_true;
  }
  ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("Output file `%s\' already exists",filename));
  if (!(stdin_is_a_tty != 0U)) 
    return sox_false;
  do 
    fprintf(stderr,"%s sox: overwrite `%s\' (y/n)\? ",myname,filename);while ((scanf(" %c%*[^\n]",&c) != 1) || !(strchr("yYnN",c) != 0));
  return ((c == 'y') || (c == 'Y'));
}

static char *fndup_with_count(const char *filename,size_t count)
{
  char *expand_fn;
  char *efn;
  const char *fn;
  const char *ext;
  const char *end;
  sox_bool found_marker = sox_false;
  fn = filename;
  efn = (expand_fn = (lsx_realloc(0,((size_t )4096))));
/* Find extension in case user didn't specify a substitution
     * marker.
     */
  end = (ext = (filename + strlen(filename)));
  while((ext > filename) && (( *ext) != '.'))
    ext--;
/* In case extension not found, point back to end of string to do less
     * copying later.
     */
  if (( *ext) != '.') 
    ext = end;
  while(fn < end){
/* Look for %n. If found, replace with count.  Can specify an
         * option width of 1-9.
         */
    if (( *fn) == '%') {
      char width = 0;
      fn++;
      if ((( *fn) >= '1') && (( *fn) <= '9')) {
        width =  *(fn++);
      }
      if (( *fn) == 'n') {
        char format[5UL];
        found_marker = sox_true;
        if (width != 0) {
          sprintf(format,"%%0%cd",width);
        }
        else {
          strcpy(format,"%02d");
        }
        efn += sprintf(efn,format,count);
        fn++;
      }
      else 
         *(efn++) =  *(fn++);
    }
    else 
       *(efn++) =  *(fn++);
  }
   *efn = 0;
/* If user didn't tell us what to do then default to putting
     * the count right before file extension.
     */
  if (!(found_marker != 0U)) {
    efn -= strlen(ext);
    sprintf(efn,"%03lu",((unsigned long )count));
    efn = (efn + 3);
    strcat(efn,ext);
  }
  return expand_fn;
}

static void open_output_file()
{
  double factor;
  int i;
  sox_comments_t p = ( *files[file_count - 1]).oob.comments;
  sox_oob_t oob = ( *( *files[0]).ft).oob;
  char *expand_fn;
/* Skip opening file if we are not recreating output effect */
  if (save_output_eff != 0) 
    return ;
  oob.comments = sox_copy_comments(( *( *files[0]).ft).oob.comments);
  if (!(oob.comments != 0) && !(p != 0)) 
    sox_append_comment(&oob.comments,"Processed by SoX");
  else if (p != 0) {
    if (!(( *p)[0] != 0)) {
      sox_delete_comments(&oob.comments);
      ++p;
    }
    while( *p != 0)
      sox_append_comment(&oob.comments,( *(p++)));
  }
/* Copy loop info, resizing appropriately it's in samples, so # channels
     * don't matter FIXME: This doesn't work for multi-file processing or effects
     * that change file length.  */
  factor = (((double )( *files[file_count - 1]).signal.rate) / combiner_signal.rate);
  for (i = 0; i < 8; i++) {
    oob.loops[i].start = (oob.loops[i].start * factor);
    oob.loops[i].length = (oob.loops[i].length * factor);
  }
  if (output_method == sox_multiple) 
    expand_fn = fndup_with_count(( *files[file_count - 1]).filename,++output_count);
  else 
    expand_fn = ((( *files[file_count - 1]).filename != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(( *files[file_count - 1]).filename) + 1)))),( *files[file_count - 1]).filename) : ((char *)((void *)0)));
  ( *files[file_count - 1]).ft = sox_open_write(expand_fn,(&( *files[file_count - 1]).signal),(&( *files[file_count - 1]).encoding),( *files[file_count - 1]).filetype,(&oob),overwrite_permitted);
  sox_delete_comments(&oob.comments);
  free(expand_fn);
  if (!(( *files[file_count - 1]).ft != 0)) 
/* sox_open_write() will call lsx_warn for most errors.
         * Rely on that printing something. */
    exit(2);
/* If whether to enable the progress display (similar to that of ogg123) has
     * not been specified by the user, auto turn on when outputting to an audio
     * device: */
  if (show_progress == sox_option_default) 
    show_progress = (((( *( *files[file_count - 1]).ft).handler.flags & 2) != 0) && ((( *( *files[file_count - 1]).ft).handler.flags & 4) == 0));
  report_file_info(files[file_count - 1]);
}

static void sigint(int s)
{
  static struct timeval then;
  if (((((input_count > 1) && (show_progress != 0U)) && (s == 2)) && (combine_method <= sox_concatenate)) && (since(&then,1.,sox_true) != 0U)) {
    signal(2,sigint);
    user_skip = sox_true;
  }
  else 
    user_abort = sox_true;
}

static void calculate_combiner_signal_parameters()
{
  size_t i;
/* If user didn't specify # of channels then see if an effect
     * is specifying them.  This is of most use currently with the
     * synth effect were user can use null input handler and specify
     * channel counts directly in effect.  Forcing to use -c with
     * -n isn't as convenient.
     */
  for (i = 0; i < input_count; i++) {
    unsigned int j;
    for (j = 0; (j < nuser_effects[current_eff_chain]) && !(( *( *files[i]).ft).signal.channels != 0U); ++j) 
      ( *( *files[i]).ft).signal.channels = ( *user_efftab[j]).in_signal.channels;
/* For historical reasons, default to one channel if not specified. */
    if (!(( *( *files[i]).ft).signal.channels != 0U)) 
      ( *( *files[i]).ft).signal.channels = 1;
  }
/* Set the combiner output signal attributes to those of the 1st/next input
     * file.  If we are in sox_sequence mode then we don't need to check the
     * attributes of the other inputs, otherwise, it is mandatory that all input
     * files have the same sample rate, and for sox_concatenate, it is mandatory
     * that they have the same number of channels, otherwise, the number of
     * channels at the output of the combiner is calculated according to the
     * combiner mode. */
  combiner_signal = ( *( *files[current_input]).ft).signal;
  if (combine_method == sox_sequence) {
/* Report all input files; do this only the 1st time process() is called: */
    if (!(current_input != 0UL)) 
      for (i = 0; i < input_count; i++) 
        report_file_info(files[i]);
    combiner_signal.length = ((sox_uint64_t )(-1));
  }
  else {
    size_t total_channels = 0;
    size_t min_channels = (size_t )(-1);
    size_t max_channels = 0;
    size_t min_rate = (size_t )(-1);
    size_t max_rate = 0;
    uint64_t total_length = 0;
    uint64_t max_length_ws = 0;
/* Report all input files and gather info on differing rates & numbers of
         * channels, and on the resulting output audio length: */
    for (i = 0; i < input_count; i++) {
      report_file_info(files[i]);
      total_channels += ( *( *files[i]).ft).signal.channels;
      min_channels = ((min_channels <= ( *( *files[i]).ft).signal.channels)?min_channels : ( *( *files[i]).ft).signal.channels);
      max_channels = ((max_channels >= ( *( *files[i]).ft).signal.channels)?max_channels : ( *( *files[i]).ft).signal.channels);
      min_rate = (((min_rate <= ( *( *files[i]).ft).signal.rate)?min_rate : ( *( *files[i]).ft).signal.rate));
      max_rate = (((max_rate >= ( *( *files[i]).ft).signal.rate)?max_rate : ( *( *files[i]).ft).signal.rate));
      max_length_ws = ((( *( *files[i]).ft).signal.length != 0UL)?(((max_length_ws >= (( *( *files[i]).ft).signal.length / ( *( *files[i]).ft).signal.channels))?max_length_ws : (( *( *files[i]).ft).signal.length / ( *( *files[i]).ft).signal.channels))) : ((sox_uint64_t )(-1)));
      if ((total_length != ((sox_uint64_t )(-1))) && (( *( *files[i]).ft).signal.length != 0UL)) 
        total_length += ( *( *files[i]).ft).signal.length;
      else 
        total_length = ((sox_uint64_t )(-1));
    }
/* Check for invalid/unusual rate or channel combinations: */
    if (min_rate != max_rate) 
      ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("Input files must have the same sample-rate"));
/* Don't exit quite yet; give the user any other message 1st */
    if (min_channels != max_channels) {
      if (combine_method == sox_concatenate) {
        ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("Input files must have the same # channels"));
        exit(1);
      }
      else if (combine_method != sox_merge) 
        ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("Input files don\'t have the same # channels"));
    }
    if (min_rate != max_rate) 
      exit(1);
/* Store the calculated # of combined channels: */
    combiner_signal.channels = (((combine_method == sox_merge)?total_channels : max_channels));
    if (combine_method == sox_concatenate) 
      combiner_signal.length = total_length;
    else if (!(combine_method <= sox_concatenate)) 
      combiner_signal.length = ((max_length_ws != ((sox_uint64_t )(-1)))?(max_length_ws * combiner_signal.channels) : ((sox_uint64_t )(-1)));
  }
/* calculate_combiner_signal_parameters */
}

static void calculate_output_signal_parameters()
{
  sox_bool known_length = (combine_method != sox_sequence);
  size_t i;
  uint64_t olen = 0;
/* Report all input files and gather info on differing rates & numbers of
     * channels, and on the resulting output audio length: */
  for (i = 0; i < input_count; i++) {
    known_length = ((known_length != 0U) && (( *( *files[i]).ft).signal.length != 0));
    if (combine_method == sox_concatenate) 
      olen += (( *( *files[i]).ft).signal.length / ( *( *files[i]).ft).signal.channels);
    else 
      olen = ((olen >= (( *( *files[i]).ft).signal.length / ( *( *files[i]).ft).signal.channels))?olen : (( *( *files[i]).ft).signal.length / ( *( *files[i]).ft).signal.channels));
  }
/* Determine the output file signal attributes; set from user options
     * if given: */
  ( *files[file_count - 1]).signal = ofile_signal_options;
/* If no user option for output rate or # of channels, set from the last
     * effect that sets these, or from the input combiner if there is none such */
  for (i = 0; (i < nuser_effects[current_eff_chain]) && !(( *files[file_count - 1]).signal.rate != 0.00000); ++i) 
    ( *files[file_count - 1]).signal.rate = ( *user_efftab[(nuser_effects[current_eff_chain] - 1) - i]).out_signal.rate;
  for (i = 0; (i < nuser_effects[current_eff_chain]) && !(( *files[file_count - 1]).signal.channels != 0U); ++i) 
    ( *files[file_count - 1]).signal.channels = ( *user_efftab[(nuser_effects[current_eff_chain] - 1) - i]).out_signal.channels;
  if (!(( *files[file_count - 1]).signal.rate != 0.00000)) 
    ( *files[file_count - 1]).signal.rate = combiner_signal.rate;
  if (!(( *files[file_count - 1]).signal.channels != 0U)) 
    ( *files[file_count - 1]).signal.channels = combiner_signal.channels;
/* FIXME: comment this: */
  ( *files[file_count - 1]).signal.precision = combiner_signal.precision;
/* If any given user effect modifies the audio length, then we assume that
     * we don't know what the output length will be.  FIXME: in most cases,
     * an effect that modifies length will be able to determine by how much from
     * its getopts parameters, so olen should be calculable. */
  for (i = 0; i < nuser_effects[current_eff_chain]; i++) 
    known_length = ((known_length != 0U) && !((( *user_efftab[i]).handler.flags & 8) != 0U));
  if (!(known_length != 0U)) 
    olen = 0;
  ( *files[file_count - 1]).signal.length = ((uint64_t )((((olen * ( *files[file_count - 1]).signal.channels) * ( *files[file_count - 1]).signal.rate) / combiner_signal.rate) + 0.5));
}

static void set_combiner_and_output_encoding_parameters()
{
/* The input encoding parameters passed to the effects chain are those of
     * the first input file (for each segued block if sox_sequence):*/
  combiner_encoding = ( *( *files[current_input]).ft).encoding;
/* Determine the output file encoding attributes; set from user options
     * if given: */
  ( *files[file_count - 1]).encoding = ofile_encoding_options;
/* Get unspecified output file encoding attributes from the input file and
     * set the output file to the resultant encoding if this is supported by the
     * output file type; if not, the output file handler should select an
     * encoding suitable for the output signal and its precision. */
{
    sox_encodinginfo_t t = ( *files[file_count - 1]).encoding;
    if (!(t.encoding != 0U)) 
      t.encoding = combiner_encoding.encoding;
    if (!(t.bits_per_sample != 0U)) 
      t.bits_per_sample = combiner_encoding.bits_per_sample;
    if (sox_format_supports_encoding(( *files[file_count - 1]).filename,( *files[file_count - 1]).filetype,(&t)) != 0U) 
      ( *files[file_count - 1]).encoding = t;
  }
}
/* Input(s) -> Balancing -> Combiner -> Effects -> Output */

static int process()
{
  int flow_status;
  create_user_effects();
  calculate_combiner_signal_parameters();
  set_combiner_and_output_encoding_parameters();
  calculate_output_signal_parameters();
  open_output_file();
  if (!(effects_chain != 0)) 
    effects_chain = sox_create_effects_chain((&combiner_encoding),(&( *( *files[file_count - 1]).ft).encoding));
  add_effects(effects_chain);
  if (very_first_effchain != 0U) 
    optimize_trim();
#if defined(HAVE_TERMIOS_H) || defined(HAVE_CONIO_H)
  if (stdin_is_a_tty != 0U) {
    if (((show_progress != 0U) && (is_player != 0U)) && !(interactive != 0U)) {
      ((( *sox_get_globals()).subsystem = "sox.c") , lsx_debug_impl("automatically entering interactive mode"));
      interactive = sox_true;
    }
  }
  else if (interactive != 0U) {
/* User called for interactive mode, but ... */
    ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("Standard input has to be a terminal for interactive mode"));
    interactive = sox_false;
  }
#endif
#ifdef HAVE_TERMIOS_H
/* Prepare terminal for interactive mode and save the original termios
       settings. Do this only once, otherwise the "original" settings won't
       be original anymore after a second call to process() (next/restarted
       effects chain). */
  if ((interactive != 0U) && !(original_termios_saved != 0U)) {
    struct termios modified_termios;
    original_termios_saved = sox_true;
    tcgetattr(fileno(stdin),&original_termios);
    modified_termios = original_termios;
    modified_termios.c_lflag &= (~(2 | 8));
    modified_termios.c_cc[6] = (modified_termios.c_cc[5] = 0);
    tcsetattr(fileno(stdin),0,(&modified_termios));
  }
#endif
/* Stop gracefully, as soon as we possibly can. */
  signal(15,sigint);
/* Either skip current input or behave as SIGTERM. */
  signal(2,sigint);
  flow_status = sox_flow_effects(effects_chain,update_status,0);
/* Don't return SOX_EOF if
     * 1) input reach EOF and there are more input files to process or
     * 2) output didn't return EOF (disk full?) there are more
     *    effect chains.
     * For case #2, something else must decide when to stop processing.
     */
  if (((input_eof != 0U) && (current_input < input_count)) || (!(output_eof != 0U) && (current_eff_chain < eff_chain_count))) 
    flow_status = SOX_SUCCESS;
  return flow_status;
}

static void display_SoX_version(FILE *file)
{
#if HAVE_SYS_UTSNAME_H
  struct utsname uts;
#endif
  const sox_version_info_t *info = sox_version_info();
  fprintf(file,"%s:      SoX v%s%s%s\n",myname,(info -> version),(((info -> version_extra) != 0)?"-" : ""),(((info -> version_extra) != 0)?(info -> version_extra) : ""));
  if (( *sox_get_globals()).verbosity > 3) {
    if ((info -> time) != 0) 
      fprintf(file,"time:     %s\n",(info -> time));
    if ((info -> distro) != 0) 
      fprintf(file,"issue:    %s\n",(info -> distro));
#if HAVE_SYS_UTSNAME_H
    if (!(uname(&uts) != 0)) 
      fprintf(file,"uname:    %s %s %s %s %s\n",uts.sysname,uts.nodename,uts.release,uts.version,uts.machine);
#endif
    if ((info -> compiler) != 0) 
      fprintf(file,"compiler: %s\n",(info -> compiler));
    if ((info -> arch) != 0) 
      fprintf(file,"arch:     %s\n",(info -> arch));
  }
}

static int strcmp_p(const void *p1,const void *p2)
{
  return strcmp( *((const char **)p1), *((const char **)p2));
}

static void display_supported_formats()
{
  size_t i;
  size_t formats;
  const char **format_list;
  const char *const *names;
  sox_format_init();
  for (i = (formats = 0); sox_get_format_fns()[i].fn != 0; ++i) {
    const char *const *names = ( *( *sox_get_format_fns()[i].fn)()).names;
    while( *(names++) != 0)
      formats++;
  }
  format_list = (lsx_realloc(0,(formats * sizeof(( *format_list)))));
  printf("AUDIO FILE FORMATS:");
  for (i = (formats = 0); sox_get_format_fns()[i].fn != 0; ++i) {
    const sox_format_handler_t *handler = ( *sox_get_format_fns()[i].fn)();
    if (!(((handler -> flags) & 2) != 0U)) 
      for (names = (handler -> names);  *names != 0; ++names) 
        if (!(strchr( *names,'/') != 0)) 
          format_list[formats++] =  *names;
  }
  qsort(((void *)format_list),formats,(sizeof(( *format_list))),strcmp_p);
  for (i = 0; i < formats; i++) 
    printf(" %s",format_list[i]);
  putchar(10);
  printf("PLAYLIST FORMATS: m3u pls\nAUDIO DEVICE DRIVERS:");
  for (i = (formats = 0); sox_get_format_fns()[i].fn != 0; ++i) {
    const sox_format_handler_t *handler = ( *sox_get_format_fns()[i].fn)();
    if ((((handler -> flags) & 2) != 0U) && !(((handler -> flags) & 4) != 0U)) 
      for (names = (handler -> names);  *names != 0; ++names) 
        format_list[formats++] =  *names;
  }
  qsort(((void *)format_list),formats,(sizeof(( *format_list))),strcmp_p);
  for (i = 0; i < formats; i++) 
    printf(" %s",format_list[i]);
  puts("\n");
  free(((void *)format_list));
}

static void display_supported_effects()
{
  size_t i;
  const sox_effect_handler_t *e;
  printf("EFFECTS:");
  for (i = 0; sox_get_effect_fns()[i] != 0; i++) {
    e = ( *sox_get_effect_fns()[i])();
    if ((e != 0) && ((e -> name) != 0)) 
      printf(" %s%s",(e -> name),((((e -> flags) & 64) != 0U)?"*" : (((((e -> flags) & 512) != 0U)?"+" : (((((e -> flags) & 1024) != 0U)?"#" : ""))))));
  }
  puts("\n  * Deprecated effect    + Experimental effect    # LibSoX-only effect");
}

static void usage(const char *message)
{
  const sox_version_info_t *info = sox_version_info();
  size_t i;
  static const char *const lines1[] = {("SPECIAL FILENAMES (infile, outfile):"), ("-                        Pipe/redirect input/output (stdin/stdout); may need -t"), ("-d, --default-device     Use the default audio device (where available)"), ("-n, --null               Use the `null\' file handler; e.g. with synth effect"), ("-p, --sox-pipe           Alias for `-t sox -\'")};
  static const char *const linesPopen[] = {("\nSPECIAL FILENAMES (infile only):"), ("\"|program [options] ...\" Pipe input from external program (where supported)"), ("http://server/file       Use the given URL as input file (where supported)")};
  static const char *const lines2[] = {(""), ("GLOBAL OPTIONS (gopts) (can be specified at any point before the first effect):"), ("--buffer BYTES           Set the size of all processing buffers (default 8192)"), ("--clobber                Don\'t prompt to overwrite output file (default)"), ("--combine concatenate    Concatenate all input files (default for sox, rec)"), ("--combine sequence       Sequence all input files (default for play)"), ("-D, --no-dither          Don\'t dither automatically"), ("--effects-file FILENAME  File containing effects and options"), ("-G, --guard              Use temporary files to guard against clipping"), ("-h, --help               Display version number and usage information"), ("--help-effect NAME       Show usage of effect NAME, or NAME=all for all"), ("--help-format NAME       Show info on format NAME, or NAME=all for all"), ("--i, --info              Behave as soxi(1)"), ("--input-buffer BYTES     Override the input buffer size (default: as --buffer)"), ("--no-clobber             Prompt to overwrite output file"), ("-m, --combine mix        Mix multiple input files (instead of concatenating)"), ("--combine mix-power      Mix to equal power (instead of concatenating)"), ("-M, --combine merge      Merge multiple input files (instead of concatenating)")};
  static const char *const linesMagic[] = {("--magic                  Use `magic\' file-type detection")};
  static const char *const linesThreads[] = {("--multi-threaded         Enable parallel effects channels processing")};
  static const char *const lines3[] = {("--norm                   Guard (see --guard) & normalise"), ("--play-rate-arg ARG      Default `rate\' argument for auto-resample with `play\'"), ("--plot gnuplot|octave    Generate script to plot response of filter effect"), ("-q, --no-show-progress   Run in quiet mode; opposite of -S"), ("--replay-gain track|album|off  Default: off (sox, rec), track (play)"), ("-R                       Use default random numbers (same on each run of SoX)"), ("-S, --show-progress      Display progress while processing audio data"), ("--single-threaded        Disable parallel effects channels processing"), ("--temp DIRECTORY         Specify the directory to use for temporary files"), ("-T, --combine multiply   Multiply samples of corresponding channels from all"), ("                         input files (instead of concatenating)"), ("--version                Display version number of SoX and exit"), ("-V[LEVEL]                Increment or set verbosity level (default 2); levels:"), ("                           1: failure messages"), ("                           2: warnings"), ("                           3: details of processing"), ("                           4-6: increasing levels of debug messages"), ("FORMAT OPTIONS (fopts):"), ("Input file format options need only be supplied for files that are headerless."), ("Output files will have the same format as the input file where possible and not"), ("overriden by any of various means including providing output format options."), (""), ("-v|--volume FACTOR       Input file volume adjustment factor (real number)"), ("--ignore-length          Ignore input file length given in header; read to EOF"), ("-t|--type FILETYPE       File type of audio"), ("-e|--encoding ENCODING   Set encoding (ENCODING may be one of signed-integer,"), ("                         unsigned-integer, floating-point, mu-law, a-law,"), ("                         ima-adpcm, ms-adpcm, gsm-full-rate)"), ("-b|--bits BITS           Encoded sample size in bits"), ("-N|--reverse-nibbles     Encoded nibble-order"), ("-X|--reverse-bits        Encoded bit-order"), ("--endian little|big|swap Encoded byte-order; swap means opposite to default"), ("-L/-B/-x                 Short options for the above"), ("-c|--channels CHANNELS   Number of channels of audio data; e.g. 2 = stereo"), ("-r|--rate RATE           Sample rate of audio"), ("-C|--compression FACTOR  Compression factor for output format"), ("--add-comment TEXT       Append output file comment"), ("--comment TEXT           Specify comment text for the output file"), ("--comment-file FILENAME  File containing comment text for the output file"), ("--no-glob                Don\'t `glob\' wildcard match the following filename"), ("")
#if HAVE_GLOB_H
#endif
};
  if (!(( *sox_get_globals()).verbosity > 2)) {
    display_SoX_version(stdout);
    putchar(10);
  }
  if (message != 0) 
/* N.B. stderr */
    ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("%s\n",message));
  printf("Usage summary: [gopts] [[fopts] infile]... [fopts]%s [effect [effopt]]...\n\n",((sox_mode == sox_play)?"" : " outfile"));
  for (i = 0; i < sizeof(lines1) / sizeof(lines1[0]); ++i) 
    puts(lines1[i]);
  if (((info -> flags) & sox_version_have_popen) != 0U) 
    for (i = 0; i < sizeof(linesPopen) / sizeof(linesPopen[0]); ++i) 
      puts(linesPopen[i]);
  for (i = 0; i < sizeof(lines2) / sizeof(lines2[0]); ++i) 
    puts(lines2[i]);
  if (((info -> flags) & sox_version_have_magic) != 0U) 
    for (i = 0; i < sizeof(linesMagic) / sizeof(linesMagic[0]); ++i) 
      puts(linesMagic[i]);
  if (((info -> flags) & sox_version_have_threads) != 0U) 
    for (i = 0; i < sizeof(linesThreads) / sizeof(linesThreads[0]); ++i) 
      puts(linesThreads[i]);
  for (i = 0; i < sizeof(lines3) / sizeof(lines3[0]); ++i) 
    puts(lines3[i]);
  display_supported_formats();
  display_supported_effects();
  printf("EFFECT OPTIONS (effopts): effect dependent; see --help-effect\n");
  exit((message != ((const char *)((void *)0))));
}

static void usage_effect(const char *name)
{
  int i;
  display_SoX_version(stdout);
  putchar(10);
  if ((strcmp("all",name) != 0) && !(sox_find_effect(name) != 0)) {
    printf("Cannot find an effect called `%s\'.\n",name);
    display_supported_effects();
  }
  else {
    printf("Effect usage:\n\n");
    for (i = 0; sox_get_effect_fns()[i] != 0; i++) {
      const sox_effect_handler_t *e = ( *sox_get_effect_fns()[i])();
      if (((e != 0) && ((e -> name) != 0)) && (!(strcmp("all",name) != 0) || !(strcmp((e -> name),name) != 0))) {
        printf("%s %s\n",(e -> name),(((e -> usage) != 0)?(e -> usage) : ""));
        if (((e -> flags) & (64 | 512 | 1024)) != 0U) 
          putchar(10);
        if (((e -> flags) & 64) != 0U) 
          printf("`%s\' is deprecated\n",(e -> name));
        if (((e -> flags) & 512) != 0U) 
          printf("`%s\' is experimental/incomplete\n",(e -> name));
        if (((e -> flags) & 1024) != 0U) 
          printf("`%s\' is libSoX-only\n",(e -> name));
        printf("\n\n");
      }
    }
  }
  exit(1);
}

static void usage_format1(const sox_format_handler_t *f)
{
  const char *const *names;
  printf("\nFormat: %s\n",(f -> names)[0]);
  printf("Description: %s\n",(f -> description));
  if ((f -> names)[1] != 0) {
    printf("Also handles:");
    for (names = ((f -> names) + 1);  *names != 0; ++names) 
      printf(" %s", *names);
    putchar(10);
  }
  if (((f -> flags) & (256 | 512 | 1024)) != 0U) {
    printf("Channels restricted to:");
    if (((f -> flags) & 256) != 0U) 
      printf(" mono");
    if (((f -> flags) & 512) != 0U) 
      printf(" stereo");
    if (((f -> flags) & 1024) != 0U) 
      printf(" quad");
    putchar(10);
  }
  if ((f -> write_rates) != 0) {
    const sox_rate_t *p = (f -> write_rates);
    printf("Sample-rate restricted to:");
    while( *p != 0.00000)
      printf(" %g", *(p++));
    putchar(10);
  }
  printf("Reads: %s\n",((((f -> startread) != 0) || ((f -> read) != 0))?"yes" : "no"));
  if (((f -> startwrite) != 0) || ((f -> write) != 0)) {
    if ((f -> write_formats) != 0) {
      sox_encoding_t e;
      unsigned int i;
      unsigned int s;
#define enc_arg(T) (T)f->write_formats[i++]
      i = 0;
      puts("Writes:");
      while((e = ((sox_encoding_t )(f -> write_formats)[i++])) != 0U)
        do {
          s = ((unsigned int )(f -> write_formats)[i++]);
          if (sox_precision(e,s) != 0U) {
            printf("  ");
            if (s != 0U) 
              printf("%2u-bit ",s);
            printf("%s (%u-bit precision)\n",sox_get_encodings_info()[e].desc,sox_precision(e,s));
          }
        }while (s != 0U);
    }
    else 
      puts("Writes: yes");
  }
  else 
    puts("Writes: no");
}

static void usage_format(const char *name)
{
  const sox_format_handler_t *f;
  unsigned int i;
  display_SoX_version(stdout);
  if (strcmp("all",name) != 0) {
    if (!((f = sox_find_format(name,sox_false)) != 0)) {
      printf("Cannot find a format called `%s\'.\n",name);
      display_supported_formats();
    }
    else 
      usage_format1(f);
  }
  else {
    for (i = 0; sox_get_format_fns()[i].fn != 0; ++i) {
      const sox_format_handler_t *f = ( *sox_get_format_fns()[i].fn)();
      if (!(((f -> flags) & 4) != 0U)) 
        usage_format1(f);
    }
  }
  exit(1);
}

static void read_comment_file(sox_comments_t *comments,const char *const filename)
{
  int c;
  size_t text_length = 100;
  char *text = (lsx_realloc(0,(text_length + 1)));
  FILE *file = fopen(filename,"rt");
  if (file == ((FILE *)((void *)0))) {
    ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("Cannot open comment file `%s\'",filename));
    exit(1);
  }
  do {
    size_t i = 0;
    while(((c = _IO_getc(file)) != -1) && !(strchr("\r\n",c) != 0)){
      if (i == text_length) 
        text = (lsx_realloc(text,((text_length <<= 1) + 1)));
      text[i++] = c;
    }
    if (ferror(file) != 0) {
      ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("Error reading comment file `%s\'",filename));
      exit(1);
    }
    if (i != 0UL) {
      text[i] = 0;
      sox_append_comment(comments,text);
    }
  }while (c != -1);
  fclose(file);
  free(text);
}
static const char *const getoptstr = "+ab:c:de:fghimnopqr:st:uv:xABC:DGLMNRSTUV::X12348";
static const struct lsx_option_t long_options[] = {{("add-comment"), (lsx_option_arg_required), ((int *)((void *)0)), (0)}, {("buffer"), (lsx_option_arg_required), ((int *)((void *)0)), (0)}, {("combine"), (lsx_option_arg_required), ((int *)((void *)0)), (0)}, {("comment-file"), (lsx_option_arg_required), ((int *)((void *)0)), (0)}, {("comment"), (lsx_option_arg_required), ((int *)((void *)0)), (0)}, {("endian"), (lsx_option_arg_required), ((int *)((void *)0)), (0)}, {("input-buffer"), (lsx_option_arg_required), ((int *)((void *)0)), (0)}, {("interactive"), (lsx_option_arg_none), ((int *)((void *)0)), (0)}, {("help-effect"), (lsx_option_arg_required), ((int *)((void *)0)), (0)}, {("help-format"), (lsx_option_arg_required), ((int *)((void *)0)), (0)}, {("no-glob"), (lsx_option_arg_none), ((int *)((void *)0)), (0)}, {("plot"), (lsx_option_arg_required), ((int *)((void *)0)), (0)}, {("replay-gain"), (lsx_option_arg_required), ((int *)((void *)0)), (0)}, {("version"), (lsx_option_arg_none), ((int *)((void *)0)), (0)}, {("output"), (lsx_option_arg_required), ((int *)((void *)0)), (0)}, {("effects-file"), (lsx_option_arg_required), ((int *)((void *)0)), (0)}, {("temp"), (lsx_option_arg_required), ((int *)((void *)0)), (0)}, {("single-threaded"), (lsx_option_arg_none), ((int *)((void *)0)), (0)}, {("ignore-length"), (lsx_option_arg_none), ((int *)((void *)0)), (0)}, {("norm"), (lsx_option_arg_optional), ((int *)((void *)0)), (0)}, {("magic"), (lsx_option_arg_none), ((int *)((void *)0)), (0)}, {("play-rate-arg"), (lsx_option_arg_required), ((int *)((void *)0)), (0)}, {("clobber"), (lsx_option_arg_none), ((int *)((void *)0)), (0)}, {("no-clobber"), (lsx_option_arg_none), ((int *)((void *)0)), (0)}, {("multi-threaded"), (lsx_option_arg_none), ((int *)((void *)0)), (0)}, {("bits"), (lsx_option_arg_required), ((int *)((void *)0)), ('b')}, {("channels"), (lsx_option_arg_required), ((int *)((void *)0)), ('c')}, {("compression"), (lsx_option_arg_required), ((int *)((void *)0)), ('C')}, {("default-device"), (lsx_option_arg_none), ((int *)((void *)0)), ('d')}, {("no-dither"), (lsx_option_arg_none), ((int *)((void *)0)), ('D')}, {("encoding"), (lsx_option_arg_required), ((int *)((void *)0)), ('e')}, {("help"), (lsx_option_arg_none), ((int *)((void *)0)), ('h')}, {("null"), (lsx_option_arg_none), ((int *)((void *)0)), ('n')}, {("no-show-progress"), (lsx_option_arg_none), ((int *)((void *)0)), ('q')}, {("pipe"), (lsx_option_arg_none), ((int *)((void *)0)), ('p')}, {("rate"), (lsx_option_arg_required), ((int *)((void *)0)), ('r')}, {("reverse-bits"), (lsx_option_arg_none), ((int *)((void *)0)), ('X')}, {("reverse-nibbles"), (lsx_option_arg_none), ((int *)((void *)0)), ('N')}, {("show-progress"), (lsx_option_arg_none), ((int *)((void *)0)), ('S')}, {("type"), (lsx_option_arg_required), ((int *)((void *)0)), ('t')}, {("volume"), (lsx_option_arg_required), ((int *)((void *)0)), ('v')}, {("guard"), (lsx_option_arg_none), ((int *)((void *)0)), ('G')}, {((const char *)((void *)0)), (0), ((int *)((void *)0)), (0)}};

static int opt_index(int val)
{
  int i;
  for (i = 0; long_options[i].name != 0; ++i) 
    if (long_options[i].val == val) 
      return i;
  return -1;
}
static const lsx_enum_item combine_methods[] = {{("sequence"), (sox_sequence)}, {("concatenate"), (sox_concatenate)}, {("mix"), (sox_mix)}, {("mix-power"), (sox_mix_power)}, {("merge"), (sox_merge)}, {("multiply"), (sox_multiply)}, {(0), (0)}};
enum __unnamed_enum___F0_L2155_C1_ENDIAN_little__COMMA__ENDIAN_big__COMMA__ENDIAN_swap {ENDIAN_little,ENDIAN_big,ENDIAN_swap};
static const lsx_enum_item endian_options[] = {{("little"), (ENDIAN_little)}, {("big"), (ENDIAN_big)}, {("swap"), (ENDIAN_swap)}, {(0), (0)}};
static const lsx_enum_item plot_methods[] = {{("off"), (sox_plot_off)}, {("octave"), (sox_plot_octave)}, {("gnuplot"), (sox_plot_gnuplot)}, {("data"), (sox_plot_data)}, {(0), (0)}};
enum __unnamed_enum___F0_L2174_C1_encoding_signed_integer__COMMA__encoding_unsigned_integer__COMMA__encoding_floating_point__COMMA__encoding_ms_adpcm__COMMA__encoding_ima_adpcm__COMMA__encoding_oki_adpcm__COMMA__encoding_gsm_full_rate__COMMA__encoding_u_law__COMMA__encoding_a_law {encoding_signed_integer,encoding_unsigned_integer,encoding_floating_point,encoding_ms_adpcm,encoding_ima_adpcm,encoding_oki_adpcm,encoding_gsm_full_rate,encoding_u_law,encoding_a_law};
static const lsx_enum_item encodings[] = {{("signed-integer"), (encoding_signed_integer)}, {("unsigned-integer"), (encoding_unsigned_integer)}, {("floating-point"), (encoding_floating_point)}, {("ms-adpcm"), (encoding_ms_adpcm)}, {("ima-adpcm"), (encoding_ima_adpcm)}, {("oki-adpcm"), (encoding_oki_adpcm)}, {("gsm-full-rate"), (encoding_gsm_full_rate)}, {("u-law"), (encoding_u_law)}, {("mu-law"), (encoding_u_law)}, {("a-law"), (encoding_a_law)}, {(0), (0)}};

static int enum_option(const char *arg,int option_index,const lsx_enum_item *items)
{
  const lsx_enum_item *p = lsx_find_enum_text(arg,items,0);
  if (p == ((const lsx_enum_item *)((void *)0))) {
    size_t len = 1;
    char *set = (lsx_realloc(0,len));
     *set = 0;
    for (p = items; (p -> text) != 0; ++p) {
      set = (lsx_realloc(set,(len += (2 + strlen((p -> text))))));
      strcat(set,", ");
      strcat(set,(p -> text));
    }
    ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("--%s: `%s\' is not one of: %s.",long_options[option_index].name,arg,(set + 2)));
    free(set);
    exit(1);
  }
  return (p -> value);
}

static char parse_gopts_and_fopts(file_t *f)
{
  const sox_version_info_t *info = sox_version_info();
  while(1){
    int c;
/* sscanf silently accepts negative numbers for %u :( */
    int i;
/* To check for extraneous chars in optarg. */
    char dummy;
{
      switch(c = lsx_getopt(&optstate)){
/* @ one of: file-name, effect name, end of arg-list. */
        case -1:
{
/* i.e. not device. */
          return 0;
        }
/* Long options with no short equivalent. */
        case 0:
{
          switch(optstate.lngind){
            case 0:
{
              if (optstate.arg != 0) 
                sox_append_comment(&f -> oob.comments,optstate.arg);
              break; 
            }
            case 1:
{
#define SOX_BUFMIN 16
              if ((sscanf(optstate.arg,"%i %c",&i,&dummy) != 1) || (i <= 16)) {
                ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("Buffer size `%s\' must be > %d",optstate.arg,16));
                exit(1);
              }
              ( *sox_get_globals()).bufsiz = i;
              break; 
            }
            case 2:
{
              combine_method = (enum_option(optstate.arg,optstate.lngind,combine_methods));
              break; 
            }
            case 3:
{
              sox_append_comment(&f -> oob.comments,"");
              read_comment_file(&f -> oob.comments,optstate.arg);
              break; 
            }
            case 4:
{
              sox_append_comment(&f -> oob.comments,"");
              if (( *optstate.arg) != 0) 
                sox_append_comment(&f -> oob.comments,optstate.arg);
              break; 
            }
            case 5:
{
              if ((f -> encoding.reverse_bytes != sox_option_default) || (f -> encoding.opposite_endian != 0U)) 
                usage("only one endian option per file is allowed");
              switch(enum_option(optstate.arg,optstate.lngind,endian_options)){
                case 0:
{
                  f -> encoding.reverse_bytes = 0;
                  break; 
                }
                case 1:
{
                  f -> encoding.reverse_bytes = 1;
                  break; 
                }
                case 2:
{
                  f -> encoding.opposite_endian = sox_true;
                  break; 
                }
              }
              break; 
            }
            case 6:
{
              if ((sscanf(optstate.arg,"%i %c",&i,&dummy) != 1) || (i <= 16)) {
                ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("Buffer size `%s\' must be > %d",optstate.arg,16));
                exit(1);
              }
              ( *sox_get_globals()).input_bufsiz = i;
              break; 
            }
            case 7:
{
#if defined(HAVE_TERMIOS_H) || defined(HAVE_CONIO_H)
              interactive = sox_true;
              break; 
            }
#else
#endif
            case 8:
{
              usage_effect(optstate.arg);
              break; 
            }
            case 9:
{
              usage_format(optstate.arg);
              break; 
            }
            case 10:
{
              f -> no_glob = sox_true;
              break; 
            }
            case 11:
{
              ( *sox_get_effects_globals()).plot = (enum_option(optstate.arg,optstate.lngind,plot_methods));
              break; 
            }
            case 12:
{
              replay_gain_mode = (enum_option(optstate.arg,optstate.lngind,rg_modes));
              break; 
            }
            case 13:
{
              display_SoX_version(stdout);
              exit(0);
              break; 
            }
            case 14:
{
              break; 
            }
            case 15:
{
              effects_filename = ((optstate.arg != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(optstate.arg) + 1)))),optstate.arg) : ((char *)((void *)0)));
              break; 
            }
            case 16:
{
              ( *sox_get_globals()).tmp_path = ((optstate.arg != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(optstate.arg) + 1)))),optstate.arg) : ((char *)((void *)0)));
              break; 
            }
            case 17:
{
              ( *sox_get_globals()).use_threads = sox_false;
              break; 
            }
            case 18:
{
              f -> signal.length = ((sox_uint64_t )(-2));
              break; 
            }
            case 19:
{
              do_guarded_norm = (is_guarded = sox_true);
              norm_level = ((optstate.arg != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(optstate.arg) + 1)))),optstate.arg) : ((char *)((void *)0)));
              break; 
            }
            case 20:
{
              if (((info -> flags) & sox_version_have_magic) != 0U) 
                ( *sox_get_globals()).use_magic = sox_true;
              else 
                ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("this build of SoX does not include `magic\'"));
              break; 
            }
            case 21:
{
              play_rate_arg = ((optstate.arg != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(optstate.arg) + 1)))),optstate.arg) : ((char *)((void *)0)));
              break; 
            }
            case 22:
{
              no_clobber = sox_false;
              break; 
            }
            case 23:
{
              no_clobber = sox_true;
              break; 
            }
            case 24:
{
              ( *sox_get_globals()).use_threads = sox_true;
              break; 
            }
          }
          break; 
        }
        case 'G':
{
          is_guarded = sox_true;
          break; 
        }
        case 'm':
{
          combine_method = sox_mix;
          break; 
        }
        case 'M':
{
          combine_method = sox_merge;
          break; 
        }
        case 'T':
{
          combine_method = sox_multiply;
          break; 
        }
/* Useful for regression testing. */
        case 'R':
{
          ( *sox_get_globals()).repeatable = sox_true;
          break; 
        }
        case 'd':
{
        }
        case 'n':
{
        }
        case 'p':
{
          optstate.ind = optstate.ind;
          return c;
        }
        case 'h':
{
          usage(0);
          break; 
        }
        case '?':
{
/* No return */
          usage("invalid option");
          break; 
        }
        case 't':
{
          f -> filetype = optstate.arg;
          if ((f -> filetype)[0] == '.') 
            f -> filetype++;
          break; 
        }
        case 'r':
{
{
            char k = 0;
            size_t n = (sscanf(optstate.arg,"%lf %c %c",&f -> signal.rate,&k,&dummy));
            if ((((n < 1) || (f -> signal.rate <= 0)) || ((n > 1) && (k != 'k'))) || (n > 2)) {
              ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("Rate value `%s\' is not a positive number",optstate.arg));
              exit(1);
            }
            f -> signal.rate *= ((k == 'k')?1000. : 1.);
            break; 
          }
        }
        case 'v':
{
          if (sscanf(optstate.arg,"%lf %c",&f -> volume,&dummy) != 1) {
            ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("Volume value `%s\' is not a number",optstate.arg));
            exit(1);
          }
          uservolume = sox_true;
          if ((f -> volume) < 0.00000) 
            ((( *sox_get_globals()).subsystem = "sox.c") , lsx_report_impl("Volume adjustment is negative; this will result in a phase change"));
          break; 
        }
        case 'c':
{
          if ((sscanf(optstate.arg,"%d %c",&i,&dummy) != 1) || (i <= 0)) {
            ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("Channels value `%s\' is not a positive integer",optstate.arg));
            exit(1);
          }
          f -> signal.channels = i;
          break; 
        }
        case 'C':
{
          if (sscanf(optstate.arg,"%lf %c",&f -> encoding.compression,&dummy) != 1) {
            ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("Compression value `%s\' is not a number",optstate.arg));
            exit(1);
          }
          break; 
        }
        case 'b':
{
          if ((sscanf(optstate.arg,"%d %c",&i,&dummy) != 1) || (i <= 0)) {
            ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("Bits value `%s\' is not a positive integer",optstate.arg));
            exit(1);
          }
          f -> encoding.bits_per_sample = i;
          break; 
        }
        case 'e':
{
          switch(enum_option(optstate.arg,opt_index('e'),encodings)){
            case 0:
{
              f -> encoding.encoding = SOX_ENCODING_SIGN2;
              break; 
            }
            case 1:
{
              f -> encoding.encoding = SOX_ENCODING_UNSIGNED;
              break; 
            }
            case 2:
{
              f -> encoding.encoding = SOX_ENCODING_FLOAT;
              break; 
            }
            case 3:
{
              f -> encoding.encoding = SOX_ENCODING_MS_ADPCM;
              break; 
            }
            case 4:
{
              f -> encoding.encoding = SOX_ENCODING_IMA_ADPCM;
              break; 
            }
            case 5:
{
              f -> encoding.encoding = SOX_ENCODING_OKI_ADPCM;
              break; 
            }
            case 6:
{
              f -> encoding.encoding = SOX_ENCODING_GSM;
              break; 
            }
            case 7:
{
              f -> encoding.encoding = SOX_ENCODING_ULAW;
              if (f -> encoding.bits_per_sample == 0) 
                f -> encoding.bits_per_sample = 8;
              break; 
            }
            case 8:
{
              f -> encoding.encoding = SOX_ENCODING_ALAW;
              if (f -> encoding.bits_per_sample == 0) 
                f -> encoding.bits_per_sample = 8;
              break; 
            }
          }
          break; 
        }
#define DEPRECATED lsx_warn("Option `-%c' is deprecated, use `-b %i' instead.", c, (c-'0')*8)
        case '1':
{
          f -> encoding.bits_per_sample = 8;
          ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("Option `-%c\' is deprecated, use `-b %i\' instead.",c,((c - 48) * 8)));
          break; 
        }
        case '2':
{
          f -> encoding.bits_per_sample = 16;
          ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("Option `-%c\' is deprecated, use `-b %i\' instead.",c,((c - 48) * 8)));
          break; 
        }
        case '3':
{
          f -> encoding.bits_per_sample = 24;
          ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("Option `-%c\' is deprecated, use `-b %i\' instead.",c,((c - 48) * 8)));
          break; 
        }
        case '4':
{
          f -> encoding.bits_per_sample = 32;
          ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("Option `-%c\' is deprecated, use `-b %i\' instead.",c,((c - 48) * 8)));
          break; 
        }
        case '8':
{
          f -> encoding.bits_per_sample = 64;
          ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("Option `-%c\' is deprecated, use `-b %i\' instead.",c,((c - 48) * 8)));
          break; 
        }
#undef DEPRECATED
#define DEPRECATED(encname) lsx_warn("Option `-%c' is deprecated, use `-e %s' instead.", c, encname)
        case 's':
{
          f -> encoding.encoding = SOX_ENCODING_SIGN2;
          ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("Option `-%c\' is deprecated, use `-e %s\' instead.",c,"signed-integer"));
          break; 
        }
        case 'u':
{
          f -> encoding.encoding = SOX_ENCODING_UNSIGNED;
          ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("Option `-%c\' is deprecated, use `-e %s\' instead.",c,"unsigned-integer"));
          break; 
        }
        case 'f':
{
          f -> encoding.encoding = SOX_ENCODING_FLOAT;
          ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("Option `-%c\' is deprecated, use `-e %s\' instead.",c,"floating-point"));
          break; 
        }
        case 'a':
{
          f -> encoding.encoding = SOX_ENCODING_MS_ADPCM;
          ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("Option `-%c\' is deprecated, use `-e %s\' instead.",c,"ms-adpcm"));
          break; 
        }
        case 'i':
{
          f -> encoding.encoding = SOX_ENCODING_IMA_ADPCM;
          ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("Option `-%c\' is deprecated, use `-e %s\' instead.",c,"ima-adpcm"));
          break; 
        }
        case 'o':
{
          f -> encoding.encoding = SOX_ENCODING_OKI_ADPCM;
          ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("Option `-%c\' is deprecated, use `-e %s\' instead.",c,"oki-adpcm"));
          break; 
        }
        case 'g':
{
          f -> encoding.encoding = SOX_ENCODING_GSM;
          ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("Option `-%c\' is deprecated, use `-e %s\' instead.",c,"gsm-full-rate"));
          break; 
        }
        case 'U':
{
          f -> encoding.encoding = SOX_ENCODING_ULAW;
          ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("Option `-%c\' is deprecated, use `-e %s\' instead.",c,"mu-law"));
          if (f -> encoding.bits_per_sample == 0) 
            f -> encoding.bits_per_sample = 8;
          break; 
        }
        case 'A':
{
          f -> encoding.encoding = SOX_ENCODING_ALAW;
          ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("Option `-%c\' is deprecated, use `-e %s\' instead.",c,"a-law"));
          if (f -> encoding.bits_per_sample == 0) 
            f -> encoding.bits_per_sample = 8;
          break; 
        }
#undef DEPRECATED
        case 'B':
{
        }
        case 'L':
{
        }
        case 'x':
{
          if ((f -> encoding.reverse_bytes != sox_option_default) || (f -> encoding.opposite_endian != 0U)) 
            usage("only one endian option per file is allowed");
          switch(c){
            case 'L':
{
              f -> encoding.reverse_bytes = 0;
              break; 
            }
            case 'B':
{
              f -> encoding.reverse_bytes = 1;
              break; 
            }
            case 'x':
{
              f -> encoding.opposite_endian = sox_true;
              break; 
            }
          }
          break; 
        }
        case 'X':
{
          f -> encoding.reverse_bits = sox_option_yes;
          break; 
        }
        case 'N':
{
          f -> encoding.reverse_nibbles = sox_option_yes;
          break; 
        }
        case 'S':
{
          show_progress = sox_option_yes;
          break; 
        }
        case 'q':
{
          show_progress = sox_option_no;
          break; 
        }
        case 'D':
{
          no_dither = sox_true;
          break; 
        }
        case 'V':
{
          if (optstate.arg == ((const char *)((void *)0))) 
            ++( *sox_get_globals()).verbosity;
          else {
            if ((sscanf(optstate.arg,"%d %c",&i,&dummy) != 1) || (i < 0)) {
              ( *sox_get_globals()).verbosity = 2;
              ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("Verbosity value `%s\' is not a non-negative integer",optstate.arg));
              exit(1);
            }
            ( *sox_get_globals()).verbosity = ((unsigned int )i);
          }
          break; 
        }
      }
    }
  }
}

static const char *device_name(const char *const type)
{
  char *name = (char *)((void *)0);
  char *from_env = getenv("AUDIODEV");
  if (!(type != 0)) 
    return 0;
  if (!(strcmp(type,"sunau") != 0)) 
    name = "/dev/audio";
  else if (!(strcmp(type,"oss") != 0) || !(strcmp(type,"ossdsp") != 0)) 
    name = "/dev/dsp";
  else if (((((!(strcmp(type,"alsa") != 0) || !(strcmp(type,"ao") != 0)) || !(strcmp(type,"sndio") != 0)) || !(strcmp(type,"coreaudio") != 0)) || !(strcmp(type,"pulseaudio") != 0)) || !(strcmp(type,"waveaudio") != 0)) 
    name = "default";
  return ((name != 0)?(((from_env != 0)?from_env : name)) : ((char *)((void *)0)));
}

static const char *try_device(const char *name)
{
  const sox_format_handler_t *handler = sox_find_format(name,sox_false);
  if (handler != 0) {
    sox_format_t format;
    sox_format_t *ft = &format;
    ((( *sox_get_globals()).subsystem = "sox.c") , lsx_debug_impl("Looking for a default device: trying format `%s\'",name));
    memset(ft,0,(sizeof(( *ft))));
    ft -> filename = ((char *)(device_name(name)));
    ft -> priv = (((1 * (handler -> priv_size)) != 0UL)?memset(lsx_realloc(0,(1 * (handler -> priv_size))),0,(1 * (handler -> priv_size))) : ((void *)((void *)0)));
    if (( *(handler -> startwrite))(ft) == SOX_SUCCESS) {
      ( *(handler -> stopwrite))(ft);
      free((ft -> priv));
      return name;
    }
    free((ft -> priv));
  }
  return 0;
}

static const char *set_default_device(file_t *f)
{
/* Default audio driver type in order of preference: */
  if (!((f -> filetype) != 0)) 
    f -> filetype = (getenv("AUDIODRIVER"));
  if (!((f -> filetype) != 0) && (sox_find_format("coreaudio",sox_false) != 0)) 
    f -> filetype = "coreaudio";
  if (!((f -> filetype) != 0)) 
    f -> filetype = try_device("pulseaudio");
  if (!((f -> filetype) != 0) && (sox_find_format("alsa",sox_false) != 0)) 
    f -> filetype = "alsa";
  if (!((f -> filetype) != 0) && (sox_find_format("waveaudio",sox_false) != 0)) 
    f -> filetype = "waveaudio";
  if (!((f -> filetype) != 0) && (sox_find_format("sndio",sox_false) != 0)) 
    f -> filetype = "sndio";
  if (!((f -> filetype) != 0) && (sox_find_format("oss",sox_false) != 0)) 
    f -> filetype = "oss";
  if (!((f -> filetype) != 0) && (sox_find_format("sunau",sox_false) != 0)) 
    f -> filetype = "sunau";
/*!rec*/
  if ((!((f -> filetype) != 0) && (sox_find_format("ao",sox_false) != 0)) && (file_count != 0UL)) 
    f -> filetype = "ao";
  if (!((f -> filetype) != 0)) {
    ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("Sorry, there is no default audio device configured"));
    exit(1);
  }
  return device_name((f -> filetype));
}

static int add_file(const file_t *const opts,const char *const filename)
{
  file_t *f = (lsx_realloc(0,(sizeof(( *f)))));
   *f =  *opts;
  if (!(filename != 0)) 
/* No return */
    usage("missing filename");
  f -> filename = ((filename != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(filename) + 1)))),filename) : ((char *)((void *)0)));
  files = (lsx_realloc(files,((file_count + 1) * sizeof(( *files)))));
  files[file_count++] = f;
  return 0;
}
#if HAVE_GLOB_H
#ifndef GLOB_BRACE
#define GLOB_BRACE 0
#endif
#ifndef GLOB_TILDE
#define GLOB_TILDE 0
#endif

static int add_glob_file(const file_t *const opts,const char *const filename)
{
  glob_t globbuf;
  size_t i;
  if ((opts -> no_glob) != 0U) 
    return add_file(opts,filename);
  if (glob(filename,1 << 10 | 1 << 12 | 1 << 4,0,&globbuf) != 0) {
    ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("glob: %s",strerror( *__errno_location())));
    exit(1);
  }
  for (i = 0; i < globbuf.gl_pathc; ++i) 
    add_file(opts,globbuf.gl_pathv[i]);
  globfree(&globbuf);
  return 0;
}
#else
#define add_glob_file add_file
#endif

static void init_file(file_t *f)
{
  memset(f,0,(sizeof(( *f))));
  sox_init_encodinginfo(&f -> encoding);
  f -> volume = __builtin_huge_val();
  f -> replay_gain = __builtin_huge_val();
}

static void parse_options_and_filenames(int argc,char **argv)
{
  const char *env_opts = (getenv("SOX_OPTS"));
  file_t opts;
  file_t opts_none;
  (init_file(&opts) , init_file(&opts_none));
  if (sox_mode == sox_rec) 
    (add_file((&opts),set_default_device(&opts)) , init_file(&opts));
  if ((env_opts != 0) && (( *env_opts) != 0)) {
    char **argv2;
    char *str = (lsx_realloc(0,((strlen(argv[0]) + strlen(env_opts)) + 2)));
    int argc2;
    strcpy(str,argv[0]);
    strcat(str," ");
    strcat(str,env_opts);
    argv2 = strtoargv(str,&argc2);
    lsx_getopt_init(argc2,argv2,getoptstr,long_options,lsx_getopt_flag_opterr,1,&optstate);
    if ((parse_gopts_and_fopts(&opts)) != 0) {
      ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("invalid option for SOX_OPTS"));
      exit(1);
    }
    free(str);
    free(argv2);
  }
  lsx_getopt_init(argc,argv,getoptstr,long_options,lsx_getopt_flag_opterr,1,&optstate);
{
    for (; (optstate.ind < argc) && !(sox_find_effect(argv[optstate.ind]) != 0); init_file(&opts)) {
      char c = parse_gopts_and_fopts(&opts);
/* is null file? */
      if (c == 'n') {
        if ((opts.filetype != ((const char *)((void *)0))) && (strcmp(opts.filetype,"null") != 0)) 
          ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("ignoring `-t %s\'.",opts.filetype));
        opts.filetype = "null";
        add_file((&opts),"");
/* is default device? */
      }
      else if (c == 100) 
        add_file((&opts),set_default_device(&opts));
      else 
/* is sox pipe? */
if (c == 'p') {
        if ((opts.filetype != ((const char *)((void *)0))) && (strcmp(opts.filetype,"sox") != 0)) 
          ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("ignoring `-t %s\'.",opts.filetype));
        opts.filetype = "sox";
        add_file((&opts),"-");
      }
      else if ((optstate.ind >= argc) || (sox_find_effect(argv[optstate.ind]) != 0)) 
        break; 
      else if (!(sox_is_playlist(argv[optstate.ind]) != 0U)) 
        add_glob_file((&opts),argv[optstate.ind++]);
      else if (sox_parse_playlist(((sox_playlist_callback_t )add_file),(&opts),argv[optstate.ind++]) != SOX_SUCCESS) 
        exit(1);
    }
  }
  if ((env_opts != 0) && (( *env_opts) != 0)) {
    ((( *sox_get_globals()).subsystem = "sox.c") , lsx_report_impl("using SOX_OPTS=%s",env_opts));
    reported_sox_opts = sox_true;
  }
  if (sox_mode == sox_play) 
    add_file((&opts),set_default_device(&opts));
  else 
/* fopts but no file */
if (memcmp((&opts),(&opts_none),(sizeof(opts))) != 0) 
    add_file((&opts),device_name(opts.filetype));
}
static double soxi_total;
static size_t soxi_file_count;
typedef enum __unnamed_enum___F0_L2687_C9_Full__COMMA__Type__COMMA__Rate__COMMA__Channels__COMMA__Samples__COMMA__Duration__COMMA__Duration_secs__COMMA__Bits__COMMA__Bitrate__COMMA__Encoding__COMMA__Annotation {Full,Type,Rate,Channels,Samples,Duration,Duration_secs,Bits,Bitrate,Encoding,Annotation}soxi_t;

static int soxi1(const soxi_t *type,const char *filename)
{
  sox_format_t *ft = sox_open_read(filename,0,0,0);
  double secs;
  uint64_t ws;
  const char *text = (const char *)((void *)0);
  if (!(ft != 0)) 
    return 1;
  ws = (ft -> signal.length / (((ft -> signal.channels >= 1)?ft -> signal.channels : 1)));
  secs = (((double )ws) / (((ft -> signal.rate >= 1)?ft -> signal.rate : 1)));
  ++soxi_file_count;
  if ((soxi_total >= 0) && !(ws != 0UL)) 
    soxi_total = (-2);
  if (soxi_total >= 0) 
    soxi_total += ((( *type) == Samples)?ws : secs);
  switch( *type){
    case Type:
{
      printf("%s\n",(ft -> filetype));
      break; 
    }
    case Rate:
{
      printf("%g\n",ft -> signal.rate);
      break; 
    }
    case Channels:
{
      printf("%u\n",ft -> signal.channels);
      break; 
    }
    case Samples:
{
      if (soxi_total == (-1)) 
        printf("%lu\n",ws);
      break; 
    }
    case Duration:
{
      if (soxi_total == (-1)) 
        printf("%s\n",str_time(secs));
      break; 
    }
    case Duration_secs:
{
      if (soxi_total == (-1)) 
        printf("%f\n",secs);
      break; 
    }
    case Bits:
{
      printf("%u\n",ft -> encoding.bits_per_sample);
      break; 
    }
    case Bitrate:
{
      size_and_bitrate(ft,&text);
      puts(((text != 0)?text : "0"));
      break; 
    }
    case Encoding:
{
      printf("%s\n",sox_get_encodings_info()[ft -> encoding.encoding].desc);
      break; 
    }
    case Annotation:
{
      if (ft -> oob.comments != 0) {
        sox_comments_t p = ft -> oob.comments;
        do 
          printf("%s\n", *p);while ( *(++p) != 0);
      }
      break; 
    }
    case Full:
{
      display_file_info(ft,0,sox_false);
      break; 
    }
  }
  return !(!(sox_close(ft) != 0));
}

static void soxi_usage(int return_code)
{
  display_SoX_version(stdout);
  printf("\nUsage: soxi [-V[level]] [-T] [-t|-r|-c|-s|-d|-D|-b|-B|-e|-a] infile1 ...\n\n-V[n]\tIncrement or set verbosity level (default is 2)\n-T\tWith -s, -d or -D, display the total across all given files\n\n-t\tShow detected file-type\n-r\tShow sample-rate\n-c\tShow number of channels\n");
  printf("-s\tShow number of samples (0 if unavailable)\n-d\tShow duration in hours, minutes and seconds (0 if unavailable)\n-D\tShow duration in seconds (0 if unavailable)\n-b\tShow number of bits per sample (0 if not applicable)\n-B\tShow the bitrate averaged over the whole file (0 if unavailable)\n-e\tShow the name of the audio encoding\n-a\tShow file comments (annotations) if available\n\nWith no options, as much information as is available is shown for\neach given file.\n");
  exit(return_code);
}

static int soxi(int argc,char *const *argv)
{
  static const char opts[] = "trcsdDbBea\?TV::";
  soxi_t type = Full;
  int opt;
  int num_errors = 0;
  sox_bool do_total = sox_false;
  if (argc < 2) 
    soxi_usage(0);
  lsx_getopt_init(argc,argv,opts,0,lsx_getopt_flag_opterr,1,&optstate);
/* act only on last option */
  while((opt = lsx_getopt(&optstate)) > 0)
    if (opt == 'V') {
/* sscanf silently accepts negative numbers for %u :( */
      int i;
/* To check for extraneous chars in optstate.arg. */
      char dummy;
      if (optstate.arg == ((const char *)((void *)0))) 
        ++( *sox_get_globals()).verbosity;
      else {
        if ((sscanf(optstate.arg,"%d %c",&i,&dummy) != 1) || (i < 0)) {
          ( *sox_get_globals()).verbosity = 2;
          ((( *sox_get_globals()).subsystem = "sox.c") , lsx_fail_impl("Verbosity value `%s\' is not a non-negative integer",optstate.arg));
          exit(1);
        }
        ( *sox_get_globals()).verbosity = ((unsigned int )i);
      }
    }
    else if (opt == 'T') 
      do_total = sox_true;
    else if ((type = (1 + (strchr(opts,opt) - opts))) > Annotation) 
      soxi_usage(1);
  if (type == Full) 
    do_total = sox_true;
  else if ((do_total != 0U) && ((type < Samples) || (type > Duration_secs))) {
    fprintf(stderr,"soxi: ignoring -T; n/a with other given option");
    do_total = sox_false;
  }
  soxi_total = (-(!(do_total != 0U)));
  for (; optstate.ind < argc; ++optstate.ind) {
    if (sox_is_playlist(argv[optstate.ind]) != 0U) 
      num_errors += (sox_parse_playlist(((sox_playlist_callback_t )soxi1),(&type),argv[optstate.ind]) != SOX_SUCCESS);
    else 
      num_errors += soxi1((&type),argv[optstate.ind]);
  }
  if (type == Full) {
    if ((soxi_file_count > 1) && (soxi_total > 0)) 
      printf("Total Duration of %u files: %s\n",((unsigned int )soxi_file_count),str_time(soxi_total));
  }
  else if (do_total != 0U) {
    if (soxi_total < 0) 
      puts("0");
    else if (type == Duration) 
      printf("%s\n",str_time(soxi_total));
    else 
      printf("%f\n",soxi_total);
  }
  return num_errors;
}

static void set_replay_gain(sox_comments_t comments,file_t *f)
{
  rg_mode rg = replay_gain_mode;
/* Will try to find the other GAIN if preferred one not found */
  int try = 2;
  size_t i;
  size_t n = sox_num_comments(comments);
  if (rg != RG_off) 
    while(try-- != 0){
      const char *target = ((rg == RG_track)?"REPLAYGAIN_TRACK_GAIN=" : "REPLAYGAIN_ALBUM_GAIN=");
      for (i = 0; i < n; ++i) {
        if (strncasecmp(comments[i],target,strlen(target)) == 0) {
          f -> replay_gain = atof((comments[i] + strlen(target)));
          f -> replay_gain_mode = rg;
          return ;
        }
      }
      rg ^= (RG_track ^ RG_album);
    }
}

static void output_message(unsigned int level,const char *filename,const char *fmt,va_list ap)
{
  const char *const str[] = {("FAIL"), ("WARN"), ("INFO"), ("DBUG")};
  if (( *sox_get_globals()).verbosity >= level) {
    char base_name[128UL];
    sox_basename(base_name,(sizeof(base_name)),filename);
    fprintf(stderr,"%s %s %s: ",myname,str[(((level - 1) <= 3)?(level - 1) : 3)],base_name);
    vfprintf(stderr,fmt,ap);
    fprintf(stderr,"\n");
  }
}

static sox_bool cmp_comment_text(const char *c1,const char *c2)
{
  return (((c1 != 0) && (c2 != 0)) && !(strcasecmp(c1,c2) != 0));
}

int main(int argc,char **argv)
{
  size_t i;
  char mybase[6UL];
  myname = argv[0];
  ( *sox_get_globals()).output_message_handler = output_message;
  if (0 != sox_basename(mybase,(sizeof(mybase)),myname)) {
    if (0 == lsx_strcasecmp(mybase,"play")) 
      sox_mode = sox_play;
    else if (0 == lsx_strcasecmp(mybase,"rec")) 
      sox_mode = sox_rec;
    else if (0 == lsx_strcasecmp(mybase,"soxi")) 
      sox_mode = sox_soxi;
  }
  if ((!(sox_mode != 0U) && (argc > 1)) && (!(strcmp(argv[1],"--i") != 0) || !(strcmp(argv[1],"--info") != 0))) 
    (((--argc , ++argv)) , (sox_mode = sox_soxi));
  if (sox_init() != SOX_SUCCESS) 
    exit(1);
  stdin_is_a_tty = (isatty(fileno(stdin)));
/* Both isatty & fileno may set errno. */
   *__errno_location() = 0;
  atexit(atexit_cleanup);
  if (sox_mode == sox_soxi) 
    exit(soxi(argc,argv));
  parse_options_and_filenames(argc,argv);
  if (( *sox_get_globals()).verbosity > 2) 
    display_SoX_version(stderr);
  input_count = ((file_count != 0UL)?(file_count - 1) : 0);
  if (file_count != 0UL) {
    const sox_format_handler_t *handler = sox_write_handler(( *files[file_count - 1]).filename,( *files[file_count - 1]).filetype,0);
    is_player = (((handler != 0) && (((handler -> flags) & 2) != 0U)) && !(((handler -> flags) & 4) != 0U));
  }
  if (combine_method == sox_default) 
    combine_method = (((is_player != 0U)?sox_sequence : sox_concatenate));
/* Allow e.g. known length processing in this case */
  if ((combine_method == sox_sequence) && (input_count == 1)) 
    combine_method = sox_concatenate;
/* Make sure we got at least the required # of input filenames */
  if (input_count < ((size_t )(((combine_method <= sox_concatenate)?1 : 2)))) 
    usage("Not enough input filenames specified");
/* Check for misplaced input/output-specific options */
  for (i = 0; i < input_count; ++i) {
    if (( *files[i]).encoding.compression != __builtin_huge_val()) 
      usage("A compression factor can be given only for an output file");
    if (( *files[i]).oob.comments != ((char **)((void *)0))) 
      usage("Comments can be given only for an output file");
  }
  if (( *files[file_count - 1]).volume != __builtin_huge_val()) 
    usage("-v can be given only for an input file;\n\tuse the `gain\' or `vol\' effect to set the output file volume");
  if (( *files[file_count - 1]).signal.length != 0) 
    usage("--ignore-length can be given only for an input file");
/* So child pipes aren't killed by track skip */
  signal(2,((__sighandler_t )((__sighandler_t )1)));
  for (i = 0; i < input_count; i++) {
/* Open in reverse order 'cos of rec (below) */
    int j = ((input_count - 1) - i);
    file_t *f = files[j];
/* When mixing audio, default to input side volume adjustments that will
         * make sure no clipping will occur.  Users probably won't be happy with
         * this, and will override it, possibly causing clipping to occur. */
    if ((combine_method == sox_mix) && !(uservolume != 0U)) 
      f -> volume = (1. / input_count);
    else if ((combine_method == sox_mix_power) && !(uservolume != 0U)) 
      f -> volume = (1. / sqrt(((double )input_count)));
/* Set the recording parameters: */
    if ((sox_mode == sox_rec) && !(j != 0)) {
/* from the (just openned) next */
      if (input_count > 1) {
/* input file, or from the output */
        f -> signal = ( *( *files[1]).ft).signal;
        f -> encoding = ( *( *files[1]).ft).encoding;
      }
      else {
/* file (which is not open yet). */
        f -> signal = ( *files[1]).signal;
        f -> encoding = ( *files[1]).encoding;
      }
    }
    ( *files[j]).ft = sox_open_read((f -> filename),(&f -> signal),(&f -> encoding),(f -> filetype));
    if (!(( *files[j]).ft != 0)) 
/* sox_open_read() will call lsx_warn for most errors.
             * Rely on that printing something. */
      exit(2);
    if (((show_progress == sox_option_default) && ((( *( *files[j]).ft).handler.flags & 2) != 0)) && ((( *( *files[j]).ft).handler.flags & 4) == 0)) 
      show_progress = sox_option_yes;
  }
  if (replay_gain_mode == RG_default) 
    replay_gain_mode = (((is_player != 0U)?(((((input_count > 1) && (cmp_comment_text(sox_find_comment(( *( *files[0]).ft).oob.comments,"artist"),sox_find_comment(( *( *files[1]).ft).oob.comments,"artist")) != 0U)) && (cmp_comment_text(sox_find_comment(( *( *files[0]).ft).oob.comments,"album"),sox_find_comment(( *( *files[1]).ft).oob.comments,"album")) != 0U))?RG_album : RG_track)) : RG_off));
/* Simple heuristic to determine if */
/* replay-gain should be in album mode */
  for (i = 0; i < input_count; i++) 
    set_replay_gain(( *( *files[i]).ft).oob.comments,files[i]);
  signal(2,0);
/* Loop through the rest of the arguments looking for effects */
  add_eff_chain();
  parse_effects(argc,argv);
  eff_chain_count++;
/* Note: Purposely not calling add_eff_chain() to save some
     * memory although it would be more consistent to do so.
     */
/* Not the best way for users to do this; now deprecated in favour of soxi. */
  if (((!(show_progress != 0U) && !(nuser_effects[current_eff_chain] != 0U)) && (( *files[file_count - 1]).filetype != 0)) && !(strcmp(( *files[file_count - 1]).filetype,"null") != 0)) {
    for (i = 0; i < input_count; i++) 
      report_file_info(files[i]);
    exit(0);
  }
/* Re-seed PRNG? */
  if (!(( *sox_get_globals()).repeatable != 0U)) {
    struct timeval now;
    gettimeofday(&now,0);
    ( *sox_get_globals()).ranqd1 = ((int32_t )(now.tv_sec - now.tv_usec));
  }
/* Save things that sox_sequence needs to be reinitialised for each segued
     * block of input files.*/
  ofile_signal_options = ( *files[file_count - 1]).signal;
  ofile_encoding_options = ( *files[file_count - 1]).encoding;
/* If user specified an effects filename then use that file
     * to load user effects.  Free any previously specified options
     * from the command line.
     */
  if (effects_filename != 0) {
    read_user_effects(effects_filename);
  }
{
    while(((process() != SOX_EOF) && !(user_abort != 0U)) && (current_input < input_count)){
      if (advance_eff_chain() == SOX_EOF) 
        break; 
      if (!(save_output_eff != 0)) {
        sox_close(( *files[file_count - 1]).ft);
        ( *files[file_count - 1]).ft = ((sox_format_t *)((void *)0));
      }
    }
  }
  sox_delete_effects_chain(effects_chain);
  delete_eff_chains();
  for (i = 0; i < file_count; ++i) 
    if (( *( *files[i]).ft).clips != 0) 
      ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl((((i < input_count)?"`%s\' input clipped %lu samples" : "`%s\' output clipped %lu samples; decrease volume\?")),(((( *( *files[i]).ft).handler.flags & 2) != 0U)?( *( *files[i]).ft).handler.names[0] : ( *( *files[i]).ft).filename),( *( *files[i]).ft).clips));
  if (mixing_clips > 0) 
    ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("mix-combining clipped %lu samples; decrease volume\?",mixing_clips));
  for (i = 0; i < file_count; i++) 
    if (( *files[i]).volume_clips > 0) 
      ((( *sox_get_globals()).subsystem = "sox.c") , lsx_warn_impl("`%s\' balancing clipped %lu samples; decrease volume\?",( *files[i]).filename,( *files[i]).volume_clips));
  if (show_progress != 0U) {
    if (user_abort != 0U) 
      fprintf(stderr,"Aborted.\n");
    else if ((user_skip != 0U) && (sox_mode != sox_rec)) 
      fprintf(stderr,"Skipped.\n");
    else 
      fprintf(stderr,"Done.\n");
  }
/* Signal success to cleanup so the output file isn't removed. */
  success = 1;
  cleanup();
  return 0;
}
