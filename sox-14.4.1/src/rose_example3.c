/* Simple example of using SoX libraries
 *
 * Copyright (c) 2007-9 robs@users.sourceforge.net
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#ifdef NDEBUG /* N.B. assert used with active statements so enable always. */
#undef NDEBUG /* Must undef above assert.h or other that might include it. */
#endif
#include "sox.h"
#include "util.h"
#include <stdio.h>
#include <assert.h>
/*
 * Example of a custom output message handler.
 */

static void output_message(unsigned int level,const char *filename,const char *fmt,va_list ap)
{
  const char *const str[] = {("FAIL"), ("WARN"), ("INFO"), ("DBUG")};
  if (( *sox_get_globals()).verbosity >= level) {
    char base_name[128UL];
    sox_basename(base_name,(sizeof(base_name)),filename);
    fprintf(stderr,"%s %s: ",str[(((level - 1) <= 3)?(level - 1) : 3)],base_name);
    vfprintf(stderr,fmt,ap);
    fprintf(stderr,"\n");
  }
}
/*
 * On an alsa capable system, plays an audio file starting 10 seconds in.
 * Copes with sample-rate and channel change if necessary since its
 * common for audio drivers to to support subset of rates and channel
 * counts..
 * E.g. example3 song2.ogg
 *
 * Can easily be changed to work with other audio device drivers supported
 * by libSoX; e.g. "oss", "ao", "coreaudio", etc.
 * See the soxformat(7) manual page.
 */

int main(int argc,char *argv[])
{
/* input and output files */
  static sox_format_t *in;
  static sox_format_t *out;
  sox_effects_chain_t *chain;
  sox_effect_t *e;
  char *args[10UL];
  (argc == 2)?((void )0) : __assert_fail("argc == 2","example3.c",62,"int main(int, char **)");
  ( *sox_get_globals()).output_message_handler = output_message;
  ( *sox_get_globals()).verbosity = 1;
  (sox_init() == SOX_SUCCESS)?((void )0) : __assert_fail("sox_init() == SOX_SUCCESS","example3.c",66,"int main(int, char **)");
  ((in = sox_open_read(argv[1],0,0,0)) != 0)?((void )0) : __assert_fail("in = sox_open_read(argv[1], ((void *)0), ((void *)0), ((void *)0))","example3.c",67,"int main(int, char **)");
/* Change "alsa" in this line to use an alternative audio device driver: */
  ((out = sox_open_write("default",(&in -> signal),0,"alsa",0,0)) != 0)?((void )0) : __assert_fail("out= sox_open_write(\"default\", &in->signal, ((void *)0), \"alsa\", ((void *)0), ((void *)0))","example3.c",69,"int main(int, char **)");
  chain = sox_create_effects_chain((&in -> encoding),(&out -> encoding));
  e = sox_create_effect(sox_find_effect("input"));
  ((args[0] = ((char *)in)) , ((sox_effect_options(e,1,args) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_effect_options(e, 1, args) == SOX_SUCCESS","example3.c",74,"int main(int, char **)")));
  (sox_add_effect(chain,e,&in -> signal,(&in -> signal)) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_add_effect(chain, e, &in->signal, &in->signal) == SOX_SUCCESS","example3.c",75,"int main(int, char **)");
  free(e);
  e = sox_create_effect(sox_find_effect("trim"));
  ((args[0] = "10") , ((sox_effect_options(e,1,args) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_effect_options(e, 1, args) == SOX_SUCCESS","example3.c",79,"int main(int, char **)")));
  (sox_add_effect(chain,e,&in -> signal,(&in -> signal)) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_add_effect(chain, e, &in->signal, &in->signal) == SOX_SUCCESS","example3.c",80,"int main(int, char **)");
  free(e);
  if (in -> signal.rate != out -> signal.rate) {
    e = sox_create_effect(sox_find_effect("rate"));
    (sox_effect_options(e,0,0) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_effect_options(e, 0, ((void *)0)) == SOX_SUCCESS","example3.c",85,"int main(int, char **)");
    (sox_add_effect(chain,e,&in -> signal,(&out -> signal)) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_add_effect(chain, e, &in->signal, &out->signal) == SOX_SUCCESS","example3.c",86,"int main(int, char **)");
    free(e);
  }
  if (in -> signal.channels != out -> signal.channels) {
    e = sox_create_effect(sox_find_effect("channels"));
    (sox_effect_options(e,0,0) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_effect_options(e, 0, ((void *)0)) == SOX_SUCCESS","example3.c",92,"int main(int, char **)");
    (sox_add_effect(chain,e,&in -> signal,(&out -> signal)) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_add_effect(chain, e, &in->signal, &out->signal) == SOX_SUCCESS","example3.c",93,"int main(int, char **)");
    free(e);
  }
  e = sox_create_effect(sox_find_effect("output"));
  ((args[0] = ((char *)out)) , ((sox_effect_options(e,1,args) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_effect_options(e, 1, args) == SOX_SUCCESS","example3.c",98,"int main(int, char **)")));
  (sox_add_effect(chain,e,&in -> signal,(&out -> signal)) == SOX_SUCCESS)?((void )0) : __assert_fail("sox_add_effect(chain, e, &in->signal, &out->signal) == SOX_SUCCESS","example3.c",99,"int main(int, char **)");
  free(e);
  sox_flow_effects(chain,0,0);
  sox_delete_effects_chain(chain);
  sox_close(out);
  sox_close(in);
  sox_quit();
  return 0;
}
