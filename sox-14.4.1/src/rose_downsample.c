/* libSoX effect: Downsample
 *
 * First version of this effect written 11/2011 by Ulrich Klauer.
 *
 * Copyright 2011 Chris Bagwell and SoX Contributors
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
typedef struct __unnamed_class___F0_L24_C9_unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__factor__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__carry {
unsigned int factor;
/* number of samples still to be discarded,
                         carried over from last block */
unsigned int carry;}priv_t;

static int create(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  p -> factor = 2;
  (--argc , ++argv);
{
/* break-able block */
    do {{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 1) || (d > 16384)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "downsample.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","factor",((double )1),((double )16384)));
            return lsx_usage(effp);
          }
          p -> factor = d;
          (--argc , ++argv);
        }
      }
    }while (0);
  }
  return (argc != 0)?lsx_usage(effp) : SOX_SUCCESS;
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  effp -> out_signal.rate = (effp -> in_signal.rate / (p -> factor));
  return ((p -> factor) == 1)?32 : SOX_SUCCESS;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t ilen =  *isamp;
  size_t olen =  *osamp;
  size_t t;
  t = (((p -> carry) <= ilen)?(p -> carry) : ilen);
  p -> carry -= t;
  ibuf += t;
  ilen -= t;
/* NB: either p->carry (usually) or ilen is now zero; hence, a
     non-zero value of ilen implies p->carry == 0, and there is no
     need to test for this in the following while and if. */
  while((ilen >= (p -> factor)) && (olen != 0UL)){
     *(obuf++) =  *ibuf;
    ibuf += (p -> factor);
    olen--;
    ilen -= (p -> factor);
  }
  if ((ilen != 0UL) && (olen != 0UL)) {
     *(obuf++) =  *ibuf;
    p -> carry = ((p -> factor) - ilen);
    olen--;
    ilen = 0;
  }
  (( *isamp -= ilen) , ( *osamp -= olen));
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_downsample_effect_fn()
{
  static sox_effect_handler_t handler = {("downsample"), ("[factor (2)]"), ((2 | 256)), (create), (start), (flow), ((sox_effect_handler_drain )((void *)0)), ((sox_effect_handler_stop )((void *)0)), ((sox_effect_handler_kill )((void *)0)), ((sizeof(priv_t )))};
  return (&handler);
}
