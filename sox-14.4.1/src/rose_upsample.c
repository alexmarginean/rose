/* libSoX effect: Upsample (zero stuff)    (c) 2011 robs@users.sourceforge.net
 *
 * Sometimes filters perform better at higher sampling rates, so e.g.
 *   sox -r 48k input output upsample 4 filter rate 48k vol 4
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
typedef struct __unnamed_class___F0_L23_C9_unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__factor__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__pos {
unsigned int factor;
unsigned int pos;}priv_t;

static int create(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  p -> factor = 2;
  (--argc , ++argv);
{
    do {{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 1) || (d > 256)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "upsample.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","factor",((double )1),((double )256)));
            return lsx_usage(effp);
          }
          p -> factor = d;
          (--argc , ++argv);
        }
      }
    }while (0);
  }
  return (argc != 0)?lsx_usage(effp) : SOX_SUCCESS;
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  effp -> out_signal.rate = (effp -> in_signal.rate * (p -> factor));
  return ((p -> factor) == 1)?32 : SOX_SUCCESS;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t ilen =  *isamp;
  size_t olen =  *osamp;
{
    while(1){
      for (; ((p -> pos) != 0U) && (olen != 0UL); ((p -> pos = (((p -> pos) + 1) % (p -> factor))) , --olen)) 
         *(obuf++) = 0;
      if (!(ilen != 0UL) || !(olen != 0UL)) 
        break; 
       *(obuf++) =  *(ibuf++);
      (--olen , --ilen);
      ++p -> pos;
    }
  }
  (( *isamp -= ilen) , ( *osamp -= olen));
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_upsample_effect_fn()
{
  static sox_effect_handler_t handler = {("upsample"), ("[factor (2)]"), ((2 | 256)), (create), (start), (flow), ((sox_effect_handler_drain )((void *)0)), ((sox_effect_handler_stop )((void *)0)), ((sox_effect_handler_kill )((void *)0)), ((sizeof(priv_t )))};
  return (&handler);
}
