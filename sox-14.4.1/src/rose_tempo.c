/* libSoX effect: change tempo (and duration) or pitch (maintain duration)
 * Copyright (c) 2007,8 robs@users.sourceforge.net
 * Based on ideas from Olli Parviainen's SoundTouch Library.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include "fifo.h"
#include <math.h>
typedef struct __unnamed_class___F0_L24_C9_unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__channels__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__quick_search__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__factor__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__search__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__segment__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__overlap__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__process_size__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_fifo_tL126R__typedef_declaration_variable_name_unknown_scope_and_name__scope__input_fifo__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__f__Pe___variable_name_unknown_scope_and_name__scope__overlap_buf__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_fifo_tL126R__typedef_declaration_variable_name_unknown_scope_and_name__scope__output_fifo__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__samples_in__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__samples_out__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__segments_total__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__skip_total {
/* Configuration parameters: */
size_t channels;
/* Whether to quick search or linear search */
sox_bool quick_search;
/* 1 for no change, < 1 for slower, > 1 for faster. */
double factor;
/* Wide samples to search for best overlap position */
size_t search;
/* Processing segment length in wide samples */
size_t segment;
/* In wide samples */
size_t overlap;
/* # input wide samples needed to process 1 segment */
size_t process_size;
/* Buffers: */
fifo_t input_fifo;
float *overlap_buf;
fifo_t output_fifo;
/* Counters: */
uint64_t samples_in;
uint64_t samples_out;
uint64_t segments_total;
uint64_t skip_total;}tempo_t;
/* Waveform Similarity by least squares; works across multi-channels */

static float difference(const float *a,const float *b,size_t length)
{
  float diff = 0;
  size_t i = 0;
  #define _ diff += sqr(a[i] - b[i]), ++i; /* Loop optimisation */
/* N.B. length ≡ 0 (mod 8) */
  do {
    ((diff += ((a[i] - b[i]) * (a[i] - b[i]))) , ++i);
    ((diff += ((a[i] - b[i]) * (a[i] - b[i]))) , ++i);
    ((diff += ((a[i] - b[i]) * (a[i] - b[i]))) , ++i);
    ((diff += ((a[i] - b[i]) * (a[i] - b[i]))) , ++i);
    ((diff += ((a[i] - b[i]) * (a[i] - b[i]))) , ++i);
    ((diff += ((a[i] - b[i]) * (a[i] - b[i]))) , ++i);
    ((diff += ((a[i] - b[i]) * (a[i] - b[i]))) , ++i);
    ((diff += ((a[i] - b[i]) * (a[i] - b[i]))) , ++i);
  }while (i < length);
  #undef _
  return diff;
}
/* Find where the two segments are most alike over the overlap period. */

static size_t tempo_best_overlap_position(tempo_t *t,const float *new_win)
{
  float *f = (t -> overlap_buf);
  size_t j;
  size_t best_pos;
  size_t prev_best_pos = (((t -> search) + 1) >> 1);
  size_t step = 64;
  size_t i = (best_pos = (((t -> quick_search) != 0U)?prev_best_pos : 0));
  float diff;
  float least_diff = difference((new_win + ((t -> channels) * i)),f,((t -> channels) * (t -> overlap)));
  int k = 0;
/* hierarchical search */
  if ((t -> quick_search) != 0U) 
    do {
      for (k = -1; k <= 1; k += 2) {
        for (j = 1; (j < 4) || (step == 64); ++j) {
          i = (prev_best_pos + ((k * j) * step));
          if ((((int )i) < 0) || (i >= (t -> search))) 
            break; 
          diff = difference((new_win + ((t -> channels) * i)),f,((t -> channels) * (t -> overlap)));
          if (diff < least_diff) 
            ((least_diff = diff) , (best_pos = i));
        }
      }
      prev_best_pos = best_pos;
    }while ((step >>= 2) != 0UL);
  else 
/* linear search */
    for (i = 1; i < (t -> search); i++) {
      diff = difference((new_win + ((t -> channels) * i)),f,((t -> channels) * (t -> overlap)));
      if (diff < least_diff) 
        ((least_diff = diff) , (best_pos = i));
    }
  return best_pos;
}

static void tempo_overlap(tempo_t *t,const float *in1,const float *in2,float *output)
{
  size_t i;
  size_t j;
  size_t k = 0;
  float fade_step = (1.0f / ((float )(t -> overlap)));
  for (i = 0; i < (t -> overlap); ++i) {
    float fade_in = (fade_step * ((float )i));
    float fade_out = (1.0f - fade_in);
    for (j = 0; j < (t -> channels); (++j , ++k)) 
      output[k] = ((in1[k] * fade_out) + (in2[k] * fade_in));
  }
}

static void tempo_process(tempo_t *t)
{
  while(fifo_occupancy(&t -> input_fifo) >= (t -> process_size)){
    size_t skip;
    size_t offset;
/* Copy or overlap the first bit to the output */
    if (!((t -> segments_total) != 0UL)) {
      offset = ((t -> search) / 2);
      fifo_write(&t -> output_fifo,(t -> overlap),(((float *)(fifo_read(&t -> input_fifo,((size_t )0),0))) + ((t -> channels) * offset)));
    }
    else {
      offset = tempo_best_overlap_position(t,(fifo_read(&t -> input_fifo,((size_t )0),0)));
      tempo_overlap(t,(t -> overlap_buf),(((float *)(fifo_read(&t -> input_fifo,((size_t )0),0))) + ((t -> channels) * offset)),(fifo_write(&t -> output_fifo,(t -> overlap),0)));
    }
/* Copy the middle bit to the output */
    fifo_write(&t -> output_fifo,((t -> segment) - (2 * (t -> overlap))),(((float *)(fifo_read(&t -> input_fifo,((size_t )0),0))) + ((t -> channels) * (offset + (t -> overlap)))));
/* Copy the end bit to overlap_buf ready to be mixed with
     * the beginning of the next segment. */
    memcpy((t -> overlap_buf),(((float *)(fifo_read(&t -> input_fifo,((size_t )0),0))) + ((t -> channels) * ((offset + (t -> segment)) - (t -> overlap)))),(((t -> channels) * (t -> overlap)) * sizeof(( *(t -> overlap_buf)))));
/* Advance through the input stream */
    skip = (((t -> factor) * (++t -> segments_total * ((t -> segment) - (t -> overlap)))) + 0.5);
    t -> skip_total += (skip -= (t -> skip_total));
    fifo_read(&t -> input_fifo,skip,0);
  }
}

static float *tempo_input(tempo_t *t,const float *samples,size_t n)
{
  t -> samples_in += n;
  return (fifo_write(&t -> input_fifo,n,samples));
}

static const float *tempo_output(tempo_t *t,float *samples,size_t *n)
{
  t -> samples_out += ( *n = (( *n <= fifo_occupancy(&t -> output_fifo))? *n : fifo_occupancy(&t -> output_fifo)));
  return (fifo_read(&t -> output_fifo, *n,samples));
}
/* Flush samples remaining in overlap_buf & input_fifo to the output. */

static void tempo_flush(tempo_t *t)
{
  uint64_t samples_out = (((t -> samples_in) / (t -> factor)) + 0.5);
  size_t remaining = (samples_out > (t -> samples_out))?(samples_out - (t -> samples_out)) : 0;
  float *buff = ((((128 * (t -> channels)) * sizeof(( *buff))) != 0ULL)?memset(lsx_realloc(0,((128 * (t -> channels)) * sizeof(( *buff)))),0,((128 * (t -> channels)) * sizeof(( *buff)))) : ((void *)((void *)0)));
  if (remaining > 0) {
    while(fifo_occupancy(&t -> output_fifo) < remaining){
      tempo_input(t,buff,((size_t )128));
      tempo_process(t);
    }
    fifo_trim_to(&t -> output_fifo,remaining);
    t -> samples_in = 0;
  }
  free(buff);
}

static void tempo_setup(tempo_t *t,double sample_rate,sox_bool quick_search,double factor,double segment_ms,double search_ms,double overlap_ms)
{
  size_t max_skip;
  t -> quick_search = quick_search;
  t -> factor = factor;
  t -> segment = (((sample_rate * segment_ms) / 1000) + 0.5);
  t -> search = (((sample_rate * search_ms) / 1000) + 0.5);
  t -> overlap = ((((((sample_rate * overlap_ms) / 1000) + 4.5) >= 16)?(((sample_rate * overlap_ms) / 1000) + 4.5) : 16));
/* Make divisible by 8 for loop optimisation */
  t -> overlap &= (~7);
  if (((t -> overlap) * 2) > (t -> segment)) 
    t -> overlap -= 8;
  t -> overlap_buf = (lsx_realloc(0,(((t -> overlap) * (t -> channels)) * sizeof(( *(t -> overlap_buf))))));
  max_skip = (ceil((factor * ((t -> segment) - (t -> overlap)))));
  t -> process_size = (((((max_skip + (t -> overlap)) >= (t -> segment))?(max_skip + (t -> overlap)) : (t -> segment))) + (t -> search));
  memset(fifo_reserve(&t -> input_fifo,((t -> search) / 2)),0,((((t -> search) / 2) * (t -> channels)) * sizeof(float )));
}

static void tempo_delete(tempo_t *t)
{
  free((t -> overlap_buf));
  fifo_delete(&t -> output_fifo);
  fifo_delete(&t -> input_fifo);
  free(t);
}

static tempo_t *tempo_create(size_t channels)
{
  tempo_t *t = (1?memset(lsx_realloc(0,(1 * sizeof(( *t)))),0,(1 * sizeof(( *t)))) : ((void *)((void *)0)));
  t -> channels = channels;
  fifo_create(&t -> input_fifo,((t -> channels) * sizeof(float )));
  fifo_create(&t -> output_fifo,((t -> channels) * sizeof(float )));
  return t;
}
/*------------------------------- SoX Wrapper --------------------------------*/
typedef struct __unnamed_class___F0_L204_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__tempo_tL127R__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__tempo__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__quick_search__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__factor__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__segment_ms__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__search_ms__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__overlap_ms {
tempo_t *tempo;
sox_bool quick_search;
double factor;
double segment_ms;
double search_ms;
double overlap_ms;}priv_t;

static int getopts(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  enum __unnamed_enum___F0_L213_C3_Default__COMMA__Music__COMMA__Speech__COMMA__Linear {Default,Music,Speech,Linear}profile = Default;
  static const double segments_ms[] = {(82), (82), (35), (20)};
  static const double segments_pow[] = {(0), (1), (.33), (1)};
  static const double overlaps_div[] = {(6.833), (7), (2.5), (2)};
  static const double searches_div[] = {(5.587), (6), (2.14), (2)};
  int c;
  lsx_getopt_t optstate;
  lsx_getopt_init(argc,argv,"+qmls",0,lsx_getopt_flag_none,1,&optstate);
  p -> segment_ms = (p -> search_ms = (p -> overlap_ms = __builtin_huge_val()));
  while((c = lsx_getopt(&optstate)) != -1)
    switch(c){
      case 'q':
{
        p -> quick_search = sox_true;
        break; 
      }
      case 'm':
{
        profile = Music;
        break; 
      }
      case 's':
{
        profile = Speech;
        break; 
      }
      case 'l':
{
        profile = Linear;
        p -> search_ms = 0;
        break; 
      }
      default:
{
        ((( *sox_get_globals()).subsystem = "tempo.c") , lsx_fail_impl("unknown option `-%c\'",optstate.opt));
        return lsx_usage(effp);
      }
    }
  ((argc -= optstate.ind) , (argv += optstate.ind));
{
/* break-able block */
    do {{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 0.1) || (d > 100)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "tempo.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","factor",((double )0.1),((double )100)));
            return lsx_usage(effp);
          }
          p -> factor = d;
          (--argc , ++argv);
        }
      }
{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 10) || (d > 120)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "tempo.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","segment_ms",((double )10),((double )120)));
            return lsx_usage(effp);
          }
          p -> segment_ms = d;
          (--argc , ++argv);
        }
      }
{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 0) || (d > 30)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "tempo.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","search_ms",((double )0),((double )30)));
            return lsx_usage(effp);
          }
          p -> search_ms = d;
          (--argc , ++argv);
        }
      }
{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 0) || (d > 30)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "tempo.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","overlap_ms",((double )0),((double )30)));
            return lsx_usage(effp);
          }
          p -> overlap_ms = d;
          (--argc , ++argv);
        }
      }
    }while (0);
  }
  if ((p -> segment_ms) == __builtin_huge_val()) 
    p -> segment_ms = ((10 >= (segments_ms[profile] / (((pow((p -> factor),segments_pow[profile]) >= 1)?pow((p -> factor),segments_pow[profile]) : 1))))?10 : (segments_ms[profile] / (((pow((p -> factor),segments_pow[profile]) >= 1)?pow((p -> factor),segments_pow[profile]) : 1))));
  if ((p -> overlap_ms) == __builtin_huge_val()) 
    p -> overlap_ms = ((p -> segment_ms) / overlaps_div[profile]);
  if ((p -> search_ms) == __builtin_huge_val()) 
    p -> search_ms = ((p -> segment_ms) / searches_div[profile]);
  p -> overlap_ms = (((p -> overlap_ms) <= ((p -> segment_ms) / 2))?(p -> overlap_ms) : ((p -> segment_ms) / 2));
  ((( *sox_get_globals()).subsystem = "tempo.c") , lsx_report_impl("quick_search=%u factor=%g segment=%g search=%g overlap=%g",(p -> quick_search),(p -> factor),(p -> segment_ms),(p -> search_ms),(p -> overlap_ms)));
  return (argc != 0)?lsx_usage(effp) : SOX_SUCCESS;
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  if ((p -> factor) == 1) 
    return 32;
  p -> tempo = tempo_create(((size_t )effp -> in_signal.channels));
  tempo_setup((p -> tempo),effp -> in_signal.rate,(p -> quick_search),(p -> factor),(p -> segment_ms),(p -> search_ms),(p -> overlap_ms));
/* TODO: calculate actual length */
  effp -> out_signal.length = ((sox_uint64_t )(-1));
  return SOX_SUCCESS;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t i;
  size_t odone = ( *osamp /= effp -> in_signal.channels);
  const float *s = tempo_output((p -> tempo),0,&odone);
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  for (i = 0; i < (odone * effp -> in_signal.channels); ++i) 
     *(obuf++) = ((sox_sample_t )((((sox_macro_temp_sample , (sox_macro_temp_double = (( *(s++)) * (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.))))) , ((sox_macro_temp_double < ((sox_sample_t )(1 << 32 - 1)))?((++effp -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (((sox_macro_temp_double >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)?(((sox_macro_temp_double > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)?((++effp -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : sox_macro_temp_double))))));
  if (( *isamp != 0UL) && (odone <  *osamp)) {
    float *t = tempo_input((p -> tempo),0,( *isamp / effp -> in_signal.channels));
    for (i =  *isamp; i != 0UL; --i) 
       *(t++) = ((((sox_macro_temp_double , (sox_macro_temp_sample =  *(ibuf++)))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - 128)?((++effp -> clips , 1)) : (((sox_macro_temp_sample + 128) & ~255) * (1. / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.))))));
    tempo_process((p -> tempo));
  }
  else 
     *isamp = 0;
   *osamp = (odone * effp -> in_signal.channels);
  return SOX_SUCCESS;
}

static int drain(sox_effect_t *effp,sox_sample_t *obuf,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  static size_t isamp = 0;
  tempo_flush((p -> tempo));
  return flow(effp,0,obuf,&isamp,osamp);
}

static int stop(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  tempo_delete((p -> tempo));
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_tempo_effect_fn()
{
  static sox_effect_handler_t handler = {("tempo"), ("[-q] [-m | -s | -l] factor [segment-ms [search-ms [overlap-ms]]]"), ((16 | 8)), (getopts), (start), (flow), (drain), (stop), ((sox_effect_handler_kill )((void *)0)), ((sizeof(priv_t )))};
  return (&handler);
}
/*---------------------------------- pitch -----------------------------------*/

static int pitch_getopts(sox_effect_t *effp,int argc,char **argv)
{
  double d;
  char dummy;
  char arg[100UL];
  char **argv2 = (lsx_realloc(0,(argc * sizeof(( *argv2)))));
  int result;
  int pos = ((argc > 1) && !(strcmp(argv[1],"-q") != 0))?2 : 1;
  if ((argc <= pos) || (sscanf(argv[pos],"%lf %c",&d,&dummy) != 1)) 
    return lsx_usage(effp);
/* cents --> factor */
  d = pow(2.,(d / 1200));
  sprintf(arg,"%g",(1 / d));
  memcpy(argv2,argv,(argc * sizeof(( *argv2))));
  argv2[pos] = arg;
  result = getopts(effp,argc,argv2);
  free(argv2);
  return result;
}

static int pitch_start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  int result = start(effp);
  effp -> out_signal.rate = (effp -> in_signal.rate / (p -> factor));
  return result;
}

const sox_effect_handler_t *lsx_pitch_effect_fn()
{
  static sox_effect_handler_t handler;
  handler =  *lsx_tempo_effect_fn();
  handler.name = "pitch";
  ((handler.usage = "[-q] shift-in-cents [segment-ms [search-ms [overlap-ms]]]") , (handler.getopts = pitch_getopts));
  handler.start = pitch_start;
  handler.flags &= (~8);
  handler.flags |= 2;
  return (&handler);
}
