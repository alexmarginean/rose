/* multiband compander effect for SoX
 * by Daniel Pouzzner <douzzer@mega.nu> 2002-Oct-8
 *
 * Compander code adapted from the SoX compand effect, by Nick Bailey
 *
 * SoX is Copyright 1999 Chris Bagwell And Nick Bailey This source code is
 * freely redistributable and may be used for any purpose.  This copyright
 * notice must be maintained.  Chris Bagwell And Nick Bailey are not
 * responsible for the consequences of using this software.
 *
 *
 * Usage:
 *   mcompand quoted_compand_args [crossover_frequency
 *      quoted_compand_args [...]]
 *
 *   quoted_compand_args are as for the compand effect:
 *
 *   attack1,decay1[,attack2,decay2...]
 *                  in-dB1,out-dB1[,in-dB2,out-dB2...]
 *                 [ gain [ initial-volume [ delay ] ] ]
 *
 *   Beware a variety of headroom (clipping) bugaboos.
 *
 * Implementation details:
 *   The input is divided into bands using 4th order Linkwitz-Riley IIRs.
 *   This is akin to the crossover of a loudspeaker, and results in flat
 *   frequency response absent compander action.
 *
 *   The outputs of the array of companders is summed, and sample truncation
 *   is done on the final sum.
 *
 *   Modifications to the predictive compression code properly maintain
 *   alignment of the outputs of the array of companders when the companders
 *   have different prediction intervals (volume application delays).  Note
 *   that the predictive mode of the limiter needs some TLC - in fact, a
 *   rewrite - since what's really useful is to assure that a waveform won't
 *   be clipped, by slewing the volume in advance so that the peak is at
 *   limit (or below, if there's a higher subsequent peak visible in the
 *   lookahead window) once it's reached.  */
#ifdef NDEBUG /* Enable assert always. */
#undef NDEBUG /* Must undef above assert.h or other that might include it. */
#endif
#include "sox_i.h"
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include "compandt.h"
#include "mcompand_xover.h"
typedef struct __unnamed_class___F0_L52_C9_unknown_scope_and_name_variable_declaration__variable_type_sox_compandt_tL130R__typedef_declaration_variable_name_unknown_scope_and_name__scope__transfer_fn__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__expectedChannels__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__d__Pe___variable_name_unknown_scope_and_name__scope__attackRate__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__d__Pe___variable_name_unknown_scope_and_name__scope__decayRate__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__d__Pe___variable_name_unknown_scope_and_name__scope__volume__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__delay__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__topfreq__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_crossover_tL131R__typedef_declaration_variable_name_unknown_scope_and_name__scope__filter__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L5R__Pe___variable_name_unknown_scope_and_name__scope__delay_buf__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__delay_size__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_ptrdiff_tl__typedef_declaration_variable_name_unknown_scope_and_name__scope__delay_buf_ptr__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__delay_buf_cnt {
sox_compandt_t transfer_fn;
/* Also flags that channels aren't to be treated
                           individually when = 1 and input not mono */
size_t expectedChannels;
/* An array of attack rates */
double *attackRate;
/*    ... and of decay rates */
double *decayRate;
/* Current "volume" of each channel */
double *volume;
/* Delay to apply before companding */
double delay;
/* upper bound crossover frequency */
double topfreq;
crossover_t filter;
/* Old samples, used for delay processing */
sox_sample_t *delay_buf;
/* lookahead for this band (in samples) - function of delay, above */
size_t delay_size;
/* Index into delay_buf */
ptrdiff_t delay_buf_ptr;
/* No. of active entries in delay_buf */
size_t delay_buf_cnt;}comp_band_t;
typedef struct __unnamed_class___F0_L69_C9_unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__nBands__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L5R__Pe___variable_name_unknown_scope_and_name__scope__band_buf1__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L5R__Pe___variable_name_unknown_scope_and_name__scope__band_buf2__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L5R__Pe___variable_name_unknown_scope_and_name__scope__band_buf3__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__band_buf_len__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__delay_buf_size__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__comp_band_tL132R__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__bands__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__arg {
size_t nBands;
sox_sample_t *band_buf1;
sox_sample_t *band_buf2;
sox_sample_t *band_buf3;
size_t band_buf_len;
/* Size of delay_buf in samples */
size_t delay_buf_size;
comp_band_t *bands;
/* copy of current argument */
char *arg;}priv_t;
/*
 * Process options
 *
 * Don't do initialization now.
 * The 'info' fields are not yet filled in.
 */

static int sox_mcompand_getopts_1(comp_band_t *l,size_t n,char **argv)
{
  char *s;
  size_t rates;
  size_t i;
  size_t commas;
/* Start by checking the attack and decay rates */
  for (((s = argv[0]) , (commas = 0)); ( *s) != 0; ++s) 
    if (( *s) == ',') 
      ++commas;
/* There must be an even number of
                              attack/decay parameters */
  if ((commas % 2) == 0) {
    ((( *sox_get_globals()).subsystem = "mcompand.c") , lsx_fail_impl("compander: Odd number of attack & decay rate parameters"));
    return SOX_EOF;
  }
  rates = (1 + (commas / 2));
  l -> attackRate = (lsx_realloc(0,(sizeof(double ) * rates)));
  l -> decayRate = (lsx_realloc(0,(sizeof(double ) * rates)));
  l -> volume = (lsx_realloc(0,(sizeof(double ) * rates)));
  l -> expectedChannels = rates;
  l -> delay_buf = ((sox_sample_t *)((void *)0));
/* Now tokenise the rates string and set up these arrays.  Keep
         them in seconds at the moment: we don't know the sample rate yet. */
  s = strtok(argv[0],",");
  i = 0;
  do {
    (l -> attackRate)[i] = atof(s);
    s = strtok(0,",");
    (l -> decayRate)[i] = atof(s);
    s = strtok(0,",");
    ++i;
  }while (s != ((char *)((void *)0)));
  if (!(lsx_compandt_parse(&l -> transfer_fn,argv[1],((n > 2)?argv[2] : 0)) != 0U)) 
    return SOX_EOF;
/* Set the initial "volume" to be attibuted to the input channels.
         Unless specified, choose 1.0 (maximum) otherwise clipping will
         result if the user has seleced a long attack time */
  for (i = 0; i < (l -> expectedChannels); ++i) {
    double v = (n >= 4)?pow(10.0,(atof(argv[3]) / 20)) : 1.0;
    (l -> volume)[i] = v;
/* If there is a delay, store it. */
    if (n >= 5) 
      l -> delay = atof(argv[4]);
    else 
      l -> delay = 0.0;
  }
  return SOX_SUCCESS;
}

static int parse_subarg(char *s,char **subargv,size_t *subargc)
{
  char **ap;
  char *s_p;
  s_p = s;
   *subargc = 0;
{
    for (ap = subargv; ( *ap = strtok(s_p," \t")) != ((char *)((void *)0)); ) {
      s_p = ((char *)((void *)0));
      if ( *subargc == 5) {
        ++( *subargc);
        break; 
      }
      if (( *( *ap)) != 0) {
        ++ap;
        ++( *subargc);
      }
    }
  }
  if (( *subargc < 2) || ( *subargc > 5)) {
    ((( *sox_get_globals()).subsystem = "mcompand.c") , lsx_fail_impl("Wrong number of parameters for the compander effect within mcompand; usage:\n\tattack1,decay1{,attack2,decay2} [soft-knee-dB:]in-dB1[,out-dB1]{,in-dB2,out-dB2} [gain [initial-volume-dB [delay]]]\n\twhere {} means optional and repeatable and [] means optional.\n\tdB values are floating point or -inf\'; times are in seconds."));
    return SOX_EOF;
  }
  else 
    return SOX_SUCCESS;
}

static int getopts(sox_effect_t *effp,int argc,char **argv)
{
  char *subargv[6UL];
  char *cp;
  size_t subargc;
  size_t i;
  priv_t *c = (priv_t *)(effp -> priv);
  (--argc , ++argv);
  c -> band_buf1 = (c -> band_buf2 = (c -> band_buf3 = 0));
  c -> band_buf_len = 0;
/* how many bands? */
  if (!((argc & 1) != 0)) {
    ((( *sox_get_globals()).subsystem = "mcompand.c") , lsx_fail_impl("mcompand accepts only an odd number of arguments:\argc  mcompand quoted_compand_args [crossover_freq quoted_compand_args [...]]"));
    return SOX_EOF;
  }
  c -> nBands = ((argc + 1) >> 1);
  c -> bands = (((((c -> nBands) * sizeof(comp_band_t )) != 0ULL)?memset(lsx_realloc(0,((c -> nBands) * sizeof(comp_band_t ))),0,((c -> nBands) * sizeof(comp_band_t ))) : ((void *)((void *)0))));
  for (i = 0; i < (c -> nBands); ++i) {
    c -> arg = ((argv[i << 1] != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(argv[i << 1]) + 1)))),argv[i << 1]) : ((char *)((void *)0)));
    if (parse_subarg((c -> arg),subargv,&subargc) != SOX_SUCCESS) 
      return SOX_EOF;
    if (sox_mcompand_getopts_1(((c -> bands) + i),subargc,(subargv + 0)) != SOX_SUCCESS) 
      return SOX_EOF;
    free((c -> arg));
    c -> arg = ((char *)((void *)0));
    if (i == ((c -> nBands) - 1)) 
      (c -> bands)[i].topfreq = 0;
    else {
      (c -> bands)[i].topfreq = lsx_parse_frequency_k(argv[(i << 1) + 1],&cp,2147483647);
      if (( *cp) != 0) {
        ((( *sox_get_globals()).subsystem = "mcompand.c") , lsx_fail_impl("bad frequency in args to mcompand"));
        return SOX_EOF;
      }
      if ((i > 0) && ((c -> bands)[i].topfreq < (c -> bands)[i - 1].topfreq)) {
        ((( *sox_get_globals()).subsystem = "mcompand.c") , lsx_fail_impl("mcompand crossover frequencies must be in ascending order."));
        return SOX_EOF;
      }
    }
  }
  return SOX_SUCCESS;
}
/*
 * Prepare processing.
 * Do all initializations.
 */

static int start(sox_effect_t *effp)
{
  priv_t *c = (priv_t *)(effp -> priv);
  comp_band_t *l;
  size_t i;
  size_t band;
  for (band = 0; band < (c -> nBands); ++band) {
    l = ((c -> bands) + band);
    l -> delay_size = (((c -> bands)[band].delay * effp -> out_signal.rate) * effp -> out_signal.channels);
    if ((l -> delay_size) > (c -> delay_buf_size)) 
      c -> delay_buf_size = (l -> delay_size);
  }
  for (band = 0; band < (c -> nBands); ++band) {
    l = ((c -> bands) + band);
/* Convert attack and decay rates using number of samples */
    for (i = 0; i < (l -> expectedChannels); ++i) {
      if ((l -> attackRate)[i] > (1.0 / effp -> out_signal.rate)) 
        (l -> attackRate)[i] = (1.0 - exp((-1.0 / (effp -> out_signal.rate * (l -> attackRate)[i]))));
      else 
        (l -> attackRate)[i] = 1.0;
      if ((l -> decayRate)[i] > (1.0 / effp -> out_signal.rate)) 
        (l -> decayRate)[i] = (1.0 - exp((-1.0 / (effp -> out_signal.rate * (l -> decayRate)[i]))));
      else 
        (l -> decayRate)[i] = 1.0;
    }
/* Allocate the delay buffer */
    if ((c -> delay_buf_size) > 0) 
      l -> delay_buf = ((((sizeof(long ) * (c -> delay_buf_size)) != 0ULL)?memset(lsx_realloc(0,(sizeof(long ) * (c -> delay_buf_size))),0,(sizeof(long ) * (c -> delay_buf_size))) : ((void *)((void *)0))));
    l -> delay_buf_ptr = 0;
    l -> delay_buf_cnt = 0;
    if ((l -> topfreq) != 0) 
      crossover_setup(effp,&l -> filter,(l -> topfreq));
  }
  return SOX_SUCCESS;
}
/*
 * Update a volume value using the given sample
 * value, the attack rate and decay rate
 */

static void doVolume(double *v,double samp,comp_band_t *l,size_t chan)
{
  double s = (samp / (~(((sox_sample_t )1) << 31)));
  double delta = (s -  *v);
/* increase volume according to attack rate */
  if (delta > 0.0) 
     *v += (delta * (l -> attackRate)[chan]);
  else 
/* reduce volume according to decay rate */
     *v += (delta * (l -> decayRate)[chan]);
}

static int sox_mcompand_flow_1(sox_effect_t *effp,priv_t *c,comp_band_t *l,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t len,size_t filechans)
{
  size_t idone;
  size_t odone;
  for (((idone = 0) , (odone = 0)); idone < len; ibuf += filechans) {
    size_t chan;
/* Maintain the volume fields by simulating a leaky pump circuit */
    if (((l -> expectedChannels) == 1) && (filechans > 1)) {
/* User is expecting same compander for all channels */
      double maxsamp = 0.0;
      for (chan = 0; chan < filechans; ++chan) {
        double rect = fabs(((double )ibuf[chan]));
        if (rect > maxsamp) 
          maxsamp = rect;
      }
      doVolume(((l -> volume) + 0),maxsamp,l,((size_t )0));
    }
    else {
      for (chan = 0; chan < filechans; ++chan) 
        doVolume(((l -> volume) + chan),fabs(((double )ibuf[chan])),l,chan);
    }
/* Volume memory is updated: perform compand */
    for (chan = 0; chan < filechans; ++chan) {
      int ch = (((l -> expectedChannels) > 1)?chan : 0);
      double level_in_lin = (l -> volume)[ch];
      double level_out_lin = lsx_compandt(&l -> transfer_fn,level_in_lin);
      double checkbuf;
      if ((c -> delay_buf_size) <= 0) {
        checkbuf = (ibuf[chan] * level_out_lin);
        do {
          if (checkbuf > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32))) {
            checkbuf = ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32));
            effp -> clips++;
          }
          else if (checkbuf < ((sox_sample_t )(1 << 32 - 1))) {
            checkbuf = ((sox_sample_t )(1 << 32 - 1));
            effp -> clips++;
          }
        }while (0);
        obuf[odone++] = checkbuf;
        idone++;
      }
      else {
/* FIXME: note that this lookahead algorithm is really lame:
           the response to a peak is released before the peak
           arrives. */
/* because volume application delays differ band to band, but
           total delay doesn't, the volume is applied in an iteration
           preceding that in which the sample goes to obuf, except in
           the band(s) with the longest vol app delay.
           the offset between delay_buf_ptr and the sample to apply
           vol to, is a constant equal to the difference between this
           band's delay and the longest delay of all the bands. */
        if ((l -> delay_buf_cnt) >= (l -> delay_size)) {
          checkbuf = ((l -> delay_buf)[(((l -> delay_buf_ptr) + (c -> delay_buf_size)) - (l -> delay_size)) % (c -> delay_buf_size)] * level_out_lin);
          do {
            if (checkbuf > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32))) {
              checkbuf = ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32));
              effp -> clips++;
            }
            else if (checkbuf < ((sox_sample_t )(1 << 32 - 1))) {
              checkbuf = ((sox_sample_t )(1 << 32 - 1));
              effp -> clips++;
            }
          }while (0);
          (l -> delay_buf)[(((l -> delay_buf_ptr) + (c -> delay_buf_size)) - (l -> delay_size)) % (c -> delay_buf_size)] = checkbuf;
        }
        if ((l -> delay_buf_cnt) >= (c -> delay_buf_size)) {
          obuf[odone] = (l -> delay_buf)[l -> delay_buf_ptr];
          odone++;
          idone++;
        }
        else {
          l -> delay_buf_cnt++;
/* no "odone++" because we did not fill obuf[...] */
          idone++;
        }
        (l -> delay_buf)[l -> delay_buf_ptr++] = ibuf[chan];
        l -> delay_buf_ptr %= (c -> delay_buf_size);
      }
    }
  }
  if ((idone != odone) || (idone != len)) {
/* Emergency brake - will lead to memory corruption otherwise since we
       cannot report back to flow() how many samples were consumed/emitted.
       Additionally, flow() doesn't know how to handle diverging
       sub-compander delays. */
    ((( *sox_get_globals()).subsystem = "mcompand.c") , lsx_fail_impl("Using a compander delay within mcompand is currently not supported"));
    exit(1);
/* FIXME */
  }
  return SOX_SUCCESS;
}
/*
 * Processed signed long samples from ibuf to obuf.
 * Return number of samples processed.
 */

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *c = (priv_t *)(effp -> priv);
  comp_band_t *l;
  size_t len = ( *isamp <=  *osamp)? *isamp :  *osamp;
  size_t band;
  size_t i;
  sox_sample_t *abuf;
  sox_sample_t *bbuf;
  sox_sample_t *cbuf;
  sox_sample_t *oldabuf;
  sox_sample_t *ibuf_copy;
  double out;
  if ((c -> band_buf_len) < len) {
    c -> band_buf1 = (lsx_realloc((c -> band_buf1),(len * sizeof(sox_sample_t ))));
    c -> band_buf2 = (lsx_realloc((c -> band_buf2),(len * sizeof(sox_sample_t ))));
    c -> band_buf3 = (lsx_realloc((c -> band_buf3),(len * sizeof(sox_sample_t ))));
    c -> band_buf_len = len;
  }
  len -= (len % effp -> out_signal.channels);
  ibuf_copy = (lsx_realloc(0,(( *isamp) * sizeof(sox_sample_t ))));
  memcpy(ibuf_copy,ibuf,(( *isamp) * sizeof(sox_sample_t )));
/* split ibuf into bands using filters, pipe each band through sox_mcompand_flow_1, then add back together and write to obuf */
  memset(obuf,0,(len * sizeof(( *obuf))));
  for (((((((band = 0) , (abuf = ibuf_copy))) , (bbuf = (c -> band_buf2)))) , (cbuf = (c -> band_buf1))); band < (c -> nBands); ++band) {
    l = ((c -> bands) + band);
    if ((l -> topfreq) != 0.0) 
      crossover_flow(effp,&l -> filter,abuf,bbuf,cbuf,len);
    else {
      bbuf = abuf;
      abuf = cbuf;
    }
    if (abuf == ibuf_copy) 
      abuf = (c -> band_buf3);
    sox_mcompand_flow_1(effp,c,l,bbuf,abuf,len,((size_t )effp -> out_signal.channels));
    for (i = 0; i < len; ++i) {
      out = (obuf[i] + abuf[i]);
      do {
        if (out > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32))) {
          out = ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32));
          effp -> clips++;
        }
        else if (out < ((sox_sample_t )(1 << 32 - 1))) {
          out = ((sox_sample_t )(1 << 32 - 1));
          effp -> clips++;
        }
      }while (0);
      obuf[i] = out;
    }
    oldabuf = abuf;
    abuf = cbuf;
    cbuf = oldabuf;
  }
   *isamp = ( *osamp = len);
  free(ibuf_copy);
  return SOX_SUCCESS;
}

static int sox_mcompand_drain_1(sox_effect_t *effp,priv_t *c,comp_band_t *l,sox_sample_t *obuf,size_t maxdrain)
{
  size_t done;
  double out;
/*
   * Drain out delay samples.  Note that this loop does all channels.
   */
  for (done = 0; (done < maxdrain) && ((l -> delay_buf_cnt) > 0); done++) {
    out = (obuf[done] + (l -> delay_buf)[l -> delay_buf_ptr++]);
    do {
      if (out > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32))) {
        out = ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32));
        effp -> clips++;
      }
      else if (out < ((sox_sample_t )(1 << 32 - 1))) {
        out = ((sox_sample_t )(1 << 32 - 1));
        effp -> clips++;
      }
    }while (0);
    obuf[done] = out;
    l -> delay_buf_ptr %= (c -> delay_buf_size);
    l -> delay_buf_cnt--;
  }
/* tell caller number of samples played */
  return done;
}
/*
 * Drain out compander delay lines.
 */

static int drain(sox_effect_t *effp,sox_sample_t *obuf,size_t *osamp)
{
  size_t band;
  size_t drained;
  size_t mostdrained = 0;
  priv_t *c = (priv_t *)(effp -> priv);
  comp_band_t *l;
   *osamp -= ( *osamp % effp -> out_signal.channels);
  memset(obuf,0,(( *osamp) * sizeof(( *obuf))));
  for (band = 0; band < (c -> nBands); ++band) {
    l = ((c -> bands) + band);
    drained = (sox_mcompand_drain_1(effp,c,l,obuf, *osamp));
    if (drained > mostdrained) 
      mostdrained = drained;
  }
   *osamp = mostdrained;
  if (mostdrained != 0UL) 
    return SOX_SUCCESS;
  else 
    return SOX_EOF;
}
/*
 * Clean up compander effect.
 */

static int stop(sox_effect_t *effp)
{
  priv_t *c = (priv_t *)(effp -> priv);
  comp_band_t *l;
  size_t band;
  free((c -> band_buf1));
  c -> band_buf1 = ((sox_sample_t *)((void *)0));
  free((c -> band_buf2));
  c -> band_buf2 = ((sox_sample_t *)((void *)0));
  free((c -> band_buf3));
  c -> band_buf3 = ((sox_sample_t *)((void *)0));
  for (band = 0; band < (c -> nBands); band++) {
    l = ((c -> bands) + band);
    free((l -> delay_buf));
    if ((l -> topfreq) != 0) 
      free(l -> filter.previous);
  }
  return SOX_SUCCESS;
}

static int lsx_kill(sox_effect_t *effp)
{
  priv_t *c = (priv_t *)(effp -> priv);
  comp_band_t *l;
  size_t band;
  for (band = 0; band < (c -> nBands); band++) {
    l = ((c -> bands) + band);
    lsx_compandt_kill(&l -> transfer_fn);
    free((l -> decayRate));
    free((l -> attackRate));
    free((l -> volume));
  }
  free((c -> arg));
  free((c -> bands));
  c -> bands = ((comp_band_t *)((void *)0));
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_mcompand_effect_fn()
{
  static sox_effect_handler_t handler = {("mcompand"), ("quoted_compand_args [crossover_frequency[k] quoted_compand_args [...]]\n\nquoted_compand_args are as for the compand effect:\n\n  attack1,decay1[,attack2,decay2...]\n                 in-dB1,out-dB1[,in-dB2,out-dB2...]\n                [ gain [ initial-volume [ delay ] ] ]"), ((16 | 128)), (getopts), (start), (flow), (drain), (stop), (lsx_kill), ((sizeof(priv_t )))};
  return (&handler);
}
