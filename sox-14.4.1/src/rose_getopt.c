/* lsx_getopt for SoX
 *
 * (c) 2011 Doug Cook and SoX contributors
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>

void lsx_getopt_init(
/* Number of arguments in argv */
int argc,
/* Array of arguments */
char *const *argv,
/* Short option characters */
const char *shortopts,
/* Array of long option descriptors */
const lsx_option_t *longopts,
/* Flags for longonly and opterr */
lsx_getopt_flags_t flags,
/* First argument to check (usually 1) */
int first,
/* State object to initialize */
lsx_getopt_t *state)
{
  (argc >= 0)?((void )0) : __assert_fail("argc >= 0","getopt.c",35,"void lsx_getopt_init(int, char *const *, const char *, const struct lsx_option_t *, enum lsx_getopt_flags_t, int, struct lsx_getopt_t *)");
  (argv != ((char *const *)((void *)0)))?((void )0) : __assert_fail("argv != ((void *)0)","getopt.c",36,"void lsx_getopt_init(int, char *const *, const char *, const struct lsx_option_t *, enum lsx_getopt_flags_t, int, struct lsx_getopt_t *)");
  (shortopts != 0)?((void )0) : __assert_fail("shortopts","getopt.c",37,"void lsx_getopt_init(int, char *const *, const char *, const struct lsx_option_t *, enum lsx_getopt_flags_t, int, struct lsx_getopt_t *)");
  (first >= 0)?((void )0) : __assert_fail("first >= 0","getopt.c",38,"void lsx_getopt_init(int, char *const *, const char *, const struct lsx_option_t *, enum lsx_getopt_flags_t, int, struct lsx_getopt_t *)");
  (first <= argc)?((void )0) : __assert_fail("first <= argc","getopt.c",39,"void lsx_getopt_init(int, char *const *, const char *, const struct lsx_option_t *, enum lsx_getopt_flags_t, int, struct lsx_getopt_t *)");
  (state != 0)?((void )0) : __assert_fail("state","getopt.c",40,"void lsx_getopt_init(int, char *const *, const char *, const struct lsx_option_t *, enum lsx_getopt_flags_t, int, struct lsx_getopt_t *)");
  if (state != 0) {
    if (((((argc < 0) || !(argv != 0)) || !(shortopts != 0)) || (first < 0)) || (first > argc)) {
      memset(state,0,(sizeof(( *state))));
    }
    else {
      state -> argc = argc;
      state -> argv = argv;
      state -> shortopts = (((shortopts[0] == '+') || (shortopts[0] == '-'))?(shortopts + 1) : shortopts);
/* Requesting GNU special behavior? */
/* Ignore request. */
/* No special behavior requested. */
      state -> longopts = longopts;
      state -> flags = flags;
      state -> curpos = ((const char *)((void *)0));
      state -> ind = first;
      state -> opt = '?';
      state -> arg = ((const char *)((void *)0));
      state -> lngind = -1;
    }
  }
}

static void CheckCurPosEnd(lsx_getopt_t *state)
{
  if (!((state -> curpos)[0] != 0)) {
    state -> curpos = ((const char *)((void *)0));
    state -> ind++;
  }
}

int lsx_getopt(lsx_getopt_t *state)
{
  int oerr;
  (state != 0)?((void )0) : __assert_fail("state","getopt.c",85,"int lsx_getopt(struct lsx_getopt_t *)");
  if (!(state != 0)) {
    ((( *sox_get_globals()).subsystem = "getopt.c") , lsx_fail_impl("lsx_getopt called with state=NULL"));
    return -1;
  }
  ((state -> argc) >= 0)?((void )0) : __assert_fail("state->argc >= 0","getopt.c",92,"int lsx_getopt(struct lsx_getopt_t *)");
  ((state -> argv) != ((char *const *)((void *)0)))?((void )0) : __assert_fail("state->argv != ((void *)0)","getopt.c",93,"int lsx_getopt(struct lsx_getopt_t *)");
  ((state -> shortopts) != 0)?((void )0) : __assert_fail("state->shortopts","getopt.c",94,"int lsx_getopt(struct lsx_getopt_t *)");
  ((state -> ind) >= 0)?((void )0) : __assert_fail("state->ind >= 0","getopt.c",95,"int lsx_getopt(struct lsx_getopt_t *)");
  ((state -> ind) <= ((state -> argc) + 1))?((void )0) : __assert_fail("state->ind <= state->argc + 1","getopt.c",96,"int lsx_getopt(struct lsx_getopt_t *)");
  oerr = (0 != ((state -> flags) & lsx_getopt_flag_opterr));
  state -> opt = 0;
  state -> arg = ((const char *)((void *)0));
  state -> lngind = -1;
  if (((((state -> argc) < 0) || !((state -> argv) != 0)) || !((state -> shortopts) != 0)) || ((state -> ind) < 0)) 
/* programmer error */
{
    ((( *sox_get_globals()).subsystem = "getopt.c") , lsx_fail_impl("lsx_getopt called with invalid information"));
    state -> curpos = ((const char *)((void *)0));
    return -1;
  }
  else if (((((state -> argc) <= (state -> ind)) || !((state -> argv)[state -> ind] != 0)) || ((state -> argv)[state -> ind][0] != '-')) || ((state -> argv)[state -> ind][1] == 0)) 
/* return no more options */
{
    state -> curpos = ((const char *)((void *)0));
    return -1;
  }
  else if (((state -> argv)[state -> ind][1] == '-') && ((state -> argv)[state -> ind][2] == 0)) 
/* skip "--", return no more options. */
{
    state -> curpos = ((const char *)((void *)0));
    state -> ind++;
    return -1;
  }
  else 
/* Look for the next option */
{
    const char *current = (state -> argv)[state -> ind];
    const char *param = (current + 1);
    if ((((state -> curpos) == ((const char *)((void *)0))) || ((state -> curpos) <= param)) || ((param + strlen(param)) <= (state -> curpos))) 
/* Start parsing a new parameter - check for a long option */
{
      state -> curpos = ((const char *)((void *)0));
      if (((state -> longopts) != 0) && ((param[0] == '-') || (((state -> flags) & lsx_getopt_flag_longonly) != 0U))) {
        size_t nameLen;
        int doubleDash = (param[0] == '-');
        if (doubleDash != 0) {
          param++;
        }
        for (nameLen = 0; (param[nameLen] != 0) && (param[nameLen] != '='); nameLen++) {
        }
/* For single-dash, you have to specify at least two letters in the name. */
        if ((doubleDash != 0) || (nameLen >= 2)) {
          const lsx_option_t *pCur;
          const lsx_option_t *pMatch = (const lsx_option_t *)((void *)0);
          int matches = 0;
{
            for (pCur = (state -> longopts); (pCur -> name) != 0; pCur++) {
              if (0 == strncmp((pCur -> name),param,nameLen)) 
/* Prefix match. */
{
                matches++;
                pMatch = pCur;
                if (nameLen == strlen((pCur -> name))) 
/* Exact match - no ambiguity, stop search. */
{
                  matches = 1;
                  break; 
                }
              }
            }
          }
          if (matches == 1) 
/* Matched. */
{
            state -> ind++;
            if (param[nameLen] != 0) 
/* --name=value */
{
              if ((pMatch -> has_arg) != 0U) 
/* Required or optional arg - done. */
{
                state -> arg = ((param + nameLen) + 1);
              }
              else 
/* No arg expected. */
{
                if (oerr != 0) {
                  ((( *sox_get_globals()).subsystem = "getopt.c") , lsx_warn_impl("`%s\' did not expect an argument from `%s\'",(pMatch -> name),current));
                }
                return '?';
              }
            }
            else if ((pMatch -> has_arg) == lsx_option_arg_required) 
/* Arg required. */
{
              state -> arg = (state -> argv)[state -> ind];
              state -> ind++;
              if ((state -> ind) > (state -> argc)) {
                if (oerr != 0) {
                  ((( *sox_get_globals()).subsystem = "getopt.c") , lsx_warn_impl("`%s\' requires an argument from `%s\'",(pMatch -> name),current));
                }
/* Missing required value. */
                return ((state -> shortopts)[0] == ':')?':' : '?';
              }
            }
            state -> lngind = (pMatch - (state -> longopts));
            if ((pMatch -> flag) != 0) {
               *(pMatch -> flag) = (pMatch -> val);
              return 0;
            }
            else {
              return pMatch -> val;
            }
          }
          else if ((matches == 0) && (doubleDash != 0)) 
/* No match */
{
            if (oerr != 0) {
              ((( *sox_get_globals()).subsystem = "getopt.c") , lsx_warn_impl("parameter not recognized from `%s\'",current));
            }
            state -> ind++;
            return '?';
          }
          else if (matches > 1) 
/* Ambiguous. */
{
            if (oerr != 0) {
              ((( *sox_get_globals()).subsystem = "getopt.c") , lsx_warn_impl("parameter `%s\' is ambiguous:",current));
              for (pCur = (state -> longopts); (pCur -> name) != 0; pCur++) {
                if (0 == strncmp((pCur -> name),param,nameLen)) {
                  ((( *sox_get_globals()).subsystem = "getopt.c") , lsx_warn_impl("parameter `%s\' could be `--%s\'",current,(pCur -> name)));
                }
              }
            }
            state -> ind++;
            return '?';
          }
        }
      }
      state -> curpos = param;
    }
    state -> opt = (state -> curpos)[0];
    if ((state -> opt) == ':') 
/* ':' is never a valid short option character */
{
      if (oerr != 0) {
        ((( *sox_get_globals()).subsystem = "getopt.c") , lsx_warn_impl("option `%c\' not recognized",(state -> opt)));
      }
      state -> curpos++;
      CheckCurPosEnd(state);
/* unrecognized option */
      return '?';
    }
    else 
/* Short option needs to be matched from option list */
{
      const char *pShortopt = (strchr((state -> shortopts),(state -> opt)));
      state -> curpos++;
      if (!(pShortopt != 0)) 
/* unrecognized option */
{
        if (oerr != 0) {
          ((( *sox_get_globals()).subsystem = "getopt.c") , lsx_warn_impl("option `%c\' not recognized",(state -> opt)));
        }
        CheckCurPosEnd(state);
        return '?';
      }
      else if ((pShortopt[1] == ':') && ((state -> curpos)[0] != 0)) 
/* Return the rest of the parameter as the option's value */
{
        state -> arg = (state -> curpos);
        state -> curpos = ((const char *)((void *)0));
        state -> ind++;
        return state -> opt;
      }
      else if ((pShortopt[1] == ':') && (pShortopt[2] != ':')) 
/* Option requires a value */
{
        state -> curpos = ((const char *)((void *)0));
        state -> ind++;
        state -> arg = (state -> argv)[state -> ind];
        state -> ind++;
        if ((state -> ind) <= (state -> argc)) 
/* A value was present, so we're good. */
{
          return state -> opt;
        }
        else 
/* Missing required value. */
{
          if (oerr != 0) {
            ((( *sox_get_globals()).subsystem = "getopt.c") , lsx_warn_impl("option `%c\' requires an argument",(state -> opt)));
          }
          return ((state -> shortopts)[0] == ':')?':' : '?';
        }
      }
      else 
/* Option without a value. */
{
        CheckCurPosEnd(state);
        return state -> opt;
      }
    }
  }
}
#ifdef TEST_GETOPT
#include <stdio.h>
#endif /* TEST_GETOPT */
