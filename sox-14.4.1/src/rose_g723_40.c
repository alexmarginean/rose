/* This source code is a product of Sun Microsystems, Inc. and is provided
 * for unrestricted use.  Users may copy or modify this source code without
 * charge.
 *
 * SUN SOURCE CODE IS PROVIDED AS IS WITH NO WARRANTIES OF ANY KIND INCLUDING
 * THE WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, OR ARISING FROM A COURSE OF DEALING, USAGE OR TRADE PRACTICE.
 *
 * Sun source code is provided with no support and without any obligation on
 * the part of Sun Microsystems, Inc. to assist in its use, correction,
 * modification or enhancement.
 *
 * SUN MICROSYSTEMS, INC. SHALL HAVE NO LIABILITY WITH RESPECT TO THE
 * INFRINGEMENT OF COPYRIGHTS, TRADE SECRETS OR ANY PATENTS BY THIS SOFTWARE
 * OR ANY PART THEREOF.
 *
 * In no event will Sun Microsystems, Inc. be liable for any lost revenue
 * or profits or other special, indirect and consequential damages, even if
 * Sun has been advised of the possibility of such damages.
 *
 * Sun Microsystems, Inc.
 * 2550 Garcia Avenue
 * Mountain View, California  94043
 */
/*
 * g723_40.c
 *
 * Description:
 *
 * g723_40_encoder(), g723_40_decoder()
 *
 * These routines comprise an implementation of the CCITT G.723 40Kbps
 * ADPCM coding algorithm.  Essentially, this implementation is identical to
 * the bit level description except for a few deviations which
 * take advantage of workstation attributes, such as hardware 2's
 * complement arithmetic.
 *
 * The deviation from the bit level specification (lookup tables),
 * preserves the bit level performance specifications.
 *
 * As outlined in the G.723 Recommendation, the algorithm is broken
 * down into modules.  Each section of code below is preceded by
 * the name of the module which it is implementing.
 *
 */
#include "sox_i.h"
#include "g711.h"
#include "g72x.h"
/*
 * Maps G.723_40 code word to ructeconstructed scale factor normalized log
 * magnitude values.
 */
static const short _dqlntab[32UL] = {((-2048)), ((-66)), (28), (104), (169), (224), (274), (318), (358), (395), (429), (459), (488), (514), (539), (566), (566), (539), (514), (488), (459), (429), (395), (358), (318), (274), (224), (169), (104), (28), ((-66)), ((-2048))};
/* Maps G.723_40 code word to log of scale factor multiplier. */
static const short _witab[32UL] = {(448), (448), (768), (1248), (1280), (1312), (1856), (3200), (4512), (5728), (7008), (8960), (11456), (14080), (16928), (22272), (22272), (16928), (14080), (11456), (8960), (7008), (5728), (4512), (3200), (1856), (1312), (1280), (1248), (768), (448), (448)};
/*
 * Maps G.723_40 code words to a set of values whose long and short
 * term averages are computed and then compared to give an indication
 * how stationary (steady state) the signal is.
 */
static const short _fitab[32UL] = {(0), (0), (0), (0), (0), (0x200), (0x200), (0x200), (0x200), (0x200), (1024), (0x600), (2048), (0xA00), (0xC00), (0xC00), (0xC00), (0xC00), (0xA00), (2048), (0x600), (1024), (0x200), (0x200), (0x200), (0x200), (0x200), (0), (0), (0), (0), (0)};
static const short qtab_723_40[15UL] = {((-122)), ((-16)), (68), (139), (198), (250), (298), (339), (378), (413), (445), (475), (502), (528), (553)};
/*
 * g723_40_encoder()
 *
 * Encodes a 16-bit linear PCM, A-law or u-law input sample and retuens
 * the resulting 5-bit CCITT G.723 40Kbps code.
 * Returns -1 if the input coding value is invalid.
 */

int lsx_g723_40_encoder(int sl,int in_coding,struct g72x_state *state_ptr)
{
/* ACCUM */
  short sei;
  short sezi;
  short se;
  short sez;
/* SUBTA */
  short d;
/* MIX */
  short y;
/* ADDB */
  short sr;
/* ADDC */
  short dqsez;
  short dq;
  short i;
/* linearize input sample to 14-bit PCM */
  switch(in_coding){
    case 2:
{
      sl = (lsx_alaw2linear16[sl] >> 2);
      break; 
    }
    case 1:
{
      sl = (lsx_ulaw2linear16[sl] >> 2);
      break; 
    }
    case 3:
{
/* sl of 14-bit dynamic range */
      sl >>= 2;
      break; 
    }
    default:
{
      return -1;
    }
  }
  sezi = (lsx_g72x_predictor_zero(state_ptr));
  sez = (sezi >> 1);
  sei = (sezi + lsx_g72x_predictor_pole(state_ptr));
/* se = estimated signal */
  se = (sei >> 1);
/* d = estimation difference */
  d = (sl - se);
/* quantize prediction difference */
/* adaptive quantizer step size */
  y = (lsx_g72x_step_size(state_ptr));
/* i = ADPCM code */
  i = (lsx_g72x_quantize(d,y,qtab_723_40,15));
/* quantized diff */
  dq = (lsx_g72x_reconstruct((i & 16),_dqlntab[i],y));
/* reconstructed signal */
  sr = (((dq < 0)?(se - (dq & 0x7FFF)) : (se + dq)));
/* dqsez = pole prediction diff. */
  dqsez = ((sr + sez) - se);
  lsx_g72x_update(5,y,_witab[i],_fitab[i],dq,sr,dqsez,state_ptr);
  return i;
}
/*
 * g723_40_decoder()
 *
 * Decodes a 5-bit CCITT G.723 40Kbps code and returns
 * the resulting 16-bit linear PCM, A-law or u-law sample value.
 * -1 is returned if the output coding is unknown.
 */

int lsx_g723_40_decoder(int i,int out_coding,struct g72x_state *state_ptr)
{
/* ACCUM */
  short sezi;
  short sei;
  short sez;
  short se;
/* MIX */
  short y;
/* ADDB */
  short sr;
  short dq;
  short dqsez;
/* mask to get proper bits */
  i &= 0x1f;
  sezi = (lsx_g72x_predictor_zero(state_ptr));
  sez = (sezi >> 1);
  sei = (sezi + lsx_g72x_predictor_pole(state_ptr));
/* se = estimated signal */
  se = (sei >> 1);
/* adaptive quantizer step size */
  y = (lsx_g72x_step_size(state_ptr));
/* estimation diff. */
  dq = (lsx_g72x_reconstruct((i & 16),_dqlntab[i],y));
/* reconst. signal */
  sr = (((dq < 0)?(se - (dq & 0x7FFF)) : (se + dq)));
/* pole prediction diff. */
  dqsez = ((sr - se) + sez);
  lsx_g72x_update(5,y,_witab[i],_fitab[i],dq,sr,dqsez,state_ptr);
  switch(out_coding){
    case 2:
{
      return lsx_g72x_tandem_adjust_alaw(sr,se,y,i,16,qtab_723_40);
    }
    case 1:
{
      return lsx_g72x_tandem_adjust_ulaw(sr,se,y,i,16,qtab_723_40);
    }
    case 3:
{
/* sr was of 14-bit dynamic range */
      return sr << 2;
    }
    default:
{
      return -1;
    }
  }
}
