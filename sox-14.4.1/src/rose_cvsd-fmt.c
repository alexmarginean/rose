/* libSoX file format: CVSD (see cvsd.c)        (c) 2007-8 SoX contributors
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "cvsd.h"
const sox_format_handler_t *lsx_cvsd_format_fn();

const sox_format_handler_t *lsx_cvsd_format_fn()
{
  static const char *const names[] = {("cvsd"), ("cvs"), ((const char *)((void *)0))};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_CVSD), (1), (0), (0)};
  static const sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("Headerless MIL Std 188 113 Continuously Variable Slope Delta modulation"), (names), (256), (lsx_cvsdstartread), (lsx_cvsdread), (lsx_cvsdstopread), (lsx_cvsdstartwrite), (lsx_cvsdwrite), (lsx_cvsdstopwrite), (lsx_rawseek), (write_encodings), ((const sox_rate_t *)((void *)0)), ((sizeof(cvsd_priv_t )))};
  return &handler;
}
/* libSoX file format: CVU   (c) 2008 robs@users.sourceforge.net
 * Unfiltered, therefore, on decode, use with either filter -4k or rate 8k */
typedef struct __unnamed_class___F0_L37_C9_unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__sample__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__step__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__step_mult__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__step_add__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__last_n_bits__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Uc_variable_name_unknown_scope_and_name__scope__byte__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L65R_variable_name_unknown_scope_and_name__scope__bit_count {
double sample;
double step;
double step_mult;
double step_add;
unsigned int last_n_bits;
unsigned char byte;
off_t bit_count;}priv_t;

static int start(sox_format_t *ft)
{
  priv_t *p = (priv_t *)(ft -> priv);
  ft -> signal.channels = 1;
  lsx_rawstart(ft,sox_true,sox_false,sox_true,SOX_ENCODING_CVSD,1);
/* 101 */
  p -> last_n_bits = 5;
  p -> step_mult = exp(((-1) / .005 / ft -> signal.rate));
  p -> step_add = ((1 - (p -> step_mult)) * (.1 * ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32))));
  ((( *sox_get_globals()).subsystem = "cvsd-fmt.c") , lsx_debug_impl("step_mult=%g step_add=%f",(p -> step_mult),(p -> step_add)));
  return SOX_SUCCESS;
}

static void decode(priv_t *p,int bit)
{
  p -> last_n_bits = ((((p -> last_n_bits) << 1) | bit) & 7);
  p -> step *= (p -> step_mult);
  if (((p -> last_n_bits) == 0) || ((p -> last_n_bits) == 7)) 
    p -> step += (p -> step_add);
  if (((p -> last_n_bits) & 1) != 0U) 
    p -> sample = (((((p -> step_mult) * (p -> sample)) + (p -> step)) <= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))?(((p -> step_mult) * (p -> sample)) + (p -> step)) : ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)));
  else 
    p -> sample = (((((p -> step_mult) * (p -> sample)) - (p -> step)) >= ((sox_sample_t )(1 << 32 - 1)))?(((p -> step_mult) * (p -> sample)) - (p -> step)) : ((sox_sample_t )(1 << 32 - 1)));
}

static size_t cvsdread(sox_format_t *ft,sox_sample_t *buf,size_t len)
{
  priv_t *p = (priv_t *)(ft -> priv);
  size_t i;
{
    for (i = 0; i < len; ++i) {
      if (!(((p -> bit_count) & 7) != 0L)) 
        if (lsx_read_b_buf(ft,&p -> byte,((size_t )1)) != 1) 
          break; 
      ++p -> bit_count;
      decode(p,((p -> byte) & 1));
      p -> byte >>= 1;
       *(buf++) = (floor(((p -> sample) + .5)));
    }
  }
  return i;
}

static size_t cvsdwrite(sox_format_t *ft,const sox_sample_t *buf,size_t len)
{
  priv_t *p = (priv_t *)(ft -> priv);
  size_t i;
{
    for (i = 0; i < len; ++i) {
      decode(p,(( *(buf++)) > (p -> sample)));
      p -> byte >>= 1;
      p -> byte |= ((p -> last_n_bits) << 7);
      if (!((++p -> bit_count & 7) != 0L)) 
        if (lsx_writeb(ft,(p -> byte)) != SOX_SUCCESS) 
          break; 
    }
  }
  return len;
}
const sox_format_handler_t *lsx_cvu_format_fn();

const sox_format_handler_t *lsx_cvu_format_fn()
{
  static const char *const names[] = {("cvu"), ((const char *)((void *)0))};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_CVSD), (1), (0), (0)};
  static const sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("Headerless Continuously Variable Slope Delta modulation (unfiltered)"), (names), (256), (start), (cvsdread), ((sox_format_handler_stopread )((void *)0)), (start), (cvsdwrite), ((sox_format_handler_stopwrite )((void *)0)), (lsx_rawseek), (write_encodings), ((const sox_rate_t *)((void *)0)), ((sizeof(priv_t )))};
  return &handler;
}
