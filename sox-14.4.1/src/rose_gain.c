/* libSoX effect: gain/norm/etc.   (c) 2008-9 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#define LSX_EFF_ALIAS
#include "sox_i.h"
#include <ctype.h>
#include <string.h>
typedef struct __unnamed_class___F0_L23_C9_unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__do_equalise__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__do_balance__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__do_balance_no_clip__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__do_limiter__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__do_restore__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__make_headroom__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__do_normalise__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__do_scan__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__fixed_gain__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__mult__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__reclaim__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__rms__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__limiter__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L65R_variable_name_unknown_scope_and_name__scope__num_samples__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L5R_variable_name_unknown_scope_and_name__scope__min__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L5R_variable_name_unknown_scope_and_name__scope__max__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__FILE_IO_FILE__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__tmp_file {
sox_bool do_equalise;
sox_bool do_balance;
sox_bool do_balance_no_clip;
sox_bool do_limiter;
sox_bool do_restore;
sox_bool make_headroom;
sox_bool do_normalise;
sox_bool do_scan;
/* Valid only in channel 0 */
double fixed_gain;
double mult;
double reclaim;
double rms;
double limiter;
off_t num_samples;
sox_sample_t min;
sox_sample_t max;
FILE *tmp_file;}priv_t;

static int create(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  const char *q;
  for ((--argc , ++argv); ((((argc != 0) && (( *( *argv)) == '-')) && (argv[0][1] != 0)) && !((( *__ctype_b_loc())[(int )((unsigned char )argv[0][1])] & ((unsigned short )_ISdigit)) != 0)) && (argv[0][1] != '.'); (--argc , ++argv)) 
    for (q = (argv[0] + 1); ( *q) != 0; ++q) 
      switch(( *q)){
        case 'n':
{
          p -> do_scan = (p -> do_normalise = sox_true);
          break; 
        }
        case 'e':
{
          p -> do_scan = (p -> do_equalise = sox_true);
          break; 
        }
        case 'B':
{
          p -> do_scan = (p -> do_balance = sox_true);
          break; 
        }
        case 'b':
{
          p -> do_scan = (p -> do_balance_no_clip = sox_true);
          break; 
        }
        case 'r':
{
          p -> do_scan = (p -> do_restore = sox_true);
          break; 
        }
        case 'h':
{
          p -> make_headroom = sox_true;
          break; 
        }
        case 'l':
{
          p -> do_limiter = sox_true;
          break; 
        }
        default:
{
          ((( *sox_get_globals()).subsystem = effp -> handler.name) , lsx_fail_impl("invalid option `-%c\'",( *q)));
          return lsx_usage(effp);
        }
      }
  if ((((((p -> do_equalise) + (p -> do_balance)) + (p -> do_balance_no_clip)) + (p -> do_restore)) / sox_true) > 1) {
    ((( *sox_get_globals()).subsystem = effp -> handler.name) , lsx_fail_impl("only one of -e, -B, -b, -r may be given"));
    return SOX_EOF;
  }
  if (((p -> do_normalise) != 0U) && ((p -> do_restore) != 0U)) {
    ((( *sox_get_globals()).subsystem = effp -> handler.name) , lsx_fail_impl("only one of -n, -r may be given"));
    return SOX_EOF;
  }
  if (((p -> do_limiter) != 0U) && ((p -> make_headroom) != 0U)) {
    ((( *sox_get_globals()).subsystem = effp -> handler.name) , lsx_fail_impl("only one of -l, -h may be given"));
    return SOX_EOF;
  }
{
    do {{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < -__builtin_huge_val()) || (d > __builtin_huge_val())) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = effp -> handler.name) , lsx_fail_impl("parameter `%s\' must be between %g and %g","fixed_gain",-__builtin_huge_val(),__builtin_huge_val()));
            return lsx_usage(effp);
          }
          p -> fixed_gain = d;
          (--argc , ++argv);
        }
      }
    }while (0);
  }
  p -> fixed_gain = exp((((p -> fixed_gain) * 2.30258509299404568402) * 0.05));
  return (argc != 0)?lsx_usage(effp) : SOX_SUCCESS;
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  if ((effp -> flow) == 0) {
    if ((p -> do_restore) != 0U) {
      if (!(effp -> in_signal.mult != 0) || ( *effp -> in_signal.mult >= 1)) {
        ((( *sox_get_globals()).subsystem = effp -> handler.name) , lsx_fail_impl("can\'t reclaim headroom"));
        return SOX_EOF;
      }
      p -> reclaim = (1 /  *effp -> in_signal.mult);
    }
    effp -> out_signal.mult = (((p -> make_headroom) != 0U)?&p -> fixed_gain : ((double *)((void *)0)));
    if ((!((p -> do_equalise) != 0U) && !((p -> do_balance) != 0U)) && !((p -> do_balance_no_clip) != 0U)) 
/* essentially a conditional SOX_EFF_MCHAN */
      effp -> flows = 1;
  }
  p -> mult = (p -> max = (p -> min = 0));
  if ((p -> do_scan) != 0U) {
    p -> tmp_file = lsx_tmpfile();
    if ((p -> tmp_file) == ((FILE *)((void *)0))) {
      ((( *sox_get_globals()).subsystem = effp -> handler.name) , lsx_fail_impl("can\'t create temporary file: %s",strerror( *__errno_location())));
      return SOX_EOF;
    }
  }
  if ((p -> do_limiter) != 0U) 
    p -> limiter = ((1 - (1 / (p -> fixed_gain))) * (1. / ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32))));
  else if (((p -> fixed_gain) == floor((p -> fixed_gain))) && !((p -> do_scan) != 0U)) 
    effp -> out_signal.precision = effp -> in_signal.precision;
  return SOX_SUCCESS;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t len;
  if ((p -> do_scan) != 0U) {
    if (fwrite(ibuf,(sizeof(( *ibuf))), *isamp,(p -> tmp_file)) !=  *isamp) {
      ((( *sox_get_globals()).subsystem = effp -> handler.name) , lsx_fail_impl("error writing temporary file: %s",strerror( *__errno_location())));
      return SOX_EOF;
    }
    if (((p -> do_balance) != 0U) && !((p -> do_normalise) != 0U)) 
      for (len =  *isamp; len != 0UL; (--len , ++ibuf)) {
        double d = (( *ibuf) * (1. / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)));
        p -> rms += (d * d);
        ++p -> num_samples;
      }
    else if (((p -> do_balance) != 0U) || ((p -> do_balance_no_clip) != 0U)) 
      for (len =  *isamp; len != 0UL; (--len , ++ibuf)) {
        double d = (( *ibuf) * (1. / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)));
        p -> rms += (d * d);
        ++p -> num_samples;
        p -> max = (((p -> max) >=  *ibuf)?(p -> max) :  *ibuf);
        p -> min = (((p -> min) <=  *ibuf)?(p -> min) :  *ibuf);
      }
    else 
      for (len =  *isamp; len != 0UL; (--len , ++ibuf)) {
        p -> max = (((p -> max) >=  *ibuf)?(p -> max) :  *ibuf);
        p -> min = (((p -> min) <=  *ibuf)?(p -> min) :  *ibuf);
      }
/* samples not output until drain */
     *osamp = 0;
  }
  else {
    double mult = ( *((priv_t *)( *(effp - (effp -> flow))).priv)).fixed_gain;
    len = ( *isamp = ( *osamp = (( *isamp <=  *osamp)? *isamp :  *osamp)));
    if (!((p -> do_limiter) != 0U)) 
      for (; len != 0UL; (--len , ++ibuf)) 
         *(obuf++) = ((((( *ibuf) * mult) < 0)?((((( *ibuf) * mult) <= ((sox_sample_t )(1 << 32 - 1)) - 0.5)?((++effp -> clips , ((sox_sample_t )(1 << 32 - 1)))) : ((( *ibuf) * mult) - 0.5))) : ((((( *ibuf) * mult) >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 0.5)?((++effp -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : ((( *ibuf) * mult) + 0.5)))));
    else 
      for (; len != 0UL; (--len , ++ibuf)) {
        double d = (( *ibuf) * mult);
         *(obuf++) = (((d < 0)?((1 / ((1 / d) - (p -> limiter))) - 0.5) : (((d > 0)?((1 / ((1 / d) + (p -> limiter))) + 0.5) : 0))));
      }
  }
  return SOX_SUCCESS;
}

static void start_drain(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  double max = ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32));
  double max_peak = 0;
  double max_rms = 0;
  size_t i;
  if (((p -> do_balance) != 0U) || ((p -> do_balance_no_clip) != 0U)) {
    for (i = 0; i < (effp -> flows); ++i) {
      priv_t *q = (priv_t *)( *((effp - (effp -> flow)) + i)).priv;
      max_rms = ((max_rms >= sqrt(((q -> rms) / (q -> num_samples))))?max_rms : sqrt(((q -> rms) / (q -> num_samples))));
      rewind((q -> tmp_file));
    }
    for (i = 0; i < (effp -> flows); ++i) {
      priv_t *q = (priv_t *)( *((effp - (effp -> flow)) + i)).priv;
      double this_rms = sqrt(((q -> rms) / (q -> num_samples)));
      double this_peak = (((q -> max) / max) >= ((q -> min) / ((double )((sox_sample_t )(1 << 32 - 1)))))?((q -> max) / max) : ((q -> min) / ((double )((sox_sample_t )(1 << 32 - 1))));
      q -> mult = ((this_rms != 0)?(max_rms / this_rms) : 1);
      max_peak = ((max_peak >= ((q -> mult) * this_peak))?max_peak : ((q -> mult) * this_peak));
      q -> mult *= (p -> fixed_gain);
    }
    if (((p -> do_normalise) != 0U) || (((p -> do_balance_no_clip) != 0U) && (max_peak > 1))) 
      for (i = 0; i < (effp -> flows); ++i) {
        priv_t *q = (priv_t *)( *((effp - (effp -> flow)) + i)).priv;
        q -> mult /= max_peak;
      }
  }
  else if (((p -> do_equalise) != 0U) && !((p -> do_normalise) != 0U)) {
    for (i = 0; i < (effp -> flows); ++i) {
      priv_t *q = (priv_t *)( *((effp - (effp -> flow)) + i)).priv;
      double this_peak = (((q -> max) / max) >= ((q -> min) / ((double )((sox_sample_t )(1 << 32 - 1)))))?((q -> max) / max) : ((q -> min) / ((double )((sox_sample_t )(1 << 32 - 1))));
      max_peak = ((max_peak >= this_peak)?max_peak : this_peak);
      q -> mult = ((p -> fixed_gain) / this_peak);
      rewind((q -> tmp_file));
    }
    for (i = 0; i < (effp -> flows); ++i) {
      priv_t *q = (priv_t *)( *((effp - (effp -> flow)) + i)).priv;
      q -> mult *= max_peak;
    }
  }
  else {
    p -> mult = (((max / (p -> max)) <= (((double )((sox_sample_t )(1 << 32 - 1))) / (p -> min)))?(max / (p -> max)) : (((double )((sox_sample_t )(1 << 32 - 1))) / (p -> min)));
    if ((p -> do_restore) != 0U) {
      if ((p -> reclaim) > (p -> mult)) 
        ((( *sox_get_globals()).subsystem = effp -> handler.name) , lsx_report_impl("%.3gdB not reclaimed",(log10(((p -> reclaim) / (p -> mult))) * 20)));
      else 
        p -> mult = (p -> reclaim);
    }
    p -> mult *= (p -> fixed_gain);
    rewind((p -> tmp_file));
  }
}

static int drain(sox_effect_t *effp,sox_sample_t *obuf,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t len;
  int result = SOX_SUCCESS;
   *osamp -= ( *osamp % effp -> in_signal.channels);
  if ((p -> do_scan) != 0U) {
    if (!((p -> mult) != 0.00000)) 
      start_drain(effp);
    len = fread(obuf,(sizeof(( *obuf))), *osamp,(p -> tmp_file));
    if ((len !=  *osamp) && !(feof((p -> tmp_file)) != 0)) {
      ((( *sox_get_globals()).subsystem = effp -> handler.name) , lsx_fail_impl("error reading temporary file: %s",strerror( *__errno_location())));
      result = SOX_EOF;
    }
    if (!((p -> do_limiter) != 0U)) 
      for ( *osamp = len; len != 0UL; (--len , ++obuf)) 
         *obuf = ((((( *obuf) * (p -> mult)) < 0)?((((( *obuf) * (p -> mult)) <= ((sox_sample_t )(1 << 32 - 1)) - 0.5)?((++effp -> clips , ((sox_sample_t )(1 << 32 - 1)))) : ((( *obuf) * (p -> mult)) - 0.5))) : ((((( *obuf) * (p -> mult)) >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 0.5)?((++effp -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : ((( *obuf) * (p -> mult)) + 0.5)))));
    else 
      for ( *osamp = len; len != 0UL; --len) {
        double d = (( *obuf) * (p -> mult));
         *(obuf++) = (((d < 0)?((1 / ((1 / d) - (p -> limiter))) - 0.5) : (((d > 0)?((1 / ((1 / d) + (p -> limiter))) + 0.5) : 0))));
      }
  }
  else 
     *osamp = 0;
  return result;
}

static int stop(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  if ((p -> do_scan) != 0U) 
/* auto-deleted by lsx_tmpfile */
    fclose((p -> tmp_file));
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_gain_effect_fn()
{
  static sox_effect_handler_t handler = {("gain"), ((const char *)((void *)0)), (128), (create), (start), (flow), (drain), (stop), ((sox_effect_handler_kill )((void *)0)), ((sizeof(priv_t )))};
  static const char *lines[] = {("[-e|-b|-B|-r] [-n] [-l|-h] [gain-dB]"), ("-e\t Equalise channels: peak to that with max peak;"), ("-B\t Balance channels: rms to that with max rms; no clip protection"), ("-b\t Balance channels: rms to that with max rms; clip protection"), ("\t   Note -Bn = -bn"), ("-r\t Reclaim headroom (as much as possible without clipping); see -h"), ("-n\t Norm file to 0dBfs(output precision); gain-dB, if present, usually <0"), ("-l\t Use simple limiter"), ("-h\t Apply attenuation for headroom for subsequent effects; gain-dB, if"), ("\t   present, is subject to reclaim by a subsequent gain -r"), ("gain-dB\t Apply gain in dB")};
  static char *usage;
  handler.usage = (lsx_usage_lines(&usage,lines,(sizeof(lines) / sizeof(lines[0]))));
  return (&handler);
}
/*------------------ emulation of the old `normalise' effect -----------------*/

static int norm_getopts(sox_effect_t *effp,int argc,char **argv)
{
  char *argv2[3UL];
  int argc2 = 2;
  ((((argv2[0] = argv[0]) , --argc)) , ++argv);
  argv2[1] = "-n";
  if (argc != 0) 
    ((((argv2[argc2++] =  *argv) , --argc)) , ++argv);
  return (argc != 0)?lsx_usage(effp) : ( *( *lsx_gain_effect_fn()).getopts)(effp,argc2,argv2);
}

const sox_effect_handler_t *lsx_norm_effect_fn()
{
  static sox_effect_handler_t handler;
  handler =  *lsx_gain_effect_fn();
  handler.name = "norm";
  handler.usage = "[level]";
  handler.getopts = norm_getopts;
  return (&handler);
}
