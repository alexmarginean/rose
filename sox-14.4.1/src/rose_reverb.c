/* libSoX effect: stereo reverberation
 * Copyright (c) 2007 robs@users.sourceforge.net
 * Filter design based on freeverb by Jezar at Dreampoint.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include "fifo.h"
#define lsx_zalloc(var, n) var = lsx_calloc(n, sizeof(*var))
#define filter_advance(p) if (--(p)->ptr < (p)->buffer) (p)->ptr += (p)->size
#define filter_delete(p) free((p)->buffer)
typedef struct __unnamed_class___F0_L27_C9_unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__size__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__f__Pe___variable_name_unknown_scope_and_name__scope__buffer__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__f__Pe___variable_name_unknown_scope_and_name__scope__ptr__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_f_variable_name_unknown_scope_and_name__scope__store {
size_t size;
float *buffer;
float *ptr;
float store;}filter_t;
/* gcc -O2 will inline this */

static float comb_process(filter_t *p,const float *input,const float *feedback,const float *hf_damping)
{
  float output =  *(p -> ptr);
  p -> store = (output + (((p -> store) - output) *  *hf_damping));
   *(p -> ptr) = ( *input + ((p -> store) *  *feedback));
  if (--p -> ptr < (p -> buffer)) 
    p -> ptr += (p -> size);
  return output;
}
/* gcc -O2 will inline this */

static float allpass_process(filter_t *p,const float *input)
{
  float output =  *(p -> ptr);
   *(p -> ptr) = (( *input) + (output * .5));
  if (--p -> ptr < (p -> buffer)) 
    p -> ptr += (p -> size);
  return output -  *input;
}
/* Filter delay lengths in samples (44100Hz sample-rate) */
static const size_t comb_lengths[] = {(1116), (1188), (1277), (1356), (1422), (1491), (1557), (1617)};
static const size_t allpass_lengths[] = {(225), (341), (441), (556)};
#define stereo_adjust 12
typedef struct __unnamed_class___F0_L57_C9_unknown_scope_and_name_variable_declaration__variable_type__Ab_filter_tL126R__typedef_declaration_index_8_Ae__variable_name_unknown_scope_and_name__scope__comb__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_filter_tL126R__typedef_declaration_index_4_Ae__variable_name_unknown_scope_and_name__scope__allpass {
filter_t comb[sizeof(comb_lengths) / sizeof(comb_lengths[0])];
filter_t allpass[sizeof(allpass_lengths) / sizeof(allpass_lengths[0])];}filter_array_t;

static void filter_array_create(filter_array_t *p,double rate,double scale,double offset)
{
  size_t i;
/* Compensate for actual sample-rate */
  double r = (rate * (1 / 44100.));
  for (i = 0; i < sizeof(comb_lengths) / sizeof(comb_lengths[0]); (++i , (offset = -offset))) {
    filter_t *pcomb = ((p -> comb) + i);
    pcomb -> size = ((size_t )(((scale * r) * (comb_lengths[i] + (12 * offset))) + .5));
    pcomb -> ptr = (pcomb -> buffer = (((((pcomb -> size) * sizeof(( *(pcomb -> buffer)))) != 0ULL)?memset(lsx_realloc(0,((pcomb -> size) * sizeof(( *(pcomb -> buffer))))),0,((pcomb -> size) * sizeof(( *(pcomb -> buffer))))) : ((void *)((void *)0)))));
  }
  for (i = 0; i < sizeof(allpass_lengths) / sizeof(allpass_lengths[0]); (++i , (offset = -offset))) {
    filter_t *pallpass = ((p -> allpass) + i);
    pallpass -> size = ((size_t )((r * (allpass_lengths[i] + (12 * offset))) + .5));
    pallpass -> ptr = (pallpass -> buffer = (((((pallpass -> size) * sizeof(( *(pallpass -> buffer)))) != 0ULL)?memset(lsx_realloc(0,((pallpass -> size) * sizeof(( *(pallpass -> buffer))))),0,((pallpass -> size) * sizeof(( *(pallpass -> buffer))))) : ((void *)((void *)0)))));
  }
}

static void filter_array_process(filter_array_t *p,size_t length,const float *input,float *output,const float *feedback,const float *hf_damping,const float *gain)
{
  while(length-- != 0UL){
    float out = 0;
    float in =  *(input++);
    size_t i = (sizeof(comb_lengths) / sizeof(comb_lengths[0]) - 1);
    do 
      out += comb_process(((p -> comb) + i),(&in),feedback,hf_damping);while (i-- != 0UL);
    i = (sizeof(allpass_lengths) / sizeof(allpass_lengths[0]) - 1);
    do 
      out = allpass_process(((p -> allpass) + i),(&out));while (i-- != 0UL);
     *(output++) = (out *  *gain);
  }
}

static void filter_array_delete(filter_array_t *p)
{
  size_t i;
  for (i = 0; i < sizeof(allpass_lengths) / sizeof(allpass_lengths[0]); ++i) 
    free((p -> allpass)[i].buffer);
  for (i = 0; i < sizeof(comb_lengths) / sizeof(comb_lengths[0]); ++i) 
    free((p -> comb)[i].buffer);
}
typedef struct __unnamed_class___F0_L111_C9_unknown_scope_and_name_variable_declaration__variable_type_f_variable_name_unknown_scope_and_name__scope__feedback__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_f_variable_name_unknown_scope_and_name__scope__hf_damping__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_f_variable_name_unknown_scope_and_name__scope__gain__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_fifo_tL127R__typedef_declaration_variable_name_unknown_scope_and_name__scope__input_fifo__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_filter_array_tL128R__typedef_declaration_index_2_Ae__variable_name_unknown_scope_and_name__scope__chan__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab___Pb__f__Pe___index_2_Ae__variable_name_unknown_scope_and_name__scope__out {
float feedback;
float hf_damping;
float gain;
fifo_t input_fifo;
filter_array_t chan[2UL];
float *out[2UL];}reverb_t;

static void reverb_create(reverb_t *p,double sample_rate_Hz,double wet_gain_dB,
/* % */
double room_scale,
/* % */
double reverberance,
/* % */
double hf_damping,double pre_delay_ms,double stereo_depth,size_t buffer_size,float **out)
{
  size_t i;
  size_t delay = (((pre_delay_ms / 1000) * sample_rate_Hz) + .5);
  double scale = (((room_scale / 100) * .9) + .1);
  double depth = (stereo_depth / 100);
/**/
/**/
/* Set minimum feedback */
  double a = ((-1) / log(1 - .3));
/**/
/**/
/* Set maximum feedback */
  double b = (100 / ((log(1 - .98) * a) + 1));
  memset(p,0,(sizeof(( *p))));
  p -> feedback = (1 - exp(((reverberance - b) / (a * b))));
  p -> hf_damping = (((hf_damping / 100) * .3) + .2);
  p -> gain = (exp(((wet_gain_dB * 2.30258509299404568402) * 0.05)) * .015);
  fifo_create(&p -> input_fifo,(sizeof(float )));
  memset(fifo_write(&p -> input_fifo,delay,0),0,(delay * sizeof(float )));
  for (i = 0; i <= ceil(depth); ++i) {
    filter_array_create(((p -> chan) + i),sample_rate_Hz,scale,(i * depth));
    out[i] = ((p -> out)[i] = ((((buffer_size * sizeof(( *(p -> out)[i]))) != 0ULL)?memset(lsx_realloc(0,(buffer_size * sizeof(( *(p -> out)[i])))),0,(buffer_size * sizeof(( *(p -> out)[i])))) : ((void *)((void *)0)))));
  }
}

static void reverb_process(reverb_t *p,size_t length)
{
  size_t i;
  for (i = 0; (i < 2) && ((p -> out)[i] != 0); ++i) 
    filter_array_process(((p -> chan) + i),length,((float *)(fifo_read(&p -> input_fifo,((size_t )0),0))),(p -> out)[i],(&p -> feedback),(&p -> hf_damping),(&p -> gain));
  fifo_read(&p -> input_fifo,length,0);
}

static void reverb_delete(reverb_t *p)
{
  size_t i;
  for (i = 0; (i < 2) && ((p -> out)[i] != 0); ++i) {
    free((p -> out)[i]);
    filter_array_delete(((p -> chan) + i));
  }
  fifo_delete(&p -> input_fifo);
}
/*------------------------------- SoX Wrapper --------------------------------*/
typedef struct __unnamed_class___F0_L168_C9_unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__reverberance__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__hf_damping__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__pre_delay_ms__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__stereo_depth__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__wet_gain_dB__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__room_scale__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__wet_only__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__ichannels__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__ochannels__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_unnamed_base_type_index_2_Ae__variable_name_unknown_scope_and_name__scope__chan {
double reverberance;
double hf_damping;
double pre_delay_ms;
double stereo_depth;
double wet_gain_dB;
double room_scale;
sox_bool wet_only;
size_t ichannels;
size_t ochannels;
struct __unnamed_class___F0_L174_C3_L129R_variable_declaration__variable_type_reverb_tL130R__typedef_declaration_variable_name_L129R__scope__reverb__DELIMITER__L129R_variable_declaration__variable_type___Pb__f__Pe___variable_name_L129R__scope__dry__DELIMITER__L129R_variable_declaration__variable_type__Ab___Pb__f__Pe___index_2_Ae__variable_name_L129R__scope__wet {
reverb_t reverb;
float *dry;
float *wet[2UL];}chan[2UL];}priv_t;

static int getopts(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
/* Set non-zero defaults */
  p -> reverberance = (p -> hf_damping = 50);
  p -> stereo_depth = (p -> room_scale = 100);
  (--argc , ++argv);
  p -> wet_only = (((argc != 0) && (!(strcmp(( *argv),"-w") != 0) || !(strcmp(( *argv),"--wet-only") != 0))) && (((((--argc , ++argv)) , sox_true)) != 0));
{
/* break-able block */
    do {{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 0) || (d > 100)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "reverb.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","reverberance",((double )0),((double )100)));
            return lsx_usage(effp);
          }
          p -> reverberance = d;
          (--argc , ++argv);
        }
      }
{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 0) || (d > 100)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "reverb.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","hf_damping",((double )0),((double )100)));
            return lsx_usage(effp);
          }
          p -> hf_damping = d;
          (--argc , ++argv);
        }
      }
{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 0) || (d > 100)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "reverb.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","room_scale",((double )0),((double )100)));
            return lsx_usage(effp);
          }
          p -> room_scale = d;
          (--argc , ++argv);
        }
      }
{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 0) || (d > 100)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "reverb.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","stereo_depth",((double )0),((double )100)));
            return lsx_usage(effp);
          }
          p -> stereo_depth = d;
          (--argc , ++argv);
        }
      }
{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 0) || (d > 500)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "reverb.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","pre_delay_ms",((double )0),((double )500)));
            return lsx_usage(effp);
          }
          p -> pre_delay_ms = d;
          (--argc , ++argv);
        }
      }
{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < (-10)) || (d > 10)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "reverb.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","wet_gain_dB",((double )(-10)),((double )10)));
            return lsx_usage(effp);
          }
          p -> wet_gain_dB = d;
          (--argc , ++argv);
        }
      }
    }while (0);
  }
  return (argc != 0)?lsx_usage(effp) : SOX_SUCCESS;
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t i;
  p -> ichannels = (p -> ochannels = 1);
  effp -> out_signal.rate = effp -> in_signal.rate;
  if ((effp -> in_signal.channels > 2) && ((p -> stereo_depth) != 0.00000)) {
    ((( *sox_get_globals()).subsystem = "reverb.c") , lsx_warn_impl("stereo-depth not applicable with >2 channels"));
    p -> stereo_depth = 0;
  }
  if ((effp -> in_signal.channels == 1) && ((p -> stereo_depth) != 0.00000)) 
    effp -> out_signal.channels = (p -> ochannels = 2);
  else 
    effp -> out_signal.channels = effp -> in_signal.channels;
  if ((effp -> in_signal.channels == 2) && ((p -> stereo_depth) != 0.00000)) 
    p -> ichannels = (p -> ochannels = 2);
  else 
    effp -> flows = effp -> in_signal.channels;
  for (i = 0; i < (p -> ichannels); ++i) 
    reverb_create(&(p -> chan)[i].reverb,effp -> in_signal.rate,(p -> wet_gain_dB),(p -> room_scale),(p -> reverberance),(p -> hf_damping),(p -> pre_delay_ms),(p -> stereo_depth),(( *( *(effp -> global_info)).global_info).bufsiz / (p -> ochannels)),(p -> chan)[i].wet);
  if (effp -> in_signal.mult != 0) 
     *effp -> in_signal.mult /= ((!((p -> wet_only) != 0U)) + (2 * exp((((((0 >= (p -> wet_gain_dB))?0 : (p -> wet_gain_dB))) * 2.30258509299404568402) * 0.05))));
  return SOX_SUCCESS;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t c;
  size_t i;
  size_t w;
  size_t len = (( *isamp / (p -> ichannels)) <= ( *osamp / (p -> ochannels)))?( *isamp / (p -> ichannels)) : ( *osamp / (p -> ochannels));
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  (( *isamp = (len * (p -> ichannels))) , ( *osamp = (len * (p -> ochannels))));
  for (c = 0; c < (p -> ichannels); ++c) 
    (p -> chan)[c].dry = (fifo_write(&(p -> chan)[c].reverb.input_fifo,len,0));
  for (i = 0; i < len; ++i) 
    for (c = 0; c < (p -> ichannels); ++c) 
      (p -> chan)[c].dry[i] = ((((sox_macro_temp_double , (sox_macro_temp_sample =  *(ibuf++)))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - 128)?((++effp -> clips , 1)) : (((sox_macro_temp_sample + 128) & ~255) * (1. / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.))))));
  for (c = 0; c < (p -> ichannels); ++c) 
    reverb_process(&(p -> chan)[c].reverb,len);
  if ((p -> ichannels) == 2) 
    for (i = 0; i < len; ++i) 
      for (w = 0; w < 2; ++w) {
        float out = (((1 - (p -> wet_only)) * (p -> chan)[w].dry[i]) + (.5 * ((p -> chan)[0].wet[w][i] + (p -> chan)[1].wet[w][i])));
         *(obuf++) = ((sox_sample_t )((((sox_macro_temp_sample , (sox_macro_temp_double = (out * (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.))))) , ((sox_macro_temp_double < ((sox_sample_t )(1 << 32 - 1)))?((++effp -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (((sox_macro_temp_double >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)?(((sox_macro_temp_double > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)?((++effp -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : sox_macro_temp_double))))));
      }
  else 
    for (i = 0; i < len; ++i) 
      for (w = 0; w < (p -> ochannels); ++w) {
        float out = (((1 - (p -> wet_only)) * (p -> chan)[0].dry[i]) + (p -> chan)[0].wet[w][i]);
         *(obuf++) = ((sox_sample_t )((((sox_macro_temp_sample , (sox_macro_temp_double = (out * (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.))))) , ((sox_macro_temp_double < ((sox_sample_t )(1 << 32 - 1)))?((++effp -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (((sox_macro_temp_double >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)?(((sox_macro_temp_double > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)?((++effp -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : sox_macro_temp_double))))));
      }
  return SOX_SUCCESS;
}

static int stop(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t i;
  for (i = 0; i < (p -> ichannels); ++i) 
    reverb_delete(&(p -> chan)[i].reverb);
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_reverb_effect_fn()
{
  static sox_effect_handler_t handler = {("reverb"), ("[-w|--wet-only] [reverberance (50%) [HF-damping (50%) [room-scale (100%) [stereo-depth (100%) [pre-delay (0ms) [wet-gain (0dB)]]]]]]"), (16), (getopts), (start), (flow), ((sox_effect_handler_drain )((void *)0)), (stop), ((sox_effect_handler_kill )((void *)0)), ((sizeof(priv_t )))};
  return (&handler);
}
