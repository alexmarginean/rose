/* libSoX file format: Grandstream ring tone (c) 2009 robs@users.sourceforge.net
 *
 * See http://www.grandstream.com/ringtone.html
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include <time.h>
#define VERSION_      0x1000000
#define MAX_FILE_SIZE 0x10000
#define HEADER_SIZE   (size_t)512
#define PADDING_SIZE  (size_t)478
static const char id[16UL] = "ring.bin";
typedef struct __unnamed_class___F0_L30_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__string__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__ft_encoding__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__bits_per_sample__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L9R_variable_name_unknown_scope_and_name__scope__sox_encoding {
const char *string;
int ft_encoding;
unsigned int bits_per_sample;
sox_encoding_t sox_encoding;}table_t;
static const table_t table[] = {{((const char *)((void *)0)), (0), (8), (SOX_ENCODING_ULAW)}, {("G726"), (2), (0), (SOX_ENCODING_UNKNOWN)}, {((const char *)((void *)0)), (3), (0), (SOX_ENCODING_GSM)}, {((const char *)((void *)0)), (4), (0), (SOX_ENCODING_G723)}, {((const char *)((void *)0)), (8), (8), (SOX_ENCODING_ALAW)}, {("G722"), (9), (0), (SOX_ENCODING_UNKNOWN)}, {("G728"), (15), (2), (SOX_ENCODING_UNKNOWN)}, {("iLBC"), (98), (0), (SOX_ENCODING_UNKNOWN)}};

static int ft_enc(unsigned int bits_per_sample,sox_encoding_t encoding)
{
  size_t i;
  for (i = 0; i < sizeof(table) / sizeof(table[0]); ++i) {
    const table_t *t = (table + i);
    if (((t -> sox_encoding) == encoding) && ((t -> bits_per_sample) == bits_per_sample)) 
      return t -> ft_encoding;
  }
/* Should never get here. */
  return -1;
}

static sox_encoding_t sox_enc(int ft_encoding,unsigned int *bits_per_sample)
{
  size_t i;
  for (i = 0; i < sizeof(table) / sizeof(table[0]); ++i) {
    const table_t *t = (table + i);
    if ((t -> ft_encoding) == ft_encoding) {
       *bits_per_sample = (t -> bits_per_sample);
      if ((t -> sox_encoding) == SOX_ENCODING_UNKNOWN) 
        ((( *sox_get_globals()).subsystem = "gsrt.c") , lsx_report_impl("unsupported encoding: %s",(t -> string)));
      return t -> sox_encoding;
    }
  }
   *bits_per_sample = 0;
  return SOX_ENCODING_UNKNOWN;
}

static int start_read(sox_format_t *ft)
{
  off_t num_samples;
  char read_id[16UL];
  uint32_t file_size;
  int16_t ft_encoding;
  sox_encoding_t encoding;
  unsigned int bits_per_sample;
  lsx_readdw(ft,&file_size);
  num_samples = (((file_size != 0U)?((file_size * 2) - ((size_t )512)) : 0));
  if ((file_size >= 2) && ((ft -> seekable) != 0U)) {
    int i;
    int checksum = ((file_size >> 16) + file_size);
    for (i = (file_size - 2); i != 0; --i) {
      int16_t int16;
      lsx_readsw(ft,&int16);
      checksum += int16;
    }
    if (lsx_seeki(ft,((off_t )(sizeof(file_size))),0) != 0) 
      return SOX_EOF;
    if ((checksum & 0xffff) != 0) 
      ((( *sox_get_globals()).subsystem = "gsrt.c") , lsx_warn_impl("invalid checksum in input file %s",(ft -> filename)));
  }
/* Checksum, version, time stamp. */
  lsx_skipbytes(ft,((size_t )(2 + 4 + 6)));
  lsx_readchars(ft,read_id,(sizeof(read_id)));
  if (memcmp(read_id,id,strlen(id)) != 0) {
    lsx_fail_errno(ft,SOX_EHDR,"gsrt: invalid file name in header");
    return SOX_EOF;
  }
  lsx_readsw(ft,&ft_encoding);
  encoding = sox_enc(ft_encoding,&bits_per_sample);
  if ((encoding != SOX_ENCODING_ALAW) && (encoding != SOX_ENCODING_ULAW)) 
    ft -> handler.read = ((sox_format_handler_read )((void *)0));
  lsx_skipbytes(ft,((size_t )478));
  return lsx_check_read_params(ft,1,8000.,encoding,bits_per_sample,((uint64_t )num_samples),sox_true);
}

static int start_write(sox_format_t *ft)
{
  int i;
  int encoding = ft_enc(ft -> encoding.bits_per_sample,ft -> encoding.encoding);
  time_t now = (( *sox_get_globals()).repeatable != 0U)?0 : time(0);
  const struct tm *t = ((( *sox_get_globals()).repeatable != 0U)?gmtime((&now)) : localtime((&now)));
  int checksum = (0x1000000 >> 16) + 0x1000000;
  checksum += ((t -> tm_year) + 1900);
  checksum += ((((t -> tm_mon) + 1) << 8) + (t -> tm_mday));
  checksum += (((t -> tm_hour) << 8) + (t -> tm_min));
  for (i = (sizeof(id) - 2); i >= 0; i -= 2) 
    checksum += ((id[i] << 8) + id[i + 1]);
  checksum += encoding;
  return (((((((((((lsx_writedw(ft,0) != 0) || (lsx_writesw(ft,-checksum) != 0)) || (lsx_writedw(ft,0x1000000) != 0)) || (lsx_writesw(ft,((t -> tm_year) + 1900)) != 0)) || (lsx_writesb(ft,((t -> tm_mon) + 1)) != 0)) || (lsx_writesb(ft,(t -> tm_mday)) != 0)) || (lsx_writesb(ft,(t -> tm_hour)) != 0)) || (lsx_writesb(ft,(t -> tm_min)) != 0)) || (((((lsx_writebuf(ft,id,(sizeof(id)))) == sizeof(id))?SOX_SUCCESS : SOX_EOF)) != 0)) || (lsx_writesw(ft,encoding) != 0)) || (lsx_padbytes(ft,((size_t )478)) != 0))?SOX_EOF : SOX_SUCCESS;
}

static size_t write_samples(sox_format_t *ft,const sox_sample_t *buf,size_t nsamp)
{
  size_t n = (nsamp <= (0x10000 - ((size_t )(ft -> tell_off))))?nsamp : (0x10000 - ((size_t )(ft -> tell_off)));
  if (n != nsamp) 
    ((( *sox_get_globals()).subsystem = "gsrt.c") , lsx_warn_impl("audio truncated"));
  return lsx_rawwrite(ft,buf,n);
}

static int stop_write(sox_format_t *ft)
{
  long num_samples = ((ft -> tell_off) - ((size_t )512));
  if ((num_samples & 1) != 0L) 
    lsx_writeb(ft,0);
  if ((ft -> seekable) != 0U) {
    unsigned int i;
    unsigned int file_size = ((ft -> tell_off) >> 1);
    int16_t int16;
    int checksum;
    if (!(lsx_seeki(ft,((off_t )(sizeof(uint32_t ))),0) != 0)) {
      lsx_readsw(ft,&int16);
      checksum = (((file_size >> 16) + file_size) - int16);
      if (!(lsx_seeki(ft,((off_t )((size_t )512)),0) != 0)) {
        for (i = ((num_samples + 1) >> 1); i != 0U; --i) {
          lsx_readsw(ft,&int16);
          checksum += int16;
        }
        if (!(lsx_seeki(ft,((off_t )0),0) != 0)) {
          lsx_writedw(ft,file_size);
          lsx_writesw(ft,-checksum);
          return SOX_SUCCESS;
        }
      }
    }
  }
  ((( *sox_get_globals()).subsystem = "gsrt.c") , lsx_warn_impl("can\'t seek in output file `%s\'; length in file header will be unspecified",(ft -> filename)));
  return SOX_SUCCESS;
}
const sox_format_handler_t *lsx_gsrt_format_fn();

const sox_format_handler_t *lsx_gsrt_format_fn()
{
  static const char *const names[] = {("gsrt"), ((const char *)((void *)0))};
  static const sox_rate_t write_rates[] = {(8000), (0)};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_ALAW), (8), (0), (SOX_ENCODING_ULAW), (8), (0), (0)};
  static const sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("Grandstream ring tone"), (names), ((0x0040 | 0x0080 | 256)), (start_read), (lsx_rawread), ((sox_format_handler_stopread )((void *)0)), (start_write), (write_samples), (stop_write), (lsx_rawseek), (write_encodings), (write_rates), (0)};
  return &handler;
}
