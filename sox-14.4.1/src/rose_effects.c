/* SoX Effects chain     (c) 2007 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#define LSX_EFF_ALIAS
#include "sox_i.h"
#include <assert.h>
#include <string.h>
#ifdef HAVE_STRINGS_H
  #include <strings.h>
#endif
#define DEBUG_EFFECTS_CHAIN 0
/* Default effect handler functions for do-nothing situations: */

static int default_function(sox_effect_t *effp)
{
  return SOX_SUCCESS;
}
/* Pass through samples verbatim */

int lsx_flow_copy(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
   *isamp = ( *osamp = (( *isamp <=  *osamp)? *isamp :  *osamp));
  memcpy(obuf,ibuf,(( *isamp) * sizeof(( *obuf))));
  return SOX_SUCCESS;
}
/* Inform no more samples to drain */

static int default_drain(sox_effect_t *effp,sox_sample_t *obuf,size_t *osamp)
{
   *osamp = 0;
  return SOX_EOF;
}
/* Check that no parameters have been given */

static int default_getopts(sox_effect_t *effp,int argc,char **argv)
{
  return (--argc != 0)?lsx_usage(effp) : SOX_SUCCESS;
}
/* Partially initialise the effect structure; signal info will come later */

sox_effect_t *sox_create_effect(const sox_effect_handler_t *eh)
{
  sox_effect_t *effp = (1?memset(lsx_realloc(0,(1 * sizeof(( *effp)))),0,(1 * sizeof(( *effp)))) : ((void *)((void *)0)));
  effp -> obuf = ((sox_sample_t *)((void *)0));
  effp -> global_info = sox_get_effects_globals();
  effp -> handler =  *eh;
  if (!(effp -> handler.getopts != 0)) 
    effp -> handler.getopts = default_getopts;
  if (!(effp -> handler.start != 0)) 
    effp -> handler.start = default_function;
  if (!(effp -> handler.flow != 0)) 
    effp -> handler.flow = lsx_flow_copy;
  if (!(effp -> handler.drain != 0)) 
    effp -> handler.drain = default_drain;
  if (!(effp -> handler.stop != 0)) 
    effp -> handler.stop = default_function;
  if (!(effp -> handler.kill != 0)) 
    effp -> handler.kill = default_function;
  effp -> priv = (((1 * effp -> handler.priv_size) != 0UL)?memset(lsx_realloc(0,(1 * effp -> handler.priv_size)),0,(1 * effp -> handler.priv_size)) : ((void *)((void *)0)));
  return effp;
/* sox_create_effect */
}

int sox_effect_options(sox_effect_t *effp,int argc,char *const argv[])
{
  int result;
  char **argv2 = (lsx_realloc(0,((argc + 1) * sizeof(( *argv2)))));
  argv2[0] = ((char *)effp -> handler.name);
  memcpy((argv2 + 1),argv,(argc * sizeof(( *argv2))));
  result = ( *effp -> handler.getopts)(effp,(argc + 1),argv2);
  free(argv2);
  return result;
/* sox_effect_options */
}
/* Effects chain: */

sox_effects_chain_t *sox_create_effects_chain(const sox_encodinginfo_t *in_enc,const sox_encodinginfo_t *out_enc)
{
  sox_effects_chain_t *result = (1?memset(lsx_realloc(0,(1 * sizeof(sox_effects_chain_t ))),0,(1 * sizeof(sox_effects_chain_t ))) : ((void *)((void *)0)));
  result -> global_info =  *sox_get_effects_globals();
  result -> in_enc = in_enc;
  result -> out_enc = out_enc;
  return result;
/* sox_create_effects_chain */
}

void sox_delete_effects_chain(sox_effects_chain_t *ecp)
{
  if ((ecp != 0) && ((ecp -> length) != 0U)) 
    sox_delete_effects(ecp);
  free((ecp -> effects));
  free(ecp);
/* sox_delete_effects_chain */
}
/* Effect can call in start() or flow() to set minimum input size to flow() */

int lsx_effect_set_imin(sox_effect_t *effp,size_t imin)
{
  if (imin > (( *sox_get_globals()).bufsiz / (effp -> flows))) {
    ((( *sox_get_globals()).subsystem = effp -> handler.name) , lsx_fail_impl("sox_bufsiz not big enough"));
    return SOX_EOF;
  }
  effp -> imin = imin;
  return SOX_SUCCESS;
}
/* Effects table to be extended in steps of EFF_TABLE_STEP */
#define EFF_TABLE_STEP 8
/* Add an effect to the chain. *in is the input signal for this effect. *out is
 * a suggestion as to what the output signal should be, but depending on its
 * given options and *in, the effect can choose to do differently.  Whatever
 * output rate and channels the effect does produce are written back to *in,
 * ready for the next effect in the chain.
 */

int sox_add_effect(sox_effects_chain_t *chain,sox_effect_t *effp,sox_signalinfo_t *in,const sox_signalinfo_t *out)
{
  int ret;
  int (*start)(sox_effect_t *) = effp -> handler.start;
  unsigned int f;
/* Copy of effect for flow 0 before calling start */
  sox_effect_t eff0;
  effp -> global_info = &chain -> global_info;
  effp -> in_signal =  *in;
  effp -> out_signal =  *out;
  effp -> in_encoding = (chain -> in_enc);
  effp -> out_encoding = (chain -> out_enc);
  if (!((effp -> handler.flags & 1) != 0U)) 
    effp -> out_signal.channels = (in -> channels);
  if (!((effp -> handler.flags & 2) != 0U)) 
    effp -> out_signal.rate = (in -> rate);
  if (!((effp -> handler.flags & 4) != 0U)) 
    effp -> out_signal.precision = (((effp -> handler.flags & 256) != 0U)?(in -> precision) : 32);
  if (!((effp -> handler.flags & 128) != 0U)) 
    effp -> out_signal.mult = (in -> mult);
  effp -> flows = ((((effp -> handler.flags & 16) != 0U)?1 : effp -> in_signal.channels));
  effp -> clips = 0;
  effp -> imin = 0;
  ((eff0 =  *effp) , (eff0.priv = ((eff0.priv != 0)?memcpy(lsx_realloc(0,eff0.handler.priv_size),eff0.priv,eff0.handler.priv_size) : ((void *)((void *)0)))));
/* Only used in channel 0 */
  eff0.in_signal.mult = ((double *)((void *)0));
  ret = ( *start)(effp);
  if (ret == 32) {
    ((( *sox_get_globals()).subsystem = effp -> handler.name) , lsx_report_impl("has no effect in this configuration"));
    free(eff0.priv);
    free((effp -> priv));
    effp -> priv = ((void *)((void *)0));
    return SOX_SUCCESS;
  }
  if (ret != SOX_SUCCESS) {
    free(eff0.priv);
    return SOX_EOF;
  }
  if ((in -> mult) != 0) 
    ((( *sox_get_globals()).subsystem = effp -> handler.name) , lsx_debug_impl("mult=%g", *(in -> mult)));
  if (!((effp -> handler.flags & 8) != 0U)) {
    effp -> out_signal.length = (in -> length);
    if (effp -> out_signal.length != ((sox_uint64_t )(-1))) {
      if ((effp -> handler.flags & 1) != 0U) 
        effp -> out_signal.length = ((effp -> out_signal.length / (in -> channels)) * effp -> out_signal.channels);
      if ((effp -> handler.flags & 2) != 0U) 
        effp -> out_signal.length = (((effp -> out_signal.length / (in -> rate)) * effp -> out_signal.rate) + .5);
    }
  }
   *in = (effp -> out_signal);
  if ((chain -> length) == (chain -> table_size)) {
    chain -> table_size += 8;
    ((( *sox_get_globals()).subsystem = "effects.c") , lsx_debug_more_impl("sox_add_effect: extending effects table, new size = %lu",((unsigned long )(chain -> table_size))));
    chain -> effects = (lsx_realloc((chain -> effects),((chain -> table_size) * sizeof(( *(chain -> effects))))));
  }
  (chain -> effects)[chain -> length] = (((((effp -> flows) * sizeof((chain -> effects)[chain -> length][0])) != 0ULL)?memset(lsx_realloc(0,((effp -> flows) * sizeof((chain -> effects)[chain -> length][0]))),0,((effp -> flows) * sizeof((chain -> effects)[chain -> length][0]))) : ((void *)((void *)0))));
  (chain -> effects)[chain -> length][0] =  *effp;
  for (f = 1; f < (effp -> flows); ++f) {
    (chain -> effects)[chain -> length][f] = eff0;
    (chain -> effects)[chain -> length][f].flow = f;
    (chain -> effects)[chain -> length][f].priv = ((eff0.priv != 0)?memcpy(lsx_realloc(0,eff0.handler.priv_size),eff0.priv,eff0.handler.priv_size) : ((void *)((void *)0)));
    if (( *start)(((chain -> effects)[chain -> length] + f)) != SOX_SUCCESS) {
      free(eff0.priv);
      return SOX_EOF;
    }
  }
  ++chain -> length;
  free(eff0.priv);
  return SOX_SUCCESS;
}

static int flow_effect(sox_effects_chain_t *chain,size_t n)
{
  sox_effect_t *effp1 = ((chain -> effects)[n - 1] + 0);
  sox_effect_t *effp = ((chain -> effects)[n] + 0);
  int effstatus = SOX_SUCCESS;
  int f = 0;
  size_t i;
  const sox_sample_t *ibuf;
  size_t idone = ((effp1 -> oend) - (effp1 -> obeg));
  size_t obeg = (( *sox_get_globals()).bufsiz - (effp -> oend));
#if DEBUG_EFFECTS_CHAIN
#endif
/* Run effect on all channels at once */
  if ((effp -> flows) == 1) {
    idone -= (idone % effp -> in_signal.channels);
    effstatus = ( *effp -> handler.flow)(effp,((effp1 -> obuf) + (effp1 -> obeg)),((effp -> obuf) + (effp -> oend)),&idone,&obeg);
    if ((obeg % effp -> out_signal.channels) != 0) {
      ((( *sox_get_globals()).subsystem = effp -> handler.name) , lsx_fail_impl("multi-channel effect flowed asymmetrically!"));
      effstatus = SOX_EOF;
    }
/* Run effect on each channel individually */
  }
  else {
    sox_sample_t *obuf = ((effp -> obuf) + (effp -> oend));
/* Initialised to prevent warning */
    size_t idone_last = 0;
    size_t odone_last = 0;
    ibuf = ((effp1 -> obuf) + (effp1 -> obeg));
    for (i = 0; i < idone; i += (effp -> flows)) 
      for (f = 0; f < ((int )(effp -> flows)); ++f) 
        (chain -> ibufc)[f][i / (effp -> flows)] =  *(ibuf++);
#ifdef HAVE_OPENMP
    if ((( *sox_get_globals()).use_threads != 0U) && ((effp -> flows) > 1)) {
      
#pragma omp parallel for
      for (f = 0; f < ((int )(effp -> flows)); ++f) {
        size_t idonec = (idone / (effp -> flows));
        size_t odonec = (obeg / (effp -> flows));
        int eff_status_c = ( *effp -> handler.flow)(((chain -> effects)[n] + f),(chain -> ibufc)[f],(chain -> obufc)[f],&idonec,&odonec);
        if (!(f != 0)) {
          idone_last = idonec;
          odone_last = odonec;
        }
        if (eff_status_c != SOX_SUCCESS) 
          effstatus = SOX_EOF;
      }
    }
    else 
/* sox_globals.use_threads */
#endif
{
      for (f = 0; f < ((int )(effp -> flows)); ++f) {
        size_t idonec = (idone / (effp -> flows));
        size_t odonec = (obeg / (effp -> flows));
        int eff_status_c = ( *effp -> handler.flow)(((chain -> effects)[n] + f),(chain -> ibufc)[f],(chain -> obufc)[f],&idonec,&odonec);
        if ((f != 0) && ((idonec != idone_last) || (odonec != odone_last))) {
          ((( *sox_get_globals()).subsystem = effp -> handler.name) , lsx_fail_impl("flowed asymmetrically!"));
          effstatus = SOX_EOF;
        }
        idone_last = idonec;
        odone_last = odonec;
        if (eff_status_c != SOX_SUCCESS) 
          effstatus = SOX_EOF;
      }
    }
    for (i = 0; i < odone_last; ++i) 
      for (f = 0; f < ((int )(effp -> flows)); ++f) 
         *(obuf++) = (chain -> obufc)[f][i];
    idone = ((effp -> flows) * idone_last);
    obeg = ((effp -> flows) * odone_last);
  }
#if DEBUG_EFFECTS_CHAIN
#endif
  effp1 -> obeg += idone;
  if ((effp1 -> obeg) == (effp1 -> oend)) 
    effp1 -> obeg = (effp1 -> oend = 0);
  else 
/* Need to refill? */
if (((effp1 -> oend) - (effp1 -> obeg)) < (effp -> imin)) {
    memmove((effp1 -> obuf),((effp1 -> obuf) + (effp1 -> obeg)),(((effp1 -> oend) - (effp1 -> obeg)) * sizeof(( *(effp1 -> obuf)))));
    effp1 -> oend -= (effp1 -> obeg);
    effp1 -> obeg = 0;
  }
  effp -> oend += obeg;
  return (effstatus == SOX_SUCCESS)?SOX_SUCCESS : SOX_EOF;
}
/* The same as flow_effect but with no input */

static int drain_effect(sox_effects_chain_t *chain,size_t n)
{
  sox_effect_t *effp = ((chain -> effects)[n] + 0);
  int effstatus = SOX_SUCCESS;
  size_t i;
  size_t f;
  size_t obeg = (( *sox_get_globals()).bufsiz - (effp -> oend));
#if DEBUG_EFFECTS_CHAIN
#endif
/* Run effect on all channels at once */
  if ((effp -> flows) == 1) {
    effstatus = ( *effp -> handler.drain)(effp,((effp -> obuf) + (effp -> oend)),&obeg);
    if ((obeg % effp -> out_signal.channels) != 0) {
      ((( *sox_get_globals()).subsystem = effp -> handler.name) , lsx_fail_impl("multi-channel effect drained asymmetrically!"));
      effstatus = SOX_EOF;
    }
/* Run effect on each channel individually */
  }
  else {
    sox_sample_t *obuf = ((effp -> obuf) + (effp -> oend));
/* Initialised to prevent warning */
    size_t odone_last = 0;
    for (f = 0; f < (effp -> flows); ++f) {
      size_t odonec = (obeg / (effp -> flows));
      int eff_status_c = ( *effp -> handler.drain)(((chain -> effects)[n] + f),(chain -> obufc)[f],&odonec);
      if ((f != 0UL) && (odonec != odone_last)) {
        ((( *sox_get_globals()).subsystem = effp -> handler.name) , lsx_fail_impl("drained asymmetrically!"));
        effstatus = SOX_EOF;
      }
      odone_last = odonec;
      if (eff_status_c != SOX_SUCCESS) 
        effstatus = SOX_EOF;
    }
    for (i = 0; i < odone_last; ++i) 
      for (f = 0; f < (effp -> flows); ++f) 
         *(obuf++) = (chain -> obufc)[f][i];
    obeg = (f * odone_last);
  }
#if DEBUG_EFFECTS_CHAIN
#endif
/* This is the only thing that drain has and flow hasn't */
  if (!(obeg != 0UL)) 
    effstatus = SOX_EOF;
  effp -> oend += obeg;
  return (effstatus == SOX_SUCCESS)?SOX_SUCCESS : SOX_EOF;
}
/* Flow data through the effects chain until an effect or callback gives EOF */

int sox_flow_effects(sox_effects_chain_t *chain,int (*callback)(sox_bool , void *),void *client_data)
{
  int flow_status = SOX_SUCCESS;
/* effect indices */
  size_t e;
  size_t source_e = 0;
  size_t f;
  size_t max_flows = 0;
  sox_bool draining = sox_true;
  for (e = 0; e < (chain -> length); ++e) {
    (chain -> effects)[e][0].obuf = (lsx_realloc((chain -> effects)[e][0].obuf,(( *sox_get_globals()).bufsiz * sizeof((chain -> effects)[e][0].obuf[0]))));
/* Possibly there is already a buffer, if this is a used effect;
         it may still contain samples in that case. */
/* Memory will be freed by sox_delete_effect() later. */
    max_flows = ((max_flows >= (chain -> effects)[e][0].flows)?max_flows : (chain -> effects)[e][0].flows);
  }
/* don't need interleave buffers */
  if (max_flows == 1) 
    max_flows = 0;
  chain -> ibufc = ((((max_flows * sizeof(( *(chain -> ibufc)))) != 0ULL)?memset(lsx_realloc(0,(max_flows * sizeof(( *(chain -> ibufc))))),0,(max_flows * sizeof(( *(chain -> ibufc))))) : ((void *)((void *)0))));
  chain -> obufc = ((((max_flows * sizeof(( *(chain -> obufc)))) != 0ULL)?memset(lsx_realloc(0,(max_flows * sizeof(( *(chain -> obufc))))),0,(max_flows * sizeof(( *(chain -> obufc))))) : ((void *)((void *)0))));
  for (f = 0; f < max_flows; ++f) {
    (chain -> ibufc)[f] = (((((( *sox_get_globals()).bufsiz / 2) * sizeof((chain -> ibufc)[f][0])) != 0ULL)?memset(lsx_realloc(0,((( *sox_get_globals()).bufsiz / 2) * sizeof((chain -> ibufc)[f][0]))),0,((( *sox_get_globals()).bufsiz / 2) * sizeof((chain -> ibufc)[f][0]))) : ((void *)((void *)0))));
    (chain -> obufc)[f] = (((((( *sox_get_globals()).bufsiz / 2) * sizeof((chain -> obufc)[f][0])) != 0ULL)?memset(lsx_realloc(0,((( *sox_get_globals()).bufsiz / 2) * sizeof((chain -> obufc)[f][0]))),0,((( *sox_get_globals()).bufsiz / 2) * sizeof((chain -> obufc)[f][0]))) : ((void *)((void *)0))));
  }
  e = ((chain -> length) - 1);
{
    while(source_e < (chain -> length)){
#define have_imin (e > 0 && e < chain->length && chain->effects[e - 1][0].oend - chain->effects[e - 1][0].obeg >= chain->effects[e][0].imin)
      size_t osize = ((chain -> effects)[e][0].oend - (chain -> effects)[e][0].obeg);
      if ((e == source_e) && ((draining != 0U) || !(((e > 0) && (e < (chain -> length))) && (((chain -> effects)[e - 1][0].oend - (chain -> effects)[e - 1][0].obeg) >= (chain -> effects)[e][0].imin)))) {
        if (drain_effect(chain,e) == SOX_EOF) {
          ++source_e;
          draining = sox_false;
        }
      }
      else if ((((e > 0) && (e < (chain -> length))) && (((chain -> effects)[e - 1][0].oend - (chain -> effects)[e - 1][0].obeg) >= (chain -> effects)[e][0].imin)) && (flow_effect(chain,e) == SOX_EOF)) {
        flow_status = SOX_EOF;
        if (e == ((chain -> length) - 1)) 
          break; 
        source_e = e;
        draining = sox_true;
      }
/* False for output */
      if ((e < (chain -> length)) && (((chain -> effects)[e][0].oend - (chain -> effects)[e][0].obeg) > osize)) 
        ++e;
      else if (e == source_e) 
        draining = sox_true;
      else if (((int )(--e)) < ((int )source_e)) 
        e = source_e;
      if ((callback != 0) && (( *callback)((source_e == (chain -> length)),client_data) != SOX_SUCCESS)) {
/* Client has requested to stop the flow. */
        flow_status = SOX_EOF;
        break; 
      }
    }
  }
  for (f = 0; f < max_flows; ++f) {
    free((chain -> ibufc)[f]);
    free((chain -> obufc)[f]);
  }
  free((chain -> obufc));
  free((chain -> ibufc));
  return flow_status;
}

sox_uint64_t sox_effects_clips(sox_effects_chain_t *chain)
{
  unsigned int i;
  unsigned int f;
  uint64_t clips = 0;
  for (i = 1; i < ((chain -> length) - 1); ++i) 
    for (f = 0; f < (chain -> effects)[i][0].flows; ++f) 
      clips += (chain -> effects)[i][f].clips;
  return clips;
}

sox_uint64_t sox_stop_effect(sox_effect_t *effp)
{
  unsigned int f;
  uint64_t clips = 0;
  for (f = 0; f < (effp -> flows); ++f) {
    ( *effp[f].handler.stop)((effp + f));
    clips += effp[f].clips;
  }
  return clips;
}

void sox_push_effect_last(sox_effects_chain_t *chain,sox_effect_t *effp)
{
  if ((chain -> length) == (chain -> table_size)) {
    chain -> table_size += 8;
    ((( *sox_get_globals()).subsystem = "effects.c") , lsx_debug_more_impl("sox_push_effect_last: extending effects table, new size = %lu",((unsigned long )(chain -> table_size))));
    chain -> effects = (lsx_realloc((chain -> effects),((chain -> table_size) * sizeof(( *(chain -> effects))))));
  }
  (chain -> effects)[chain -> length++] = effp;
/* sox_push_effect_last */
}

sox_effect_t *sox_pop_effect_last(sox_effects_chain_t *chain)
{
  if ((chain -> length) > 0) {
    sox_effect_t *effp;
    chain -> length--;
    effp = (chain -> effects)[chain -> length];
    (chain -> effects)[chain -> length] = ((sox_effect_t *)((void *)0));
    return effp;
  }
  else 
    return 0;
/* sox_pop_effect_last */
}
/* Free resources related to effect.
 * Note: This currently closes down the effect which might
 * not be obvious from name.
 */

void sox_delete_effect(sox_effect_t *effp)
{
  uint64_t clips;
  unsigned int f;
  if ((clips = sox_stop_effect(effp)) != 0) 
    ((( *sox_get_globals()).subsystem = effp -> handler.name) , lsx_warn_impl("%s clipped %lu samples; decrease volume\?",effp -> handler.name,clips));
  if ((effp -> obeg) != (effp -> oend)) 
    ((( *sox_get_globals()).subsystem = effp -> handler.name) , lsx_debug_impl("output buffer still held %lu samples; dropped.",(((effp -> oend) - (effp -> obeg)) / effp -> out_signal.channels)));
/* May or may not indicate a problem; it is normal if the user aborted
         processing, or if an effect like "trim" stopped early. */
/* N.B. only one kill; not one per flow */
  ( *effp -> handler.kill)(effp);
  for (f = 0; f < (effp -> flows); ++f) 
    free(effp[f].priv);
  free((effp -> obuf));
  free(effp);
}

void sox_delete_effect_last(sox_effects_chain_t *chain)
{
  if ((chain -> length) > 0) {
    chain -> length--;
    sox_delete_effect((chain -> effects)[chain -> length]);
    (chain -> effects)[chain -> length] = ((sox_effect_t *)((void *)0));
  }
/* sox_delete_effect_last */
}
/* Remove all effects from the chain.
 * Note: This currently closes down the effect which might
 * not be obvious from name.
 */

void sox_delete_effects(sox_effects_chain_t *chain)
{
  size_t e;
  for (e = 0; e < (chain -> length); ++e) {
    sox_delete_effect((chain -> effects)[e]);
    (chain -> effects)[e] = ((sox_effect_t *)((void *)0));
  }
  chain -> length = 0;
}
/*----------------------------- Effects library ------------------------------*/
static sox_effect_fn_t s_sox_effect_fns[] = {((sox_effect_fn_t )((void *)0))
#define EFFECT(f) lsx_##f##_effect_fn,
#include "effects.h"
#undef EFFECT
};

const sox_effect_fn_t *sox_get_effect_fns()
{
  return s_sox_effect_fns;
}
/* Find a named effect in the effects library */

const sox_effect_handler_t *sox_find_effect(const char *name)
{
  int e;
  const sox_effect_fn_t *fns = sox_get_effect_fns();
  for (e = 0; fns[e] != 0; ++e) {
    const sox_effect_handler_t *eh = ( *fns[e])();
    if (((eh != 0) && ((eh -> name) != 0)) && (strcasecmp((eh -> name),name) == 0)) 
/* Found it. */
      return eh;
  }
  return 0;
}
