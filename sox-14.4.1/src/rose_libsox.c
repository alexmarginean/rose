/* Implements the public API for libSoX general functions
 * All public functions & data are prefixed with sox_ .
 *
 * (c) 2006-8 Chris Bagwell and SoX contributors
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include <string.h>

const char *sox_version()
{
  static char versionstr[15 * sizeof(int ) - 4 * sizeof(void *) - sizeof(size_t )];
  sprintf(versionstr,"%d.%d.%d",((14 << 16) + (4 << 8) + 1 & 0xff0000) >> 16,((14 << 16) + (4 << 8) + 1 & 0x00ff00) >> 8,(14 << 16) + (4 << 8) + 1 & 0x0000ff);
  return versionstr;
}

const sox_version_info_t *sox_version_info()
{
#define STRINGIZE1(x) #x
#define STRINGIZE(x) STRINGIZE1(x)
  static char arch[30UL];
  static sox_version_info_t info = {((sizeof(sox_version_info_t ))), ((sox_version_flags_t )(sox_version_have_popen + sox_version_have_threads + sox_version_have_memopen + sox_version_none)), (((14 << 16) + (4 << 8) + 1)), ((const char *)((void *)0)), ((const char *)((void *)0)), ("Oct 28 2014 13:38:42"), ((const char *)((void *)0)), ("gcc 4.4.7"), ((const char *)((void *)0))
/* size */
/* flags */
#if HAVE_POPEN
#endif
#if  HAVE_MAGIC
#endif
#if HAVE_OPENMP
#endif
#ifdef HAVE_FMEMOPEN
#endif
/* version_code */
/* version */
/* sox_version_extra */
#ifdef PACKAGE_EXTRA
#else
#endif
/* sox_time */
/* sox_distro */
#ifdef DISTRO
#else
#endif
/* sox_compiler */
#if defined __GNUC__
#elif defined _MSC_VER
#elif defined __SUNPRO_C
#else
#endif
/* sox_arch */
};
  if (!(info.version != 0)) {
    info.version = sox_version();
  }
  if (!(info.arch != 0)) {
    snprintf(arch,(sizeof(arch)),"%lu%lu%lu%lu %lu%lu %lu%lu %c %s",sizeof(char ),sizeof(short ),sizeof(long ),sizeof(off_t ),sizeof(float ),sizeof(double ),sizeof(int *),sizeof(int (*)()),(0?'B' : 'L'),(((info.flags & sox_version_have_threads) != 0U)?"OMP" : ""));
    arch[sizeof(arch) - 1] = 0;
    info.arch = arch;
  }
  return (&info);
}
/* Default routine to output messages; can be overridden */

static void output_message(unsigned int level,const char *filename,const char *fmt,va_list ap)
{
  if (( *sox_get_globals()).verbosity >= level) {
    char base_name[128UL];
    sox_basename(base_name,(sizeof(base_name)),filename);
    fprintf(stderr,"%s: ",base_name);
    vfprintf(stderr,fmt,ap);
    fprintf(stderr,"\n");
  }
}
static sox_globals_t s_sox_globals = {(2), (output_message), (sox_false), (8192), (0), (0), ((const char *)((void *)0)), ((const char *)((void *)0)), ((const char *)((void *)0)), ((char *)((void *)0)), (sox_false), (sox_false)
/* unsigned     verbosity */
/* sox_output_message_handler */
/* sox_bool     repeatable */
/* size_t       bufsiz */
/* size_t       input_bufsiz */
/* int32_t      ranqd1 */
/* char const * stdin_in_use_by */
/* char const * stdout_in_use_by */
/* char const * subsystem */
/* char       * tmp_path */
/* sox_bool     use_magic */
/* sox_bool     use_threads */
};

sox_globals_t *sox_get_globals()
{
  return &s_sox_globals;
}
/* FIXME: Not thread safe using globals */
static sox_effects_globals_t s_sox_effects_globals = {(sox_plot_off), (&s_sox_globals)};

sox_effects_globals_t *sox_get_effects_globals()
{
  return &s_sox_effects_globals;
}

const char *sox_strerror(int sox_errno)
{
  static const char *const errors[] = {("Invalid Audio Header"), ("Unsupported data format"), ("Can\'t allocate memory"), ("Operation not permitted"), ("Operation not supported"), ("Invalid argument")};
  if (sox_errno < SOX_EHDR) 
    return (strerror(sox_errno));
  sox_errno -= SOX_EHDR;
  if ((sox_errno < 0) || (((size_t )sox_errno) >= sizeof(errors) / sizeof(errors[0]))) 
    return "Unknown error";
  return errors[sox_errno];
}

size_t sox_basename(char *base_buffer,size_t base_buffer_len,const char *filename)
{
  if (!(base_buffer != 0) || !(base_buffer_len != 0UL)) {
    return 0;
  }
  else {
    const char *slash_pos = (strrchr(filename,'/'));
    const char *base_name = (slash_pos != 0)?(slash_pos + 1) : filename;
    const char *dot_pos = (strrchr(base_name,'.'));
    size_t i;
    size_t len;
    dot_pos = ((dot_pos != 0)?dot_pos : (base_name + strlen(base_name)));
    len = (dot_pos - base_name);
    len = ((len <= (base_buffer_len - 1))?len : (base_buffer_len - 1));
    for (i = 0; i < len; i++) {
      base_buffer[i] = base_name[i];
    }
    base_buffer[i] = 0;
    return i;
  }
}
#define SOX_MESSAGE_FUNCTION(name,level) \
void name(char const * fmt, ...) { \
  va_list ap; \
  va_start(ap, fmt); \
  if (sox_globals.output_message_handler) \
    (*sox_globals.output_message_handler)(level,sox_globals.subsystem,fmt,ap); \
  va_end(ap); \
}

void lsx_fail_impl(const char *fmt,... )
{
  va_list ap;
  va_start(ap,fmt);
  if (( *sox_get_globals()).output_message_handler != 0) 
    ( *( *sox_get_globals()).output_message_handler)(1,( *sox_get_globals()).subsystem,fmt,ap);
  va_end(ap);
}

void lsx_warn_impl(const char *fmt,... )
{
  va_list ap;
  va_start(ap,fmt);
  if (( *sox_get_globals()).output_message_handler != 0) 
    ( *( *sox_get_globals()).output_message_handler)(2,( *sox_get_globals()).subsystem,fmt,ap);
  va_end(ap);
}

void lsx_report_impl(const char *fmt,... )
{
  va_list ap;
  va_start(ap,fmt);
  if (( *sox_get_globals()).output_message_handler != 0) 
    ( *( *sox_get_globals()).output_message_handler)(3,( *sox_get_globals()).subsystem,fmt,ap);
  va_end(ap);
}

void lsx_debug_impl(const char *fmt,... )
{
  va_list ap;
  va_start(ap,fmt);
  if (( *sox_get_globals()).output_message_handler != 0) 
    ( *( *sox_get_globals()).output_message_handler)(4,( *sox_get_globals()).subsystem,fmt,ap);
  va_end(ap);
}

void lsx_debug_more_impl(const char *fmt,... )
{
  va_list ap;
  va_start(ap,fmt);
  if (( *sox_get_globals()).output_message_handler != 0) 
    ( *( *sox_get_globals()).output_message_handler)(5,( *sox_get_globals()).subsystem,fmt,ap);
  va_end(ap);
}

void lsx_debug_most_impl(const char *fmt,... )
{
  va_list ap;
  va_start(ap,fmt);
  if (( *sox_get_globals()).output_message_handler != 0) 
    ( *( *sox_get_globals()).output_message_handler)(6,( *sox_get_globals()).subsystem,fmt,ap);
  va_end(ap);
}
#undef SOX_MESSAGE_FUNCTION

int sox_init()
{
  return lsx_effects_init();
}

int sox_quit()
{
  sox_format_quit();
  return lsx_effects_quit();
}
