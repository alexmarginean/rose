/* libSoX effect: Spectrogram       (c) 2008-9 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifdef NDEBUG /* Enable assert always. */
#undef NDEBUG /* Must undef above assert.h or other that might include it. */
#endif
#include "sox_i.h"
#include "fft4g.h"
#include <assert.h>
#include <math.h>
#ifdef HAVE_LIBPNG_PNG_H
#include <libpng/png.h>
#else
#include <png.h>
#endif
#include <zlib.h>
#define MAX_FFT_SIZE 4096
#define is_p2(x) !(x & (x - 1))
#define MAX_X_SIZE 200000
typedef enum __unnamed_enum___F0_L38_C9_Window_Hann__COMMA__Window_Hamming__COMMA__Window_Bartlett__COMMA__Window_Rectangular__COMMA__Window_Kaiser {Window_Hann,Window_Hamming,Window_Bartlett,Window_Rectangular,Window_Kaiser}win_type_t;
static const lsx_enum_item window_options[] = {{("Hann"), (Window_Hann)}, {("Hamming"), (Window_Hamming)}, {("Bartlett"), (Window_Bartlett)}, {("Rectangular"), (Window_Rectangular)}, {("Kaiser"), (Window_Kaiser)}, {(0), (0)}};
typedef struct __unnamed_class___F0_L47_C9_unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__pixels_per_sec__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__duration__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__start_time__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__window_adjust__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__x_size0__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__y_size__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__Y_size__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__dB_range__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__gain__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__spectrum_points__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__perm__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__monochrome__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__light_background__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__high_colour__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__slack_overlap__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__no_axes__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__raw__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__alt_palette__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__truncate__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_win_type_tL215R__typedef_declaration_variable_name_unknown_scope_and_name__scope__win_type__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__out_name__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__title__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__comment__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__d__Pe___variable_name_unknown_scope_and_name__scope__shared__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb____Pb__d__Pe____Pe___variable_name_unknown_scope_and_name__scope__shared_ptr__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__WORK__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__skip__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__dft_size__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__step_size__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__block_steps__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__block_num__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__rows__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__cols__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__read__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__x_size__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__end__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__end_min__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__last_end__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__truncated__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_d_index_4096_Ae__variable_name_unknown_scope_and_name__scope__buf__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_d_index_4096_Ae__variable_name_unknown_scope_and_name__scope__dft_buf__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_d_index_4096_Ae__variable_name_unknown_scope_and_name__scope__window__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__block_norm__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__max__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_d_index_2049_Ae__variable_name_unknown_scope_and_name__scope__magnitudes__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__f__Pe___variable_name_unknown_scope_and_name__scope__dBfs {
/* Parameters */
double pixels_per_sec;
double duration;
double start_time;
double window_adjust;
int x_size0;
int y_size;
int Y_size;
int dB_range;
int gain;
int spectrum_points;
int perm;
sox_bool monochrome;
sox_bool light_background;
sox_bool high_colour;
sox_bool slack_overlap;
sox_bool no_axes;
sox_bool raw;
sox_bool alt_palette;
sox_bool truncate;
win_type_t win_type;
const char *out_name;
const char *title;
const char *comment;
/* Shared work area */
double *shared;
double **shared_ptr;
/* Per-channel work area */
/* Start of work area is marked by this dummy variable. */
int WORK;
uint64_t skip;
int dft_size;
int step_size;
int block_steps;
int block_num;
int rows;
int cols;
int read;
int x_size;
int end;
int end_min;
int last_end;
sox_bool truncated;
double buf[4096UL];
double dft_buf[4096UL];
double window[4096UL];
double block_norm;
double max;
double magnitudes[(4096 >> 1) + 1];
float *dBfs;}priv_t;
#define secs(cols) \
  ((double)(cols) * p->step_size * p->block_steps / effp->in_signal.rate)
static const unsigned char alt_palette[] = "\000\000\000\000\000\003\000\001\005\000\001\b\000\001\n\000\001\v\000\001\016\001\002\020\001\002\022\001\002\025\001\002\026\001\002\030\001\003\033\001\003\035\001\003\037\001\003 \001\003\"\001\003$\001\003%\001\003\'\001\003(\001\003*\001\003,\001\003.\001\003/\001\0030\001\0032\001\0034\002\0036\004\0038\005\0039\a\003;\t\003=\v\003\?\016\003A\017\002B\021\002D\023\002F\025\002H\027\002J\030\002K\032\002M\035\002O \002Q$\002S(\002U+\002W0\002Z3\002\\7\002_;\002a>\002cB\002eE\002hI\002jM\002lQ\002nU\002pZ\002r_\002tc\002uh\002vl\002xp\003zu\003|z\003}~\003~\203\003\200\207\003\202\214\003\204\220\003\205\223\003\203\226\003\200\230\003~\233\003|\236\003z\240\003x\243\003u\246\003s\251\003q\253\003o\256\003m\261\003j\263\003h\266\003f\272\003b\274\003^\300\003Z\303\003V\307\003R\312\003N\315\003J\321\003F\324\003C\327\003>\333\003:\336\0036\342\0032\344\003/\346\a-\350\r,\352\021+\354\027*\355\033)\356 (\360&\'\362*&\364/$\3654#\3669#\370>!\372C \374I \374O\"\374V&\374]*\374d,\374k0\374r3\374z7\375\201;\375\210>\375\217B\375\226E\375\236I\375\245M\375\254P\375\261T\375\267X\375\274\\\375\301a\375\306e\375\313i\375\320m\376\325q\376\332v\376\337z\376\344~\376\351\202\376\356\206\376\363\213\375\365\217\374\366\223\373\367\230\372\367\234\371\370\241\370\371\245\367\371\252\366\372\256\365\372\263\364\373\267\363\374\274\361\375\300\360\375\305\360\376\311\357\376\314\357\376\317\360\376\321\360\376\324\360\376\326\360\376\330\360\376\332\361\377\335\361\377\337\361\377\341\361\377\344\361\377\346\362\377\350\362\377\353";
#define alt_palette_len ((array_length(alt_palette) - 1) / 3)

static int getopts(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  uint64_t duration;
  const char *next;
  int c;
  lsx_getopt_t optstate;
  lsx_getopt_init(argc,argv,"+S:d:x:X:y:Y:z:Z:q:p:W:w:st:c:AarmlhTo:",0,lsx_getopt_flag_none,1,&optstate);
/* Non-0 defaults */
  ((((p -> dB_range = 120) , (p -> spectrum_points = 249))) , (p -> perm = 1));
  ((p -> out_name = "spectrogram.png") , (p -> comment = "Created by SoX"));
  while((c = lsx_getopt(&optstate)) != -1){
    switch(c){
      case 'x':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < 100)) || (d > 200000)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","x_size0",((double )100),((double )200000)));
            return lsx_usage(effp);
          }
          p -> x_size0 = d;
          break; 
        }
      }
      case 'X':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < 1)) || (d > 5000)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","pixels_per_sec",((double )1),((double )5000)));
            return lsx_usage(effp);
          }
          p -> pixels_per_sec = d;
          break; 
        }
      }
      case 'y':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < 64)) || (d > 1200)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","y_size",((double )64),((double )1200)));
            return lsx_usage(effp);
          }
          p -> y_size = d;
          break; 
        }
      }
      case 'Y':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < 130)) || (d > (4096 / 2 + 2))) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","Y_size",((double )130),((double )4096) / 2 + 2));
            return lsx_usage(effp);
          }
          p -> Y_size = d;
          break; 
        }
      }
      case 'z':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < 20)) || (d > 180)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","dB_range",((double )20),((double )180)));
            return lsx_usage(effp);
          }
          p -> dB_range = d;
          break; 
        }
      }
      case 'Z':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < (-100))) || (d > 100)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","gain",((double )(-100)),((double )100)));
            return lsx_usage(effp);
          }
          p -> gain = d;
          break; 
        }
      }
      case 'q':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < 0)) || (d > (p -> spectrum_points))) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","spectrum_points",((double )0),((double )(p -> spectrum_points))));
            return lsx_usage(effp);
          }
          p -> spectrum_points = d;
          break; 
        }
      }
      case 'p':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < 1)) || (d > 6)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","perm",((double )1),((double )6)));
            return lsx_usage(effp);
          }
          p -> perm = d;
          break; 
        }
      }
      case 'W':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < (-10))) || (d > 10)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","window_adjust",((double )(-10)),((double )10)));
            return lsx_usage(effp);
          }
          p -> window_adjust = d;
          break; 
        }
      }
      case 'w':
{
        p -> win_type = (lsx_enum_option(c,optstate.arg,window_options));
        break; 
      }
      case 's':
{
        p -> slack_overlap = sox_true;
        break; 
      }
      case 'A':
{
        p -> alt_palette = sox_true;
        break; 
      }
      case 'a':
{
        p -> no_axes = sox_true;
        break; 
      }
      case 'r':
{
        p -> raw = sox_true;
        break; 
      }
      case 'm':
{
        p -> monochrome = sox_true;
        break; 
      }
      case 'l':
{
        p -> light_background = sox_true;
        break; 
      }
      case 'h':
{
        p -> high_colour = sox_true;
        break; 
      }
      case 'T':
{
        p -> truncate = sox_true;
        break; 
      }
      case 't':
{
        p -> title = optstate.arg;
        break; 
      }
      case 'c':
{
        p -> comment = optstate.arg;
        break; 
      }
      case 'o':
{
        p -> out_name = optstate.arg;
        break; 
      }
      case 'S':
{
        next = lsx_parsesamples(1e5,optstate.arg,&duration,'t');
        if ((next != 0) && !(( *next) != 0)) {
          p -> start_time = (duration * 1e-5);
          break; 
        }
        return lsx_usage(effp);
      }
      case 'd':
{
        next = lsx_parsesamples(1e5,optstate.arg,&duration,'t');
        if ((next != 0) && !(( *next) != 0)) {
          p -> duration = (duration * 1e-5);
          break; 
        }
        return lsx_usage(effp);
      }
      default:
{
        ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_fail_impl("invalid option `-%c\'",optstate.opt));
        return lsx_usage(effp);
      }
    }
  }
  if (((!(!((p -> x_size0) != 0)) + !(!((p -> pixels_per_sec) != 0.00000))) + !(!((p -> duration) != 0.00000))) > 2) {
    ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_fail_impl("only two of -x, -X, -d may be given"));
    return SOX_EOF;
  }
  if (((p -> y_size) != 0) && ((p -> Y_size) != 0)) {
    ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_fail_impl("only one of -y, -Y may be given"));
    return SOX_EOF;
  }
  p -> gain = -(p -> gain);
  --p -> perm;
  p -> spectrum_points += 2;
  if ((p -> alt_palette) != 0U) 
    p -> spectrum_points = (((p -> spectrum_points) <= ((int )((sizeof(alt_palette) / sizeof(alt_palette[0]) - 1) / 3)))?(p -> spectrum_points) : ((int )((sizeof(alt_palette) / sizeof(alt_palette[0]) - 1) / 3)));
  p -> shared_ptr = &p -> shared;
  return ((optstate.ind != argc) || ((p -> win_type) == 2147483647))?lsx_usage(effp) : SOX_SUCCESS;
}

static double make_window(priv_t *p,int end)
{
  double sum = 0;
  double *w = (end < 0)?(p -> window) : ((p -> window) + end);
  int i;
  int n = ((p -> dft_size) - abs(end));
  if (end != 0) 
    memset((p -> window),0,(sizeof(p -> window)));
  for (i = 0; i < n; ++i) 
    w[i] = 1;
  switch(p -> win_type){
    case Window_Hann:
{
      lsx_apply_hann(w,n);
      break; 
    }
    case Window_Hamming:
{
      lsx_apply_hamming(w,n);
      break; 
    }
    case Window_Bartlett:
{
      lsx_apply_bartlett(w,n);
      break; 
    }
    case Window_Rectangular:
{
      break; 
    }
    default:
{
      lsx_apply_kaiser(w,n,lsx_kaiser_beta((((p -> dB_range) + (p -> gain)) * (1.1 + ((p -> window_adjust) / 50)))));
    }
  }
  for (i = 0; i < (p -> dft_size); ++i) 
    sum += (p -> window)[i];
  for (i = 0; i < (p -> dft_size); ++i) 
    (p -> window)[i] *= ((2 / sum) * ((((double )n) / (p -> dft_size)) * (((double )n) / (p -> dft_size))));
/* empirical small window adjustment */
  return sum;
}

static double *rdft_init(int n)
{
  double *q = (lsx_realloc(0,(((2 * ((n / 2) + 1)) * n) * sizeof(( *q)))));
  double *p = q;
  int i;
  int j;
  for (j = 0; j <= (n / 2); ++j) 
    for (i = 0; i < n; ++i) 
      (( *(p++) = cos((((2 * 3.14159265358979323846 * j) * i) / n))) , ( *(p++) = sin((((2 * 3.14159265358979323846 * j) * i) / n))));
  return q;
}
#define _ re += in[i] * *q++, im += in[i++] * *q++,

static void rdft_p(const double *q,const double *in,double *out,int n)
{
  int i;
  int j;
  for (j = 0; j <= (n / 2); ++j) {
    double re = 0;
    double im = 0;
    for (i = 0; i < (n & ~7); ) 
      ((((((((((((((((((((((((((((((((re += (in[i] *  *(q++))) , (im += (in[i++] *  *(q++))))) , (re += (in[i] *  *(q++))))) , (im += (in[i++] *  *(q++))))) , (re += (in[i] *  *(q++))))) , (im += (in[i++] *  *(q++))))) , (re += (in[i] *  *(q++))))) , (im += (in[i++] *  *(q++))))) , (re += (in[i] *  *(q++))))) , (im += (in[i++] *  *(q++))))) , (re += (in[i] *  *(q++))))) , (im += (in[i++] *  *(q++))))) , (re += (in[i] *  *(q++))))) , (im += (in[i++] *  *(q++))))) , (re += (in[i] *  *(q++))))) , (im += (in[i++] *  *(q++))))) , 0);
    while(i < n)
      ((((re += (in[i] *  *(q++))) , (im += (in[i++] *  *(q++))))) , 0);
     *(out++) += ((re * re) + (im * im));
  }
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  double actual;
  double duration = (p -> duration);
  double pixels_per_sec = (p -> pixels_per_sec);
  memset((&p -> WORK),0,(sizeof(( *p)) - ((size_t )(&( *((priv_t *)((priv_t *)0))).WORK))));
  p -> skip = (((p -> start_time) * effp -> in_signal.rate) + .5);
  p -> x_size = (p -> x_size0);
{
    while(1){
      if ((!(pixels_per_sec != 0.00000) && ((p -> x_size) != 0)) && (duration != 0.00000)) 
        pixels_per_sec = ((5000 <= ((p -> x_size) / duration))?5000 : ((p -> x_size) / duration));
      else if ((!((p -> x_size) != 0) && (pixels_per_sec != 0.00000)) && (duration != 0.00000)) 
        p -> x_size = ((200000 <= ((int )((pixels_per_sec * duration) + .5)))?200000 : ((int )((pixels_per_sec * duration) + .5)));
      if (!(duration != 0.00000) && (effp -> in_signal.length != ((sox_uint64_t )(-1)))) {
        duration = (effp -> in_signal.length / (effp -> in_signal.rate * effp -> in_signal.channels));
        duration -= (p -> start_time);
        if (duration <= 0) 
          duration = 1;
        continue; 
      }
      else if (!((p -> x_size) != 0)) {
        p -> x_size = 800;
        continue; 
      }
      else if (!(pixels_per_sec != 0.00000)) {
        pixels_per_sec = 100;
        continue; 
      }
      break; 
    }
  }
  if ((p -> y_size) != 0) {
    p -> dft_size = (2 * ((p -> y_size) - 1));
    if (!(!(((p -> dft_size) & ((p -> dft_size) - 1)) != 0)) && !((effp -> flow) != 0UL)) 
      p -> shared = rdft_init((p -> dft_size));
  }
  else {
    int y = ((32 >= ((((((p -> Y_size) != 0)?(p -> Y_size) : 550)) / effp -> in_signal.channels) - 2))?32 : ((((((p -> Y_size) != 0)?(p -> Y_size) : 550)) / effp -> in_signal.channels) - 2));
    for (p -> dft_size = 128; (p -> dft_size) <= y; p -> dft_size <<= 1) ;
  }
  if (!(((p -> dft_size) & ((p -> dft_size) - 1)) != 0) && !((effp -> flow) != 0UL)) 
    lsx_safe_rdft((p -> dft_size),1,(p -> dft_buf));
  ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_debug_impl("duration=%g x_size=%i pixels_per_sec=%g dft_size=%i",duration,(p -> x_size),pixels_per_sec,(p -> dft_size)));
  p -> end = (p -> dft_size);
  p -> rows = (((p -> dft_size) >> 1) + 1);
  actual = make_window(p,(p -> last_end = 0));
  ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_debug_impl("window_density=%g",(actual / (p -> dft_size))));
  p -> step_size = (((((p -> slack_overlap) != 0U)?sqrt((actual * (p -> dft_size))) : actual)) + .5);
  p -> block_steps = (effp -> in_signal.rate / pixels_per_sec);
  p -> step_size = (((p -> block_steps) / ceil((((double )(p -> block_steps)) / (p -> step_size)))) + .5);
  p -> block_steps = (floor(((((double )(p -> block_steps)) / (p -> step_size)) + .5)));
  p -> block_norm = (1. / (p -> block_steps));
  actual = ((effp -> in_signal.rate / (p -> step_size)) / (p -> block_steps));
  if (!((effp -> flow) != 0UL) && (actual != pixels_per_sec)) 
    ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_report_impl("actual pixels/s = %g",actual));
  ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_debug_impl("step_size=%i block_steps=%i",(p -> step_size),(p -> block_steps)));
  p -> max = (-(p -> dB_range));
  p -> read = (((p -> step_size) - (p -> dft_size)) / 2);
  return SOX_SUCCESS;
}

static int do_column(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  int i;
  if ((p -> cols) == (p -> x_size)) {
    p -> truncated = sox_true;
    if (!((effp -> flow) != 0UL)) 
      ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_report_impl("PNG truncated at %g seconds",(((((double )(p -> cols)) * (p -> step_size)) * (p -> block_steps)) / effp -> in_signal.rate)));
    return ((p -> truncate) != 0U)?SOX_EOF : SOX_SUCCESS;
  }
  ++p -> cols;
  p -> dBfs = (lsx_realloc((p -> dBfs),(((p -> cols) * (p -> rows)) * sizeof(( *(p -> dBfs))))));
  for (i = 0; i < (p -> rows); ++i) {
    double dBfs = (10 * log10(((p -> magnitudes)[i] * (p -> block_norm))));
    (p -> dBfs)[(((p -> cols) - 1) * (p -> rows)) + i] = (dBfs + (p -> gain));
    p -> max = ((dBfs >= (p -> max))?dBfs : (p -> max));
  }
  memset((p -> magnitudes),0,((p -> rows) * sizeof(( *p -> magnitudes))));
  p -> block_num = 0;
  return SOX_SUCCESS;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t len = ( *isamp = ( *osamp = (( *isamp <=  *osamp)? *isamp :  *osamp)));
  int i;
/* Pass on audio unaffected */
  memcpy(obuf,ibuf,(len * sizeof(( *obuf))));
  if ((p -> skip) != 0UL) {
    if ((p -> skip) >= len) {
      p -> skip -= len;
      return SOX_SUCCESS;
    }
    ibuf += (p -> skip);
    len -= (p -> skip);
    p -> skip = 0;
  }
{
    while(!((p -> truncated) != 0U)){
      if ((p -> read) == (p -> step_size)) {
        memmove((p -> buf),((p -> buf) + (p -> step_size)),(((p -> dft_size) - (p -> step_size)) * sizeof(( *p -> buf))));
        p -> read = 0;
      }
      for (; (len != 0UL) && ((p -> read) < (p -> step_size)); (((--len , ++p -> read)) , --p -> end)) 
        (p -> buf)[((p -> dft_size) - (p -> step_size)) + (p -> read)] = (( *(ibuf++)) * (1. / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)));
      if ((p -> read) != (p -> step_size)) 
        break; 
      if ((p -> end = (((p -> end) >= (p -> end_min))?(p -> end) : (p -> end_min))) != (p -> last_end)) 
        make_window(p,(p -> last_end = (p -> end)));
      for (i = 0; i < (p -> dft_size); ++i) 
        (p -> dft_buf)[i] = ((p -> buf)[i] * (p -> window)[i]);
      if (!(((p -> dft_size) & ((p -> dft_size) - 1)) != 0)) {
        lsx_safe_rdft((p -> dft_size),1,(p -> dft_buf));
        (p -> magnitudes)[0] += ((p -> dft_buf)[0] * (p -> dft_buf)[0]);
        for (i = 1; i < ((p -> dft_size) >> 1); ++i) 
          (p -> magnitudes)[i] += (((p -> dft_buf)[2 * i] * (p -> dft_buf)[2 * i]) + ((p -> dft_buf)[(2 * i) + 1] * (p -> dft_buf)[(2 * i) + 1]));
        (p -> magnitudes)[(p -> dft_size) >> 1] += ((p -> dft_buf)[1] * (p -> dft_buf)[1]);
      }
      else 
        rdft_p(( *(p -> shared_ptr)),(p -> dft_buf),(p -> magnitudes),(p -> dft_size));
      if ((++p -> block_num == (p -> block_steps)) && (do_column(effp) == SOX_EOF)) 
        return SOX_EOF;
    }
  }
  return SOX_SUCCESS;
}

static int drain(sox_effect_t *effp,sox_sample_t *obuf_,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  if (!((p -> truncated) != 0U)) {
    sox_sample_t *ibuf = ((((p -> dft_size) * sizeof(( *ibuf))) != 0ULL)?memset(lsx_realloc(0,((p -> dft_size) * sizeof(( *ibuf)))),0,((p -> dft_size) * sizeof(( *ibuf)))) : ((void *)((void *)0)));
    sox_sample_t *obuf = ((((p -> dft_size) * sizeof(( *obuf))) != 0ULL)?memset(lsx_realloc(0,((p -> dft_size) * sizeof(( *obuf)))),0,((p -> dft_size) * sizeof(( *obuf)))) : ((void *)((void *)0)));
    size_t isamp = (((p -> dft_size) - (p -> step_size)) / 2);
    int left_over = ((isamp + (p -> read)) % (p -> step_size));
    if (left_over >= ((p -> step_size) >> 1)) 
      isamp += ((p -> step_size) - left_over);
    ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_debug_impl("cols=%i left=%i end=%i",(p -> cols),(p -> read),(p -> end)));
    ((p -> end = 0) , (p -> end_min = -(p -> dft_size)));
    if ((flow(effp,ibuf,obuf,&isamp,&isamp) == SOX_SUCCESS) && ((p -> block_num) != 0)) {
      p -> block_norm *= (((double )(p -> block_steps)) / (p -> block_num));
      do_column(effp);
    }
    ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_debug_impl("flushed cols=%i left=%i end=%i",(p -> cols),(p -> read),(p -> end)));
    free(obuf);
    free(ibuf);
  }
  (obuf_ , ( *osamp = 0));
  return SOX_SUCCESS;
}
enum __unnamed_enum___F0_L352_C1_Background__COMMA__Text__COMMA__Labels__COMMA__Grid__COMMA__fixed_palette {Background,Text,Labels,Grid,fixed_palette};

static unsigned int colour(const priv_t *p,double x)
{
  unsigned int c = ((x < (-(p -> dB_range)))?0 : (((x >= 0)?((p -> spectrum_points) - 1) : (1 + ((1 + (x / (p -> dB_range))) * ((p -> spectrum_points) - 2))))));
  return fixed_palette + c;
}

static void make_palette(const priv_t *p,png_color *palette)
{
  int i;
  if ((p -> light_background) != 0U) {
    memcpy((palette++),(((p -> monochrome) != 0U)?"\337\337\337" : "\335\330\320"),((size_t )3));
    memcpy((palette++),"\000\000\000",((size_t )3));
    memcpy((palette++),"\?\?\?",((size_t )3));
    memcpy((palette++),"\?\?\?",((size_t )3));
  }
  else {
    memcpy((palette++),"\000\000\000",((size_t )3));
    memcpy((palette++),"\377\377\377",((size_t )3));
    memcpy((palette++),"\277\277\277",((size_t )3));
    memcpy((palette++),"\177\177\177",((size_t )3));
  }
  for (i = 0; i < (p -> spectrum_points); ++i) {{
      double c[3UL];
      double x = (((double )i) / ((p -> spectrum_points) - 1));
      int at = ((p -> light_background) != 0U)?(((p -> spectrum_points) - 1) - i) : i;
      if ((p -> monochrome) != 0U) {
        c[2] = (c[1] = (c[0] = x));
        if ((p -> high_colour) != 0U) {
          c[(1 + (p -> perm)) % 3] = ((x < .4)?0 : (5 / 3. * (x - .4)));
          if ((p -> perm) < 3) 
            c[(2 + (p -> perm)) % 3] = ((x < .4)?0 : (5 / 3. * (x - .4)));
        }
        palette[at].red = (.5 + (255 * c[0]));
        palette[at].green = (.5 + (255 * c[1]));
        palette[at].blue = (.5 + (255 * c[2]));
        continue; 
      }
      if ((p -> high_colour) != 0U) {
        static const int states[3UL][7UL] = {{(4), (5), (0), (0), (2), (1), (1)}, {(0), (0), (2), (1), (1), (3), (2)}, {(4), (1), (1), (3), (0), (0), (2)}};
        int j;
        int phase_num = (((7 * x) <= 6)?(7 * x) : 6);
        for (j = 0; j < 3; ++j) 
          switch(states[j][phase_num]){
            case 0:
{
              c[j] = 0;
              break; 
            }
            case 1:
{
              c[j] = 1;
              break; 
            }
            case 2:
{
              c[j] = sin(((((7 * x) - phase_num) * 3.14159265358979323846) / 2));
              break; 
            }
            case 3:
{
              c[j] = cos(((((7 * x) - phase_num) * 3.14159265358979323846) / 2));
              break; 
            }
            case 4:
{
              c[j] = ((7 * x) - phase_num);
              break; 
            }
            case 5:
{
              c[j] = (1 - ((7 * x) - phase_num));
              break; 
            }
          }
      }
      else if ((p -> alt_palette) != 0U) {
        int n = (((((double )i) / ((p -> spectrum_points) - 1)) * ((sizeof(alt_palette) / sizeof(alt_palette[0]) - 1) / 3 - 1)) + .5);
        c[0] = (alt_palette[(3 * n) + 0] / 255.);
        c[1] = (alt_palette[(3 * n) + 1] / 255.);
        c[2] = (alt_palette[(3 * n) + 2] / 255.);
      }
      else {
        if (x < .13) 
          c[0] = 0;
        else if (x < .73) 
          c[0] = (1 * sin(((((x - .13) / .60) * 3.14159265358979323846) / 2)));
        else 
          c[0] = 1;
        if (x < .60) 
          c[1] = 0;
        else if (x < .91) 
          c[1] = (1 * sin(((((x - .60) / .31) * 3.14159265358979323846) / 2)));
        else 
          c[1] = 1;
        if (x < .60) 
          c[2] = (.5 * sin((((x - 0.00000) / .60) * 3.14159265358979323846)));
        else if (x < .78) 
          c[2] = 0;
        else 
          c[2] = ((x - .78) / .22);
      }
      palette[at].red = (.5 + (255 * c[(p -> perm) % 3]));
      palette[at].green = (.5 + (255 * c[((1 + (p -> perm)) + ((p -> perm) % 2)) % 3]));
      palette[at].blue = (.5 + (255 * c[((2 + (p -> perm)) - ((p -> perm) % 2)) % 3]));
    }
  }
}
static const Bytef fixed[] = "x\332eT\241\266\2450\fDVV>Y\371$re%2\237\200|2\022YY\211D\"+\337\'<y\345\312\375\fd\345f\222\224\313\236\235{\270\344L\247a\232\004\246\351\201d\230\222\304D\364^ \352\362S\"m\347\311\237\237\0274K\243\2302\265\035\v\371<\363y\354_\226g\354\214)e \2458\341\017\020J4\215[z<\271\277\367\00034@\177\330c\227\204 Ir.\005$U\200\260\224\326S\017\200=\\k\020QA\334%\342\020*\303P\234\211\366\036#\370R\276_\316s-\345\222Dlz\363my*;\217\373\346z\267\343\236\364\246\236\365\2419\305p\333\267\023(\207\265\333\233\325Y\342\243\265\357\262\215\263t\271$\276\226ea\271.\367&\320\347\202_\234\027\377\345\222\253\?\3422\364\207y\256\236\0229\331\033\f\376\227\266\"\356\253j\366\363\347\334US\034]\371\?\255\371\336\372z\265v\034\226\247\032\324\217\334\337\317U4\016\316{N\370\031\365\357iL\231y\033y\264\211D7\337\004\244\261\220D\346\001\261\357\355>\003\342\223\363\000\303\277\f[\214A,p\034`\255\355\364\037\372\224\342\277\f\207\255\036_V\a\034\241^\316W\257\177\b\242\300\034\f\276\033\?/9_\331f\346\036\025Y)\2301\257\2414|\035\365\237\3424k\003\244\003\242\261\006\b\275>z$\370\215:\270\363w\036/\265kF\v\020o6\242\301\364\336\027\325\257\321\364fs\231\215G\032=\257\305Di\304^\177\304R\364Q=\225\373\033\320\375\375\372\200\337\037\374}\334\337\020\364\310(]\304\267b\177\326Yrj\312\277\373\233\037\340";
static unsigned char *font;
#define font_x 5
#define font_y 12
#define font_X (font_x + 1)
#define pixel(x,y) pixels[(y) * cols + (x)]
#define print_at(x,y,c,t) print_at_(pixels,cols,x,y,c,t,0)
#define print_up(x,y,c,t) print_at_(pixels,cols,x,y,c,t,1)

static void print_at_(png_byte *pixels,int cols,int x,int y,int c,const char *text,int orientation)
{
  for (; ( *text) != 0; ++text) {
    int pos = ((((((( *text) < 32) || (( *text) > '~'))?'~' + 1 : ( *text))) - 32) * 12);
    int i;
    int j;
    for (i = 0; i < 12; ++i) {
      unsigned int line = font[pos++];
      for (j = 0; j < 5; (++j , (line <<= 1))) 
        if ((line & 128) != 0U) 
          switch(orientation){
            case 0:
{
              pixels[((y - i) * cols) + (x + j)] = c;
              break; 
            }
            case 1:
{
              pixels[((y + j) * cols) + (x + i)] = c;
              break; 
            }
          }
    }
    switch(orientation){
      case 0:
{
        x += 5 + 1;
        break; 
      }
      case 1:
{
        y += 5 + 1;
        break; 
      }
    }
  }
}

static int axis(double to,int max_steps,double *limit,char **prefix)
{
  double scale = 1;
  double step = (1 >= (10 * to))?1 : (10 * to);
  int i;
  int prefix_num = 0;
  if (max_steps != 0) {
    double try;
    double log_10 = __builtin_huge_val();
    double min_step = ((to *= 10) / max_steps);
    for (i = 5; i != 0; i >>= 1) 
      if ((try = ceil(log10((min_step * i)))) <= log_10) 
        ((step = (pow(10.,(log_10 = try)) / i)) , (log_10 -= (i > 1)));
    prefix_num = (floor((log_10 / 3)));
    scale = pow(10.,(-3. * prefix_num));
  }
   *prefix = (("pnum-kMGTPE" + prefix_num) + (((prefix_num != 0)?4 : 11)));
   *limit = (to * scale);
  return ((step * scale) + .5);
}
#define below 48
#define left 58
#define between 37
#define spectrum_width 14
#define right 35

static int stop(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  FILE *file = fopen((p -> out_name),"wb");
  uLong font_len = (96 * 12);
  int chans = effp -> in_signal.channels;
  int c_rows = ((((p -> rows) * chans) + chans) - 1);
  int rows = ((p -> raw) != 0U)?c_rows : (((48 + c_rows) + 30) + (20 * !(!((p -> title) != 0))));
  int cols = ((p -> raw) != 0U)?(p -> cols) : ((((58 + (p -> cols)) + 37) + 14) + 35);
  png_byte *pixels = (lsx_realloc(0,((cols * rows) * sizeof(( *pixels)))));
  png_bytepp png_rows = (lsx_realloc(0,(rows * sizeof(( *png_rows)))));
  png_structp png = png_create_write_struct("1.2.50",0,0,0);
  png_infop png_info = png_create_info_struct(png);
  png_color palette[256UL];
  int i;
  int j;
  int k;
  int base;
  int step;
  int tick_len = (3 - (p -> no_axes));
  char text[200UL];
  char *prefix;
  double limit;
  free((p -> shared));
  if (!(file != 0)) {
    ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_fail_impl("failed to create `%s\': %s",(p -> out_name),strerror( *__errno_location())));
    goto error;
  }
  ((( *sox_get_globals()).subsystem = "spectrogram.c") , lsx_debug_impl("signal-max=%g",(p -> max)));
  font = (lsx_realloc(0,font_len));
  (uncompress(font,&font_len,fixed,(sizeof(fixed) - 1)) == 0)?((void )0) : __assert_fail("uncompress(font, &font_len, fixed, sizeof(fixed)-1) == 0","spectrogram.c",519,"int stop(struct sox_effect_t *)");
  make_palette(p,palette);
  memset(pixels,Background,((cols * rows) * sizeof(( *pixels))));
  png_init_io(png,file);
  png_set_PLTE(png,png_info,palette,(fixed_palette + (p -> spectrum_points)));
  png_set_IHDR(png,png_info,((png_uint_32 )cols),((png_uint_32 )rows),8,2 | 1,0,0,0);
/* Put (0,0) at bottom-left of PNG */
  for (j = 0; j < rows; ++j) 
    png_rows[(rows - 1) - j] = (pixels + (j * cols));
/* Spectrogram */
  for (k = 0; k < chans; ++k) {
    priv_t *q = (priv_t *)( *((effp - (effp -> flow)) + k)).priv;
    base = ((!((p -> raw) != 0U) * 48) + (((chans - 1) - k) * ((p -> rows) + 1)));
    for (j = 0; j < (p -> rows); ++j) {
      for (i = 0; i < (p -> cols); ++i) 
        pixels[((base + j) * cols) + ((!((p -> raw) != 0U) * 58) + i)] = (colour(p,(q -> dBfs)[(i * (p -> rows)) + j]));
/* Y-axis lines */
      if (!((p -> raw) != 0U) && !((p -> no_axes) != 0U)) 
        pixels[((base + j) * cols) + (58 - 1)] = (pixels[((base + j) * cols) + (58 + (p -> cols))] = Grid);
    }
/* X-axis lines */
    if (!((p -> raw) != 0U) && !((p -> no_axes) != 0U)) 
      for (i = -1; i <= (p -> cols); ++i) 
        pixels[((base - 1) * cols) + (58 + i)] = (pixels[((base + (p -> rows)) * cols) + (58 + i)] = Grid);
  }
  if (!((p -> raw) != 0U)) {
/* Title */
    if (((p -> title) != 0) && ((i = (((int )(strlen((p -> title)))) * (5 + 1))) < (cols + 1))) 
      print_at_(pixels,cols,((cols - i) / 2),(rows - 12),Text,(p -> title),0);
/* Footer comment */
    if ((((int )(strlen((p -> comment)))) * (5 + 1)) < (cols + 1)) 
      print_at_(pixels,cols,1,12,Text,(p -> comment),0);
/* X-axis */
    step = axis((((((double )(p -> cols)) * (p -> step_size)) * (p -> block_steps)) / effp -> in_signal.rate),((p -> cols) / ((5 + 1) * 9 / 2)),&limit,&prefix);
/* Axis label */
    sprintf(text,"Time (%.1ss)",prefix);
    print_at_(pixels,cols,(58 + (((p -> cols) - ((5 + 1) * ((int )(strlen(text))))) / 2)),24,Text,text,0);
    for (i = 0; i <= limit; i += step) {{
        int y;
        int x = ((limit != 0.00000)?(((((double )i) / limit) * (p -> cols)) + .5) : 0);
/* Ticks */
        for (y = 0; y < tick_len; ++y) 
          pixels[((48 - 1 - y) * cols) + (58 - 1 + x)] = (pixels[(((48 + c_rows) + y) * cols) + (58 - 1 + x)] = Grid);
        if ((step == 5) && ((i % 10) != 0)) 
          continue; 
/* Tick labels */
        sprintf(text,"%g",(.1 * i));
        x = ((58 + x) - (3 * strlen(text)));
        print_at_(pixels,cols,x,48 - 6,Labels,text,0);
        print_at_(pixels,cols,x,((48 + c_rows) + 14),Labels,text,0);
      }
    }
/* Y-axis */
    step = axis((effp -> in_signal.rate / 2),(((p -> rows) - 1) / (12 * 3 + 1 >> 1)),&limit,&prefix);
/* Axis label */
    sprintf(text,"Frequency (%.1sHz)",prefix);
    print_at_(pixels,cols,10,(48 + ((c_rows - ((5 + 1) * ((int )(strlen(text))))) / 2)),Text,text,1);
    for (k = 0; k < chans; ++k) {
      base = (48 + (k * ((p -> rows) + 1)));
      for (i = 0; i <= limit; i += step) {{
          int x;
          int y = ((limit != 0.00000)?(((((double )i) / limit) * ((p -> rows) - 1)) + .5) : 0);
/* Ticks */
          for (x = 0; x < tick_len; ++x) 
            pixels[((base + y) * cols) + (58 - 1 - x)] = (pixels[((base + y) * cols) + ((58 + (p -> cols)) + x)] = Grid);
          if (((step == 5) && ((i % 10) != 0)) || ((!(i != 0) && (k != 0)) && (chans > 1))) 
            continue; 
/* Tick labels */
          sprintf(text,((i != 0)?"%5g" : "   DC"),(.1 * i));
          print_at_(pixels,cols,58 - 4 - (5 + 1) * 5,((base + y) + 5),Labels,text,0);
          sprintf(text,((i != 0)?"%g" : "DC"),(.1 * i));
          print_at_(pixels,cols,((58 + (p -> cols)) + 6),((base + y) + 5),Labels,text,0);
        }
      }
    }
/* Z-axis */
    k = ((400 <= c_rows)?400 : c_rows);
    base = (48 + ((c_rows - k) / 2));
/* Axis label */
    print_at_(pixels,cols,(((cols - 35) - 2) - (5 + 1)),(base - 13),Text,"dBFS",0);
/* Spectrum */
    for (j = 0; j < k; ++j) {
      png_byte b = (colour(p,((p -> dB_range) * ((j / (k - 1.)) - 1))));
      for (i = 0; i < 14; ++i) 
        pixels[((base + j) * cols) + (((cols - 35) - 1) - i)] = b;
    }
    step = (10 * ceil(((((p -> dB_range) / 10.) * (12 + 2)) / (k - 1))));
/* (Tick) labels */
    for (i = 0; i <= (p -> dB_range); i += step) {
      int y = (((((double )i) / (p -> dB_range)) * (k - 1)) + .5);
      sprintf(text,"%+i",((i - (p -> gain)) - (p -> dB_range)));
      print_at_(pixels,cols,((cols - 35) + 1),((base + y) + 5),Labels,text,0);
    }
  }
  free(font);
  png_set_rows(png,png_info,png_rows);
  png_write_png(png,png_info,0,0);
  fclose(file);
  error:
  png_destroy_write_struct(&png,&png_info);
  free(png_rows);
  free(pixels);
  free((p -> dBfs));
  return SOX_SUCCESS;
}

static int end(sox_effect_t *effp)
{
  return ((effp -> flow) != 0UL)?SOX_SUCCESS : stop(effp);
}

const sox_effect_handler_t *lsx_spectrogram_effect_fn()
{
  static sox_effect_handler_t handler = {("spectrogram"), (0), (256), (getopts), (start), (flow), (drain), (end), (0), ((sizeof(priv_t )))};
  static const char *lines[] = {("[options]"), ("\t-x num\tX-axis size in pixels; default derived or 800"), ("\t-X num\tX-axis pixels/second; default derived or 100"), ("\t-y num\tY-axis size in pixels (per channel); slow if not 1 + 2^n"), ("\t-Y num\tY-height total (i.e. not per channel); default 550"), ("\t-z num\tZ-axis range in dB; default 120"), ("\t-Z num\tZ-axis maximum in dBFS; default 0"), ("\t-q num\tZ-axis quantisation (0 - 249); default 249"), ("\t-w name\tWindow: Hann (default), Hamming, Bartlett, Rectangular, Kaiser"), ("\t-W num\tWindow adjust parameter (-10 - 10); applies only to Kaiser"), ("\t-s\tSlack overlap of windows"), ("\t-a\tSuppress axis lines"), ("\t-r\tRaw spectrogram; no axes or legends"), ("\t-l\tLight background"), ("\t-m\tMonochrome"), ("\t-h\tHigh colour"), ("\t-p num\tPermute colours (1 - 6); default 1"), ("\t-A\tAlternative, inferior, fixed colour-set (for compatibility only)"), ("\t-t text\tTitle text"), ("\t-c text\tComment text"), ("\t-o text\tOutput file name; default `spectrogram.png\'"), ("\t-d time\tAudio duration to fit to X-axis; e.g. 1:00, 48"), ("\t-S time\tStart the spectrogram at the given time through the input")};
  static char *usage;
  handler.usage = (lsx_usage_lines(&usage,lines,(sizeof(lines) / sizeof(lines[0]))));
  return (&handler);
}
