/* libSoX effect: remix   Copyright (c) 2008-9 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include <string.h>
typedef struct __unnamed_class___F0_L21_C9_unknown_scope_and_name_variable_declaration__variable_type_L126R_variable_name_unknown_scope_and_name__scope__mode__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__mix_power__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__num_out_channels__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__min_in_channels__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__un_named_base_type__Pe___variable_name_unknown_scope_and_name__scope__out_specs {
enum __unnamed_enum___F0_L22_C3_semi__COMMA__automatic__COMMA__manual {semi,automatic,manual}mode;
sox_bool mix_power;
unsigned int num_out_channels;
unsigned int min_in_channels;
struct __unnamed_class___F0_L25_C3_L127R_variable_declaration__variable_type___Pb__c__Pe___variable_name_L127R__scope__str__DELIMITER__L127R_variable_declaration__variable_type_Ui_variable_name_L127R__scope__num_in_channels__DELIMITER__L127R_variable_declaration__variable_type___Pb__L127R__scope__in_spec__Pe___variable_name_L127R__scope__in_specs {
/* Command-line argument to parse for this out_spec */
char *str;
unsigned int num_in_channels;
struct in_spec {
unsigned int channel_num;
double multiplier;}*in_specs;}*out_specs;}priv_t;
#define PARSE(SEP, SCAN, VAR, MIN, SEPARATORS) do {\
  end = strpbrk(text, SEPARATORS); \
  if (end == text) \
    SEP = *text++; \
  else { \
    SEP = (SEPARATORS)[strlen(SEPARATORS) - 1]; \
    n = sscanf(text, SCAN"%c", &VAR, &SEP); \
    if (n == 0 || VAR < MIN || (n == 2 && !strchr(SEPARATORS, SEP))) \
      return lsx_usage(effp); \
    text = end? end + 1 : text + strlen(text); \
  } \
} while (0)

static int parse(sox_effect_t *effp,char **argv,unsigned int channels)
{
  priv_t *p = (priv_t *)(effp -> priv);
  unsigned int i;
  unsigned int j;
  double mult;
  p -> min_in_channels = 0;
  for (i = 0; i < (p -> num_out_channels); ++i) {
    sox_bool mul_spec = sox_false;
    char *text;
    char *end;
/* 1st parse only */
    if (argv != 0) 
      (p -> out_specs)[i].str = ((argv[i] != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(argv[i]) + 1)))),argv[i]) : ((char *)((void *)0)));
    for (((j = 0) , (text = (p -> out_specs)[i].str)); ( *text) != 0; ) {{
        static const char separators[] = "-vpi,";
        char sep1;
        char sep2;
        int chan1 = 1;
        int chan2 = channels;
        int n;
        double multiplier = __builtin_huge_val();
        do {
          end = strpbrk(text,separators);
          if (end == text) 
            sep1 =  *(text++);
          else {
            sep1 = separators[strlen(separators) - 1];
            n = sscanf(text,"%i%c",&chan1,&sep1);
            if (((n == 0) || (chan1 < 0)) || ((n == 2) && !(strchr(separators,sep1) != 0))) 
              return lsx_usage(effp);
            text = ((end != 0)?(end + 1) : (text + strlen(text)));
          }
        }while (0);
        if (!(chan1 != 0)) {
          if ((j != 0U) || (( *text) != 0)) 
            return lsx_usage(effp);
          continue; 
        }
        if (sep1 == '-') 
          do {
            end = strpbrk(text,(separators + 1));
            if (end == text) 
              sep1 =  *(text++);
            else {
              sep1 = (separators + 1)[strlen((separators + 1)) - 1];
              n = sscanf(text,"%i%c",&chan2,&sep1);
              if (((n == 0) || (chan2 < 0)) || ((n == 2) && !(strchr((separators + 1),sep1) != 0))) 
                return lsx_usage(effp);
              text = ((end != 0)?(end + 1) : (text + strlen(text)));
            }
          }while (0);
        else 
          chan2 = chan1;
        if (sep1 != ',') {
          multiplier = (((sep1 == 'v')?1 : 0));
          do {
            end = strpbrk(text,(separators + 4));
            if (end == text) 
              sep2 =  *(text++);
            else {
              sep2 = (separators + 4)[strlen((separators + 4)) - 1];
              n = sscanf(text,"%lf%c",&multiplier,&sep2);
              if (((n == 0) || (multiplier < -__builtin_huge_val())) || ((n == 2) && !(strchr((separators + 4),sep2) != 0))) 
                return lsx_usage(effp);
              text = ((end != 0)?(end + 1) : (text + strlen(text)));
            }
          }while (0);
          if (sep1 != 'v') 
            multiplier = ((((sep1 == 'p')?1 : -1)) * exp(((multiplier * 2.30258509299404568402) * 0.05)));
          mul_spec = sox_true;
        }
        if (chan2 < chan1) {
          int t = chan1;
          chan1 = chan2;
          chan2 = t;
        }
        (p -> out_specs)[i].in_specs = (lsx_realloc((p -> out_specs)[i].in_specs,((((j + chan2) - chan1) + 1) * sizeof(( *(p -> out_specs)[i].in_specs)))));
        while(chan1 <= chan2){
          (p -> out_specs)[i].in_specs[j].channel_num = (chan1++ - 1);
          (p -> out_specs)[i].in_specs[j++].multiplier = multiplier;
        }
        p -> min_in_channels = (((p -> min_in_channels) >= ((unsigned int )chan2))?(p -> min_in_channels) : ((unsigned int )chan2));
      }
    }
    (p -> out_specs)[i].num_in_channels = j;
    mult = (1. / ((((p -> mix_power) != 0U)?sqrt(((double )j)) : j)));
    for (j = 0; j < (p -> out_specs)[i].num_in_channels; ++j) 
      if ((p -> out_specs)[i].in_specs[j].multiplier == __builtin_huge_val()) 
        (p -> out_specs)[i].in_specs[j].multiplier = ((((p -> mode) == automatic) || (((p -> mode) == semi) && !(mul_spec != 0U)))?mult : 1);
  }
  effp -> out_signal.channels = (p -> num_out_channels);
  return SOX_SUCCESS;
}

static int show(priv_t *p)
{
  unsigned int i;
  unsigned int j;
  for (j = 0; j < (p -> num_out_channels); j++) {
    ((( *sox_get_globals()).subsystem = "remix.c") , lsx_debug_impl("%i: ",j));
    for (i = 0; i < (p -> out_specs)[j].num_in_channels; i++) 
      ((( *sox_get_globals()).subsystem = "remix.c") , lsx_debug_impl("\t%i %g",(p -> out_specs)[j].in_specs[i].channel_num,(p -> out_specs)[j].in_specs[i].multiplier));
  }
  return SOX_SUCCESS;
}

static int create(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  (--argc , ++argv);
  if ((argc != 0) && !(strcmp(( *argv),"-m") != 0)) 
    ((((p -> mode = manual) , ++argv)) , --argc);
  if ((argc != 0) && !(strcmp(( *argv),"-a") != 0)) 
    ((((p -> mode = automatic) , ++argv)) , --argc);
  if ((argc != 0) && !(strcmp(( *argv),"-p") != 0)) 
    ((((p -> mix_power = sox_true) , ++argv)) , --argc);
  if (!(argc != 0)) {
    ((( *sox_get_globals()).subsystem = "remix.c") , lsx_fail_impl("must specify at least one output channel"));
    return SOX_EOF;
  }
  p -> num_out_channels = argc;
  p -> out_specs = (((((p -> num_out_channels) * sizeof(( *(p -> out_specs)))) != 0ULL)?memset(lsx_realloc(0,((p -> num_out_channels) * sizeof(( *(p -> out_specs))))),0,((p -> num_out_channels) * sizeof(( *(p -> out_specs))))) : ((void *)((void *)0))));
/* No channels yet; parse with dummy */
  return parse(effp,argv,1);
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  double max_sum = 0;
  unsigned int i;
  unsigned int j;
  int non_integer = 0;
  parse(effp,0,effp -> in_signal.channels);
  if (effp -> in_signal.channels < (p -> min_in_channels)) {
    ((( *sox_get_globals()).subsystem = "remix.c") , lsx_fail_impl("too few input channels"));
    return SOX_EOF;
  }
  for (j = 0; j < effp -> out_signal.channels; j++) {
    double sum = 0;
    for (i = 0; i < (p -> out_specs)[j].num_in_channels; i++) {
      double mult = (p -> out_specs)[j].in_specs[i].multiplier;
      sum += fabs(mult);
      non_integer += (floor(mult) != mult);
    }
    max_sum = ((max_sum >= sum)?max_sum : sum);
  }
  if ((effp -> in_signal.mult != 0) && (max_sum > 1)) 
     *effp -> in_signal.mult /= max_sum;
  if (!(non_integer != 0)) 
    effp -> out_signal.precision = effp -> in_signal.precision;
  else 
    effp -> out_signal.precision = 32;
  show(p);
  return SOX_SUCCESS;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  unsigned int i;
  unsigned int j;
  unsigned int len;
  len = (((( *isamp / effp -> in_signal.channels) <= ( *osamp / effp -> out_signal.channels))?( *isamp / effp -> in_signal.channels) : ( *osamp / effp -> out_signal.channels)));
   *isamp = (len * effp -> in_signal.channels);
   *osamp = (len * effp -> out_signal.channels);
  for (; len-- != 0U; ibuf += effp -> in_signal.channels) 
    for (j = 0; j < effp -> out_signal.channels; j++) {
      double out = 0;
      for (i = 0; i < (p -> out_specs)[j].num_in_channels; i++) 
        out += (ibuf[(p -> out_specs)[j].in_specs[i].channel_num] * (p -> out_specs)[j].in_specs[i].multiplier);
       *(obuf++) = (((out < 0)?(((out <= ((sox_sample_t )(1 << 32 - 1)) - 0.5)?((++effp -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (out - 0.5))) : (((out >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 0.5)?((++effp -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : (out + 0.5)))));
    }
  return SOX_SUCCESS;
}

static int closedown(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  unsigned int i;
  for (i = 0; i < (p -> num_out_channels); ++i) {
    free((p -> out_specs)[i].str);
    free((p -> out_specs)[i].in_specs);
  }
  free((p -> out_specs));
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_remix_effect_fn()
{
  static sox_effect_handler_t handler = {("remix"), ("[-m|-a] [-p] <0|in-chan[v|p|i volume]{,in-chan[v|p|i volume]}>"), ((16 | 1 | 128 | 4)), (create), (start), (flow), ((sox_effect_handler_drain )((void *)0)), ((sox_effect_handler_stop )((void *)0)), (closedown), ((sizeof(priv_t )))};
  return (&handler);
}
/*----------------------- The `channels' effect alias ------------------------*/

static int channels_create(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
/* To check for extraneous chars. */
  char dummy;
  if (argc == 2) {
    if ((sscanf(argv[1],"%d %c",((int *)(&p -> num_out_channels)),&dummy) != 1) || (((int )(p -> num_out_channels)) <= 0)) 
      return lsx_usage(effp);
    effp -> out_signal.channels = (p -> num_out_channels);
  }
  else if (argc != 1) 
    return lsx_usage(effp);
  return SOX_SUCCESS;
}

static int channels_start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  unsigned int num_out_channels = ((p -> num_out_channels) != 0)?(p -> num_out_channels) : effp -> out_signal.channels;
  unsigned int i;
  unsigned int j;
  p -> out_specs = ((((num_out_channels * sizeof(( *(p -> out_specs)))) != 0ULL)?memset(lsx_realloc(0,(num_out_channels * sizeof(( *(p -> out_specs))))),0,(num_out_channels * sizeof(( *(p -> out_specs))))) : ((void *)((void *)0))));
  if (effp -> in_signal.channels == num_out_channels) 
    return 32;
  if (effp -> in_signal.channels > num_out_channels) {
    for (j = 0; j < num_out_channels; j++) {
      unsigned int in_per_out = ((((effp -> in_signal.channels + num_out_channels) - 1) - j) / num_out_channels);
      (p -> out_specs)[j].in_specs = (lsx_realloc(0,(in_per_out * sizeof(( *(p -> out_specs)[j].in_specs)))));
      (p -> out_specs)[j].num_in_channels = in_per_out;
      for (i = 0; i < in_per_out; ++i) {
        (p -> out_specs)[j].in_specs[i].channel_num = ((i * num_out_channels) + j);
        (p -> out_specs)[j].in_specs[i].multiplier = (1. / in_per_out);
      }
    }
  }
  else 
    for (j = 0; j < num_out_channels; j++) {
      (p -> out_specs)[j].in_specs = (lsx_realloc(0,(1 * sizeof(( *(p -> out_specs)[j].in_specs)))));
      (p -> out_specs)[j].num_in_channels = 1;
      (p -> out_specs)[j].in_specs[0].channel_num = (j % effp -> in_signal.channels);
      (p -> out_specs)[j].in_specs[0].multiplier = 1;
    }
  effp -> out_signal.channels = (p -> num_out_channels = num_out_channels);
  effp -> out_signal.precision = ((effp -> in_signal.channels > num_out_channels)?32 : effp -> in_signal.precision);
  show(p);
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_channels_effect_fn()
{
  static sox_effect_handler_t handler;
  handler =  *lsx_remix_effect_fn();
  handler.name = "channels";
  handler.usage = "number";
  handler.flags &= (~128);
  handler.getopts = channels_create;
  handler.start = channels_start;
  return (&handler);
}
/*------------------------- The `oops' effect alias --------------------------*/

static int oops_getopts(sox_effect_t *effp,int argc,char **argv)
{
  char *args[] = {(0), ("1,2i"), ("1,2i")};
  args[0] = argv[0];
  return (--argc != 0)?lsx_usage(effp) : create(effp,3,args);
}

const sox_effect_handler_t *lsx_oops_effect_fn()
{
  static sox_effect_handler_t handler;
  handler =  *lsx_remix_effect_fn();
  handler.name = "oops";
  handler.usage = ((const char *)((void *)0));
  handler.getopts = oops_getopts;
  return (&handler);
}
