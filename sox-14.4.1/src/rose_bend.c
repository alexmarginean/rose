/* libSoX effect: Pitch Bend   (c) 2008 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Portions based on http://www.dspdimension.com/download smbPitchShift.cpp:
 *
 * COPYRIGHT 1999-2006 Stephan M. Bernsee <smb [AT] dspdimension [DOT] com>
 *
 *             The Wide Open License (WOL)
 *
 * Permission to use, copy, modify, distribute and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice and this license appear in all source copies. 
 * THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY OF
 * ANY KIND. See http://www.dspguru.com/wol.htm for more information.
 */
#ifdef NDEBUG /* Enable assert always. */
#undef NDEBUG /* Must undef above assert.h or other that might include it. */
#endif
#include "sox_i.h"
#include <assert.h>
#define MAX_FRAME_LENGTH 8192
typedef struct __unnamed_class___F0_L40_C9_unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__nbends__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__un_named_base_type__Pe___variable_name_unknown_scope_and_name__scope__bends__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__frame_rate__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__in_pos__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__bends_pos__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__shift__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_f_index_8192_Ae__variable_name_unknown_scope_and_name__scope__gInFIFO__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_f_index_8192_Ae__variable_name_unknown_scope_and_name__scope__gOutFIFO__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_d_index_16384_Ae__variable_name_unknown_scope_and_name__scope__gFFTworksp__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_f_index_4097_Ae__variable_name_unknown_scope_and_name__scope__gLastPhase__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_f_index_4097_Ae__variable_name_unknown_scope_and_name__scope__gSumPhase__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_f_index_16384_Ae__variable_name_unknown_scope_and_name__scope__gOutputAccum__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_f_index_8192_Ae__variable_name_unknown_scope_and_name__scope__gAnaFreq__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_f_index_8192_Ae__variable_name_unknown_scope_and_name__scope__gAnaMagn__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_f_index_8192_Ae__variable_name_unknown_scope_and_name__scope__gSynFreq__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_f_index_8192_Ae__variable_name_unknown_scope_and_name__scope__gSynMagn__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_l_variable_name_unknown_scope_and_name__scope__gRover__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__fftFrameSize__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__ovsamp {
/* Number of bends requested */
unsigned int nbends;
struct __unnamed_class___F0_L42_C3_L126R_variable_declaration__variable_type___Pb__c__Pe___variable_name_L126R__scope__str__DELIMITER__L126R_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_L126R__scope__start__DELIMITER__L126R_variable_declaration__variable_type_d_variable_name_L126R__scope__cents__DELIMITER__L126R_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_L126R__scope__duration {
/* Command-line argument to parse for this bend */
char *str;
/* Start bending when in_pos equals this */
size_t start;
double cents;
/* Number of samples to bend */
uint64_t duration;}*bends;
unsigned int frame_rate;
/* Number of samples read from the input stream */
size_t in_pos;
/* Number of bends completed so far */
unsigned int bends_pos;
double shift;
float gInFIFO[8192UL];
float gOutFIFO[8192UL];
double gFFTworksp[2 * 8192];
float gLastPhase[8192 / 2 + 1];
float gSumPhase[8192 / 2 + 1];
float gOutputAccum[2 * 8192];
float gAnaFreq[8192UL];
float gAnaMagn[8192UL];
float gSynFreq[8192UL];
float gSynMagn[8192UL];
long gRover;
int fftFrameSize;
int ovsamp;}priv_t;

static int parse(sox_effect_t *effp,char **argv,sox_rate_t rate)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t i;
  uint64_t time = 0;
  uint64_t delay;
  const char *next;
{
    for (i = 0; i < (p -> nbends); ++i) {
/* 1st parse only */
      if (argv != 0) 
        (p -> bends)[i].str = ((argv[i] != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(argv[i]) + 1)))),argv[i]) : ((char *)((void *)0)));
      next = lsx_parsesamples(rate,(p -> bends)[i].str,&delay,'t');
      if ((next == ((const char *)((void *)0))) || (( *next) != ',')) 
        break; 
      (p -> bends)[i].start = (time += delay);
      (p -> bends)[i].cents = strtod((next + 1),((char **)(&next)));
      if (((p -> bends)[i].cents == 0) || (( *next) != ',')) 
        break; 
      next = lsx_parsesamples(rate,(next + 1),&(p -> bends)[i].duration,'t');
      if ((next == ((const char *)((void *)0))) || (( *next) != 0)) 
        break; 
      time += (p -> bends)[i].duration;
    }
  }
  if (i < (p -> nbends)) 
    return lsx_usage(effp);
  return SOX_SUCCESS;
}

static int create(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  const char *opts = "f:o:";
  int c;
  lsx_getopt_t optstate;
  lsx_getopt_init(argc,argv,opts,0,lsx_getopt_flag_none,1,&optstate);
  p -> frame_rate = 25;
  p -> ovsamp = 16;
  while((c = lsx_getopt(&optstate)) != -1){
    switch(c){
      case 'f':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < 10)) || (d > 80)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "bend.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","frame_rate",((double )10),((double )80)));
            return lsx_usage(effp);
          }
          p -> frame_rate = d;
          break; 
        }
      }
      case 'o':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < 4)) || (d > 32)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "bend.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","ovsamp",((double )4),((double )32)));
            return lsx_usage(effp);
          }
          p -> ovsamp = d;
          break; 
        }
      }
      default:
{
        ((( *sox_get_globals()).subsystem = "bend.c") , lsx_fail_impl("unknown option `-%c\'",optstate.opt));
        return lsx_usage(effp);
      }
    }
  }
  ((argc -= optstate.ind) , (argv += optstate.ind));
  p -> nbends = argc;
  p -> bends = (((((p -> nbends) * sizeof(( *(p -> bends)))) != 0ULL)?memset(lsx_realloc(0,((p -> nbends) * sizeof(( *(p -> bends))))),0,((p -> nbends) * sizeof(( *(p -> bends))))) : ((void *)((void *)0))));
/* No rate yet; parse with dummy */
  return parse(effp,argv,0.);
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  unsigned int i;
  int n = ((effp -> in_signal.rate / (p -> frame_rate)) + .5);
  for (p -> fftFrameSize = 2; n > 2; ((p -> fftFrameSize <<= 1) , (n >>= 1))) ;
  ((p -> fftFrameSize) <= 8192)?((void )0) : __assert_fail("p->fftFrameSize <= 8192","bend.c",125,"int start(struct sox_effect_t *)");
  p -> shift = 1;
/* Re-parse now rate is known */
  parse(effp,0,effp -> in_signal.rate);
  p -> in_pos = (p -> bends_pos = 0);
  for (i = 0; i < (p -> nbends); ++i) 
    if ((p -> bends)[i].duration != 0UL) 
      return SOX_SUCCESS;
  return 32;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t i;
  size_t len = ( *isamp = ( *osamp = (( *isamp <=  *osamp)? *isamp :  *osamp)));
  double magn;
  double phase;
  double tmp;
  double window;
  double real;
  double imag;
  double freqPerBin;
  double expct;
  long k;
  long qpd;
  long index;
  long inFifoLatency;
  long stepSize;
  long fftFrameSize2;
  float pitchShift = (p -> shift);
/* set up some handy variables */
  fftFrameSize2 = ((p -> fftFrameSize) / 2);
  stepSize = ((p -> fftFrameSize) / (p -> ovsamp));
  freqPerBin = (effp -> in_signal.rate / (p -> fftFrameSize));
  expct = ((2. * 3.14159265358979323846 * ((double )stepSize)) / ((double )(p -> fftFrameSize)));
  inFifoLatency = ((p -> fftFrameSize) - stepSize);
  if (!((p -> gRover) != 0L)) 
    p -> gRover = inFifoLatency;
/* main processing loop */
  for (i = 0; i < len; i++) {
    sox_sample_t sox_macro_temp_sample;
    double sox_macro_temp_double;
    ++p -> in_pos;
/* As long as we have not yet collected enough data just read in */
    (p -> gInFIFO)[p -> gRover] = ((((sox_macro_temp_double , (sox_macro_temp_sample = ibuf[i]))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - 128)?((++effp -> clips , 1)) : (((sox_macro_temp_sample + 128) & ~255) * (1. / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.))))));
    obuf[i] = ((sox_sample_t )((((sox_macro_temp_sample , (sox_macro_temp_double = ((p -> gOutFIFO)[(p -> gRover) - inFifoLatency] * (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.))))) , ((sox_macro_temp_double < ((sox_sample_t )(1 << 32 - 1)))?((++effp -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (((sox_macro_temp_double >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)?(((sox_macro_temp_double > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)?((++effp -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : sox_macro_temp_double))))));
    p -> gRover++;
/* now we have enough data for processing */
    if ((p -> gRover) >= (p -> fftFrameSize)) {
      if (((p -> bends_pos) != (p -> nbends)) && ((p -> in_pos) >= ((p -> bends)[p -> bends_pos].start + (p -> bends)[p -> bends_pos].duration))) {
        pitchShift = (p -> shift *= pow(2.,((p -> bends)[p -> bends_pos].cents / 1200)));
        ++p -> bends_pos;
      }
      if (((p -> bends_pos) != (p -> nbends)) && ((p -> in_pos) >= (p -> bends)[p -> bends_pos].start)) {
        double progress = (((double )((p -> in_pos) - (p -> bends)[p -> bends_pos].start)) / (p -> bends)[p -> bends_pos].duration);
        progress = (1 - cos((3.14159265358979323846 * progress)));
        progress *= ((p -> bends)[p -> bends_pos].cents * (.5 / 1200));
        pitchShift = ((p -> shift) * pow(2.,progress));
      }
      p -> gRover = inFifoLatency;
/* do windowing and re,im interleave */
      for (k = 0; k < (p -> fftFrameSize); k++) {
        window = ((-.5 * cos(((2 * 3.14159265358979323846 * k) / ((double )(p -> fftFrameSize))))) + .5);
        (p -> gFFTworksp)[2 * k] = ((p -> gInFIFO)[k] * window);
        (p -> gFFTworksp)[(2 * k) + 1] = 0.;
      }
/* ***************** ANALYSIS ******************* */
      lsx_safe_cdft((2 * (p -> fftFrameSize)),1,(p -> gFFTworksp));
/* this is the analysis step */
      for (k = 0; k <= fftFrameSize2; k++) {
/* de-interlace FFT buffer */
        real = (p -> gFFTworksp)[2 * k];
        imag = -(p -> gFFTworksp)[(2 * k) + 1];
/* compute magnitude and phase */
        magn = (2. * sqrt(((real * real) + (imag * imag))));
        phase = atan2(imag,real);
/* compute phase difference */
        tmp = (phase - (p -> gLastPhase)[k]);
        (p -> gLastPhase)[k] = phase;
/* subtract expected phase difference */
        tmp -= (((double )k) * expct);
/* map delta phase into +/- Pi interval */
        qpd = (tmp / 3.14159265358979323846);
        if (qpd >= 0) 
          qpd += (qpd & 1);
        else 
          qpd -= (qpd & 1);
        tmp -= (3.14159265358979323846 * ((double )qpd));
/* get deviation from bin frequency from the +/- Pi interval */
        tmp = (((p -> ovsamp) * tmp) / (2. * 3.14159265358979323846));
/* compute the k-th partials' true frequency */
        tmp = ((((double )k) * freqPerBin) + (tmp * freqPerBin));
/* store magnitude and true frequency in analysis arrays */
        (p -> gAnaMagn)[k] = magn;
        (p -> gAnaFreq)[k] = tmp;
      }
/* this does the actual pitch shifting */
      memset((p -> gSynMagn),0,((p -> fftFrameSize) * sizeof(float )));
      memset((p -> gSynFreq),0,((p -> fftFrameSize) * sizeof(float )));
      for (k = 0; k <= fftFrameSize2; k++) {
        index = (k * pitchShift);
        if (index <= fftFrameSize2) {
          (p -> gSynMagn)[index] += (p -> gAnaMagn)[k];
          (p -> gSynFreq)[index] = ((p -> gAnaFreq)[k] * pitchShift);
        }
      }
/* SYNTHESIS */
      for (k = 0; k <= fftFrameSize2; k++) {
/* get magnitude and true frequency from synthesis arrays */
        ((magn = (p -> gSynMagn)[k]) , (tmp = (p -> gSynFreq)[k]));
/* subtract bin mid frequency */
        tmp -= (((double )k) * freqPerBin);
/* get bin deviation from freq deviation */
        tmp /= freqPerBin;
/* take p->ovsamp into account */
        tmp = ((2. * 3.14159265358979323846 * tmp) / (p -> ovsamp));
/* add the overlap phase advance back in */
        tmp += (((double )k) * expct);
/* accumulate delta phase to get bin phase */
        (p -> gSumPhase)[k] += tmp;
        phase = (p -> gSumPhase)[k];
/* get real and imag part and re-interleave */
        (p -> gFFTworksp)[2 * k] = (magn * cos(phase));
        (p -> gFFTworksp)[(2 * k) + 1] = (-magn * sin(phase));
      }
      for (k = ((p -> fftFrameSize) + 2); k < (2 * (p -> fftFrameSize)); k++) 
/* zero negative frequencies */
        (p -> gFFTworksp)[k] = 0.;
      lsx_safe_cdft((2 * (p -> fftFrameSize)),-1,(p -> gFFTworksp));
/* do windowing and add to output accumulator */
      for (k = 0; k < (p -> fftFrameSize); k++) {
        window = ((-.5 * cos(((2. * 3.14159265358979323846 * ((double )k)) / ((double )(p -> fftFrameSize))))) + .5);
        (p -> gOutputAccum)[k] += (((2. * window) * (p -> gFFTworksp)[2 * k]) / (fftFrameSize2 * (p -> ovsamp)));
      }
      for (k = 0; k < stepSize; k++) 
        (p -> gOutFIFO)[k] = (p -> gOutputAccum)[k];
/* shift accumulator */
      memmove((p -> gOutputAccum),((p -> gOutputAccum) + stepSize),((p -> fftFrameSize) * sizeof(float )));
/* move input FIFO */
      for (k = 0; k < inFifoLatency; k++) 
        (p -> gInFIFO)[k] = (p -> gInFIFO)[k + stepSize];
    }
  }
  return SOX_SUCCESS;
}

static int stop(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  if ((p -> bends_pos) != (p -> nbends)) 
    ((( *sox_get_globals()).subsystem = "bend.c") , lsx_warn_impl("Input audio too short; bends not applied: %u",((p -> nbends) - (p -> bends_pos))));
  return SOX_SUCCESS;
}

static int lsx_kill(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  unsigned int i;
  for (i = 0; i < (p -> nbends); ++i) 
    free((p -> bends)[i].str);
  free((p -> bends));
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_bend_effect_fn()
{
  static sox_effect_handler_t handler = {("bend"), ("[-f frame-rate(25)] [-o over-sample(16)] {delay,cents,duration}"), (0), (create), (start), (flow), (0), (stop), (lsx_kill), ((sizeof(priv_t )))};
  return (&handler);
}
