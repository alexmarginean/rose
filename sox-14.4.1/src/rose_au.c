/* libSoX Sun format with header (SunOS 4.1; see /usr/demo/SOUND).
 * Copyright 1991, 1992, 1993 Guido van Rossum And Sundry Contributors.
 *
 * This source code is freely redistributable and may be used for
 * any purpose.  This copyright notice must be maintained.
 * Guido van Rossum And Sundry Contributors are not responsible for
 * the consequences of using this software.
 *
 * October 7, 1998 - cbagwell@sprynet.com
 *   G.723 was using incorrect # of bits.  Corrected to 3 and 5 bits.
 *
 * NeXT uses this format also, but has more format codes defined.
 * DEC uses a slight variation and swaps bytes.
 * We support only the common formats, plus
 * CCITT G.721 (32 kbit/s) and G.723 (24/40 kbit/s),
 * courtesy of Sun's public domain implementation.
 */
#include "sox_i.h"
#include "g72x.h"
#include <string.h>
/* Magic numbers used in Sun and NeXT audio files */
static struct __unnamed_class___F0_L24_C8_unknown_scope_and_name_variable_declaration__variable_type__Ab_c_index_4_Ae__variable_name_unknown_scope_and_name__scope__str__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__reverse_bytes__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__Cc__Pe___variable_name_unknown_scope_and_name__scope__desc {
char str[4UL];
sox_bool reverse_bytes;
const char *desc;}id[] = {{(".snd"), (1), ("big-endian `.snd\'")}, {("dns."), (0), ("little-endian `.snd\'")}, {("\000ds."), (0), ("little-endian `\\0ds.\' (for DEC)")}, {(".sd"), (1), ("big-endian `\\0ds.\'")}, {("    "), (0), ((const char *)((void *)0))}};
#define FIXED_HDR     24
#define SUN_UNSPEC    ~0u         /* Unspecified data size (this is legal) */
typedef enum  {Unspecified,Mulaw_8,Linear_8,Linear_16,Linear_24,Linear_32,Float,Double,Indirect,Nested,Dsp_core,Dsp_data_8,Dsp_data_16,Dsp_data_24,Dsp_data_32,Unknown,Display,Mulaw_squelch,Emphasized,Compressed,Compressed_emphasized,Dsp_commands,Dsp_commands_samples,Adpcm_g721,Adpcm_g722,Adpcm_g723_3,Adpcm_g723_5,Alaw_8,Unknown_other}ft_encoding_t;
static const char *const str[] = {("Unspecified"), ("8-bit mu-law"), ("8-bit signed linear"), ("16-bit signed linear"), ("24-bit signed linear"), ("32-bit signed linear"), ("Floating-point"), ("Double precision float"), ("Fragmented sampled data"), ("Unknown"), ("DSP program"), ("8-bit fixed-point"), ("16-bit fixed-point"), ("24-bit fixed-point"), ("32-bit fixed-point"), ("Unknown"), ("Non-audio data"), ("Mu-law squelch"), ("16-bit linear with emphasis"), ("16-bit linear with compression"), ("16-bit linear with emphasis and compression"), ("Music Kit DSP commands"), ("Music Kit DSP samples"), ("4-bit G.721 ADPCM"), ("G.722 ADPCM"), ("3-bit G.723 ADPCM"), ("5-bit G.723 ADPCM"), ("8-bit a-law"), ("Unknown")};

static ft_encoding_t ft_enc(unsigned int size,sox_encoding_t encoding)
{
  if ((encoding == SOX_ENCODING_ULAW) && (size == 8)) 
    return Mulaw_8;
  if ((encoding == SOX_ENCODING_ALAW) && (size == 8)) 
    return Alaw_8;
  if ((encoding == SOX_ENCODING_SIGN2) && (size == 8)) 
    return Linear_8;
  if ((encoding == SOX_ENCODING_SIGN2) && (size == 16)) 
    return Linear_16;
  if ((encoding == SOX_ENCODING_SIGN2) && (size == 24)) 
    return Linear_24;
  if ((encoding == SOX_ENCODING_SIGN2) && (size == 32)) 
    return Linear_32;
  if ((encoding == SOX_ENCODING_FLOAT) && (size == 32)) 
    return Float;
  if ((encoding == SOX_ENCODING_FLOAT) && (size == 64)) 
    return Double;
  return Unspecified;
}

static sox_encoding_t sox_enc(uint32_t ft_encoding,unsigned int *size)
{
  switch(ft_encoding){
    case Mulaw_8:
{
       *size = 8;
      return SOX_ENCODING_ULAW;
    }
    case Alaw_8:
{
       *size = 8;
      return SOX_ENCODING_ALAW;
    }
    case Linear_8:
{
       *size = 8;
      return SOX_ENCODING_SIGN2;
    }
    case Linear_16:
{
       *size = 16;
      return SOX_ENCODING_SIGN2;
    }
    case Linear_24:
{
       *size = 24;
      return SOX_ENCODING_SIGN2;
    }
    case Linear_32:
{
       *size = 32;
      return SOX_ENCODING_SIGN2;
    }
    case Float:
{
       *size = 32;
      return SOX_ENCODING_FLOAT;
    }
    case Double:
{
       *size = 64;
      return SOX_ENCODING_FLOAT;
    }
/* read-only */
    case Adpcm_g721:
{
       *size = 4;
      return SOX_ENCODING_G721;
    }
/* read-only */
    case Adpcm_g723_3:
{
       *size = 3;
      return SOX_ENCODING_G723;
    }
/* read-only */
    case Adpcm_g723_5:
{
       *size = 5;
      return SOX_ENCODING_G723;
    }
    default:
{
      return SOX_ENCODING_UNKNOWN;
    }
  }
}
/* For G72x decoding: */
typedef struct __unnamed_class___F0_L82_C9_unknown_scope_and_name_variable_declaration__variable_type_g72x_state_variable_name_unknown_scope_and_name__scope__state__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb___Fb_i_Gb_i__sep__i__sep____Pb__g72x_state__Pe___Fe___Pe___variable_name_unknown_scope_and_name__scope__dec_routine__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ui_variable_name_unknown_scope_and_name__scope__in_buffer__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__in_bits {
struct g72x_state state;
int (*dec_routine)(int , int , struct g72x_state *);
unsigned int in_buffer;
int in_bits;}priv_t;
/*
 * Unpack input codes and pass them back as bytes.
 * Returns 1 if there is residual input, returns -1 if eof, else returns 0.
 * (Adapted from Sun's decode.c.)
 */

static int unpack_input(sox_format_t *ft,unsigned char *code)
{
  priv_t *p = (priv_t *)(ft -> priv);
  unsigned char in_byte;
  if ((p -> in_bits) < ((int )ft -> encoding.bits_per_sample)) {
    if (lsx_read_b_buf(ft,&in_byte,((size_t )1)) != 1) {
       *code = 0;
      return -1;
    }
    p -> in_buffer |= (in_byte << (p -> in_bits));
    p -> in_bits += 8;
  }
   *code = ((p -> in_buffer) & ((1 << ft -> encoding.bits_per_sample) - 1));
  p -> in_buffer >>= ft -> encoding.bits_per_sample;
  p -> in_bits -= ft -> encoding.bits_per_sample;
  return (p -> in_bits) > 0;
}

static size_t dec_read(sox_format_t *ft,sox_sample_t *buf,size_t samp)
{
  priv_t *p = (priv_t *)(ft -> priv);
  unsigned char code;
  size_t done;
  for (done = 0; (samp > 0) && (unpack_input(ft,&code) >= 0); (++done , --samp)) 
     *(buf++) = (( *(p -> dec_routine))(code,3,&p -> state) << 32 - 16);
  return done;
}

static int startread(sox_format_t *ft)
{
  priv_t *p = (priv_t *)(ft -> priv);
/* These 6 variables represent a Sun sound */
  char magic[4UL];
/* header on disk.  The uint32_t are written as */
  uint32_t hdr_size;
/* big-endians.  At least extra bytes (totalling */
  uint32_t data_size;
/* hdr_size - FIXED_HDR) are an "info" field of */
  uint32_t ft_encoding;
/* unspecified nature, usually a string.  By */
  uint32_t rate;
/* convention the header size is a multiple of 4. */
  uint32_t channels;
  unsigned int i;
  unsigned int bits_per_sample;
  sox_encoding_t encoding;
  if (lsx_readchars(ft,magic,(sizeof(magic))) != 0) 
    return SOX_EOF;
  for (i = 0; (id[i].desc != 0) && (memcmp(magic,id[i].str,(sizeof(magic))) != 0); ++i) ;
  if (!(id[i].desc != 0)) {
    lsx_fail_errno(ft,SOX_EHDR,"au: can\'t find Sun/NeXT/DEC identifier");
    return SOX_EOF;
  }
  ((( *sox_get_globals()).subsystem = "au.c") , lsx_report_impl("found %s identifier",id[i].desc));
  ft -> encoding.reverse_bytes = id[i].reverse_bytes;
  if (((((lsx_readdw(ft,&hdr_size) != 0) || (lsx_readdw(ft,&data_size) != 0)) || (lsx_readdw(ft,&ft_encoding) != 0)) || (lsx_readdw(ft,&rate) != 0)) || (lsx_readdw(ft,&channels) != 0)) 
/* Can be SUN_UNSPEC */
    return SOX_EOF;
  if (hdr_size < 24) {
    lsx_fail_errno(ft,SOX_EHDR,"header size %u is too small",hdr_size);
    return SOX_EOF;
  }
  if (hdr_size < (24 + 4)) 
    ((( *sox_get_globals()).subsystem = "au.c") , lsx_warn_impl("header size %u is too small",hdr_size));
  if (!((encoding = sox_enc(ft_encoding,&bits_per_sample)) != 0U)) {
    int n = ((ft_encoding <= Unknown_other)?ft_encoding : Unknown_other);
    lsx_fail_errno(ft,SOX_EFMT,"unsupported encoding `%s\' (%#x)",str[n],ft_encoding);
    return SOX_EOF;
  }
  switch(ft_encoding){
    case Adpcm_g721:
{
      p -> dec_routine = lsx_g721_decoder;
      break; 
    }
    case Adpcm_g723_3:
{
      p -> dec_routine = lsx_g723_24_decoder;
      break; 
    }
    case Adpcm_g723_5:
{
      p -> dec_routine = lsx_g723_40_decoder;
      break; 
    }
  }
  if ((p -> dec_routine) != 0) {
    lsx_g72x_init_state(&p -> state);
    ft -> handler.seek = ((sox_format_handler_seek )((void *)0));
    ft -> handler.read = dec_read;
  }
  if (hdr_size > 24) {
    size_t info_size = (hdr_size - 24);
/* +1 ensures null-terminated */
    char *buf = (((1 * (info_size + 1)) != 0UL)?memset(lsx_realloc(0,(1 * (info_size + 1))),0,(1 * (info_size + 1))) : ((void *)((void *)0)));
    if (lsx_readchars(ft,buf,info_size) != SOX_SUCCESS) {
      free(buf);
      return SOX_EOF;
    }
    sox_append_comments(&ft -> oob.comments,buf);
    free(buf);
  }
  if (data_size == ~0u) 
    data_size = 0;
  return lsx_check_read_params(ft,channels,((sox_rate_t )rate),encoding,bits_per_sample,((((uint64_t )data_size) * 8) / bits_per_sample),sox_true);
}

static int write_header(sox_format_t *ft)
{
  char *comment = lsx_cat_comments(ft -> oob.comments);
/* Write out null-terminated */
  size_t len = (strlen(comment) + 1);
/* Minimum & multiple of 4 bytes */
  size_t info_len = (4 >= ((len + 3) & (~3u)))?4 : ((len + 3) & (~3u));
  int i = (ft -> encoding.reverse_bytes == 0)?2 : 0;
  uint64_t size64 = ((ft -> olength) != 0UL)?(ft -> olength) : ft -> signal.length;
  unsigned int size = (size64 == 0)?~0u : ((((size64 * (ft -> encoding.bits_per_sample >> 3)) > (2147483647 * 2U + 1U))?~0u : ((unsigned int )(size64 * (ft -> encoding.bits_per_sample >> 3)))));
  sox_bool error = ((((((((0 || (((((lsx_writebuf(ft,id[i].str,(sizeof(id[i].str)))) == sizeof(id[i].str))?SOX_SUCCESS : SOX_EOF)) != 0)) || (lsx_writedw(ft,(24 + ((unsigned int )info_len))) != 0)) || (lsx_writedw(ft,size) != 0)) || (lsx_writedw(ft,(ft_enc(ft -> encoding.bits_per_sample,ft -> encoding.encoding))) != 0)) || (lsx_writedw(ft,((unsigned int )(ft -> signal.rate + .5))) != 0)) || (lsx_writedw(ft,ft -> signal.channels) != 0)) || ((((lsx_writebuf(ft,comment,len) == len)?SOX_SUCCESS : SOX_EOF)) != 0)) || (lsx_padbytes(ft,(info_len - len)) != 0));
  free(comment);
  return (error != 0U)?SOX_EOF : SOX_SUCCESS;
}
const sox_format_handler_t *lsx_au_format_fn();

const sox_format_handler_t *lsx_au_format_fn()
{
  static const char *const names[] = {("au"), ("snd"), ((const char *)((void *)0))};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_ULAW), (8), (0), (SOX_ENCODING_ALAW), (8), (0), (SOX_ENCODING_SIGN2), (8), (16), (24), (32), (0), (SOX_ENCODING_FLOAT), (32), (64), (0), (0)};
  static const sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("PCM file format used widely on Sun systems"), (names), ((64 | 0x0080 | 8)), (startread), (lsx_rawread), ((sox_format_handler_stopread )((void *)0)), (write_header), (lsx_rawwrite), ((sox_format_handler_stopwrite )((void *)0)), (lsx_rawseek), (write_encodings), ((const sox_rate_t *)((void *)0)), ((sizeof(priv_t )))};
  return &handler;
}
