/* Effect: change sample rate     Copyright (c) 2008 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Inspired by, and builds upon some of the ideas presented in:
 * `The Quest For The Perfect Resampler' by Laurent De Soras;
 * http://ldesoras.free.fr/doc/articles/resampler-en.pdf */
#ifdef NDEBUG /* Enable assert always. */
#undef NDEBUG /* Must undef above assert.h or other that might include it. */
#endif
#include "sox_i.h"
#include "fft4g.h"
#include "dft_filter.h"
#include <assert.h>
#include <string.h>
#define  calloc     lsx_calloc
#define  malloc     lsx_malloc
#define  raw_coef_t double
#define  sample_t   double
#define  TO_SOX     SOX_FLOAT_64BIT_TO_SAMPLE
#define  FROM_SOX   SOX_SAMPLE_TO_FLOAT_64BIT
#define  coef(coef_p, interp_order, fir_len, phase_num, coef_interp_num, fir_coef_num) coef_p[(fir_len) * ((interp_order) + 1) * (phase_num) + ((interp_order) + 1) * (fir_coef_num) + (interp_order - coef_interp_num)]

static double *prepare_coefs(const double *coefs,int num_coefs,int num_phases,int interp_order,int multiplier)
{
  int i;
  int j;
  int length = (num_coefs * num_phases);
  double *result = (lsx_realloc(0,((length * (interp_order + 1)) * sizeof(( *result)))));
  double fm1 = coefs[0];
  double f1 = 0;
  double f2 = 0;
  for (i = (num_coefs - 1); i >= 0; --i) 
    for (j = (num_phases - 1); j >= 0; --j) {
/* = 0 to kill compiler warning */
      double f0 = fm1;
      double b = 0;
      double c = 0;
      double d = 0;
      int pos = (((i * num_phases) + j) - 1);
      fm1 = ((((pos > 0)?coefs[pos - 1] : 0)) * multiplier);
      switch(interp_order){
        case 1:
{
          b = (f1 - f0);
          break; 
        }
        case 2:
{
          b = ((f1 - ((.5 * (f2 + f0)) - f1)) - f0);
          c = ((.5 * (f2 + f0)) - f1);
          break; 
        }
        case 3:
{
          c = ((.5 * (f1 + fm1)) - f0);
          d = (1 / 6. * ((((f2 - f1) + fm1) - f0) - (4 * c)));
          b = (((f1 - f0) - d) - c);
          break; 
        }
        default:
{
          if (interp_order != 0) 
            0?((void )0) : __assert_fail("0","rate.c",56,"double *prepare_coefs(const double *, int, int, int, int)");
        }
      }
      #define coef_coef(x) \
        coef(result, interp_order, num_coefs, j, x, num_coefs - 1 - i)
      result[(((num_coefs * (interp_order + 1)) * j) + ((interp_order + 1) * ((num_coefs - 1) - i))) + (interp_order - 0)] = f0;
      if (interp_order > 0) 
        result[(((num_coefs * (interp_order + 1)) * j) + ((interp_order + 1) * ((num_coefs - 1) - i))) + (interp_order - 1)] = b;
      if (interp_order > 1) 
        result[(((num_coefs * (interp_order + 1)) * j) + ((interp_order + 1) * ((num_coefs - 1) - i))) + (interp_order - 2)] = c;
      if (interp_order > 2) 
        result[(((num_coefs * (interp_order + 1)) * j) + ((interp_order + 1) * ((num_coefs - 1) - i))) + (interp_order - 3)] = d;
      #undef coef_coef
      ((f2 = f1) , (f1 = f0));
    }
  return result;
}
/* Data that are shared between channels and filters */
typedef struct __unnamed_class___F0_L70_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__d__Pe___variable_name_unknown_scope_and_name__scope__poly_fir_coefs__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type__Ab_dft_filter_tL128R__typedef_declaration_index_2_Ae__variable_name_unknown_scope_and_name__scope__half_band {
double *poly_fir_coefs;
/* [0]: halve; [1]: down/up: halve/double */
dft_filter_t half_band[2UL];}rate_shared_t;
struct stage ;
typedef void (*stage_fn_t)(struct stage *, fifo_t *);
typedef struct stage {
rate_shared_t *shared;
fifo_t fifo;
/* Number of past samples to store */
int pre;
/* pre + number of future samples to store */
int pre_post;
/* Number of zero samples to pre-load the fifo */
int preload;
/* Which of the 2 half-band filters to use */
int which;
stage_fn_t fn;
/* For poly_fir & spline: */
/* 32bit.32bit fixed point arithmetic */
union __unnamed_class___F0_L86_C3_stage_variable_declaration__variable_type__variable_name_stage__scope__parts__DELIMITER__stage_variable_declaration__variable_type_int64_tl__typedef_declaration_variable_name_stage__scope__all {
    #if defined(WORDS_BIGENDIAN)
    #else
struct __unnamed_class___F0_L90_C5_L129R_variable_declaration__variable_type_uint32_tUi__typedef_declaration_variable_name_L129R__scope__fraction__DELIMITER__L129R_variable_declaration__variable_type_int32_ti__typedef_declaration_variable_name_L129R__scope__integer {
uint32_t fraction;
int32_t integer;}parts;
    #endif
int64_t all;
    #define MULT32 (65536. * 65536.)
}at;
union __unnamed_class___F0_L86_C3_stage_variable_declaration__variable_type__variable_name_stage__scope__parts__DELIMITER__stage_variable_declaration__variable_type_int64_tl__typedef_declaration_variable_name_stage__scope__all step;
/* For step: > 1 for rational; 1 otherwise */
int divisor;
double out_in_ratio;}stage_t;
#define stage_occupancy(s) max(0, fifo_occupancy(&(s)->fifo) - (s)->pre_post)
#define stage_read_p(s) ((sample_t *)fifo_read_ptr(&(s)->fifo) + (s)->pre)

static void cubic_spline(stage_t *p,fifo_t *output_fifo)
{
  int i;
  int num_in = (0 >= (fifo_occupancy(&p -> fifo) - (p -> pre_post)))?0 : (fifo_occupancy(&p -> fifo) - (p -> pre_post));
  int max_num_out = (1 + (num_in * (p -> out_in_ratio)));
  const double *input = (((double *)(fifo_read(&p -> fifo,((int )0),0))) + (p -> pre));
  double *output = (fifo_reserve(output_fifo,max_num_out));
  for (i = 0; p -> at.parts.integer < num_in; (++i , (p -> at.all += p -> step.all))) {
    const double *s = (input + p -> at.parts.integer);
    double x = (p -> at.parts.fraction * (1 / (65536. * 65536.)));
    double b = ((.5 * (s[1] + s[-1])) -  *s);
    double a = (1 / 6. * ((((s[2] - s[1]) + s[-1]) -  *s) - (4 * b)));
    double c = (((s[1] -  *s) - a) - b);
    output[i] = ((((((a * x) + b) * x) + c) * x) +  *s);
  }
  ((max_num_out - i) >= 0)?((void )0) : __assert_fail("max_num_out - i >= 0","rate.c",115,"void cubic_spline(struct stage *, struct <unnamed> *)");
  fifo_trim_by(output_fifo,(max_num_out - i));
  fifo_read(&p -> fifo,p -> at.parts.integer,0);
  p -> at.parts.integer = 0;
}

static void half_sample(stage_t *p,fifo_t *output_fifo)
{
  double *output;
  int i;
  int j;
  int num_in = (0 >= fifo_occupancy(&p -> fifo))?0 : fifo_occupancy(&p -> fifo);
  const rate_shared_t *s = (p -> shared);
  const dft_filter_t *f = ((s -> half_band) + (p -> which));
  const int overlap = ((f -> num_taps) - 1);
  while(num_in >= (f -> dft_length)){
    const double *input = (fifo_read(&p -> fifo,((int )0),0));
    fifo_read(&p -> fifo,((f -> dft_length) - overlap),0);
    num_in -= ((f -> dft_length) - overlap);
    output = (fifo_reserve(output_fifo,(f -> dft_length)));
    fifo_trim_by(output_fifo,(((f -> dft_length) + overlap) >> 1));
    memcpy(output,input,((f -> dft_length) * sizeof(( *output))));
    lsx_safe_rdft((f -> dft_length),1,output);
    output[0] *= (f -> coefs)[0];
    output[1] *= (f -> coefs)[1];
    for (i = 2; i < (f -> dft_length); i += 2) {
      double tmp = output[i];
      output[i] = (((f -> coefs)[i] * tmp) - ((f -> coefs)[i + 1] * output[i + 1]));
      output[i + 1] = (((f -> coefs)[i + 1] * tmp) + ((f -> coefs)[i] * output[i + 1]));
    }
    lsx_safe_rdft((f -> dft_length),-1,output);
    for (((j = 1) , (i = 2)); i < ((f -> dft_length) - overlap); (++j , (i += 2))) 
      output[j] = output[i];
  }
}

static void double_sample(stage_t *p,fifo_t *output_fifo)
{
  double *output;
  int i;
  int j;
  int num_in = (0 >= fifo_occupancy(&p -> fifo))?0 : fifo_occupancy(&p -> fifo);
  const rate_shared_t *s = (p -> shared);
  const dft_filter_t *f = ((s -> half_band) + 1);
  const int overlap = ((f -> num_taps) - 1);
  while(num_in > ((f -> dft_length) >> 1)){
    const double *input = (fifo_read(&p -> fifo,((int )0),0));
    fifo_read(&p -> fifo,(((f -> dft_length) - overlap) >> 1),0);
    num_in -= (((f -> dft_length) - overlap) >> 1);
    output = (fifo_reserve(output_fifo,(f -> dft_length)));
    fifo_trim_by(output_fifo,overlap);
    for (j = (i = 0); i < (f -> dft_length); (++j , (i += 2))) 
      ((output[i] = input[j]) , (output[i + 1] = 0));
    lsx_safe_rdft((f -> dft_length),1,output);
    output[0] *= (f -> coefs)[0];
    output[1] *= (f -> coefs)[1];
    for (i = 2; i < (f -> dft_length); i += 2) {
      double tmp = output[i];
      output[i] = (((f -> coefs)[i] * tmp) - ((f -> coefs)[i + 1] * output[i + 1]));
      output[i + 1] = (((f -> coefs)[i + 1] * tmp) + ((f -> coefs)[i] * output[i + 1]));
    }
    lsx_safe_rdft((f -> dft_length),-1,output);
  }
}

static void half_band_filter_init(rate_shared_t *p,unsigned int which,int num_taps,const double h[],double Fp,double att,int multiplier,double phase,sox_bool allow_aliasing)
{
  dft_filter_t *f = ((p -> half_band) + which);
  int dft_length;
  int i;
  if ((f -> num_taps) != 0) 
    return ;
  if (h != 0) {
    dft_length = lsx_set_dft_length(num_taps);
    f -> coefs = ((((dft_length * sizeof(( *(f -> coefs)))) != 0ULL)?memset(lsx_realloc(0,(dft_length * sizeof(( *(f -> coefs))))),0,(dft_length * sizeof(( *(f -> coefs))))) : ((void *)((void *)0))));
    for (i = 0; i < num_taps; ++i) 
      (f -> coefs)[(((i + dft_length) - num_taps) + 1) & (dft_length - 1)] = (((h[abs(((num_taps / 2) - i))] / dft_length) * 2) * multiplier);
    f -> post_peak = (num_taps / 2);
  }
  else {
    double *h2 = lsx_design_lpf(Fp,1.,2.,allow_aliasing,att,&num_taps,0);
    if (phase != 50) 
      lsx_fir_to_phase(&h2,&num_taps,&f -> post_peak,phase);
    else 
      f -> post_peak = (num_taps / 2);
    dft_length = lsx_set_dft_length(num_taps);
    f -> coefs = ((((dft_length * sizeof(( *(f -> coefs)))) != 0ULL)?memset(lsx_realloc(0,(dft_length * sizeof(( *(f -> coefs))))),0,(dft_length * sizeof(( *(f -> coefs))))) : ((void *)((void *)0))));
    for (i = 0; i < num_taps; ++i) 
      (f -> coefs)[(((i + dft_length) - num_taps) + 1) & (dft_length - 1)] = (((h2[i] / dft_length) * 2) * multiplier);
    free(h2);
  }
  ((num_taps & 1) != 0)?((void )0) : __assert_fail("num_taps & 1","rate.c",214,"void half_band_filter_init(struct <unnamed> *, unsigned int, int, const double *, double, double, int, double, enum sox_bool)");
  f -> num_taps = num_taps;
  f -> dft_length = dft_length;
  ((( *sox_get_globals()).subsystem = "rate.c") , lsx_debug_impl("fir_len=%i dft_length=%i Fp=%g att=%g mult=%i",num_taps,dft_length,Fp,att,multiplier));
  lsx_safe_rdft(dft_length,1,(f -> coefs));
}
#include "rate_filters.h"
typedef struct __unnamed_class___F0_L224_C9_unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__factor__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__samples_in__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__samples_out__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__level__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__input_stage_num__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__output_stage_num__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__upsample__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__stage_tstage__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__stages {
double factor;
uint64_t samples_in;
uint64_t samples_out;
int level;
int input_stage_num;
int output_stage_num;
sox_bool upsample;
stage_t *stages;}rate_t;
#define pre_stage p->stages[-1]
#define last_stage p->stages[p->level]
#define post_stage p->stages[p->level + 1]
typedef enum  {Default=-1,Quick,Low,Medium,High,Very}quality_t;

static void rate_init(rate_t *p,rate_shared_t *shared,double factor,quality_t quality,int interp_order,double phase,double bandwidth,sox_bool allow_aliasing)
{
  int i;
  int mult;
  int divisor = 1;
  (factor > 0)?((void )0) : __assert_fail("factor > 0","rate.c",244,"void rate_init(struct <unnamed> *, struct <unnamed> *, double, enum <unnamed>, int, double, double, enum sox_bool)");
  p -> factor = factor;
  if ((quality < Quick) || (quality > Very)) 
    quality = High;
  if (quality != Quick) {
/* Keep coef table size ~< 500kb */
    const int max_divisor = 2048;
/* Scaled to half this at max_divisor */
    const double epsilon = 4 / (65536. * 65536.);
    p -> upsample = ((p -> factor) < 1);
/* log base 2 */
    for (((i = factor) , (p -> level = 0)); (i >>= 1) != 0; ++p -> level) ;
    factor /= (1 << ((p -> level) + !((p -> upsample) != 0U)));
    for (i = 2; (i <= max_divisor) && (divisor == 1); ++i) {
      double try_d = (factor * i);
      int try = (try_d + .5);
      if (fabs((try - try_d)) < ((try * epsilon) * (1 - ((.5 / max_divisor) * i)))) {
/* Rounded to 1:1? */
        if (try == i) 
          ((((factor = 1) , (divisor = 2))) , (p -> upsample = sox_false));
        else 
          ((factor = try) , (divisor = i));
      }
    }
  }
  p -> stages = (((stage_t *)(((((((size_t )(p -> level)) + 4) * sizeof(( *(p -> stages)))) != 0ULL)?memset(lsx_realloc(0,((((size_t )(p -> level)) + 4) * sizeof(( *(p -> stages))))),0,((((size_t )(p -> level)) + 4) * sizeof(( *(p -> stages))))) : ((void *)((void *)0))))) + 1);
  for (i = -1; i <= ((p -> level) + 1); ++i) 
    (p -> stages)[i].shared = shared;
  (p -> stages)[p -> level].step.all = ((factor * (65536. * 65536.)) + .5);
  (p -> stages)[p -> level].out_in_ratio = ((65536. * 65536. * divisor) / (p -> stages)[p -> level].step.all);
  if (divisor != 1) 
    !((p -> stages)[p -> level].step.parts.fraction != 0U)?((void )0) : __assert_fail("!p->stages[p->level].step.parts.fraction","rate.c",270,"void rate_init(struct <unnamed> *, struct <unnamed> *, double, enum <unnamed>, int, double, double, enum sox_bool)");
  else if (quality != Quick) 
    !((p -> stages)[p -> level].step.parts.integer != 0)?((void )0) : __assert_fail("!p->stages[p->level].step.parts.integer","rate.c",272,"void rate_init(struct <unnamed> *, struct <unnamed> *, double, enum <unnamed>, int, double, double, enum sox_bool)");
  ((( *sox_get_globals()).subsystem = "rate.c") , lsx_debug_impl("i/o=%g; %.9g:%i @ level %i",(p -> factor),factor,divisor,(p -> level)));
/* Compensate for zero-stuffing in double_sample */
  mult = (1 + (p -> upsample));
  p -> input_stage_num = (-(p -> upsample));
  p -> output_stage_num = (p -> level);
  if (quality == Quick) {
    ++p -> output_stage_num;
    (p -> stages)[p -> level].fn = cubic_spline;
    (p -> stages)[p -> level].pre_post = ((3 >= (p -> stages)[p -> level].step.parts.integer)?3 : (p -> stages)[p -> level].step.parts.integer);
    (p -> stages)[p -> level].preload = ((p -> stages)[p -> level].pre = 1);
  }
  else if (((p -> stages)[p -> level].out_in_ratio != 2) || (((p -> upsample) != 0U) && (quality == Low))) {
    const poly_fir_t *f;
    const poly_fir1_t *f1;
    int n = (((4 * (p -> upsample)) + ((((((quality >= Medium)?quality : Medium)) <= Very)?(((quality >= Medium)?quality : Medium)) : Very))) - Medium);
    if (interp_order < 0) 
      interp_order = (quality > High);
    interp_order = ((divisor == 1)?(1 + interp_order) : 0);
    (p -> stages)[p -> level].divisor = divisor;
    p -> output_stage_num += 2;
    if (((p -> upsample) != 0U) && (quality == Low)) 
      ((((((mult = 1) , ++p -> input_stage_num)) , --p -> output_stage_num)) , --n);
    f = (poly_firs + n);
    f1 = ((f -> interp) + interp_order);
    if (!(( *(p -> stages)[p -> level].shared).poly_fir_coefs != 0)) {
      int num_taps = 0;
      int phases = (divisor == 1)?(1 << (f1 -> phase_bits)) : divisor;
      double *coefs = lsx_design_lpf((f -> pass),(f -> stop),1.,sox_false,(f -> att),&num_taps,phases);
      (num_taps == (((f -> num_coefs) * phases) - 1))?((void )0) : __assert_fail("num_taps == f->num_coefs * phases - 1","rate.c",301,"void rate_init(struct <unnamed> *, struct <unnamed> *, double, enum <unnamed>, int, double, double, enum sox_bool)");
      ( *(p -> stages)[p -> level].shared).poly_fir_coefs = prepare_coefs(coefs,(f -> num_coefs),phases,interp_order,mult);
      ((( *sox_get_globals()).subsystem = "rate.c") , lsx_debug_impl("fir_len=%i phases=%i coef_interp=%i mult=%i size=%s",(f -> num_coefs),phases,interp_order,mult,lsx_sigfigs3((((num_taps + 1.) * (interp_order + 1)) * (sizeof(double ))))));
      free(coefs);
    }
    (p -> stages)[p -> level].fn = (f1 -> fn);
    (p -> stages)[p -> level].pre_post = ((f -> num_coefs) - 1);
    (p -> stages)[p -> level].pre = 0;
    (p -> stages)[p -> level].preload = ((p -> stages)[p -> level].pre_post >> 1);
    mult = 1;
  }
  if (quality > Low) {
    typedef struct __unnamed_class___F0_L316_C13_L136R_variable_declaration__variable_type_i_variable_name_L136R__scope__len__DELIMITER__L136R_variable_declaration__variable_type___Pb__Cd__Pe___variable_name_L136R__scope__h__DELIMITER__L136R_variable_declaration__variable_type_d_variable_name_L136R__scope__bw__DELIMITER__L136R_variable_declaration__variable_type_d_variable_name_L136R__scope__a {
    int len;
    const double *h;
    double bw;
    double a;}filter_t;
    static const filter_t filters[] = {{((2 * (sizeof(half_fir_coefs_low) / sizeof(half_fir_coefs_low[0])) - 1)), (half_fir_coefs_low), (0), (0)}, {(0), ((const double *)((void *)0)), (.931), (110)}, {(0), ((const double *)((void *)0)), (.931), (125)}, {(0), ((const double *)((void *)0)), (.931), (170)}};
    const filter_t *f = (filters + (quality - Low));
/* negate att degrade */
    double att = (allow_aliasing != 0U)?(34. / 33 * (f -> a)) : (f -> a);
    double bw = (bandwidth != 0.00000)?(1 - ((1 - (bandwidth / 100)) / (2 / 3. * (.5 + .5869)))) : (f -> bw);
    double min = (1 - ((((allow_aliasing != 0U)?36. / (1 + 2 / 3. * (.5 + .5869)) : 36.)) / 100));
    (((size_t )(quality - Low)) < sizeof(filters) / sizeof(filters[0]))?((void )0) : __assert_fail("(size_t)(quality - Low) < (sizeof(filters)/sizeof(filters[0]))","rate.c",324,"void rate_init(struct <unnamed> *, struct <unnamed> *, double, enum <unnamed>, int, double, double, enum sox_bool)");
    half_band_filter_init(shared,(p -> upsample),(f -> len),(f -> h),bw,att,mult,phase,allow_aliasing);
    if ((p -> upsample) != 0U) {
/* Finish off setting up pre-stage */
      (p -> stages)[-1].fn = double_sample;
      (p -> stages)[-1].preload = ((shared -> half_band)[1].post_peak >> 1);
/* Start setting up post-stage; TODO don't use dft for short filters */
      if (((1 - (p -> factor)) / (1 - bw)) > 2) 
        half_band_filter_init(shared,0,0,0,(((p -> factor) >= min)?(p -> factor) : min),att,1,phase,allow_aliasing);
      else 
        (shared -> half_band)[0] = (shared -> half_band)[1];
    }
    else if (((p -> level) > 0) && ((p -> output_stage_num) > (p -> level))) {
      double pass = (((bw * divisor) / factor) / 2);
      if (((1 - pass) / (1 - bw)) > 2) 
        half_band_filter_init(shared,1,0,0,((pass >= min)?pass : min),att,1,phase,allow_aliasing);
    }
    (p -> stages)[(p -> level) + 1].fn = half_sample;
    (p -> stages)[(p -> level) + 1].preload = (shared -> half_band)[0].post_peak;
  }
  else 
/* dft is slower here, so */
if ((quality == Low) && !((p -> upsample) != 0U)) {
/* use normal convolution */
    (p -> stages)[(p -> level) + 1].fn = half_sample_low;
    (p -> stages)[(p -> level) + 1].pre_post = (2 * (sizeof(half_fir_coefs_low) / sizeof(half_fir_coefs_low[0]) - 1));
    (p -> stages)[(p -> level) + 1].preload = ((p -> stages)[(p -> level) + 1].pre = ((p -> stages)[(p -> level) + 1].pre_post >> 1));
  }
  if ((p -> level) > 0) {
    stage_t *s = ((p -> stages) + ((p -> level) - 1));
    if ((shared -> half_band)[1].num_taps != 0) {
      s -> fn = half_sample;
      s -> preload = (shared -> half_band)[1].post_peak;
      s -> which = 1;
    }
    else 
       *s = (p -> stages)[(p -> level) + 1];
  }
  for (i = (p -> input_stage_num); i <= (p -> output_stage_num); ++i) {
    stage_t *s = ((p -> stages) + i);
    if ((i >= 0) && (i < ((p -> level) - 1))) {
      s -> fn = half_sample_25;
      s -> pre_post = (2 * (sizeof(half_fir_coefs_25) / sizeof(half_fir_coefs_25[0]) - 1));
      s -> preload = (s -> pre = ((s -> pre_post) >> 1));
    }
    fifo_create(&s -> fifo,((int )(sizeof(double ))));
    memset(fifo_reserve(&s -> fifo,(s -> preload)),0,(sizeof(double ) * (s -> preload)));
    if (i < (p -> output_stage_num)) 
      ((( *sox_get_globals()).subsystem = "rate.c") , lsx_debug_impl("stage=%-3ipre_post=%-3ipre=%-3ipreload=%i",i,(s -> pre_post),(s -> pre),(s -> preload)));
  }
}

static void rate_process(rate_t *p)
{
  stage_t *stage = ((p -> stages) + (p -> input_stage_num));
  int i;
  for (i = (p -> input_stage_num); i < (p -> output_stage_num); (++i , ++stage)) 
    ( *(stage -> fn))(stage,&( *(stage + 1)).fifo);
}

static double *rate_input(rate_t *p,const double *samples,size_t n)
{
  p -> samples_in += n;
  return (fifo_write(&(p -> stages)[p -> input_stage_num].fifo,((int )n),samples));
}

static const double *rate_output(rate_t *p,double *samples,size_t *n)
{
  fifo_t *fifo = &(p -> stages)[p -> output_stage_num].fifo;
  p -> samples_out += ( *n = (( *n <= ((size_t )(fifo_occupancy(fifo))))? *n : ((size_t )(fifo_occupancy(fifo)))));
  return (fifo_read(fifo,((int )( *n)),samples));
}

static void rate_flush(rate_t *p)
{
  fifo_t *fifo = &(p -> stages)[p -> output_stage_num].fifo;
  uint64_t samples_out = (((p -> samples_in) / (p -> factor)) + .5);
  size_t remaining = (samples_out > (p -> samples_out))?(samples_out - (p -> samples_out)) : 0;
  double *buff = (1?memset(lsx_realloc(0,(1024 * sizeof(( *buff)))),0,(1024 * sizeof(( *buff)))) : ((void *)((void *)0)));
  if (remaining > 0) {
    while(((size_t )(fifo_occupancy(fifo))) < remaining){
      rate_input(p,buff,((size_t )1024));
      rate_process(p);
    }
    fifo_trim_to(fifo,((int )remaining));
    p -> samples_in = 0;
  }
  free(buff);
}

static void rate_close(rate_t *p)
{
  rate_shared_t *shared = (p -> stages)[0].shared;
  int i;
  for (i = (p -> input_stage_num); i <= (p -> output_stage_num); ++i) 
    fifo_delete(&(p -> stages)[i].fifo);
  free((shared -> half_band)[0].coefs);
  if ((shared -> half_band)[1].coefs != (shared -> half_band)[0].coefs) 
    free((shared -> half_band)[1].coefs);
  free((shared -> poly_fir_coefs));
  memset(shared,0,(sizeof(( *shared))));
  free(((p -> stages) - 1));
}
/*------------------------------- SoX Wrapper --------------------------------*/
typedef struct __unnamed_class___F0_L429_C9_unknown_scope_and_name_variable_declaration__variable_type_sox_rate_td__typedef_declaration_variable_name_unknown_scope_and_name__scope__out_rate__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__quality__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__coef_interp__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__phase__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__bandwidth__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__allow_aliasing__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_rate_tL132R__typedef_declaration_variable_name_unknown_scope_and_name__scope__rate__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_rate_shared_tL133R__typedef_declaration_variable_name_unknown_scope_and_name__scope__shared__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__rate_shared_tL133R__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__shared_ptr {
sox_rate_t out_rate;
int quality;
double coef_interp;
double phase;
double bandwidth;
sox_bool allow_aliasing;
rate_t rate;
rate_shared_t shared;
rate_shared_t *shared_ptr;}priv_t;

static int create(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  int c;
  char *dummy_p;
  char *found_at;
  char *opts = "+i:b:p:MILasqlmhv";
  char *qopts = (opts + 12);
  lsx_getopt_t optstate;
  lsx_getopt_init(argc,argv,opts,0,lsx_getopt_flag_none,1,&optstate);
  p -> quality = -1;
  p -> phase = 50;
  p -> shared_ptr = &p -> shared;
  while((c = lsx_getopt(&optstate)) != -1){
    switch(c){
      case 'i':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < 1)) || (d > 3)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "rate.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","coef_interp",((double )1),((double )3)));
            return lsx_usage(effp);
          }
          p -> coef_interp = d;
          break; 
        }
      }
      case 'p':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < 0)) || (d > 100)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "rate.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","phase",((double )0),((double )100)));
            return lsx_usage(effp);
          }
          p -> phase = d;
          break; 
        }
      }
      case 'b':
{
{
          char *end_ptr;
          double d = strtod(optstate.arg,&end_ptr);
          if ((((end_ptr == optstate.arg) || (d < (100 - floor(36. * (2 / 3. * (.5 + .5869)))))) || (d > 99.7)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "rate.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","bandwidth",(((double )100) - floor(36. * (2 / 3. * (.5 + .5869)))),((double )99.7)));
            return lsx_usage(effp);
          }
          p -> bandwidth = d;
          break; 
        }
      }
      case 'M':
{
        p -> phase = 0;
        break; 
      }
      case 'I':
{
        p -> phase = 25;
        break; 
      }
      case 'L':
{
        p -> phase = 50;
        break; 
      }
      case 's':
{
        p -> bandwidth = 99;
        break; 
      }
      case 'a':
{
        p -> allow_aliasing = sox_true;
        break; 
      }
      default:
{
        if ((found_at = strchr(qopts,c)) != 0) 
          p -> quality = (found_at - qopts);
        else {
          ((( *sox_get_globals()).subsystem = "rate.c") , lsx_fail_impl("unknown option `-%c\'",optstate.opt));
          return lsx_usage(effp);
        }
      }
    }
  }
  ((argc -= optstate.ind) , (argv += optstate.ind));
  if ((((unsigned int )(p -> quality)) < 2) && ((((p -> bandwidth) != 0.00000) || ((p -> phase) != 50)) || ((p -> allow_aliasing) != 0U))) {
    ((( *sox_get_globals()).subsystem = "rate.c") , lsx_fail_impl("override options not allowed with this quality level"));
    return SOX_EOF;
  }
  if ((((p -> bandwidth) != 0.00000) && ((p -> bandwidth) < (100 - floor(36. / (1 + 2 / 3. * (.5 + .5869)) * (2 / 3. * (.5 + .5869)))))) && ((p -> allow_aliasing) != 0U)) {
    ((( *sox_get_globals()).subsystem = "rate.c") , lsx_fail_impl("minimum allowed bandwidth with aliasing is %g%%",(100 - floor(36. / (1 + 2 / 3. * (.5 + .5869)) * (2 / 3. * (.5 + .5869))))));
    return SOX_EOF;
  }
  if (argc != 0) {
    if (((p -> out_rate = lsx_parse_frequency_k(( *argv),&dummy_p,2147483647)) <= 0) || (( *dummy_p) != 0)) 
      return lsx_usage(effp);
    argc--;
    argv++;
    effp -> out_signal.rate = (p -> out_rate);
  }
  return (argc != 0)?lsx_usage(effp) : SOX_SUCCESS;
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  double out_rate = ((p -> out_rate) != 0)?(p -> out_rate) : effp -> out_signal.rate;
  if (effp -> in_signal.rate == out_rate) 
    return 32;
  if (effp -> in_signal.mult != 0) 
/* 1/(2/sinc(pi/3)-1); see De Soras 4.1.2 */
     *effp -> in_signal.mult *= .705;
  effp -> out_signal.channels = effp -> in_signal.channels;
  effp -> out_signal.rate = out_rate;
  rate_init(&p -> rate,(p -> shared_ptr),(effp -> in_signal.rate / out_rate),(p -> quality),(((int )(p -> coef_interp)) - 1),(p -> phase),(p -> bandwidth),(p -> allow_aliasing));
  return SOX_SUCCESS;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t i;
  size_t odone =  *osamp;
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  const double *s = rate_output(&p -> rate,0,&odone);
  for (i = 0; i < odone; ++i) 
     *(obuf++) = ((sox_sample_t )((((sox_macro_temp_sample , (sox_macro_temp_double = ( *(s++) * (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.))))) , ((sox_macro_temp_double < 0)?(((sox_macro_temp_double <= ((sox_sample_t )(1 << 32 - 1)) - .5)?((++effp -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (sox_macro_temp_double - .5))) : (((sox_macro_temp_double >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + .5)?(((sox_macro_temp_double > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)?((++effp -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : (sox_macro_temp_double + .5)))))));
  if (( *isamp != 0UL) && (odone <  *osamp)) {
    double *t = rate_input(&p -> rate,0, *isamp);
    for (i =  *isamp; i != 0UL; --i) 
       *(t++) = (( *(ibuf++)) * (1. / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)));
    rate_process(&p -> rate);
  }
  else 
     *isamp = 0;
   *osamp = odone;
  return SOX_SUCCESS;
}

static int drain(sox_effect_t *effp,sox_sample_t *obuf,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  static size_t isamp = 0;
  rate_flush(&p -> rate);
  return flow(effp,0,obuf,&isamp,osamp);
}

static int stop(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  rate_close(&p -> rate);
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_rate_effect_fn()
{
  static sox_effect_handler_t handler = {("rate"), (0), (2), (create), (start), (flow), (drain), (stop), (0), ((sizeof(priv_t )))};
  static const char *lines[] = {("[-q|-l|-m|-h|-v] [override-options] RATE[k]"), ("                    BAND-"), ("     QUALITY        WIDTH  REJ dB   TYPICAL USE"), (" -q  quick          n/a  ~30 @ Fs/4 playback on ancient hardware"), (" -l  low            80%     100     playback on old hardware"), (" -m  medium         95%     100     audio playback"), (" -h  high (default) 95%     125     16-bit mastering (use with dither)"), (" -v  very high      95%     175     24-bit mastering"), ("              OVERRIDE OPTIONS (only with -m, -h, -v)"), (" -M/-I/-L     Phase response = minimum/intermediate/linear(default)"), (" -s           Steep filter (band-width = 99%)"), (" -a           Allow aliasing above the pass-band"), (" -b 74-99.7   Any band-width %"), (" -p 0-100     Any phase response (0 = minimum, 25 = intermediate,"), ("              50 = linear, 100 = maximum)")};
  static char *usage;
  handler.usage = (lsx_usage_lines(&usage,lines,(sizeof(lines) / sizeof(lines[0]))));
  return (&handler);
}
