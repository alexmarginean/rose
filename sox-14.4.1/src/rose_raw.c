/* libSoX raw I/O
 *
 * Copyright 1991-2007 Lance Norskog And Sundry Contributors
 * This source code is freely redistributable and may be used for
 * any purpose.  This copyright notice must be maintained.
 * Lance Norskog And Sundry Contributors are not responsible for
 * the consequences of using this software.
 */
#include "sox_i.h"
#include "g711.h"
typedef sox_uint16_t sox_uint14_t;
typedef sox_uint16_t sox_uint13_t;
typedef sox_int16_t sox_int14_t;
typedef sox_int16_t sox_int13_t;
#define SOX_ULAW_BYTE_TO_SAMPLE(d,clips)   SOX_SIGNED_16BIT_TO_SAMPLE(sox_ulaw2linear16(d),clips)
#define SOX_ALAW_BYTE_TO_SAMPLE(d,clips)   SOX_SIGNED_16BIT_TO_SAMPLE(sox_alaw2linear16(d),clips)
#define SOX_SAMPLE_TO_ULAW_BYTE(d,c) sox_14linear2ulaw(SOX_SAMPLE_TO_UNSIGNED(14,d,c) - 0x2000)
#define SOX_SAMPLE_TO_ALAW_BYTE(d,c) sox_13linear2alaw(SOX_SAMPLE_TO_UNSIGNED(13,d,c) - 0x1000)

int lsx_rawseek(sox_format_t *ft,uint64_t offset)
{
  return lsx_offset_seek(ft,((off_t )(ft -> data_start)),((off_t )offset));
}
/* Works nicely for starting read and write; lsx_rawstart{read,write}
 * are #defined in sox_i.h */

int lsx_rawstart(sox_format_t *ft,sox_bool default_rate,sox_bool default_channels,sox_bool default_length,sox_encoding_t encoding,unsigned int size)
{
  if ((default_rate != 0U) && (ft -> signal.rate == 0)) {
    ((( *sox_get_globals()).subsystem = "raw.c") , lsx_warn_impl("`%s\': sample rate not specified; trying 8kHz",(ft -> filename)));
    ft -> signal.rate = 8000;
  }
  if ((default_channels != 0U) && (ft -> signal.channels == 0)) {
    ((( *sox_get_globals()).subsystem = "raw.c") , lsx_warn_impl("`%s\': # channels not specified; trying mono",(ft -> filename)));
    ft -> signal.channels = 1;
  }
  if (encoding != SOX_ENCODING_UNKNOWN) {
    if ((((ft -> mode) == 'r') && (ft -> encoding.encoding != SOX_ENCODING_UNKNOWN)) && (ft -> encoding.encoding != encoding)) 
      ((( *sox_get_globals()).subsystem = "raw.c") , lsx_report_impl("`%s\': Format options overriding file-type encoding",(ft -> filename)));
    else 
      ft -> encoding.encoding = encoding;
  }
  if (size != 0) {
    if ((((ft -> mode) == 'r') && (ft -> encoding.bits_per_sample != 0)) && (ft -> encoding.bits_per_sample != size)) 
      ((( *sox_get_globals()).subsystem = "raw.c") , lsx_report_impl("`%s\': Format options overriding file-type sample-size",(ft -> filename)));
    else 
      ft -> encoding.bits_per_sample = size;
  }
  if (((!(ft -> signal.length != 0UL) && ((ft -> mode) == 'r')) && (default_length != 0U)) && (ft -> encoding.bits_per_sample != 0U)) 
    ft -> signal.length = ((lsx_filelength(ft) * 8) / ft -> encoding.bits_per_sample);
  return SOX_SUCCESS;
}
#define READ_SAMPLES_FUNC(type, size, sign, ctype, uctype, cast) \
  static size_t sox_read_ ## sign ## type ## _samples( \
      sox_format_t * ft, sox_sample_t *buf, size_t len) \
  { \
    size_t n, nread; \
    SOX_SAMPLE_LOCALS; \
    ctype *data = lsx_malloc(sizeof(ctype) * len); \
    LSX_USE_VAR(sox_macro_temp_sample), LSX_USE_VAR(sox_macro_temp_double); \
    nread = lsx_read_ ## type ## _buf(ft, (uctype *)data, len); \
    for (n = 0; n < nread; n++) \
      *buf++ = cast(data[n], ft->clips); \
    free(data); \
    return nread; \
  }

static size_t sox_read_ub_samples(sox_format_t *ft,sox_sample_t *buf,size_t len)
{
  size_t n;
  size_t nread;
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  uint8_t *data = (lsx_realloc(0,(sizeof(uint8_t ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  nread = lsx_read_b_buf(ft,((uint8_t *)data),len);
  for (n = 0; n < nread; n++) 
     *(buf++) = ((((sox_sample_t )data[n]) << 32 - 8) ^ 1 << 32 - 1);
  free(data);
  return nread;
}

static size_t sox_read_sb_samples(sox_format_t *ft,sox_sample_t *buf,size_t len)
{
  size_t n;
  size_t nread;
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  int8_t *data = (lsx_realloc(0,(sizeof(int8_t ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  nread = lsx_read_b_buf(ft,((uint8_t *)data),len);
  for (n = 0; n < nread; n++) 
     *(buf++) = (((sox_sample_t )data[n]) << 32 - 8);
  free(data);
  return nread;
}

static size_t sox_read_ulawb_samples(sox_format_t *ft,sox_sample_t *buf,size_t len)
{
  size_t n;
  size_t nread;
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  uint8_t *data = (lsx_realloc(0,(sizeof(uint8_t ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  nread = lsx_read_b_buf(ft,((uint8_t *)data),len);
  for (n = 0; n < nread; n++) 
     *(buf++) = (((sox_sample_t )lsx_ulaw2linear16[data[n]]) << 32 - 16);
  free(data);
  return nread;
}

static size_t sox_read_alawb_samples(sox_format_t *ft,sox_sample_t *buf,size_t len)
{
  size_t n;
  size_t nread;
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  uint8_t *data = (lsx_realloc(0,(sizeof(uint8_t ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  nread = lsx_read_b_buf(ft,((uint8_t *)data),len);
  for (n = 0; n < nread; n++) 
     *(buf++) = (((sox_sample_t )lsx_alaw2linear16[data[n]]) << 32 - 16);
  free(data);
  return nread;
}

static size_t sox_read_uw_samples(sox_format_t *ft,sox_sample_t *buf,size_t len)
{
  size_t n;
  size_t nread;
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  uint16_t *data = (lsx_realloc(0,(sizeof(uint16_t ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  nread = lsx_read_w_buf(ft,((uint16_t *)data),len);
  for (n = 0; n < nread; n++) 
     *(buf++) = ((((sox_sample_t )data[n]) << 32 - 16) ^ 1 << 32 - 1);
  free(data);
  return nread;
}

static size_t sox_read_sw_samples(sox_format_t *ft,sox_sample_t *buf,size_t len)
{
  size_t n;
  size_t nread;
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  int16_t *data = (lsx_realloc(0,(sizeof(int16_t ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  nread = lsx_read_w_buf(ft,((uint16_t *)data),len);
  for (n = 0; n < nread; n++) 
     *(buf++) = (((sox_sample_t )data[n]) << 32 - 16);
  free(data);
  return nread;
}

static size_t sox_read_u3_samples(sox_format_t *ft,sox_sample_t *buf,size_t len)
{
  size_t n;
  size_t nread;
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  sox_uint24_t *data = (lsx_realloc(0,(sizeof(sox_uint24_t ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  nread = lsx_read_3_buf(ft,((sox_uint24_t *)data),len);
  for (n = 0; n < nread; n++) 
     *(buf++) = ((((sox_sample_t )data[n]) << 32 - 24) ^ 1 << 32 - 1);
  free(data);
  return nread;
}

static size_t sox_read_s3_samples(sox_format_t *ft,sox_sample_t *buf,size_t len)
{
  size_t n;
  size_t nread;
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  sox_int24_t *data = (lsx_realloc(0,(sizeof(sox_int24_t ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  nread = lsx_read_3_buf(ft,((sox_uint24_t *)data),len);
  for (n = 0; n < nread; n++) 
     *(buf++) = (((sox_sample_t )data[n]) << 32 - 24);
  free(data);
  return nread;
}

static size_t sox_read_udw_samples(sox_format_t *ft,sox_sample_t *buf,size_t len)
{
  size_t n;
  size_t nread;
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  uint32_t *data = (lsx_realloc(0,(sizeof(uint32_t ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  nread = lsx_read_dw_buf(ft,((uint32_t *)data),len);
  for (n = 0; n < nread; n++) 
     *(buf++) = (((sox_sample_t )data[n]) ^ 1 << 32 - 1);
  free(data);
  return nread;
}

static size_t sox_read_sdw_samples(sox_format_t *ft,sox_sample_t *buf,size_t len)
{
  size_t n;
  size_t nread;
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  int32_t *data = (lsx_realloc(0,(sizeof(int32_t ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  nread = lsx_read_dw_buf(ft,((uint32_t *)data),len);
  for (n = 0; n < nread; n++) 
     *(buf++) = ((sox_sample_t )data[n]);
  free(data);
  return nread;
}

static size_t sox_read_suf_samples(sox_format_t *ft,sox_sample_t *buf,size_t len)
{
  size_t n;
  size_t nread;
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  float *data = (lsx_realloc(0,(sizeof(float ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  nread = lsx_read_f_buf(ft,((float *)data),len);
  for (n = 0; n < nread; n++) 
     *(buf++) = ((sox_sample_t )((((sox_macro_temp_sample , (sox_macro_temp_double = (data[n] * (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.))))) , ((sox_macro_temp_double < ((sox_sample_t )(1 << 32 - 1)))?((++ft -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (((sox_macro_temp_double >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)?(((sox_macro_temp_double > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)?((++ft -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : sox_macro_temp_double))))));
  free(data);
  return nread;
}

static size_t sox_read_sudf_samples(sox_format_t *ft,sox_sample_t *buf,size_t len)
{
  size_t n;
  size_t nread;
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  double *data = (lsx_realloc(0,(sizeof(double ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  nread = lsx_read_df_buf(ft,((double *)data),len);
  for (n = 0; n < nread; n++) 
     *(buf++) = ((sox_sample_t )((((sox_macro_temp_sample , (sox_macro_temp_double = (data[n] * (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.))))) , ((sox_macro_temp_double < 0)?(((sox_macro_temp_double <= ((sox_sample_t )(1 << 32 - 1)) - .5)?((++ft -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (sox_macro_temp_double - .5))) : (((sox_macro_temp_double >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + .5)?(((sox_macro_temp_double > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)?((++ft -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : (sox_macro_temp_double + .5)))))));
  free(data);
  return nread;
}
#define WRITE_SAMPLES_FUNC(type, size, sign, ctype, uctype, cast) \
  static size_t sox_write_ ## sign ## type ## _samples( \
      sox_format_t * ft, sox_sample_t const * buf, size_t len) \
  { \
    SOX_SAMPLE_LOCALS; \
    size_t n, nwritten; \
    ctype *data = lsx_malloc(sizeof(ctype) * len); \
    LSX_USE_VAR(sox_macro_temp_sample), LSX_USE_VAR(sox_macro_temp_double); \
    for (n = 0; n < len; n++) \
      data[n] = cast(buf[n], ft->clips); \
    nwritten = lsx_write_ ## type ## _buf(ft, (uctype *)data, len); \
    free(data); \
    return nwritten; \
  }

static size_t sox_write_ub_samples(sox_format_t *ft,const sox_sample_t *buf,size_t len)
{
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  size_t n;
  size_t nwritten;
  uint8_t *data = (lsx_realloc(0,(sizeof(uint8_t ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  for (n = 0; n < len; n++) 
    data[n] = ((sox_uint8_t )(((sox_int8_t )((((sox_macro_temp_double , (sox_macro_temp_sample = buf[n]))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - (1 << 31 - 8))?((++ft -> clips , ((unsigned int )(-1)) >> 33 - 8)) : (((sox_uint32_t )(sox_macro_temp_sample + (1 << 31 - 8))) >> 32 - 8))))) ^ 1 << 8 - 1));
  nwritten = lsx_write_b_buf(ft,((uint8_t *)data),len);
  free(data);
  return nwritten;
}

static size_t sox_write_sb_samples(sox_format_t *ft,const sox_sample_t *buf,size_t len)
{
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  size_t n;
  size_t nwritten;
  int8_t *data = (lsx_realloc(0,(sizeof(int8_t ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  for (n = 0; n < len; n++) 
    data[n] = ((sox_int8_t )((((sox_macro_temp_double , (sox_macro_temp_sample = buf[n]))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - (1 << 31 - 8))?((++ft -> clips , ((unsigned int )(-1)) >> 33 - 8)) : (((sox_uint32_t )(sox_macro_temp_sample + (1 << 31 - 8))) >> 32 - 8)))));
  nwritten = lsx_write_b_buf(ft,((uint8_t *)data),len);
  free(data);
  return nwritten;
}

static size_t sox_write_ulawb_samples(sox_format_t *ft,const sox_sample_t *buf,size_t len)
{
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  size_t n;
  size_t nwritten;
  uint8_t *data = (lsx_realloc(0,(sizeof(uint8_t ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  for (n = 0; n < len; n++) 
    data[n] = lsx_14linear2ulaw[(((sox_uint14_t )(((sox_int14_t )((((sox_macro_temp_double , (sox_macro_temp_sample = buf[n]))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - (1 << 31 - 14))?((++ft -> clips , ((unsigned int )(-1)) >> 33 - 14)) : (((sox_uint32_t )(sox_macro_temp_sample + (1 << 31 - 14))) >> 32 - 14))))) ^ 1 << 14 - 1)) - 0x2000) + 0x2000];
  nwritten = lsx_write_b_buf(ft,((uint8_t *)data),len);
  free(data);
  return nwritten;
}

static size_t sox_write_alawb_samples(sox_format_t *ft,const sox_sample_t *buf,size_t len)
{
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  size_t n;
  size_t nwritten;
  uint8_t *data = (lsx_realloc(0,(sizeof(uint8_t ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  for (n = 0; n < len; n++) 
    data[n] = lsx_13linear2alaw[(((sox_uint13_t )(((sox_int13_t )((((sox_macro_temp_double , (sox_macro_temp_sample = buf[n]))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - (1 << 31 - 13))?((++ft -> clips , ((unsigned int )(-1)) >> 33 - 13)) : (((sox_uint32_t )(sox_macro_temp_sample + (1 << 31 - 13))) >> 32 - 13))))) ^ 1 << 13 - 1)) - 0x1000) + 0x1000];
  nwritten = lsx_write_b_buf(ft,((uint8_t *)data),len);
  free(data);
  return nwritten;
}

static size_t sox_write_uw_samples(sox_format_t *ft,const sox_sample_t *buf,size_t len)
{
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  size_t n;
  size_t nwritten;
  uint16_t *data = (lsx_realloc(0,(sizeof(uint16_t ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  for (n = 0; n < len; n++) 
    data[n] = ((sox_uint16_t )(((sox_int16_t )((((sox_macro_temp_double , (sox_macro_temp_sample = buf[n]))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - (1 << 31 - 16))?((++ft -> clips , ((unsigned int )(-1)) >> 33 - 16)) : (((sox_uint32_t )(sox_macro_temp_sample + (1 << 31 - 16))) >> 32 - 16))))) ^ 1 << 16 - 1));
  nwritten = lsx_write_w_buf(ft,((uint16_t *)data),len);
  free(data);
  return nwritten;
}

static size_t sox_write_sw_samples(sox_format_t *ft,const sox_sample_t *buf,size_t len)
{
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  size_t n;
  size_t nwritten;
  int16_t *data = (lsx_realloc(0,(sizeof(int16_t ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  for (n = 0; n < len; n++) 
    data[n] = ((sox_int16_t )((((sox_macro_temp_double , (sox_macro_temp_sample = buf[n]))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - (1 << 31 - 16))?((++ft -> clips , ((unsigned int )(-1)) >> 33 - 16)) : (((sox_uint32_t )(sox_macro_temp_sample + (1 << 31 - 16))) >> 32 - 16)))));
  nwritten = lsx_write_w_buf(ft,((uint16_t *)data),len);
  free(data);
  return nwritten;
}

static size_t sox_write_u3_samples(sox_format_t *ft,const sox_sample_t *buf,size_t len)
{
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  size_t n;
  size_t nwritten;
  sox_uint24_t *data = (lsx_realloc(0,(sizeof(sox_uint24_t ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  for (n = 0; n < len; n++) 
    data[n] = ((sox_uint24_t )(((sox_int24_t )((((sox_macro_temp_double , (sox_macro_temp_sample = buf[n]))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - (1 << 31 - 24))?((++ft -> clips , ((unsigned int )(-1)) >> 33 - 24)) : (((sox_uint32_t )(sox_macro_temp_sample + (1 << 31 - 24))) >> 32 - 24))))) ^ 1 << 24 - 1));
  nwritten = lsx_write_3_buf(ft,((sox_uint24_t *)data),len);
  free(data);
  return nwritten;
}

static size_t sox_write_s3_samples(sox_format_t *ft,const sox_sample_t *buf,size_t len)
{
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  size_t n;
  size_t nwritten;
  sox_int24_t *data = (lsx_realloc(0,(sizeof(sox_int24_t ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  for (n = 0; n < len; n++) 
    data[n] = ((sox_int24_t )((((sox_macro_temp_double , (sox_macro_temp_sample = buf[n]))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - (1 << 31 - 24))?((++ft -> clips , ((unsigned int )(-1)) >> 33 - 24)) : (((sox_uint32_t )(sox_macro_temp_sample + (1 << 31 - 24))) >> 32 - 24)))));
  nwritten = lsx_write_3_buf(ft,((sox_uint24_t *)data),len);
  free(data);
  return nwritten;
}

static size_t sox_write_udw_samples(sox_format_t *ft,const sox_sample_t *buf,size_t len)
{
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  size_t n;
  size_t nwritten;
  uint32_t *data = (lsx_realloc(0,(sizeof(uint32_t ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  for (n = 0; n < len; n++) 
    data[n] = ((sox_uint32_t )(buf[n] ^ 1 << 32 - 1));
  nwritten = lsx_write_dw_buf(ft,((uint32_t *)data),len);
  free(data);
  return nwritten;
}

static size_t sox_write_sdw_samples(sox_format_t *ft,const sox_sample_t *buf,size_t len)
{
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  size_t n;
  size_t nwritten;
  int32_t *data = (lsx_realloc(0,(sizeof(int32_t ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  for (n = 0; n < len; n++) 
    data[n] = ((sox_int32_t )buf[n]);
  nwritten = lsx_write_dw_buf(ft,((uint32_t *)data),len);
  free(data);
  return nwritten;
}

static size_t sox_write_suf_samples(sox_format_t *ft,const sox_sample_t *buf,size_t len)
{
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  size_t n;
  size_t nwritten;
  float *data = (lsx_realloc(0,(sizeof(float ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  for (n = 0; n < len; n++) 
    data[n] = ((((sox_macro_temp_double , (sox_macro_temp_sample = buf[n]))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - 128)?((++ft -> clips , 1)) : (((sox_macro_temp_sample + 128) & ~255) * (1. / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.))))));
  nwritten = lsx_write_f_buf(ft,((float *)data),len);
  free(data);
  return nwritten;
}

static size_t sox_write_sudf_samples(sox_format_t *ft,const sox_sample_t *buf,size_t len)
{
  sox_sample_t sox_macro_temp_sample;
  double sox_macro_temp_double;
  size_t n;
  size_t nwritten;
  double *data = (lsx_realloc(0,(sizeof(double ) * len)));
  (sox_macro_temp_sample , ((void )sox_macro_temp_double));
  for (n = 0; n < len; n++) 
    data[n] = (buf[n] * (1. / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)));
  nwritten = lsx_write_df_buf(ft,((double *)data),len);
  free(data);
  return nwritten;
}
#define GET_FORMAT(type) \
static ft_##type##_fn * type##_fn(sox_format_t * ft) { \
  switch (ft->encoding.bits_per_sample) { \
    case 8: \
      switch (ft->encoding.encoding) { \
        case SOX_ENCODING_SIGN2: return sox_##type##_sb_samples; \
        case SOX_ENCODING_UNSIGNED: return sox_##type##_ub_samples; \
        case SOX_ENCODING_ULAW: return sox_##type##_ulawb_samples; \
        case SOX_ENCODING_ALAW: return sox_##type##_alawb_samples; \
        default: break; } \
      break; \
    case 16: \
      switch (ft->encoding.encoding) { \
        case SOX_ENCODING_SIGN2: return sox_##type##_sw_samples; \
        case SOX_ENCODING_UNSIGNED: return sox_##type##_uw_samples; \
        default: break; } \
      break; \
    case 24: \
      switch (ft->encoding.encoding) { \
        case SOX_ENCODING_SIGN2:    return sox_##type##_s3_samples; \
        case SOX_ENCODING_UNSIGNED: return sox_##type##_u3_samples; \
        default: break; } \
      break; \
    case 32: \
      switch (ft->encoding.encoding) { \
        case SOX_ENCODING_SIGN2: return sox_##type##_sdw_samples; \
        case SOX_ENCODING_UNSIGNED: return sox_##type##_udw_samples; \
        case SOX_ENCODING_FLOAT: return sox_##type##_suf_samples; \
        default: break; } \
      break; \
    case 64: \
      switch (ft->encoding.encoding) { \
        case SOX_ENCODING_FLOAT: return sox_##type##_sudf_samples; \
        default: break; } \
      break; \
    default: \
      lsx_fail_errno(ft, SOX_EFMT, "this handler does not support this data size"); \
      return NULL; } \
  lsx_fail_errno(ft, SOX_EFMT, "this encoding is not supported for this data size"); \
  return NULL; }
typedef size_t ft_read_fn(sox_format_t *, sox_sample_t *, size_t );

static ft_read_fn *read_fn(sox_format_t *ft)
{
  switch(ft -> encoding.bits_per_sample){
    case 8:
{
      switch(ft -> encoding.encoding){
        case SOX_ENCODING_SIGN2:
{
          return sox_read_sb_samples;
        }
        case SOX_ENCODING_UNSIGNED:
{
          return sox_read_ub_samples;
        }
        case SOX_ENCODING_ULAW:
{
          return sox_read_ulawb_samples;
        }
        case SOX_ENCODING_ALAW:
{
          return sox_read_alawb_samples;
        }
        default:
{
          break; 
        }
      }
      break; 
    }
    case 16:
{
      switch(ft -> encoding.encoding){
        case SOX_ENCODING_SIGN2:
{
          return sox_read_sw_samples;
        }
        case SOX_ENCODING_UNSIGNED:
{
          return sox_read_uw_samples;
        }
        default:
{
          break; 
        }
      }
      break; 
    }
    case 24:
{
      switch(ft -> encoding.encoding){
        case SOX_ENCODING_SIGN2:
{
          return sox_read_s3_samples;
        }
        case SOX_ENCODING_UNSIGNED:
{
          return sox_read_u3_samples;
        }
        default:
{
          break; 
        }
      }
      break; 
    }
    case 32:
{
      switch(ft -> encoding.encoding){
        case SOX_ENCODING_SIGN2:
{
          return sox_read_sdw_samples;
        }
        case SOX_ENCODING_UNSIGNED:
{
          return sox_read_udw_samples;
        }
        case SOX_ENCODING_FLOAT:
{
          return sox_read_suf_samples;
        }
        default:
{
          break; 
        }
      }
      break; 
    }
    case 64:
{
      switch(ft -> encoding.encoding){
        case SOX_ENCODING_FLOAT:
{
          return sox_read_sudf_samples;
        }
        default:
{
          break; 
        }
      }
      break; 
    }
    default:
{
      lsx_fail_errno(ft,SOX_EFMT,"this handler does not support this data size");
      return 0;
    }
  }
  lsx_fail_errno(ft,SOX_EFMT,"this encoding is not supported for this data size");
  return 0;
}
/* Read a stream of some type into SoX's internal buffer format. */

size_t lsx_rawread(sox_format_t *ft,sox_sample_t *buf,size_t nsamp)
{
  ft_read_fn *read_buf = read_fn(ft);
  if ((read_buf != 0) && (nsamp != 0UL)) 
    return ( *read_buf)(ft,buf,nsamp);
  return 0;
}
typedef size_t ft_write_fn(sox_format_t *, const sox_sample_t *, size_t );

static ft_write_fn *write_fn(sox_format_t *ft)
{
  switch(ft -> encoding.bits_per_sample){
    case 8:
{
      switch(ft -> encoding.encoding){
        case SOX_ENCODING_SIGN2:
{
          return sox_write_sb_samples;
        }
        case SOX_ENCODING_UNSIGNED:
{
          return sox_write_ub_samples;
        }
        case SOX_ENCODING_ULAW:
{
          return sox_write_ulawb_samples;
        }
        case SOX_ENCODING_ALAW:
{
          return sox_write_alawb_samples;
        }
        default:
{
          break; 
        }
      }
      break; 
    }
    case 16:
{
      switch(ft -> encoding.encoding){
        case SOX_ENCODING_SIGN2:
{
          return sox_write_sw_samples;
        }
        case SOX_ENCODING_UNSIGNED:
{
          return sox_write_uw_samples;
        }
        default:
{
          break; 
        }
      }
      break; 
    }
    case 24:
{
      switch(ft -> encoding.encoding){
        case SOX_ENCODING_SIGN2:
{
          return sox_write_s3_samples;
        }
        case SOX_ENCODING_UNSIGNED:
{
          return sox_write_u3_samples;
        }
        default:
{
          break; 
        }
      }
      break; 
    }
    case 32:
{
      switch(ft -> encoding.encoding){
        case SOX_ENCODING_SIGN2:
{
          return sox_write_sdw_samples;
        }
        case SOX_ENCODING_UNSIGNED:
{
          return sox_write_udw_samples;
        }
        case SOX_ENCODING_FLOAT:
{
          return sox_write_suf_samples;
        }
        default:
{
          break; 
        }
      }
      break; 
    }
    case 64:
{
      switch(ft -> encoding.encoding){
        case SOX_ENCODING_FLOAT:
{
          return sox_write_sudf_samples;
        }
        default:
{
          break; 
        }
      }
      break; 
    }
    default:
{
      lsx_fail_errno(ft,SOX_EFMT,"this handler does not support this data size");
      return 0;
    }
  }
  lsx_fail_errno(ft,SOX_EFMT,"this encoding is not supported for this data size");
  return 0;
}
/* Writes SoX's internal buffer format to buffer of various data types. */

size_t lsx_rawwrite(sox_format_t *ft,const sox_sample_t *buf,size_t nsamp)
{
  ft_write_fn *write_buf = write_fn(ft);
  if ((write_buf != 0) && (nsamp != 0UL)) 
    return ( *write_buf)(ft,buf,nsamp);
  return 0;
}
