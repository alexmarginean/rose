/* libSoX effect: Overdrive            (c) 2008 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
typedef struct __unnamed_class___F0_L20_C9_unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__gain__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__colour__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__last_in__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__last_out__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__b0__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__b1__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__a1 {
double gain;
double colour;
double last_in;
double last_out;
double b0;
double b1;
double a1;}priv_t;

static int create(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  (--argc , ++argv);
  p -> gain = (p -> colour = 20);
{
    do {{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 0) || (d > 100)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "overdrive.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","gain",((double )0),((double )100)));
            return lsx_usage(effp);
          }
          p -> gain = d;
          (--argc , ++argv);
        }
      }
{
        char *end_ptr;
        double d;
        if (argc == 0) 
          break; 
        d = strtod(( *argv),&end_ptr);
        if (end_ptr !=  *argv) {
          if (((d < 0) || (d > 100)) || (( *end_ptr) != 0)) {
            ((( *sox_get_globals()).subsystem = "overdrive.c") , lsx_fail_impl("parameter `%s\' must be between %g and %g","colour",((double )0),((double )100)));
            return lsx_usage(effp);
          }
          p -> colour = d;
          (--argc , ++argv);
        }
      }
    }while (0);
  }
  p -> gain = exp((((p -> gain) * 2.30258509299404568402) * 0.05));
  p -> colour /= 200;
  return (argc != 0)?lsx_usage(effp) : SOX_SUCCESS;
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  if ((p -> gain) == 1) 
    return 32;
  return SOX_SUCCESS;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t dummy = 0;
  size_t len = ( *isamp = ( *osamp = (( *isamp <=  *osamp)? *isamp :  *osamp)));
  while(len-- != 0UL){
    sox_sample_t sox_macro_temp_sample;
    double sox_macro_temp_double;
    double d = (( *(ibuf++)) * (1. / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)));
    double d0 = d;
    d *= (p -> gain);
    d += (p -> colour);
    d = ((d < (-1))?-2. / 3 : (((d > 1)?2. / 3 : (d - (((d * d) * d) * (1. / 3))))));
    p -> last_out = ((d - (p -> last_in)) + (.995 * (p -> last_out)));
    p -> last_in = d;
     *(obuf++) = ((sox_sample_t )((((sox_macro_temp_sample , (sox_macro_temp_double = (((d0 * .5) + ((p -> last_out) * .75)) * (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.))))) , ((sox_macro_temp_double < 0)?(((sox_macro_temp_double <= ((sox_sample_t )(1 << 32 - 1)) - .5)?((++dummy , ((sox_sample_t )(1 << 32 - 1)))) : (sox_macro_temp_double - .5))) : (((sox_macro_temp_double >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + .5)?(((sox_macro_temp_double > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.)?((++dummy , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : (sox_macro_temp_double + .5)))))));
  }
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_overdrive_effect_fn()
{
  static sox_effect_handler_t handler = {("overdrive"), ("[gain [colour]]"), (128), (create), (start), (flow), ((sox_effect_handler_drain )((void *)0)), ((sox_effect_handler_stop )((void *)0)), ((sox_effect_handler_kill )((void *)0)), ((sizeof(priv_t )))};
  return (&handler);
}
