/* libSoX dcshift.c
 * (c) 2000.04.15 Chris Ausbrooks <weed@bucket.pp.ualr.edu>
 *
 * based on vol.c which is
 * (c) 20/03/2000 Fabien COELHO <fabien@coelho.net> for sox.
 *
 * DC shift a sound file, with basic linear amplitude formula.
 * Beware of saturations! clipping is checked and reported.
 * Cannot handle different number of channels.
 * Cannot handle rate change.
 */
#include "sox_i.h"
typedef struct __unnamed_class___F0_L15_C9_unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__dcshift__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__uselimiter__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__limiterthreshhold__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_d_variable_name_unknown_scope_and_name__scope__limitergain__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__limited__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__totalprocessed {
/* DC shift. */
double dcshift;
/* boolean: are we using the limiter? */
int uselimiter;
double limiterthreshhold;
/* limiter gain. */
double limitergain;
/* number of limited values to report. */
uint64_t limited;
uint64_t totalprocessed;}priv_t;
/*
 * Process options: dcshift (double) type (amplitude, power, dB)
 */

static int sox_dcshift_getopts(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *dcs = (priv_t *)(effp -> priv);
/* default is no change */
  dcs -> dcshift = 1.0;
/* default is no limiter */
  dcs -> uselimiter = 0;
  (--argc , ++argv);
  if (argc < 1) 
    return lsx_usage(effp);
  if ((argc != 0) && !(sscanf(argv[0],"%lf",&dcs -> dcshift) != 0)) 
    return lsx_usage(effp);
  if (argc > 1) {
    if (!(sscanf(argv[1],"%lf",&dcs -> limitergain) != 0)) 
      return lsx_usage(effp);
/* ok, we'll use it */
    dcs -> uselimiter = 1;
/* The following equation is derived so that there is no
         * discontinuity in output amplitudes */
/* and a SOX_SAMPLE_MAX input always maps to a SOX_SAMPLE_MAX output
         * when the limiter is activated. */
/* (NOTE: There **WILL** be a discontinuity in the slope of the
         * output amplitudes when using the limiter.) */
    dcs -> limiterthreshhold = (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) * (1.0 - (fabs((dcs -> dcshift)) - (dcs -> limitergain))));
  }
  return SOX_SUCCESS;
}
/*
 * Start processing
 */

static int sox_dcshift_start(sox_effect_t *effp)
{
  priv_t *dcs = (priv_t *)(effp -> priv);
  if ((dcs -> dcshift) == 0) 
    return 32;
  dcs -> limited = 0;
  dcs -> totalprocessed = 0;
  return SOX_SUCCESS;
}
/*
 * Process data.
 */

static int sox_dcshift_flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *dcs = (priv_t *)(effp -> priv);
  double dcshift = (dcs -> dcshift);
  double limitergain = (dcs -> limitergain);
  double limiterthreshhold = (dcs -> limiterthreshhold);
  double sample;
  size_t len;
  len = (( *osamp <=  *isamp)? *osamp :  *isamp);
/* report back dealt with amount. */
   *isamp = len;
   *osamp = len;
  if ((dcs -> uselimiter) != 0) {
    dcs -> totalprocessed += len;
    for (; len > 0; len--) {
      sample = ( *(ibuf++));
      if ((sample > limiterthreshhold) && (dcshift > 0)) {
        sample = (((((sample - limiterthreshhold) * limitergain) / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - limiterthreshhold)) + limiterthreshhold) + dcshift);
        dcs -> limited++;
      }
      else if ((sample < -limiterthreshhold) && (dcshift < 0)) {
/* Note this should really be SOX_SAMPLE_MIN but
                         * the clip() below will take care of the overflow.
                         */
        sample = (((((sample + limiterthreshhold) * limitergain) / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - limiterthreshhold)) - limiterthreshhold) + dcshift);
        dcs -> limited++;
      }
      else {
/* Note this should consider SOX_SAMPLE_MIN but
                         * the clip() below will take care of the overflow.
                         */
        sample = ((dcshift * ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32))) + sample);
      }
      do {
        if (sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32))) {
          sample = ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32));
          effp -> clips++;
        }
        else if (sample < ((sox_sample_t )(1 << 32 - 1))) {
          sample = ((sox_sample_t )(1 << 32 - 1));
          effp -> clips++;
        }
      }while (0);
       *(obuf++) = sample;
    }
  }
  else 
/* quite basic, with clipping */
    for (; len > 0; --len) {
      double d = ((dcshift * (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.0)) + ( *(ibuf++)));
       *(obuf++) = (((d < 0)?(((d <= ((sox_sample_t )(1 << 32 - 1)) - 0.5)?((++effp -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (d - 0.5))) : (((d >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 0.5)?((++effp -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : (d + 0.5)))));
    }
  return SOX_SUCCESS;
}
/*
 * Do anything required when you stop reading samples.
 * Don't close input file!
 */

static int sox_dcshift_stop(sox_effect_t *effp)
{
  priv_t *dcs = (priv_t *)(effp -> priv);
  if ((dcs -> limited) != 0UL) {
    ((( *sox_get_globals()).subsystem = "dcshift.c") , lsx_warn_impl("DCSHIFT limited %lu values (%d percent).",(dcs -> limited),((int )(((dcs -> limited) * 100.0) / (dcs -> totalprocessed)))));
  }
  return SOX_SUCCESS;
}
static sox_effect_handler_t sox_dcshift_effect = {("dcshift"), ("shift [ limitergain ]\n\tThe peak limiter has a gain much less than 1.0 (ie 0.05 or 0.02) which\n\tis only used on peaks to prevent clipping. (default is no limiter)"), ((16 | 128)), (sox_dcshift_getopts), (sox_dcshift_start), (sox_dcshift_flow), ((sox_effect_handler_drain )((void *)0)), (sox_dcshift_stop), ((sox_effect_handler_kill )((void *)0)), ((sizeof(priv_t )))};

const sox_effect_handler_t *lsx_dcshift_effect_fn()
{
  return (&sox_dcshift_effect);
}
