/* June 1, 1992
 * Copyright 1992 Guido van Rossum And Sundry Contributors
 * This source code is freely redistributable and may be used for
 * any purpose.  This copyright notice must be maintained.
 * Guido van Rossum And Sundry Contributors are not responsible for
 * the consequences of using this software.
 */
/*
 * "reverse" effect, uses a temporary file created by lsx_tmpfile().
 */
#include "sox_i.h"
#include <string.h>
typedef struct __unnamed_class___F0_L16_C9_unknown_scope_and_name_variable_declaration__variable_type_L65R_variable_name_unknown_scope_and_name__scope__pos__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__FILE_IO_FILE__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__tmp_file {
off_t pos;
FILE *tmp_file;}priv_t;

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  p -> pos = 0;
  p -> tmp_file = lsx_tmpfile();
  if ((p -> tmp_file) == ((FILE *)((void *)0))) {
    ((( *sox_get_globals()).subsystem = "reverse.c") , lsx_fail_impl("can\'t create temporary file: %s",strerror( *__errno_location())));
    return SOX_EOF;
  }
  return SOX_SUCCESS;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  if (fwrite(ibuf,(sizeof(( *ibuf))), *isamp,(p -> tmp_file)) !=  *isamp) {
    ((( *sox_get_globals()).subsystem = "reverse.c") , lsx_fail_impl("error writing temporary file: %s",strerror( *__errno_location())));
    return SOX_EOF;
  }
/* samples not output until drain */
  (obuf , ( *osamp = 0));
  return SOX_SUCCESS;
}

static int drain(sox_effect_t *effp,sox_sample_t *obuf,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  int i;
  int j;
  if ((p -> pos) == 0) {
    fflush((p -> tmp_file));
    p -> pos = ftello((p -> tmp_file));
    if (((p -> pos) % sizeof(sox_sample_t )) != 0) {
      ((( *sox_get_globals()).subsystem = "reverse.c") , lsx_fail_impl("temporary file has incorrect size"));
      return SOX_EOF;
    }
    p -> pos /= sizeof(sox_sample_t );
  }
  p -> pos -= ( *osamp = (((((off_t )( *osamp)) <= (p -> pos))?((off_t )( *osamp)) : (p -> pos))));
  fseeko((p -> tmp_file),((off_t )((p -> pos) * sizeof(sox_sample_t ))),0);
  if (fread(obuf,(sizeof(sox_sample_t )), *osamp,(p -> tmp_file)) !=  *osamp) {
    ((( *sox_get_globals()).subsystem = "reverse.c") , lsx_fail_impl("error reading temporary file: %s",strerror( *__errno_location())));
    return SOX_EOF;
  }
/* reverse the samples */
  for (((i = 0) , (j = ( *osamp - 1))); i < j; (++i , --j)) {
    sox_sample_t temp = obuf[i];
    obuf[i] = obuf[j];
    obuf[j] = temp;
  }
  return ((p -> pos) != 0L)?SOX_SUCCESS : SOX_EOF;
}

static int stop(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
/* auto-deleted by lsx_tmpfile */
  fclose((p -> tmp_file));
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_reverse_effect_fn()
{
  static sox_effect_handler_t handler = {("reverse"), ((const char *)((void *)0)), (256), ((sox_effect_handler_getopts )((void *)0)), (start), (flow), (drain), (stop), ((sox_effect_handler_kill )((void *)0)), ((sizeof(priv_t )))};
  return (&handler);
}
