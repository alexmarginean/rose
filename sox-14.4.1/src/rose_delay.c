/* libSoX effect: Delay one or more channels (c) 2008 robs@users.sourceforge.net
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#include <string.h>
typedef struct __unnamed_class___F0_L21_C9_unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__argc__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb____Pb__c__Pe____Pe___variable_name_unknown_scope_and_name__scope__argv__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__max_arg__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__delay__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__pre_pad__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_uint64_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__pad__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__buffer_size__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_size_tUl__typedef_declaration_variable_name_unknown_scope_and_name__scope__buffer_index__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__L5R__Pe___variable_name_unknown_scope_and_name__scope__buffer__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_sox_boolsox_bool__typedef_declaration_variable_name_unknown_scope_and_name__scope__drain_started {
size_t argc;
char **argv;
char *max_arg;
uint64_t delay;
uint64_t pre_pad;
uint64_t pad;
size_t buffer_size;
size_t buffer_index;
sox_sample_t *buffer;
sox_bool drain_started;}priv_t;

static int lsx_kill(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  unsigned int i;
  for (i = 0; i < (p -> argc); ++i) 
    free((p -> argv)[i]);
  free((p -> argv));
  return SOX_SUCCESS;
}

static int create(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *p = (priv_t *)(effp -> priv);
  uint64_t delay;
  uint64_t max_samples = 0;
  unsigned int i;
  (--argc , ++argv);
  p -> argc = argc;
  p -> argv = (((((p -> argc) * sizeof(( *(p -> argv)))) != 0ULL)?memset(lsx_realloc(0,((p -> argc) * sizeof(( *(p -> argv))))),0,((p -> argc) * sizeof(( *(p -> argv))))) : ((void *)((void *)0))));
  for (i = 0; i < (p -> argc); ++i) {
    const char *next = lsx_parsesamples(1e5,((p -> argv)[i] = ((argv[i] != 0)?strcpy(((char *)(lsx_realloc(0,(strlen(argv[i]) + 1)))),argv[i]) : ((char *)((void *)0)))),&delay,'t');
    if (!(next != 0) || (( *next) != 0)) {
      lsx_kill(effp);
      return lsx_usage(effp);
    }
    if (delay > max_samples) {
      max_samples = delay;
      p -> max_arg = (p -> argv)[i];
    }
  }
  return SOX_SUCCESS;
}

static int stop(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  free((p -> buffer));
  return SOX_SUCCESS;
}

static int start(sox_effect_t *effp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  uint64_t max_delay;
  uint64_t temp;
  if (!((p -> max_arg) != 0)) 
    return 32;
  if ((p -> argc) > effp -> in_signal.channels) {
    ((( *sox_get_globals()).subsystem = "delay.c") , lsx_fail_impl("too few input channels"));
    return SOX_EOF;
  }
  if ((effp -> flow) < (p -> argc)) {
    lsx_parsesamples(effp -> in_signal.rate,(p -> argv)[effp -> flow],&temp,'t');
    p -> buffer_size = temp;
  }
  lsx_parsesamples(effp -> in_signal.rate,(p -> max_arg),&max_delay,'t');
  if ((effp -> flow) == 0) {
    effp -> out_signal.length = ((effp -> in_signal.length != ((sox_uint64_t )(-1)))?(effp -> in_signal.length + (max_delay * effp -> in_signal.channels)) : ((sox_uint64_t )(-1)));
    ((( *sox_get_globals()).subsystem = "delay.c") , lsx_debug_impl("extending audio by %lu samples",max_delay));
  }
  p -> buffer_index = (p -> delay = (p -> pre_pad = 0));
  p -> pad = (max_delay - (p -> buffer_size));
  p -> buffer = (lsx_realloc(0,((p -> buffer_size) * sizeof(( *(p -> buffer))))));
  p -> drain_started = sox_false;
  return SOX_SUCCESS;
}

static int flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t len = ( *isamp = ( *osamp = (( *isamp <=  *osamp)? *isamp :  *osamp)));
  if (!((p -> buffer_size) != 0UL)) 
    memcpy(obuf,ibuf,(len * sizeof(( *obuf))));
  else 
    for (; len != 0UL; --len) {
      if ((p -> delay) < (p -> buffer_size)) {
        (p -> buffer)[p -> delay++] =  *(ibuf++);
         *(obuf++) = 0;
      }
      else {
         *(obuf++) = (p -> buffer)[p -> buffer_index];
        (p -> buffer)[p -> buffer_index++] =  *(ibuf++);
        p -> buffer_index %= (p -> buffer_size);
      }
    }
  return SOX_SUCCESS;
}

static int drain(sox_effect_t *effp,sox_sample_t *obuf,size_t *osamp)
{
  priv_t *p = (priv_t *)(effp -> priv);
  size_t len;
  if (!((p -> drain_started) != 0U)) {
    p -> drain_started = sox_true;
    p -> pre_pad = ((p -> buffer_size) - (p -> delay));
/* If the input was too short to fill the buffer completely,
         flow() has not yet output enough silence to reach the
         desired delay. */
  }
  len = ( *osamp = (((((p -> pre_pad) + (p -> delay)) + (p -> pad)) <=  *osamp)?(((p -> pre_pad) + (p -> delay)) + (p -> pad)) :  *osamp));
  for (; ((p -> pre_pad) != 0UL) && (len != 0UL); (--p -> pre_pad , --len)) 
     *(obuf++) = 0;
  for (; ((p -> delay) != 0UL) && (len != 0UL); (--p -> delay , --len)) {
     *(obuf++) = (p -> buffer)[p -> buffer_index++];
    p -> buffer_index %= (p -> buffer_size);
  }
  for (; ((p -> pad) != 0UL) && (len != 0UL); (--p -> pad , --len)) 
     *(obuf++) = 0;
  return SOX_SUCCESS;
}

const sox_effect_handler_t *lsx_delay_effect_fn()
{
  static sox_effect_handler_t handler = {("delay"), ("{length}"), ((8 | 256)), (create), (start), (flow), (drain), (stop), (lsx_kill), ((sizeof(priv_t )))};
  return (&handler);
}
