/* libSoX MAUD file format handler, by Lutz Vieweg 1993
 *
 * supports: mono and stereo, linear, a-law and u-law reading and writing
 *
 * an IFF format; description at http://lclevy.free.fr/amiga/MAUDINFO.TXT
 *
 * Copyright 1998-2006 Chris Bagwell and SoX Contributors
 * This source code is freely redistributable and may be used for
 * any purpose.  This copyright notice must be maintained.
 * Lance Norskog And Sundry Contributors are not responsible for
 * the consequences of using this software.
 */
#include "sox_i.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
/* Private data for MAUD file */
typedef struct __unnamed_class___F0_L21_C9_unknown_scope_and_name_variable_declaration__variable_type_uint32_tUi__typedef_declaration_variable_name_unknown_scope_and_name__scope__nsamples {
uint32_t nsamples;}priv_t;
static void maudwriteheader(sox_format_t *);
/*
 * Do anything required before you start reading samples.
 * Read file header.
 *      Find out sampling rate,
 *      size and encoding of samples,
 *      mono/stereo/quad.
 */

static int startread(sox_format_t *ft)
{
  priv_t *p = (priv_t *)(ft -> priv);
  char buf[12UL];
  char *chunk_buf;
  unsigned short bitpersam;
  uint32_t nom;
  unsigned short denom;
  unsigned short chaninf;
  uint32_t chunksize;
  uint32_t trash32;
  uint16_t trash16;
  int rc;
/* Needed for rawread() */
  rc = lsx_rawstart(ft,sox_false,sox_false,sox_false,SOX_ENCODING_UNKNOWN,0);
  if (rc != 0) 
    return rc;
/* read FORM chunk */
  if ((lsx_reads(ft,buf,((size_t )4)) == SOX_EOF) || (strncmp(buf,"FORM",((size_t )4)) != 0)) {
    lsx_fail_errno(ft,SOX_EHDR,"MAUD: header does not begin with magic word `FORM\'");
    return SOX_EOF;
  }
/* totalsize */
  lsx_readdw(ft,&trash32);
  if ((lsx_reads(ft,buf,((size_t )4)) == SOX_EOF) || (strncmp(buf,"MAUD",((size_t )4)) != 0)) {
    lsx_fail_errno(ft,SOX_EHDR,"MAUD: `FORM\' chunk does not specify `MAUD\' as type");
    return SOX_EOF;
  }
/* read chunks until 'BODY' (or end) */
  while((lsx_reads(ft,buf,((size_t )4)) == SOX_SUCCESS) && (strncmp(buf,"MDAT",((size_t )4)) != 0)){
/*
                buf[4] = 0;
                lsx_debug("chunk %s",buf);
                */
    if (strncmp(buf,"MHDR",((size_t )4)) == 0) {
      lsx_readdw(ft,&chunksize);
      if (chunksize != (8 * 4)) {
        lsx_fail_errno(ft,SOX_EHDR,"MAUD: MHDR chunk has bad size");
        return SOX_EOF;
      }
/* fseeko(ft->fp,12,SEEK_CUR); */
/* number of samples stored in MDAT */
      lsx_readdw(ft,&p -> nsamples);
/* number of bits per sample as stored in MDAT */
      lsx_readw(ft,&bitpersam);
/* number of bits per sample after decompression */
      lsx_readw(ft,&trash16);
/* clock source frequency */
      lsx_readdw(ft,&nom);
/* clock devide           */
      lsx_readw(ft,&denom);
      if (denom == 0) {
        lsx_fail_errno(ft,SOX_EHDR,"MAUD: frequency denominator == 0, failed");
        return SOX_EOF;
      }
      ft -> signal.rate = (nom / denom);
/* channel information */
      lsx_readw(ft,&chaninf);
      switch(chaninf){
        case 0:
{
          ft -> signal.channels = 1;
          break; 
        }
        case 1:
{
          ft -> signal.channels = 2;
          break; 
        }
        default:
{
          lsx_fail_errno(ft,SOX_EFMT,"MAUD: unsupported number of channels in file");
          return SOX_EOF;
        }
      }
/* number of channels (mono: 1, stereo: 2, ...) */
      lsx_readw(ft,&chaninf);
      if (chaninf != ft -> signal.channels) {
        lsx_fail_errno(ft,SOX_EFMT,"MAUD: unsupported number of channels in file");
        return SOX_EOF;
      }
/* compression type */
      lsx_readw(ft,&chaninf);
/* rest of chunk, unused yet */
      lsx_readdw(ft,&trash32);
      lsx_readdw(ft,&trash32);
      lsx_readdw(ft,&trash32);
      if ((bitpersam == 8) && (chaninf == 0)) {
        ft -> encoding.bits_per_sample = 8;
        ft -> encoding.encoding = SOX_ENCODING_UNSIGNED;
      }
      else if ((bitpersam == 8) && (chaninf == 2)) {
        ft -> encoding.bits_per_sample = 8;
        ft -> encoding.encoding = SOX_ENCODING_ALAW;
      }
      else if ((bitpersam == 8) && (chaninf == 3)) {
        ft -> encoding.bits_per_sample = 8;
        ft -> encoding.encoding = SOX_ENCODING_ULAW;
      }
      else if ((bitpersam == 16) && (chaninf == 0)) {
        ft -> encoding.bits_per_sample = 16;
        ft -> encoding.encoding = SOX_ENCODING_SIGN2;
      }
      else {
        lsx_fail_errno(ft,SOX_EFMT,"MAUD: unsupported compression type detected");
        return SOX_EOF;
      }
      continue; 
    }
    if (strncmp(buf,"ANNO",((size_t )4)) == 0) {
      lsx_readdw(ft,&chunksize);
      if ((chunksize & 1) != 0U) 
        chunksize++;
      chunk_buf = (lsx_realloc(0,(chunksize + ((size_t )1))));
      if (lsx_readbuf(ft,chunk_buf,((size_t )chunksize)) != chunksize) {
        lsx_fail_errno(ft,SOX_EOF,"MAUD: Unexpected EOF in ANNO header");
        return SOX_EOF;
      }
      chunk_buf[chunksize] = 0;
      ((( *sox_get_globals()).subsystem = "maud.c") , lsx_debug_impl("%s",chunk_buf));
      free(chunk_buf);
      continue; 
    }
/* some other kind of chunk */
    lsx_readdw(ft,&chunksize);
    if ((chunksize & 1) != 0U) 
      chunksize++;
    lsx_seeki(ft,((off_t )chunksize),1);
    continue; 
  }
  if (strncmp(buf,"MDAT",((size_t )4)) != 0) {
    lsx_fail_errno(ft,SOX_EFMT,"MAUD: MDAT chunk not found");
    return SOX_EOF;
  }
  lsx_readdw(ft,&p -> nsamples);
  return SOX_SUCCESS;
}

static int startwrite(sox_format_t *ft)
{
  priv_t *p = (priv_t *)(ft -> priv);
  int rc;
/* Needed for rawwrite() */
  rc = lsx_rawstart(ft,sox_false,sox_false,sox_false,SOX_ENCODING_UNKNOWN,0);
  if (rc != 0) 
    return rc;
/* If you have to seek around the output file */
  if (!((ft -> seekable) != 0U)) {
    lsx_fail_errno(ft,SOX_EOF,"Output .maud file must be a file, not a pipe");
    return SOX_EOF;
  }
  p -> nsamples = 0x7f000000;
  maudwriteheader(ft);
  p -> nsamples = 0;
  return SOX_SUCCESS;
}

static size_t write_samples(sox_format_t *ft,const sox_sample_t *buf,size_t len)
{
  priv_t *p = (priv_t *)(ft -> priv);
  p -> nsamples += len;
  return lsx_rawwrite(ft,buf,len);
}

static int stopwrite(sox_format_t *ft)
{
/* All samples are already written out. */
  priv_t *p = (priv_t *)(ft -> priv);
/* MDAT chunk size */
  uint32_t mdat_size;
  mdat_size = ((p -> nsamples) * (ft -> encoding.bits_per_sample >> 3));
  lsx_padbytes(ft,((size_t )(mdat_size % 2)));
  if (lsx_seeki(ft,((off_t )0),0) != 0) {
    lsx_fail_errno(ft, *__errno_location(),"can\'t rewind output file to rewrite MAUD header");
    return SOX_EOF;
  }
  maudwriteheader(ft);
  return SOX_SUCCESS;
}
#define MAUDHEADERSIZE (4+(4+4+32)+(4+4+19+1)+(4+4))

static void maudwriteheader(sox_format_t *ft)
{
  priv_t *p = (priv_t *)(ft -> priv);
/* MDAT chunk size */
  uint32_t mdat_size;
  mdat_size = ((p -> nsamples) * (ft -> encoding.bits_per_sample >> 3));
  lsx_writes(ft,"FORM");
/* size of file */
  lsx_writedw(ft,(((4 + (4 + 4 + 32) + (4 + 4 + 19 + 1) + (4 + 4)) + mdat_size) + (mdat_size % 2)));
/* File type */
  lsx_writes(ft,"MAUD");
  lsx_writes(ft,"MHDR");
/* number of bytes to follow */
  lsx_writedw(ft,(8 * 4));
/* number of samples stored in MDAT */
  lsx_writedw(ft,(p -> nsamples));
  switch(ft -> encoding.encoding){
    case SOX_ENCODING_UNSIGNED:
{
/* number of bits per sample as stored in MDAT */
      lsx_writew(ft,8);
/* number of bits per sample after decompression */
      lsx_writew(ft,8);
      break; 
    }
    case SOX_ENCODING_SIGN2:
{
/* number of bits per sample as stored in MDAT */
      lsx_writew(ft,16);
/* number of bits per sample after decompression */
      lsx_writew(ft,16);
      break; 
    }
    case SOX_ENCODING_ULAW:
{
    }
    case SOX_ENCODING_ALAW:
{
/* number of bits per sample as stored in MDAT */
      lsx_writew(ft,8);
/* number of bits per sample after decompression */
      lsx_writew(ft,16);
      break; 
    }
    default:
{
      break; 
    }
  }
/* sample rate, Hz */
  lsx_writedw(ft,((unsigned int )(ft -> signal.rate + .5)));
/* clock devide */
  lsx_writew(ft,((int )1));
  if (ft -> signal.channels == 1) {
/* channel information */
    lsx_writew(ft,0);
/* number of channels (mono: 1, stereo: 2, ...) */
    lsx_writew(ft,1);
  }
  else {
    lsx_writew(ft,1);
    lsx_writew(ft,2);
  }
  switch(ft -> encoding.encoding){
    case SOX_ENCODING_SIGN2:
{
    }
    case SOX_ENCODING_UNSIGNED:
{
/* no compression */
      lsx_writew(ft,0);
      break; 
    }
    case SOX_ENCODING_ULAW:
{
      lsx_writew(ft,3);
      break; 
    }
    case SOX_ENCODING_ALAW:
{
      lsx_writew(ft,2);
      break; 
    }
    default:
{
      break; 
    }
  }
/* reserved */
  lsx_writedw(ft,0);
/* reserved */
  lsx_writedw(ft,0);
/* reserved */
  lsx_writedw(ft,0);
  lsx_writes(ft,"ANNO");
/* length of block */
  lsx_writedw(ft,19);
  lsx_writes(ft,"file created by SoX");
  lsx_padbytes(ft,((size_t )1));
  lsx_writes(ft,"MDAT");
/* samples in file */
  lsx_writedw(ft,((p -> nsamples) * (ft -> encoding.bits_per_sample >> 3)));
}
const sox_format_handler_t *lsx_maud_format_fn();

const sox_format_handler_t *lsx_maud_format_fn()
{
  static const char *const names[] = {("maud"), ((const char *)((void *)0))};
  static const unsigned int write_encodings[] = {(SOX_ENCODING_SIGN2), (16), (0), (SOX_ENCODING_UNSIGNED), (8), (0), (SOX_ENCODING_ULAW), (8), (0), (SOX_ENCODING_ALAW), (8), (0), (0)};
  static const sox_format_handler_t handler = {(((14 << 16) + (4 << 8) + 1)), ("Used with the \342\200\230Toccata\342\200\231 sound-card on the Amiga"), (names), ((0x0040 | 0x0080 | 256 | 0x0200)), (startread), (lsx_rawread), ((sox_format_handler_stopread )((void *)0)), (startwrite), (write_samples), (stopwrite), ((sox_format_handler_seek )((void *)0)), (write_encodings), ((const sox_rate_t *)((void *)0)), ((sizeof(priv_t )))};
  return &handler;
}
