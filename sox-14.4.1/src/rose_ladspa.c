/* LADSPA effect support for sox
 * (c) Reuben Thomas <rrt@sc3d.org> 2007
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include "sox_i.h"
#ifdef HAVE_LADSPA_H
#include <assert.h>
#include <limits.h>
#include <string.h>
#include <ltdl.h>
#include "ladspa.h"
extern sox_effect_handler_t sox_ladspa_effect;
/* Private data for resampling */
typedef struct __unnamed_class___F0_L32_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__name__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L114R_variable_name_unknown_scope_and_name__scope__lth__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__CL124R__Pe___variable_name_unknown_scope_and_name__scope__desc__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_L123R_variable_name_unknown_scope_and_name__scope__handle__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__LADSPA_Dataf__typedef_declaration__Pe___variable_name_unknown_scope_and_name__scope__control__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ul_variable_name_unknown_scope_and_name__scope__input_port__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_Ul_variable_name_unknown_scope_and_name__scope__output_port {
/* plugin name */
char *name;
/* dynamic object handle */
lt_dlhandle lth;
/* plugin descriptor */
const LADSPA_Descriptor *desc;
/* instantiated plugin handle */
LADSPA_Handle handle;
/* control ports */
LADSPA_Data *control;
unsigned long input_port;
unsigned long output_port;}priv_t;

static LADSPA_Data ladspa_default(const LADSPA_PortRangeHint *p)
{
  LADSPA_Data d;
  if (((p -> HintDescriptor) & 0x3C0) == 0x200) 
    d = 0.0;
  else if (((p -> HintDescriptor) & 0x3C0) == 0x240) 
    d = 1.0;
  else if (((p -> HintDescriptor) & 0x3C0) == 0x280) 
    d = 100.0;
  else if (((p -> HintDescriptor) & 0x3C0) == 0x2C0) 
    d = 440.0;
  else if (((p -> HintDescriptor) & 0x3C0) == 0x40) 
    d = (p -> LowerBound);
  else if (((p -> HintDescriptor) & 0x3C0) == 0x140) 
    d = (p -> UpperBound);
  else if (((p -> HintDescriptor) & 0x3C0) == 0x80) {
    if (((p -> HintDescriptor) & 16) != 0) 
      d = (exp(((log((p -> LowerBound)) * 0.75) + (log((p -> UpperBound)) * 0.25))));
    else 
      d = (((p -> LowerBound) * 0.75) + ((p -> UpperBound) * 0.25));
  }
  else if (((p -> HintDescriptor) & 0x3C0) == 0xC0) {
    if (((p -> HintDescriptor) & 16) != 0) 
      d = (exp(((log((p -> LowerBound)) * 0.5) + (log((p -> UpperBound)) * 0.5))));
    else 
      d = (((p -> LowerBound) * 0.5) + ((p -> UpperBound) * 0.5));
  }
  else if (((p -> HintDescriptor) & 0x3C0) == 256) {
    if (((p -> HintDescriptor) & 16) != 0) 
      d = (exp(((log((p -> LowerBound)) * 0.25) + (log((p -> UpperBound)) * 0.75))));
    else 
      d = (((p -> LowerBound) * 0.25) + ((p -> UpperBound) * 0.75));
/* shouldn't happen */
  }
  else {
/* FIXME: Deal with this at a higher level */
    ((( *sox_get_globals()).subsystem = "ladspa.c") , lsx_fail_impl("non-existent default value; using 0.1"));
/* Should at least avoid divide by 0 */
    d = 0.1;
  }
  return d;
}
/*
 * Process options
 */

static int sox_ladspa_getopts(sox_effect_t *effp,int argc,char **argv)
{
  priv_t *l_st = (priv_t *)(effp -> priv);
  char *path;
  union __unnamed_class___F0_L88_C3_L132R__L133R__scope____SgSS2___variable_declaration__variable_type_L125R_variable_name_L132R__L133R__scope____SgSS2____scope__fn__DELIMITER__L132R__L133R__scope____SgSS2___variable_declaration__variable_type___Pb__v__Pe___variable_name_L132R__L133R__scope____SgSS2____scope__ptr {
  LADSPA_Descriptor_Function fn;
  void *ptr;}ltptr;
  unsigned long index = 0;
  unsigned long i;
  double arg;
  (--argc , ++argv);
  l_st -> input_port = 9223372036854775807L * 2UL + 1UL;
  l_st -> output_port = 9223372036854775807L * 2UL + 1UL;
/* Get module name */
  if (argc >= 1) {
    l_st -> name = argv[0];
    argc--;
    argv++;
  }
/* Load module */
  path = getenv("LADSPA_PATH");
  if (path == ((char *)((void *)0))) 
    path = "/usr/local/lib/ladspa";
  if (((lt_dlinit() != 0) || (lt_dlsetsearchpath(path) != 0)) || ((l_st -> lth = lt_dlopenext((l_st -> name))) == ((struct lt__handle *)((void *)0)))) {
    ((( *sox_get_globals()).subsystem = "ladspa.c") , lsx_fail_impl("could not open LADSPA plugin %s",(l_st -> name)));
    return SOX_EOF;
  }
/* Get descriptor function */
  if ((ltptr.ptr = lt_dlsym((l_st -> lth),"ladspa_descriptor")) == ((void *)((void *)0))) {
    ((( *sox_get_globals()).subsystem = "ladspa.c") , lsx_fail_impl("could not find ladspa_descriptor"));
    return SOX_EOF;
  }
/* If no plugins in this module, complain */
  if (( *ltptr.fn)(0UL) == ((const LADSPA_Descriptor *)((void *)0))) {
    ((( *sox_get_globals()).subsystem = "ladspa.c") , lsx_fail_impl("no plugins found"));
    return SOX_EOF;
  }
/* Get first plugin descriptor */
  l_st -> desc = ( *ltptr.fn)(0UL);
/* We already know this will work */
  ((l_st -> desc) != 0)?((void )0) : __assert_fail("l_st->desc","ladspa.c",127,"int sox_ladspa_getopts(struct sox_effect_t *, int, char **)");
/* If more than one plugin, or first argument is not a number, try
     to use first argument as plugin label. */
  if ((argc > 0) && ((( *ltptr.fn)(1UL) != ((const LADSPA_Descriptor *)((void *)0))) || !(sscanf(argv[0],"%lf",&arg) != 0))) {
    while(((l_st -> desc) != 0) && (strcmp(( *(l_st -> desc)).Label,argv[0]) != 0))
      l_st -> desc = ( *ltptr.fn)(++index);
    if ((l_st -> desc) == ((const LADSPA_Descriptor *)((void *)0))) {
      ((( *sox_get_globals()).subsystem = "ladspa.c") , lsx_fail_impl("no plugin called `%s\' found",argv[0]));
      return SOX_EOF;
    }
    else 
      argc--;
    argv++;
  }
/* Scan the ports to check there's one input and one output */
  l_st -> control = ((((( *(l_st -> desc)).PortCount * sizeof(LADSPA_Data )) != 0ULL)?memset(lsx_realloc(0,(( *(l_st -> desc)).PortCount * sizeof(LADSPA_Data ))),0,(( *(l_st -> desc)).PortCount * sizeof(LADSPA_Data ))) : ((void *)((void *)0))));
  for (i = 0; i < ( *(l_st -> desc)).PortCount; i++) {
    const LADSPA_PortDescriptor port = ( *(l_st -> desc)).PortDescriptors[i];
/* Check port is well specified. All control ports should be
       inputs, but don't bother checking, as we never rely on this. */
    if (((port & 1) != 0) && ((port & 2) != 0)) {
      ((( *sox_get_globals()).subsystem = "ladspa.c") , lsx_fail_impl("port %lu is both input and output",i));
      return SOX_EOF;
    }
    else if (((port & 4) != 0) && ((port & 8) != 0)) {
      ((( *sox_get_globals()).subsystem = "ladspa.c") , lsx_fail_impl("port %lu is both audio and control",i));
      return SOX_EOF;
    }
    if ((port & 8) != 0) {
      if ((port & 1) != 0) {
        if ((l_st -> input_port) != 9223372036854775807L * 2UL + 1UL) {
          ((( *sox_get_globals()).subsystem = "ladspa.c") , lsx_fail_impl("can\'t use a plugin with more than one audio input port"));
          return SOX_EOF;
        }
        l_st -> input_port = i;
      }
      else if ((port & 2) != 0) {
        if ((l_st -> output_port) != 9223372036854775807L * 2UL + 1UL) {
          ((( *sox_get_globals()).subsystem = "ladspa.c") , lsx_fail_impl("can\'t use a plugin with more than one audio output port"));
          return SOX_EOF;
        }
        l_st -> output_port = i;
      }
/* Control port */
    }
    else {
      if (argc == 0) {
        if (!((( *(l_st -> desc)).PortRangeHints[i].HintDescriptor & 0x3C0) != 0)) {
          ((( *sox_get_globals()).subsystem = "ladspa.c") , lsx_fail_impl("not enough arguments for control ports"));
          return SOX_EOF;
        }
        (l_st -> control)[i] = ladspa_default((( *(l_st -> desc)).PortRangeHints + i));
        ((( *sox_get_globals()).subsystem = "ladspa.c") , lsx_debug_impl("default argument for port %lu is %f",i,(l_st -> control)[i]));
      }
      else {
        if (!(sscanf(argv[0],"%lf",&arg) != 0)) 
          return lsx_usage(effp);
        (l_st -> control)[i] = ((LADSPA_Data )arg);
        ((( *sox_get_globals()).subsystem = "ladspa.c") , lsx_debug_impl("argument for port %lu is %f",i,(l_st -> control)[i]));
        argc--;
        argv++;
      }
    }
  }
/* Stop if we have any unused arguments */
  return (argc != 0)?lsx_usage(effp) : SOX_SUCCESS;
}
/*
 * Prepare processing.
 */

static int sox_ladspa_start(sox_effect_t *effp)
{
  priv_t *l_st = (priv_t *)(effp -> priv);
  unsigned long i;
/* Instantiate the plugin */
  ((( *sox_get_globals()).subsystem = "ladspa.c") , lsx_debug_impl("rate for plugin is %g",effp -> in_signal.rate));
  l_st -> handle = ( *( *(l_st -> desc)).instantiate)((l_st -> desc),((unsigned long )effp -> in_signal.rate));
  if ((l_st -> handle) == ((void *)((void *)0))) {
    ((( *sox_get_globals()).subsystem = "ladspa.c") , lsx_fail_impl("could not instantiate plugin"));
    return SOX_EOF;
  }
  for (i = 0; i < ( *(l_st -> desc)).PortCount; i++) {
    const LADSPA_PortDescriptor port = ( *(l_st -> desc)).PortDescriptors[i];
    if ((port & 4) != 0) 
      ( *( *(l_st -> desc)).connect_port)((l_st -> handle),i,((l_st -> control) + i));
  }
/* If needed, activate the plugin */
  if (( *(l_st -> desc)).activate != 0) 
    ( *( *(l_st -> desc)).activate)((l_st -> handle));
  return SOX_SUCCESS;
}
/*
 * Process one bufferful of data.
 */

static int sox_ladspa_flow(sox_effect_t *effp,const sox_sample_t *ibuf,sox_sample_t *obuf,size_t *isamp,size_t *osamp)
{
  priv_t *l_st = (priv_t *)(effp -> priv);
  size_t i;
  size_t len = ( *isamp <=  *osamp)? *isamp :  *osamp;
   *osamp = ( *isamp = len);
  if (len != 0UL) {
    LADSPA_Data *buf = (lsx_realloc(0,(sizeof(LADSPA_Data ) * len)));
    sox_sample_t sox_macro_temp_sample;
    double sox_macro_temp_double;
/* Insert input if effect takes it */
    if ((l_st -> input_port) != 9223372036854775807L * 2UL + 1UL) {
/* Copy the input; FIXME: Assume LADSPA_Data == float! */
      for (i = 0; i < len; i++) 
        buf[i] = ((((sox_macro_temp_double , (sox_macro_temp_sample = ibuf[i]))) , ((sox_macro_temp_sample > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) - 0x80)?((++effp -> clips , 1)) : (((sox_macro_temp_sample + 0x80) & ~255) * (1.0 / (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.0))))));
/* Connect the input port */
      ( *( *(l_st -> desc)).connect_port)((l_st -> handle),(l_st -> input_port),buf);
    }
/* Connect the output port if used */
    if ((l_st -> output_port) != 9223372036854775807L * 2UL + 1UL) 
      ( *( *(l_st -> desc)).connect_port)((l_st -> handle),(l_st -> output_port),buf);
/* Run the plugin */
    ( *( *(l_st -> desc)).run)((l_st -> handle),len);
/* Grab output if effect produces it */
    if ((l_st -> output_port) != 9223372036854775807L * 2UL + 1UL) 
/* FIXME: Assume LADSPA_Data == float! */
      for (i = 0; i < len; i++) {
        obuf[i] = ((sox_sample_t )((((sox_macro_temp_sample , (sox_macro_temp_double = (buf[i] * (((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.0))))) , ((sox_macro_temp_double < ((sox_sample_t )(1 << 32 - 1)))?((++effp -> clips , ((sox_sample_t )(1 << 32 - 1)))) : (((sox_macro_temp_double >= ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.0)?(((sox_macro_temp_double > ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)) + 1.0)?((++effp -> clips , ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : ((sox_sample_t )(((unsigned int )(-1)) >> 33 - 32)))) : sox_macro_temp_double))))));
      }
    free(buf);
  }
  return SOX_SUCCESS;
}
/*
 * Nothing to do.
 */

static int sox_ladspa_drain(sox_effect_t *effp,sox_sample_t *obuf,size_t *osamp)
{
   *osamp = 0;
  return SOX_SUCCESS;
}
/*
 * Do anything required when you stop reading samples.
 * Don't close input file!
 */

static int sox_ladspa_stop(sox_effect_t *effp)
{
  priv_t *l_st = (priv_t *)(effp -> priv);
/* If needed, deactivate the plugin */
  if (( *(l_st -> desc)).deactivate != 0) 
    ( *( *(l_st -> desc)).deactivate)((l_st -> handle));
/* If needed, cleanup memory used by the plugin */
  if (( *(l_st -> desc)).cleanup != 0) 
    ( *( *(l_st -> desc)).cleanup)((l_st -> handle));
  return SOX_SUCCESS;
}

static int sox_ladspa_kill(sox_effect_t *effp)
{
  priv_t *l_st = (priv_t *)(effp -> priv);
  free((l_st -> control));
  return SOX_SUCCESS;
}
static sox_effect_handler_t sox_ladspa_effect = {("ladspa"), ("MODULE [PLUGIN] [ARGUMENT...]"), (0x80), (sox_ladspa_getopts), (sox_ladspa_start), (sox_ladspa_flow), (sox_ladspa_drain), (sox_ladspa_stop), (sox_ladspa_kill), ((sizeof(priv_t )))};

const sox_effect_handler_t *lsx_ladspa_effect_fn()
{
  return (&sox_ladspa_effect);
}
#endif /* HAVE_LADSPA */
