/* This file is part of GNU cflow
   Copyright (C) 1997, 2005, 2006, 2007, 2009, 2010, 2011 Sergey Poznyakoff
 
   GNU cflow is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
 
   GNU cflow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public
   License along with GNU cflow; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301 USA */
#include <cflow.h>
#include <parser.h>
typedef struct __unnamed_class___F0_L22_C9_unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__name__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__type_end__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__parmcnt__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__line__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_storage_variable_name_unknown_scope_and_name__scope__storage {
char *name;
int type_end;
int parmcnt;
int line;
enum storage storage;}Ident;
void parse_declaration(Ident *,int );
void parse_variable_declaration(Ident *,int );
void parse_function_declaration(Ident *,int );
void parse_dcl(Ident *,int maybe_knr);
void parse_knr_dcl(Ident *);
void parse_typedef();
void expression();
void initializer_list();
void func_body();
void declare(Ident *,int maybe_knr);
void declare_type(Ident *);
int dcl(Ident *);
int parmdcl(Ident *);
int dirdcl(Ident *);
void skip_struct();
Symbol *get_symbol(char *name);
void maybe_parm_list(int *parm_cnt_return);
void call(char *,int );
void reference(char *,int );
/* Current nesting level */
int level;
/* Current caller */
Symbol *caller;
/* Obstack for composing declaration line */
struct obstack text_stk;
/* Parameter declaration nesting level */
int parm_level;
typedef struct __unnamed_class___F0_L57_C9_unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__type__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type___Pb__c__Pe___variable_name_unknown_scope_and_name__scope__token__DELIMITER__unknown_scope_and_name_variable_declaration__variable_type_i_variable_name_unknown_scope_and_name__scope__line {
int type;
char *token;
int line;}TOKSTK;
typedef int Stackpos[1UL];
TOKSTK tok;
TOKSTK *token_stack;
int tos;
int curs;
int token_stack_length = 64;
int token_stack_increase = 32;
static int need_space;
void mark(Stackpos );
void restore(Stackpos );
void tokpush(int ,int ,char *);
void save_token(TOKSTK *);

static void print_token(TOKSTK *tokptr)
{
  switch(tokptr -> type){
    case 257:
{
    }
    case 260:
{
    }
    case 264:
{
    }
    case 265:
{
    }
    case 266:
{
    }
    case 270:
{
    }
    case 272:
{
    }
    case 273:
{
      fprintf(stderr,"`%s\'",(tokptr -> token));
      break; 
    }
    case '{':
{
    }
    case 258:
{
      fprintf(stderr,"`{\'");
      break; 
    }
    case '}':
{
    }
    case 259:
{
      fprintf(stderr,"`}\'");
      break; 
    }
    case 261:
{
      fprintf(stderr,"`extern\'");
      break; 
    }
    case 262:
{
      fprintf(stderr,"`static\'");
      break; 
    }
    case 263:
{
      fprintf(stderr,"`typedef\'");
      break; 
    }
    case 271:
{
      fprintf(stderr,"\"%s\"",(tokptr -> token));
      break; 
    }
    default:
{
      fprintf(stderr,"`%c\'",(tokptr -> type));
    }
  }
}

static void file_error(char *msg,TOKSTK *tokptr)
{
  fprintf(stderr,"%s:%d: %s",filename,tok.line,msg);
  if (tokptr != 0) {
    fprintf(stderr,(gettext(" near ")));
    print_token(tokptr);
  }
  fprintf(stderr,"\n");
}

void mark(Stackpos pos)
{
  pos[0] = curs;
}

void restore(Stackpos pos)
{
  curs = pos[0];
  if (curs != 0) 
    tok = token_stack[curs - 1];
}

void tokpush(int type,int line,char *token)
{
  token_stack[tos].type = type;
  token_stack[tos].token = token;
  token_stack[tos].line = line;
  if (++tos == token_stack_length) {
    token_stack_length += token_stack_increase;
    token_stack = (xrealloc(token_stack,(token_stack_length * sizeof(( *token_stack)))));
  }
}

void cleanup_stack()
{
  int delta = (tos - curs);
  if (delta != 0) 
    memmove(token_stack,(token_stack + curs),(delta * sizeof(token_stack[0])));
  tos = delta;
  curs = 0;
}

void clearstack()
{
  tos = (curs = 0);
}

int nexttoken()
{
  int type;
  if (curs == tos) {
    type = get_token();
    tokpush(type,line_num,yylval.str);
  }
  tok = token_stack[curs];
  curs++;
  return tok.type;
}

int putback()
{
  if (curs == 0) 
    error(10,0,(gettext("INTERNAL ERROR: cannot return token to stream")));
  curs--;
  if (curs > 0) {
    tok.type = token_stack[curs - 1].type;
    tok.token = token_stack[curs - 1].token;
  }
  else 
    tok.type = 0;
  return tok.type;
}

void init_parse()
{
  _obstack_begin(&text_stk,0,0,((void *(*)(long ))xmalloc),((void (*)(void *))free));
  token_stack = (xmalloc((token_stack_length * sizeof(( *token_stack)))));
  clearstack();
}

void save_token(TOKSTK *tokptr)
{
  int len;
  switch(tokptr -> type){
    case 257:
{
    }
    case 260:
{
    }
    case 264:
{
    }
    case 270:
{
    }
    case 272:
{
    }
    case 273:
{
      if (need_space != 0) 
        (
{
          struct obstack *__o = &text_stk;
          if (((__o -> next_free) + 1) > (__o -> chunk_limit)) 
            _obstack_newchunk(__o,1);
           *(__o -> next_free++) = 32;
          (void )0;
        });
      len = (strlen((tokptr -> token)));
      (
{
        struct obstack *__o = &text_stk;
        int __len = len;
        if (((__o -> next_free) + __len) > (__o -> chunk_limit)) 
          _obstack_newchunk(__o,__len);
        memcpy((__o -> next_free),(tokptr -> token),__len);
        __o -> next_free += __len;
        (void )0;
      });
      need_space = 1;
      break; 
    }
    case 265:
{
      if (need_space != 0) 
        (
{
          struct obstack *__o = &text_stk;
          if (((__o -> next_free) + 1) > (__o -> chunk_limit)) 
            _obstack_newchunk(__o,1);
           *(__o -> next_free++) = 32;
          (void )0;
        });
      if ((tokptr -> token)[0] == '*') 
        need_space = 0;
      else 
        need_space = 1;
      len = (strlen((tokptr -> token)));
      (
{
        struct obstack *__o = &text_stk;
        int __len = len;
        if (((__o -> next_free) + __len) > (__o -> chunk_limit)) 
          _obstack_newchunk(__o,__len);
        memcpy((__o -> next_free),(tokptr -> token),__len);
        __o -> next_free += __len;
        (void )0;
      });
      break; 
    }
/* storage class specifiers are already taken care of */
    case 261:
{
    }
    case 262:
{
      break; 
    }
    case ',':
{
      (
{
        struct obstack *__o = &text_stk;
        if (((__o -> next_free) + 1) > (__o -> chunk_limit)) 
          _obstack_newchunk(__o,1);
         *(__o -> next_free++) = ',';
        (void )0;
      });
      need_space = 1;
      break; 
    }
    case '(':
{
      if (need_space != 0) 
        (
{
          struct obstack *__o = &text_stk;
          if (((__o -> next_free) + 1) > (__o -> chunk_limit)) 
            _obstack_newchunk(__o,1);
           *(__o -> next_free++) = 32;
          (void )0;
        });
      (
{
        struct obstack *__o = &text_stk;
        if (((__o -> next_free) + 1) > (__o -> chunk_limit)) 
          _obstack_newchunk(__o,1);
         *(__o -> next_free++) = (tokptr -> type);
        (void )0;
      });
      need_space = 0;
      break; 
    }
    case ')':
{
      (
{
        struct obstack *__o = &text_stk;
        if (((__o -> next_free) + 1) > (__o -> chunk_limit)) 
          _obstack_newchunk(__o,1);
         *(__o -> next_free++) = (tokptr -> type);
        (void )0;
      });
      need_space = 1;
      break; 
    }
    case '[':
{
    }
    case ']':
{
      (
{
        struct obstack *__o = &text_stk;
        if (((__o -> next_free) + 1) > (__o -> chunk_limit)) 
          _obstack_newchunk(__o,1);
         *(__o -> next_free++) = (tokptr -> type);
        (void )0;
      });
      need_space = 0;
      break; 
    }
    case 266:
{
      (
{
        struct obstack *__o = &text_stk;
        if (((__o -> next_free) + 1) > (__o -> chunk_limit)) 
          _obstack_newchunk(__o,1);
         *(__o -> next_free++) = 32;
        (void )0;
      });
      (
{
        struct obstack *__o = &text_stk;
        int __len = (strlen((tokptr -> token)));
        if (((__o -> next_free) + __len) > (__o -> chunk_limit)) 
          _obstack_newchunk(__o,__len);
        memcpy((__o -> next_free),(tokptr -> token),__len);
        __o -> next_free += __len;
        (void )0;
      });
      need_space = 1;
      break; 
    }
    default:
{
      if (verbose != 0) 
        file_error(gettext("unrecognized definition"),tokptr);
    }
  }
}
/* Start position in stack for saving tokens */
static Stackpos start_pos;
/* Stack position up to which the tokens are saved */
static int save_end;

void save_stack()
{
  mark(start_pos);
  save_end = (curs - 1);
}

void undo_save_stack()
{
  save_end = -1;
}

char *finish_save_stack(char *name)
{
  int i;
  int level = 0;
  int found_ident = !(omit_symbol_names_option != 0);
  need_space = 0;
  for (i = 0; i < save_end; i++) {
    switch(token_stack[i].type){
      case '(':
{
        if (omit_arguments_option != 0) {
          if (level == 0) {
            save_token((token_stack + i));
          }
          level++;
        }
        break; 
      }
      case ')':
{
        if (omit_arguments_option != 0) 
          level--;
        break; 
      }
      case 260:
{
        if (!(found_ident != 0) && (strcmp(name,token_stack[i].token) == 0)) {
          need_space = 1;
          found_ident = 1;
          continue; 
        }
      }
    }
    if (level == 0) 
      save_token((token_stack + i));
  }
  (
{
    struct obstack *__o = &text_stk;
    if (((__o -> next_free) + 1) > (__o -> chunk_limit)) 
      _obstack_newchunk(__o,1);
     *(__o -> next_free++) = 0;
    (void )0;
  });
  return ((
{
    struct obstack *__o1 = &text_stk;
    void *__value = (void *)(__o1 -> object_base);
    if ((__o1 -> next_free) == __value) 
      __o1 -> maybe_empty_object = 1;
    __o1 -> next_free = (((0?(__o1 -> object_base) : ((char *)((char *)0)))) + ((((__o1 -> next_free) - ((0?(__o1 -> object_base) : ((char *)((char *)0))))) + (__o1 -> alignment_mask)) & (~(__o1 -> alignment_mask))));
    if (((__o1 -> next_free) - ((char *)(__o1 -> chunk))) > ((__o1 -> chunk_limit) - ((char *)(__o1 -> chunk)))) 
      __o1 -> next_free = (__o1 -> chunk_limit);
    __o1 -> object_base = (__o1 -> next_free);
    __value;
  }));
}

void skip_to(int c)
{
{
    while(nexttoken() != 0){
      if (tok.type == c) 
        break; 
    }
  }
}

int skip_balanced(int open_tok,int close_tok,int level)
{
  if (level == 0) {
    if (nexttoken() != open_tok) {
      return 1;
    }
    level++;
  }
  while(nexttoken() != 0){
    if ((tok.type == 258) && (open_tok == '{')) 
      tok.type = '{';
    else if ((tok.type == 259) && (close_tok == '}')) 
      tok.type = '}';
    if (tok.type == open_tok) 
      level++;
    else if (tok.type == close_tok) {
      if (--level == 0) {
        nexttoken();
        return 0;
      }
    }
  }
  return -1;
}

int yyparse()
{
  Ident identifier;
  level = 0;
  caller = ((Symbol *)((void *)0));
  clearstack();
  while(nexttoken() != 0){
    identifier.storage = ExternStorage;
    switch(tok.type){
      case 0:
{
        return 0;
      }
      case 273:
{
        continue; 
      }
      case 263:
{
        parse_typedef();
        break; 
      }
      case 261:
{
        identifier.storage = ExplicitExternStorage;
        parse_declaration(&identifier,0);
        break; 
      }
      case 262:
{
        identifier.storage = StaticStorage;
        nexttoken();
      }
/* FALLTHRU */
      default:
{
        parse_declaration(&identifier,0);
        break; 
      }
    }
    cleanup_stack();
  }
  return 0;
}

static int is_function()
{
  Stackpos sp;
  int res = 0;
  mark(sp);
{
    while(1){
      switch(tok.type){
        case 260:
{
        }
        case 261:
{
        }
        case 262:
{
        }
        case 265:
{
        }
        case 270:
{
        }
        case 273:
{
          nexttoken();
          continue; 
        }
        case 272:
{
          if (skip_balanced(40,')',0) == -1) 
            file_error(gettext("unexpected end of file in declaration"),0);
          continue; 
        }
        case '(':
{
          res = (nexttoken() != 265);
          break; 
        }
        default:
{
          break; 
        }
      }
      break; 
    }
  }
  restore(sp);
  return res;
}

void parse_declaration(Ident *ident,int parm)
{
  if (is_function() != 0) 
    parse_function_declaration(ident,parm);
  else 
    parse_variable_declaration(ident,parm);
  delete_parms(parm_level);
}

void expression()
{
  char *name;
  int line;
  int parens_lev;
  parens_lev = 0;
  while(1){
    switch(tok.type){
      case ';':
{
        return ;
      }
      case '{':
{
      }
      case '}':
{
      }
      case 258:
{
      }
      case 259:
{
        putback();
        return ;
      }
      case ',':
{
        if (parens_lev == 0) 
          return ;
        break; 
      }
      case 0:
{
        if (verbose != 0) 
          file_error(gettext("unexpected end of file in expression"),0);
        return ;
      }
      case 260:
{
        name = tok.token;
        line = tok.line;
        nexttoken();
        if (tok.type == 40) {
          call(name,line);
          parens_lev++;
        }
        else {
          reference(name,line);
          if (tok.type == 269) {
            while(tok.type == 269)
              nexttoken();
          }
          else {
            putback();
          }
        }
        break; 
      }
      case '(':
{
/* maybe typecast */
        if (nexttoken() == 270) 
          skip_to(')');
        else {
          putback();
          parens_lev++;
        }
        break; 
      }
      case ')':
{
        parens_lev--;
        break; 
      }
    }
    nexttoken();
  }
}

void parse_function_declaration(Ident *ident,int parm)
{
  int error_recovery = 0;
  ident -> type_end = -1;
  parse_knr_dcl(ident);
  restart:
{
    switch(tok.type){
      case ')':
{
        if (parm != 0) 
          break; 
      }
/*FALLTHROUGH*/
      default:
{
        if (error_recovery != 0) 
          nexttoken();
        else {
          if (verbose != 0) 
            file_error(gettext("expected `;\'"),&tok);
          error_recovery = 1;
        }
        goto restart;
      }
      case ',':
{
      }
      case ';':
{
        break; 
      }
      case '{':
{
      }
      case 258:
{
        if ((ident -> name) != 0) {
          caller = lookup((ident -> name));
          func_body();
        }
        break; 
      }
      case 0:
{
        if (verbose != 0) 
          file_error(gettext("unexpected end of file in declaration"),0);
      }
    }
  }
}

int fake_struct(Ident *ident)
{
  Stackpos sp;
  mark(sp);
  ident -> type_end = -1;
  if (tok.type == 264) {
    if (nexttoken() == 260) {
      ident -> type_end = tos;
    }
    putback();
    skip_struct();
    if ((tok.type == 260) || (tok.type == 265)) {
      TOKSTK hold = tok;
      restore(sp);
      if ((ident -> type_end) == -1) {
/* there was no tag. Insert { ... } */
        tos = curs;
        token_stack[curs].type = 260;
        token_stack[curs].token = "{ ... }";
        tos++;
      }
      else {
        tos = (curs + 1);
      }
      tokpush(hold.type,hold.line,hold.token);
    }
    else if (tok.type == 40) 
      return 0;
    else if (tok.type != ';') 
      file_error(gettext("missing `;\' after struct declaration"),&tok);
    return 1;
  }
  return 0;
}

void parse_variable_declaration(Ident *ident,int parm)
{
  Stackpos sp;
  mark(sp);
  ident -> type_end = -1;
  if (tok.type == 264) {
    if (nexttoken() == 260) {
      ident -> type_end = tos;
    }
    putback();
    skip_struct();
    while((tok.type == 265) || (tok.type == 273))
      nexttoken();
    if (tok.type == 260) {
      TOKSTK hold = tok;
      restore(sp);
      if ((ident -> type_end) == -1) {
/* there was no tag. Insert { ... } */
        tos = curs;
        token_stack[curs].type = 260;
        token_stack[curs].token = "{ ... }";
        tos++;
      }
      else {
        tos = (curs + 1);
      }
      tokpush(hold.type,hold.line,hold.token);
    }
    else {
      if (tok.type == ';') 
        return ;
      restore(sp);
    }
  }
  again:
  parse_dcl(ident,0);
  select:
{
    switch(tok.type){
      case ')':
{
        if (parm != 0) 
          break; 
      }
/*FALLTHROUGH*/
      default:
{
        if (verbose != 0) 
          file_error(gettext("expected `;\'"),&tok);
      }
/* FIXME: should putback() here */
/* FALLTHRU */
      case ';':
{
        break; 
      }
      case ',':
{
        if (parm != 0) 
          break; 
        tos = (ident -> type_end);
        restore(sp);
        goto again;
      }
      case '=':
{
        nexttoken();
        if ((tok.type == '{') || (tok.type == 258)) 
          initializer_list();
        else 
          expression();
        goto select;
        break; 
      }
      case '{':
{
      }
      case 258:
{
        func_body();
        break; 
      }
      case 0:
{
        if (verbose != 0) 
          file_error(gettext("unexpected end of file in declaration"),0);
      }
    }
  }
}

void initializer_list()
{
  int lev = 0;
  while(1){
    switch(tok.type){
      case '{':
{
      }
      case 258:
{
        lev++;
        break; 
      }
      case '}':
{
      }
      case 259:
{
        if (--lev <= 0) {
          nexttoken();
          return ;
        }
        break; 
      }
      case 0:
{
        file_error(gettext("unexpected end of file in initializer list"),0);
        return ;
      }
      case ',':
{
        break; 
      }
      default:
{
        expression();
        break; 
      }
    }
    nexttoken();
  }
}

void parse_knr_dcl(Ident *ident)
{
  ident -> type_end = -1;
  parse_dcl(ident,!(strict_ansi != 0));
}

void skip_struct()
{
  if (nexttoken() == 260) {
    nexttoken();
  }
  else if (tok.type == ';') 
    return ;
  if ((tok.type == '{') || (tok.type == 258)) {
    if (skip_balanced('{','}',1) == -1) {
      file_error(gettext("unexpected end of file in struct"),0);
      return ;
    }
  }
  while(tok.type == 272){
    if (skip_balanced(40,')',0) == -1) 
      file_error(gettext("unexpected end of file in struct"),0);
  }
}

void parse_typedef()
{
  Ident ident;
  ident.name = ((char *)((void *)0));
  ident.type_end = -1;
  ident.parmcnt = -1;
  ident.line = -1;
  ident.storage = AnyStorage;
  nexttoken();
  if (!(fake_struct(&ident) != 0)) 
    putback();
  dcl(&ident);
  if (ident.name != 0) 
    declare_type(&ident);
}

void parse_dcl(Ident *ident,int maybe_knr)
{
  ident -> parmcnt = -1;
  ident -> name = ((char *)((void *)0));
  putback();
  dcl(ident);
  save_stack();
  if ((ident -> name) != 0) 
    declare(ident,maybe_knr);
  else 
    undo_save_stack();
}

int dcl(Ident *idptr)
{
{
    while((nexttoken() != 0) && (tok.type != 40)){
      if (tok.type == 265) {
        if ((idptr != 0) && ((idptr -> type_end) == -1)) 
          idptr -> type_end = (curs - 1);
      }
      else if (tok.type == 272) {
        if (skip_balanced(40,')',0) == -1) {
          file_error(gettext("unexpected end of file in function declaration"),0);
          return 1;
        }
      }
      else if (tok.type == 260) {
        int type;
        while(tok.type == 260)
          nexttoken();
        type = tok.type;
        putback();
        if (!(((type == 270) || (type == 265)) || (type == 273))) 
          break; 
      }
      else if ((tok.type == ')') || (tok.type == ';')) {
        return 1;
      }
    }
  }
  if ((idptr != 0) && ((idptr -> type_end) == -1)) 
    idptr -> type_end = (curs - 1);
  return dirdcl(idptr);
}

int dirdcl(Ident *idptr)
{
  int wrapper = 0;
  int *parm_ptr = (int *)((void *)0);
  if (tok.type == 40) {
    dcl(idptr);
    if ((tok.type != ')') && (verbose != 0)) {
      file_error(gettext("expected `)\'"),&tok);
      return 1;
    }
  }
  else if (tok.type == 260) {
    if (idptr != 0) {
      idptr -> name = tok.token;
      idptr -> line = tok.line;
      parm_ptr = &idptr -> parmcnt;
    }
  }
  if (nexttoken() == 272) {
    wrapper = 1;
/* read '(' */
    nexttoken();
  }
  else 
    putback();
  while((nexttoken() == '[') || (tok.type == 40)){
    if (tok.type == '[') 
      skip_to(']');
    else {
      maybe_parm_list(parm_ptr);
      if ((tok.type != ')') && (verbose != 0)) {
        file_error(gettext("expected `)\'"),&tok);
        return 1;
      }
    }
  }
  if (wrapper != 0) 
/* read ')' */
    nexttoken();
  while(tok.type == 272){
    if (skip_balanced(40,')',0) == -1) 
      file_error(gettext("unexpected end of file in function declaration"),0);
  }
  return 0;
}

int parmdcl(Ident *idptr)
{
  int type;
{
    while((nexttoken() != 0) && (tok.type != 40)){
      if (tok.type == 265) {
        if ((idptr != 0) && ((idptr -> type_end) == -1)) 
          idptr -> type_end = (curs - 1);
      }
      else if (tok.type == 260) {
        while(tok.type == 260)
          nexttoken();
        type = tok.type;
        putback();
        if (type != 265) 
          break; 
      }
      else if ((tok.type == ')') || (tok.type == ',')) 
        return 0;
    }
  }
  if ((idptr != 0) && ((idptr -> type_end) == -1)) 
    idptr -> type_end = (curs - 1);
  return dirdcl(idptr);
}

void maybe_parm_list(int *parm_cnt_return)
{
  int parmcnt = 0;
  Ident ident;
  int level;
  parm_level++;
  while(nexttoken() != 0){
    switch(tok.type){
      case ')':
{
        if (parm_cnt_return != 0) 
           *parm_cnt_return = parmcnt;
        parm_level--;
        return ;
      }
      case ',':
{
        break; 
      }
      case 260:
{
      }
/* unsigned * */
      case 264:
{
      }
      case 265:
{
      }
      case 267:
{
      }
      case 270:
{
      }
      case 273:
{
        parmcnt++;
        ident.storage = AutoStorage;
        parse_declaration(&ident,1);
        putback();
        break; 
      }
      default:
{
        if (verbose != 0) 
          file_error(gettext("unexpected token in parameter list"),&tok);
        level = 0;
{
          do {
            if (tok.type == 40) 
              level++;
            else if (tok.type == ')') {
              if (level-- == 0) 
                break; 
            }
          }while (nexttoken() != 0);
        };
        putback();
      }
    }
  }
  if (verbose != 0) 
    file_error(gettext("unexpected end of file in parameter list"),0);
}

void func_body()
{
  Ident ident;
  level++;
  move_parms(level);
  while(level != 0){
    cleanup_stack();
    nexttoken();
{
      switch(tok.type){
        default:
{
          expression();
          break; 
        }
        case 262:
{
          ident.storage = StaticStorage;
          nexttoken();
          parse_variable_declaration(&ident,0);
          break; 
        }
        case 264:
{
        }
        case 270:
{
          ident.storage = AutoStorage;
          parse_variable_declaration(&ident,0);
          break; 
        }
        case 261:
{
          ident.storage = ExplicitExternStorage;
          parse_declaration(&ident,0);
          break; 
        }
        case '{':
{
        }
        case 258:
{
          level++;
          break; 
        }
        case 259:
{
          if (use_indentation != 0) {
            if ((verbose != 0) && (level != 1)) 
              file_error(gettext("forced function body close"),0);
            for (; level != 0; level--) {
              delete_autos(level);
            }
            break; 
          }
        }
/* else: */
/* FALLTHRU */
        case '}':
{
          delete_autos(level);
          level--;
          break; 
        }
        case 0:
{
          if (verbose != 0) 
            file_error(gettext("unexpected end of file in function body"),0);
          caller = ((Symbol *)((void *)0));
          return ;
        }
      }
    }
  }
  caller = ((Symbol *)((void *)0));
}

int get_knr_args(Ident *ident)
{
  int parmcnt;
  int stop;
  Stackpos sp;
  Stackpos new_sp;
  Ident id;
  switch(tok.type){
    case 260:
{
    }
    case 264:
{
    }
    case 270:
{
/* maybe K&R function definition */
      mark(sp);
      parmcnt = 0;
      for (stop = 0; !(stop != 0) && (parmcnt < (ident -> parmcnt)); nexttoken()) {
        id.type_end = -1;
{
          switch(tok.type){
            case '{':
{
            }
            case 258:
{
              putback();
              stop = 1;
              break; 
            }
            case 260:
{
            }
            case 264:
{
            }
            case 270:
{
              putback();
              mark(new_sp);
              if (dcl(&id) == 0) {
                parmcnt++;
                if (tok.type == ',') {
                  do {
/* ouch! */
                    tos = id.type_end;
                    restore(new_sp);
                    dcl(&id);
                  }while (tok.type == ',');
                }
                else if (tok.type != ';') 
                  putback();
                break; 
              }
            }
/* else */
/* FALLTHRU */
            default:
{
              restore(sp);
              return 1;
            }
          }
        }
      }
    }
  }
  return 0;
}

void declare(Ident *ident,int maybe_knr)
{
  Symbol *sp;
  if ((ident -> storage) == AutoStorage) {
    undo_save_stack();
    sp = install_ident((ident -> name),(ident -> storage));
    if (parm_level != 0) {
      sp -> level = parm_level;
      sp -> flag = symbol_parm;
    }
    else 
      sp -> level = level;
    sp -> arity = -1;
    return ;
  }
  if (((((ident -> parmcnt) >= 0) && (!(maybe_knr != 0) || (get_knr_args(ident) == 0))) && !((((tok.type == '{') || (tok.type == 258)) || (tok.type == 270)) || (tok.type == 272))) || (((ident -> parmcnt) < 0) && ((ident -> storage) == ExplicitExternStorage))) {
    undo_save_stack();
/* add_external()?? */
    return ;
  }
  sp = get_symbol((ident -> name));
  if ((sp -> source) != 0) {
    if (((ident -> storage) == StaticStorage) && (((sp -> storage) != StaticStorage) || (level > 0))) {
      sp = install_ident((ident -> name),(ident -> storage));
    }
    else {
      if ((sp -> arity) >= 0) 
        error_at_line(0,0,filename,(ident -> line),(gettext("%s/%d redefined")),(ident -> name),(sp -> arity));
      else 
        error_at_line(0,0,filename,(ident -> line),(gettext("%s redefined")),(ident -> name));
      error_at_line(0,0,(sp -> source),(sp -> def_line),(gettext("this is the place of previous definition")));
    }
  }
  sp -> type = SymIdentifier;
  sp -> arity = (ident -> parmcnt);
  ident_change_storage(sp,(((ident -> storage) == ExplicitExternStorage)?ExternStorage : (ident -> storage)));
  sp -> decl = finish_save_stack((ident -> name));
  sp -> source = filename;
  sp -> def_line = (ident -> line);
  sp -> level = level;
  if (debug != 0) 
    printf((gettext("%s:%d: %s/%d defined to %s\n")),filename,line_num,(ident -> name),(ident -> parmcnt),(sp -> decl));
}

void declare_type(Ident *ident)
{
  Symbol *sp;
  undo_save_stack();
  sp = lookup((ident -> name));
{
    for (; sp != 0; sp = (sp -> next)) 
      if (((sp -> type) == SymToken) && ((sp -> token_type) == 270)) 
        break; 
  }
  if (!(sp != 0)) 
    sp = install((ident -> name),4);
  sp -> type = SymToken;
  sp -> token_type = 270;
  sp -> source = filename;
  sp -> def_line = (ident -> line);
  sp -> ref_line = ((struct linked_list *)((void *)0));
  if (debug != 0) 
    printf((gettext("%s:%d: type %s\n")),filename,line_num,(ident -> name));
}

Symbol *get_symbol(char *name)
{
  Symbol *sp = lookup(name);
  if (sp != 0) {{
      for (; sp != 0; sp = (sp -> next)) {
        if (((sp -> type) == SymIdentifier) && (strcmp((sp -> name),name) == 0)) 
          break; 
      }
    }
    if (sp != 0) 
      return sp;
  }
  return install_ident(name,ExternStorage);
}

Symbol *add_reference(char *name,int line)
{
  Symbol *sp = get_symbol(name);
  Ref *refptr;
  if (((sp -> storage) == AutoStorage) || (((sp -> storage) == StaticStorage) && (globals_only() != 0))) 
    return 0;
  refptr = (xmalloc((sizeof(( *refptr)))));
  refptr -> source = filename;
  refptr -> line = line;
  if (!((sp -> ref_line) != 0)) 
    sp -> ref_line = linked_list_create(free);
  linked_list_append(&sp -> ref_line,refptr);
  return sp;
}

void call(char *name,int line)
{
  Symbol *sp;
  sp = add_reference(name,line);
  if (!(sp != 0)) 
    return ;
  if ((sp -> arity) < 0) 
    sp -> arity = 0;
  if (caller != 0) {
    if (!(data_in_list(caller,(sp -> caller)) != 0)) 
      linked_list_append(&sp -> caller,caller);
    if (!(data_in_list(sp,(caller -> callee)) != 0)) 
      linked_list_append(&caller -> callee,sp);
  }
}

void reference(char *name,int line)
{
  Symbol *sp = add_reference(name,line);
  if (!(sp != 0)) 
    return ;
  if (caller != 0) {
    if (!(data_in_list(caller,(sp -> caller)) != 0)) 
      linked_list_append(&sp -> caller,caller);
    if (!(data_in_list(sp,(caller -> callee)) != 0)) 
      linked_list_append(&caller -> callee,sp);
  }
}
