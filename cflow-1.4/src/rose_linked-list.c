/* This file is part of GNU cflow
   Copyright (C) 1997, 2005, 2006, 2007, 2009, 2010 Sergey Poznyakoff
   GNU cflow is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   GNU cflow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public
   License along with GNU cflow; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301 USA */
#include <cflow.h>

static struct linked_list *deref_linked_list(struct linked_list **plist)
{
  if (!( *plist != 0)) {
    struct linked_list *list = (xmalloc((sizeof(( *list)))));
    list -> free_data = ((linked_list_free_data_fp )((void *)0));
    list -> head = (list -> tail = ((struct linked_list_entry *)((void *)0)));
     *plist = list;
  }
  return  *plist;
}

struct linked_list *linked_list_create(linked_list_free_data_fp fun)
{
  struct linked_list *list = (xmalloc((sizeof(( *list)))));
  list -> free_data = fun;
  list -> head = (list -> tail = ((struct linked_list_entry *)((void *)0)));
  return list;
}

void linked_list_append(struct linked_list **plist,void *data)
{
  struct linked_list *list = deref_linked_list(plist);
  struct linked_list_entry *entry = (xmalloc((sizeof(( *entry)))));
  entry -> list = list;
  entry -> data = data;
  entry -> next = ((struct linked_list_entry *)((void *)0));
  entry -> prev = (list -> tail);
  if ((list -> tail) != 0) 
    ( *(list -> tail)).next = entry;
  else 
    list -> head = entry;
  list -> tail = entry;
}
#if 0
#endif

void linked_list_destroy(struct linked_list **plist)
{
  if ((plist != 0) && ( *plist != 0)) {
    struct linked_list *list =  *plist;
    struct linked_list_entry *p;
    for (p = (list -> head); p != 0; ) {
      struct linked_list_entry *next = (p -> next);
      if ((list -> free_data) != 0) 
        ( *(list -> free_data))((p -> data));
      free(p);
      p = next;
    }
    free(list);
     *plist = ((struct linked_list *)((void *)0));
  }
}

void linked_list_unlink(struct linked_list *list,struct linked_list_entry *ent)
{
  struct linked_list_entry *p;
  if ((p = (ent -> prev)) != 0) 
    p -> next = (ent -> next);
  else 
    list -> head = (ent -> next);
  if ((p = (ent -> next)) != 0) 
    p -> prev = (ent -> prev);
  else 
    list -> tail = (ent -> prev);
  if ((list -> free_data) != 0) 
    ( *(list -> free_data))((ent -> data));
  free(ent);
}

void linked_list_iterate(struct linked_list **plist,int (*itr)(void *, void *),void *data)
{
  struct linked_list *list;
  struct linked_list_entry *p;
  if (!( *plist != 0)) 
    return ;
  list =  *plist;
  for (p = ((list != 0)?(list -> head) : ((struct linked_list_entry *)((void *)0))); p != 0; ) {
    struct linked_list_entry *next = (p -> next);
    if (( *itr)((p -> data),data) != 0) 
      linked_list_unlink(list,p);
    p = next;
  }
  if (!((list -> head) != 0)) 
    linked_list_destroy(&list);
   *plist = list;
}

int data_in_list(void *data,struct linked_list *list)
{
  struct linked_list_entry *p;
  for (p = ((list != 0)?(list -> head) : ((struct linked_list_entry *)((void *)0))); p != 0; p = (p -> next)) 
    if ((p -> data) == data) 
      return 1;
  return 0;
}
