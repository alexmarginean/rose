/* This file is part of GNU cflow.
   Copyright (C) 2008, 2009, 2010 Sergey Poznyakoff
   GNU cflow is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
   GNU cflow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with GNU cflow.  If not, see <http://www.gnu.org/licenses/>. */
#include <cflow.h>
#define CHAR_BIT 8
#define BITS_PER_WORD   (sizeof(unsigned)*CHAR_BIT)
#define WORDSIZE(n)     (((n) + BITS_PER_WORD - 1) / BITS_PER_WORD)
#define SETBIT(x, i)    ((x)[(i)/BITS_PER_WORD] |= (1<<((i) % BITS_PER_WORD)))
#define RESETBIT(x, i)  ((x)[(i)/BITS_PER_WORD] &= ~(1<<((i) % BITS_PER_WORD)))
#define BITISSET(x, i)  (((x)[(i)/BITS_PER_WORD] & (1<<((i) % BITS_PER_WORD))) != 0)

static void transitive_closure(unsigned int *R,int n)
{
  register size_t rowsize;
  register unsigned int mask;
  register unsigned int *rowj;
  register unsigned int *rp;
  register unsigned int *rend;
  register unsigned int *ccol;
  unsigned int *relend;
  unsigned int *cword;
  unsigned int *rowi;
  rowsize = ((((n + sizeof(unsigned int ) * 8) - 1) / (sizeof(unsigned int ) * 8)) * sizeof(unsigned int ));
  relend = ((unsigned int *)(((char *)R) + (n * rowsize)));
  cword = R;
  mask = 1;
  rowi = R;
  while(rowi < relend){
    ccol = cword;
    rowj = R;
    while(rowj < relend){
      if (( *ccol & mask) != 0U) {
        rp = rowi;
        rend = ((unsigned int *)(((char *)rowj) + rowsize));
        while(rowj < rend)
           *(rowj++) |=  *(rp++);
      }
      else {
        rowj = ((unsigned int *)(((char *)rowj) + rowsize));
      }
      ccol = ((unsigned int *)(((char *)ccol) + rowsize));
    }
    mask <<= 1;
    if (mask == 0) {
      mask = 1;
      cword++;
    }
    rowi = ((unsigned int *)(((char *)rowi) + rowsize));
  }
}

struct cflow_depmap 
{
  size_t nrows;
  size_t rowlen;
  unsigned int r[1UL];
}
;

cflow_depmap_t depmap_alloc(size_t count)
{
  size_t size = (((count + sizeof(unsigned int ) * 8) - 1) / (sizeof(unsigned int ) * 8));
  cflow_depmap_t dmap = (xzalloc((sizeof(( *dmap)) - 1 + ((count * size) * sizeof(unsigned int )))));
  dmap -> nrows = count;
  dmap -> rowlen = size;
  return dmap;
}

static unsigned int *depmap_rowptr(cflow_depmap_t dmap,size_t row)
{
  return (dmap -> r) + ((dmap -> rowlen) * row);
}

void depmap_set(cflow_depmap_t dmap,size_t row,size_t col)
{
  unsigned int *rptr = depmap_rowptr(dmap,row);
  rptr[col / (sizeof(unsigned int ) * 8)] |= (1 << (col % (sizeof(unsigned int ) * 8)));
}

int depmap_isset(cflow_depmap_t dmap,size_t row,size_t col)
{
  unsigned int *rptr = depmap_rowptr(dmap,row);
  return (rptr[col / (sizeof(unsigned int ) * 8)] & (1 << (col % (sizeof(unsigned int ) * 8)))) != 0;
}

void depmap_tc(cflow_depmap_t dmap)
{
  transitive_closure((dmap -> r),(dmap -> nrows));
}
