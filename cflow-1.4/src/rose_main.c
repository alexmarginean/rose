/* This file is part of GNU cflow
   Copyright (C) 1997, 2005, 2007, 2009, 2010, 2011 Sergey Poznyakoff
 
   GNU cflow is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
 
   GNU cflow is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public
   License along with GNU cflow; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301 USA */
#include <cflow.h>
#include <argp.h>
#include <argp-version-etc.h>
#include <progname.h>
#include <stdarg.h>
#include <parser.h>
#include <version-etc.h>
#define MAXLEVELINDENT 216
#define LEVEL_BEGIN 1
#define LEVEL_INDENT0 2
#define LEVEL_INDENT1 3
#define LEVEL_END0 4
#define LEVEL_END1 5
const char *argp_program_bug_address = "<bug-cflow@gnu.org>";
static char doc[] = "generate a program flowgraph\v* The effect of each option marked with an asterisk is reversed if the option\'s long name is prefixed with `no-\'. For example, --no-cpp cancels --cpp.";
const char *program_authors[] = {("Sergey Poznyakoff"), ((const char *)((void *)0))};
enum option_code {OPT_DEFINES=256,OPT_LEVEL_INDENT,OPT_DEBUG,OPT_PREPROCESS,OPT_NO_PREPROCESS,OPT_EMACS,OPT_NO_USE_INDENTATION,OPT_NO_ANSI,OPT_NO_TREE,OPT_NO_BRIEF,OPT_NO_EMACS,OPT_NO_VERBOSE,OPT_NO_NUMBER,OPT_NO_PRINT_LEVEL,OPT_NO_REVERSE,OPT_OMIT_ARGUMENTS,OPT_NO_OMIT_ARGUMENTS,OPT_OMIT_SYMBOL_NAMES,OPT_NO_OMIT_SYMBOL_NAMES};
static struct argp_option options[] = {
#define GROUP_ID 0
{((const char *)((void *)0)), (0), ((const char *)((void *)0)), (0), ("General options:"), (0)}, {("depth"), ('d'), ("NUMBER"), (0), ("Set the depth at which the flowgraph is cut off"), (0 + 1)}, {("include"), ('i'), ("CLASSES"), (0), ("Include specified classes of symbols (see below). Prepend CLASSES with ^ or - to exclude them from the output"), (0 + 1)}, {("format"), ('f'), ("NAME"), (0), ("Use given output format NAME. Valid names are `gnu\' (default) and `posix\'"), (0 + 1)}, {("reverse"), ('r'), ((const char *)((void *)0)), (0), ("* Print reverse call tree"), (0 + 1)}, {("xref"), ('x'), ((const char *)((void *)0)), (0), ("Produce cross-reference listing only"), (0 + 1)}, {("print"), ('P'), ("OPT"), (0x2), ("Set printing option to OPT. Valid OPT values are: xref (or cross-ref), tree. Any unambiguous abbreviation of the above is also accepted"), (0 + 1)}, {("output"), ('o'), ("FILE"), (0), ("Set output file name (default -, meaning stdout)"), (0 + 1)}, {((const char *)((void *)0)), (0), ((const char *)((void *)0)), (0), ("Symbols classes for --include argument"), (0 + 2)}, {("  x"), (0), ((const char *)((void *)0)), (8 | 32), ("all data symbols, both external and static"), (0 + 3)}, {("  _"), (0), ((const char *)((void *)0)), (8 | 32), ("symbols whose names begin with an underscore"), (0 + 3)}, {("  s"), (0), ((const char *)((void *)0)), (8 | 32), ("static symbols"), (0 + 3)}, {("  t"), (0), ((const char *)((void *)0)), (8 | 32), ("typedefs (for cross-references only)"), (0 + 3)}, 
#undef GROUP_ID
#define GROUP_ID 10     
{((const char *)((void *)0)), (0), ((const char *)((void *)0)), (0), ("Parser control:"), (10)}, {("use-indentation"), ('S'), ((const char *)((void *)0)), (0), ("* Rely on indentation"), (10 + 1)}, {("no-use-indentation"), (OPT_NO_USE_INDENTATION), ((const char *)((void *)0)), (0x2), (""), (10 + 1)}, {("ansi"), ('a'), ((const char *)((void *)0)), (0), ("* Accept only sources in ANSI C"), (10 + 1)}, {("no-ansi"), (OPT_NO_ANSI), ((const char *)((void *)0)), (0x2), (""), (10 + 1)}, {("pushdown"), ('p'), ("NUMBER"), (0), ("Set initial token stack size to NUMBER"), (10 + 1)}, {("symbol"), ('s'), ("SYMBOL:[=]TYPE"), (0), ("Register SYMBOL with given TYPE, or define an alias (if := is used). Valid types are: keyword (or kw), modifier, qualifier, identifier, type, wrapper. Any unambiguous abbreviation of the above is also accepted"), (10 + 1)}, {("main"), ('m'), ("NAME"), (0), ("Assume main function to be called NAME"), (10 + 1)}, {("define"), ('D'), ("NAME[=DEFN]"), (0), ("Predefine NAME as a macro"), (10 + 1)}, {("undefine"), ('U'), ("NAME"), (0), ("Cancel any previous definition of NAME"), (10 + 1)}, {("include-dir"), ('I'), ("DIR"), (0), ("Add the directory DIR to the list of directories to be searched for header files."), (10 + 1)}, {("preprocess"), (OPT_PREPROCESS), ("COMMAND"), (0x1), ("* Run the specified preprocessor command"), (10 + 1)}, {("cpp"), (0), ((const char *)((void *)0)), (0x4), ((const char *)((void *)0)), (10 + 1)}, {("no-preprocess"), (OPT_NO_PREPROCESS), ((const char *)((void *)0)), (0x2), (""), (10 + 1)}, {("no-cpp"), (0), ((const char *)((void *)0)), (4 | 2), ((const char *)((void *)0)), (10 + 1)}, 
#undef GROUP_ID
#define GROUP_ID 20          
{((const char *)((void *)0)), (0), ((const char *)((void *)0)), (0), ("Output control:"), (20)}, {("number"), ('n'), ((const char *)((void *)0)), (0), ("* Print line numbers"), (20 + 1)}, {("no-number"), (OPT_NO_NUMBER), ((const char *)((void *)0)), (0x2), (""), (20 + 1)}, {("print-level"), ('l'), ((const char *)((void *)0)), (0), ("* Print nesting level along with the call tree"), (20 + 1)}, {("no-print-level"), (OPT_NO_PRINT_LEVEL), ((const char *)((void *)0)), (0x2), (""), (20 + 1)}, {("level-indent"), (OPT_LEVEL_INDENT), ("ELEMENT"), (0), ("Control graph appearance"), (20 + 1)}, {("tree"), ('T'), ((const char *)((void *)0)), (0), ("* Draw ASCII art tree"), (20 + 1)}, {("no-tree"), (OPT_NO_TREE), ((const char *)((void *)0)), (0x2), (""), (20 + 1)}, {("brief"), ('b'), ((const char *)((void *)0)), (0), ("* Brief output"), (20 + 1)}, {("no-brief"), (OPT_NO_BRIEF), ((const char *)((void *)0)), (0x2), (""), (20 + 1)}, {("emacs"), (OPT_EMACS), ((const char *)((void *)0)), (0), ("* Additionally format output for use with GNU Emacs"), (20 + 1)}, {("no-emacs"), (OPT_NO_EMACS), ((const char *)((void *)0)), (0x2), (""), (20 + 1)}, {("omit-arguments"), (OPT_OMIT_ARGUMENTS), ((const char *)((void *)0)), (0), ("* Do not print argument lists in function declarations"), (20 + 1)}, {("no-ignore-arguments"), (OPT_NO_OMIT_ARGUMENTS), ((const char *)((void *)0)), (0x2), (""), (20 + 1)}, {("omit-symbol-names"), (OPT_OMIT_SYMBOL_NAMES), ((const char *)((void *)0)), (0), ("* Do not print symbol names in declaration strings"), (20 + 1)}, {("no-omit-symbol-names"), (OPT_NO_OMIT_SYMBOL_NAMES), ((const char *)((void *)0)), (0x2), (""), (20 + 1)}, 
#undef GROUP_ID
#define GROUP_ID 30                 
{((const char *)((void *)0)), (0), ((const char *)((void *)0)), (0), ("Informational options:"), (30)}, {("verbose"), ('v'), ((const char *)((void *)0)), (0), ("* Verbose error diagnostics"), (30 + 1)}, {("no-verbose"), (OPT_NO_VERBOSE), ((const char *)((void *)0)), (0x2), (""), (30 + 1)}, {("debug"), (OPT_DEBUG), ("NUMBER"), (0x1), ("Set debugging level"), (30 + 1)}, 
#undef GROUP_ID     
{(0)}};
/* Structure representing various arguments of command line options */

struct option_type 
{
/* optarg value */
  char *str;
/* minimal number of characters to match */
  int min_match;
/* data associated with the arg */
  int type;
}
;
/* debug level */
int debug;
/* default output file name */
char *outname = "-";
/* what to print. */
int print_option = 0;
/* be verbose on output */
int verbose;
/* Rely on indentation,
			 * i.e. suppose the function body
                         * is necessarily surrounded by the curly braces
			 * in the first column
                         */
int use_indentation;
/* Record macro definitions */
int record_defines;
/* Assume sources to be written in ANSI C */
int strict_ansi;
/* Print line numbers */
int print_line_numbers;
/* Print level number near every branch */
int print_levels;
/* Print as tree */
int print_as_tree;
/* Produce short listing */
int brief_listing;
/* Generate reverse tree */
int reverse_tree;
/* The depth at which the flowgraph is cut off */
int max_depth;
/* Format and check for use with Emacs cflow-mode */
int emacs_option;
/* Omit arguments from function declaration string */
int omit_arguments_option;
/* Omit symbol name from symbol declaration string */
int omit_symbol_names_option;
#define SM_FUNCTIONS   0x0001
#define SM_DATA        0x0002
#define SM_STATIC      0x0004
#define SM_UNDERSCORE  0x0008
#define SM_TYPEDEF     0x0010
#define SM_UNDEFINED   0x0020
#define CHAR_TO_SM(c) ((c)=='x' ? SM_DATA : \
                        (c)=='_' ? SM_UNDERSCORE : \
                         (c)=='s' ? SM_STATIC : \
                          (c)=='t' ? SM_TYPEDEF : \
                           (c)=='u' ? SM_UNDEFINED : 0)
#define SYMBOL_INCLUDE(c) (symbol_map |= CHAR_TO_SM(c))
#define SYMBOL_EXCLUDE(c) (symbol_map &= ~CHAR_TO_SM(c))
/* A bitmap of symbols included in the graph. */
int symbol_map;
char *level_indent[] = {((char *)((void *)0)), ((char *)((void *)0))};
char *level_end[] = {(""), ("")};
char *level_begin = "";
/* Do they want to preprocess sources? */
int preprocess_option = 0;
/* Name of start symbol */
char *start_name = "main";
/* List of command line arguments */
struct linked_list *arglist;
/* Given the option_type array and (possibly abbreviated) option argument
 * find the type corresponding to that argument.
 * Return 0 if the argument does not match any one of OPTYPE entries
 */

static int find_option_type(struct option_type *optype,const char *str,int len)
{
  if (len == 0) 
    len = (strlen(str));
  for (; (optype -> str) != 0; optype++) {
    if ((len >= (optype -> min_match)) && (memcmp(str,(optype -> str),len) == 0)) {
      return optype -> type;
    }
  }
  return 0;
}
/* Args for --symbol option */
static struct option_type symbol_optype[] = {{("keyword"), (2), (257)}, {("kw"), (2), (257)}, {("modifier"), (1), (265)}, {("identifier"), (1), (260)}, {("type"), (1), (270)}, {("wrapper"), (1), (272)}, {("qualifier"), (1), (273)}, {(0)}};
/* Parse the string STR and store the symbol in the temporary symbol table.
 * STR is the string of form: NAME:TYPE
 * NAME means symbol name, TYPE means symbol type (possibly abbreviated)
 */

static void symbol_override(const char *str)
{
  const char *ptr;
  char *name;
  Symbol *sp;
  ptr = (strchr(str,':'));
  if (!(ptr != 0)) {
    error(0,0,(gettext("%s: no symbol type supplied")),str);
    return ;
  }
  else {
    name = strndup(str,(ptr - str));
    if (ptr[1] == '=') {
      Symbol *alias = lookup((ptr + 2));
      if (!(alias != 0)) {
        alias = install(xstrdup((ptr + 2)),1);
        alias -> type = SymToken;
        alias -> token_type = 0;
        alias -> source = ((char *)((void *)0));
        alias -> def_line = -1;
        alias -> ref_line = ((struct linked_list *)((void *)0));
      }
      sp = install(name,1);
      sp -> type = SymToken;
      sp -> alias = alias;
      sp -> flag = symbol_alias;
    }
    else {
      int type = find_option_type(symbol_optype,(ptr + 1),0);
      if (type == 0) {
        error(0,0,(gettext("unknown symbol type: %s")),(ptr + 1));
        return ;
      }
      sp = install(name,1);
      sp -> type = SymToken;
      sp -> token_type = type;
    }
    sp -> source = ((char *)((void *)0));
    sp -> def_line = -1;
    sp -> ref_line = ((struct linked_list *)((void *)0));
  }
}
/* Args for --print option */
static struct option_type print_optype[] = {{("xref"), (1), (0x01)}, {("cross-ref"), (1), (0x01)}, {("tree"), (1), (0x02)}, {(0)}};

static void set_print_option(char *str)
{
  int opt;
  opt = find_option_type(print_optype,str,0);
  if (opt == 0) {
    error(0,0,(gettext("unknown print option: %s")),str);
    return ;
  }
  print_option |= opt;
}
/* Convert first COUNT bytes of the string pointed to by STR_PTR
 * to integer using BASE. Move STR_PTR to the point where the
 * conversion stopped.
 * Return the number obtained.
 */

static int number(const char **str_ptr,int base,int count)
{
  int c;
  int n;
  unsigned int i;
  const char *str =  *str_ptr;
{
    for (n = 0; (( *str) != 0) && (count != 0); count--) {
      c = ( *(str++));
      if ((( *__ctype_b_loc())[(int )c] & ((unsigned short )_ISdigit)) != 0) 
        i = (c - 48);
      else 
        i = ((toupper(c) - 'A') + 10);
      if (i > base) {
        break; 
      }
      n = ((n * base) + i);
    }
  }
   *str_ptr = (str - 1);
  return n;
}
/* Processing for --level option
 * The option syntax is
 *    --level NUMBER
 * or
 *    --level KEYWORD=STR
 * where
 *    KEYWORD is one of "begin", "0", ", "1", "end0", "end1",
 *    or an abbreviation thereof,
 *    STR is the value to be assigned to the parameter.
 *  
 * STR can contain usual C escape sequences plus \e meaning '\033'.
 * Apart from this any character followed by xN suffix (where N is
 * a decimal number) is expanded to the sequence of N such characters.
 * 'x' looses its special meaning at the start of the string.
 */
static struct option_type level_indent_optype[] = {{("begin"), (1), (1)}, {("start"), (1), (1)}, {("0"), (1), (2)}, {("1"), (1), (3)}, {("end0"), (4), (4)}, {("end1"), (4), (5)}};

static void parse_level_string(const char *str,char **return_ptr)
{
  static char text[216UL];
  char *p;
  int i;
  int c;
  int num;
  p = text;
  memset(text,32,(sizeof(text)));
  text[sizeof(text) - 1] = 0;
  while(( *str) != 0){
    switch(( *str)){
      case '\\':
{
        switch(( *(++str))){
          case 'a':
{
             *(p++) = 7;
            break; 
          }
          case 'b':
{
             *(p++) = 8;
            break; 
          }
          case 'e':
{
             *(p++) = '\033';
            break; 
          }
          case 'f':
{
             *(p++) = '\f';
            break; 
          }
          case 'n':
{
             *(p++) = 10;
            break; 
          }
          case 'r':
{
             *(p++) = 13;
            break; 
          }
          case 't':
{
             *(p++) = 9;
            break; 
          }
          case 'X':
{
          }
          case 'x':
{
            ++str;
             *(p++) = (number(&str,16,2));
            break; 
          }
          case '0':
{
            ++str;
             *(p++) = (number(&str,8,3));
            break; 
          }
          default:
{
             *(p++) =  *str;
          }
        }
        ++str;
        break; 
      }
      case 'x':
{
        if (p == text) {
          goto copy;
        }
        num = (strtol((str + 1),((char **)(&str)),10));
        c = p[-1];
        for (i = 1; i < num; i++) {
           *(p++) = c;
          if (( *p) == 0) {
            error(1,0,(gettext("level indent string is too long")));
            return ;
          }
        }
        break; 
      }
      default:
{
        copy:
         *(p++) =  *(str++);
        if (( *p) == 0) {
          error(1,0,(gettext("level indent string is too long")));
          return ;
        }
      }
    }
  }
   *p = 0;
   *return_ptr = strdup(text);
}

static void set_level_indent(const char *str)
{
  long n;
  const char *p;
  char *q;
  n = strtol(str,&q,0);
  if ((( *q) == 0) && (n > 0)) {
    char *s = (xmalloc((n + 1)));
    memset(s,32,(n - 1));
    s[n - 1] = 0;
    level_indent[0] = (level_indent[1] = s);
    return ;
  }
  p = str;
  while(( *p) != '='){
    if (( *p) == 0) {
      error(1,0,(gettext("level-indent syntax")));
      return ;
    }
    p++;
  }
  ++p;
  switch(find_option_type(level_indent_optype,str,((p - str) - 1))){
    case 1:
{
      parse_level_string(p,&level_begin);
      break; 
    }
    case 2:
{
      parse_level_string(p,(level_indent + 0));
      break; 
    }
    case 3:
{
      parse_level_string(p,(level_indent + 1));
      break; 
    }
    case 4:
{
      parse_level_string(p,(level_end + 0));
      break; 
    }
    case 5:
{
      parse_level_string(p,(level_end + 1));
      break; 
    }
    default:
{
      error(1,0,(gettext("unknown level indent option: %s")),str);
    }
  }
}

static void add_name(const char *name)
{
  linked_list_append(&arglist,((void *)name));
}

static void add_preproc_option(int key,const char *arg)
{
  char *opt = (xmalloc((3 + strlen(arg))));
  sprintf(opt,"-%c%s",key,arg);
  add_name(opt);
  preprocess_option = 1;
}

static error_t parse_opt(int key,char *arg,struct argp_state *state)
{
  int num;
  switch(key){
    case 'a':
{
      strict_ansi = 1;
      break; 
    }
    case 263:
{
      strict_ansi = 0;
      break; 
    }
    case 258:
{
      debug = ((arg != 0)?atoi(arg) : 1);
      break; 
    }
    case 'P':
{
      set_print_option(arg);
      break; 
    }
    case 'S':
{
      use_indentation = 1;
      break; 
    }
    case 262:
{
      use_indentation = 0;
      break; 
    }
    case 'T':
{
      print_as_tree = 1;
/* two spaces */
      set_level_indent("0=  ");
      set_level_indent("1=| ");
      set_level_indent("end0=+-");
      set_level_indent("end1=\\\\-");
      break; 
    }
    case 264:
{
      print_as_tree = 0;
      level_indent[0] = (level_indent[1] = ((char *)((void *)0)));
      level_end[0] = (level_end[0] = ((char *)((void *)0)));
      break; 
    }
    case 'b':
{
      brief_listing = 1;
      break; 
    }
    case 265:
{
      brief_listing = 0;
      break; 
    }
    case 'd':
{
      max_depth = atoi(arg);
      if (max_depth < 0) 
        max_depth = 0;
      break; 
    }
/* FIXME: Not used. */
    case 256:
{
      record_defines = 1;
      break; 
    }
    case 261:
{
      emacs_option = 1;
      break; 
    }
    case 266:
{
      emacs_option = 0;
      break; 
    }
    case 'f':
{
      if (select_output_driver(arg) != 0) 
        argp_error(state,(gettext("%s: No such output driver")),rpl_optarg);
      output_init();
      break; 
    }
    case 257:
{
      set_level_indent(arg);
      break; 
    }
    case 'i':
{
      num = 1;
      for (; ( *arg) != 0; arg++) 
        switch(( *arg)){
          case '-':
{
          }
          case '^':
{
            num = 0;
            break; 
          }
          case '+':
{
            num = 1;
            break; 
          }
          case '_':
{
          }
          case 's':
{
          }
          case 't':
{
          }
          case 'u':
{
          }
          case 'x':
{
            if (num != 0) 
              symbol_map |= ((( *arg) == 'x')?2 : (((( *arg) == '_')?8 : (((( *arg) == 's')?4 : (((( *arg) == 't')?16 : (((( *arg) == 'u')?32 : 0)))))))));
            else 
              symbol_map &= ~(((( *arg) == 'x')?2 : (((( *arg) == '_')?8 : (((( *arg) == 's')?4 : (((( *arg) == 't')?16 : (((( *arg) == 'u')?32 : 0))))))))));
            break; 
          }
          default:
{
            argp_error(state,(gettext("Unknown symbol class: %c")),( *arg));
          }
        }
      break; 
    }
    case 271:
{
      omit_arguments_option = 1;
      break; 
    }
    case 272:
{
      omit_arguments_option = 0;
      break; 
    }
    case 273:
{
      omit_symbol_names_option = 1;
      break; 
    }
    case 274:
{
      omit_symbol_names_option = 0;
      break; 
    }
    case 'l':
{
      print_levels = 1;
      break; 
    }
    case 269:
{
      print_levels = 0;
      break; 
    }
    case 'm':
{
      start_name = strdup(arg);
      break; 
    }
    case 'n':
{
      print_line_numbers = 1;
      break; 
    }
    case 268:
{
      print_line_numbers = 0;
      break; 
    }
    case 'o':
{
      outname = strdup(arg);
      break; 
    }
    case 'p':
{
      num = atoi(arg);
      if (num > 0) 
        token_stack_length = num;
      break; 
    }
    case 'r':
{
      reverse_tree = 1;
      break; 
    }
    case 270:
{
      reverse_tree = 0;
      break; 
    }
    case 's':
{
      symbol_override(arg);
      break; 
    }
    case 'v':
{
      verbose = 1;
      break; 
    }
    case 267:
{
      verbose = 0;
      break; 
    }
    case 'x':
{
      print_option = 1;
      break; 
    }
    case 259:
{
      preprocess_option = 1;
      set_preprocessor(((arg != 0)?arg : "/usr/bin/cpp"));
      break; 
    }
    case 260:
{
      preprocess_option = 0;
      break; 
    }
    case 0:
{
      add_name(arg);
      break; 
    }
    case 'D':
{
    }
    case 'I':
{
    }
    case 'U':
{
      add_preproc_option(key,arg);
      break; 
    }
    default:
{
      return 7;
    }
  }
  return 0;
}
static struct argp argp = {(options), (parse_opt), ("[FILE]..."), (doc), ((const struct argp_child *)((void *)0)), ((char *(*)(int , const char *, void *))((void *)0)), ((const char *)((void *)0))};

int globals_only()
{
  return !((symbol_map & 4) != 0);
}

int include_symbol(Symbol *sym)
{
  int type = 0;
  if (!(sym != 0)) 
    return 0;
  if ((sym -> type) == SymIdentifier) {
    if (((sym -> name)[0] == '_') && !((symbol_map & 8) != 0)) 
      return 0;
    if ((sym -> storage) == StaticStorage) 
      type |= 4;
    if (((sym -> arity) == -1) && ((sym -> storage) != AutoStorage)) 
      type |= 2;
    else if ((sym -> arity) >= 0) 
      type |= 1;
    if (!((sym -> source) != 0)) 
      type |= 32;
  }
  else if ((sym -> type) == SymToken) {
    if (((sym -> token_type) == 270) && ((sym -> source) != 0)) 
      type |= 16;
    else 
      return 0;
  }
  return (symbol_map & type) == type;
}

void xalloc_die()
{
  error(1,'\f',(gettext("Exiting")));
}

void init()
{
  if (level_indent[0] == ((char *)((void *)0))) 
/* 4 spaces */
    level_indent[0] = "    ";
  if (level_indent[1] == ((char *)((void *)0))) 
    level_indent[1] = level_indent[0];
  if (level_end[0] == ((char *)((void *)0))) 
    level_end[0] = "";
  if (level_end[1] == ((char *)((void *)0))) 
    level_end[1] = "";
  init_lex((debug > 1));
  init_parse();
}
const char version_etc_copyright[] = "Copyright %s 2005, 2006, 2009, 2010, 2011 %d Sergey Poznyakoff";
/* Do *not* mark this string for translation.  %s is a copyright
     symbol suitable for this locale, and %d is the copyright
     year.  */
typedef char *temp_string_parse;
typedef int temp_index;

int main(int argc,char **argv)
{
  temp_index index;
  set_program_name(argv[0]);
  argp_version_setup("cflow",program_authors);
  setlocale(6,"");
  bindtextdomain("cflow","/usr/local/share/locale");
  textdomain("cflow");
  register_output("gnu",gnu_output_handler,0);
  register_output("posix",posix_output_handler,0);
  symbol_map = 1 | 4 | 32;
  if (getenv("POSIXLY_CORRECT") != 0) {
    if (select_output_driver("posix") != 0) 
      error(1,0,(gettext("%s: No such output driver")),"posix");
    output_init();
  }
  sourcerc(&argc,&argv);
  if (argp_parse((&argp),argc,argv,8,&index,0) != 0) 
    exit(1);
  if (print_option == 0) 
    print_option = 2;
  init();
  if (arglist != 0) {
    struct linked_list_entry *p;
    for (p = (arglist -> head); p != 0; p = (p -> next)) {
      temp_string_parse s = (char *)(p -> data);
      if (s[0] == '-') 
        pp_option(s);
      else if (source(s) == 0) 
        yyparse();
    }
  }
  argc -= index;
  argv += index;
  while(argc-- != 0){
    if (source( *(argv++)) == 0) 
      yyparse();
  }
  if (input_file_count == 0) 
    error(1,0,(gettext("no input files")));
//char * myOutName;
//myOutName = (char *) malloc (500 * sizeof(char));
//strcpy(myOutName, "/home/alex/Development/GENERATING_RESULTS_FINAL_PROJ/CACA.out");
//outname = myOutName;
  output();
  return 0;
}
