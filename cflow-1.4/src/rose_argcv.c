/* argcv.c - simple functions for parsing input based on whitespace
   Copyright (C) 1999, 2000, 2001, 2005, 2007 Free Software Foundation, Inc.
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.
   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.
   You should have received a copy of the GNU Lesser General
   Public License along with this library; if not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301 USA */
#include <config.h>
#include <ctype.h>
#include <errno.h>
#include <argcv.h>
#define to_num(c) \
  (isdigit(c) ? c - '0' : (isxdigit(c) ? toupper(c) - 'A' + 10 : 255 ))
#define isws(c) ((c)==' '||(c)=='\t'||(c)=='\n')
#define isdelim(c,delim) (strchr(delim,(c))!=NULL)

static int argcv_scan(int len,const char *command,const char *delim,const char *cmnt,int *start,int *end,int *save)
{
  int i = 0;
{
    for (; ; ) {
      i =  *save;
      if (i >= len) 
        return i + 1;
/* Skip initial whitespace */
      while((i < len) && (((command[i] == 32) || (command[i] == 9)) || (command[i] == 10)))
        i++;
       *start = i;
      if (!(strchr(delim,command[i]) != ((char *)((void *)0)))) {{
          while(i < len){
            if (command[i] == '\\') {
              if (++i == len) 
                break; 
              i++;
              continue; 
            }
            if ((command[i] == '\'') || (command[i] == '"')) {
              int j;
              for (j = (i + 1); (j < len) && (command[j] != command[i]); j++) 
                if (command[j] == '\\') 
                  j++;
              if (j < len) 
                i = (j + 1);
              else 
                i++;
            }
            else if ((((command[i] == 32) || (command[i] == 9)) || (command[i] == 10)) || (strchr(delim,command[i]) != ((char *)((void *)0)))) 
              break; 
            else 
/* skip the escaped character */
              i++;
          }
        }
        i--;
      }
       *end = i;
       *save = (i + 1);
/* If we have a token, and it starts with a comment character, skip
         to the newline and restart the token search. */
      if ( *save <= len) {
        if ((cmnt != 0) && (strchr(cmnt,command[ *start]) != ((char *)((void *)0)))) {
          i =  *save;
          while((i < len) && (command[i] != 10))
            i++;
           *save = i;
          continue; 
        }
      }
      break; 
    }
  }
  return  *save;
}
static char quote_transtab[] = "\\\\a\ab\bf\fn\nr\rt\t";

int argcv_unquote_char(int c)
{
  char *p;
  for (p = quote_transtab; ( *p) != 0; p += 2) {
    if (( *p) == c) 
      return p[1];
  }
  return c;
}

int argcv_quote_char(int c)
{
  char *p;
  for (p = ((quote_transtab + sizeof(quote_transtab)) - 2); p > quote_transtab; p -= 2) {
    if (( *p) == c) 
      return p[-1];
  }
  return -1;
}

static int xtonum(int *pval,const char *src,int base,int cnt)
{
  int i;
  int val;
{
    for (((i = 0) , (val = 0)); i < cnt; (i++ , src++)) {
      int n = ( *((unsigned char *)src));
      if ((n > 127) || ((n = (((( *__ctype_b_loc())[(int )n] & ((unsigned short )_ISdigit)) != 0)?(n - 48) : ((((( *__ctype_b_loc())[(int )n] & ((unsigned short )_ISxdigit)) != 0)?((toupper(n) - 'A') + 10) : 255)))) >= base)) 
        break; 
      val = ((val * base) + n);
    }
  }
   *pval = val;
  return i;
}

size_t argcv_quoted_length(const char *str,int *quote)
{
  size_t len = 0;
   *quote = 0;
  for (; ( *str) != 0; str++) {
    if (( *str) == 32) {
      len++;
       *quote = 1;
    }
    else if ((( *str) == '"') || (( *str) == '\'')) {
      len += 2;
       *quote = 1;
    }
    else if (((( *str) != 9) && (( *str) != '\\')) && ((( *__ctype_b_loc())[(int )( *str)] & ((unsigned short )_ISprint)) != 0)) 
      len++;
    else if (argcv_quote_char(( *str)) != -1) 
      len += 2;
    else 
      len += 4;
  }
  return len;
}

void argcv_unquote_copy(char *dst,const char *src,size_t n)
{
  int i = 0;
  int c;
  int expect_delim = 0;
  while(i < n){
    switch(src[i]){
      case '"':
{
      }
      case '\'':
{
        if (!(expect_delim != 0)) {
          const char *p;
          for (p = ((src + i) + 1); (( *p) != 0) && (( *p) != src[i]); p++) 
            if (( *p) == '\\') 
              p++;
          if (( *p) != 0) 
            expect_delim = src[i++];
          else 
             *(dst++) = src[i++];
        }
        else if (expect_delim == src[i]) 
          ++i;
        else 
           *(dst++) = src[i++];
        break; 
      }
      case '\\':
{
        ++i;
        if ((src[i] == 'x') || (src[i] == 'X')) {
          if ((n - i) < 2) {
             *(dst++) = '\\';
             *(dst++) = src[i++];
          }
          else {
            int off = xtonum(&c,((src + i) + 1),16,2);
            if (off == 0) {
               *(dst++) = '\\';
               *(dst++) = src[i++];
            }
            else {
               *(dst++) = c;
              i += (off + 1);
            }
          }
        }
        else if ((((unsigned char )src[i]) < 128) && ((( *__ctype_b_loc())[(int )src[i]] & ((unsigned short )_ISdigit)) != 0)) {
          if ((n - i) < 1) {
             *(dst++) = '\\';
             *(dst++) = src[i++];
          }
          else {
            int off = xtonum(&c,(src + i),8,3);
            if (off == 0) {
               *(dst++) = '\\';
               *(dst++) = src[i++];
            }
            else {
               *(dst++) = c;
              i += off;
            }
          }
        }
        else 
           *(dst++) = (argcv_unquote_char(src[i++]));
        break; 
      }
      default:
{
         *(dst++) = src[i++];
      }
    }
  }
   *dst = 0;
}

void argcv_quote_copy(char *dst,const char *src)
{
  for (; ( *src) != 0; src++) {
    if ((( *src) == '"') || (( *src) == '\'')) {
       *(dst++) = '\\';
       *(dst++) =  *src;
    }
    else if (((( *src) != 9) && (( *src) != '\\')) && ((( *__ctype_b_loc())[(int )( *src)] & ((unsigned short )_ISprint)) != 0)) 
       *(dst++) =  *src;
    else {
      int c = argcv_quote_char(( *src));
       *(dst++) = '\\';
      if (c != -1) 
         *(dst++) = c;
      else {
        char tmp[4UL];
        snprintf(tmp,(sizeof(tmp)),"%03o",( *((unsigned char *)src)));
        memcpy(dst,tmp,3);
        dst += 3;
      }
    }
  }
}

int argcv_get(const char *command,const char *delim,const char *cmnt,int *argc,char ***argv)
{
  int len = (strlen(command));
  int i = 0;
  int start;
  int end;
  int save;
   *argv = ((char **)((void *)0));
/* Count number of arguments */
   *argc = 0;
  save = 0;
  while(argcv_scan(len,command,delim,cmnt,&start,&end,&save) <= len)
    ( *argc)++;
   *argv = (calloc(( *argc + 1),(sizeof(char *))));
  if ( *argv == ((char **)((void *)0))) 
    return 12;
  i = 0;
  save = 0;
  for (i = 0; i <  *argc; i++) {
    int n;
    argcv_scan(len,command,delim,cmnt,&start,&end,&save);
    if (((command[start] == '"') || (command[end] == '\'')) && (command[end] == command[start])) {
      start++;
      end--;
    }
    n = ((end - start) + 1);
    ( *argv)[i] = (calloc((n + 1),(sizeof(char ))));
    if (( *argv)[i] == ((char *)((void *)0))) 
      return 12;
    argcv_unquote_copy(( *argv)[i],(command + start),n);
    ( *argv)[i][n] = 0;
  }
  ( *argv)[i] = ((char *)((void *)0));
  return 0;
}
/*
 * frees all elements of an argv array
 * argc is the number of elements
 * argv is the array
 */

int argcv_free(int argc,char **argv)
{
  while(--argc >= 0)
    if (argv[argc] != 0) 
      free(argv[argc]);
  free(argv);
  return 0;
}
/* Take a argv an make string separated by ' '.  */

int argcv_string(int argc,char **argv,char **pstring)
{
  size_t i;
  size_t j;
  size_t len;
  char *buffer;
/* No need.  */
  if (pstring == ((char **)((void *)0))) 
    return 22;
  buffer = (malloc(1));
  if (buffer == ((char *)((void *)0))) 
    return 12;
   *buffer = 0;
  for (len = (i = (j = 0)); i < argc; i++) {
    int quote;
    int toklen;
    toklen = (argcv_quoted_length(argv[i],&quote));
    len += (toklen + 2);
    if (quote != 0) 
      len += 2;
    buffer = (realloc(buffer,len));
    if (buffer == ((char *)((void *)0))) 
      return 12;
    if (i != 0) 
      buffer[j++] = 32;
    if (quote != 0) 
      buffer[j++] = '"';
    argcv_quote_copy((buffer + j),argv[i]);
    j += toklen;
    if (quote != 0) 
      buffer[j++] = '"';
  }
  for (; (j > 0) && ((( *__ctype_b_loc())[(int )buffer[j - 1]] & ((unsigned short )_ISspace)) != 0); j--) ;
  buffer[j] = 0;
  if (pstring != 0) 
     *pstring = buffer;
  return 0;
}
