/* -*- buffer-read-only: t -*- vi: set ro: */
/* DO NOT EDIT! GENERATED AUTOMATICALLY! */
/* basename.c -- return the last element in a file name
   Copyright (C) 1990, 1998, 1999, 2000, 2001, 2003, 2004, 2005, 2006,
   2009 Free Software Foundation, Inc.
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */
#include <config.h>
#include "dirname.h"
#include <string.h>
/* Return the address of the last file name component of NAME.  If
   NAME has no relative file name components because it is a file
   system root, return the empty string.  */

char *last_component(const char *name)
{
  const char *base = (name + 0);
  const char *p;
  _Bool saw_slash = 0;
  while(( *base) == '/')
    base++;
  for (p = base; ( *p) != 0; p++) {
    if (( *p) == '/') 
      saw_slash = 1;
    else if (saw_slash != 0) {
      base = p;
      saw_slash = 0;
    }
  }
  return (char *)base;
}
/* Return the length of the basename NAME.  Typically NAME is the
   value returned by base_name or last_component.  Act like strlen
   (NAME), except omit all trailing slashes.  */

size_t base_len(const char *name)
{
  size_t len;
  size_t prefix_len = 0;
  for (len = strlen(name); (1 < len) && (name[len - 1] == '/'); len--) {
    continue; 
  }
  if ((((0 && (len == 1)) && (name[0] == '/')) && (name[1] == '/')) && !(name[2] != 0)) 
    return 2;
  if (((0 && (prefix_len != 0UL)) && (len == prefix_len)) && (name[prefix_len] == '/')) 
    return prefix_len + 1;
  return len;
}
