/* -*- buffer-read-only: t -*- vi: set ro: */
/* DO NOT EDIT! GENERATED AUTOMATICALLY! */
/* vsprintf with automatic memory allocation.
   Copyright (C) 1999, 2002-2009 Free Software Foundation, Inc.
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.  */
/* This file can be parametrized with the following macros:
     VASNPRINTF         The name of the function being defined.
     FCHAR_T            The element type of the format string.
     DCHAR_T            The element type of the destination (result) string.
     FCHAR_T_ONLY_ASCII Set to 1 to enable verification that all characters
                        in the format string are ASCII. MUST be set if
                        FCHAR_T and DCHAR_T are not the same type.
     DIRECTIVE          Structure denoting a format directive.
                        Depends on FCHAR_T.
     DIRECTIVES         Structure denoting the set of format directives of a
                        format string.  Depends on FCHAR_T.
     PRINTF_PARSE       Function that parses a format string.
                        Depends on FCHAR_T.
     DCHAR_CPY          memcpy like function for DCHAR_T[] arrays.
     DCHAR_SET          memset like function for DCHAR_T[] arrays.
     DCHAR_MBSNLEN      mbsnlen like function for DCHAR_T[] arrays.
     SNPRINTF           The system's snprintf (or similar) function.
                        This may be either snprintf or swprintf.
     TCHAR_T            The element type of the argument and result string
                        of the said SNPRINTF function.  This may be either
                        char or wchar_t.  The code exploits that
                        sizeof (TCHAR_T) | sizeof (DCHAR_T) and
                        alignof (TCHAR_T) <= alignof (DCHAR_T).
     DCHAR_IS_TCHAR     Set to 1 if DCHAR_T and TCHAR_T are the same type.
     DCHAR_CONV_FROM_ENCODING A function to convert from char[] to DCHAR[].
     DCHAR_IS_UINT8_T   Set to 1 if DCHAR_T is uint8_t.
     DCHAR_IS_UINT16_T  Set to 1 if DCHAR_T is uint16_t.
     DCHAR_IS_UINT32_T  Set to 1 if DCHAR_T is uint32_t.  */
/* Tell glibc's <stdio.h> to provide a prototype for snprintf().
   This must come before <config.h> because <config.h> may include
   <features.h>, and once <features.h> has been included, it's too late.  */
#ifndef _GNU_SOURCE
# define _GNU_SOURCE    1
#endif
#ifndef VASNPRINTF
# include <config.h>
#endif
#ifndef IN_LIBINTL
# include <alloca.h>
#endif
/* Specification.  */
#ifndef VASNPRINTF
# if WIDE_CHAR_VERSION
#  include "vasnwprintf.h"
# else
#  include "vasnprintf.h"
# endif
#endif
#include <locale.h>	/* localeconv() */
#include <stdio.h>	/* snprintf(), sprintf() */
#include <stdlib.h>	/* abort(), malloc(), realloc(), free() */
#include <string.h>	/* memcpy(), strlen() */
#include <errno.h>	/* errno */
#include <limits.h>	/* CHAR_BIT */
#include <float.h>	/* DBL_MAX_EXP, LDBL_MAX_EXP */
#if HAVE_NL_LANGINFO
# include <langinfo.h>
#endif
#ifndef VASNPRINTF
# if WIDE_CHAR_VERSION
#  include "wprintf-parse.h"
# else
#  include "printf-parse.h"
# endif
#endif
/* Checked size_t computations.  */
#include "xsize.h"
#if (NEED_PRINTF_DOUBLE || NEED_PRINTF_LONG_DOUBLE) && !defined IN_LIBINTL
# include <math.h>
# include "float+.h"
#endif
#if (NEED_PRINTF_DOUBLE || NEED_PRINTF_INFINITE_DOUBLE) && !defined IN_LIBINTL
# include <math.h>
# include "isnand-nolibm.h"
#endif
#if (NEED_PRINTF_LONG_DOUBLE || NEED_PRINTF_INFINITE_LONG_DOUBLE) && !defined IN_LIBINTL
# include <math.h>
# include "isnanl-nolibm.h"
# include "fpucw.h"
#endif
#if (NEED_PRINTF_DIRECTIVE_A || NEED_PRINTF_DOUBLE) && !defined IN_LIBINTL
# include <math.h>
# include "isnand-nolibm.h"
# include "printf-frexp.h"
#endif
#if (NEED_PRINTF_DIRECTIVE_A || NEED_PRINTF_LONG_DOUBLE) && !defined IN_LIBINTL
# include <math.h>
# include "isnanl-nolibm.h"
# include "printf-frexpl.h"
# include "fpucw.h"
#endif
/* Default parameters.  */
#ifndef VASNPRINTF
# if WIDE_CHAR_VERSION
#  define VASNPRINTF vasnwprintf
#  define FCHAR_T wchar_t
#  define DCHAR_T wchar_t
#  define TCHAR_T wchar_t
#  define DCHAR_IS_TCHAR 1
#  define DIRECTIVE wchar_t_directive
#  define DIRECTIVES wchar_t_directives
#  define PRINTF_PARSE wprintf_parse
#  define DCHAR_CPY wmemcpy
#  define DCHAR_SET wmemset
# else
#  define VASNPRINTF vasnprintf
#  define FCHAR_T char
#  define DCHAR_T char
#  define TCHAR_T char
#  define DCHAR_IS_TCHAR 1
#  define DIRECTIVE char_directive
#  define DIRECTIVES char_directives
#  define PRINTF_PARSE printf_parse
#  define DCHAR_CPY memcpy
#  define DCHAR_SET memset
# endif
#endif
#if WIDE_CHAR_VERSION
/* TCHAR_T is wchar_t.  */
# define USE_SNPRINTF 1
# if HAVE_DECL__SNWPRINTF
/* On Windows, the function swprintf() has a different signature than
      on Unix; we use the _snwprintf() function instead.  */
#  define SNPRINTF _snwprintf
# else
/* Unix.  */
#  define SNPRINTF swprintf
# endif
#else
/* TCHAR_T is char.  */
/* Use snprintf if it exists under the name 'snprintf' or '_snprintf'.
     But don't use it on BeOS, since BeOS snprintf produces no output if the
     size argument is >= 0x3000000.
     Also don't use it on Linux libc5, since there snprintf with size = 1
     writes any output without bounds, like sprintf.  */
# if (HAVE_DECL__SNPRINTF || HAVE_SNPRINTF) && !defined __BEOS__ && !(__GNU_LIBRARY__ == 1)
#  define USE_SNPRINTF 1
# else
#  define USE_SNPRINTF 0
# endif
# if HAVE_DECL__SNPRINTF
/* Windows.  */
#  define SNPRINTF _snprintf
# else
/* Unix.  */
#  define SNPRINTF snprintf
/* Here we need to call the native snprintf, not rpl_snprintf.  */
#  undef snprintf
# endif
#endif
/* Here we need to call the native sprintf, not rpl_sprintf.  */
#undef sprintf
/* GCC >= 4.0 with -Wall emits unjustified "... may be used uninitialized"
   warnings in this file.  Use -Dlint to suppress them.  */
#ifdef lint
# define IF_LINT(Code) Code
#else
# define IF_LINT(Code) /* empty */
#endif
/* Avoid some warnings from "gcc -Wshadow".
   This file doesn't use the exp() and remainder() functions.  */
#undef exp
#define exp expo
#undef remainder
#define remainder rem
#if !USE_SNPRINTF && !WIDE_CHAR_VERSION
# if (HAVE_STRNLEN && !defined _AIX)
#  define local_strnlen strnlen
# else
#  ifndef local_strnlen_defined
#   define local_strnlen_defined 1
#  endif
# endif
#endif
#if (!USE_SNPRINTF || (NEED_PRINTF_DIRECTIVE_LS && !defined IN_LIBINTL)) && HAVE_WCHAR_T && (WIDE_CHAR_VERSION || DCHAR_IS_TCHAR)
# if HAVE_WCSLEN
#  define local_wcslen wcslen
# else
/* Solaris 2.5.1 has wcslen() in a separate library libw.so. To avoid
      a dependency towards this library, here is a local substitute.
      Define this substitute only once, even if this file is included
      twice in the same compilation unit.  */
#  ifndef local_wcslen_defined
#   define local_wcslen_defined 1
#  endif
# endif
#endif
#if !USE_SNPRINTF && HAVE_WCHAR_T && WIDE_CHAR_VERSION
# if HAVE_WCSNLEN
#  define local_wcsnlen wcsnlen
# else
#  ifndef local_wcsnlen_defined
#   define local_wcsnlen_defined 1
#  endif
# endif
#endif
#if (NEED_PRINTF_DIRECTIVE_A || NEED_PRINTF_LONG_DOUBLE || NEED_PRINTF_INFINITE_LONG_DOUBLE || NEED_PRINTF_DOUBLE || NEED_PRINTF_INFINITE_DOUBLE) && !defined IN_LIBINTL
/* Determine the decimal-point character according to the current locale.  */
# ifndef decimal_point_char_defined
#  define decimal_point_char_defined 1
/* Determine it in a multithread-safe way.  We know nl_langinfo is
     multithread-safe on glibc systems, but is not required to be multithread-
     safe by POSIX.  sprintf(), however, is multithread-safe.  localeconv()
     is rarely multithread-safe.  */
#  if HAVE_NL_LANGINFO && __GLIBC__
#  elif 1
#  else
#  endif
/* The decimal point is always a single byte: either '.' or ','.  */
# endif
#endif
#if NEED_PRINTF_INFINITE_DOUBLE && !NEED_PRINTF_DOUBLE && !defined IN_LIBINTL
/* Equivalent to !isfinite(x) || x == 0, but does not require libm.  */
#endif
#if NEED_PRINTF_INFINITE_LONG_DOUBLE && !NEED_PRINTF_LONG_DOUBLE && !defined IN_LIBINTL
/* Equivalent to !isfinite(x) || x == 0, but does not require libm.  */
#endif
#if (NEED_PRINTF_LONG_DOUBLE || NEED_PRINTF_DOUBLE) && !defined IN_LIBINTL
/* Converting 'long double' to decimal without rare rounding bugs requires
   real bignums.  We use the naming conventions of GNU gmp, but vastly simpler
   (and slower) algorithms.  */
# define GMP_LIMB_BITS 32
# define GMP_TWOLIMB_BITS 64
/* Representation of a bignum >= 0.  */
/* Bits in little-endian order, allocated with malloc().  */
/* Compute the product of two bignums >= 0.
   Return the allocated memory in case of success, NULL in case of memory
   allocation failure.  */
/* Now 0 <= len1 <= len2.  */
/* src1 or src2 is zero.  */
/* Here 1 <= len1 <= len2.  */
/* Normalise.  */
/* Compute the quotient of a bignum a >= 0 and a bignum b > 0.
   a is written as  a = q * b + r  with 0 <= r < b.  q is the quotient, r
   the remainder.
   Finally, round-to-even is performed: If r > b/2 or if r = b/2 and q is odd,
   q is incremented.
   Return the allocated memory in case of success, NULL in case of memory
   allocation failure.  */
/* Algorithm:
     First normalise a and b: a=[a[m-1],...,a[0]], b=[b[n-1],...,b[0]]
     with m>=0 and n>0 (in base beta = 2^GMP_LIMB_BITS).
     If m<n, then q:=0 and r:=a.
     If m>=n=1, perform a single-precision division:
       r:=0, j:=m,
       while j>0 do
         {Here (q[m-1]*beta^(m-1)+...+q[j]*beta^j) * b[0] + r*beta^j =
               = a[m-1]*beta^(m-1)+...+a[j]*beta^j und 0<=r<b[0]<beta}
         j:=j-1, r:=r*beta+a[j], q[j]:=floor(r/b[0]), r:=r-b[0]*q[j].
       Normalise [q[m-1],...,q[0]], yields q.
     If m>=n>1, perform a multiple-precision division:
       We have a/b < beta^(m-n+1).
       s:=intDsize-1-(highest bit in b[n-1]), 0<=s<intDsize.
       Shift a and b left by s bits, copying them. r:=a.
       r=[r[m],...,r[0]], b=[b[n-1],...,b[0]] with b[n-1]>=beta/2.
       For j=m-n,...,0: {Here 0 <= r < b*beta^(j+1).}
         Compute q* :
           q* := floor((r[j+n]*beta+r[j+n-1])/b[n-1]).
           In case of overflow (q* >= beta) set q* := beta-1.
           Compute c2 := ((r[j+n]*beta+r[j+n-1]) - q* * b[n-1])*beta + r[j+n-2]
           and c3 := b[n-2] * q*.
           {We have 0 <= c2 < 2*beta^2, even 0 <= c2 < beta^2 if no overflow
            occurred.  Furthermore 0 <= c3 < beta^2.
            If there was overflow and
            r[j+n]*beta+r[j+n-1] - q* * b[n-1] >= beta, i.e. c2 >= beta^2,
            the next test can be skipped.}
           While c3 > c2, {Here 0 <= c2 < c3 < beta^2}
             Put q* := q* - 1, c2 := c2 + b[n-1]*beta, c3 := c3 - b[n-2].
           If q* > 0:
             Put r := r - b * q* * beta^j. In detail:
               [r[n+j],...,r[j]] := [r[n+j],...,r[j]] - q* * [b[n-1],...,b[0]].
               hence: u:=0, for i:=0 to n-1 do
                              u := u + q* * b[i],
                              r[j+i]:=r[j+i]-(u mod beta) (+ beta, if carry),
                              u:=u div beta (+ 1, if carry in subtraction)
                      r[n+j]:=r[n+j]-u.
               {Since always u = (q* * [b[i-1],...,b[0]] div beta^i) + 1
                               < q* + 1 <= beta,
                the carry u does not overflow.}
             If a negative carry occurs, put q* := q* - 1
               and [r[n+j],...,r[j]] := [r[n+j],...,r[j]] + [0,b[n-1],...,b[0]].
         Set q[j] := q*.
       Normalise [q[m-n],..,q[0]]; this yields the quotient q.
       Shift [r[n-1],...,r[0]] right by s bits and normalise; this yields the
       rest r.
       The room for q[j] can be allocated at the memory location of r[n+j].
     Finally, round-to-even:
       Shift r left by 1 bit.
       If r > b or if r = b and q[0] is odd, q := q+1.
   */
/* Allocate room for a_len+2 digits.
     (Need a_len+1 digits for the real division and 1 more digit for the
     final rounding of q.)  */
/* Normalise a.  */
/* Normalise b.  */
/* Division by zero.  */
/* Here m = a_len >= 0 and n = b_len > 0.  */
/* m<n: trivial case.  q=0, r := copy of a.  */
/* n=1: single precision division.
	 beta^(m-1) <= a < beta^m  ==>  beta^(m-2) <= a/b < beta^m  */
/* Normalise and store r.  */
/* Normalise q.  */
/* n>1: multiple precision division.
	 beta^(m-1) <= a < beta^m, beta^(n-1) <= b < beta^n  ==>
	 beta^(m-n-1) <= a/b < beta^(m-n+1).  */
/* Determine s.  */
/* = b[n-1], > 0 */
/* 0 <= s < GMP_LIMB_BITS.
	 Copy b, shifting it left by s bits.  */
/* accu must be zero, since that was how s was determined.  */
/* Copy a, shifting it left by s bits, yields r.
	 Memory layout:
	 At the beginning: r = roomptr[0..a_len],
	 at the end: r = roomptr[0..b_len-1], q = roomptr[b_len..a_len]  */
/* q will have m-n+1 limbs */
/* m-n */
/* b[n-1] */
/* b[n-2] */
/* b[n-1]*beta+b[n-2] */
/* Division loop, traversed m-n+1 times.
	   j counts down, b is unchanged, beta/2 <= b[n-1] < beta.  */
/* r[j+n] < b[n-1] ? */
/* Divide r[j+n]*beta+r[j+n-1] by b[n-1], no overflow.  */
/* Overflow, hence r[j+n]*beta+r[j+n-1] >= beta*b[n-1].  */
/* q* = beta-1 */
/* Test whether r[j+n]*beta+r[j+n-1] - (beta-1)*b[n-1] >= beta
		   <==> r[j+n]*beta+r[j+n-1] + b[n-1] >= beta*b[n-1]+beta
		   <==> b[n-1] < floor((r[j+n]*beta+r[j+n-1]+b[n-1])/beta)
		        {<= beta !}.
		   If yes, jump directly to the subtraction loop.
		   (Otherwise, r[j+n]*beta+r[j+n-1] - (beta-1)*b[n-1] < beta
		    <==> floor((r[j+n]*beta+r[j+n-1]+b[n-1])/beta) = b[n-1] ) */
/* r[j+n] >= b[n-1]+1 or
		     r[j+n] = b[n-1] and the addition r[j+n-1]+b[n-1] gives a
		     carry.  */
/* q_star = q*,
	       c1 = (r[j+n]*beta+r[j+n-1]) - q* * b[n-1] (>=0, <beta).  */
/* c1*beta+r[j+n-2] */
/* b[n-2] * q* */
/* While c2 < c3, increase c2 and decrease c3.
		 Consider c3-c2.  While it is > 0, decrease it by
		 b[n-1]*beta+b[n-2].  Because of b[n-1]*beta+b[n-2] >= beta^2/2
		 this can happen only twice.  */
/* q* := q* - 1 */
/* q* := q* - 1 */
/* Subtract r := r - b * q* * beta^j.  */
/* Here 0 <= carry <= q*.  */
/* Here 0 <= carry <= beta*q* + beta-1.  */
/* <= q* */
/* Subtract cr from r_ptr[j + b_len], then forget about
		   r_ptr[j + b_len].  */
/* Subtraction gave a carry.  */
/* q* := q* - 1 */
/* Add b back.  */
/* Forget about the carry and about r[j+n].  */
/* q* is determined.  Store it as q[j].  */
/* Normalise q.  */
# if 0 /* Not needed here, since we need r only to compare it with b/2, and
	  b is shifted left by s bits.  */
/* Shift r right by s bits.  */
# endif
/* Normalise r.  */
/* Compare r << 1 with b.  */
/* q is odd.  */
/* Convert a bignum a >= 0, multiplied with 10^extra_zeroes, to decimal
   representation.
   Destroys the contents of a.
   Return the allocated memory - containing the decimal digits in low-to-high
   order, terminated with a NUL character - in case of success, NULL in case
   of memory allocation failure.  */
/* 0.03345 is slightly larger than log(2)/(9*log(10)).  */
/* Divide a by 10^9, in-place.  */
/* Store the remainder as 9 decimal digits.  */
/* Normalize a.  */
/* Remove leading zeroes.  */
/* But keep at least one zero.  */
/* Terminate the string.  */
# if NEED_PRINTF_LONG_DOUBLE
/* Assuming x is finite and >= 0:
   write x as x = 2^e * m, where m is a bignum.
   Return the allocated memory in case of success, NULL in case of memory
   allocation failure.  */
/* Allocate memory for result.  */
/* Split into exponential part and mantissa.  */
/* x = 2^exp * y = 2^(exp - LDBL_MANT_BIT) * (y * LDBL_MANT_BIT), and the
     latter is an integer.  */
/* Convert the mantissa (y * LDBL_MANT_BIT) to a sequence of limbs.
     I'm not sure whether it's safe to cast a 'long double' value between
     2^31 and 2^32 to 'unsigned int', therefore play safe and cast only
     'long double' values between 0 and 2^16 (to 'unsigned int' or 'int',
     doesn't matter).  */
#  if (LDBL_MANT_BIT % GMP_LIMB_BITS) != 0
#   if (LDBL_MANT_BIT % GMP_LIMB_BITS) > GMP_LIMB_BITS / 2
#   else
#   endif
#  endif
#if 0 /* On FreeBSD 6.1/x86, 'long double' numbers sometimes have excess
         precision.  */
#endif
/* Normalise.  */
# endif
# if NEED_PRINTF_DOUBLE
/* Assuming x is finite and >= 0:
   write x as x = 2^e * m, where m is a bignum.
   Return the allocated memory in case of success, NULL in case of memory
   allocation failure.  */
/* Allocate memory for result.  */
/* Split into exponential part and mantissa.  */
/* x = 2^exp * y = 2^(exp - DBL_MANT_BIT) * (y * DBL_MANT_BIT), and the
     latter is an integer.  */
/* Convert the mantissa (y * DBL_MANT_BIT) to a sequence of limbs.
     I'm not sure whether it's safe to cast a 'double' value between
     2^31 and 2^32 to 'unsigned int', therefore play safe and cast only
     'double' values between 0 and 2^16 (to 'unsigned int' or 'int',
     doesn't matter).  */
#  if (DBL_MANT_BIT % GMP_LIMB_BITS) != 0
#   if (DBL_MANT_BIT % GMP_LIMB_BITS) > GMP_LIMB_BITS / 2
#   else
#   endif
#  endif
/* Normalise.  */
# endif
/* Assuming x = 2^e * m is finite and >= 0, and n is an integer:
   Returns the decimal representation of round (x * 10^n).
   Return the allocated memory - containing the decimal digits in low-to-high
   order, terminated with a NUL character - in case of success, NULL in case
   of memory allocation failure.  */
/* x = 2^e * m, hence
     y = round (2^e * 10^n * m) = round (2^(e+n) * 5^n * m)
       = round (2^s * 5^n * m).  */
/* Factor out a common power of 10 if possible.  */
/* Here y = round (2^s * 5^n * m) * 10^extra_zeroes.
     Before converting to decimal, we need to compute
     z = round (2^s * 5^n * m).  */
/* Compute 5^|n|, possibly shifted by |s| bits if n and s have the same
     sign.  2.322 is slightly larger than log(5)/log(2).  */
/* Initialize with 1.  */
/* Multiply with 5^|n|.  */
/* Multiply with 2^|s|.  */
/* Multiply m with pow5.  No division needed.  */
/* Divide m by pow5 and round.  */
/* n >= 0, s < 0.
	     Multiply m with pow5, then divide by 2^|s|.  */
/* Construct 2^|s|.  */
/* n < 0, s > 0.
	     Multiply m with 2^s, then divide by pow5.  */
/* Here y = round (x * 10^n) = z * 10^extra_zeroes.  */
# if NEED_PRINTF_LONG_DOUBLE
/* Assuming x is finite and >= 0, and n is an integer:
   Returns the decimal representation of round (x * 10^n).
   Return the allocated memory - containing the decimal digits in low-to-high
   order, terminated with a NUL character - in case of success, NULL in case
   of memory allocation failure.  */
# endif
# if NEED_PRINTF_DOUBLE
/* Assuming x is finite and >= 0, and n is an integer:
   Returns the decimal representation of round (x * 10^n).
   Return the allocated memory - containing the decimal digits in low-to-high
   order, terminated with a NUL character - in case of success, NULL in case
   of memory allocation failure.  */
# endif
# if NEED_PRINTF_LONG_DOUBLE
/* Assuming x is finite and > 0:
   Return an approximation for n with 10^n <= x < 10^(n+1).
   The approximation is usually the right n, but may be off by 1 sometimes.  */
/* Split into exponential part and mantissa.  */
/* Compute an approximation for l = log2(x) = exp + log2(y).  */
/* Now 0.95 <= z <= 1.01.  */
/* log2(1-z) = 1/log(2) * (- z - z^2/2 - z^3/3 - z^4/4 - ...)
     Four terms are enough to get an approximation with error < 10^-7.  */
/* Finally multiply with log(2)/log(10), yields an approximation for
     log10(x).  */
/* Round down to the next integer.  */
# endif
# if NEED_PRINTF_DOUBLE
/* Assuming x is finite and > 0:
   Return an approximation for n with 10^n <= x < 10^(n+1).
   The approximation is usually the right n, but may be off by 1 sometimes.  */
/* Split into exponential part and mantissa.  */
/* Compute an approximation for l = log2(x) = exp + log2(y).  */
/* Now 0.95 <= z <= 1.01.  */
/* log2(1-z) = 1/log(2) * (- z - z^2/2 - z^3/3 - z^4/4 - ...)
     Four terms are enough to get an approximation with error < 10^-7.  */
/* Finally multiply with log(2)/log(10), yields an approximation for
     log10(x).  */
/* Round down to the next integer.  */
# endif
/* Tests whether a string of digits consists of exactly PRECISION zeroes and
   a single '1' digit.  */
#endif

char *vasnprintf(char *resultbuf,size_t *lengthp,const char *format,va_list args)
{
  char_directives d;
  arguments a;
  if (printf_parse(format,&d,&a) < 0) 
/* errno is already set.  */
    return 0;
#define CLEANUP() \
  free (d.dir);								\
  if (a.arg)								\
    free (a.arg);
  if (printf_fetchargs(args,&a) < 0) {
    free(d.dir);
    if (a.arg != 0) 
      free(a.arg);;
     *__errno_location() = 22;
    return 0;
  }
{
    size_t buf_neededlength;
    char *buf;
    char *buf_malloced;
    const char *cp;
    size_t i;
    char_directive *dp;
/* Output string accumulator.  */
    char *result;
    size_t allocated;
    size_t length;
/* Allocate a small buffer that will hold a directive passed to
       sprintf or snprintf.  */
    buf_neededlength = xsum4(7,d.max_width_length,d.max_precision_length,6);
#if HAVE_ALLOCA
    if (buf_neededlength < 4000 / sizeof(char )) {
      buf = ((char *)(__builtin_alloca((buf_neededlength * sizeof(char )))));
      buf_malloced = ((char *)((void *)0));
    }
    else 
#endif
{
      size_t buf_memsize = ((buf_neededlength <= 18446744073709551615UL / sizeof(char ))?(((size_t )buf_neededlength) * sizeof(char )) : 18446744073709551615UL);
      if (buf_memsize == 18446744073709551615UL) 
        goto out_of_memory_1;
      buf = ((char *)(malloc(buf_memsize)));
      if (buf == ((char *)((void *)0))) 
        goto out_of_memory_1;
      buf_malloced = buf;
    }
    if (resultbuf != ((char *)((void *)0))) {
      result = resultbuf;
      allocated =  *lengthp;
    }
    else {
      result = ((char *)((void *)0));
      allocated = 0;
    }
    length = 0;
{
/* Invariants:
       result is either == resultbuf or == NULL or malloc-allocated.
       If length > 0, then result != NULL.  */
/* Ensures that allocated >= needed.  Aborts through a jump to
       out_of_memory if needed is SIZE_MAX or otherwise too big.  */
#define ENSURE_ALLOCATION(needed) \
    if ((needed) > allocated)						     \
      {									     \
	size_t memory_size;						     \
	DCHAR_T *memory;						     \
									     \
	allocated = (allocated > 0 ? xtimes (allocated, 2) : 12);	     \
	if ((needed) > allocated)					     \
	  allocated = (needed);						     \
	memory_size = xtimes (allocated, sizeof (DCHAR_T));		     \
	if (size_overflow_p (memory_size))				     \
	  goto out_of_memory;						     \
	if (result == resultbuf || result == NULL)			     \
	  memory = (DCHAR_T *) malloc (memory_size);			     \
	else								     \
	  memory = (DCHAR_T *) realloc (result, memory_size);		     \
	if (memory == NULL)						     \
	  goto out_of_memory;						     \
	if (result == resultbuf && length > 0)				     \
	  DCHAR_CPY (memory, result, length);				     \
	result = memory;						     \
      }
      for (((((cp = format) , (i = 0))) , (dp = (d.dir + 0))); ; ((((cp = (dp -> dir_end)) , i++)) , dp++)) {
        if (cp != (dp -> dir_start)) {
          size_t n = ((dp -> dir_start) - cp);
          size_t augmented_length = xsum(length,n);
          if (augmented_length > allocated) {
            size_t memory_size;
            char *memory;
            allocated = ((allocated > 0)?(((allocated <= 18446744073709551615UL / 2)?(((size_t )allocated) * 2) : 18446744073709551615UL)) : 12);
            if (augmented_length > allocated) 
              allocated = augmented_length;
            memory_size = (((allocated <= 18446744073709551615UL / sizeof(char ))?(((size_t )allocated) * sizeof(char )) : 18446744073709551615UL));
            if (memory_size == 18446744073709551615UL) 
              goto out_of_memory;
            if ((result == resultbuf) || (result == ((char *)((void *)0)))) 
              memory = ((char *)(malloc(memory_size)));
            else 
              memory = ((char *)(realloc(result,memory_size)));
            if (memory == ((char *)((void *)0))) 
              goto out_of_memory;
            if ((result == resultbuf) && (length > 0)) 
              memcpy(memory,result,length);
            result = memory;
          };
/* This copies a piece of FCHAR_T[] into a DCHAR_T[].  Here we
	       need that the format string contains only ASCII characters
	       if FCHAR_T and DCHAR_T are not the same type.  */
          if (1) {
            memcpy((result + length),((const char *)cp),n);
            length = augmented_length;
          }
          else {
            do 
              result[length++] = ((unsigned char )( *(cp++)));while (--n > 0);
          }
        }
        if (i == d.count) 
          break; 
/* Execute a single directive.  */
        if ((dp -> conversion) == '%') {
          size_t augmented_length;
          if (!((dp -> arg_index) == (~((size_t )0)))) 
            abort();
          augmented_length = xsum(length,1);
          if (augmented_length > allocated) {
            size_t memory_size;
            char *memory;
            allocated = ((allocated > 0)?(((allocated <= 18446744073709551615UL / 2)?(((size_t )allocated) * 2) : 18446744073709551615UL)) : 12);
            if (augmented_length > allocated) 
              allocated = augmented_length;
            memory_size = (((allocated <= 18446744073709551615UL / sizeof(char ))?(((size_t )allocated) * sizeof(char )) : 18446744073709551615UL));
            if (memory_size == 18446744073709551615UL) 
              goto out_of_memory;
            if ((result == resultbuf) || (result == ((char *)((void *)0)))) 
              memory = ((char *)(malloc(memory_size)));
            else 
              memory = ((char *)(realloc(result,memory_size)));
            if (memory == ((char *)((void *)0))) 
              goto out_of_memory;
            if ((result == resultbuf) && (length > 0)) 
              memcpy(memory,result,length);
            result = memory;
          };
          result[length] = '%';
          length = augmented_length;
        }
        else {
          if (!((dp -> arg_index) != (~((size_t )0)))) 
            abort();
          if ((dp -> conversion) == 'n') {
            switch(a.arg[dp -> arg_index].type){
              case TYPE_COUNT_SCHAR_POINTER:
{
                 *a.arg[dp -> arg_index].a.a_count_schar_pointer = length;
                break; 
              }
              case TYPE_COUNT_SHORT_POINTER:
{
                 *a.arg[dp -> arg_index].a.a_count_short_pointer = length;
                break; 
              }
              case TYPE_COUNT_INT_POINTER:
{
                 *a.arg[dp -> arg_index].a.a_count_int_pointer = length;
                break; 
              }
              case TYPE_COUNT_LONGINT_POINTER:
{
                 *a.arg[dp -> arg_index].a.a_count_longint_pointer = length;
                break; 
              }
#if HAVE_LONG_LONG_INT
              case TYPE_COUNT_LONGLONGINT_POINTER:
{
                 *a.arg[dp -> arg_index].a.a_count_longlongint_pointer = length;
                break; 
              }
#endif
              default:
{
                abort();
              }
            }
          }
          else 
#if ENABLE_UNISTDIO
/* The unistdio extensions.  */
/* "A negative field width is taken as a '-' flag
			        followed by a positive field width."  */
/* "A negative precision is taken as if the precision
			    were omitted."  */
/* Use only PRECISION characters, from the left.  */
/* Use the entire string, and count the number of
			     characters.  */
/* Use the entire string.  */
/* The number of characters doesn't matter.  */
# if DCHAR_IS_UINT8_T
# else
/* Convert.  */
#  if DCHAR_IS_TCHAR
/* Convert from UTF-8 to locale encoding.  */
#  else
/* Convert from UTF-8 to UTF-16/UTF-32.  */
#  endif
# endif
/* Use only PRECISION characters, from the left.  */
/* Use the entire string, and count the number of
			     characters.  */
/* Use the entire string.  */
/* The number of characters doesn't matter.  */
# if DCHAR_IS_UINT16_T
# else
/* Convert.  */
#  if DCHAR_IS_TCHAR
/* Convert from UTF-16 to locale encoding.  */
#  else
/* Convert from UTF-16 to UTF-8/UTF-32.  */
#  endif
# endif
/* Use only PRECISION characters, from the left.  */
/* Use the entire string, and count the number of
			     characters.  */
/* Use the entire string.  */
/* The number of characters doesn't matter.  */
# if DCHAR_IS_UINT32_T
# else
/* Convert.  */
#  if DCHAR_IS_TCHAR
/* Convert from UTF-32 to locale encoding.  */
#  else
/* Convert from UTF-32 to UTF-8/UTF-16.  */
#  endif
# endif
#endif
#if (!USE_SNPRINTF || (NEED_PRINTF_DIRECTIVE_LS && !defined IN_LIBINTL)) && HAVE_WCHAR_T
# if WIDE_CHAR_VERSION
# else
# endif
/* The normal handling of the 's' directive below requires
		   allocating a temporary buffer.  The determination of its
		   length (tmp_length), in the case when a precision is
		   specified, below requires a conversion between a char[]
		   string and a wchar_t[] wide string.  It could be done, but
		   we have no guarantee that the implementation of sprintf will
		   use the exactly same algorithm.  Without this guarantee, it
		   is possible to have buffer overrun bugs.  In order to avoid
		   such bugs, we implement the entire processing of the 's'
		   directive ourselves.  */
/* "A negative field width is taken as a '-' flag
			        followed by a positive field width."  */
/* "A negative precision is taken as if the precision
			    were omitted."  */
# if WIDE_CHAR_VERSION
/* %s in vasnwprintf.  See the specification of fwprintf.  */
/* Use only as many bytes as needed to produce PRECISION
			 wide characters, from the left.  */
#  if HAVE_MBRTOWC
#  endif
#  if HAVE_MBRTOWC
#  else
#  endif
/* Found the terminating NUL.  */
/* Invalid or incomplete multibyte character.  */
/* Use the entire string, and count the number of wide
			 characters.  */
#  if HAVE_MBRTOWC
#  endif
#  if HAVE_MBRTOWC
#  else
#  endif
/* Found the terminating NUL.  */
/* Invalid or incomplete multibyte character.  */
/* Use the entire string.  */
/* The number of characters doesn't matter.  */
/* We know the number of wide characters in advance.  */
#  if HAVE_MBRTOWC
#  endif
#  if HAVE_MBRTOWC
#  else
#  endif
/* mbrtowc not consistent with mbrlen, or mbtowc
			       not consistent with mblen.  */
#  if HAVE_MBRTOWC
#  endif
#  if HAVE_MBRTOWC
#  else
#  endif
/* mbrtowc not consistent with mbrlen, or mbtowc
			       not consistent with mblen.  */
# else
/* %ls in vasnprintf.  See the specification of fprintf.  */
#  if !DCHAR_IS_TCHAR
/* This code assumes that TCHAR_T is 'char'.  */
#  endif
/* Use only as many wide characters as needed to produce
			 at most PRECISION bytes, from the left.  */
#  if HAVE_WCRTOMB
#  endif
/* Assume MB_CUR_MAX <= 64.  */
/* Found the terminating null wide character.  */
#  if HAVE_WCRTOMB
#  else
#  endif
/* Cannot convert.  */
#  if DCHAR_IS_TCHAR
#  else
#  endif
/* Use the entire string, and count the number of
			 bytes.  */
#  if HAVE_WCRTOMB
#  endif
/* Assume MB_CUR_MAX <= 64.  */
/* Found the terminating null wide character.  */
#  if HAVE_WCRTOMB
#  else
#  endif
/* Cannot convert.  */
#  if DCHAR_IS_TCHAR
/* Use the entire string.  */
/* The number of bytes doesn't matter.  */
#  endif
#  if !DCHAR_IS_TCHAR
/* Convert the string into a piece of temporary memory.  */
#   if HAVE_WCRTOMB
#   endif
/* Assume MB_CUR_MAX <= 64.  */
#   if HAVE_WCRTOMB
#   else
#   endif
/* Inconsistency.  */
/* Convert from TCHAR_T[] to DCHAR_T[].  */
#  endif
#  if ENABLE_UNISTDIO
/* Outside POSIX, it's preferrable to compare the width
			 against the number of _characters_ of the converted
			 value.  */
#  else
/* The width is compared against the number of _bytes_
			 of the converted value, says POSIX.  */
#  endif
/* w doesn't matter.  */
#  if DCHAR_IS_TCHAR
/* We know the number of bytes in advance.  */
#   if HAVE_WCRTOMB
#   endif
/* Assume MB_CUR_MAX <= 64.  */
#   if HAVE_WCRTOMB
#   else
#   endif
/* Inconsistency.  */
#   if HAVE_WCRTOMB
#   endif
/* Assume MB_CUR_MAX <= 64.  */
#   if HAVE_WCRTOMB
#   else
#   endif
/* Inconsistency.  */
#  else
#  endif
# endif
#endif
#if (NEED_PRINTF_DIRECTIVE_A || NEED_PRINTF_LONG_DOUBLE || NEED_PRINTF_DOUBLE) && !defined IN_LIBINTL
# if !(NEED_PRINTF_DIRECTIVE_A || (NEED_PRINTF_LONG_DOUBLE && NEED_PRINTF_DOUBLE))
#  if NEED_PRINTF_DOUBLE
#  endif
#  if NEED_PRINTF_LONG_DOUBLE
#  endif
# endif
/* "A negative field width is taken as a '-' flag
			        followed by a positive field width."  */
/* "A negative precision is taken as if the precision
			    were omitted."  */
/* Allocate a temporary buffer of sufficient size.  */
/* decimal -> hexadecimal */
/* turn floor into ceil */
/* decimal -> hexadecimal */
/* turn floor into ceil */
/* Account for sign, decimal point etc. */
/* account for trailing NUL */
/* Overflow, would lead to out of memory.  */
/* Out of memory.  */
# if NEED_PRINTF_DIRECTIVE_A || NEED_PRINTF_LONG_DOUBLE
/* arg < 0.0L or negative zero */
/* Round the mantissa.  */
/* This loop terminates because we assume
				     that FLT_RADIX is a power of 2.  */
#  if WIDE_CHAR_VERSION
#  else
#  endif
# else
# endif
# if NEED_PRINTF_DIRECTIVE_A || NEED_PRINTF_DOUBLE
/* arg < 0.0 or negative zero */
/* Round the mantissa.  */
/* This loop terminates because we assume
				     that FLT_RADIX is a power of 2.  */
#  if WIDE_CHAR_VERSION
#  else
#  endif
# else
# endif
/* The generated string now extends from tmp to p, with the
		   zero padding insertion point being at pad_ptr.  */
/* Pad with spaces on the right.  */
/* Pad with zeroes.  */
/* Pad with spaces on the left.  */
/* tmp_length was incorrectly calculated - fix the
		       code above!  */
/* Make room for the result.  */
/* Append the result.  */
#endif
#if (NEED_PRINTF_INFINITE_DOUBLE || NEED_PRINTF_DOUBLE || NEED_PRINTF_INFINITE_LONG_DOUBLE || NEED_PRINTF_LONG_DOUBLE) && !defined IN_LIBINTL
# if NEED_PRINTF_DOUBLE
# elif NEED_PRINTF_INFINITE_DOUBLE
/* The systems (mingw) which produce wrong output
				for Inf, -Inf, and NaN also do so for -0.0.
				Therefore we treat this case here as well.  */
# endif
# if NEED_PRINTF_LONG_DOUBLE
# elif NEED_PRINTF_INFINITE_LONG_DOUBLE
/* Some systems produce wrong output for Inf,
				-Inf, and NaN.  Some systems in this category
				(IRIX 5.3) also do so for -0.0.  Therefore we
				treat this case here as well.  */
# endif
# if (NEED_PRINTF_DOUBLE || NEED_PRINTF_INFINITE_DOUBLE) && (NEED_PRINTF_LONG_DOUBLE || NEED_PRINTF_INFINITE_LONG_DOUBLE)
# endif
/* "A negative field width is taken as a '-' flag
			        followed by a positive field width."  */
/* "A negative precision is taken as if the precision
			    were omitted."  */
/* POSIX specifies the default precision to be 6 for %f, %F,
		   %e, %E, but not for %g, %G.  Implementations appear to use
		   the same default precision also for %g, %G.  But for %a, %A,
		   the default precision is 0.  */
/* Allocate a temporary buffer of sufficient size.  */
# if NEED_PRINTF_DOUBLE && NEED_PRINTF_LONG_DOUBLE
# elif NEED_PRINTF_INFINITE_DOUBLE && NEED_PRINTF_LONG_DOUBLE
# elif NEED_PRINTF_LONG_DOUBLE
# elif NEED_PRINTF_DOUBLE
# else
# endif
# if NEED_PRINTF_LONG_DOUBLE
#  if NEED_PRINTF_DOUBLE || NEED_PRINTF_INFINITE_DOUBLE
#  endif
/* arg is finite and nonzero.  */
# endif
# if NEED_PRINTF_DOUBLE
#  if NEED_PRINTF_LONG_DOUBLE || NEED_PRINTF_INFINITE_LONG_DOUBLE
#  endif
/* arg is finite and nonzero.  */
# endif
/* Account for sign, decimal point etc. */
/* account for trailing NUL */
/* Overflow, would lead to out of memory.  */
/* Out of memory.  */
# if NEED_PRINTF_LONG_DOUBLE || NEED_PRINTF_INFINITE_LONG_DOUBLE
#  if NEED_PRINTF_DOUBLE || NEED_PRINTF_INFINITE_DOUBLE
#  endif
/* arg < 0.0L or negative zero */
#  if NEED_PRINTF_LONG_DOUBLE
/* Here ndigits <= precision.  */
/* arg > 0.0L.  */
/* The exponent was not guessed
					     precisely enough.  */
/* None of two values of exponent is
					     the right one.  Prevent an endless
					     loop.  */
/* Here ndigits = precision+1.  */
/* Maybe the exponent guess was too high
					   and a smaller exponent can be reached
					   by turning a 10...0 into 9...9x.  */
/* Here ndigits = precision+1.  */
/* 'e' or 'E' */
#   if WIDE_CHAR_VERSION
#   else
#   endif
/* precision >= 1.  */
/* The exponent is 0, >= -4, < precision.
				     Use fixed-point notation.  */
/* Number of trailing zeroes that have to be
				       dropped.  */
/* arg > 0.0L.  */
/* The exponent was not guessed
					     precisely enough.  */
/* None of two values of exponent is
					     the right one.  Prevent an endless
					     loop.  */
/* Here ndigits = precision.  */
/* Maybe the exponent guess was too high
					   and a smaller exponent can be reached
					   by turning a 10...0 into 9...9x.  */
/* Here ndigits = precision.  */
/* Determine the number of trailing zeroes
				       that have to be dropped.  */
/* The exponent is now determined.  */
/* Fixed-point notation:
					   max(exponent,0)+1 digits, then the
					   decimal point, then the remaining
					   digits without trailing zeroes.  */
/* Note: count <= precision = ndigits.  */
/* Exponential notation.  */
/* 'e' or 'E' */
#   if WIDE_CHAR_VERSION
#   else
#   endif
#  else
/* arg is finite.  */
/* 'e' or 'E' */
#  endif
#  if NEED_PRINTF_DOUBLE || NEED_PRINTF_INFINITE_DOUBLE
#  endif
# endif
# if NEED_PRINTF_DOUBLE || NEED_PRINTF_INFINITE_DOUBLE
/* arg < 0.0 or negative zero */
#  if NEED_PRINTF_DOUBLE
/* Here ndigits <= precision.  */
/* arg > 0.0.  */
/* The exponent was not guessed
					     precisely enough.  */
/* None of two values of exponent is
					     the right one.  Prevent an endless
					     loop.  */
/* Here ndigits = precision+1.  */
/* Maybe the exponent guess was too high
					   and a smaller exponent can be reached
					   by turning a 10...0 into 9...9x.  */
/* Here ndigits = precision+1.  */
/* 'e' or 'E' */
#   if WIDE_CHAR_VERSION
/* Produce the same number of exponent digits
				       as the native printf implementation.  */
#    if (defined _WIN32 || defined __WIN32__) && ! defined __CYGWIN__
#    else
#    endif
#   else
/* Produce the same number of exponent digits
				       as the native printf implementation.  */
#    if (defined _WIN32 || defined __WIN32__) && ! defined __CYGWIN__
#    else
#    endif
#   endif
/* precision >= 1.  */
/* The exponent is 0, >= -4, < precision.
				     Use fixed-point notation.  */
/* Number of trailing zeroes that have to be
				       dropped.  */
/* arg > 0.0.  */
/* The exponent was not guessed
					     precisely enough.  */
/* None of two values of exponent is
					     the right one.  Prevent an endless
					     loop.  */
/* Here ndigits = precision.  */
/* Maybe the exponent guess was too high
					   and a smaller exponent can be reached
					   by turning a 10...0 into 9...9x.  */
/* Here ndigits = precision.  */
/* Determine the number of trailing zeroes
				       that have to be dropped.  */
/* The exponent is now determined.  */
/* Fixed-point notation:
					   max(exponent,0)+1 digits, then the
					   decimal point, then the remaining
					   digits without trailing zeroes.  */
/* Note: count <= precision = ndigits.  */
/* Exponential notation.  */
/* 'e' or 'E' */
#   if WIDE_CHAR_VERSION
/* Produce the same number of exponent digits
					       as the native printf implementation.  */
#    if (defined _WIN32 || defined __WIN32__) && ! defined __CYGWIN__
#    else
#    endif
#   else
/* Produce the same number of exponent digits
					       as the native printf implementation.  */
#    if (defined _WIN32 || defined __WIN32__) && ! defined __CYGWIN__
#    else
#    endif
#   endif
#  else
/* arg is finite.  */
/* 'e' or 'E' */
/* Produce the same number of exponent digits as
				   the native printf implementation.  */
#   if (defined _WIN32 || defined __WIN32__) && ! defined __CYGWIN__
#   endif
#  endif
# endif
/* The generated string now extends from tmp to p, with the
		   zero padding insertion point being at pad_ptr.  */
/* Pad with spaces on the right.  */
/* Pad with zeroes.  */
/* Pad with spaces on the left.  */
/* tmp_length was incorrectly calculated - fix the
		       code above!  */
/* Make room for the result.  */
/* Append the result.  */
#endif
{
            arg_type type = a.arg[dp -> arg_index].type;
            int flags = (dp -> flags);
#if !USE_SNPRINTF || !DCHAR_IS_TCHAR || ENABLE_UNISTDIO || NEED_PRINTF_FLAG_LEFTADJUST || NEED_PRINTF_FLAG_ZERO || NEED_PRINTF_UNBOUNDED_PRECISION
#endif
#if !USE_SNPRINTF || NEED_PRINTF_UNBOUNDED_PRECISION
#endif
#if NEED_PRINTF_UNBOUNDED_PRECISION
#else
#		define prec_ourselves 0
#endif
#if NEED_PRINTF_FLAG_LEFTADJUST
#		define pad_ourselves 1
#elif !DCHAR_IS_TCHAR || ENABLE_UNISTDIO || NEED_PRINTF_FLAG_ZERO || NEED_PRINTF_UNBOUNDED_PRECISION
#else
#		define pad_ourselves 0
#endif
            char *fbp;
            unsigned int prefix_count;
            int prefixes[2UL];
#if !USE_SNPRINTF
#endif
#if !USE_SNPRINTF || !DCHAR_IS_TCHAR || ENABLE_UNISTDIO || NEED_PRINTF_FLAG_LEFTADJUST || NEED_PRINTF_FLAG_ZERO || NEED_PRINTF_UNBOUNDED_PRECISION
/* "A negative field width is taken as a '-' flag
			        followed by a positive field width."  */
#endif
#if !USE_SNPRINTF || NEED_PRINTF_UNBOUNDED_PRECISION
/* "A negative precision is taken as if the precision
			    were omitted."  */
#endif
/* Decide whether to handle the precision ourselves.  */
#if NEED_PRINTF_UNBOUNDED_PRECISION
#endif
/* Decide whether to perform the padding ourselves.  */
#if !NEED_PRINTF_FLAG_LEFTADJUST && (!DCHAR_IS_TCHAR || ENABLE_UNISTDIO || NEED_PRINTF_FLAG_ZERO || NEED_PRINTF_UNBOUNDED_PRECISION)
# if !DCHAR_IS_TCHAR || ENABLE_UNISTDIO
/* If we need conversion from TCHAR_T[] to DCHAR_T[], we need
		     to perform the padding after this conversion.  Functions
		     with unistdio extensions perform the padding based on
		     character count rather than element count.  */
# endif
# if NEED_PRINTF_FLAG_ZERO
# endif
#endif
#if !USE_SNPRINTF
/* Allocate a temporary buffer of sufficient size for calling
		   sprintf.  */
# if HAVE_LONG_LONG_INT
/* binary -> decimal */
/* turn floor into ceil */
# endif
/* binary -> decimal */
/* turn floor into ceil */
/* binary -> decimal */
/* turn floor into ceil */
/* Multiply by 2, as an estimate for FLAG_GROUP.  */
/* Add 1, to account for a leading sign.  */
# if HAVE_LONG_LONG_INT
/* binary -> octal */
/* turn floor into ceil */
# endif
/* binary -> octal */
/* turn floor into ceil */
/* binary -> octal */
/* turn floor into ceil */
/* Add 1, to account for a leading sign.  */
# if HAVE_LONG_LONG_INT
/* binary -> hexadecimal */
/* turn floor into ceil */
# endif
/* binary -> hexadecimal */
/* turn floor into ceil */
/* binary -> hexadecimal */
/* turn floor into ceil */
/* Add 2, to account for a leading sign or alternate form.  */
/* binary -> decimal */
/* estimate for FLAG_GROUP */
/* turn floor into ceil */
/* sign, decimal point etc. */
/* binary -> decimal */
/* estimate for FLAG_GROUP */
/* turn floor into ceil */
/* sign, decimal point etc. */
/* sign, decimal point, exponent etc. */
/* decimal -> hexadecimal */
/* turn floor into ceil */
/* decimal -> hexadecimal */
/* turn floor into ceil */
/* Account for sign, decimal point etc. */
# if HAVE_WINT_T && !WIDE_CHAR_VERSION
# endif
# if HAVE_WCHAR_T
#  if WIDE_CHAR_VERSION
/* ISO C says about %ls in fwprintf:
			       "If the precision is not specified or is greater
				than the size of the array, the array shall
				contain a null wide character."
			     So if there is a precision, we must not use
			     wcslen.  */
#  else
/* ISO C says about %ls in fprintf:
			       "If a precision is specified, no more than that
				many bytes are written (including shift
				sequences, if any), and the array shall contain
				a null wide character if, to equal the
				multibyte character sequence length given by
				the precision, the function would need to
				access a wide character one past the end of the
				array."
			     So if there is a precision, we must not use
			     wcslen.  */
/* This case has already been handled above.  */
#  endif
# endif
# if WIDE_CHAR_VERSION
/* ISO C says about %s in fwprintf:
			       "If the precision is not specified or is greater
				than the size of the converted array, the
				converted array shall contain a null wide
				character."
			     So if there is a precision, we must not use
			     strlen.  */
/* This case has already been handled above.  */
# else
/* ISO C says about %s in fprintf:
			       "If the precision is not specified or greater
				than the size of the array, the array shall
				contain a null character."
			     So if there is a precision, we must not use
			     strlen.  */
# endif
/* binary -> hexadecimal */
/* turn floor into ceil */
/* account for leading 0x */
# if ENABLE_UNISTDIO
/* Padding considers the number of characters, therefore
			 the number of elements after padding may be
			   > max (tmp_length, width)
			 but is certainly
			   <= tmp_length + width.  */
# else
/* Padding considers the number of elements,
			 says POSIX.  */
# endif
/* account for trailing NUL */
/* Overflow, would lead to out of memory.  */
/* Out of memory.  */
#endif
/* Construct the format string for calling snprintf or
		   sprintf.  */
            fbp = buf;
             *(fbp++) = '%';
#if NEED_PRINTF_FLAG_GROUPING
/* The underlying implementation doesn't support the ' flag.
		   Produce no grouping characters in this case; this is
		   acceptable because the grouping is locale dependent.  */
#else
            if ((flags & 1) != 0) 
               *(fbp++) = '\'';
#endif
            if ((flags & 2) != 0) 
               *(fbp++) = '-';
            if ((flags & 4) != 0) 
               *(fbp++) = '+';
            if ((flags & 8) != 0) 
               *(fbp++) = 32;
            if ((flags & 16) != 0) 
               *(fbp++) = '#';
            if (1) {
              if ((flags & 32) != 0) 
                 *(fbp++) = 48;
              if ((dp -> width_start) != (dp -> width_end)) {
                size_t n = ((dp -> width_end) - (dp -> width_start));
/* The width specification is known to consist only
			   of standard ASCII characters.  */
                if (1) {
                  memcpy(fbp,(dp -> width_start),(n * sizeof(char )));
                  fbp += n;
                }
                else {
                  const char *mp = (dp -> width_start);
                  do 
                     *(fbp++) = ((unsigned char )( *(mp++)));while (--n > 0);
                }
              }
            }
            if (1) {
              if ((dp -> precision_start) != (dp -> precision_end)) {
                size_t n = ((dp -> precision_end) - (dp -> precision_start));
/* The precision specification is known to consist only
			   of standard ASCII characters.  */
                if (1) {
                  memcpy(fbp,(dp -> precision_start),(n * sizeof(char )));
                  fbp += n;
                }
                else {
                  const char *mp = (dp -> precision_start);
                  do 
                     *(fbp++) = ((unsigned char )( *(mp++)));while (--n > 0);
                }
              }
            }
            switch(type){
#if HAVE_LONG_LONG_INT
              case TYPE_LONGLONGINT:
{
              }
              case TYPE_ULONGLONGINT:
{
# if (defined _WIN32 || defined __WIN32__) && ! defined __CYGWIN__
# else
                 *(fbp++) = 'l';
              }
/*FALLTHROUGH*/
# endif
#endif
              case TYPE_LONGINT:
{
              }
              case TYPE_ULONGINT:
{
              }
#if HAVE_WINT_T
              case TYPE_WIDE_CHAR:
{
              }
#endif
#if HAVE_WCHAR_T
              case TYPE_WIDE_STRING:
{
#endif
                 *(fbp++) = 'l';
                break; 
              }
              case TYPE_LONGDOUBLE:
{
                 *(fbp++) = 'L';
                break; 
              }
              default:
{
                break; 
              }
            }
#if NEED_PRINTF_DIRECTIVE_F
#endif
             *fbp = (dp -> conversion);
#if USE_SNPRINTF
# if !(__GLIBC__ > 2 || (__GLIBC__ == 2 && __GLIBC_MINOR__ >= 3) || ((defined _WIN32 || defined __WIN32__) && ! defined __CYGWIN__))
# else
/* On glibc2 systems from glibc >= 2.3 - probably also older
		   ones - we know that snprintf's returns value conforms to
		   ISO C 99: the gl_SNPRINTF_DIRECTIVE_N test passes.
		   Therefore we can avoid using %n in this situation.
		   On glibc2 systems from 2004-10-18 or newer, the use of %n
		   in format strings in writable memory may crash the program
		   (if compiled with _FORTIFY_SOURCE=2), so we should avoid it
		   in this situation.  */
/* On native Win32 systems (such as mingw), we can avoid using
		   %n because:
		     - Although the gl_SNPRINTF_TRUNCATION_C99 test fails,
		       snprintf does not write more than the specified number
		       of bytes. (snprintf (buf, 3, "%d %d", 4567, 89) writes
		       '4', '5', '6' into buf, not '4', '5', '\0'.)
		     - Although the gl_SNPRINTF_RETVAL_C99 test fails, snprintf
		       allows us to recognize the case of an insufficient
		       buffer size: it returns -1 in this case.
		   On native Win32 systems (such as mingw) where the OS is
		   Windows Vista, the use of %n in format strings by default
		   crashes the program. See
		     <http://gcc.gnu.org/ml/gcc/2007-06/msg00122.html> and
		     <http://msdn2.microsoft.com/en-us/library/ms175782(VS.80).aspx>
		   So we should avoid %n in this situation.  */
            fbp[1] = 0;
# endif
#else
#endif
/* Construct the arguments for calling snprintf or sprintf.  */
            prefix_count = 0;
            if (1 && ((dp -> width_arg_index) != (~((size_t )0)))) {
              if (!(a.arg[dp -> width_arg_index].type == TYPE_INT)) 
                abort();
              prefixes[prefix_count++] = a.arg[dp -> width_arg_index].a.a_int;
            }
            if (1 && ((dp -> precision_arg_index) != (~((size_t )0)))) {
              if (!(a.arg[dp -> precision_arg_index].type == TYPE_INT)) 
                abort();
              prefixes[prefix_count++] = a.arg[dp -> precision_arg_index].a.a_int;
            }
#if USE_SNPRINTF
/* The SNPRINTF result is appended after result[0..length].
		   The latter is an array of DCHAR_T; SNPRINTF appends an
		   array of TCHAR_T to it.  This is possible because
		   sizeof (TCHAR_T) divides sizeof (DCHAR_T) and
		   alignof (TCHAR_T) <= alignof (DCHAR_T).  */
# define TCHARS_PER_DCHAR (sizeof (DCHAR_T) / sizeof (TCHAR_T))
/* Ensure that maxlen below will be >= 2.  Needed on BeOS,
		   where an snprintf() with maxlen==1 acts like sprintf().  */
            if (xsum(length,((2 + sizeof(char ) / sizeof(char ) - 1) / (sizeof(char ) / sizeof(char )))) > allocated) {
              size_t memory_size;
              char *memory;
              allocated = ((allocated > 0)?(((allocated <= 18446744073709551615UL / 2)?(((size_t )allocated) * 2) : 18446744073709551615UL)) : 12);
              if (xsum(length,((2 + sizeof(char ) / sizeof(char ) - 1) / (sizeof(char ) / sizeof(char )))) > allocated) 
                allocated = xsum(length,((2 + sizeof(char ) / sizeof(char ) - 1) / (sizeof(char ) / sizeof(char ))));
              memory_size = (((allocated <= 18446744073709551615UL / sizeof(char ))?(((size_t )allocated) * sizeof(char )) : 18446744073709551615UL));
              if (memory_size == 18446744073709551615UL) 
                goto out_of_memory;
              if ((result == resultbuf) || (result == ((char *)((void *)0)))) 
                memory = ((char *)(malloc(memory_size)));
              else 
                memory = ((char *)(realloc(result,memory_size)));
              if (memory == ((char *)((void *)0))) 
                goto out_of_memory;
              if ((result == resultbuf) && (length > 0)) 
                memcpy(memory,result,length);
              result = memory;
            };
/* Prepare checking whether snprintf returns the count
		   via %n.  */
             *(result + length) = 0;
{
#endif
              for (; ; ) {{
                  int count = -1;
#if USE_SNPRINTF
                  int retcount = 0;
                  size_t maxlen = (allocated - length);
/* SNPRINTF can fail if its second argument is
		       > INT_MAX.  */
                  if (maxlen > 2147483647 / (sizeof(char ) / sizeof(char ))) 
                    maxlen = (2147483647 / (sizeof(char ) / sizeof(char )));
                  maxlen = (maxlen * (sizeof(char ) / sizeof(char )));
# define SNPRINTF_BUF(arg) \
		    switch (prefix_count)				    \
		      {							    \
		      case 0:						    \
			retcount = SNPRINTF ((TCHAR_T *) (result + length), \
					     maxlen, buf,		    \
					     arg, &count);		    \
			break;						    \
		      case 1:						    \
			retcount = SNPRINTF ((TCHAR_T *) (result + length), \
					     maxlen, buf,		    \
					     prefixes[0], arg, &count);	    \
			break;						    \
		      case 2:						    \
			retcount = SNPRINTF ((TCHAR_T *) (result + length), \
					     maxlen, buf,		    \
					     prefixes[0], prefixes[1], arg, \
					     &count);			    \
			break;						    \
		      default:						    \
			abort ();					    \
		      }
#else
# define SNPRINTF_BUF(arg) \
		    switch (prefix_count)				    \
		      {							    \
		      case 0:						    \
			count = sprintf (tmp, buf, arg);		    \
			break;						    \
		      case 1:						    \
			count = sprintf (tmp, buf, prefixes[0], arg);	    \
			break;						    \
		      case 2:						    \
			count = sprintf (tmp, buf, prefixes[0], prefixes[1],\
					 arg);				    \
			break;						    \
		      default:						    \
			abort ();					    \
		      }
#endif
                  switch(type){
                    case TYPE_SCHAR:
{
{
                        int arg = a.arg[dp -> arg_index].a.a_schar;
                        switch(prefix_count){
                          case 0:
{
                            retcount = snprintf((result + length),maxlen,buf,arg,&count);
                            break; 
                          }
                          case 1:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],arg,&count);
                            break; 
                          }
                          case 2:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],prefixes[1],arg,&count);
                            break; 
                          }
                          default:
{
                            abort();
                          }
                        };
                      }
                      break; 
                    }
                    case TYPE_UCHAR:
{
{
                        unsigned int arg = a.arg[dp -> arg_index].a.a_uchar;
                        switch(prefix_count){
                          case 0:
{
                            retcount = snprintf((result + length),maxlen,buf,arg,&count);
                            break; 
                          }
                          case 1:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],arg,&count);
                            break; 
                          }
                          case 2:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],prefixes[1],arg,&count);
                            break; 
                          }
                          default:
{
                            abort();
                          }
                        };
                      }
                      break; 
                    }
                    case TYPE_SHORT:
{
{
                        int arg = a.arg[dp -> arg_index].a.a_short;
                        switch(prefix_count){
                          case 0:
{
                            retcount = snprintf((result + length),maxlen,buf,arg,&count);
                            break; 
                          }
                          case 1:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],arg,&count);
                            break; 
                          }
                          case 2:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],prefixes[1],arg,&count);
                            break; 
                          }
                          default:
{
                            abort();
                          }
                        };
                      }
                      break; 
                    }
                    case TYPE_USHORT:
{
{
                        unsigned int arg = a.arg[dp -> arg_index].a.a_ushort;
                        switch(prefix_count){
                          case 0:
{
                            retcount = snprintf((result + length),maxlen,buf,arg,&count);
                            break; 
                          }
                          case 1:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],arg,&count);
                            break; 
                          }
                          case 2:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],prefixes[1],arg,&count);
                            break; 
                          }
                          default:
{
                            abort();
                          }
                        };
                      }
                      break; 
                    }
                    case TYPE_INT:
{
{
                        int arg = a.arg[dp -> arg_index].a.a_int;
                        switch(prefix_count){
                          case 0:
{
                            retcount = snprintf((result + length),maxlen,buf,arg,&count);
                            break; 
                          }
                          case 1:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],arg,&count);
                            break; 
                          }
                          case 2:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],prefixes[1],arg,&count);
                            break; 
                          }
                          default:
{
                            abort();
                          }
                        };
                      }
                      break; 
                    }
                    case TYPE_UINT:
{
{
                        unsigned int arg = a.arg[dp -> arg_index].a.a_uint;
                        switch(prefix_count){
                          case 0:
{
                            retcount = snprintf((result + length),maxlen,buf,arg,&count);
                            break; 
                          }
                          case 1:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],arg,&count);
                            break; 
                          }
                          case 2:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],prefixes[1],arg,&count);
                            break; 
                          }
                          default:
{
                            abort();
                          }
                        };
                      }
                      break; 
                    }
                    case TYPE_LONGINT:
{
{
                        long arg = a.arg[dp -> arg_index].a.a_longint;
                        switch(prefix_count){
                          case 0:
{
                            retcount = snprintf((result + length),maxlen,buf,arg,&count);
                            break; 
                          }
                          case 1:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],arg,&count);
                            break; 
                          }
                          case 2:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],prefixes[1],arg,&count);
                            break; 
                          }
                          default:
{
                            abort();
                          }
                        };
                      }
                      break; 
                    }
                    case TYPE_ULONGINT:
{
{
                        unsigned long arg = a.arg[dp -> arg_index].a.a_ulongint;
                        switch(prefix_count){
                          case 0:
{
                            retcount = snprintf((result + length),maxlen,buf,arg,&count);
                            break; 
                          }
                          case 1:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],arg,&count);
                            break; 
                          }
                          case 2:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],prefixes[1],arg,&count);
                            break; 
                          }
                          default:
{
                            abort();
                          }
                        };
                      }
                      break; 
                    }
#if HAVE_LONG_LONG_INT
                    case TYPE_LONGLONGINT:
{
{
                        long long arg = a.arg[dp -> arg_index].a.a_longlongint;
                        switch(prefix_count){
                          case 0:
{
                            retcount = snprintf((result + length),maxlen,buf,arg,&count);
                            break; 
                          }
                          case 1:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],arg,&count);
                            break; 
                          }
                          case 2:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],prefixes[1],arg,&count);
                            break; 
                          }
                          default:
{
                            abort();
                          }
                        };
                      }
                      break; 
                    }
                    case TYPE_ULONGLONGINT:
{
{
                        unsigned long long arg = a.arg[dp -> arg_index].a.a_ulonglongint;
                        switch(prefix_count){
                          case 0:
{
                            retcount = snprintf((result + length),maxlen,buf,arg,&count);
                            break; 
                          }
                          case 1:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],arg,&count);
                            break; 
                          }
                          case 2:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],prefixes[1],arg,&count);
                            break; 
                          }
                          default:
{
                            abort();
                          }
                        };
                      }
                      break; 
                    }
#endif
                    case TYPE_DOUBLE:
{
{
                        double arg = a.arg[dp -> arg_index].a.a_double;
                        switch(prefix_count){
                          case 0:
{
                            retcount = snprintf((result + length),maxlen,buf,arg,&count);
                            break; 
                          }
                          case 1:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],arg,&count);
                            break; 
                          }
                          case 2:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],prefixes[1],arg,&count);
                            break; 
                          }
                          default:
{
                            abort();
                          }
                        };
                      }
                      break; 
                    }
                    case TYPE_LONGDOUBLE:
{
{
                        long double arg = a.arg[dp -> arg_index].a.a_longdouble;
                        switch(prefix_count){
                          case 0:
{
                            retcount = snprintf((result + length),maxlen,buf,arg,&count);
                            break; 
                          }
                          case 1:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],arg,&count);
                            break; 
                          }
                          case 2:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],prefixes[1],arg,&count);
                            break; 
                          }
                          default:
{
                            abort();
                          }
                        };
                      }
                      break; 
                    }
                    case TYPE_CHAR:
{
{
                        int arg = a.arg[dp -> arg_index].a.a_char;
                        switch(prefix_count){
                          case 0:
{
                            retcount = snprintf((result + length),maxlen,buf,arg,&count);
                            break; 
                          }
                          case 1:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],arg,&count);
                            break; 
                          }
                          case 2:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],prefixes[1],arg,&count);
                            break; 
                          }
                          default:
{
                            abort();
                          }
                        };
                      }
                      break; 
                    }
#if HAVE_WINT_T
                    case TYPE_WIDE_CHAR:
{
{
                        wint_t arg = a.arg[dp -> arg_index].a.a_wide_char;
                        switch(prefix_count){
                          case 0:
{
                            retcount = snprintf((result + length),maxlen,buf,arg,&count);
                            break; 
                          }
                          case 1:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],arg,&count);
                            break; 
                          }
                          case 2:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],prefixes[1],arg,&count);
                            break; 
                          }
                          default:
{
                            abort();
                          }
                        };
                      }
                      break; 
                    }
#endif
                    case TYPE_STRING:
{
{
                        const char *arg = a.arg[dp -> arg_index].a.a_string;
                        switch(prefix_count){
                          case 0:
{
                            retcount = snprintf((result + length),maxlen,buf,arg,&count);
                            break; 
                          }
                          case 1:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],arg,&count);
                            break; 
                          }
                          case 2:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],prefixes[1],arg,&count);
                            break; 
                          }
                          default:
{
                            abort();
                          }
                        };
                      }
                      break; 
                    }
#if HAVE_WCHAR_T
                    case TYPE_WIDE_STRING:
{
{
                        const wchar_t *arg = a.arg[dp -> arg_index].a.a_wide_string;
                        switch(prefix_count){
                          case 0:
{
                            retcount = snprintf((result + length),maxlen,buf,arg,&count);
                            break; 
                          }
                          case 1:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],arg,&count);
                            break; 
                          }
                          case 2:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],prefixes[1],arg,&count);
                            break; 
                          }
                          default:
{
                            abort();
                          }
                        };
                      }
                      break; 
                    }
#endif
                    case TYPE_POINTER:
{
{
                        void *arg = a.arg[dp -> arg_index].a.a_pointer;
                        switch(prefix_count){
                          case 0:
{
                            retcount = snprintf((result + length),maxlen,buf,arg,&count);
                            break; 
                          }
                          case 1:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],arg,&count);
                            break; 
                          }
                          case 2:
{
                            retcount = snprintf((result + length),maxlen,buf,prefixes[0],prefixes[1],arg,&count);
                            break; 
                          }
                          default:
{
                            abort();
                          }
                        };
                      }
                      break; 
                    }
                    default:
{
                      abort();
                    }
                  }
#if USE_SNPRINTF
/* Portability: Not all implementations of snprintf()
		       are ISO C 99 compliant.  Determine the number of
		       bytes that snprintf() has produced or would have
		       produced.  */
                  if (count >= 0) {
/* Verify that snprintf() has NUL-terminated its
			   result.  */
                    if ((count < maxlen) && ((result + length)[count] != 0)) 
                      abort();
/* Portability hack.  */
                    if (retcount > count) 
                      count = retcount;
                  }
                  else {
/* snprintf() doesn't understand the '%n'
			   directive.  */
                    if (fbp[1] != 0) {
/* Don't use the '%n' directive; instead, look
			       at the snprintf() return value.  */
                      fbp[1] = 0;
                      continue; 
                    }
                    else {
/* Look at the snprintf() return value.  */
                      if (retcount < 0) {
/* HP-UX 10.20 snprintf() is doubly deficient:
				   It doesn't understand the '%n' directive,
				   *and* it returns -1 (rather than the length
				   that would have been required) when the
				   buffer is too small.  */
                        size_t bigger_need = xsum(((allocated <= 18446744073709551615UL / 2)?(((size_t )allocated) * 2) : 18446744073709551615UL),12);
                        if (bigger_need > allocated) {
                          size_t memory_size;
                          char *memory;
                          allocated = ((allocated > 0)?(((allocated <= 18446744073709551615UL / 2)?(((size_t )allocated) * 2) : 18446744073709551615UL)) : 12);
                          if (bigger_need > allocated) 
                            allocated = bigger_need;
                          memory_size = (((allocated <= 18446744073709551615UL / sizeof(char ))?(((size_t )allocated) * sizeof(char )) : 18446744073709551615UL));
                          if (memory_size == 18446744073709551615UL) 
                            goto out_of_memory;
                          if ((result == resultbuf) || (result == ((char *)((void *)0)))) 
                            memory = ((char *)(malloc(memory_size)));
                          else 
                            memory = ((char *)(realloc(result,memory_size)));
                          if (memory == ((char *)((void *)0))) 
                            goto out_of_memory;
                          if ((result == resultbuf) && (length > 0)) 
                            memcpy(memory,result,length);
                          result = memory;
                        };
                        continue; 
                      }
                      else 
                        count = retcount;
                    }
                  }
#endif
/* Attempt to handle failure.  */
                  if (count < 0) {
                    if (!((result == resultbuf) || (result == ((char *)((void *)0))))) 
                      free(result);
                    if (buf_malloced != ((char *)((void *)0))) 
                      free(buf_malloced);
                    free(d.dir);
                    if (a.arg != 0) 
                      free(a.arg);;
                     *__errno_location() = 22;
                    return 0;
                  }
#if USE_SNPRINTF
/* Handle overflow of the allocated buffer.
		       If such an overflow occurs, a C99 compliant snprintf()
		       returns a count >= maxlen.  However, a non-compliant
		       snprintf() function returns only count = maxlen - 1.  To
		       cover both cases, test whether count >= maxlen - 1.  */
                  if ((((unsigned int )count) + 1) >= maxlen) {
/* If maxlen already has attained its allowed maximum,
			   allocating more memory will not increase maxlen.
			   Instead of looping, bail out.  */
                    if (maxlen == 2147483647 / (sizeof(char ) / sizeof(char ))) 
                      goto overflow;
                    else {
/* Need at least (count + 1) * sizeof (TCHAR_T)
			       bytes.  (The +1 is for the trailing NUL.)
			       But ask for (count + 2) * sizeof (TCHAR_T)
			       bytes, so that in the next round, we likely get
			         maxlen > (unsigned int) count + 1
			       and so we don't get here again.
			       And allocate proportionally, to avoid looping
			       eternally if snprintf() reports a too small
			       count.  */
                      size_t n = xmax(xsum(length,((((((unsigned int )count) + 2) + sizeof(char ) / sizeof(char )) - 1) / (sizeof(char ) / sizeof(char )))),((allocated <= 18446744073709551615UL / 2)?(((size_t )allocated) * 2) : 18446744073709551615UL));
                      if (n > allocated) {
                        size_t memory_size;
                        char *memory;
                        allocated = ((allocated > 0)?(((allocated <= 18446744073709551615UL / 2)?(((size_t )allocated) * 2) : 18446744073709551615UL)) : 12);
                        if (n > allocated) 
                          allocated = n;
                        memory_size = (((allocated <= 18446744073709551615UL / sizeof(char ))?(((size_t )allocated) * sizeof(char )) : 18446744073709551615UL));
                        if (memory_size == 18446744073709551615UL) 
                          goto out_of_memory;
                        if ((result == resultbuf) || (result == ((char *)((void *)0)))) 
                          memory = ((char *)(malloc(memory_size)));
                        else 
                          memory = ((char *)(realloc(result,memory_size)));
                        if (memory == ((char *)((void *)0))) 
                          goto out_of_memory;
                        if ((result == resultbuf) && (length > 0)) 
                          memcpy(memory,result,length);
                        result = memory;
                      };
                      continue; 
                    }
                  }
#endif
#if NEED_PRINTF_UNBOUNDED_PRECISION
/* Handle the precision.  */
# if USE_SNPRINTF
# else
# endif
/* Put the additional zeroes after the sign.  */
/* Put the additional zeroes after the 0x prefix if
			   (flags & FLAG_ALT) || (dp->conversion == 'p').  */
/* Insert zeroes.  */
# if USE_SNPRINTF
# endif
#endif
#if !USE_SNPRINTF
/* tmp_length was incorrectly calculated - fix the
			 code above!  */
#endif
#if !DCHAR_IS_TCHAR
/* Convert from TCHAR_T[] to DCHAR_T[].  */
/* type = TYPE_CHAR or TYPE_WIDE_CHAR or TYPE_STRING
			   TYPE_WIDE_STRING.
			   The result string is not certainly ASCII.  */
/* This code assumes that TCHAR_T is 'char'.  */
# if USE_SNPRINTF
# else
# endif
/* The result string is ASCII.
			   Simple 1:1 conversion.  */
# if USE_SNPRINTF
/* If sizeof (DCHAR_T) == sizeof (TCHAR_T), it's a
			   no-op conversion, in-place on the array starting
			   at (result + length).  */
# endif
# if USE_SNPRINTF
/* ENSURE_ALLOCATION will not move tmpsrc
				   (because it's part of resultbuf).  */
/* ENSURE_ALLOCATION will move the array
				   (because it uses realloc().  */
# else
# endif
/* Copy backwards, because of overlapping.  */
#endif
#if DCHAR_IS_TCHAR && !USE_SNPRINTF
/* Make room for the result.  */
/* Need at least count elements.  But allocate
			   proportionally.  */
#endif
/* Here count <= allocated - length.  */
/* Perform padding.  */
#if !DCHAR_IS_TCHAR || ENABLE_UNISTDIO || NEED_PRINTF_FLAG_LEFTADJUST || NEED_PRINTF_FLAG_ZERO || NEED_PRINTF_UNBOUNDED_PRECISION
# if ENABLE_UNISTDIO
/* Outside POSIX, it's preferrable to compare the width
			   against the number of _characters_ of the converted
			   value.  */
# else
/* The width is compared against the number of _bytes_
			   of the converted value, says POSIX.  */
# endif
/* Make room for the result.  */
/* Need at least count + pad elements.  But
				   allocate proportionally.  */
# if USE_SNPRINTF
# else
# endif
/* Here count + pad <= allocated - length.  */
# if !DCHAR_IS_TCHAR || USE_SNPRINTF
# else
# endif
# if !DCHAR_IS_TCHAR || ENABLE_UNISTDIO
/* No zero-padding for string directives.  */
# endif
/* No zero-padding of "inf" and "nan".  */
/* The generated string now extends from rp to p,
				 with the zero padding insertion point being at
				 pad_ptr.  */
/* = end - rp */
/* Pad with spaces on the right.  */
/* Pad with zeroes.  */
/* Pad with spaces on the left.  */
#endif
/* Here still count <= allocated - length.  */
#if !DCHAR_IS_TCHAR || USE_SNPRINTF
/* The snprintf() result did fit.  */
#else
/* Append the sprintf() result.  */
#endif
#if !USE_SNPRINTF
#endif
#if NEED_PRINTF_DIRECTIVE_F
/* Convert the %f result to upper case for %F.  */
#endif
                  length += count;
                  break; 
                }
              }
            }
          }
        }
      }
    }
/* Add the final NUL.  */
    if (xsum(length,1) > allocated) {
      size_t memory_size;
      char *memory;
      allocated = ((allocated > 0)?(((allocated <= 18446744073709551615UL / 2)?(((size_t )allocated) * 2) : 18446744073709551615UL)) : 12);
      if (xsum(length,1) > allocated) 
        allocated = xsum(length,1);
      memory_size = (((allocated <= 18446744073709551615UL / sizeof(char ))?(((size_t )allocated) * sizeof(char )) : 18446744073709551615UL));
      if (memory_size == 18446744073709551615UL) 
        goto out_of_memory;
      if ((result == resultbuf) || (result == ((char *)((void *)0)))) 
        memory = ((char *)(malloc(memory_size)));
      else 
        memory = ((char *)(realloc(result,memory_size)));
      if (memory == ((char *)((void *)0))) 
        goto out_of_memory;
      if ((result == resultbuf) && (length > 0)) 
        memcpy(memory,result,length);
      result = memory;
    };
    result[length] = 0;
    if ((result != resultbuf) && ((length + 1) < allocated)) {
/* Shrink the allocated memory if possible.  */
      char *memory;
      memory = ((char *)(realloc(result,((length + 1) * sizeof(char )))));
      if (memory != ((char *)((void *)0))) 
        result = memory;
    }
    if (buf_malloced != ((char *)((void *)0))) 
      free(buf_malloced);
    free(d.dir);
    if (a.arg != 0) 
      free(a.arg);;
     *lengthp = length;
/* Note that we can produce a big string of a length > INT_MAX.  POSIX
       says that snprintf() fails with errno = EOVERFLOW in this case, but
       that's only because snprintf() returns an 'int'.  This function does
       not have this limitation.  */
    return result;
#if USE_SNPRINTF
    overflow:
    if (!((result == resultbuf) || (result == ((char *)((void *)0))))) 
      free(result);
    if (buf_malloced != ((char *)((void *)0))) 
      free(buf_malloced);
    free(d.dir);
    if (a.arg != 0) 
      free(a.arg);;
     *__errno_location() = 75;
    return 0;
#endif
    out_of_memory:
    if (!((result == resultbuf) || (result == ((char *)((void *)0))))) 
      free(result);
    if (buf_malloced != ((char *)((void *)0))) 
      free(buf_malloced);
    out_of_memory_1:
    free(d.dir);
    if (a.arg != 0) 
      free(a.arg);;
     *__errno_location() = 12;
    return 0;
  }
}
#undef TCHARS_PER_DCHAR
#undef SNPRINTF
#undef USE_SNPRINTF
#undef DCHAR_SET
#undef DCHAR_CPY
#undef PRINTF_PARSE
#undef DIRECTIVES
#undef DIRECTIVE
#undef DCHAR_IS_TCHAR
#undef TCHAR_T
#undef DCHAR_T
#undef FCHAR_T
#undef VASNPRINTF
